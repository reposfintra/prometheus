<%-- 
    Document   : Demanda
    Created on : 9/09/2015, 02:38:32 PM
    Author     : mcastillo
--%>

<%@page contentType="text/html" pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
        <title>Procesos en Demanda</title>
        
        <link type="text/css" rel="stylesheet" href="./css/jquery/jquery-ui/jquery-ui.css" />
        <link type="text/css" rel="stylesheet" href="./css/buttons/botonVerde.css"/>
        <link href="./css/popup.css" rel="stylesheet" type="text/css">

        <script type="text/javascript" src="./js/jquery/jquery.jqGrid/js/jquery.min.js"></script> 
        <script type="text/javascript" src="./js/jquery-ui-1.8.5.custom.min.js"></script>
<!--        <script src="./js/jquery/editor/nicEdit.js" type="text/javascript"></script>-->
        <script src="./js/jquery/editor/nicEdit-latest.js" type="text/javascript"></script>      
        <script src="./js/procesoEjecutivo.js" type="text/javascript"></script>

        <!--css logica de negocio-->
        <link href="./css/proceso_ejecutivo.css" rel="stylesheet" type="text/css">

        <!--jqgrid--> 
        <script type="text/javascript" src="./js/jquery/jquery.jqGrid/js/grid.locale-es.js"></script>
        <script type="text/javascript" src="./js/jquery/jquery.jqGrid/js/jquery.jqGrid.min.js"></script>
    </head>
    <body>
        <div id="capaSuperior"  style="position:absolute; width:100%; height:100px; z-index:0; left: 0px; top: 0px;">
            <jsp:include page="/WEB-INF/toptsp.jsp?encabezado=PROCESOS EN ESTADO DE DEMANDA"/>
        </div>
        <div id="capaCentral" style="position:absolute; width:100%; height:100%; z-index:0; left: 0px; top: 100px; ">
            <center>
                <div id="tabs_proceso" style="width: 1750px;height:765px">
                    <ul id="lst_etapas">

                    </ul>                  
                </div>               
            </center>
<!--            <fieldset>
                <legend class="labels">FILTROS</legend>

                <table width="1900" border="0" align="left" cellpadding="1" cellspacing="1" class="labels">
                    <tr>
                        <td width="190">
                            <fieldset>
                                <legend>UNIDADES DE NEGOCIO</legend>
                                <table border="0" align="center" cellpadding="1" cellspacing="1" class="labels">
                                    <tr>
                                        <td>
                                            <select name="unidad_negocio" class="combo_180px" id="unidad_negocio">

                                            </select>
                                        </td>
                                    </tr>
                                </table>
                            </fieldset>
                        </td>                                   

                        <td>
                            <span  class="form-submit-button form-submit-button-simple_green_apple" id="btn_search_negocios" />Buscar </span>
                        </td>
                    </tr>
                </table>
            </fieldset>
            <center>
                <br>
                <table id="tabla_demandas"></table>
                <div id="page_tabla_demandas"></div>    
            </center>-->
            <!-- Dialogo ventana ingreso de acciones al proceso> -->
            <div id="dialogAsignarRespProceso"  style="display:none;">       
                <br>
                <fieldset>                   
                    <table border="0" align="center" cellpadding="1" cellspacing="1" class="labels">
                        <tr>
                            <td style="width: 25%"><span>Estado del proceso<b style="color:red">*</b></span></td>        
                            <td style="width: 75%">
                                <select name="respuesta_etapa" class="combo_180px" id="respuesta_etapa" style="width: 250px"></select>
                            </td>
                        </tr>
                        <tr>
                                <td style="width: 25%"><span>Comentario:</span></td>   
                                <td style="width: 75%" colspan="4"><textarea id ="comentario" name="comentario" cols="55"  rows="3" maxlength="300"></textarea></td>                        
                        </tr>
                    </table>
                </fieldset>
            </div>
            <!-- Dialogo ventana asignación costos al proceso> -->
            <div id="dialogAsignarCostos" title="Asignación de costos al proceso" style="display:none;">       
                <center><table id="tabla_costos_proceso"></table></center>
            </div>
            <!-- Dialogo ventana agregar comentario> -->
            <div id="dialogAddComent" style="display:none;">       
                <br>                          
                    <table border="0" align="center" cellpadding="1" cellspacing="1" class="labels">                      
                        <tr>
                                <td style="width: 20%"><span>Comentario:</span></td>   
                                <td style="width: 80%" colspan="4"><textarea id ="comment" name="comment" cols="55"  rows="4" maxlength="300"></textarea></td>                        
                        </tr>
                    </table> 
            </div>
            <!-- Dialogo ventana ver trazabilidad -->
            <div id="dialogTrazabilidad" title="Ver Trazabilidad Proceso" style="display:none;">       
                <center><table id="tabla_trazabilidad"></table></center>
            </div>     
            <!-- Dialogo ventana radicado de juzgado> -->
            <div id="dialogRadicadoJuzgado"  style="display:none;">       
                <br>
                <fieldset>                   
                    <table border="0" align="left" cellpadding="1" cellspacing="1" class="labels">
                        <tr>
                            <td style="width: 25%"><span>Juzgado <b style="color:red">*</b></span></td>        
                            <td style="width: 75%">
                                <select name="juzgados" class="combo_180px" id="juzgados" style="width: 270px"></select>
                            </td>
                        </tr>
                        <tr>
                                <td style="width: 25%"><span>Radicado <b style="color:red">*</b></span></td>   
                                <td style="width: 75%"><input type="text" id="radicado" name="radicado" style=" width: 262px"></td>                        
                        </tr>
                    </table>
                </fieldset>
            </div>
            
             <!-- Dialogo ventana generación de demanda -->
            <div id="div_generar_demanda"  style="display: none; width: 950px">
                <div id="tabs" style="width: 930px">
                    <ul>
                        <li><a href="#tabDemanda">Demanda</a></li>
                        <li><a href="#tabMedidas">Medidas Cautelares</a></li>
                        <li><a href="#tabPoder">Poder</a></li>
                    </ul>
                    <input type="hidden" id="id_demanda" name="id_demanda" readOnly>     
                    <div id="tabDemanda">
                        <input type="hidden" id="id_doc_dem" name="id_doc_dem" readOnly>   
                        <h4>Encabezado</h4> 
                        <textarea id ="header_dem" name="header_dem"  class="editor" rows="5"></textarea>
                        </br> 

                        <h4>Intro</h4> 
                        <textarea id ="intro_dem" name="intro_dem" class="editor"  rows="10"></textarea>
                        </br> 
                        
                        <div id="divHechosDem">
                            <h4> <span> HECHOS <img id="mas_hechos" src="/fintra/images/botones/iconos/add.png" 
                                                    style="margin-left: 5px; height: 100%; vertical-align: middle;">    
                                </span> </h4>               
                        </div>
                        
                        <div id="divPretensionesDem">
                            <h4> <span> PRETENSIONES <img id="mas_pretensiones" src="/fintra/images/botones/iconos/add.png" 
                                                          style="margin-left: 5px; height: 100%; vertical-align: middle;">
                                </span> </h4>
                        </div>

                        <h4>FUNDAMENTOS DE DERECHO</h4> 
                        <textarea id ="fundamentos" name="fundamentos" class="editor" rows="3"></textarea>
                        </br> 

                        <h4>COMPETENCIA Y CUANTIA</h4> 
                        <textarea id ="competencia" name="competencia" class="editor" rows="3"></textarea>
                        </br> 

                        <h4>PRUEBAS</h4> 
                        <textarea id ="prueba" name="prueba" class="editor" rows="5"></textarea>
                        </br> 

                        <h4>ANEXOS</h4> 
                        <textarea id ="anexo" name="anexo" class="editor" rows="3"></textarea>
                        </br> 

                        <h4>NOTIFICACIONES</h4> 
                        <textarea id ="notificacion" name="notificacion" class="editor" rows="10" ></textarea>
                        </br> 

                        <h4>Pie del documento</h4> 
                        <textarea id ="footer_dem" name="footer_dem" class="editor"  rows="5" ></textarea>
                        </br> 

<!--                    <h4>Pie de pagina</h4> 
                        <textarea id ="footerpage_dem" name="footerpage_dem"  class="editor" rows="3"></textarea>
                        </br>                  -->
                    </div>                    
                    <div id="tabMedidas">
                        <input type="hidden" id="id_doc_med" name="id_doc_med" readOnly>   
                        <h4>Encabezado</h4> 
                        <textarea id ="header_med" name="header_med" class="editor"  rows="5" ></textarea>
                        </br> 

                        <h4>Intro</h4> 
                        <textarea id ="intro_med" name="intro_med" class="editor" rows="10"></textarea>
                        </br> 
                        
                        <div id="divMedidas">    
                            <h4> <span> MEDIDAS <img id="mas_medidas" src="/fintra/images/botones/iconos/add.png" 
                                                          style="margin-left: 5px; height: 100%; vertical-align: middle;">
                                </span> </h4>   
                        </div>


                        <h4>Pie del documento</h4> 
                        <textarea id ="footer_med" name="footer_med" class="editor"  rows="5" ></textarea>
                        </br> 

<!--                    <h4>Pie de pagina</h4> 
                        <textarea id ="footerpage_med" name="footerpage_med" class="editor"  rows="3" ></textarea>
                        </br>  -->
                    </div>
                    <div id="tabPoder">
                         <input type="hidden" id="id_doc_pod" name="id_doc_pod" readOnly>   
                        <h4>Encabezado</h4> 
                        <textarea id ="header_pod" name="header_pod" class="editor"  rows="5"></textarea>
                        </br> 

                        <h4>Intro</h4> 
                        <textarea id ="intro_pod" name="intro_pod" class="editor" rows="10" ></textarea>
                        </br> 


                        <h4>Pie del documento</h4> 
                        <textarea id ="footer_pod" name="footer_pod" class="editor" rows="5"></textarea>
                        </br> 

<!--                    <h4>Pie de pagina</h4> 
                        <textarea id ="footerpage_pod" name="footerpage_pod" class="editor"  rows="3"></textarea>
                        </br> -->
                    </div>
                </div>
            </div> 
            <div id="dialogMsj" title="Mensaje" style="display:none;">
                <p style="font-size: 12px;text-align:justify;" id="msj" > Texto </p>
            </div>  
             
            <div id="dialogLoading" style="display:none;z-index:1100 !important">
                <p  style="font-size: 12px;text-align:justify;" id="msj2">Texto </p> <br/>
                <center>
                    <img src="./images/cargandoCM.gif"/>
                </center>
            </div>
            <div id="divSalidaEx" title="Exportacion" style=" display: block" >
                 <p  id="msjEx" style=" display:  none"> Espere un momento por favor...</p>
                 <center>
                     <img id="imgloadEx" style="position: relative;  top: 7px; display: none " src="./images/cargandoCM.gif"/>
                 </center>
                 <div id="respEx" style=" display: none"></div>
            </div> 
             
        </div>
        <script type="text/javascript">
     

            bkLib.onDomLoaded(function() { 
                //nicEditors.allTextAreas({maxHeight : 280});{fullPanel : true}
                new nicEditor({maxHeight : 280, buttonList : ['bold','italic','underline','left','center','right','justify','ol','ul','fontSize','fontFamily','indent','outdent' ]}).panelInstance('header_dem');
                new nicEditor({maxHeight : 280, buttonList : ['bold','italic','underline','left','center','right','justify','ol','ul','fontSize','fontFamily','indent','outdent' ]}).panelInstance('intro_dem');
                new nicEditor({maxHeight : 280, buttonList : ['bold','italic','underline','left','center','right','justify','ol','ul','fontSize','fontFamily','indent','outdent' ]}).panelInstance('fundamentos');
                new nicEditor({maxHeight : 280, buttonList : ['bold','italic','underline','left','center','right','justify','ol','ul','fontSize','fontFamily','indent','outdent' ]}).panelInstance('competencia');
                new nicEditor({maxHeight : 280, buttonList : ['bold','italic','underline','left','center','right','justify','ol','ul','fontSize','fontFamily','indent','outdent' ]}).panelInstance('prueba');
                new nicEditor({maxHeight : 280, buttonList : ['bold','italic','underline','left','center','right','justify','ol','ul','fontSize','fontFamily','indent','outdent' ]}).panelInstance('anexo');
                new nicEditor({maxHeight : 280, buttonList : ['bold','italic','underline','left','center','right','justify','ol','ul','fontSize','fontFamily','indent','outdent' ]}).panelInstance('notificacion');
//              new nicEditor({maxHeight : 280, buttonList : ['bold','italic','underline','left','center','right','justify','ol','ul','fontSize','fontFamily','indent','outdent' ]}).panelInstance('concl_dem');
                new nicEditor({maxHeight : 280, buttonList : ['bold','italic','underline','left','center','right','justify','ol','ul','fontSize','fontFamily','indent','outdent' ]}).panelInstance('footer_dem');
//              new nicEditor({maxHeight : 280, buttonList : ['bold','italic','underline','left','center','right','justify','ol','ul','fontSize','fontFamily','indent','outdent' ]}).panelInstance('footerpage_dem');
                new nicEditor({maxHeight : 280, buttonList : ['bold','italic','underline','left','center','right','justify','ol','ul','fontSize','fontFamily','indent','outdent' ]}).panelInstance('header_med');
                new nicEditor({maxHeight : 280, buttonList : ['bold','italic','underline','left','center','right','justify','ol','ul','fontSize','fontFamily','indent','outdent' ]}).panelInstance('intro_med');               
//              new nicEditor({maxHeight : 280, buttonList : ['bold','italic','underline','left','center','right','justify','ol','ul','fontSize','fontFamily','indent','outdent' ]}).panelInstance('concl_med');
                new nicEditor({maxHeight : 280, buttonList : ['bold','italic','underline','left','center','right','justify','ol','ul','fontSize','fontFamily','indent','outdent' ]}).panelInstance('footer_med');
//              new nicEditor({maxHeight : 280, buttonList : ['bold','italic','underline','left','center','right','justify','ol','ul','fontSize','fontFamily','indent','outdent' ]}).panelInstance('footerpage_med');
                new nicEditor({maxHeight : 280, buttonList : ['bold','italic','underline','left','center','right','justify','ol','ul','fontSize','fontFamily','indent','outdent' ]}).panelInstance('header_pod');
                new nicEditor({maxHeight : 280, buttonList : ['bold','italic','underline','left','center','right','justify','ol','ul','fontSize','fontFamily','indent','outdent' ]}).panelInstance('intro_pod');
//              new nicEditor({maxHeight : 280, buttonList : ['bold','italic','underline','left','center','right','justify','ol','ul','fontSize','fontFamily','indent','outdent' ]}).panelInstance('concl_pod');
                new nicEditor({maxHeight : 280, buttonList : ['bold','italic','underline','left','center','right','justify','ol','ul','fontSize','fontFamily','indent','outdent' ]}).panelInstance('footer_pod');
//              new nicEditor({maxHeight : 280, buttonList : ['bold','italic','underline','left','center','right','justify','ol','ul','fontSize','fontFamily','indent','outdent' ]}).panelInstance('footerpage_pod');
            });

            initDemanda();      
        </script>
<!--        <script> 
               initDemanda();       
        </script>-->
    </body>
</html>
