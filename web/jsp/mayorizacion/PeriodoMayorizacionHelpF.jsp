<%-- 
    Document   : PeriodoMayorizacionHelpF
    Created on : 18/02/2010, 02:19:45 PM
    Author     : MGarizao - GEOTECH
--%>

<%@ page session="true"%>
<%@page errorPage="../error/ErrorPage.jsp"%>
<%@page import="com.tsp.operation.model.beans.*"%>
<%@page import="java.util.*"%>
<%@ include file="/WEB-INF/InitModel.jsp"%>
<%@page contentType="text/html" pageEncoding="ISO-8859-1"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
   "http://www.w3.org/TR/html4/loose.dtd">

<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
        <link href="<%=BASEURL%>/css/letras.css" rel="stylesheet" type="text/css">
        <link href="<%=BASEURL%>/css/estilostsp.css" rel="stylesheet" type="text/css">
         <script type="text/javascript" src="<%=BASEURL%>/js/boton.js" ></script>
        <title>Ayuda mayorizacion</title>
    </head>
    <body>
        <BR>
        <table width="550"  border="2" align="center">
            <tr>
                <td width="550" >
                    <table width="100%" border="0" align="center">
                        <tr  class="subtitulo">
                            <td height="24" align="center">MAYORIZACION </td>
                        </tr>
                        <tr class="subtitulo1">
                            <td> FORMULARIO PARA EL PROCESO DE MAYORIZACION</td>
                        </tr>
                        <tr class="ayudaHtmlTexto">
                            <td align="center">
                                <br>
                                <img src="<%= BASEURL%>/images/ayuda/mayorizacion/mayorizacion.png" align="absmiddle">

                            </td>
                        </tr>

                        <tr class="ayudaHtmlTexto">
                            <td>
                                <br>
							Luego de iniciar el proceso de mayorizacion en el sistema, le debe salir el siguiente mensaje confirmando la accion.
                                <br>
                                <center>
                                    <img src="<%= BASEURL%>/images/ayuda/mayorizacion/msnMayor.png" align="absmiddle" >
                                </center>

                            </td>
                        </tr>
                       
                    </table>
                </td>
            </tr>
        </table>
        <br>
        <center>
            <img src="<%=BASEURL%>/images/botones/salir.gif" name="c_salir" onClick="window.close();" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" align="absmiddle">
        </center>
        <br>
        <p align="center" class="fuenteAyuda"> Transportes Sanchez Polo. copyright <span class="Estilo1">&copy;</span> 2006 </p>
        <br>
    </body>
</html>
