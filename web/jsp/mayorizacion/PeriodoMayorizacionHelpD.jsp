<%-- 
    Document   : PeriodoMayorizacionHelpD
    Created on : 18/02/2010, 02:19:21 PM
    Author     : MGarizao - GEOTECH
--%>

<%@page errorPage="../error/ErrorPage.jsp"%>
<%@page import="com.tsp.operation.model.beans.*"%>
<%@page import="java.util.*"%>
<%@ include file="/WEB-INF/InitModel.jsp"%>
<%@page contentType="text/html" pageEncoding="ISO-8859-1"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
   "http://www.w3.org/TR/html4/loose.dtd">

<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
        <link href="<%=BASEURL%>/css/letras.css" rel="stylesheet" type="text/css">
        <link href="<%=BASEURL%>/css/estilostsp.css" rel="stylesheet" type="text/css">
        <script type="text/javascript" src="<%=BASEURL%>/js/boton.js" ></script>
        <title>Ayuda mayorizacion</title>
    </head>
    <body>
        <table width="696"  border="2" align="center">
  <tr>
    <td width="811" height="" >
      <table width="100%" border="0" align="center">
        <tr  class="subtitulo">
          <td height="24" colspan="2" align="center">MAYORIZACION</td>
        </tr>
		<tr class="subtitulo1">
          <td colspan="2"> DATOS DE INGRESO</td>
        </tr>
        <tr>
          <td width="149" class="fila">Mes </td>
          <td width="525"  class="ayudaHtmlTexto">Mes de captura del periodo a mayorizar. </td>
        </tr>
        <tr>
          <td width="149" class="fila"> A�o </td>
          <td width="525"  class="ayudaHtmlTexto">A�o de captura del periodo a mayorizar. </td>
        </tr>
       
      </table>

  </tr>
</table>
<br>
<center>
	<img src="<%=BASEURL%>/images/botones/salir.gif" name="c_salir" onClick="window.close();" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" align="absmiddle">
</center>
<br>
<p align="center" class="fuenteAyuda"> Transportes Sanchez Polo. copyright <span class="Estilo1">&copy;</span> 2006 </p>
<br>
    </body>
</html>
