<%--
    Document   : PeriodoMayorizacion
    Created on : 12/02/2010, 11:56:22 AM
    Author     : MGarizao - GEOTECH
--%>

<%@ page session="true"%>
<%@page errorPage="../error/ErrorPage.jsp"%>
<%@page import="com.tsp.operation.model.beans.*"%>
<%@page import="java.util.*"%>
<%@ include file="/WEB-INF/InitModel.jsp"%>
<%@page contentType="text/html" pageEncoding="ISO-8859-1"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
   "http://www.w3.org/TR/html4/loose.dtd">

<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
        <title>Desmayorizacion</title>
        <link href="<%=BASEURL%>/css/letras.css" rel="stylesheet" type="text/css">
        <link href="<%=BASEURL%>/css/estilostsp.css" rel="stylesheet" type="text/css">
        <script type="text/javascript" src="<%=BASEURL%>/js/boton.js" ></script>
        <script type="text/javascript" src="<%=BASEURL%>/js/prototype.js"></script>
        <script type='text/javascript' src="<%=BASEURL%>/js/Mayorizacion.js"></script>
        <script type='text/javascript' src="<%=BASEURL%>/js/validar.js"></script>
    </head>
     <%
                String path = application.getRealPath(model.menuService.getRealUrl(request.getRequestURI()));
                String ayuda_online[] = model.menuService.getContenidoMenu(BASEURL,request.getRequestURI(),path);
                String msn = request.getParameter("msn");
                String mes = request.getParameter("mes");
                String ano = request.getParameter("ano");
                

        %>
    <body>
        <div id="capaSuperior" style="position:absolute; width:100%; height:100px; z-index:0; left: 0px; top: 0px;">
            <jsp:include page="/toptsp.jsp?encabezado=DESMAYORIZACION"/>
        </div>
        <div id="capaCentral"  style="position:absolute; width:100%; height:85%; z-index:0; left: 2px; top: 100px;">
            <form id="frmMayor" name="frmMayor" method="post" action="<%=CONTROLLERCONTAB%>?estado=actualizar&accion=Mayorizacion">
                <input type="hidden" id="modo" name="modo" value="2">
                <input type="hidden" id="mes" value="<%=mes%>">
                <input type="hidden" id="a�o" value="<%=ano%>">
             <table align="center" width="400px" border="2">
                    <tr>
                        <td>
                            <table width="400px" align="center"  class="tablaInferior">
                                <tr class="fila">
                                    <td align="left" class="subtitulo1">Datos del periodo</td>
                                    <td align="left" class="barratitulo"><img src="<%=BASEURL%>/images/titulo.gif" /></td>
                                    <td width=""  class="barratitulo"><%=ayuda_online[0]%></td>
                                </tr>
                            </table>
                                <table width="400px" align="center"  class="tablaInferior">
                                    <tr class="fila">
                                        <td>
                                            Mes
                                        </td>
                                        <td>
                                            <select id="cmbMes"  name="cmbMes"style="width:80px">
                                                <option value="01">Enero</option>
                                                <option value="02">Febrero</option>
                                                <option value="03">Marzo</option>
                                                <option value="04">Abril</option>
                                                <option value="05">Mayo</option>
                                                <option value="06">Junio</option>
                                                <option value="07">Julio</option>
                                                <option value="08">Agosto</option>
                                                <option value="09">Septiembre</option>
                                                <option value="10">Octubre</option>
                                                <option value="11">Noviembre</option>
                                                <option value="12">Diciembre</option>
                                                <option value="13">Fiscal</option>
                                            </select>
                                            <img name="imageField"  src="<%=BASEURL%>/images/botones/iconos/obligatorio.gif" border="0">
                                        </td>
                                    </tr>
                                    <tr class="fila">
                                        <td> A�o</td>
                                        <td>
                                            <select id="cmbAno" name="cmbAno" style="width:80px">

                                            </select>
                                            <img name="imageField"  src="<%=BASEURL%>/images/botones/iconos/obligatorio.gif"  border="0">
                                        </td>
                                    </tr>
                                </table>


                        </td>
                    </tr>
             </table>
            </form>
                                <br>
                               

                                            <%if(msn != null){%>
                                            <table align="center">
                                                <tr>
                                                    <td class="fila"  align="center"><img name="imageField"  src="<%=BASEURL%>/images/botones/aceptarDisable.gif" width="90" height="21" border="0"  onclick="this.disabled=true;" ></td>
                                                    <td class="fila"  align="center"><img name="imageField"  src="<%=BASEURL%>/images/botones/salir.gif" width="90" height="21" border="0"  onclick="parent.close()" onMouseOver="botonOver(this) ;" onMouseOut="botonOut(this);"></td>
                                                </tr>
                                            </table>
                                            <br>
                                            <table border='2' align='center'><tr>
                                                    <td><table width='410' height='73' border='1' align='center'  bordercolor='#F7F5F4' bgcolor='#FFFFFF'><tr>
                                                                <td width='282' align='center' class='mensajes'>El proceso acaba de iniciar,
                                                                    para realizar seguimiento mirar el log de procesos sistema </td>
                                                                <td width='28' background='"<%=BASEURL%>/images/cuadronaranja.JPG'>&nbsp;</td>
                                                                <td width='78'>&nbsp;</td></tr>
                                                        </table></td></tr></table>
                                            <%}else{%>
                                            <table align="center">
                                                <tr>
                                                    <td class="fila"  align="center"><img name="imageField"  src="<%=BASEURL%>/images/botones/aceptar.gif" width="90" height="21" border="0"   onclick="submitform()" onMouseOver="botonOver(this) ;" onMouseOut="botonOut(this);"></td>
                                                    <td class="fila"  align="center"><img name="imageField"  src="<%=BASEURL%>/images/botones/salir.gif" width="90" height="21" border="0"  onclick="parent.close()" onMouseOver="botonOver(this) ;" onMouseOut="botonOut(this);"></td>
                                                </tr>
                                            </table>
                                            <br>
                                            <%}%>
        </div>
                                 <%=ayuda_online[1]%>
    </body>
</html>
