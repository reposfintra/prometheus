<%-- 
    Document   : ReasignarNegocios
    Created on : 2/12/2018, 08:17:57 AM
    Author     : Roberto Parra
--%>
<%@page contentType="text/html" pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<%	
    String id = request.getParameter("id");
%>
<html>

    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
        <title>Reasignación de Negocios</title>
            <link href="/fintra/css/jquery/jquery-ui/jquery-ui.css" rel="stylesheet" type="text/css"/>
            <link href="/fintra/css/popup.css" rel="stylesheet" type="text/css">
    
            <script type='text/javascript' src="/fintra/js/jquery/jquery-ui/jquery.min.js"></script>
            <script type='text/javascript' src="/fintra/js/jquery/jquery-ui/jquery.ui.min.js"></script>
            <script type="text/javascript" src="/fintra/js/jquery/jquery.jqGrid-4.7.0/js/grid.locale-es.js"></script>
            <script type="text/javascript" src="/fintra/js/jquery/jquery.jqGrid-4.7.0/js/jquery.jqGrid.min.js"></script>
            <script type="text/javascript" src="/fintra/js/jquery/jquery.jqGrid-4.7.0/js/jquery.tabletojson.js"></script>
      
   <script type='text/javascript' src="/fintra/js/ReasignacionNegocios.js"></script>
    </head>
<body>
    
         
     <!--Encabezado (Inicio)-->
     <div id="capaSuperior" style="position:absolute; width:100%; z-index:0; left: 0px; top: 0px;">
         <jsp:include page="/WEB-INF/toptsp.jsp?encabezado=VER DATOS DEL NEGOCIO"/>
     </div>
     <!--Encabezado (Fin)-->
         <!--Cuerpo (Inicio)-->
    <div id="capaCentral" style="position: absolute; width: 100%; height: 557px; z-index: 0; left: 0px; top: 100px; overflow: scroll;">        
      <p>&nbsp;</p>
        <div id="capaCentral" style="text-align: center;">
            <center>
    <div id="info"  class="ventana" >
      <p id="notific" ></p>
    </div>
              <div id="info"  class="ventana" >
                <p id="notific" ></p>
              </div>
                <div class="ui-jqgrid ui-widget ui-widget-content ui-corner-all" 
                     style="min-width: 1300px; max-width: 900px; padding: 2em; margin: 2em;">
                    <table>
                        <tbody><tr><td colspan="8" class="ui-dialog-titlebar ui-widget-header ui-corner-all ui-helper-clearfix" style="text-align: center;">INFORMACION DE LA REASIGNACION</td></tr>
                        <tr>
                            <td colspan="4" align="left">Analista<span style="color:red;">*</span>
                                <select id="analista" onchange="RetornarDatosNegAna();">
                                    <option value='-1'>Negocios sin analistas</option>
                                    <option value="todos">Todos</option>
                                </select> 
                            </td>
                           
                            
                            <td colspan="4" align="left">Analista a reasignar<span style="color:red;">*</span>
                                <select id="analista_reasignado" onchange="RetornarDatosNegAnaReasig();">
                                    <option value="-1">Seleccionar analista</option>
                                </select> 
                            </td>
                        </tr>
                        <tr>
                            
                            <td colspan="4" align="rigth">
                                <div id="divTablaNegAna">
                                   <table id="TabNegociosAnalista"></table>
                                </div>
                            </td>
                            <td colspan="4" align="left">
                                <div id="divTabNegAnaReasig">
                                    <table id="TabNegociosAnalistaReasig"></table>
                                </div>
                            </td>
                        </tr>
                       
                          <tr><td colspan="8" class="ui-dialog-titlebar ui-widget-header ui-corner-all ui-helper-clearfix" style="text-align: center;">ESPECIFICAR LOS NEGOCIOS A REASIGNAR</td></tr>
                        <tr>
                            <td colspan="8">
                                <div id="divTabNeg">
                                <table id="TabNegocios"></table>
                                </div>
                            </td>
                        </tr>
                        <tr>
                               <td style="border-top: 1px solid rgb(0, 128, 0); padding: 0.5em; text-align: left;"> 
                                <input type="button" value="Salir" onclick="window.close();" class="ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only" /> 
                               <input type="button" value="Refrescar" onclick="location.reload();" class="ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only" />
                                    <button id="aceptar_btn" class="ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only" role="button" aria-disabled="false" onclick="ReasignarNegocios();">
                                    <span class="ui-button-text">Reasignar</span>
                                </button>
                            </td>
                            <td colspan="8" style="border-top: 1px solid rgb(0, 128, 0); padding: 0.5em;">
                                <span id="mensaje" style="color:red;"></span>
                            </td>

                        </tr>
                    </tbody>
                  </table>
                </div>
            </center>
        </div>

    </div>
   <!--Cuerpo (Fin)-->
</body>
</html>

<script>
         ListaAnalistas();
</script>
