<%-- 
    Document   : distribucion_nomina
    Created on : 17/09/2018, 10:56:16 AM
    Author     : jbermudez
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <meta charset="UTF-8">
        <title>DISTRIBUCIÓN NÓMINA</title>
        <link href="./css/popup.css" rel="stylesheet" type="text/css">
        <script type="text/javascript" src="./js/jquery/jquery.jqGrid/js/jquery.min.js"></script>
        <link type="text/css" rel="stylesheet" href="./css/jquery/jquery-ui/jquery-ui.css" />
        <script type="text/javascript" src="./js/jquery-ui-1.8.5.custom.min.js"></script>
        <style>
            #content-wrapper {
                width: 650px;
                margin: 0 auto;
            }

            fieldset {
                box-sizing: border-box;
                padding: 10px;
                border-width: thin;
                border-style: outset;
                border-color: black;
                border-spacing: 2px;
                border-collapse: separate;
            }

            legend {
                background: #2A88C8;
                color: white;
                padding: 5px;
            }

            #buttons-wrapper {
                margin-top: 10px;
                text-align: center;
            }

            #boton-aceptar:hover {
                cursor: pointer;
                background: url('/fintra/images/botones/aceptarOver.gif') no-repeat;
            }

            #periodo {
                width: 40px;
                margin-right: 10px;
            }

            #boton-aceptar {
                width: 90px;
                height: 21px;
                background: url('/fintra/images/botones/aceptar.gif') no-repeat;
                display: inline-block;
            }

            #boton-salir:hover {
                cursor: pointer;
                background: url('/fintra/images/botones/salirOver.gif') no-repeat;
            }

            #boton-salir {
                width: 90px;
                height: 21px;
                background: url('/fintra/images/botones/salir.gif') no-repeat;
                display: inline-block;
            }

            .ventana {
                display: none;
            }
        </style>
    </head>
    <body>
        <div id="capaSuperior" style="height:100px;">
            <jsp:include page="/WEB-INF/toptsp.jsp?encabezado=DISTRIBUCIÓN NÓMINA"/>
        </div>
        <div id="content-wrapper">
            <form id="formulario" action="/fintra/controller?estado=AdmonRecursos&accion=Humanos&opcion=155" method="post" enctype="multipart/form-data">
                <fieldset>
                    <legend>Archivo de distribución</legend>
                    <label for="archivo">Seleccione su archivo</label>
                    <input type="file" name="archivo" required id="archivo" accept=".xlsx,application/vnd.ms-excel,application/vnd.openxmlformats-officedocument.spreadsheetml.sheet">
                    <label for="periodo">Periodo</label>
                    <input type="text" id="periodo" maxlength="6">
                    <label for="periodos-checkbox">Múltiples periodos</label>
                    <input type="checkbox" id="periodos-checkbox"/>
                    <div id="buttons-wrapper">
                        <div id="boton-aceptar" onclick="submit()"></div>
                        <div id="boton-salir" onclick="salir()"></div>
                    </div>
                </fieldset>
            </form>
        </div>
        <div id="info"  class="ventana" >
            <p id="notific">EXITO AL GUARDAR</p>
        </div>
        <div id="dialogLoading"  class="ventana" >
            <p  id="msj2">texto </p> <br/>
            <center>
                <img src="/fintra/images/cargandoCM.gif"/>
            </center>
        </div>
        <script>
            document.getElementById("periodos-checkbox").addEventListener("change", (e) => {
                let periodo = document.getElementById("periodo");
                periodo.disabled = !periodo.disabled;
            });
            
            
            function submit() {
                let archivo = document.getElementById("archivo");
                let periodo = document.getElementById("periodo");
                let periodos = document.getElementById("periodos-checkbox");

                if ((periodo.value === "" && !periodos.checked) || archivo.files.length === 0) {
                    mensajesDelSistema("Falta informaciòn por digitar")
                } else {
                    loading("Esta operación puede tardar varios minutos, por favor espere.", 450);
                    
                    let xhr;
                    if (window.XMLHttpRequest) {
                        xhr = new XMLHttpRequest();
                    } else {
                        xhr = new ActiveXObject();
                    }

                    let fd = new FormData();
                    fd.append("archivo", archivo.files[0]);

                    xhr.onreadystatechange = function () {
                        if (xhr.status === 200) {
                            if (xhr.readyState === 4) {
                                $("#dialogLoading").dialog("close");

                                let json = JSON.parse(xhr.responseText);
                                if (json.error) {
                                    mensajesDelSistema(json.error, "300", "170", true);
                                } else {
                                    mensajesDelSistema(json.respuesta, "300", "170", true);
                                }
                            }
                        }
                    }
                    xhr.open("POST", "/fintra/controller?estado=AdmonRecursos&accion=Humanos&opcion=155&periodo=" + periodo.value + "&periodos=" + periodos.checked, true);
                    xhr.send(fd);
                }
            }

            function loading(msj, width) {
                $("#msj2").html(msj);
                $("#dialogLoading").dialog({
                    width: width,
                    height: 130,
                    show: "scale",
                    hide: "scale",
                    resizable: false,
                    position: "center",
                    modal: true,
                    closeOnEscape: true
                });

                $("#dialogLoading").siblings('div.ui-dialog-titlebar').remove();
            }

            function salir() {
                window.close();
            }

            function mensajesDelSistema(msj, width, height, swHideDialog) {
                if (swHideDialog) {
                    $("#notific").html("<span class='ui-icon ui-icon-info' style='float: left; margin: 0 7px 20px 0;'></span> " + msj);
                } else {
                    $("#notific").html("<span class='ui-icon ui-icon-alert' style='float: left; margin: 0 7px 20px 0;'></span> " + msj);
                }
                $("#info").dialog({
                    width: width,
                    height: height,
                    show: "scale",
                    hide: "scale",
                    resizable: false,
                    position: "center",
                    modal: true,
                    closable: true,
                    closeOnEscape: false,
                    buttons: {
                        "Aceptar": function () {
                            $(this).dialog("destroy");
                        }
                    }
                });
                $("#info").siblings('div.ui-dialog-titlebar').remove();
            }
        </script>
    </body>
</html>
