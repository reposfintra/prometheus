<%-- 
    Document   : cambioFechaContabilizacion
    Created on : 4/11/2015, 02:47:16 PM
    Author     : mariana
--%>

<html>
    <head>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
        <link href="./css/popup.css" rel="stylesheet" type="text/css">
        <link type="text/css" rel="stylesheet" href="./css/jquery/jquery-ui/jquery-ui.css" />
        <link type="text/css" rel="stylesheet" href="./css/TransportadorasApi.css" />
        <script type="text/javascript" src="./js/jquery/jquery.jqGrid/js/jquery.min.js"></script>
        <script type="text/javascript" src="./js/jquery-ui-1.8.5.custom.min.js"></script>   
        <script type="text/javascript" src="./js/jquery/jquery.jqGrid-4.7.0/js/grid.locale-es.js"></script>
        <script type="text/javascript" src="./js/jquery/jquery.jqGrid-4.7.0/js/jquery.jqGrid.min.js"></script>
        <script type="text/javascript" src="./js/jquery/jquery.jqGrid-4.7.0/js/jquery.tabletojson.js"></script>    
        <script type="text/javascript" src="./js/cambioFechaContabilizacion.js"></script> 

        <title>CAMBIO DE FECHA</title>
    </head>
    <body>
        <style>


        </style>


        <div id="capaSuperior" style="position:absolute; width:100%; height:100px; z-index:0; left: 0px; top: 0px;">
            <jsp:include page="/WEB-INF/toptsp.jsp?encabezado=CAMBIO DE FECHA"/>
        </div>

        <div id="capaCentral" style="position:absolute; width:100%; height:80%; z-index:0; left: 0px; top: 100px; ">
            <center>

                <div style="background-color: #FFF;
                     width: 500px;
                     height: 95px;
                     border-radius: 4px;
                     padding: 1px;
                     border: 1px solid #2A88C8;
                     margin: 15px auto 0px; " >

                    <div id="encabezadotablita" style="width: 495px">
                        <label class="titulotablita"><b>CAMBIO DOCUMENTOS OPERATIVOS </b></label>
                    </div> 
                    <table border="0" style="width:500px">
                        <tr>
                            <td style="font-size: 12px;width:117px;padding-top:  5px">
                                Seleccione archivo
                            </td>
                            <td style="padding-top:16px">
                                <form id="formulario" name="formulario">
                                    <input type="file" id="examinar" name="examinar"  >
                                </form>
                            </td>
                            <td style="padding-top:  5px">
                                <div id ='botones'>
                                    <button id="subir" class="ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only" 
                                            role="button" aria-disabled="false" >
                                        <span class="ui-button-text">Subir Archivo</span>
                                    </button> 
                                </div>
                            </td>
                        </tr>
                    </table>
                </div>

                <br><br>
                <div id="contenido">
                    <table id="tabla_documentos" ></table>
                    <div id="pager1"></div>
                    <br>

                    <table style="float: left;margin-left: 10px;">  
                        <tr>
                            <td >
                                <div style="background-color: #F32541;width: 17px;height: 17px;margin-bottom: 5px;margin-top: 3px;"></div>
                            </td>
                            <td>
                                <label style="font-size: 14px;">Datos no validos para los cambios</label>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <div style="background-color: #FFB215;width: 17px;height: 17px;margin-bottom: 5px;margin-top: 3px;"></div>
                            </td>
                            <td>
                                <label style="font-size: 14px;">Datos incorrectos o falta de informacion del documento</label>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <div style="background-color: #90CC00;width: 17px;height: 17px;margin-bottom: 5px;margin-top: 3px;"></div>
                            </td>
                            <td>
                                <label style="font-size: 14px;">Datos correctos</label>
                            </td>
                        </tr>
                    </table>
                </div>

                <div id="dialogMsjPrefijos"  class="ventana" >
                    <table id="tabla_prefijos" ></table>
                    <div id="pager"></div>
                </div>

                <div id="dialogMsjDocumentos"  class="ventana" >

                    <table>
                        <tr>
                            <td>
                                <label style="font-size: 12px;">Fecha Inicio</label>
                            </td>
                            <td>
                                <input type="datetime" id="fechainicio" name="fechainicio" style="height: 25px;width: 110px;color: #070708;font-size: 12px;">                            
                            </td>
                            <td>
                                <label style="font-size: 12px;">Fecha Fin</label>
                            </td>
                            <td>
                                <input type="datetime" id="fechafin" name="fechafin" style="height: 25px;width: 110px;color: #070708;font-size: 12px;">
                            </td>
                        </tr>

                        <tr>
                            <td>
                                <label style="font-size: 12px;">Usuario de Creacion</label>
                            </td>
                            <td>
                                <input type="text" id="creacion_user" onchange="conMayusculas(this)"style="height: 25px;width: 110px;color: #070708;font-size: 12px;">
                            </td>
                        </tr>
                        <tr>
                            <td style="font-size: 12px;">Tipo Documento</td>
                            <td>
                                <select id="tipodocumento" style="width: 109px;font-size: 12px;">
                                    <option value="">Seleccione</option>
                                    <option value="CXP">CXP</option>
                                    <option value="CXC">CXC</option>
                                    <option value="INGRESO">Ingreso</option>
                                    <option value="EGRESO">Egreso</option>
                                </select>
                            </td>
                        </tr>

                    </table>
                </div>

                <div id="dialogMsjVerDocumentos"  class="ventana" >
                    <table id="tabla_ver_documentos" ></table>
                    <div id="pager2"></div>
                </div>

                <div id="divSalidaEx" title="Exportacion" style=" display: block" >
                    <p  id="msjEx" style=" display:  none"> Espere un momento por favor...</p>
                    <center>
                        <img id="imgloadEx" style="position: relative;  top: 7px; display: none " src="./images/cargandoCM.gif"/>
                    </center>
                    <div id="respEx" style=" display: none"></div>
                </div> 
                
            </center>
        </div>

        <div id="info"  class="ventana" >
            <p id="notific">EXITO AL GUARDAR</p>
        </div>
    </body>
</html>
