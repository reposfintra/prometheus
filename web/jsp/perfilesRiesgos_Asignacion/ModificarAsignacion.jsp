<%-- 
    Document   : ModificarAsignacion
    Created on : 22/10/2018, 07:50:04 PM
    Author     : Roberto Parra
--%>


<%@page contentType="text/html" pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>

<%	
    String nit = request.getParameter("nit");
%>
<html>

    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
        <title>Modificar Asignacion</title>
            <link href="fintra/css/jquery/jquery-ui/jquery-ui.css" rel="stylesheet" type="text/css"/>
            <link href="css/popup.css" rel="stylesheet" type="text/css">
    
            <script type='text/javascript' src="js/jquery/jquery-ui/jquery.min.js"></script>
            <script type='text/javascript' src="js/jquery/jquery-ui/jquery.ui.min.js"></script>
            <script type="text/javascript" src="js/jquery/jquery.jqGrid-4.7.0/js/grid.locale-es.js"></script>
            <script type="text/javascript" src="js/jquery/jquery.jqGrid-4.7.0/js/jquery.jqGrid.min.js"></script>
            <script type="text/javascript" src="js/jquery/jquery.jqGrid-4.7.0/js/jquery.tabletojson.js"></script>
      
   <script type='text/javascript' src="js/AsigPerfilesRiesgos.js"></script>
    </head>
<body>
    
         
     <!--Encabezado (Inicio)-->
     <div id="capaSuperior" style="position:absolute; width:100%; z-index:0; left: 0px; top: 0px;">
         <jsp:include page="/WEB-INF/toptsp.jsp?encabezado=VER DATOS DEL NEGOCIO"/>
     </div>
     <!--Encabezado (Fin)-->
         <!--Cuerpo (Inicio)-->
    <div id="capaCentral" style="position: absolute; width: 100%; height: 557px; z-index: 0; left: 0px; top: 100px; overflow: scroll;">        
      <p>&nbsp;</p>
        <div id="capaCentral" style="text-align: center;">
            <center>
    <div id="info"  class="ventana" >
      <p id="notific" ></p>
    </div>
                <div class="ui-jqgrid ui-widget ui-widget-content ui-corner-all" style="min-width: 550px; max-width: 900px; padding: 2em; margin: 2em;">
                    <table>
                         <tbody><tr><td colspan="4" class="ui-dialog-titlebar ui-widget-header ui-corner-all ui-helper-clearfix" style="text-align: center;">INFORMACION DE LOS ANALISTAS</td></tr>
                        <tr>
                            <td>Analista<span style="color:red;">*</span></td>
                            <td colspan="3">
                               <select id="usuarios" disabled></select> 
                            </td>
                        </tr>
                        <tr>
                            <td>Rol<span style="color:red;">*</span></td>
                            <td><select id="rol"></select></td>
                        </tr>
                        <tr>
                            <td>Monto minimo para decidir<span style="color:red;">*</span></td>
                            <td><input type="text" id="monto_minimo_decision" maxlength="12" class="solo_numero puntos_de_mil"></select></td>
                        </tr>
                        <tr>
                            <td>Monto m�ximo para decidir<span style="color:red;">*</span></td>
                            <td><input type="text" id="monto_decision" maxlength="12" class="solo_numero puntos_de_mil"></select></td>
                        </tr>
                        <tr><td colspan="4" class="ui-dialog-titlebar ui-widget-header ui-corner-all ui-helper-clearfix" style="text-align: center;">ESPECIFICAR LOS PERFILES A ASIGNAR</td></tr>
                        <tr>
                            <td colspan="4">
                                <table id="Tabperfiles"></table>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="2" style="border-top: 1px solid rgb(0, 128, 0); padding: 0.5em;">
                                <span id="mensaje" style="color:red;"></span>
                            </td>
                            <td style="border-top: 1px solid rgb(0, 128, 0); padding: 0.5em; text-align: right;">
                                 <form id="FormCrearPerfil" name="FormCrearPerfil" action="controller?estado=AsigPerfiles&accion=Riesgos&carpeta=/jsp/perfilesRiesgos_Asignacion&pagina=ListarAnalistas.jsp" autocomplete="on" method="post">                          
         
                                     <p class="login button"> 
                                     <!--<input id="btnCrearPerfil" name="btnCrearPerfil" onclick='CrearPerfil()' type="submit" value="Crear Perfil"  /> -->
                                     <input id="btnCrearPerfil" name="btnCrearPerfil" type="submit" value="Volver" class="ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only" /> 
                                     <input type="button" value="Refrescar" onclick="location.reload();" class="ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only" />
                          
                                     </p>
                                 </form>
                            </td>
                            
                            <td style="border-top: 1px solid rgb(0, 128, 0); padding: 0.5em; text-align: right;"> 
                                 <button id="aceptar_btn" class="ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only" role="button" aria-disabled="false" onclick="ModificarAsignacion('<%=nit%>')">
                                    <span class="ui-button-text">Modificar</span>
                                </button>
                            </td>
                        </tr>
                    </tbody>
                  </table>
                </div>
            </center>
        </div>

    </div>
   <!--Cuerpo (Fin)-->
</body>
</html>

<script>
         
         
         ListarAnalistasFabrica("<%=nit%>","formModi"); 
</script>