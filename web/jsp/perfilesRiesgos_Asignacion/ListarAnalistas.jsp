<%-- 
    Document   : ListarAnalistas
    Created on : 21/10/2018, 06:56:23 PM
    Author     : Roberto Parra
--%>

<%@ page session="true" %>



<!DOCTYPE html>
<html  class="no-js" >
    <head>
        
        
        <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
        <title>Asignacion de Perfiles</title>
        <meta name="viewport" content="width=device-width, initial-scale=1.0"> 
        
    <link href='css/estilostsp.css' rel='stylesheet' type='text/css'/>
    <link href='css/estilostsp.css' rel='stylesheet' type='text/css'/>
    <link href="css/mac_os_x.css" rel="stylesheet" type="text/css"/>
    <link href="css/default.css" rel="stylesheet" type="text/css"/>
   <!-- <script src="js/negocioTrazabilidad.js" type="text/javascript"></script>-->

    <link href="css/jquery/jquery-ui/jquery-ui.css" rel="stylesheet" type="text/css"/>
    
    <!--Estilos para el swich -->
    <link type="text/css" rel="stylesheet" href="css/AsignacionAnalistas.css " />
    <script type='text/javascript' src="js/jquery/jquery-ui/jquery.min.js"></script>
    <script type='text/javascript' src="js/jquery/jquery-ui/jquery.ui.min.js"></script>

    <link type="text/css" rel="stylesheet" href="css/jquery/jquery-ui/jquery-ui.css" /> 
    <link href="css/popup.css" rel="stylesheet" type="text/css" />
    <script type="text/javascript" src="js/jquery/jquery.jqGrid-4.7.0/js/grid.locale-es.js"></script>
    <script type="text/javascript" src="js/jquery/jquery.jqGrid-4.7.0/js/jquery.jqGrid.min.js"></script>
    <script type="text/javascript" src="js/jquery/jquery.jqGrid-4.7.0/js/jquery.tabletojson.js"></script>
      
   <script type='text/javascript' src="../fintra/js/AsigPerfilesRiesgos.js"></script>
   </head>
    <body>
        
     <!--Encabezado (Inicio)-->
     <div id="capaSuperior" style="position:absolute; width:100%; z-index:0; left: 0px; top: 0px;">
         <jsp:include page="/WEB-INF/toptsp.jsp?encabezado=VER DATOS DEL NEGOCIO"/>
     </div>
     <!--Encabezado (Fin)-->
     
     
    <!--Cuerpo (Inicio)-->
    <div id="capaCentral" style="position: absolute; width: 100%; height: 557px; z-index: 0; left: 0px; top: 100px; overflow: scroll;">        

        <p>&nbsp;</p>
        <!--     <form id="FormCrearPerfil" name="FormCrearPerfil" action="./controller?estado=Perfiles&accion=Riesgos&carpeta=/jsp/perfilesRiesgos&pagina=CrearPrfilesRiesgos.jsp&opcion=3" autocomplete="on" method="post">                          
     -->
     <form id="FormCrearPerfil" name="FormCrearPerfil" action="/fintra/jsp/perfilesRiesgos_Asignacion/AsignarPerfilAnalistas.jsp" autocomplete="on" method="post">                          
      <center> 
    <div id="info"  class="ventana" >
      <p id="notific" ></p>
    </div>
         <p class="login button"> 
            <input type="button" value="Salir" onclick="window.close();" class="ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only" /> 
            <input type="button" value="Refrescar" onclick="location.reload();" class="ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only" />
            <input id="btnCrearPerfil" name="btnNuevaAsignacion" type="submit" value="Asignar Perfil"  class="ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only" /> 

         </p>

    </form>

         <table id="TabPerfRies"  border="2" align="center" width="100%"></table>
         <div id="CargarTablaPerfiles"></div>
      </center>

 </div>
   <!--Cuerpo (Fin)-->

    </body>
</html>
    <script>
        ListarAnalistas("");
    </script>