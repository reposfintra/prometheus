<%-- 
    Document   : maestroEds
    Created on : 28/05/2015, 05:49:32 PM
    Author     : mariana
--%>

<%@page contentType="text/html" pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
        <link href="./css/popup.css" rel="stylesheet" type="text/css">
        <link type="text/css" rel="stylesheet" href="./css/jquery/jquery-ui/jquery-ui.css" />
        <link type="text/css" rel="stylesheet" href="./css/TransportadorasApi.css " />
        <script type="text/javascript" src="./js/jquery/jquery.jqGrid/js/jquery.min.js"></script>
        <script type="text/javascript" src="./js/jquery-ui-1.8.5.custom.min.js"></script>   
        <script type="text/javascript" src="./js/EDS.js"></script> 
        <script type="text/javascript" href="http://www.ddginc-usa.com/spanish/editcheck.js"></script>
        <title>MAESTRO EDS</title>
    </head>
    <body>
        <div id="capaSuperior" style="position:absolute; width:100%; height:100px; z-index:0; left: 0px; top: 0px;">
            <jsp:include page="/WEB-INF/toptsp.jsp?encabezado=EDS "/>
        </div>
    <center>
        <!--        style="height: 350px;width: 505px;"-->
        <div id="tablita" style="width: 505px;" >
            <div id="encabezadotablita">
                <label class="titulotablita"><b>Administracion servicios EDS</b></label>
            </div> 
            <table id="tablainterna"  >
                <tr>
                    <td>
                        <label>Propietarios</label>
                    </td>
                    <td>
                        <select id="idpropietario" style="width: 218px" onchange="cargarComboEds()" ></select>
                    </td>
                </tr>
                <tr>
                    <td>
                        <label>Estaciones de Servicio</label>
                    </td>
                    <td>
                        <select id="estaciones" style="width: 218px" onchange="cargarProductos()"></select>
                    </td>


                </tr>
            </table>

            <div id="servicios" style="margin-top: 43px">
                <fieldset class="cajita"  >
                    <legend class="tituloCajita">Servicios EDS</legend>
                    <div id="opciones">

                    </div>
                </fieldset>

            </div>
            <div id ='probotones' style="text-align:right">
             <button id="guardarprod" class="ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only" 
                     role="button" aria-disabled="false">
                 <span class="ui-button-text">Guardar</span>
             </button> 

             <button id="salirprod" class="ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only" 
                     role="button" aria-disabled="false" onclick="window.close()">
                 <span class="ui-button-text">Salir</span>
             </button> 
         </div>
        </div>


    </center>

</body>
<script>
    init3();
</script>
</html>
