<%-- 
    Document   : registrarMovimeinto
    Created on : 23/02/2012, 05:37:05 PM
    Author     : jpinedo
--%>

<%@page import="com.tsp.operation.model.beans.MovimientosCaptaciones"%>
<%@page import="java.util.List"%>
<%@page import="java.util.TreeMap"%>
<%@page import="com.tsp.operation.model.beans.Inversionista"%>
<%@page import="java.util.ArrayList"%>
<%@page import="com.tsp.operation.model.services.CaptacionInversionistaService"%>
<%@page import="com.tsp.operation.model.beans.NitSot"%>
<%@page import="com.tsp.operation.model.beans.Usuario"%>
<%@page import="com.tsp.util.*"%>
<%@taglib uri="http://jakarta.apache.org/taglibs/input-0.90" prefix="input" %>
<%@page contentType="text/html" pageEncoding="ISO-8859-1"%>
<%@include file="/WEB-INF/InitModel.jsp"%>
<%
CaptacionInversionistaService cis = new CaptacionInversionistaService();
Usuario usuario = (Usuario)session.getAttribute("Usuario");
String age = (usuario.getId_agencia().equals("OP")?"":usuario.getId_agencia());
model.servicioBanco.loadBancos(age, usuario.getCia());
TreeMap b = model.servicioBanco.getBanco();
String c_banco="BANCOLOMBIA";
String no_transaccion = (request.getParameter("no_transaccion") != null) ? request.getParameter("no_transaccion") : "";
String opcion = (request.getParameter("opcion") != null) ? request.getParameter("opcion") : "";
 ArrayList<MovimientosCaptaciones> lista_movimientos = new ArrayList<MovimientosCaptaciones>() ;
MovimientosCaptaciones retiro = new MovimientosCaptaciones() ;
if ( ((List)session.getAttribute("lista_retiros_x_confirmar") != null)&& opcion.equals("confirmar")) {
  lista_movimientos= ( ArrayList<MovimientosCaptaciones>)session.getAttribute("lista_retiros_x_confirmar");
retiro = cis.getRetiroSession(lista_movimientos, no_transaccion);
}
if ( ((List)session.getAttribute("lista_retiros_x_aprobar") != null)&& opcion.equals("aprobar")) {
  lista_movimientos= ( ArrayList<MovimientosCaptaciones>)session.getAttribute("lista_retiros_x_aprobar");
retiro = cis.getRetiroSession(lista_movimientos, no_transaccion);
}



%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1"/>
        <title>Crear inversionista</title>
        <link href="<%= BASEURL%>/css/estilostsp.css" rel='stylesheet'/>
        <script type='text/javascript' src="<%= BASEURL%>/js/captaciones.js"></script>

    </head>
    <script>
    $(function() {
		//$( "#fecha" ).datepicker({dateFormat: "yy-mm-dd" });
                
$('#fecha').datepicker({
						minDate: new Date(2000, 0, 1),
						maxDate: new Date(),
						dateFormat: 'yy-mm-dd',
						constrainInput: true						
					});

		

	});

    </script>
    
    
            <style type="text/css">
            div.ui-dialog{
                font-size:12px;
            }
			
			  div.ui-datepicker{
                font-size:12px;
            }
        </style>   
    <body>
    <div  id="contenedor">


  <%if(retiro.getTipo_transferencia().equals("Electronica")){	%>
  
 <table align="center" style="border: green solid 1px;" width="500" id="tabla_datos2" >
   <tr class="fila">
     <th width="50%" align="left"  ><strong> Datos Transferencia </strong></th>
     <th width="50%" align="left" > <img alt="" src="<%= BASEURL%>/images/titulo.gif" width="32" height="20" align="left"/> </th>
   </tr>
   <tr class="fila">
     <td >Nombre titular de la cuenta </td>
     <td width="50%"><%=retiro.getTitular_cuenta()%></td>
   </tr>
   <tr class="fila">
     <td>Cedula</td>
     <td><%=retiro.getNit_cuenta()%></td>
   </tr>
   <tr class="fila">
     <td>Banco</td>
     <td><%=retiro.getBanco()%></td>
   </tr>
   <tr class="fila">
     <td>N&deg; de cuenta</td>
     <td><%=retiro.getCuenta()%></td>
   </tr>
   <tr class="fila">
     <td>Tipo de Cuenta</td>
     <td><%=retiro.getTipo_cuenta()%></td>
     
   </tr>
 </table>
 <%}
	 
if(retiro.getTipo_transferencia().equals("Cheque")){
/* retiro transferencia cheque */ %>
 <table align="center" style="border: green solid 1px;" width="500" id="tabla_datos2" >
   <tr>
     <th width="50%" align="left"  ><strong> Datos Cheque</strong></th>
     <th width="50%" align="left" > <img alt="" src="<%= BASEURL%>/images/titulo.gif" width="32" height="20" align="left"/> </th>
   </tr>
   <tr class="fila">
     <td >Nombre beneficiario</td>
     <td width="50%"><%=retiro.getNombre_beneficiario()%></td>
   </tr>
   <tr class="fila">
     <td>Nit / Cedula</td>
     <td><%=retiro.getNit_beneficiario()%></td>
   </tr>
   <tr class="fila">
     <td>Cruzado</td>
     <td><%=retiro.getCheque_cruzado()%></td>
   </tr>
   <tr class="fila">
     <td>Primer beneficiario</td>
     <td><%=retiro.getCheque_primer_beneficiario() %></td>
   </tr>
   </table>
 <%} 
	if(retiro.getTipo_transferencia().equals("Interna")){
/* retiro transferencia interna */ %>
 <table align="center" style="border: green solid 1px;" width="500" id="tabla_datos2" >
   <tr>
     <th width="50%" align="left"  ><strong> Datos Cuenta Interna</strong></th>
     <th width="50%" align="left" > <img alt="" src="<%= BASEURL%>/images/titulo.gif" width="32" height="20" align="left"/> </th>
   </tr>
   <tr class="fila">
     <td >Nombre beneficiario captacion</td>
     <td width="50%"><%=retiro.getNombre_beneficiario_captacion()%></td>
   </tr>
 </table>
 
<%}
   %>
  <br/>
                <div align="center">
                    <img style="cursor:pointer" src="<%= BASEURL%>/images/botones/salir.gif" onMouseOver="botonOver(this);" onMouseOut="botonOut(this); " 
                    onClick="$('#divSalida').dialog('close');"/>
                </div>
       
                    <div id="divMensaje">
                       
                    </div>
                    
    </div>
        
    </body>
</html>
