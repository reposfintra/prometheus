<%-- 
    Document   : aprobar retiros
    Created on : 18/05/2012, 05:37:23 PM
    Author     : jpinedo
--%>

<%@page import="java.util.Vector"%>
<%@page import="com.tsp.operation.model.services.CaptacionInversionistaService"%>
<%@page import="com.tsp.util.Util"%>
<%@page import="com.tsp.util.UtilFinanzas"%>
<%@page contentType="text/html" pageEncoding="ISO-8859-1"%>
<%@include file    ="/WEB-INF/InitModel.jsp"%>
<%@page import="java.util.ArrayList"%>
<%@page import="com.tsp.operation.model.beans.*"%>
<%
Usuario usuario = (Usuario)session.getAttribute("Usuario");
CaptacionInversionistaService cis = new CaptacionInversionistaService(usuario.getBd());

String mensaje = request.getParameter("mensaje");


Vector vb=new Vector();
model.Negociossvc.listb("todos");
vb=model.Negociossvc.getListNeg();



%>


<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1"/>
        <title>Transferir Retiros</title>
        <link href="<%= BASEURL%>/css/estilostsp.css" rel='stylesheet'/>
        <link href="<%= BASEURL%>/css/letras.css" rel='stylesheet'/>
        <link href="<%=BASEURL%>/css/default.css"       rel="stylesheet" type="text/css"/>
        <script type='text/javascript' src="<%= BASEURL%>/js/boton.js"></script>
     
        <script type='text/javascript' src="<%= BASEURL%>/js/captaciones.js"></script>
        <script type='text/javascript' src="<%=BASEURL%>/js/validar.js"></script>
          <link href="<%= BASEURL%>/css/estilosFintra.css" rel='stylesheet'/>

        
          <link href="<%= BASEURL%>/css/estilosFintra.css" rel='stylesheet'/>
        
         <link href="<%= BASEURL%>/css/estilostsp.css" rel="stylesheet" type="text/css"> 
        <link href="<%= BASEURL%>/css/jquery/jquery-ui/jquery-ui.css" rel="stylesheet" type="text/css"/>
        <link href="<%= BASEURL%>/css/jquery/jquery.jqGrid/ui.jqgrid.css" rel="stylesheet" type="text/css"/>
        <script type='text/javascript' src="<%= BASEURL%>/js/drypers/CiudadDrypers.js"></script>
        <script type='text/javascript' src="<%= BASEURL%>/js/boton.js"></script>
        <script type='text/javascript' src="<%= BASEURL%>/js/jquery/jquery-ui/jquery.min.js"></script>
        <script type='text/javascript' src="<%= BASEURL%>/js/jquery/jquery-ui/jquery.ui.min.js"></script>
        <script type='text/javascript' src="<%= BASEURL%>/js/jquery/jquery.jqGrid/js/grid.locale-es.js"></script>
        <script type='text/javascript' src="<%= BASEURL%>/js/jquery/jquery.jqGrid/js/jquery.jqGrid.min.js"></script>
   
                
        
        
        
        
        <script>
         <!-- cargar movimientos pendientes por confirmar -->
          var unchecked = null;
                $(document).ready(
				
                function() {
                    $("#tabla_datos").styleTable();
	            $("#totales").styleTable();
                    jQuery("#Retiros").jqGrid({
                        caption: 'Aprobacion De Retiros' ,
                        url: "<%=CONTROLLER%>?estado=Captacion&accion=Inversionista&op=13",
                        datatype: "json",
                        height: "auto",
                        width: 1600,
                     colNames:['Transaccion','Nombre','Fecha','Valor Retiro','','Tipo Trasferencia','Banco','Tipo Cuenta','Titular Cuenta','Nit Cuenta',
                         'N� Cuenta','Nit Beneficiario','Nombre Beneficiario','Cheque Cruzado','Primer Beneficiario' ,'Benef.Captacion'],
                       colModel:[
                            {name:'no_transaccion',index:'no_transaccion', sortable:false, align:'center',key:true,width: 90},                          			    
                            {name:'nombre_inversionista',index:'nombre_inversionista', sortable:false,align:'right',width: 200 },
			    {name:'fecha',index:'fecha', sortable:false,align:'center',formatter: 'date' ,width: 85},
                            {name:'retiro',index:'retiro', sortable:false, align:'right',
		            formatter:'currency', formatoptions:{decimalSeparator:",", thousandsSeparator: ",", decimalPlaces: 2, prefix: "$ "}} ,
                             {name:'retiro',index:'retiro', sortable:false, hidden:true},
                            {name:'tipo_transferencia',index:'tipo_transferencia', sortable:false,align:'center'},
                            {name:'banco',index:'banco', sortable:false ,align:'center'},
                            {name:'tipo_cuenta',index:'tipo_cuenta', sortable:false ,align:'center' ,width:70},
                            {name:'titular_cuenta',index:'titular_cuenta', sortable:false ,align:'center'},
                            {name:'nit_cuenta',index:'nit_cuenta', sortable:false ,align:'center' ,width:75},
                            {name:'cuenta',index:'cuenta', sortable:false ,align:'center' },
                            {name:'nit_beneficiario',index:'nit_beneficiario', sortable:false ,align:'center'},
                            {name:'nombre_beneficiario',index:'nombre_beneficiario', sortable:false ,align:'center'},
                            {name:'cheque_cruzado',index:'cheque_cruzado', sortable:false ,align:'center'},
                            {name:'cheque_primer_beneficiario',index:'cheque_primer_beneficiario', sortable:false ,align:'center'},
                            {name:'nombre_beneficiario_captacion',index:'nombre_beneficiario_captacion', sortable:false ,align:'center'},
                    ],
                        rowNum:-1,
                        rowTotal: 500000,
                        loadonce:true,
                        rownumWidth: 40,
                        gridview: true,
                        viewrecords: true,
                        hidegrid: false,           
                        multiselect: true,
                        jsonReader : {
                            root:"rows",
                            repeatitems: false,
                            id: "0"
                        },

                        gridComplete:
                function(){
                    var total = 0;
                    var ids = jQuery("#Retiros").jqGrid('getDataIDs');
                    for(var i=0;i < ids.length;i++){                      
                        var ret = jQuery("#Retiros").jqGrid('getRowData',ids[i]);
                        total += parseFloat(ret.retiro);
                    }
                    document.getElementById("total").innerHTML = formatear(total);
                } ,
                loadError: function(xhr, status, error){
                            alert(error);
                        }
                    });
                });

   


						
(function ($) {
    $.fn.styleTable = function (options) {
        var defaults = {
            css: 'styleTable'
        };
        options = $.extend(defaults, options);

        return this.each(function () {

            input = $(this);
            input.addClass(options.css);

                       
            input.find("th").addClass("ui-state-default");
            input.find("td").addClass("ui-widget-content");

            input.find("tr").each(function () {
                $(this).children("td:not(:first)").addClass("first");
                $(this).children("th:not(:first)").addClass("first");
            });
        });
    };
})(jQuery);		


				

 $(document).ready(function(){
                $("#divSalida").dialog({
                    autoOpen: false,
                    height: "auto",
                    width: "35%",
                    modal: true,
                    autoResize: true,
                    resizable: false,
                    position: "center",
                    closeOnEscape: false


                });
              
            });

				
</script>
        
        <style type="text/css">
            div.ui-dialog{
                font-size:12px;            }

            .no-close.ui-dialog-titlebar-close {display: none }
        </style>   
      
        
    </head>
    <body>
        <div id="capaSuperior" style="position:absolute; width:100%; height:100px; z-index:0; left: 0px; top: 0px;">
            <jsp:include page="/WEB-INF/toptsp.jsp?encabezado=Movimientos y Transacciones"/>
        </div>
        <div id="capaCentral" style="position:absolute; width:100%; height:85%; z-index:0; left: 4px; top: 97px; overflow: auto;">
            <form id="form1" name="form1" method="post" >
            <input name="BASEURL" type="hidden" id="BASEURL" value="<%=BASEURL%>">
            
            <input name="CONTROLLER" type="hidden" id="CONTROLLER" value="<%=CONTROLLER%>">
            
         
<div id="contenido" align="center" style=" width: 600px; height: 300px; visibility: hidden; background-color:#F7F5F4; display: none"></div>
<br/>
<table  width="30%" align="center" id="tabla_datos"  class="tableTextUI" >
  <tr >
    <th width="136"><b>Banco girador</b></th>
    <td width="418">
        <select name="banco" id="banco" >
      <option value="...">Seleccione</option>
      <%for(int j=0;j<vb.size();j++){
						Negocios  banco = (Negocios)vb.get(j);%>
      <option value="<%=banco.getNom_cli()%>,<%=banco.getCod_cli()%>,<%=banco.getCod_tabla()%>"><%=banco.getEstado()%></option>
      <%if(banco.getNom_cli().equals("BANCOLOMBIA")){%>
       <option value="BANCOLOMBIAPAB,<%=banco.getCod_cli()%>,<%=banco.getCod_tabla()%>">BANCOLOMBIA PAB <%=banco.getCod_cli()%> <%=banco.getCod_tabla()%></option>
       <%}%>

      <%}%>
      </select></td>
  </tr>
</table>
<br/>
<table border="0"  align="center">
<tr>
                    <td>
                     <table id="Retiros" class="center_div" ></table>
                    </td>
                    </tr>            
                  
              </table>
  <br/>
                 <table width="15%" align="left"  id="totales" style="margin-left:2%; font-size:63%""  class="center_div">
                     <tr><th width="93"><b>Total</b></th>
                       <td width="178"><b>$<span id="total"></span>

                       </b></td>
                     </tr>
                     </table>
                  <br/><p><br/>
                  
                
                </p>
                <div align="center">
        
                    
                        <table align="center">
                          <tr >
                            <td><img src="<%=BASEURL%>/images/botones/aceptar.gif"   name="imgaceptar" height="21" align="absmiddle" id="imgaceptar" style="cursor:pointer; display: block" onClick="Transferir()" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);"></td>
                            <td><img src="<%=BASEURL%>/images/botones/restablecer.gif"   name="imgrestablecer" height="21" align="absmiddle" id="imgrestablecer" style="cursor:pointer; display: block" onClick="refreshGridCiuTsp('<%=CONTROLLER%>');" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);"></td>
                            <td><img src="<%=BASEURL%>/images/botones/salir.gif"    name="imgsalir" height="21" id="imgsalir2" style="cursor:pointer" onClick="parent.close();"  onMouseOver="botonOver(this);" onMouseOut="botonOut(this);"/></td>
                          </tr>
                        </table>
              </div>
                
                
                             <div id="divSalida" title="Confirmacion de retiros" style=" display: block" >
        <div id="resp"></div>
         </div>  
                
                <br/>
            <br/>
                <br/>
                <br/>
                <br/>
                <br/>
                <br/>
                <br/>
                
                
                
                
                   

            <br>
            </form>

         
         
                  
        </div>
        
       
    </body>
</html>

