<%--
    Document   : informe mensual 
    Created on : 28/05/2012, 05:37:23 PM
    Author     : jpinedo
--%>

<%@page import="java.util.Vector"%>
<%@page import="com.tsp.operation.model.services.CaptacionInversionistaService"%>
<%@page import="com.tsp.util.Util"%>
<%@page import="com.tsp.util.UtilFinanzas"%>
<%@page contentType="text/html" pageEncoding="ISO-8859-1"%>
<%@include file    ="/WEB-INF/InitModel.jsp"%>
<%@page import="java.util.ArrayList"%>
<%@page import="com.tsp.operation.model.beans.*"%>
<%
Usuario usuario = (Usuario)session.getAttribute("Usuario");
BeanGeneral info =model.captacionInversionistaService.clasificacionInversionistas(usuario.getDstrct());
int ano=0;
int mes=0;
String fecha = Util.getFechahoy();
String m="";
if (fecha!=null) {
                    ano = Integer.parseInt(fecha.substring(0, 4));
                    mes = Integer.parseInt(fecha.substring(5, 7));
                    mes = mes -1;
                    if(mes<10)
                    m="0"+mes;
                    else
                    m=""+mes;
                   
}
%>


<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1"/>
        <title>Informe Mensual</title>
        <link href="<%= BASEURL%>/css/estilostsp.css" rel='stylesheet'/>
        <link href="<%= BASEURL%>/css/letras.css" rel='stylesheet'/>
        <link href="<%=BASEURL%>/css/default.css"       rel="stylesheet" type="text/css"/>
          <link href="<%= BASEURL%>/css/estilosFintra.css" rel='stylesheet'/>
        <script type='text/javascript' src="<%= BASEURL%>/js/boton.js"></script>

        <script type='text/javascript' src="<%= BASEURL%>/js/captaciones.js"></script>
        <script type='text/javascript' src="<%=BASEURL%>/js/validar.js"></script>




         <link href="<%= BASEURL%>/css/estilostsp.css" rel="stylesheet" type="text/css">
        <link href="<%= BASEURL%>/css/jquery/jquery-ui/jquery-ui.css" rel="stylesheet" type="text/css"/>
        <link href="<%= BASEURL%>/css/jquery/jquery.jqGrid/ui.jqgrid.css" rel="stylesheet" type="text/css"/>
        <script type='text/javascript' src="<%= BASEURL%>/js/drypers/CiudadDrypers.js"></script>
        <script type='text/javascript' src="<%= BASEURL%>/js/boton.js"></script>
        <script type='text/javascript' src="<%= BASEURL%>/js/jquery/jquery-ui/jquery.min.js"></script>
        <script type='text/javascript' src="<%= BASEURL%>/js/jquery/jquery-ui/jquery.ui.min.js"></script>
        <script type='text/javascript' src="<%= BASEURL%>/js/jquery/jquery.jqGrid/js/grid.locale-es.js"></script>
        <script type='text/javascript' src="<%= BASEURL%>/js/jquery/jquery.jqGrid/js/jquery.jqGrid.min.js"></script>






        <script>
       $(document).ready(

                function() {
	      
       
                    cargarMovimientos();
                 
	            $("#totales").styleTable();
				$("#datos").styleTable();

	});

function cargarMovimientos()
{
	
                    jQuery("#inversionistas").jqGrid({
                        caption: 'Reporte Consolidado' ,
                        url: "<%=CONTROLLER%>?estado=Captacion&accion=Inversionista&op=28",
                        datatype: "json",
                        height: 'auto',
                        width: 900,
                     colNames:['Nit','Inversionista','Saldo','Tipo Inversionista','Computa'],
                       colModel:[
                           
                            {name:'nit',index:'nit', sortable:true,align:'center' ,key:true },
                             {name:'nombre_subcuenta',index:'nombre_subcuenta', sortable:true,align:'center',width:'400px' },
                               {name:'saldo_total',index:'saldo_total', sortable:false, align:'right',
		            formatter:'currency', formatoptions:{decimalSeparator:",", thousandsSeparator: ".", decimalPlaces: 2, prefix: "$ "}},

					{name:'tipo_inversionista',index:'tipo_inversionista', sortable:true, width:'125px', align:'center' },                           
                            {name:'computa',index:'computa', sortable:true,  align:'center',width:'80px' },

                        ],
                        rowNum:1000,
                        rowTotal: 500000,
                        loadonce:true,
                        rownumWidth: 40,
                        gridview: true,
                        viewrecords: true,
                        hidegrid: false,
                        jsonReader : {
                            root:"rows",
                            repeatitems: false,
                            id: "0"
                        },
			
                        loadError: function(xhr, status, error){
                            alert(error);
                        }
                    });
}

(function ($) {
    $.fn.styleTable = function (options) {
        var defaults = {
            css: 'styleTable'
        };
        options = $.extend(defaults, options);

        return this.each(function () {

            input = $(this);
            input.addClass(options.css);


            input.find("th").addClass("ui-state-default");
            input.find("td").addClass("ui-widget-content");

            input.find("tr").each(function () {
                $(this).children("td:not(:first)").addClass("first");
                $(this).children("th:not(:first)").addClass("first");
            });
        });
    };
})(jQuery);




 $(document).ready(function(){
                $("#divSalida").dialog({
                    autoOpen: false,
                    height: "auto",
                    width: "35%",
                    modal: true,
                    autoResize: true,
                    resizable: false,
                    position: "center",
                    closeOnEscape: false


                });

            });


</script>

        <style type="text/css">
            div.ui-dialog{
                font-size:12px;            }

            .no-close.ui-dialog-titlebar-close {display: none }
        </style>


    </head>
    <body>
        <div id="capaSuperior" style="position:absolute; width:100%; height:100px; z-index:0; left: 0px; top: 0px;">
            <jsp:include page="/WEB-INF/toptsp.jsp?encabezado=Movimientos y Transacciones"/>
        </div>
        <div id="capaCentral" style="position:absolute; width:100%; height:85%; z-index:0; left: 4px; top: 97px; overflow: auto;">
            <form id="form1" name="form1" method="post" >
            <input name="BASEURL" type="hidden" id="BASEURL" value="<%=BASEURL%>">

            <input name="CONTROLLER" type="hidden" id="CONTROLLER" value="<%=CONTROLLER%>">


<div id="contenido" align="center" style=" width: 600px; height: 300px; visibility: hidden; background-color:#F7F5F4; display: none"></div>

<br/>
<br/>
<table border="0"  align="center">
    <tr>
        <td>
            <table id="inversionistas" class="center_div" ></table>
        </td>
    </tr>

</table><br/>
<div  class="center_div">
    <table width="300" align="center"  id="datos" class="tableTextUI">
        <tr>
            <td width="252"  align="center">Socios</td>
            <td align="center"><%=info.getValor_03()%></td>
        </tr>

        <tr>
            <td align="center">Particulares</td>
            <td align="center"><%=info.getValor_04()%></td>
        </tr>

        <tr>
            <td align="center">Familiar Socio</td>
            <td align="center"><%=info.getValor_06()%></td>
        </tr>

        <tr>
            <th align="center"  >
                <b>Total Inversionistas</b></th>
            <th width="36" align="center"><b><%=info.getValor_05()%></b></th>
        </tr>
    </table>
   
    <br/>

</div>
<br/>
<br/><br/>


 
                
<div  id="imagen" align="center" style="text-align:center">

</div>
                <div align="center">


                        <table align="center">
                          <tr >
                            <td><img src="<%=BASEURL%>/images/botones/generarPdf.gif"   name="imgapdf" height="21" align="absmiddle" id="imgapdf" style="cursor:pointer; display: block" onClick="ExportarInformeConsolidado('pdf')" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);"></td>
                           
                           <td><img src="<%=BASEURL%>/images/botones/exportarExcel.gif"   name="imgexcel" height="21" align="absmiddle" id="imgexcel" style="cursor:pointer; display: block" onClick="ExportarInformeConsolidado('xls')" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);"></td>
                            
                            <td> <img style="cursor:pointer" src="<%= BASEURL%>/images/botones/salir.gif" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" onClick="parent.close();"/></td>
                          </tr>
                        </table>
              </div>


                             <div id="divSalida" title="Exportacion" style=" display: block" >
        <div id="resp"></div>
         </div>

                <br/>
            <br/>
                <br/>
                <br/>
                <br/>
                <br/>
                <br/>
                <br/>






            <br>
            </form>




        </div>


    </body>
</html>


