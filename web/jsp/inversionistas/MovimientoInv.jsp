<%-- 
    Document   : consultaCreditoBancario
    Created on : 10/01/2012, 05:37:23 PM
    Author     : darrieta
--%>

<%@page import="java.util.LinkedList"%>
<%@page import="com.tsp.operation.model.services.Tipo_impuestoService"%>
<%@page import="com.tsp.operation.model.services.CaptacionInversionistaService"%>
<%@page import="com.tsp.util.Util"%>
<%@page import="com.tsp.util.UtilFinanzas"%>
<%@page contentType="text/html" pageEncoding="ISO-8859-1"%>
<%@include file    ="/WEB-INF/InitModel.jsp"%>
<%@page import="java.util.ArrayList"%>
<%@page import="com.tsp.operation.model.beans.*"%>
<%
Usuario usuario = (Usuario)session.getAttribute("Usuario");
CaptacionInversionistaService cis = new CaptacionInversionistaService(usuario.getBd());


String nit = request.getParameter("nit");
int  codsubcuenta = Integer.parseInt(request.getParameter("subcuenta"));
Inversionista subcuenta = cis.consultarSubcuentaInversionista(usuario.getDstrct(),nit, codsubcuenta);

String fechaCau = cis.obtenerUltimaFechaCausacion(usuario.getDstrct(), nit, codsubcuenta);

model.tablaGenService.buscarRegistrosNoAnulados("CAP_RENDIM");
LinkedList<TablaGen> rendimientos = model.tablaGenService.getTablas();

String mensaje = request.getParameter("mensaje");
model.tablaGenService.buscarDatos("CAP_INTERE", String.valueOf(subcuenta.getTipo_interes()));
TablaGen tintereses = model.tablaGenService.getTblgen();

model.tablaGenService.setTblgen(null);
TablaGen trendimiento = new TablaGen();
model.tablaGenService.buscarDatos("CAP_RENDIM", String.valueOf(subcuenta.getRendimiento()));
trendimiento =   model.tablaGenService.getTblgen();


  Tipo_impuestoService TimpuestoSvc = new Tipo_impuestoService(usuario.getBd());
  Tipo_impuesto impuestos_rfte =TimpuestoSvc.buscarImpuestoxCodigo( usuario.getDstrct(), subcuenta.getRetefuente());
  Tipo_impuesto impuestos_rica =TimpuestoSvc.buscarImpuestoxCodigo( usuario.getDstrct(), subcuenta.getReteica());

%>
<!DOCTYPE html>
<html>

    <html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1"/>
        <title>Movimientos y Transacciones</title>
        <link href="<%= BASEURL%>/css/estilostsp.css" rel='stylesheet'/>
        <link href="<%= BASEURL%>/css/letras.css" rel='stylesheet'/>
        <link href="<%=BASEURL%>/css/default.css"       rel="stylesheet" type="text/css"/>
        <script type='text/javascript' src="<%= BASEURL%>/js/boton.js"></script>

        <script type='text/javascript' src="<%= BASEURL%>/js/captaciones.js"></script>
        <script type='text/javascript' src="<%=BASEURL%>/js/validar.js"></script>
         <link href="<%= BASEURL%>/css/estilosFintra.css" rel='stylesheet'/>





         <link href="<%= BASEURL%>/css/estilostsp.css" rel="stylesheet" type="text/css">
        <link href="<%= BASEURL%>/css/jquery/jquery-ui/jquery-ui.css" rel="stylesheet" type="text/css"/>
        <link href="<%= BASEURL%>/css/jquery/jquery.jqGrid/ui.jqgrid.css" rel="stylesheet" type="text/css"/>

        <script type='text/javascript' src="<%= BASEURL%>/js/boton.js"></script>
        <script type='text/javascript' src="<%= BASEURL%>/js/jquery/jquery-ui/jquery.min.js"></script>
        <script type='text/javascript' src="<%= BASEURL%>/js/jquery/jquery-ui/jquery.ui.min.js"></script>
        <script type='text/javascript' src="<%= BASEURL%>/js/jquery/jquery.jqGrid/js/grid.locale-es.js"></script>
        <script type='text/javascript' src="<%= BASEURL%>/js/jquery/jquery.jqGrid/js/jquery.jqGrid.min.js"></script>
                
        
        
      
        
        <script>
         <!-- cargar movimientos iniciales  -->
	
                $(document).ready(
                function() {
                    $("#tabla_datos").styleTable();
                    jQuery("#MovimientosCaptaciones").jqGrid({
                        caption: 'Movimientos   Captaciones' ,
                        url: "<%=CONTROLLER%>?estado=Captacion&accion=Inversionista&op=6&inversionista=<%=nit%>&subcuenta=<%=codsubcuenta%>",
                        datatype: "json",
                        height: 'auto',
                        width: 1200,
                     colNames:['Transaccion','Fecha','Saldo Inicial','Base Intereses','Intereses','Itereses acomulados','Retefuente','Reteica','SubTotal','Consignacion','Retiro','Saldo'],
                       colModel:[
                            {name:'no_transaccion',index:'items', sortable:true, align:'center',key:true,width: 90},
                            {name:'fecha',index:'fecha', sortable:true ,align:'center',
			    formatter: 'date' },
                            {name:'saldo_inicial',index:'saldo_inicial', sortable:true,align:'right' ,hidden:<%=!subcuenta.getTipo_interes().equals("C")%>,
							formatter:'currency', formatoptions:{decimalSeparator:",", thousandsSeparator: ",", decimalPlaces: 2, prefix: "$ "}},
                            {name:'base_intereses',index:'base_intereses', sortable:true,align:'right',hidden:<%=!subcuenta.getTipo_interes().equals("S")%>,
							formatter:'currency', formatoptions:{decimalSeparator:",", thousandsSeparator: ",", decimalPlaces: 2, prefix: "$ "}},                          
                            {name:'valor_intereses',index:'valor_intereses', sortable:true, align:'right',
		            formatter:'currency', formatoptions:{decimalSeparator:",", thousandsSeparator: ",", decimalPlaces: 2, prefix: "$ "}
							},
                            {name:'intereses_acomulados',index:'intereses_acomulados', sortable:true,align:'right',hidden:<%=!subcuenta.getTipo_interes().equals("S")%>,
			    formatter:'currency', formatoptions:{decimalSeparator:",", thousandsSeparator: ",", decimalPlaces: 2, prefix: "$ "}
							},
                            {name:'valor_retefuente',index:'valor_retefuente', sortable:true,align:'right',
			    formatter:'currency', formatoptions:{decimalSeparator:",", thousandsSeparator: ",", decimalPlaces: 2, prefix: "$ "}
							},
                            {name:'valor_reteica',index:'valor_reteica', sortable:true,align:'right',
			    formatter:'currency', formatoptions:{decimalSeparator:",", thousandsSeparator: ",", decimalPlaces: 2, prefix: "$ "}
							},
                            {name:'subtotal',index:'subtotal', sortable:true,align:'right',hidden:<%=!subcuenta.getTipo_interes().equals("C")%>,
			    formatter:'currency', formatoptions:{decimalSeparator:",", thousandsSeparator: ",", decimalPlaces: 2, prefix: "$ "}
							},
                            {name:'consignacion',index:'consignacion', sortable:true,align:'right',
							formatter:'currency', formatoptions:{decimalSeparator:",", thousandsSeparator: ",", decimalPlaces: 2, prefix: "$ "}
			    },
			    {name:'retiro',index:'retiro', sortable:true,align:'right',
							formatter:'currency', formatoptions:{decimalSeparator:",", thousandsSeparator: ",", decimalPlaces: 2, prefix: "$ "}
			    },
			    {name:'saldo_final',index:'saldo_final', sortable:true,align:'right',
			    formatter:'currency', formatoptions:{decimalSeparator:",", thousandsSeparator: ",", decimalPlaces: 2, prefix: "$ "}
							}
                        ],
                        rowNum:-1,
                        rowTotal: 500000,
                        loadonce:true,
                        rownumWidth: 40,
                        gridview: true,
                        viewrecords: true,
                        hidegrid: false,
                        multiselect: true,
                        jsonReader : {
                            root:"rows",
                            repeatitems: false,
                            id: "0"
                        },
                        loadError: function(xhr, status, error){
                            alert(error);
                        }
                    });
                });

   


				
(function ($) {
    $.fn.styleTable = function (options) {
        var defaults = {
            css: 'styleTable'
        };
        options = $.extend(defaults, options);

        return this.each(function () {

            input = $(this);
            input.addClass(options.css);

                       
            input.find("th").addClass("ui-state-default");
            input.find("td").addClass("ui-widget-content");

            input.find("tr").each(function () {
                $(this).children("td:not(:first)").addClass("first");
                $(this).children("th:not(:first)").addClass("first");
            });
        });
    };
})(jQuery);

				

 $(document).ready(function(){
                $("#divCrearMovimiento").dialog({
                    autoOpen: false,
                    height: "auto",
                    width: 'auto',
                    modal: true,
                    autoResize: true,
                    resizable: false,
                    position: "center"
                });
            });


 $(document).ready(function(){
                $("#VentanaCuentas").dialog({
                    autoOpen: false,
                    height: "auto",
                    width: '40%',
                    modal: true,
                    autoResize: true,
                    resizable: false,
                    position: "center"
                });
            });
 
 
 $(document).ready(function(){
                $("#divSalida").dialog({
                    autoOpen: false,
                    height: "auto",
                    width: "400",
                    modal: true,
                    autoResize: true,
                    resizable: false,
                    position: "center",
                    closeOnEscape: false


                });
              
            });
	          

 
				
</script>
        
        <style type="text/css">
            div.ui-dialog{
                font-size:12px;
            }
					  div.ui-datepicker{
                font-size:12px;
            }
        </style>   
      
        
    </head>
    <body>
        <div id="capaSuperior" style="position:absolute; width:100%; height:100px; z-index:0; left: 0px; top: 0px;">
            <jsp:include page="/WEB-INF/toptsp.jsp?encabezado=Movimientos y Transacciones"/>
        </div>
        <div id="capaCentral" style="position:absolute; width:100%; height:85%; z-index:0; left: 0px; top: 100px; overflow: auto;">
            <form id="form1" name="form1" method="post" action="<%=CONTROLLER%>?estado=Credito&accion=Bancario&opcion=GUARDAR_DETALLE_CREDITO">
            <input name="BASEURL" type="hidden" id="BASEURL" value="<%=BASEURL%>">
            
            <input name="CONTROLLER" type="hidden" id="CONTROLLER" value="<%=CONTROLLER%>">
            <input name="nit" type="hidden" id="nit" value="<%=subcuenta.getNit()%>">
            <input name="subcuenta" type="hidden" id="subcuenta" value="<%=subcuenta.getSubcuenta()%>">
<div id="contenido" align="center" style=" width: 600px; height: 300px; visibility: hidden; background-color:#F7F5F4; display: none"  ></div>
<table  width="50%" align="center" id="tabla_datos" class="tableTextUI" >
  <tr>
    <th width="50" align="left" colspan="2" ><strong> Datos Vigentes</strong></th>
    <th width="50" align="left" colspan="2" > <img alt="" src="<%= BASEURL%>/images/titulo.gif" width="32" height="20" align="left"/> </th>
  </tr>
  <tr >
    <th>Inversionista</th>
    <td><%= subcuenta.getNombre_nit() %></td>
    <th>Nit</th>
    <td><%= subcuenta.getNit()%></td>
  </tr>
  <tr >
    <th>Subcuenta</th>
    <td><%= subcuenta.getNombre_subcuenta()%></td>
    <th>Periocidad</th>
    <td><%= trendimiento!= null ? trendimiento.getDescripcion() :subcuenta.getRendimiento() %></td>
  </tr>
  <tr >
    <th>A&ntilde;o Base</th>
    <td><%= subcuenta.getAno_base()%></td>
    <th>Intereses</th>
    <td><%= tintereses.getDescripcion()%></td>
  </tr>
  <tr >
    <th>Tasa</th>
    <td><%= subcuenta.getTasa()%>%</td>
    <th>Tasa Diaria</th>
    <td><%= Util.FormatoDecimal(subcuenta.getTasa_diaria(),"#0.0000") %>%</td>
  </tr>
  <tr >
    <th>Tasa Nominal</th>
    <td><%=Util.FormatoDecimal(subcuenta.getTasa_nominal(),"#0.0000")%>%</td>
    <th>Tasa E.A</th>
    <td><%= Util.FormatoDecimal(subcuenta.getTasa_ea(),"#0.0000")%>%</td>
  </tr>
  <tr >
    <th>Retefuente</th>
    <td><%=Util.FormatoDecimal(Math.abs(impuestos_rfte.getPorcentaje1()),"#0.0")%>%</td>
    <th>Reteica</th>
    <td><%= Util.FormatoDecimal(Math.abs(impuestos_rica.getPorcentaje1()),"#0.0")%>%</td>
  </tr>
  <tr >
    <th>Dias Calendario</th>
    <td><%=subcuenta.getDias_calendario()%></td>
    <th>Saldo inicial </th>
 <td>$ <%=Util.FormatearMiles(model.captacionInversionistaService.getSaldoInicialMes(subcuenta),2)%></td>
  </tr>
  
  
    <tr >
    <th>Pagos Automaticos</th>
    <td>
    
      <input name="pagos_automaticos" type="checkbox" id="pagos_automaticos"  onChange="confirmarModificacionPagos()"  value="S"   
       <%=subcuenta.getPago_automatico().equals("S") ?   "checked" : ""%> <%=subcuenta.getPago_automatico().equals("S") ?   "" : "disabled"%> >
</td>
    <th>Periodicidad de Pago</th>
    <td><select id="periodicidad_pago" name="periodicidad_pago" onChange="confirmarModificacionPagos()" <%=subcuenta.getPago_automatico().equals("S") ?   "" : "disabled"%>   >
      <option value="0"  >Seleccione...</option>
      <%for(int i=0; i<rendimientos.size(); i++)

      if(!rendimientos.get(i).getTable_code().equals("0"))
     {%>                          
      <option value="<%=rendimientos.get(i).getTable_code()%>" 
              <%=rendimientos.get(i).getTable_code().equals(subcuenta.getPeriodicidad_pago()) ?   "selected" : ""%> >
	  <%=rendimientos.get(i).getDescripcion()%></option>
      <%}%>
     
    </select></td>
  </tr>
  
</table>
<br/>
                
                <table border="0"  align="center">
                  <tr>
                    <td>
                     <table id="MovimientosCaptaciones" class="center_div" ></table>
                    </td>
                    </tr>
                </table>
                <p><br/>
                  <br/>
                  
                
                </p>
                <div align="center">
                   
                    <img style="cursor:pointer" src="<%= BASEURL%>/images/botones/retirop.png" onClick="registrarRetiro(2,'RP','<%= fechaCau%>')"/>
          <img style="cursor:pointer" src="<%= BASEURL%>/images/botones/retirot.png"  onClick="registrarRetiro(2,'RT')" />
                    <img style="cursor:pointer" src="<%= BASEURL%>/images/botones/regresar.gif" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" onClick="history.back(1)"/>
                 

                    <img alt="" src="<%=BASEURL%>/images/botones/salir.gif" style="cursor:pointer" name="imgsalir" id="imgsalir" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" onclick='parent.close();'/>
              </div>
            
                <br/>
                <%if(mensaje!=null && !mensaje.equals("")){%>
                <div id="divMensaje">
               
                    <table border="2" align="center">
                        <tr>
                            <td>
                                <table width="100%"  border="0" align="center"  bordercolor="#f7f5f4" bgcolor="#ffffff">
                                    <tr>
                                        <td width="229" align="center" class="mensajes"><%=mensaje%></td>
                                        <td width="29" background="<%=BASEURL%>/images/cuadronaranja.JPG">&nbsp;&nbsp;</td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                    </table>
                </div>
               
                <%}%>
                <br/>
                <br/>
                <br/>
                <br/>
                <br/>
                <br/>
                <br/>
                
                
               <div id="VentanaCuentas" title="Listado Cuentas" style=" display: none" ></div>
                
              <div id="divCrearMovimiento" title="Registrar Movimiento" style=" display:none" >            
 
            <br>
                <table align="center">
                <tr>
                    <td><img src="<%=BASEURL%>/images/botones/aceptar.gif"  name="imgaceptar" id="imgaceptar" height="21" align="absmiddle" style="cursor:pointer; display: block" onClick="abrirModificarCiudadTsp('<%=CONTROLLER%>','<%=BASEURL%>');" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);"></td>
                    <td><img src="<%=BASEURL%>/images/botones/restablecer.gif"  name="imgrestablecer" id="imgrestablecer" height="21" align="absmiddle" style="cursor:pointer; display: block" onClick="refreshGridCiuTsp('<%=CONTROLLER%>');" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);"></td>
                    <td><img src="<%=BASEURL%>/images/botones/salir.gif"   name="imgsalir" id="imgsalir" height="21"  onMouseOver="botonOver(this);" onClick="$('#divCrearMovimiento').dialog('close');  " onMouseOut="botonOut(this);" style="cursor:pointer"/></td>
                </tr>
            </table>



        </div>
                
            </form>
        </div>
        
       <div id="divSalida" title="Actualizacion Para Pagos Automaticos" style=" display: none" >
        <div id="resp">Cargarndo..
        </div>
         </div>  
    </body>
</html>

