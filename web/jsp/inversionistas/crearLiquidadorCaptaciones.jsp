<%-- 
    Document   : crearLiquidadorCaptaciones
    Created on : 27/02/2012, 09:24:10 AM
    Author     : darrieta
--%>

<%@page import="com.tsp.operation.model.beans.Tipo_impuesto"%>
<%@page import="com.tsp.util.Util"%>
<%@page import="java.util.Vector"%>
<%@page import="com.tsp.operation.model.beans.Inversionista"%>
<%@page import="java.util.ArrayList"%>
<%@page import="com.tsp.operation.model.services.CaptacionInversionistaService"%>
<%@page import="com.tsp.operation.model.beans.TablaGen"%>
<%@page import="java.util.LinkedList"%>
<%@page import="com.tsp.operation.model.beans.Usuario"%>
<%@page contentType="text/html" pageEncoding="ISO-8859-1"%>
<%@include file="/WEB-INF/InitModel.jsp"%>
<%
Usuario usuario = (Usuario)session.getAttribute("Usuario");
CaptacionInversionistaService svc = new CaptacionInversionistaService(usuario.getBd());
ArrayList<Inversionista> inversionistas = svc.consultarInversionistas(usuario.getDstrct());

model.tablaGenService.buscarRegistrosNoAnulados("CAP_ANOBAS");
LinkedList<TablaGen> anobase = model.tablaGenService.getTablas();
model.tablaGenService.buscarRegistrosNoAnulados("CAP_RENDIM");
LinkedList<TablaGen> rendimientos = model.tablaGenService.getTablas();
model.tablaGenService.buscarRegistrosNoAnulados("CAP_INTERE");
LinkedList<TablaGen> intereses = model.tablaGenService.getTablas();

Vector impuestos_rfte = model.TimpuestoSvc.buscarImpuestoCliente("RFTE", Util.getFechaActual_String(4), usuario.getDstrct(), "OP");
Vector impuestos_rica = model.TimpuestoSvc.buscarImpuestoCliente("RICA", Util.getFechaActual_String(4), usuario.getDstrct(), "OP");
%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1"/>
        <title>liquidador captaciones</title>
        <link href="<%= BASEURL%>/css/estilostsp.css" rel='stylesheet'/>
        <link href="<%= BASEURL%>/css/letras.css" rel='stylesheet'/>
        <link href="<%=BASEURL%>/css/default.css"       rel="stylesheet" type="text/css"/>
        <script type='text/javascript' src="<%= BASEURL%>/js/boton.js"></script>

        <script type='text/javascript' src="<%= BASEURL%>/js/captaciones.js"></script>
        <script type='text/javascript' src="<%=BASEURL%>/js/validar.js"></script>
       <link href="<%= BASEURL%>/css/estilosFintra.css" rel='stylesheet'/>





         <link href="<%= BASEURL%>/css/estilostsp.css" rel="stylesheet" type="text/css">
        <link href="<%= BASEURL%>/css/jquery/jquery-ui/jquery-ui.css" rel="stylesheet" type="text/css"/>
        <link href="<%= BASEURL%>/css/jquery/jquery.jqGrid/ui.jqgrid.css" rel="stylesheet" type="text/css"/>

        <script type='text/javascript' src="<%= BASEURL%>/js/boton.js"></script>
        <script type='text/javascript' src="<%= BASEURL%>/js/jquery/jquery-ui/jquery.min.js"></script>
        <script type='text/javascript' src="<%= BASEURL%>/js/jquery/jquery-ui/jquery.ui.min.js"></script>
        <script type='text/javascript' src="<%= BASEURL%>/js/jquery/jquery.jqGrid/js/grid.locale-es.js"></script>
        <script type='text/javascript' src="<%= BASEURL%>/js/jquery/jquery.jqGrid/js/jquery.jqGrid.min.js"></script>
          
          
          <script type='text/javascript'>
          
                         $(document).ready(
                function() {
                    $("#datos_liquidacion").styleTable();
                    });
					
					
					
				
(function ($) {
    $.fn.styleTable = function (options) {
        var defaults = {
            css: 'styleTable'
        };
        options = $.extend(defaults, options);

        return this.each(function () {

            input = $(this);
            input.addClass(options.css);

                       
            input.find("th").addClass("ui-state-default");
            input.find("td").addClass("ui-widget-content");

            input.find("tr").each(function () {
                $(this).children("td:not(:first)").addClass("first");
                $(this).children("th:not(:first)").addClass("first");
            });
        });
    };
})(jQuery);

				
					
					</script>
    </head>
    <body>
        <div id="capaSuperior" style="position:absolute; width:100%; height:100px; z-index:0; left: 0px; top: 0px;">
            <jsp:include page="/WEB-INF/toptsp.jsp?encabezado=Datos De Liquidacion"/>
        </div>
        <div id="capaCentral" style="position:absolute; width:100%; height:85%; z-index:0; left: 0px; top: 100px; overflow: auto;">
            <form id="form1" name="form1" method="post">
                <input type="hidden" id="CONTROLLER" value="<%=CONTROLLER%>"/>
                <table width="450" align="center"  class="tableTextUI" id="datos_liquidacion" >
                    <tr class="barratitulo">
                        <th width="160" ><b>Datos liquidacion</b> </th>
                        <td colspan="2"><img src="<%=BASEURL%>/images/titulo.gif" width="32" height="20" align="left"/></td>
                    </tr>
                    <tr class="fila">
                        <th>Inversionista</th>
                        <td colspan="2">
                            <select id="inversionista" name="inversionista" onChange="cargarSubcuentas()" >
                                <option value="">Seleccione</option>
                                <%for(int i=0; i<inversionistas.size(); i++){%>
                                    <option value="<%=inversionistas.get(i).getNit()%>" ><%=inversionistas.get(i).getNombre_subcuenta()%></option>
                                <%}%>
                            </select>
                        </td>
                    </tr>
                    <tr class="fila">
                        <th>Subcuenta</th>
                        <td colspan="2">
                            <select id="subcuenta" name="subcuenta" onChange="cargarDatosSubcuenta()" >
                            </select>
                        </td>
                    </tr>
                    <tr class="fila">
                        <th>Tasa</th>
                        <td colspan="2">
                            <input type="text" id="tasa" name="tasa" onKeyUp="soloNumerosformateado(this.id);" onChange="calcularTasas();"/>
                        </td>
                    </tr>
                    <tr class="fila">
                        <th>A&ntilde;o base</th>
                        <td colspan="2">
                            <select id="ano_base" name="ano_base" onChange="calcularTasas();">
                                <option value="">Seleccione</option>
                                <%for(int i=0; i<anobase.size(); i++){%>
                                    <option value="<%=anobase.get(i).getTable_code()%>" ><%=anobase.get(i).getDescripcion()%></option>
                                <%}%>
                            </select>
                        </td>
                    </tr>
                    <tr class="fila">
                        <th>Rendimiento</th>
                        <td colspan="2">
                            <select id="rendimiento" name="rendimiento" onChange="calcularTasas()">
                                <option value="">Seleccione</option>
                                <%for(int i=0; i<rendimientos.size(); i++){%>
                                    <option value="<%=rendimientos.get(i).getTable_code()%>" ><%=rendimientos.get(i).getDescripcion()%></option>
                                <%}%>
                                
                           
                            </select>
                        <input name="rendimiento_dias" type="hidden" id="rendimiento_dias" onChange="calcularTasas();" onKeyUp="soloNumerosformateado(this.id);calcularTasas()" value="0" size="4" maxlength="3" /></td>
                    </tr>
                    <tr class="fila">
                        <th>Intereses</th>
                        <td colspan="2">
                            <select id="intereses" name="intereses" onChange="calcularTasas();">
                                <option value="">Seleccione</option>
                                <%for(int i=0; i<intereses.size(); i++){%>
                                    <option value="<%=intereses.get(i).getTable_code()%>" ><%=intereses.get(i).getDescripcion()%></option>
                                <%}%>
                            </select>
                        </td>
                    </tr>
                    <tr class="fila">
                        <th>Tasa E.A.</th> 
                        <td colspan="2">
                            <input type="text"id="ea" name="ea"    style="border: 0pt none;"  readonly/>
                            <input type="hidden" id="tasa_ea" name="tasa_ea"  /></td>
                    </tr>
                    <tr class="fila">
                      <th>Tasa Nominal</th>
                      <td colspan="2"><input type="text"id="nominal" name="nominal"   style="border: 0pt none;"  readonly/>
                      <input  type="hidden" id="tasa_nominal" name="tasa_nominal"  /></td>
                    </tr>
                    <tr class="fila">
                        <th>Tasa Diaria</th>
                        <td width="189">
                        <input name="diaria" type="text"  id="diaria" style="border: 0pt none;" maxlength="6"  readonly>
                        <input name="tasa_diaria" type="hidden"   class="fila" id="tasa_diaria" ></td>
                        <td width="85"><select id="ano_base_td" name="ano_base_td" onChange="calcularTasas();">
                          <option value="">Seleccione</option>
                          <%for(int i=0; i<anobase.size(); i++){%>
                          <option value="<%=anobase.get(i).getTable_code()%>" ><%=anobase.get(i).getDescripcion()%></option>
                          <%}%>
                        </select></td>
                    </tr>
                    <tr class="fila">
                        <th>Retefuente</th>
                        <td colspan="2">

                            <select name="rfte" id="rfte" style="width: 110px;">
                                                <option value="">...</option>
                                                <%
                                                            for (int i = 0; i < impuestos_rfte.size(); i++) {
                                                                Tipo_impuesto imp = (Tipo_impuesto) impuestos_rfte.get(i);
                                                                if (!imp.getDescripcion().equals("")) {
                                                %>
                                                <option value="<%=imp.getCodigo_impuesto()%>"  ><%=imp.getDescripcion()%></option>
                                                <%  }
                                                            }%>
                                            </select>
                        </td>
                    </tr>
                    <tr class="fila">
                      <th>Reteica</th>
                      <td colspan="2">                            <select name="rica" id="rica" style="width: 110px;">
                                                <option value="">...</option>
                                                <%
                                                            for (int i = 0; i < impuestos_rica.size(); i++) {
                                                                Tipo_impuesto imp = (Tipo_impuesto) impuestos_rica.get(i);
                                                                if (!imp.getDescripcion().equals("")) {
                                                %>
                                                <option value="<%=imp.getCodigo_impuesto()%>"  ><%=imp.getDescripcion()%></option>
                                                <%  }
                                                            }%>
                                            </select></td>
                    </tr>
                    <tr class="fila">
                      <th>Dias Calendarios I.</th>
                      <td colspan="2"><select id="dias_calendario" name="dias_calendario"  >
                        <option value="">Seleccione</option>
                        <%for(int i=0; i<anobase.size(); i++){%>
                        <option value="<%=anobase.get(i).getTable_code()%>" ><%=anobase.get(i).getDescripcion()%></option>
                        <%}%>
                      </select></td>
                    </tr>
                    <tr class="fila">
                      <th>Pago Automatico Intereses</th>
                      <td colspan="2"><select  id="pago_automatico" name="pago_automatico"  >
                        <option value="0">Seleccione</option>
                        <option value="S">SI</option>
                        <option value="N">NO</option>
                      </select></td>
                    </tr>
                    <tr class="fila">
                      <th>Periodicidad De Pago</th>
                      <td colspan="2"><select id="periodicidad_pago" name="periodicidad_pago" >
                        <option value="">Seleccione</option>
                        <%for(int i=0; i<rendimientos.size(); i++){%>
                        
                          if(!rendimientos.get(i).getTable_code().equals("0"))
                          
                        <option value="<%=rendimientos.get(i).getTable_code()%>" ><%=rendimientos.get(i).getDescripcion()%></option>
                        <%}%>
                      </select></td>
                    </tr>
                    <tr class="fila">
                        <th>Generar Documentos</th>
                        <td colspan="2">
                          <input name="genera_documentos" type="checkbox" id="genera_documentos">
                        </td>
                    </tr>
                </table>
<br/>
                <div align="center">
                    <img style="cursor:pointer" src="<%= BASEURL%>/images/botones/aceptar.gif" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" onClick="guardarLiquidacion('<%=CONTROLLER%>')"/>
                    <img style="cursor:pointer" src="<%= BASEURL%>/images/botones/salir.gif" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" onClick="parent.close();"/>
                </div>
                <%if(request.getParameter("mensaje")!=null && !request.getParameter("mensaje").equals("")){%>
                    <div id="divMensaje">
                        <table border="2" align="center">
                            <tr>
                                <td>
                                    <table width="100%"  border="0" align="center"  bordercolor="#f7f5f4" bgcolor="#ffffff">
                                        <tr>
                                            <td width="229" align="center" class="mensajes"><%=request.getParameter("mensaje")%></td>
                                            <td width="29" background="<%=BASEURL%>/images/cuadronaranja.JPG">&nbsp;&nbsp;</td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                        </table>
                    </div>
                <%}%>
            </form>
        </div>
    </body>
</html>
