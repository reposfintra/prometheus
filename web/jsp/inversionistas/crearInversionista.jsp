<%-- 
    Document   : crearInversionista
    Created on : 23/02/2012, 05:37:05 PM
    Author     : darrieta
--%>

<%@page import="com.tsp.operation.model.beans.TablaGen"%>
<%@page import="java.util.LinkedList"%>
<%@page import="com.tsp.operation.model.beans.Inversionista"%>
<%@page import="java.util.ArrayList"%>
<%@page import="com.tsp.operation.model.services.CaptacionInversionistaService"%>
<%@page import="com.tsp.operation.model.beans.NitSot"%>
<%@page import="com.tsp.operation.model.beans.Usuario"%>
<%@page contentType="text/html" pageEncoding="ISO-8859-1"%>
<%@include file="/WEB-INF/InitModel.jsp"%>
<%
Usuario usuario = (Usuario)session.getAttribute("Usuario");
String inversionista = (request.getParameter("inversionista")==null)?"":request.getParameter("inversionista");
CaptacionInversionistaService srv = new CaptacionInversionistaService(usuario.getBd());

model.nitService.searchNit(inversionista);
NitSot nit = srv.buscarNit(inversionista);
CaptacionInversionistaService svc = new CaptacionInversionistaService(usuario.getBd());
ArrayList<Inversionista> inversionistas = svc.consultarInversionistas(usuario.getDstrct());
model.tablaGenService.buscarRegistrosNoAnulados("PARENTESCO");
LinkedList<TablaGen> parentesco = model.tablaGenService.getTablas();
model.tablaGenService.buscarRegistrosNoAnulados("TIPO_INVER");
LinkedList<TablaGen> tipo_inversionistas = model.tablaGenService.getTablas();
Inversionista Objinversionista = new Inversionista();
if(nit!=null)
Objinversionista=svc.consultarSubcuentaInversionista(usuario.getDstrct(), nit.getCedula(), 1);
%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1"/>
        <title>Crear inversionista</title>
        <link href="<%= BASEURL%>/css/estilostsp.css" rel='stylesheet'/>
          <link href="<%= BASEURL%>/css/estilosFintra.css" rel='stylesheet'/>
        <script type='text/javascript' src="<%= BASEURL%>/js/boton.js"></script>
        <script type='text/javascript' src="<%= BASEURL%>/js/prototype.js"></script>
        <script type='text/javascript' src="<%= BASEURL%>/js/captaciones.js"></script>
        
    </head>
    <body>
        <div id="capaSuperior" style="position:absolute; width:100%; height:100px; z-index:0; left: 0px; top: 0px;">
            <jsp:include page="/WEB-INF/toptsp.jsp?encabezado=Inversionistas"/>
        </div>
        <div id="capaCentral" style="position:absolute; width:100%; height:85%; z-index:0; left: 0px; top: 100px; overflow: auto;">
            <form id="form1" name="form1" method="post">
                <table align="center" style="border: green solid 1px;" width="400"  class="tableTextUI" >
                    <tr class="barratitulo">
                        <td class="subtitulo1">Inversionista</td>
                        <td><img src="<%=BASEURL%>/images/titulo.gif" width="32" height="20" align="left"/></td>
                    </tr>
                    <tr class="fila">
                        <td>Cedula inversionista</td>
                        <td>
                            <input type="text" id="inversionista" name="inversionista" value="<%=inversionista%>" />
                            <img alt="buscar" src="<%=BASEURL%>/images/botones/iconos/lupa.gif" width="15" height="15" style="cursor:pointer;" onClick="consultarCedula('<%=CONTROLLER%>','<%=BASEURL%>');"/>
                        </td>
                    </tr>
                    <%if(!inversionista.equals("")){
                        ArrayList<Inversionista> subcuentas = srv.consultarSubcuentas(inversionista, usuario.getDstrct());
                    %>
                        <tr class="fila">
                            <td>Nombre</td>
                            <td>
                                <%=nit.getNombre()%>
                            </td>
                        </tr>
                        <tr class="fila">
                            <td>Cedula</td>
                            <td>
                                <%=nit.getCedula()%>
                                <input type="hidden" id="cedula" name="cedula" value="<%=nit.getCedula()%>" />
                            </td>
                        </tr>
                        <tr class="fila">
                            <td>Direccion casa</td>
                            <td>
                                <%=nit.getDireccion()%>
                            </td>
                        </tr>
                        <tr class="fila">
                            <td>Direccion oficina</td>
                            <td>
                                <input type="text" id="direccion_oficina" name="direccion_oficina" value="<%=nit.getDireccion_oficina()%>" />
                            </td>
                        </tr>
                        <tr class="fila">
                            <td>Pais</td>
                            <td>
                                <%=nit.getCodpais()%>
                            </td>
                        </tr>
                        <tr class="fila">
                            <td>Ciudad</td>
                            <td>
                                <%=nit.getCodciu()%>
                            </td>
                        </tr>
                        <tr class="fila">
                            <td>Telefono</td>
                            <td>
                                <%=nit.getTelefono()%>
                            </td>
                        </tr>
                        <tr class="fila">
                            <td>Celular</td>
                            <td>
                                <%=nit.getCellular()%>
                            </td>
                        </tr>
                        <tr class="fila">
                            <td>Correo electronico</td>
                            <td>
                                <%=nit.getE_mail()%>
                            </td>
                        </tr>
                        <tr class="fila">
                          <td>Correo electronico 2</td>
                          <td><input type="text" id="e_mail2" name="e_mail2" value="<%=nit.getE_mail2()%>" /></td>
                        </tr>
                        <tr class="fila">
                          <td>Tipo Inversionista</td>
                          <td><select id="tipo_inversionista" name="tipo_inversionista" onChange="validaTipoInversionista()">
                            <option value="">Seleccione</option>
                            <%for(int i=0; i<tipo_inversionistas.size(); i++){%>
                            <option value="<%=tipo_inversionistas.get(i).getTable_code()%>" <%= tipo_inversionistas.get(i).getTable_code().equals(Objinversionista.getTipo_inversionista()) ? "selected" :""%> > <%=tipo_inversionistas.get(i).getDescripcion()%></option>
                            <%}%>
                          </select></td>
                        </tr>
                        <tr class="fila">
                          <td>Frente a </td>
                          <td><select id="nit_parentesco" name="nit_parentesco"  >
                            <option value="">Seleccione</option>
                            <%for(int i=0; i<inversionistas.size(); i++){%>
                            <option value="<%=inversionistas.get(i).getNit()%>"
                            <%= inversionistas.get(i).getNit_parentesco().equals(Objinversionista.getNit_parentesco()) ? "selected" :""%>><%=inversionistas.get(i).getNombre_subcuenta()%></option>
                            <%}%>
                          </select></td>
                        </tr>
                        <tr class="fila">
                            <td>Parentesco</td>
                            <td><select id="tipo_parentesco" name="tipo_parentesco" onChange="">
                              <option value="">Seleccione</option>
                              <%for(int i=0; i<parentesco.size(); i++){%>
                              <option value="<%=parentesco.get(i).getTable_code()%>"
                                      <%= parentesco.get(i).getTable_code().equals(Objinversionista.getTipo_parentesco()) ? "selected" :""%>><%=parentesco.get(i).getTable_code()%> - <%=parentesco.get(i).getReferencia()+" "+ parentesco.get(i).getDescripcion()%></option>
                              <%}%>
                            </select></td>
                        </tr>
                        <tr class="barratitulo">
                            <td class="subtitulo1">Subcuentas</td>
                            <td><img src="<%=BASEURL%>/images/titulo.gif" width="32" height="20" align="left"/></td>
                        </tr>
                        <tr class="Titulos">
                            <td colspan="2">
                                Nombre
                            </td>
                        </tr>
                        <%if(subcuentas.size()==0){%>
                            <tr id="fila0" class="fila">
                                <td colspan="2">
                                    <img alt="+" width="15" height="15" src="<%=BASEURL%>/images/botones/iconos/mas.gif" onClick="agregarSubcuenta(0,'<%=BASEURL%>')" />
                                    <img alt="-" width="15" height="15" src="<%=BASEURL%>/images/botones/iconos/menos1.gif" onClick="eliminarSubcuenta(0)" />
                                    <input type="text" style="width: 300px;" id="nombre_subcuenta0" name="nombre_subcuenta0" value="<%=nit.getNombre()%>" />
                                </td>
                            </tr>
                        <%}else{
                            for(int i = 0; i<subcuentas.size(); i++){
                        %>
                            <tr id="fila<%=i%>" class="fila">
                                <td colspan="2">
                                    <img alt="+" width="15" height="15" src="<%=BASEURL%>/images/botones/iconos/mas.gif" onClick="agregarSubcuenta(<%=i%>,'<%=BASEURL%>')" />
                                    <input type="text" style="width: 300px;" id="nombre_subcuenta<%=i%>" name="nombre_subcuenta<%=i%>" value="<%=subcuentas.get(i).getNombre_subcuenta()%>" />
                                    <input type="hidden" style="width: 300px;" id="subcuenta<%=i%>" name="subcuenta<%=i%>" value="<%=subcuentas.get(i).getSubcuenta()%>" />
                                </td>
                            </tr>
                            <%}%>
                        <%}%>
                        <input type="hidden" id="max" name="max" value="<%=(subcuentas.size()==0)?0:subcuentas.size()-1%>" />
                        <input type="hidden" id="count" name="count" value="<%=(subcuentas.size()==0)?1:subcuentas.size()%>" />
                        <input type="hidden" id="tipo" name="tipo" value="<%=(subcuentas.size()==0)?"NUEVO":"EDITAR"%>" />
                    <%}%>
                </table>
  <br/>
                <div align="center">
                    <%if(!inversionista.equals("")){%>
                    <img style="cursor:pointer" src="<%= BASEURL%>/images/botones/aceptar.gif" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" onClick="guardarInversionista('<%=CONTROLLER%>')"/>
                <%}%>
                    <img style="cursor:pointer" src="<%= BASEURL%>/images/botones/salir.gif" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" onClick="parent.close();"/>
                </div>
                <%if(request.getParameter("mensaje")!=null && !request.getParameter("mensaje").equals("")){%>
                    <div id="divMensaje">
                        <table border="2" align="center">
                            <tr>
                                <td>
                                    <table width="100%"  border="0" align="center"  bordercolor="#f7f5f4" bgcolor="#ffffff">
                                        <tr>
                                            <td width="229" align="center" class="mensajes"><%=request.getParameter("mensaje")%></td>
                                            <td width="29" background="<%=BASEURL%>/images/cuadronaranja.JPG">&nbsp;&nbsp;</td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                        </table>
                    </div>
                <%}%>
            </form>
        </div>
    </body>
</html>
