<%-- 
    Document   : aprobar retiros
    Created on : 18/05/2012, 05:37:23 PM
    Author     : jpinedo
--%>

<%@page import="com.tsp.operation.model.services.ClientesVerService"%>
<%@page import="java.util.Vector"%>
<%@page import="com.tsp.operation.model.services.CaptacionInversionistaService"%>
<%@page import="com.tsp.util.Util"%>
<%@page import="com.tsp.util.UtilFinanzas"%>
<%@page contentType="text/html" pageEncoding="ISO-8859-1"%>
<%@include file    ="/WEB-INF/InitModel.jsp"%>
<%@page import="java.util.ArrayList"%>
<%@page import="com.tsp.operation.model.beans.*"%>
<%
Usuario usuario = (Usuario)session.getAttribute("Usuario");
CaptacionInversionistaService cis = new CaptacionInversionistaService(usuario.getBd());


int  codsubcuenta = 1;

String nit=usuario.getCedula();
ArrayList<Inversionista> inversionistas = null;
ClientesVerService clvsrv = new ClientesVerService(usuario.getBd());
String perfil = clvsrv.getPerfil(((Usuario) session.getAttribute("Usuario")).getLogin());

if(perfil.contains("permiso_extractos")) {
   inversionistas = cis.consultarInversionistas(usuario.getDstrct());
}
Inversionista subcuenta = cis.consultarSubcuentaInversionista(usuario.getDstrct(),nit, codsubcuenta);
ArrayList<Inversionista> subcuentas =cis.consultarSubcuentas(nit, usuario.getDstrct());


%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1"/>
        <title>Extracto Mensual</title>
        <link href="<%= BASEURL%>/css/estilostsp.css" rel='stylesheet'/>
        <link href="<%= BASEURL%>/css/letras.css" rel='stylesheet'/>
        <link href="<%=BASEURL%>/css/default.css"       rel="stylesheet" type="text/css"/>
        <script type='text/javascript' src="<%= BASEURL%>/js/boton.js"></script>

        <script type='text/javascript' src="<%= BASEURL%>/js/captaciones.js"></script>
        <script type='text/javascript' src="<%=BASEURL%>/js/validar.js"></script>
          <link href="<%= BASEURL%>/css/estilosFintra.css" rel='stylesheet'/>




         <link href="<%= BASEURL%>/css/estilostsp.css" rel="stylesheet" type="text/css">
        <link href="<%= BASEURL%>/css/jquery/jquery-ui/jquery-ui.css" rel="stylesheet" type="text/css"/>
        <link href="<%= BASEURL%>/css/jquery/jquery.jqGrid/ui.jqgrid.css" rel="stylesheet" type="text/css"/>
        <script type='text/javascript' src="<%= BASEURL%>/js/drypers/CiudadDrypers.js"></script>
        <script type='text/javascript' src="<%= BASEURL%>/js/boton.js"></script>
        <script type='text/javascript' src="<%= BASEURL%>/js/jquery/jquery-ui/jquery.min.js"></script>
        <script type='text/javascript' src="<%= BASEURL%>/js/jquery/jquery-ui/jquery.ui.min.js"></script>
        <script type='text/javascript' src="<%= BASEURL%>/js/jquery/jquery.jqGrid/js/grid.locale-es.js"></script>
        <script type='text/javascript' src="<%= BASEURL%>/js/jquery/jquery.jqGrid/js/jquery.jqGrid.min.js"></script>






                
        
        
        
        
        <script>
 
       $(document).ready(
						
                function() {
	            cargarAnoMes();
                    cargarMovimientos();
                    $("#tabla_datos").styleTable();

					});

   



function cargarMovimientos()
{
        if (document.getElementById('inversionista')) {
            document.getElementById('nit').value = document.getElementById('inversionista').value;
        }
        
                    jQuery("#Movimientos").jqGrid({
                        caption: 'Movimientos   Captaciones' ,
                        url: "<%=CONTROLLER%>?estado=Captacion&accion=Inversionista&op=17&nit="+$('#nit').val()+"&subcuenta="+$('#subcuenta').val()+"&periodo="+$('#ano').val()+$('#mes').val(),
                        datatype: "json",
                        height: 'auto',
                        width: 1200,
                     colNames:['Transaccion','Fecha','Saldo Inicial','Base Intereses','Intereses','Itereses acomulados','Retefuente','Reteica','SubTotal','Consignacion','Retiro','Saldo','Concepto Transacción'],
                       colModel:[
                            {name:'no_transaccion',index:'items', sortable:false, align:'center',key:true,width: 90},
                            {name:'fecha',index:'fecha', sortable:false ,align:'center',
			    formatter: 'date' },
                            {name:'saldo_inicial',index:'saldo_inicial', sortable:false,align:'right' ,hidden:<%=(subcuenta.getTipo_interes()!=null)?(!subcuenta.getTipo_interes().equals("C")):false%>,
							formatter:'currency', formatoptions:{decimalSeparator:",", thousandsSeparator: ",", decimalPlaces: 2, prefix: "$ "}},
                            {name:'base_intereses',index:'base_intereses', sortable:false,align:'right',hidden:<%=(subcuenta.getTipo_interes()!=null)?(!subcuenta.getTipo_interes().equals("S")):false%>,
							formatter:'currency', formatoptions:{decimalSeparator:",", thousandsSeparator: ",", decimalPlaces: 2, prefix: "$ "}},
                            {name:'valor_intereses',index:'valor_intereses', sortable:false, align:'right',
		            formatter:'currency', formatoptions:{decimalSeparator:",", thousandsSeparator: ",", decimalPlaces: 2, prefix: "$ "}
							},
                            {name:'intereses_acomulados',index:'intereses_acomulados', sortable:false,align:'right',hidden:<%=(subcuenta.getTipo_interes()!=null)?(!subcuenta.getTipo_interes().equals("S")):false%>,
			    formatter:'currency', formatoptions:{decimalSeparator:",", thousandsSeparator: ",", decimalPlaces: 2, prefix: "$ "}
							},
                            {name:'valor_retefuente',index:'valor_retefuente', sortable:false,align:'right',
			    formatter:'currency', formatoptions:{decimalSeparator:",", thousandsSeparator: ",", decimalPlaces: 2, prefix: "$ "}
							},
                            {name:'valor_reteica',index:'valor_reteica', sortable:false,align:'right',
			    formatter:'currency', formatoptions:{decimalSeparator:",", thousandsSeparator: ",", decimalPlaces: 2, prefix: "$ "}
							},
                            {name:'subtotal',index:'subtotal', sortable:false,align:'right',hidden:<%=(subcuenta.getTipo_interes()!=null)?(!subcuenta.getTipo_interes().equals("C")):false%>,
			    formatter:'currency', formatoptions:{decimalSeparator:",", thousandsSeparator: ",", decimalPlaces: 2, prefix: "$ "}
							},
                            {name:'consignacion',index:'consignacion', sortable:false,align:'right',
							formatter:'currency', formatoptions:{decimalSeparator:",", thousandsSeparator: ",", decimalPlaces: 2, prefix: "$ "}
			    },
			    {name:'retiro',index:'retiro', sortable:false,align:'right',
							formatter:'currency', formatoptions:{decimalSeparator:",", thousandsSeparator: ",", decimalPlaces: 2, prefix: "$ "}
			    },
			    {name:'saldo_final',index:'saldo_final', sortable:false,align:'right',
			    formatter:'currency', formatoptions:{decimalSeparator:",", thousandsSeparator: ",", decimalPlaces: 2, prefix: "$ "}
			    },
                            {name:'concepto_transaccion',index:'concepto_transaccion',sortable:false}
                        ],
                        rowNum:-1,
                        rowTotal: 500000,
                        loadonce:true,
                        rownumWidth: 40,
                        gridview: true,
                        viewrecords: true,
                        hidegrid: false,
                        jsonReader : {
                            root:"rows",
                            repeatitems: false,
                            id: "0"
                        },
                        loadError: function(xhr, status, error){
                            alert(error);
                        }
                    });
}



						
(function ($) {
    $.fn.styleTable = function (options) {
        var defaults = {
            css: 'styleTable'
        };
        options = $.extend(defaults, options);

        return this.each(function () {

            input = $(this);
            input.addClass(options.css);

                       
            input.find("th").addClass("ui-state-default");
            input.find("td").addClass("ui-widget-content");

            input.find("tr").each(function () {
                $(this).children("td:not(:first)").addClass("first");
                $(this).children("th:not(:first)").addClass("first");
            });
        });
    };
})(jQuery);		


				

 $(document).ready(function(){
                $("#divSalida").dialog({
                    autoOpen: false,
                    height: "auto",
                    width: "35%",
                    modal: true,
                    autoResize: true,
                    resizable: false,
                    position: "center",
                    closeOnEscape: false


                });
              
            });

				
</script>
        
        <style type="text/css">
            div.ui-dialog{
                font-size:12px;            }

            .no-close.ui-dialog-titlebar-close {display: none }
        </style>   
      
        
    </head>
    <body>
        <div id="capaSuperior" style="position:absolute; width:100%; height:100px; z-index:0; left: 0px; top: 0px;">
            <jsp:include page="/WEB-INF/toptsp.jsp?encabezado=Movimientos y Transacciones"/>
        </div>
        <div id="capaCentral" style="position:absolute; width:100%; height:85%; z-index:0; left: 4px; top: 97px; overflow: auto;">
            
            <input name="BASEURL" type="hidden" id="BASEURL" value="<%=BASEURL%>">
            
            <input name="CONTROLLER" type="hidden" id="CONTROLLER" value="<%=CONTROLLER%>">
            
         
<div id="contenido" align="center" style=" width: 600px; height: 300px; visibility: hidden; background-color:#F7F5F4; display: none"></div>
<input name="nit" type="hidden" id="nit" value="<%=nit%>">
<br/>
<table  width="30%" align="center" id="tabla_datos"   class="tableTextUI" >
  <tr >
      <%if(inversionistas != null){%>
        <th align="left">Inversionista</th>
        <td align="left" ><select id="inversionista" name="inversionista" onChange="cargarSubcuentas()" >
                <option value="">Seleccione</option>
                <%for (int i = 0; i < inversionistas.size(); i++) {%>
                <option value="<%=inversionistas.get(i).getNit()%>"  <%=inversionistas.get(i).getNit().equals(usuario.getCedula()) ? "selected" : ""%> >
                    <%=inversionistas.get(i).getNombre_subcuenta()%></option>
                <%}%>
            </select>
        </td>
    <%}%>
      <th width="418">Subcuenta</th>
    <td width="418"><select name="subcuenta" id="subcuenta" onChange= " <%if(inversionistas != null){%>cargarMovimientos();<%}%> refrescarGridMovimientosMes();">
              <%for (int i = 0; i < subcuentas.size(); i++) {%>
           <option value=<%= subcuentas.get(i).getSubcuenta() %>>   <%=subcuentas.get(i).getNombre_subcuenta() %></option>;
        <%}%>
    </select></td>
    <th width="136"><b>A&ntilde;o</b></th>
    <td width="418">
        <select name="ano" id="ano" onChange= "refrescarGridMovimientosMes();">
      <option value="...">Seleccione</option>
 
      </select></td>
    <th width="418">Mes</th>
    <td width="418"><select name="mes" id="mes" onChange= "refrescarGridMovimientosMes();" >
      <option value="...">Seleccione</option>
    </select>
 </td>

  </tr>
</table>
<br/>
<table border="0"  align="center">
<tr>
                    <td>
                     <table id="Movimientos" class="center_div" ></table>
                    </td>
      </tr>            
                  
              </table>
<br/>
<br/><p><br/>
                  
                
                </p>
                <div align="center">
        
                    
                        <table align="center">
                          <tr >
                            <td><img src="<%=BASEURL%>/images/botones/generarPdf.gif"   name="imgapdf" height="21" align="absmiddle" id="imgapdf" style="cursor:pointer; display: block" onClick="Exportar('pdf')" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);"></td>
                            <td><img src="<%=BASEURL%>/images/botones/exportarExcel.gif"   name="imgexcel" height="21" align="absmiddle" id="imgexcel" style="cursor:pointer; display: block" onClick="Exportar('xls')" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);"></td>
                            <td><img src="<%=BASEURL%>/images/botones/restablecer.gif"   name="imgrestablecer" height="21" align="absmiddle" id="imgrestablecer" style="cursor:pointer; display: block" onClick="refreshGridCiuTsp('<%=CONTROLLER%>');" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);"></td>
                            <td> <img style="cursor:pointer" src="<%= BASEURL%>/images/botones/salir.gif" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" onClick="parent.close();"/></td>
                          </tr>
                        </table>
              </div>
                
                
                             <div id="divSalida" title="Exportacion" style=" display: block" >
        <div id="resp"></div>
         </div>  
                
                <br/>
            <br/>
                <br/>
                <br/>
                <br/>
                <br/>
                <br/>
                <br/>
                
                 <br>
           
         
         
                  
        </div>
        
       
    </body>
</html>

