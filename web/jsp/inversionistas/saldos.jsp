<%-- 
    Document   : consultaCreditoBancario
    Created on : 10/01/2012, 05:37:23 PM
    Author     : darrieta
--%>

<%@page import="com.tsp.operation.model.services.CaptacionInversionistaService"%>
<%@page import="com.tsp.util.Util"%>
<%@page import="com.tsp.util.UtilFinanzas"%>
<%@page contentType="text/html" pageEncoding="ISO-8859-1"%>
<%@include file    ="/WEB-INF/InitModel.jsp"%>
<%@page import="java.util.ArrayList"%>
<%@page import="com.tsp.operation.model.beans.*"%>
<%
Usuario usuario = (Usuario)session.getAttribute("Usuario");
CaptacionInversionistaService cis = new CaptacionInversionistaService();
String nombre = request.getParameter("nombre");
String nit = request.getParameter("nit");
Double total = Double.parseDouble(request.getParameter("total"));

%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1"/>
        <title>Saldos Inversionistas</title>

                
        
                <style type="text/css">
            div.ui-dialog{
                font-size:12px;            }

            <%-- .no-close.ui-dialog-titlebar-close {display: none } --%>

              .link_green{color: #063; cursor:pointer; text-decoration:underline }
        



        </style>
        
        
        <script>
      
        
                $(document).ready(
                function() {
		// $("#tabla_datos").styleTable();
		// $("#tabla_total").styleTable();
		 
                    
                    jQuery("#Movimientos").jqGrid({
                        caption: "Saldos " ,
                        url: "<%=CONTROLLER%>?estado=Captacion&accion=Inversionista&op=15&nit=<%=nit%>&subcuenta=1",
                        datatype: "json",
                        height: 'auto',
                        width: 600,
                     colNames:['Nombre subcuenta','Saldo'],
                       colModel:[
                            {name:'nombre_subcuenta',index:'nombre_subcuenta', sortable:false,align:'center',width: 300 },
			  
                            {name:'saldo_final',index:'saldo_final', sortable:false, align:'right',width: 135,
		            formatter:'currency', formatoptions:{decimalSeparator:",", thousandsSeparator: ",", decimalPlaces: 2, prefix: "$ "}} ,
                           
                           
                    ],
                        rowNum:-1,
                        rowTotal: 500000,
                        loadonce:true,
                        rownumWidth: 40,
                        gridview: true,
                        viewrecords: true,
                        hidegrid: false,                      
                        jsonReader : {
                            root:"rows",
                            repeatitems: false,
                            id: "0"
                        },
                        gridComplete:
						function(){
							
                                                  
							document.getElementById("total").innerHTML = '<%=Util.customFormat(total)%>';
						} ,
                        loadError: function(xhr, status, error){
                            alert(error);
                        }
                    });
                });

(function ($) {
    $.fn.styleTable = function (options) {
        var defaults = {
            css: 'styleTable'
        };
        options = $.extend(defaults, options);

        return this.each(function () {

            input = $(this);
            input.addClass(options.css);


            input.find("th").addClass("ui-state-default");
            input.find("td").addClass("ui-widget-content");

            input.find("tr").each(function () {
                $(this).children("td:not(:first)").addClass("first");
                $(this).children("th:not(:first)").addClass("first");
            });
        });
    };
})(jQuery);	

				


				


				
</script>
        

      
        
    </head>
    <body>

<div style="width:600px">
            
         

<table  width="600" align="center" cellspacing="0" id="tabla_datos"  style="font-size:12px" >
  <tr >
    <th width="68"><b>Inversionista </b></th>
    <td width="175"><%=nombre%></td>
    <th width="63">NIt </th>
    <td width="61"><%=nit%></td>
    </tr>
</table>
<br/>
    <table border="0"  align="center">
                  <tr>
                    <td>
                     <table id="Movimientos" class="center_div" width="auto" ></table>
                    </td>
                  </tr>

                </table>
                 <br/>
                   <table  width="200" border="0" align="right" cellspacing="0" id="tabla_total"  style="font-size:12px; text-align: right;" >
  <tr >
    <th width="465" align="center"><b>Total </b></th>
    <td width="135" align="right"><b>$<span id="total"></span>

                       </b></td>
    </tr>
</table>

        
</div>
        
       
    </body>
</html>

