<%-- 
    Document   : consultaCreditoBancario
    Created on : 10/01/2012, 05:37:23 PM
    Author     : darrieta
--%>

<%@page import="com.tsp.operation.model.services.CaptacionInversionistaService"%>
<%@page import="com.tsp.util.Util"%>
<%@page import="com.tsp.util.UtilFinanzas"%>
<%@page contentType="text/html" pageEncoding="ISO-8859-1"%>
<%@include file    ="/WEB-INF/InitModel.jsp"%>
<%@page import="java.util.ArrayList"%>
<%@page import="com.tsp.operation.model.beans.*"%>
<%
Usuario usuario = (Usuario)session.getAttribute("Usuario");
CaptacionInversionistaService cis = new CaptacionInversionistaService(usuario.getBd());
String mensaje = request.getParameter("mensaje");
String opcion = request.getParameter("opcion");
int op= 0;
int opaction= 0;

String title= "";
if(opcion.equals("confirmar")){
title="Confirmacion De Retiros";
op= 9;
opaction= 8;
}
else{
    if(opcion.equals("aprobar")){
title="Aprobacion De Retiros";
op= 11;
opaction= 10;
}
    }

%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1"/>
        <title>Movimientos y Transacciones</title>
        <link href="<%= BASEURL%>/css/estilostsp.css" rel='stylesheet'/>
        <script type='text/javascript' src="<%= BASEURL%>/js/boton.js"></script>       
        <script type='text/javascript' src="<%= BASEURL%>/js/captaciones.js"></script>
        <script type='text/javascript' src="<%=BASEURL%>/js/validar.js"></script>                  
         <link href="<%= BASEURL%>/css/estilostsp.css" rel="stylesheet" type="text/css"> 
        <link href="<%= BASEURL%>/css/jquery/jquery-ui/jquery-ui.css" rel="stylesheet" type="text/css"/>
        <link href="<%= BASEURL%>/css/jquery/jquery.jqGrid/ui.jqgrid.css" rel="stylesheet" type="text/css"/>
          <link href="<%= BASEURL%>/css/estilosFintra.css" rel='stylesheet'/>

        <script type='text/javascript' src="<%= BASEURL%>/js/boton.js"></script>
        <script type='text/javascript' src="<%= BASEURL%>/js/jquery/jquery-ui/jquery.min.js"></script>
        <script type='text/javascript' src="<%= BASEURL%>/js/jquery/jquery-ui/jquery.ui.min.js"></script>
        <script type='text/javascript' src="<%= BASEURL%>/js/jquery/jquery.jqGrid/js/grid.locale-es.js"></script>
        <script type='text/javascript' src="<%= BASEURL%>/js/jquery/jquery.jqGrid/js/jquery.jqGrid.min.js"></script>
                
        
                <style type="text/css">
            div.ui-dialog{
                font-size:12px;            }

            .no-close.ui-dialog-titlebar-close {display: none }

              .link_green{color: #063; cursor:pointer; text-decoration:underline }
        



        </style>
        
        
        <script>
         <!-- cargar movimientos pendientes por confirmar -->
          var unchecked = null;
                $(document).ready(
                function() {
                    
                    jQuery("#Retiros").jqGrid({
                        caption: "<%=title%>" ,
                        url: "<%=CONTROLLER%>?estado=Captacion&accion=Inversionista&op=<%=opaction%>",
                        datatype: "json",
                        height: 'auto',
                        width: 1000,
                     colNames:['Transaccion','Nit','Nombre','Fecha','Valor Retiro','Tipo Trasferencia'],
                       colModel:[
                            {name:'no_transaccion',index:'no_transaccion', sortable:false, align:'center',key:true,width: 85},
			     {name:'nit',index:'nit', sortable:false, align:'center',key:true,width: 85},
                            {name:'nombre_inversionista',index:'nombre_inversionista', sortable:false,align:'center',width: 300 },
			    {name:'fecha',index:'fecha', sortable:false,align:'center',formatter: 'date' ,width: 90},
                            {name:'retiro',index:'retiro', sortable:false, align:'right',width: 135,
		            formatter:'currency', formatoptions:{decimalSeparator:",", thousandsSeparator: ",", decimalPlaces: 2, prefix: "$ "}} ,
                            {name:'tipo_transferencia',index:'tipo_transferencia', sortable:false,align:'center',
                                formatter: function (cellValue, options) {
                                    return "<a  class='link_green' onclick='verReferenciaRetiro(\""+options.rowId+"\")' >"+cellValue+" </a>";
                                }},
           
                           
                    ],
                        rowNum:-1,
                        rowTotal: 500000,
                        loadonce:true,
                        rownumWidth: 40,
                        gridview: true,
                        viewrecords: true,
                        hidegrid: false,
                        multiselect: true,
                        jsonReader : {
                            root:"rows",
                            repeatitems: false,
                            id: "0"
                        },
                      
                        loadError: function(xhr, status, error){
                            alert(error);
                        }
                    });
                });

   


				


				

 $(document).ready(function(){
                $("#divSalida").dialog({
                    autoOpen: false,
                    height: "auto",
                    width: "35%",
                    modal: true,
                    autoResize: true,
                    resizable: false,
                    position: "center",
                    closeOnEscape: false


                });
              
            });

				
</script>
        

      
        
    </head>
    <body>
        <div id="capaSuperior" style="position:absolute; width:100%; height:100px; z-index:0; left: 0px; top: 0px;">
            <jsp:include page="/WEB-INF/toptsp.jsp?encabezado=Movimientos y Transacciones"/>
        </div>
        <div id="capaCentral" style="position:absolute; width:100%; height:85%; z-index:0; left: 4px; top: 96px; overflow: auto;">
            <form id="form1" name="form1" method="post" action="<%=CONTROLLER%>?estado=Credito&accion=Bancario&opcion=GUARDAR_DETALLE_CREDITO">
            <input name="BASEURL" type="hidden" id="BASEURL" value="<%=BASEURL%>">
            
            <input name="CONTROLLER" type="hidden" id="CONTROLLER" value="<%=CONTROLLER%>">
            
         
<div id="contenido" align="center" style=" width: 600px; height: 300px; visibility: hidden; background-color:#F7F5F4; display: none"></div>
<input name="opcion" type="hidden" id="opcion" value="<%=opcion%>">
<br/>
                <table border="0"  align="center">
                  <tr>
                    <td>
                     <table id="Retiros" class="center_div" width="auto" ></table>
                    </td>
                    </tr>
                </table>
                 <br/>
                 <br/><p><br/>
                  
                
                </p>
                <div align="center">
        
                    
                        <table align="center">
                          <tr>
                            <td><img src="<%=BASEURL%>/images/botones/aceptar.gif"   name="imgaceptar" height="21" align="absmiddle" id="imgaceptar" style="cursor:pointer; display: block" onClick="ejecutarOperacion(<%=op%>)" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);"></td>
                            <td><img src="<%=BASEURL%>/images/botones/restablecer.gif"   name="imgrestablecer" height="21" align="absmiddle" id="imgrestablecer" style="cursor:pointer; display: block" onClick="refreshGridCiuTsp('<%=CONTROLLER%>');" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);"></td>
                            <td><img style="cursor:pointer" src="<%= BASEURL%>/images/botones/salir.gif" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" onClick="parent.close();"/></td>
                          </tr>
                        </table>
              </div>
                
                
                             <div id="divSalida" title="<%=title%>" style=" display: none" >
        <div id="resp">Cargarndo..
        </div>
         </div>  
                
                <br/>
            <br/>
                <br/>
                <br/>
                <br/>
                <br/>
                <br/>
                <br/>
                
                
                
                
                   

            <br>
            </form>

         
         
                  
        </div>
        
       
    </body>
</html>

