<%-- 
    Document   : registrarMovimeinto
    Created on : 23/02/2012, 05:37:05 PM
    Author     : agutierrez
--%>

<%@page import="com.tsp.operation.model.beans.TablaGen"%>
<%@page import="java.util.LinkedList"%>
<%@page import="com.tsp.operation.model.beans.MovimientosCaptaciones"%>
<%@page import="java.util.List"%>
<%@page import="java.util.TreeMap"%>
<%@page import="com.tsp.operation.model.beans.Inversionista"%>
<%@page import="java.util.ArrayList"%>
<%@page import="com.tsp.operation.model.services.CaptacionInversionistaService"%>
<%@page import="com.tsp.operation.model.beans.NitSot"%>
<%@page import="com.tsp.operation.model.beans.Usuario"%>
<%@page import="com.tsp.util.*"%>
<%@taglib uri="http://jakarta.apache.org/taglibs/input-0.90" prefix="input" %>
<%@page contentType="text/html" pageEncoding="ISO-8859-1"%>
<%@include file="/WEB-INF/InitModel.jsp"%>
<%
CaptacionInversionistaService cis = new CaptacionInversionistaService();
Usuario usuario = (Usuario)session.getAttribute("Usuario");
String age = (usuario.getId_agencia().equals("OP")?"":usuario.getId_agencia());

model.servicioBanco.loadBancos(age, usuario.getCia());
TreeMap b = model.servicioBanco.getBanco();
String c_banco="BANCOLOMBIA";
int op = (request.getParameter("op") != null) ? Integer.parseInt(request.getParameter("op").toString()) : -1;
String tipo_movimiento = (request.getParameter("tipo_movimiento") != null) ? request.getParameter("tipo_movimiento") : "";
String nit = (request.getParameter("nit") != null) ? request.getParameter("nit") : "";
String subcuenta = (request.getParameter("subcuenta") != null) ? request.getParameter("subcuenta") : "";
String tipo_transferencia = (request.getParameter("tipo_transferencia") != null) ? request.getParameter("tipo_transferencia") : "";
double tope=0;
 ArrayList<MovimientosCaptaciones> lista_movimientos = new ArrayList<MovimientosCaptaciones>() ;
  Inversionista inversionista = new  Inversionista();
int opcion=0;
int ano=2010;
int mes=0;
int dia=0;

if (op == 1) {//movimiento consignacion
                String fecha = cis.consultarFechaUltimoCierre(usuario.getDstrct(), nit, subcuenta);
                if (fecha!=null) {
                    ano = Integer.parseInt(fecha.substring(0, 4));
                    mes = Integer.parseInt(fecha.substring(5, 7));
                    dia = Integer.parseInt(fecha.substring(8, 10));
                }
            }


%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1"/>
        <title>Crear inversionista</title>
        <link href="<%= BASEURL%>/css/estilostsp.css" rel='stylesheet'/>
        <script type='text/javascript' src="<%= BASEURL%>/js/captaciones.js"></script>

    </head>
    <script>
    $(function() {
	

//$('select#banco').selectmenu();
//$('select#sucursal').selectmenu();

           
$('#fecha').datepicker({
						minDate: new Date(<%=ano%>,<%=mes-1%>, <%=dia%>),
						maxDate: new Date(),
						dateFormat: 'yy-mm-dd',
						constrainInput: true						
					});

		

	});

    </script>
    
    
            <style type="text/css">
            div.ui-dialog{
                font-size:12px;
            }

            ui-select{
                font-size:12px;
            }
			  div.ui-datepicker{
                font-size:12px;
            }
        </style>   
    <body>
    <div  id="contenedor">

<%


switch (op) {
  case 2 :/*medio de retiro */ 
  opcion=2;%>
 <table align="center" style="border: green solid 1px;" width="400" id="tabla_datos" >

                            <tr>
                                <th width="50%" align="left"  ><strong> Medio de Trasferencia</strong></th>
                                <th width="50%" align="left" >
                                    <img alt="" src="<%= BASEURL%>/images/titulo.gif" width="32" height="20" align="left"/>
                                </th>
                            </tr>
   <tr class="fila">
     <td width="155">Electronica</td>
     <td width="233"><label>
       <input type="radio" name="medio_retiro" id="medio_retiro" value="E">
     </label></td>
   </tr>
   <tr class="fila">
     <td>Cheque</td>
     <td><input type="radio" name="medio_retiro" id="medio_retiro2" value="C"></td>
   </tr>
   <tr class="fila">
     <td>Interna     </td>
     <td><input type="radio" name="medio_retiro" id="medio_retiro3" value="I">
      <input name="tipo_movimiento"  type="hidden"  id="tipo_movimiento" value="<%=tipo_movimiento%>"></td>
   </tr>
 </table>

  <%break;
  case 3 :
  /* retiro
 *
 * 
 */
  model.tablaGenService.buscarBanco();
%>
<input name="sw_datos_transferencia"  type="hidden"  id="sw_datos_transferencia" value="true">
<input name="tipo_movimiento"  type="hidden"  id="tipo_movimiento" value="<%=tipo_movimiento%>">
<input name="tipo_transferencia"  type="hidden"   id="tipo_transferencia" value="<%=tipo_transferencia%>">
<input name="tope"  type="hidden"  id="tope" value="<%=tope%>">

  <%if(tipo_transferencia.equals("E")){
							  %>
  
 <table align="center" style="border: green solid 1px;" width="500" id="tabla_datos2" >
   <tr class="fila">
     <th width="48%" align="left"  ><strong> Datos Transferencia </strong></th>
     <th colspan="2" align="left" > <img alt="" src="<%= BASEURL%>/images/titulo.gif" width="32" height="20" align="left"/> </th>
   </tr>
   
   <tr class="fila">
     <td >Nombre titular de la cuenta </td>
     <td width="30%"><input name="titular_cuenta" type="text" id="titular_cuenta"   /></td>
     <td width="22%"><img alt="" src="<%= BASEURL%>/images/buscar.gif" width="16" height="11"   align="left" onClick="verCuentas()"  style="cursor:pointer"/></td>
   </tr>
   <tr class="fila">
     <td>Cedula</td>
     <td colspan="2"><input name="nit_cuenta" type="text" id="nit_cuenta"    /></td>
   </tr>
   <tr class="fila">
     <td>Banco</td>
     <td colspan="2"><select id="banco" name ="banco">
    <option value="">Seleccione Un Banco</option>
                <%
                    LinkedList tblban = model.tablaGenService.getTblBanco();
                    for(int i = 0; i<tblban.size(); i++){
                       TablaGen ban = (TablaGen) tblban.get(i);
                %>
                <option value="<%=ban.getTable_code()%>">  <%=ban.getTable_code()%>   </option>
        
                <%}%> </select></td>
   </tr>
   <tr class="fila">
     <td>N&deg; de cuenta</td>
     <td colspan="2"><input name="cuenta" type="text" id="cuenta"    /></td>
   </tr>
   <tr class="fila">
     <td>Tipo de Cuenta</td>
     <td colspan="2"><select  id="tipo_cuenta" name="tipo_cuenta">
       <option value="0">Seleccione</option>
       <option value="CC">Corriente</option>
       <option value="CA">Ahorro</option>
     </select>
      <input name="tipo_transferencia2"   type="hidden" id="tipo_transferencia2" value="R"></td>
      </table>
 <%}
	 
if(tipo_transferencia.equals("C")){
/* retiro transferencia cheque */ %>
 <table align="center" style="border: green solid 1px;" width="500" id="tabla_datos2" >
   <tr>
     <th width="49%" align="left"  ><strong> Datos Cheque</strong></th>
     <th colspan="2" align="left" > <img alt="" src="<%= BASEURL%>/images/titulo.gif" width="32" height="20" align="left"/> </th>
   </tr>
   
   <tr class="fila">
     <td >Nombre beneficiario</td>
     <td width="32%"><input name="nombre_beneficiario" type="text" id="nombre_beneficiario"   /></td>
     <td width="19%"><img alt="" src="<%= BASEURL%>/images/buscar.gif" width="16" height="11"  title="Click para buscar datos de transferencias anteriores"  align="left" onClick="verCuentas()"  style="cursor:pointer"/></td>
   </tr>
   <tr class="fila">
     <td>Nit / Cedula</td>
     <td colspan="2"><input name="nit_beneficiario" type="text" id="nit_beneficiario"    /></td>
   </tr>
   <tr class="fila">
     <td>Cruzado</td>
     <td colspan="2"><select  id="cheque_cruzado" name="cheque_cruzado" onChange="validarCheuqeCruzado()">
       <option value="0">Seleccione</option>
       <option value="S">SI</option>
       <option value="N">NO</option>
     </select></td>
   </tr>
   <tr class="fila">
     <td>Primer beneficiario</td>
     <td colspan="2"><select  id="cheque_primer_beneficiario" name="cheque_primer_beneficiario" disabled>
       <option value="0">Seleccione</option>
       <option value="S">SI</option>
       <option value="N">NO</option>
      </select></td>
   </tr>
   </table>
 <%} 
	if(tipo_transferencia.equals("I")){
/* retiro transferencia interna */ %>
 <table align="center" style="border: green solid 1px;" width="500" id="tabla_datos2" >
   <tr>
     <th width="50%" align="left"  ><strong> Datos Cuenta Interna</strong></th>
     <th width="50%" align="left" > <img alt="" src="<%= BASEURL%>/images/titulo.gif" width="32" height="20" align="left"/> </th>
   </tr>
   <tr class="fila">
     <td >Nombre beneficiario captacion</td>
     <td width="50%"><input name="nombre_beneficiario_capt" type="text" id="nombre_beneficiario_capt"   /></td>
   </tr>
   </table>
 
<%}
 
 break;
  }%>





  <br/>
                <div align="center">
                    <img style="cursor:pointer" src="<%= BASEURL%>/images/botones/aceptar.gif" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" 
                    onClick="ejecutarOperacion(<%= opcion%>);    "/>
                    <img style="cursor:pointer" src="<%= BASEURL%>/images/botones/restablecer.gif" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" 
                    onClick="refrescarFormsRetiros(false,'<%=tipo_transferencia%>');"/>
                    
                    <img style="cursor:pointer" src="<%= BASEURL%>/images/botones/salir.gif" onMouseOver="botonOver(this);" onMouseOut="botonOut(this); " 
                    onClick="$('#divCrearMovimiento').dialog('close');refreshGrid()"/>
                </div>
       
                    <div id="divMensaje">
                       
                    </div>
                    
    </div>
      
    </body>
</html>
