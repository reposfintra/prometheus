<%-- 
    Document   : causacion
    Created on : 29/05/2012, 03:17:00 PM
    Author     : jpinedo
--%>



<%@page import="com.tsp.operation.model.services.CaptacionInversionistaService"%>
<%@page import="com.tsp.util.Util"%>
<%@page import="com.tsp.util.UtilFinanzas"%>
<%@page contentType="text/html" pageEncoding="ISO-8859-1"%>
<%@include file    ="/WEB-INF/InitModel.jsp"%>
<%@page import="java.util.ArrayList"%>
<%@page import="com.tsp.operation.model.beans.*"%>
<%
Usuario usuario = (Usuario)session.getAttribute("Usuario");

%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1"/>
        <title>Causacion De Interes Inversionistas</title>
        <link href="<%= BASEURL%>/css/estilostsp.css" rel='stylesheet'/>
        <script type='text/javascript' src="<%= BASEURL%>/js/boton.js"></script>
        <script type='text/javascript' src="<%= BASEURL%>/js/captaciones.js"></script>
        <script type='text/javascript' src="<%=BASEURL%>/js/validar.js"></script>
         <link href="<%= BASEURL%>/css/estilostsp.css" rel="stylesheet" type="text/css">
        <link href="<%= BASEURL%>/css/jquery/jquery-ui/jquery-ui.css" rel="stylesheet" type="text/css"/>
        <link href="<%= BASEURL%>/css/jquery/jquery.jqGrid/ui.jqgrid.css" rel="stylesheet" type="text/css"/>
          <link href="<%= BASEURL%>/css/estilosFintra.css" rel='stylesheet'/>

        <script type='text/javascript' src="<%= BASEURL%>/js/boton.js"></script>
        <script type='text/javascript' src="<%= BASEURL%>/js/jquery/jquery-ui/jquery.min.js"></script>
        <script type='text/javascript' src="<%= BASEURL%>/js/jquery/jquery-ui/jquery.ui.min.js"></script>
        <script type='text/javascript' src="<%= BASEURL%>/js/jquery/jquery.jqGrid/js/grid.locale-es.js"></script>
        <script type='text/javascript' src="<%= BASEURL%>/js/jquery/jquery.jqGrid/js/jquery.jqGrid.min.js"></script>


                <style type="text/css">
            div.ui-dialog{
                font-size:12px;            }


              .link_green{color: #063; cursor:pointer; text-decoration:underline }
  div.ui-datepicker{
                font-size:12px;
            }



        </style>


        <script>

  $(document).ready(



                function() {
                    var lastSel;

                    jQuery("#Inversionistas").jqGrid({
                        caption: "Inversionistas " ,
                        url: "<%=CONTROLLER%>?estado=Captacion&accion=Inversionista&op=19",
                        editurl: "<%=CONTROLLER%>?estado=Captacion&accion=Inversionista",
                        datatype: "json",
                        height: 'auto',
                        width: 800,
                     colNames:['subcuenta','no_transaccion','Nit','Nombre Subcuenta','Saldo','Tasa','Fecha','Intereses'],
                       colModel:[
                             {name:'no_transaccion',index:'no_transaccion', sortable:false, align:'center',key:true,hidden:true},
                                {name:'subcuenta',index:'subcuenta', sortable:false, align:'center',key:true,hidden:true},
                           {name:'nit',index:'nit', sortable:false, align:'center',key:true,width: 70},
                            {name:'nombre_subcuenta',index:'nombre_subcuenta', sortable:true, align:'center',width: 300},
               

      			    {name:'saldo_inicial',index:'saldo_inicial', sortable:false,align:'right',
			    formatter:'currency', formatoptions:{decimalSeparator:",", thousandsSeparator: ",", decimalPlaces: 2, prefix: "$ "}},
                        {name:'tasa_ea',index:'tasa_ea', sortable:false,  align:'center' ,formatter:'currency', formatoptions:{decimalSeparator:",", thousandsSeparator: ",", decimalPlaces: 4, suffix: " %"}},

                            {name:'fecha_causacion',index:'fecha', sorttype:"date",sortable:true,align:'right', formatter:'date', editable:true, formatoptions:{newformat: 'Y-m-d'},
                             editoptions: {


                                dataInit: function (elem) {
                                    $(elem).datepicker({
                                        dateFormat:"yy-mm-dd",                                                                               
                                        showOtherMonths: true,
                                        selectOtherMonths: true,
                                       onSelect: function(dateText, inst) {
                                        CalcularInteresesCausacion(lastSel);

                                       }

                                    });
                                }
                            }
                        },
                         {name:'valor_intereses',index:'valor_intereses', sortable:false, align:'right',
		            formatter:'currency', formatoptions:{decimalSeparator:",", thousandsSeparator: ",", decimalPlaces: 2, prefix: "$ "}}
		
                        ],
                        rowNum:-1,
                        rowTotal: 500000,
                        loadonce:true,
                        rownumWidth: 40,
                        gridview: true,
                        viewrecords: true,
                        hidegrid: false,
                        multiselect: true,
                        jsonReader : {
                            root:"rows",
                            repeatitems: false,
                            id: "0"
                        },              

                        onCellSelect:function(rowid,iCol,cellcontent,e){
                            if(iCol ==  7){
                         if(rowid && rowid!==lastSel){
                            jQuery('#Inversionistas').restoreRow(lastSel);
                            lastSel=rowid;
                         }
                         jQuery('#Inversionistas').jqGrid('editRow',rowid,true);
                       jQuery('#Inversionistas').editRow(rowid, true);
                        }
                        },
                        /* onCellSelect:function(rowid,iCol,cellcontent,e){
                              //  alert(jQuery('#Inversionistas').jqGrid('multiselect'));
                                if(rowid && iCol == 5){
                                    jQuery('#Inversionistas').jqGrid('editRow',rowid,true,
                                    function (rowid){
                                        jQuery("#"+rowid+"_invdate",'#Inversionistas').datepicker({
                                            dateFormat:"yy-mm-dd",
                                            changeMonth: true,
                                            changeYear: true,
                                            showOtherMonths: true,
                                            selectOtherMonths: true
                                        });
                                    });
                                    lastSel=rowid;                                 
                                }
                            },*/
                        loadError: function(xhr, status, error){
                            alert(error);
                        }
                    });
                });








 $(document).ready(function(){
                $("#VentanaCausacion").dialog({
                    autoOpen: false,
                    height: "auto",
                    width: "700",
                    modal: true,
                    autoResize: true,
                    resizable: false,
                    position: "center",
                    closeOnEscape: true


                });

            });


</script>




    </head>
    <body>
        <div id="capaSuperior" style="position:absolute; width:100%; height:100px; z-index:0; left: 0px; top: 0px;">
            <jsp:include page="/WEB-INF/toptsp.jsp?encabezado=Saldos"/>
        </div>
        <div id="capaCentral" style="position:absolute; width:100%; height:85%; z-index:0; left: 0px; top: 100px; overflow: auto;">

            <input name="BASEURL" type="hidden" id="BASEURL" value="<%=BASEURL%>">

            <input name="CONTROLLER" type="hidden" id="CONTROLLER" value="<%=CONTROLLER%>">
          <input name="id_fila" type="hidden" id="id_fila" >
          <br/>
    <table border="0"  align="center">
                  <tr>
                    <td>
                     <table id="Inversionistas" class="center_div" width="auto" ></table>
                    </td>
                  </tr>

          </table>
                 <br/><p><br/>
 <div id="VentanaCausacion" title="Causaccion De Intereses" style=" display: none" >
        <div id="resp">
        </div>
         </div>  
</p>
                <div align="center">


                        <table align="center">
                          <tr>

                            <td>
                             <img style="cursor:pointer" src="<%= BASEURL%>/images/botones/aceptar.gif" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" onClick="Causar()"/></td><td>
                            <img src="<%=BASEURL%>/images/botones/restablecer.gif"   name="imgrestablecer"align="absmiddle" id="imgrestablecer" style="cursor:pointer; display: block" onClick="refreshGridCiuTsp('<%=CONTROLLER%>');" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);"></td>
                            <td><img src="<%= BASEURL%>/images/botones/salir.gif" alt="" style="cursor:pointer" onClick="parent.close();" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);"/></td>
                          </tr>
                        </table>
              </div>




                <br/>
            <br/>
                <br/>
                <br/>
                <br/>
                <br/>
                <br/>
                <br/>






            <br>

<div id="VentanaSaldos" title="Listado Cuentas" style=" display: none" ></div>

    </div>



    </body>
</html>

