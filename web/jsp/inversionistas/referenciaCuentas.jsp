<%-- 
    Document   : registrarMovimeinto
    Created on : 23/02/2012, 05:37:05 PM
    Author     : jpinedo
--%>

<%@page import="com.tsp.operation.model.beans.MovimientosCaptaciones"%>
<%@page import="java.util.List"%>
<%@page import="java.util.TreeMap"%>
<%@page import="com.tsp.operation.model.beans.Inversionista"%>
<%@page import="java.util.ArrayList"%>
<%@page import="com.tsp.operation.model.services.CaptacionInversionistaService"%>
<%@page import="com.tsp.operation.model.beans.NitSot"%>
<%@page import="com.tsp.operation.model.beans.Usuario"%>
<%@page import="com.tsp.util.*"%>
<%@taglib uri="http://jakarta.apache.org/taglibs/input-0.90" prefix="input" %>
<%@page contentType="text/html" pageEncoding="ISO-8859-1"%>
<%@include file="/WEB-INF/InitModel.jsp"%>
<%
CaptacionInversionistaService cis = new CaptacionInversionistaService();
Usuario usuario = (Usuario)session.getAttribute("Usuario");
String age = (usuario.getId_agencia().equals("OP")?"":usuario.getId_agencia());

String nit = (request.getParameter("nit") != null) ? request.getParameter("nit") : "";
String tipo_transferencia = (request.getParameter("tipo_transferencia") != null) ? request.getParameter("tipo_transferencia") : "";
 ArrayList<MovimientosCaptaciones> lista_movimientos = new ArrayList<MovimientosCaptaciones>() ;
MovimientosCaptaciones retiro = new MovimientosCaptaciones() ;




%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1"/>
        <title>Crear inversionista</title>
        <link href="<%= BASEURL%>/css/estilostsp.css" rel='stylesheet'/>
        <script type='text/javascript' src="<%= BASEURL%>/js/captaciones.js"></script>

    </head>
        <script>
        // cargar lista cuentas  -->
	
                $(document).ready(
                function() { 
                    $("#ListaCuentas").jqGrid({
                        caption: 'Seleccione una cuenta' ,
                        url: "<%=CONTROLLER%>?estado=Captacion&accion=Inversionista&op=12&inversionista=<%=nit%>&tipo_transferencia=<%=tipo_transferencia%>",
                        datatype: "json",
                        height: '600',
                        width: 'auto',
                       colNames:['id','Tipo ','Cuenta','Nit','Titular ','Banco','Nit Beneficiario','Nombre Beneficiario','Cheque Cruzado','Primer Beneficiario'],
                       colModel:[
								 {name:'no_transaccion',index:'no_transaccion',key:true,hidden:true},
                            {name:'tipo_cuenta',index:'tipo_cuenta', sortable:false, align:'center',width: 80,hidden:<%=tipo_transferencia.equals("C")%>},
                            {name:'cuenta',index:'cuenta', sortable:false ,align:'center',key:true,width: 90,hidden:<%=tipo_transferencia.equals("C")%>},
                             {name:'nit_cuenta',index:'nit_cuenta', sortable:false,align:'center', width: 90,hidden:<%=tipo_transferencia.equals("C")%>},
                            {name:'titular_cuenta',index:'titular_cuenta', sortable:false,align:'center' ,hidden:<%=tipo_transferencia.equals("C")%>},
                            {name:'banco',index:'banco', sortable:false,align:'center',hidden:<%=tipo_transferencia.equals("C")%>},
			  {name:'nit_beneficiario',index:'nit_beneficiario', sortable:false ,align:'center',hidden:<%=tipo_transferencia.equals("E")%>},
                            {name:'nombre_beneficiario',index:'nombre_beneficiario', sortable:false ,align:'center',hidden:<%=tipo_transferencia.equals("E")%>},
                            {name:'cheque_cruzado',index:'cheque_cruzado', sortable:false ,align:'center',hidden:<%=tipo_transferencia.equals("E")%>},
                            {name:'cheque_primer_beneficiario',index:'cheque_primer_beneficiario', sortable:false ,align:'center',hidden:<%=tipo_transferencia.equals("E")%>}
							 
                        ],
                        rowNum: 500,
                        rowTotal: 500000,
                        loadonce: true,
                        rownumWidth: 40,
                        multiselect: true,
                        gridview: true,
                        pager: $('#page'),
                        viewrecords: true,
                        hidegrid: false,
			onCellSelect:function(rowid,iCol,cellcontent,e){
							if(iCol > 0)
							{
								
								asignaDatosTransferencia(rowid,'<%=tipo_transferencia%>');
							}                      
                        },
                 
                        
                        jsonReader : {
                            root:"rows",
                            repeatitems: false,
                            id: "0"
                        },
                        loadError: function(xhr, status, error){
                            alert(error);
                        }
                    });
                });




    </script>
    
    
            <style type="text/css">
            div.ui-dialog{
                font-size:12px;
            }
            div.ui-jqgrid-bdiv { overflow-y: scroll }

        </style>   
    <body>
 

                <table border="0"  align="center">
                  <tr>
                    <td>
                     <table id="ListaCuentas" class="center_div" ></table>
                    </td>
                    </tr>
                </table>
                <br/>
                <div align="center">
                  <table align="center">
                    <tr >
                      <td><img src="<%=BASEURL%>/images/botones/eliminar.gif" alt="''"   name="imgeliminar" height="21" align="absmiddle" id="imgeliminar" style="cursor:pointer; display: block" onClick="EliminarCuentasTransferencia()" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);"></td>
                    
                    
                    </tr>
                  </table>
                </div>
<br/>
                
                
                
                
                   

            <br>
                   
          
</body>
</html>
