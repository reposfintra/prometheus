<%--
    Document   : informe mensual 
    Created on : 28/05/2012, 05:37:23 PM
    Author     : jpinedo
--%>

<%@page import="java.util.Vector"%>
<%@page import="com.tsp.operation.model.services.CaptacionInversionistaService"%>
<%@page import="com.tsp.util.Util"%>
<%@page import="com.tsp.util.UtilFinanzas"%>
<%@page contentType="text/html" pageEncoding="ISO-8859-1"%>
<%@include file    ="/WEB-INF/InitModel.jsp"%>
<%@page import="java.util.ArrayList"%>
<%@page import="com.tsp.operation.model.beans.*"%>
<%
Usuario usuario = (Usuario)session.getAttribute("Usuario");
CaptacionInversionistaService cis = new CaptacionInversionistaService();
int ano=0;
int mes=0;
String fecha = Util.getFechahoy();
String m="";
if (fecha!=null) {
                    ano = Integer.parseInt(fecha.substring(0, 4));
                    mes = Integer.parseInt(fecha.substring(5, 7));
                    mes = mes -1;
                    if(mes<10)
                    m="0"+mes;
                    else
                    m=""+mes;
                   
}
%>


<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1"/>
        <title>Informe Mensual</title>
        <link href="<%= BASEURL%>/css/estilostsp.css" rel='stylesheet'/>
        <link href="<%= BASEURL%>/css/letras.css" rel='stylesheet'/>
        <link href="<%=BASEURL%>/css/default.css"       rel="stylesheet" type="text/css"/>
        <link href="<%= BASEURL%>/css/estilosFintra.css" rel='stylesheet'/>
        
        <script type='text/javascript' src="<%= BASEURL%>/js/boton.js"></script>

        <script type='text/javascript' src="<%= BASEURL%>/js/captaciones.js"></script>
        <script type='text/javascript' src="<%=BASEURL%>/js/validar.js"></script>




         <link href="<%= BASEURL%>/css/estilostsp.css" rel="stylesheet" type="text/css">
        <link href="<%= BASEURL%>/css/jquery/jquery-ui/jquery-ui.css" rel="stylesheet" type="text/css"/>
        <link href="<%= BASEURL%>/css/jquery/jquery.jqGrid/ui.jqgrid.css" rel="stylesheet" type="text/css"/>
        <script type='text/javascript' src="<%= BASEURL%>/js/drypers/CiudadDrypers.js"></script>
        <script type='text/javascript' src="<%= BASEURL%>/js/boton.js"></script>
        <script type='text/javascript' src="<%= BASEURL%>/js/jquery/jquery-ui/jquery.min.js"></script>
        <script type='text/javascript' src="<%= BASEURL%>/js/jquery/jquery-ui/jquery.ui.min.js"></script>
        <script type='text/javascript' src="<%= BASEURL%>/js/jquery/jquery.jqGrid/js/grid.locale-es.js"></script>
        <script type='text/javascript' src="<%= BASEURL%>/js/jquery/jquery.jqGrid/js/jquery.jqGrid.min.js"></script>






        <script>
       $(document).ready(

                function() {
	           cargarAnoMes();
                   $("#mes").val('<%=m%>');
                   cargarMovSocios();
                   $("#tabla_datos").styleTable();
	           $("#totales").styleTable();
                   $("#totalesPart").styleTable();
                   
                   $('#close').click(function(){
                        $('#popup').fadeOut('slow');
                        $('.popup-overlay').fadeOut('slow');
                            return false;
                    });

		});

function cargarMovSocios()
{
	
                    jQuery("#MovSocios").jqGrid({
                        caption: 'Informe Mensual Socios',
                        url: "<%=CONTROLLER%>?estado=Captacion&accion=Inversionista&op=22&periodo="+<%=ano%><%=m%>,
                        datatype: "json",
                        height: 'auto',
                        width: 890,
                        colNames:['No','Nit','Nombre Inversionista','Saldo','Porcentaje Total','Tasa','Promedio Ponderado','vp','Tipo Inversionista','Computa'],
                        colModel:[
                            {name:'id',index:'id', sortable:true,align:'center',width:'100px' },
                            {name:'nit',index:'nit', sortable:true,align:'center',width:'300px',key:true} ,
                            {name:'nombre_inversionista',index:'nombre_inversionista', sortable:true,align:'center',width:'600px' },
                            {name:'saldo_final',index:'saldo_final', sortable:true,align:'right' , sorttype:"float",
			    formatter:'currency', width:'300px', formatoptions:{decimalSeparator:".", thousandsSeparator: ",", decimalPlaces: 2}},

                            {name:'porcentaje_total',index:'porcentaje_total', sortable:true, width:'125px', align:'center' ,formatter:'currency', formatoptions:{decimalSeparator:",", thousandsSeparator: ",", decimalPlaces: 2, suffix: " %"}},
                            {name:'tasa_ea',index:'tasa_ea', sortable:true,  align:'center', width:'125px',formatter:'currency', formatoptions:{decimalSeparator:",", thousandsSeparator: ",", decimalPlaces: 2, suffix: " %"}},
                            {name:'promedio_ponderado',index:'promedio_ponderado', sortable:true,  width:'125px', align:'center' ,formatter:'currency', formatoptions:{decimalSeparator:",", thousandsSeparator: ",", decimalPlaces: 4, suffix: " %"}},
                            {name:'promedio_ponderado',index:'promedio_ponderado2', sortable:true,  width:'125px', hidden:true},
                            {name:'inversionista.tipo_inversionista',index:'tipo_inversionista', sortable:false,align:'center',width:'300px' },
                            {name:'inversionista.computa',index:'computa', sortable:false,align:'center',width:'300px' }

                        ],
                        rowNum:1000,
                        rowTotal: 500000,
                        loadonce: true,
                        rownumWidth: 40,
                        gridview: true,
                        viewrecords: true,
                        hidegrid: false,
                        multiselect: false,
                        onSelectRow:
                           function(nit) {
                            var anio = document.getElementById("ano").value;
                            var mes = document.getElementById("mes").value;
                            var periodo = anio+mes;
                            
                           
                           cargarDetalleInversionista(nit,periodo);
                           
                            $('#popup').fadeIn('slow');
                            $('.popup-overlay').fadeIn('slow');
                            $('.popup-overlay').height($(window).height());
		
                            
                            }
                        ,
                        jsonReader: {
                                    root: "rows",
                                    repeatitems: false,
                                    id: "0"
                                },
                        gridComplete:
                                        function() {
                                            var total = 0;
                                            var costo = 0;
                                            var ids = jQuery("#MovSocios").jqGrid('getDataIDs');
                                            for (var i = 0; i < ids.length; i++) {
                                                var ret = jQuery("#MovSocios").jqGrid('getRowData', ids[i]);
                                                total += eval(ret.saldo_final);
                                                costo = parseFloat(costo) + parseFloat(ret.promedio_ponderado);



                                            }
                                            if (total == 0)
                                            {
                                                document.getElementById("totales").style.display="none";

                                            } else
                                            {
                                                document.getElementById("totales").style.display = "";
                                                document.getElementById("total").innerHTML = formatear("" + total, 0);
                                                document.getElementById("costo").innerHTML = formatear(costo);

                                                var ea = (Math.pow(((costo / 100) + 1), 12)) - 1;

                                                document.getElementById("ea").innerHTML = formatear(ea * 100);
                                            }
                                            
                                            cargarMovParticular();
                                        },
                                loadError: function(xhr, status, error) {
                                    alert(error);
                                }
                            });
    }
    
    function cargarMovParticular(){                
        jQuery("#MovParticular").jqGrid({
                        caption: 'Informe Mensual Particulares',
                        url: "<%=CONTROLLER%>?estado=Captacion&accion=Inversionista&op=30&periodo="+<%=ano%><%=m%>,
                        datatype: "json",
                        height: 'auto',
                        width: 890,
                        colNames:['No','Nit','Nombre Inversionista','Saldo','Porcentaje Total','Tasa','Promedio Ponderado','vp','Tipo Inversionista','Computa'],
                        colModel:[
                            {name:'id',index:'id', sortable:true,align:'center',width:'100px' },
                            {name:'nit',index:'nit', sortable:true,align:'center',width:'300px',key:true },
                            {name:'nombre_inversionista',index:'nombre_inversionista', sortable:true,align:'center',width:'600px' },
                            {name:'saldo_final',index:'saldo_final', sortable:true,align:'right' , sorttype:"float",
			    formatter:'currency', width:'300px', formatoptions:{decimalSeparator:".", thousandsSeparator: ",", decimalPlaces: 2}},

                            {name:'porcentaje_total',index:'porcentaje_total', sortable:true, width:'130px', align:'center' ,formatter:'currency', formatoptions:{decimalSeparator:",", thousandsSeparator: ",", decimalPlaces: 2, suffix: " %"}},
                            {name:'tasa_ea',index:'tasa_ea', sortable:true,  align:'center', width:'125px',formatter:'currency', formatoptions:{decimalSeparator:",", thousandsSeparator: ",", decimalPlaces: 2, suffix: " %"}},
                            {name:'promedio_ponderado',index:'promedio_ponderado', sortable:true,  width:'125px', align:'center' ,formatter:'currency', formatoptions:{decimalSeparator:",", thousandsSeparator: ",", decimalPlaces: 4, suffix: " %"}},
                            {name:'promedio_ponderado',index:'promedio_ponderado2', sortable:true,  width:'125px', hidden:true},
                            {name:'inversionista.tipo_inversionista',index:'tipo_inversionista', sortable:false,align:'center',width:'300px' },
                            {name:'inversionista.computa',index:'computa', sortable:false,align:'center',width:'350px' }

                        ],
                        rowNum:1000,
                        rowTotal: 500000,
                        loadonce:true,
                        rownumWidth: 40,
                        gridview: true,
                        viewrecords: true,
                        hidegrid: false,
                        multiselect: false,
                    onSelectRow:
                           function(nit) {
                            var anio = document.getElementById("ano").value;
                            var mes = document.getElementById("mes").value;
                            var periodo = anio+mes;
                            
                           
                           cargarDetalleInversionista(nit,periodo);
                           
                            $('#popup').fadeIn('slow');
                            $('.popup-overlay').fadeIn('slow');
                            $('.popup-overlay').height($(window).height());
		
                            
                            }
                        ,    
                    jsonReader: {
                        root: "rows",
                        repeatitems: false,
                        id: "0"
                    },
                    gridComplete:
                            function() {
                                var totalPart = 0;
                                var costoPart = 0;
                                var idsPart = jQuery("#MovParticular").jqGrid('getDataIDs');
                                for (var i = 0; i < idsPart.length; i++) {
                                    var retPart = jQuery("#MovParticular").jqGrid('getRowData', idsPart[i]);
                                    totalPart += eval(retPart.saldo_final);
                                    costoPart = parseFloat(costoPart) + parseFloat(retPart.promedio_ponderado);



                                }
                                if (totalPart == 0)
                                {
                                    document.getElementById("totalesPart").style.display = "none";

                                } else
                                {
                                    document.getElementById("totalesPart").style.display = "";
                                    document.getElementById("totalPart").innerHTML = formatear("" + totalPart, 0);
                                    document.getElementById("costoPart").innerHTML = formatear(costoPart);

                                    var eaPart = (Math.pow(((costoPart / 100) + 1), 12)) - 1

                                    document.getElementById("eaPart").innerHTML = formatear(eaPart * 100);
                                }
                                var anio = document.getElementById("ano").value;
                                var mes = document.getElementById("mes").value;
                                var periodo = anio+mes;
                                insertarGrafico(periodo);
                            
                            },
                    loadError: function(xhr, status, error) {
                        alert(error);
                    }
                });            
                    
                    
}


function cargarDetalleInversionista(nit,periodo){ 
    
    if ($("#gview_DetalleInversionista").length) {
                                refrescarGridDetalleInversionista(periodo,nit);
                            } else {
                                function refrescarGridDetalleInversionista(periodo,nit) {
                                    jQuery("#DetalleInversionista").setGridParam({
                                datatype: 'json',
                                url: $('#CONTROLLER').val()+"?estado=Captacion&accion=Inversionista&op=31&periodo="+periodo+"&nit="+nit
                            });
                            jQuery('#DetalleInversionista').trigger("reloadGrid");
                            }
                        } 

        
                           jQuery("#DetalleInversionista").jqGrid({
                               caption: 'Detalle',
                               url: "<%=CONTROLLER%>?estado=Captacion&accion=Inversionista&op=31&nit="+nit+"&periodo="+periodo,
                               datatype: "json",
                               height: 'auto',
                               width: 800,
                               colNames:['Nombre Subcuenta','Saldo','Porcentaje Total','Tasa','Promedio Ponderado','vp'],
                               colModel:[
                                   {name:'inversionista.nombre_subcuenta',index:'nombre_subcuenta', sortable:true,align:'center',width:'400px' },
                                   {name:'saldo_final',index:'saldo_final', sortable:true,align:'right' , sorttype:"float",
                                   formatter:'currency', width:'300px', formatoptions:{decimalSeparator:".", thousandsSeparator: ",", decimalPlaces: 2}},

                                   {name:'porcentaje_total',index:'porcentaje_total', sortable:true, width:'125px', align:'center' ,formatter:'currency', formatoptions:{decimalSeparator:",", thousandsSeparator: ",", decimalPlaces: 2, suffix: " %"}},
                                   {name:'tasa_ea',index:'tasa_ea', sortable:true,  align:'center', width:'125px',formatter:'currency', formatoptions:{decimalSeparator:",", thousandsSeparator: ",", decimalPlaces: 2, suffix: " %"}},
                                   {name:'promedio_ponderado',index:'promedio_ponderado', sortable:true,  width:'125px', align:'center' ,formatter:'currency', formatoptions:{decimalSeparator:",", thousandsSeparator: ",", decimalPlaces: 4, suffix: " %"}},
                                   {name:'promedio_ponderado',index:'promedio_ponderado2', sortable:true,  width:'125px', hidden:true},

                               ],
                               rowNum:1000,
                               rowTotal: 500000,
                               loadonce: true,
                               rownumWidth: 40,
                               gridview: true,
                               viewrecords: true,
                               hidegrid: false,
                             //  pager:"#paper_grid",
                                                              
                               jsonReader: {
                                   root: "rows",
                                   repeatitems: false,
                                   id: "0"
                               },
                                       
                               loadError: function(xhr, status, error) {
                                     alert(error);
                               }
                               
                              });
                              
                              jQuery("#DetalleInversionista").trigger("reloadGrid");


}


(function ($) {
    $.fn.styleTable = function (options) {
        var defaults = {
            css: 'styleTable'
        };
        options = $.extend(defaults, options);

        return this.each(function () {

            input = $(this);
            input.addClass(options.css);


            input.find("th").addClass("ui-state-default");
            input.find("td").addClass("ui-widget-content");

            input.find("tr").each(function () {
                $(this).children("td:not(:first)").addClass("first");
                $(this).children("th:not(:first)").addClass("first");
            });
        });
    };
})(jQuery);




 $(document).ready(function(){
                $("#divSalida").dialog({
                    autoOpen: false,
                    height: "auto",
                    width: "35%",
                    modal: true,
                    autoResize: true,
                    resizable: false,
                    position: "center",
                    closeOnEscape: false


                });

            });


</script>

        <style type="text/css">
            div.ui-dialog{
                font-size:12px;            }

            .no-close.ui-dialog-titlebar-close {display: none }
            
           
            #content {
                width: 900px;
                margin: 0px auto;
                padding: 2em 1em;
            }

            #header {
                background-color: #EBE9EA;
                border: 1px solid #D2D2D2;
                border-radius: 8px 8px 8px 8px;
                margin-bottom: 10px;
                text-align: center;
                width: 900px;
                min-height: 150px;
            }

            #column-right {
                background-color: #EBE9EA;
                border: 1px solid #D2D2D2;
                border-radius: 8px 8px 8px 8px;
                float: right;
                min-height: 225px;
                margin-bottom: 10px;
                overflow: hidden;
                text-align: center;
                width: 180px;
                padding-top:10px;
            }

            #central {
                background-color: #EBE9EA;
                border: 1px solid #D2D2D2;
                border-radius: 8px 8px 8px 8px;
                float: left;
                min-height: 225px;
                margin-bottom: 10px;
                margin-right: 10px;
                width: 685px;
                padding:10px;
            }

            #footer {
                background-color: #EBE9EA;
                border: 1px solid #D2D2D2;
                border-radius: 8px 8px 8px 8px;
                margin-top: 10px;
                text-align: center;
                clear: left;
                width: 900px;
                min-height: 100px;
            }

            #popup {
                left: 0;
                position: absolute;
                top: 0;
                width: 100%;
                z-index: 1001;
            }

            .content-popup {
                margin:0px auto;
                margin-top:120px;
                position:relative;
                padding:10px;
                width:800px;
                min-height:350px;
                border-radius:4px;
                background-color:#FFFFFF;
                box-shadow: 0 2px 5px #666666;
            }

            .content-popup h2 {
                color:#48484B;
                border-bottom: 1px solid #48484B;
                margin-top: 0;
                padding-bottom: 4px;
            }

            .popup-overlay {
                left: 0;
                position: absolute;
                top: 0;
                width: 100%;
                z-index: 999;
                display:none;
                background-color: #777777;
                cursor: pointer;
                opacity: 0.7;
            }

            .close {
                position: absolute;
                right: 15px;
            }

        </style>


    </head>
    <body>
        
        <div id="capaSuperior" style="position:absolute; width:100%; height:100px; z-index:0; left: 0px; top: 0px;">
            <jsp:include page="/WEB-INF/toptsp.jsp?encabezado=Movimientos y Transacciones"/>
        </div>
        <div id="capaCentral" style="position:absolute; width:100%; height:85%; z-index:0; left: 4px; top: 97px; overflow: auto;">
            <form id="form1" name="form1" method="post" >
                <input name="BASEURL" type="hidden" id="BASEURL" value="<%=BASEURL%>">

                <input name="CONTROLLER" type="hidden" id="CONTROLLER" value="<%=CONTROLLER%>">


                <div id="contenido" align="center" style=" width: 600px; height: 300px; visibility: hidden; background-color:#F7F5F4; display: none"></div>

                <br/>
                <table  width="30%" align="center" id="tabla_datos"  class="tableTextUI">
                    <tr >

                        <th width="136"><b>A&ntilde;o</b></th>
                        <td width="418">
                            <select name="ano" id="ano" onChange= "refrescarGridInformeCierre()">
                                <option value="...">Seleccione</option>

                            </select></td>
                        <th width="418"><b>Mes</b></th>
                        <td width="418"><select name="mes" id="mes" onChange= "refrescarGridInformeCierre()" >
                                <option value="...">Seleccione</option>
                            </select>
                        </td>

                    </tr>
                </table>
                <br/>
                <table border="0"  align="center">
                    <tr>
                        <td>
                            <table id="MovSocios" class="center_div" ></table>
                        </td>
                    </tr>

                </table>
                
                <div  class="center_div">
                    <table width="800" align="center"  id="totales" class="tableTextUI">
                        <tr>
                            <th width="300"><b>Total</b></th>
                            <td width="125" align="right"><b>$<span id="total" style="text-align:right"></span></b></td>
                            <td  width="125"  align="center"><strong>100%</strong></td>
                            <th width="125"  ><b>Costo Mensual</b></th>
                            <td width="125" align="center"><strong><span id="costo"></span>%</strong></td>
                        </tr>
                        <tr>
                            <td colspan="3">&nbsp;</td>
                            <th  ><b>EA</b></th>
                            <td align="center"><b><span id="ea"></span>%</b></td>
                        </tr>
                    </table>
                </div>
                <br/>
                <br/><br/>
                <table border="0"  align="center">
                    <tr>
                        <td>
                            <table id="MovParticular" class="center_div" ></table>
                        </td>
                    </tr>

                </table>
                
                <div  class="center_div">
                    <table width="800" align="center"  id="totalesPart" class="tableTextUI">
                        <tr>
                            <th width="300"><b>Total</b></th>
                            <td width="125" align="right"><b>$<span id="totalPart" style="text-align:right"></span></b></td>
                            <td  width="125"  align="center"><strong>100%</strong></td>
                            <th width="125"  ><b>Costo Mensual</b></th>
                            <td width="125" align="center"><strong><span id="costoPart"></span>%</strong></td>
                        </tr>
                        <tr>
                            <td colspan="3">&nbsp;</td>
                            <th  ><b>EA</b></th>
                            <td align="center"><b><span id="eaPart"></span>%</b></td>
                        </tr>
                    </table>
                </div>
                <br/>
                
            <!--    <div  class="center_div">
                    <br/><br/>
                    <table border="0"  align="center">
                        <tr>
                            <td>
                                <table id="TotalInversionista" class="center_div" ></table>
                            </td>
                        </tr>

                    </table>


                    <br/>

                </div>-->
          
                <div  id="imagen" align="center" style="text-align:center">

                </div>
                <div align="center">


                    <table align="center">
                        <tr >
                            <td><img src="<%=BASEURL%>/images/botones/generarPdf.gif"   name="imgapdf" height="21" align="absmiddle" id="imgapdf" style="cursor:pointer; display: block" onClick="ExportarInformePdf()" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);"></td>

                            <td><img src="<%=BASEURL%>/images/botones/restablecer.gif"   name="imgrestablecer" height="21" align="absmiddle" id="imgrestablecer" style="cursor:pointer; display: block" onClick="refrescarGridInformeCierre();" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);"></td>
                            <td> <img style="cursor:pointer" src="<%= BASEURL%>/images/botones/salir.gif" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" onClick="parent.close();"/></td>
                        </tr>
                    </table>
                </div>


                <div id="divSalida" title="Exportacion" style=" display: block" >
                    <div id="resp"></div>
                </div>

                <br/>
                <br/>
                <br/>
                <br/>
                <br/>
                <br/>
                <br/>
                <br/>
               <br>
            </form>

</div>

        <div id="popup" style="display: none;">
            <div class="content-popup">
                <div class="close"><a href="#" id="close"><img src="images/close.png"/></a></div>
                <div>
                    <h2 style=" font-size: 12px">Detalle Subcuentas</h2>
                   <table id="DetalleInversionista" class="center_div" ></table>
                   <div id="paper_grid"></div>
                </div>
            </div>
        </div>
        <div class="popup-overlay"></div>
    </body>
</html>


