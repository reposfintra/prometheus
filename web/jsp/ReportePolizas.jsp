<%-- 
    Document   : ReportePolizas
    Created on : 1/03/2016, 10:58:17 AM
    Author     : egonzalez
--%>

<%@page contentType="text/html" pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
        <link href="./css/popup.css" rel="stylesheet" type="text/css">
        <link type="text/css" rel="stylesheet" href="./css/jquery/jquery-ui/jquery-ui.css" />
        <link type="text/css" rel="stylesheet" href="./css/TransportadorasApi.css" />
        <script type="text/javascript" src="./js/jquery/jquery.jqGrid/js/jquery.min.js"></script>
        <script type="text/javascript" src="./js/jquery-ui-1.8.5.custom.min.js"></script>   
        <script type="text/javascript" src="./js/jquery/jquery.jqGrid-4.7.0/js/grid.locale-es.js"></script>
        <script type="text/javascript" src="./js/jquery/jquery.jqGrid-4.7.0/js/jquery.jqGrid.min.js"></script>
        <script type="text/javascript" src="./js/jquery/jquery.jqGrid-4.7.0/js/jquery.tabletojson.js"></script>    
        <script type="text/javascript" src="./js/jquery/jquery.jqGrid-4.7.0/plugins/jquery.contextmenu.js"></script>
        <script type="text/javascript" href="http://www.ddginc-usa.com/spanish/editcheck.js"></script>
        <script type="text/javascript" src="./js/ReportePolizas.js"></script> 
        <title>Reporte de Polizas</title>
    </head>
    <body>
        <div id="capaSuperior" style="position:absolute; width:100%; height:100px; z-index:0; left: 0px; top: 0px;">
            <jsp:include page="/WEB-INF/toptsp.jsp?encabezado=Reporte de Polizas "/>
        </div>
        <style>
            input{    
                width: 09px;
                color: black;
            }
            .gbox_tabla_reportePlizas{
                margin-top: -12px;
            }
        </style>
    <center> 
        <div id="tablita" style="top: 170px;width: 1154px;height: 86px;" >
            <div id="encabezadotablita" style="width: 1144px;">
                <label class="titulo2tablita"><b>FILTRO DE BUSQUEDA</b></label>
            </div> 
            <!--filtros de busqueda-->
            <table id="tablainterna"style="height: 53px;"  >
                <tr>
                    <td>
                        <label>Polizas</label>
                    </td>
                    <td>
                        <select type="text" id="polizas"  style="width: 200px;"onchange="cargarComboAseguradora()" onclick="habilitarFiltros()"></select>
                    </td>
                    <td>
                        <label>Aseguradoras</label>
                    </td>
                    <td>
                        <select type="text" id="aseguradora"  style="width: 135px;"></select>
                    </td>
                    <td>
                        <label hidden>Convenio</label>
                    </td>
                    <td>
                        <select type="text" id="convenio"  style="width: 135px;"hidden ></select>
                    </td>

                    <td>
                        <label>Cliente</label>
                    </td>
                    <td>
                        <input type="text" id="cliente"  style="width: 135px;color: #0C0C0C;" class="solo-numero" maxlength="10"/>
                    </td>
                    <td>
                        <label>Estado</label>
                    </td>
                    <td>
                        <select type="text" id="estado"  style="width: 135px;color: #0C0C0C;" >
                            <option value=""></option>
                            <option value="VIGENTE">VIGENTE</option>
                            <option value="INACTIVA">INACTIVA</option>
                            <option value="PENDIENTE">PENDIENTE</option>
                        </select>
                    </td>
                    <td>
                        <div id ='botones'>
                            <button id="buscar" class="ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only" 
                                    role="button" aria-disabled="false" style="margin-top: 6px;margin-left: 6px;">
                                <span class="ui-button-text">Buscar</span>
                            </button> 
                        </div>
                    </td>
                </tr>
            </table>
        </div>

        <div id="dialogFormulario"  class="ventana" >
            <div id="tablainterna" style="width: 375px">
                <table id="tablainterna"  >
                    <tr>
                        <td>
                            <input type="text" id="tipo"  style="width: 135px;color: #0C0C0C;" readonly hidden/>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <label> Poliza</label>
                        </td>
                        <td>
                            <input type="text" id="tipo_poliza"  style="width: 225px;color: #0C0C0C;" readonly/>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <label>Cliente</label>
                        </td>
                        <td>
                            <input type="text" id="nomcliente"  style="width: 225px;color: #0C0C0C;"readonly/>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <label>Nit</label>
                        </td>
                        <td>
                            <input type="text" id="nit"  style="width: 135px;color: #0C0C0C;"readonly/>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <label>Negocio vehiculo</label>
                        </td>
                        <td>
                            <input type="text" id="neg_vehiculo"  style="width: 135px;color: #0C0C0C;"readonly/>
                        </td>
                    </tr>

                    <tr>
                        <td>
                            <label>Aseguradora</label>
                        </td>
                        <td>
                            <input type="text" id="aseguradora_asoc"  style="width: 225px;"/>
                            <select type="text" id="aseguradora_asoc2"  style="width: 225px;"></select>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <label>Afiliado</label>
                        </td>
                        <td>
                            <input type="text" id="afiliado"  style="width: 225px;color: #0C0C0C;"readonly/>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <label>Placa</label>
                        </td>
                        <td>
                            <input type="text" id="placa" onchange="conMayusculas(this)" style="width: 135px;color: #0C0C0C;" maxlength="6"/>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <label>Servicio</label>
                        </td>
                        <td>
                            <input type="text" id="servicio" onchange="conMayusculas(this)" style="width: 135px;color: #0C0C0C;"/>
                            <select type="text" id="servicio2"  style="width: 135px;">
                                <option value=""></option>
                                <option value="PUBLICO">PUBLICO</option>
                                <option value="PARTICULAR">PARTICULAR</option>
                            </select>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <label>Valor poliza</label>
                        </td>
                        <td>
                            <input type="text" id="Vlrpoliza"  style="width: 76px;color: #0C0C0C;text-align: right;" class="solo-numeric" onkeypress="return onKeyDecimal(event, this)" />
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <label>Fecha inicio</label>
                        </td>
                        <td>
                            <input type="datetime" id="fecha_inicio" name="fecha_inicio" style="width: 88px;color: #070708;" onchange="CalcularDiasVig()"readonly>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <label>Fecha fin</label>
                        </td>
                        <td>
                            <input type="datetime" id="fecha_fin" name="fecha_fin" style="width: 88px;color: #070708;"onchange="CalcularDiasVig()" readonly>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <label>Dias vigencia</label>
                        </td>
                        <td>
                            <input type="text" id="dias_vigencia"  style="width: 135px;color: #0C0C0C;" readonly/>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <label>Codigo fasecolda</label>
                        </td>
                        <td>
                            <input type="text" id="cod_fasecolda"  style="width: 135px;color: #0C0C0C;"maxlength="12" class="solo-numero" onchange="verificarFasecolda()" />
                        </td>
                        <td>
                            <img id="imagen_no" src = "/fintra/images/botones/iconos/Problem2.png"style = "margin-left: -105px; height: 23px; "onclick = "">
                        </td>
                    </tr>
                </table>
            </div>
        </div>


        <div style="position: relative;top:250px;">
            <div id="contenedor" style="width: 1517px;height: 512px; border-radius: 4px; border: 1px solid #2A88C8; background: #FFF none repeat scroll 0% 0%;">   
                <br>
                <table id="tabla_reportePlizas"></table>
                <div id="pager"></div>
                <br>

                <table style="float: left;margin-left: 10px;">  
                    <tr>
                        <td >
                            <div style="background-color: #F47787;width: 17px;height: 17px;margin-bottom: 5px;margin-top: 3px;"></div>
                        </td>
                        <td>
                            <label style="font-size: 14px;">Polizas vencidas</label>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <div style="background-color: #F2CC44;width: 17px;height: 17px;margin-bottom: 5px;margin-top: 3px;"></div>
                        </td>
                        <td>
                            <label style="font-size: 14px;">Polizas por vencer</label>
                        </td>
                    </tr>
                </table>
            </div>
        </div>

        <div id="info"  class="ventana" >
            <p id="notific"></p>
        </div>

        <div id="divSalidaEx" title="Exportacion" style=" display: block" >
            <p  id="msjEx" style=" display:  none"> Espere un momento por favor...</p>
            <center>
                <img id="imgloadEx" style="position: relative;  top: 7px; display: none " src="./images/cargandoCM.gif"/>
            </center>
            <div id="respEx" style=" display: none"></div>
        </div> 

        <div id="menu" class="ContextMenu"style="display: none">
            <ul id="listopciones">
                <li id="actualizar" >
                    <span style="font-family:Verdan">Actualizar</span>
                </li>
                <li id="renovacion" >
                    <span style="font-family:Verdan">Generar carta renovacion</span>
                </li>
                <li id="marcar" >
                    <span style="font-family:Verdan">Marcar a proceso juridico</span>
                </li>
            </ul>
        </div>
    </center>
</body>
<script>
    $(document).ready(function () {
        $('.solo-numero').keyup(function () {
            this.value = (this.value + '').replace(/[^0-9]/g, '');
        });

        $('.solo-numeric').live('blur', function (event) {
            this.value = numberConComas(this.value);
        });

        function numberConComas(x) {
            return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
        }
    });

</script>
</html>
