<%-- 
    Document   : ListarAnalistas
    Created on : 21/11/2018, 3:22:14 PM
    Author     : Roberto Parra
--%>

<%@ page session="true" %>



<!DOCTYPE html>
<html  class="no-js" >
    <head>
        
        
        <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
        <title>Administracion de Roles</title>
        <meta name="viewport" content="width=device-width, initial-scale=1.0"> 
        
    <link href='css/estilostsp.css' rel='stylesheet' type='text/css'/>
    <link href='css/estilostsp.css' rel='stylesheet' type='text/css'/>
    <link href="css/mac_os_x.css" rel="stylesheet" type="text/css"/>
    <link href="css/default.css" rel="stylesheet" type="text/css"/>
    <!-- <script src="js/negocioTrazabilidad.js" type="text/javascript"></script>-->

    <link href="css/jquery/jquery-ui/jquery-ui.css" rel="stylesheet" type="text/css"/>
    <link type="text/css" rel="stylesheet" href="css/jquery/jquery-ui/jquery-ui.css" /> 
    <link href="css/popup.css" rel="stylesheet" type="text/css" />
    
    <script type='text/javascript' src="js/jquery/jquery-ui/jquery.min.js"></script>
    <script type='text/javascript' src="js/jquery/jquery-ui/jquery.ui.min.js"></script>
    <script type="text/javascript" src="js/jquery/jquery.jqGrid-4.7.0/js/grid.locale-es.js"></script>
    <script type="text/javascript" src="js/jquery/jquery.jqGrid-4.7.0/js/jquery.jqGrid.min.js"></script>
    <script type="text/javascript" src="js/jquery/jquery.jqGrid-4.7.0/js/jquery.tabletojson.js"></script>
      
   <script type='text/javascript' src="../fintra/js/Roles.js"></script>
   

   </head>
    <body >
        
     <!--Encabezado (Inicio)-->
     <div id="capaSuperior" style="position:absolute; width:100%; z-index:0; left: 0px; top: 0px;">
         <jsp:include page="/WEB-INF/toptsp.jsp?encabezado=VER DATOS DEL NEGOCIO"/>
     </div>
     <!--Encabezado (Fin)-->
     
     
    <!--Cuerpo (Inicio)-->
    <div id="capaCentral" style="position: absolute; width: 100%; height: 557px; z-index: 0; left: 0px; top: 100px; overflow: scroll;">        

        <p>&nbsp;</p>
        <center >

     <form id="FormCrearRol" name="FormCrearRol" action="/fintra/jsp/roles/CrearRoles.jsp" autocomplete="on" method="post">                          
         
         <p class="login button"> 
            <input type="button" value="Salir" onclick="window.close();" class="ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only" /> 
            <input type="button" value="Refrescar" onclick="location.reload();" class="ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only" />
            <input class="ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only" id="btnCrearRol" name="btnCrearRol" type="submit" value="Crear Rol"  /> 
        </p>
    </form>
         <table id="TabRoles"  border="2" align="center" width="100%"></table>
         <div id="CargarTablaRoles"></div>
         </center>
    </div>
   <!--Cuerpo (Fin)-->
    </body>
</html>
    <script>
        ListarRoles("");
    </script>