<%-- 
    Document   : reporteOfertas
    Created on : 28/12/2015, 03:59:56 PM
    Author     : mariana
--%>

<%@page contentType="text/html" pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
    <head>

        <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
        <link href="./css/popup.css" rel="stylesheet" type="text/css">
        <link type="text/css" rel="stylesheet" href="./css/jquery/jquery-ui/jquery-ui.css" />
        <link type="text/css" rel="stylesheet" href="./css/TransportadorasApi.css " />
        <script type="text/javascript" src="./js/jquery/jquery.jqGrid/js/jquery.min.js"></script>
        <script type="text/javascript" src="./js/jquery-ui-1.8.5.custom.min.js"></script>   
        <script type="text/javascript" src="./js/jquery/jquery.jqGrid-4.7.0/js/grid.locale-es.js"></script>
        <script type="text/javascript" src="./js/jquery/jquery.jqGrid-4.7.0/js/jquery.jqGrid.min.js"></script>
        <script type="text/javascript" src="./js/jquery/jquery.jqGrid-4.7.0/js/jquery.tabletojson.js"></script>    
        <script type="text/javascript" src="./js/reporteOfertas.js"></script> 
        <script type="text/javascript" href="http://www.ddginc-usa.com/spanish/editcheck.js"></script>
        <script type="text/javascript" src="./js/jquery/jquery.jqGrid-4.7.0/plugins/jquery.contextmenu.js"></script>
        <title>REPORTE TRABAJOS ACEPTADOS</title>
    </head>
    <body>
        <div id="capaSuperior" style="position:absolute; width:100%; height:100px; z-index:0; left: 0px; top: 0px;">
            <jsp:include page="/WEB-INF/toptsp.jsp?encabezado=REPORTE TRABAJOS ACEPTADOS"/>
        </div>

        <style>
            input{    
                width: 5px;
                color: black;
            }

        </style>
    <center>
        <div id="tablita" style="top: 170px;width: 1362px;height: 86px;" >
            <div id="encabezadotablita" style="width: 1349px">
                <label class="titulotablita"><b>FILTRO DE BUSQUEDA</b></label>
            </div> 
            <table id="tablainterna"style="height: 53px;"  >
                <tr>
                    <td>
                        <label>Estado de oferta</label>
                    </td>
                    <td>
                        <select id="estadoOfert" style="width: 151px;color: #050505;height: 25px;margin-left: 5px;" >  </select>
                    </td>
                    <td>
                        <label>Multi-servicio</label>
                    </td>
                    <td>
                        <input type="text" id="multiservicio"  style="width: 100px" ><!--hidden ="false"-->
                    </td>
                    <td>
                        <label>Nombre cliente</label>
                    </td>
                    <td>
                        <input type="text" id="cliente"  style="width: 100px" ><!--hidden ="false"-->
                    </td>
                    <td>
                        <label>Fecha Inicio</label>
                    </td>
                    <td>
                        <input type="datetime" id="fecha" name="fecha" style="height: 20px;width: 88px;color: #070708;" readonly>
                    </td>
                    <td>
                        <label>Fecha Fin </label>
                    </td>
                    <td>
                        <input type="datetime" id="fechafin" name="fecha" style="height: 20px;width: 88px;color: #070708;" readonly>
                    </td>
                    <td>
                        <div id ='botones'>
                            <button id="limpiar" class="ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only" 
                                    role="button" aria-disabled="false" style="margin-top: 6px;margin-left: 6px;width: 107px;">
                                <span class="ui-button-text">Limpiar fechas</span>
                            </button> 
                        </div>
                    </td>
                    <td>
                        <div id ='botones'>
                            <button id="excluir" class="ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only" 
                                    role="button" aria-disabled="false" style="margin-top: 6px;margin-left: 6px;">
                                <span class="ui-button-text">Excluir</span>
                            </button> 
                        </div>
                    </td>
                    <td>
                        <div id ='botones'>
                            <button id="buscar" class="ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only" 
                                    role="button" aria-disabled="false" style="margin-top: 6px;margin-left: 6px;">
                                <span class="ui-button-text">Buscar</span>
                            </button> 
                        </div>
                    </td>
                </tr>
            </table>
        </div>

        <div style="position: relative;top: 231px;">
            <table id="tabla_ofertas" ></table>
            <div id="pager1"></div>
        </div>

        <div id="dialogMsjAcciones">
            <table id="tabla_acciones" ></table>
            <div id="pager2"></div>
        </div>

        <div id="dialogMsjdistribuciones">
            <table id="tabla_distribuciones" ></table>
            <div id="pager3"></div>
        </div>
        

          <div id="dialogMsjObservacion">
            <table id="tabla_observacion" ></table>
            <div id="pager6"></div>
        </div>
        
        <div id="dialogMsjexclusion" class="ventana">
            <table id="tablainterna" >
                <tr>
                    <td>
                        <label>Codigo cliente </label>
                    </td>
                    <td>
                        <input type="text" id="client"  style="width: 120px" >
                    </td>
                    <td>
                        <label>Multiservicio</label>
                    </td>
                    <td>
                        <input type="text" id="numos"  style="width:101px" >
                    </td>
                    <td>
                        <label>Excluidos</label>
                    </td>
                    <td>
                        <input type="checkbox" id="excluidos" >
                    </td>
                    <td>
                        <div id ='botones'>
                            <button id="buscarinfo" class="ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only" 
                                    role="button" aria-disabled="false" style="margin-top: 6px;margin-left: 6px;">
                                <span class="ui-button-text">Buscar</span>
                            </button> 
                        </div>
                    </td>
                </tr>
                <tr>
                    <td colspan="7">
                        <hr>
                    </td>
                </tr>
            </table>
            <center>
                <div id="dialogMsjbusqueda">
                    <table id="tabla_busqueda" ></table>
                    <div id="pager4"></div>
                </div>  
            </center>

        </div>

        <div id="divSalidaEx" title="Exportacion" style=" display: block" >
            <p  id="msjEx" style=" display:  none"> Espere un momento por favor...</p>
            <center>
                <img id="imgloadEx" style="position: relative;  top: 7px; display: none " src="./images/cargandoCM.gif"/>
            </center>
            <div id="respEx" style=" display: none"></div>
        </div>  

        <div id="divSalidaEx2" title="Exportacion" style=" display: block" >
            <p  id="msjEx2" style=" display:  none"> Espere un momento por favor...</p>
            <center>
                <img id="imgloadEx2" style="position: relative;  top: 7px; display: none " src="./images/cargandoCM.gif"/>
            </center>
            <div id="respEx2" style=" display: none"></div>
        </div>  

        <div id="info"  class="ventana" >
            <p id="notific">EXITO AL GUARDAR</p>
        </div>
        
        <div id="menu" class="ContextMenu"style="display: none">
            <ul id="listopciones">
                <li id="observaciones" >
                    <span style="font-family:Verdan">Observaciones/Alcance</span>
                </li>
            </ul>
        </div>
        

    </center>
</body>
</html>
