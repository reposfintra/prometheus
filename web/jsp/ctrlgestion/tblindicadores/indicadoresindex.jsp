<!--
- Autor : Ing. Tito Andrés Maturana D.
- Date  : 2 de marzo de 2006
- Copyrigth Notice : Fintravalores S.A. S.A
-->
<%--
-@(#)
--Descripcion : Pagina JSP, que muestra las opciones de agregación, modificación y eliminación de indicadores
--%>

<%-- Declaracion de librerias--%>
<%@ page session="true" %>
<%@ page import="java.util.*" %>
<%@ page import="com.tsp.operation.model.beans.*" %>
<%@page import="com.tsp.util.*"%>
<%@ include file="/WEB-INF/InitModel.jsp" %>
<%@ taglib uri="http://jakarta.apache.org/taglibs/input-0.90" prefix="input" %>
<%    
    List lista = model.indicadoresSvc.getLista();
	String rec = request.getParameter("recargarDirs");
	TreeMap metodos = model.tblgensvc.getMetodos();
	metodos.remove(" Seleccione un Item");  
    metodos.put(" Seleccione", "");
%>
<html>
<head>                
    <link href="<%= BASEURL %>/css/estilostsp.css" rel="stylesheet" type="text/css">
    <script type='text/javascript' src="<%= BASEURL %>/js/boton.js"></script>
    <script type='text/javascript' src="<%= BASEURL %>/js/reporte.js"></script>
	<script language="JavaScript" type="text/javascript" src="<%=BASEURL%>/js/finanzas/presupuesto/move.js"></script>
    <title>Conceptos De Pago</title>
    <script src='<%= BASEURL %>/js/indicadores.js'></script>
    <script src='<%= BASEURL %>/js/Validaciones.js'></script>
	<script type='text/javascript' src="<%= BASEURL %>/js/validar.js"></script>
	<script type='text/javascript' src="<%= BASEURL %>/js/validate.js"></script>
    <script>                
        var opNew  = new objetoOpcion('', '', '', '', '', '', '', '', '', '', '','');
		BASEURL    = '<%= BASEURL %>';
		CONTROLLER = '<%= CONTROLLER %>';		
        <%= model.ConceptoPagosvc.getVarJSSeparador()%>        
    </script>
</head>
<body onload="<%=(request.getParameter("reload")!=null? "parent.code.location.reload();": "")%>" onMouseMove="_Move('formNuevo');" onMouseUp="_stopMove();">
    <FORM METHOD='POST' ACTION="<%= CONTROLLER %>?estado=Indicadores&accion=Entrar&pagina=indicadoresindex.jsp&carpeta=jsp/ctrlgestion/tblindicadores" NAME='formularioNuevo' onsubmit='javascript: return validarFormulario(this);'>
    <div style="HEIGHT:170px; WIDTH:510px; TOP:20px; LEFT:50px; background-color:#EEEEEE; BORDER-RIGHT: #666666 1px solid; PADDING-RIGHT: 2px; BORDER-TOP: #666666 1px solid; PADDING-LEFT: 2px;  VISIBILITY: hidden; PADDING-BOTTOM: 2px; BORDER-LEFT: #666666 1px solid; PADDING-TOP: 2px; BORDER-BOTTOM: #666666 1px solid; POSITION: absolute; " id='formNuevo' >
        <TABLE align="center" width='100%' class="tablaInferior" border='2'>   
            <TR style='cursor: move;' ondragstart='' onMouseDown="_initMove('formNuevo');">
              <TD>                                
                    <TABLE width='100%' class="tablaInferior" cellspacing='1'>
                        <tr>
                            <td class="subtitulo1" width='50%'>Nuevo Indicador </td>
                            <td class="barratitulo" width='50%'><img src="<%=BASEURL%>/images/titulo.gif" ></td>                                                    
                        </tr>
                    </table>
                    <table width='100%' class='tablaInferior' >
                        <tr class='fila' valign="top">
                            <TD>Descripci&oacute;n</TD>
                            <TD><input name='descripcion' type='text' class='textbox' id="descripcion" value='' size="45"></TD>                                             
                            <TD rowspan='9' align=left>Tipo Opcion<br><input type='radio' name='idFolder' id='idFolder1' value='Y' checked>Subcuenta<br><input type='radio' name='idFolder' id='idFolder2' value='N'>Cuenta</TD>                  
                        </tr>                        
                        <TR class='fila'>    
                            <TD>Valor M&iacute;nimo </TD>
                            <TD><input name='vlr_min' type='text' class='textbox' id="vlr_min" value='' size="30" maxlength="10" onKeyPress="soloDigitos(event,'decNo')"></TD>                            
                        </TR>
                        <TR class='fila'>
                          <TD>Valor M&aacute;ximo </TD>
                          <TD><input name='vlr_max' type='text' class='textbox' id="vlr_max" value='' size="30" maxlength="10" onKeyPress="soloDigitos(event,'decNo')"></TD>
                        </TR>
                        <TR class='fila'>
                          <TD>Valor</TD>
                          <TD><input name='valor' type='text' class='textbox' id="valor" value='' size="30" maxlength="10" onKeyPress="soloDigitos(event,'decNo')"></TD>
                        </TR>
                        <TR class='fila'>
                          <TD>Porcentaje</TD>
                          <TD><input name='porcent' type='text' class='textbox' id="porcent" value='' size="30" maxlength="3" onKeyPress="soloDigitos(event,'decNo')"></TD>
                        </TR>
                        <TR class='fila'>
                          <TD>Metodo</TD>
                          <TD><input:select name="metodo" attributesText="class=textbox" options="<%= metodos %>" default=""/></TD>
                        </TR>
                        <TR class='fila'>
                          <TD>F&oacute;rmula</TD>
                          <TD><input name='formula' type='text' class='textbox' id="formula" value='' size="45"></TD>
                        </TR>
                        <TR class='fila'>    
                            <TD>Ejecuci&oacute;n</TD>
                            <TD><input name='ejecucion' type='text' class='textbox' id="ejecucion" value='' size="45"></TD>                            
                        </TR>
                        <TR class='fila'>
                            <td valign=center><input type=checkbox name=movera value='Mover a: ' onclick="moveradest(this.form);">Mover a:</td>
                            <td colspan='2' id="moverselect">
                                <select name='moveradestino'>
                                    <option value='0' SELECTED>RAIZ</option>
                                    <%                                                                                 
                                        List listafs = model.indicadoresSvc.getPadres();													
                                        if(listafs!=null){											
                                            for(int i=0;i<listafs.size();i++){
                                                Indicador tmp = (Indicador)(listafs.get(i));
                                                String subcuenta = "";
                                                for(int j=0;j<tmp.getNivel();j++){
                                                    subcuenta += "- ";
                                                }
                                                out.print("<option value="+tmp.getCodigo()+"> "+subcuenta+" "+tmp.getDescripcion()+"</option>");
                                            }
                                        }
                                    %>
                                </select>
                            </td>                            
                        </TR>
                    </TABLE>                                                
              </TD>
            </TR>            
      </TABLE>
	  <br>
	  <table align="center">
	  	<tr>
		  <td>
				<input type='hidden' name='Id' value=''>
				<input type='hidden' name='mover' value=''>
				<input type='hidden' name='nivel' value=''>
				<input type='hidden' name='index' value=''>
				<input type='hidden' name='Opcion' id='Opcion'>
				<input name="listaSize" type="hidden" id="listaSize" value="<%= lista.size() %>">
			  <img name='botonVariable' id='botonVariable' onmouseover="botonOver(this);" onmouseout="botonOut(this);" onclick="if(validarFormulario(formularioNuevo)) formularioNuevo.submit();"> 
			  <img src="<%= BASEURL %>/images/botones/cancelar.gif" onclick="formNuevo.style.visibility='hidden';" onmouseover="botonOver(this);" onmouseout="botonOut(this);">     
			</td>
		</tr>
  	  </table>              
      </div>
    </FORM>
<center>
<form action='<%= CONTROLLER %>?estado=Indicadores&accion=Entrar&pagina=indicadoresindex.jsp&carpeta=jsp/ctrlgestion/tblindicadores' method='post' name='FormularioListado' onsubmit='javascript: return validarListado(); '>
<TABLE align="center" width='100%' class="tablaInferior">   
    <TR>
      <TD>  
            <TABLE width='100%'class="tablaInferior">
                <TR>
                    <td class="subtitulo1" colspan='5' align=center>Indicador : <%= model.indicadoresSvc.getTrazado() %></td>
                    <td class="barratitulo"><img src="<%=BASEURL%>/images/titulo.gif" width="32" height="20"></td>                                                                
                </TR>                
                <% if (lista!=null && lista.size()>0){ %>                         
                <tr class='tblTitulo'>
                  <td width='20' align=center><input type='checkbox' name='All' onClick='jscript: selAll();'></td>
                    <td width='20' align=center>&nbsp;</td>
                    <td width='230' align=center colspan='2'>Indicador</td>
                    <td width='120' align=center>Nivel</td>
                    <td width='120' align=center>Codigo</td>
                </tr>
                <%   for (int i=0;i<lista.size();i++){
                        Indicador indc = (Indicador) lista.get(i); %>
                <tr class="<%=(i % 2 == 0 )?"filagris":"filaazul"%>" onMouseOver='cambiarColorMouse(this)' style='cursor:hand'>
                  <td><input type='checkbox' name='LOV' value='<%= indc.getCodigo() %>' onClick='jscript: actAll();'></td>
                    <td><img title='Modificar' src="<%= BASEURL %>/images/botones/iconos/modificar.gif"  name="imgsalir"  onClick="goYaNIN ('<%= i %>');/* edicion('<%= i %>', true);*/" style="cursor:hand">
							<%= (indc.getId_folder().equalsIgnoreCase("Y")? "<img title='Ver Detalles' src="+BASEURL+"/images/botones/iconos/detalles.gif width='16px'  name=imgcancelar onClick=document.location.href='"+CONTROLLER+"?estado=Indicadores&accion=Entrar&pagina=indicadoresindex.jsp&carpeta=jsp/ctrlgestion/tblindicadores&Opcion=Ver&Index="+i+"';": "") %>         </td> 
                    <td align='center'><img src="<%= BASEURL %>/images/menu-images/<%= (indc.getId_folder().equalsIgnoreCase("Y")?"menu_folder_closed.gif":"menu_link_local.gif") %>"></td>
                    <td><%= indc.getDescripcion()%></td>
                    <td align=center><%= indc.getNivel() %></td>
                    <td align=center><%= indc.getCodigo() %></td>
                </tr>
                <script>var op<%= i %> = new objetoOpcion('<%= indc.getCodigo() %>', '<%= indc.getNivel() %>', '<%= indc.getId_folder() %>','<%= indc.getDescripcion() %>','<%= indc.getVlr_min() %>','<%= indc.getVlr_max() %>', '<%= indc.getValor() %>','<%= indc.getPorcentaje() %>', '<%= indc.getMetodo() %>', '<%= indc.getFormula() %>', '<%= indc.getEjecucion() %>', '<%= indc.getCodpadre() %>');  </script>                            
<%   } %>            
<% } %>
			</TABLE>
      </TD>
    </TR>    
</table>    
<br>
<table align="center">
	<tr>
		<td>
			<input type='hidden' name='Opcion' value=''>
			<img src="<%= BASEURL %>/images/botones/agregar.gif" value='Nuevo' onmouseover="botonOver(this);" onmouseout="botonOut(this);" onclick="botonAgregar(formularioNuevo);edicion('New', false);doFieldFocus(formularioNuevo);" title='Agregar Nuevo'></img>
			<img title='Eliminar' src="<%= BASEURL %>/images/botones/eliminar.gif" value='Eliminar' onmouseover="botonOver(this);" onmouseout="botonOut(this);" onclick="(validarListado(FormularioListado))?eliminar(FormularioListado):''" class='boton'></img>
			<% 
				if(model.indicadoresSvc.getListaTraza()!=null && model.indicadoresSvc.getListaTraza().size()>0){
					out.print("<img title='Regresar' src="+BASEURL+"/images/botones/regresar.gif type='button' class='boton' onClick=document.location.href='"+CONTROLLER+"?estado=Indicadores&accion=Entrar&pagina=indicadoresindex.jsp&carpeta=jsp/ctrlgestion/tblindicadores&Opcion=Reg' value='Regresar'></img>");
				}
				else{        
					out.print("<img src="+BASEURL+"/images/botones/regresarDisable.gif type='button' class='boton' onClick='' value='Regresar'></img>"); 
				}
			%>
			<img title='Salir' src="<%= BASEURL %>/images/botones/salir.gif" onmouseover="botonOver(this);" onmouseout="botonOut(this);" onClick="parent.close()">
		</td>
	</tr>
</table>
</form>
</body>
</html>
<% if (request.getParameter("index")!=null && !request.getParameter("index").equals("")){ %>
<script>
  edicion('<%= request.getParameter("index") %>', true);
</script>
<% } %>

