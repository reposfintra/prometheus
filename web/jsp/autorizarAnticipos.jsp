<%-- 
    Document   : autorizarAnticipos
    Created on : 11/04/2017, 02:44:55 PM
    Author     : dvalencia
--%>

<%@page contentType="text/html" pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
        <title>AUTORIZACION ANTICIPOS ESTACIONES</title>
        <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
        <link href="./css/popup.css" rel="stylesheet" type="text/css">
        <link type="text/css" rel="stylesheet" href="./css/jquery/jquery-ui/jquery-ui.css" />
        <link type="text/css" rel="stylesheet" href="./css/TransportadorasApi.css" />
        <script type="text/javascript" src="./js/jquery/jquery.jqGrid/js/jquery.min.js"></script>
        <script type="text/javascript" src="./js/jquery-ui-1.8.5.custom.min.js"></script>   
        <script type="text/javascript" src="./js/jquery/jquery.jqGrid-4.7.0/js/grid.locale-es.js"></script>
        <script type="text/javascript" src="./js/jquery/jquery.jqGrid-4.7.0/js/jquery.jqGrid.min.js"></script>
        <script type="text/javascript" src="./js/jquery/jquery.jqGrid-4.7.0/js/jquery.tabletojson.js"></script>   
        <script type="text/javascript" src="./js/autorizarAnticipos.js"></script> 
    </head>
    <body>
       <div id="capaSuperior" style="position:absolute; width:100%; height:100px; z-index:0; left: 0px; top: 0px;">
            <jsp:include page="/WEB-INF/toptsp.jsp?encabezado=ANTICIPOS ESTACIONES"/>
        </div>
        <style>
            .onoffswitch-inner:before {
                content: "A";
                padding-left: 14px;
                background-color: #34A7C1; color: #FFFFFF;
            }
            .onoffswitch-inner:after {
                content: "I";
                padding-right: 11px;
                background-color: #C20F0F; color: #F5F0F0;
                text-align: right;
            }
        </style>
        <div id="capaCentral" style="position:absolute; width:100%; height:100%; z-index:0; left: 0px; top: 120px; ">
        <center>
        <div id="div_filtro" class="frm_search" style="width:450px">     
        <table width="350" border="0" cellpadding="0" cellspacing="1" class="labels" id="tbl_hys"> 
                <tr>
                  <td><table width="100%" class="tablaInferior">
                    <tr>
                      <td colspan="2" >
                        <table width="100%"  border="0" cellpadding="0" cellspacing="1" class="barratitulo">
                          <tr>
                            <td width="67%" class="subtitulo1">&nbsp;Datos del Anticipo</td>
                          </tr>
                        </table></td>
                    </tr>
                    <tr class="fila">
                      <td align="center">C�dula Conductor </td>
                      <td nowrap>
                          <input name="conductor" type="conductor" id="conductor" class="textbox" style="width:150px"/>
                      </td>
                    </tr>
                    <tr class="fila">
                      <td align="center">Placa</td>
                      <td nowrap>
                          <input name="placa" type="placa" id="placa" class="textbox"  style="width:150px"/>
                      </td>
                    </tr>
                    <tr class="fila">
                      <td align="center">Planilla</td>
                      <td nowrap>
                          <input name="planilla" type="planilla" id="planilla" class="textbox" style="width:150px"/>
                      </td>
                    </tr>
                    <tr class="fila">
                    <td align="center">
                        <button id="listar_Anticipos" class="ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only"  
                            role="button" aria-disabled="false">
                        <span class="ui-button-text">Buscar</span>
                        </button>
                    </td>
                    <td align="center">
                        <button id="limpiar" class="ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only"  > 
                            <span class="ui-button-text">Limpiar</span>
                        </button> 
                    </td>
                    </tr>
                    </table></td>
                </tr>
                
        </table>
        </div>
        <div style="position: relative;top: 10px;">
            <table id="tabla_Anticipos" ></table>
            <div id="pager"></div>
        </div>
        
        </center>
        </div>
    <script>
  
        //cargarAnticipos();
    </script>
    </body>
</html>

