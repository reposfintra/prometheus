<%-- 
    Document   : consultaLineaCredito
    Created on : 20/04/2014, 
    Author     : lcanchila - geotech
--%>

<%@page import="com.tsp.operation.model.beans.CupoCreditoBancario"%>
<%@page import="com.tsp.operation.model.services.CreditosBancariosService"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@page session    ="true"%>
<%@page errorPage  ="/error/ErrorPage.jsp"%>
<%@include file    ="/WEB-INF/InitModel.jsp"%>
<%@page import    ="java.util.*" %>
<%@page import    ="com.tsp.util.*" %>
<%@page import    ="com.tsp.operation.model.beans.Usuario" %>


<%
    Usuario usuario = (Usuario) session.getAttribute("Usuario");
    String modificar = request.getParameter("modificar");
    CreditosBancariosService cbs = new CreditosBancariosService(usuario.getBd());
    ArrayList<CupoCreditoBancario> cupos = cbs.listarCuposCreditos();
%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
        <title>Consulta de Lineas de Credito</title>
        <link href="<%=BASEURL%>/css/estilotabla.css" rel="stylesheet" type="text/css">
        <link href="<%=BASEURL%>/css/style_azul.css" rel="stylesheet" type="text/css">
        <link type="text/css" rel="stylesheet" href="./css/jquery/jquery-ui/jquery-ui.css"/>
        <script type="text/javascript" src="./js/jquery/jquery-ui/jquery.min.js"></script>
        <script type="text/javascript" src="./js/jquery/jquery-ui/jquery.ui.min.js"></script> 
        <script src="<%=BASEURL%>/js/boton.js"          type="text/javascript"></script>
        <script type='text/javascript' src="<%= BASEURL%>/js/creditoBancario.js"></script>
        <script type='text/javascript' src="<%=BASEURL%>/js/validar.js"></script>-->

        <link href="<%= BASEURL%>/css/estilostsp.css" rel="stylesheet" type="text/css"/>
        <link href="<%=BASEURL%>/css/default.css"       rel="stylesheet" type="text/css"/>

    </head>
    <script>

        $(document).ready(function() {
            $("#div_modificar").draggable({handle: "#drag_div_modificar"});

        });

    </script>

    <body>
        <div id="capaSuperior" style="position:absolute; width:100%; height:100px; z-index:0; left: 0px; top: 0px;">
            <jsp:include page="/WEB-INF/toptsp.jsp?encabezado=Manejo de cupos de creditos bancarios"/>
        </div>
        <div id="capaCentral" style="position:absolute; width:100%; height:85%; z-index:0; left: 0px; top: 100px; overflow: auto;">
            <form id="form1" action="">
                <br/>
                <div id="contenido" align="center" style=" width: 600px; height: 300px; visibility: hidden; background-color:#F7F5F4; display: none"></div>
                <input type="hidden" id="reload" value="true"/>
                <table border="2" align="center" width="400">
                    <tr>
                        <td>
                            <table class="tablaInferior" border="0" width="100%">
                                <tr class="Titulos">
                                    <td style=" width: 60%">CUPOS DE CREDITO BANCARIO </td>
                                    <td style=" width: 40%">CUENTA ASOCIADA</td>
                                </tr>
                                <%for (int i = 0; i < cupos.size(); i++) {%>
                                <tr class='fila' align='center' style='cursor: pointer' onMouseOver='cambiarColorMouse(this)' onclick="editarCupoCredito('<%=BASEURL%>', 'EDITAR', '<%=i%>')" >
                                    <td align="left">
                                        <%=cupos.get(i).getNombre()%>
                                        <input type="hidden" id="cupo<%=i%>" value="<%=cupos.get(i).getNombre()%>" />
                                        <input type="hidden" id="idCupo<%=i%>" value="<%=cupos.get(i).getId()%>" />
                                    </td>
                                    <td align="left" style=" width: 80%">
                                        <%=cupos.get(i).getCuenta()%>
                                    </td>
                                </tr>
                                <%}%>
                            </table>
                        </td>
                    </tr>
                </table>
                <br/>
                <br/>
                <div align="center">
                    <%if (modificar != null && modificar.equals("SI")) {%>
                    <img alt="" src = "<%=BASEURL%>/images/botones/nuevo.gif" style = "cursor:pointer" name = "imgaceptar" id="imgaceptar"  onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" onclick='editarCupoCredito("<%=BASEURL%>", "NUEVO")'/>
                    <%}%>
                    <img alt="" src = "<%=BASEURL%>/images/botones/restablecer.gif" style = "cursor:pointer" name = "imgsalir" onClick = "location.reload()" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);"/>
                    <img alt="" src="<%=BASEURL%>/images/botones/salir.gif" style="cursor:pointer" name="imgsalir" id="imgsalir" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" onclick='parent.close();'/>
                </div>
            </form>
        </div>

        <div id="div_modificar" style="display:none;z-index:100; position:absolute;border: 1px solid #165FB6; background:#FFFFFF; padding: 3px 3px 3px 3px; margin: 15% auto;margin-left: 40% ;"></div>


    </body>
</html>
