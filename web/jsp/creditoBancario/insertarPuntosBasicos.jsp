<%-- 
    Document   : insertarPuntosBasicos
    Created on : 23/11/2011, 04:07:13 PM
    Author     : darrieta
--%>
    
<%@page import="com.tsp.operation.model.beans.LineaCredito"%>
<%@page import="com.tsp.operation.model.beans.Bancos"%>
<%@page import="com.tsp.operation.model.services.BancosService"%>
<%@page import="com.tsp.operation.model.beans.TablaGen"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@page session    ="true"%>
<%@page errorPage  ="/error/ErrorPage.jsp"%>
<%@include file    ="/WEB-INF/InitModel.jsp"%>
<%@page import    ="java.util.*" %>
<%@page import    ="com.tsp.operation.model.beans.Usuario" %>
<%@page import="com.tsp.operation.model.beans.PuntosBasicosBanco"%>
<%@page import="com.tsp.operation.model.services.CreditosBancariosService"%>
<%
Usuario usuario = (Usuario) session.getAttribute("Usuario");
CreditosBancariosService cbs = new CreditosBancariosService(usuario.getBd());
ArrayList<LineaCredito> lineas = cbs.listarLineasCredito();

BancosService bs = new BancosService(usuario.getBd());
ArrayList<Bancos> bancos = bs.obtenerBancosNit();

String opcion = request.getParameter("opcion");
String banco = request.getParameter("banco");
String linea = request.getParameter("linea");
String puntosBasicos = request.getParameter("ptoBasicos");

%>
<table border="2" align="center" width="95%">
    <tr>
        <td>
            <table class="tablaInferior" border="0" width="100%" style="text-align: left">
                <tr>
                    <td width="50%" align="left" class="subtitulo1">&nbsp;Puntos b&aacute;sicos</td>
                    <td width="50%" align="left" class="barratitulo">
                        <img alt="" src="<%= BASEURL%>/images/titulo.gif" width="32" height="20"  align="left"/>
                    </td>
                </tr>
                <tr class='fila'>
                    <td>Banco</td>
                    <td>
                        <select id="banco" name="banco" <%=opcion.equals("EDITAR")?"disabled":""%>>
                            <option value="" selected="selected">Seleccione</option>
                            <%for (int i = 0; i < bancos.size(); i++) {%>
                            <option value="<%=bancos.get(i).getCodigo()%>" <%=(banco!=null && banco.equals(bancos.get(i).getCodigo()))?"selected":""%> ><%=bancos.get(i).getNombre()%></option>
                            <%}%>
                        </select>
                    </td>
                </tr>
                <tr class='fila'>
                    <td>Linea de cr&eacute;dito</td>
                    <td>
                        <select id="linea" name="linea" <%=opcion.equals("EDITAR")?"disabled":""%>>
                            <option value="" selected="selected">Seleccione</option>
                            <%for (int i = 0; i < lineas.size(); i++) {%>
                            <option value="<%=lineas.get(i).getLinea() %>" <%=(linea!=null && linea.equals(lineas.get(i).getLinea()))?"selected":""%> ><%=lineas.get(i).getLinea()%></option>
                            <%}%>
                        </select>
                    </td>
                </tr>
                <tr class='fila'>
                    <td>Puntos b&aacute;sicos</td>
                    <td>
                        <input type="text" name="ptoBasicos" id="ptoBasicos" onkeyup="soloNumerosformateado(this.id);" size="3" value="<%=puntosBasicos!=null?puntosBasicos:""%>"/>%
                    </td>
                </tr>
            </table>
        </td>
    </tr>
</table>
<br/>
<br/>
<div align="center">
    <img style="cursor:hand" src="<%= BASEURL%>/images/botones/aceptar.gif" onmouseover="botonOver(this);" onmouseout="botonOut(this);" onClick="guardarPuntosBasicos('<%=CONTROLLER%>', '<%=opcion%>');"/>
    <img alt="" src="<%=BASEURL%>/images/botones/salir.gif" style="cursor:pointer" name="imgsalir" id="imgsalir" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" onclick='parent.closewin();'/>
</div>