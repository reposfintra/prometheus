<%-- 
    Document   : insertarLineaCredito
    Created on : 20/04/2014, 
    Author     : lcanchila - geotech
--%>
    
<%@page import="com.tsp.operation.model.services.CreditosBancariosService"%>
<%@page import="com.tsp.operation.model.beans.Cmc"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@page session    ="true"%>
<%@page errorPage  ="/error/ErrorPage.jsp"%>
<%@include file    ="/WEB-INF/InitModel.jsp"%>
<%@page import    ="java.util.*" %>
<%@page import    ="com.tsp.operation.model.beans.Usuario" %>
<%
Usuario usuario = (Usuario) session.getAttribute("Usuario");
String opcion = request.getParameter("opcion");
String linea = request.getParameter("linea");
String idLinea = request.getParameter("idLinea");
CreditosBancariosService cbs = new CreditosBancariosService(usuario.getBd());
ArrayList<Cmc> listaCmc = cbs.listarCmc(); 

%>
<table class="tablaInferior" border="0" width="100%" style="text-align: left" >
    <tr>
        <td width="50%" align="left" class="subtitulo1">&nbsp;L&iacute;neas de Cr&eacute;dito</td>
        <td width="50%" align="left" class="barratitulo">
            <img alt="" src="<%= BASEURL%>/images/titulo.gif" width="32" height="20"  align="left"/>
        </td>
    </tr>
    <tr class='fila'>
        <td>Linea de Cr&eacute;dito</td>
        <td>
            <input type="text" name="linea" id="linea" value="<%=linea != null ? linea : ""%>" style=" width: 70%"/>
            <input type="hidden" id="idLinea" value="<%=idLinea%>" />
        </td>
    </tr>
    <tr class="fila">
        <td>HC Asociado</td>
        <td>
            <select class="element select medium" id="hc" name="hc" >
                <option value="" selected="selected">Seleccione</option>
                <%for (int i = 0; i < listaCmc.size(); i++) {%>
                <option value="<%=listaCmc.get(i).getCmc()%>" ><%=listaCmc.get(i).getCmc() + " " + listaCmc.get(i).getCuenta()%></option>
                <%}%>
            </select>
        </td>
    </tr>
</table>
<br/>
<br/>
<div align="center">
    <img style="cursor:hand" src="<%= BASEURL%>/images/botones/aceptar.gif" onmouseover="botonOver(this);" onmouseout="botonOut(this);" onClick="guardarLineaCredito('<%=CONTROLLER%>', '<%=opcion%>');"/>
    <img alt="" src="<%=BASEURL%>/images/botones/salir.gif" style="cursor:pointer" name="imgsalir" id="imgsalir" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" onclick='parent.closewin();'/>
</div>