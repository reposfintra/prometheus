<%-- 
    Document   : filtroCredito
    Created on : 6/01/2012, 10:15:40 AM
    Author     : darrieta
--%>

<%@page contentType="text/html" pageEncoding="ISO-8859-1"%>
<%@include file    ="/WEB-INF/InitModel.jsp"%>
<%@page import="java.util.ArrayList"%>
<%@page import="com.tsp.operation.model.beans.*"%>
<%@page import="com.tsp.operation.model.services.BancosService"%>
<!DOCTYPE html>
<%
Usuario usuario = (Usuario) session.getAttribute("Usuario");
BancosService bs = new BancosService(usuario.getBd());
ArrayList<Bancos> bancos = bs.obtenerBancosNacionalidad(true);
if(request.getParameter("pago")!=null){
    session.setAttribute("EDITAR_CRBANCARIO", "true");
}else{
    session.setAttribute("EDITAR_CRBANCARIO", "false");
}
%>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1"/>
        <title>Buscar credito bancario</title>
        <link href="<%= BASEURL%>/css/estilostsp.css" rel='stylesheet'/>
        <script type='text/javascript' src="<%= BASEURL%>/js/boton.js"></script>
        <script type='text/javascript' src="<%= BASEURL%>/js/prototype.js"></script>
        <script type='text/javascript' src="<%=BASEURL%>/js/validar.js"></script>
        <script type='text/javascript' src="<%= BASEURL%>/js/creditoBancario.js"></script>
        <!-- Las siguientes librerias CSS y JS son para el manejo del calendario -->
        <link rel="stylesheet" type="text/css" href="<%=BASEURL%>/css/jCal/jscal2.css" />
        <link rel="stylesheet" type="text/css" href="<%=BASEURL%>/css/jCal/border-radius.css" />
        <script type="text/javascript" src="<%=BASEURL%>/js/jCal/jscal2.js"></script>
        <script type="text/javascript" src="<%=BASEURL%>/js/jCal/lang/es.js"></script>
    </head>
    <body>
        <div id="capaSuperior" style="position:absolute; width:100%; height:100px; z-index:0; left: 0px; top: 0px;">
            <jsp:include page="/WEB-INF/toptsp.jsp?encabezado=Manejo de creditos bancarios"/>
        </div>
        <div id="capaCentral" style="position:absolute; width:100%; height:85%; z-index:0; left: 0px; top: 100px; overflow: auto;">
            <form id="form1" name="form1" method="post" action="<%=CONTROLLER%>?estado=Credito&accion=Bancario&opcion=FILTRO_CREDITOS">
                <br/>
                <input type="hidden" name="CONTROLLER" id="CONTROLLER" value="<%=CONTROLLER%>"/>
                <table border="2" align="center" width="400">
                    <tr><td>
                        <table class="tablaInferior" border="0" width="100%">
                            <tr>
                                <td width="50%" align="left" class="subtitulo1">Buscar cr&eacute;dito bancario</td>
                                <td width="50%" align="left" class="barratitulo" colspan="2">
                                    <img alt="" src="<%= BASEURL%>/images/titulo.gif" width="32" height="20" align="left"/>
                                </td>
                            </tr>
                            <tr class="fila">
                                <td>Tipo de cr&eacute;dito</td>
                                <td>
                                    <label>
                                        <input onchange="cargarBancosNacionalidad(true)" id="rdoNacional" type="radio" name="tipo" checked="checked" value="N"/> Nacional
                                    </label>
                                    <label>
                                        <input onchange="cargarBancosNacionalidad(false)" id="rdoExtranjero" type="radio" name="tipo" value="E"/> Extranjero
                                    </label>
                                </td>
                            </tr>
                            <tr class="fila">
                                <td>Banco</td>
                                <td>
                                    <select class="element select medium" id="banco" name="banco" style="width: 240px">
                                        <option value="" selected="selected">Todos</option>
                                        <%for(int i=0; i<bancos.size(); i++){%>
                                            <option value="<%=bancos.get(i).getNit()%>" id="<%=bancos.get(i).getCodigo()%>" ><%=bancos.get(i).getNombre()%></option>
                                        <%}%>
                                    </select>
                                </td>
                            </tr>
                            <tr class="fila">
                                <td>Vigente</td>
                                <td>
                                    <input onclick="mostrarFechas()" type="checkbox" checked="checked" id="chkVigente" name="chkVigente" value="Vigente" />
                                </td>
                            </tr>
                            <tr id="trRango" style="display:none" class="fila">
                                <td colspan="2">Rango de fechas en que inici&oacute; el cr&eacute;dito:</td>
                            </tr>
                            <tr id="trFechaInicial" style="display:none" class="fila">
                                <td>Fecha inicial</td>
                                <td>
                                    <input type="text" name="fechaInicial" id="fechaInicial" readonly />
                                    <img style="cursor:pointer" src="<%=BASEURL%>/js/Calendario/cal.gif" id="imgFechaInicial" />
                                </td>
                            </tr>
                            <tr id="trFechaFinal" style="display:none" class="fila">
                                <td>Fecha final</td>
                                <td>
                                    <input type="text" name="fechaFinal" id="fechaFinal" readonly />
                                    <img style="cursor:pointer" src="<%=BASEURL%>/js/Calendario/cal.gif" id="imgFechaFinal" />
                                </td>
                            </tr>
                        </table>
                    </td></tr>
                </table>
                <br/>
                <br/>
                <div align="center">
                    <img style="cursor:pointer" src="<%= BASEURL%>/images/botones/aceptar.gif" onmouseover="botonOver(this);" onmouseout="botonOut(this);" onClick="validarFiltros()"/>
                    <img alt="" src = "<%=BASEURL%>/images/botones/restablecer.gif" style = "cursor:pointer" name = "imgsalir" onClick = "location.href='<%=request.getRequestURI()%>'" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);"/>
                    <img alt="" src="<%=BASEURL%>/images/botones/salir.gif" style="cursor:pointer" name="imgsalir" id="imgsalir" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" onclick='parent.close();'/>
                </div>
            </form>
            <%if(request.getParameter("mensaje")!=null && !request.getParameter("mensaje").equals("")){%>
            <div id="divMensaje">
                <table border="2" align="center">
                    <tr>
                        <td>
                            <table width="100%"  border="0" align="center"  bordercolor="#f7f5f4" bgcolor="#ffffff">
                                <tr>
                                    <td width="229" align="center" class="mensajes"><%=request.getParameter("mensaje")%></td>
                                    <td width="29" background="<%=BASEURL%>/images/cuadronaranja.JPG">&nbsp;&nbsp;</td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
            </div>
            <%}%>
        </div>
        <script type="text/javascript">
            Calendar.setup({
                inputField : "fechaInicial",
                trigger    : "imgFechaInicial",
                onSelect   : function() { this.hide() }
            });
            Calendar.setup({
                inputField : "fechaFinal",
                trigger    : "imgFechaFinal",
                onSelect   : function() { this.hide() }
            });
        </script>
    </body>
</html>
