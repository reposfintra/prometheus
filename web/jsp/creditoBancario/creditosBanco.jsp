<%-- 
    Document   : creditosBanco
    Created on : 10/01/2012, 05:07:04 PM
    Author     : darrieta
--%>

<%@page import="com.tsp.operation.model.services.CreditosBancariosService"%>
<%@page import="com.tsp.util.UtilFinanzas"%>
<%@page contentType="text/html" pageEncoding="ISO-8859-1"%>
<%@include file    ="/WEB-INF/InitModel.jsp"%>
<%@page import="java.util.ArrayList"%>
<%@page import="com.tsp.operation.model.beans.*"%>
<%
Usuario usuario = (Usuario) session.getAttribute("Usuario");
CreditosBancariosService cbs = new CreditosBancariosService(usuario.getBd());
String nit = request.getParameter("nit");
boolean vigente = Boolean.parseBoolean(request.getParameter("vigente"));
ArrayList<CreditoBancario> saldosBancos = cbs.consultarCreditosBanco(nit, vigente);
%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
        <title>Creditos banco</title>
        <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
        <title>Buscar credito bancario</title>
        <link href="<%= BASEURL%>/css/estilostsp.css" rel='stylesheet'/>
        <script type='text/javascript' src="<%= BASEURL%>/js/boton.js"></script>
        <script type='text/javascript' src="<%= BASEURL%>/js/prototype.js"></script>
        <script type='text/javascript' src="<%= BASEURL%>/js/creditoBancario.js"></script>
    </head>
    <body>
        <div id="capaSuperior" style="position:absolute; width:100%; height:100px; z-index:0; left: 0px; top: 0px;">
            <jsp:include page="/WEB-INF/toptsp.jsp?encabezado=Manejo de creditos bancarios"/>
        </div>
        <div id="capaCentral" style="position:absolute; width:100%; height:85%; z-index:0; left: 0px; top: 100px; overflow: auto;">
            <input type="hidden" id="BASEURL" value="<%=BASEURL%>" />
            <input type="hidden" id="nit" value="<%=nit%>" />
            <table border="2" align="center" width="650">
                <tr><td>
                    <table class="tablaInferior" border="0" width="100%">
                        <tr>
                            <td width="50%" align="left" class="subtitulo1" colspan="2">Consulta cr&eacute;ditos bancarios <%=request.getParameter("nombreBanco")%></td>
                            <td width="50%" align="left" class="barratitulo" colspan="2">
                                <img alt="" src="<%= BASEURL%>/images/titulo.gif" width="32" height="20" align="left"/>
                            </td>
                        </tr>
                        <tr class="Titulos">
                            <td width="20%">Documento</td>
                            <td width="30%">Linea de Credito</td>
                            <td width="30%">Cupo Credito</td>
                            <td width="20%">Valor total</td>
                        </tr>
                        <%for(int i=0; i<saldosBancos.size(); i++){%>
                        <tr class="fila" style='cursor: pointer' onMouseOver='cambiarColorMouse(this)' onclick="verLiquidacionCredito('<%=saldosBancos.get(i).getDocumento()%>')" >
                            <td><%=saldosBancos.get(i).getDocumento()%></td>
                            <td><%=saldosBancos.get(i).getLinea_credito()%></td>
                            <td><%=saldosBancos.get(i).getCupo()%></td>
                            <td><%=UtilFinanzas.customFormat("#,###.##",saldosBancos.get(i).getVlr_credito(),2)%></td>
                        </tr>
                        <%}%>
                    </table>
                </td></tr>
            </table>
            <br/>
            <div align="center">
                <img style="cursor:pointer" src="<%= BASEURL%>/images/botones/regresar.gif" onmouseover="botonOver(this);" onmouseout="botonOut(this);" onClick="history.back(1)"/>
                <img alt="" src="<%=BASEURL%>/images/botones/salir.gif" style="cursor:pointer" name="imgsalir" id="imgsalir" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" onclick='parent.close();'/>
            </div>
        </div>
    </body>
</html>
