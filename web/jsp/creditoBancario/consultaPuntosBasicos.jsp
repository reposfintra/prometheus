<%-- 
    Document   : consultaPuntosBasicos
    Created on : 23/11/2011, 10:53:34 AM
    Author     : darrieta - geotech
--%>

<%@page import="com.tsp.operation.model.beans.PuntosBasicosBanco"%>
<%@page import="com.tsp.operation.model.services.CreditosBancariosService"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@page session    ="true"%>
<%@page errorPage  ="/error/ErrorPage.jsp"%>
<%@include file    ="/WEB-INF/InitModel.jsp"%>
<%@page import    ="java.util.*" %>
<%@page import    ="com.tsp.util.*" %>
<%@page import    ="com.tsp.operation.model.beans.Usuario" %>


<%
    Usuario usuario = (Usuario) session.getAttribute("Usuario");
    String id = request.getParameter("id");
    String modificar = request.getParameter("modificar");
    CreditosBancariosService cbs = new CreditosBancariosService(usuario.getBd());
    ArrayList<PuntosBasicosBanco> bancos = cbs.listarPuntosBasicos();
%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
        <title>Consulta de puntos básicos</title>
        <script src="<%=BASEURL%>/js/prototype.js"      type="text/javascript"></script>
        <script src="<%=BASEURL%>/js/boton.js"          type="text/javascript"></script>
        <script src="<%=BASEURL%>/js/effects.js"        type="text/javascript"></script>
        <script src="<%=BASEURL%>/js/window.js"         type="text/javascript"></script>
        <script src="<%=BASEURL%>/js/window_effects.js" type="text/javascript"></script>
        <script type='text/javascript' src="<%= BASEURL%>/js/creditoBancario.js"></script>
        <script type='text/javascript' src="<%=BASEURL%>/js/validar.js"></script>

        <link href="<%= BASEURL%>/css/estilostsp.css" rel="stylesheet" type="text/css"/>
        <link href="<%=BASEURL%>/css/default.css"       rel="stylesheet" type="text/css"/>

    </head>
    <body>
        <div id="capaSuperior" style="position:absolute; width:100%; height:100px; z-index:0; left: 0px; top: 0px;">
            <jsp:include page="/WEB-INF/toptsp.jsp?encabezado=Manejo de creditos bancarios"/>
        </div>
        <div id="capaCentral" style="position:absolute; width:100%; height:85%; z-index:0; left: 0px; top: 100px; overflow: auto;">
            <form id="form1" action="">
                <br/>
                <div id="contenido" align="center" style=" width: 600px; height: 300px; visibility: hidden; background-color:#F7F5F4; display: none"></div>
                <input type="hidden" id="reload" value="true"/>
                <table border="2" align="center" width="600">
                    <tr>
                        <td>
                            <table class="tablaInferior" border="0" width="100%">
                                <tr>
                                    <td width="50%" align="left" class="subtitulo1">&nbsp;Puntos b&aacute;sicos</td>
                                    <td width="50%" align="left" class="barratitulo" colspan="2">
                                        <img alt="" src="<%= BASEURL%>/images/titulo.gif" width="32" height="20"  align="left"/>
                                    </td>
                                </tr>
                                <tr class="Titulos">
                                    <td>BANCO</td>
                                    <td>LINEA DE CR&Eacute;DITO</td>
                                    <td>PUNTOS B&Aacute;SICOS</td>
                                </tr>
                                <%for (int i = 0; i < bancos.size(); i++) {%>
                                    <tr class='fila' align='center' style='cursor: pointer' onMouseOver='cambiarColorMouse(this)' <%=(modificar!=null && modificar.equals("SI"))?"onclick='editarPuntosBasicos(\""+BASEURL+"\", \"EDITAR\", \""+i+"\")'":""%> >
                                        <td align="left">
                                            <%=bancos.get(i).getNombreBanco()%>
                                            <input type="hidden" id="banco<%=i%>" value="<%=bancos.get(i).getBanco()%>" />
                                        </td>
                                        <td align="left">
                                            <%=bancos.get(i).getDescripcionLinea()%>
                                            <input type="hidden" id="linea<%=i%>" value="<%=bancos.get(i).getLineaCredito()%>" />
                                        </td>
                                        <td>
                                            <%=bancos.get(i).getPuntosBasicos()%>%
                                            <input type="hidden" id="ptoBasicos<%=i%>" value="<%=bancos.get(i).getPuntosBasicos()%>" />
                                        </td>
                                    </tr>
                                <%}%>
                            </table>
                        </td>
                    </tr>
                </table>
                <br/>
                <br/>
                <div align="center">
                    <%if (modificar != null && modificar.equals("SI")) {%>
                    <img alt="" src = "<%=BASEURL%>/images/botones/nuevo.gif" style = "cursor:pointer" name = "imgaceptar" id="imgaceptar"  onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" onclick='editarPuntosBasicos("<%=BASEURL%>", "NUEVO")'/>
                    <%}%>
                    <img alt="" src = "<%=BASEURL%>/images/botones/restablecer.gif" style = "cursor:pointer" name = "imgsalir" onClick = "location.reload()" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);"/>
                    <img alt="" src="<%=BASEURL%>/images/botones/salir.gif" style="cursor:pointer" name="imgsalir" id="imgsalir" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" onclick='parent.close();'/>
                </div>
            </form>
        </div>
    </body>
</html>
