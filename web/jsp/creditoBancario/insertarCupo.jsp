<%-- 
    Document   : insertarCupo
    Created on : 23/11/2011, 04:07:13 PM
    Author     : darrieta
--%>
    
<%@page import="com.tsp.operation.model.beans.CupoCreditoBancario"%>
<%@page import="com.tsp.operation.model.beans.Bancos"%>
<%@page import="com.tsp.operation.model.services.BancosService"%>
<%@page import="com.tsp.operation.model.beans.TablaGen"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@page session    ="true"%>
<%@page errorPage  ="/error/ErrorPage.jsp"%>
<%@include file    ="/WEB-INF/InitModel.jsp"%>
<%@page import    ="java.util.*" %>
<%@page import    ="com.tsp.operation.model.beans.Usuario" %>
<%@page import="com.tsp.operation.model.beans.PuntosBasicosBanco"%>
<%@page import="com.tsp.operation.model.services.CreditosBancariosService"%>
<%
 Usuario usuario = (Usuario) session.getAttribute("Usuario");
 CreditosBancariosService cbs = new CreditosBancariosService(usuario.getBd());
 ArrayList<CupoCreditoBancario> cupos = cbs.listarCuposCreditos();

BancosService bs = new BancosService(usuario.getBd());
ArrayList<Bancos> bancos = bs.obtenerBancosNit();

String opcion = request.getParameter("opcion");
String banco = request.getParameter("banco");
String linea = request.getParameter("linea");
String cupo = request.getParameter("cupo");

%>
<table class="tablaInferior" border="0" width="100%" style="text-align: left" >
    <tr>
        <td width="50%" align="left" class="subtitulo1">&nbsp;Cupos</td>
        <td width="50%" align="left" class="barratitulo">
            <img alt="" src="<%= BASEURL%>/images/titulo.gif" width="32" height="20"  align="left"/>
        </td>
    </tr>
    <tr class='fila'>
        <td>Banco</td>
        <td>
            <select id="banco" name="banco" <%=opcion.equals("EDITAR")?"disabled":""%>>
                <option value="" selected="selected">Seleccione</option>
                <%for (int i = 0; i < bancos.size(); i++) {%>
                <option value="<%=bancos.get(i).getCodigo()%>" <%=(banco!=null && banco.equals(bancos.get(i).getCodigo()))?"selected":""%> ><%=bancos.get(i).getNombre()%></option>
                <%}%>
            </select>
        </td>
    </tr>
    <tr class='fila'>
        <td>Tipo de Cupo</td>
        <td>
            <select id="linea" name="linea" <%=opcion.equals("EDITAR")?"disabled":""%>>
                <option value="" selected="selected">Seleccione</option>
                <%for (int i = 0; i < cupos.size(); i++) {%>
                <option value="<%=cupos.get(i).getNombre()%>" <%=(linea!=null && linea.equals(cupos.get(i).getNombre()))?"selected":""%> ><%=cupos.get(i).getNombre()%></option>
                <%}%>
            </select>
        </td>
    </tr>
    <tr class='fila'>
        <td>Valor Cupo</td>
        <td>
            <input type="text" name="cupo" id="cupo" onkeyup="soloNumerosformateado(this.id);" value="<%=cupo!=null?cupo:""%>"/>
        </td>
    </tr>
</table>
</td>
</tr>
</table>
<br/>
<br/>
<div align="center">
<img style="cursor:hand" src="<%= BASEURL%>/images/botones/aceptar.gif" onmouseover="botonOver(this);" onmouseout="botonOut(this);" onClick="guardarCupo('<%=CONTROLLER%>', '<%=opcion%>');"/>
<img alt="" src="<%=BASEURL%>/images/botones/salir.gif" style="cursor:pointer" name="imgsalir" id="imgsalir" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" onclick='parent.closewin();'/>
</div>