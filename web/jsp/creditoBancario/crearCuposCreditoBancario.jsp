<%-- 
    Document   : consultaLineaCredito
    Created on : 20/04/2014 
    Author     : lcanchila - geotech
--%>

<%@page import="com.tsp.finanzas.contab.model.beans.Cuentas"%>
<%@page import="com.tsp.operation.model.services.CreditosBancariosService"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@page session    ="true"%>
<%@page errorPage  ="/error/ErrorPage.jsp"%>
<%@include file    ="/WEB-INF/InitModel.jsp"%>
<%@page import    ="java.util.*" %>
<%@page import    ="com.tsp.operation.model.beans.Usuario" %>
<%
    Usuario usuario = (Usuario) session.getAttribute("Usuario");
    CreditosBancariosService cbs = new CreditosBancariosService(usuario.getBd());
    String opcion = request.getParameter("opcion");
    String cupo = request.getParameter("nomCupo");
    String idCupo = request.getParameter("idCupo");

%>

<table width="400" height="18" border='0' align='center' cellpadding="0" cellspacing="0" class="tablas" id="tbl_proceso">

  <tr>
        <td class="titulo_ventana" id="drag_div_modificar" colspan="18">
            <div style="float:left; background-color: #009900" ><font style=" color: #ffffff; font-size: 13px; font-weight:  bold "> Cupo Cr&eacute;dito Bancario </font></div> 
        <td width="50%" align="left" class="barratitulo">
            <img alt="" src="<%= BASEURL%>/images/titulo.gif" width="32" height="20"  align="left"/>
        </td>
    <div style="float:right" onClick="$('#div_modificar').fadeOut('slow');"><a class="ui-widget-header ui-corner-all"><span>X</span></a></div>
 </tr>    

</table> 
<br>
<div>
    <table class="tablaInferior" border="0" width="100%" style="text-align: left" >
        <tr class='fila'>
            <td>Cupos de Cr&eacute;dito Bancario</td>
            <td>
                <input type="text" name="cupo" id="cupo" value="<%=cupo != null ? cupo : ""%>"   style=" width:80%"/>
                <input type="hidden" id="idCupo" value="<%=idCupo%>" />
            </td>
        </tr>
        <tr class="fila">
            <td>Cuenta Asociada</td>
            <td>
                <input type="text" name="cuenta" id="cuenta" style=" width: 80%" onkeypress="buscarCuenta(this.id)"/> 
            </td>
        </tr> 
    </table>
</div>
<br/>
<br/>
<div align="center">
    <img style="cursor:hand" src="<%= BASEURL%>/images/botones/aceptar.gif" onmouseover="botonOver(this);" onmouseout="botonOut(this);" onClick="guardarCupoCredito('<%=CONTROLLER%>', '<%=opcion%>');"/>
    <img alt="" src="<%=BASEURL%>/images/botones/salir.gif" style="cursor:pointer" name="imgsalir" id="imgsalir" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" onclick="$('#div_modificar').fadeOut('slow');"/>
    <br/>
</div>
