<%-- 
    Document   : consultaCreditoBancario
    Created on : 10/01/2012, 05:37:23 PM
    Author     : darrieta
--%>

<%@page import="com.tsp.util.Util"%>
<%@page import="com.tsp.operation.model.services.BancosService"%>
<%@page import="com.tsp.operation.model.services.CreditosBancariosService"%>
<%@page import="com.tsp.util.UtilFinanzas"%>
<%@page contentType="text/html" pageEncoding="ISO-8859-1"%>
<%@include file    ="/WEB-INF/InitModel.jsp"%>
<%@page import="java.util.ArrayList"%>
<%@page import="com.tsp.operation.model.beans.*"%>
<%
Usuario usuario = (Usuario)session.getAttribute("Usuario");
CreditosBancariosService cbs = new CreditosBancariosService(usuario.getBd());
BancosService bs = new BancosService(usuario.getBd());

String nit = request.getParameter("nit");
String documento = request.getParameter("documento");
CreditoBancario credito = cbs.consultarCreditoBancario(usuario.getDstrct(),nit, documento);
Bancos banco = bs.obtenerBancoXNit(nit);
ArrayList<CreditoBancarioDetalle> detalles = cbs.consultarDetallesCreditoBancario(usuario.getDstrct(),nit, documento);
ArrayList<CreditoBancario> movExternos = cbs.consultarMovimientosExternos(usuario.getDstrct(), nit, documento, credito.getPeriodicidad());

String mensaje = request.getParameter("mensaje");
boolean editar = Boolean.parseBoolean((String)session.getAttribute("EDITAR_CRBANCARIO"));
if(editar){
    CXP_Doc cxp = model.cxpDocService.ConsultarCXP_Doc(usuario.getDstrct(), nit, "FAP", documento);
    if(!cxp.getCorrida().equals("")){
        editar = false;
        mensaje = "No se pueden agregar detalles porque la factura se encuentra en la corrida "+cxp.getCorrida();
    }
}
model.tablaGenService.buscarDatos("CB_PERIODI", String.valueOf(credito.getPeriodicidad()));
TablaGen tperiodicidad = model.tablaGenService.getTblgen();

%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1"/>
        <title>Buscar credito bancario</title>
        <link href="<%= BASEURL%>/css/estilostsp.css" rel='stylesheet'/>
        <link href="<%= BASEURL%>/css/letras.css" rel='stylesheet'/>
        <link href="<%=BASEURL%>/css/default.css"       rel="stylesheet" type="text/css"/>
        <script type='text/javascript' src="<%= BASEURL%>/js/boton.js"></script>
        <script type='text/javascript' src="<%= BASEURL%>/js/prototype.js"></script>
        <script type='text/javascript' src="<%= BASEURL%>/js/creditoBancario.js"></script>
        <script type='text/javascript' src="<%=BASEURL%>/js/validar.js"></script>
        <!-- Para las ventanas modales-->
        <script src="<%=BASEURL%>/js/effects.js" type="text/javascript"></script>
        <script src="<%=BASEURL%>/js/window.js" type="text/javascript"></script>
        <script src="<%=BASEURL%>/js/window_effects.js" type="text/javascript"></script>
        <!-- Las siguientes librerias CSS y JS son para el manejo del calendario -->
        <link rel="stylesheet" type="text/css" href="<%=BASEURL%>/css/jCal/jscal2.css" />
        <link rel="stylesheet" type="text/css" href="<%=BASEURL%>/css/jCal/border-radius.css" />
        <script type="text/javascript" src="<%=BASEURL%>/js/jCal/jscal2.js"></script>
        <script type="text/javascript" src="<%=BASEURL%>/js/jCal/lang/es.js"></script>
    </head>
    <body>
        <div id="capaSuperior" style="position:absolute; width:100%; height:100px; z-index:0; left: 0px; top: 0px;">
            <jsp:include page="/WEB-INF/toptsp.jsp?encabezado=Manejo de creditos bancarios"/>
        </div>
        <div id="capaCentral" style="position:absolute; width:100%; height:85%; z-index:0; left: 0px; top: 100px; overflow: auto;">
            <form id="form1" name="form1" method="post" action="<%=CONTROLLER%>?estado=Credito&accion=Bancario&opcion=GUARDAR_DETALLE_CREDITO">
                <div id="contenido" align="center" style=" width: 600px; height: 300px; visibility: hidden; background-color:#F7F5F4; display: none"></div>
                <table border="2" width="500" align="center">
                    <tr><td>
                        <table class="tablaInferior" border="0" width="100%">
                            <tr>
                                <td width="50%" align="left" colspan="2" class="subtitulo1">Datos del cr&eacute;dito</td>
                                <td width="50%" align="left" colspan="2" class="barratitulo">
                                    <img alt="" src="<%= BASEURL%>/images/titulo.gif" width="32" height="20" align="left"/>
                                </td>
                            </tr>
                            <tr class="fila">
                                <td>Banco</td>
                                <td><%= banco.getNombre()%></td>
                                <td>Documento</td>
                                <td><%= credito.getDocumento()%></td>
                            </tr>
                            <tr class="fila">
                                <td>Linea de credito</td>
                                <td><%= credito.getLinea_credito()%></td>
                                <td>Cupo Credito</td>
                                <td><%= credito.getCupo()%></td>
                            </tr>
                            <tr class="fila">
                                <td>Fecha inicial</td>
                                <td><%= credito.getFecha_inicial()%></td>
                                <td>Fecha vencimiento</td>
                                <td><%= credito.getFecha_vencimiento()%></td>
                            </tr>
                            <tr class="fila">
                                <td>Periodicidad</td>
                                <td><%= tperiodicidad==null?"UNICO":tperiodicidad.getDescripcion()%></td>
                                <td>Valor del cr&eacute;dito</td>
                                <td><%= UtilFinanzas.customFormat("#,###.##",credito.getVlr_credito(),2)%></td>
                            </tr>
                        </table>
                    </td></tr>
                </table>
                <br/>
                <table border="2" width="1300" align="center">
                    <tr><td>
                        <table id="tablaDetalles" class="tablaInferior" border="0" width="100%">
                            <tr>
                                <td width="50%" align="left" class="subtitulo1" colspan="8">Detalles del cr&eacute;dito</td>
                                <td width="50%" align="left" class="barratitulo" colspan="8">
                                    <img alt="" src="<%= BASEURL%>/images/titulo.gif" width="32" height="20" align="left"/>
                                </td>
                            </tr>
                            <tr class="Titulos" align="center">
                                <td>Cuota</td>
                                <td>Fecha inicial</td>
                                <td>Fecha final</td>
                                <td>DTF</td>
                                <td>Tasa</td>
                                <td>Saldo inicial</td>
                                <td>Capital</td>
                                <td>Intereses a fecha final</td>
                                <td>Ajuste</td>
                                <td>Intereses acumulados</td>
                                <td>Pago capital</td>
                                <td>Pago intereses</td>
                                <td>Valor pago</td>
                                <td>Saldo</td>
                                <td>Doc. intereses</td>
                                <td>Doc. pago</td>
                            </tr>
                            <%
                            //Se instancian las variables necesarias que guardan lso datos de la ultima fila
                            String fechaFinal = credito.getFecha_inicial();
                            double capitalInicial=credito.getVlr_credito();
                            double pagoCapital=0;
                            double pagoIntereses=0;
                            double interesAcumulado=0;
                            double saldo=credito.getVlr_credito();
                            double dtf=credito.getDtf();
                            for(int i=0; i<detalles.size(); i++){
                                double pago = detalles.get(i).getPago_capital()+detalles.get(i).getPago_intereses();
                                double saldoFinal = detalles.get(i).getCapital_inicial() + detalles.get(i).getInteres_acumulado() - pago;
                                double tasa = 0;
                                if(credito.getPeriodicidad()==0){
                                    tasa = credito.getTasa_cobrada();
                                }else{
                                    tasa = cbs.calcularTasaDiaria(credito.getNit_banco(), detalles.get(i).getDtf(), credito.getPuntos_basicos(), credito.getPeriodicidad(), credito.getTipo_dtf()) * 100;
                                }
                                //Si es la ultima fila
                                if(i==detalles.size()-1){
                                    fechaFinal = detalles.get(i).getFecha_final();
                                    pagoCapital = detalles.get(i).getPago_capital();
                                    pagoIntereses = detalles.get(i).getPago_intereses();
                                    capitalInicial = detalles.get(i).getCapital_inicial() - detalles.get(i).getPago_capital();
                                    interesAcumulado = detalles.get(i).getInteres_acumulado();
                                    saldo = saldoFinal;
                                    dtf = detalles.get(i).getDtf();
                                }
                            %>
                                <tr class="fila" style="font-size: 10px;">
                                    <td><%=i+1%></td>
                                    <td><%=detalles.get(i).getFecha_inicial()%></td>
                                    <td><%=detalles.get(i).getFecha_final()%></td>
                                    <td align="right" width="60"><%=detalles.get(i).getDtf()%></td>
                                    <td align="right"><%=UtilFinanzas.customFormat("#,###.##",tasa,4)%></td>
                                    <td align="right"><%=UtilFinanzas.customFormat("#,###.##",detalles.get(i).getSaldo_inicial(),2)%></td>
                                    <td align="right"><%=UtilFinanzas.customFormat("#,###.##",detalles.get(i).getCapital_inicial(),2)%></td>
                                    <td align="right"><%=UtilFinanzas.customFormat("#,###.##",detalles.get(i).getIntereses(),2)%></td>
                                    <td align="right"><%=UtilFinanzas.customFormat("#,###.##",detalles.get(i).getAjuste_intereses(),2)%></td>
                                    <td align="right"><%=UtilFinanzas.customFormat("#,###.##",detalles.get(i).getInteres_acumulado(),2)%></td>
                                    <td align="right"><%=UtilFinanzas.customFormat("#,###.##",detalles.get(i).getPago_capital(),2)%></td>
                                    <td align="right"><%=UtilFinanzas.customFormat("#,###.##",detalles.get(i).getPago_intereses(),2)%></td>
                                    <td align="right"><%=UtilFinanzas.customFormat("#,###.##",pago,2)%></td>
                                    <td align="right"><%=UtilFinanzas.customFormat("#,###.##",saldoFinal,2)%></td>
                                    <td align="right"><%=Util.coalesce(detalles.get(i).getDoc_intereses(),"")%></td>
                                    <td align="right"><%=Util.coalesce(detalles.get(i).getDoc_pago(),"")%></td>
                                </tr>
                            <%}%>
                        </table>
                        <input type="hidden" id="CONTROLLER" value="<%=CONTROLLER%>"/>
                        <input type="hidden" id="dstrct" name="dstrct" value="<%=usuario.getDstrct()%>" />
                        <input type="hidden" id="nit" name="nit" value="<%=nit%>" />
                        <input type="hidden" id="documento" name="documento" value="<%=documento%>" />
                        <input type="hidden" id="numDetalles" value="<%=detalles.size()%>" />
                        <input type="hidden" id="tasaEA" value="0" />
                        <input type="hidden" id="tasaNominal" value="0" />
                        <input type="hidden" id="tasaVencida" value="0" />
                        <input type="hidden" id="tasaDiaria" value="0" />
                        <input type="hidden" id="tasaCobrada" value="<%= credito.getTasa_cobrada()%>" />
                        <input type="hidden" id="periodicidad" name="periodicidad" value="<%= credito.getPeriodicidad()%>" />
                        <input type="hidden" id="ptoBasicos" value="<%= credito.getPuntos_basicos()%>" />
                        <input type="hidden" id="baseBanco" value="<%= banco.getBaseAno()%>" />
                        <input type="hidden" id="hidFechaFinal" value="<%=fechaFinal%>"/>
                        <input type="hidden" id="hidDtf" value="<%=dtf%>"/>
                        <input type="hidden" id="hidCapitalInicial" value="<%=capitalInicial%>"/>
                        <input type="hidden" id="hidPagoCapital" value="<%=pagoCapital%>"/>
                        <input type="hidden" id="hidPagoIntereses" value="<%=pagoIntereses%>"/>
                        <input type="hidden" id="hidInteresAcumulado" value="<%=interesAcumulado%>"/>
                        <input type="hidden" id="hidSaldo" value="<%=saldo%>"/>
                        <input type="hidden" id="tipoDTF" value="<%=credito.getTipo_dtf()%>"/>
                        <input type="hidden" id="dtfTA" value="0"/>
                         <input type="hidden" id="ref" name="ref" value="<%=credito.getRef_credito() %>" />
                    </td></tr>
                </table>
                <br/>
                
                <%if(movExternos.size()>0){%>
                    <table border="2" width="500" align="center">
                        <tr><td>
                            <table class="tablaInferior" border="0" width="100%">
                                <tr>
                                    <td width="50%" align="left" colspan="2" class="subtitulo1">Movimientos externos</td>
                                    <td width="50%" align="left" class="barratitulo">
                                        <img alt="" src="<%= BASEURL%>/images/titulo.gif" width="32" height="20" align="left"/>
                                    </td>
                                </tr>
                                <tr class="Titulos" align="center">
                                    <td>Fecha</td>
                                    <td>Documento</td>
                                    <td>Valor</td>
                                </tr>
                                <%for(int i=0; i< movExternos.size(); i++){%>
                                <tr class="fila">
                                        <td><%= movExternos.get(i).getFecha_inicial()%></td>
                                        <td><%= movExternos.get(i).getDocumento()%></td>
                                        <td  ondblclick="modificarSaldoAgregarFila(this.id)" id='miTd_<%=i%>' align="right"><%= UtilFinanzas.customFormat("#,###.##",movExternos.get(i).getVlr_credito(),2)%></td>                                      
                                    </tr>
                                <%}%>
                            </table>
                        </td></tr>
                    </table>
                    <br/>
                <%}%>
                <div align="center">
                    <%if(editar){%>
                        <img style="cursor:pointer" src="<%= BASEURL%>/images/botones/agregar.gif" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" onClick="agregarFila('<%=BASEURL%>','<%=credito.getPeriodicidad()%>')"/>
                        <img style="cursor:pointer" src="<%= BASEURL%>/images/botones/guardar.gif" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" onClick="validarDetalle('<%=BASEURL%>')"/>
                    <%}%>
                    <img style="cursor:pointer" src="<%= BASEURL%>/images/botones/regresar.gif" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" onClick="history.back(1)"/>
                    <img alt="" src="<%=BASEURL%>/images/botones/salir.gif" style="cursor:pointer" name="imgsalir" id="imgsalir" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" onclick='parent.close();'/>
                </div>
                <br/>
                <%if(mensaje!=null && !mensaje.equals("")){%>
                <div id="divMensaje">
                    <table border="2" align="center">
                        <tr>
                            <td>
                                <table width="100%"  border="0" align="center"  bordercolor="#f7f5f4" bgcolor="#ffffff">
                                    <tr>
                                        <td width="229" align="center" class="mensajes"><%=mensaje%></td>
                                        <td width="29" background="<%=BASEURL%>/images/cuadronaranja.JPG">&nbsp;&nbsp;</td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                    </table>
                </div>
                <%}%>
                <br/>
                <br/>
                <br/>
                <br/>
                <br/>
                <br/>
                <br/>
            </form>
        </div>
    </body>
</html>

