<%-- 
    Document   : crearCreditoBancario
    Created on : 18/11/2011, 02:40:23 PM
    Author     : darrieta - Geotech
--%>

<%@page import="com.tsp.operation.model.services.CreditosBancariosService"%>
<%@page import="com.tsp.operation.model.beans.CupoCreditoBancario"%>
<%@page import="com.tsp.operation.model.beans.LineaCredito"%>
<%@page import="com.tsp.operation.model.beans.Usuario"%>
<%@page import="java.util.List"%>
<%@page import="com.tsp.operation.model.beans.Banco"%>
<%@page import="java.util.Vector"%>
<%@page import="java.util.TreeMap"%>
<%@page import="com.tsp.operation.model.BancoService"%>
<%@page import="com.tsp.operation.model.services.BancosService"%>
<%@page import="com.tsp.operation.model.beans.Bancos"%>
<%@page import="java.util.ArrayList"%>
<%@page import="com.tsp.operation.model.beans.TablaGen"%>
<%@page import="java.util.LinkedList"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/InitModel.jsp"%>
<%@ taglib uri="http://jakarta.apache.org/taglibs/input-0.90" prefix="input" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">crea
<%
Usuario usuario = (Usuario)session.getAttribute("Usuario");
//Se obtiene las lineas y cupos de credito
CreditosBancariosService cbs = new CreditosBancariosService(usuario.getBd());
ArrayList<LineaCredito> lineas = cbs.listarLineasCredito();
ArrayList<CupoCreditoBancario> cupos = cbs.listarCuposCreditos();

//Se obtienen las tablagen

model.tablaGenService.buscarRegistrosNoAnulados("CB_PERIODI");
LinkedList<TablaGen> periodicidad = model.tablaGenService.getTablas();
model.tablaGenService.buscarRegistrosNoAnulados("CB_DTF");
LinkedList<TablaGen> dtfs = model.tablaGenService.getTablas();

BancosService bs = new BancosService(usuario.getBd());
ArrayList<Bancos> bancos = bs.obtenerBancosNit();

//Bancos egreso
String age = (usuario.getId_agencia().equals("OP")?"":usuario.getId_agencia());
model.servicioBanco.loadBancos(age, usuario.getCia());
TreeMap b = model.servicioBanco.getBanco();
//Autorizador CXP
model.tablaGenService.buscarRegistrosNoAnulados("AUT_CBANC");
LinkedList<TablaGen> ListAutxcp = model.tablaGenService.getTablas();
//lista de HC
model.cxpDocService.BuscarHC();
List listHC = model.cxpDocService.getListHC();



%>
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
        <title>Creacion de creditos bancarios</title>
        <link href="<%= BASEURL%>/css/estilostsp.css" rel='stylesheet'/>
        <script type='text/javascript' src="<%= BASEURL%>/js/boton.js"></script>
        <script type='text/javascript' src="<%= BASEURL%>/js/prototype.js"></script>
        <script type='text/javascript' src="<%= BASEURL%>/js/creditoBancario.js"></script>
        <script type='text/javascript' src="<%=BASEURL%>/js/validar.js"></script>
        <!-- Las siguientes librerias CSS y JS son para el manejo del calendario -->
        <link rel="stylesheet" type="text/css" href="<%=BASEURL%>/css/jCal/jscal2.css" />
        <link rel="stylesheet" type="text/css" href="<%=BASEURL%>/css/jCal/border-radius.css" />
        <script type="text/javascript" src="<%=BASEURL%>/js/jCal/jscal2.js"></script>
        <script type="text/javascript" src="<%=BASEURL%>/js/jCal/lang/es.js"></script>
    </head>
    <body id="main_body" >
        <div id="capaSuperior" style="position:absolute; width:100%; height:100px; z-index:0; left: 0px; top: 0px;">
            <jsp:include page="/WEB-INF/toptsp.jsp?encabezado=Manejo de creditos bancarios"/>
        </div>
        <div id="capaCentral" style="position:absolute; width:100%; height:85%; z-index:0; left: 0px; top: 100px; overflow: scroll;">
            <form id="form1" name="form1" method="post" action="<%=CONTROLLER%>?estado=Credito&accion=Bancario&opcion=CREAR">
                <input type="hidden" name="CONTROLLER" id="CONTROLLER" value="<%=CONTROLLER%>"/>
                <table align="center" style="border: green solid 1px;" width="700px">
                    <tr class="barratitulo">
                        <td class="subtitulo1">Creación de creditos bancarios</td>
                        <td><img src="<%=BASEURL%>/images/titulo.gif" width="32" height="20" align="left"/></td>
                    </tr>
              
                    <tr class="fila">
                        <td>L&iacute;nea cr&eacute;dito</td>
                        <td>
                            <select class="element select medium" id="linea" name="linea" onchange="cargarHC()">
                                <option value="" selected="selected">Seleccione</option>
                                <%for(int i=0; i<lineas.size(); i++){%>
                                    <option value="<%=lineas.get(i).getHc()+";"+lineas.get(i).getLinea()%>" ><%=lineas.get(i).getLinea()%></option>
                                <%}%>
                            </select>
                        </td>
                    </tr>  
                    <tr class="fila">
                        <td>HC l&iacute;nea</td>
                        <td>
                            <input type="text" name="hc" id="hc" value="" readonly />
                        </td>
                    </tr>   
                    <tr class="fila">
                        <td>Cupo cr&eacute;dito</td>
                        <td>
                            <select class="element select medium" id="cupo" name="cupo" onchange="cargarCuenta()">
                                 <option value="" selected="selected">Seleccione</option>
                                <%for(int i=0; i<cupos.size(); i++){%>
                                    <option value="<%=cupos.get(i).getCuenta()+ ";"+cupos.get(i).getNombre() %>" ><%=cupos.get(i).getNombre()%></option>
                                    
                                <%}%>
                            </select>
                        </td>
                    </tr>
                    <tr class="fila">
                        <td>Cuenta cupo</td>
                        <td>
                            <input type="text" name="cuenta" id="cuenta" value="" readonly />
                        </td>
                    </tr>        
                    <tr class="fila">
                        <td>Banco</td>
                        <td>
                            <select class="element select medium" id="banco" name="banco" >
                                <option value="" selected="selected">Seleccione</option>
                                <%for(int i=0; i<bancos.size(); i++){%>
                                    <option value="<%=bancos.get(i).getNit()%>" id="<%=bancos.get(i).getCodigo()%>_<%=bancos.get(i).getBaseAno()%>" ><%=bancos.get(i).getNombre()%></option>
                                <%}%>
                            </select>
                        </td>
                    </tr>
                    
                    <tr class="fila">
                        <td>Documento</td>
                        <td>
                            <input type="text" name="documento" id="documento" />
                        </td>
                    </tr>
                    <tr class="fila">
                        <td>Tipo DTF</td>
                        <td>                            
                            <select id="tipoDTF" name="tipoDTF" onchange="calcularDTF();recalcularTasas();">
                                <%for(int i=0; i<dtfs.size(); i++){%>
                                    <option value="<%=dtfs.get(i).getTable_code()%>" ><%=dtfs.get(i).getDescripcion()%></option>
                                <%}%>
                            </select>
                            <span id="dtfFactoring" style="display: none;">DTF E.A.</span>
			</td>
                    </tr>
                    <tr class="fila">
                        <td>DTF</td>
                        <td>
                            <input type="hidden" id="dtfTA" value="<%=cbs.obtenerDTFActual()%>" />
                            <input type="text" id="dtf" name="dtf" class="fila" style="border: 0pt none;"  value="<%=cbs.obtenerDTFActual()%>" onkeyup="soloNumerosformateado(this.id);" onblur="recalcularTasas()" />
			</td>
                    </tr>
                    <tr class="fila">
                        <td>Puntos básicos</td>
                        <td>
                            <input type="text" name="ptoBasicos" id="ptoBasicos" />
                        </td>
                    </tr>
                    <tr id="trTasaEA" class="fila">
                        <td>Tasa EA</td>
                        <td>
                            <input type="text" name="tasaEA" id="tasaEA"  class="fila" style="border: 0pt none;" />
                        </td>
                    </tr>
                    <tr class="fila" style="height: 23px;">
                        <td>Periodicidad de pago</td>
                        <td>
                            <select class="element select medium" id="periodicidad" name="periodicidad" onchange="recalcularTasas()">
                                <option value="">Seleccione</option>
                                <%for(int i=0; i<periodicidad.size(); i++){%>
                                    <option value="<%=periodicidad.get(i).getTable_code()%>" ><%=periodicidad.get(i).getDescripcion()%></option>
                                <%}%>
                            </select>
                            <span id="unico" style="display: none;">UNICO</span>
                        </td>
                    </tr>
                    <tr id="trTasaNominal" class="fila">
                        <td>Tasa nominal</td>
                        <td>
                            <input type="text" name="tasaNominal" id="tasaNominal" readonly="readonly" class="fila" style="border: 0pt none;" />
                        </td>
                    </tr>
                    <tr id="trTasaVencida" class="fila">
                        <td>Tasa vencida</td>
                        <td>
                            <input type="text" name="tasaVencida" id="tasaVencida" readonly="readonly" class="fila" style="border: 0pt none;" />
                        </td>
                    </tr>
                    <tr id="trTasaDiaria" class="fila">
                        <td>Tasa diaria</td>
                        <td>
                            <input type="text" name="tasaDiaria" id="tasaDiaria" readonly="readonly" class="fila" style="border: 0pt none;" />
                        </td>
                    </tr>
                    <tr id="trTasaPactada" style="display: none" class="fila">
                        <td>Tasa pactada</td>
                        <td>
                            <input type="text" name="tasaPactada" id="tasaPactada" readonly="readonly" class="fila" style="border: 0pt none;" />
                        </td>
                    </tr>
                    <tr id="trTasaCobrada" style="display: none" class="fila">
                        <td>Tasa cobrada</td>
                        <td>
                            <input type="hidden" name="tasaCobrada" id="tasaCobrada" readonly="readonly" class="fila" style="border: 0pt none;" />
                            <input type="text" id="tasaCobrada2" readonly="readonly" class="fila" style="border: 0pt none;" />
                        </td>
                    </tr>
                    <tr class="fila">
                        <td>Valor desembolso</td>
                        <td>
                            <input type="text" name="vlrCredito" id="vlrCredito" onkeyup="soloNumerosformateado(this.id);" onblur="recalcularTasas()"/>
                        </td>
                    </tr>
                    <tr id="trVlrIntereses" style="display: none" class="fila">
                        <td>Valor intereses</td>
                        <td>
                            <input type="text" name="vlrIntereses" id="vlrIntereses" onkeyup="soloNumerosformateado(this.id);" onblur="recalcularTasas()"/>
                        </td>
                    </tr>
                    <tr id="trVlrFactura" style="display: none" class="fila">
                        <td>Valor factura</td>
                        <td>
                            <input type="text" name="vlrFactura" id="vlrFactura" readonly/>
                        </td>
                    </tr>
                    <tr class="fila">
                        <td>Fecha inicial</td>
                        <td>
                            <input type="text" name="fechaInicial" id="fechaInicial" readonly />
                            <img src="<%=BASEURL%>/js/Calendario/cal.gif" id="imgFechaInicial" />
                        </td>
                    </tr>
                    <tr class="fila">
                        <td>Fecha vencimiento</td>
                        <td>
                            <input type="text" name="fechaVencimiento" id="fechaVencimiento" readonly />
                            <img src="<%=BASEURL%>/js/Calendario/cal.gif" id="imgFechaVencimiento" />
                        </td>
                    </tr>
                    <tr class="fila">
                        <td>Banco egreso</td>
                        <td>
                            <input:select name="bancoEgreso" attributesText="id='bancoEgreso' style='width:150px;' onChange='cargarSucursalesEgreso()' default=''" options="<%=b%>" >
                                <option value="" selected="selected">Seleccione</option>
                            </input:select>
                        </td>
                    </tr>
                    <tr class="fila">
                        <td>Sucursal egreso</td>
                        <td>
                            <select class="element select medium" id="sucursalEgreso" name="sucursalEgreso">
                            </select>
                        </td>
                    </tr>
                    <tr class="fila">
                        <td>Autorizador</td>
                        <td>
                            <select class="element select medium" id="autorizador" name="autorizador">
                                <%for (int i = 0; i < ListAutxcp.size(); i++) {
                                    TablaGen t = ListAutxcp.get(i);%>
                                    <option value="<%=t.getTable_code()%>" ><%=t.getTable_code()%></option>
                                <%}%>
                            </select>
                        </td>
                    </tr>
                    <tr class="fila">
                        <td>Descripci&oacute;n</td>
                        <td>
                            <textarea name="descripcion" id="descripcion" rows="2" cols="60"></textarea>
                        </td>
                    </tr>
                    <tr class="fila">
                        <td>Banco ingreso</td>
                        <td>
                            <input:select name="bancoIngreso" attributesText="id='bancoIngreso' style='width:150px;' onChange='cargarSucursalesIngreso()' default=''" options="<%=b%>" >
                                <option value="" selected="selected">Seleccione</option>
                            </input:select>
                        </td>
                    </tr>
                    <tr class="fila">
                        <td>Sucursal ingreso</td>
                        <td>
                            <select class="element select medium" id="sucursalIngreso" name="sucursalIngreso">
                            </select>
                        </td>
                    </tr>
                </table>
                <br/>
                <div align="center">
                    <img style="cursor:hand" src="<%= BASEURL%>/images/botones/aceptar.gif" align="absmiddle" onmouseover="botonOver(this);" onmouseout="botonOut(this);" onClick="validarCredito();"/>
                    <img style="cursor:hand" src="<%= BASEURL%>/images/botones/salir.gif" align="absmiddle" onmouseover="botonOver(this);" onmouseout="botonOut(this);" onClick="parent.close();"/>
                </div>
            </form>
            <br/>
            <%if(request.getParameter("mensaje")!=null && !request.getParameter("mensaje").equals("")){%>
            <div id="divMensaje">
                <table border="2" align="center">
                    <tr>
                        <td>
                            <table width="100%"  border="0" align="center"  bordercolor="#f7f5f4" bgcolor="#ffffff">
                                <tr>
                                    <td width="229" align="center" class="mensajes"><%=request.getParameter("mensaje")%></td>
                                    <td width="29" background="<%=BASEURL%>/images/cuadronaranja.JPG">&nbsp;&nbsp;</td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
            </div>
            <%}%>
        </div>
        <iframe width=188 height=166 name="gToday:normal:agenda.js:gfPop:plugins.js" id="gToday:normal:agenda.js:gfPop:plugins.js" src="<%=BASEURL%>/js/Calendario/ipopeng.htm" scrolling="no" frameborder="0" style="visibility:visible; z-index:999; position:absolute; left:-500px; top:0px;"> </iframe>
        <script type="text/javascript">
            Calendar.setup({
                inputField : "fechaInicial",
                trigger    : "imgFechaInicial",
                onSelect   : function() {
                    recalcularTasas();
                    this.hide();
                }
            });
            Calendar.setup({
                inputField : "fechaVencimiento",
                trigger    : "imgFechaVencimiento",
                onSelect   : function() {
                    recalcularTasas();
                    this.hide();
                }
            });
        </script>
    </body>
</html>
