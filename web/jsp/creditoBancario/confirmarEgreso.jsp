<%-- 
    Document   : confirmarEgreso
    Created on : 23/01/2012, 03:20:51 PM
    Author     : diana
--%>

<%@page import="com.tsp.operation.model.beans.CXP_Doc"%>
<%@page import="com.tsp.operation.model.beans.Usuario"%>
<%@page import="java.util.TreeMap"%>
<%@page contentType="text/html" pageEncoding="ISO-8859-1"%>
<%@page session    ="true"%>
<%@include file    ="/WEB-INF/InitModel.jsp"%>
<%@taglib uri="http://jakarta.apache.org/taglibs/input-0.90" prefix="input" %>
<%
Usuario usuario = (Usuario)session.getAttribute("Usuario");
//Bancos egreso
String age = (usuario.getId_agencia().equals("OP")?"":usuario.getId_agencia());
model.servicioBanco.loadBancos(age, usuario.getCia());
TreeMap b = model.servicioBanco.getBanco();

//consultar los datos de egreso de la factura para ponerlos por defecto
String nit = request.getParameter("nit");
String documento = request.getParameter("documento");
CXP_Doc cxp = model.cxpDocService.ConsultarCXP_Doc(usuario.getDstrct(), nit, "FAP", documento);

%>
<table border="2" align="center" width="95%">
    <tr>
        <td>
            <table class="tablaInferior" border="0" width="100%" style="text-align: left">
                <tr>
                    <td width="50%" align="left" class="subtitulo1">Confirmar detalles credito bancario</td>
                    <td width="50%" align="left" class="barratitulo">
                        <img alt="" src="<%= BASEURL%>/images/titulo.gif" width="32" height="20"  align="left"/>
                    </td>
                </tr>
                <tr class="fila">
                    <td colspan="2">
                        <label>
                            <input type="radio" id="tipoTodo" name="tipo" value="TODO" checked="checked" onclick="mostrarBancoEgreso()" />
                            Causaci&oacute;n y egreso
                        </label>
                    </td>
                </tr>
                <tr class="fila">
                    <td colspan="2">
                        <label>
                            <input type="radio" id="tipoCausacion" name="tipo" value="CAUSACION" onclick="mostrarBancoEgreso()" />
                            Solo causaci&oacute;n
                        </label>
                    </td>
                </tr>
                <tr id="trBancoEgreso" class="fila">
                    <td>Banco egreso</td>
                    <td>                        
                        <input:select name="bancoEgreso" attributesText="id='bancoEgreso' style='width:150px;' onChange='cargarSucursalesEgreso()' " default='<%=cxp.getBanco()%>' options="<%=b%>" />
                    </td>
                </tr>
                <tr id="trSucursalEgreso" class="fila">
                    <td>Sucursal egreso</td>
                    <td>
                        <input type="hidden" id="sucursalDefault" value="<%=cxp.getSucursal()%>"/>
                        <select class="element select medium" id="sucursalEgreso" name="sucursalEgreso">
                        </select>
                    </td>
                </tr>
            </table>
        </td>
    </tr>
</table>
<br/>
<br/>
<div align="center">
    <img style="cursor:hand" src="<%= BASEURL%>/images/botones/aceptar.gif" onmouseover="botonOver(this);" onmouseout="botonOut(this);" onClick="guardarDetallesCredito();"/>
    <img alt="" src="<%=BASEURL%>/images/botones/salir.gif" style="cursor:pointer" name="imgsalir" id="imgsalir" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" onclick='parent.closewin();'/>
</div>