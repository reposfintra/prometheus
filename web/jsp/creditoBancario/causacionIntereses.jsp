<%-- 
    Document   : consultaPuntosBasicos
    Created on : 23/11/2011, 10:53:34 AM
    Author     : darrieta - geotech
--%>

<%@page import="com.tsp.operation.model.beans.CreditoBancario"%>
<%@page import="com.tsp.operation.model.beans.CreditoBancarioDetalle"%>
<%@page import="com.tsp.operation.model.beans.CupoBanco"%>
<%@page import="com.tsp.operation.model.beans.PuntosBasicosBanco"%>
<%@page import="com.tsp.operation.model.services.CreditosBancariosService"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@page session    ="true"%>
<%@page errorPage  ="/error/ErrorPage.jsp"%>
<%@include file    ="/WEB-INF/InitModel.jsp"%>
<%@page import    ="java.util.*" %>
<%@page import    ="com.tsp.util.*" %>
<%@page import    ="com.tsp.operation.model.beans.Usuario" %>


<%
    Usuario usuario = (Usuario) session.getAttribute("Usuario");
    String id = request.getParameter("id");
    String modificar = request.getParameter("modificar");
    CreditosBancariosService cbs = new CreditosBancariosService(usuario.getBd());
    ArrayList<CreditoBancario> creditos = cbs.consultarDatosCausacion();
%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
        <title>Causación Intereses</title>
        <script src="<%=BASEURL%>/js/prototype.js"      type="text/javascript"></script>
        <script src="<%=BASEURL%>/js/boton.js"          type="text/javascript"></script>
        <script src="<%=BASEURL%>/js/effects.js"        type="text/javascript"></script>
        <script src="<%=BASEURL%>/js/window.js"         type="text/javascript"></script>
        <script src="<%=BASEURL%>/js/window_effects.js" type="text/javascript"></script>
        <script type='text/javascript' src="<%= BASEURL%>/js/creditoBancario.js"></script>
        <script type='text/javascript' src="<%=BASEURL%>/js/validar.js"></script>

        <link href="<%= BASEURL%>/css/estilostsp.css" rel="stylesheet" type="text/css"/>
        <link href="<%=BASEURL%>/css/default.css"       rel="stylesheet" type="text/css"/>
        
        <!-- Las siguientes librerias CSS y JS son para el manejo del calendario -->
        <link rel="stylesheet" type="text/css" href="<%=BASEURL%>/css/jCal/jscal2.css" />
        <link rel="stylesheet" type="text/css" href="<%=BASEURL%>/css/jCal/border-radius.css" />
        <script type="text/javascript" src="<%=BASEURL%>/js/jCal/jscal2.js"></script>
        <script type="text/javascript" src="<%=BASEURL%>/js/jCal/lang/es.js"></script>
    </head>
    <body>
        <div id="capaSuperior" style="position:absolute; width:100%; height:100px; z-index:0; left: 0px; top: 0px;">
            <jsp:include page="/WEB-INF/toptsp.jsp?encabezado=Manejo de creditos bancarios"/>
        </div>
        <div id="capaCentral" style="position:absolute; width:100%; height:85%; z-index:0; left: 0px; top: 100px; overflow: auto;">
            <form id="form1" name="form1" method="post" action="<%=CONTROLLER%>?estado=Credito&accion=Bancario&opcion=CAUSACION">
                <br/>
                <div id="contenido" align="center" style=" width: 600px; height: 300px; visibility: hidden; background-color:#F7F5F4; display: none"></div>
                <table border="2" align="center" width="900">
                    <tr>
                        <td>
                            <table class="tablaInferior" border="0" width="100%">
                                <tr>
                                    <td width="50%" colspan="5" align="left" class="subtitulo1">&nbsp;Causaci&oacute;n de intereses</td>
                                    <td width="50%" colspan="4" align="left" class="barratitulo" colspan="2">
                                        <img alt="" src="<%= BASEURL%>/images/titulo.gif" width="32" height="20" align="left"/>
                                    </td>
                                </tr>
                                <tr class="Titulos">
                                    <td><input type="checkbox" onClick="checkAll(this)"/></td>
                                    <td>Banco</td>
                                    <td>Documento</td>
                                    <td>DTF</td>
                                    <td>Tasa Cobrada / EA</td>
                                    <td>Fecha inicial</td>
                                    <td>Fecha final</td>
                                    <td>Saldo actual</td>
                                    <td>Valor intereses</td>
                                </tr>
                                <input type="hidden" name="numCreditos" id="numCreditos" value="<%=creditos.size()%>" />
                                <input type="hidden" id="tasaEA" value="0" />
                                <input type="hidden" id="tasaNominal" value="0" />
                                <input type="hidden" id="tasaVencida" value="0" />
                                <input type="hidden" id="tasaDiaria" value="0" />
                                <input type="hidden" id="tasa" value="0" />
                                <%for (int i = 0; i < creditos.size(); i++) {
                                    CreditoBancarioDetalle detalle = creditos.get(i).getDetalles().get(0);
                                    
                                    double tasa = 0;
                                    if(creditos.get(i).getPeriodicidad()==0){
                                        tasa = creditos.get(i).getTasa_cobrada();
                                    }else{
                                        tasa = detalle.getTasaEA() * 100;
                                    }
                                %>
                                    <tr class='fila' align='center' style='cursor: pointer' onMouseOver='cambiarColorMouse(this)' <%=(modificar!=null && modificar.equals("SI"))?"onclick='editarCupo(\""+BASEURL+"\", \"EDITAR\", \""+i+"\")'":""%> >
                                        <td align="left">
                                            <input type="checkbox" name="chkCredito<%=i%>" id="chkCredito<%=i%>" value="ON" checked="checked"/>
                                            <input type="hidden" id="capital_inicial<%=i%>" name="capital_inicial<%=i%>" value="<%=detalle.getCapital_inicial()%>" />
                                            <input type="hidden" id="tasaDiaria<%=i%>" name="tasaDiaria<%=i%>" value="<%=detalle.getTasaDiaria()%>" />
                                            <input type="hidden" id="periodicidad<%=i%>" name="periodicidad<%=i%>" value="<%=creditos.get(i).getPeriodicidad()%>" />
                                            <input type="hidden" id="tasaCobrada<%=i%>" name="tasaCobrada<%=i%>" value="<%=creditos.get(i).getTasa_cobrada()%>" />
                                            <input type="hidden" id="tasaEA<%=i%>" name="tasaEA<%=i%>" value="<%=detalle.getTasaEA()%>" />
                                            <input type="hidden" id="interesAcumulado<%=i%>" name="interesAcumulado<%=i%>" value="<%=detalle.getInteres_acumulado()%>" />
                                            <input type="hidden" id="pagoIntereses<%=i%>" name="pagoIntereses<%=i%>" value="<%=detalle.getPago_intereses()%>" />
                                            <input type="hidden" id="ptoBasicos<%=i%>" name="ptoBasicos<%=i%>" value="<%=creditos.get(i).getPuntos_basicos()%>" />
                                            <input type="hidden" id="baseBanco<%=i%>" name="baseBanco<%=i%>" value="<%=creditos.get(i).getBaseAno()%>" />
                                            <input type="hidden" id="ref<%=i%>" name="ref<%=i%>" value="<%=creditos.get(i).getRef_credito() %>" />
                                        </td>
                                        <td align="left">
                                            <%=creditos.get(i).getNombre_banco()%>
                                            <input type="hidden" id="nit_banco<%=i%>" name="nit_banco<%=i%>" value="<%=creditos.get(i).getNit_banco()%>" />
                                        </td>
                                        <td align="left">
                                            <%=creditos.get(i).getDocumento()%>
                                            <input type="hidden" id="documento<%=i%>" name="documento<%=i%>" value="<%=creditos.get(i).getDocumento()%>" />
                                        </td>
                                        <td align="rigth">
                                            <input type="text" id="dtf<%=i%>" class="fila" style="border: 0pt none;text-align:right; width:50px;" name="dtf<%=i%>" value="<%=detalle.getDtf()%>" onBlur="calcularTasasCausacion('<%=i%>')" />
                                        </td>
                                        <td align="right">                                            
                                            <input type="text" id="tasa<%=i%>" class="fila" style="border: 0pt none;text-align:right; width:90px;" name="tasa<%=i%>" value="<%=UtilFinanzas.customFormat("#,###.##",tasa,5)%>" readonly />
                                        </td>
                                        <td align="right">
                                            <%=detalle.getFecha_inicial()%>
                                            <input type="hidden" id="fecha_inicial<%=i%>" name="fecha_inicial<%=i%>" value="<%=detalle.getFecha_inicial()%>" />
                                        </td>
                                        <td align="right">
                                            <input type="text" id="fecha_final<%=i%>" name="fecha_final<%=i%>" value="<%=Util.getFechaActual_String(4)%>" class="fila" style="border: 0pt none;text-align:right; width:80px;" readonly/>
                                            <img src="<%=BASEURL%>/js/Calendario/cal.gif" id="imgFechaFinal<%=i%>" />
                                            <script type="text/javascript">
                                                Calendar.setup({
                                                    inputField : "fecha_final<%=i%>",
                                                    trigger    : "imgFechaFinal<%=i%>",
                                                    onSelect   : function() {
                                                        calcularInteresesCausados(<%=i%>);
                                                        this.hide();
                                                    }
                                                });
                                            </script>
                                        </td>
                                        <td align="right">
                                            <%=UtilFinanzas.customFormat("#,###.##",detalle.getSaldo_inicial(),2)%>
                                            <input type="hidden" id="saldo_inicial<%=i%>" name="saldo_inicial<%=i%>" value="<%=detalle.getSaldo_inicial()%>" />
                                        </td>
                                        <td align="right">
                                            <input type="hidden" id="interesesCalculados<%=i%>" name="interesesCalculados<%=i%>" value="<%=detalle.getIntereses()%>" />
                                            <input type="text" id="intereses<%=i%>" name="intereses<%=i%>" class="fila" style="border: 0pt none;text-align:right; width:100px;" onKeyUp="soloNumerosformateado(this.id);"
                                                   value="<%=UtilFinanzas.customFormat("#,###.##",detalle.getIntereses(),2)%>" />
                                        </td>
                                    </tr>
                                <%}%>
                            </table>
                        </td>
                    </tr>
                </table>
                <br/>
                <br/>
                <div align="center">
                    <img style="cursor:pointer" src="<%= BASEURL%>/images/botones/aceptar.gif" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" onClick="validarCausacion()"/>
                    <img alt="" src = "<%=BASEURL%>/images/botones/restablecer.gif" style = "cursor:pointer" name = "imgsalir" onClick = "location.href='<%=request.getRequestURI()%>'" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);"/>
                    <img alt="" src="<%=BASEURL%>/images/botones/salir.gif" style="cursor:pointer" name="imgsalir" id="imgsalir" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" onclick='parent.close();'/>
                </div>
            </form>
            <br/>
            <%if(request.getParameter("mensaje")!=null && !request.getParameter("mensaje").equals("")){%>
            <div id="divMensaje">
                <table border="2" align="center">
                    <tr>
                        <td>
                            <table width="100%"  border="0" align="center"  bordercolor="#f7f5f4" bgcolor="#ffffff">
                                <tr>
                                    <td width="229" align="center" class="mensajes"><%=request.getParameter("mensaje")%></td>
                                    <td width="29" background="<%=BASEURL%>/images/cuadronaranja.JPG">&nbsp;&nbsp;</td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
            </div>
            <%}%>
        </div>
    </body>
</html>
