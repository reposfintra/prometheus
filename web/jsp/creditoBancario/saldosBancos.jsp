<%-- 
    Document   : saldosBancos
    Created on : 10/01/2012, 03:38:29 PM
    Author     : darrieta
--%>

<%@page import="com.tsp.util.UtilFinanzas"%>
<%@page contentType="text/html" pageEncoding="ISO-8859-1"%>
<%@include file    ="/WEB-INF/InitModel.jsp"%>
<%@page import="java.util.ArrayList"%>
<%@page import="com.tsp.operation.model.beans.*"%>
<%
ArrayList<CreditoBancario> saldosBancos = (ArrayList<CreditoBancario>)request.getAttribute("saldosBancos");
%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
        <title>JSP Page</title>
        <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
        <title>Buscar credito bancario</title>
        <link href="<%= BASEURL%>/css/estilostsp.css" rel='stylesheet'/>
        <script type='text/javascript' src="<%= BASEURL%>/js/boton.js"></script>
        <script type='text/javascript' src="<%= BASEURL%>/js/prototype.js"></script>
        <script type='text/javascript' src="<%= BASEURL%>/js/creditoBancario.js"></script>
    </head>
    <body>
        <div id="capaSuperior" style="position:absolute; width:100%; height:100px; z-index:0; left: 0px; top: 0px;">
            <jsp:include page="/WEB-INF/toptsp.jsp?encabezado=Manejo de creditos bancarios"/>
        </div>
        <div id="capaCentral" style="position:absolute; width:100%; height:85%; z-index:0; left: 0px; top: 100px; overflow: auto;">
            <table border="2" align="center" width="400">
                <tr><td>
                    <table class="tablaInferior" border="0" width="100%">
                        <tr>
                            <td width="50%" align="left" class="subtitulo1">Consulta cr&eacute;dito bancario</td>
                            <td width="50%" align="left" class="barratitulo" colspan="2">
                                <img alt="" src="<%= BASEURL%>/images/titulo.gif" width="32" height="20" align="left"/>
                            </td>
                        </tr>
                        <tr class="Titulos">
                            <td>Instituci&oacute;n</td>
                            <td>Valor total</td>
                        </tr>
                        <%for(int i=0; i<saldosBancos.size(); i++){%>
                        <tr class="fila" style='cursor: pointer' onMouseOver='cambiarColorMouse(this)' onclick="verCreditosBanco('<%=BASEURL%>', '<%=saldosBancos.get(i).getNit_banco()%>', '<%=saldosBancos.get(i).getNombre_banco()%>', '<%=request.getParameter("vigente")%>')" >
                            <td>
                                <%=saldosBancos.get(i).getNombre_banco()%>
                            </td>
                            <td><%=UtilFinanzas.customFormat("#,###.##",saldosBancos.get(i).getVlr_credito(),2)%></td>
                        </tr>
                        <%}%>
                    </table>
                </td></tr>
            </table>
            <br/>
            <div align="center">
                <img style="cursor:pointer" src="<%= BASEURL%>/images/botones/regresar.gif" onmouseover="botonOver(this);" onmouseout="botonOut(this);" onClick="history.back(1)"/>
                <img alt="" src="<%=BASEURL%>/images/botones/salir.gif" style="cursor:pointer" name="imgsalir" id="imgsalir" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" onclick='parent.close();'/>
            </div>
        </div>
    </body>
</html>
