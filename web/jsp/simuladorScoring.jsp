<%-- 
    Document   : simuladorScoring
    Created on : 14/04/2016, 11:07:37 AM
    Author     : user
--%>

<%@page contentType="text/html" pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
        <title>Simulador Scoring</title>
            <link type="text/css" rel="stylesheet" href="./css/jquery/jquery-ui/jquery-ui-2.css"/>
        <link type="text/css" rel="stylesheet" href="./css/jquery/jquery.jqGrid/ui.jqgrid.css"/>        
        <script type="text/javascript" src="./js/jquery/jquery-ui/jquery.min.js"></script>
        <script type="text/javascript" src="./js/jquery/jquery-ui/jquery.ui.min.js"></script>            
        <script type="text/javascript" src="./js/jquery/jquery.jqGrid/js/grid.locale-es.js"></script>
        <script type="text/javascript" src="./js/jquery/jquery.jqGrid/js/jquery.jqGrid.min.js"></script>
        <script type="text/javascript" src="./js/simuladorScoring.js"></script>  
        
         <!--css logica de negocio-->
       <link href="./css/scoring.css" rel="stylesheet" type="text/css">      

        <!--jqgrid--> 
        <script type="text/javascript" src="./js/jquery/jquery.jqGrid-4.7.0/js/grid.locale-es.js"></script>
        <script type="text/javascript" src="./js/jquery/jquery.jqGrid-4.7.0/js/jquery.jqGrid.min.js"></script>
        <script type="text/javascript" src="./js/jquery/jquery.jqGrid-4.7.0/plugins/jquery.contextmenu.js"></script>
        <script type="text/javascript" src="./js/jquery/jquery.jqGrid-4.7.0/js/jquery.tabletojson.js"></script>   
    </head>
    <body>
        <div id="capaSuperior" style="position:absolute; width:100%; height:100px; z-index:0; left: 0px; top: 0px;">
            <jsp:include page="/WEB-INF/toptsp.jsp?encabezado=SIMULADOR SCORING"/>
        </div>
        <div id="capaCentral" align="left" style="position:absolute; width:100%; height:100%; z-index:0; top: 110px;" >
            <center>
                <h4 style="width:610px">SIMULADOR SCORING</h4>
                <div id="div_search_scoring" style="width:600px"> 
                    <table>                          
                        <tr>
                            <td  class="td">
                                <label>Unidad de Negocio</label> 
                            </td>
                            <td  class="td">
                                <select id="id_und_negocio"></select>
                                <input type="hidden" id="id_solicitud" name="id_solicitud">
                            </td>                           
                            <td  class="td">
                                <button id="btn_show_simulador" class="ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only" >
                                    <span class="ui-button-text">Buscar</span>
                                </button>      
                                <button id="btn_clear_simulador" class="ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only" 
                                        role="button" aria-disabled="false">
                                    <span class="ui-button-text">Limpiar</span>
                                </button>
                            </td>
                            <td  class="td">
                                <label>Minimo</label> 
                            </td>
                            <td  class="td">
                                <input type="text" id="min_value" name="min_value" value="" class="resaltar_valor" readonly style="width:40px">
                            </td> 
                            <td  class="td">
                                <label>Maximo</label> 
                            </td>
                            <td  class="td">
                                <input type="text" id="max_value" name="max_value" value="" class="resaltar_valor" readonly style="width:40px">
                            </td> 
                        </tr>  
                    </table>                              
                </div>
                <br><br>
                 <div id="div_simulador_scoring" style="display: none;">  
                    <table border="0"  align="center">
                        <tr>
                            <td>
                                <table id="tabla_simulador_scoring" align="center" ></table>  
                                <div id="page_tabla_simulador_scoring"></div>
                            </td>
                        </tr>
                    </table>
                </div> 
                </br>      
            </center>
            <div id="dialogLoading" style="display:none;">
                <p  style="font-size: 12px;text-align:justify;" id="msj2">Texto </p> <br/>
                <center>
                    <img src="./images/cargandoCM.gif"/>
                </center>
            </div>
            <div id="dialogMsj" title="Mensaje" style="display:none;">
                <p style="font-size: 12.5px;text-align:justify;" id="msj" > Texto </p>
            </div>  
        </div>      
    </body>
</html>
