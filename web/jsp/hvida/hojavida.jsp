<%@page contentType="text/html"%>
<%@page errorPage="/error/ErrorPage.jsp"%>
<%@page import="com.tsp.operation.model.beans.*, java.util.*"%>
<%@include file="/WEB-INF/InitModel.jsp" %>
<%@ taglib uri="http://jakarta.apache.org/taglibs/input-0.90" prefix="input" %>
<%String path = application.getRealPath(model.menuService.getRealUrl(request.getRequestURI()));
  String datos[] = model.menuService.getContenidoMenu(BASEURL,request.getRequestURI(),path);
%>
<%String msg = (request.getParameter("msg")!=null)?request.getParameter("msg"):"";
  String placa = (request.getParameter("placa")!=null && !request.getParameter("placa").equals(""))?request.getParameter("placa"):"prede.jpg";
  String huella = (request.getParameter("huella")!=null)?request.getParameter("huella"):"none.jpg";
  String huella2 = (request.getParameter("huella2")!=null)?request.getParameter("huella2"):"none.jpg";
  String firma = (request.getParameter("firma")!=null)?request.getParameter("firma"):"none.jpg";
  String cond = (request.getParameter("cond")!=null && !request.getParameter("cond").equals(""))?request.getParameter("cond"):"prede.jpg";
  String agencrea = (request.getParameter("agencrea")!=null)?request.getParameter("agencrea"):"";
  String agenmod = (request.getParameter("agenmod")!=null)?request.getParameter("agenmod"):"";
  String tipo = "";
  String rh = "";
  Proveedor pro = (Proveedor) request.getAttribute("propietario");
  Placa pla = (Placa) request.getAttribute("placa");
 %>
<html>
<head>
<title>HOJA DE VIDA</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link href="../../css/estilostsp.css" rel="stylesheet" type="text/css"> 
<link href="<%=BASEURL%>/css/estilostsp.css" rel="stylesheet" type="text/css"> 
<script type='text/javascript' src="<%=BASEURL%>/js/Validacion.js"></script> 
<script type='text/javascript' src="<%=BASEURL%>/js/referencia.js"></script> 
<script type='text/javascript' src="<%=BASEURL%>/js/boton.js"></script> 
<script type='text/javascript' src="<%=BASEURL%>/js/general.js"></script> 
<script type='text/javascript' src="<%=BASEURL%>/js/validar.js"></script> 
<style type="text/css">
<!--
.texto_sinBorde{
    font-family: Tahoma, Arial;
	font-size:11px;
	color:#003399;
	background-color:#D1DCEB;
	border-style:none;	
}
.Estilo1 {
	color: #FF0000;
	font-weight: bold;
    font-family: Tahoma, Arial;
	font-size: small;
	font-size: 12px;
	background-color:#D1DCEB;
}
.Estilo2 {	color: #FF0000;
	font-weight: bold;
    font-family: Tahoma, Arial;
	font-size: small;
	font-size: 12px;
	background-color:#D1DCEB;
}
-->
</style>
</head>
<body onLoad="redimensionar();" onResize="redimensionar();">
<div id="capaSuperior" style="position:absolute; width:100%; height:100px; z-index:0; left: 0px; top: 0px;">
<jsp:include page="/WEB-INF/toptsp.jsp?encabezado=Hoja de Vida"/>
</div>

<div id="capaCentral" style="position:absolute; width:100%; height:85%; z-index:0; left: 0px; top: 100px; overflow: scroll;"> 
<%
String pag = (request.getParameter("buscar")!=null) ? request.getParameter("buscar") : "";
if (!pag.equals("no")) {
%>
<form name="frmbuscarplaca" action="<%=CONTROLLER%>?estado=Placa&accion=Search&cmd=show&pag=hoja" method="post">
<table border="2" align="center" width="797">
  <tr>
    <td width="818" height="72">
<table width="99%" align="center"  >
  <tr>
    <td width="392" height="24"  class="subtitulo1"><p align="left">Buscar Placa </p></td>
    <td width="392"  class="barratitulo"><img src="<%=BASEURL%>/images/titulo.gif" width="32" height="20" align="left"><%=datos[0]%></td>
  </tr>
</table>
<table width="99%" align="center" >
<tr class="fila">
    <td width="112" height="30">Digite la placa</td>
    <td width="668" ><input name="idplaca" type="text" class="textbox" title="Numero de la placa" maxlength="6" onKeyPress="soloAlfa(event)" onKeyUp="if(event.keyCode == 13 && frmbuscarplaca.idplaca.value!='' ){frmbuscarplaca.submit();}" style="text-transform:uppercase" > 
                     <img src="<%=BASEURL%>/images/botones/buscar.gif" alt="buscar"  name="buscar" height="21" align="absbottom" style="cursor:hand" onClick="if(frmbuscarplaca.idplaca.value!=''){frmbuscarplaca.submit();}else{alert('Digite el  numero de la placa');}" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);">
</td>
</tr>
</table></td>
</tr>
</table>
<%if(!msg.equals("OK") && !msg.equals("")){%>
	<p>
   <table border="2" align="center">
     <tr>
    <td><table width="100%"  align="center"  >
      <tr>
        <td width="229" align="center" class="mensajes"> <%=msg%></td>
        <td width="29" background="<%=BASEURL%>/images/cuadronaranja.JPG">&nbsp;</td>
        <td width="58">&nbsp;</td>
      </tr>
    </table></td>
  </tr>
</table>
  </p>
   <%}%>
</form>
<% } %>
<%
 Conductor conductor = null;
 Identidad iden = null;
 if (msg.equals("OK")){
     conductor = model.conductorService.obtConductor();
     iden = model.identidadService.obtIdentidad();
     TreeMap ciudades = model.ciudadService.getTreMapCiudades(); 
 
%>
<input:form name="frmplaca" method="post" bean="placa">
<table border="2" align="center" width="797">
  <tr>
    <td width="818">
<table width="100%" align="center"   >
  <tr>
    <td width="392" height="24"  class="subtitulo1"><p align="left">Informacion del Vehiculo </p></td>
    <td width="392"  class="barratitulo" align="right"><img src="<%=BASEURL%>/images/titulo.gif" width="32" height="20" align="left"><div align="right" class="Estilo1"><% String aprobado = (pla!=null && pla.getAprobado().equals("N"))? "NO APROBADO &nbsp; - &nbsp;" :""; %><%=aprobado%>  <% String vetopla = (pla!=null && pla.getVeto().equals("S"))? "REPORTADO" :""; %><%=vetopla%>&nbsp;</div></td>
  </tr>
</table>
<table width="100%" align="center" >
<tr class="fila">
    <td width="245" rowspan="7" align="center" class="filaresaltada" ><img src="<%=BASEURL%>/documentos/imagenes/<%=placa%>"  onClick="window.open('<%=BASEURL%>/documentos/imagenes/<%=placa%>','','scrollbars=yes,resizable=yes');" title="Haga click para agrandar" style="cursor:hand " width="241" height="175" ></td>
    <td width="62" height="22" class="filaresaltada">Placa cabezote </td>
    <td width="122" ><input:text name="placa" attributesText="size='8' readonly class='texto_sinBorde'"/> </td>
    <td width="79" class="filaresaltada">Tipo</td>
    <td width="42"><input:text name="tipo" attributesText="maxlength='12' size='13' readonly class='texto_sinBorde'"/></td>
    <td width="81" class="filaresaltada">Chasis No </td>
    <td width="114"><input:text name="nochasis" attributesText="maxlength='15' size='16' readonly class='texto_sinBorde'"/></td>
</tr>
<tr class="fila">
  <td width="62" height="22" class="filaresaltada">Marca</td>
    <td width="122" >      <input:text name="marca" attributesText="style='width:122;' readonly class='texto_sinBorde'"/></td>
    <td width="79" class="filaresaltada">Modelo</td>
    <td width="42"><input:text name="modelo" attributesText="class='texto_sinBorde'readonly  size='10' onkeypress='return acceptNum(event)' maxlength='4''"/></td>
    <td class="filaresaltada">Color</td>
    <td width="114"><input:text name="color"  attributesText="style='width:114;'  readonly class='texto_sinBorde'"/></td>
</tr>
<tr class="fila">
  <td width="62" height="22" class="filaresaltada">Carroceria</td>
    <td width="122" ><input:text name="carroceria" attributesText="style='width:122;'  readonly class='texto_sinBorde'"/></td>
    <td width="79" class="filaresaltada">Placa Trailer</td>
    <td width="42"><input:text name="placa_trailer" attributesText="maxlength='15' size='8' readonly class='texto_sinBorde'"/></td>
    <td class="filaresaltada">Agencia</td>
    <td width="114"><input:text name="agencia" attributesText="style='width:114;' class='texto_sinBorde'" /></td>
</tr>
<tr class="fila">
  <td width="62" height="22" class="filaresaltada">Condici&oacute;n</td>
    <td width="122" ><input:text name="condicion" attributesText="maxlength='1' readonly size='13' class='texto_sinBorde'"/></td>
    <td width="79" class="filaresaltada">Homologado</td>
    <td width="42"><input:text name="homologado" attributesText="maxlength='1' size='13' readonly class='texto_sinBorde'"/></td>
    <td><span class="filaresaltada">Numero ejes </span></td>
    <td width="114"><input:text name="noejes" attributesText="maxlength='1' size='8' readonly onkeypress='return acceptNum(event)' class='texto_sinBorde'"/></td>
</tr>
<tr class="fila">
  <td width="62" height="22" class="filaresaltada">Numero Rin </td>
    <td width="122" ><input:text name="numero_rin" attributesText="maxlength='1' size='13' readonly class='texto_sinBorde'"/></td>
    <td width="79" class="filaresaltada">Capacidad</td>
    <td width="42"><input:text name="capacidad" attributesText="maxlength='2' readonly  size='4' class='texto_sinBorde'"/></td>
    <td class="filaresaltada">Motor No. </td>
    <td width="114"><input:text name="nomotor" attributesText="maxlength='15' readonly class='texto_sinBorde'"/></td>
</tr>
<tr>
  <td height="22" colspan="6" class="subtitulo1"><div align="center">Empresa Afiliadora</div></td>
    </tr>
<tr class="fila">
  <td width="62" height="22" class="filaresaltada">Nombre</td>
    <td width="122" ><input:text name="empresaafil" attributesText="maxlength='1' size='13' readonly class='texto_sinBorde'"/></td>
    <td width="79" class="filaresaltada">Pais</td>
    <td width="42"><input:text name="paisAfiliada" attributesText="maxlength='1' size='13' readonly class='texto_sinBorde'"/></td>
    <td class="filaresaltada">Ciudad</td>
    <td width="114"><input:text name="ciudadAfiliada" attributesText="maxlength='1' size='13' readonly class='texto_sinBorde'"/></td>
</tr>


</table>
<table width="100%" align="center"   >
<tr class="subtitulo1">
    <td height="18" colspan="6"><div align="center">Documentaci&oacute;n</div></td>
    </tr>
	<tr class="tblTitulo">
    <td width="125" height="18" align="left">Clase Documento</td>
    <td width="129" align="center">Documento</td>
    <td width="111" align="center">Vencimiento</td>
    <td width="129" align="center">Clase Documento</td>
    <td width="135" align="center">Documento</td>
    <td width="108" align="center">Vencimiento</td>
    </tr>
  <tr class="fila">
    <td width="125" height="30" class="filaresaltada">Tarjeta de Operaci&oacute;n </td>
    <td width="129" ><div align="center"><input:text name="tarjetaoper" attributesText="readonly size='19' class='texto_sinBorde'"/></div></td>
    <td  ><div align="center"><input:text name="venctarjetaoper" attributesText="maxlength='10' size='11' readonly class='texto_sinBorde' readonly"/></div></td>
    <td width="129" class="filaresaltada">Tarjeta Empresarial </td>
    <td width="135"><div align="center"><input:text name="tarempresa" attributesText="maxlength='15'  readonly size='19' class='texto_sinBorde'"/></div></td>
    <td><div align="center"><input:text name="fecvenempresa" attributesText="readonly  size='11' class='texto_sinBorde' readonly"/></div></td>
    </tr>
  <tr class="fila">
    <td width="125" height="32" class="filaresaltada">Tarjeta de Habilitaci&oacute;n </td>
    <td width="129" ><div align="center"><input:text name="tarhabil" attributesText="maxlength='15'  size='19' readonly class='texto_sinBorde'"/></div></td>
    <td  ><div align="center"><input:text name="fecvenhabil" attributesText="maxlength='15'  readonly size='11' class='texto_sinBorde' readonly"/></div></td>
    <td width="129" class="filaresaltada">Reg. nal de carga </td>
    <td width="135"><div align="center"><input:text name="reg_nal_carga" attributesText="maxlength='15'  size='19' readonly class='texto_sinBorde'"/></div></td>
    <td ><div align="center"><input:text name="fecvenreg" attributesText="readonly  size='11' class='texto_sinBorde' readonly"/></div></td>
	  </tr>
  <tr class="fila">
    <td width="125" height="30" class="filaresaltada">Tarjeta de Propiedad </td>
    <td width="129" ><div align="center"><input:text name="tarprop" attributesText="readonly  size='19' class='texto_sinBorde'"/></div></td>
    <td ><div align="center"><input:text name="fecvenprop" attributesText="readonly  size='11' class='texto_sinBorde' readonly"/></div></td>
    <td width="129" class="filaresaltada">Poliza Andina </td>
    <td width="135"><div align="center"><input:text name="poliza_andina" attributesText="readonly  size='19' class='texto_sinBorde'"/></div></td>
    <td ><div align="center"><input:text name="fecvenandina" attributesText="readonly  size='11' class='texto_sinBorde' readonly"/></div></td>
    </tr>
  <tr class="fila">
    <td width="125" height="30" class="filaresaltada">Poliza SOAT </td>
    <td width="129" ><div align="center"><input:text name="polizasoat" attributesText="readonly  size='19' class='texto_sinBorde'"/></div></td>
    <td ><div align="center"><input:text name="venseguroobliga" attributesText="maxlength='15'  size='11' class='texto_sinBorde' readonly"/></div></td>
    <td width="129" class="filaresaltada">Cert. de Emisi&oacute;n de Gases </td>
    <td width="135"><div align="center"><input:text name="certgases" attributesText="readonly  size='19' class='texto_sinBorde'"/></div></td>
    <td ><div align="center"><input:text name="fecvengases" attributesText="readonly  size='11' class='texto_sinBorde' readonly"/></div></td>
    </tr>
  
</table>
</td>
</tr>
</table>
<table width="797" border="2" align="center" class="tabla">
  <tr>
    <td  height="30">
<table width="99%" align="center"   >
</table>
<table width="100%" height="10" align="center"  >
  <tr class="fila">
    <td width="50%" height="16"><div align="center">      <a href="javascript:modReferenciaPlaca('<%=CONTROLLER%>')" class="Simulacion_Hiper">Ver Referencias de este Vehiculo</a> </div></td>
    <td width="50%" ><div align="center">      <a href="javascript:openWindow('<%=CONTROLLER%>?estado=Veridocumen&accion=Buscar&evento=Placa&pagina=documento.jsp&carpeta=jsp/hvida/veridocumen&placa='+document.frmplaca.placa.value)" class="Simulacion_Hiper">    Verificar Documentos de este Vehiculo </a></div></td>
  </tr>
</table>
    </td>
  </tr>
</table>

</input:form>
<input:form name="frmprop"  method="post" bean="propietario">
<table border="2" align="center" width="797">
  <tr>
    <td width="818">

<table width="100%" align="center"   >
  <tr>
    <td width="392" height="24"  class="subtitulo1"><p align="left">Informaci&oacute;n del Propietario </p></td>
    <td width="392"  class="barratitulo"><img src="<%=BASEURL%>/images/titulo.gif" width="32" height="20" align="left"><div align="right" class="Estilo1"><% String veto1 = (pro!=null && pro.getVeto().equals("S"))? "REPORTADO" :""; %><%=veto1%>&nbsp;</div></td>
  </tr>
</table>

<table width="100%" align="center" >
  <tr class="fila">
    <td width="113" height="18" class="filaresaltada">Cedula</td>
    <td width="127"><input:text name="c_nit" attributesText="size='12' readonly class='texto_sinBorde'"/></td>
    <td width="62" class="filaresaltada">Lugar de Expedicion </td>
    <td colspan="5"><input:text name="expced" attributesText="size='18' readonly class='texto_sinBorde'"/></td>
    </tr>
  <tr class="fila">
    <td height="18" class="filaresaltada">Nombres y Apellidos</td>
    <td colspan="3"><input:text name="Nombre" attributesText=" size='38' readonly class='texto_sinBorde'"/>
	</td>
    <td width="83" class="filaresaltada">Ciudad/Pais</td>
    <td width="89"><input:text name="ciudad" attributesText="readonly size='15' class='texto_sinBorde'"/> <input:text name="pais" attributesText="readonly size='15' class='texto_sinBorde'"/></td>
    <td width="92" class="filaresaltada">Telefono</td>
    <td width="83"><input:text name="telefono" attributesText="readonly size='13' class='texto_sinBorde'"/></td>
  </tr>
  <tr class="fila">
    <td height="18" class="filaresaltada">Direcci&oacute;n</td>
    <td><input:text name="direccion" attributesText="readonly size='20' class='texto_sinBorde'"/></td>
    <td class="filaresaltada">Celular</td>
    <td width="76"><input:text name="celular" attributesText="readonly size='15' class='texto_sinBorde'"/></td>
    <td class="filaresaltada">Sede de Pago </td>
    <td nowrap><input:text name="C_agency_id" attributesText="readonly size='15' class='texto_sinBorde'"/></td>
    <td class="filaresaltada">Grado de riesgo </td>
    <td>&nbsp;</td>
  </tr>
</table>
</td>
</tr>
</table>
<table width="797" border="2" align="center" height="38" class="tabla">
  <tr>
    <td  height="30">
<table width="99%" align="center"   >
</table>
<table width="100%" height="21" align="center"  >
  <tr>
    <td height="16" class="filaresaltada"><div align="center">      <a href="javascript:modReferenciaPropietario('<%=CONTROLLER%>')" class="Simulacion_Hiper">Ver Referencias del Propietario</a> </div>      
    <div align="center">
      </div></td>
    </tr>
</table>
    </td>
  </tr>
</table>
</input:form>
<form name="formulario">
<table width="797" border="2" align="center">
    <tr>
      <td>
	  <table width="100%" align="center"   >
  <tr>
    <td width="392" height="22"  class="subtitulo1"><p align="left">Informaci&oacute;n del Conductor </p></td>
    <td width="392"  class="barratitulo"><img src="<%=BASEURL%>/images/titulo.gif" width="32" height="20" align="left"><div align="right" class="Estilo1"><% String veto = (iden!=null && iden.getVeto().equals("S"))? "REPORTADO" :""; %><%=veto%>&nbsp;</div></td>
  </tr>
</table>
<table width="100%" align="center" >
  <tr class="fila">
    <td colspan="2" rowspan="6" class="fila"><span class="filaresaltada"><img src="<%=BASEURL%>/documentos/imagenes/<%=cond%>" onClick="window.open('<%=BASEURL%>/documentos/imagenes/<%=cond%>','','scrollbars=yes,resizable=yes');" title="Haga click para agrandar" style="cursor:hand" width="241" height="175"></span></td>
    <td width="91" class="fila"><span class="filaresaltada">Cedula</span></td>
    <td width="175" class="fila"><span class="filaresaltada">
      <% String est1=(iden!=null && iden.getCedula()!=null)?iden.getCedula():"";%>
      <input name='identificacion' type='text'  class='texto_sinBorde' id="identificacion" onKeyPress="soloDigitos(event,'decNO')"  value='<%=est1%>' size='15' maxlength='15' readonly >
    </span></td>
    <td width="81" class="fila"><span class="filaresaltada">Expedicion </span></td>
    <td width="154" class="fila"><%String expced =(iden!=null && iden.getExpced()!=null)?iden.getExpced():"";%>
      <input name="expeced" type="text" class="texto_sinBorde" id="expeced2" value="<%=expced%>" readonly></td>
  </tr>
  <tr>
    <td class="fila"><span class="filaresaltada">Libreta Militar</span></td>
    <td class="fila"><%String lib =(iden!=null && iden.getLibmilitar()!=null)?iden.getLibmilitar():"";%>
      <input name="c_libreta" type="text" class="texto_sinBorde" id="c_libreta2" value="<%=lib%>" onKeyPress="soloDigitos(event,'decNO')" maxlength="15" readonly></td>
    <td class="fila"><span class="filaresaltada">Estado Civil</span></td>
    <td class="fila"><%String estcivil =(iden!=null && iden.getEst_civil()!=null)?iden.getEst_civil():"";
                     if (estcivil.equals("S")){
					 	 estcivil ="Soltero";
				     }
                     else if (estcivil.equals("C")){ 
					     estcivil="Casado";
				     }
                     else if (estcivil.equals("V")){
					     estcivil = "Viudo";
					 }
			         else if (estcivil.equals("E")){
					      estcivil = "Separado";
					 }
                     else if (estcivil.equals("U")){
					      estcivil = "Union Libre";
					 }%>
      <input name="c_estcivil" type="text" class="texto_sinBorde" id="c_estcivil3" value="<%=estcivil%>" size="15" readonly></td>
  </tr>
  <tr>
    <td class="fila"><span class="filaresaltada">Nombres</span></td>
    <td class="fila"><%String nom1 =(iden!=null && iden.getNom1()!=null)?iden.getNom1():"";
	      String nom2 =(iden!=null && iden.getNom2()!=null)?iden.getNom2():"";
		  nom1+=" "+nom2;
		%>      <input name="c_nom1" type="text" class="texto_sinBorde" id="c_nom13" value="<%=nom1%>" size="29" maxlength="15" readonly></td>
    <td class="fila"><span class="filaresaltada">Apellidos</span></td>
    <td class="fila"><%String ape1 =(iden!=null && iden.getApe1()!=null)?iden.getApe1():"";
	      String ape2 =(iden!=null && iden.getApe2()!=null)?iden.getApe2():"";
		  ape1+=" "+ape2;%>
      <input name="c_ape1" type="text" class="texto_sinBorde" id="c_ape1" value="<%=ape1%>" size="29" maxlength="15" readonly></td>
  </tr>
  <tr>
    <td class="fila"><span class="filaresaltada">Grupo Sangu&iacute;neo</span></td>
    <td class="letra"><% if (conductor != null && !conductor.getRh().equals("")){
                      if(conductor.getRh().replaceAll(" ", "").length() == 2){
                           tipo = conductor.getRh().replaceAll(" ", "").substring(0,1);
                           rh = conductor.getRh().replaceAll(" ", "").substring(1,2); 
                      }
					  else if(conductor.getRh().replaceAll(" ", "").length() == 3){
                           tipo = conductor.getRh().replaceAll(" ", "").substring(0,2);
                           rh = conductor.getRh().replaceAll(" ", "").substring(2,3);               
                      }
                  }%><%=tipo%>
      </td>
    <td class="fila"><span class="filaresaltada">RH</span></td>
    <td class="letra"><%=rh%></td>
  </tr>
  <tr>
    <td class="fila"><span class="filaresaltada">Fecha de Nacimiento</span></td>
    <td class="fila"><span class="comentario">
      <% String est4=(iden!=null && iden.getFechanac()!=null )?iden.getFechanac():""; %>
      <input  name='fechaNacimiento' class="texto_sinBorde" value='<%= est4%>' size="11" readonly>
      <a href="javascript:void(0)" onClick="if(self.gfPop)gfPop.fPopCalendar(fechaNacimiento);return false;" hidefocus> </a></span></td>
    <td class="fila">Lugar Nacimiento</td>
    <td class="letra"><% String lug = (iden!=null && iden.getLugarnac()!=null)?iden.getLugarnac():"";%><%=lug%>
	  </td>
  </tr>
  
  <tr>
    <td class="fila"><span class="filaresaltada">Direcci&oacute;n</span></td>
    <td class="fila"><%String dir =(iden!=null && iden.getDireccion()!=null)?iden.getDireccion():"";%>
      <input name="c_dir" type="text" class="texto_sinBorde" id="c_dir" value="<%=dir%>" size="35" maxlength="40" readonly></td>
    <td class="fila"><span class="filaresaltada">Barrio</span></td>
    <td class="fila"><%String barrio =(iden!=null && iden.getBarrio()!=null)?iden.getBarrio():"";%>
      <input name="barrio" type="text" class="texto_sinBorde" id="barrio" value="<%=barrio%>" readonly></td>
  </tr>
  <tr class="fila">
    <td width="107"><span class="filaresaltada">Celular</span></td>
    <td width="129"><%String cel =(iden!=null && iden.getCelular()!=null)?iden.getCelular():"";%>     
	 <input name="c_cel" type="text" class="texto_sinBorde" id="c_cel4" onKeyPress="soloDigitos(event,'decNO')" value="<%=cel%>" maxlength="15" readonly></td>
    <td><span class="filaresaltada">Telefono1</span></td>
    <td><%String t11 =(iden!=null && iden.getPais1()!=null)?iden.getPais1():"";
	      String t12 =(iden!=null && iden.getArea1()!=null)?iden.getArea1():"";      
	      String t13 =(iden!=null && iden.getnum1()!=null)?iden.getnum1():"";
		  t11+=" "+t12+" "+t13;%>
      <input name="c_tel13" type="text" class="texto_sinBorde" id="c_tel13" onKeyPress="soloDigitos(event,'decNO')" readonly value="<%=t11%>" size="21" maxlength="11"></td>
    <td><span class="filaresaltada">Telefono2</span></td>
    <td><span class="filaresaltada">
      <%String t21 =(iden!=null && iden.getPais2()!=null)?iden.getPais2():"";
	    String t22 =(iden!=null && iden.getArea2()!=null)?iden.getArea2():"";
	    String t23 =(iden!=null && iden.getnum2()!=null)?iden.getnum2():"";
		t21+=" "+t22+" "+t23;%>
<input name="c_tel23" type="text" class="texto_sinBorde" id="c_tel23" onKeyPress="soloDigitos(event,'decNO')" value="<%=t21%>" maxlength="11" readonly>
    </span></td>
  </tr>
</table>
	  
 <table width="100%" align="center" >
          <tr class="subtitulo1">
            <td colspan="7" align="center">Documentos</td>
          </tr><tr class="tblTitulo">
            <td width="106" align="center">Tipo Documento</td>
            <td width="111" align="center">Documento</td>
            <td width="84"  align="center">Vencimiento</td>
            <td colspan="4" align="center">Otra informaci&oacute;n</td>
            </tr>

          <tr>
            <td width="106" class="fila">N&uacute;mero Pase</td>
            <td width="111" align="center" class="letra"><% String est5=(conductor!=null && !conductor.getPassNo().equals(""))?conductor.getPassNo():""; %>
                  <input class='texto_sinBorde' value='<%= est5%>' type='text' name='numeroPase' size='20' maxlength='20' readonly onKeyPress="soloDigitos(event,'decNO')">
            </td>
            <td width="84" class="filaresaltada"><div align="center"><span class="comentario">
                <% String vpase=(conductor!=null && !conductor.getPassExpiryDate().equals("0099-01-01"))?conductor.getPassExpiryDate():""; %>
                <input name='VencePase' type='text' class='texto_sinBorde' id="VencePase2" value="<%=vpase%>"  size='10' maxlength='10'  readonly='readonly'>
              <a href="javascript:void(0)" onClick="if(self.gfPop)gfPop.fPopCalendar(VencePase);return false;" hidefocus> </a></span></div></td>
            <td width="87" class="fila">Categoria del pase</td>
            <td width="152" class="letra"><%=( conductor!=null && !conductor.getPassCat().equals("") )?conductor.getPassCat():""%> 
            </td>
            <td width="91" class="fila">Arp</td>
            <td width="100" class="letra"><% String arp=(conductor!=null && !conductor.getNomarp().equals(""))?conductor.getNomarp():""; %>
              <input name="arp" type="text" class="texto_sinBorde" id="arp3" value="<%=arp%>" size="20" maxlength="18" readonly></td>
          </tr>
          <tr class="fila">
            <td class="filaresaltada">Pasaporte</td>
            <td><div align="center">
                <% String pasa=(conductor!=null && !conductor.getPassport().equals(""))?conductor.getPassport():""; %>
                  <input name='pasaporte' type='text' class='texto_sinBorde' id="pasaporte" onKeyPress="soloDigitos(event,'decNO')" readonly value="<%=pasa%>" size='20' maxlength='20'>
            </div></td>
            <td class="filaresaltada" align="center">
                <% String vpasa=(conductor!=null && !conductor.getPassportExpiryDate().equals("0099-01-01"))?conductor.getPassportExpiryDate():""; %>
                <input  name='VencePasaporte' type='text' class='texto_sinBorde' id="VencePasaporte2" value='<%= vpasa%>' size='10' maxlength='10'   readonly='readonly'>
             </td>
            <td><span class="comentario"><span class="filaresaltada">Restricciones del pase</span></span> </td>
            <td><span class="comentario">
              <% String resp=(conductor!=null && !conductor.getDesrespase().equals(""))?conductor.getDesrespase():""; %>
              <input name="respase" type="text" class="texto_sinBorde" id="respase4" value="<%=resp%>" size="25" readonly>            
              </span></td>
            <td><span class="filaresaltada">Fecha Afiliaci&oacute;n</span></td>
            <td><% String aarp=(conductor!=null && !conductor.getFecafiarp().equals("0099-01-01"))?conductor.getFecafiarp():""; %>
              <input  name='Vencearp' type='text' class='texto_sinBorde' id="Vencearp3" value="<%=aarp%>" size='10' maxlength='10'   readonly='readonly'>
              <a href="javascript:void(0)" onClick="if(self.gfPop)gfPop.fPopCalendar(Vencearp);return false;" hidefocus> </a></td>
          </tr>
          <tr class="fila">
            <td height="40" class="filaresaltada">Certificado Judicial </td>
            <td><div align="center">
                <% String jud=(conductor!=null && !conductor.getNrojudicial().equals(""))?conductor.getNrojudicial():""; %>
                  <input name="Judicial" type="text" class="texto_sinBorde" id="Judicial"  readonly onKeyPress="soloDigitos(event,'decNO')" value="<%=jud%>" size="20">
            </div></td>
            <td class="filaresaltada"><div align="center">
                <% String vjud=(conductor!=null && !conductor.getVencejudicial().equals("0099-01-01"))?conductor.getVencejudicial():""; %>
                <input  name='VenceJudicial' type='text' class='texto_sinBorde' id="VenceJudicial2" value="<%=vjud%>" size='10'maxlength='10'   readonly>
                  <a href="javascript:void(0)" onClick="if(self.gfPop)gfPop.fPopCalendar(VenceJudicial);return false;" hidefocus> </a></div></td>
            <td><span class="filaresaltada">Numero Afiliaci&oacute;n Eps </span> </td>
            <td><% String neps=(conductor!=null && !conductor.getNroeps().equals(""))?conductor.getNroeps():""; %>
              <input name="nroeps" type="text" class="texto_sinBorde" id="nroeps2" onKeyPress="soloDigitos(event,'decNO')" readonly value="<%=neps%>" size="20"></td>
            <td><span class="filaresaltada">Grado Riesgo </span></td>
            <td><% int grie = Integer.parseInt((conductor!=null)?conductor.getGradoriesgo():"0"); %>
              <span class="filaresaltada">
              <input  name='select' type='text' class='texto_sinBorde' id="VenceVisa" value="<%=grie%>" size='10' maxlength='10'   readonly='readonly'>
              </span></td>
          </tr>
          <tr class="fila">
            <td height="31" class="filaresaltada">Visa</td>
            <td align="center">
                <% String vi=(conductor!=null && !conductor.getNrovisa().equals(""))?conductor.getNrovisa():""; %>
                  <input name="visa" type="text" class="texto_sinBorde" id="visa" onKeyPress="soloDigitos(event,'decNO')" readonly value="<%=vi%>" size="20">
            </td>
            <td class="filaresaltada" align="center">
                <% String vvi=(conductor!=null && !conductor.getVencevisa().equals("0099-01-01"))?conductor.getVencevisa():""; %>
                <input  name='VenceVisa' type='text' class='texto_sinBorde' id="VenceVisa2" value="<%=vvi%>" size='10' maxlength='10'   readonly='readonly'>
              </td>
            <td><span class="filaresaltada">Nombre Eps</span> </td>
            <td><% String nomeps=(conductor!=null && !conductor.getNomeps().equals(""))?conductor.getNomeps():""; %>
              <input name="eps" type="text" class="texto_sinBorde" id="eps3" value="<%=nomeps%>" readonly size="26"></td>
            <td class="filaresaltada">Fecha Afiliaci&oacute;n</td>
            <td><% String aeps=(conductor!=null && !conductor.getFecafieps().equals("0099-01-01"))?conductor.getFecafieps():""; %>
              <input  name='Afiliaeps' type='text' class='texto_sinBorde' id="Afiliaeps3" value="<%=aeps%>" size='10' maxlength='10'   readonly='readonly'>
              </td>
          </tr>
          <tr class="fila">
            <td height="23" class="filaresaltada">Libreta Tripulante </td>
            <td align="center"><%String libr=(conductor!=null && !conductor.getNrolibtripulante().equals(""))?conductor.getNrolibtripulante():""; %><input name="libTripulante" type="text" class="texto_sinBorde" id="libTripulante" onKeyPress="soloDigitos(event,'decNO')" value="<%=libr%>" readonly size="20"></td>
            <td class="filaresaltada" align="center">
                <% String vlib=(conductor!=null && !conductor.getVencelibtripulante().equals("0099-01-01"))?conductor.getVencelibtripulante():""; %>
                <input  name='VenceLibTripulante' type='text' class='texto_sinBorde' id="VenceLibTripulante2" value="<%=vlib%>" size='10' maxlength='10'   readonly='readonly'>
            </td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
          </tr>
      </table></td>
    </tr>
</table>
<table width="797" border="2" align="center" class="tabla">
  <tr>
    <td>
<table width="100%" align="center"   >
</table>
<table width="100%" height="15" align="center"  >
  <tr>
    <td width="50%" height="16" class="filaresaltada"><div align="center">      <a href="javascript:modReferenciaConductor('<%=CONTROLLER%>')" class="Simulacion_Hiper">Ver Referencias de este Conductor </a></div></td>
    <td width="50%" class="filaresaltada"><div align="center">      <a href="javascript:openWindow('<%=CONTROLLER%>?estado=Veridocumen&accion=Buscar&evento=Conductor&pagina=documento.jsp&carpeta=jsp/hvida/veridocumen&identificacion='+document.formulario.identificacion.value)" class="Simulacion_Hiper">    Verificar Documentos de este Conductor </a></div></td>
  </tr>
</table>    </td>
  </tr>
</table>
</form>
<input:form name="frmTen" action="controller?estado=Placa&accion=Update&cmd=show" method="post" bean="tenedor">
<table border="2" align="center" width="797">
  <tr>
    <td>

<table width="100%" align="center"   >
  <tr>
    <td width="392" height="24"  class="subtitulo1"><p align="left">Informaci&oacute;n del Tenedor </p></td>
    <td width="392"  class="barratitulo"><img src="<%=BASEURL%>/images/titulo.gif" width="32" height="20"></td>
  </tr>
</table>

<table width="100%" align="center" >
  <tr class="fila">
    <td width="131" height="18" class="filaresaltada">Cedula</td>
    <td width="125"><input:text name="cedula" attributesText="size='12' class='texto_sinBorde' readonly"/></td>
    <td width="69" class="filaresaltada">Lugar de Expedicion </td>
    <td colspan="5"><input:text name="expedicionced" attributesText="size='18' readonly class='texto_sinBorde'"/></td>
    </tr>
  <tr class="fila">
    <td height="18" class="filaresaltada">Nombres y Apellidos</td>
    <td colspan="3"><input:text name="nombre" attributesText=" size='38' readonly class='texto_sinBorde'"/>
	</td>
    <td width="84" class="filaresaltada">Ciudad/Pais</td>
    <td width="83"><input:text name="ciudad" attributesText="readonly size='20' class='texto_sinBorde'"/> <input:text name="pais" attributesText="readonly size='20' class='texto_sinBorde'"/></td>
    <td width="62" class="filaresaltada">Telefono</td>
    <td width="98"><input:text name="telefono" attributesText="readonly size='13' class='texto_sinBorde'"/></td>
  </tr>
  <tr class="fila">
    <td height="18" class="filaresaltada">Direcci&oacute;n</td>
    <td><input:text name="direccion" attributesText="readonly size='20' class='texto_sinBorde'"/></td>
    <td class="filaresaltada">Celular</td>
    <td width="73"><input:text name="celular" attributesText="readonly size='9' class='texto_sinBorde'"/></td>
    <td class="filaresaltada">A titulo </td>
    <td colspan="3"><input:text name="atitulo" attributesText="readonly size='9' class='texto_sinBorde'"/></td>
    </tr>
</table>
</td>
</tr>
</table>
</input:form>
<input:form name="frpl" action="controller?estado=Placa&accion=Update&cmd=show" method="post" bean="placa">
  <table border="2" align="center" width="797">
    <tr>
      <td width="818">
  
<table width="100%" align="center"   >
    <tr>
      <td width="392" height="24"  class="subtitulo1"><p align="left">Control de Registro </p></td>
      <td width="392"  class="barratitulo"><img src="<%=BASEURL%>/images/titulo.gif" width="32" height="20" align="left"></td>
    </tr>
  </table>
  
<table width="100%" height="89" align="center" >
    <tr>
      <td width="155" height="18" class="fila">Fecha de Creaci&oacute;n</td>
      <td width="121" class="letra"><input:text name="fechacrea" attributesText="readonly size='8' class='texto_sinBorde'"/></td>
      <td width="95" class="filaresaltada">Usuario</td>
      <td width="197" class="letra"><input:text name="usuariocrea" attributesText="readonly size='9' class='texto_sinBorde'"/></td>
      <td width="67" class="filaresaltada">Agencia</td>
      <td width="122" class="letra"><%=agencrea%></td>
    </tr>
    <tr >
      <td height="18" class="fila">Fecha de Actualizaci&oacute;n</td>
      <td class="letra"><input:text name="fechaultact" attributesText="readonly size='8' class='texto_sinBorde'"/></td>
      <td class="filaresaltada">Usuario</td>
      <td class="letra"><input:text name="usuario" attributesText="readonly size='9' class='texto_sinBorde'"/></td>
      <td class="filaresaltada">Agencia</td>
      <td class="letra"><%=agenmod%></td>
    </tr>
    <tr class="fila" align="center">
      <td height="20" colspan="2" align="center">Huella 1 <img src="<%=BASEURL%>/images/botones/iconos/documento.gif" alt="Adjuntar Imagen" width="20" height="15" 
						  onClick="AdjuntarDoc('<%=CONTROLLER%>','','015','<%=(conductor!=null)?conductor.getHuella_der():""%>');" style="cursor:hand"> <%=(conductor!=null)?conductor.getDesHuella1():""%></td>
      <td height="20" colspan="2"><span class="filaresaltada">Huella 2 <img src="<%=BASEURL%>/images/botones/iconos/documento.gif" alt="Adjuntar Imagen" width="20" height="15" 
						  onClick="AdjuntarDoc('<%=CONTROLLER%>','','016','<%=(conductor!=null)?conductor.getHuella_izq():""%>');" style="cursor:hand">  <%=(conductor!=null)?conductor.getDesHuella2():""%> </span> </td>
      <td height="18" class="filaresaltada" colspan="2" align="center">Firma Conductor <img src="<%=BASEURL%>/images/botones/iconos/documento.gif" alt="Adjuntar Imagen" width="20" height="15" 
						  onClick="AdjuntarDoc('<%=CONTROLLER%>','','003','039');" style="cursor:hand"></td>
      </tr>
    <tr class="fila" align="center">
      <td height="21" colspan="2" align="center"><img src="<%=BASEURL%>/documentos/imagenes/<%=huella%>" width="50" height="60"></td>
      <td height="21" colspan="2" align="center"><img src="<%=BASEURL%>/documentos/imagenes/<%=huella2%>" width="50" height="60"></td>
      <td colspan="2" align="center"><span class="filaresaltada"><img src="<%=BASEURL%>/documentos/imagenes/<%=firma%>" width="100" height="60"></span></td>
    </tr>
  </table>
  </td>
  </tr>
  </table>
  <br>
  <center>
    <img src="<%=BASEURL%>/images/botones/imprimir.gif"  name="imgsalir"  height="21" onMouseOver="botonOver(this);" onClick="window.open('<%=CONTROLLER%>?estado=HojaVida&accion=PDF&cmd=show&pag=hoja','HVPDF','menubar=yes, resizable=yes, scrollbars=yes')" onMouseOut="botonOut(this);" style="cursor:hand"> &nbsp;<img src="<%=BASEURL%>/images/botones/restablecer.gif" name="restablecer"    onClick="window.location='<%=CONTROLLER%>?estado=Placa&accion=Search&cmd=show&pag=hoja&idplaca=<%=pla.getPlaca()%>'" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" style="cursor:hand">&nbsp;<img src="<%=BASEURL%>/images/botones/salir.gif"  name="imgsalir"  height="21" onMouseOver="botonOver(this);" onClick="parent.close();" onMouseOut="botonOut(this);" style="cursor:hand">
  </center>
  
</input:form>
<%}%>
</div>
<%=datos[1]%>
</body>
</html>
