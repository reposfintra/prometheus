<!--
- Autor : Ing. Leonardo Parody
- Date  : 13 de Enero de 2005
- Copyrigth Notice : Fintravalores S.A. S.A
-->
<%--
-@(#)
--Descripcion : Pagina JSP, que realiza reportes de las fotos subidas
--%>
<%@page contentType="text/html"%>
<%@page session="true"%>
<%@page import="java.util.*"%>
<%@page import="com.tsp.operation.model.*"%>
<%@page import="com.tsp.operation.model.beans.*"%>
<%@page import="com.tsp.operation.controller.*"%>
<%@page import="com.tsp.util.*"%>
<%@include file="/WEB-INF/InitModel.jsp"%>
<%@ taglib uri="http://jakarta.apache.org/taglibs/input-0.90" prefix="input" %>

<html>
<head>
<title>Reporte De Fotos</title>
<link href="<%= BASEURL %>/css/estilostsp.css" rel='stylesheet'>
<link href="../../../../css/estilostsp.css" rel="stylesheet" type="text/css">
<link href="/css/estilostsp.css" rel="stylesheet" type="text/css">
</head>
<script type='text/javascript' src="<%= BASEURL %>/js/validar.js"></script>
<script src='<%= BASEURL %>/js/date-picker.js'></script>
<script type='text/javascript' src="<%= BASEURL %>/js/boton.js"></script>
<body onresize="redimensionar()" onload = 'redimensionar()'>
<div id="capaSuperior" style="position:absolute; width:100%; height:100px; z-index:0; left: 0px; top: 1px;">
<jsp:include page="/toptsp.jsp?encabezado=Hoja de Vida"/></div>
<div id="capaCentral" style="position:absolute; width:100%; height:87%; z-index:0; left: -5px; top: 101px; overflow: scroll;">
<%TreeMap agencias = model.agenciaService.listar();
String      hoy       = Utility.getDate(4).replaceAll("/","-");
%>
<form name="forma" id="forma" method="post" action="<%=CONTROLLER%>?estado=Reporte&accion=PlacaNitFoto">
  <table width="300"  border="2" align="center" cellpadding="0" cellspacing="0">
    <tr>
      <td><table width="100%" class="tablaInferior">
        <tr>
          <td colspan="2" ><table width="100%"  border="0" cellpadding="0" cellspacing="1" class="barratitulo">
              <tr>
                <td width="56%" class="subtitulo1">&nbsp;Reporte de Fotos Subidas </td>
                <td width="44%" class="barratitulo"><img src="<%=BASEURL%>/images/titulo.gif" width="32" height="20"></td>
              </tr>
          </table></td>
        </tr>
        <tr class="fila">
          <td>Fecha Inicial </td>
          <td nowrap>
            <input name="FechaI" type='text' class="textbox" id="FechaI" style='width:120' value='<%=hoy%>' readonly>
           <a href="javascript:void(0)" onclick="if(self.gfPop)gfPop.fPopCalendar(document.forma.FechaI);return false;" HIDEFOCUS><img name="popcal" align="absmiddle" src="<%=BASEURL%>\js\Calendario\cal.gif" width="15" height="15" border="0" alt=""></a></td>
        </tr>
		 <tr class="fila">
          <td>Fecha Final </td>
          <td nowrap>
            <input name="FechaF" type='text' class="textbox" id="FechaF" style='width:120' value='<%=hoy%>' readonly>
           <a href="javascript:void(0)" onclick="if(self.gfPop)gfPop.fPopCalendar(document.forma.FechaF);return false;" HIDEFOCUS><img name="popcal" align="absmiddle" src="<%=BASEURL%>\js\Calendario\cal.gif" width="15" height="15" border="0" alt=""></a> </td>
        </tr>
		<tr class="fila">
          <td>Agencia </td>      
          <td ><input:select name="agencia" attributesText="class=textbox" options="<%= agencias %>" default="NADA"/></td>
		  </tr>
      </table></td>
    </tr>
  </table>
  <br>
  <center>
    <img src="<%=BASEURL%>/images/botones/aceptar.gif" name="c_aceptar" onClick="validarreporte();" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);"> 
    <img src="<%=BASEURL%>/images/botones/salir.gif" name="c_salir" onClick="parent.close();" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);">
  </center>
</form>
  <% if( !request.getParameter("msg").equals("") ){%>
  <p><table border="2" align="center">
  <tr>
    <td><table width="100%"  border="1" align="center"  bordercolor="#F7F5F4" bgcolor="#FFFFFF">
      <tr>
        <td width="229" align="center" class="mensajes"><%=request.getParameter("msg").toString()%></td>
        <td width="29" background="<%=BASEURL%>/images/cuadronaranja.JPG">&nbsp;</td>
        <td width="58">&nbsp;</td>
      </tr>
    </table></td>
  </tr>
</table>
</p>
 <%} %>
</div>
<iframe width=188 height=166 name="gToday:normal:agenda.js:gfPop:plugins.js" id="gToday:normal:agenda.js:gfPop:plugins.js" src="<%=BASEURL%>/js/Calendario/ipopeng.htm" scrolling="no" frameborder="0" style="visibility:visible; z-index:999; position:absolute; left:-500px; top:0px;"> </iframe>
</body>
<script>
function validarreporte(){
  	var fecha1 = document.forma.FechaI.value.replace('-','').replace('-','');
   	var fecha2 = document.forma.FechaF.value.replace('-','').replace('-','');
	var dia1 = fecha1.substring(6,8);
	var dia2 = fecha2.substring(6,8);
	var mes1 = fecha1.substring(4,6);
	var mes2 = fecha2.substring(4,6);
	var a�o1 = fecha1.substring(0,4);
	var a�o2 = fecha2.substring(0,4);
	if(fecha1>fecha2) {     
   		alert('La fecha final debe ser mayor que la fecha inicial');
		return (false);
    }else {
		forma.submit();
	}
}
</script>
</html>