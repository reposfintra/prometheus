<%@page contentType="text/html"%>
<%@page session="true"%>
<%@page import="java.util.*"%>
<%@page import="com.tsp.operation.model.*"%>
<%@page import="com.tsp.operation.model.beans.*"%>
<%@page import="com.tsp.util.*"%>
<%@include file="/WEB-INF/InitModel.jsp"%><html>
<head>
<title>Buscar Conductor</title>
<link href="<%= BASEURL %>/css/estilostsp.css" rel='stylesheet'>
<link href="../../../css/estilostsp.css" rel="stylesheet" type="text/css">
<script type='text/javascript' src="<%= BASEURL %>/js/validar.js"></script>
</head>
<script type='text/javascript' src="<%= BASEURL %>/js/boton.js"></script>             
<body onLoad="redimensionar();doFieldFocus( forma );" onresize="redimensionar()">
<div id="capaSuperior" style="position:absolute; width:100%; height:100px; z-index:0; left: 0px; top: 0px;">
	<jsp:include page="/WEB-INF/toptsp.jsp?encabezado=Datos del Conductor"/>
</div>
<div id="capaCentral" style="position:absolute; width:100%; height:87%; z-index:0; left: 0px; top: 100px; overflow: scroll;">
<%String res="";
String msg = (request.getParameter("men")!=null)?request.getParameter("men"):"" ;
if ( msg.equals("ok")){//exito en el ingreso
	res = "Conductor Modificado...";
}
Conductor con = model.conductorService.getCond();
String est = con.getEstado();
%>
<form name='forma' id="forma" method='POST' action="<%=CONTROLLER%>?estado=CambiarEstado&accion=Evento" >
<table width="350" border="2" align="center">
    <tr>
      <td><table width="100%" align="center"   >
        <tr>
          <td width="188" height="22"  class="subtitulo1"><p align="left"> Datos Conductor</p></td>
          <td width="188"  class="barratitulo"><img src="<%=BASEURL%>/images/titulo.gif" width="32" height="20"></td>
        </tr>
      </table>      <table width="100%" class="tablaInferior">
        <tr class="fila">
          <td width="88" >Estado</td>
          <td width="238" ><input name="est" type="radio" value="A" <%=(!est.equals("I"))?"checked":""%>>
            Activo
              <input name="est" type="radio" value="I" <%=(est.equals("I"))?"checked":""%>>
             Inactivo  
             <input type="hidden" name="tipo" value="Conductor"></td>
          </tr>
        <tr>
          <td class="fila">Cedula</td>
          <td class="letra"><%=con.getCedula()%><input  type="hidden" name='identificacion' size='15' maxlength='15' value="<%=con.getCedula()%>"  ></td>
        </tr>
        <tr>
          <td class="fila">Nombre</td>
          <td class="letra" nowrap><%=con.getNombre()%></td>
        </tr>
      </table></td>
    </tr>
  </table>
</form>
<div align="center"> <img src="<%=BASEURL%>/images/botones/aceptar.gif"  name="imgaceptar" onClick="forma.submit();" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" style="cursor:hand">
                     <img src="<%=BASEURL%>/images/botones/regresar.gif"  name="imgregresar" onClick="window.location='<%=CONTROLLER%>?estado=Menu&accion=Cargar&pagina=buscarConductor.jsp&carpeta=/jsp/hvida/cambiar_estado&marco=no'" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" style="cursor:hand"><img src="<%=BASEURL%>/images/botones/salir.gif"  name="imgsalir"  onMouseOver="botonOver(this);" onClick="parent.close();" onMouseOut="botonOut(this);" style="cursor:hand"> </div>
 <%if(!res.equals("")){%>
  <p><table border="2" align="center">
  <tr>
    <td><table width="100%"  border="1" align="center"  bordercolor="#F7F5F4" bgcolor="#FFFFFF">
      <tr>
        <td width="229" align="center" class="mensajes"><%=res%></td>
        <td width="29" background="<%=BASEURL%>/images/cuadronaranja.JPG">&nbsp;</td>
        <td width="58">&nbsp;</td>
      </tr>
    </table></td>
  </tr>
</table>
<%}%>
</div>
</body>

</html>

