<%@ include file="/WEB-INF/InitModel.jsp"%>

<HTML>
<HEAD>
<TITLE></TITLE>
<META http-equiv=Content-Type content="text/html; charset=windows-1252">
<link href="../../css/estilostsp.css" rel="stylesheet" type="text/css"> 
<link href="<%=BASEURL%>/css/estilostsp.css" rel="stylesheet" type="text/css"> 
<style type="text/css">
<!--
.Estilo2 {
	font-size: 16pt;
	color: #FF0000;
}
-->
</style>
</HEAD>
<BODY> 

        <table width="90%" border="0" align="center">
          <tr  class="subtitulo">
            <td height="20" align="center">MANUAL DE CONSULTA GENERAL DE HOJA DE VIDA</td>
          </tr>
          <tr class="subtitulo1">
            <td> Registro de referencias del veh&iacute;culo </td>
          </tr>
          <tr>
            <td  height="8"  class="ayudaHtmlTexto">Para poder realizar la consulta general de Hoja de vida, solo debe seleccionar el par&aacute;metro de b&uacute;squeda y digitar la informaci&oacute;n correspondiente su selecci&oacute;n.</td>
          </tr>
          <tr>
            <td height="18"  class="ayudaHtmlTexto" align="center"><div align="center"><img src="<%=BASEURL%>/images/ayuda/hvida/general/img.JPG"></div></td>
          </tr>
          <tr>
            <td height="18"  class="ayudaHtmlTexto" align="center"><p>Si presiono buscar conductor o buscar Propietario el sistema le muestra la siguiente formulario </p>
            </td>
          </tr>
          <tr>
            <td height="18"  class="ayudaHtmlTexto" align="center"><div align="center"><img src="<%=BASEURL%>/images/ayuda/hvida/general/img1.JPG"></div></td>
          </tr>
          <tr>
            <td height="18"  class="ayudaHtmlTexto" align="center">Escriba el nombre, ejemplo Karen y luego presione buscar.</td>
          </tr>
          <tr>
            <td height="18"  class="ayudaHtmlTexto" align="center"><div align="center"><img src="<%=BASEURL%>/images/ayuda/hvida/general/img2.JPG" ></div></td>
          </tr>
          <tr>
            <td height="18"  class="ayudaHtmlTexto" align="center"><p>Seleccione el nombre d&aacute;ndole clic sobre el mismo, para obtener el documento.</p>
            <p>Nota: Si se sabe el documento solo presione el bot&oacute;n Buscar.</p></td>
          </tr>
          <tr>
            <td height="18"  class="ayudaHtmlTexto" align="center"><div align="center"><img src="<%=BASEURL%>/images/ayuda/hvida/general/img4.JPG"> </div></td>
          </tr>
          <tr>
            <td height="18"  class="ayudaHtmlTexto" align="center">El sistema le muestra las placas relacionadas seg&uacute;n el tipo de b&uacute;squeda seleccionado.</td>
          </tr>
          <tr>
            <td height="18"  class="ayudaHtmlTexto" align="center"><div align="center"><img src="<%=BASEURL%>/images/ayuda/hvida/general/img3.JPG"></div></td>
          </tr>
          <tr>
            <td height="18"  class="ayudaHtmlTexto" align="center">Luego seleccione el numero de la placa a consultar, para poder obtener la informaci&oacute;n correspondiente a la hoja de vida.</td>
          </tr>
          <tr>
            <td height="18"  class="ayudaHtmlTexto" align="center"><div align="center"><img src="<%=BASEURL%>/images/ayuda/hvida/general/img5.JPG"></div></td>
          </tr>
      </table>
</BODY>
</HTML>
