<%@page contentType="text/html"%>
<%@page session="true"%>
<%@page import="java.util.*"%>
<%@page import="com.tsp.operation.model.*"%>
<%@page import="com.tsp.operation.model.beans.*"%>
<%@page import="com.tsp.util.*"%>
<%@include file="/WEB-INF/InitModel.jsp"%>
<%String path = application.getRealPath(model.menuService.getRealUrl(request.getRequestURI()));
  String datos[] = model.menuService.getContenidoMenu(BASEURL,request.getRequestURI(),path);
%>
<html>
<head>
<title>.: Verificacion de Documentos</title>
<link href="<%= BASEURL %>/css/estilostsp.css" rel='stylesheet'>
<link href="../../../css/estilostsp.css" rel="stylesheet" type="text/css">
<script type='text/javascript' src="<%= BASEURL %>/js/validar.js"></script>
<script type='text/javascript' src="<%= BASEURL %>/js/boton.js"></script>

</head>                       

<body>

<% VerificacionDoc veridoc = model.veridocService.ObtDocumentos();
String msg = (request.getParameter("mensaje")!=null)?request.getParameter("mensaje"):"" ;
String est = (String) session.getAttribute("men");
String res="";

if ( msg.equals("Actualizado")){//exito en el ingreso
	res = "Informacion Almacenada!";
}
String tipcon="011"; 
String actcon="003";
String tippla="012"; 
String actpla="005";

%>
<form name='formulario' method='POST' action="<%=CONTROLLER%>?estado=Veridocumen&accion=Verificar">
<%if(est.equals("Conductor")){%>
  <table width="650" border="2" align="center">
    <tr>
      <td><table width="100%" border="0">
        <tr>
          <td width="52%" class="subtitulo1">&nbsp;Documentos del Conductor</td>
          <td width="48%" class="barratitulo" ><img src="<%=BASEURL%>/images/titulo.gif" width="32" height="20" align="left"><%=datos[0]%></td>
        </tr>
      </table>
      <table width="100%" class="tablaInferior">
	   	<tr >
			<td colspan="6" class="fila">&nbsp;</td>
		    <td align="center" class="tblTitulo">SI</td>
		    <td align="center" class="tblTitulo">NO</td>		 
	   	</tr>
		 <tr>
          <td  class="fila">Cedula</td>
          <td class="letra"><%=veridoc.getCedula()%>
            <img src="<%=BASEURL%>/images/botones/iconos/documento.gif" alt="Adjuntar Imagen" width="15" height="15" 
						  onClick="Documento('<%=CONTROLLER%>',1,'<%=actcon%>','011');" style="cursor:hand">
            <input name="identificacion" type="hidden" id="identificacion" value="<%=veridoc.getCedula()%>"></td>
          <td colspan="4" class="fila">&nbsp;</td>
          <td width="26" align="center" class="letra"><%if(veridoc.getT_cedula().equals("S")){%>            <img src="<%=BASEURL%>/images/botones/iconos/chulo.gif">            <%}%></td>
          <td align="center" class="letra"><%if(!veridoc.getT_cedula().equals("S")){%><img src="<%=BASEURL%>/images/botones/iconos/chulo.gif"> <%}%></td>
        </tr>
<tr>
          <td  class="fila">Libreta Militar</td>
          <td class="letra"><%=(veridoc.getLibmilitar()!=null) ? veridoc.getLibmilitar() : ""%> <img src="<%=BASEURL%>/images/botones/iconos/documento.gif" alt="Adjuntar Imagen" width="15" height="15" 
						  onClick="Documento('<%=CONTROLLER%>',1,'<%=actcon%>','024');" style="cursor:hand">
            <input type="hidden" name="est" value="Conductor"></td>
          <td colspan="4" class="fila">&nbsp;</td>
          <td width="26" align="center" class="letra"><%if(veridoc.getT_libmilitar().equals("S")){%>
            <img src="<%=BASEURL%>/images/botones/iconos/chulo.gif">
            <%}%></td>
          <td align="center" class="letra"><%if(!veridoc.getT_libmilitar().equals("S")){%>
            <img src="<%=BASEURL%>/images/botones/iconos/chulo.gif">
            <%}%></td>
</tr>
       
         
        <tr>
          <td class="fila" >Nro Judicial </td>
          <td class="letra"><%=veridoc.getNrojudicial()%>
            <img src="<%=BASEURL%>/images/botones/iconos/documento.gif" alt="Adjuntar Imagen" width="15" height="15" 
						  onClick="Documento('<%=CONTROLLER%>',1,'<%=actcon%>','025');" style="cursor:hand"></td>
          <td width="63" class="fila">Vence</td>
          <td width="91" class="letra"><%=(!veridoc.getVencejudicial().equals("0099-01-01")) ? veridoc.getVencejudicial() : ""%></td>
          <td colspan="2" class="fila">&nbsp;</td>
          <td align="center" class="fila"><%if(veridoc.getT_judicial().equals("S")){%><img src="<%=BASEURL%>/images/botones/iconos/chulo.gif"><%}%></td>
          <td align="center" class="fila"><%if(!veridoc.getT_judicial().equals("S")){%><img src="<%=BASEURL%>/images/botones/iconos/chulo.gif"><%}%></td>
        </tr>
        <tr>
          <td  class="fila">Nombre Eps </td>
          <td width="150" class="letra"><%=veridoc.getNomeps()%>
            <img src="<%=BASEURL%>/images/botones/iconos/documento.gif" alt="Adjuntar Imagen" width="15" height="15" 
						  onClick="Documento('<%=CONTROLLER%>',1,'<%=actcon%>','027');" style="cursor:hand"></td>
          <td class="fila">Nro Eps </td>
          <td class="letra"><%=veridoc.getNroeps()%></td>
          <td width="55" class="fila">Afiliacion</td>
          <td width="91" class="letra"><%=(!veridoc.getFecafieps().equals("0099-01-01")) ? veridoc.getFecafieps() : ""%></td>
          <td align="center" class="letra"><%if(veridoc.getT_eps().equals("S")){%><img src="<%=BASEURL%>/images/botones/iconos/chulo.gif"><%}%></td>
          <td align="center" class="letra"><%if(!veridoc.getT_eps().equals("S")){%><img src="<%=BASEURL%>/images/botones/iconos/chulo.gif"><%}%></td>
        </tr>
        <tr>
          <td  class="fila">Nombre Arp </td>
          <td class="letra"><%=veridoc.getNomarp()%>
		      <img src="<%=BASEURL%>/images/botones/iconos/documento.gif" alt="Adjuntar Imagen" width="15" height="15" 
			  onClick="Documento('<%=CONTROLLER%>',1,'<%=actcon%>','028');" style="cursor:hand"></td>
          <td class="fila">Afiliacion</td>
          <td class="letra"><%=(!veridoc.getFecafiarp().equals("0099-01-01")) ? veridoc.getFecafiarp() : ""%></td>
          <td colspan="2" class="fila">&nbsp;</td>
          <td align="center" class="fila"><%if(veridoc.getT_arp().equals("S")){%><img src="<%=BASEURL%>/images/botones/iconos/chulo.gif"><%}%></td>
          <td align="center" class="fila"><%if(!veridoc.getT_arp().equals("S")){%><img src="<%=BASEURL%>/images/botones/iconos/chulo.gif"><%}%></td>
        </tr>
        <tr>
          <td  class="fila">Pase </td>
          <td class="letra"><%=veridoc.getNopase()%>
            <img src="<%=BASEURL%>/images/botones/iconos/documento.gif" alt="Adjuntar Imagen" width="15" height="15" 
						  onClick="Documento('<%=CONTROLLER%>',1,'<%=actcon%>','023');" style="cursor:hand"></td>
          <td class="fila">Categoria </td>
          <td class="letra"><%=veridoc.getCategoriapase()%></td>
          <td class="fila">Vence</td>
          <td class="letra"><%=(!veridoc.getVigenciapase().equals("0099-01-01")) ? veridoc.getVigenciapase() : ""%></td>
          <td align="center" class="letra"><%if(veridoc.getT_categoriapase().equals("S")){%><img src="<%=BASEURL%>/images/botones/iconos/chulo.gif"><%}%></td>
          <td align="center" class="letra"><%if(!veridoc.getT_categoriapase().equals("S")){%><img src="<%=BASEURL%>/images/botones/iconos/chulo.gif"><%}%></td>
        </tr>
        <tr>
          <td  class="fila">Libreta Tripulante </td>
          <td class="letra"><%=veridoc.getNrolibtripulante()%>
            <img src="<%=BASEURL%>/images/botones/iconos/documento.gif" alt="Adjuntar Imagen" width="15" height="15" 
						  onClick="Documento('<%=CONTROLLER%>',1,'<%=actcon%>','029');" style="cursor:hand"></td>
          <td class="fila">Vence</td>
          <td class="letra"><%=(!veridoc.getVencelibtripulante().equals("0099-01-01")) ? veridoc.getVencelibtripulante() : ""%></td>
          <td colspan="2" class="fila">&nbsp;</td>
          <td align="center" class="letra"><%if(veridoc.getT_libtripulante().equals("S")){%><img src="<%=BASEURL%>/images/botones/iconos/chulo.gif"><%}%></td>
          <td align="center" class="letra"><%if(!veridoc.getT_libtripulante().equals("S")){%><img src="<%=BASEURL%>/images/botones/iconos/chulo.gif"><%}%></td>
        </tr>
        <tr>
          <td class="fila" >Pasaporte</td>
          <td class="letra"><%=veridoc.getNopasaporte()%>
            <img src="<%=BASEURL%>/images/botones/iconos/documento.gif" alt="Adjuntar Imagen" width="15" height="15" 
						  onClick="Documento('<%=CONTROLLER%>',1,'<%=actcon%>','030');" style="cursor:hand"></td>
          <td class="fila">Vence</td>
          <td class="letra"><%=(!veridoc.getVencepasaporte().equals("0099-01-01")) ? veridoc.getVencepasaporte() : ""%></td>
          <td colspan="2" class="letra">&nbsp;</td>
          <td align="center" class="fila"><%if(veridoc.getT_pasaporte()!=null && veridoc.getT_pasaporte().equals("S")){%><img src="<%=BASEURL%>/images/botones/iconos/chulo.gif"><%}%></td>
          <td align="center" class="fila"><%if(!veridoc.getT_pasaporte().equals("S")){%><img src="<%=BASEURL%>/images/botones/iconos/chulo.gif"><%}%></td>
        </tr>
        <tr>
          <td  class="fila">Visa</td>
          <td class="letra"><%=veridoc.getNrovisa()%>
            <img src="<%=BASEURL%>/images/botones/iconos/documento.gif" alt="Adjuntar Imagen" width="15" height="15" 
						  onClick="Documento('<%=CONTROLLER%>',1,'<%=actcon%>','030');" style="cursor:hand"></td>
          <td class="fila">vence</td>
          <td class="letra"><%=(!veridoc.getVencevisa().equals("0099-01-01")) ? veridoc.getVencevisa() : ""%></td>
          <td colspan="2" class="letra">&nbsp;</td>
          <td align="center" class="letra"><%if(veridoc.getT_visa().equals("S")){%><img src="<%=BASEURL%>/images/botones/iconos/chulo.gif"><%}%></td>
          <td align="center" class="letra"><%if( !veridoc.getT_visa().equals("S")){%><img src="<%=BASEURL%>/images/botones/iconos/chulo.gif"><%}%></td>
        </tr>
      </table></td>
    </tr>
  </table>
<%}else if(est.equals("Placa")){%>

  <table width="650" border="2" align="center">
    <tr>
      <td>      <table width="100%" border="0">
        <tr>
          <td width="52%" class="subtitulo1">&nbsp;Documentos del Vehiculo</td>
          <td width="48%" class="barratitulo" ><img src="<%=BASEURL%>/images/titulo.gif" width="32" height="20" align="left"><%=datos[0]%></td>
        </tr>
      </table>
        <table width="100%" class="tablaInferior" >
<tr>
          <td colspan="4" class="fila">&nbsp;</td>
          <td align="center" class="fila"> SI            </td>
          <td align="center" class="fila"> NO            </td>
</tr>
        <tr>
          <td width="237" class="fila">Tarjeta Propiedad Cabezote 
            <input name="placa" type="hidden" id="placa" value="<%=veridoc.getPlaca()%>">
            <input type="hidden" name="est" value="Placa"></td>
          <td width="148" class="letra"><%=(veridoc.getTarprop()!=null)?veridoc.getTarprop():"No Encontrado"%>
            <img src="<%=BASEURL%>/images/botones/iconos/documento.gif" alt="Adjuntar Imagen" width="15" height="15" 
			onClick="Documento('<%=CONTROLLER%>',2,'<%=actpla%>','014');" style="cursor:hand "></td>
          <td width="64" class="fila">Vence</td>
          <td width="117" class="letra"><%=(veridoc.getFecvenprop()!=null && !veridoc.getFecvenprop().equals("0099-01-01"))?veridoc.getFecvenprop():""%></td>
          <td width="15" align="center" class="fila"><%if(veridoc.getT_tarpropcab().equals("S")){%>            <img src="<%=BASEURL%>/images/botones/iconos/chulo.gif">            <%}%></td>
          <td width="24" align="center" class="fila"><%if(!veridoc.getT_tarpropcab().equals("S")){%>            <img src="<%=BASEURL%>/images/botones/iconos/chulo.gif">            <%}%></td>
        </tr>
        <tr>
          <td class="fila">Certificado de Emision de Gases</td>
          <td class="letra"><%=(veridoc.getCertemgases()!=null)?veridoc.getCertemgases():""%>           
		  <img src="<%=BASEURL%>/images/botones/iconos/documento.gif" alt="Adjuntar Imagen" width="15" height="15" style="cursor:hand " 
		  onClick="Documento('<%=CONTROLLER%>',2,'<%=actpla%>','017');"></td>
          <td class="fila">Vence</td>
         
          <td class="letra"><%=(veridoc.getFecvegases()!=null && !veridoc.getFecvegases().equals("0099-01-01") )?veridoc.getFecvegases():""%></td>
          <td align="center" class="fila"><%if(veridoc.getT_certemgases().equals("S")){%><img src="<%=BASEURL%>/images/botones/iconos/chulo.gif"><%}%></td>
		  <td align="center" class="fila"><%if(!veridoc.getT_certemgases().equals("S")){%><img src="<%=BASEURL%>/images/botones/iconos/chulo.gif"><%}%></td>
        </tr>
        <tr>
          <td class="fila">SOAT</td>
          <td class="letra"><%=(veridoc.getCiasoat()!=null)?veridoc.getCiasoat():""%>
            <img src="<%=BASEURL%>/images/botones/iconos/documento.gif" alt="Adjuntar Imagen" width="15" height="15" 
			onClick="Documento('<%=CONTROLLER%>',2,'<%=actpla%>','019');" style="cursor:hand "></td>
          <td class="fila">Vence</td>
          <td class="letra"><%=(veridoc.getVenseguroobliga()!=null && !veridoc.getVenseguroobliga().equals("0099-01-01"))?veridoc.getVenseguroobliga():""%></td>
          <td align="center" class="fila"><%if(veridoc.getT_soat().equals("S")){%><img src="<%=BASEURL%>/images/botones/iconos/chulo.gif"><%}%></td>
          <td align="center" class="fila"><%if(!veridoc.getT_soat().equals("S")){%><img src="<%=BASEURL%>/images/botones/iconos/chulo.gif"><%}%></td>
        </tr>
        <tr>
          <td class="fila" >Registro Nacional de Carga </td>
          <td class="letra"><%=veridoc.getReg_nal_carga()%>
            <img src="<%=BASEURL%>/images/botones/iconos/documento.gif" alt="Adjuntar Imagen" width="15" height="15" 
			onClick="Documento('<%=CONTROLLER%>',2,'<%=actpla%>','020');" style="cursor:hand "></td>
          <td class="fila">Vence</td>
          <td class="letra"><%=(veridoc.getFecvenreg()!=null && !veridoc.getFecvenreg().equals("0099-01-01")) ? veridoc.getFecvenreg() : ""%>
          </td>
          <td align="center" class="fila"><%if(veridoc.getT_regnalcarga().equals("S")){%><img src="<%=BASEURL%>/images/botones/iconos/chulo.gif"><%}%></td>
          <td align="center" class="fila"><%if( !veridoc.getT_regnalcarga().equals("S")){%><img src="<%=BASEURL%>/images/botones/iconos/chulo.gif"><%}%></td>
        </tr>
        <tr>
          <td  class="fila">Tarjeta Empresarial </td>
          <td class="letra"><%=veridoc.getTarempresa()%>
            <img src="<%=BASEURL%>/images/botones/iconos/documento.gif" alt="Adjuntar Imagen" width="15" height="15" 
			onClick="Documento('<%=CONTROLLER%>',2,'<%=actpla%>','018');" style="cursor:hand "></td>
          <td class="fila">Vence</td>
          <td class="letra"><%=(veridoc.getFecvenempresa()!=null && ! veridoc.getFecvenempresa().equals("0099-01-01") ) ? veridoc.getFecvenempresa() : ""%></td>
          <td align="center" class="fila"><%if( veridoc.getT_tarempresarial().equals("S")){%><img src="<%=BASEURL%>/images/botones/iconos/chulo.gif"><%}%></td>
          <td align="center" class="fila"><%if( !veridoc.getT_tarempresarial().equals("S")){%><img src="<%=BASEURL%>/images/botones/iconos/chulo.gif"><%}%></td>
        </tr>
        <tr>
          <td class="fila" >Tarjeta de Habilitaci&oacute;n </td>
          <td class="letra"><%=veridoc.getTarhabil()%>
            <img src="<%=BASEURL%>/images/botones/iconos/documento.gif" alt="Adjuntar Imagen" width="15" height="15" 
			onClick="Documento('<%=CONTROLLER%>',2,'<%=actpla%>','015');" style="cursor:hand "></td>
          <td class="fila">Vence</td>
          <td class="letra"><%=(veridoc.getFecvenhabil()!=null && !veridoc.getFecvenhabil().equals("0099-01-01") ) ? veridoc.getFecvenhabil() : ""%></td>
          <td align="center" class="fila"><%if(veridoc.getT_tarhabil()!=null && veridoc.getT_tarhabil().equals("S")){%><img src="<%=BASEURL%>/images/botones/iconos/chulo.gif"><%}%></td>
          <td align="center" class="letra"><%if(veridoc.getT_tarhabil()==null || !veridoc.getT_tarhabil().equals("S")){%><img src="<%=BASEURL%>/images/botones/iconos/chulo.gif"><%}%></td>
        </tr>
        <tr>
          <td class="fila" >Tarjeta Propiedad Semiremolque </td>
          <td class="letra"><%=(veridoc.getTarpropsemi()!=null) ? veridoc.getTarpropsemi() : ""%>
            <img src="<%=BASEURL%>/images/botones/iconos/documento.gif" alt="Adjuntar Imagen" width="15" height="15" 
			onClick="Documento('<%=CONTROLLER%>',2,'<%=actpla%>','033');" style="cursor:hand "></td>
          <td class="fila">Vence</td>
          <td class="letra"><%=(veridoc.getVentarpropsemi()!=null && !veridoc.getVentarpropsemi().equals("0099-01-01")) ? veridoc.getVentarpropsemi() : ""%></td>
          <td align="center" class="fila"><%if(veridoc.getT_tarpropsemi()!=null && veridoc.getT_tarpropsemi().equals("S")){%><img src="<%=BASEURL%>/images/botones/iconos/chulo.gif"><%}%></td>
          <td align="center" class="fila"><%if(veridoc.getT_tarpropsemi()==null || !veridoc.getT_tarpropsemi().equals("S")){%><img src="<%=BASEURL%>/images/botones/iconos/chulo.gif"><%}%></td>
        </tr>
        <tr>
          <td class="fila" >Registro Nacional de Carga Semiremolque </td>
          <td class="letra"><%=(veridoc.getRegnalsemi()!=null) ? veridoc.getRegnalsemi() : ""%>
            <img src="<%=BASEURL%>/images/botones/iconos/documento.gif" alt="Adjuntar Imagen" width="15" height="15" 
			onClick="Documento('<%=CONTROLLER%>',2,'<%=actpla%>','034');" style="cursor:hand "></td>
          <td class="fila">Vence</td>
          <td class="letra"><%=(veridoc.getVenregnalsemi()!=null && !veridoc.getVenregnalsemi().equals("0099-01-01")) ? veridoc.getVenregnalsemi() : ""%></td>
          <td align="center" class="fila"> <%if(veridoc.getT_regnalsemire()!=null && veridoc.getT_regnalsemire().equals("S")){%><img src="<%=BASEURL%>/images/botones/iconos/chulo.gif"><%}%></td>
          <td align="center" class="fila"><%if(veridoc.getT_regnalsemire()==null || !veridoc.getT_regnalsemire().equals("S")){%><img src="<%=BASEURL%>/images/botones/iconos/chulo.gif"><%}%></td>
        </tr>
        <tr>
          <td  class="fila">Poliza Andina </td>
          <td class="letra"><%=( veridoc.getPoliza_andina()!=null && !veridoc.getPoliza_andina().equals("0099-01-01") ) ? veridoc.getPoliza_andina() : "" %>
            <img src="<%=BASEURL%>/images/botones/iconos/documento.gif" alt="Adjuntar Imagen" width="15" height="15" 
			onClick="Documento('<%=CONTROLLER%>',2,'<%=actpla%>','016');" style="cursor:hand "></td>
          <td class="fila">Vence</td>
          <td class="letra"><%=(veridoc.getFecvenandina() !=null && !veridoc.getFecvenandina().equals("0099-01-01")) ? veridoc.getFecvenandina() : ""%></td>
          <td align="center" class="fila"><%if(veridoc.getT_polizaandina()!=null && veridoc.getT_polizaandina().equals("S")){%><img src="<%=BASEURL%>/images/botones/iconos/chulo.gif"><%}%></td>
          <td align="center" class="letra"><%if(veridoc.getT_polizaandina()==null || !veridoc.getT_polizaandina().equals("S")){%><img src="<%=BASEURL%>/images/botones/iconos/chulo.gif"><%}%></td>
        </tr>
        <tr class="fila">
          <td colspan="2" >Copia de Contrato compra venta <img src="<%=BASEURL%>/images/botones/iconos/documento.gif" alt="Adjuntar Imagen" width="15" height="15" 
		  onClick="Documento('<%=CONTROLLER%>',2,'<%=actpla%>','021');" style="cursor:hand "></td>
          <td colspan="2">&nbsp;</td>
          <td align="center"><%if(veridoc.getT_copiacontcomventa()!=null && veridoc.getT_copiacontcomventa().equals("S")){%><img src="<%=BASEURL%>/images/botones/iconos/chulo.gif"><%}%></td>
          <td align="center"><%if(veridoc.getT_copiacontcomventa()==null || !veridoc.getT_copiacontcomventa().equals("S")){%><img src="<%=BASEURL%>/images/botones/iconos/chulo.gif"><%}%></td>
        </tr>
        <tr class="fila">
          <td colspan="2" >Copia de Contrato de Arrendamiento <img src="<%=BASEURL%>/images/botones/iconos/documento.gif" alt="Adjuntar Imagen" width="15" height="15" 
		  onClick="Documento('<%=CONTROLLER%>',2,'<%=actpla%>','022');" style="cursor:hand "></td>
          <td colspan="2">&nbsp;</td>
          <td align="center"><%if(veridoc.getT_copiacontarr()!=null && veridoc.getT_copiacontarr().equals("S")){%><img src="<%=BASEURL%>/images/botones/iconos/chulo.gif"><%}%></td>
          <td align="center"><%if(veridoc.getT_copiacontarr()==null || !veridoc.getT_copiacontarr().equals("S")){%><img src="<%=BASEURL%>/images/botones/iconos/chulo.gif"><%}%></td>
        </tr>
      </table></td>
    </tr>
  </table>
<%}%>
  <p align="center">
    <img src="<%=BASEURL%>/images/botones/aceptar.gif"  name="imgaceptar" onClick="formulario.submit();" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" style="cursor:hand" title="Para actualizar la verificacion de documentos haga clic aqui...">&nbsp;
	<img src="<%=BASEURL%>/images/botones/restablecer.gif" name="restablecer"    onClick="window.location='<%=CONTROLLER%>?estado=Veridocumen&accion=Buscar&pagina=documento.jsp&carpeta=jsp/hvida/veridocumen&evento=<%=est%>&<%=(est.equals("Conductor"))?"identificacion="+veridoc.getCedula():"placa="+veridoc.getPlaca()%>'" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" style="cursor:hand"> &nbsp;
    <img src="<%=BASEURL%>/images/botones/salir.gif"  name="imgsalir"  onMouseOver="botonOver(this);" onClick="parent.close();" onMouseOut="botonOut(this);" style="cursor:hand">  </p>	
<table width="400" border="0" align="center">
  <tr>
    <td>	
		<FIELDSET>
			<legend><span class="letraresaltada">Recuerde...</span></legend>
			<table width="100%"  border="0" cellpadding="0" cellspacing="0" class="informacion">
  				<tr>
    			  <td nowrap align="center">&nbsp;Una vez asignada una imagen a un documento presione RESTABLECER </td>
    			</tr>
			</table>
  		</FIELDSET>
	</td>
  </tr>
</table>


</form>
 <%if(!res.equals("")){%>
  <p><table border="2" align="center">
  <tr>
    <td><table width="100%"  border="1" align="center"  bordercolor="#F7F5F4" bgcolor="#FFFFFF">
      <tr>
        <td width="229" align="center" class="mensajes"><%=res%></td>
        <td width="29" background="<%=BASEURL%>/images/cuadronaranja.JPG">&nbsp;</td>
        <td width="58">&nbsp;</td>
      </tr>
    </table></td>
  </tr>
</table>
<%}%>
<iframe width=188 height=166 name="gToday:datetime:<%=BASEURL%>/js/calendartsp/agenda.js:gfPop:<%=BASEURL%>/js/calendartsp/plugins_24.js" id="gToday:datetime:<%=BASEURL%>/js/calendartsp/agenda.js:gfPop:<%=BASEURL%>/js/calendartsp/plugins_24.js" src="<%=BASEURL%>/js/calendartsp/ipopeng.htm" scrolling="no" frameborder="0" style="visibility:visible; z-index:999; position:absolute; left:-500px; top:0px;"></iframe>
<%=datos[1]%>
</body>
</html>

