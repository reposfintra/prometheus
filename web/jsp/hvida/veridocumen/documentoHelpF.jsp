<%@ include file="/WEB-INF/InitModel.jsp"%>

<HTML>
<HEAD>
<TITLE></TITLE>
<META http-equiv=Content-Type content="text/html; charset=windows-1252">
<link href="../../../css/estilostsp.css" rel="stylesheet" type="text/css"> 
<link href="<%=BASEURL%>/css/estilostsp.css" rel="stylesheet" type="text/css"> 
<style type="text/css">
<!--
.Estilo1 {
	font-size: 16pt;
	color: #FF0000;
}
-->
</style>
</HEAD>
<BODY> 

        <table width="90%" border="0" align="center">
          <tr  class="subtitulo">
            <td height="20"><div align="center">MANUAL DE 


 VERIFICACI&Oacute;N DE DOCUMENTOS </div></td>
          </tr>
          <tr class="subtitulo1">
            <td>Como  verificar los documentos de veh&iacute;culos y conductores. </td>
          </tr>
          <tr>
            <td  height="8"  class="ayudaHtmlTexto">El sistema muestra una ventana con los documentos correspondientes.</td>
          </tr>
          <tr>
            <td height="18"  class="ayudaHtmlTexto" align="center"><div align="center"><img src="<%=BASEURL%>/images/ayuda/hvida/doc1.JPG"></div></td>
          </tr>
          <tr>
            <td height="18"  class="ayudaHtmlTexto" align="center"><p>Si desea agregar la imagen de alg&uacute;n documento, solo debe presionar el icono. </p></td>
          </tr>
          <tr>
            <td height="18"  class="ayudaHtmlTexto" align="center"><div align="center" class="Estilo1"><img src="<%=BASEURL%>/images/ayuda/hvida/doc2.JPG"></div></td>
          </tr>
          <tr>
            <td height="18"  class="ayudaHtmlTexto" align="center"><p>Presione el bot&oacute;n examinar para buscar la imagen adjuntar. </p>            </td>
          </tr>
          <tr>
            <td height="18"  class="ayudaHtmlTexto" align="center">Nota: usted puede adjuntar de 1 a 10 im&aacute;genes. </td>
          </tr>
          <tr>
            <td height="18"  class="ayudaHtmlTexto" align="center"><div align="center"><img src="<%=BASEURL%>/images/ayuda/hvida/img2.JPG"></div></td>
          </tr>
          <tr>
            <td height="18"  class="ayudaHtmlTexto" align="center"> Despu&eacute;s de cargar la imagen al servidor se procede almacenarla. </td>
          </tr>
<tr>
            <td height="18"  class="ayudaHtmlTexto" align="center"><div align="center"><img src="<%=BASEURL%>/images/ayuda/hvida/img3.JPG"></div></td>
          </tr>
          <tr>
            <td height="18"  class="ayudaHtmlTexto" align="center">Luego de presionar aceptar el sistema le muestra el siguiente formulario, inform&aacute;ndole que se almaceno la imagen </td>
          </tr>
          <tr>
            <td height="18" ><div align="center"><img src="<%=BASEURL%>/images/ayuda/hvida/img4.JPG"></div></td>
          </tr>
          <tr>
            <td height="18"  class="ayudaHtmlTexto" align="center">Ya almacenada la imagen, debe presionar el bot&oacute;n Restablecer , para actualizar los indicadores de documentos.  </td>
          </tr>
          <tr>
            <td height="18"  class="ayudaHtmlTexto" align="center">&nbsp;</td>
          </tr>
      </table>
</BODY>
</HTML>
