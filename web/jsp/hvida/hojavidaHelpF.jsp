<%@ include file="/WEB-INF/InitModel.jsp"%>

<HTML>
<HEAD>
<TITLE></TITLE>
<META http-equiv=Content-Type content="text/html; charset=windows-1252">
<link href="../../css/estilostsp.css" rel="stylesheet" type="text/css"> 
<link href="<%=BASEURL%>/css/estilostsp.css" rel="stylesheet" type="text/css"> 
</HEAD>
<BODY> 

        <table width="90%" border="0" align="center">
          <tr  class="subtitulo">
            <td height="20"><div align="center">MANUAL DE HOJA DE VIDA </div></td>
          </tr>
          <tr class="subtitulo1">
            <td>Como consultar la Hoja de Vida </td>
          </tr>
          <tr>
            <td  height="18"  class="ayudaHtmlTexto"><p>Si desea consultar la hoja de vida por la placa de un veh&iacute;culo, debe realizar los siguientes pasos: </p></td>
          </tr>
          <tr align="center">
            <td height="18"  class="ayudaHtmlTexto"><div align="center"><img src="<%=BASEURL%>/images/ayuda/hvida/hvida1.JPG""></div></td>
          </tr>
          <tr>
            <td  class="ayudaHtmlTexto"><p>El sistema le muestra la informaci&oacute;n correspondiente a la hoja de vida, detallada en 5 grupos:</p></td>
          </tr>
          <tr>
            <td  class="ayudaHtmlTexto"> <p>1. Informaci&oacute;n de Veh&iacute;culo. </p>
            </td>
          </tr>
          <tr  align="center">
            <td  class="ayudaHtmlTexto"><div align="center"><img src="<%=BASEURL%>/images/ayuda/hvida/hvida2.JPG"></div></td>
          </tr>
          <tr  align="center">
            <td  class="ayudaHtmlTexto"> 2. Informaci&oacute;n del Propietario </td>
          </tr>
          <tr  align="center">
            <td  class="ayudaHtmlTexto"><div align="center"><img src="<%=BASEURL%>/images/ayuda/hvida/hvida3.JPG"></div></td>
          </tr>
          <tr  align="center">
            <td  class="ayudaHtmlTexto">3. Informaci&oacute;n del Conductor.</td>
          </tr>
          <tr>
            <td  class="ayudaHtmlTexto"><div align="center"><img src="<%=BASEURL%>/images/ayuda/hvida/hvida4.JPG"></div></td>
          </tr>
          <tr>
            <td  class="ayudaHtmlTexto"> 4.Informaci&oacute;n del tenedor </td>
          </tr>
          <tr>
            <td  class="ayudaHtmlTexto"><div align="center"><img src="<%=BASEURL%>/images/ayuda/hvida/hvida5.JPG"></div></td>
          </tr>
          <tr>
            <td  class="ayudaHtmlTexto"><p>5.Control de registros </p></td>
          </tr>
          <tr>
            <td  class="ayudaHtmlTexto"><div align="center"><img src="<%=BASEURL%>/images/ayuda/hvida/hvida6.JPG"></div></td>
          </tr>
          <tr>
            <td  class="ayudaHtmlTexto"><p>Despu&eacute;s de presionar Imprimir, el sistema presenta una vista previa del documento. </p>
            </td>
          </tr>
          <tr>
            <td  class="ayudaHtmlTexto"><div align="center"><img src="<%=BASEURL%>/images/ayuda/hvida/print.JPG"></div></td>
          </tr>
          <tr>
            <td  class="ayudaHtmlTexto">&nbsp;</td>
          </tr>
      </table>
  <br>
</BODY>
</HTML>
