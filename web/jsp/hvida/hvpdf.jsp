<!--
- Autor : Ing. Andr�s Maturana De La Cruz
- Date  : 21 de abril del 2006
- Copyrigth Notice : Fintravalores S.A. S.A
-->
<%--
-@(#)
--Descripcion : Pagina JSP, que permite la creacion del archivo PDF
--%>

<%-- Declaracion de librerias--%>
<%@page contentType="text/html"%>
<%@page session="true"%>
<%@page errorPage="/error/ErrorPage.jsp"%>
<%@page import="java.util.*"%>
<%@page import="java.util.Vector"%>
<%@page import="java.text.*"%>
<%@page import="com.tsp.pdf.*"%>
<%@page import="com.tsp.operation.model.beans.*"%>
<%@page import="com.tsp.util.*"%>
<%@page import="com.tsp.operation.model.*"%>
<%@ include file="/WEB-INF/InitModel.jsp"%>
<%@ page import="java.io.File"%>
<html>
<head>
<title>Formato de Impresi�n Hoja de Vida</title>
</head>
<%
  HojaVidaPDF hvc =  new HojaVidaPDF();
    
  HojaVida hoja = model.hojaVidaService.getHojaVida();
  File xslt = (File) request.getAttribute("ArchivoXSLT");
  File pdf = (File) request.getAttribute("ArchivoPDF");
  
  hvc.generarPDF(xslt, pdf, hoja);
  response.sendRedirect("pdf/hv.pdf");
  
%>
</body>
</html>
