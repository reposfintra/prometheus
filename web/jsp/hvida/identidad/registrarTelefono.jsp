<!--
- Autor : Ing. Diogenes Bastidas Morales
- Date  : 28 de noviembre de 2005
- Copyrigth Notice : Fintravalores S.A. S.A
-->
<%--
-@(#)
--Descripcion : Pagina JSP, que maneja el ingreso de telefonos

--%>

<%-- Declaracion de librerias--%>
<%@page contentType="text/html"%>
<%@page session="true"%>
<%@page import="java.util.*"%>
<%@page import="com.tsp.operation.model.*"%>
<%@page import="com.tsp.operation.model.beans.*"%>
<%@page import="com.tsp.util.*"%>
<%@include file="/WEB-INF/InitModel.jsp"%>
<%@taglib uri="http://jakarta.apache.org/taglibs/input-0.90" prefix="input" %>

<html>
<head>
<link href="../../../css/estilostsp.css" rel="stylesheet" type="text/css">
<link href="<%=BASEURL%>/css/estilostsp.css" rel="stylesheet" type="text/css">

<title>Resgistrar Telefono</title>
<script language="javascript" src="<%=BASEURL%>/js/validar.js"></script>
<script language="javascript" src="<%=BASEURL%>/js/hvida.js"></script>
<script language="javascript" src="<%=BASEURL%>/js/boton.js"></script>
<script type="text/javascript">
function soloNumeros(id) {
                 var valor = document.getElementById(id).value;
                 valor =  valor.replace(/[^0-9^.]+/gi,"");
                 document.getElementById(id).value = valor;
            }
</script>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
</head>
<body>
<%String cpais = (request.getParameter("pais")!=null)?request.getParameter("pais"):"";
 String cciu = (request.getParameter("ciudad")!=null)?request.getParameter("ciudad"):"";
 String npais = (request.getParameter("codp")!=null)?request.getParameter("codp"):"";
 String ind = (request.getParameter("ind")!=null)?request.getParameter("ind"):"";
 String val = (request.getParameter("opc")!=null)?request.getParameter("opc"):"";
 String num = (request.getParameter("num")!=null)?request.getParameter("num"):"";
 System.out.println("Numero "+num);
 %>

<form name="forma" method="post" action="" >
<table border="2" align="center" width="400">
  <tr>
    <td>
	<table width="100%"  align="center"   >
  <tr>
    <td width="248" height="22"  class="subtitulo1">Regitrar Telefono</td>
    <td width="216"  class="barratitulo"><img src="<%=BASEURL%>/images/titulo.gif" width="32" height="20"></td>
  </tr>
</table>
    <table width="100%" align="center" class="tablaInferior">
      <tr class="fila">
        <td width="119" align="left" valign="middle" >Pais</td>
        <td width="250" valign="middle"><select name="pais"  class="textbox" onChange="cargarSelects('<%=CONTROLLER%>?estado=Cargar&accion=Select&carpeta=jsp/hvida/identidad&pagina=registrarTelefono.jsp&tel=ok');">
          <option value="" >Seleccione</option>
          <%
			List paises = model.paisservice.obtenerpaises();
    		Iterator it = paises.iterator();
			while (it.hasNext()){
  				Pais pais = (Pais) it.next();%>
  			      <option value="<%=pais.getCountry_code()%>" <%=(pais.getCountry_code().equals(cpais))?"selected":""%> ><%=pais.getCountry_name()%></option>
			<%}%>
       			 </select>
          <input type="hidden" name="opc" value="<%=val%>"></td>
      </tr>
      <tr class="fila">
        <td width="119" align="left" valign="middle" >Ciudad</td>
        <td valign="middle"><select id="ciudad" name="ciudad"  class="textbox" onChange="cargarSelects('<%=CONTROLLER%>?estado=Identidad&accion=Buscarindicativo&carpeta=jsp/hvida/identidad&pagina=registrarTelefono.jsp');">
          <%if(cpais.equals("")){%>
          	 <option value="" selected>Seleccione</option>
          <%}
		    else {
  				    List ciudades = model.ciudadservice.obtenerCiudades();
  				      if(ciudades.size() > 0){
            		      Iterator it2 = ciudades.iterator();%>
					       <option value="" >Seleccione</option>
                          <%while (it2.hasNext()){
			      		        Ciudad ciu = (Ciudad) it2.next();%>
              					<option value="<%=ciu.getcodciu()%>" <%=(ciu.getcodciu().equals(cciu))?"selected":""%>><%=ciu.getnomciu()%></option>
					      <%}
				    }
                    else {%>
              		    <option value="" selected>No registra datos</option>
                  <%}
				}%>
        </select></td>
      </tr>
      <tr class="fila">
        <td width="119" rowspan="2" valign="middle" >Telefono</td>
        <td valign="middle"><table width="50%"  border="0" cellpadding="0" cellspacing="0">
          <tr class="fila">
            <td width="30%">Pais</td>
            <td width="34%">Area</div></td>
            <td width="36%">Numero</td>
          </tr>
        </table> </td>
      </tr>
      <tr class="fila">
        <td valign="middle"><input name="cpais" type="text" class="textbox" id="cpais"  value="<%=npais%>" size="3" maxlength="3" readonly>
          <input name="ind" type="text" class="textbox" id="ind" value="<%=ind%>" size="3" maxlength="3" readonly>
          <input name="num" type="text" class="textbox" id="num" maxlength="10" onKeyUp="soloNumeros(this.id)" value="<%=num%>"></td>
      </tr>
    </table></td>
  </tr>
  </table>
  <div align="center"><br>
    <img src="<%=BASEURL%>/images/botones/aceptar.gif"  name="imgaceptar"  height="21" onMouseOver="botonOver(this);" onClick="setearTelefono(<%=val%>);" onMouseOut="botonOut(this);" style="cursor:hand">&nbsp; <img src="<%=BASEURL%>/images/botones/salir.gif"  name="imgsalir"  height="21" onMouseOver="botonOver(this);" onClick="parent.close();" onMouseOut="botonOut(this);" style="cursor:hand">
  </div>
</form>


<iframe width=188 height=166 name="gToday:normal:agenda.js:gfPop:plugins.js" id="gToday:normal:agenda.js:gfPop:plugins.js" src="js/Calendario/ipopeng.htm" scrolling="no" frameborder="0" style="visibility:visible; z-index:999; position:absolute; left:-500px; top:0px;"> </iframe>

</body>
</html>


