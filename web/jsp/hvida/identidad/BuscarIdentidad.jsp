<!--
- Autor : Ing. Diogenes Bastidas Morales
- Date  : 20 de julio de 2005
- Copyrigth Notice : Fintravalores S.A. S.A
-->
<%--
-@(#)
--Descripcion : Pagina JSP, que maneja la busqueda de identidades

--%>

<%-- Declaracion de librerias--%>
<%@page contentType="text/html"%>
<%@page session="true"%>
<%@page errorPage="../error/ErrorPage.jsp"%>
<%@page import="java.util.*"%>
<%@page import="com.tsp.operation.model.*"%>
<%@page import="com.tsp.util.*"%>
<%@page import="com.tsp.operation.model.beans.*, java.util.*"%>
<%@include file="/WEB-INF/InitModel.jsp"%>

<html>
<head>
<title>Buscar Identidad</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link href="<%= BASEURL %>/css/estilostsp.css" rel="stylesheet" type="text/css"> 
<link href="../../../css/estilostsp.css" rel="stylesheet" type="text/css"> 
<script type='text/javascript' src="<%= BASEURL %>/js/boton.js"></script>
<script type='text/javascript' src="<%= BASEURL %>/js/hvida.js"></script>
<script type='text/javascript' src="<%= BASEURL %>/js/general.js"></script>
</head>

<body onLoad="redimensionar();forma.c_documento.focus();" onresize="redimensionar()">
<div id="capaSuperior" style="position:absolute; width:100%; height:100px; z-index:0; left: 0px; top: 0px;">
	<jsp:include page="/WEB-INF/toptsp.jsp?encabezado=Buscar Identidad"/>
</div>
<div id="capaCentral" style="position:absolute; width:100%; height:87%; z-index:0; left: 0px; top: 100px; overflow: scroll;">
<% String tipid="";%>
<form name="forma" method="post" action="<%=CONTROLLER%>?estado=Identidad&accion=Buscarx&pagina=VerIdentidades.jsp&carpeta=jsp/hvida/identidad">
  <table width="394" border="2" align="center">
    <tr>
      <td><table width="100%">
        <tr>
          <td width="173" class="subtitulo1">&nbsp;Informaci&oacute;n</td>
          <td width="205" class="barratitulo"><img src="<%=BASEURL%>/images/titulo.gif" width="32" height="20"></td>
        </tr>
      </table>
      <table width="100%" align="center" class="tablaInferior">
      <tr class="fila">
        <td width="119" align="left" valign="middle" >Documento</td>
        <td width="250" valign="middle"><input name="c_documento" class="textbox" type="text" id="c_documento2" maxlength="15" onKeyPress="soloDigitos(event,'decNO')" ></td>
      </tr>
    <tr class="fila">
      <td width="119" valign="middle" >Nombre</td>
      <td valign="middle"><input name="c_nombre" type="text" class="textbox" id="c_nombre"  size="40" maxlength="30" onKeyPress="soloAlfa(event)"></td>
    </tr>
    <tr class="fila">
      <td width="119" valign="middle" >Distrito</td>
      <td valign="middle"><input name="c_cia" type="text" class="textbox" id="c_cia" onKeyPress="soloAlfa(event)" size="5" maxlength="3" ></td>
    </tr> 
   
  </table></td>
    </tr>
  </table>
  <br>
   <div align="center">
		<img src="<%=BASEURL%>/images/botones/buscar.gif"  name="imgbuscar" onClick="BuscarIdentidad();" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" style="cursor:hand">&nbsp;		       &nbsp;
        <img src="<%=BASEURL%>/images/botones/salir.gif"  name="imgsalir"  onMouseOver="botonOver(this);" onClick="parent.close();" onMouseOut="botonOut(this);" style="cursor:hand">
   </div>

</form>
</div>
</body>
</html>
