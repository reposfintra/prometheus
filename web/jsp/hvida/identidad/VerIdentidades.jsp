<!--
- Autor : Ing. Diogenes Bastidas Morales
- Date  : 28 de noviembre de 2005
- Copyrigth Notice : Fintravalores S.A. S.A
-->
<%--
-@(#)
--Descripcion : Pagina JSP, lista las identidades

--%>

<%-- Declaracion de librerias--%>
<%@page contentType="text/html"%>
<%@page session="true"%>
<%@page errorPage="/error/ErrorPage.jsp"%>
<%@page import="java.util.*"%> 
<%@page import="com.tsp.operation.model.*"%>
<%@page import="com.tsp.util.*"%>
<%@page import="com.tsp.operation.model.beans.*, java.util.*"%>
<%@include file="/WEB-INF/InitModel.jsp"%>
<%@taglib uri="http://jsptags.com/tags/navigation/pager" prefix="pg" %>
 
<html>
<head>
<title>Listado de Identidad</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link href="<%= BASEURL %>/css/estilostsp.css" rel="stylesheet" type="text/css">
<link href="../../../css/estilostsp.css" rel="stylesheet" type="text/css"> 
<script type='text/javascript' src="<%= BASEURL %>/js/reporte.js"></script>
<script type='text/javascript' src="<%= BASEURL %>/js/boton.js"></script>

</head>
<body onLoad="redimensionar();" onresize="redimensionar()">
<div id="capaSuperior" style="position:absolute; width:100%; height:100px; z-index:0; left: 0px; top: 0px;">
	<jsp:include page="/WEB-INF/toptsp.jsp?encabezado=Listar Identidad"/>
</div>
<div id="capaCentral" style="position:absolute; width:100%; height:87%; z-index:0; left: 0px; top: 100px; overflow: scroll;">

  <%  String style = "simple";
    String position =  "bottom";
    String index =  "center";
    int maxPageItems = 10;
    int maxIndexPages = 10;
	
	Vector VecIden = model.identidadService.obtVecIdentidad();
    Identidad iden;

	if ( VecIden.size() >0 ){  
%>
</p>
<table width="1000" border="2" align="center">
    <tr>
      <td width="1009">
	  <table width="100%" align="center">
              <tr>
                <td width="373" class="subtitulo1">&nbsp;Datos Identidad</td>
                <td width="427" class="barratitulo"><img src="<%=BASEURL%>/images/titulo.gif" width="32" height="20"></td>
              </tr>
        </table>
		  <table width="99%" border="1" align="center" bordercolor="#999999" bgcolor="#F7F5F4">
          <tr class="tblTitulo">
          <td width="5%" align="center">Estado</td>
          <td width="5%" align="center">Documento</td>
          <td width="29%"  align="center">Nombres y Apellidos</td>
          <td width="18%" align="center">Direccion</td>
          <td width="14%"  align="center">Telefono</td>
          <td width="16%"  align="center"> Pais - Ciudad</td>
          <td width="13%" align="center">Distrito</td>
        </tr>
        <pg:pager
         items="<%=VecIden.size()%>"
         index="<%= index %>"
         maxPageItems="<%= maxPageItems %>"
         maxIndexPages="<%= maxIndexPages %>"
         isOffset="<%= true %>"
         export="offset,currentPageNumber=pageNumber"
         scope="request">
        <%-- keep track of preference --%>
        <%
      for (int i = offset.intValue(), l = Math.min(i + maxPageItems, VecIden.size()); i < l; i++)
	  {
          iden = (Identidad) VecIden.elementAt(i);%>
        <pg:item>
        <tr class="<%=( iden.getAprobado().equalsIgnoreCase("N") )?"filaroja":(i % 2 == 0 )?"filagris":"filaazul"%>" onMouseOver='cambiarColorMouse(this)'   style="cursor:hand"
        onClick="window.open('<%=CONTROLLER%>?estado=Identidad&accion=Buscar&pagina=identidadMod.jsp&carpeta=jsp/hvida/identidad&ced=<%=iden.getCedula()%>' ,'M','status=yes,scrollbars=no,width=780,height=650,resizable=yes');">
          <td width="5%" class="bordereporte"><%=iden.getEstado().equals("A")||iden.getEstado().equals("")?"Activo":"Inactivo"%></td>
          <td width="5%" class="bordereporte"><%=iden.getCedula()%></td>
          <td width="29%" class="bordereporte"><%=iden.getNombre()%></td>
          <td width="18%" class="bordereporte"><%=iden.getDireccion()%></td>
          <td width="14%" class="bordereporte"><%=iden.getTelefono()%> <%=(!iden.getTelefono1().trim().equals(""))?"<br>"+iden.getTelefono1():""%></td>
          <td width="16%" class="bordereporte" ><%=iden.getCodpais()%> - <%=iden.getCodciu()%></td>
          <td width="13%" class="bordereporte"><%=iden.getCia()%></td>
        </tr>
        </pg:item>
        <%}%>
        <tr class="bordereporte">
          <td td height="20" colspan="11" nowrap align="center">
		   <pg:index>
            <jsp:include page="/WEB-INF/jsp/googlesinimg.jsp" flush="true"/>      
           </pg:index> 
	      </td>
        </tr>
        </pg:pager>
      </table></td>
    </tr>
</table>
  <p>
      <%}
 else { %>
</p>
  <table border="2" align="center">
    <tr>
      <td><table width="410" height="40" border="1" align="center"  bordercolor="#F7F5F4" bgcolor="#FFFFFF">
          <tr>
            <td width="282" align="center" class="mensajes">Su b&uacute;squeda no arroj&oacute; resultados!</td>
            <td width="28" background="<%=BASEURL%>/images/cuadronaranja.JPG">&nbsp;</td>
            <td width="78">&nbsp;</td>
          </tr>
      </table></td>
    </tr>
  </table>
  <p>&nbsp; </p>
  <%}%>
<table width="1000" border="0" align="center">
   <tr>
     <td width="1515"><img src="<%=BASEURL%>/images/botones/regresar.gif"  name="imgaceptar" onClick="window.location='<%=CONTROLLER%>?estado=Menu&accion=Cargar&pagina=BuscarIdentidad.jsp&carpeta=/jsp/hvida/identidad&marco=no'" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" style="cursor:hand"></td>
   </tr>
 </table>
</div> 
</body>

</html>
