<%@page contentType="text/html"%>
<%@page session="true"%>
<%@page import="java.util.*"%>
<%@page import="com.tsp.operation.model.*"%>
<%@page import="com.tsp.operation.model.beans.*"%>
<%@page import="com.tsp.util.*"%>
<%@include file="/WEB-INF/InitModel.jsp"%>
<%@ taglib uri="http://jakarta.apache.org/taglibs/input-0.90" prefix="input" %>
<html>
<head>
<title>Identidad</title>
<link href="<%= BASEURL %>/css/estilotsp.css" rel='stylesheet'>
<link href="../../../css/estilotsp.css" rel="stylesheet" type="text/css">
</head>
<script type='text/javascript' src="<%= BASEURL %>/js/validar.js"></script>
<body onLoad="<%if(request.getParameter("mensajeAut")!=null){%>alert('<%=request.getParameter("mensajeAut")%>');<%}%>">
<%
String ccia = "",  doc = "", tipid, dir = "", idmims = "", nom="",  nom1="", nom2="",ape1="",ape2="",fec="", tel11 = "",tel12 = "",tel13 = "",tel21 = "",tel22 = "",tel23 = "",tel = "", em = "",cel="", cpais="", npais="", cest="", nest="", cciu="", nciu="",genero="",cub="",cargo="",ref="",obs="",estcivil="",expeced="",lib="",barrio="",lug="",sena="",nomnemo=""; 
String msg = (request.getParameter("mensaje")!=null)? request.getParameter("mensaje"):"" ;
String estad = (request.getAttribute("estados")!=null)? (String) request.getAttribute("estados"):"";
String ciuda = (request.getAttribute("ciudades")!=null)? (String) request.getAttribute("ciudades"):"";

String res = "";//respuesta de acciones

String p = request.getParameter("pais");
String e = request.getParameter("est");
String c = request.getParameter("ciudad");
tipid = (request.getParameter("tdoc")!=null)?request.getParameter("tdoc"):"";
String reg = (request.getParameter("r")!=null)?request.getParameter("r"):"";


int sw = 0; //recargar el td y vacio en pais
int sw1 = 0;//cargar estados
int sw2 = 0;//cargar ciudades
int sw3 = 0;//vacio en estados
int sw4 = 0;
int swcon = 0;
System.out.println("Clave "+request.getParameter("clave"));

if ( msg.equals("MsgAgregado")){//exito en el ingreso
	res = "Identidad ingresada exitosamente!";
} else if (msg.equals("MsgErrorIng")){//error en el ingreso y carga los valores de nuevo
	sw4 = 1;
    swcon = 1; 
    cpais = request.getParameter("pais");
	cest = request.getParameter("est");
	ccia = request.getParameter("ciudad");
    res = "Error...Identidad ya existe!";
		model.paisservice.buscarpais(p);      
        Pais spais = model.paisservice.obtenerpais();
		cpais = spais.getCountry_code();
		npais = spais.getCountry_name();

		model.estadoservice.listarEstado(cpais);
		model.estadoservice.buscarestado(cpais, e);      
		Estado sest = model.estadoservice.obtenerestado(); 
    	cest = sest.getdepartament_code();
		sw2 = 1;
        sw1 = 1;
		model.ciudadservice.listarCiudad(cpais, cest);
		model.ciudadservice.buscarCiudad(cpais,cest,c);
		Ciudad sciu = model.ciudadservice.obtenerCiudad();
        cciu = sciu.getcodciu();

}

if ( estad.equals("cargados") ){
    System.out.println("Pais "+ p);
	sw = 1;
	sw1 =1;
	genero = request.getParameter("c_genero");
	if (sw4 == 0){
		model.estadoservice.listarEstado(p);
        cpais = request.getParameter("cp");
        swcon=1;		
	}
	else {
		model.paisservice.buscarpais(p);      
        Pais spais = model.paisservice.obtenerpais();
		cpais = spais.getCountry_code();
		npais = spais.getCountry_name();

		model.estadoservice.listarEstado(cpais);
		model.estadoservice.buscarestado(cpais, e);      
		Estado sest = model.estadoservice.obtenerestado(); 
    	cest = sest.getdepartament_code();
		sw2 = 1;
		model.ciudadservice.listarCiudad(cpais, cest);
		model.ciudadservice.buscarCiudad(cpais,cest,c);
		Ciudad sciu = model.ciudadservice.obtenerCiudad();
        cciu = sciu.getcodciu();
	}
}
if ( ciuda.equals("cargados") ){
	sw = 1;
	sw1 = 1;
	sw2 = 1;
	model.ciudadservice.listarCiudad(p,e);
	genero = request.getParameter("c_genero");
    cpais = request.getParameter("cp");
	npais = request.getParameter("np");
	cest = request.getParameter("ce");
    swcon=1;
}
if(request.getParameter("clave")!=null || request.getParameter("verificacion")!=null){
	cpais = request.getParameter("pais");
	cciu = request.getParameter("ciudad");
	cest = request.getParameter("est");
	sw1=1;
	sw2=1;
	sw3=1;
	swcon = 1;
}
if(swcon ==1){
    tipid = request.getParameter("c_tipdoc");
    doc =  request.getParameter("c_doc");
	nom =  request.getParameter("c_nom");
    idmims =  request.getParameter("c_idmims");
    nom1 =  request.getParameter("c_nom1");
    nom2 =  request.getParameter("c_nom2");
    ape1 =  request.getParameter("c_ape1");
    ape2 =  request.getParameter("c_ape2");
    genero = request.getParameter("c_genero");
    fec = request.getParameter("c_fecha");   
    dir = request.getParameter("c_dir");
    tipid = request.getParameter("c_tipdoc");
	tel11 = request.getParameter("c_tel11");
	tel12 = request.getParameter("c_tel12");
	tel13 = request.getParameter("c_tel13");
    tel21 = request.getParameter("c_tel21");
    tel22 = request.getParameter("c_tel22");
    tel23 = request.getParameter("c_tel23");
    cel = request.getParameter("c_cel");
	em = request.getParameter("c_email");
    cargo = request.getParameter("c_cargo");
    ref = request.getParameter("c_ref");
    obs =request.getParameter("c_obse");
	//nuevos campos
	lib = request.getParameter("c_libreta");
	expeced = request.getParameter("c_expe");
	lug = request.getParameter("c_lugar");
	barrio = request.getParameter("c_barrio");
	estcivil = request.getParameter("c_estcivil");
	sena = request.getParameter("c_senal");
	reg =request.getParameter("reg");
	nomnemo =request.getParameter("c_nomnemo");
	
	
}


%>
<FORM name='forma' method='POST' action="<%=CONTROLLER%>?estado=Identidad&accion=Ingresar" >
  <table width="650" border="2"align="center">
    <tr>
      <td>
	  <table width="99%" border="1" align="center" bordercolor="#F7F5F4" bgcolor="#FFFFFF" >
        <tr class="fila">
          <td colspan="2" align="left" class="subtitulo1">&nbsp;Documentaci&oacute;n</td>
          <td colspan="2" align="left" class="barratitulo"><img src="<%=BASEURL%>/images/cuadrosverde.JPG" width="32" height="20"></td>
        </tr>
        <tr class="fila">
          <td align="left" >Tipo Documento</td>
          <td colspan="3" valign="middle">
            <select id="select2" name="c_tipdoc" class="listmenu" onChange="CamposIdent();">
              <%if ( sw == 0 ){%>
              <option value="" selected>Seleccione</option>
              <%}
         Vector tdocs = model.tdocumentoService.listarTDocumentos();
		for (int i = 0; i < tdocs.size(); i++){
			Tipo_Documento tdoc = (Tipo_Documento) tdocs.elementAt(i);				
		    if (tdoc.getId().equals(tipid)){%>
              <option value="<%=tipid%>" selected><%=tdoc.getDescripcion()%></option>
              <%}else{%>
              <option value="<%=tdoc.getId()%>"><%=tdoc.getDescripcion()%></option>
              <%}
		}%>
          </select>
            <input name="reg" type="hidden" id="reg" value="<%=reg%>"></td>
        </tr>
        <tr class="fila">
          <td width="90" align="left" > Documento </td>
          <td width="200" valign="middle"><input id="c_doc2" name="c_doc" type="text" class="textbox" value="<%=doc%>" maxlength="15" onKeyPress="soloDigitos(event,'decNO')"></td>
          <td width="81" valign="middle">Expedicion</td>
          <td width="183" valign="middle">           
			<%TreeMap expe = model.ciudadService.getCiudades(); %>
            <input:select name="c_expe" options="<%=expe%>" attributesText="style='width:80% ' class='textbox'"/></td></tr>
        <tr class="fila">
          <td align="left" >Id Mims</td>
          <td width="200" valign="middle"><input name="c_idmims" type="text" class="textbox" value="<%=idmims%>" maxlength="15" onKeyPress="soloDigitos(event,'decNO')"></td>
          <td width="81" valign="middle" id="lib" style="display:<%if (tipid.equals("CED")){%>block <%}else{%>none<%}%>">Libreta Militar </td>
          <td width="183" valign="middle" id="lib1" style="display:<%if (tipid.equals("CED")){%>block <%}else{%>none<%}%>"><input name="c_libreta" type="text" class="textbox" id="c_libreta" onKeyPress="soloDigitos(event,'decNO')" value="<%=lib%>" maxlength="15"></td>
        </tr>
        <tr class="fila" id="titnom" style="display:<%if (tipid.equals("CED")){%>block <%}else{%>none<%}%>">
          <td colspan="2" align="left" class="subtitulo1">&nbsp;Datos Personales</td>
          <td colspan="2" align="left" class="barratitulo"><img src="<%=BASEURL%>/images/cuadrosverde.JPG" width="32" height="20"></td>
        </tr>
        <tr class="fila" id="nombre" style=" display:<%if (tipid.equals("NIT")){%>block <%}else{%>none<%}%>">
          <td align="left" >Nombre</td>
          <td colspan="3" valign="middle"><input name="c_nom" type="text" class="textbox" id="c_nom3" value="<%=nom%>" size="40" maxlength="30">            </td>
        </tr>
        <tr class="fila" id="nombres" style="display:<%if (tipid.equals("CED")){%>block <%}else{%>none<%}%>">
          <td align="left" >Nombres Primero </td>
          <td valign="middle"><input id="c_nom1" name="c_nom1" type="text" class="textbox" value="<%=nom1%>" maxlength="15"></td>
          <td valign="middle">Segundo </td>
          <td valign="middle"><input id="c_nom22" name="c_nom2" type="text" class="textbox" value="<%=nom2%>" maxlength="15"></td>
        </tr>
        <tr class="fila" id="apellidos" style="display:<%if (tipid.equals("CED")){%>block <%}else{%>none<%}%>">
          <td width="90" align="left" >Apellidos Primero </td>
          <td valign="middle"><input id="c_ape12" name="c_ape1" type="text" class="textbox" value="<%=ape1%>" maxlength="15"></td>
          <td valign="middle">Segundo</td>
          <td valign="middle"><input id="c_ape22" name="c_ape2" type="text" class="textbox" value="<%=ape2%>" maxlength="15"></td>
        </tr> 
		<tr class="fila">
          <td align="left" >Nombre Nemotecnico</td>
          <td colspan="3" valign="middle"><input name="c_nomnemo" type="text" class="textbox" id="c_nomnemo" value="<%=nomnemo%>" maxlength="10">            </td>
        </tr>
        <tr class="fila" id="gen" style="display:<%if (tipid.equals("CED")){%>block <%}else{%>none<%}%>">
          <td width="90" align="left">Genero</td>
          <td valign="middle">
            <select name="c_genero" class="textbox" id="c_genero">
              <%if (genero.equals("M")){%>
              <option value="M" selected>Masculino</option>
              <option value="F">Femenino</option>
              <%}
          else if (genero.equals("F")){%>
              <option value="M">Masculino</option>
              <option value="F" selected>Femenino</option>
              <%} else{%>
              <option value="M" selected>Masculino</option>
              <option value="F">Femenino</option>
              <%}%>
          </select></td>
          <td valign="middle">Estado Civil</td>
          <td valign="middle">
            <select name="c_estcivil" class="textbox" id="c_estcivil">
              <option value="S" <%if (estcivil.equals("S")){%> selected <%}%>>Soltero</option>
              <option value="C" <%if (estcivil.equals("C")){%> selected <%}%>>Casado</option>
              <option value="V" <%if (estcivil.equals("V")){%> selected <%}%>>Viudo</option>
			  <option value="E" <%if (estcivil.equals("E")){%> selected <%}%>>Separado</option>
              <option value="U" <%if (estcivil.equals("U")){%> selected <%}%>>Union Libre</option>
            </select>
       </td>
        </tr>
        <tr class="fila" id="otros" style="display:<%if (tipid.equals("CED")){%>block <%}else{%>none<%}%>">
          <td width="90" align="left">Fecha de Nacimiento</td>
          <td valign="middle">
            <input  type="text"   name="c_fecha"  class="textbox" value="<%=fec%>" readonly >
            <a href="javascript:void(0)" onClick="if(self.gfPop)gfPop.fPopCalendar(document.forma.c_fecha);return false;" hidefocus><img name="popcal" align="absmiddle" src="<%=BASEURL%>/js/Calendario/calbtn.gif" width="34" height="22" border="0" alt=""></a>			</td>
          <td valign="middle">Lugar Nacimiento</td>
          <td valign="middle"><%TreeMap lugar = model.ciudadService.getCiudades(); %>
            <input:select name="c_lugar" options="<%=lugar%>" attributesText="style='width:80% ' class='textbox'"/></td>
        </tr>
         <tr class="fila" id="senal" style="display:<%if (tipid.equals("CED")){%>block <%}else{%>none<%}%>">
              <td align="left" >Se&ntilde;al Particular </td>
              <td colspan="3" valign="middle"> <input  name='c_senal' type='text'  class='textbox' id="c_senal" value='<%= sena%>' size='50' maxlength='50'></td></tr>
        <tr class="fila" >
          <td width="90"align="left" >Direcci&oacute;n</td>
          <td valign="middle"><input name="c_dir" type="text" class="textbox" id="c_dir2" value="<%=dir%>" size="40" maxlength="40">
          <td valign="middle">Barrio
          <td valign="middle"><input name="c_barrio" id="c_barrio" type="text" class="textbox" value="<%=barrio%>"> 
        </tr>
        <tr class="fila">
          <td width="90" rowspan="2" align="left" >Telefonos</td>
          <td valign="middle" class="subtitulos"><table width="62%"  border="0" cellpadding="0" cellspacing="0">
              <tr class="filaresaltada">
                <td width="18%">Pais</td>
                <td width="46%"><div align="left">&nbsp;&nbsp;&nbsp;&nbsp;Area</div></td>
                <td width="36%">Numero</td>
              </tr>
          </table></td>
          <td colspan="2" valign="middle" class="subtitulos"><table width="50%"  border="0" cellpadding="0" cellspacing="0">
              <tr class="filaresaltada">
                <td width="17%">Pais</td>
                <td width="36%"><div align="left">&nbsp;&nbsp;&nbsp;&nbsp;Area</div></td>
                <td width="47%">Numero</td>
              </tr>
          </table></td>
        <tr class="fila">
          <td valign="middle"><input name="c_tel11" type="text" class="textbox" id="c_tel112" onKeyPress="soloDigitos(event,'decNO')"  value="<%=tel11%>" size="2" maxlength="2">
              <input name="c_tel12" type="text" class="textbox" id="c_tel122" onKeyPress="soloDigitos(event,'decNO')" value="<%=tel12%>" size="3" maxlength="3">
              <input name="c_tel13" type="text" class="textbox" id="c_tel132" onKeyPress="soloDigitos(event,'decNO')" value="<%=tel13%>" maxlength="15"></td>
          <td colspan="2" valign="middle"><input name="c_tel21" type="text" class="textbox" id="c_tel21" onKeyPress="soloDigitos(event,'decNO')" value="<%=tel21%>" size="2" maxlength="2">
              <input name="c_tel22" type="text" class="textbox" id="c_tel222" onKeyPress="soloDigitos(event,'decNO')" value="<%=tel22%>" size="3" maxlength="3">
              <input name="c_tel23" type="text" class="textbox" id="c_tel232" onKeyPress="soloDigitos(event,'decNO')" value="<%=tel23%>" maxlength="15">
          </td>
        <tr class="fila">
          <td width="90" align="left" >Celular</td>
          <td valign="middle">
            <input name="c_cel" type="text" class="textbox" id="c_cel2" onKeyPress="soloDigitos(event,'decNO')" value="<%=cel%>" maxlength="15">
          </td>
          <td valign="middle">Email</td>
          <td valign="middle"><input name="c_email" id="c_email" type="text" class="textbox" value="<%=em%>"></td>
        </tr>
		<tr class="fila">
          <td width="90" align="left" >Pais</td>
          <td valign="middle">
            <select name="pais" id="select4" class="textbox" onChange="cargarSelects('<%=CONTROLLER%>?estado=Todos&accion=CargarEstados&carpeta=jsp/hvida/identidad&pagina=identidad.jsp');">
              <%if (model.paisservice.existePaises()){
			model.paisservice.listarpaises();
			Pais pais; 
	        List paises;
    	    paises = model.paisservice.obtenerpaises();		
			Iterator it = paises.iterator();%>
              <%if ( sw == 0 ){%>
              <option value="" selected>Seleccione</option>
              <%}
			while (it.hasNext()){  
				pais = (Pais) it.next();	
    	    	if (pais.getCountry_code().equals(cpais)){%>
              <option value="<%=cpais%>" selected><%=pais.getCountry_name()%></option>
              <%}else{%>
              <option value="<%=pais.getCountry_code()%>"><%=pais.getCountry_name()%></option>
              <%}
			}%>
              <%}else {%>
              <option value="" selected>No registra datos</option>
              <%}%>
            </select>
          <td valign="middle">Estado      
          <td valign="middle">
            <select id="select5" name="est"  class="textbox" onChange="cargarSelects('<%=CONTROLLER%>?estado=Todos&accion=CargarCiudades&carpeta=jsp/hvida/identidad&pagina=identidad.jsp');">
              <%if ( sw3 == 0 ){%>
              <option value="" selected>Seleccione</option>
              <%}   
            if (sw1 == 1){
			  if (model.estadoservice.existeEstadosxPais(p)){			
				List estados = model.estadoservice.obtenerEstados();
	        	Iterator it = estados.iterator();
				while (it.hasNext()){  
			    	Estado estado = (Estado) it.next();
					if (estado.getdepartament_code().equals(cest)){ %>
              <option value="<%=cest%>" selected><%=estado.getdepartament_name()%></option>
              <%}else{%>
              <option value="<%=estado.getdepartament_code()%>"><%=estado.getdepartament_name()%></option>
              <%}
		    	}
			}else {%>
              <option value="" selected>No registra datos</option>
              <%}%>
              <%}%>
            </select>          
          </tr>
        <tr class="fila">
          <td width="90"align="left" >Ciudad</td>
          <td valign="middle">
            <select id="select6" name="ciudad"  class="textbox">
              <option value="" selected>Seleccione</option>
              <%List ciudades;   
		if (sw2 == 1){
			if(model.ciudadservice.existeCiudadesxEstado(cpais, cest)){
				model.ciudadservice.listarCiudades(cpais, cest);
	           	ciudades = model.ciudadservice.obtenerCiudades();
            	Iterator it = ciudades.iterator();
			    while (it.hasNext()){  
			      	Ciudad ciu = (Ciudad) it.next();
					if (ciu.getcodciu().equals(cciu)){ %>
              <option value="<%=cciu%>" selected><%=ciu.getnomciu()%></option>
              <%}else {%>
              <option value="<%=ciu.getcodciu()%>"><%=ciu.getnomciu()%></option>
              <%}
				}%>
              <%}	else {%>
              <option value="" selected>No registra datos</option>
              <%}
        }%>
          </select>          
            
          <td valign="middle">Cargo          
          <td valign="middle"><input name="c_cargo" id="c_cargo" type="text" class="textbox" value="<%=cargo%>">        </tr>
        <tr class="fila">
          <td width="90" align="left" valign="top" >Referencia</td>
          <td valign="top"><input name="c_ref" id="c_ref2" type="text" class="textbox" value="<%=ref%>">
          </td>
          <td valign="top">Observaci&oacute;n</td>
          <td valign="middle"><textarea name="c_obse" cols="30" class="textbox" id="textarea"><%=obs%></textarea></td>
        </tr>
        <tr class="pie">
          <td colspan="4"><div align="center">
              <input type="button" class="boton" id="ingresar2" name="ingresar"  value="Ingresar" onClick="validarIdentidad('<%=CONTROLLER%>','<%=BASEURL%>','<%=request.getParameter("verificacion")%>');">
              <input type="button" class="boton" name="regresar" 
                value="Cancelar" 
                onClick="parent.close()">
          </div></td>
        </tr>
      </table>	  </td>
    </tr>
  </table>
  <%if(!res.equals("")){%>
  <p><table border="2" align="center">
  <tr>
    <td><table width="100%"  border="1" align="center"  bordercolor="#F7F5F4" bgcolor="#FFFFFF">
      <tr>
        <td width="229" align="center" class="mensajes"><%=res%></td>
        <td width="29" background="<%=BASEURL%>/images/cuadronaranja.JPG">&nbsp;</td>
        <td width="58">&nbsp;</td>
      </tr>
    </table></td>
  </tr>
</table>
</p>
 <%}%>
 <br>
 <%if(request.getParameter("clave")!=null){%>
 <table width="50%" border="2" align="center">
   <tr>
     <td><table width="100%"  border="1" align="center" cellpadding="3" cellspacing="2" bordercolor="#F7F5F4" bgcolor="#FFFFFF" class="fila">
       <tr >
         <td width="52%" height="22" class="subtitulo1" ><div align="center" class=>
           <div align="left"><strong> Solicitud de Clave</strong></div>
         </div></td>
         <td width="48%" height="22" class="barratitulo" ><img src="<%=BASEURL%>/images/cuadrosverde.JPG" width="32" height="20"></td>
       </tr>
       <tr>
         <td>INGRESE SU CLAVE:   
         </td>
         <td><input name="nclave" type="text" class="textbox" id="nclave"></td>
       </tr>
       <tr class="titulo">
         <td><div align="center" class="fila" >
           <div align="left">INGRESE SU NUMERO DE SOLICITUD:</div>
         </div></td>
         <td><input name="numsol" type="text" class="textbox" id="numsol" onKeyUp="window.opener.form1.solicitud.value=this.value;"></td>
       </tr>
       <tr>
         <td colspan="2"><div align="center">
             <input name="Submit3" type="button" class="boton" onClick="forma.action='<%=CONTROLLER%>?estado=Validar&accion=ClavePlacaProp';forma.submit()" value="Aceptar">
         </div></td>
       </tr>
     </table></td>
   </tr>
 </table>
  <%}%>
</form>
<iframe width=188 height=166 name="gToday:datetime:<%=BASEURL%>/js/calendartsp/agenda.js:gfPop:<%=BASEURL%>/js/calendartsp/plugins.js" id="gToday:datetime:<%=BASEURL%>/js/calendartsp/agenda.js:gfPop:<%=BASEURL%>/js/calendartsp/plugins.js" src="<%=BASEURL%>/js/calendartsp/ipopeng.htm" scrolling="no" frameborder="0" style="visibility:visible; z-index:999; position:absolute; left:-500px; top:0px;"></iframe>
</body>
</html>
