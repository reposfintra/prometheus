<!--
- Autor : Ing. Diogenes Bastidas Morales
- Date  : 19 de julio de 2005
- Copyrigth Notice : Fintravalores S.A. S.A
-->
<%--
-@(#)
--Descripcion : Pagina JSP, que maneja las modificaciones de identidades

--%>

<%-- Declaracion de librerias--%>
<%@page contentType="text/html"%>
<%@page session="true"%>
<%@page import="java.util.*"%>
<%@page import="com.tsp.operation.model.*"%>
<%@page import="com.tsp.operation.model.beans.*"%>
<%@page import="com.tsp.util.*"%>
<%@include file="/WEB-INF/InitModel.jsp"%> 
<%@ taglib uri="http://jakarta.apache.org/taglibs/input-0.90" prefix="input" %>
<html>
<head>
<title>Identidad</title>
<link href="<%= BASEURL %>/css/estilostsp.css" rel='stylesheet'>
<link href="../../../css/estilostsp.css" rel="stylesheet" type="text/css">
</head>
<script type='text/javascript' src="<%=BASEURL%>/js/hvida.js"></script>
<script type='text/javascript' src="<%=BASEURL%>/js/boton.js"></script>
<script type='text/javascript' src="<%=BASEURL%>/js/general.js"></script>
<script type='text/javascript' src="<%=BASEURL%>/js/Validacion.js"></script>
 
<body onLoad="<%=(request.getParameter("men").equalsIgnoreCase("MsgModificado"))?"parent.opener.location.reload();redimensionar();window.close();":"redimensionar();campos('no');"%>" onResize="redimensionar();">
<div id="capaSuperior" style="position:absolute; width:100%; height:100px; z-index:0; left: 0px; top: 0px;">
	<jsp:include page="/WEB-INF/toptsp.jsp?encabezado=Modificar Identidad"/>
</div>
<div id="capaCentral" style="position:absolute; width:100%; height:87%; z-index:0; left: 0px; top: 100px; overflow: scroll;">
<%
String control = session.getAttribute("USRIDEN")!=null?(String) session.getAttribute("USRIDEN"):"";
String ccia = "",  doc = "", tipid = "", dir = "", idmims = "", nom="", nom1="", nom2="",ape1="",ape2="",fec="", tel11 = "",tel12 = "",tel13 = "",tel21 = "",tel22 = "",tel23 = "",tel = "", em = "",cel="", cpais="", npais="", cest="", nest="", cciu="", nciu="",genero="",cub="",cargo="",ref="",obs="",estcivil="",expeced="",lib="",barrio="",lug="",sena="",nomnemo="",clas="",alias="",veto=""; 
String causa = "", ob = "",fuente="", mostrar ="",des="" ;
 cpais = (request.getParameter("pais")!=null)?request.getParameter("pais"):"";
 cest = (request.getParameter("est")!=null)?request.getParameter("est"):"";
 ccia = (request.getParameter("ciudad")!=null)?request.getParameter("ciudad"):"";
 tipid = (request.getParameter("tdoc")!=null)?request.getParameter("tdoc"):"";
 clas = (request.getParameter("clas")!=null)?request.getParameter("clas"):"0000000000";
 String op = "";
String men = (request.getParameter("men")!=null)?request.getParameter("men"):"";

String res = "";

int swcon =Integer.parseInt( (request.getParameter("sw")!=null)?request.getParameter("sw"):"0");


if(men.equalsIgnoreCase("MsgModificado") ){
	res = "Modificacion exitosa!";//respuesta de acciones
}
else if(men.equalsIgnoreCase("Existe") ){
    swcon =1;
	res = "Error...Nombre Nemotecnico ya existe!";//respuesta de acciones
}


if( men.equalsIgnoreCase("OK") || men.equals("MsgModificado")){
    Identidad iden = model.identidadService.obtIdentidad();
    Veto vetoB = model.vetoSvc.obtenerVeto();
    tipid = iden.getTipo_iden();
    doc =  iden.getCedula();
    idmims =  iden.getId_mims();
	nom =  iden.getNombre();
    nom1 =  iden.getNom1();
    nom2 =  iden.getNom2();
    ape1 =  iden.getApe1();
    ape2 =  iden.getApe2();
    genero = iden.getSexo();
    fec = iden.getFechanac();   
    dir = iden.getDireccion();
    tel11 = iden.getPais1();
	tel12 = iden.getArea1();
	tel13 = iden.getnum1();
    nomnemo = iden.getNomnemo();
    tel21 = iden.getPais2().trim();
    tel22 = iden.getArea2().trim();
    tel23 = iden.getnum2().trim();
    cel = iden.getCelular();
	em = iden.getE_mail();
    cargo = iden.getCargo();
    ref = iden.getRef1();
    obs = iden.getObservacion();
	//nuevos campos
	lib = iden.getLibmilitar();
	expeced = iden.getExpced();
	lug = iden.getLugarnac();
	barrio = iden.getBarrio();
	estcivil = iden.getEst_civil();
	cpais = iden.getCodpais(); 
	cest = iden.getCoddpto();
    sena =iden.getSenalParticular();
	clas = iden.getClasificacion();
    alias = iden.getNomnemo();
	veto = iden.getVeto();
    
	causa = (vetoB.getCausa()!=null)?vetoB.getCausa():"";
    ob = (vetoB.getObservacion()!=null)?vetoB.getObservacion():"";
    fuente = (vetoB.getFuente()!=null)?vetoB.getFuente():"";
	mostrar = (vetoB.getReferencia()!=null)?vetoB.getReferencia():"";
	des = (vetoB.getDescripcion()!=null)?vetoB.getDescripcion():"";
	
	if( cpais.equals("NR") && cest.equals("NR")){
	  cciu = "NR";
	}
	else{
		cciu = iden.getCodciu();	
	}	
}
if(swcon ==1 ){
    doc =  request.getParameter("c_doc");
    idmims =  request.getParameter("c_idmims");
	nom =  request.getParameter("c_nom");
    nom1 =  request.getParameter("c_nom1");
    nom2 =  request.getParameter("c_nom2");
    ape1 =  request.getParameter("c_ape1");
    ape2 =  request.getParameter("c_ape2");
    genero = request.getParameter("c_genero");
    fec = request.getParameter("c_fecha");   
    dir = request.getParameter("c_dir");
	tel11 = request.getParameter("c_tel11");
	tel12 = request.getParameter("c_tel12");
	tel13 = request.getParameter("c_tel13");
    tel21 = request.getParameter("c_tel21");
    tel22 = request.getParameter("c_tel22");
    tel23 = request.getParameter("c_tel23");
    cel = request.getParameter("c_cel");
	em = request.getParameter("c_email");
    cargo = request.getParameter("c_cargo");
    ref = request.getParameter("c_ref");
    obs =request.getParameter("c_obse");
    cpais = request.getParameter("pais");
	cest = request.getParameter("est");
	cciu = request.getParameter("ciudad");
	lib = request.getParameter("c_libreta");
	expeced = request.getParameter("c_expe");
	lug = request.getParameter("c_lugar");
	barrio = request.getParameter("c_barrio");
	estcivil = request.getParameter("c_estcivil");
	nomnemo = request.getParameter("c_nomnemo");
	tipid = request.getParameter("c_tipdoc");
	clas =  request.getParameter("clas");
    alias = request.getParameter("alias");
	veto = request.getParameter("veto");
	causa = request.getParameter("causa");;
    ob = request.getParameter("observacion");
    fuente = request.getParameter("fuente");
}
char[] tipo = clas.toCharArray();
String tit = (request.getParameter("soloinfo")==null)?"Modificar Identidad":request.getParameter("soloinfo");
%>

<FORM id="forma" name='forma' method='POST' action="<%=CONTROLLER%>?estado=Identidad&accion=Modificar" >
  <table width="790" border="1" align="center">
    <tr>
      <td><table width="100%" class="tablaInferior">
        <tr class="fila">
          <td colspan="2" align="left" class="subtitulo1">&nbsp;Documentaci&oacute;n </td>
          <td colspan="5" align="left" class="barratitulo"><img src="<%=BASEURL%>/images/titulo.gif" width="32" height="20"></td>
        </tr>
        <tr class="fila">
          <td width="78" align="left" >Clasificaci&oacute;n</td>
          <td colspan="6" valign="middle"><table width="100%" border="0" cellpadding="0" cellspacing="0">
                <tr class="fila">
					<td width="137" nowrap>
                       <input name="procarbon" type="checkbox" id="procarbon" value="A" onClick="campos('no')" <%if(tipo[4]!='0'){%> checked <%}%> <%if(tipo[4]=='A'){%> style='visibility:hidden' <%}%> >Proveedor Carbon</td>
                    <td width="447" <%if(tipo[4]=='A'){%> style='visibility:hidden' <%}%> >
                         <input name="empleado" type="checkbox" id="empleado" value="E" onClick="campos('no')" <%if( tipo[0]!='0' ){%> checked <%}%>>Empleado
                         <input name="conductor" type="checkbox" id="conductor" value="C" onClick="campos('no')" <%if(tipo[1]!='0'){%> checked <%}%> >Conductor
                         <input name="propietario" type="checkbox" id="propietario" value="P" onClick="campos('no')" <%if(tipo[2]!='0'){%> checked <%}%> >Propietario y/o Tenedor
	                     <input name="proveedor" type="checkbox" id="proveedor" value="V" onClick="campos('no')" <%if(tipo[3]!='0'){%> checked <%}%>>Proveedor</td>
                  </tr>
              </table></td>
        </tr>
        <tr class="fila">
          <td width="78" align="left" >Tipo de Documento </td>
          <td valign="middle"><select id="c_tipdoc" name="c_tipdoc" class="listmenu">
            <%if ( tipid.equals("") ){%>
            <option value="" selected>Seleccione</option>
            <%}
         		Vector tdocs = model.tdocumentoService.obtenerTipoDocumentos();
			  for (int i = 0; i < tdocs.size(); i++){
				   Tipo_Documento tdoc = (Tipo_Documento) tdocs.elementAt(i); %>
                   <option value="<%=tdoc.getId()%>" <%=(tdoc.getId().equals(tipid))?"selected":""%> ><%=tdoc.getDescripcion()%></option>
              <%}%>
          </select>
            <img src="<%=BASEURL%>/images/botones/iconos/obligatorio.gif" width="10" height="10"> </td>
          <td valign="middle"><%if(control.equals("S")){%>Reportado<%}%></td>                              
          <td colspan="4" valign="middle"><%if(control.equals("S")){%>
            <input type='hidden' name='veto_cambio' id='veto_cambio' value='<%=veto%>'>
            <select name="veto">
              <option value="N" <%=!veto.equals("S")?"selected":""%>>No</option>
              <option value="S" <%=veto.equals("S")?"selected":""%>>Si</option>
            </select>
            <%}else{%><%=mostrar.equals("S")?des:veto.equals("S")?"Reportado Si":"Reportado No"%>
            <input style='visibility:hidden' name="veto" type="hidden" id="veto" value="<%=veto%>">
            <%}%></td>
        </tr>
        <tr class="fila">
          <td width="78" align="left" > Documento </td>
          <td width="299" valign="middle"><input id="c_doc2" name="c_doc" type="hidden" class="textbox" value="<%=doc%>" maxlength="15" onKeyPress="soloDigitos(event,'decNO')"><%=doc%></td>
          <td width="165" valign="middle">Expedicion</td>
          <td colspan="4" valign="middle">           
			<%TreeMap expe = model.ciudadService.getTreMapCiudades();%>
            <input:select name="c_expe" options="<%=expe%>" attributesText="style='width:80%' class='textbox' " default="<%=expeced%>"/>			 </td>
       </tr>
	   <tr class="fila">
          <td align="left" >Id Mims</td>
          <td width="299" valign="middle"><input name="c_idmims" type="text" class="textbox" value="<%=idmims%>" maxlength="10" onKeyPress="soloDigitos(event,'decNO')">            </td>
          <td width="165" valign="middle">Libreta Militar </td>
          <td colspan="4" valign="middle"><input name="c_libreta" type="text" class="textbox" id="c_libreta" onKeyPress="soloDigitos(event,'decNO')" maxlength="15" value="<%=lib%>">		   </td>
        </tr>
        <tr class="fila" style="display:<%if(control.equals("S")){%> block <%}else{%>none<%}%>">
            
                        <td width="78">Causa</td>
                        <% TreeMap ca = model.tablaGenService.BuscarDatosTreemap("CAUSAVETO"); %>
                        
                            <td width="299">
                            <input type='hidden' name='causa_cambio' id='causa_cambio' value='<%=causa%>'>
                            <input:select name="causa" options="<%=ca%>" attributesText="style='width:80%' class='textbox' " default="<%=causa%>"/>
                            <img  src="<%=BASEURL%>/images/botones/iconos/obligatorio.gif" width="10" height="10"></td>
                       
                        <td width="126">Fuente Reporte </td>
                        <td width="517"><% TreeMap fu = model.tablaGenService.BuscarDatosTreemap("FUENTEREP"); %>
						                  <input:select name="fuente" options="<%=fu%>" attributesText="style='width:100%' class='textbox' " default="<%=fuente%>"/></td>
                        <td width="104">Observaci&oacute;n</td>
              <td width="280"><input type='hidden' name='observacion_cambio' id='observacion_cambio' value='<%=ob%>'>
                        <input name="observacion" value="<%=ob%>">                </td>
                        
        
       
        
		 <tr class="fila" id="titnom" style="display:<%if (tipo[0]!='0' || tipo[1]!='0' || tipo[2]!='0'){%>block <%}else{%>none<%}%>">
          <td colspan="2" align="left" class="subtitulo1">&nbsp;Datos Personales</td>
          <td colspan="5" align="left" class="barratitulo"><img src="<%=BASEURL%>/images/titulo.gif" width="32" height="20"></td>
        </tr>
        <tr class="fila" id="nombre" style=" display:<%if ((tipo[3]!='0' || tipo[4]!='0') && tipo[1]=='0' && tipo[2]=='0' && tipo[3]=='0'){%>block <%}else{%>none<%}%>">
          <td width="78" align="left" >Nombre</td>
          <td colspan="6" valign="middle"><input name="c_nom" type="text" class="textbox" id="c_nom" value="<%=nom%>" size="40" maxlength="100" onKeyPress="soloAlfa(event)">
            <img src="<%=BASEURL%>/images/botones/iconos/obligatorio.gif" width="10" height="10"></td>
        </tr>
        <tr class="fila" id="nombres" style="display:<%if (tipo[0]!='0' || tipo[1]!='0' || tipo[2]!='0'){%>block <%}else{%>none<%}%>">
          <td width="78" align="left" >Nombres Primero</td>
          <td valign="middle"><input id="c_nom1" name="c_nom1" type="text" class="textbox" value="<%=nom1%>" maxlength="15">
            <img src="<%=BASEURL%>/images/botones/iconos/obligatorio.gif" width="10" height="10"></td>
          <td valign="middle">Segundo</td>
          <td colspan="4" valign="middle"><input id="c_nom2" name="c_nom2" type="text" class="textbox" value="<%=nom2%>" maxlength="15"></td>
        </tr>
        <tr class="fila" id="apellidos" style="display:<%if (tipo[0]!='0' || tipo[1]!='0' || tipo[2]!='0'){%>block <%}else{%>none<%}%>">
          <td width="78" align="left" >Apellidos Primero</td>
          <td valign="middle"><input id="descripcion3" name="c_ape1" type="text" class="textbox" value="<%=ape1%>" maxlength="15">
            <img src="<%=BASEURL%>/images/botones/iconos/obligatorio.gif" width="10" height="10"></td>
          <td valign="middle">Segundo</td>
          <td colspan="4" valign="middle"><input id="c_ape2" name="c_ape2" type="text" class="textbox" value="<%=ape2%>" maxlength="15">
            <img src="<%=BASEURL%>/images/botones/iconos/obligatorio.gif" width="10" height="10"></td>
        </tr>
		<tr class="fila">
          <td align="left" >Nombre Nemotecnico</td>
          <td colspan="6" valign="middle"><input name="c_nomnemo" type= class="textbox" id="c_nomnemo" value="<%=(nomnemo==null)?"":nomnemo%>" maxlength="10" >
            <input name="alias" type="hidden" id="alias" value="<%=alias%>"> </td>
        </tr>
       <tr class="fila" id="gen" style="display:<%if (tipo[0]!='0' || tipo[1]!='0' || tipo[2]!='0'){%>block <%}else{%>none<%}%>">
          <td width="78">Genero</td>
          <td valign="middle">
            <select name="c_genero" class="textbox" id="select3">
              <%if (genero.equals("M")){%>
              <option value="M" selected>Masculino</option>
              <option value="F">Femenino</option>
              <%}
          else if (genero.equals("F")){%>
              <option value="M">Masculino</option>
              <option value="F" selected>Femenino</option>
              <%} else{%>
              <option value="M" selected>Masculino</option>
              <option value="F">Femenino</option>
              <%}%>
          </select>            </td>
          <td valign="middle">Estado Civil</td>
          <td colspan="4" valign="middle">
            <select name="c_estcivil" class="textbox" id="c_estcivil">
			  <option value="S" <%if (estcivil.equals("S")){%> selected <%}%>>Soltero</option>
              <option value="C" <%if (estcivil.equals("C")){%> selected <%}%>>Casado</option>
              <option value="V" <%if (estcivil.equals("V")){%> selected <%}%>>Viudo</option>
			  <option value="E" <%if (estcivil.equals("E")){%> selected <%}%>>Separado</option>
              <option value="U" <%if (estcivil.equals("U")){%> selected <%}%>>Union Libre</option>
            </select>            </td>
      </tr>
		
		<tr class="fila" id="otros" style="display:<%if (tipo[0]!='0' || tipo[1]!='0' || tipo[2]!='0'){%>block <%}else{%>none<%}%>">
          <td width="78">Fecha de Nacimiento</td>
          <td valign="middle">            <p>              
              <input  type="text"   name="c_fecha"  class="textbox" value="<%=fec%>" readonly >
            <a href="javascript:void(0)" onclick="if(self.gfPop)gfPop.fPopCalendar(document.forma.c_fecha);return false;" HIDEFOCUS><img src="<%=BASEURL%>\js\Calendario\cal.gif" width="16" height="16" border="0" alt="De click aqui para escoger la fecha"></a> </p>            </td>
          <td valign="middle">Lugar Nacimiento</td>
          <td colspan="4" valign="middle"><%TreeMap lugar = model.ciudadService.getTreMapCiudades(); %>
            <input:select name="c_lugar" options="<%=lugar%>" attributesText="style='width:80% ' class='textbox'" default="<%=lug%>"/></td>
        </tr>
		<tr class="fila" id="senal" style="display:<%if (tipo[0]!='0' || tipo[1]!='0' || tipo[2]!='0'){%>block <%}else{%>none<%}%>">
              <td align="left" >Se&ntilde;al Particular </td>
              <td colspan="6" valign="middle"> <input  name='c_senal' type='text'  class='textbox' id="c_senal" value='<%= sena%>' size='50' maxlength='50' onKeyPress="soloAlfa(event)"></td></tr>
        <tr class="fila">
          <td width="78"align="left" >Direcci&oacute;n</td>
          <td valign="middle"><input name="c_dir" type="text" class="textbox" id="c_dir2" value="<%=dir%>" size="40" maxlength="40">
            <td valign="middle">Barrio
          <td colspan="4" valign="middle"><input name="c_barrio" type="text" class="textbox" id="c_barrio" value="<%=barrio%>" maxlength="25" onKeyPress="soloAlfa(event)">            
          </tr>
        <tr class="fila">
          <td width="78" rowspan="2" align="left" >Telefonos</td>
          <td valign="middle" class="subtitulos"><table width="62%"  border="0" cellpadding="0" cellspacing="0">
              <tr class="fila">
                <td width="18%">Pais</td>
                <td width="46%"><div align="left">&nbsp;&nbsp;&nbsp;&nbsp;Area</div></td>
                <td width="36%">Numero</td>
              </tr>
          </table></td>
          <td colspan="5" valign="middle" class="subtitulos"><table width="50%"  border="0" cellpadding="0" cellspacing="0">
              <tr class="fila">
                <td width="17%">Pais</td>
                <td width="36%"><div align="left">&nbsp;&nbsp;&nbsp;&nbsp;Area</div></td>
                <td width="47%">Numero</td>
              </tr>
          </table></td>
        <tr class="fila">
          <td valign="middle"><input name="c_tel11" type="text" class="textbox" id="c_tel11" onKeyPress="soloDigitos(event,'decNO')"  value="<%=tel11%>" size="2" maxlength="2" readonly>
              <input name="c_tel12" type="text" class="textbox" id="c_tel12" onKeyPress="soloDigitos(event,'decNO')" value="<%=tel12%>" size="3" maxlength="3" readonly>
              <input name="c_tel13" type="text" class="textbox" id="c_tel13" onKeyPress="soloDigitos(event,'decNO')" value="<%=tel13%>" maxlength="15" readonly>              
              <img src="<%=BASEURL%>/images/botones/iconos/notas.gif" width="15" height="15" onClick="modTelefono( '<%=BASEURL%>' ,1 )"> </td>
          <td colspan="5" valign="middle"><input name="c_tel21" type="text" class="textbox" id="c_tel21" onKeyPress="soloDigitos(event,'decNO')" value="<%=tel21%>" size="2" maxlength="2" readonly>
              <input name="c_tel22" type="text" class="textbox" id="c_tel22" onKeyPress="soloDigitos(event,'decNO')" value="<%=tel22%>" size="3" maxlength="3" readonly>
              <input name="c_tel23" type="text" class="textbox" id="c_tel23" onKeyPress="soloDigitos(event,'decNO')" value="<%=tel23%>" maxlength="15" readonly> <img src="<%=BASEURL%>/images/botones/iconos/notas.gif" width="15" height="15" onClick="modTelefono( '<%=BASEURL%>' ,2 )">
          </td>
        <tr class="fila">
          <td width="78" align="left" >Celular</td>
          <td valign="middle">
            <input name="c_cel" type="text" class="textbox" id="c_cel2" onKeyPress="soloDigitos(event,'decNO')" value="<%=cel%>" maxlength="15">
          </td>
          <td valign="middle">Email</td>
          <td colspan="4" valign="middle"><input name="c_email" type="text" class="textbox" id="c_email" value="<%=em%>" maxlength="50"></td>
        </tr>
		<tr class="fila">
          <td width="78" align="left" >Pais</td>
          <td valign="middle">
            <select name="pais"  class="textbox" onChange="cargarSelects('<%=CONTROLLER%>?estado=Cargar&accion=Select&carpeta=jsp/hvida/identidad&pagina=identidadMod.jsp');">
              <option value="" >Seleccione</option>
              <%    List paises = model.paisservice.obtenerpaises();		
						Iterator it = paises.iterator();
				  		while (it.hasNext()){  
								Pais pais = (Pais) it.next();%>
     			      		  		<option value="<%=pais.getCountry_code()%>" <%=(pais.getCountry_code().equals(cpais))?"selected":""%> ><%=pais.getCountry_name()%></option>
						<%}%>
            </select>          
            <td valign="middle">Estado      
          <td colspan="4" valign="middle">
            <select id="select" name="est"  class="textbox" onChange="cargarSelects('<%=CONTROLLER%>?estado=Cargar&accion=Select&carpeta=jsp/hvida/identidad&pagina=identidadMod.jsp');">
              <%if(cpais.equals("")){%>
              <option value="" selected>Seleccione</option>
              <%}else{	
			  	 	List estados = model.estadoservice.obtenerEstados();
					if (estados.size() > 0){
	        		   Iterator it1 = estados.iterator();%>
					   <option value="" >Seleccione</option>
					   <% while (it1.hasNext()){  
			    		    Estado estado = (Estado) it1.next();%>
              				<option value="<%=estado.getdepartament_code()%>" <%=(estado.getdepartament_code().equals(cest))?"selected":""%> ><%=estado.getdepartament_name()%></option>
		    		  <%}
			       }else {%>
              			<option value="" selected>No registra datos</option>
                 <%}
			   }%>
            </select>          
          </tr>
        <tr class="fila">
          <td width="78"align="left" >Ciudad</td>
          <td valign="middle">
            <select id="select2" name="ciudad"  class="textbox">
              <%if(cpais.equals("")&&cest.equals("")){%>
              <option value="" selected>Seleccione</option>
              <%} else{
	           		  List ciudades = model.ciudadservice.obtenerCiudades();
  				      if(ciudades.size() > 0){
            		      Iterator it2 = ciudades.iterator();%>
					       <option value="" >Seleccione</option>
                          <%while (it2.hasNext()){  
			      		        Ciudad ciu = (Ciudad) it2.next();%>
              					<option value="<%=ciu.getcodciu()%>" <%=(ciu.getcodciu().equals(cciu))?"selected":""%>><%=ciu.getnomciu()%></option>
					      <%}
				    }
                    else {%>
              		    <option value="" selected>No registra datos</option>
                  <%}
				}%>
            </select>          
            <td valign="middle">Cargo          
          <td colspan="4" valign="middle"><input name="c_cargo" type="text" class="textbox" id="c_cargo" value="<%=cargo%>" size="40" maxlength="30">        
          </tr>
        <tr class="fila">
          <td width="78" align="left" valign="top" >Referencia</td>
          <td valign="top"><input name="c_ref" type="text" class="textbox" id="c_ref2" value="<%=ref%>" size="40" maxlength="30" onKeyPress="soloAlfa(event)">
          </td>
          <td valign="top">Observaci&oacute;n</td>
          <td colspan="4" valign="middle"><textarea name="c_obse" cols="30" class="textbox" id="textarea"><%=obs%></textarea></td>
        </tr>
      </table></td>
    </tr>
  </table>
  <br>
    <%if (request.getParameter("soloinfo")==null){%>
  	 	<div align="center">
			<img src="<%=BASEURL%>/images/botones/modificar.gif"  name="imgmodificar" onClick="validarIdentidad('<%=CONTROLLER%>','<%=BASEURL%>');" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" style="cursor:hand">&nbsp;
        	<img src="<%=BASEURL%>/images/botones/anular.gif"  name="imganular" onClick="window.location='<%=CONTROLLER%>?estado=Identidad&accion=Anular&cedula=<%=doc%>'" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" style="cursor:hand">&nbsp;
        	<img src="<%=BASEURL%>/images/botones/salir.gif"  name="imgsalir"  onMouseOver="botonOver(this);" onClick="parent.close();" onMouseOut="botonOut(this);" style="cursor:hand">
     	</div>
    <%}%>
  <br>
    <%if(!res.equals("")){%>
	<p>
   <table border="2" align="center">
     <tr>
    <td><table width="100%"  border="1" align="center"  bordercolor="#F7F5F4" bgcolor="#FFFFFF">
      <tr>
        <td width="229" align="center" class="mensajes"><%=res%></td>
        <td width="29" background="<%=BASEURL%>/images/cuadronaranja.JPG">&nbsp;</td>
        <td width="58">&nbsp;</td>
      </tr>
    </table></td>
  </tr>
</table>
  </p>
   <%}%>
</form>

<!--  PopCalendar(tag name and id must match) Tags should sit at the page bottom -->
<iframe width=188 height=166 name="gToday:normal:agenda.js:gfPop:plugins.js" id="gToday:normal:agenda.js:gfPop:plugins.js" src="<%=BASEURL%>/js/Calendario/ipopeng.htm" scrolling="no" frameborder="0" style="visibility:visible; z-index:999; position:absolute; left:-500px; top:0px;"> </iframe>
</div>
</body>
</html>
 


