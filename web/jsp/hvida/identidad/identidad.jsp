<!--
- Autor : Ing. Diogenes Bastidas Morales
- Date  : 19 de julio de 2005
- Copyrigth Notice : Fintravalores S.A. S.A
-->
<%--
-@(#)
--Descripcion : Pagina JSP, que maneja el ingreso de identidades

--%>

<%-- Declaracion de librerias--%>
<%@page contentType="text/html"%>
<%@page session="true"%>
<%@page import="java.util.*"%>
<%@page import="com.tsp.operation.model.*"%>
<%@page import="com.tsp.operation.model.beans.*"%>
<%@page import="com.tsp.util.*"%>
<%@include file="/WEB-INF/InitModel.jsp"%>
<%@taglib uri="http://jakarta.apache.org/taglibs/input-0.90" prefix="input" %>
<%@taglib uri="http://www.sanchezpolo.com/sot" prefix="tsp"%>
<html>
<head>
<!--<script type="text/javascript">
            jQuery.noConflict();
            var $j = jQuery;
        </script>    -->
<title>Identidad</title>
<link href="<%= BASEURL %>/css/estilostsp.css" rel='stylesheet'>
<link href="../../../css/estilostsp.css" rel="stylesheet" type="text/css">
<script type='text/javascript' src="<%= BASEURL %>/js/hvida.js"></script>
<script type='text/javascript' src="<%= BASEURL %>/js/boton.js"></script>
<script type='text/javascript' src="<%= BASEURL %>/js/prototype.js"></script>
<<script type="text/javascript" src="/fintra/js/jquery/jquery-ui/jquery.min.js"></script>
<script language="JavaScript" type="text/javascript" src="/fintra/js/jquery-ui-1.8.5.custom.min.js"></script>
        <script type="text/javascript" src="./js/recursosHumanos/maestroEmpleados.js"></script>
        
<%String afil = request.getParameter("afil")!=null?request.getParameter("afil"):"N";
String control = session.getAttribute("USRIDEN")!=null?(String) session.getAttribute("USRIDEN"):"";
String ccia = "",  doc = "", tipid, dir = "", idmims = "", nom="",  nom1="", nom2="",ape1="",ape2="",fec="", tel11 = "",tel12 = "",tel13 = "",tel21 = "",tel22 = "",tel23 = "",tel = "", em = "",cel="", cpais="", npais="", cest="", nest="", cciu="", nciu="",genero="",cub="",cargo="",ref="",obs="",estcivil="",expeced="",lib="",barrio="",lug="",sena="",nomnemo="",clas="",veto="";
String msg = (request.getParameter("mensaje")!=null)? request.getParameter("mensaje"):"" ;


String res = "";//respuesta de acciones

 cpais = (request.getParameter("pais")!=null)?request.getParameter("pais"):"";
 cest = (request.getParameter("est")!=null)?request.getParameter("est"):"";
 ccia = (request.getParameter("ciudad")!=null)?request.getParameter("ciudad"):"";
%>
<script type="text/javascript">
    function habilAfil(){
        var check = $('sede').checked;
        if(check!=true){
            $('afiliado').selectedIndex = 0;
            $('c_tipdoc').selectedIndex = 0;
            $('c_doc').value = "";
        }
    }
    function bloquear(){
        var check = $('sede').checked;
        if(check==false){
            setTimeout(function() { document.getElementById('c_nom').focus(); }, 5);
        }
    }
    function limpiar(){
    var check = $('sede').checked;
        if(check==true){
            document.getElementById('c_tipdoc').selectedIndex = 0;
            document.getElementById('c_doc').value="";
        }else{
            document.getElementById('afiliado').selectedIndex = 0;
        }
    }

    function bloquear2(){
        var check = $('sede').checked;
        if(check==true){
           setTimeout(function() { document.getElementById('afiliado').focus(); }, 5);
        }
    }

    function cargadoc(ind){
    var check = $('sede').checked;
        if(check==true){
        var val = $('afiliado').options[ind].value;
        $('c_doc').value = val;
        var url = "<%= CONTROLLER%>?estado=Proveedor&accion=Ingresar";
        //alert(valorfiltro);
        var p =  'opcion=buscardat&nit='+val;
        new Ajax.Request(
            url,
            {
                method: 'post',
                parameters: p,
                //onLoading: loading,
                onComplete: llenarDiv
            }
        );
            }
    }

    function llenarDiv(response){
        var splitt = (response.responseText).split(";_;");
        $('c_doc').value = $('c_doc').value + '_' + splitt[1];
        escogerSelect('c_tipdoc',splitt[0]);
    }

    function escogerSelect(nombresel,dato){
        var select = document.getElementById(nombresel);
        for (i=0;i<select.length;i++){
            if(dato == select.options[i].value){
                select.options[i].selected = true;
                break;//<!-- 20101111  -->
            }
        }
    }
    //<!-- 20101111  -->
    var id_div='';
    function cargarEstados(ind){
        id_div='est_1';
        //var dato = $('pais').options[ind].value;
        var dato = $('#pais').val();
        //alert(dato);
        var url = "<%= CONTROLLER%>?estado=Proveedor&accion=Ingresar";
        var p =  'opcion=cargarest&cod='+dato;
        new Ajax.Request(url,{method:'post', parameters:p,/* onLoading:loading, */onComplete:finish});
    }

    function cargarCiudades(ind){
        id_div='ciudad_1';
//        var dato = $('est').options[ind].value;
        var dato = $('#est').val();
        //var ind2 = $('pais').selectedIndex;
        //var pais = $('pais').options[ind2].value;
        var pais = $('#pais').val();
        var url = "<%= CONTROLLER%>?estado=Proveedor&accion=Ingresar";
        var p =  'opcion=cargarciu&cod='+dato+'&pais='+pais;
        new Ajax.Request(url,{method:'post', parameters:p, /*onLoading:loading,*/ onComplete:finish2});
    }

    function loading(){
        $(id_div).innerHTML = '<select><option>Procesando...</option></select>';
    }

    function finish(response){
        est_1.innerHTML = response.responseText;

    }

    function finish2(response){
        ciudad_1.innerHTML = response.responseText;
    }

    function soloNumeros(id) {
                 var valor = document.getElementById(id).value;
                 valor =  valor.replace(/[^0-9^.]+/gi,"");
                 document.getElementById(id).value = valor;
            }
    function soloAlfa(id) {
         var valor = document.getElementById(id).value;
         valor =  valor.replace(/[^A-Z,a-z^ ]+/gi,"");
         document.getElementById(id).value = valor;
    }

</script>
</head>

<body  onLoad="campos('si');redimensionar();" onresize="redimensionar()" >
<div id="capaSuperior" style="position:absolute; width:100%; height:100px; z-index:0; left: 0px; top: 0px;">
	<jsp:include page="/WEB-INF/toptsp.jsp?encabezado=Registrar Identidad"/>
</div>
<div id="capaCentral" style="position:absolute; width:100%; height:87%; z-index:0; left: 0px; top: 100px; overflow: scroll;">
<%
String base = (request.getParameter("base")!=null)?request.getParameter("base"):"";
tipid = (request.getParameter("tdoc")!=null)?request.getParameter("tdoc"):"";
clas = (request.getParameter("clas")!=null)?request.getParameter("clas"):"0000000000";
String reg = (request.getParameter("r")!=null)?request.getParameter("r"):"";


int swcon =Integer.parseInt( (request.getParameter("sw")!=null)?request.getParameter("sw"):"0");


if ( msg.equals("MsgAgregado")){//exito en el ingreso
	res = "Identidad ingresada exitosamente!";
}
else if (msg.equals("MsgErrorIng")){//error en el ingreso y carga los valores de nuevo
    swcon = 1;
    res = "Error...Identidad ya existe!";
}
else if (msg.equals("MsgNomNemo")){//error en el nombre nemotecnico
    swcon = 1;
    res = "Error...Nombre Nemotecnico ya existe!";
}
else if (msg.equals("MsgErrorAfl")){//error en el nombre nemotecnico
    swcon = 1;
    res = "Error...El afiliado se encuentra anulado!";
}

if(swcon ==1){
    doc =  request.getParameter("c_doc");
	nom =  request.getParameter("c_nom");
    idmims =  request.getParameter("c_idmims");
    nom1 =  request.getParameter("c_nom1");
    nom2 =  request.getParameter("c_nom2");
    ape1 =  request.getParameter("c_ape1");
    ape2 =  request.getParameter("c_ape2");
    genero = request.getParameter("c_genero");
    fec = request.getParameter("c_fecha");
    dir = request.getParameter("c_dir");
	tel11 = request.getParameter("c_tel11");
	tel12 = request.getParameter("c_tel12");
	tel13 = request.getParameter("c_tel13");
    tel21 = request.getParameter("c_tel21");
    tel22 = request.getParameter("c_tel22");
    tel23 = request.getParameter("c_tel23");
    cel = request.getParameter("c_cel");
	em = request.getParameter("c_email");
    cargo = request.getParameter("c_cargo");
    ref = request.getParameter("c_ref");
    obs =request.getParameter("c_obse");
	//nuevos campos
	lib = request.getParameter("c_libreta");
	expeced = request.getParameter("c_expe");
	lug = request.getParameter("c_lugar");
	barrio = request.getParameter("c_barrio");
	estcivil = request.getParameter("c_estcivil");
	sena = request.getParameter("c_senal");
	reg =request.getParameter("reg");
	nomnemo =request.getParameter("c_nomnemo");
	tipid = request.getParameter("c_tipdoc");
    veto = request.getParameter("veto");

}

char[] tipo = clas.toCharArray();
%>
<% if(!cpais.equals("")&&!cest.equals("")&&!ccia.equals("")){%>
<script type="text/javascript">
      id_div='est_1';
        var url = "<%= CONTROLLER%>?estado=Proveedor&accion=Ingresar";
        var p =  'opcion=cargarest&cod=<%=cpais%>';
        new Ajax.Request(url,{method:'post', parameters:p,/* onLoading:loading, */onComplete:finish});
        var url = "<%= CONTROLLER%>?estado=Proveedor&accion=Ingresar";
        var p =  'opcion=cargarciu&cod=<%=cest%>&pais=<%=cpais%>';
        new Ajax.Request(url,{method:'post', parameters:p, /*onLoading:loading,*/ onComplete:finish2});

</script>

<%}%>

<form name='forma' id='forma'  method='POST' action="<%=CONTROLLER%>?estado=Identidad&accion=Ingresar&pag=<%=afil%>" >
  <table width="760" border="2"align="center">
    <tr>
      <td>
          <table width="100%" class="tablaInferior" style="border-collapse: collapse;">
        <tr class="fila">
          <td colspan="2" align="left" class="subtitulo1">&nbsp;Documentaci&oacute;n</td>
          <td colspan="2" align="left" class="barratitulo"><img src="<%=BASEURL%>/images/titulo.gif" width="32" height="20"> </td>
        </tr>
        <%
        if(afil.equals("N")){
        %>
        <tr class="fila">
          <td align="left" >Clasificaci&oacute;n</td>
          <td colspan="3" valign="middle">
              <table width="100%" border="0" cellpadding="0" cellspacing="0">
                <tr class="fila">
                    <td width="137" nowrap>
                       <input name="procarbon" type="checkbox" id="procarbon" value="A" onClick="campos('si')" <%if(tipo[4]!='0'){%> checked <%}%> <%=(!base.equals(""))?"style='visibility:hidden'":""%> >Proveedor Carbon
                    </td>
                    <td width="447" <%=(!base.equals(""))?"style='visibility:hidden'":""%> >
                         <input name="empleado" type="checkbox" id="empleado" value="E" onClick="campos('si')" <%if( tipo[0]!='0' ){%> checked <%}%>>Empleado
                         <input name="conductor" type="checkbox" id="conductor" value="C" onClick="campos('si')" <%if(tipo[1]!='0'){%> checked <%}%> >Conductor
                         <input name="propietario" type="checkbox" id="propietario" value="P" onClick="campos('si')" <%if(tipo[2]!='0'){%> checked <%}%> >Propietario y/o Tenedor
	                 <input name="proveedor" type="checkbox" id="proveedor" value="V" onClick="campos('si')" <%if(tipo[3]!='0'){%> checked <%}%>>Proveedor</td>
                  </tr>
              </table>
         </td>
        </tr>
        <%
            }else{
        %>
        <tr class="fila">
        <div style="display:none; visibility:hidden">
            <input name="proveedor" type="checkbox" id="proveedor" value="V" onClick="campos('si')"  checked>Proveedor
            <input name="procarbon" type="checkbox" id="procarbon" value="A" onClick="campos('si')" <%if (tipo[4] != '0') {%> checked <%}%> <%=(!base.equals("")) ? "style='visibility:hidden'" : ""%> >Proveedor Carbon
            <input name="empleado" type="checkbox" id="empleado" value="E" onClick="campos('si')" <%if (tipo[0] != '0') {%> checked <%}%>>Empleado
            <input name="conductor" type="checkbox" id="conductor" value="C" onClick="campos('si')" <%if (tipo[1] != '0') {%> checked <%}%> >Conductor
            <input name="propietario" type="checkbox" id="propietario" value="P" onClick="campos('si')" <%if (tipo[2] != '0') {%> checked <%}%> >Propietario y/o Tenedor
        </div>
          <td colspan="2" align="left">
              <table width="100%" border="0" cellpadding="0" cellspacing="0">
                  <tr class="fila">
                      <td>Sede</td>
                      <td>
                          <input type="checkbox" name="sede" id="sede" value="S" onclick="habilAfil();">
                      </td>
                      <td align="left">Afiliado</td>
                      <td>
                          <select name="afiliado" id="afiliado" onfocus="bloquear();" onchange="limpiar();cargadoc(this.selectedIndex);">
                              <option value="0">...</option>
                              <%
                                ArrayList listafil = null;
                                boolean pasa = true;
                                try{
                                    listafil = model.proveedorService.listaAfils();
                                }
                                catch(Exception e){
                                    pasa = false;
                                    System.out.println("Error: "+e.toString());
                                    e.printStackTrace();
                                }
                                if(pasa==true && listafil.size()>0){
                                    String[] splitt = null;
                                    for(int i=0;i<listafil.size();i++){
                                        splitt = ((String)(listafil.get(i))).split(";_;");
                              %>
                              <option value="<%=splitt[0]%>"><%=splitt[1]%></option>
                              <%
                                    }
                                }
                              %>
                          </select>
                      </td>
                  </tr>
              </table>
          </td>
          <td colspan="2" ></td>
        </tr>
        <%
            }
        %>
        <tr class="fila">
          <td align="left" >Tipo Documento</td>
          <td valign="middle">
            <select id="c_tipdoc" name="c_tipdoc" class="listmenu" onfocus="bloquear2();" onchange="limpiar();">
            <%if ( tipid.equals("") ){%>
                <option value="" selected>Seleccione</option>
            <%}
         	  Vector tdocs = model.tdocumentoService.obtenerTipoDocumentos();
			  for (int i = 0; i < tdocs.size(); i++){
				   Tipo_Documento tdoc = (Tipo_Documento) tdocs.elementAt(i); %>
                   <option value="<%=tdoc.getId()%>" <%=(tdoc.getId().equals(tipid))?"selected":""%> ><%=tdoc.getDescripcion()%></option>
                  <%}%>
            </select>
            <input name="reg" type="hidden" id="reg" value="<%=reg%>">
            <img src="<%=BASEURL%>/images/botones/iconos/obligatorio.gif" width="10" height="10">
            <input name="base" type="hidden" id="base" value="<%=base%>">
          </td>
          <td valign="middle"><%if(control.equals("S")){%>Reportado<%}%></td>
          <td valign="middle"><%if(control.equals("S")){%><select name="veto" >
            <option value="" <%=!veto.equals("S")&&!veto.equals("V")?"selected":""%>  >No</option>
            <option value="V" <%=veto.equals("S")||veto.equals("V")?"selected":""%>>Si</option>
          </select>
		  <%}else{%>
		     <input name="veto" type="hidden" id="veto" value="V">
		  <%}%>
          </td>
        </tr>
        <tr class="fila">
          <td width="141" align="left" > Documento </td>
          <td width="225" valign="middle">
              <input id="c_doc" name="c_doc" type="text" class="textbox" value="<%=doc%>"  maxlength="15" onchange="cargarDatosPersona(this.value);" onkeyup="soloNumeros(this.id)" onfocus="bloquear2();">
            <img src="<%=BASEURL%>/images/botones/iconos/obligatorio.gif" width="10" height="10"></td>
          <td width="151" valign="middle">Expedicion</td>
          <td width="201" valign="middle">
            <%TreeMap expe = model.ciudadService.getTreMapCiudades(); %>
            <input:select name="c_expe" options="<%=expe%>" attributesText="style='width:80% ' class='textbox'"/>
          </td>
        </tr>
        <!--
        <tr class="fila">
          <td align="left" >Id Mims</td>
          <td width="225" valign="middle"><input name="c_idmims" type="text" class="textbox" value="<%=idmims%>" maxlength="10" onKeyUp="soloNumeros(this.id)">            </td>
          <td width="151" valign="middle" >Libreta Militar </td>
          <td width="201" valign="middle"><input name="c_libreta" type="text" class="textbox" id="c_libreta" onKeyUp="soloNumeros(this.id)" value="<%=lib%>" maxlength="15">            </td>
        </tr>

        <tr class="fila" id="titnom" style="display:<%if (tipo[0]!='0' || tipo[1]!='0' || tipo[2]!='0'){%>block <%}else{%>none<%}%>">-->
        <tr class="fila">
            <td colspan="4">
                <div id="titnom" style="display:<%if (tipo[0]!='0' || tipo[1]!='0' || tipo[2]!='0'){%>block <%}else{%>none<%}%>">
                    <table border="0" width="100%">
                        <tr>
                            <td colspan="2" align="left" class="subtitulo1" width="50%">&nbsp;Datos Personales</td>
                            <td colspan="2" align="left" class="barratitulo" width="50%">
                                <img alt="" src="<%=BASEURL%>/images/titulo.gif" width="32" height="20">
                            </td>
                        </tr>
                    </table>
                </div>
            </td>

        </tr>
        <!-- 20100827 rhonalf -->

        <tr class="fila" >
            <td colspan="4">
              <div id="nombre">
                  <table border="0" width="100%" style="border-collapse: collapse;">
                      <tr>
                          <td align="left" class="fila" width="76px">Nombre</td>
                          <td colspan="3" valign="middle"><input name="c_nom" type="text" class="textbox" id="c_nom" value="<%=nom%>" size="40" maxlength="100" >
                              <img alt="" src="<%=BASEURL%>/images/botones/iconos/obligatorio.gif" width="10" height="10">

                              <input type="hidden" name="afil" value="<%=afil%>">
                          </td>
                      </tr>
                  </table>
              </div>
            </td>
        </tr>
        <tr class="fila">
            <td colspan="4">
                <div id="nombres" style="display:<%if (tipo[0]!='0' || tipo[1]!='0' || tipo[2]!='0'){%>block<%}else{%>none<%}%>;">
                    <table border="0" width="100%" style="border-collapse: collapse;">
                        <tr>
                            <td align="left" class="fila" width="76px">Nombres Primero </td>
                            <td valign="middle" width="302px">
                                <input id="c_nom1" name="c_nom1" type="text" class="textbox" value="<%=nom1%>" maxlength="15" onKeyUp="soloAlfa(this.id)">
                                <img alt="" src="<%=BASEURL%>/images/botones/iconos/obligatorio.gif" width="10" height="10">
                            </td>
                            <td valign="middle" class="fila" width="94px">Segundo </td>
                            <td valign="middle"><input id="c_nom2" name="c_nom2" type="text" class="textbox" value="<%=nom2%>" maxlength="15" onKeyUp="soloAlfa(this.id)"></td>
                        </tr>
                    </table>
                </div>
            </td>
        </tr>
        <tr class="fila">
            <td colspan="4">
                <div id="apellidos" style="display:<%if (tipo[0]!='0' || tipo[1]!='0' || tipo[2]!='0'){%>block <%}else{%>none<%}%>">
                    <table border="0" width="100%" style="border-collapse: collapse;">
                        <tr class="fila">
                            <td align="left" width="76px">Apellidos Primero</td>
                            <td valign="middle" width="302px"><input id="c_ape1" name="c_ape1" type="text" class="textbox" value="<%=ape1%>" maxlength="15" onKeyUp="soloAlfa(this.id)">
                                <img alt="" src="<%=BASEURL%>/images/botones/iconos/obligatorio.gif" width="10" height="10"></td>
                            <td valign="middle" width="94px">Segundo</td>
                            <td valign="middle"><input id="c_ape2" name="c_ape2" type="text" class="textbox" value="<%=ape2%>" maxlength="15" onKeyUp="soloAlfa(this.id)">
                                <img alt="" src="<%=BASEURL%>/images/botones/iconos/obligatorio.gif" width="10" height="10"></td>
                        </tr>
                    </table>
                </div>
            </td>
        </tr>
	<tr class="fila">
          <td align="left" >Nombre Nemotecnico</td>
          <td colspan="3" valign="middle">
              <input name="c_nomnemo" type="text" class="textbox" id="c_nomnemo" value="<%=nomnemo%>" maxlength="10">
          </td>
        </tr>
        <tr class="fila">
            <td colspan="4">
                <div id="gen" style="display:<%if (tipo[0]!='0' || tipo[1]!='0' || tipo[2]!='0'){%>block <%}else{%>none<%}%>">
                    <table border="0" width="100%" style="border-collapse: collapse;">
                        <tr class="fila">
                            <td align="left" width="76px">Genero</td>
                            <td valign="middle" width="302px">
                                <select name="c_genero" class="textbox" id="c_genero">
                                    <option value="M" <%if (genero.equals("M")){%> selected <%}%>>Masculino</option>
                                    <option value="F" <%if (genero.equals("F")){%> selected <%}%>>Femenino</option>
                                </select>
                            </td>
                            <td valign="middle" width="94px">Estado Civil</td>
                            <td valign="middle">
                                <select name="c_estcivil" class="textbox" id="c_estcivil">
                                    <option value="S" <%if (estcivil.equals("S")){%> selected <%}%>>Soltero</option>
                                    <option value="C" <%if (estcivil.equals("C")){%> selected <%}%>>Casado</option>
                                    <option value="V" <%if (estcivil.equals("V")){%> selected <%}%>>Viudo</option>
                                    <option value="E" <%if (estcivil.equals("E")){%> selected <%}%>>Separado</option>
                                    <option value="U" <%if (estcivil.equals("U")){%> selected <%}%>>Union Libre</option>
                                </select>
                            </td>
                        </tr>
                    </table>
                </div>
            </td>
        </tr>
        <tr class="fila">
            <td colspan="4">
                <div id="otros" style="display:<%if (tipo[0]!='0' || tipo[1]!='0' || tipo[2]!='0'){%>block <%}else{%>none<%}%>">
                    <table border="0" width="100%" style="border-collapse: collapse;">
                        <tr class="fila">
                            <td width="76px" align="left">Fecha de Nacimiento</td>
                            <td valign="middle" width="302px">
                                <input type="text" name="c_fecha" class="textbox" value="<%=fec%>" readonly >
                                <a href="javascript:void(0)" onclick="if(self.gfPop)gfPop.fPopCalendar(document.forma.c_fecha);return false;">
                                    <img src="<%=BASEURL%>/js/Calendario/cal.gif" width="16" height="16" border="0" alt="De click aqui para escoger la fecha">
                                </a>
                            </td>
                            <td valign="middle" width="94px">Lugar Nacimiento</td>
                            <td valign="middle"><%TreeMap lugar = model.ciudadService.getTreMapCiudades(); %>

                               <input:select name="c_lugar" options="<%=lugar%>" attributesText="style='width:80% ' class='textbox'"/>
                            </td>
                        </tr>
                    </table>
                </div>
            </td>
        </tr>
        <tr class="fila">
            <td colspan="4">
                <div id="senal" style="display:<%if (tipo[0]!='0' || tipo[1]!='0' || tipo[2]!='0'){%>block <%}else{%>none<%}%>">
                    <table border="0" width="100%" style="border-collapse: collapse;">
                        <tr class="fila">
                            <td align="left" width="76px">Se&ntilde;al Particular </td>
                            <td colspan="3" valign="middle">
                                <input name='c_senal' type='text' class='textbox' id="c_senal" value='<%= sena%>' size='50' maxlength='50' onKeyUp="soloAlfa(this.id)">
                            </td>
                        </tr>
                    </table>
                </div>
            </td>
        </tr>
        <tr class="fila" >
          <td width="141"align="left" >Direccion</td>
          <td valign="middle"><input name="c_dir" type="text" class="textbox" id="c_dir2" value="<%=dir%>" size="40" maxlength="40" style='max-width:85%;'> 
              <img src="/fintra/images/Direcciones.png" width="26" height="23" border="0" align="middle" onclick="genDireccion('c_dir',event);" alt="Direcciones"  title="Direcciones" /></td>             
         <td valign="middle">Barrio
          <td valign="middle"><input name="c_barrio" type="text" class="textbox" id="c_barrio" value="<%=barrio%>" maxlength="25" onKeyUp="soloAlfa(this.id)">
        </tr>
        <tr class="fila">
          <td width="141" align="left" >Pais</td>
          <td valign="middle">
            <select name="pais" id="pais" class="textbox" onchange="cargarEstados(this.selectedIndex);">
              <option value="">Seleccione</option>
              <%
                  List paises = model.paisservice.obtenerpaises();
		  Iterator it = paises.iterator();
		  while (it.hasNext()){
                    Pais pais = (Pais) it.next();%>
              <option value="<%=pais.getCountry_code()%>" <%=(pais.getCountry_code().equals(cpais))?"selected":""%> ><%=pais.getCountry_name()%></option>
              <%}%>
            </select>
            <td valign="middle">Estado
            <td valign="middle">
            <div id="est_1">
                <select id="est" name="est" class="textbox" onchange="cargarCiudades(this.selectedIndex);">
		<%if(cpais.equals("")){%>
               <option value="" selected>Seleccione</option>
               <%}else{
                        List estados = model.estadoservice.obtenerEstados();
                        if(estados!=null){
                        if (estados.size() > 0){
                           Iterator it1 = estados.iterator();
                %>
                <option value="" >Seleccione</option>
                <%
                           while (it1.hasNext()){
                                Estado estado = (Estado) it1.next();
                %>
                <option value="<%=estado.getdepartament_code()%>" <%=(estado.getdepartament_code().equals(cest))?"selected":""%> ><%=estado.getdepartament_name()%></option>
                <%      }
                        }else {%>
                <option value="" selected>No registra datos</option>
                <%
                        }
                        }
                   }
                %>
            </select>
            </div>
        </tr>
        <tr class="fila">
          <td width="141"align="left" >Ciudad</td>
          <td valign="middle" colspan="3">
            <div id="ciudad_1">
            <select id="ciudad" name="ciudad"  class="textbox">
			<%if(cpais.equals("") && cest.equals("")){%>
              	<option value="" selected>Seleccione</option>
            <%}else{
	           		  List ciudades = model.ciudadservice.obtenerCiudades();
                                  if(ciudades!=null){
  				      if(ciudades.size() > 0){
            		      Iterator it2 = ciudades.iterator();%>
					       <option value="" >Seleccione</option>
                          <%while (it2.hasNext()){
			      		        Ciudad ciu = (Ciudad) it2.next();%>
              					<option value="<%=ciu.getcodciu()%>" <%=(ciu.getcodciu().equals(cciu))?"selected":""%>><%=ciu.getnomciu()%></option>
					      <%}
				    }

                    else {%>
              		    <option value="" selected>No registra datos</option>
                  <%} }
				}%>
          </select>
          </div>
        </tr>
        <tr class="fila">
          <td width="141" rowspan="2" align="left" >Telefonos</td>
          <td valign="middle" class="subtitulos"><table width="62%"  border="0" cellpadding="0" cellspacing="0">
              <tr class="fila">
                <td width="18%">Pais</td>
                <td width="46%"><div align="left">&nbsp;&nbsp;&nbsp;&nbsp;Area</div></td>
                <td width="36%">Numero</td>
              </tr>
          </table></td>
          <td colspan="2" valign="middle" class="subtitulos"><table width="50%"  border="0" cellpadding="0" cellspacing="0">
              <tr class="fila">
                <td width="17%">Pais</td>
                <td width="36%"><div align="left">&nbsp;&nbsp;&nbsp;&nbsp;Area</div></td>
                <td width="47%">Numero</td>
              </tr>
          </table></td>
        </tr>
        <tr class="fila">
          <td valign="middle">
              <input type="hidden" id="paiscode" value="<%=cpais%>"><% /*20100827 rhonalf*/ %>
              <input name="c_tel11" type="text" class="textbox" id="c_tel112" onKeyUp="soloNumeros(this.id)"  value="<%=tel11%>" size="2" maxlength="2" readonly>
              <input name="c_tel12" type="text" class="textbox" id="c_tel122" onKeyUp="soloNumeros(this.id)" value="<%=tel12%>" size="3" maxlength="3" readonly>
              <input name="c_tel13" type="text" class="textbox" id="c_tel132" onKeyUp="soloNumeros(this.id)" value="<%=tel13%>" maxlength="15" readonly>
              <img src="<%=BASEURL%>/images/botones/iconos/notas.gif" width="15" height="15" onClick="modTelefono( '<%=BASEURL%>' ,1 )"></td>
          <td colspan="2" valign="middle">
              <input name="c_tel21" type="text" class="textbox" id="c_tel21" onKeyUp="soloNumeros(this.id)" value="<%=tel21%>" size="2" maxlength="2" readonly>
              <input name="c_tel22" type="text" class="textbox" id="c_tel222" onKeyUp="soloNumeros(this.id)" value="<%=tel22%>" size="3" maxlength="3" readonly>
              <input name="c_tel23" type="text" class="textbox" id="c_tel232" onKeyUp="soloNumeros(this.id)" value="<%=tel23%>" maxlength="15" readonly>
              <img src="<%=BASEURL%>/images/botones/iconos/notas.gif" width="15" height="15" onClick="modTelefono( '<%=BASEURL%>' ,2 )">
          </td>
        </tr>
        <tr class="fila">
          <td width="141" align="left" >Celular</td>
          <td valign="middle">
            <input name="c_cel" type="text" class="textbox" id="c_cel2" onKeyUp="soloNumeros(this.id)" value="<%=cel%>" maxlength="15">            </td>
          <td valign="middle">Email</td>
          <td valign="middle"><input name="c_email" type="text" class="textbox" id="c_email" value="<%=em%>" size="25" maxlength="50">            </td>
        </tr>


        <tr class="fila">
          <td width="141" align="left" valign="top" >Referencia</td>
          <td valign="top"><input name="c_ref" type="text" class="textbox" id="c_ref2" value="<%=ref%>" size="40" maxlength="30" onKeyUp="soloAlfa(this.id)">            </td>
          <td valign="middle">Cargo
          <td valign="middle"><input name="c_cargo" type="text" class="textbox" id="c_cargo" value="<%=cargo%>" size="40" maxlength="30"></td>
        </tr>
        <tr class="fila">
          <td valign="top">Observaci&oacute;n</td>
          <td valign="middle" colspan="3"><textarea rows="3" name="c_obse" cols="96" class="textbox" id="textarea"><%=obs%></textarea></td>
        </tr>
      </table>	  </td>
    </tr>
  </table>
  <br>
    <div align="center">
	   <img src="<%=BASEURL%>/images/botones/aceptar.gif"  name="imgaceptar" onClick="validarIdentidad('<%=CONTROLLER%>','<%=BASEURL%>');" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" style="cursor:hand">&nbsp;
	   <img src="<%=BASEURL%>/images/botones/cancelar.gif"  name="imgcancelar"  onMouseOver="botonOver(this);" onClick="window.location='<%=CONTROLLER%>?estado=Menu&accion=Cargar&pagina=identidad.jsp&carpeta=/jsp/hvida/identidad&marco=no'" onMouseOut="botonOut(this);" style="cursor:hand"> &nbsp;
       <img src="<%=BASEURL%>/images/botones/salir.gif"  name="imgsalir"  onMouseOver="botonOver(this);" onClick="parent.close();" onMouseOut="botonOut(this);" style="cursor:hand">
   </div>



    <%if(!res.equals("")){%>

  <table border="2" align="center">
  <tr>
    <td><table width="100%"  border="1" align="center"  bordercolor="#F7F5F4" bgcolor="#FFFFFF">
      <tr>
        <td width="229" align="center" class="mensajes"><%=res%></td>
        <td width="29" background="<%=BASEURL%>/images/cuadronaranja.JPG">&nbsp;</td>
        <td width="58">&nbsp;</td>
      </tr>
    </table></td>
  </tr>
</table>
 <%}%>
 <br>
</form>
</div>
<iframe width=188 height=166 name="gToday:normal:agenda.js:gfPop:plugins.js" id="gToday:normal:agenda.js:gfPop:plugins.js" src="<%=BASEURL%>/js/Calendario/ipopeng.htm" scrolling="no" frameborder="0" style="visibility:visible; z-index:999; position:absolute; left:-500px; top:0px;"> </iframe>
<div id="direccion_dialogo" style="display:none;left: 0; position: absolute; top: 0;padding: 1em 1.2em; margin:0px auto; margin-top:10px; padding:10px; width:370px; min-height:140px; border-radius:4px; background-color:#FFFFFF; box-shadow: 0 2px 5px #666666;"> <!-- class="content-popup" style="padding: 1em 1.2em; width: 370px;"-->
    <div>
        <table style="width: 100%;">

            <tr>
                <td class="titulo_ventana" id="drag_direcciones" colspan="3">
                    <div style="float:center">FORMATO DIRECCIONES<input type="hidden" name="id_usuario" id="id_usuario" value="coco"/></div> 
                </td>
            </tr>
            <tr>  
                <td><span>Departamento:</span></td>                     
                <td colspan="2">
                    <select type="text" id="dpto" name="dpto" style="width:20em;" onchange="ciudad(this.value)"></select>
                </td>
            </tr>
            <tr>
                <td><span>Ciudad:</span></td>     
                <td colspan="2">                            
                    <div id="d_ciu_dir">
                        <select type="text" id="ciu_dir" name="ciu_dir" style="width:20em;" onchange="cargarVias(this.value)"></select>
                    </div>
                </td>
            </tr> 
            <tr>
                        <td>Via Principal</td>
                        <td>
                            <select id="via_princip_dir" onchange="setDireccion(2)">                                
                            </select>
                        </td>
                        <td><input type="text" id="nom_princip_dir" style="width: 87%;" onchange="setDireccion(1)"/></td>
                    </tr>
                    <tr>
                        <!-- <td>Via Generadora</td> -->
                        <td>Numero</td>
                        <td>
                            <select id="via_genera_dir" onchange="setDireccion(1)">                               
                            </select>
                        </td>
                        
                        <td>
                            <table width="100%" border="0">
                                <tr>
                                    <td align="center" width="49%">
                                        <input type="text" id="nom_genera_dir" style="width: 50px;" onchange="setDireccion(1)"/>
                                    </td>
                                    <td width="2%" align="center"> - </td>
                                    <td align="center" width="49%">
                                        <input type="text" id="placa_dir" style="width: 50px;" onchange="setDireccion(1)"/>
                                    </td>    
                                </tr>
                            </table>                        
                        </td>
                    </tr>
                    <!--
                    <tr>
                        <td>Placa</td>
                        <td colspan="2"><input type="text" id="placa_dir" style="width: 100%;" onchange="setDireccion(1)"/></td>
                    </tr>-->
                    <tr>
                        <td>Complemento</td>
                        <td colspan="2"><input type="text" id="cmpl_dir" style="width: 92%;" onchange="setDireccion(1)"/></td>
                    </tr>
                    <tr>
                        <td colspan="3"><input type="text" id="dir_resul" name="dir_resul" style="width: 95%;" readonly/></td>
                    </tr>
                    <tr>
                        <td colspan="3">
                            <button onclick="setDireccion(3);">Aceptar</button>
                            <button onclick="setDireccion(0);">Salir</button>
                        </td>
                    </tr>
        </table>
    </div>
</div>
</body>
</html>


