<!--
- Autor : Ing. Henry Osorio
- Date  : 26 de septiembre de 2005
- Copyrigth Notice : Fintravalores S.A. S.A
-->
<%--
-@(#)
--Descripcion : Pagina JSP, que maneja la busqueda de las referecias
--%>

<%-- Declaracion de librerias--%>

<%@page contentType="text/html"%>
<%@page session="true"%>
<%@page import="java.util.*"%>
<%@page import="com.tsp.operation.model.*"%>
<%@page import="com.tsp.operation.model.beans.*"%>
<%@page import="com.tsp.util.*"%>
<%@include file="/WEB-INF/InitModel.jsp"%><html>
<head>
<title>Verificar Referencias</title>
<script type='text/javascript' src="<%= BASEURL %>/js/boton.js"></script>
<script type='text/javascript' src="<%= BASEURL %>/js/general.js"></script>
<link href="<%= BASEURL %>/css/estilostsp.css" rel='stylesheet'>
<link href="../../../css/estilostsp.css" rel="stylesheet" type="text/css">
                    
</head>
<%String tipo = request.getParameter("tipo");
String titulo = (tipo.equals("C"))?"Verificar Referencia Identidad":"Verificar Referencia Placa";
String pagina = "/toptsp.jsp?encabezado="+titulo;%>
<body onLoad="redimensionar();doFieldFocus( forma );" onresize="redimensionar()">
<div id="capaSuperior" style="position:absolute; width:100%; height:100px; z-index:0; left: 0px; top: 0px;">
	<jsp:include page="<%=pagina%>"/>
</div>
<div id="capaCentral" style="position:absolute; width:100%; height:87%; z-index:0; left: 0px; top: 100px; overflow: scroll;">
<%String res="";
String msg = (request.getParameter("men")!=null)?request.getParameter("men"):"" ;
%>
<form name='forma' method='POST' action="<%=CONTROLLER%>?estado=Referencia&accion=Buscar&pagina=referencias.jsp&carpeta=/jsp/hvida/referencias"  >
<table width="400" border="2" align="center">
    <tr>
      <td><table width="100%" class="tablaInferior">
        <tr>
          <td width="171" class="subtitulo1">&nbsp;Buscar Referencia</td>
          <td width="205" class="barratitulo"><img src="<%=BASEURL%>/images/titulo.gif" width="32" height="20"></td>
        </tr>
        <tr class="fila">
          <td>Documento
            <input name="tipo" type="hidden" id="tipo" value="<%=tipo%>">            </td>
          <td><input  name='documento' <%if(tipo.equals("C")){%>onKeyPress="soloDigitos(event,'decNO');"<%}%> onKeyUp="if(event.keyCode== 13 && forma.documento.value!=''){forma.submit();}"   class='textbox' id="documento" size='15' maxlength='15' >
              <img src="<%=BASEURL%>/images/botones/iconos/lupa.gif" width="20" height="20" onClick="if(forma.documento.value!=''){forma.submit();}else{alert('Verifique el campo tenga Datos')}" style="cursor:hand "></td>
          </tr>
      </table></td>
    </tr>
  </table>
</form>
<div align="center">    <img src="<%=BASEURL%>/images/botones/salir.gif"  name="imgsalir"  onMouseOver="botonOver(this);" onClick="parent.close();" onMouseOut="botonOut(this);" style="cursor:hand"> </div>
</div>
</body>
</html>

