<!--
- Autor : Ing. Henry Osorio
- Date  : 26 de septiembre de 2005
- Copyrigth Notice : Fintravalores S.A. S.A
-->
<%--
-@(#)
--Descripcion : Pagina JSP, que maneja el formulario de ingreso de las referecias
--%>

<%-- Declaracion de librerias--%>
<%@page contentType="text/html"%>
<%@page pageEncoding="UTF-8"%>
<%@page errorPage="/error/ErrorPage.jsp"%>
<%@page import="com.tsp.operation.model.beans.*, java.util.*"%>
<%@include file="/WEB-INF/InitModel.jsp" %>
<%@ taglib uri="http://jakarta.apache.org/taglibs/input-0.90" prefix="input" %>
<%
  String path = application.getRealPath(model.menuService.getRealUrl(request.getRequestURI()));
  String datos[] = model.menuService.getContenidoMenu(BASEURL,request.getRequestURI(),path);
  String ref = (request.getParameter("ref")!=null)?request.getParameter("ref"):"";
  String doc = (request.getParameter("doc")!=null)?request.getParameter("doc"):"";
  String tipo = (request.getParameter("tipo")!=null)?request.getParameter("tipo"):"";
  String msg = (request.getParameter("msg")!=null)?request.getParameter("msg"):"";
  String recargar = request.getParameter("recargar");
  String val="";
  if(tipo.equals("EA")) 
      val = "Placa";
  else if(tipo.equals("CO") || tipo.equals("PR") || tipo.equals("EC"))
      val = "Cedula";  
  TreeMap ciudades = null;
%>
<html>
<head>
<title>Agregar Referencias</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link href="../../../css/estilostsp.css" rel="stylesheet" type="text/css"> 
<link href="<%=BASEURL%>/css/estilostsp.css" rel="stylesheet" type="text/css"> 
<script type='text/javascript' src="<%=BASEURL%>/js/general.js"></script> 
<script type='text/javascript' src="<%=BASEURL%>/js/referencia.js"></script> 
<script type='text/javascript' src="<%=BASEURL%>/js/boton.js"></script> 
</head>
<body onLoad="<%=(msg.equals("Referencia agregada exitosamente") && recargar.equals("ok"))?"parent.opener.location='"+CONTROLLER+"?estado=Referencia&accion=Search&tipo="+tipo+"&documento="+doc+"';doFieldFocus( frmref );window.close();":"doFieldFocus( frmref );"%>"> 
<form name="frmref" action="<%=CONTROLLER%>?estado=Referencia&accion=Insert&cmd=show" method="post" >    
<table border="2" align="center" width="800">
  <tr>
    <td>
<table width="100%" align="center">
  <tr>
    <td width="392" height="24"  class="subtitulo1"><p align="left"> Informaci&oacute;n de la referencia </p></td>
    <td width="392"  class="barratitulo"><img src="<%=BASEURL%>/images/titulo.gif" width="32" height="20" align="left"><%=datos[0]%></td>
  </tr>
</table>
<table width="100%" height="36"  align="center">
  <tr >
    <td width="136" height="30" class="fila">Tipo de documento </td>
    <td width="117" class="letra"><%=val%><input name="empresa" type="hidden" class="textbox" id="empresa" value="<%=val%>" size="15" readonly> 
      <input name="tiporeferencia" type="hidden" value="<%=tipo%>"> 
      <input name="recargar" type="hidden" id="recargar" value="<%=recargar%>">   
    </td>
    <td width="100" class="fila" >No. documento </td>
    <td width="222" class="letra"><%=doc%><input name="documento" type="hidden" class="textbox" id="docu" value="<%=doc%>" size="15"> 
      <input name="ref" type="hidden" id="ref" value="<%=ref%>">
    </td>
    </tr>
</table>
</td>
</tr>
</table>  
<%if(tipo.equals("EA")){%>
  <table border="2" align="center" width="800">
  <tr>
    <td >
<table width="100%"  align="center"   >
  <tr>
    <td width="392" height="24"  class="subtitulo1"><p align="left">
      Empresa Afiliadora </p></td>
    <td width="392"  class="barratitulo"><img src="<%=BASEURL%>/images/titulo.gif" width="32" height="20"></td>
  </tr>
</table>
<table width="100%"  align="center" >
  <tr class="fila">
    <td width="77" height="30">Nombre empresa </td>
    <td width="272"><input name="nombre" type="text" class="textbox" id="nombre" size="40" maxlength="30" onKeyPress="soloAlfa(event)"> 
    <img src="<%=BASEURL%>/images/botones/iconos/obligatorio.gif" width="10" height="10">    </td>
    <td width="63" >Telefono</td>
    <td width="125"><input name="telefono" type="text" class="textbox" id="telefono" size="15" maxlength="15" onKeyPress="soloDigitos(event,'decNO')"> 
    <img src="<%=BASEURL%>/images/botones/iconos/obligatorio.gif" width="10" height="10"></td>
    <td width="50">Ciudad </td>
    <td width="173"><%ciudades = model.ciudadService.getTreMapCiudades(); %> 	  <input:select name="telciu" options="<%=ciudades%>" attributesText="style='width:90%;' class='listmenu'" /> <img src="<%=BASEURL%>/images/botones/iconos/obligatorio.gif" width="10" height="10">
    </td>
  </tr>
  <tr class="fila">
    <td width="77" height="30">Contacto</td>
    <td width="272" ><input name="contacto" type="text" class="textbox" id="contacto" size="40" maxlength="30" onKeyPress="soloAlfa(event)">
      <img src="<%=BASEURL%>/images/botones/iconos/obligatorio.gif" width="10" height="10"> </td>
    <td width="63" >Cargo</td>
    <td colspan="3"><input name="cargo" type="text" class="textbox" id="cargo" size="30" maxlength="25" onKeyPress="soloAlfa(event)">
 <input name="frm" type="hidden" id="frm" value="1"> <img src="<%=BASEURL%>/images/botones/iconos/obligatorio.gif" width="10" height="10">     <span class="textbox">
    </span></td>
    </tr>
  <tr class="fila">
          <td height="30">Referencia</td>
          <td colspan="5"><textarea name="referencia" cols="130" class="textbox" id="referencia"></textarea>
            <img src="<%=BASEURL%>/images/botones/iconos/obligatorio.gif" width="10" height="10"></td>
          </tr>
</table>
</td>
</tr>
</table>
<%}%>
<%if(tipo.equals("CO")){%>
<table border="2" align="center" width="800">
  <tr>
    <td width="818">

<table width="100%"  align="center"   >
  <tr>
    <td width="392" height="24"  class="subtitulo1"><p align="left">
      Referencia Personal del Conductor </p></td>
    <td width="392"  class="barratitulo"><img src="<%=BASEURL%>/images/titulo.gif" width="32" height="20"></td>
  </tr>
</table>
<table width="100%"  align="center" >
  <tr class="fila">
    <td width="83" height="30">Nombre</td>
    <td width="282" ><input name="nombre" type="text" class="textbox" id="nombre" size="40" maxlength="30" onKeyPress="soloAlfa(event)">
      <img src="<%=BASEURL%>/images/botones/iconos/obligatorio.gif" width="10" height="10">    </td>
    <td width="56" >Telefono</td>
    <td width="129"><input name="telefono" type="text" class="textbox" id="telefono" onKeyPress="soloDigitos(event,'decNO')"  size="15" maxlength="15" >      
    <img src="<%=BASEURL%>/images/botones/iconos/obligatorio.gif" width="10" height="10"> </td>
    <td width="50">Ciudad </td>
    <td width="160"><%ciudades = model.ciudadService.getTreMapCiudades(); %>
       <input:select name="telciu" options="<%=ciudades%>" attributesText="style='width:80%;' class='listmenu'" /> <img src="<%=BASEURL%>/images/botones/iconos/obligatorio.gif" width="10" height="10">
</td>
  </tr>
  <tr class="fila">
    <td width="83" height="21">Direccion</td>
    <td width="282" ><input name="direccion" type="text" class="textbox" id="direccion" size="40" maxlength="30">          </td>
    <td width="56" >Ciudad</td>
    <td colspan="4"> <%ciudades = model.ciudadService.getCiudades(); %>
      <input:select name="ciudaddir" options="<%=ciudades%>" attributesText="style='width:60%;' class='listmenu'" /> <img src="<%=BASEURL%>/images/botones/iconos/obligatorio.gif" width="10" height="10">
</td>
    </tr>
	<tr class="fila">
    <td width="83" height="22">Relacion</td>
    <td ><select name="relacion" class="textbox" id="relacion" onChange="Otros();" style='width:80%;'>
      <option value="">Seleccione Un Item</option>
      <% LinkedList tblres = model.tablaGenService.obtenerTablas();
               for(int i = 0; i<tblres.size(); i++){
                       TablaGen respa = (TablaGen) tblres.get(i); %>
      <option value="<%=respa.getTable_code()%>"  ><%=respa.getDescripcion()%></option>
      <%}%>
    </select>
      <img src="<%=BASEURL%>/images/botones/iconos/obligatorio.gif" width="10" height="10">    </td>
    <td >Otros</td>
    <td colspan="4"><input name="otra_rel" type="text" id="otra_rel" style="visibility:hidden" size="30" maxlength="30" onKeyPress="soloAlfa(event)"> 
    <img name="img_otro" id="img_otro" src="<%=BASEURL%>/images/botones/iconos/obligatorio.gif" width="10" height="10" style="visibility:hidden">
      <input name="frm" type="hidden" id="frm" value="2"></td></tr>
	<tr class="fila">
	  <td height="22">Referencia</td>
	  <td colspan="6" ><textarea name="referencia" cols="130" class="textbox" id="referencia"></textarea>
	    <img src="<%=BASEURL%>/images/botones/iconos/obligatorio.gif" width="10" height="10"></td>
	  </tr>
</table>
</td>
</tr>
</table>
<table border="2" align="center" width="800">
  <tr>
    <td>
      <table width="100%"  align="center"   >
        <tr>
          <td width="392" height="24"  class="subtitulo1"><p align="left"> Empresa Donde Ha Cargado </p></td>
          <td width="392"  class="barratitulo"><img src="<%=BASEURL%>/images/titulo.gif" width="32" height="20"></td>
        </tr>
      </table>
      <table width="100%"  align="center" >
        <tr class="fila">
          <td width="77" height="21">Nombre</td>
          <td width="271"><input name="nombreEC" type="text" class="textbox" id="nombreEC" size="40" maxlength="30" onKeyPress="soloAlfa(event)">
            <img src="<%=BASEURL%>/images/botones/iconos/obligatorio.gif" width="10" height="10">          </td>
          <td width="71" >Telefono</td>
          <td width="129"><input name="telefonoEC" type="text" class="textbox" id="telefonoEC" size="15" maxlength="15" onKeyPress="soloDigitos(event,'decNO')" > 
          <img src="<%=BASEURL%>/images/botones/iconos/obligatorio.gif" width="10" height="10">
          </td>
          <td width="46">Ciudad </td>
          <td width="166"><%ciudades = model.ciudadService.getTreMapCiudades(); %>
             <input:select name="telciuEC" options="<%=ciudades%>" attributesText="style='width:80%;' class='listmenu'" /> <img src="<%=BASEURL%>/images/botones/iconos/obligatorio.gif" width="10" height="10"> </td>
        </tr>
        <tr class="fila">
          <td width="77" height="21">Direcci&oacute;n</td>
          <td width="271" ><input name="direccionEC" type="text" class="textbox" id="direccionEC" size="40" maxlength="30">            </td>
          <td width="71" >Ciudad</td>
          <td colspan="3"><%ciudades = model.ciudadService.getCiudades(); %>
            <input:select name="ciudaddirEC" options="<%=ciudades%>" attributesText="style='width:60%;' class='listmenu'" /> <img src="<%=BASEURL%>/images/botones/iconos/obligatorio.gif" width="10" height="10"> </td>
        </tr>
        <tr class="fila">
          <td width="77" height="30">Contacto</td>
          <td width="271"><input name="contactoEC" type="text" class="textbox" id="contactoEC" size="30" maxlength="30" onKeyPress="soloAlfa(event)">
            <img src="<%=BASEURL%>/images/botones/iconos/obligatorio.gif" width="10" height="10">          </td>
          <td width="71" >Cargo</td>
          <td colspan="3">
            <input name="cargoEC" type="text" class="textbox" id="cargoC" size="30" maxlength="25" onKeyPress="soloAlfa(event)">
            <input type="hidden" name="emp" value=""> 
            <img src="<%=BASEURL%>/images/botones/iconos/obligatorio.gif" width="10" height="10">          </td>
        </tr>
        <tr class="fila">
          <td height="30">Referencia</td>
          <td colspan="5"><textarea name="referenciaEC" cols="130%" class="textbox" id="referenciaEC"></textarea>
            <img src="<%=BASEURL%>/images/botones/iconos/obligatorio.gif" width="10" height="10"></td>
          </tr>
    </table></td>
  </tr>
</table>
<%}%>
<%if(tipo.equals("PR")){%>
<table border="2" align="center" width="800">
  <tr>
    <td>
      <table width="100%"  align="center"   >
        <tr>
          <td width="392" height="24"  class="subtitulo1"><p align="left"> Referencia personal del propietario </p></td>
          <td width="392"  class="barratitulo"><img src="<%=BASEURL%>/images/titulo.gif" width="32" height="20"></td>
        </tr>
      </table>
      <table width="100%"  align="center" >
        <tr class="fila">
          <td width="77" height="21">Nombre</td>
          <td width="273" ><input name="nombre" type="text" class="textbox" id="nombre" size="40" maxlength="30" onKeyPress="soloAlfa(event)">
            <img src="<%=BASEURL%>/images/botones/iconos/obligatorio.gif" width="10" height="10">            </td>
          <td width="66" >Telefono</td>
          <td width="133"><input name="telefono" type="text" class="textbox" id="telefono" size="15" maxlength="15" onKeyPress="soloDigitos(event,'decNO')">
            <img src="<%=BASEURL%>/images/botones/iconos/obligatorio.gif" width="10" height="10">          </td>
          <td width="46">Ciudad </td>
          <td width="165" colspan="2"><%ciudades = model.ciudadService.getTreMapCiudades(); %>
             <input:select name="telciu" options="<%=ciudades%>" attributesText="style='width:80%;' class='listmenu'" /> <img src="<%=BASEURL%>/images/botones/iconos/obligatorio.gif" width="10" height="10">
		</td>
        </tr>
        <tr class="fila">
          <td width="77" height="21">Direccion</td>
          <td width="273" ><input name="direccion" type="text" class="textbox" id="direccion" size="40" maxlength="30">            </td>
          <td width="66" >Ciudad</td>
          <td colspan="4"><%ciudades = model.ciudadService.getCiudades(); %> 
		  	  <input:select name="ciudaddir" options="<%=ciudades%>" attributesText="style='width:60%;' class='listmenu'" /> <img src="<%=BASEURL%>/images/botones/iconos/obligatorio.gif" width="10" height="10">
		</td>
        </tr>
        <tr class="fila">
    <td width="77" height="22">Relacion</td>
    <td ><select name="relacion" class="textbox" id="relacion" onChange="Otros();" style='width:80%;'>
      <option value="">Seleccione Un Item</option>
      <% LinkedList tblres = model.tablaGenService.obtenerTablas();
               for(int i = 0; i<tblres.size(); i++){
                       TablaGen respa = (TablaGen) tblres.get(i); %>
      <option value="<%=respa.getTable_code()%>"  ><%=respa.getDescripcion()%></option>
      <%}%>
    </select>
      <img src="<%=BASEURL%>/images/botones/iconos/obligatorio.gif" width="10" height="10">    </td>
    <td >Otros</td>
    <td colspan="4" ><input name="otra_rel" type="text" id="otra_rel" style="visibility:hidden" size="30" maxlength="30" onKeyPress="soloAlfa(event)"> 
    <img name="img_otro" id="img_otro" src="<%=BASEURL%>/images/botones/iconos/obligatorio.gif" width="10" height="10" style="visibility:hidden" >
      <input name="frm" type="hidden" id="frm" value="3">      </td>
        </tr>
        <tr class="fila">
          <td height="22">Referencia</td>
          <td colspan="6" ><textarea name="referencia" cols="130" class="textbox" id="referencia"></textarea>
            <img src="<%=BASEURL%>/images/botones/iconos/obligatorio.gif" width="10" height="10"></td>
          </tr>
    </table></td>
  </tr>
</table>
<%}%>
<br>
<table width="595" border="0" align="center">
  <tr>
    <td align="center">         
      <img src="<%=BASEURL%>/images/botones/aceptar.gif" name="mod" height="21" onClick="return validarReferencia()" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" style="cursor:hand">&nbsp;
      <img src="<%=BASEURL%>/images/botones/cancelar.gif" name="mod" height="21" onClick="frmref.reset();" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" style="cursor:hand">&nbsp;
      <img src="<%=BASEURL%>/images/botones/regresar.gif"  height="21" name="imgsalir"  onMouseOver="botonOver(this);" onClick="parent.close();" onMouseOut="botonOut(this);" style="cursor:hand">      </td>
    </tr>
</table>
<%if(!msg.equals("")){%>
	<p>
   <table border="2" align="center">
     <tr>
    <td><table width="100%"   align="center" bordercolor="#F7F5F4" bgcolor="#FFFFFF"  >
      <tr>
        <td width="229" align="center" class="mensajes"><%=msg%></td>
        <td width="29" background="<%=BASEURL%>/images/cuadronaranja.JPG">&nbsp;</td>
        <td width="58" bgcolor="#FFFFFF">&nbsp;</td>
      </tr>
    </table></td>
  </tr>
</table>
  </p>
  <%}%>

</form>
<iframe width=188 height=166 name="gToday:datetime:<%=BASEURL%>/js/calendartsp/agenda.js:gfPop:<%=BASEURL%>/js/calendartsp/plugins.js" id="gToday:datetime:<%=BASEURL%>/js/calendartsp/agenda.js:gfPop:<%=BASEURL%>/js/calendartsp/plugins.js" src="<%=BASEURL%>/js/calendartsp/ipopeng.htm" scrolling="no" frameborder="0" style="visibility:visible; z-index:999; position:absolute; left:-500px; top:0px;">
</iframe>
<%=datos[1]%>
</html>