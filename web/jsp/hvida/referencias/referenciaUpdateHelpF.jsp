<%@ include file="/WEB-INF/InitModel.jsp"%>

<HTML>
<HEAD>
<TITLE></TITLE>
<META http-equiv=Content-Type content="text/html; charset=windows-1252">
<link href="../../../css/estilostsp.css" rel="stylesheet" type="text/css"> 
<link href="<%=BASEURL%>/css/estilostsp.css" rel="stylesheet" type="text/css"> 
<style type="text/css">
<!--
.Estilo2 {
	font-size: 16pt;
	color: #FF0000;
}
-->
</style>
</HEAD>
<BODY> 

        <table width="90%" border="0" align="center">
          <tr  class="subtitulo">
            <td height="20"><div align="center">MANUAL DE


 REFERENCIA</div></td>
          </tr>
          <tr class="subtitulo1">
            <td> Modificaci&oacute;n de Referencia  del veh&iacute;culo </td>
          </tr>
          <tr>
            <td  height="8"  class="ayudaHtmlTexto"><p> El sistema muestra el formulario con la informaci&oacute;n almacenada de la referencia, para poder ser modificada. </p></td>
          </tr>
          <tr>
            <td height="18"  class="ayudaHtmlTexto" align="center"><img src="../../../images/ayuda/hvida/referencia/ref2.JPG" width="828" height="289"></td>
          </tr>
          <tr>
            <td height="18"  class="ayudaHtmlTexto" align="center"><p>Luego presione bot&oacute;n modificar para almacenar los respectivos cambios. </p>
            </td>
          </tr>
          <tr>
            <td height="18"  class="ayudaHtmlTexto" align="center">&nbsp;</td>
          </tr>
          <tr>
            <td height="18" class="subtitulo1">Modificaci&oacute;n de Referencia del Conductor</td>
          </tr>
          <tr>
            <td height="18" class="ayudaHtmlTexto">El sistema muestra el formulario con la informaci&oacute;n almacenada de la referencia, para poder ser modificada. </td>
          </tr>
          <tr>
            <td height="18" class="ayudaHtmlTexto"><div align="center"><img src="<%=BASEURL%>/images/ayuda/hvida/referencia/refCon2.JPG" ></div></td>
          </tr>
          <tr>
            <td height="18" class="ayudaHtmlTexto">Luego presione bot&oacute;n modificar para almacenar los respectivos cambios. </td>
          </tr>
          <tr>
            <td height="18" class="ayudaHtmlTexto">&nbsp;</td>
          </tr>
          <tr>
            <td height="18" class="subtitulo1">Modificaci&oacute;n de Referencia del Propietario </td>
          </tr>
          <tr>
            <td height="18" class="ayudaHtmlTexto">El sistema muestra el formulario con la informaci&oacute;n almacenada de la referencia, para poder ser modificada. </td>
          </tr>
          <tr>
            <td height="18" class="ayudaHtmlTexto"><div align="center"><img src="<%=BASEURL%>/images/ayuda/hvida/referencia/refProp2.JPG"></div></td>
          </tr>
          <tr>
            <td height="18" class="ayudaHtmlTexto">Luego presione bot&oacute;n modificar para almacenar los respectivos cambios. </td>
          </tr>
          <tr>
            <td height="18" class="ayudaHtmlTexto">&nbsp;</td>
          </tr>
      </table>
</BODY>
</HTML>
