<!--
- Autor : Ing. Diogenes Bastidas
- Date  : 09 de marzo de 2006
- Copyrigth Notice : Fintravalores S.A. S.A
-->
<%--
-@(#)
--Descripcion : Pagina JSP, maneja el formulario de verifcacion de referencia
--%>

<%-- Declaracion de librerias--%>
<%@page contentType="text/html"%>
<%@page session="true"%>
<%@page import="java.util.*"%>
<%@page import="com.tsp.operation.model.*"%>
<%@page import="com.tsp.operation.model.beans.*"%>
<%@page import="com.tsp.util.*"%>
<%@page import="com.tsp.util.Util"%>
<%@include file="/WEB-INF/InitModel.jsp"%><html>
<head>
<title>Verificar Referencias</title>
<script type='text/javascript' src="<%= BASEURL %>/js/boton.js"></script>
<script type='text/javascript' src="<%= BASEURL %>/js/general.js"></script>
<link href="<%= BASEURL %>/css/estilostsp.css" rel='stylesheet'>
<link href="../../../css/estilostsp.css" rel="stylesheet" type="text/css">
                    
</head>
 
<body onResize="redimensionar()" <%= ( request.getParameter("men")!= null ) ?"onLoad='redimensionar();doFieldFocus( forma );window.opener.location.reload();'" : "onLoad='redimensionar();'"%>>
<div id="capaSuperior" style="position:absolute; width:100%; height:100px; z-index:0; left: 0px; top: 0px;">
	<jsp:include page="/WEB-INF/toptsp.jsp?encabezado=Verificar Referencia"/>
</div>
<div id="capaCentral" style="position:absolute; width:100%; height:87%; z-index:0; left: 0px; top: 100px; overflow: scroll;">
<%String res="";
String hoy  = Utility.getDate(8);
String msg = (request.getParameter("men")!=null)?request.getParameter("men"):"" ;
String tipo = request.getParameter("tipo");
String clase = request.getParameter("clase");
String fecref = request.getParameter("fecref");
Usuario usuario = (Usuario) session.getAttribute("Usuario"); 

if ( msg.equals("ok")){//exito en el ingreso
	res = "Referencia Verificada";	
}%>
<form name='forma' method='POST' action="<%=CONTROLLER%>?estado=Referencia&accion=Verificar&pagina=referenciaVerificacion.jsp&carpeta=/jsp/hvida/referencias" >
<table width="500" border="2" align="center">
    <tr>
      <td><table width="100%" class="tablaInferior">
        <tr>
          <td width="159" class="subtitulo1">&nbsp;Datos de Verifacion</td>
          <td width="217" class="barratitulo"><img src="<%=BASEURL%>/images/titulo.gif" width="32" height="20"></td>
        </tr>
		<tr class="fila">
          <td>Estado</td>
          <td>Correcto
            <input name="esta" type="radio" value="C" checked>
            Incorrecto            
            <input name="esta" type="radio" value="I"></td>
        </tr>
        <tr>
          <td class="fila">Nombre </td>
          <td class="letra"><input  name='nombre'type="hidden"  class='textbox' id="nombre" maxlength='25' value="<%=usuario.getLogin()%>"><%=usuario.getNombre()%>
            <input name="tipo" type="hidden" id="tipo" value="<%=tipo%>">
            <input name="clase" type="hidden" id="clase" value="<%=clase%>">
            <input name="fecref" type="hidden" id="fecref" value="<%=fecref%>">            </td>
          </tr>
        <tr class="fila">
          <td>Fecha</td>
          <td><input  type="text"   name="fecha"  class="textbox" value="<%=hoy%>"  readonly >
            <a href="javascript:void(0)" onclick="if(self.gfPop)gfPop.fPopCalendar(document.forma.fecha);return false;" HIDEFOCUS><img src="<%=BASEURL%>\js\Calendario\cal.gif" width="16" height="16" border="0" alt="De click aqui para escoger la fecha"></a> </td>
        </tr>
        <tr class="fila">
          <td>Referencia</td>
          <td><textarea name="referencia" cols="60" class='textbox'></textarea></td>
        </tr>
      </table></td>
    </tr>
  </table>
  <div align="center"><br>
      <img src="<%=BASEURL%>/images/botones/aceptar.gif"  name="imgaceptar" onClick="return TCamposLlenos(forma);" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" style="cursor:hand">&nbsp;
	  <img src="<%=BASEURL%>/images/botones/cancelar.gif"  name="imgcancelar"  onMouseOver="botonOver(this);" onClick="forma.reset();" onMouseOut="botonOut(this);" style="cursor:hand"> &nbsp;
      <img src="<%=BASEURL%>/images/botones/salir.gif"  name="imgsalir"  onMouseOver="botonOver(this);" onClick="parent.close();" onMouseOut="botonOut(this);" style="cursor:hand">
  </div>
</form>
 <%if(!res.equals("")){%>
  <p><table border="2" align="center">
  <tr>
    <td><table width="100%"  border="1" align="center"  bordercolor="#F7F5F4" bgcolor="#FFFFFF">
      <tr>
        <td width="229" align="center" class="mensajes"><%=res%></td>
        <td width="29" background="<%=BASEURL%>/images/cuadronaranja.JPG">&nbsp;</td>
        <td width="58">&nbsp;</td>
      </tr>
    </table></td>
  </tr>
</table>
<%}%>
</div>
<iframe width=188 height=166 name="gToday:normal:agenda.js:gfPop:plugins_24.js" id="gToday:normal:agenda.js:gfPop:plugins_24.js" src="<%=BASEURL%>/js/Calendario/ipopeng.htm" scrolling="no" frameborder="0" style="visibility:visible; z-index:999; position:absolute; left:-500px; top:0px;"> </iframe>
</body>
</html>
