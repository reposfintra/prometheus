<!--
- Autor : Ing. Diogenes Bastidas
- Date  : 09 de marzo de 2006
- Copyrigth Notice : Fintravalores S.A. S.A
-->
<%--
-@(#)
--Descripcion : Pagina JSP, muestra las referecias 
--%>

<%-- Declaracion de librerias--%>
<%@page contentType="text/html"%>
<%@page errorPage="/error/ErrorPage.jsp"%>
<%@page import="com.tsp.operation.model.beans.*, java.util.*"%>
<%@include file="/WEB-INF/InitModel.jsp" %>
<%@ taglib uri="http://jakarta.apache.org/taglibs/input-0.90" prefix="input" %>
<%
  String doc = (request.getParameter("documento")!=null)?request.getParameter("documento").toUpperCase():"";        
  String tipo = (request.getParameter("tipo")!=null)?request.getParameter("tipo"):"";  
  String msg = (request.getParameter("msg")!=null)?request.getParameter("msg"):"";
  System.out.println("Documento "+doc);  
  System.out.println("Tipo "+tipo);     
  String val ="";
  if(tipo.equals("P")) 
      val = "Placa";
  else if(tipo.equals("C"))
      val = "Cedula";

  Vector vecRef = model.referenciaService.getVectorReferencias();
  TreeMap ciudades = null;
  String nom = "";
%>
<html>
<head>
<title>Verificar Referencias</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link href="../../../css/estilostsp.css" rel="stylesheet" type="text/css"> 
<link href="<%=BASEURL%>/css/estilostsp.css" rel="stylesheet" type="text/css"> 
<script type='text/javascript' src="<%=BASEURL%>/js/referencia.js"></script> 
<script type='text/javascript' src="<%=BASEURL%>/js/boton.js"></script> 
</head>
<body onLoad="redimensionar();" onresize="redimensionar()">
<div id="capaSuperior" style="position:absolute; width:100%; height:100px; z-index:0; left: 0px; top: 0px;">
	<jsp:include page="/WEB-INF/toptsp.jsp?encabezado=Referencias"/>
</div>
<div id="capaCentral" style="position:absolute; width:100%; height:87%; z-index:0; left: 0px; top: 100px; overflow: scroll;">
<form name="frmref" action="<%=CONTROLLER%>?estado=Referencia&accion=Verifivar&cmd=show" method="post" >    
<table border="2" align="center" width="595">
  <tr>
    <td width="818" height="70">
<table width="595" align="center"   >
  <tr>
    <td width="392" height="22"  class="subtitulo1"><p align="left"> Informaci&oacute;n de la referencia </p></td>
    <td width="392"  class="barratitulo"><img src="<%=BASEURL%>/images/titulo.gif" width="32" height="20"></td>
  </tr>
</table>
<table width="595" height="34" align="center" >
  <tr >
    <td width="124" height="30" class="fila">Tipo de documento </td>
    <td width="146" class="letra" ><%=val%><input name="empresa" type="hidden" class="textbox" id="empresa" value="<%=val%>" size="15" readonly>
      <input name="tipo" type="hidden" id="tipo" value="<%=tipo%>"> 
    </td>
    <td width="110" class="fila" >No. documento </td>
    <td width="195" class="letra"><%=doc%><input name="documento" type="hidden" class="textbox" id="docu" value="<%=doc%>" size="15" readonly> </td>
    </tr>
</table>
</td>
</tr>
</table>  

<%int p = 1,c=1,ea=1,ec=1;
for (int i=0; i<vecRef.size(); i++) {
    Referencia referencia = (Referencia) vecRef.elementAt(i);%>   
<%if(referencia.getTiporef().equals("EA")) {%>   
    <input name="fecreferenciaEA<%=ea%>" type="hidden" value="<%=referencia.getFecref()%>">
    <input name="tiporeferenciaEA<%=ea%>" type="hidden" value="<%=referencia.getTiporef()%>">
    <input name="dstrct_codeEA<%=ea%>" type="hidden" value="<%=referencia.getDstrct_code()%>">
  <table border="2" align="center" width="595">
  <tr>
    <td width="818">
<table width="595" align="center"   >
  <tr>
    <td width="392" height="24"  class="subtitulo1"><p align="left">
      Empresa Afiliadora <%=ea%></p></td>
    <td width="392"  class="barratitulo"><img src="<%=BASEURL%>/images/titulo.gif" width="32" height="20"></td>
  </tr>
</table>
<table width="595" align="center" >
  <tr>
    <td width="72" height="30" class="fila">Nombre </td>
    <td width="180" class="letra"> <%=referencia.getNomempresa()%>     
    </td><td width="62" class="fila" >Telefono</td>
    <td width="60" class="letra"> <%=referencia.getTelefono()%></td><td width="46" class="fila">Ciudad </td>
    <td width="147" class="letra"><%=referencia.getCiudadtel()%>   </td>
  </tr>
  <tr>
    <td width="72" height="30" class="fila">Contacto</td>
    <td width="180" class="letra" ><%=referencia.getContacto()%></td>
    <td width="62" class="fila" >Cargo</td>
    <td colspan="3" class="letra"><%=referencia.getCargo_contacto()%> 
    <input name="frm" type="hidden" id="frm" value="1">      </td>
    </tr>
  <tr>
          <td height="30" class="fila">Referencia</td>
          <td colspan="4" class="letra"><%=referencia.getReferencia()%></td>
          <td align="center" <%=(!referencia.getReg_status().equals("")&& referencia.getReg_status().equals("C") )?"class='filaverde'":"class='filaroja'"%>><%if( referencia.getReg_status().equals("") ){%> <a style="cursor:hand "class="Simulacion_Hiper" onClick="window.open('<%=BASEURL%>/jsp/hvida/referencias/referenciaVerificacion.jsp?tipo=<%=referencia.getTiporef()%>&clase=<%=doc%>&fecref=<%=referencia.getFecref()%>','v','status=yes,scrollbars=no,width=650,height=350,resizable=yes')">Verificar</a> <%}else if(referencia.getReg_status().equals("C")){%>Correcto<%}else{%> Incorrecto <%}%>
  </tr>
</table>
<%if (!referencia.getUsrconfirma().equals("")){%>
<table width="100%" border="0">
  <tr>
    <td width="26%" class="fila">Usuario Confirmación </td>
    <td width="30%" class="letra"><%=referencia.getUsrconfirma()%></td>
    <td width="11%" class="fila">Fecha</td>
    <td width="33%" class="letra"><%=referencia.getFecconfir()%></td>
  </tr>
  <tr>
    <td class="fila"><p>Referencia Confirmaci&oacute;n </p>
      </td>
    <td colspan="3" class="letra"><%=referencia.getRef_confirmacion()%></td>
    </tr>
</table>
<%}%>
</td>
</tr>
</table>
<%ea++;
}
if(referencia.getTiporef().equals("CO")) {%>
    <input name="fecreferenciaCO<%=c%>" type="hidden" value="<%=referencia.getFecref()%>">
    <input name="tiporeferenciaCO<%=c%>" type="hidden" value="<%=referencia.getTiporef()%>">
    <input name="dstrct_codeCO<%=c%>" type="hidden" value="<%=referencia.getDstrct_code()%>">
<table border="2" align="center" width="595">
  <tr>
    <td width="818">

<table width="595" align="center"   >
  <tr>
    <td width="392" height="24"  class="subtitulo1"><p align="left">
      Referencia Personal del Conductor <%=c%></p></td>
    <td width="392"  class="barratitulo"><img src="<%=BASEURL%>/images/titulo.gif" width="32" height="20"></td>
  </tr>
</table>
<table width="595" align="center" >
  <tr>
    <td width="91" height="30" class="fila">Nombre</td>
    <td width="134" class="letra"><%=referencia.getNomempresa()%>   
    </td>
    <td width="64"  class="fila">Telefono</td>
    <td width="67" class="letra"><%=referencia.getTelefono()%></td>
    <td width="93" class="fila">Ciudad Telefono </td>
    <td width="118" class="letra"><%=referencia.getCiudadtel()%>
</td>
  </tr>
  <tr>
    <td width="91" height="25" class="fila">Direccion</td>
    <td width="134" class="letra" ><%=referencia.getDireccion()%>     
    </td>
    <td width="64" class="fila">Ciudad</td>
    <td class="letra"><%=referencia.getCiudadtel()%>
</td>
    <td class="fila">Relacion</td>
    <td class="letra"><%=referencia.getRelacion()%></td>
  </tr>
	<tr>
	  <td height="22" class="fila">Referencia</td>
	  <td colspan="4" class="letra"><%=referencia.getReferencia()%></td>
	  <td align="center" <%=(!referencia.getReg_status().equals("")&& referencia.getReg_status().equals("C") )?"class='filaverde'":"class='filaroja'"%>><%if( referencia.getReg_status().equals("") ){%> <a style="cursor:hand "class="Simulacion_Hiper" onClick="window.open('<%=BASEURL%>/jsp/hvida/referencias/referenciaVerificacion.jsp?tipo=<%=referencia.getTiporef()%>&clase=<%=doc%>&fecref=<%=referencia.getFecref()%>','v','status=yes,scrollbars=no,width=650,height=350,resizable=yes')">Verificar</a> <%}else if(referencia.getReg_status().equals("C")){%>Correcto<%}else{%> Incorrecto <%}%></td>
	</tr>
</table>
<%if (!referencia.getUsrconfirma().equals("")){%>
<table width="100%" border="0">
  <tr>
    <td width="26%" class="fila">Usuario Confirmaci&oacute;n </td>
    <td width="30%" class="letra"><%=referencia.getUsrconfirma()%></td>
    <td width="11%" class="fila">Fecha</td>
    <td width="33%" class="letra"><%=referencia.getFecconfir()%></td>
  </tr>
  <tr>
    <td class="fila"><p>Referencia Confirmaci&oacute;n </p></td>
    <td colspan="3" class="letra"><%=referencia.getRef_confirmacion()%></td>
  </tr>
</table>
<%}%>
</td>
  </tr>
</table>
<%c++;
}
if(referencia.getTiporef().equals("PR")) {%>
   <input name="fecreferenciaPR<%=p%>" type="hidden" value="<%=referencia.getFecref()%>">
    <input name="tiporeferenciaPR<%=p%>" type="hidden" value="<%=referencia.getTiporef()%>">
    <input name="dstrct_codePR<%=p%>" type="hidden" value="<%=referencia.getDstrct_code()%>">
<table border="2" align="center" width="595">
  <tr>
    <td width="818">
      <table width="595" align="center"   >
        <tr>
          <td width="392" height="24"  class="subtitulo1"><p align="left"> Referencia personal del propietario <%=p%></p></td>
          <td width="392"  class="barratitulo"><img src="<%=BASEURL%>/images/titulo.gif" width="32" height="20"></td>
        </tr>
      </table>
      <table width="595" align="center" >
        <tr>
          <td width="74" height="27" class="fila">Nombre</td>
          <td width="171" class="letra"><%=referencia.getNomempresa()%>
            </td>
          <td width="68" class="fila">Telefono</td>
          <td width="79" class="letra"><%=referencia.getTelefono()%>
          </td>
          <td width="51" class="fila">Ciudad </td>
          <td width="124" colspan="2" class="letra"><%=referencia.getCiudadtel()%>
		</td>
        </tr>
        <tr>
          <td width="74" height="25" class="fila">Direccion</td>
          <td width="171" class="letra"><%=referencia.getDireccion()%>
            </td>
          <td width="68" class="fila">Ciudad</td>
          <td class="letra"><%=referencia.getCiudadtel()%>
		</td>
          <td class="fila">Relacion</td>
          <td class="letra"><%=referencia.getRelacion()%></td>
        </tr>
        <tr>
          <td height="22" class="fila">Referencia</td>
          <td colspan="4" class="letra"><%=referencia.getReferencia()%></td>
          <td align="center" <%=(!referencia.getReg_status().equals("")&& referencia.getReg_status().equals("C") )?"class='filaverde'":"class='filaroja'"%>><%if( referencia.getReg_status().equals("") ){%> <a style="cursor:hand "class="Simulacion_Hiper" onClick="window.open('<%=BASEURL%>/jsp/hvida/referencias/referenciaVerificacion.jsp?tipo=<%=referencia.getTiporef()%>&clase=<%=doc%>&fecref=<%=referencia.getFecref()%>','v','status=yes,scrollbars=no,width=650,height=350,resizable=yes')">Verificar</a> <%}else if(referencia.getReg_status().equals("C")){%>Correcto<%}else{%> Incorrecto <%}%></td>
        </tr>
    </table>      
      <%if (!referencia.getUsrconfirma().equals("")){%>
      <table width="100%" border="0">
        <tr>
          <td width="26%" class="fila">Usuario Confirmaci&oacute;n </td>
          <td width="30%" class="letra"><%=referencia.getUsrconfirma()%></td>
          <td width="11%" class="fila">Fecha</td>
          <td width="33%" class="letra"><%=referencia.getFecconfir()%></td>
        </tr>
        <tr>
          <td class="fila"><p>Referencia Confirmaci&oacute;n </p></td>
          <td colspan="3" class="letra"><%=referencia.getRef_confirmacion()%></td>
        </tr>
      </table>
      <%}%></td>
  </tr>
</table>
<%p++;
}
if(referencia.getTiporef().equals("EC")) {%>
    <input name="fecreferenciaEC<%=ec%>" type="hidden" value="<%=referencia.getFecref()%>">
    <input name="tiporeferenciaEC<%=ec%>" type="hidden" value="<%=referencia.getTiporef()%>">
    <input name="dstrct_codeEC<%=ec%>" type="hidden" value="<%=referencia.getDstrct_code()%>">
<table border="2" align="center" width="595">
  <tr>
    <td width="818">

<table width="595" align="center"   >
  <tr>
    <td width="392" height="24"  class="subtitulo1"><p align="left">
      Empresa Donde Ha Cargado <%=ec%></p></td>
    <td width="392"  class="barratitulo"><img src="<%=BASEURL%>/images/titulo.gif" width="32" height="20"></td>
  </tr>
</table>
<table width="595" align="center" >
  <tr>
    <td width="76" height="30" class="fila">Nombre</td>
    <td width="185" class="letra"><%=referencia.getNomempresa()%>     
    </td>
    <td width="56" class="fila">Telefono</td>
    <td width="82" class="letra"><%=referencia.getTelefono()%></td>
    <td width="51" class="fila">Ciudad </td>
    <td width="117" class="letra"><%=referencia.getCiudadtel()%>
    </td>
  </tr>
  <tr>
    <td width="76" height="25" class="fila">Direcci&oacute;n</td>
    <td width="185" class="letra"><%=referencia.getDireccion()%>      
    </td>
    <td width="56" class="fila">Ciudad</td>
    <td colspan="3" class="letra"><%=referencia.getCiudadtel()%>
</td>
    </tr>
	<tr>
    <td width="76" height="30" class="fila">Contacto</td>
    <td width="185" class="letra"><%=referencia.getContacto()%>
    </td>
    <td width="56" class="fila">Cargo</td>
    <td colspan="3" class="letra">
    <%=referencia.getCargo_contacto()%></td>
    </tr>
	<tr>
          <td height="30" class="fila">Referencia</td>
          <td colspan="4" class="letra"><%=referencia.getReferencia()%></td>
          <td align="center" <%=(!referencia.getReg_status().equals("")&& referencia.getReg_status().equals("C") )?"class='filaverde'":"class='filaroja'"%>><%if( referencia.getReg_status().equals("") ){%> <a style="cursor:hand "class="Simulacion_Hiper" onClick="window.open('<%=BASEURL%>/jsp/hvida/referencias/referenciaVerificacion.jsp?tipo=<%=referencia.getTiporef()%>&clase=<%=doc%>&fecref=<%=referencia.getFecref()%>','v','status=yes,scrollbars=no,width=650,height=350,resizable=yes')">Verificar</a> <%}else if(referencia.getReg_status().equals("C")){%>Correcto<%}else{%> Incorrecto <%}%></td>
	</tr>
</table>
<%if (!referencia.getUsrconfirma().equals("")){%>
<table width="100%" border="0">
  <tr>
    <td width="26%" class="fila">Usuario Confirmaci&oacute;n </td>
    <td width="30%" class="letra"><%=referencia.getUsrconfirma()%></td>
    <td width="11%" class="fila">Fecha</td>
    <td width="33%" class="letra"><%=referencia.getFecconfir()%></td>
  </tr>
  <tr>
    <td class="fila"><p>Referencia Confirmaci&oacute;n </p></td>
    <td colspan="3" class="letra"><%=referencia.getRef_confirmacion()%></td>
  </tr>
</table>
<%}%></td>
</tr>
</table>
<%ec++;
}%>
<%}%>

<br>
<%if(vecRef.size()==0){
    msg="No existen referencias almacenadas";}%>
<%if(!msg.equals("")){%>
	<p>
   <table border="2" align="center">
     <tr>
    <td><table width="100%"  align="center" bordercolor="#F7F5F4" bgcolor="#FFFFFF"  >
      <tr>
        <td width="229" align="center" class="mensajes"><%=msg%></td>
        <td width="29" background="<%=BASEURL%>/images/cuadronaranja.JPG">&nbsp;</td>
        <td width="58">&nbsp;</td>
      </tr>
    </table></td>
  </tr> 
</table>
  </p>
   <%}%>
   <table width="595" border="0" align="center">
   <tr>
     <td><img src="<%=BASEURL%>/images/botones/regresar.gif"  name="imgaceptar" onClick="window.location='<%=CONTROLLER%>?estado=Menu&accion=Cargar&pagina=buscarReferencia.jsp&carpeta=/jsp/hvida/referencias&marco=no&tipo=<%=tipo%>'" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" style="cursor:hand"></td>
   </tr>
 </table>
</form>
</div>
<iframe width=188 height=166 name="gToday:datetime:<%=BASEURL%>/js/calendartsp/agenda.js:gfPop:<%=BASEURL%>/js/calendartsp/plugins.js" id="gToday:datetime:<%=BASEURL%>/js/calendartsp/agenda.js:gfPop:<%=BASEURL%>/js/calendartsp/plugins.js" src="<%=BASEURL%>/js/calendartsp/ipopeng.htm" scrolling="no" frameborder="0" style="visibility:visible; z-index:999; position:absolute; left:-500px; top:0px;">
</iframe>
</html>
