<%@ include file="/WEB-INF/InitModel.jsp"%>

<HTML>
<HEAD>
<TITLE></TITLE>
<META http-equiv=Content-Type content="text/html; charset=windows-1252">
<link href="../../../css/estilostsp.css" rel="stylesheet" type="text/css"> 
<link href="<%=BASEURL%>/css/estilostsp.css" rel="stylesheet" type="text/css"> 
<style type="text/css">
<!--
.Estilo2 {
	font-size: 16pt;
	color: #FF0000;
}
-->
</style>
</HEAD>
<BODY> 

        <table width="90%" border="0" align="center">
          <tr  class="subtitulo">
            <td height="20"><div align="center">MANUAL DE CREACION DE


 REFERENCIA</div></td>
          </tr>
          <tr class="subtitulo1">
            <td> Registro de referencias del veh&iacute;culo </td>
          </tr>
          <tr>
            <td  height="8"  class="ayudaHtmlTexto"><p>Para la creaci&oacute;n de referencias del veh&iacute;culo, solo debe registrar la informaci&oacute;n solicitada por el siguiente formulario. </p></td>
          </tr>
          <tr>
            <td height="18"  class="ayudaHtmlTexto" align="center"><div align="center"><img src="<%=BASEURL%>/images/ayuda/hvida/referencia/ref1.JPG"></div></td>
          </tr>
          <tr>
            <td height="18"  class="ayudaHtmlTexto" align="center"><p>Luego presiona el boton aceptar para almacenar la informaci&oacute;n. </p>
            </td>
          </tr>
          <tr>
            <td height="18"  class="ayudaHtmlTexto" align="center">&nbsp;</td>
          </tr>
          <tr>
            <td height="18" class="subtitulo1">Registro de referencias del Conductor</td>
          </tr>
          <tr>
            <td height="18" class="ayudaHtmlTexto">Para la creaci&oacute;n de referencias del Conductor, solo debe registrar la informaci&oacute;n solicitada por el siguiente formulario. </td>
          </tr>
          <tr>
            <td height="18" class="ayudaHtmlTexto"><div align="center"><img src="<%=BASEURL%>/images/ayuda/hvida/referencia/refCon1.JPG"></div></td>
          </tr>
          <tr>
            <td height="18" class="ayudaHtmlTexto">Luego presiona el boton aceptar para almacenar la informaci&oacute;n. </td>
          </tr>
          <tr>
            <td height="18" class="ayudaHtmlTexto">&nbsp;</td>
          </tr>
          <tr>
            <td height="18" class="subtitulo1">Registro de referencias del Propietario </td>
          </tr>
          <tr>
            <td height="18" class="ayudaHtmlTexto">Para la creaci&oacute;n de referencias del Propietario, solo debe registrar la informaci&oacute;n solicitada por el siguiente formulario. </td>
          </tr>
          <tr>
            <td height="18" class="ayudaHtmlTexto"><div align="center"><img src="<%=BASEURL%>/images/ayuda/hvida/referencia/refProp1.JPG"></div></td>
          </tr>
          <tr>
            <td height="18" class="ayudaHtmlTexto">Luego presiona el boton aceptar para almacenar la informaci&oacute;n. </td>
          </tr>
          <tr>
            <td height="18" class="ayudaHtmlTexto">&nbsp;</td>
          </tr>
      </table>
</BODY>
</HTML>
