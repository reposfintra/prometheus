<!--
- Autor : Ing. Henry Osorio
- Date  : 26 de septiembre de 2005
- Copyrigth Notice : Fintravalores S.A. S.A
-->
<%--
-@(#)
--Descripcion : Pagina JSP, maneja el formulario de modificacion de referencia
--%>

<%-- Declaracion de librerias--%>
<%--  <%@ page contentType="text/html"%> --%>
<%@ page pageEncoding="UTF-8" %>
<%@ page language="java" contentType="text/html;charset=UTF-8" %>
<%@page errorPage="/error/ErrorPage.jsp"%>
<%@page import="com.tsp.operation.model.beans.*, java.util.*"%>
<%@include file="/WEB-INF/InitModel.jsp" %>
<%@ taglib uri="http://jakarta.apache.org/taglibs/input-0.90" prefix="input" %>
<%String path = application.getRealPath(model.menuService.getRealUrl(request.getRequestURI()));
  String datos[] = model.menuService.getContenidoMenu(BASEURL,request.getRequestURI(),path);
  String doc = (request.getParameter("documento")!=null)?request.getParameter("documento"):"";        
  String tipo = (request.getParameter("tipo")!=null)?request.getParameter("tipo"):"";  
  String msg = (request.getParameter("msg")!=null)?request.getParameter("msg"):"";
  System.out.println("Documento "+doc);  
  System.out.println("Tipo "+tipo);     
  String val ="";
  if(tipo.equals("EA")) 
      val = "Placa";
  else if(tipo.equals("CO") || tipo.equals("PR") || tipo.equals("EC"))
      val = "Cedula";

  Vector vecRef = model.referenciaService.getVectorReferencias();
  TreeMap ciudades = null;
  String nom = "";
%>
<html>
<head>
<title>Agregar Referencias</title>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<link href="../../../css/estilostsp.css" rel="stylesheet" type="text/css"> 
<link href="<%=BASEURL%>/css/estilostsp.css" rel="stylesheet" type="text/css"> 
<script type='text/javascript' src="<%=BASEURL%>/js/referencia.js"></script> 
<script type='text/javascript' src="<%=BASEURL%>/js/boton.js"></script>
<script type='text/javascript' src="<%=BASEURL%>/js/general.js"></script>  
</head>

<body>
<form name="frmref" action="<%=CONTROLLER%>?estado=Referencia&accion=Update&cmd=show" method="post" id="frmref" >    
<table border="2" align="center" width="800">
  <tr>
    <td >
<table width="100%" align="center"   >
  <tr>
    <td width="392" height="22"  class="subtitulo1"><p align="left"> Informaci&oacute;n de la referencia </p></td>
    <td width="392"  class="barratitulo"><img src="<%=BASEURL%>/images/titulo.gif" width="32" height="20" align="left"><%=datos[0]%></td>
  </tr>
</table>
<table width="100%" height="34" align="center" >
  <tr>
    <td width="124" height="30" class="fila">Tipo de documento </td>
    <td width="146" class="letra" ><%=val%><input name="empresa" type="hidden" class="textbox" id="empresa" value="<%=val%>" size="15" readonly>
      <input name="tipo" type="hidden" id="tipo" value="<%=tipo%>"> 
    </td>
    <td width="110" class="fila">No. documento </td>
    <td width="195" class="letra"><%=doc%><input name="documento" type="hidden" class="textbox" id="docu" value="<%=doc%>" size="15" readonly> </td>
    </tr>
</table>
</td>
</tr>
</table>  

<%int p = 1,c=1,ea=1,ec=1;
for (int i=0; i<vecRef.size(); i++) {
    Referencia referencia = (Referencia) vecRef.elementAt(i);%>   
<%if(tipo.equals("EA")) {%>   
    <input name="fecreferenciaEA<%=ea%>" type="hidden" value="<%=referencia.getFecref()%>">
    <input name="tiporeferenciaEA<%=ea%>" type="hidden" value="<%=referencia.getTiporef()%>">
    <input name="dstrct_codeEA<%=ea%>" type="hidden" value="<%=referencia.getDstrct_code()%>">
  <table border="2" align="center" width="800">
  <tr>
    <td >
<table width="100%" align="center"   >
  <tr>
    <td width="392" height="24"  class="subtitulo1"><p align="left">
      Empresa Afiliadora <%=ea%></p></td>
    <td width="392"  class="barratitulo"><img src="<%=BASEURL%>/images/titulo.gif" width="32" height="20"></td>
  </tr>
</table>
<table width="100%" align="center" >
  <tr class="fila">
    <td width="78" height="30">Nombre </td>
    <td width="285" ><input name="nombreEA<%=ea%>" type="text" class="textbox" id="nombre" value="<%=referencia.getNomempresa()%>" size="40" maxlength="30" onKeyPress="soloAlfa(event)">
      <img src="<%=BASEURL%>/images/botones/iconos/obligatorio.gif" width="10" height="10">    </td>
    <td width="60" >Telefono</td>
    <td width="110"><input name="telefonoEA<%=ea%>" type="text" class="textbox" id="telefono<%=referencia.getTiporef()%><%=ea%>" value="<%=referencia.getTelefono()%>" size="15" maxlength="15" onKeyPress="soloDigitos(event,'decNO')">
      <img src="<%=BASEURL%>/images/botones/iconos/obligatorio.gif" width="10" height="10"></td>
    <td width="52">Ciudad </td>
    <td width="175"><%ciudades = model.ciudadService.getTreMapCiudades();	
	nom="telciuEA"+ea;%>
        <input:select name="<%=nom%>" default="<%=referencia.getCiudadtel()%>" options="<%=ciudades%>" attributesText="style='width:80%;' class='listmenu'" /> <img src="<%=BASEURL%>/images/botones/iconos/obligatorio.gif" width="10" height="10">	
    </td>
  </tr>
  <tr class="fila">
    <td width="78" height="30">Contacto</td> 
    <td width="285" ><input name="contactoEA<%=ea%>" type="text" class="textbox" id="contacto" value="<%=referencia.getContacto()%>" size="40" maxlength="30" onKeyPress="soloAlfa(event)">
      <img src="<%=BASEURL%>/images/botones/iconos/obligatorio.gif" width="10" height="10"> </td>
    <td width="60" >Cargo</td>
    <td colspan="3"><input name="cargoEA<%=ea%>" type="text" class="textbox" id="placatrailer" value="<%=referencia.getCargo_contacto()%>" size="30" maxlength="25" onKeyPress="soloAlfa(event)"> 
    <input name="frm" type="hidden" id="frm" value="1"> <img src="<%=BASEURL%>/images/botones/iconos/obligatorio.gif" width="10" height="10"> </td>
    </tr>
  <tr class="fila">
          <td height="30">Referencia</td>
          <td colspan="5"><textarea name="referenciaEA<%=ea%>" cols="130" class="textbox" id="referenciaEA<%=ea%>"><%=referencia.getReferencia()%></textarea>
            <img src="<%=BASEURL%>/images/botones/iconos/obligatorio.gif" width="10" height="10"></td>
          </tr>
</table>
</td>
</tr>
</table>
  <%ea++;
}
if(referencia.getTiporef().equals("CO")) {%>
    <input name="fecreferenciaCO<%=c%>" type="hidden" value="<%=referencia.getFecref()%>">
    <input name="tiporeferenciaCO<%=c%>" type="hidden" value="<%=referencia.getTiporef()%>">
    <input name="dstrct_codeCO<%=c%>" type="hidden" value="<%=referencia.getDstrct_code()%>">
<table border="2" align="center" width="800">
  <tr>
    <td>

<table width="100%" align="center"   >
  <tr>
    <td width="392" height="24"  class="subtitulo1"><p align="left">
      Referencia Personal del Conductor <%=c%></p></td>
    <td width="392"  class="barratitulo"><img src="<%=BASEURL%>/images/titulo.gif" width="32" height="20"></td>
  </tr>
</table>
<table width="100%" align="center" >
  <tr class="fila">
    <td width="74" height="30">Nombre</td>
    <td width="271" ><input name="nombreCO<%=c%>" type="text" class="textbox" id="nombre" value="<%=referencia.getNomempresa()%>" size="40" maxlength="30" onKeyPress="soloAlfa(event)">
      <img src="<%=BASEURL%>/images/botones/iconos/obligatorio.gif" width="10" height="10">    </td>
    <td width="57" >Telefono</td>
    <td width="113"><input name="telefonoCO<%=c%>" type="text" class="textbox" id="telefonoCO<%=c%>" value="<%=referencia.getTelefono()%>"   size="15" maxlength="15" onKeyPress="soloDigitos(event,'decNO')">      
    <img src="<%=BASEURL%>/images/botones/iconos/obligatorio.gif" width="10" height="10"> </td>
    <td width="46">Ciudad </td>
    <td width="149"><%ciudades = model.ciudadService.getTreMapCiudades(); 
	nom="telciuCO"+c;%>
      	  <input:select name="<%=nom%>" default="<%=referencia.getCiudadtel()%>" options="<%=ciudades%>" attributesText="style='width:80%;' class='listmenu'" /> <img src="<%=BASEURL%>/images/botones/iconos/obligatorio.gif" width="10" height="10">
</td>
  </tr>
  <tr class="fila">
    <td width="74" height="25">Direccion</td>
    <td width="271" ><input name="direccionCO<%=c%>" type="text" class="textbox" id="direccion" value="<%=referencia.getDireccion()%>" size="40" maxlength="30">        </td>
    <td width="57" >Ciudad</td>
    <td colspan="4"><%ciudades = model.ciudadService.getCiudades();
	nom="ciudaddirCO"+c;%>
     	<input:select name="<%=nom%>" options="<%=ciudades%>" attributesText="style='width:60% ' class='listmenu'" default="<%=referencia.getCiudadtel()%>"/> <img src="<%=BASEURL%>/images/botones/iconos/obligatorio.gif" width="10" height="10">
</td>
    </tr>
	<tr class="fila">
    <td width="74" height="22">Relacion</td>
    <td ><select name="relacionCO<%=c%>" class="textbox" id="relacionCO<%=c%>" onChange="OtrosUPdate(<%=c%>,'CO');" style='width:75%;'>
      <option value="">Seleccione Un Item</option>
      <% LinkedList tblres = model.tablaGenService.obtenerTablas();
               for(int j = 0; j<tblres.size(); j++){
                       TablaGen respa = (TablaGen) tblres.get(j); %>
      <option value="<%=respa.getTable_code()%>" <%=(referencia.getRelacion().equals(respa.getTable_code()))?"selected":""%>  ><%=respa.getDescripcion()%></option>
      <%}%>
    </select>
      <img src="<%=BASEURL%>/images/botones/iconos/obligatorio.gif" width="10" height="10">    </td>
    <td >Otros</td>
    <td colspan="4" ><input name="otra_relCO<%=c%>" type="text" id="otra_relCO<%=c%>" value="<%=(!referencia.getRelacion().equals("OTRO"))?" ":referencia.getOtra_relacion()%>" size="30" maxlength="30" <%=(!referencia.getRelacion().equals("OTRO"))?"style='visibility:hidden'":""%> onKeyPress="soloAlfa(event)" > 
    <img name="img_otroCO<%=c%>" id="img_otroCO<%=c%>" src="<%=BASEURL%>/images/botones/iconos/obligatorio.gif" width="10" height="10" <%=(!referencia.getRelacion().equals("OTRO"))?"style='visibility:hidden'":"style='visibility:visible'"%>>
      <input name="frm" type="hidden" id="frm" value="2"></td>
    </tr>
	<tr class="fila">
	  <td height="22">Referencia</td>
	  <td colspan="6" ><textarea name="referenciaCO<%=c%>" cols="130" class="textbox" id="referenciaCO<%=c%>"><%=referencia.getReferencia()%></textarea>
	    <img src="<%=BASEURL%>/images/botones/iconos/obligatorio.gif" width="10" height="10"></td>
	  </tr>
</table>
</td>
</tr>
</table>
<%c++;
}
if(referencia.getTiporef().equals("PR")) {%>
   <input name="fecreferenciaPR<%=p%>" type="hidden" value="<%=referencia.getFecref()%>">
    <input name="tiporeferenciaPR<%=p%>" type="hidden" value="<%=referencia.getTiporef()%>">
    <input name="dstrct_codePR<%=p%>" type="hidden" value="<%=referencia.getDstrct_code()%>">
<table border="2" align="center" width="800">
  <tr>
    <td>
      <table width="100%" align="center"   >
        <tr>
          <td width="392" height="24"  class="subtitulo1"><p align="left"> Referencia personal del propietario <%=p%></p></td>
          <td width="392"  class="barratitulo"><img src="<%=BASEURL%>/images/titulo.gif" width="32" height="20"></td>
        </tr>
      </table>
      <table width="100%" align="center" >
        <tr class="fila">
          <td width="75" height="27">Nombre</td>
          <td width="294" ><input name="nombrePR<%=p%>" type="text" class="textbox" id="empresa4" value="<%=referencia.getNomempresa()%>" size="40" maxlength="30" onKeyPress="soloAlfa(event)">
            <img src="<%=BASEURL%>/images/botones/iconos/obligatorio.gif" width="10" height="10">            </td>
          <td width="62" >Telefono</td>
          <td width="116"><input name="telefonoPR<%=p%>" type="text" class="textbox" id="telefono" value="<%=referencia.getTelefono()%>" size="15" maxlength="15" onKeyPress="soloDigitos(event,'decNO')">
            <img src="<%=BASEURL%>/images/botones/iconos/obligatorio.gif" width="10" height="10">          </td>
          <td width="46">Ciudad </td>
          <td width="167" colspan="2"><%ciudades = model.ciudadService.getTreMapCiudades(); 
		  nom="telciuPR"+p;%>  <input:select name="<%=nom%>" default="<%=referencia.getCiudadtel()%>" options="<%=ciudades%>" attributesText="style='width:80%;' class='listmenu'"  /> <img src="<%=BASEURL%>/images/botones/iconos/obligatorio.gif" width="10" height="10">
		</td>
        </tr>
        <tr class="fila">
          <td width="75" height="25">Direccion</td>
          <td width="294" ><input name="direccionPR<%=p%>" type="text" class="textbox" id="direccion" value="<%=referencia.getDireccion()%>" size="40" maxlength="30" >            </td>
          <td width="62" >Ciudad</td>
          <td colspan="4"><%ciudades = model.ciudadService.getCiudades(); 
		  nom = "ciudaddirPR"+p;%>
            	  <input:select name="<%=nom%>" default="<%=referencia.getCiudadtel()%>" options="<%=ciudades%>" attributesText="style='width:60%;' class='listmenu'" /> <img src="<%=BASEURL%>/images/botones/iconos/obligatorio.gif" width="10" height="10">
		</td>
        </tr>
        <tr class="fila">
    <td width="75" height="22">Relacion</td>
    <td ><select name="relacionPR<%=p%>" class="textbox" id="relacionPR<%=p%>" onChange="OtrosUPdate(<%=p%>,'PR');" style='width:75%;'>
      <option value="">Seleccione Un Item</option>
      <% LinkedList tblres = model.tablaGenService.obtenerTablas();
               for(int j = 0; j<tblres.size(); j++){
                       TablaGen respa = (TablaGen) tblres.get(j); %>
      <option value="<%=respa.getTable_code()%>" <%=(referencia.getRelacion().equals(respa.getTable_code()))?"selected":""%>  ><%=respa.getDescripcion()%></option>
      <%}%>
    </select>
      <img src="<%=BASEURL%>/images/botones/iconos/obligatorio.gif" width="10" height="10">    </td>
    <td>Otros</td>
    <td colspan="4"><input name="otra_relPR<%=p%>" type="text" id="otra_relPR<%=p%>"  value="<%=(!referencia.getRelacion().equals("OTRO"))?" ":referencia.getOtra_relacion()%>" size="30" maxlength="30" <%=(!referencia.getRelacion().equals("OTRO"))?"style='visibility:hidden'":""%> onKeyPress="soloAlfa(event)" > 
    <img name="img_otroPR<%=p%>" id="img_otroPR<%=p%>" src="<%=BASEURL%>/images/botones/iconos/obligatorio.gif" width="10" height="10" <%=(!referencia.getRelacion().equals("OTRO"))?"style='visibility:hidden'":"style='visibility:visible'"%>><input name="frm" type="hidden" id="frm" value="3"></td>
    </tr>
        <tr class="fila">
          <td height="22">Referencia</td>
          <td colspan="6" ><textarea name="referenciaPR<%=p%>" cols="130" class="textbox" id="referenciaPR<%=p%>"><%=referencia.getReferencia()%></textarea>
            <img src="<%=BASEURL%>/images/botones/iconos/obligatorio.gif" width="10" height="10"></td>
          </tr>
    </table></td>
  </tr>
</table>
<%p++;
}
if(referencia.getTiporef().equals("EC")) {%>
    <input name="fecreferenciaEC<%=ec%>" type="hidden" value="<%=referencia.getFecref()%>">
    <input name="tiporeferenciaEC<%=ec%>" type="hidden" value="<%=referencia.getTiporef()%>">
    <input name="dstrct_codeEC<%=ec%>" type="hidden" value="<%=referencia.getDstrct_code()%>">
<table border="2" align="center" width="800">
  <tr>
    <td>
<table width="100%" align="center"   >
  <tr>
    <td width="392" height="24"  class="subtitulo1"><p align="left">
      Empresa Donde Ha Cargado <%=ec%></p></td>
    <td width="392"  class="barratitulo"><img src="<%=BASEURL%>/images/titulo.gif" width="32" height="20"></td>
  </tr>
</table>
<table width="100%" align="center" >
  <tr class="fila">
    <td width="72" height="30">Nombre</td>
    <td width="296" ><input name="nombreEC<%=ec%>" type="text" class="textbox" id="nombreEC" value="<%=referencia.getNomempresa()%>" size="40" maxlength="30" onKeyPress="soloAlfa(event)">
      <img src="<%=BASEURL%>/images/botones/iconos/obligatorio.gif" width="10" height="10">    </td>
    <td width="56" >Telefono</td>
    <td width="115"><input name="telefonoEC<%=ec%>" type="text" class="textbox" id="telefonoEC" value="<%=referencia.getTelefono()%>" size="15" maxlength="15" onKeyPress="soloDigitos(event,'decNO')">
      <img src="<%=BASEURL%>/images/botones/iconos/obligatorio.gif" width="10" height="10"> </td>
    <td width="46">Ciudad </td>
    <td width="175"><%ciudades = model.ciudadService.getTreMapCiudades();
	  nom="telciuEC"+ec;%>  <input:select name="<%=nom%>" default="<%=referencia.getCiudadtel()%>" options="<%=ciudades%>" attributesText="style='width:80%;' class='listmenu'" /> <img src="<%=BASEURL%>/images/botones/iconos/obligatorio.gif" width="10" height="10">
    </td>
  </tr>
  <tr class="fila">
    <td width="72" height="25">Direcci&oacute;n</td>
    <td width="296" ><input name="direccionEC<%=ec%>" type="text" class="textbox" id="direccionEC" value="<%=referencia.getDireccion()%>" size="40" maxlength="30">          </td>
    <td width="56" >Ciudad</td>
    <td colspan="3"><%ciudades = model.ciudadService.getTreMapCiudades(); 
	nom = "ciudaddirEC"+ec;%> <input:select name="<%=nom%>" default="<%=referencia.getCiudadtel()%>" options="<%=ciudades%>" attributesText="style='width:60%;' class='listmenu'" /> <img src="<%=BASEURL%>/images/botones/iconos/obligatorio.gif" width="10" height="10">
</td>
    </tr>
	<tr class="fila">
    <td width="72" height="30">Contacto</td>
    <td width="296" ><input name="contactoEC<%=ec%>" type="text" class="textbox" id="contactoEC" value="<%=referencia.getContacto()%>" size="30" maxlength="30" onKeyPress="soloAlfa(event)">
      <img src="<%=BASEURL%>/images/botones/iconos/obligatorio.gif" width="10" height="10">    </td>
    <td width="56" >Cargo</td>
    <td colspan="3">
    <input name="cargoEC<%=ec%>" type="text" class="textbox" id="cargoEC<%=ec%>" value="<%=referencia.getCargo_contacto()%>" size="30" maxlength="25">
    <img src="<%=BASEURL%>/images/botones/iconos/obligatorio.gif" width="10" height="10"> </td>
    </tr>
	<tr class="fila">
          <td height="30">Referencia</td>
          <td colspan="5"><textarea name="referenciaEC<%=ec%>" cols="130" class="textbox" id="referenciaEC<%=ea%>"><%=referencia.getReferencia()%></textarea>
            <img src="<%=BASEURL%>/images/botones/iconos/obligatorio.gif" width="10" height="10"></td>
          </tr>
</table>
</td>
</tr>
</table>
<%ec++;
}%>
<%}%>

<table width="800"  border="2" align="center">
  <tr>
    <td>
<table width="99%" align="center"   >
  
</table>
<table width="100%" align="center"  >
  <tr class="fila">
    <td height="22" style="cursor:hand "class="Simulacion_Hiper" onClick="agregarReferenciaMod('<%=CONTROLLER%>')">Agregar Referencia</td>
    </tr>
</table>    </td>
  </tr>
</table>

<br>
<table width="595" border="0" align="center">
  <tr>
    <td align="center">         
      <img src="<%=BASEURL%>/images/botones/modificar.gif" name="mod" height="21" onClick="TCamposLlenos(frmref);" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" style="cursor:hand">&nbsp; <img src="<%=BASEURL%>/images/botones/regresar.gif"  height="21" name="imgsalir"  onMouseOver="botonOver(this);" onClick="parent.close();" onMouseOut="botonOut(this);" style="cursor:hand">      </td>
    </tr>
</table>
<%if(vecRef.size()==0){
    msg="No existen referencias almacenadas";}%>
<%if(!msg.equals("")){%>
	<p>
   <table border="2" align="center">
     <tr>
    <td><table width="100%"  align="center" bordercolor="#F7F5F4" bgcolor="#FFFFFF" >
      <tr>
        <td width="229" align="center" class="mensajes"><%=msg%></td>
        <td width="29" background="<%=BASEURL%>/images/cuadronaranja.JPG">&nbsp;</td>
        <td width="58" bgcolor="#FFFFFF">&nbsp;</td>
      </tr>
    </table></td>
  </tr> 
</table>
  </p>
   <%}%>
</form>
<iframe width=188 height=166 name="gToday:datetime:<%=BASEURL%>/js/calendartsp/agenda.js:gfPop:<%=BASEURL%>/js/calendartsp/plugins.js" id="gToday:datetime:<%=BASEURL%>/js/calendartsp/agenda.js:gfPop:<%=BASEURL%>/js/calendartsp/plugins.js" src="<%=BASEURL%>/js/calendartsp/ipopeng.htm" scrolling="no" frameborder="0" style="visibility:visible; z-index:999; position:absolute; left:-500px; top:0px;">
</iframe>
<%=datos[1]%>
</html>
