<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
"http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<title>Descripcion Campos Ingresar Despachos</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link href="../../../css/estilostsp.css" rel="stylesheet" type="text/css"> 
</head>

<body>
<br>
<table width="690"  border="2" align="center">
  <tr>
    <td width="811" >
      <table width="100%" border="0" align="center">
        <tr  class="subtitulo">
          <td height="20" colspan="2"><div align="center"> Referencias del Veh&iacute;culo</div></td>
        </tr>
        <tr class="subtitulo1">
          <td colspan="2"> Empresa Afiliadora </td>
        </tr>
		
        <tr>
          <td width="147" class="fila">Nombre empresa</td>
          <td width="527"  class="ayudaHtmlTexto">Campo para digitar el


 Nombre de la empresa afiliadora.</td>
        </tr>
        <tr>
          <td  class="fila">Telefono</td>
          <td  class="ayudaHtmlTexto"> Campo para digitar el numero telef&oacute;nico de la empresa afiliadora </td>
        </tr>
        <tr>
          <td class="fila">Ciudad</td>
          <td  class="ayudaHtmlTexto"> Campo de selecci&oacute;n donde escoge la ciudad del tel&eacute;fono </td>
        </tr>
<tr>
          <td class="fila">Contacto</td>
          <td  class="ayudaHtmlTexto"> Campo para digitar el Nombre del contacto de la empresa. </td>
        </tr>
		<tr>
          <td width="147" class="fila">Cargo</td>
          <td width="527"  class="ayudaHtmlTexto"> Campo para digitar el cargo que ocupa el contacto en la empresa </td>
        </tr>
        <tr>
          <td width="147" class="fila">Referencia</td>
          <td width="527"  class="ayudaHtmlTexto"> Campo para digitar la referencia de la empresa </td>
        </tr>
      </table>
    </td>
  </tr>
</table>
<br>
<table width="690" border="2" align="center">
  <tr>
    <td><table width="100%" border="0" align="center">
		 <tr class="subtitulo">
          <td colspan="2" align="center" height="20"> Referencias del Conductor </td>
        </tr>
		<tr>
          <td colspan="2" class="subtitulo1">Referencias personales </td>
        </tr>
		<tr>
          <td width="147" class="fila">Nombre</td>
          <td width="527"  class="ayudaHtmlTexto">Campo para digitar el Nombre de la referencia.</td>
        </tr>
		<tr>
		  <td  class="fila">Telefono</td>
          <td  class="ayudaHtmlTexto"> Campo para digitar el numero telef&oacute;nico de la referencia.</td>
		</tr>
		<tr>
		  <td class="fila">Ciudad</td>
          <td  class="ayudaHtmlTexto"> Campo de selecci&oacute;n donde escoge la ciudad del tel&eacute;fono. </td>
		</tr>
		<tr>
		  <td class="fila"> Direcci&oacute;n</td>
          <td  class="ayudaHtmlTexto">Campo para digitar la direcci&oacute;n de la referencia.</td>
		</tr>
		<tr>
		  <td width="147" class="fila">Ciudad</td>
          <td width="527"  class="ayudaHtmlTexto">Campo de selecci&oacute;n donde escoge la ciudad de la direcci&oacute;n.</td>
		</tr>
		<tr>
		  <td width="147" class="fila"> Relaci&oacute;n</td>
          <td width="527"  class="ayudaHtmlTexto">Campo de selecci&oacute;n para 


 escoger la relaci&oacute;n (


 Si la relaci&oacute;n no existe la selecci&oacute;n la se digita en el campo otros ) </td>
		</tr>
		<tr>
          <td width="147" class="fila">Otros</td>
          <td width="527"  class="ayudaHtmlTexto">Campo para digitar la otra relaci&oacute;n</td>
        </tr>
		<tr>
          <td class="fila">Referencia</td>
          <td  class="ayudaHtmlTexto"> Campo para digitar la referencia personal </td>
	    </tr>
		<tr>
          <td colspan="2" class="subtitulo1">Empresa donde ha cargado</td>
        </tr>
		<tr>
          <td class="fila">Nombre</td>
          <td  class="ayudaHtmlTexto">Campo para digitar el Nombre de la referencia.</td>
	    </tr>
		<tr>
          <td  class="fila">Telefono</td>
          <td  class="ayudaHtmlTexto"> Campo para digitar el numero telef&oacute;nico de la referencia.</td>
	    </tr>
		<tr>
          <td class="fila">Ciudad</td>
          <td  class="ayudaHtmlTexto"> Campo de selecci&oacute;n donde escoge la ciudad del tel&eacute;fono. </td>
	    </tr>
		<tr>
          <td class="fila"> Direcci&oacute;n</td>
          <td  class="ayudaHtmlTexto">Campo para digitar la direcci&oacute;n de la referencia.</td>
	    </tr>
		<tr>
          <td class="fila">Ciudad</td>
          <td  class="ayudaHtmlTexto">Campo de selecci&oacute;n donde escoge la ciudad de la direcci&oacute;n.</td>
	    </tr>
		<tr>
          <td class="fila">Contacto</td>
          <td  class="ayudaHtmlTexto"> Campo para digitar el Nombre del contacto de la empresa. </td>
	    </tr>
		<tr>
          <td class="fila">Cargo</td>
          <td  class="ayudaHtmlTexto"> Campo para digitar el cargo que ocupa el contacto en la empresa </td>
	    </tr>
		<tr>
          <td class="fila">Referencia</td>
          <td  class="ayudaHtmlTexto"> Campo para digitar la referencia de la empresa </td>
	    </tr>
      </table></td>
  </tr>
</table>
<br>
<table width="690" border="2" align="center">
  <tr>
    <td><table width="100%" border="0" align="center">
       <tr class="subtitulo">
          <td colspan="2" align="center" height="20"> Referencias del Propietario </td>
        </tr>
		 <tr class="subtitulo1">
          <td colspan="2"> Referencias personales </td>
        </tr>
		 <tr>
           <td class="fila">Nombre</td>
           <td  class="ayudaHtmlTexto">Campo para digitar el Nombre de la referencia.</td>
	    </tr>
		 <tr>
           <td  class="fila">Telefono</td>
           <td  class="ayudaHtmlTexto"> Campo para digitar el numero telef&oacute;nico de la referencia.</td>
	    </tr>
		 <tr>
           <td class="fila">Ciudad</td>
           <td  class="ayudaHtmlTexto"> Campo de selecci&oacute;n donde escoge la ciudad del tel&eacute;fono. </td>
	    </tr>
		 <tr>
           <td class="fila"> Direcci&oacute;n</td>
           <td  class="ayudaHtmlTexto">Campo para digitar la direcci&oacute;n de la referencia.</td>
	    </tr>
		 <tr>
           <td class="fila">Ciudad</td>
           <td  class="ayudaHtmlTexto">Campo de selecci&oacute;n donde escoge la ciudad de la direcci&oacute;n.</td>
	    </tr>
		 <tr>
           <td class="fila"> Relaci&oacute;n</td>
           <td  class="ayudaHtmlTexto">Campo de selecci&oacute;n para escoger la relaci&oacute;n ( Si la relaci&oacute;n no existe la selecci&oacute;n la se digita en el campo otros ) </td>
	    </tr>
		 <tr>
           <td class="fila">Otros</td>
           <td  class="ayudaHtmlTexto">Campo para digitar la otra relaci&oacute;n</td>
	    </tr>
		 <tr>
           <td class="fila">Referencia</td>
           <td  class="ayudaHtmlTexto"> Campo para digitar la referencia personal </td>
	    </tr>
      </table></td>
  </tr>
</table>
<p><br>
</p>
<p>&nbsp;</p>

<p>&nbsp;</p>
<p>&nbsp; </p>
</body>
</html>
