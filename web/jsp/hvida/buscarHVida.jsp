<%@page contentType="text/html"%>
<%@page errorPage="/error/ErrorPage.jsp"%>
<%@page import="com.tsp.operation.model.beans.*, java.util.*"%>
<%@include file="/WEB-INF/InitModel.jsp"%>
<%String path = application.getRealPath(model.menuService.getRealUrl(request.getRequestURI()));
  String datos[] = model.menuService.getContenidoMenu(BASEURL,request.getRequestURI(),path);
  String msg = (request.getParameter("msg")!=null)?request.getParameter("msg"):"";%>
<html>
<head>
<title>Buscar Hoja de Vida</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link href="../../css/estilostsp.css" rel="stylesheet" type="text/css"> 
<link href="<%=BASEURL%>/css/estilostsp.css" rel="stylesheet" type="text/css"> 
<script type='text/javascript' src="<%=BASEURL%>/js/boton.js"></script> 
<script type='text/javascript' src="<%=BASEURL%>/js/hvida.js"></script> 
<script type='text/javascript' src="<%=BASEURL%>/js/funciones.js"></script> 
<script type='text/javascript' src="<%=BASEURL%>/js/reporte.js"></script> 
<script type='text/javascript' src="<%=BASEURL%>/js/general.js"></script> 
</head>
<body onLoad="redimensionar();" onResize="redimensionar();">
<div id="capaSuperior" style="position:absolute; width:100%; height:100px; z-index:0; left: 0px; top: 0px;">
<jsp:include page="/WEB-INF/toptsp.jsp?encabezado=Buscar Hoja de Vida"/>
</div>

<div id="capaCentral" style="position:absolute; width:100%; height:85%; z-index:0; left: 0px; top: 100px; overflow: scroll;"> 
  
<%
   String busq = (request.getParameter("busq")!=null)?request.getParameter("busq"):""; 
   String cedu = (request.getParameter("cedu")!=null)?request.getParameter("cedu"):""; 
%>
<form name="form1" id="form1" action="<%=CONTROLLER%>?estado=Placa&accion=SearchOtro" method="post">
  <table border="2" align="center" width="366">
    <tr>
      <td width="818">
        <table width="99%" align="center">
          <tr>
            <td width="161" height="22"   class="subtitulo1"><p align="left">Busqueda</p></td>
            <td width="177"  class="barratitulo"><img src="<%=BASEURL%>/images/titulo.gif" width="32" height="20" align="left"><%=datos[0]%></td>
          </tr>
        </table>
        <table width="99%" align="center">
          <tr class="fila">
            <td width="22"  align="left"><input name="checkbox" type="radio" value="1"></td>
            <td align="left" width="86" >Conductor</td>
            <td align="left" width="236" ><input name="conductor" type="text" class="textbox" id="conductor" title="Numero de la placa" size="20" maxlength="15" onKeyPress="soloDigitos(event,'decNO')">
            <a class="Simulacion_Hiper" style="cursor:hand "  onClick="window.open('<%=CONTROLLER%>?estado=Menu&accion=Enviar&numero=37&conductor=ok','cond','height=450,width=750,left=300,scrollbars=no,status=yes,resizable=yes')" >Buscar Conductor </a></td>
          </tr>
          <tr class="fila">
            <td align="left" ><input name="checkbox" type="radio" value="2"></td>
            <td align="left" >Propietario</td>
            <td align="left" ><input name="propietario" type="text" class="textbox" id="propietario" title="Numero de la placa" size="20" maxlength="15" onKeyPress="soloDigitos(event,'decNO')"><a class="Simulacion_Hiper" style="cursor:hand "  onClick="window.open('<%=CONTROLLER%>?estado=Menu&accion=Enviar&numero=37&hvida=ok','Prop','height=450,width=750,left=300,status=yes,scrollbars=no,resizable=yes')" >Buscar Propietario</a></td>
          </tr>
          <tr class="fila">
            <td align="left" ><input name="checkbox" type="radio" value="3"></td>
            <td align="left" >Placa</td>
            <td align="left" ><input name="placa" type="text" class="textbox" id="placa" title="Numero de la placa" size="10" maxlength="7" onKeyPress="soloAlfa(event)" style="text-transform:uppercase"></td>
          </tr>
      </table></td>
    </tr>
  </table>
  <br>
  <div align="center">    <img src="<%=BASEURL%>/images/botones/buscar.gif" name="mod" height="21" onClick="busquedaHV();" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" style="cursor:hand">&nbsp; 
	<img src="<%=BASEURL%>/images/botones/salir.gif"  height="21" name="imgsalir"  onMouseOver="botonOver(this);" onClick="parent.close();" onMouseOut="botonOut(this);" style="cursor:hand"></div>
</form>
<%if (!msg.equals("")) {
%>

<table border="2" align="center">
    <tr>
      <td height="33"><table width="100%"  border="1" align="center"  bordercolor="#F7F5F4" bgcolor="#FFFFFF">
          <tr>
            <td width="229" align="center" class="mensajes"><%=msg%></td>
            <td width="29" background="<%=BASEURL%>/images/cuadronaranja.JPG">&nbsp;</td>
            <td width="58">&nbsp;</td>
          </tr>
      </table></td>
    </tr>
  </table>
<%}%>
	   <%
  Vector vehiculos = model.placaService.getVehiculos();
  if (vehiculos!=null && vehiculos.size()>0) {  
 %>
<table width="400" border="2" align="center">
    <tr>
      <td>

	  <table width="99%" align="center">
              <tr>
                <td width="171" height="22" bgcolor="#BDCF10" class="subtitulo1"><p>Placas Relacionadas con el <%=busq%> <%=cedu%></p>
                  </td>
                <td width="201" class="barratitulo"><img src="<%=BASEURL%>/images/titulo.gif" width="32" height="20"></td>
              </tr>
  </table> 
  <table width="99%" border="1" align="center">
  <tr class="tblTitulo">
    <td align="center"> Placa</td>
    </tr> 
  <%for (int i=0; i<vehiculos.size(); i++) {
       String placa = (String) vehiculos.elementAt(i);			%>
 <tr class="<%=(i % 2 == 0 )?"filagris":"filaazul"%>" onMouseOver='cambiarColorMouse(this)'>    
    <td  class="bordereporte" onClick="window.open('<%=CONTROLLER%>?estado=Placa&accion=Search&cmd=show&pag=hoja&idplaca=<%=placa%>','hv','height=500,width=750,left=300,scrollbars=no,status=yes,resizable=yes')" style="cursor:hand "><div align="center"><%=placa%></div></td>    
    </tr>
  
   <%}%>
</table>
</td>
</tr>
</table>
<%}%>
  <%if(vehiculos!=null && vehiculos.size()==0) {%>
  <p>
  <table border="2" align="center">
    <tr>
      <td height="33"><table width="100%"  border="1" align="center"  bordercolor="#F7F5F4" bgcolor="#FFFFFF">
          <tr>
            <td width="229" align="center" class="mensajes">No se encontraron resultados</td>
            <td width="29" background="<%=BASEURL%>/images/cuadronaranja.JPG">&nbsp;</td>
            <td width="58">&nbsp;</td>
          </tr>
      </table></td>
    </tr>
  </table>
  <p></p>
  <%}%>
</div>
<%=datos[1]%>
</body>
</html>
