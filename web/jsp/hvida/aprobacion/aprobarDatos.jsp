<!--
- Autor : Ing. Diogenes Bastidas
- Date  : 14 de enero de 2006
- Copyrigth Notice : Fintravalores S.A. S.A
-->
<%--
-@(#)
--Descripcion : Pagina JSP, que lista las ubicacion
--%>

<%-- Declaracion de librerias--%>
<%@page contentType="text/html"%>
<%@page session="true"%>
<%@page errorPage="/error/ErrorPage.jsp"%>
<%@page import="java.util.*"%>
<%@page import="com.tsp.operation.model.*"%>
<%@page import="com.tsp.operation.model.beans.*"%>
<%@page import="com.tsp.util.*"%>
<%@include file="/WEB-INF/InitModel.jsp"%>
<%@taglib uri="http://jsptags.com/tags/navigation/pager" prefix="pg" %>
<html>
<head><title>Listar Tablas</title>
<link href="<%= BASEURL %>/css/estilostsp.css" rel="stylesheet" type="text/css">
<script type='text/javascript' src="<%= BASEURL %>/js/boton.js"></script>
<script type='text/javascript' src="<%= BASEURL %>/js/reporte.js"></script>
</head>
<body onLoad="redimensionar();" onResize="redimensionar();">
<div id="capaSuperior" style="position:absolute; width:100%; height:100px; z-index:0; left: 0px; top: 0px;">
<jsp:include page="/WEB-INF/toptsp.jsp?encabezado=Lista de Tablas"/>
</div>

<div id="capaCentral" style="position:absolute; width:100%; height:85%; z-index:0; left: 0px; top: 100px; overflow: scroll;"> 
<%  
    String style = "simple";
    String position =  "bottom";
    String index =  "center";
    int maxPageItems = 10;
    int maxIndexPages = 2;
    Vector vec = model.usuaprobacionService.obtVecTablaAprovacion();
	System.out.println("Tama�o "+vec.size());
	if ( vec.size() >0 ){%>
<table width="425" border="2" align="center">
    <tr>
      <td width="448">
	  	  <table width="100%" align="center">
              <tr>
                <td width="373" class="subtitulo1">Tablas Aprobar </td>
                <td width="427" class="barratitulo"><img src="<%=BASEURL%>/images/titulo.gif" width="32" height="20"></td>
              </tr>
        </table>	
	<table width="100%" border="1" bordercolor="#999999" bgcolor="#F7F5F4" align="center">
		<tr class="tblTitulo" >
			<td align="center">Tabla</td>
		</tr>
		<pg:pager
			items="<%=vec.size()%>"
			index="<%= index %>"
			maxPageItems="<%= maxPageItems %>"
			maxIndexPages="<%= maxIndexPages %>"
			isOffset="<%= true %>"
			export="offset,currentPageNumber=pageNumber"
			scope="request">
	<%
		for (int i = offset.intValue(), l = Math.min(i + maxPageItems, vec.size()); i < l; i++){
			UsuarioAprobacion usuapro = (UsuarioAprobacion) vec.elementAt(i);%>
			<pg:item>
				<tr class="<%=(i % 2 == 0 )?"filagris":"filaazul"%>" onMouseOver='cambiarColorMouse(this)'   style="cursor:hand" onClick="window.open('<%=CONTROLLER%>?estado=UsuarioAprobacion&accion=Listar&tabla=<%=usuapro.getTabla()%>','myWindow','status=yes,scrollbars=no,width=650,height=600,resizable=yes');" >
					<td align="center" class="bordereporte"><%=usuapro.getTabla()%></td>
				</tr>
		  </pg:item>
	<%  }%>
		<tr class="pie">
			<td td height="20" colspan="10" nowrap align="center">          <pg:index>
					<jsp:include page="/WEB-INF/jsp/googlesinimg.jsp" flush="true"/>
			</pg:index>
			</td>
		</tr>
    	</pg:pager>        
	 </table>
	</td>
  </tr>
</table>
<br>
<%}else { %>
</p>
  <table border="2" align="center">
    <tr>
      <td><table width="410" height="73" border="1" align="center"  bordercolor="#F7F5F4" bgcolor="#FFFFFF">
          <tr>
            <td width="282" align="center" class="mensajes">No tiene tablas asignadas !.. </td>
            <td width="28" background="<%=BASEURL%>/images/cuadronaranja.JPG">&nbsp;</td>
            <td width="78">&nbsp;</td>
          </tr>
      </table></td>
    </tr>
  </table>
  <p>&nbsp; </p>
  <%}%>
 <table width="500" border="0" align="center">
    <tr>
      <td>
	  <img src="<%=BASEURL%>/images/botones/salir.gif" style="cursor:hand" title="Volver" name="buscar"  onclick="window.close()" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);">
	  </td>
	</tr>
</table>
</div>
</body>
</html>
