<!--
- Autor : Ing. Diogenes Bastidas
- Date  : 14 de enero de 2006
- Copyrigth Notice : Fintravalores S.A. S.A
-->
<%--
-@(#)
--Descripcion : Pagina JSP, que lista las ubicacion
--%>

<%-- Declaracion de librerias--%>
<%@page contentType="text/html"%>
<%@page session="true"%>
<%@page errorPage="/error/ErrorPage.jsp"%>
<%@page import="java.util.*"%> 
<%@page import="com.tsp.operation.model.*"%>
<%@page import="com.tsp.util.*"%>
<%@page import="com.tsp.operation.model.beans.*, java.util.*"%>
<%@include file="/WEB-INF/InitModel.jsp"%>
<%@taglib uri="http://jsptags.com/tags/navigation/pager" prefix="pg" %>
 
<html>
<head>
<title>Listado de Identidad</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link href="<%= BASEURL %>/css/estilostsp.css" rel="stylesheet" type="text/css">
<link href="../../../css/estilostsp.css" rel="stylesheet" type="text/css"> 
<script type='text/javascript' src="<%= BASEURL %>/js/reporte.js"></script>
<script type='text/javascript' src="<%= BASEURL %>/js/boton.js"></script>
<script>
    function validar(){
        for(i=0;i < forma.length;i++){				
            if (forma.elements[i].checked){
                forma.submit(); 
                return true;
            }
        }
        alert('Por favor seleccione un registro para poder continuar');
        return false;
    } 
    
    function chequear(apr){
    
        var chk = apr.checked;
        
        for( var i=0; i < forma.length; i++ ){
            var ele  = forma.elements[i];      
            if(ele.type=='checkbox' ){
                ele.checked = chk;
            }
        }
    }
</script>
</head>
<body onLoad="redimensionar();" onResize="redimensionar();">
    <div id="capaSuperior" style="position:absolute; width:100%; height:100px; z-index:0; left: 0px; top: 0px;">
        <jsp:include page="/WEB-INF/toptsp.jsp?encabezado=Lista de Tablas"/>
    </div>

    <div id="capaCentral" style="position:absolute; width:100%; height:85%; z-index:0; left: 0px; top: 100px; overflow: scroll;"> 
        <p>&nbsp;</p> 
        <p>
  <% 
	Vector VecIden = model.identidadService.obtVecIdentidad();
    Identidad iden;

	if ( VecIden.size() >0 ){  
%>
        </p>
        <FORM name='forma' id='forma' method='POST' action="<%=CONTROLLER%>?estado=UsuarioAprobacion&accion=Manager" >
        <table width="500" border="2" align="center">
            <tr>
                <td width="1508">
                <table width="100%" align="center">
                    <tr>
                        <td width="373" class="subtitulo1">&nbsp;Datos Identidad 
                        <input name="tabla" type="hidden" id="tabla" value="NIT"></td>
                        <td width="427" class="barratitulo"><img src="<%=BASEURL%>/images/titulo.gif" width="32" height="20"></td>
                    </tr>
                </table>
                <table width="99%" border="1" align="center" bordercolor="#999999" bgcolor="#F7F5F4">
                    <tr class="tblTitulo">
                        <td width="16%" align="center">Documento</td>
                        <td width="50%"  align="center">Nombres Completo</td>
                        <td align="center">Aprobado
                            <input name="apr" onclick = 'chequear(this)' type="checkbox" id="apr" value="S">
                        </td>
                        <td width="80%"  align="center">Usuario Creaci�n</td>
                    </tr>                        
        
        <%
        
      for (int i = 0; i < VecIden.size() ; i++)  {
          iden = (Identidad) VecIden.elementAt(i);%>

                    <tr class="<%=(i % 2 == 0 )?"filagris":"filaazul"%>" >
                        <td class="bordereporte" width="16%" ><%=iden.getCedula()%></td>
                        <td width="65%" class="bordereporte"><%=iden.getNombre()%></td>
                        <td class="bordereporte"align="center">
                            <input name="apro<%=i%>" type="checkbox" id="apro<%=i%>" value="S">
                        </td>
                        <td width="65%" class="bordereporte"><%=iden.getUsuariocrea()%></td>
                    </tr>

        <%}%>
        
                </table></td>
            </tr>
        </table>
        </form>
        <br>
        <div align="center">
            <img src="<%=BASEURL%>/images/botones/aceptar.gif"  name="imgaceptar" onClick="return validar();" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" style="cursor:hand">&nbsp;
            <img src="<%=BASEURL%>/images/botones/salir.gif"  name="imgsalir"  onMouseOver="botonOver(this);" onClick="parent.close();" onMouseOut="botonOut(this);" style="cursor:hand">
        </div>
      <%}
 else { %>
        </p>
        <table border="2" align="center">
            <tr>
                <td><table width="410" height="40" border="1" align="center"  bordercolor="#F7F5F4" bgcolor="#FFFFFF">
                    <tr>
                        <td width="282" align="center" class="mensajes">No existen Nit pendientes por aprobacion!</td>
                        <td width="28" background="<%=BASEURL%>/images/cuadronaranja.JPG">&nbsp;</td>
                        <td width="78">&nbsp;</td>
                    </tr>
                </table></td>
            </tr>
        </table>
        <p>&nbsp; </p>
        <table width="500" border="0" align="center">
            <tr>
                <td><img src="<%=BASEURL%>/images/botones/salir.gif"  name="imgaceptar" onClick="window.close()" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" style="cursor:hand"></td>
            </tr>
        </table>
  <%}%>
    </div>
</body>
</html>
