<%@page contentType="text/html"%>
<%@page errorPage="/error/ErrorPage.jsp"%>
<%@page import="com.tsp.operation.model.beans.*, java.util.*"%>
<%@include file="/WEB-INF/InitModel.jsp" %>
<%@ taglib uri="http://jakarta.apache.org/taglibs/input-0.90" prefix="input" %>
<%

  HojaVida hv = model.hojaVidaService.getHojaVida();
  Conductor conductor      = hv.getConductor();
  Identidad iden           = hv.getIdentidad();
  Proveedor propietario  = hv.getPropietario();
  Tenedor   tenedor        = hv.getTenedor();      
  Placa     placa          = hv.getPlaca();
  String    cond           = (!hv.getFotoConductor().equals("")) ? hv.getFotoConductor() : "prede.jpg";
  String    plac          = (!hv.getFotoVehiculo().equals("")) ? hv.getFotoVehiculo() : "prede.jpg";
  String    huella        = hv.getHuella_der();
  String    huella2       = hv.getHuella_izq();
  String    leyenda1      = hv.getLeyenda1();
  String    leyenda2      = hv.getLeyenda2();
  String    firma         = hv.getFirma();

  session.setAttribute("placa",hv.getPlaca());
%>
<html>
<head>
<title>HOJA DE VIDA</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link href="../../css/print.css" rel="stylesheet" type="text/css"> 
<link href="<%=BASEURL%>/css/print.css" rel="stylesheet" type="text/css"> 
<script type='text/javascript' src="<%=BASEURL%>/js/Validacion.js"></script> 
<script type='text/javascript' src="<%=BASEURL%>/js/boton.js"></script> 
</head>
<body>
<input:form name="frmplaca" method="post" bean="placa">
<table width="100%" border="1" align="center" cellpadding="0" cellspacing="0" bordercolor="#000000"   >
  <tr class="titulo">
    <td rowspan="2" width="15%" align="center">&nbsp;</td>
    <td width="66%" height="14" align="center">FINTRAVALORES S. A.</td>
    <td width="15%" rowspan="2" align="center">&nbsp;</td>
  </tr>
  <tr class="titulo">
    <td height="14" align="center">FORMATO DE VERIFICACION DE HOJA DE VIDA</td>
  </tr>
</table>


<table width="100%"  align="center" cellpadding="0" >
  <tr>
    <td colspan="8" valign="top">
  <table width="100%" border="1" align="center" cellpadding="0" cellspacing="0" bordercolor="#000000">
      <tr>
        <td colspan="7" align="center" class="titulo" >HOJA DE VIDA </td>
        </tr>
      <tr>
        <td colspan="7" class="titulo" align="center">INFORMACION DEL VEHICULO </td>
        </tr>
      <tr>
        <td width="241" rowspan="19" class="titulo" align="center" ><img src="<%=BASEURL%>/documentos/imagenes/<%=plac%>" width="241" height="180" align="middle"></td>
        <td  width="122" class="titulo" >PLACA CABEZOTE </td>
        <td width="129" class="detalle" ><input:text name="placa" attributesText="size='8' readonly class='texto_sinBorde'"/></td>
        <td width="139" ><span class="titulo"># EJES </span></td>
        <td width="109" class="detalle"><input:text name="noejes" attributesText="maxlength='1' size='8' readonly onkeypress='return acceptNum(event)' class='texto_sinBorde'"/></td>
        <td width="103"  class="titulo">TIPO CARROCERIA</td>
        <td width="108" class="detalle" ><input:text name="carroceria" attributesText="style='width:100%;'  readonly class='texto_sinBorde'"/></td>
        </tr>
      <tr>
        <td  class="titulo">TIPO</td>
        <td class="detalle"><input:text name="tipo" attributesText="maxlength='12' size='13' readonly class='texto_sinBorde'"/></td>
        <td  class="titulo">REG. NAL CARGA </td>
        <td class="detalle" ><input:text name="reg_nal_carga" attributesText="maxlength='15'  size='19' readonly class='texto_sinBorde'"/></td>
        <td  class="titulo">VENCE</td>
        <td class="detalle" ><input:text name="fecvenreg" attributesText="readonly  size='11' class='texto_sinBorde' readonly"/></td>
        </tr>
      <tr>
        <td  class="titulo">CHASIS</td>
        <td class="detalle"><input:text name="nochasis" attributesText="maxlength='12' size='13' readonly class='texto_sinBorde'"/></td>
        <td  class="titulo">PLACA TRAILER </td>
        <td class="detalle" ><input:text name="placa_trailer" attributesText="maxlength='15' size='8' readonly class='texto_sinBorde'"/></td>
        <td  class="titulo">NUMERO RIN</td>
        <td class="detalle" ><input:text name="numero_rin" attributesText="maxlength='1' size='13' readonly class='texto_sinBorde'"/></td>
        </tr>
      <tr>
        <td  class="titulo">MOTOR</td>
        <td class="detalle"><input:text name="nomotor" attributesText="maxlength='12'  readonly class='texto_sinBorde'"/></td>
        <td  class="titulo">CAPACIDAD TRAILER </td>
        <td class="detalle" ><input:text name="capacidad_trailer" attributesText="maxlength='2' readonly  class='texto_sinBorde'"/></td>
        <td  class="titulo">GRADO RIESGO</td>
        <td class="detalle" ><input:text name="grado_riesgo" attributesText="maxlength='2' readonly   class='texto_sinBorde'"/></td>
        </tr>
      <tr>
        <td  class="titulo">COLOR</td>
        <td class="detalle"><input:text name="color"  attributesText="style='width:100%;'  readonly class='texto_sinBorde'"/></td>
        <td  class="titulo">REG NAL. GASES</td>
        <td class="detalle" ><input:text name="certgases" attributesText="readonly  size='19' class='texto_sinBorde'"/></td>
        <td  class="titulo">VENCE </td>
        <td class="detalle" ><input:text name="fecvengases" attributesText="readonly   class='texto_sinBorde' readonly"/></td>
        </tr>
      <tr>
        <td  class="titulo">MARCA</td>
        <td class="detalle"><input:text name="marca" attributesText="style='width:100%;' readonly class='texto_sinBorde'"/></td>
        <td  class="titulo">POLIZA ANDINA</td>
        <td class="detalle" ><input:text name="poliza_andina" attributesText="readonly   class='texto_sinBorde'"/></td>
        <td  class="titulo">VENCE</td>
        <td class="detalle" ><input:text name="fecvenandina" attributesText="readonly   class='texto_sinBorde' readonly"/></td>
        </tr>
      <tr>
        <td  class="titulo">MODELO</td>
        <td class="detalle"><input:text name="modelo" attributesText="class='texto_sinBorde'readonly  size='10'  maxlength='4''"/></td>
        <td  class="titulo">S.O.A.T # </td>
        <td class="detalle" ><input:text name="polizasoat" attributesText="readonly  size='19' class='texto_sinBorde'"/></td>
        <td  class="titulo">VENCE</td>
        <td class="detalle" ><input:text name="venseguroobliga" attributesText="maxlength='15'  size='12' class='texto_sinBorde' readonly"/></td>
        </tr>
      <tr>
        <td  class="titulo">CAPACIDAD</td>
        <td class="detalle"><input:text name="capacidad" attributesText="maxlength='2' readonly   class='texto_sinBorde'"/></td>
        <td  class="titulo">TARJETA EMPRESARIAL</td>
        <td class="detalle" ><input:text name="tarempresa" attributesText="maxlength='15'  readonly size='19' class='texto_sinBorde'"/></td>
        <td  class="titulo">VENCE</td>
        <td class="detalle" ><input:text name="fecvenempresa" attributesText="readonly   class='texto_sinBorde' readonly"/></td>
        </tr>
      <tr>
        <td  class="titulo">EMP. AFILIADORA</td>
        <td class="detalle"><input:text name="empresaafil" attributesText="maxlength='1'  readonly class='texto_sinBorde'"/></td>
        <td  class="titulo">TARJETA HABILITACION</td>
        <td class="detalle" ><input:text name="tarhabil" attributesText="maxlength='15'  size='19' readonly class='texto_sinBorde'"/></td>
        <td ><span class="titulo">VENCE</span></td>
        <td class="detalle" ><input:text name="fecvenhabil" attributesText="maxlength='15'  size='19' readonly class='texto_sinBorde'"/></td>
      </tr>
      <tr>
        <td height="21"  class="titulo">CIUDAD</td>
        <td class="detalle"><input:text name="ciudadAfiliada" attributesText="maxlength='1'  readonly class='texto_sinBorde'"/></td>
        <td  class="titulo">TARJETA PROPIEDAD</td>
        <td class="detalle" ><input:text name="tarprop" attributesText="readonly  size='19' class='texto_sinBorde'"/></td>
        <td ><span class="titulo">VENCE</span></td>
        <td class="detalle" ><input:text name="fecvenprop" attributesText="readonly  size='19' class='texto_sinBorde'"/></td>
      </tr>
    </table>
    </td>
  </tr>
<tr>
    <td colspan="8" class="titulo" >    <table width="100%" border="1" align="center" cellpadding="0" cellspacing="0" bordercolor="#000000">
      <tr>
        <td colspan="7" class="titulo" align="center">INFORMACION DEL CONDUCTOR </td>
      </tr>
      <tr>
        <td width="240" rowspan="19" class="titulo"  align="center"><img src="<%=BASEURL%>/documentos/imagenes/<%=cond%>" width="241" height="180" align="middle"></td>
        <td width="123" class="titulo">NOMBRES</td>
        <td width="130" class="detalle" > <input name="Input" type="text" size="22"  readonly class="texto_sinBorde" value="<%=iden.getNom1()+" "+iden.getNom2()%>"></td>
        <td width="139" class="titulo" >CELULAR</td>
        <td width="109" class="detalle" ><span class="detalle"><%=iden.getCelular()%>&nbsp;</span></td>
        <td width="116"  class="titulo">GRUPO SANGUINEO</td>
        <td width="94" class="detalle" ><%=conductor.getRh()%>&nbsp;</td>
      </tr>
      <tr>
        <td class="titulo">APELLIDOS</td>
        <td class="detalle"><input name="Input2" type="text" size="22"  readonly class="texto_sinBorde" value="<%=iden.getApe1()+" "+iden.getApe2()%>"></td>
        <td class="titulo">CIUDAD/PAIS</td>
        <td class="detalle" ><%=(iden.getCodciu()!=null)?iden.getCodciu():""%> <%=(iden.getCodpais()!=null)?iden.getCodpais().substring(0,3):""%></td>
        <td  class="titulo">GRADO RIESGO </td>
        <td class="detalle" ><%=conductor.getGradoriesgo()%>&nbsp;</td>
      </tr>
      <tr>
        <td class="titulo">CEDULA</td>
        <td class="detalle"><%=iden.getCedula()%>&nbsp;</td>
        <td class="titulo">LIBRETA MILITAR </td>
        <td class="detalle" ><%=iden.getLibmilitar()%>&nbsp;</td>
        <td  class="titulo">LIBRETA TRIPULANTE </td>
        <td class="detalle" ><%=conductor.getNrolibtripulante()%>&nbsp;</td>
      </tr>
      <tr>
        <td class="titulo">LUGAR EXPEDICION </td>
        <td class="detalle"><%=iden.getExpced()%>&nbsp;</td>
        <td class="titulo">PASE</td>
        <td class="detalle"><%=conductor.getPassNo()%>&nbsp;</td>
        <td  class="titulo">EPS</td>
        <td class="detalle" ><%=conductor.getNomeps()%>&nbsp;</td>
      </tr>
      <tr>
        <td class="titulo">LUGAR NACIMIENTO </td>
        <td class="detalle">
          <%String lug =(iden!=null && iden.getLugarnac()!=null)?iden.getLugarnac():"";%><%=lug%>

       </td>
        <td class="titulo">RESTRICCION PASE </td>
        <td class="detalle"><%=conductor.getDesrespase()%>&nbsp;</td>
        <td  class="titulo">No. AFILIACION </td>
        <td class="detalle" ><%=conductor.getNroeps()%>&nbsp;</td>
      </tr>
      <tr>
        <td class="titulo">FECHA NACIMIENTO </td>
        <td class="detalle"><%=(!iden.getFechanac().equals("0099-01-01"))?iden.getFechanac():""%>&nbsp;</td>
        <td class="titulo">CATEGORIA PASE </td>
        <td class="detalle" ><%=conductor.getPassCat()%>&nbsp;</td>
        <td  class="titulo"> AFILIACION EPS </td>
        <td class="detalle"><%=(!conductor.getFecafieps().equals("0099-01-01"))?conductor.getFecafieps():""%>&nbsp;</td>
      </tr>
      <tr>
        <td class="titulo">ESTADO CIVIL</td>
        <td class="detalle"><%String estcivil =(iden!=null && iden.getEst_civil()!=null)?iden.getEst_civil():"";
                     if (estcivil.equals("S")){
					 	 estcivil ="SOLTERO";
				     }
                     else if (estcivil.equals("C")){ 
					     estcivil="CASADO";
				     }
                     else if (estcivil.equals("V")){
					     estcivil = "VIUDO";
					 }
			         else if (estcivil.equals("E")){
					      estcivil = "SEPARADO";
					 }
                     else if (estcivil.equals("U")){
					      estcivil = "UNION LIBRE";
					 }%><%=estcivil%>&nbsp;</td>
        <td class="titulo">VENCIMIENTO PASE </td>
        <td class="detalle" ><%=(!conductor.getPassExpiryDate().equals("0099-01-01"))?conductor.getPassExpiryDate():" "%></td>
        <td  class="titulo">ARP</td>
        <td class="detalle" ><%=(!conductor.getNomarp().equals("0099-01-01")?conductor.getNomarp():"")%>&nbsp;</td>
      </tr>
      <tr>
        <td class="titulo">DIRECCION</td>
        <td class="detalle"><%=iden.getDireccion()%>&nbsp;</td>
        <td class="titulo">CERTIFICADO JUDICIAL </td>
        <td class="detalle"><%=conductor.getNrojudicial()%>&nbsp;</td>
        <td  class="titulo"> AFILIACION ARP </td>
        <td class="detalle" ><%=(!conductor.getFecafiarp().equals("0099-01-01"))?conductor.getFecafiarp():""%>&nbsp;</td>
      </tr>
      <tr>
        <td  class="titulo">BARRIO</td>
        <td class="detalle"><%=iden.getBarrio()%>&nbsp;</td>
        <td class="titulo">PASAPORTE</td>
        <td class="detalle" ><%=conductor.getPassport()%>&nbsp;</td>
        <td  class="titulo">VENCE</td>
        <td class="detalle" ><%=(!conductor.getPassportExpiryDate().equals("0099-01-01"))?conductor.getPassportExpiryDate():""%>&nbsp;</td>
      </tr>
      <tr>
        <td height="20"  class="titulo">TELEFONO</td>
        <td class="detalle"><%String t11 =(iden!=null && iden.getPais1()!=null)?iden.getPais1():"";
	      String t12 =(iden!=null && iden.getArea1()!=null)?iden.getArea1():"";      
	      String t13 =(iden!=null && iden.getnum1()!=null)?iden.getnum1():"";
		  t11+=" "+t12+" "+t13;%> <%=t11%></td>
        <td class="titulo">VISA</td>
        <td class="detalle" ><%=conductor.getNrovisa()%>&nbsp;</td>
        <td class="titulo" >VENCE</td>
        <td class="detalle" ><%=(!conductor.getVencevisa().equals("0099-01-01"))?conductor.getVencevisa():""%>&nbsp;</td>
      </tr>
    </table></td>
  </tr>
<tr align="CENTER">
    <td colspan="8"  class="titulo"> 
       <table width="100%" border="1" cellpadding="0" cellspacing="0" bordercolor="#000000">
<tr>
    <td height="14" colspan="8"  class="titulo" align="center">INFORMACION DEL PROPIETARIO Y TENEDOR</td>
    </tr>
			<tr>
    <td height="14" colspan="4"  class="titulo" align="center">PROPIETARIO</td>
    <td height="14" colspan="4"  class="titulo" align="center">TENEDOR</td>
  </tr>
  <tr>
    <td width="108" height="20"  class="titulo">NOMBRES</td>
    <td width="112" class="detalle"><input name="" type="text" size="22"  readonly class="texto_sinBorde" value="<%=propietario.getNom1()+" "+propietario.getNom2()%>"></td>
    <td width="113" class="titulo">CIUDAD/PAIS</td>
    <td width="140" class="detalle"><%=propietario.getCiudad()%> <%=propietario.getPais().substring(0,3)%></td>
    <td width="118" height="20"  class="titulo">NOMBRES</td>
    <td width="102" class="detalle">
    <input name="Input232" type="text" size="22"  class="texto_sinBorde" readonly value="<%=tenedor.getNom1()+" "+tenedor.getNom2()%>"></td>
    <td width="111" class="titulo">CIUDAD/PAIS</td>
    <td width="145" class="detalle"><%=tenedor.getCiudad()%> <%=tenedor.getPais().substring(0,3)%></td>
  </tr>
  <tr>
    <td height="20"  class="titulo">APELLIDOS</td>
    <td class="detalle"><input name="Input22" type="text" size="22"  class="texto_sinBorde" readonly value="<%=propietario.getApe1()+" "+propietario.getApe2()%>"> </td>
    <td class="titulo">TELEFONO</td>
    <td class="detalle"><%=propietario.getTelefono()%>&nbsp;</td>
    <td height="20"  class="titulo">APELLIDOS</td>
    <td class="detalle">      
      <input name="Input23" type="text" size="22"  class="texto_sinBorde" readonly value="<%=tenedor.getApe1()+"  "+tenedor.getApe2()%>"></td>
    <td class="titulo">TELEFONO</td>
    <td class="detalle"><%=tenedor.getTelefono()%>&nbsp;</td>
  </tr>
  <tr>
    <td height="20"  class="titulo">CEDULA</td>
    <td class="detalle"><%=propietario.getC_nit()%>&nbsp;</td>
    <td class="titulo">CELULAR</td>
    <td class="detalle"><%=propietario.getCelular()%>&nbsp;</td>
    <td height="20"  class="titulo">CEDULA</td>
    <td class="detalle"><%=tenedor.getCedula()%>&nbsp;</td>
    <td class="titulo">CELULAR</td>
    <td class="detalle"><span class="detalle"><%=tenedor.getCelular()%>&nbsp;</span></td>
  </tr>
<tr>
    <td height="20"  class="titulo">LUGAR EXPEDICION </td>
    <td class="detalle"><%=propietario.getExpced()%>&nbsp;</td>
    <td class="titulo">SEDE PAGO </td>
    <td class="detalle"><%=propietario.getC_agency_id()%>&nbsp;</td>
    <td height="20"  class="titulo">LUGAR EXPEDICION </td>
    <td class="detalle"><%=tenedor.getExpedicionced()%>&nbsp;</td>
    <td class="titulo">&nbsp;</td>
    <td class="detalle">&nbsp;</td>
  </tr>
<tr>
    <td height="20"  class="titulo">DIRECCION</td>
    <td class="detalle"><%=propietario.getDireccion()%>&nbsp;</td>
    <td class="titulo">GRADO RIESGO </td>
    <td class="detalle">&nbsp;</td>
    <td height="20"  class="titulo">DIRECCION</td>
    <td class="detalle"><%=tenedor.getDireccion()%>&nbsp;</td>
    <td class="titulo">&nbsp;</td>
    <td class="detalle">&nbsp;</td>
  </tr>
       </table>
    </td>
  </tr>

<tr>
    <td height="21" colspan="8"  class="titulo"> 
<table width="100%" height="91"  border="1"  align="center" cellpadding="0" cellspacing="0" bordercolor="#000000" >
<tr align="center">
      <td height="14" colspan="9" align="center" class="titulo">CONTROL DE REGISTRO</td>
      </tr>
    <tr>
      <td width="130" height="20" align="right" class="titulo"><div align="center">FECHA DE CREACION</div></td>
      <td width="86" class="detalle"><input:text name="fechacrea" attributesText="readonly size='10' class='texto_sinBorde'"/></td>
      <td width="50" class="titulo">USUARIO</td>
      <td width="93" class="detalle"><input:text name="usuariocrea" attributesText="readonly size='15' class='texto_sinBorde'"/></td>
      <td width="44" class="titulo">AGENCIA</td>
      <td width="93" class="detalle"><%=(!hv.getAgenUsuarioCrea().equals(""))? hv.getAgenUsuarioCrea() :""%>&nbsp;</td>
      <td width="152" class="titulo" align="center" nowrap>HUELLA 1 <%=leyenda1%></td>
      <td width="158" class="titulo" align="center" nowrap>HUELLA 2 <%=leyenda2%></td>
      <td width="140" class="titulo" align="center"><span class="filaresaltada">FIRMA CONDUCTOR </span></td>
    </tr>
    <tr class="detalle" height="60">
      <td height="49" class="titulo"><div align="right" class="filaresaltada">
        <div align="center">FECHA DE ACTUALIZACION </div>
      </div></td>
      <td class="detalle"><input:text name="fechaultact" attributesText="readonly size='10' class='texto_sinBorde'"/></td>
      <td class="titulo">USUARIO</td>
      <td class="detalle"><input:text name="usuario" attributesText="readonly size='15' class='texto_sinBorde'"/></td>
      <td class="titulo">AGENCIA</td>
      <td class="detalle"><%=hv.getAgenUsuarioMod()%>&nbsp;</td>
      <td align="center"><img src="<%=BASEURL%>/documentos/imagenes/<%=huella%>" width="45" height="50"></td>
      <td align="center"><img src="<%=BASEURL%>/documentos/imagenes/<%=huella2%>" width="45" height="50"></td>
      <td align="center"><span class="filaresaltada"><img src="<%=BASEURL%>/documentos/imagenes/<%=firma%>" width="56" height="37"></span></td>
    </tr>
  </table>
    </td>
  </tr>
</table> 
<script>
alert("No olvide configurar su impresora a modo de impresion HORIZONTAL!");
window.print();
</script>
</input:form>
</body>
</html>
