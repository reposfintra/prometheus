<%-- 
    Document   : controlCuentasAp
    Created on : 22/03/2016, 08:56:26 AM
    Author     : egonzalez
--%>

<%@page contentType="text/html" pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
        <link href="./css/popup.css" rel="stylesheet" type="text/css">
        <link type="text/css" rel="stylesheet" href="./css/jquery/jquery-ui/jquery-ui.css" />
        <link type="text/css" rel="stylesheet" href="./css/TransportadorasApi.css" />
        <script type="text/javascript" src="./js/jquery/jquery.jqGrid/js/jquery.min.js"></script>
        <script type="text/javascript" src="./js/jquery-ui-1.8.5.custom.min.js"></script>   
        <script type="text/javascript" src="./js/jquery/jquery.jqGrid-4.7.0/js/grid.locale-es.js"></script>
        <script type="text/javascript" src="./js/jquery/jquery.jqGrid-4.7.0/js/jquery.jqGrid.min.js"></script>
        <script type="text/javascript" src="./js/jquery/jquery.jqGrid-4.7.0/js/jquery.tabletojson.js"></script>    
        <script type="text/javascript" src="./js/ControlCuentasAp.js"></script> 

        <title>CONTROL CUENTAS APLICACION PAGO</title>
    </head>
    <body>
        <div id="capaSuperior" style="position:absolute; width:100%; height:100px; z-index:0; left: 0px; top: 0px;">
            <jsp:include page="/WEB-INF/toptsp.jsp?encabezado=CONTROL CUENTAS APLICACION PAGO"/>
        </div>
        <style>
            .onoffswitch-inner:before {
                content: "A";
                padding-left: 14px;
                background-color: #34A7C1; color: #FFFFFF;
            }
            .onoffswitch-inner:after {
                content: "I";
                padding-right: 11px;
                background-color: #C20F0F; color: #F5F0F0;
                text-align: right;
            }
        </style>
    <center>
        <div style="position: relative;top: 231px;">
            <table id="tabla_control_cuentas_ap" ></table>
            <div id="pager"></div>
        </div>
        <div id="info"  class="ventana" >
            <p id="notific">EXITO AL GUARDAR</p>
        </div>
        <div id="dialogControlCuentas"  class="ventana" >
            <div id="tablainterna" style="width: 341px;" >
                <table id="tablainterna"  >
                    <tr>
                        <td>
                            <input type="text" id="id" style="width: 137px;color: black;" hidden>
                            <input type="text" id="cod_controlcuentas" style="width: 137px;color: black;" hidden>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <label>Tipos negocio</label>
                            <select id="tiponeg" style="width: 180px"></select>
                        </td>
                    </tr>

                    <tr>
                        <td>
                            <div style="margin-top:  20px"><label>Asignacion de cuentas</label></div>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <hr style="color: #8FBFDF;">
                        </td>
                    </tr>

                    <tr>
                        <td>
                            <label>Cta Interes x Mora </label>
                        </td>
                    </tr>
                    <tr></tr>
                    <tr>
                        <td>
                            <div id="config1">
                                <input type="checkbox"  id="cmc1"  style="margin-right: -37px;margin-left: -29px;" onchange="valor(this.id)"/>
                                <label>Cmc factura</label>
                                <input type="checkbox" id="cuenta1"   style="margin-right: -37px;"  onchange="valor(this.id)" >
                                <label>Digitar cuenta</label>
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <input type="text" id="ixm" style="width: 137px;color: black;">
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <div style="margin-top:  20px"></div>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <label>Cta Gasto a Cobranza </label>
                        </td>
                    </tr>
                    <tr></tr>
                    <tr>
                        <td>
                            <div id="config2">
                                <input type="checkbox" id="cmc2" style="margin-right: -37px;margin-left: -29px;"onchange="valor(this.id)">
                                <label>Cmc factura</label>
                                <input type="checkbox" id="cuenta2" style="margin-right: -37px;" onchange="valor(this.id)">
                                <label>Digitar cuenta</label>
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <input type="text" id="gac" style="width: 137px;color: black;">
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <div style="margin-top:  20px"></div>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <label>Cta Cabecera Ingreso </label>
                        </td>
                    </tr>
                    <tr></tr>
                    <tr>
                        <td>
                            <div id="config3">
                                <input type="checkbox" id="cmc3" style="margin-right: -37px;margin-left: -29px;"onchange="valor(this.id)">
                                <label>Cmc factura</label>
                                <input type="checkbox" id="cuenta3" style="margin-right: -37px;" onchange="valor(this.id)">
                                <label>Digitar cuenta</label>
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <input type="text" id="cabing" style="width: 137px;color: black;">
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <div style="margin-top:  20px"></div>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <label>Cta Cabecera Ingreso </label>
                        </td>
                    </tr>
                    <tr></tr>
                    <tr>
                        <td>
                            <div id="config4">
                                <input type="checkbox" id="cmc4" style="margin-right: -37px;margin-left: -29px;"onchange="valor(this.id)">
                                <label>Cmc factura</label>
                                <input type="checkbox" id="cuenta4" style="margin-right: -37px;" onchange="valor(this.id)">
                                <label>Digitar cuenta</label>
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <input type="text" id="deting" style="width: 137px;color: black;">
                        </td>
                    </tr>
                </table>
            </div>
        </div>
    </center>
</body>
</html>
