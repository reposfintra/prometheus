<%-- 
    Document   : Scoring
    Created on : 12/04/2016, 02:47:09 PM
    Author     : user
--%>

<%@page contentType="text/html" pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
        <title>Scoring</title>
        <link type="text/css" rel="stylesheet" href="./css/jquery/jquery-ui/jquery-ui.css"/>     
        <script type="text/javascript" src="./js/jquery/jquery-ui/jquery.min.js"></script>
        <script type="text/javascript" src="./js/jquery/jquery-ui/jquery.ui.min.js"></script>  
<!--        <link href="./css/style_azul.css" rel="stylesheet" type="text/css">-->
        <link type="text/css" rel="stylesheet" href="./css/buttons/botonVerde.css"/>
        <link href="./css/scoring.css" rel="stylesheet" type="text/css">
        <script type="text/javascript" src="./js/Auto_Scoring.js"></script> 
    </head>
    <body>
        <div id="capaSuperior" style="position:absolute; width:100%; height:100px; z-index:0; left: 0px; top: 0px;">
            <jsp:include page="/toptsp.jsp?encabezado=Scoring"/>
        </div>
        <div id="capaCentral" align="left" style="position:absolute; width:100%; height:100%; z-index:0; top: 110px;" >
            <center>
                <h4 style="width:450px">CONSOLIDADO MODELOS</h4>
                <div id="div_search_scoring"> 
                    <table>                          
                        <tr>
                            <td  class="td">
                                <label>Unidad de Negocio</label> 
                            </td>
                            <td  class="td">
                                <select id="idProMeta"></select>
                            </td>                           
                            <td  class="td">
                                <button id="btn_show_scoring" class="ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only" >
                                    <span class="ui-button-text">Buscar</span>
                                </button>      
                                <button id="btn_clear" class="ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only" 
                                        role="button" aria-disabled="false">
                                    <span class="ui-button-text">Limpiar</span>
                                </button>
                            </td>  
                        </tr>  
                    </table>                              
                </div>
                <br><br>
                               
                <table id="tbl_scoring" class="tablas" align="center" style=" width: 50%">

                </table>  
                </br> 
                <table id="tbl_tot_scoring" class="tablas" align="center" style=" width: 50%">

                </table>   
                <div id="botones_footer" style="display:none "> 
                    <span aling="center" id="btn_guardar"  class="form-submit-button form-submit-button-simple_green_apple"> Guardar </span>&nbsp;
                    <span aling="center" id="btn_salir" class="form-submit-button form-submit-button-simple_green_apple"> Salir </span>
                </div>
            </center>
            <div id="dialogLoading" style="display:none;">
                <p  style="font-size: 12px;text-align:justify;" id="msj2">Texto </p> <br/>
                <center>
                    <img src="./images/cargandoCM.gif"/>
                </center>
            </div>
            <div id="dialogMsgMeta" title="Mensaje" style="display:none; visibility: hidden">
                <p style="font-size: 12.5px;text-align:justify;" id="msj" > Texto </p>
            </div>  
        </div>       
    </body>
</html>
