<%-- 
    Document   : anular_ingresos
    Created on : Jun 19, 2019, 3:51:50 PM
    Author     : jdbc
--%>

<%@page contentType="text/html" pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="ISO-8859-1">
        <link href="./css/popup.css" rel="stylesheet" type="text/css">
        <link type="text/css" rel="stylesheet" href="./css/jquery/jquery-ui/jquery-ui.css"/>
        <link type="text/css" rel="stylesheet" href="./css/TransportadorasApi.css "/>
        <script type="text/javascript" src="./js/jquery/jquery.jqGrid/js/jquery.min.js"></script>
        <script type="text/javascript" src="./js/jquery-ui-1.8.5.custom.min.js"></script>
        <script type="text/javascript" src="./js/jquery/jquery.jqGrid-4.7.0/js/jquery.jqGrid.min.js"></script>
        <script type="text/javascript" src="./js/jquery/jquery.jqGrid-4.7.0/js/grid.locale-es.js"></script>
        <script type="text/javascript" src="./js/jquery/jquery.jqGrid-4.7.0/js/jquery.tabletojson.js"></script>    
        <script type="text/javascript" src="./js/jquery/jquery.jqGrid-4.7.0/plugins/jquery.contextmenu.js"></script>            
        <title>ANULAR INGRESOS</title>
        <style>
            html {
                font-size: 16px;
            }

            input {
                width: auto;
                margin: 0;
            }

            #negocio, #unegocio {
                padding-left: 0;
                padding-right: 0;
            }

            .button-wrapper {
                display: inline-block;
                margin-top: 10px;
                margin-left: 15px;
            }

            button {
                width: 116px;
            }

            #tablaIngresosWrapper {
                position: relative;
                top: 140px;
                width: 90%;
            }
        </style>
    </head>
    <body>
        <div id="capaSuperior" style="position:absolute; width:100%; height:100px; z-index:0; left: 0px; top: 0px;">
            <jsp:include page="/WEB-INF/toptsp.jsp?encabezado=ANULAR INGRESOS"/>
        </div>
    <center>
        <div id="tablita" class="ui-jqgrid ui-widget ui-widget-content center-block" style="width: 300px; padding: 8px;">
            <div class="ui-dialog-titlebar ui-widget-header ui-helper-clearfix">
                <label class="titulotablita"><b>FILTRO</b></label>
            </div>
            <table id="tablainterna" style="height: 53px;">
                <br>
                <tr>                        
                    <td style="text-align: center;">
                        <label>N�mero de ingreso</label>
                    </td>
                    <td>
                        <input id="numIngreso" type="text">
                    </td>                    
                </tr>
                <tr>
                    <td>
                        <div class="button-wrapper">
                            <center>
                                <button type="button" id="buscar" class="ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only"> 
                                    <span class="ui-button-text">Buscar</span>
                                </button>  
                            </center>
                        </div>
                    </td>
                    <td>
                        <div class="button-wrapper">
                            <center>
                                <button type="button" id="salir" class="ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only" > 
                                    <span class="ui-button-text">Salir</span>
                                </button>  
                            </center>
                        </div>
                    </td>
                </tr>
            </table>
        </div>
        <div id="tablaIngresosWrapper">
            <table id="tablaIngresos"></table>
            <div id="pager"></div>
        </div>
        <div id="dialogLoading" style="display:none;">
            <p style="font-size: 12px;text-align:justify;" id="msj2">Texto</p>
            <br/>
            <center>
                <img src="./images/cargandoCM.gif"/>
            </center>
        </div>
    </center>
    <div id="dialogMsg" title="Mensaje" style="display:none;">
        <p style="font-size: 12.5px;text-align:justify;" id="msj">Cargando la informaci�n</p>
    </div> 
    <script src="./js/anular_ingresos.js"></script>
</body>
</html>
