<%-- 
    Document   : creacionIngreso
    Created on : 2/11/2016, 04:55:57 PM
    Author     : mariana
--%>

<%@page contentType="text/html" pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>

    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
        <link href="./css/popup.css" rel="stylesheet" type="text/css">
        <link type="text/css" rel="stylesheet" href="./css/jquery/jquery-ui/jquery-ui.css" />
        <link type="text/css" rel="stylesheet" href="./css/TransportadorasApi.css" />
        <script type="text/javascript" src="./js/jquery/jquery.jqGrid/js/jquery.min.js"></script>
        <script type="text/javascript" src="./js/jquery/jquery-ui/jquery.ui.min.js"></script>
        <script type="text/javascript" src="./js/jquery-ui-1.8.5.custom.min.js"></script>   
        <script type="text/javascript" src="./js/jquery/jquery.jqGrid-4.7.0/js/grid.locale-es.js"></script>
        <script type="text/javascript" src="./js/jquery/jquery.jqGrid-4.7.0/js/jquery.jqGrid.min.js"></script>
        <script type="text/javascript" src="./js/jquery/jquery.jqGrid-4.7.0/js/jquery.tabletojson.js"></script>  
        <script type="text/javascript" src="./js/creacionIngreso.js"></script> 
        <script type="text/javascript" href="http://www.ddginc-usa.com/spanish/editcheck.js"></script>
        <!-- <link href="./css/style_azul.css" rel="stylesheet" type="text/css"> -->
        <title>CREACION INGRESO</title>
    </head>

    <style>
        #division{
            background-color: #bda8a8b3;
            border: 1px solid #96aaaa;
            width: 1642px;
            height: 6px;
            margin-top: 199px;
        }
        .cbox{
            width: 10px;
        }
        #cb_tabla_ingreso_detalle{
            display: none;
        }
    </style>


    <body>

        <div id="capaSuperior" style="position:absolute; width:100%; height:100px; z-index:0; left: 0px; top: 0px;">
            <jsp:include page="/WEB-INF/toptsp.jsp?encabezado=CREACION INGRESO"/>
        </div>
    <center>
        <div id="capaCentral" style="position:absolute; width:100%; height:85%; z-index:0; left: 0px; top: 110px; overflow: scroll; background-color:white" >

            <table width="80%" border="0" cellpadding="0" cellspacing="1" class="labels" id="tbl_hys" >
                <tr>
                    <td>
                        <fieldset><legend class="labels">INGRESO</legend>
                            <table align="center" id="tablainterna" style="height: 53px; width: 1165px" border="0">
                                <tr><td class="subtitulo1" id="titulo" widht="100%" colspan="10">Informacion Ingreso</td></tr>
                                <tr>
                                    <td>
                                        <label>Tipo Ingreso</label>
                                    </td>
                                    <td>
                                        <select id="tipo_ingreso" style="width: 133px;" onchange="mostrarCampos()" >
                                        </select>
                                    </td>
                                    <td>
                                        <label>Cliente</label>
                                    </td>
                                    <td>
                                        <input id="cliente" type="text"  style="width: 110px;">
                                    </td>
                                    <td>
                                        <img  src='/fintra/images/botones/iconos/search.png' style="width: 32px;height: 28px;"id='buscar_cliente' />
                                    </td>
                                    <td>
                                        <label>Banco</label>
                                    </td>
                                    <td>
                                        <select id="banco" style="width: 133px;" onchange="cargarSucursal()">
                                        </select>
                                    </td>
                                    <td>
                                        <label>Sucursal</label>
                                    </td>
                                    <td>
                                        <select id="sucursal" style="width: 133px;">
                                        </select>
                                    </td>
                                    <td>
                                        <label id="labelcuenta" style="display: none">Cuenta</label>
                                    </td>
                                    <td>
                                        <input id="cuenta" type="text" style="width: 110px;display: none">
                                    </td>
                                    <td rowspan="4">
                                        <table border="0">
                                            <tr>
                                                <td>

                                                </td>
                                            </tr>
                                        </table>
                                    </td> 
                                </tr>
                                <tr>
                                    <td>
                                        <label>Fecha Consignacion</label>
                                    </td>
                                    <td>
                                        <input id="f_consignacion" type="text"  style="width: 117px;">
                                    </td>
                                    <td>
                                        <label>Valor Consignacion</label>
                                    </td>
                                    <td>
                                        <input id="v_consignacion" type="text"  style="width: 110px;"class="solo-numeric" onkeyup="numberConComas2(this)"> 
                                    </td>
                                    <td></td>
                                    <td>
                                        <label>Moneda</label>
                                    </td>
                                    <td>
                                        <select id="moneda" style="width: 133px;">
                                        </select>
                                    </td>
                                    <td>
                                        <label>Concepto</label>
                                    </td>
                                    <td>
                                        <select id="concepto" style="width: 133px;">
                                        </select>
                                    </td>
                                    <td>
                                        <label id="labelhc" style="display: none">Hc</label>
                                    </td>
                                    <td>
                                        <select id="hc"  style="width: 133px; display: none">
                                        </select>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <label>Descripcion</label>
                                    </td>
                                    <td colspan="6">
                                        <textarea id="descripcion" cols="106" rows="34" style="width: 610px;height: 60px;"></textarea>
                                    </td>
                                    <td>
                                        <label>Numero Extracto</label>
                                    </td>
                                    <td>
                                        <input id="num_extracto" type="text"  style="width: 110px;">
                                    </td>
                                </tr> 
                            </table>

                        </fieldset>

                    </td>
                </tr>  

                <tr>
                    <td width="1000" align="right" >
                        <fieldset>
                            <legend>DETALLE INGRESO</legend>

                            <div id="fondo4" style="position:relative; width:1630px; height:600px; visibility: visible; overflow:auto;" align="center">

                                <div id="xxxxx" align="center" style="position:absolute; z-index:58; left:0px; top:0px; visibility:visible;">

                                    <table id="tablainterna" width="100%" border="0" >

                                        <tr>
                                            <td width="20%" align="left"> <!-- class="subtitulo1" -->
                                                <table>

                                                    <td>
                                                        <div style="padding-top: 10px">
                                                            <center>
                                                                <button id="ajustar_ingreso" class="ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only" style="margin-left: 15px;top: -7px;height: 30px;" > 
                                                                    <span class="ui-button-text">Ajustar Ingreso</span>
                                                                </button>  
                                                            </center>
                                                        </div>
                                                    </td>
                                                    <td><hr style="width: 100%;height: 25px;top: -25px;color: rgba(128, 128, 128, 0.39);" /></td>
                                                    <td>
                                                        <div style="padding-top: 10px">
                                                            <center>
                                                                <button id="agregar_items" class="ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only" style="margin-left: 15px;top: -7px;height: 30px;" > 
                                                                    <span class="ui-button-text">Agregar Items</span>
                                                                </button>  
                                                            </center>
                                                        </div>
                                                    </td> 
                                                </table>
                                            </td>
                                            <td width="80%" align="center"></td>   <!-- class="subtitulo1" --> 
                                        </tr>
                                        <tr><td colspan="2"><div id="botones" style="margin-left: 8px;"><hr style="width: 100%;height: 1px;top: -25px;color: rgba(128, 128, 128, 0.39);" /> </div></td></tr>
                                        <tr>
                                            <td colspan="2" width="100%">
                                                <table id="tabla_ingreso_detalle" width="100%"></table>
                                                <div id="pager1"></div>
                                            </td>   
                                        </tr>
                                    </table>                                        
                                </div>
                            </div>
                        </fieldset>
                    </td>
                </tr>
            </table>
        </div>
        <div id="dialogMsjAddFacturas" class="ventana" >
            <table id="tablainterna" style="height: 53px; width: 273px"  >
                <tr>
                    <td>
                        <label>Cliente</label>
                    </td>
                    <td>
                        <input id="cliente_" type="text"  style="width: 110px;" value="1129522267">
                    </td>
                    <td>
                        <img  src='/fintra/images/botones/iconos/search.png' style="width: 32px;height: 28px;"id='buscar_cliente_' />
                    </td>
                    <td>
                        <hr style="width: 100%;height: 25px;top: -25px;color: rgba(128, 128, 128, 0.39);" />
                    </td>
                    <td>
                        <div style="padding-top: 10px">
                            <center>
                                <button id="buscar" class="ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only" style="margin-left: 15px;top: -7px;height: 30px;" > 
                                    <span class="ui-button-text">Buscar</span>
                                </button>  
                            </center>
                        </div>
                    </td> 
                </tr>
            </table>
            <div>
                <hr style="width: 100%;height: 2px;top: -25px;color: rgba(128, 128, 128, 0.39);" />
            </div>
            <div style="position: relative;top: 20px;">
                <table id="tabla_facturas" ></table>
                <div id="pager2"></div>
            </div>
        </div>
        <div id="dialogMsjCliente" class="ventana">
            <table id="tablainterna" >
                <tr>
                    <td>
                        <label>Buscar por:</label>
                    </td>
                    <td>
                        <select id="tipo_busqueda_">
                            <option value="nit">Nit</option>
                            <option value="nombre">Nombre</option>
                        </select>
                    </td>
                    <td>
                        <label>Cliente</label>
                    </td>
                    <td>
                        <input type="text" id="cliente_busqueda"  style="width: 290px" >
                    </td>
                </tr>
            </table>
        </div>
    </center>
</body>
<script>
    $(document).ready(function () {
        $('.solo-numero').keyup(function () {
            this.value = (this.value + '').replace(/[^0-9]/g, '');
        });

        $('.solo-numeric').live('blur', function (event) {
            this.value = numberConComas(this.value);
        });

        function numberConComas(x) {
            return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
        }
    });
</script>
</html>
