<%-- 
    Document   : reliquidacionLibranza
    Created on : 6/03/2017, 03:16:27 PM
    Author     : mariana
--%>

<%@page contentType="text/html" pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
        <link href="./css/popup.css" rel="stylesheet" type="text/css">
        <link type="text/css" rel="stylesheet" href="./css/jquery/jquery-ui/jquery-ui.css" />
        <link type="text/css" rel="stylesheet" href="./css/TransportadorasApi.css " />
        <script type="text/javascript" src="./js/jquery/jquery.jqGrid/js/jquery.min.js"></script>
        <script type="text/javascript" src="./js/jquery-ui-1.8.5.custom.min.js"></script>   
        <script type="text/javascript" src="./js/jquery/jquery.jqGrid-4.7.0/js/grid.locale-es.js"></script>
        <script type="text/javascript" src="./js/jquery/jquery.jqGrid-4.7.0/js/jquery.jqGrid.min.js"></script>
        <script type="text/javascript" src="./js/jquery/jquery.jqGrid-4.7.0/js/jquery.tabletojson.js"></script>   
        <script type="text/javascript" src="./js/utilidadInformacion.js"></script> 
        <script type="text/javascript" src="./js/reliquidacionLibranza.js"></script> 
        <title>RELIQUIDAR LIBRANZA</title>
    </head>
    <body>
        <div id="capaSuperior" style="position:absolute; width:100%; height:100px; z-index:0; left: 0px; top: 0px;">
            <jsp:include page="/WEB-INF/toptsp.jsp?encabezado=RELIQUIDAR LIBRANZA"/>
        </div>
    <center>
        <div id="tablita" class="ui-jqgrid ui-widget ui-widget-content center-block" style="width: 359px; margin-top: 80px;margin-left: 60px;padding: 8px;">
            <div class="ui-dialog-titlebar ui-widget-header ui-helper-clearfix">
                <label class="titulotablita"><b>LIQUIDADOR</b></label>
            </div>
            <table id="tablainterna" style="height: 53px; width: 502px"  >
                <tr>
                    <td>
                        <label>Busqueda</label>
                        <select id="busqueda" style="margin-left: 10px;">
                            <option value="NEG">NEGOCIO</option>
                            <option value="CED">CEDULA</option>
                        </select>
                    </td>
                    <td>
                        <input id="formalario" type="text"  style="width: 134px;"  hidden>
                        <input id="valor_compra" type="text"  style="width: 134px;"  hidden>
                        <input id="cant_obligaciones" type="text"  style="width: 134px;"  hidden>
                        <input id="ocupacion" type="text"  style="width: 134px;"  hidden>
                        <input id="tipo" type="text"  style="width: 134px;"  hidden>
                        <input id="fianza" type="text"  style="width: 134px;"  hidden>
                        
                        <input id="dato" type="text"  style="width: 134px;" class="requerido" >
                    </td>
                    <td>
                        <img  src='/fintra/images/botones/iconos/search.png'style="width: 35px;margin-right: 135px;margin-left: -21px;" id='buscar_form'  title ='Buscar el Formulario'/>
                    </td>
                </tr>
                <tr>
                    <td>
                        <label>Convenio</label>
                    </td>
                    <td>
                        <!--<select id="convenio" style="width: 150px;height: 22px"></select>-->
                        <input id="convenio" type="text"  style="width: 134px;" disabled class="requerido">
                    </td>
                </tr>
                <tr>
                    <td>
                        <label>Convenio de Libranza</label>
                    </td>
                    <td>
                        <select id="convenio_l" style="width: 150px;height: 22px" onchange="cargarEmpresasLibranza()"></select>
                    </td>
                </tr>
                <tr>
                    <td>
                        <label>Empresas</label>
                    </td>
                    <td>
                        <select id="empresas" style="width: 150px;height: 22px"></select>
                    </td>
                </tr>
                <tr>
                    <td>
                        <label>Cliente</label>
                    </td>
                    <td>
                        <input id="cliente" type="text"  style="width: 134px;" disabled class="requerido">
                    </td>
                </tr>
                <tr>
                    <td>
                        <label>Tipo Cuota</label>
                    </td>
                    <td>
                        <select id="tipo_cuota" style="width: 150px;height: 22px" class="requerido"></select>
                    </td>
                </tr>
                <tr>
                    <td>
                        <label>Nro cuotas</label>
                    </td>
                    <td>
                        <input id="num_cuotas" type="text"  style="width: 134px;" class="requerido">
                    </td>
                </tr>
                <tr>
                    <td>
                        <label>Valor a Financiar</label>
                    </td>
                    <td>
                        <input id="valor_financiar" type="text"  style="width: 134px;"onkeyup="formato.numberConComasKeyup(this)" class="requerido">
                    </td>
                </tr>
                <tr>
                    <td>
                        <label hidden>Fecha Negocio</label>
                    </td>
                    <td>
                        <input id="fecha_negocio" type="text"  style="width: 134px;" hidden>
                    </td>
                </tr>
                <tr>
                    <td>
                        <label>Fecha Primera cuota</label>
                    </td>
                    <td>
                        <input id="Fecha_primera_cuota" type="text"  style="width: 134px;"readonly class="requerido">
                    </td>
                </tr>
                <tr>
                    <td>
                        <label>Tipo Titulo Valor</label>
                    </td>
                    <td>
                        <select id="tipo_titulo_valor" style="width: 150px;height: 22px" class="requerido"></select>
                    </td>
                </tr>
                <tr>
                    <td colspan="3">
                        <hr style="width: 345px;height: 2px;margin-left: 3px;color: rgba(128, 128, 128, 0.39);" /> 
                    </td>

                </tr>
                <tr>
                    <td colspan="3">
                        <div style="padding-top: 10px">
                            <center>
                                <button type="button" id="aceptar" class="ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only" style="margin-right: 118px;height: 29px;width: 88px;" > 
                                    <span class="ui-button-text">Aceptar</span>
                                </button>  
                            </center>
                        </div>
                    </td>
                </tr>
            </table>
        </div>
        <div id="dialogMsjDetalle" style="position: relative;" hidden="true">
            <table id="tabla_infoLibranza" ></table>
            <div id="pager1"></div>
        </div>
        <div id="info"  class="ventana" >
            <p id="notific">EXITO AL GUARDAR</p>
        </div>
        <div id="dialogMsjVentanaReliq" style="position: relative;" hidden="true">
            <center>
                <table id="tablainternaLineas" class="tablainternaLineas" style="height: 53px; width: 440px;margin-top: 28px;" >
                    <th colspan="2">Informacion del negocio</th>
                    <tr>
                        <td>
                            <label>Valor del negocio</label>
                        </td>
                        <td>
                            <label id="val_neg"></label>
                            <!--<input id="val_neg" type="text"  style="width: 134px;height: 10px">-->
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <label>Tasa Interes</label>
                        </td>
                        <td>
                            <label id="tasa_interes"></label>
                            <!--<input id="tasa_interes" type="text"  style="width: 134px;height: 10px">-->
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <label>No Cuotas</label>
                        </td>
                        <td>
                            <label id="no_cuotas"></label>
                            <!--<input id="no_cuotas" type="text"  style="width: 134px;height: 10px">-->
                        </td>
                    </tr>
                    <!--                    <tr>
                                            <td>
                                                <label>Procentaje Comision Ley mipyme</label>
                                            </td>
                                            <td>
                                                <label id="porc_comision_ley"></label>
                                                <input id="porc_comision_ley" type="text"  style="width: 134px;height: 10px">
                                            </td>
                                        </tr>-->
                    <tr>
                        <td>
                            <label>Fecha Negocio</label>
                        </td>
                        <td>
                            <label id="fecha_neg"></label>
                            <!--<input id="fecha_neg" type="text"  style="width: 134px;height: 10px">-->
                        </td>
                    </tr>
                    <!--                    <tr>
                                            <td>
                                                <label>Fecha Liquidacion</label>
                                            </td>
                                            <td>
                                                <label id="fecha_liq"></label>
                                                <input id="fecha_liq" type="text"  style="width: 134px;height: 10px">
                                            </td>
                                        </tr>-->
                    <tr>
                        <td>
                            <label>Valor Fianza</label>
                        </td>
                        <td>
                            <label id="vlr_fianza"></label>
                            <!--<input id="fecha_liq" type="text"  style="width: 134px;height: 10px">-->
                        </td>
                    </tr>
                </table>

                <div style="margin-top: 36px;">
                    <table id="tabla_reliquidacion"></table>
                    <div id="pager2"></div>
                </div>
            </center>
        </div>
    </center>
</body>
</html>
