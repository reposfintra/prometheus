<%-- 
    Document   : mostrarCuentasProveedor
    Created on : 15/11/2007, 03:58:14 PM
    Author     : iamorales
--%>

<%@ page session   ="true"%> 
<%@ page errorPage ="/error/ErrorPage.jsp"%>
<%@ page import    ="java.util.*" %>
<%@ page import    ="com.tsp.util.*" %>
<%@ page import    ="com.tsp.operation.model.beans.*" %>
<%@ include file   ="/WEB-INF/InitModel.jsp"%>
<%
	String path = application.getRealPath(model.menuService.getRealUrl(request.getRequestURI()));
	String datos[] = model.menuService.getContenidoMenu(BASEURL,request.getRequestURI(),path);
        
        String idAnticipoGasolina;
        idAnticipoGasolina=(String)request.getParameter("idAnticipoGas");
		String loginx="";
		loginx=(String)request.getParameter("loginx");
		
        AnticipoGasolina anticipoGasolina=(AnticipoGasolina)model.AnticiposGasolinaService.getAnticipoGasolina(idAnticipoGasolina,loginx);
                                                                        
        String valor=""+anticipoGasolina.getVlr();//request.getParameter("valor");
		
		double tot_preant=Double.parseDouble(request.getParameter("tot_preant"));

        String valor_neto=""+Util.roundByDecimal(anticipoGasolina.getVlrNeto()-tot_preant,0);//request.getParameter("valor_neto");        
        //String valor_neto=""+Util.roundByDecimal(anticipoGasolina.getVlrNeto(),0);//request.getParameter("valor_neto");
        
        //System.out.println("valor"+valor+"valor_neto"+valor_neto);
%>
<html>
<head>
	<title>Gesti�n de Anticipos de Estaciones de Gasolina</title>
    <link href="<%= BASEURL %>/css/estilostsp.css"     rel="stylesheet" type="text/css"> 
    <link href="/css/estilostsp.css" rel="stylesheet"  type="text/css">
    
    <script type='text/javascript' src="<%=BASEURL%>/js/Validacion.js"></script>
    <script type='text/javascript' src="<%= BASEURL %>/js/boton.js"></script>
    <script type='text/javascript' src="<%= BASEURL %>/js/general.js"></script>
    <script src='<%=BASEURL%>/js/tools.js' language='javascript'></script>
    <script src='<%=BASEURL%>/js/date-picker.js'></script>
    <script src="<%= BASEURL %>/js/transferencias.js"></script>
   
     <script>
        function calcularGasolina(){
            if (formulario.valgasolina.value!=''){
                if (<%=valor_neto%>-formulario.valgasolina.value>=0){
                    formulario.valefectivo.value=<%=valor_neto%>-formulario.valgasolina.value;
                }
            }
        }
     </script>     
     <script>
        function validar(){
            if ((formulario.valefectivo.value=="") || formulario.valgasolina.value=="" ){
                alert ("Revise antes los datos...");
            }else{
                if (((parseInt(formulario.valefectivo.value)+parseInt(formulario.valgasolina.value))==<%=valor_neto%>) ){
                    formulario.submit();
                }else{
                    alert ("Revise antes la suma de valores...");
                }
            }
        }	
     </script>        
     
</head>
<body onLoad="redimensionar();" onResize="redimensionar();">
<div id="capaSuperior" style="position:absolute; width:100%; height:100px; z-index:0; left: 0px; top: 0px;">
<jsp:include page="/WEB-INF/toptsp.jsp?encabezado=Anticipos de Estaci�n"/>
</div>

<div id="capaCentral" style="position:absolute; width:100%; height:85%; z-index:0; left: 0px; top: 100px; overflow: scroll;"> 
   
<%
   
   
   if (true  ){
%>   
   
    <form action="<%=CONTROLLER%>?estado=Anticipo&accion=Gasolina&opcion=aceptarAnticipoGas" method="post" name="formulario">
    <input type="hidden" name="conductor" value="<%=request.getParameter("conductor")%>" >
    <input type="hidden" name="placa" value="<%=request.getParameter("placa")%>" >
    <table width="689"  border="2" align="center">		<%//out.print("asf"+tot_preant);%>
     <tr>
         <td >           
    
                <table width="100%"  align="center">
                  <tr >
                     <td>
                          <table width='100%'  class="barratitulo">
                                <tr class="fila">
                                        <td align="left" width='85%' class="subtitulo1" nowrap> Conductor <%=request.getParameter("conductor")%>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Placa:  <%=request.getParameter("placa")%></td>
                                        <td width='15%' ><img src="<%=BASEURL%>/images/titulo.gif" width="32" height="20"align="left"><%=datos[0]%></td>
                                </tr>
                          </table>
                     </td>
                  </tr>
                  <tr class='fila'>
                     <td>
					 	
							<table width="100%" border="1" align="center" bordercolor="#999999" bgcolor="#F7F5F4">
								<tr class="fila">
									<th  > PLANILLA</th>
									<th  > FECHA</th>
                                                                        <th  > VALOR ANTICIPO       </th>
                                                                        <th  > CONDUCTOR  </th>
                                                                        <th  > VALOR A ENTREGAR</th>                                                                                                                                        
									<th  > PROPIETARIO </th>                                                                                                                                        
                                                                    </tr>
								<% 
                                                                        //String banc,cuent,estado_aprobacion,nombre,secuenciax,dstrctx,nitx;
                                                                        //nitx=request.getParameter("proveedor");
                                                                                                                                                
                                                                        %>
                                                                        <input type="hidden" value='<%=idAnticipoGasolina%>' name='idAnticipoGas'>
                                                                        <input type="hidden" value='<%=valor%>' name='valor'>
                                                                        <input type="hidden" value='<%=valor_neto%>' name='valor_neto'>
									<tr class="filagris">						 
                                                                                
                                                                                <td class="bordereporte" nowrap align="center"> <%=  anticipoGasolina.getPlanilla()%></td>                                                                             
                                                                                <td class="bordereporte" nowrap align="center"> <%=  (anticipoGasolina.getFecha_anticipo()).substring(0,10) %></td>									
                                                                                <td class="bordereporte" nowrap align="center"> <%= com.tsp.util.Util.customFormat(anticipoGasolina.getVlr())    %></td>									
                                                                                <td class="bordereporte" nowrap align="center"> <%= anticipoGasolina.getNombreConductor()    %></td>
                                                                                <td class="bordereporte" nowrap align="center"> <%= com.tsp.util.Util.customFormat(Double.parseDouble(valor_neto))   %></td><!--//anticipoGasolina.getVlrNeto())-->
                                                                                <td class="bordereporte" nowrap align="center"> <%= anticipoGasolina.getPla_owner()+" - "+anticipoGasolina.getNombrePropietario()%></td>
                                                                         </tr>																
								
							</table>					
					 
					 </td>
                  </tr>				  
		  
                </table>    
              
        </td>
      </tr>
      </table>  
            
        <br>
        <br>
        
        <table align="center" border="1">
            <tr class="fila">
                <td>Valor en combustible</td>
                <td>
                    <input name="valgasolina" type="text" class="textbox" maxlength="9" onKeyPress="soloDigitos(event,'decNO')" onChange="calcularGasolina()">
                        <img width='14'    title='Calcular efectivo' style="cursor:hand" src='<%=BASEURL%>/images/botones/iconos/modificar.gif'
                        onclick="calcularGasolina()">
                </td>
            </tr>
            <p></p>
            <tr class="fila">
                <td>Valor en efectivo</td>
                <td>
                    <input name="valefectivo" type="text" class="textbox" maxlength="9" onKeyPress="soloDigitos(event,'decNO')" readonly>                
                </td>
            </tr>
        </table>
        <br><br>
        <center>
	<img src='<%=BASEURL%>/images/botones/aceptar.gif'    style=" cursor:hand"  title='Aceptar...'     name='i_crear'       onclick="validar(); " >         
	<img src='<%=BASEURL%>/images/botones/regresar.gif'   style=" cursor:hand"  title='Regresar...'     name='i_regresar'       onclick="document.location = '<%= BASEURL %>/jsp/gasolina/mostrarAnticiposGasolina.jsp?conductor=<%=request.getParameter("conductor")%>&placa=<%=request.getParameter("placa")%>' "     onMouseOver='botonOver(this);' onMouseOut='botonOut(this);'> 
        <img src='<%=BASEURL%>/images/botones/salir.gif'      style='cursor:hand'    title='Salir...'      name='i_salir'       onclick='parent.close();'           onMouseOver='botonOver(this);' onMouseOut='botonOut(this);'>
		</center>
	     <br>
        <%  
       if (false){//request.getParameter("resultado")!=null && request.getParameter("resultado").equals("ok") ){
            %>
            <br><br>
            <p><table border="2" align="center">
              <tr>
                <td><table width="100%"  border="1" align="center"  bordercolor="#F7F5F4" bgcolor="#FFFFFF">
                  <tr>
                    <td width="170" align="center" class="mensajes"><%="Cuenta(s) aprobada(s)."%></td>
                    <td width="29" background="<%=BASEURL%>/images/cuadronaranja.JPG">&nbsp;</td>
                    <td width="40">&nbsp;</td>
                  </tr>
                </table></td>
              </tr>
            </table>
            </p>
            
            <%
        }
                                                                       
  %>
 
     
     
 </form>
 
    <% }else{
    %>lista de cuentas vac�a. raro...<%    
   }	
%>
	
</div>
<%=datos[1]%>
	
</body>
</html>
