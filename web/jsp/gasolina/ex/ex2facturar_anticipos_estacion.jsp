<%-- 
    Document   : mostrarCuentasProveedor
    Created on : 15/11/2007, 03:58:14 PM
    Author     : iamorales
--%>

<%@ page session   ="true"%> 
<%@ page errorPage ="/error/ErrorPage.jsp"%>
<%@ page import    ="java.util.*" %>
<%@ page import    ="com.tsp.util.*" %>
<%@ page import    ="com.tsp.operation.model.beans.*" %>
<%@ include file   ="/WEB-INF/InitModel.jsp"%>

<%
	String path = application.getRealPath(model.menuService.getRealUrl(request.getRequestURI()));
	String datos[] = model.menuService.getContenidoMenu(BASEURL,request.getRequestURI(),path);
	
	Usuario usuario = (Usuario) session.getAttribute("Usuario");
	String loginx=usuario.getLogin();
	       
        //String cond = (request.getParameter("cond")!=null && !request.getParameter("cond").equals(""))?request.getParameter("cond"):"prede.jpg";
        //String firma = (request.getParameter("firma")!=null)?request.getParameter("firma"):"none.jpg";
        
%>
<html>
<head>
	<title>Gesti�n de Anticipos de Estaciones de Gasolina</title>
    <link href="<%= BASEURL %>/css/estilostsp.css"     rel="stylesheet" type="text/css"> 
    <link href="/css/estilostsp.css" rel="stylesheet"  type="text/css">
    <script type='text/javascript' src="<%= BASEURL %>/js/boton.js"></script>
    <script>
		function validarSeleccion(){
		   var elementos = document.getElementsByName("idAnticipo");
		   
		   for (var i=0;i<elementos.length; i++){
		      if (elementos[i].checked){
			    return true;			
                      }  
		   }		   
		   alert ('Seleccione por lo menos un anticipo para iniciar el proceso de transferencia.');
		   return false;
		}
		
		function Sell_all_col(theForm,nombre){
			 for (i=0;i<theForm.length;i++)
					  if (theForm.elements[i].type=='checkbox' && theForm.elements[i].name==nombre)
						 theForm.elements[i].checked=theForm.All.checked;
	    }
		
		function send(theForm){
			theForm.submit();		
		}
		
    </script>
</head>
<body onLoad="redimensionar();" onResize="redimensionar();"><!--redimensionar-->

<div id="capaSuperior" style="position:absolute; width:100%; height:100px; z-index:0; left: 0px; top: 0px;">
<jsp:include page="/WEB-INF/toptsp.jsp?encabezado=Anticipos de Estaci�n"/>
</div>

<div id="capaCentral" style="position:absolute; width:100%; height:85%; z-index:0; left: -1px; top: 100px; overflow: scroll;"> 

   
    
<%

   String estaci=request.getParameter("estacion");
   if (estaci==null){estaci="";}
   //out.println("estaci"+estaci);
   ArrayList anticiposGasolina ;
   try { 
	   anticiposGasolina = model.AnticiposGasolinaService.getAnticiposGasolinaAprobados(estaci);
   }catch(Exception e ){
   		System.out.println("errorcillo en model.AnticiposGasolinaService.getAnticiposGasolinaAprobados()");
		anticiposGasolina=null;
   }
   if (anticiposGasolina!=null  ){
%>   
    <form action="<%=CONTROLLER%>?estado=Anticipo&accion=Gasolina&opcion=facturarAnticipoGas" method="post" name="formulario">
    <input type="hidden" name="estacion" value="<%=estaci%>">
    <%

    String respuesta=request.getParameter("respuesta");
	//String valor_neto="";//request.getParameter("valor_neto");
	
    %>
   
    
    <table width="689"  border="2" align="center">
     <tr>
         <td >           
    
                <table width="100%"  align="center">
                  
                  <tr class='fila'>
                     <td>
					 	
							<table width="100%" border="1" align="center" bordercolor="#999999" bgcolor="#F7F5F4">
								<tr class="fila">
									<TH   nowrap style="font size:11; font weight: bold" > <input type='checkbox'  id='All' onclick="Sell_all_col(this.form,'idAnticipo');">  </TH> 
				
									<th > PLANILLA  </th>
									<th > FECHA</th>
																		<th> VALOR ANTICIPO      </th>                                                                        
                                                                        <th > CONDUCTOR  </th>
                                                                        <th > VALOR ENTREGADO </th>																		
                                                                        <th > PROPIETARIO </th>
																		<th > APROBADOR</th>
																		<th > FECHA APROBACION</th>
																		<th > ESTACION</th>
                              </tr>
								<% 
                                                                        //String banc,cuent,estado_aprobacion,nombre,secuenciax,dstrctx,nitx;
                                                                        //nitx=request.getParameter("proveedor");
                                                                        AnticipoGasolina anticipoGasolina;
                                                                        for (int i=0; i<anticiposGasolina.size(); i++) { 
                                                                            anticipoGasolina=(AnticipoGasolina)anticiposGasolina.get(i);
                                                                            //valor=""+anticipoGasolina.getVlr()    ;
																			//valor_neto=""+anticipoGasolina.getVlrNeto()    ;
                                                                        
                                                                        %>
									
								<tr class='<%= (i%2==0?"filagris":"filaazul") %>' id='fila<%=i%>'   style=" font size:12"   >
                                                                                
																			    <td class="bordereporte" align='center' nowrap style="font size:11">  
 																			    	<input type='checkbox' name='idAnticipo' value='<%= anticipoGasolina.getId()%>' onclick=" cambiarColorMouse(fila<%=i%>); "> 
				   															    </td>
															 
                                                                                
                                                                                <td class="bordereporte" nowrap align="center"> <%=  anticipoGasolina.getPlanilla()%></td>                                                                             
                                                                                <td class="bordereporte" nowrap align="center"> <%=  (anticipoGasolina.getFecha_anticipo()).substring(0,10)    %></td>									
                                                                                <td class="bordereporte" nowrap align="center"> <%= com.tsp.util.Util.customFormat(anticipoGasolina.getVlr())    %></td>									
                                                                                <td class="bordereporte" nowrap align="center"> <%= anticipoGasolina.getNombreConductor()    %></td>									
                                                                                <td class="bordereporte" nowrap align="center"> <%= com.tsp.util.Util.customFormat(anticipoGasolina.getVlrNeto())    %></td>																													
                                                                                <td class="bordereporte" nowrap align="center"> <%= anticipoGasolina.getPla_owner()+" - "+anticipoGasolina.getNombrePropietario()%></td>	
																				<td class="bordereporte" nowrap align="center"> <%= anticipoGasolina.getUser_autorizacion()%></td>
																				<td class="bordereporte" nowrap align="center"> <%= (anticipoGasolina.getFecha_autorizacion()).substring(0,16)%></td>
																				
																				<td class="bordereporte" nowrap align="center"> <%=anticipoGasolina.getAsesor()%></td>																																																									
                              </tr>								
                                                                            <% System.out.println("i"+i); %>
								<% } %>
								
							</table>						
					 
					 </td>
                  </tr>				  
		  
                </table>
                
        </td>
      </tr>
      </table>  
            
        <br>
        <center>
	
		<img src="<%=BASEURL%>/images/botones/aceptar.gif"       height="21"  title='Aceptar'    onclick='send(formulario)'                                                                                         onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" style="cursor:hand">        
		<img src='<%=BASEURL%>/images/botones/regresar.gif'   style=" cursor:hand"  title='Regresar...'     name='i_regresar'       onclick="document.location = '<%= BASEURL %>/jsp/gasolina/filtro_consulta_anticipos_gasolina_fac.jsp' "     onMouseOver='botonOver(this);' onMouseOut='botonOut(this);'> 
        <img src='<%=BASEURL%>/images/botones/salir.gif'      style='cursor:hand'    title='Salir...'      name='i_salir'       onclick='parent.close();'           onMouseOver='botonOver(this);' onMouseOut='botonOut(this);'>
		</center>
	     <br>
        <%  
       if (respuesta!=null){//request.getParameter("resultado")!=null && request.getParameter("resultado").equals("ok") ){
            %>
            <br><br>
            <p><table border="2" align="center">
              <tr>
                <td><table width="100%"  border="1" align="center"  bordercolor="#F7F5F4" bgcolor="#FFFFFF">
                  <tr>
                    <td width="170" align="center" class="mensajes"><%=""+respuesta%></td>
                    <td width="29" background="<%=BASEURL%>/images/cuadronaranja.JPG">&nbsp;</td>
                    <td width="40">&nbsp;</td>
                  </tr>
                </table></td>
              </tr>
            </table>
            </p>
            
            <%
        }
                                                                    
  %>
     
 </form>
 <br>
 
    <% }else{
    %>lista de cuentas vac�a. raro...<%    
   }	
%>
</div>
<%=datos[1]%>
	
</body>
</html>
