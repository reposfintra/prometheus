<%-- 
    Document   : mostrarCuentasProveedor
    Created on : 15/11/2007, 03:58:14 PM
    Author     : iamorales
--%>

<%@ page session   ="true"%> 
<%@ page errorPage ="/error/ErrorPage.jsp"%>
<%@ page import    ="java.util.*" %>
<%@ page import    ="com.tsp.util.*" %>
<%@ page import    ="com.tsp.operation.model.beans.*" %>
<%@ include file   ="/WEB-INF/InitModel.jsp"%>

<%
	String path = application.getRealPath(model.menuService.getRealUrl(request.getRequestURI()));
	String datos[] = model.menuService.getContenidoMenu(BASEURL,request.getRequestURI(),path);
	
	Usuario usuario = (Usuario) session.getAttribute("Usuario");
	String loginx=usuario.getLogin();
	
	ArrayList preanticipos=new 	ArrayList();
	       
        //String cond = (request.getParameter("cond")!=null && !request.getParameter("cond").equals(""))?request.getParameter("cond"):"prede.jpg";
        //String firma = (request.getParameter("firma")!=null)?request.getParameter("firma"):"none.jpg";
        
%>
<html>
<head>
	<title>Gesti�n de Anticipos de Estaciones de Gasolina</title>
    <link href="<%= BASEURL %>/css/estilostsp.css"     rel="stylesheet" type="text/css"> 
    <link href="/css/estilostsp.css" rel="stylesheet"  type="text/css">
    <script type='text/javascript' src="<%= BASEURL %>/js/boton.js"></script>
    <script>
		function validarSeleccion(){
		   var elementos = document.getElementsByName("idAnticipo");
		   
		   for (var i=0;i<elementos.length; i++){
		      if (elementos[i].checked){
			    return true;			
                      }  
		   }		   
		   alert ('Seleccione por lo menos un anticipo para iniciar el proceso de transferencia.');
		   return false;
		}
		
    </script>
</head>
<body onLoad="redimensionar();" onResize="redimensionar();"><!--redimensionar-->

<div id="capaSuperior" style="position:absolute; width:100%; height:100px; z-index:0; left: 0px; top: 0px;">
<jsp:include page="/WEB-INF/toptsp.jsp?encabezado=Anticipos de Estaci�n"/>
</div>

<div id="capaCentral" style="position:absolute; width:100%; height:85%; z-index:0; left: -1px; top: 100px; overflow: scroll;"> 

    <form name='formpdf' id='formpdf' method='post' target='blank' action="<%=CONTROLLER%>?estado=Anticipo&accion=Gasolina&opcion=verPdf">
    </form>
    
<%
   if (request.getParameter("MostrarPdf")!=null && request.getParameter("MostrarPdf").equals("mostrar")){
        System.out.println("mostraPdf en jsp");
        %><script>formpdf.submit();</script><%
   }
    
   List anticiposGasolina = model.AnticiposGasolinaService.getAnticiposGasolina();
   if (anticiposGasolina!=null  ){
%>   
    <form action="<%=CONTROLLER%>?estado=Anticipo&accion=Gasolina&opcion=escogerAnticipoGas" method="post" name="formulario">
    <%
    String conductor=request.getParameter("conductor");
	
	preanticipos=model.AnticiposGasolinaService.searchPreAnticiposConductor(conductor);
	double total_preanticipos=0;
	if (preanticipos!=null && preanticipos.size()>0){
		String[] tem =(String[])preanticipos.get(0);
		total_preanticipos=Double.parseDouble(tem[4]);
		total_preanticipos=total_preanticipos+(total_preanticipos*0.014);
	}
	
    String placa=request.getParameter("placa");
	
	String cond= model.AnticiposGasolinaService.getImagenConductor( conductor, loginx);                  
	String firma =  model.AnticiposGasolinaService.getImagenFirma( conductor, loginx)   ;
	
    //String valor="";//request.getParameter("valor");
    //String idAnticipoGas=request.getParameter("idAnticipoGas");
    String respuesta=request.getParameter("respuesta");
	//String valor_neto="";//request.getParameter("valor_neto");
	
    %>
    <input type="hidden" name="conductor" value="<%=conductor%>" >
    <input type="hidden" name="placa" value="<%=placa%>" >
    
    
	
    
    <table width="689"  border="2" align="center">
     <tr>
         <td >           
    
                <table width="100%"  align="center">
                  <tr >
                     <td>
                          <table width='100%'  class="barratitulo">
                                <tr class="fila">
                                        <td align="left" width='85%' class="subtitulo1" nowrap> Conductor: <%=request.getParameter("conductor")%> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Placa:  <%=request.getParameter("placa")%>  </td>
                                        <td width='15%' ><img src="<%=BASEURL%>/images/titulo.gif" width="32" height="20"align="left"><%=datos[0]%></td>
                                </tr>
                          </table>
                     </td>
                  </tr>
                  <tr class='fila'>
                     <td>
					 	
							<table width="100%" border="1" align="center" bordercolor="#999999" bgcolor="#F7F5F4">
								<tr class="fila">
									<th >&nbsp;  </th>								
									<th > PLANILLA  </th>
									<th > FECHA</th>
																		<th> VALOR ANTICIPO    </th>                                                                        
                                                                        <th > CONDUCTOR  </th>
                                                                        <th > VALOR CON DESCUENTO </th>																		
																		<th > VALOR A ENTREGAR </th>
                                                                        <th > PROPIETARIO </th>
                              </tr>
								<% 
                                                                        //String banc,cuent,estado_aprobacion,nombre,secuenciax,dstrctx,nitx;
                                                                        //nitx=request.getParameter("proveedor");
                                                                        AnticipoGasolina anticipoGasolina;
                                                                        for (int i=0; i<anticiposGasolina.size(); i++) { 
                                                                            anticipoGasolina=(AnticipoGasolina)anticiposGasolina.get(i);
                                                                            //valor=""+anticipoGasolina.getVlr()    ;
																			//valor_neto=""+anticipoGasolina.getVlrNeto()    ;
                                                                        
                                                                        %>
									<tr class="<%= (i%2==0?"filagris":"filaazul")%>">
                                                                                
                                                                                <td class="bordereporte" align='center' nowrap>
                                                                                    <%if (model.AnticiposGasolinaService.getEstado_foto()){%>
                                                                                        <img width='14'    title='Escoger Anticipo <%= anticipoGasolina.getId()%>' style="cursor:hand" src='<%=BASEURL%>/images/botones/iconos/modificar.gif'
                                                                                        onclick="location.href = '<%=CONTROLLER%>?estado=Anticipo&accion=Gasolina&opcion=escogerAnticipoGas&idAnticipoGas=<%= anticipoGasolina.getId()%>&placa=<%=placa%>&conductor=<%=conductor%>&tot_preant=<%=total_preanticipos%>';">
                                                                                    <%}else{%>
                                                                                        								
                                                                                    <%}%>
										</td>
															 
                                                                                
                                                                                <td class="bordereporte" nowrap align="center"> <%=  anticipoGasolina.getPlanilla()%></td>                                                                             
                                                                                <td class="bordereporte" nowrap align="center"> <%=  (anticipoGasolina.getFecha_anticipo()).substring(0,10) %></td>									
                                                                                <td class="bordereporte" nowrap align="center"> <%= com.tsp.util.Util.customFormat(anticipoGasolina.getVlr())    %></td>									
                                                                                <td class="bordereporte" nowrap align="center"> <%= anticipoGasolina.getNombreConductor()    %></td>									

																				<td class="bordereporte" nowrap align="center"> <%= com.tsp.util.Util.customFormat(anticipoGasolina.getVlrNeto())    %></td>
																				<td class="bordereporte" nowrap align="center"> <%= com.tsp.util.Util.customFormat(anticipoGasolina.getVlrNeto()-total_preanticipos)    %></td>
																				<input type="hidden" name="tot_preant" value="<%=total_preanticipos%>">

                                                                                <td class="bordereporte" nowrap align="center"> <%= anticipoGasolina.getPla_owner()+" - "+anticipoGasolina.getNombrePropietario()%></td>																													
                              </tr>								
                                                                            <% System.out.println("i"+i); %>
								<% } %>
								
							</table>						
					 
					 </td>
                  </tr>				  
		  
                </table>
                
        </td>
      </tr>
      </table>  
            
        <br>
        <center>
	
        <img src='<%=BASEURL%>/images/botones/regresar.gif'   style=" cursor:hand"  title='Regresar...'     name='i_regresar'       onclick="document.location = '<%= BASEURL %>/jsp/gasolina/anticipo_gasolina.jsp?conductor=<%=request.getParameter("conductor")%>&placa=<%=request.getParameter("placa")%>' "     onMouseOver='botonOver(this);' onMouseOut='botonOut(this);'> 
        <img src='<%=BASEURL%>/images/botones/salir.gif'      style='cursor:hand'    title='Salir...'      name='i_salir'       onclick='parent.close();'           onMouseOver='botonOver(this);' onMouseOut='botonOut(this);'>
		</center>
	     <br>
        <%  
       if (respuesta!=null){//request.getParameter("resultado")!=null && request.getParameter("resultado").equals("ok") ){
            %>
            <br><br>
            <p><table border="2" align="center">
              <tr>
                <td><table width="100%"  border="1" align="center"  bordercolor="#F7F5F4" bgcolor="#FFFFFF">
                  <tr>
                    <td width="170" align="center" class="mensajes"><%=""+respuesta%></td>
                    <td width="29" background="<%=BASEURL%>/images/cuadronaranja.JPG">&nbsp;</td>
                    <td width="40">&nbsp;</td>
                  </tr>
                </table></td>
              </tr>
            </table>
            </p>
            
            <%
        }
                                                                    
  %>
  <table align="center" border="1" bordercolor="#000066">
    <tr align="center">
        <td align="center" >
            <img src="<%=BASEURL%>/documentos/imagenes/<%=cond%>" onClick="window.open('<%=BASEURL%>/documentos/imagenes/<%=cond%>','','scrollbars=yes,resizable=yes');" title="Haga click para agrandar" style="cursor:hand" 
             alt='FOTO AUSENTE.' height="100" width="100"
            >    <!-- width="241" height="175"-->
        </td>
   
        <td align="center">
          <img src="<%=BASEURL%>/documentos/imagenes/<%=firma%>" width="200" height="100" onClick="window.open('<%=BASEURL%>/documentos/imagenes/<%=firma%>','','scrollbars=yes,resizable=yes');"
            alt='FIRMA AUSENTE.' title="Haga click para agrandar"   style="cursor:hand"  
            > </td>
    </tr>
  </table>
   
 </form>
 <br>
 
 <form name='formfoto' id='formfoto' method='post' action="<%=CONTROLLER%>?estado=Anticipo&accion=Gasolina&opcion=subirFoto" enctype="multipart/form-data" >
 <!--<form action="http://localhost:8060/ejerWeb_ServletUpload/servBlob" enctype="multipart/form-data" method="post">   -->
    <table align="center">
		<tr class="filagris"><td>
        <input type="hidden" name="conductor" value="<%=conductor%>"><!--88156794-->
        <input type="hidden" name="placa" value="<%=placa%>"><!--88156794-->
        FOTO ACTUAL: 

   		<input type="file" name="cv" size="70" accept="text/plain;image/jpeg" >

        <input type="submit" name="enviar" value="Enviar" >
		</td></tr>
    </table>
 </form>

 
    <% }else{
    %>lista de cuentas vac�a. raro...<%    
   }	
%>
<table align="center" border="2"><tr><td>

	<table width="100%" align="center" bordercolor="#999999" bgcolor="#F7F5F4">
	
	<tr class="barratitulo"> 
		<td align="left" class="subtitulo1" nowrap colspan="2">
	
			Preanticipos del conductor 
		</td>
		<td  ><img src="<%=BASEURL%>/images/titulo.gif" width="32" height="20" align="left"><%=datos[0]%></td>
		
			
	</tr>
	</table>
</td></tr>
<tr><td>
	<table  width="100%"  border="1" align="center" bordercolor="#999999" bgcolor="#F7F5F4">
	<tr class="fila">
		<th>
			VALOR
		</th>
		<th>
			FECHA
		</th>
		<th>
			CREADOR
		</th>
			
	</tr>
	
	<%
	int i=0;
	String[] preanticipo=new String[5];
	
	for (i=0;i<preanticipos.size();i++){
	
		preanticipo=(String[])preanticipos.get(i); 
		//out.print("LOOK:"+preanticipo[0]);
		
	
	
	%>
	
	<tr class="<%= (i%2==0?"filagris":"filaazul")%>">
		<td class="bordereporte" nowrap align="center">
			<%=com.tsp.util.Util.customFormat(Double.parseDouble(preanticipo[1]))%>
		</td>
		<td class="bordereporte" nowrap align="center">
			<%=preanticipo[2].substring(0,16)%>
		</td>
		<td class="bordereporte" nowrap align="center">
			<%=preanticipo[3]%>
		</td>
				
	</tr>
	
	
		
	<%}%>
	<%if (preanticipo[4] != null){%>
	<tr class="<%= (i%2==0?"filagris":"filaazul")%>">
		<td class="bordereporte" nowrap align="center" >
			<strong><%=""+com.tsp.util.Util.customFormat(Double.parseDouble(preanticipo[4]))%></strong>
		</td>
		<td class="bordereporte" nowrap align="center" colspan="2">

		</td>
						
	</tr>
	<%}%>
	</table>
	
</td></tr></table>
</div>
<%=datos[1]%>
	
</body>
</html>
