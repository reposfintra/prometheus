<%-- 
    Document   : mostrarCuentasProveedor
    Created on : 15/11/2007, 03:58:14 PM
    Author     : iamorales
--%>

<%@ page session   ="true"%> 
<%@ page errorPage ="/error/ErrorPage.jsp"%>
<%@ page import    ="java.util.*" %>
<%@ page import    ="com.tsp.util.*" %>
<%@ page import    ="com.tsp.operation.model.beans.*" %>
<%@ include file   ="/WEB-INF/InitModel.jsp"%>

<%
	String path = application.getRealPath(model.menuService.getRealUrl(request.getRequestURI()));
	String datos[] = model.menuService.getContenidoMenu(BASEURL,request.getRequestURI(),path);
	
	Usuario usuario = (Usuario) session.getAttribute("Usuario");
	String loginx=usuario.getLogin();
	       
        //String cond = (request.getParameter("cond")!=null && !request.getParameter("cond").equals(""))?request.getParameter("cond"):"prede.jpg";
        //String firma = (request.getParameter("firma")!=null)?request.getParameter("firma"):"none.jpg";
        
%>
<html>
<head>
	<title>Gesti�n de Anticipos de Estaciones de Gasolina</title>
    <link href="<%= BASEURL %>/css/estilostsp.css"     rel="stylesheet" type="text/css"> 
    <link href="/css/estilostsp.css" rel="stylesheet"  type="text/css">
    <script type='text/javascript' src="<%= BASEURL %>/js/boton.js"></script>
	<script language='javascript'>	
	function exporta()
	{
		window.open('<%= CONTROLLER %>?estado=Anticipos&accion=Ver&op=3','EXP','status=yes,scrollbars=yes,width=400,height=150,resizable=no');
	}
	</script>
    <script>
		function validarSeleccion(){
		   var elementos = document.getElementsByName("idAnticipo");
		   
		   for (var i=0;i<elementos.length; i++){
		      if (elementos[i].checked){
			    return true;			
                      }  
		   }		   
		   alert ('Seleccione por lo menos un anticipo para iniciar el proceso de transferencia.');
		   return false;
		}
		
		function Sell_all_col(theForm,nombre){
			 for (i=0;i<theForm.length;i++)
					  if (theForm.elements[i].type=='checkbox' && theForm.elements[i].name==nombre)
						 theForm.elements[i].checked=theForm.All.checked;
	    }
		
		function send(theForm){
			theForm.submit();		
		}
		
    </script>
</head>
<body onLoad="redimensionar();" onResize="redimensionar();"><!--redimensionar-->

<div id="capaSuperior" style="position:absolute; width:100%; height:100px; z-index:0; left: 0px; top: 0px;">
<jsp:include page="/WEB-INF/toptsp.jsp?encabezado=Anticipos de Estaci�n"/>
</div>

<div id="capaCentral" style="position:absolute; width:100%; height:85%; z-index:0; left: -1px; top: 100px; overflow: scroll;"> 

   
    
<%
   model.AnticiposGasolinaService.setCarpeta("documentos/imagenes");
   
   ArrayList anticiposGasolina ;
   try { 
	   anticiposGasolina = model.AnticiposGasolinaService.getLista();
	   
	   ArrayList af =  anticiposGasolina;
	   session.setAttribute("vant",af);
	   
   }catch(Exception e ){
   		System.out.println("errorcillo en model.AnticiposGasolinaService.getAnticiposGasolinaAprobados()");
		anticiposGasolina=null;
   }
   if (anticiposGasolina!=null  ){
   	//System.out.println(((AnticipoGasolina)anticiposGasolina.get(0)).getId());
%>   

	<form name='formpdf' id='formpdf' method='post' target='blank' action="<%=CONTROLLER%>?estado=Anticipo&accion=Gasolina&opcion=verPdf">
		
    </form>
	
	<%
	if (request.getParameter("MostrarPdf")!=null && request.getParameter("MostrarPdf").equals("mostrar")){
        System.out.println("mostraPdf en jsp");
        %><script>formpdf.submit();</script><%
    }
   %>
	
	
    <form action="<%=CONTROLLER%>?estado=Anticipo&accion=Gasolina&opcion=facturarAnticipoGas" method="post" name="formulario">

	
	<%
    
    String respuesta=request.getParameter("respuesta");
	//String valor_neto="";//request.getParameter("valor_neto");
	
    %>
   
    
    <table width="689"  border="2" align="center">
     <tr>
         <td >           
    
                <table width="100%"  align="center">
                  
                  <tr class='fila'>
                     <td>
					 	
							<table width="100%" border="1" align="center" bordercolor="#999999" bgcolor="#F7F5F4">
								<tr class="fila">
									
									
									<th > PLANILLA  </th>
									<th > FECHA</th>
									<th> VALOR ANTICIPO     </th>                                                                        
									<th > CONDUCTOR  </th>
									<th > VALOR ENTREGADO </th>																		
									<th > PROPIETARIO </th>
									<th > FACTURADOR</th>
									<th > FECHA FACTURA</th>
									<th > APROBADOR</th>
									<th > FECHA APROBACION</th>
									<th > VALOR COMBUSTIBLE</th>
									<th > VALOR EFECTIVO</th>
									<th > ESTADO</th>
									<th > POSIBLE CXP</th>
									<th > POSIBLE CXP 2</th>
									<th > CXP</th>			
									<th > SUMA ANTICIPOS DE PLANILLA</th>			
									<th > SUMA CXPS TSP DE PLANILLA</th>			
									<th > RESTA</th>			
									<th > CXP TSP POSIBLE</th>									
									<th > CORRIDA</th>
									<th > CHEQUE</th>
									<th > VALOR</th>
									<th > CXP TSP POSIBLE 2</th>
									<th > CORRIDA</th>
									<th > CHEQUE</th>									
									<th > VALOR</th>
                              </tr>
								<% 
										//String banc,cuent,estado_aprobacion,nombre,secuenciax,dstrctx,nitx;
										//nitx=request.getParameter("proveedor");
										AnticipoGasolina anticipoGasolina;
										for (int i=0; i<anticiposGasolina.size(); i++) { 
											anticipoGasolina=(AnticipoGasolina)anticiposGasolina.get(i);
											//valor=""+anticipoGasolina.getVlr()    ;
											//valor_neto=""+anticipoGasolina.getVlrNeto()    ;
								
											int idAntGas = anticipoGasolina.getId();
											double valor_neto = anticipoGasolina.getVlrNeto();		
											System.out.println(idAntGas+" "+valor_neto);
																
										%>
										<tr class='<%= (i%2==0?"filagris":"filaazul") %>' id='fila<%=i%>'   style=" font size:12"   >

											<td class="bordereporte" nowrap align="center"> <a target="_blank" href="<%=BASEURL%>/jsp/applus/importar.jsp?num_osx=<%=  anticipoGasolina.getPlanilla()%>&tipito=planilla"> <%=  anticipoGasolina.getPlanilla()%> </td>                                                                             
											<td class="bordereporte" nowrap align="center"> <a target="_blank" href="<%=BASEURL%>/jsp/applus/mostrar_archivos.jsp?num_osx=<%=  anticipoGasolina.getPlanilla()%>&tipito=planilla"> <%=  (anticipoGasolina.getFecha_anticipo()).substring(0,10)    %> </a></td>									
		 
											
											<td class="bordereporte" nowrap align="center"> <%= com.tsp.util.Util.customFormat(anticipoGasolina.getVlr())    %></td>									
											<td class="bordereporte" nowrap align="center"> <%= anticipoGasolina.getNombreConductor()    %></td>									
											
											<% 
											String menu = request.getParameter("menu");
											if(menu!=null && menu.equals("consultar_propio")){
											
											%>
											<td class="bordereporte" nowrap align="center"> <a href="<%=CONTROLLER%>?estado=Anticipo&accion=Gasolina&opcion=exportarPdf&idAntGas=<%=idAntGas+""%>&valor_neto=<%=valor_neto+""%>&menu=consultar_propio"><%= com.tsp.util.Util.customFormat(valor_neto)    %></a></td>																													
											<%
											}else{
											%>
											<td class="bordereporte" nowrap align="center"><%=com.tsp.util.Util.customFormat(valor_neto)%></td>																													
											<%
											}
											%>
											
											<td class="bordereporte" nowrap align="center"> <%= anticipoGasolina.getPla_owner()+" - "+anticipoGasolina.getNombrePropietario()%></td>	
											<td class="bordereporte" nowrap align="center"> <%= anticipoGasolina.getUser_transferencia()%></td>	
											<%if ((anticipoGasolina.getFecha_transferencia()).equals("")){%>																																																																												
												<td class="bordereporte" nowrap align="center"> <%=  (anticipoGasolina.getFecha_transferencia())%></td>																																																									
											<%}else{%>
												<td class="bordereporte" nowrap align="center"> <%=  (anticipoGasolina.getFecha_transferencia()).substring(0,16)%></td>																																																									
		
											<%}%>	
											<td class="bordereporte" nowrap align="center"> <%= anticipoGasolina.getUser_autorizacion()%></td>																							
											<%if ((anticipoGasolina.getFecha_autorizacion()).equals("")){%>																																																																												
												<td class="bordereporte" nowrap align="center"> <%= (anticipoGasolina.getFecha_autorizacion())%></td>																																																									
											<%}else{%>
												<td class="bordereporte" nowrap align="center"> <%= (anticipoGasolina.getFecha_autorizacion()).substring(0,16)%></td>																																																									
											<%}%>	
											<td class="bordereporte" nowrap align="center"> <%= com.tsp.util.Util.customFormat(Double.parseDouble(anticipoGasolina.getVlr_gasolina()))%></td>																							
											<td class="bordereporte" nowrap align="center"> <%= com.tsp.util.Util.customFormat(Double.parseDouble(anticipoGasolina.getVlr_efectivo()))%></td>																							
											<td class="bordereporte" nowrap align="center"> <%= anticipoGasolina.getEstado_pago_tercero()%></td>		
																																
											<td class="bordereporte" nowrap align="center"> <%= anticipoGasolina.getCxpPosible()%></td>
											<td class="bordereporte" nowrap align="center"> <%= anticipoGasolina.getCxpPosible2()%></td>
											<td class="bordereporte" nowrap align="center"> <%= anticipoGasolina.getCxp()%></td>											
											<td class="bordereporte" nowrap align="center"> <%= com.tsp.util.Util.customFormat(Double.parseDouble(anticipoGasolina.getSumaAnticiposDplanilla()))%></td>
											<td class="bordereporte" nowrap align="center"> <%= com.tsp.util.Util.customFormat(Double.parseDouble(anticipoGasolina.getSumaCxpsTspDplanilla()))%></td>																															
											<td class="bordereporte" nowrap align="center"> <%= com.tsp.util.Util.customFormat(Double.parseDouble(anticipoGasolina.getSumaCxpsTspDplanilla())-Double.parseDouble(anticipoGasolina.getSumaAnticiposDplanilla()))%></td>
											<%String[] CxpTspPosible=anticipoGasolina.getCxpTspPosible_corrida_cheque().split("_");%>
											<td class="bordereporte" nowrap align="center"> <% if (CxpTspPosible.length>0){out.print(CxpTspPosible[0]);}else{out.print("");} %></td>
											<td class="bordereporte" nowrap align="center"> <% if (CxpTspPosible.length>1){out.print(CxpTspPosible[1]);}else{out.print("");} %></td>
											<td class="bordereporte" nowrap align="center"> <% if (CxpTspPosible.length>2){out.print(CxpTspPosible[2]);}else{out.print("");} %></td>
											<td class="bordereporte" nowrap align="center"> <% if (CxpTspPosible.length>3){out.print(com.tsp.util.Util.customFormat(Double.parseDouble(CxpTspPosible[3])));}else{out.print("");} %></td>
											<%CxpTspPosible=anticipoGasolina.getCxpTspPosible2_corrida_cheque().split("_");%>
											<td class="bordereporte" nowrap align="center"> <% if (CxpTspPosible.length>0){out.print(CxpTspPosible[0]);}else{out.print("");} %></td>
											<td class="bordereporte" nowrap align="center"> <% if (CxpTspPosible.length>1){out.print(CxpTspPosible[1]);}else{out.print("");} %></td>
											<td class="bordereporte" nowrap align="center"> <% if (CxpTspPosible.length>2){out.print(CxpTspPosible[2]);}else{out.print("");} %></td>
											<td class="bordereporte" nowrap align="center"> <% if (CxpTspPosible.length>3){out.print(com.tsp.util.Util.customFormat(Double.parseDouble(CxpTspPosible[3])));}else{out.print("");} %></td>

									  </tr>								
																				   
										<% } %>
								
							</table>						
					 
					 </td>
                  </tr>				  
		  
                </table>
                
        </td>
      </tr>
      </table>  
            
        <br>
        <center>
	
		<!--<img src='<%//=BASEURL%>/images/botones/regresar.gif'   style=" cursor:hand"  title='Regresar...'     name='i_regresar'       onclick="document.location = '<%//= BASEURL %>/jsp/gasolina/filtro_consulta_anticipos_gasolina_estacion.jsp' "     onMouseOver='botonOver(this);' onMouseOut='botonOut(this);'> -->
        <img src='<%=BASEURL%>/images/botones/salir.gif'      style='cursor:hand'    title='Salir...'      name='i_salir'       onclick='parent.close();'           onMouseOver='botonOver(this);' onMouseOut='botonOut(this);'>
		<img src="<%=BASEURL%>/images/botones/exportarExcel.gif" name="exportar" id="exportar" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" onClick='javascript:exporta()' >
		</center>
	     <br>
        <%  
       if (respuesta!=null){//request.getParameter("resultado")!=null && request.getParameter("resultado").equals("ok") ){
            %>
            <br><br>
            <p><table border="2" align="center">
              <tr>
                <td><table width="100%"  border="1" align="center"  bordercolor="#F7F5F4" bgcolor="#FFFFFF">
                  <tr>
                    <td width="170" align="center" class="mensajes"><%=""+respuesta%></td>
                    <td width="29" background="<%=BASEURL%>/images/cuadronaranja.JPG">&nbsp;</td>
                    <td width="40">&nbsp;</td>
                  </tr>
                </table></td>
              </tr>
            </table>
            </p>
            
            <%
        }
                                                                    
  %>
     
 </form>
 <br>
 
    <% }else{
    %>lista de cuentas vac�a. raro...<%    
   }	
%>
</div>
<%=datos[1]%>
	
</body>
</html>
