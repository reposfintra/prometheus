

<%-- Declaracion de librerias--%>
<%@page contentType="text/html;"%>
<%@page session="true"%>
<%@page import="java.util.*"%>
<%@page import="com.tsp.operation.model.*"%>
<%@page import="com.tsp.operation.model.beans.*"%>
<%@page import="com.tsp.util.*"%>
<%@page import="com.tsp.util.Util.*"%>
<%@include file="/WEB-INF/InitModel.jsp"%>
<%@page import="com.tsp.operation.model.*"%>
<%@taglib uri="http://jakarta.apache.org/taglibs/input-0.90" prefix="input" %>

<html>
    <head>
        <title>Preanticipos</title>
        <link href="../../../css/estilostsp.css" rel="stylesheet" type="text/css"> 
        <link href="<%=BASEURL%>/css/estilostsp.css" rel="stylesheet" type="text/css">
    </head>
    
    <script type='text/javascript' src="<%=BASEURL%>/js/Validacion.js"></script>
    <script type='text/javascript' src="<%= BASEURL %>/js/boton.js"></script>
    <script type='text/javascript' src="<%= BASEURL %>/js/general.js"></script>
    <script src='<%=BASEURL%>/js/tools.js' language='javascript'></script>
    <script src='<%=BASEURL%>/js/date-picker.js'></script>
    <script src="<%= BASEURL %>/js/transferencias.js"></script>
    
    <%
    int d=0;//cantidad de decimales
    Usuario usuario            = (Usuario) session.getAttribute("Usuario");


    String Retorno=request.getParameter("mensaje");


	%>
    
    <script>

        function enviarFormularioX(CONTROLLER,frm){	
		

			frm.action='<%=CONTROLLER%>?estado=Anticipo&accion=Gasolina&opcion=aceptarPreanticipos';
		
			frm.submit();
        }    
		

        function enviarFormularioParaAgregar(CONTROLLER,frm){	
			//alert("cas");
			
			frm.action='<%=CONTROLLER%>?estado=Anticipo&accion=Gasolina&opcion=agregarPreanticipo';
			//alert("cas");
			if ((frm.conductor.value!="") && (frm.valorcillo.value!="")) {
				frm.submit();
			}else{
				alert("Por favor revise los datos.");
			}
        }
		
		function salir(){	
		
			<%
			if (request.getParameter("primeravez")==null){
				model.AnticiposGasolinaService.reiniciarArregloPreanticipos();
			}
			%>
			//alert("dfsdla");
			parent.close();
			
        }
		
    </script>
    
    <body onLoad="javascript:formulario.conductor.focus">
        
        <div id="capaSuperior" style="position:absolute; width:100%; height:100px; z-index:0; left: 0px; top: 0px;">
            <jsp:include page="/toptsp.jsp?encabezado=PREANTICIPOS"/>
        </div>
        <div id="capaCentral" style="position:absolute; width:100%; height:87%; z-index:0; left: 0px; top: 100px; overflow: scroll;">
            
            <FORM name='formulario' method="post" >
                
                <table width="432"  border="2"align="center">
                    
                    <tr>
                        <td width="420">
                            <table width="100%"  class="tablaInferior" >
                                <tr class="fila">
                                    <td colspan="1" align="left" nowrap class="subtitulo">&nbsp;Preanticipos </td>
                                    <td colspan="1" align="left" nowrap class="bordereporte"><img src="<%=BASEURL%>/images/fintra.gif" width="166" height="28" class="bordereporte"> </td>
                                </tr>
                                
                                <tr class="fila">
                                    <td colspan="1" >Cedula </td>
                                    <td class="fila">
										<input name="conductor" type="text" class="textbox" id="conductor" maxlength="12">
                                        
                                    </td>
                                </tr>
						
								<tr class="fila">
                                    <td colspan="1" >Valor </td>
                                    <td class="fila">
										<input name="valorcillo" type="text" class="textbox" 
                                                                                       	
                                            maxlength="10" onKeyPress="soloDigitos(event,'decNO')">
										
                                        
                                    </td>
                                </tr>


                            </table>

                        </td>
                    </tr>
                </table>
                <br>
                <div align="center">
                    <img src="<%=BASEURL%>/images/botones/agregar.gif"  name="imgaceptar" onClick="javascript:enviarFormularioParaAgregar('<%=CONTROLLER%>',formulario);" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" style="cursor:hand">
                </div>
                
                <br>
                <table id="detalle" border="1" align="center" width="75%">
                    <tr  id="fila1" class="tblTitulo">
                        <td align="center"><strong>ITEM</strong>  </td>
                        <td align="center"><strong>CEDULA </strong></td>
                        <td align="center"><strong>VALOR</strong></td>

                    </tr>				
                    <% 
                      
                    double total_valor_preanticipos=0.0;
                    int i=0;
                    ArrayList detalle=model.AnticiposGasolinaService.getPreanticipos();
                    
                    
                    for(  i = 0; i < detalle.size(); i++ ){
						String[] preant=(String[])detalle.get(i);
                    %>
                    <tr class="<%= (i%2==0)? "filagris" : "filaazul" %> style="cursor:default" align="center"> 					   
						<td align="center" > <%=(i+1)%></td>
                        <td align="center" > <%=preant[0]%></td>
                        <td align="center"   ><%=com.tsp.util.Util.customFormat(Double.parseDouble(preant[1]))%></td>
                        <%total_valor_preanticipos=total_valor_preanticipos+Double.parseDouble(preant[1]);%>

                    </tr>
                    <%}%>
					<tr  id="fila1" class="tblTitulo">
                        <td align="center">  </td>
                        <td align="center"> </td>
                        <td align="center"><%=com.tsp.util.Util.customFormat(total_valor_preanticipos)%></td>

                    </tr>				
                </table>
                <br>
                <div align="center">
                    <img src="<%=BASEURL%>/images/botones/aceptar.gif"  name="imgaceptar" onClick="javascript:enviarFormularioX('<%=CONTROLLER%>',formulario);" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" style="cursor:hand">&nbsp;       &nbsp;
                    <img src="<%=BASEURL%>/images/botones/salir.gif"  name="imgsalir"  onMouseOver="botonOver(this);" onClick="javascript:salir();" onMouseOut="botonOut(this);" style="cursor:hand">&nbsp;       &nbsp;
                    &nbsp;       &nbsp;
                    
                </div>
                <br>
                <%
                String mensaje="";
                if( Retorno != null && !(Retorno.equals(""))){
                    if (Retorno.equals("ok")){
                         mensaje="Inserci�n exitosa.";
                    }else{
	  					 mensaje="Ocurri� un errorcillo.";
					}
				
                %>  
                <table border="2" align="center">
                    <tr>
                        <td>
                            <table width="100%"  border="1" align="center"  bordercolor="#F7F5F4" bgcolor="#FFFFFF">
                                <tr>
                                    <td width="229" align="center" class="mensajes"><%=mensaje%></td>
                                    <td width="29" background="<%=BASEURL%>/images/cuadronaranja.JPG">&nbsp;</td>
                                    <td width="58">&nbsp;</td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
                <%}%>            
				
				
                            
                <br>
            </form>
            
        </div>
       
        <iframe width="188" height="166" name="gToday:normal:agenda.js:gfPop:plugins.js" id="gToday:normal:agenda.js:gfPop:plugins_24.js" src="<%=BASEURL%>/js/Calendario/ipopeng.htm" scrolling="yes" frameborder="0" style="visibility:visible; z-index:999; position:absolute; left:-500px; top:0px;">
        </iframe>
		
    </body>
</html>


