<%@page session="true"%> 
<%@page errorPage="/error/ErrorPage.jsp"%>
<%@page import="java.*" %>
<%@page import="java.util.*" %>
<%@include file="/WEB-INF/InitModel.jsp"%>
<%@page import="com.tsp.util.Util"%>
<%@page import    ="com.tsp.operation.model.beans.Usuario" %>

<html>
<head>
        <title>Lista de Estaciones</title>
        <link href="<%= BASEURL %>/css/estilostsp.css" rel="stylesheet" type="text/css"> 
        <script language="JavaScript" type="text/javascript" src="<%=BASEURL%>/js/boton.js"></script>
     
</head>
<body>
<center>


<div id="capaSuperior" style="position:absolute; width:100%; height:100px; z-index:0; left: 0px; top: 0px;">
<jsp:include page="/WEB-INF/toptsp.jsp?encabezado=Estaciones"/>
</div>

<div id="capaCentral" style="position:absolute; width:100%; height:85%; z-index:0; left: 0px; top: 100px; overflow: scroll;"> 
<center>


   <% Usuario   User           = (Usuario)session.getAttribute("Usuario");
      String    usuario        = User.getLogin();   
      String    distrito       = User.getDstrct();
      List      lista          = model.AnticiposGasolinaService.getEstaciones();
      String    msj            = request.getParameter("msj");%>
      
      
      
    <% if( lista.size()>0 ){%>
      <FORM method='post' name='formCuenta'  id='formCuenta' >
      <table width="98%" border="2" align="center">
       <tr>
          <td>  
               <table width='100%' align='center' class='tablaInferior'>

                      <tr class="barratitulo">
                        <td colspan='4' >
                           <table cellpadding='0' cellspacing='0' width='100%'>
                             <tr>
                              <td align="left" width='55%' class="subtitulo1">&nbsp;LISTA DE ESTACIONES</td>
                              <td align="left" width='*'  ><img src="<%=BASEURL%>/images/titulo.gif" width="32" height="20"  height="20" align="left"></td>
                            </tr>
                           </table>
                        </td>
                     </tr>
                     
                     <tr class="fila">
                            <td width='100%' colspan='2'>
                                <table width='100%' border="1" bordercolor="#999999" bgcolor="#F7F5F4" align="center">
                                
                                    <TR  class="tblTitulo"  >  
                                            <!--<TH nowrap style="font size:11; font weight: bold" height='25'>&nbsp          </TH>-->
                                            <TH nowrap style="font size:11; font weight: bold">   ID     </TH>
                                            <TH nowrap style="font size:11; font weight: bold">TELEFONO            </TH>
                                            <TH nowrap style="font size:11; font weight: bold">DIRECCION</TH>
                                            <TH nowrap style="font size:11; font weight: bold">NIT      </TH>                                            
                                            <TH nowrap style="font size:11; font weight: bold">TIPO ID</TH>
                                            <TH nowrap style="font size:11; font weight: bold">NOMBRE         </TH>
                                            <TH nowrap style="font size:11; font weight: bold">E_MAIL</TH>
                                            <TH nowrap style="font size:11; font weight: bold">DESCUENTO         </TH>
                                            <TH nowrap style="font size:11; font weight: bold">PAIS    </TH>
                                            <TH nowrap style="font size:11; font weight: bold">CIUDAD</TH>
											<TH nowrap style="font size:11; font weight: bold">NOMBRE ESTACION</TH>
											<TH nowrap style="font size:11; font weight: bold">LOGIN</TH>
                                        </TR>        
                                
                                
                                     <% for(int i=0;i<lista.size();i++){
                                           Hashtable ht = (Hashtable) lista.get(i);%>

                                              <TR class='<%= (i%2==0?"filagris":"filaazul") %>' >
                                                 <!--<td nowrap>
                                                        <input type='checkbox' name='cuenta' value='<%//= ht.get("id")  %>'> 
                                                 </td>-->
                                                 <td  nowrap  align='center'>&nbsp<%= ht.get("id")  %></td>
                                                 <td  nowrap                >&nbsp<%= ht.get("tel")     %></td>
                                                 <td  nowrap                >&nbsp<%= ht.get("dir")  %></td>                 
                                                 <td  nowrap  align='center'>
                                                       <a href='#' title='Modificar' onclick="window.close(); 
													   	parent.opener.location.href='<%= CONTROLLER%>?estado=Anticipo&accion=Gasolina&opcion=INIT&id=<%= ht.get("id")%>&nit=<%= ht.get("nit")%>&tel=<%= ht.get("tel")%>&dir=<%= ht.get("dir")%>&tipo_id=<%=ht.get("tipo_id")%>&nombre=<%=ht.get("nombre")%>&e_mail=<%=ht.get("e_mail")%>&descuento=<%=ht.get("descuento")%>&c_paiso=<%=ht.get("pais")%>&nombre_estacion=<%=ht.get("nombre_estacion")%>&c_paiso=<%=ht.get("pais")%>&login_estacion=<%=ht.get("login_estacion")%>';"> <%= ht.get("nit")%> </a>
                                                 </td>
                                                 <td  nowrap align='center' >&nbsp<%= ht.get("tipo_id")    %></td>
                                                 <td  nowrap                >&nbsp<%= ht.get("nombre")   %></td>
                                                 <td  nowrap                >&nbsp<%= ht.get("e_mail")%></td>
                                                 <td  nowrap  align='right' >&nbsp<%= ht.get("descuento")  %></td>
                                                 <td  nowrap  align='center'>&nbsp<%= ht.get("pais")    %></td>
                                                 <td  nowrap                >&nbsp<%= ht.get("ciudad")%></td> 
												 <td  nowrap                >&nbsp<%= ht.get("nombre_estacion")%></td>                 
												 <td  nowrap                >&nbsp<%= ht.get("login_estacion")%></td>                      
                                              </TR>
                                        <%}%>
                                
                                
                                </table>
                            </td>
                     </tr>
                     
                     
                     
            </table>
         </td>
      </tr>
   </table> 
   
     <input type='hidden' name='distrito' value='<%= distrito %>'> 
     <input type='hidden' name='usuario'  value='<%= usuario  %>'> 
  </FORM>
 
      
 <p> 
      
      <img src="<%=BASEURL%>/images/botones/salir.gif"            height="21"  title='Salir'      onClick="window.close();"               onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" style="cursor:hand">           
 </p>
      
  
  
  
  <%}else  msj="No hay estaciones...";%>
 
  
  
  
  <% if(msj!=null  &&  !msj.equals("") ){%>
                <br>
                <table border="2" align="center">
                      <tr>
                        <td>
                            <table width="100%"  border="1" align="center"  bordercolor="#F7F5F4" bgcolor="#FFFFFF">
                                  <tr>
                                        <td width="450" align="center" class="mensajes"><%= msj %></td>
                                        <td width="29" background="<%=BASEURL%>/images/cuadronaranja.JPG">&nbsp;</td>
                                        <td width="58">&nbsp; </td>
                                  </tr>
                             </table>
                        </td>
                      </tr>
                </table>           
                
                <% if( lista.size()==0 ){%>
                      <p> 
                          <img src="<%=BASEURL%>/images/botones/salir.gif"            height="21"  title='Salir'      onClick="window.close();"               onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" style="cursor:hand">           
                     </p>
                <%}%>
                
    <%}%>
  
   
</div>   
      
  
  
</body>
</html>
