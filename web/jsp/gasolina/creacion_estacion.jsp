<%@page session="true"%> 
<%@page errorPage="/error/ErrorPage.jsp"%>
<%@page import="java.*" %>
<%@page import="java.util.*" %>
<%@include file="/WEB-INF/InitModel.jsp"%>
<%@page import="com.tsp.util.Util"%>
<%@page import="com.tsp.operation.model.beans.Usuario"%>

<%@page contentType="text/html"%>
<%@page import="com.tsp.operation.model.*"%>
<%@page import="com.tsp.operation.model.beans.*"%>

<%@ taglib uri="http://jakarta.apache.org/taglibs/input-0.90" prefix="input" %>

<html>
<head>
          <title>Estaci�n</title>
          <link href="<%= BASEURL %>/css/estilostsp.css" rel="stylesheet" type="text/css"> 
          <script language="JavaScript" type="text/javascript" src="<%=BASEURL%>/js/boton.js"></script>
     		<script src='<%=BASEURL%>/js/validar.js'></script>

          
          <script>
              function send(theForm, evento){
                 var  sw    = 0;
                 var  url   =  theForm.action;
                 var  emono = false;
                 
                 if(evento=='SAVEESTACION' ||  evento=='UPDATEESTACION' )
                   for(var i=0;i<theForm.length;i++){
                      
                         if( theForm.elements[i].type=='text' &&  ( theForm.elements[i].name=='nit'  
						 	|| theForm.elements[i].name=='tel'  ||  theForm.elements[i].name=='dir' ||  
							theForm.elements[i].name=='id' ||  theForm.elements[i].name=='nombre'   )  &&   
							theForm.elements[i].value=='' && theForm.elements[i].name!='e_mail' ){
                               sw = 1;
                               alert('Deber� llenar el campo ' + theForm.elements[i].title );
                               theForm.elements[i].focus();
                               break;
                         }
                      
                      
					  if( theForm.elements[i].name == 'e_mail' ){
					  		if( theForm.elements[i].value != '' && isEmail(theForm.elements[i].value) == false){
						    	theForm.elements[i].value='';
							}
					  }
						 
                   }
                    
                if(sw==0){
                                       
                         if(  isNaN( theForm.nit.value )  ){
                              alert('Deber� digitar solo n�meros en campos num�ricos; por favor verifique...');
                         }else{
                                theForm.action += evento;
                                theForm.submit();
                                theForm.action  = url;  
                        }
                    
                }
              }
			  function isEmail(s){
			  		var t = s; 
			  		var positionOfAt;
					var positionOfDot;
					positionOfAt  = s.indexOf('@',1);
					positionOfDot = t.lastIndexOf('.');
					if( (positionOfAt == -1) || (positionOfDot == -1) || (positionOfAt == (s.length-1)) || (positionOfDot == (s.length-1)) || (positionOfAt > positionOfDot) ){
						alert('Direcci�n e-mail en formato no v�lido, por lo tanto no se modificara el campo');
						return false;
					}
					return true;
			  }
          </script>
          
</head>
<body>
 
<div id="capaSuperior" style="position:absolute; width:100%; height:100px; z-index:0; left: 0px; top: 0px;">

<jsp:include page="/WEB-INF/toptsp.jsp?encabezado=Crear Estaci�n de Gasolina" />
    
</div>

<div id="capaCentral" style="position:absolute; width:100%; height:85%; z-index:0; left: 0px; top: 100px; overflow: scroll;"> 
<center>
  

  <% Usuario   User           = (Usuario)session.getAttribute("Usuario");//
     String    usuario        = User.getLogin();   //
     String    distrito       = User.getDstrct();//

	 List      tcuenta        = model.CuentaBancoSvc.getTipoCuenta();  //   
     //Hashtable cuenta         = model.CuentaBancoSvc.getCuentaPropietio();
     //List      tlugar        = model.AnticiposGasolinaService.getLugares();     
	 //List      lLugares        = model.CuentaBancoSvc.getListaBancos();    
     String    tel           = (request.getParameter("tel")      == null)?"":request.getParameter("tel"); //
     String    nit            = (request.getParameter("nit")       == null)?"":request.getParameter("nit"); // 
     //String    banco          = (request.getParameter("banco")     == null)?"":request.getParameter("banco");
     String    dir       = (request.getParameter("dir")  == null)?"":request.getParameter("dir");//
     String    nombre        = (request.getParameter("nombre")   == null)?"":request.getParameter("nombre");//
	 String    nombre_estacion        = (request.getParameter("nombre_estacion")   == null)?"":request.getParameter("nombre_estacion");//
	 String    login_estacion        = (request.getParameter("login_estacion")   == null)?"":request.getParameter("login_estacion");//
     //String    nitCta         = (request.getParameter("nitCta")    == null)?"":request.getParameter("nitCta");
     String    id            = (request.getParameter("id")     == null)?"":request.getParameter("id");//
     //String    tipoCta        = (request.getParameter("tipoCta")   == null)?"":request.getParameter("tipoCta");
     //String    tipo_descuento        = (request.getParameter("tipo_descuento")   == null)?"":request.getParameter("tipo_descuento");
     String    tipo_id        = (request.getParameter("tipo_id")   == null)?"":request.getParameter("tipo_id");//
     
     //String descuento="0";
     String    descuento      = (request.getParameter("descuento") == null)?"0":request.getParameter("descuento");//navi_si descuento es null se propone un descuento, sea para anticipo (opcion1) o para pronto pago (opcion2) . ej: 1.4//navi_nov6
               
     //String    secuencia      = (request.getParameter("secuencia") == null)?"0":request.getParameter("secuencia");
     //String    primaria       = (request.getParameter("primaria")  == null)?"N":request.getParameter("primaria"); 
	 String    e_mail         = (request.getParameter("e_mail")    == null)?"":request.getParameter("e_mail");//
     String    msj            = request.getParameter("msj"); 
	 
	 String 	 origen = request.getParameter("c_origen");
	 String 	 cpaiso = request.getParameter("c_paiso");
	 String 	 cpaisd = request.getParameter("c_paisd");
	 if (cpaiso==null){	 	cpaiso="";	 }
	 if (cpaisd==null){	 	cpaisd="";	 }	 
	 if (origen==null){	 	origen="";	 }
	 	 
	 %>
     
     
   <FORM method='post' action="<%= CONTROLLER %>?estado=Anticipo&accion=Gasolina&opcion=" name='forma'  id='forma' >
   
   <input name="pagina" type="hidden" id="pagina" value="<%=CONTROLLER%>?estado=Cargar&accion=Variosruta&carpeta=jsp/fintra&pagina=creacion_estacion.jsp">
   <input name="c_via" type="hidden" id="c_via" value=">
   
    <table width="620" border="2" align="center">
       <tr>
          <td>  
               <table width='100%' align='center' class='tablaInferior'>

                      <tr class="barratitulo">
                        <td colspan='4' >
                           <table cellpadding='0' cellspacing='0' width='100%'>
                             <tr>
                              <td align="left" width='55%' class="subtitulo1">&nbsp;ESTACION DE GASOLINA</td>
                              <td align="left" width='*'  ><img src="<%=BASEURL%>/images/titulo.gif" width="32" height="20"  height="20" align="left"></td>
                            </tr>
                           </table>
                        </td>
                     </tr>
                     
                     
                      <TR class='fila'>
                             <TD width='22%' >&nbsp <b>NIT</TD>  
                        <TD width='32%'>
                                        <input   style="width:70%" class='comentario'   type='text' name='nit'  id='nit'  value='<%=nit %>'   title='NIT'         maxlength='15'>
										
										<img src="<%=BASEURL%>/images/botones/iconos/new.gif"    title='Listar estaciones'      onclick="if( true ) {var x = window.open('<%= CONTROLLER %>?estado=Anticipo&accion=Gasolina&opcion=LISTAR_ESTACIONES&nit='+ nit.value,'rare',   '  top=100,left=100, width=600, height=500, scrollbars=yes, status=yes, resizable=yes' );  }else{ alert('Deber� digitar el nit');  nit.focus();  }">
										
                             </TD>      
                             <TD width='20%' class='fila'>&nbsp <b>TELEFONO</TD>  
                        <TD width='*'  >
                                        <input   style="width:100%" class='comentario'  type='text' name='tel' id='tel'      value='<%=tel %>'    title='TELEFONO'  maxlength='21'>
                          </TD>
                 </TR>       
                            
                           
 
                       
                       
                       
                       
                       <TR  class='fila'>
                                <TD  class='comentario'>&nbsp <b>PAIS</TD>  
								<!--  onChange="cargarSelects('<%//=CONTROLLER%>?estado=Cargar&accion=Variosruta&carpeta=jsp/fintra&pagina=creacion_estacion.jsp');"--> 
                                <td width="274" valign="middle"><select name="c_paiso" id="select" class="textbox" 
								
								>
									<%if (model.paisservice.existePaises()){
										model.paisservice.listarpaises();
										Pais pais; 
										List paises;
										paises = model.paisservice.obtenerpaises();		
										Iterator it = paises.iterator();%>
										<option value="">Seleccione</option>
									<%
									while (it.hasNext()){  
										pais = (Pais) it.next();
										if(cpaiso.equals(pais.getCountry_code())){
									%>
										<option value="<%=pais.getCountry_code()%>" selected><%=pais.getCountry_name()%></option>
									<%}else{%>
										<option value="<%=pais.getCountry_code()%>"><%=pais.getCountry_name()%></option>
									<%}
									}
								}%>
								</select></td>  
								 
                              <TD  class='comentario'>&nbsp <b>CIUDAD</TD>   
                              <td valign="middle">
							  <input type="text" name="c_origen" id="select4" class="textbox"  style="width:100%"  value="<%=origen%>" >
								
							  </td>
                       </TR>
                           
                       
                       
                           <TR   class='fila'>
                                <TD class='comentario'>&nbsp <b>ID RESPONSABLE </TD>   
                                <td><input value='<%= id%>'   style="width:100%" class='comentario' type='text' name='id'    title='ID'          maxlength='20'></td>
                                <TD height='25' class='comentario'>&nbsp <b>TIPO ID</TD>   
                                <td colspan='1'>
                                    <select name='tipo_id'  class='comentario' style="width:100%">
                                        <option value='01' <%if (tipo_id.equals("01")){%> selected <% }%> >C�dula de ciudadan�a
                                        <option value='02' <%if (tipo_id.equals("02")){%> selected <% }%> >C�dula de extranjer�a
                                        <option value='03' <%if (tipo_id.equals("03")){%> selected <% }%> >N.I.T.
                                        <option value='04' <%if (tipo_id.equals("04")){%> selected <% }%> >Tarjeta de identidad
                                        <option value='05' <%if (tipo_id.equals("05")){%> selected <% }%> >Pasaporte
                                        <option value='06' <%if (tipo_id.equals("06")){%> selected <% }%> >Sociedad extranjera sin nit
                                        <option value='07' <%if (tipo_id.equals("07")){%> selected <% }%> >Fideicomiso
                                   </select>
                                </td>
                           </TR>
                           
                           
                           <TR   class='fila'>
                                <TD height='25' class='comentario'>&nbsp <b>NOMBRE RESPONSABLE</TD>   
                                <td colspan='3'><input value='<%= nombre%>'   style="width:100%" class='comentario' type='text' name='nombre'   title='NOMBRE' maxlength='100'></td>
                           </TR>
                          
						   <TR   class='fila'>
	 						    <TD height='25' class='comentario'>&nbsp <b>DIRECCION</TD>   
                                <td colspan='1'>
								<input value='<%= dir%>'  style="width:100%" class='comentario' type='text' name='dir'  title='DIRECCION'   maxlength='50'></td>

						   	  	<TD height='25' class='comentario'>&nbsp <b>E_MAIL</TD>   
                                <td colspan='1'><input value='<%=e_mail%>'   style="width:100%" class='comentario' type='text' name='e_mail' title='e_mail' maxlength='40'></td>
                                                                
						   </TR>
						   <TR   class='fila'>
                                <TD height='25' class='comentario'>&nbsp <b>NOMBRE ESTACION</TD>   
                                <td colspan='3'><input value='<%= nombre_estacion%>'   style="width:100%" class='comentario' type='text' name='nombre_estacion'   title='NOMBRE_ESTACION' maxlength='100'></td>
                           </TR>
                           <TR   class='fila'>
                                <TD height='25' class='comentario'>&nbsp <b>LOGIN ESTACION</TD>   
                                <td colspan='3'><input value='<%= login_estacion%>'   style="width:100%" class='comentario' type='text' name='login_estacion'   title='LOGIN' maxlength='100'></td>
                           </TR>
                           
                       </table>
         </td>
      </tr>
   </table>   
   
   
     <input type='hidden' name='distrito'     value='<%= distrito   %>'> 
     <input type='hidden' name='usuario'      value='<%= usuario    %>'> 
          
 </FORM>
 
 
 
 <p> 
         <img src="<%=BASEURL%>/images/botones/aceptar.gif"          height="21"  title='Guardar'             onClick="send(forma, 'SAVEESTACION')"      onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" style="cursor:hand">
         <img src="<%=BASEURL%>/images/botones/modificar.gif"        height="21"  title='Modificar'           onClick="send(forma, 'UPDATEESTACION')"    onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" style="cursor:hand">
          <img src="<%=BASEURL%>/images/botones/restablecer.gif"     height="21"  title='Limpiar formulario'  onclick="location.href ='<%=CONTROLLER%>?estado=Anticipo&accion=Gasolina&opcion=INIT';"   onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" style="cursor:hand">  
         <img src="<%=BASEURL%>/images/botones/salir.gif"            height="21"  title='Salir'              onClick="window.close();"                    onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" style="cursor:hand">
  
 </p>
 
 
 
 
  
  
  <% if(msj!=null  &&  !msj.equals("") ){%>
                <br>
                <table border="2" align="center">
                      <tr>
                        <td>
                            <table width="100%"  border="1" align="center"  bordercolor="#F7F5F4" bgcolor="#FFFFFF">
                                  <tr>
                                        <td width="450" align="center" class="mensajes"><%= msj %></td>
                                        <td width="29" background="<%=BASEURL%>/images/cuadronaranja.JPG">&nbsp;</td>
                                        <td width="58">&nbsp; </td>
                                  </tr>
                          </table>
                        </td>
                      </tr>
                </table>           
    <%}%>
           
</div>


</body>
</html>
