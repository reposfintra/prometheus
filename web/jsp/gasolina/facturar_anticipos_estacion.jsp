<%--
    Document   : mostrarCuentasProveedor
    Created on : 15/11/2007, 03:58:14 PM
    Author     : iamorales
--%>

<%@ page session   ="true"%>
<%@ page errorPage ="/error/ErrorPage.jsp"%>
<%@ page import    ="java.util.*" %>
<%@ page import    ="com.tsp.util.*" %>
<%@ page import    ="com.tsp.operation.model.beans.*" %>
<%@ include file   ="/WEB-INF/InitModel.jsp"%>

<%
	String path = application.getRealPath(model.menuService.getRealUrl(request.getRequestURI()));
	String datos[] = model.menuService.getContenidoMenu(BASEURL,request.getRequestURI(),path);

	Usuario usuario = (Usuario) session.getAttribute("Usuario");
	String loginx=usuario.getLogin();

        //String cond = (request.getParameter("cond")!=null && !request.getParameter("cond").equals(""))?request.getParameter("cond"):"prede.jpg";
        //String firma = (request.getParameter("firma")!=null)?request.getParameter("firma"):"none.jpg";

%>
<html>
<head>
	<title>Gesti�n de Anticipos de Estaciones de Gasolina</title>
    <link href="<%= BASEURL %>/css/estilostsp.css"     rel="stylesheet" type="text/css">
    <link href="/css/estilostsp.css" rel="stylesheet"  type="text/css">
    <script type='text/javascript' src="<%= BASEURL %>/js/boton.js"></script> 
    <link href="<%=BASEURL%>/css/jquery/jquery-ui/jquery-ui.css" type="text/css" rel="stylesheet" />
    <style>
        .ventana{
            display:none;    
            font-family:Arial, Helvetica, sans-serif;
            color:#808080;
            font-size:12px;
            text-align:justify;

        }
    </style>
    <script>
		var cuenta=0;
            function validar(theForm) {               
                       
                        if (cuenta == 0)
                        {   divBtnAceptar = document.getElementById('BtnAceptar');
                            divBtnAceptar.disabled= true;
                            cuenta=1;
                            var elementos = document.getElementsByName("idAnticipo");
                            var sw = false;
                            for (var i=0;i<elementos.length; i++){
                                if (elementos[i].checked){
                                    sw = true;
                                }
                            }
                            if(sw==true && cuenta==1){
                               divEspera("espera un momento miestras se genera la cuenta de cobro", "300","150");
                               setTimeout(function() {
                               theForm.submit();
                               }, 1500);
                            }
                            else{                              
                                alert("Verifique que haya al menos un anticipo seleccionado");
                            }
                            cuenta=0;
                            divBtnAceptar = document.getElementById('BtnAceptar');
                            divBtnAceptar.disable=false;
                        }
                        else
                        {                           
                            alert("El formulario ya est� siendo enviado, por favor aguarde un instante.");
                        }
                
            }
                
                
                    function divEspera(msj, width, height) {

                        $("#msj2").html(msj);
                        $("#dialogo2").dialog({
                            width: width,
                            height: height,
                            show: "scale",
                            hide: "scale",
                            resizable: false,
                            position: "center",
                            modal: true,
                            closeOnEscape: false
                        });

                        $("#dialogo2").siblings('div.ui-dialog-titlebar').remove();


                    }

		function marcarIds(theForm){//090830
			var idss = document.getElementById("idsx").value;
			var marcadosx=0;//090831
			//alert("ids:"+idss+"_");
			idss=			replaceAll(idss,"\n",'.');
			idss=			replaceAll(idss,"\r",'.');
			//alert("ids::"+idss+"_");

			var lista = idss.split('..');
			//alert("_"+lista[0]+"_"+lista[1]+"_..."+"lista.length"+lista.length);

			for (i=0;i<theForm.length;i++){

			  	if (theForm.elements[i].type=='checkbox' && theForm.elements[i].name=="idAnticipo"){
					//alert("i"+i);
					for (j=0;j<lista.length;j++){
						//alert("theForm.elements[i].id"+theForm.elements[i].id+"lista[j]"+lista[j]+"_");
						//if (theForm.elements[i].id==lista[j]){
						if (theForm.elements[i].id==lista[j] && theForm.elements[i].checked==false){//090831
							theForm.elements[i].checked=true;
							marcadosx=marcadosx+1;//090831
						}
					}
				}
			}
			alert("Cantidad de anticipos marcados: "+marcadosx+".");//090831
		}

		function formatx(input){//090831
		  	var num=""+input;
			num = num.replace(/\./g,'');
			if(!isNaN(num)){
				num = num.toString().split('').reverse().join('').replace(/(?=\d*\.?)(\d{3})/g,'$1.');
				num = num.split('').reverse().join('').replace(/^[\.]/,'');
				input = num;
			}else{
				alert('Solo se permiten n�meros');
				//input.value = input.value.replace(/[^\d\.]*/g,'');
			}
			return num;
		}

		function replaceAll(cad,pcFrom, pcTo){//090830
			var i = cad.indexOf(pcFrom);
			var c = cad;

			while (i > -1){
				c = c.replace(pcFrom, pcTo);
				i = c.indexOf(pcFrom);
			}
			return c;
		}


		function Sell_all_col(theForm,nombre){
			 for (i=0;i<theForm.length;i++)
					  if (theForm.elements[i].type=='checkbox' && theForm.elements[i].name==nombre)
						 theForm.elements[i].checked=theForm.All.checked;
	    }

		function send(theForm){
			theForm.submit();
		}

		function sumar_valores(theForm){
			var elementos = document.getElementsByName("idAnticipo");
			var elementos_valores = document.getElementsByName("valor");
			var subtotal=0;
			var tem="";
			for (var i=0;i<elementos.length; i++){

				if (elementos[i].checked){
					for (var j=0;j<elementos_valores.length; j++){
						//alert("id_valor"+elementos_valores[j].id);
						var emp = elementos[i].value.split(';_')[0];
						//alert("El id es: "+emp);
						if (emp==elementos_valores[j].id){
							//alert("igualess");
							tem=elementos_valores[j].value.replace(",","");
							tem=tem.replace(",","");
							subtotal=parseInt(subtotal)+parseInt(tem);
						}
					}
				}
			 }
			 alert("Subtotal seleccionado = "+formatx(subtotal));	   //090831
		}

    </script>
</head>
<body onLoad="redimensionar();" onResize="redimensionar();"><!--redimensionar-->

<div id="capaSuperior" style="position:absolute; width:100%; height:100px; z-index:0; left: 0px; top: 0px;">
<jsp:include page="/WEB-INF/toptsp.jsp?encabezado=Anticipos de Estaci�n"/>
</div>

<div id="capaCentral" style="position:absolute; width:100%; height:85%; z-index:0; left: -1px; top: 100px; overflow: scroll;">



<%

   String estaci=request.getParameter("estacion");
   if (estaci==null){estaci="";}
   //out.println("estaci"+estaci);
   ArrayList anticiposGasolina ;
   try {
	   anticiposGasolina = model.AnticiposGasolinaService.getAnticiposGasolinaAprobados(estaci);
   }catch(Exception e ){
   		System.out.println("errorcillo en model.AnticiposGasolinaService.getAnticiposGasolinaAprobados()");
		anticiposGasolina=null;
   }
   if (anticiposGasolina!=null  ){
%>
    <form action="<%=CONTROLLER%>?estado=Anticipo&accion=Gasolina&opcion=facturarAnticipoGas" method="post" name="formulario">
    <input type="hidden" name="estacion" value="<%=estaci%>">
    <%
        boolean isStat = false;//2010-09-28
        try{
            isStat = model.AnticiposGasolinaService.esEstacionUsuario(loginx);//2010-09-28
        }
        catch(Exception e){
            System.out.println("error: "+e.toString());//2010-09-28
            e.printStackTrace();
        }

    String respuesta=request.getParameter("respuesta");
	//String valor_neto="";//request.getParameter("valor_neto");

    %>


    <table width="689"  border="2" align="center">
     <tr>
         <td >

                <table width="100%"  align="center">

                  <tr class='fila'>
                     <td>

							<table width="100%" border="1" align="center" bordercolor="#999999" bgcolor="#F7F5F4">
								<tr class="fila">
									<TH   nowrap style="font-size:11; font-weight: bold" > <input type='checkbox'  id='All' onclick="Sell_all_col(this.form,'idAnticipo');">  </TH>
                                                                        <%
                                                                            if(isStat==false){//2010-09-28
                                                                        %>
                                                                        <th > &nbsp;ID&nbsp;  </th>
                                                                        <%
                                                                            }
                                                                        %>

									<th > &nbsp;PLANILLA&nbsp;  </th>
									<th > FECHA</th>
                                                                                                                                                                                                                            <%
                                                                                if(isStat==false){//2010-09-28
                                                                            %>
                                                                            <th> &nbsp;VALOR ANTICIPO&nbsp;      </th>
                                                                            <th > SUMA FIN</th>
                                                                            <th > SUMA TSP</th>
                                                                            <th > RESTA</th>
                                                                            <th > CXP1 TSP</th>
                                                                            <th > CORRIDA</th>
                                                                            <th > VALOR</th>
                                                                            <th > CXP2 TSP</th>
                                                                            <th > CORRIDA</th>
                                                                            <th > VALOR</th>
                                                                            <%
                                                                                }
                                                                            %>
                                                                        <th > CONDUCTOR  </th>
																		<%
                                                                            if(isStat==false){//2010-09-28
                                                                        %>
                                                                        <th > &nbsp;PREANTICIPOS&nbsp;  </th>
                                                                        <%
                                                                            }
                                                                        %>
                                                                        
                                                                        <th > &nbsp;VALOR ENTREGADO&nbsp; </th>
                                                                        <th > PROPIETARIO </th>
																		<th > &nbsp;APROBADOR&nbsp;</th>
																		<th > &nbsp;FECHA APROBACION&nbsp;</th>
																		<th > &nbsp;ESTACION&nbsp;</th>
                              </tr>
								<%
                                                                        //String banc,cuent,estado_aprobacion,nombre,secuenciax,dstrctx,nitx;
                                                                        //nitx=request.getParameter("proveedor");
                                                                        AnticipoGasolina anticipoGasolina;
                                                                        for (int i=0; i<anticiposGasolina.size(); i++) {
                                                                            anticipoGasolina=(AnticipoGasolina)anticiposGasolina.get(i);
                                                                            //valor=""+anticipoGasolina.getVlr()    ;
																			//valor_neto=""+anticipoGasolina.getVlrNeto()    ;
                                                                        	String valor_preanticipos=anticipoGasolina.getVlr_preanticipos();
																			if (valor_preanticipos==null || valor_preanticipos.equals("")){
																				valor_preanticipos="0";
																			}
                                                                        %>

								<tr class='<%= (i%2==0?"filagris":"filaazul") %>' id='fila<%=i%>'   style=" font-size:12"   >

																			    <td class="bordereporte" align='center' nowrap style="font-size:11">
																				<%
																					//item;_descripcion;_estacion;_valor
																					String value = anticipoGasolina.getId()+";_"+anticipoGasolina.getPlanilla()+";_"+estaci+";_"+anticipoGasolina.getVlrNeto();
																					String cxp_doc_value = "";
																					//System.out.println("El valor de value es: "+value);
																				%>
 																			    	<input type='checkbox' name='idAnticipo' id="<%=anticipoGasolina.getId()%>"  value='<%= value%>' onclick=" cambiarColorMouse(fila<%=i%>); ">

				   															    </td>
				   															    <%
                                                                                                                                                                if(isStat==false){//2010-09-28
                                                                                                                                                            %>
                                                                                                                                                            <td class="bordereporte" nowrap align="center"> <%= anticipoGasolina.getId()    %></td><!--090829-->
                                                                                                                                                            <%
                                                                                                                                                                }
                                                                                                                                                            %>
                                                                                <td class="bordereporte" nowrap align="center"> <a target="_blank" href="<%=BASEURL%>/jsp/applus/importar.jsp?num_osx=<%=  anticipoGasolina.getPlanilla()%>&tipito=planilla"> <%=  anticipoGasolina.getPlanilla()%> </a></td>
                                                                                <td class="bordereporte" nowrap align="center"> <a target="_blank" href="<%=BASEURL%>/jsp/applus/mostrar_archivos.jsp?num_osx=<%= anticipoGasolina.getPlanilla()%>&tipito=planilla"> <%=  (anticipoGasolina.getFecha_anticipo()).substring(0,10)    %> </a></td>

                                                                                <%
                                                                                        if(isStat==false){//2010-09-28
                                                                                    %>
                                                                                    <td class="bordereporte" nowrap align="center"> <%= com.tsp.util.Util.customFormat(anticipoGasolina.getVlr())    %></td>

                                                                                    <td class="bordereporte" nowrap align="center"> <%= com.tsp.util.Util.customFormat(Double.parseDouble(anticipoGasolina.getSumaAnticiposDplanilla()))%></td>
                                                                                    <td class="bordereporte" nowrap align="center"> <%= com.tsp.util.Util.customFormat(Double.parseDouble(anticipoGasolina.getSumaCxpsTspDplanilla()))%></td>
                                                                                    <td class="bordereporte" nowrap align="center"> <%= com.tsp.util.Util.customFormat(Double.parseDouble(anticipoGasolina.getSumaCxpsTspDplanilla())-Double.parseDouble(anticipoGasolina.getSumaAnticiposDplanilla()))%></td>
                                                                                    <%String[] CxpTspPosible=anticipoGasolina.getCxpTspPosible_corrida_cheque().split("_");%>
                                                                                    <td class="bordereporte" nowrap align="center"> <% if (CxpTspPosible.length>0){out.print(CxpTspPosible[0]);}else{out.print("");} %></td>
                                                                                    <td class="bordereporte" nowrap align="center"> <% if (CxpTspPosible.length>1){out.print(CxpTspPosible[1]);}else{out.print("");} %></td>

                                                                                    <td class="bordereporte" nowrap align="center"> <% if (CxpTspPosible.length>3){out.print(com.tsp.util.Util.customFormat(Double.parseDouble(CxpTspPosible[3])));}else{out.print("");} %></td>
                                                                                    <%CxpTspPosible=anticipoGasolina.getCxpTspPosible2_corrida_cheque().split("_");%>
                                                                                    <td class="bordereporte" nowrap align="center"> <% if (CxpTspPosible.length>0){out.print(CxpTspPosible[0]);}else{out.print("");} %></td>
                                                                                    <td class="bordereporte" nowrap align="center"> <% if (CxpTspPosible.length>1){out.print(CxpTspPosible[1]);}else{out.print("");} %></td>

                                                                                    <td class="bordereporte" nowrap align="center"> <% if (CxpTspPosible.length>3){out.print(com.tsp.util.Util.customFormat(Double.parseDouble(CxpTspPosible[3])));}else{out.print("");} %></td>
                                                                                    <%
                                                                                        }
                                                                                    %>

                                                                                <td class="bordereporte" nowrap align="center"> <%= anticipoGasolina.getNombreConductor()    %>&nbsp;-&nbsp;<%=anticipoGasolina.getConductor()%></td>

																				<%
                                                                                                    if(isStat==false){//2010-09-28
                                                                                                %>
                                                                                                <td class="bordereporte" nowrap align="center">
                                                                                                        <%= com.tsp.util.Util.customFormat(Double.parseDouble(valor_preanticipos))    %>
                                                                                                </td>
                                                                                                <%
                                                                                                    }
                                                                                                %>

																				<td class="bordereporte" nowrap align="center">
																				 	<%= com.tsp.util.Util.customFormat(anticipoGasolina.getVlrNeto())    %>
																				</td>
																				<input type="hidden" name="valor" value="<%= com.tsp.util.Util.customFormat(anticipoGasolina.getVlrNeto())    %>" id="<%= anticipoGasolina.getId()%>">
                                                                                <td class="bordereporte" nowrap align="center"> <%= anticipoGasolina.getPla_owner()+" - "+anticipoGasolina.getNombrePropietario()%></td>
																				<td class="bordereporte" nowrap align="center"> <%= anticipoGasolina.getUser_autorizacion()%></td>
																				<td class="bordereporte" nowrap align="center"> <%= (anticipoGasolina.getFecha_autorizacion()).substring(0,16)%></td>

																				<td class="bordereporte" nowrap align="center"> <%=anticipoGasolina.getAsesor()%></td>
                              </tr>
                                                                            <% //System.out.println("i"+i); %>
								<% } %>

							</table>

					 </td>
                  </tr>

                </table>

        </td>
      </tr>
      </table>

        <br>
        <center>
		<a href="javascript: void(0);" class="Simulacion_Hiper" onClick= " sumar_valores(formulario); "  title="Calcular subtotal" >Calcular subtotal</a>
		<br><br>
		<input type="hidden" id="documento" name="documento">
		<br><br>
		<img src="<%=BASEURL%>/images/botones/aceptar.gif" id="BtnAceptar"   height="21"  title='Aceptar'    onclick='validar(formulario)'       onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" style="cursor:hand">
		<img src='<%=BASEURL%>/images/botones/regresar.gif'   style=" cursor:hand"  title='Regresar...'     name='i_regresar'       onclick="document.location = '<%= BASEURL %>/jsp/gasolina/filtro_consulta_anticipos_gasolina_fac.jsp' "     onMouseOver='botonOver(this);' onMouseOut='botonOut(this);'>
        <img src='<%=BASEURL%>/images/botones/salir.gif'      style='cursor:hand'    title='Salir...'      name='i_salir'       onclick='parent.close();'           onMouseOver='botonOver(this);' onMouseOut='botonOut(this);'>

		</center>
	     <br>
        <%
       if (respuesta!=null){//request.getParameter("resultado")!=null && request.getParameter("resultado").equals("ok") ){
            %>
            <br><br>
            <p><table border="2" align="center">
              <tr>
                <td><table width="100%"  border="1" align="center"  bordercolor="#F7F5F4" bgcolor="#FFFFFF">
                  <tr>
                    <td width="170" align="center" class="mensajes"><%=""+respuesta%></td>
                    <td width="29" background="<%=BASEURL%>/images/cuadronaranja.JPG">&nbsp;</td>
                    <td width="40">&nbsp;</td>
                  </tr>
                </table></td>
              </tr>
            </table>
            </p>

            <%
        }

  %>
  	  <%
        if(isStat==false){//2010-09-28
        %>
              <table bgcolor="#F7F5F4" align="center"><!--090830-->
                            <tr  class="fila">
                              <td class="bordereporte" nowrap align="center">Ids</td>
                            </tr>
                            <tr  class="fila"><td class="bordereporte" nowrap align="center"><textarea cols="30" rows="10" name="idsx"></textarea></td></tr>
                            <tr  class="fila"><td class="bordereporte" nowrap align="center">
                                    <a href="javascript: void(0);" class="Simulacion_Hiper" onClick= " marcarIds(formulario); "  title="Marcar Ids" >Marcar Anticipos</a>
                            </td></tr>
                    </table>
       <%
            }
       %>

 </form>
 <br>

    <% }else{
    %>lista de cuentas vac�a. raro...<%
   }
%>
</div>
<%=datos[1]%>

<div id="dialogo2" class="ventana">
    <p  id="msj2">texto </p> <br/>
    <center>
        <img src="<%=BASEURL%>/images/cargandoCM.gif"/>
    </center>
</div>
</body>
</html>
<script type="text/javascript" src="<%=BASEURL%>/js/jquery/jquery.jqGrid/js/jquery.min.js"></script>
<script type="text/javascript" src="<%=BASEURL%>/js/jquery-ui-1.8.5.custom.min.js"></script>

