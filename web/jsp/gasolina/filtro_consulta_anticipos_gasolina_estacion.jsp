<!--
- Autor      : Ing. Ivan Morales
- Date       : feb 2008
- Copyrigth Notice : Transporte Sanchez Polo S.A
-->
<%--
-@(#)
--Descripcion : Vista que permite consultar anticipos de gasolina
--%>


<%-- Declaracion de librerias--%>
<%@page contentType="text/html"%>
<%@page session="true"%>
<%@page errorPage="../error/ErrorPage.jsp"%>
<%@page import="java.util.*"%>
<%@page import="com.tsp.util.*"%>
<%@include file="/WEB-INF/InitModel.jsp"%>


<html>
<head>
        <title>Consulta Anticipos Estaci�n</title>
        <link href="<%= BASEURL %>/css/estilostsp.css" rel="stylesheet" type="text/css"> 
	<script language="JavaScript" type="text/javascript" src="<%=BASEURL%>/js/boton.js"></script> 
	
	<script>
	   function send(theForm){
	           var sw   = 0;
	           var cont = 0;
	           for(var i=0;i<theForm.length;i++){
                      var ele = theForm.elements[i];
                      if(ele.type=='checkbox' && ele.checked  ){
                         cont++;
                         if( ele.name=='ckAgencia')         {  if( theForm.Agencia.value        ==''){  alert('Deber� seleccionar la agencia');                  sw=1; theForm.Agencia.focus();         break;  }  }
                         if( ele.name=='ckPropietario')     {  if( theForm.Propietario.value    ==''){  alert('Deber� establecer nit del propietario');          sw=1; theForm.Propietario.focus();     break;  }  } 
                         if( ele.name=='ckPlanilla')        {  if( theForm.Planilla.value       ==''){  alert('Deber� establecer la planilla');                  sw=1; theForm.Planilla.focus();        break;  }  }
                         if( ele.name=='ckPlaca')           {  if( theForm.Placa.value          ==''){  alert('Deber� establecer la placa');                     sw=1; theForm.Placa.focus();           break;  }  }                      
                         if( ele.name=='ckConductor')       {  if( theForm.Conductor.value      ==''){  alert('Deber� establecer nit del Conductor');            sw=1; theForm.Conductor.focus();       break;  }  }
                         if( ele.name=='ckReanticipo')      {  if( theForm.reanticipo.value     ==''){  alert('Deber� establecer clase de registro');            sw=1; theForm.reanticipo.focus();      break;  }  }                          
                         if( ele.name=='ckLiquidacion')     {  if( theForm.Liquidacion.value    ==''){  alert('Deber� establecer n�mero de la Liquidaci�n');     sw=1; theForm.Liquidacion.focus();     break;  }  } 
                         //if( ele.name=='ckTransferencia')   {  if( theForm.Transferencia.value  ==''){  alert('Deber� establecer n�mero de la Transferencia');   sw=1; theForm.Transferencia.focus();   break;  }  } 
                         //if( ele.name=='ckFactura')         {  if( theForm.Factura.value        ==''){  alert('Deber� establecer n�mero de la factura');         sw=1; theForm.Factura.focus();         break;  }  } 
                         
                         
                      }
                   }
                   if(cont==0){
	              sw=1;
	              alert('Deber� seleccionar filtros de busqueda');
	           }
                   
                   if(sw==0)
	              theForm.submit();
	   }
	</script>
	
</head>

<body onLoad="redimensionar();" onResize="redimensionar();">


<%  String path    = application.getRealPath(model.menuService.getRealUrl(request.getRequestURI()));
    String datos[] = model.menuService.getContenidoMenu(BASEURL,request.getRequestURI(),path);    %>

<div id="capaSuperior" style="position:absolute; width:100%; height:100px; z-index:0; left: 0px; top: 0px;">
<jsp:include page="/WEB-INF/toptsp.jsp?encabezado=Anticipos de Estaci�n"/>
</div>

<div id="capaCentral" style="position:absolute; width:100%; height:85%; z-index:0; left: 0px; top: 100px; overflow: scroll;"> 
<center>
  

   <% List    agencias  =  model.AnticiposPagosTercerosSvc.getListAgencias(); 
      String  hoy       = Utility.getHoy("-");
      String  msj       = request.getParameter("msj");%>
      

   <form action="<%=CONTROLLER%>?estado=Anticipo&accion=Gasolina&opcion=CONSULTARPERIODO&menu=consultar_propio" method='post' name='formulario' >
 
   <table width="450" border="2" align="center">
       <tr>
          <td>  
               <table width='100%' align='center' class='tablaInferior'>

                  <tr class="barratitulo">
                    <td colspan='2' >
                       <table cellpadding='0' cellspacing='0' width='100%'>
                         <tr>
                          <td align="left" width='65%' class="subtitulo1">&nbsp;FILTROS DE CONSULTA</td>
                          <td align="left" width='*'  ><img src="<%=BASEURL%>/images/titulo.gif" width="32" height="20"  height="20" align="left"><%=datos[0]%></td>
                        </tr>
                       </table>
                    </td>
                 </tr>
                 

                 <tr  class="fila">
                       <td colspan='2' >
                       
                          <table class='tablaInferior' width='100%'>
                                                         
                               <tr  class="fila">
                                   <td             > <input type='checkbox'   name='ckFechas' value="1" disabled checked>   </td>
                                   <td             > Fecha Aprobaci�n                              </td>
                                   <td             > 
                                        
                                         <!-- Fecha Inicial -->
                                          <input id='fechaInicio' name='fechaInicio' size="20" readonly="true" class="textbox" value='<%=hoy%>' style='width:35%'> 
                                          <a href="javascript:void(0)" onClick="if(self.gfPop)gfPop.fPopCalendar(fechaInicio);return false;" hidefocus>
                                          <img name="popcal" align="absmiddle" src="<%=BASEURL%>/js/calendartsp/calbtn.gif" width="16" height="16" border="0" alt="">
                                          </a>
                                          &nbsp
                                          <!-- Fecha Final -->                   
                                         <input id='fechaFinal' name='fechaFinal' size="20" readonly="true" class="textbox" value='<%=hoy%>' style='width:35%'> 
                                         <a href="javascript:void(0)" onClick="if(self.gfPop)gfPop.fPopCalendar(fechaFinal);return false;" hidefocus>
                                          <img name="popcal" align="absmiddle" src="<%=BASEURL%>/js/calendartsp/calbtn.gif" width="16" height="16" border="0" alt="">
                                         </a>
                                   
                                   </td>
                               </tr>
                               
                          </table>
                       
                       </td>
                 </tr>
                 
                 

           </table>
         </td>
      </tr>
   </table> 
  
   <br>                 
   <img src="<%=BASEURL%>/images/botones/aceptar.gif"    height="21"  title='Consultar'  onclick='send(formulario);'   onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" style="cursor:hand">
   <img src="<%=BASEURL%>/images/botones/salir.gif"      height="21"  title='Salir'      onClick="window.close();"     onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" style="cursor:hand">
   
   
   
   
   
 
   <!-- MENSAJE -->  
   <% if(msj!=null  &&  !msj.equals("") ){%>
        <br><br>
        <table border="2" align="center">
              <tr>
                <td>
                    <table width="100%"  border="1" align="center"  bordercolor="#F7F5F4" bgcolor="#FFFFFF">
                          <tr>
                                <td width="450" align="center" class="mensajes"><%= msj %></td>
                                <td width="29" background="<%=BASEURL%>/images/cuadronaranja.JPG">&nbsp;</td>
                                <td width="58">&nbsp; </td>
                          </tr>
                     </table>
                </td>
              </tr>
        </table>  
                
    <%}%>

   
   
   
   
</div>
<!-- Necesario para los calendarios-->
<iframe width="188" height="166" name="gToday:normal:<%=BASEURL%>/js/calendartsp/agenda.js:gfPop:<%=BASEURL%>/js/calendartsp/plugins.js" id="gToday:normal:<%=BASEURL%>/js/calendartsp/agenda.js:gfPop:<%=BASEURL%>/js/calendartsp/plugins.js" src="<%=BASEURL%>/js/calendartsp/ipopeng.htm" scrolling="no" frameborder="0" style="visibility:visible; z-index:999; position:absolute; left:-500px; top:0px;"></iframe>


<%=datos[1]%>  


</body>
</html>
