<%-- 
    Document   : upload_filefact
    Created on : 15/04/2030, 08:00:22 AM
    Author     : bterraza
--%>

<%@page contentType="text/html" pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
        <title>Cargar Archivo Facturacion</title>

        <link href="./css/upload_filefact.css" rel="stylesheet" type="text/css">
        <link href="./css/popup_rl.css" rel="stylesheet" type="text/css">
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">


        <script type="text/javascript" src="./js/jquery/jquery.jqGrid/js/jquery.min.js"></script>
        <script type="text/javascript" src="./js/jquery-ui-1.8.5.custom.min.js"></script>
        <script type="text/javascript" src="./js/jquery/jquery.jqGrid-4.7.0/js/grid.locale-es.js"></script>
        <script type="text/javascript" src="./js/jquery/jquery.jqGrid-4.7.0/js/jquery.jqGrid.min.js"></script>
        <!--        <script type="text/javascript" src="./js/jquery/jquery.jqGrid-4.7.0/js/jquery.tabletojson.js"></script>-->
        <script type="text/javascript" src="./js/upload_filefact.js"></script>


    </head>    
    <body>
        <div id="capaSuperior" style="position:absolute; width:100%; height:100px; z-index:0; left: 0px; top: 0px;">
            <jsp:include page="/WEB-INF/toptsp.jsp?encabezado=Carga Archivo Facturacion"/>
        </div>



        <div id="capaCentral" class="container-fluid" style="width:100%; height:90%; z-index:0; left: 0px; margin-top: 150px; margin-bottom: 40px ">
            <div class="card" style="-webkit-box-shadow: 6px 10px 16px 0px rgba(0,0,0,0.75); -moz-box-shadow: 6px 10px 16px 0px rgba(0,0,0,0.75); box-shadow: 6px 10px 16px 0px rgba(0,0,0,0.75);">
                <div class="card-header" style="font-family: monospace; background: rgb(57, 126, 173); color: rgb(255, 255, 255); font-weight: bold">
                    Carga Archivo Facturacion
                </div>
                <div class="card-body">
                    <!--<h5 class="card-title">Special title treatment</h5>-->
                    <!--<p class="card-text">With supporting text below as a natural lead-in to additional content.</p>-->
                    <form id="formulario" name="formulario">
                        <div class="row">
                            <div class="col-sm">
                                <div class="form-group">
                                    <label for="fecha">Periodo</label>
                                    <input type="text" maxlength="6" class="form-control" id="periodo" name="periodo" style="font-family: monospace; width: 150px; height: 40px;">
                                    <small id="emailHelp" class="form-text text-muted">Se Tomara el periodo en curso!</small>
                                </div>
                            </div>
                            <div class="col-sm">
                                <div class="form-group">
                                    <label for="ciclo">Ciclo</label>
                                    <select class="form-control" id="ciclo" name="ciclo" style="font-family: monospace; width: 150px; height: 40px;">
                                        <option value="" selected>...</option>
                                        <option value="1">1</option> 
                                        <option value="2">2</option>
                                        <option value="3">3</option>
                                        <option value="4">4</option>
                                    </select>
                                </div>
                            </div>

                            <div class="col-sm">
                                <div class="form-group">
                                    <label for="file_excel">
                                        Elige un archivo</label>
                                    <input type="file" id="file_excel" name="file_excel" accept=".xls" style="font-family: monospace; height: 40px;">
                                </div>
                            </div>

                            <div class="col-sm">
                                <div class="form-group" style="display: grid;">
                                    <label for="upload">&nbsp;</label>
                                    <button type="button" id="upload" class="btn btn-secondary" style="width: 100%; font-family: monospace; height: 40px; font-weight: bold">Subir Archivo</button>
                                </div>
                            </div>
                            
                             <div class="col-sm">
                                <div class="form-group">
                                    <label for="ciclo">Lotes por generar</label>
                                    <select class="form-control" id="lote" name="lote" onchange="buscarExtractos(this.value)"style="font-family: monospace; width: 150px; height: 40px;">
                                        <option value="" selected></option>
                                    </select>
                                </div>
                            </div>
                            
                            <div class="col-sm">
                                <div class="form-group">
                                    <label for="ciclo">Sms por generar</label>
                                    <select class="form-control" id="sms" name="sms" onchange="buscarExtractosPorEnviar(this.value)"style="font-family: monospace; width: 150px; height: 40px;">
                                        <option value="" selected></option>
                                    </select>
                                </div>
                            </div>
                        </div>
                    </form>

                    <!--Previsualizacion de archivo-->

                    <div id="capaCentralprev" class="container" style="display: none; width:95%; height:95%; z-index:0; left: 0px; margin-top: 40px; margin-bottom: 40px ">
                        <div class="card" style="">
                            <div class="card-header" style="font-family: monospace; background: rgb(57, 126, 173); color: rgb(255, 255, 255); font-weight: bold">
                                Previsualizacion
                            </div>
                            <div class="card-body">

                                <div class="row" id="tabla" style="display: flex; justify-content: center; align-items: center;">
                                    <table id="tbl_prev_excel" class="table table-striped"></table>
                                    <div id="page_prev_excel"></div>
                                </div>

                                <div class="row" style="display: flex; justify-content: center; align-items: center;"> 
                                    <div id='gen' style='padding-top: 30px; display:none'>
                                        <button id="generar" class="btn btn-secondary" style="font-family: monospace;" onclick="generarExtractoSms();">Generar Extractos Digitales</button>                
                                    </div>
                                </div>

                            </div>
                            <!--<div class="card-footer bg-transparent" style="font-family: monospace; display: flex; justify-content: flex-end; color: rgb(108, 117, 125); ">Prev</div>-->
                        </div>
                    </div>   

                    <!--Previsualizacion de archivo-->

                    <!--envio de mensajes-->
                    
                    <div id="capaCentralmsj" class="container" style="display: none; width:95%; height:95%; z-index:0; left: 0px; margin-top: 40px; margin-bottom: 40px ">
                        <div class="card" style="">
                            <div class="card-header" style="font-family: monospace; background: rgb(57, 126, 173); color: rgb(255, 255, 255); font-weight: bold">
                                Envio de Extractos digitales
                            </div>
                            <div class="card-body">
                                
                                <div class="row" id="tabla" style="display: flex; justify-content: center; align-items: center;">
                                    <table id="tbl_envio_sms" class="table table-striped"></table>
                                    <div id="page_prev_excel"></div>
                                </div>
                                <div class="row" style="display: flex; justify-content: center; align-items: center;"> 
                                    <div id='gen' style='padding-top: 30px; display: block'>
                                        <button id="generar" class="btn btn-secondary" style="font-family: monospace;" onclick=" enviarSmsExtracto()">Enviar Extractos</button>                
                                    </div>
                                </div>    
                            </div>
                            <!--<div class="card-footer bg-transparent" style="font-family: monospace; display: flex; justify-content: flex-end; color: rgb(108, 117, 125); ">Send Msj</div>-->
                        </div>
                    </div>            
                    
                    <!--fin envio de msj-->

                </div>
                <div class="card-footer bg-transparent" style="font-family: monospace; display: flex; justify-content: flex-end; color: rgb(108, 117, 125); ">Fintra S.A</div>
            </div>

        </div>


        <div id="dialogo" class="ventana" title="Mensaje">
            <p id="msj">texto </p>
        </div>


        <div id="dialogo2" class="ventana">
            <p id="msj2">texto </p> <br/>
            <center>
                <img src="./images/cargandoCM.gif" />
            </center>
        </div>
    </body>
</html>
