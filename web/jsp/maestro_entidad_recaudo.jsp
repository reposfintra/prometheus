<%-- 
    Document   : maestro_entidad_recaudo
    Created on : 4/09/2020, 08:37:38 AM
    Author     : mcamargo
--%>


<%@page contentType="text/html" pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
    <head>        
        <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
        <link type="text/css" rel="stylesheet" href="./css/jquery/jquery-ui/jquery-ui.css" />
        <link href="./css/popup.css" rel="stylesheet" type="text/css">
        <script type="text/javascript" src="./js/jquery/jquery.jqGrid/js/jquery.min.js"></script> 
        <script type="text/javascript" src="./js/jquery-ui-1.8.5.custom.min.js"></script>
        <script type="text/javascript" src="./js/jquery/jquery.jqGrid/js/grid.locale-es.js"></script>
        <script type="text/javascript" src="./js/jquery/jquery.jqGrid/js/jquery.jqGrid.min.js"></script>  
        <script src="./js/maestro_entidad_recaudo.js" type="text/javascript"></script>
        <title>Maestro Entidad Recaudo</title>
        <style type="text/css">
            
            .filtro{
                width: 550px;
                margin: 10px auto; 
                
            }

            .header_filtro{
                text-align: center;   
                border: 1px solid #234684;
                background: #2A88C8;
                color: #ffffff;
                font-weight: bold;
                border-radius: 8px 8px 0px 0px;
            }

            .body_filtro{                
                background: white; 
                border:1px solid #2A88C8;
                height: 80px;
                border-radius: 0px 0px 8px 8px;
            }

            .contenido{
                height: 83px; 
                width: auto;
                margin-top: 5px;
            }

            .label_cabecera{
                display: block;
                text-align: center;
                font-family: 'Lucida Grande','Lucida Sans',Arial,sans-serif;
                font-size: 15px;
            }
            .label_valor{
                height:17px;
                width:100px;
                color:#070708;
                font-weight:bold;
                font-size:12px;
                display: block;
                text-align: center;
                font-family: 'Lucida Grande','Lucida Sans',Arial,sans-serif;
                font-size: 12px; 
            }
            .caja{
                float:left;
                margin-left:10px;
                margin-right:10px;
                margin-top: 8px;
            }
            input[type=text],select{
                padding: 5px 5px;
                margin: 8px auto;
                border: 1px solid #ccc;
                border-radius: 4px;
                font-size: 15px;
                color: black;
            }
            .select {
                font-size: 12px;
                background: #2A88C8;
                color: white;
                font-weight: bold;
            }
            .button {
                padding: 8px 8px;
                font-size: 12px;
                cursor: pointer;
                text-align: center;
                color: #fff;
                background-color: #2a88c8;
                border: none;
                border-radius:8px;
                margin: 22px auto;
                font-weight: bold;
            }
            
            #tablainterna_tb td{
                width: 600px;
                text-align: left;
            }  
            
        </style>
    </head>
    <body>
        <div>
            <jsp:include page="/WEB-INF/toptsp.jsp?encabezado=Maestro Entidad Recaudo"/>
        </div>

        <div class="contenido" style="margin: 10px auto; ">   
            <center>
            <table id="tabla_entidad_recaudo" ></table>
             <div id="page_tabla_entidad_recaudo" ></div> 
             <br>
             <div id="div_modificarComision" style="display:none;">
                 
                 <table aling="center" style=" width: 100%" >
                    <tr>                
                        <td>
                            <label>Codigo Entidad: </label> 
                        </td>
                        <td>
                             <b><label  id="codigo_entidad"></label></b>
                        </td>                       
                    </tr>
                    <tr>                
                        <td>
                            <label> Entidad: </label> 
                        </td>
                        <td>
                             <b><label  id="entidad"></label></b>
                        </td>                       
                    </tr>
                    <tr>                
                        <td>
                            <label> Codigo Canal </label> 
                        </td>
                        <td>
                             <b><label  id="codigo_canal"></label></b>
                        </td>                       
                    </tr>
                    <tr>                
                        <td>
                            <b><label>Valor Comision: </label></b>
                        </td>
                        <td>
                            <input type="number" id="valor_comision" onchange="calcularValorTotal();">
                        </td>                       
                    </tr>
                    <tr>                
                        <td>
                            <b><label>Porcentaje Iva </label></b>
                        </td>
                        <td>
                            <input type="number" id="porcentaje_iva" onchange="calcularValorTotal();">
                        </td>                       
                    </tr>
                    <tr>                
                        <td>
                            <b><label>Total Comision: </label></b>
                        </td>
                        <td>
                            <input type="number" id="valor_total_comision" readonly="true">
                        </td>                       
                    </tr>
                 </table> 
                        
            </div>
             <br>
            </center>
            <div id="dialogLoading" style="display:none;">
                <p  style="font-size: 12px;text-align:justify;" id="msj2">Texto </p> <br/>
                <center>
                    <img src="./images/cargandoCM.gif"/>
                </center>
            </div>
            <div id="dialogMsj" title="Mensaje del sistema" style="display:none;">
                <p style="font-size: 12px;text-align:justify;" id="msj" > Texto </p>
            </div>  
        </div>
    </body>
</html>

