<%-- 
    Document   : registroHorasExtras
    Created on : 2/01/2017, 09:29:43 PM
    Author     : dvalencia
--%>

<%@page contentType="text/html" pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
        <title>REGISTRO Y SOLICITUD DE HORAS EXTRAS</title>
        <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
        <link href="./css/popup.css" rel="stylesheet" type="text/css">
        <link type="text/css" rel="stylesheet" href="./css/estilosRRHH.css" />
        <link type="text/css" rel="stylesheet" href="./css/jquery/jquery-ui/jquery-ui.css" />
        <link type="text/css" rel="stylesheet" href="./css/TransportadorasApi.css" />
        <script type="text/javascript" src="./js/jquery/jquery.jqGrid/js/jquery.min.js"></script>
        <script type="text/javascript" src="./js/jquery-ui-1.8.5.custom.min.js"></script>   
        <script type="text/javascript" src="./js/jquery/jquery.jqGrid-4.7.0/js/grid.locale-es.js"></script>
        <script type="text/javascript" src="./js/jquery/jquery.jqGrid-4.7.0/js/jquery.jqGrid.min.js"></script>
        <script type="text/javascript" src="./js/jquery/jquery.jqGrid-4.7.0/js/jquery.tabletojson.js"></script> 
        <script type="text/javascript" src="./js/jquery/timepicker/timepicker.js"></script>
        <script type="text/javascript" src="./js/utilidadInformacion.js"></script> 
        <%--<script type="text/javascript" src="./js/jquery/timepicker/timepicker.css"></script>--%>
        <script type="text/javascript" src="./js/recursosHumanos/registroHorasExtras.js"></script> 
    </head>
    <body>
       <div id="capaSuperior" style="position:absolute; width:100%; height:100px; z-index:0; left: 0px; top: 0px;">
            <jsp:include page="/WEB-INF/toptsp.jsp?encabezado=REGISTRO Y SOLICITUD DE HORAS EXTRAS"/>
        </div>
        <style>
            .onoffswitch-inner:before {
                content: "A";
                padding-left: 14px;
                background-color: #34A7C1; color: #FFFFFF;
            }
            .onoffswitch-inner:after {
                content: "I";
                padding-right: 11px;
                background-color: #C20F0F; color: #F5F0F0;
                text-align: right;
            }
        </style>
        <center>
        <div style="position: relative;top: 150px">
            <table id="tabla_HorasExtras" ></table>
            <div id="pager"></div>
        </div>
        <div id="dialogHorasExtras"  class="ventana">
            <div id="tablainterna" style="width: 500px;" >
                <table style="width: 500px;" >
                <tr>
                        <td>
                            <input type="text" id="id" style="width: 137px;color: black;" hidden>
                        </td>
                </tr>
                <table style="margin-left: 10px; margin-right: 20px; ">
                <tr >
                        <td>
                            <label>Tipo Hora Extra: </label>
                            <select id="tipo_hora" name="tipo_novedad" style="width:120px;" class="form-label15" class="requerido" ></select>
                        </td>  
                        <td>
                            <label>No. doc: </label>
                            <input type="text"  id="identificacion" class="form-label5" <%--onkeyup="format(this)" onchange="format(this)" --%>onblur="cargarNombre(this.value)" class="requerido" >
                        </td>
                        
                        <td colspan="2">
                            <label>Nombre Completo: </label>
                            <input type="text" id="nombre_completo" style="width: 340px" class="form-label15" onchange="conMayusculas(this)" readonly class="requerido">
                        </td>
                        <td>
                            <label>Fecha Sol.:</label>
                            <input type="text" id="fecha_solicitud"  name="fecha_solicitud" class="fechas" style="width:80px;" class="requerido"/>
                        </td>
                        
                </tr>
                </table>
                <table style="margin-left: 10px; margin-right: 20px; ">
                <tr >   
                        <td>
                            <label>Jefe Directo</label>
                            <select id="jefe_directo" name="jefe_directo" class="form-label15" style="width: 120px" class="requerido"></select>
                        </td>
                        <td>
                            <label>Fecha Trabajo:</label>
                            <input type="text" id="fecha_trabajo"  name="fecha_ini" class="fechas" style="width:80px;" class="requerido"/>
                        </td>
                        <td>
                            <label>Hora Inicio:</label>
                            <input type="text" id="hora_ini"  name="hora_ini" class="horas" style="width:80px;" class="requerido"/>
                        </td>    
                        <td>
                            <label>Hora Fin:</label>
                            <input type="text" id="hora_fin"  name="hora_fin" class="horas" style="width:80px;" onchange="restarHoras()" class="requerido"/>
                        </td>
                        
                        <td >
                            <label>Total Horas: </label>
                            <input type="text" id="duracion_horas" style="width: 80px" class="form-label15" class="requerido">
                        </td>
                        
                    </tr> 
                    </table>
                <table style="margin-left: 20px; margin-right: 20px; " >
                    <tr >
                        <label style="margin-left: 14px">Descripción</label>
                        <textarea id="descripcion" name="descripcion" style="width: 665px;margin-left: 14px" type="text" class="requerido">
                        
                        </textarea>
                    </tr> 
                </table>  
                </table>
            </div>
        </div>
        <div id="cargararchivo" style="display: none" >
            <table style="width: 100%">
            <tr>                          
                            <td class="letra_resaltada" align="center">&nbsp; Seleccione el archivo </td>
                            <td align="left">
                                <form id="formulario" name="formulario"> 
                                    <input type="hidden" name="identificacion1" id="identificacion1" >
                                    <input type="file" id="archivo"  name="archivo" style="width: 270px">                                  
                                </form>
                            </td>       
                            
                            <td>
                                <button id="cargarArchivo" class="ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only" 
                                        role="button" aria-disabled="false" onclick="cargarArchivo()">
                                    <span class="ui-button-text">Subir Archivo</span>
                                </button>
                            </td>   
                            <tr
                        </tr> 
            </table>
        </div>
        <div id="verarchivo" style="display: none" >
            <table style="width: 100%">
                <td style="width:50%" colspan="3">
                     <fieldset >
                         <legend>Archivos Cargados</legend>    
                         <div style="height:80px;overflow:auto;">
                            <table id="tbl_archivos_cargados" aling="center" style=" width: 100%" >             

                            </table>
                         </div>
                     </fieldset>
                </td>

            </table>
        </div>
        <div id="info"  class="ventana" >
            <p id="notific">EXITO AL GUARDAR</p>
        </div>
    </center>
    </body>
    <script>
  
        inicio1();
    

</script>
</html>

