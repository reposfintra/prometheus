<%-- 
    Document   : maestroAFP
    Created on : 14/10/2016, 03:12:56 PM
    Author     : dvalencia
--%>


<%@page contentType="text/html" pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
        <title>MAESTRO ADMINISTRADORAS FONDO DE PENSIONES</title>
        <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
        <link href="./css/popup.css" rel="stylesheet" type="text/css">
        <link type="text/css" rel="stylesheet" href="./css/jquery/jquery-ui/jquery-ui.css" />
        <link type="text/css" rel="stylesheet" href="./css/TransportadorasApi.css" />
        <script type="text/javascript" src="./js/jquery/jquery.jqGrid/js/jquery.min.js"></script>
        <script type="text/javascript" src="./js/jquery-ui-1.8.5.custom.min.js"></script>   
        <script type="text/javascript" src="./js/jquery/jquery.jqGrid-4.7.0/js/grid.locale-es.js"></script>
        <script type="text/javascript" src="./js/jquery/jquery.jqGrid-4.7.0/js/jquery.jqGrid.min.js"></script>
        <script type="text/javascript" src="./js/jquery/jquery.jqGrid-4.7.0/js/jquery.tabletojson.js"></script>   
        <script type="text/javascript" src="./js/recursosHumanos/maestroAFP.js"></script> 
    </head>
    <body>
       <div id="capaSuperior" style="position:absolute; width:100%; height:100px; z-index:0; left: 0px; top: 0px;">
            <jsp:include page="/WEB-INF/toptsp.jsp?encabezado=MAESTRO ADMINISTRADORAS FONDO DE PENSIONES"/>
        </div>
        <style>
            .onoffswitch-inner:before {
                content: "A";
                padding-left: 14px;
                background-color: #34A7C1; color: #FFFFFF;
            }
            .onoffswitch-inner:after {
                content: "I";
                padding-right: 11px;
                background-color: #C20F0F; color: #F5F0F0;
                text-align: right;
            }
        </style>
        <center>
        <div style="position: relative;top: 150px">
            <table id="tabla_AFP" ></table>
            <div id="pager"></div>
        </div>
        <div id="dialogAFP"  class="ventana">
            <div id="tablainterna" style="width: 400px;" >
                <table id="tablainterna"  >
                    <tr>
                        <td>
                            <input type="text" id="id" style="width: 137px;color: black;" hidden>
                        </td>
                    </tr>
                    tr>
                        <td>
                            <label>Nit: </label>
                            <input type="text" id="nit" style="width: 100px;color: black;" onkeyup="if (/\D/g.test(this.value)) this.value = this.value.replace(/\D/g,'')">
                            <label>     </label>
                            <label>     </label>
                            <label>     </label>
                            <label>     </label>
                            <label>     </label>
                            <label>     </label>
                            <label>     </label>
                            <label>     </label>
                            <label>     </label>
                            <label>     </label>
                            <label>     </label>
                            <label>     </label>
                            <label>     </label>
                            <label>     </label>
                            <label>Digito: </label>
                            <input type="text" id="digito_verificacion" style="width: 100px;color: black;" onkeyup="if (/\D/g.test(this.value)) this.value = this.value.replace(/\D/g,'')">
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <label>Descripcion</label>
                            <input type="text" id="descripcion" style="width: 300px;color: black;"onchange="conMayusculas(this)">
                        </td>
                    </tr>
                </table>
            </div>
        </div>
        <div id="info"  class="ventana" >
            <p id="notific">EXITO AL GUARDAR</p>
        </div>
    </center>
    </body>
    <script>
  
        inicio1();
    

</script>
</html>
