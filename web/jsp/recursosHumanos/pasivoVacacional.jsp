<%-- 
    Document   : gestionSolicitudPasivoVacacional
    Created on : 14/02/2017, 10:33:39 AM
    Author     : dvalencia
--%>

<%@page contentType="text/html" pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
        <title>PASIVO VACACIONAL</title>
        <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
        <link href="./css/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css"/>
        <link href="./css/simple-sidebar.css" rel="stylesheet" type="text/css"/>
        <link href="./css/popup.css" rel="stylesheet" type="text/css">
        <link type="text/css" rel="stylesheet" href="./css/estilosRRHH.css" />
        <link type="text/css" rel="stylesheet" href="./css/jquery/jquery-ui/jquery-ui.css" />
        <link type="text/css" rel="stylesheet" href="./css/TransportadorasApi.css" />
        <script type="text/javascript" src="./js/jquery/jquery.jqGrid/js/jquery.min.js"></script>
        <script type="text/javascript" src="./js/jquery-ui-1.8.5.custom.min.js"></script>   
        <script type="text/javascript" src="./js/jquery/jquery.jqGrid-4.7.0/js/grid.locale-es.js"></script>
        <script type="text/javascript" src="./js/jquery/jquery.jqGrid-4.7.0/js/jquery.jqGrid.min.js"></script>
        <script type="text/javascript" src="./js/jquery/jquery.jqGrid-4.7.0/js/jquery.tabletojson.js"></script> 
        <script type="text/javascript" src="./js/jquery/timepicker/timepicker.js"></script>
        <script type="text/javascript" src="./js/utilidadInformacion.js"></script> 
        <%--<script type="text/javascript" src="./js/jquery/timepicker/timepicker.css"></script>--%>
        <script type="text/javascript" src="./js/recursosHumanos/pasivoVacacional.js"></script> 
    </head>
    <body>
       <div id="capaSuperior" style="position:absolute; width:100%; height:100px; z-index:0; left: 0px; top: 0px;">
            <jsp:include page="/WEB-INF/toptsp.jsp?encabezado=PASIVO VACACIONAL"/>
        </div>
<style>
            .onoffswitch-inner:before {
                content: "A";
                padding-left: 14px;
                background-color: #34A7C1; color: #FFFFFF;
                
            }
            .onoffswitch-inner:after {
                content: "I";
                padding-right: 14px;
                background-color: #C20F0F; color: #F5F0F0;
                text-align: right;
                
            }
            fieldset    {
                height: 207px;
                margin: 0 1em 0 0;
                padding: 0.5em 1em;
                overflow-y: auto;
                border-radius: 4px;  
            }
            input.form-control, select.form-control{
                height: 30px !important;
                padding-top:0;
                padding-bottom: 0;
            }
            .row{
                margin-top: 10px;
            }
            #dialogPasivoVacacional{
                height:auto !important;
                width:auto !important;
                overflow-x: hidden !important;
                padding-left: 3em;
                padding-right: 3em;
            }
            
        </style>
<div id="capaCentral" align="left" style="position:absolute; width:100%; height:100%; z-index:0; top: -40px;" >
    
        <center>
          <div id="tablita" style="top: 150px;width: 500px;height: 100px;" >
            <div class="ui-dialog-titlebar ui-widget-header ui-helper-clearfix" style="height: 30px;">
<!--                <span class="ui-dialog-title text-center" id="ui-dialog-title-listaGlobal">-->
            
                    <label  style='display:flex !important; justify-content:center; align-items:center;'>
                        <h5><b>FILTRO DE BUSQUEDA</b></h5>
                    </label>
<!--                </span>-->
            </div>  
                   <table id="tablainterna" style="height: 1px; width: 400px"  >
                        <tr>
                            <td>
                                <label><h5>Identificacion</h5></label>
                            </td>
                            <td>
                                <input type="text" id="identificacion2" name="identificacion2" style="height: 23px;width: 100px;color: #070708;" class="solo-numero" >
                            </td>

                            <td>
                                <hr style="width: 2px;height: 39px;top: -25px;margin-left: 13px;color: rgba(128, 128, 128, 0.39);" /> 
                            </td>
                            <td>
                                <div style="padding-top: 10px">
                                    <button id="buscar" class="ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only" style="margin-left: 15px;top: -7px;" > 
                                        <span class="ui-button-text">Buscar</span>
                                    </button>  
                                </div>
                            </td>
                            <td>
                                <div style="padding-top: 10px">
                                    <button id="limpiar" class="ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only" style="margin-left: -2px;top: -7px;width: 102px;" > 
                                        <span class="ui-button-text">Limpiar</span>
                                    </button>  
                                </div>
                            </td>
                        </tr>
                    </table>

            </div>
          </div>    
        <div style="position: relative;top: 230px">
            <center><table id="tabla_PasivoVacacional" ></table><div id="pager"></div></center>
            
        </div>
        <div id="divSalidaEx" title="Exportacion" style=" display: block" >
            <p  id="msjEx" style=" display:  none"> Espere un momento por favor...</p>
            <center>
                <img id="imgloadEx" style="position: relative;  top: 7px; display: none " src="./images/cargandoCM.gif"/>
            </center>
            <div id="respEx" style=" display: none"></div>
        </div> 
  


                                   
 </div>
