<%-- 
    Document   : trazabilidadEmpleados
    Created on : 27/01/2017, 04:01:37 PM
    Author     : dvalencia
--%>

<%@page contentType="text/html" pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
        <title>TRAZABILIDAD EMPLEADOS</title>
      
        <link type="text/css" rel="stylesheet" href="./css/estilosRRHH.css" />
        <link type="text/css" rel="stylesheet" href="./css/jquery/jquery-ui/jquery-ui.css" />
        <link href="./css/popup.css" rel="stylesheet" type="text/css">      
        <link type="text/css" rel="stylesheet" href="./css/TransportadorasApi.css" />
        <script type="text/javascript" src="./js/jquery/jquery.jqGrid/js/jquery.min.js"></script>
        <script type="text/javascript" src="./js/jquery-ui-1.8.5.custom.min.js"></script>   
        <script type="text/javascript" src="./js/jquery/jquery.jqGrid/js/grid.locale-es.js"></script>
        <script type="text/javascript" src="./js/jquery/jquery.jqGrid/js/jquery.jqGrid.min.js"></script>   

        <script type="text/javascript" src="./js/recursosHumanos/trazabilidadEmpleados.js"></script>

        
    </head>
    <body>
       <div id="capaSuperior" style="position:absolute; width:100%; height:100px; z-index:0; left: 0px; top: 0px;">
            <jsp:include page="/WEB-INF/toptsp.jsp?encabezado=TRAZABILIDAD EMPLEADOS"/>
        </div>
        <style>
            
            h2, td, label {
                font-family: Calibri, Candara, Segoe, "Segoe UI", Optima, Arial, sans-serif;
                font-size: 13px;
            }
            
        </style>
        <center>
        <div id="tablita" style="top: 100px;width: 800px;height: 100px;" >
            <div class="ui-dialog-titlebar ui-widget-header ui-helper-clearfix" style="height: 30px;">
<!--                <span class="ui-dialog-title text-center" id="ui-dialog-title-listaGlobal">-->
            
                    <label  style='display:flex !important; justify-content:center; align-items:center;'>
                        <h5><b>FILTRO DE BUSQUEDA</b></h5>
                    </label>
<!--                </span>-->
            </div>  
                   <table id="tablainterna" style="height: 1px; width: 700px"  >
                        <tr>
                            <td style="padding-top: 20px">
                                <label>Identificacion</label>
                            </td>
                            <td style="padding-top: 20px">
                                <input type="text" id="identificacion2" name="identificacion2" style="height: 18px;width: 100px;color: #070708;top: 5px;" class="solo-numero" >
                            </td>
                             
                            <td>
                                <div style="padding-top: 10px">
                                    <button id="buscar" class="ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only" style="margin-left: 15px;top: 5px;width: 102px;" > 
                                        <span class="ui-button-text">Buscar</span>
                                    </button>  
                                </div>
                            </td>
                            <td>
                                <div style="padding-top: 10px">
                                    <button id="limpiar" class="ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only" style="margin-left: -2px;top: 5px;width: 102px;" > 
                                        <span class="ui-button-text">Limpiar</span>
                                    </button>  
                                </div>
                            </td>
                        </tr>
                    </table>

            </div>    
        <div style="position: relative;top: 120px">
            <table id="tabla_Empleados" ></table>
            <div id="pager"></div>
        </div>
        <div id="dialogEmpleados"  class="ventana" style="width: 680px;">
            <div id="tablainterna" style="width: 680px;" >
                
                <table id="tablainterna"  style="margin-left: 10px; margin-right: 20px; ">
                  <tr>
                        <td>
                            <input type="text" id="id" style="width: 137px;color: black;" hidden>
                        </td>
                        
                </tr>
                
                <H2 style="background-color:#2A88C8; width:750px; margin-left: 10px; color:#FFF">INFORMACION BASICA</H2>
                <tr >
                        <td>
                            <label>Tipo doc: </label>
                            <select id="tipo_doc" name="tipo_doc" class="form-label1"></select>
                        </td>    
                        <td>
                            <label>No. doc: </label>
                            <input type="text" id="identificacion" class="form-label5" onkeyup="format(this)" onchange="format(this)">
                        </td>
                        
                        <td colspan="2">
                            <label>Nombre Completo: </label>
                            <input type="text" id="nombre_completo" class="form-label4" onchange="conMayusculas(this)">
                        </td>
                        <td>
                            <label>Sexo: </label>
                            <select type="text" id="sexo" class="form-label1" onChange="activarCampo1();">
                            <option value="">...</option>
                            <option value="F">FEMENINO</option>
                            <option value="M">MASCULINO</option>
                            </select>
                            </td>
                        <td>
                            <label>Estado civil: </label>
                            <select type="text" id="estado_civil" class="form-label1"></select>
                        </td>
                    </tr>
                </table>
                <table style="margin-left: 10px; margin-right: 20px; ">
                <tr>
                        <td>
                            <label>Fecha Exp.:</label>
                            <input type="text" id="fecha_expedicion"  name="fecha_expedicion" class="fechas" style="width:100px;"/>
                        </td>
                            
                        <td>
                            <label>Dpto Expedicion: </label>
                            <select type="text" id="dpto_expedicion" name="dpto_expedicion" class="form-label2" onchange="ciudadExpedicion(this.value)"></select>
                        </td> 
                        <td>
                            <label>Ciudad Expedicion: </label><br>
                            <select type="text" id="ciudad_expedicion" name="ciudad_expedicion" class="form-label2"></select>
                        </td> 
                        <td>
                            <label>Nivel Estudio: </label>
                            <select id="nivel_estudio" name="nivel_estudio" class="form-label2"></select>
                        </td>
                        <td colspan="2">
                            <label>Profesion: </label>
                            <select id="profesion" name="profesion" class="form-label2"></select>
                        </td>
                    </tr>
                </table>
                <table style="margin-left: 10px; margin-right: 20px; ">
                    <tr>
                        <td>
                            <label>Fecha Nac.:</label>
                            <input type="text" id="fecha_nacimiento"  name="fecha_nacimiento" class="fechas" style="width:100px;"/>
                        </td>
                        <td>
                            <label>Dpto Nacimiento: </label>
                            <select type="text" id="dpto_nacimiento" name="dpto_nacimiento" class="form-label2" onchange="ciudadNacimiento(this.value)" ></select>
                        </td> 
                        <td>
                            <label>Ciudad Nacimiento: </label><br>
                            <select type="text" id="ciudad_nacimiento" name="ciudad_nacimiento" class="form-label2"></select>
                        </td> 
                        <td>
                            <label>Dpto Residencia: </label>
                            <select type="text" id="dpto" name="dpto" class="form-label2" onchange="ciudadResidencia(this.value)" ></select>
                        </td>
                        <td>
                            <label>Ciudad Residencia </label>
                            <select type="text" id="ciudad" name="ciudad" class="form-label2"></select>
                        </td>
                    </tr>
                </table>
                <table style="margin-left: 10px; margin-right: 20px; ">
                    <tr>
                        <td colspan="2">
                         <label >Direccion Residencia:</label>
                         <input type="text" id="direccion" class="form-label4" onchange="conMayusculas(this)"/>
                        </td> 
                        <td>
                        <label >Telefono:</label>
                         <input type="text" id="telefono" class="form-label1" class="solo-numero"/>
                        </td> 
                        <td>
                        <label >Celular:</label>
                        <input type="text" id="celular" class="form-label1" class="solo-numero"/>
                        </td> 
                        <td colspan="2">
                        <label >Email:</label>
                        <input type="email" id="email" class="form-label6" onblur="validarEmail(this)" onchange="conMayusculas(this)"/>
                        </td> 
                    </tr>
                </table>
                <table style="margin-left: 10px; margin-right: 20px;" >
                <tr>
                <H2 style="background-color:#2A88C8; width:750px; margin-left: 10px; color:#FFF">INFORMACION LABORAL</H2>
                </tr>
                    <tr >
                        <td>
                            <label>Macroproceso: </label>
                            <select id="macroproceso" name="macroproceso" class="form-label7" onchange="cargarProcesos(this.value)"></select>
                        </td>
                        <td>
                            <label>Proceso: </label>
                            <select id="proceso" name="proceso" class="form-label7" onchange="cargarLineasNegocio(this.value)"></select>
                        </td>
                        <td>
                            <label>L�nea Negocio: </label>
                            <select id="linea_negocio" name="linea_negocio" class="form-label7" onchange="cargarProductos(this.value)"></select>
                        </td>
                        
                        <td >
                            <label>Producto: </label>
                            <select id="producto" name="producto" class="form-label7"></select>
                        </td>
                    </tr>
                </table>
                <table style="margin-left: 10px; margin-right: 20px;" >
                <tr >
                        <td>
                            <label>Tipo Contrato: </label>
                            <select id="tipo_contrato" name="tipo_contrato" class="form-label5" onChange="activarCampo();"></select>
                        </td>
                        <td>
                            <label>Duracion: </label>
                            <input type="text" id="duracion" class="form-label3" value="0"  onclick="if(this.value=='0') this.value=''" onblur="if(this.value=='') this.value='0'" onkeyup="format(this)" onchange="format(this)" disabled/>
                        </td>
                        <td colspan="2">
                            <label>Nivel Jerarquico: </label>
                            <select id="nivel_jerarquico" name="nivel_jerarquico" class="form-label14"></select>
                        </td>
                        
                        <td >
                            <label>Cargo: </label>
                            <select id="cargo" name="cargo" class="form-label9"></select>
                        </td>
                        <td>
                            <label>Salario: </label>
                            <input type="text" id="salario" class="form-label3" class="solo-numeric" onkeypress="return onKeyDecimal(event, this)">
                        </td>
                    </tr>
                </table>
                <table style="margin-left: 10px; margin-right: 20px;">    
                    <tr >
                        <td>
                            <label>Libreta Militar: </label><br>
                            <input type="text" id="libreta_militar" class="form-label1" value="N/A"  onclick="if(this.value=='N/A') this.value=''" onblur="if(this.value=='') this.value='N/A'" disabled/>
                        </td>    
                        <td >
                            <label>Riesgo: </label><br>
                            <select id="riesgo" name="riesgo" class="form-label5"></select>
                        </td>
                        
                        <td colspan="2">
                            <label>EPS: </label><br>
                            <select id="eps" name="cargo" class="form-label8"></select>
                        </td>
                        <td colspan="2">
                            <label>ARL: </label>
                            <select id="arl" name="cargo" class="form-label8"></select>
                        </td>
                    </tr>
                </table>
                <table style="margin-left: 10px; margin-right: 20px;">     
                    <tr >
                        <td colspan="2">
                            <label>AFP: </label><br>
                            <select id="afp" name="afp" class="form-label6"></select>
                        </td>
                        
                        <td colspan="2">
                            <label>CCF: </label><br>
                            <select id="ccf" name="ccf" class="form-label6"></select>
                        </td>
                        <td colspan="2">
                            <label>Cesantias: </label><br>
                            <select id="cesantias" name="cesantias" class="form-label6"></select>
                        </td>
                        <td>
                            <label>F. Ingreso:</label>
                            <input type="text" id="fecha_ingreso" name="fecha_ingreso" class="fechas" class="form-label3"  onChange="fechaRetiro();"/>
                        </td>
                    </tr>
                </table>
                <table style="margin-left: 10px; margin-right: 20px;"> 
                    <tr >
                        <td >
                            <label>Banco: </label><br>
                            <select id="banco" name="banco" class="form-label10"></select>
                        </td>
                        
                        <td >
                            <label>Tipo Cuenta </label><br>
                            <select id="tipo_cuenta" name="tipo_cuenta" class="form-label11">
                            <option value="">...</option>
                            <option value="CA">AHORROS</option>
                            <option value="CC">CORRIENTE</option>    
                            </select>
                        </td>
                        <td colspan="2">
                            <label>No. cuenta:</label>
                            <input type="text" id="no_cuenta" name="no_cuenta"class="form-label9" class="solo-numero"/>
                        </td>
                        <td>
                            <label>Fecha Retiro:</label>
                            <input type="text" id="fecha_retiro" name="fecha_retiro" class="fechas"  class="form-label3"/>
                        </td>
                    </tr>
                  </table>
                <table style="margin-left: 10px; margin-right: 20px;"> 
                <tr>
                <H2 style="background-color:#2A88C8; width:750px; margin-left: 10px; color:#FFF">INFORMACION FAMILIAR</H2>
                </tr>
                    <tr >
                        <td >
                            <label>No. Personas a Cargo: </label><br>
                            <input type="text" id="personas_a_cargo" name="personas_a_cargo"   class="form-label13" onkeyup="format(this)" onchange="format(this)"/>
                        </td>
                        
                        <td >
                            <label>No. Hijos: </label><br>
                            <input type="text" id="num_de_hijos" name="num_de_hijos"   class="form-label13" onkeyup="format(this)" onchange="format(this)"/>
                        </td>
                        <td colspan="2">
                            <label>Total Grupo Familiar:</label>
                            <input type="text" id="total_grupo_familiar" name="total_grupo_familiar" class="form-label13" onkeyup="format(this)" onchange="format(this)" />
                        </td>
                        <td>
                            <label>Observaciones:</label>
                            <input type="text" id="observaciones" name="observaciones" class="form-label12" onchange="conMayusculas(this)"/>
                        </td>
                    </tr>
                </table>
            </div>
        </div>
        <div id="cargararchivo" style="display: none" >
            <table style="width: 100%">
            <tr>                          
                            <td class="letra_resaltada" align="center">&nbsp; Seleccione el archivo </td>
                            <td align="left">
                                <form id="formulario" name="formulario"> 
                                      <input type="hidden" name="identificacion1" id="identificacion1" >
                                    <input type="file" id="archivo"  name="archivo" style="width: 270px">                                  
                                </form>
                            </td>       
                            
                            <td>
                                <button id="cargarArchivo" class="ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only" 
                                        role="button" aria-disabled="false" onclick="cargarHV()">
                                    <span class="ui-button-text">Subir Archivo</span>
                                </button>
                            </td>   
                            <tr
                        </tr> 
            </table>
        </div>
        <div id="verarchivo" style="display: none" >
            <table style="width: 100%">
                <td style="width:50%" colspan="3">
                     <fieldset >
                         <legend>Archivos Cargados</legend>    
                         <div style="height:80px;overflow:auto;">
                            <table id="tbl_archivos_cargados" aling="center" style=" width: 100%" >             

                            </table>
                         </div>
                     </fieldset>
                </td>

            </table>
        </div>
        <div id="divSalidaEx" title="Exportacion" style=" display: block" >
            <p  id="msjEx" style=" display:  none"> Espere un momento por favor...</p>
            <center>
                <img id="imgloadEx" style="position: relative;  top: 7px; display: none " src="./images/cargandoCM.gif"/>
            </center>
            <div id="respEx" style=" display: none"></div>
        </div>     
        <div id="info"  class="ventana" >
            <p id="notific">EXITO AL GUARDAR</p>
        </div>
    </center>
    </body>

</html>
