<%-- 
    Document   : RegistroIncapacidad
    Created on : 25/01/2017, 10:37:14 AM
    Author     : dvalencia
--%>


<script type="text/javascript" src="./js/recursosHumanos/RegistroVacaciones.js"></script> 
<style>
            .onoffswitch-inner:before {
                content: "A";
                padding-left: 14px;
                background-color: #34A7C1; color: #FFFFFF;
                
            }
            .onoffswitch-inner:after {
                content: "I";
                padding-right: 14px;
                background-color: #C20F0F; color: #F5F0F0;
                text-align: right;
                
            }
            fieldset    {
                height: 207px;
                margin: 0 1em 0 0;
                padding: 0.5em 1em;
                overflow-y: auto;
                border-radius: 4px;  
            }
            input.form-control, select.form-control{
                height: 30px !important;
                padding-top:0;
                padding-bottom: 0;
            }
            .row{
                margin-top: 10px;
            }
            #dialogNovedades{
                height:auto !important;
                width:auto !important;
                overflow-x: hidden !important;
                padding-left: 3em;
                padding-right: 3em;
            }
            
        </style>
<div id="capaCentral" align="left" style="position:absolute; width:100%; height:100%; z-index:0; top: -40px;" >
    
        <center>
          <div id="tablita" style="top: 10px;width: 1300px;height: 100px;" >
            <div class="ui-dialog-titlebar ui-widget-header ui-helper-clearfix" style="height: 30px;">
<!--                <span class="ui-dialog-title text-center" id="ui-dialog-title-listaGlobal">-->
            
                    <label  style='display:flex !important; justify-content:center; align-items:center;'>
                        <h5><b>FILTRO DE BUSQUEDA VACACIONES</b></h5>
                    </label>
<!--                </span>-->
            </div>  
                   <table id="tablainterna" style="height: 1px; width: 1200px"  >
                        <tr>
                            <td>
                                <label><h6>Fecha Solicitud</h6></label>
                            </td>
                            <td>
                                <input type="text" id="fechaini" name="fechaini" style="height: 23px;width: 80px;color: #070708;" readonly>

                                <label >-</label>

                                <input type="text" id="fechafin" name="fechafin" style="height: 23px;width: 80px;color: #070708;margin-left: 4px;" readonly>
                            </td>
                            <td>
                                <label><h6>Identificacion</h6></label>
                            </td>
                            <td>
                                <input type="text" id="identificacion2" name="identificacion2" style="height: 23px;width: 100px;color: #070708;" class="solo-numero" >
                            </td>
<!--                            <td>
                                <label><h6>Estado Solicitud</h6></label>
                            </td>
                            <td>
                                <select id="status" name="status" style="height: 23px;width: 118px;color: #070708;">
                                    <option value="">...</option>
                                    <option value="S">APROBADO</option>
                                    <option value="N">RECHAZADO</option>
                                    <option value="P">PENDIENTE</option>
                                </select>
                            </td>-->
                            <td>
                                <hr style="width: 2px;height: 39px;top: -25px;margin-left: 13px;color: rgba(128, 128, 128, 0.39);" /> 
                            </td>
                            <td>
                                <div style="padding-top: 10px">
                                    <button id="nuevo" class="ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only" style="margin-left: 15px;top: -7px;" > 
                                        <span class="ui-button-text">Nuevo</span>
                                    </button>  
                                </div>
                            </td>
                            <td>
                                <div style="padding-top: 10px">
                                    <button id="buscar" class="ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only" style="margin-left: 15px;top: -7px;" > 
                                        <span class="ui-button-text">Buscar</span>
                                    </button>  
                                </div>
                            </td>
                            <td>
                                <div style="padding-top: 10px">
                                    <button id="limpiar" class="ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only" style="margin-left: -2px;top: -7px;width: 102px;" > 
                                        <span class="ui-button-text">Limpiar</span>
                                    </button>  
                                </div>
                            </td>
                        </tr>
                    </table>

            </div>
          </div>    
        <div style="position: relative;top: 120px">
            <center><table id="tabla_Novedades" ></table><div id="pager"></div></center>
            
        </div>
        <div id="dialogEnfermedades"  class="ventana">
            <table id="tabla_Enfermedades" ></table>
            <div id="pager"></div>
        </div>    
        <div id="dialogNovedades"  class="ventana" >
            <div class='row'>
                <div class='col-md-4' >
                    
                </div>
                <div class='col-md-8 text-right' >

                <div style="float: right">
                    <table style="margin-right: 20px;">
                        <tr>
                            <td>
                                <label style="float: right;">Numero Solicitud:</label>
                            </td>
                            <td>
                                <input id="numsolicitud" readonly>
                            </td>
                            <td>
                                <label>Fecha Solicitud:</label>
                            </td>
                            <td>
                                <input id="fechaSolicitud"  readonly>
                            </td>
                        </tr>
                    </table>
                </div>
                </div>
            </div>
            <div class='row form-novedades'>
                <tr>
                        <td>
                            <input type="text" id="id" style="width: 137px;color: black;" hidden>
                        </td>
                </tr>
                <div class='col-md-3' >
                    <label style='font-weight: 10px !important;'>Tipo Novedad:</label>
                    <select id="tipo_novedad" name="tipo_novedad"  class="form-control" class="requerido"></select>
                </div>
<!--                <div class='col-md-4' >
                    <label style='font-weight: 10px !important;'>Origen: </label>

                            <select id="origen" name="origen" class="form-control" onchange="cargarEntidadOtorga();
                                    activarCampos();" class="requerido">
                                    <option value="">...</option>
                                    <option value="C">COMUN</option>
                                    <option value="P">PROFESIONAL</option>
                            </select> 
                </div>
                <div class='col-md-4' >
                    <label style='font-weight: 10px !important;'>Otorgada por:</label>
                            <select id="razon" name="razon"  class="form-control" class="requerido" onchange="activarCampos();
                                    visualizarRazon()"></select>
                </div>-->
                <div class='col-md-3' >
                    <label>Identificacion</label>
                    <input type="text"  id="identificacion" class="form-control" <%--onkeyup="format(this)" onchange="format(this)" --%>onblur="cargarNombre(this.value);cargarSaldo(this.value);cargarPeriodo(this.value)" class="requerido" >
                </div>
                <div class='col-md-6' >
                    <label>Nombre Completo: </label>
                        <input type="text" id="nombre_completo"  class="form-control" onchange="conMayusculas(this)" readonly class="requerido">
                </div>
                
            </div>
                
            <div class='row form-novedades'>
                <div class='col-md-3'>
                        <label  class="visfecha">Saldo:</label>
                        <input type="text" id="saldo"  name="saldo" class="visfecha form-control" readonly />
                </div>
                <div class='col-md-9'>
                        <label  class="visfecha">Periodo:</label>
                        <input type="text" id="periodo"  name="periodo" class="visfecha form-control" readonly />
                </div>
                    
            </div>
            <div class='row form-novedades'>
                    <div class='col-md-2'>
                        <label  class="visfecha">Fecha Ini:</label>
                        <input type="text" id="fecha_ini"  name="fecha_ini" class="visfecha form-control" class="requerido" />
                    </div>
                    <div class='col-md-2'>
                        <label class="visfecha">Total Dias: </label>
                        <input type="text" id="duracion_dias"  class="visfecha form-control" onchange ="calcularDiasF();restringirDias()" class="requerido">
                    </div>
                    <div class='col-md-2'>
                         <label  class="visfecha">Fecha Fin:</label>
                         <input type="text" id="fecha_fin"  name="fecha_fin"  class="visfecha form-control" class="requerido">
                    </div>
                    <div class='col-md-2'>
                        <label class="visfecha">Dias Comp.: </label>
                        <input type="text" id="dias_compensados"  class="visfecha form-control" onchange ="validarTotalDias();restringirDiasCompensados()" value="0"  onclick="if(this.value=='0') this.value=''" onblur="if(this.value=='') this.value='0'">
                    </div>
                    <div class='col-md-4'>
                        <label>Jefe Directo</label>
                        <select id="jefe_directo" name="jefe_directo" class="form-control" class="requerido"></select>
                    </div>
            </div>
<!--                <div class='row form-novedades'>
                    <div class="col-md-2" style='postion:relative;'>
                        <label>C�digo:</label>
                        <input id="cod_enfermedad" name="cod_enfermedad"  type="text" class="form-control" >
                        <img alt="buscar" src="/fintra/images/botones/iconos/lupa.gif" style="cursor:pointer; right:-10px; top:27px; position: absolute;" onclick="cargarEnfermedades();" height="15px" width="15px">
                        
                    </div>
                    
                    <div class="col-md-10 " >
                        <label >Enfermedad:</label>
                                <input type="text" id="enfermedad"  class="form-control" readonly >
                    </div>
                </div>-->
                <div class='row'>
                    <div class="col-md-12">
                        <label >Descripci�n</label>
                        <textarea id="descripcion" name="descripcion"  type="text" class="form-control" onchange="conMayusculas(this)" class="requerido"> </textarea>
                    </div>
                </div>
<!--            <div id="tablainterna" style="width: 840px;" >
                <div style="float: right">
                    <table style="margin-right: 20px;">
                        <tr>
                            <td>
                                <label style="float: right;">Numero Solicitud:</label>
                            </td>
                            <td>
                                <input id="numsolicitud" readonly>
                            </td>
                            <td>
                                <label>Fecha Solicitud:</label>
                            </td>
                            <td>
                                <input id="fechaSolicitud"  readonly>
                            </td>
                        </tr>
                    </table>
                </div>
                <table style="margin-left: 20px; margin-right: 20px;width: 100px; ">
                    <tr>
                        <td>
                            <input type="text" id="id" style="width: 137px;color: black;" hidden>
                        </td>
                    </tr>
                    
                    <tr>
                        <td>
                            <label>Origen: </label>

                            <select id="origen" name="origen" style="width:120px;" class="form-label15" onchange="cargarEntidadOtorga();
                                    activarCampos();" class="requerido">
                                    <option value="">...</option>
                                    <option value="C">COMUN</option>
                                    <option value="P">PROFESIONAL</option>
                            </select>        
                        </td>  
                        <td>
                            <label>Otorgada por:</label>
                            <select id="razon" name="razon" style="width: 120px" class="form-label15" class="requerido" onchange="activarCampos();
                                    visualizarRazon()"></select>
                        </td>
                        <td>
                            <label>Identificacion</label>
                            <input type="text"  id="identificacion" class="form-label15" <%--onkeyup="format(this)" onchange="format(this)" --%>onblur="cargarNombre(this.value)" class="requerido" >
                        </td>

                        <td colspan="2">
                            <label>Nombre Completo: </label>
                            <input type="text" id="nombre_completo" style="width: 340px" class="form-label15" onchange="conMayusculas(this)" readonly class="requerido">
                        </td>
                        <td>
                            <label>Jefe Directo</label>
                            <select id="jefe_directo" name="jefe_directo" class="form-label15" style="width: 130px" ></select>
                        </td>
                    </tr>
                </table>
                <table style="margin-left: 20px; margin-right: 20px;width: 100px ">  615 
                        <tr >   
                            <td>
                                <label  class="visfecha">Fecha Ini:</label>
                                <input type="text" id="fecha_ini"  name="fecha_ini" class="visfecha" class="visfecha" style="width:80px;" class="requerido"/>
                            </td>
                            <td >
                                <label class="visfecha">Total Dias: </label>
                                <input type="text" id="duracion_dias" style="width:80px" class="visfecha" class="requerido" onkeyup ="calcularDiasF()">
                            </td>
                            <td>
                                <label  class="visfecha">Fecha Fin:</label>
                                <input type="text" id="fecha_fin"  name="fecha_fin"  class="visfecha" style="width:80px;" onblur="calcularDias()" class="requerido"  readonly/>
                            </td>
                            <td>
                                <label class="visHoras"> Hora Inicio:</label>
                                <input type="text" id="hora_ini"  name="hora_ini" class="horas" style="width:80px;" />
                            </td>    
                            <td>
                                <label  class="visHoras">Hora Fin:</label>
                                <input type="text" id="hora_fin"  name="hora_fin" class="horas" style="width:80px;" onchange="restarHoras()"/>
                            </td>
                        </tr> 
                    </table>
                    <table style="margin-left: 20px; margin-right: 20px;width: 100px ">
                        <tr  class="vis">
                            <td>
                                <label style="margin-left: -3px;">C�digo:</label>
                                <input id="cod_enfermedad" name="cod_enfermedad" style="width: 80px" type="text" class="form-label15">
                            </td>  
                            <td>
                                <img alt="buscar" src="/fintra/images/botones/iconos/lupa.gif" style="cursor:pointer; margin-top: 10px;" onclick="cargarEnfermedades();" height="15px" width="15px">
                            </td>
                            <td>
                                <label style="margin-left: 15px;">Enfermedad:</label>
                                <input type="text" id="enfermedad" style="width: 430px;margin-left: 15px;" class="form-label15" readonly >
                            </td>
                        </tr>  
                    </table> 
                    <table style="margin-left: 20px; margin-right: 20px; width: 100px" >
                        <tr>
                            <td class="descripcion">
                                <label >Descripci�n</label>
                                <textarea id="descripcion" name="descripcion" style="width: 685px;height: 80px;" type="text" class="requerido" class="form-label15"> </textarea>
                            </td> 
                        </tr> 
                    </table>  -->

            </div>
        </div>
        <div id="cargararchivo" style="display: none" >
            <table style="width: 100%">
                <tr>                          
                    <td class="letra_resaltada" align="center">&nbsp; Seleccione el archivo </td>
                    <td align="left">
                        <form id="formulario" name="formulario"> 
                            <input type="hidden" name="identificacion1" id="identificacion1" >
                            <input type="file" id="archivo"  name="archivo" style="width: 270px">                                  
                        </form>
                    </td>       
                    <td>
                        <button id="cargarArchivo" class="ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only" 
                                role="button" aria-disabled="false" onclick="cargarArchivo()">
                            <span class="ui-button-text">Subir Archivo</span>
                        </button>
                    </td>   
                <tr
            </tr> 
        </table>
    </div>
    <div id="verarchivo" style="display: none" >
        <table style="width: 100%">
            <td style="width:50%" colspan="3">
                <fieldset >
                    <legend>Archivos Cargados</legend>    
                    <div style="height:80px;overflow:auto;">
                        <table id="tbl_archivos_cargados" aling="center" style=" width: 100%" >             

                        </table>
                    </div>
                </fieldset>
            </td>

            </table>
        </div>
        <div id="info"  class="ventana" >
            <p id="notific">EXITO AL GUARDAR</p>
        </div>

            </center>
<!--            <div id="dialogMsj" title="Mensaje" style="display:none; visibility: hidden;">
                <p style="font-size: 12px;text-align:justify;" id="msj" > Texto </p>
            </div>                                   -->
 </div>
