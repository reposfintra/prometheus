<%-- 
    Document   : gestionSolicitudNovedades
    Created on : 20/12/2016, 10:33:39 AM
    Author     : dvalencia
--%>

<%@page contentType="text/html" pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
        <title>GESTION DE SOLICITUD DE NOVEDADES</title>
        <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
        <link href="./css/popup.css" rel="stylesheet" type="text/css">
        <link type="text/css" rel="stylesheet" href="./css/estilosRRHH.css" />
        <link type="text/css" rel="stylesheet" href="./css/jquery/jquery-ui/jquery-ui.css" />
        <link type="text/css" rel="stylesheet" href="./css/TransportadorasApi.css" />
        <script type="text/javascript" src="./js/jquery/jquery.jqGrid/js/jquery.min.js"></script>
        <script type="text/javascript" src="./js/jquery-ui-1.8.5.custom.min.js"></script>   
        <script type="text/javascript" src="./js/jquery/jquery.jqGrid-4.7.0/js/grid.locale-es.js"></script>
        <script type="text/javascript" src="./js/jquery/jquery.jqGrid-4.7.0/js/jquery.jqGrid.min.js"></script>
        <script type="text/javascript" src="./js/jquery/jquery.jqGrid-4.7.0/js/jquery.tabletojson.js"></script> 
        <script type="text/javascript" src="./js/jquery/timepicker/timepicker.js"></script>
        <script type="text/javascript" src="./js/utilidadInformacion.js"></script> 
        <%--<script type="text/javascript" src="./js/jquery/timepicker/timepicker.css"></script>--%>
        <script type="text/javascript" src="./js/recursosHumanos/gestionSolicitudNovedades.js"></script> 
    </head>
    <body>
       <div id="capaSuperior" style="position:absolute; width:100%; height:100px; z-index:0; left: 0px; top: 0px;">
            <jsp:include page="/WEB-INF/toptsp.jsp?encabezado=GESTION DE SOLICITUD DE NOVEDADES"/>
        </div>
        <style>
            .onoffswitch-inner:before {
                content: "A";
                padding-left: 14px;
                background-color: #34A7C1; color: #FFFFFF;
            }
            .onoffswitch-inner:after {
                content: "I";
                padding-right: 11px;
                background-color: #C20F0F; color: #F5F0F0;
                text-align: right;
            }
        </style>
        <center>
        <div style="position: relative;top: 150px">
            <table id="tabla_Novedades" ></table>
            <div id="pager"></div>
        </div>
        <div id="dialogNovedades"  class="ventana">
            <div id="tablainterna" style="width: 500px;" >
                <table style="width: 500px;" >
                <tr>
                        <td>
                            <input type="text" id="id" style="width: 137px;color: black;" hidden>
                        </td>
                </tr>
                <table style="margin-left: 10px; margin-right: 20px; ">
                <tr >
                        <td>
                            <label>Tipo Novedad: </label>
                            <select id="tipo_novedad" name="tipo_novedad" style="width:120px;" class="form-label15" onchange="cargarRazon(this.value);activarCampos()" class="requerido" readonly></select>
                        </td>  
                        <td>
                            <label>Razon:</label>
                            <select id="razon" name="razon" style="width: 120px" class="form-label15" class="requerido" readonly></select>
                        </td>
                        <td>
                            <label>No. doc: </label>
                            <input type="text"  id="identificacion" class="form-label15" <%--onkeyup="format(this)" onchange="format(this)" --%>onblur="cargarNombre(this.value)" class="requerido" readonly >
                        </td>
                        
                        <td colspan="2">
                            <label>Nombre Completo: </label>
                            <input type="text" id="nombre_completo" style="width: 340px" class="form-label15" onchange="conMayusculas(this)" readonly class="requerido">
                        </td>
                        
                </tr>
                </table>
                <table style="margin-left: 10px; margin-right: 20px; ">
                <tr >   
                        <td>
                            <label>Fecha Sol.:</label>
                            <input type="text" id="fecha_solicitud"  name="fecha_solicitud" class="fechas" style="width:80px;" class="requerido" readonly/>
                        </td>
                        <td>
                            <label>Fecha Inicio:</label>
                            <input type="text" id="fecha_ini"  name="fecha_ini" class="fechas" style="width:80px;" class="requerido" readonly/>
                        </td>    
                         <td >
                            <label>Total Dias: </label>
                            <input type="text" id="duracion_dias" style="width:80px" class="form-label15" class="requerido" onchange="calcularDiasF()">
                        </td>
                        <td>
                            <label>Fecha Fin:</label>
                            <input type="text" id="fecha_fin"  name="fecha_fin" class="fechas" style="width:80px;" onblur="calcularDias()" class="requerido" />
                        </td>
                        <td>
                            <label>Hora Inicio:</label>
                            <input type="text" id="hora_ini"  name="hora_ini" class="horas" style="width:80px;" class="requerido" readonly/>
                        </td>    
                        <td>
                            <label>Hora Fin:</label>
                            <input type="text" id="hora_fin"  name="hora_fin" class="horas" style="width:80px;" onchange="restarHoras()" class="requerido" readonly/>
                        </td>
                        
                        <td >
                            <label>Total Horas: </label>
                            <input type="text" id="duracion_horas" style="width: 80px" class="form-label15" class="requerido" readonly>
                        </td>
                        
                    </tr> 
                    </table>

                <table style="margin-left: 10px; margin-right: 20px; ">
                    <tr >
                        <td>
                            <label>C�digo:</label>
                            <input id="cod_enfermedad" name="cod_enfermedad" style="width: 80px" type="text" class="requerido" readonly>
                        </td>    <td>
                            <img alt="buscar" src="/fintra/images/botones/iconos/lupa.gif" style="cursor:pointer; margin-top: 10px;" onclick="cargarEnfermedades();" height="15px" width="15px" readonly>
                        </td>
                        <td colspan="3">
                            <label>Enfermedad:</label>
                            <input type="text" id="enfermedad" style="width: 430px" class="form-label15" readonly class="requerido" readonly>
                        </td>
                        <td>
                            <label>Jefe Directo</label>
                            <select id="jefe_directo" name="jefe_directo" class="form-label15" style="width: 130px" class="requerido" readonly></select>
                        </td>
                    </tr>  
                </table> 
                <table style="margin-left: 20px; margin-right: 20px; " >
                    <tr >
                        <label style="margin-left: 14px">Descripci�n</label>
                        <textarea id="descripcion" name="descripcion" style="width: 685px;margin-left: 14px" type="text" class="requerido" readonly>
                        
                        </textarea>
                    </tr>
                    <tr >
                        <label style="margin-left: 14px">Comentario</label>
                        <textarea id="comentario" name="comentario" style="width: 685px;margin-left: 14px" type="text" class="requerido">
                        
                        </textarea>
                    </tr>
                    <tr>
                        <td >
                            <label>Dias a disfrutar: </label>
                            <input type="text" id="dias_disfrute" style="width:80px" class="form-label15" class="requerido" readonly>
                        </td>
                        <td >
                            <label>Dias a pagar: </label>
                            <input type="text" id="dias_a_pagar" style="width:80px" class="form-label15" class="requerido" readonly>
                        </td>
                        <td >
                            <label>Aprobado: </label>
                            <select type="text" id="aprobado" style="width:80px" class="form-label15" class="requerido" >
                                <option value="">...</option>
                                <option value="S">SI </option>
                                <option value="N">NO</option>
                            </select>
                        </td>
                        
                     </tr>
                </table>  
                </table>
            </div>
        </div>
        <div id="verarchivo" style="display: none" >
            <table style="width: 100%">
                <td style="width:50%" colspan="3">
                     <fieldset >
                         <legend>Archivos Cargados</legend>    
                         <div style="height:80px;overflow:auto;">
                            <table id="tbl_archivos_cargados" aling="center" style=" width: 100%" >             

                            </table>
                         </div>
                     </fieldset>
                </td>

            </table>
        </div>
        <div id="info"  class="ventana" >
            <p id="notific">EXITO AL GUARDAR</p>
        </div>
    </center>
    </body>
    <script>
  
        inicio1();
    

</script>
</html>

