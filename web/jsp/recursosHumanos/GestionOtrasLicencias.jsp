<%-- 
    Document   : GestionLicencia
    Created on : 13/06/2017, 12:20:14 AM
    Author     : dvalencia
--%>


<script type="text/javascript" src="./js/recursosHumanos/GestionOtrasLicencias.js"></script> 
<style>
            .onoffswitch-inner:before {
                content: "A";
                padding-left: 14px;
                background-color: #34A7C1; color: #FFFFFF;
                
            }
            .onoffswitch-inner:after {
                content: "I";
                padding-right: 14px;
                background-color: #C20F0F; color: #F5F0F0;
                text-align: right;
                
            }
            fieldset    {
                height: 207px;
                margin: 0 1em 0 0;
                padding: 0.5em 1em;
                overflow-y: auto;
                border-radius: 4px;  
            }
            input.form-control, select.form-control{
                height: 30px !important;
                padding-top:0;
                padding-bottom: 0;
            }
            .row{
                margin-top: 10px;
            }
            #dialogNovedades{
                height:auto !important;
                width:auto !important;
                overflow-x: hidden !important;
                padding-left: 3em;
                padding-right: 3em;
            }
            
        </style>
<div id="capaCentral" align="left" style="position:absolute; width:100%; height:100%; z-index:0; top: -40px;" >
    
        <center>
          <div id="tablita" style="top: 10px;width: 1300px;height: 100px;" >
            <div class="ui-dialog-titlebar ui-widget-header ui-helper-clearfix" style="height: 30px;">
<!--                <span class="ui-dialog-title text-center" id="ui-dialog-title-listaGlobal">-->
            
                    <label  style='display:flex !important; justify-content:center; align-items:center;'>
                        <h5><b>FILTRO DE BUSQUEDA OTRAS LICENCIAS</b></h5>
                    </label>
<!--                </span>-->
            </div>  
                   <table id="tablainterna" style="height: 1px; width: 1200px"  >
                        <tr>
                            <td>
                                <label><h6>Fecha Solicitud</h6></label>
                            </td>
                            <td>
                                <input type="text" id="fechaini" name="fechaini" style="height: 23px;width: 80px;color: #070708;" readonly>

                                <label >-</label>

                                <input type="text" id="fechafin" name="fechafin" style="height: 23px;width: 80px;color: #070708;margin-left: 4px;" readonly>
                            </td>
                            <td>
                                <label><h6>Identificacion</h6></label>
                            </td>
                            <td>
                                <input type="text" id="identificacion2" name="identificacion2" style="height: 23px;width: 100px;color: #070708;" class="solo-numero" >
                            </td>
<!--                            <td>
                                <label><h6>Estado Solicitud</h6></label>
                            </td>
                            <td>
                                <select id="status" name="status" style="height: 23px;width: 118px;color: #070708;">
                                    <option value="">...</option>
                                    <option value="S">APROBADO</option>
                                    <option value="N">RECHAZADO</option>
                                    <option value="P">PENDIENTE</option>
                                </select>
                            </td>-->
                            <td>
                                <hr style="width: 2px;height: 39px;top: -25px;margin-left: 13px;color: rgba(128, 128, 128, 0.39);" /> 
                            </td>

                            <td>
                                <div style="padding-top: 10px">
                                    <button id="buscar" class="ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only" style="margin-left: 15px;top: -7px;" > 
                                        <span class="ui-button-text">Buscar</span>
                                    </button>  
                                </div>
                            </td>
                            <td>
                                <div style="padding-top: 10px">
                                    <button id="limpiar" class="ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only" style="margin-left: -2px;top: -7px;width: 102px;" > 
                                        <span class="ui-button-text">Limpiar</span>
                                    </button>  
                                </div>
                            </td>
                        </tr>
                    </table>

            </div>
          </div>    
        <div style="position: relative;top: 120px">
            <center><table id="tabla_Novedades" ></table><div id="pager"></div></center>
            
        </div>
        <div id="dialogEnfermedades"  class="ventana">
            <table id="tabla_Enfermedades" ></table>
            <div id="pager"></div>
        </div>    
        <div id="dialogNovedades"  class="ventana" >
            <div class='row'>
                <div class='col-md-4' >
                    
                </div>
                <div class='col-md-8 text-right' >

                <div style="float: right">
                    <table style="margin-right: 20px;">
                        <tr>
                            <td>
                                <label style="float: right;">Numero Solicitud:</label>
                            </td>
                            <td>
                                <input id="numsolicitud" readonly>
                            </td>
                            <td>
                                <label>Fecha Solicitud:</label>
                            </td>
                            <td>
                                <input id="fechaSolicitud"  readonly>
                            </td>
                        </tr>
                    </table>
                </div>
                </div>
            </div>
            <div class='row form-novedades'>
                <tr>
                        <td>
                            <input type="text" id="id" style="width: 137px;color: black;" hidden>
                        </td>
                </tr>
                <div class='col-md-4' >
                    <label style='font-weight: 10px !important;'>Tipo Novedad:</label>
                    <select id="tipo_novedad" name="tipo_novedad"  class="form-control" class="requerido"></select>
                </div>
                <div class='col-md-4' >
                    <label style='font-weight: 10px !important;'>Clase de Licencia:  </label>
                    <select id="razon" name="razon" class="form-control" class="requerido"></select> 
                </div>
            </div>
                
            <div class='row form-novedades'>
                <div class='col-md-5' >
                    <label>Identificacion</label>
                    <input type="text"  id="identificacion" class="form-control" <%--onkeyup="format(this)" onchange="format(this)" --%>onblur="cargarNombre(this.value);cargarNombreEPS(this.value);cargarNombreARL(this.value);" class="requerido" >
                </div>
                <div class='col-md-7' >
                    <label>Nombre Completo: </label>
                        <input type="text" id="nombre_completo"  class="form-control" onchange="conMayusculas(this)" readonly class="requerido">
                </div>
                    
            </div>
            <div class='row form-novedades'>
                    
                    <div class='col-md-3'>
                        <label  class="visfecha">Fecha Ini:</label>
                        <input type="text" id="fecha_ini"  name="fecha_ini" class="visfecha form-control" readonly />
                    </div>
                    <div class='col-md-3'>
                        <label class="visfecha">Total Dias: </label>
                        <input type="text" id="duracion_dias"  class="visfecha form-control"  onkeyup ="sumarDias()" readonly>
                    </div>
                    <div class='col-md-3'>
                         <label  class="visfecha">Fecha Fin:</label>
                         <input type="text" id="fecha_fin"  name="fecha_fin"  class="visfecha form-control" readonly/>
                    </div>

                    <div class='col-md-3'>
                        <label>Jefe Directo</label>
                        <select id="jefe_directo" name="jefe_directo" class="form-control" readonly></select>
                    </div>
            </div>
            <div class='row'>
                <div class="col-md-12">
                    <label >Descripción:</label>
                    <textarea id="descripcion" name="descripcion"  type="text" class="form-control" onchange="conMayusculas(this)" readonly> </textarea>
                </div>
                <div class="col-md-12">
                    <label >Comentario</label>
                    <textarea id="comentario" name="comentario"  type="text" class="form-control" onchange="conMayusculas(this)" > </textarea>
                </div>
            </div>
            </div>
        </div>
        <div id="cargararchivo" style="display: none" >
            <table style="width: 100%">
                <tr>                          
                    <td class="letra_resaltada" align="center">&nbsp; Seleccione el archivo </td>
                    <td align="left">
                        <form id="formulario" name="formulario"> 
                            <input type="hidden" name="identificacion1" id="identificacion1" >
                            <input type="file" id="archivo"  name="archivo" style="width: 270px">                                  
                        </form>
                    </td>       
                    <td>
                        <button id="cargarArchivo" class="ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only" 
                                role="button" aria-disabled="false" onclick="cargarArchivo()">
                            <span class="ui-button-text">Subir Archivo</span>
                        </button>
                    </td>   
                <tr
            </tr> 
        </table>
    </div>
    <div id="verarchivo" style="display: none" >
        <table style="width: 100%">
            <td style="width:50%" colspan="3">
                <fieldset >
                    <legend>Archivos Cargados</legend>    
                    <div style="height:80px;overflow:auto;">
                        <table id="tbl_archivos_cargados" aling="center" style=" width: 100%" >             

                        </table>
                    </div>
                </fieldset>
            </td>

            </table>
        </div>
        <div id="info"  class="ventana" >
            <p id="notific">EXITO AL GUARDAR</p>
        </div>

            </center>
<!--            <div id="dialogMsj" title="Mensaje" style="display:none; visibility: hidden;">
                <p style="font-size: 12px;text-align:justify;" id="msj" > Texto </p>
            </div>                                   -->
 </div>
