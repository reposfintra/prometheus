<%-- 
    Document   : menuRegistroSolicitudNovedad
    Created on : 25/01/2017, 08:32:26 AM
    Author     : dvalencia
--%>

<%@page contentType="text/html" pageEncoding="ISO-8859-1"%>
<%@page session="true"%>
<%@page errorPage="/error/ErrorPage.jsp"%>
<!DOCTYPE html>
<html lang="es">

    <head>

        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="description" content="">
        <meta name="author" content="">
        <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
     
        <title>Visto Bueno Solicitud de Novedades</title>

<!--        Bootstrap Core CSS -->
        <link href="./css/bootstrap.min.css" rel="stylesheet" type="text/css"/>
        <link href="./css/simple-sidebar.css" rel="stylesheet" type="text/css"/>

        <link type="text/css" rel="stylesheet" href="./css/jquery/jquery-ui/jquery-ui.css" />
        <link href="./css/popup.css" rel="stylesheet" type="text/css">
        <link type="text/css" rel="stylesheet" href="./css/estilosRRHH.css" />
        <link type="text/css" rel="stylesheet" href="./css/TransportadorasApi.css" />
        
        <script type="text/javascript" src="./js/jquery/jquery.jqGrid/js/jquery.min.js"></script> 
        <script type="text/javascript" src="./js/jquery-ui-1.8.5.custom.min.js"></script>
        <script type="text/javascript" src="./js/recursosHumanos/menuVistoBuenoSolicitudNovedad1.js"></script> 
        <script type="text/javascript" src="./js/recursosHumanos/menuVistoBuenoSolicitudNovedad.js"></script> 


        <!--css logica de negocio-->
        <link href="./css/asobancaria.css" rel="stylesheet" type="text/css">

        <!--jqgrid--> 
        <script type="text/javascript" src="./js/jquery/jquery.jqGrid-4.7.0/js/grid.locale-es.js"></script>
        <script type="text/javascript" src="./js/jquery/jquery.jqGrid-4.7.0/js/jquery.jqGrid.min.js"></script>
        <script type="text/javascript" src="./js/jquery/jquery.jqGrid-4.7.0/plugins/jquery.contextmenu.js"></script>
        <script type="text/javascript" src="./js/jquery/jquery.jqGrid-4.7.0/js/jquery.tabletojson.js"></script>    
        <!--<script type="text/javascript" src="./js/jquery/jquery.jqGrid/js/jquery.min.js"></script>--> 
        <script type="text/javascript" src="./js/jquery-ui-1.8.5.custom.min.js"></script>
        <script type="text/javascript" src="./js/jquery/timepicker/timepicker.js"></script>
        <script type="text/javascript" src="./js/utilidadInformacion.js"></script>
    </head>
    <body>
        <jsp:include page="/WEB-INF/toptsp.jsp?encabezado=MODULO VISTO BUENO DE NOVEDADES"/>
        <div id="wrapper">

            <!-- Sidebar -->
            <div id="sidebar-wrapper">
                <ul id="menu-pagina" class="sidebar-nav">
                </ul>
            </div>
            <!-- /#sidebar-wrapper -->

            <!-- Page Content -->
            <div id="page-content-wrapper" >                 
               
                <button id="menu-toggle" type="button" class="btn btn-default btn-lg">
                    <span class="glyphicon glyphicon-menu-hamburger" aria-hidden="true"></span>
                </button>    
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-lg-12">
                            <div id="container">
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- /#page-content-wrapper -->

        </div>
        <!-- /#wrapper -->
        <!-- Dialogo ventana detalle de recaudo> -->
        <div id="dialogRecaudoDet" title="Detalle de recaudo" style="display:none;">       
            <center><table id="tabla_detalle_recaudo"></table></center>
        </div>
        <!-- Dialogo ventana emergente detalle de recaudo> -->
        <div id="div_recaudo_info_proc"  style="display: none; width: 800px" >   
                <table border="0"  align="center">
                    <tr>
                        <td>
                            <table id="tabla_info_pago_extracto" ></table>
                            <div id="page_tabla_info_pago_extracto"></div>
                        </td>
                    </tr>
                </table>
        </div> 
         <!-- Dialogo menu contextual ventana detalle de recaudo> -->
        <div class="contextMenu" id="myMenu1" style="display:none;">
            <ul  id="lstOpciones" >
                <li id="ver_detalle_pago" class="even">
                    <span style="float:left"></span>
                    <span style="font-size:11px; font-family:Verdana">Ver información de pago</span>
                </li>  
                <li id="ver_causal_dev" class="odd">
                    <span style="float:left"></span>
                    <span style="font-size:11px; font-family:Verdana">Ver causal devolución</span>
                </li>  
            </ul>
        </div>
        <div id="dialogLoading" style="display:none;">
            <p  style="font-size: 12px;text-align:justify;" id="msj2">Texto </p> <br/>
            <center>
                <img src="./images/cargandoCM.gif"/>
            </center>
        </div>
        <!-- Dialogo de los jsp> -->
        <div id="dialogMsj" style="display:none;">
            <p style="font-size: 12px;text-align:justify;" id="msj" > Texto </p>
        </div>            
    </body>

</html>