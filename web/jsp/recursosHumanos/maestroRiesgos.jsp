<%-- 
    Document   : maestroRiesgos
    Created on : 20/10/2016, 04:06:04 PM
    Author     : dvalencia
--%>

<%@page contentType="text/html" pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
        <title>MAESTRO TIPOS DE RIESGOS</title>
        <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
        <link href="./css/popup.css" rel="stylesheet" type="text/css">
        <link type="text/css" rel="stylesheet" href="./css/jquery/jquery-ui/jquery-ui.css" />
        <link type="text/css" rel="stylesheet" href="./css/TransportadorasApi.css" />
        <script type="text/javascript" src="./js/jquery/jquery.jqGrid/js/jquery.min.js"></script>
        <script type="text/javascript" src="./js/jquery-ui-1.8.5.custom.min.js"></script>   
        <script type="text/javascript" src="./js/jquery/jquery.jqGrid-4.7.0/js/grid.locale-es.js"></script>
        <script type="text/javascript" src="./js/jquery/jquery.jqGrid-4.7.0/js/jquery.jqGrid.min.js"></script>
        <script type="text/javascript" src="./js/jquery/jquery.jqGrid-4.7.0/js/jquery.tabletojson.js"></script>   
        <script type="text/javascript" src="./js/recursosHumanos/maestroRiesgos.js"></script> 
    </head>
    <body>
       <div id="capaSuperior" style="position:absolute; width:100%; height:100px; z-index:0; left: 0px; top: 0px;">
            <jsp:include page="/WEB-INF/toptsp.jsp?encabezado=MAESTRO TIPOS DE RIESGOS"/>
        </div>
        <style>
            .onoffswitch-inner:before {
                content: "A";
                padding-left: 14px;
                background-color: #34A7C1; color: #FFFFFF;
            }
            .onoffswitch-inner:after {
                content: "I";
                padding-right: 11px;
                background-color: #C20F0F; color: #F5F0F0;
                text-align: right;
            }
        </style>
        <center>
        <div style="position: relative;top: 150px">
            <table id="tabla_Riesgos" ></table>
            <div id="pager"></div>
        </div>
        <div id="dialogRiesgos"  class="ventana">
            <div id="tablainterna" style="width: 550px;" >
                <table id="tablainterna"  >
                    <tr>
                        <td>
                            <input type="text" id="id" style="width: 137px;color: black;" hidden>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <label>Intensidad: </label>
                            <input type="text" id="intensidad" style="width: 100px;color: black;" onchange="conMayusculas(this)">
                            <label>Codigo: </label>
                            <input type="text" id="codigo" style="width: 100px;color: black;" readonly>
                            <label>Porcentaje: </label>
                            <input type="text" id="porcentaje" style="width: 99px;color: black;"  onkeypress="return onKeyDecimal(event,this);"  >
                        </td>
                    </tr>
                    <br>
                    <tr>
                        <td>
                            <label>Actividades: </label>
                            <input type="text" id="actividades" style="width: 525px;color: black;"onchange="conMayusculas(this)">
                        </td>
                    </tr>
                </table>
            </div>
        </div>
            
        <div id="info"  class="ventana" >
            <p id="notific">EXITO AL GUARDAR</p>
        </div>
    </center>
    </body>
    <script>
  
        inicio1();
    

</script>
</html>
