<%-- 
    Document   : registroSolicitudNovedades
    Created on : 5/12/2016, 05:29:43 PM
    Author     : dvalencia
--%>

<%@page contentType="text/html" pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
        <title>REGISTRO Y SOLICITUD DE NOVEDADES</title>
        <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
        <link href="./css/popup.css" rel="stylesheet" type="text/css">
        <link type="text/css" rel="stylesheet" href="./css/estilosRRHH.css" />
        <link type="text/css" rel="stylesheet" href="./css/jquery/jquery-ui/jquery-ui.css" />
        <link type="text/css" rel="stylesheet" href="./css/TransportadorasApi.css" />
        <script type="text/javascript" src="./js/jquery/jquery.jqGrid/js/jquery.min.js"></script>
        <script type="text/javascript" src="./js/jquery-ui-1.8.5.custom.min.js"></script>   
        <script type="text/javascript" src="./js/jquery/jquery.jqGrid-4.7.0/js/grid.locale-es.js"></script>
        <script type="text/javascript" src="./js/jquery/jquery.jqGrid-4.7.0/js/jquery.jqGrid.min.js"></script>
        <script type="text/javascript" src="./js/jquery/jquery.jqGrid-4.7.0/js/jquery.tabletojson.js"></script> 
        <script type="text/javascript" src="./js/jquery/timepicker/timepicker.js"></script>
        <script type="text/javascript" src="./js/utilidadInformacion.js"></script> 
        <%--<script type="text/javascript" src="./js/jquery/timepicker/timepicker.css"></script>--%>
        <script type="text/javascript" src="./js/recursosHumanos/registroSolicitudNovedades.js"></script> 
    </head>
    <body>
        <div id="capaSuperior" style="position:absolute; width:100%; height:100px; z-index:0; left: 0px; top: 0px;">
            <jsp:include page="/WEB-INF/toptsp.jsp?encabezado=REGISTRO Y SOLICITUD DE NOVEDADES"/>
        </div>
        <style>
            .onoffswitch-inner:before {
                content: "A";
                padding-left: 14px;
                background-color: #34A7C1; color: #FFFFFF;
            }
            .onoffswitch-inner:after {
                content: "I";
                padding-right: 11px;
                background-color: #C20F0F; color: #F5F0F0;
                text-align: right;
            }
            fieldset    {
                height: 207px;
                margin: 0 1em 0 0;
                padding: 0.5em 1em;
                overflow-y: auto;
                border-radius: 5px;  
            }
        </style>
    <center>
        <div style="position: relative;top: 150px">
            <table id="tabla_Novedades" ></table>
            <div id="pager"></div>
        </div>
        <div id="dialogEnfermedades"  class="ventana">
            <table id="tabla_Enfermedades" ></table>
            <div id="pager"></div>
        </div>    
        <div id="dialogNovedades"  class="ventana">
            <div id="tablainterna" style="width: 915px;" >
                <div style="float: right">
                    <table>
                        <tr>
                            <td>
                                <label style="float: right;">Numero Solicitud:</label>
                            </td>
                            <td>
                                <input id="numsolicitud" readonly>
                            </td>
                            <td>
                                <label>Fecha Solicitud:</label>
                            </td>
                            <td>
                                <input id="fechaSolicitud"  readonly>
                            </td>
                        </tr>
                    </table>
                </div>
                <table style="margin-left: 10px; margin-right: 20px; ">
                    <tr>
                        <td>
                            <input type="text" id="id" style="width: 137px;color: black;" hidden>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="6">
                            <hr style="width: 902px;height: 2px;top: -25px;color: rgba(128, 128, 128, 0.39);" /> 
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <label>Tipo Novedad: </label>
                            <select id="tipo_novedad" name="tipo_novedad" style="width:120px;" class="form-label15" onchange="cargarRazon(this.value);
                                    activarCampos();
                                    visualizar()" class="requerido" ></select>
                        </td>  
                        <td>
                            <label>Novedad</label>
                            <select id="razon" name="razon" style="width: 120px" class="form-label15" class="requerido" onchange="activarCampos();
                                    visualizarRazon()"></select>
                        </td>
                        <td>
                            <label>Identificacion</label>
                            <input type="text"  id="identificacion" class="form-label15" <%--onkeyup="format(this)" onchange="format(this)" --%>onblur="cargarNombre(this.value)" class="requerido" >
                        </td>

                        <td colspan="2">
                            <label>Nombre Completo: </label>
                            <input type="text" id="nombre_completo" style="width: 340px" class="form-label15" onchange="conMayusculas(this)" readonly class="requerido">
                        </td>
                        <td>
                            <label>Jefe Directo</label>
                            <select id="jefe_directo" name="jefe_directo" class="form-label15" style="width: 130px" ></select>
                        </td>
                    </tr>
                    <!--                    <tr>
                                            <td colspan="6">
                                                <hr style="width: 902px;height: 2px;top: -25px;color: rgba(128, 128, 128, 0.39);" /> 
                                            </td>
                                        </tr>-->
                </table>
                <fieldset class="scheduler-border" style="margin-top: 18px;margin-left: 12px;" >
                    <table style="margin-left: 10px; margin-right: 20px; width: 325px;"> <!-- 615 -->
                        <tr >   
                            <td>
                                <label  class="visfecha">Fecha Inicio:</label>
                                <input type="text" id="fecha_ini"  name="fecha_ini" class="visfecha" class="visfecha" style="width:80px;" class="requerido"/>
                            </td>
                            <td>
                                <label class="visfecha">Total Dias: </label>
                                <input type="text" id="duracion_dias" style="width:80px" class="visfecha" class="requerido" onkeyup ="calcularDiasF()">
                            </td>
                            <td>
                                <label  class="visfecha">Fecha Fin:</label>
                                <input type="text" id="fecha_fin"  name="fecha_fin"  class="visfecha" style="width:80px;" onblur="calcularDias()" class="requerido"  readonly/>
                            </td>
                            <td>
                                <label class="visHoras"> Hora Inicio:</label>
                                <input type="text" id="hora_ini"  name="hora_ini" class="horas" style="width:80px;"/>
                            </td>    
                            <td>
                                <label  class="visHoras">Hora Fin:</label>
                                <input type="text" id="hora_fin"  name="hora_fin" class="horas" style="width:80px;" onchange="restarHoras()"/>
                            </td>
                            <td>
                                <label   class="visHoras">Total Horas: </label>
                                <input type="text" id="duracion_horas" style="width: 80px"   class="visHoras">
                            </td>
                            <td>
                                <label class="visDiaD">Dias a disfrutar: </label>
                                <input type="text" id="dias_disfrute" style="width:80px" class="visDiaD" >
                            </td>
                            <td>
                                <label class="visDiaD">Dias a pagar: </label>
                                <input type="text" id="dias_a_pagar" style="width:80px" class="visDiaD" readonly>
                            </td>
                            <td class="visrecobro">
                                <label>Recobro: </label>
                                <select type="text" id="recobro" style="width:80px;margin-top: 2px;" class="requerido" readonly >
                                    <option value="">...</option>
                                    <option value="S">SI </option>
                                    <option value="N">NO</option>
                                </select>
                            </td>
                            <!--                            <td>
                                                            <label  class="visproceso" style="margin-left: -36px;">Proceso Actual:</label>
                                                            <input type="text" id="proceso_actual" style="width: 267px;margin-left: -36px;"  class="visproceso">
                                                        </td>
                                                        <td>
                                                            <label  class="visproceso" style="margin-left: -285px;">Proceso Nuevo</label>
                                                            <select type="text" id="proceso_nuevo" style="width:254px;margin-left: -285px;"  class="visproceso"></select>
                                                        </td>-->
                        </tr> 
                    </table>
                    <table style="margin-left: 51px; margin-right: 20px; width: 553px;">
                        <tr>
                            <td>
                                <label  class="visproceso" style="margin-left: -36px;">Proceso Actual:</label>
                                <input type="text" id="proceso_actual" style="width: 267px;margin-left: -36px;"  class="visproceso">
                            </td>
                            <td>
                                <label  class="visproceso" style="margin-left: -285px;">Proceso Nuevo</label>
                                <select type="text" id="proceso_nuevo" style="width:254px;margin-left: -285px;"  class="visproceso"></select>
                            </td>
                        </tr> 
                    </table>

                    <table style="margin-left: 10px; margin-right: 20px;width: 200px ">
                        <tr  class="vis">
                            <td>
                                <label style="margin-left: -3px;">C�digo:</label>
                                <input id="cod_enfermedad" name="cod_enfermedad" style="width: 80px" type="text">
                            </td>  
                            <td>
                                <img alt="buscar" src="/fintra/images/botones/iconos/lupa.gif" style="cursor:pointer; margin-top: 10px;" onclick="cargarCodigosEnfermedades();" height="15px" width="15px">
                            </td>
                            <td>
                                <label style="margin-left: 15px;">Enfermedad:</label>
                                <input type="text" id="enfermedad" style="width: 430px;margin-left: 15px;" class="form-label15" readonly >
                            </td>
                        </tr>  
                    </table> 
                    <table style="margin-left: 20px; margin-right: 20px; width: 100px" >
                        <tr>
                            <td class="descripcion">
                                <label style="margin-left: -10px">Descripci�n</label>
                                <textarea id="descripcion" name="descripcion" style="width: 685px;margin-left: -6px;height: 80px;" type="text" class="requerido"> </textarea>
                            </td> 
                        </tr> 
                    </table>  
                </fieldset>
            </div>
        </div>
        <div id="cargararchivo" style="display: none" >
            <table style="width: 100%">
                <tr>                          
                    <td class="letra_resaltada" align="center">&nbsp; Seleccione el archivo </td>
                    <td align="left">
                        <form id="formulario" name="formulario"> 
                            <input type="hidden" name="identificacion1" id="identificacion1" >
                            <input type="file" id="archivo"  name="archivo" style="width: 270px">                                  
                        </form>
                    </td>       
                    <td>
                        <button id="cargarArchivo" class="ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only" 
                                role="button" aria-disabled="false" onclick="cargarArchivo()">
                            <span class="ui-button-text">Subir Archivo</span>
                        </button>
                    </td>   
                <tr
            </tr> 
        </table>
    </div>
    <div id="verarchivo" style="display: none" >
        <table style="width: 100%">
            <td style="width:50%" colspan="3">
                <fieldset >
                    <legend>Archivos Cargados</legend>    
                    <div style="height:80px;overflow:auto;">
                        <table id="tbl_archivos_cargados" aling="center" style=" width: 100%" >             

                        </table>
                    </div>
                </fieldset>
            </td>

        </table>
    </div>
    <div id="info"  class="ventana" >
        <p id="notific">EXITO AL GUARDAR</p>
    </div>
</center>
</body>
<script>

    inicio1();


</script>
</html>

