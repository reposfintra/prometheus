<%-- 
    Document   : cxc_caja_recaudos.jsp
    Created on : 26/09/2018, 10:59:16 AM
    Author     : MCAMARGO
--%>


<%@page contentType="text/html" pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
    
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
       <link href="./css/popup.css" rel="stylesheet" type="text/css">
        <link type="text/css" rel="stylesheet" href="./css/jquery/jquery-ui/jquery-ui.css" />
        <script type="text/javascript" src="./js/jquery/jquery.jqGrid/js/jquery.min.js"></script>
        <script type="text/javascript" src="./js/jquery-ui-1.8.5.custom.min.js"></script>   
        <script type="text/javascript" src="./js/jquery/jquery.jqGrid-4.7.0/js/grid.locale-es.js"></script>
        <script type="text/javascript" src="./js/jquery/jquery.jqGrid-4.7.0/js/jquery.jqGrid.min.js"></script>
        <script type="text/javascript" src="./js/jquery/jquery.jqGrid-4.7.0/js/jquery.tabletojson.js"></script>   
         <style type="text/css">             
             .hide-close .ui-dialog-titlebar-close { display: none }
         </style>
                    

        <script type="text/javascript" src="./js/cxc_caja_recaudos.js"></script> 
        <title>Generar cxc cajas recaudo</title>
    </head>
    
    <body>
        
        <div id="capaSuperior" style="position:absolute; width:100%; height:100px; z-index:0; left: 0px; top: 0px;">
            <jsp:include page="/WEB-INF/toptsp.jsp?encabezado=CXC CAJAS RECAUDO"/>
        </div>
        
        <center>
            <div id="tablita" style="top: 170px;width: 624px;height: 95px;  margin-top: 100px;" >

                <div class="ui-dialog-titlebar ui-widget-header ui-helper-clearfix">
                    <span class="ui-dialog-title text-center" id="ui-dialog-title-listaGlobal">
                        &nbsp;
                        <label  ><b>FILTRO DE BUSQUEDA</b></label>
                    </span>
                </div>

                <div  style="padding: 5px; background: white; border:1px solid #2A88C8">
                    <div class="col-md-12 ">
                        <table id="tablainterna" style="height: 53px; width: 614px"  >

                            <tr style = "text-align: middle;vertical-align: middle">

                                <td style = "text-align: middle;vertical-align: middle">
                                    <div style = "float: left;valign:middle">
                                    <label>Caja:</label>
                                   <select id="caja" name="caja" style="height: 23px;width: 150px;color: #070708;">
                                                                 
                                   </select>
                                    </div>  
                                </td> 

                                <td style = "text-align: middle;vertical-align: middle">
                                    <div style = "float: left;valign:middle">
                                    <label>A�o:</label>
                                   <select id="anio" name="anio" style="height: 23px;width: 80px;color: #070708;">
                                                                    
                                   </select>
                                    </div>  
                                </td>  
                                
                                <td style = "text-align: middle;vertical-align: middle">
                                    <div style = "float: left;valign:middle">
                                    <label>Mes:</label>
                                   <select id="mes" name="mes" style="height: 23px;width: 50px;color: #070708;">
                                                                
                                   </select>
                                    </div>  
                                </td> 
                                
                                <td style = "text-align: middle;vertical-align: middle">
                                    <div style = "float: left;valign:middle">
                                    <label>Dia:</label>
                                   <select id="dia" name="dia" style="height: 23px;width: 50px;color: #070708;">
                                                                    
                                   </select>
                                    </div>  
                                </td> 
                                
                                <td style = "text-align: middle;vertical-align: middle">
                                    <div style = "float: left">
                                     <hr style="width: 2px;height: 39px;margin-left: 13px;color: rgba(128, 128, 128, 0.39);" /> 
                                     </div>
                                </td> 

                                <td style = "text-align: middle;vertical-align: middle">
                                     <div style = "float: left;" >
                                        <button id="buscar" class="ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only" style="margin-left: 15px;" > 
                                            <span class="ui-button-text">Buscar</span>
                                        </button>  
                                    </div>
                                </td>

                            </tr>
                        </table>
                    </div>
                </div>
            </div>

            <div  style="margin-top: 20px; " >
                <table id="tabla_detalle_caja_recaudo" ></table>
                <div id="pager"></div>
            </div>

            <div id="info"  class="ventana" >
                <p id="notific"></p>
            </div>
            
            <div id="dialogLoading" style="display:none;">
                <p  style="font-size: 12px;text-align:justify;" id="msj2">Texto </p> <br/>
                <center>
                    <img src="./images/cargandoCM.gif"/>
                </center>
            </div>
            <div id="dialogMsj" title="Mensaje" style="display:none; .hide-close .ui-dialog-titlebar-close { display: none }">
                <p style="font-size: 12px;text-align:justify;" id="msj" > Texto </p>
            </div>  


        </center>
    </body>
</html>

