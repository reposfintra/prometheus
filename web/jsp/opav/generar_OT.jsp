 <!--
- Autor : Ing. Pablo Emilio Bassil Orozco
- Date  : 26 de Noviembre de 2009
- Copyrigth Notice : Fintravalores S.A.

Descripcion : Creacion de PDF de prueba
-->
<%-- Declaracion de librerias--%>
<%@page contentType="text/html;"%>
<%@page session="true"%>
<%@page import="java.util.*"%>
<%@page import="com.tsp.opav.model.*"%>
<%@page import="com.tsp.opav.model.beans.*"%>
<%@page import="com.tsp.util.*"%>
<%@page import="com.tsp.util.Util.*"%>
<%@include file="/WEB-INF/InitModel.jsp"%>
<%@taglib uri="http://jakarta.apache.org/taglibs/input-0.90" prefix="input" %>
<html>
    <head>
        <title>Generacion Orden de trabajo</title>
        <link href="../../../css/estilostsp.css" rel="stylesheet" type="text/css">
        <link href="<%=BASEURL%>/css/estilostsp.css" rel="stylesheet" type="text/css">
    </head>

    <script type='text/javascript' src="<%=BASEURL%>/js/boton.js"></script>
    <script type='text/javascript' src="<%=BASEURL%>/js/prototype.js"></script>

    <body>
        <div id="capaSuperior" style="position:absolute; width:100%; height:100px; z-index:0; left: 0px; top: 0px;">
            <jsp:include page="/toptsp.jsp?encabezado=Generacion Orden de trabajo"/>
        </div>

        <script type="text/javascript">

            var generar = 0;

            function sendAction(url){

                if(generar == 0){
                    var p = "id_solicitud=" + $("id_solicitud").value + "&observaciones=" + $("observaciones").value;

                    //if($('consecutivo').checked == 1){ Esto dejo de funcionar
                    if(document.getElementById("consecutivo").checked){
                        p = p + "&cambiarcons=true"
                    }
                    else{
                        p = p + "&cambiarcons=false"
                    }

                    new Ajax.Request(
                        url,
                        {
                            method: 'get',
                            parameters: p,
                            onLoading: loading,
                            onComplete: sendMessage
                        });
                }
                else{
                    alert('Solicitud en proceso...');
                }
            }

            function loading(){
                generar = 1;
                $('loading_div').style.visibility = 'visible';
            }

            function sendMessage(response){
                $('loading_div').style.visibility = 'hidden';
                generar = 0;
                $('imgaceptar').style.visibility = 'hidden';
                $('msj_ok').style.visibility = 'visible';
                $('msj').innerHTML=response.responseText;
            }

        </script>

        <div id="capaCentral" align="center" style="position:absolute; width:100%; height:87%; z-index:0; left: 0px; top: 100px; overflow: scroll;">
            <br>
            <br>
            <table width="414">
                <tr class="fila">
                    <td colspan="2">
                        <div align="center"><strong>GENERACION</strong></div>
                    </td>
                </tr>
                <tr class="fila">
                    <td width="159"><div align="center">Consecutivo Oferta: </div></td>
                    <td width="243" align="center">
                        <input type="text" id="cons" value="<%=request.getParameter("cons")%>" readonly>
                    </td>
                </tr>
                <tr class="fila">
                    <td width="159"><div align="center">Id Solicitud: </div></td>
                    <td width="243" align="center">
                        <input type="text" id="id_solicitud" value="<%=request.getParameter("id_sol")%>" readonly>
                    </td>
                </tr>
                <tr class="fila">
                    <td width="159" height="81"><div align="center">Observaciones: </div></td>
                    <td width="243" align="center">
                        <textarea style="height:8em;width:15em;" id="observaciones"></textarea>
                    </td>
                </tr>
            </table>
            <br>

            <input id="consecutivo" name="consecutivo" type="checkbox" value="true">Si esta OT tiene un consecutivo, no borrarlo

            <br>
            <br>

            <img src="<%=BASEURL%>/images/botones/generar.gif"      id="imgaceptar"     name="imgaceptar"   onClick="sendAction('<%=CONTROLLEROPAV%>?estado=Electricaribe&accion=Ot&opcion=2');" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" style="cursor:hand">
            <img src="<%=BASEURL%>/images/botones/generarPdf.gif"   id="imgimprimir"    name="imgimprimir"  onClick="sendAction('<%=CONTROLLEROPAV%>?estado=Electricaribe&accion=Ot&opcion=3');" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" style="cursor:hand">

            <br>

            <div align="center" id="loading_div" style="visibility: hidden">
                Cargando...
            </div>
            <div align="center" id="msj_ok" style="visibility: hidden">
                <table border="2" align="center">
                  <tr>
                    <td><table width="100%" border="1" align="center"  bordercolor="#F7F5F4" bgcolor="#FFFFFF">
                        <tr>
                            <td width="262" align="center" class="mensajes"><div id="msj"></div></td>
                          <td width="32" background="<%=BASEURL%>/images/cuadronaranja.JPG">&nbsp;</td>
                          <td width="44">&nbsp;</td>
                        </tr>
                    </table></td>
                  </tr>
                </table>
            </div>


        </div>
    </body>
</html>
