<%-- 
    Document   : seguimientoEjecucion
    Created on : 16/06/2010, 03:03:21 PM
    Author     : darrieta-GEOTECH
--%>

<%@page import="com.tsp.operation.model.beans.Usuario"%>
<%@page contentType="text/html" pageEncoding="ISO-8859-1"%>
<%@page import ="java.util.*" %>
<%@page import ="java.sql.Timestamp" %>
<%@page import ="com.tsp.opav.model.*"%>
<%@page import ="com.tsp.opav.model.beans.*"%>
<%@page import ="com.tsp.opav.model.services.*"%>
<%@include file ="/WEB-INF/InitModel.jsp"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">
<%
    String idSolicitud = request.getParameter("id_solicitud");
    NegocioApplus infoSolicitud = modelopav.seguimientoEjecucionService.obtenerInfoSolicitud(idSolicitud);
    String fechaGuardado = modelopav.seguimientoEjecucionService.buscarFechaSeguimiento(idSolicitud, "G");
    String fechaConfirmado = modelopav.seguimientoEjecucionService.buscarFechaSeguimiento(idSolicitud, "");
    boolean guardado = true;
    if(fechaGuardado==null){
        guardado = false;
        if(request.getParameter("fecha")==null){
            fechaGuardado =  new Timestamp(new Date().getTime()).toString().substring(0, 10);
        }else{
            fechaGuardado = request.getParameter("fecha");
        }
    }
    ArrayList<Actividades> consulta = modelopav.seguimientoEjecucionService.consultarAvanceSolicitud(idSolicitud,fechaGuardado);
    ArrayList<OfertaSeguimiento> obsConfirmadas = modelopav.seguimientoEjecucionService.consultarObservacionesSolicitud(idSolicitud, "");
    ArrayList<OfertaSeguimiento> obsGuardadas = modelopav.seguimientoEjecucionService.consultarObservacionesSolicitud(idSolicitud, "G");
    boolean progCompleta = modelopav.seguimientoEjecucionService.tieneProgramacion(idSolicitud);
    String mensaje = progCompleta && !infoSolicitud.getFecha().equals("")
                   ? request.getParameter("mensaje") 
                   : "A�n no se ha realizado toda la programaci�n para esta solicitud";
    
    Usuario usuario = (Usuario) session.getAttribute("Usuario");
    ClientesVerService clvsrv = new ClientesVerService(usuario.getBd());
    String perfil = clvsrv.getPerfil(usuario.getLogin());
%>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
        <title>Seguimiento a la ejecucion de la obra</title>
        <link href="<%= BASEURL%>/css/estilostsp.css" rel='stylesheet'>
        <script type='text/javascript' src="<%= BASEURL%>/js/boton.js"></script>
        <script type='text/javascript' src="<%= BASEURL%>/js/fxHeader.js"></script>
        <script type='text/javascript' src="<%= BASEURL%>/js/prototype.js"></script>
        <script type='text/javascript' src="<%= BASEURL%>/js/validar.js"></script>
        <script type='text/javascript' src="<%= BASEURL%>/js/validate.js"></script>
        <script type='text/javascript' src="<%= BASEURL%>/js/seguimientoEjecucion.js"></script>
        <!-- Las siguientes librerias CSS y JS son para el manejo del calendario -->
        <link rel="stylesheet" type="text/css" href="<%=BASEURL%>/css/jCal/jscal2.css" />
        <link rel="stylesheet" type="text/css" href="<%=BASEURL%>/css/jCal/border-radius.css" />
        <script type="text/javascript" src="<%=BASEURL%>/js/jCal/jscal2.js"></script>
        <script type="text/javascript" src="<%=BASEURL%>/js/jCal/lang/es.js"></script>
        <style type="text/css">
            a, a:visited{
                color:#015C05;
                cursor: pointer;
            }
        </style>
        <script type="text/javascript">
            var json = new Array();
            var fechaI = '';
            function genJSON() {
                json = new Array();
                fechaI = (document.getElementById("verDias").checked)
                         ? '' : '<%=infoSolicitud.getFecha()%>';
                        //document.getElementById('seg'+columna).innerHTML;
                for (k = 0; k < <%=consulta.size()%>; k++) {
                    json[k] = {
                        name: document.getElementById("hidAccion"+k).value,
                        desc: document.getElementById("hidDescActividad"+k).value,
                        values: [{
                                from: parseInt(document.getElementById("hidFechaIni"+k).value),
                                to: parseInt(document.getElementById("hidFechaFin"+k).value),
                                progress: document.getElementById("hidAvanceEsperado"+k).value +'%',
                                progress2: document.getElementById("txtAvance"+k).value +'%'                                       
                            }]
                    };
                }
                AbrirVentana('<%= BASEURL%>');
            }
        </script>
    </head>
    <body onload="segEjecucionLoad()">
        <form method='post' name='form1' action="<%=CONTROLLEROPAV%>?estado=Seguimiento&accion=Ejecucion&id_solicitud=<%=idSolicitud%>">
            <div id="capaSuperior" style="position:absolute; width:100%; height:100px; z-index:0; left: 0px; top: 0px;">
                <jsp:include page="/WEB-INF/toptsp.jsp?encabezado=Seguimiento ejecucion"/>
            </div>
            <div id="capaCentral" style="position:absolute; width:100%; height:100%; z-index:0; left: 0px; top: 100px;">                
                <table border="2" cellspacing="0" cellpadding="0" width="95%" align="center">
                    <tr>
                        <td>
                            <table border="0" cellspacing="0" cellpadding="3" class="tblTitulo" align="center" width="100%">
                                <tr>
                                    <td>Id solicitud</td>
                                    <td style="font-weight: normal"><%=idSolicitud%></td>
                                    <td><strong>Cliente</strong></td>
                                    <td style="font-weight: normal"><%=infoSolicitud.getNombreCliente()%></td>
                                </tr>
                                <tr>
                                    <td><strong>Fecha inicial</strong></td>
                                    <td style="font-weight: normal"><%=infoSolicitud.getFecha()%></td>
                                    <td><!--strong>Fecha final</strong--></td>
                                    <td style="font-weight: normal"><!--%=infoSolicitud.getFRecepcion()%-->
                                        <img src="<%=BASEURL%>/images/botones/iconos/gantt.gif" title="Ver Grafico" onclick="genJSON()">
                                        <input type="checkbox" id="verDias">Ver por Dias
                                    </td>
                                </tr>
                                <tr>
                                    <td><strong>Orden de trabajo</strong></td>
                                    <td style="font-weight: normal"><%=infoSolicitud.getNumOs()%></td>
                                    <td><strong>Fecha seguimiento</strong></td>
                                    <td>
                                        <input type="text" id="txtFecha" name="txtFecha" value="<%=fechaGuardado%>" readonly/>
                                        <!--%if(!guardado){%-->
                                        <img src="<%=BASEURL%>/js/Calendario/cal.gif" id="imgFecha" />
                                        <script type="text/javascript">
                                            Calendar.setup({
                                                inputField : "txtFecha",
                                                trigger    : "imgFecha",
                                                max        : Calendar.dateToInt(new Date()),
                                                //dateFormat : "%Y-%m-%d %H:%M:%S",
                                                //showTime   : true,
                                                onSelect   : function() {
                                                    this.hide();
                                                    location.href = "<%=BASEURL%>/jsp/opav/seguimientoEjecucion.jsp?id_solicitud=<%=idSolicitud%>&fecha="+$F("txtFecha");
                                                }
                                            });
                                        </script>
                                        <!--%}%-->
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <input type="hidden" id="numActividades" name="numActividades" value="<%=consulta.size()%>"/>
                            <input type="hidden" id="guardado" name="guardado" value="<%=guardado%>"/>
                            <input type="hidden" id="fechaGuardado" name="fechaGuardado" value="<%=fechaGuardado%>"/>
                            <input type="hidden" id="fechaConfirmado" name="fechaConfirmado" value="<%=fechaConfirmado%>"/>
                            <table border="0" id="tabActividades" width="95%" >
                                <tr class="tblTitulo">
                                    <th scope="col" width="30%">Acci&oacute;n</th>
                                    <th scope="col" width="20%">Actividad</th>
                                    <th scope="col">Responsable</th>
                                    <th scope="col">Peso obra</th>
                                    <th scope="col">% avance esperado</th>
                                    <th scope="col" width="110">% anterior <%=consulta.size()>0?(consulta.get(0).getFecha()==null?"":consulta.get(0).getFecha()):""%></th>
                                    <th scope="col" width="110">% actual</th>
                                    <th scope="col"><!--a onclick="agregarObservacion()"></a-->Observaci&oacute;n</th>
                                </tr>
                                <tr style="height: 20px;" class="filaazul">
                                    <td>&nbsp;</td>
                                    <td>&nbsp;</td>
                                    <td>&nbsp;</td>
                                    <td>&nbsp;</td>
                                    <td align="center"><strong id="totalEsp">&nbsp;</strong></td>
                                    <td align="center"><strong id="totalAnt">&nbsp;</strong></td>
                                    <td align="center"><strong id="total">&nbsp;</strong></td>
                                    <td>&nbsp;</td>
                                </tr>
                                <%
                                float totalAvanceAnt=0;
                                float totalAvanceEsp=0;
                                float totalAvance=0;
                                for (int j = 0; j < consulta.size(); j++) {
                                    Actividades act = consulta.get(j);
                                    totalAvanceAnt += (act.getPeso_obra()*act.getAvance_anterior()) / 100;
                                    float avEsperado = (100*act.getDias_actual()) / act.getDias_actividad();
                                    avEsperado = (avEsperado<100)?avEsperado:100;
                                    totalAvanceEsp += (act.getPeso_obra()*avEsperado)/100;
                                    totalAvance += (act.getPeso_obra()*act.getAvance())/100;
                                %>
                                <tr class="<%=(j % 2 == 0 ? "filagris" : "filaazul")%>" id="fila<%=j%>">
                                    <td align="left">
                                        <%=act.getDescAccion()%>
                                        <input type="hidden" name="hidIdAccion<%=j%>" value="<%=act.getId_accion()%>"/>
                                        <input type="hidden" name="hidIdActividad<%=j%>" value="<%=act.getId()%>"/>
                                        <input type="hidden" name="hidResponsable<%=j%>" value="<%=act.getResponsable()%>"/>
                                        <input type="hidden" name="hidDescActividad<%=j%>" id="hidDescActividad<%=j%>" value="<%=act.getDescripcion()%>"/>
                                        <input type="hidden" name="hidPesoObra<%=j%>" id="hidPesoObra<%=j%>" value="<%=act.getPeso_obra()%>"/>
                                        <input type="hidden" name="hidAvanceAnterior<%=j%>" id="hidAvanceAnterior<%=j%>" value="<%=act.getAvance_anterior()%>"/>
                                        <input type="hidden" name="hidAvanceEsperado<%=j%>" id="hidAvanceEsperado<%=j%>" value="<%=avEsperado%>"/>
                                        <input type="hidden" name="hidFechaIni<%=j%>" id="hidFechaIni<%=j%>" value="<%=act.getFechaInicial()%>"/>
                                        <input type="hidden" name="hidFechaFin<%=j%>" id="hidFechaFin<%=j%>" value="<%=act.getFechaFinal()%>"/>
                                        <input type="hidden" name="hidAccion<%=j%>" id="hidAccion<%=j%>" value="<%=act.getDescAccion()%>"/>
                                    </td>
                                    <td align="left"><%=act.getDescripcion()%> </td>
                                    <td align="left"><%=act.getResponsable()%> </td>
                                    <td align="center"><%=act.getPeso_obra()%> </td>
                                    <td align="center"><%=avEsperado%>%</td>
                                    <td align="center"><%=act.getAvance_anterior()%>%</td>
                                    <td align="center"><input type="text" id="txtAvance<%=j%>" name="txtAvance<%=j%>" value="<%=act.getAvance()%>" size="5" onKeyPress="return soloDigitos(event, '');" onchange="calcularTotal()"/>%</td>
                                    <td align="center" title=""><!--a onclick="agregarObservacion(< %=j%>)">Observaci&oacute;n</a-->
                                        <textarea id="comentario<%=j%>" name="comentario<%=j%>" cols="10"><%act.getObservaciones();%></textarea>
                                    </td>
                                </tr>
                                <%}%>
                            </table>
                            <input type="hidden" id="hidTotalAvanceAnt" name="hidTotalAvanceAnt" value="<%=totalAvanceAnt%>"/>
                            <input type="hidden" id="hidTotalAvanceEsp" name="hidTotalAvanceEsp" value="<%=totalAvanceEsp%>"/>
                            <input type="hidden" id="hidTotalAvance" name="hidTotalAvance" value="<%=totalAvance%>"/>
                        </td>
                        <!--td class="fila" style="padding: 10px; vertical-align: top">
                            < %
                            String totalObservaciones = "";
                            for (int idx = 0; idx < obsConfirmadas.size(); idx++) {
                                OfertaSeguimiento os = obsConfirmadas.get(idx);
                                totalObservaciones += os.getUsuario()+" "+os.getFecha()+"\n"+os.getObservaciones()+"\n\n";
                            }%>
                            Observaciones solicitud<br/>
                            <textarea name="areaObsGuardada" id="areaObsGuardada" cols="45" rows="5" readonly>< %=totalObservaciones%></textarea>
                            <br/><br/>
                            < %
                            totalObservaciones = "";
                            if(guardado && obsGuardadas.size()>0 ){
                                OfertaSeguimiento os = obsGuardadas.get(0);
                                totalObservaciones = os.getObservaciones();
                            }%>
                            Observacion fecha<br/>
                            <textarea name="areaObservacion" id="areaObservacion" cols="45" rows="5" readonly>< %=totalObservaciones%></textarea>
                        </td-->
                    </tr>
                </table>
                <div align="center">
                    <%if (progCompleta && !infoSolicitud.getFecha().equals("")) {%>
                    <p><br/>
                        <img style="cursor:hand" src="<%=BASEURL%>/images/botones/confirmar.gif" align="absmiddle" onmouseover="botonOver(this);" onmouseout="botonOut(this);" onClick="confirmarSeguimiento()">
                        <img style="cursor:hand" src="<%=BASEURL%>/images/botones/guardar.gif" align="absmiddle" onmouseover="botonOver(this);" onmouseout="botonOut(this);" onClick="guardarSeguimiento()">
                        <img style="cursor:hand" src="<%=BASEURL%>/images/botones/restablecer.gif" align="absmiddle" onmouseover="botonOver(this);" onmouseout="botonOut(this);" onClick="restablecerSeguimiento('<%=BASEURL%>','<%=idSolicitud%>')">
                        <img style="cursor:hand" src="<%=BASEURL%>/images/botones/salir.gif" align="absmiddle" onmouseover="botonOver(this);" onmouseout="botonOut(this);" onClick="parent.close();">
                        <br/>
                        <br/>
                    </p>
                    <%}%>
                    <%if (mensaje != null) {%>
                    <table border="2" align="center">
                        <tr>
                            <td>
                                <table width="100%"  border="0" align="center"  bordercolor="#f7f5f4" bgcolor="#ffffff">
                                    <tr>
                                        <td width="229" align="center" class="mensajes"><%=mensaje%></td>
                                        <td width="29" background="<%=BASEURL%>/images/cuadronaranja.JPG">&nbsp;&nbsp;</td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                    </table>
                    <%}%>
                </div>
            </div>
        </form>
        <script type="text/javascript">
            fxheaderInit('tabActividades',500,2,0);
            fxheader();
        </script>
    </body>
</html>
