<%--
    Document   : ajustesProgramacion
    Created on : 9/06/2010, 08:18:04 AM
    Author     : darrieta-GEOTECH
--%>
 
<%@page import="com.tsp.operation.model.beans.Usuario"%>
<%@page import="com.tsp.opav.model.services.ClientesVerService"%>
<%@page contentType="text/html" pageEncoding="ISO-8859-1"%>
<%@page session ="true"%>
<%@page errorPage ="/error/ErrorPage.jsp"%>
<%@page import ="java.util.*" %>
<%@page import ="com.tsp.opav.model.*"%>
<%@page import ="com.tsp.opav.model.beans.*"%>
<%@include file ="/WEB-INF/InitModel.jsp"%> 
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">
<%
    String idSolicitud = request.getParameter("id_solicitud");
    NegocioApplus infoSolicitud = modelopav.seguimientoEjecucionService.obtenerInfoSolicitud(idSolicitud);
    ArrayList<Actividades> consulta = modelopav.seguimientoEjecucionService.consultarActividadesSolicitud(idSolicitud);
    boolean progCompleta = modelopav.seguimientoEjecucionService.tieneProgramacion(idSolicitud);
    String mensaje = progCompleta ? request.getParameter("mensaje") : "A�n no se ha realizado toda la programaci�n para esta solicitud";
    
    Usuario usuario = ((Usuario) session.getAttribute("Usuario"));
    ClientesVerService clvsrv = new ClientesVerService(usuario.getBd());
    String perfil = clvsrv.getPerfil(usuario.getLogin());
%>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
        <title>Ajustes a la programacion</title>
        <link href="<%= BASEURL%>/css/estilostsp.css" rel='stylesheet'>
        <script type='text/javascript' src="<%= BASEURL%>/js/boton.js"></script>
        <script type='text/javascript' src="<%= BASEURL%>/js/prototype.js"></script>
        <script type='text/javascript' src="<%= BASEURL%>/js/seguimientoEjecucion.js"></script>
        <!-- Las siguientes librerias CSS y JS son para el manejo del calendario -->
        <link rel="stylesheet" type="text/css" href="<%=BASEURL%>/css/jCal/jscal2.css" />
        <link rel="stylesheet" type="text/css" href="<%=BASEURL%>/css/jCal/border-radius.css" />
        <script type="text/javascript" src="<%=BASEURL%>/js/jCal/jscal2.js"></script>
        <script type="text/javascript" src="<%=BASEURL%>/js/jCal/lang/es.js"></script>
        
        <script type="text/javascript">
            var json = new Array();
            var fechaI = '';
            function genJSON() {
                try {
                    var rows = document.getElementById('tabActividades').rows;
                    for(i = 1; i < rows.length; i++) {
                        //alert(rows[i].cells[1].innerHTML);
                        json[i-1] = {
                            name: (!(rows[i-1].cells[0].innerHTML===rows[i].cells[0].innerHTML) 
                                    ? rows[i].cells[0].innerHTML
                                    : ''),
                            desc: rows[i].cells[1].innerHTML,
                            values: [{
                                    from: parseInt(document.getElementById('txtFechaInicial'+(i-1)).value),
                                    to: parseInt(document.getElementById('txtFechaFinal'+(i-1)).value),
                                    progress: ""
                                }]
                        };
                    } 
                    fechaI = (document.getElementById("verDias").checked)?'':document.getElementById('txtFecha').value;
                    AbrirVentana('<%= BASEURL%>');
                } catch(excepcion) {}
            }
            function generarCronograma() {
            new Ajax.Request(
                    "<%=CONTROLLEROPAV%>?estado=Seguimiento&accion=Ejecucion&opcion=generarCronograma&id_solicitud=<%=idSolicitud%>"
                    , { method:'get' }
                );
                
            }
        </script>
    </head>
    <body>
        <div id="capaSuperior" style="position:absolute; width:100%; height:100px; z-index:0; left: 0px; top: 0px;">
            <jsp:include page="/WEB-INF/toptsp.jsp?encabezado=Programacion de actividades"/>
        </div>
        <div id="capaCentral" style="position:absolute; width:100%; height:100%; z-index:0; left: 0px; top: 100px;">
            <form method='post' name='form1' action="<%=CONTROLLEROPAV%>?estado=Seguimiento&accion=Ejecucion&opcion=ajustes&id_solicitud=<%=idSolicitud%>">
                <table border="1" cellspacing="0" cellpadding="0" width="1000px" align="center">
                    <tr>
                        <td>
                            <table border="0" cellspacing="0" cellpadding="3" class="tblTitulo" align="center"  width="100%">
                                <tr>
                                    <td>Id solicitud</td>
                                    <td style="font-weight: normal"><%=idSolicitud%></td>
                                    <td><strong>Cliente</strong></td>
                                    <td style="font-weight: normal"><%=infoSolicitud.getNombreCliente()%></td>
                                </tr>
                                <tr>
                                    <td><strong>Orden de trabajo</strong></td>
                                    <td style="font-weight: normal"><%=infoSolicitud.getNumOs()%></td>
                                    <td><strong>D&iacute;as ejecuci&oacute;n</strong></td>
                                    <td style="font-weight: normal"><%=infoSolicitud.getFRecepcion()%></td>
                                </tr>
                                <tr>
                                    <td><strong>Fecha inicial</strong></td>
                                    <td style="font-weight: normal"><!--%=infoSolicitud.getFecha()%-->
                                        <input type="text" id="txtFecha" name="txtFecha" value="<%=infoSolicitud.getFecha()%>" readonly/>
                                        <% if (clvsrv.ispermitted(perfil, "53")) {%>
                                        <img src="<%=BASEURL%>/js/Calendario/cal.gif" id="imgFecha" />
                                        <script type="text/javascript">
                                            Calendar.setup({
                                                inputField : "txtFecha",
                                                trigger    : "imgFecha",
                                                max        : Calendar.dateToInt(new Date()),
                                                //dateFormat : "%Y-%m-%d %H:%M:%S",
                                                //showTime   : true,
                                                onSelect   : function() {
                                                    this.hide();
                                                    //form1.txtFecha.value = $F("txtFecha");
                                                }
                                            });
                                        </script>
                                        <%}%>
                                    </td>
                                    <td><!--strong>Fecha final</strong--></td>
                                    <td style="font-weight: normal"><!--%=infoSolicitud.getFRecepcion()%-->
                                        <img src="<%=BASEURL%>/images/botones/iconos/gantt.gif" title="Ver Grafico" 
                                             onclick="if(confirm('generar pdf')){generarCronograma();}else{genJSON();}">
                                        <input type="checkbox" id="verDias">Ver por Dias
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <input type="hidden" id="cont" name="cont" value="0">
                            <input type="hidden" id="maximo" name="maximo" value="1">
                            <%if (progCompleta) {%>
                            <input type="hidden" id="numActividades" name="numActividades" value="<%=consulta.size()%>"/>
                            <table border="0" id="tabActividades" width="100%">
                                <tr class="tblTitulo">
                                    <th scope="col" width="30%">Acci&oacute;n</th>
                                    <th scope="col" width="20%">Actividad</th>
                                    <th scope="col">Responsable</th>
                                    <th scope="col">Peso en la obra</th>
                                    <th scope="col" width="100">Dia inicio</th>
                                    <th scope="col" width="100">Dia fin</th>
                                    <th scope="col" width="100">Dias</th>
                                </tr>
                                <%
                                    for (int j = 0; j < consulta.size(); j++) {
                                        Actividades act = consulta.get(j);
                                %>
                                <tr class="<%=(j % 2 == 0 ? "filagris" : "filaazul")%>" id="fila<%=j%>">
                                        <input type="hidden" name="hidIdAccion<%=j%>" value="<%=act.getId_accion()%>"/>
                                        <input type="hidden" name="hidIdActividad<%=j%>" value="<%=act.getId()%>"/>
                                        <input type="hidden" name="hidResponsable<%=j%>" value="<%=act.getResponsable().split(" ")[0]%>"/>
                                        <input type="hidden" name="hidPeso<%=j%>" value="<%=act.getPeso()%>"/>
                                        <input type="hidden" name="hidFechaInicial<%=j%>" value="<%=act.getFechaInicial()%>"/>
                                        <input type="hidden" name="hidFechafinal<%=j%>" value="<%=act.getFechaFinal()%>"/>
                                    <td align="left">
                                        <%=act.getDescAccion()%>    
                                    </td>
                                    <td align="left">
                                        <%=act.getDescripcion()%>
                                    </td>
                                    <td align="left">
                                        <%=act.getResponsable()%>
                                    </td>
                                    <td align="center">
                                        <%=act.getPeso_obra()%>%
                                    </td>
                                    <td nowrap align="center">
                                        <input type="text" id="txtFechaInicial<%=j%>" name="txtFechaInicial<%=j%>"
                                               onchange="calcDias('txtFechaInicial<%=j%>','txtFechaFinal<%=j%>','txtDif<%=j%>')"
                                               value="<%=(act != null && act.getFechaInicial() != null) ? act.getFechaInicial() : ""%>" size="6"/>
                                        <!--img src="< %=BASEURL%>/js/Calendario/cal.gif" id="imgFechaInicial< %=j%>" />
                                        <script>
                                            Calendar.setup({
                                                inputField : "txtFechaInicial< %=j%>",
                                                trigger    : "imgFechaInicial< %=j%>",
                                                onSelect   : function() { this.hide() }
                                            });
                                        </script-->
                                    </td>
                                    <td nowrap align="center">
                                        <input type="text" id="txtFechaFinal<%=j%>" name="txtFechaFinal<%=j%>" 
                                               onchange="calcDias('txtFechaInicial<%=j%>','txtFechaFinal<%=j%>','txtDif<%=j%>')"
                                               value="<%=(act != null && act.getFechaFinal() != null) ? act.getFechaFinal() : ""%>" size="6"/>
                                        <!--img src="< %=BASEURL%>/js/Calendario/cal.gif" id="imgFechaFinal< %=j%>" />
                                        <script>
                                            Calendar.setup({
                                                inputField : "txtFechaFinal< %=j%>",
                                                trigger    : "imgFechaFinal< %=j%>",
                                                onSelect   : function() { this.hide() }
                                            });
                                        </script-->
                                    </td>
                                    <td nowrap align="center">
                                        <input type="text" id="txtDif<%=j%>" name="txtDif<%=j%>" size="6"
                                               onchange="calcDias('txtFechaInicial<%=j%>','txtFechaFinal<%=j%>','txtDif<%=j%>')" readonly/>
                                        <script type="text/javascript">
                                            calcDias('txtFechaInicial<%=j%>','txtFechaFinal<%=j%>','txtDif<%=j%>');
                                        </script>
                                    </td>
                                </tr>
                                <%}%>
                            </table>
                            <%}%>
                        </td>
                    </tr>
                </table>
                <br/>
                <div align="center">
                    <%if (progCompleta) {%>
                    <img style="cursor:hand" src="<%=BASEURL%>/images/botones/confirmar.gif" align="absmiddle" onmouseover="botonOver(this);" onmouseout="botonOut(this);" onClick="confirmarAjustes()">
                    <img style="cursor:hand" src="<%=BASEURL%>/images/botones/salir.gif" align="absmiddle" onmouseover="botonOver(this);" onmouseout="botonOut(this);" onClick="parent.close();">
                    <%}%>
                    <br/><br/>
                    <%if(mensaje != null) {%>
                    <table border="2" align="center">
                        <tr>
                            <td>
                                <table width="100%"  border="0" align="center"  bordercolor="#f7f5f4" bgcolor="#ffffff">
                                    <tr>
                                        <td width="229" align="center" class="mensajes"><%=mensaje%></td>
                                        <td width="29" background="<%=BASEURL%>/images/cuadronaranja.JPG">&nbsp;&nbsp;</td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                    </table>
                    <%}%>
                </div>
            </form>
        </div>
    </body>
</html>
