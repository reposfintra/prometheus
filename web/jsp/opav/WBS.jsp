<%-- 
    Document   : WBS
    Created on : 24/05/2016, 11:01:32 AM
    Author     : mcastillo
--%>

<%@page contentType="text/html" pageEncoding="utf-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
        <title>WBS</title>
        <link rel="stylesheet" href="./css/jquery/jquery-ui/jquery-ui-1.10.2.custom.css" />
        <script type="text/javascript" src="./js/jquery/jquery.jqGrid/js/jquery.min.js"></script>   
        <script type="text/javascript" src="./js/jquery/jquery-1.10.2.min/jquery-1.9.1.js"></script>   
        <script type="text/javascript" src="./js/jquery/jquery-1.10.2.min/jquery-ui.min.js"></script>        
        <link type="text/css" rel="stylesheet" href="./css/buttons/botonVerde.css"/>
        <link type="text/css" rel="stylesheet" href="./css/contratos.css " />
        <link type="text/css" rel="stylesheet" href="./css/jquery/jquery.jqGrid/ui.jqgrid.css"/>    
        <script type="text/javascript" src="./js/jquery/jquery.jqGrid-4.7.0/js/grid.locale-es.js"></script>
        <script type="text/javascript" src="./js/jquery/jquery.jqGrid-4.7.0/js/jquery.jqGrid.min.js"></script>
        <script type="text/javascript" src="./js/jquery/jquery.jqGrid-4.7.0/js/jquery.tabletojson.js"></script> 

        <script  type="text/javascript" src="./js/jquery/primitives/primitives.min.js"></script>
        <link href="./js/jquery/primitives/primitives.latest.css" media="screen" rel="stylesheet" type="text/css" /> 
        <script src="./js/wbs.js" type="text/javascript"></script>


        <!-------------------------------------Notificaciones----------------------------------------->
        <link type="text/css" rel="stylesheet" href="./css/toastr.min.css"/>
        <script type="text/javascript" src="./js/toastr.min.js"></script>
        <!-------------------------------------/Notificaciones----------------------------------------->

        <!--se necesita un div para que funcione el preload se llama loader-wrapper-->

        <link type="text/css" rel="stylesheet" href="./css/main2.css">

        <!----------------------------------------Pre-Load--------------------------------------------->

        <style>
            .ui-autocomplete {
                z-index: 1001 !important;
            }
            #jqg2 {
                z-index: 1001 !important;
            }
            #basicdiagram > *:focus {
                outline: none;
            }
        </style>

        <%
            String num_solicitud = (request.getParameter("num_solicitud") != null) ? request.getParameter("num_solicitud") : "";
        %>

    </head>
    <body>
        <div id="capaSuperior"  style="position:absolute; width:100%; height:100px; z-index:0; left: 0px; top: 0px;">
            <jsp:include page="/WEB-INF/toptsp.jsp?encabezado=WBS"/>
        </div>
        <div id="capaCentral" style="position:absolute; width:100%; height:100%; z-index:0; left: 0px; top: 120px; ">
            <center>     
                <input type="hidden" id="id_solicitud" name="id_solicitud" value="<%=num_solicitud%>" readOnly> 
                <div id="basicdiagram" style="width: 98%; height: 98%; border-style: hidden; border-width: 1px;" />
            </center>
            <div id="dialogProyecto"  style="display:none;"> 
                <br>
                <table border="0" align="left" cellpadding="1" cellspacing="1" class="labels">
                    <tr>                 
                        <td style="width: 25%"><span>Proyecto <b style="color:red">*</b></span></td>        
                        <td style="width: 75%">
                            <!--<input type="text" name="nombre_proyecto" id="nombre_proyecto" style="width: 270px">-->
                            <textarea rows="3" name="nombre_proyecto" id="nombre_proyecto" style="width:  600px"></textarea>
                        </td>
                    </tr>                      
                </table>     
            </div>
            <div id="dialogArea"  style="display:none;"> 
                <br>
                <table border="0" align="left" cellpadding="1" cellspacing="1" class="labels">
                    <tr>
                    <input type="hidden" id="id_area_proyecto" name="id_area_proyecto" readOnly> 
                    <!--                <input type="hidden" id="idarea" name="idarea" readOnly>   -->
                    <td style="width: 25%"><span>Area <b style="color:red">*</b></span></td>        
                    <td style="width: 75%">
                        <input id="area" type="text" placeholder="Ingrese el nombre del area..." class="mayuscula" maxlength="150"  style="width: 270px">
                    </td>
                    </tr>                      
                </table>     
            </div>
            <div id="dialogDisciplina"  style="display:none;"> 
                <br>
                <table border="0" align="left" cellpadding="1" cellspacing="1" class="labels">                        
                    <tr>
                    <input type="hidden" id="id_disciplina_area" name="id_disciplina_area" readOnly> 
                    <input type="hidden" id="id_area" name="id_area" readOnly>   
                    <td style="width: 25%"><span>Area <b style="color:red">*</b></span></td>        
                    <td style="width: 75%">
                        <input type="text" name="nomarea" id="nomarea" readonly style="width: 270px">
                    </td>
                    </tr>           
                    <tr>
                        <td style="width: 25%"><span>Disciplina <b style="color:red">*</b></span></td>        
                        <td style="width: 75%">
                            <select name="disciplina" class="combo_180px" id="disciplina" style="width: 278px"></select>
                        </td>
                    </tr>                      
                </table>     
            </div>
            <div id="dialogCapitulo"  style="display:none;"> 
                <br>
                <table border="0" align="left" cellpadding="1" cellspacing="1" class="labels">                       
                    <tr style="display:none">
                    <input type="hidden" id="id_capitulo_disciplina" name="id_capitulo_disciplina" readOnly> 
                    <input type="hidden" id="id_disciplina" name="id_disciplina" readOnly>    
                    <td style="width: 25%"><span>Nombre <b style="color:red">*</b></span></td>        
                    <td style="width: 75%">
                        <input type="text" name="nomcapitulo" id="nomcapitulo" style="width: 270px">
                    </td>
                    </tr>           
                    <tr>
                        <td style="width: 25%"><span>Descripción <b style="color:red">*</b></span></td>        
                        <td style="width: 75%">
                            <textarea id ="desccapitulo" name="desccapitulo" rows="4" class="mayuscula" style="width: 330px;resize:none"></textarea>                               
                        </td>
                    </tr>                      
                </table>     
            </div>
            <div id="dialogActividad"  style="display:none;">               
                <table border="0" align="left" cellpadding="1" cellspacing="1" class="labels" style="width:98%">                         
                    <tr>
                        <td style="width: 10%">                           
                            <input type="hidden" id="id_capitulo" name="id_capitulo" readOnly>  
                        </td>   
                        <td style="width: 15%"><span>Capitulo </span></td>        
                        <td style="width: 75%">
                            <input type="text" name="nombre_capitulo" id="nombre_capitulo" readonly style="width: 530px">                             
                        </td>
                    </tr>   
                    <tr>
                        <td style="width: 10%">
                            <input type="hidden" id="id_actividad" name="id_actividad" readOnly>  
                        </td>   
                        <td style="width: 15%"><span>Actividad <b style="color:red">*</b></span></td>        
                        <td style="width: 75%">
                            <input id="actividad"  type="text" class="mayuscula" placeholder="Seleccione la actividad y presione enter para agregar" style="width: 530px"> <img id="mas_actividades" src="/fintra/images/botones/iconos/add.png" 
                                                                                                                                                                                title="Crear nueva actividad"   style="margin-left: 5px; height: 100%; vertical-align: middle;">
                        </td>
                    </tr>                     
                </table>         
                <br><br><br> <br><br><br>
                <center> 
                    <table id="tabla_actividades"></table>
                    <div id="page_tabla_actividades"></div>  
                </center>
            </div>


            <!------------------Jean Paul Zapata------------------------------------------------------->
            <!-----------div para cargar los apus al WBS------------------>
            <div id="div_apus"  style="display: none; width: 800px" > 


                <div id="filtros" class="ui-jqgrid ui-widget ui-widget-content ui-corner-all" style="height:84px;width: 430px;padding: 0px 10px 5px 10px">
                    <center>

                        <table style=" padding: 0.5em 1em 0.5em 1em" style="width: 95%;">

                            <input type="hidden" id="id" name="id">

                            <tr>
                                <td>Grupos APUs</td>
                                <td>
                                    <select id="grupo_apu1" nombre="grupo_apu1" style="width: 264px;" >
                                        <!--option value=''>...</option-->
                                    </select>
                                </td>
                            </tr>
                            <tr>
                                <td><label>Busqueda : </label></td>
                                <td><input type="text" id="filtro_nom_apu" style="width: 90%" /></td>
                                <td> 
                                    <button id="btn_filtro_apu" class="ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only" 
                                            role="button" aria-disabled="false">
                                        <span class="ui-button-text">Buscar</span>
                                    </button> 
                                </td>
                            </tr>
                            <tr>
                                <td></td>
                                <td colspan="2" >
                                    <input type="checkbox" id="apu_proyecto"> <span style="margin-bottom: 1px" >APUS DE PROYECTO  </span> 
                                </td>
                            </tr>
                        </table>
                    </center>
                </div> 
                <img id="img_selectrick" src="./images/opav/Selectrick.png" style="position:absolute; left: 60%; top: 1%; width: 500px ;height: 80px">

                <div style="position:absolute; left: 1; top: 14%; width: 45% ">
                    <center>
                        <table id="tbl_apus" ></table>                
                        <div id="page_apus"></div>
                    </center>
                </div>

                <div style="position:absolute; left: 51%; top: 14%; width: 45% ">
                    <center>
                        <table id="tbl_apus_asociado" ></table>                
                        <div id="page_apus_asociado"></div>
                    </center>
                </div>

                <div id="bt_asociar_apu" title="Asociar APU" style="position:absolute; left: 47.2%; top: 48%; width: 10%;"> 
                    <span aling="center"  class="form-submit-button form-submit-button-simple_green_apple" onClick="AsociarAPUActividad()"> >> </span>
                </div>
                <div id="bt_desasociar_apu" title="Desasociar APU" style="position:absolute; left: 47.2%; top: 62%; width: 10%;"> 
                    <span aling="center"  class="form-submit-button form-submit-button-simple_green_apple" onClick="DesAsociarAPUActividad()"> << </span>
                </div>

            </div>
            <!------------------------------------------------------------------------>
            <div id="dialogAddActividad"  style="display:none;"> 
                <br>
                <table border="0" align="left" cellpadding="1" cellspacing="1" class="labels">   
                    <tr>
                        <td style="width: 25%"><span>Nombre <b style="color:red">*</b></span></td>        
                        <td style="width: 75%">
                            <textarea id ="nombre_actividad" name="nombre_actividad" rows="4" class="mayuscula" style="width: 330px;resize:none"></textarea>                               
                        </td>
                    </tr>                      
                </table>     
            </div>
            <div id="dialogLoading" style="display:none;">
                <p  style="font-size: 12px;text-align:justify;" id="msj2">Texto </p> <br/>
                <center>
                    <img src="./images/cargandoCM.gif"/>
                </center>
            </div>
            <div id="dialogMsj" title="Mensaje" style="display:none;">
                <p style="font-size: 12px;text-align:justify;" id="msj" > Texto </p>
            </div>      
        </div>

        <!---------------------------------------------------->
        <!-------------------crear APU-------------------------------->
        <div id="div_apu"  style="display: none; width: 800px" >   
            <center>
                <div id="filtros" class="ui-jqgrid ui-widget ui-widget-content ui-corner-all" style="height: 93px; width: 60%">
                    </br>
                    <table aling="center" style=" width: 100%" >
                        <tr>
                            <td>Grupo APU:</td>
                            <td>
                                <select id="grupo_apu" nombre="grupo_apu" style="width: 70%;" >
                                    <!--option value=''>...</option-->
                                </select>
                            </td>
                            <td><img id = "add_grupo" src = "/fintra/images/botones/iconos/adds.png"
                                     style = "margin-left: 5px; height: 19px; vertical-align: middle;" 
                                     onclick = "crearGrupoApu();"></td>
                        </tr> 
                        <tr>
                            <td>Unidad Medida:</td>
                            <td>
                                <select id="unmed" nombre="unmed" style="width: 70%;" >
                                    <!--option value=''>...</option-->
                                </select>
                            </td>
                            <td><img id = "add_unidadmed" src = "/fintra/images/botones/iconos/adds.png"
                                     style = "margin-left: 5px; height: 19px; vertical-align: middle;" 
                                     onclick = "crearUnidadMedida();">
                            </td>
                        </tr>
                        <tr>
                            <td>Nombre APU:<span style="color: red; ">*</span></td>
                            <td style="width: 80%">
                                <input type="text" id="nomapu" class="mayuscula"  name="nomapu" style=" width: 90%;text-align: justify">
                                <input type="hidden" id="idapu" name="idapu">
                                <input type="hidden" id="cantidadapu" name="cantidadapu">
                                <input type="hidden" id="tipo_apu" name="tipo_apu">
                            </td>
                        </tr>
                    </table>
                </div>  
                </br> 

                <!---------Grid que contiene los insumos ----------->
                <div style="margin-top: 0px">
                    <table id="tbl_insumos" ></table>                
                    <div id="page_insumos"></div>
                </div>
                <!-------------------->

                <div id="div_filtro_insumos"  style="display: none; width: 800px" >
                    <div id="filtros" class="ui-jqgrid ui-widget ui-widget-content ui-corner-all" style="height: 130px;padding: 0px 10px 5px 10px">
                        <center>

                            <table style=" padding: 0.5em 1em 0.5em 1em" style="width: 95%;">

                                <input type="hidden" id="idregistro" name="idregistro">

                                <tr>
                                    <td>Categoria :</td>
                                    <td>
                                        <select id="categoria" nombre="categoria" onchange="cargarCombo('sub', [this.value])" style="width: 364px;" >
                                            <option value='0'>...</option>
                                        </select>
                                    </td>
                                </tr>
                                <tr>
                                    <td>SubCategoria :</td>
                                    <td>
                                        <select id="sub"  style="width: 364px;" >
                                            <option value='0'>...</option>
                                        </select>

                                        <input type="hidden" name="nomsub" id="nomsub">
                                        <input type="hidden" name="_tipo_insumo" id="_tipo_insumo">
                                    </td>
                                </tr>
                                <tr>
                                    <td><label>Buqueda : </label></td>
                                    <td>
                                        <input type="text" id="filtro_insumo" style="width: 90%" />
                                        <img id = "btn_filtro_insumo" src = "/fintra/images/opav/enter.png"
                                             style = "margin-left: 5px; height: 19px; vertical-align: middle;" 
                                             onclick = "" title="Enter">
                                    </td>

                                </tr>
                            </table>
                            <hr>
                            <div id ='botones'>
                                <button id="buscar" class="ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only" 
                                        role="button" aria-disabled="false">
                                    <span class="ui-button-text">Buscar</span>
                                </button> 
                            </div>
                        </center>
                    </div>  
                    </br>  

                    <!---------Grid que contiene filtro de insumos ----------->
                    <div style="margin-top: 0px">
                        <center>
                            <table id="tbl_filtro_insumos" ></table>                
                            <div id="page_filtro_insumos"></div>
                        </center>
                    </div>
                    <!-------------------->

                    <!--------div materiales------------>
                    <div style="margin-top: 0px">
                        <center>
                            <table id="tbl_materiales" ></table>                
                            <div id="page_materiales"></div>
                        </center>
                    </div>

                </div>

            </center>
        </div>

        <!-------------------crear grupo APU-------------------------------->
        <div id="div_grupo_apu"  style="display: none; width: 800px" >                       
            <div id="filtros" class="ui-jqgrid ui-widget ui-widget-content ui-corner-all" style="height: 110px;">
                </br>
                <table aling="center" style=" width: 100%" >
                    <tr>
                        <td style="width: 10%"><span>Nombre Grupo</span></td>                          
                        <td style="width: 50%"><input type="text" id="nomgrupo" class="mayuscula" name="nomgrupo" style=" width: 248px">
                        </td>  
                    </tr> 
                    <tr>
                        <td style="width: 10%"><span>Descripcion Grupo:</span></td>   
                        <td style="width: 90%" colspan="4"><textarea id ="descgrupo" name="descgrupo" style="width:535px;resize:none" rows="2" ></textarea></td>                        
                    </tr>
                </table>
            </div>  
            </br> 
        </div>

        <!-------------------crear Unidad Medida-------------------------------->
        <div id="div_unidad_medida"  style="display: none; width: 800px" >                       
            <div id="filtros" class="ui-jqgrid ui-widget ui-widget-content ui-corner-all" style="height: 53px;">
                </br>
                <table aling="center" style=" width: 100%" >
                    <tr>
                        <td style="width: 20%"><span>Unidad de Medida</span></td>                          
                        <td style="width: 50%"><input type="text" id="nomunidad" class="mayuscula" name="nomunidad" style=" width: 280px">
                        </td>  
                    </tr> 
                </table>
            </div>  
            </br> 
        </div>
        <!---------------------------------------------------->

        <!---------------------------------------------------->
        <!-------------------crear AREA PROYECTO-------------------------------->
        <div id="div_area_proyecto"  style="display: none; width: 800px" >
            <center>
                <br>
                <table id="tabla_areas"></table>
                <div id="page_tabla_areas"></div>    
            </center>
        </div>
        <div id="div_area"  style="width: 530px;display: none;" >  
            <table aling="center" style=" width: 100%" >                   
                <tr>
                    <td>
                        <input type="text" id="idArea"  style="width: 50px"hidden >
                    </td>
                </tr>      
                <tr>
                    <td colspan="2" >
                        <label> Nombre<span style="color:red;">*</span></label> 
                    </td>
                    <td colspan="3">
                        <input type="text" id="nombre" class="mayuscula" style="width: 298px" maxlength="30"  />
                    </td>
                </tr>  
            </table>
        </div>

        <div id="dialogMsgMeta" title="Mensaje" style="display:none; visibility: hidden">
            <p style="font-size: 12.5px;text-align:justify;" id="msj1" > Texto </p>
        </div>  
        <div id="loader-wrapper">
            <div id="loader"></div>
            <img id="logowast" src="/fintra/images/Selectrik.png" style="z-index: 10002 !important">
            <div class="loader-section section-left">	
            </div>
            <div class="loader-section section-right"></div>
        </div>

    </body>
</html>
