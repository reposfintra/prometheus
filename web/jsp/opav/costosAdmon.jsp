<%-- 
    Document   : costosAdmon
    Created on : 12/08/2016, 02:26:56 PM
    Author     : mcastillo
--%>

<%@page contentType="text/html" pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
        <title>Costos Administración</title>

        <link type="text/css" rel="stylesheet" href="./css/jquery/jquery-ui/jquery-ui.css" />
        <link type="text/css" rel="stylesheet" href="./css/buttons/botonVerde.css"/>
        <link href="./css/popup.css" rel="stylesheet" type="text/css">

        <script type="text/javascript" src="./js/jquery/jquery.jqGrid/js/jquery.min.js"></script> 
        <script type="text/javascript" src="./js/jquery-ui-1.8.5.custom.min.js"></script>
        <script src="./js/costosAdmon.js" type="text/javascript"></script>

        <!--jqgrid--> 
        <script type="text/javascript" src="./js/jquery/jquery.jqGrid/js/grid.locale-es.js"></script>
        <script type="text/javascript" src="./js/jquery/jquery.jqGrid/js/jquery.jqGrid.min.js"></script>
        <script type="text/javascript" src="./js/jquery/jquery.jqGrid-4.7.0/plugins/jquery.contextmenu.js"></script>

        <%
            String num_solicitud = (request.getParameter("num_solicitud") != null) ? request.getParameter("num_solicitud") : "";
        %>

    </head>
    <body>
        <div id="capaSuperior"  style="position:absolute; width:100%; height:100px; z-index:0; left: 0px; top: 0px;">
            <jsp:include page="/WEB-INF/toptsp.jsp?encabezado=Costos Administración"/>
        </div>
        <div id="capaCentral" style="position:absolute; width:100%; height:100%; z-index:0; left: 0px; top: 100px; ">
            <input type="hidden" id="id_solicitud" name="id_solicitud" value="<%=num_solicitud%>" readOnly> 
            <center>
                
            <table>
                <tr>
                    <td>
                <center>
                    <div id="div_costos_admon"></div>  

                </center>
                </td>
                </tr>
                <tr>

                    <td style="text-align:right">
                <center>
                    <div style="width: 850px; margin-top: -16px">                    
                    <span style="margin-left:690px">Total</span> <input type="text" id="total_costos_admon" readonly value="0" style="color:#000;width:112px;text-align:right;margin-top:10px">
                    </div>
                </center> 
                </td>
                </tr>
                <tr>
                    <td>
                <center>
                    <div id="botones_footer" style="text-align:center;"> 
                        <span aling="center" id="btn_guardar"  class="form-submit-button form-submit-button-simple_green_apple"> Guardar </span>&nbsp;
                        <span aling="center" id="btn_salir" class="form-submit-button form-submit-button-simple_green_apple"> Salir </span>
                    </div>
                </center>
                </td>
                </tr>
            </table> 
                </center>
                 
            <!--            <center>
                            <br>
                            <table id="tabla_costos"></table>
                            <div id="page_tabla_costos"></div>    
                        </center>-->
        </div>
        <div id="dialogLoading" style="display:none;">
            <p  style="font-size: 12px;text-align:justify;" id="msj2">Texto </p> <br/>
            <center>
                <img src="/fintra/images/cargandoCM.gif"/>
            </center>
        </div>
        <div id="dialogMsj" title="Mensaje" style="display:none;">
            <p style="font-size: 12px;text-align:justify;" id="msj" > Texto </p>
        </div> 
        <div class="contextMenu" id="myMenu" style="display:none;">
            <ul  id="lstOpciones" >
                <li id="eliminar">
                    <span style="float:left"></span>
                    <span style="font-size:11px; font-family:Verdana">Eliminar fila</span>
                </li> 
            </ul>
        </div>
    </body>
</html>
