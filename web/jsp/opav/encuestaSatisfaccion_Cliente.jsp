<%-- 
    Document   : encuestaSatisfaccion_Cliente
    Created on : 10/04/2015, 11:39:17 AM
    Author     : egonzalez
--%>

<%@page import="com.tsp.operation.model.beans.Usuario"%>
<%@page import="com.tsp.opav.model.services.NegociosApplusService"%>
<%@page contentType="text/html" pageEncoding="ISO-8859-1"%>
<%@page session   ="true"%>
<%@page errorPage ="/error/ErrorPage.jsp"%>
<%@include file   ="/WEB-INF/InitModel.jsp"%>

<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
        <title>Formato Encuesta Satistaccion del Cliente</title>
        <link href="<%=BASEURL%>/css/estilostsp.css"    rel="stylesheet" type="text/css">
        <link href="<%=BASEURL%>/css/mac_os_x.css"      rel="stylesheet" type="text/css">
        <link type="text/css" rel="stylesheet" href="/fintra/css/jquery/jquery-ui/jquery-ui.css">
        <script type="text/javascript" src="<%=BASEURL%>/js/jquery/jquery-ui/jquery.min.js"></script>
        <%
            String solicitud = request.getParameter("solicitud");
            Usuario usuario = (Usuario) session.getAttribute("Usuario");
            NegociosApplusService nas = new NegociosApplusService(usuario.getBd());
            String datos = nas.buscarEncuesta(solicitud);
        %>
        <script>
            var json = <%=datos%>;
            function enviar() {
                //alert(json.creation_user);

                $.ajax({
                    url: '/fintra/controlleropav?estado=Negocios&accion=Applus&opcion=registrarEncuestaObra',
                    datatype: 'text',
                    type: 'post',
                    data: {
                        informacion: JSON.stringify({
                            pregunta1: $("input:radio[name='preg1']:checked").val(),
                            pregunta2: $("input:radio[name='preg2']:checked").val(),
                            pregunta3: $("input:radio[name='preg3']:checked").val(),
                            pregunta4: $("input:radio[name='preg4']:checked").val(),
                            pregunta5: $("input:radio[name='preg5']:checked").val(),
                            pregunta6: $("input:radio[name='preg6']:checked").val(),
                            pregunta7: $("input:radio[name='preg7']:checked").val(),
                            pregunta8: $("input:radio[name='preg8']:checked").val(),
                            pregunta9: $("input:radio[name='preg9']:checked").val(),
                            pregunta10: $("input:radio[name='preg10']:checked").val(),
                            pregunta11: $("input:radio[name='preg11']:checked").val(),
                            pregunta12: $("input:radio[name='preg12']:checked").val(),
                            pregunta13: $("input:radio[name='preg13']:checked").val(),
                            pregunta14: $("select[name='preg14'] option:selected").val(),
                            pregunta15: $("select[name='preg15'] option:selected").val(),
                            pregunta16: $("select[name='preg16'] option:selected").val(),
                            comentario: $("#comentario").val(),
                            solicitud: '<%=solicitud%>'
                        })
                    },
                    async: true,
                    success: function (texto) {
                        alert(texto);
                    },
                    error: function (e) {
                        alert('error enviando la encuesta: ' + e);
                    }
                });
            }
        </script>
    </head>

    <body>
        <div>
            <div class="ui-jqgrid ui-widget ui-widget-content ui-corner-all">
                <table style=" width: 400px ; margin-top: 2%;" align="center">
                    <tr>
                        <td>Cliente: </td>
                        <td colspan="3"><label id="cliente"></label></td>
                    </tr>
                    <tr>
                        <td>Fecha: </td>
                        <td><label id="fecha"></label></td>
                        <td>No. MS: </td>
                        <td><label id="ms"></label></td>
                    </tr>
                    <tr><td colspan="4" align="center">
                            <!--img src="/fintra/images/botones/guardar.gif" id="imgguardar" alt="Guardar" onClick="enviar()" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" style="cursor:pointer "-->
                            <button id="imgguardar" class="ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only" role="button" aria-disabled="false" onclick="enviar();">
                                <span class="ui-button-text">Guardar</span>
                            </button>
                        </td></tr>
                </table>
                <table width="1000" align="center">
                    <tr>&nbsp;<br>
                    </tr>
                    <tr class="Titulos">
                        <td align="center" style="width:1%;"><strong>&nbsp;Items&nbsp;</strong> </td>
                        <td align="center" style="width:75%;"><strong>&nbsp;Criterios a evaluar&nbsp;</strong> </td>
                        <td align="center" style="width:5%;">
                            <strong>
                                <table style="width: 100%; text-align: center;"><tr>&nbsp;Grado de satisfaccion&nbsp;</tr>
                                    <tr>
                                        <td>1</td><td>2</td><td>3</td><td>4</td><td>5</td>
                                    </tr>
                                </table>
                            </strong> 
                        </td>
                    </tr>

                    <tr class="fila">
                        <td align="center"><strong>&nbsp;1&nbsp;</strong> </td>
                        <td align="justify"><strong>Facilidad para comunicarse con la empresa</strong> </td>
                        <td align="center">
                            <strong>
                                <table  style="width: 100%;"><tr>
                                        <td><input type="radio" value="1" name="preg1" checked="true"/></td>
                                        <td><input type="radio" value="2" name="preg1"/></td>
                                        <td><input type="radio" value="3" name="preg1"/></td>
                                        <td><input type="radio" value="4" name="preg1"/></td>
                                        <td><input type="radio" value="5" name="preg1"/></td>
                                    </tr></table>
                            </strong> </td>
                    </tr>

                    <tr class="fila">
                        <td align="center"><strong>&nbsp;2&nbsp;</strong> </td>
                        <td align="justify"><strong>Cumplimiento de los tiempos de entrega de la oferta</strong> </td>
                        <td align="center">
                            <strong>
                                <table  style="width: 100%"><tr>
                                        <td><input type="radio" value="1" name="preg2" checked="true"/></td>
                                        <td><input type="radio" value="2" name="preg2"/></td>
                                        <td><input type="radio" value="3" name="preg2"/></td>
                                        <td><input type="radio" value="4" name="preg2"/></td>
                                        <td><input type="radio" value="5" name="preg2"/></td>
                                    </tr></table>
                            </strong> </td>
                    </tr>

                    <tr class="fila">
                        <td align="center"><strong>&nbsp;3&nbsp;</strong> </td>
                        <td align="justify"><strong>Cumplimiento de la fecha de la Visita T�cnica</strong> </td>
                        <td align="center">
                            <strong>
                                <table  style="width: 100%"><tr>
                                        <td><input type="radio" value="1" name="preg3" checked="true"/></td>
                                        <td><input type="radio" value="2" name="preg3"/></td>
                                        <td><input type="radio" value="3" name="preg3"/></td>
                                        <td><input type="radio" value="4" name="preg3"/></td>
                                        <td><input type="radio" value="5" name="preg3"/></td>
                                    </tr></table>
                            </strong> </td>
                    </tr>

                    <tr class="fila">
                        <td align="center"><strong>&nbsp;4&nbsp;</strong> </td>
                        <td align="justify"><strong>Cumplimiento de los tiempos de ejecuci�n de los trabajos</strong> </td>
                        <td align="center">
                            <strong>
                                <table  style="width: 100%"><tr>
                                        <td><input type="radio" value="1" name="preg4" checked="true"/></td>
                                        <td><input type="radio" value="2" name="preg4"/></td>
                                        <td><input type="radio" value="3" name="preg4"/></td>
                                        <td><input type="radio" value="4" name="preg4"/></td>
                                        <td><input type="radio" value="5" name="preg4"/></td>
                                    </tr></table>
                            </strong> </td>
                    </tr>

                    <tr class="fila">
                        <td align="center"><strong>&nbsp;5&nbsp;</strong> </td>
                        <td align="justify"><strong>Calidad en la ejecuci�n de los trabajos</strong> </td>
                        <td align="center">
                            <strong>
                                <table  style="width: 100%"><tr>
                                        <td><input type="radio" value="1" name="preg5" checked="true"/></td>
                                        <td><input type="radio" value="2" name="preg5"/></td>
                                        <td><input type="radio" value="3" name="preg5"/></td>
                                        <td><input type="radio" value="4" name="preg5"/></td>
                                        <td><input type="radio" value="5" name="preg5"/></td>
                                    </tr></table>
                            </strong> </td>
                    </tr>

                    <tr class="fila">
                        <td align="center"><strong>&nbsp;6&nbsp;</strong> </td>
                        <td align="justify"><strong>Uso de Elementos de Protecci�n Personal (EPP)</strong> </td>
                        <td align="center">
                            <strong>
                                <table  style="width: 100%"><tr>
                                        <td><input type="radio" value="1" name="preg6" checked="true"/></td>
                                        <td><input type="radio" value="2" name="preg6"/></td>
                                        <td><input type="radio" value="3" name="preg6"/></td>
                                        <td><input type="radio" value="4" name="preg6"/></td>
                                        <td><input type="radio" value="5" name="preg6"/></td>
                                    </tr></table>
                            </strong> </td>
                    </tr>

                    <tr class="fila">
                        <td align="center"><strong>&nbsp;7&nbsp;</strong> </td>
                        <td align="justify"><strong>Informaci�n completa y veraz sobre el servicio prestado</strong> </td>
                        <td align="center">
                            <strong>
                                <table  style="width: 100%"><tr>
                                        <td><input type="radio" value="1" name="preg7" checked="true"/></td>
                                        <td><input type="radio" value="2" name="preg7"/></td>
                                        <td><input type="radio" value="3" name="preg7"/></td>
                                        <td><input type="radio" value="4" name="preg7"/></td>
                                        <td><input type="radio" value="5" name="preg7"/></td>
                                    </tr></table>
                            </strong> </td>
                    </tr>

                    <tr class="fila">
                        <td align="center"><strong>&nbsp;8&nbsp;</strong> </td>
                        <td align="justify"><strong>Profesionalismo del personal que lo atendi�</strong> </td>
                        <td align="center">
                            <strong>
                                <table  style="width: 100%"><tr>
                                        <td><input type="radio" value="1" name="preg8" checked="true"/></td>
                                        <td><input type="radio" value="2" name="preg8"/></td>
                                        <td><input type="radio" value="3" name="preg8"/></td>
                                        <td><input type="radio" value="4" name="preg8"/></td>
                                        <td><input type="radio" value="5" name="preg8"/></td>
                                    </tr></table>
                            </strong> </td>
                    </tr>

                    <tr class="fila">
                        <td align="center"><strong>&nbsp;9&nbsp;</strong> </td>
                        <td align="justify"><strong>Amabilidad en la atenci�n</strong> </td>
                        <td align="center">
                            <strong>
                                <table  style="width: 100%"><tr>
                                        <td><input type="radio" value="1" name="preg9" checked="true"/></td>
                                        <td><input type="radio" value="2" name="preg9"/></td>
                                        <td><input type="radio" value="3" name="preg9"/></td>
                                        <td><input type="radio" value="4" name="preg9"/></td>
                                        <td><input type="radio" value="5" name="preg9"/></td>
                                    </tr></table>
                            </strong> </td>
                    </tr>

                    <tr class="fila">
                        <td align="center"><strong>&nbsp;10&nbsp;</strong> </td>
                        <td align="justify"><strong>Eficacia en la recepci�n y soluci�n de dudas o inquietudes</strong> </td>
                        <td align="center">
                            <strong>
                                <table  style="width: 100%"><tr>
                                        <td><input type="radio" value="1" name="preg10" checked="true"/></td>
                                        <td><input type="radio" value="2" name="preg10"/></td>
                                        <td><input type="radio" value="3" name="preg10"/></td>
                                        <td><input type="radio" value="4" name="preg10"/></td>
                                        <td><input type="radio" value="5" name="preg10"/></td>
                                    </tr></table>
                            </strong> </td>
                    </tr>

                    <tr class="fila">
                        <td align="center"><strong>&nbsp;11&nbsp;</strong> </td>
                        <td align="justify"><strong>Gesti�n de documentos enviados y recibidos</strong> </td>
                        <td align="center">
                            <strong>
                                <table  style="width: 100%"><tr>
                                        <td><input type="radio" value="1" name="preg11" checked="true"/></td>
                                        <td><input type="radio" value="2" name="preg11"/></td>
                                        <td><input type="radio" value="3" name="preg11"/></td>
                                        <td><input type="radio" value="4" name="preg11"/></td>
                                        <td><input type="radio" value="5" name="preg11"/></td>
                                    </tr></table>
                            </strong> </td>
                    </tr>

                    <tr class="fila">
                        <td align="center"><strong>&nbsp;12&nbsp;</strong> </td>
                        <td align="justify"><strong>Presentaci�n del personal que efect�o el trabajo</strong> </td>
                        <td align="center">
                            <strong>
                                <table  style="width: 100%"><tr>
                                        <td><input type="radio" value="1" name="preg12" checked="true"/></td>
                                        <td><input type="radio" value="2" name="preg12"/></td>
                                        <td><input type="radio" value="3" name="preg12"/></td>
                                        <td><input type="radio" value="4" name="preg12"/></td>
                                        <td><input type="radio" value="5" name="preg12"/></td>
                                    </tr></table>
                            </strong> </td>
                    </tr>

                    <tr class="fila">
                        <td align="center"><strong>&nbsp;13&nbsp;</strong> </td>
                        <td align="justify"><strong>Atenci�n de las garant�as</strong> </td>
                        <td align="center">
                            <strong>
                                <table  style="width: 100%"><tr>
                                        <td><input type="radio" value="1" name="preg13" checked="true"/></td>
                                        <td><input type="radio" value="2" name="preg13"/></td>
                                        <td><input type="radio" value="3" name="preg13"/></td>
                                        <td><input type="radio" value="4" name="preg13"/></td>
                                        <td><input type="radio" value="5" name="preg13"/></td>
                                    </tr></table>
                            </strong> </td>
                    </tr>

                    <tr class="Titulos"><td align="center" colspan="3"></td></tr>
                    
                    <tr class="fila">
                        <td align="center"><strong>&nbsp;14&nbsp;</strong> </td>
                        <td align="justify"><strong>Presentaci�n por parte del Contratista del soporte de pago de la planilla de Seguridad social, de trabajadores que ejecutan el trabajo.</strong> </td>
                        <td align="center">
                            <select name="preg14">
                                <option value="N">NO</option>
                                <option value="S">SI</option>
                            </select>  </td>
                    </tr>

                    <tr class="fila">
                        <td align="center"><strong>&nbsp;15&nbsp;</strong> </td>
                        <td align="justify"><strong>�Cu�l es su grado de satisfacci�n general con respecto al servicio prestado?</strong> </td>
                        <td align="center">
                            <select name="preg15">
                                <option value="1">Completamente insatisfecho</option>
                                <option value="2">Insatisfecho</option>
                                <option value="3">Satisfecho</option>
                                <option value="4">Completamente satisfecho</option>
                            </select>  </td>
                    </tr>

                    <tr class="fila">
                        <td align="center"><strong>&nbsp;16&nbsp;</strong> </td>
                        <td align="justify"><strong>�Si necesitara nuevamente uno o m�s de los servicios, volver�a a utilizarlos o nos recomendar�a?</strong> </td>
                        <td align="center">
                            <select name="preg16">
                                <option value="N">NO</option>
                                <option value="S">SI</option>
                            </select>  </td>
                    </tr>

                    <tr class="Titulos"><td align="center" colspan="3"></td></tr>

                    <tr class="fila"><td align="center" colspan="3">&nbsp;Comentario o recomendaci�n adicional.<BR><textarea id="comentario" cols="3" style="width: 95%;"></textarea></td></tr>

                    <tr class="Titulos"><td align="center" colspan="3"></td></tr>
                </table>
                <br>

                <script>
                    document.getElementById('cliente').innerHTML = json.nombcliente;
                    document.getElementById('ms').innerHTML = json.ms;
                    document.getElementById('fecha').innerHTML = json.fecha;

                    if (json.creation_user != '') {
                        document.getElementById('imgguardar').style.visibility = 'hidden';
                        alert('Esta solicitud ya tiene una encuesta creada por ' + json.creation_user);
                        $("input[name=preg1]").val([json.pregunta1]);
                        $("input[name=preg2]").val([json.pregunta2]);
                        $("input[name=preg3]").val([json.pregunta3]);
                        $("input[name=preg4]").val([json.pregunta4]);
                        $("input[name=preg5]").val([json.pregunta5]);
                        $("input[name=preg6]").val([json.pregunta6]);
                        $("input[name=preg7]").val([json.pregunta7]);
                        $("input[name=preg8]").val([json.pregunta8]);
                        $("input[name=preg9]").val([json.pregunta9]);
                        $("input[name=preg10]").val([json.pregunta10]);
                        $("input[name=preg11]").val([json.pregunta11]);
                        $("input[name=preg12]").val([json.pregunta12]);
                        $("input[name=preg13]").val([json.pregunta13]);
                        $("select[name=preg14]").val([json.pregunta14]);
                        $("select[name=preg15]").val([json.pregunta15]);
                        $("select[name=preg16]").val([json.pregunta16]);
                        $("#comentario").val(json.comentario);
                    }
                </script>
            </div>
        </div>
    </body>
</html>
