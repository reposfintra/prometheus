<%-- 
    Document   : modulobodegas
    Created on : 20/06/2017, 10:25:58 AM
    Author     : Ing.William A. Siado Torres
--%>

<%@page contentType="text/html" pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html lang="ES">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
        <title>Bodegas</title>


        <link type="text/css" rel="stylesheet" href="./css/jquery/jquery-ui/jquery-ui.css"/>     
        <script type="text/javascript" src="./js/jquery/jquery-ui/jquery.min.js"></script>
        <script type="text/javascript" src="./js/jquery/jquery-ui/jquery.ui.min.js"></script> 


        <!-------------------------------------JQGRID----------------------------------------->
        <link type="text/css" rel="stylesheet" href="./css/jquery/jquery.jqGrid/ui.jqgrid.css"/>
        <script type="text/javascript" src="./js/jquery/jquery.jqGrid-4.7.0/js/grid.locale-es.js"></script>
        <script type="text/javascript" src="./js/jquery/jquery.jqGrid-4.7.0/js/jquery.jqGrid.min.js"></script>
        <script type="text/javascript" src="./js/jquery/jquery.jqGrid-4.7.0/js/jquery.tabletojson.js"></script> 
        <!-------------------------------------/JQGRID----------------------------------------->

        <!-------------------------------------Notificaciones----------------------------------------->
        <link type="text/css" rel="stylesheet" href="./css/toastr.min.css"/>
        <script type="text/javascript" src="./js/toastr.min.js"></script>
        <!-------------------------------------/Notificaciones----------------------------------------->

        <!----------------------------------------Pre-Load--------------------------------------------->
        <!--se necesita un div para que funcione el preload se llama loader-wrapper-->

        <link type="text/css" rel="stylesheet" href="./css/main2.css">

        <!----------------------------------------Pre-Load--------------------------------------------->

        <link href="./css/bootstrap/css/bootsrtap-3.3.7.min.css" rel="stylesheet" type="text/css"/>
        <script src="./js/OportunidadNegocio/moduloBodegasTerc.js" type="text/javascript"></script>
        <link href="./css/OportunidadNegocio.css" rel="stylesheet" type="text/css"/>

    </head>

    <body>

        <jsp:include page="/WEB-INF/toptsp.jsp?encabezado=Modulo Bodega"/>

        <input type="hidden" id="id_cliente" name="id_cliente">

        <div class="container-fluid">

            <!-- **********************************************************************************************************************************************************
                                                                                TABLAS
            *********************************************************************************************************************************************************** -->
            <div style="display: table ;margin: 35px auto;">
                <table id="tabla_contratista"></table>
                <div id="pager_tabla_contratista"></div>
            </div>

            
            <div id="modal_tabla_bodega" title="Bodegas" style="display: table ;margin: 35px auto;">
                <table id="tabla_bodega"></table>
                <div id="pager_tabla_bodega"></div>
            </div>
            <input type="hidden" id="codigo_contratista" class="form-control input-sm" style="width: 100%;" onkeyup="setDireccion(1)"/>
            

            <!-- **********************************************************************************************************************************************************
                                                                                FIN TABLAS
            *********************************************************************************************************************************************************** -->

            <!-- **********************************************************************************************************************************************************
                                                                                Formulario Bodega
            *********************************************************************************************************************************************************** -->



            <div id="div_tabla_bodegas" style="display: none">

                <div class="container-fluid">
                    <table id="tabla_bodegas"></table>
                    <div id="pager_tabla_bodegas"></div>
                </div>

                <div class="container-fluid">

                    <form id="form_bodega" name="bodega" method="POST" class="form-horizontal">

                        <input type="hidden" id="id_solicitud" name="id_solicitud">
                        <input type="hidden" id="id_bodega" name="id_bodega">


                        <h4>Formulario de Bodegas</h4> 
                        <hr>
                        <div class="form-group">
                            <label class="col-md-3 control-label">Departamento:</label>
                            <div class="col-md-9">
                                <select id="dep_dir" name="dep_dir" class="form-control input-sm"></select>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-md-3 control-label">Ciudad:</label>
                            <div class="col-md-9">
                                <select id="ciu_dir" name="ciu_dir" class="form-control input-sm"></select>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-md-3 control-label">Via Principal:</label>
                            <div class="col-md-5">
                                <select id="via_princip_dir" onchange="setDireccion(2)" class="form-control input-sm"></select>
                            </div>
                            <div class="col-md-4">
                                <input type="text" id="nom_princip_dir" class="form-control input-sm" style="width: 100%;" onkeyup="setDireccion(1)"/>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-md-3 control-label">Numero:</label>
                            <div class="col-md-5">
                                <select id="via_genera_dir" class="form-control input-sm" onkeyup="setDireccion(1)"  ></select>
                            </div>


                            <div class="col-md-2">
                                <input type="text" id="nom_genera_dir" class="form-control input-sm"style="width: 100%;" onkeyup="setDireccion(1)"/>
                            </div>

                            <div class="col-md-2">
                                <input type="text" id="placa_dir" class="form-control input-sm" style="width: 100%;" onkeyup="setDireccion(1)"/>
                            </div>


                        </div>

                        <div class="form-group">
                            <label class="col-md-3 control-label">Complemento:</label>
                            <div class="col-md-8">
                                <input type="text" id="cmpl_dir" class="form-control input-sm" onkeyup="setDireccion(1)" onchange="setDireccion(1)"/>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-md-3 control-label">Direccion:</label>
                            <div class="col-md-9">
                                <input type="text" id="dir_resul" class="form-control input-sm" style="width: 100%;" readOnly/>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-md-3 control-label">Descripcion:</label>
                            <div class="col-md-9">
                                <textarea rows ="3" id="desc_bodega_terc" name="descripcion" class="form-control input-sm" placeholder="Descripcion de bodega." maxlength="100"></textarea>
                            </div>
                        </div>
                        
                        <div class="form-group">
                            <label class="col-md-3 control-label">Nombre de contacto: </label>
                            <div class="col-md-9">
                                <input type="text" id="nombre_contacto" class="form-control input-sm" style="width: 100%;"/>
                            </div>
                        </div>       

                        <div class="form-group">
                            <label class="col-md-3 control-label">Cargo de contacto: </label>
                            <div class="col-md-9">
                                <input type="text" id="cargo_contacto" class="form-control input-sm" style="width: 100%;"/>
                            </div>
                        </div>                
                        
                        <div class="form-group">
                            <label class="col-md-3 control-label">Telefono 1: </label>
                            <div class="col-md-9">
                                <input type="text" id="telefono1_contacto" class="form-control input-sm" style="width: 100%;"/>
                            </div>
                        </div>                
                        
                        <div class="form-group">
                            <label class="col-md-3 control-label">Telefono 2: </label>
                            <div class="col-md-9">
                                <input type="text" id="telefono2_contacto" class="form-control input-sm" style="width: 100%;"/>
                            </div>
                        </div>                
                        
                    </form>
                </div>

            </div>




            <!-- **********************************************************************************************************************************************************
                                                                                FIN Formulario Bodega
            *********************************************************************************************************************************************************** -->


            <div id="loader-wrapper">
                <div id="loader"></div>
                <img id="logowast" src="/fintra/images/Selectrik.png" style="z-index: 10002 !important">
                <div class="loader-section section-left">	
                </div>
                <div class="loader-section section-right"></div>
            </div>

        </div>
    </body>
</html>
