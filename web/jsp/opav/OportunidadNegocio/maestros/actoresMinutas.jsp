<%-- 
    Document   : actoresMinutas
    Created on : 20/06/2016, 05:54:52 PM
    Author     : mcastillo
--%>

<%@page contentType="text/html" pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
        <title>Actores</title>
        
        <link type="text/css" rel="stylesheet" href="./css/jquery/jquery-ui/jquery-ui.css" />
        <link href="./css/popup.css" rel="stylesheet" type="text/css">

        <script type="text/javascript" src="./js/jquery/jquery.jqGrid/js/jquery.min.js"></script> 
        <script type="text/javascript" src="./js/jquery-ui-1.8.5.custom.min.js"></script>
        <script src="./js/actoresMinutas.js" type="text/javascript"></script>

        <!--css logica de negocio-->
        <link type="text/css" rel="stylesheet" href="./css/contratos.css " />
         
        <!--jqgrid--> 
        <script type="text/javascript" src="./js/jquery/jquery.jqGrid/js/grid.locale-es.js"></script>
        <script type="text/javascript" src="./js/jquery/jquery.jqGrid/js/jquery.jqGrid.min.js"></script>
    </head>
    <body>
        <div id="capaSuperior"  style="position:absolute; width:100%; height:100px; z-index:0; left: 0px; top: 0px;">
            <jsp:include page="/WEB-INF/toptsp.jsp?encabezado=Actores"/>
        </div>
        <div id="capaCentral" style="position:absolute; width:100%; height:100%; z-index:0; left: 0px; top: 100px; ">
            <center>
                <br>
                <table id="tabla_actores"></table>
                <div id="page_tabla_actores"></div>    
            </center>
          <div id="div_actores"  style="display: none; width: 850px" >              
            <div id="direccion_dialogo" style="display:none;left: 0; position: absolute; top: 0;padding: 1em 1.2em; margin:0px auto; margin-top:10px; padding:10px; width:370px; min-height:140px; border-radius:4px; background-color:#FFFFFF; box-shadow: 0 2px 5px #666666;"> 
                <div>
                    <table style="width: 100%;">

                        <tr>
                            <td class="titulo_ventana" id="drag_direcciones" colspan="3">
                                <div style="float:center">FORMATO DIRECCIONES<input type="hidden" name="id_usuario" id="id_usuario" value="coco"/></div> 
                            </td>
                        </tr>                    
                        <tr>  
                            <td><span>Departamento:</span></td>                     
                            <td colspan="2"> <select id="dep_dir" name="dep_dir" style="width:20em;"></select>
                            </td>
                        </tr>
                        <tr>
                            <td><span>Ciudad:</span></td>     
                            <td colspan="2">                            
                                <div id="d_ciu_dir">
                                    <select id="ciu_dir" name="ciu_dir" style="width:20em;"></select>
                                </div>
                            </td>
                        </tr>  
                        <tr>
                            <td>Via Principal</td>
                            <td>
                                <select id="via_princip_dir" onchange="setDireccion(2)">                                
                                </select>
                            </td>
                            <td><input type="text" id="nom_princip_dir" style="width: 100%;" onchange="setDireccion(1)"/></td>
                        </tr>
                        <tr>
                            <!-- <td>Via Generadora</td> -->
                            <td>Numero</td>
                            <td>
                                <select id="via_genera_dir" onchange="setDireccion(1)">                               
                                </select>
                            </td>

                            <td>
                                <table width="100%" border="0">
                                    <tr>
                                        <td align="center" width="49%">
                                            <input type="text" id="nom_genera_dir" style="width: 90%;" onchange="setDireccion(1)"/>
                                        </td>
                                        <td width="2%" align="center"> - </td>
                                        <td align="center" width="49%">
                                            <input type="text" id="placa_dir" style="width: 100%;" onkeypress="setDireccion(1)" onchange="setDireccion(1)"/>
                                        </td>    
                                    </tr>
                                </table>                        
                            </td>
                        </tr>
                        <tr>
                            <td>Complemento</td>
                            <td colspan="2"><input type="text" id="cmpl_dir" style="width: 100%;" onkeypress="setDireccion(1)" onchange="setDireccion(1)"/></td>
                        </tr>
                        <tr>
                            <td colspan="3"><input type="text" id="dir_resul" name="" style="width: 100%;" readonly/></td>
                        </tr>
                        <tr>
                            <td colspan="3">
                                <button onclick="setDireccion(3)">Aceptar</button>
                                <button onclick="setDireccion(0);">Salir</button>
                            </td>
                        </tr>
                    </table>
                </div>
            </div>
            <br>
            <div id="div_actores"  style="width: 700px">                       
                        <table aling="center" style=" width: 100%" >
                         <input type="hidden" id="idActor" name="idActor">  
                         <tr>                            
                             <td style="width: 10%"><span>Tipo:<b style="color:red">*</b></span></td>
                             <td style="width: 40%"> <select name="tipo" style="font-size: 14px;width:225px" id="tipo">
                             </select></td>                           
                             <td style="width: 10%"><span>Direcci�n:</span></td>                          
                             <td style="width: 40%"><input type="text" id="direccion" name="direccion" style=" width: 218px;" readonly >
                              <img src="/fintra/images/Direcciones.png" width="26" height="23" border="0" align="ABSMIDDLE" onclick="genDireccion('direccion', event);" alt="Direcciones"  title="Direcciones" />
                             </td>  
                         </tr> 
                         <tr>
                                <td style="width: 10%"><span>Tipo Doc.:<b style="color:red">*</b></span></td>
                                <td style="width: 40%"> <select name="tipo_doc" style="font-size: 14px;width:225px" id="tipo_doc">
                                </select></td>      
                                <td style="width: 10%"><span>Lugar Exp.:</span></td>                          
                                <td style="width: 30%"><input type="text" id="doc_lugar_exp" class="mayuscula" name="doc_lugar_exp" style=" width: 248px;" ></td>                              
                         </tr>
                         <tr>
                             <td style="width: 10%"><span>Documento:<b style="color:red">*</b></span></td>                          
                             <td style="width: 30%"><input type="text" id="documento" name="documento" class="solo-numero" maxlength="15" style=" width: 218px;text-transform: uppercase;" ></td>   
                             <td style="width: 10%"><span>Tel�fono:</span></td>                          
                             <td style="width: 40%">
                                 <input type="text" id="telefono" name="telefono" class="solo-numero" maxlength="7" style=" width: 130px;text-transform: uppercase;">
                                 <span>Ext:</span>  
                                 <input type="text" id="ext" name="ext" class="solo-numero" maxlength="4" style=" width: 80px;text-transform: uppercase;">
                             </td> 
                         </tr>
                         <tr>
                              <td style="width: 10%"><span>Nombre:<b style="color:red">*</b></span></td>   
                              <td style="width: 90%" colspan="4"><textarea id ="nombre" class="mayuscula" name="nombre" rows="1" maxlength="150" style="text-transform: uppercase;resize:none;width: 98%"></textarea></td>                      
                         </tr>
                         <tr>    
                             <td style="width: 10%"><span>Pa�s:<b style="color:red">*</b></span></td>
                             <td style="width: 50%"> <select name="pais" style="font-size: 14px;width:225px" id="pais">
                                 </select></td>
                             <td style="width: 10%"><span>Celular:<b style="color:red">*</b></span></td>                          
                             <td style="width: 30%"><input type="text" id="celular" name="celular" class="solo-numero" maxlength="10" style=" width: 245px;text-transform: uppercase;" ></td>  
                         </tr>
                         <tr>  
                             <td style="width: 10%"><span>Departamento:<b style="color:red">*</b></span></td>
                             <td style="width: 40%"> <select name="departamento" style="font-size: 14px;width:225px" id="departamento">
                                 </select></td>            
                             <td style="width: 10%"><span>Email:</span></td>                          
                             <td style="width: 40%"><input type="text" id="email" name="email" style=" width: 245px;" ></td> 
                         </tr>
                         <tr>
                             <td style="width: 10%"><span>Ciudad:<b style="color:red">*</b></span></td>
                             <td style="width: 50%"> <select name="ciudad" style="font-size: 14px;width:225px" id="ciudad"> 
                                 </select></td>
                             <td style="width: 10%"><span>Tarjeta Prof.:</span></td>                          
                             <td style="width: 30%"><input type="text" id="tarjeta_prof" name="tarjeta_prof" maxlength="10" style=" width: 245px;text-transform: uppercase;" ></td>  
                         </tr>
                     </table>                 
            </div>
        </div>           
        
            <div id="dialogLoading" style="display:none;">
                <p  style="font-size: 12px;text-align:justify;" id="msj2">Texto </p> <br/>
                <center>
                    <img src="./images/cargandoCM.gif"/>
                </center>
            </div>
            <div id="dialogMsj" title="Mensaje" style="display:none;">
                <p style="font-size: 12px;text-align:justify;" id="msj" > Texto </p>
            </div> 
        </div>
        <script type="text/javascript">  
               initActores();    
        </script>
    </body>
</html>
