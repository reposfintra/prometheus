<%-- 
    Document   : actividades_proyecto
    Created on : 8/06/2016, 02:34:38 PM
    Author     : mcastillo
--%>

<%@page contentType="text/html" pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
        <title>Actividades Proyecto</title>
        
        <link type="text/css" rel="stylesheet" href="./css/jquery/jquery-ui/jquery-ui.css" />
        <link href="./css/popup.css" rel="stylesheet" type="text/css">

        <script type="text/javascript" src="./js/jquery/jquery.jqGrid/js/jquery.min.js"></script> 
        <script type="text/javascript" src="./js/jquery-ui-1.8.5.custom.min.js"></script>
        <script src="./js/maestroProyectos.js" type="text/javascript"></script>

        <!--css logica de negocio-->
        <link type="text/css" rel="stylesheet" href="./css/contratos.css " />
         
        <!--jqgrid--> 
        <script type="text/javascript" src="./js/jquery/jquery.jqGrid/js/grid.locale-es.js"></script>
        <script type="text/javascript" src="./js/jquery/jquery.jqGrid/js/jquery.jqGrid.min.js"></script>
    </head>
    <body>
        <div id="capaSuperior"  style="position:absolute; width:100%; height:100px; z-index:0; left: 0px; top: 0px;">
            <jsp:include page="/WEB-INF/toptsp.jsp?encabezado=Actividades Proyecto"/>
        </div>
        <div id="capaCentral" style="position:absolute; width:100%; height:100%; z-index:0; left: 0px; top: 100px; ">         
            <center>
                <br>
                <table id="tabla_actividades"></table>
                <div id="page_tabla_actividades"></div>    
            </center>
            <div id="div_actividad"  style="width: 530px;display: none;" >  
                <table aling="center" style=" width: 100%" >                   
                    <tr>
                        <td>
                            <input type="text" id="idActividad"  style="width: 50px"hidden >
                        </td>
                    </tr>                   
                    <tr>
                        <td colspan="2" >
                            <label> Descripcion<span style="color:red;">*</span></label> 
                        </td>
                        <td colspan="3">
                            <textarea id ="descripcion" class="mayuscula" name="descripcion" rows="5" maxlength="300" onchange="conMayusculas(this)" style="resize:none;width:350px"></textarea>                      
                        </td>
                    </tr>  
                </table>
            </div>
            <div id="dialogLoading" title="Mensaje" style="display:none;">
                <p  style="font-size: 12px;text-align:justify;" id="msj2">Texto </p> <br/>
                <center>
                    <img src="./images/cargandoCM.gif"/>
                </center>
            </div>
            <div id="dialogMsj" title="Mensaje" style="display:none;">
                <p style="font-size: 12px;text-align:justify;" id="msj" > Texto </p>
            </div> 
        </div>
        <script type='text/javascript'>
            initActividades();            
        </script>
    </body>
</html>
