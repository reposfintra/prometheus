<%-- 
    Document   : foms_padre_hijo
    Created on : 28/11/2018, 10:46:44 AM
    Author     : Ing.William A. Siado Torres
--%>
<%@page contentType="text/html" pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html lang="ES">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
        <title>Relacionar Foms Padre - Hijo</title>
        
        <link href="./css/OportunidadNegocio.css" rel="stylesheet" type="text/css"/>
        
        <!----------------------------------------JQUERY--------------------------------------------->
        <link type="text/css" rel="stylesheet" href="./css/jquery/jquery-ui/jquery-ui.css"/>     
        <script type="text/javascript" src="./js/jquery/jquery-ui/jquery.min.js"></script>
        <script type="text/javascript" src="./js/jquery/jquery-ui/jquery.ui.min.js"></script> 
        <!----------------------------------------JQUERY--------------------------------------------->

        <!-------------------------------------JQGRID----------------------------------------->
        <link type="text/css" rel="stylesheet" href="./css/jquery/jquery.jqGrid/ui.jqgrid.css"/>
        <script type="text/javascript" src="./js/jquery/jquery.jqGrid-4.7.0/js/grid.locale-es.js"></script>
        <script type="text/javascript" src="./js/jquery/jquery.jqGrid-4.7.0/js/jquery.jqGrid.min.js"></script>
        <script type="text/javascript" src="./js/jquery/jquery.jqGrid-4.7.0/js/jquery.tabletojson.js"></script> 
        <!-------------------------------------/JQGRID----------------------------------------->

        <!-------------------------------------Notificaciones----------------------------------------->
        <link type="text/css" rel="stylesheet" href="./css/toastr.min.css"/>
        <script type="text/javascript" src="./js/toastr.min.js"></script>
        <!-------------------------------------/Notificaciones----------------------------------------->

        <!----------------------------------------Pre-Load--------------------------------------------->
        <!--se necesita un div para que funcione el preload se llama loader-wrapper-->

        <link type="text/css" rel="stylesheet" href="./css/main2.css">

        <!----------------------------------------Pre-Load--------------------------------------------->

        <!----------------------------------------bootstrap--------------------------------------------->
        <link href="./css/bootstrap/css/bootsrtap-3.3.7.min.css" rel="stylesheet" type="text/css"/>
        <!----------------------------------------bootstrap--------------------------------------------->
        
        
        <!----------------------------------------js-plantilla--------------------------------------------->
        <script src="./js/OportunidadNegocio/foms_padre_hijo.js" type="text/javascript"></script>
        <!----------------------------------------js-plantilla--------------------------------------------->
        



    </head>

       <!----------------------------------------Variables--------------------------------------------->
       
       <input id="id_solicitud_ok" type="text" name="id_solicitud" value="" hidden="true" />
       
       
       <!----------------------------------------Variables--------------------------------------------->
    
    <body>
    <center> 
        <jsp:include page="/WEB-INF/toptsp.jsp?encabezado=Relacionar  Foms Padres Hijos"/>
        <div id="div_busqueda" class="ui-jqgrid ui-widget ui-widget-content center-block" style="width: 900px; height-min: 300px;margin-top: 10px;border-radius: 10px;border: 1px solid #2A88C8;">
            <div class="container-fluid">

                <div class="row" style="padding: 5px">
                    <div class="col-md-12 ">

                        <fieldset class="scheduler-border" style=" width: 855px">
                            <legend class="scheduler-border">B�squeda De Solicitud</legend>
                            <!--<form>-->
                            <div class="form-row">
                                <div class="col-md-5">
                                    <input type="text" class="form-control" placeholder="Idsolicitud" id="id_solicitud">
                                </div>
                                <div class="col-md-5">
                                    <input type="text" class="form-control" placeholder="Foms" id="txt_foms">
                                </div>
                                <div class="col-md-2 ">
                                    <button id="btn_Aceptar_tipoCliente" class="btn btn-primary" > 
                                        <span class="ui-button-text">Buscar</span>
                                    </button> 
                                </div>
                            </div>
                            <!--</form>-->
                        </fieldset>
                    </div>
                </div>
            </div>
        </div>
        <div style="position: relative;top: 35px;">
            <table id="tabla_infoSolicitud"></table>
            <div id="pager"></div>
        </div>
    </center>
        
    



    <!-- **********************************************************************************************************************************************************
                                                                         modal - Relacionar Foms
    *********************************************************************************************************************************************************** -->
    <!----------------------------pantalla editar subcategoria ----------------------------->
        <div id="div_asociacion"  style="display: none;">
            
            <div class="container-fluid">
                <div class="form-row" style="display:flex; align-items: center">
                    <div class="col-md-5">
                        <table id="tbl_foms" ></table>
                        <div id="page_tbl_foms"></div>
                    </div>
                    <div class="col-md-1" style="display: flex; justify-content: center; flex-direction: column; align-items: center; padding-left: 80px">
                        <div id="bt_asociar_foms" title="Asignar Foms hijo" style="margin-bottom: 15px"> 
                            <span aling="center"  class="btn btn-primary" onClick="asociarFoms();"> >> </span>
                        </div>
                        <div id="bt_desasociar_foms" title="Desasignar Foms hijo"> 
                            <span aling="center"  class="btn btn-primary" onClick="desasociarFoms();"> << </span>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <table id="tbl_foms_r" ></table>
                        <div id="page_tbl_foms_r"></div>
                    </div>
            </div>
        </div>



    <!-- **********************************************************************************************************************************************************
                                                                    FIN  modal - Relacionar Foms
    *********************************************************************************************************************************************************** -->

    

    <!-- **********************************************************************************************************************************************************
                                                                         loader-wrapper
    *********************************************************************************************************************************************************** -->
    <div id="loader-wrapper">
        <div id="loader"></div>
        <img id="logowast" src="/fintra/images/Selectrik.png" style="z-index: 10002 !important">
        <div class="loader-section section-left">	
        </div>
        <div class="loader-section section-right"></div>
    </div>


    <!-- **********************************************************************************************************************************************************
                                                                    FIN  loader-wrapper
    *********************************************************************************************************************************************************** -->


</body>

</html>
