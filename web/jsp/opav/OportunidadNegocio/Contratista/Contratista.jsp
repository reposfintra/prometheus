<%@page contentType="text/html" pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html lang="es">

    <head>

        <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
        <meta name="viewport" content="width=device-width, initial-scale=1">


        <title>GESTION DE PRESUPUESTO</title>



        <link href="./css/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css"/>
        <link href="./css/simple-sidebar.css" rel="stylesheet" type="text/css"/>
        <link href="./css/OportunidadNegocio.css" rel="stylesheet" type="text/css"/>




        <link type="text/css" rel="stylesheet" href="./css/jquery/jquery-ui/jquery-ui.css"/>     
        <script type="text/javascript" src="./js/jquery/jquery-ui/jquery.min.js"></script>
        <script type="text/javascript" src="./js/jquery/jquery-ui/jquery.ui.min.js"></script> 
        <script src="./js/jquery/editor/ckeditor/ckeditor.js" type="text/javascript"></script>



        <link type="text/css" rel="stylesheet" href="./css/buttons/botonVerde.css"/>
        <script type="text/javascript" src="./js/jquery/jquery.jqGrid/js/grid.locale-es.js"></script>
        <script type="text/javascript" src="./js/jquery/jquery.jqGrid/js/jquery.jqGrid.min.js"></script>
        <link type="text/css" rel="stylesheet" href="./css/jquery/jquery.jqGrid/ui.jqgrid.css"/>    

        <!-------------------------------------Notificaciones----------------------------------------->
        <link type="text/css" rel="stylesheet" href="./css/toastr.min.css"/>
        <script type="text/javascript" src="./js/toastr.min.js"></script>
        <!-------------------------------------/Notificaciones----------------------------------------->

        <!----------------------------------------Pre-Load--------------------------------------------->
        <!--se necesita un div para que funcione el preload se llama loader-wrapper-->

        <link type="text/css" rel="stylesheet" href="./css/main2.css">

        <!----------------------------------------Pre-Load--------------------------------------------->




    </head> 
    <body>

        <input type="hidden" id="id_cliente" name="id_cliente">
    <center>
        <jsp:include page="/WEB-INF/toptsp.jsp?encabezado=GESTION DE PRESUPUESTO"/>
        <!-- **********************************************************************************************************************************************************
                                   Buscar Editar Cliente
        *********************************************************************************************************************************************************** -->
        <div id="div_busqueda" class="ui-jqgrid ui-widget ui-widget-content center-block" style="width: 1317px; height-min: 300px;margin-top: 10px;border-radius: 10px;border: 1px solid #2A88C8;">
            <!--
                        <div class="ui-dialog-titlebar ui-widget-header ui-helper-clearfix">
                            <span class="ui-dialog-title text-center" id="ui-dialog-title-listaGlobal">
                                &nbsp;
                                <label class="titulotablita"><b>Busqueda Solicitud</b></label>
                            </span>
                        </div>-->
            <div class="row" style="padding: 5px">
                <div class="col-md-12 ">

                    <fieldset class="scheduler-border" >
                        <legend class="scheduler-border">Busqueda De Solicitud</legend>
                        <table id="tablainterna" style="height: 53px; width: 1271px"  >
                            <tr>
                                <td>
                                    <label>Tipo De Proyecto</label>
                                </td>
                                <td>
                                    <select id="tipo_proyecto" style="width: 151px;color: #050505;height: 25px;margin-left: 5px;" >
                                        <option value=''></option>                                        
                                        <option value='0'>Proyectos Transicion</option>
                                        <option value='1'>Nuevos Proyectos</option>

                                    </select>
                                </td>
                                <td>
                                    <label>Linea Negocio</label>
                                </td>
                                <td>
                                    <select id="linea_negocio" style="width: 151px;color: #050505;height: 25px;margin-left: 5px;" ></select>
                                </td>
                                <td>
                                    <label style="margin-left: 10px;">Solicitud</label>
                                </td>
                                <td>
                                    <input type="text" id="solicitud"  style="width: 80px;margin-left: 5px;height: 26px;" >
                                </td>
                                <td>
                                    <label style="margin-left: 10px;">Foms</label>
                                </td>
                                <td>
                                    <input type="text" id="txt_foms"  style="width: 80px;margin-left: 5px;height: 26px;" >
                                </td>
                                <td>
                                    <label style="margin-left: 10px;">Nombre Cliente</label>
                                </td>
                                <td>
                                    <input type="text" id="txt_nom_cliente"  style="width: 80px;margin-left: 5px;height: 26px;" >
                                </td>
                                <td>
                                    <label style="margin-left: 10px;">Nombre Proyecto</label>
                                </td>
                                <td>
                                    <input type="text" id="txt_nom_proyecto"  style="width: 80px;margin-left: 5px;height: 26px;" >
                                </td>
                                <td>
                                    <label style="margin-left: 10px;"hidden="true">Responsable</label>
                                </td>
                                <td>
                                    <input type="text" id="responsable"  hidden="true" style="width: 139px;margin-left: 5px;height: 26px;" >

                                </td>

                                <td>
                                    <label style="margin-left: 10px;" hidden="true"  >Estado cartera</label>
                                </td>
                                <td>
                                    <select id="estadocartera" hidden="true" style="width: 108px;color: #050505;height: 25px;margin-left: 5px;" ></select>
                                </td> 
                                <td>
                                    <label style="margin-left: 10px;" hidden="true">Fecha validacion cartera</label>
                                </td>
                                <td>
                                    <input type="datetime" id="fechaini" hidden="true" style="width: 100px ;height: 26px;"  readonly>
                                </td>
                                <td>
                                    <input  type="datetime" id="fechafin" hidden="true" style="width: 100px;height: 26px;" readonly>
                                </td>
                                <td>
                                    <input type="text" id="etapaActual"  style="width: 80px;margin-left: 5px;height: 26px;" value="PRESUPUESTO" hidden >
                                </td>
                                <td>
                                    <div>

                                        <center>
                                            <button id="btn_Aceptar_tipoClienteP" class="ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only" style="margin-left: 15px;" > 
                                                <span class="ui-button-text">Buscar</span>
                                            </button> 
                                        </center>
                                    </div>
                                </td>
                            </tr>
                        </table>
                    </fieldset>
                </div>
            </div>
        </div>
        <div style="position: relative;top: 35px;">
            <table id="tabla_infoSolicitudP" ></table>
            <div id="pagerP"></div>
        </div>
        <!-- **********************************************************************************************************************************************************
                                                   Div Presenta Solicitudes
        *********************************************************************************************************************************************************** -->

        <div id="div_Solicitidudes" class="col-md-12 fx-center" style="display: none" >    


            <table id="solicitudes" >

                <div id="searchsolicitudes"></div>
                <div id="page_tabla_solicitudes"></div>
            </table>



        </div>
        <!-- **********************************************************************************************************************************************************
                                   
        *********************************************************************************************************************************************************** -->
        <div id="fformPrincipal3" style="display: none">
            <!--    <div class="ui-jqgrid ui-widget ui-widget-content center-block" style="width: 100%; height-min: 300px; margin-top: 30px">
                    <div class="ui-dialog-titlebar ui-widget-header ui-helper-clearfix">
                        <span class="ui-dialog-title text-center" id="ui-dialog-title-listaGlobal">
                            &nbsp;
                            <label class="titulotablita"><b>Cliente</b></label>
                        </span>
            
                    </div>
                    <div class="row"style="padding: 5px">
                        <div class="col-md-12 ">
                            <fieldset  class="scheduler-border" style="margin: 3px">
                                <legend class="scheduler-border">Datos Clientes </legend>
                                <div class="row">
                                    <div class="col-md-2">Nombre :</div>
                                    <div class="col-md-4"><input id="NombreCliente" disabled="true" type="text" placeholder="" style="width: 86%;" ></div>
                                    <div class="col-md-2">Departamento :</div>
                                    <div class="col-md-4">
                                        <input id="Departamento" type="text" disabled="true" placeholder="" style="width: 86%;" >
                                        <input id="idDepartamento" type="text" disabled="true" placeholder=""  style="display: none" >
            
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-2">Nit :</div>
                                    <div class="col-md-4">
                                        <input id="NitCliente" disabled="true" class="solo-numero" type="text" placeholder="" style="width: 73%;" onblur="CalcularDv()" >
                                        -
                                        <input id="DigitoVerificacion" disabled="true" type="text" placeholder="" style="width: 10%; text-align: center" >
                                    </div>
                                    <div class="col-md-2">Ciudad :</div>
                                    <div class="col-md-4">
                                        <input id="Ciudad" type="text" disabled="true" placeholder="" style="width: 86%;" >
                                        <input id="idCiudad" type="text" style="display: none">
                                    </div>
            
                                </div>
                                <div class="row">
                                    <div class="col-md-2">Nombre Contacto:</div>
                                    <div class="col-md-4"><input id="NombreContacto" disabled="true" type="text" placeholder="" style="width: 86%;" ></div>
            
                                    <div class="col-md-2">Direccion :</div>
                                    <div class="col-md-4"> 
                                        <input id="DireccionCliente" type="text" disabled="true" placeholder="" style="width: 73%;">
                                        <img src="/fintra/images/Direcciones.png" width="26" height="23" border="0" align="ABSMIDDLE" onclick="genDireccion('direccion', event);" alt="Direcciones"  title="Direcciones" />
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-2">Celular Contacto:</div>
                                    <div class="col-md-4">
                                        <input id="CelularContacto" class="solo-numero" disabled="true" type="text" placeholder="" style="width: 86%;" maxlength="10">
                                    </div>
                                </div>
                            </fieldset>
                        </div>
                    </div>
                </div>-->




            <div class="ui-jqgrid ui-widget ui-widget-content center-block" style="width: 100%; height-min: 300px;">
                <!--                <div class="fx-distribuir-between ui-dialog-titlebar ui-widget-header ui-helper-clearfix">
                                    <span class="ui-dialog-title text-center" id="ui-dialog-title-listaGlobal">
                                        &nbsp;
                                        <label class="titulotablita"><b>Datos Solicitud</b></label>
                                    </span>
                
                                </div>-->
                <!-- **********************************************************************************************************************************************************
                                                                        INICIO Div Datos Solicitud
                *********************************************************************************************************************************************************** -->

                <div class="row"  style="padding: 5px;">


                    <div class="col-md-12 ">
                        <div id='formPrincipal'>
                            <fieldset  class="scheduler-border">
                                <legend class="scheduler-border">Datos Solicitud </legend>
                                <div class="row">
                                    <div class="col-md-9">
                                        <div class="row">
                                            <div class="col-md-2">Cliente :</div>
                                            <div class="col-md-4">
                                                <input id="txt_NombreCliente" readonly="true" type="text" placeholder="" style="width: 86%;" >
                                                <input type="hidden" id="idCliente" name="idCliente">
                                            </div>
                                            <div id="div_opd" style="display: ">
                                                <div class="col-md-2">OPD :</div>
                                                <div class="col-md-4">
                                                    <input id="OPD"  type="text" readonly="true" placeholder="" style="width: 86%;" >                            
                                                </div>
                                            </div>

                                        </div>
                                        <div class="row">
                                            <div class="col-md-2">Linea Negocio:</div>
                                            <div class="col-md-4">
                                                <select id='cbx_LineaDeNegocio'  disabled="true" class="selectpicker" name="cbx_LineaDeNegocio"  onchange="cargarCbxTipoSolicitud();" style="width: 86%;"></select>                         
                                            </div>
                                            <!--                                            <div class="col-md-2">Tipo Solicitud :</div>
                                                                                        <div class="col-md-4">
                                                                                            <select id='cbx_TipoSolicitud' disabled="true" class="selectpicker" name="cbx_TipoSolicitud"  style="width: 86%;"></select>                         
                                                                                        </div>-->
                                            <div class="col-md-2">Nic :</div>
                                            <div class="col-md-4"> 
                                                <select id='cbx_nic' disabled="true" class="selectpicker" name="cbx_nic"  style="width: 60%;"></select>
                                            </div>


                                        </div>
                                        <div class="row">
                                            <div class="col-md-2">Responsable :</div>
                                            <div class="col-md-4"> 
                                                <!--<select id='cbx_Responsable' class="selectpicker" name="cbx_Responsable"  style="width: 86%;"></select>-->
                                                <input id="txt_responsable" readonly="true" type="text" placeholder="" style="width: 86%;" >
                                                <input id="idresponsable" hidden="true" type="text" placeholder="" style="width: 86%;" >
                                            </div>
                                            
                                            <div class="col-md-2">Fecha limite :</div>
                                            <div class="col-md-4"> 
                                                <input id="fecha_limite3" disabled="true" type="datetime" placeholder="" style="line-height: 10px;">
                                            </div> 
                                            
                                        </div>
                                        <div class="row" style="display: none">

                                            <div class="col-md-2">Interventor :</div>
                                            <div class="col-md-4"> 
                                                <select id='cbx_Interventor' class="selectpicker" name="cbx_Interventor"  style="width: 86%;"></select>

                                            </div>
                                        </div>
                                    </div>  
                                    <div class="col-md-3">  
                                        <div class="row">
                                            <div class="col-md-4">IdSolicitud:</div>
                                            <div class="col-md-8"> 
                                                <input id="idSolicitud" readonly="true" type="text" placeholder="" style="width: 100%;">
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-4">Cartera :</div>
                                            <div class="col-md-8"> 
                                                <select id='cbx_Cartera' class="selectpicker" disabled="true" name="cbx_nic" style="width: 100%;"></select>
                                            </div>
                                        </div>
                                    </div>
                                </div>


                                <div class="row">
                                    <div class="col-md-9">
                                        <div class="row">

                                            <div class="col-md-2">Historia Solicitud :</div>
                                            <div class="col-md-10"> 
                                                <div class="form-group">
                                                    <textarea id="historiasolicitud" readonly="true" class="form-control" rows="5" ></textarea>
                                                </div>
                                            </div>   
                                        </div>
                                    </div>
                                </div>


                        </div>
                        <div class="row">
                            <div class="col-md-9">
                                <div class="row">
                                    <div class="col-md-2">Fecha Creacion :</div>
                                    <div class="col-md-3"> 
                                        <input id="fechaCreacion" readonly="true" type="text" placeholder="" style="width: 100%;">
                                    </div>
                                </div>
                            </div>
                        </div>




                    </div>


                </div>
            </div>

            <div class="ui-jqgrid ui-widget ui-widget-content center-block"  style="width: 100%; height-min: 300px; display: none;margin-top: 30px">
                <div class="ui-dialog-titlebar ui-widget-header ui-helper-clearfix">
                    <span class="ui-dialog-title text-center" id="ui-dialog-title-listaGlobal">
                        &nbsp;
                        <label class="titulotablita"><b>Acciones</b></label>
                    </span>
                </div>
                <!-- **********************************************************************************************************************************************************
                                                                        INICIO Div Acciones
                *********************************************************************************************************************************************************** -->

                <div class="row"  style="padding: 5px;">
                    <div class="col-md-12">


                        <div id="div_acciones" >    
                            <table border="0"  align="center">
                                <tr>
                                    <td>
                                        <table id="acciones" >

                                            <div id="searchacciones"></div>
                                        </table>
                                        <div id="page_tabla_acciones"></div>
                                    </td>
                                </tr>
                            </table>   
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <!-- **********************************************************************************************************************************************************
                                                                Inicio Div Escoger Contratista
        *********************************************************************************************************************************************************** -->


        <div id="div_escoger_contratistas"  style="display: none; width: 800px" >

            <div class="fx-center">

                <table id="escoger_contratistas" ></table>
                <div id="page_tabla_escoger_contratistas"></div>

            </div>




            </table>

        </div> 


        <!-- **********************************************************************************************************************************************************
                                                                Inicio Crear Accion
        *********************************************************************************************************************************************************** -->


        <div id="div_accion"  style="display: none; width: 400px" >


            <div class="row">
                <div class="col-md-2">
                    Tipo Trabajo :
                </div>
                <div class="col-md-4">
                    <select id='cbx_TipoTrabajo2' class="form-control" name="cbx_TipoSolicitud"  style="width: 86%;"></select>                         
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    Descripcion :
                </div>

                <div class="col-md-12">
                    <textarea id="descripcion_accion" placeholder="Descripcion" class="form-control" rows="4" ></textarea>
                </div>


            </div>



        </div> 

        <div class="contextMenu" id="myMenu1" style="display:none;">
            <ul  id="lstOpciones" >
                <li id="ver_detalle_pago">
                    <span style="float:left"></span>
                    <span style="font-size:11px; font-family:Verdana">Ver informaci�n de pago</span>
                </li>  
                <li id="ver_causal_dev">
                    <span style="float:left"></span>
                    <span style="font-size:11px; font-family:Verdana">Ver causal devoluci�n</span>
                </li>  
            </ul>
        </div>

        <!------------------------------------------------------------------------->
        <!-- **********************************************************************************************************************************************************
                                                           nicio Div Definiciones de Acciones y Alcances Solicitud
        *********************************************************************************************************************************************************** -->


        <div id="div_def_acciones_alcances" class="container" style="display: none" >

            <ul class="nav nav-tabs" id="myTab">
                <li id="li_definicion_acciones" class="active" onclick="tabbs(1)"><a href="#definicion_acciones" data-toggle="tab">Definicion De Acciones</a></li>
                <li id="li_definicion_alcances" onclick="tabbs(2)"><a href="#definicion_alcances" data-toggle="tab">Definicion De Alcances</a></li>
            </ul>

            <div class="tab-content">
                <div id="definicion_acciones" class="tab-pane fade in active" style="padding-top: 10px">
                    <div class="row">
                        <div class="col-md-2">
                            Fecha Asignacion :
                        </div>
                        <div class="col-md-4">
                            <input id="fechaAsignacion" class="form-control" type="text" readonly="true">
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-2">
                            Descripcion :
                        </div>
                        <div class="col-md-10">
                            <textarea id="descripcionn" readonly="true" type="text" rows="6" class="form-control" style="width: 100%"></textarea>
                        </div>
                    </div>
                    <div class="row">
                        <div class ="col-md-2">
                            Fecha Planeada Visita :
                        </div>
                        <div class ="col-md-4">
                            <input type="text" id="fecha_planeada_visita" class="form-control">
                        </div>
                        <div class ="col-md-2 fecha_ejecu_visita" >
                            Fecha Ejecucion Visita :
                        </div>
                        <div class ="col-md-4 fecha_ejecu_visita">
                            <input type="text" id="fecha_ejecucion_visita" class="form-control">
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-2">
                            Tipo Trabajo :
                        </div>
                        <div class="col-md-4">
                            <select id='cbx_TipoTrabajo' class="form-control" name="cbx_TipoSolicitud"  style="width: 86%;"></select>                         
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-2">
                            Observacion :
                        </div>
                        <div class="col-md-10">
                            <textarea id="observacion_accion" type="text" class="form-control" rows="6" style="width: 100%"></textarea>
                        </div>
                    </div>
                    <div class="row" id="row_obsAdicional">
                        <div class="col-md-2">
                            Observacion Adicional :
                        </div>
                        <div class="col-md-10">
                            <textarea id="descripcion_adicional" class="form-control" type="text" rows="6" style="width: 100%"></textarea>
                        </div>
                    </div>
                </div>
                <div id="definicion_alcances" class="tab-pane fade" style="padding-top: 10px">
                    <div class="row">
                        <div class="col-md-2">
                            Alcances :
                        </div>
                        <div class="col-md-10">
                            <textarea id="alcances" class="form-control" type="text" rows="6" style="width: 100%"></textarea>
                        </div>
                    </div>
                    <div class="row" style="margin-top: 15px">
                        <div class="col-md-2">
                            Adicionales :
                        </div>
                        <div class="col-md-10">
                            <textarea id="descripcion_adicional2" class="form-control" type="text" rows="6" style="width: 100%"></textarea>
                        </div>
                    </div>
                    <div class="row" style="margin-top: 15px">
                        <div class="col-md-2">
                            Descripcion :
                        </div>
                        <div class="col-md-10">
                            <textarea id="descripcion_alcances" class="form-control" type="text" rows="6" style="width: 100%"></textarea>
                        </div>
                    </div>
                </div>
            </div>
        </div>


        <!--************************************************************************************-->
        <!-------------------crear APU-------------------------------->
        <div id="div_apu"  style="display: none; width: 800px" >   
            <center>
                <div id="filtros" class="ui-jqgrid ui-widget ui-widget-content ui-corner-all" style="height: 93px; width: 445px">
                    </br>
                    <table aling="center" style=" width: 100%" >
                        <tr>
                            <td>Grupo APU:</td>
                            <td>
                                <select id="grupo_apu" nombre="grupo_apu" style="width: 264px;" >
                                    <!--option value=''>...</option-->
                                </select>
                            </td>
                            <td><img id = "add_grupo" src = "/fintra/images/botones/iconos/adds.png"
                                     style = "margin-left: 5px; height: 19px; vertical-align: middle;" 
                                     onclick = "crearGrupoApu();"></td>
                        </tr> 
                        <tr>
                            <td>Unidad Medida:</td>
                            <td>
                                <select id="unmed" nombre="unmed" style="width: 264px;" >
                                    <!--option value=''>...</option-->
                                </select>
                            </td>
                            <td><img id = "add_unidadmed" src = "/fintra/images/botones/iconos/adds.png"
                                     style = "margin-left: 5px; height: 19px; vertical-align: middle;" 
                                     onclick = "crearUnidadMedida();"></td>
                        </tr>
                        <tr>
                            <td>Nombre APU:</td>
                            <td style="width: 50%">
                                <input type="text" id="nomapu" name="nomapu" style=" width: 248px">
                                <input type="hidden" id="idapu" name="idapu">
                            </td>
                        </tr>
                    </table>
                </div>  
                </br> 

                <!---------Grid que contiene los insumos ----------->
                <div style="margin-top: 0px">
                    <table id="tbl_insumos" ></table>                
                    <div id="page_insumos"></div>
                </div>
                <!-------------------->

                <div id="div_filtro_insumos"  style="display: none; width: 800px" >
                    <div id="filtros" class="ui-jqgrid ui-widget ui-widget-content ui-corner-all" style="height: 95px;padding: 0px 10px 5px 10px">
                        <center>

                            <table style=" padding: 0.5em 1em 0.5em 1em" style="width: 95%;">

                                <input type="hidden" id="idregistro" name="idregistro">

                                <tr>
                                    <td>Categoria</td>
                                    <td>
                                        <select id="categoria" nombre="categoria" onchange="cargarCombo('sub', [this.value])" style="width: 264px;" >
                                            <option value=''>...</option>
                                        </select>
                                    </td>
                                </tr>
                                <tr>
                                    <td>SubCategoria</td>
                                    <td>
                                        <select id="sub"  style="width: 264px;" >
                                            <option value=''>...</option>
                                        </select>

                                        <input type="hidden" name="nomsub" id="nomsub">
                                    </td>
                                </tr>
                            </table>
                            <hr>
                            <div id ='botones'>
                                <button id="buscar" class="ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only" 
                                        role="button" aria-disabled="false">
                                    <span class="ui-button-text">Buscar</span>
                                </button> 
                            </div>
                        </center>
                    </div>  
                    </br>  

                    <!---------Grid que contiene filtro de insumos ----------->
                    <div style="margin-top: 0px">
                        <center>
                            <table id="tbl_filtro_insumos" ></table>                
                            <div id="page_filtro_insumos"></div>
                        </center>
                    </div>
                    <!-------------------->

                    <!--------div materiales------------>
                    <div style="margin-top: 0px">
                        <center>
                            <table id="tbl_materiales" ></table>                
                            <div id="page_materiales"></div>
                        </center>
                    </div>

            </center>
        </div>

        <!-------------------crear grupo APU-------------------------------->
        <div id="div_grupo_apu"  style="display: none; width: 800px" >                       
            <div id="filtros" class="ui-jqgrid ui-widget ui-widget-content ui-corner-all" style="height: 110px;">
                </br>
                <table aling="center" style=" width: 100%" >
                    <tr>
                        <td style="width: 10%"><span>Nombre Grupo</span></td>                          
                        <td style="width: 50%"><input type="text" id="nomgrupo" name="nomgrupo" style=" width: 248px">
                        </td>  
                    </tr> 
                    <tr>
                        <td style="width: 10%"><span>Descripcion Grupo:</span></td>   
                        <td style="width: 90%" colspan="4"><textarea id ="descgrupo" name="descgrupo" style="width:535px;resize:none" rows="2" ></textarea></td>                        
                    </tr>
                </table>
            </div>  
            </br> 
        </div>

        <!-------------------crear Unidad Medida-------------------------------->
        <div id="div_unidad_medida"  style="display: none; width: 800px" >                       
            <div id="filtros" class="ui-jqgrid ui-widget ui-widget-content ui-corner-all" style="height: 53px;">
                </br>
                <table aling="center" style=" width: 100%" >
                    <tr>
                        <td style="width: 20%"><span>Unidad de Medida</span></td>                          
                        <td style="width: 50%"><input type="text" id="nomunidad" name="nomunidad" style=" width: 280px">
                        </td>  
                    </tr> 
                </table>
            </div>  
            </br> 
        </div>
        <!--************************************************************************************-->
        <!-- **********************************************************************************************************************************************************
                                                                Ventana Clonacion
        *********************************************************************************************************************************************************** -->

        <div id="ventanaClonacion" style="display: none">
            <table>
                <tbody>
                    <tr>
                        <td>Id_solicitud origen :</td>
                        <td><input type="text" id="clonacion_id_solicitud_origen" style="margin-left: 3px" readonly></td>
                    </tr>
                    <tr>
                        <td>Id_solicitud destino :</td>
                        <td><input type="text" id="clonacion_id_solicitud_destino" class="soloNumeros" style="margin-left: 3px" ></td>
                    </tr>
                </tbody>
            </table>
        </div>
        <!-- **********************************************************************************************************************************************************
                                                                Ventana cambio de etapa y estado
        *********************************************************************************************************************************************************** -->

        <div id="cambioEtapa" style="display: none">
            <div class="row"style="padding: 5px">
                <div class="col-md-2 ">
                    <fieldset class="scheduler-border" style="width: 720px">

                        <div class="row">
                            <div class="col-md-2">
                                Idsolicitud :
                            </div>
                            <div class="col-md-4">
                                <input type="text" id="idsolicitud" readonly>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-2">
                                Etapa Actual:
                            </div>
                            <div class="col-md-4">
                                <input id="etapa" type="Text" readonly="true" style="width: 80%">
                                <input id="idetapa" hidden="true" type="Text" style="width: 80%">
                            </div>
                            <div class="col-md-2">
                                Estado Actual:
                            </div>
                            <div class="col-md-4">
                                <input id="estadoActual" readonly="true" type="Text" style="width: 80%">
                                <input id="idestadoActual" readonly="true" type="Text" style="width: 80%" hidden>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-2">
                                Estado :
                            </div>
                            <div class="col-md-4">
                                <select id="estado" class="selectpicker" style="width: 80%"></select>
                            </div>
                        </div>
                        <div class="row" hidden="true">
                            <div class="col-md-2">
                                Causal :
                            </div>
                            <div class="col-md-10">
                                <textarea id="causal" class="form-control" rows="5"></textarea>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-2">
                                Trazabilidad :
                            </div>
                            <div class="col-md-10">
                                <textarea id="trazabilidad" class="form-control" readonly="true" rows="5"></textarea>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-2">
                                Descripcion :
                            </div>
                            <div class="col-md-10">
                                <textarea id="descripcion_causal" class="form-control" rows="5"></textarea>
                            </div>
                        </div>


                    </fieldset>
                </div>
            </div>
        </div>



    </div>



    <script src="./js/OportunidadNegocio/Contratista.js" type="text/javascript"></script>


    <script type="text/javascript" href="http://www.ddginc-usa.com/spanish/editcheck.js"></script>
    <script type="text/javascript" src="./js/jquery/jquery.jqGrid-4.7.0/js/grid.locale-es.js"></script>
    <script type="text/javascript" src="./js/jquery/jquery.jqGrid-4.7.0/js/jquery.jqGrid.min.js"></script>
    <script type="text/javascript" src="./js/jquery/jquery.jqGrid-4.7.0/js/jquery.tabletojson.js"></script> 

    <div id="dialogLoading" style="display:none;">
        <p  style="font-size: 12px;text-align:justify;" id="msj2">Texto </p> <br/>
        <center>
            <img src="./images/cargandoCM.gif"/>
        </center>
    </div>
    <!-- Dialogo de los jsp> -->
    <div id="dialogMsj" style="display:none;">
        <p style="font-size: 12px;text-align:justify;" id="msj" > Texto </p>
    </div>
    <div id="div_Cargar_Archivos" style="display: none">
        <form name="form_Cargar_Archivos" id="form_Cargar_Archivos" method="post" enctype="multipart/form-data">

            <table aling="center" style=" width: 100%">
                <tr>
                    <td>
                        <fieldset style="height:90px">
                            <legend>Carga De Archivos</legend>    
                            <table aling="center" style=" width: 100%" >             
                                <tr>
                                    <td style="font-size: 12px;padding-right: 5px;width: 25%">
                                        Seleccione archivo
                                    </td>
                                    <td style="max-width: 300px">
                                        <input type="text" id="idsolicitud_carga_archivo" name="idsolicitud_carga_archivo" hidden="true">
                                        <input type="file" id="examinar" name="examinar"  style="font-size: 11px;" >                                                
                                    </td>

                                </tr>
                                <tr>
                                    <td></td>
                                    <td>

                                        <button id="subirArchivo" class="ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only" 
                                                role="button" aria-disabled="false"style="width: 50px;margin-top: 5px" >
                                            <span if class="ui-button-text"  >Subir</span>
                                        </button> 

                                    </td>  
                                    <!--                                    <tr>
                                                                            <td style="font-size: 12px;">
                                                                                Tipo Contrato
                                                                            </td>                                              
                                                                            <td>
                                                                                <input type="radio" name="tipo_contrato" value="2">P�blico
                                                                                <input type="radio" name="tipo_contrato" value="3" checked> Privado
                                                                            </td>         
                                                                        </tr>-->
                                </tr>
                            </table>
                        </fieldset>
                    </td> 

                </tr>
                <tr>
                    <td>
                        <fieldset style="height:123px">
                            <legend style="padding-top: 15px">Archivos Cargados</legend>
                            <div style="height:123px;overflow:auto;">
                                <table id="tbl_archivos_cargados" class="table table-hover" aling="center" style=" width: 100%" > 


                                </table>

                            </div>
                        </fieldset>
                    </td> 
                </tr>
            </table> 

        </form>
    </div>
</center>
<div id="loader-wrapper">
    <div id="loader"></div>
    <img id="logowast" src="/fintra/images/Selectrik.png" style="z-index: 10002 !important">
    <div class="loader-section section-left">	
    </div>
    <div class="loader-section section-right"></div>
</div>
</body>
</html>