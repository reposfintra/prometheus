<%-- 
    Document   : moduloEjecucion
    Created on : 8/11/2016, 09:25:16 AM
    Author     : Ing.William Siado Torres
--%>


<%@page contentType="text/html" pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>  

    <head>

        <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">



        <title>Oportunidad De Negocio</title>



        <link href="./css/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css"/>

        <link type="text/css" rel="stylesheet" href="./css/jquery/jquery-ui/jquery-ui.css"/>     
        <script type="text/javascript" src="./js/jquery/jquery-ui/jquery.min.js"></script>
        <script type="text/javascript" src="./js/jquery/jquery-ui/jquery.ui.min.js"></script> 
        <script src="./js/jquery/editor/ckeditor/ckeditor.js" type="text/javascript"></script>

        <link href="./css/OportunidadNegocio.css" rel="stylesheet" type="text/css"/>



        <link type="text/css" rel="stylesheet" href="./css/buttons/botonVerde.css"/>
        <script type="text/javascript" src="./js/jquery/jquery.jqGrid/js/grid.locale-es.js"></script>
        <script type="text/javascript" src="./js/jquery/jquery.jqGrid/js/jquery.jqGrid.min.js"></script>
        <link type="text/css" rel="stylesheet" href="./css/jquery/jquery.jqGrid/ui.jqgrid.css"/>    



        <script src="./js/OportunidadNegocio/moduloEjecucion.js" type="text/javascript"></script>

        <!-------------------------------------Notificaciones----------------------------------------->
        <link type="text/css" rel="stylesheet" href="./css/toastr.min.css"/>
        <script type="text/javascript" src="./js/toastr.min.js"></script>
        <!-------------------------------------/Notificaciones----------------------------------------->

        <!----------------------------------------Pre-Load--------------------------------------------->
        <!--se necesita un div para que funcione el preload se llama loader-wrapper-->

        <link type="text/css" rel="stylesheet" href="./css/main2.css">

        <!----------------------------------------Pre-Load--------------------------------------------->


    </head> 
    <body>

        <%
            String idaccion = request.getParameter("idaccion");
        %>
    <center>
        <jsp:include page="/WEB-INF/toptsp.jsp?encabezado=Modulo Ejecucion"/>

        <input type="hidden" id="id_cliente" name="id_cliente">

        <!-- **********************************************************************************************************************************************************
                                                                    Buscar Solicitud
        *********************************************************************************************************************************************************** -->
        <div id="div_busqueda" class="ui-jqgrid ui-widget ui-widget-content center-block" style="width: 1317px; height-min: 300px;margin-top: 10px;border-radius: 10px;border: 1px solid #2A88C8;">

            <!--            <div class="ui-dialog-titlebar ui-widget-header ui-helper-clearfix">
                            <span class="ui-dialog-title text-center" id="ui-dialog-title-listaGlobal">
                                &nbsp;
                                <label class="titulotablita"><b>B�squeda Solicitud</b></label>
                            </span>
                        </div>-->
            <div class="row" style="padding: 5px">
                <div class="col-md-12 ">

                    <fieldset class="scheduler-border" >
                        <legend class="scheduler-border">B�squeda De Solicitud</legend>
                        <table id="tablainterna" style="height: 53px; width: 1271px"  >
                            <tr>
                                <td>
                                    <label>Tipo De Proyecto</label>
                                </td>
                                <td>
                                    <select id="tipo_proyecto" style="width: 151px;color: #050505;height: 25px;margin-left: 5px;" >
                                        <option value=''></option>                                        
                                        <option value='0'>Proyectos Transicion</option>
                                        <option value='1'>Nuevos Proyectos</option>

                                    </select>
                                </td>
                                <td>
                                    <label>Linea Negocio</label>
                                </td>
                                <td>
                                    <select id="linea_negocio" style="width: 151px;color: #050505;height: 25px;margin-left: 5px;" ></select>
                                </td>
                                <td>
                                    <label style="margin-left: 10px;">Solicitud</label>
                                </td>
                                <td>
                                    <input type="text" id="solicitud"  style="width: 80px;margin-left: 5px;height: 26px;" >
                                </td>
                                <td>
                                    <label style="margin-left: 10px;">Foms</label>
                                </td>
                                <td>
                                    <input type="text" id="txt_foms"  style="width: 80px;margin-left: 5px;height: 26px;" >
                                </td>
                                <td>
                                    <label style="margin-left: 10px;">Nombre Cliente</label>
                                </td>
                                <td>
                                    <input type="text" id="txt_nom_cliente"  style="width: 80px;margin-left: 5px;height: 26px;" >
                                </td>
                                <td>
                                    <label style="margin-left: 10px;">Nombre Proyecto</label>
                                </td>
                                <td>
                                    <input type="text" id="txt_nom_proyecto"  style="width: 80px;margin-left: 5px;height: 26px;" >
                                </td>
                                <td>
                                    <label style="margin-left: 10px;"hidden="true">Responsable</label>
                                </td>
                                <td>
                                    <input type="text" id="responsable"  hidden="true" style="width: 139px;margin-left: 5px;height: 26px;" >

                                </td>

                                <td>
                                    <label style="margin-left: 10px;" hidden="true"  >Estado cartera</label>
                                </td>
                                <td>
                                    <select id="estadocartera" hidden="true" style="width: 108px;color: #050505;height: 25px;margin-left: 5px;" ></select>
                                </td> 
                                <td>
                                    <label style="margin-left: 10px;" hidden="true">Fecha validacion cartera</label>
                                </td>
                                <td>
                                    <input type="datetime" id="fechaini" hidden="true" style="width: 100px ;height: 26px;"  readonly>
                                </td>
                                <td>
                                    <input  type="datetime" id="fechafin" hidden="true" style="width: 100px;height: 26px;" readonly>
                                </td>
                                <td>
                                    <input type="text" id="etapaActual"  style="width: 80px;margin-left: 5px;height: 26px;" value="COMERCIAL" hidden >
                                </td>
                                <td>
                                    <div>
                                        <center>
                                            <button id='buscar_solicitud' class="ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only" > 
                                                <span class="ui-button-text">Buscar</span>
                                            </button> 

                                        </center>
                                    </div>
                                </td>
                            </tr>
                        </table>
                    </fieldset>
                </div>
            </div>
        </div>
        <div style="position: relative;top: 35px;">
            <table id="tabla_infoSolicitud" ></table>
            <div id="pager"></div>
        </div>
        
        <div id="ventana_Responsables" style="margin-top: 25px">
            <table id="tbl_Responsables" ></table>
            <div id="page_tbl_Responsables"></div>  
        </div>

        <!-- **********************************************************************************************************************************************************
                                                                    
        *********************************************************************************************************************************************************** -->

        <div id="div_Control"  style="width: 530px;display: none; " >  
            <table aling="center" style=" width: 100%" >                   
                <tr>
                    <td colspan="2" >
                        <label> Responsable : <span style="color:red;">*</span></label> 
                    </td>
                    <td colspan="3">
                        <input type="text" id="responsable_" class="" style="width: 298px" maxlength="30"  />
                        <input type="text" id="usuario_" class="" style="width: 298px" maxlength="30" hidden="true" />
                        <input type="text" id="id_" class="" style="width: 298px" maxlength="30" hidden="true" />
                        <input type="text" id="id_solicitud_" class="" style="width: 298px" maxlength="30" hidden="true" />
                    </td>
                </tr>  
            </table>
        </div>

        <!-- **********************************************************************************************************************************************************
                                                                    Ventana cambio de etapa y estado de la Solicitud
        *********************************************************************************************************************************************************** -->

        <div id="cambioEtapa" style="display: none">
            <div class="row"style="padding: 5px">
                <div class="col-md-2 ">
                    <fieldset class="scheduler-border" style="width: 720px">

                        <div class="row">
                            <div class="col-md-2">
                                Idsolicitud :
                            </div>
                            <div class="col-md-4">
                                <input type="text" id="idsolicitud" readonly>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-2">
                                Etapa Actual:
                            </div>
                            <div class="col-md-4">
                                <input id="etapa" type="Text" readonly="true" style="width: 80%">
                                <input id="idetapa" hidden="true" type="Text" style="width: 80%">
                            </div>
                            <div class="col-md-2">
                                Estado Actual:
                            </div>
                            <div class="col-md-4">
                                <input id="estadoActual" readonly="true" type="Text" style="width: 80%">
                                <input id="idestadoActual" readonly="true" type="Text" style="width: 80%" hidden>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-2">
                                Estado :
                            </div>
                            <div class="col-md-4">
                                <select id="estado" class="selectpicker" style="width: 80%"></select>
                            </div>
                            <div class="col-md-2" hidden="true">
                                Causal :
                            </div>
                            <div class="col-md-4" hidden="true">
                                <select id="causal" class="selectpicker" style="width: 80%"></select>
                            </div>

                        </div>
                        <div class="row">
                            <div class="col-md-2">
                                Trazabilidad :
                            </div>
                            <div class="col-md-10">
                                <textarea id="trazabilidad" class="form-control" readonly="true" rows="5"></textarea>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-2">
                                Descripcion :
                            </div>
                            <div class="col-md-10">
                                <textarea id="descripcion_causal" class="form-control" rows="5"></textarea>
                            </div>
                        </div>



                    </fieldset>
                </div>
            </div>
        </div>

        <!-- **********************************************************************************************************************************************************
                                                                   Ventana de dialogos
       *********************************************************************************************************************************************************** -->
        <div>
            <div id="dialogLoading" style="display:none;">
                <p  style="font-size: 12px;text-align:justify;" id="msj2">Texto </p> <br/>
                <center>
                    <img src="./images/cargandoCM.gif"/>
                </center>
            </div>
            <!-- Dialogo de los jsp> -->
            <div id="dialogMsj" style="display:none;">
                <p style="font-size: 12px;text-align:justify;" id="msj" > Texto </p>
            </div>
        </div>




        <script type="text/javascript" src="./js/jquery/jquery.jqGrid-4.7.0/js/grid.locale-es.js"></script>
        <script type="text/javascript" src="./js/jquery/jquery.jqGrid-4.7.0/js/jquery.jqGrid.min.js"></script>
        <script type="text/javascript" src="./js/jquery/jquery.jqGrid-4.7.0/js/jquery.tabletojson.js"></script> 


        <!------------------------------------------------------------------------->
   

    </center>
    <div id="loader-wrapper">
        <div id="loader"></div>
        <img id="logowast" src="/fintra/images/Selectrik.png" style="z-index: 10002 !important">
        <div class="loader-section section-left">	
        </div>
        <div class="loader-section section-right"></div>
    </div>
</body>
</html>