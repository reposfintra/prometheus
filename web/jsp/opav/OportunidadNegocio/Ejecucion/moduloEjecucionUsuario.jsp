<%-- 
    Document   : modalidadProyecto
    Created on : 26/09/2016, 08:31:26 AM
    Author     : mmedina
--%>

<%@page contentType="text/html" pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
        <link href="./css/popup.css" rel="stylesheet" type="text/css">
        <link type="text/css" rel="stylesheet" href="./css/jquery/jquery-ui/jquery-ui.css" />
        <link type="text/css" rel="stylesheet" href="./css/TransportadorasApi.css " />
        <script type="text/javascript" src="./js/jquery/jquery.jqGrid/js/jquery.min.js"></script>
        <script type="text/javascript" src="./js/jquery/jquery-ui/jquery.ui.min.js"></script> 
        <script type="text/javascript" src="./js/jquery-ui-1.8.5.custom.min.js"></script>   
        <script type="text/javascript" src="./js/jquery/jquery.jqGrid-4.7.0/js/grid.locale-es.js"></script>
        <script type="text/javascript" src="./js/jquery/jquery.jqGrid-4.7.0/js/jquery.jqGrid.min.js"></script>
        <script type="text/javascript" src="./js/jquery/jquery.jqGrid-4.7.0/js/jquery.tabletojson.js"></script>    
        <script type="text/javascript" src="./js/OportunidadNegocio/ModuloEjecucionUsuario.js"></script> 
        <script type="text/javascript" src="./js/jquery/jquery.jqGrid-4.7.0/plugins/jquery.contextmenu.js">< /script

<!-------------------------------------Notificaciones----------------------------------------->
                    < link type = "text/css" rel = "stylesheet" href = "./css/toastr.min.css" / >
                    < script type = "text/javascript" src = "./js/toastr.min.js" ></script>
        <!-------------------------------------/Notificaciones----------------------------------------->

        <!----------------------------------------Pre-Load--------------------------------------------->
        <!--se necesita un div para que funcione el preload se llama loader-wrapper-->

        <link type="text/css" rel="stylesheet" href="./css/main2.css">

        <!----------------------------------------Pre-Load--------------------------------------------->
        <title>Ejecucion</title>
    </head>
    <body>
        <div id="capaSuperior" style="position:absolute; width:100%; height:100px; z-index:0; left: 0px; top: 0px;">
            <jsp:include page="/WEB-INF/toptsp.jsp?encabezado="/>
        </div>
    <center>

        <div id="tablita" class="ui-jqgrid ui-widget ui-widget-content center-block" style="width: 1024px; height-min: 300px;margin-top: 10px">
            <div class="ui-dialog-titlebar ui-widget-header ui-helper-clearfix">
                <span class="ui-dialog-title text-center" id="ui-dialog-title-listaGlobal">
                    &nbsp;
                    <label class="titulotablita" ><b>Proyectos Asignados</b></label>
                </span>
            </div>


            <!--            <table id="tablainterna" style="height: 53px; width: 615px"  >
                            <tr>
                                <td>
                                    <label>Id solictud</label>
                                </td>
                                <td>
                                    <input type="text"  id="id_solicitud"  style="width: 125px;margin-left: 5px;height: 15px;" readonly="true" value="">
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <label>Nombre del proyecto</label>
                                </td>
                                <td>
                                    <input type="text" id="nombre_proyecto"  readonly="true" style="width: 371px;margin-left: 5px;height: 15px;" >
                                </td>
                            </tr>
                        </table>-->
            <div style="margin: 15px 0px">
                <table id="tbl_Proyectos_Asignados" ></table>                
                <div id="page_tbl_Proyectos_Asignados"></div>
            </div>
        </div>

        <div id="dialogMsj" style="display:none;">
            <p style="font-size: 12px;text-align:justify;" id="msj" > Texto </p>
        </div>

        <div id="loader-wrapper">
            <div id="loader"></div>
            <img id="logowast" src="/fintra/images/Selectrik.png" style="z-index: 10002 !important">
            <div class="loader-section section-left">	
            </div>
            <div class="loader-section section-right"></div>
        </div>

    </center>

</body>
</html>
