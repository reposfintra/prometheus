<%-- 
    Document   : EjecucionApuInsumo
    Created on : 9/01/2017, 09:45:59 AM
    Author     : Ing.William Siado T
--%>

<%@page contentType="text/html" pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
        <title>Oportunidad De Negocio</title>
        <link href="./css/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css"/>

        <link rel="stylesheet" href="./css/jquery/jquery-ui/jquery-ui-1.10.2.custom.css" />
        <script type="text/javascript" src="./js/jquery/jquery.jqGrid/js/jquery.min.js"></script>   
        <script type="text/javascript" src="./js/jquery/jquery-1.10.2.min/jquery-1.9.1.js"></script>   
        <script type="text/javascript" src="./js/jquery/jquery-1.10.2.min/jquery-ui.min.js"></script>        

        <link href="./css/OportunidadNegocio.css" rel="stylesheet" type="text/css"/>

        <!--        <link type="text/css" rel="stylesheet" href="./css/buttons/botonVerde.css"/>-->
        <link type="text/css" rel="stylesheet" href="./css/EjecucionApuInsumo.css"/>
        <script type="text/javascript" src="./js/jquery/jquery.jqGrid-4.7.0/js/grid.locale-es.js"></script>
        <script type="text/javascript" src="./js/jquery/jquery.jqGrid-4.7.0/js/jquery.jqGrid.min.js"></script>
        <script type="text/javascript" src="./js/jquery/jquery.jqGrid-4.7.0/js/jquery.tabletojson.js"></script> 
        <link type="text/css" rel="stylesheet" href="./css/jquery/jquery.jqGrid/ui.jqgrid.css"/> 


        <script src="./js/OportunidadNegocio/Ejecucion_2.js" type="text/javascript"></script>


        <!----------------------------------------Pre-Load--------------------------------------------->
        <!--se necesita un div para que funcione el preload se llama loader-wrapper-->

        <link type="text/css" rel="stylesheet" href="./css/main2.css">

        <!----------------------------------------Pre-Load--------------------------------------------->


        <!-------------------------------------Notificaciones----------------------------------------->
        <link type="text/css" rel="stylesheet" href="./css/toastr.min.css"/>
        <script type="text/javascript" src="./js/toastr.min.js"></script>
        <!-------------------------------------/Notificaciones----------------------------------------->
        <%
            String num_solicitud = (request.getParameter("num_solicitud") != null) ? request.getParameter("num_solicitud") : "";
        %>
        <style>
            #div_cantidad_apu>table>tbody>tr>td{
                padding-left:  5px;
            }
        </style>
    </head>
    <body>
        <div id="capaSuperior"  style="position:absolute; width:100%; height:100px; z-index:0; left: 0px; top: 0px;" hidden="true">
            <jsp:include page="/WEB-INF/toptsp.jsp?encabezado=Ejecuci�n"/>
        </div>
        <input type="hidden" id="id_solicitud" name="id_solicitud" value="<%=num_solicitud%>" readOnly> 
        <input type="hidden" id="id_lote" name="id_lote" value="" > 
        <input type="hidden" id="id_tabla_ejecucion" name="id_tabla_ejecucion" value="" > 
        <input type="hidden" id="id_proceso" value="1" > 
        <input type="hidden" id="opcion_" value="1" > 

        <!-- **********************************************************************************************************************************************************
                                                                    Buscar APUS - Insumos
        *********************************************************************************************************************************************************** -->

        <div class="container">
            <div class="row">
                <section>
                    <div class="wizard">
                        
                        <div id="div_busqueda" class="ui-jqgrid ui-widget ui-widget-content center-block" style="width: 1317px; height-min: 300px;border-radius: 10px;border: 1px solid #2A88C8; margin: 10px auto; display: none">

                            <div class="row" style="padding: 5px;">
                                <div class="col-md-12 ">

                                    <fieldset class="scheduler-border" >
                                        <legend class="scheduler-border">Filtro de B�squeda</legend>
                                        <table id="tablainterna" style="height: 53px; width: 1271px"  >
                                            <tr>
                                                <td>
                                                    <label>Areas</label>
                                                </td>
                                                <td>
                                                    <select id="area" style="width: 151px;color: #050505;height: 25px;margin-left: 5px;" ></select>
                                                </td>
                                                <td>
                                                    <label>Disciplinas</label>
                                                </td>
                                                <td>
                                                    <select id="disciplina" style="width: 151px;color: #050505;height: 25px;margin-left: 5px;" ></select>
                                                </td>
                                                <td>
                                                    <label>Capitulos</label>
                                                </td>
                                                <td>
                                                    <select id="capitulo" style="width: 151px;color: #050505;height: 25px;margin-left: 5px;" ></select>
                                                </td>
                                                <td>
                                                    <label>Actividades</label>
                                                </td>
                                                <td>
                                                    <select id="actividad" style="width: 151px;color: #050505;height: 25px;margin-left: 5px;" ></select>
                                                </td>
                                                <td>
                                                    <div>
                                                        <center>
                                                            <button id='buscar_insumo_apu' onclick="cargarContenidoProyecto()" class="ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only" > 
                                                                <span class="ui-button-text">Buscar</span>
                                                            </button> 
                                                        </center>
                                                    </div>
                                                </td>
                                                <td>
                                            <center>
                                                <div class="round-button" style="margin-top: 1px" hidden>
                                                    <div class="round-button-circle" onclick="menu(2);">
                                                        <a href="#" title="Siguiente" >
                                                            <span class="round-w">
                                                                <i class="glyphicon glyphicon-menu-right"></i>
                                                            </span>
                                                        </a>
                                                    </div>
                                                </div>
                                            </center>
                                            </td>
                                            </tr>
                                        </table>
                                    </fieldset>
                                </div>
                            </div>
                        </div>


                        
                        <div class="tab-content">
                            <div class="tab-pane" role="tabpanel" id="div_General">

                                <center>
                                    <div style="position: relative;top: 5px; margin: 0 auto;">
                                        <table id="tabla_ejecucion" ></table>
                                        <div id="pager_tabla_ejecucion"></div>
                                    </div>
                                    <div id="div_tbl_ejecucion_insumos" hidden="true">
                                        <div id="div_cantidad_apu" style="border: 1px solid #2a88c8;border-radius: 10px; width: 700px;margin: 10px auto;" hidden>

                                            <table style="margin: auto">


                                                <tr>
                                                    <td><h6>Cantidad Apu :</h6></td>
                                                    <td><input type="text" id="txt_cantidad_apu" class="solo-numero" style="text-align: center"> </td>
                                                    <td><h6>% Avance :</h6></td>
                                                    <td><input type="text" id="txt_apu_equi" class="solo-numero" style="text-align: center"> </td>
                                                <input type="text" id="txt_cantidad_apu_" class="solo-numero" style="text-align: center"hidden="true"> 
                                                <td>
                                                    <button onclick="insertar_cantidades_actuales_grid()" class="ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only "  > 
                                                        <span class="ui-button-text">Aceptar</span>
                                                    </button>  
                                                </td>
                                                </tr>


                                            </table>

                                        </div>


                                        <table id="tbl_ejecucion_insumos" ></table>
                                        <div id="pager_tbl_ejecucion_insumos"></div>
                                    </div>
                                </center>

                            </div>


                        </div>
                        
                        <div id="div_tbl_ejecucion_apus"  style=" display: none; justify-content: flex-start ">
                            <div style="border: 1px solid #2a88c8;border-radius: 10px; width: 20%; margin: 10px;" >

                                <div style="width: 100%; margin: 2px;"><h4> Ubicacion de APU</h4></div>
                                <div  style="display: flex; justify-content: flex-start; flex-direction: column; margin: 0px 5px;">
                                    <div>                                        
                                        <div style="width: 30%; margin: 20px 3px 0px 0px;"><label>Areas</label></div>
                                        <select id="area_" style="width: 151px;color: #050505;height: 25px;margin-left: 5px;" ></select>
                                    </div>
                                    <div>                                        
                                        <div style="width: 30%; margin: 10px px 0px 0px;"><label>Disciplinas</label></div>
                                        <select id="disciplina_" style="width: 151px;color: #050505;height: 25px;margin-left: 5px;" ></select>
                                    </div>
                                    <div>                                        
                                        <div style="width: 30%; margin: 5px 3px 0px 0px;"><label>Capitulos</label></div>
                                        <select id="capitulo_" style="width: 151px;color: #050505;height: 25px;margin-left: 5px;" ></select>
                                    </div>
                                    <div>                                        
                                        <div style="width: 30%; margin: 5px 3px 0px 0px;"><label>Actividades</label></div>
                                        <select id="actividad_" style="width: 151px;color: #050505;height: 25px;margin-left: 5px;" ></select>
                                    </div>
                                    <div>
                                        <div style="width: 30%; margin: 2px;"><label> Cantidad</label></div>
                                        <input id="txt_cantidad_apu_eje" class="solo-numero" type="text" style="width: 100px;color: #050505;height: 25px;margin-left: 5px;" ></input>                                        
                                    </div>
                                </div>
                                
                            </div>
                            <div style=" margin:10px;  width: 75%;">
                                <center>
                                    <div id="filtros" class="ui-jqgrid ui-widget ui-widget-content ui-corner-all" style="height:84px;width: 430px;padding: 10px 10px 5px 10px ; margin-bottom: 15px; ">
                                        <center>

                                            <table style=" padding: 0.5em 1em 0.5em 1em">

                                                <input type="hidden" id="id" name="id" value="3666">

                                                <tbody><tr>
                                                    <td>Grupos APUs</td>
                                                    <td>
                                                        <select id="grupo_apu1" nombre="grupo_apu1" style="width: 264px;">
                                                            <option value="0" onclick="CargarGridApus();">TODOS</option>
                                                        </select>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td><label>Busqueda : </label></td>
                                                    <td><input type="text" id="filtro_nom_apu" style="width: 90%"></td>
                                                    <td> 
                                                        <button id="btn_filtro_apu" class="ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only" role="button" aria-disabled="false">
                                                            <span class="ui-button-text">Buscar</span>
                                                        </button> 
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td></td>
                                                    <td colspan="2">
                                                        <input type="checkbox" id="apu_proyecto"> <span style="margin-bottom: 1px">APUS DE PROYECTO  </span> 
                                                    </td>
                                                </tr>
                                            </tbody></table>
                                        </center>
                                    </div>  
                                    <div>
                                        <table id="tbl_ejecucion_apus" ></table>
                                        <div id="pager_tbl_ejecucion_apus"></div>
                                    </div>
                                </center>
                            </div>
                                
                        </div>
                        
                            <!---------------------------------------------------------------------------------------------------------->

                        <center>
                            <div style="position: relative;top: 5px; margin: 0 auto;">
                                <table id="tabla_ejecucion_1" ></table>
                                <div id="pager_tabla_ejecucion_1"></div>
                            </div>
                        </center>

                        <div class="tab-pane" role="tabpanel" id="div_actas" hidden="" >

                            <h3>Constitucion de Actas.</h3>
                            <p>Constitucion de Actas.</p>
                            <center>
                                <div style="position: relative;top: 5px; margin: 0 auto;">
                                    <table id="tbl_actas" ></table>
                                    <div id="pager_tbl_actas"></div>
                                </div>
                            </center>


                        </div>

                        <div class="tab-pane" role="tabpanel" id="div_actas_detalle" hidden="" >

                            <h3>Constitucion de Actas.</h3>
                            <p>Constitucion de Actas.</p>
                            <center>
                                <div style="position: relative;top: 5px; margin: 0 auto;">
                                    <table id="tabla_ejecucion_2" ></table>
                                    <div id="pager_tabla_ejecucion_2"></div>
                                </div>
                            </center>


                        </div>




                        <div class="clearfix"></div>
                    </div>
                </section>            
            </div>

            <div id="div_filtro_insumos"  style="display: none; width: 800px" >
                <div id="filtros" class="ui-jqgrid ui-widget ui-widget-content ui-corner-all" style="height: 130px;padding: 0px 10px 5px 10px">
                    <center>

                        <table id="tablabusqueda" style=" padding: 0.5em 1em 0.5em 1em" style="width: 95%;">

                            <input type="hidden" id="idregistro" name="idregistro">

                            <tr>
                                <td>Tipo Insumo :</td>
                                <td>
                                    <select id="tipo_insumo" nombre="tipo_insumo" onchange="limpiarCombos2();
                                            cargarCombo('categoria', [$('#tipo_insumo').val()]);" style="width: 160px;" >
                                        <option value='0'>...</option>
                                    </select>
                                </td>
                                <td>Unidad Medida :</td>
                                <td>
                                    <select id="_unidad_medida" nombre="_unidad_medida" style="width: 160px;" >
                                        <option value='0'>...</option>
                                    </select>
                                </td>
                            </tr>
                            <tr>
                                <td>Categoria :</td>
                                <td>
                                    <select id="categoria" nombre="categoria" onchange="cargarCombo('sub', [this.value])" style="width: 160px;" >
                                        <option value='0'>...</option>
                                    </select>
                                </td>
                                <td>SubCategoria :</td>
                                <td>
                                    <select id="sub"  style="width: 160px;" >
                                        <option value='0'>...</option>
                                    </select>

                                    <input type="hidden" name="nomsub" id="nomsub">
                                </td>
                            </tr>

                            <tr>
                                <td><label>Buqueda : </label></td>
                                <td colspan="3">
                                    <input type="text" id="filtro_insumo" style="width: 90%" />
                                    <img id = "btn_filtro_insumo" src = "/fintra/images/opav/enter.png"
                                         style = "margin-left: 5px; height: 19px; vertical-align: middle;" 
                                         onclick = "" title="Enter">
                                </td>

                            </tr>
                        </table>
                        <hr class="hrw">
                        <div id ='botones' hidden>
                            <button id="buscar" class="ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only" 
                                    role="button" aria-disabled="false">
                                <span class="ui-button-text">Buscar</span>
                            </button> 
                        </div>
                        <div style="height: 53px; width: 80%;display: flex;justify-content: center;">
                            <label style="padding-top: 2px">Cantidad de insumo : </label>
                                    <input id="txt_cantidad_insu_eje" class="solo-numero" type="text" style="width: 100px;color: #050505;height: 25px;margin-left: 5px;" ></input>                                        
                        </div>
                    </center>
                </div>  
                </br>  

                <!---------Grid que contiene filtro de insumos ----------->
                <div style="margin-top: 0px">
                    <center>
                        <table id="tbl_filtro_insumos" ></table>                
                        <div id="page_filtro_insumos"></div>
                    </center>
                </div>
                <!-------------------->

                <!--------div materiales------------>
                <div style="margin-top: 0px">
                    <center>
                        <table id="tbl_materiales" ></table>                
                        <div id="page_materiales"></div>
                    </center>
                </div>

            </div>

            <div id="div_impresiones"  style="display: none;padding: auto;" >

                <div style="margin: 10px">
                    <label>Imprimir :</label>   
                    <select id="cbx_impresion" style="width: 151px;color: #050505;height: 25px;margin-left: 5px;" >                                                                     
                        <option value='0'>Registro Diario</option>
                        <option value='1'>Liquidacion</option>

                    </select>
                </div>
            </div>


        </div>

        <!-- **********************************************************************************************************************************************************
                                                                   Ventana de dialogos
        *********************************************************************************************************************************************************** -->
        <div>
            <div id="dialogLoading" style="display:none;">
                <p  style="font-size: 12px;text-align:justify;" id="msj2">Texto </p> <br/>
                <center>
                    <img src="./images/cargandoCM.gif"/>
                </center>
            </div>
            <!-- Dialogo de los jsp> -->
            <div id="dialogMsj" style="display:none;">
                <p style="font-size: 12px;text-align:justify;" id="msj" > Texto </p>
            </div>
        </div>

        <div id="divSalidaEx" title="Exportacion" style=" display: block" >
            <p  id="msjEx" style=" display:  none"> Espere un momento por favor...</p>
            <center>
                <img id="imgloadEx" style="position: relative;  top: 7px; display: none " src="./images/cargandoCM.gif"/>
            </center>
            <div id="respEx" style=" display: none"></div>
        </div> 


        <div id="dialogMsgMeta" title="Mensaje" style="display:none; visibility: hidden">
            <p style="font-size: 12.5px;text-align:justify;" id="msj3" > Texto </p>
        </div>  







        <div id="loader-wrapper">
            <div id="loader"></div>
            <img id="logowast" src="/fintra/images/Selectrik.png" style="z-index: 10002 !important">
            <div class="loader-section section-left">	
            </div>
            <div class="loader-section section-right"></div>
        </div>
    </body>
</html>
