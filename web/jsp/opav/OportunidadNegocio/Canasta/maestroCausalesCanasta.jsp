<%-- 
    Document   : maestroCausalesCanasta
    Created on : 14/07/2017, 09:46:05 AM
    Author     : Ing.William A. Siado T.
--%>

<%@page contentType="text/html" pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
    <head>

        <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">



        <title>Oportunidad De Negocio</title>






        <link href="./css/bootstrap/css/bootsrtap-3.3.7.min.css" rel="stylesheet" type="text/css"/>
        <link type="text/css" rel="stylesheet" href="./css/jquery/jquery-ui/jquery-ui.css"/>     
        <script type="text/javascript" src="./js/jquery/jquery-ui/jquery.min.js"></script>
        <script type="text/javascript" src="./js/jquery/jquery-ui/jquery.ui.min.js"></script> 


        <!-------------------------------------JQGRID----------------------------------------->
        <link type="text/css" rel="stylesheet" href="./css/jquery/jquery.jqGrid/ui.jqgrid.css"/>
        <script type="text/javascript" src="./js/jquery/jquery.jqGrid-4.7.0/js/grid.locale-es.js"></script>
        <script type="text/javascript" src="./js/jquery/jquery.jqGrid-4.7.0/js/jquery.jqGrid.min.js"></script>
        <script type="text/javascript" src="./js/jquery/jquery.jqGrid-4.7.0/js/jquery.tabletojson.js"></script> 
        <!-------------------------------------/JQGRID----------------------------------------->

        <!-------------------------------------Notificaciones----------------------------------------->
        <link type="text/css" rel="stylesheet" href="./css/toastr.min.css"/>
        <script type="text/javascript" src="./js/toastr.min.js"></script>
        <!-------------------------------------/Notificaciones----------------------------------------->

        <!----------------------------------------Pre-Load--------------------------------------------->
        <!--se necesita un div para que funcione el preload se llama loader-wrapper-->

        <link type="text/css" rel="stylesheet" href="./css/main2.css">

        <!----------------------------------------Pre-Load--------------------------------------------->

        <script src="./js/OportunidadNegocio/maestroCausalesCanasta.js" type="text/javascript"></script>





    </head> 
    <body>

    <center>
        <jsp:include page="/WEB-INF/toptsp.jsp?encabezado=Maestro Causales Canasta"/>



        <!-- **********************************************************************************************************************************************************
                                                                    
        *********************************************************************************************************************************************************** -->
        <div class="container-fluid" > 

            <div class="row">
                <div class="col-md-12" style="padding-top: 50px">

                    <table id="tbl_causales_canasta" style=""></table>
                    <div id="pager_tbl_causales_canasta"></div>

                </div>
            </div>

        </div>

        <div id="divCausal"  style="display:none;">
            <form class="form-horizontal" id="form_causales_canasta">
                <input type="hidden" id="id_causal">
                <div class="form-group">
                    <label class="control-label col-sm-2" for="nombre">Nombre :</label>
                    <div class="col-sm-10">
                        <input type="text" class="form-control" id="nombre" placeholder="Nombre">
                    </div>
                </div>
                
                <div class="form-group">
                    <label class="control-label col-sm-2" for="descripcion">Descripcion :</label>
                    <div class="col-sm-10">
                        <textarea class="form-control" id="descripcion" rows="4" placeholder="Descripcion"></textarea>
                    </div>
                </div>
                                              
            </form> 
        </div>





        <!-- **********************************************************************************************************************************************************
                                                                   Ventana de dialogos
       *********************************************************************************************************************************************************** -->
        <div>
            <div id="dialogLoading" style="display:none;">
                <p  style="font-size: 12px;text-align:justify;" id="msj2">Texto </p> <br/>
                <center>
                    <img src="./images/cargandoCM.gif"/>
                </center>
            </div>
            <!-- Dialogo de los jsp> -->
            <div id="dialogMsj" style="display:none;">
                <p style="font-size: 12px;text-align:justify;" id="msj" > Texto </p>
            </div>
        </div>
         



        <!------------------------------------------------------------------------->


    </center>
    <div id="loader-wrapper">
        <div id="loader"></div>
        <img id="logowast" src="/fintra/images/Selectrik.png" style="z-index: 10002 !important">
        <div class="loader-section section-left">	
        </div>
        <div class="loader-section section-right"></div>
    </div>
</body>
</html>