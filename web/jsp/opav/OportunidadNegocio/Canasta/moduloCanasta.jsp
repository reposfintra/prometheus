<%@page contentType="text/html" pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html lang="ES">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
        <title>Bodegas</title>



        <link type="text/css" rel="stylesheet" href="./css/jquery/jquery-ui/jquery-ui.css"/>     
        <script type="text/javascript" src="./js/jquery/jquery-ui/jquery.min.js"></script>
        <script type="text/javascript" src="./js/jquery/jquery-ui/jquery.ui.min.js"></script> 
        <!--<link type="text/css" rel="stylesheet" href="https://code.jquery.com/ui/1.12.1/themes/ui-lightness/jquery-ui.css"/>     
        <script src="https://code.jquery.com/jquery-1.12.4.min.js" integrity="sha256-ZosEbRLbNQzLpnKIkEdrPv7lOy9C27hHQ+Xp8a4MxAQ=" crossorigin="anonymous"></script>
        <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.min.js" integrity="sha256-VazP97ZCwtekAsvgPBSUwPFKdrwD3unUfSGVYrahUqU=" crossorigin="anonymous"></script>-->

        <!-------------------------------------JQGRID----------------------------------------->
        <link type="text/css" rel="stylesheet" href="./css/jquery/jquery.jqGrid/ui.jqgrid.css"/>
        <script type="text/javascript" src="./js/jquery/jquery.jqGrid-4.7.0/js/grid.locale-es.js"></script>
        <script type="text/javascript" src="./js/jquery/jquery.jqGrid-4.7.0/js/jquery.jqGrid.min.js"></script>
        <script type="text/javascript" src="./js/jquery/jquery.jqGrid-4.7.0/js/jquery.tabletojson.js"></script> 
        <!-------------------------------------/JQGRID----------------------------------------->

        <!-------------------------------------Notificaciones----------------------------------------->
        <link type="text/css" rel="stylesheet" href="./css/toastr.min.css"/>
        <script type="text/javascript" src="./js/toastr.min.js"></script>
        <!-------------------------------------/Notificaciones----------------------------------------->

        <!----------------------------------------Pre-Load--------------------------------------------->
        <!--se necesita un div para que funcione el preload se llama loader-wrapper-->

        <link type="text/css" rel="stylesheet" href="./css/main2.css">

        <!----------------------------------------Pre-Load--------------------------------------------->

        <link href="./css/bootstrap/css/bootsrtap-3.3.7.min.css" rel="stylesheet" type="text/css"/>
        <script src="./js/OportunidadNegocio/moduloCanasta.js" type="text/javascript"></script>
        <link href="./css/OportunidadNegocio.css" rel="stylesheet" type="text/css"/>
        <link href="./css/opav/moduloCanasta.css" rel="stylesheet" type="text/css"/>

    </head>

    <body>

        <jsp:include page="/WEB-INF/toptsp.jsp?encabezado=Modulo Bodega"/>

        <input type="hidden" id="id_cliente" name="id_cliente">
        <!--        se ultiliza para la canasta cuando le dan abrir_canasta-->
        <input type="hidden" id="id_solicitud" name="id_solicitud" readOnly> 
        <!--        se ultiliza para la canasta cuando escogen un apu-->
        <input type="hidden" id="id_apu" name="id_apu" readOnly> 
        <!--        se ultiliza para la canasta cuando se escogen los insumos-->
        <input type="hidden" id="id_insumos" name="id_insumos" readOnly> 




        <div class="container-fluid">

            <!-- ********************************************************************************** ************************************************************************
                                                                                Buscar Solicitud
            *********************************************************************************************************************************************************** -->
            <div class="contenedorprincipal">
                <div id="contenedor1" class="contenedor show">
                    <div id="div_busqueda" class="ui-jqgrid ui-widget ui-widget-content center-block" style="width: 1317px; border-radius: 10px; height-min: 300px;margin: 10px auto;">

                        <div class="row" style="padding: 5px">
                            <div class="col-md-12 ">

                                <fieldset class="scheduler-border" >
                                    <legend class="scheduler-border"><h4>B�squeda De Solicitud</h4></legend>
                                    <table id="tablainterna" style="width: 1271px"  >
                                        <tr>
                                            <td>
                                                <label>Tipo De Proyecto</label>
                                            </td>
                                            <td>
                                                <select id="tipo_proyecto" style="width: 151px;color: #050505;" >
                                                    <option value=''></option>                                        
                                                    <option value='0'>Proyectos Transicion</option>
                                                    <option value='1'>Nuevos Proyectos</option>

                                                </select>
                                            </td>
                                            <td>
                                                <label>Linea Negocio</label>
                                            </td>
                                            <td>
                                                <select id="linea_negocio" style="width: 151px;color: #050505;" ></select>
                                            </td>
                                            <td>
                                                <label style="margin-left: 10px;">Solicitud</label>
                                            </td>
                                            <td>
                                                <input type="text" id="solicitud"  style="width: 80px;" >
                                            </td>
                                            <td>
                                                <label style="margin-left: 10px;">Foms</label>
                                            </td>
                                            <td>
                                                <input type="text" id="txt_foms"  style="width: 80px;" >
                                            </td>
                                            <td>
                                                <label>Nombre Cliente</label>
                                            </td>
                                            <td>
                                                <input type="text" id="txt_nom_cliente"  style="width: 80px;" >
                                            </td>
                                            <td>
                                                <label style="margin-left: 10px;">Nombre Proyecto</label>
                                            </td>
                                            <td>
                                                <input type="text" id="txt_nom_proyecto"  style="width: 80px;" >
                                            </td>
                                            <td>
                                                <label style="margin-left: 10px;"hidden="true">Responsable</label>
                                            </td>
                                            <td>
                                                <input type="text" id="responsable"  hidden="true" style="width: 139px;" >

                                            </td>

                                            <td>
                                                <label style="margin-left: 10px;" hidden="true"  >Estado cartera</label>
                                            </td>
                                            <td>
                                                <select id="estadocartera" hidden="true" style="width: 108px;color: #050505;" ></select>
                                            </td> 
                                            <td>
                                                <label style="margin-left: 10px;" hidden="true">Fecha validacion cartera</label>
                                            </td>
                                            <td>
                                                <input type="datetime" id="fechaini" hidden="true" style="width: 100px ;"  readonly>
                                            </td>
                                            <td>
                                                <input  type="datetime" id="fechafin" hidden="true" style="width: 100px;" readonly>
                                            </td>
                                            <td>
                                                <input type="text" id="etapaActual"  style="width: 80px;margin-left: 5px;" value="COMERCIAL" hidden >
                                            </td>
                                            <td>
                                                <div style="padding-top: 10px">
                                                    <center>
                                                        <button id='buscar_solicitud' class="ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only" > 
                                                            <span class="ui-button-text">Buscar</span>
                                                        </button> 

                                                    </center>
                                                </div>
                                            </td>
                                        </tr>
                                    </table>
                                </fieldset>
                            </div>
                        </div>
                    </div>

                    <div style="display: table ;margin: 35px auto;">
                        <table id="tabla_infoSolicitud"></table>
                        <div id="pager"></div>
                    </div>
                </div>


                <!-- **********************************************************************************************************************************************************
                                                                                    FIN Buscar Solicitud
                *********************************************************************************************************************************************************** -->

                <!-- **********************************************************************************************************************************************************
                                                                        Contenedor 2 - PERTENECE A LA OPCION APU
                *********************************************************************************************************************************************************** -->
                <div id="contenedor2" class="contenedor">

                    <div class="ui-jqgrid ui-widget ui-widget-content center-block" style="width: 1317px; height-min: 300px;border-radius: 10px;border: 1px solid #2A88C8; margin: 10px auto;">

                        <div class="row" style="padding: 5px;">
                            <div class="col-md-12 ">

                                <fieldset class="scheduler-border" >
                                    <legend class="scheduler-border"><h4>Filtro de B�squeda</h4></legend>
                                    <table id="tablainterna" style="height: 53px; width: 1271px"  >
                                        <tr>
                                            <td>
                                                <label>Areas</label>
                                            </td>
                                            <td>
                                                <select id="area" style="width: 151px;color: #050505;height: 25px;margin-left: 5px;" ></select>
                                            </td>
                                            <td>
                                                <label>Disciplinas</label>
                                            </td>
                                            <td>
                                                <select id="disciplina" style="width: 151px;color: #050505;height: 25px;margin-left: 5px;" ></select>
                                            </td>
                                            <td>
                                                <label>Capitulos</label>
                                            </td>
                                            <td>
                                                <select id="capitulo" style="width: 151px;color: #050505;height: 25px;margin-left: 5px;" ></select>
                                            </td>
                                            <td>
                                                <label>Actividades</label>
                                            </td>
                                            <td>
                                                <select id="actividad" style="width: 151px;color: #050505;height: 25px;margin-left: 5px;" ></select>
                                            </td>
                                            <td>
                                                <div>
                                                    <center>
                                                        <button id='buscar_insumo_apu' onclick="cargarContenidoProyecto(1)" class="ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only" > 
                                                            <span class="ui-button-text">Buscar</span>
                                                        </button> 
                                                    </center>
                                                </div>
                                            </td>
                                        </tr>
                                    </table>
                                </fieldset>
                            </div>
                        </div>
                    </div>

                    <div style="display: table ;margin: 35px auto;">
                        <table id="tabla_apu" ></table>
                        <div id="pager_tabla_apu"></div>
                    </div>


                </div>
                <!-- **********************************************************************************************************************************************************
                                                                                FIN Contenedor 2
                *********************************************************************************************************************************************************** -->



                <!-- **********************************************************************************************************************************************************
                                                                    Contenedor 3 - PERTENECE A LA OPCION APU
                *********************************************************************************************************************************************************** -->
                <div id="contenedor3" class="contenedor">
                    <div style="display: table ;margin: 35px auto;">
                        <table id="tabla_insumo" ></table>
                        <div id="pager_tabla_insumo"></div>
                    </div>
                </div>
                <!-- **********************************************************************************************************************************************************
                                                                                FIN Contenedor 3
                *********************************************************************************************************************************************************** -->



                <!-- **********************************************************************************************************************************************************
                                                                                 Contenedor 4
                *********************************************************************************************************************************************************** -->
                <div id="contenedor4" class="contenedor">
                    <div style="width: 60% ;border-radius: 10px; height-min: 300px;margin: 10px auto;">
                        <fieldset class="scheduler-border" >
                            <legend class="scheduler-border"><h4>Causales :</h4></legend>
                            <select id="cbx_causal"  class="form-control" >
                            </select>
                        </fieldset>
                    </div> 




                    <div style="display: table ;margin: 35px auto;">
                        <table id="tabla_apu_bloqueo" ></table>
                        <div id="pager_tabla_apu_bloqueo"></div>
                    </div>
                </div>
                <!-- **********************************************************************************************************************************************************
                                                                                FIN Contenedor 4
                *********************************************************************************************************************************************************** -->
                <!-- **********************************************************************************************************************************************************
                                                                    Contenedor 5 - PERTENECE A LA OPCION INSUMOS
                *********************************************************************************************************************************************************** -->
                <div id="contenedor5" class="contenedor">

                    <div class="ui-jqgrid ui-widget ui-widget-content center-block" style="width: 600px; height-min: 300px;border-radius: 10px;border: 1px solid #2A88C8; margin: 10px auto;">

                        <div class="row" style="padding: 5px;">
                            <div class="col-md-12 ">

                                <fieldset class="scheduler-border" >
                                    <legend class="scheduler-border"><h4>B�squeda</h4></legend>
                                    <label>Nombre de Insumo :</label>
                                    <input id="nom_insumo" type="text" size="48" style="text-align: center;text-transform:uppercase" placeholder="Insumo">
                                    <button id='buscar_insumo' class="ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only" > 
                                        <span class="ui-button-text">Buscar</span>
                                    </button> 
                                </fieldset>
                            </div>
                        </div>
                    </div>

                    <div style="display: table ;margin: 35px auto;">
                        <table id="tbl_insumos" ></table>
                        <div id="pager_tbl_insumos"></div>
                    </div>


                </div>
                <!-- **********************************************************************************************************************************************************
                                                                                FIN Contenedor 5
                *********************************************************************************************************************************************************** -->
                <!-- **********************************************************************************************************************************************************
                                                                    Contenedor 6 - PERTENECE A LA OPCION INSUMOS
                *********************************************************************************************************************************************************** -->
                <div id="contenedor6" class="contenedor">
                    <div class="ui-jqgrid ui-widget ui-widget-content center-block" style="width: 1317px; height-min: 300px;border-radius: 10px;border: 1px solid #2A88C8; margin: 10px auto; display: flex">
                        <div style="width: 57% ;border-radius: 10px; height-min: 300px;margin: 10px 4px; display: inline-block">
                            <fieldset class="scheduler-border" >
                                <legend class="scheduler-border">Descripcion Insumo :</legend>
                                <input id="id_insumo" hidden="true">
                                <input id="id_unidad_medida_insumo" hidden="true" disabled="true">
                                <table>
                                        <tr>
                                            <td>
                                                <label>Nombre :</label>
                                            </td>
                                
                                            <td colspan="3">
                                                <input id="descripcion_insumo" disabled="true" size="89px">
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <label>Codigo :</label>
                                            </td>
                                
                                            <td>
                                                <input id="codigo_insumo" disabled="true">
                                            </td>
                                            <td>
                                                <label>Und.Medida :</label>
                                            </td>
                                
                                            <td>
                                                <input id="nombre_unidad_insumo" disabled="true">
                                            </td>
                                        </tr>
                                </table>
                            </fieldset>
                        </div>

                        <div style="width: 42% ;border-radius: 10px; height-min: 300px;margin: 10px auto; display: inline-block ; margin-bottom : 22px">
                            <fieldset class="scheduler-border" >
                                <legend class="scheduler-border">Causales :</legend>
                                <select id="cbx_causal_2"  class="form-control" >
                                </select>
                            </fieldset>
                        </div>

                    </div>
                    <div style="display: table ;margin: 35px auto;">
                        <table id="tbl_apu_bloqueo" ></table>
                        <div id="pager_tbl_apu_bloqueo"></div>
                    </div>
                </div>
                <!-- **********************************************************************************************************************************************************
                                                                                FIN Contenedor 6
                *********************************************************************************************************************************************************** -->

            </div>


            <!-- **********************************************************************************************************************************************************
                                                                             loader-wrapper
                *********************************************************************************************************************************************************** -->
            <div id="loader-wrapper">
                <div id="loader"></div>
                <img id="logowast" src="/fintra/images/Selectrik.png" style="z-index: 10002 !important">
                <div class="loader-section section-left">	
                </div>
                <div class="loader-section section-right"></div>
            </div>


            <!-- **********************************************************************************************************************************************************
                                                                            FIN  loader-wrapper
            *********************************************************************************************************************************************************** -->

            <!-- **********************************************************************************************************************************************************
                                                                        Ventana cambio de etapa y estado de la Solicitud
            *********************************************************************************************************************************************************** -->

            <div id="cambioEtapa" style="display: none">
                <div class="row"style="padding: 5px">
                    <div class="col-md-2 ">
                        <fieldset class="scheduler-border" style="width: 720px">

                            <div class="row">
                                <div class="col-md-2">
                                    Idsolicitud :
                                </div>
                                <div class="col-md-4">
                                    <input type="text" id="idsolicitud" readonly>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-2">
                                    Etapa Actual:
                                </div>
                                <div class="col-md-4">
                                    <input id="etapa" type="Text" readonly="true" style="width: 80%">
                                    <input id="idetapa" hidden="true" type="Text" style="width: 80%">
                                </div>
                                <div class="col-md-2">
                                    Estado Actual:
                                </div>
                                <div class="col-md-4">
                                    <input id="estadoActual" readonly="true" type="Text" style="width: 80%">
                                    <input id="idestadoActual" readonly="true" type="Text" style="width: 80%" hidden>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-2">
                                    Estado :
                                </div>
                                <div class="col-md-4">
                                    <select id="estado" class="selectpicker" style="width: 80%"></select>
                                </div>
                                <div class="col-md-2" hidden="true">
                                    Causal :
                                </div>
                                <div class="col-md-4" hidden="true">
                                    <select id="causal" class="selectpicker" style="width: 80%"></select>
                                </div>

                            </div>
                            <div class="row">
                                <div class="col-md-2">
                                    Trazabilidad :
                                </div>
                                <div class="col-md-10">
                                    <textarea id="trazabilidad" class="form-control" readonly="true" rows="5"></textarea>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-2">
                                    Descripcion :
                                </div>
                                <div class="col-md-10">
                                    <textarea id="descripcion_causal" class="form-control" rows="5"></textarea>
                                </div>
                            </div>



                        </fieldset>
                    </div>
                </div>
            </div>

            <!-- **********************************************************************************************************************************************************
                                                                        FIN Ventana cambio de etapa y estado de la Solicitud
            *********************************************************************************************************************************************************** -->

        </div>


        <!-- **********************************************************************************************************************************************************
                                                                                 modo de liberacion
        *********************************************************************************************************************************************************** -->
        <div id="div_modo_liberacion">

            <select id="modo_liberacion" class="form-control" style="width: 300px;color: #050505; margin: 0 auto" >                                    
                <option value='0'>Insumos</option>
                <option value='1'>APU</option>
            </select>
        </div>      

        <!-- **********************************************************************************************************************************************************
                                                                                 modo de liberacion
        *********************************************************************************************************************************************************** -->

    </body>
</html>
