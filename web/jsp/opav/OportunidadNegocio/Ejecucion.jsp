<%-- 
    Document   : Ejecucion
    Created on : 28/06/2016, 05:50:29 PM
    Author     : egonzalez
--%>

<%@page contentType="text/html" pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
        <link href="./css/popup.css" rel="stylesheet" type="text/css">
        <link type="text/css" rel="stylesheet" href="./css/jquery/jquery-ui/jquery-ui.css" />
        <link type="text/css" rel="stylesheet" href="./css/TransportadorasApi.css " />
        <script type="text/javascript" src="./js/jquery/jquery.jqGrid/js/jquery.min.js"></script>
        <script type="text/javascript" src="./js/jquery/jquery-ui/jquery.ui.min.js"></script> 
        <script type="text/javascript" src="./js/jquery-ui-1.8.5.custom.min.js"></script>   
        <script type="text/javascript" src="./js/jquery/jquery.jqGrid-4.7.0/js/grid.locale-es.js"></script>
        <script type="text/javascript" src="./js/jquery/jquery.jqGrid-4.7.0/js/jquery.jqGrid.min.js"></script>
        <script type="text/javascript" src="./js/jquery/jquery.jqGrid-4.7.0/js/jquery.tabletojson.js"></script>    
        <script type="text/javascript" src="./js/OportunidadNegocio/Ejecucion.js"></script> 
        <script type="text/javascript" href="http://www.ddginc-usa.com/spanish/editcheck.js"></script>
        <script type="text/javascript" src="./js/jquery/jquery.jqGrid-4.7.0/plugins/jquery.contextmenu.js"></script>
        <title>Facturación </title>
    </head>
    <body>
        <div id="capaSuperior" style="position:absolute; width:100%; height:100px; z-index:0; left: 0px; top: 0px;">
            <jsp:include page="/WEB-INF/toptsp.jsp?encabezado=Facturación Y Anticipo"/>
        </div>
        <style>
            #gbox_tabla_facturacion{
                top: 45px; 

            }
            .editable{
                width: 90px !important;
            }
            #gbox_tabla_facturacion{
                top: 18px;
                margin-left: 10px;
            }
            #gbox_tabla_anticipo{
                margin-left: 63px !important;
                margin-top: 26px !important;
            }

        </style>
    <center>
        <%
            String num_solicitud = (request.getParameter("num_solicitud") != null) ? request.getParameter("num_solicitud") : "";
            String opcion_ = (request.getParameter("opcion_") != null) ? request.getParameter("opcion_") : "";
        %>
        <input type="hidden" id="id_cliente" name="id_cliente" >
        <input type="text" id="idsolicitud_" name="idsolicitud_" value="<%=num_solicitud%>">
        <input type="text" id="opcion_" name="opcion_" value="<%=opcion_%>">

        <div id="tablita" class="ui-jqgrid ui-widget ui-widget-content center-block" style="width: 1317px; height-min: 300px;margin-top: 10px" hidden="true">

            <div class="ui-dialog-titlebar ui-widget-header ui-helper-clearfix">
                <span class="ui-dialog-title text-center" id="ui-dialog-title-listaGlobal">
                    &nbsp;
                    <label class="titulotablita" ><b>Busqueda Solicitud</b></label>
                </span>
            </div>
            <div class="row" style="padding: 5px">
                <div class="col-md-12 ">
                    <fieldset class="scheduler-border" >
                        <legend class="scheduler-border" >Busqueda De Solicitud</legend>

                        <table id="tablainterna" style="height: 53px; width: 1271px"  >

                            <tr>
                                <td>
                                    <label>Tipo De Proyecto</label>
                                </td>
                                <td>
                                    <select id="tipo_proyecto" style="width: 151px;color: #050505;height: 25px;margin-left: 5px;" >
                                        <option value=''></option>                                        
                                        <option value='0'>Proyectos Transicion</option>
                                        <option value='1'>Nuevos Proyectos</option>

                                    </select>
                                </td>
                                <td>
                                    <label>Linea Negocio</label>
                                </td>
                                <td>
                                    <select id="linea_negocio" style="width: 151px;color: #050505;height: 25px;margin-left: 5px;" ></select>
                                </td>
                                <td>
                                    <label style="margin-left: 10px;">Solicitud</label>
                                </td>
                                <td>
                                    <input type="text" id="solicitud"  style="width: 80px;margin-left: 5px;height: 26px;" >
                                </td>
                                <td>
                                    <label style="margin-left: 10px;">Foms</label>
                                </td>
                                <td>
                                    <input type="text" id="txt_foms"  style="width: 80px;margin-left: 5px;height: 26px;" >
                                </td>
                                <td>
                                    <label style="margin-left: 10px;">Nombre Cliente</label>
                                </td>
                                <td>
                                    <input type="text" id="txt_nom_cliente"  style="width: 80px;margin-left: 5px;height: 26px;" >
                                </td>
                                <td>
                                    <label style="margin-left: 10px;">Nombre Proyecto</label>
                                </td>
                                <td>
                                    <input type="text" id="txt_nom_proyecto"  style="width: 80px;margin-left: 5px;height: 26px;" >
                                </td>
                                <td>
                                    <label style="margin-left: 10px;"hidden="true">Responsable</label>
                                </td>
                                <td>
                                    <input type="text" id="responsable"  hidden="true" style="width: 139px;margin-left: 5px;height: 26px;" >

                                </td>

                                <td>
                                    <label style="margin-left: 10px;" hidden="true"  >Estado cartera</label>
                                </td>
                                <td>
                                    <select id="estadocartera" hidden="true" style="width: 108px;color: #050505;height: 25px;margin-left: 5px;" ></select>
                                </td> 
                                <td>
                                    <label style="margin-left: 10px;" hidden="true">Fecha validacion cartera</label>
                                </td>
                                <td>
                                    <input type="datetime" id="fechaini" hidden="true" style="width: 100px ;height: 26px;"  readonly>
                                </td>
                                <td>
                                    <input  type="datetime" id="fechafin" hidden="true" style="width: 100px;height: 26px;" readonly>
                                </td>
                                <td>
                                    <input type="text" id="etapaActual"  style="width: 80px;margin-left: 5px;height: 26px;" value="EJECUCION" hidden >
                                </td>
                            </tr>


                            <tr style="height: 15px">
                                <td colspan="12">
                                    <div style="padding-top: 10px">

                                        <center>
                                            <button id="buscar" class="ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only" style="margin-left: 15px;" > 
                                                <span class="ui-button-text">Buscar</span>
                                            </button>  
                                        </center>
                                    </div>
                                </td>
                            </tr>
                        </table>
                    </fieldset>

                </div>
            </div>
        </div>
        <div style="position: relative;top: 135px;" hidden="true">
            <table id="tabla_infoSolicitud" ></table>
            <div id="pager"></div>
        </div>
        <div id="dialogMsj" style="display:none;">
            <p style="font-size: 12px;text-align:justify;" id="msj" > Texto </p>
        </div>
        <div id="menu" class="ContextMenu"style="display: none" hidden="true">
            <ul id="listopciones">
                <li id="cuadro_control" >
                    <span style="font-family:Verdan">Cuadro de control</span>
                </li>
                <li id="anticipo" >
                    <span style="font-family:Verdan">Anticipo</span>
                </li>
                <li id="facturacion" >
                    <span style="font-family:Verdan">Facturacion</span>
                </li>
            </ul>
        </div>
        <div id="dialogMsjCuadroControl" style="position: relative;margin-top: -7;"hidden="true">
            <div id="tablita" class="ui-jqgrid ui-widget ui-widget-content center-block" style="width: 699px; height-min: 300px;margin-top: 10px" >
                <div class="ui-dialog-titlebar ui-widget-header ui-helper-clearfix">
                    <span class="ui-dialog-title text-center" id="ui-dialog-title-listaGlobal">
                        &nbsp;
                        <label class="titulotablita" ><b>CUADRO DE CONTROL</b></label>
                    </span>
                </div>
                <table id="tablainterna" style="height: 53px; width: 653px">
                    <tr>
                        <td>
                            <label> % Anticipo<span id="req_porc_anticipo" style="color:red;display:none;">*</span></label> 
                        </td>
                        <td>
                            <input type="text" name="porc_anticipo" id="porc_anticipo" class="solo-numeric" value="0" readonly style="width:90px;"/><span>%</span> 
                        </td>  
                        <td>
                            <label style="margin-left: 20px;"> Valor Anticipo</label> 
                        </td>
                        <td>
                            <input type="text" name="valor_anticipo" id="valor_anticipo" class="solo-numero" />
                        </td>
                        <td>
                            <label style="margin-left: 20px;"> % Retegarantía</label> 
                        </td>
                        <td>
                            <input type="text" name="porc_retegarantia" id="porc_retegarantia" class="solo-numeric" value="0"  style="width:90px;"/><span>%</span> 
                        </td> 
                    </tr>
                </table>
            </div>
            <div style="position: relative;top: 177px;" >
                <table id="tabla_cuadroControl" style="top: 37px;"></table>
                <div id="pager1"></div>
            </div>
        </div>

        <div id="dialogMsjFacturacion" style="position: relative;margin-top: -7;"hidden="true" >
            <div id="tablita" class="ui-jqgrid ui-widget ui-widget-content center-block" style="width: 1317px; height-min: 300px;margin-top: 10px" >
                <div class="ui-dialog-titlebar ui-widget-header ui-helper-clearfix">
                    <span class="ui-dialog-title text-center" id="ui-dialog-title-listaGlobal">
                        &nbsp;
                        <label class="titulotablita" id="lb_facturacion" ><b></b></label>
                    </span>
                </div>
                <table id="tablainterna" style="height: 53px; width: 1271px">
                    <tr>
                        <td>
                            Precio venta:
                        </td>
                        <td>
                            <input id="val_total" type="text" disabled="true" placeholder="" style="width: 110px" >
                        </td>
                        <td>
                            <label>Anticipo:</label>
                        </td>
                        <td>
                            <input id="anticipo_f" disabled="true" type="text" onchange="habdesRet()" style="width: 60px; margin-left: -4px" >
                        </td>
                        <td>
                            <label>% Anticipo: </label>
                        </td>
                        <td>
                            <input id="perc_anticipo" disabled onkeyup="resta();
                                    calculateAmortizacion();

                                    calculateFooter();" maxlength="2" onchange="resta()" type="text" placeholder="" style="width: 35px;" >
                        </td>
                        <td>
                            <label>Valor Anticipo:</label>
                        </td>
                        <td>
                            <input id="val_anticipo" type="text" disabled="true" placeholder="" style="width: 110px;" >
                        </td>
                        <td>
                            <label>Retegarantia:</label>
                        </td>
                        <td>
                            <input id="retegarantia" disabled type="text" onchange="habdesRet()" style="width: 60px; margin-left: -4px" >
                        </td>  
                        <td>
                            <label>% Retegarantia: </label>
                        </td>
                        <td>
                            <input id="perc_rete" disabled type="text" placeholder="" onkeyup="calculateretegarantia()" style="width: 35px;" >
                        </td>
                        <td>
                            <label>Valor Retegarantia:</label>
                        </td>
                        <td>
                            <input id="val_retegarantia" type="text" disabled="true" placeholder="" style="width: 110px;" >
                        </td>
                    </tr>
                </table>
            </div>
            <div style="position: relative;top: 177px;" >
                <table id="tabla_facturacion" style="top: 37px;"></table>
                <div id="pager2"></div>
            </div>
        </div>

        <div id="info"  class="ventana" >
            <p id="notific">EXITO AL GUARDAR</p>
        </div>
        <div id="menu2" class="ContextMenu"style="" hidden="true">
            <ul id="listopciones">
                <li id="eliminar" >
                    <span style="font-family:Verdan">Eliminar fila</span>
                </li>
            </ul>

        </div>

        <div id="dialogMsjAnticipo" style="position: relative;margin-top: -7;" hidden="true">
            <div id="tablita" class="ui-jqgrid ui-widget ui-widget-content center-block" style="width: 1467px; height-min: 300px;margin-top: 10px" >
                <div class="ui-dialog-titlebar ui-widget-header ui-helper-clearfix">
                    <span class="ui-dialog-title text-center" id="ui-dialog-title-listaGlobal">
                        &nbsp;
                        <label class="titulotablita" ><b>ANTICIPOS</b></label>
                    </span>
                </div>
                <table id="tablainterna" style="height: 53px; width: 1462px">
                    <tr>
                        <td>
                            <label>Nombre del Proyecto</label>
                        </td>
                        <td colspan="3">
                            <input id="nombre_proyecto_" type="text" disabled="true" style="width: 329px;" >
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <label>Codig Cliente:</label>
                        </td>
                        <td>
                            <input id="codcli_" type="text" disabled="true" style="width: 110px;" >
                        </td>
                        <td>
                            <label>Nombre cliente:</label>
                        </td>
                        <td>
                            <input id="nomcli_" type="text" disabled="true"  style="width: 110px;" >
                        </td>
                        <td>
                            <label>Codigo Cotizacion:</label>
                        </td>
                        <td>
                            <input id="codcot_" type="text" disabled="true" style="width: 110px;" >
                        </td>
                        <td>
                            <label>Valor Subtotal</label>
                        </td>
                        <td>
                            <input id="val_subtotal_" type="text" disabled="true" style="width: 110px;" >
                        </td>
                        <td>
                            <label>Anticipo:</label>
                        </td>
                        <td>
                            <select id="anticipo_" nombre="anticipo_" onchange="habdesAnt();" style="width: 78px" >
                                <option value='1'>Si</option>
                                <option value='2' selected>No</option>
                            </select>
                        </td>
                        <td>
                            <label>% Anticipo: </label>
                        </td>
                        <td>
                            <input id="perc_anticipo_" disabled onkeyup="calculateFooter();" maxlength="2" onchange="" type="text" placeholder="" style="width: 110px;" >
                        </td>
                        <td>
                            <label>Valor Anticipo:</label>
                        </td>
                        <td>
                            <input id="val_anticipo_" type="text" disabled="true" placeholder="" style="width: 110px;" >
                        </td>
                    </tr>
                </table>
            </div>
            <div style="position: relative;top: 177px;" >
                <table id="tabla_anticipo" style="top: 37px;"></table>
                <div id="pager3"></div>
            </div>
        </div>

        <div id="dialogEmpresa"  class="ventana" >
            <div id="tablainterna" style="width: 492px;" >
                <table id="tablainterna"  >
                    <tr>
                        <td>
                            <label>Empresa</label>
                            <select id="empresa">
                                <option value="fintra">Fintra</option>
                                <option value="selectrik">Selectrik</option>
                            </select>
                            <label>Firma</label>
                            <select id="firma">
                                <option value="jgomez">Jose Luis Gomez</option>
                                <option value="cramos">Carlos Ramos Fontalvo</option>
                            </select>
                            <label>Cuenta</label>
                            <select id="cuenta">
                                <option value="cuentaFintra">Fintra</option>
                                <option value="cuentaSelectrik">Selectrik</option>
                            </select>
                        </td>
                    </tr>
                </table>
            </div>
        </div>
    </center>
</body>
</html>
