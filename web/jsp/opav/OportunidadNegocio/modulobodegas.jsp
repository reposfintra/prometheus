<%-- 
    Document   : modulobodegas
    Created on : 20/06/2017, 10:25:58 AM
    Author     : Ing.William A. Siado Torres
--%>

<%@page contentType="text/html" pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html lang="ES">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
        <title>Bodegas</title>


        <link type="text/css" rel="stylesheet" href="./css/jquery/jquery-ui/jquery-ui.css"/>     
        <script type="text/javascript" src="./js/jquery/jquery-ui/jquery.min.js"></script>
        <script type="text/javascript" src="./js/jquery/jquery-ui/jquery.ui.min.js"></script> 


        <!-------------------------------------JQGRID----------------------------------------->
        <link type="text/css" rel="stylesheet" href="./css/jquery/jquery.jqGrid/ui.jqgrid.css"/>
        <script type="text/javascript" src="./js/jquery/jquery.jqGrid-4.7.0/js/grid.locale-es.js"></script>
        <script type="text/javascript" src="./js/jquery/jquery.jqGrid-4.7.0/js/jquery.jqGrid.min.js"></script>
        <script type="text/javascript" src="./js/jquery/jquery.jqGrid-4.7.0/js/jquery.tabletojson.js"></script> 
        <!-------------------------------------/JQGRID----------------------------------------->

        <!-------------------------------------Notificaciones----------------------------------------->
        <link type="text/css" rel="stylesheet" href="./css/toastr.min.css"/>
        <script type="text/javascript" src="./js/toastr.min.js"></script>
        <!-------------------------------------/Notificaciones----------------------------------------->

        <!----------------------------------------Pre-Load--------------------------------------------->
        <!--se necesita un div para que funcione el preload se llama loader-wrapper-->

        <link type="text/css" rel="stylesheet" href="./css/main2.css">

        <!----------------------------------------Pre-Load--------------------------------------------->

        <link href="./css/bootstrap/css/bootsrtap-3.3.7.min.css" rel="stylesheet" type="text/css"/>
        <script src="./js/OportunidadNegocio/modulobodegas.js" type="text/javascript"></script>
        <link href="./css/OportunidadNegocio.css" rel="stylesheet" type="text/css"/>

    </head>

    <body>

        <jsp:include page="/WEB-INF/toptsp.jsp?encabezado=Modulo Bodega"/>

        <input type="hidden" id="id_cliente" name="id_cliente">

        <div class="container-fluid">

            <!-- **********************************************************************************************************************************************************
                                                                                Buscar Solicitud
            *********************************************************************************************************************************************************** -->
            <div id="div_busqueda" class="ui-jqgrid ui-widget ui-widget-content center-block" style="width: 1317px; height-min: 300px;margin: 10px auto; mar">

                <div class="row" style="padding: 5px">
                    <div class="col-md-12 ">

                        <fieldset class="scheduler-border" >
                            <legend class="scheduler-border"><h4>B�squeda De Solicitud</h4></legend>
                            <table id="tablainterna" style="width: 1271px"  >
                                <tr>
                                    <td>
                                        <label>Tipo De Proyecto</label>
                                    </td>
                                    <td>
                                        <select id="tipo_proyecto" style="width: 151px;color: #050505;" >
                                            <option value=''></option>                                        
                                            <option value='0'>Proyectos Transicion</option>
                                            <option value='1'>Nuevos Proyectos</option>

                                        </select>
                                    </td>
                                    <td>
                                        <label>Linea Negocio</label>
                                    </td>
                                    <td>
                                        <select id="linea_negocio" style="width: 151px;color: #050505;" ></select>
                                    </td>
                                    <td>
                                        <label style="margin-left: 10px;">Solicitud</label>
                                    </td>
                                    <td>
                                        <input type="text" id="solicitud"  style="width: 80px;" >
                                    </td>
                                    <td>
                                        <label style="margin-left: 10px;">Foms</label>
                                    </td>
                                    <td>
                                        <input type="text" id="txt_foms"  style="width: 80px;" >
                                    </td>
                                    <td>
                                        <label>Nombre Cliente</label>
                                    </td>
                                    <td>
                                        <input type="text" id="txt_nom_cliente"  style="width: 80px;" >
                                    </td>
                                    <td>
                                        <label style="margin-left: 10px;">Nombre Proyecto</label>
                                    </td>
                                    <td>
                                        <input type="text" id="txt_nom_proyecto"  style="width: 80px;" >
                                    </td>
                                    <td>
                                        <label style="margin-left: 10px;"hidden="true">Responsable</label>
                                    </td>
                                    <td>
                                        <input type="text" id="responsable"  hidden="true" style="width: 139px;" >

                                    </td>

                                    <td>
                                        <label style="margin-left: 10px;" hidden="true"  >Estado cartera</label>
                                    </td>
                                    <td>
                                        <select id="estadocartera" hidden="true" style="width: 108px;color: #050505;" ></select>
                                    </td> 
                                    <td>
                                        <label style="margin-left: 10px;" hidden="true">Fecha validacion cartera</label>
                                    </td>
                                    <td>
                                        <input type="datetime" id="fechaini" hidden="true" style="width: 100px ;"  readonly>
                                    </td>
                                    <td>
                                        <input  type="datetime" id="fechafin" hidden="true" style="width: 100px;" readonly>
                                    </td>
                                    <td>
                                        <input type="text" id="etapaActual"  style="width: 80px;margin-left: 5px;" value="COMERCIAL" hidden >
                                    </td>
                                    <td>
                                        <div style="padding-top: 10px">
                                            <center>
                                                <button id='buscar_solicitud' class="ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only" > 
                                                    <span class="ui-button-text">Buscar</span>
                                                </button> 

                                            </center>
                                        </div>
                                    </td>
                                </tr>
                            </table>
                        </fieldset>
                    </div>
                </div>
            </div>

            <div style="display: table ;margin: 35px auto;">
                <table id="tabla_infoSolicitud"></table>
                <div id="pager"></div>
            </div>

            <!-- **********************************************************************************************************************************************************
                                                                                FIN Buscar Solicitud
            *********************************************************************************************************************************************************** -->

            <!-- **********************************************************************************************************************************************************
                                                                                Formulario Bodega
            *********************************************************************************************************************************************************** -->



            <div id="div_tabla_bodegas" style="display: none">

                <div class="container-fluid">
                    <table id="tabla_bodegas"></table>
                    <div id="pager_tabla_bodegas"></div>
                </div>

                <div class="container-fluid">

                    <form name="bodega" method="POST" class="form-horizontal">

                        <input type="hidden" id="id_solicitud" name="id_solicitud">
                        <input type="hidden" id="id_bodega" name="id_bodega">


                        <h4>Formulario de Bodegas</h4> 
                        <hr>
                        <div class="form-group">
                            <label class="col-md-3 control-label">Departamento:</label>
                            <div class="col-md-9">
                                <select id="dep_dir" name="dep_dir" class="form-control input-sm"></select>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-md-3 control-label">Ciudad :</label>
                            <div class="col-md-9">
                                <select id="ciu_dir" name="ciu_dir" class="form-control input-sm"></select>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-md-3 control-label">Via Principal :</label>
                            <div class="col-md-5">
                                <select id="via_princip_dir" onchange="setDireccion(2)" class="form-control input-sm"></select>
                            </div>
                            <div class="col-md-4">
                                <input type="text" id="nom_princip_dir" class="form-control input-sm" style="width: 100%;" onkeyup="setDireccion(1)"/>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-md-3 control-label">Numero :</label>
                            <div class="col-md-5">
                                <select id="via_genera_dir" class="form-control input-sm" onkeyup="setDireccion(1)"  ></select>
                            </div>


                            <div class="col-md-2">
                                <input type="text" id="nom_genera_dir" class="form-control input-sm"style="width: 100%;" onkeyup="setDireccion(1)"/>
                            </div>

                            <div class="col-md-2">
                                <input type="text" id="placa_dir" class="form-control input-sm" style="width: 100%;" onkeyup="setDireccion(1)"/>
                            </div>


                        </div>

                        <div class="form-group">
                            <label class="col-md-3 control-label">Complemento :</label>
                            <div class="col-md-8">
                                <input type="text" id="cmpl_dir" class="form-control input-sm" onkeyup="setDireccion(1)" onchange="setDireccion(1)"/>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-md-3 control-label">Direccion :</label>
                            <div class="col-md-9">
                                <input type="text" id="dir_resul" class="form-control input-sm" style="width: 100%;" readOnly/>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-md-3 control-label">Descripcion :</label>
                            <div class="col-md-9">
                                <textarea rows ="3" id="desc_bodega" name="descripcion" class="form-control input-sm" placeholder="Descripcion de bodega." maxlength="100"></textarea>
                            </div>
                        </div>
                        
                        <div class="form-group">
                            <label class="col-md-3 control-label">Nombre de contacto: </label>
                            <div class="col-md-9">
                                <input type="text" id="nombre_contacto" class="form-control input-sm" style="width: 100%;"/>
                            </div>
                        </div>       

                        <div class="form-group">
                            <label class="col-md-3 control-label">Cargo de contacto: </label>
                            <div class="col-md-9">
                                <input type="text" id="cargo_contacto" class="form-control input-sm" style="width: 100%;"/>
                            </div>
                        </div>                
                        
                        <div class="form-group">
                            <label class="col-md-3 control-label">Telefono 1: </label>
                            <div class="col-md-9">
                                <input type="text" id="telefono1_contacto" class="form-control input-sm" style="width: 100%;"/>
                            </div>
                        </div>                
                        
                        <div class="form-group">
                            <label class="col-md-3 control-label">Telefono 2: </label>
                            <div class="col-md-9">
                                <input type="text" id="telefono2_contacto" class="form-control input-sm" style="width: 100%;"/>
                            </div>
                        </div>   
                    </form>
                </div>
            </div>


            <!-- **********************************************************************************************************************************************************
                                                                                FIN Formulario Bodega
            *********************************************************************************************************************************************************** -->



            <div id="loader-wrapper">
                <div id="loader"></div>
                <img id="logowast" src="/fintra/images/Selectrik.png" style="z-index: 10002 !important">
                <div class="loader-section section-left">	
                </div>
                <div class="loader-section section-right"></div>
            </div>


            <!-- **********************************************************************************************************************************************************
                                                                        Ventana cambio de etapa y estado de la Solicitud
            *********************************************************************************************************************************************************** -->

            <div id="cambioEtapa" style="display: none">
                <div class="row"style="padding: 5px">
                    <div class="col-md-2 ">
                        <fieldset class="scheduler-border" style="width: 720px">

                            <div class="row">
                                <div class="col-md-2">
                                    Idsolicitud :
                                </div>
                                <div class="col-md-4">
                                    <input type="text" id="idsolicitud" readonly>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-2">
                                    Etapa Actual:
                                </div>
                                <div class="col-md-4">
                                    <input id="etapa" type="Text" readonly="true" style="width: 80%">
                                    <input id="idetapa" hidden="true" type="Text" style="width: 80%">
                                </div>
                                <div class="col-md-2">
                                    Estado Actual:
                                </div>
                                <div class="col-md-4">
                                    <input id="estadoActual" readonly="true" type="Text" style="width: 80%">
                                    <input id="idestadoActual" readonly="true" type="Text" style="width: 80%" hidden>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-2">
                                    Estado :
                                </div>
                                <div class="col-md-4">
                                    <select id="estado" class="selectpicker" style="width: 80%"></select>
                                </div>
                                <div class="col-md-2" hidden="true">
                                    Causal :
                                </div>
                                <div class="col-md-4" hidden="true">
                                    <select id="causal" class="selectpicker" style="width: 80%"></select>
                                </div>

                            </div>
                            <div class="row">
                                <div class="col-md-2">
                                    Trazabilidad :
                                </div>
                                <div class="col-md-10">
                                    <textarea id="trazabilidad" class="form-control" readonly="true" rows="5"></textarea>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-2">
                                    Descripcion :
                                </div>
                                <div class="col-md-10">
                                    <textarea id="descripcion_causal" class="form-control" rows="5"></textarea>
                                </div>
                            </div>



                        </fieldset>
                    </div>
                </div>
            </div>

            <!-- **********************************************************************************************************************************************************
                                                                        FIN Ventana cambio de etapa y estado de la Solicitud
            *********************************************************************************************************************************************************** -->

        </div>
    </body>
</html>
