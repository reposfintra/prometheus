<%-- 
    Document   : modalidadProyecto
    Created on : 26/09/2016, 08:31:26 AM
    Author     : mmedina
--%>

<%@page contentType="text/html" pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
        <link href="./css/popup.css" rel="stylesheet" type="text/css">
        <link type="text/css" rel="stylesheet" href="./css/jquery/jquery-ui/jquery-ui.css" />
        <link type="text/css" rel="stylesheet" href="./css/TransportadorasApi.css " />
        <script type="text/javascript" src="./js/jquery/jquery.jqGrid/js/jquery.min.js"></script>
        <script type="text/javascript" src="./js/jquery/jquery-ui/jquery.ui.min.js"></script> 
        <script type="text/javascript" src="./js/jquery-ui-1.8.5.custom.min.js"></script>   
        <script type="text/javascript" src="./js/jquery/jquery.jqGrid-4.7.0/js/grid.locale-es.js"></script>
        <script type="text/javascript" src="./js/jquery/jquery.jqGrid-4.7.0/js/jquery.jqGrid.min.js"></script>
        <script type="text/javascript" src="./js/jquery/jquery.jqGrid-4.7.0/js/jquery.tabletojson.js"></script>    
        <script type="text/javascript" src="./js/OportunidadNegocio/modalidadProyecto.js"></script> 
        <script type="text/javascript" src="./js/jquery/jquery.jqGrid-4.7.0/plugins/jquery.contextmenu.js"></script>
               <!----------------------------------------Pre-Load--------------------------------------------->
        <!--se necesita un div para que funcione el preload se llama loader-wrapper-->

        <link type="text/css" rel="stylesheet" href="./css/main2.css">

        <!----------------------------------------Pre-Load--------------------------------------------->
        <title>COTIZACION DEFINITIVA</title>
    </head>
    <body>
        <div id="capaSuperior" style="position:absolute; width:100%; height:100px; z-index:0; left: 0px; top: 0px;">
            <jsp:include page="/WEB-INF/toptsp.jsp?encabezado=COTIZACION DEFINITIVA"/>
        </div>
        <style>
            .onoffswitch-inner::before {
                content: "SI";
                padding-left: 10px;

            }
        </style>
    <center>
        <%
            String num_solicitud = (request.getParameter("num_solicitud") != null) ? request.getParameter("num_solicitud") : "";
        %>
        <div id="tablita" class="ui-jqgrid ui-widget ui-widget-content center-block" style="width: 645px; height-min: 300px;margin-top: 10px">
            <div class="ui-dialog-titlebar ui-widget-header ui-helper-clearfix">
                <span class="ui-dialog-title text-center" id="ui-dialog-title-listaGlobal">
                    &nbsp;
                    <label class="titulotablita" ><b>SOLICITUD</b></label>
                </span>
            </div>
            <table id="tablainterna" style="height: 53px; width: 615px"  >
                <tr>
                    <td>
                        <label>Id solictud</label>
                    </td>
                    <td>
                        <input type="text"  id="id_solicitud"  style="width: 125px;margin-left: 5px;height: 15px;" readonly="true" value="<%=num_solicitud%>">
                    </td>
                    <!--                    <td rowspan="2">
                                            <hr style="width: 2px;height: 60px;top: -25px;margin-left: 13px;color: rgba(128, 128, 128, 0.39);" /> 
                                        </td>
                                        <td rowspan="2">
                                            <div style="padding-top: 10px">
                                                <button id="buscar" class="ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only" style="margin-left: 15px;top: -7px;" > 
                                                    <span class="ui-button-text">Buscar</span>
                                                </button>  
                                            </div>
                                        </td>-->
                </tr>
                <tr>
                    <td>
                        <label>Nombre del proyecto</label>
                    </td>
                    <td>
                        <input type="text" id="nombre_proyecto"  readonly="true" style="width: 371px;margin-left: 5px;height: 15px;" >
                    </td>
                </tr>
            </table>
        </div>
        <div id="tablita" class="ui-jqgrid ui-widget ui-widget-content center-block" style="width: 430px; height-min: 300px;margin-top: 26px;padding: 8px;">
            <table id="tablainterna" style="height: 53px; width: 370px"  >
                <tr>
                    <td>
                        <label>Costo proyecto</label>
                    </td>
                    <td>
                        <input type="text" id="costo_proyecto"  style="width: 189px;margin-left: 5px;height: 15px;" readonly>
                    </td>
                </tr>
                <tr>
                    <td>
                        <label>Rentabilidad contratista</label>
                    </td>
                    <td>
                        <input type="text" id="rentabilidad_contratista"  style="width: 189px;margin-left: 5px;height: 15px;" readonly>
                    </td>
                </tr>
                <tr>
                    <td>
                        <label>Rentabilidad esquema</label>
                    </td>
                    <td>
                        <input type="text" id="rentabilidad_esquema"  style="width: 189px;margin-left: 5px;height: 15px;"readonly >
                    </td>
                </tr>
                <tr>
                    <td>
                        <label style="font-weight: bold">SubTotal</label>
                    </td>
                    <td>
                        <input type="text" id="subtotal"  style="width: 189px;margin-left: 5px;height: 15px;font-weight: bold" readonly>
                    </td>
                </tr>
                <tr>
                    <td>
                        <label>C.I.A</label>
                    </td>
                    <td>
                        <input type="text" id="cia_t" style="width: 70px;margin-left: 5px;height: 15px;" hidden="true">
                        <input type="text" id="cia"  style="width: 70px;margin-left: 5px;height: 15px;" readonly>
                        <label>Ver en detalle</label>
                        <div class='onoffswitch' style="margin-top: -22px;margin-left: 165px;">
                            <input type='checkbox' name='onoffswitch' class='onoffswitch-checkbox' id='myonoffswitch_' style='width: 51px;' value='cambio'> 
                            <label class='onoffswitch-label' for='myonoffswitch_' id="detalle">
                                <span class='onoffswitch-inner' ></span>
                                <span class='onoffswitch-switch' style ='height: 15px;'></span> 
                            </label>
                        </div>
                        <input type="text" id="perc_cia"  style="width: 189px;margin-left: 5px;height: 15px;font-weight: bold" hidden="true">
                    </td>
                </tr>
                <tr class="lab">
                    <td>
                        <label>IVA Material</label>
                    </td>
                    <td>
                        <input type="text" id="iva_material"  style="width: 189px;margin-left: 5px;height: 15px;" readonly>
                    </td>
                </tr>
                <!--                <tr class="lab">
                                    <td>
                                        <label>IVA Compensar</label>
                                    </td>
                                    <td>
                                        <input type="text" id="iva_compensar"  style="width: 189px;margin-left: 5px;height: 15px;" readonly>
                                    </td>
                                </tr>-->
                <tr class="lab">
                    <td>
                        <label style="font-weight: bold">SubTotal</label>

                    </td>
                    <td>
                        <input type="text" id="subtotal11"  style="width: 189px;margin-left: 5px;height: 15px;font-weight: bold" readonly>
                    </td>
                </tr>
                <tr class="lab">
                    <td>
                        <label>Iva Compensado :</label>
                    </td>
                    <td>
                        <input type="text" id="porc_iva_compensado"  style="width: 25px;margin-left: 5px;height: 15px;" value="3" readonly>
                        <label>%</label>
                        <input type="text" id="iva_compensado"  style="width: 130px;margin-left: 5px;height: 15px;" readonly>
                    </td>
                </tr>
                <tr>
                    <td>
                        <label style="font-weight: bold">SubTotal</label>

                    </td>
                    <td>
                        <input type="text" id="subtotal1"  style="width: 189px;margin-left: 5px;height: 15px;font-weight: bold" readonly>
                    </td>
                </tr>
                <tr>
                    <td>
                        <label>Modalidad</label>
                    </td>
                    <td>
                        <select id="modalidad" style="width: 189px;color: #050505;height: 25px;margin-left: 5px;" onchange="visualizacion()" >
                            <option value="IVA">IVA</option>
                            <option value="AIU">AIU</option>
                        </select>
                    </td>
                </tr>
                <tr class="iva_">
                    <td>
                        <label id="l_iva" >IVA</label>
                    </td>
                    <td>
                        <input type="text" id="iva_"  readonly="true" style="width: 189px;margin-left: 5px;height: 15px;" >
                    </td>
                </tr>
                <tr class="lab">
                    <td>
                        <label>A (Administracion)</label>
                    </td>
                    <td>
                        <input type="text" id="porc_administracion"  style="width: 25px;margin-left: 5px;height: 15px;" >
                        <label>%</label>
                        <input type="text" id="administracion_t"  style="width: 125px;margin-left: 5px;height: 15px;" hidden="true">
                        <input type="text" id="administracion"  style="width: 125px;margin-left: 5px;height: 15px;" readonly>
                    </td>
                </tr>
                <tr class="lab">
                    <td>
                        <label>I (Imprevisto)</label>
                    </td>
                    <td>
                        <input type="text" id="porc_imprevisto"  style="width: 25px;margin-left: 5px;height: 15px;" >
                        <label>%</label>
                        <input type="text" id="imprevisto"  style="width: 125px;margin-left: 5px;height: 15px;" readonly>
                    </td>
                </tr>

                <tr class="lab">
                    <td>
                        <label>U (Utilidad)</label>
                    </td>
                    <td>
                        <input type="text" id="porc_utilidad"  style="width: 25px;margin-left: 5px;height: 15px;">
                        <label>%</label>
                        <input type="text" id="utilidad"  style="width: 125px;margin-left: 5px;height: 15px;" readonly>
                    </td>
                </tr>
                <tr class="lab">
                    <td>
                        <label>IVA (Utilidad)</label>
                    </td>
                    <td>
                        <input type="text" style="width: 25px;margin-left: 5px;height: 15px;"value="19" readonly="true">
                        <label>%</label>
                        <input type="text" id="iva_utilidad"  style="width: 125px;margin-left: 5px;height: 15px;" readonly>
                    </td>
                </tr>
                <tr>
                    <td>
                        <label hidden>AIU</label>
                    </td>
                    <td>
                        <input type="text" id="porc_aiu"  style="width: 25px;margin-left: 5px;height: 15px;" hidden>
                        <label hidden>%</label>
                        <input type="text" id="aiu"  style="width: 125px;margin-left: 5px;height: 15px;"readonly hidden>
                    </td>
                </tr>
                <tr>
                    <td>
                        <label style="font-weight: bold">Total</label>
                    </td>
                    <td>
                        <input type="text" id="total"  style="width: 189px;margin-left: 5px;height: 15px;font-weight: bold" readonly>
                    </td>
                </tr>
                <tr>
                    <td colspan="2">
                        <hr style="width: 350px;height: 3px;top: -25px;margin-left: 13px;color: rgba(128, 128, 128, 0.39);" /> 
                    </td>
                </tr>
                <tr>
                    <td colspan="2">
                        <div style="padding-top: 10px">
                            <center>
                                <button id="guardar" class="ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only" style="margin-left: 15px;top: -7px;" > 
                                    <span class="ui-button-text">Guardar</span>
                                </button>  
                                <button id="calcular" onclick="calcular_iva_compensar()" class="ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only lab" style="margin-left: 15px;top: -7px;" > 
                                    <span class="ui-button-text">Calcular</span>
                                </button>  
                            </center>
                        </div>
                    </td>
                </tr>
            </table>
        </div>
        <div id="dialogMsj" style="display:none;">
            <p style="font-size: 12px;text-align:justify;" id="msj" > Texto </p>
        </div>
        <div id="dialogMsjImpresion" style="position: relative;margin-top: -7;" hidden="true">
            <table>
                <tr>
                    <td colspan="2" valign="bottom" style="width: 1024px">
                <center>
                    <div class="ui-jqgrid ui-widget ui-widget-content center-block" style="width: 794px; height-min: 300px;border-radius: 14px;" >
                        <table id="tablainterna" style="height: 53px; width: 781px">
                            <tr>
                                <td>
                                    <label>Id solictud</label>
                                </td>
                                <td>
                                    <input type="text" id="id_solicitud_"  readonly="true" style="width: 125px;margin-left: 5px;height: 15px;">
                                </td>
                                <td rowspan="2">
                                    <hr style="width: 2px;height: 60px;top: -25px;margin-left: 13px;color: rgba(128, 128, 128, 0.39);" /> 
                                </td>
                                <td rowspan="2">
                                    <label>Modalidad</label>
                                </td>
                                <td rowspan="2">
                                    <select id="modalidad_" style="width: 189px;color: #050505;height: 25px;margin-left: 5px;" onchange="filtro_buscar()">
                                        <option value="APU">APU</option>
                                        <option value="WBS">WBS</option>
                                    </select>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <label>Nombre del proyecto</label>
                                </td>
                                <td>
                                    <input type="text" id="nombre_proyecto_"  readonly="true" style="width: 371px;margin-left: 5px;height: 15px;" >
                                </td>
                            </tr>
                        </table>
                    </div>
                </center>

                </td>
                </tr>
                <tr>
                    <td valign="middle" style="height: 380px">
                        <div id="contenedor_" >
                            <div  style="width: 141px; height-min: 300px;padding: 8px;border-radius: 14px; border: rgba(32, 66, 126, 0.84) solid 1px">
                                <table id="tablainterna" style="height: 53px; width: 370px"  >
                                    <tr>
                                        <td>
                                            <div id="opcion_1">
                                                <input type="checkbox" id="areas"  class="check" style="margin-right: -37px;"onchange="cargar_tabla(1)">
                                                <label class="check" style="margin-left: 8px;">�reas</label>
                                            </div>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <div id="opcion_1">
                                                <input type="checkbox" id="disciplinas" class="check" style="margin-right: -37px;" onchange="cargar_tabla(2)">
                                                <label class="check" style="margin-left: 8px;">Disciplinas</label>
                                            </div>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <div id="opcion_1">
                                                <input type="checkbox" id="capitulos" class="check" style="margin-right: -37px;" onchange="cargar_tabla(3)">
                                                <label class="check" style="margin-left: 8px;">Capitulos</label>
                                            </div>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <div id="opcion_1">
                                                <input type="checkbox" id="actividades"class="check"  style="margin-right: -37px;" onchange="cargar_tabla(4)">
                                                <label class="check" style="margin-left: 8px;">Actividades</label>
                                            </div>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <div id="opcion_1">
                                                <input type="checkbox" id="apu_" class="check" style="margin-right: -37px;" onchange="cargar_tabla(5)">
                                                <label class="check" style="margin-left: 8px;">APU</label>
                                            </div>
                                        </td>
                                    </tr>
                                </table>
                                <input type="text" id="opcion_tbl" style="width: 371px;margin-left: 5px;height: 15px;" hidden="true" >

                            </div>


                        </div>

                    </td>
                    <td valign="top">
                        <div style="">
                            <div id="div_tbl_Area" hidden="true">
                                <table id="tbl_Area" ></table>                
                                <div id="page_Area"></div>
                            </div>
                            <div id="div_tbl_Disciplina" hidden="true">
                                <table id="tbl_Disciplina"></table>                
                                <div id="page_Disciplina"></div>
                            </div>
                            <div id="div_tbl_Capitulo" hidden="true">
                                <table id="tbl_Capitulo" ></table>                
                                <div id="page_Capitulo"></div>
                            </div>
                            <div id="div_tbl_Actividad" hidden="true">
                                <table id="tbl_Actividad" ></table>                
                                <div id="page_Actividad"></div>
                            </div>
                            <div id="div_tbl_Apu" hidden="true">
                                <table id="tbl_Apu" ></table>                
                                <div id="page_Apu"></div>
                            </div>
                            <div id="div_tbl_Apu_Exp" hidden="true">
                                <table id="tbl_Apu_Exp" ></table>                
                                <div id="page_Apu_Exp"></div>
                            </div>
                        </div>

                    </td>
                </tr>

            </table>

        </div>
    </center>

    <div id="divSalidaEx" title="Exportacion" style=" display: block" >
        <p  id="msjEx" style=" display:  none"> Espere un momento por favor...</p>
        <center>
            <img id="imgloadEx" style="position: relative;  top: 7px; display: none " src="./images/cargandoCM.gif"/>
        </center>
        <div id="respEx" style=" display: none"></div>
    </div> 
    <div id="loader-wrapper">
        <div id="loader"></div>
        <img id="logowast" src="/fintra/images/Selectrik.png" style="z-index: 10002 !important">
        <div class="loader-section section-left">	
        </div>
        <div class="loader-section section-right"></div>
    </div>
</body>
</html>
