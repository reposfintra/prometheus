<%-- 
    Document   : GeneracionPdf
    Created on : 20/12/2017, 09:10:59 AM
    Author     : bterraza
--%>

<%@page contentType="text/html" pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html lang="es">
    <head>
        
        
        <meta http-equiv="-ContentType" content="text/html; charset=ISO-8859-1">

        <title>Generacion PdfSelectrik </title>



        <link href="./css/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css"/>
        <link href="./css/fontStyle.css" rel="stylesheet" type="text/css"/>

        <script type="text/javascript" src="./js/jquery-latest.js"></script>
                <script type="text/javascript" src="./js/jquery/editor/nicEdit-latest.js"></script>
                    <script> bkLib.onDomLoaded(function() { nicEditors.allTextAreas() });</script>



        <link type="text/css" rel="stylesheet" href="./css/jquery/jquery-ui/jquery-ui.css"/>     
    <script type="text/javascript" src="./js/jquery/jquery-ui/jquery.min.js"></script>
    <script type="text/javascript" src="./js/jquery/jquery-ui/jquery.ui.min.js"></script>
       <script src="./js/jquery/editor/ckeditor/ckeditor.js" type="text/javascript"></script>

        <link href="./css/OportunidadNegocio.css" rel="stylesheet" type="text/css"/>



        <link type="text/css" rel="stylesheet" href="./css/buttons/botonVerde.css"/>
        <script type="text/javascript" src="./js/jquery/jquery.jqGrid/js/grid.locale-es.js"></script>
        <script type="text/javascript" src="./js/jquery/jquery.jqGrid/js/jquery.jqGrid.min.js"></script>
        <link type="text/css" rel="stylesheet" href="./css/jquery/jquery.jqGrid/ui.jqgrid.css"/>    

        <link type="text/css" rel="stylesheet" href="./css/inputStyle.css " />


        <script src="./js/GeneraPdf/GeneracionPdf.js" type="text/javascript"></script>

        
        
        
        
        <!-------------------------------------Notificaciones----------------------------------------->
        <link type="text/css" rel="stylesheet" href="./css/toastr.min.css"/>
        <script type="text/javascript" src="./js/toastr.min.js"></script>
        <!-------------------------------------/Notificaciones----------------------------------------->

        <!----------------------------------------Pre-Load--------------------------------------------->
        <!--se necesita un div para que funcione el preload se llama loader-wrapper-->

        <link type="text/css" rel="stylesheet" href="./css/main2.css">

        <!----------------------------------------Pre-Load--------------------------------------------->

        <style>
            #logo_pdf{
                cursor:pointer;
            }           
            .onoffswitch-inner::before {
                content: "SI";
                padding-left: 10px;

            }
            .loader {
                position: fixed;
                left: 0px;
                top: 0px;
                width: 100%;
                height: 100%;
                z-index: 9999;
                background: url('/fintra/images/bigrotation.gif') 50% 50% no-repeat rgb(249,249,249);
                opacity: .8;
            }
        </style>
    </style>
    
    
    

    
    
    
    
</head>
<body>
<center>
    <jsp:include page="/WEB-INF/toptsp.jsp?encabezado=Generacion Pdf Selectrik"/>

    <!-- **********************************************************************************************************************************************************
                                                                Buscar Solicitud
    *********************************************************************************************************************************************************** -->
    <div id="div_busqueda" class="ui-jqgrid ui-widget ui-widget-content center-block" style="width: 1317px; height-min: 300px;margin-top: 10px;border-radius: 10px;border: 1px solid #2A88C8;">

        <!--            <div class="ui-dialog-titlebar ui-widget-header ui-helper-clearfix">
                        <span class="ui-dialog-title text-center" id="ui-dialog-title-listaGlobal">
                            &nbsp;
                            <label class="titulotablita"><b>B�squeda Solicitud</b></label>
                        </span>
                    </div>-->
        <div class="row" style="padding: 5px">
            <div class="col-md-12 ">

                <fieldset class="scheduler-border" >
                    <legend class="scheduler-border">B�squeda De Solicitud</legend>
                    <table id="tablainterna" style="height: 53px; width: 1271px"  >
                        <tr>
                            <td>O
                                <label>Tipo De Proyecto</label>
                            </td>
                            <td>
                                <select id="tipo_proyecto" class="form-control" style="width: 151px;color: #050505;height: 25px;margin-left: 5px;" >
                                    <option value=''></option>                                        
                                    <option value='0'>Proyectos Transicion</option>
                                    <option value='1'>Nuevos Proyectos</option>

                                </select>
                            </td>
                            <td>
                                <label>Linea Negocio</label>
                            </td>
                            <td>
                                <select id="linea_negocio" class="form-control" style="width: 151px; color: #050505;height: 25px;margin-left: 5px;" ></select>
                            </td>
                            <td>
                                <label style="margin-left: 10px;">Solicitud</label>
                            </td>
                            <td>
                                <input type="text" id="solicitud" class="form-control" style="width: 80px; margin-left: 5px;height: 26px;" >
                            </td>
                            <td>
                                <label style="margin-left: 10px;">Foms</label>
                            </td>
                            <td>
                                <input type="text" id="txt_foms"  class="form-control" style="width: 80px;margin-left: 5px;height: 26px;" >
                            </td>
                            <td>
                                <label style="margin-left: 10px;">Nombre Cliente</label>
                            </td>
                            <td>
                                <input type="text" id="txt_nom_cliente" class="form-control" style="width: 80px;margin-left: 5px;height: 26px;" >
                            </td>
                            <td>
                                <label style="margin-left: 10px;">Nombre Proyecto</label>
                            </td>
                            <td>
                                <input type="text" id="txt_nom_proyecto"  class="form-control" accept=""style="width: 80px;margin-left: 5px;height: 26px;" >
                            </td>
                            <td>
                                <label style="margin-left: 10px;"hidden="true">Responsable</label>
                            </td>
                            <td>
                                <input type="text" id="responsable"  hidden="true" style="width: 139px;margin-left: 5px;height: 26px;" >

                            </td>

                            <td>
                                <label style="margin-left: 10px;" hidden="true"  >Estado cartera</label>
                            </td>
                            <td>
                                <select id="estadocartera" hidden="true" style="width: 108px;color: #050505;height: 25px;margin-left: 5px;" ></select>
                            </td> 
                            <td>
                                <label style="margin-left: 10px;" hidden="true">Fecha validacion cartera</label>
                            </td>
                            <td>
                                <input type="datetime" id="fechaini" hidden="true" style="width: 100px ;height: 26px;"  readonly>
                            </td>
                            <td>
                                <input  type="datetime" id="fechafin" hidden="true" style="width: 100px;height: 26px;" readonly>
                            </td>
                            <td>
                                <input type="text" id="etapaActual"  style="width: 80px;margin-left: 5px;height: 26px;" value="COMERCIAL" hidden >
                            </td>
                            <td>
                                <div>
                                    <center>
                                        <button id='buscar_solicitud' class='btn btn-primary' > 
                                            <span class="ui-button-text">Buscar</span>
                                        </button> 

                                    </center>
                                </div>
                            </td>
                        </tr>
                    </table>
                </fieldset>
            </div>
        </div>
    </div>
    <div style="position: relative;top: 35px;">
        <table id="tabla_infoSolicitud" ></table>
        <div id="pager"></div>
    </div>

    <div id="ventana_Responsables" style="margin-top: 25px">
        <table id="tbl_Responsables" ></table>
        <div id="page_tbl_Responsables"></div>  
    </div>

    <!-- **********************************************************************************************************************************************************
                                                                
    *********************************************************************************************************************************************************** -->
    <!-- **********************************************************************************************************************************************************
                                                                Ventana cambio de etapa y estado de la Solicitud
    *********************************************************************************************************************************************************** -->

    <div id="cambioEtapa" style="display: none">
        <div class="row"style="padding: 5px">
            <div class="col-md-2 ">
                <fieldset class="scheduler-border" style="width: 720px">

                    <div class="row">
                        <div class="col-md-2">
                            Idsolicitud :
                        </div>
                        <div class="col-md-4">
                            <input type="text" id="idsolicitud" readonly>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-2">
                            Etapa Actual:
                        </div>
                        <div class="col-md-4">
                            <input id="etapa" type="Text" readonly="true" style="width: 80%">
                            <input id="idetapa" hidden="true" type="Text" style="width: 80%">
                        </div>
                        <div class="col-md-2">
                            Estado Actual:
                        </div>
                        <div class="col-md-4">
                            <input id="estadoActual" readonly="true" type="Text" style="width: 80%">
                            <input id="idestadoActual" readonly="true" type="Text" style="width: 80%" hidden>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-2">
                            Estado :
                        </div>
                        <div class="col-md-4">
                            <select id="estado" class="selectpicker" style="width: 80%"></select>
                        </div>
                        <div class="col-md-2" hidden="true">
                            Causal :
                        </div>
                        <div class="col-md-4" hidden="true">
                            <select id="causal" class="selectpicker" style="width: 80%"></select>
                        </div>

                    </div>
                    <div class="row">
                        <div class="col-md-2">
                            Trazabilidad :
                        </div>
                        <div class="col-md-10">
                            <textarea id="trazabilidad" class="form-control" readonly="true" rows="5"></textarea>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-2">
                            Descripcion :
                        </div>
                        <div class="col-md-10">
                            <textarea id="descripcion_causal" class="form-control" rows="5"></textarea>
                        </div>
                    </div>



                </fieldset>
            </div>
        </div>
    </div>

    <!-- **********************************************************************************************************************************************************
                                                               Ventana de dialogos
   *********************************************************************************************************************************************************** -->
    <div>
        <div id="dialogLoading" style="display:none;">
            <p  style="font-size: 12px;text-align:justify;" id="msj2">Texto </p> <br/>
            <center>
                <img src="./images/cargandoCM.gif"/>
            </center>
        </div>
        <!-- Dialogo de los jsp> -->
        <div id="dialogMsj" style="display:none;">
            <p style="font-size: 12px;text-align:justify;" id="msj" > Texto </p>
        </div>
    </div>

    
    
    
    
    
    
    
    
        <div id="div_ver_notas"  style="display: none; " >  
           
        
               
        <div style="position: relative;top: 35px;">
        <table id="tabla_Notas" ></table>
        <div id="pager"></div>
            </div>

       
        </div> 
    
    
    
    
    
    
    
    <div id="div_modificar_notas"  style="display: none; " >  
           
        
      	<h2>Edici�n de notas de la oferta</h2>
        
        
        <select id='tipo_nota_modificar' disabled="true">
                <option value="-1"> Seleccione el tipo de nota</option>
                <option value='1'>Nota Aclaratoria</option>
                <option value='2'>Objeto y Alcance de la Oferta</option>
                <option value='3'>Condiciones Comerciales</option>
                <option value='4'>Documentos Requeridos</option>
                <option value='5'>Informaci�n del Asesor</option>

        </select>
        
         <br>
         <br>
         <br>
	
	<textarea id="descripcion_nota_modificar" style="width: 900px; height: 500px;" placeholder="Escriba sobre la caja de Texto.">
	</textarea>

       
</div>  
    
    

    
    <div id="div_editar_notas"  style="display: none; " >  
           
        

        
      	<h2>Notas de la oferta</h2>
        
       
        
        <select id='tipo_nota'>
                <option value="-1">Seleccione el tipo de nota</option>
                <option value='1'>Nota Aclaratoria</option>
                <option value='2'>Objeto y Alcance de la Oferta</option>
                <option value='3'>Condiciones Comerciales</option>
                <option value='4'>Documentos Requeridos</option>
                <option value='5'>Informaci�n del Asesor</option>

        </select>
        
         <br>
         <br>
         <br>
	
	<textarea id="descripcion_nota" style="width: 900px; height: 500px;" placeholder="Escriba sobre la caja de Texto.">
	</textarea>

       
</div>  
        
        

    <script type="text/javascript" src="./js/jquery/jquery.jqGrid-4.7.0/js/grid.locale-es.js"></script>
    <script type="text/javascript" src="./js/jquery/jquery.jqGrid-4.7.0/js/jquery.jqGrid.min.js"></script>
    <script type="text/javascript" src="./js/jquery/jquery.jqGrid-4.7.0/js/jquery.tabletojson.js"></script> 



    <!------------------------------------------------------------------------->

    <!--Dialogo Cotizaciones-->
    <div id="dialog" title="Generar Cotizaci�n" style="display: none;" >
        <label>Ver en detalle</label>
        <label class="switch">
            <input class="switch-input" type="checkbox" id="switch"/>
            <span class="switch-label" data-on="Si" data-off="No"></span> 
            <span class="switch-handle"></span> 
        </label>
        <label>Detalle APU</label>
        <label class="switch">
            <input class="switch-input" type="checkbox" id="switch2"/>
            <span class="switch-label" data-on="Si" data-off="No"></span> 
            <span class="switch-handle"></span> 
        </label>
        
        <label>Precios</label>
        <label class="switch">
            <input class="switch-input" type="checkbox" id="precios"/>
            <span class="switch-label" data-on="Si" data-off="No"></span> 
            <span class="switch-handle"></span> 
        </label>
    </div>

    <div class="loader" style="display: none;"></div>


</center>
<div id="loader-wrapper">
    <div id="loader"></div>
    <img id="logowast" src="/fintra/images/Selectrik.png" style="z-index: 10002 !important">
    <div class="loader-section section-left">	
    </div>
    <div class="loader-section section-right"></div>
</div>
    

 <div id="dialogMsgMeta" title="Mensaje" style="display:none; visibility: hidden">
                <p style="font-size: 12.5px;text-align:justify;" id="msj3" > Texto </p>
            </div>      
    
</center>
</body>
</html>
 