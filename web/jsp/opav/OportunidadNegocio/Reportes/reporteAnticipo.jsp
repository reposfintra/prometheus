<%-- 
    Document   : reporteAnticipo
    Created on : 11/04/2017, 09:33:04 AM
    Author     : mariana
--%>

<%@page contentType="text/html" pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
        <link href="./css/popup.css" rel="stylesheet" type="text/css">
        <link type="text/css" rel="stylesheet" href="./css/jquery/jquery-ui/jquery-ui.css" />
        <link type="text/css" rel="stylesheet" href="./css/TransportadorasApi.css " />
        <script type="text/javascript" src="./js/jquery/jquery.jqGrid/js/jquery.min.js"></script>
        <script type="text/javascript" src="./js/jquery/jquery-ui/jquery.ui.min.js"></script> 
        <script type="text/javascript" src="./js/jquery-ui-1.8.5.custom.min.js"></script>   
        <script type="text/javascript" src="./js/jquery/jquery.jqGrid-4.7.0/js/grid.locale-es.js"></script>
        <script type="text/javascript" src="./js/jquery/jquery.jqGrid-4.7.0/js/jquery.jqGrid.min.js"></script>
        <script type="text/javascript" src="./js/jquery/jquery.jqGrid-4.7.0/js/jquery.tabletojson.js"></script>    
        <script type="text/javascript" src="./js/utilidadInformacion.js"></script> 
        <script type="text/javascript" src="./js/reporteAnticipo.js"></script> 

        <title>Facturación </title>
    </head>
    <body>
        <div id="capaSuperior" style="position:absolute; width:100%; height:100px; z-index:0; left: 0px; top: 0px;">
            <jsp:include page="/WEB-INF/toptsp.jsp?encabezado=Facturación Y Anticipo"/>
        </div>

    <center>
        <div id="tablita" class="ui-jqgrid ui-widget ui-widget-content center-block" style="width: 667px; margin-top: 80px;padding: 8px;">
            <div class="ui-dialog-titlebar ui-widget-header ui-helper-clearfix">
                <label class="titulotablita"><b>FILTRO</b></label>
            </div>
            <table id="tablainterna" style="height: 53px; width: 787px"  >
                <tr>
                    <td>
                        <label>Solicitud</label>
                        <input type="text" id="solicitud"  style="width: 59px;" >
                    </td>
                    <td>
                        <label>Foms</label>
                        <input type="text" id="foms"  style="width: 94px;" onkeyup="funcionFormatos.textMayusKeyup(this)"  >
                    </td>
<!--                    <td>
                        <label>Estado</label>
                        <select id="estado"  style="width: 59px;" ></select>
                    </td>-->
                    <td>
                        <label>Anticipo</label>
                        <input type="text" id="anticipo"  style="width: 86px;" onkeyup="funcionFormatos.textMayusKeyup(this)" >
                    </td>
                    <td>
                        <div>
                            <center>
                                <button type="button" id="buscar" name="buscar" class="ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only" style="margin-right: 118px;height: 29px;width: 75px;" > 
                                    <span class="ui-button-text">Buscar</span>
                                </button>  
                            </center>
                        </div>
                    </td>
                </tr>
            </table>
        </div>
        <div style="position: relative;top: 195px;">
            <table id="tabla_anticipo" style="top: 37px;"></table>
            <div id="pager3"></div>
        </div>
        <div id="divSalidaEx" title="Exportacion" style=" display: block" >
            <p  id="msjEx" style=" display:  none"> Espere un momento por favor...</p>
            <center>
                <img id="imgloadEx" style="position: relative;  top: 7px; display: none " src="./images/cargandoCM.gif"/>
            </center>
            <div id="respEx" style=" display: none"></div>
        </div> 
    </center>
</body>
</html>
