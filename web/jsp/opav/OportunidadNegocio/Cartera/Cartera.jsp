<%-- 
    Document   : OportunidadNegocio
    Created on : 15/04/2016, 09:11:26 AM
    Author     : William Siado Torres
--%>

<%@page contentType="text/html" pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html lang="es">

    <head>

        <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
        <meta name="viewport" content="width=device-width, initial-scale=1">


        <title>GESTION CARTERA</title>



        <link href="./css/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css"/>
        <link href="./css/simple-sidebar.css" rel="stylesheet" type="text/css"/>
        <link href="./css/OportunidadNegocio.css" rel="stylesheet" type="text/css"/>




        <link type="text/css" rel="stylesheet" href="./css/jquery/jquery-ui/jquery-ui.css"/>     
        <script type="text/javascript" src="./js/jquery/jquery-ui/jquery.min.js"></script>
        <script type="text/javascript" src="./js/jquery/jquery-ui/jquery.ui.min.js"></script> 
        <script src="./js/jquery/editor/ckeditor/ckeditor.js" type="text/javascript"></script>



        <link type="text/css" rel="stylesheet" href="./css/buttons/botonVerde.css"/>
        <script type="text/javascript" src="./js/jquery/jquery.jqGrid/js/grid.locale-es.js"></script>
        <script type="text/javascript" src="./js/jquery/jquery.jqGrid/js/jquery.jqGrid.min.js"></script>
        <link type="text/css" rel="stylesheet" href="./css/jquery/jquery.jqGrid/ui.jqgrid.css"/>    


    </head> 
    <body>
    <center>
        <jsp:include page="/WEB-INF/toptsp.jsp?encabezado=GESTION CARTERA"/>


        <input type="hidden" id="id_cliente" name="id_cliente">
        <!-- **********************************************************************************************************************************************************
                                   Buscar Editar Cliente
        *********************************************************************************************************************************************************** -->
        <div id="div_busqueda" class="ui-jqgrid ui-widget ui-widget-content center-block" style="width: 1317px; height-min: 300px;margin-top: 10px;border-radius: 10px;border: 1px solid #2A88C8;">

            <!--            <div class="ui-dialog-titlebar ui-widget-header ui-helper-clearfix">
                            <span class="ui-dialog-title text-center" id="ui-dialog-title-listaGlobal">
                                &nbsp;
                                <label class="titulotablita"><b>Busqueda Solicitud</b></label>
                            </span>
                        </div>-->
            <div class="row" style="padding: 5px">
                <div class="col-md-12 ">

                    <fieldset class="scheduler-border" >
                        <legend class="scheduler-border">Busqueda De Solicitud</legend>
                        <table id="tablainterna" style="height: 53px; width: 1271px"  >
                            <tr>
                                <td>
                                    <label>Tipo De Proyecto</label>
                                </td>
                                <td>
                                    <select id="tipo_proyecto" style="width: 151px;color: #050505;height: 25px;margin-left: 5px;" >
                                        <option value=''></option>                                        
                                        <option value='0'>Proyectos Transicion</option>
                                        <option value='1'>Nuevos Proyectos</option>

                                    </select>
                                </td>
                                <td>
                                    <label>Linea Negocio</label>
                                </td>
                                <td>
                                    <select id="linea_negocio" style="width: 151px;color: #050505;height: 25px;margin-left: 5px;" ></select>
                                </td>
                                <td>
                                    <label style="margin-left: 10px;">Solicitud</label>
                                </td>
                                <td>
                                    <input type="text" id="solicitud"  style="width: 80px;margin-left: 5px;height: 26px;" >
                                </td>
                                <td>
                                    <label style="margin-left: 10px;">Foms</label>
                                </td>
                                <td>
                                    <input type="text" id="txt_foms"  style="width: 80px;margin-left: 5px;height: 26px;" >
                                </td>
                                <td>
                                    <label style="margin-left: 10px;">Nombre Cliente</label>
                                </td>
                                <td>
                                    <input type="text" id="txt_nom_cliente"  style="width: 80px;margin-left: 5px;height: 26px;" >
                                </td>
                                <td>
                                    <label style="margin-left: 10px;">Nombre Proyecto</label>
                                </td>
                                <td>
                                    <input type="text" id="txt_nom_proyecto"  style="width: 80px;margin-left: 5px;height: 26px;" >
                                </td>
                                <td>
                                    <label style="margin-left: 10px;"hidden="true">Responsable</label>
                                </td>
                                <td>
                                    <input type="text" id="responsable"  hidden="true" style="width: 139px;margin-left: 5px;height: 26px;" >

                                </td>

                                <td>
                                    <label style="margin-left: 10px;" hidden="true"  >Estado cartera</label>
                                </td>
                                <td>
                                    <select id="estadocartera" hidden="true" style="width: 108px;color: #050505;height: 25px;margin-left: 5px;" ></select>
                                </td> 
                                <td>
                                    <label style="margin-left: 10px;" hidden="true">Fecha validacion cartera</label>
                                </td>
                                <td>
                                    <input type="datetime" id="fechaini" hidden="true" style="width: 100px ;height: 26px;"  readonly>
                                </td>
                                <td>
                                    <input  type="datetime" id="fechafin" hidden="true" style="width: 100px;height: 26px;" readonly>
                                </td>
                                <td>
                                    <input type="text" id="etapaActual"  style="width: 80px;margin-left: 5px;height: 26px;" value="CARTERA" hidden >
                                </td>
                                <td>
                                    <div>
                                        <center>
                                            <button id="btn_Aceptar_tipoClienteC" class="ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only" style="margin-left: 15px;" > 
                                                <span class="ui-button-text">Buscar</span>
                                            </button>  
                                        </center>
                                    </div>
                                </td>
                            </tr>

                        </table>

                    </fieldset>
                </div>
            </div>
        </div>
        <div style="position: relative;top: 35px;">
            <table id="tabla_infoSolicitudC" ></table>
            <div id="pagerC"></div>
        </div>
        <!-- **********************************************************************************************************************************************************
                                                   Div Presenta Solicitudes
        *********************************************************************************************************************************************************** -->

        <div id="div_Solicitidudes" class="col-md-12 fx-center" style="display: none" >    


            <table id="solicitudes" >

                <div id="searchsolicitudes"></div>
                <div id="page_tabla_solicitudes"></div>
            </table>



        </div>
        <!-- **********************************************************************************************************************************************************
                                   
        *********************************************************************************************************************************************************** -->
        <div id="fformPrincipal2" style="display: none">
            <div class="ui-jqgrid ui-widget ui-widget-content center-block" style="width: 100%;">
                <div class="ui-dialog-titlebar ui-widget-header ui-helper-clearfix">
                    <span class="ui-dialog-title text-center" id="ui-dialog-title-listaGlobal">
                        &nbsp;
                        <label class="titulotablita"><b>Cliente</b></label>
                    </span>

                </div>
                <div class="row"style="padding: 5px">
                    <div class="col-md-12 ">
                        <fieldset  class="scheduler-border" style="margin: 3px">
                            <legend class="scheduler-border">Datos Clientes </legend>
                            <div class="row">
                                <div class="col-md-2">Nombre :</div>
                                <div class="col-md-4"><input id="NombreCliente" readonly="true" type="text" placeholder="" style="width: 86%;" ></div>
                                <div class="col-md-2">Departamento :</div>
                                <div class="col-md-4">
                                    <input id="Departamento" type="text" readonly="true" placeholder="" style="width: 86%;" >
                                    <input id="idDepartamento" type="text" readonly="true" placeholder=""  style="display: none" >

                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-2">Nit :</div>
                                <div class="col-md-4">
                                    <input id="NitCliente" readonly="true" class="solo-numero" type="text" placeholder="" style="width: 73%;" onblur="CalcularDv()" >
                                    -
                                    <input id="DigitoVerificacion" readonly="true" type="text" placeholder="" style="width: 10%; text-align: center" >
                                </div>
                                <div class="col-md-2">Ciudad :</div>
                                <div class="col-md-4">
                                    <input id="Ciudad" type="text" readonly="true" placeholder="" style="width: 86%;" >
                                    <input id="idCiudad" type="text" style="display: none">
                                </div>

                            </div>
                            <div class="row">
                                <div class="col-md-2">Nombre Contacto:</div>
                                <div class="col-md-4"><input id="NombreContacto" readonly="true" type="text" placeholder="" style="width: 86%;" ></div>

                                <div class="col-md-2">Direccion :</div>
                                <div class="col-md-4"> 
                                    <input id="DireccionCliente" type="text" readonly="true" placeholder="" style="width: 73%;">
                                    <img src="/fintra/images/Direcciones.png" width="26" height="23" border="0" align="ABSMIDDLE" onclick="genDireccion('direccion', event);" alt="Direcciones"  title="Direcciones" />
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-2">Celular Contacto:</div>
                                <div class="col-md-4">
                                    <input id="CelularContacto" class="solo-numero" readonly="true" type="text" placeholder="" style="width: 86%;" maxlength="10">
                                </div>
                            </div>
                        </fieldset>
                    </div>
                </div>
            </div>




            <div class="ui-jqgrid ui-widget ui-widget-content center-block" style="width: 100%; height-min: 300px; margin-top: 30px">
                <div class="ui-dialog-titlebar ui-widget-header ui-helper-clearfix">
                    <span class="ui-dialog-title text-center" id="ui-dialog-title-listaGlobal">
                        &nbsp;
                        <label class="titulotablita"><b>Datos Solicitud</b></label>
                    </span>
                </div>
                <!-- **********************************************************************************************************************************************************
                                                                        INICIO Div Datos Solicitud
                *********************************************************************************************************************************************************** -->

                <div class="row"  style="padding: 5px;">


                    <div class="col-md-12 ">
                        <div id='formPrincipal'>
                            <fieldset  class="scheduler-border">
                                <legend class="scheduler-border">Datos Solicitud </legend>
                                <div class="row">
                                    <div class="col-md-9">
                                        <div class="row">
                                            <div class="col-md-2">Cliente :</div>
                                            <div class="col-md-4">
                                                <input id="txt_NombreCliente" readonly="true" type="text" placeholder="" style="width: 86%;" >
                                                <input type="hidden" id="idCliente" name="idCliente">
                                            </div>
                                            <div id="div_opd" style="display: ">
                                                <div class="col-md-2">OPD :</div>
                                                <div class="col-md-4">
                                                    <input id="OPD"  type="text" readonly="true" placeholder="" style="width: 86%;" >                            
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-2">Linea Negocio:</div>
                                            <div class="col-md-4">
                                                <select id='cbx_LineaDeNegocio'  disabled="true" class="selectpicker" name="cbx_LineaDeNegocio"  onchange="cargarCbxTipoSolicitud();" style="width: 86%;"></select>                         
                                            </div>
                                            <!--                                            <div class="col-md-2">Tipo Solicitud :</div>
                                                                                        <div class="col-md-4">
                                                                                            <select id='cbx_TipoSolicitud' disabled="true" class="selectpicker" name="cbx_TipoSolicitud"  style="width: 86%;"></select>                         
                                                                                        </div>-->
                                            <div class="col-md-2">Nic :</div>
                                            <div class="col-md-4"> 
                                                <select id='cbx_nic' disabled="true" class="selectpicker" name="cbx_nic"  style="width: 60%;"></select>
                                            </div>

                                        </div>
                                        <div class="row">
                                            <div class="col-md-2">Responsable :</div>
                                            <div class="col-md-4"> 
                                                <!--<select id='cbx_Responsable' class="selectpicker" name="cbx_Responsable"  style="width: 86%;"></select>-->
                                                <input id="txt_responsable" readonly="true" type="text" placeholder="" style="width: 86%;" >
                                                <input id="idresponsable" hidden="true" type="text" placeholder="" style="width: 86%;" >
                                            </div>



                                        </div>
                                        <div class="row" style="display: none">

                                            <div class="col-md-2">Interventor :</div>
                                            <div class="col-md-4"> 
                                                <select id='cbx_Interventor' class="selectpicker" name="cbx_Interventor"  style="width: 86%;"></select>

                                            </div>
                                        </div>
                                    </div>  
                                    <div class="col-md-3">
                                        <div class="row">
                                            <div class="col-md-4">IdSolicitud:</div>
                                            <div class="col-md-8"> 
                                                <input id="idSolicitud" readonly="true" type="text" placeholder="" style="width: 100%;">
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-4">Cartera:<span style="color: red;">*</span></div>
                                            <div class="col-md-8"> 
                                                <select id='cbx_Cartera' class="selectpicker" name="cbx_nic" style="width: 100%;"></select>
                                            </div>
                                        </div>
                                    </div>
                                </div>


                                <div class="row">
                                    <div class="col-md-9">
                                        <div class="row">

                                            <div class="col-md-2">Historia Solicitud :</div>
                                            <div class="col-md-10"> 
                                                <div class="form-group">
                                                    <textarea id="historiasolicitud" readonly="true" class="form-control" rows="5" ></textarea>
                                                </div>
                                            </div>   
                                        </div>
                                        <div class="row">
                                            <div class="col-md-2">Observacion :<span style="color: red;">*</span></div>
                                            <div class="col-md-10"> 
                                                <div class="form-group">
                                                    <textarea id="descripcion"  class="form-control" rows="5" ></textarea>
                                                </div>
                                            </div>   
                                        </div>   

                                    </div>
                                </div>


                        </div>
                        <div class="row">
                            <div class="col-md-9">
                                <div class="row">
                                    <div class="col-md-3">Fecha Creacion :</div>
                                    <div class="col-md-3"> 
                                        <input id="fechaCreacion" readonly="true" type="text" placeholder="" style="width: 100%;">
                                    </div>
                                </div>
                            </div>
                        </div>

                    </div>
                    <hr>
                </div>
            </div>
        </div>

        <!-- **********************************************************************************************************************************************************
                                                                FIN Div Datos Solicitud
        *********************************************************************************************************************************************************** -->
        <!-- **********************************************************************************************************************************************************
                                                                Ventana cambio de etapa y estado
        *********************************************************************************************************************************************************** -->

        <div id="cambioEtapa" style="display: none">
            <div class="row"style="padding: 5px">
                <div class="col-md-2 ">
                    <fieldset class="scheduler-border" style="width: 720px">

                        <div class="row">
                            <div class="col-md-2">
                                Idsolicitud :
                            </div>
                            <div class="col-md-4">
                                <input type="text" id="idsolicitud" readonly>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-2">
                                Etapa Actual:
                            </div>
                            <div class="col-md-4">
                                <input id="etapa" type="Text" readonly="true" style="width: 80%">
                                <input id="idetapa" hidden="true" type="Text" style="width: 80%">
                            </div>
                            <div class="col-md-2">
                                Estado Actual:
                            </div>
                            <div class="col-md-4">
                                <input id="estadoActual" readonly="true" type="Text" style="width: 80%">
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-2">
                                Estado :
                            </div>
                            <div class="col-md-4">
                                <select id="estado" class="selectpicker" style="width: 80%"></select>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-2">
                                Causal :
                            </div>
                            <div class="col-md-10">
                                <textarea id="causal" class="form-control" rows="5"></textarea>
                            </div>
                        </div>



                    </fieldset>
                </div>
            </div>
        </div>

        <script src="./js/OportunidadNegocio/Cartera.js" type="text/javascript"></script>


        <!------------------------------------------------------------------------->
        <script type="text/javascript" href="http://www.ddginc-usa.com/spanish/editcheck.js"></script>
        <script type="text/javascript" src="./js/jquery/jquery.jqGrid-4.7.0/js/grid.locale-es.js"></script>
        <script type="text/javascript" src="./js/jquery/jquery.jqGrid-4.7.0/js/jquery.jqGrid.min.js"></script>
        <script type="text/javascript" src="./js/jquery/jquery.jqGrid-4.7.0/js/jquery.tabletojson.js"></script> 

        <div id="dialogLoading" style="display:none;">
            <p  style="font-size: 12px;text-align:justify;" id="msj2">Texto </p> <br/>
            <center>
                <img src="./images/cargandoCM.gif"/>
            </center>
        </div>
        <!-- Dialogo de los jsp> -->
        <div id="dialogMsj" style="display:none;">
            <p style="font-size: 12px;text-align:justify;" id="msj" > Texto </p>
        </div>
    </center>
</body>
</html>