<%-- 
    Document   : OportunidadNegocio
    Created on : 15/04/2016, 09:11:26 AM
    Author     : William Siado Torres
--%>

<%@page contentType="text/html" pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>  

    <head>

        <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">



        <title>GESTION DE SOLICITUDES</title>



        <link href="./css/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css"/>
        <link href="./css/simple-sidebar.css" rel="stylesheet" type="text/css"/>
        <link href="./css/OportunidadNegocio.css" rel="stylesheet" type="text/css"/>




        <link type="text/css" rel="stylesheet" href="./css/jquery/jquery-ui/jquery-ui.css"/>     
        <script type="text/javascript" src="./js/jquery/jquery-ui/jquery.min.js"></script>
        <script type="text/javascript" src="./js/jquery/jquery-ui/jquery.ui.min.js"></script> 
        <script src="./js/jquery/editor/ckeditor/ckeditor.js" type="text/javascript"></script>



        <link type="text/css" rel="stylesheet" href="./css/buttons/botonVerde.css"/>
        <script type="text/javascript" src="./js/jquery/jquery.jqGrid/js/grid.locale-es.js"></script>
        <script type="text/javascript" src="./js/jquery/jquery.jqGrid/js/jquery.jqGrid.min.js"></script>
        <link type="text/css" rel="stylesheet" href="./css/jquery/jquery.jqGrid/ui.jqgrid.css"/>   

        <!-------------------------------------Notificaciones----------------------------------------->
        <link type="text/css" rel="stylesheet" href="./css/toastr.min.css"/>
        <script type="text/javascript" src="./js/toastr.min.js"></script>
        <!-------------------------------------/Notificaciones----------------------------------------->



    </head> 
    <body>

        <%
            String idaccion = request.getParameter("idaccion");
        %>
    <center>
        <jsp:include page="/WEB-INF/toptsp.jsp?encabezado=GESTION DE SOLICITUDES"/>

        <input type="hidden" id="id_cliente" name="id_cliente">

        <!-- **********************************************************************************************************************************************************
                                   Buscar Editar Cliente
        *********************************************************************************************************************************************************** -->
        <div id="div_busqueda" class="ui-jqgrid ui-widget ui-widget-content center-block" style="width: 1365px; height-min: 300px;margin-top: 10px;border-radius: 10px;border: 1px solid #2A88C8;">
            <!--
                        <div class="ui-dialog-titlebar ui-widget-header ui-helper-clearfix">
                            <span class="ui-dialog-title text-center" id="ui-dialog-title-listaGlobal">
                                &nbsp;
                                <label class="titulotablita"><b>B�squeda Solicitud</b></label>
                            </span>
                        </div>-->
            <div class="row" style="padding: 5px">
                <div class="col-md-12 ">

                    <fieldset class="scheduler-border" >
                        <legend class="scheduler-border">B�squeda De Solicitud</legend>
                        <table id="tablainterna" style="height: 53px; width: 1305px"  >
                            <tr>
                                <td>
                                    <label>Tipo De Proyecto</label>
                                </td>
                                <td>
                                    <select id="tipo_proyecto" style="width: 151px;color: #050505;height: 25px;margin-left: 5px;" >
                                        <option value=''></option>                                        
                                        <option value='0'>Proyectos Transicion</option>
                                        <option value='1'>Nuevos Proyectos</option>

                                    </select>
                                </td>
                                <td>
                                    <label>Linea Negocio</label>
                                </td>
                                <td>
                                    <select id="linea_negocio" style="width: 151px;color: #050505;height: 25px;margin-left: 5px;" ></select>
                                </td>
                                <td>
                                    <label style="margin-left: 10px;">Solicitud</label>
                                </td>
                                <td>
                                    <input type="text" id="solicitud"  style="width: 80px;margin-left: 5px;height: 26px;" >
                                </td>
                                <td>
                                    <label style="margin-left: 10px;">Foms</label>
                                </td>
                                <td>
                                    <input type="text" id="txt_foms"  style="width: 80px;margin-left: 5px;height: 26px;" >
                                </td>
                                <td>
                                    <label style="margin-left: 10px;">Nombre Cliente</label>
                                </td>
                                <td>
                                    <input type="text" id="txt_nom_cliente"  style="width: 80px;margin-left: 5px;height: 26px;" >
                                </td>
                                <td>
                                    <label style="margin-left: 10px;">Nombre Proyecto</label>
                                </td>
                                <td>
                                    <input type="text" id="txt_nom_proyecto"  style="width: 80px;margin-left: 5px;height: 26px;" >
                                </td>
                                <td>
                                    <label style="margin-left: 10px;"hidden="true">Responsable</label>
                                </td>
                                <td>
                                    <input type="text" id="responsable"  hidden="true" style="width: 139px;margin-left: 5px;height: 26px;" >

                                </td>

                                <td>
                                    <label style="margin-left: 10px;" hidden="true"  >Estado cartera</label>
                                </td>
                                <td>
                                    <select id="estadocartera" hidden="true" style="width: 108px;color: #050505;height: 25px;margin-left: 5px;" ></select>
                                </td> 
                                <td>
                                    <label style="margin-left: 10px;" hidden="true">Fecha validacion cartera</label>
                                </td>
                                <td>
                                    <input type="datetime" id="fechaini" hidden="true" style="width: 100px ;height: 26px;"  readonly>
                                </td>
                                <td>
                                    <input  type="datetime" id="fechafin" hidden="true" style="width: 100px;height: 26px;" readonly>
                                </td>
                                <td>
                                    <input type="text" id="etapaActual"  style="width: 80px;margin-left: 5px;height: 26px;" value="COMERCIAL" hidden >
                                </td>
                                <td>
                                    <div style="margin-left: 15px;">
                                        <center>
                                            <button id="btn_Aceptar_tipoCliente" class="ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only" > 
                                                <span class="ui-button-text">Buscar</span>
                                            </button> 
                                            <!--                                            <button id="btn_pdf" class="ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only"> 
                                                                                            <span class="ui-button-text">pdf</span>
                                                                                        </button> -->
                                        </center>
                                    </div>
                                </td>
                                <td>
                                    <div>
                                        <button id="btn_nuevo_tipoCliente" class="ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only"> 
                                            <span class="ui-button-text">Nuevo</span>
                                        </button> 
                                    </div>
                                </td>
                            </tr>
                        </table>

                    </fieldset>
                </div>
            </div>
        </div>
        <div style="position: relative;top: 35px;">
            <table id="tabla_infoSolicitud"></table>
            <div id="pager"></div>
        </div>
        <!-- *********************************************************************************************************************************************************
                                                   Div Presenta Solicitudes
        *********************************************************************************************************************************************************** -->

        <div id="div_Solicitidudes" class="col-md-12 fx-center" style="display: none" >    


            <table id="solicitudes" >

                <div id="searchsolicitudes"></div>
                <div id="page_tabla_solicitudes"></div>
            </table>



        </div>
        <!-- **********************************************************************************************************************************************************
                                   
        *********************************************************************************************************************************************************** -->

        <div id="fformPrincipal" style="display: none">
            <div class="ui-jqgrid ui-widget ui-widget-content center-block" style="width: 100%; "> 
                <div class="ui-dialog-titlebar ui-widget-header ui-helper-clearfix">
                    <span class="ui-dialog-title text-center" id="ui-dialog-title-listaGlobal">
                        &nbsp;
                        <label class="titulotablita"><b>Cliente</b></label>
                    </span>

                </div>
                <div class="row"style="padding: 5px">
                    <div class="col-md-12 ">
                        <fieldset  class="scheduler-border" style="margin: 3px">
                            <legend class="scheduler-border">Datos Clientes </legend>
                            <div class="row">
                                <div class="col-md-2">Nombre :</div>
                                <div class="col-md-4"><input id="NombreCliente" readonly="true" type="text" placeholder="" style="width: 86%;" ></div>
                                <div class="col-md-2">Departamento :</div>
                                <div class="col-md-4">
                                    <input id="Departamento" type="text" readonly="true" placeholder="" style="width: 86%;" >
                                    <input id="idDepartamento" type="text" readonly="true" placeholder=""  style="display: none" >

                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-2">Nit :</div>
                                <div class="col-md-4">
                                    <input id="NitCliente" readonly="true" class="solo-numero" type="text" placeholder="" style="width: 70%;" onblur="CalcularDv()" >
                                    -
                                    <input id="DigitoVerificacion" readonly="true" type="text" placeholder="" style="width: 12%;" >
                                </div>
                                <div class="col-md-2">Ciudad :</div>
                                <div class="col-md-4">
                                    <input id="Ciudad" type="text" readonly="true" placeholder="" style="width: 86%;" >
                                    <input id="idCiudad" type="text" style="display: none">
                                </div>

                            </div>
                            <div class="row">
                                <div class="col-md-2">Nombre Contacto:</div>
                                <div class="col-md-4"><input id="NombreContacto" readonly="true" type="text" placeholder="" style="width: 86%;" ></div>

                                <div class="col-md-2">Direccion :</div>
                                <div class="col-md-4"> 
                                    <input id="DireccionCliente" type="text" readonly="true" placeholder="" style="width: 73%;">
                                    <img src="/fintra/images/Direcciones.png" width="26" height="23" border="0" align="ABSMIDDLE" onclick="genDireccion('direccion', event);" alt="Direcciones"  title="Direcciones" />
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-2">Celular Contacto:</div>
                                <div class="col-md-4">
                                    <input id="CelularContacto" class="solo-numero" readonly="true" type="text" placeholder="" style="width: 86%;" maxlength="10">
                                </div>
                            </div>
                        </fieldset>
                    </div>
                </div>
            </div>

            <div class="ui-jqgrid ui-widget ui-widget-content center-block" style="width:100%;  margin-top: 30px">
                <div class="ui-dialog-titlebar ui-widget-header ui-helper-clearfix">
                    <span class="ui-dialog-title text-center" id="ui-dialog-title-listaGlobal">
                        &nbsp;
                        <label class="titulotablita"><b>Datos Solicitud</b></label>
                    </span>
                </div>
                <!-- **********************************************************************************************************************************************************
                                                                        INICIO Div Datos Solicitud
                *********************************************************************************************************************************************************** -->

                <div class="row"  style="padding: 5px;">
                    <div class="col-md-12 ">
                        <div id='formPrincipal'>
                            <fieldset  class="scheduler-border">
                                <legend class="scheduler-border">Datos Solicitud </legend>
                                <div class="row">
                                    <div class="col-md-9">
                                        <div class="row">
                                            <div class="col-md-2">Cliente :</div>
                                            <div class="col-md-4">
                                                <input id="txt_NombreCliente" readonly="true" type="text" placeholder="" style="width: 86%;" >
                                                <input type="hidden" id="idCliente" name="idCliente">

                                            </div>
                                            <div id="div_opd" style="display: ">
                                                <div class="col-md-2">OPD :</div>
                                                <div class="col-md-4">

                                                    <input id="OPD" type="text" placeholder="" style="width: 86%;" > 


                                                </div>
                                            </div>

                                        </div>
                                        <div class="row">
                                            <div class="col-md-2">Linea Negocio:</div>
                                            <div class="col-md-4">  
                                                <select id='cbx_LineaDeNegocio'  disabled="true" class="selectpicker" name="cbx_LineaDeNegocio"  style="width: 86%;"></select>                         
                                            </div>
                                            <!--                                            <div class="col-md-2">Tipo Solicitud :</div>
                                                                                        <div class="col-md-4">
                                                                                            <select id='cbx_TipoSolicitud' disabled="true" class="selectpicker" name="cbx_TipoSolicitud"  style="width: 86%;"></select>                         
                                                                                        </div>-->
                                            <div class="col-md-2">Nic :</div>
                                            <div class="col-md-4"> 
                                                <select id='cbx_nic' disabled="true" class="selectpicker" name="cbx_nic"  style="width: 60%;"></select>
                                            </div>

                                        </div>
                                        <div class="row">
                                            <div class="col-md-2">Responsable :</div>
                                            <div class="col-md-4"> 
                                                <!--<select id='cbx_Responsable' class="selectpicker" name="cbx_Responsable"  style="width: 86%;"></select>-->
                                                <input id="txt_responsable" readonly="true" type="text" placeholder="" style="width: 86%;" >
                                                <input id="idresponsable" hidden="true" type="text" placeholder="" style="width: 86%;" >
                                            </div>
                                            <div class="col-md-2">Fecha limite :</div>
                                            <div class="col-md-4"> 
                                                <input id="fecha_limite2" disabled="true" type="datetime" placeholder="" style="line-height: 10px;">
                                            </div>   
                                        </div>
                                        <div class="row" style="display: none">

                                            <div class="col-md-2">Interventor :</div>
                                            <div class="col-md-4"> 
                                                <select id='cbx_Interventor' class="selectpicker" name="cbx_Interventor"  style="width: 86%;"></select>

                                            </div>
                                        </div>
                                    </div>  
                                    <div class="col-md-3">
                                        <div class="row">
                                            <div class="col-md-4">IdSolicitud:</div>
                                            <div class="col-md-8"> 
                                                <input id="idSolicitud" readonly="true" type="text" placeholder="" style="width: 100%;">
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-4">Cartera :</div>
                                            <div class="col-md-8"> 
                                                <select id='cbx_Cartera' class="selectpicker" name="cbx_nic" disabled="true" style="width: 100%;"></select>
                                            </div>
                                        </div>
                                    </div>
                                </div>


                                <div class="row">
                                    <div class="col-md-9">
                                        <div class="row">

                                            <div class="col-md-2">Historia Solicitud :</div>
                                            <div class="col-md-10"> 
                                                <div class="form-group">
                                                    <textarea id="historiasolicitud" readonly="true" class="form-control" rows="5" ></textarea>
                                                </div>
                                            </div>   
                                        </div>
                                        <div class="row">
                                            <div class="col-md-2">Descripcion :<span style="color: red; ">*</span></div>
                                            <div class="col-md-10"> 
                                                <div class="form-group">
                                                    <textarea id="descripcion"  class="form-control" rows="5" ></textarea>
                                                </div>
                                            </div>   
                                        </div>   

                                    </div>
                                </div>


                        </div>
                        <div class="row">
                            <div class="col-md-9">
                                <div class="row">
                                    <div class="col-md-2">Fecha Creacion :</div>
                                    <div class="col-md-3"> 
                                        <input id="fechaCreacion" readonly="true" type="text" placeholder="" style="width: 100%;">
                                    </div>
                                </div>
                            </div>
                        </div>

                    </div>
                    <hr>
                    <!--            <div class="row">
                                    <div class="col-md-4 col-md-offset-4">
                    
                                        <div class="col-md-6 center-block">
                                            <button id="btn_crear_solicitud" class="ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only"onclick="modificarSolicitud()" >
                                                <span class="ui-button-text">Actualizar</span>
                                            </button>      
                                        </div>
                                        <div class="col-md-6 center-block">
                                            <button id="btn_Limpiar" class="ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only" onclick="Recargar();">
                                                <span class="ui-button-text">Cancelar</span>
                                            </button>      
                                        </div>
                                    </div>
                    
                    
                                </div>-->
                </div>
            </div>
        </div>

        <!-- **********************************************************************************************************************************************************
                                                                FIN Div Datos Solicitud
        *********************************************************************************************************************************************************** -->
        <!-- **********************************************************************************************************************************************************
                                                                creacion de solicitud
        *********************************************************************************************************************************************************** -->
        <div id="formulario" style="display: none">
            <div div="principal" class="ui-jqgrid ui-widget ui-widget-content center-block" style="width: 100%; height-min: 300px; ">

                <div class="ui-dialog-titlebar ui-widget-header ui-helper-clearfix">
                    <span class="ui-dialog-title text-center" id="ui-dialog-title-listaGlobal">
                        &nbsp;
                        <label class="titulotablita"><b>Crear Solicitud</b></label>
                    </span>
                </div>

                <div class="row" style="padding: 5px">


                    <div class="col-md-12 ">
                        <div id='formPrincipal'>
                            <fieldset  class="scheduler-border">
                                <legend class="scheduler-border">Datos Solicitud </legend>
                                <div class="row">
                                    <div class="col-md-2">Nom. Proyecto:<span style="color: red; ">*</span></div>
                                    <div class="col-md-10">
                                        <input id="txt_Nomproyecto" class="mayuscula" type="text" placeholder="" style="width: 95%;text-align: center" >
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-2">Cliente :<span style="color: red; ">*</span></div>
                                    <div class="col-md-4">
                                        <input id="txt_NombreClienteCS" class="mayuscula" type="text" placeholder="" style="width: 86%;" >
                                        <input type="hidden" id="idClienteCS" name="idClienteCS">
                                    </div>
                                    <div class="col-md-2">OPD :</div>
                                    <div class="col-md-4">
                                        <input id="OPD2" type="text" placeholder="" style="width: 60%;display: none;" >    
                                        <label style="font-weight: lighter;" onclick="adOPD()">
                                            <input id="chx_OPD" type="checkbox" value="" style="vertical-align: middle">
                                            <span style="vertical-align: middle">Activar/Desactivar</span> 
                                        </label>
                                    </div>
                                </div>
                                <div class="row" >
                                    <div class="col-md-2">Tipo Negocio:<span style="color: red; ">*</span></div>
                                    <div class="col-md-4">
                                        <select id='tipoNegocio' class="selectpicker" name="tipoNegocio"  onchange="" style="width: 86%;"></select>                         
                                    </div>
                                    
                                    <div class="col-md-2">Tipo Trabajo<span style="color: red; ">*</span></div>
                                    <div class="col-md-4">
                                        <select id='tipoTrabajo' class="selectpicker" name="tipoNegocio"  onchange=";" style="width: 86%;"></select>                         
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-2">Responsable :<span style="color: red; ">*</span></div>
                                    <div class="col-md-4"> 
<!--                                        <input id="txt_responsable2" readonly="true" type="text" placeholder="" style="width: 86%;" >
                                        <input id="idresponsable2" hidden="true" type="text" placeholder="" style="width: 86%;" >-->
                                        <select id='ResponsableCS' class="selectpicker" name="ResponsableCS"  style="width: 86%;"></select>

                                    </div>
                                     <div class="col-md-2">Nic :</div>
                                    <div class="col-md-4"> 
                                        <select id='cbx_nicCS' class="selectpicker" name="cbx_nicCS"  style="width: 60%;"></select>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-2">Fecha limite :</div>
                                        <div class="col-md-4"> 
                                            <input id="fecha_limite" type="datetime" placeholder="" style="line-height: 10px;">
                                        </div>   
                                    </div>
                                </div>   
                                <div class="row" style="display: none">
                                    <div class="col-md-2">Interventor :<span style="color: red; ">*</span></div>
                                    <div class="col-md-4"> 
                                        <select id='InterventorCS' class="selectpicker" name="InterventorCS"  style="width: 86%;"></select>

                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-md-2">Descripcion :<span style="color: red; ">*</span></div>
                                    <div class="col-md-8"> 
                                        <div class="form-group">
                                            <textarea id="descripcionCS"  class="form-control" rows="5" ></textarea>
                                        </div>
                                    </div>   


                                </div>
                                <div class="col-md-3 " style="display: none">

                                    <label>
                                        <input id="chx_envio_cartera" type="checkbox" value="">
                                        Guardar y Enviar
                                    </label>

                                </div>


                            </fieldset>

                            <!--                    <hr>
                                                <div class="row">
                                                    <div class="col-md-4"></div>
                            
                                                    <div class="col-md-2 center-block">
                                                        <button id="btn_crear_solicitudCS" class="ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only"onclick="crearSolicitud();" >
                                                            <span class="ui-button-text">Crear</span>
                                                        </button>      
                                                    </div>
                                                    <div class="col-md-2 center-block">
                                                        <button id="btn_Limpiar" class="ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only" onclick="Recargar()">
                                                            <span class="ui-button-text">Limpiar</span>
                                                        </button>      
                                                    </div>
                            
                            
                                                </div>-->
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <!-- **********************************************************************************************************************************************************
                                                                Ventana cambio de etapa y estado
        *********************************************************************************************************************************************************** -->

        <div id="cambioEtapa" style="display: none">
            <div class="row"style="padding: 5px">
                <div class="col-md-2 ">
                    <fieldset class="scheduler-border" style="width: 720px">

                        <div class="row">
                            <div class="col-md-2">
                                Idsolicitud :
                            </div>
                            <div class="col-md-4">
                                <input type="text" id="idsolicitud" readonly>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-2">
                                Etapa Actual:
                            </div>
                            <div class="col-md-4">
                                <input id="etapa" type="Text" readonly="true" style="width: 80%">
                                <input id="idetapa" hidden="true" type="Text" style="width: 80%">
                            </div>
                            <div class="col-md-2">
                                Estado Actual:
                            </div>
                            <div class="col-md-4">
                                <input id="estadoActual" readonly="true" type="Text" style="width: 80%">
                                <input id="idestadoActual" readonly="true" type="Text" style="width: 80%" hidden>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-2">
                                Estado :
                            </div>
                            <div class="col-md-4">
                                <select id="estado" class="selectpicker" style="width: 80%"></select>
                            </div>
                            <div class="col-md-2" hidden="true">
                                Causal :
                            </div>
                            <div class="col-md-4" hidden="true">
                                <select id="causal" class="selectpicker" style="width: 80%"></select>
                            </div>

                        </div>
                        <div class="row">
                            <div class="col-md-2">
                                Trazabilidad :
                            </div>
                            <div class="col-md-10">
                                <textarea id="trazabilidad" class="form-control" readonly="true" rows="5"></textarea>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-2">
                                Descripcion :
                            </div>
                            <div class="col-md-10">
                                <textarea id="descripcion_causal" class="form-control" rows="5"></textarea>
                            </div>
                        </div>



                    </fieldset>
                </div>
            </div>
        </div>

        <!------------------------------------------------------------------------->
        <!-- **********************************************************************************************************************************************************
                                                           nicio Div Definiciones de Acciones y Alcances Solicitud
        *********************************************************************************************************************************************************** -->


        <div id="div_coste_proyecto" class="container" style="display: none" >
            <!-------------------------------temporal---------------------------------------------------------->
            <table aling="center" style=" width: 100%"> 
                <tr>                          
                    <td colspan="12">
                        <h4>DATOS DE CLIENTE</h4>
                        <hr style="margin-top: -5px;">
                    </td>
                </tr>
                <tr>  
                    <td>Codigo Cliente:</td>
                    <td>
                        <input id="CodCliente" disabled="true" type="text" placeholder="" style="width: 72px;" >
                        <input id="num_solicitud" hidden="true" type="text" placeholder="" style="width: 72px;" >
                    </td>

                    <td>Nombre Cliente:</td>
                    <td>
                        <input id="idaccion" type="hidden">
                        <input id="NombreCliente2" disabled="true" type="text" placeholder="" style="width: 213px;" >
                    </td>

                    <td>Codigo Cotizacion:</td>
                    <td>
                        <input id="CodCotizacion" disabled="true" type="text" placeholder="" style="width: 72px" >
                    </td>

                    <td>Vigencia Cotizacion:</td>
                    <td>
                        <input type="datetime" id="fecha"  style="width: 100px ;height: 26px;"  readonly>
                    </td>

                    <td>Visualizacion:</td>
                    <td>
                        <select id="visualizacion" nombre="visualizacion" style="width: 150px; height: 19px" >
                            <option value='1'>Consolidado APU/Precio consolidado</option>
                            <option value='2'>Detalle del APU/Precio consolidado</option>
                            <option value='3'>Detalle del APU/Precio detallado</option>
                        </select>
                    </td>
                    <td>
                        Modalidad C.:
                    </td>
                    <td>
                        <select id="modalidad" nombre="modalidad" onchange="habdesMod();" style="width: 78px;margin-left: -4px" >
                            <option value='0'>Con Iva</option>
                            <option value='1'>Con AIU</option>
                        </select>
                    </td>
                </tr>     
            </table>
            <table aling="center" style=" width: 100%"> 
                <tr style="margin-top: 3px;">                          
                    <td colspan="12">
                        <h4>NEGOCIACI�N</h4>
                        <hr style="margin-top: -5px;">
                    </td>
                </tr>
                <tr>
                    <td>
                        Valor Cotizacion:
                    </td>
                    <td>
                        <input id="valcotizacion" disabled="true" type="text"  placeholder="" style="width: 110px;" > 
                    </td>
                    <td>
                        Porcentaje Descuento:
                    </td>
                    <td>
                        <input id="perc_desc" class="solo-numero" type="text" onkeyup="resta();" onchange="resta()"  placeholder="" style="width: 110px;" >
                    </td>
                    <td>
                        Valor Descuento:
                    </td>
                    <td>
                        <input id="valdesc" type="text" onkeyup="resta();" onchange="resta()"  placeholder="" style="width: 110px;" readonly="true" >
                    </td>
                    <td>
                        Subtotal:
                    </td>
                    <td>
                        <input id="subtotal" type="text" disabled="true" placeholder="" style="width: 110px;" >
                    </td>
                    <td>
                        Porcentaje Iva:
                    </td> 
                    <td>
                        <input id="perc_iva" type="text" disabled value="16"  placeholder="" style="width: 110px" >
                    </td>


                </tr>
                <tr>
                    <td>
                        Valor Iva:
                    </td>
                    <td>
                        <input id="valiva" type="text" disabled placeholder="" style="width: 110px" >
                    </td>
                    <td>
                        % Administracion:
                    </td>
                    <td>
                        <input id="perc_admon" type="text" disabled onkeyup="resta()" onchange="resta()" placeholder="" style="width: 110px;" >
                    </td>
                    <td>
                        Valor Administracion:
                    </td>
                    <td>
                        <input id="val_admon" type="text" disabled="true" placeholder="" style="width: 110px" >
                    </td>
                    <td>
                        % Imprevisto: 
                    </td>
                    <td>
                        <input id="perc_imprevisto" type="text" disabled onkeyup="resta()" onchange="resta()" placeholder="" style="width: 110px;" >
                    </td>
                    <td>
                        Valor Imprevisto:
                    </td>
                    <td>
                        <input id="val_imprevisto" type="text" disabled="true" placeholder="" style="width: 110px" >
                    </td>



                </tr>
                <tr>
                    <td>
                        % Utilidad:
                    </td>
                    <td>
                        <input id="perc_utilidad" type="text" disabled onkeyup="resta()" onchange="resta()" placeholder="" style="width: 110px;" >
                    </td>
                    <td>
                        Valor Utilidad:
                    </td>
                    <td>
                        <input id="val_utilidad" type="text" disabled="true" placeholder="" style="width: 110px" >
                    </td>
                    <td>
                        % AIU: 
                    </td>
                    <td>
                        <input id="perc_aiu" disabled type="text" placeholder="" style="width: 110px;" >
                    </td>
                    <td>
                        Valor AIU:
                    </td>
                    <td>
                        <input id="val_aiu" type="text" disabled="true" placeholder="" style="width: 110px" >
                    </td>
                    <td>
                        Total Cotizacion:
                    </td>
                    <td>
                        <input id="val_total" type="text" disabled="true" placeholder="" style="width: 110px" >
                    </td>
                </tr>
            </table>

            <!-------------------------------temporal---------------------------------------------------------->
            <table aling="center" style=" width: 100%"> 
                <tr>                          
                    <td colspan="6">
                        <h4>FORMAS DE PAGO Y GARANTIAS</h4>
                        <hr style="margin-top: -5px;">
                    </td>
                </tr>
                <td>
                    <label>Anticipo:</label>
                </td>
                <td>
                    <select id="anticipo" nombre="anticipo" onchange="habdesAnt();" style="width: 78px" >
                        <option value='1'>Si</option>
                        <option value='2' selected>No</option>
                    </select>
                </td>
                <td>
                    <label>% Anticipo: </label>
                </td>
                <td>
                    <input id="perc_anticipo" disabled onkeyup="resta();
                            calculateAmortizacion();
                            calculateFooter();" maxlength="2" onchange="resta()" type="text" placeholder="" style="width: 110px;" >
                </td>
                <td>
                    <label>Valor Anticipo:</label>
                </td>
                <td>
                    <input id="val_anticipo" type="text" disabled="true" placeholder="" style="width: 110px;" >
                </td>
                <td>
                    <label>Retegarantia:</label>
                </td>
                <td>
                    <select id="retegarantia" nombre="retegarantia" onchange="habdesRet()" style="width: 78px; margin-left: -4px" >
                        <option value='1'>Si</option>
                        <option value='2' selected>No</option>
                    </select>
                </td>  
                <td>
                    <label>% Retegarantia: </label>
                </td>
                <td>
                    <input id="perc_rete" disabled type="text" placeholder="" onkeyup="calculateretegarantia()" style="width: 72px;" >
                </td>
                </tr>
            </table>

            <table aling="center" style=" width: 100%">
                <tr>
                    <td style="width:90%;" colspan="3">
                        <fieldset style="height:275px">
                            <h4>FACTURAS PROYECTADAS</h4>
                            <hr style="margin-top: -5px;">
                            <div style="display: flex;justify-content:center" >
                                <div style="width: 200">
                                    <table aling="center" style=" width: 100%" >             
                                        <tr>
                                            <td>
                                                <table id="tabla_facturacion" ></table>
                                                <div id="page_tabla_facturacion"></div>
                                            </td>
                                        </tr>
                                    </table>
                                </div>
                            </div>
                        </fieldset>
                    </td>  
                </tr>  
            </table>
        </div>
        <div id="dialogLoading" style="display:none;">
            <p  style="font-size: 12px;text-align:justify;" id="msj2">Texto </p> <br/>
            <center>
                <img src="./images/cargandoCM.gif"/>
            </center>
        </div>
        <!-- Dialogo de los jsp> -->
        <div id="dialogMsj" style="display:none;">
            <p style="font-size: 12px;text-align:justify;" id="msj" > Texto </p>
        </div>
        <div id="div_Cargar_Archivos" style="display: none">
            <form name="form_Cargar_Archivos" id="form_Cargar_Archivos" method="post" enctype="multipart/form-data">

                <table aling="center" style=" width: 100%">
                    <tr>
                        <td>
                            <fieldset style="height:90px">
                                <legend>Carga De Archivos</legend>    
                                <table aling="center" style=" width: 100%" >             
                                    <tr>
                                        <td style="font-size: 12px;padding-right: 5px;width: 25%">
                                            Seleccione archivo
                                        </td>
                                        <td style="max-width: 300px">
                                            <input type="text" id="idsolicitud_carga_archivo" name="idsolicitud_carga_archivo" hidden="true">
                                            <input type="file" id="examinar" name="examinar"  style="font-size: 11px;" >                                                
                                        </td>

                                    </tr>
                                    <tr>
                                        <td></td>
                                        <td>

                                            <button id="subirArchivo" class="ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only" 
                                                    role="button" aria-disabled="false"style="width: 50px;margin-top: 5px" >
                                                <span if class="ui-button-text"  >Subir</span>
                                            </button> 

                                        </td>  
                                        <!--                                    <tr>
                                                                                <td style="font-size: 12px;">
                                                                                    Tipo Contrato
                                                                                </td>                                              
                                                                                <td>
                                                                                    <input type="radio" name="tipo_contrato" value="2">P�blico
                                                                                    <input type="radio" name="tipo_contrato" value="3" checked> Privado
                                                                                </td>         
                                                                            </tr>-->
                                    </tr>
                                </table>
                            </fieldset>
                        </td> 

                    </tr>
                    <tr>
                        <td>
                            <fieldset style="height:123px">
                                <legend style="padding-top: 15px">Archivos Cargados</legend>
                                <div style="height:123px;overflow:auto;">
                                    <table id="tbl_archivos_cargados" class="table table-hover" aling="center" style=" width: 100%" > 


                                    </table>

                                </div>
                            </fieldset>
                        </td> 
                    </tr>
                </table> 

            </form>
        </div>
        <!--        <div id="div_Coste_Proyecto" class="container" style="display: none" >
                    <table  style="width: 1317">
                        <tr>
                            <td valign="top" style="width: 30%">
                                <fieldset style="width: 90% ">
                                    <legend>TIPO FILTRO</legend> 
                                    <select id='tipo_Filtro' class="selectpicker" name="categoriaCliente"  style="width: 85%;" onchange="tipo_Filtro()">
                                        <option value='0'>...</option>
                                        <option value='1'>TIPO INSUMO</option>
                                        <option value='2'>WBS</option>
                                    </select>
                                </fieldset>
                                <fieldset style="width: 90%; margin-top: 14px" hidden="true" id="ww">
                                    <legend>Filtro</legend> 
                                    <div id="tipo_Wbs" style="margin-top: -19px;" hidden="true">
                                        <h6>Categoria :</h6>
                                        <select id='categoriaCliente' class="selectpicker" name="categoriaCliente"  style="width: 85%;"></select>
                                        <h6>Sub-Categoria :</h6>
                                        <select id='categoriaCliente' class="selectpicker" name="categoriaCliente"  style="width: 85%;"></select>
                                        <h6>Especificaci�n :</h6>
                                        <select id='categoriaCliente' class="selectpicker" name="categoriaCliente"  style="width: 85%;"></select>
                                    </div>  
                                    <div id="tipo_Insumo" style="margin-top: -19px" hidden="true">
                                        <h6>Area :</h6>
                                        <select id='categoriaCliente' class="selectpicker" name="categoriaCliente"  style="width: 85%;"></select>
                                        <h6>Disciplina :</h6>
                                        <select id='categoriaCliente' class="selectpicker" name="categoriaCliente"  style="width: 85%;"></select>
                                        <h6>Capitulo :</h6>
                                        <select id='categoriaCliente' class="selectpicker" name="categoriaCliente"  style="width: 85%;"></select>
                                    </div>
                                </fieldset>
                            </td>
                            <td style="width: 70%">
                                <fieldset style="width: 90%">
                                    <legend>NOMBRE</legend> 
                                    <table id="tabla_Coste_Proyecto" ></table>
                                    <div id="pager_Coste_Proyecto"></div>
                                </fieldset>
                            </td>
                        </tr>
                    </table>
                </div>-->


        <script src="./js/OportunidadNegocio/BuscarEditarSolicitud.js" type="text/javascript"></script>
        <script type="text/javascript" href="http://www.ddginc-usa.com/spanish/editcheck.js"></script>
        <script type="text/javascript" src="./js/jquery/jquery.jqGrid-4.7.0/js/grid.locale-es.js"></script>
        <script type="text/javascript" src="./js/jquery/jquery.jqGrid-4.7.0/js/jquery.jqGrid.min.js"></script>
        <script type="text/javascript" src="./js/jquery/jquery.jqGrid-4.7.0/js/jquery.tabletojson.js"></script> 


        <!------------------------------------------------------------------------->


    </center>
</body>
</html>