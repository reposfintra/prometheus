<%-- 
    Document   : CotizacionHistorico
    Created on : 8/08/2016, 09:58:12 AM
    Author     : Ing.William Siado
--%>



<%@page contentType="text/html" pageEncoding="utf-8"%>
<%@page import="com.tsp.operation.model.CompaniaService"%>
<%@page import="com.tsp.opav.model.DAOS.impl.CotizacionSlImpl"%>
<%@page import="com.tsp.opav.model.DAOS.CotizacionSlDAO"%>
<%@include file="/WEB-INF/InitModel.jsp"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
        <title></title>
        <link href="./css/popup.css" rel="stylesheet" type="text/css">
        <link type="text/css" rel="stylesheet" href="./css/jquery/jquery-ui/jquery-ui.css" />
        <link type="text/css" rel="stylesheet" href="./css/APU.css " />
        <link type="text/css" rel="stylesheet" href="./css/CotizacionHistorico.css "/>
        <link type="text/css" rel="stylesheet" href="./css/TransportadorasApi.css " />
        <script type="text/javascript" src="./js/jquery/jquery.jqGrid/js/jquery.min.js"></script>
        <script type="text/javascript" src="./js/jquery-ui-1.8.5.custom.min.js"></script>   
        <script type="text/javascript" src="./js/jquery/jquery.jqGrid-4.7.0/js/grid.locale-es.js"></script>
        <script type="text/javascript" src="./js/jquery/jquery.jqGrid-4.7.0/js/jquery.jqGrid.min.js"></script>
        <script type="text/javascript" src="./js/jquery/jquery.jqGrid-4.7.0/js/jquery.tabletojson.js"></script>  
        <link type="text/css" rel="stylesheet" href="./css/buttons/botonVerde.css"/>
        <script type="text/javascript" src="./js/OportunidadNegocio/CotizacionHistorico.js"></script>
        <script type="text/javascript" href="http://www.ddginc-usa.com/spanish/editcheck.js"></script>
        <link type="text/css" rel="stylesheet" href="./css/main2.css">
        <title>DEFINICION DE RENTABILIDAD Y ESQUEMA</title>

    </head>
    <body>

        <%            String idsolicitud = request.getParameter("num_solicitud");
        %>

        <input type="text" hidden="true" id="idsolicitud" value=<%=idsolicitud%>>
        <input type="text" hidden="true" id="idarea" value="">
        <input type="text" hidden="true" id="iddisciplina" value="">
        <input type="text" hidden="true" id="idcapitulo" value="">
        <input type="text" hidden="true" id="idActividad" value="">
        <input type="text" hidden="true" id="id_rel_actividades_apu" value="">
        <input type="text" hidden="true" id="id_rel_actividades_apu2" value="">
        <input type="text" hidden="true" id="valor_contratista_proyecto" value="0">
        <input type="text" hidden="true" id="costo_proyecto" value="0">

        <div id="capaSuperior" style="position:absolute; width:100%; height:100px; z-index:0; left: 0px; top: 0px;">
            <jsp:include page="/WEB-INF/toptsp.jsp?encabezado=DEFINICION DE RENTABILIDAD Y ESQUEMA"/>
        </div>

        <style>
            input{    
                width: 5px;
                color: black;
            }

        </style>

    <center>


        <div id="fformPrincipal" style="position:absolute; width:100%; height:85%; z-index:0; left: 0px; top: 88px; overflow: auto;">
            <table>
                <tr>
                    <td>
                <center>


                    <!--                    <div class="ui-jqgrid ui-widget ui-widget-content center-block" style="width: 768px; height-min: 300px; margin-top: 5px">
                                            <div class="ui-dialog-titlebar ui-widget-header ui-helper-clearfix">
                                                <span class="ui-dialog-title text-center" id="ui-dialog-title-listaGlobal">
                                                    &nbsp;
                                                    <label class="titulotablita"><b>Ingresar</b></label>
                                                </span>
                                            </div>
                                            <table style="margin-top: 10px;width: 90%">
                                                <tr>
                                                    <td style="width: 20%">
                                                        <label>% Esquema :</label>
                                                    </td>
                                                    <td style="width: 60%">
                                                        
                                                    </td>            
                                                    <td>
                                                        <button id="setEsquema" class="ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only" 
                                                                role="button" aria-disabled="false" onclick="ActualizarPorcentajes(1);">
                                                            <span class="ui-button-text">Setear Esq</span>
                                                        </button> 
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td style="width: 20%">
                                                        <label>% Contratista :</label>
                                                    </td>
                                                    <td style="width: 60%">
                                                        
                                                    </td>
                                                    <td>
                                                        <button id="setContratista" class="ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only" 
                                                                role="button" aria-disabled="false" onclick="ActualizarPorcentajes(2);">
                                                            <span class="ui-button-text">Setear Cont</span>
                                                        </button> 
                                                    </td>
                                                </tr>
                                                <tr>
                    
                                                </tr>
                                            </table>
                                        </div>-->

                    <div id="ww" style="margin-top:  5px">
                        <div id="tipo_Wbs" hidden="true">
                            <table id="tabla_Historico_cotizacion" ></table>
                            <div id="pager"></div>
                        </div>
                        <div id="divwbs" hidden="true">

                        </div>
                        <div id="tipo_Insumo" hidden="true">
                            <h1>tipo_Insumo</h1>
                        </div>
                        <div id="divinsumo" hidden="true">

                        </div>
                    </div

                    <!- -->
                    <div id="divemergente_filro" hidden="true">




                        <table id="tabla_Apu" ></table>
                        <div id="pager_Apu"></div> 

                    </div>


                </center>
                </td>
                </tr>
                <tr>

                    <td>
                <center>
                    <!--***************************************************************-->

                    <div hidden="true" class="ui-jqgrid ui-widget ui-widget-content center-block" style="width: 95%; height-min: 300px; margin-top: 5px">
                        <div class="ui-dialog-titlebar ui-widget-header ui-helper-clearfix">
                            <span class="ui-dialog-title text-center" id="ui-dialog-title-listaGlobal">
                                &nbsp;
                                <label class="titulotablita"><b>Datos Accion</b></label>
                            </span>

                        </div>
                        <div class="row"style="padding: 5px">
                            <div class="col-md-12 ">
                                <fieldset  class="scheduler-border" style="margin: 3px">
                                    <legend class="scheduler-border"><H2>Datos Cliente</H2></legend>

                                    <table style=" width: 100%;">
                                        <tr>

                                            <td>Codigo Cliente:</td>
                                            <td>
                                                <input id="CodCliente" disabled="true" type="text" placeholder="" style="width: 72px;" >
                                            </td>

                                            <td>Nombre Cliente:</td>
                                            <td>
                                                <input id="idaccion" type="hidden" >
                                                <input id="existe" name="existe" type="hidden">
                                                <input id="id" name="id" type="hidden">
                                                <input id="NombreCliente" disabled="true" type="text" placeholder="" style="width: 95%;" >
                                            </td>

                                            <td>Codigo Cotizacion:</td>
                                            <td>
                                                <input id="CodCotizacion" disabled="true" type="text" placeholder="" style="width: 72px" >
                                            </td>

                                            <td hidden="true">
                                                <label>Metodo Calculo:</label> 
                                            </td>
                                            <td hidden="true">
                                                <select id="metcalc" nombre="metcalc" style="width: 117px;" onchange="GridCotizacion();">
                                                    <option value='1'>Percentil 95</option>
                                                    <option value='2' selected>Ultimo Precio</option>
                                                    <option value='3'>Promedio Ultimo 3 Meses</option>
                                                    <option value='4'>Promedio Ultimo 6 Meses</option>
                                                </select>
                                            </td>

                                            <td hidden="true">Vigencia Cotizacion:</td>
                                            <td>
                                                <input hidden="true" type="datetime" id="fecha" placeholder="Escoja La Fecha de Vigencia..." name="fecha" style="height: 12px;width: 72px;color: #070708;" readonly>
                                            </td>

                                            <td hidden="true">Visualizacion:</td>
                                            <td hidden="true">
                                                <select id="visualizacion" nombre="visualizacion" style="width: 150px; height: 19px" >
                                                    <option value='1'>Consolidado APU/Precio consolidado</option>
                                                    <option value='2'>Detalle del APU/Precio consolidado</option>
                                                    <option value='3'>Detalle del APU/Precio detallado</option>
                                                </select>
                                            </td>
                                            <td hidden="true">
                                                <label>Modalidad C.:</label>
                                            </td>
                                            <td hidden="true">
                                                <select id="modalidad" nombre="modalidad" onchange="habdesMod();" style="width: 78px;margin-left: -4px" >
                                                    <option value='0'>Con Iva</option>
                                                    <option value='1'>Con AIU</option>
                                                </select>
                                            </td>

                                        </tr>
                                    </table>

                                </fieldset>
                                <br>
                                <fieldset  class="scheduler-border" style="margin: 3px" hidden="true">
                                    <legend class="scheduler-border">Calculo de Valores</legend>

                                    <table style=" width: 100%;">
                                        <tr>
                                            <td>
                                                <label>Valor Cotizacion:</label>
                                            </td>
                                            <td>
                                                <input id="valcotizacion" disabled="true" type="text"  placeholder="" style="width: 72px;" > 
                                            </td>
                                            <td>
                                                <label> Valor Descuento:</label>
                                            </td>
                                            <td>
                                                <input id="valdesc" type="text" value=0 onkeyup="resta()" onchange="resta()" onkeypress="return decimales(event, this);"   placeholder="" style="width: 72px;" >
                                            </td>
                                            <td>
                                                <label>Subtotal:</label>
                                            </td>
                                            <td>
                                                <input id="subtotal" type="text" disabled="true" placeholder="" style="width: 72px;" >
                                            </td>
                                            <td>
                                                <label>Porcentaje Iva:</label>
                                            </td> 
                                            <td>
                                                <input id="perc_iva" type="text" disabled value="16"  placeholder="" style="width: 72px" >
                                            </td>
                                            <td>
                                                <label>Valor Iva:</label>
                                            </td>
                                            <td>
                                                <input id="valiva" type="text" disabled placeholder="" style="width: 72px" >
                                            </td>
                                            <td>
                                                <label style="margin-left: -17px;">% Administracion:</label>
                                            </td>
                                            <td>
                                                <input id="perc_admon" onkeypress="return decimales(event, this);" maxlength="5" type="text" value=0 disabled onkeyup="resta()" onchange="resta()" placeholder="" style="width: 72px;margin-left: -4px;" >
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <label>Valor Administracion:</label>
                                            </td>
                                            <td>
                                                <input id="val_admon" type="text" disabled="true" placeholder="" style="width: 72px" >
                                            </td>
                                            <td>
                                                <label>% Imprevisto:</label> 
                                            </td>
                                            <td>
                                                <input id="perc_imprevisto" onkeypress="return decimales(event, this);" maxlength="5" type="text" value=0 disabled onkeyup="resta()" onchange="resta()" placeholder="" style="width: 72px;" >
                                            </td>
                                            <td>
                                                <label>Valor Imprevisto:</label>
                                            </td>
                                            <td>
                                                <input id="val_imprevisto" type="text" disabled="true" placeholder="" style="width: 72px" >
                                            </td>
                                            <td>
                                                <label> % Utilidad:</label>
                                            </td>
                                            <td>
                                                <input id="perc_utilidad" onkeypress="return decimales(event, this);" maxlength="5" type="text" value=0 disabled onkeyup="resta()" onchange="resta()" placeholder="" style="width: 72px;" >
                                            </td>
                                            <td>
                                                <label>Valor Utilidad:</label>
                                            </td>
                                            <td>
                                                <input id="val_utilidad" type="text" disabled="true" placeholder="" style="width: 72px" >
                                            </td>
                                            <td>
                                                <label>% AIU:</label> 
                                            </td>
                                            <td>
                                                <input id="perc_aiu" disabled type="text" placeholder="" style="width: 72px; margin-left: -4px" >
                                            </td>

                                        </tr>
                                        <tr>
                                            <td>
                                                <label>Valor AIU:</label>
                                            </td>
                                            <td>
                                                <input id="val_aiu" type="text" disabled="true" placeholder="" style="width: 72px" >
                                            </td>
                                            <td>
                                                <label>Total Cotizacion:</label>
                                            </td>
                                            <td>
                                                <input id="val_total" type="text" disabled="true" placeholder="" style="width: 37%" >
                                            </td>
                                            <td>
                                                <label>Anticipo:</label>
                                            </td>
                                            <td>
                                                <select id="anticipo" nombre="anticipo" onchange="habdesAnt();" style="width: 78px" >
                                                    <option value='1'>Si</option>
                                                    <option value='2' selected>No</option>
                                                </select>
                                            </td>
                                            <td>
                                                <label>% Anticipo:</label> 
                                            </td>
                                            <td>
                                                <input id="perc_anticipo" onkeypress="return decimales(event, this);" maxlength="5" value=0 disabled onkeyup="resta()" onchange="resta()" type="text" placeholder="" style="width: 72px;" >
                                            </td>
                                            <td>
                                                <label>Valor Anticipo:</label>
                                            </td>
                                            <td>
                                                <input id="val_anticipo" type="text" disabled="true" placeholder="" style="width: 72px;" >
                                            </td>
                                            <td>
                                                <label>Retegarantia:</label>
                                            </td>
                                            <td>
                                                <select id="retegarantia" nombre="retegarantia" onchange="habdesRet()" style="width: 78px; margin-left: -4px" >
                                                    <option value='1'>Si</option>
                                                    <option value='2' selected>No</option>
                                                </select>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <label>% Retegarantia:</label> 
                                            </td>
                                            <td>
                                                <input id="perc_rete" onkeypress="return decimales(event, this);" maxlength="5" disabled type="text" value=0 placeholder="" style="width: 72px;" >
                                            </td>
                                            <td>
                                                <label>Metodo Calculo:</label> 
                                            </td>
                                            <td>
                                                <select id="metcalc" nombre="metcalc" style="width: 117px;" onchange="GridCotizacion();">
                                                    <option value='1'>Percentil 95</option>
                                                    <option value='2' selected>Ultimo Precio</option>
                                                    <option value='3'>Promedio Ultimo 3 Meses</option>
                                                    <option value='4'>Promedio Ultimo 6 Meses</option>
                                                </select>
                                            </td>
                                        </tr>
                                    </table>
                                </fieldset>
                            </div>
                        </div>
                    </div>
                    <!--***************************************************************-->
                </center>
                </td>
                </tr>
                <tr>
                    <td>
                <center>
                    <table>
                        <tr>
                            <td colspan="2"></td>
                            <td>

                            </td> 
                        </tr>
                        <tr>
                            <td valign="top" style="max-height: 650px">

                                <div id='divListaNegocios' valign="top">
                                    <table>
                                        <tr>
                                            <td valign="top" style="width: 50%">
                                                <h2 style="color: #234684">Porcentajes Globales </h2>
                                                <hr style="color: #234684;margin-top: -12px" >
                                                <div>
                                                    <table id="tabla_porcentajes"  ></table>
                                                    <div id="pager_porcentajes"></div>
                                                </div>
                                            </td>
                                            <td valign="top" style="width: 50%">
                                                <h2 style="color: #234684">Ingreso Global Rentanbilidad Esquema </h2>
                                                <hr style="color: #234684;margin-top: -12px" >                                                
                                                <label>Rentabilidad Esquema :</label>
                                                <select id="cbx_rent_esquema" name="dep_dir" onchange="set_rent_esquema()" style="width: 250px;margin-left: 10px;height: 20px"></select>

                                            </td>
                                        </tr>
                                    </table>
                                    <h2 style="color: #234684">Filtro </h2>
                                    <hr style="color: #234684;margin-top: -12px" >
                                    <div>

                                        <table id="tabla_Areas"  ></table>
                                        <div id="pager_Areas"></div>
                                    </div>
                                    <div style="margin-top: 10px">
                                        <table id="tabla_Disciplina" ></table>
                                        <div id="pager_Disciplina"></div> 
                                    </div>
                                    <div style="margin-top: 10px">
                                        <table id="tabla_Capitulo" ></table>
                                        <div id="pager_Capitulo"></div>
                                    </div>
                                    <div style="margin-top: 10px">
                                        <table id="tabla_Actividades" ></table>
                                        <div id="pager_Actividades"></div>
                                    </div>


                                </div>  
                            </td>
                            <td valign="top">
                                <div id="divbarra">
                                    <div id ="btnoculto" class="divOcultar">
                                        <a href="#verocultar" id="boton"  > <img src='/fintra/images/botones/iconos/filtro2.png' align='absbottom' name='carta' width='26' height='25'> </a>
                                        <input name="campoVisible" id="campoVisible" type="hidden" value="0" />
                                    </div>
                                    <div onclick="reiniciar_filtro()" style="margin-top: 5px ">
                                        <a href="#jaja"> <img src='/fintra/images/botones/iconos/boton-arriba.png' align='absbottom' name='carta' width='26' height='25'> </a>
                                    </div>

                                </div>
                            </td>
                            <td valign="top" style="width: 1608px; min-width: 800px;" >

                                <!--                                    <input id="percesquema"  onkeypress="return onKeyDecimal(event, this)" type="text" placeholder="" style="width: 80px;margin-top: 10px;margin-left: 1149px;position: absolute;z-index: 2;text-align: center;" >
                                                                    <input id="percecontratista"  onkeypress="return onKeyDecimal(event, this)" type="text" placeholder="" style="width: 80px;margin-top: 10px;margin-left: 1319px;position: absolute;z-index: 2;text-align: center;" >-->
                                <table id="tbl_cotizacion" >

                                </table>                
                                <div id="page_cotizacion"></div>




                            </td>
                        </tr>

                    </table>


                    <div style="margin-top: 10px" hidden="true" >
                        <table id="tbl_cotizacion_detalle" ></table>                
                        <div id="page_cotizacion_detalle"></div>
                    </div>
                </center>
                </td>
                </tr>

            </table>
        </div>
    </center>


    <!-- Dialogo de los jsp> -->
    <div id="dialogMsj1" style="display:none;">
        <p style="font-size: 12px;text-align:justify;" id="msj1" > Texto </p>
    </div>

</div>



<!-----------div para cargar los apus a la cotizacion------------------>
<div id="div_apus"  style="display: none; width: 800px" > 


    <div id="filtros" class="ui-jqgrid ui-widget ui-widget-content ui-corner-all" style="height:34px;width: 425px;padding: 0px 10px 5px 10px">
        <center>

            <table style=" padding: 0.5em 1em 0.5em 1em" style="width: 95%;">

                <input type="hidden" id="idregistro" name="idregistro">

                <tr>
                    <td>Grupos APUs</td>
                    <td>
                        <select id="grupo_apu1" nombre="grupo_apu1" style="width: 264px;" >
                            <!--option value=''>...</option-->
                        </select>
                    </td>
                </tr>
            </table>
        </center>
    </div> 

    <div style="position:absolute; left: 1%; top: 13%; width: 40% ">
        <center>
            <table id="tbl_apus" ></table>                
            <div id="page_apus"></div>
        </center>
    </div>

    <div style="position:absolute; left: 53%; top: 13%; width: 40% ">
        <center>
            <table id="tbl_apus_asoc" ></table>                
            <div id="page_apus_asoc"></div>
        </center>
    </div>

    <div id="bt_asociar_apu" title="Asociar APU" style="position:absolute; left: 47%; top: 48%; width: 10%;"> 
        <span aling="center"  class="form-submit-button form-submit-button-simple_green_apple" onClick="AgregarGridAso()"> >> </span>
    </div>
    <div id="bt_desasociar_apu" title="Desasociar APU" style="position:absolute; left: 47%; top: 62%; width: 10%;"> 
        <span aling="center"  class="form-submit-button form-submit-button-simple_green_apple" onClick="AgregarGridNoAso()"> << </span>
    </div>

</div>

<!-------------------crear APU-------------------------------->
<div id="div_apu"  style="display: none; width: 800px" >   
    <center>
        <div id="filtros" class="ui-jqgrid ui-widget ui-widget-content ui-corner-all" style="height: 93px; width: 445px">
            </br>
            <table aling="center" style=" width: 100%" >
                <tr>
                    <td>Grupo APU:</td>
                    <td>
                        <select id="grupo_apu" nombre="grupo_apu" style="width: 264px;" >
                            <!--option value=''>...</option-->
                        </select>
                    </td>
                    <td><img id = "add_grupo" src = "/fintra/images/botones/iconos/adds.png"
                             style = "margin-left: 5px; height: 19px; vertical-align: middle;" 
                             onclick = "crearGrupoApu();"></td>
                </tr> 
                <tr>
                    <td>Unidad Medida:</td>
                    <td>
                        <select id="unmed" nombre="unmed" style="width: 264px;" >
                            <!--option value=''>...</option-->
                        </select>
                    </td>
                    <td><img id = "add_unidadmed" src = "/fintra/images/botones/iconos/adds.png"
                             style = "margin-left: 5px; height: 19px; vertical-align: middle;" 
                             onclick = "crearUnidadMedida();"></td>
                </tr>
                <tr>
                    <td>Nombre APU:</td>
                    <td style="width: 50%">
                        <input type="text" id="nomapu" name="nomapu" style=" width: 248px">
                        <input type="hidden" id="idapu" name="idapu">
                    </td>
                </tr>
            </table>
        </div>  
        </br> 

        <!---------Grid que contiene los insumos ----------->
        <div style="margin-top: 0px">
            <table id="tbl_insumos" ></table>                
            <div id="page_insumos"></div>
        </div>
        <!-------------------->

        <div id="div_filtro_insumos"  style="display: none; width: 800px" >
            <div id="filtros" class="ui-jqgrid ui-widget ui-widget-content ui-corner-all" style="height: 95px;padding: 0px 10px 5px 10px">
                <center>

                    <table style=" padding: 0.5em 1em 0.5em 1em" style="width: 95%;">

                        <input type="hidden" id="idregistro" name="idregistro">

                        <tr>
                            <td>Categoria</td>
                            <td>
                                <select id="categoria" nombre="categoria" onchange="cargarCombo('sub', [this.value])" style="width: 264px;" >
                                    <option value=''>...</option>
                                </select>
                            </td>
                        </tr>
                        <tr>
                            <td>SubCategoria</td>
                            <td>
                                <select id="sub"  style="width: 264px;" >
                                    <option value=''>...</option>
                                </select>

                                <input type="hidden" name="nomsub" id="nomsub">
                            </td>
                        </tr>
                    </table>
                    <hr>
                    <div id ='botones'>
                        <button id="buscar" class="ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only" 
                                role="button" aria-disabled="false">
                            <span class="ui-button-text">Buscar</span>
                        </button> 
                    </div>
                </center>
            </div>  
            </br>  

            <!---------Grid que contiene filtro de insumos ----------->
            <div style="margin-top: 0px">
                <center>
                    <table id="tbl_filtro_insumos" ></table>                
                    <div id="page_filtro_insumos"></div>
                </center>
            </div>
            <!-------------------->

            <!--------div materiales------------>
            <div style="margin-top: 0px">
                <center>
                    <table id="tbl_materiales" ></table>                
                    <div id="page_materiales"></div>
                </center>
            </div>

        </div>

    </center>
</div>

<!-------------------crear grupo APU-------------------------------->
<div id="div_grupo_apu"  style="display: none; width: 800px" >                       
    <div id="filtros" class="ui-jqgrid ui-widget ui-widget-content ui-corner-all" style="height: 110px;">
        </br>
        <table aling="center" style=" width: 100%" >
            <tr>
                <td style="width: 10%"><span>Nombre Grupo</span></td>                          
                <td style="width: 50%"><input type="text" id="nomgrupo" name="nomgrupo" style=" width: 248px">
                </td>  
            </tr> 
            <tr>
                <td style="width: 10%"><span>Descripcion Grupo:</span></td>   
                <td style="width: 90%" colspan="4"><textarea id ="descgrupo" name="descgrupo" style="width:535px;resize:none" rows="2" ></textarea></td>                        
            </tr>
        </table>
    </div>  
    </br> 
</div>

<!-------------------crear Unidad Medida-------------------------------->
<div id="div_unidad_medida"  style="display: none; width: 800px" >                       
    <div id="filtros" class="ui-jqgrid ui-widget ui-widget-content ui-corner-all" style="height: 53px;">
        </br>
        <table aling="center" style=" width: 100%" >
            <tr>
                <td style="width: 20%"><span>Unidad de Medida</span></td>                          
                <td style="width: 50%"><input type="text" id="nomunidad" name="nomunidad" style=" width: 280px">
                </td>  
            </tr> 
        </table>
    </div>  
    </br> 
</div>

<!--        <div id="dialogMsgMeta" title="Mensaje" style="display:none; visibility: hidden">
            <p style="font-size: 12.5px;text-align:justify;" id="msj" > Texto </p>
        </div> -->
</center>
</div>
<div id="dialogMsj" style="display:none;">
    <p style="font-size: 12px;text-align:justify;" id="msj" > Texto </p>
</div>
<div id="dialogMsgMeta" title="Mensaje" style="display:none; visibility: hidden">
    <p style="font-size: 12.5px;text-align:justify;" id="msj1" > Texto </p>
</div> 



<div id="loader-wrapper">
    <div id="loader"></div>
    <img id="logowast" src="/fintra/images/Selectrik.png" style="z-index: 10002 !important">
    <div class="loader-section section-left">	
    </div>
    <div class="loader-section section-right"></div>
</div>
</body>
</html>

