<!-- **********************************************************************************************************************************************************
                          Crear Solicitud
*********************************************************************************************************************************************************** -->

<div div="principal" class="ui-jqgrid ui-widget ui-widget-content center-block" style="width: 50%; height-min: 300px; ">

    <div class="ui-dialog-titlebar ui-widget-header ui-helper-clearfix">
        <span class="ui-dialog-title text-center" id="ui-dialog-title-listaGlobal">
            &nbsp;
            <label class="titulotablita"><b>Crear Solicitud</b></label>
        </span>
    </div>

    <div class="row" style="padding: 5px">


        <div class="col-md-12 ">
            <div id='formPrincipal'>
                <fieldset  class="scheduler-border">
                    <legend class="scheduler-border">Datos Solicitud </legend>
                    <div class="row">
                        <div class="col-md-2">Nombre Proyecto:</div>
                        <div class="col-md-10">
                            <input id="txt_Nomproyecto" type="text" placeholder="" style="width: 95%;text-align: center" >
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-2">Cliente :</div>
                        <div class="col-md-4">
                            <input id="txt_NombreCliente" type="text" placeholder="" style="width: 86%;" >
                            <input type="hidden" id="idCliente" name="idCliente">
                        </div>
                        <div class="col-md-2">OPD :</div>
                        <div class="col-md-4">
                            <input id="OPD" type="text" placeholder="" style="width: 86%;" >                            
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-2">Linea de Negocio:</div>
                        <div class="col-md-4">
                            <select id='cbx_LineaDeNegocio' class="selectpicker" name="cbx_LineaDeNegocio"  onchange="cargarCbxTipoSolicitud();" style="width: 86%;"></select>                         
                        </div>
                        <div class="col-md-2">Tipo Solicitud :</div>
                        <div class="col-md-4">
                            <select id='cbx_TipoSolicitud' class="selectpicker" name="cbx_TipoSolicitud"  style="width: 86%;"></select>                         
                        </div>


                    </div>
                    <div class="row">
                        <div class="col-md-2">Responsable :</div>
                        <div class="col-md-4"> 
                            <select id='cbx_Responsable' class="selectpicker" name="cbx_Responsable"  style="width: 86%;"></select>
                        </div>

                        <div class="col-md-2">Interventor :</div>
                        <div class="col-md-4"> 
                            <select id='cbx_Interventor' class="selectpicker" name="cbx_Interventor"  style="width: 86%;"></select>

                        </div>
                        <div class="col-md-2">Nic :</div>
                        <div class="col-md-4"> 
                            <select id='cbx_nic' class="selectpicker" name="cbx_nic"  style="width: 60%;"></select>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-2">Descripcion :</div>
                        <div class="col-md-8"> 
                            <div class="form-group">
                                <textarea id="descripcion"  class="form-control" rows="5" ></textarea>
                            </div>
                        </div>   


                    </div>


                </fieldset>

                <hr>
                <div class="row fx-distribuir-between">
                    <div class="col-md-3 checkbox">

                        <div class="col-md-3 checkbox">

                            <label>
                                <input id="chx_envio_cartera" type="checkbox" value="">
                                Guardar y Enviar
                            </label>

                        </div>

                    </div>

                    <div class="col-md-6 fx-distribuir-around">
                        <div class="col-md-6 fx-center">
                            <button id="btn_crear_solicitud"  class="ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only"onclick="crearSolicitud();" >
                                <span class="ui-button-text">Crear</span>
                            </button> 
                        </div>
                        <div class="col-md-6 fx-center">
                            <button id="btn_Limpiar" class="ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only" onclick="Recargar()">
                                <span class="ui-button-text">Limpiar</span>
                            </button> 
                        </div>



                    </div>

                    <div class="col-md-3 "></div>


                </div>
            </div>
        </div>
    </div>
</div>







<script src="./js/OportunidadNegocio/Solicitud.js" type="text/javascript"></script>


<!------------------------------------------------------------------------->