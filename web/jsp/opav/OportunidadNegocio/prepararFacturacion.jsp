<%-- 
    Document   : prepararFacturacion
    Created on : 11/01/2017, 03:30:59 PM
    Author     : mcastillo
--%>

<%@page contentType="text/html" pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
        <link href="./css/popup.css" rel="stylesheet" type="text/css">
        <link type="text/css" rel="stylesheet" href="./css/jquery/jquery-ui/jquery-ui.css" />
        <link type="text/css" rel="stylesheet" href="./css/TransportadorasApi.css " />
        <script type="text/javascript" src="./js/jquery/jquery.jqGrid/js/jquery.min.js"></script>
        <script type="text/javascript" src="./js/jquery/jquery-ui/jquery.ui.min.js"></script> 
        <script type="text/javascript" src="./js/jquery-ui-1.8.5.custom.min.js"></script>   
        <script type="text/javascript" src="./js/jquery/jquery.jqGrid-4.7.0/js/grid.locale-es.js"></script>
        <script type="text/javascript" src="./js/jquery/jquery.jqGrid-4.7.0/js/jquery.jqGrid.min.js"></script>
        <script type="text/javascript" src="./js/jquery/jquery.jqGrid-4.7.0/js/jquery.tabletojson.js"></script>    
        <script type="text/javascript" src="./js/OportunidadNegocio/prepararFacturacion.js"></script> 
        <script type="text/javascript" src="./js/jquery/jquery.jqGrid-4.7.0/plugins/jquery.contextmenu.js"></script>
        <title>PREPARAR FACTURACION</title>
    </head>
    <body>
        <div id="capaSuperior" style="position:absolute; width:100%; height:100px; z-index:0; left: 0px; top: 0px;">
            <jsp:include page="/WEB-INF/toptsp.jsp?encabezado=PREPARAR FACTURACION"/>
        </div>
    <center>
        <%
            String num_solicitud = (request.getParameter("num_solicitud") != null) ? request.getParameter("num_solicitud") : "";
        %>
        <div id="tablita" class="ui-jqgrid ui-widget ui-widget-content center-block" style="width: 645px; height-min: 300px;margin-top: 10px">
            <div class="ui-dialog-titlebar ui-widget-header ui-helper-clearfix">
                <span class="ui-dialog-title text-center" id="ui-dialog-title-listaGlobal">
                    &nbsp;
                    <label class="titulotablita" ><b>SOLICITUD</b></label>
                </span>
            </div>
            <table id="tablainterna" style="height: 53px; width: 615px"  >
                <tr>
                    <td>
                        <label>Id solictud</label>
                    </td>
                    <td>
                        <input type="text"  id="id_solicitud"  style="width: 125px;margin-left: 5px;height: 15px;" readonly="true" value="<%=num_solicitud%>">
                    </td>                 
                </tr>
                <tr>
                    <td>
                        <label>Nombre del proyecto</label>
                    </td>
                    <td>
                        <input type="text" id="nombre_proyecto"  readonly="true" style="width: 371px;margin-left: 5px;height: 15px;" >
                    </td>
                </tr>
            </table>
        </div>
        <div id="tablita" class="ui-jqgrid ui-widget ui-widget-content center-block" style="width: 430px; height-min: 300px;margin-top: 26px;padding: 8px;">
            <table id="tablainterna" style="height: 53px; width: 370px"  >
                <tr>
                    <td>
                        <label>Valor venta</label>
                    </td>
                    <td>
                        <input type="text" id="valor_facturar"  style="width: 189px;margin-left: 5px;height: 15px;">
                    </td>
                </tr>
                <tr>
                    <td>
                        <label>Valor Material</label>
                    </td>
                    <td>
                        <input type="text" id="valor_material"  style="width: 189px;margin-left: 5px;height: 15px;">
                    </td>
                </tr>
                <tr>
                    <td>
                        <label>Valor IVA</label>
                    </td>
                    <td>
                        <input type="text" id="valor_iva"  style="width: 189px;margin-left: 5px;height: 15px;" readonly>
                    </td>
                </tr>
                <tr>
                    <td>
                        <label>Valor contratista</label>
                    </td>
                    <td>
                        <input type="text" id="valor_contratista"  style="width: 189px;margin-left: 5px;height: 15px;" readonly>
                    </td>
                </tr>              
                <tr>
                    <td>
                        <label style="font-weight: bold">SubTotal</label>
                    </td>
                    <td>
                        <input type="text" id="subtotal"  style="width: 189px;margin-left: 5px;height: 15px;font-weight: bold" readonly>
                    </td>
                </tr>             
                <tr>
                    <td colspan="2">
                        <div style="padding-top: 10px">
                            <center>
                                <button id="aceptar" class="ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only" style="margin-left: 15px;top: -7px;" > 
                                    <span class="ui-button-text">Aceptar</span>
                                </button>  
                                <button id="cancelar" onclick=" window.close();" class="ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only lab" style="margin-left: 15px;top: -7px;" > 
                                    <span class="ui-button-text">Cancelar</span>
                                </button>  
                            </center>
                        </div>
                    </td>
                </tr>
            </table>
        </div>
        <div id="dialogMsj" style="display:none;">
            <p style="font-size: 12px;text-align:justify;" id="msj" > Texto </p>
        </div>
    </center>  
</body>
</html>
