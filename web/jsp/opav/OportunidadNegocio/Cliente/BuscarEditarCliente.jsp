<%-- 
    Document   : OportunidadNegocio
    Created on : 15/04/2016, 09:11:26 AM
    Author     : William Siado Torres
--%>

<%@page contentType="text/html" pageEncoding="utf-8"%>
<!DOCTYPE html>
<html>

    <head>

        <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
        <meta name="viewport" content="width=device-width, initial-scale=1">


        <title>Oportunidad De Negocio</title>



        <link href="./css/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css"/>
        <link href="./css/simple-sidebar.css" rel="stylesheet" type="text/css"/>
        <link href="./css/OportunidadNegocio.css" rel="stylesheet" type="text/css"/>




        <link type="text/css" rel="stylesheet" href="./css/jquery/jquery-ui/jquery-ui.css"/>     
        <script type="text/javascript" src="./js/jquery/jquery-ui/jquery.min.js"></script>
        <script type="text/javascript" src="./js/jquery/jquery-ui/jquery.ui.min.js"></script> 
        <script src="./js/jquery/editor/ckeditor/ckeditor.js" type="text/javascript"></script>



        <link type="text/css" rel="stylesheet" href="./css/buttons/botonVerde.css"/>
        <script type="text/javascript" src="./js/jquery/jquery.jqGrid/js/grid.locale-es.js"></script>
        <script type="text/javascript" src="./js/jquery/jquery.jqGrid/js/jquery.jqGrid.min.js"></script>
        <link type="text/css" rel="stylesheet" href="./css/jquery/jquery.jqGrid/ui.jqgrid.css"/>    


    </head> 
    <body>
    <center>
        <jsp:include page="/WEB-INF/toptsp.jsp?encabezado=Modulo Oportunidad De Negocio"/>
    </center>


    <!-- **********************************************************************************************************************************************************
                               Buscar Editar Cliente
    *********************************************************************************************************************************************************** -->

    <div class="ui-jqgrid ui-widget ui-widget-content center-block" style="margin-top: 10px; width: 50%; height-min: 300px;width: 796px;">

        <div class="ui-dialog-titlebar ui-widget-header ui-helper-clearfix">
            <span class="ui-dialog-title text-center" id="ui-dialog-title-listaGlobal">
                &nbsp;
                <label class="titulotablita"><b>Busqueda - Edicion De Cliente</b></label>
            </span>
        </div>

        <div class="row" style="padding: 5px">

            <div class="col-md-12 ">

                <fieldset class="scheduler-border">
                    <legend class="scheduler-border">Busqueda De Cliente</legend>
                    <div class="row">


                        <div class="col-md-2">Tipo Busqueda  :</div>
                        <div class="col-md-4">
                            <select id='filtro' class="selectpicker" name="filtro"  style="width: 100%;" onchange="autocompletarNombre()" >
                                <option value="1">NIT</option>
                                <option value="2">Nombre</option>
                            </select>
                        </div>
                        <div class="col-md-3">
                            <input id="busqueda"  class="mayuscula" btype="text" placeholder="" style="width: 100%;">
                            <input type="hidden" id="idcliente" name="idCliente">
                        </div>
                        <div class="col-md-3">

                            <button id="btn_Aceptar_tipoCliente" class="ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only" onclick="buscar()" >
                                <span class="ui-button-text">Buscar</span>
                            </button> 


                        </div>


                    </div>
                </fieldset>
            </div>


            <div class="col-md-12 ">            

                <div id='formPrincipal'style="display: none" >
                    <fieldset  class="scheduler-border">
                        <legend class="scheduler-border">Datos Clientes</legend>


                        <div class="row" id="div_clientepadre" style="display: none">
                            <div class="col-md-2">Cliente Padre :<span style="color: red; font-size: 17px;">*</span></div>
                            <div class="col-md-4"> 
                                <input id="ClientePadre" type="text" placeholder="" style="width: 86%;">
                                <input type="hidden" id="idClientePadre" name="idClientePadre">
                            </div>   
                            <div class="col-md-2">Nit Padre :</div>
                            <div class="col-md-4">
                                <input id="nitPadre" type="text" readonly="true" placeholder="" style="width: 86%;" >
                            </div>

                        </div>



                        <div class="row">
                            <div class="col-md-2">Nombre :<span style="color: red; font-size: 17px;">*</span></div>
                            <div class="col-md-4"><input id="NombreCliente" class="mayuscula" type="text" placeholder="" style="width: 86%;" ></div>
                            <div class="col-md-2">Departamento :<span style="color: red; font-size: 17px;">*</span></div>
                            <div class="col-md-4">
                                <input id="Departamento" type="text" readonly="true" placeholder="" style="width: 86%;" >
                                <input id="idDepartamento" type="text"   readonly="true" placeholder=""  style="display: none" >

                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-2">Nit :<span style="color: red; font-size: 17px;">*</span></div>
                            <div class="col-md-4">
                                <input id="NitCliente"  maxlength="15" class="solo-numero" type="text" readonly="true" placeholder="" style="width: 70%;" onblur="CalcularDv()" >
                                -
                                <input id="DigitoVerificacion" readonly="true" type="text" placeholder="" style="width: 12%;" >
                            </div>
                            <div class="col-md-2">Ciudad :<span style="color: red; font-size: 17px;">*</span></div>
                            <div class="col-md-4">
                                <input id="Ciudad" type="text" readonly="true" placeholder="" style="width: 86%;" >
                                <input id="idCiudad" type="text" style="display: none">
                            </div>

                        </div>
                        <div class="row">
                            <div class="col-md-2">Nic :</div>
                            <div class="col-md-4"> 
                                <input id="estadonic" value="1" hidden="true" type="text" placeholder="" style="width: 86%;">   
                                <div id="div_Nic" style="display: none">    
                                    <table border="0"  align="center">
                                        <tr>
                                            <td>
                                                <table id="valorespredeterminados" >

                                                    <div id="searchvalorespredeterminados"></div>
                                                </table>
                                                <div id="page_tabla_valorespredeterminados"></div>
                                            </td>
                                        </tr>
                                    </table>   
                                </div>
                                <button id="btn_agregar_nic" class="ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only" onclick="Nics();" >
                                    <span id="estadoxx" class="ui-button-text">+</span>
                                </button> 

                            </div>

                            <div class="col-md-2">Direccion :<span style="color: red; font-size: 17px;">*</span></div>
                            <div class="col-md-4"> 
                                <input id="DireccionCliente" type="text" readonly="true" placeholder="" style="width: 73%;">
                                <img src="/fintra/images/Direcciones.png" width="26" height="23" border="0" align="ABSMIDDLE" onclick="genDireccion('direccion', event);" alt="Direcciones"  title="Direcciones" />
                            </div>
                        </div>



                    </fieldset>

                    <fieldset  class="scheduler-border">
                        <legend  class="scheduler-border">Datos Contacto</legend>
                        <div class="row">
                            <div class="col-md-2">Nombre :<span style="color: red; font-size: 17px;">*</span></div>
                            <div class="col-md-4"><input id="NombreContacto" class="mayuscula" type="text" placeholder="" style="width: 80%;" ></div>
                            <div class="col-md-2">Email :<span style="color: red; font-size: 17px;">*</span></div>
                            <div class="col-md-4">
                                <input id="EmailContacto" type="text" placeholder="Ejemplo@fintra.co" style="width: 80%;" onblur="validarEmail2(1)" >
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-2">Telefono :</div>
                            <div class="col-md-4">
                                <input id="TelefonoContacto" class="solo-numero" type="text" placeholder="" style="width: 80%;" maxlength="7" >
                            </div>
                            <div class="col-md-2">Cargo :<span style="color: red; font-size: 17px;">*</span></div> 
                            <div class="col-md-4">
                                <input id="CargoContacto" maxlength="40" class="mayuscula" type="text" placeholder="" style="width: 80%;" >
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-2">Celular :<span style="color: red; font-size: 17px;">*</span> </div>
                            <div class="col-md-4">
                                <input id="CelularContacto" class="solo-numero" type="text" placeholder="" style="width: 80%;" maxlength="10">
                            </div>

                        </div>

                    </fieldset>

                    <fieldset class="scheduler-border">
                        <legend  class="scheduler-border">Datos Representante Legal</legend>

                        <div class="row">
                            <div class="col-md-2">Nombre :<span style="color: red; font-size: 17px;">*</span></div>
                            <div class="col-md-4"><input id="NombreRepresentateLegal" class="mayuscula" type="text" placeholder="" style="width: 80%;" ></div>
                            <div class="col-md-2">Telefono :</div>
                            <div class="col-md-4">
                                <input id="TelRepresentateLegal" class="solo-numero"  type="text" placeholder="" style="width: 80%;"maxlength="7" >
                            </div>
                        </div>                    
                        <div class="row">
                            <div class="col-md-2">Email :</div>
                            <div class="col-md-4"><input id="EmailRepresentanteLegal" type="text" placeholder="Ejemplo@fintra.co" style="width: 80%;" onblur="validarEmail2(2)" ></div>
                            <div class="col-md-2">Celular :<span style="color: red; font-size: 17px;">*</span> </div>
                            <div class="col-md-4">
                                <input id="CelRepresentateLegal" class="solo-numero"  type="text" placeholder="" style="width: 80%;"maxlength="10" >
                            </div>
                        </div>                    
                    </fieldset>
                    <fieldset class="scheduler-border" style="display: none">
                        <legend  class="scheduler-border">Configuracion Cliente</legend>
                        <div class="row">   

                            <div class="col-md-3"></div>
                            <div class="col-md-2">Categorizacion :</div>
                            <div class="col-md-4">
                                <select class="selectpicker" style="width: 100%;">
                                    <option value="2">!"#$!"#$</option>
                                    <option value="3">!"#$"#$%</option>
                                    <option value="4">!"#$"%&</option>
                                </select>
                            </div>

                        </div>
                    </fieldset>

                    <hr>
                    <div class="row">
                        <div class="col-md-4"></div>

                        <div class="col-md-2 center-block">
                            <button id="btn_show_scoring" class="ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only"onclick="modificarCliente()" >
                                <span class="ui-button-text">Modificar</span>
                            </button>      
                        </div>
                        <div class="col-md-2 center-block">
                            <button id="btn_show_scoring" class="ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only" onclick="desabilitar_form()">
                                <span class="ui-button-text">Cancelar</span>
                            </button>      
                        </div>


                    </div>
                </div>
            </div>
        </div>
    </div>

    <!-- **********************************************************************************************************************************************************
                               Div Emergente Para Nics
    *********************************************************************************************************************************************************** -->

    <div id="div_Nic" style="display: none" >    
        <table border="0"  align="center">
            <tr>
                <td>
                    <table id="valorespredeterminados" >

                        <div id="searchvalorespredeterminados"></div>
                    </table>
                    <div id="page_tabla_valorespredeterminados"></div>
                </td>
            </tr>
        </table>   
    </div>



    <!-- **********************************************************************************************************************************************************
                               Div Emergente Para Direccion
    *********************************************************************************************************************************************************** -->

    <div id="direccion_dialogo" style="display:none;left: 0; position: absolute; top: 0;padding: 1em 1.2em; margin:0px auto; margin-top:10px; padding:10px; width:370px; min-height:140px; border-radius:4px; background-color:#FFFFFF; box-shadow: 0 2px 5px #666666;"> 
        <div>
            <table style="width: 100%;">

                <tr>
                    <td class="titulo_ventana" id="drag_direcciones" colspan="3">
                        <div style="float:center">FORMATO DIRECCIONES<input type="hidden" name="id_usuario" id="id_usuario" value="coco"/></div> 
                    </td>
                </tr>                    
                <tr>  
                    <td><span>Departamento:</span></td>                     
                    <td colspan="2"> <select id="dep_dir" name="dep_dir" style="width:20em;"></select>
                    </td>
                </tr>
                <tr>
                    <td><span>Ciudad:</span></td>     
                    <td colspan="2">                            
                        <div id="d_ciu_dir">
                            <select id="ciu_dir" name="ciu_dir" style="width:20em;"></select>
                        </div>
                    </td>
                </tr>  
                <tr>
                    <td>Via Principal</td>
                    <td>
                        <select id="via_princip_dir" onchange="setDireccion(2)">                                
                        </select>
                    </td>
                    <td><input type="text" id="nom_princip_dir" style="width: 100%;" onchange="setDireccion(1)"/></td>
                </tr>
                <tr>
                    <!-- <td>Via Generadora</td> -->
                    <td>Numero</td>
                    <td>
                        <select id="via_genera_dir" onchange="setDireccion(1)">                               
                        </select>
                    </td>

                    <td>
                        <table width="100%" border="0">
                            <tr>
                                <td align="center" width="49%">
                                    <input type="text" id="nom_genera_dir" style="width: 90%;" onchange="setDireccion(1)"/>
                                </td>
                                <td width="2%" align="center"> - </td>
                                <td align="center" width="49%">
                                    <input type="text" id="placa_dir" style="width: 100%;" onkeypress="setDireccion(1)" onchange="setDireccion(1)"/>
                                </td>    
                            </tr>
                        </table>                        
                    </td>
                </tr>
                <tr>
                    <td>Complemento</td>
                    <td colspan="2"><input type="text" id="cmpl_dir" style="width: 100%;" onkeypress="setDireccion(1)" onchange="setDireccion(1)"/></td>
                </tr>
                <tr>
                    <td colspan="3"><input type="text" id="dir_resul" name="" style="width: 100%;" readonly/></td>
                </tr>
                <tr>
                    <td colspan="3">
                        <button onclick="sendDireccion()">Aceptar</button>
                        <button onclick="setDireccion(0);">Salir</button>
                    </td>
                </tr>
            </table>
        </div>
    </div>







    <script src="./js/OportunidadNegocio/BuscarEditarCliente.js" type="text/javascript"></script>


    <!------------------------------------------------------------------------->
    <div id="dialogLoading" style="display:none;">
        <p  style="font-size: 12px;text-align:justify;" id="msj2">Texto </p> <br/>
        <center>
            <img src="./images/cargandoCM.gif"/>
        </center>
    </div>
    <!-- Dialogo de los jsp> -->
    <div id="dialogMsj" style="display:none;">
        <p style="font-size: 12px;text-align:justify;" id="msj" > Texto </p>
    </div>

</body>
</html>