<%-- 
    Document   : facturacionApuInsumo
    Created on : 4/01/2017, 09:17:24 AM
    Author     : mcastillo
--%>

<%@page contentType="text/html" pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
        <title>Oportunidad De Negocio</title>
         <link href="./css/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css"/>

        <link type="text/css" rel="stylesheet" href="./css/jquery/jquery-ui/jquery-ui.css"/>     
        <script type="text/javascript" src="./js/jquery/jquery-ui/jquery.min.js"></script>
        <script type="text/javascript" src="./js/jquery/jquery-ui/jquery.ui.min.js"></script> 
     
        <link href="./css/OportunidadNegocio.css" rel="stylesheet" type="text/css"/>

        <link type="text/css" rel="stylesheet" href="./css/buttons/botonVerde.css"/>
        <script type="text/javascript" src="./js/jquery/jquery.jqGrid/js/grid.locale-es.js"></script>
        <script type="text/javascript" src="./js/jquery/jquery.jqGrid/js/jquery.jqGrid.min.js"></script>
        <link type="text/css" rel="stylesheet" href="./css/jquery/jquery.jqGrid/ui.jqgrid.css"/> 


        <script src="./js/OportunidadNegocio/facturacionApuInsumo.js" type="text/javascript"></script>
        <%
           String num_solicitud = (request.getParameter("num_solicitud") != null) ? request.getParameter("num_solicitud") : "";
        %>
    </head>
    <body>
        <div id="capaSuperior"  style="position:absolute; width:100%; height:100px; z-index:0; left: 0px; top: 0px;">
            <jsp:include page="/WEB-INF/toptsp.jsp?encabezado=Facturaci�n"/>
        </div>
        <input type="hidden" id="id_solicitud" name="id_solicitud" value="<%=num_solicitud%>" readOnly> 
        <!-- **********************************************************************************************************************************************************
                                                                    Buscar APUS - Insumos pendientes por facturar
        *********************************************************************************************************************************************************** -->
        <div id="div_busqueda" class="ui-jqgrid ui-widget ui-widget-content center-block" style="width: 1317px; height-min: 300px;margin-top: 110px;border-radius: 10px;border: 1px solid #2A88C8;">

            <div class="row" style="padding: 5px">
                <div class="col-md-12 ">

                    <fieldset class="scheduler-border" >
                        <legend class="scheduler-border">Filtro de B�squeda</legend>
                        <table id="tablainterna" style="height: 53px; width: 1271px"  >
                            <tr>
                                <td>
                                    <label>Areas</label>
                                </td>
                                <td>
                                    <select id="area" style="width: 151px;color: #050505;height: 25px;margin-left: 5px;" ></select>
                                </td>
                                <td>
                                    <label>Disciplinas</label>
                                </td>
                                <td>
                                    <select id="disciplina" style="width: 151px;color: #050505;height: 25px;margin-left: 5px;" ></select>
                                </td>
                                <td>
                                    <label>Capitulos</label>
                                </td>
                                <td>
                                    <select id="capitulo" style="width: 151px;color: #050505;height: 25px;margin-left: 5px;" ></select>
                                </td>
                                <td>
                                    <label>Actividades</label>
                                </td>
                                <td>
                                    <select id="actividad" style="width: 151px;color: #050505;height: 25px;margin-left: 5px;" ></select>
                                </td>
                                <td>
                                    <label>Modo</label>
                                </td>
                                <td>
                                    <select id="modo_busqueda" style="width: 151px;color: #050505;height: 25px;margin-left: 5px;" >                                                                     
                                        <option value='0'>APU</option>
                                        <option value='1'>INSUMO</option>

                                    </select>
                                </td>     
                                <td>
                                    <div>
                                        <center>
                                            <button id='buscar_insumo_apu' class="ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only" > 
                                                <span class="ui-button-text">Buscar</span>
                                            </button> 

                                        </center>
                                    </div>
                                </td>
                            </tr>
                        </table>
                    </fieldset>
                </div>
            </div>
        </div>
        <div style="position: relative;top: 35px;">
            <table id="tabla_infoFacturar" ></table>
            <div id="pager"></div>
        </div>
        <div id="dialogLoading" style="display:none;">
            <p  style="font-size: 12px;text-align:justify;" id="msj2">Texto </p> <br/>
            <center>
                <img src="/fintra/images/cargandoCM.gif"/>
            </center>
        </div>
        <div id="dialogMsj" title="Mensaje" style="display:none;">
            <p style="font-size: 12px;text-align:justify;" id="msj" > Texto </p>
        </div>
    </body>
</html>
