
<%@page contentType="text/html" pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>  

    <head>

        <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">



        <title>Oportunidad De Negocio</title>



        <link href="./css/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css"/>

        <link type="text/css" rel="stylesheet" href="./css/jquery/jquery-ui/jquery-ui.css"/>     
        <script type="text/javascript" src="./js/jquery/jquery-ui/jquery.min.js"></script>
        <script type="text/javascript" src="./js/jquery/jquery-ui/jquery.ui.min.js"></script> 
        <script src="./js/jquery/editor/ckeditor/ckeditor.js" type="text/javascript"></script>

        <link href="./css/OportunidadNegocio.css" rel="stylesheet" type="text/css"/>



        <link type="text/css" rel="stylesheet" href="./css/buttons/botonVerde.css"/>
        <script type="text/javascript" src="./js/jquery/jquery.jqGrid/js/grid.locale-es.js"></script>
        <script type="text/javascript" src="./js/jquery/jquery.jqGrid/js/jquery.jqGrid.min.js"></script>
        <link type="text/css" rel="stylesheet" href="./css/jquery/jquery.jqGrid/ui.jqgrid.css"/>    



        <script src="./js/OportunidadNegocio/ConfiguradorCargaLaboral.js" type="text/javascript"></script>

        <!-------------------------------------Notificaciones----------------------------------------->
        <link type="text/css" rel="stylesheet" href="./css/toastr.min.css"/>
        <script type="text/javascript" src="./js/toastr.min.js"></script>
        <!-------------------------------------/Notificaciones----------------------------------------->

        <!----------------------------------------Pre-Load--------------------------------------------->
        <!--se necesita un div para que funcione el preload se llama loader-wrapper-->

        <link type="text/css" rel="stylesheet" href="./css/main2.css">

        <!----------------------------------------Pre-Load--------------------------------------------->


    </head> 
    <body>

    <center>
        <jsp:include page="/WEB-INF/toptsp.jsp?encabezado=Configurador De Carga Laboral"/>

        <div style="margin-top: 25px">
            <table id="tbl_CargaLaboral" ></table>
            <div id="page_tbl_CargaLaboral"></div>  
        </div>

        <!-- **********************************************************************************************************************************************************
                                                                    
        *********************************************************************************************************************************************************** -->

        <div id="div_Control"  style="width: 530px;display: none; " >  
            <table aling="center" style=" width: 100%" >                   
                <tr>
                    <td>
                        <input type="text" id="idCargaLaboral"  style="width: 50px"hidden >
                    </td>
                </tr>
                <tr>
                    <td colspan="2">
                        <label> Nombre<span style="color:red;">*</span></label> 
                    </td>
                    <td colspan="6">
                        <input type="text" id="nombre" class="mayuscula" style="width: 95%" maxlength="30"  />
                    </td>
                    <td colspan="2">
                        <label> Nivel<span style="color:red;">*</span></label> 
                    </td>
                    <td colspan="2">
                        <select id="cbx_nivel" style="width: 100px;" >
                            <option value='Bajo'>Bajo</option>                                        
                            <option value='Medio'>Medio</option>
                            <option value='Alta'>Alta</option>
                            <option value='Muy Alta'>Muy Alta</option>
                        </select>
                    </td>
                    
                </tr>  
                <tr>
                    <td colspan="2">
                        <label> Descripcion<span style="color:red;">*</span></label> 
                    </td>
                    <td colspan="10">
                        <textarea id ="descripcion" name="descripcion" rows="5" maxlength="300" style="resize:none;width: 96%"></textarea>                      
                    </td>
                </tr>  
                
                <tr>
                    <td colspan="2">
                        <label> Desde<span style="color:red;">*</span></label> 
                    </td>
                    <td colspan="2">
                        <input type="text" id="txt_desde" class="solo-numero" onchange="mayor0(this)" style="width: 100px" maxlength="30"  />
                    </td>
                    <td colspan="2">
                        <label> Hasta<span style="color:red;">*</span></label> 
                    </td>
                    <td colspan="2">
                        <input type="text" id="txt_hasta" class="solo-numero numberConComas" style="width: 100px" maxlength="30"  />
                    </td>
                    <td colspan="2">
                        <label> Unidad Medida<span style="color:red;">*</span></label> 
                    </td>
                    <td colspan="2">
                        <select id="cbx_Unidad_medida" style="width: 100px;" >
                            <option value=''>...</option>
                        </select>
                    </td>
 
                </tr>
            </table>
        </div>




        <!-- **********************************************************************************************************************************************************
                                                                   Ventana de dialogos
       *********************************************************************************************************************************************************** -->
        <div>
            <div id="dialogLoading" style="display:none;">
                <p  style="font-size: 12px;text-align:justify;" id="msj2">Texto </p> <br/>
                <center>
                    <img src="./images/cargandoCM.gif"/>
                </center>
            </div>
            <!-- Dialogo de los jsp> -->
            <div id="dialogMsj" style="display:none;">
                <p style="font-size: 12px;text-align:justify;" id="msj" > Texto </p>
            </div>
            <div id="dialogMsgMeta" title="Mensaje" style="display:none; visibility: hidden">
                <p style="font-size: 12.5px;text-align:justify;" id="msj3" > Texto </p>
            </div>  
        </div>




        <script type="text/javascript" src="./js/jquery/jquery.jqGrid-4.7.0/js/grid.locale-es.js"></script>
        <script type="text/javascript" src="./js/jquery/jquery.jqGrid-4.7.0/js/jquery.jqGrid.min.js"></script>
        <script type="text/javascript" src="./js/jquery/jquery.jqGrid-4.7.0/js/jquery.tabletojson.js"></script> 


        <!------------------------------------------------------------------------->


    </center>
    <div id="loader-wrapper">
        <div id="loader"></div>
        <img id="logowast" src="/fintra/images/Selectrik.png" style="z-index: 10002 !important">
        <div class="loader-section section-left">	
        </div>
        <div class="loader-section section-right"></div>
    </div>
</body>
</html>
