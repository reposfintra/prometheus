<%-- 
    Document   : procesoCategoria
    Created on : 24/02/2016, 11:23:05 AM
    Author     : user
--%>
<%@page contentType="text/html" pageEncoding="utf-8"%>
<%@page import="com.tsp.operation.model.beans.Compania"%>
<%@page import="com.tsp.operation.model.CompaniaService"%>
<%@page import="com.tsp.operation.model.beans.Usuario"%>
<%@page import="com.tsp.operation.model.beans.CmbGeneralScBeans"%>
<%@page import="java.util.ArrayList"%>

<%@include file="/WEB-INF/InitModel.jsp"%>
<!DOCTYPE html>
<%    Usuario usuario = (Usuario) session.getAttribute("Usuario");
    String nomEmpresa = "";
    CompaniaService ciaserv = new CompaniaService();
    ciaserv.buscarCia(usuario.getDstrct());
    Compania cia = ciaserv.getCompania();
    nomEmpresa = cia.getdescription();
%>   

<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
        <title>Categoria - Impacto</title>
        <script type="text/javascript" src="./js/jquery/jquery-ui/jquery.min.js"></script>
        <script type="text/javascript" src="./js/jquery/jquery-ui/jquery.ui.min.js"></script>           
        <link type="text/css" rel="stylesheet" href="./css/jquery/jquery-ui/jquery-ui.css"/>
        <link type="text/css" rel="stylesheet" href="./css/jquery/jquery.jqGrid/ui.jqgrid.css"/>        
        <script type="text/javascript" src="./js/jquery/jquery.jqGrid/js/grid.locale-es.js"></script>
        <script type="text/javascript" src="./js/jquery/jquery.jqGrid/js/jquery.jqGrid.min.js"></script>
        <link type="text/css" rel="stylesheet" href="./css/buttons/botonVerde.css"/>
        <link type="text/css" rel="stylesheet" href="./css/popup.css"/>
        <link href="./css/style_azul.css" rel="stylesheet" type="text/css">
        <script type="text/javascript" src="./js/OportunidadNegocio/Categoria_Riesgo.js"></script> 
        <link type="text/css" rel="stylesheet" href="./css/main2.css">

    </head>
    <body>

        <div id="capaSuperior" style="position:absolute; width:100%; height:100px; z-index:0; left: 0px; top: 0px;">
            <jsp:include page="/toptsp.jsp?encabezado=Administracion de Catalogos"/>
        </div>
        <div id="capaCentral" style="position:absolute; width:100%; height:83%; z-index:0; left: 0px; top: 100px; overflow:auto;">       


            <!-------------------------------------->


            <center>
                <br>
                <div class="ui-jqgrid ui-widget ui-widget-content ui-corner-all" style="max-width: 407px; height: 110PX;">

                    <div class="ui-dialog-titlebar ui-widget-header ui-corner-all ui-helper-clearfix">
                        <span class="ui-dialog-title" id="ui-dialog-title-listaGlobal">
                            &nbsp;
                            <label class="titulotablita"><b>FILTRO DE BUSQUEDA</b></label>
                        </span>
                    </div>
                    <table style=" padding: 15px 1em 0.5em 1em"  style="width: 95%;margin-top: 20px">
                        <tr>
                            <td>Tipo Categoria</td>
                            <td>

                                <select id="categoria" name="categoria" style="width: 264px;">
                                    <!--option value="1">MATERIAL</option>
                                    <option value="2">EQUIPOS</option>
                                    <option value="3">MANO DE OBRA</option>
                                    <option value="4">HERRAMIENTAS</option-->
                                </select>
                            </td>
                            <td>
                                <img id = "add_grupo" src = "/fintra/images/botones/iconos/adds.png"
                                     style = "margin-left: 5px; height: 19px; vertical-align: middle;" 
                                     onclick = "crearTipoCategoria()">
                            </td>
                        </tr>
                    </table>
                    <hr>
                    <div id ='botones'>
                        <button id="C_categoria" class="ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only" 
                                role="button" aria-disabled="false" onclick="">
                            <span class="ui-button-text">Buscar</span>
                            <input type="text" id="idcategoria" name="idcategoria" style="display: none"> 


                        </button> 
                    </div>
                </div>
                <br>
                <div>
                    <table id="tabla_productos"></table>
                    <div id="page_productos"></div>
                </div>

                <!------------Div para crear Tipo de Categorias------------->
                <div id="div_tipo_categoria"  style="display: none; width: 800px" >                       
                    <div id="filtros" class="ui-jqgrid ui-widget ui-widget-content ui-corner-all" style="height: 90px;">
                        </br>
                        <table aling="center" style=" width: 100%" >
                            <tr>
                                <td style="width: 20%"><span>Tipo Categoria<span style="color:red;">*</span></span></td>                          
                                <td colspan="2" style="width: 80%"><input type="text" id="nomcategoria" name="nomcategoria" class="mayuscula" style=" width: 94%">
                                </td>  
                            </tr> 
                            <tr>
                                <td style="width: 20%"><span>Puntaje<span style="color:red;">*</span></span></td>                          
                                <td style="width: 50%">
                                    <input type="text" id="puntaje" name="puntaje" class="solo-numeric" style=" width: 90px">                                    
                                </td>  

                            </tr> 
                        </table>
                    </div>  
                    </br> 
                </div>

            </center>


            <!-------------------------------------->


            </br>
            <table border="0"  align="center">
                <tr>
                    <td>

                        <div id="searchImpacto"></div>
                        <table id="Impacto" ></table>
                        <div id="page_tabla_impacto"></div>
                    </td>
                </tr>
            </table>
        </div>


        <div id="div_categoria"  style="display: none; width: 800px" >                       
            <div id="filtros" class="ui-jqgrid ui-widget ui-widget-content ui-corner-all" style="height: 150px;">
                </br>
                <table aling="center" style=" width: 100%" >
                    <tr>
                        <input type="text" id="idImpacto" name="idImpacto" hidden="true">
                    <input type="hidden" id="idProceso" name="idMeta">     
                    <input type="hidden" id="idempresa" name="idempresa" value="<%=usuario.getDstrct()%>" >      
                    <td style="width: 10%"><span>Nombre</span><span style="color: red; ">*</span></td>                          
                    <td style="width: 50%"><input type="text" id="nommeta" class="mayuscula" name="nommeta" style=" width: 248px" onblur="igualarcampo('descmeta', 'nommeta');">
                        <!--img src='<%=BASEURL%>/images/botones/iconos/lupa.gif' id="buscarnumos" width="15" height="15"  style="cursor:hand"title="Buscar">
                        <input type="text" id="idcategoria" name="idcategoria"/-->
                    </td>  
                    </tr> 
                    <tr>
                        <td style="width: 10%"><span>Descripcion:</span><span style="color: red; ">*</span></td>   
                        <td style="width: 90%" colspan="4"><textarea id ="descmeta" name="descmeta" style="width:535px;resize:none" rows="2" ></textarea></td>                        
                    </tr>

                    <tr>
                        <td style="width: 10%"><span>Puntaje:</span><span style="color: red; ">*</span></td>   
                        <td style="width: 90%" colspan="4">
                            <input type="text" id="puntaje11" name="puntaje11" class="solo-numeric" style=" width: 90px">
                        </td>                        
                    </tr>

                </table>
            </div>  
            </br> 
        </div>

        <div id="div_editar_categoria"  style="display: none; width: 800px" >
            <div id="filtros" class="ui-jqgrid ui-widget ui-widget-content ui-corner-all" style="height: 145px;padding: 0px 10px 5px 10px">
                </br>
                <table aling="center" style=" width: 100%" >
                    <tr>
                    <input type="hidden" id="idMetaEdit" name="idMetaEdit">    
                    <input type="hidden" id="idempresaEdit" name="idempresaEdit" value="<%=usuario.getDstrct()%>" >      
                    <td style="width: 10%"><span>Nombre</span><span style="color: red; ">*</span></td>                          
                    <td style="width: 50%"><input type="text" id="nommetaEdit" class="mayuscula" name="nommetaEdit" style=" width: 250px" ></td>
                    </tr>
                    <tr>
                        <td style="width: 10%"><span>Descripcion:</span><span style="color: red; ">*</span></td>   
                        <td style="width: 90%" colspan="4"><textarea id ="descmetaEdit" name="descmetaEdit" style="width:525px;resize:none"  rows="2" ></textarea></td>                        
                    </tr>
                    <tr>
                        <td style="width: 100%"><div id="boton_guardar" style="position:absolute; left: 83%; top: 80%; "> 
                                <span aling="center"  class="form-submit-button form-submit-button-simple_green_apple" onClick="actualizarMetaProceso();"> Actualizar </span>
                            </div></td>
                    </tr>
                </table>
            </div>  
            </br>  
            <table border="0"  align="center">
                <tr>
                    <td>
                        <table id="subcategoria" ></table>
                        <div id="page_tabla_subcategorias"></div>
                    </td>
                </tr>
            </table>
        </div> 

        <div id="dialogMsgMeta" title="Mensaje" style="display:none; visibility: hidden">
            <p style="font-size: 12.5px;text-align:justify;" id="msj" > Texto </p>
        </div>  
        <!--SOLO PRUEBA-->            
        <div id="dialogMsjmultiservicio" class="ui-widget" style="display:none;top: 14px;" >
            <table id="tablainterna"  >
                <tr>
                    <td>
                        <label style="font-family: Tahoma,Arial; padding-left: 5px;font-size: 14px; ">Nombre Categoria</label>
                        <input type="text" id="multiser"style="width: 119px;font-family: Tahoma,Arial;color: black;font-size: 13px;" > 
                    </td>
                </tr>
            </table>

            <table id="tabla_multiservicio" >

            </table>
        </div>

        <!------------------------------->
        <div id="loader-wrapper">
            <div id="loader"></div>
            <img id="logowast" src="/fintra/images/Selectrik.png" style="z-index: 10002 !important">
            <div class="loader-section section-left">	
            </div>
            <div class="loader-section section-right"></div>
        </div>


    </body>
</html>