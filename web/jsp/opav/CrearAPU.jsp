<%-- 
    Document   : CrearAPU
    Created on : 28/04/2016, 10:53:08 AM
    Author     : user
--%>

<%@page import="com.tsp.operation.model.beans.Compania"%>
<%@page import="com.tsp.operation.model.CompaniaService"%>
<%@page import="com.tsp.operation.model.beans.Usuario"%>
<%@page import="com.tsp.operation.model.beans.CmbGeneralScBeans"%>
<%@page import="java.util.ArrayList"%>
<%@page import="com.tsp.opav.model.DAOS.impl.ProcesosAPUImpl"%>
<%@page import="com.tsp.opav.model.DAOS.ProcesosAPUDAO"%>
<%@page contentType="text/html" pageEncoding="utf-8"%>
<%@include file="/WEB-INF/InitModel.jsp"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
        <title>Analisis de Precio Unitario</title>
        <link href="./css/popup.css" rel="stylesheet" type="text/css">
        <link type="text/css" rel="stylesheet" href="./css/jquery/jquery-ui/jquery-ui.css" />
        <link type="text/css" rel="stylesheet" href="./css/APU.css " />
        <script type="text/javascript" src="./js/jquery/jquery.jqGrid/js/jquery.min.js"></script>
        <script type="text/javascript" src="./js/jquery-ui-1.8.5.custom.min.js"></script>   
        <script type="text/javascript" src="./js/jquery/jquery.jqGrid-4.7.0/js/grid.locale-es.js"></script>
        <script type="text/javascript" src="./js/jquery/jquery.jqGrid-4.7.0/js/jquery.jqGrid.min.js"></script>
        <script type="text/javascript" src="./js/jquery/jquery.jqGrid-4.7.0/js/jquery.tabletojson.js"></script>    
        <script type="text/javascript" src="./js/CrearAPU.js"></script>   
    </head>
    <body>
        <div id="capaSuperior" style="position:absolute; width:100%; height:100px; z-index:0; left: 0px; top: 0px;">
            <jsp:include page="/WEB-INF/toptsp.jsp?encabezado=Analisis de Precio Unitario"/>
        </div>

    <center>
        
        <div id="capaCentral" class='capaCentral' style="position:absolute; width:100%; height:85%; z-index:0; left: 0px; top: 100px; overflow: auto;" align="center">
            <center>
                <br>
                <div class="ui-jqgrid ui-widget ui-widget-content ui-corner-all" style="max-width: 454px; height: 55px;">

                    <div class="ui-dialog-titlebar ui-widget-header ui-corner-all ui-helper-clearfix">
                        <span class="ui-dialog-title" id="ui-dialog-title-listaGlobal">
                            &nbsp;
                            <label class="titulotablita"><b>Grupo APU</b></label>
                        </span>
                    </div>
                    <table style=" padding: 0.5em 2em 0.1em 2em" style="width: 95%;">

                        <tr align='center'>
                            <td>Grupo APU:</td>
                            <td>
                                <select id="grupo_apu1" nombre="grupo_apu1" style="width: 264px;" >
                                    <!--option value=''>...</option-->
                                </select>
                            </td>

                        </tr>

                    </table>
                </div>
            </center>
        </div>

        <div style="margin-top: 180px">
            <table id="tbl_grupos_apu" ></table>                
            <div id="page_grupos_apu"></div>
        </div>

        <!-------MATERIALES------->
        <div style="margin-top: 15px">
            <table id="tbl_materiales_apu" ></table>                
            <div id="page_materiales_apu"></div>
        </div>

        <!-------EQUIPO------->
        <div style="margin-top: 15px">
            <table id="tbl_equipo_apu" ></table>                
            <div id="page_equipo_apu"></div>
        </div>

        <!-------MANO DE OBRA------->
        <div style="margin-top: 15px">
            <table id="tbl_maobra_apu" ></table>                
            <div id="page_maobra_apu"></div>
        </div>

        <!-------HERRAMIENTAS------->
        <div style="margin-top: 15px">
            <table id="tbl_herramientas_apu" ></table>                
            <div id="page_herramientas_apu"></div>
        </div>
        
        <!-------TRANSPORTES------->
        <div style="margin-top: 15px">
            <table id="tbl_transportes_apu" ></table>                
            <div id="page_transportes_apu"></div>
        </div>
        
        <!-------PERMISOSTRAMITES------->
        <div style="margin-top: 15px">
            <table id="tbl_pertra_apu" ></table>                
            <div id="page_pertra_apu"></div>
        </div>

        <!-------------------crear APU-------------------------------->
        <div id="div_apu"  style="display: none; width: 800px" >   
            <center>
                <div id="filtros" class="ui-jqgrid ui-widget ui-widget-content ui-corner-all" style="height: 93px; width: 445px">
                    </br>
                    <table aling="center" style=" width: 100%" >
                        <tr>
                            <td>Grupo APU:</td>
                            <td>
                                <select id="grupo_apu" nombre="grupo_apu" style="width: 264px;" >
                                    <!--option value=''>...</option-->
                                </select>
                            </td>
                            <td><img id = "add_grupo" src = "/fintra/images/botones/iconos/adds.png"
                                     style = "margin-left: 5px; height: 19px; vertical-align: middle;" 
                                     onclick = "crearGrupoApu();"></td>
                        </tr> 
                        <tr>
                            <td>Unidad Medida:</td>
                            <td>
                                <select id="unmed" nombre="unmed" style="width: 264px;" >
                                    <!--option value=''>...</option-->
                                </select>
                            </td>
                            <td><img id = "add_unidadmed" src = "/fintra/images/botones/iconos/adds.png"
                                     style = "margin-left: 5px; height: 19px; vertical-align: middle;" 
                                     onclick = "crearUnidadMedida();"></td>
                        </tr>
                        <tr>
                            <td>Nombre APU: <span style="color: red; ">*</span></td>
                            <td style="width: 50%">
                                <input type="text" id="nomapu" class="mayuscula" name="nomapu" style=" width: 248px">
                                <input type="hidden" id="idapu"  name="idapu">
                            </td>
                        </tr>
                    </table>
                </div>  
                </br> 

                <!---------Grid que contiene los insumos ----------->
                <div style="margin-top: 0px">
                    <table id="tbl_insumos" ></table>                
                    <div id="page_insumos"></div>
                </div>
                <!-------------------->

                <div id="div_filtro_insumos"  style="display: none; width: 800px" >
                    <div id="filtros" class="ui-jqgrid ui-widget ui-widget-content ui-corner-all" style="height: 95px;padding: 0px 10px 5px 10px">
                        <center>

                            <table style=" padding: 0.5em 1em 0.5em 1em" style="width: 95%;">

                                <input type="hidden" id="idregistro" name="idregistro">

                                <tr>
                                    <td>Categoria</td>
                                    <td>
                                        <select id="categoria" nombre="categoria" onchange="cargarCombo('sub', [this.value])" style="width: 264px;" >
                                            <option value=''>...</option>
                                        </select>
                                    </td>
                                </tr>
                                <tr>
                                    <td>SubCategoria</td>
                                    <td>
                                        <select id="sub"  style="width: 264px;" >
                                            <option value=''>...</option>
                                        </select>

                                        <input type="hidden" name="nomsub" id="nomsub">
                                    </td>
                                </tr>
                            </table>
                            <hr>
                            <div id ='botones'>
                                <button id="buscar" class="ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only" 
                                        role="button" aria-disabled="false">
                                    <span class="ui-button-text">Buscar</span>
                                </button> 
                            </div>
                        </center>
                    </div>  
                    </br>  

                    <!---------Grid que contiene filtro de insumos ----------->
                    <div style="margin-top: 0px">
                        <center>
                            <table id="tbl_filtro_insumos" ></table>                
                            <div id="page_filtro_insumos"></div>
                        </center>
                    </div>
                    <!-------------------->

                    <!--------div materiales------------>
                    <div style="margin-top: 0px">
                        <center>
                            <table id="tbl_materiales" ></table>                
                            <div id="page_materiales"></div>
                        </center>
                    </div>
                    
                </div>

            </center>
        </div>

        <!-------------------crear grupo APU-------------------------------->
        <div id="div_grupo_apu"  style="display: none; width: 800px" >                       
            <div id="filtros" class="ui-jqgrid ui-widget ui-widget-content ui-corner-all" style="height: 110px;">
                </br>
                <table aling="center" style=" width: 100%" >
                    <tr>
                        <td style="width: 10%"><span>Nombre Grupo</span><span style="color: red; ">*</span></td>                          
                        <td style="width: 50%"><input type="text" id="nomgrupo" class="mayuscula" name="nomgrupo" style=" width: 248px">
                        </td>  
                    </tr> 
                    <tr>
                        <td style="width: 10%"><span>Descripcion Grupo:</span><span style="color: red; ">*</span></td>   
                        <td style="width: 90%" colspan="4"><textarea id ="descgrupo" name="descgrupo" style="width:535px;resize:none" rows="2" ></textarea></td>                        
                    </tr>
                </table>
            </div>  
            </br> 
        </div>
        
        <!-------------------crear Unidad Medida-------------------------------->
        <div id="div_unidad_medida"  style="display: none; width: 800px" >                       
            <div id="filtros" class="ui-jqgrid ui-widget ui-widget-content ui-corner-all" style="height: 53px;">
                </br>
                <table aling="center" style=" width: 100%" >
                    <tr>
                        <td style="width: 20%"><span>Unidad de Medida</span></td>                          
                        <td style="width: 50%"><input type="text" id="nomunidad" class="mayuscula" name="nomunidad" style=" width: 280px">
                        </td>  
                    </tr> 
                </table>
            </div>  
            </br> 
        </div>

    </center>
    <div id="dialogMsgMeta" title="Mensaje" style="display:none; visibility: hidden">
        <p style="font-size: 12.5px;text-align:justify;" id="msj" > Texto </p>
    </div> 
    <div id="dialogMsj" title="Mensaje" style="display:none;">
        <p style="font-size: 12.5px;text-align:justify;" id="msj2" > Texto </p>
    </div> 
</body>
</html>
