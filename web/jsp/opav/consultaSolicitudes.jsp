<%--
    Document   : consultaSolicitudes
    Created on : 02/06/2010, 02:00:41 PM
    Author     : darrieta - GEOTECH
--%>

<%@page import="com.tsp.operation.model.beans.Usuario"%>
<%@page contentType="text/html" pageEncoding="ISO-8859-1"%>
<%@page session ="true"%>
<%@page errorPage ="/error/ErrorPage.jsp"%>
<%@page import ="java.util.*" %>
<%@page import ="com.tsp.opav.model.*"%>
<%@page import ="com.tsp.opav.model.beans.*"%>
<%@page import ="com.tsp.opav.model.services.*"%>
<%@include file ="/WEB-INF/InitModel.jsp"%>
<%@ taglib uri="http://jsptags.com/tags/navigation/pager" prefix="pg" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">

<%
            String estadoSolicitud = request.getParameter("estadoSolicitud")!=null?request.getParameter("estadoSolicitud"):"";
            ArrayList listaEstados = modelopav.NegociosApplusService.getEstadosApplus2();
            ArrayList contratistas = modelopav.NegociosApplusService.getContratistas();
            ArrayList consulta = modelopav.NegociosApplusService.getLista();

            Usuario usuario = (Usuario) session.getAttribute("Usuario");
            ClientesVerService clvsrv = new ClientesVerService(usuario.getBd());
            String perfil = clvsrv.getPerfil(usuario.getLogin());
            String interventor = request.getParameter("interventor")!=null?request.getParameter("interventor"):"";
%>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
        <title>Consulta de solicitudes</title>
        <link href="<%= BASEURL%>/css/estilostsp.css" rel='stylesheet'>
        <link href="<%= BASEURL%>/css/autocompleter.css" rel='stylesheet'>
        <script type='text/javascript' src="<%= BASEURL%>/js/boton.js"></script>
        <script type='text/javascript' src="<%= BASEURL%>/js/prototype.js"></script>
        <script type='text/javascript' src="<%= BASEURL%>/js/scriptaculous/scriptaculous.js"></script>
        <script type='text/javascript' src="<%= BASEURL%>/js/interventor.js"></script>	
        <style>
            .grande{
                width: 350px;
            }

            #tabResultados td{
                text-align: center;
            }
        </style>
        <script type='text/javascript'>

            function abrirSeleccion(i,idSolicitud,idAccion){
                var url = "<%=BASEURL%>";
                switch($F("select_opt"+i)){
                    case "1":
                        url += "/jsp/opav/programacionActividades.jsp?id_solicitud="+idSolicitud+"&id_accion="+idAccion;
                        break;
                    case "2":
                        url += "/jsp/opav/ajustesProgramacion.jsp?id_solicitud="+idSolicitud;
                        break;
                    case "3":
                        url += "/jsp/opav/seguimientoEjecucion.jsp?id_solicitud="+idSolicitud;
                        break;
                    case "4":
                        url += "/jsp/opav/consultaEjecucion.jsp?id_solicitud="+idSolicitud;
                        break;
                    default:
                        url = "#";
                        return false;
                }
                window.open(url);
            }

        </script>
    </head>
    <body>
        <div id="capaSuperior" style="position:absolute; width:100%; height:100px; z-index:0; left: 0px; top: 0px;">
            <jsp:include page="/WEB-INF/toptsp.jsp?encabezado=Consulta de solicitudes"/>
        </div>
        <div id="capaCentral" style="position:absolute; width:100%; height:100%; z-index:0; left: 0px; top: 100px;">
            <form method='post' name='form1' action="<%=CONTROLLEROPAV%>?estado=Negocios&accion=Applus&opcion=consultarSolicitudesEstado">
                <table width='80%' align="center" style="border: green solid 1px;"> <!--%=/*estadoSolicitud.equals("")?"":"onmouseout='form1.cmbEstado.disabled=false;'"%-->
                    <tr>
                        <th colspan='4' class="barratitulo">
                            <table width="100%">
                                <tr>
                                    <td width="40%" class="subtitulo1">Consulta de solicitudes</td>
                                    <td width="60%"><img src="<%=BASEURL%>/images/titulo.gif" width="32" height="20" align="left"></td>
                                </tr>
                            </table>
                        </th>
                    </tr>
                    <tr class='fila'>
                        <td>Estado actual</td>
                        <td> <!--%=estadoSolicitud.equals("")?"":"onmouseout='form1.cmbEstado.disabled=false;' onmouseover='form1.cmbEstado.disabled=true;'"%-->
                            <select name="cmbEstado" id="cmbEstado" class="grande" ><!--onmousemove="this.disabled=true;"-->
                                <option value="0">&nbsp;</option>
                                <%for (int i = 0; i < listaEstados.size(); i++) {
                                      String[] estado = (String[]) listaEstados.get(i);
                                %>
                                <option value="<%=estado[0]%>" <%=estado[0].equals(estadoSolicitud)?"selected":""%>><%=estado[1]%></option>
                                <%}%>
                            </select>
                        </td>
                        <td>Id solicitud</td>
                        <td><input type="text" name="txtSolicitud" id="txtSolicitud" value="<%=request.getParameter("solicitud")==null?"":request.getParameter("solicitud")%>"></td>
                    </tr>
                    <tr class='fila'>
                        <td>Nombre cliente</td>
                        <td>
                            <input type="text" name="txtCliente" id="txtCliente" class="grande" value="<%=request.getParameter("cliente")==null?"":request.getParameter("cliente")%>">
                            <div id="divCliente" class="autocomplete"></div>
                            <script type="text/javascript" language="javascript" charset="utf-8">
                                // <![CDATA[
                                new Ajax.Autocompleter('txtCliente','divCliente',"<%=CONTROLLEROPAV%>?estado=Negocios&accion=Applus&opcion=buscarCliente",{paramName:'txtCliente'});
                                // ]]>
                            </script>
                        </td>
                        <td>Consecutivo</td>
                        <td><input type="text" name="txtConsecutivo" id="txtConsecutivo" value="<%=request.getParameter("consecutivo")==null?"":request.getParameter("consecutivo")%>"></td>
                    </tr>
                    <tr class='fila'>
                        <td>Contratista</td>
                        <td>
                            <select name="cmbContratista" id="cmbContratista" class="grande">
                                <option value="" selected="selected"> </option>
                                <%for (int i = 0; i < contratistas.size(); i++) {
                                                String[] contratista = (String[]) contratistas.get(i);
                                %>
                                <option value="<%=contratista[0]%>" <%=contratista[0].equals(request.getParameter("contratista"))?"selected":""%> ><%=contratista[1]%></option>
                                <%}%>
                            </select>
                        </td>
                        <td>Nic</td>
                        <td><input type="text" name="txtNic" id="txtNic" value="<%=request.getParameter("nic")==null?"":request.getParameter("nic")%>"></td>
                    </tr>
                    <tr class='fila'>
                        <td>Interventor</td>
                        <td><select name="interventor" id="interventor" >
                          <option value="0">Seleccione un Interventor</option>
                        </select></td>
                        <td>Multiservicio</td>
                        <td><input type="text" name="txtMultiservicio" id="txtMultiservicio"></td>
                    </tr>
                </table>
                <br/>
                <div align="center">
                    <img style="cursor:hand" src="<%= BASEURL%>/images/botones/buscar.gif" align="absmiddle" onmouseover="botonOver(this);" onmouseout="botonOut(this);" onClick="form1.submit()">
                    <img style="cursor:hand" src="<%= BASEURL%>/images/botones/salir.gif" align="absmiddle" onmouseover="botonOver(this);" onmouseout="botonOut(this);" onClick="parent.close();">
                </div>
            </form>
            <br/>
            <%if(request.getParameter("resultado")!=null){
                if(consulta!=null && consulta.size()>0){
                    int maxPageItems = 50;
            %>
            <table id="tabResultados" style="border: green solid 1px;" align="center">
                        <tr class="tblTitulo">
                            <th>Id solicitud</th>
                            <th>Consecutivo</th>
                            <th>No Orden de trabajo</th>
                            <th>Accion</th>
                            <th>Entrega Oferta</th>
                            <th>Id cliente</th>
                            <th>Nic</th>
                            <th>Nit</th>
                            <th>Nombre cliente</th>
                            <th>Tipo cliente</th>
                            <th>Nombre contratista</th>
                        </tr>
                        <pg:pager
                            items="<%=consulta.size()%>"
                            index="center"
                            maxPageItems="<%=maxPageItems%>"
                            maxIndexPages="10"
                            isOffset="<%= true %>"
                            export="offset,currentPageNumber=pageNumber"
                            scope="request">
                            <pg:param name="resultado"/>
                            <pg:param name="estadoSolicitud"/>
                            <pg:param name="solicitud"/>
                            <pg:param name="cliente"/>
                            <pg:param name="contratista"/>
                            <pg:param name="consecutivo"/>
                            <pg:param name="nic"/>
                            <pg:param name="multiservicio"/>
                    <%for (int i = offset.intValue(), l = Math.min(i + maxPageItems, consulta.size()); i < l; i++) {
                        NegocioApplus n = (NegocioApplus)consulta.get(i);
                    %>
                            <pg:item>
                                <tr class='<%= (i%2==0?"filagris":"filaazul") %>'>
                                    <td nowrap>
                                        <%=n.getIdSolicitud()%>
                                        <select id="select_opt<%=i%>" onchange="abrirSeleccion(<%=i%>,'<%=n.getIdSolicitud()%>','<%=n.getIdAccion()%>');" style="font-size:11px;">
                                            <option>...</option>
                                            <option value="1">Programaci&oacute;n</option>
                                            <%if (clvsrv.ispermitted(perfil, "52")){%>
                                            <option value="2">Ajustes programaci&oacute;n</option>
                                            <%}%>
                                            <%if (clvsrv.ispermitted(perfil, "52")){%>
                                            <option value="3">Seguimiento ejecuci&oacute;n</option>
                                            <%}%>
                                            <option value="4">Consulta ejecuci&oacute;n</option>
                                        </select>
                                    </td>
                                    <td><%=n.getConsecutivo_oferta()%></td>
                                    <td><%=n.getNumOs()%></td>
                                    <td><%=n.getIdAccion()%></td>
                                    <td title="<%=n.getUsuarioEntregaOferta()+"_"+n.getCreacionFechaEntregaOferta()%>"> <%=n.getFechaEntregaOferta()%></td>
                                    <td><%=n.getIdCliente()%></td>
                                    <td><%=n.getNicClient()%></td>
                                    <td><%=n.getNitClient()%></td>
                                    <td><%=n.getNombreCliente()%></td>
                                    <td><%=n.getTipoCliente()%></td>
                                    <td><%=n.getNombreContratista()%></td>
                                </tr>
                            </pg:item>
                    <%}%>
                            <tr bgcolor="#FFFFFF" class="fila">
                                <td height="30" colspan="18" nowrap>
                                    <pg:index>
                                        <jsp:include page="/WEB-INF/jsp/googlesinimg.jsp" flush="true"/>
                                    </pg:index>
                                </td>
                            </tr>
                        </pg:pager>
                </table>
                <%}else{%>
                    <table border="2" align="center">
                        <tr>
                            <td>
                                <table width="100%"  border="0" align="center"  bordercolor="#f7f5f4" bgcolor="#ffffff">
                                    <tr>
                                        <td width="229" align="center" class="mensajes">No se encontraron resultados</td>
                                        <td width="29" background="<%=BASEURL%>/images/cuadronaranja.JPG">&nbsp;&nbsp;</td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                    </table>
                <%}%>
            <%}%>
        </div>
        <tsp:InitCalendar/>
    </body>
</html>
<script>
     buscarInterventores('<%=CONTROLLEROPAV%>', '<%=interventor%>');
</script>
