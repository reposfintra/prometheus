<%@page import="com.tsp.operation.model.beans.TablaGen"%>
<%@page import="com.tsp.operation.model.beans.Usuario"%>
<!--
- Autor : Ing. Pablo Emilio Bassil Orozco
- Date  : 15 de Octubre de 2009
- Copyrigth Notice : Fintravalores S.A. S.A

Descripcion : Creacion de PDF de prueba
-->
<%-- Declaracion de librerias--%>
<%@page contentType="text/html;"%>
<%@page session="true"%>
<%@page import="java.util.*"%>
<%@page import="com.tsp.opav.model.*"%>
<%@page import="com.tsp.opav.model.beans.*"%>
<%@page import="com.tsp.opav.model.services.*"%>
<%@page import="com.tsp.util.*"%>
<%@page import="com.tsp.util.Util.*"%>
<%@include file="/WEB-INF/InitModel.jsp"%>
<%@taglib uri="http://jakarta.apache.org/taglibs/input-0.90" prefix="input" %>

<html>
    <head>
        <title>Generar Oferta</title>
        <link href="../../../css/estilostsp.css" rel="stylesheet" type="text/css">
        <link href="<%=BASEURL%>/css/estilostsp.css" rel="stylesheet" type="text/css">
        <%
                    String id = "";
        %>
    </head>

    <%
                /* Este codigo es para poder hacer la validacion
                 * de las acciones de cada perfil.
                 *
                 * Ing. Pablo Bassil
                 */
                Usuario usuario = (Usuario)session.getAttribute("Usuario");
                ClientesVerService clvsrv = new ClientesVerService(usuario.getBd());
                String perfil = clvsrv.getPerfil(usuario.getLogin());
    %>

    <script type='text/javascript' src="<%=BASEURL%>/js/Validacion.js"></script>
    <script type='text/javascript' src="<%=BASEURL%>/js/boton.js"></script>
    <script type='text/javascript' src="<%=BASEURL%>/js/general.js"></script>
    <script type='text/javascript' src="<%=BASEURL%>/js/tools.js"></script>
    <script type='text/javascript' src="<%=BASEURL%>/js/date-picker.js"></script>
    <script type='text/javascript' src="<%=BASEURL%>/js/transferencias.js"></script>
    <script type='text/javascript' src="<%=BASEURL%>/js/prototype.js"></script>

    <script type="text/javascript">

        var generar = 0;

        function validate(){
            var a = "";
            var x = document.getElementsByName("checkbox");
            var l = x.length;

            for(var i=0; i<l; i++){
                if(x[i].checked){
                    a = a + x[i].value + ",";
                }
            }

            $("consideraciones").value = a.substring(0,(a.length-1));
        }

        function sendAction(url){
            if(generar == 0){
                if($("nom_oferta").value==''){//2010-04-30 rhonalf
                    alert('Debe escribir un nombre para la oferta');
                }
                else{
                    var p = "nom_oferta=" + $("nom_oferta").value + "&consideraciones=" + $("consideraciones").value + "&text_consi=" + $("text_consi").value + "&meses_mora=" + $("meses_mora").value + "&valh=" + $('nom_oferta_h').value +  "&num_oferta=" + $("num_oferta").value +  "&consecutivo=" + $("consecutivo").value;//JJCASTRO EMERGENCIA
                    new Ajax.Request(
                    url,
                    {
                        method: 'post',
                        parameters: p,
                        onLoading: loading,
                        onComplete: sendMessage
                    });
            }
        }
        else{
            alert('Generacion en proceso...');
        }
    }


    function sendToPrint(url){
        if(generar == 0){

            var p = "num_oferta=" + $("num_oferta").value+"&globales="+$("globales").value;
            if($("nom_oferta").value==''){//2010-04-30 rhonalf
                alert('Debe escribir un nombre para la oferta');
            }
            else{               
                    new Ajax.Request(
                    url,
                    {
                        method: 'post',
                        parameters: p,
                        onLoading: loading,
                        onComplete: sendMessage
                    });               
            }


        }
        else{
            alert('PDF en proceso...');
        }
    }


    function loading(){
        generar = 1;
        $('loading_div').style.visibility = 'visible';
    }

    function sendMessage(response){
        alert(response.responseText);
        $('loading_div').style.visibility = 'hidden';
        generar = 0;
    }

    </script>

    <body>
        <div id="capaSuperior" style="position:absolute; width:100%; height:100px; z-index:0; left: 0px; top: 0px;">
            <jsp:include page="/toptsp.jsp?encabezado=GENERAR OFERTA"/>
        </div>

        <div id="capaCentral" style="position:absolute; width:100%; height:87%; z-index:0; left: 2px; top: 101px; overflow: scroll;">
            <form id="forma" name="forma" method="post" action="<%=CONTROLLEROPAV%>?estado=Electricaribe&accion=Oferta&opcion=2">
                <div align="center">

                    <%
                        id                  = request.getParameter("id_solicitud");
                        DatosOferta dato    = modelopav.ElectricaribeOfertaSvc.ofertaInfo(id);
                        ArrayList ofertas   = modelopav.ElectricaribeOfertaSvc.getConsideracionesItems();
                    %>

                    <table width="700" border="1">
                        <tr>
                            <td width="30%" class="subtitulo">Numero de oferta</td>
                            <td width="70%">
                                <input id="num_oferta" name="num_oferta" type="text" value="<%=request.getParameter("id_solicitud")%>" style="width:490px;" readonly>
                                <input  id="nom_oferta_h" name="nom_oferta_h" type="hidden" value="<%=dato.getOferta()%>" style="width:490px;">
                            </td>
                        </tr>
                        <tr>
                            <td width="30%" class="subtitulo">Nombre de la oferta</td>
                            <td width="70%">
                                <input id="nom_oferta" name="nom_oferta" type="text" value="<%=dato.getOferta()%>" style="width:490px;" maxlength="100" readonly>
                            </td>
                        </tr>
                        <tr>
                            <td width="30%" class="subtitulo">Meses Mora</td>
                            <td width="70%" class="letra">
                                <%  String ofc = dato.getOferta();
                                            int valor = 0;
                                if(ofc==""||ofc==null){
                                    if(clvsrv.isOficial(id)){
                                                    valor = 3;
                                                }
                                }
                                else{
                                                valor = Integer.parseInt(clvsrv.getMesesMora(id));
                                            }
                                %>

                                <select id="meses_mora" disabled>
                                    <option value="0" <% if(valor==0){out.print(" selected");} %>>0</option>
                                    <option value="1" <% if(valor==1){out.print(" selected");} %>>1</option>
                                    <option value="2" <% if(valor==2){out.print(" selected");} %>>2</option>
                                    <option value="3" <% if(valor==3){out.print(" selected");} %>>3</option>
                                </select>

                                <br>
                                Meses mora actual <input id="meses_mora_actual" name="meses_mora_actual" type="text" value="<%=((!dato.getOficial().equals("0"))?dato.getOficial():"No tiene")%>" readonly>

                            </td>
                        </tr>
                        <tr>
                            <td width="30%" class="subtitulo">Consideraciones</td>
                            <td width="70%">
                                <table width="490" border="0">

                                    <%
                                        if(ofertas.size() > 0){
                                            for(int i = 0; i < ofertas.size(); i++){
                                                    TablaGen oferta=(TablaGen)ofertas.get(i);
                                    %>
                                    <tr>
                                        <td>

                                            <%
                                                        if(oferta.getDato().equals("true")){%>
                                            <input checked="true" onClick="validate()" value="<%=oferta.getTable_code()%>" name="checkbox" type="checkbox"  disabled>
                                            <label class="letra"><%=oferta.getDescripcion()%></label>
                                            <%
                                                        }
                                                        else{%>
                                            <input onClick="validate()" value="<%=oferta.getTable_code()%>" name="checkbox" type="checkbox" disabled>
                                            <label class="letra"><%=oferta.getDescripcion()%></label>
                                            <%
                                                                                            }
                                            %>

                                        </td>
                                    </tr>
                                    <%
                                                    }
                                                }
                                    %>

                                </table>
                            </td>
                        </tr>
                        <tr>
                            <td height="50" width="30%" class="subtitulo">Otras</td>
                            <td height="50" width="70%">
                                <textarea id="text_consi" name="text_consi" style="width:490px; height:50px;"><%=dato.getOtras_consideraciones()%></textarea>
                            </td>
                        </tr>
                    </table>

                    <br>
                    <select id="globales">
                        <option value="0" >Generar oferta Costos Detallados</option>
                        <option value="1" >Generar oferta Costos Globales</option>
                    </select>
                    <input  type="hidden" id="consecutivo" name="consecutivo" value="<%=dato.getConsecutivo()%>">

                    <input type="hidden" id="consideraciones">

                    <br>
                    <img id="cargando" name="text_consi" src="<%=BASEURL%>/images/cargando.gif" style="display:none;">
                    <br>

                 

                    <img src="<%=BASEURL%>/images/botones/generarPdf.gif" name="imgimprimir"  onMouseOver="botonOver(this);" onClick="sendToPrint('<%=CONTROLLEROPAV%>?estado=Electricaribe&accion=Oferta&opcion=1');" onMouseOut="botonOut(this);" style="cursor:hand">
                    <img src="<%=BASEURL%>/images/botones/salir.gif"      name="imgsalir"     onMouseOver="botonOver(this);" onClick="parent.close();" onMouseOut="botonOut(this);" style="cursor:hand">

                    <br>

                    <div align="center" id="loading_div" style="visibility: hidden">
                        Cargando...
                    </div>
                </div>
            </form>
        </div>
    </body>
</html>
