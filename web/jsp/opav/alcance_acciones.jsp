<!--     - Author(s)       :      NAVI     - Date            :      10/09     - Copyright Notice:      FINV-->
<%@ page session   ="true"%> 
<%@ page errorPage ="/error/ErrorPage.jsp"%>
<%@ page import    ="java.util.*" %>
<%@ page import    ="com.tsp.util.*" %>
<%@ page import    ="com.tsp.opav.model.beans.NegocioApplus" %>
<%@ page import    ="com.tsp.operation.model.beans.Usuario" %>
<%@ include file="/WEB-INF/InitModel.jsp"%>
<%@taglib prefix="tsp"  uri="http://www.sanchezpolo.com/sot" %>

<html>
    <head>
        <%
                      String solicitud_consultable = request.getParameter("id_solici");
                      String id_accion = request.getParameter("id_acci");
                      String tipo_sol=modelopav.NegociosApplusService.getDescTipoSolc(solicitud_consultable);
                      ArrayList tipo_trabajo = new ArrayList();
                      if(tipo_sol.equals("AAAE")){
                      tipo_trabajo = modelopav.NegociosApplusService.busquedaGeneralRef("TIPO_TRAB","AAAE");
                      }else{
                      tipo_trabajo = modelopav.NegociosApplusService.busquedaGeneral("TIPO_TRAB");
                      }

        %>


        <%
response.setHeader("Content-Type", "text/html; charset=ISO-8859-1");
response.setHeader("Pragma", "no-cache"); response.setHeader("Expires","0"); response.setHeader("Cache-Control", "no-cache");
response.setHeader("Cache-Control", "no-store"); response.setHeader("Cache-Control", "must-revalidate");

%>
        <title>Alcance</title>
        <link   href="<%= BASEURL%>/css/estilostsp.css" rel="stylesheet" type="text/css">
        <script src="<%= BASEURL%>/js/boton.js"></script>

        <script>
            function enviar(theForm, bot,opcione){
                if (opcione=="observaciones"){//
                    if (theForm.observaci.value==""){//

                        alert("Debe tener una observaci�n.");//
   					    theForm.aceptir.style.visibility='visible';
                    }
					else
					{
						if (theForm.tipotraba.value=="")
						{//
						alert("Debe seleccionar el tipo de trabajo.");//
   					    theForm.aceptir.style.visibility='visible';
						}
						else
						{
                        theForm.submit();
						}
                    }
                }
////JJCASTRO PDF OT
                if (opcione=="alcances"){//
                    //alert("alcances_");
                    if (theForm.alcanci.value==""){//
                        alert("Debe tener un alcance.");//
                        theForm.aceptir2.style.visibility='visible';			//091030
                    }else{

                        if (theForm.trabaji.value==""){

                            alert("Debe digitar la descripción del trabajo.");//
                            theForm.aceptir2.style.visibility='visible';

                        }else{
                            formulario.action="<%=CONTROLLEROPAV%>?estado=Negocios&accion=Applus&opcion=marcarAlcances";
                            theForm.submit();

                        }
                    }
                }
//JJCASTRO PDF OT

            }
            function MostrarTablaInferior(){
                var definicion = document.getElementById("definicionI");

                if(definicion.checked){
                    document.getElementById("tablaSuperior").style.display = 'none';
                    document.getElementById("tablaInferior").style.display = 'block';
                }
            }
            function MostrarTablaSuperior(){
                var definicion = document.getElementById("definicionS");

                if(definicion.checked){
                    document.getElementById("tablaSuperior").style.display = 'block';
                    document.getElementById("tablaInferior").style.display = 'none';
                }
            }
            function CargarTabla(){
                var auxiliar = document.getElementById("auxiliar").value;
                if(auxiliar == 'on'){
                    document.getElementById("tablaSuperior").style.display = 'block';
                    document.getElementById("tablaInferior").style.display = 'none';
                    document.getElementById("definicionS").checked = true;
                    document.getElementById("definicionI").checked = false;
                }else if(auxiliar == 'off'){
                    document.getElementById("tablaSuperior").style.display = 'none';
                    document.getElementById("tablaInferior").style.display = 'block';
                    document.getElementById("definicionS").checked = false;
                    document.getElementById("definicionI").checked = true;
                }
            }
            function regresar(){

                    document.getElementById("formulario").action="<%=CONTROLLEROPAV%>?estado=Clientes&accion=Ver&opcion=buscarsolicitud&idsolicitud=<%=solicitud_consultable%>";
                    document.getElementById("formulario").submit();

            }
        </script>
    </head>
    <body onLoad="CargarTabla()">
        <%
            String path = application.getRealPath(model.menuService.getRealUrl(request.getRequestURI()));
            String datos[] = model.menuService.getContenidoMenu(BASEURL, request.getRequestURI(), path);%>


        <div id="capaSuperior" style="position:absolute; width:100%; height:100px; z-index:0; left: 0px; top: 0px;">
            <jsp:include page="/WEB-INF/toptsp.jsp?encabezado=Alcances"/>
        </div>

        <div id="capaCentral" style="position:absolute; width:100%; height:85%; z-index:0; left: 0px; top: 100px; overflow: scroll;">
            <center>
                <% String msj = (request.getParameter("msj") == null) ? "" : request.getParameter("msj");%>

          <form action='<%= CONTROLLEROPAV%>?estado=Negocios&accion=Applus&opcion=marcarTipoTrabajo' method='post' name='formulario' id='formulario' onsubmit='return false;'>
              <!--AGREGADO JPENA 07-04-2010 -->
              <input type="hidden" name="auxiliar" value="<%=request.getParameter("auxiliar")%>" id="auxiliar" />
                    <table width="500" border="2" align="center">
                        <tr>
                            <td>
                                <table width='100%' align='center' class='tablaInferior'>
                                  <tr>
                                    <td colspan='2' class="barratitulo" >
									<table cellpadding='0' cellspacing='0' width='100%'>
                                        <tr class="fila">
                                          <td width="50%" align="left" class="subtitulo1">&nbsp;Acciones</td>
                                                    <td width="*"   align="left"><img src="<%= BASEURL%>/images/titulo.gif" width="32" height="20"  align="left"><%=datos[0]%></td>
                                        </tr>
                                        <tr class="fila">
                                          <td align="left" ><div align="center">
                                            <label>
                                            <input name="definicion" id="definicionS" type="radio" value="radiobutton" checked onClick="MostrarTablaSuperior()">
                                            </label>
                                            <strong>Definici&oacute;n de acciones </strong></div></td>
                                          <td align="left" ><div align="center">
                                            <label>
                                                <input name="definicion" id="definicionI" type="radio" value="radiobutton" onClick="MostrarTablaInferior()">
                                            </label>
                                            <strong>Definici&oacute;n de alcances </strong></div></td>
                                        </tr>
                                    </table>
                                    <!--<img src='<%//=BASEURL%>/images/botones/iconos/mas.gif' width="15" height="15" onClick="agregarObservacion(formulario);" title="Agregar" style="cursor:hand" >-->                                    </td>
                                  </tr>
                                </table>
                                <table width='100%' align='center' id="tablaSuperior"  style="display:block">


                                    <input type="hidden" value="<%=solicitud_consultable%>" name="id_solici">
                                    <input type="hidden" value="<%=id_accion%>" name="id_accion">
                                    <%
Usuario usuario = (Usuario) session.getAttribute("Usuario");
String loginx = usuario.getLogin();

            NegocioApplus negocioApplus = modelopav.NegociosApplusService.obtainAlcanc(solicitud_consultable, id_accion, loginx);%>

            <input type="hidden" name="estadinho" value="<%=negocioApplus.getIdEstado()%>" id="estadinho" /><!--20100513-->
            <%System.out.println("negocioApplus.getIdEstado()" + negocioApplus.getIdEstado());%>
                                    <%System.out.println("negocioApplus.getFecha()" + negocioApplus.getFecha() + "solicitud_consultable" + solicitud_consultable + "id_accion" + id_accion + "loginx" + loginx);%>

                                    <tr class="barratitulo">
                                        <td  class='informacion' >
                                            <strong>&nbsp;Descripcion&nbsp;</strong>                                        </td>
                                        <td  class='informacion' height='60'>
                                            <textarea  cols="60" rows="7" readonly><%=negocioApplus.getSolicitud()%></textarea>                                        </td>
                                    </tr>
                                    <tr class="barratitulo">
                                        <td  class='informacion' >
                                            <strong>&nbsp;Fecha&nbsp;</strong>                                        </td>
                                        <td  class='informacion' height='25'>

                                            &nbsp;<%=(negocioApplus.getFecha()).substring(0, 16)%>&nbsp;                                        </td>
                                    </tr>
                                    <tr class="barratitulo">
                                        <td  class='informacion' >
                                            <strong>&nbsp;Acciones&nbsp;</strong>                                        </td>
                                        <td  class='informacion' height='60'>
                                            &nbsp;<%=negocioApplus.getAcciones()%>&nbsp;                                        </td>
                                    </tr>

									<tr class="barratitulo">
                                        <td  class='informacion' >
                                            <strong>&nbsp;Fecha Visita Planeada&nbsp;</strong>                                        </td>
                                        <td  class='informacion' height='60'>
                                            &nbsp;
											<!--091030-->
											<input name="fecvisitaplan" type="text" class="textbox" id="fecvisitaplan" size="10" readonly value="<%=negocioApplus.getFecVisitaPlan()%>" >
                                                                                        <%if (negocioApplus.getFecVisitaPlan()==null || negocioApplus.getFecVisitaPlan().equals("")||negocioApplus.getIdEstado().equals("020")){%>
													<span class="Letras"><img src="<%=BASEURL%>/images/cal.gif" width="16" height="16" align="absmiddle" style="cursor:hand " onClick="if(self.gfPop)gfPop.fPopCalendar(document.formulario.fecvisitaplan);return false;" HIDEFOCUS></span>
											<%}%>
&nbsp;                                        </td>
                                    </tr>

									<tr class="barratitulo">
                                        <td  class='informacion' >
                                            <strong>&nbsp;Fecha Visita Hecha&nbsp;</strong>                                        </td>
                                        <td  class='informacion' height='60'>
                                            &nbsp;
											<!--091030-->
											<input name="fecvisitareal" type="text" class="textbox" id="fecvisitareal" size="10" readonly value="<%=negocioApplus.getFecVisitaReal()%>" >
											<%if (negocioApplus.getFecVisitaReal()==null || negocioApplus.getFecVisitaReal().equals("")||negocioApplus.getIdEstado().equals("020")){%>
												<span class="Letras"><img src="<%=BASEURL%>/images/cal.gif" width="16" height="16" align="absmiddle" style="cursor:hand " onClick="if(self.gfPop)gfPop.fPopCalendar(document.formulario.fecvisitareal);return false;" HIDEFOCUS></span>
											<%}%>
&nbsp;                                        </td>
                                    </tr>

                                    <tr class="barratitulo">
                                        <td  class='informacion' >
                                            <strong>&nbsp;Tipo de trabajo&nbsp;</strong>                                        </td>
                                    <strong>

<td  class='informacion' height='60'>
                                            <input type="hidden" name="extipotraba" value="<%=negocioApplus.getTipoTrabajo()%>">
                                            <%if (negocioApplus.getTipoTrabajo() == null || negocioApplus.getTipoTrabajo().equals("") || negocioApplus.getTipoTrabajo().equals("Sin Definir") || negocioApplus.getIdEstado().equals("020")) {%>
                                            &nbsp;										<select name="tipotraba"    >
                                                <option  value=""></option>
                                                <% String[] dato1 = null;
                                                     for (int i = 0; i < tipo_trabajo.size(); i++) {
                                                         dato1 = ((String) tipo_trabajo.get(i)).split(";_;");%>
                                                <option value="<%=dato1[0]%>" <%=(negocioApplus.getTipoTrabajo().equals(dato1[0])) ? " selected" : ""%> ><%=dato1[1]%></option>
                                                <%  }%>
                                                <%} else {%>
                                                <input type="text" name="tipotraba" readonly value="<%=negocioApplus.getTipoTrabajo()%>">
                                                <%}%>
                                            </select>
                                </td>

                                    </strong>                        </tr>

                        <%if (negocioApplus.getObservacion()!=null && !(negocioApplus.getObservacion().equals(""))){%>
                            <tr class="barratitulo">
                                <td  class='informacion' >
                                    <strong>&nbsp;Observacion&nbsp;</strong>                                </td>
                                <td  class='informacion' height='60'>
                                    &nbsp;
                                    <textarea cols="60" rows="7" readonly><%=negocioApplus.getObservacion()%></textarea>
&nbsp;                                </td>
                            </tr>
                        <%}%>
                        <tr class="barratitulo">
                            <td  class='informacion' >
                                <strong>&nbsp;Observacion Adicional&nbsp;</strong>                            </td>
                            <td  class='informacion' height='60'>
                                &nbsp;
                                <textarea cols="60" rows="7" name="observaci"></textarea>
                                &nbsp;
                                <!--<img src='<%//=BASEURL%>/images/botones/iconos/mas.gif' width="15" height="15" onClick="agregarObservacion(formulario);" title="Agregar" style="cursor:hand" >-->                            </td>
                        </tr>
                        <tr class="barratitulo">
                          <td height="60" colspan="2"  class='informacion' ><div align="center">
						  <!--botones -->
                        <p align="center">
                            <img alt="regresar" src="<%= BASEURL%>/images/botones/regresar.gif"  name="imgregresar"  onMouseOver="botonOver(this);" onClick="regresar();" onMouseOut="botonOut(this);">
                            <% if (msj.equals("")) {%>
                            <tsp:boton value="aceptar"   onclick="this.style.visibility='hidden';enviar(formulario,this,'observaciones');" name="aceptir"  />
                            <%}%>
                            <tsp:boton value="salir"     onclick="parent.close();"/>
                        </p>
						  </div></td>
                          </tr>
                    </table>


                         <!--SEGUNDA PARTE DEL FORMULARIO -->
                         <table id="tablaInferior" style="display:none">

						<tr class="barratitulo">
                            <td  class='informacion' >
                                <strong>&nbsp;Alcances&nbsp;</strong>                            </td>
                            <td  class='informacion' height='60'>
                                &nbsp;
                                <textarea cols="60" rows="7" name="alcanci" ><%=negocioApplus.getAlcances()%></textarea>
                                &nbsp;
                                <!--<img src='<%//=BASEURL%>/images/botones/iconos/mas.gif' width="15" height="15" title="Agregar" style="cursor:hand" >-->                            </td>
                        </tr>


						<tr class="barratitulo">
                            <td  class='informacion' >
                                <strong>&nbsp;Adicionales&nbsp;</strong>                            </td>
                            <td  class='informacion' height='60'>
                                &nbsp;
                                <textarea cols="60" rows="7" name="adicioni"><%=negocioApplus.getAdiciones()%></textarea>
                                &nbsp;
                                <!--<img src='<%//=BASEURL%>/images/botones/iconos/mas.gif' width="15" height="15" title="Agregar" style="cursor:hand" >-->                            </td>
                        </tr>
                    <tr class="barratitulo">
                      <td  class='informacion' >
                           <strong>&nbsp;Descripcion_Trabajo&nbsp;</strong>                      </td>
                      <td  class='informacion' height='60'>
                           &nbsp;
                           <textarea cols="60" rows="7" name="trabaji"><%=negocioApplus.getTrabajo()%></textarea>
&nbsp;                      </td>
                    </tr>
                    <tr class="barratitulo">
                      <td height="60" colspan="2"  class='informacion' ><div align="center">
                              <!--botones -->
                        <p align="center">
                            <tsp:boton value="aceptar"   onclick="this.style.visibility='hidden';enviar(formulario,this,'alcances');"  name="aceptir2" />
                            <tsp:boton value="salir"     onclick="parent.close();"/>
                        </p>
					  </div></td>
                      </tr>
                    </table>




                    </td>
                    </tr>
                    </table>

                </form>
                <br>

                <% if (!msj.equals("")) {%>
                <table border="2" align="center">
                    <tr>
                        <td>
                            <table width="100%"  border="1" align="center"  bordercolor="#F7F5F4" bgcolor="#FFFFFF">
                                <tr>
                                    <td width="400" align="center" class="mensajes"><%=msj%></td>
                                    <td width="29" background="<%=BASEURL%>/images/cuadronaranja.JPG">&nbsp;</td>
                                    <td width="58">&nbsp; </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
                <%}%>

        </div>
        <%=datos[1]%>
		<iframe width=188 height=166 name="gToday:normal:agenda.js:gfPop:plugins.js" id="gToday:normal:agenda.js:gfPop:plugins.js" src="<%=BASEURL%>/js/Calendario/ipopeng.htm" scrolling="no" frameborder="0" style="visibility:visible; z-index:999; position:absolute; left:-500px; top:0px;"> </iframe><!--091030-->
    </body>
</html>