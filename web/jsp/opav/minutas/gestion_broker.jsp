<%-- 
    Document   : gestion_broker
    Created on : 10/07/2016, 11:16:17 AM
    Author     : mcastillo
--%>

<%@page contentType="text/html" pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
        <meta name="viewport" content="width=device-width, initial-scale=1">


        <title>Gesti�n de P�lizas</title>


        <link href="./css/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css"/>
        <link href="./css/simple-sidebar.css" rel="stylesheet" type="text/css"/>
        <link href="./css/OportunidadNegocio.css" rel="stylesheet" type="text/css"/>


        <link type="text/css" rel="stylesheet" href="./css/jquery/jquery-ui/jquery-ui.css"/>     
        <script type="text/javascript" src="./js/jquery/jquery-ui/jquery.min.js"></script>
        <script type="text/javascript" src="./js/jquery/jquery-ui/jquery.ui.min.js"></script>      

        <link type="text/css" rel="stylesheet" href="./css/buttons/botonVerde.css"/>
        <script type="text/javascript" src="./js/jquery/jquery.jqGrid/js/grid.locale-es.js"></script>
        <script type="text/javascript" src="./js/jquery/jquery.jqGrid/js/jquery.jqGrid.min.js"></script>
        <link type="text/css" rel="stylesheet" href="./css/jquery/jquery.jqGrid/ui.jqgrid.css"/> 
    </head>
    <body>
        <jsp:include page="/WEB-INF/toptsp.jsp?encabezado=Modulo Gesti�n de P�lizas"/>

        <script type="text/javascript" src="./js/gestionBroker.js"></script> 
        <!--css logica de negocio-->
        <link href="./css/contratos.css" rel="stylesheet" type="text/css"> 
        <div class="ui-jqgrid ui-widget ui-widget-content center-block" style="width: 1317px; height-min: 300px; margin-top: 10px">

            <div class="ui-dialog-titlebar ui-widget-header ui-helper-clearfix">
                <span class="ui-dialog-title text-center" id="ui-dialog-title-listaGlobal">
                    &nbsp;
                    <label><b>Busqueda - P�lizas Asignadas a Broker</b></label>
                </span>
            </div>

            <div class="row" style="padding: 5px">

                <div class="col-md-12 ">

                    <fieldset class="scheduler-border">
                        <legend class="scheduler-border">Busqueda De Solicitudes</legend>
                        <div id="div_filtro_solicitudes"> 
                            <div class="row">
                                <div class="col-md-12">
                                    <table>
                                        <tr>
                                            <td>
                                                <label for="fecha" style="">Fecha contrato:</label>
                                            </td>
                                            <td>
                                                <input type="text" id="fecha_ini_contrato" readonly style="width:84px"/>
                                            </td>
                                            <td>
                                                --
                                            </td>
                                            <td>
                                                <input type="text" id="fecha_fin_contrato" readonly style="width:84px"/>
                                            </td>
                                            <td>
                                                <label for="solicitud" style="margin-left: 10px;">No Solicitud:&nbsp;&nbsp;</label>
                                            </td>
                                            <td>
                                                <input id="num_solicitud" name="num_solicitud" size="15" style="width: 84px;" type="text" maxlength="15"/> 
                                            </td>
                                            <td>
                                                <label style="margin-left: 10px;">Cliente</label>
                                            </td>
                                            <td>
                                                <input type="text" id="nombre_cliente"  style="width: 230px;margin-left: 5px;height: 26px;" >
                                            </td>
                                            <td>
                                                <label style="margin-left: 10px;">Linea de negocio</label>
                                            </td>
                                            <td>
                                                <select id="linea_negocio" style="width: 151px;color: #050505;height: 25px;margin-left: 5px;" ></select>
                                            </td>
                                            <td>
                                                <label style="margin-left: 10px;">Responsable</label>
                                            </td>
                                            <td>
                                                <input type="text" id="responsable"  style="width: 139px;margin-left: 5px;height: 26px;" >
                                            </td>
                                        </tr>
                                    </table>
                                </div>


                            </div>
                        </div>
                    </fieldset>
                    <div class="row fx-center">
                        <div class="col-md-4 fx-center">
                            <button id="listarSolicitudes" class="ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only" 
                                    role="button" aria-disabled="false">
                                <span class="ui-button-text">Buscar</span>
                            </button>&nbsp;&nbsp;
                            <button id="clearSolicitudes" class="ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only" 
                                    role="button" aria-disabled="false">
                                <span class="ui-button-text">Limpiar</span>
                            </button>
                        </div>
                    </div>     
                </div>
            </div>


        </div> 
    <center>

        <br><br>
        <table id="tabla_solicitudes_broker" align="center" ></table>  
        <div id="page_tabla_solicitudes_broker"></div>

    </center>
    <div id="div_polizas_proyecto"  style="display: none; width: 750px" >
        <center>
            <br>
            <table id="tabla_garantias"></table>
            <div id="page_tabla_garantias"></div>    
        </center>
    </div>    
    <div id="div_detalle_cot_aseguradora"  style="display: none; width: 750px" >
        <table border="0" align="left" cellpadding="1" cellspacing="1" class="labels">
            <tr>               
                <td style="width: 25%"><span>&nbsp;Nombre aseguradora</span></td>        
                <td style="width: 75%">
                    <input type="text" name="nombre_aseguradora" id="nombre_aseguradora" readonly style="width: 390px"/>
                </td>
            </tr>                 
        </table> <br>
        <center>
            <br>
            <table id="tabla_detalle_aseguradora"></table>
            <div id="page_tabla_detalle_aseguradora"></div>    
        </center>
        <table border="0" align="right" cellpadding="1" cellspacing="1" class="labels bordecotizacion"style="margin-top: -1px;margin-right: 17px">
            <tr>               
                <td style="width: 62.9%" class="bordecotizacion"><span style="font-weight: bold;">&nbsp;Otros Gastos</span></td>        
                <td align="right" class="bordecotizacion">
                    <input type="text" name="total_gastos" id="total_gastos" readonly value="0" style="width: 133px; border: 1px solid transparent;text-align: right"/>
                </td>
            </tr>                 
           <tr>               
                <td style="width: 62.9%" class="bordecotizacion"><span style="font-weight: bold;">&nbsp;Valor IVA</span></td>        
                <td align="right" class="bordecotizacion">
                    <input type="text" name="total_iva" id="total_iva" value="0" readonly style="width: 133px; border: 1px solid transparent;text-align: right"/>
                </td>
            </tr>                 
            <tr>               
                <td style="width: 62.9%" class="bordecotizacion"><span  style="font-weight: bold;">&nbsp;Total</span></td>        
                <td align="right" class="bordecotizacion">
                    <input type="text" name="total" id="total" readonly value ="0" style="width: 133px; border: 1px solid transparent;text-align: right"/>
                </td>
            </tr>                 
        </table>       
    </div> 
    <!-- Dialogo ventana agregar cotizaci�n aseguradora -->
    <div id="dialogCotizacionAseguradora"  style="display:none;">       
        <br>                           
        <table border="0" align="left" cellpadding="1" cellspacing="1" class="labels">
            <tr>
            <input type="text" id="id_solicitud"  style="width: 50px"hidden >
            <td style="width: 11%"><span>Aseguradora <b style="color:red">*</b></span></td>        
            <td style="width: 28%">
                <select name="aseguradora" class="combo_180px" id="aseguradora" style="width: 210px"></select>
            </td>
            <td style="width: 11%"><span>Beneficiario <b style="color:red">*</b></span></td>        
            <td style="width: 26%">
                <select name="beneficiario" class="combo_180px" id="beneficiario" style="width: 190px"></select>
            </td>
            <td style="width: 8%"><span>Otro Si </span></td>        
            <td style="width: 20%">
                <select name="otro_si" class="combo_180px" id="otro_si" style="width: 129px"></select>
            </td>
            </tr>                 
        </table> <br>
        <div id="div_cotizacion_aseguradora"  style="width: 550px" >
            <center>
                <br>
                <table id="tabla_cotizacion_aseguradora"></table>
                <div id="page_tabla_cotizacion_aseguradora"></div>    
            </center>
        </div> <br>

        <table border="0" align="right" cellpadding="1" cellspacing="1" class="labels bordecotizacion"style="margin-top: -18px;margin-right: 9px">
            <tr>               
                <td style="width: 62.7%" class="bordecotizacion"><span style="font-weight: bold;">&nbsp;Otros Gastos</span></td>        
                <td align="right" class="bordecotizacion">
                    <input type="text" name="valor_gastos" id="valor_gastos" class="solo-numero" value="0" style="width: 122.2px; border: 1px solid transparent;text-align: right"/>
                </td>
            </tr>                 
            <tr>               
                <td style="width: 62.7%" class="bordecotizacion"><span style="font-weight: bold;">&nbsp;Valor IVA</span></td>        
                <td align="right" class="bordecotizacion">
                    <input type="text" name="valor_iva" id="valor_iva" class="solo-numero" value="0" readonly style="width: 122.2px; border: 1px solid transparent;text-align: right"/>
                </td>
            </tr>                 
            <tr>               
                <td style="width: 62.7%" class="bordecotizacion"><span  style="font-weight: bold;">&nbsp;Total</span></td>        
                <td align="right" class="bordecotizacion">
                    <input type="text" name="valor_total" id="valor_total" readonly value ="0" style="width: 122.2px; border: 1px solid transparent;text-align: right"/>
                </td>
            </tr>                 
        </table>
    </div>

    <div id="dialogLoading" style="display:none;">
        <p  style="font-size: 12px;text-align:justify;" id="msj2">Texto </p> <br/>
        <center>
            <img src="/fintra/images/cargandoCM.gif"/>
        </center>
    </div>
    <div id="dialogMsj" title="Mensaje" style="display:none;">
        <p style="font-size: 12px;text-align:justify;" id="msj" > Texto </p>
    </div>    
</body>
</html>
