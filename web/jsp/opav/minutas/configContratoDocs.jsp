<%-- 
    Document   : configContratoDocs
    Created on : 25/04/2016, 09:01:18 AM
    Author     : user
--%>


<%@page contentType="text/html" pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html lang="es">

    <head>

        <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
        <meta name="viewport" content="width=device-width, initial-scale=1">


        <title>Oportunidad De Negocio</title>



        <link href="./css/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css"/>
        <link href="./css/simple-sidebar.css" rel="stylesheet" type="text/css"/>
        <link href="./css/OportunidadNegocio.css" rel="stylesheet" type="text/css"/>




        <link type="text/css" rel="stylesheet" href="./css/jquery/jquery-ui/jquery-ui.css"/>     
        <script type="text/javascript" src="./js/jquery/jquery-ui/jquery.min.js"></script>
        <script type="text/javascript" src="./js/jquery/jquery-ui/jquery.ui.min.js"></script> 
        <script src="./js/jquery/editor/ckeditor/ckeditor.js" type="text/javascript"></script>



        <link type="text/css" rel="stylesheet" href="./css/buttons/botonVerde.css"/>
        <script type="text/javascript" src="./js/jquery/jquery.jqGrid/js/grid.locale-es.js"></script>
        <script type="text/javascript" src="./js/jquery/jquery.jqGrid/js/jquery.jqGrid.min.js"></script>
        <link type="text/css" rel="stylesheet" href="./css/jquery/jquery.jqGrid/ui.jqgrid.css"/>    


    </head> 
    <body>
    <center>
        <jsp:include page="/WEB-INF/toptsp.jsp?encabezado=Modulo Oportunidad De Negocio"/>


        <script src="./js/configMinutas.js" type="text/javascript"></script>
        <link type="text/css" rel="stylesheet" href="./css/contratos.css " />


        <center>
            <div id="div_busqueda" class="frm_search" style="width:550px;display:none;">  
                <table>            
                    <tr>
                        <td>
                            <label for="tipo_trabajo"> Tipo de Proyecto:&nbsp&nbsp</label>
                            <select name="tipo_trabajo" id="tipo_proyecto" class="combo_180px">
                                <option value="">Seleccione el proyecto</option>
                            </select>                     
                        </td>             
                        <td style="padding-left: 10px">
                            <button id="listarConfiguracion" class="ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only" 
                                    role="button" aria-disabled="false">
                                <span class="ui-button-text">Buscar</span>
                            </button>
                        </td>
                    </tr> 
                </table>
            </div>
            <br>
            <div id="tabs_contrato" style="width: 1750px;height:630px">
                <ul id="lst_documents">
                    <span style="float:right;">
                        <img id="correspondencias" title="Ver correspondencias" src="/fintra/images/botones/iconos/tab_search.png" 
                             style="margin-left: 5px; height: 100%; vertical-align: middle;"> 
                    </span>
                </ul>  
            </div>   
        </center>
        <div id="equiv_variables" style="display:none;">  
            <table id="drag_popup_equivalencias" width="282" style="margin-left:8px" >
                <tr>
                    <td  class="titulo_ventana" height="25">
                        <div style="float:left; font-size: 15px">CORRESPONDENCIAS</div> 
                        <div style="float:right"><a id="close" class="ui-widget-header ui-corner-all"><span>X</span></a></div>
                    </td>
                </tr>

            </table>
            <center><table id="tbl_equiv_variables" class="tablas" style=" width: 95%"></table></center>
        </div>
        <div id="dialogLoading" style="display:none;">
            <p  style="font-size: 12px;text-align:justify;" id="msj2">Texto </p> <br/>
            <center>
                <img src="./images/cargandoCM.gif"/>
            </center>
        </div>
        <!-- Dialogo de los jsp> -->
        <div id="dialogMsj" style="display:none;">
            <p style="font-size: 12px;text-align:justify;" id="msj" > Texto </p>
        </div>
    </center>
</body>
</html>
