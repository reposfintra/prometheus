<%-- 
    Document   : generarContrato
    Created on : 19/04/2016, 02:47:13 PM
    Author     : user
--%>

<%@page contentType="text/html" pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
        <title>Generar Contrato</title>

        <link type="text/css" rel="stylesheet" href="/fintra/css/jquery/jquery-ui/jquery-ui.css" />      
        <link type="text/css" rel="stylesheet" href="/fintra/css/buttons/botonVerde.css"/>
        <link href="/fintra/css/popup.css" rel="stylesheet" type="text/css">

        <script type="text/javascript" src="/fintra/js/jquery/jquery.jqGrid/js/jquery.min.js"></script> 
        <script type="text/javascript" src="/fintra/js/jquery-ui-1.8.5.custom.min.js"></script>
        <script src="/fintra/js/contratos.js" type="text/javascript"></script>

        <!--css logica de negocio-->
        <link type="text/css" rel="stylesheet" href="/fintra/css/contratos.css " />


        <!--jqgrid--> 
        <script type="text/javascript" src="/fintra/js/jquery/jquery.jqGrid/js/grid.locale-es.js"></script>
        <script type="text/javascript" src="/fintra/js/jquery/jquery.jqGrid/js/jquery.jqGrid.min.js"></script>
        <script type="text/javascript" src="/fintra/js/jquery/jquery.jqGrid-4.7.0/plugins/jquery.contextmenu.js"></script>

        <%

            String num_solicitud = (request.getParameter("num_solicitud") != null) ? request.getParameter("num_solicitud") : "";
            String nombre_proyecto = (request.getParameter("proyecto") != null) ? request.getParameter("proyecto") : "N";
            String aiu = (request.getParameter("aiu") != null) ? request.getParameter("aiu") : "N";

        %>
        <style>
            .seleccionado:focus{
                /*width: 293px;*/
                border-radius: 7px;
                box-shadow: 0 0 2px 1px #2A88C8E6;
            }
        </style>
    </head>
    <body>
        <div id="capaSuperior"  style="position:absolute; width:100%; height:100px; z-index:0; left: 0px; top: 0px;">
            <jsp:include page="/WEB-INF/toptsp.jsp?encabezado=Generaci�n Contrato"/>
        </div>
        <div id="capaCentral" style="position:absolute; width:100%; height:100%; z-index:0; left: 0px; top: 100px; ">
            <br>
            <center>
                <form name="form_generar_contrato" id="form_generar_contrato" method="post" enctype="multipart/form-data">
                    <div id="div_generar_contrato"  style="width: 1310px" >   
                        <div id="encabezadotablita" style="width: 1310px">
                            <table aling="center" style="position: absolute; width: 100%" > 
                                <tr>
                                    <td colspan="2">
                                        <label class="titulotablita"><b><%=nombre_proyecto%></b></label>
                                    </td>                              
                                </tr>
                                <tr>
                                    <td>
                                        <input type="text" name="num_solicitud" id="num_solicitud" value="<%=num_solicitud%>"  style="width: 50px" hidden >
                                        <input type="hidden" name="asignado_broker" id="asignado_broker" value=""  style="width: 50px" >
                                        <input type="hidden" name="num_contrato" id="num_contrato" value=""  style="width: 50px" >
                                    </td>
                                </tr>
                            </table>                       
                        </div> 
                        <table aling="center" style=" width: 100%" >        
                            <tr>
                                <td colspan="6">
                                    <h4>DATOS CONTRATANTE</h4>
                                    <hr>
                                </td>
                            </tr>                       
                            <tr>
                                <td>
                                    <label> Razon Social<span id="req_razon_social_empresa" style="color:red;display:none;">*</span></label> 
                                </td>
                                <td>
                                    <input type="text" class="seleccionado" name="razon_social_empresa" id="razon_social_empresa" onchange="conMayusculas(this)" style="width: 293px;" readonly/>
                                </td>
                                <td>
                                    <label> Nit<span id="req_nit_empresa" style="color:red;display:none;" readonly>*</span></label> 
                                </td>
                                <td colspan="3">
                                    <input type="text" class="seleccionado" name="nit_empresa"  id="nit_empresa" class="solo-numero" maxlength="12" readonly/>
                                    <span>DV&nbsp;</span><input name="digito_verificacion" type="text" id="digito_verif_empresa" style="width:30px;" size="15" readonly>

                                </td>
                            </tr>  
                            <tr>
                                <td>
                                    <label> Direcci�n<span id="req_direccion_empresa" style="color:red;display:none;">*</span></label> 
                                </td>
                                <td>
                                    <input type="text" class="seleccionado" name="direccion_empresa" id="direccion_empresa" onchange="conMayusculas(this)" style="width: 293px" readonly/>
                                    &nbsp;
                                    <img src="/fintra/images/Direcciones.png" width="26" height="23" border="0" align="ABSMIDDLE" onclick="false/*genDireccion('direccion_empresa',event);*/" alt="Direcciones"  title="Direcciones" />
                                </td>  
                                <td>
                                    <label> Fecha Expedici�n</label>                                       
                                </td>
                                <td>
                                    <input type="text" class="seleccionado" id="fecha_expedicion" readonly/>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <label> Representante Legal<span id="req_representante_empresa" style="color:red;display:none;">*</span></label> 
                                </td>
                                <td>
                                    <input type="text" class="seleccionado" name="representante_empresa" id="representante_empresa" onchange="conMayusculas(this)" style="width: 293px" readonly />
                                </td>
                                <td>
                                    <label> Cedula <span id="req_cedula_rep_empresa" style="color:red;">*</span></label> 
                                </td>
                                <td colspan="3">
                                    <input type="text" class="seleccionado" name="cedula_rep_empresa"  id="cedula_rep_empresa" class="solo-numero" maxlength="12"/>                              
                                </td>
                            </tr>  
                            <!--                    <tr>
                                                        <td colspan="6">
                                                           <h4>DATOS CONTRATISTA</h4>
                                                           <hr>
                                                        </td>
                                                    </tr>                      -->
                            <!--                        <tr>
                                                        <td>
                                                            <label> Razon Social<span id="req_razon_social_contratista" style="color:red;">*</span></label> 
                                                        </td>
                                                        <td>
                                                            <input type="text" name="razon_social_contratista" id="razon_social_contratista" onchange="conMayusculas(this)" style="width: 293px"  />
                                                        </td>
                                                        <td>
                                                            <label> Nit<span id="req_nit_contratista" style="color:red;">*</span></label> 
                                                        </td>
                                                        <td colspan="3">
                                                            <input type="text" name="nit_contratista"  id="nit_contratista" class="solo-numero" maxlength="12"/>
                                                            <span>DV&nbsp;</span><input name="digito_verificacion" type="text" id="digito_verif_contratista" style="width:30px;" size="15" readonly>
                            
                                                        </td>
                                                    </tr>  
                                                    <tr>
                                                        <td>
                                                            <label> Direcci�n<span id="req_direccion_contratista" style="color:red;">*</span></label> 
                                                        </td>
                                                        <td colspan="5">
                                                            <input type="text" name="direccion_contratista" id="direccion_contratista" onchange="conMayusculas(this)" style="width: 293px" readonly/>
                                                            &nbsp;
                                                            <img src="/fintra/images/Direcciones.png" width="26" height="23" border="0" align="ABSMIDDLE" onclick="genDireccion('direccion_contratista',event);" alt="Direcciones"  title="Direcciones" />
                                                        </td>                            
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            <label> Representante Legal<span id="req_representante_contratista" style="color:red;">*</span></label> 
                                                        </td>
                                                        <td>
                                                            <input type="text" name="representante_contratista" id="representante_contratista" onchange="conMayusculas(this)" style="width: 293px"  />
                                                        </td>
                                                        <td>
                                                            <label> Cedula <span id="req_cedula_rep_contratista" style="color:red;">*</span></label> 
                                                        </td>
                                                        <td colspan="3">
                                                            <input type="text" name="cedula_rep_contratista" id="cedula_rep_contratista" class="solo-numero" maxlength="12"/>                              
                                                        </td>
                                                    </tr>-->
                            <tr  style="display:none">                           
                                <td>   
                                    <label> Nombre del Proyecto<span id="req_nombre_proyecto" style="color:red;display:none">*</span></label> 
                                </td>
                                <td colspan="5">
                                    <br><br>
                                    <input type="hidden" class="seleccionado" name="nombre_proyecto" id="nombre_proyecto" onchange="conMayusculas(this)" style="width:750px"/>                              
                                </td>                            
                            </tr>
                            <!--                    <tr>                           
                                                        <td>   
                                                            <label> Alcance del Proyecto<span id="req_alcance_proyecto" style="color:red;">*</span></label> 
                                                        </td>
                                                        <td colspan="5">
                                                            <textarea name="alcance_proyecto" id ="alcance_proyecto" style="width:750px;resize:none" rows="3" maxlength="300"></textarea>                                                       
                                                        </td>                            
                                                    </tr>
                                                    <tr>                           
                                                        <td>   
                                                            <label> Duraci�n Proyecto<span id="req_duracion_proyecto" style="color:red;">*</span></label> 
                                                        </td>
                                                        <td colspan="5">
                                                            <input type="text" name="fecha_inicio_proyecto" id="fecha_inicio_proyecto" readonly/>
                                                            <input type="text" name="fecha_fin_proyecto" id="fecha_fin_proyecto" readonly/>                                                     
                                                        </td>                            
                                                    </tr>-->                      
                        </table> 
                        <table aling="center" style=" width: 100%">
                            <tr>
                                <td style="width:50%" colspan="3">
                                    <fieldset style="height:90px">
                                        <legend>Cargar Contrato</legend>    
                                        <table aling="center" style=" width: 100%" >             
                                            <tr>
                                                <td style="font-size: 12px;width:110px;padding: 5px">
                                                    Seleccione archivo
                                                </td>
                                                <td>
                                                    <input type="file" id="examinar" name="examinar"  style="font-size: 11px;width:250px" multiple>                                                
                                                    <button id="subirArchivo" class="ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only" 
                                                            role="button" aria-disabled="false" >
                                                        <span class="ui-button-text">Subir</span>
                                                    </button> 
                                                </td>                                               
                                            </tr>
                                            <tr>
                                                <td style="font-size: 12px;width:110px;">
                                                    Tipo Contrato
                                                </td>                                              
                                                <td>
                                                    <input type="radio" name="tipo_contrato" value="2">P�blico
                                                    <input type="radio" name="tipo_contrato" value="3" checked> Privado
                                                </td>         
                                            </tr>
                                        </table>
                                    </fieldset>
                                </td> 
                                <td style="width:50%" colspan="3">
                                    <fieldset style="height:90px">
                                        <legend>Archivos Cargados</legend>    
                                        <div style="height:80px;overflow:auto;">
                                            <table id="tbl_archivos_cargados" aling="center" style=" width: 100%" >             

                                            </table>
                                        </div>
                                    </fieldset>
                                </td>  
                            </tr>
                        </table>  
                        <table id="tbl_info_tributaria" aling="center" style=" width: 100%" >   
                            <tr>
                                <td colspan="6">
                                    <h4>INFORMACION TRIBUTARIA</h4>
                                    <hr>
                                </td>
                            </tr> 
                            <tr>
                                <td style="width:180px">
                                    <label> Clasificacion <span style="color: red; ">*</span></label> 
                                </td>
                                <td>
                                    <select type="text" class="seleccionado" name="clasificacion" id="clasificacion" style="width: 180px">                                                     
                                    </select>
                                </td>   
                                <td style="width:180px">
                                    <label> Autoretenedor </label> 
                                </td>
                                <td>
                                    <select type="text" class="seleccionado" name="agente_retenedor" id="agente_retenedor">
                                        <option value="N"> No</option>
                                        <option value="S"> Si</option>
                                    </select>
                                </td>    
                                <td style="width:180px">
                                    <label> Aplica ReteFuente </label> 
                                </td>
                                <td>
                                    <select type="text" class="seleccionado" name="autoretenedor_rfte" id="autoretenedor_rfte">
                                        <option value="N"> No</option>
                                        <option value="S"> Si</option>
                                    </select>
                                </td>    
                            </tr>
                            <tr>
                                <td>
                                    <label> Regimen <span style="color: red; ">*</span></label> 
                                </td>
                                <td>
                                    <select type="text" class="seleccionado" name="regimen" id="regimen" style="width: 180px">
                                        <option value="">...</option>
                                        <option value="C">Comun</option>
                                        <option value="S">Simplificado</option>
                                    </select>
                                </td>   
                                <td>
                                    <label> Aplica ICA </label> 
                                </td>
                                <td>
                                    <select type="text" class="seleccionado" name="autoretenedor_ica" id="autoretenedor_ica">
                                        <option value="N"> No</option>
                                        <option value="S"> Si</option>
                                    </select>
                                </td>    
                            </tr>   
                            <tr>
                                <td>
                                    <label> Gran Contribuyente </label> 
                                </td>
                                <td>
                                    <select type="text" class="seleccionado" name="gran_contribuyente" id="gran_contribuyente">
                                        <option value="N"> No</option>
                                        <option value="S"> Si</option>
                                    </select>
                                </td>   
                                <td>
                                    <label> Aplica IVA </label> 
                                </td>
                                <td>
                                    <select type="text" class="seleccionado" name="autoretenedor_iva" id="autoretenedor_iva">
                                        <option value="N"> No</option>
                                        <option value="S"> Si</option>
                                    </select>
                                </td>    
                            </tr>   
                            <tr>
                                <td colspan="6">
                                    <h4>VALOR DEL CONTRATO</h4> 
                                    <hr>
                                </td>                                  
                            </tr> 
                        </table>
                        <fieldset>
                            <legend>CONDICIONES COMERCIALES</legend>    
                            <table id="tbl_cond_comerciales" aling="center" style=" width: 100%" >     
                                <tr>
                                    <td>
                                        <label> Valor antes del IVA<span style="float:right;">$</span></label> 
                                    </td>
                                    <td colspan="3">
                                        <input type="text" name="vlr_antes_iva" id="vlr_antes_iva" class="solo-numero"  value="0" readonly/>
                                    </td>

                                </tr>                        
                                <tr class="valores_aiu" style="display:<%=aiu.equals("N") ? "none" : ""%>">
                                    <td>
                                        <label> % Administracion<span id="req_porc_admon" style="color:red;display:none;">*</span></label> 
                                    </td>
                                    <td>
                                        <input type="text" class="seleccionado" name="porc_admon" id="porc_admon" class="solo-numeric" value="0" style="width:30px" readonly/><span> &nbsp;$</span> 
                                        <input type="text" class="seleccionado" name="vlr_admon" id="vlr_admon" class="solo-numeric" value="0" readonly style="width:105px" readonly/>
                                        <img  src='/fintra/images/boton_ant.png' id='administracion_ant'  name="administracion_ant" style="width: 23px;height: 22px;margin-bottom: -6px;" class="anticipo" title="Anticipo" onclick="calcularClicImagen(this.id)"/>
                                        <img  src='/fintra/images/boton_rete.png' id='administracion_ret' name='administracion_ret' style="width: 23px;height: 22px;margin-bottom: -6px;" class="retegarantia" title="Retegarantia" onclick="calcularClicImagen(this.id)"/>
                                    </td>                        
                                    <td>
                                        <label> % Imprevisto<span id="req_porc_imprevisto" style="color:red;display:none;">*</span></label> 
                                    </td>
                                    <td>
                                        <input type="text" class="seleccionado" name="porc_imprevisto" id="porc_imprevisto" class="solo-numeric" value="0" style="width:30px" readonly/><span> &nbsp;$</span> 
                                        <input type="text" class="seleccionado" name="vlr_imprevisto" id="vlr_imprevisto" class="solo-numeric" value="0" readonly style="width:105px" readonly/>
                                        <img  src='/fintra/images/boton_ant.png' id='imprevisto_ant' name="imprevisto_ant" style="width: 23px;height: 22px;margin-bottom: -6px;" class="anticipo" title="Anticipo" onclick="calcularClicImagen(this.id)"/>
                                        <img  src='/fintra/images/boton_rete.png' id='imprevisto_rete' name="imprevisto_rete" style="width: 23px;height: 22px;margin-bottom: -6px;" class="retegarantia" title="Retegarantia" onclick="calcularClicImagen(this.id)"/>
                                    </td>
                                    <td>
                                        <label> % Utilidad<span id="req_porc_utilidad" style="color:red;display:none;">*</span></label> 
                                    </td>
                                    <td>
                                        <input type="text" class="seleccionado" name="porc_utilidad" id="porc_utilidad" class="solo-numeric" value="0" style="width:30px" readonly/><span> &nbsp;$</span>
                                        <input type="text" class="seleccionado" name="vlr_utilidad" id="vlr_utilidad" class="solo-numeric" value="0" readonly style="width:105px" readonly/>
                                        <img  src='/fintra/images/boton_ant.png' id='utilidad_ant' name ="utilidad_ant" style="width: 23px;height: 22px;margin-bottom: -6px;" class="anticipo" title="Anticipo" onclick="calcularClicImagen(this.id)"/>
                                        <img  src='/fintra/images/boton_rete.png' id='utilidad_rete' name ="utilidad_rete" style="width: 23px;height: 22px;margin-bottom: -6px;" class="retegarantia" title="Retegarantia" onclick="calcularClicImagen(this.id)"/>
                                    </td>                          
                                </tr>
                                <tr class="valores_aiu" style="display:<%=aiu.equals("N") ? "none" : ""%>">
                                    <td>
                                        <label> % AIU <span id="req_porc_aiu" style="color:red;">*</span></label> 
                                    </td>
                                    <td>
                                        <input type="text" class="seleccionado" name="porc_aiu" id="porc_aiu" class="solo-numeric" value="0" readonly/>
                                    </td>
                                    <td>
                                        <label> Valor de AIU<span style="float:right;">$</span></label> 
                                    </td>
                                    <td>
                                        <input type="text" class="seleccionado" name="valor_aiu"  id="valor_aiu" class="solo-numeric" value="0" readonly/>
                                    </td>  
                                    <td>
                                        <label> Valor Total<span style="float:right;">$</span></label> 
                                    </td>
                                    <td>
                                        <input type="text" class="seleccionado" name="valor_total_aiu" id="valor_total_aiu" class="solo-numeric" value="0" readonly/>
                                    </td>   
                                </tr>
                                <tr class="valores_iva" style="display:<%=aiu.equals("N") ? "" : "none"%>">                           
                                    <td>
                                        <label> % IVA<span id="req_porc_iva" style="color:red;display:none;">*</span></label> 
                                    </td>
                                    <td>
                                        <input type="text" class="seleccionado" name="porc_iva" id="porc_iva" class="solo-numeric" value="0" readonly/>
                                    </td>      
                                    <td>
                                        <label>Valor de IVA<span style="float:right;">$</span></label> 
                                    </td>
                                    <td>
                                        <input type="text" class="seleccionado" name="valor_iva"  id="valor_iva" class="solo-numeric" value="0" readonly/>
                                        <img  src='/fintra/images/boton_ant.png' id='antes_iva_ant'  name="antes_iva_ant" style="width: 23px;height: 22px;margin-bottom: -6px;" class="anticipo" title="Anticipo" onclick="calcularClicImagen(this.id)"/>
                                        <img  src='/fintra/images/boton_rete.png' id='antes_iva_ret' name="antes_iva_ret" style="width: 23px;height: 22px;margin-bottom: -6px;" class="retegarantia" title="Retegarantia" onclick="calcularClicImagen(this.id)"/>
                                    </td>   
                                    <td>
                                        <label> Valor Total<span style="float:right;">$</span></label> 
                                    </td>
                                    <td>
                                        <input type="text" class="seleccionado" name="valor_total_iva" id="valor_total_iva" class="solo-numeric" value="0" readonly/>
                                    </td>     
                                </tr>
                            </table>
                        </fieldset>                    
                        <table aling="center" style=" width: 100%"> 
                            <tr>                          
                                <td colspan="6">
                                    <h4>FORMAS DE PAGO</h4>
                                    <hr>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <label>Base Calculo:</label>
                                </td>
                                <td>
                                    <input type="text" class="seleccionado" name="base_calculo_an" id="base_calculo_an" class="solo-numeric" value="0" readonly style="width:70px"/> 
                                </td>
                                <td>
                                    <label>Anticipo:</label>
                                </td>
                                <td>
                                    <select id="anticipo" class="seleccionado" nombre="anticipo" style="width: 60px" onchange="habdesAnt(true);" >
                                        <option value='1'>Si</option>
                                        <option value='2' selected>No</option>
                                    </select>
                                </td>
                                <td style="width:30%">
                                    <input type="text" class="seleccionado" name="porc_anticipo" id="porc_anticipo" class="solo-numeric" value="0" readonly style="width:70px"/><span>%</span> 
                                    <span style="margin-left:10px;">$</span><input type="text" class="seleccionado"  name="valor_anticipo" id="valor_anticipo" class="solo-numero" readonly/>
                                </td> 
                                <td>
                                    <label>Base Calculo:</label>
                                </td>
                                <td>
                                    <input type="text" class="seleccionado" name="base_calculo_ret" id="base_calculo_ret" class="solo-numeric" value="0" readonly style="width:70px"/> 
                                </td>
                                <td>
                                    <label> Retegarant�a</label> 
                                </td>
                                <td>
                                    <select type="text" class="seleccionado" name="retegarantia" id="retegarantia" style="width: 60px" onchange="habdesRet(true)">
                                        <option value='1'>Si</option>
                                        <option value='2' selected>No</option>
                                    </select>
                                </td>    
                                <td>
                                    <input type="text" class="seleccionado" name="porc_retegarantia" id="porc_retegarantia" class="solo-numeric" value="0" readonly style="width:70px"/><span>%</span> 
                                    <span style="margin-left:10px;">$</span><input type="text" class="seleccionado" name="valor_retegarantia" id="valor_retegarantia" class="solo-numero" readonly/>
                                </td> 
                            </tr>     
                        </table>
                        <table aling="center" style=" width: 100%">
                            <tr>                          
                                <td colspan="6">
                                    <h4>GARANTIAS</h4>
                                    <hr>
                                </td>
                            </tr>
                            <tr>
                                <td style="width:50%;display:none" colspan="3">
                                    <fieldset style="height:275px">
                                        <legend>Facturas Parciales</legend>    
                                        <table aling="center" style=" width: 100%" >             
                                            <tr>
                                                <td>
                                                    <table id="tabla_facturacion" ></table>
                                                    <div id="page_tabla_facturacion"></div>
                                                </td>
                                            </tr>
                                        </table>
                                    </fieldset>
                                </td> 
                                <td style="width:100%" colspan="3">
                                    <!--                                    <fieldset style="height:auto">
                                                                            <legend>Garant�as</legend>   -->
                                    <table aling="center" style=" width: 91%;margin-left: 59px;" >             
                                        <tr>
                                            <td style="text-align:center;">
                                                <table id="tabla_garantias" ></table>
                                                <div id="page_tabla_garantias"></div>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td style="text-align:center;">
                                                <table id="tabla_otro_si" ></table>
                                                <div id="page_tabla_otro_si"></div>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td style="text-align:center;">
                                                <table id="tabla_garantias_extra" ></table>
                                                <div id="page_tabla_garantias_extra"></div>
                                            </td>
                                        </tr>
                                    </table>
                                    <!--                                    </fieldset>-->
                                </td>
                            </tr>  
                            <!--                        <tr>
                                                            <td style="width:20%" colspan="1">   
                                                                <label> Fecha Expedici�n</label> 
                                                            </td>
                                                            <td>
                                                                <input type="text" id="fecha_expedicion" readonly/>                                                                     
                                                            </td>          
                                                        </tr>-->
                        </table>
                        <br>
                        <div id="botones_footer" style="text-align:center"> 
                            <span aling="center" id="btn_guardar"  class="form-submit-button form-submit-button-simple_green_apple"> Guardar </span>&nbsp;
                            <span aling="center" id="btn_salir" class="form-submit-button form-submit-button-simple_green_apple"> Salir </span>
                        </div>
                        <br>
                    </div>
                </form>
                <div id="esp_blanco" style="height: 100px"></div>
            </center>
            <div id="direccion_dialogo" style="display:none;left: 0; position: absolute; top: 0;padding: 1em 1.2em; margin:0px auto; margin-top:10px; padding:10px; width:370px; min-height:140px; border-radius:4px; background-color:#FFFFFF; box-shadow: 0 2px 5px #666666;"> 
                <div>
                    <table style="width: 100%;">

                        <tr>
                            <td class="titulo_ventana" id="drag_direcciones" colspan="3">
                                <div style="float:center">FORMATO DIRECCIONES<input type="hidden" name="id_usuario" id="id_usuario" value="coco"/></div> 
                            </td>
                        </tr>                    
                        <tr>  
                            <td><span>Departamento:</span></td>                     
                            <td colspan="2"> <select id="dep_dir" name="dep_dir" style="width:20em;"></select>
                            </td>
                        </tr>
                        <tr>
                            <td><span>Ciudad:</span></td>     
                            <td colspan="2">                            
                                <div id="d_ciu_dir">
                                    <select id="ciu_dir" name="ciu_dir" style="width:20em;"></select>
                                </div>
                            </td>
                        </tr>  
                        <tr>
                            <td>Via Principal</td>
                            <td>
                                <select id="via_princip_dir" onchange="setDireccion(2)">                                
                                </select>
                            </td>
                            <td><input type="text" id="nom_princip_dir" style="width: 100%;" onchange="setDireccion(1)"/></td>
                        </tr>
                        <tr>
                            <!-- <td>Via Generadora</td> -->
                            <td>Numero</td>
                            <td>
                                <select id="via_genera_dir" onchange="setDireccion(1)">                               
                                </select>
                            </td>

                            <td>
                                <table width="100%" border="0">
                                    <tr>
                                        <td align="center" width="49%">
                                            <input type="text" id="nom_genera_dir" style="width: 90%;" onkeypress="setDireccion(1)" onchange="setDireccion(1)"/>
                                        </td>
                                        <td width="2%" align="center"> - </td>
                                        <td align="center" width="49%">
                                            <input type="text" id="placa_dir" style="width: 100%;" onkeypress="setDireccion(1)" onchange="setDireccion(1)"/>
                                        </td>    
                                    </tr>
                                </table>                        
                            </td>
                        </tr>
                        <tr>
                            <td>Complemento</td>
                            <td colspan="2"><input type="text" id="cmpl_dir" style="width: 100%;" onkeypress="setDireccion(1)" onchange="setDireccion(1)"/></td>
                        </tr>
                        <tr>
                            <td colspan="3"><input type="text" id="dir_resul" name="" style="width: 100%;" readonly/></td>
                        </tr>
                        <tr>
                            <td colspan="3">
                                <button onclick="setDireccion(3);">Aceptar</button>
                                <button onclick="setDireccion(0);">Salir</button>
                            </td>
                        </tr>
                    </table>
                </div>
            </div>
            <div id="dialogLoading" style="display:none;">
                <p  style="font-size: 12px;text-align:justify;" id="msj2">Texto </p> <br/>
                <center>
                    <img src="/fintra/images/cargandoCM.gif"/>
                </center>
            </div>
            <div id="dialogMsj" title="Mensaje" style="display:none;">
                <p style="font-size: 12px;text-align:justify;" id="msj" > Texto </p>
            </div>
            <div class="contextMenu" id="myMenu" style="display:none;">
                <ul  id="lstOpciones" >
                    <li id="eliminar">
                        <span style="float:left"></span>
                        <span style="font-size:11px; font-family:Verdana">Eliminar fila</span>
                    </li> 
                </ul>
            </div>
            <div id="div_detalle_otro_si"  style="width: 550px" >
                <center>
                    <br>
                    <table id="tabla_garantias_otro_si" ></table>
                    <div id="page_tabla_garantias_otro_si"></div>
                </center>
            </div>
        </div>  
        <script type='text/javascript'>
            initContrato();
            resizeTo(screen.width, screen.height);
            moveTo(0, 0);
        </script>


    </body>
</html>
