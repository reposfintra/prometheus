<%-- 
    Document   : solicitudesPendientesPorContrato
    Created on : 19/04/2016, 02:46:21 PM
    Author     : user
--%>

<%@page contentType="text/html" pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>

    <head>

        <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
        <!--<meta name="viewport" content="width=device-width, initial-scale=1">-->


        <title>Administración De Garantias  </title>



        <link href="./css/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css"/>
        <link href="./css/simple-sidebar.css" rel="stylesheet" type="text/css"/>
        <link href="./css/OportunidadNegocio.css" rel="stylesheet" type="text/css"/>




        <link type="text/css" rel="stylesheet" href="./css/jquery/jquery-ui/jquery-ui.css"/>     
        <script type="text/javascript" src="./js/jquery/jquery-ui/jquery.min.js"></script>
        <script type="text/javascript" src="./js/jquery/jquery-ui/jquery.ui.min.js"></script> 
        <script src="./js/jquery/editor/ckeditor/ckeditor.js" type="text/javascript"></script>



        <link type="text/css" rel="stylesheet" href="./css/buttons/botonVerde.css"/>
        <script type="text/javascript" src="./js/jquery/jquery.jqGrid/js/grid.locale-es.js"></script>
        <script type="text/javascript" src="./js/jquery-ui-1.8.5.custom.min.js"></script>   
        <script type="text/javascript" src="./js/jquery/jquery.jqGrid-4.7.0/js/grid.locale-es.js"></script>
        <script type="text/javascript" src="./js/jquery/jquery.jqGrid-4.7.0/js/jquery.jqGrid.min.js"></script>
        <link type="text/css" rel="stylesheet" href="./css/jquery/jquery.jqGrid/ui.jqgrid.css"/>  
        <script type="text/javascript" src="./js/jquery/jquery.jqGrid-4.7.0/js/jquery.tabletojson.js"></script>    
        <script type="text/javascript" src="./js/jquery/jquery.jqGrid-4.7.0/plugins/jquery.contextmenu.js"></script>
        <script type="text/javascript" src="./js/utilidadInformacion.js"></script>
        <script type="text/javascript" src="./js/contratos.js"></script> 

        <!-------------------------------------Notificaciones----------------------------------------->
        <link type="text/css" rel="stylesheet" href="./css/toastr.min.css"/>
        <script type="text/javascript" src="./js/toastr.min.js"></script>
        <!-------------------------------------/Notificaciones----------------------------------------->



    </head> 
    <body>
    <center>
        <jsp:include page="/WEB-INF/toptsp.jsp?encabezado=Administración De Garantias"/>

        <input type="hidden" id="id_cliente" name="id_cliente">
        <!--css logica de negocio-->
        <link href="./css/contratos.css" rel="stylesheet" type="text/css"> 
        <div class="ui-jqgrid ui-widget ui-widget-content center-block" style="width: 1355px; margin-top: 10px;height: 121px;border-radius: 10px;border: 1px solid #2A88C8;">
            <!--
                        <div class="ui-dialog-titlebar ui-widget-header ui-helper-clearfix">
                            <span class="ui-dialog-title text-center" id="ui-dialog-title-listaGlobal">
                                &nbsp;
                                <label><b>Busqueda - Solicitudes Pendientes Por Contrato</b></label>
                            </span>
                        </div>-->

            <div class="row" style="padding: 5px;width: 1375px;">

                <div class="col-md-12 ">

                    <fieldset class="scheduler-border">
                        <legend class="scheduler-border">Busqueda De Solicitudes</legend>
                        <div id="div_solicitudes_pendientes"> 
                            <div class="row">
                                <div class="col-md-12">
                                    <table>
                                        <tr>
                                            <td>
                                                <label>Tipo De Proyecto</label>
                                            </td>
                                            <td>
                                                <select id="tipo_proyecto" style="width: 151px;color: #050505;height: 25px;margin-left: 5px;" >
                                                    <option value=''></option>                                        
                                                    <option value='0'>Proyectos Transicion</option>
                                                    <option value='1'>Nuevos Proyectos</option>

                                                </select>
                                            </td>
                                            <td>
                                                <label>Linea Negocio</label>
                                            </td>
                                            <td>
                                                <select id="linea_negocio" style="width: 151px;color: #050505;height: 25px;margin-left: 5px;" ></select>
                                            </td>
                                            <td>
                                                <label style="margin-left: 10px;">Solicitud</label>
                                            </td>
                                            <td>
                                                <input type="text" id="solicitud"  style="width: 80px;margin-left: 5px;height: 26px;" >
                                            </td>
                                            <td>
                                                <label style="margin-left: 10px;">Foms</label>
                                            </td>
                                            <td>
                                                <input type="text" id="txt_foms"  style="width: 80px;margin-left: 5px;height: 26px;" >
                                            </td>
                                            <td>
                                                <label style="margin-left: 10px;">Nombre Cliente</label>
                                            </td>
                                            <td>
                                                <input type="text" id="txt_nom_cliente"  style="width: 80px;margin-left: 5px;height: 26px;" >
                                            </td>
                                            <td>
                                                <label style="margin-left: 10px;">Nombre Proyecto</label>
                                            </td>
                                            <td>
                                                <input type="text" id="txt_nom_proyecto"  style="width: 80px;margin-left: 5px;height: 26px;" >
                                            </td>
                                            <td>
                                                <label style="margin-left: 10px;"hidden="true">Responsable</label>
                                            </td>
                                            <td>
                                                <input type="text" id="responsable"  hidden="true" style="width: 139px;margin-left: 5px;height: 26px;" >

                                            </td>

                                            <td>
                                                <label style="margin-left: 10px;" hidden="true"  >Estado cartera</label>
                                            </td>
                                            <td>
                                                <select id="estadocartera" hidden="true" style="width: 108px;color: #050505;height: 25px;margin-left: 5px;" ></select>
                                            </td> 
                                            <td>
                                                <label style="margin-left: 10px;" hidden="true">Fecha validacion cartera</label>
                                            </td>
                                            <td>
                                                <input type="datetime" id="fechaini" hidden="true" style="width: 100px ;height: 26px;"  readonly>
                                            </td>
                                            <td>
                                                <input  type="datetime" id="fechafin" hidden="true" style="width: 100px;height: 26px;" readonly>
                                            </td>
                                            <td>
                                                <input type="text" id="etapaActual"  style="width: 80px;margin-left: 5px;height: 26px;" value="GARANTIAS" hidden >
                                            </td>
                                            <td>
                                                <div style="margin-left: 15px;">
                                                    <button id="listarSolicitudes" class="ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only" 
                                                            role="button" aria-disabled="false">
                                                        <span class="ui-button-text">Buscar</span>
                                                    </button>&nbsp;&nbsp;
                                                </div> 
                                            </td>
                                            <td>
                                                <div>
                                                    <button id="clearSolicitudes" class="ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only" 
                                                            role="button" aria-disabled="false">
                                                        <span class="ui-button-text">Limpiar</span>
                                                    </button>
                                                </div> 
                                            </td>
                                        </tr>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </fieldset>
                </div> 
                <!--                <div class="col-md-12 ">
                                    <div>
                                        <button id="listarSolicitudes" class="ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only" 
                                                role="button" aria-disabled="false">
                                            <span class="ui-button-text">Buscar</span>
                                        </button>&nbsp;&nbsp;
                                        <button id="clearSolicitudes" class="ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only" 
                                                role="button" aria-disabled="false">
                                            <span class="ui-button-text">Limpiar</span>
                                        </button>
                                    </div>     
                                </div>-->
            </div> 

        </div> 
        <center>



            <br><br>
            <table id="tabla_solicitudes_pendientes" align="center" ></table>  
            <div id="page_tabla_solicitudes_pendientes"></div>
            <!--                <div id="div_solicitudes_pendientes" style="display: none;">  
                                <table border="0"  align="center">
                                    <tr>
                                        <td>
                                            <table id="tabla_solicitudes_pendientes" align="center" ></table>  
                                            <div id="page_tabla_solicitudes_pendientes"></div>
                                        </td>
                                    </tr>
                                </table>
                            </div> -->   


        </center>
        <div style="display: flex;justify-content:center" >
            <div id="divObservaciones" style="text-align: left;display:none;width: 1450px;">
                <h4> <span> <img id="square_red" src="/fintra/images/cuadroRojo.png" style="margin-left: 5px; height: 16px; width:16px; vertical-align: middle;">
                    </span> Proyectos pendientes por diligenciar parte contractual. </h4>                
            </div>
        </div>

        
        <!-- **********************************************************************************************************************************************************
                                                                                Formulario Bodega
            *********************************************************************************************************************************************************** -->



            <div id="div_tabla_bodegas" style="display: none">

                <div class="container-fluid">
                    <table id="tabla_bodegas"></table>
                    <div id="pager_tabla_bodegas"></div>
                </div>

                <div class="container-fluid">

                    <form name="bodega" method="POST" class="form-horizontal">

                        <input type="hidden" id="id_solicitud" name="id_solicitud">
                        <input type="hidden" id="id_bodega" name="id_bodega">


                        <h4>Formulario de Bodegas</h4> 
                        <hr>
                        <div class="form-group">
                            <label class="col-md-3 control-label">Departamento:</label>
                            <div class="col-md-9">
                                <select id="dep_dir" name="dep_dir" class="form-control input-sm"></select>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-md-3 control-label">Ciudad :</label>
                            <div class="col-md-9">
                                <select id="ciu_dir" name="ciu_dir" class="form-control input-sm"></select>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-md-3 control-label">Via Principal :</label>
                            <div class="col-md-5">
                                <select id="via_princip_dir" onchange="setDireccion(2)" class="form-control input-sm"></select>
                            </div>
                            <div class="col-md-4">
                                <input type="text" id="nom_princip_dir" class="form-control input-sm" style="width: 100%;" onkeyup="setDireccion(1)"/>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-md-3 control-label">Numero :</label>
                            <div class="col-md-5">
                                <select id="via_genera_dir" class="form-control input-sm" onkeyup="setDireccion(1)"  ></select>
                            </div>


                            <div class="col-md-2">
                                <input type="text" id="nom_genera_dir" class="form-control input-sm"style="width: 100%;" onkeyup="setDireccion(1)"/>
                            </div>

                            <div class="col-md-2">
                                <input type="text" id="placa_dir" class="form-control input-sm" style="width: 100%;" onkeyup="setDireccion(1)"/>
                            </div>


                        </div>

                        <div class="form-group">
                            <label class="col-md-3 control-label">Complemento :</label>
                            <div class="col-md-8">
                                <input type="text" id="cmpl_dir" class="form-control input-sm" onkeyup="setDireccion(1)" onchange="setDireccion(1)"/>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-md-3 control-label">Direccion :</label>
                            <div class="col-md-9">
                                <input type="text" id="dir_resul" class="form-control input-sm" style="width: 100%;" readOnly/>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-md-3 control-label">Descripcion :</label>
                            <div class="col-md-9">
                                <textarea rows ="3" name="descripcion" id="desc_bodega" class="form-control input-sm" placeholder="Descripcion de bodega." maxlength="100"></textarea>
                            </div>
                        </div>
                        
                        <div class="form-group">
                            <label class="col-md-3 control-label">Nombre de contacto: </label>
                            <div class="col-md-9">
                                <input type="text" id="nombre_contacto" class="form-control input-sm" style="width: 100%;"/>
                            </div>
                        </div>       

                        <div class="form-group">
                            <label class="col-md-3 control-label">Cargo de contacto: </label>
                            <div class="col-md-9">
                                <input type="text" id="cargo_contacto" class="form-control input-sm" style="width: 100%;"/>
                            </div>
                        </div>                
                        
                        <div class="form-group">
                            <label class="col-md-3 control-label">Telefono 1: </label>
                            <div class="col-md-9">
                                <input type="text" id="telefono1_contacto" class="form-control input-sm" style="width: 100%;"/>
                            </div>
                        </div>                
                        
                        <div class="form-group">
                            <label class="col-md-3 control-label">Telefono 2: </label>
                            <div class="col-md-9">
                                <input type="text" id="telefono2_contacto" class="form-control input-sm" style="width: 100%;"/>
                            </div>
                        </div>

                    </form>
                </div>

            </div>




            <!-- **********************************************************************************************************************************************************
                                                                                FIN Formulario Bodega
            *********************************************************************************************************************************************************** -->

        <!-- **********************************************************************************************************************************************************
                                                                Ventana cambio de etapa y estado
        *********************************************************************************************************************************************************** -->

        <div id="cambioEtapa" style="display: none">
            <div class="row" style="padding: 5px">
                <div class="col-md-2 ">
                    <fieldset class="scheduler-border" style="width: 720px">

                        <div class="row">
                            <div class="col-md-2">
                                Idsolicitud :
                            </div>
                            <div class="col-md-4">
                                <input type="text" id="idsolicitud" readonly>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-2">
                                Etapa Actual:
                            </div>
                            <div class="col-md-4">
                                <input id="etapa" type="Text" readonly="true" style="width: 80%">
                                <input id="idetapa" hidden="true" type="Text" style="width: 80%">
                            </div>
                            <div class="col-md-2">
                                Estado Actual:
                            </div>
                            <div class="col-md-4">
                                <input id="estadoActual" readonly="true" type="Text" style="width: 80%">
                                <input id="idestadoActual" readonly="true" type="Text" style="width: 80%" hidden>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-2">
                                Estado :
                            </div>
                            <div class="col-md-4">
                                <select id="estado" class="selectpicker" style="width: 80%"></select>
                            </div>
                        </div>
                        <div class="row" hidden="true">
                            <div class="col-md-2">
                                Causal :
                            </div>
                            <div class="col-md-10">
                                <textarea id="causal" class="form-control" rows="5"></textarea>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-2">
                                Trazabilidad :
                            </div>
                            <div class="col-md-10">
                                <textarea id="trazabilidad" class="form-control" readonly="true" rows="5"></textarea>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-2">
                                Descripcion :
                            </div>
                            <div class="col-md-10">
                                <textarea id="descripcion_causal" class="form-control" rows="5"></textarea>
                            </div>
                        </div>


                    </fieldset>
                </div>
            </div>
        </div>
        <div id="div_docs_contrato" style="display:none;">
            <form>
                <input type="text" name="numero_contrato" id="numero_contrato" style="width: 50px" hidden >
                <input type="text" name="tipo_doc" id="tipo_doc" style="width: 50px" hidden >
                <textarea cols="80" id="editor1" name="editor1' " rows="20" ></textarea>
            </form>
        </div>   
        <div id="dialogLoading" style="display:none;">
            <p  style="font-size: 12px;text-align:justify;" id="msj2">Texto </p> <br/>
            <center>
                <img src="/fintra/images/cargandoCM.gif"/>
            </center>
        </div>
        <div id="dialogMsj" title="Mensaje" style="display:none;">
            <p style="font-size: 12px;text-align:justify;" id="msj" > Texto </p>
        </div> 
    </center>

    <!-- **********************************************************************************************************************************************************
                                                           Ventana Anticipo
   *********************************************************************************************************************************************************** -->

    <!--    <div id="ventana_Anticipo_Facturacion" style="display: none">
            <div class="row"style="padding: 5px">
                <div class="col-md-2 ">
    
                    <div class="row" style="width: 420px;padding-top: 10px">
                        <div class="col-md-4">
                            <label>Se dirije a : </label>
                        </div>
                        <div class="col-md-8">
                            <select id="select_AYF" class="selectpicker" style="width: 100%">
                                                            <option value='1'>Cuadro De Control</option>
                                <option value='2'>Facturacion</option>
                                <option value='3'>Anticipo</option>
                            </select>
                        </div>
                    </div>
    
                </div>
            </div>
        </div>-->
    <div id="dialogAnticipo"  class="ventana" style="position: relative;margin-top: -7;"hidden="true">
        <div id="tablita" >
            <table>
                <tr>
                    <td>
                        <label hidden>Anticipo %: </label>
                    </td>
                    <td>
                        <input id="perc_anticipo_" disabled onkeyup="calculateFooter();" maxlength="2" onchange="" type="text" placeholder="" style="width: 110px;" hidden >
                    </td>
                    <td>
                        <label style="margin-left: 14px;">Valor Anticipo:</label>
                    </td>
                    <td>
                        <input id="val_anticipo_" type="text"  placeholder="" style="width: 110px;" onkeyup="funciones.numberConComasKeyup(this)" >
                    </td> 
                    <td>
                        <label style="margin-left: 14px;">Caso Anticipo:</label>
                    </td>
                    <td>
                        <select id="caso_anticipo_" style="width: 151px;color: #050505;height: 25px;margin-left: 5px;" onchange="valorAticipo()" ></select>
                    </td>
                    <td>
                        <input id="codcli_" type="text" disabled="true" style="width: 110px;" hidden >
                    </td>
                    <td>
                        <input id="nomcli_" type="text" disabled="true"  style="width: 110px;" hidden >
                    </td>
                    <td>
                        <input id="nombre_proyecto_" type="text" disabled="true" style="width: 329px;" hidden >
                    </td>
                    <td>
                        <input id="codcot_" type="text" disabled="true" style="width: 110px;" hidden >
                    </td>
                </tr>
            </table>
        </div>
        <div style="position: relative;top: 21px;">
            <table id="tabla_anticipo" style="top: 37px;"></table>
            <div id="pager3"></div>
        </div>
    </div>

    <div id="dialogMsjFacturacion" style="position: relative;margin-top: -7;"hidden="true" >
        <div id="tablita" class="ui-jqgrid ui-widget ui-widget-content center-block" style="width: 763px; margin-top: 10px" >
            <div class="ui-dialog-titlebar ui-widget-header ui-helper-clearfix">
                <span class="ui-dialog-title text-center" id="ui-dialog-title-listaGlobal">
                    <label class="titulotablita" id="lb_facturacion" style="height: 28px;"><b></b></label>
                </span>
            </div>
            <table id="tablainterna" style="height: 53px; width: 305px">
                <tr>
                    <td>
                        <input id="id_solicitudSelec" type="text" disabled="true" placeholder="" style="width: 110px" hidden>
                    </td>
                    <td>
                        <label style="margin-left: 14px;">Precio venta:</label>  
                    </td>
                    <td>
                        <input id="val_total" type="text" disabled="true" placeholder="" style="width: 110px" >
                    </td>
                    <td>
                        <div>
                            <button id="prepararAccion" class="ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only" 
                                    role="button" aria-disabled="false" style="margin-left: 10px;">
                                <span class="ui-button-text">Preparar</span>
                            </button>
                        </div> 
                    </td>
                </tr>
            </table>
        </div>
        <center>
            <div style="position: relative;top: 15px;" >
                <table id="tabla_facturacion" style="top: 37px;"></table>
                <div id="pager2"></div>
            </div>
            <div style="position: relative;top: 30px;" >
                <table id="tabla_accionPreparada" style="top: 37px;"></table>
                <div id="pager4"></div>
            </div>
        </center>
        <div id="info"  class="ventana" hidden="true">
            <p id="notific">EXITO AL GUARDAR</p>
        </div>
        <div id="menu2" class="ContextMenu"style="" hidden="true">
            <ul id="listopciones">
                <li id="eliminar" >
                    <span style="font-family:Verdan">Eliminar fila</span>
                </li>
            </ul>

        </div>
    </div>
    <div id="dialogEmpresa"  class="ventana" style="position: relative;margin-top: -7;"hidden="true">
        <div id="tablainterna" style="width: 492px;" >
            <table id="tablainterna"  >
                <tr>
                    <td>
                        <label>Empresa</label>
                        <select id="empresa">
                            <option value="fintra">Fintra</option>
                            <option value="selectrik">Selectrik</option>
                        </select>
                        <label>Firma</label>
                        <select id="firma">
                            <option value="jgomez">Jose Luis Gomez</option>
                            <option value="cramos">Carlos Ramos Fontalvo</option>
                        </select>
                        <label>Cuenta</label>
                        <select id="cuenta">
                            <option value="cuentaFintra">Fintra</option>
                            <option value="cuentaSelectrik">Selectrik</option>
                        </select>
                    </td>
                </tr>
            </table>
        </div>
    </div>
    <div id="dialogMsjPrepararFacturacion" style="position: relative;"hidden="true" >
        <table id="tablainterna" style="height: 53px; width: 595px">
            <tr>
                <td>
                    <input id="_id_solicitud" style="width: 148px;height: 26px" readonly hidden>
                </td>
            </tr>
            <tr>
                <td>
                    <label style="margin-top: 9px;">Valor por Facturar:</label>
                    <input id="porFacturar" style="width: 148px;height: 26px" readonly>
                </td>
            </tr>
            <tr>
                <td>
                    <label>Valor a Facturar:</label>
                    <input id="aFacturar" style="width: 148px;height: 26px;margin-left: 12px;" onkeyup="calcularValorxFatura();
                            funcionFormatos.numberConComasKeyup(this)">
                </td>
                <td>
                    <label>Valor Material:</label>
                    <input id="valorMaterial" style="width: 148px;height: 26px;margin-left: 12px;" onkeyup="funcionFormatos.numberConComasKeyup(this);
                            ValidarValorMaterial()">
                </td>
            </tr>
            <tr>
                <td colspan="4">
                    <hr style="width: 590px;height: 3px;top: -25px;margin-left: 0px;color: rgba(128, 128, 128, 0.39);" /> 
                </td>
            </tr>
            <tr>
                <td>
                    <label>Valor Pendiente:</label>
                    <input id="valorPendiente" style="width: 148px;height: 26px;margin-left: 15px"readonly>
                </td>
            </tr>
            <tr>
                <td colspan="2">
                    <fieldset style="margin: 30px auto;padding: 5px;border-color: #3934341A;">
                        <h5>Observaciones</h5>
                        <textarea id="observaciones" cols="106" rows="34" style="width: 583px;height: 117px;"></textarea>
                    </fieldset>
                </td>
            </tr>
        </table>
        <!--</div>-->
    </div>
    <script>
        funcionFormatos = new formatos()
    </script>
</body>
</html>
