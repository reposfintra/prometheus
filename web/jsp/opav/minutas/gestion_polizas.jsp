<%-- 
    Document   : gestion_polizas
    Created on : 9/07/2016, 10:44:27 AM
    Author     : mcastillo
--%>

<%@page contentType="text/html" pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
        <meta name="viewport" content="width=device-width, initial-scale=1">


        <title>Asignación de Pólizas</title>


        <link href="./css/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css"/>
        <link href="./css/simple-sidebar.css" rel="stylesheet" type="text/css"/>
        <link href="./css/OportunidadNegocio.css" rel="stylesheet" type="text/css"/>


        <link type="text/css" rel="stylesheet" href="./css/jquery/jquery-ui/jquery-ui.css"/>     
        <script type="text/javascript" src="./js/jquery/jquery-ui/jquery.min.js"></script>
        <script type="text/javascript" src="./js/jquery/jquery-ui/jquery.ui.min.js"></script>      

        <link type="text/css" rel="stylesheet" href="./css/buttons/botonVerde.css"/>
        <script type="text/javascript" src="./js/jquery/jquery.jqGrid/js/grid.locale-es.js"></script>
        <script type="text/javascript" src="./js/jquery/jquery.jqGrid/js/jquery.jqGrid.min.js"></script>
        <link type="text/css" rel="stylesheet" href="./css/jquery/jquery.jqGrid/ui.jqgrid.css"/>    
        
        <!-------------------------------------Notificaciones----------------------------------------->
        <link type="text/css" rel="stylesheet" href="./css/toastr.min.css"/>
        <script type="text/javascript" src="./js/toastr.min.js"></script>
        <!-------------------------------------/Notificaciones----------------------------------------->
    </head>
    <body>
        <jsp:include page="/WEB-INF/toptsp.jsp?encabezado=Comercialización De Pólizas"/>

        <input type="hidden" id="id_cliente" name="id_cliente">
        <script type="text/javascript" src="./js/gestionPolizas.js"></script> 
        <!--css logica de negocio-->
        <link href="./css/contratos.css" rel="stylesheet" type="text/css"> 
        <div class="ui-jqgrid ui-widget ui-widget-content center-block" style="width: 1355px; height-min: 300px; margin-top: 10px;border-radius: 10px;border: 1px solid #2A88C8;">

            <!--            <div class="ui-dialog-titlebar ui-widget-header ui-helper-clearfix">
                            <span class="ui-dialog-title text-center" id="ui-dialog-title-listaGlobal">
                                &nbsp;
                                <label><b>Busqueda - Asignación Pólizas a Broker</b></label>
                            </span>
                        </div>-->

            <div class="row" style="padding: 5px;width: 1380px;">

                <div class="col-md-12 ">

                    <fieldset class="scheduler-border">
                        <legend class="scheduler-border">Busqueda De Solicitudes</legend>
                        <div id="div_solicitudes_pendientes"> 
                            <div class="row">
                                <div class="col-md-12">
                                    <table>
                                        <tr>
                                            <td>
                                                <label>Tipo De Proyecto</label>
                                            </td>
                                            <td>
                                                <select id="tipo_proyecto" style="width: 151px;color: #050505;height: 25px;margin-left: 5px;" >
                                                    <option value=''></option>                                        
                                                    <option value='0'>Proyectos Transicion</option>
                                                    <option value='1'>Nuevos Proyectos</option>

                                                </select>
                                            </td>
                                            <td>
                                                <label>Linea Negocio</label>
                                            </td>
                                            <td>
                                                <select id="linea_negocio" style="width: 151px;color: #050505;height: 25px;margin-left: 5px;" ></select>
                                            </td>
                                            <td>
                                                <label style="margin-left: 10px;">Solicitud</label>
                                            </td>
                                            <td>
                                                <input type="text" id="solicitud"  style="width: 80px;margin-left: 5px;height: 26px;" >
                                            </td>
                                            <td>
                                                <label style="margin-left: 10px;">Foms</label>
                                            </td>
                                            <td>
                                                <input type="text" id="txt_foms"  style="width: 80px;margin-left: 5px;height: 26px;" >
                                            </td>
                                            <td>
                                                <label style="margin-left: 10px;">Nombre Cliente</label>
                                            </td>
                                            <td>
                                                <input type="text" id="txt_nom_cliente"  style="width: 80px;margin-left: 5px;height: 26px;" >
                                            </td>
                                            <td>
                                                <label style="margin-left: 10px;">Nombre Proyecto</label>
                                            </td>
                                            <td>
                                                <input type="text" id="txt_nom_proyecto"  style="width: 80px;margin-left: 5px;height: 26px;" >
                                            </td>
                                            <td>
                                                <label style="margin-left: 10px;"hidden="true">Responsable</label>
                                            </td>
                                            <td>
                                                <input type="text" id="responsable"  hidden="true" style="width: 139px;margin-left: 5px;height: 26px;" >

                                            </td>

                                            <td>
                                                <label style="margin-left: 10px;" hidden="true"  >Estado cartera</label>
                                            </td>
                                            <td>
                                                <select id="estadocartera" hidden="true" style="width: 108px;color: #050505;height: 25px;margin-left: 5px;" ></select>
                                            </td> 
                                            <td>
                                                <label style="margin-left: 10px;" hidden="true">Fecha validacion cartera</label>
                                            </td>
                                            <td>
                                                <input type="datetime" id="fechaini" hidden="true" style="width: 100px ;height: 26px;"  readonly>
                                            </td>
                                            <td>
                                                <input  type="datetime" id="fechafin" hidden="true" style="width: 100px;height: 26px;" readonly>
                                            </td>
                                            <td>
                                                <input type="text" id="etapaActual"  style="width: 80px;margin-left: 5px;height: 26px;" value="" hidden >
                                            </td>
                                            <td>
                                                <div style="margin-left: 10px;">
                                                    <button id="listarSolicitudes" class="ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only" 
                                                            role="button" aria-disabled="false">
                                                        <span class="ui-button-text">Buscar</span>
                                                    </button>
                                                </div>
                                            </td>
                                            <td>
                                                <div>
                                                    <button id="clearSolicitudes" class="ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only" 
                                                            role="button" aria-disabled="false">
                                                        <span class="ui-button-text">Limpiar</span>
                                                    </button>
                                                </div>
                                            </td>
                                        </tr>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </fieldset>
                </div>
            </div>
        </div> 
    <center>

        <br><br>
        <table id="tabla_solicitudes_pendientes" align="center" ></table>  
        <div id="page_tabla_solicitudes_pendientes"></div>

    </center>
    <div id="div_polizas_proyecto"  style="display: none; width: 750px" >
        <center>
            <br>
            <table id="tabla_garantias"></table>
            <div id="page_tabla_garantias"></div>    
        </center>
    </div>
    <div id="div_detalle_cot_aseguradora"  style="display: none; width: 750px" >
        <table border="0" align="left" cellpadding="1" cellspacing="1" class="labels">
            <tr>                           
                <td style="width: 25%"><span>&nbsp;Nombre aseguradora</span></td>        
                <td style="width: 75%">
                    <input type="text" name="nombre_aseguradora" id="nombre_aseguradora" readonly style="width: 371px"/>
                </td>
            </tr>                 
        </table> <br>
        <center>
            <br>
            <table id="tabla_detalle_aseguradora"></table>
            <div id="page_tabla_detalle_aseguradora"></div>    
        </center>
        <table border="0" align="right" cellpadding="1" cellspacing="1" class="labels bordecotizacion"style="margin-top: -1px;margin-right: 27px">
            <tr>               
                <td style="width: 60.8%" class="bordecotizacion"><span style="font-weight: bold;">&nbsp;Otros Gastos</span></td>        
                <td align="right" class="bordecotizacion">
                    <input type="text" name="total_gastos" id="total_gastos" readonly value="0" style="width: 133px; border: 1px solid transparent;text-align: right"/>
                </td>
            </tr>                 
            <tr>               
                <td style="width: 62.8%" class="bordecotizacion"><span style="font-weight: bold;">&nbsp;Valor IVA</span></td>        
                <td align="right" class="bordecotizacion">
                    <input type="text" name="total_iva" id="total_iva" value="0" readonly style="width: 133px; border: 1px solid transparent;text-align: right"/>
                </td>
            </tr>                 
            <tr>               
                <td style="width: 62.8%" class="bordecotizacion"><span  style="font-weight: bold;">&nbsp;Total</span></td>        
                <td align="right" class="bordecotizacion">
                    <input type="text" name="total" id="total" readonly value ="0" style="width: 133px; border: 1px solid transparent;text-align: right"/>
                </td>
            </tr>                 
        </table>       
    </div> 
    <!-- Dialogo ventana generación cxp aseguradora -->
    <div id="div_cxp_aseguradora"  style="display: none; width: 750px" >
        <table border="0" align="left" cellpadding="1" cellspacing="1" class="labels">
            <tr>        
            <input type="text" id="num_contrato"  style="width: 50px"hidden >
            <td style="width: 11%"><span>Aseguradora <b style="color:red">*</b></span></td>        
            <td style="width: 28%">
                <select name="id_aseguradora" class="combo_180px" id="id_aseguradora" style="width: 210px"></select>
            </td>
            <td style="width: 11%"><span>Beneficiario <b style="color:red">*</b></span></td>        
            <td style="width: 26%">
                <select name="id_beneficiario" class="combo_180px" id="id_beneficiario" style="width: 190px"></select>
            </td>
            <td style="width: 8%"><span>Otro Si </span></td>        
            <td style="width: 20%">
                <select name="otro_si" class="combo_180px" id="otro_si" style="width: 129px"></select>
            </td>
            </tr>                 
        </table> <br>
        <center>
            <br>
            <table id="tabla_cxp_aseguradora"></table>
            <div id="page_tabla_cxp_aseguradora"></div>    
        </center>
        <table border="0" align="right" cellpadding="1" cellspacing="1" class="labels bordecotizacion"style="margin-top: -1px;margin-right: 5px">
            <tr>               
                <td style="width: 62.7%" class="bordecotizacion"><span style="font-weight: bold;">&nbsp;Otros Gastos</span></td>        
                <td align="right" class="bordecotizacion">
                    <input type="text" name="total_gastos_cxp" id="total_gastos_cxp" class="solo-numero" value="0" style="width: 135px; border: 1px solid transparent;text-align: right"/>
                </td>
            </tr>                 
            <tr>               
                <td style="width: 62.7%" class="bordecotizacion"><span style="font-weight: bold;">&nbsp;Valor IVA</span></td>        
                <td align="right" class="bordecotizacion">
                    <input type="text" name="total_iva" id="total_iva_cxp" value="0" readonly style="width: 135px; border: 1px solid transparent;text-align: right"/>
                </td>
            </tr>                 
            <tr>               
                <td style="width: 62.7%" class="bordecotizacion"><span  style="font-weight: bold;">&nbsp;Total</span></td>        
                <td align="right" class="bordecotizacion">
                    <input type="text" name="total" id="total_cxp" readonly value ="0" style="width: 135px; border: 1px solid transparent;text-align: right"/>
                </td>
            </tr>                 
        </table>       
    </div> 
    <!-- Dialogo ventana ingreso numero de factura -->
    <div id="dialogFactura"  style="display:none;">       
        <br>                           
        <table border="0" align="left" cellpadding="1" cellspacing="1" class="labels">
            <tr>
                <td style="width: 25%"><span>No Factura <b style="color:red">*</b></span></td>        
                <td style="width: 75%">
                    <input type="text" id="num_factura" class="mayuscula" style="width: 298px" maxlength="30"/>
                </td>
            </tr>                 
        </table>
    </div>
    <!-- Dialogo ventana asignación de broker -->
    <div id="dialogAsignarBroker"  style="display:none;">       
        <br>                           
        <table border="0" align="left" cellpadding="1" cellspacing="1" class="labels">
            <tr>
                <td style="width: 25%"><span>Beneficiario <b style="color:red">*</b></span></td>        
                <td style="width: 75%">
                    <select name="beneficiario" class="combo_180px" id="beneficiario" style="width: 270px"></select>
                </td>
            </tr>      
            <tr>
                <td style="width: 25%"><span>Broker <b style="color:red">*</b></span></td>        
                <td style="width: 75%">
                    <select name="broker" class="combo_180px" id="broker" style="width: 270px"></select>
                </td>
            </tr>  
        </table>
    </div>
    <div id="dialogLoading" style="display:none;">
        <p  style="font-size: 12px;text-align:justify;" id="msj2">Texto </p> <br/>
        <center>
            <img src="/fintra/images/cargandoCM.gif"/>
        </center>
    </div>
    <div id="dialogMsj" title="Mensaje" style="display:none;">
        <p style="font-size: 12px;text-align:justify;" id="msj" > Texto </p>
    </div>      
</body>
</html>
