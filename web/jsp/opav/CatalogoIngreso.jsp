<%@page import="com.tsp.operation.model.beans.Compania"%>
<%@page import="com.tsp.operation.model.CompaniaService"%>
<%@page import="com.tsp.operation.model.beans.Usuario"%>
<%@page import="com.tsp.operation.model.beans.CmbGeneralScBeans"%>
<%@page import="java.util.ArrayList"%>
<%@page import="com.tsp.opav.model.DAOS.impl.ProcesosCatalogoImpl"%>
<%@page import="com.tsp.opav.model.DAOS.ProcesosCatalogoDAO"%>
<%@page contentType="text/html" pageEncoding="utf-8"%>
<%@include file="/WEB-INF/InitModel.jsp"%>
<!DOCTYPE html>
<%    Usuario usuario = (Usuario) session.getAttribute("Usuario");
    String nomEmpresa = "";
    CompaniaService ciaserv = new CompaniaService();
    ciaserv.buscarCia(usuario.getDstrct());
    Compania cia = ciaserv.getCompania();
    nomEmpresa = cia.getdescription();
%>   
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
        <title>Administracion de Catalogos</title>
        <link href="./css/popup.css" rel="stylesheet" type="text/css">
        <link type="text/css" rel="stylesheet" href="./css/jquery/jquery-ui/jquery-ui.css" />
        <link type="text/css" rel="stylesheet" href="./css/CatalogoIngreso.css " />
        <script type="text/javascript" src="./js/jquery/jquery.jqGrid/js/jquery.min.js"></script>
        <script type="text/javascript" src="./js/jquery-ui-1.8.5.custom.min.js"></script>   
        <script type="text/javascript" src="./js/jquery/jquery.jqGrid-4.7.0/js/grid.locale-es.js"></script>
        <script type="text/javascript" src="./js/jquery/jquery.jqGrid-4.7.0/js/jquery.jqGrid.min.js"></script>
        <script type="text/javascript" src="./js/jquery/jquery.jqGrid-4.7.0/js/jquery.tabletojson.js"></script>    

        <script type="text/javascript" src="./js/CatalogoIngreso.js"></script>   

    </head>
    <body>
        <div id="capaSuperior" style="position:absolute; width:100%; height:100px; z-index:0; left: 0px; top: 0px;">
            <jsp:include page="/WEB-INF/toptsp.jsp?encabezado=Catalogo Ingreso"/>
        </div>
    <center>
        <!------------NUEVO------------------>
        <div id="capaCentral" style="position:absolute; width:100%; height:85%; z-index:0; left: 0px; top: 100px; overflow: auto;" align="center">
            <center>
                <br>
                <div class="ui-jqgrid ui-widget ui-widget-content ui-corner-all" style="max-width: 407px; height: 1;">

                    <div class="ui-dialog-titlebar ui-widget-header ui-corner-all ui-helper-clearfix">
                        <span class="ui-dialog-title" id="ui-dialog-title-listaGlobal">
                            &nbsp;
                            <label class="titulotablita"><b>FILTRO DE BUSQUEDA</b></label>
                        </span>
                    </div>
                    <table style=" padding: 0.5em 1em 0.5em 1em" style="width: 95%;">
                        <tr>
                            <td>Insumo</td>
                            <td>
                                <select id="insumo" onchange="cargarComboCategoria();" style="width: 264px;" >
                                    <option value=''>...</option>
                                </select>
                            </td>
                        </tr>
                        <tr>
                            <td>Categoria</td>
                            <td>
                                <select id="categoria" nombre="categoria" onchange="cargarCombo('sub', [this.value])" style="width: 264px;" >
                                    <option value=''>...</option>
                                </select>
                            </td>
                        </tr>
                        <tr>
                            <td>SubCategoria</td>
                            <td>
                                <select id="sub"  style="width: 264px;" >
                                    <option value=''>...</option>
                                </select>

                                <input type="hidden" name="nomsub" id="nomsub">
                            </td>
                        </tr>
                    </table>
                    <hr>
                    <div id ='botones'>
                        <button id="buscar" class="ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only" 
                                role="button" aria-disabled="false">
                            <span class="ui-button-text">Buscar</span>
                        </button> 
                    </div>
                </div>

            </center>
        </div>
        <!------------NUEVO------------------>
        <div style="margin-top: 280px">
            <table id="tbl_ingreso_catalogo" ></table>                
            <div id="page_ingreso_catalogo"></div>
        </div>
        <!---------------------------------------------- Proveedor Tabla------------------------------------------------->
        <div id="div_buscar_proveedor"  style="display: none; width: 800px" >
            <table border="0"  align="center">
                <tr>
                    <td>
                        <div id="searchProveedor"></div>
                        <table id="proveedores" ></table>
                        <div id="page_tabla_proveedores"></div>
                    </td>
                </tr>
            </table>
        </div>     


        <div id="dialogLoading" style="display:none;">
            <p  style="font-size: 12px;text-align:justify;" id="msj2">Texto </p> <br/>
            <center>
                <img src="./images/cargandoCM.gif"/>
            </center>
        </div>

        <div id="divSalidaEx" title="Exportacion" style=" display: block" >
            <p  id="msjEx" style=" display:  none"> Espere un momento por favor...</p>
            <center>
                <img id="imgloadEx" style="position: relative;  top: 7px; display: none " src="./images/cargandoCM.gif"/>
            </center>
            <div id="respEx" style=" display: none"></div>
        </div>  


    </center>

    <div id="dialogMsgMeta" title="Mensaje" style="display:none;">
        <p style="font-size: 12.5px;text-align:justify;" id="msj" > Texto </p>
    </div>      

</body>
</html>
