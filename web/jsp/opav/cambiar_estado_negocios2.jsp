<%--    Created on : 02/02/2009    Author     : iamorales--%>
<%@page import="com.tsp.operation.model.beans.AnticiposTerceros"%>
<%@page import="com.tsp.operation.model.beans.Usuario"%>
<%@ page session   ="true"%>
<%@ page errorPage ="/error/ErrorPage.jsp"%>
<%@ page import    ="java.util.*" %>
<%@ page import    ="com.tsp.opav.model.*"%>
<%@ page import    ="com.tsp.opav.model.beans.*"%>
<%@ page import    ="com.tsp.opav.model.services.*"%>
<%@ page import    ="com.tsp.util.*"%>
<%@ page import    ="com.tsp.util.Util.*"%>
<%@ include file   ="/WEB-INF/InitModel.jsp"%>
<%@taglib uri="http://jakarta.apache.org/taglibs/input-0.90" prefix="input" %>

<%
        Double totalValMaterial =0.0;
        Double totalValManoObra =0.0;
        Double totalValOtros =0.0;
        Double totalValAdministracion =0.0;
        Double totalValImprevistos =0.0;
        Double totalValUtilidad =0.0;
        Double totalValCostoContra =0.0;
        Double totalValPrecioVenta =0.0;
	String id_conse=request.getParameter("id_conse");//08/04/2010
	if (id_conse==null){id_conse="";}//MGarizao - GEOTECH
        String vista = request.getParameter("vista")!=null ? request.getParameter("vista") : "1";

        String path = application.getRealPath(model.menuService.getRealUrl(request.getRequestURI()));
	String datos[] = model.menuService.getContenidoMenu(BASEURL,request.getRequestURI(),path);

	Usuario usuario = (Usuario) session.getAttribute("Usuario");
	String loginx=usuario.getLogin();

	ClientesVerService clvsrv = new ClientesVerService(usuario.getBd());
        String perfil = clvsrv.getPerfil(usuario.getLogin());

	String estadi=request.getParameter("estadito");
	if (estadi==null){estadi="0";}

	String contrati=request.getParameter("contratistica");
	if (contrati==null){contrati="";}

	String fact_conformada_consultar=request.getParameter("fact_conformada_consultar");
	if (fact_conformada_consultar==null){fact_conformada_consultar="";}

	String id_solici=request.getParameter("id_solici");//090922
	if (id_solici==null){id_solici="";}//090922

	String id_cliente=(request.getParameter("id_cliente")==null)?"":request.getParameter("id_cliente");//090922
	if (id_cliente==null){id_cliente="";}//090922

	String nicc=request.getParameter("nicc");//090922
	if (nicc==null){nicc="";}//090922

	String nomclie=request.getParameter("nomclie");//090922
	if (nomclie==null){nomclie="";}//090922

	String num_osxi=request.getParameter("num_osx");
	if (num_osxi==null){num_osxi="";}

	ArrayList negocios ;

	try {
            if (vista.equals("2")) {
                negocios = modelopav.NegociosApplusService.getNegociosApplusEquipos(estadi, contrati, num_osxi, fact_conformada_consultar, loginx, id_solici, nicc, "", id_cliente, id_conse, "");
            } else {
                negocios = modelopav.NegociosApplusService.getNegociosApplus2(estadi, contrati, num_osxi, fact_conformada_consultar, loginx, id_solici, nicc, "", id_cliente, id_conse, "");

            }
	}
        catch(Exception e ){
            System.out.println("errorcillo en model.NegociosApplusService.getNegociosApplus2()"+e.toString());
            negocios=null;
	}


%>
<html>
<%try{%>
<head>

    <!--//20101112-->
        <style type="text/css">
              .fmsgx {
              background: #040;
              color: white;
              font-family: serif;
              font-size: 24px;
              text-align: center;
              padding: 10px;
              -moz-border-radius: 10px;
              -webkit-border-radius: 10px;
              border-radius: 10px;
              position: absolute;
              top: 50px;
              right: 50%;
              width: 400px;
              height: 75px;
              margin-right: -200px;
              overflow: scroll;
              opacity: 0.9;
            }
        </style>
        <!--
        width: 400px;
        height: 75px;
        top: 50px;
        right: 50%;
        overflow: hidden;
        -->

    <title>Gestion de Negocios</title>

    <link href="<%=BASEURL%>/css/estilostsp.css"    rel="stylesheet" type="text/css">

    <script src="<%=BASEURL%>/js/boton.js"          type="text/javascript"></script>
    <script src="<%=BASEURL%>/js/Validacion.js"     type='text/javascript'></script>
    <script src="<%=BASEURL%>/js/tools.js"          type="text/javascript"></script>
    <script src="<%=BASEURL%>/js/date-picker.js"    type="text/javascript"></script>
    <script src="<%=BASEURL%>/js/transferencias.js" type="text/javascript"></script>
    <script src="<%=BASEURL%>/js/prototype.js"      type="text/javascript"></script>

    <script type='text/javascript' src="<%= BASEURL %>/js/validar.js"></script><!--20101112-->
   <script type='text/javascript' src="<%= BASEURL %>/js/buscarproveedor.js"></script><!--20101112-->

    <!-- Las siguientes librerias CSS y JS son para el manejo de
        DIVS dinamicos.
    -->

    <link href="<%=BASEURL%>/css/default.css"       rel="stylesheet" type="text/css">
    <link href="<%=BASEURL%>/css/mac_os_x.css"      rel="stylesheet" type="text/css">
    <link href="<%=BASEURL%>/css/alphacube.css"      rel="stylesheet" type="text/css">

    <script src="<%=BASEURL%>/js/effects.js"        type="text/javascript"></script>
    <script src="<%=BASEURL%>/js/window.js"         type="text/javascript"></script>
    <script src="<%=BASEURL%>/js/window_effects.js" type="text/javascript"></script>

    <!-- Las siguientes librerias CSS y JS son para el manejo del
        calendario(jsCalendar).
    -->

    <link rel="stylesheet" type="text/css" href="<%=BASEURL%>/css/jCal/jscal2.css" />
    <link rel="stylesheet" type="text/css" href="<%=BASEURL%>/css/jCal/border-radius.css" />

    <script type="text/javascript" src="<%=BASEURL%>/js/jCal/jscal2.js"></script>
    <script type="text/javascript" src="<%=BASEURL%>/js/jCal/lang/es.js"></script>

<!--    <script type="text/javascript" src="<%=BASEURL%>/js/jquery.min.js"></script>-->
    <script type="text/javascript" src="./js/jquery/jquery.jqGrid/js/jquery.min.js"></script>
    <script type="text/javascript" src="./js/jquery-ui-1.8.5.custom.min.js"></script>
    <!--<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.2.6/jquery.min.js"></script><!--20101111-->

    <script type="text/javascript">//20101112am
        jQuery.noConflict();//20101112am
        var $j = jQuery;//20101112am
        //20101112
        var msgx =document.createElement("div");//20101112
        msgx.className = "fmsgx";//20101112
        textx=document.createTextNode("xxx");//20101112
        msgx.appendChild(textx);//20101112
        //msgx.is_showing=true;//20101112

        msgx.style.textIndent = "-1000em";//20101112
                    jQuery(msgx).animate({//20101112
                        width: "5px", height: "5px", top: "20px",//20101112
                        right: "20px", marginRight: "0px", opacity: "0.2"//20101112
                    }, 300);//20101112
        msgx.is_showing=false;//20101112

        jQuery(document).ready(function() {//20101112
          fmsgx = {//20101112
            messagex: function(textx1) {//20101112
                if (msgx.is_showing) {//20101112
                    msgx.style.textIndent = "-1000em";//20101112
                    jQuery(msgx).animate({//20101112
                        width: "5px", height: "5px", top: "20px",//20101112
                        right: "20px", marginRight: "0px", opacity: "0.2"//20101112
                        ,scrollTop: "100"
                    }, 300);//20101112
                    msgx.is_showing = false;//20101112
                } else {//20101112
                    msgx.removeChild(textx);//20101112
                    textx=document.createTextNode(textx1);//20101112
                    msgx.appendChild(textx);//20101112
                    msgx.style.textIndent = "0";//20101112
                    jQuery(msgx).animate({//20101112
                        //width: "400px", height: "75px", top: "50px",//20101112
                        //width: "400px", height: "75px", top: "40%",//20101112
                        width: "40%", height: "30%", top: "35%",//20101112
                        //right: "50%", marginRight: "-200px", opacity: "0.9"//20101112
                        right: "30%", marginRight: "0px", opacity: "0.9"//20101112
                        }, 500);//20101112
                    //}, 300);//20101112
                    msgx.is_showing = true;//20101112
                    document.getElementsByTagName("body")[0].appendChild(msgx);//20101112
                }//20101112
            }//20101112
          };//20101112
        });//20101112

        jQuery(msgx).click(function(){//20101112
          if (msgx.is_showing) {//20101112
           msgx.style.textIndent = "-1000em";//20101112
            jQuery(msgx).animate({//20101112
              width: "5px", height: "5px", top: "20px",//20101112
              right: "20px", marginRight: "0px", opacity: "0.2"//20101112
            }, 300);//20101112
            msgx.is_showing = false;//20101112
          }//20101112
        });//20101112

        function mostrarInfoX(msjxxx){//20101112
            fmsgx.messagex(msjxxx);//20101112
        }//20101112

    </script>

    <%
    if (request.getParameter("nadita")!=null && request.getParameter("nadita").equals("si")){%>
        <script>alert("No se hizo el cambio por restricci�n de seguridad.");</script>
    <%}
    %>
    <script>
        var temp=';;;;;;;;;;';

        var generar=0;

        function openWin(i){
            var cad = $('select_opt'+i).value;
            cad = cad.split(';');

            if(cad[0] == '1')
            {
     
                window.open('<%=BASEURL%>/jsp/opav/generar_oferta.jsp?id_solicitud='+cad[1],'','width=1000,height=800');
            }
            if(cad[0] == '2'){
                window.open('<%=BASEURL%>/jsp/opav/aceptacion_ot.jsp?id_padre='+cad[1]+'&id_sol='+cad[2]+'&estado='+cad[3],'','width=1000,height=800');
            }
            if(cad[0] == '3'){
                window.open('<%=BASEURL%>/jsp/opav/generar_OT.jsp?id_padre='+cad[1]+'&id_sol='+cad[2]+'&cons='+cad[3]+'&estado='+cad[4]+'&numos='+cad[5],'','width=1000,height=800');//Modificado jpena 04-05-2010
            }
            if(cad[0] == '4'){
                showWin(cad[1]);
            }
            if(cad[0] == '5'){
                showWinRecepcionObra(cad[1]);
            }

            if(cad[0] == '6'){
                window.open('<%=BASEURL%>/jsp/opav/observaciones_open.jsp?id_solicitud='+cad[1],'','width=1000,height=800');
            }
            if(cad[0] == '7'){
                window.open('<%=BASEURL%>/jsp/opav/cambiarSolicitud_Cliente.jsp?id_sol='+cad[1]+'&estado='+cad[2],'','width=1000,height=800');
            }
            if(cad[0] == '8'){
                if(confirm("Esta seguro que desee reevaluar esta solicitud?")){
                    reevaluar(cad[1]);
                }
            }
            if(cad[0] == '9'){
                showWinFormEncuesta(cad[1]);
            }
        }

        function reevaluar(id_sol){
         var p = 'opcion=7&num_oferta='+id_sol;
         new Ajax.Request(
                '<%=CONTROLLEROPAV%>?estado=Electricaribe&accion=Oferta',
                {
                    method: 'post',
                    parameters: p,
                    onComplete: ajaxFinalReevaluar
                });
        }

        function ajaxFinalReevaluar(response){
            var texto = response.responseText;
            texto= texto.replace(/^\s+/g,'').replace(/\s+$/g,'');
            if(texto=='si'){
                alert("la solicitud se ha reevaluado con exito")
            }else{
                alert("no se pudo reevaluar la solicitud")
            }


        }

        function denegar(theForm){

            var elementos = document.getElementsByName("idNegocio");
            var sw = false;

            for (var i=0;i<elementos.length; i++){
                    if (elementos[i].checked){
                            sw = true;
                    }
            }
            if(sw==true){
                    formulario.action="<%=CONTROLLEROPAV%>?estado=Negocios&accion=Applus&opcion=denegar";
                    theForm.submit();
            }else{
                    alert("Verifique que haya al menos una solicitud seleccionada.");
            }
        }


       function showWinRecepcionObra(idxx){
            var content = '';

            var win = new Window({/*className: "mac_os_x",*/
                            id: "recepobr",
                            title: "RECEPCION_OBRA",
                            width:1150,
                            height:600,
                            showEffectOptions: {duration:0},
                            hideEffectOptions: {duration:0},
                            destroyOnClose: true,
                            //url:'<%=BASEURL%>/jsp/opav/recepcion_obra.jsp',
                            url:'<%=CONTROLLEROPAV%>?estado=Negocios&accion=Applus&opcion=recibirObra&solicitud='+idxx,
                            recenterAuto: false});

            win.showCenter();

        }
        
        function showWinFormEncuesta(idxx){
            var content = '';

            var win = new Window({/*className: "mac_os_x",*/
                            id: "encuenta",
                            title: "Formato Encuesta Satistaccion del Cliente",
                            width:1150,
                            height:600,
                            showEffectOptions: {duration:0},
                            hideEffectOptions: {duration:0},
                            destroyOnClose: true,
                            url:'<%=BASEURL%>/jsp/opav/encuestaSatisfaccion_Cliente.jsp?solicitud='+idxx,
                            recenterAuto: false});
            win.showCenter();

        }

        function showWin(id){
            var content = '';

            var win = new Window({/*className: "mac_os_x",*/
                            title: "FINANCIACION",
                            width:300,
                            height:150,
                            showEffectOptions: {duration:0},
                            hideEffectOptions: {duration:0},
                            destroyOnClose: true,
                            recenterAuto: false});

            content = '<div align="center">';
            content += '<br>';
            content += '<br>';
            content += '<a style="font-size:12px;">Fecha de financiacion:</a>';

            content += '<br>';
            content += '<input id="calendar-field" readonly>';
            content += '<button id="calendar-trigger">...</button>';

            content += '<br>';
            content += '<img src="<%=BASEURL%>/images/botones/aceptar.gif" height="21" title="Aceptar" onclick="ajaxFinanciacion('+id+');" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" style="cursor:hand">';
            content += '</div>';

            content += '<br>';
            content += '<div align="center" name="loading_div" id="loading_div" style="visibility: hidden">';
            content += 'Cargando...';
            content += '</div>';

            win.getContent().update( content );
            win.showCenter();

            Calendar.setup({
                inputField : "calendar-field",
                trigger    : "calendar-trigger",
                onSelect   : function() { this.hide() }
            });
        }

        function ajaxFinanciacion(id){
            if(generar==0){
                var p = 'opcion=4&idsol='+id+'&fecha='+$('calendar-field').value;

                new Ajax.Request(
                '<%=CONTROLLEROPAV%>?estado=Electricaribe&accion=Ot',
                {
                    method: 'get',
                    parameters: p,
                    onLoading: whenLoading,
                    onComplete: ajaxFinanFinish
                });
            }
            else{
                alert('Solicitud en proceso');
            }
        }

        function switchDiv(){
            if($('loading_div').style.visibility == 'visible'){
                $('loading_div').style.visibility = 'hidden';
            }
            else{
                $('loading_div').style.visibility = 'visible';
            }
        }

        function whenLoading(){
            switchDiv();
            generar = 1;
        }

        function ajaxFinanFinish(response){
            alert(response.responseText);
            switchDiv();
            generar = 0;
        }

        function seleccionarOrdenDeAccion(theForm,orden,che){
            if (che){
                for (i=0;i<theForm.length;i++){
                    if (theForm.elements[i].type=='checkbox' && theForm.elements[i].title=="s"+orden){
                        theForm.elements[i].checked=che;
                    }
                }
            }
        }

	function asignarEstado(theForm){
            if (theForm.estado_asignable.value=="0"){
                alert("Debe seleccionar 1 estado a asignar.");
                return;
            }

            var elementos   = document.getElementsByName("idAcc");
            var sw          = false;

            for (var i=0;i<elementos.length; i++){
                    if (elementos[i].checked){
                            sw = true;
                    }
            }
            if(sw==true){
                    formulario.action="<%=CONTROLLEROPAV%>?estado=Negocios&accion=Applus&opcion=asignarEstado";
                    theForm.submit();
            }else{
                    alert("Debe seleccionar por lo menos 1 'id_accion'.");
            }
        }

        function checkEnter(e){ //e is event object passed from function invocation//090720
                var characterCode //literal character code will be stored in this variable

                if(e && e.which){ //if which property of event object is supported (NN4)
                        e = e
                        characterCode = e.which //character code is contained in NN4's which property
                }else{
                        e = event
                        characterCode = e.keyCode //character code is contained in IE's keyCode property
                }

                if(characterCode == 13){ //if generated character code is equal to ascii 13 (if enter key)
                        //document.forms[0].submit() //submit the form
                        //alert("enter");
                        //return false
                        consultarEstado();
                }else{
                        //alert("e"+e+"char"+characterCode);
                        return true
                }
        }

        function formatx(input){//090831
                var num=""+input;
                num = num.replace(/\./g,'');
                if(!isNaN(num)){
                        num = num.toString().split('').reverse().join('').replace(/(?=\d*\.?)(\d{3})/g,'$1.');
                        num = num.split('').reverse().join('').replace(/^[\.]/,'');
                        input = num;
                }else{
                        alert('Solo se permiten n�meros');
                        //input.value = input.value.replace(/[^\d\.]*/g,'');
                }
                return num;
        }

        function sumarOferta(solicitt){
                var vals = document.getElementsByName(solicitt);
                var respuest=0;
                var temx=0;
                for (var i=0;i<vals.length; i++){
                        temx=vals[i].value.replace(",","");
                        temx=temx.replace(",","");
                        temx=temx.replace(",","");//091216
                        //subtotal=parseInt(subtotal)+parseInt(tem);
                        respuest=parseInt(respuest)+parseInt(temx);
                        //respuest=respuest+vals[i].value;
                }
                //alert("oferta: "+formatx(respuest));
                Dialog.closeInfo();//20100727

                Dialog.alert("<br><center>"+  "oferta: $ "+formatx(respuest)  +"</center>", {
                                width:130,
                                height:90,
                                showProgress: false,
                                windowParameters: {className: "alphacube"}
                            });
        }

        function validar(theForm){
                var elementos = document.getElementsByName("idNegocio");
                var sw = false;

                for (var i=0;i<elementos.length; i++){
                    if (elementos[i].checked){
                        sw = true;
                    }
                }

                if(sw==true){
                    //alert ("cas");
                    formulario.action="<%=CONTROLLEROPAV%>?estado=Negocios&accion=Applus&opcion=cambiarEstado";
                    theForm.submit();
                }
                else{
                    alert("Verifique que haya al menos un negocio seleccionado.");
                }
        }

        function Sell_all_col(theForm,nombre){

            for (i=0;i<theForm.length;i++)
                if (theForm.elements[i].type=='checkbox' && theForm.elements[i].name==nombre)
                    theForm.elements[i].checked=theForm.All.checked;
        }

        function consultarCliente(){
                if (formulario.nicc.value=="" && formulario.idclie.value==""){
                        alert("Debe tener nic o nombre de cliente.");
                }else{
                        formulario.target="_blank";
                        formulario.action="<%=CONTROLLEROPAV%>?estado=Clientes&accion=Ver&opcion=buscarcliente";
                        formulario.submit();
                }
        }

        function seleccionarOrden(theForm,orden,che){
            for (i=0;i<theForm.length;i++){
                if (theForm.elements[i].type=='checkbox' && theForm.elements[i].value==orden){
                    theForm.elements[i].checked=che;
                }
            }
        }

        function consultarEstado(){
                formulario.target ="";
                formulario.action="<%=CONTROLLEROPAV%>?estado=Negocios&accion=Applus&opcion=consultarEstado2";
                formulario.submit();

        }
        
        function exportarExcel(){
            var opt = {
            autoOpen: false,
            modal: true,
            width: 250,
            height: 150,
            title: 'Descarga Reporte'
        };
        //  $("#divSalidaEx").dialog("open");
        $j("#divSalidaEx").dialog(opt);
        $j("#divSalidaEx").dialog("open");
        $j("#imgloadEx").show();
        $j("#msjEx").show();
        $j("#respEx").hide();
        setTimeout(function () {
            $j.ajax({
                async: false,
                url: "<%=CONTROLLEROPAV%>?estado=Negocios&accion=Applus",
                type: 'POST',
                dataType: "html",
                data: {
                    opcion: 'exportarExcel',
                    nicc:formulario.nicc.value,
                    nomclie:formulario.nomclie.value,
                    idclie:formulario.idclie.value,
                    id_conse:formulario.id_conse.value,
                    id_solici:formulario.id_solici.value,
                    num_osx:formulario.num_osx.value
                },
                success: function (resp) {
                    $j("#imgloadEx").hide();
                    $j("#msjEx").hide();
                    $j("#respEx").show();
                    var boton = "<div style='text-align:center'>\n\ </div>";                   
                    document.getElementById('respEx').innerHTML = resp + "<br/><br/><br/><br/>" + boton;


                }, error: function (xhr, ajaxOptions, thrownError) {
                    $j("#dialogLoading").dialog('close');
                    alert("Error: " + xhr.status + "\n" +
                            "Message: " + xhr.statusText + "\n" +
                            "Response: " + xhr.responseText + "\n" + thrownError);
                }
            });

        }, 500);


        }
             
        function asignar(theForm){

            var elementos = document.getElementsByName("idNegocio");
            var sw = false;

            for (var i=0;i<elementos.length; i++){
                    if (elementos[i].checked){
                            sw = true;
                    }
            }
            if(sw==true){
                    formulario.action="<%=CONTROLLEROPAV%>?estado=Negocios&accion=Applus&opcion=asignar";
                    theForm.submit();
            }else{
                    alert("Verifique que haya al menos una solicitud seleccionada.");
            }
        }

        function sendAction(url,filtro,nomselect,def){
            if(temp!=filtro){
                if(filtro!=''){
                    $('imgworking').style.visibility="visible";
                    temp=filtro;
                    var p = "filtro=" + filtro + "&nomselect=" + nomselect  + "&opcion=clselect" + "&idclie="+def;

                    new Ajax.Request(
                    url,
                    {
                        method: 'get',
                        parameters: p,
                        onSuccess: deploySelect
                    });
                }
                else{
                    $("cliselect").innerHTML = "<select name='idclie'><option value=''>Seleccione</option></select>";
                }
            }
        }

        function deploySelect(response){
            respuesta=response.responseText.split(';;;;;;;;;;');
            if(respuesta[0]==$('nomclie').value){
                document.getElementById("cliselect").innerHTML=respuesta[1];
                $('imgworking').style.visibility="hidden";
            }
        }

        /* Esta funcion init() se inicializa
         * en el onLoad del body.
         */
        function init(){
            redimensionar();
            formulario.id_solici.focus();
            <%
                String nomx = (request.getParameter("nomclie")!=null)?request.getParameter("nomclie"):"";
                if(nomx.equals("")){
            %>
            $("cliselect").innerHTML = "<select name='idclie'><option value=''>Seleccione</option></select>";
            <%
                }
                else{
            %>
            sendAction('<%=CONTROLLEROPAV%>?estado=Negocios&accion=Applus',document.getElementById('nomclie').value,'idclie','<%=(request.getParameter("id_cliente")!=null)?request.getParameter("id_cliente"):""%>');
            <%
                }
            %>
        }

               function factConf(cantfilas){
            var texto = document.getElementById("fact_conf").value;
            if(texto==''){
                alert('Debe escribir un numero de factura para poder proceder');
            }
            else {
                var sw = false;
                var cadenasel="";
                for (var i=0;i<cantfilas; i++){
                    if (document.getElementById("idacc"+i).checked){
                        cadenasel = cadenasel + document.getElementById("idacc"+i).value + ";_;";
                        sw = true;
                    }
                }
                if(sw==true){
                    var url = "<%=CONTROLLEROPAV%>?estado=Negocios&accion=Applus";
                    //var p =  "opcion=conformar&datos="+cadenasel+"&factura="+texto+"&usuario=<%= loginx %>";
                    var p =  "opcion=conformar&datos="+cadenasel+"&factura="+texto+"&usuario=<%= loginx %>&fecfactconf="+document.getElementById("calendar-fieldx").value;
                    new Ajax.Request(
                        url,
                        {
                            method: 'post',
                            parameters: p,
                            onLoading: loading,
                            onComplete: llenarDiv
                        });
                }else{
                    alert("Verifique que haya al menos una accion seleccionada.");
                }
            }
        }
        function loading(){
            document.getElementById("divcarga").innerHTML ='<span class="fila">Procesando </span><img alt="cargando" src="<%= BASEURL%>/images/cargando.gif"  name="imgload">';
        }

        function llenarDiv(response){
            var texto = response.responseText;
            document.getElementById("divcarga").innerHTML = '<span class="fila">'+texto+'</span>';
            if(texto.charAt(0)=='D'){
                alert(texto);
                formulario.target ="";
                formulario.action="<%=CONTROLLEROPAV%>?estado=Negocios&accion=Applus&opcion=consultarEstado2";
                formulario.submit();
            }
        }
    </script>
</head>
<body onLoad="init();" onResize="redimensionar();">

<div id="capaSuperior" style="position:absolute; width:100%; height:100px; z-index:0; left: 0px; top: 0px;">
<jsp:include page="/WEB-INF/toptsp.jsp?encabezado=Negocios"/>
</div>

<div id="capaCentral" style="position:absolute; width:100%; height:85%; z-index:0; left: -1px; top: 100px; overflow: scroll;">
<% GestionSolicitudAiresAAEService sasrv = new GestionSolicitudAiresAAEService(usuario.getBd());
boolean sw=false;
   ArrayList listaEstados = new ArrayList();
    ArrayList contratistas = new ArrayList();
       if (vista.equals("2")) {
               listaEstados = modelopav.NegociosApplusService.getEstadosApplusAAAE();
               contratistas = sasrv.buscarContratistasAAAE("P");
               contratistas.addAll(sasrv.buscarContratistasAAAE("I"));
               for (int i = 0; i < contratistas.size(); i++) {
                   contratistas.set(i, ((String) contratistas.get(i)).split(";_;"));
               }
       } else {
           listaEstados = modelopav.NegociosApplusService.getEstadosApplus2();
            contratistas = modelopav.NegociosApplusService.getContratistas();
       }
   ArrayList listaEstadosUser   = modelopav.NegociosApplusService.getEstadosApplusUser(loginx);

   String nombre_estado="";
   String nombre_contratista="";

   String[] estadixx;

   String[] contratixx;

   for (int i=0;i<listaEstados.size();i++){
		estadixx=(String[]) listaEstados.get(i);
		if(estadixx[0].equals(estadi)){ nombre_estado=estadixx[1];}
   }
   for (int i=0;i<contratistas.size();i++){
		contratixx=(String[]) contratistas.get(i);
		if(contratixx[0].equals(contrati)){ nombre_contratista=contratixx[1];}
   }

   if (loginx!=null ){
        for (int kk=0;kk<contratistas.size();kk++)
        {   if ( usuario.getNitPropietario().equals(((String[])contratistas.get(kk))[0]))
            {   contrati=usuario.getNitPropietario(); sw=true;
            }
        }
   }

   if (negocios!=null  ){
%>
    <form  method="post" name="formulario"  >
    <input type="hidden" name="estadito" value="<%=estadi%>">
    <%
    String respuesta=request.getParameter("respuesta");
	//String valor_neto="";//request.getParameter("valor_neto");
    %>
    <br><br>
	<table align="center">
		<tr class="fila">
			<td>
				&nbsp;&nbsp;Estado actual:&nbsp;&nbsp;
			</td><td>
                            <input type="hidden" name="vista" value="<%=vista%>">
				<select name="estado_consultar" onChange="consultarEstado();">

					<option value="<%=estadi%>"><%=nombre_estado%></option>
					<%
					for (int i=0;i<listaEstados.size();i++){
						String[] estadinho=(String[])listaEstados.get(i);
						%>
						<option value="<%=estadinho[0]%>"><%=estadinho[1]%></option>
						<%
					}
					%>

				</select>
				&nbsp;&nbsp;
			</td>
		</tr>
		<tr class="fila">
                    <td>
                        &nbsp;&nbsp;Contratista:&nbsp;&nbsp;
                    </td>
                    <td>
                        <%if (sw)
                        {%>
                        <%//if (loginx!=null && (  loginx.equals("SENTEL") )) {%>
                                <input type="text" name="contratista_consultar"  value="<%=contrati%>" readonly  >
                        <%}else{%>
                                <select name="contratista_consultar"  >
                                        <option value="<%=contrati%>"><%=nombre_contratista%></option>
                                        <option value=""> </option>
                                        <%
                                        for (int i=0;i<contratistas.size();i++){
                                                String[] contratistanho=(String[])contratistas.get(i);
                                                %>
                                                <option value="<%=contratistanho[0]%>"><%=contratistanho[1]%></option>
                                                <%
                                        }
                                        %>
                                </select>
                        <%}%>
                        &nbsp;&nbsp;
                    </td>
		</tr>

		<tr class="fila">
		<td>
                    &nbsp;&nbsp;Multiservicio&nbsp;:&nbsp;
		</td>
                <td>
                    <input type="text" name="num_osx"  value="<%=num_osxi%>"  onKeyPress="checkEnter(event)" >
		</td>
	</tr>

	<input type="hidden" name="fact_conformada_consultar" value="">

	<tr class="fila">
		<td>
			&nbsp;&nbsp;Id Solicitud&nbsp;:&nbsp;
		</td><td>
			<input type="text" name="id_solici"  value="<%=id_solici%>"  onKeyPress="checkEnter(event)">
			<img src='<%=BASEURL%>/images/botones/iconos/buscar.gif' width="15" height="15" onClick="consultarEstado();" title="Buscar Solicitud" style="cursor:hand" >
		</td>
	</tr>

        <tr class="fila">
		<td>
			&nbsp;&nbsp;Consecutivo&nbsp;:&nbsp;
		</td><td>
			<input type="text" name="id_conse"  value="<%=id_conse%>"  onKeyPress="checkEnter(event)">

		</td>
	</tr>

	<tr class="fila">
		<td>
			&nbsp;&nbsp;Nic&nbsp;:&nbsp;
		</td><td>
			<input type="text" name="nicc"  value="<%=nicc%>"  onKeyPress="checkEnter(event)" >
		</td>
	</tr>
	<tr class="fila">
		<td>
			&nbsp;&nbsp;Nombre Cliente&nbsp;:&nbsp;
		</td><td>
			<!--<input type="text" name="nomclie"  value="<%//=nomclie%>"  onKeyPress="checkEnter(event)" >-->
                        <input name="nomclie" id="nomclie" type="text" value="<%=nomclie%>"  onKeyPress="checkEnter(event)" class="textbox" id="campo" style="width:200;"  onChange="sendAction('<%=CONTROLLEROPAV%>?estado=Negocios&accion=Applus',document.getElementById('nomclie').value,'idclie','');" size="15" >

		</td>
	</tr>
	<tr class="fila">
		<td>&nbsp;

		</td>
                <td>
                    <div id="cliselect"></div>
                    <div id="imgworking" align="right"  style="visibility:hidden"><img src="<%=BASEURL%>/images/cargando.gif"></div>

                    <%if ( (clvsrv.ispermitted(perfil, "5")||clvsrv.ispermitted(perfil, "6")) && !vista.equals("2") ){%>
                        <img src='<%=BASEURL%>/images/botones/iconos/lupa.gif' width="15" height="15" onClick="consultarCliente();" title="Buscar Cliente" style="cursor:hand" >&nbsp;&nbsp;
                    <%}%>
                    <%if ( (clvsrv.ispermitted(perfil, "5")||clvsrv.ispermitted(perfil, "6"))&& !vista.equals("2") ){%>
                        <a target="_blank" href="<%=CONTROLLEROPAV%>?estado=Clientes&accion=Ver&opcion=N">crear cliente</a>
                    <%}%>
                     <img src='<%=BASEURL%>/images/excel.gif' width="15" height="15" title="excel" style="cursor:hand;margin-left: 35px" >&nbsp;&nbsp;
                     <a target="_blank" onClick="exportarExcel()" >Descargar Excel</a>
                </td>
        </tr>
        </table>
        <br>
	<table width="689"  border="2" align="center">
		<tr>
		    <td >
			<table width="100%" border="1" align="center" bordercolor="#999999" bgcolor="#F7F5F4">
                            <tr class="fila">
                                <th nowrap style="font-size:11; font-weight: bold"> <input type='checkbox'  id='All' onClick="Sell_all_col(this.form,'idNegocio');">  </TH>

                                <th> &nbsp;ID SOLICITUD&nbsp;  </th>
                                <%if (vista.equals("2")) {%>
                                <th> &nbsp;FECHA CREACI&Oacute;N&nbsp;  </th>
                                <th> &nbsp;OPD&nbsp;  </th>
                                <%}%>
                                <th> &nbsp;CONSECUTIVO&nbsp;  </th>
                                <th> &nbsp;No. ORDEN DE TRABAJO&nbsp;  </th>
                                <th> &nbsp;ACCION&nbsp;  </th>
                                <th> &nbsp;ESTADO&nbsp;</th>
                                <th> &nbsp;ENTREGA OFERTA&nbsp;</th>
                                <th> &nbsp;ID CLIENTE&nbsp;</th>
                                <th> &nbsp;NIC&nbsp;</th>
                                <th> &nbsp;NIT&nbsp;</th>
                                <th> &nbsp;NOMBRE CLIENTE&nbsp;</th>
                                <th> &nbsp;TIPO CLIENTE&nbsp;</th>
                                 <th> &nbsp;CLIENTE PADRE &nbsp;</th>

                                <th> &nbsp;NOMBRE CONTRATISTA&nbsp;</th>

                                <%if (clvsrv.ispermitted(perfil, "15")){%>
                                    <th> &nbsp;VALOR MATERIAL&nbsp;</th>
                                    <th> &nbsp;VALOR MANO DE OBRA&nbsp;</th>
                                    <th> &nbsp;VALOR OTROS&nbsp;</th>
                                    <th> &nbsp;ADMINISTRACION&nbsp;</th>
                                    <th> &nbsp;IMPREVISTOS&nbsp;</th>
                                    <th> &nbsp;UTILIDAD&nbsp;</th>
                                    <th> &nbsp;PORCENTAJE ADMINISTACION&nbsp;</th>
                                    <th> &nbsp;PORCENTAJE IMPREVISTO&nbsp;</th>
                                    <th> &nbsp;PORCENTAJE UTILIDAD&nbsp;</th>
                                <%}%>

                                <th> &nbsp;COSTO CONTRATISTA&nbsp;</th>

                                <%if (!perfil.equals("contratista")){%>
                                    <th> &nbsp;PRECIO VENTA&nbsp;</th>
                                <%}%>

                                <%if ( (clvsrv.ispermitted(perfil, "50")||clvsrv.ispermitted(perfil, "51")) ){%>
                                <th>&nbsp;FACTURA CONFORMADA&nbsp;</th>
                                <th>&nbsp;FEC FACT CONFORMADA&nbsp;</th><!--20100714-->
                                <th>&nbsp;PREFACTURA&nbsp;</th>
                                <th>&nbsp;FEC CXP CONTRATISTA&nbsp;</th>
                                <th>&nbsp;NC CONTRATISTA&nbsp;</th>

                                <%}%>


                                <th> &nbsp;ACCIONES&nbsp;</th>
                            </tr>
                            <%
                            NegocioApplus negocioApplus;
                            String idsolneg ="";
                            String idaccion ="";
                            String cons ="";

                            int rowcounter = 0;//20100531 rhonalf

                            int estado      = 0;//20100214

                            for (int i=0; i<negocios.size(); i++) {
                                negocioApplus   = (NegocioApplus)negocios.get(i);
                                idsolneg        = negocioApplus.getIdSolicitud();
                                cons            = negocioApplus.getConsecutivo_oferta();
                                idaccion        = negocioApplus.getIdAccion();

                                rowcounter++;//20100531 rhonalf

                                estado          = Integer.parseInt(negocioApplus.getIdEstado());//20100214

                                String valor_negocio=negocioApplus.getVlr();
                                if (valor_negocio==null || valor_negocio.equals("")){
                                    valor_negocio="0";
                                }

                                String value = negocioApplus.getIdSolicitud();
                            %>
                            <tr class='<%= (i%2==0?"filagris":"filaazul") %>' id='fila<%=i%>' style="font-size:12px;">
                                <td class="bordereporte" align="center" style="font-size:11px;" nowrap>
                                    <input type='checkbox' id="id<%=i%>" name='idNegocio' value='<%=value%>' onClick="cambiarColorMouse(fila<%=i%>); seleccionarOrden(formulario,this.value,this.checked);$('id_solicitud').value = <%=value%>"">
                                </td>

                                <td class="bordereporte" align="center" style="font-size:11px;" nowrap>
                                    <a style="font-size: 12px;" href="<%=CONTROLLEROPAV%>?estado=Clientes&accion=Ver&opcion=buscarsolicitud&idsolicitud=<%=negocioApplus.getIdSolicitud()%>" target="_blank">
                                        <%=negocioApplus.getIdSolicitud()%>
                                    </a>
                                    &nbsp;
                                    <select id="select_opt<%=i%>" onChange="openWin(<%=i%>);" style="font-size:11px;">
                                        <option>...</option>
                                       <%if (clvsrv.ispermitted(perfil, "25") && modelopav.ElectricaribeOfertaSvc.isOfferReady(negocioApplus.getIdSolicitud())&&estado>10 && !vista.equals("2")){%><!--si se tiene el PERMISO PARA GENERAR OFERTA y NO EXISTE UNA ACCION  PARA ESA SOLICITUD QUE TENGA ESTADO MENOR QUE PENDIENTE POR GENERAR OFERTA O MAYOR QUE EN EJECUCION-->
                                        <option value="1;<%=idsolneg%>">GENERAR OFERTA</option>
                                        <%}%>
                                        <%if (clvsrv.ispermitted(perfil, "22") && modelopav.ElectricaribeOfertaSvc.minEstado(idsolneg)>=30&&modelopav.ElectricaribeOfertaSvc.maxEstado(idsolneg)<=60  && !vista.equals("2")){%><!--si se tiene el PERMISO PARA REEVALUAR OFERTA y NO EXISTE UNA ACCION  PARA ESA SOLICITUD QUE TENGA ESTADO MENOR QUE PENDIENTE POR HACER COTIZACION O MAYOR QUE PENDIENTE POR ACEPTACION DEL CLIENTE-->
                                        <option value="8;<%=idsolneg%>">REEVALUAR OFERTA</option>
                                        <%}%>
                                        <%if ((clvsrv.ispermitted(perfil, "26") && modelopav.ElectricaribeOtSvc.isPaymentReady(idsolneg) && estado>=60 && estado!=667) && (!vista.equals("2")||(vista.equals("2")&&modelopav.ElectricaribeOfertaSvc.maxEstado(idsolneg)>=80)))
                                        {%><!--si se tiene el PERMISO PARA ACEPTAR PAGOS y  NO EXISTE UNA ACCION CON ESTADO MENOR QUE "PENDIENTE POR ACEPTACION DEL CLIENTE" O MAYOR QUE "EN EJECUCION" O (ES MAYOR QUE EN EJECUCION Y MENOR QUE "INGRESADO EN OPEN" Y SE TIENE PERMISO ESPECIAL)-->
                                        <option value="2;<%=negocioApplus.getIdCliente()%>;<%=idsolneg%>;<%=estado%>">ACEPTAR PAGOS</option>
                                        <%}%>
                                        <%if (clvsrv.ispermitted(perfil, "27") && ((modelopav.ElectricaribeOtSvc.isOtReady(idsolneg)&&!vista.equals("2")) ||(modelopav.ElectricaribeOtSvc.isPaymentReady(idsolneg)&&vista.equals("2"))|| clvsrv.ispermitted(perfil, "56"))&&estado>10){%><!--si tiene PERMISO PARA GENERAR OT Y NO EXISTE UNA ACCION PARA ESA SOLICITUD ANTES DE "PENDIENTE POR EJECUCION" NI DESPUES DE EN EJECUCION-->
                                        <option value="3;<%=negocioApplus.getIdCliente()%>;<%=idsolneg%>;<%=cons%>;<%=negocioApplus.getIdEstado()%>;<%=negocioApplus.getNumOs()%>">GENERAR OT</option>
                                        <%}%>
                                        <%if ((clvsrv.ispermitted(perfil, "9") && estado<=80 && estado>60) || ( (estado>80) && (estado<110) && (clvsrv.ispermitted(perfil, "20")) )){%><!--SI TIENE PERMISO PARA PONER FECHA DE FINANCIACION Y ESTADO MENOR O IGUAL QUE EN EJECUCION O (EL ESTADO ES MAYOR QUE EN EJECUCION Y MENOR QUE INGRESADO EN OPEN Y TIENE PERMISO ESPECIAL-->
                                        <option value="4;<%=idsolneg%>">FINANCIACION</option>
                                        <%}%>
                                        <%if (clvsrv.ispermitted(perfil, "19") && estado>=80 && estado!=667) {%><!--SI TIENE PERMISO PARA RECIBIR OBRA Y ESTADO ES EJECUCION o mayor -->
                                        <option value="9;<%=idsolneg%>">DILIGENCIAR ENCUESTA</option>
                                        <option value="5;<%=idsolneg%>">RECEPCION OBRA</option>
                                        <%}%>
                                        <% if(clvsrv.ispermitted(perfil,"18") && estado==110) { %>
                                        <option value="6;<%=idsolneg%>">NOTAS OPEN</option>
                                        <% } %>
                                        <%if (clvsrv.ispermitted(perfil, "32") && estado<=100) {%><!--SI TIENE PERMISO PARA CAMBIAR CLIENT Y ESTADO MENOR O IGUAL QUE INGRESAR EN OPEN -->
                                        <option value="7;<%=idsolneg%>;<%=estado%>">CAMBIAR CLIENTE</option>
                                        <%}%>
                                </select>

                                </td>
                                <%if (vista.equals("2")) {%>
                                    <td class="bordereporte" style="font-size:11px;" nowrap align="center"> <%= negocioApplus.getFecha()%></td>
                                    <td class="bordereporte" style="font-size:11px;" nowrap align="center"> <%= negocioApplus.getAviso()%></td>
                                <%}%>

                                <td class="bordereporte" style="font-size:11px;" nowrap align="center"> <%= negocioApplus.getConsecutivo_oferta()%></td>
                                <td class="bordereporte" style="font-size:11px;" nowrap align="center"> <%= negocioApplus.getNumOs()%></td>

                                <td class="bordereporte" nowrap align="center">
                                    <input type='checkbox' id="idacc<%=i%>" name='idAcc' title="s<%=negocioApplus.getIdSolicitud()%>" value='<%=negocioApplus.getIdAccion()%>' onClick=" seleccionarOrdenDeAccion(formulario,<%=negocioApplus.getIdSolicitud()%>,this.checked);">
                                    <%= negocioApplus.getIdAccion()%>
									<!-- opcion adicional-->
<%if (clvsrv.ispermitted(perfil,"35")) {%>

                                                    <img src='<%=BASEURL%>/images/botones/iconos/iconsubir.gif'        width="21" height="21" onClick="formulario.action='<%=BASEURL%>/jsp/adminfile/importar.jsp?num_osx=<%=negocioApplus.getIdAccion()%>&tipito=id_accion&swSubir=1';formulario.target='_blank';formulario.submit();" title="Subir Archivo" style="cursor:hand" >
<% }else{ %>

<% if(clvsrv.ispermitted(perfil,"36")){ %>
                                                    <img src='<%=BASEURL%>/images/botones/iconos/icondes.gif'        width="16" height="20" onClick="formulario.action='<%=BASEURL%>/jsp/adminfile/importar.jsp?num_osx=<%=negocioApplus.getIdAccion()%>&tipito=id_accion&swSubir=0';formulario.target='_blank';formulario.submit();" title="Mostrar Archivo" style="cursor:hand" >

<%} }%>

<!-- opcion adicional-->

                                </td>

                                <td class="bordereporte" style="font-size:11px;" nowrap align="center"> <%= negocioApplus.getEstado()%></td>
                                <td class="bordereporte" style="font-size:11px;" nowrap align="center" title="<%= negocioApplus.getUsuarioEntregaOferta()+"_"+negocioApplus.getCreacionFechaEntregaOferta()%>"> <%=  negocioApplus.getFechaEntregaOferta()%></td>
                                <td class="bordereporte" style="font-size:11px;" nowrap align="center"> <%= negocioApplus.getIdCliente()%></td>
                                <td class="bordereporte" style="font-size:11px;" nowrap align="center"> <%= negocioApplus.getNicClient()%></td>
                                <td class="bordereporte" style="font-size:11px;" nowrap align="center"> <%= negocioApplus.getNitClient()%></td>
                                <td class="bordereporte" style="font-size:11px;" nowrap align="center">   <%= negocioApplus.getNombreCliente()%></td>
                                <td class="bordereporte" style="font-size:11px;" nowrap align="center"> <%= negocioApplus.getTipoCliente()%></td>
                                <td class="bordereporte" style="font-size:11px;" nowrap align="center"> <%= modelopav.NegociosApplusService.getNomClientepadre(negocioApplus.getIdCliente())%></td>
                                <td class="bordereporte" style="font-size:11px;" nowrap align="center">   <%= negocioApplus.getNombreContratista()%></td>

                                <%if (clvsrv.ispermitted(perfil, "15")){%>
                                    <td class="bordereporte" style="font-size:11px;" align="center"> <%=Util.customFormat(Double.parseDouble(negocioApplus.getMaterial()))%> </td>
                                    <td class="bordereporte" style="font-size:11px;" align="center"> <%=Util.customFormat(Double.parseDouble(negocioApplus.getMano_obra()))%> </td>
                                    <td class="bordereporte" style="font-size:11px;" align="center"> <%=Util.customFormat(Double.parseDouble(negocioApplus.getOtros()))%> </td>
                                    <td class="bordereporte" style="font-size:11px;" align="center"> <%=Util.customFormat(Double.parseDouble(negocioApplus.getAdministracion()))%> </td>
                                    <td class="bordereporte" style="font-size:11px;" align="center"> <%=Util.customFormat(Double.parseDouble(negocioApplus.getImprevisto()))%> </td>
                                    <td class="bordereporte" style="font-size:11px;" align="center"> <%=Util.customFormat(Double.parseDouble(negocioApplus.getUtilidad()))%> </td>
                                    <td class="bordereporte" style="font-size:11px;" align="center"> <%=negocioApplus.getPorc_a()%> </td>
                                    <td class="bordereporte" style="font-size:11px;" align="center"> <%=negocioApplus.getPorc_i()%> </td>
                                    <td class="bordereporte" style="font-size:11px;" align="center"> <%=negocioApplus.getPorc_u()%> </td>
                                    <%
                                 
                                       totalValMaterial =  totalValMaterial+(Double.parseDouble(negocioApplus.getMaterial()));
                                       totalValManoObra = totalValManoObra +(Double.parseDouble(negocioApplus.getMano_obra()));
                                       totalValOtros = totalValOtros +(Double.parseDouble(negocioApplus.getOtros()));
                                       totalValAdministracion =totalValAdministracion+(Double.parseDouble(negocioApplus.getAdministracion()));
                                       totalValImprevistos =totalValImprevistos+(Double.parseDouble(negocioApplus.getImprevisto()));
                                       totalValUtilidad =totalValUtilidad+(Double.parseDouble(negocioApplus.getUtilidad()));

                                       totalValCostoContra =totalValCostoContra+(Double.parseDouble(negocioApplus.getVlr()));
                                       totalValPrecioVenta =totalValPrecioVenta+(Double.parseDouble(negocioApplus.getEcaOferta()));
                                    %>
                                <%}%>

                                <td class="bordereporte" style="font-size:11px;" nowrap align="center"> <%= Util.customFormat(Double.parseDouble(negocioApplus.getVlr()))%></td>

                                <%if (!perfil.equals("contratista")){%>
                                    <td class="bordereporte" style="font-size:11px;" nowrap align="center" >
                                        <a onClick="sumarOferta(<%=value%>);" style="cursor:hand" title="sumar">
                                            <%= Util.customFormat(Double.parseDouble(negocioApplus.getEcaOferta()))%>
                                        </a>
                                    </td>
                                <%}%>

                                <input type="hidden" name="<%=value%>" value="<%=Util.customFormat(Double.parseDouble(negocioApplus.getEcaOferta()))%>">

                                <%if ( (clvsrv.ispermitted(perfil, "50")||clvsrv.ispermitted(perfil, "51")) ){%>
		                        <td class="bordereporte" style="font-size:11px;" nowrap align="center">
		                              <% out.print(negocioApplus.getFacturaConformada());%>
		                        </td>
                                        <td class="bordereporte" style="font-size:11px;" nowrap align="center"><!--20100714-->
		                              <% out.print(negocioApplus.getFecFactConformed());%><!--20100714-->
		                        </td><!--20100714-->
                                        <td class="bordereporte" style="font-size:11px;" nowrap align="center">
                                              <% out.print(negocioApplus.getPrefactura());//20100618%>
                                        </td>
                                        <td class="bordereporte" style="font-size:11px;" nowrap align="center">
                                              <% out.print(negocioApplus.getFecFacContratistaFin());//20100618%>
                                        </td>
                                        <td class="bordereporte" style="font-size:11px;" nowrap align="center">
                                              <% out.print(negocioApplus.getNotaCredContratista());//20100618%>
                                        </td>
                                <%}%>


                                        <td class="bordereporte" style="font-size:11px;" nowrap align="left">
                                            <%
                                            String temx11=negocioApplus.getAcciones();
                                            temx11=temx11.replaceAll("\r\n","  ");
                                            //temx11=temx11.replaceAll("\r"," ");
                                            temx11=temx11.replaceAll("\n","  ");
                                            temx11=temx11.replaceAll("\"","?");
                                            //System.out.println("temx11:"+temx11);
                                            //String lineSep=System.getProperty("line.separator");
                                            //temx11=temx11.replaceAll(lineSep," ");
                                            //temx11=Util.RegexUtil.htmlToPlainText(temx11);
                                            %>
                                            <img src='<%=BASEURL%>/images/botones/iconos/notas.gif' width="15" height="15"  onclick="mostrarInfoX('<%=temx11%>');" style="cursor:hand" title="expandir" ><!--//20101112-->

                                            <%if (negocioApplus.getAcciones()!=null && negocioApplus.getAcciones().length()>40){//20101113%>
                                                <a target="_blank" href="<%=CONTROLLEROPAV%>?estado=Negocios&accion=Applus&opcion=consultarAlcances&auxiliar=on&id_solici=<%=  negocioApplus.getIdSolicitud()%>&id_accion=<%=negocioApplus.getIdAccion()%>"><%=  negocioApplus.getAcciones().substring(0,30)%>...</a>
                                            <%}else{%>
                                                <a target="_blank" href="<%=CONTROLLEROPAV%>?estado=Negocios&accion=Applus&opcion=consultarAlcances&auxiliar=on&id_solici=<%=  negocioApplus.getIdSolicitud()%>&id_accion=<%=negocioApplus.getIdAccion()%>"><%=  negocioApplus.getAcciones()%></a>
                                            <%}%>
                                        </td>

                            </tr>
                     

			<% } %>
                        <tr>
                            <td colspan="14">
                                <label style="font-family: Tahoma,Arial;font-size: 12px;font-weight: bold;margin-left: 1404px;">TOTALES</label> 
                            </td>
                            <td nowrap align="center"> <label style="font-size: 11px;"> <%= Util.customFormat(Math.rint(totalValMaterial))%> </label></td>
                            <td nowrap align="center"> <label style="font-size: 11px;"> <%= Util.customFormat(Math.rint(totalValManoObra))%> </label></td>
                            <td nowrap align="center"> <label style="font-size: 11px;"> <%= Util.customFormat(Math.rint(totalValOtros))%> </label></td>
                            <td nowrap align="center"> <label style="font-size: 11px;"> <%= Util.customFormat(Math.rint(totalValAdministracion))%> </label></td>
                            <td nowrap align="center"> <label style="font-size: 11px;"> <%= Util.customFormat(Math.rint(totalValImprevistos))%> </label></td>
                            <td nowrap align="center"> <label style="font-size: 11px;"> <%= Util.customFormat(Math.rint(totalValUtilidad))%> </label></td>
                            <td nowrap align="center"></td>
                            <td></td>
                            <td></td>
                            <td nowrap align="center"> <label style="font-size: 11px;"> <%= Util.customFormat(Math.rint(totalValCostoContra))%> </label></td>
                            <td nowrap align="center"> <label style="font-size: 11px;"> <%= Util.customFormat(Math.rint(totalValPrecioVenta))%> </label></td>
                            <td colspan="6"></td>
                        </tr>
			</table>
		    </td>
		</tr>
	</table>
	<br>
	<center>
	<br>
	<table>
	<%
	boolean sw_modificador=false;
	%>
	</div>
	</table>
	<%
	String hoy = Utility.getHoy("-");
	boolean poner_fecha_entrega_oferta=false;
	if (negocios!=null && negocios.size()>0 && estadi.equals("050")){
		poner_fecha_entrega_oferta=true;
	}
	if (poner_fecha_entrega_oferta){
	%>

	 <table align="center">
		<tr  class="fila">
			<td>&nbsp;Fecha de entrega de oferta :&nbsp;</td>
			<td >
				<input name="fecof" type="text" class="textbox" id="fecof" size="10" value="<%=hoy%>" >
				<span class="Letras"><img src="<%=BASEURL%>/images/cal.gif" width="16" height="16" align="absmiddle" style="cursor:hand " onClick="if(self.gfPop)gfPop.fPopCalendar(document.formulario.fecof);return false;" HIDEFOCUS></span>

			</td>
                        <td>
                            &nbsp;&nbsp;<img src="<%=BASEURL%>/images/botones/aceptar.gif" height="21" title='Aceptar' onclick='asignar(formulario)' onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" style="cursor:hand">
                        </td>
		</tr>
	 </table>
	<%}%>
        <% if (clvsrv.ispermitted(perfil, "29") && modelopav.ElectricaribeOfertaSvc.minEstado(idsolneg) >= 20 && modelopav.ElectricaribeOfertaSvc.maxEstado(idsolneg) <= 80) {
        %>
        <table align="center">
		<tr  class="fila">
			<td>&nbsp;Denegar Ofertas  &nbsp;</td>

                        <td>
                            &nbsp;&nbsp;<img src="<%=BASEURL%>/images/botones/aceptar.gif" height="21" title='Aceptar' onclick='denegar(formulario)' onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" style="cursor:hand">
                        </td>
		</tr>
	 </table>
        <%   }
        %>
        <%if (negocios!=null && negocios.size()>0 && estadi.equals("090") && clvsrv.ispermitted(perfil, "30")){//20100305%>
            <table>
            <tr class="fila">
                <td>
                    &nbsp;&nbsp;Estado asignable :&nbsp;&nbsp;
                </td>
                <td>
                    <select name="estado_asignable" >
                    <%
                        for (int i=0;i<listaEstados.size();i++){
                            String[] estadinho=(String[])listaEstados.get(i);
                            if (estadinho[0].equals("100")){
                                %>
                                <option value="<%=estadinho[0]%>"><%=estadinho[1]%></option>
                                <%
                            }
                        }
                    %>
                    </select>
                    &nbsp;&nbsp;
                </td>
                <td align="center" colspan="2">
                    &nbsp;&nbsp;<img src="<%=BASEURL%>/images/botones/aceptar.gif" height="21" title='Asignar Estado' onclick='asignarEstado(formulario)' onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" style="cursor:hand">
                <td>
            </tr>
            </table>
        <%}//20100305%>
	<br>

        <%if (clvsrv.ispermitted(perfil, "11")){%>
        <table>
            <tr class="fila">
                <td>
                    &nbsp;&nbsp;Estado asignable :&nbsp;&nbsp;
                </td>
                <td>
                    <select name="estado_asignable" >
                    <%
                        for (int i=0;i<listaEstados.size();i++){
                            String[] estadinho=(String[])listaEstados.get(i);
                            %>
                            <option value="<%=estadinho[0]%>"><%=estadinho[1]%></option>
                            <%
                        }
                    %>
                    </select>
                    &nbsp;&nbsp;
                </td>
                <td align="center" colspan="2">
                    &nbsp;&nbsp;<img src="<%=BASEURL%>/images/botones/aceptar.gif" height="21" title='Asignar Estado' onclick='asignarEstado(formulario)' onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" style="cursor:hand">
                <td>
            </tr>
        </table>

        <br>
        <%}%>

        <%
            if ( clvsrv.ispermitted(perfil, "50") ){
        %>
        <table style="border-collapse:collapse;">
            <tr class="fila">
                <td>Factura Conformada&nbsp;</td>
                <td> <input type="text" id="fact_conf" name="fact_conf" value="" size="40" maxlength="40">&nbsp;Fecha&nbsp;</td>
                <td width="66" valign="top"><input id="calendar-fieldx" name="fecfactconf" size="10" value="<%=Util.getFechaActual_String(4)%>"  readonly></td><!--20100824-->
                <td width="29" valign="top"><button id="calendar-triggerx" type="button" >...</button></td><!--20100824-->
                <td>
                    <img alt="conformar factura" src="<%=BASEURL%>/images/botones/aceptar.gif" height="21" title='Agregar factura' onclick='factConf(<%=rowcounter%>);' onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" style="cursor:pointer">
                </td>
            </tr>
        </table>
        <div id="divcarga"></div>
        <br>
        <%
            }
        %>


	<img src='<%=BASEURL%>/images/botones/salir.gif'      style='cursor:hand'    title='Salir...'      name='i_salir'       onclick='parent.close();'           onMouseOver='botonOver(this);' onMouseOut='botonOut(this);'>
	<br><br><br>


	</center>
	<br>

    	<%
	if (respuesta!=null){//request.getParameter("resultado")!=null && request.getParameter("resultado").equals("ok") ){
		%>
		<br><br>
		<p><table border="2" align="center">
		  <tr>
			<td><table width="100%"  border="1" align="center"  bordercolor="#F7F5F4" bgcolor="#FFFFFF">
			  <tr>
				<td width="170" align="center" class="mensajes"><%=""+respuesta%></td>
				<td width="29" background="<%=BASEURL%>/images/cuadronaranja.JPG">&nbsp;</td>
				<td width="40">&nbsp;</td>
			  </tr>
			</table></td>
		  </tr>
		</table>
		</p>

	<% }%>
 </form>
 <br>
 <% }else{
 	%>lista de negocios vacio. raro...<%
 }	%>
</div>
<%=datos[1]%>
	<iframe width=188 height=166 name="gToday:normal:agenda.js:gfPop:plugins.js" id="gToday:normal:agenda.js:gfPop:plugins.js" src="<%=BASEURL%>/js/Calendario/ipopeng.htm" scrolling="no" frameborder="0" style="visibility:visible; z-index:999; position:absolute; left:-500px; top:0px;"> </iframe>
</body>
<%}catch(Exception eeee){
	System.out.println("error:::" +eeee.toString());
}
%>

    <!--20100824-->
    <script>
            Calendar.setup({
                inputField : "calendar-fieldx",
                trigger    : "calendar-triggerx",
                onSelect   : function() { this.hide() ; }
            });//20100824
    </script>
    
    
   
    <div id="divSalidaEx" title="Exportacion" style=" display: block" >
        <p  id="msjEx" style=" display:  none"> Espere un momento por favor...</p>
        <center>
            <img id="imgloadEx" style="position: relative;  top: 7px; display: none " src="./images/cargandoCM.gif"/>
        </center>
        <div id="respEx" style=" display: none"></div>
    </div> 

    
    
</html>