<%@page contentType="text/html"%>
<%@page session="true"%>
<%@page import="java.util.*"%>
<%@page import="com.tsp.opav.model.*"%>
<%@page import="com.tsp.opav.model.beans.*"%>
<%@page import="com.tsp.opav.model.services.*"%>
<%@page import="com.tsp.util.*"%>
<%@ taglib uri="http://jakarta.apache.org/taglibs/input-0.90" prefix="input" %>
<%@include file="/WEB-INF/InitModel.jsp"%>
<script src="<%=BASEURL%>/js/prototype.js"        type="text/javascript"></script>
<script src="<%=BASEURL%>/js/categoria.js"        type="text/javascript"></script>
<link href="<%= BASEURL%>/css/estilostsp.css" rel="stylesheet" type="text/css">
<link href="<%=BASEURL%>/css/default.css"       rel="stylesheet" type="text/css">
<link href="<%=BASEURL%>/css/alert.css" rel="stylesheet" type="text/css"/>
<link href="<%=BASEURL%>/css/alphacube.css" rel="stylesheet" type="text/css"/>
<script type="text/javascript" src="<%=BASEURL%>/js/buscarproveedor.js"></script>
<script type='text/javascript' src="<%= BASEURL%>/js/boton.js"></script>
<script type='text/javascript' src="<%=BASEURL%>/js/Validacion.js"></script>

<%@page contentType="text/html"  pageEncoding="UTF-8"%>
<link href="<%=BASEURL%>/css/mac_os_x.css"      rel="stylesheet" type="text/css">


<script type='text/javascript' src="<%= BASEURL%>/js/general.js"></script>
<script src='<%=BASEURL%>/js/tools.js' language='javascript'></script>
<script src='<%=BASEURL%>/js/date-picker.js'></script>
<script src="<%= BASEURL%>/js/transferencias.js"></script>

<script type='text/javascript' src="<%= BASEURL%>/js/prototype.js"></script>
<script type='text/javascript' src="<%= BASEURL%>/js/interventor.js"></script>
<script type='text/javascript' src="<%= BASEURL%>/js/scriptaculous/scriptaculous.js"></script>
<script type='text/javascript' src="<%= BASEURL%>/js/autosuggest.js"></script>
<script type='text/javascript' src="<%= BASEURL%>/js/scriptaculous/controls.js"></script>
<script type='text/javascript' src="<%= BASEURL%>/js/scriptaculous/builder.js"></script>
<script type="text/javascript" src="<%=BASEURL%>/js/effects.js"        ></script>
<script type="text/javascript"src="<%=BASEURL%>/js/window.js"        ></script>
<script type="text/javascript"src="<%=BASEURL%>/js/window_effects.js" ></script>
<link href="<%=BASEURL%>/css/default.css"       rel="stylesheet" type="text/css">
<link href="<%=BASEURL%>/css/alert.css" rel="stylesheet" type="text/css"/>
<link href="<%=BASEURL%>/css/alphacube.css" rel="stylesheet" type="text/css"/>



<%

String cod_inter = request.getParameter("cod_inter") == null ? "0" : request.getParameter("cod_inter");
String msj = request.getParameter("msj") == null ? "" : request.getParameter("msj");
System.out.println("cod_inter____________"+cod_inter);


%>

<script>
 function color_fila(id,color){
   
                    $(id).style.backgroundColor=color;
                }



</script>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=windows-1252" />
<title>Asignacion de Departamentos a Un Interventor</title>
</head>
    <body  onload="" >
		<div id="capaSuperior" style="position:absolute; width:100%; height:100px; z-index:0; left: 0px; top: 0px;">
		<jsp:include page="/WEB-INF/toptsp.jsp?encabezado=Asignacion de Departamentos a Un Interventor"/>
		</div>
		<div id="formulario" style="position:absolute; width:100%; height:100%; z-index:0; left: 0px; top: 100px; ">
		

  <table width="450" border="2"align="center">
    <tr>
      <td width="100%">
	  <table width="100%" align="center" >
        <tr>
          <td  align="left" class="subtitulo1">Asignar Interventor a Departamentos </td>
          <td colspan="2"  align="left" class="barratitulo"><img src="<%=BASEURL%>/images/titulo.gif"></td>
          </tr>
		  <tr>
          <td colspan="3"  align="left" class="letra">
		    <div align="center">Interventor</div></td>
          </tr>
		  
		  <tr>
          <td colspan="3"  align="left" class="letra"><div align="center">
            <label>
            <select name="interventor" id="interventor" onChange="buscarDepartamentos('<%=CONTROLLEROPAV%>',this.value, 'listadepartamentos', 'dptos');">
                <option value="0" selected="selected">Seleccione un Interventor</option>
			
            </select>
            </label>
          </div></td>
          </tr>
		  
		<tr>
          <td width="207" align="left" class="letra"><div align="center">Departamentos</div></td>
          <td width="36" valign="middle" class="letra" ><div align="center"></div></td>

		  <td width="179" valign="middle" class="letra" ><div align="center">Asignados</div></td>
		</tr>
		<tr>
		  <td align="left" valign="top" class="fila" ><div align="center"><span class="bordereporte">
		    <select name="dptos"  id="dptos" size="8" multiple="multiple" class="textbox" style="width:221"  >
		      </select>
		    </span></div></td>
		  <td valign="middle" class="letra" ><span class="bordereporte fila">
                          <img src='<%=BASEURL%>/images/botones/envIzq.gif' name='imgEnvIzq' id="imgEnvIzq" style='cursor:hand'  onclick="pasarDerIzq(); " onmouseover='botonOver(this);' onmouseout='botonOut(this);' />
                          <img src='<%=BASEURL%>/images/botones/envDer.gif' style='cursor:hand'  name='imgEnvDer'  onclick="pasarIzqDer(); " onMouseOver='botonOver(this);' onMouseOut='botonOut(this);'>
                          <br></span></td>
		  <td valign="middle" class="letra" ><div align="center"><span class="bordereporte">
		    <select name="dpto_asignados" id="dpto_asignados" size="8" multiple="multiple" class="textbox" style="width:221">
		      </select>
		    </span></div></td>
		  </tr>
      </table>	  
	  </td>
    </tr>
  </table>
  <div align="center">
    <br>
    <img src="<%= BASEURL%>/images/botones/aceptar.gif" name="imgaceptar" onClick="saveSels('<%=CONTROLLEROPAV%>','<%=BASEURL%>' );" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" style="cursor:pointer ">&nbsp;
    
        <img src="<%= BASEURL%>/images/botones/salir.gif" name="imgsalir" onMouseOver="botonOver(this);" onClick="parent.close();" onMouseOut="botonOut(this);" style="cursor:pointer ">  </div>
		</div>
		

</body>
</html>


<script>
     buscarInterventores('<%=CONTROLLEROPAV%>', '<%=cod_inter%>');
</script>






