<%-- 
    Document   : categoriasAdmon
    Created on : 2/08/2016, 09:13:13 AM
    Author     : mcastillo
--%>

<%@page contentType="text/html" pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
        <title>Categorķas de la A</title>
        
        <link type="text/css" rel="stylesheet" href="./css/jquery/jquery-ui/jquery-ui.css" />
        <link href="./css/popup.css" rel="stylesheet" type="text/css">

        <script type="text/javascript" src="./js/jquery/jquery.jqGrid/js/jquery.min.js"></script> 
        <script type="text/javascript" src="./js/jquery-ui-1.8.5.custom.min.js"></script>
        <link type="text/css" rel="stylesheet" href="./css/buttons/botonVerde.css"/>
        <link href="./css/style_azul.css" rel="stylesheet" type="text/css">
        <script src="./js/categoriasAdmon.js" type="text/javascript"></script>     
         
        <!--jqgrid--> 
        <script type="text/javascript" src="./js/jquery/jquery.jqGrid/js/grid.locale-es.js"></script>
        <script type="text/javascript" src="./js/jquery/jquery.jqGrid/js/jquery.jqGrid.min.js"></script>
    </head>
    <body>
         <div id="capaSuperior"  style="position:absolute; width:100%; height:100px; z-index:0; left: 0px; top: 0px;">
            <jsp:include page="/WEB-INF/toptsp.jsp?encabezado=Categorķas de la A"/>
        </div>
        <div id="capaCentral" style="position:absolute; width:100%; height:100%; z-index:0; left: 0px; top: 100px; ">
            <center>
                <br>
                <table id="tabla_categorias"></table>
                <div id="page_tabla_categorias"></div>    
            </center>
        </div>
        <div id="div_categoria"  style="display: none; width: 800px" >                       
            <div id="filtros" class="ui-jqgrid ui-widget ui-widget-content ui-corner-all" style="height: 110px;">
                </br>
                <table aling="center" style=" width: 100%" >
                    <tr>
                    <input type="hidden" id="idCategoria" name="idMeta">     
                    <td style="width: 10%"><span>Nombre</span><span style="color: red; ">*</span></td>                          
                    <td style="width: 50%"><input type="text" id="nomcategoria" class="mayuscula" name="nomcategoria" style=" width: 248px" onblur="igualarcampo('desccategoria', 'nomcategoria');">                   
                    </td>  
                    </tr> 
                    <tr>
                        <td style="width: 10%"><span>Descripcion:</span><span style="color: red; ">*</span></td>   
                        <td style="width: 90%" colspan="4"><textarea id ="desccategoria" name="desccategoria" style="width:535px;resize:none" rows="2" ></textarea></td>                        
                    </tr>
                </table>
            </div>  
            </br> 
        </div>
        <div id="div_editar_categoria"  style="display: none; width: 800px" >
            <div id="filtros" class="ui-jqgrid ui-widget ui-widget-content ui-corner-all" style="height: 145px;padding: 0px 10px 5px 10px">
                </br>
                <table aling="center" style=" width: 100%" >
                    <tr>                 
                    <input type="hidden" id="idCategoriaEdit" name="idCategoriaEdit">                     
                    <td style="width: 10%"><span>Nombre</span><span style="color: red; ">*</span></td>                          
                    <td style="width: 50%"><input type="text" id="nomcategoriaEdit" class="mayuscula" name="nomcategoriaEdit" style=" width: 250px" ></td>
                    </tr>
                    <tr>
                        <td style="width: 10%"><span>Descripcion:</span><span style="color: red; ">*</span></td>   
                        <td style="width: 90%" colspan="4"><textarea id ="desccategoriaEdit" name="desccategoriaEdit" style="width:525px;resize:none"  rows="2" ></textarea></td>                        
                    </tr>
                    <tr>
                        <td style="width: 100%"><div id="boton_guardar" style="position:absolute; left: 83%; top: 80%; "> 
                                <span aling="center"  class="form-submit-button form-submit-button-simple_green_apple" onClick="actualizarCategoria();"> Actualizar </span>
                            </div></td>
                    </tr>
                </table>
            </div>  
            </br>  
            <table border="0"  align="center">
                <tr>
                    <td>
                        <table id="tabla_items_categoria" ></table>
                        <div id="page_tabla_items_categoria"></div>
                    </td>
                </tr>
            </table>
        </div> 
        <div id="div_items_categoria" style="display: none; width: 800px" >                

            <div id="filtros" class="ui-jqgrid ui-widget ui-widget-content ui-corner-all" style="height: 110px;padding: 0px 10px 5px 10px">
                </br>
                <table aling="center" style=" width: 100%" >
                    <tr>               
                        <input type="hidden" id="idItem" name="idItem"> 
                        <td style="width: 10%"><span>Nombre</span><span style="color: red; ">*</span></td>
                        <td style="width: 90%" colspan="3"><input type="text" id="nomItem" name="nomItem" class="mayuscula" style=" width: 350px"/> </td>
                    </tr>
                    <tr>
                        <td style="width: 10%"><label> Valor<span style="color:red;">*</span></label></td>
                        <td  style="width: 30%"><input type="text"  id="valor_item" style="width: 150px" class="solo-numeric"/></td> 
                        <td style="width: 20%"><span>Por Defecto</span></td>   
                        <td style="width: 30%;padding:10px">
                            <input type="radio" name="default" value="S">Si
                            <input type="radio" name="default" value="N" checked> No
                        </td>      
                    </tr>
                </table>
            </div>   
            </br> 
        </div>   
        
        <div id="dialogLoading" style="display:none;">
            <p  style="font-size: 12px;text-align:justify;" id="msj2">Texto </p> <br/>
            <center>
                <img src="./images/cargandoCM.gif"/>
            </center>
        </div>
        <div id="dialogMsj" title="Mensaje" style="display:none;">
            <p style="font-size: 12px;text-align:justify;" id="msj" > Texto </p>
        </div> 
    </body>
</html>
