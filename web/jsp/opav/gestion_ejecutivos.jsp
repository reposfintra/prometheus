<%-- 
    Document   : gestion_ejecutivos
    Created on : 7/04/2010, 10:37:58 AM
    Author     : rhonalf
--%>

<%@ page session   ="true"%>
<%@ page errorPage ="/error/ErrorPage.jsp"%>
<%@ page import    ="java.util.*"%>
<%@ page import    ="com.tsp.opav.model.*"%>
<%@ page import    ="com.tsp.opav.model.beans.*"%>
<%@ page import    ="com.tsp.opav.model.services.*"%>
<%@ page import    ="com.tsp.util.*"%>
<%@ page import    ="com.tsp.util.Util.*"%>
<%@ include file   ="/WEB-INF/InitModel.jsp"%>
<%@taglib uri="http://jakarta.apache.org/taglibs/input-0.90" prefix="input" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
   "http://www.w3.org/TR/html4/loose.dtd">
<!-- This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>. -->

<html>
    <head>
        <link href="<%= BASEURL %>/css/estilostsp.css" rel="stylesheet" type="text/css">
        <script type='text/javascript' src="<%= BASEURL %>/js/boton.js"></script>
        <script type='text/javascript' src="<%= BASEURL %>/js/prototype.js"></script>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Gestion de ejecutivos</title>
        <script type="text/javascript">
            var CONTROLLEROPAV = '<%= CONTROLLEROPAV %>';
            function buscar(){
                var cad = document.getElementById("cadena").value;
                var ind = document.getElementById("param").selectedIndex;
                var par = document.getElementById("param").options[ind].value;
                var url = document.getElementById("forma").action;
                //alert('cadena '+cad+' param '+par);
                var p =  'opcion=search&param='+par+'&cadena='+cad;
                new Ajax.Request(
                    url,
                    {
                        method: 'get',
                        parameters: p,
                        onLoading: loading,
                        onComplete: llenarDiv
                    });
            }

            function loading(){
                document.getElementById("busqueda").innerHTML ='<span class="letra">Buscando </span><img alt="cargando" src="<%= BASEURL%>/images/cargando.gif"  name="imgload">';
            }
            
            function llenarDiv(response){
                document.getElementById("busqueda").innerHTML = response.responseText;
            }

            function buscarDatos(){
                var idx = document.getElementById("codigo").value;
                var url = document.getElementById("forma").action;
                var ps = 'opcion=listar&idx='+idx;
                //alert('ps '+ ps + ' idx '+ idx);
                new Ajax.Request(
                    url,
                    {
                        method: 'get',
                        parameters: ps,
                        onComplete: llenarForm
                    });
            }

            function llenarForm(response){
                //alert(response.responseText);
                var array = response.responseText.split(';_;');
                var tam = array.length;
                if(tam>0){
                    document.getElementById("nit").value = array[0];
                    document.getElementById("nombre").value = array[1];
                    document.getElementById("direccion").value = array[2];
                    document.getElementById("telefono").value = array[3];
                    document.getElementById("ciudad").value = array[4];
                    document.getElementById("departamento").value = array[5];
                    document.getElementById("email").value = array[6];
                    document.getElementById("cargo").value = array[7];
                    document.getElementById("mercado").value = array[8];
                }
                else{
                    alert('No se encontraron datos');
                }
                
            }

            function limpiarForm(){
                document.getElementById("codigo").value = '';
                document.getElementById("nit").value = '';
                document.getElementById("nombre").value = '';
                document.getElementById("direccion").value ='';
                document.getElementById("telefono").value = '';
                document.getElementById("ciudad").value = '';
                document.getElementById("departamento").value = '';
                document.getElementById("email").value = '';
                document.getElementById("cargo").value = '';
                document.getElementById("mercado").value = '';
            }

            function limpiar(){
                document.getElementById("cadena").value = '';
                document.getElementById("param").selectedIndex = 0;

            }

            function validar(){
                var valido = false;
                if(trim(document.getElementById("nit").value)!=''){
                    if(trim(document.getElementById("nit").value)!='-'){
                        if(trim(document.getElementById("nombre").value)!=''){
                            valido = true;
                        }
                        else{
                            alert('Digite un nombre valido');
                        }
                    }
                    else{
                        alert('Digite un nit valido');
                    }
                }
                else{
                    alert('Digite un nit valido');
                }
                return valido;
            }

            function trim(cadena){
                for(i=0; i<cadena.length;){
                    if(cadena.charAt(i)==" ") cadena=cadena.substring(i+1, cadena.length);
                    else break;
                }
                for(i=cadena.length-1; i>=0; i=cadena.length-1){
                    if(cadena.charAt(i)==" ") cadena=cadena.substring(0,i);
                    else break;
                }
                for(i=0;i<cadena.length;){
                    if(cadena.charCodeAt(i)=="13") cadena=cadena.substring(i+1, cadena.length);
                    else if(cadena.charCodeAt(i)=="10") cadena=cadena.substring(i+1, cadena.length);
                    else break;
                }
                for(i=cadena.length-1; i>=0; i=cadena.length-1){
                    if(cadena.charCodeAt(i)=="13") cadena=cadena.substring(0,i);
                    else if(cadena.charCodeAt(i)=="10") cadena=cadena.substring(0,i);
                    else break;
                }
                return cadena;
            }

            function enviar(opcion){
                var ids = document.getElementById('codigo').value;
                var txtalert = '';
                if(opcion=='delete'){
                    txtalert = '¿Esta seguro de eliminar el registro?';
                }
                else if(opcion==='modify'){
                    txtalert = '¿Esta seguro de modificar el registro?';
                }
                else if(opcion=='insert'){
                    txtalert = '¿Esta seguro de insertar un nuevo registro?';
                }
                else{
                    alert('No se selecciono nada ... raro');
                }
                var choice = confirm(txtalert);
                if(choice==true){
                    if(txtalert!=''){
                        if(validar()==true){
                            if(opcion=='delete'){
                                if(trim(document.getElementById("codigo").value)!=''){
                                    sendForm(opcion,ids);
                                }
                                else{
                                    alert('Debe colocar un codigo para poder eliminar');
                                }
                            }
                            else{
                                sendForm(opcion,ids);
                            }
                        }
                    }
                }
            }

            function sendForm(opcion,ids){
                document.getElementById("forma").action = document.getElementById("forma").action + '&opcion=' + opcion;
                if(opcion=='delete' || opcion=='modify'){
                    document.getElementById("forma").action = document.getElementById("forma").action + '&idx=' + ids;
                }
                document.getElementById("forma").submit();
            }

        </script>
    </head>
    <body>
        <div id="capaSuperior" style="position:absolute; width:100%; height:100px; z-index:0; left: 0px; top: 0px;">
            <jsp:include page="/WEB-INF/toptsp.jsp?encabezado=Gestion de ejecutivos"/>
        </div>
        <br>
        <br>
        <div align="center" id="capaCentral" style="position:absolute; width:100%; height:85%; z-index:0; left: -1px; top: 100px; overflow:visible;">
            <form name="forma" id="forma" action="<%= CONTROLLEROPAV%>?estado=Ejecutivo&accion=Gestion" method="POST">
                <table border="1" style="border-collapse:collapse;" width="100%">
                    <thead>
                        <tr class="subtitulo1">
                            <th>Campo</th>
                            <th>Valor</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr class="fila">
                            <td>Codigo del ejecutivo</td>
                            <td>
                                <input type="text" name="codigo" id="codigo" value="" size="60" >
                                <img alt="buscar" src="<%= BASEURL%>/images/botones/iconos/buscar.gif"  name="imgbuscar1" onClick="buscarDatos();" style="cursor:pointer ">
                            </td>
                        </tr>
                        <tr class="fila">
                            <td>Nit</td>
                            <td><input type="text" name="nit" id="nit" value="" size="60" ></td>
                        </tr>
                        <tr class="fila">
                            <td>Nombre</td>
                            <td><input type="text" name="nombre" id="nombre" value="" size="60" ></td>
                        </tr>
                        <tr class="fila">
                            <td>Direccion</td>
                            <td><input type="text" name="direccion" id="direccion" value="" size="60" ></td>
                        </tr>
                        <tr class="fila">
                            <td>Telefono</td>
                            <td><input type="text" name="telefono" id="telefono" value="" size="60" ></td>
                        </tr>
                        <tr class="fila">
                            <td>Ciudad</td>
                            <td><input type="text" name="ciudad" id="ciudad" value="" size="60" ></td>
                        </tr>
                        <tr class="fila">
                            <td>Departamento</td>
                            <td><input type="text" name="departamento" id="departamento" value="" size="60" ></td>
                        </tr>
                        <tr class="fila">
                            <td>Correo electronico</td>
                            <td><input type="text" name="email" id="email" value="" size="60" ></td>
                        </tr>
                        <tr class="fila">
                            <td>Cargo</td>
                            <td><input type="text" name="cargo" id="cargo" value="" size="60" ></td>
                        </tr>
                        <tr class="fila">
                            <td>Mercado</td>
                            <td><input type="text" name="mercado" id="mercado" value="" size="60" ></td>
                        </tr>
                        <tr class="fila">
                            <td colspan="2" align="center">
                                <img alt="aceptar" src="<%= BASEURL%>/images/botones/aceptar.gif"  name="imgaceptar"  onMouseOver="botonOver(this);" onClick="enviar('insert');" onMouseOut="botonOut(this);"  style="cursor:pointer ">
                                <img alt="eliminar" src="<%= BASEURL%>/images/botones/eliminar.gif"  name="imgdelr"  onMouseOver="botonOver(this);" onClick="enviar('delete');" onMouseOut="botonOut(this);"  style="cursor:pointer ">
                                <img alt="modificar" src="<%= BASEURL%>/images/botones/modificar.gif"  name="imgmody"  onMouseOver="botonOver(this);" onClick="enviar('modify');" onMouseOut="botonOut(this);"  style="cursor:pointer ">
                                <img alt="borrar" src="<%= BASEURL%>/images/botones/cancelar.gif"  name="imgclear"  onMouseOver="botonOver(this);" onClick="limpiarForm();" onMouseOut="botonOut(this);"  style="cursor:pointer ">
                                <img alt="salir" src="<%= BASEURL%>/images/botones/salir.gif"  name="imgsalir"  onMouseOver="botonOver(this);" onClick="parent.close();" onMouseOut="botonOut(this);"  style="cursor:pointer ">
                            </td>
                        </tr>
                    </tbody>
                </table>
                <br>
                <br>
                <table border="1" style="border-collapse:collapse;" width="100%">
                    <thead>
                        <tr class="subtitulo1">
                            <th colspan="2">Busqueda</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr class="fila">
                            <td>Filtro de busqueda</td>
                            <td>
                                <select name="param" id="param">
                                    <option>Seleccione...</option>
                                    <option value="id_ejecutivo">Codigo del ejecutivo</option>
                                    <option value="nit">Nit</option>
                                    <option value="nombre">Nombre</option>
                                    <option value="direccion">Direccion</option>
                                    <option value="tel1">Telefono</option>
                                    <option value="ciudad">Ciudad</option>
                                    <option value="depto">Departamento</option>
                                    <option value="correo">Correo electronico</option>
                                    <option value="cargo">Cargo</option>
                                    <option value="mercado">Mercado</option>
                                </select>
                            </td>
                        </tr>
                        <tr class="fila">
                            <td>Cadena a buscar</td>
                            <td><input type="text" name="cadena" id="cadena" value="" size="60"></td>
                        </tr>
                        <tr class="fila">
                            <td colspan="2" align="center">
                                <img alt="buscar" src="<%= BASEURL%>/images/botones/buscar.gif"  name="imgbusq"  onMouseOver="botonOver(this);" onClick="buscar();" onMouseOut="botonOut(this);"  style="cursor:pointer ">
                                <img alt="borrar" src="<%= BASEURL%>/images/botones/cancelar.gif"  name="imgcancel"  onMouseOver="botonOver(this);" onClick="limpiar();" onMouseOut="botonOut(this);"  style="cursor:pointer ">
                            </td>
                        </tr>
                    </tbody>
                </table>

            </form>
            <br>
            <div id="headerb" class="subtitulo1" align="center">Resultados de la busqueda</div>
            <br>
            <div id="busqueda"></div>
            <br>
        </div>
    </body>
</html>
