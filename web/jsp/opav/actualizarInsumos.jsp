<%-- 
    Document   : actualizarInsumos
    Created on : 16/09/2016, 09:08:01 AM
    Author     : dvalencia
--%>

<%@page contentType="text/html" pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
        <title>Insumos</title>
        <link href="./css/style_azul.css" rel="stylesheet" type="text/css">
        <link type="text/css" rel="stylesheet" href="./css/jquery/jquery-ui/jquery-ui.css"/>
        <link type="text/css" rel="stylesheet" href="./css/popup.css"/>
        <script type="text/javascript" src="./js/jquery/jquery-ui/jquery.min.js"></script>
        <link type="text/css" rel="stylesheet" href="./css/jquery/jquery.jqGrid/ui.jqgrid.css"/>        
        <script type="text/javascript" src="./js/jquery-ui-1.8.5.custom.min.js"></script>
        <script type="text/javascript" src="./js/jquery/jquery.jqGrid-4.7.0/js/grid.locale-es.js"></script>
        <script type="text/javascript" src="./js/jquery/jquery.jqGrid-4.7.0/js/jquery.jqGrid.min.js"></script>
        <script type="text/javascript" src="./js/jquery/jquery.jqGrid-4.7.0/js/jquery.tabletojson.js"></script>
        <link type="text/css" rel="stylesheet" href="./css/buttons/botonVerde.css"/>
        
        
        <script type="text/javascript" src="./js/actualizarCaracteres.js"></script> 
    </head>
    <body>
        <div id="capaSuperior"  style="position:absolute; width:100%; height:100px; z-index:0; left: 0px; top: 0px;">
            <jsp:include page="/WEB-INF/toptsp.jsp?encabezado=Insumos"/>
        </div>
        <div id="capaCentral" style="position:absolute; width:100%; height:100%; z-index:0; left: 0px; top: 100px; ">         
            <center>
                <div style="position: relative;top: 80px"> 
                <table id="tabla_insumos"></table>
                <div id="page_tabla_insumos"></div>    
            </div>
            </center>
            <div id="div_insumos"  style="width: 530px;display: none;" >  
                <div id="tablainterna" style="width: 600px;" >
                    <table id="tablainterna"  >
                        <tr>
                            <td>
                                <input type="text" id="id" style="width: 137px;color: black;" hidden>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <label>Codigo material</label>
                                <input type="text" id="codigo_material" value="Solo lectura" readonly style="width: 100px;color: black; ">
                            </td>
                            <td>
                                <label>Nombre</label>
                                <input type="text" id="descripcion" style="width: 550px;color: black;">
                            </td>
                        </tr>
                    </table>
                </div>
            </div>
            <div id="dialogLoading" style="display:none;">
                <p  style="font-size: 12px;text-align:justify;" id="msj2">Texto </p> <br/>
                <center>
                    <img src="./images/cargandoCM.gif"/>
                </center>
            </div>
            <div id="dialogMsj" title="Mensaje" style="display:none;">
                <p style="font-size: 12px;text-align:justify;" id="msj" > Texto </p>
            </div> 
        </div>
        <script type='text/javascript'>
            cargarInsumos();            
        </script>
    </body>
</html>
