<%@page contentType="text/html" pageEncoding="utf-8"%>
<!DOCTYPE html>
<html lang="ES">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
        <title>Tasa</title>


        
        <!----------------------------------------JQUERY--------------------------------------------->
        <link type="text/css" rel="stylesheet" href="./css/jquery/jquery-ui/jquery-ui.css"/> 
        <script src="https://code.jquery.com/jquery-3.4.1.min.js" integrity="sha256-CSXorXvZcTkaix6Yvo6HppcZGetbYMGWSFlBw8HfCJo=" crossorigin="anonymous"></script>
        <script type="text/javascript" src="./js/jquery/jquery-ui/jquery.ui.min.js"></script>   
        <!----------------------------------------JQUERY--------------------------------------------->

        <!-------------------------------------JQGRID----------------------------------------->
        <link type="text/css" rel="stylesheet" href="./css/jquery/jquery.jqGrid/ui.jqgrid.css"/>
        
        <script type="text/javascript" src="./js/jquery/jquery.jqGrid-4.7.0/js/jquery.jqGrid.min.js"></script>
        <script type="text/javascript" src="./js/jquery/jquery.jqGrid-4.7.0/js/grid.locale-es.js"></script>
        <script type="text/javascript" src="./js/jquery/jquery.jqGrid-4.7.0/js/jquery.tabletojson.js"></script> 
        <!-------------------------------------/JQGRID----------------------------------------->

       

        <!----------------------------------------Pre-Load--------------------------------------------->
        <!--se necesita un div para que funcione el preload se llama loader-wrapper-->

        <link type="text/css" rel="stylesheet" href="./css/main2.css">

        <!----------------------------------------Pre-Load--------------------------------------------->

        <!----------------------------------------bootstrap--------------------------------------------->
        <link href="./css/bootstrap/css/bootsrtap-3.3.7.min.css" rel="stylesheet" type="text/css"/>
        <!----------------------------------------bootstrap--------------------------------------------->
        
        
        <!----------------------------------------js-plantilla--------------------------------------------->
        <script src="./js/sl_tasa.js" type="text/javascript"></script>
        <!----------------------------------------js-plantilla--------------------------------------------->
        
        <!----------------------------------------JQUERY- Confirmation------------------------------------->
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/jquery-confirm/3.2.3/jquery-confirm.min.css">
        <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-confirm/3.2.3/jquery-confirm.min.js"></script>
        <!----------------------------------------JQUERY- Confirmation------------------------------------->

         <!-------------------------------------Notificaciones----------------------------------------->
        <link type="text/css" rel="stylesheet" href="./css/toastr.min.css"/>
        <script type="text/javascript" src="./js/toastr.min.js"></script>
        <!-------------------------------------/Notificaciones----------------------------------------->
    </head>

    <body>
            
        <jsp:include page="/WEB-INF/toptsp.jsp?encabezado=Cartera"/>

        <div class="container-fluid">
           
            
            <div id="capaCentral" class='capaCentral' style="margin-top: 10px;" align="center">
                    <div class="ui-jqgrid ui-widget ui-widget-content ui-corner-all" style="max-width: 454px; height: 80px;">

                        <div class="ui-dialog-titlebar ui-widget-header ui-corner-all ui-helper-clearfix">
                            <span class="ui-dialog-title" id="ui-dialog-title-listaGlobal">
                                &nbsp;
                                <label class="titulotablita"><b>INSERTAR TASA</b></label>
                            </span>
                        </div>
                        <table style=" margin: 15px " style="width: 95%;">

                            <tr align='center'>
                                <td>TASA:</td>
                                <td>
                                    <input type="text" id="txt_tasa" class="solo-numero" style="width: 90%;"/>
                                </td>
                                <td>
                                    <button onclick="insertarTasa()">Aceptar</button>
                                </td>
                            </tr>

                        </table>
                    </div>
                <div style="margin-top: 15px">
                    <center>
                        <table id="tbl_auditoria_tasas" ></table>                
                        <div id="page_tbl_auditoria_tasas"></div>
                    </center>
                </div>
            </div>
            
            
                
        </div>
            

        
        

        <!-- **********************************************************************************************************************************************************
                                                                             loader-wrapper
        *********************************************************************************************************************************************************** -->
        <div id="loader-wrapper">
            <div id="loader"></div>
            <img id="logowast" src="/fintra/images/Selectrik.png" style="z-index: 10002 !important">
            <div class="loader-section section-left">	
            </div>
            <div class="loader-section section-right"></div>
        </div>


        <!-- **********************************************************************************************************************************************************
                                                                        FIN  loader-wrapper
        *********************************************************************************************************************************************************** -->


    </body>
</html>
