<%@page import="com.tsp.operation.model.beans.TablaGen"%>
<%@page import="com.tsp.operation.model.beans.Usuario"%>
<!--
- Autor : Ing. imorales
- Date  : diciembre de 2009
- Copyrigth Notice : Fintravalores S.A.
-->
<%-- Declaracion de librerias--%>
<%@page contentType="text/html;"%>
<%@page session="true"%>
<%@page import="java.util.*"%>
<%@page import="com.tsp.opav.model.*"%>
<%@page import="com.tsp.opav.model.beans.*"%>
<%@page import="com.tsp.opav.model.services.*"%>
<%@page import="com.tsp.util.*"%>
<%@page import="com.tsp.util.Util.*"%>
<%@include file="/WEB-INF/InitModel.jsp"%>

<%@taglib uri="http://jakarta.apache.org/taglibs/input-0.90" prefix="input" %>
<html>
    <head>
        <title>Denegar Oferta</title>
        <link href="../../../css/estilostsp.css" rel="stylesheet" type="text/css">


        <link href="<%//=BASEURL%>/css/estilostsp.css" rel="stylesheet" type="text/css">
        <%  String id = "";
        %>
    </head>
    <%
        Usuario usuario = (Usuario)session.getAttribute("Usuario");
        ClientesVerService clvsrv = new ClientesVerService(usuario.getBd());
        String perfil = clvsrv.getPerfil(usuario.getLogin());

        id = request.getParameter("solicitudes");
    %>

    <script src="<%= BASEURL %>/js/boton.js"></script>

    <script type='text/javascript' src="<%=BASEURL%>/js/Validacion.js"></script>
    <!--<script type='text/javascript' src="<%//=BASEURL%>/js/boton.js"></script>-->
    <script type='text/javascript' src="<%=BASEURL%>/js/general.js"></script>
    <script type='text/javascript' src="<%=BASEURL%>/js/tools.js"></script>
    <script type='text/javascript' src="<%=BASEURL%>/js/date-picker.js"></script>
    <script type='text/javascript' src="<%=BASEURL%>/js/transferencias.js"></script>

    <script type='text/javascript' src="<%=BASEURL%>/js/prototype.js"></script>

    <script type="text/javascript">

        function validate(){
            var a = "";
            var x = document.getElementsByName("checkbox");
            var l = x.length;

            for(var i=0; i<l; i++){
                    if(x[i].checked){
                            a = a + x[i].value + ",";
                    }
            }

            $("anulaciones").value = a.substring(0,(a.length-1));
        }

        function validarAnulacion(){
            validate();//091224
            if (forma.text_consi.value==""){
                alert("Debe colocar una observación.");
                forma.imgaceptar.style.visibility='visible';//091223
            }else{
                sendAction('<%=CONTROLLEROPAV%>?estado=Electricaribe&accion=Oferta&opcion=4&solicitudes=<%=id%>');
            }
        }

        function sendAction(url){

            var p = "anulaciones=" + $("anulaciones").value + "&text_consi=" + $("text_consi").value;

            new Ajax.Request(
                url,
                {
                    method: 'get',
                    parameters: p,
                    onComplete: sendMessage
                });
        }


        function sendToPrint(url){

            var p = "num_oferta=" + $("num_oferta").value;

            new Ajax.Request(
                    url,
                    {
                        method: 'get',
                        parameters: p,
                        onComplete: sendMessage
                    });
        }

        function sendMessage(response){
            alert(response.responseText);
        }

    </script>

    <body>
        <div id="capaSuperior" style="position:absolute; width:100%; height:100px; z-index:0; left: 0px; top: 0px;">
            <jsp:include page="/toptsp.jsp?encabezado=DENEGAR OFERTA"/>
        </div>
        <div id="capaCentral" style="position:absolute; width:100%; height:85%; z-index:0; left: 0px; top: 100px; overflow: scroll;">
        <!--<div id="capaCentral" style="position:absolute; width:100%; height:87%; z-index:0; left: 2px; top: 100px; overflow:scroll;">-->
            <form id="forma" name="forma" method="post" action="<%=CONTROLLEROPAV%>?estado=Electricaribe&accion=Oferta&opcion=2">
                <div align="center">

                    <%

                        DatosOferta dato = modelopav.ElectricaribeOfertaSvc.ofertaInfoAnul(id);
                        ArrayList ofertas = modelopav.ElectricaribeOfertaSvc.getAnulacionesItems(id);
                    %>

                    <table width="700" border="1">
                        <tr>
                            <td width="30%"  class="subtitulo">Solicitud</td>
                            <td width="70%">
                                <input id="num_oferta" name="num_oferta" type="text" value="<%=id%>" style="width:490px;" readonly>
                            </td>
                        </tr>

                        <tr>
                            <td width="30%" class="subtitulo">Motivos de denegación</td>
                            <td width="70%">
                                <table width="490" border="0">

                                    <%
                                        if(ofertas.size() > 0){
                                            for(int i = 0; i < ofertas.size(); i++){
                                                    TablaGen oferta=(TablaGen)ofertas.get(i);
                                                %>
                                                <tr>
                                                    <td>

                                                        <%
                                                        if(oferta.getDato().equals("true")){%>
                                                            <input checked="true" onclick="validate()" value="<%=oferta.getTable_code()%>" name="checkbox" type="checkbox">
                                                            <label class="letra"><%=oferta.getDescripcion()%></label>
                                                        <%
                                                        }
                                                        else{%>
                                                            <input onclick="validate()" value="<%=oferta.getTable_code()%>" name="checkbox" type="checkbox">
                                                            <label class="letra"><%=oferta.getDescripcion()%></label>
                                                        <%
                                                        }
                                                        %>

                                                    </td>
                                                </tr>
                                                <%
                                            }
                                        }
                                    %>

                                </table>
                            </td>
                        </tr>
                        <tr>
                            <td height="50" width="30%" class="subtitulo">Observacion</td>
                            <td height="50" width="70%">
                                <textarea id="text_consi" name="text_consi"  style="width:490px; height:50px;"><%=dato.getOtras_consideraciones()%></textarea>
                            </td>
                        </tr>
                    </table>

                    <br>

                    <input type="hidden" id="anulaciones" ><!--091221-->

                    <br>
                    <img id="cargando" name="text_consi" src="<%=BASEURL%>/images/cargando.gif" style="display:none;">
                    <br>

                    <%if (  (clvsrv.ispermitted(perfil, "4")||clvsrv.ispermitted(perfil, "6"))  ){%>
                        <img src="<%=BASEURL%>/images/botones/anular.gif"  name="imgaceptar"  title="Denegar"   onClick="this.style.visibility='hidden';validarAnulacion();" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" style="cursor:hand">
                    <%}%>

                    <img src="<%=BASEURL%>/images/botones/salir.gif"      name="imgsalir"     onMouseOver="botonOver(this);" onClick="parent.close();" onMouseOut="botonOut(this);" style="cursor:hand">
                </div>

                <%String msg = (request.getParameter("msg")!=null)?request.getParameter("msg"):"";%>
                <%if(!msg.equals("OK") && !msg.equals("")){%>
                    <p>
                        <table width="416" border="2" align="center">
                            <tr>
                                <td>
                                    <table width="100%"  border="1" align="center" >
                                        <tr>
                                            <td width="229" align="center" class="mensajes"> <%=msg%></td>
                                            <td width="29" background="<%=BASEURL%>/images/cuadronaranja.JPG">&nbsp;</td>
                                            <td width="58">&nbsp;</td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                        </table>
                    </p>
                <%}%>
            </form>
        </div>
    </body>
</html>