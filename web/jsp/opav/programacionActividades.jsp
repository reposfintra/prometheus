<%-- 
    Document   : programacionActividades
    Created on : 3/06/2010, 10:55:41 AM
    Author     : darrieta-GEOTECH
--%>

<%@page import="com.tsp.operation.model.beans.Usuario"%>
<%@page import="com.tsp.opav.model.services.ClientesVerService"%>
<%@page import="com.tsp.operation.model.beans.TablaGen"%>
<%@page contentType="text/html" pageEncoding="ISO-8859-1"%>
<%@page session ="true"%>
<%@page errorPage ="/error/ErrorPage.jsp"%>
<%@page import ="java.util.*" %>
<%@page import ="com.tsp.opav.model.*"%>
<%@page import ="com.tsp.opav.model.beans.*"%>
<%@include file ="/WEB-INF/InitModel.jsp"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">

<%

	
    List<TablaGen> lista = new LinkedList<TablaGen>();
    model.tablaGenService.buscarRegistros("TIPO_ACT");
    lista=model.tablaGenService.getTablas();

    String idSolicitud = request.getParameter("id_solicitud");
    String idAccion = request.getParameter("id_accion");
    NegocioApplus infoAccion = modelopav.seguimientoEjecucionService.obtenerInfoAccion(idSolicitud, idAccion);
    String tipo = request.getParameter("tipo")!=null ? request.getParameter("tipo") : "20";
   
    model.tablaGenService.buscarRegistrosNoAnulados("RESP_ACTIV");
    LinkedList responsables = model.tablaGenService.getTablas();
    ArrayList<Actividades> consulta = modelopav.seguimientoEjecucionService.consultarActividadesAccion(idAccion);
        if(consulta.size()!=0)
        {
             tipo=consulta.get(0).getTipo();
        }
     ArrayList<Actividades> actividades = modelopav.seguimientoEjecucionService.obtenerListaActividadesTrabajo(tipo);
    int tam = (consulta.size() == 0 ? actividades.size() : consulta.size());
    boolean confirmado = (consulta.size() > 0 && consulta.get(0).getReg_status().equals("") ? true : false);
     
    Usuario usuario = ((Usuario) session.getAttribute("Usuario"));
    ClientesVerService clvsrv = new ClientesVerService(usuario.getBd());
    String perfil = clvsrv.getPerfil(usuario.getLogin());
%>
<script>
    var json = new Array();
    var fechaI = '';
            function abrirSeleccion(idSolicitud,idAccion)
			{
				var tipo = document.getElementById('tipo').value;
                var url = "<%=BASEURL%>";
				url += "/jsp/opav/programacionActividades.jsp?id_solicitud="+idSolicitud+"&id_accion="+idAccion+"&tipo="+tipo;                                
                location.href=url;
            }


    function genJSON() {
        try {
            json = new Array();
            var titulo = '';
            var rows = document.getElementsByClassName('fila');
            var index = 0;
            
            for (i = 0; 1 < rows.length; i++) {
                try {                
                    index = parseInt(rows[i].id.replace('fila',''));
                }catch(e) {break;}
                json[i] = {
                        name: '',
                        desc: document.getElementById('cmbActividad'+index).options[document.getElementById('cmbActividad'+index).selectedIndex].text,
                        values: [{
                            from: parseInt(document.getElementById('txtFechaInicial'+index).value),
                            to: parseInt(document.getElementById('txtFechaFinal'+index).value),
                            progress: ""
                        }]
                };
            }
            json[0].name= document.getElementById('tipo').options[document.getElementById('tipo').selectedIndex].text;
            fechaI = (document.getElementById("verDias").checked)?'':document.getElementById('fechaInicial').value;
            AbrirVentana('<%= BASEURL%>');
        } catch(excepcion) { }
    }
</script>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
        <title>Programacion actividades</title>
        <link href="<%= BASEURL%>/css/estilostsp.css" rel='stylesheet'>
        <script type='text/javascript' src="<%= BASEURL%>/js/boton.js"></script>
        <script type='text/javascript' src="<%= BASEURL%>/js/prototype.js"></script>
        <script type='text/javascript' src="<%= BASEURL%>/js/validar.js"></script>
        <script type='text/javascript' src="<%= BASEURL%>/js/seguimientoEjecucion.js"></script>
        <!-- Las siguientes librerias CSS y JS son para el manejo del calendario -->
        <link rel="stylesheet" type="text/css" href="<%=BASEURL%>/css/jCal/jscal2.css" />
        <link rel="stylesheet" type="text/css" href="<%=BASEURL%>/css/jCal/border-radius.css" />
        <script type="text/javascript" src="<%=BASEURL%>/js/jCal/jscal2.js"></script>
        <script type="text/javascript" src="<%=BASEURL%>/js/jCal/lang/es.js"></script>
        

        
        
        
        
    </head>
    <body onload="actualizarTotalProgramacion()">
        <div id="capaSuperior" style="position:absolute; width:100%; height:100px; z-index:0; left: 0px; top: 0px;">
            <jsp:include page="/WEB-INF/toptsp.jsp?encabezado=Programacion de actividades"/>
        </div>
        <div id="capaCentral" style="position:absolute; width:100%; height:100%; z-index:0; left: 0px; top: 100px;">
            <form method='post' name='form1' action="<%=CONTROLLEROPAV%>?estado=Seguimiento&accion=Ejecucion&idAccion=<%=idAccion%>&idSolicitud=<%=idSolicitud%>">
                <table border="1" cellspacing="0" cellpadding="0" width="1000px" align="center">
                    <tr>
                        <td>
                            <table border="0" cellspacing="0" cellpadding="3" class="tblTitulo" align="center" width="100%">
                                <tr>
                                    <td>Id solicitud</td>
                                    <td style="font-weight: normal"><%=idSolicitud%></td>
                                    <td><strong>Cliente</strong></td>
                                    <td style="font-weight: normal"><%=infoAccion.getNombreCliente()%></td>
                                </tr>
                                <tr>
                                    <td><strong>Orden de trabajo</strong></td>
                                    <td style="font-weight: normal"><%=infoAccion.getNumOs()%></td>
                                    <td><strong>Contratista</strong></td>
                                    <td style="font-weight: normal"><%=infoAccion.getNombreContratista()%></td>
                                </tr>
                                <tr>
                                    <td><strong>Acci�n</strong></td>
                                    <td colspan="3" style="font-weight: normal"><%=infoAccion.getAcciones()%></td>
                                </tr>

                                <tr>
                                    <td><strong>Porcentaje total</strong></td>
                                    <td style="font-weight: normal"><span id="pTotal">0</span></td>
                                    <td><strong>Fecha inicial</strong></td>
                                    <td style="font-weight: normal">
                                        <input type="text" id="fechaInicial" name="fechaInicial" value="<%=infoAccion.getFecha()%>" readonly/>
                                        <% if (clvsrv.ispermitted(perfil, "53")) {%>
                                        <img src="<%=BASEURL%>/js/Calendario/cal.gif" id="imgFecha" />
                                        <script type="text/javascript">
                                            Calendar.setup({
                                                inputField : "fechaInicial",
                                                trigger    : "imgFecha",
                                                //max        : Calendar.dateToInt(new Date()),
                                                //dateFormat : "%Y-%m-%d %H:%M:%S",
                                                //showTime   : true,
                                                onSelect   : function() {
                                                    this.hide();
                                                }
                                            });
                                        </script>    
                                        <%}%>
                                    </td>
                                </tr>
                                
                                                                <tr>
                                  <td><strong>Tipos De Actividades </strong></td>
                                  <td colspan="2" style="font-weight: normal">
                                   <select id="tipo" name="tipo" onchange="abrirSeleccion('<%=idSolicitud%>','<%=idAccion%>');">
                               
                                <%for (int i=0;i<lista.size();i++){%>
                                    
                                    <option value="<%=lista.get(i).getTable_code() %>"  <%=confirmado ? "disabled" : ""%>  
                                            <%if( lista.get(i).getTable_code().equals(tipo)) {out.print(" selected");}%>
                                    
                                      ><%=lista.get(i).getDato() %></option>
                                     <%}%>
                                  </select>
                                  </td>
                                  <td>
                                      <img src="<%=BASEURL%>/images/botones/iconos/gantt.gif" title="Ver Grafico" 
                                           onclick=" genJSON();">
                                      <input type="checkbox" id="verDias">Ver por Dias
                                  </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <input type="hidden" id="cont" name="cont" value="<%=tam%>">
                            <input type="hidden" id="maximo" name="maximo" value="<%=tam%>">
                            <table border="0" id="tabActividades" width="1000px">
                                <tr class="tblTitulo">
                                    <th scope="col">&nbsp;</th>
                                    <th scope="col">Actividad</th>
                                    <th scope="col">Responsable</th>
                                    <th scope="col">Peso</th>
                                    <th scope="col">Dia inicio</th>
                                    <th scope="col">Dia fin</th>
                                    <th scope="col">Dias</th>
                                </tr>
                                <%
                                            for (int j = 1; j <= tam; j++) {
                                                Actividades act = null;
                                                if (consulta.size() > 0) {
                                                    act = consulta.get(j - 1);
                                                }else{
                                                    act = actividades.get(j - 1);
                                                }

                                                String peso = (act != null ? String.valueOf(act.getPeso()) : "");
                                %>
                                <tr class="fila" id="fila<%=j%>">
                                    <td nowrap>
                                        <%if (!confirmado) {%>
                                        <img src="<%=BASEURL%>/images/botones/iconos/add.png" id="add<%=j%>" alt="Agregar" title ="Agregar" onclick="agregarActividad(<%=j%>)"/>
                                        <img src="<%=BASEURL%>/images/botones/iconos/eliminar.gif" id="delete<%=j%>" alt="Eliminar" title ="Eliminar" onclick="eliminarActividad(<%=j%>)" />
                                        <%}%>
                                    </td>
                                    <td>
                                        <select style="width: 400px" id="cmbActividad<%=j%>" name="cmbActividad<%=j%>" <%=confirmado ? "disabled" : ""%> onchange="pesoPredeterminado(<%=j%>)">
                                            <option value="-1"> </option>
                                            <%for (int i = 0; i < actividades.size(); i++) {%>
                                            <option value="<%=actividades.get(i).getId() + ";" + actividades.get(i).getPeso() + ";" + actividades.get(i).getResponsable()%>" <%=(act != null && act.getId() == actividades.get(i).getId()) ? "selected" : ""%> ><%=actividades.get(i).getDescripcion()%></option>
                                            <%}%>
                                        </select>
                                    </td>
                                    <td>
                                        <select id="cmbResponsable<%=j%>" name="cmbResponsable<%=j%>" <%=confirmado ? "disabled" : ""%>>
                                            <option value="-1"> </option>
                                            <%for (int i = 0; i < responsables.size(); i++) {
                                                                            TablaGen resp = (TablaGen) responsables.get(i);
                                            %>
                                            <option value="<%=resp.getTable_code()%>" <%=(act.getResponsable() != null && act.getResponsable().equals(resp.getTable_code())) ? "selected" : ""%> ><%=resp.getReferencia()%></option>
                                            <%}%>
                                        </select>
                                    </td>
                                    <td>
                                        <input type="text" id="txtPeso<%=j%>" name="txtPeso<%=j%>" size="2" onKeyPress="return soloDigitos(event, '');" onchange="actualizarTotalProgramacion()" value="<%=act != null ? act.getPeso() : (actividades.size() > 0 ? actividades.get(0).getPeso() : "")%>" <%=confirmado ? "disabled" : ""%>/>
                                        %
                                    </td>
                                    <td nowrap align="center" >
                                        <input type="text" id="txtFechaInicial<%=j%>" name="txtFechaInicial<%=j%>" onchange="calcDias('txtFechaInicial<%=j%>','txtFechaFinal<%=j%>','txtDif<%=j%>')"
                                               value="<%=(act != null && act.getFechaInicial() != null) ? act.getFechaInicial() : ""%>"
                                               size="6" <%=confirmado?"readonly":""%>/>
                                        <!--%if (!confirmado) {%>
                                        <img src="< %=BASEURL%>/js/Calendario/cal.gif" id="imgFechaInicial< %=j%>" />
                                        <script>
                                            Calendar.setup({
                                                inputField : "txtFechaInicial< %=j%>",
                                                trigger    : "imgFechaInicial< %=j%>",
                                                onSelect   : function() { this.hide() }
                                            });
                                        </script>
                                        < %}%-->
                                    </td>
                                    <td nowrap align="center" >
                                        <input type="text" id="txtFechaFinal<%=j%>" name="txtFechaFinal<%=j%>" onchange="calcDias('txtFechaInicial<%=j%>','txtFechaFinal<%=j%>','txtDif<%=j%>')"
                                               value="<%=(act != null && act.getFechaFinal() != null) ? act.getFechaFinal() : ""%>"
                                               size="6" <%=confirmado?"readonly":""%>/>
                                        <!--%if (!confirmado) {%>
                                        <img src="< %=BASEURL%>/js/Calendario/cal.gif" id="imgFechaFinal< %=j%>" />
                                        <script>
                                            Calendar.setup({
                                                inputField : "txtFechaFinal< %=j%>",
                                                trigger    : "imgFechaFinal< %=j%>",
                                                onSelect   : function() { this.hide() }
                                            });
                                        </script>
                                        < %}%-->
                                    </td>
                                    <td nowrap align="center" >
                                        <input type="text" id="txtDif<%=j%>" name="txtDif<%=j%>" size="6" style="text-align: center" 
                                               onchange="calcDias('txtFechaInicial<%=j%>','txtFechaFinal<%=j%>','txtDif<%=j%>')" readonly/>
                                        <script type="text/javascript">
                                            calcDias('txtFechaInicial<%=j%>','txtFechaFinal<%=j%>','txtDif<%=j%>');
                                        </script>
                                    </td>
                                </tr>
                                <%}%>
                            </table>
                        </td>
                    </tr>
                </table>
                <br/>
                <div align="center">
                    <%if (!confirmado) {%>
                    <img style="cursor:hand" src="<%=BASEURL%>/images/botones/confirmar.gif" align="absmiddle" onmouseover="botonOver(this);" onmouseout="botonOut(this);" onClick="confirmarProgramacion()">
                    <img style="cursor:hand" src="<%=BASEURL%>/images/botones/guardar.gif" align="absmiddle" onmouseover="botonOver(this);" onmouseout="botonOut(this);" onClick="guardarProgramacion()">
                    <img style="cursor:hand" src="<%=BASEURL%>/images/botones/restablecer.gif" align="absmiddle" onmouseover="botonOver(this);" onmouseout="botonOut(this);" onClick="restablecerProgramacion('<%=BASEURL%>','<%=idSolicitud%>','<%=idAccion%>')">
                    <%}%>
                    <img style="cursor:hand" src="<%=BASEURL%>/images/botones/salir.gif" align="absmiddle" onmouseover="botonOver(this);" onmouseout="botonOut(this);" onClick="parent.close();">
                    <br/>
                    <%if (request.getParameter("mensaje") != null) {%>
                    <table border="2" align="center">
                        <tr>
                            <td>
                                <table width="100%"  border="0" align="center"  bordercolor="#f7f5f4" bgcolor="#ffffff">
                                    <tr>
                                        <td width="229" align="center" class="mensajes"><%=request.getParameter("mensaje")%></td>
                                        <td width="29" background="<%=BASEURL%>/images/cuadronaranja.JPG">&nbsp;&nbsp;</td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                    </table>
                    <%}%>
                </div>
                
            </form>
        </div>
    </body>
</html>
