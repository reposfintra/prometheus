<%-- 
    Document   : procesoCategoria
    Created on : 24/02/2016, 11:23:05 AM
    Author     : user
--%>
<%@page contentType="text/html" pageEncoding="utf-8"%>
<%@page import="com.tsp.operation.model.beans.Compania"%>
<%@page import="com.tsp.operation.model.CompaniaService"%>
<%@page import="com.tsp.operation.model.beans.Usuario"%>
<%@page import="com.tsp.operation.model.beans.CmbGeneralScBeans"%>
<%@page import="java.util.ArrayList"%>
<%@page import="com.tsp.opav.model.DAOS.impl.ProcesosCatalogoImpl"%>
<%@page import="com.tsp.opav.model.DAOS.ProcesosCatalogoDAO"%>

<%@include file="/WEB-INF/InitModel.jsp"%>
<!DOCTYPE html>
<%    Usuario usuario = (Usuario) session.getAttribute("Usuario");
    String nomEmpresa = "";
    CompaniaService ciaserv = new CompaniaService();
    ciaserv.buscarCia(usuario.getDstrct());
    Compania cia = ciaserv.getCompania();
    nomEmpresa = cia.getdescription();
%>   

<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
        <title>Administracion de Catalogos</title>
        <script type="text/javascript" src="./js/jquery/jquery-ui/jquery.min.js"></script>
        <script type="text/javascript" src="./js/jquery/jquery-ui/jquery.ui.min.js"></script>           
        <link type="text/css" rel="stylesheet" href="./css/jquery/jquery-ui/jquery-ui.css"/>
        <link type="text/css" rel="stylesheet" href="./css/jquery/jquery.jqGrid/ui.jqgrid.css"/>        
        <script type="text/javascript" src="./js/jquery/jquery.jqGrid/js/grid.locale-es.js"></script>
        <script type="text/javascript" src="./js/jquery/jquery.jqGrid/js/jquery.jqGrid.min.js"></script>
        <link type="text/css" rel="stylesheet" href="./css/buttons/botonVerde.css"/>
        <link type="text/css" rel="stylesheet" href="./css/popup.css"/>
        <link href="./css/style_azul.css" rel="stylesheet" type="text/css">
        <script type="text/javascript" src="./js/procesoCategoria.js"></script> 
        <link type="text/css" rel="stylesheet" href="./css/main2.css">

    </head>
    <body>

        <div id="capaSuperior" style="position:absolute; width:100%; height:100px; z-index:0; left: 0px; top: 0px;">
            <jsp:include page="/toptsp.jsp?encabezado=Administracion de Catalogos"/>
        </div>
        <div id="capaCentral" style="position:absolute; width:100%; height:83%; z-index:0; left: 0px; top: 100px; overflow:auto;">       


            <!-------------------------------------->


            <center>
                <br>
                <div class="ui-jqgrid ui-widget ui-widget-content ui-corner-all" style="max-width: 407px; height: 110PX;">

                    <div class="ui-dialog-titlebar ui-widget-header ui-corner-all ui-helper-clearfix">
                        <span class="ui-dialog-title" id="ui-dialog-title-listaGlobal">
                            &nbsp;
                            <label class="titulotablita"><b>FILTRO DE BUSQUEDA</b></label>
                        </span>
                    </div>
                    <table style=" padding: 15px 1em 0.5em 1em"  style="width: 95%;margin-top: 20px">
                        <tr>
                            <td>Tipo Insumo</td>
                            <td>

                                <select id="insumos" name="insumos" style="width: 264px;">
                                    <!--option value="1">MATERIAL</option>
                                    <option value="2">EQUIPOS</option>
                                    <option value="3">MANO DE OBRA</option>
                                    <option value="4">HERRAMIENTAS</option-->
                                </select>
                            </td>
                            <td>
                                <img id = "add_grupo" src = "/fintra/images/botones/iconos/adds.png"
                                     style = "margin-left: 5px; height: 19px; vertical-align: middle;" 
                                     onclick = "crearTipoInsumo()">
                            </td>
                        </tr>
                    </table>
                    <hr>
                    <div id ='botones'>
                        <button id="C_insumo" class="ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only" 
                                role="button" aria-disabled="false" onclick="">
                            <span class="ui-button-text">Buscar</span>
                            <input type="text" id="idinsumo" name="idinsumo" style="display: none"> 


                        </button> 
                    </div>
                </div>
                <br>
                <div>
                    <table id="tabla_productos"></table>
                    <div id="page_productos"></div>
                </div>

                <!------------Div para crear Tipo de Insumos------------->
                <div id="div_tipo_insumo"  style="display: none; width: 800px" >                       
                    <div id="filtros" class="ui-jqgrid ui-widget ui-widget-content ui-corner-all" style="height: 90px;">
                        </br>
                        <table aling="center" style=" width: 100%" >
                            <tr>
                                <td style="width: 20%"><span>Tipo Insumo<span style="color:red;">*</span></span></td>                          
                                <td colspan="2" style="width: 80%"><input type="text" id="nominsumo" name="nominsumo" class="mayuscula" style=" width: 94%">
                                </td>  
                            </tr> 
                            <tr>
                                <td style="width: 20%"><span>% Rentabilidad<span style="color:red;">*</span></span></td>                          
                                <td style="width: 50%">
                                    <input type="text" id="porc_rent" name="porc_rent" class="solo-numeric" style=" width: 90px">                                    
                                </td>  

                                <td style="text-align:right;width: 30%;">
                                    <label style="font-weight: lighter;width:120px;margin-right:14px">
                                        <input id="chx_iva_aiu" type="checkbox" value="" style="vertical-align:middle">
                                        <span style="vertical-align:middle;text-align:right">Iva Con Aiu</span>
                                    </label>
                                </td>
                            </tr> 
                        </table>
                    </div>  
                    </br> 
                </div>

            </center>


            <!-------------------------------------->


            </br>
            <table border="0"  align="center">
                <tr>
                    <td>

                        <div id="searchCategoria"></div>
                        <table id="Categorias" ></table>
                        <div id="page_tabla_categorias"></div>
                    </td>
                </tr>
            </table>
        </div>


        <div id="div_categoria"  style="display: none; width: 800px" >                       
            <div id="filtros" class="ui-jqgrid ui-widget ui-widget-content ui-corner-all" style="height: 110px;">
                </br>
                <table aling="center" style=" width: 100%" >
                    <tr>
                    <input type="hidden" id="idProceso" name="idMeta">     
                    <input type="hidden" id="idempresa" name="idempresa" value="<%=usuario.getDstrct()%>" >      
                    <td style="width: 10%"><span>Nombre</span><span style="color: red; ">*</span></td>                          
                    <td style="width: 50%"><input type="text" id="nommeta" class="mayuscula" name="nommeta" style=" width: 248px" onblur="igualarcampo('descmeta', 'nommeta');">
                        <!--img src='<%=BASEURL%>/images/botones/iconos/lupa.gif' id="buscarnumos" width="15" height="15"  style="cursor:hand"title="Buscar">
                        <input type="text" id="idcategoria" name="idcategoria"/-->
                    </td>  
                    </tr> 
                    <tr>
                        <td style="width: 10%"><span>Descripcion:</span><span style="color: red; ">*</span></td>   
                        <td style="width: 90%" colspan="4"><textarea id ="descmeta" name="descmeta" style="width:535px;resize:none" rows="2" ></textarea></td>                        
                    </tr>
                </table>
            </div>  
            </br> 
        </div>

        <div id="div_editar_categoria"  style="display: none; width: 800px" >
            <div id="filtros" class="ui-jqgrid ui-widget ui-widget-content ui-corner-all" style="height: 145px;padding: 0px 10px 5px 10px">
                </br>
                <table aling="center" style=" width: 100%" >
                    <tr>
                    <input type="hidden" id="idMetaEdit" name="idMetaEdit">    
                    <input type="hidden" id="idempresaEdit" name="idempresaEdit" value="<%=usuario.getDstrct()%>" >      
                    <td style="width: 10%"><span>Nombre</span><span style="color: red; ">*</span></td>                          
                    <td style="width: 50%"><input type="text" id="nommetaEdit" class="mayuscula" name="nommetaEdit" style=" width: 250px" ></td>
                    </tr>
                    <tr>
                        <td style="width: 10%"><span>Descripcion:</span><span style="color: red; ">*</span></td>   
                        <td style="width: 90%" colspan="4"><textarea id ="descmetaEdit" name="descmetaEdit" style="width:525px;resize:none"  rows="2" ></textarea></td>                        
                    </tr>
                    <tr>
                        <td style="width: 100%"><div id="boton_guardar" style="position:absolute; left: 83%; top: 80%; "> 
                                <span aling="center"  class="form-submit-button form-submit-button-simple_green_apple" onClick="actualizarMetaProceso();"> Actualizar </span>
                            </div></td>
                    </tr>
                </table>
            </div>  
            </br>  
            <table border="0"  align="center">
                <tr>
                    <td>
                        <table id="subcategoria" ></table>
                        <div id="page_tabla_subcategorias"></div>
                    </td>
                </tr>
            </table>
        </div> 

        <div id="div_Subcategoria" title="CREAR SUBCATEGORIA" style="display: none; width: 800px" >                

            <div id="filtros" class="ui-jqgrid ui-widget ui-widget-content ui-corner-all" style="height: 110px;padding: 0px 10px 5px 10px">
                </br>
                <table aling="center" style=" width: 100%" >
                    <tr>
                        <td style="display: none"><span>Categoria:</span></td>
                        <td style="display: none"> <select name="idProMeta" disabled = "true" hidden="true" class="combo_180px" style="font-size: 14px;width:230px" id="idProMeta">
                            </select></td>
                        <td style="width: 10%"><span>Nombre</span><span style="color: red; ">*</span></td>
                        <td style="width: 30%"><input type="text" id="nomProceso" name="nomProceso" class="mayuscula" style=" width: 222px" onblur="igualarcampo('descProceso', 'nomProceso')"/> </td>
                    </tr>
                    <tr>
                        <td style="width: 10%"><span>Descripcion:</span><span style="color: red; ">*</span></td>   
                        <td style="width: 90%" colspan="4"><textarea id ="descProceso" name="descProceso" style="width:525px;resize:none"  rows="2" ></textarea></td>                        
                    </tr>
                </table>
            </div>   
            </br> 
        </div>                    


        <!----------------------------pantalla editar subcategoria ----------------------------->
        <div id="div_editar_especificacion"  style="display: none; width: 800px; z-index:1100" >
            <!----------------------------cabezera----------------------------->

            <div id="filtros"  class="ui-jqgrid ui-widget ui-widget-content ui-corner-all" style="height: 140px;z-index:1100;padding: 0px 10px 5px 10px; ">

                </br>
                <table aling="center" style=" width: 100%" >
                    <tr>
                    <input type="hidden" id="idProinterno" name="idProinterno">
                    <input type="hidden" id="idSubcategoria" name="idSubcategoria">
                    <td style="display: none"><span>Categoria:</span></td>
                    <td style="display: none"> <select name="idProMetaEdit" class="combo_180px" style="font-size: 14px;width:280px" id="idProMetaEdit">
                        </select> </td> 
                    <td style="width: 10%"><span>Nombre SubCategoria:</span><span style="color: red; ">*</span></td>
                    <td style="width: 32%"><input type="text" id="nomProinterno" class="mayuscula" style=" width: 245px" name="nomProinterno" /></td>                                             
                    <!--                         <td style="width: 20%"><span>Meta Proceso:</span></td>
                                             <td style="width: 30%"><input type="text" id="metaProceso" name="metaProceso" readOnly style=" width: 200px" </td>-->
                    </tr>
                    <tr>
                        <td style="width: 10%"><span>Descripcion:</span><span style="color: red; ">*</span></td>   
                        <td style="width: 90%" colspan="4"><textarea id ="descProinterno" name="descProinterno" style="width:572px;resize:none"  rows="2" ></textarea></td>                        
                    </tr>
                    <td style="width: 100%"><div id="boton_guardar" style="position:absolute; left: 86%; top: 81%; "> 
                            <span aling="center"  class="form-submit-button form-submit-button-simple_green_apple" onClick="actualizarProInterno();"> Actualizar </span>
                        </div></td>
                </table>
            </div> 
            </br>        

            <div id="bt_asociar_especificacion" title="Asignar especificificacion a subcategoria" style="position:absolute; left: 47%; top: 54%; width: 10%; display:  none "> 
                <span aling="center"  class="form-submit-button form-submit-button-simple_green_apple" onClick="asociarUndProinterno();"> >> </span>
            </div>
            <div id="bt_desasociar_especificacion" title="DesAsignar especificificacion a subcategoria" style="position:absolute; left: 47%; top: 62%; width: 10%; display:  none "> 
                <span aling="center"  class="form-submit-button form-submit-button-simple_green_apple" onClick="desasociarUndProinterno();"> << </span>
            </div>
            <div id="div_und_prointerno" style="display: none; position: absolute; left: 55%; top: 27%; width: 40%;z-index:1200 ">    
                <table border="0"  align="center">
                    <tr>
                        <td>
                            <table id="UndNegocioPro" ></table>
                        </td>
                    </tr>
                </table>   
            </div>
            <div id="div_undNegocios" style="position:absolute; left: 5%; top: 27%; width: 40% ">    
                <table border="0"  align="center">
                    <tr>
                        <td>
                            <table id="UndadesNegocios" >
                                <div id="searchEspecificaciones"></div>
                            </table>
                            <div id="page_tabla_especificacion"></div>
                        </td>
                    </tr>
                </table>   
            </div>
        </div>

        <!---------------------------------------------modificar el div acoplar a la foto de harold------------------------------------------------------------------->
        <div id="div_especificacion" onreadystatechange="alert(1);" title="CREAR ESPECIFICACION" style="display: none;">   


            <div id="filtros" class="ui-jqgrid ui-widget ui-widget-content ui-corner-all" style="height: 156px;padding: 0px 10px 5px 10px;width: auto">
                </br>
                <table aling="center" style=" width: 100%" >
                    <tr>
                    <input type="hidden" id="idEsp" name="idEsp">  
                    <input type="hidden" id="tipo_dato" name="tipo_dato">  
                    <td style="width: 10%"><span>Nombre</span><span style="color: red; ">*</span></td>
                    <td style="width: 30%"><input type="text" id="nomEspecificacion" class="mayuscula" name="nomEspecificacion" style=" width: 222px" onblur="igualarcampo('descEspecificacion', 'nomEspecificacion')"/> </td>
                    <td style="width: 50%">

                        <fieldset style="width:274px ">
                            <legend>Tipo De Ingreso <span style="color: red; ">*</span></legend>
                            <label>
                                <input id= 'radio1' type="radio" name="color" value="1" onclick="prueba(this.value)"> Seleccion
                            </label>
                            <label>
                                <input id= 'radio2' type="radio" name="color" value="2" onclick ="prueba(this.value)"> Digitacion
                            </label>                                    
                        </fieldset>
                        </div>
                    </td>
                    <input type="hidden" id="idRelCatSub" name="idRelCatSub">
                    </tr>
                    <tr>
                        <td style="width: 10%"><span>Descripcion:</span><span style="color: red; ">*</span></td>   
                        <td style="width: 90%" colspan="4"><textarea id ="descEspecificacion" name="descEspecificacion" style="width:524px;resize:none"  rows="2" ></textarea></td>                        
                    </tr>

                    <tr id='rtipodato' style="display: none;" >
                        <td style="width: 10%"><span id=>Tipo de Dato</span></td>  
                        <td> 
                            <div id="tipodato" style=" width: 40%">
                                <select id="tipodatos" name="producto" style="width: 264px;">
                                    <option value="2">ENTERO</option>
                                    <option value="3">DECIMAL</option>
                                    <option value="4">TEXTO</option>
                                </select>
                            </div>
                            <input type="text" id="idtiposeleccion" hidden="true" name="idcategoria">
                        </td>
                    </tr>
                </table>

            </div>  


            <div id="div_seleccion"   style="z-index:1100;display: none;">

                <div id="bt_asociar_valorpredeterminado" title="Asignar und de negocio al proceso interno" style="position:absolute; left: 47.3%; top: 68%; width: 10%;"> 
                    <span aling="center"  class="form-submit-button form-submit-button-simple_green_apple" onClick="AgregarGridAso()"> >> </span>
                </div>
                <div id="bt_desasociar_valorpredeterminado" title="Desasignar und de negocio del proceso interno" style="position:absolute; left: 47.3%; top: 76%; width: 10%;"> 
                    <span aling="center"  class="form-submit-button form-submit-button-simple_green_apple" onClick="AgregarGridNoAso()"> << </span>
                </div>

                <!-----------------------grid valores predeterminados ------------------------------->
                <div id="div_valorespredeterminados" style="position:absolute; left: 2%; top: 48%; width: 40% ">    
                    <table border="0"  align="center">
                        <tr>
                            <td>
                                <table id="valorespredeterminados" >

                                    <div id="searchvalorespredeterminados"></div>
                                </table>
                                <div id="page_tabla_valorespredeterminados"></div>
                            </td>
                        </tr>
                    </table>   
                </div>
                <!-----------------------grid valores predeterminados q estan asiganados a una especificacion------------------------------->
                <div id="div_valorespredeterminados_aso" style="position:absolute; left: 55%; top: 48%; width: 40% ">    
                    <table border="0"  align="center">
                        <tr>
                            <td>
                                <table id="valorespredeterminados_aso" ></table>
                            </td>
                        </tr>
                    </table>   
                </div>
                <!-----------------------Juego de Busqueda------------------------------->

                <center>
                    <br>
                    <div class="ui-jqgrid ui-widget ui-widget-content ui-corner-all" style="max-width: 294px; height: 142px;">

                        <div class="ui-dialog-titlebar ui-widget-header ui-corner-all ui-helper-clearfix">
                            <span class="ui-dialog-title" id="ui-dialog-title-listaGlobal">
                                &nbsp;
                                <label class="titulotablita"><b>FILTRO DE BUSQUEDA</b></label>
                            </span>
                        </div>
                        <table style=" padding: 0.5em 1em 0.5em 1em" style="width: 95%;">
                            <tr>
                                <td>SubCategoria</td>
                                <td>
                                    <select id="b_idsubcategoria" nombre="b_idsubcategoria" onchange="cargarCombo('b_idespecificacion', [this.value])" style="width: 200px;" >
                                        <option value=''>...</option>
                                    </select>
                                </td>
                            </tr>
                            <tr>
                                <td>Especificacion</td>
                                <td>
                                    <select id="b_idespecificacion"  style="width: 200px;" >
                                        <option value=''>...</option>
                                    </select>
                                </td>
                            </tr>
                        </table>
                        <hr>
                        <div id ='botones'>
                            <button id="buscarfitro" class="ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only" 
                                    role="button" aria-disabled="false">
                                <span class="ui-button-text">Buscar</span>
                            </button> 
                        </div>
                    </div>

                </center>





                <div id="div_valpre"  style="display: none; width: 800px" >                       
                    <div id="filtros" class="ui-jqgrid ui-widget ui-widget-content ui-corner-all" style="height: 57px;">
                        </br>
                        <table aling="center" style=" width: 100%" >
                            <tr>
                                <td style="width: 10%"><span>Nombre</span></td>                          
                                <td style="width: 50%"><input type="text" id="nomvalorpredeterminado" name="nommeta" style=" width: 248px" >
                                    <!--img src='<%=BASEURL%>/images/botones/iconos/lupa.gif' id="buscarnumos" width="15" height="15"  style="cursor:hand"title="Buscar">
                                    <input type="text" id="idcategoria" name="idcategoria"/-->
                                </td>  
                            </tr> 

                        </table>
                    </div>  
                    </br> 
                </div>
            </div>


        </div>

        <div id="div_asoc_user_prointerno"  style="display: none; width: 800px">
            <div id="filtros" class="ui-jqgrid ui-widget ui-widget-content ui-corner-all" style="height:110px; z-index:1100; padding: 0px 10px 0px 10px;">

                </br>
                <table aling="center" style=" width: 100%" >
                    <tr>
                    <input type="hidden" id="idProinternoAsoc" name="idProinternoAsoc"/>
                    <td style="width: 10%"><span>Nombre:</span></td>
                    <td style="width: 32%"><input type="text" id="nomProinternoAsoc" name="nomProinternoAsoc" readonly style=" width: 245px" </td>
                    <td style="width: 12%"><span>Categoria:</span></td>
                    <td style="width: 40%"><input type="text" id="nommetaproceso" name="nommetaproceso" readonly style=" width: 278px" </td>
                    </select> </td>  
                    </tr>div_procesos_internos_anul
                    <tr>
                        <td style="width: 10%"><span>Descripcion:</span></td>   
                        <td style="width: 90%" colspan="3"><textarea id ="descProinternoAsoc" name="descProinternoAsoc" style="width:340px;resize:none"  rows="2" readonly></textarea></td>                        
                    </tr>
                </table>
            </div> 
            </br>
            <div id="bt_asociar_userPro" title="Asignar usuarios al proceso interno" style="position:absolute; left: 47%; top: 54%; width: 10%; display:  none "> 
                <span aling="center"  class="form-submit-button form-submit-button-simple_green_apple" onClick="asociarUsuariosProinterno();"> >> </span>
            </div>  
            <div id="bt_desasociar_userPro" title="Desasignar usuarios del proceso interno" style="position:absolute; left: 47%; top: 62%; width: 10%; display:  none "> 
                <span aling="center"  class="form-submit-button form-submit-button-simple_green_apple" onClick="desasocia
                        rUsuariosProinterno()
                                ;"> << </span>
            </div>  
            <div id="div_user_prointerno" style="display: none; position: absolute; left: 55%; top: 22%; width: 40% ">    
                <table border="0"  align="center">
                    <tr>
                        <td>
                            <table id="userProInterno" ></table>
                        </td>
                    </tr>
                </table>   
            </div>
            <div id="div_usuarios" style="position:absolute; left: 3%; top: 21%; width: 40% ">    
                <table border="0"  align="center">
                    <tr>
                        <td>
                            <table id="listUsuarios" ></table>
                        </td>
                    </tr>
                </table> 
            </div>
        </div> 

        <div id="div_procesos_internos_anul"  style="display: none; width: 800px" >
            <div id="filtros" class="ui-jqgrid ui-widget ui-widget-content ui-corner-all" style="height: 110px;padding: 0px 10px 5px 10px">
                </br>
                <table aling="center" style=" width: 100%" >
                    <tr>          
                    <input type="hidden" id="idMetaAnul" name="idMetaAnul">    
                    <td style="width: 10%"><span>Nombre</span></td>                          
                    <td style="width: 50%"><input type="text" id="nommetaprocesoAnul" name="nommetaproceso" style=" width: 275px" readonly></td>
                    </tr>
                    <tr>
                        <td style="width: 10%"><span>Descripcion:</span></td>   
                        <td style="width: 90%" colspan="4"><textarea id ="descmetaprocesoAnul" name="descmetaproceso" style="width:565px;resize:none"  rows="2" readonly></textarea></td>                        
                    </tr>                         
                </table>
            </div>  
            </br>  
            <table border="0"  align="center">
                <tr>
                    <td>
                        <table id="procesos_internos_anul" ></table>
                        <div id="page_tabla_procesos_internos_anul"></div>
                    </td>
                </tr>
            </table>
        </div>                             

        <div id="dialogMsgMeta" title="Mensaje" style="display:none; visibility: hidden">
            <p style="font-size: 12.5px;text-align:justify;" id="msj" > Texto </p>
        </div>  
        <!--SOLO PRUEBA-->            
        <div id="dialogMsjmultiservicio" class="ui-widget" style="display:none;top: 14px;" >
            <table id="tablainterna"  >
                <tr>
                    <td>
                        <label style="font-family: Tahoma,Arial; padding-left: 5px;font-size: 14px; ">Nombre Categoria</label>
                        <input type="text" id="multiser"style="width: 119px;font-family: Tahoma,Arial;color: black;font-size: 13px;" > 
                    </td>
                </tr>
            </table>

            <table id="tabla_multiservicio" >

            </table>
        </div>

        <!------------------------------->
        <div id="loader-wrapper">
            <div id="loader"></div>
            <img id="logowast" src="/fintra/images/Selectrik.png" style="z-index: 10002 !important">
            <div class="loader-section section-left">	
            </div>
            <div class="loader-section section-right"></div>
        </div>


    </body>
</html>