
<!--
- Autor : Ing. Julio Ernesto Barros Rueda
- Date  : 14 de Mayo de 2007
- Copyrigth Notice : Fintravalores S.A. S.A

Descripcion : Pagina JSP, que maneja el ingreso de identidades
-->

<%-- Declaracion de librerias--%>
<%@page import="com.tsp.operation.model.CiudadService"%>
<%@page import="com.tsp.operation.model.beans.Usuario"%>
<%@page contentType="text/html;"%>
<%@page session="true"%>
<%@page import="java.util.*"%>
<%@page import="com.tsp.opav.model.*"%>
<%@page import="com.tsp.opav.model.beans.*"%>
<%@page import="com.tsp.opav.model.services.*"%>


<%@page import="com.tsp.util.*"%>
<%@page import="com.tsp.util.Util.*"%>
<%@include file="/WEB-INF/InitModel.jsp"%>
<%@page import="com.tsp.opav.model.*"%>
<%@taglib uri="http://jakarta.apache.org/taglibs/input-0.90" prefix="input" %>

<%
response.setHeader("Content-Type", "text/html; charset=ISO-8859-1");
response.setHeader("Pragma", "no-cache"); response.setHeader("Expires","0"); response.setHeader("Cache-Control", "no-cache");
response.setHeader("Cache-Control", "no-store"); response.setHeader("Cache-Control", "must-revalidate");
%>

<html>
    <head>
        <title>Datos Cliente Electricaribe</title>
        <link href="../../../css/estilostsp.css" rel="stylesheet" type="text/css">
        <link href="<%=BASEURL%>/css/estilostsp.css" rel="stylesheet" type="text/css">
    </head>

    <script type="text/javascript" src="<%=BASEURL%>/js/Validacion.js"></script>
    <script type="text/javascript" src="<%=BASEURL%>/js/boton.js"></script>
    <script type="text/javascript" src="<%=BASEURL%>/js/general.js"></script>
    <script type="text/javascript" src="<%=BASEURL%>/js/tools.js"></script>
    <script type="text/javascript" src="<%=BASEURL%>/js/date-picker.js"></script>
    <script type="text/javascript" src="<%=BASEURL%>/js/transferencias.js"></script>

     <!-- Las siguientes librerias CSS y JS son para el manejo de DIVS dinamicos.-->
  <link href="<%=BASEURL%>/css/default.css" rel="stylesheet" type="text/css">
   <link href="<%=BASEURL%>/css/alert.css" rel="stylesheet" type="text/css"/>

<script src="<%=BASEURL%>/js/prototype.js"      type="text/javascript"></script>
<script src="<%=BASEURL%>/js/effects.js"        type="text/javascript"></script>
<script src="<%=BASEURL%>/js/window.js"         type="text/javascript"></script>
<script src="<%=BASEURL%>/js/boton.js"          type="text/javascript"></script>



    <%        String idsolicitud = request.getParameter("idsolicitud");//091221
              String edificio =(request.getParameter("edificio") != null) ? request.getParameter("edificio") : ""  ;
              String tipo_sol=modelopav.NegociosApplusService.getDescTipoSolc(idsolicitud);
              
	%>

    <script type="text/javascript">


	//jpinedo



	function letras(event) {


}

function numbersonly(myfield, e, dec)
{
var key;
var keychar;

if (window.event)
   key = window.event.keyCode;
else if (e)
   key = e.which;
else
   return true;
keychar = String.fromCharCode(key);

// control keys
if ((key==null) || (key==0) || (key==8) || 
    (key==9) || (key==13) || (key==27) )
   return true;

// numbers
else if ((("0123456789").indexOf(keychar) > -1))
   return true;

// decimal point jump
else if (dec && (keychar == "."))
   {
   myfield.form.elements[dec].focus();
   return false;
   }
else
   return false;
}

function CalcularDv() {
    var vpri, x, y, z, i, nit1, dv1;
    nit1 = document.getElementById("nit").value;
    if (isNaN(nit1)) {
        //document.form1.dv.value="X";
        alert('El valor digitado no es un numero valido');
    } else {
        vpri = new Array(16);
        x = 0;
        y = 0;
        z = nit1.length;
        vpri[1] = 3;
        vpri[2] = 7;
        vpri[3] = 13;
        vpri[4] = 17;
        vpri[5] = 19;
        vpri[6] = 23;
        vpri[7] = 29;
        vpri[8] = 37;
        vpri[9] = 41;
        vpri[10] = 43;
        vpri[11] = 47;
        vpri[12] = 53;
        vpri[13] = 59;
        vpri[14] = 67;
        vpri[15] = 71;
        for (i = 0; i < z; i++) {
            y = (nit1.substr(i, 1));
            //document.write(y+"x"+ vpri[z-i] +":");
            x += (y * vpri[z - i]);
            //document.write(x+"<br>");     
        }
        y = x % 11;
        //document.write(y+"<br>");
        if (y > 1) {
            dv1 = 11 - y;
        } else {
            dv1 = y;
        }
        //document.form1.dv.value=dv1;
        document.getElementById("digito_verificacion").value = dv1;
    }
}


	function soloDigits(event, decimal) {
}

function isControlKey2(v,key)
{
	var sw=false;
	for(var i=0;i<v.length;i++)
	{
		if(key==v[i])
		{
			sw=true;
			return sw;
		}
	}
}




	function alfanumerico(event) {

    var key = (document.all) ? event.keyCode : event.which;
    var obj = (document.all) ? event.srcElement : event.target;
    var controlKeys = [46,32,8,35,45];
	alert(key);
    var isControlKey = controlKeys.join(",").match(new RegExp(key)) && (decimal == 'decOK' && obj.value.indexOf('.') == -1);
    if (!key || (48 < key && key > 57) )/*|| (48 == key && $(this).attr("value")) || isControlKey)*/ {
        return;
    } else {
        if(!document.all){
            event.preventDefault();
        }else{
            window.event.keyCode = 0;
        }
    }
}




	            function Valida_cliente(){

                var url = "<%= CONTROLLEROPAV%>?estado=Clientes&accion=Ver";

                var nit = $("nit").value;

                var p = 'opcion=10&nit='+nit;

                new Ajax.Request(url,{method: 'post',parameters: p,onLoading: cargando,onComplete:respuesta});
            }


		    function cargando()
			{
			Dialog.info('<div align="center" id ="salida"  class="enviando" ><img alt="cargando" src="<%= BASEURL%>/images/enviando.gif" name="imgenvia" id="imgenvia"></div>',
							 {    width:300, height:100}

							 );
			}

			function respuesta(param)
			{
			var form = $("formulario");
			var respuestax=param.responseXML;
			var sw=respuestax.getElementsByTagName('mensaje')[0].childNodes[0].data;

			if(sw=='N')
			{

			document.getElementById("salida").innerHTML = "Ya existe como cliente.�Desea agregarlo ala lista de clientes ECA?<br><br><br><input type='button' name='bt_close' id='bt_close' 			value='Salir' onclick='Dialog.closeInfo();' /><input type='button' name='bt_agregar' id='bt_agregar' value='Acptar' onclick='Marca_Cliente();' />";
			}
			else
			{
				/*if(sw=='S')
				{
				document.getElementById("salida").innerHTML = "Ya existe como cliente.<br><br><br><input type='button' name='bt_close' id='bt_close' value='Salir'"+"' onclick='Dialog.closeInfo();' />";

				}
				else
				{*/

					form.submit();

				/*}*/
			}

            }


function Marca_Cliente()
{
			    var url = "<%= CONTROLLEROPAV%>?estado=Clientes&accion=Ver";

                var nit = $("nit").value;

                var p = 'opcion=11&nit='+nit;

                new Ajax.Request(url,{method: 'post',parameters: p,onLoading: cargando,onComplete:respuesta_marca_cliente});
}


function respuesta_marca_cliente(param)
			{
			var respuestax=param.responseXML;
			var codigo=respuestax.getElementsByTagName('codigo')[0].childNodes[0].data;
			var nit=respuestax.getElementsByTagName('nit')[0].childNodes[0].data;
			var nombre=respuestax.getElementsByTagName('nombre')[0].childNodes[0].data;

			var mensaje ="El Cliente "+nombre+" Con Nit: "+nit +" Se Agrego Como Cliente Eca ";



			document.getElementById("salida").innerHTML = mensaje+"<br><br><br><input type='button' name='acptar' id='acptar' value='Acptar' onclick=consultarCliente('"+codigo+"'); />";
			}




		function consultarCliente(cod)
		{

			formulario.action="<%=CONTROLLEROPAV%>?estado=Clientes&accion=Ver&opcion=buscarcliente&idclie="+cod;
			formulario.submit();
        }
























        function openHiddenDiv(string){
            if(string == 'Emergencia'){
                $('divOpt').style.visibility = 'visible';
            }
            else{
                $('divOpt').style.visibility = 'hidden';
                $('divOpt').value = '';
            }
        }

        function denegarOfert(){
            formulario.target="_blank";
            formulario.action="<%=BASEURL%>/jsp/opav/anular_oferta.jsp?id_solicitud=<%=idsolicitud%>";
            formulario.submit();
        }

        function function1() {
            //alert('si funciona');
            var sw=0;
            for(var i=0;i<document.getElementById("nics").options.length;i++){
                if(document.getElementById("nic").value==document.getElementById("nics").options[i].value){
                    sw=1;
                }
            }

            if(document.getElementById("nic").value!=""&&sw==0){
                var optn = document.createElement("OPTION");
                optn.text = document.getElementById("nic").value;
                optn.value = document.getElementById("nic").value;
                document.getElementById("nics").options.add(optn);

                document.getElementById("niccs").innerHTML=document.getElementById("niccs").innerHTML+"<input name='nicc' type='hidden' id='nicc' value='"+document.getElementById("nic").value+"'>";
                document.getElementById("nic").value="";
            }
            else{
                if(sw==1 && document.getElementById("nic").value!=""){
                    alert("Ese Nic ya existe en la lista");
                }
                else{
                    alert("Nic invalido");
                }
            }
        }

        function submitt(form){
            var sw=0;
            for(var i=0;i<form.length;i++)
            {   if(((form.elements[i].type=='text' ||form.elements[i].type=='select') && form.elements[i].value=='')&&form.elements[i].id!='nic'&&form.elements[i].id!='nics'&&form.elements[i].id!='nic'&&form.elements[i].id!='client'&&form.elements[i].id!='idsolicitud'&&form.elements[i].id!='inpAviso')
                {   sw=1;
                }
            }

            if(document.getElementById('checkspec').checked){
                form.action = form.action + "&cliespec=true";
            }
            else{
                form.action = form.action + "&cliespec=false";
            }




            if(!(document.getElementById('tipo_identificacion').value!=''&&document.getElementById('zona').value!=''&&document.getElementById('ciudad').value!=''&&document.getElementById('sector').value!=''&&document.getElementById('ejecutivo_cta').value!=''))
            {   sw=1;
            }
            if(sw==0)
            {   form.submit();
            }
            else
            {   alert('Porfavor llene todos los campos requeridos para la creacion de un cliente');
            }
form.submit();

        }







		function submitt2(form,op){
            var sw=0;
            for(var i=0;i<form.length;i++){
                if(((form.elements[i].type=='text' ||form.elements[i].type=='select') && form.elements[i].value=='')&&form.elements[i].id!='nic'&&form.elements[i].id!='nics'&&form.elements[i].id!='opd'&&form.elements[i].id!='client'&&form.elements[i].id!='idsolicitud'&&form.elements[i].id!='inpAviso')
                {   sw=1;
                }
            }

            if(document.getElementById('checkspec').checked){
                form.action = form.action + "&cliespec=true";
            }
            else{
                form.action = form.action + "&cliespec=false";
            }

            if(sw==0){
				document.getElementById('ejecutivo_cta').disabled=false;
				document.getElementById('padre').disabled=false;

				if(op==1)
				{
				Valida_cliente();
				}
				else
				{
					form.submit();
				}
            }
            else
			{
                alert('Porfavor llene todos los campos requeridos para la modificacion de un cliente');
            }
        }









        function submittt(form){
            var sw2=0;

            if(!(document.getElementById('responsable').value!='' && document.getElementById('detalle_inconsistencia').value!=''&&document.getElementById('client').value!=''&&document.getElementById('nicsoff').value!=''&&(document.getElementById('tipo_solicitud').value!='' || document.getElementById('solicitud_asignada').value!='')))
            {   sw2=1;
            }
            if(sw2==0)
            {  form.submit();
            }
            else
            {   alert('Por favor llene todos los campos requeridos');
            }

        }

        function submitttt(form){
            var sw2=0;

            if(!(document.getElementById('descripcion').value!=''&&document.getElementById('contratista').value!=''))
            {
                sw2=1;
            }

            if(sw2==0)
            {
                form.submit();
            }
            else
            {
                alert('Porfavor llene todos los campos requeridos para la modificacion de una solicitud');
            }

        }




function cargarCiudades(id_dept, id_ciud){

    var dept = $(id_dept).value;

    var url = "<%=CONTROLLER%>?estado=Ciudad&accion=Search";
    var p = "opcion=cargarciu&dept="+dept+"&ciu="+id_ciud;
    new Ajax.Request(url, {
        parameters: p,
        method: 'post',
        onComplete:  function (resp){
            document.getElementById(id_ciud).innerHTML = resp.responseText;
        }
    });

}

function validarCartera(sel) {
    var sCar = document.getElementById('estado_cartera');
    if (sel.inicial === 'Emergencia' 
       && sel.options[sel.selectedIndex].value === 'Programado'
       && sCar.inicial !== 2) {
        document.getElementById('est_mar').setAttribute('value','Estudio');
        sCar.selectedIndex = 0;
    } else {
        sCar.selectedIndex = sCar.inicial;
        document.getElementById('est_mar').value=sCar.options[sCar.inicial].value;
    }
}





    </script>

    <%
            String[] dato1 = null;
            Usuario usuario = (Usuario) session.getAttribute("Usuario");
            CiudadService cs= new CiudadService(usuario.getBd());

            ArrayList optdep = cs.listadoDeps();
             ArrayList optciu = new ArrayList();
            ArrayList listadoResponsables = (ArrayList) session.getAttribute("listadoResponsables");
            ArrayList listadoInterventores = (ArrayList) session.getAttribute("listadoInterventores");
            String[] nics = (String[]) session.getAttribute("nics");
            String id_cliente = ((session.getAttribute("id_cliente")!= null) ? (String)session.getAttribute("id_cliente"):request.getParameter("idusuario"));

            session.removeAttribute("nics");
            String Retorno = request.getParameter("mensaje");
            String cli_creation_user = request.getParameter("creation_user");
            String sol_creation_user = request.getParameter("sol_creation_user");
            ClientesVerService clvsrv = new ClientesVerService(usuario.getBd());
            String perfil = clvsrv.getPerfil(usuario.getLogin());
           //String perfil = clvsrv.getPerfil("RTROCHA");
            ArrayList zona = clvsrv.getDatos("zona");
            ArrayList ciudad = clvsrv.getDatos("ciudad");
            ArrayList sector = clvsrv.getDatos("sector");
            ArrayList contratistas = new ArrayList();
            GestionSolicitudAiresAAEService sasrv = new GestionSolicitudAiresAAEService(usuario.getBd());

            if (tipo_sol.equals("AAAE")) {
                    contratistas = sasrv.buscarContratistasAAAE("P");
                    contratistas.addAll(sasrv.buscarContratistasAAAE("I"));
                    for (int i = 0; i < contratistas.size(); i++) {
                        String[] datos = {((String) contratistas.get(i)).split(";_;")[1], ((String) contratistas.get(i)).split(";_;")[0]};
                        contratistas.set(i, datos);
                    }
            } else {
                contratistas = clvsrv.getContratistas();
            }

           // boolean
             boolean vmc= true;
             if(id_cliente!= null)
                    vmc= clvsrv.Validar_modifcacion_cliente(id_cliente);


            ArrayList tipos_solicitud = clvsrv.getTiposSolicitud();//20100802
            ArrayList padres = clvsrv.getPadres();
            ArrayList estados = clvsrv.getEstados();
            ArrayList ejecutivo_cta = clvsrv.getEjecutivos();
            ArrayList acciones = (ArrayList) session.getAttribute("acciones");
            session.removeAttribute("acciones");
            String fechainicio = request.getParameter("fechainicio");
            String padre = request.getParameter("padre");
            String idusuario = request.getParameter("idusuario");
            String login_ejecutivo = request.getParameter("login_ejecutivo");
            System.out.println("login_ejecutivo:" + login_ejecutivo + "cli_creation_user" + sol_creation_user);
            System.out.println("perfil!!!:" + perfil + idusuario + idsolicitud);

            boolean oficial = clvsrv.isOficial(idusuario);
            String aviso = clvsrv.getAviso(idsolicitud);

            if (fechainicio == null) {
                fechainicio = "" + com.tsp.util.Util.AnoActual() + "-" + com.tsp.util.Util.getFechaActual_String(3) + "-" + com.tsp.util.Util.getFechaActual_String(5);
            }

            //INICIO DE PBASSIL EN 201002 movido en //20100226 de abajo a aca
            int actionState = 0;
            boolean denegable = true;//20100226
            if (acciones != null) {//20100226
                for (int i = 0; i < acciones.size(); i++) {

                    int state = Integer.parseInt(((AccionesEca) acciones.get(i)).getEstado());

                    if (state >= 90) {//MODIFICADO PARA CORREGIR
                        actionState = state;
                        denegable = false;//20100226
                    }
                }//SI EXISTE UNA ACCION QUE EST� IGUAL O MAYOR QUE "RECEPCION DE OBRA" QUEDA EN EL ESTADO ESE, SI NO 0
            }//20100226
            //FIN DE PBASSIL EN 201002

    %>
    <body onLoad="openHiddenDiv('0') ">
        <div id="capaSuperior" style="position:absolute; width:100%; height:100px; z-index:0; left: 0px; top: 0px;">
            <jsp:include page="/toptsp.jsp?encabezado=DATOS CLIENTE/SOLICITUD ELECTRICARIBE"/>
        </div>
        <div id="capaCentral" align="center" style="position:absolute; width:100%; height:87%; z-index:0; left: 0px; top: 100px;">

            <FORM name='formulario' method="post" action="<%=CONTROLLEROPAV%>?estado=Clientes&accion=Ver" id="formulario">
                <%/* try {*/%>
                <table width="432" height="159" border="2"align="center">
                    <tr><td colspan="2">
                            <table width="100%" height="72%" class="tablaInferior" >
                                <tr class="fila">
                                    <td colspan="2" align="left" nowrap class="subtitulo">&nbsp;Datos Cliente Electricaribe </td>
                                    <td colspan="2" align="left" nowrap class="bordereporte">&nbsp;</td>
                                </tr>

                                <tr class="fila">
                                    <td colspan="2" >Nombre</td>
                                    <td class="fila">
                                <input name="nombre" type="text" class="textbox" id="nombre" style="width:300;" size="15" maxlength="159"
                                value="<%=(request.getParameter("nombre") != null) ? request.getParameter("nombre") : ""%>"
								<%
								if((vmc==false)&&(request.getParameter("nombre") != null)){%> readonly="readonly" <%}%>
                                >
                                    </td>
                                </tr>

                                <tr class="fila">
                                    <td colspan="2" >Nics</td>
                                    <td class="fila">
                                        <select name='nics' multiple id="nics">
                                            <%for (int i = 0; nics != null && i < nics.length; i++) {%>
                                            <option value="<%=nics[i]%>"><%=nics[i]%></option>
                                            <%}%>
                                        </select>
                                    <input name="nic" type="text" class="textbox" id="nic" style="width:140;" size="15" maxlength="15"  onKeyPress="soloDigits(event)">
                                        <img src='<%=BASEURL%>/images/botones/iconos/mas.gif' width ="12" height="12" onClick="formulario.target='';function1();" title="Agregar" style="cursor:hand" >
                                        <div id="niccs"></div>
                                    </td>
                                </tr>

                                <tr class="fila">
                                    <td colspan="2" >NIT</td>
                                    <td class="fila">
                                        <input name="nit" type="text" class="textbox" id="nit" style="width:170;" size="15" maxlength="11" onblur="CalcularDv()" onKeyPress="return numbersonly(this, event)"
                                        value="<%=(request.getParameter("nit") != null) ? request.getParameter("nit") : ""%>"
										<%if(vmc==false){%> readonly="readonly" <%}%>>
                                        <span>DV&nbsp;</span><input name="digito_verificacion" type="text" class="textbox" id="digito_verificacion" style="width:30;" size="15" readonly value ="<%=(request.getParameter("digito_verif") != null) ? request.getParameter("digito_verif") : ""%>">
                                    </td>
                                </tr>

                                <tr class="fila">
                                    <td colspan="2" >Tipo de Cliente</td>
                                    <td class="fila">
                                    <select name='tipo_identificacion' id="tipo_identificacion">
                                    <% String tipo = (request.getParameter("tipo_identificacionn") != null) ? request.getParameter("tipo_identificacionn") : "";%>
                                <option value=''>...</option>
                                            <option value='R' <% if(tipo.equals("R")){%> selected <%}%> >R</option>
                                            <option value='NR' <% if(tipo.equals("NR")){%> selected <%}%> >NR</option>
                                        </select>
                                    </td>
                                </tr>

                                <tr class="fila">
                                    <td colspan="2" >Distrito</td>
                                    <td class="fila">

                                    <select name='zona' id="zona">
                                      <option value="">...</option>
                                            <%
											String cod_zona =  (request.getParameter("sectorr") != null) ? request.getParameter("sectorr") : "";
											for (int i = 0; i < zona.size(); i++) {%>
                                            <option value="<%=zona.get(i)%>"   <%if(zona.get(i).equals(cod_zona) ){ %> selected <%}%>   ><%=zona.get(i)%></option>
                                          <%}%>
                                        </select>
                                    </td>
                                </tr>

                                <tr class="fila">
                                    <td colspan="2" >Departamento</td>
                                    <td class="fila"><select name='sector' id="sector"   onChange="cargarCiudades('sector', 'ciudad');">
                                    <option value="">...</option>
                                        <%  dato1 = null;
                                            dato1 = null;
                                            String var_ciudad=(request.getParameter("ciudadd") != null) ? request.getParameter("ciudadd") : "";
                                            String cod_dpt=cs.getCodDtp(var_ciudad);
                                            for (int i = 0; i < optdep.size(); i++) {
                                                dato1 = ((String) optdep.get(i)).split(";_;");%>
                                                <option value="<%=dato1[0]%>" <%if(dato1[0].equals(cod_dpt) ){ %> selected<%}%> ><%=dato1[1]%></option>
                                        <%  }%>
                                    </select></td>
                                </tr>

                                <tr class="fila">
                                  <td colspan="2" >Ciudad</td>
                                  <td class="fila">
                                  <span id="ciudad">

                                       <select
                                          <%if(!var_ciudad.equals(""))
                                       {%>id="ciudad" name="ciudad" >
                                      <option value="">...</option>
                                      <%

                                      optciu = cs.listadoCiudades(cod_dpt);
                                      }
                                        for (int i = 0; i < optciu.size(); i++) {
                                            dato1 = ((String) optciu.get(i)).split(";_;");      %>
                                     <option value="<%=dato1[0]%>" <%if(dato1[0].equals(var_ciudad) ){ %> selected<%}%> >  <%=dato1[1]%></option>
                                      <%}%>
                                    </select>
                                  </span>
                                  </td>
                                </tr>
                                <tr class="fila">
                                    <td colspan="2" >Direccion</td>
                                    <td class="fila">
                                        <input name="direccion" type="text" class="textbox" id="direccion" style="width:170;" size="15" maxlength="60"
                                        value="<%=(request.getParameter("direccion") != null) ? request.getParameter("direccion") : ""%>">
                                    </td>
                                </tr>

                                <tr class="fila">
                                    <td colspan="2" >Nombre Contacto</td>
                                    <td class="fila">
                                        <input name="nombre_contacto" type="text" class="textbox" id="nombre_contacto" style="width:170;" size="15" maxlength="60" value="<%=(request.getParameter("nombre_contacto") != null) ? request.getParameter("nombre_contacto") : ""%>" >
                                    </td>
                                </tr>

                                <tr class="fila">
                                    <td colspan="2" >Telefono Contacto</td>
                                    <td class="fila">
                                        <input name="telefono_contacto" type="text" class="textbox" id="telefono_contacto" onKeyPress="soloDigits(event)" style="width:170;" size="15" maxlength="15" value="<%=(request.getParameter("telefono_contacto") != null) ? request.getParameter("telefono_contacto") : ""%>">
                                    </td>
                                </tr>

                                <tr class="fila">
                                    <td colspan="2" >Celular Contacto</td>
                                    <td class="fila">
                                        <input name="celular_contacto" type="text" class="textbox" id="celular" onKeyPress="soloDigits(event)" style="width:170;" size="15" maxlength="15" value="<%=(request.getParameter("celular_contacto") != null) ? request.getParameter("celular_contacto") : ""%>">
                                    </td>
                                </tr>
                                
                                <tr class="fila">
                                    <td colspan="2" >Email Contacto</td>
                                    <td class="fila">
                                        <input name="email_contacto" type="text" class="textbox" id="email_contacto" style="width:170;" size="15" maxlength="100" value="<%=(request.getParameter("email_contacto") != null) ? request.getParameter("email_contacto") : ""%>">
                                    </td>
                                </tr>                               

                                <tr class="fila">
                                    <td colspan="2" >Cargo Contacto</td>
                                    <td class="fila">
                                        <input name="cargo_contacto" type="text" class="textbox" id="cargo_contacto"
                                        style="width:170;" size="15" maxlength="15" value="<%=(request.getParameter("cargo_contacto") != null) ? request.getParameter("cargo_contacto") : ""%>">
                                    </td>
                                </tr>

                                <tr class="fila">
                                    <td colspan="2" >Nombre Representante Legal</td>
                                    <td class="fila">
                                        <input name="nombre_representante" type="text" class="textbox" id="nombre_representante" style="width:170;" size="15" maxlength="60" value="<%=(request.getParameter("nombre_representante") != null) ? request.getParameter("nombre_representante") : ""%>">
                                    </td>
                                </tr>

                                <tr class="fila">
                                    <td colspan="2" >Telefono Representante Legal</td>
                                    <td class="fila">
                                        <input name="telefono_representante" type="text" class="textbox" id="telefono_representante" onKeyPress="soloDigits(event)" style="width:170;" size="15" maxlength="15" value="<%=(request.getParameter("telefono_representante") != null) ? request.getParameter("telefono_representante") : ""%>">
                                    </td>
                                </tr>

                                <tr class="fila">
                                    <td colspan="2" >Celular Representante Legal</td>
                                    <td class="fila">
                                        <input name="celular_representante" type="text" class="textbox" id="celular_representante" onKeyPress="soloDigits(event)" style="width:170;" size="15" maxlength="60" value="<%=(request.getParameter("celular_representante") != null) ? request.getParameter("celular_representante") : ""%>">
                                    </td>
                                </tr>

                                <tr class="fila">
                                    <td colspan="2" >Ejecutivo de Cuenta</td>
                                    <td class="fila"><select name='ejecutivo_cta' id="ejecutivo_cta" <%if((vmc==false) && (clvsrv.ispermitted(perfil, "59")==false)){%> disabled <%}%> >
                                            <option value="">...</option>
                                            <% String ejecutivo =  (request.getParameter("ejecutivo_ctaa") != null) ? request.getParameter("ejecutivo_ctaa") : "";
                                            for (int i = 0; i < ejecutivo_cta.size(); i++) {%>
                                            <option value="<%=((String[]) ejecutivo_cta.get(i))[1]%>"   <%
                                            if(((String[]) ejecutivo_cta.get(i))[0].equals(ejecutivo) ){ %> selected <%}%>> <%=((String[]) ejecutivo_cta.get(i))[0]%></option>
                                            <%}%>
                                        </select>

                                    </td>
                                </tr>

                                <tr class="fila">
                                    <td colspan="2" >Cliente padre (Opcional)     </td>
                                    <td><select name='padre' id="padre"
										 <%if((vmc==false) && (clvsrv.ispermitted(perfil, "59")==false)){%> disabled <%}%> >
                                      <option value="">...</option>
                                             <% String cl_padre =  (request.getParameter("padre") != null) ? request.getParameter("padre") : "";

                                            for (int i = 0; i < padres.size(); i++) {%>
                                            <option value="<%=((String[]) padres.get(i))[1]%>"   <%if(((String[]) padres.get(i))[1].equals(cl_padre) ){ %> selected <%}%>   >
                                             <%=((String[]) padres.get(i))[0]%>-<%=((String[]) padres.get(i))[1]%></option>
                                          <%}%>
                                        </select></td>
                                </tr>

                                <tr class="fila">
                                    <td colspan="2" >Cliente especial </td>
                                    <td>
                                        <%
                                        if (oficial) {
                                        %>
                                        <input type="checkbox" id="checkspec" name="checkspec" value="true" checked>
                                        <%}else{
                                        %>
                                        <input type="checkbox" id="checkspec" name="checkspec" value="true">
                                        <%}
                                        %>
                                        </td>
                                </tr>

                                                                <tr class="fila">
                                    <td colspan="2" >Edificio</td>
                                    <td>
                                        <%
            if (edificio.equals("S")) { %>
                                          <input type="checkbox" id="edificio" name="edificio" value="true" checked>
                                          <%} else {
                                        %>
                                        <input type="checkbox" id="edificio" name="edificio" value="true">
                                    <%}
                                        %>
                                        </td>
                                </tr>

                            </table>
                            <table width="100%">
<tr class="fila">
                                    <td>
                                        <br>
                                    </td>
                                    <td>
                                        <br>
                                    </td>
                                </tr>
                                <tr class="fila">
                                    <%if ((idusuario == null) && (clvsrv.ispermitted(perfil, "5") || clvsrv.ispermitted(perfil, "6"))) {%>
                                    <td colspan="2" ><div align="center"><img src="<%=BASEURL%>/images/botones/aceptar.gif"  name="imgaceptar"
                                    onClick="formulario.target='';formulario.action='<%=CONTROLLEROPAV%>?estado=Clientes&accion=Ver'+'&opcion=creacliente';submitt2(formulario,1);" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" style="cursor:hand"></div>
                                    </td>
                                    <%} else {%>
                                    <%if (((idusuario != null) && (clvsrv.ispermitted(perfil, "5") || cli_creation_user.equals(usuario.getLogin()) || (login_ejecutivo != null && login_ejecutivo.equals(usuario.getLogin())))) || clvsrv.ispermitted(perfil, "6")) {%>
                                    <td colspan="2" ><div align="center"><img src="<%=BASEURL%>/images/botones/modificar.gif"  name="imgsalir"  onMouseOver="botonOver(this);" onClick="formulario.target='';formulario.action='<%=CONTROLLEROPAV%>?estado=Clientes&accion=Ver'+'&opcion=modcliente';submitt2(formulario,0);" onMouseOut="botonOut(this);" style="cursor:hand" ></div>
                                    </td>
                                    <%}
            }%>
                                </tr>
                                <tr class="fila">
                                    <td>
                                        <br>
                                    </td>
                                    <td>
                                        <br>
                                    </td>
                                </tr>
                            </table>
                    </tr>
                    <tr>
                        <%  if (idusuario != null) {%>
                        <td valign="top" >
                            <table>
                                <tr class="fila">
                                    <td colspan="2" align="left" nowrap class="subtitulo">&nbsp;Solicitud</td>
                                    <td colspan="2" align="left" nowrap class="bordereporte"> </td>
                                </tr>

                                <tr class="fila">
                                    <td colspan="2" >Solicitud</td>
                                    <td class="fila">
                                        <input  readonly name="idsolicitud" type="text" id="idsolicitud" class="textbox" value="<%=(idsolicitud != null) ? idsolicitud : ""%>">
                                    </td>
                                </tr>

                                <tr class="fila">
                                    <td colspan="2" >Cliente</td>
                                    <td class="fila">
                                        <input name="client" type="text" class="textbox" id="client" value="<%=(request.getParameter("nombre") != null) ? request.getParameter("nombre") : ""%>" size="80"  maxlength="80" readonly>
                                        <input name="cliente" type="hidden" id="cliente" value="<%=(idusuario != null) ? idusuario : ""%>">
                                    </td>
                                </tr>
                                <tr class="fila">
                                    <td colspan="2" >Solicitud Asignada</td>
                                    <td class="fila" style="color:red"><%=(request.getParameter("tipo_solicitudd")!= null)?request.getParameter("tipo_solicitudd"):"NR"%></td>
                                    <input name="solicitud_asignada" type="hidden" id="solicitud_asignada" value="<%=(request.getParameter("tipo_solicitudd")!= null)?request.getParameter("tipo_solicitudd"):""%>">
                                </tr>

                                <tr class="fila">
                                    <td colspan="2" >Tipo de Solicitud</td>
                                    <td class="fila">
                                        <%if( idsolicitud != null && !(idsolicitud.equals("")) )
										{											
										if(clvsrv.ispermitted(perfil, "33") &&!(clvsrv.estadoSolicitud(idsolicitud).equals("110")||clvsrv.estadoSolicitud(idsolicitud).equals("667"))){
                                                %>
                                                    <select  name='tipo_solicitud' id="tipo_solicitud" onchange="validarCartera(this)" >
                                                        <option value=''>...</option>
                                                                                                        <%
                                                        for (int i = 0; i < tipos_solicitud.size(); i++) {%>
                                                            <option value="<%=((String[]) tipos_solicitud.get(i))[1]%>" <% if (request.getParameter("tipo_solicitudd").equals(((String[]) tipos_solicitud.get(i))[1])){out.print(" selected");}%>  ><%=((String[]) tipos_solicitud.get(i))[0]%></option>
                                                        <%} %>
                                                    </select>
                                                    <script> document.getElementById('tipo_solicitud').inicial="<%=request.getParameter("tipo_solicitudd")%>";</script>


                        <%

                                                        }
                                                        else{
                                                %>
                                                    <input name="tipo_solicitud" type="hidden" class="textbox" id="tipo_solicitud" size="15" maxlength="15" value="<%=(request.getParameter("tipo_solicitudd") != null) ? request.getParameter("tipo_solicitudd") : ""%>"><!--20100604-->
                                                    <input name="tipo_solicitudd" type="text" class="textbox" id="tipo_solicitudd" style="width:100;" size="15" maxlength="15" value="<%=(request.getParameter("tipo_solicitudd") != null) ? request.getParameter("tipo_solicitudd") : ""%>" readonly >

                               
                                                    <%}
                                                    %>
        <%
                                                            String opd = aviso!=null?aviso:"";//<!-- 2010-06-12 rhonalf -->
                                                            if (!(request.getParameter("tipo_solicitudd").equals("Emergencia"))){//<!-- 2010-06-12 rhonalf -->
                                                    %>
                                                            OPD: <input type="text" id="opd" name="opd" value="<%= opd %>" <% if(!clvsrv.ispermitted(perfil, "33")){%> readonly <%}%> ><!-- 2010-06-12 rhonalf -->



                                                            <%
                                                            }
                                                            else
                                                                {
                                                            
                                                    if( aviso != null && !(aviso.equals("")) ){%>
                                                        Aviso <input id="inpAviso" name="inpAviso" value="<%=aviso%>" readonly>
                                                    <%}
                                                        }
                                                    %>
                                            <%} else {%>
                                        <select onChange="openHiddenDiv( $('tipo_solicitud').value );" name='tipo_solicitud' id="tipo_solicitud">
                                            <option value=''>...</option>
                                            	    <%
                                            for (int i = 0; i < tipos_solicitud.size(); i++) {%>
                                                <option value="<%=((String[]) tipos_solicitud.get(i))[1]%>"><%=((String[]) tipos_solicitud.get(i))[0]%></option>
                                            <%} %>

                                        </select>

                                        <input name="tipo_solicitudd" type="hidden" class="textbox" id="tipo_solicitudd" size="15" maxlength="15" value="">
                                        OPD: <input type="text" id="opd" name="opd" value=""><!-- 2010-06-12 rhonalf -->

                                        <table width="374">
                                            <tr>
                                                <td width="105">&nbsp;</td>
                                                <td width="257" class="fila">
                                                    <div id="divOpt" name="divOpt">
                                                        Aviso <input id="inpAviso" name="inpAviso" value="">
                                                    </div></td>

                                            </tr>
                                        </table>

                                        <%}%>
                                    </td>
                                </tr>

                                <%
String responsable = request.getParameter("responsable") == null ? "" : request.getParameter("responsable");
String interventor = request.getParameter("interventor") == null ? "" : request.getParameter("interventor");
%>

                                <tr class="fila">
                                    <td colspan="2" >Responsable</td>
                                    <td class="fila">
									<select name='responsable' id="responsable">
                                      <option value="">...</option>
                                      <%for (int i = 0; i < listadoResponsables.size(); i++) {%>
                                      <option value="<%=((String[]) listadoResponsables.get(i))[0]%>" <% if (((String[]) listadoResponsables.get(i))[0].equals(responsable)) { out.print("selected"); }%>><%=((String[]) listadoResponsables.get(i))[1]%></option>
                                      <%}%>
                                    </select></td>
                                </tr>

                                <tr class="fila">
<!--                                    <% if(!clvsrv.ispermitted(perfil, "33")){%> style="display: none;" <%}%>-->
                                   
                                    <td colspan="2" >Interventor</td>
                                    <td class="fila">
                                        <select name='interventor' id="interventor"> <!--disabled-->
                                            <%for (int i = 0; i < listadoInterventores.size(); i++) {%>
                                            <option value="<%=((Interventor) listadoInterventores.get(i)).getCodigo()%>" <% if (((Interventor) listadoInterventores.get(i)).getCodigo().equals(interventor)) { out.print("selected"); }%>><%=((Interventor) listadoInterventores.get(i)).getNombre()%></option>
                                            <%}%>
                                        </select></td>
                                </tr>
                                
                                <tr class="fila">
                                    <td colspan="2" >Nic</td>
                                    <td class="fila">
                                        <%if (request.getParameter("nicsof") != null) {%>
                                        <input name="nicsoff" type="text" class="textbox" id="nicsoff" value="<%=(request.getParameter("nicsof") != null) ? request.getParameter("nicsof") : ""%>" style="width:170;" size="15"  maxlength="80" readonly>
                                        <%} else {%>
                                        <select name='nicsoff' id="nicsoff">
                                            <%for (int i = 0; nics != null && i < nics.length; i++) {%>
                                            <option value="<%=nics[i]%>"><%=nics[i]%></option>
                                            <%}%>
                                        </select>
                                        <%}%>
                                    </td>
                                </tr>
                                <%  System.out.println("print1");
                            if (request.getParameter("historico") != null) {%>
                                <tr class="fila">
                                    <td colspan="2" >Historico Situacion</td>
                                    <td class="fila">
                                        <textarea name="historico"  id="historico" rows="12" cols="50" readonly><%=(request.getParameter("historico") != null) ? request.getParameter("historico") : ""%></textarea>
                                    </td>
                                </tr>
                                <%}%>
                                <tr class="fila">
                                    <td colspan="2" >Situacion</td>
                                    <td class="fila">
                                        <textarea name="detalle_inconsistencia"  id="detalle_inconsistencia" rows="5" cols="50" ><%=(request.getParameter("detalle_inconsistenciaa") != null) ? request.getParameter("detalle_inconsistenciaa") : ""%></textarea>
                                    </td>
                                </tr>
                                <tr class="fila">
                                    <td colspan="2" >Fecha Solicitud</td>
                                    <td valign="middle" ><table width="100%" border="0">
                                            <tr class="fila">
                                                <td width="39%"><input  name='fecha_oferta' size="20" readonly="true" class="textbox" value='<%=fechainicio%>' style='width:60%'></td>
                                                <td width="26%" >Estado de Cartera </td>
                                                <td width="35%"><label>
                                                        <% //JJCASTRO
                                         String esca = request.getParameter("estado_cartera") == null ? "Estudio" : request.getParameter("estado_cartera");


                                                        %>
                                                          <select name="estado_cartera" id="estado_cartera" <%if ( clvsrv.ispermitted(perfil, "57")==false) {%>   disabled="disabled"  <%}%>>
                                                            <option value="Estudio" <% if (esca.equals("Estudio")) {
                                out.print("selected");
                            }%>>Estudio</option>
                                                            <option value="010"     <% if (esca.equals("010")) {
                                out.print("selected");
                            }%>>Aprobada</option>
                                                            <option value="000"     <% if (esca.equals("000")) {
                                out.print("selected");
                            }%>>Rechazada</option>
                                                        </select>
                                                        <script> var sel = document.getElementById('estado_cartera'); sel.inicial=sel.selectedIndex;</script>
                                                        <br>
                                                        <input   type="hidden" name="est_mar" id="est_mar"  <%if (esca.equals("010") || esca.equals("000")) {%>   value="<%=esca%>"    <%} else {%> value=""  <%}%>  >






                                                        <input type="text" name="fec_val_cartera" id="fec_val_cartera" value="<%=(request.getParameter("fec_val_cartera2") != null) ? request.getParameter("fec_val_cartera2") : "Sin Fecha"%>"  readonly>
                                                    </label></td>
                                            </tr>
                                        </table>                                    </td>

                                </tr>

                            </table>

                            <table width="100%">
                                <tr class="fila">
                                    <td><br>
                                    </td>
                                    <td>
                                        <br>
                                    </td>
                                </tr>
                                <tr class="fila">
                                    <%  System.out.println("print2");
                            if ((idsolicitud == null || idsolicitud.equals("")) && (clvsrv.ispermitted(perfil, "1"))) {%>
                                    <td colspan="2" ><div align="center">
                                            <img src="<%=BASEURL%>/images/botones/aceptar.gif"  name="imgaceptar" onClick="formulario.target='';formulario.action='<%=CONTROLLEROPAV%>?estado=Clientes&accion=Ver'+'&opcion=creasolicitud';submittt(formulario);" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" style="cursor:hand"></div>
                                    </td>
                                    <%} else {
                                        if (((idsolicitud != null) && (clvsrv.ispermitted(perfil, "2") || (sol_creation_user != null && sol_creation_user.equals(usuario.getLogin()) || login_ejecutivo.equals(usuario.getLogin())))) || clvsrv.ispermitted(perfil, "6")) {%>
                                    <td colspan="2" ><div align="center">
                                            <img src="<%=BASEURL%>/images/botones/modificar.gif"  name="imgsalir"  onMouseOver="botonOver(this);" onClick="formulario.target='';formulario.action='<%=CONTROLLEROPAV%>?estado=Clientes&accion=Ver'+'&opcion=modsolicitud';submittt(formulario);" onMouseOut="botonOut(this);" style="cursor:hand" >

                     <% if(clvsrv.ispermitted(perfil, "58")) { %>
                 <img src="<%=BASEURL%>/images/botones/generarPdf.gif" name="imgimprimir"
                 onMouseOver="botonOver(this);" onClick="window.open('<%=BASEURL%>/jsp/opav/generar_oferta_pdf.jsp?id_solicitud=<%=(idsolicitud != null) ? idsolicitud : ""%>')"
                  onMouseOut="botonOut(this);" style="cursor:hand">
                  <%}%>

                                            </div>
                                    </td>
                                    <%}
                            }%>
                                </tr>
                            </table>


                        </td>
                        <%}%>
                    <%System.out.println("print3" + idsolicitud);
            if (idsolicitud != null && !idsolicitud.equals("")) {%>
                        <td valign="top" >
                            <table>
                                <tr class="fila">
                                    <td colspan="2" align="left" nowrap class="subtitulo">&nbsp;Acciones</td>
                                    <td colspan="2" align="left" nowrap class="bordereporte"> </td>
                                </tr>
                                <tr class="fila">
                                    <td colspan="2" >Contratista</td>
                                    <td class="fila">
                                        <select name='contratista' id="contratista">
                                            <option value="">...</option>
                                            <%
                            System.out.println("print4");
                            for (int i = 0; i < contratistas.size(); i++) {%>
                                            <option value="<%=((String[]) contratistas.get(i))[1]%>"><%=((String[]) contratistas.get(i))[0]%></option>
                                            <%}
                            System.out.println("print5");
                                            %>
                                        </select>
                                    </td>
                                </tr>

                                <tr class="fila">
                                    <td colspan="2">Descripcion</td>
                                    <td class="fila">
                                        <textarea name="descripcion"  id="descripcion" rows="5" cols="50" ><%=(request.getParameter("desc") != null) ? request.getParameter("desc") : ""%></textarea>
                                    </td>
                                </tr>
                            </table>

                            <table width="100%">
                                <tr class="fila">
                                    <td>
                                        <br>
                                    </td>
                                    <td>
                                        <br>
                                    </td>
                                </tr>
                                <%

                            System.out.println("print7");
                            if (clvsrv.ispermitted(perfil, "3") || (sol_creation_user.equals(usuario.getLogin()) || login_ejecutivo.equals(usuario.getLogin()))) {
                                System.out.println("print8");
                                %>


                                <tr class="fila">
                                    <td colspan="2" >

                                        <div align="center">
                                            <%if (modelopav.ElectricaribeOfertaSvc.maxEstado(idsolicitud) <= 100) {%><!--SI TODAS LAS ACCIONES SON MENORES QUE INGRESADO EN OPEN-->

                                            <%
    String esca = request.getParameter("estado_cartera") == null ? "Estudio" : request.getParameter("estado_cartera");
    if (esca.equals("010")) {%>
                                            <img src="<%=BASEURL%>/images/botones/agregar.gif"  name="imgaceptar" onClick="formulario.target='';formulario.action='<%=CONTROLLEROPAV%>?estado=Clientes&accion=Ver'+'&opcion=creaaccion';submitttt(formulario);" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" style="cursor:hand">
                                            <%}%>
                                            <%}%>
                                        </div>
                                    </td>
                                </tr>
                                <%}
                            System.out.println("print9");
                                %>
                            </table>

                            <% if (acciones != null && acciones.size() > 0) {%>
                            <table width="100%">
                                <tr class="fila">
                                    <td>Contratista</td><td>Descripcion</td><td>Fecha Creacion</td><td>Observacion</td><td>Estado</td><td>Tipo de Trabajo</td><td>Opciones</td>
                                </tr>
                                <%
     System.out.println("print11");

     for (int i = 0; i < acciones.size(); i++) {
         System.out.println("print12");
         AccionesEca temp = ((AccionesEca) acciones.get(i));
         String contratemp = "";
         String estadostemp = "";%>
                                <tr class="fila">
                                <%
                                   System.out.println("print13");
                                   for (int j = 0; j < contratistas.size(); j++) {
                                       if (temp.getContratista().equals(((String[]) contratistas.get(j))[1])) {
                                           contratemp = ((String[]) contratistas.get(j))[0];
                                       }
                                   }
                                   for (int j = 0; j < estados.size(); j++) {
                                       if (temp.getEstado().equals(((String[]) estados.get(j))[1])) {
                                           estadostemp = ((String[]) estados.get(j))[0];
                                       }
                                   }
                                    %>
                                    <td title="<%=temp.getContratista()%>"><%=contratemp%></td>
                                    <td><%=temp.getDescripcion()%></td>
                                    <td><%=temp.getCreation_date().substring(0, 16)%></td>
                                    <td>
                                        <a target="_self" href="<%=CONTROLLEROPAV%>?estado=Negocios&accion=Applus&opcion=consultarAccion&id_solici=<%=  temp.getId_solicitud()%>&id_accion=<%=temp.getId_accion()%>">
                                            <%=(temp.getObservaciones().length() > 50) ? temp.getObservaciones().substring(0, 50) + "..." : ((temp.getObservaciones().equals("")) ? "agregar" : temp.getObservaciones())%>
                                        </a>
                                    </td>
                                    <td title="<%=estadostemp%>"><%=temp.getEstado()%></td>
                                    <td><%=temp.getTipo_trabajo()%></td>
                                    <td align="center">
                                        <%
                                    //System.out.println("prin14");
                                    int estado = Integer.parseInt(temp.getEstado());

                                    if (clvsrv.ispermitted(perfil, "4") || sol_creation_user.equals(usuario.getLogin()) || login_ejecutivo.equals(usuario.getLogin()) || clvsrv.ispermitted(perfil, "6")) {%>

                                    <%if (modelopav.ElectricaribeOfertaSvc.minEstado(idsolicitud) >= 20 && modelopav.ElectricaribeOfertaSvc.maxEstado(idsolicitud) <= 100 && clvsrv.ispermitted(perfil, "3")) {%><!--si el estado es menor que "INGRESADO EN OPEN" y tiene permiso PARA MODIFICAR ACCIONES-->

                                        <img src='<%=BASEURL%>/images/botones/iconos/menos1.gif' width="20" height="20" onClick="formulario.action='<%=CONTROLLEROPAV%>?estado=Clientes&accion=Ver'+'&opcion=eliminaaccion&idaccion=<%=temp.getId_accion()%>';if(confirm('¿Seguro quiere elminar esta accion?')){formulario.submit()};" title="Eliminar" style="cursor:hand" >
                                        <%}%>
                                        <%if (!(temp.getEstado().equals("020")) && !(temp.getEstado().equals("667"))) {//091029pm%>

                                        <%if (temp.getEstado().equals("030")) {%>
                                        <img src='<%=BASEURL%>/images/botones/iconos/clasificar.gif'        width="20" height="20" onClick="formulario.action='<%=BASEURL%>/jsp/opav/cotizacion/listadoitemcotizacion.jsp?operacion=guardar&idaccion=<%=temp.getId_accion()%>';formulario.target='_self';formulario.submit();" title="Generar Cotizacion" style="cursor:hand" >
            <!-- opcion adicional-->
<%if (clvsrv.ispermitted(perfil,"35")) {%>

                                                    <img src='<%=BASEURL%>/images/botones/iconos/iconsubir.gif'        width="21" height="21" onClick="formulario.action='<%=BASEURL%>/jsp/adminfile/importar.jsp?num_osx=<%=temp.getId_accion()%>&tipito=id_accion&swSubir=1';formulario.target='_blank';formulario.submit();" title="Subir Archivo" style="cursor:hand" >
<% }else{ %>

<% if(clvsrv.ispermitted(perfil,"36")){ %>
                                                    <img src='<%=BASEURL%>/images/botones/iconos/icondes.gif'        width="16" height="20" onClick="formulario.action='<%=BASEURL%>/jsp/adminfile/importar.jsp.jsp?num_osx=<%=temp.getId_accion()%>&tipito=id_accion&swSubir=0';formulario.target='_blank';formulario.submit();" title="Mostrar Archivo" style="cursor:hand" >

<%} }%>

<!-- opcion adicional-->
										<%} else {%>
                                        <img src='<%=BASEURL%>/images/botones/iconos/srchclasificar.gif'    width="20" height="20" onClick="formulario.action='<%=CONTROLLEROPAV%>?estado=Cotizacion&accion=Ver&consecutivo=<%=temp.getId_accion()%>';formulario.target='_blank';formulario.submit();" title="Consultar Cotizacion" style="cursor:hand" >
                                    <%if ((estado < 90) || ((estado > 80) && (estado < 110) && clvsrv.ispermitted(perfil, "21"))) {%><!--SI EL ESTADO ES MENOR QUE RECEPCION DE OBRA O (EL ESTADO ES MAYOR QUE EN EJECUCION Y EL ESTADO ES MENOR QUE INGRESADO EN OPEN CON PERMISO ESPECIAL)-->
                                        <img src='<%=BASEURL%>/images/botones/iconos/aiu.gif'               width="20" height="20" onClick="formulario.action='<%=BASEURL%>/jsp/opav/cotizacion/aiu_cotizacion.jsp?idaccion=<%=temp.getId_accion()%>';formulario.target='_blank';formulario.submit();" title="AIU" style="cursor:hand" >
                                        <%}%>
                                        <%}%>

                                        <%}//091029pm%>
                                        <%}%>
                                    </td>
                                </tr>
                                <%}%>
                          </table>
                            <% }%>
                      </td>
                        <%}%>
                    </tr>
                </table>
                <%/*}catch(Exception e)
            {   e.printStackTrace();
            System.out.println(e.getMessage());
            System.out.println(e.toString());
            }*/%>
            </FORM>

            <div align="center">
                <img src="<%=BASEURL%>/images/botones/restablecer.gif"  name="imgrefrescar" onClick="formulario.target='';formulario.action='<%=CONTROLLEROPAV%>?estado=Clientes&accion=Ver'+'&opcion=refrescarx';formulario.submit();" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" style="cursor:hand">
                <img src="<%= BASEURL%>/images/botones/salir.gif" name="imgsalir" alt="Salir" onMouseOver="botonOver(this);" onClick="parent.close();" onMouseOut="botonOut(this);" style="cursor:pointer ">
            </div>


            <%String mensaje = "";
            if (Retorno != null && !(Retorno.equals(""))) {%>
            <br>
            <table align="center" width="458" height="70">
                <tr>
                    <td align="center">
                        <table border="2" width="250" height="70" border="2" align="center">
                            <tr>
                                <td align="center">
                                    <table width="100%" height="100%"  border="1" align="center"  bordercolor="#F7F5F4" bgcolor="#FFFFFF">
                                        <tr>
                                            <td width="229" align="center" class="mensajes"><%=Retorno%></td>
                                            <td width="29" background="<%=BASEURL%>/images/cuadronaranja.JPG">&nbsp;</td>
                                            <td width="58">&nbsp;</td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
            </table>
            <%}%>


        </div>
        <iframe width="188" height="166" name="gToday:normal:agenda.js:gfPop:plugins.js" id="gToday:normal:agenda.js:gfPop:plugins_24.js" src="<%=BASEURL%>/js/Calendario/ipopeng.htm" scrolling="yes" frameborder="0" style="visibility:visible; z-index:999; position:absolute; left:-500px; top:0px;">
        </iframe>


    </body>
</html>