<%--
    Document   : seguimientoSolicitudes
    Created on : 7/05/2010, 08:30:41 AM
    Author     : darrieta - GEOTECH
--%>

<%@page import="com.tsp.operation.model.beans.Usuario"%>
<%@page contentType="text/html" pageEncoding="ISO-8859-1"%>
<%@page session ="true"%>
<%@page errorPage ="/error/ErrorPage.jsp"%>
<%@page import ="java.util.*" %>
<%@page import ="com.tsp.opav.model.*"%>
<%@page import ="com.tsp.opav.model.beans.*"%>
<%@page import ="com.tsp.opav.model.services.*"%>
<%@include file ="/WEB-INF/InitModel.jsp"%>

<%@ taglib uri="http://jsptags.com/tags/navigation/pager" prefix="pg" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">

<%
            ArrayList listaEstados = modelopav.NegociosApplusService.getEstadosApplus2();
            ArrayList contratistas = modelopav.NegociosApplusService.getContratistas();
            ArrayList consulta = modelopav.NegociosApplusService.getLista();
            Usuario usuario = (Usuario) session.getAttribute("Usuario");
            ClientesVerService clvsrv = new ClientesVerService(usuario.getBd());
            ArrayList departamentos = clvsrv.listarDepartamentos();


String cod_responsable = request.getParameter("responsable")!=null?request.getParameter("responsable"):"";
String cuadro_informe = request.getParameter("cuadro_informe")!=null?request.getParameter("cuadro_informe"):"";//----responsable opav jcastro

%>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
        <title>Seguimiento de solicitudes</title>
        <link href="<%= BASEURL%>/css/estilostsp.css" rel='stylesheet'>
        <script type='text/javascript' src="<%= BASEURL%>/js/boton.js"></script>
        <script type='text/javascript' src="<%= BASEURL%>/js/prototype.js"></script>
        <script type='text/javascript' src="<%= BASEURL%>/js/scriptaculous/scriptaculous.js"></script>
        <script type='text/javascript' src="<%= BASEURL%>/js/seguimientoSolicitudes.js"></script>
        <style>
            .grande{
                width: 350px;
            }
            div.autocomplete {
                position:absolute;
                width:250px;
                background-color:white;
                border:1px solid #888;
                margin:0;
                padding:0;
            }
            div.autocomplete ul {
                list-style-type:none;
                margin:0;
                padding:0;
            }
            div.autocomplete ul li.selected { background-color: #D7FFD7;}
            div.autocomplete ul li {
                list-style-type:none;
                display:block;
                margin:0;
                padding:2px;
                height:20px;
                cursor:pointer;
            }
            #tabResultados td{
                text-align: center;
            }
        </style>
    </head>
    <body>
        <div id="capaSuperior" style="position:absolute; width:100%; height:100px; z-index:0; left: 0px; top: 0px;">
            <jsp:include page="/WEB-INF/toptsp.jsp?encabezado=Seguimiento de solicitudes"/>
        </div>
        <div id="capaCentral" style="position:absolute; width:100%; height:100%; z-index:0; left: 0px; top: 100px; <%--overflow: scroll;--%>">
            <form method='post' name='form1' action="<%=CONTROLLEROPAV%>?estado=Negocios&accion=Applus&opcion=consultarSolicitudes">
                <table width='80%' align="center" style="border: green solid 1px;">
                    <tr>
                        <th colspan='4' class="barratitulo">
                            <table width="100%">
                                <tr>
                                    <td width="40%" class="subtitulo1">Seguimiento de solicitudes</td>
                                    <td width="60%"><img src="<%=BASEURL%>/images/titulo.gif" width="32" height="20" align="left"></td>
                                </tr>
                            </table>                        </th>
                    </tr>
                    <tr class='fila'>
                        <td>Estado actual</td>
                        <td>
                            <select name="cmbEstado" id="cmbEstado" style="width:270px;" onchange="mostrarVencido()">
                                <option value="-1">&nbsp;</option>
                                <%for (int i = 0; i < listaEstados.size(); i++) {
                                      String[] estado = (String[]) listaEstados.get(i);
                                      if (estado[0].compareTo("000") >= 0 && estado[0].compareTo("060") <= 0) {
                                %>
                                <option value="<%=estado[0]%>" <%=estado[0].equals(request.getParameter("estadoSol"))?"selected":""%>><%=estado[1]%></option>
                                <%    }
                                            }%>
                            </select>
                            <span id="spanVencido">
                                &nbsp;&nbsp;&nbsp;Vencido<input type="checkbox" name="chkVencido" id="chkVencido">
                            </span>                        </td>
                        <td>Tipo Solicitud </td>
                      <td>


                                        <%
                                        String tipo_solicitud = request.getParameter("tipo_solicitudd")!=null?request.getParameter("tipo_solicitudd"):"";
                                        %>
					<select  name='tipo_solicitud' id="tipo_solicitud">
						<option value=''>...</option>
						<option value='Programado' <% if (tipo_solicitud.equals("Programado")){out.print(" selected");}%>>Programado</option>
						<option value='Emergencia' <% if (tipo_solicitud.equals("Emergencia")){out.print(" selected");}%>>Emergencia</option>
						<option value='ValorAgregado' <% if (tipo_solicitud.equals("ValorAgregado")){out.print(" selected");}%>>ValorAgregado</option>
					</select>

					  </td>
                    </tr>
                    <tr class='fila'>
                        <td>Nombre cliente</td>
                        <td>
                            <input type="text" name="txtCliente" id="txtCliente" class="grande" value="<%=request.getParameter("cliente")==null?"":request.getParameter("cliente")%>">
                            <div id="divCliente" class="autocomplete"></div>
                            <script type="text/javascript" language="javascript" charset="utf-8">
                                // <![CDATA[
                                new Ajax.Autocompleter('txtCliente','divCliente',"<%=CONTROLLEROPAV%>?estado=Negocios&accion=Applus&opcion=buscarCliente",{paramName:'txtCliente'});
                                // ]]>
                            </script>                        </td>
                        <td>Id solicitud</td>
                        <td><input type="text" name="txtSolicitud" id="txtSolicitud" value="<%=request.getParameter("solicitud")==null?"":request.getParameter("solicitud")%>"></td>
                    </tr>
                    <tr class='fila'>
                        <td>Ejecutivo</td>
                        <td>
                            <input type="text" name="txtEjecutivo" id="txtEjecutivo" class="grande" value="<%=request.getParameter("ejecutivo")==null?"":request.getParameter("ejecutivo")%>">
                            <div id="divEjecutivo" class="autocomplete"></div>
                            <script type="text/javascript" language="javascript" charset="utf-8">
                                // <![CDATA[
                                new Ajax.Autocompleter('txtEjecutivo','divEjecutivo',"<%=CONTROLLEROPAV%>?estado=Negocios&accion=Applus&opcion=buscarEjecutivo",{paramName:'txtEjecutivo'});
                                // ]]>
                            </script>                        </td>
                        <td>Departamento</td>
                        <td><select name="cmbDepartamento" id="cmbDepartamento">
                            <option value="-1" selected="selected"> </option>
                            <%for (int i = 0; i < departamentos.size(); i++) {%>
                            <option value="<%=((String[]) departamentos.get(i))[0]%>" <%=((String[]) departamentos.get(i))[0].equals(request.getParameter("departamento"))?"selected":""%> ><%=((String[]) departamentos.get(i))[1]%></option>
                            <%}%>
                        </select></td>
                    </tr>
                    <tr class='fila'>
                        <td>Contratista</td>
                        <td>
                            <select name="cmbContratista" id="cmbContratista" class="grande">
                                <option value="-1" selected="selected"> </option>
                                <%for (int i = 0; i < contratistas.size(); i++) {
                                                String[] contratista = (String[]) contratistas.get(i);
                                %>
                                <option value="<%=contratista[0]%>" <%=contratista[0].equals(request.getParameter("contratista"))?"selected":""%> ><%=contratista[1]%></option>
                                <%}%>
                            </select>                        </td>
                        <td>Consecutivo</td>
                        <td><input type="text" name="txtConsecutivo" id="txtConsecutivo" value="<%=request.getParameter("consecutivo")==null?"":request.getParameter("consecutivo")%>"></td>
                    </tr>





					<tr>

					<td class='fila'> Responsable</td>
					<td class='fila'>
				    <select name="responsable" id="responsable">
    	            <option value="0" selected="selected">Seleccione un Responsable</option>
	    	        </select>
					</td>
					<td class="fila">Nic</td>
					<td class="fila"><input type="text" name="txtNic" id="txtNic" value="<%=request.getParameter("nic")==null?"":request.getParameter("nic")%>"></td>
					</tr>







                    <tr class='fila'>
                        <td colspan="2">
                            <label style="margin-right: 25px;">
                                Fecha inicial solicitud
                                <tsp:Date formulario="form1" clase="textbox" name="txtFechaInicial" otros="size='20'" fechaInicial='<%=request.getParameter("fechaInicial")==null?"":request.getParameter("fechaInicial")%>' />
                            </label>
                            <label>
                                Fecha final solicitud
                                <tsp:Date formulario="form1" clase="textbox" name="txtFechaFinal" otros="size='20'" fechaInicial='<%=request.getParameter("fechaFinal")==null?"":request.getParameter("fechaFinal")%>' />
                            </label>                        </td>
                        <td>Registros Encontrados: <input type="hidden" name="informe" id="informe" value="">
						</td>
					  <td>
						  <input type="text" name="resul_encontrados" id="resul_encontrados" disabled="disabled">
					  </td>
                    </tr>
                </table>
                <br/>
                <div align="center">
                    <img style="cursor:pointer"  src="<%= BASEURL%>/images/botones/generar.gif" title="Generar Informe" alt="Generar Informe" align="absmiddle" onClick="consultarSolicitudes('informe');" onmouseover="botonOver(this);" onmouseout="botonOut(this);">
                    <img style="cursor:pointer" src="<%= BASEURL%>/images/botones/buscar.gif" align="absmiddle" onmouseover="botonOver(this);" onmouseout="botonOut(this);" onClick="consultarSolicitudes('filtro');">
                    <img style="cursor:pointer" src="<%= BASEURL%>/images/botones/salir.gif" align="absmiddle" onmouseover="botonOver(this);" onmouseout="botonOut(this);" onClick="parent.close();">                </div>
            </form>
            <br/>



            <%

            String reg_encontrados= "";

            %>


            <%if((request.getParameter("resultado")!=null)&& (cuadro_informe.equals(""))){ //-- RESPONSABLE OPAV JCASTRO
                if(consulta!=null && consulta.size()>0){
                    int maxPageItems = 20;
            %>
            <table id="tabResultados" style="border: green solid 1px;" align="center">
                        <tr class="tblTitulo">
                            <th>Id solicitud</th>
                            <th>Id Accion</th>
                            <th>Aviso</th>
                            <th>Departamento</th>
                            <th>Responsable</th>
                            <th>Nic</th>
                            <th>Ejecutivo de Cuetas</th>
                            <th>Cliente</th>
                            <th>Tipo cliente</th>
                            <th style="min-width: 250px;">Estado</th>
                            <th>Contratista</th>
                            <th>Descripcion</th>
                            <th>Fecha solicitud</th>
                            <th>Fecha verificaci&oacute;n cartera</th>
                            <th>Fecha asignaci&oacute;n contratista</th>
                            <th>Fecha visita</th>
                            <th>Fecha creaci&oacute;n visita</th>
                            <th>Fecha cotizaci&oacute;n</th>
                            <th>Fecha entrega oferta</th>
                            <th>Dias vencidos visita</th>
                            <th>Dias vencidos cotizaci&oacute;n</th>
                            <th>Dias vencidos entrega oferta</th>
                            <th>Dias vencidos totales</th>
                            <th style="min-width: 200px;">Observaci&oacute;n</th>
                        </tr>
                        <pg:pager
                            items="<%=consulta.size()%>"
                            index="center"
                            maxPageItems="<%=maxPageItems%>"
                            maxIndexPages="10"
                            isOffset="<%= true %>"
                            export="offset,currentPageNumber=pageNumber"
                            scope="request">
                            <pg:param name="resultado"/>
                            <pg:param name="estadoSol"/>
                            <pg:param name="vencido"/>
                            <pg:param name="solicitud"/>
                            <pg:param name="cliente"/>
                            <pg:param name="ejecutivo"/>
                            <pg:param name="contratista"/>
                            <pg:param name="departamento"/>
                            <pg:param name="consecutivo"/>
                            <pg:param name="nic"/>
                            <pg:param name="fechaInicial"/>
                            <pg:param name="fechaFinal"/>
                    <%for (int i = offset.intValue(), l = Math.min(i + maxPageItems, consulta.size()); i < l; i++) {
                        NegocioApplus n = (NegocioApplus)consulta.get(i);
                    %>
                            <pg:item>
                                <tr class='fila'>
                                    <td><%=n.getIdSolicitud()%></td>
                                    <td><%=n.getIdAccion()%></td>
                                    <td><%=n.getAviso()%></td>
                                    <td><%=n.getDptoCliente()%></td>
                                    <td><%=n.getResponsable()%></td>
                                    <td><%=n.getNicClient()%></td>
                                    <td><%=n.getEjecutivo()%></td>
                                    <td><%=n.getNombreCliente()%></td>
                                    <td><%=n.getTipoCliente()%></td>
                                    <td><%=n.getEstado()%></td>
                                    <td><%=n.getNombreContratista()!=null?n.getNombreContratista():""%></td>
                                    <td><%=n.getDescripcion()%></td>
                                    <td><%=n.getFecha()%></td>
                                    <td><%=n.getFVerificacion_cartera().equals("0099-01-01")?"":n.getFVerificacion_cartera()%></td>
                                    <td><%=n.getFAsignacion_contratista()!=null?n.getFAsignacion_contratista():""%></td>
                                    <td><%=n.getFecVisitaReal()!=null?n.getFecVisitaReal():""%></td>
                                    <td><%=n.getCreationFecVisitaHecha()!=null?n.getCreationFecVisitaHecha():""%></td>
                                    <td><%=n.getFCotizacion()!=null?n.getFCotizacion():""%></td>
                                    <td><%=n.getFechaEntregaOferta().equals("0099-01-01")?"":n.getFechaEntregaOferta()%></td>
                                    <td <%=n.isVenc_visita()?"style='color:red'":""%>><%=n.getDiaVenc_visita()!=null?n.getDiaVenc_visita():""%></td>
                                    <td <%=n.isVenc_cotizacion()?"style='color:red'":""%>><%=n.getDiaVenc_cotizacion()!=null?n.getDiaVenc_cotizacion():""%></td>
                                    <td <%=n.isVenc_oferta()?"style='color:red'":""%>><%=n.getDiaVenc_entregaOferta()!=null?n.getDiaVenc_entregaOferta():""%></td>
                                    <td <%=n.isVenc_totales()?"style='color:red'":""%>><%=n.getDiaVenc_totales()!=null?n.getDiaVenc_totales():""%></td>
                                    <td><%=n.getObservacion()!=null?n.getObservacion():""%></td>
                                </tr>
                            </pg:item>

                                <%
reg_encontrados = /*n.getReg_encontrados();*/((NegocioApplus)consulta.get(consulta.size()-1)).getReg_encontrados();
                                %>





                                <%}%>
                            <tr bgcolor="#FFFFFF" class="fila">
                                <td height="30" colspan="24" nowrap>
                                    <pg:index>
                                        <jsp:include page="/WEB-INF/jsp/googlesinimg.jsp" flush="true"/>
                                    </pg:index>
                                </td>
                            </tr>
                        </pg:pager>
          </table>
                <%}else{%>
                    <table border="2" align="center">
                    <tr>
                        <td>
                            <table width="100%"  border="0" align="center"  bordercolor="#f7f5f4" bgcolor="#ffffff">
                                <tr>
                                    <td width="229" align="center" class="mensajes">No se encontraron resultados</td>
                                    <td width="29" background="<%=BASEURL%>/images/cuadronaranja.JPG">&nbsp;&nbsp;</td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
                <%}%>




<%}else{ %>
<% //--Cuadro Informe ---------------------------------------------------------------------------------------------%>


<% if((request.getParameter("resultado")!=null)&& (cuadro_informe.equals("mostrar"))){

                    if(consulta!=null && consulta.size()>0){
                    int maxPageItems = 20;

    %>








                <table id="tabResultados" style="border: green solid 1px;" align="center">
                        <tr class="tblTitulo">
                    <th>Departamento</th>
                    <th>Contratista</th>
                    <th>Cuenta Solicitudes</th>
                    <th>Promedio Dias Vencidos</th>
                        </tr>
                        <pg:pager
                            items="<%=consulta.size()%>"
                            index="center"
                            maxPageItems="<%=maxPageItems%>"
                            maxIndexPages="10"
                            isOffset="<%= true %>"
                            export="offset,currentPageNumber=pageNumber"
                            scope="request">
                            <pg:param name="resultado"/>
                            <pg:param name="estadoSol"/>
                            <pg:param name="vencido"/>
                            <pg:param name="solicitud"/>
                            <pg:param name="cliente"/>
                            <pg:param name="ejecutivo"/>
                            <pg:param name="contratista"/>
                            <pg:param name="departamento"/>
                            <pg:param name="consecutivo"/>
                            <pg:param name="nic"/>
                            <pg:param name="fechaInicial"/>
                            <pg:param name="fechaFinal"/>
                    <%for (int i = offset.intValue(), l = Math.min(i + maxPageItems, consulta.size()); i < l; i++) {
                        NegocioApplus n = (NegocioApplus)consulta.get(i);
                    %>
                            <pg:item>
                                <tr class='fila'>
                                <td><%=n.getDptoCliente()%></td>
                                <td><%=n.getNombreContratista()%></td>
                                <td><%=n.getCuenta_solicitudes()%></td>
                                <td><%=n.getPromedio()%></td>
                                </tr>
                            </pg:item>


                                <%}%>
                            <tr bgcolor="#FFFFFF" class="fila">
                                <td height="30" colspan="24" nowrap>
                                    <pg:index>
                                        <jsp:include page="/WEB-INF/jsp/googlesinimg.jsp" flush="true"/>
                                    </pg:index>
                                </td>
                            </tr>
                        </pg:pager>
          </table>

















<%}else{%>

                    <table border="2" align="center">
                    <tr>
                        <td>
                            <table width="100%"  border="0" align="center"  bordercolor="#f7f5f4" bgcolor="#ffffff">
                                <tr>
                                    <td width="229" align="center" class="mensajes">No se encontraron resultados</td>
                                    <td width="29" background="<%=BASEURL%>/images/cuadronaranja.JPG">&nbsp;&nbsp;</td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>


<%}}%>








<% } //--Cuadro Informe ---------------------------------------------------------------------------------------------%>

        </div>
        <tsp:InitCalendar/>
    </body>
</html>
<script>
     buscarResponsables('<%=CONTROLLEROPAV%>', '<%=cod_responsable%>');
     document.getElementById("resul_encontrados").value = '<%=reg_encontrados%>';

     //alert("___________"+document.getElementById("resul_encontrados").value);

</script>
