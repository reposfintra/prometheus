<%-- 
    Document   : gestion_actividades
    Created on : 28/06/2010, 04:07:17 PM
    Author     : rhonalf
--%>

<%@page import="com.tsp.operation.model.beans.TablaGen"%>
<%@page import="java.util.List"%>
<%@page import="com.tsp.operation.model.beans.Usuario"%>
<%@page import="com.tsp.opav.model.beans.*"%>
<%@page import="com.tsp.opav.model.services.*"%>
<%@ page session   ="true"%>
<%@ page errorPage ="/error/ErrorPage.jsp"%>
<%@ page import    ="java.util.*"%>
<%@include file ="/WEB-INF/InitModel.jsp"%>

<%@ page import    ="com.tsp.util.*"%>
<%@ page import    ="com.tsp.util.Util.*"%>
<%
    response.addHeader("Cache-Control","no-cache");
    response.addHeader("Pragma","No-cache");
    response.addDateHeader ("Expires", 0);

 
%>
<%@taglib uri="http://jakarta.apache.org/taglibs/input-0.90" prefix="input" %>
<%@page contentType="text/html" pageEncoding="ISO-8859-1"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
   "http://www.w3.org/TR/html4/loose.dtd">
<!-- This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>. -->
<html>
    <head>
        <link href="<%= BASEURL %>/css/estilostsp.css" rel="stylesheet" type="text/css">
        <script type='text/javascript' src="<%= BASEURL %>/js/boton.js"></script>
        <script type='text/javascript' src="<%= BASEURL %>/js/prototype.js"></script>
        <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
        <title>Gestion de actividades</title>
        <script type="text/javascript">
            function buscar(){
                var cad = document.getElementById("cadena").value;
                var ind = document.getElementById("param").selectedIndex;
                var par = document.getElementById("param").options[ind].value;
                var url = document.getElementById("formulario").action;
                //alert('cadena '+cad+' param '+par);
                var p =  'opcion=search&param='+par+'&cadena='+cad;
                new Ajax.Request(
                    url,
                    {
                        method: 'post',
                        parameters: p,
                        onLoading: loading,
                        onComplete: llenarDiv
                    });
            }

            function loading(){
                document.getElementById("busqueda").innerHTML ='<span class="letra">Buscando </span><img alt="cargando" src="<%= BASEURL%>/images/cargando.gif"  name="imgload">';
            }

            function llenarDiv(response){
                document.getElementById("busqueda").innerHTML = response.responseText;
            }

            function limpiarForm(){
                document.getElementById('codigo').value = '';
                document.getElementById('descripcion').value = '';
                escogerSelect('tipo','');
                document.getElementById('orden').value = '';
                document.getElementById('peso').value = '';
                //----------------------------------------------------------------------
                document.getElementById("imgaceptar").src='<%= BASEURL%>/images/botones/agregar.gif';
                document.getElementById("imgaceptar").onclick=function z(){enviar('insert');};
                document.getElementById("imgaceptar").onmouseover=function z1(){botonOver(document.getElementById("imgaceptar"));};
                document.getElementById("imgaceptar").onmouseout=function z2(){botonOut(document.getElementById("imgaceptar"));};
                //----------------------------------------------------------------------
                document.getElementById("imgeliminar").src='<%= BASEURL%>/images/botones/eliminarDisable.gif';
                document.getElementById("imgeliminar").onclick='';
                document.getElementById("imgeliminar").onmouseover='';
                document.getElementById("imgeliminar").onmouseout='';
                //----------------------------------------------------------------------
                document.getElementById("imgmodificar").src='<%= BASEURL%>/images/botones/modificarDisable.gif';
                document.getElementById("imgmodificar").onclick='';
                document.getElementById("imgmodificar").onmouseover='';
                document.getElementById("imgmodificar").onmouseout='';

            }

            function limpiar(){
                document.getElementById("cadena").value = '';
                document.getElementById("param").selectedIndex = 0;
            }

            function buscarDatos(){
                var idx = document.getElementById("codigo").value;
                if(idx!=''){
                    inactivar();
                    var url = document.getElementById("formulario").action;
                    var ps = 'opcion=listar&id='+idx;
                    new Ajax.Request(
                        url,
                        {
                            method: 'post',
                            parameters: ps,
                            onComplete: llenarForm
                        }
                    );
                }
                else{
                    alert('Debe escribir un codigo para poder cargar los datos');
                }
            }

            function inactivar(){
                document.getElementById("imgaceptar").src='<%= BASEURL%>/images/botones/agregarDisable.gif';
                document.getElementById("imgaceptar").onclick='';
                document.getElementById("imgaceptar").onmouseover='';
                document.getElementById("imgaceptar").onmouseout='';
                //----------------------------------------------------------------------
                document.getElementById("imgeliminar").src='<%= BASEURL%>/images/botones/eliminar.gif';
                document.getElementById("imgeliminar").onclick=function x(){enviar('delete');};
                document.getElementById("imgeliminar").onmouseover=function x1(){botonOver(document.getElementById("imgeliminar"));};
                document.getElementById("imgeliminar").onmouseout=function x2(){botonOut(document.getElementById("imgeliminar"));};
                //----------------------------------------------------------------------
                document.getElementById("imgmodificar").src='<%= BASEURL%>/images/botones/modificar.gif';
                document.getElementById("imgmodificar").onclick=function y(){enviar('modify');};
                document.getElementById("imgmodificar").onmouseover=function y1(){botonOver(document.getElementById("imgmodificar"));};
                document.getElementById("imgmodificar").onmouseout=function y2(){botonOut(document.getElementById("imgmodificar"));};
            }

            function llenarForm(response){
                var array = response.responseText.split(';_;');
                var tam = array.length;
                if(tam>0){
                    var desc = array[0];
                    var tipo = array[1];
                    var orden = array[2];
                    var peso = array[3];
                    document.getElementById('descripcion').value = desc;
                    escogerSelect('tipo',tipo);
                    document.getElementById('orden').value = orden;
                    document.getElementById('peso').value = peso;
                }
                else{
                    alert("No se han obtenido resultados");
                }
            }

            function escogerSelect(nombresel,dato){
                var select = document.getElementById(nombresel);
                for (var i=0;i<select.length;i++){
                    if(dato == select.options[i].value){
                        select.options[i].selected = true;
                    }
                }
            }

            function enviar(opcion){
                var ids = document.getElementById('codigo').value;
                var txtalert = '';
                if(opcion=='delete'){
                    txtalert = '�Esta seguro de eliminar el registro?';
                }
                else if(opcion==='modify'){
                    txtalert = '�Esta seguro de modificar el registro?';
                }
                else if(opcion=='insert'){
                    txtalert = '�Esta seguro de insertar un nuevo registro?';
                }
                else{
                    alert('No se selecciono nada ... raro');
                }
                var choice = confirm(txtalert);
                if(choice==true){
                    if(txtalert!=''){
                        sendForm(opcion,ids);
                    }
                }
            }
            <%
                Usuario usuario = (Usuario) session.getAttribute("Usuario");
                String user = usuario.getLogin();
                usuario = null;
            %>

            function sendForm(opcion,ids){
                var usuario = '<%= user %>';
                
                document.getElementById("formulario").action = document.getElementById("formulario").action + '&opcion=' + opcion + '&usuario=' + usuario;
                if(opcion=='delete' || opcion=='modify'){
                    document.getElementById("formulario").action = document.getElementById("formulario").action + '&codigo=' + ids;
                }
                document.getElementById("formulario").submit();
            }

        </script>
    </head>
    
   
	<%
List<TablaGen> lista = new LinkedList<TablaGen>();
model.tablaGenService.buscarRegistros("TIPO_ACT");
lista=model.tablaGenService.getTablas();
%>
	%>
    <body>
        <div id="capaSuperior" style="position:absolute; width:100%; height:100px; z-index:0; left: 0px; top: 0px;">
            <jsp:include page="/WEB-INF/toptsp.jsp?encabezado=Gestion de actividades"/>
        </div>
        <br>
        <br>
        <div align="center" id="capaCentral" style="position:absolute; width:100%; height:85%; z-index:0; left: -1px; top: 100px; overflow:auto;">
            <form id="formulario" name="formulario" action="<%= CONTROLLEROPAV %>?estado=Actividad&accion=Gestion" method="POST">
                <table border="0" width="100%">
                    <thead>
                        <tr class="subtitulo1">
                            <th>Campo</th>
                            <th>Valor</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr class="fila">
                            <td>Codigo</td>
                            <td>
                                <input type="text" id="codigo" name="codigo" value="" >
                                <img alt="buscar" src="<%= BASEURL%>/images/botones/iconos/buscar.gif" name="imgbuscar1" onClick="buscarDatos();" style="cursor:pointer ">
                            </td>
                        </tr>
                        <tr class="fila">
                            <td>Tipo</td>
                            <td>
                                <select id="tipo" name="tipo">
                                <option value="" selected>...</option>
                                <%for (int i=0;i<lista.size();i++){%>
                                    
                                    <option value="<%=lista.get(i).getTable_code() %>"><%=lista.get(i).getDato() %></option>
                                     <%}%>
                                  </select>
                            </td>
                        </tr>
                        <tr class="fila">
                            <td>Peso predeterminado</td>
                            <td><input type="text" id="peso" name="peso" value="" size="15" maxlength="15"></td>
                        </tr>
                        <tr class="fila">
                            <td>Orden predeterminado</td>
                            <td><input type="text" id="orden" name="orden" value="" size="15" maxlength="15"></td>
                        </tr>
                        <tr class="fila">
                            <td>Descripcion</td>
                            <td><input type="text" id="descripcion" name="descripcion" value="" size="60" maxlength="100"></td>
                        </tr>
                        <tr class="fila">
                            <td colspan="2" align="center">
                                <img id="imgaceptar" alt="aceptar" src="<%= BASEURL%>/images/botones/agregar.gif" name="imgaceptar" onMouseOver="botonOver(this);" onClick="enviar('insert');" onMouseOut="botonOut(this);" style="cursor:pointer ">
                                <img id="imgeliminar" alt="eliminar" src="<%= BASEURL%>/images/botones/eliminarDisable.gif" name="imgdelr" style="cursor:pointer ">
                                <img id="imgmodificar" alt="modificar" src="<%= BASEURL%>/images/botones/modificarDisable.gif" name="imgmody" style="cursor:pointer ">
                                <img alt="borrar" src="<%= BASEURL%>/images/botones/cancelar.gif" name="imgclear" onMouseOver="botonOver(this);" onClick="limpiarForm();" onMouseOut="botonOut(this);" style="cursor:pointer ">
                                <img alt="salir" src="<%= BASEURL%>/images/botones/salir.gif" name="imgsalir" onMouseOver="botonOver(this);" onClick="parent.close();" onMouseOut="botonOut(this);" style="cursor:pointer ">
                            </td>
                        </tr>
                    </tbody>
                </table>
                <br>
                <br>
                <table border="1" style="border-collapse:collapse;" width="100%">
                    <thead>
                        <tr class="subtitulo1">
                            <th colspan="2">Busqueda</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr class="fila">
                            <td>Filtro de busqueda</td>
                            <td>
                                <select name="param" id="param">
                                    <option>Seleccione...</option>
                                    <option value="a.id">Codigo</option>
                                    <option value="a.descripcion">Descripcion</option>
                                    <option value="a.tipo">Tipo</option>
                                    <option value="a.peso_predeterminado">Peso predeterminado</option>
                                    <option value="a.orden_predeterminado">Orden predeterminado</option>
                                </select>
                            </td>
                        </tr>
                        <tr class="fila">
                            <td>Cadena a buscar</td>
                            <td><input type="text" name="cadena" id="cadena" value="" size="60"></td>
                        </tr>
                        <tr class="fila">
                            <td colspan="2" align="center">
                                <img alt="buscar" src="<%= BASEURL%>/images/botones/buscar.gif"  name="imgbusq"  onMouseOver="botonOver(this);" onClick="buscar();" onMouseOut="botonOut(this);" style="cursor:pointer ">
                                <img alt="borrar" src="<%= BASEURL%>/images/botones/cancelar.gif"  name="imgcancel"  onMouseOver="botonOver(this);" onClick="limpiar();" onMouseOut="botonOut(this);" style="cursor:pointer ">
                            </td>
                        </tr>
                    </tbody>
                </table>
            </form>
            <br>
            <div id="headerb" class="subtitulo1" align="center">Resultados de la busqueda</div>
            <br>
            <div id="busqueda"></div>
            <br>
            <div align="center">
                <img alt="salir" src="<%= BASEURL%>/images/botones/salir.gif" name="imgsalir" onMouseOver="botonOver(this);" onClick="parent.close();" onMouseOut="botonOut(this);" style="cursor:pointer ">
            </div>
        </div>
    </body>
</html>