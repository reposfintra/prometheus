<%--
    Document   : ofertaDetallar
    Created on : 26/02/2009, 08:01:09 PM
    Author     : Alvaro
--%>

<%@page contentType="text/html"%>
<%@page session="true"%>

<%@page import="java.util.*"%>
<%@page import="com.tsp.util.*"%>
<%@include file="/WEB-INF/InitModel.jsp"%>

<%@page import="com.tsp.opav.model.beans.*"%>

<%String path    = application.getRealPath(model.menuService.getRealUrl(request.getRequestURI()));
  String datos[] = model.menuService.getContenidoMenu(BASEURL,request.getRequestURI(),path);
  String hoy     = Utility.getHoy("-");
  String  opcion = request.getParameter("opcion");
  String  pagina = "solicitudLiquidacionEspecifica.jsp";
  String  msj    = request.getParameter("msj");



  //System.out.println("inicio");

  if (opcion.equalsIgnoreCase("LISTADO") ) {
      pagina = "solicitudListaPorFacturar.jsp";
  }

   SolicitudPorFacturar solicitud  = modelopav.consorcioService.getSolicitud();
   List listaSubclientePorFacturar = modelopav.consorcioService.getSubclientePorSolicitud();
   List listaAccionPorFacturar     = modelopav.consorcioService.getAccionPorSolicitud();

   String posible_sv="";
   String posible_observacion="";
   String posible_fechaFactura= hoy;



   double total_material = 0.00;
   double total_mano_obra = 0.00;
   double total_otros = 0.00;
   double total_administracion = 0.00;
   double total_imprevisto = 0.00;
   double total_utilidad = 0.00;
   double total_costo_contratista = 0.00;
   double total_iva_contratista = 0.00;
   double total_bonificacion = 0.00;
   double total_iva_bonificacion = 0.00;
   double total_comision_opav = 0.00;
   double total_comision_fintra = 0.00;
   double total_comision_interventoria = 0.00;
   double total_comision_provintegral = 0.00;
   double total_comision_eca = 0.00;
   double total_iva_comision_opav = 0.00;
   double total_iva_comision_fintra = 0.00;
   double total_iva_comision_interventoria = 0.00;
   double total_iva_comision_provintegral = 0.00;
   double total_iva_comision_eca = 0.00;
   double total_valor_a_financiar = 0.00;
   double total_subtotal_iva = 0.00;
   double total_subtotal_sin_iva = 0.00;


 %>


<html >
    <head  >

        <title>Liquidacion de solicitud</title>
        <link href="<%= BASEURL%>/css/estilostsp.css" rel='stylesheet'>
        <link href="../../../../css/estilostsp.css" rel="stylesheet" type="text/css">
        <link href="/css/estilostsp.css" rel="stylesheet" type="text/css">
    </head>

    <script type='text/javascript' src="<%= BASEURL%>/js/validar.js"></script>
    <script type='text/javascript' src="<%= BASEURL%>/js/boton.js"></script>

    <script>
	   function send(theForm){
             theForm.submit();
	   }

	   function regresar(){
             window.close();
             window.open('<%= BASEURL%>/jsp/opav/liquidacion/solicitudLiquidacion.jsp');
           }


	   function aplicarLiquidacion(opcion){
             
             var k = 1;
             var existe = "SI";
             var lista_simbolo      = "";
             var lista_observacion  = "";
             var lista_fechaFactura = "";
             var lista_fechaInicioPago = "";

             while (existe=="SI") {

                 if (document.getElementById("simboloVariable"+k)) {
                     

                   var elemento_simbolo      = document.getElementById("simboloVariable"+k).value;
                   var elemento_observacion  = document.getElementById("observacion"+k).value;
                   var elemento_fechaFactura = document.getElementById("fechaFactura"+k).value;
                   var elemento_fechaInicioPago = document.getElementById("fechaInicioPago"+k).value;


                   lista_simbolo = lista_simbolo + elemento_simbolo + "|";
                   lista_observacion = lista_observacion + elemento_observacion + "|";
                   lista_fechaFactura = lista_fechaFactura + elemento_fechaFactura + "|";

                                   
                   lista_fechaInicioPago = lista_fechaInicioPago + elemento_fechaInicioPago + "|";

                   k++;

                 }else {
                     existe= "NO";
                 }
             }

            var parametro = "&opcion="+opcion+"&numItens="+k;  <%-- Para permitir regresar a la pagina solicitudListaPorFacturar.jsp --%>
            //  parametro     = parametro + "&lista_simboloVariable=" + lista_simbolo + "&lista_observacion=" + lista_observacion + "&lista_fechaFactura=" + lista_fechaFactura + "&lista_fechaInicioPago=" + lista_fechaInicioPago;
            // alert(parametro);
            //mientras
            var accion = '<%= CONTROLLEROPAV%>?estado=Consorcio&accion=Acceso&evento=APLICAR_LIQUIDACION';
            document.getElementById('formulario').action = accion +parametro ;
            document.getElementById('formulario').submit();
            //  window.close();
            //  window.open('<%= CONTROLLEROPAV%>?estado=Consorcio&accion=Acceso&evento=APLICAR_LIQUIDACION' + parametro );
    }






       function validarNumero(evento){
         if((evento.keyCode<48)||((evento.keyCode>57))){
          evento.returnValue=false;
          return false;
         }
         else{
          return true;
         }
       }

       function validarPunto(evento){
         if((evento.keyCode==46)){
          return true;
         }
         else{
          evento.returnValue=false;
          return false;
         }
       }


       function validarNumeroPunto(evento){
         if(  ((evento.keyCode<48)||(evento.keyCode>57))  &&  (evento.keyCode!=46)  ) {
          evento.returnValue=false;
          return false;
         }else{
          return true;
         }
       }


      function validar(campo,entero,decimal){
       evento=event;
       var vector=campo.value.split('.');
       if (vector.length==1){
           if(vector[0].length<entero){
              return (validarNumeroPunto(evento));
           }
           else{
               if(vector[0].length==entero){
                  return (validarPunto(evento));
               }else{
                  evento.returnValue=false;
                  return false;
               }
           }
       }
       else{
           if(vector.length<3){
               if(vector[1].length<decimal){
                   return (validarNumero(evento));
               }
               else{
                   evento.returnValue=false;
                   return false;
               }
           }
       }
       return true;
      }


    </script>






    <body onload = 'redimensionar()'  >

        <div id="capaSuperior" style="position:absolute; width:100%; height:110px; z-index:0; left: 0px; top: 0px;">
            <jsp:include page="/toptsp.jsp?encabezado= DETALLE DE SOLICITUD"/>
        </div>
        <div id="capaCentral" style="position:absolute; width:100%; height:488px; z-index:0; left: 0px; top: 109px; overflow: scroll">



      <form name='formulario' method='post' id="formulario" action=""  >
        <p></p>

        <table width="840"  border="1"  align="center">
              <tr class='fila'>
                <td width="100"  align="center"  > ID SOLICITUD: </td>
                <td width="80"  align="center"  > <%= solicitud.getId_solicitud() %> </td>
                <td width="80"  align="center"  > NUM OS:  </td>
                <td width="120"  align="center" > <%= solicitud.getNum_os() %> </td>
                <td width="160"  align="center"  > CLIENTE PRIMARIO: </td>
                <td width="300" align="left"  > <%= solicitud.getNombre() %> </td>
              </tr>

              <tr class='fila'>
                <td width="100"  align="center"  > FECHA OFERTA: </td>
                <td width="80" align="center"  > <%= solicitud.getFecha_oferta()  %> </td>
                <td width="80"  align="center"  > TIPO_CLIENTE: </td>
                <td width="80" align="center"  > <%= solicitud.getTipo_cliente()  %> </td>
                <td width="80"  align="center"  > DTF SEMANA: </td>
                <td width="80" align="center"  > <%= ((SubclientePorSolicitud)listaSubclientePorFacturar.get(0)).getDtf_semana()  %> </td><!--20100206-->
              </tr>






        </table>


        <table width="2000"  border="1"  align="center">

              <tr class="tblTitulo1"  align="center">
                      <th  width="80" align="center"> SUBCLIENTE</th>
                      <th  width="60" align="center"> PARCIAL</th>
                      <th  width="150" align="center"> NOMBRE</th>
                      <th  width="60" align="center"> NIC</th>
                      <th  width="60" align="center"> NIT</th>
                      <th  width="70" align="center"> FECHA FINANCIACION</th>
                      <th  width="70" align="center"> TIPO INTERES</th>
                      <th  width="70" align="center"> PUNTOS</th>
                      <th  width="70" align="center"> % INTERES</th>


                      <th  width="70" align="center"> VALOR SIN IVA</th>                <!-- subcliente.getValor_sin_iva            -->
                      <th  width="60" align="center"> % BASE</th>                       <!-- subcliente.getPorcentaje_base()        -->
                      <th  width="70" align="center"> SUBTOTAL BASE 1</th>              <!-- subcliente.getSubtotal_base_1          -->
                      <th  width="70" align="center"> % INCREMENTO EXTEMPORANEO</th>    <!-- subcliente.getPorcentaje_extemporaneo  -->
                      <th  width="70" align="center"> VALOR EXTEMPORANEO 1</th>         <!-- subcliente.getValor_extemporaneo 1     -->
                      <th  width="70" align="center"> SUBTOTAL VALOR BASE 2 </th>       <!-- subcliente.getSubtotal_base_2          -->
                      <th  width="70" align="center"> CUOTA INICIAL</th>                <!-- subcliente.getValor_cuota_inicial      -->
                      <th  width="70" align="center"> SUBTOTAL VALOR BASE 3</th>        <!-- subcliente.setSubtotal_base_3          -->
                      <th  width="70" align="center"> IVA</th>                          <!-- subcliente.getValor_iva                -->
                      <th  width="70" align="center"> BASE IVA</th>                     <!-- subcliente.getValor_base_iva           -->
                      <th  width="70" align="center"> VALOR EXTEMPORANEO 2</th>         <!-- subcliente.getValor_extemporaneo 2     -->
                      <th  width="70" align="center"> SUBTOTAL IVA</th>                 <!-- subcliente.getSubtotal_iva             -->
                      <th  width="70" align="center"> VALOR A FINANCIAR</th>            <!-- subcliente.getSubtotal_a_financiar     -->
                      <th  width="60" align="center"> TOTAL EXTEMPORANEO </th>          <!-- subcliente.getValor_extemporaneo       -->

                      <th  width="70" align="center"> INTERESES</th>
                      <th  width="70" align="center"> VALOR CON FINANCIACION</th>
                      <th  width="60" align="center"> NUMERO CUOTAS </th>
                      <th  width="60" align="center"> TIPO R. </th>
                      <th  width="70" align="center"> VALOR CUOTA</th>

                      <th  width="80" align="center"> ESTUDIO ECONOMICO</th>
              </tr>


              <%
                int i=0;
                SubclientePorSolicitud subcliente = null;
                Iterator it = listaSubclientePorFacturar.iterator();
                while (it.hasNext()) {

                  i++;
                  subcliente = (SubclientePorSolicitud)it.next();

              %>

              <tr class=' <%= (i%2==0?"filagris1":"filaazul1") %>' >

                <td width="80"  align="center">  <%= subcliente.getId_subcliente() %> </td>
                <td width="60"  align="center">  <%= subcliente.getParcial() %> </td>
                <td width="150"  align="left" >  <%= subcliente.getNombre() %> </td>
                <td width="60"  align="center" > <%= subcliente.getNic() %> </td>
                <td width="60"  align="center" > <%= subcliente.getNit() %> </td>
                <td width="70"  align="center" > <%= subcliente.getFecha_financiacion() %> </td>

                <td width="70"  align="center" > <%= subcliente.getClase_dtf() %> </td>
                <td width="70"  align="center" > <%= subcliente.getPuntos() %> </td>

                <%
                  if(subcliente.getTipo().equalsIgnoreCase("MAXIMA")) {
                 %>
                     <td width="70"  align="center" > <%= subcliente.getPuntos() %> </td>
                <%}
                  else {
                 %>
                     <td width="70"  align="center" > <%= subcliente.getDtf_semana() + subcliente.getPuntos() %> </td><!--20100206-->
                <%
                  }
                 %>


                <td width="70"  align="right" > <%= Util.FormatoMiles(subcliente.getValor_sin_iva()) %> </td>
                <td width="60"  align="center" ><%= Util.FormatoMiles(subcliente.getPorcentaje_base()) %> </td>
                <td width="70"  align="center" ><%= Util.FormatoMiles(subcliente.getSubtotal_base_1()) %></td>
                <td width="70"  align="right" > <%= subcliente.getPorcentaje_extemporaneo() %> </td>
                <td width="70"  align="right" > <%= Util.FormatoMiles(subcliente.getValor_extemporaneo_1()) %> </td>
                <td width="70"  align="right" > <%= Util.FormatoMiles(subcliente.getSubtotal_base_2() ) %> </td>
                <td width="70"  align="right" > <%= Util.FormatoMiles(subcliente.getValor_cuota_inicial()) %> </td>
                <td width="70"  align="right" > <%= Util.FormatoMiles(subcliente.getSubtotal_base_3()) %> </td>
                <td width="70"  align="right" > <%= Util.FormatoMiles(subcliente.getValor_iva()) %> </td>
                <td width="70"  align="right" > <%= Util.FormatoMiles(subcliente.getValor_base_iva()) %> </td>
                <td width="70"  align="right" > <%= Util.FormatoMiles(subcliente.getValor_extemporaneo_2()) %> </td>
                <td width="70"  align="right" > <%= Util.FormatoMiles(subcliente.getSubtotal_iva()) %> </td>
                <td width="70"  align="right" > <%= Util.FormatoMiles(subcliente.getSubtotal_a_financiar()) %> </td>
                <td width="60"  align="right" > <%= Util.FormatoMiles(subcliente.getValor_extemporaneo()) %> </td>
                <td width="70"  align="right" > <%= Util.FormatoMiles(subcliente.getIntereses()) %> </td>
                <td width="70"  align="right" > <%= Util.FormatoMiles(subcliente.getValor_con_financiacion()) %> </td>
                <td width="60"  align="center" > <%= subcliente.getPeriodo() %> </td>

                <td width="60"  align="center" > <%= subcliente.getTipo_recivo() %> </td>
                
                <td width="70"  align="right" > <%= Util.FormatoMiles(subcliente.getValor_cuota()) %> </td>

                <td width="80"  align="center" > <%= subcliente.getEstudio_economico() %></td>

              </tr>

           <%
              }
           %>
        </table>

        <p></p>

        <table width="2010"  border="1"  align="center">

              <tr class="tblTitulo1"  align="center">
                      <th  width="80" align="center"> ACCION</th>
                      <th  width="150" align="center"> NOMBRE CONTRATISTA</th>
                      <th  width="80" align="center"> MATERIAL</th>
                      <th  width="80" align="center"> MANO OBRA</th>
                      <th  width="80" align="center"> OTROS</th>
                      <th  width="80" align="center"> ADMINISTRACION</th>
                      <th  width="80" align="center"> IMPREVISTO</th>
                      <th  width="60" align="center"> UTILIDAD</th>
                      <th  width="100" align="center"> COSTO CONTRATISTA</th>

                      <th  width="80" align="center"> BONIFICACION</th>
                      <th  width="75" align="center"> COMISION OPAV</th>
                      <th  width="75" align="center"> COMISION FINTRA</th>
                      <th  width="80" align="center"> COMISION INTERVENTORIA</th>
                      <th  width="80" align="center"> COMISION PROVINTEGRAL</th>
                      <th  width="80" align="center"> COMISION ECA</th>
                      <th  width="90" align="center"> SUBTOTAL SIN IVA</th>




                      <th  width="60" align="center"> BASE IVA</th>


                      <th  width="80" align="center"> IVA CONTRATISTA</th>
                      <th  width="80" align="center"> IVA BONIFICACION</th>
                      <th  width="80" align="center"> IVA OPAV</th>
                      <th  width="80" align="center"> IVA FINTRA</th>
                      <th  width="80" align="center"> IVA INTERVENTORIA</th>
                      <th  width="70" align="center"> IVA PROVINTEGRAL</th>
                      <th  width="80" align="center"> IVA ECA</th>

                      <th  width="80" align="center"> SUBTOTAL IVA</th>

                      <th  width="110" align="center"> VALOR SOLICITUD</th>




              </tr>


              <%
                i=0;
                AccionPorSolicitud accion = null;
                it = listaAccionPorFacturar.iterator();
                while (it.hasNext()) {

                  i++;
                  accion = (AccionPorSolicitud)it.next();

                  total_material += accion.getMaterial();
                  total_mano_obra += accion.getMano_obra();
                  total_otros += accion.getOtros();
                  total_administracion += accion.getAdministracion();
                  total_imprevisto += accion.getImprevisto();
                  total_utilidad += accion.getUtilidad();
                  total_costo_contratista +=accion.getCosto_contratista();
                  total_iva_contratista += accion.getIva_contratista();
                  total_bonificacion += accion.getBonificacion();
                  total_iva_bonificacion += accion.getIva_bonificacion();
                  total_comision_opav += accion.getComision_opav();
                  total_comision_fintra += accion.getComision_fintra();
                  total_comision_interventoria += accion.getComision_interventoria();
                  total_comision_provintegral += accion.getComision_provintegral();
                  total_comision_eca += accion.getComision_eca();
                  total_iva_comision_opav += accion.getIva_comision_opav();
                  total_iva_comision_fintra += accion.getIva_comision_fintra();
                  total_iva_comision_interventoria += accion.getIva_comision_interventoria();
                  total_iva_comision_provintegral += accion.getIva_comision_provintegral();
                  total_iva_comision_eca += accion.getIva_comision_eca();
                  total_valor_a_financiar += accion.getValor_a_financiar();

                  total_subtotal_iva += accion.getSubtotal_iva();
                  total_subtotal_sin_iva += accion.getValor_a_financiar_sin_iva();



              %>

              <tr class=' <%= (i%2==0?"filagris1":"filaazul1") %>' >

                <td width="80"  align="center"> <%= accion.getId_accion() %> </td>
                <td width="240" align="left" >  <%= accion.getNombre_contratista() %> </td>
                <td width="80"  align="right">  <%= Util.FormatoMiles(accion.getMaterial()) %> </td>
                <td width="80"  align="right">  <%= Util.FormatoMiles(accion.getMano_obra()) %> </td>
                <td width="80"  align="right">  <%= Util.FormatoMiles(accion.getOtros()) %> </td>
                <td width="80"  align="right">  <%= Util.FormatoMiles(accion.getAdministracion()) %> </td>
                <td width="80"  align="right">  <%= Util.FormatoMiles(accion.getImprevisto()) %> </td>
                <td width="60"  align="right">  <%= Util.FormatoMiles(accion.getUtilidad()) %> </td>
                <td width="100"  align="right">  <%= Util.FormatoMiles(accion.getCosto_contratista()) %> </td>


                <td width="80"  align="right">  <%= Util.FormatoMiles(accion.getBonificacion()) %> </td>
                <td width="75"  align="right">  <%= Util.FormatoMiles(accion.getComision_opav()) %> </td>
                <td width="75"  align="right">  <%= Util.FormatoMiles(accion.getComision_fintra()) %> </td>
                <td width="80"  align="right">  <%= Util.FormatoMiles(accion.getComision_interventoria()) %> </td>
                <td width="80"  align="right">  <%= Util.FormatoMiles(accion.getComision_provintegral()) %> </td>
                <td width="80"  align="right">  <%= Util.FormatoMiles(accion.getComision_eca()) %> </td>

                <td width="90"  align="right">  <%= Util.FormatoMiles(accion.getValor_a_financiar_sin_iva()) %> </td>



                <td width="60"  align="right">  <%= Util.FormatoMiles(accion.getBase_iva_contratista()) %> </td>

                <td width="80"  align="right">  <%= Util.FormatoMiles(accion.getIva_contratista()) %> </td>
                <td width="80"  align="right">  <%= Util.FormatoMiles(accion.getIva_bonificacion()) %> </td>
                <td width="80"  align="right">  <%= Util.FormatoMiles(accion.getIva_comision_opav()) %> </td>
                <td width="80"  align="right">  <%= Util.FormatoMiles(accion.getIva_comision_fintra()) %> </td>
                <td width="80"  align="right">  <%= Util.FormatoMiles(accion.getIva_comision_interventoria()) %> </td>
                <td width="70"  align="right">  <%= Util.FormatoMiles(accion.getIva_comision_provintegral()) %> </td>
                <td width="80"  align="right">  <%= Util.FormatoMiles(accion.getIva_comision_eca()) %> </td>
                <td width="80"  align="right">  <%= Util.FormatoMiles(accion.getSubtotal_iva()) %> </td>



                <td width="110"  align="right">  <%= Util.FormatoMiles(accion.getValor_a_financiar() ) %> </td>


              </tr>

           <%
              }
           %>


              <tr class='filaTotal'>

                <td width="80"  align="center"> TOTAL </td>
                <td width="240" align="left" >  </td>
                <td width="80"  align="right">  <%= Util.FormatoMiles(total_material) %> </td>
                <td width="80"  align="right">  <%= Util.FormatoMiles(total_mano_obra) %> </td>
                <td width="80"  align="right">  <%= Util.FormatoMiles(total_otros) %> </td>
                <td width="80"  align="right">  <%= Util.FormatoMiles(total_administracion) %> </td>
                <td width="80"  align="right">  <%= Util.FormatoMiles(total_imprevisto) %> </td>
                <td width="60"  align="right">  <%= Util.FormatoMiles(total_utilidad) %> </td>
                <td width="100"  align="right">  <%= Util.FormatoMiles(total_costo_contratista) %> </td>


                <td width="80"  align="right">  <%= Util.FormatoMiles(total_bonificacion) %> </td>
                <td width="75"  align="right">  <%= Util.FormatoMiles(total_comision_opav) %> </td>
                <td width="75"  align="right">  <%= Util.FormatoMiles(total_comision_fintra) %> </td>
                <td width="80"  align="right">  <%= Util.FormatoMiles(total_comision_interventoria) %> </td>
                <td width="80"  align="right">  <%= Util.FormatoMiles(total_comision_provintegral) %> </td>
                <td width="80"  align="right">  <%= Util.FormatoMiles(total_comision_eca) %> </td>
                <td width="90"  align="right">  <%= Util.FormatoMiles(total_subtotal_sin_iva) %> </td>
                <td width="70"  align="right">   </td>

                <td width="80"  align="right">  <%= Util.FormatoMiles(total_iva_contratista) %> </td>
                <td width="80"  align="right">  <%= Util.FormatoMiles(total_iva_bonificacion) %> </td>

                <td width="80"  align="right">  <%= Util.FormatoMiles(total_iva_comision_opav) %> </td>
                <td width="80"  align="right">  <%= Util.FormatoMiles(total_iva_comision_fintra) %> </td>
                <td width="80"  align="right">  <%= Util.FormatoMiles(total_iva_comision_interventoria) %> </td>
                <td width="70"  align="right">  <%= Util.FormatoMiles(total_iva_comision_provintegral) %> </td>
                <td width="80"  align="right">  <%= Util.FormatoMiles(total_iva_comision_eca) %> </td>
                <td width="80"  align="right">  <%= Util.FormatoMiles(total_subtotal_iva) %> </td>


                <td width="110"  align="right">  <%= Util.FormatoMiles(total_valor_a_financiar) %> </td>

              </tr>
        </table>

        <p></p>

          <table width="1150"  border="2" align="center" cellpadding="0" cellspacing="0">
            <tr>
              <td><table width="100%" class="tablaInferior">

                <tr>
                    <td colspan="7" >
                      <table width="100%"  border="0" cellpadding="0" cellspacing="1" class="barratitulo">
                          <tr>
                            <td width="60%" class="subtitulo1">&nbsp;INGRESO DE SIMBOLO VARIABLE</td>
                            <td width="40%" class="barratitulo"><img align="left" src="<%=BASEURL%>/images/titulo.gif" width="32" height="20"><%=datos[0]%></td>
                          </tr>
                     </table>
                   </td>
                </tr>


             <tr class="tblTitulo1"  align="center">


                      <th  width="6%" align="center">SUBCLIENTE</th>
                      <th  width="4%"  align="center">PARCIAL</th>
                      <th  width="23%" align="center">NOMBRE</th>
                      <th width="8%"  align="center">SIMBOLO VARIABLE</th>
                      <th  width="31%" align="center">OBSERVACIONES</th>
                      <th  width="14%" align="center">FECHA POSIBLE PAGO</th>
                      <th  width="14%" align="center">FECHA POSIBLE FACTURA</th>

              </tr>




               <%
                 i=0;
                 it = listaSubclientePorFacturar.iterator();
                 while (it.hasNext()) {
                   i++;
                   subcliente = (SubclientePorSolicitud)it.next();
               %>


                <tr class="fila">

                  <td width="6%" align="center"> <%= subcliente.getId_subcliente() %> </td>

                  <td width="4%"  align="center"> <%= subcliente.getParcial() %> </td>
                  <td width="23%" align="left">   <%= subcliente.getNombre() %></td>

                  <td nowrap  width="8%" align="center"> <input name=simboloVariable<%= i %> type="text" class="textbox" id= simboloVariable<%= i %>  value="">  </td>

                  <td  width=31%" align="center">  <textarea name= observacion<%= i %>  rows="2" id=observacion<%= i %>  style='width:100%' ></textarea>  </td>


                  <td  width="14%" align="center">
                      <!-- Fecha -->
                      <input  name=fechaInicioPago<%= i %> id=fechaInicioPago<%= i %> size="20" readonly="true" class="textbox" value='<%=subcliente.getFecha_inicio_pago()%>' style='width:50%'>
                  </td>


                  <td  width="14%" align="center">
                      <!-- Fecha -->
                      <input  name=fechaFactura<%= i %> id=fechaFactura<%= i %> size="20" readonly="true" class="textbox" value='<%=posible_fechaFactura%>' style='width:50%'>
                      <a href="javascript:void(0)" onClick="if(self.gfPop)gfPop.fPopCalendar(fechaFactura<%= i %>);return false;" >
                          <img name="popcal" align="absmiddle" src="<%=BASEURL%>/js/calendartsp/calbtn.gif" width="16" height="16" border="0" alt="">
                      </a>
                  </td>



                </tr>



                <%}%>


              </table></td>
            </tr>
          </table>


        <p></p>

        <table align="center">
          <tr>

            <td nowrap align="center">
                  <img alt="REGISTRAR liquidacion y simbolo" src="<%=BASEURL%>/images/botones/aplicar.gif"      height="21"  title='REGISTRAR liquidacion y simbolo'      onClick="aplicarLiquidacion('LISTADO');"     onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" style="cursor: hand">
            </td>

            <td nowrap align="center">
                  <img alt="Regresar" src="<%=BASEURL%>/images/botones/salir.gif"      height="21"  title='Salir'      onClick="regresar();"     onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" style="cursor:hand">
            </td>

          </tr>
        </table>




       <!-- MENSAJE -->
       <% if(msj!=null  &&  !msj.equals("") ){ %>
            <br><br>
            <table border="2" align="center">
                  <tr>
                    <td>
                        <table width="100%"  border="1" align="center"   bordercolor="#F7F5F4" bgcolor="#FFFFFF">
                              <tr>
                                    <td width="450" align="center" class="mensajes"><%= msj %></td>
                                    <td width="29" background= "<%=BASEURL%>/images/cuadronaranja.JPG">&nbsp;</td>
                                    <td width="58">&nbsp; </td>
                              </tr>
                         </table>
                    </td>
                  </tr>
            </table>

        <% } %>




      </form >
    </div>


        <!-- Necesario para los calendarios-->
        <iframe width="188" height="166"    id="gToday:normal:<%=BASEURL%>/js/calendartsp/agenda.js:gfPop:<%=BASEURL%>/js/calendartsp/plugins.js" name="gToday:normal:<%=BASEURL%>/js/calendartsp/agenda.js:gfPop:<%=BASEURL%>/js/calendartsp/plugins.js"  src="<%=BASEURL%>/js/calendartsp/ipopeng.htm" scrolling="no" frameborder="0" style="visibility:visible; z-index:999; position:absolute; left:-500px; top:0px;"></iframe>



        <%=datos[1]%>

    </body>
</html>

