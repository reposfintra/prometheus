<%--
    Document   : solicitudLiquidacion
    Created on : 25/02/2009, 08:38:38 PM
    Author     : Alvaro
--%>


<%@page contentType="text/html"%>
<%@page session="true"%>
<%@page errorPage="../error/ErrorPage.jsp"%>
<%@page import="java.util.*"%>
<%@page import="com.tsp.util.*"%>
<%@include file="/WEB-INF/InitModel.jsp"%>

<%@page import="com.tsp.opav.model.beans.*"%>



<%

            String path = application.getRealPath(model.menuService.getRealUrl(request.getRequestURI()));
            String datos[] = model.menuService.getContenidoMenu(BASEURL, request.getRequestURI(), path);

            List listaSolicitudPorFacturar = modelopav.consorcioService.getSolicitudPorFacturar();

            String fechaInicioPago = "";

%>




<html>
  <head>

    <title>Consultar para listar ofertas</title>

    <script language="JavaScript" type="text/javascript" src="<%=BASEURL%>/js/boton.js"></script>
    <script type='text/javascript' src="<%=BASEURL%>/js/validar.js"></script>

    <link rel="stylesheet" type="text/css" href="<%=BASEURL%>/css/jCal/jscal2.css" />
    <link rel="stylesheet" type="text/css" href="<%=BASEURL%>/css/jCal/border-radius.css" />
    <script type="text/javascript" src="<%=BASEURL%>/js/jCal/jscal2.js"></script>
    <script type="text/javascript" src="<%=BASEURL%>/js/jCal/lang/es.js"></script>

    <link href="../css/estilostsp.css" rel="stylesheet" type="text/css">

  </head>
  <body  >
    <div id="capaSuperior" style="position:absolute; width:100%; height:100px; z-index:0; left: 1px; top: 0px;  ">
      <jsp:include page="/toptsp.jsp?encabezado=Lista de Ofertas"/>
    </div>


    <div id="capaSiguiente" style="position:absolute; width:100%; height:87%; z-index:0; left: 0px; top: 100px; overflow: scroll;">




      <form name='formulario' resizable=yes method='post' id="formulario" action="" scrollbars=yes toolbars=yes location=yes >
        <p>&nbsp;</p>

        <table width="1150"  border="1"  align="center">


              <TR class="tblTitulo1"  align="center">

                      <TH  width="70" align="center"> NUM_OS</TH>
                      <TH  width="50" align="center"> SOLICITUD</TH>
                      <TH  width="50" align="center"> PARCIAL</TH>
                      <TH  width="220" align="center"> FECHA POSIBLE PAGO</TH>
                      <TH  width="50" align="center"> CLIENTE</TH>
                      <TH  width="90" align="center"> FECHA ENTREGA OFERTA</TH>
                      <TH  width="50" align="center"> NIC</TH>
		      <TH  width="70" align="center"> NIT </TH>
                      <TH  width="300" align="center"> NOMBRE CLIENTE</TH>
                      <TH  width="70" align="center"> TIPO DISTRIBUCION</TH>
                      <TH  width="50" align="center"> VALOR AGREGADO</TH>
                      <TH  width="70" align="center"> INCREMENTO EXTEMPORANEO</TH>
		      <TH  width="80" align="center"> CONSECUTIVO OFERTA</TH>
                      <TH  width="80" align="center"> TIPO</TH>
              </TR>


          <%
            int i=0;
            Iterator it = listaSolicitudPorFacturar.iterator();
            SolicitudPorFacturar  solicitud = null;
            while (it.hasNext()) {

              i++;
              solicitud = (SolicitudPorFacturar)it.next();

              String fecha_entrega_oferta    = solicitud.getFecha_entrega_oferta().substring(0, 11);
              String incremento_extemporaneo = solicitud.getEs_oficial();

              String selectURL  = CONTROLLEROPAV + "?estado=Consorcio&accion=Acceso&evento=DETALLAR_SOLICITUD&opcion=LISTADO&id_solicitud="+solicitud.getId_solicitud()+"&parcial="+solicitud.getParcial() +"&fechaInicioPago="+fechaInicioPago;

          %>
              <tr class='<%= (i%2==0?"filagris1":"filaazul1") %>'>

  		<td width="70"  align="center"> <%= solicitud.getNum_os() %> </td>
                <td width="50"  align="center"><a id="selectURL<%=i%>" HREF="<%= selectURL %>"  onClick="obtenerFechaInicioPago(<%=i%>)" > <%=solicitud.getId_solicitud() %>  </a> </td>
                <td width="50"  align="center"> <%=solicitud.getParcial() %>  </td>
                <td width="100"  align="center">

                      <!-- Fecha -->
                      <input  name="fechaInicioPago<%= i %>" id="fechaInicioPago<%= i %>" size="8" class="textbox"  style='width:80%'>
                      <!--a href="javascript:void(0)" onClick = "if(self.gfPop)gfPop.fPopCalendar(fechaInicioPago<%= i %>);return false;" >
                          <img name="popcal" align="absmiddle" src="<%=BASEURL%>/js/calendartsp/calbtn.gif" width="16" height="16" border="0" alt="">
                      </a-->
                        <img src="<%=BASEURL%>/js/Calendario/cal.gif" id="imgFecha<%= i %>" />
                        <script type="text/javascript">
                            Calendar.setup({
                                inputField : "fechaInicioPago<%= i %>",
                                trigger    : "imgFecha<%= i %>",
                                //max        : Calendar.dateToInt(new Date()),
                                onSelect   : function() {
                                    this.hide();
                                }
                            });
                        </script>


                </td>
                <td width="50"  align="center"> <%= solicitud.getId_cliente() %>  </td>
                <td width="90"  align="center"> <%if (fecha_entrega_oferta!=null && fecha_entrega_oferta.equals("0099-01-01 ")){out.print("-");}else{out.print(fecha_entrega_oferta);} %>  </td>
		<td width="50"  align="center"> <%= solicitud.getNic() %> </td>
                <td width="70"  align="center"> <%= solicitud.getNit() %> </td>
                <td width="300" align="left"  > <%= solicitud.getNombre() %> </td>
                <td width="70" align="center">  <%= solicitud.getTipo_distribucion() %> </td>
                <td width="50"  align="center"> <%= solicitud.getNo_es_valoragregado() %> </td>
                <td width="70"  align="center"> <%= incremento_extemporaneo %> </td>
                <td width="80"  align="center"> <%= solicitud.getConsecutivo_oferta() %> </td>
                <td width="80"  align="center"> <%= solicitud.getTipo() %> </td>

              </tr>
           <%
                }
           %>

        </table>

        <p>&nbsp;</p>

        <table align="center">
          <tr>
            <td nowrap align="center">
                  <img src="<%=BASEURL%>/images/botones/salir.gif"      height="21"  title='Salir'      onClick="window.close();"     onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" style="cursor:hand">
          </tr>
        </table>
      </form >
    </div>



    <script >


    function obtenerFechaInicioPago( item)
    {

        var referencia      = document.getElementById("selectURL"+item).href;
        var posicion        = referencia.indexOf('fechaInicioPago=');
        var nuevaReferencia = referencia.substr(0,posicion);
        var fecha           = document.getElementById("fechaInicioPago"+item).value;
        nuevaReferencia     = nuevaReferencia + "fechaInicioPago=" +fecha;
        document.getElementById("selectURL"+item).href = nuevaReferencia;

    }



      function Formatear(numero,decimales,miles)
      {
        var numero = new oNumero(numero);
        return numero.formato(decimales, miles);
      }

      //Objeto oNumero
      function oNumero(numero)
      {
      //Propiedades
      this.valor = numero || 0
      this.dec = -1;
      //M�todos
      this.formato = numFormat;
      this.ponValor = ponValor;
      //Definici�n de los m�todos
      function ponValor(cad)
      {
      if (cad =='-' || cad=='+') return
      if (cad.length ==0) return
      if (cad.indexOf('.') >=0)
          this.valor = parseFloat(cad);
      else
          this.valor = parseInt(cad);
      }
      function numFormat(dec, miles)
      {
      var num = this.valor, signo=3, expr;
      var cad = ""+this.valor;
      var ceros = "", pos, pdec, i;
      for (i=0; i < dec; i++)
      ceros += '0';
      pos = cad.indexOf('.')
      if (pos < 0)
          cad = cad+"."+ceros;
      else
          {
          pdec = cad.length - pos -1;
          if (pdec <= dec)
              {
              for (i=0; i< (dec-pdec); i++)
                  cad += '0';
              }
          else
              {
              num = num*Math.pow(10, dec);
              num = Math.round(num);
              num = num/Math.pow(10, dec);
              cad = new String(num);
              }
          }
      pos = cad.indexOf('.')
      if (pos < 0) pos = cad.lentgh
      if (cad.substr(0,1)=='-' || cad.substr(0,1) == '+')
             signo = 4;
      if (miles && pos > signo)
          do{
              expr = /([+-]?\d)(\d{3}[\.\,]\d*)/
              cad.match(expr)
              cad=cad.replace(expr, RegExp.$1+','+RegExp.$2)
              }
      while (cad.indexOf(',') > signo)
          if (dec<0) cad = cad.replace(/\./,'')
              return cad;
      }
      }//Fin del objeto oNumero:


    </script>

    <!-- Necesario para los calendarios-->
    <!--iframe width="188" height="166"    id="gToday:normal:<%=BASEURL%>/js/calendartsp/agenda.js:gfPop:<%=BASEURL%>/js/calendartsp/plugins.js" name="gToday:normal:<%=BASEURL%>/js/calendartsp/agenda.js:gfPop:<%=BASEURL%>/js/calendartsp/plugins.js"  src="<%=BASEURL%>/js/calendartsp/ipopeng.htm" scrolling="no" frameborder="0" style="visibility:visible; z-index:999; position:absolute; left:-500px; top:0px;"></iframe-->



  </body>
</html>
