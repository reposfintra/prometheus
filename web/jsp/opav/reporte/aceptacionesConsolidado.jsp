<%-- 
    Document   : aceptacionesConsolidado
    Created on : 15/07/2014, 09:52:42 AM
    Author     : jpacosta
--%>
<%@page import="com.tsp.opav.model.beans.Interventor"%>
<%@page import="com.tsp.opav.model.services.InterventorService"%>
<%@page import="com.tsp.opav.model.services.ClientesVerService"%>
<%@ page import="com.tsp.operation.model.beans.CmbGeneralScBeans"%>
<%@ page import="com.tsp.operation.model.services.SeguimientoCarteraService"%>

<%@ page contentType="text/html; charset=UTF-8" language="java" %>
<%@ page session   ="true"%>
<%@ page errorPage ="/error/ErrorPage.jsp"%>
<%@ page import    ="java.util.*" %>
<%@ page import    ="java.text.SimpleDateFormat" %>
<%@ page import    ="com.tsp.util.*" %>
<%@ page import    ="javax.servlet.*" %>
<%@ page import    ="javax.servlet.http.*" %>
<%@ include file="/WEB-INF/InitModel.jsp"%>
<%@ taglib uri="http://www.sanchezpolo.com/sot" prefix="tsp"%>

<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
        <title>Consolidado aceptados</title>
        <link href="<%=BASEURL%>/css/estilostsp.css" rel="stylesheet" type="text/css">
        <link href="<%=BASEURL%>/css/estilotabla.css" rel="stylesheet" type="text/css">
        <link href="<%=BASEURL%>/css/style_azul.css" rel="stylesheet" type="text/css">

        <script type="text/javascript" src="<%=BASEURL%>/js/jquery-1.4.2.min.js"></script>
    <div id="div_espera" style="display:none;z-index:1000; position:absolute"></div>

</head>
<body>
    <div id="capaSuperior" style="position:absolute; width:100%; height:100px; z-index:0; left: 0px; top: 0px;">
        <jsp:include page="/WEB-INF/toptsp.jsp?encabezado=Consolidado aceptaciones"/>
    </div>
<%ArrayList opciones;%>
    <div id="capaCentral" style="position:absolute; width:100%; height:85%; z-index:0; left: 0px; top: 100px; overflow: scroll;">
        <br>
        <center>
            <fieldset style="text-align:left;">
                <legend>FILTROS</legend>
                <fieldset style="display:inline-block">
                    <legend>Periodo aprobacion</legend>
                    <input id='periodo' value=''/>
                </fieldset>
                <fieldset style="display:inline-block">
                    <legend>Estado</legend>
                    <select id='estados' multiple size='5'>
                        <% opciones = (ArrayList) modelopav.ClientesVerService.getEstados();
                        for(int i = 0; i < opciones.size(); i++) {%>
                        <option value='<%=((String[]) opciones.get(i))[1]%>'>
                            <%=((String[]) opciones.get(i))[0]%>
                        </option>    
                        <%}%>
                    </select>
                </fieldset>
                <fieldset style="display:inline-block">
                    <legend>Tipo solicitud</legend>
                    <select id='tipos_solicitud' multiple size='5'>
                        <% opciones = (ArrayList) modelopav.ClientesVerService.getTiposSolicitud();
                        for(int i = 0; i < opciones.size(); i++) {%>
                        <option value='<%=((String[]) opciones.get(i))[1]%>'>
                            <%=((String[]) opciones.get(i))[0]%>
                        </option>    
                        <%}%>
                    </select>
                </fieldset>
                <fieldset style="display:inline-block">
                    <legend>Interventor</legend>
                    <select id='interventores' multiple size='5'>
                        <% opciones = (ArrayList) (new InterventorService()).listadoInterventores();
                        for(int i = 0; i < opciones.size(); i++) {%>
                        <option value='<%=((Interventor) opciones.get(i)).getCodigo()%>'>
                            <%=((Interventor) opciones.get(i)).getNombre()%>
                        </option>    
                        <%}%>
                    </select>
                </fieldset>
                <fieldset style="display:inline-block">
                    <legend>Responsable</legend>
                    <select id='responsables' multiple size='5'>
                        <% opciones = (ArrayList) modelopav.ClientesVerService.listadoResponsables();
                        for(int i = 0; i < opciones.size(); i++) {%>
                        <option value='<%=((String[]) opciones.get(i))[0]%>'>
                            <%=((String[]) opciones.get(i))[1]%>
                        </option>    
                        <%}%>
                    </select>
                </fieldset>
                <input id="buscar" type="button" value="Buscar" onclick='buscar();'/>
            </fieldset>
            <br>
            <fieldset style='min-height: 150px'>
                <legend>Consolidado de Aceptaciones</legend>
                <table width="95%" border='0' align='center' cellpadding="0" cellspacing="0" class="tablas" id="tbl_proceso">
                    <thead>
                        <th>&Assign;</th>
                        <th>Mes Aprobacion</th>
                        <th>Sector Cliente</th>
                        <th>Cantidad Trabajos</th>
                        <th>Promedio dias Ejecucion</th>
                        <th>Costo Contratista</th>
                        <th>Precio Venta</th>
                    </thead>
                    <tbody>
                    </tbody>
                </table>
            </fieldset>
            
            <br>
            <tsp:boton value="salir" onclick="parent.close();"/>
            <br><br>

            <center class='comentario'>
                <div id="comentario" style="visibility: hidden" >
                    <table border="2" align="center">
                        <tr>
                            <td>
                                <table width="100%"  border="1" align="center"  bordercolor="#F7F5F4" bgcolor="#FFFFFF">
                                    <tr>
                                        <td width="229" align="center" class="mensajes"><span class="normal"><div id="mensaje">mensaje</div></span></td>
                                        <td width="29" background="<%=BASEURL%>/images/cuadronaranja.JPG">&nbsp;</td>
                                        <td width="58">&nbsp;</td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                    </table>
                </div>
            </center> 
        </center>
    </div>
    <script>
        
        function buscar() {
            var periodo = (''+$('#periodo').val()+'').trim().split(',');
            if(periodo.length === 2) {
                periodo = periodo.join(' AND ');
            } else {
                if(periodo != '' && periodo.length === 1){ 
                    periodo = periodo +' AND '+periodo;                     
                } else {
                    periodo = '';
                }
            }
            var estados = ('\''+$('#estados').val()+'\'').split(',').join('\',\'')
                          .replace('null','0');
            var tipos_solicitud = ('\''+$('#tipos_solicitud').val()+'\'').split(',').join('\',\'')
                          .replace('null','0');
            var interventores = ('\''+$('#interventores').val()+'\'').split(',').join('\',\'')
                          .replace('null','0');
            var responsables = ('\''+$('#responsables').val()+'\'').split(',').join('\',\'')
                          .replace('null','0');
            
            $.ajax ({
                url:'<%=CONTROLLEROPAV%>?estado=Aceptaciones&accion=Consolidado&opcion=0',
                datatype:'json',
                type:'get',
                data:{'periodo':periodo,'tipos_sol':tipos_solicitud,'estados':estados
                     ,'interventores':interventores,'responsables':responsables},
                success: function(json) {
                    if(json.mensaje) {
                        alert(json.mensaje);
                    } else {
                        llenarTabla(json);
                    }
                },
                error: function(result) {
                    alert(result);
                }
            });
        }
        
        function llenarTabla(json) {
            $('#tbl_proceso tbody').html('');
            if(jQuery.isEmptyObject(json)) {
                $('#tbl_proceso tbody').html('<tr><td colspan="7" class="odd" style="text-align:center">Sin datos para mostrar</td></tr>');
            } else {
                for(var i in json) {
                    $('#tbl_proceso tbody').append('<tr class="odd" >'
                            +'<th style="background-color:mediumseagreen; cursor:pointer" rowspan="'+parseInt(1+parseInt(json[i].valores.length))+'" onclick="visible(\''+i+'\', '+(json[i].valores.length)+')" name="C'+i+'"></th>'
                            +'<th style="background-color:mediumseagreen" rowspan="'+parseInt(1+parseInt(json[i].valores.length))+'" style="text-align:center"  name="C'+i+'">'+i+'</th>'
                            +'<th style="background-color:mediumseagreen"></th>'
                            +'<th style="background-color:mediumseagreen; text-align:right">'+json[i].totales.cant_trabajos+'</th>'
                            +'<th style="background-color:mediumseagreen; text-align:right">'+(parseFloat(json[i].totales.promedio_dias)/parseInt(json[i].valores.length)).toFixed(2).replace(/\d(?=(\d{3})+\.)/g, '$&,')+'</th>'
                            +'<th style="background-color:mediumseagreen; text-align:right">'+json[i].totales.costo_contratista.toFixed(2).replace(/\d(?=(\d{3})+\.)/g, '$&,')+'</th>'
                            +'<th style="background-color:mediumseagreen; text-align:right">'+json[i].totales.precio_venta.toFixed(2).replace(/\d(?=(\d{3})+\.)/g, '$&,')+'</th>'
                            +'</tr>');
                    for (var j = 0; j < json[i].valores.length; j++) {
                        $('#tbl_proceso tbody').append(
                             '<tr class="'+((j%2 === 0)?'even':'odd')+'" name="'+i+'" onDblClick="detallar(\''+i+'\',\''+json[i].valores[j].sector_cliente+'\')">'
                            +'<td >'+json[i].valores[j].sector_cliente+'</td>'
                            +'<td style="text-align:right">'+json[i].valores[j].cant_trabajos+'</td>'
                            +'<td style="text-align:right">'+json[i].valores[j].promedio_dias+'</td>'
                            +'<td style="text-align:right">'+json[i].valores[j].costo_contratista.replace(/\d(?=(\d{3})+\.)/g, '$&,')+'</td>'
                            +'<td style="text-align:right">'+json[i].valores[j].precio_venta.replace(/\d(?=(\d{3})+\.)/g, '$&,')+'</td>'
                            +'</tr>');
                    }
                }
            }
        }
        
        function visible(periodo, nFilas) {
            var filas = document.getElementsByName(periodo);
            var cabs = document.getElementsByName('C'+periodo);
            var vsbl;
            for(var i = 0; i < filas.length; i++) {
                vsbl = filas[i].style.display;
                filas[i].style.display = (vsbl == 'none')?'':'none';
            }
            for(var i = 0; i < cabs.length; i++) {
                vsbl = cabs[i].rowSpan;
                cabs[i].rowSpan = (vsbl == '1')?''+(nFilas+1):'1';
            }
        }
        
        function detallar(periodo, sector) {
            $.ajax ({
                url:'<%=CONTROLLEROPAV%>?estado=Aceptaciones&accion=Consolidado&opcion=1',
                datatype:'json',
                type:'get',
                data:{'periodo':periodo,'sector':sector},
                success: function(json) {
                    if(json.mensaje) {
                        alert(json.mensaje);
                    } else {
                        var div = document.createElement('div');
                        div.className='ui-draggable';
                        div.style='overflow-y:auto;max-height: 400px;background: none repeat scroll 0% 0% rgb(255, 255, 255); border: 1px solid rgb(22, 95, 182); position: absolute; z-index: 102; padding: 3px; left: 535px; top: 279px;';
                        
                        var tabla = document.createElement('table');
                        tabla.style='';//<th>Estado</th>
                        var iHTML = '<tr class="even"><th>Solicitud</th><th>Dias Ejecucion</th><th>Costo Contratista</th><th>Precio Venta</th></tr>';
                        for(var i in json) { //<td>'+json[i].estado+'</td>
                           iHTML += '<tr class="odd"><td>'+json[i].solicitud+'</td><td>'+json[i].dias_ejecucion+'</td><td>'
                                 +json[i].costo_contratista+'</td><td>'+json[i].precio_venta+'</td></tr>';
                        } 
                        tabla.innerHTML = iHTML;
                        div.appendChild(tabla);
                        var cerrar = document.createElement('button');
                        cerrar.innerHTML = 'Cerrar';
                        cerrar.onclick = function() {
                            document.getElementById('capaCentral').removeChild(this.parentElement);
                        };
                        div.appendChild(cerrar);
                        document.getElementById('capaCentral').appendChild(div);
                    }
                },
                error: function(result) {
                    alert(result.message);
                }
            });
        }
    </script>
</body>
</html>