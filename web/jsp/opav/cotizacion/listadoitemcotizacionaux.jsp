<%@page import="com.tsp.operation.model.beans.Usuario"%>
<!--
	- Autor : Ing. Jose Castro
	- Date  : 12 agosto 2010
	- Copyrigth Notice : Consorcio.
	-->

<%--
	-@(#)
	--Descripcion : P�gina JSP, que maneja el ingreso de ordenes de compra.
	--%>
<%@page contentType="text/html"%>
<%@page session="true"%>
<%@page import="java.util.*"%>
<%@page import="com.tsp.opav.model.*"%>
<%@page import="com.tsp.opav.model.beans.*"%>
<%@page import="com.tsp.opav.model.services.*"%>
<%@page import="com.tsp.util.*"%>
<%@include file="/WEB-INF/InitModel.jsp"%>
<%@ taglib uri="http://jakarta.apache.org/taglibs/input-0.90" prefix="input" %>


<%
            String operacion = operacion = request.getParameter("operacion") == null ? "" : request.getParameter("operacion");

            int op = (request.getParameter("op") != null) ? Integer.parseInt(request.getParameter("op").toString()) : -1;
            int idcargado = 0;
            switch (op) {
                case 0:
%>
<form id ="forma" name="forma" action="<%= CONTROLLEROPAV%>?estado=Cotizacion&accion=Crear&cmd=show" method="post">

    <!-- Tabla Grande-->
    <table width="100%" border="2"align="center">
        <tr>
            <td>
                <!-- Tabla Interna-->
                <table width="100%" align="center" id="tablaform" >
                    <tr>
                        <td colspan="2" align="left" class="subtitulo1">Ingresar datos</td>
                        <td width="1081" colspan="3" align="left" class="barratitulo"><img alt="titulo" src="<%=BASEURL%>/images/titulo.gif"></td><!-- Modificacion Colspan de 6 a 3 Ing. Jose Castro-->
                    </tr>
                    <!-- Linea Datos-->								<input name="filas" type="hidden" id="filas" value="1">

                    <!-- Linea Datos-->
                    <tr> 
                        <%
                                            String idaccion = "";//903116";
                                            idaccion = request.getParameter("idaccion") == null ? "" : request.getParameter("idaccion");
                                            String bo = (String) session.getAttribute("borrador");
                                            boolean borrador = Boolean.parseBoolean(bo);

                                            Boolean existeCotizacion = (Boolean) session.getAttribute("existeCotizacion");


                                                                                       String[] datos = (String[]) session.getAttribute("datos_cabecera");
                                            String[] datitos = new String[2];
                                            datitos = datos;
                        %>

                        <td align="left" valign="middle" class="fila">Multiservicio</td>
                        <td colspan="6" valign="middle" class="letra">
                            <table width="100%">
                                <tr class="fila" >
                                    <td width="103" >Id accion</td>
                                    <td width="161"><input name="multiservicios" type="text" id="multiservicios" value="<%=idaccion%>" size="15" READONLY></td>
                                    <td width="119" >Nombre cliente</td>
                                    <td width="1102" ><input name="textfield2" type="text" value="<%=datitos[1]%>" size="100" READONLY></td>
                                </tr>
                                <tr class="fila">
                                    <td>Id Solicitud</td>
                                    <td><input name="textfield" id="textfield" type="text" value="<%=datitos[0]%>" size="15" READONLY></td>
                                    <td>Fecha</td>
                                    <td>											 <%
                                                        if ((borrador)||(operacion.equals("modificar"))) {
                                                            String fechaplantilla = (String) session.getAttribute("fecha");

                                        %>
                                        <input name="fecha" type="text" id="fecha" value="<%=fechaplantilla%>" READONLY>
                                        <%
                                                                                                    } else {
                                        %>
                                        <input name="fecha" type="text" id="fecha" READONLY>
                                        <img alt="calendario" src="<%=BASEURL%>/images/cal.gif" width="16" height="16" align="absmiddle" style="cursor:pointer " onClick="if(self.gfPop)gfPop.fPopCalendar(document.forma.fecha);return false;" HIDEFOCUS>
                                        <%
                                                            }
                                        %>
                                        <% if(!operacion.equals("modificar")){%>
                                        <a href="<%=BASEURL%>/jsp/opav/cotizacion/listadocotizaciones.jsp?id_accion_inicial=<%=idaccion%>&contratista=<%=datitos[2]%>&fechap='document.forma.fecha'">Copiar Cotizacion</a>
                                        <% }%>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <!-- Linea Fecha -->
<%
                    String criterio = request.getParameter("criterio") == null ? "" : request.getParameter("criterio");

if((!existeCotizacion)||(operacion.equals("modificar")||(borrador))){
%>
                    <!-- fila Producto -->
                    <tr class="fila">
                        <td colspan="2" align="left" class="subtitulo1">Productos</td>
                        <td colspan="5" align="left" class="barratitulo"><img alt="titulo" src="<%=BASEURL%>/images/titulo.gif"></td>
                    </tr>

<!-- Desde aqui Maneja la fila que carga todo........................................................................................................................................... -->
<tr class="fila"  >
 <td colspan="5" >
            <table  width="1543" border="1" style="border-collapse:collapse; border-color:#E4E4E4" >
            <tr>
                <td width="20"> </td>
                  <td width="57" height="20" class="fila"><div align="center">Tipo </div></td>
                    <td width="92" class="fila"><div align="center">Categor&iacute;a</div></td>
                    <td colspan="2" class="fila"><div align="center">Descripci&oacute;n</div></td>
                    <td width="83" class="fila"><div align="center">Cantidad</div></td>
                    <td width="69" class="fila"><div align="center">Pnd.Compra</div>
                    <td width="83" class="fila"><div align="center">Valor</div></td>
                    <td width="90" class="fila"><div align="center">Base SubTotal </div></td>
                    <td width="107" class="fila"><div align="center">Observaci&oacute;n</div></td>
                    <td width="61" class="fila"><div align="center">Compra</div></td>
                    <td width="69" class="fila"><div align="center">Cant.Compra </div>
                    
                </td>
            </tr>
            </table>

<!-- -->
<div id="detalles_items" style="display:none; padding:0px 0px 0px 0px;" ></div>
<!-- -->

  </td>
</tr>
                </table>
            
            </td>
        </tr>

        <%  } else {%>
        <script type="text/javascript">
            alert("Ya existe una cotizacion para esta accion...");
        </script>
        <% }%>
    </table>
    <!-- Tabla Grande-->

    <div align="center">
        <br>         
        <%
                            if (((!existeCotizacion)||(borrador))&&(!operacion.equals("modificar"))) {
        %>
        <img alt="regresar" src="<%= BASEURL%>/images/botones/regresar.gif"  name="imgregresar"  onMouseOver="botonOver(this);" onClick="regresar('<%= CONTROLLEROPAV%>',<%=datitos[0]%>);" onMouseOut="botonOut(this);">
        <img src="<%= BASEURL%>/images/botones/aceptar.gif" name="imgaceptar" alt="Aceptar" onClick="guardarCotizacion('<%=CONTROLLEROPAV%>','<%=BASEURL%>', 'crear');" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" style="cursor:pointer ">&nbsp;
        <img src="<%= BASEURL%>/images/botones/guardar.gif" name="imgguardar" alt="Guardar" onMouseOver="botonOver(this);" onClick="guardarCotizacion('<%=CONTROLLEROPAV%>','<%=BASEURL%>', 'guardar');" onMouseOut="botonOut(this);" style="cursor:pointer ">
        <%
                            }
       if (operacion.equals("modificar")) {
        %>
        <img src="<%= BASEURL%>/images/botones/aceptar.gif" name="imgaceptar" alt="Aceptar" onClick="guardarCotizacion('<%=CONTROLLEROPAV%>','<%=BASEURL%>', 'modificar');" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" style="cursor:pointer ">&nbsp;
    <%
        }
        %>
        <img src="<%= BASEURL%>/images/botones/salir.gif" name="imgsalir" alt="Salir" onMouseOver="botonOver(this);" onClick="parent.close();" onMouseOut="botonOut(this);" style="cursor:pointer ">
    </div>
    <div id="contenido" align="center" style="width: 200px;height: 100px; visibility: hidden; background-color: white">
        <textarea id="cont" style="width: 95%;height: 95%;"  onkeyup="$(argumento).value=this.value;"></textarea>
    </div>
</form>


<iframe width="188" height="166" name="gToday:normal:agenda.js:gfPop:plugins.js" id="gToday:normal:agenda.js:gfPop:plugins.js" src="<%=BASEURL%>/js/Calendario/ipopeng.htm" scrolling="yes" frameborder="0" style="visibility:visible; z-index:999; position:absolute; left:-500px; top:0px;">
</iframe>

<%
            String mens = (String) request.getAttribute("msg");
 if (mens != null && !mens.equals("")) {
 %>
      <script type="text/javascript">
                alert('<%=mens%>');
            </script>
<%}%>


<% break;
                case 1:
%>
            <table width="100%" id="tablaValores" onClick="if(event.target.type!='checkbox' && event.target.type!='text'){wclose();}">

<%
ArrayList detallesCotizacion = (ArrayList) session.getAttribute("detallesCotizacion");
Usuario usuario = (Usuario) session.getAttribute("Usuario");
ClientesVerService clvsrv = new ClientesVerService(usuario.getBd());
String perfil= clvsrv.getPerfil(usuario.getLogin());

for (int x = 0; x < detallesCotizacion.size(); x++) {

Cotizacion cot = null;
cot = (Cotizacion) detallesCotizacion.get(x);
            
           String cod_cotizacion = cot.getCodigo();
           int idcategoria = cot.getIdcategoria();
           String cod_material = cot.getMaterial();
           String tipo_material = (cot.getOficial().equals("S")) ? "MO" : cot.getTipo_material();//validamos si es un material oficial.
           String fecha = cot.getFecha();
           String observacion = cot.getObservacion();
           String idaccionlis = cot.getAccion();
           double cantidad = cot.getCantidad();
           double cantpend = Math.rint((cot.getCantidad()-cot.getCantidadComprada())*100)/100;
           double cantcomp = cot.getCantidadComprada();
           String idcotdet = cot.getId();
           String compra_pro = cot.getCompra();
           double cant_pro = cot.getCantidad_compra();
           double precio = cot.getValor();
           String desc_cat = cot.getDescripcion_categoria();
           String desc_mat = cot.getDescripcion_material();
           double precioen_mat = cot.getPrecioen_material();
           System.out.println("operacion___"+operacion);
%>

<!-- Ciclo repeticion -->
            <tr>
            <td>
                <table width="1540"  height="30" border="1"   style="border-collapse:collapse; border-color:#E4E4E4" cellpadding="0">
              <tr>
                <td width="107" rowspan="2" align="center">
                    <table width="100%" border="0" align="center" cellpadding="0" style="border-collapse:collapse; ">
                        <tr>
                            <td width="17" height="20">
                                <div align="right" class="fila"><%=x+1%></div>
                            </td>
                            <td width="33">
                                <div align="center" id="resta">
                                    <img id="imgmas<%=x%>" name="imgmas<%=x%>" src="<%=BASEURL%>/images/botones/iconos/mas.gif" alt="Agregar fila" onClick="agregarCelda1('<%=CONTROLLEROPAV%>', '<%=BASEURL%>')" width="12" height="12" <%if(clvsrv.ispermitted(perfil,"23")&&operacion.equals("modificar")){%>   style="display: none;"  <% }%>>
                                    <img id="imgini<%=x%>" name="imgini<%=x%>" src="<%=BASEURL%>/images/botones/iconos/menos1.gif" alt="Eliminar fila" onClick="borrarDatosListaNew1(<%=x%>);"  width="12" height="12"  <%if((clvsrv.ispermitted(perfil,"23")&&operacion.equals("modificar"))||cantcomp>0){%>   style="display: none;"  <% }%>>
                                </div>
                            </td>
                        </tr>
                    </table>
                </td>
              </tr>
              <tr>
                <td height="27" width="45">
                      <div align="center">
                          <select name="tipo<%=x%>" id="tipo<%=x%>" onChange="buscarCategorias('<%=CONTROLLEROPAV%>', '<%=BASEURL%>',this.selectedIndex,<%=x%>, '<%=idcategoria%>')" style="width:40" <%if(operacion.equals("modificar")){%>   disabled  <% }%>>
                              <option value="-1">Seleccione</option>
                                <option value="M" <% if (tipo_material.equals("M")) { out.print("selected"); }%> >Material</option>
                                <option value="MO"<% if (tipo_material.equals("MO")) { out.print("selected"); }%> >Material Oficial</option>
                                <option value="D" <% if (tipo_material.equals("D")) { out.print("selected"); }%> >Mano de Obra</option>
                                <option value="O" <% if (tipo_material.equals("O")) { out.print("selected"); }%> >Otros</option>
                          </select>
                      </div>
                </td>
<td width="146">
                    <div align="center">
                          <select name="categoria<%=x%>" id="categoria<%=x%>" onChange="buscarMateriales('<%=CONTROLLEROPAV%>', '<%=BASEURL%>',<%=x%>);" style="width:50" <%if(operacion.equals("modificar")){%>   disabled  <% }%> >
                              <%if(idcategoria!=0){ %> <option value="<%=idcategoria%>" selected ><%=desc_cat%></option>  <% }else{%>
                              <option value="-1" selected >Seleccione...</option><% }%>

                          </select>
<%if(!operacion.equals("modificar")){%>
<img src='<%=BASEURL%>/images/botones/iconos/lupa.gif' width="12" height="12" border="0" style="cursor:hand" title="Filtrar Categoria" onClick="abrirPagina('<%=BASEURL%>/jsp/delectricaribe/cotizacion/filtrocotizacion/listadoCategoria.jsp?ind=<%=x%>&tipo='+forma.tipo<%=x%>.value+'&idcat='+forma.categoria<%=x%>.value,'');" >
<% }%>
</div>
                  </td>
                  <td width="60">
                      <input name="idsubcategoria<%=x%>" type="hidden" id="idsubcategoria<%=x%>" value="0" >
                      <input name="idtiposubcategoria<%=x%>" type="hidden" id="idtiposubcategoria<%=x%>" value="0">
                      <span class="barratitulo">
                          <input name="textfieldx<%=x%>" type="text" id="textfieldx<%=x%>"      onKeyUp="buscarfiltromaterial(document.forma.material<%=x%>,this);instantValue(<%=x%>);xperience(document.forma.cantidad<%=x%>.value,<%=x%>);" size="10" <%if(operacion.equals("modificar")){%>   readonly    <% }%> >
                    </span>
                </td>
                  <td width="700"><div align="center"><span class="barratitulo">
                    <select name="material<%=x%>" id="material<%=x%>" style="width:740px" onChange="precioMaterial('<%=CONTROLLEROPAV%>', '<%=BASEURL%>',<%=x%>);" <%if(operacion.equals("modificar")){%>   disabled  <% }%>>
                      <%if(!cod_material.equals("")){ %>
                      <option value="<%=cod_material%>" selected ><%=desc_mat%></option>
                      <% }else{%>
                      <option value="-1" selected >Seleccione...</option>
                      <% }%>
                    </select>
                    </span>
                  </div></td>

                  <td width="68">
                      <div align="center">
                          <input name="cantidad<%=x%>" type="text" id="cantidad<%=x%>" onChange="calcularpndcompra(<%=x%>)" value="<%=cantidad%>" style="text-align:right" onFocus="instantValue(<%=x%>)" onKeyUp="xperience(this.value,<%=x%>);"  size="8" <%if(clvsrv.ispermitted(perfil,"23")&&operacion.equals("modificar")){%>   readonly <% }%> >
                      </div>
                </td>
                <td width="68">
                      <div align="center">
                          <input name="cantcomp<%=x%>" type="hidden" id="cantcomp<%=x%>" value="<%=cantcomp%>" >
                          <input name="pndcompra<%=x%>" type="text" id="pndcompra<%=x%>"  value="<%=cantpend%>" style="text-align:right"  readonly size="7">
                      </div>
                </td>
                  <td width="68">
                      <div align="center">
                          <input name="precio<%=x%>" type="text" id="precio<%=x%>" value="<%= Util.customFormat(precio==0?precioen_mat:precio) %>"  size="10"  style="text-align:right" onKeyUp="xperience(document.getElementById('cantidad<%=x%>').value,<%=x%>);soloNumeros(this.id)" <% if (tipo_material.equals("M")||(clvsrv.ispermitted(perfil,"23")&&operacion.equals("modificar"))) { %> readonly <% }%>  >
                          <input name="valor_2<%=x%>" type="hidden" id="valor_2<%=x%>">
                      </div>
                </td>

                  <%
                                    double bs = cantidad * (precio == 0 ? precioen_mat : precio);
                                    %>

                  <td width="68">
                      <div align="center">
                          <input name="base<%=x%>" type="text" id="base<%=x%>" value="<%=Util.customFormat(bs)%>" size="10" readonly style="text-align:right" >
                          <input name="base_2<%=x%>" type="hidden" id="base_2<%=x%>">
                </div></td>
                  <td width="20">
                      <div align="center">
                          <input name="idcotdet<%=x%>" type="hidden" id="idcotdet<%=x%>" value="<%=idcotdet%>" >
                          <input name="nota<%=x%>" type="text" id="nota<%=x%>" alt="<%=x + 1%>"  value="<%=observacion%>"  readonly onClick="fila=this.alt;control(event,'nota<%=x%>')" size="10">
                      </div>
                </td>
                  <td width="23">
                      <div align="center">
                <input name="compra<%=x%>" type="checkbox" value="S"   id="compra<%=x%>" onClick="cambiarCantcompra(<%=x%>)"  
                <% if(!compra_pro.equals("N")){%> checked <%}                    if (cantcomp>0) {%>  disabled="disabled" <%}%> >
                      </div>
                </td>
                  <td width="74">
                      <div align="center">
                          <input name="cantidad_compra<%=x%>"  type="text" id="cantidad_compra<%=x%>"  style="text-align:right" onFocus="instantValue(<%=x%>)" onBlur="validarvalor(this.value,<%=x%>)" value="<%=cant_pro%>" size="8"  >
                      </div>
                </td>
              </tr>
            </table>

<input name="indiceDelete<%=x%>" id="indiceDelete<%=x%>" type="hidden" value="-1"><!-- Valor Inicial Indice-->
           </td>
        </tr>
<% }%>            <!-- -->


</table>
<input name="tam_items" type="hidden" id="tam_items" value="<%=detallesCotizacion.size()%>">

<%
                    break;
}
%>