	<!--
	- Autor : Ing. Rhonalf Martinez
	- Date  : 25 de julio del 2009
	- Copyrigth Notice : Fintravalores S.A.
	-->

	<%--
	-@(#)
	--Descripcion : P�gina JSP, que maneja el ingreso de ordenes de compra.
	--%>

	<%@page contentType="text/html"%>
	<%@page session="true"%>
	<%@page import="java.util.*"%>
	<%@page import="com.tsp.operation.model.*"%>
	<%@page import="com.tsp.operation.model.beans.*"%>
	<%@page import="com.tsp.operation.model.services.*"%>
	<%@page import="com.tsp.util.*"%>
	<%@include file="/WEB-INF/InitModel.jsp"%>
	<%@ taglib uri="http://jakarta.apache.org/taglibs/input-0.90" prefix="input" %>

	<html>

		<head>

			<title>Cotizaciones - Crear</title>
			<link href="<%= BASEURL%>/css/estilostsp.css" rel="stylesheet" type="text/css">
			<script type='text/javascript' src="<%=BASEURL%>/js/Validacion.js"></script>
			<script type='text/javascript' src="<%= BASEURL%>/js/boton.js"></script>
			<script type='text/javascript' src="<%= BASEURL%>/js/general.js"></script>
			<script src='<%=BASEURL%>/js/tools.js' language='javascript'></script>
			<script src='<%=BASEURL%>/js/date-picker.js'></script>
			<script src="<%= BASEURL%>/js/transferencias.js"></script>
			<script type="text/javascript" src="<%=BASEURL%>/js/buscarproveedor.js"></script>
                        <script type='text/javascript' src="<%= BASEURL%>/js/prototype.js"></script>
                        <script type='text/javascript' src="<%= BASEURL%>/js/scriptaculous/scriptaculous.js"></script>
                        <script type='text/javascript' src="<%= BASEURL%>/js/autosuggest.js"></script>
                        <script type='text/javascript' src="<%= BASEURL%>/js/scriptaculous/controls.js"></script>
                        <script type='text/javascript' src="<%= BASEURL%>/js/scriptaculous/builder.js"></script>


     <link href="<%=BASEURL%>/css/mac_os_x.css"      rel="stylesheet" type="text/css">

        <script src="<%=BASEURL%>/js/effects.js"        type="text/javascript"></script>
        <script src="<%=BASEURL%>/js/window.js"         type="text/javascript"></script>
        <script src="<%=BASEURL%>/js/window_effects.js" type="text/javascript"></script>
        <link href="<%=BASEURL%>/css/default.css"       rel="stylesheet" type="text/css">
        <link href="<%=BASEURL%>/css/alert.css" rel="stylesheet" type="text/css"/>
        <link href="<%=BASEURL%>/css/alphacube.css" rel="stylesheet" type="text/css"/>


			<%
				CotizacionService coserv = new CotizacionService();//091001
				MaterialesService prod = new MaterialesService();
				ArrayList ver = (ArrayList) prod.verTodos();
				int tam = ver.size();
				String spl = "";
				Material mat = null;

				ArrayList array1 = prod.cargaTipoMats();//091207
				ArrayList array2 = prod.cargaTipoMano();//091207
				ArrayList array3 = prod.cargaTipoOtros();//091207
				String matx = "";//091207
			%>




			<script type="text/javascript">
				var cadenaT='';
				var cadenaT2='';
				var cadenaT3='';
				var texto = '';
				var listadoMat = '';
				var listadoMan = '';
				var listadoOt = '';
				var listadoMat2 = '';
				var listadoMan2 = '';
				var listadoOt2 = '';
				var listadoMat3 = '';
				var listadoMan3 = '';
				var listadoOt3 = '';
				var listadoMat4 = '';
				var listadoMan4 = '';
				var listadoOt4 = '';

				var last_used='M';

			</script>
			<%
						for (int i = 0; i < array1.size(); i++) {
							matx = (String) array1.get(i);
							//System.out.println(".... "+matx);
			%>
			<script type="text/javascript">
				cadenaT = cadenaT + '<%=matx%>' + ';_;';
			</script>
			<%
						}
						for (int i = 0; i < array2.size(); i++) {
							matx = (String) array2.get(i);
							//System.out.println("....2 "+matx);
	%>
			<script type="text/javascript">
				cadenaT2 = cadenaT2 + '<%=matx%>' + ';_;';
			</script>
			<%
						}
						for (int i = 0; i < array3.size(); i++) {
							matx = (String) array3.get(i);
							//System.out.println("....3 "+matx);
	%>
			<script type="text/javascript">
				cadenaT3 = cadenaT3 + '<%=matx%>' + ';_;';
			</script>
			<%
						}
						ArrayList ds = prod.buscarPor(1);
						mat = null;
						for (int d = 0; d < ds.size(); d++) {
							mat = (Material) ds.get(d);
							//System.out.println("linea de catsx: "+mat.getCategoria());

			%>
			<script type="text/javascript">
				listadoMat = listadoMat + '<%=mat.getDescripcion()%>' +' Medida: <%=mat.getMedida()%>' + ';_;';
				listadoMat2 = listadoMat2 + '<%=mat.getCodigo()%>' + ';_;';
				listadoMat3 = listadoMat3 + '<%=mat.getValor()%>' + ';_;';//20100608 jjcastro
				listadoMat4 = listadoMat4 + '<%=mat.getCategoria()%>' + ';_;';
			</script>
			<%
						}
						ArrayList ds2 = prod.buscarPor(2);
						for (int d = 0; d < ds2.size(); d++) {
							mat = (Material) ds2.get(d);
			%>
			<script type="text/javascript">
				listadoMan = listadoMan + '<%=mat.getDescripcion()%>' +' Medida: <%=mat.getMedida()%>' + ';_;';
				listadoMan2 = listadoMan2 + '<%=mat.getCodigo()%>' + ';_;';
				listadoMan3 = listadoMan3 + '<%=mat.getValor()%>' + ';_;';//20100608 jjcastro
				listadoMan4 = listadoMan4 + '<%=mat.getCategoria()%>' + ';_;';
			</script>
			<%
						}
						ArrayList ds3 = prod.buscarPor(3);
						for (int d = 0; d < ds3.size(); d++) {
							mat = (Material) ds3.get(d);
			%>
			<script type="text/javascript">
				listadoOt = listadoOt + '<%=mat.getDescripcion()%>' +' Medida: <%=mat.getMedida()%>' + ';_;';
				listadoOt2 = listadoOt2 + '<%=mat.getCodigo()%>' + ';_;';
				listadoOt3 = listadoOt3 + '<%=mat.getValor()%>' + ';_;';//20100608 jjcastro
				listadoOt4 = listadoOt4 + '<%=mat.getCategoria()%>' + ';_;';
			</script>
			<%
						}
			%>

			<%int x = 1;%>

        <script>

var n=1;
function agregarCelda(){
n++;
agregar(n);

var tabla = document.getElementById("tablaValores");
//Obtengo el tamano de la tabla para ubicar la ultima celda
var numrow = tabla.rows.length;
//alert(numrow);
var tbody = tabla.insertRow(numrow);
var td1 = tbody.insertCell(0);
//td1.colSpan = "5";

//<tr class="fila">
//<td colspan="5">
var x1='';
//x1 = x1 + '<table id="tablaValores"><tr><td>';
x1 = x1 + '<table width="1600" height="30" border="1" style="border-collapse:collapse; border-color:#E4E4E4">';
x1 = x1 + '<tr class = "fila">';

x1 = x1 + '<td width="63" rowspan="2" align="center"><table width="100%"  align="left"><tr>';
x1 = x1 + '<td width="17" ><div align="right" class="fila">'+n+'</div></td>';
x1 = x1 + '<td width="33"><div align="center" id="resta">';
x1 = x1 + '<img id="imgmas'+n+'" name="imgmas'+n+'" src="<%=BASEURL%>/images/botones/iconos/mas.gif" alt="Agregar fila" onClick="agregarCelda()" width="12" height="12"> ';
x1 = x1 + '<img id="imgini'+n+'" name="imgini'+n+'" src="<%=BASEURL%>/images/botones/iconos/menos1.gif" alt="Eliminar fila" onClick="borrarDatosListaNew('+n+');"  width="12" height="12">';
x1 = x1 + '</div></td>';
x1 = x1 + '</tr></table></td>';
/*x1 = x1 + '<td width="40" height="20" class="fila"><div align="center">Tipo </div></td>';
x1 = x1 + '<td width="60" class="fila"><div align="center">Categor&iacute;a</div></td>';
x1 = x1 + '<td colspan="2" class="fila"><div align="center">Descripci&oacute;n</div></td>';
x1 = x1 + '<td width="55" class="fila"><div align="center">Cantidad</div></td>';
x1 = x1 + '<td width="50" class="fila"><div align="center">Valor</div></td>';
x1 = x1 + '<td width="110" class="fila"><div align="center">Base SubTotal </div></td>';
x1 = x1 + '<td width="120" class="fila"><div align="center">Observaci&oacute;n</div></td>';
x1 = x1 + '<td width="47" class="fila"><div align="center">Compra</div></td>';
x1 = x1 + '<td width="177" class="fila"><div align="center">Cantidad Compra </div></td>';*/
x1 = x1 + '</tr>';

x1 = x1 + '<tr class = "fila">';


x1 = x1 + '<td height="27" width="40"><div align="center">';
x1 = x1 + '<select name="radiobutton'+n+'" id="radiobutton'+n+'" onChange="cargarTipos(this.selectedIndex,'+n+')" style="width:40">';
x1 = x1 + '<option value="-1">Seleccione</option>';
x1 = x1 + '<option value="M">M</option>';
x1 = x1 + '<option value="D">D</option>';
x1 = x1 + '<option value="O">O</option>';
x1 = x1 + '</select></div></td>';


x1 = x1 + '<td width="49"><div align="center">';
x1 = x1 + '<select name="menutipos'+n+'" id="menutipos'+n+'" onChange="updateList(this.options[this.selectedIndex],'+n+');" style="width:50">';
x1 = x1 + '<option value="-1" selected >Seleccione...</option>';
x1 = x1 + '</select>';
x1 = x1 + '</div></td>';


//


x1 = x1 + '<td width="10"><span class="barratitulo"><input name="textfieldx'+n+'" type="text" id="textfieldx'+n+'" onKeyUp="buscar(document.forma.producto'+n+',this);instantValue('+n+');xperience(document.forma.cantidad'+n+'.value,'+n+');" size="10"></span></td>';





x1 = x1 + '<td width="830"><span class="barratitulo">';
x1 = x1 + '<select name="producto'+n+'" id="producto'+n+'" style="width:830" onChange="actualizarPrecio(this,'+n+'); xperience(document.forma.cantidad'+n+'.value,'+n+')">';
x1 = x1 + '<option value="-1" selected>Seleccione ...</option>';
        <%
															for (int i = 0; i < tam; i++) {
																mat = (Material) ver.get(i);
																if (mat != null) {
		%>
x1 = x1 + '<option id="<%=mat.getCodigo()%>" value="<%=mat.getValor()%>"><%=mat.getDescripcion()%> Medida: <%=mat.getMedida()%></option>';
        <%
																}
															}
												%>
x1 = x1 + '</select>';
x1 = x1 + '</span></td>';
///

x1 = x1 + '<td width="10"><div align="center">';
x1 = x1 + '<input name="cantidad'+n+'" type="text" id="cantidad'+n+'"  style="text-align:right" onFocus="instantValue   ('+n+')" onKeyUp="xperience(this.value,'+n+');colocarvalor(this.value,'+n+');soloNumeros(this.id)" value="0" size="10"  >';
x1 = x1 + '</div></td>';

x1 = x1 + '<td width="10"><div align="center">';
x1 = x1 + '<input name="precio'+n+'" type="text" id="precio'+n+'" value="0.00" size="10"  style="text-align:right"  onKeyUp="xperience(document.getElementById(\'cantidad'+n+'\').value,'+n+');soloNumeros(this.id)">';//fase3
x1 = x1 + '<input name="valor_2'+n+'" type="hidden" id="valor_2'+n+'">';
x1 = x1 + '</div></td>';

x1 = x1 + '<td width="10"><div align="center">';
x1 = x1 + '<input name="base'+n+'" type="text" id="base'+n+'" value="0.00" size="10" readonly style="text-align:right" >';
x1 = x1 + '<input name="base_2'+n+'" type="hidden" id="base_2'+n+'">';
x1 = x1 + '</div></td>';

x1 = x1 + '<td width="18"><div align="center">';
x1 = x1 + '<input name="nota'+n+'" type="text" id="nota'+n+'" readonly onclick="fila=this.alt;control(event,nota'+n+')" name="nota'+n+'" id="nota'+n+'" width="18" >';
x1 = x1 + '</div></td>';

x1 = x1 + '<td width="20">';
x1 = x1 + '<input name="compra'+n+'" type="checkbox" value="checkbox'+n+'" checked onClick="cambiarCantcompra('+n+')" id="compra'+n+'">';
x1 = x1 + '</td><td width="10">';
x1 = x1 + '<input name="cantidad_compra'+n+'" type="text" id="cantidad_compra'+n+'"  style="text-align:right" onFocus="instantValue('+n+')" onBlur="validarvalor(this.value,'+n+')" value="0" size="10"  >';
x1 = x1 + '</td>';
x1 = x1 + '</tr>';
x1 = x1 + '</table>';
x1 = x1 + '<input name="codp'+n+'" id="codp'+n+'" type="hidden" value="0">';

x1 = x1 + '<input name="indiceDelete'+n+'" id="indiceDelete'+n+'" type="hidden" value="-1">';

//x1 = x1 + '</td></tr></table>';
//</td>
//</tr>

td1.innerHTML = x1;

					if(last_used=='M') cargarTipos(1,n);
					if(last_used=='D') cargarTipos(2,n);
					if(last_used=='O') cargarTipos(3,n);

}



			</script>

			<script type="text/javascript">
				function cargarTipos(valor,ind){
                                    //alert("1");
					var y=null;
					var arraytexto = null;
					if(valor >0){
                                            //alert("2");
						switch(valor){
							case 1:
                                                           // alert("3");
								arraytexto = cadenaT.split(";_;");
								document.getElementById("precio"+ind).readOnly = true;//JJCASTRO
								break;
							case 2:
                                                           // alert("4");
								arraytexto = cadenaT2.split(";_;");
								document.getElementById("precio"+ind).readOnly = false;//JJCASTRO
								break;
							case 3:
                                                         //   alert("5");
								arraytexto = cadenaT3.split(";_;");
								document.getElementById("precio"+ind).readOnly = false;
								break;
						}

						var selection = document.getElementById("menutipos"+ind);
						borradorOptions(selection);
						for(i in arraytexto){
							insertarOptions(arraytexto[i],selection);
						}
					}
					else {
						document.getElementById("precio"+ind).readOnly = true;
						alert('(Debe Seleccionar un Tipo ...)');//fase3
					}
                                        borradorOptions(document.getElementById('producto'+ind));
				}

				function insertarOptions(texto,selection){
					if(texto!='') {
						y = document.createElement('option');
						y.text = texto;
						y.value = texto;
						try {
							//Para navegadores buenos
							selection.add(y,null);
						}
						catch(ex){
							selection.add(y);//Tenia que ser Internet Exploiter ... funcionando a las malas...
						}
					}
				}

				function borradorOptions(selection){
					var ind = 1;
					var tm = selection.length;
					var elim = tm-1;
					while(ind<tm){
						selection.remove(elim);
						elim = elim - 1;
						ind = ind + 1;
					}
				}

				function updateList(optionDrop,indicef){
					//alert('Se ha presionado la categoria '+optionDrop.value);

					var opcionx = document.getElementsByName("radiobutton"+indicef);
					var arrayMat = null;
					var arrayMan = null;
					var arrayOt = null;
					var arrayMat2 = null;
					var arrayMan2 = null;
					var arrayOt2 = null;
					var arrayMat3 = null;
					var arrayMan3 = null;
					var arrayOt3 = null;
					var arrayMat4 = null;
					var arrayMan4 = null;
					var arrayOt4 = null;
					var texto='';
					var index =  0;
					for(var ix = 0;ix<opcionx.length;ix++){
						if(opcionx[ix].selectedIndex > 0){
							index = opcionx[ix].selectedIndex;
							if(opcionx[ix].options[index].value==null) texto='M';
							else texto = opcionx[ix].options[index].value;
							//alert('Se escogio tipo '+texto);
						}
					}
					//alert('Tipo '+texto);

					if(texto=='M'){
						borradorOptions(document.getElementById('producto'+indicef));
						//alert('despues de borrar y antes del split');
						arrayMat = listadoMat.split(';_;');
						//alert('despues primer split long. '+arrayMat.length);
						arrayMat2 = listadoMat2.split(';_;');
						//alert('despues segundo split long. '+arrayMat.length);
						arrayMat3 = listadoMat3.split(';_;');
						arrayMat4 = listadoMat4.split(';_;');
						//alert('Largo lista '+arrayMat4.length);
						//alert('Option text '+optionDrop.value);
						for(var indic=0;indic<arrayMat4.length;indic++){
							if(arrayMat4[indic]==optionDrop.value){
								insertarOptions2(arrayMat[indic],arrayMat3[indic],arrayMat2[indic],document.getElementById("producto"+indicef));
								//alert('Hay '+arrayMat[indic]);
							}

						}
						//alert('Final del if');
					}

					if(texto=='D'){
						borradorOptions(document.getElementById('producto'+indicef));
						arrayMan = listadoMan.split(';_;');
						arrayMan2 = listadoMan2.split(';_;');
						arrayMan3 = listadoMan3.split(';_;');
						arrayMan4 = listadoMan4.split(';_;');
						for(var indic2=0;indic2<arrayMan4.length;indic2++){
							if(arrayMan4[indic2]==optionDrop.value){
								insertarOptions2(arrayMan[indic2],arrayMan3[indic2],arrayMan2[indic2],document.getElementById("producto"+indicef));
							}
						}
					}

					if(texto=='O'){
						borradorOptions(document.getElementById('producto'+indicef));
						arrayOt = listadoOt.split(';_;');
						arrayOt2 = listadoOt2.split(';_;');
						arrayOt3 = listadoOt3.split(';_;');
						arrayOt4 = listadoOt4.split(';_;');
						for(var indic3=0;indic3<arrayOt4.length;indic3++){
							if(arrayOt4[indic3]==optionDrop.value){
								insertarOptions2(arrayOt[indic3],arrayOt3[indic3],arrayOt2[indic3],document.getElementById("producto"+indicef));
							}
						}
					}
					last_used=texto;
				}

				function insertarOptions2(texto,valor,id,selection){
					if(texto!='') {
						y = document.createElement('option');
						y.text = texto;
						y.value = valor;
						y.id = id;
						try {
							//Para navegadores buenos
							selection.add(y,null);
						}
						catch(ex){
							//Para IE
							selection.add(y);//Tenia que ser Internet Exploiter ... funcionando a las malas...
						}
					}
				}






			</script>

<script language="JavaScript">
<!--
//var nav4 = window.Event ? true : false;
/*function acceptNum(evt){
    alert("nada");

// NOTE: Backspace = 8, Enter = 13, '0' = 48, '9' = 57
var key = nav4 ? evt.which : evt.keyCode;
return (key <= 13 || (key >= 48 && key <= 57 || key==46));
}*/
//-->
</script>



			<script type="text/javascript">
				var controller='<%=CONTROLLER%>';
				function enviarP(){



					var tabla = document.getElementById("tablaValores");//jjcastro
					var numrow = tabla.rows.length;
					var i=1;
					while(i<numrow){//100114   //jjcastro 10 mayo 2010
						i++;
					}
					var sendit = confirm("Confirma que ya esta terminada la cotizacion?");
					if(sendit==true){
						document.getElementById("filas").value = i+1;
											forma.action= controller + '?estado=Cotizacion&accion=Crear&numrows='+n;
						//alert("Filas enviadas: "+i);
						if(i>0) forma.submit();
						else alert('Debe enviar algo...');
					}

				}

				function guardar(){

                                                                   // alert("guardar");
					var tabla = document.getElementById("tablaValores");//jjcastro
					var numrow = tabla.rows.length;

                                        //alert("NUMERO_FILAS________"+numrow);

					var i=1;
					while(i<numrow){//100114 //jjcastro 10 mayo 2010
						i++;
					}
					var sendit = confirm("Confirma que quiere guardar la cotizacion?");
					if(sendit==true){
						forma.action= controller + '?estado=Cotizacion&accion=Crear&opcion=save&numrows='+n;
						document.getElementById("filas").value = i;
						if(i>0) forma.submit();
						else alert('Debe enviar algo...');
					}
				}



				//JCastro
				function cambiarCantcompra(indice){

                                var variable = document.getElementById("compra"+indice).checked;

                                if(variable ==  true){
                                    document.getElementById("cantidad_compra"+indice).readOnly = false;//JJCASTRO
                                }else{
                                     document.getElementById("cantidad_compra"+indice).readOnly = true;//JJCASTRO
                                     document.getElementById("cantidad_compra"+indice).value = 0;//JJCASTRO
                                }

				}





			</script>

			<link href="../../../css/estilostsp.css" rel="stylesheet" type="text/css">


                        <style>

 div.autocomplete {
                position:absolute;
                width:550px;
                background-color:white;
                border:1px solid #888;
                margin:0;
                padding:0;
            }
            div.autocomplete ul {
                list-style-type:none;
                margin:0;
                padding:0;
            }
            div.autocomplete ul li.selected { background-color: #D7FFD7;}
            div.autocomplete ul li {
	list-style-type:none;
	display:block;
	margin:0;
	padding:2px;
	height:20px;
	cursor:pointer;
	font-size: 9px;
            }



                        .style1 {font-size: 10px}
                        .style2 {font-size: 8px; }
                        </style>


		</head>

		<body onResize="redimensionar()" onload = "redimensionar();">

	<div id="capaSuperior" style="position:absolute; width:130%; height:100px; z-index:0; left: 0px; top: 0px;">
		<jsp:include page="/toptsp.jsp?encabezado=Crear Cotizacion Contratista"/>
	</div>

	<div id="capaCentral" style="position:absolute; width:130%; height:87%; z-index:0; left: 0px; top: 100px; overflow: scroll;">
				<form id ="forma" name="forma" action="<%= CONTROLLER%>?estado=Cotizacion&accion=Crear&cmd=show" method="post">

<!-- Tabla Grande-->
					<table width="100%" border="2"align="center">
						<tr>
							<td>
<!-- Tabla Interna-->
								<table width="100%" align="center" id="tablaform" >
									<tr>
										<td colspan="2" align="left" class="subtitulo1">Ingresar datos</td>
										<td width="1081" colspan="3" align="left" class="barratitulo"><img alt="titulo" src="<%=BASEURL%>/images/titulo.gif"></td><!-- Modificacion Colspan de 6 a 3 Ing. Jose Castro-->
									</tr>
<!-- Linea Datos-->								<input name="filas" type="hidden" id="filas" value="1">

<!-- Linea Datos-->
									<tr>
										<%
											String idaccion = "";
											//idaccion = request.getParameter("idaccion") == null ? "900046" : request.getParameter("idaccion");
											idaccion = request.getParameter("idaccion") == null ? "" : request.getParameter("idaccion");
											String[] datitos = new String[2];
											datitos = coserv.datosAccion(idaccion).split(";");
											if (datitos.length < 1) {
												System.out.println("datitos esta vacio");
											} else {
												System.out.println("datitos : " + datitos[0] + " " + datitos[1]);
											}
										%>

										<td align="left" valign="middle" class="fila">Multiservicio</td>
										<td colspan="6" valign="middle" class="letra">
										<table width="100%">
                                          <tr class="fila" >
                                            <td width="103" >Id accion</td>
                                            <td width="161"><input name="multiservicios" type="text" id="multiservicios" value="<%=idaccion%>" size="15" READONLY></td>
                                            <td width="119" >Nombre cliente</td>
                                            <td width="1102" ><input name="textfield2" type="text" value="<%=datitos[1]%>" size="100" READONLY></td>
                                          </tr>
                                          <tr class="fila">
                                            <td>Id Solicitud</td>
                                            <td><input name="textfield" type="text" value="<%=datitos[0]%>" size="15" READONLY></td>
											<td>Fecha</td>
											<td>											 <%
												if(coserv.buscarBorrador(idaccion)) {
													ArrayList cde = coserv.buscarDets(idaccion, "id_accion");
													Cotizacion cdets = (Cotizacion)cde.get(0);

                                                                                                        session.setAttribute("fecha", cdets.getFecha().substring(0,10));

											%>
											 <input name="fecha" type="text" id="fecha" value="<%=cdets.getFecha().substring(0,10)%>" READONLY>
                                                                                         <script>
                                                                                                                                                                               //alert("fecha1"+<%=cdets.getFecha().substring(0,10)%>);
                                                                                                                                                                                  </script>
											<%
												} else {
											%>
                                                                                        <script>
                                                                                                                                                                               //alert("fecha2");
                                                                                                                                                                                  </script>

											<input name="fecha" type="text" id="fecha" READONLY>
										  <img alt="calendario" src="<%=BASEURL%>/images/cal.gif" width="16" height="16" align="absmiddle" style="cursor:pointer " onClick="if(self.gfPop)gfPop.fPopCalendar(document.forma.fecha);return false;" HIDEFOCUS>
										  											<%
												}
											%>
							          <a href="<%=BASEURL%>/jsp/delectricaribe/cotizacion/listadocotizaciones.jsp?id_accion_inicial=<%=idaccion%>&contratista=<%=datitos[2]%>&fechap='document.forma.fecha'">Copiar Cotizacion</a></td>
                                          </tr>
                                        </table>
										</td>
									</tr>




<!-- Linea Fecha -->


									<%

                                                                         String criterio = request.getParameter("criterio") == null ? "" : request.getParameter("criterio");

										   if (((!coserv.existeCotizacion(idaccion))&&(criterio.equals("")))  ||(criterio.equals("CANCELAR")&&(!coserv.buscarBorrador(idaccion)))) {
									%>

									<script type="text/javascript">
												  xp();
									</script>


<!-- fila Producto -->
									<tr class="fila">
										<td colspan="2" align="left" class="subtitulo1">Productos</td>
										<td colspan="5" align="left" class="barratitulo"><img alt="titulo" src="<%=BASEURL%>/images/titulo.gif"></td>
									</tr>


									<%int fils = 1;%>
<!-- inicial nueva -->
<tr class="fila"  >
<td colspan="5" >

<table  width="1603" border="1" style="border-collapse:collapse; border-color:#E4E4E4" >
<tr>
<td width="59"> </td>
    <td width="35" height="20" class="fila"><div align="center">Tipo </div></td>
    <td width="60" class="fila"><div align="center">Categor&iacute;a</div></td>

	<td colspan="2" class="fila"><div align="center">Descripci&oacute;n</div></td>

    <td width="83" class="fila"><div align="center">Cantidad</div></td>
    <td width="83" class="fila"><div align="center">Valor</div></td>
    <td width="90" class="fila"><div align="center">Base SubTotal </div></td>
    <td width="107" class="fila"><div align="center">Observaci&oacute;n</div></td>
    <td width="61" class="fila"><div align="center">Compra</div></td>
    <td width="69" class="fila"><div align="center" class="style2">Cantidad Compra </div></td>
</tr>
</table>

<table width="100%" id="tablaValores" onClick="if(event.target.type!='checkbox' && event.target.type!='text'){wclose();}">
  <tr><td><!--Prueba -->





<table width="1600"  height="30" border="1"   style="border-collapse:collapse; border-color:#E4E4E4" cellpadding="0">



  <tr>
    <td width="63" rowspan="2" align="center">
	<table width="100%" border="0" align="center" cellpadding="0" style="border-collapse:collapse; ">
      <tr>
        <td width="17" height="20"><div align="right" class="fila"><%=x%></div></td>
        <td width="33"><div align="center" id="resta"><img id="imgmas<%=x%>" name="imgmas<%=x%>" src="<%=BASEURL%>/images/botones/iconos/mas.gif" alt="Agregar fila" onClick="agregarCelda()" width="12" height="12"> <img id="imgini<%=x%>" name="imgini<%=x%>" src="<%=BASEURL%>/images/botones/iconos/menos1.gif" alt="Eliminar fila" onClick="borrarDatosListaNew(<%=x%>);"  width="12" height="12"></div>		</td>
      </tr>
    </table>	</td>

  </tr>
  <tr>
    <td height="27" width="40">
	  <div align="center">
	    <select name="radiobutton<%=x%>" id="radiobutton<%=x%>" onChange="cargarTipos(this.selectedIndex,<%=x%>)" style="width:40">
	      <option value="-1">Seleccione</option>
	      <option value="M">M</option>
	      <option value="D">D</option>
	      <option value="O">O</option>
	        </select>
      </div></td>
    <td width="52">
	  <div align="center">
	    <select name="menutipos<%=x%>" id="menutipos<%=x%>" onChange="updateList(this.options[this.selectedIndex],<%=x%>);" style="width:50">
	      <option value="-1" selected >Seleccione...</option>
	        </select>
      </div></td>

	  <td width="52"><span class="barratitulo">
	     <input name="textfieldx<%=x%>" type="text" id="textfieldx<%=x%>" onKeyUp="buscar(document.forma.producto<%=x%>,this);instantValue(<%=x%>);xperience(document.forma.cantidad<%=x%>.value,<%=x%>);" size="10">
	  </span></td>




    <td width="850"><span class="barratitulo">
      <select name="producto<%=x%>" id="producto<%=x%>" style="width:850" onChange="actualizarPrecio(this,<%=x%>); xperience(document.forma.cantidad<%=x%>.value,<%=x%>);">
        <option value="-1" selected>Seleccione ...</option>

      </select>
    </span></td>






    <td width="10"><div align="center">
      <input name="cantidad<%=x%>" type="text" id="cantidad<%=x%>"  style="text-align:right" onFocus="instantValue(<%=x%>)" onKeyUp="xperience(this.value,<%=x%>);colocarvalor(this.value,<%=x%>);soloNumeros(this.id)" value="0" size="10"  >
    </div></td>
    <td width="52"><div align="center">
            <input name="precio<%=x%>" type="text" id="precio<%=x%>" value="0.00" size="10"  style="text-align:right" onKeyUp="xperience(document.getElementById('cantidad<%=x%>').value,<%=x%>);soloNumeros(this.id)" >
      <input name="valor_2<%=x%>" type="hidden" id="valor_2<%=x%>">
    </div></td>
    <td width="52"><div align="center">
      <input name="base<%=x%>" type="text" id="base<%=x%>" value="0.00" size="10" readonly style="text-align:right" >
      <input name="base_2<%=x%>" type="hidden" id="base_2<%=x%>">
    </div></td>
    <td width="18"><div align="center">
      <input name="nota<%=x%>" type="text" id="nota<%=x%>" alt="<%=x+1%>" readonly onClick="fila=this.alt;control(event,'nota<%=x%>')" width="18">
    </div></td>
    <td width="20">
      <div align="center">
          <input name="compra<%=x%>" type="checkbox" value="checkbox<%=x%>"   id="compra<%=x%>" checked onClick="cambiarCantcompra(<%=x%>)">
        </div></td><td width="60"><div align="center">
          <input name="cantidad_compra<%=x%>" type="text" id="cantidad_compra<%=x%>"  style="text-align:right" onFocus="instantValue(<%=x%>)" onBlur="validarvalor(this.value,<%=x%>)" value="0" size="10"  >
        </div></td>
  </tr>
</table>
<input name="codp<%=x%>" id="codp<%=x%>" type="hidden" value="0">

<input name="indiceDelete<%=x%>" id="indiceDelete<%=x%>" type="hidden" value="-1"><!-- Valor Inicial Indice-->
<input name="Maximo" id="Maximo" type="hidden" value=""><!-- Valor Inicial Indice-->
<script type="text/javascript">
      document.getElementById("Maximo").value = n;
      instantValue(<%=x%>);
</script>

</td></tr></table><!--Prueba -->
</td>
</tr>



<!-- inicial nueva -->







<!-- fila inicial de valores -->


<!-- fila inicio de valores fin -->





									<%
										   } else if ((coserv.buscarBorrador(idaccion))||(criterio.equals("ADICIONAR")||(criterio.equals("REEMPLAZAR"))||(criterio.equals("CANCELAR")&&coserv.buscarBorrador(idaccion)))){

                                                                                          ArrayList detalles = null;
                                                                                           //criterio = request.getParameter("criterio") == null ? "" : request.getParameter("criterio");
											   if(criterio.equals("")){
                                                                                            detalles = coserv.buscarDets(idaccion, "id_accion");
                                                                                           }else{
                                                                                            detalles = (ArrayList) session.getAttribute("detalles");
                                                                                           }

                                                                                           String fechaplantilla = (String) session.getAttribute("fecha");
                                                                                          // System.out.println("fechaplantilla_________________"+fechaplantilla);


                                                                                      if((fechaplantilla!=null)&&(!fechaplantilla.equals(""))) {
                                                                                   %>
                			<script type="text/javascript">

                                                                                var f = <%=fechaplantilla%>;
                                                                                //alert("f_____"+f);
                                                                                document.getElementById("fecha").value= '<%=fechaplantilla%>';
                                                                                //alert(document.getElementById("fecha").value);
                    								</script>
                                        <%
                                                                                }else{
                                                                                %>

                			<script type="text/javascript">
												  xp();
                    								</script>
                                        <%}%>




									<tr class="fila">
										<td colspan="2" align="left" class="subtitulo1">Productos</td>
										<td colspan="5" align="left" class="barratitulo"><img alt="titulo" src="<%=BASEURL%>/images/titulo.gif"></td>
									</tr>

<tr class="fila"  >
<td colspan="5" >

<table  width="1603" border="1" style="border-collapse:collapse; border-color:#E4E4E4" >
<tr>
<td width="59"> </td>
    <td width="35" height="20" class="fila"><div align="center">Tipo </div></td>
    <td width="60" class="fila"><div align="center">Categor&iacute;a</div></td>

	<td colspan="2" class="fila"><div align="center">Descripci&oacute;n</div></td>

    <td width="83" class="fila"><div align="center">Cantidad</div></td>
    <td width="83" class="fila"><div align="center">Valor</div></td>
    <td width="90" class="fila"><div align="center">Base SubTotal </div></td>
    <td width="107" class="fila"><div align="center">Observaci&oacute;n</div></td>
    <td width="61" class="fila"><div align="center">Compra</div></td>
    <td width="69" class="fila"><div align="center" class="style2">Cantidad Compra </div></td>
</tr>
</table>
<table width="100%" id="tablaValores" onClick="if(event.target.type!='checkbox' && event.target.type!='text'){wclose();}">



									<%
												Material mtx = null;
												Cotizacion ctx = null;
												String mxt = "";
												ArrayList listdets = null;
												while(x<=detalles.size()) {
													ctx = (Cotizacion)detalles.get(x-1);
													mxt = ctx.getMaterial();
													listdets = prod.buscarPor(1, mxt);
													if(listdets.size()>0) mtx = (Material)listdets.get(0);
													else listdets = prod.buscarPorAnul(1, mxt);
													if(listdets.size()>0) mtx = (Material)listdets.get(0);

									%>

								<!-- Cuando trae valor-->

  <tr><td><!--Prueba -->


<table width="1600"  height="30" border="1"   style="border-collapse:collapse; border-color:#E4E4E4" cellpadding="0">

  <tr>
    <td width="63" rowspan="2" align="center">
	<table width="100%" border="0" align="left" cellpadding="0" style="border-collapse:collapse;">
      <tr>
        <td width="24"><div align="right" class="fila"><%=x%></div></td>
        <td width="42"><div align="center" id="resta"><img id="imgmas<%=x%>" name="imgmas<%=x%>" src="<%=BASEURL%>/images/botones/iconos/mas.gif" alt="Agregar fila" onClick="agregarCelda()" width="12" height="12"> <img id="imgini<%=x%>" name="imgini<%=x%>" src="<%=BASEURL%>/images/botones/iconos/menos1.gif" alt="Eliminar fila" onClick="borrarDatosListaNew(<%=x%>);"  width="12" height="12"></div>
		</td>
      </tr>
    </table>
	</td>
  </tr>
  <tr>
    <td height="27" width="40">
	  <div align="center">
	    <select name="radiobutton<%=x%>" id="radiobutton<%=x%>" onChange="cargarTipos(this.selectedIndex,<%=x%>)" style="width:40">
	      <option value="-1">Seleccione</option>
	      <option value="M" <% if(mtx.getTipo().equals("M")) out.print("selected"); %> >M</option>
	      <option value="D" <% if(mtx.getTipo().equals("D")) out.print("selected"); %> >D</option>
	      <option value="O" <% if(mtx.getTipo().equals("O")) out.print("selected"); %> >O</option>
	        </select>
      </div></td>
    <td width="50">
	  <div align="center">
	    <select name="menutipos<%=x%>" id="menutipos<%=x%>" onChange="updateList(this.options[this.selectedIndex],<%=x%>);" style="width:50">
	      <option value="-1" selected >Seleccione...</option>
	        </select>
	    <script type="text/javascript">
																		<% if(mtx.getTipo().equals("M")){%>
																		cargarTipos(1,<%=x%>);
																		<% }  if(mtx.getTipo().equals("D")){%>
																		cargarTipos(2,<%=x%>);
																		<% }  if(mtx.getTipo().equals("O")){%>
																		cargarTipos(3,<%=x%>);
																		<% } %>
														</script>
	  </div></td>

	  	  <td width="53"><span class="barratitulo">
	    <input name="textfieldx<%=x%>" type="text" id="textfieldx<%=x%>" onKeyUp="buscar(document.forma.producto<%=x%>,this);instantValue(<%=x%>);xperience(document.forma.cantidad<%=x%>.value,<%=x%>);" size="10">
	  	  </span></td>


	      <input name="codp<%=x%>" id="codp<%=x%>" type="hidden" value="<%=mtx.getCodigo()%>">
    <td width="850"><div align="center"><span class="barratitulo">
												<select name="producto<%=x%>" id="producto<%=x%>" onChange="actualizarPrecio(this,<%=x%>); xperience(document.forma.cantidad<%=x%>.value,<%=x%>);" style="width:850">
													<option value="-1" selected>Seleccione ...</option>
													<%
																for (int i = 0; i < tam; i++) {
																	mat = (Material) ver.get(i);
																	if (mat != null) {
													%>

													<option id="<%=mat.getCodigo()%>" value="<%=mat.getValor()%>" <% if(mtx.getCodigo().equals(mat.getCodigo())) out.print(" selected "); %>><%=mat.getDescripcion()%> Medida: <%=mat.getMedida()%></option>

													<%
																	}
																}
													%>

						  </select>    </span></div></td>
    <td width="10"><div align="center">

	  <input name="cantidad<%=x%>" type="text" id="cantidad<%=x%>" value="<%=ctx.getCantidad()%>" onFocus="instantValue(<%=x%>)" onKeyUp="xperience(this.value,<%=x%>);colocarvalor(this.value,<%=x%>);soloNumeros(this.id)" size="10" style="text-align:right">
    </div></td>
    <script>
          document.getElementById("cantidad"+<%=x%>).value = formatCantidad('<%=ctx.getCantidad()%>','<%=x%>');

       </script>


    <td width="10"><div align="center">
            <input type="text" id="precio<%=x%>" name="precio<%=x%>" value="<%= Util.customFormat(ctx.getValor()==0?mtx.getValor():ctx.getValor()) %>"  onKeyUp="xperience(document.getElementById('cantidad<%=x%>').value,<%=x%>);soloNumeros(this.id)" style="text-align:right" size="10"   >
	<input name="valor_2<%=x%>" type="hidden" id="valor_2<%=x%>">
														    <%
																double bs = ctx.getCantidad() * (ctx.getValor()==0?mtx.getValor():ctx.getValor());


															%>

    </div></td>
    <td width="10"><div align="center">
		<input name="base<%=x%>" type="text" id="base<%=x%>" readonly value="<%=Util.customFormat(bs)%>" style="text-align:right" size="10">
		<input name="base_2<%=x%>" type="hidden" id="base_2<%=x%>" value="<%=bs%>">



    </div></td>
    <td width="18">
	  <input name="nota<%=x%>" type="text" id="nota<%=x%>" value="<%=ctx.getObservacion() %>" readonly onClick="fila=this.alt;control(event,'nota<%=x%>')"  width="18">
    </td>
    <td width="20">
      <div align="center">
          <input name="compra<%=x%>" type="checkbox" value="checkbox<%=x%>" <% if(ctx.getCompra().equals("S")){ %>checked <%}%>>
        </div></td><td width="60"><div align="center">
          <input name="cantidad_compra<%=x%>" type="text" id="cantidad_compra<%=x%>"  style="text-align:right" onFocus="instantValue(<%=x%>)" onBlur="validarvalor(this.value,<%=x%>)" value="<%=ctx.getCantidad_compra()%>" size="10"  >
        </div></td>
  </tr>




</table>
<input name="codp<%=x%>" id="codp<%=x%>" type="hidden" value="0">

<input name="indiceDelete<%=x%>" id="indiceDelete<%=x%>" type="hidden" value="-1"><!-- Valor Inicial Indice-->
<input name="Maximo" id="Maximo" type="hidden" value=""><!-- Valor Inicial Indice-->
</td></tr>
  <script type="text/javascript">
      document.getElementById("Maximo").value = n;
</script>
	<script type="text/javascript">
													n++;
												</script>
												<% 	x++;
													}
											    %>






</table><!--Prueba -->
</td>
</tr>

								<!-- Cuando trae valor-->











							  </table>
<!-- Tabla Interna-->

						  </td>
					  </tr>
								   <script type="text/javascript">	n = n - 1;//esto corrige un bug... </script>
								   <%  } else { %>
									<script type="text/javascript">
										alert("Ya existe una cotizacion para esta accion...");
									</script>
									<% } %>
				  </table>

<!-- Tabla Grande-->



		<div align="center">
						<br>
						<%
							 if (!coserv.existeCotizacion(idaccion)) {
						%>
	<img src="<%= BASEURL%>/images/botones/aceptar.gif" name="imgaceptar" alt="Aceptar" onClick="enviarP();" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" style="cursor:pointer ">&nbsp;
						<img src="<%= BASEURL%>/images/botones/guardar.gif" name="imgguardar" alt="Guardar" onMouseOver="botonOver(this);" onClick="guardar();" onMouseOut="botonOut(this);" style="cursor:pointer ">
						<%
							 }
							 if(coserv.buscarBorrador(idaccion)) {
						%>
						<img src="<%= BASEURL%>/images/botones/aceptar.gif" name="imgaceptar" alt="Aceptar" onClick="enviarP();" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" style="cursor:pointer ">&nbsp;

						<img src="<%= BASEURL%>/images/botones/guardar.gif" name="imgguardar" alt="Guardar" onMouseOver="botonOver(this);" onClick="guardar();" onMouseOut="botonOut(this);" style="cursor:pointer ">
						<%
							}
						%>
						<img src="<%= BASEURL%>/images/botones/salir.gif" name="imgsalir" alt="Salir" onMouseOver="botonOver(this);" onClick="parent.close();" onMouseOut="botonOut(this);" style="cursor:pointer ">
		</div>
	                <div id="contenido" align="center" style="width: 200px;height: 100px; visibility: hidden; background-color: white">
                        <textarea id="cont" style="width: 95%;height: 95%;"  onkeyup="$(argumento).value=this.value;"></textarea>
                    </div>

					            <script>
                var win ;
                var argumento;
                var fila;
                function control(e,arg)
                {   argumento=arg;
                    $('cont').value=$(arg).value;
                    if(!win){
                        openWin(e,arg);
                    }
                    else{
                        $('cont').value=$(arg).value;
                        win.setLocation(e.clientY,e.clientX+20);
                        win.setTitle("OBSERVACION ITEM "+fila);
                    }
                    $('cont').focus();
                }

                function wclose(){
                    if(win){
                        $('contenido').style.visibility='hidden';
                        win.destroy();
                        win=null;
                    }
                }
                function wclose2(){
                    $('contenido').style.visibility='hidden';
                    win=null;
                }
                function Select_all(theForm,ele,tipo){
                    if(!ele.checked){
                        wclose();
                    }
                    if (ele.id=='All' && tipo=='00'){
                        for (i=0;i<theForm.length;i++)
                            if (theForm.elements[i].type=='checkbox')
                                theForm.elements[i].checked=ele.checked;
                    }else{
                        for (i=0;i<theForm.length;i++)
                            if (theForm.elements[i].type=='checkbox' && theForm.elements[i].id==ele.id && tipo=='50')
                                theForm.elements[i].checked=ele.checked;
                    }

                }


                function openWin(e,arg){
                    $('contenido').style.visibility='visible';
                    win= new Window(
                    {   id: "detalles",
                        title: "OBSERVACION ITEM "+fila,
                        width:$('contenido').width,
                        height:$('contenido').heigth,
                        destroyOnClose: true,
                        onClose:wclose2,
                        maximizable:false,
                        resizable: false,
                        closable:false,
                        minimizable:false
                    });
                    win.setContent('contenido', true, true);
                    win.show(false);
                    win.setLocation(e.clientY, e.clientX);
                    $('cont').focus();
                }
                function send (theForm,url)
                {   p = "evento=ANULAR";
                    openInfoDialog('<b> Ejecutando<br/>Por favor espere...</b>');
                    for (i=0;i<theForm.length;i++)
                    {   if (theForm.elements[i].type=='checkbox' && theForm.elements[i].id!='All' && theForm.elements[i].checked)
                        {   p=p+"&id=" + theForm.elements[i].value + "&obs"+theForm.elements[i].value+"="+ $("obs"+theForm.elements[i].value).value;
                            index=theForm.elements[i].parentNode.parentNode.rowIndex;
                            $('mytable').deleteRow(index);
                        }
                    }
                    new Ajax.Request(
                    url,
                    {   method: 'post',
                        parameters: p,
                        onComplete: complete
                    });
                }
                function complete(response)
                {   Dialog.closeInfo();
                    Dialog.alert(response.responseText, {
                        width:250,
                        height:100,
                        windowParameters: {className: "alphacube"}
                    });
                }
                function openInfoDialog(mensaje) {
                    Dialog.info(mensaje, {
                        width:250,
                        height:100,
                        showProgress: true,
                        windowParameters: {className: "alphacube"}
                    });
                }
            </script>
				</form>
	</div>

			<iframe width="188" height="166" name="gToday:normal:agenda.js:gfPop:plugins.js" id="gToday:normal:agenda.js:gfPop:plugins.js" src="<%=BASEURL%>/js/Calendario/ipopeng.htm" scrolling="yes" frameborder="0" style="visibility:visible; z-index:999; position:absolute; left:-500px; top:0px;">
			</iframe>
		</body>

	</html>
	<%
		String mens = (String) request.getAttribute("msg");
		if (mens != null && !mens.equals("")) {
	%>
	<script type="text/javascript">
		alert('<%=mens%>');
	</script>
	<%}%>