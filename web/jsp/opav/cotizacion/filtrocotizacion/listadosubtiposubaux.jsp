	<%@page contentType="text/html"%>
<%@page session="true"%>
<%@page import="java.util.*"%>
<%@page import="com.tsp.operation.model.*"%>
<%@page import="com.tsp.operation.model.beans.*"%>
<%@page import="com.tsp.operation.model.services.*"%>
<%@page import="com.tsp.util.*"%>
<%@ taglib uri="http://jakarta.apache.org/taglibs/input-0.90" prefix="input" %>
<%@include file="/WEB-INF/InitModel.jsp"%>
<script src="<%=BASEURL%>/js/prototype.js"        type="text/javascript"></script>
<script src="<%=BASEURL%>/js/categoria.js"        type="text/javascript"></script>
<link href="<%= BASEURL%>/css/estilostsp.css" rel="stylesheet" type="text/css">
<link href="<%=BASEURL%>/css/default.css"       rel="stylesheet" type="text/css">
<link href="<%=BASEURL%>/css/alert.css" rel="stylesheet" type="text/css"/>
<link href="<%=BASEURL%>/css/alphacube.css" rel="stylesheet" type="text/css"/>
<script type="text/javascript" src="<%=BASEURL%>/js/buscarproveedor.js"></script>
<script type='text/javascript' src="<%= BASEURL%>/js/boton.js"></script>
<script type='text/javascript' src="<%=BASEURL%>/js/Validacion.js"></script>

<%@page contentType="text/html"  pageEncoding="UTF-8"%>
<link href="<%=BASEURL%>/css/mac_os_x.css"      rel="stylesheet" type="text/css">


<script type='text/javascript' src="<%= BASEURL%>/js/general.js"></script>
<script src='<%=BASEURL%>/js/tools.js' language='javascript'></script>
<script src='<%=BASEURL%>/js/date-picker.js'></script>
<script src="<%= BASEURL%>/js/transferencias.js"></script>

<script type='text/javascript' src="<%= BASEURL%>/js/prototype.js"></script>
<script type='text/javascript' src="<%= BASEURL%>/js/scriptaculous/scriptaculous.js"></script>
<script type='text/javascript' src="<%= BASEURL%>/js/autosuggest.js"></script>
<script type='text/javascript' src="<%= BASEURL%>/js/scriptaculous/controls.js"></script>
<script type='text/javascript' src="<%= BASEURL%>/js/scriptaculous/builder.js"></script>
<script type="text/javascript" src="<%=BASEURL%>/js/effects.js"        ></script>
<script type="text/javascript"src="<%=BASEURL%>/js/window.js"        ></script>
<script type="text/javascript"src="<%=BASEURL%>/js/window_effects.js" ></script>
<link href="<%=BASEURL%>/css/default.css"       rel="stylesheet" type="text/css">
<link href="<%=BASEURL%>/css/alert.css" rel="stylesheet" type="text/css"/>
<link href="<%=BASEURL%>/css/alphacube.css" rel="stylesheet" type="text/css"/>


<%-- 
    Document   : listados de subcategorias y tiposubcategorias 
    Created on : 18/08/2010, 11:22:39 AM
    Author     : Jose Castro
--%>
<%
            int op = (request.getParameter("op") != null) ? Integer.parseInt(request.getParameter("op").toString()) : -1;
            switch (op) {
              
                case 0:
                      String ind = request.getParameter("ind") == null ? "" : request.getParameter("ind");
                      String tipo = request.getParameter("tipo") != null ? request.getParameter("tipo") : "";
%>
                <table width="53%" border="2"align="center">
                    <tr>
                        <td>
                            <table width="100%" align="center" >
                                <tr>
                                    <td colspan="2" align="left" class="subtitulo1">Listado </td>
                                    <td colspan="3" align="left" class="letra"><img src="<%=BASEURL%>/images/titulo.gif"> Clic sobre la fila para asignar... </td>
                                </tr>
                                <tr>
                                    <td width="90" align="left" class="fila" ><div align="right">IDSubCategoria</div></td>
                                    <td colspan="2"  class="fila" width="350" ><div align="center">Descripcion Subcategoria</div></td>
                                    <td width="90"  class="fila" ><div align="center">IDTipoSubCategoria</div></td>
                                    <td width="350" align="left" class="fila" ><div align="center">Descripcion TipoSubCategoria</div></td>
                                </tr>
                                <%
                            ArrayList ver = (ArrayList) session.getAttribute("resultado");
                            int tam = ver.size();
                            Material spl = null;
                            for (int i = 0; i < tam; i++) {
                                spl = (Material) ver.get(i);
                                if (spl != null) {
                                %>
                                <tr  id="fila<%=i%>" onMouseOver="uno(this,'orange', <%=i%>);"  onmouseout="uno(this,'#EAFFEA', <%=i%>);" onclick="setCheckedValue('<%=CONTROLLER%>', '<%=BASEURL%>',<%=ind%>, <%=spl.getIdsubcategoria()%>, <%=spl.getIdtiposubcategoria()%>, <%=spl.getIdcategoria()%> , '<%=spl.getDesc_categoria()%>','<%=tipo%>');"  class="letra">
                                    <td align="left" valign="top" >
                                      <div align="center"><%=spl.getIdsubcategoria()%></div></td>
                                    <td colspan="2" valign="middle" ><div align="center"><%=spl.getDesc_subcat()%></div></td>
                                    <td width="90"><div align="center"><%= spl.getIdtiposubcategoria()%></div></td>
                                    <td><div align="center"><%=spl.getDesc_tiposub()%></div></td>
                                </tr>
                                <%
                                 }
                             }
                                %>
                          </table>	  </td>
                    </tr>
                </table>
                <div align="center"><br>

                    <img src="<%= BASEURL%>/images/botones/salir.gif"  name="imgsalir"  onMouseOver="botonOver(this);" onClick="parent.close();" onMouseOut="botonOut(this);"  style="cursor:hand ">                  
                </div>
                      
                     
                      
                      
                      <%
                    break;
            }
%>
                    
                