<%@page import="com.tsp.operation.model.beans.Usuario"%>
<!--
- Autor : Ing. Rhonalf Martinez
- Date  : 25 de julio del 2009
- Copyrigth Notice : Fintravalores S.A.
-->
<%--
-@(#)
--Descripcion : P�gina JSP, que maneja el ingreso de ordenes de compra.
--%>
<%@page contentType="text/html"%>
<%@page session="true"%>
<%@page import="java.util.*"%>
<%@page import="com.tsp.opav.model.*"%>
<%@page import="com.tsp.opav.model.beans.*"%>
<%@page import="com.tsp.opav.model.services.*"%>
<%@page import="com.tsp.util.*"%>
<%@include file="/WEB-INF/InitModel.jsp"%>
<%@ taglib uri="http://jakarta.apache.org/taglibs/input-0.90" prefix="input" %>
<html>
<head>
<title>Cotizacion - Datos</title>
<link href="<%= BASEURL %>/css/estilostsp.css" rel="stylesheet" type="text/css">
    <script type='text/javascript' src="<%=BASEURL%>/js/Validacion.js"></script>
    <script type='text/javascript' src="<%= BASEURL %>/js/boton.js"></script>
    <script type='text/javascript' src="<%= BASEURL %>/js/general.js"></script>
    <script type='text/javascript' src='<%=BASEURL%>/js/tools.js' language='javascript'></script>
    <script type='text/javascript' src='<%=BASEURL%>/js/date-picker.js'></script>
    <script type='text/javascript' src="<%= BASEURL %>/js/transferencias.js"></script>
    <script type="text/javascript" src="<%=BASEURL%>/js/buscarproveedor.js"></script>
    <script type="text/javascript" src="<%=BASEURL%>/js/prototype.js"></script>
    <script type="text/javascript">
            var nfil=0;
            function enviarForm(){
                var sendit = confirm("Seguro que quiere anular esta cotizacion ?");
                if(sendit==true) document.forma.submit();
            }
            function exportar(accion){//2010-06-17 rhonalf
                document.getElementById("imgexport").src='<%= BASEURL%>/images/botones/exportarExcelDisable.gif';
                document.getElementById("imgexport").onclick='';
                document.getElementById("imgexport").onmouseover='';
                document.getElementById("imgexport").onmouseout='';
                var url = "<%=CONTROLLEROPAV%>?estado=Cotizacion&accion=Excel";
                var p =  "idaccion="+accion;
                new Ajax.Request(
                    url,
                    {
                        method: 'post',
                        parameters: p,
                        onLoading: loading,
                        onComplete: llenarDiv
                    });
            }

            function loading(){
                document.getElementById("busqueda").innerHTML ='<span class="fila">Generando </span><img alt="cargando" src="<%= BASEURL%>/images/cargando.gif"  name="imgload">';
            }

            function llenarDiv(response){
                document.getElementById("busqueda").innerHTML = response.responseText;
            }
    </script>
</head>

<body onResize="redimensionar()" onload = "redimensionar();">
<div id="capaSuperior" style="position:absolute; width:100%; height:100px; z-index:0; left: 0px; top: 0px;">
 <jsp:include page="/toptsp.jsp?encabezado=Cotizacion - Datos"/>
</div>
<div id="capaCentral" style="position:absolute; width:100%; height:87%; z-index:0; left: 0px; top: 100px; overflow: scroll;">


<%
	   //String codCons = (String)request.getAttribute("consecutivo");
	   ArrayList listica = (ArrayList)request.getAttribute("lista");
           Usuario usuario = (Usuario) session.getAttribute("Usuario");//100129
	   CotizacionService cserv = new CotizacionService(usuario.getBd());
	   MaterialesService prserv = new MaterialesService(usuario.getBd());
	   String idaccion="";
	   if (listica==null || listica.size()==0){
	   		String mens1=(String)request.getAttribute("msg");
	   		%>
			<script type="text/javascript">
				alert('<%=mens1%>');
				window.close();
			</script>
			<%
	   }else{

%>
<form id ="forma" name="forma" action="<%=CONTROLLEROPAV%>?estado=Cotizacion&accion=Anular" method="post">

  <table width="100%" border="2"align="center">
    <tr>
      <td>
	  <table width="100%" align="center" >
        <tr>
          <td width="610" colspan="6" align="left" class="subtitulo1">Cotizacion - Datos</td>
          <td width="285" colspan="3" align="left" class="barratitulo"><img alt="titulo" src="<%=BASEURL%>/images/titulo.gif"></td>
          </tr>
      </table>
	  <br>
	  <%
	  		int tam = listica.size();
			Cotizacion compra = null;
			if(tam>0) {
	  %>
	  <table width="100%"  border="0">
        <tr class="subtitulo1">
          <td>Consecutivo</td>
          <td>Id Accion </td>
          <td>Codigo del material </td>
          <td>Descripcion</td>
          <td>Medida</td>
          <td>Cantidad</td>
	  <td>Pnd.Compra</td>
          <td>Valor unitario </td>
          <td>Valor total </td>
          <td>Observacion</td>
        </tr>
		<%
				ArrayList rs = null;//2010-01-14
                                Material f = null;//2010-01-14
                                for(int i=0;i<tam;i++){
					compra=(Cotizacion)listica.get(i);
					String con = compra.getCodigo();
                                        idaccion = compra.getAccion();
		%>
        <tr class="fila">
          <td><span class="letra"><%=con%></span></td>
          <td><span class="letra"><%=compra.getAccion()%></span>
		  	<input id="codigo<%=i%>" name="codigo<%=i%>" type="hidden" value="<%=con%>">
		  </td>
          <td><span class="letra"><%=compra.getMaterial()%></span></td>
          <td><span class="letra">
            <%
					System.out.println("compra.getMaterial():"+compra.getMaterial());
					rs = (ArrayList)prserv.buscarPor(1,compra.getMaterial());
                                        if (rs.size()>0) f = (Material)rs.get(0);//2010-01-14
                                        else {
                                            rs = prserv.buscarPorAnul(1, compra.getMaterial());//2010-01-30
                                            if (rs.size()>0) f = (Material)rs.get(0);//2010-01-30
                                        }
					if(f!=null) {
						out.print(f.getDescripcion());
					}
					else {
						out.print(compra.getMaterial());
					}
		  %>
          </span></td>
          <td><span class="letra">
            <%
					if(f!=null) {
						out.print(f.getMedida());
					}
					else {
						out.print("Unidades");
					}
					if(f!=null){
		  %>
          </span></td>
          <td><span class="letra"><script type="text/javascript">
                                        document.write(formatNumber(<%=compra.getCantidad()%>));
                                  </script>
                </span></td>
	  <td><span class="letra"><script type="text/javascript">
                                        document.write(formatNumber(<%=(compra.getCantidad()-compra.getCantidadComprada())%>));
                                  </script>
                </span>
           </td>
          <td><span class="letra">$ <script type="text/javascript">document.write(formatNumber(<%=compra.getValor()==0?f.getValor():compra.getValor()%>));</script></span></td>
          <td><span class="letra">
            <%
		  		double val_st =(compra.getValor()==0?f.getValor():compra.getValor())*compra.getCantidad();//Gratis1
		  	%>$
			<script type="text/javascript">
			  document.write(formatNumber(<%=val_st%>));
			  nfil = nfil + <%=val_st%>;
		    </script>
          </span></td>
          <td><span class="letra"><%=compra.getObservacion()%></span></td>
        </tr>
        <%                      }
                                     }
        %>
      </table>
	  <br>
      <table width="100%" border="1" align="center" cellpadding="0" cellspacing="0" bordercolor="#000000">
        <tr class="fila">
          <td width="53%" align="center" class="barratitulo">Total cotizacion</td>
          <td width="47%" align="center" class="barratitulo">$ <script type="text/javascript"> document.write(formatNumber(nfil)); </script></td>
        </tr>
      </table>
	  <br>
	  <%
	  		}
			else {
				request.setAttribute("msg","No hay datos que mostrar...");
			}
	  %>
	  </td>
    </tr>
  </table>
  <div align="center">
    <br>
      <img alt="salir" src="<%= BASEURL%>/images/botones/salir.gif" name="imgsalir" onMouseOver="botonOver(this);" onClick="parent.close();" onMouseOut="botonOut(this);"  style="cursor:pointer ">
      <img alt="exportarr" src="<%= BASEURL%>/images/botones/exportarExcel.gif" id="imgexport" name="imgexport" onMouseOver="botonOver(this);" onClick="exportar('<%= idaccion %>');" onMouseOut="botonOut(this);" style="cursor:pointer ">

      <%
        if(tam>0) {
            ClientesVerService clvsrv = new ClientesVerService(usuario.getBd());//100129
            String perfil= clvsrv.getPerfil(usuario.getLogin());//100129
            String estado = clvsrv.estadoAccion(idaccion);
            if(estado.equals("040") || estado.equals("050") || estado.equals("060") || estado.equals("070") || estado.equals("080")){
                //aca hay que verificar que el usuario tenga autorizacion para modificar la cotizacion en este punto
                if(clvsrv.ispermitted(perfil,"10")||(clvsrv.ispermitted(perfil,"23")&& (Integer.parseInt(estado)>=50) || (Integer.parseInt(estado)<=80)  ) ){
      %>
      <img alt="modificar" src="<%= BASEURL%>/images/botones/modificar.gif" name="imgmodificar" id="imgmodificar"  style="cursor:pointer " onClick="window.location.href='<%=BASEURL%>/jsp/opav/cotizacion/listadoitemcotizacion.jsp?operacion=modificar&idaccion=<%=idaccion%>','mywindow','status=no,scrollbars=no, width=450, height=190, resizable=yes';"  onMouseOver="botonOver(this);" onMouseOut="botonOut(this);">

      <%
                }
                if(clvsrv.ispermitted(perfil,"12") && estado.equals("040")){//20100217
      %>
      <img alt="anular" src="<%= BASEURL%>/images/botones/anular.gif" name="imganular" id="imganular"  style="cursor:pointer " onClick="enviarForm();"  onMouseOver="botonOver(this);" onMouseOut="botonOut(this);">

      <%
                }
               if(!clvsrv.ispermitted(perfil,"10")&&! ((Integer.parseInt(estado)>=50) || (Integer.parseInt(estado)<=80) ) ) {
      %>
      <img alt="modificar" src="<%= BASEURL%>/images/botones/modificarDisable.gif" name="imgmodificar" id="imgmodificar">

      <%
                }
                if(!clvsrv.ispermitted(perfil,"12")) {
      %>
      <img alt="anular" src="<%= BASEURL%>/images/botones/anularDisable.gif" name="imganular" id="imganular">

      <%
                }
            }
            else if(estado.equals("090")){
                 //aca hay que verificar que el usuario tenga autorizacion para modificar la cotizacion en este punto
                if(clvsrv.ispermitted(perfil,"13")){
      %>
      <img alt="modificar" src="<%= BASEURL%>/images/botones/modificar.gif" name="imgmodificar" id="imgmodificar"  style="cursor:pointer " onClick="window.location.href='<%=BASEURL%>/jsp/opav/cotizacion/listadoitemcotizacion.jsp?operacion=modificar&idaccion=<%=idaccion%>','mywindow','status=no,scrollbars=no, width=450, height=190, resizable=yes';"  onMouseOver="botonOver(this);" onMouseOut="botonOut(this);">

      <%
                }
                if(clvsrv.ispermitted(perfil,"14") && estado.equals("040")){//20100217
      %>
      <img alt="anular" src="<%= BASEURL%>/images/botones/anular.gif" name="imganular" id="imganular"  style="cursor:pointer " onClick="enviarForm();"  onMouseOver="botonOver(this);" onMouseOut="botonOut(this);">

      <%
                }
                if(!clvsrv.ispermitted(perfil,"13")){
      %>
      <img alt="modificar" src="<%= BASEURL%>/images/botones/modificarDisable.gif" name="imgmodificar" id="imgmodificar">

      <%
                }
                if(!clvsrv.ispermitted(perfil,"14")){
      %>
      <img alt="anular" src="<%= BASEURL%>/images/botones/anularDisable.gif" name="imganular" id="imganular">

      <%
                }
            }
            else{
                //aca llega cuando la accion esta en un punto en el que ningun usuario esta autorizado para modificar la cotizacion
       %>
      <img alt="modificar" src="<%= BASEURL%>/images/botones/modificarDisable.gif" name="imgmodificar" id="imgmodificar">
      <img alt="anular" src="<%= BASEURL%>/images/botones/anularDisable.gif" name="imganular" id="imganular">
       <%
            }
        }
      %>
  </div>
  <div align="center" id="busqueda"></div>
</form>
<%
    String mens = (String) request.getAttribute("msg");
    if (mens != null && !mens.equals("")) {
%>
<script type="text/javascript">
    alert('<%=mens%>');
</script>
<%}%>

<%}%>
</div>
</body>
<iframe width="188" height="166" name="gToday:normal:agenda.js:gfPop:plugins.js" id="gToday:normal:agenda.js:gfPop:plugins.js" src="<%=BASEURL%>/js/Calendario/ipopeng.htm" scrolling="yes" frameborder="0" style="visibility:visible; z-index:999; position:absolute; left:-500px; top:0px;">
</iframe>
</html>
