<!--
- Autor : Ing. Rhonalf Martinez
- Date  : 25 de julio del 2009
- Copyrigth Notice : Fintravalores S.A.
-->
<%--
-@(#)
--Descripcion : P�gina JSP, que maneja el ingreso de ordenes de compra.
--%>
<%@page contentType="text/html"%>
<%@page session="true"%>
<%@page import="java.util.*"%>
<%@page import="com.tsp.opav.model.*"%>
<%@page import="com.tsp.opav.model.beans.*"%>
<%@page import="com.tsp.opav.model.services.*"%>
<%@page import="com.tsp.operation.model.beans.Usuario"%>
<%@page import="com.tsp.util.*"%>
<%@include file="/WEB-INF/InitModel.jsp"%>
<%@ taglib uri="http://jakarta.apache.org/taglibs/input-0.90" prefix="input" %>
<html>
<%try{%>
<head>
<title>Cotizacion AIU - Datos</title>
<link href="<%= BASEURL %>/css/estilostsp.css" rel="stylesheet" type="text/css">
    <script type='text/javascript' src="<%=BASEURL%>/js/Validacion.js"></script>
    <script type='text/javascript' src="<%= BASEURL %>/js/boton.js"></script>
    <script type='text/javascript' src="<%= BASEURL %>/js/general.js"></script>
    <script src='<%=BASEURL%>/js/tools.js' language='javascript'></script>
    <script src='<%=BASEURL%>/js/date-picker.js'></script>
    <script src="<%= BASEURL %>/js/transferencias.js"></script>
	<script type="text/javascript" src="<%=BASEURL%>/js/buscarproveedor.js"></script>
	<script type="text/javascript" src="<%=BASEURL%>/js/act_valores_aiu.js"></script>
	<script type="text/javascript">
		var nfil=0;
		function enviarForm(){
			document.forma.submit();
		}
	</script>

<link href="../../../css/estilostsp.css" rel="stylesheet" type="text/css">
</head>

<body onresize="redimensionar()" onload = "imgProveedor();redimensionar();">
<div id="capaSuperior" style="position:absolute; width:100%; height:100px; z-index:0; left: 0px; top: 0px;">
 <jsp:include page="/toptsp.jsp?encabezado=Cotizacion AIU - Datos"/>
</div>
<div id="capaCentral" style="position:absolute; width:100%; height:87%; z-index:0; left: 0px; top: 100px; overflow: scroll;">
<%      
	 String idaccion = request.getParameter("idaccion")!=null?(String)request.getParameter("idaccion"):"";
         Usuario usuario = (Usuario) session.getAttribute("Usuario");
	 CotizacionService cserv = new CotizacionService(usuario.getBd());
	 
	 double[] porcentajesx=cserv.getPorcentajesAIU(idaccion); //091029
	 
	 //if(cserv.hayValoresAiu(idaccion)){//091029
%>
<!--091029
<script type="text/javascript">
	alert("Ya se han colocado valores para esta accion");
	window.close();
</script>

<br>
<span style="background-color:#FF0000; color:#FFFFFF; border: 1px solid #000000; font-family:Verdana, Arial, Helvetica, sans-serif;  margin-left:10px; padding:2px; ">Ya hay valores asignados de AIU para esta accion</span>
091029-->
<br>
<%	   
  	   //}	 else {//091029
	   double[] valores = new double[3];
	   valores[0] = 0.0;
	   valores[1] = 0.0;
	   valores[2] = 0.0;
	   ArrayList alist = cserv.getValoresAccion(idaccion);
	   System.out.println("alist.size()"+alist.size());//091029
	   if(alist.size()>0){
 		   System.out.println("(String)alist.get(0)"+alist.get(0));//091029
		   valores=(double[])alist.get(0);//091029		   
		   /*valores[0] = alist.get(0)==null ? 0.0:Double.parseDouble((String)alist.get(0));//091029
		   valores[1] = alist.get(1)==null ? 0.0:Double.parseDouble((String)alist.get(1));//091029
		   valores[2] = alist.get(2)==null ? 0.0:Double.parseDouble((String)alist.get(2));//091029*/
	   }
	   else {
%>
<script type="text/javascript">
	alert("No tengo datos para el idaccion "+<%=idaccion%>);
	window.close();
</script>
<%
	  }
%>
<form id ="forma" name="forma" action="<%=CONTROLLEROPAV%>?estado=Cotizacion&accion=AIU" method="post">

  <table width="100%" border="2"align="center">
    <tr>
      <td>
	  <table width="100%" align="center" >
        <tr>
          <td width="610" colspan="6" align="left" class="subtitulo1">Cotizacion AIU- Datos</td>
          <td width="285" colspan="3" align="left" class="barratitulo"><img src="<%=BASEURL%>/images/titulo.gif"></td>
          </tr>
      </table>	  
	  <input name="idaccion" type="hidden" id="idaccion" value="<%=idaccion%>">
	  <br>
	  <table width="100%"  border="0">
        <tr class="fila">
          <td width="15%">Total Materiales </td>
          <td colspan="3"><input name="materiales" type="text" id="materiales" value="<%=valores[0]%>" READONLY></td>
          </tr>
        <tr class="fila">
          <td>Total Mano De Obra</td>
          <td colspan="3"><input name="mano" type="text" id="mano" value="<%=valores[1]%>" READONLY></td>
          </tr>
        <tr class="fila">
          <td>Total Otros </td>
          <td colspan="3"><input name="otros" type="text" id="otros" value="<%=valores[2]%>" READONLY></td>
          </tr>
        <tr class="fila">
          <td>Porcentaje de A </td>
          <td width="34%"><input name="porc_a" type="text" id="porc_a"  value="<%=porcentajesx[0]%>"></td><!--091029-->
          <td width="15%">Valor A </td>
          <td width="36%"><input name="val_a" type="text" id="val_a" READONLY>
            <input name="val_a2" type="hidden" id="val_a2"></td>
        </tr>
        <tr class="fila">
          <td>Porcentaje de I </td>
          <td><input name="porc_i" type="text" id="porc_i"  value="<%=porcentajesx[1]%>"></td><!--091029-->
          <td>Valor I </td>
          <td><input name="val_i" type="text" id="val_i" READONLY>
            <input name="val_i2" type="hidden" id="val_i2"></td>
        </tr>
        <tr class="fila">
          <td>Porcentaje de U </td>
          <td> <input name="porc_u" type="text" id="porc_u"  value="<%=porcentajesx[2]%>"> <!--091029-->
          <img src="<%= BASEURL%>/images/botones/iconos/lupa.gif" name="imgbuscar" height="15" id="imgbuscar" style="cursor:hand " onClick="calcVals();" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);"></td>
          <td>Valor U </td>
          <td><input name="val_u" type="text" id="val_u" READONLY>
            <input name="val_u2" type="hidden" id="val_u2"></td>
        </tr>
      </table>	
	    
	  <br>
	  <br>
	  <table width="100%" border="0" align="center" cellpadding="0" bordercolor="#000000">
        <tr class="fila">
          <td width="53%" align="center" class="barratitulo">Subtotal</td>
          <td width="47%" align="center" class="barratitulo"><input name="subt" type="text" id="subt">
            <input name="subt2" type="hidden" id="subt2"></td>
        </tr>
        <tr class="fila">
          <td align="center" class="barratitulo">Iva</td>
          <td align="center" class="barratitulo"><input name="iva" type="text" id="iva">
            <input name="iva2" type="hidden" id="iva2"></td>
        </tr>
        <tr class="fila">
          <td align="center" class="barratitulo">Total</td>
          <td align="center" class="barratitulo"><input name="total" type="text" id="total">
            <input name="total2" type="hidden" id="total2"></td>
        </tr>
      </table>
	  <br>
	  </td>
    </tr>
  </table>
  <%if (porcentajesx[2]!=0){	  //091029%>
		  <script type="text/javascript">calcVals();</script>
  <%}%>
  <div align="center">
    <br>
	  <img src="<%= BASEURL%>/images/botones/aceptar.gif"  name="imgok" id="imgok"  style="cursor:hand " onClick="calcVals();alert('datos calculados.');enviarForm();"  onMouseOver="botonOver(this);" onMouseOut="botonOut(this);"> <!--091029-->
      <img src="<%= BASEURL%>/images/botones/salir.gif"  name="imgsalir"  onMouseOver="botonOver(this);" onClick="parent.close();" onMouseOut="botonOut(this);"  style="cursor:hand "></div>
</form>
<%//}//091029%>
</div>
</body>
<iframe width="188" height="166" name="gToday:normal:agenda.js:gfPop:plugins.js" id="gToday:normal:agenda.js:gfPop:plugins.js" src="<%=BASEURL%>/js/Calendario/ipopeng.htm" scrolling="yes" frameborder="0" style="visibility:visible; z-index:999; position:absolute; left:-500px; top:0px;">
</iframe>
<%}catch(Exception e){
	System.out.println("errror en jsp:"+e.toString()+"__"+e.getMessage());
	e.printStackTrace();

}%>
</html>
<%
	String mens=(String)request.getAttribute("msg");
	if(mens!=null && !mens.equals("")){%>
	<script>
		alert('<%=mens%>');
	</script>
<%}%>