<!--
- Autor : Ing. Rhonalf Martinez
- Date  : 25 de julio del 2009
- Copyrigth Notice : Fintravalores S.A.
-->
<%--
-@(#)
--Descripcion : P�gina JSP, que maneja el ingreso de ordenes de compra.
--%>
<%@page contentType="text/html"%>
<%@page session="true"%>
<%@page import="java.util.*"%>
<%@page import="com.tsp.operation.model.*"%>
<%@page import="com.tsp.operation.model.beans.*"%>
<%@page import="com.tsp.operation.model.services.*"%>
<%@page import="com.tsp.util.*"%>
<%@include file="/WEB-INF/InitModel.jsp"%>
<%@ taglib uri="http://jakarta.apache.org/taglibs/input-0.90" prefix="input" %>
<html>
<head>
<title>Cotizaciones - Anular</title>
<link href="<%= BASEURL %>/css/estilostsp.css" rel="stylesheet" type="text/css">
    <script type='text/javascript' src="<%=BASEURL%>/js/Validacion.js"></script>
    <script type='text/javascript' src="<%= BASEURL %>/js/boton.js"></script>
    <script type='text/javascript' src="<%= BASEURL %>/js/general.js"></script>
    <script src='<%=BASEURL%>/js/tools.js' language='javascript'></script>
    <script src='<%=BASEURL%>/js/date-picker.js'></script>
    <script src="<%= BASEURL %>/js/transferencias.js"></script>
	<script type="text/javascript" src="<%=BASEURL%>/js/buscarproveedor.js"></script>
	<script type="text/javascript">
		function enviarForm(){
			document.forma.submit();
		}
	</script>

<link href="../../../css/estilostsp.css" rel="stylesheet" type="text/css">
</head>

<body onresize="redimensionar()" onload = "imgProveedor();redimensionar();">
<div id="capaSuperior" style="position:absolute; width:100%; height:100px; z-index:0; left: 0px; top: 0px;">
 <jsp:include page="/toptsp.jsp?encabezado=Cotizaciones - Anular"/>
</div>
<div id="capaCentral" style="position:absolute; width:100%; height:87%; z-index:0; left: 0px; top: 100px; overflow: scroll;">


<%      
	   CotizacionService cserv = new CotizacionService();
	   //ProductosService prserv = new ProductosService();
%>
<form id ="forma" name="forma" action="<%= CONTROLLER%>?estado=Cotizacion&accion=Anular" method="post">

  <table width="650" border="2"align="center">
    <tr>
      <td>
	  <table width="100%" align="center" >
        <tr>
          <td colspan="3" align="left" class="subtitulo1">Cotizaciones  - Datos</td>
          <td colspan="2" align="left" class="barratitulo"><span class="barratitulo"><img src="<%=BASEURL%>/images/titulo.gif"></span></td>
          </tr>
        <tr>
          <td width="20" align="left" class="fila" >&nbsp;</td>
          <td width="80" align="left" class="fila" >Consecutivo</td>
          <td colspan="2" valign="middle" class="fila" >Id accion </td>
		  <td width="80" align="left" class="fila" >Fecha</td>
          </tr>
		 <%
		 	ArrayList ver = (ArrayList)cserv.verTodas();
			int tam = ver.size();
			Cotizacion compra = null;
			String cons = "";
			if(tam>0) {
		%>
		<input id="filas" name="filas" type="hidden" value="<%=tam%>">
		<%
				for(int i=0;i<tam;i++){
					compra=(Cotizacion)ver.get(i);
					cons = compra.getCodigo();
					
		 %>
        <tr style="cursor:hand">
          <td align="left" valign="top" class="letra">
		  <input name="ordenes" type="checkbox" value="<%=cons%>">
		  </td>
		  <a href="<%=CONTROLLER%>?estado=Cotizacion&accion=Ver&consecutivo=<%=cons%>" target="_blank">
          <td align="left" valign="top" class="letra"><%=compra.getCodigo()%></td>
          <td colspan="2" valign="middle" class="letra"><%=compra.getAccion()%></td>
          <td align="left" class="letra" ><%=compra.getFecha().substring(0,10)%></td>
		  </a>
          </tr>
		<%
				}
			}
			else {
				request.setAttribute("msg","No hay datos que mostrar...");
			}
		%>
      </table>	  </td>
    </tr>
  </table>
  <div align="center">
    <br>
      <img src="<%= BASEURL%>/images/botones/aceptar.gif"  name="imgaceptar" onClick="enviarForm();" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" style="cursor:hand "> <img src="<%= BASEURL%>/images/botones/salir.gif"  name="imgsalir"  onMouseOver="botonOver(this);" onClick="parent.close();" onMouseOut="botonOut(this);"  style="cursor:hand "></div>
</form>
</div>
</body>
<iframe width="188" height="166" name="gToday:normal:agenda.js:gfPop:plugins.js" id="gToday:normal:agenda.js:gfPop:plugins.js" src="<%=BASEURL%>/js/Calendario/ipopeng.htm" scrolling="yes" frameborder="0" style="visibility:visible; z-index:999; position:absolute; left:-500px; top:0px;">
</iframe>
</html>
<%
	String mens=(String)request.getAttribute("msg");
	if(!mens.equals("")){%>
	<script>
		alert('<%=mens%>');
	</script>
<%}%>