<%@taglib prefix="sql" uri="http://java.sun.com/jsp/jstl/sql"%>
<%@page contentType="text/html"%>
<%@page session="true"%>
<%@page import="java.util.*"%>
<%@page import="com.tsp.opav.model.*"%>
<%@page import="com.tsp.opav.model.beans.*"%>
<%@page import="com.tsp.opav.model.services.*"%>
<%@page import="com.tsp.util.*"%>
<%@ taglib uri="http://jakarta.apache.org/taglibs/input-0.90" prefix="input" %>
<%@include file="/WEB-INF/InitModel.jsp"%>
<%@page contentType="text/html" pageEncoding="ISO-8859-1"%>







<%
            //String estado = request.getParameter("est") == null ? "" : request.getParameter("est");
            String estado = (String) session.getAttribute("estado");;
            String idcat = request.getParameter("idcat") == null ? "" : request.getParameter("idcat");
            String contratista = request.getParameter("contratista") == null ? "" : request.getParameter("contratista");
            int op = (request.getParameter("op") != null) ? Integer.parseInt(request.getParameter("op").toString()) : -1;
            switch (op) {
                case 0:
                    ArrayList listadoCategorias = (ArrayList) session.getAttribute("listadoCategorias");
%>

<table width="948" border="1" align="center" style="border-collapse:collapse">
    <tr>
        <td><table width="100%" border="0" cellpadding="0" cellspacing="0">
                <tr>
                    <td width="46%"  align="left" class="subtitulo1">Listado Categorias: </td>
                    <td  width="54%"align="left" class="barratitulo"><img alt="titulo" src="<%=BASEURL%>/images/titulo.gif"></td>
                </tr>
            </table></td>
    </tr>
    <tr>
        <td><table width="948" border="0" align="center" style="border-collapse:collapse">
                <tr>
                    <td width="65" class="subtitulo1"><div align="center">id</div></td>
                    <td width="720" class="subtitulo1"><div align="left">Categoria</div></td>
                    <td width="81" class="subtitulo1"><div align="center">Estado</div></td>

                </tr>
            </table></td>
    </tr>
    <tr>
        <td><table width="948" border="1" style="border-collapse:collapse">
                <%for (int i = 0; i < listadoCategorias.size(); i++) {%>
                <tr id="fila<%=i%>"<%= (i == 999990) ? " class='fila' align='center' style='color: white;background-color:green;'" : " class='fila' align='center' style='cursor: pointer'  onmouseover=\"color_fila('fila" + i + "','orange');\" onmouseout=\"color_fila('fila" + i + "','#EAFFEA');\""%>  >
                    <td width="68" ><div align="center"> <img src="<%=BASEURL%>/images/botones/iconos/expand.gif" width="9" height="9" id="ima<%=i%>" onClick="obtenerSubcategorias('<%=CONTROLLEROPAV%>', '<%=BASEURL%>', '<%=i%>','<%=((String[]) listadoCategorias.get(i))[0]%>');" style="cursor:hand; "><%=i%></div></td>
                    <td width="777" ><div align="left">
                            <input name="idcat<%=i%>" id="idcat<%=i%>" type="text"  value="<%=((String[]) listadoCategorias.get(i))[1]%>" size="30" maxlength="230" <% if (estado.equals("consulta")) {%>  readonly  <% }%>     />
                            <input name="codcat<%=i%>" id="codcat<%=i%>" type="hidden"  value="<%=((String[]) listadoCategorias.get(i))[0]%>">
                        </div></td>
                    <td width="81"><label>
                            <%
                                String reg_status = ((String[]) listadoCategorias.get(i))[2];
                            %>
                            <select name="status<%=i%>"  id="status<%=i%>" onchange="activar();" <% if (estado.equals("consulta")) {%>  disabled  <% }%>  >
                                <option value="" <% if (reg_status.equals("")) {%>  selected  <% }%> >Activo</option>
                                <option value="A"   <% if (reg_status.equals("A")) {%>  selected  <% }%>>Inactivo</option>
                            </select>
                        </label></td>

                </tr>
                <tr>
                    <td colspan="4" align="right"  class="fila"><div id="subcategorias<%=i%>" style="display:none; padding:0px 0px 0px 0px;" ></div></td>
                </tr>
                <%}%>
            </table></td>
    </tr>
</table>
<div align="center">
    <p>
        <% if (estado.equals("modificar")) {%>    
        <img src="<%= BASEURL%>/images/botones/modificar.gif" name="imgaceptar" alt="Modificar" onClick="modificarCategoria('<%=CONTROLLEROPAV%>', '<%=BASEURL%>');" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" style="cursor:pointer ">
        <% }%>
        <% if (estado.equals("agregar")) {%>
        <img src="<%= BASEURL%>/images/botones/guardar.gif" name="imgguardar" alt="Guardar" onClick="guardarNuevo('<%=CONTROLLEROPAV%>', '<%=BASEURL%>');" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" style="cursor:pointer ">
        <% }%>
        <a href="<%=BASEURL%>/jsp/delectricaribe/cotizacion/listadocategoria.jsp"><img src="<%= BASEURL%>/images/botones/regresar.gif" alt="Aceptar" name="imgaceptar" border="0" style="cursor:pointer "  onMouseOver="botonOver(this);" onMouseOut="botonOut(this);"></a>
        <img src="<%= BASEURL%>/images/botones/salir.gif" name="imgsalir" alt="Salir" onMouseOver="botonOver(this);" onClick="parent.close();" onMouseOut="botonOut(this);" style="cursor:pointer ">    </p>
</div>  
<% break;
                case 1:
                    ArrayList listadoSubcategorias = (ArrayList) session.getAttribute("subcategorias");
                    int index = Integer.parseInt(request.getParameter("index").toString());
                    String estadoCat = request.getParameter("estadoCategoria");
%>


<table width="100%" border="1" align="center" style="border-collapse:collapse" id="tabla_subcategorias">

    <tr>
        <td width="119"   style="border:none" >&nbsp;</td>
        <td width="106" class="subtitulo1"><div align="center">id</div></td>
        <td width="1172" class="subtitulo1">SubCategoria</td>
        <td width="92" class="subtitulo1"><div align="center">Estado</div></td>
    <input name="tam_sub" id="tam_sub" type="hidden" value="<%=listadoSubcategorias.size()%>"><!-- Valor Inicial Indice-->
    </tr>

    <% if (listadoSubcategorias.size() > 0) {%>

    <%for (int i = 0; i < listadoSubcategorias.size(); i++) {%>
    <tr id="fila2<%=i%>"<%= (i == 999990) ? " class='fila' align='center' style='color: white;background-color:green;'" : " class='fila' align='center' style='cursor: pointer'  onmouseover=\"color_fila('fila2" + i + "','orange');\" onmouseout=\"color_fila('fila2" + i + "','#EAFFEA');\""%>  >
        <td width="119"  style="border:none">&nbsp;</td>
        <td width="106" ><div align="center"> <img src="<%=BASEURL%>/images/botones/iconos/expand.gif" width="9" height="9" id="ima<%=index + "_" + i%>" onClick="obtenertipoSubcategorias('<%=CONTROLLEROPAV%>', '<%=BASEURL%>', '<%=index%>','<%=i%>' ,'<%=((String[]) listadoSubcategorias.get(i))[0]%>');" style="cursor:hand; "><%=i%>


<% if (!estado.equals("consulta")) {%>
                <img id="imgmas<%=i%>" name="imgmas<%=i%>" src="<%=BASEURL%>/images/botones/iconos/mas.gif" alt="Agregar fila" onClick="agregarSubcategoria('<%=CONTROLLEROPAV%>', '<%=BASEURL%>', '<%=index%>','<%=((String[]) listadoSubcategorias.get(i))[0]%>');" width="12" height="12">
                <img id="imgini<%=i%>" name="imgini<%=i%>" src="<%=BASEURL%>/images/botones/iconos/menos1.gif" alt="Eliminar fila" onClick="eliminarSubcategoria('<%=CONTROLLEROPAV%>', '<%=BASEURL%>', '<%=index%>','<%=((String[]) listadoSubcategorias.get(i))[0]%>', '<%=i%>');"  width="12" height="12">
<% }%>
            </div></td>
        <td width="1172" ><div align="left">
                <input name="idcat<%=index + "_" + i%>"   id="idcat<%=index + "_" + i%>" type="text"  value="<%=((String[]) listadoSubcategorias.get(i))[1]%>" size="30" maxlength="230" <% if (estado.equals("consulta")) {%>  readonly  <% }%> />
                <input name="codcatsub<%=index + "_" + i%>"   id="codcatsub<%=index + "_" + i%>" type="hidden"  value="<%=((String[]) listadoSubcategorias.get(i))[0]%>"   />
            </div></td>
            <%
                String reg_status = "";
                if (estadoCat.equals("A")) {
                    reg_status = "A";
                } else {
                    reg_status = ((String[]) listadoSubcategorias.get(i))[2];
                }

            %>
        <td width="92" >
            <select name="status<%=index + "_" + i%>" id="status<%=index + "_" + i%>"  onchange="activarSubcategorias('<%=index%>', '<%=i%>')" <% if (estado.equals("consulta")) {%>  disabled  <% }%>>
                <option value="" <% if (reg_status.equals("")) {%>  selected  <% }%> >Activo</option>
                <option value="A"   <% if (reg_status.equals("A")) {%>  selected  <% }%>>Inactivo</option>
            </select></td>

    </tr>
    <tr>
    <input name="indiceDelete<%=index + "_" + i%>" id="indiceDelete<%=index + "_" + i%>" type="hidden" value="-1"><!-- Valor Inicial Indice-->
    <input name="Maximo" id="Maximo" type="hidden" value=""><!-- Valor Inicial Indice-->
    <td colspan="4"  class="fila"><div id="tiposubcategorias<%=index + "_" + i%>" style="display:none; padding:0px 0px 0px 0px;" ></div></td>
</tr>
<script type="text/javascript">
    //alert("_________________");
    document.getElementById("Maximo").value = n;
</script>
<script type="text/javascript">
    n++;
</script>
<%}%>
<%} else {
     int i = 0;
%>

<tr id="fila2<%=i%>"<%= (i == 999990) ? " class='fila' align='center' style='color: white;background-color:green;'" : " class='fila' align='center' style='cursor: pointer'  onmouseover=\"color_fila('fila2" + i + "','orange');\" onmouseout=\"color_fila('fila2" + i + "','#EAFFEA');\""%>  >
    <td width="119"  style="border:none">&nbsp;</td>
    <td width="106" ><div align="center"> <img src="<%=BASEURL%>/images/botones/iconos/expand.gif" width="9" height="9" id="ima<%=index + "_" + i%>" onClick="obtenertipoSubcategorias('<%=CONTROLLEROPAV%>', '<%=BASEURL%>', '<%=index%>','<%=i%>' ,'');" style="cursor:hand; ">
<% if (!estado.equals("consulta")) {%>
            <img id="imgmas<%=i%>" name="imgmas<%=i%>" src="<%=BASEURL%>/images/botones/iconos/mas.gif" alt="Agregar fila" onClick="agregarCelda()" width="12" height="12">
            <img id="imgini<%=i%>" name="imgini<%=i%>" src="<%=BASEURL%>/images/botones/iconos/menos1.gif" alt="Eliminar fila" onClick="borrarDatosListaNew(<%=i%>);"  width="12" height="12">
<% }%>
        </div></td>
    <td width="1172" ><div align="left">
            <input name="idcat<%=index + "_" + i%>"  id="idcat<%=index + "_" + i%>" type="text"  value="" size="30" maxlength="230" <% if (estado.equals("consulta")) {%>  readonly  <% }%> />
        </div></td>
    <td width="92" >
        <%
             String reg_status = "";
             if (estadoCat.equals("A")) {
                 reg_status = "A";
             }
        %>

        <select name="status<%=index + "_" + i%>" id="status<%=index + "_" + i%>" <% if (estado.equals("consulta")) {%>  disabled  <% }%>>
            <option value="" <% if (reg_status.equals("")) {%>  selected  <% }%> >Activo</option>
            <option value="A"   <% if (reg_status.equals("A")) {%>  selected  <% }%>>Inactivo</option>
        </select></td>

</tr>
<tr> 
<input name="Maximo" id="Maximo" type="hidden" value=""><!-- Valor Inicial Indice-->
<td colspan="4"  class="fila"><div id="tiposubcategorias<%=index + "_" + i%>" style="display:none; padding:0px 0px 0px 0px;" ></div></td>
</tr>
<script type="text/javascript">
    document.getElementById("Maximo").value = n;
</script>
<script type="text/javascript">
    n++;
</script>

<%}%>

</table>



<% break;
                case 2:

                    index = Integer.parseInt(request.getParameter("index").toString());
                    int ind = Integer.parseInt(request.getParameter("ind").toString());

                    String indicetabla = ind + "";
                    ArrayList listadotipoSubcategorias = (ArrayList) session.getAttribute("tiposubcategorias" + index + "_" + ind);

                    String estadosubCategoria = request.getParameter("estadosubCategoria");



%>

<table width="100%" border="1" align="center" style="border-collapse:collapse" id="tabla_tiposubcategorias<%=ind%>">
    <tr>
        <td width="112"  style="border:none">&nbsp;</td>
        <td width="138"  style="border:none">&nbsp;</td>
        <td width="76" class="subtitulo1"><div align="center">id</div></td>
        <td width="1181" class="subtitulo1">TiposubCategoria</td>
        <td width="90" class="subtitulo1"><div align="center">Estado</div></td>
    <input name="tam_tiposub<%=index + "_" + ind%>" id="tam_tiposub<%=index + "_" + ind%>" type="hidden" value="<%=listadotipoSubcategorias.size()%>"><!-- Valor Inicial Indice-->

    </tr>
    <% if (listadotipoSubcategorias.size() > 0) {%>

    <%for (int i = 0; i < listadotipoSubcategorias.size(); i++) {%>
    <tr id="fila3<%=ind + "_" + i%>"<%= (i == 999990) ? " class='fila' align='center' style='color: white;background-color:green;'" : " class='fila' align='center' style='cursor: pointer'  onmouseover=\"color_fila('fila3" + ind + "_" + i + "','orange');\" onmouseout=\"color_fila('fila3" + ind + "_" + i + "','#EAFFEA');\""%>  >
        <td width="112"  style="border:none">&nbsp;</td>
        <td width="138"  style="border:none">&nbsp;</td>
        <td width="76" ><div align="center"><%=i%>
<% if (!estado.equals("consulta")) {%>
                <img id="imgmas<%=ind + "_" + i%>" name="imgmas<%=ind + "_" + i%>" src="<%=BASEURL%>/images/botones/iconos/mas.gif" alt="Agregar fila" onClick="agregarFila('<%=CONTROLLEROPAV%>', '<%=BASEURL%>', '<%=index%>','<%=ind%>' ,'<%=((String[]) listadotipoSubcategorias.get(i))[0]%>');" width="12" height="12">
                <img id="imgini<%=ind + "_" + i%>" name="imgini<%=ind + "_" + i%>" src="<%=BASEURL%>/images/botones/iconos/menos1.gif" alt="Eliminar fila" onClick="eliminarFila('<%=CONTROLLEROPAV%>', '<%=BASEURL%>', '<%=index%>','<%=ind%>' ,'<%=((String[]) listadotipoSubcategorias.get(i))[0]%>', '<%=i%>');"  width="12" height="12">
<% }%>
            </div></td>
        <td width="1181" ><div align="left">
           <input name="idcatts<%=ind + "_" + i%>" id="idcatts<%=ind + "_" + i%>" type="text"  value="<%=((String[]) listadotipoSubcategorias.get(i))[1]%>" size="30" maxlength="230" <% if (estado.equals("consulta")) {%>  readonly  <% }%> />
           <input name="codcatts<%=ind + "_" + i%>"   id="codcatts<%=ind + "_" + i%>" type="hidden"  value="<%=((String[]) listadotipoSubcategorias.get(i))[0]%>"   />
            </div></td>
        <td width="90" >
            <%
                String reg_status = "";
                if (estadosubCategoria.equals("A")) {
                    reg_status = "A";
                } else {
                    reg_status = ((String[]) listadotipoSubcategorias.get(i))[2];
                }

            %>
            <select name="statust<%=ind + "_" + i%>"  id="statust<%=ind + "_" + i%>" <% if ((reg_status.equals("A"))||(estado.equals("consulta"))) {%>   disabled  <% }%>  >
                <option value="" <% if (reg_status.equals("")) {%>  selected  <% }%> >Activo</option>
                <option value="A"   <% if (reg_status.equals("A")) {%>  selected  <% }%>>Inactivo</option>
            </select>
        </td>

    </tr>
    <tr>
    <input name="Maximotipo<%=ind%>" id="Maximotipo<%=ind%>" type="hidden" value="<%=listadotipoSubcategorias.size()%>"><!-- Valor Inicial Indice-->
    </tr>

    <script type="text/javascript">
        m++;
    </script>
    <%}%>
    <%} else {

         int i = 0;
    %>

    <tr id="fila3<%=ind + "_" + i%>"<%= (i == 999990) ? " class='fila' align='center' style='color: white;background-color:green;'" : " class='fila' align='center' style='cursor: pointer'  onmouseover=\"color_fila('fila3" + ind + "_" + i + "','orange');\" onmouseout=\"color_fila('fila3" + ind + "_" + i + "','#EAFFEA');\""%>  >
        <td width="112"  style="border:none">&nbsp;</td>
        <td width="138"  style="border:none">&nbsp;</td>
        <td width="76" ><div align="center">
<% if (!estado.equals("consulta")) {%>
                <img id="imgmas<%=ind + "_" + i%>" name="imgmas<%=ind + "_" + i%>" src="<%=BASEURL%>/images/botones/iconos/mas.gif" alt="Agregar fila" onClick="agregarFila('<%=CONTROLLEROPAV%>', '<%=BASEURL%>', '<%=index%>','<%=ind%>' ,'0');" width="12" height="12">
                <img id="imgini<%=ind + "_" + i%>" name="imgini<%=ind + "_" + i%>" src="<%=BASEURL%>/images/botones/iconos/menos1.gif" alt="Eliminar fila" onClick="eliminarFila('<%=CONTROLLEROPAV%>', '<%=BASEURL%>', '<%=index%>','<%=ind%>' ,'0', '<%=i%>');"  width="12" height="12">
<% }%>
            </div></td>
        <td width="1181" ><div align="left">
                <input name="idcatts<%=ind + "_" + i%>"   id="idcatts<%=ind + "_" + i%>" type="text"  value="" size="30" maxlength="230" <% if (estado.equals("consulta")) {%>  readonly  <% }%> />
            </div></td>
        <td width="90" >
            <%
                 String reg_status = "";
                 if (estadosubCategoria.equals("A")) {
                     reg_status = "A";
                 }
            %>
            <select name="statust<%=ind + "_" + i%>" id="statust<%=ind + "_" + i%>" <% if (estado.equals("consulta")) {%>  disabled  <% }%>>
                <option value="" <% if (reg_status.equals("")) {%>  selected  <% }%> >Activo</option>
                <option value="A"   <% if (reg_status.equals("A")) {%>  selected  <% }%>>Inactivo</option>
            </select></td>

    </tr>
    <tr>
    <input name="Maximotipo<%=ind%>" id="Maximotipo<%=ind%>" type="hidden" value="0"><!-- Valor Inicial Indice-->
    </tr>

    <script type="text/javascript">
        m++;
    </script>


    <%}%>
</table>

<%
                    break;
            }
%>