	<%@page contentType="text/html"%>
<%@page session="true"%>
<%@page import="java.util.*"%>
<%@page import="com.tsp.opav.model.*"%>
<%@page import="com.tsp.opav.model.beans.*"%>
<%@page import="com.tsp.opav.model.services.*"%>
<%@page import="com.tsp.util.*"%>
<%@ taglib uri="http://jakarta.apache.org/taglibs/input-0.90" prefix="input" %>
<%@include file="/WEB-INF/InitModel.jsp"%>
<script src="<%=BASEURL%>/js/prototype.js"        type="text/javascript"></script>
<script src="<%=BASEURL%>/js/categoria.js"        type="text/javascript"></script>
<link href="<%= BASEURL%>/css/estilostsp.css" rel="stylesheet" type="text/css">
<link href="<%=BASEURL%>/css/default.css"       rel="stylesheet" type="text/css">
<link href="<%=BASEURL%>/css/alert.css" rel="stylesheet" type="text/css"/>
<link href="<%=BASEURL%>/css/alphacube.css" rel="stylesheet" type="text/css"/>
<script type="text/javascript" src="<%=BASEURL%>/js/buscarproveedor.js"></script>
<script type='text/javascript' src="<%= BASEURL%>/js/boton.js"></script>
<script type='text/javascript' src="<%=BASEURL%>/js/Validacion.js"></script>

<%@page contentType="text/html"  pageEncoding="UTF-8"%>
<link href="<%=BASEURL%>/css/mac_os_x.css"      rel="stylesheet" type="text/css">


<script type='text/javascript' src="<%= BASEURL%>/js/general.js"></script>
<script src='<%=BASEURL%>/js/tools.js' language='javascript'></script>
<script src='<%=BASEURL%>/js/date-picker.js'></script>
<script src="<%= BASEURL%>/js/transferencias.js"></script>

<script type='text/javascript' src="<%= BASEURL%>/js/prototype.js"></script>
<script type='text/javascript' src="<%= BASEURL%>/js/scriptaculous/scriptaculous.js"></script>
<script type='text/javascript' src="<%= BASEURL%>/js/autosuggest.js"></script>
<script type='text/javascript' src="<%= BASEURL%>/js/scriptaculous/controls.js"></script>
<script type='text/javascript' src="<%= BASEURL%>/js/scriptaculous/builder.js"></script>
<script type="text/javascript" src="<%=BASEURL%>/js/effects.js"        ></script>
<script type="text/javascript"src="<%=BASEURL%>/js/window.js"        ></script>
<script type="text/javascript"src="<%=BASEURL%>/js/window_effects.js" ></script>
<link href="<%=BASEURL%>/css/default.css"       rel="stylesheet" type="text/css">
<link href="<%=BASEURL%>/css/alert.css" rel="stylesheet" type="text/css"/>
<link href="<%=BASEURL%>/css/alphacube.css" rel="stylesheet" type="text/css"/>


<%--
    Document   : listadomaterialaux
    Created on : 2/08/2010, 11:22:39 AM
    Author     : Jose Castro
--%>
<%
            int op = (request.getParameter("op") != null) ? Integer.parseInt(request.getParameter("op").toString()) : -1;
            switch (op) {
                case 0:

%>
                <table width="100%" border="2"align="center">
                    <tr>
                        <td>
                            <table width="100%" align="center" >
                                <tr>
                                    <td colspan="7" align="left" class="subtitulo1">&nbsp;Ver datos </td>
                                    <td colspan="2" align="left" class="barratitulo"><img src="<%=BASEURL%>/images/titulo.gif"></td>
                                </tr>
                                <tr>
                                    <td width="176" align="left" class="fila" >Consecutivo</td>
                                    <td colspan="2" valign="middle" class="fila" >Descripcion del producto</td>
                                    <td width="99" align="left" class="fila" >Precio</td>
                                    <td width="100" align="left" class="fila" >Tipo</td>
                                    <td width="190" align="left" class="fila" >Categoria</td>
									<td width="121" align="left" class="fila" >Unidad Empaque</td>
                                                                        <td width="121" align="left" class="fila" >Ente Certificador</td>
									<td width="50"  align="center"  class="fila">Estado</td>
                                </tr>
                                <%
                            ArrayList ver = (ArrayList) session.getAttribute("resultado");
                            int tam = ver.size();
                            Material spl = null;
                            for (int i = 0; i < tam; i++) {
                                spl = (Material) ver.get(i);
                                if (spl != null) {
                                %>
                                <tr>
                                    <td align="left" valign="top" class="letra">
                                        <%if (spl.getRegStatus()!=null && !(spl.getRegStatus().equals("A"))){%><!--091031-->
                                                <a target="_blank" href="<%=CONTROLLEROPAV%>?estado=Materiales&accion=Crear&opcion=2&opcion_pagina=modificar&idmat=<%=spl.getCodigo()%>">
                                                        <%=spl.getCodigo()%>											</a>
                                        <%}else{ %>
<a target="_blank" href="<%=CONTROLLEROPAV%>?estado=Materiales&accion=Crear&opcion=2&opcion_pagina=consultar&idmat=<%=spl.getCodigo()%>">
                                                        <%=spl.getCodigo()%>											</a>
<%
                                        }
                                        %>
                                    </td>
                                    <td colspan="2" valign="middle" class="letra"><%=spl.getDescripcion()%></td>
                                    <td align="left" class="letra" >$ <%= Util.customFormat(spl.getValor())%></td>
                                    <td align="left" class="letra" >
                                    <%
                                        if(spl.getTipo().equals("M")) out.print("Material");
                                        if(spl.getTipo().equals("D")) out.print("Mano de obra");
                                        if(spl.getTipo().equals("O")) out.print("Otros");
                                    %>
                                    </td>
                                    <td align="left" class="letra" ><%=spl.getDesc_categoria()%></td>
                                    <td align="left" class="letra" ><%=spl.getUnidad_empaque()%></td>
                                    <td align="center" class="letra"><%=spl.getEnte_certificador()%></td>
                                    <td align="center" class="letra"><%=spl.getRegStatus()%></td>
                                </tr>
                                <%
                                 }
                             }
                                %>
                            </table>	  </td>
                    </tr>
                </table>
                <div align="center"><br>
<img src="<%= BASEURL%>/images/botones/exportarExcel.gif"  name="imgsalir"  onMouseOver="botonOver(this);" onClick="generarExcel('<%=CONTROLLEROPAV%>', '<%=BASEURL%>');" onMouseOut="botonOut(this);"  style="cursor:hand ">
                    <img src="<%= BASEURL%>/images/botones/salir.gif"  name="imgsalir"  onMouseOver="botonOver(this);" onClick="parent.close();" onMouseOut="botonOut(this);"  style="cursor:hand ">
                                     </div>




                      <%
                    break;
            }
%>

                