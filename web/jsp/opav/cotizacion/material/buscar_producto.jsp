<!--
- Autor : Ing. Rhonalf Martinez
- Date  : 25 de julio del 2009
- Copyrigth Notice : Fintravalores S.A.
-->
<%--
-@(#)
--Descripcion : P�gina JSP, que maneja el ingreso de ordenes de compra.
--%>
<%@page contentType="text/html"%>
<%@page session="true"%>
<%@page import="java.util.*"%>
<%@page import="com.tsp.operation.model.*"%>
<%@page import="com.tsp.operation.model.beans.*"%>
<%@page import="com.tsp.operation.model.services.*"%>
<%@page import="com.tsp.util.*"%>
<%@include file="/WEB-INF/InitModel.jsp"%>
<%@ taglib uri="http://jakarta.apache.org/taglibs/input-0.90" prefix="input" %>
<html>
<head>
<title>Productos - Buscar</title>
<link href="<%= BASEURL %>/css/estilostsp.css" rel="stylesheet" type="text/css">
    <script type='text/javascript' src="<%=BASEURL%>/js/Validacion.js"></script>
    <script type='text/javascript' src="<%= BASEURL %>/js/boton.js"></script>
    <script type='text/javascript' src="<%= BASEURL %>/js/general.js"></script>
    <script src='<%=BASEURL%>/js/tools.js' language='javascript'></script>
    <script src='<%=BASEURL%>/js/date-picker.js'></script>
    <script src="<%= BASEURL %>/js/transferencias.js"></script>
	<script type="text/javascript" src="<%=BASEURL%>/js/buscarproveedor.js"></script>
	<script type="text/javascript">
		function enviarForm(){
			document.forma.submit();
		}
	</script>

<link href="../../../css/estilostsp.css" rel="stylesheet" type="text/css">
</head>

<body onresize="redimensionar()" onload = "imgProveedor();redimensionar();">
<div id="capaSuperior" style="position:absolute; width:100%; height:100px; z-index:0; left: 0px; top: 0px;">
 <jsp:include page="/toptsp.jsp?encabezado=Buscar productos"/>
</div>
<div id="capaCentral" style="position:absolute; width:100%; height:87%; z-index:0; left: 0px; top: 100px; overflow: scroll;">


<%
	   ProductosService prod = new ProductosService();
%>
<form id ="forma" name="forma" action="<%= CONTROLLER%>?estado=Productos&accion=Buscar" method="post">

  <table width="785" border="2"align="center">
    <tr>
      <td>
	  <table width="100%" align="center" >
        <tr>
          <td colspan="2" align="left" class="subtitulo1">&nbsp;Buscar datos </td>
          <td width="210" align="left" class="barratitulo"><img src="<%=BASEURL%>/images/titulo.gif"></td>
        </tr>
        <tr>
          <td width="311" align="left" class="fila" >Seleccione filtro y parametro de busqueda </td>
          <td width="236" valign="middle" class="letra" ><select name="filtro" id="filtro">
            <option value="-1" selected>Seleccione...</option>
            <option value="1">Codigo del producto</option>
            <option value="2">Descripcion</option>
            <option value="3">Precio</option>
            <option value="4">Categoria</option>
          </select></td>
		  <td valign="middle" class="letra" ><input name="parametro" type="text" id="parametro" size="30"></td>
          </tr>
      </table>	  </td>
    </tr>
  </table>
  <div align="center">
    <br>
    <img src="<%= BASEURL%>/images/botones/aceptar.gif" name="imgaceptar" onClick="enviarForm();" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" style="cursor:pointer ">&nbsp;
    <img src="<%= BASEURL%>/images/botones/salir.gif" name="imgsalir" onMouseOver="botonOver(this);" onClick="parent.close();" onMouseOut="botonOut(this);" style="cursor:pointer ">
  </div>
</form>
</div>
</body>
<iframe width="188" height="166" name="gToday:normal:agenda.js:gfPop:plugins.js" id="gToday:normal:agenda.js:gfPop:plugins.js" src="<%=BASEURL%>/js/Calendario/ipopeng.htm" scrolling="yes" frameborder="0" style="visibility:visible; z-index:999; position:absolute; left:-500px; top:0px;">
</iframe>
</html>
<%
	String mens=(String)request.getAttribute ("msg");
	if(!mens.equals("")&& mens!=null){%>
	<script>
		alert('<%=mens%>');
	</script>
<%}%>