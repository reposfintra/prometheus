<%@page contentType="text/html"%>
<%@page session="true"%>
<%@page import="java.util.*"%>
<%@page import="com.tsp.opav.model.*"%>
<%@page import="com.tsp.opav.model.beans.*"%>
<%@page import="com.tsp.opav.model.services.*"%>
<%@page import="com.tsp.util.*"%>
<%@ taglib uri="http://jakarta.apache.org/taglibs/input-0.90" prefix="input" %>
<%@include file="/WEB-INF/InitModel.jsp"%>
<script src="<%=BASEURL%>/js/prototype.js"        type="text/javascript"></script>
<script src="<%=BASEURL%>/js/categoria.js"        type="text/javascript"></script>
<link href="<%= BASEURL%>/css/estilostsp.css" rel="stylesheet" type="text/css">
<link href="<%=BASEURL%>/css/default.css"       rel="stylesheet" type="text/css">
<link href="<%=BASEURL%>/css/alert.css" rel="stylesheet" type="text/css"/>
<link href="<%=BASEURL%>/css/alphacube.css" rel="stylesheet" type="text/css"/>
<script type="text/javascript" src="<%=BASEURL%>/js/buscarproveedor.js"></script>
<script type='text/javascript' src="<%= BASEURL%>/js/boton.js"></script>
<script type='text/javascript' src="<%=BASEURL%>/js/Validacion.js"></script>

<%@page contentType="text/html"  pageEncoding="UTF-8"%>
<link href="<%=BASEURL%>/css/mac_os_x.css"      rel="stylesheet" type="text/css">


<script type='text/javascript' src="<%= BASEURL%>/js/general.js"></script>
<script src='<%=BASEURL%>/js/tools.js' language='javascript'></script>
<script src='<%=BASEURL%>/js/date-picker.js'></script>
<script src="<%= BASEURL%>/js/transferencias.js"></script>

<script type='text/javascript' src="<%= BASEURL%>/js/prototype.js"></script>
<script type='text/javascript' src="<%= BASEURL%>/js/scriptaculous/scriptaculous.js"></script>
<script type='text/javascript' src="<%= BASEURL%>/js/autosuggest.js"></script>
<script type='text/javascript' src="<%= BASEURL%>/js/scriptaculous/controls.js"></script>
<script type='text/javascript' src="<%= BASEURL%>/js/scriptaculous/builder.js"></script>
<script type="text/javascript" src="<%=BASEURL%>/js/effects.js"        ></script>
<script type="text/javascript"src="<%=BASEURL%>/js/window.js"        ></script>
<script type="text/javascript"src="<%=BASEURL%>/js/window_effects.js" ></script>
<link href="<%=BASEURL%>/css/default.css"       rel="stylesheet" type="text/css">
<link href="<%=BASEURL%>/css/alert.css" rel="stylesheet" type="text/css"/>
<link href="<%=BASEURL%>/css/alphacube.css" rel="stylesheet" type="text/css"/>


<script>

                function formatNumber(num,prefix){
                prefix = prefix || '';
                num += '';
                var splitStr = num.split('.');
                var splitLeft = splitStr[0];
                var splitRight = splitStr.length > 1 ? '.' + splitStr[1] : '';
                splitRight = splitRight.substring(0,3);//090929
                var regx = /(\d+)(\d{3})/;
                while (regx.test(splitLeft)) {
                    splitLeft = splitLeft.replace(regx, '$1' + ',' + '$2');
                }
                return prefix + splitLeft + splitRight;
            }

</script>

<%
String contratista = request.getParameter("contratista") == null ? "" : request.getParameter("contratista");
String id_accion_inicial = request.getParameter("id_accion_inicial") == null ? "" : request.getParameter("id_accion_inicial");
String msj = request.getParameter("msj") == null ? "" : request.getParameter("msj");
%>

<script>
 function color_fila(id,color){

                    $(id).style.backgroundColor=color;
                }



</script>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=windows-1252" />
<title>Listado de Categorias de un Material</title>
</head>
	<body onLoad="buscarCategorias('<%=CONTROLLEROPAV%>', '<%=BASEURL%>');">
		<div id="capaSuperior" style="position:absolute; width:100%; height:100px; z-index:0; left: 0px; top: 0px;">
		<jsp:include page="/WEB-INF/toptsp.jsp?encabezado=Listado de Categorias de un Material"/>
		</div>
		<div id="formulario" style="position:absolute; width:100%; height:100%; z-index:0; left: 0px; top: 100px; ">


  <table width="785" border="2"align="center">
    <tr>
      <td>
	  <table width="100%" align="center" >
        <tr>
          <td colspan="2" align="left" class="subtitulo1">&nbsp;Buscar datos  </td>
          <td colspan="4" align="left" class="barratitulo"><img src="<%=BASEURL%>/images/titulo.gif"></td>
          </tr>
		<tr>
		  <td class="fila" >Categoria
		    </td>
		  <td colspan="6" class="letra">
          <select name="categoria"   id="categoria" onChange="buscarSubcategorias('<%=CONTROLLEROPAV%>', '<%=BASEURL%>');" style=" width: 450">
		    <option value="" selected="selected">Seleccione Categoria</option>
		    </select>
		    </td>
		  </tr>

                    <tr>
                    <td  align="left" class="fila" >Tipo      </td>
                    <td class="letra" colspan="3">
                    <select name="tipomat" id="tipomat" style="width: 138">
		    <option value="M">Material</option>
		    <option value="D">Mano de obra</option>
		    <option value="O">Otros</option>
		   </select>
                    </td>
                    </tr>
		<tr>
		  <td colspan="2" align="left" class="fila" >Filtro	 </td>
		  <td  valign="middle" class="letra" >
                    <select name="filtro" id="filtro">
		    <option value="-1" selected>Seleccione...</option>
		    <option value="1">Codigo del producto</option>
		    <option value="2">Descripcion</option>
		    <option value="3">Precio</option>
		    </select></td>
                   <td colspan="2" valign="middle" class="letra" ><input name="parametro" type="text" id="parametro" size="30" /><input name="pagina"  type="hidden" id="pagina" value="filtro" ></td>
		  </tr>
                    </td>
      </table>	  
 
  </table>
  <div align="center">
    <br>
    <img src="<%= BASEURL%>/images/botones/aceptar.gif" name="imgaceptar" onClick="ListadoMateriales('<%=CONTROLLEROPAV%>', '<%=BASEURL%>');" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" style="cursor:pointer ">&nbsp;
    <a href="<%=BASEURL%>/jsp/opav/cotizacion/material/crear_producto2.jsp?opcion_pagina=crear"><img src="<%= BASEURL%>/images/botones/nuevo.gif"  name="imgsalir" border="0"  style="cursor:hand "   onMouseOver="botonOver(this);" onMouseOut="botonOut(this);"></a>
        <img src="<%= BASEURL%>/images/botones/salir.gif" name="imgsalir" onMouseOver="botonOver(this);" onClick="parent.close();" onMouseOut="botonOut(this);" style="cursor:pointer ">
  </div>

		</div>


<div id="capaLista" style="position:absolute; width:100%; height:100%; z-index:0; left: 0px; top: 290px; overflow: scroll;">
</div>

</body>
</html>









