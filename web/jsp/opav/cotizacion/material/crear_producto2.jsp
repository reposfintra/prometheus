<%@page import="com.tsp.operation.model.beans.Usuario"%>
<%@page contentType="text/html"%>
<%@page session="true"%>
<%@page import="java.util.*"%>
<%@page import="com.tsp.opav.model.*"%>
<%@page import="com.tsp.opav.model.beans.*"%>
<%@page import="com.tsp.opav.model.services.*"%>
<%@page import="com.tsp.util.*"%>
<%@ taglib uri="http://jakarta.apache.org/taglibs/input-0.90" prefix="input" %>
<%@include file="/WEB-INF/InitModel.jsp"%>
<script src="<%=BASEURL%>/js/prototype.js"        type="text/javascript"></script>
<script src="<%=BASEURL%>/js/categoria.js"        type="text/javascript"></script>
<link href="<%= BASEURL%>/css/estilostsp.css" rel="stylesheet" type="text/css">
<link href="<%=BASEURL%>/css/default.css"       rel="stylesheet" type="text/css">
<link href="<%=BASEURL%>/css/alert.css" rel="stylesheet" type="text/css"/>
<link href="<%=BASEURL%>/css/alphacube.css" rel="stylesheet" type="text/css"/>
<script type="text/javascript" src="<%=BASEURL%>/js/buscarproveedor.js"></script>
<script type='text/javascript' src="<%= BASEURL%>/js/boton.js"></script>
<script type='text/javascript' src="<%=BASEURL%>/js/Validacion.js"></script>

<%@page contentType="text/html"  pageEncoding="ISO-8859-1"%>
<link href="<%=BASEURL%>/css/mac_os_x.css"      rel="stylesheet" type="text/css">
<script type='text/javascript' src="<%= BASEURL%>/js/general.js"></script>
<script src='<%=BASEURL%>/js/tools.js' language='javascript'></script>
<script src='<%=BASEURL%>/js/date-picker.js'></script>
<script src="<%= BASEURL%>/js/transferencias.js"></script>
<script type='text/javascript' src="<%= BASEURL%>/js/prototype.js"></script>
<script type='text/javascript' src="<%= BASEURL%>/js/scriptaculous/scriptaculous.js"></script>
<script type='text/javascript' src="<%= BASEURL%>/js/autosuggest.js"></script>
<script type='text/javascript' src="<%= BASEURL%>/js/scriptaculous/controls.js"></script>
<script type='text/javascript' src="<%= BASEURL%>/js/scriptaculous/builder.js"></script>
<script type="text/javascript" src="<%=BASEURL%>/js/effects.js"        ></script>
<script type="text/javascript"src="<%=BASEURL%>/js/window.js"        ></script>
<script type="text/javascript"src="<%=BASEURL%>/js/window_effects.js" ></script>
<link href="<%=BASEURL%>/css/default.css"       rel="stylesheet" type="text/css">
<link href="<%=BASEURL%>/css/alert.css" rel="stylesheet" type="text/css"/>
<link href="<%=BASEURL%>/css/alphacube.css" rel="stylesheet" type="text/css"/>
<%
        Material mat = (Material) session.getAttribute("datosMaterial");
        String pag = request.getParameter("pag") != null ? request.getParameter("pag") : "";
%>


<script>
 function color_fila(id,color){

                    $(id).style.backgroundColor=color;
                }


function alfanumerico(e) {
   
}

function calcula_precio()
{
	var a =document.getElementById('precio_compra').value;
        var p = 100 + parseFloat(document.getElementById('porcentaje_compra').value);
	var pc= parseFloat(a*(p/100));
        document.getElementById('precio').value=	Math.floor(pc);
	
}

function guardar_material(url,opcion){   
   
    var tipo  = document.getElementById('tipo').value;
    var pre = document.getElementById('precio_compra');
    var precio  = (pre) ? pre.value : 0;
    var desc = document.getElementById('descripcion').value;
    if (precio <= 0 && tipo==='M'){
        alert('Para los materiales el precio debe ser mayor a 0');
    }else if (desc == ''){
        alert('Debe ingresar una descripcion para el producto o servicio');
    }else{
         document.getElementById('descripcion').value=desc.replace(/&/g,'Y');        
         enviarAction(url, opcion);
    }
    
}


</script>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=windows-1252" />
<title>Creacion y Modificacion de Material</title>
</head>
    <body  onload="deshabilitarPorTipo();">
		<div id="capaSuperior" style="position:absolute; width:100%; height:100px; z-index:0; left: 0px; top: 0px;">
		  <jsp:include page="/WEB-INF/toptsp.jsp?encabezado=Creación y Modificación de  Material"/>
		</div>
		<div id="formulario" style="position:absolute; width:100%; height:87%; z-index:0; left: -1px; top: 104px; overflow: scroll;">

		            <%
			Usuario usuario = (Usuario) session.getAttribute("Usuario");
                        ClientesVerService clvsrv = new ClientesVerService(usuario.getBd());
            			String perfil = clvsrv.getPerfil(usuario.getLogin());

                        String idmaterial = "";
                        String tipo = "";
                        String descripcion = "";
                        String medida = "";
                        double valor = 0;
			double valor_compra= 0;
                        double porcentaje_compra = 17.49;
                        String unidad_empaque = "";
                        String alcance = "";
                        String codigo = "";
                        int idcat = 0;
                        int idsubcat = 0;
                        int idts = 0;
                        String reg_status = "";

                        //--Ente Cerficador
                        String certificado="";
                        String ente_certi="";




                        //Datos una vez creados debe cargar los ultimos tipos, idcat, idsubcat, idtiposubcat
                        tipo = request.getParameter("tipo") != null ? request.getParameter("tipo") : "";
                        idcat = request.getParameter("idcat") != null ? Integer.parseInt(request.getParameter("idcat").toString()) : 0;
                        idsubcat = request.getParameter("idsubcat") != null ? Integer.parseInt(request.getParameter("idsubcat").toString()) : 0;
                        idts = request.getParameter("idts") != null ? Integer.parseInt(request.getParameter("idts").toString()) : 0;


                        String opcion_pagina = request.getParameter("opcion_pagina")!=null?request.getParameter("opcion_pagina"):"";
                        String idmat = request.getParameter("idmat")!=null?request.getParameter("idmat"):"";

                        //



                        if (mat != null) {
                            idmaterial = mat.getIdMaterial();
                            tipo = mat.getTipo();
                            descripcion = mat.getDescripcion();
                            medida = mat.getMedida();
                            valor = mat.getValor();
		            valor_compra = mat.getValorCompra();
                            porcentaje_compra = Math.rint(((100*mat.getValor())/mat.getValorCompra())-100);
                            unidad_empaque = mat.getUnidad_empaque();
                            alcance = mat.getAlcance();
                            codigo = mat.getCodigo();
                            idcat = mat.getIdcategoria();
                            idsubcat = mat.getIdsubcategoria();
                            idts = mat.getIdtiposubcategoria();
                            reg_status = mat.getRegStatus();

                            //--Ente Cerficador
                            certificado = mat.getCertificado();
                            ente_certi = mat.getEnte_certificador();


                        }





            %>

		<form id ="forma" name="forma" action="<%= CONTROLLEROPAV%>?estado=Materiales&accion=Crear&opcion=0" method="post">

                <table width="54%"  border="1" align="center">
                    <tr>
                        <td>
                            <table width="100%" height="330"  border="0" align="center">
                                <tr>

                                    <td width="16%" height="23" class="subtitulo1">Ingresar Producto </td>
                                    <td colspan="4" class="barratitulo"><img src="<%=BASEURL%>/images/titulo.gif" width="25" height="21"></td>
                                </tr>
                                <tr class="fila">
                                    <td class="fila">Codigo <input type="hidden" name="exid" value="<%=idmaterial%>"></td>

                                    <td colspan="2"><span class="fila">
                                            <input name="exproducto" type="text" id="exproducto" value="<%=codigo%>" READONLY>

                                            <input name="pagina"  type="hidden" id="pagina" value="crear" >

                                        </span></td>
                                    <td><div align="left"><span class="fila">Tipo: </span>
                               <select name="tipo" id="tipo" onChange="deshabilitarPorTipo();" <% if(opcion_pagina.equals("consultar")){%> disabled <% }%>>
                          
                         <%if(clvsrv.ispermitted(perfil, "62") || clvsrv.ispermitted(perfil, "63")) {%>
                        <option value="M" <%if (tipo.equals("M")) {out.print(" selected");}%>>Material</option>
                         <%}
                       
        %>
                                <option value="D" <%if (tipo.equals("D")) {out.print(" selected");}%>>Mano de obra</option>
                                <option value="O" <%if (tipo.equals("O")) {out.print(" selected");}%>>Otros</option>
                               
                                      </select>
                                        </div></td>
                                    <td class="fila"><label>Estado:
                                            <select name="status" id="status" >
                                                <option value=""    <% if (reg_status.equals("")) {%>  selected  <% }%> >Activo</option>
                                                <option value="A"   <% if (reg_status.equals("A")) {%>  selected  <% }%>>Inactivo</option>
                                    </select>
                                        </label></td>
                                </tr>
                                <tr>
                                  <td class="fila"><div align="center">Categoria</div></td>
                                  <td colspan="4" class="fila"><span class="letra">
                                    <%if(clvsrv.ispermitted(perfil, "62")) {%>                                  
                                    <select name="categoria"   id="categoria"  style=" width: 190" <% if(opcion_pagina.equals("consultar")){%> disabled="disabled" <% }%>>
                                      <option value="">Seleccione Categoria</option>
                                    </select>                                                                        
                                  </span>
                                  <%}
								  else
								  {%>
                                  <select name="categoria"   id="categoria"  style=" width: 190" <% if(opcion_pagina.equals("consultar")){%> disabled="disabled" <% }%>>
                                   
                                         <option value="4">ALCALDIA DISTRITAL DE BARRANQUILLA </option>
                                         <option value="333">ALCALDIA-GOBERNACION DE BOLIVAR</option>
                                    </select> 
                                    <%}%>
                                  </td>
                                </tr>
                                <%if(clvsrv.ispermitted(perfil, "62")) {%>
                                
                                
                                <tr>
                                  <td class="fila">  <div align="center">Precio</div></td>
                                  <td class="fila"><input name="precio_compra" type="text" id="precio_compra"  onchange="calcula_precio()"value="<%=valor_compra%>" size="20"<% if(opcion_pagina.equals("consultar")){%> disabled="disabled" <% }%> /></td>
                                  <td class="fila"><input name="porcentaje_compra" type="text" id="porcentaje_compra" size="3" value="<%=porcentaje_compra%>" onblur="calcula_precio()" />&nbsp;%</td>
                                  <td align="center" class="fila"> Precio Contratista</td>
                                  <td class="fila"><input name="precio" type="text" id="precio" value="<%=valor%>" size="20"  readonly="readonly"/></td>
                                </tr>
                                <%}else
								{%>
                                <tr>
                                <td class="fila"><div align="center">Precio</div></td>                                
                                  <td colspan="4" class="fila"><input name="precio" type="text" id="precio" value="<%=valor%>" size="20"<% if(opcion_pagina.equals("consultar")){%> disabled="disabled" <% }%> /></td>
                                </tr>
                                 <%}%>
                                <tr>
                                    <td class="fila">Descripcion </td>
                                    <td colspan="4" class="fila"><textarea name="descripcion" cols="90" rows="4" id="descripcion"  
                                    onKeyPress="return alfanumerico(event)" ><%=descripcion%></textarea></td>
                                </tr>


                                <tr class="fila">
                                    <td>&nbsp;</td>
                                    <td colspan="2">&nbsp;</td>
                                    <td>&nbsp;</td>
                                    <td>&nbsp;</td>
                                </tr>
                                <tr>
                                    <td height="25" class="subtitulo1">Datos</td>
                                    <td colspan="4" class="fila"><span class="barratitulo"><img src="<%=BASEURL%>/images/titulo.gif" width="25" height="21"></span></td>
                                </tr>





								<tr class="fila">
                                    <td>&nbsp;</td>
                                    <td colspan="2"><div align="center">Unidad de medida </div></td>
                                    <td><div align="center">Unidad de Empaque </div></td>
                                    <td>&nbsp;</td>
                                </tr>








                                <tr>
                                    <td class="fila">&nbsp;</td>
                                    <td width="28%" colspan="2" class="fila">
                                        <div align="center">
                                          <select name="medida" id="medida" <% if(opcion_pagina.equals("consultar")){%> disabled <% }%>>
                                            <option value="UNIDADES" <%if (medida.equals("UNIDADES")) {
                                                            out.print(" selected");
                                                        }%>>Unidades</option>
                                            <option value="METROS" <%if (medida.equals("METROS")) {
                                                            out.print(" selected");
                                                        }%>>Metros</option>
                                            <option value="KILOGRAMOS" <%if (medida.equals("KILOGRAMOS")) {
                                                            out.print(" selected");
                                                        }%>>Kilogramos</option>
                                            <option value="GALON" <%if (medida.equals("GALON")) {
                                                            out.print(" selected");
                                                        }%>Galon
                                            </option>
                                            <option value="GLOBAL" <%if (medida.equals("GLOBAL")) {
                                                            out.print(" selected");
                                                        }%>Global
                                            </option>
                                            <option value="LITRO" <%if (medida.equals("LITRO")) {
                                                            out.print(" selected");
                                                        }%>Litro
                                            </option>
                                            </select>
                                        </div></td>
                                    <td width="24%" class="fila"><div align="center">
                                      <input type="text" name="uni_empaque" id="uni_empaque" value="<%=unidad_empaque%>" />
                                    </div></td>
                                    <td width="32%" class="fila"><label>
                                      <div align="center"></div>
                                    </label></td>
                                </tr>




                                <tr class="fila">
                                    <td>&nbsp;</td>
                                    <td colspan="2"><div align="center">Certificado</div></td>
                                    <td><div align="center">Ente Certificador </div></td>
                                    <td><div align="center"></div></td>
                                </tr>




                                <tr>
                                    <td class="fila">&nbsp;</td>
                                    <td colspan="2"  class="fila"><div align="center">
                                            <select name="certificado" id="certificado" <% if(opcion_pagina.equals("consultar")){%> disabled <% }%>  onchange="habilitarCertificador();">
                                          <option value="N" <% if(certificado.equals("N")){ %>selected="selected" <%}%> >NO</option>
                                        <option value="S" <% if(certificado.equals("S")){ %>selected="selected" <%}%> >SI</option>
                                      </select>
                                  </div></td>
                                    <td   class="fila"><label>
                                      <div align="center">
                                          <select name="ente_certi" id="ente_certi" <% if(opcion_pagina.equals("consultar")){%>  disabled  <% }%>    >
                                          <option value="">Seleccione Certificador</option>
                                        </select>
                                        </div>
                                    </label></td>
									<td   class="fila"><label>

								    <div align="left"></div>
									</label></td>
                                </tr>



                                
                            </table>
                        </td>
                    </tr>
                </table>
          <div align="center">
                    <br>
              <img src="<%= BASEURL%>/images/botones/guardar.gif" name="imgguardar"  onClick="guardar_material('<%=CONTROLLEROPAV%>','<%=opcion_pagina%>');"  onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" style="cursor:hand ">

<% if(opcion_pagina.equals("modificar")){%>
<a href="<%=CONTROLLEROPAV%>?estado=Materiales&accion=Crear&opcion=2&opcion_pagina=modificar&idmat=<%=idmat%>"><img src="<%= BASEURL%>/images/botones/restablecer.gif"  name="imgsalir" border="0"  style="cursor:hand "   onMouseOver="botonOver(this);" onMouseOut="botonOut(this);"></a>
<% }%>
                      <img src="<%= BASEURL%>/images/botones/salir.gif"   name="imgsalir"     onClick="parent.close();"   onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" style="cursor:hand ">            </div>

            </form>

                      <%
String mens = request.getParameter("msg") != null ? request.getParameter("msg") : "";
if ((!mens.equals("")) && (mens != null)) {
%>

                <table border="2" align="center">
                    <tr>
                        <td>
                            <table width="100%"  border="0" align="center"  bordercolor="#f7f5f4" bgcolor="#ffffff">
                                <tr>
                                    <td width="229" align="center" class="mensajes"><%=mens%></td>
                                    <td width="29" background="<%=BASEURL%>/images/cuadronaranja.JPG">&nbsp;&nbsp;</td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>

<% }%>


	</div>




</body>
</html>


<%
           session.setAttribute("datosMaterial", null);
//            if (mat != null) {
%>

<script>
    document.getElementById("pagina").value = "consulta";
//alert("idcat________________________"+idcat);
    <% if(idcat==0){        
     
     if(clvsrv.ispermitted(perfil, "62")) { 
    %>
    buscarCategorias('<%=CONTROLLEROPAV%>', '<%=BASEURL%>');
    <%}%>
    //prueba();
 buscarCertificadores('<%=CONTROLLEROPAV%>', '<%=BASEURL%>', '<%=ente_certi%>');


    <% }else{%>
    //buscarCategorias('<%=CONTROLLEROPAV%>', '<%=BASEURL%>',  '19', '8', '5');
     buscarCategorias('<%=CONTROLLEROPAV%>', '<%=BASEURL%>',  '<%=idcat%>', '<%=idsubcat%>', '<%=idts%>');

     buscarCertificadores('<%=CONTROLLEROPAV%>', '<%=BASEURL%>','<%=ente_certi%>');

    <% }%>





</script>

<%// }%>







