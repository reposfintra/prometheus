<%--
    Document   : gestion_contratistas
    Created on : 9/06/2010, 12:02:26 PM
    Author     : rhonalf
--%> 
<%@page import="com.tsp.operation.model.beans.Ciudad"%>
<%@page import="com.tsp.operation.model.beans.Usuario"%>
<%@ page session   ="true"%>
<%@ page errorPage ="/error/ErrorPage.jsp"%>
<%@ page import    ="java.util.*"%>
<%@ page import    ="com.tsp.operation.model.CiudadService"%>
<%@ page import    ="com.tsp.opav.model.beans.*"%>
<%@ page import    ="com.tsp.opav.model.services.*"%>
<%@ page import    ="com.tsp.util.*"%>
<%@ page import    ="com.tsp.util.Util.*"%>
<%@include file="/WEB-INF/InitModel.jsp"%>
<%
    response.addHeader("Cache-Control","no-cache");
    response.addHeader("Pragma","No-cache");
    response.addDateHeader ("Expires", 0);

%>
<%@taglib uri="http://jakarta.apache.org/taglibs/input-0.90" prefix="input" %>
<%@page contentType="text/html" pageEncoding="ISO-8859-1"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
   "http://www.w3.org/TR/html4/loose.dtd">
<!-- This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>. -->
<html>
    <head>
        <link href="<%= BASEURL %>/css/estilostsp.css" rel="stylesheet" type="text/css">
        <script type='text/javascript' src="<%= BASEURL %>/js/boton.js"></script>
        <script type='text/javascript' src="<%= BASEURL %>/js/prototype.js"></script>
        <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
        <title>Gestion de contratistas</title>
        <%
            Usuario usuario = (Usuario) session.getAttribute("Usuario");
            ContratistaService csr = new ContratistaService(usuario.getBd());
        %>
        <script type="text/javascript">
            function buscar(){
                var cad = document.getElementById("cadena").value;
                var ind = document.getElementById("param").selectedIndex;
                var par = document.getElementById("param").options[ind].value;
                var url = document.getElementById("formulario").action;
                //alert('cadena '+cad+' param '+par);
                var p =  'opcion=search&param='+par+'&cadena='+cad;
                new Ajax.Request(
                    url,
                    {
                        method: 'post',
                        parameters: p,
                        onLoading: loading,
                        onComplete: llenarDiv
                    });
            }

            function loading(){
                document.getElementById("busqueda").innerHTML ='<span class="letra">Buscando </span><img alt="cargando" src="<%= BASEURL%>/images/cargando.gif"  name="imgload">';
            }

            function llenarDiv(response){
                document.getElementById("busqueda").innerHTML = response.responseText;
            }

            function limpiarForm(){
                document.getElementById("codigo").value = '';
                document.getElementById("clave").value = '';
                document.getElementById("nit").value = '';
                document.getElementById("nombre").value = '';
                document.getElementById("email").value = '';
                document.getElementById("secuencia").value = '';
                escogerSelect("actividad",'');
                escogerSelect("autoretenedor",'');
                escogerSelect("domicilio",'');
                escogerSelect("reteica",'');
                escogerSelect("contribuyente",'');
                escogerSelect("regimen",'');
                document.getElementById("responsable").value = '';
                document.getElementById("direccion").value = '';
                document.getElementById("telefono").value = '';
                document.getElementById("celular").value = '';
            }

            function limpiar(){
                document.getElementById("cadena").value = '';
                document.getElementById("param").selectedIndex = 0;
            }

            function buscarDatos(){
                var idx = document.getElementById("codigo").value;
                if(idx!=''){
                    inactivar();
                    var url = document.getElementById("formulario").action;
                    var ps = 'opcion=listar&codigo='+idx;
                    new Ajax.Request(
                        url,
                        {
                            method: 'post',
                            parameters: ps,
                            onComplete: llenarForm
                        }
                    );
                }
                else{
                    alert('Debe escribir un codigo para poder cargar los datos');
                }
            }

            function inactivar(){
                document.getElementById("imgaceptar").src='<%= BASEURL%>/images/botones/agregarDisable.gif';
                document.getElementById("imgaceptar").onclick='';
                document.getElementById("imgaceptar").onmouseover='';
                document.getElementById("imgaceptar").onmouseout='';
                //----------------------------------------------------------------------
                document.getElementById("imgeliminar").src='<%= BASEURL%>/images/botones/eliminar.gif';
                document.getElementById("imgeliminar").onclick=function x(){enviar('delete');};
                document.getElementById("imgeliminar").onmouseover=function x1(){botonOver(document.getElementById("imgeliminar"));};
                document.getElementById("imgeliminar").onmouseout=function x2(){botonOut(document.getElementById("imgeliminar"));};
                //----------------------------------------------------------------------
                document.getElementById("imgmodificar").src='<%= BASEURL%>/images/botones/modificar.gif';
                document.getElementById("imgmodificar").onclick=function y(){enviar('modify');};
                document.getElementById("imgmodificar").onmouseover=function y1(){botonOver(document.getElementById("imgmodificar"));};
                document.getElementById("imgmodificar").onmouseout=function y2(){botonOut(document.getElementById("imgmodificar"));};
            }

            function llenarForm(response){
                var array = response.responseText.split(';_;');
                var tam = array.length;
                if(tam>0){
                    var actividad = array[0];
                    var autoretenedor = array[1];
                    var clave = array[2];
                    var reteica = array[3];
                    var domicilio = array[4];
                    var mail = array[5];
                    var contribuyente = array[6];
                    var nit = array[7];
                    var nombre = array[8];
                    var regimen = array[9];
                    var secuencia = array[10];
                    document.getElementById("clave").value = clave;
                    document.getElementById("nit").value = nit;
                    document.getElementById("nombre").value = nombre;
                    document.getElementById("email").value = mail;
                    document.getElementById("secuencia").value = secuencia;
                    escogerSelect("actividad",actividad);
                    escogerSelect("autoretenedor",autoretenedor);
                    escogerSelect("domicilio",domicilio);
                    escogerSelect("reteica",reteica);
                    escogerSelect("contribuyente",contribuyente);
                    escogerSelect("regimen",regimen);
                    document.getElementById("responsable").value = array[11];
                    document.getElementById("direccion").value = array[12];
                    document.getElementById("telefono").value = array[13];
                    document.getElementById("celular").value = array[14];
                }
                else{
                    alert("No se han obtenido resultados");
                }
            }

            function escogerSelect(nombresel,dato){
                var select = document.getElementById(nombresel);
                for (i=0;i<select.length;i++){
                    if(dato == select.options[i].value){
                        select.options[i].selected = true;
                    }
                }
            }

            function enviar(opcion){
                var ids = document.getElementById('codigo').value;
                var txtalert = '';
                if(opcion=='delete'){
                    txtalert = '�Esta seguro de eliminar el registro?';
                }
                else if(opcion==='modify'){
                    txtalert = '�Esta seguro de modificar el registro?';
                }
                else if(opcion=='insert'){
                    txtalert = '�Esta seguro de insertar un nuevo registro?';
                }
                else{
                    alert('No se selecciono nada ... raro');
                }
                var choice = confirm(txtalert);
                if(choice==true){
                    if(txtalert!=''){
                        sendForm(opcion,ids);
                    }
                }
            }

            function sendForm(opcion,ids){
                document.getElementById("formulario").action = document.getElementById("formulario").action + '&opcion=' + opcion;
                if(opcion=='delete' || opcion=='modify'){
                    document.getElementById("formulario").action = document.getElementById("formulario").action + '&codigo=' + ids;
                }
                document.getElementById("formulario").submit();
            }

        </script>
    </head>
    <body>
        <div id="capaSuperior" style="position:absolute; width:100%; height:100px; z-index:0; left: 0px; top: 0px;">
            <jsp:include page="/WEB-INF/toptsp.jsp?encabezado=Gestion de contratistas"/>
        </div>
        <br>
        <br>
        <div align="center" id="capaCentral" style="position:absolute; width:100%; height:85%; z-index:0; left: -1px; top: 100px; overflow:visible;">
            <form id="formulario" name="formulario" action="<%= CONTROLLEROPAV %>?estado=Contratista&accion=Gestion" method="POST">
                <table border="0" width="100%">
                    <thead>
                        <tr class="subtitulo1">
                            <th>Campo</th>
                            <th>Valor</th>
                            <th>Campo</th>
                            <th>Valor</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr class="fila">
                            <td>Codigo del contratista</td>
                            <td>
                                <input type="text" id="codigo" name="codigo" value="" >
                                <img alt="buscar" src="<%= BASEURL%>/images/botones/iconos/buscar.gif" name="imgbuscar1" onClick="buscarDatos();" style="cursor:pointer ">
                            </td>
                            <td>Clave</td>
                            <td> <input type="password" id="clave" name="clave" value=""> </td>
                        </tr>
                        <tr class="fila">
                            <td>Nit</td>
                            <td> <input type="text" id="nit" name="nit" value="" size="15" maxlength="15"> </td>
                            <td>Gran contribuyente</td>
                            <td>
                                <select id="contribuyente" name="contribuyente">
                                    <option value="" selected>...</option>
                                    <option value="NO">NO</option>
                                    <option value="SI">SI</option>
                                </select>
                            </td>
                        </tr>
                        <tr class="fila">
                            <td>Nombre</td>
                            <td><input type="text" id="nombre" name="nombre" value="" size="60" maxlength="60"></td>
                            <td>Regimen tributario</td>
                            <td>
                                <select id="regimen" name="regimen">
                                    <option value="" selected>...</option>
                                    <option value="COMUN">Comun</option>
                                    <option value="SIMPLIFICADO">Simplificado</option>
                                    <option value="NATURAL">Natural</option>
                                </select>
                            </td>
                        </tr>
                        <tr class="fila">
                            <td>E-mail</td>
                            <td><input type="text" id="email" name="email" value="" size="60"></td>
                            <td>Autoretenedor</td>
                            <td>
                                <select id="autoretenedor" name="autoretenedor">
                                    <option value="" selected>...</option>
                                    <option value="NO">NO</option>
                                    <option value="SI">SI</option>
                                </select>
                            </td>
                        </tr>
                        <tr class="fila">
                            <td>Domicilio comercial</td>
                            <td>
                                <select id="domicilio" name="domicilio">
                                    <option value="" selected>...</option>
                                    <%
                                        try {
                                            CiudadService cius = new CiudadService();
                                            cius.listarCiudadGeneral();
                                            Ciudad ciu2=null;
                                            Vector listac = cius.obtCiudades();
                                            for(int i=0;i<listac.size();i++){
                                                ciu2 = (Ciudad)listac.get(i);
                                            
                                     %>
                                     <option value="<%= ciu2.getcodciu() %>"><%= ciu2.getnomciu() %></option>
                                     <%
                                            }
                                        }
                                        catch (Exception e) {
                                            System.out.println("Error listando los datos de ciudades en gestion_contratistas.jsp: "+e.toString());
                                            e.printStackTrace();
                                        }
                                    %>
                                </select>
                            </td>
                            <td>Actividad</td>
                            <td>
                                <select id="actividad" name="actividad">
                                    <option value="" selected>...</option>
                                    <%
                                        try{
                                            String str="";
                                            ArrayList lista = csr.listaActividades();
                                            for (int idx = 0; idx <lista.size(); idx++) {
                                                str = (String)lista.get(idx);
                                                out.println(str);
                                            }
                                        }
                                        catch(Exception e){
                                    %>
                                    <option value="-1">No se encontraron</option>
                                    <%
                                            System.out.println("Error al listar actividades en gestion_contratistas.jsp: "+e.toString());
                                            e.printStackTrace();
                                        }
                                    %>
                                </select>
                            </td>
                        </tr>
                        <tr class="fila">
                            <td>Secuencia prefactura</td>
                            <td><input type="text" id="secuencia" name="secuencia" value="" size="15"></td>
                            <td>Codigo reteica</td>
                            <td>
                                <select id="reteica" name="reteica">
                                    <option value="" selected>...</option>
                                    <%
                                        try{
                                            String str="";
                                            ArrayList lista = csr.listarImpuestos(1);
                                            for (int idx = 0; idx <lista.size(); idx++) {
                                                str = (String)lista.get(idx);
                                                out.println(str);
                                            }
                                        }
                                        catch(Exception e){
                                    %>
                                    <option value="-1">No se encontraron</option>
                                    <%
                                            System.out.println("Error al listar reteica en gestion_contratistas.jsp: "+e.toString());
                                            e.printStackTrace();
                                        }
                                    %>
                                </select>
                            </td>
                        </tr>
                        <tr class="subtitulo1" align="center">
                            <td colspan="4">Informacion para envio y recibido de materiales</td>
                        </tr>
                        <tr class="fila">
                            <td>Responsable</td>
                            <td><input type="text" id="responsable" name="responsable" value="" size="60"></td>
                            <td>Telefono</td>
                            <td><input type="text" id="telefono" name="telefono" value="" size="30"></td>
                        </tr>
                        <tr class="fila">
                            <td> Direccion </td>
                            <td><input type="text" id="direccion" name="direccion" value="" size="60"></td>
                            <td>Celular</td>
                            <td><input type="text" id="celular" name="celular" value="" size="30"></td>
                        </tr>
                        <tr class="fila">
                            <td colspan="4" align="center">
                                <img id="imgaceptar" alt="aceptar" src="<%= BASEURL%>/images/botones/agregar.gif" name="imgaceptar" onMouseOver="botonOver(this);" onClick="enviar('insert');" onMouseOut="botonOut(this);"  style="cursor:pointer ">
                                <img id="imgeliminar" alt="eliminar" src="<%= BASEURL%>/images/botones/eliminarDisable.gif" name="imgdelr" style="cursor:pointer ">
                                <img id="imgmodificar" alt="modificar" src="<%= BASEURL%>/images/botones/modificarDisable.gif" name="imgmody" style="cursor:pointer ">
                                <img alt="borrar" src="<%= BASEURL%>/images/botones/cancelar.gif" name="imgclear" onMouseOver="botonOver(this);" onClick="limpiarForm();" onMouseOut="botonOut(this);"  style="cursor:pointer ">
                                <img alt="salir" src="<%= BASEURL%>/images/botones/salir.gif" name="imgsalir" onMouseOver="botonOver(this);" onClick="parent.close();" onMouseOut="botonOut(this);"  style="cursor:pointer ">
                            </td>
                        </tr>
                    </tbody>
                </table>
                <br>
                <br>
                <table border="1" style="border-collapse:collapse;" width="100%">
                    <thead>
                        <tr class="subtitulo1">
                            <th colspan="2">Busqueda</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr class="fila">
                            <td>Filtro de busqueda</td>
                            <td>
                                <select name="param" id="param">
                                    <option>Seleccione...</option>
                                    <option value="id_contratista">Codigo del contratista</option>
                                    <option value="descripcion">Nombre</option>
                                    <option value="nit">Nit</option>
                                    <option value="email">E-mail</option>
                                </select>
                            </td>
                        </tr>
                        <tr class="fila">
                            <td>Cadena a buscar</td>
                            <td><input type="text" name="cadena" id="cadena" value="" size="60"></td>
                        </tr>
                        <tr class="fila">
                            <td colspan="2" align="center">
                                <img alt="buscar" src="<%= BASEURL%>/images/botones/buscar.gif"  name="imgbusq"  onMouseOver="botonOver(this);" onClick="buscar();" onMouseOut="botonOut(this);"  style="cursor:pointer ">
                                <img alt="borrar" src="<%= BASEURL%>/images/botones/cancelar.gif"  name="imgcancel"  onMouseOver="botonOver(this);" onClick="limpiar();" onMouseOut="botonOut(this);"  style="cursor:pointer ">
                            </td>
                        </tr>
                    </tbody>
                </table>
            </form>
            <br>
            <div id="headerb" class="subtitulo1" align="center">Resultados de la busqueda</div>
            <br>
            <div id="busqueda"></div>
            <br>
            <div align="center">
                <img alt="salir" src="<%= BASEURL%>/images/botones/salir.gif" name="imgsalir" onMouseOver="botonOver(this);" onClick="parent.close();" onMouseOut="botonOut(this);" style="cursor:pointer ">
            </div>
        </div>
    </body>
</html>
