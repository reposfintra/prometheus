<%-- 
    Document   : consultaEjecucion
    Created on : 16/06/2010, 01:33:17 PM
    Author     : darrieta-GEOTECH
--%>

<%@page contentType="text/html" pageEncoding="ISO-8859-1"%>
<%@page session ="true"%>
<%@page errorPage ="/error/ErrorPage.jsp"%>
<%@page import ="java.util.*" %>
<%@page import ="com.tsp.opav.model.*"%>
<%@page import ="com.tsp.opav.model.beans.*"%>
<%@include file ="/WEB-INF/InitModel.jsp"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
   "http://www.w3.org/TR/html4/loose.dtd">


<%
    String idSolicitud = request.getParameter("id_solicitud");
    String idAccion = request.getParameter("id_accion")==null?"-1":request.getParameter("id_accion");
    String graphURL = CONTROLLEROPAV+"?estado=Seguimiento&accion=Ejecucion&opcion=grafico&id_solicitud="+idSolicitud;
    NegocioApplus infoSolicitud = modelopav.seguimientoEjecucionService.obtenerInfoSolicitud(idSolicitud);
    ArrayList<Actividades> consulta;
    if(idAccion.equals("-1")){
        consulta= modelopav.seguimientoEjecucionService.consultarActividadesSolicitud(idSolicitud);
    }else{
        consulta= modelopav.seguimientoEjecucionService.consultarActividadesSolicitud(idSolicitud,idAccion);
    }
    ArrayList<Actividades> seguimientos = modelopav.seguimientoEjecucionService.consultarSeguimientosSolicitud(idSolicitud, idAccion);
    ArrayList<OfertaSeguimiento> totales = modelopav.seguimientoEjecucionService.consultarTotalesSeguimientos(idSolicitud);
    ArrayList<OfertaSeguimiento> observaciones = modelopav.seguimientoEjecucionService.consultarObservacionesSolicitud(idSolicitud,"");
    ArrayList acciones = modelopav.NegociosApplusService.listarAccionesSolicitud(idSolicitud);
    int numSeg = totales.size();
    boolean progCompleta = modelopav.seguimientoEjecucionService.tieneProgramacion(idSolicitud);
    String mensaje = progCompleta ? request.getParameter("mensaje") : "A�n no se ha realizado toda la programaci�n para esta solicitud";
%>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
        <title>Consulta de ejecuciones</title>
        <link href="<%= BASEURL%>/css/estilostsp.css" rel='stylesheet'>
        <script type='text/javascript' src="<%= BASEURL%>/js/boton.js"></script>
        <script type='text/javascript' src="<%= BASEURL%>/js/prototype.js"></script>
        <script type='text/javascript' src="<%= BASEURL%>/js/fxHeader.js"></script>
        <script type='text/javascript' src="<%= BASEURL%>/js/seguimientoEjecucion.js"></script>
        <script type="text/javascript">
            var json = new Array();
            var fechaI = '';
            function genJSON(columna) {
                json = new Array();
                fechaI = (document.getElementById("verDias").checked)
                         ? '' : '<%=infoSolicitud.getFecha()%>';
                        //document.getElementById('seg'+columna).innerHTML;
                for (k = 0; k < <%=consulta.size()%>; k++) {
                    json[k] = {
                        name: ( !document.getElementById("I_"+(k-1)+"_0") ||
                                !document.getElementById("I_"+k+"_0").innerHTML
                                === document.getElementById("I_"+(k-1)+"_0").innerHTML)
                                ? document.getElementById("I_"+k+"_0").innerHTML
                                : '',
                        desc: document.getElementById("I_"+k+"_1").innerHTML,
                        values: [{
                                from: parseInt(document.getElementById("FI_"+k).innerHTML),
                                to: parseInt(document.getElementById("FF_"+k).innerHTML),
                                progress: (columna !== -1)
                                        ? document.getElementById("P_"+k+"_"+columna).innerHTML
                                        : 0,                                        
                                progress2: (columna !== -1)
                                        ? document.getElementById("E_"+k+"_"+columna).innerHTML
                                        : 0
                            }]
                    };
                }
                AbrirVentana('<%= BASEURL%>');
            }
        </script>
    </head>
    <body>
        <div id="capaSuperior" style="position:absolute; width:100%; height:100px; z-index:0; left: 0px; top: 0px;">
            <jsp:include page="/WEB-INF/toptsp.jsp?encabezado=Programacion de actividades"/>
        </div>
        <div id="capaCentral" style="position:absolute; width:100%; height:100%; z-index:0; left: 0px; top: 100px;">
            <form method='post' name='form1' action="<%=CONTROLLEROPAV%>?estado=Seguimiento&accion=Ejecucion&opcion=generarPDF&id_solicitud=<%=idSolicitud%>">
                <table border="1" cellspacing="0" cellpadding="0" width="95%" align="center">
                    <tr>
                        <td>
                            <table border="0" cellspacing="0" cellpadding="3" class="tblTitulo" align="center"  width="100%">
                                <tr>
                                    <td>Id solicitud</td>
                                    <td style="font-weight: normal"><%=idSolicitud%></td>
                                    <td><strong>Cliente</strong></td>
                                    <td style="font-weight: normal"><%=infoSolicitud.getNombreCliente()%></td>
                                </tr>
                                <tr>
                                    <td><strong>Fecha inicial</strong></td>
                                    <td style="font-weight: normal"><%=infoSolicitud.getFecha()%></td>
                                    <td></td>
                                    <td style="font-weight: normal"><!--%=infoSolicitud.getFRecepcion()%-->
                                        <img src="<%=BASEURL%>/images/botones/iconos/gantt.gif" title="Ver Grafico" onclick="genJSON(-1)">
                                        <input type="checkbox" id="verDias" >Ver graficos en dias
                                    </td>
                                </tr>
                                <tr>
                                    <td><strong>Orden de trabajo</strong></td>
                                    <td style="font-weight: normal"><%=infoSolicitud.getNumOs()%></td>
                                    <td><strong>Accion</strong></td>
                                    <td style="font-weight: normal">
                                        <select id="cmbAcciones" onchange="actualizarConsultaEjecucion('<%=BASEURL%>','<%=idSolicitud%>')">
                                            <option value="-1">Todas</option>
                                            <%for (int i = 0; i < acciones.size(); i++) {
                                                NegocioApplus n = (NegocioApplus)acciones.get(i);
                                            %>
                                                <option value="<%=n.getIdAccion()%>" <%=idAccion.equals(n.getIdAccion())?"selected":""%>><%=n.getAcciones()%></option>
                                            <%}%>
                                        </select>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <input type="hidden" id="cont" name="cont" value="0">
                            <input type="hidden" id="maximo" name="maximo" value="1">
                            <input type="hidden" id="numActividades" name="numActividades" value="<%=consulta.size()%>"/>
                            <table border="0" id="tabActividades" width="95%">
                                <tr class="tblTitulo">
                                    <th width="30%">Acci&oacute;n</th>
                                        <!--onclick="mostrarObservacion('0:< %=consulta.size()-1%>','0:< %=numSeg-1%>')"-->
                                    <th width="20%">Actividad</th>
                                    <th >Responsable</th>
                                    <th >Peso obra</th>
                                    <th width="2%">Proyectado /ejecutado</th>
                                    <th width="2%">Inicio / Fin</th>
                                    <%for (int i = 0; i < numSeg; i++) {%>
                                        <th id="seg<%=i%>" onclick="genJSON(<%=i%>)">
                                            <%=totales.get(i).getFecha()%>
                                        </th>
                                    <%}%>
                                </tr>
                                <tr class="filagris">
                                    <td width="30%" rowspan="2">&nbsp;</td>
                                    <td width="20%" rowspan="2">&nbsp;</td>
                                    <td  rowspan="2">&nbsp;</td>
                                    <td  rowspan="2">&nbsp;</td>
                                    <td align="center">P</td>
                                    <td align="center">1</td>
                                    <%for (int i = 0; i < numSeg; i++) {%>
                                        <td align="center"
                                            onclick="mostrarObservacion('0:<%=consulta.size()-1%>','<%=i%>')">
                                            <strong id="P_G_<%=i%>"><%=totales.get(i).getAvance_esperado()%>%</strong>
                                        </td>
                                    <%}%>
                                </tr>
                                <tr style="height: 20px;" class="filaazul">
                                    <!--td width="30%">&nbsp;</td>
                                    <td width="20%">&nbsp;</td>
                                    <td >&nbsp;</td>
                                    <td >&nbsp;</td-->
                                    <td align="center">E</td>
                                    <td align="center"><%=infoSolicitud.getFRecepcion()%></td>
                                    <%for (int i = 0; i < numSeg; i++) {%>                                        
                                        <td align="center"
                                            onclick="mostrarObservacion('0:<%=consulta.size()-1%>','<%=i%>')">
                                            <strong id="E_G_<%=i%>"><%=totales.get(i).getAvance_registrado()%>%</strong>
                                        </td>
                                    <%}%>
                                </tr>
                                <%
                                for (int j = 0; j < consulta.size(); j++) {
                                    Actividades act = consulta.get(j);
                                %>
                                    <tr class="filagris">
                                        <td align="left" rowspan="2" id ="I_<%=j%>_0">
                                            <%=act.getDescAccion()%>
                                        </td>
                                        <td align="left" rowspan="2" id ="I_<%=j%>_1"
                                            onclick="mostrarObservacion('<%=j%>','0:<%=numSeg-1%>')">
                                            <%=act.getDescripcion()%>
                                        </td>
                                        <td align="left" rowspan="2" id ="I_<%=j%>_2">
                                            <%=act.getResponsable()%>
                                        </td>
                                        <td align="center" rowspan="2" id ="I_<%=j%>_3">
                                            <%=act.getPeso_obra()%>
                                        </td>
                                        <td align="center">P</td>
                                        <td align="center" id ="FI_<%=j%>"><%=act.getFechaInicial()%></td>
                                        <% Actividades a, b;
                                        for (int i = 0; i < numSeg; i++) {
                                            try {
                                                a = seguimientos.get((j*numSeg)+i);
                                            } catch(Exception e) {
                                                a = null;
                                            } 
                                        %>
                                            <td align="center" id="P_<%=j%>_<%=i%>"
                                                title="<%=(a != null) ? a.getObservaciones() : ""%>"
                                                onclick="<% if (a != null) {%> mostrarObservacion('<%=j%>','<%=i%>') <%}%>">
                                                <%=(a != null) ? a.getAvance_esperado() : "0.00"%>%
                                            </td>
                                        <%}%>
                                    </tr>
                                    <tr class="filaazul" id="fila<%=j%>">                                        
                                        <!--td align="left">&nbsp;</td>
                                        <td align="left">&nbsp;</td>
                                        <td align="left">&nbsp;</td>
                                        <td align="left">&nbsp;</td-->
                                        <td align="center">E</td>
                                        <td align="center" id ="FF_<%=j%>"><%=act.getFechaFinal()%></td>
                                        <%for (int i = 0; i < numSeg; i++) {
                                            try {
                                                a = seguimientos.get(j);
                                            } catch(Exception e) {
                                                a = null;
                                            } 
                                            try {
                                                b = seguimientos.get((j*numSeg)+i);
                                            } catch(Exception e) {
                                                b = null;
                                            } 
                                        %>
                                            <td align="center" id="E_<%=j%>_<%=i%>"
                                                title="<%=(a != null) ? a.getObservaciones() : ""%>"
                                                onclick="<% if (a != null) {%> mostrarObservacion('<%=j%>','<%=i%>') <%}%>">
                                                <%=(b != null) ? b.getAvance() : "0.00"%>%
                                            </td>
                                        <%}%>
                                    </tr>
                                <%}%>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <img src="<%= graphURL %>" border=0 style="float: left; margin-right: 20px;">                            
                            <!--div>
                            < %
                            String totalObservaciones = "";
                            for (int idx = 0; idx < observaciones.size(); idx++) {
                                OfertaSeguimiento os = observaciones.get(idx);
                                totalObservaciones += os.getUsuario()+" "+os.getFecha()+"\n"+os.getObservaciones()+"\n\n";
                            }%>
                            <p style="margin-right: 350px;">Observaciones solicitud</p>
                            <textarea name="areaObsGuardada" id="areaObsGuardada" cols="70" rows="15" readonly>< %=totalObservaciones%></textarea>
                            </div-->
                        </td>
                    </tr>
                </table>
                <div align="center">
                    <%if (progCompleta) {%>
                    <p>
                        <br/>
                        <img style="cursor:hand" src="<%=BASEURL%>/images/botones/generarPdf.gif" align="absmiddle" onmouseover="botonOver(this);" onmouseout="botonOut(this);" onClick="generarPDF()">
                        <img style="cursor:hand" src="<%=BASEURL%>/images/botones/salir.gif" align="absmiddle" onmouseover="botonOver(this);" onmouseout="botonOut(this);" onClick="parent.close();">
                        <br/>
                        <br/>
                    </p>
                    <%}%>
                    <%if (mensaje != null) {%>
                    <table border="2" align="center">
                        <tr>
                            <td>
                                <table width="100%"  border="0" align="center"  bordercolor="#f7f5f4" bgcolor="#ffffff">
                                    <tr>
                                        <td width="229" align="center" class="mensajes"><%=mensaje%></td>
                                        <td width="29" background="<%=BASEURL%>/images/cuadronaranja.JPG">&nbsp;&nbsp;</td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                    </table>
                    <%}%>
                </div>
            </form>
        </div>
        <script type="text/javascript">
            fxheaderInit('tabActividades',500,3,0);
            fxheader();
        </script>
    </body>
</html>
