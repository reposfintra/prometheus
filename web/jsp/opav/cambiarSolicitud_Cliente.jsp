<%@page import="com.tsp.operation.model.beans.Usuario"%>
<%@ page session   ="true"%>
<%@ page errorPage ="/error/ErrorPage.jsp"%>
<%@ page import    ="java.util.*" %>
<%@ page import    ="com.tsp.opav.model.*"%>
<%@ page import    ="com.tsp.opav.model.beans.*"%>
<%@ page import    ="com.tsp.opav.model.services.*"%>
<%@ page import    ="com.tsp.util.*"%>
<%@ page import    ="com.tsp.util.Util.*"%>
<%@ include file   ="/WEB-INF/InitModel.jsp"%>
<%@taglib uri="http://jakarta.apache.org/taglibs/input-0.90" prefix="input" %>

<%
	String path = application.getRealPath(model.menuService.getRealUrl(request.getRequestURI()));
	String datos[] = model.menuService.getContenidoMenu(BASEURL,request.getRequestURI(),path);

	Usuario usuario = (Usuario) session.getAttribute("Usuario");
	String loginx=usuario.getLogin();

	ClientesVerService clvsrv = new ClientesVerService(usuario.getBd());
        String perfil = clvsrv.getPerfil(usuario.getLogin());

	String estadi=request.getParameter("estado");
	if (estadi==null){estadi="0";}

	String contrati=request.getParameter("contratistica");
	if (contrati==null){contrati="";}

	String fact_conformada_consultar=request.getParameter("fact_conformada_consultar");
	if (fact_conformada_consultar==null){fact_conformada_consultar="";}

	String id_solici=request.getParameter("id_sol");//090922
	if (id_solici==null){id_solici="";}//090922

	String id_cliente=(request.getParameter("id_cliente")==null)?"":request.getParameter("id_cliente");//090922
	if (id_cliente==null){id_cliente="";}//090922

	String nicc=request.getParameter("nicc");//090922
	if (nicc==null){nicc="";}//090922

	String nomclie=request.getParameter("nomclie");//090922
	if (nomclie==null){nomclie="";}//090922

	String num_osxi=request.getParameter("num_osx");
	if (num_osxi==null){num_osxi="";}

	ArrayList negocios ;

	try {
            //negocios = model.negociosApplusService.getNegociosApplus2(estadi,contrati,num_osxi,fact_conformada_consultar,loginx,id_solici,nicc ,"",id_cliente,"");
            negocios = modelopav.NegociosApplusService.getNegociosApplus2(estadi,contrati,num_osxi,fact_conformada_consultar,loginx,id_solici,nicc ,"",id_cliente,"","");
	}
        catch(Exception e ){
            System.out.println("errorcillo en model.NegociosApplusService.getNegociosApplus2()"+e.toString());
            negocios=null;
	}
%>
<html>
<%try{%>
<head>
    <title>Cambiar cliente de solicitud</title>

    <link href="<%=BASEURL%>/css/estilostsp.css"    rel="stylesheet" type="text/css">

    <script src="<%=BASEURL%>/js/boton.js"          type="text/javascript"></script>
    <script src="<%=BASEURL%>/js/Validacion.js"     type='text/javascript'></script>
    <script src="<%=BASEURL%>/js/tools.js"          type="text/javascript"></script>
    <script src="<%=BASEURL%>/js/date-picker.js"    type="text/javascript"></script>
    <script src="<%=BASEURL%>/js/transferencias.js" type="text/javascript"></script>
    <script src="<%=BASEURL%>/js/prototype.js"      type="text/javascript"></script>

    <!-- Las siguientes librerias CSS y JS son para el manejo de
        DIVS dinamicos.
    -->

    <link href="<%=BASEURL%>/css/default.css"       rel="stylesheet" type="text/css">
    <link href="<%=BASEURL%>/css/mac_os_x.css"      rel="stylesheet" type="text/css">

    <script src="<%=BASEURL%>/js/effects.js"        type="text/javascript"></script>
    <script src="<%=BASEURL%>/js/window.js"         type="text/javascript"></script>
    <script src="<%=BASEURL%>/js/window_effects.js" type="text/javascript"></script>

    <!-- Las siguientes librerias CSS y JS son para el manejo del
        calendario(jsCalendar).
    -->

    <link rel="stylesheet" type="text/css" href="<%=BASEURL%>/css/jCal/jscal2.css" />
    <link rel="stylesheet" type="text/css" href="<%=BASEURL%>/css/jCal/border-radius.css" />

    <script type="text/javascript" src="<%=BASEURL%>/js/jCal/jscal2.js"></script>
    <script type="text/javascript" src="<%=BASEURL%>/js/jCal/lang/es.js"></script>

    <%
    if (request.getParameter("nadita")!=null && request.getParameter("nadita").equals("si")){%>
        <script>alert("No se hizo el cambio por restricción de seguridad.");</script>
    <%}
    %>
    <script>
        var temp=';;;;;;;;;;';

        var generar=0;







        function switchDiv(){
            if($('loading_div').style.visibility == 'visible'){
                $('loading_div').style.visibility = 'hidden';
            }
            else{
                $('loading_div').style.visibility = 'visible';
            }
        }





        function checkEnter(e){ //e is event object passed from function invocation//090720
                var characterCode //literal character code will be stored in this variable

                if(e && e.which){ //if which property of event object is supported (NN4)
                        e = e
                        characterCode = e.which //character code is contained in NN4's which property
                }else{
                        e = event
                        characterCode = e.keyCode //character code is contained in IE's keyCode property
                }

                if(characterCode == 13){ //if generated character code is equal to ascii 13 (if enter key)
                        //document.forms[0].submit() //submit the form
                        //alert("enter");
                        //return false
                        consultarEstado();
                }else{
                        //alert("e"+e+"char"+characterCode);
                        return true
                }
        }





        function consultarEstado(){

                formulario.target ="";
                formulario.action="<%=CONTROLLEROPAV%>?estado=Negocios&accion=Applus&opcion=consultarEstado2";
                formulario.submit();

        }



        function sendAction(url,filtro,nomselect,def){
            if(temp!=filtro){
                $('imgworking').style.visibility="visible";
                temp=filtro;
                var p = "filtro=" + filtro + "&nomselect=" + nomselect  + "&opcion=clnics" + "&idclie="+def;

                new Ajax.Request(
                url,
                {
                    method: 'get',
                    parameters: p,
                    onSuccess: deploySelect
                });

            }
        }

         function sendUpdate(url,baseurl){

                var p = "opcion=5&id_solici="+$('id_solici').value+"&idnics="+$('idnics').value+"&est="+$('estado').value;
                var msn = "";
                var div = document.getElementById("msn");

                if($('id_solici').value.length > 0 &&  $('idnics').value.length > 0){

                        new Ajax.Request(
                    url,
                    {
                        method: 'post',
                        parameters: p,
                        onSuccess: function(transport){



                                msn = "<table border='2' align='center'><tr>"+
                                "<td><table width='410' height='73' border='1' align='center'  bordercolor='#F7F5F4' bgcolor='#FFFFFF'><tr>"+
                                "<td width='282' align='center' class='mensajes'>"+transport.responseText+"</td>"+
                                "<td width='28' background='"+baseurl+"/images/cuadronaranja.JPG'>&nbsp;</td>"+
                                "<td width='78'>&nbsp;</td></tr>"+
                                "</table></td></tr></table>";

                            div.innerHTML= msn;

                        },
                        onFailure: function(){

                            msn = "<table border='2' align='center'><tr>"+
                            "<td><table width='410' height='73' border='1' align='center'  bordercolor='#F7F5F4' bgcolor='#FFFFFF'><tr>"+
                            "<td width='282' align='center' class='mensajes'>Proceso finalizado con errores</td>"+
                            "<td width='28' background='"+baseurl+"/images/cuadronaranja.JPG'>&nbsp;</td>"+
                            "<td width='78'>&nbsp;</td></tr>"+
                            "</table></td></tr></table>";

                            div.innerHTML= msn;
                        }
                    });



                }else{

                          msn = "<table border='2' align='center'><tr>"+
                            "<td><table width='410' height='73' border='1' align='center'  bordercolor='#F7F5F4' bgcolor='#FFFFFF'><tr>"+
                            "<td width='282' align='center' class='mensajes'>Ingrese la informacion para continuar</td>"+
                            "<td width='28' background='"+baseurl+"/images/cuadronaranja.JPG'>&nbsp;</td>"+
                            "<td width='78'>&nbsp;</td></tr>"+
                            "</table></td></tr></table>";

                            div.innerHTML= msn;

                }



        }

        function deploySelect(response){
            respuesta=response.responseText.split(';;;;;;;;;;');
            if(respuesta[0]==$('nomclie').value){
                document.getElementById("cliselect").innerHTML=respuesta[1];
                $('imgworking').style.visibility="hidden";
            }
        }

        /* Esta funcion init() se inicializa
         * en el onLoad del body.
         */
        function init(){
            redimensionar();
            formulario.id_solici.focus();
            sendAction('<%=CONTROLLEROPAV%>?estado=Negocios&accion=Applus',document.getElementById('nomclie').value,'idnics','<%=(request.getParameter("id_cliente")!=null)?request.getParameter("id_cliente"):""%>');
        }
    </script>
</head>
<body onLoad="init();" onResize="redimensionar();">

<div id="capaSuperior" style="position:absolute; width:100%; height:100px; z-index:0; left: 0px; top: 0px;">
<jsp:include page="/WEB-INF/toptsp.jsp?encabezado=Cambiar cliente de solicitud"/>
</div>

<div id="capaCentral" style="position:absolute; width:100%; height:85%; z-index:0; left: -1px; top: 100px; overflow: scroll;">

    <form  method="post" name="formulario"  >
        <input type="hidden" name="estadito" id="estado" value="<%=estadi%>">
    <%
    String respuesta=request.getParameter("respuesta");
	//String valor_neto="";//request.getParameter("valor_neto");
    %>
    <br><br>
    <table align="center" border="2px">




	<tr class="fila">
		<td>
			&nbsp;&nbsp;Id Solicitud&nbsp;:&nbsp;
		</td><td>
                    <input type="text" name="id_solici"  id="id_solici" value="<%=id_solici%>"  onKeyPress="checkEnter(event)">
			<img src='<%=BASEURL%>/images/botones/iconos/buscar.gif' width="15" height="15" onClick="consultarEstado();" title="Buscar Solicitud" style="cursor:hand" >
		</td>
	</tr>

	<tr class="fila">
		<td>
			&nbsp;&nbsp;Nombre Cliente&nbsp;:&nbsp;
		</td><td>
			<!--<input type="text" name="nomclie"  value="<%//=nomclie%>"  onKeyPress="checkEnter(event)" >-->
                        <input name="nomclie" id="nomclie" type="text" value="<%=nomclie%>"  onKeyPress="checkEnter(event)" class="textbox" id="campo" style="width:200;"  onChange="sendAction('<%=CONTROLLEROPAV%>?estado=Negocios&accion=Applus',document.getElementById('nomclie').value,'idnics','');" size="15" >

		</td>
	</tr>
	<tr class="fila">
		<td>&nbsp;

		</td>
                <td>
                    <div id="cliselect"></div>
                    <div id="imgworking" align="right"  style="visibility:hidden"><img src="<%=BASEURL%>/images/cargando.gif"></div>



                </td>
        </tr>
        </table>

                    <br>
                        <table  align="center">
                            <tr>
                                <td>
                                    <img src="<%=BASEURL%>/images/botones/aceptar.gif" height="21" title='Asignar Estado' onclick="sendUpdate('<%=CONTROLLEROPAV%>?estado=Electricaribe&accion=Oferta','<%=BASEURL%>')" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" style="cursor:hand">
                                </td>
                                <td>
                                    <img src='<%=BASEURL%>/images/botones/salir.gif'      style='cursor:hand'    title='Salir...'      name='i_salir'       onclick='parent.close();'           onMouseOver='botonOver(this);' onMouseOut='botonOut(this);'>
                                </td>

                            </tr>
                        </table>

 </form>
 <br>


                                    <div id="msn">


                                    </div>

</div>
<%=datos[1]%>
	<iframe width=188 height=166 name="gToday:normal:agenda.js:gfPop:plugins.js" id="gToday:normal:agenda.js:gfPop:plugins.js" src="<%=BASEURL%>/js/Calendario/ipopeng.htm" scrolling="no" frameborder="0" style="visibility:visible; z-index:999; position:absolute; left:-500px; top:0px;"> </iframe>
</body>
<%}catch(Exception eeee){
	System.out.println("error:::"+eeee.toString());
}
%>

</html>