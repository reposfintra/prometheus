<%--
    Document   : solicitud_equipos
    Created on : 24/09/2011, 09:36:47 AM
    Author     : ivargas
--%>


<%@page import="com.tsp.util.Util"%>
<%@page import="java.text.SimpleDateFormat"%>
<%@page import="java.text.DateFormat"%>
<%@page session="true"%>
<%@page import="java.util.*"%>
<%@page import="com.tsp.operation.model.*"%>
<%@page import="com.tsp.operation.model.beans.*"%>
<%@page import="com.tsp.operation.model.services.*"%>
<%@page import="com.tsp.opav.model.services.ClientesVerService"%>
<%@page import="com.tsp.opav.model.services.GestionSolicitudAiresAAEService"%>
<%@include file="/WEB-INF/InitModel.jsp"%>
<%

            response.addHeader("Cache-Control", "no-cache");
            response.addHeader("Pragma", "No-cache");
            response.addDateHeader("Expires", 0);
            Usuario usuario = (Usuario) session.getAttribute("Usuario");
            GestionSolicitudAvalService gsaserv = new GestionSolicitudAvalService(usuario.getBd());
            WSHistCreditoService wsdatacred = new WSHistCreditoService(usuario.getBd());
            GestionSolicitudAiresAAEService sasrv = new GestionSolicitudAiresAAEService(usuario.getBd());
            GestionConveniosService gcserv = new GestionConveniosService(usuario.getBd());
            ArrayList ejecutivo_cta = sasrv.buscarEjecutivosAAAE();
            ArrayList optdep = gsaserv.busquedaGeneral("DEPT_AAAE");
            ArrayList est_civil = gsaserv.busquedaGeneral("ESTCIV");
            ArrayList proveedor = sasrv.buscarContratistasAAAE("P");
            ArrayList instalador = sasrv.buscarContratistasAAAE("I");
            ArrayList equipos = sasrv.buscarMaterialesAAAE("M");
            ArrayList listadoResponsables = sasrv.busquedaGeneralRef("RESPONSABL", "AAAE");
            ArrayList tipos_solicitud = sasrv.getTiposSolicitud();
            ArrayList zona = gsaserv.busquedaGeneral("ZONA_AAAE");
            ArrayList convenios = new ArrayList();
            String vista = request.getParameter("vista") != null ? request.getParameter("vista") : "";
            String contratista = sasrv.asignarContratista();
            int num_solicitud =  Integer.parseInt(gsaserv.numeroSolc());
            String[] rangoHoras = sasrv.rangoHoras().split(";");
            convenios = gcserv.buscarConveniosTipo("Multiservicio", false);
            ArrayList<Codigo> tipo_vivienda = wsdatacred.buscarTabla("H", "vivienda");
            DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
            Date dat = new Date();
            String fec_act = dateFormat.format(dat);
            String visita_pla = "";
            if (rangoHoras != null) {
                visita_pla = Util.fechaMasDias(fec_act, Integer.parseInt(rangoHoras[2]));           
            }
            String[] dato1 = null;
            try {
            } catch (Exception e) {
                System.out.println("Error al buscar listados: " + e.toString());
                e.printStackTrace();
            }
%>
<%@taglib uri="http://jakarta.apache.org/taglibs/input-0.90" prefix="input" %>
<%@page contentType="text/html" pageEncoding="ISO-8859-1"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">
<!-- This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>. -->
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
        <link href="<%= BASEURL%>/css/estilostsp.css" rel='stylesheet'>
        <link href="<%=BASEURL%>/css/mac_os_x.css" rel="stylesheet" type="text/css">
        <link href="<%=BASEURL%>/css/default.css" rel="stylesheet" type="text/css">
        <style type="text/css">
            td{ white-space:nowrap;}
            b{
                color:white;
            }

            b{
                color:white;
            }

            .filableach{
                color: #000000;
                font-family: Tahoma, Arial;
                background-color:#FFFFFF;
                font-size: 12px;
            }

            .filanic{
                background-color:#C9EBC9;
                color: #000000;
                font-family: Tahoma, Arial;
                font-size: 12px;
            }

            .divs {
                font-family: Verdana, Tahoma, Arial, Helvetica, sans-serif;
                font-size: small;
                color: #000000;
                background-color:#006C3A;
                -moz-border-radius: 0.7em;
                -webkit-border-radius: 0.7em;
            }
            input.validation-failed, textarea.validation-failed {
                border: 1px solid #FF3300;
                color : #FF3300;
            }
        </style>
        <script type='text/javascript' src="<%= BASEURL%>/js/boton.js"></script>
        <script type='text/javascript' src="<%= BASEURL%>/js/prototype.js"></script>
        <script src="<%=BASEURL%>/js/effects.js" type="text/javascript"></script>
        <script src="<%=BASEURL%>/js/window.js" type="text/javascript"></script>
        <script src="<%=BASEURL%>/js/solicitud_equipo.js" type="text/javascript"></script>
        <script src="<%=BASEURL%>/js/validation.js" type="text/javascript" ></script>
        <script type="text/javascript">
            var valid;
            var fily = 0;
            valid = new Validation('formulario', {immediate : true, onFormValidate : formCallback});
            function formCallback(result, form) {
                window.status = "valiation callback for form '" + form.id + "': result = " + result;
            }
            function delrow(row){
                fily -= 1;
                var myTable = $("materiales");
                myTable.deleteRow(myTable.rows.length-1);
                getPrecioVenta('equipo');
                getPrecioVenta('instalacion');
            }
            function insertcelda(rowobject,dato){
                var celdita = null;
                if(navigator.appName=='Microsoft Internet Explorer') celdita = rowobject.insertCell();
                else celdita = rowobject.insertCell(-1);
                celdita.innerHTML = dato;
                return celdita;
            }

            function addrow(fit){
                fily += 1;
                fit = fily;
                fit -= 1;
                var celda = '<img alt="+" style="cursor: pointer" onclick="addrow('+fily+');" src="<%=BASEURL%>/images/botones/iconos/mas.gif" width="12" height="12" >'
                celda += '<img alt="-"style="cursor: pointer" onclick="delrow(this);" src="<%=BASEURL%>/images/botones/iconos/menos1.gif" width="12" height="12" >'
                var myTable = $("materiales");
                var myRow = null;
                if(navigator.appName=='Microsoft Internet Explorer') myRow = myTable.insertRow();
                else myRow = myTable.insertRow(-1);
                myRow.className = "fila";
                var myCell = null;
                myCell = insertcelda(myRow,celda);

                celda = '<select id="equipo'+fily+'" name="equipo'+fily+'" style="width:90%;" onchange="cargarInstalaciones('+fily+');getPrecioVenta('+"'equipo',"+fily+");getPrecioVenta('instalacion',"+fily+');">' +
                    '<option selected value="">...</option>'+
            <%
                        dato1 = null;
                        for (int i = 0; i < equipos.size(); i++) {
                            dato1 = ((String) equipos.get(i)).split(";_;");
            %>
                    '<option value="<%=dato1[0]%>" ><%=dato1[1]%></option>' +
            <%
                        }

            %>
                    '</select>';
                    myCell = insertcelda(myRow,celda);
                    celda = '<div id="d_inst'+fily+'"><select id="instalacion'+fily+'" name="instalacion'+fily+'" style="width:90%;" onchange="getPrecioVenta('+"'instalacion',"+fily+");"+'">' +
                        '<option value="">...</option>'+
                        '</select>';
                    myCell = insertcelda(myRow,celda);
                    celda = ' <input type="text" id="cantidad'+fily+'"  name="cantidad'+fily+'"  onblur="getPrecioVenta('+"'equipo',"+fily+');getPrecioVenta('+"'instalacion',"+fily+');'+'"  onkeyup="soloNumeros(this.id);" size="15" value="1" >';
                    myCell = insertcelda(myRow,celda);
        
          
                    celda = '<input type="text" id="valor_equipo'+fily+'"  name="valor_equipo'+fily+'"   size="15" value="0.0" readonly >'+
                        '<input type="hidden" id="nmb_equipo'+fily+'"  name="nmb_equipo'+fily+'" value="">';
                    myCell = insertcelda(myRow,celda);
                    celda = '<input type="text" id="valor_instalacion'+fily+'" name="valor_instalacion'+fily+'"  size="15" value="0.0"  readonly>'+
                        '<input type="hidden" id="nmb_instalacion'+fily+'"  name="nmb_instalacion'+fily+'" value="">';
                    myCell = insertcelda(myRow,celda);
                   
                    
                   

                }

        </script>
        <title>Solicitud de Equipo</title>
    </head>
    <body  onload="varcontropav('<%=CONTROLLEROPAV%>');varcontr('<%=CONTROLLER%>');cargarHoras();cargarCiudades('dep_nat', 'ciu_nat');">
        <div id="capaSuperior" style="position:absolute; width:100%; height:100px; z-index:0; left: 0px; top: 0px;">

            <jsp:include page="/toptsp.jsp?encabezado=Solicitud de Equipo"/>
        </div>
        <div id="capaCentral" style="position:absolute; width:100%; height:83%; z-index:0; left: 0px; top: 100px; " align="center">
            <div id="contenido2" style=" display: none; height: 150px; width: 500px; background-color: #EEEEEE;"></div>

            <form id="formulario" name="formulario" action="" method="post" >

                <div  id="contenido" class="divs" style="width:95%; text-align: left;"   >

                    <b><br></b>
                    <b>INFORMACION&Oacute;N DEL CLIENTE</b><br>
                    <table id="tnatural" style="border-collapse:collapse; width:100%" border="1">
                        <tr class="filanic">
                            <td  width="25%">
                                Nic<br>
                                <input type="text" id="nic" name="nic" value="" style="text-transform: uppercase;" size="25" >
                                <img alt="buscar" src="<%=BASEURL%>/images/botones/iconos/lupa.gif" width="15" height="15" style="cursor:pointer;" onclick="busqueda('nic');">

                            </td>
                            <td style="">
                                Nombre titular<br>
                                <input type="text" id="titular" name="titular" style="color:#33ccff;text-transform: uppercase;" value="" size="50" readonly >
                            </td>
                            <td width="25%">
                                Puntaje<br>
                                <input type="text" id="puntaje" name="puntaje" style="text-transform: uppercase;" value="" size="10" readonly >
                            </td>
                            <td>
                                Telefono<br>
                                <input type="text" id="tel_tit" name="tel_tit" style="text-transform: uppercase;" value="" size="15"  readonly>
                            </td>
                        </tr>
                        <tr class="filaazul">
                            <td width="25%">
                                Tipo<br>
                                <select id="tipo_p" name="tipo_p" style="width:12em;" onchange="tipopersona()">
                                    <option value="natural" >Persona natural</option>
                                    <option value="juridica">Persona juridica</option>
                                </select>&nbsp;
                            </td>
                            <td width="25%">
                                Identificacion<br>
                                <input type="text" id="id" onkeyup="soloNumeros(this.id);"  name="id" value="" size="20" >
                                <img alt="buscar" src="<%=BASEURL%>/images/botones/iconos/lupa.gif" width="15" height="15" style="cursor:pointer;" onclick="busqueda('id');">
                            </td>
                            <td>
                                Fecha de nacimiento (yyyy-MM-dd)<br>
                                <input type="text" id="f_nac_nat" class="validate-date" name="f_nac_nat" onblur="validarEdad(this.id, 'Deudor')" value="" size="15" >
                            </td>
                            <td>
                                Estado civil<br>
                                <select id="est_civil_nat" name="est_civil_nat">
                                    <option value="">...</option>
                                    <%  dato1 = null;
                                                for (int i = 0; i < est_civil.size(); i++) {
                                                    dato1 = ((String) est_civil.get(i)).split(";_;");%>
                                    <option value="<%=dato1[0]%>" ><%=dato1[1]%></option>
                                    <%  }%>
                                </select>
                            </td>
                        </tr>
                        <tr class="filaazul" id="natural">
                            <td id="tdpr_apellido_nat">
                                Primer apellido<br>
                                <input type="text" id="pr_apellido_nat" name="pr_apellido_nat" style="text-transform: uppercase;" value="" size="25" >
                            </td>
                            <td class="filableach" id="tdseg_apellido_nat"  >
                                Segundo Apellido<br>
                                <input type="text" id="seg_apellido_nat" name="seg_apellido_nat" style="text-transform: uppercase;" value="" size="25">
                            </td>
                            <td id="tdpr_nombre_nat"  >
                                Primer Nombre<br>
                                <input type="text" id="pr_nombre_nat" name="pr_nombre_nat" style="text-transform: uppercase;" value="" size="25">
                            </td>
                            <td class="filableach" id="tdseg_nombre_nat" >
                                Segundo Nombre<br>
                                <input type="text" id="seg_nombre_nat" name="seg_nombre_nat" style="text-transform: uppercase;" value="" size="25">
                            </td>
                        </tr>
                        <tr class="filaazul" style="visibility:hidden;display:none; " id="juridica">
                            <td colspan="2" id="tdrazon_social"   >
                                Raz&oacute;n Social<br>
                                <input type="text" id="razon_social" name="razon_social" value="" style="text-transform: uppercase;" size="60">
                            </td>
                            <td colspan="2" style="" id="tdrep_legal">
                                Representante legal<br>
                                <input type="text" id="rep_legal" name="rep_legal" value="" style="text-transform: uppercase;" size="60">
                            </td>
                        </tr>
                        <tr class="filaazul">
                            <td>
                                Tipo de vivienda<br>
                                <select id="tipo_viv_nat" name="tipo_viv_nat" style="width:10em;">
                                    <option value="">...</option>
                                    <% for (int i = 0; i < tipo_vivienda.size(); i++) {%>
                                    <option value="<%=tipo_vivienda.get(i).getCodigo()%>"  ><%=tipo_vivienda.get(i).getValor()%></option>
                                    <%  }%>
                                </select>
                            </td>
                            <td>
                                Direccion residencia<br>
                                <input type="text" id="dir_nat" name="dir_nat" value="" style="text-transform: uppercase;" onblur="dircobro(this.id, 'dir_cob_nat');" size="60">
                            </td>
                            <td>
                                Departamento<br>
                                <select id="dep_nat" name="dep_nat" style="width:20em;" onchange="cargarCiudades('dep_nat', 'ciu_nat');">
                                                         
                                    <% dato1 = null;
                                    if (optdep.size()>0){
                                        
                                                for (int i = 0; i < optdep.size(); i++) {
                                                    dato1 = ((String) optdep.get(i)).split(";_;");%>
                                    <option value="<%=dato1[0]%>"  ><%=dato1[1]%></option>
                                    <%  }  }else{%>
                                    <option value="">...</option>
                                    <%}%>
                                </select>
                            </td>
                            <td>
                                Ciudad<br>
                                <div id="d_ciu_nat">
                                    <select id="ciu_nat" name="ciu_nat" style="width:20em;">
                                        <option value="">...</option>
                                    </select>
                                </div>
                            </td>
                        </tr>
                        <tr class="filaazul">
                            <td class="filanic">
                                Estrato<br>
                                <select name="estr_nat" id="estr_nat" style="width:10em;">
                                    <option value="">...</option>
                                    <%
                                                for (int i = 3; i < 7; i++) {
                                                    out.print("<option value='" + i + "' >" + i + "</option>");
                                                }
                                    %>
                                </select>
                            </td>
                            <td>
                                Telefono<br>
                                <input type="text" id="tel_nat" name="tel_nat" value="" size="15" onkeyup="soloNumeros(this.id);">
                            </td>
                            <td colspan="2" >Ejecutivo de Cuenta<br>
                                <select name='ejecutivo_cta' id="ejecutivo_cta" style="width:20em;">
                                    <option value="">...</option>
                                    <%  dato1 = null;
                                                for (int i = 0; i < ejecutivo_cta.size(); i++) {
                                                    dato1 = ((String) ejecutivo_cta.get(i)).split(";_;");%>
                                    <option value="<%=dato1[0]%>" ><%=dato1[1]%></option>
                                    <%  }%>
                                </select>
                            </td>                           
                        </tr>
                        <tr class="filaazul">
                            <td>
                                Gastos por arriendo o cuota de vivienda<br>
                                $ <input type="text" id="arr_nat" name="arr_nat" value="" size="25" onkeyup="soloNumeros(this.id);" onblur="formato(this, 0)">
                            </td>
                            <td>
                                Salario/Mesada/Ingreso mes<br>
                                $ <input type="text" id="sal_nat" name="sal_nat" value="" size="25" onkeyup="soloNumeros(this.id);" onblur="formato(this, 0)">
                            </td>
                            <td class="filableach" >
                                Otros ingresos<br>
                                $ <input type="text" id="otros_nat" name="otros_nat" value="" size="25" onkeyup="soloNumeros(this.id);" onblur="formato(this, 0)">
                            </td>

                            <td class="filableach">
                                Salario/Mesada/Ingreso mes del Conyuge<br>
                                $ <input type="text" id="emp_sal_con_nat" name="emp_sal_con_nat" value="" size="25" onkeyup="soloNumeros(this.id);" onblur="formato(this, 0)">
                            </td>                           
                        </tr>
                    </table>

                    <table id="tsup" style="border-collapse:collapse; width:100%" border="1" >
                        <br>
                        <b>INFORMACI&Oacute;N DE LA SOLICITUD</b><br>
                        <tr class="filaazul">

                            <td width="25%">
                                Convenio<br>
                                <select id="convenio" name="convenio" style="width:95%;"   >
                                   
                                    <% if (convenios.size()>0){
                                                for (int j = 0; j < convenios.size(); j++) {
                                                    dato1 = ((String) convenios.get(j)).split(";_;");%>
                                    <option value="<%=dato1[0]%>"><%=dato1[1]%></option>
                                    <%  }}else{
                                    %>
                                     <option value="">...</option>
                                     <%}%>
                                </select>
                            </td>
                            <td>Tipo de Solicitud<br>
                                <select  name='distribucion' id="distribucion" onchange="tiposol();getPrecioVenta('equipo');getPrecioVenta('instalacion');" style="width:30em;">
                                  
                                    <%if (tipos_solicitud.size()>0){
                                                for (int i = 0; i < tipos_solicitud.size(); i++) {%>
                                    <option value="<%=((String[]) tipos_solicitud.get(i))[1]%>"><%=((String[]) tipos_solicitud.get(i))[0]%></option>
                                    <%}}else{%>
                                      <option value=''>...</option>
                                      <%}%>
                                </select>
                                <input type="hidden" id="tipo_solicitud"  name="tipo_solicitud" value="<%=tipos_solicitud.size()>0?(((String[]) tipos_solicitud.get(0))[0]):""%>">
                            </td>
                            <td width="25%" >
                                Fecha<br>
                                <input type="text" id="fecha_cons" name="fecha_cons" size="15" readonly="readonly"  value="<%= fec_act%> ">
                            </td>

                            <td width="25%">
                                No.Formulario<br>
                                <input type="text" id="num_form" name="num_form" value="<%=num_solicitud%>"  size="15" readonly>
                            </td>
                        </tr>


                        <tr class="filaazul">

                            <td width="25%">
                                Proveedor<br>
                                <select id="proveedor" name="proveedor" style="width:95%;">
                                    <option value="">...</option>
                                    <%  dato1 = null;
                                                for (int i = 0; i < proveedor.size(); i++) {
                                                    dato1 = ((String) proveedor.get(i)).split(";_;");%>
                                    <option value="<%=dato1[0]%>" ><%=dato1[1]%></option>
                                    <%  }%>
                                </select>
                            </td>
                            <td>
                                Instalador<br>
                                <select id="instalador" name="instalador" style="width:95%;" <%=vista.equals("2") ? "" : "disabled"%> onChange="cargarHoras();">
                                    <option value="">...</option>
                                    <%  dato1 = null;
                                                for (int i = 0; i < instalador.size(); i++) {
                                                    dato1 = ((String) instalador.get(i)).split(";_;");%>
                                    <option value="<%=dato1[0]%>" <%=dato1[0].equals(contratista) ? "selected" : ""%> ><%=dato1[1]%></option>
                                    <%  }%>
                                </select>
                                <input type="hidden" id="contratista"  name="contratista" value="<%=contratista%>">
                            </td>

                            <td width="25%">
                                Distrito<br>
                                <select name='zona' id="zona"  style="width:20em;">
                                  
                                    <% if (zona.size()>0){ for (int i = 0; i < zona.size(); i++) {
                                                    dato1 = ((String) zona.get(i)).split(";_;");%>
                                    <option value="<%=dato1[0]%>" ><%=dato1[1]%></option>
                                    <%}}else{%>
                                      <option value="">...</option>
                                      <%}%>
                                </select>
                            </td>

                            <td>
                                Responsable<br>
                                <select name='responsable' id="responsable" style="width:30em;">
                                    <option value="">...</option>
                                    <%for (int i = 0; i < listadoResponsables.size(); i++) {
                                                    dato1 = ((String) listadoResponsables.get(i)).split(";_;");%>
                                    <option value="<%=dato1[0]%>" ><%=dato1[1]%></option>
                                    <%}%>
                                </select>
                            </td>
                        </tr>
                        <tr class="filaazul">

                            <td colspan="4">
                                <input type="hidden" id="filas"  name="filas" value="">
                                <table id="materiales" style="text-align: left; width: 100%; " border="0">
                                    <tr class="subtitulo1" style=" background-color:#006C3A;">

                                        <th style="vertical-align: top; width:4%;">&nbsp;</th>

                                        <th style="vertical-align: top; width: 28%;">Equipos
                                        </th>
                                        <th style="vertical-align: top; width: 28%;">Instalaciones
                                        </th>
                                        <th style="vertical-align: top; width: 10%;">Cantidad
                                        </th>
                                        <th style="vertical-align: top; width: 10%;">$ Valor Equipos
                                        </th>
                                        <th style="vertical-align: top; width: 10%;">$ Valor Instalaciones
                                        </th>
                                    </tr>
                                    <tbody>

                                        <tr class="fila">
                                            <td style="vertical-align: top; width: 26px;">
                                                <img alt="+" style="cursor: pointer" onclick="addrow('0');" src='<%=BASEURL%>/images/botones/iconos/mas.gif' width='12' height='12' >
                                            </td>
                                            <td>
                                                <select id="equipo0" name="equipo0" style="width:90%;" onchange="cargarInstalaciones('0');getPrecioVenta('equipo','0');getPrecioVenta('instalacion', '0')">
                                                    <option value="">...</option>
                                                    <%  dato1 = null;
                                                                for (int i = 0; i < equipos.size(); i++) {
                                                                    dato1 = ((String) equipos.get(i)).split(";_;");%>
                                                    <option value="<%=dato1[0]%>" ><%=dato1[1]%></option>
                                                    <%  }%>
                                                </select>
                                            </td>
                                            <td>
                                                <div id="d_inst0">
                                                    <select id="instalacion0" name="instalacion0" style="width:90%;" onchange="getPrecioVenta('instalacion', '0')">
                                                        <option value="">...</option>
                                                    </select>
                                                </div>
                                            </td>
                                            <td>
                                                <input type="text" id="cantidad0"  name="cantidad0"  onblur="getPrecioVenta('equipo','0');getPrecioVenta('instalacion', '0');"   onkeyup="soloNumeros(this.id);"  size="15" value="1" >
                                            </td>
                                            <td>
                                                <input type="text" id="valor_equipo0"  name="valor_equipo0"   size="15" value="0.0" readonly >
                                                <input type="hidden" id="nmb_equipo0"  name="nmb_equipo0" value="">
                                            </td>
                                            <td>
                                                <input type="text" id="valor_instalacion0" name="valor_instalacion0"  size="15" value="0.0"  readonly>
                                                <input type="hidden" id="nmb_instalacion0"  name="nmb_instalacion0" value="">
                                            </td>

                                        </tr>
                                    </tbody>

                                </table>
                            </td>
                        </tr>


                        <tr class="filaazul">

                            <td>
                                Fecha visita planeada (yyyy-MM-dd)<br>
                                <input type="text" id="f_vis_plan" class="validate-date" name="f_vis_plan" value="<%=visita_pla%>" size="15" onblur="validarFechaPla();" >
                                <input type="hidden" id="visita_pla"  name="visita_pla" value="<%=visita_pla%>">

                            </td>
                            <td>  Hora visita planeada<br>
                                <div id="d_hora" style=" display: inline " >
                                    <select name='hora' id="hora" onchange="puedeHacerVisita()" >
                                        <option value="">...</option>
                                    </select>
                                </div>
                            </td>

                            <td>
                                Plazo&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Valor cuota $<br>
                                <input type="text" id="plazo"  name="plazo"  onblur="valorFinanciado();"  size="2" value="6" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                <input type="text" id="cuota"  name="cuota"   size="15" readonly >
                            </td>

                            <td>
                                Valor sin financianci&oacute;n $&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Valor solicitado $<br>
                                <input type="text" id="valor_sin_fin"  name="valor_sin_fin"   size="15" value=""  readonly>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                <input type="text" id="valor_solicitado"  name="valor_solicitado"   size="15" value=""  readonly>
                            </td>
                        </tr>
                        <tr class="filaazul">                           
                            <td colspan="4" >
                                Situaci&oacute;n<br>
                                <textarea rows="1" cols="100"   id="situacion" name="situacion"  style="width:95%;text-transform: uppercase;"></textarea>
                            </td>
                        </tr>
                    </table>
                    <table id="tsup" style="border-collapse:collapse; width:100%" border="1" >
                        <br>
                        <b>INFORMACI&Oacute;N TECNICA DEL EQUIPO</b><br>

                        <tr class="filaazul">
                            <td colspan="2" width="50%" >
                                1. �Cu�l es el �rea aproximada en metros cuadrados (incluida altura) que desea acondicionar � enfriar?<br>
                                <input type="text" id="pregunta1" name="pregunta1"  size="100%" style="text-transform: uppercase;" value="">
                            </td>
                            <td colspan="2">
                                2. �Qu� actividad realiza en esa �rea?<br>
                                <input type="text" id="pregunta2" name="pregunta2"   size="100%" style="text-transform: uppercase;" value="">
                            </td>
                        </tr>
                        <tr class="filaazul">

                            <td colspan="2">
                                3. �Cu�ntas personas est�n en esa �rea?<br>
                                <input type="text" id="pregunta3" name="pregunta3"   size="100%" style="text-transform: uppercase;" value="">
                            </td>
                            <td colspan="2">
                                4. �Qu� equipos tiene instalado en el recinto (sitio)?<br>
                                <input type="text" id="pregunta4"  name="pregunta4"    size="100%" style="text-transform: uppercase;" value="">
                            </td>
                        </tr>
                        <tr class="filaazul">
                            <td colspan="4">
                                5. Qu� temperatura en grados cent�grados desea?<br>
                                <input type="text" id="pregunta5" name="pregunta5"    size="100%" style="text-transform: uppercase;" value="">
                            </td>
                        </tr>

                    </table>
                </div>
                <br>

            </form>
            <br>
            <div align="center">
                <img alt="" src = "<%=BASEURL%>/images/botones/aceptar.gif" style="cursor:pointer; " onclick="validar_solicitud_equipos();" name= "imgaceptar" id="imgaceptar"  onMouseOver="botonOver(this);" onMouseOut="botonOut(this);">

                <img alt="" src = "<%=BASEURL%>/images/botones/salir.gif" style = "cursor:pointer" name = "imgsalir" onclick = "window.close();" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);">
            </div>
        </div>
    </body>
</html>






