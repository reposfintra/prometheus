<%-- 
    Document   : fintraSoluciones
    Created on : 15/11/2016, 04:32:41 PM
    Author     : mariana
--%>

<%@page contentType="text/html" pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
        <link type="text/css" rel="stylesheet" href="./js/sweetalert/sweetalert.css"/>
        <link href="./css/popup.css" rel="stylesheet" type="text/css">
        <link type="text/css" rel="stylesheet" href="./css/jquery/jquery-ui/jquery-ui.css" />
        <link type="text/css" rel="stylesheet" href="./css/TransportadorasApi.css " />
        <script type="text/javascript" src="./js/sweetalert/sweetalert.min.js"></script>
        <script type="text/javascript" src="./js/jquery/jquery.jqGrid/js/jquery.min.js"></script>
        <script type="text/javascript" src="./js/jquery-ui-1.8.5.custom.min.js"></script>   
        <script type="text/javascript" src="./js/jquery/jquery.jqGrid-4.7.0/js/grid.locale-es.js"></script>
        <script type="text/javascript" src="./js/jquery/jquery.jqGrid-4.7.0/js/jquery.jqGrid.min.js"></script>
        <script type="text/javascript" src="./js/jquery/jquery.jqGrid-4.7.0/js/jquery.tabletojson.js"></script>    
        <script type="text/javascript" src="./js/jquery/jquery.jqGrid-4.7.0/plugins/jquery.contextmenu.js"></script>
        <script type="text/javascript" src="./js/fintraSoluciones.js"></script> 
        <link type="text/css" rel="stylesheet" href="./css/loaderApi.css">
        <script type="text/javascript" src="./js/highcharts/highcharts.js"></script>
        <script type="text/javascript" src="./js/highcharts/data.js"></script>
        <title>FINTRA SOLUCIONES</title>
    </head>
    <body>
        <div id="capaSuperior" style="position:absolute; width:100%; height:100px; z-index:0; left: 0px; top: 0px;">
            <jsp:include page="/WEB-INF/toptsp.jsp?encabezado=FINTRA SOLUCIONES"/>
        </div>
        <style>
            ul.tab{
                margin-top:120px;
                width: 1591px;
                margin-left: 3%;               

            }
            #gbox_tabla_fintra_soluciones{
                margin-top: 134px; 
            }
        </style>
        <ul class="tab">
            <li><a href="#" class="tablinks" id="atpaso1" onclick="openTAb(event, 'paso1')">Nacimiento Multiservicio</a></li>
            <!--        <li><a href="#" class="tablinks" id="atpaso2" onclick="openTAb(event, 'paso2')">2nd Step</a></li>
                        <li><a href="#" class="tablinks" id="atpaso3" onclick="openTAb(event, 'paso3')">3rd Step</a></li>
                        <li><a href="#" class="tablinks" id="atpaso4" onclick="openTAb(event, 'paso4')">4th Step</a></li>
                        <li><a href="#" class="tablinks" id="atpaso5" onclick="openTAb(event, 'paso5')">5th Step</a></li>
                        <li><a href="#" class="tablinks" id="atpaso6" onclick="openTAb(event, 'paso6')">6th Step</a></li>-->
            <li style='display:block; float:right; padding-right:10px; padding-top: 2px; cursor:pointer;'>
                <img  src='/fintra/images/help_.png' id='wast'  title ='Help'/>
            </li>
        </ul>
        <div id="tablita" class="ui-jqgrid ui-widget ui-widget-content center-block" style="width: 290px; height-min: 300px;margin-top: -104px;margin-left: 67px;;padding: 8px;display: none">
            <table id="tablainterna" style="height: 53px; width: 274px"  >
                <tr>
                    <td>
                        <label>Filter:</label>
                    </td>
                    <td>
                        <select id="filtro_" style="width: 133px;" onchange="busqueda_filtro()">
                            <option value="aniocorrido">This Year</option>
                            <option value="anioanterior">Last Year</option>                           
                            <option value="mespresente">This Month</option>
                            <option value="mespasado">Last Month</option>
                            <option value="seismeses">Last 6 Months</option>
                            <option value="docemeses">Last 12 Months</option>
                            <option value="otro">Custom Filter</option>
                        </select>
                    </td>
                    <td>
                        <label  class='f_otro' style="display: none">Periodo Inicio</label>
                    </td>
                    <td>
                        <input id="periodo_inicio" class='f_otro' type="text"  style="width: 110px;display: none" onKeyPress="return soloNumeros(event)"  >
                    </td>
                    <td>
                        <label  class='f_otro' style="display: none">Periodo Fin</label>
                    </td>
                    <td>
                        <input id="periodo_fin" class='f_otro' type="text"  style="width: 110px;display: none"onKeyPress="return soloNumeros(event)"  >
                    </td>
                    <td>
                        <hr style="width: 2px;height: 37px;top: -25px;margin-left: 13px;color: rgba(128, 128, 128, 0.39);" /> 
                    </td>
                    <td>
                        <div style="padding-top: 10px">
                            <center>
                                <button id="buscar" class="ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only" style="margin-left: 15px;top: -7px;" > 
                                    <span class="ui-button-text">Search</span>
                                </button>  
                            </center>
                        </div>
                    </td>
                    <td>
                        <input id="paso_busqueda"  type="text"  style="width: 110px;margin-top: -22px;display: none" hidden >
                    </td>
                    <td>
                        <input id="modulo"  type="text" value="FINTRA_SOLUCIONES" style="width: 110px;margin-top: -22px;display: none" readonly >
                    </td>
                </tr>
            </table>
            <!--<img src="/fintra/images/logo_selectrik2.png" alt="" class='loaderimg' style="width: 126px;height: 88px;"/>-->
        </div>
        <div id="paso1" class="tabcontent">
            <div id="izq" style="height: 0px;">
                <table id="tabla_fintra_soluciones" class="tablas" ></table>
                <div id="pager"></div>
            </div>
        </div>
        <div id="divSalidaEx" title="Exportacion" style=" display: block" >
            <p  id="msjEx" style=" display:  none"> Espere un momento por favor...</p>
            <center>
                <img id="imgloadEx" style="position: relative;  top: 7px; display: none " src="./images/cargandoCM.gif"/>
            </center>
            <div id="respEx" style=" display: none"></div>
        </div> 

        <div id="dialogMsjHelp" title="Help" class="ventana" >
            <table id="tablainterna" style="height: 53px; width: 273px"  >
                <tr>
                    <td>
                        <label hidden>Id</label>
                    </td>
                    <td>
                        <input id="id" type="text"  style="width: 110px;" readonly hidden>
                    </td>
                    <td>
                        <label>Modulo</label>
                    </td>
                    <td>
                        <input id="modulo_" type="text"  style="width: 205px;"readonly>
                    </td>
                    <td>
                        <label>Paso</label>
                    </td>
                    <td>
                        <input id="paso_" type="text"  style="width: 110px;"readonly>
                    </td>
                </tr>
            </table>
            <br>
            <br>
            <fieldset class="scheduler-border" style="width: 100px;border-color:#B6ADAB66;" >
                <legend class="scheduler-border" >Descripcion de la ayuda</legend>
                <textarea id="textoArea" cols="106" rows="34" style="width: 501px;height: 130px;"></textarea>
            </fieldset>
        </div>

        <div id="loader-wrapper" style="display: none">
            <img src="/fintra/images/logo_selectrik2_.png" alt="" class='loaderimg' style="width: 200px;height: 109px;"/>
            <div id="loader">          </div>
            <div class="loader-section section-left"></div>
            <div class="loader-section section-right"></div>
        </div>
    </body>
</html>
