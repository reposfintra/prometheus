<%-- 
    Document   : seguimientoCaja
    Created on : 22/10/2016, 12:06:02 PM
    Author     : mariana
--%>

<%@page contentType="text/html" pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
        <link type="text/css" rel="stylesheet" href="./js/sweetalert/sweetalert.css"/>
        <link href="./css/popup.css" rel="stylesheet" type="text/css">
        <link type="text/css" rel="stylesheet" href="./css/jquery/jquery-ui/jquery-ui.css" />
        <link type="text/css" rel="stylesheet" href="./css/TransportadorasApi.css " />
        <script type="text/javascript" src="./js/sweetalert/sweetalert.min.js"></script>
        <script type="text/javascript" src="./js/jquery/jquery.jqGrid/js/jquery.min.js"></script>
        <script type="text/javascript" src="./js/jquery-ui-1.8.5.custom.min.js"></script>   
        <script type="text/javascript" src="./js/jquery/jquery.jqGrid-4.7.0/js/grid.locale-es.js"></script>
        <script type="text/javascript" src="./js/jquery/jquery.jqGrid-4.7.0/js/jquery.jqGrid.min.js"></script>
        <script type="text/javascript" src="./js/jquery/jquery.jqGrid-4.7.0/js/jquery.tabletojson.js"></script>    
        <script type="text/javascript" src="./js/jquery/jquery.jqGrid-4.7.0/plugins/jquery.contextmenu.js"></script>
        <script type="text/javascript" src="./js/seguimientoCaja.js"></script> 
        <link type="text/css" rel="stylesheet" href="./css/loaderApi.css">
        <title>POSICION DE CAJA</title>
    </head>
    <body>
        <div id="capaSuperior" style="position:absolute; width:100%; height:100px; z-index:0; left: 0px; top: 0px;">
            <jsp:include page="/WEB-INF/toptsp.jsp?encabezado=POSICION DE CAJA"/>
        </div>
        <style>
            .editable{
                width: 90px !important;
            }

            .ms-options-wrap > .ms-options > ul li.selected label,
            .ms-options-wrap > .ms-options > ul label:hover {
                background-color: #fff;
            }

            .ms-options-wrap > button {
                width: 200px;
                height: 27px;
                color: #0b0a0a !important;
            }
            .ms-options-wrap{
                width: 135px;
                height: 27px;
            }

            .ms-options > ul > li > label{
                padding-left: 23px !important;
                padding-top: 10px !important;
                margin-left: -35px !important;
            }

            .ms-options > ul > li {
                list-style: none;
            }
            .ms-options {
                width: 250px !important;
                max-height: 200px!important;
            }

            .ms-options-wrap > .ms-options > ul li.selected label, .ms-options-wrap > .ms-options > ul label:hover {
                background-color: RGBA(42, 136, 200, 0.12)!important;
                color: #2A88C8 !important;
            }

            .ms-options-wrap > .ms-options > ul input[type="checkbox"]{
                left: -37px !important;
            }

        </style>
    <center>
        <div id="tablita" class="ui-jqgrid ui-widget ui-widget-content center-block" style="width: 690px;margin-top: 31px;margin-left: 67px;">
            <div class="ui-dialog-titlebar ui-widget-header ui-helper-clearfix">
                <span class="ui-dialog-title text-center" id="ui-dialog-title-listaGlobal">
                    &nbsp;
                    <label class="titulotablita" ><b>Filtro</b></label>
                </span>
            </div>
            <table id="tablainterna" style="height: 53px; width:675px"  >
                <tr>
                    <td>
                        <label>A�o:</label>
                    </td>
                    <td>
                        <input type="text" id="anio" name="anio" style="height: 15px;width: 30px;color: #070708;">
                    </td>
                    <td>
                        <label>Fecha Movimiento:</label>
                    </td>
                    <td>
                        <input type="datetime" id="fecha" name="fecha" style="height: 15px;width: 67px;color: #070708;">
                    </td>
                    <td>
                        <label>Cuentas</label>
                    </td>
                    <td>
                        <select id="cuenta" style="width: 237px;"></select>
                    </td>
                    <td>
                        <hr style="width: 5px;height: 37px;top: -25px;margin-left: 13px;color: rgba(128, 128, 128, 0.39);" /> 
                    </td>
                    <td>
                        <div style="padding-top: 10px">
                            <center>
                                <button id="buscar_" class="ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only" style="margin-left: 15px;top: -7px;" > 
                                    <span class="ui-button-text">Buscar</span>
                                </button>  
                            </center>
                        </div>
                    </td>
                </tr>
            </table>
        </div>

        <div style="position: relative;top: 185px;">
            <table id="tabla_seguimiento_caja" ></table>
            <div id="pager"></div>
        </div>
        <div id="info"  class="ventana" >
            <p id="notific">EXITO AL GUARDAR</p>
        </div>

        <div id="divSalidaEx" title="Exportacion" style=" display: block" >
            <p  id="msjEx" style=" display:  none"> Espere un momento por favor...</p>
            <center>
                <img id="imgloadEx" style="position: relative;  top: 7px; display: none " src="./images/cargandoCM.gif"/>
            </center>
            <div id="respEx" style=" display: none"></div>
        </div> 

        <div id="dialogMsjMoviAux" class="ventana" >
            <fieldset class="scheduler-border" style="width: 100px;border-color:#B6ADAB66;" >
                <legend class="scheduler-border" >Descargar Movimiento Auxiliar</legend>
                <table id="tablainterna" style="height: 53px; width: 630px"  >
                    <tr>
                        <td>
                            <label>Cuentas</label>
                        </td>
                        <td>
                            <select name="cuentas[]" multiple id="cuentas" style="width: 200px;">
             
                            </select>
                        </td>
                        <td>
                            <label>Periodo Inicio</label>
                        </td>
                        <td>
                            <input id="periodoInicio" type="text" placeholder="AAAAMM"  style="width: 110px;"  class="solo-numero" maxlength="6">                        
                        </td>
                        <td>
                            <label>Periodo Fin</label>
                        </td>
                        <td>
                            <input id="periodoFin" type="text"  placeholder="AAAAMM" style="width: 110px;"  class="solo-numero" maxlength="6">
                        </td>
                    </tr>
                </table>
            </fieldset>

        </div>
        <div id="dialogMsj" title="Mensaje" style="display:none;">
            <p style="font-size: 12px;text-align:justify;" id="msj" > Texto </p>
        </div>    
    </center>
    <!--para el multiselect-->
    <script type="text/javascript" src="./js/multiselect/jquery.min.js"></script>
    <script type="text/javascript" src="./js/multiselect/jquery.multiselect.js"></script>
    <link type="text/css" rel="stylesheet" href="./js/multiselect/jquery.multiselect.css" />

</body>
</html>
