<%-- 
    Document   : analisisRecaudo
    Created on : 1/02/2017, 09:19:57 AM
    Author     : mariana
--%>

<%@page contentType="text/html" pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
        <link href="./css/popup.css" rel="stylesheet" type="text/css">
        <link type="text/css" rel="stylesheet" href="./css/jquery/jquery-ui/jquery-ui.css" />
        <link type="text/css" rel="stylesheet" href="./css/TransportadorasApi.css " />
        <script type="text/javascript" src="./js/jquery/jquery.jqGrid/js/jquery.min.js"></script>
        <script type="text/javascript" src="./js/jquery-ui-1.8.5.custom.min.js"></script>   
        <script type="text/javascript" src="./js/jquery/jquery.jqGrid-4.7.0/js/grid.locale-es.js"></script>
        <script type="text/javascript" src="./js/jquery/jquery.jqGrid-4.7.0/js/jquery.jqGrid.min.js"></script>
        <script type="text/javascript" src="./js/jquery/jquery.jqGrid-4.7.0/js/jquery.tabletojson.js"></script>  
        <script type="text/javascript" href="http://www.ddginc-usa.com/spanish/editcheck.js"></script>
        <script type="text/javascript" src="./js/analisisRecaudo.js"></script> 
        <title>ANALISIS DE RECAUDO</title>
    </head>
    <body>
        <div id="capaSuperior" style="position:absolute; width:100%; height:100px; z-index:0; left: 0px; top: 0px;">
            <jsp:include page="/WEB-INF/toptsp.jsp?encabezado=INVERSIONISTAS"/>
        </div>
    <center>
        <div id="tablita" class="ui-jqgrid ui-widget ui-widget-content center-block" style="width: 665px;margin-top: 31px;">
            <div class="ui-dialog-titlebar ui-widget-header ui-helper-clearfix">
                <span class="ui-dialog-title text-center" id="ui-dialog-title-listaGlobal">
                    <label class="titulotablita" ><b>Filtro</b></label>
                </span>
            </div>
            <table id="tablainterna" style="height: 53px; width:655px"  >
                <tr>
                    <td>
                        <label>Banco</label>
                    </td>
                    <td>
                        <select id="banco" style="width: 133px;">
                            <option value=""></option>
                            <option value="BANCOLOMBIA">BANCOLOMBIA</option>
                            <option value="BANCOLMBIA MC">BANCOLMBIA MC</option>
                            <option value="BCO COLPATRIA">BANCO COLPATRIA</option>
                            <option value="BANCO OCCIDENTE">BANCO OCCIDENTE</option>
                            <option value="CAJA TESORERIA">CAJA TESORERIA</option>
                            <option value="CAJA UNIATONOMA">CAJA UNIATONOMA</option>
                            <option value="FID COLP RECFEN">FENALCO ATLANTI</option>
                        </select>
                    </td>
                    <td>
                        <label> Fecha Consignacion</label>
                    </td>
                    <td>
                        <input id="fecha_inicio" type="text"  style="width: 110px;">
                    </td>
                    <td>
                        <input id="fecha_fin" type="text"  style="width: 110px;">
                    </td>
                    <td>
                        <hr style="width: 3px;height: 37px;top: -25px;margin-left: 13px;color: rgba(128, 128, 128, 0.39);" /> 
                    </td>
                    <td>
                        <div style="padding-top: 10px">
                            <center>
                                <button id="buscar" class="ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only" style="margin-left: 15px;top: -7px;" > 
                                    <span class="ui-button-text">Buscar</span>
                                </button>  
                            </center>
                        </div>
                    </td>
                </tr>
            </table>
        </div>
        <div id="info"  class="ventana" >
            <p id="notific">EXITO AL GUARDAR</p>
        </div>
        <div style="position: relative;top: 185px;">
            <table id="tabla_recaudos" ></table>
            <div id="pager"></div>
        </div>
    </center>
</body>
</html>
