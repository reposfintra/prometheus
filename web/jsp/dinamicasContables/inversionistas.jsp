<%-- 
    Document   : inversionistas
    Created on : 7/10/2016, 05:29:19 PM
    Author     : mariana
--%>

<%@page contentType="text/html" pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
        <link type="text/css" rel="stylesheet" href="./js/sweetalert/sweetalert.css"/>
        <link href="./css/popup.css" rel="stylesheet" type="text/css">
        <link type="text/css" rel="stylesheet" href="./css/jquery/jquery-ui/jquery-ui.css" />
        <link type="text/css" rel="stylesheet" href="./css/TransportadorasApi.css " />
        <script type="text/javascript" src="./js/sweetalert/sweetalert.min.js"></script>
        <script type="text/javascript" src="./js/jquery/jquery.jqGrid/js/jquery.min.js"></script>
        <script type="text/javascript" src="./js/jquery-ui-1.8.5.custom.min.js"></script>   
        <script type="text/javascript" src="./js/jquery/jquery.jqGrid-4.7.0/js/grid.locale-es.js"></script>
        <script type="text/javascript" src="./js/jquery/jquery.jqGrid-4.7.0/js/jquery.jqGrid.min.js"></script>
        <script type="text/javascript" src="./js/jquery/jquery.jqGrid-4.7.0/js/jquery.tabletojson.js"></script>    
        <script type="text/javascript" src="./js/jquery/jquery.jqGrid-4.7.0/plugins/jquery.contextmenu.js"></script>
        <script type="text/javascript" src="./js/inversionistas.js"></script> 
        <script type="text/javascript" src="./js/highcharts/highcharts.js"></script>
        <script type="text/javascript" src="./js/highcharts/data.js"></script>
        <link type="text/css" rel="stylesheet" href="./css/loaderApi.css">

        <title>INVERSIONISTAS</title>
    </head>
    <body>
        <div id="capaSuperior" style="position:absolute; width:100%; height:100px; z-index:0; left: 0px; top: 0px;">
            <jsp:include page="/WEB-INF/toptsp.jsp?encabezado=INVERSIONISTAS"/>
        </div>
        <style>
            ul.tab{
                margin-top:120px;
                width: 1591px;
                margin-left: 3%;               

            }
            #gbox_tabla_inversionista{
                margin-top: 134px; 
            }
            #gbox_tabla_inversionista2{
                margin-top: 134px; 
            }
            .ui-pg-div{
                margin-left: 16px!important;
            }
            .highcharts-credits{
                display:none;
            }
            .ui-pg-button ui-corner-all{
                width: 120px;
            }

        </style>
        <ul class="tab">
            <li><a href="#" class="tablinks" id="atpaso1" onclick="openTAb(event, 'paso1')">Registro Ingresos</a></li>
            <li><a href="#" class="tablinks" id="atpaso2" onclick="openTAb(event, 'paso2')">Salida Inversionistas</a></li>
            <li><a href="#" class="tablinks" id="atpaso3" onclick="openTAb(event, 'paso3')">Desembolso de Salida Inversionistas</a></li>
            <li><a href="#" class="tablinks" id="atpaso4" onclick="openTAb(event, 'paso4')">Causación Mensual Intereses</a></li>
            <li><a href="#" class="tablinks" id="atpaso5" onclick="openTAb(event, 'paso5')">Validación Pago Impuestos</a></li>
            <li style='display:block; float:right; padding-right:10px; padding-top: 2px; cursor:pointer;'>
                <img  src='/fintra/images/help_.png' id='wast'  title ='Help'/>
            </li>
        </ul>
        <div id="tablita" class="ui-jqgrid ui-widget ui-widget-content center-block" style="width: 621px; height-min: 300px;margin-top: -104px;margin-left: 67px;;padding: 8px;display: none">
            <table id="tablainterna" style="height: 53px; width: 273px"  >
                <tr>
                    <td>
                        <label>Filter:</label>
                    </td>
                    <td>
                        <select id="filtro_" style="width: 133px;" onchange="busqueda_filtro()">
                            <option value="aniocorrido">This Year</option>
                            <option value="anioanterior">Last Year</option>
                            <option value="mespasado">Last Month</option>
                            <option value="mespresente">This Month</option>
                            <option value="seismeses">Last 6 Months</option>
                            <option value="docemeses">Last 12 Months</option>
                            <option value="otro">Custom Filter</option>
                        </select>
                    </td>
                    <td>
                        <label  class='f_otro' style="display: none">Period beginning(YYYYMM)</label>
                    </td>
                    <td>
                        <input id="periodo_inicio" class='f_otro' type="text" maxlength="6"  style="width: 110px;display: none" onKeyPress="return soloNumeros(event)"  >
                    </td>
                    <td>
                        <label  class='f_otro' style="display: none">Period End(YYYYMM)</label>
                    </td>
                    <td>
                        <input id="periodo_fin" class='f_otro' maxlength="6" type="text"  style="width: 110px;display: none" onKeyPress="return soloNumeros(event)"  >
                    </td>
                    <td>
                        <label>Name</label>
                    </td>
                    <td>
                        <select id="tercero" style="width: 280px;"></select>
                    </td>
                    <td>
                        <hr style="width: 5px;height: 37px;top: -25px;margin-left: 13px;color: rgba(128, 128, 128, 0.39);" /> 
                    </td>
                    <td>
                        <div style="padding-top: 10px">
                            <center>
                                <button id="buscar" class="ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only" style="margin-left: 15px;top: -7px;" > 
                                    <span class="ui-button-text">Search</span>
                                </button>  
                            </center>
                        </div>
                    </td>
                    <td>
                        <input id="paso_busqueda"  type="text"  style="width: 110px;margin-top: -22px;display: none"  >
                    </td>
                    <td>
                        <input id="modulo"  type="text" value="INVERSIONISTA" style="width: 110px;margin-top: -22px;display: none" readonly >
                    </td>
                </tr>
            </table>
        </div>

        <div id="paso1" class="tabcontent">
            <div id="izq" style="height: 0px;margin-top: 136px;">
                <table id="tabla_inversionistaPaso1" class="tablas" ></table>
                <div id="pager"></div>
            </div>
            <div id="der" style="float: right;margin-right: 100px;margin-top: 25px; width:771px;">
                <div id="containerinversionistaPaso1" style="height: 400px;float: left;display: none"></div>
            </div>
        </div>
        <div id="paso2" class="tabcontent">
            <div id="izq" style="height: 0px;margin-top: 136px;">
                <table id="tabla_inversionistaPaso2"class="tablas" ></table>
                <div id="pager1"></div>
            </div>
            <div id="der"style="float: right;width:771px;margin-top: 154px;">
                <div id="containerinversionistaPaso2" style="height: 400px;float: left;margin-left: -93px;display: none"></div>
            </div>
        </div>
        <div id="paso3" class="tabcontent">
            <div id="izq" style="height: 0px;margin-top: 136px;">
                <table id="tabla_inversionistaPaso3" class="tablas" ></table>
                <div id="pager2"></div>
            </div>
            <div id="der"style="float: right;margin-top: 25px; width:771px;margin-top: 154px;">
                <div id="containerinversionistaPaso3" style="height: 400px;float: left;display: none"></div>
            </div>
        </div>
        <div id="paso4" class="tabcontent">
            <div id="izq" style="height: 0px;margin-top: 136px;">
                <table id="tabla_inversionistaPaso4" class="tablas" ></table>
                <div id="pager3"></div>
            </div>
            <div id="der"style="float: right;margin-top:145px; width:771px;">
                <div id="containerinversionistaPaso4" style="height: 400px;float: left;display: none"></div>
            </div>
        </div>
        <div id="paso5" class="tabcontent">
            <div id="izq" style="height: 0px;margin-top: 136px;">
                <table id="tabla_inversionistaPaso5" class="tablas" ></table>
                <div id="pager4"></div>
            </div>
            <div id="der"style="float: right;margin-top:145px; width:771px;">
                <div id="containerinversionistaPaso5" style="height: 400px;float: left;display: none"></div>
            </div>
        </div>

        <div id="info"  class="ventana" >
            <p id="notific">EXITO AL GUARDAR</p>
        </div>
        <div id="divSalidaEx" title="Exportacion" style=" display: block" >
            <p  id="msjEx" style=" display:  none"> Espere un momento por favor...</p>
            <center>
                <img id="imgloadEx" style="position: relative;  top: 7px; display: none " src="./images/cargandoCM.gif"/>
            </center>
            <div id="respEx" style=" display: none"></div>
        </div> 

        <div id="dialogMsjHelp" title="Help" class="ventana" >
            <table id="tablainterna" style="height: 53px; width: 273px"  >
                <tr>
                    <td>
                        <label hidden>Id</label>
                    </td>
                    <td>
                        <input id="id" type="text"  style="width: 110px;" readonly hidden>
                    </td>
                    <td>
                        <label>Modulo</label>
                    </td>
                    <td>
                        <input id="modulo_" type="text"  style="width: 110px;"readonly>
                    </td>
                    <td>
                        <label>Paso</label>
                    </td>
                    <td>
                        <input id="paso_" type="text"  style="width: 110px;"readonly>
                    </td>
                </tr>
            </table>
            <br>
            <br>
            <fieldset class="scheduler-border" style="width: 100px;border-color:#B6ADAB66;" >
                <legend class="scheduler-border" >Descripcion de la ayuda</legend>
                <textarea id="textoArea" cols="106" rows="34" style="width: 501px;height: 130px;"></textarea>
            </fieldset>
        </div>

        <div id="loader-wrapper" style="display: none">
            <img src="http://www.fintra.co/wp-content/uploads/2016/04/fintrafooter.png" alt="" class='loaderimg' />
            <div id="loader">          </div>
            <div class="loader-section section-left"></div>
            <div class="loader-section section-right"></div>
        </div>
    </body>
</html>
