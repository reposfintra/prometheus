<%-- 
    Document   : maestroCuentasDC
    Created on : 28/10/2016, 11:32:19 AM
    Author     : mariana
--%>

<%@page contentType="text/html" pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
        <link href="./css/popup.css" rel="stylesheet" type="text/css">
        <link type="text/css" rel="stylesheet" href="./css/jquery/jquery-ui/jquery-ui.css" />
        <link type="text/css" rel="stylesheet" href="./css/TransportadorasApi.css " />
        <script type="text/javascript" src="./js/jquery/jquery.jqGrid/js/jquery.min.js"></script>
        <script type="text/javascript" src="./js/jquery/jquery-ui/jquery.ui.min.js"></script><!--autocompletar--> 
        <script type="text/javascript" src="./js/jquery-ui-1.8.5.custom.min.js"></script>  
        <script type="text/javascript" src="./js/jquery/jquery.jqGrid-4.7.0/js/grid.locale-es.js"></script>
        <script type="text/javascript" src="./js/jquery/jquery.jqGrid-4.7.0/js/jquery.jqGrid.min.js"></script>
        <script type="text/javascript" src="./js/jquery/jquery.jqGrid-4.7.0/js/jquery.tabletojson.js"></script>    
        <script type="text/javascript" src="./js/jquery/jquery.jqGrid-4.7.0/plugins/jquery.contextmenu.js"></script>
        <script type="text/javascript" src="./js/maestroCuentasDC.js"></script> 
        <title>MAESTRO CUENTAS</title>
    </head>
    <body>
        <div id="capaSuperior" style="position:absolute; width:100%; height:100px; z-index:0; left: 0px; top: 0px;">
            <jsp:include page="/WEB-INF/toptsp.jsp?encabezado=MAESTRO CUENTAS"/>
        </div>
        <style>
            .onoffswitch-inner:before {
                padding-left: 13px;

            }
            .cbox{
                width: 10px;
            }
        </style>

    <center>
        <div id="tablita" class="ui-jqgrid ui-widget ui-widget-content center-block" style="width: 600px;margin-top: 31px;">
            <div class="ui-dialog-titlebar ui-widget-header ui-helper-clearfix">
                <span class="ui-dialog-title text-center" id="ui-dialog-title-listaGlobal">
                    &nbsp;
                    <label class="titulotablita" ><b>Filtro</b></label>
                </span>
            </div>
            <table id="tablainterna" style="height: 53px; width:590px"  >
                <tr>
                    <td>
                        <label>Cuenta</label>
                    </td>
                    <td>
                        <input type="text" id="cuenta" name="cuenta" style="width: 190px;">
                    </td>
                    <td>
                        <label>Modulo</label>
                    </td>
                    <td>
                        <select id="modulo" style="width: 199px;">
                            <!--                            <option value=""> </option>
                                                        <option value="TRANSFERENCIA">ANTICIPO TRANSFERENCIA</option>
                                                        <option value="COMBUSTIBLE">ANTICIPO COMBUSTIBLE</option>
                                                        <option value="PRONTO_PAGO">PRONTO PAGO</option>
                                                        <option value="REPORTE_PRODUCCION">ANALISIS REPORTE PRODUCCION</option>
                                                        <option value="INVERSIONISTA">INVERSIONISTA</option>
                                                        <option value="POSICION_CAJA">POSICION CAJA</option>
                                                        <option value="BINOMINA">BI NOMINA</option>
                                                        <option value="GASTOS">GASTOS</option>
                                                        <option value="INGRESOS">INGRESOS</option>-->
                        </select>
                    </td>
                    <td>
                        <hr style="width: 5px;height: 37px;top: -25px;margin-left: 13px;color: rgba(128, 128, 128, 0.39);" /> 
                    </td>
                    <td>
                        <div style="padding-top: 10px">
                            <center>
                                <button id="buscar_" class="ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only" style="margin-left: 15px;top: -7px;" > 
                                    <span class="ui-button-text">Buscar</span>
                                </button>  
                            </center>
                        </div>
                    </td>
                </tr>
            </table>
        </div>
        <div style="position: relative;top: 185px;">
            <table id="tabla_cuentas" ></table>
            <div id="pager"></div>
        </div>
        <div id="info"  class="ventana" >
            <p id="notific">EXITO AL GUARDAR</p>
        </div>

        <div id="dialogMsguardar"  class="ventana" >
            <table id="tablainterna" style="height: 53px;width: 651px;margin-top: 18px;"  >
                <tr>
                    <td>
                        <input type="text" id="id_" name="id_" style="width: 190px;" hidden>
                    </td>
                </tr>
                <tr>
                    <td>
                        <label><span style="color:red;">*</span>Cuenta</label>
                    </td>
                    <td>
                        <input type="text" id="cuenta_" name="cuenta_" style="width: 190px;"class="requerido">
                    </td>
                    <td>
                        <label style="margin-left: -118px;" id="mostrarlabel">Mostar</label>
                    </td>
                    <td>
                        <input type="checkbox" name="mostrar" id="mostrar" style="margin-left: -104px;"/>
                    </td>
                    <td>
                        <div style="padding-top: 10px">
                            <center>
                                <button type="button" id="buscar_2" class="ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only" style="margin-left: 15px;top: -7px;" > 
                                    <span class="ui-button-text">Buscar</span>
                                </button>  
                            </center>
                        </div>
                    </td>
                <tr>
                <tr>
                    <td>
                        <label><span style="color:red;">*</span>Nombre Cuenta</label>
                    </td>
                    <td>
                        <input type="text" id="nombre_cuenta_" name="nombre_cuenta_" style="width: 301px;" class="requerido" readonly>
                    </td>
                <tr>
                <tr>
                    <td>
                        <label><span style="color:red;">*</span>Modulo</label>
                    </td>
                    <td>
                        <select id="modulo_" style="width: 206px;height: 24px;border-radius: 4px;" class="requerido">
                            <!--                            <option value=""> </option>
                                                        <option value="TRANSFERENCIA">ANTICIPO TRANSFERENCIA</option>
                                                        <option value="COMBUSTIBLE">ANTICIPO COMBUSTIBLE</option>
                                                        <option value="PRONTO_PAGO">PRONTO PAGO</option>
                                                        <option value="REPORTE_PRODUCCION">ANALISIS REPORTE PRODUCCION</option>
                                                        <option value="INVERSIONISTA">INVERSIONISTA</option>
                                                        <option value="POSICION_CAJA">POSICION CAJA</option>
                                                        <option value="BINOMINA">BI NOMINA</option>
                                                        <option value="GASTOS">GASTOS</option>
                                                        <option value="INGRESOS">INGRESOS</option>-->
                        </select>
                    </td>
                </tr>
                <tr>
                    <td>
                        <label style="margin-left: 6px;">Paso</label>
                    </td>
                    <td>
                        <input id="paso_" type="text"  style="width: 190px;">
                    </td>
                </tr>
                <tr>
                    <td>
                        <label><span style="color:red;">*</span>Clasificacion</label>
                    </td>
                    <td>
                        <input id="clasificacion_" type="clasificacion_"  style="width: 301px;" class="requerido">
                    </td>
                </tr>
                <tr>
                    <td>
                        <label><span style="color:red;">*</span>Linea Negocio</label>
                    </td>
                    <td>
                        <select id="linea_negocio" style="width: 206px;height: 24px;border-radius: 4px;" class="requerido">
                            <option value="FA">FENALCO ATL</option>
                            <option value="FB">FENALCO BOL</option>
                            <option value="MC">MICROCREDITO</option>
                            <option value="LB">LIBRANZA</option>
                            <option value="AET">TRANSFERENCIAS</option>
                            <option value="AGA">COMBUSTIBLE</option>
                            <option value="EXT">PRONTO PAGO</option>
                            <option value="-">OTROS</option>
                        </select>
                    </td>
                </tr>
            </table>
        </div>
    </body>
</html>
