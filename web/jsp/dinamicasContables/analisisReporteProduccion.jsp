<%-- 
    Document   : analisisReporteProduccion
    Created on : 8/10/2016, 08:33:16 AM
    Author     : mariana
--%>

<%@page contentType="text/html" pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
        <link href="./css/popup.css" rel="stylesheet" type="text/css">
        <link type="text/css" rel="stylesheet" href="./css/jquery/jquery-ui/jquery-ui.css" />
        <link type="text/css" rel="stylesheet" href="./css/TransportadorasApi.css " />
        <script type="text/javascript" src="./js/jquery/jquery.jqGrid/js/jquery.min.js"></script>
        <script type="text/javascript" src="./js/jquery-ui-1.8.5.custom.min.js"></script>   
        <script type="text/javascript" src="./js/jquery/jquery.jqGrid-4.7.0/js/grid.locale-es.js"></script>
        <script type="text/javascript" src="./js/jquery/jquery.jqGrid-4.7.0/js/jquery.jqGrid.min.js"></script>
        <script type="text/javascript" src="./js/jquery/jquery.jqGrid-4.7.0/js/jquery.tabletojson.js"></script>    
        <script type="text/javascript" src="./js/jquery/jquery.jqGrid-4.7.0/plugins/jquery.contextmenu.js"></script>
        <script type="text/javascript" src="./js/analisisReporteProduccion.js"></script> 
        <script type="text/javascript" src="./js/highcharts/highcharts.js"></script>
        <script type="text/javascript" src="./js/highcharts/data.js"></script>
        <script type="text/javascript" src="./js/highcharts/drilldown.js"></script>



        <title>ANALISIS REPORTE PRODUCCION</title>
    </head>
    <body>
        <div id="capaSuperior" style="position:absolute; width:100%; height:100px; z-index:0; left: 0px; top: 0px;">
            <jsp:include page="/WEB-INF/toptsp.jsp?encabezado=ANALISIS REPORTE PRODUCCION"/>
        </div>
        <style>
            .ui-widget-header {
                border: 0px solid #234684;
            }

            #container {
                height: 400px; 
                min-width: 310px; 
                max-width: 800px;
                margin: 0 auto;
                margin-top: 48px;
            }
            #dialogMsjGraficaAnalisisReporteProduccion{
                padding: 35px;
            }
            .highcharts-credits{
                display:none;
            }


        </style>
    <center>
        <div id="tablita" class="ui-jqgrid ui-widget ui-widget-content center-block" style="width: 564px; height-min: 300px;margin-top: 31px;margin-left: 67px;">
            <div class="ui-dialog-titlebar ui-widget-header ui-helper-clearfix">
                <span class="ui-dialog-title text-center" id="ui-dialog-title-listaGlobal">
                    &nbsp;
                    <label class="titulotablita" ><b>Filtro</b></label>
                </span>
            </div>
            <table id="tablainterna" style="height: 53px; width: 566px"  >
                <tr>
                    <td>
                        <label>Filter:</label>
                    </td>
                    <td>
                        <select id="filtro_" style="width: 133px;" onchange="busqueda_filtro()">
                            <option value="aniocorrido">This Year</option>
                            <option value="anioanterior">Last Year</option>
                            <option value="mespasado">Last Month</option>
                            <option value="mespresente">This Month</option>
                            <option value="seismeses">Last 6 Months</option>
                            <option value="docemeses">Last 12 Months</option>
                            <option value="otro">Custom Filter</option>
                        </select>
                    </td>
                    <td>
                        <label  class='f_otro' style="display: none">Periodo Inicio</label>
                    </td>
                    <td>
                        <input id="periodo_inicio" class='f_otro' type="text"  style="width: 110px;display: none" onKeyPress="return soloNumeros(event)"  >
                    </td>
                    <td>
                        <label  class='f_otro' style="display: none">Periodo Fin</label>
                    </td>
                    <td>
                        <input id="periodo_fin" class='f_otro' type="text"  style="width: 110px;display: none"onKeyPress="return soloNumeros(event)"  >
                    </td>
                    <td>
                        <label class="estado_">Production Report</label>
                    </td>
                    <td>
                        <select id="anulado_" class="estado_" style="width: 133px;" onchange="">
                            <option value="0"></option>
                            <option value="1" selected="selected">Not Annulled</option>
                            <option value="2">Annulled</option>
                        </select>
                    </td>
                    <td>
                        <hr style="width: 5px;height: 37px;top: -25px;margin-left: 13px;color: rgba(128, 128, 128, 0.39);" /> 
                    </td>
                    <td>
                        <div style="padding-top: 10px">
                            <center>
                                <button id="buscar" class="ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only" style="margin-left: 15px;top: -7px;" > 
                                    <span class="ui-button-text">Search</span>
                                </button>  
                            </center>
                        </div>
                    </td>
                </tr>
            </table>
        </div>
        <div style="position: relative;top: 185px;">
            <table id="tabla_analisisReporteProduccion" ></table>
            <div id="pager"></div>
        </div>
        <div id="info"  class="ventana" >
            <p id="notific">EXITO AL GUARDAR</p>
        </div>
        <div id="divSalidaEx" title="Exportacion" style=" display: block" >
            <p  id="msjEx" style=" display:  none"> Espere un momento por favor...</p>
            <center>
                <img id="imgloadEx" style="position: relative;  top: 7px; display: none " src="./images/cargandoCM.gif"/>
            </center>
            <div id="respEx" style=" display: none"></div>
        </div> 

        <div id="dialogMsjGraficaAnalisisReporteProduccion" style="position: relative;margin-top: -7;"hidden="true">
            <div id="containerGasolina" style="height: 400px;float: left;"></div>
            <div id="containerProntoPago" style="height: 400px;float: right;"></div>
            <div id="containerTransferencia" style="height: 400px;float: left;"></div>
            <div id="containerEfectivo" style="height: 400px;float: right;"></div>
        </div>

        <div id="dialogMsjGraficaGeneralAnalisisReporteProduccion" style="position: relative;margin-top:-7;"hidden="true">
            <div id="container_" style="height: 400px;"></div>
        </div>
    </center>
</body>
</html>
