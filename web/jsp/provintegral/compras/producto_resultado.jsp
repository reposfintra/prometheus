<!--
- Autor : Ing. Rhonalf Martinez
- Date  : 25 de julio del 2009
- Copyrigth Notice : Fintravalores S.A.
-->
<%--
-@(#)
--Descripcion : P�gina JSP, que maneja el ingreso de ordenes de compra.
--%>
<%@page contentType="text/html"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
   "http://www.w3.org/TR/html4/loose.dtd">
<%@page session="true"%>
<%@page import="java.util.*"%>
<%@page import="com.tsp.operation.model.*"%>
<%@page import="com.tsp.operation.model.beans.*"%>
<%@page import="com.tsp.operation.model.services.*"%>
<%@page import="com.tsp.util.*"%>
<%@include file="/WEB-INF/InitModel.jsp"%>
<%@ taglib uri="http://jakarta.apache.org/taglibs/input-0.90" prefix="input" %>
<html>
    <head>
        <title>Productos - Resultado</title>
        <link href="<%= BASEURL%>/css/estilostsp.css" rel="stylesheet" type="text/css">
        <script type='text/javascript' src="<%=BASEURL%>/js/Validacion.js"></script>
        <script type='text/javascript' src="<%= BASEURL%>/js/boton.js"></script>
        <script type='text/javascript' src="<%= BASEURL%>/js/general.js"></script>
        <script src='<%=BASEURL%>/js/tools.js' language='javascript'></script>
        <script src='<%=BASEURL%>/js/date-picker.js'></script>
        <script src="<%= BASEURL%>/js/transferencias.js"></script>
        <script type="text/javascript" src="<%=BASEURL%>/js/buscarproveedor.js"></script>
        <script type="text/javascript">
            function enviarForm(){
                document.forma.submit();
            }
        </script>

        <link href="../../../css/estilostsp.css" rel="stylesheet" type="text/css">
    </head>

    <body onResize="redimensionar()" onload = "imgProveedor();redimensionar();">
        <div id="capaSuperior" style="position:absolute; width:100%; height:100px; z-index:0; left: 0px; top: 0px;">
            <jsp:include page="/toptsp.jsp?encabezado=Productos - Resultado"/>
        </div>
        <div id="capaCentral" style="position:absolute; width:100%; height:87%; z-index:0; left: 0px; top: 100px; overflow: scroll;">


            <%
                       ProductosService prod = new ProductosService();

            %>
            <form id ="forma" name="forma" action="" method="post">

                <table width="100%" border="2"align="center">
                    <tr>
                        <td>
                            <table width="100%" align="center" >
                                <tr>
                                    <td colspan="4" align="left" class="subtitulo1">&nbsp;Ver datos </td>
                                    <td colspan="2" align="left" class="barratitulo"><img src="<%=BASEURL%>/images/titulo.gif"></td>
                                </tr>
                                <tr>
                                    <td width="200" align="left" class="fila" >Consecutivo</td>
                                    <td colspan="2" valign="middle" class="fila" >Descripcion del producto</td>
                                    <td width="204" align="left" class="fila" >Precio</td>
                                    <td width="100" align="left" class="fila" >Tipo</td>
                                    <td align="left" class="fila" >Categoria</td>
                                </tr>
                                <%
                            ArrayList ver = (ArrayList) request.getAttribute("resultado");
                            int tam = ver.size();
                            Material spl = null;
                            for (int i = 0; i < tam; i++) {
                                spl = (Material) ver.get(i);
                                if (spl != null) {
                                %>
                                <tr>
                                    <td align="left" valign="top" class="letra">
                                        <%if (spl.getRegStatus()!=null && !(spl.getRegStatus().equals("A"))){%><!--091031-->
                                                <a target="_blank" href="<%=CONTROLLER%>?estado=Negocios&accion=Applus&opcion=consultarprod&idmat=<%=spl.getIdMaterial()%>">
                                                        <%=spl.getCodigo()%>											</a>
                                        <%}else{
                                                out.print(spl.getCodigo());
                                        }
                                        %>
                                    </td>
                                    <td colspan="2" valign="middle" class="letra"><%=spl.getDescripcion()%></td>
                                    <td align="left" class="letra" >$ <script type="text/javascript">document.write(formatNumber(<%=spl.getValor()%>));</script></td>
                                    <td align="left" class="letra" >
                                    <%
                                        if(spl.getTipo().equals("M")) out.print("Material");
                                        if(spl.getTipo().equals("D")) out.print("Mano de obra");
                                        if(spl.getTipo().equals("O")) out.print("Otros");
                                    %>
                                    </td>
                                    <td align="left" class="letra" ><%=spl.getCategoria()%></td>
                                </tr>
                                <%
                                 }
                             }
                                %>
                            </table>	  </td>
                    </tr>
                </table>
                <div align="center">
                    <br>
                    <img src="<%= BASEURL%>/images/botones/salir.gif"  name="imgsalir"  onMouseOver="botonOver(this);" onClick="parent.close();" onMouseOut="botonOut(this);"  style="cursor:hand "></div>
            </form>
        </div>
    </body>
    <iframe width="188" height="166" name="gToday:normal:agenda.js:gfPop:plugins.js" id="gToday:normal:agenda.js:gfPop:plugins.js" src="<%=BASEURL%>/js/Calendario/ipopeng.htm" scrolling="yes" frameborder="0" style="visibility:visible; z-index:999; position:absolute; left:-500px; top:0px;">
    </iframe>
</html>
<%
           String mens = (String) request.getAttribute("msg");
           if (!mens.equals("")) {%>
<script>
    alert('<%=mens%>');
</script>
<%}%>