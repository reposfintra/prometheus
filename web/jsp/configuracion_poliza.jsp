<%-- 
    Document   : configuracion_poliza
    Created on : 2/09/2018, 10:26:11 PM
    Author     : jbermudez
--%>

<%@page contentType="text/html" pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
        <title>CONFIGURACIÓN POLIZAS</title>
        <link href="./css/popup.css" rel="stylesheet" type="text/css">
        <link href="./css/maestro_polizas.css" rel="stylesheet" type="text/css">
        <link type="text/css" rel="stylesheet" href="./css/jquery/jquery-ui/jquery-ui.css" />
        <link type="text/css" rel="stylesheet" href="./css/TransportadorasApi.css" />
        <script type="text/javascript" src="./js/jquery/jquery.jqGrid/js/jquery.min.js"></script>
        <script type="text/javascript" src="./js/jquery-ui-1.8.5.custom.min.js"></script>
        <script type="text/javascript" src="./js/jquery/jquery.jqGrid-4.7.0/js/grid.locale-es.js"></script>
        <script type="text/javascript" src="./js/jquery/jquery.jqGrid-4.7.0/js/jquery.jqGrid.min.js"></script>
        <script type="text/javascript" src="./js/jquery/jquery.jqGrid-4.7.0/js/jquery.tabletojson.js"></script>
    </head>
    <body>
        <div id="capaSuperior" style="height:100px;">
            <jsp:include page="/WEB-INF/toptsp.jsp?encabezado=CONFIGURACIÓN POLIZAS"/>
        </div>
        <div class="wrappers" id="options-wrapper">
            <label for="unidades_de_negocios">Unidad de negocio</label>
            <select id="unidades_de_negocios"></select>            
            <label for="dpto">Departamento</label>
            <select id="dpto"></select>
            <label for="ciudades">Ciudad</label>
            <select id="ciudades"></select>
            <label for="sucursales">Sucursal</label>
            <select id="sucursales"></select>
            <div>
                <button id="btn_buscar" class="ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only">
                    <span class="ui-button-text">Buscar</span>
                </button>
                <button id="btn_aplicar" class="ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only">
                    <span class="ui-button-text">Aplicar a todas las sucursales</span>
                </button>
            </div>
        </div>
        <div id="table-wrapper">
            <table id="tabla_conf" ></table>
            <div id="pager"></div>
        </div>        
        <div id="dialog"  class="ventana">
            <div class="tabla-interna-wrapper">
                <input type="text" id="id" hidden>
                <table>
                    <tr>
                        <td>
                            <label>Aseguradora</label>
                        </td>
                        <td>
                            <select id="aseguradora">
                                <option selected>...</option>
                            </select>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <label>Poliza</label>
                        </td>
                        <td>
                            <select id="poliza">
                                <option selected>...</option>
                            </select>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <label>Tipo cobro</label>
                        </td>
                        <td>
                            <select id="tipo_cobro">
                                <option selected>...</option>
                            </select>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <label>Valor póliza</label>
                        </td>
                        <td>
                            <select id="valor_poliza">
                                <option selected>...</option>
                            </select>
                        </td>
                    </tr>
                </table>
            </div>                               
        </div>  
        <div id="info"  class="ventana" >
            <p id="notific">EXITO AL GUARDAR</p>
        </div>
        <script type="text/javascript" src="./js/configuracion_poliza.js"></script> 
    </body>
</html>
