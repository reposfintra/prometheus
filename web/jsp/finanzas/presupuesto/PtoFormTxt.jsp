 <!--  
     - Author(s)       :      MARIO FONTALVO
     - Date            :      10/12/2005  
     - Copyright Notice:      TRANSPORTES SANCHEZ POLO S.A.
  -->
 <%--   
     - @(#)  
     - Description: Formulario seleccion de presupuesto forma directa capturas de codigos
 --%>



<%@page session="true"%>
<%@page errorPage="/error/ErrorPage.jsp"%>
<%@page import="java.util.*" %>
<%@include file="/WEB-INF/InitModel.jsp"%>
<%@page import="com.tsp.finanzas.presupuesto.model.beans.*" %>
<%
    String path = application.getRealPath(model.menuService.getRealUrl(request.getRequestURI()));
    String datos[] = model.menuService.getContenidoMenu(BASEURL,request.getRequestURI(),path);
%>
<html>
<head>
    <title>Presupuesto de Ventas</title>
    <script src="<%= BASEURL %>/js/boton.js"></script>
    <link   href="<%= BASEURL %>/css/estilostsp.css" rel='stylesheet'>

    <script>
        function addOption(Comb,valor,texto){
                var Ele = document.createElement("OPTION");
                Ele.value=valor;
                Ele.text=texto;
                Comb.add(Ele);
        }    
        function Llenar(CmbAnno, CmbMes, TipoVista){
                var Meses    = new Array('Enero','Febrero','Marzo','Abril','Mayo','Junio','Julio','Agosto','Septiembre','Octubre','Noviembre','Diciembre');
                var FechaAct = new Date();
                CmbAnno.length = 0;
                CmbMes.length  = 0;
                
                for (i=FechaAct.getYear()-10;i<=FechaAct.getYear()+10;i++) addOption(CmbAnno,i,i)
                CmbAnno.value = FechaAct.getYear();
                if (TipoVista.value=='M')  addOption(CmbMes,'TD','TODOS');
                for (i=0;i<Meses.length;i++)
                    if ((i+1)<10) addOption(CmbMes,'0'+(i+1),Meses[i]);
                    else          addOption(CmbMes,(i+1),Meses[i]);                
                CmbMes.value  = ((FechaAct.getMonth()+1)<10)?('0'+(FechaAct.getMonth()+1)):(FechaAct.getMonth()+1);                 
                
                if (TipoVista.value == 'D')
                    rango.style.display = 'block';
                else
                    rango.style.display = 'none';                
        }    
        function cargardias (CmbDiaI, CmbDiaF){
                for (i=1;i<=31;i++){
                    dia = (i<10?'0'+i:i);
                    addOption(CmbDiaI,dia,dia);
                    addOption(CmbDiaF,dia,dia);                
                }
                CmbDiaI.value = '01';
                CmbDiaF.value = '31';
        }        
        function _onsubmit (tform){
            with (tform){
                var diaI = parseFloat(diaInicial.value);
                var diaF = parseFloat(diaFinal.value);
                if (diaI>diaF){
                   alert ('El dia final debe ser mayor que el dia Inicial, por favor rectifiquelo para continuar');
                   return false;
                }
            
                tform.Cliente.value = completar(tform.Cliente.value);
                tform.Stdjob.value  = completar(tform.Stdjob.value );
                var url = "<%= CONTROLLERPTO %>?ACCION=/Opciones/Presupuesto&Agencia="+ Agencia.value +"&Cliente=" + Cliente.value + "&Stdjob=" + Stdjob.value +"&TipoVista=" + TipoVista.value + "&Ano=" + Ano.value + "&Mes=" + Mes.value + "&diaInicial="+ diaInicial.value +"&diaFinal="+ diaFinal.value +"&AgenciaDespacho="+ AgenciaDespacho.value + "&Origen=" + Origen.value + "&Opcion="+ Opcion.value + "&Filtro=" + (Filtro.checked?Filtro.value:'');
                // newWindow(url, 'PtoVentas');
                window.location.href = url;
            }
            return false;
        }
        function newWindow(url, nombre){
           option="  width="+ (screen.width-30) +", height="+ (screen.height-80)  +",  scrollbars=yes, statusbars=yes, resizable=yes, menubar=no ,top=10 , left=10 ";
           ventana=window.open('',nombre,option);
           ventana.location.href=url;
           ventana.focus();
        }  

        function completar (codigo){
            var nuevo = codigo;
            if (codigo!='') for (i=codigo.length;i<6;i++) nuevo = '0' + nuevo;
            return nuevo;
        }
    </script>
</head>
<body onResize="redimensionar()" onLoad="redimensionar()">
<div id="capaSuperior" style="position:absolute; width:100%; height:100px; z-index:0; left: 0px; top: 0px;">
 <jsp:include page="/toptsp.jsp?encabezado=PRESUPUESTO DE VENTAS"/>
</div>
<div id="capaCentral" style="position:absolute; width:100%; height:87%; z-index:0; left: 0px; top: 100px; overflow: scroll;">
<center>


<form action="<%= CONTROLLERPTO %>?ACCION=/Opciones/Presupuesto" method="post" name="fpto" onsubmit='javascript: return _onsubmit(this);'>
<table width="380" border="2" align="center"  bgcolor="#F7F5F4">

    <tr><td align="center">

        <table width="100%" class="tablaInferior">
        <tr>
           <td colspan='2'>
            <table border='0' width='100%' cellspacing='0'>
                <tr>
                 <td class='subtitulo1'  width="80%">BUSQUEDA DIRECTA</td>
                 <td class='barratitulo' width="20%"><img src="<%= BASEURL %>/images/titulo.gif" width="32" height="20" align='left'><%=datos[0]%></td>
                </tr>
            </table>
           </td>
        </tr>
        
        <tr class="fila">
            <td width="40%">&nbsp;Estandar</td>
            <td width="60%"><input type="text" name="Stdjob" class="input" style="width:100%;text-align:center"></td>
        </tr>
        <tr class="fila">
            <td width="40%">&nbsp;Agencia</td>
            <td width="60%"><input type="text" name="Agencia" class="input" style="width:100%;text-align:center"></td>
        </tr> 
        <tr class="fila">
            <td width="40%">&nbsp;Cliente</td>
            <td width="60%"><input type="text" name="Cliente" class="input" style="width:100%;text-align:center"></td>
        </tr>    
        
        <tr class="fila">
            <td width="40%">&nbsp;Agencia Despacho</td>
            <td width="60%"><input type="text" name="AgenciaDespacho" class="input" style="width:100%;text-align:center"></td>
        </tr> 
        
        <tr class="fila">
            <td width="40%">&nbsp;Tipo de Vista</td>
            <td width="60%">
                <select class="select" style="width:100%" name="TipoVista" onclick="jscript: Llenar(Ano,Mes,this);">
                <option value="M">Mensual</option>
                <option value="S">Semanal</option>
                <option value="D">Diaria</option>
                </select>
            </td>
        </tr>    
        <tr class="fila">
            <td>&nbsp;Periodo</td>
            <td>
                <select class="select" style="width:49%" name="Ano"></select>
                <select class="select" style="width:49%" name="Mes"></select>
            </td>
        </tr>   
        <tr class="fila" id='rango' style='display:none'>
            <td >&nbsp;Rango de dias</td>
            <td >
                <select class="select" style="width:25%" name="diaInicial"></select>
                <select class="select" style="width:25%" name="diaFinal"></select>
            </td>
        </tr>    

        <tr class="fila">
            <td colspan="2"><input type="checkbox" name="Filtro" value="Ok"> Mostrar solo estandares con presupuesto.</td>
        </tr>     
        </table>
    </td></tr>
</table>        
<br>
<input type='hidden' name='Origen' value='PtoFormTxt'>
<input type="hidden" name="Opcion" value="Ver Presupuesto" >

<img src='<%=BASEURL%>/images/botones/buscar.gif'  style='cursor:hand' onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" onclick=" _onsubmit(fpto); ">
&nbsp;
<img src='<%=BASEURL%>/images/botones/salir.gif'   style='cursor:hand' onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" onclick='window.close();'>        



<script> Llenar(fpto.Ano,fpto.Mes,fpto.TipoVista); cargardias (fpto.diaInicial, fpto.diaFinal);</script>
</form
</center>
</div>
<%=datos[1]%>
</body>
</html>
