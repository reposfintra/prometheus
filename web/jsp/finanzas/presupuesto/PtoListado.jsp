 <!--  
     - Author(s)       :      MARIO FONTALVO
     - Date            :      10/12/2005  
     - Copyright Notice:      TRANSPORTES SANCHEZ POLO S.A.
  -->
 <%--   
     - @(#)  
     - Description: Visualizacion general de estandares con sus  presupuestos
 --%>


<%@page session="true"%> 
<%@page errorPage="/error/ErrorPage.jsp"%>
<%@page import="java.*" %>
<%@page import="java.util.*" %>
<%@include file="/WEB-INF/InitModel.jsp"%>
<%@page import="com.tsp.finanzas.presupuesto.model.beans.*" %>
<%@page import="com.tsp.util.UtilFinanzas" %>
<%
    String Origen = (request.getParameter("Origen")!=null)?request.getParameter("Origen"):"PtoFormCmb";
    Paginacion paginacion = modelpto.DPtoSvc.paginacion.getPaginacion();
    String TipoVista   = modelpto.DPtoSvc.getValor("Tipo");
    String Mes         = modelpto.DPtoSvc.getValor("Mes");
    String Ano         = modelpto.DPtoSvc.getValor("Ano");
    int    NroColumnas = modelpto.DPtoSvc.getNroColumnas();
    int    anchoMensual = 140;
    int    anchoResto   = 100;  // Semanal y diario
    int    diaInicial = (modelpto.DPtoSvc.getValor("diaInicial")!=null?Integer.parseInt(modelpto.DPtoSvc.getValor("diaInicial")):0);
    int    diaFinal   = (modelpto.DPtoSvc.getValor("diaFinal")!=null?Integer.parseInt(modelpto.DPtoSvc.getValor("diaFinal")):0);
    int    diferencia = diaFinal - diaInicial +1;
    if (!TipoVista.equals("D")) diferencia = NroColumnas;
%>
<html>
    <head>
        <title>Presupuesto de Venta</title>
        <script src="<%= BASEURL %>/js/boton.js"></script>
        <link   href="<%= BASEURL %>/css/estilostsp.css" rel='stylesheet'>
        <script language='javascript' src='<%= BASEURL %>/js/finanzas/presupuesto/listadoPto.js'></script>
        <script>
        <%= modelpto.DPtoSvc.getVarJSSeparador() %>
        <%= modelpto.DPtoSvc.getVarJSTasa() %>
        </script>
    </head>
<body onResize="redimensionar()" onLoad="redimensionar()">


<div id="capaSuperior" style="position:absolute; width:100%; height:100px; z-index:0; left: 0px; top: 0px;">
 <jsp:include page="/toptsp.jsp?encabezado=PRESUPUESTO DE VENTAS"/>
</div>
<div id="capaCentral" style="position:absolute; width:100%; height:85%; z-index:0; left: 0px; top: 100px; overflow: scroll;">
<center>

<%  if (modelpto.DPtoSvc.getLista()!=null && modelpto.DPtoSvc.getLista().size()>0){ %>

<table align="center"  border="2" bgcolor="#F7F5F4">
<tr>
  <td align='center'>
 
  
  
        <table class='tablaInferior'>
        <tr>
            <td colspan='<%= (diferencia) + 17 + (TipoVista.equals("M")?0:1)  %>'>
            
             <table border='0' width='100%' cellspacing='0'>
                  <tr>
                     <td class='subtitulo1'  width="40%">PRESUPUESTO DE VENTA  -  <%=  (!Mes.equals("TD")? UtilFinanzas.NombreMes(Integer.parseInt(Mes)) : "" )%>   <%= Ano %> </td>
                     <td class='barratitulo' width="60%"><img src="<%= BASEURL %>/images/titulo.gif" width="32" height="20"></td>
                  </tr>
                </table>
                        
            </td> 
        </tr>
        <tr class="tblTitulo">
            <th class="bordereporte" nowrap width='10'>NRO</th>
            <th class="bordereporte" nowrap width='30'>DISTRITO</th>
            <th class="bordereporte" nowrap width='40'>ESTANDAR</th>
            <th class="bordereporte" nowrap width='130'>AGENCIA</th>
            <th class="bordereporte" nowrap width='130'>CLIENTE</th>
            <th class="bordereporte" nowrap width='130'>ORIGEN</th>
            <th class="bordereporte" nowrap width='130'>DESTINO</th>
            <th class="bordereporte" nowrap width='350'>DESCRIPCION DEL ESTANDAR</th>
            <th class="bordereporte" nowrap width='200'>RECURSO</th>
            <th class="bordereporte" nowrap width='40'>TIPOVIAJE</th>
            <!--<th width='40'>UNIDAD</th>
            <th width='40'>CANT. REQ</th>-->
            <th class="bordereporte" nowrap width='100'>TARIFA</th>
            <th class="bordereporte" nowrap width='40'>MONEDA</th>
            <th class="bordereporte" nowrap width='80'>VIAJES</th>
            <th class="bordereporte" nowrap width='100'>TOTAL PTO</th>
            <%
                if (!TipoVista.equals("M"))
                    out.print("<th class='bordereporte' nowrap width=40 align=center>Calc</th>");
                for (int i=0;i<NroColumnas;i++){
                    String estilo1 = "";
                    if (TipoVista.equals("D") && ((i+1)>diaFinal || (i+1)<diaInicial) ) estilo1 = " style='display:none' ";
                    out.print("<th class='bordereporte' nowrap "+ estilo1 +" >"+ (TipoVista.equals("M")?( Mes.equals("TD")? UtilFinanzas.NombreMes(i+1).substring(0,3): UtilFinanzas.NombreMes( Integer.parseInt(Mes)) ):((TipoVista.equals("S")?"Sem ": UtilFinanzas.DiaSemana( Ano + Mes + UtilFinanzas.DiaFormat(i+1) ) + " " ) + UtilFinanzas.DiaFormat(i+1)    )) +"</th>");
                }
            %>
        <th class="bordereporte" nowrap width='70'>Modificar</th>
        </tr>    
<%
        Iterator it = paginacion.getListado().iterator();
        int Cont = (paginacion.getVista_Actual()-1)* paginacion.getCantidad_Filas();
        int form=0;
        while(it.hasNext()){
            DatosGeneral datos = (DatosGeneral) it.next(); 
            String title = "Distrito : "+ datos.getValor("DISTRITO") +"\nAgencia : "+ datos.getValor("NOMBREAGENCIA") +"\nCliente : "+datos.getValor("NOMBRECLIENTE")+"\nStandar : "+datos.getValor("STDJOBNO")+"\nDescripcion: "+ datos.getValor("STDJOBDESC") +"\nRuta :"+datos.getValor("NOMBREORIGEN")+"-"+datos.getValor("NOMBREDESTINO");
%>
        <form action="<%= CONTROLLERPTO %>?ACCION=/Opciones/Presupuesto" method="post"  name='form<%= ++form %>'>
        <tr class='<%= (Cont%2==0?"filagris":"filaazul") %>'  style='font-size:9px' title='<%= title %>' ondblclick="  if (this.className=='filaresaltada') this.className='<%= (Cont%2==0?"filagris":"filaazul") %>'; else this.className='filaresaltada';">
        <th class="bordereporte" nowrap ><%= (++Cont) %></th>
        <td class="bordereporte" nowrap align='center'><%= datos.getValor("DISTRITO")       %></td>
        <td class="bordereporte" nowrap align='center'><%= datos.getValor("STDJOBNO")       %></td>
        <td class="bordereporte" nowrap ><%= UtilFinanzas.Trunc(datos.getValor("NOMBREAGENCIA"),15) %></td>
        <td class="bordereporte" nowrap ><%= UtilFinanzas.Trunc(datos.getValor("NOMBRECLIENTE"),15) %></td>
        <td class="bordereporte" nowrap ><%= UtilFinanzas.Trunc(datos.getValor("NOMBREORIGEN"),15)  %></td>
        <td class="bordereporte" nowrap ><%= UtilFinanzas.Trunc(datos.getValor("NOMBREDESTINO"),15) %></td>
        <td class="bordereporte" nowrap ><%= datos.getValor("STDJOBDESC") %></td>
        <td class="bordereporte" nowrap align='center'><%= datos.getValor("RECURSO")        %></td>
        <td class="bordereporte" nowrap align='center'><%= datos.getValor("TIPOVIAJE")      %></td>
        <!--<td nowrap align='center'><%= datos.getValor("UNIDAD_R")       %></td>
        <td nowrap align='center'><%= datos.getValor("CANTIDAD_R")     %></td>-->
        <td class="bordereporte"nowrap ><input type='text' name='VTarifa'  value='<%= UtilFinanzas.customFormat2(datos.getValor("TARIFA"))    %>' readonly class='input' size='2' style='text-align:right;  width:100%;'><input type='hidden' name='Tarifa'   value='<%= datos.getValor("TARIFA")%>'></td>
        <td class="bordereporte"nowrap ><input type='text' name='Moneda'   value='<%= datos.getValor("MONEDA")                                %>' readonly class='input' size='2' style='text-align:center; width:100%;'></td>
        <td class="bordereporte"nowrap ><input type='text' name='Total'    value='<%= datos.getValor("TOTAL")                                 %>' readonly class='input' size='2' style='text-align:center; width:100%;'></td>
        <td class="bordereporte"nowrap ><input type='text' name='TotalPto' value='<%= datos.getValor("TOTALPTO")                              %>' readonly class='input' size='2' style='text-align:right;  width:100%;'></td>
            <%
                if (!TipoVista.equals("M")){
                    String url1  = CONTROLLERPTO +"?ACCION=/Opciones/Presupuesto&Opcion=ShowCalendar&Distrito="+ datos.getValor("DISTRITO") +"&Agencia="+ datos.getValor("CODIGOAGENCIA") +"&Cliente="+ datos.getValor("CODIGOCLIENTE") +"&Stdjob="+ datos.getValor("STDJOBNO") +"&Ano="+ Ano +"&Mes="+ Mes;
                    String link1 = "<a name='link' href='javascript:void(0)' onclick=\"jscript: newWindow('"+ url1 +"','PtoMensual',650,600,100,100); \" HIDEFOCUS><img align='absmiddle' src='"+ BASEURL+ "/js/Calendario/cal.gif' border='0' alt='' ></a>";
                    out.print("<td align=center>"+ link1 +"</td>");
                }
            %>
        <input type='hidden'   name='Distrito'  value="<%= datos.getValor("DISTRITO")      %>">
        <input type='hidden'   name='Stdjob'    value="<%= datos.getValor("STDJOBNO")      %>">
        <input type='hidden'   name='Agencia'   value="<%= datos.getValor("CODIGOAGENCIA") %>">
        <input type='hidden'   name='Cliente'   value="<%= datos.getValor("CODIGOCLIENTE") %>"> 
        <input type='hidden'   name='Ano'       value="<%= Ano     %>">
        <input type='hidden'   name='Mes'       value="<%= Mes     %>">  
        <input type='hidden'   name='UnidadR'   value="<%= datos.getValor("UNIDAD_R")   %>">  
        <input type='hidden'   name='CantidadR' value="<%= datos.getValor("CANTIDAD_R") %>">  
            
            <%
                for (int i=0;i<NroColumnas;i++){
                    String TipoBD    = datos.getValor( "TIPO" + (TipoVista.equals("M")?String.valueOf(i):""));
                    String aviajes   = (!datos.getValor(String.valueOf(i)).equals("0")?datos.getValor(String.valueOf(i)):""  );
                    String hviajes   = (!datos.getValor("VP" + String.valueOf(i)).equals("0")?datos.getValor("VP" + String.valueOf(i)):""  );
                    String titleDatosPrevios = "Registrado por    : " + datos.getValor("UP" + String.valueOf(i)) + "\n"+ 
                                               "Fecha de Registro : " + datos.getValor("FP" + String.valueOf(i)) ;
                    String masestilo = (!datos.getValor(String.valueOf(i)).equals("0")?" style='color:red; font-weight:bold;' ":"");
                    String readonly  = (!modelpto.DPtoSvc.Modificable(TipoBD, TipoVista)?"readonly":"") ;
                    String url       = CONTROLLERPTO + "?ACCION=/Opciones/Presupuesto&Opcion=ShowCalendar&Distrito="+ datos.getValor("DISTRITO") +"&Agencia="+ datos.getValor("CODIGOAGENCIA") +"&Cliente="+ datos.getValor("CODIGOCLIENTE") +"&Stdjob="+ datos.getValor("STDJOBNO") +"&Ano="+ Ano +"&Mes="+ (TipoVista.equals("M")? ( Mes.equals("TD")? UtilFinanzas.DiaFormat(i+1) :  Mes   ):Mes);
                    String link      = "<a name='link' href='javascript:void(0)'  onclick=\"jscript: newWindow('"+url+"','PtoMensual"+ i +"'); \" HIDEFOCUS><img align='absmiddle' src='"+ BASEURL+ "/js/Calendario/cal.gif' border='0' alt='' style='cursor:hand'></a>";
                    String estilo1 = "";
                    if (TipoVista.equals("D") && ((i+1)>diaFinal || (i+1)<diaInicial) ) estilo1 = " style='display:none' "; %>
                    
                    <td class="bordereporte" nowrap width='<%= (TipoVista.equals("M")?anchoMensual: anchoResto ) %>' align='center' <%= estilo1 %>>
                         <input type='text'   name='HViajes' value='<%= hviajes %>' class='input' onfocus='this.select()' title='<%= titleDatosPrevios %>' readonly  style='text-align:center;width:40; font-weight: bold; border-left-color: gray; border-bottom-color: gray; color: white; border-top-color: gray; background-color: gray; border-right-color: gray'  >
                         <input type='text'   name='Viajes'  value='<%= aviajes %>' class='input' onfocus='this.select()' onkeyup="jscript: validar(this);" style='text-align:center;width:40' <%= masestilo %> <%= readonly %> > <%= (TipoVista.equals("M")?link:"") %> 
                         <input type='hidden' name='CViajes' value='<%= datos.getValor(String.valueOf(i)) %>'>
                    </td>
            <%  } %>
            <%  if (TipoVista.equals("M") && NroColumnas==1) { %>
            <input type='hidden' value='0' name='Viajes'>
            <input type='hidden' value='0' name='CViajes'>
            <% } %> 
            <th nowrap class="bordereporte" >
               <input type="hidden" value="Grabar" name="Opcion"> 
               <img src="<%=BASEURL%>/images/botones/iconos/modificar.gif" style='cursor:hand' onclick=" if (qsubmit(form<%=form%>)) {  form<%=form%>.submit();  } "> 
            </th>
        </tr>
        </form>
        <script>calcular(form<%= form %>);</script>
<%      } 
        out.print("<tr class='letraresaltada'><td class='bordereporte' align='right' colspan='"+ (TipoVista.equals("M")?14:15)  +"'>Total Viajes Periodo</td>");
        TreeMap Totales = modelpto.DPtoSvc.getListaTotales();
        for(int i=0;i<Totales.size();i++) {
            String estilo1 = "";
            if (TipoVista.equals("D") && ((i+1)>diaFinal || (i+1)<diaInicial) ) estilo1 = " style='display:none' "; 
            out.print("<td class='bordereporte' align='center' "+ estilo1 +">"+ com.tsp.util.UtilFinanzas.customFormat("#,###",Double.parseDouble(String.valueOf(Totales.get(String.valueOf(i)))),0)     +"</td>");
        }
        out.print("<td>&nbsp</td></tr>");
%>
        </table>
     </td>
    </table>
    
      <%@include file="/jsp/finanzas/presupuesto/paginacion.jsp"%>   
      <br>
<%   }else{%>
        <BR><BR>
            <table width="400" border="2" align="center"  bgcolor="#F7F5F4">
            <tr>
            <td>
                <table width='100%' class='tablaInferior'>
                <tr>
                         <td class='subtitulo1'  width="40%">INFORMACION</td>
                         <td class='barratitulo' width="60%"><img src="<%= BASEURL %>/images/titulo.gif" width="32" height="20"></td>
                </tr>
                <tr><td align='center' colspan='2' class='letra'>
                     <br>
                     No se encontraron datos para mostrar, si desea puede intentar nuevamente
                     <br>&nbsp;
                </td></tr>
              </table>
            </td>
            </tr></table>
        <BR><BR>
     <%}%>
        <img src='<%=BASEURL%>/images/botones/regresar.gif'   style='cursor:hand' onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" onclick="window.location.href='<%= CONTROLLERPTO %>?ACCION=/Me/nu&JSP=<%= modelpto.DPtoSvc.getOrigen() %>'; ">
        <%  if (modelpto.DPtoSvc.getLista()!=null && modelpto.DPtoSvc.getLista().size()>0){ %>
        &nbsp;
        <img src="<%=BASEURL%>/images/botones/exportarExcel.gif"  style='cursor:hand' onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" onclick="window.location.href='<%= CONTROLLERPTO %>?ACCION=/Opciones/Presupuesto&Opcion=Exportar'; ">         
        <% } %>
        &nbsp; <!--       
        <img src='<%=BASEURL%>/images/botones/refrescar.gif'  style='cursor:hand' onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" onclick="window.location.href='<%= CONTROLLERPTO %>?ACCION=/Opciones/Presupuesto&Opcion=Refrescar'; ">
        &nbsp;-->
        <img src='<%=BASEURL%>/images/botones/salir.gif'  style='cursor:hand' onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" onclick="window.close(); ">
        
        <%  String msg = request.getParameter("Mensaje");
            if (msg!=null && !msg.equals("")) { %>
        <table border="2" align="center">
            <tr>
                <td><table width="100%"  border="1" align="center"  bordercolor="#F7F5F4" bgcolor="#FFFFFF">
                    <tr>
                        <td width="320" align="center" class="mensajes"><%=msg%></td>
                        <td width="29" background="<%=BASEURL%>/images/cuadronaranja.JPG">&nbsp;</td>
                        <td width="58">&nbsp;</td>
                    </tr>
                </table>
                </td>
            </tr>
        </table>  
        <% } %> 
        </center>  
</div>        
</body>
</html>
