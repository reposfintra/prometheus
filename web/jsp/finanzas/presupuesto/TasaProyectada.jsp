<!--  
     - Author(s)       :      MARIO FONTALVO
     - Date            :      10/12/2005  
     - Copyright Notice:      TRANSPORTES SANCHEZ POLO S.A.
  -->
 <%--   
     - @(#)  
     - Description: Permite manejar tasa proyectada
 --%>

<%@page session="true"%> 
<%@page errorPage="/error/ErrorPage.jsp"%>
<%@page import="java.*" %>
<%@page import="java.util.*" %>
<%@include file="/WEB-INF/InitModel.jsp"%>
<%@page import="com.tsp.util.UtilFinanzas" %>
<%@page import="com.tsp.finanzas.presupuesto.model.beans.*" %>
<html>
<head>
<title>Tasa Proyectada</title>

 <link href="<%= BASEURL %>/css/estilostsp.css" rel="stylesheet" type="text/css"> 
 <script type='text/javascript' src="<%= BASEURL %>/js/boton.js"></script>
 <script language="JavaScript" type="text/javascript" src="<%=BASEURL%>/js/finanzas/presupuesto/Validaciones.js"></script>
 
</head>
<body>


<div id="capaSuperior" style="position:absolute; width:100%; height:100px; z-index:0; left: 0px; top: 0px;">
<jsp:include page="/WEB-INF/toptsp.jsp?encabezado=Tasa Proyectada"/>
</div>

<div id="capaCentral" style="position:absolute; width:100%; height:85%; z-index:0; left: 0px; top: 100px; overflow: scroll;"> 



 <%
  int Ano_Actual    = Integer.parseInt(UtilFinanzas.getFechaActual_String(1));
  int Mes_Actual    = Integer.parseInt(UtilFinanzas.getFechaActual_String(3));
  String comentario = (request.getParameter("comentario")==null)?"":request.getParameter("comentario");
%>

<center>

<FORM ACTION="<%=CONTROLLERPTO%>?ACCION=/Tasa/Proyectada" METHOD='post' name='formulario'>
<table width="400" border="2" align="center">
      <tr>
        <td ALIGN='center'>  
               <TABLE  width='100%'   class='tablaInferior'   >       
                  <tr>
                    <td colspan='2'>
                       <table cellpadding='0' cellspacing='0' width='100%'>
                         <tr class="fila">
                          <td align="left" width='65%' class="subtitulo1">&nbsp Asignaci�n Tasa  Proyectada</td>
                          <td align="left" width='*' class="barratitulo"><img src="<%=BASEURL%>/images/titulo.gif" width="32" height="20"></td>
                        </tr>
                       </table>
                    </td>
                  </tr>
                  <tr>
                    <td>
                              
                           <TABLE width='100%' class='tablaInferior' >
                                <tr class='fila'>
                                   <td width="40%">&nbsp A�o  :</td>
                                   <td width="60%">
                                      <select name='ano' class='comentario'>
                                      <%
                                        for(int i=Ano_Actual;i<=(Ano_Actual+10);i++){
                                          String select="";
                                          if(i==Ano_Actual)select="selected";
                                          %><option value='<%= String.valueOf(i) %>' <%= select %> ><%= String.valueOf(i) %></option>
                                      <%} %>
                                     </select>
                                   </td>
                                </tr>
                                <tr class='fila'>
                                   <td width="40%">&nbsp Mes :</td>
                                   <td width="60%">
                                      <select name="mes" style="width:54%" class='comentario'>
                                      <%
                                      for(int i=1;i<=12;i++){
                                          String select="";
                                          if(i==Mes_Actual)select="selected";
                                          %><option value='<%= UtilFinanzas.mesFormat(i) %>' <%= select %> ><%= UtilFinanzas.NombreMes(i) %></option>
                                     <%} %>
                                     </select> 
                                  </td>
                                </tr>
                                <tr class='fila'>
                                   <td width="40%">&nbsp Dolar:</td>
                                   <td width="60%">
                                      <input  type='text' name='dol'  size='13'  value="0" class="comentario">		
                                  </td>
                                </tr>
                                <tr class='fila'>
                                   <td width="40%">&nbsp Bolivar:</td>
                                   <td width="60%">
                                      <input  type='text' name='bol'  size='13'  value="0" class="comentario">		
                                  </td>
                                </tr>

                                <tr class='fila'>
                                   <td width="40%">&nbsp DTF:</td>
                                   <td width="60%">
                                      <input  type='text' name='dtf'  size='13'  value="0" class="comentario">		
                                  </td>
                                </tr>
                            </TABLE>
                    </td>
                  </tr>
              </table>
           </td>
         </tr>
      </table> 
      
      
     <input type='hidden'               name='opcion'> 
     <input type='hidden'               name='usuario' value='FVD'>
    
    
    </FORM>
  
    
       
 
    <img src="<%=BASEURL%>/images/botones/cancelar.gif"  name="imgcancelar" onClick="OpcionTasa(formulario,'1')"    onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" style="cursor:hand">&nbsp;
    <img src="<%=BASEURL%>/images/botones/aceptar.gif"   name="imgaceptar"  onClick="OpcionTasa(formulario,'2')"    onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" style="cursor:hand">&nbsp;
    <img src="<%=BASEURL%>/images/botones/buscar.gif"    name="imgbuscar"   onClick="OpcionTasa(formulario,'3')"    onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" style="cursor:hand">&nbsp; 
    <img src="<%=BASEURL%>/images/botones/salir.gif"     name="imgsalir"    onClick=" parent.close();"              onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" style="cursor:hand">&nbsp;
    
    
  
  
<!--  Excepciones --> 

    <% if (!comentario.equals("")){%>
         <br>
           <table border="2" align="center">
              <tr>
                <td><table width="100%"  border="1" align="center"  bordercolor="#F7F5F4" bgcolor="#FFFFFF">
                  <tr>
                    <td width="320" align="center" class="mensajes"><%=comentario%></td>
                    <td width="29" background="<%=BASEURL%>/images/cuadronaranja.JPG">&nbsp;</td>
                    <td width="58">&nbsp;</td>
                  </tr>
                </table></td>
              </tr>
            </table>
     <%}%>
   
  
  
  
  
<!-- Listado -->
<% List lista = modelpto.TasaSvc.getList();
   if(lista!=null && lista.size()>0){%>
   
   
   <FORM ACTION="<%=CONTROLLERPTO%>?ACCION=/Tasa/Proyectada" METHOD='post'  name="formParametros">
   
   <table width="750" border="2" align="center">
      <tr>
        <td ALIGN='center'> 
        
               <table class='tablaInferior' width='100%' >
                    <tr>
                        <td colspan='7'>
                             <table border='0' width='100%' cellspacing='0'>
                                  <tr>
                                     <td class='subtitulo1'  width="40%">LISTADO  </td>
                                     <td class='barratitulo' width="60%"><img src="<%= BASEURL %>/images/titulo.gif" width="32" height="20"></td>
                                  </tr>
                             </table>
                        </td> 
                    </tr>
                    <tr class="tblTitulo">
                          <TH width='5%'><input type='checkbox' name='All' id='All' onclick="Sell_all(this.form,this)">     </TH>
                          <TH class="bordereporte" nowrap width='10%'>A�O     </TH>
                          <TH class="bordereporte" nowrap width='15%'>MES     </TH>
                          <TH class="bordereporte" nowrap width='20%'>DOLAR   </TH>
                          <TH class="bordereporte" nowrap width='20%'>BOLIVAR </TH>
                          <TH class="bordereporte" nowrap width='20%'>DTF     </TH>
                          <TH class="bordereporte" nowrap width='*'  >EDITAR  </TH>                    
                    </tr>

                    
                  <% Iterator it = lista.iterator();
                     int cont=0;
                     while(it.hasNext()){
                       Tasa tasa    = (Tasa)it.next();
                       int id       = tasa.getId();
                       String name  = "para_"+cont;
                       String value = String.valueOf(id);
                       cont++; %>
                       <TR  class='<%= (cont%2==0?"filagris":"filaazul") %>' style='font-size:12px'>
                          <TD align='center' class="bordereporte"><input type='checkbox'  value="<%=value%>" name="<%= name%>"  onclick='Sell_all(this.form,this)'>  </TD>
                          <TD align='center' class="bordereporte"><%=tasa.getAno()    %>                                         </TD>
                          <TD                class="bordereporte"><%= UtilFinanzas.NombreMes(Integer.parseInt(tasa.getMes()))%>  </TD>
                          <TD align='right'  class="bordereporte"><%=tasa.getDolar()  %> <input  type='text'  name='TextDol_<%=id%>' value="<%=tasa.getDolar()  %>" maxlength='10' size='10'  >  </TD>
                          <TD align='right'  class="bordereporte"><%=tasa.getBolivar()%> <input  type='text'  name='TextBol_<%=id%>' value="<%=tasa.getBolivar()%>" maxlength='10' size='10'  >  </TD>
                          <TD align='right'  class="bordereporte"><%=tasa.getDTF()    %> <input  type='text'  name='TextDtf_<%=id%>' value="<%=tasa.getDTF()    %>"maxlength='10'  size='10'  >  </TD>
                          <TD align='center' class="bordereporte"><% String url=CONTROLLERPTO+"?ACCION=/Tasa/Proyectada&opcion=MODIFICAR&objeto="+id; String comilla="'"; %>                                   
                                  <img src="<%=BASEURL%>/images/botones/iconos/modificar.gif" title='Modificar'   name="imgmod"  onClick="enviarParametroTasa(<%=comilla+url+comilla%>,<%=comilla+id+comilla%>);"    style="cursor:hand">&nbsp;                                  
                          </TD>
                       </TR>
                    <%}%>
               </table>    
           </td>
         </tr>                     
      </TABLE>
      <br> 
     
      <img src="<%=BASEURL%>/images/botones/eliminar.gif"   name="imgeliminar"  onClick="OpcionTasa(formParametros,'4')"    onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" style="cursor:hand">&nbsp;
      <img src="<%=BASEURL%>/images/botones/salir.gif"     name="imgsalir"    onClick=" parent.close();"              onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" style="cursor:hand">&nbsp;
   
      
       <input type='hidden'               name='opcion'>
       <input type='hidden'               name='usuario' value='FVD'>
      </FORM>
             
      
    <% 
   }
%>
</body>
</html>
