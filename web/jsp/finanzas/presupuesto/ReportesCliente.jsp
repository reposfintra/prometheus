<!--  
     - Author(s)       :      MARIO FONTALVO
     - Date            :      10/12/2005  
     - Copyright Notice:      TRANSPORTES SANCHEZ POLO S.A.
  -->
 <%--   
     - @(#)  
     - Description: Permite realizar Reportes de Cliente
 --%>
<%@page session="true"%> 
<%@page errorPage="/error/ErrorPage.jsp"%>
<%@include file="/WEB-INF/InitModel.jsp"%>

<%@page import="java.util.*" %>
<%@page import="com.tsp.finanzas.presupuesto.model.beans.*" %>
<%@page import="com.tsp.util.UtilFinanzas" %>

<%
    String Ano = modelpto.ReportesSvc.getAno();
    String Mes = modelpto.ReportesSvc.getMes();
    String Distrito    = request.getParameter("Distrito");
    String AgenciaDes  = (request.getParameter("AgenciaDespacho")!=null?request.getParameter("AgenciaDespacho"):"");
    String Agencia     = (request.getParameter("Agencia")!=null?request.getParameter("Agencia"):"");
    String Cliente     = (request.getParameter("Cliente")!=null?request.getParameter("Cliente"):"");
    int anchoCol = 29; 
    int restoCol = 790;
    int diaInicial = modelpto.ReportesSvc.getDiaInicial();
    int diaFinal   = modelpto.ReportesSvc.getDiaFinal();
    int diferencia = diaFinal - diaInicial;    
    String estiloPtoMyEjt = "style='color:red;        font-weight:bold;'";
    String estiloPtoMnEjt = "style='color:darkgreen;  font-weight:bold;'";
    int today = Integer.parseInt(UtilFinanzas.getFechaActual_String(8));
%>

<html>
    <head><title>Reporte [Viajes Presupuestados Vs Viajes Ejecuados] Por Cliente</title>
        <link href="<%= BASEURL %>/css/estilotsp.css" rel='stylesheet'>
		<script language="javaScript" type="text/javascript" src="<%=BASEURL%>/js/boton.js"></script>
        <script>
            function newWindow(url, nombre){
              option="  width="+ (screen.width-30) +", height="+ (screen.height-80)  +",  scrollbars=no, statusbars=yes, resizable=yes, menubar=no ,top=10 , left=10 ";
              ventana=window.open('',nombre,option);
              ventana.location.href=url;
            }
        
            function go(Cliente){			
                var url = "<%= CONTROLLERPTO %>?ACCION=/Opciones/Reportes&Opcion=Generar&Ano=<%= Ano %>&Mes=<%= Mes %>&Distrito=<%= Distrito %>&AgenciaDespacho=<%= AgenciaDes %>&diaInicial=<%= diaInicial %>&diaFinal=<%= diaFinal %>&Agencia=<%= Agencia %>&Tipo=2&Cliente=" + Cliente;
                newWindow(url, 'PE'+Cliente);
            }    
            function exportar (){
                var url = "<%= CONTROLLERPTO %>?ACCION=/Opciones/Reportes&Opcion=Exportar&Ano=<%= Ano %>&Mes=<%= Mes %>&Distrito=<%= Distrito %>&AgenciaDespacho=<%= AgenciaDes %>&diaInicial=<%= diaInicial %>&diaFinal=<%= diaFinal %>&Agencia=<%= Agencia %>&Cliente=<%= Cliente %>&Tipo=1&tipoReporte=CLIENTE";
                window.location.href = url;
            }            
    </script>
    </head>
   <body onResize="redimensionar()" onLoad="redimensionar()">
		<div id="capaSuperior" style="position:absolute; width:100%; height:100px; z-index:0; left: 1px; top: 0px;">
 			<jsp:include page="/toptsp.jsp?encabezado=Reporte Por Cliente"/>
		</div>
	<div id="capaCentral" style="position:absolute; width:100%; height:87%; z-index:0; left: 0px; top: 100px; overflow: scroll;">

<% 
    List listado = modelpto.ReportesSvc.getListadoList();
    if (listado!=null && listado.size()>0){
%>
            <%
                for (int i_ag = 0 ; i_ag < listado.size(); i_ag++){
                    ViajesAgencia ag = (ViajesAgencia) listado.get(i_ag);
            %>
        <table border='2' >            
			<tr> 
				<td>
					<table width="100%">
						<td width="25%" class="subtitulo1">AGENCIA [<%= ag.getAgencia() %>] <%= ag.getAgenciaNombre() %></td>
						<td width="52%" class="barratitulo"><img src="<%=BASEURL%>/images/titulo.gif" width="32" height="20"></td>
					</table>
				</td> 
			</tr>  
            <tr><td>
                <!-- ******************************* CLIENTES   ************************************ -->
                <table border="1">
                <tr><td>
                <table border='0' cellpadding='1' cellspacing='1' width='<%= (anchoCol* (diferencia*3) )+restoCol %>'>
                <tr class='tblTitulo'>
                    <th rowspan='2'>Cliente</th>
                    <th rowspan='2'>Presup<br>Mensual   </th>
                    <th rowspan='2'>Presup<br>a la Fecha</th>
                    <th rowspan='2'>Ejecut<br>a la Fecha</th>
                    <th rowspan='2'>Viajes<br>no Prog.  </th>
                    <th rowspan='2'>Difer</th>
                    <th rowspan='2'>Porcen<br>Cumpl</th>
                        <% for (int ix_st = diaInicial ; ix_st <= diaFinal ; ix_st++ ) out.print("<th colspan='3'>"+ UtilFinanzas.DiaSemana(Ano + Mes + UtilFinanzas.DiaFormat(ix_st)) + " " + ix_st +"</th>"); %>
                </tr>
                        
                <tr class='tblTitulo' >
                        <% for (int ix_st = diaInicial ; ix_st <= diaFinal ; ix_st++ ) out.print("<th>Pt</th><th>Ej</th><th>Np</th>"); %>
                </tr>
                
                        <%
                            List listacliente = ag.getListadoClientesToList();
                            for (int i_cl = 0 ; i_cl < ag.getListadoClientes().size(); i_cl++)      {
                                ViajesCliente cl = (ViajesCliente) listacliente.get(i_cl); 
                        %>

                <tr class=<%=(i_cl % 2 == 0 )?"filagris":"filaazul"%>>
                <td class='bordereporte' width='370' align='left'><a href='javascript: void(0);' onclick="go('<%= cl.getCliente() %>');">[<%= cl.getCliente() %>]</a> <%= cl.getClienteNombre() %></td>
                <td class='bordereporte' width='70' align='center'><%= cl.getTotalPt()    %></td>
                <td class='bordereporte' width='70' align='center'><%= cl.getTotalPtF()   %></td>
                <td class='bordereporte' width='70' align='center'><%= cl.getTotalEj()    %></td>
                <td class='bordereporte' width='70' align='center'><%= cl.getTotalNP()    %></td>
                <td class='bordereporte' width='70' align='center'><%= cl.getDiferencia() %></td>
                <td class='bordereporte' width='70' align='center'><%= UtilFinanzas.customFormat2(cl.getPorcentajeIncumplimiento()) %></td>
                    <% for (int ix_cl = diaInicial; ix_cl <= diaFinal; ix_cl++) {
                        String estilo = "";
                        int fecha = Integer.parseInt(cl.getAno() + cl.getMes() + UtilFinanzas.DiaFormat(ix_cl));
                        if (fecha <= today ){
                            estilo = ((cl.getViajePtdo(ix_cl) > cl.getViajeEjdo(ix_cl))?estiloPtoMyEjt:
                                      (cl.getViajePtdo(ix_cl) < cl.getViajeEjdo(ix_cl))?estiloPtoMnEjt:"");
                        }
                    %>
                    <td class='bordereporte' align='center' width='<%= anchoCol %>' <%= estilo %> ><%= (cl.getViajePtdo(ix_cl)==0?"-":String.valueOf(cl.getViajePtdo(ix_cl))) %></td>
                    <td class='bordereporte' align='center' width='<%= anchoCol %>' <%= estilo %> ><%= (cl.getViajeEjdo(ix_cl)==0?"-":String.valueOf(cl.getViajeEjdo(ix_cl))) %></td>
                    <td class='bordereporte' align='center' width='<%= anchoCol %>' <%= estilo %> ><%= (cl.getViajeNP  (ix_cl)==0?"-":String.valueOf(cl.getViajeNP  (ix_cl))) %></td>
                    <%  } %>
            </tr>
            <%  } %>
            
            
            
                <!-- TOTALES AGENCIAS -->
                <tr class='filagris'>
                <td class='bordereporte' width='370'align='right' > <b>Totales</b>     </td>
                <td class='bordereporte' width='70' align='center'><%= ag.getTotalPt()    %></td>
                <td class='bordereporte' width='70' align='center'><%= ag.getTotalPtF()   %></td>
                <td class='bordereporte' width='70' align='center'><%= ag.getTotalEj()    %></td>
                <td class='bordereporte' width='70' align='center'><%= ag.getTotalNP()    %></td>
                <td class='bordereporte' width='70' align='center'><%= ag.getDiferencia() %></td>
                <td class='bordereporte' width='70' align='center'><%= UtilFinanzas.customFormat2(ag.getPorcentajeIncumplimiento()) %></td>
                <% for (int ix_ag = diaInicial; ix_ag <= diaFinal; ix_ag++) {
                    String estilo = "";
                    int fecha = Integer.parseInt(ag.getAno() + ag.getMes() + UtilFinanzas.DiaFormat(ix_ag));
                    if (fecha <= today ){
                        estilo = ((ag.getViajePtdo(ix_ag) > ag.getViajeEjdo(ix_ag))?estiloPtoMyEjt:
                                  (ag.getViajePtdo(ix_ag) < ag.getViajeEjdo(ix_ag))?estiloPtoMnEjt:"");
                    }
                %>
                        <td class='bordereporte' align='center' width='<%= anchoCol %>' <%= estilo %> ><%= (ag.getViajePtdo(ix_ag)==0?"-":String.valueOf(ag.getViajePtdo(ix_ag))) %></td>
                        <td class='bordereporte' align='center' width='<%= anchoCol %>' <%= estilo %> ><%= (ag.getViajeEjdo(ix_ag)==0?"-":String.valueOf(ag.getViajeEjdo(ix_ag))) %></td>
                        <td class='bordereporte' align='center' width='<%= anchoCol %>' <%= estilo %> ><%= (ag.getViajeNP  (ix_ag)==0?"-":String.valueOf(ag.getViajeNP  (ix_ag))) %></td>
                <%  } %>
                </tr>
                <!-- FIN TOTALES AGENCIAS -->
            
            
            
            
            </table>
            </td></tr></table>

                
            <!-- *****************************  FIN CLIENTES  ************************************ -->
            </td>
            </tr>
        </table>
        <br><br>
        <%  } // end for %>        
        
<%   }     // end if %>
<table>
<tr><td>
    <%if (listado!=null && !listado.isEmpty()) { %><img src="<%=BASEURL%>/images/botones/exportarExcel.gif" width="90" height="21" onClick="exportar();" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" style="cursor:hand"><% } %>
    <img src="<%=BASEURL%>/images/botones/salir.gif" width="90" height="21" onClick="parent.close();" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" style="cursor:hand">
</td></tr>
</table>

<%  String msg = (String) request.getAttribute("msg");
    if (msg!=null && !msg.equals("")) { %>
        <table border="2" align="center">
           <tr>
           <td><table width="100%"  border="1" align="center"  bordercolor="#F7F5F4" bgcolor="#FFFFFF">
                <tr>
                 <td width="320" align="center" class="mensajes"><%=msg%></td>
                 <td width="29" background="<%=BASEURL%>/images/cuadronaranja.JPG">&nbsp;</td>
                 <td width="58">&nbsp;</td>
                </tr>
              </table>
           </td>
           </tr>
        </table>  
<% } %> 


    </div>
    </body>
</html>
