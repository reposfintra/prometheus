<!--
     - Author(s)       :      MARIO FONTALVO
     - Date            :      25/01/2006
     - Copyright Notice:      TRANSPORTES SANCHEZ POLO S.A.
-->
<%--
     - @(#)
     - Description: Repoortes
--%> 
<%@include file="/WEB-INF/InitModel.jsp"%>
<html>
<head>
<title>CONSULTA PRESUPUESTO DE VENTAS</title>
<link href="../../../css/estilostsp.css" rel="stylesheet" type="text/css">
<link href="<%= BASEURL %>/css/estilostsp.css" rel="stylesheet" type="text/css">
</head>
<body>
<BR>
<table width="696"  border="2" align="center">
  <tr>
    <td width="811" >
      <table width="100%" border="0" align="center">
        <tr  class="subtitulo">
          <td height="24" colspan="2"><div align="center">CONSULTA PRESUPUESTO DE VENTAS</div></td>
        </tr>
		<tr class="subtitulo1">
          <td colspan="2"> PARAMETROS DE CONSULTA PRESUPUESTO DE VENTAS</td>
        </tr>
        <tr>
          <td width="172" class="fila"> DISTRITO </td>
          <td width="502"  class="ayudaHtmlTexto">Distrito del estandar</td>
        </tr>
        <tr>
          <td width="172" class="fila"> AGENCIA </td>
          <td width="502"  class="ayudaHtmlTexto">Agencia due�a del cliente, recuerde que aqui solo podra escribir el codigo de la agencia tal cual como se encuentra en la base de datos.</td>
        </tr>

        <tr>
          <td  class="fila">CLIENTE</td>
          <td  class="ayudaHtmlTexto">Cliente relacionados a la Agencia seleccionada, recuerde que aqui solo podra escribir el codigo del cliente tal cual como se encuentra en la base de datos.</td>
        </tr>
        <tr>
          <td  class="fila">ORIGEN</td>
          <td  class="ayudaHtmlTexto">Origenes de los estandares relacionados al cliente, recuerde que aqui solo podra escribir el codigo del origen tal cual como se encuentra en la base de datos.</td>
        </tr>

        <tr>
          <td  class="fila">DESTINO</td>
          <td  class="ayudaHtmlTexto">Destinos relacionados a los origenes de los estandares de clinete seleccionado, recuerde que aqui solo podra escribir el codigo del destino tal cual como se encuentra en la base de datos.</td>
        </tr>

        <tr>
          <td class="fila">ESTANDAR</td>
          <td  class="ayudaHtmlTexto">Estandar relacionado a los parametros anteriores, recuerde que aqui solo podra escribir el codigo del estandar tal cual como se encuentra en la base de datos.</td>
        </tr>
        <tr>
          <td class="fila">AGENCIA DESPACHO</td>
          <td  class="ayudaHtmlTexto">Agencia de Despacho de los estandares, recuerde que aqui solo podra escribir el codigo de la agencia tal cual como se encuentra en la base de datos.</td>
        </tr>
        <tr>
          <td width="172" class="fila"> TIPO DE VISTA </td>
          <td width="502"  class="ayudaHtmlTexto">Tipo de vista que desea generar el usuario este es de tres Tipos: MENSUAL, SEMANAL, DIARIO</td>
        </tr>

        <tr>
          <td width="172" class="fila"> PERIODO </td>
          <td width="502"  class="ayudaHtmlTexto">Periodo a consultar</td>
        </tr>
        <tr>
          <td width="172" class="fila"> RANGO DE DIAS </td>
          <td width="502"  class="ayudaHtmlTexto">Numero de dias a consultar del periodo por parte del usuario, este depende del Tipo de Vista especificado por el usuario.</td>
        </tr>
        <tr>
          <td width="172" class="fila" align='center'> <img src="<%= BASEURL %>/images/botones/buscar.gif"> </td>
          <td width="502"  class="ayudaHtmlTexto">Boton que da inicio al proceso de consulta de Presupuesto</td>
        </tr>

      </table>
    </td>
  </tr>
</table>
</body>
</html>
