 <!--  
     - Author(s)       :      MARIO FONTALVO
     - Date            :      10/12/2005  
     - Copyright Notice:      TRANSPORTES SANCHEZ POLO S.A.
  -->
 <%--   
     - @(#)  
     - Description: Formulario seleccion de presupuesto atraves de los clientes
 --%>

<%@page session="true"%>
<%@page errorPage="/error/ErrorPage.jsp"%>
<%@page import="java.util.*" %>
<%@include file="/WEB-INF/InitModel.jsp"%>
<%@page import="com.tsp.finanzas.presupuesto.model.beans.*" %>
<%
    String path = application.getRealPath(model.menuService.getRealUrl(request.getRequestURI()));
    String datos[] = model.menuService.getContenidoMenu(BASEURL,request.getRequestURI(),path);
%>
<html>
<head>
<title>Presupuesto de Venta</title>
<script src="<%= BASEURL %>/js/boton.js"></script>
<link   href="<%= BASEURL %>/css/estilostsp.css" rel='stylesheet'>
<script>

    function completar (codigo){
        var nuevo = codigo;
        if (codigo!='') for (i=codigo.length;i<6;i++) nuevo = '0' + nuevo;
        return nuevo;
    }
   
    
    function addOption(Comb,valor,texto){
            var Ele = document.createElement("OPTION");
            Ele.value=valor;
            Ele.text=texto;
            Comb.add(Ele);
    }    
    
   
    function LoadOrigenes(CmbOrigen, CmbDestino ,CmbStandar){
       CmbOrigen.length=0;
       var  aux='?';
       
       if (datos.length>0){
           for (i=0;i<datos.length;i++){
             var info=(new String(datos[i])).split(separador);
             if (aux!=info[6]){
                 addOption(CmbOrigen,info[6],info[7]);
                 aux=info[6];
             }
           }
        }else
           addOption(CmbOrigen,'NINGUNO','NO SE ENCONTRARON ORIGENES');
           
        LoadDestino (CmbOrigen, CmbDestino, CmbStandar);
    } 
   
    function LoadDestino (CmbOrigen, CmbDestino, CmbStandar){
       CmbDestino.length=0;
       var  aux='?';
       if (datos.length>0){	  
           if (CmbOrigen.value!='')
               for (i=0;i<datos.length;i++){
                 var info=(new String(datos[i])).split(separador);
                 if (CmbOrigen.value==info[6])
                    if (aux!=info[8]){
                         addOption(CmbDestino,info[8],info[9]);
                         aux=info[8];
                    }
               }
        }else
           addOption(CmbDestino,'NINGUNO','NO SE ENCONTRARON DESTINOS'); 
       LoadEstandar(CmbOrigen,CmbDestino,CmbStandar);
    }
    
    function LoadEstandar(CmbOrigen,CmbDestino,CmbStandar){
       CmbStandar.length=0;
       var  aux='?';
       if (datos.length>0){	   
           if (CmbDestino.value!='')	   
           for (i=0;i<datos.length;i++){
             var info=(new String(datos[i])).split(separador);
             if (CmbOrigen.value==info[6] && CmbDestino.value==info[8] )
                if (aux!=info[4]){
                     addOption(CmbStandar,info[4], '[' + info[4] + '] ' + info[5]);
                     aux=info[4];
                }
           }
        }else
           addOption(CmbStandar,'NINGUNO','NO SE ENCONTRARON ESTANDARES');               
    }

    function cargardias (CmbDiaI, CmbDiaF){
        for (i=1;i<=31;i++){
            dia = (i<10?'0'+i:i);
            addOption(CmbDiaI,dia,dia);
            addOption(CmbDiaF,dia,dia);                
        }
        CmbDiaI.value = '01';
        CmbDiaF.value = '31';
    } 
        
    function Llenar(CmbAnno, CmbMes, TipoVista){
            var Meses    = new Array('Enero','Febrero','Marzo','Abril','Mayo','Junio','Julio','Agosto','Septiembre','Octubre','Noviembre','Diciembre');
            var FechaAct = new Date();
            CmbAnno.length = 0;
            CmbMes.length  = 0;

            for (i=FechaAct.getYear()-10;i<=FechaAct.getYear()+10;i++) addOption(CmbAnno,i,i)
            CmbAnno.value = FechaAct.getYear();
            if (TipoVista.value=='M') addOption(CmbMes,'TD','TODOS');
            for (i=0;i<Meses.length;i++)
                if ((i+1)<10) addOption(CmbMes,'0'+(i+1),Meses[i]);
                else          addOption(CmbMes,(i+1),Meses[i]);                
            CmbMes.value  = ((FechaAct.getMonth()+1)<10)?('0'+(FechaAct.getMonth()+1)):(FechaAct.getMonth()+1);                 
            
            if (TipoVista.value == 'D')
                rango.style.display = 'block';
            else
                rango.style.display = 'none'; 
                    
        }    
        function newWindow(url, nombre){
           option="  width="+ (screen.width-8) +", height="+ (screen.height-60)  +",  scrollbars=no, statusbars=yes, resizable=yes, menubar=no ,top=0 , left=0 ";
           ventana=window.open('',nombre,option);
           ventana.location.href=url;
           ventana.focus();
        }        
        
       function _onsubmit (tform){
       
            if (tform.Opcion.value=='Buscar'){
               tform.Cliente.value = completar (tform.Cliente.value);
               return true;
            }
            if (tform.Estandar.value=='NINGUNO'){
              alert ('Primero debe indicar un estandar para poder continuar');
              return false;
            }
            
            with (tform){
                var diaI = parseFloat(diaInicial.value);
                var diaF = parseFloat(diaFinal.value);
                if (diaI>diaF){
                   alert ('El dia final debe ser mayor que el dia Inicial, por favor rectifiquelo para continuar');
                   return false;
                }            
                var url = "<%= CONTROLLERPTO %>?ACCION=/Opciones/Presupuesto&Cliente=" + Cliente.value + "&Stdjob=" + Estandar.value +"&TipoVista=" + TipoVista.value + "&Ano=" + Ano.value + "&Mes=" + Mes.value + "&diaInicial="+ diaInicial.value +"&diaFinal="+ diaFinal.value +"&Opcion="+ Opcion.value + "&Origen=PtoFormClientes";
                window.location.href = url;
                //newWindow(url, 'PtoVentas');
            }
        }
   
    
    <%@include file="/jsp/finanzas/presupuesto/datosEstandarCombos.jsp" %>
    
</script>
</head>

<body onResize="redimensionar()" onLoad="redimensionar()">
<div id="capaSuperior" style="position:absolute; width:100%; height:100px; z-index:0; left: 0px; top: 0px;">
 <jsp:include page="/toptsp.jsp?encabezado=PRESUPUESTO DE VENTAS"/>
</div>
<div id="capaCentral" style="position:absolute; width:100%; height:87%; z-index:0; left: 0px; top: 100px; overflow: scroll;">
<center>

<form action="<%= CONTROLLERPTO %>?ACCION=/Buscar/ClientesEstandar" method="post" name="fpto" onsubmit=" return _onsubmit(this); ">
    <table width="480" align="center" border="2" bgcolor="#F7F5F4">
            <td align="center">
                <table width="99%" border="0" align="center" class="tablaInferior">
                    <tr>
                       <td align='center' colspan='3'>
                       <table border='0' width='100%' cellspacing='0'>
                          <tr>
                             <td class='subtitulo1'  width="80%">ASIGNACION POR CLIENTES</td>
                             <td class='barratitulo' width="20%"><img src="<%= BASEURL %>/images/titulo.gif" width="32" height="20" align='left'><%=datos[0]%></td>
                          </tr>
                        </table>
                    </td>    
                    </tr>
                    <tr class="fila" >
                        <td width="15%">&nbsp;Cliente</td>
                        <td width="15%" align='center'> 
                            <input name="Cliente" type="text"   class="input"  style="width:100%; text-align:center" maxlength="6" onkeyup="" onfocus='this.select();' >		     
                        </td>
                        <td width="70%">
                            <img src="<%= BASEURL %>/images/botones/iconos/lupa.gif" name="Buscar"  value="Buscar" width="20" onclick=" if (_onsubmit(fpto)) fpto.submit(); " style='cursor:hand;'>
                        </td>
                    </tr>
		
                    <tr class="fila" >
                        <td >&nbsp;Nombre</td>
                        <td colspan='2'> 
                            <select name='ItemNombreCliente' class='select' style="width:100%" onchange="fpto.Cliente.value = this.value; if (_onsubmit(fpto)) fpto.submit();">
                            <% Iterator it = modelpto.DPtoSvc.getListaClientes().iterator();
                               while (it.hasNext()) {
                                   DatosGeneral dt = (DatosGeneral) it.next();
                                   out.print("\n\t\t<option value='"+ dt.getValor("codigo") +"'>"+ dt.getValor("descripcion") +"</option>");
                               }
                            %>
                            </select>
                        </td>
                    </tr>              
                    
                    <tr class="fila">
                        <td colspan="3">&nbsp;</td>
                    </tr>
                    <!--
                    <tr class="fila">
                        <td >&nbsp;Nombre</td>
                        <td colspan="2"><input name="NomCliente" type="text" class="input" style="width:100%;"  readonly></td>
                    </tr>-->

                    <tr class="fila" >
                        <td >&nbsp;Origen</td>
                        <td colspan="2" >
                            <select class="select" style="width:100%" name="Origen" onchange="LoadDestino (Origen, Destino,Estandar);"></select>
                        </td>
                    </tr>
		
                    <tr class="fila" >
                        <td >&nbsp;Destino</td>
                        <td colspan="2" >
                            <select class="select" style="width:100%" name="Destino" onchange="LoadEstandar (Origen, Destino, Estandar);" ></select>
                        </td>
                    </tr>		

                    <tr class="fila">
                        <td colspan="3">&nbsp;</td>
                    </tr>	
				
                    <tr class="fila" >
                        <td >&nbsp;Estandar</td>
                        <td colspan="2" >
                            <select name="Estandar" class="select" id="Estandar" style="width:100%">
                            </select>
                        </td>
                    </tr>	
                
                    <tr class="fila">
                        <td >&nbsp;Vista</td>
                        <td colspan='2'>
                            <select class="select" style="width:120" name="TipoVista" onclick="jscript: Llenar(Ano,Mes,this);">
                            <option value="M">Mensual</option>
                            <option value="S">Semanal</option>
                            <option value="D">Diaria</option>
                            </select>
                        </td>
                    </tr>    
                    <tr class="fila">
                        <td >&nbsp;Periodo</td>
                        <td colspan='2'>
                            <select class="select" style="width:49%" name="Ano"></select>
                            <select class="select" style="width:49%" name="Mes"></select>
                        </td>
                    </tr>
                    <tr class="fila" id='rango' style='display:none'>
                        <td >&nbsp;Rango</td>
                        <td colspan='2' >
                            <select class="select" style="width:25%" name="diaInicial"></select>
                            <select class="select" style="width:25%" name="diaFinal"></select>
                        </td>
                    </tr>                     
                </table>
                
               
                
            </td>
        </tr>
    </table>    
        <br>
        <input type="hidden" name="Opcion" value="Buscar">                
        <img src='<%=BASEURL%>/images/botones/buscar.gif'  style='cursor:hand' onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" onclick=" fpto.Opcion.value='Ver Presupuesto';  _onsubmit(fpto); ">
        &nbsp;
        <img src='<%=BASEURL%>/images/botones/salir.gif'   style='cursor:hand' onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" onclick='window.close();'>        
</form>
</center>
</div>
<script> 
LoadOrigenes(fpto.Origen, fpto.Destino ,fpto.Estandar);
Llenar(fpto.Ano,fpto.Mes,fpto.TipoVista); 
fpto.Cliente.value           = <%=  (request.getParameter("Cliente")!=null?"completar('" + request.getParameter("Cliente") + "')": "''") %>;
fpto.ItemNombreCliente.value = <%=  (request.getParameter("Cliente")!=null?"completar('" + request.getParameter("Cliente") + "')": "''") %>;
fpto.Cliente.focus();
cargardias (fpto.diaInicial, fpto.diaFinal);
</script>
<%=datos[1]%>
</body>
</html>
