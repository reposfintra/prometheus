<!--  
     - Author(s)       :      MARIO FONTALVO
     - Date            :      10/12/2005  
     - Copyright Notice:      TRANSPORTES SANCHEZ POLO S.A.
  -->
 <%--   
     - @(#)  
     - Description: Formulario ingresar el presupuesto en la vista calendario
 --%> 
<%@page session="true"%> 
<%@page errorPage="/error/ErrorPage.jsp"%>
<%@page import="java.*" %>
<%@page import="java.util.*" %>
<%@include file="/WEB-INF/InitModel.jsp"%>
<%@page import="com.tsp.finanzas.presupuesto.model.beans.*" %>
<% String Mensaje = (request.getParameter("Mensaje")!=null?request.getParameter("Mensaje"):""); %>

<html>
<head>
<title>Presupuesto de Venta</title>
<link href="<%= BASEURL %>/css/estilostsp.css" rel="stylesheet" type="text/css"> 
<script type='text/javascript' src="<%= BASEURL %>/js/boton.js"></script>

<script language='jscript' src='<%= BASEURL %>/js/finanzas/presupuesto/move.js' type='text/javascript'></script>
<script language='jscript' src='<%= BASEURL %>/js/finanzas/presupuesto/dtpPto.js' type='text/javascript'></script>
<script language='jscript'>
<% if (modelpto.DPtoSvc.getDatos()!=null && modelpto.DPtoSvc.getDatos().size()>0)  {
    DatosGeneral datos = (DatosGeneral) modelpto.DPtoSvc.getDatos().get(0);
    String Cambio = "0.00";
    try {
        Cambio =  ((DatosGeneral) modelpto.DPtoSvc.getListadoTasa().get(modelpto.DPtoSvc.getDValor("Mes")) ).getValor(datos.getValor("MONEDA"));
    }
    catch (Exception e){}
	modelpto.ReprogamacionSvc.SEARCH_ALL_ACTIVAS();
%>
    var Distrito      = "<%= datos.getValor("DISTRITO")      %>";
    var Ano           =  <%= modelpto.DPtoSvc.getDValor("Ano")    %> ;
    var Mes           =  <%= modelpto.DPtoSvc.getDValor("Mes")    %> ;
    var strMes        = "<%= modelpto.DPtoSvc.getDValor("Mes")    %>";
    var StdJobNo      = "<%= datos.getValor("STDJOBNO")      %>";
    var StdJobDesc    = "<%= datos.getValor("STDJOBDESC")    %>";
    var CodigoAgencia = "<%= datos.getValor("CODIGOAGENCIA") %>";
    var NombreAgencia = "<%= datos.getValor("NOMBREAGENCIA") %>";
    var CodigoCliente = "<%= datos.getValor("CODIGOCLIENTE") %>";
    var NombreCliente = "<%= datos.getValor("NOMBRECLIENTE") %>";
    var CodigoOrigen  = "<%= datos.getValor("CODIGOORIGEN")  %>";
    var NombreOrigen  = "<%= datos.getValor("NOMBREORIGEN")  %>";
    var CodigoDestino = "<%= datos.getValor("CODIGODESTINO") %>";
    var NombreDestino = "<%= datos.getValor("NOMBREDESTINO") %>";
    var Recurso       = "<%= datos.getValor("RECURSO")       %>";
    var TipoViaje     = "<%= datos.getValor("TIPOVIAJE")     %>";
    var Moneda        = "<%= datos.getValor("MONEDA")        %>";
    var Tarifa        =  <%= datos.getValor("TARIFA")        %> ;
    var Unidad        = '<%= datos.getValor("UNIDAD_R")      %>' ;
    var Cantidad      =  <%= datos.getValor("CANTIDAD_R")    %> ;
    var Mensaje       = '<%= Mensaje %>' ;
    
    ////////////////////////////////////////////////////////////////
    var  Cambio;
    if (Moneda=='PES') Cambio= 1;
    else Cambio =  <%= Cambio %>
    /////////////////////////////////////////////////////////////////
    
    var Total         =  <%= datos.getValor("TOTAL")         %>; 
    <%
        out.print("var PtoViajes  = [");
        for (int i=0;i<30;i++) out.print( datos.getValor(String.valueOf(i)) + ",");
        out.print(datos.getValor(String.valueOf(30)) + "];");
    %>
    <%
        out.print("var HPtoViajes  = [");
        for (int i=0;i<30;i++) out.print( datos.getValor("VP" + String.valueOf(i)) + ",");
        out.print(datos.getValor("VP" + String.valueOf(30)) + "];");
    %>
    <%
        out.print("var comboRP = \"");
		List lista = modelpto.ReprogamacionSvc.getListaReprogramacionesActivas();
		if (lista!=null){
			for (int i=0;i<lista.size();i++) {
				Reprogramacion dt = (Reprogramacion) lista.get(i);
				out.print( "<option value='" + dt.getCodigo() + "'>" + dt.getDescripcion() + "</option>" );
			}
		}
        out.print("\"");
    %>	
    var BASEURL          = "<%= BASEURL %>";
    var Action           = "<%= CONTROLLERPTO %>?ACCION=/Opciones/Presupuesto&Opcion=GrabarCalendario";
    var ActionRefrescar  = "<%= CONTROLLERPTO %>?ACCION=/Opciones/Presupuesto&Opcion=Refrescar";
    var ActionAnular     = "<%= CONTROLLERPTO %>?ACCION=/Opciones/Presupuesto&Opcion=AnularCalendario&Distrito="+ Distrito +"&Stdjob="+ StdJobNo +"&Ano="+ Ano +"&Mes=" + strMes + "&Agencia=" + CodigoAgencia + "&Cliente=" + CodigoCliente;
    var ActionHistorial  = "<%= CONTROLLERPTO %>?ACCION=/Opciones/Presupuesto&Opcion=Historial&Distrito="+ Distrito +"&Stdjob="+ StdJobNo +"&Ano="+ Ano +"&Mes=" + strMes + "&StdJobDesc=" + StdJobDesc + "&NombreAgencia=" + NombreAgencia + "&NombreCliente=" + NombreCliente + "&Ruta=" + NombreOrigen + " - " + NombreDestino;
    var ActionHistorialE = "<%= CONTROLLERPTO %>?ACCION=/Opciones/Presupuesto&Opcion=HistorialEdicion&Distrito="+ Distrito +"&Stdjob="+ StdJobNo +"&Ano="+ Ano +"&Mes=" + strMes + "&Agencia=" + CodigoAgencia + "&Cliente=" + CodigoCliente + "&StdJobDesc=" + StdJobDesc + "&NombreAgencia=" + NombreAgencia + "&NombreCliente=" + NombreCliente + "&Ruta=" + NombreOrigen + " - " + NombreDestino;
    var ActionRef_viaje = "<%= CONTROLLERPTO %>?ACCION=/Ref_viaje/Evento&opcion=verificar&Distrito="+ Distrito +"&Stdjob="+ StdJobNo +"&Ano="+ Ano +"&Mes=" + strMes;
    var cal = new Calendar(Ano,Mes);
    cal.ShowCalendar();
<%  } %>
</script>
</head >
<body onMouseMove="_Move('capaRP');" onMouseUp="_stopMove();" >
</body>
</html>