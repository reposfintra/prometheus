<!--  
     - Author(s)       :      MARIO FONTALVO
     - Date            :      10/12/2005  
     - Copyright Notice:      TRANSPORTES SANCHEZ POLO S.A.
  -->
 <%--   
     - @(#)  
     - Description: Permite realizar Reportes de Cliente
 --%>
 

<%@page session="true"%> 
<%@page errorPage="/error/ErrorPage.jsp"%>
<%@include file="/WEB-INF/InitModel.jsp"%>

<%@page import="java.util.*" %>
<%@page import="com.tsp.finanzas.presupuesto.model.beans.*" %>
<%@page import="com.tsp.util.UtilFinanzas" %>

<%
    String Ano = modelpto.ReportesSvc.getAno();
    String Mes = modelpto.ReportesSvc.getMes();
    String Distrito = request.getParameter("Distrito");
    String Agencia  = request.getParameter("Agencia");
    
    int anchoCol = 29; 
    int restoCol = 720;
    
    String estiloPtoMyEjt = "style='color:red;        font-weight:bold;'";
    String estiloPtoMnEjt = "style='color:darkgreen;  font-weight:bold;'";
    
    int today = Integer.parseInt(UtilFinanzas.getFechaActual_String(8));
%>

<html>
    <head><title>Reporte [Viajes Presupuestados Vs Viajes Ejecuados] Por Cliente</title>
        <link href="<%= BASEURL %>/css/Style.css" rel='stylesheet'>
        <script>
            function newWindow(url, nombre){
              option="  width="+ (screen.width-30) +", height="+ (screen.height-80)  +",  scrollbars=yes, statusbars=yes, resizable=yes, menubar=no ,top=10 , left=10 ";
              ventana=window.open('',nombre,option);
              ventana.location.href=url;
            }
        
            function go(Cliente){
                var url = "<%= CONTROLLERPTO %>?ACCION=/Opciones/Reportes&Opcion=Generar&Ano=<%= Ano %>&Mes=<%= Mes %>&Distrito=<%= Distrito %>&Agencia=<%= Agencia %>&Tipo=2&Cliente=" + Cliente;
                newWindow(url, 'PE'+Cliente);
            }    
    </script>
    </head>
    <body>

<% 
    List listado = modelpto.ReportesSvc.getListado();
    if (listado.size()>0){
%>
            <%
                for (int i_ag = 0 ; i_ag < listado.size(); i_ag++){
                    ViajesAgencia ag = (ViajesAgencia) listado.get(i_ag);
            %>
        <table border='0' >
            <tr class='titulo'><td height='35'>AGENCIA [<%= ag.getAgencia() %>] <%= ag.getAgenciaNombre() %></td></tr>    
            <tr><td>
                <!-- ******************************* CLIENTES   ************************************ -->
                <br>
                <table border="1">
                <tr><td>
                <table border='0' cellpadding='1' cellspacing='1' width='<%= (anchoCol*62)+restoCol %>'>
                <tr class='subtitulo'>
                    <th rowspan='2'>Cliente</th>
                    <th rowspan='2'>Presup<br>Mensual   </th>
                    <th rowspan='2'>Presup<br>a la Fecha</th>
                    <th rowspan='2'>Ejecut<br>a la Fecha</th>
                    <th rowspan='2'>Difer</th>
                    <th rowspan='2'>Porcen<br>Cumpl</th>
                        <% for (int ix_st = 1 ; ix_st <= 31 ; ix_st++ ) out.print("<th colspan='2'>"+ UtilFinanzas.DiaSemana(Ano + Mes + UtilFinanzas.DiaFormat(ix_st)) + " " + ix_st +"</th>"); %>
                </tr>
                        
                <tr class='subtitulo' >
                        <% for (int ix_st = 1 ; ix_st <= 31 ; ix_st++ ) out.print("<th>Pt</th><th>Ej</th>"); %>
                </tr>
                
                        <%
                            for (int i_cl = 0 ; i_cl < ag.getListadoClientes().size(); i_cl++)      {
                                ViajesCliente cl = (ViajesCliente) ag.getListadoClientes().get(i_cl); 
                        %>

                <tr class='fondotabla'>
                <td class='comentario' width='370' align='left'><a href='javascript: void(0);' onclick="go('<%= cl.getCliente() %>');">[<%= cl.getCliente() %>]</a> <%= cl.getClienteNombre() %></td>
                <td class='comentario' width='70' align='center'><%= cl.getTotalPt()    %></td>
                <td class='comentario' width='70' align='center'><%= cl.getTotalPtF()   %></td>
                <td class='comentario' width='70' align='center'><%= cl.getTotalEj()    %></td>
                <td class='comentario' width='70' align='center'><%= cl.getDiferencia() %></td>
                <td class='comentario' width='70' align='center'><%= UtilFinanzas.customFormat2(cl.getPorcentajeIncumplimiento()) %></td>
                    <% for (int ix_cl = 1; ix_cl <= 31; ix_cl++) {
                        String estilo = "";
                        int fecha = Integer.parseInt(cl.getAno() + cl.getMes() + UtilFinanzas.DiaFormat(ix_cl));
                        if (fecha <= today ){
                            estilo = ((cl.getViajePtdo(ix_cl) > cl.getViajeEjdo(ix_cl))?estiloPtoMyEjt:
                                      (cl.getViajePtdo(ix_cl) < cl.getViajeEjdo(ix_cl))?estiloPtoMnEjt:"");
                        }
                    %>
                    <td class='comentario' align='center' width='<%= anchoCol %>' <%= estilo %> ><%= (cl.getViajePtdo(ix_cl)==0?"-":String.valueOf(cl.getViajePtdo(ix_cl))) %></td>
                    <td class='comentario' align='center' width='<%= anchoCol %>' <%= estilo %> ><%= (cl.getViajeEjdo(ix_cl)==0?"-":String.valueOf(cl.getViajeEjdo(ix_cl))) %></td>
                    <%  } %>
            </tr>
            <%  } %>
            
            
            
                <!-- TOTALES AGENCIAS -->
                <tr class='fondoA'>
                <td class='comentario' width='360'align='right' > <b>Totales</b>     </td>
                <td class='comentario' width='50' align='center'><%= ag.getTotalPt()    %></td>
                <td class='comentario' width='50' align='center'><%= ag.getTotalPtF()   %></td>
                <td class='comentario' width='50' align='center'><%= ag.getTotalEj()    %></td>
                <td class='comentario' width='50' align='center'><%= ag.getDiferencia() %></td>
                <td class='comentario' width='50' align='center'><%= UtilFinanzas.customFormat2(ag.getPorcentajeIncumplimiento()) %></td>
                <% for (int ix_ag = 1; ix_ag <= 31; ix_ag++) {
                    String estilo = "";
                    int fecha = Integer.parseInt(ag.getAno() + ag.getMes() + UtilFinanzas.DiaFormat(ix_ag));
                    if (fecha <= today ){
                        estilo = ((ag.getViajePtdo(ix_ag) > ag.getViajeEjdo(ix_ag))?estiloPtoMyEjt:
                                  (ag.getViajePtdo(ix_ag) < ag.getViajeEjdo(ix_ag))?estiloPtoMnEjt:"");
                    }
                %>
                        <td class='comentario' align='center' width='<%= anchoCol %>' <%= estilo %> ><%= (ag.getViajePtdo(ix_ag)==0?"-":String.valueOf(ag.getViajePtdo(ix_ag))) %></td>
                        <td class='comentario' align='center' width='<%= anchoCol %>' <%= estilo %> ><%= (ag.getViajeEjdo(ix_ag)==0?"-":String.valueOf(ag.getViajeEjdo(ix_ag))) %></td>
                <%  } %>
                </tr>
                <!-- FIN TOTALES AGENCIAS -->
            
            
            
            
            </table>
            </td></tr></table>

                
            <!-- *****************************  FIN CLIENTES  ************************************ -->
            </td>
            </tr>
        </table>
        <br><br>
        <%  } // end for %>        
        
<%   }     // end if %>

    </body>
</html>
