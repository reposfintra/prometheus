 <!--  
     - Author(s)       :      MARIO FONTALVO
     - Date            :      10/12/2005  
     - Copyright Notice:      TRANSPORTES SANCHEZ POLO S.A.
  -->
 <%--   
     - @(#)  
     - Description: Formulario seleccion de presupuesto atraves de agencias, clientes, y estandares
 --%>


<%@page session="true"%>
<%@page errorPage="/error/ErrorPage.jsp"%>
<%@page import="java.util.*" %>
<%@include file="/WEB-INF/InitModel.jsp"%>
<%@page import="com.tsp.finanzas.presupuesto.model.beans.*" %>
<%@taglib uri="http://jakarta.apache.org/taglibs/input-0.90" prefix="input" %>
<%
    String path = application.getRealPath(model.menuService.getRealUrl(request.getRequestURI()));
    String datos[] = model.menuService.getContenidoMenu(BASEURL,request.getRequestURI(),path);
%>
<html>
<head>
    <title>Presupuesto de Ventas</title>
    <script src="<%= BASEURL %>/js/boton.js"></script>
    <link   href="<%= BASEURL %>/css/estilostsp.css" rel='stylesheet'>
    <script>
    <%@include file="/jsp/finanzas/presupuesto/datosEstandarCombos.jsp"%>
        function addOption(Comb,valor,texto){
                var Ele = document.createElement("OPTION");
                Ele.value=valor;
                Ele.text=texto;
                Comb.add(Ele);
        }    
        function Llenar(CmbAnno, CmbMes, TipoVista){
                var Meses    = new Array('Enero','Febrero','Marzo','Abril','Mayo','Junio','Julio','Agosto','Septiembre','Octubre','Noviembre','Diciembre');
                var FechaAct = new Date();
                CmbAnno.length = 0;
                CmbMes.length  = 0;
                
                for (i=FechaAct.getYear()-10;i<=FechaAct.getYear()+10;i++) addOption(CmbAnno,i,i)
                CmbAnno.value = FechaAct.getYear();
                if (TipoVista.value=='M') addOption(CmbMes,'TD','TODOS');
                for (i=0;i<Meses.length;i++)
                    if ((i+1)<10) addOption(CmbMes,'0'+(i+1),Meses[i]);
                    else          addOption(CmbMes,(i+1),Meses[i]);                
                CmbMes.value  = ((FechaAct.getMonth()+1)<10)?('0'+(FechaAct.getMonth()+1)):(FechaAct.getMonth()+1);  
               
                if (TipoVista.value == 'D')
                    rango.style.display = 'block';
                else
                    rango.style.display = 'none';
                
	
        }
        
        
        function LoadAgencias(CmbAgencias,CmbClientes, CmbOrigenes, CmbDestino, CmbStandar){
	   CmbAgencias.length=0;
	   var  aux='?';
	   if (datos.length>0){
               for (i=0;i<datos.length;i++){
                 var info=(new String(datos[i])).split(separador);
                 if (aux!=info[1]){
                     addOption(CmbAgencias,info[0],info[1]);
                     aux=info[1];
                 }
               }
               addOption(CmbAgencias,'','TODAS LAS AGENCIAS'); 
            }else
               addOption(CmbAgencias,'NINGUNO','NO SE ENCONTRARON AGENCIAS');
            LoadClientes(CmbAgencias,CmbClientes, CmbOrigenes, CmbDestino, CmbStandar);
	} 
	
	
       	function LoadClientes(CmbAgencias,CmbClientes, CmbOrigenes, CmbDestino, CmbStandar){
	   CmbClientes.length=0;
	   var  aux='?';
	   if (datos.length>0){	  
               if (CmbAgencias.value!='')
                   for (i=0;i<datos.length;i++){
                     var info=(new String(datos[i])).split(separador);
                     if (CmbAgencias.value==info[0])
                        if (aux!=info[3]){
                             addOption(CmbClientes,info[2],info[3]);
                             aux=info[3];
                        }
                   }
                addOption(CmbClientes,'','TODOS LOS CLIENTES');
            }else
               addOption(CmbClientes,'NINGUNO','NO SE ENCONTRARON CLIENTES');  
               
           LoadOrigenes (CmbAgencias,CmbClientes, CmbOrigenes, CmbDestino, CmbStandar);
	}
	
	
        function LoadOrigenes(CmbAgencias,CmbClientes, CmbOrigenes, CmbDestino, CmbStandar){
           CmbOrigenes.length=0;
           var  aux='?';

           if (datos.length>0){
                   if (CmbClientes.value!='')
                   for (i=0;i<datos.length;i++){
                     var info=(new String(datos[i])).split(separador);
                     if (CmbAgencias.value==info[0] && CmbClientes.value==info[2])
                        if (aux!=info[7]){
                             addOption(CmbOrigenes,info[6],info[7]);
                             aux=info[7];
                        }
                   }
                addOption(CmbOrigenes,'','TODOS LOS ORIGENES');
            }else
               addOption(CmbOrigenes,'NINGUNO','NO SE ENCONTRARON ORIGENES');
               
            LoadDestino (CmbAgencias,CmbClientes, CmbOrigenes, CmbDestino, CmbStandar);
        } 
   
        function LoadDestino (CmbAgencias,CmbClientes, CmbOrigenes, CmbDestino, CmbStandar){
           CmbDestino.length=0;
           var  aux='?';

           if (datos.length>0){
                   if (CmbOrigenes.value!='')
                   for (i=0;i<datos.length;i++){
                     var info=(new String(datos[i])).split(separador);
                     if (CmbAgencias.value==info[0] && CmbClientes.value==info[2] && CmbOrigenes.value==info[6])
                        if (aux!=info[9]){
                             addOption(CmbDestino,info[8],info[9]);
                             aux=info[9];
                        }
                   }
                addOption(CmbDestino,'','TODOS LOS DESTINOS');
            }else
               addOption (CmbClientes,'NINGUNO','NO SE ENCONTRARON DESTINOS');
            LoadEstandar (CmbAgencias,CmbClientes, CmbOrigenes, CmbDestino, CmbStandar);
        }
	
    
        function LoadEstandar(CmbAgencias,CmbClientes, CmbOrigenes, CmbDestino, CmbStandar){
	   CmbStandar.length=0;
	   var  aux='?';
	   if (datos.length>0){	   
               if (CmbDestino.value!='')	   
               for (i=0;i<datos.length;i++){
                 var info=(new String(datos[i])).split(separador);
                 if (CmbAgencias.value==info[0] && CmbClientes.value==info[2] && CmbOrigenes.value==info[6] && CmbDestino.value==info[8])
                    if (aux!=info[5]){
                         addOption(CmbStandar,info[4],info[5]);
                         aux=info[5];
                    }
               }
               addOption(CmbStandar,'','TODOS LOS ESTANDARES'); 
            }else
               addOption(CmbStandar,'NINGUNO','NO SE ENCONTRARON ESTANDARES');               
          }
          
          
      function _onsubmit (tform){
          with (tform){
            var diaI = parseFloat(diaInicial.value);
            var diaF = parseFloat(diaFinal.value);
            if (diaI>diaF){
               alert ('El dia final debe ser mayor que el dia Inicial, por favor rectifiquelo para continuar');
               return false;
            }          
            var url = "<%= CONTROLLERPTO %>?ACCION=/Opciones/Presupuesto&Agencia="+ Agencia.value + "&Cliente=" + Cliente.value + "&sOrigen=" + sOrigen.value + "&sDestino=" + sDestino.value + "&Stdjob=" + Stdjob.value +"&TipoVista=" + TipoVista.value + "&Ano=" + Ano.value + "&Mes=" + Mes.value + "&diaInicial="+ diaInicial.value +"&diaFinal="+ diaFinal.value +"&AgenciaDespacho="+ AgenciaDespacho.value + "&Origen=" + Origen.value + "&Opcion="+ Opcion.value + "&Filtro=" + (Filtro.checked?Filtro.value:'');
            //newWindow(url, 'PtoVentas');
            window.location.href = url;
          }
          return false;
      }
      function newWindow(url, nombre){
          option="  width="+ (screen.width-30) +", height="+ (screen.height-80)  +",  scrollbars=yes, statusbars=yes, resizable=yes, menubar=no ,top=10 , left=10 ";
          ventana=window.open('',nombre,option);
          ventana.location.href=url;
          ventana.focus();
       }
       
       function cargardias (CmbDiaI, CmbDiaF){
                for (i=1;i<=31;i++){
                    dia = (i<10?'0'+i:i);
                    addOption(CmbDiaI,dia,dia);
                    addOption(CmbDiaF,dia,dia);                
                }
                CmbDiaI.value = '01';
                CmbDiaF.value = '31';
       }
    </script>
</head>
<body onResize="redimensionar()" onLoad="redimensionar()">
<div id="capaSuperior" style="position:absolute; width:100%; height:100px; z-index:0; left: 0px; top: 0px;">
 <jsp:include page="/toptsp.jsp?encabezado=PRESUPUESTO DE VENTAS"/>
</div>
<div id="capaCentral" style="position:absolute; width:100%; height:87%; z-index:0; left: 0px; top: 100px; overflow: scroll;">
<center>


<form action="<%= CONTROLLERPTO %>?ACCION=/Opciones/Presupuesto" method="post" name="fpto" onsubmit='javascript: return _onsubmit(this);'>
<table width="550" border="2" bgcolor="#F7F5F4">
<tr><td align="center" >

    <table width="100%" class="tablaInferior">
    <tr>
           <td colspan='2'>
            <table border='0' width='100%' cellspacing='0'>
                <tr>
                 <td class='subtitulo1'  width="80%">PRESUPUESTO DE VENTA</td>
                 <td class='barratitulo' width="20%"><img src="<%= BASEURL %>/images/titulo.gif" width="32" height="20" align='left'><%=datos[0]%></td>
                </tr>
            </table>
           </td>
    </tr>

    <tr class="fila">
        <td width="30%">&nbsp;Agencia</td>
        <td width="*"><select name="Agencia" class="select" style="width:100%;" onchange='LoadClientes(this,Cliente,sOrigen, sDestino ,Stdjob);'></td>
    </tr> 
    <tr class="fila">
        <td >&nbsp;Cliente</td>
        <td ><select name="Cliente" class="select" style="width:100%" onchange='LoadOrigenes(Agencia,this, sOrigen, sDestino, Stdjob);'></select></td>
    </tr>   
    <tr class="fila">
        <td >&nbsp;Origen</td>
        <td ><select name="sOrigen" class="select" style="width:100%;" onchange='LoadDestino(Agencia,Cliente, sOrigen, sDestino, Stdjob);'></td>
    </tr>    
    <tr class="fila">
        <td >&nbsp;Destino</td>
        <td ><select name="sDestino" class="select" style="width:100%;" onchange='LoadEstandar(Agencia,Cliente, sOrigen, sDestino, Stdjob);'></td>
    </tr>        
    <tr class="fila">
        <td >&nbsp;Estandar</td>
        <td ><select name="Stdjob" class="select" style="width:100%;"></td>
    </tr>    
    
    <tr class="fila">
            <td >&nbsp;Agencia Despacho</td>
            <td ><input:select name="AgenciaDespacho" options="<%=model.agenciaService.getCbxAgencia()%>" attributesText="class='textbox' style='width:100%;'" /></td>
    </tr>
    
    <tr class="fila">
        <td >&nbsp;Tipo de Vista</td>
        <td >
            <select class="select" style="width:100%" name="TipoVista" onchange="jscript: Llenar(Ano,Mes,this);">
            <option value="M">Mensual</option>
            <option value="S">Semanal</option>
            <option value="D">Diaria</option>
            </select>
        </td>
    </tr>    
    <tr class="fila">
        <td >&nbsp;Periodo</td>
        <td >
            <select class="select" style="width:49%" name="Ano"></select>
            <select class="select" style="width:49%" name="Mes"></select>
        </td>
    </tr>   
    <tr class="fila" id='rango' style='display:none'>
        <td >&nbsp;Rango de dias</td>
        <td >
            <select class="select" style="width:15%" name="diaInicial"></select>
            <select class="select" style="width:15%" name="diaFinal"></select>
        </td>
    </tr>    
    <tr class="fila">
        <td colspan="2"><input type="checkbox" name="Filtro" value="Ok"> Mostrar solo estandares con presupuesto.</td>
    </tr>     
    </table>
    
  


</td></tr>
</table>
    
    <br>
    <input type='hidden' name='Origen' value='PtoFormCmb'>
    <input type="hidden" name="Opcion" value="Ver Presupuesto" >

<img src='<%=BASEURL%>/images/botones/buscar.gif'  style='cursor:hand' onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" onclick=" _onsubmit(fpto); ">
&nbsp;
<img src='<%=BASEURL%>/images/botones/salir.gif'   style='cursor:hand' onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" onclick='window.close();'>        



<script> 
Llenar(fpto.Ano,fpto.Mes,fpto.TipoVista); 
LoadAgencias(fpto.Agencia,fpto.Cliente,fpto.sOrigen,fpto.sDestino,fpto.Stdjob);
cargardias (fpto.diaInicial, fpto.diaFinal);
fpto.AgenciaDespacho.selectedIndex = 1;
</script>
</form>
</center>
</div>
<%=datos[1]%>
</body>
</html>
