<!--  
     - Author(s)       :      MARIO FONTALVO
     - Date            :      10/12/2005  
     - Copyright Notice:      TRANSPORTES SANCHEZ POLO S.A.
  -->
 <%--   
     - @(#)  
     - Description: Permite realizar Reportes de Pto
 --%>
 

<%@page session="true"%> 
<%@page errorPage="/error/ErrorPage.jsp"%>
<%@page import="java.util.*" %>
<%@include file="/WEB-INF/InitModel.jsp"%>
<%@page import="com.tsp.finanzas.presupuesto.model.beans.*" %>
<html>
    <head>
        <title>Reporte de Cumplimiento Presupuestado Vs Ejecutado</title>
        <link href="<%= BASEURL %>/css/Style.css" rel='stylesheet'>
        <script>
        <%@include file="/jsp/finanzas/presupuesto/datosEstandarCombos.jsp"%>
        function addOption(Comb,valor,texto){
                var Ele = document.createElement("OPTION");
                Ele.value=valor;
                Ele.text=texto;
                Comb.add(Ele);
        }    
        function Llenar(CmbAnno, CmbMes, TipoVista){
                var Meses    = new Array('Enero','Febrero','Marzo','Abril','Mayo','Junio','Julio','Agosto','Septiembre','Octubre','Noviembre','Diciembre');
                var FechaAct = new Date();
                CmbAnno.length = 0;
                CmbMes.length  = 0;
                
                for (i=FechaAct.getYear()-10;i<=FechaAct.getYear()+10;i++) addOption(CmbAnno,i,i)
                CmbAnno.value = FechaAct.getYear();
                for (i=0;i<Meses.length;i++)
                    if ((i+1)<10) addOption(CmbMes,'0'+(i+1),Meses[i]);
                    else          addOption(CmbMes,(i+1),Meses[i]);                
                CmbMes.value  = ((FechaAct.getMonth()+1)<10)?('0'+(FechaAct.getMonth()+1)):(FechaAct.getMonth()+1);                 
	
        } 
        function LoadAgencias(CmbAgencias,CmbClientes,CmbStandar){
	   CmbAgencias.length=0;
	   var  aux='?';
	   if (datos.length>0){
               for (i=0;i<datos.length;i++){
                 var info=(new String(datos[i])).split(separador);
                 if (aux!=info[0]){
                     addOption(CmbAgencias,info[0],info[1]);
                     aux=info[0];
                 }
               }
               addOption(CmbAgencias,'','TODAS LAS AGENCIAS'); 
            }else
               addOption(CmbAgencias,'NINGUNO','NO SE ENCONTRARON AGENCIAS');
            CmbAgencias.value='';
            LoadClientes(CmbAgencias,CmbClientes,CmbStandar);
	} 
       	function LoadClientes(CmbAgencias,CmbClientes,CmbStandar){
	   CmbClientes.length=0;
	   var  aux='?';
	   if (datos.length>0){	  
               if (CmbAgencias.value!='')
                   for (i=0;i<datos.length;i++){
                     var info=(new String(datos[i])).split(separador);
                     if (CmbAgencias.value==info[0])
                        if (aux!=info[2]){
                             addOption(CmbClientes,info[2],info[3]);
                             aux=info[2];
                        }
                   }
                addOption(CmbClientes,'','TODOS LOS CLIENTES'); 
            }else
               addOption(CmbClientes,'NINGUNO','NO SE ENCONTRARON CLIENTES'); 
           CmbClientes.value = '';
           LoadStandar(CmbAgencias,CmbClientes,CmbStandar);
	}
        function LoadStandar(CmbAgencias,CmbClientes,CmbStandar){
	   CmbStandar.length=0;
	   var  aux='?';
	   if (datos.length>0){	   
               if (CmbClientes.value!='')	   
               for (i=0;i<datos.length;i++){
                 var info=(new String(datos[i])).split(separador);
                 if (CmbAgencias.value==info[0] && CmbClientes.value==info[2] )
                    if (aux!=info[4]){
                         addOption(CmbStandar,info[4],info[5]);
                         aux=info[4];
                    }
               }
               addOption(CmbStandar,'','TODOS LOS STANDARES'); 
            }else
               addOption(CmbStandar,'NINGUNO','NO SE ENCONTRARON STNADARES');   
            CmbStandar.value = '';
	}
        </script>   

    
    </head>
    <body>
        <center>
        <form action='<%= CONTROLLERPTO %>?ACCION=/Opciones/Reportes' method='post' name='formulario'>
        <table width='500' class='fondotabla' border='1' >
            <tr><th class='titulo'>REPORTES DE VENTAS</th></tr>
            <tr>
                <td align='center'>
                    <br>
                    <table width='100%' align='center' >
                        <tr class="comentario">
                            <td width="20%">Distrito</td>
                            <td width="*"><select name="Distrito" class="select" style="width:120;" ><option value='FINV'>FINV</option></select></td>
                        </tr> 
                        <tr class="comentario">
                            <td width="20%">Agencia</td>
                            <td width="*"><select name="Agencia" class="select" style="width:100%;" onclick='LoadClientes(this,Cliente,Estandar);'></select></td>
                        </tr> 
                        <tr class="comentario">
                            <td width="20%">Cliente</td>
                            <td width="*"><select name="Cliente" class="select" style="width:100%" onclick='LoadStandar(Agencia,this,Estandar);'></select></td>
                        </tr>    
                        <tr class="comentario">
                            <td width="20%">Standar Job </td>
                            <td width="*"><select name="Estandar" class="select" style="width:100%;"></td>
                        </tr>                     
                        <tr><td class='comentario' >Periodo</td></tr>
                        <tr>
                            <td colspan='2' align='center'>
                                <select class="select" style="width:29%" name="Ano"></select>
                                <select class="select" style="width:28%" name="Mes"></select>    
                            </td>    
                        </tr>
                        <tr><td class='comentario'>Tipo de Reporte</td>
                            <td>
                                <select class="select" style="width:95%" name="Tipo">
                                <option value='0'>Agencia</option>
                                <option value='1'>Cliente</option>
                                <option value='2'>Estandar</option>
                                </select>
                            </td>    
                        </tr>
                    </table>
                    <br>
                    <input type='submit'  value='Generar' class='boton' style='width:120;' name='Opcion'>
                    <br>&nbsp;
                </td>
            </tr>
        </table>
        </form>
        
        <script> 
            Llenar(formulario.Ano,formulario.Mes); 
            LoadAgencias(formulario.Agencia,formulario.Cliente,formulario.Estandar);
        </script>
        
        </center>
    </body>
</html>
