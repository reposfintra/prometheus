<!--  
     - Author(s)       :      MARIO FONTALVO
     - Date            :      10/12/2005  
     - Copyright Notice:      TRANSPORTES SANCHEZ POLO S.A.
  -->
 <%--   
     - @(#)  
     - Description: Formulario exportar el presupuesto de excel
 --%> 
<%@page session="true"%> 
<%@page errorPage="/error/ErrorPage.jsp"%>
<%@include file="/WEB-INF/InitModel.jsp"%>
<%
    String Mensaje = (request.getParameter("Mensaje")!=null?request.getParameter("Mensaje"):"");
%>
<html>
<head>
    <title>Presupuesto de Ventas</title>
    <script src="<%= BASEURL %>/js/boton.js"></script>
    <link   href="<%= BASEURL %>/css/estilostsp.css" rel='stylesheet'>
    <script>
        function addOption(Comb,valor,texto){
            var Ele = document.createElement("OPTION");
            Ele.value=valor;
            Ele.text=texto;
            Comb.add(Ele);
        }    
        function Llenar(CmbAnno, CmbMes){
                var Meses    = new Array('Enero','Febrero','Marzo','Abril','Mayo','Junio','Julio','Agosto','Septiembre','Octubre','Noviembre','Diciembre');
                var FechaAct = new Date();
                CmbAnno.length = 0;
                CmbMes.length  = 0;                
                for (i=FechaAct.getYear()-10;i<=FechaAct.getYear()+10;i++) addOption(CmbAnno,i,i)
                CmbAnno.value = FechaAct.getYear();
                for (i=0;i<Meses.length;i++)
                    if ((i+1)<10) addOption(CmbMes,'0'+(i+1),Meses[i]);
                    else          addOption(CmbMes,(i+1),Meses[i]);                
                CmbMes.value  = ((FechaAct.getMonth()+1)<10)?('0'+(FechaAct.getMonth()+1)):(FechaAct.getMonth()+1);	
        } 
     </script>   
</head>
<body>
<div id="capaSuperior" style="position:absolute; width:100%; height:100px; z-index:0; left: 0px; top: 0px;">
 <jsp:include page="/toptsp.jsp?encabezado=PRESUPUESTO DE VENTAS"/>
</div>
<div id="capaCentral" style="position:absolute; width:100%; height:87%; z-index:0; left: 0px; top: 100px; overflow: scroll;">
<center>


    <form action='<%= CONTROLLERPTO %>?ACCION=/Opciones/ImportarPto' method='post' name='fpto' enctype='multipart/form-data'>
    <table border='2' width='420' bgcolor="#F7F5F4">
    <tr><td class='comentario' align='center'>
    <!--  ------------------------------------------------------- -->
        <table  width='99%' class='tablaInferior'>
        <tr class='fila'><td >
        
                       <table border='0' width='100%' cellspacing='0'>
                          <tr>
                             <td class='subtitulo1'  width="80%">IMPORTACION PRESUPUESTO</td>
                             <td class='barratitulo' width="20%"><img src="<%= BASEURL %>/images/titulo.gif" width="32" height="20"></td>
                          </tr>
                        </table>
        
        
        </td></tr>        
        <tr class='fila'><td >&nbsp;Archivo de excel</td></tr>
        <tr class='fila'><td >&nbsp;<input type='file' name='Archivo' class='input' style='text-align:center'></td></tr>
        <tr class='fila'><td >&nbsp;Periodo a aplicar</td></tr>
        <tr class='fila'><td >&nbsp;
                <select class="select" style="width:48%" name="Ano"></select>
                <select class="select" style="width:48%" name="Mes"></select>    
            </td></tr>    
        <tr class='fila'><td >&nbsp;</td></tr>    
        <tr class='fila'><td >&nbsp;Tipo de Datos del Archivo</td></tr>
        <tr class='fila'><td><input type='radio' name='Modo' value='M' checked>Mensual &nbsp;  <input type='radio' name='Modo' value='D'>Diario </td></tr>

        <tr class='fila'><td >&nbsp;</td></tr>    
        <tr class='fila'><td ><span style="width:40%">&nbsp;Formato del Archivo</span>
               <select name='Formato' class='select' style="width:55%">
                   <option value='Original' >Original</option>
                   <option value='Nuevo'    >Prog. Presupuesto</option>
               </select>    
        </td></tr>


        <tr class='fila'><td ><span style="width:40%">&nbsp;Presupuesto Actual</span>
               <select name='Accion' class='select' style="width:55%">
                   <option value='A' >Actualizarlo</option>
                   <option value='E' >Desecharlo  </option>
               </select>    
        </td></tr>    
        </table>
    <!--  ------------------------------------------------------- -->
       
    </td></tr>
    </table>
        
    <br>
     
        <input type="hidden" name="Opcion" value="Importar">                
        <img src='<%=BASEURL%>/images/botones/aceptar.gif'  style='cursor:hand' onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" onclick="  fpto.submit();  ">
        &nbsp;
        <img src='<%=BASEURL%>/images/botones/salir.gif'   style='cursor:hand' onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" onclick='window.close();'>        
     
    </form>
    <script> 
    Llenar(fpto.Ano,fpto.Mes); 
    </script>

    
<% if (!Mensaje.equals("")) { %>
    
        <table border="2" align="center">
           <tr>
           <td><table width="100%"  border="1" align="center"  bordercolor="#F7F5F4" bgcolor="#FFFFFF">
                <tr>
                 <td width="320" align="center" class="mensajes"><%=Mensaje%></td>
                 <td width="29" background="<%=BASEURL%>/images/cuadronaranja.JPG">&nbsp;</td>
                 <td width="58">&nbsp;</td>
                </tr>
              </table>
           </td>
           </tr>
        </table>  
        
<% } %>    

    </center>
</div>    
</body>
</html>
