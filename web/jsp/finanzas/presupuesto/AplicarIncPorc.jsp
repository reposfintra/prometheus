<!--  
     - Author(s)       :      MARIO FONTALVO
     - Date            :      10/12/2005  
     - Copyright Notice:      TRANSPORTES SANCHEZ POLO S.A.
  -->
 <%--   
     - @(#)  
     - Description: Formulario de Aplicar incrementos porcentuales
 --%> 
<%@page session="true"%> 
<%@page errorPage="/error/ErrorPage.jsp"%>
<%@page import="java.*" %>
<%@page import="java.util.*" %>
<%@include file="/WEB-INF/InitModel.jsp"%>
<%@page import="com.tsp.finanzas.presupuesto.model.beans.*" %>
<%@page import="com.tsp.util.UtilFinanzas" %>
<html>
<head>
<script src="<%= BASEURL %>/js/boton.js"></script>
<link   href="<%= BASEURL %>/css/estilostsp.css" rel='stylesheet'>
<title>Incrementos Porcentuales</title>
<script>
<%= modelpto.IPorcentualesSvc.getVarJSSeparador() %>
<%= modelpto.IPorcentualesSvc.getVarJSInc() %>
    function addOption(Comb,valor,texto){
            var Ele = document.createElement("OPTION");
            Ele.value=valor;
            Ele.text=texto;
            Comb.add(Ele);
    } 
    function LlenarAnosUnidades(CmbAnno, CmbMes){
        var Meses    = new Array('Enero','Febrero','Marzo','Abril','Mayo','Junio','Julio','Agosto','Septiembre','Octubre','Noviembre','Diciembre');
        var FechaAct = new Date();
        CmbAnno.length = 0;
        CmbMes.length  = 0;
        var  aux='?';
        if (datos.length>0){
           for (i=0;i<datos.length;i++){
             var info=(new String(datos[i])).split(separador);
             if (aux!=info[0]){
                 addOption(CmbAnno,info[0],info[0]);
                 aux=info[0];
             }
           }
           for (i=0;i<Meses.length;i++)
              if ((i+1)<10) addOption(CmbMes,'0'+(i+1),Meses[i]);
              else          addOption(CmbMes,(i+1),Meses[i]);                
           CmbMes.value  = ((FechaAct.getMonth()+1)<10)?('0'+(FechaAct.getMonth()+1)):(FechaAct.getMonth()+1);                 
         }
    }
    
    function LlenarUnidades(CmbAnno, CmbMes, CmbCUnidad, TxtDUnidad, TxtIUnidad){
        CmbCUnidad.length  = 0;
        if (datos.length>0){
           for (i=0;i<datos.length;i++){
             var info=(new String(datos[i])).split(separador);
             if (CmbAnno.value == info[0])
                 addOption(CmbCUnidad,info[1],info[1]);
           }
        } 
        infoUnidad(CmbAnno, CmbMes, CmbCUnidad, TxtDUnidad, TxtIUnidad);
    } 
    
    function infoUnidad(CmbAnno, CmbMes, CmbCUnidad, TxtDUnidad, TxtIUnidad){
        if (datos.length>0){
           for (i=0;i<datos.length;i++){
             var info=(new String(datos[i])).split(separador);
             if (CmbAnno.value == info[0] && CmbCUnidad.value == info[1]){
                 TxtDUnidad.value = info[2];
                 TxtIUnidad.value =    info[( 3 + parseFloat(CmbMes.value) )];   //(Modo[0].checked ? info[3] : info[( 3 + parseFloat(CmbMes.value) )] );
                 break;
             }
           }
        }     
    }
    

    
    function LlenarAnos(CmbAnno, CmbMes){
        var Meses    = new Array('Enero','Febrero','Marzo','Abril','Mayo','Junio','Julio','Agosto','Septiembre','Octubre','Noviembre','Diciembre');
        var FechaAct = new Date();
        CmbAnno.length = 0;
        CmbMes.length  = 0;
        for (i=FechaAct.getYear()-10;i<=FechaAct.getYear()+10;i++) addOption(CmbAnno,i,i)
        CmbAnno.value = FechaAct.getYear();
        for (i=0;i<Meses.length;i++)
            if ((i+1)<10) addOption(CmbMes,'0'+(i+1),Meses[i]);
            else          addOption(CmbMes,(i+1),Meses[i]);                
        CmbMes.value  = ((FechaAct.getMonth()+1)<10)?('0'+(FechaAct.getMonth()+1)):(FechaAct.getMonth()+1);                 

    }  
   
    function establecerPeriodo (CmbAnno, CmbMes, CmbAnno2, CmbMes2){ 
       CmbMes2.value  = (CmbMes.value == '01' ? '12' : ( parseFloat(CmbMes.value) < 11 ? '0': '' ) +  new String(parseFloat(CmbMes.value)-1));
       CmbAnno2.value = (CmbMes.value == '01' ? new String(parseFloat(CmbAnno.value)-1) : CmbAnno.value);
    }    
    
    function submit_AplicarIP(tform){
      with (tform){
         var periodoInc = parseFloat( Ano.value  +  Mes.value );
         var periodoBas = parseFloat( Ano2.value +  Mes2.value);
         if (periodoInc <= periodoBas){
            alert ('Atencion el periodo que desea incrementar debe ser mayor que el periodo Base.');
            return false;
         }
         if (IUnidad.value == '0.000'){
            alert ('Atencion para poder aplicar el incremento este debe ser mayor a 0.000');
            return false;
         }
         return true;
      }
    }
</script>
</head>

<body>
<div id="capaSuperior" style="position:absolute; width:100%; height:100px; z-index:0; left: 0px; top: 0px;">
 <jsp:include page="/toptsp.jsp?encabezado=INCREMENTOS PORCENTUALES"/>
</div>
<div id="capaCentral" style="position:absolute; width:100%; height:87%; z-index:0; left: 0px; top: 100px; overflow: scroll;">
<center>



<form action="<%= CONTROLLERPTO %>?ACCION=/Opciones/AplicarIP" method="post" name="formIP" >
<table width="500" border="2" bgcolor="#F7F5F4" >
  <tr>
    <td align="center">

	<table width="100%"  border="0" class="tablaInferior">

        <tr>
           <td colspan='3'>
            <table border='0' width='100%' cellspacing='0'>
                <tr>
                 <td class='subtitulo1'  width="80%">APLICAR INCREMENTOS PORCENTUALES</td>
                 <td class='barratitulo' width="20%"><img src="<%= BASEURL %>/images/titulo.gif" width="32" height="20"></td>
                </tr>
            </table>
           </td>
        </tr>	
	
	  <tr class="fila">
		<td width="30%">&nbsp;Periodo a Aplicar</td>
		<td width="20%"><select name="Ano" class="select" style="width:100%" onchange="LlenarUnidades(this, Mes, CUnidad, DUnidad, IUnidad, Modo); establecerPeriodo  (Ano, Mes, Ano2, Mes2); "></select></td>
		<td width="50%"><select name="Mes" class="select" style="width:100"  onchange="infoUnidad(Ano, this, CUnidad, DUnidad, IUnidad, Modo);     establecerPeriodo  (Ano, Mes, Ano2, Mes2); "></select></td>
	  </tr>	
	  <tr class="fila">
		<td width="30%">&nbsp;Unidad de Neg</td>
		<td width="20%"><select name="CUnidad" class="select" style="width:100%" onchange="infoUnidad(Ano, Mes, this, DUnidad, IUnidad, Modo);"></select></td>
		<td width="50%"><input type="text"  name="DUnidad" class="input" readonly></td>
	  </tr>
	  <tr class="fila">
		<td width="30%">&nbsp;Valor Incremento</td>
		<td width="50%" colspan="2"><input type="text"  name="IUnidad" class="input"  style="width:120; text-align:center;"><b>%</b></td>
	  </tr>
	
	  
	  <tr class="fila"><td colspan='3'>&nbsp;</td> </tr>
	  
	  
	  
	  <tr class="fila">
		<td width="30%">&nbsp;Aplicar sobre </td>
		<td width="20%"><select name="Ano2" class="select" style="width:100%" ></select></td>
		<td width="50%"><select name="Mes2" class="select" style="width:100"  ></select></td>
	  </tr>		  
	  
 	  <tr class="fila"><td colspan='3'>&nbsp;</td> </tr>

	  

	  <tr class="fila">	
                <td colspan='3'>&nbsp;Modo de Aplicar Incrementos </td> </tr>
	  <tr class="fila" >	
                <td colspan='3'>&nbsp;&nbsp;<input type="radio" name="Modo" value="Normal"  checked  >Solo para este mes</td> </tr>
	  <tr class="fila">	
                <td colspan='3'>&nbsp;&nbsp;<input type="radio" name="Modo" value="Cascada"          >En cascada hasta el final del a�o</td> </tr>
	 
	</table> 
	
    </td></tr>
</table>

	
<input type="hidden" name="Opcion" value="Aplicar Incremento" >
<br>
<img src='<%=BASEURL%>/images/botones/aceptar.gif' style='cursor:hand' onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" onclick=" if (submit_AplicarIP(formIP)) { formIP.submit(); } ">
&nbsp;
<img src='<%=BASEURL%>/images/botones/salir.gif'   style='cursor:hand' onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" onclick='window.close();'>        

	
</form>
<script>
  LlenarAnosUnidades (formIP.Ano, formIP.Mes);
  LlenarUnidades     (formIP.Ano, formIP.Mes, formIP.CUnidad, formIP.DUnidad, formIP.IUnidad, formIP.Modo);
  LlenarAnos         (formIP.Ano2, formIP.Mes2);
  establecerPeriodo  (formIP.Ano, formIP.Mes, formIP.Ano2, formIP.Mes2);
</script>
</center>

</div>

</body>
</html>
