 <!--  
     - Author(s)       :      MARIO FONTALVO
     - Date            :      10/12/2005  
     - Copyright Notice:      TRANSPORTES SANCHEZ POLO S.A.
  -->
 <%--   
     - @(#)  
     - Description: Formulario seleccion de parametros de exportacion de Historial
 --%>


<%@page session="true"%>
<%@page errorPage="/error/ErrorPage.jsp"%>
<%@page import="java.util.*" %>
<%@include file="/WEB-INF/InitModel.jsp"%>
<%@page import="com.tsp.finanzas.presupuesto.model.beans.*" %>
<% String Mensaje = (request.getParameter("Mensaje")!=null?request.getParameter("Mensaje"):""); %>
<html>
<head>
    <title>Presupuesto de Ventas</title>
    <script src="<%= BASEURL %>/js/boton.js"></script>
    <link   href="<%= BASEURL %>/css/estilostsp.css" rel='stylesheet'>
    <script>
    <%@include file="/jsp/finanzas/presupuesto/datosEstandarCombos.jsp"%>
        function addOption(Comb,valor,texto){
                var Ele = document.createElement("OPTION");
                Ele.value=valor;
                Ele.text=texto;
                Comb.add(Ele);
        }    
        function Llenar(CmbAnno, CmbMes, TipoVista){
                var Meses    = new Array('Enero','Febrero','Marzo','Abril','Mayo','Junio','Julio','Agosto','Septiembre','Octubre','Noviembre','Diciembre');
                var FechaAct = new Date();
                CmbAnno.length = 0;
                CmbMes.length  = 0;
                
                for (i=FechaAct.getYear()-10;i<=FechaAct.getYear()+10;i++) addOption(CmbAnno,i,i)
                CmbAnno.value = FechaAct.getYear();
                for (i=0;i<Meses.length;i++)
                    if ((i+1)<10) addOption(CmbMes,'0'+(i+1),Meses[i]);
                    else          addOption(CmbMes,(i+1),Meses[i]);                
                CmbMes.value  = ((FechaAct.getMonth()+1)<10)?('0'+(FechaAct.getMonth()+1)):(FechaAct.getMonth()+1);                 
	
        }
        function LoadAgencias(CmbAgencias,CmbClientes,CmbStandar){
	   CmbAgencias.length=0;
	   var  aux='?';
	   if (datos.length>0){
               for (i=0;i<datos.length;i++){
                 var info=(new String(datos[i])).split(separador);
                 if (aux!=info[0]){
                     addOption(CmbAgencias,info[0],info[1]);
                     aux=info[0];
                 }
               }
               addOption(CmbAgencias,'','TODAS LAS AGENCIAS'); 
            }else
               addOption(CmbAgencias,'NINGUNO','NO SE ENCONTRARON AGENCIAS');
            LoadClientes(CmbAgencias,CmbClientes,CmbStandar);
	} 
       	function LoadClientes(CmbAgencias,CmbClientes,CmbStandar){
	   CmbClientes.length=0;
	   var  aux='?';
	   if (datos.length>0){	  
               if (CmbAgencias.value!='')
                   for (i=0;i<datos.length;i++){
                     var info=(new String(datos[i])).split(separador);
                     if (CmbAgencias.value==info[0])
                        if (aux!=info[2]){
                             addOption(CmbClientes,info[2],info[3]);
                             aux=info[2];
                        }
                   }
                addOption(CmbClientes,'','TODOS LOS CLIENTES');
            }else
               addOption(CmbClientes,'NINGUNO','NO SE ENCONTRARON CLIENTES');  
           LoadStandar(CmbAgencias,CmbClientes,CmbStandar);
	}
        function LoadStandar(CmbAgencias,CmbClientes,CmbStandar){
	   CmbStandar.length=0;
	   var  aux='?';
	   if (datos.length>0){	   
               if (CmbClientes.value!='')	   
               for (i=0;i<datos.length;i++){
                 var info=(new String(datos[i])).split(separador);
                 if (CmbAgencias.value==info[0] && CmbClientes.value==info[2] )
                    if (aux!=info[4]){
                         addOption(CmbStandar,info[4],info[5]);
                         aux=info[4];
                    }
               }
               addOption(CmbStandar,'','TODOS LOS ESTANDARES'); 
            }else
               addOption(CmbStandar,'NINGUNO','NO SE ENCONTRARON ESTANDARES');               
	}
	function _onsubmit (tform){
            with (tform){
                var url = "<%= CONTROLLERPTO %>?ACCION=/Opciones/Historial&Agencia="+ Agencia.value +"&Cliente=" + Cliente.value + "&Stdjob=" + Stdjob.value +"&Ano=" + Ano.value + "&Mes=" + Mes.value +"&Opcion="+ Opcion.value;
                //alert (url);
                //newWindow(url, 'PtoVentas');
                window.location.href = url;
            }
            return false;
        }
      function newWindow(url, nombre){
          option="  width="+ (screen.width-30) +", height="+ (screen.height-80)  +",  scrollbars=yes, statusbars=yes, resizable=yes, menubar=no ,top=10 , left=10 ";
          ventana=window.open('',nombre,option);
          ventana.location.href=url;
          ventana.focus();
       }
        
   
        
    </script>
</head>
<body>
<div id="capaSuperior" style="position:absolute; width:100%; height:100px; z-index:0; left: 0px; top: 0px;">
 <jsp:include page="/toptsp.jsp?encabezado=PRESUPUESTO DE VENTAS"/>
</div>
<div id="capaCentral" style="position:absolute; width:100%; height:87%; z-index:0; left: 0px; top: 100px; overflow: scroll;">
<center>


<form action="<%= CONTROLLERPTO %>?ACCION=/Opciones/Historial" method="post" name="fpto" onsubmit='javascript: return _onsubmit(this);'>
<table width="500" border="2" bgcolor="#F7F5F4">
<tr><td align="center" >

    <table width="100%" class="tablaInferior">
    <tr>
           <td colspan='2'>
            <table border='0' width='100%' cellspacing='0'>
                <tr>
                 <td class='subtitulo1'  width="80%">EXPORTACION DE HISTORIAL</td>
                 <td class='barratitulo' width="20%"><img src="<%= BASEURL %>/images/titulo.gif" width="32" height="20"></td>
                </tr>
            </table>
           </td>
    </tr>

    <tr class="fila">
        <td width="20%">&nbsp;Agencia</td>
        <td width="*"><select name="Agencia" class="select" style="width:100%;" onclick='LoadClientes(this,Cliente,Stdjob);'></td>
    </tr> 
    <tr class="fila">
        <td width="20%">&nbsp;Cliente</td>
        <td width="*"><select name="Cliente" class="select" style="width:100%" onclick='LoadStandar(Agencia,this,Stdjob);'></select></td>
    </tr>   
    <tr class="fila">
        <td width="20%">&nbsp;Estandar</td>
        <td width="*"><select name="Stdjob" class="select" style="width:100%;"></td>
    </tr>    
       
    <tr class="fila">
        <td width="20%">&nbsp;Periodo</td>
        <td width="*">
            <select class="select" style="width:49%" name="Ano"></select>
            <select class="select" style="width:49%" name="Mes"></select>
        </td>
    </tr>   
    </table>
    
  


</td></tr>
</table>
    
    <br>
    <input type="hidden" name="Opcion" value="Exportar" >

<img src='<%=BASEURL%>/images/botones/buscar.gif'  style='cursor:hand' onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" onclick=" _onsubmit(fpto); ">
&nbsp;
<img src='<%=BASEURL%>/images/botones/salir.gif'   style='cursor:hand' onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" onclick='window.close();'>        



<script> 
Llenar(fpto.Ano,fpto.Mes,fpto.TipoVista); 
LoadAgencias(fpto.Agencia,fpto.Cliente,fpto.Stdjob);

</script>
</form>



<% if (!Mensaje.equals("")) { %>
    
        <table border="2" align="center">
           <tr>
           <td><table width="100%"  border="1" align="center"  bordercolor="#F7F5F4" bgcolor="#FFFFFF">
                <tr>
                 <td width="320" align="center" class="mensajes"><%=Mensaje%></td>
                 <td width="29" background="<%=BASEURL%>/images/cuadronaranja.JPG">&nbsp;</td>
                 <td width="58">&nbsp;</td>
                </tr>
              </table>
           </td>
           </tr>
        </table>  
        
<% } %>
</center>

</div>

</body>
</html>
