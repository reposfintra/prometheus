 <!--  
     - Author(s)       :      MARIO FONTALVO
     - Date            :      10/12/2005  
     - Copyright Notice:      TRANSPORTES SANCHEZ POLO S.A.
  -->
 <%--   
     - @(#)  
     - Description: Formulario visualizacion de Historial modo Edicion
 --%>

<%@page session="true"%> 
<%@page errorPage="/error/ErrorPage.jsp"%>
<%@page import="java.util.*" %>
<%@page import="com.tsp.finanzas.presupuesto.model.beans.*" %>
<%@page import="com.tsp.util.UtilFinanzas" %>
<%@include file="/WEB-INF/InitModel.jsp"%>
<% 
    List lista      = modelpto.DPtoSvc.getHistorial(); 
    List listaRP    = modelpto.ReprogamacionSvc.getListaReprogramaciones();
    String Distrito = (request.getParameter("Distrito")!=null?request.getParameter("Distrito"):"");
    String Agencia  = (request.getParameter("Agencia") !=null?request.getParameter("Agencia") :"");
    String Cliente  = (request.getParameter("Cliente") !=null?request.getParameter("Cliente") :"");
    String Stdjob   = (request.getParameter("Stdjob")  !=null?request.getParameter("Stdjob")  :"");
    String Ano      = (request.getParameter("Ano")     !=null?request.getParameter("Ano")     :"");
    String Mes      = (request.getParameter("Mes")     !=null?request.getParameter("Mes")     :"");
    
    String StdJobDesc    = request.getParameter("StdJobDesc");
    String NombreAgencia = request.getParameter("NombreAgencia");
    String NombreCliente = request.getParameter("NombreCliente");
    String Ruta          = request.getParameter("Ruta");
%>
<%! 
   public String obtenerModificaciones (String FechaViaje, String FechaCreacion, List ListaGeneral) {
       // definimos donde comienza y finaliza la lista
       int inicio = -1, fin = -1;
       // para el detener en el recorido donde comineza la lista
       //Reprogramacion objeto = null;
       String objeto = "";
       for (int i=0; i<ListaGeneral.size();i++ ){
           Reprogramacion rp = (Reprogramacion) ListaGeneral.get(i);
           if (rp.getFechaViaje().equals(FechaViaje) && rp.getFechaCreacion().equals(FechaCreacion)){
               objeto = rp.getDescripcion();
               break;
           }
       }
       return objeto;
   }
%>
<html>
<head>

 <title>Control de Cambios -  Presupuesto de Ventas (Modo Edidcion)</title>
 <link href="<%= BASEURL %>/css/estilostsp.css" rel="stylesheet" type="text/css"> 
 <script type='text/javascript' src="<%= BASEURL %>/js/boton.js"></script>
 

<link href="<%= BASEURL %>/css/Style.css" rel="stylesheet">
<script>
    function validar(texto){
        if( texto.value=='' || /^\d+$/.test(texto.value) )
            calcular(texto.form);
        else {
            alert("Formato numerico no valido, se colocara en vacio para que vuelva intentarlo");
            texto.value='';
        }
    }
    function calcular(tform){
        with(tform){
            var total    = 0;
            for (i=0;i<Viajes.length;i++)
              if (Viajes[i].type=='text'){
                if  (Viajes[i].value!='') {
                      total    += parseFloat(Viajes[i].value);
                }
              }
            Total.value    = total; 
        }
    }
    function qsubmit (tform){
        with(tform){
            for (i=0;i<Viajes.length;i++)
                if(  Viajes[i].value=='' ) Viajes[i].value='0';                
        }
        return true;
    }
    function _cerrar(){
        window.opener.location.href = '<%= CONTROLLERPTO %>?ACCION=/Opciones/Presupuesto&Opcion=ShowCalendar&Ano=<%= Ano %>&Mes=<%= Mes %>&Agencia=<%= Agencia %>&Cliente=<%= Cliente %>&Stdjob=<%= Stdjob %>';
        window.close();
    }
    
    
    
    function sendHistory(theForm){
       if( qsubmit(theForm) )
           theForm.submit();
    }
    
    
</script>
</head>
<body>


<div id="capaSuperior" style="position:absolute; width:100%; height:100px; z-index:0; left: 0px; top: 0px;">
<jsp:include page="/WEB-INF/toptsp.jsp?encabezado=Presupuesto de Venta"/>
</div>

<div id="capaCentral" style="position:absolute; width:100%; height:85%; z-index:0; left: 0px; top: 100px; overflow: scroll;"> 

<center>

 <form action='<%= CONTROLLERPTO %>?ACCION=/Opciones/Presupuesto&Opcion=GrabarHistorial' method='post' name='formulario' onsubmit='javascript: qsubmit(this);'>
  
  <% if (lista!=null && lista.size()>0) { %>
  
  
<table width="400" border="2" align="center" bgcolor="#F7F5F4">

       <tr>
          <td>
        
                <table width="734" align="center" class='tablaInferior' >
                
                <tr>
                <td aling='center' colspan='6'>

                   <table cellpadding='0' cellspacing='0' width='100%'>
                     <tr class="fila">
                      <td width='65%' class="subtitulo1">&nbsp Control de Cambios</td>
                      <td width='35' class="barratitulo"><img src="<%=BASEURL%>/images/titulo.gif" width="32" height="20"></td>
                    </tr>
                   </table>
                </td>
                </tr>
                
              <tr class='fila' >
                <td colspan='2'>Estandar </td >
                <td colspan='4' class='letra'><%= Stdjob  %> - <%= StdJobDesc %></td >
              </tr>
              <tr class='fila'>  
                <td colspan='2'>Agencia  </td >
                <td colspan='4'><%= NombreAgencia %></td >
              </tr>
              <tr class='fila'>  
                <td colspan='2'>Cliente  </td >
                <td colspan='4'><%= NombreCliente %></td >
              </tr>
              <tr class='fila'>  
                <td colspan='2'>Ruta     </td >
                <td colspan='4'><%= Ruta %></td>
              </tr>

                    <tr class="tblTitulo">
                            <th class='bordereporte' width="50" rowspan="2" scope="row">DIA</th>
                            <th class='bordereporte' width="106" rowspan="2">VIAJES ACTUALES</th>
                            <th class='bordereporte' colspan="4"> DATOS PREVIOS </th>
                    </tr>
                    <tr class="tblTitulo">
                            <th class='bordereporte' width="90"  nowrap >VIAJES</th>
                            <th class='bordereporte' width="153" nowrap >FECHA</th>
                            <th class='bordereporte' width="107" nowrap >MODIFICADO</th>
                            <th class='bordereporte' width="200" nowrap >CAUSA REPROGRAMACION</th>
                    </tr>
                          <% for (int dia = 1 ; dia <= 31; dia++) { 
                               String fecha = Ano + "-" + Mes + "-" + UtilFinanzas.DiaFormat(dia);
                               int valorActual = Integer.parseInt(((DatosGeneral) lista.get(0)).getValor( String.valueOf(dia-1)));  %>
                                  <tr class='<%= (dia%2==0?"filagris":"filaazul") %>' style='font-size:12px'>
                                        <td align='center' class='bordereporte'><%= UtilFinanzas.DiaFormat(dia) %></td>
                                        <td align='center' class='bordereporte'><input type='text' value='<%=  (valorActual==0?"":""+valorActual) %>' name='Viajes' class='input' onfocus='this.select()' onkeyup="jscript: validar(this);" style='text-align:center;width:40'><input type='hidden' value='<%= valorActual %>' name='CViajes'></td>
                                        <td colspan='4' align='center' class='bordereporte'>
                                          <table  width="100%" border="0" align="center" cellpadding="1" cellspacing="1">
                                          <% if (lista.size()==1) { %>
                                                  <tr class='<%= (dia%2==0?"filagris":"filaazul") %>' style='font-size:12px'>
                                                    <td align='center' colspan='4' >Sin Historia</td>
                                                  </tr>
                                                  <% } else{ 
                                                       int    memVal = Integer.parseInt(((DatosGeneral) lista.get(1)).getValor( String.valueOf(dia-1)));
                                                       String memFec = ((DatosGeneral) lista.get(1)).getValor("FECHA"  );
                                                       String memUsu = ((DatosGeneral) lista.get(1)).getValor("USUARIO");
                                                       boolean sw = false;
                                                       for (int i = 1; i < lista.size(); i++) { 
                                                           int nextVal = Integer.parseInt(((DatosGeneral) lista.get(i)).getValor( String.valueOf(dia-1)));
                                                           if ((nextVal != memVal && sw) || (nextVal != memVal && valorActual != memVal)) { 
                                                              sw=true;  %>
                                                              <tr class='<%= (dia%2==0?"filagris":"filaazul") %>' style='font-size:12px'>
                                                                <td align='center' width='90'  class='bordereporte'><%= memVal %></td>
                                                                <td align='center' width='153' class='bordereporte'><%= memFec %></td>
                                                                <td align='center' width='107' class='bordereporte'><%= memUsu %></td>
                                                                <td align='center' width='200' class='bordereporte'><%= obtenerModificaciones(fecha, memFec, listaRP) %></td>
                                                              </tr>
                                                    <%    } 
                                                              memVal = Integer.parseInt(((DatosGeneral) lista.get(i)).getValor( String.valueOf(dia-1)));
                                                              memFec = ((DatosGeneral) lista.get(i)).getValor("FECHA"  );
                                                              memUsu = ((DatosGeneral) lista.get(i)).getValor("USUARIO");
                                                      } %>
                                                  <% if (memVal!=0) {%>
                                                  <tr class='<%= (dia%2==0?"filagris":"filaazul") %>' style='font-size:12px'>
                                                    <td align='center' width='90'  class='bordereporte'><%= memVal %></td>
                                                    <td align='center' width='153' class='bordereporte'><%= memFec %></td>
                                                    <td align='center' width='107' class='bordereporte'><%= memUsu %></td>
                                                    <td align='center' width='200' class='bordereporte'><%= obtenerModificaciones(fecha, memFec, listaRP) %></td>
                                                  </tr>
                                                  <% }else if (!sw){ %>
                                                  <tr class='<%= (dia%2==0?"filagris":"filaazul") %>' style='font-size:12px'>
                                                    <td align='center' colspan='3' >Sin Historia</td>
                                                  </tr>      
                                                  <% } } %>
                                        </table>
                                   </tr>
                           <% } %>
                     </table>
                           
                           
                           
             </td>
          </tr>
      </table>
                
                
                
                

    <% } else { %>
        <br><br><br><br><br>
        <table border="2" align="center">
              <tr>
                <td><table width="100%"  border="1" align="center"  bordercolor="#F7F5F4" bgcolor="#FFFFFF">
                  <tr>
                    <td width="320" align="center" class="mensajes">No hay historial para mostrar</td>
                    <td width="29" background="<%=BASEURL%>/images/cuadronaranja.JPG">&nbsp;</td>
                    <td width="58">&nbsp;</td>
                  </tr>
                </table></td>
              </tr>
      </table>
    <% } %>
    <br>
    <br>
    <input type='hidden' name='Viajes'  value='0'>
    <input type='hidden' name='CViajes' value='0'>

    <input type='hidden' name='Distrito' value='<%= Distrito %>'>
    <input type='hidden' name='Stdjob'   value='<%= Stdjob   %>'>
    <input type='hidden' name='Agencia'  value='<%= Agencia  %>'>
    <input type='hidden' name='Cliente'  value='<%= Cliente  %>'>
    <input type='hidden' name='Ano'      value='<%= Ano      %>'>
    <input type='hidden' name='Mes'      value='<%= Mes      %>'>
    <input type='hidden' name='Total'    value=''>

    <input type='hidden' name='StdJobDesc'    value='<%= StdJobDesc    %>'>
    <input type='hidden' name='NombreCliente' value='<%= NombreCliente %>'>
    <input type='hidden' name='NombreAgencia' value='<%= NombreAgencia %>'>
    <input type='hidden' name='Ruta'          value='<%= Ruta          %>'>


    <% if (lista!=null && lista.size()>0) { %> 
         <img src="<%=BASEURL%>/images/botones/aceptar.gif"   name="imgaceptar"  onClick=" sendHistory(formulario)"    onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" style="cursor:hand">&nbsp;
     <% } %>
         <img src="<%=BASEURL%>/images/botones/salir.gif"     name="imgsalir"    onClick=" window.close();"            onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" style="cursor:hand">
    
</form>

<% if (lista!=null && lista.size()>0) { %> <script> calcular(formulario); </script> <% } %>
</center>
</body>
</html>


