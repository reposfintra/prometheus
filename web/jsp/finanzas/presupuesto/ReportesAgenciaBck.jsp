<!--  
     - Author(s)       :      MARIO FONTALVO
     - Date            :      10/12/2005  
     - Copyright Notice:      TRANSPORTES SANCHEZ POLO S.A.
  -->
 <%--   
     - @(#)  
     - Description: Permite realizar Reportes de Agencia
 --%>
 

<%@page session="true"%> 
<%@page errorPage="/error/ErrorPage.jsp"%>
<%@include file="/WEB-INF/InitModel.jsp"%>

<%@page import="java.util.*" %>
<%@page import="com.tsp.finanzas.presupuesto.model.beans.*" %>
<%@page import="com.tsp.util.UtilFinanzas" %>

<%
    String Ano      = request.getParameter("Ano");
    String Mes      = request.getParameter("Mes");
    String Distrito = request.getParameter("Distrito");
    
    int anchoCol = 29; 
    int restoCol = 610;
    
    String estiloPtoMyEjt = "style='color:red;        font-weight:bold;'";
    String estiloPtoMnEjt = "style='color:darkgreen;  font-weight:bold;'";
    
    int today = Integer.parseInt(UtilFinanzas.getFechaActual_String(8));
%>

<html>
    <head><title>Reporte [Viajes Presupuestados Vs Viajes Ejecutados] Por Agencia</title>
        <link href="<%= BASEURL %>/css/Style.css" rel='stylesheet'>
        <script>
            function newWindow(url, nombre){
              option="  width="+ (screen.width-30) +", height="+ (screen.height-80)  +",  scrollbars=yes, statusbars=yes, resizable=yes, menubar=no ,top=10 , left=10 ";
              ventana=window.open('',nombre,option);
              ventana.location.href=url;
            }
        
            function go(Agencia){
                var url = "<%= CONTROLLERPTO %>?ACCION=/Opciones/Reportes&Opcion=Generar&Ano=<%= Ano %>&Mes=<%= Mes %>&Distrito=<%= Distrito %>&Agencia="+ Agencia +"&Tipo=1";
                newWindow(url, 'PE'+Agencia);
            }
        </script>
    </head>
    <body>

<% 
    List listado = modelpto.ReportesSvc.getListado();
    if (listado.size()>0){
%>
        <table border='1' >
            <tr class='titulo'><td height='35'>Reporte Viajes Presupuestados vs Viajes Ejecutados - Periodo <%= UtilFinanzas.NombreMes(Integer.parseInt(Mes)) %> <%= Ano %></td></tr>    
            <tr><td>
                <table border='0' cellpadding='1' cellspacing='1' width='<%= (anchoCol*62)+restoCol %>'>
                <tr class='subtitulo'>
                    <th rowspan='2'>Agencia     </th>
                    <th rowspan='2'>Presup<br>Mensual   </th>
                    <th rowspan='2'>Presup<br>a la Fecha</th>
                    <th rowspan='2'>Ejecut<br>a la Fecha</th>
                    <th rowspan='2'>Difer</th>
                    <th rowspan='2'>Porcen<br>Cumpl</th>
                    <% for (int ix_st = 1 ; ix_st <= 31 ; ix_st++ ) out.print("<th colspan='2'>"+ UtilFinanzas.DiaSemana(Ano + Mes + UtilFinanzas.DiaFormat(ix_st)) + " " + ix_st +"</th>"); %>
                </tr>
                <tr class='subtitulo' ><% for (int ix_st = 1 ; ix_st <= 31 ; ix_st++ ) out.print("<th>Pt</th><th>Ej</th>"); %></tr>
            
            <%
                for (int i_ag = 0 ; i_ag < listado.size(); i_ag++){
                    ViajesAgencia ag = (ViajesAgencia) listado.get(i_ag);
            %>
                <tr class='fondotabla'>
                <td class='comentario' width='260'align='left' ><a href='javascript: void(0);' onclick="go('<%= ag.getAgencia() %>');">[<%= ag.getAgencia() %>]</a> <%= ag.getAgenciaNombre() %></td>
                <td class='comentario' width='70' align='center'><%= ag.getTotalPt()    %></td>
                <td class='comentario' width='70' align='center'><%= ag.getTotalPtF()    %></td>
                <td class='comentario' width='70' align='center'><%= ag.getTotalEj()    %></td>
                <td class='comentario' width='70' align='center'><%= ag.getDiferencia() %></td>
                <td class='comentario' width='70' align='center'><%= UtilFinanzas.customFormat2(ag.getPorcentajeIncumplimiento()) %></td>
                <% for (int ix_ag = 1; ix_ag <= 31; ix_ag++) {
                        String estilo = "";
                        int fecha = Integer.parseInt(ag.getAno() + ag.getMes() + UtilFinanzas.DiaFormat(ix_ag));
                        if (fecha <= today ){
                            estilo = ((ag.getViajePtdo(ix_ag) > ag.getViajeEjdo(ix_ag))?estiloPtoMyEjt:
                                      (ag.getViajePtdo(ix_ag) < ag.getViajeEjdo(ix_ag))?estiloPtoMnEjt:"");
                        }
                %>
                        <td class='comentario' align='center' width='<%= anchoCol %>' <%= estilo %>><%= (ag.getViajePtdo(ix_ag)==0?"-":String.valueOf(ag.getViajePtdo(ix_ag))) %></td>
                        <td class='comentario' align='center' width='<%= anchoCol %>' <%= estilo %>><%= (ag.getViajeEjdo(ix_ag)==0?"-":String.valueOf(ag.getViajeEjdo(ix_ag))) %></td>
                <%  } %>
                </tr>
            <%  } // end for %>       
            </table>
            </td></tr></table>

                
<%   }     // end if %>

    </body>
</html>
