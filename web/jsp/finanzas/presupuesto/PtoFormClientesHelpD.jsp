<!--
     - Author(s)       :      MARIO FONTALVO
     - Date            :      25/01/2006
     - Copyright Notice:      TRANSPORTES SANCHEZ POLO S.A.
-->
<%--
     - @(#)
     - Description: Repoortes
--%> 
<%@include file="/WEB-INF/InitModel.jsp"%>
<html>
<head>
<title>CONSULTA PRESUPUESTO DE VENTAS</title>
<link href="../../../css/estilostsp.css" rel="stylesheet" type="text/css">
<link href="<%= BASEURL %>/css/estilostsp.css" rel="stylesheet" type="text/css">
</head>
<body>
<BR>
<table width="696"  border="2" align="center">
  <tr>
    <td width="811" >
      <table width="100%" border="0" align="center">
        <tr  class="subtitulo">
          <td height="24" colspan="2"><div align="center">CONSULTA PRESUPUESTO DE VENTAS</div></td>
        </tr>
		<tr class="subtitulo1">
          <td colspan="2"> PARAMETROS DE CONSULTA PRESUPUESTO DE VENTAS</td>
        </tr>
        <tr>
          <td width="172" class="fila"> CLIENTE </td>
          <td width="502"  class="ayudaHtmlTexto">Indique el codigo de Cliente</td>
        </tr>
        <tr>
          <td width="172" class="fila"> NOMBRE </td>
          <td width="502"  class="ayudaHtmlTexto">Nombre del cliente seleccionado, tambie puede seleccionarlo por este iten si desconoce el codigo de cliente</td>
        </tr>

        <tr>
          <td  class="fila">ORIGEN</td>
          <td  class="ayudaHtmlTexto">Origenes de los estandares relacionados al cliente</td>
        </tr>

        <tr>
          <td  class="fila">DESTINO</td>
          <td  class="ayudaHtmlTexto">Destinos relacionados a los origenes de los estandares de clinete seleccionado</td>
        </tr>

        <tr>
          <td class="fila">ESTANDAR</td>
          <td  class="ayudaHtmlTexto">Estandar relacionado a los parametros anteriores</td>
        </tr>

        <tr>
          <td width="172" class="fila"> TIPO DE VISTA </td>
          <td width="502"  class="ayudaHtmlTexto">Tipo de vista que desea generar el usuario este es de tres Tipos: MENSUAL, SEMANAL, DIARIO</td>
        </tr>

        <tr>
          <td width="172" class="fila"> PERIODO </td>
          <td width="502"  class="ayudaHtmlTexto">Periodo a consultar</td>
        </tr>
        <tr>
          <td width="172" class="fila"> RANGO DE DIAS </td>
          <td width="502"  class="ayudaHtmlTexto">Numero de dias a consultar del periodo por parte del usuario, este depende del Tipo de Vista especificado por el usuario.</td>
        </tr>
        <tr>
          <td width="172" class="fila" align='center'> <img src="<%= BASEURL %>/images/botones/buscar.gif"> </td>
          <td width="502"  class="ayudaHtmlTexto">Boton que da inicio al proceso de consulta de Presupuesto</td>
        </tr>

      </table>
    </td>
  </tr>
</table>
</body>
</html>
