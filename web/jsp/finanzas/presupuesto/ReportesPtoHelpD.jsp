<!--
     - Author(s)       :      MARIO FONTALVO
     - Date            :      25/01/2006
     - Copyright Notice:      TRANSPORTES SANCHEZ POLO S.A.
-->
<%--   
     - @(#)  
     - Description: Repoortes
--%> 
<%@include file="/WEB-INF/InitModel.jsp"%>
<html>
<head>
<title>Reporte Ejecutado vs.Presupuestado</title>
<link href="../../../css/estilostsp.css" rel="stylesheet" type="text/css">
<link href="<%= BASEURL %>/css/estilostsp.css" rel="stylesheet" type="text/css">
</head>
<body>
<BR>
<table width="696"  border="2" align="center">
  <tr>
    <td width="811" >
      <table width="100%" border="0" align="center">
        <tr  class="subtitulo">
          <td height="24" colspan="2"><div align="center">GENERACION REPORTE VIAJES PRESUPUESTADO VS. EJECUTADO</div></td>
        </tr>
		<tr class="subtitulo1">
          <td colspan="2"> PARAMETROS DE GENERACION DE REPORTE</td>
        </tr>
        <tr>
          <td width="172" class="fila"> DISTRITO </td>
          <td width="502"  class="ayudaHtmlTexto">Distrito para restringir los datos del reporte (opcional)</td>
        </tr>
        <tr>
          <td width="172" class="fila"> AGENCIA </td>
          <td width="502"  class="ayudaHtmlTexto">Agencia para restringir los datos del reporte , para poder seleccionarla usted primero debe habiltar esta opcion con la casilla de opcion unica que aparece a la izquierda del mismo.(opcional).</td>
        </tr>

        <tr>
          <td  class="fila">CLIENTE</td>
          <td  class="ayudaHtmlTexto">Cliente para restringir los datos del reporte, Este depende de la agencia seleccionada. (opcional)</td>
        </tr>
        <tr>
          <td class="fila">ESTANDAR</td>
          <td  class="ayudaHtmlTexto">Estandar para restringir los datos del reporte, Este depende del cliente seleccionada. (opcional)</td>
        </tr>
        <tr>
          <td class="fila">AGENCIA DESPACHO</td>
          <td  class="ayudaHtmlTexto">Agencia de Despacho, para poder seleccionarla usted primero debe habiltar esta opcion con la casilla de opcion unica que aparece a la izquierda del mismo.(opcional)</td>
        </tr>

        <tr>
          <td width="172" class="fila"> PERIODO </td>
          <td width="502"  class="ayudaHtmlTexto">Periodo de generacion de reporte.</td>
        </tr>
        <tr>
          <td width="172" class="fila"> RANGO DE DIAS </td>
          <td width="502"  class="ayudaHtmlTexto">Numero de dias a consultar del periodo por parte del usuario.</td>
        </tr>

                <tr>
          <td width="172" class="fila"> TIPO DE REPORTE </td>
          <td width="502"  class="ayudaHtmlTexto">Seleccion del tipo de reporte de a generar. Opciones: Agencia, Cliente, Estandar</td>
        </tr>

        
      </table>
    </td>
  </tr>
</table>
</body>
</html>
