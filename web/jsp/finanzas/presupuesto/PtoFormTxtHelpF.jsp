<!--
     - Author(s)       :      MARIO FONTALVO
     - Date            :      25/01/2006
     - Copyright Notice:      TRANSPORTES SANCHEZ POLO S.A.
-->
<%--   
     - @(#)  
     - Description: Reportes Viajes Presupuestado vs. Ejecutado
--%>
<%@include file="/WEB-INF/InitModel.jsp"%>
<html>
<head>
    <title>CONSULTA PRESUPUESTO DE VENTAS</title>
<link href="../../../css/estilostsp.css" rel="stylesheet" type="text/css">
<link href="<%= BASEURL %>/css/estilostsp.css" rel="stylesheet" type="text/css">
</head>
<body>
<BR>
<table width="696"  border="2" align="center">
  <tr>
    <td width="811" >
      <table width="100%" border="0" align="center">
        <tr  class="subtitulo">
          <td width="674" height="24"><div align="center">CONSULTA PRESUPUESTO DE VENTAS.</div></td>
        </tr>
		<tr class="subtitulo1">
          <td> PARAMETROS DE CONSULTA PRESUPUESTO DE VENTAS</td>
        </tr>
		<tr class="ayudaHtmlTexto">
		<td>Formulario de definicion de parametros de Consulta de Viajes Prespuestados<br>
		    <div align="center"><img src="<%= BASEURL %>/images/ayuda/ConsultaPTO/dibujo02.JPG" align="absmiddle"></div>
                    <br>

        	    <br>
        	    <B>SALIDAS (Listado de Viajes Presupuestados).</B><BR><BR>
        	    Desde Este Listado se puede Consultar los Viajes Presupuestado para los estandares y a la vez se pueden modificar
        	    dependiendo del nivel en que se encuentren granados los viajes, es descir, que si un viaje se encuentra grabado a nivel diario
                    no puede ser modificado a nivel mensual.
                    
                    Este Listado permite ser exportado a Excel por el usuario, para ello debe ir a el boton de Exportar que se encuentra en la 
                    parte inferior del mismo.

        	    
		  </td>
		</tr>
      </table>
    </td>
  </tr>
</table>
</body>
</html>
