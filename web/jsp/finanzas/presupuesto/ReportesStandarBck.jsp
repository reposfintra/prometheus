<!--  
     - Author(s)       :      MARIO FONTALVO
     - Date            :      10/12/2005  
     - Copyright Notice:      TRANSPORTES SANCHEZ POLO S.A.
  -->
 <%--   
     - @(#)  
     - Description: Permite realizar Reportes de Estandar
 --%>
 

<%@page session="true"%>
<%@page errorPage="/error/ErrorPage.jsp"%>
<%@include file="/WEB-INF/InitModel.jsp"%>

<%@page import="java.util.*" %>
<%@page import="com.tsp.finanzas.presupuesto.model.beans.*" %>
<%@page import="com.tsp.util.UtilFinanzas" %>

<%
    String Ano = modelpto.ReportesSvc.getAno();
    String Mes = modelpto.ReportesSvc.getMes();
    
    int anchoCol = 29; 
    int restoCol = 680;
    
    String estiloPtoMyEjt = "style='color:red;        font-weight:bold;'";
    String estiloPtoMnEjt = "style='color:darkgreen;  font-weight:bold;'";
    
    int today = Integer.parseInt(UtilFinanzas.getFechaActual_String(8));
%>

<html>
    <head><title>Reporte [Viajes Presupuestados Vs Viajes Ejecuados] Por Estandar</title>
    <link href="<%= BASEURL %>/css/Style.css" rel='stylesheet'>
    </head>
    <body>

<% 
    List listado = modelpto.ReportesSvc.getListado();
    if (listado.size()>0){
%>
            <%
                for (int i_ag = 0 ; i_ag < listado.size(); i_ag++){
                    ViajesAgencia ag = (ViajesAgencia) listado.get(i_ag);
            %>
            <table border='0' >
            <tr class='titulo'><td height='35'>AGENCIA [<%= ag.getAgencia() %>] <%= ag.getAgenciaNombre() %></td></tr>    
            <tr><td>
                <!-- ******************************* CLIENTES   ************************************ -->
                <%
                    for (int i_cl = 0 ; i_cl < ag.getListadoClientes().size(); i_cl++)      {
                        ViajesCliente cl = (ViajesCliente) ag.getListadoClientes().get(i_cl); 
                %>
                <br>
                <table border="1">
                <tr><td  class='fondoA'><b>CLIENTE [<%= cl.getCliente() %>] <%= cl.getClienteNombre() %></b></td></tr>
                <tr><td>
                <!-- **************************** ESTANDAR ********************************** -->
                <table border='0' cellpadding='1' cellspacing='1' width='<%= (anchoCol*62)+restoCol %>'>
                        <tr class='subtitulo'>
                        <th rowspan='2'>Estandar   </th>
                        <th rowspan='2'>Descripcion</th>
                        <th rowspan='2'>Presup<br>Mensual   </th>
                        <th rowspan='2'>Presup<br>a la Fecha</th>
                        <th rowspan='2'>Ejecut<br>a la Fecha</th>
                        <th rowspan='2'>Difer</th>
                        <th rowspan='2'>Porcen<br>Cumpl</th>
                        <% for (int ix_st = 1 ; ix_st <= 31 ; ix_st++ ) out.print("<th colspan='2'>"+ UtilFinanzas.DiaSemana(Ano + Mes + UtilFinanzas.DiaFormat(ix_st)) + " " + ix_st +"</th>"); %>
                        </tr>
                        
                        <tr class='subtitulo' >
                        <% for (int ix_st = 1 ; ix_st <= 31 ; ix_st++ ) out.print("<th>Pt</th><th>Ej</th>"); %>
                        </tr>
                        
                        <%
                            for (int i_st = 0 ; i_st < cl.getListaStandar().size(); i_st++)      {
                                ViajesStandar st = (ViajesStandar) cl.getListaStandar().get(i_st);
                        %>
                        <tr class='fondotabla'>
                            <td class='comentario' width='50' align='center'><%= st.getStdJobNo()   %></td>
                            <td class='comentario' width='350'><%= st.getStdJobDesc()               %></td>
                            <td class='comentario' width='70' align='center'><%= st.getTotalPt()    %></td>
                            <td class='comentario' width='70' align='center'><%= st.getTotalPtF()   %></td>
                            <td class='comentario' width='70' align='center'><%= st.getTotalEj()    %></td>
                            <td class='comentario' width='70' align='center'><%= st.getDiferencia() %></td>
                            <td class='comentario' width='70' align='center'><%= UtilFinanzas.customFormat2(st.getPorcentajeIncumplimiento()) %></td>
                            <% for (int ix_st = 1; ix_st <= 31; ix_st++) {
                                String estilo = "";
                                int fecha = Integer.parseInt(st.getAno() + st.getMes() + UtilFinanzas.DiaFormat(ix_st));
                                if (fecha <= today ){
                                    estilo = ((st.getViajePtdo(ix_st) > st.getViajeEjdo(ix_st))?estiloPtoMyEjt:
                                              (st.getViajePtdo(ix_st) < st.getViajeEjdo(ix_st))?estiloPtoMnEjt:"");
                                }
                            %>
                            <td class='comentario' align='center' width='<%= anchoCol %>' <%= estilo %>><%= (st.getViajePtdo(ix_st)==0?"-":String.valueOf(st.getViajePtdo(ix_st))) %></td>
                            <td class='comentario' align='center' width='<%= anchoCol %>' <%= estilo %>><%= (st.getViajeEjdo(ix_st)==0?"-":String.valueOf(st.getViajeEjdo(ix_st))) %></td>
                            <%  } %>
                        </tr>
                        <%  } %>
                        <tr class='fondoA'>
                            <td class='comentario' align='center' colspan='2'>Totales Acumulados por cliente</td>
                            <td class='comentario' align='center'><%= cl.getTotalPt()    %></td>
                            <td class='comentario' align='center'><%= cl.getTotalPtF()   %></td>
                            <td class='comentario' align='center'><%= cl.getTotalEj()    %></td>
                            <td class='comentario' align='center'><%= cl.getDiferencia() %></td>
                            <td class='comentario' align='center'><%= UtilFinanzas.customFormat2(cl.getPorcentajeIncumplimiento()) %></td>
                            <% for (int ix_cl = 1; ix_cl <= 31; ix_cl++) {
                                String estilo = "";
                                int fecha = Integer.parseInt(cl.getAno() + cl.getMes() + UtilFinanzas.DiaFormat(ix_cl));
                                if (fecha <= today ){
                                    estilo = ((cl.getViajePtdo(ix_cl) > cl.getViajeEjdo(ix_cl))?estiloPtoMyEjt:
                                              (cl.getViajePtdo(ix_cl) < cl.getViajeEjdo(ix_cl))?estiloPtoMnEjt:"");
                                }
                            %>
                            
                            <td class='comentario' align='center' <%= estilo%> ><%= (cl.getViajePtdo(ix_cl)==0?"-":String.valueOf(cl.getViajePtdo(ix_cl))) %></td>
                            <td class='comentario' align='center' <%= estilo%> ><%= (cl.getViajeEjdo(ix_cl)==0?"-":String.valueOf(cl.getViajeEjdo(ix_cl))) %></td>
                            <%  } %>

                        </tr>
                        
                    </table>
                <!-- *****************************  FIN ESTANDAR ***************************** -->
                </td></tr>
                </table>
                <%  } %>
                
                <!-- *****************************  FIN CLIENTES  ************************************ -->
                </td>
            </tr>
            <tr>
            <td>
            <!-- TOTALES AGENCIAS -->
                <br><br>
                <table border='1' bgcolor='green'>
                <tr><td>
                <table border='0' cellpadding='1' cellspacing='1' width='<%= (anchoCol*62)+restoCol %>'>
                        <tr class='subtitulo'>
                        <th rowspan='2' colspan='2' >Totales Acumulados por Agencia</th>
                        <th rowspan='2'>Presup<br>Mensual   </th>
                        <th rowspan='2'>Presup<br>a la Fecha</th>
                        <th rowspan='2'>Ejecut<br>a la Fecha</th>
                        <th rowspan='2'>Difer</th>
                        <th rowspan='2'>Porcen<br>Cumpl</th>
                        <% for (int ix_ag = 1 ; ix_ag <= 31 ; ix_ag++ ) out.print("<th colspan='2' >"+ UtilFinanzas.DiaSemana(Ano + Mes + UtilFinanzas.DiaFormat(ix_ag)) + " " + ix_ag +"</th>"); %>
                        </tr>
                        
                        <tr class='subtitulo' >
                        <% for (int ix_ag = 1 ; ix_ag <= 31 ; ix_ag++ ) out.print("<th >Pt</th><th >Ej</th>"); %>
                        </tr>
                

                    <tr class='fondotabla'>
                        <td class='comentario' width='400'align='center' colspan='2'> <%= ag.getAgenciaNombre() %> </td>
                        <td class='comentario' width='70' align='center'><%= ag.getTotalPt()    %></td>
                        <td class='comentario' width='70' align='center'><%= ag.getTotalPtF()   %></td>
                        <td class='comentario' width='70' align='center'><%= ag.getTotalEj()    %></td>
                        <td class='comentario' width='70' align='center'><%= ag.getDiferencia() %></td>
                        <td class='comentario' width='70' align='center'><%= UtilFinanzas.customFormat2(ag.getPorcentajeIncumplimiento()) %></td>
                        <% for (int ix_ag = 1; ix_ag <= 31; ix_ag++) {
                                String estilo = "";
                                int fecha = Integer.parseInt(ag.getAno() + ag.getMes() + UtilFinanzas.DiaFormat(ix_ag));
                                if (fecha <= today ){
                                    estilo = ((ag.getViajePtdo(ix_ag) > ag.getViajeEjdo(ix_ag))?estiloPtoMyEjt:
                                              (ag.getViajePtdo(ix_ag) < ag.getViajeEjdo(ix_ag))?estiloPtoMnEjt:"");
                                }
                        %>
                        <td class='comentario' align='center' width='<%= anchoCol %>' <%= estilo%> ><%= (ag.getViajePtdo(ix_ag)==0?"-":String.valueOf(ag.getViajePtdo(ix_ag))) %></td>
                        <td class='comentario' align='center' width='<%= anchoCol %>' <%= estilo%> ><%= (ag.getViajeEjdo(ix_ag)==0?"-":String.valueOf(ag.getViajeEjdo(ix_ag))) %></td>
                        <%  } %>
                    </tr>
                </table>                
                </td></tr></table>
            <!-- FIN TOTALES AGENCIAS -->
            </td>
            </tr>
        </table>
        <br><br>
        <%  } // end for %>        
        
<%   }     // end if %>

    </body>
</html>
