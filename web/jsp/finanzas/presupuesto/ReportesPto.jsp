<%@page session="true"%> 
<%@page errorPage="/error/ErrorPage.jsp"%>
<%@page import="java.util.*" %>
<%@include file="/WEB-INF/InitModel.jsp"%>
<%@page import="com.tsp.finanzas.presupuesto.model.beans.*" %>
<%@taglib uri="http://jakarta.apache.org/taglibs/input-0.90" prefix="input" %>
<%
    String path = application.getRealPath(model.menuService.getRealUrl(request.getRequestURI()));
    String datos[] = model.menuService.getContenidoMenu(BASEURL,request.getRequestURI(),path);
%>
<html>
    <head>
        <title>Reporte de Cumplimiento Presupuestado Vs Ejecutado</title>        
		<link href="<%= BASEURL %>/css/estilotsp.css" rel='stylesheet'>  
		<script language="javaScript" type="text/javascript" src="<%=BASEURL%>/js/boton.js"></script>
        <script>
        <%@include file="/jsp/finanzas/presupuesto/datosEstandarCombos.jsp"%>
        function addOption(Comb,valor,texto){
                var Ele = document.createElement("OPTION");
                Ele.value=valor;
                Ele.text=texto;
                Comb.add(Ele);
        }    
        function Llenar(CmbAnno, CmbMes, CmbDiaI, CmbDiaF){
                var Meses    = new Array('Enero','Febrero','Marzo','Abril','Mayo','Junio','Julio','Agosto','Septiembre','Octubre','Noviembre','Diciembre');
                var FechaAct = new Date();
                CmbAnno.length = 0;
                CmbMes.length  = 0;
                
                for (i=FechaAct.getYear()-10;i<=FechaAct.getYear()+10;i++) addOption(CmbAnno,i,i)
                CmbAnno.value = FechaAct.getYear();
                for (i=0;i<Meses.length;i++)
                    if ((i+1)<10) addOption(CmbMes,'0'+(i+1),Meses[i]);
                    else          addOption(CmbMes,(i+1),Meses[i]);                
                CmbMes.value  = ((FechaAct.getMonth()+1)<10)?('0'+(FechaAct.getMonth()+1)):(FechaAct.getMonth()+1);                 
                
                for (i=1;i<=31;i++){
                    dia = (i<10?'0'+i:i);
                    addOption(CmbDiaI,dia,dia);
                    addOption(CmbDiaF,dia,dia);                
                }
                CmbDiaI.value = '01';
                CmbDiaF.value = '31';
                
	
        } 
        function LoadAgencias(CmbAgencias,CmbClientes,CmbStandar){
	   CmbAgencias.length=0;
	   var  aux='?';
	   if (datos.length>0){
               for (i=0;i<datos.length;i++){
                 var info=(new String(datos[i])).split(separador);
                 if (aux!=info[0]){
                     addOption(CmbAgencias,info[0],info[1]);
                     aux=info[0];
                 }
               }
               addOption(CmbAgencias,'','TODAS LAS AGENCIAS'); 
            }else
               addOption(CmbAgencias,'NINGUNO','NO SE ENCONTRARON AGENCIAS');
            CmbAgencias.value='';
            LoadClientes(CmbAgencias,CmbClientes,CmbStandar);
	} 
       	function LoadClientes(CmbAgencias,CmbClientes,CmbStandar){
	   CmbClientes.length=0;
	   var  aux='?';
	   if (datos.length>0){	  
               if (CmbAgencias.value!='')
                   for (i=0;i<datos.length;i++){
                     var info=(new String(datos[i])).split(separador);
                     if (CmbAgencias.value==info[0])
                        if (aux!=info[2]){
                             addOption(CmbClientes,info[2],info[3]);
                             aux=info[2];
                        }
                   }
                addOption(CmbClientes,'','TODOS LOS CLIENTES'); 
            }else
               addOption(CmbClientes,'NINGUNO','NO SE ENCONTRARON CLIENTES'); 
           CmbClientes.value = '';
           LoadStandar(CmbAgencias,CmbClientes,CmbStandar);
	}
        function LoadStandar(CmbAgencias,CmbClientes,CmbStandar){
	   CmbStandar.length=0;
	   var  aux='?';
	   if (datos.length>0){	   
               if (CmbClientes.value!='')	   
               for (i=0;i<datos.length;i++){
                 var info=(new String(datos[i])).split(separador);
                 if (CmbAgencias.value==info[0] && CmbClientes.value==info[2] )
                    if (aux!=info[4]){
                         addOption(CmbStandar,info[4],info[5]);
                         aux=info[4];
                    }
               }
               addOption(CmbStandar,'','TODOS LOS STANDARES'); 
            }else
               addOption(CmbStandar,'NINGUNO','NO SE ENCONTRARON STNADARES');   
            CmbStandar.value = '';
	}
	
	
	function checkRadio (seleccionado){
            if (seleccionado=='NORMAL'){
               formulario.Agencia.disabled = false;
               formulario.Cliente.disabled = false;
               formulario.Estandar.disabled = false;
               formulario.AgenciaDespacho.disabled = true;
            }
            else{
               formulario.Agencia.disabled = true;
               formulario.Cliente.disabled = true;
               formulario.Estandar.disabled = true;       
               formulario.AgenciaDespacho.disabled = false;
            }
	}
	
	
	function validar (){
             with (formulario){
                var diaI = parseFloat(diaInicial.value);
                var diaF = parseFloat(diaFinal.value);
                if (diaI>diaF){
                   alert ('El dia final debe ser mayor que el dia Inicial, por favor rectifiquelo para continuar');
                   return false;
                }
                Opcion.value='Generar';
                submit();
             }
	}
        </script>   

    
    </head>
    <body onResize="redimensionar()" onLoad="redimensionar()">
		<div id="capaSuperior" style="position:absolute; width:100%; height:100px; z-index:0; left: 1px; top: 0px;">
 			<jsp:include page="/toptsp.jsp?encabezado=Reporte de Cumplimiento"/>
		</div>
		<div id="capaCentral" style="position:absolute; width:100%; height:87%; z-index:0; left: 0px; top: 100px; overflow: scroll;">
        <center>
        <form action='<%= CONTROLLERPTO %>?ACCION=/Opciones/Reportes' method='post' name='formulario'>
        <input name="Opcion" type="hidden" id="Opcion">
		<table width='550' border="2">
                    <tr>
                            <td>
                            <table width='100%' align='center' class="tablaInferior">
                                    <tr>
                                            <td class='subtitulo1'>REPORTES DE VENTAS</td>
                                            <td class='barratitulo'><img src="<%=BASEURL%>/images/titulo.gif" width="32" height="20" align='left'><%=datos[0]%></td>
                                    </tr>
                            </table>				
                            <table width='100%' align='center' class="tablaInferior">
                                    <tr class="fila">
                                            <td width="25%" colspan='2'>Distrito</td>
                                            <td width="*"><select name="Distrito" class="textbox" style="width:120;"><option value='FINV'>FINV</option></select></td>
                                    </tr> 
                                    <tr class="fila">
                                            <td width="3%" rowspan='3'><input type='radio' name='tipoBusqueda' value='NORMAL' checked onclick='checkRadio(this.value);'></td>
                                            <td >Agencia</td>
                                            <td ><select name="Agencia" class="textbox" style="width:100%;" onclick='LoadClientes(this,Cliente,Estandar);'></select></td>
                                    </tr> 
                                    <tr class="fila">
                                            <td >Cliente</td>
                                            <td ><select name="Cliente" class="textbox" style="width:100%" onclick='LoadStandar(Agencia,this,Estandar);'></select></td>
                                    </tr>    
                                    <tr class="fila">
                                            <td >Standar Job</td>
                                            <td ><select name="Estandar" class="textbox" style="width:100%;"></td>
                                    </tr>
                                    <tr class="fila">
                                            <td ><input type='radio' name='tipoBusqueda' value='AGENCIADESPACHO' onclick='checkRadio(this.value);'></td>
                                            <td >Agencia Despacho</td>
                                            <td ><input:select name="AgenciaDespacho" options="<%=model.agenciaService.getCbxAgencia()%>" attributesText="class='textbox' style='width:100%;'" /></td>
                                    </tr>
                                    <tr class="fila">
                                            <td colspan='2'>Periodo</td>
                                            <td colspan='2'>
                                                    <select class="textbox" style="width:25%" name="Ano"></select>
                                                    <select class="textbox" style="width:25%" name="Mes"></select>   
                                            </td>    
                                    </tr>							
                                    <tr class="fila">
                                            <td colspan='2'>Rango de dias&nbsp;</td>
                                            <td colspan='2'>
                                                    <select class="textbox" style="width:25%" name="diaInicial"></select>
                                                    <select class="textbox" style="width:25%" name="diaFinal"></select>    
                                            </td>    
                                    </tr>							
                                    <tr class='fila'>
                                            <td colspan='2'>Tipo de Reporte</td>
                                            <td>
                                                    <select class="textbox" style="width:95%" name="Tipo">
                                                    <option value='0'>Agencia</option>
                                                    <option value='1'>Cliente</option>
                                                    <option value='2'>Estandar</option>
                                                    </select>
                                            </td>    
                                    </tr>
                            </table>						
                            </td>
                    </tr>
		</table>
		<br>
		<table>
			<img src="<%=BASEURL%>/images/botones/aceptar.gif" width="90" height="21" onClick="validar();" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" style="cursor:hand">
			<img src="<%=BASEURL%>/images/botones/salir.gif" width="90" height="21" onClick="parent.close();" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" style="cursor:hand">
		</table>
        </form>
        
        <script> 
            Llenar(formulario.Ano,formulario.Mes,formulario.diaInicial,formulario.diaFinal); 
            LoadAgencias(formulario.Agencia,formulario.Cliente,formulario.Estandar);
            formulario.AgenciaDespacho.selectedIndex = 1;
            checkRadio('NORMAL');
            
        </script>
        
        </center>
	</div>	
	<%=datos[1]%>
    </body>
</html>
