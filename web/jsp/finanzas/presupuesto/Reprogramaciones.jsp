<!--  
     - Author(s)       :      MARIO FONTALVO
     - Date            :      10/12/2005  
     - Copyright Notice:      TRANSPORTES SANCHEZ POLO S.A.
  -->
 <%--   
     - @(#)  
     - Description: Permite realizar Reprogramaciones
 --%>
 
 
<%@page contentType="text/html"%>
<%@page session="true"%>
<%@page errorPage="/error/ErrorPage.jsp"%>
<%@page import="java.util.*"%>
<%@page import="com.tsp.finanzas.presupuesto.model.beans.*"%>
<%@include file="/WEB-INF/InitModel.jsp"%>
<%
List                Lista       = modelpto.ReprogamacionSvc.getListaReprogramaciones();
Reprogramacion      dato        = modelpto.ReprogamacionSvc.getDato();
String              Codigo      = (dato!=null)?dato.getCodigo()     :"";
String              Descripcion = (dato!=null)?dato.getDescripcion():"";

String Mensaje       = (request.getParameter("Mensaje")!=null)?request.getParameter("Mensaje"):"";
String NombreBoton   = (dato==null)?"aceptar":"modificar";
String NombreBoton1  = (Lista!=null && Lista.size()>0)?"Ocultar Lista":"Listado";
String BloquearText  = (dato==null)?"":"ReadOnly";
String BloquearBoton = (dato!=null)?"":"Disabled";
%>

<html>
<head>
  <title>Clasificacion</title>
    <script src="<%= BASEURL %>/js/boton.js"></script>
    <link   href="<%= BASEURL %>/css/estilostsp.css" rel='stylesheet'>
  <script>
      function validar(form){
            if (form.Opcion.value=='aceptar' || form.Opcion.value=='modificar'){
                if (form.Codigo.value ==''){
                    alert('Defina el codigo para poder continuar...')
                    return false;
                }
                if (form.Descripcion.value ==''){
                    alert('Defina el descripcion para poder continuar...')
                    return false;
                }
            }
            return true;
      }
    function SelAll1(){
            for(i=0;i<FormularioListado.length;i++)
                    FormularioListado.elements[i].checked=FormularioListado.All.checked;
    }
    function ActAll(){
            FormularioListado.All.checked = true;
            for(i=1;i<FormularioListado.length;i++)	
                    if (!FormularioListado.elements[i].checked){
                            FormularioListado.All.checked = false;
                            break;
                    }
    }      
    
    function validarlistado(form){
            for(i=1;i<FormularioListado.length;i++)	
                    if (FormularioListado.elements[i].checked)
                            return tue;
            alert('Por favor seleccione un registro para poder continuar');
            return false;
    }      
  </script>
</head>
<body>
<div id="capaSuperior" style="position:absolute; width:100%; height:100px; z-index:0; left: 0px; top: 0px;">
 <jsp:include page="/toptsp.jsp?encabezado=REPROGRAMACIONES"/>
</div>
<div id="capaCentral" style="position:absolute; width:100%; height:87%; z-index:0; left: 0px; top: 100px; overflow: scroll;">
<center>

<FORM name='formulario' method='POST' action='<%= CONTROLLERPTO %>?ACCION=/Opciones/Reprogramacion'>
    <table width='380' align='center' border='2' bgcolor="#F7F5F4" >
        <tr>
            <th >
            <table  border='0' width='100%' class='tablaInferior'>  


                <tr>
                   <td colspan='2'>
                    <table border='0' width='100%' cellspacing='0'>
                        <tr>
                         <td class='subtitulo1'  width="80%">REPROGRAMACIONES</td>
                         <td class='barratitulo' width="20%"><img src="<%= BASEURL %>/images/titulo.gif" width="32" height="20"></td>
                        </tr>
                    </table>
                   </td>
                </tr>	                  
            
                <tr class='fila'>
                    <td width='29%'>&nbsp Codigo </td>
                    <td ><input  value='<%= Codigo %>' type='text' name='Codigo'  maxlength='8' SIZE='10' class='comentario' style='text-align:center;' <%= BloquearText %>></td>                        
                </tr>
                <tr class='fila'>
                    <td >&nbsp Descripcion :</td>
                    <td ><input value='<%= Descripcion %>' type='text' name='Descripcion' maxlength='40' class='comentario'style='width:95%;'></td>
                </tr>
            </table>
            </th>
        </tr>
    </table>
    <br>
    
        <input type='hidden' name='Opcion'> 
        
        <img src='<%=BASEURL%>/images/botones/cancelar.gif'            style='cursor:hand' onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" onclick="Opcion.value='Nuevo';               if (validar(formulario)) { formulario.submit(); } ">
        &nbsp;
        <img src='<%=BASEURL%>/images/botones/<%=NombreBoton  %>.gif'  style='cursor:hand' onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" onclick="Opcion.value='<%=NombreBoton  %>';  if (validar(formulario)) { formulario.submit(); } ">
        &nbsp;
        <img src='<%=BASEURL%>/images/botones/salir.gif'               style='cursor:hand' onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" onclick='window.close();'>        

        <!--<img src='<%=BASEURL%>/images/botones/eliminar.gif' style='cursor:hand' onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" onclick="Opcion.value='Eliminar'; if (validarlistado(FormularioListado)) { FormularioListado.submit(); } ">
        &nbsp;
    
        <INPUT type='submit' onclick="Opcion.value='Nuevo';"               value='Nuevo'             >
        <INPUT type='submit' onclick="Opcion.value='<%=NombreBoton  %>';"  value='<%=NombreBoton %>' >
        <INPUT type='submit' onclick="Opcion.value='<%=NombreBoton1 %>';"  value='<%=NombreBoton1 %>'>   -->             
    <br>    
    
</FORM>


<% if (!Mensaje.equals("")) { %>
    
        <table border="2" align="center">
           <tr>
           <td><table width="100%"  border="1" align="center"  bordercolor="#F7F5F4" bgcolor="#FFFFFF">
                <tr>
                 <td width="320" align="center" class="mensajes"><%=Mensaje%></td>
                 <td width="29" background="<%=BASEURL%>/images/cuadronaranja.JPG">&nbsp;</td>
                 <td width="58">&nbsp;</td>
                </tr>
              </table>
           </td>
           </tr>
        </table>  
        
<% } %>


<%  if(Lista!=null && Lista.size()>0) { %>
    <form action='<%= CONTROLLERPTO %>?ACCION=/Opciones/Reprogramacion' method='post' name='FormularioListado' >
        <br>
        <table width="570" border="2" bgcolor="#F7F5F4">
        <tr><td align='center'>
            <table width='100%' class='tablaInferior'>
            <tr>
                <td colspan='7'>

                        <table border='0' width='100%' cellspacing='0'>
                            <tr>
                             <td class='subtitulo1'  width="80%">LISTADO GENERAL DE REPROGRAMACIONES</td>
                             <td class='barratitulo' width="20%"><img src="<%= BASEURL %>/images/titulo.gif" width="32" height="20"></td>
                            </tr>
                        </table>
                </td>
            </tr>
            <tr class='tblTitulo'>
                <th class='bordereporte'  width='20' >N�</th>
                <th class='bordereporte'  width='20' ><input type='checkbox' name='All' onclick='jscript: SelAll1();'></th>
                <th class='bordereporte'  width='70' >CODIGO</th>
                <th class='bordereporte'  width='340'>DESCRIPCION</th>
                <th class='bordereporte'  width='60' >ESTADO</th>
                <th class='bordereporte'  width='60' >EDITAR</th>
            </tr>
            <%
                int Cont = 1;
                Iterator it = Lista.iterator();
                while(it.hasNext()){
                    Reprogramacion dat = (Reprogramacion) it.next();
                    String Estilo = (dat.getEstado().equals("A"))?"filaresaltada": (Cont%2==0?"filagris":"filaazul");
                    %>
            <tr class='<%= Estilo %>'>
                <td class='bordereporte' align='center'><span class='comentario'><%=Cont++%></span></td>
                <td class='bordereporte' align='center'><input type='checkbox' name='LOV' value='<%= dat.getCodigo() %>' onclick='jscript: ActAll();'></td>
                <td class='bordereporte' align='center'><%= dat.getCodigo()     %></td>
                <td class='bordereporte' ><%= dat.getDescripcion()%></td>
                <td class='bordereporte' align='center'><%= (dat.getEstado().equals("A"))?"Anulado":"Activo"%></td>
                <td class='bordereporte' align='center'><img src='<%=BASEURL%>/images/botones/iconos/modificar.gif'   style='cursor:hand'  onclick="window.location.href='<%= CONTROLLERPTO %>?ACCION=/Opciones/Reprogramacion&Opcion=Seleccionar&Codigo=<%= dat.getCodigo() %>'"></td>
            </tr>
            <%  }   %>        
            </table>
            </td>
        </tr>    
        </table>
        <br>
        <input type='hidden' name='Opcion'> 
        
        <img src='<%=BASEURL%>/images/botones/anular.gif'   style='cursor:hand' onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" onclick="Opcion.value='Anular';   if (validarlistado(FormularioListado)) { FormularioListado.submit(); } ">
        &nbsp;
        <img src='<%=BASEURL%>/images/botones/activar.gif'  style='cursor:hand' onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" onclick="Opcion.value='Activar';  if (validarlistado(FormularioListado)) { FormularioListado.submit(); } ">
        &nbsp;
        <img src='<%=BASEURL%>/images/botones/eliminar.gif' style='cursor:hand' onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" onclick="Opcion.value='Eliminar'; if (validarlistado(FormularioListado)) { FormularioListado.submit(); } ">       
        </form>
<%  } %>

</center>
</div>
</body>
</html>
