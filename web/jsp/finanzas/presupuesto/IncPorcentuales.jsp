<!--  
     - Author(s)       :      MARIO FONTALVO
     - Date            :      10/12/2005  
     - Copyright Notice:      TRANSPORTES SANCHEZ POLO S.A.
  -->
 <%--   
     - @(#)  
     - Description: Formulario ingresar los incrementos porcentuales
 --%> 
<%@page session="true"%> 
<%@page errorPage="/error/ErrorPage.jsp"%>
<%@page import="java.*" %>
<%@page import="java.util.*" %>
<%@include file="/WEB-INF/InitModel.jsp"%>
<%@page import="com.tsp.finanzas.presupuesto.model.beans.*" %>
<%@page import="com.tsp.util.Util" %>
<%
   DatosGeneral  Dato  = modelpto.IPorcentualesSvc.getDato();
   List   Lista       = modelpto.IPorcentualesSvc.getListado();
   String ReadOnly    = (Dato!=null)?"readonly":"";
   String Disabled    = (Dato!=null)?"Disable" :"";
   String DisabledInv = (Dato!=null)?"":"Disable";
   String LeyendaBtn1 = (Dato!=null)?"modificar":"aceptar";
   String LeyendaBtn2 = (Dato!=null)? (Dato.getValor("ESTADO").equals("A")?"activar":"anular" ) :"anular";
%>
<html>
<head>
    <script src="<%= BASEURL %>/js/boton.js"></script>
    <link   href="<%= BASEURL %>/css/estilostsp.css" rel='stylesheet'>

<title>Incrementos Porcentuales (Proyectados)</title>
<script language="javascript">
function validarIncPorc(texto, tipo){
    if( texto.value=='' || /^\d+(\.\d*)?$/.test(texto.value) )
        (tipo==0)?distPorcMes(texto.form) :  distPorcAno(texto.form);
    else {
        alert("Formato numerico no valido, se colocara en vacio para que vuelva intentarlo");
        texto.value='';
        (tipo==0)?distPorcMes(texto.form) :  distPorcAno(texto.form);
    }
}
function distPorcAno(theForm){
   with(theForm){
    var suma = 0;
    for (i=0;i<PMensuales.length;i++)
        suma += (PMensuales[i].value != '')? parseFloat(PMensuales[i].value) : 0;
    PAnual.value = FormatNumberIP(suma,3);
   }
}
function distPorcMes(theForm){
   with(theForm){
      var incMensual = ((PAnual.value!='')? parseFloat(PAnual.value) : 0 ) / 12;
      for (i=0;i<PMensuales.length;i++)
            if   (incMensual != 0) PMensuales[i].value = FormatNumberIP(incMensual, 3 );
        else PMensuales[i].value = '';
   }
}
function rellenar(theForm){
   with(theForm){
      for (i=0;i<PMensuales.length;i++)
        if   ( PMensuales[i].value == '' ) PMensuales[i].value = '0';
   }
}

function FormatNumberIP(numero,dec){
    var number = numero.toString();
    if (parseFloat(number) != NaN && parseInt(dec)!=NaN){
        var pos = number.indexOf('.');
        if (pos!=-1){
            var ndec = number.length - pos - 1;
            return (ndec >= dec)? number.substr(0,pos) + number.substr(pos, dec + 1 ) : number + strrepeat('0',dec - ndec);
        }
        else
            return number + '.' + strrepeat('0',dec); 
    }
    else
        return number;
}
function strrepeat(cadena, cant){
    for (cad = "", sri=0; sri<cant;sri++, cad += cadena) ;
    return cad;
}
function IPSubmit (tform){
    with (tform){
        if (Opcion.value!='Nuevo'){
            if (Distrito.value=='' || Ano.value=='' || CUnidad.value ==''){
               alert('Los campos de Distrito, A�o y Codigo de Unidad de Negocio son obligatorios.');
               return false;
            }else if (Opcion.value == 'Grabar' || Opcion.value == 'Modificar'){
               if (DUnidad.value == ''){
                   alert('Debe indicar una descripcion para la unidad de negocio para poder continuar.');
                   return false;               
               }
               else if (PAnual.value==''){
                   alert('Debe indicar por lo menos algun incremento, para poder continuar.');
                   return false;               
               }
               rellenar(tform);
            }
        }
    }
    return true;
}
function cambiarOpcion(texto){document.all('Opcion').value = texto.value;}

function addOption(Comb,valor,texto){
    var Ele = document.createElement("OPTION");
    Ele.value=valor;
    Ele.text=texto;
    Comb.add(Ele);
}
function LlenarAnos(CmbAnno){
    var FechaAct = new Date();
    CmbAnno.length = 0;
    for (i=FechaAct.getYear()-10;i<=FechaAct.getYear()+10;i++) addOption(CmbAnno,i,i)
    CmbAnno.value = FechaAct.getYear();
}

function go (url){ window.location.href = (url); }

function SellAll(elemento){
  var theForm = elemento.form;
  if (elemento.id =='All'){
     for (i=0;i<theForm.length;i++)
          if (theForm.elements[i].type=='checkbox')
             theForm.elements[i].checked=theForm.All.checked;
  }else{
     theForm.All.checked=true;
     for (i=0;i<theForm.length;i++)
        if (theForm.elements[i].type=='checkbox' && !theForm.elements[i].checked)
           theForm.All.checked=false;
  }
}
function validarSellAll(theForm){
    for (i=0;i<theForm.length;i++)
      if (theForm.elements[i].type=='checkbox' && theForm.elements[i].checked)
         return true;
    alert('Debe seleccinar por lo menos un item para poder continuar.');
    return false;
}
</script>
</head>
<body>
<div id="capaSuperior" style="position:absolute; width:100%; height:100px; z-index:0; left: 0px; top: 0px;">
 <jsp:include page="/toptsp.jsp?encabezado=INCREMENTOS PORCENTUALES"/>
</div>
<div id="capaCentral" style="position:absolute; width:100%; height:87%; z-index:0; left: 0px; top: 100px; overflow: scroll;">
<center>



<input type="hidden" name="Opcion" value="">
<form action="<%= CONTROLLERPTO %>?ACCION=/Opciones/IncrementosPorcentuales" method="post" onsubmit="jscript: return IPSubmit(this);" name="Datos">
<input type="hidden" name="Origen" value="Formulario">

    <table width="650" border='2' bgcolor="#F7F5F4">
        <tr><td align="center">
        <!-- Tabla de parametros generales -->
        <table width="100%" class="tablaInferior">
            <tr class="fila">
                <td colspan='5'>
                
                    <table border='0' width='100%' cellspacing='0'>
                        <tr>
                         <td class='subtitulo1'  width="80%">INCREMENTOS PORCENTUALES PROYECTADOS</td>
                         <td class='barratitulo' width="20%"><img src="<%= BASEURL %>/images/titulo.gif" width="32" height="20"></td>
                        </tr>
                    </table>
                
                
                </td>
            </tr>        
            <tr class="fila">
                <td width="18%">Distrito</td>
                <td width="15%"><select name="Distrito" class="select" style="width:100%"><option value="FINV"  selected>FINV</option></select></td>
                <td width="18%">Periodo</td>
                <td width="15%"><select name="Ano"      class="select" style="width:100%"></select></td>
                <td width="*" align="right"><%= (Dato!=null)? "Estado : "+(Dato.getValor("ESTADO").equals("")?"Activo":"Anulado") :""%></td>
            </tr>
            
            <tr class="fila">
                <td>Unid. Neg.</td>
                <td><input type="text" name="CUnidad" value="<%= (Dato!=null)?Dato.getValor("CUNIDAD"):"" %>" class="input" <%= ReadOnly %> ></td>
                <td>Descripcion</td>
                <td colspan="2"><input type="text" name="DUnidad" value="<%= (Dato!=null)?Dato.getValor("DUNIDAD"):"" %>" class="input"></td></tr>
            
            <tr class="fila">
                <td colspan="3">Porcentaje de Incremento Anual</td>
                <td ><input type="text" name="PAnual" value="<%= (Dato!=null)?Dato.getValor("PANUAL"):"" %>" class="input" onfocus="this.select()" onkeyup="jscript: validarIncPorc(this,0);" style="text-align:center;"></td>
                <td width="*"  >(%) .</td></tr>
        </table>
        </td>
        </tr>
        </table>
        <!-- Fin Tabla de parametros generales -->
        <br>
        <!--  Tabla de proyecciones mensuales -->
        
        <table width="800" border='2' bgcolor="#F7F5F4">
        <tr><td align="center">
        <!-- Tabla de parametros generales -->
        <table width="100%" class="tablaInferior">
            <tr class="fila">
                <td colspan='6'>
                
                    <table border='0' width='100%' cellspacing='0'>
                        <tr>
                         <td class='subtitulo1'  width="80%">PORCENTAJE DE INCREMENTOS MENSUALES (%)</td>
                         <td class='barratitulo' width="20%"><img src="<%= BASEURL %>/images/titulo.gif" width="32" height="20"></td>
                        </tr>
                    </table>
                
                
                </td>
            </tr> 
            <tr class="tblTitulo">
                <th width="16%" class='bordereporte'>Enero  </th>
                <th width="16%" class='bordereporte'>Febrero</th>
                <th width="16%" class='bordereporte'>Marzo  </th>
                <th width="16%" class='bordereporte'>Abril  </th>
                <th width="16%" class='bordereporte'>Mayo   </th>
                <th width="16%" class='bordereporte'>Junio  </th></tr>
            <tr>
                <th><input name="PMensuales" type="text" class="input" value="<%= (Dato!=null)?(!Dato.getValor("PMES01").equals("0.000")?Dato.getValor("PMES01"):"") :"" %>" onfocus="this.select()" onkeyup="jscript: validarIncPorc(this,1);" style="text-align:center;"></th>
                <th><input name="PMensuales" type="text" class="input" value="<%= (Dato!=null)?(!Dato.getValor("PMES02").equals("0.000")?Dato.getValor("PMES02"):"") :"" %>" onfocus="this.select()" onkeyup="jscript: validarIncPorc(this,1);" style="text-align:center;"></th>
                <th><input name="PMensuales" type="text" class="input" value="<%= (Dato!=null)?(!Dato.getValor("PMES03").equals("0.000")?Dato.getValor("PMES03"):"") :"" %>" onfocus="this.select()" onkeyup="jscript: validarIncPorc(this,1);" style="text-align:center;"></th>
                <th><input name="PMensuales" type="text" class="input" value="<%= (Dato!=null)?(!Dato.getValor("PMES04").equals("0.000")?Dato.getValor("PMES04"):"") :"" %>" onfocus="this.select()" onkeyup="jscript: validarIncPorc(this,1);" style="text-align:center;"></th>
                <th><input name="PMensuales" type="text" class="input" value="<%= (Dato!=null)?(!Dato.getValor("PMES05").equals("0.000")?Dato.getValor("PMES05"):"") :"" %>" onfocus="this.select()" onkeyup="jscript: validarIncPorc(this,1);" style="text-align:center;"></th>
                <th><input name="PMensuales" type="text" class="input" value="<%= (Dato!=null)?(!Dato.getValor("PMES06").equals("0.000")?Dato.getValor("PMES06"):"") :"" %>" onfocus="this.select()" onkeyup="jscript: validarIncPorc(this,1);" style="text-align:center;"></th></tr>

            <tr class="tblTitulo">
                <th class='bordereporte'>Julio     </th>
                <th class='bordereporte'>Agosto    </th>
                <th class='bordereporte'>Septiembre</th>
                <th class='bordereporte'>Octubre   </th>
                <th class='bordereporte'>Noviembre </th>
                <th class='bordereporte'>Diciembre </th></tr>
            <tr>
                <th><input name="PMensuales" type="text" class="input" value="<%= (Dato!=null)?(!Dato.getValor("PMES07").equals("0.000")?Dato.getValor("PMES07"):"") :"" %>" onfocus="this.select()" onkeyup="jscript: validarIncPorc(this,1);" style="text-align:center;"></th>
                <th><input name="PMensuales" type="text" class="input" value="<%= (Dato!=null)?(!Dato.getValor("PMES08").equals("0.000")?Dato.getValor("PMES08"):"") :"" %>" onfocus="this.select()" onkeyup="jscript: validarIncPorc(this,1);" style="text-align:center;"></th>
                <th><input name="PMensuales" type="text" class="input" value="<%= (Dato!=null)?(!Dato.getValor("PMES09").equals("0.000")?Dato.getValor("PMES09"):"") :"" %>" onfocus="this.select()" onkeyup="jscript: validarIncPorc(this,1);" style="text-align:center;"></th>
                <th><input name="PMensuales" type="text" class="input" value="<%= (Dato!=null)?(!Dato.getValor("PMES10").equals("0.000")?Dato.getValor("PMES10"):"") :"" %>" onfocus="this.select()" onkeyup="jscript: validarIncPorc(this,1);" style="text-align:center;"></th>
                <th><input name="PMensuales" type="text" class="input" value="<%= (Dato!=null)?(!Dato.getValor("PMES11").equals("0.000")?Dato.getValor("PMES11"):"") :"" %>" onfocus="this.select()" onkeyup="jscript: validarIncPorc(this,1);" style="text-align:center;"></th>
                <th><input name="PMensuales" type="text" class="input" value="<%= (Dato!=null)?(!Dato.getValor("PMES12").equals("0.000")?Dato.getValor("PMES12"):"") :"" %>" onfocus="this.select()" onkeyup="jscript: validarIncPorc(this,1);" style="text-align:center;"></th></tr>
        </table>
        <!-- Fin tabla de proyecciones mensuales -->
</td></tr>
</table>
    <br>
    
    <input type='hidden' name='Opcion'>
    
    <img src='<%=BASEURL%>/images/botones/cancelar.gif'                   style='cursor:hand' onMouseOver="botonOver(this);"  onMouseOut="botonOut(this);"      onclick="Opcion.value='Nuevo';   if (IPSubmit(Datos)) { Datos.submit(); } ">
    &nbsp;
    <img src='<%=BASEURL%>/images/botones/buscar<%= Disabled %>.gif'      style='cursor:hand' <% if(Disabled.equals(""))    { %> onMouseOver="botonOver(this);" onMouseOut="botonOut(this);"     onclick="Opcion.value='Buscar';   if (IPSubmit(Datos)) { Datos.submit(); } " <% } %>>
    &nbsp;
    <img src='<%=BASEURL%>/images/botones/<%= LeyendaBtn1 %>.gif'         style='cursor:hand' onMouseOver="botonOver(this);" onMouseOut="botonOut(this);"       onclick="Opcion.value='<%= (LeyendaBtn1.equals("aceptar")?"Grabar":"Modificar") %>';  if (IPSubmit(Datos)) { Datos.submit(); } ">
    &nbsp;
    <img src='<%=BASEURL%>/images/botones/eliminar<%= DisabledInv %>.gif' style='cursor:hand' <% if(DisabledInv.equals("")) { %> onMouseOver="botonOver(this);" onMouseOut="botonOut(this);"     onclick="Opcion.value='Eliminar'; if (IPSubmit(Datos)) { Datos.submit(); } " <% } %>>       
    &nbsp;
    <img src='<%=BASEURL%>/images/botones/<%= LeyendaBtn2+DisabledInv %>.gif' style='cursor:hand' <% if(DisabledInv.equals("")) { %>  onMouseOver="botonOver(this);" onMouseOut="botonOut(this);"     onclick="Opcion.value='<%= (LeyendaBtn2.equals("anular")?"Anular":"Activar") %>'; if (IPSubmit(Datos)) { Datos.submit(); } " <% } %>>
    &nbsp;
    <img src='<%=BASEURL%>/images/botones/salir.gif'               style='cursor:hand' onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" onclick='window.close();'>        
    
        <br>&nbsp;

</form>
<script>
LlenarAnos(Datos.Ano); 
<% if (Dato!=null) out.print("Datos.Ano.value = '"+ Dato.getValor("ANO") +"'"); %>
</script>


<table border='2' width='1150'>
<tr>
<td>
<table class="tablainferior" width='100%' >
    <tr>
        <td colspan='19'>
                <table border='0' width='100%' cellspacing='0'>
                    <tr>
                     <td class='subtitulo1'  width="50%">LISTADO DE INCREMENTOS PORCENTUALES</td>
                     <td class='barratitulo' width="50%"><img src="<%= BASEURL %>/images/titulo.gif" width="32" height="20"></td>
                    </tr>
                </table>        
        </td>
    </tr>

    
    <form action="<%= CONTROLLERPTO %>?ACCION=/Opciones/IncrementosPorcentuales&Opcion=Mostrar" method="post" name="Listado">
    <tr class='fila'>
        <td colspan='2'>Listado de</td> 
        <td colspan='2'><select class="select" name="TipoListado">
                <option value="<%= modelpto.IPorcentualesSvc.SEARCH_NANUL %>">Proyecciones Activas  </option>
                <option value="<%= modelpto.IPorcentualesSvc.SEARCH_ANUL  %>">Proyecciones Anuladas </option>
                <option value="<%= modelpto.IPorcentualesSvc.SEARCH_ALL   %>">Todas las Proyecciones</option>
            </select>
        </td>    
        <td colspan='1'>Periodo </td>
        <td colspan='1'><select name="Ano" class="select" style="width:80"></select></td>
        <th colspan='1'><image src="<%= BASEURL %>/images/botones/iconos/lupa.gif" name="filtrobuscar" width="20" style='cursor:hand' onclick='Listado.submit();'></th>
        <td colspan='12'></td>
    </tr>        
    </form>
    <script>
        LlenarAnos(Listado.Ano); 
        Listado.Ano.value         = '<%= modelpto.IPorcentualesSvc.getAno()         %>';
        Listado.TipoListado.value = '<%= modelpto.IPorcentualesSvc.getTipoListado() %>';
    </script>        
        
        
<form action="<%= CONTROLLERPTO %>?ACCION=/Opciones/IncrementosPorcentuales" method="post" name='listado' >        
    <tr class="tblTitulo">
        <th class='bordereporte' nowrap width='10'><input type="checkbox"  id="All" name="All" onclick="jscript: SellAll(this);"></th>
        <th class='bordereporte' nowrap width='30'>DISTRITO  </th>
        <th class='bordereporte' nowrap width='30'>A�O       </th>
        <th class='bordereporte' nowrap width='30'>UNIDAD    </th>
        <th class='bordereporte' nowrap width='200'>DESCRIPCION</th>
        <th class='bordereporte' nowrap width='80'>P. Anual  </th>
        <th class='bordereporte' nowrap width='50'>Enero     </th>
        <th class='bordereporte' nowrap width='50'>Febrero   </th>
        <th class='bordereporte' nowrap width='50'>Marzo     </th>
        <th class='bordereporte' nowrap width='50'>Abril     </th>
        <th class='bordereporte' nowrap width='50'>Mayo      </th>
        <th class='bordereporte' nowrap width='50'>Junio     </th>
        <th class='bordereporte' nowrap width='50'>Julio     </th>
        <th class='bordereporte' nowrap width='50'>Agosto    </th>
        <th class='bordereporte' nowrap width='50'>Septiembre</th>
        <th class='bordereporte' nowrap width='50'>Octubre   </th>
        <th class='bordereporte' nowrap width='50'>Noviembre </th>
        <th class='bordereporte' nowrap width='50'>Diciembre </th>
        <th class='bordereporte' nowrap width='50'>EDITAR</th>
     </tr>

<input type="hidden" name="Origen" value="Listado">
<% if (Lista!=null && Lista.size()>0){
     Iterator it = Lista.iterator();
     int  Cont = 0;
     while (it.hasNext()){
         DatosGeneral dt = (DatosGeneral) it.next();
         String fondo = (dt.getValor("ESTADO").equals("A")?"filaresaltada": (Cont++%2==0?"filagris":"filaazul"));
         String url   = CONTROLLERPTO + "?ACCION=/Opciones/IncrementosPorcentuales&Opcion=Buscar&Distrito="+ dt.getValor("DISTRITO") +"&Ano="+ dt.getValor("ANO") +"&CUnidad="+ dt.getValor("CUNIDAD");
         String param = dt.getValor("DISTRITO") +"~"+ dt.getValor("ANO") +"~"+ dt.getValor("CUNIDAD");
%>
     <tr class="<%= fondo %>">
        <td nowrap class="bordereporte" align="center"><input type="checkbox"  name="itemsSel" value="<%= param %>" onclick="jscript: SellAll(this);"></td>
        <td nowrap class="bordereporte" align="center"><%= dt.getValor("DISTRITO") %></td>
        <td nowrap class="bordereporte" align="center"><%= dt.getValor("ANO")      %></td>
        <td nowrap class="bordereporte" align="center"><%= dt.getValor("CUNIDAD")  %></td>
        <td nowrap class="bordereporte"><%= dt.getValor("DUNIDAD")  %></td>
        <td nowrap class="bordereporte" align="center"><%= dt.getValor("PANUAL")   %></td>
        <td nowrap class="bordereporte" align="center"><%= dt.getValor("PMES01")   %></td>
        <td nowrap class="bordereporte" align="center"><%= dt.getValor("PMES02")   %></td>
        <td nowrap class="bordereporte" align="center"><%= dt.getValor("PMES03")   %></td>
        <td nowrap class="bordereporte" align="center"><%= dt.getValor("PMES04")   %></td>
        <td nowrap class="bordereporte" align="center"><%= dt.getValor("PMES05")   %></td>
        <td nowrap class="bordereporte" align="center"><%= dt.getValor("PMES06")   %></td>
        <td nowrap class="bordereporte" align="center"><%= dt.getValor("PMES07")   %></td>
        <td nowrap class="bordereporte" align="center"><%= dt.getValor("PMES08")   %></td>
        <td nowrap class="bordereporte" align="center"><%= dt.getValor("PMES09")   %></td>
        <td nowrap class="bordereporte" align="center"><%= dt.getValor("PMES10")   %></td>
        <td nowrap class="bordereporte" align="center"><%= dt.getValor("PMES11")   %></td>
        <td nowrap class="bordereporte" align="center"><%= dt.getValor("PMES12")   %></td>
        <td nowrap class="bordereporte" align="center"><img src="<%= BASEURL %>/images/botones/iconos/modificar.gif"  value="Editar" onclick="go('<%= url %>');" width='20' style='cursor:hand'></td>
     </tr>         
<%   } %>
</table>
</td>
</tr>
</table>
    <br>
    
    <input type='hidden' name='Opcion'>
    
    <img src='<%=BASEURL%>/images/botones/anular.gif'   style='cursor:hand' onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" onclick="Opcion.value='Anular';   if (validarSellAll(listado)) { listado.submit(); } ">
    &nbsp;
    <img src='<%=BASEURL%>/images/botones/activar.gif'  style='cursor:hand' onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" onclick="Opcion.value='Activar';  if (validarSellAll(listado)) { listado.submit(); } ">
    &nbsp;
    <img src='<%=BASEURL%>/images/botones/eliminar.gif' style='cursor:hand' onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" onclick="Opcion.value='Eliminar'; if (validarSellAll(listado)) { listado.submit(); } ">       
    
    <br>&nbsp;
</form>
<% } else { %>
</table>
</td>
</tr>
</table><br>
    
        <table border="2" align="center">
           <tr>
           <td><table width="100%"  border="1" align="center"  bordercolor="#F7F5F4" bgcolor="#FFFFFF">
                <tr>
                 <td width="320" align="center" class="mensajes">No se encontraron regitros</td>
                 <td width="29" background="<%=BASEURL%>/images/cuadronaranja.JPG">&nbsp;</td>
                 <td width="58">&nbsp;</td>
                </tr>
              </table>
           </td>
           </tr>
        </table>    
<% } %>


</center>
</div>
</body>
</html>
