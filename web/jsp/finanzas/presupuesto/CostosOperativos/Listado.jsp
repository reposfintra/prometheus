<!--  
     - Author(s)       :      MARIO FONTALVO
     - Date            :      10/12/2005  
     - Copyright Notice:      TRANSPORTES SANCHEZ POLO S.A.
  -->
 <%--   
     - @(#)  
     - Description: Listado general de estadares con sus costos operativos
 --%> 

<%@page session   = "true"%> 
<%@page errorPage = "/error/ErrorPage.jsp"%>
<%@page import    = "java.util.*" %>
<%@page import    = "com.tsp.finanzas.presupuesto.model.beans.*" %>
<%@page import    = "com.tsp.util.*" %>
<%@include  file  = "/WEB-INF/InitModel.jsp"%>
<%@taglib uri="http://jsptags.com/tags/navigation/pager" prefix="pg" %>
<html>
<head>
    <title>Costos Operativos</title>
    <link href="<%= BASEURL %>/css/estilostsp.css"      rel='stylesheet'>
    <link href="<%= BASEURL %>/css/EstilosReportes.css" rel='stylesheet'>
    <script src="<%= BASEURL %>/js/boton.js"></script>
    <script>
        function newWindow(url, nombre){
           option=" status=yes, width="+ (screen.width-30) +", height="+ (screen.height-80)  +",  scrollbars=yes, statusbars=yes, resizable=yes, menubar=no ,top=10 , left=10 ";
           ventana=window.open('',nombre,option);
           ventana.location.href=url;
           ventana.focus();
        }    
        function goCostosOperativos (item){
           var url = '<%= CONTROLLERPTO %>?ACCION=/Opciones/CostosOperativos&Opcion=BuscarCostos&item='+item+"&Ano=<%= modelpto.CostosOperativosSvc.getAno() %>&Mes=<%= modelpto.CostosOperativosSvc.getMes() %>";
           newWindow(url,'x');
        }
    </script>
</head>
<body>
<div id="capaSuperior" style="position:absolute; width:100%; height:100px; z-index:0; left: 0px; top: 0px;">
    <jsp:include page="/toptsp.jsp?encabezado=Listado Presupuesto Costos Operativos"/>
</div>
<div id="capaCentral" style="position:absolute; width:100%; height:87%; z-index:0; left: 0px; top: 100px; overflow: scroll;">


    <center>
    <table border='2' >
    <tr>
    <td>

        <table width='100%' border="1" bordercolor="#999999" bgcolor="#F7F5F4" cellpadding='3' cellspacing='1'>
        <tr class='subtitulo1'>
            <td class='subtitulo1'  width="80%" colspan='2'>PRESUPUESTO COSTOS OPERATIVOS</td>
            <td class='barratitulo' width="20%" colspan='4'><img src="<%= BASEURL %>/images/titulo.gif" width="32" height="20"></td>
        </tr>
        <tr class="tblTitulo">
            <th width='140' >AGENCIA</th>
            <th width='300' >CLIENTE</th>
            <th width='350' >ESTANDAR</th>
            <th width='45'  >VIAJES</th>
            <th width='100' >COSTOS</th>
            <th width='45'  >EDIT</th>
        </tr>
        <%
            String agenciaAnt = null;
            String clienteAnt = null;
            String fondoCliente = "#F7F5F4";
            String fondoAgencia = "#F7F5F4";
            List lista = modelpto.CostosOperativosSvc.getListado();
            
            String style      = "simple";
            String position   = "bottom";
            String index      = "center";
            int maxPageItems  = 15;
            int maxIndexPages = 10;
        %>
            
            <pg:pager
                 items         ="<%= lista.size()%>"
                 index         ="<%= index %>"
                 maxPageItems  ="<%= maxPageItems %>"
                 maxIndexPages ="<%= maxIndexPages %>"
                 isOffset      ="<%= true %>"
                 export        ="offset,currentPageNumber=pageNumber"
                 scope         ="request">
            
            <%  for (int i = offset.intValue(), l = Math.min(i + maxPageItems, lista.size()); i < l; i++){
                DatosGeneral dt = (DatosGeneral) lista.get(i);
                String agenciaNue = dt.getValor("NOMBREAGENCIA");
                String clienteNue = dt.getValor("NOMBRECLIENTE");
                //int limit = 
                %>
                <pg:item>
                    <tr class='<%= (i%2==0?"filagris":"filaazul") %>'>
                    <% if ( agenciaAnt == null || !agenciaAnt.equals(agenciaNue) ){
                        int num = modelpto.CostosOperativosSvc.getRepetidas("NOMBREAGENCIA", agenciaNue, offset.intValue(), (Math.min( offset.intValue() + 15, lista.size()))  ); 
                        fondoAgencia = (fondoAgencia.equals("#F7F5F4")?"#EEEEF2":"#F7F5F4");
                    %>
                        <td nowrap class="bordereporte" style="background-color:<%= fondoAgencia %>;" rowspan='<%= num %>' valign='top'><%= dt.getValor("NOMBREAGENCIA") %></td>
                    <% } %>

                    <% if ( clienteAnt == null || !clienteAnt.equals(clienteNue) ){
                        int num = modelpto.CostosOperativosSvc.getRepetidas("NOMBRECLIENTE", clienteNue, offset.intValue(), (Math.min( offset.intValue() + 15, lista.size())) ); 
                        fondoCliente = (fondoCliente.equals("#F7F5F4")?"#EEEEF2":"#F7F5F4");
                    %>
                        <td nowrap class="bordereporte" style="background-color:<%= fondoCliente %>;" rowspan='<%= num %>' valign='top'>&nbsp;<%= "[" + dt.getValor("CODIGOCLIENTE") + "] " + dt.getValor("NOMBRECLIENTE")  %></td>
                    <% } %>
                        <td nowrap class="bordereporte" >&nbsp;<%= dt.getValor("STDJOBDESC")    %></td>
                        <td nowrap class="bordereporte" align='right' ><b>&nbsp;<%= dt.getValor("TOTAL") %>&nbsp; </b></td>
                        <td nowrap class="bordereporte" align='right' ><b>&nbsp;<%= (dt.getValor("TOTALCOSTOUN").equals("0")?"":UtilFinanzas.customFormat(dt.getValor("TOTALCOSTO"))) %>&nbsp; </b></td>
                        <td nowrap class="bordereporte" align='center' ><input type='image' src='<%=BASEURL%>/images/botones/iconos/modificar.gif' onclick='goCostosOperativos(<%=  i %>);' title='Edicion de Costos Operativos'></td>
                    </tr>
                </pg:item>
        <%  agenciaAnt = agenciaNue;
            clienteAnt = clienteNue;
            } 
        %>
            <tr>
                <td colspan="6" align="center" >
                    <pg:index>
                    <jsp:include page="/WEB-INF/jsp/googlesinimg.jsp" flush="true"/>
                    </pg:index> 
                </td>
            </tr>
            </pg:pager>        
        </table>
    </td>    
    </tr>
    </table>
    
        <br>
        <input type='image' src='<%=BASEURL%>/images/botones/regresar.gif' onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" onclick="window.location.href='<%= CONTROLLERPTO %>?ACCION=/Opciones/CostosOperativos&Opcion=Reiniciar';">
        &nbsp;
        <input type='image' src='<%=BASEURL%>/images/botones/salir.gif'   onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" onclick='window.close();'>
    
    
    </center>
</div>    
</body>
</html>
