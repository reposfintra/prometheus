<!--  
     - Author(s)       :      MARIO FONTALVO
     - Date            :      10/12/2005  
     - Copyright Notice:      TRANSPORTES SANCHEZ POLO S.A.
  -->
 <%--   
     - @(#)  
     - Description: Formulario Inicial de definicion parametros
                    para el listado de estandares con sus costos operativos
 --%> 


<%@page session   = "true"%> 
<%@page errorPage = "/error/ErrorPage.jsp"%>
<%@page import    = "java.util.*" %>
<%@page import    = "com.tsp.finanzas.presupuesto.model.beans.*" %>
<%@include file   = "/WEB-INF/InitModel.jsp"%>
<%
    String Mensaje = (request.getParameter("Mensaje")!=null?request.getParameter("Mensaje"):"");
%>
<html>
<head>
    <title>Presupuesto de Ventas - Costos Operativos</title>
    <script src="<%= BASEURL %>/js/boton.js"></script>
    <link   href="<%= BASEURL %>/css/estilostsp.css" rel='stylesheet'>
    <script>
        var datos  = [
        <%
            List lista = modelpto.CostosOperativosSvc.getListaAgCl();
            for (int i=0; lista!=null && i<lista.size();i++){
                DatosGeneral dt = (DatosGeneral) lista.get(i);
                out.print("\n\t\t"+
                          "['" + (dt.getValor("cag")!=null? dt.getValor("cag") : "") +
                          "','"+ (dt.getValor("nag")!=null? dt.getValor("nag") : "") +
                          "','"+ (dt.getValor("ccl")!=null? dt.getValor("ccl") : "") +
                          "','"+ (dt.getValor("ncl")!=null? dt.getValor("ncl") : "") +
                          "']");
                if ((i+1)!=lista.size()) out.print(",");
            }
        %>
        ];
    
    
        function addOption(Comb,valor,texto){
            var Ele = document.createElement("OPTION");
            Ele.value=valor;
            Ele.text=texto;
            Comb.add(Ele);
        }    
        function Llenar(CmbAnno, CmbMes){
                var Meses    = new Array('Enero','Febrero','Marzo','Abril','Mayo','Junio','Julio','Agosto','Septiembre','Octubre','Noviembre','Diciembre');
                var FechaAct = new Date();
                CmbAnno.length = 0;
                CmbMes.length  = 0;                
                for (i=FechaAct.getYear()-10;i<=FechaAct.getYear()+10;i++) addOption(CmbAnno,i,i)
                CmbAnno.value = FechaAct.getYear();
                for (i=0;i<Meses.length;i++)
                    if ((i+1)<10) addOption(CmbMes,'0'+(i+1),Meses[i]);
                    else          addOption(CmbMes,(i+1),Meses[i]);                
                CmbMes.value  = ((FechaAct.getMonth()+1)<10)?('0'+(FechaAct.getMonth()+1)):(FechaAct.getMonth()+1);	
        } 
        
        function llenarCmbAg(comboAG){
            var aux = '?';
            comboAG.length = 0;
            for (i=0;i<datos.length;i++){
                var dt = datos[i];
                if (aux!=dt[0]){
                    addOption(comboAG, dt[0], dt[1]);
                }
                aux = dt[0];
            }
            if (comboAG.length==0) 
                addOption(comboAG ,'-1', 'No se encontraron agencias');
            
        }        
        function llenarCmbCl(comboAG, comboCL ){
            var aux = '?';
            comboCL.length = 0;
            for (i=0;i<datos.length;i++){
                var dt = datos[i];
                if (dt[0]==comboAG.value){
                    if (aux!=dt[2]){
                        if (aux == '?') 
                            addOption(comboCL ,'ALL', 'Todos los clientes');
                        addOption(comboCL ,dt[2], dt[3]);
                    }
                    aux = dt[2];
                }
            }
            if (comboCL.length==0) 
                addOption(comboCL ,'-1', 'No se encontraron clientes relacionados');
        }
        
        
        function _onsubmit (form){
            if (form.Agencia.value=='-1'){
                alert('Debe definir una agencia para poder continuar');
                return false;
            }
            if (form.Cliente.value=='-1'){
                alert('Debe definir un cliente para poder continuar');
                return false;
            }
            return true;
            
        }
     </script>   
</head>
<body>


<div id="capaSuperior" style="position:absolute; width:100%; height:100px; z-index:0; left: 0px; top: 0px;">
 <jsp:include page="/toptsp.jsp?encabezado=Costos Operativos"/>
</div>
<div id="capaCentral" style="position:absolute; width:100%; height:87%; z-index:0; left: 0px; top: 100px; overflow: scroll;">


    <center>
    <form action='<%= CONTROLLERPTO %>?ACCION=/Opciones/CostosOperativos&Opcion=Buscar' method='post' name='fpto' onsubmit='return _onsubmit(this);' >
    
    <table width="500" align="center" border='2'>
    <tr><td>
        <table width="100%" align="center" class="tablaInferior">
            <tr >
                <td class='subtitulo1'  width="80%">PRESUPUESTO COSTOS OPERATIVOS</td>
                <td class='barratitulo' width="20%"><img src="<%= BASEURL %>/images/titulo.gif" width="32" height="20"></td>
            </tr>
            <tr class="filaresaltada">
                <td colspan='2' ><span style='width:25%'>Agencias </span><select class="select" style="width:65%" name="Agencia" onchange="llenarCmbCl(this, fpto.Cliente);"></select></td>
            </tr>
            <tr class="filaresaltada">
                <td colspan='2' ><span style='width:25%'>Clientes</span><select class="select" style="width:65%" name="Cliente"></select></td>
            </tr>
            
            <tr><td colspan='2' class="filaresaltada">Periodo al que desea asignar nuevos costos</td></tr>
            <tr><td colspan='2' class="filaresaltada" align='center'>
                        <select class="select" style="width:30%" name="Ano"></select>
                        <select class="select" style="width:30%" name="Mes"></select>    
                 </td>
            </tr>
        </table>
        </td>
        </tr>
    </table>
    
    <br> 
    <img src='<%=BASEURL%>/images/botones/buscar.gif' style='cursor:hand' onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" onclick='if ( _onsubmit(fpto) ) fpto.submit();'>
    &nbsp;
    <img src='<%=BASEURL%>/images/botones/salir.gif'  style='cursor:hand' onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" onclick='window.close();'>
    
        
    </form>
    <script> 
        Llenar(fpto.Ano,fpto.Mes); 
        llenarCmbAg(fpto.Agencia);
        <% if (!modelpto.CostosOperativosSvc.getAgencia().equals("")) { %>
              fpto.Agencia.value = '<%= modelpto.CostosOperativosSvc.getAgencia() %>';
        <% } %>
        llenarCmbCl(fpto.Agencia, fpto.Cliente);
        <% if (!modelpto.CostosOperativosSvc.getCliente().equals("")) { %>        
              fpto.Cliente.value = '<%= modelpto.CostosOperativosSvc.getCliente() %>';
        <% } %>
    </script>

<% if (!Mensaje.equals("")) { %>
    
        <table border="2" align="center">
           <tr>
           <td><table width="100%"  border="1" align="center"  bordercolor="#F7F5F4" bgcolor="#FFFFFF">
                <tr>
                 <td width="320" align="center" class="mensajes"><%=Mensaje%></td>
                 <td width="29" background="<%=BASEURL%>/images/cuadronaranja.JPG">&nbsp;</td>
                 <td width="58">&nbsp;</td>
                </tr>
              </table>
           </td>
           </tr>
        </table>  
        
<% } %>

    </center>
</div>    
</body>
</html>
