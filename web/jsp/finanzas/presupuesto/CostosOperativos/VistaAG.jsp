<!--  
     - Author(s)       :      MARIO FONTALVO
     - Date            :      10/12/2005  
     - Copyright Notice:      TRANSPORTES SANCHEZ POLO S.A.
  -->
 <%--   
     - @(#)  
     - Description: Vista general de agencia con sus costos operativos
 --%> 
<%@page session   = "true"%> 
<%@page errorPage = "/error/ErrorPage.jsp"%>
<%@page import    = "java.util.*" %>
<%@page import    = "com.tsp.finanzas.presupuesto.model.beans.*" %>
<%@page import    = "com.tsp.util.*" %>
<%@include file   = "/WEB-INF/InitModel.jsp"%>
<%@taglib uri="http://jsptags.com/tags/navigation/pager" prefix="pg" %>
<html>
<head>
    <title>Vista de Costos Operativos</title>
    <script src ="<%= BASEURL %>/js/boton.js"></script>
    <script src ="<%= BASEURL %>/js/reporte.js"></script>
    <link   href="<%= BASEURL %>/css/estilostsp.css" rel='stylesheet'>
    <script>
        function goSubView (vista, ano, mes, prolongacion, agencia, nagencia){
           var url   = '<%= CONTROLLERPTO %>?ACCION=/Opciones/CostosOperativos&Opcion=InitVista' +
                       '&Vista='+ vista +'&Agencia=' + agencia + '&Ano=' + ano + '&Mes=' + mes + "&Prolongacion=" + prolongacion + "&NAgencia=" + nagencia;
           var winct = open (url,'VistaCostos','menubar=no, resizable=yes, top=10, left=10, width='+ (screen.width-30) +', height='+ (screen.height-110)  +',  status=yes ');
        }
    </script>
</head>
<body>
<div id="capaSuperior" style="position:absolute; width:100%; height:100px; z-index:0; left: 0px; top: 0px;">
    <jsp:include page="/toptsp.jsp?encabezado=PRESUPUESTO COSTOS OPERATIVOS"/>
</div>
<div id="capaCentral" style="position:absolute; width:100%; height:87%; z-index:0; left: 0px; top: 100px; overflow: scroll;">
 <center>
 <%  
     List lista = modelpto.CostosOperativosSvc.getVistaAg(); 
     String Periodo    = (String)  modelpto.CostosOperativosSvc.getParamView("AG-Periodo");
     String Ano        = (String)  modelpto.CostosOperativosSvc.getParamView("AG-Ano");
     String Mes        = (String)  modelpto.CostosOperativosSvc.getParamView("AG-Mes");
     int Prolongacion  = (Integer) modelpto.CostosOperativosSvc.getParamView("AG-Prolongacion");
     int maxPageItems  = 15;
     int maxIndexPages = 10;
     if (lista!=null && !lista.isEmpty()){
    %>
        <table border='2' width='<%= (250 + (110*Prolongacion) ) %>'>
        <tr>
            <td colspan='<%= Prolongacion %>'>
                    <table width='100%' cellpadding='0' cellspacing='0'>
                    <tr>
                        <td class='subtitulo1'  width='75%' >&nbsp; VISTA POR AGENCIAS</td>
                        <td class='barratitulo' ><img src="<%= BASEURL %>/images/titulo.gif" width="32" height="20"></td>
                    </tr>
                    </table>            
            </td>
        </tr>    
        <tr>
            <td>
            <pg:pager items="<%= lista.size()%>" index="center" maxPageItems="<%= maxPageItems %>" maxIndexPages ="<%= maxIndexPages %>" isOffset = "true" export ="offset,currentPageNumber=pageNumber" scope="request">
                <table border="1" bordercolor="#999999" bgcolor="#F7F5F4" cellpadding='3' cellspacing='1'>
                <tr class='tblTitulo'>
                    <td>Agencia</td>
                    <% for (int j = 0 ; j < Prolongacion ; j++ ) { %>
                    <td align='center'><%= UtilFinanzas.getNextPeriodoNormal(Periodo, j) %></td>
                    <% } %>
                    
                </tr>
                
                
                <% for (int i = offset.intValue(), l = Math.min(i + maxPageItems, lista.size()); i < l; i++){ 
                      DatosGeneral dt = (DatosGeneral) lista.get(i);
                      String url = " goSubView ('CT','" + Ano + "','" + Mes + "','" + Prolongacion + "','" + dt.getValor("CODIGO") + "','" + dt.getValor("DESCRIPCION") + "'); ";
                %>
                <pg:item>
                    <tr class='<%= (i%2==0?"filagris":"filaazul") %>' style='cursor:hand;' onMouseOver='cambiarColorMouse(this)' onclick="<%= url %>">
                       <td class='bordereporte' width='250'>&nbsp;<%= dt.getValor("DESCRIPCION") %></td>
                       <% for (int j = 0; j< Prolongacion ; j++) { %>
                       <td class='bordereporte' width='110' align='right'><%= UtilFinanzas.customFormat(dt.getValor("v"+j)) %>&nbsp;</td>
                       <% } %>
                    </tr>
                </pg:item>
                <% } %>
                <tr>
                    <th colspan='<%= (Prolongacion+1) %>'><pg:index><jsp:include page="/WEB-INF/jsp/googlesinimg.jsp" flush="true"/></pg:index></th>
                </tr>
                </table>
            </pg:pager>
            </td>
        </tr>
        </table>
    
    <% } else { %>
        <span class='informacion'>No se encontraron registros!!!!</span>
    <% } %>  
    
        <br>
        <input type='image' src='<%=BASEURL%>/images/botones/regresar.gif' onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" onclick="window.location.href='<%= CONTROLLERPTO %>?ACCION=/Opciones/CostosOperativos&Opcion=InitParamsVista';">
        &nbsp;
        <input type='image' src='<%=BASEURL%>/images/botones/salir.gif'   onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" onclick='window.close();'>
    
 </center>
</div>
</body>
</html>
