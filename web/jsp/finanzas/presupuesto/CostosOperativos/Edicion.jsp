<!--  
     - Author(s)       :      MARIO FONTALVO
     - Date            :      10/12/2005  
     - Copyright Notice:      TRANSPORTES SANCHEZ POLO S.A.
  -->
 <%--   
     - @(#)  
     - Description: Formulario de edicion de costos operativos
 --%> 
<%@page session   = "true"%> 
<%@page errorPage = "/error/ErrorPage.jsp"%>
<%@page import    = "java.util.*" %>
<%@page import    = "com.tsp.finanzas.presupuesto.model.beans.*" %>
<%@page import    = "com.tsp.util.*" %>
<%@include  file  = "/WEB-INF/InitModel.jsp"%>
<% String Mensaje = (request.getParameter("Mensaje")!=null?request.getParameter("Mensaje"):""); %>
<html>
<head>
    <title>Costos Operativos</title>
    <link href="<%= BASEURL %>/css/estilostsp.css"      rel='stylesheet'>
    <link href="<%= BASEURL %>/css/EstilosReportes.css" rel='stylesheet'>
    <script src="<%= BASEURL %>/js/boton.js"></script>
    <script>
       var Memory = "";
       var comboHTML = "<select class='select' style='width:100%' name='CostoOperativo'>"+
       <% 
           modelpto.CostosOperativosSvc.BuscarElementosCostosOperativos();
           List lista = modelpto.CostosOperativosSvc.getListaElementosCostosOperativos();
           if ( !lista.isEmpty() ){
             for (int i = 0 ; i <lista.size() ; i++ ){
                DatosGeneral dt = (DatosGeneral) lista.get(i);
                out.print("\n\t\t\"<option value='"+ dt.getValor("codigo") +"'>[" + dt.getValor("codigo") + "] " +  dt.getValor("descri") +"</option>\"+");
             }
           }
       %>
       "</select>";
    
       function addNuevoCosto( costoOp, valorOp){
          var tab = document.getElementById('tabla');
          tr = tabla.insertRow();
          tr.className = (tr.rowIndex%2==0?'filagris':'filaazul');
          td = tr.insertCell();
          td.className = 'bordereporte';
          td.innerHTML = comboHTML;
          
          var combo = td.firstChild;
          combo.value = costoOp;
          
          td = tr.insertCell();
          td.className = 'bordereporte';
          td.innerHTML = "<input type='text' name='Valor' class='input' style='width:100%; text-align:right' value='"+ formato(valorOp) +"' onkeyup='calcular();' onblur='this.value=formato(this.value);' onfocus='this.value=sinformato(this.value);  this.select()'>";
          td = tr.insertCell();
          td.className = 'bordereporte';
          td.align = 'center';
          td.innerHTML = "<img  src='<%= BASEURL %>/images/delete.gif' width='16' style='cursor:hand' onclick='saveItem (this.parentNode.parentNode);  tabla.deleteRow(this.parentNode.parentNode.rowIndex); reColorear(); calcular(); ' >";
          return false;
       }
       
       function _onsubmit(tform){
          with (tform){
             var revisados = "|";
             for (i=0; i<elements.length; i++){
             
                if ( elements[i].name=='CostoOperativo' ){
                    if (elements[i].value==''){
                        elements[i].focus();
                        sombrear(elements[i],'filaroja');
                        alert ('Debe indicar un elemento de costo para este elemento');
                        return false;
                        }
                    
                    if ( revisados.indexOf('|' + elements[i].value + '|')!=-1 ){
                        elements[i].focus();
                        sombrear(elements[i],'filaroja');
                        alert ('Este elemento ya se definio con anterioridad, por favor verifiquelo');
                        return false;                    
                    }
                    else
                      revisados +=  elements[i].value + '|';
                    reColorear();  
                }
                else if ( elements[i].name=='Valor' ){
                    if (elements[i].value==''){
                       sombrear(elements[i],'filaroja'); 
                       elements[i].focus();
                       alert('Por favor verifique el valor asignado en el siguiente item');
                       return false; 
                    }                      
                    reColorear();
                }
             }
          }
          return true;
       }
       
       function reColorear (){
            var i;
            for (i=2; i<tabla.rows.length; i++){
                tabla.rows[i].className = (i%2==0?'filagris':'filaazul');
            }
       }
       
       
       function sombrear(obj, clase){
          var row = obj.parentNode.parentNode;
          tabla.rows[row.rowIndex].className=clase;
       }
       
       function calcular (){
          var total = 0;
          with (fpto){
              for (i=0; i<elements.length; i++){
                  if (elements[i].name=='Valor'){
                     var valor = elements[i].value.replace( new RegExp(",","g"), "");
                     if (!isNaN ( parseFloat(valor)) ){
                        total += parseFloat(valor);
                     }
                  }
              }
          }
          
          totCU.innerHTML = formato(total) + '&nbsp;';
          totCT.innerHTML = formato(total * parseFloat( TotalViaje.innerHTML )) + '&nbsp;';
          totUT.innerHTML = formato( parseFloat( sinformato(totTP.innerHTML) ) - parseFloat( sinformato(totCT.innerHTML) )) + '&nbsp;';
          var porcentaje  = (parseFloat( sinformato(totUT.innerHTML) ) / parseFloat( sinformato(totTP.innerHTML) ) * 100) + '';
          if (porcentaje.length>5) porcentaje = porcentaje.substr(0,5);
          totTR.innerHTML =  (porcentaje=='NaN'?0:porcentaje) + ' %&nbsp;';
       }
       
       function formato(numero){
           
           var tmp = parseInt(numero) ;
           var factor = (tmp < 0 ? - 1 : 1);
           tmp *= factor;
          
           var num = '';
           var pos = 0;
           while(tmp>0){
                  if (pos%3==0 && pos!=0) num = ',' + num;
                  res  = tmp % 10;
                  tmp  = parseInt(tmp / 10);
                  num  = res + num  ;
                  pos++;
           }
           return (factor==-1 ? '-' : '' ) + num ;
        }      
        
        function sinformato(element){
           return element.replace( new RegExp(",","g") ,'');
        }
        
        function saveItem (tr){
           var elemento = tr.cells[0].firstChild.value;
           var valor    = tr.cells[1].firstChild.value;
           Memory = (elemento + '~' + sinformato(valor));
           
           if (Memory!='~'){
               var obj = document.getElementById ('btnRestablecer');
               obj.src = "<%=BASEURL%>/images/botones/restablecer.gif";
               obj.onmouseover = new Function ("botonOver(this);");
               obj.onmouseout  = new Function ("botonOut (this);");
           }
        }
        
        function restaurar (){
            if (Memory!=''){
                var dt = Memory.split('~');
                addNuevoCosto(dt[0], dt[1]);
                Memory = '';
                var obj = document.getElementById ('btnRestablecer');
                obj.src = "<%=BASEURL%>/images/botones/restablecerDisable.gif";
                obj.onmouseover = new Function ("");
                obj.onmouseout  = new Function ("");    
                calcular();
            }
        }
      
    </script>
</head>
<body>
<div id="capaSuperior" style="position:absolute; width:100%; height:100px; z-index:0; left: 0px; top: 0px;">
    <jsp:include page="/toptsp.jsp?encabezado=Edicion Presupuesto Costos Operativos"/>
</div>
<div id="capaCentral" style="position:absolute; width:100%; height:87%; z-index:0; left: 0px; top: 100px; overflow: scroll;">

    <center>     
    <form action='<%= CONTROLLERPTO %>?ACCION=/Opciones/CostosOperativos' method='post' onsubmit='return _onsubmit(this);' name='fpto'>
        <% DatosGeneral dt = modelpto.CostosOperativosSvc.getDatos(); %>

        <input type='hidden' name='Estandar' value='<%= dt.getValor("STDJOBNO")%>'>
        <input type='hidden' name='Ano'      value='<%= modelpto.CostosOperativosSvc.getAno() %>'>
        <input type='hidden' name='Mes'      value='<%= modelpto.CostosOperativosSvc.getMes() %>'>
        <input type='hidden' name='Opcion'   value='Guardar Cambios' >

        <table width='700' border='2'>
        <tr>
        <td>
            <table width='100%' cellpadding='1' cellspacing='1' border="1" bordercolor="#999999" bgcolor="#F7F5F4">
                <tr class='subtitulo1'>
                    <td class='subtitulo1'  colspan='2'>DATOS GENERALES DEL ESTANDAR</td>
                    <td class='barratitulo' colspan='2'><img src="<%= BASEURL %>/images/titulo.gif" width="32" height="20"></td>
                </tr>
                <tr >
                    <td class='fila'>&nbsp;Estandar</td>
                    <td colspan='3' class='letra'>&nbsp;<%= "[" + dt.getValor("STDJOBNO") + "]  " + dt.getValor("STDJOBDESC")%></td>
                </tr>

                <tr >
                    <td width='75'  class='fila'>&nbsp;Agencia</td>
                    <td width='275' class='letra'>&nbsp;<%= dt.getValor("NOMBREAGENCIA")%></td>
                    <td width='75'  class='fila'>&nbsp;Cliente</td>
                    <td width='275' class='letra'>&nbsp;<%= dt.getValor("NOMBRECLIENTE")%></td>
                </tr>

                <tr>
                    <td  class='fila'>&nbsp;Origen </td><td class='letra'>&nbsp;<%= dt.getValor("NOMBREORIGEN")%></td>
                    <td  class='fila'>&nbsp;Destino</td><td class='letra'>&nbsp;<%= dt.getValor("NOMBREDESTINO")%></td>
                </tr>
                </table>
        </td>
        </tr>
        </table>                
                
                <BR>
                
        <table width='700' border='2'>
        <tr>
        <td>                
                <table width='100%' cellpadding='1' cellspacing='1' border="1" bordercolor="#999999" bgcolor="#F7F5F4">
                    <tr class='subtitulo1'>
                        <td class='subtitulo1'  colspan='3'>RESUMEN</td>
                        <td class='barratitulo' colspan='2'><img src="<%= BASEURL %>/images/titulo.gif" width="32" height="20"></td>
                    </tr>                
                    <tr class="tblTitulo">
                       <th width='20%'>Periodo     </th>                       
                       <th width='20%'>Tarifa      </th>
                       <th width='20%'>Moneda      </th>
                       <th width='20%'>Tasa        </th>
                       <th width='20%'>Nro. Viajes </th>
                    </tr>    
                    <tr class='filaazul' >
                        <td align='center' ><%= modelpto.CostosOperativosSvc.getAno() + modelpto.CostosOperativosSvc.getMes() %></td>                        
                        <td align='right'  ><%= UtilFinanzas.customFormat2(dt.getValor("TARIFA")) %>&nbsp;</td>
                        <td align='center' ><%= dt.getValor("MONEDA") %></td>
                        <td align='right'  ><%= UtilFinanzas.customFormat2(dt.getValor("TASA"))    %>&nbsp;</td>
                        <td align='right'  id='TotalViaje'><%= dt.getValor("TOTAL")  %>&nbsp;</td>                        
                    </tr>
                    <tr class="tblTitulo">
                       <th >Total Pto     </th>
                       <th >Tot.Costo Unit</th>
                       <th >Tot.Costo     </th>
                       <th >Utilidad      </th>
                       <th >Rentabilidad  </th>
                       
                    </tr>    
                    <tr class='filaazul' >
                        <td align='right'  id='totTP'><%= UtilFinanzas.customFormat(dt.getValor("TOTALPTO"))%>&nbsp;</td>
                        <td align='right'  id='totCU'>0&nbsp;</td>
                        <td align='right'  id='totCT'>0&nbsp;</td>
                        <td align='right'  id='totUT'>0&nbsp;</td>
                        <td align='right'  id='totTR'>0&nbsp;</td>
                    </tr>
                </table>
        </td>
        </tr>
        </table>


        <br><br>



        <table width='600'  border='2' >
            <tr>
                <th>
                    <table id='tabla' width='100%' cellpadding='1' cellspacing='1' border="1" bordercolor="#999999" bgcolor="#F7F5F4">
                        <tr >
                            <td class='subtitulo1'  >COSTOS OPERATIVOS POR VIAJE</td>
                            <td class='barratitulo' colspan='2'><img src="<%= BASEURL %>/images/titulo.gif" width="32" height="20"></td>
                        </tr>    
                        <tr class='tblTitulo'>
                            <td width='70%'>Elemento del gasto</td> 
                            <td width='25%'>Valor Unitario</td>
                            <td width='5%' >Eliminar</td>
                        </tr>            
                    </table>    
                </th>
            </tr>
        </table>
        <br>
        
        <image style='cursor:hand' src='<%=BASEURL%>/images/botones/agregar.gif'                onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" onclick="addNuevoCosto('','');" >
        &nbsp;
        <input type='image' src='<%=BASEURL%>/images/botones/modificar.gif' onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" title='Presione para guardar definitivamente los cambios'>
        &nbsp;
        <image style='cursor:hand' src='<%=BASEURL%>/images/botones/restablecerDisable.gif'     onclick='restaurar();' id='btnRestablecer' title='Esta opcion permitira restablecer el ultimo item eliminado'>
        &nbsp;
        <image style='cursor:hand' src='<%=BASEURL%>/images/botones/salir.gif'                  onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" onclick='window.close();'>
        
    </form>
    <span class='informacion'><b>
    <%= Mensaje %></b>
    </span>

    </center>
</div>
    
</body>

</html>
<script>
  <%    List LCO = modelpto.CostosOperativosSvc.getListaElementosCostosOperativosAsociadas();
        if ( !LCO.isEmpty() ){
            for (int i = 0; i< LCO.size(); i++ ){
                DatosGeneral co = (DatosGeneral) LCO.get(i);
                out.print("\n\t\t addNuevoCosto('"+ co.getValor("elemento") +"','"+ co.getValor("valor") +"');");
            }
            out.print("\n\t\t calcular();");
        }
  %>
</script>