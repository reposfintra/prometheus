<!--  
     - Author(s)       :      MARIO FONTALVO
     - Date            :      10/12/2005  
     - Copyright Notice:      TRANSPORTES SANCHEZ POLO S.A.
  -->
 <%--   
     - @(#)  
     - Description: Formulario Inicial de definicion parametros
                    de tipo de vistas
 --%> 
<%@page session   = "true"%> 
<%@page errorPage = "/error/ErrorPage.jsp"%>
<%@page import    = "java.util.*" %>
<%@page import    = "com.tsp.finanzas.presupuesto.model.beans.*" %>
<%@include file   = "/WEB-INF/InitModel.jsp"%>
<% String Mensaje = (request.getParameter("Mensaje")!=null?request.getParameter("Mensaje"):""); %>
<html>
<head>
    <title>Definicion de Parametros de Vistas de Costos Operativos</title>
    <script src ="<%= BASEURL %>/js/boton.js"></script>
    <link   href="<%= BASEURL %>/css/estilostsp.css" rel='stylesheet'>    
    <script>
        function addOption(Comb,valor,texto){
            var Ele = document.createElement("OPTION");
            Ele.value=valor;
            Ele.text=texto;
            Comb.add(Ele);
        }    
        function Llenar(CmbAnno, CmbMes){
                var Meses    = new Array('Enero','Febrero','Marzo','Abril','Mayo','Junio','Julio','Agosto','Septiembre','Octubre','Noviembre','Diciembre');
                var FechaAct = new Date();
                CmbAnno.length = 0;
                CmbMes.length  = 0;                
                for (i=FechaAct.getYear()-10;i<=FechaAct.getYear()+10;i++) addOption(CmbAnno,i,i)
                CmbAnno.value = FechaAct.getYear();
                for (i=0;i<Meses.length;i++)
                    if ((i+1)<10) addOption(CmbMes,'0'+(i+1),Meses[i]);
                    else          addOption(CmbMes,(i+1),Meses[i]);                
                CmbMes.value  = ((FechaAct.getMonth()+1)<10)?('0'+(FechaAct.getMonth()+1)):(FechaAct.getMonth()+1);	
        }   
        function init (){
            Llenar(fpto.Ano,fpto.Mes); 
            for (i=0; i<12; i++)
               addOption(fpto.Prolongacion,(i+1),(i+1));
               
            addOption(fpto.Vista,'CT' , 'Costos Operativos Generales');    
            addOption(fpto.Vista,'AG' , 'Costos Operativos por Agencias'); 
            
        }
    </script>
</head>
<body>
<div id="capaSuperior" style="position:absolute; width:100%; height:100px; z-index:0; left: 0px; top: 0px;">
   <jsp:include page="/toptsp.jsp?encabezado=Vista Costos Operativos"/>
</div>
<div id="capaCentral" style="position:absolute; width:100%; height:87%; z-index:0; left: 0px; top: 100px; overflow: scroll;">
<center>
    <form action='<%= CONTROLLERPTO %>?ACCION=/Opciones/CostosOperativos&Opcion=InitVista' method='post' name='fpto' onsubmit='return _onsubmit(this);' >
    
    <table width="400" align="center" border='2'>
    <tr><td>
        <table width="100%" align="center" class="tablaInferior">
            <tr class="filaresaltada">
                <td colspan='2'>
                    <table width='100%' cellpadding='0' cellspacing='0'>
                    <tr>
                        <td class='subtitulo1'  width='75%' >&nbsp; PRESUPUESTO COSTOS OPERATIVOS</td>
                        <td class='barratitulo' ><img src="<%= BASEURL %>/images/titulo.gif" width="32" height="20"></td>
                    </tr>
                    </table>
                </td>
            </tr>
            
            <tr class="filaresaltada">
                <td colspan='2' >&nbsp;Defina el periodo Inicial</td>
            </tr>
            <tr class="filaresaltada">
                <td width='30%'>&nbsp;A�o</td>
                <td width='70%' align='left'><select class="select" style="width:40%" name="Ano"></select></td>
            </tr>
            <tr class="filaresaltada">
                <td >&nbsp;Mes</td>
                <td align='left'><select class="select" style="width:40%" name="Mes"></select></td>
            </tr>
            <tr class="filaresaltada">
                <td >&nbsp;Proyecci�n</td>
                <td align='left'><select class="select" style="width:40%" name="Prolongacion"></select>&nbsp;(meses)</td>
            </tr>
            <tr class="filaresaltada">
                <td >&nbsp;Tipo de Vista</td>
                <td align='left'><select class="select" style="width:80%" name="Vista"></select></td>
            </tr>
        </table>
        </td>
        </tr>
    </table>
    
    <br> 
    <img src='<%=BASEURL%>/images/botones/aceptar.gif' style='cursor:hand' onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" onclick='fpto.submit();'>
    &nbsp;
    <img src='<%=BASEURL%>/images/botones/salir.gif'  style='cursor:hand' onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" onclick='window.close();'>
    </form>


<% if (!Mensaje.equals("")) { %>
    
        <table border="2" align="center">
           <tr>
           <td><table width="100%"  border="1" align="center"  bordercolor="#F7F5F4" bgcolor="#FFFFFF">
                <tr>
                 <td width="320" align="center" class="mensajes"><%=Mensaje%></td>
                 <td width="29" background="<%=BASEURL%>/images/cuadronaranja.JPG">&nbsp;</td>
                 <td width="58">&nbsp;</td>
                </tr>
              </table>
           </td>
           </tr>
        </table>  
        
<% } %>

    </center>
</div>
</body>
</html>
<script>
    init();
</script>