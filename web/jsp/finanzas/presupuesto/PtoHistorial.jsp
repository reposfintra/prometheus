 <!--  
     - Author(s)       :      MARIO FONTALVO
     - Date            :      10/12/2005  
     - Copyright Notice:      TRANSPORTES SANCHEZ POLO S.A.
  -->
 <%--   
     - @(#)  
     - Description: Formulario visualizacion de Historial
 --%>

<%@page session="true"%> 
<%@page errorPage="/error/ErrorPage.jsp"%>
<%@page import="java.util.*" %>
<%@page import="com.tsp.finanzas.presupuesto.model.beans.*" %>
<%@page import="com.tsp.util.UtilFinanzas" %>
<%@include file="/WEB-INF/InitModel.jsp"%>
<% 
    List listaRP         = modelpto.ReprogamacionSvc.getListaReprogramaciones();
    List lista           = modelpto.DPtoSvc.getHistorial(); 
    String StdJobNo      = request.getParameter("Stdjob");
    String StdJobDesc    = request.getParameter("StdJobDesc");
    String NombreAgencia = request.getParameter("NombreAgencia");
    String NombreCliente = request.getParameter("NombreCliente");
    String Ruta          = request.getParameter("Ruta");
    String Ano           = request.getParameter("Ano");
    String Mes           = request.getParameter("Mes");
    
%>
<%! 
    public boolean mostrar(int dia, List lista){
       for (int i=0; i<lista.size(); i++)
           if (Integer.parseInt(((DatosGeneral) lista.get(i)).getValor( String.valueOf(dia-1))) !=0 ) 
               return true;
       return false;
    }
   public String obtenerModificaciones (String FechaViaje, String FechaCreacion, List ListaGeneral) {
       // definimos donde comienza y finaliza la lista
       int inicio = -1, fin = -1;
       // para el detener en el recorido donde comineza la lista
       //Reprogramacion objeto = null;
       String objeto = "";
       for (int i=0; i<ListaGeneral.size();i++ ){
           Reprogramacion rp = (Reprogramacion) ListaGeneral.get(i);
           if (rp.getFechaViaje().equals(FechaViaje) && rp.getFechaCreacion().equals(FechaCreacion)){
               objeto = rp.getDescripcion();
               break;
           }
       }
       return objeto;
   }
%>
<html>
<head>

 <title>Control de Cambios -  Presupuesto de Ventas</title>
 <link href="<%= BASEURL %>/css/estilostsp.css" rel="stylesheet" type="text/css"> 
 <script type='text/javascript' src="<%= BASEURL %>/js/boton.js"></script>

</head>
<body>

<center>


<div id="capaSuperior" style="position:absolute; width:100%; height:100px; z-index:0; left: 0px; top: 0px;">
<jsp:include page="/WEB-INF/toptsp.jsp?encabezado=Presupuesto de Venta"/>
</div>

<div id="capaCentral" style="position:absolute; width:100%; height:85%; z-index:0; left: 0px; top: 100px; overflow: scroll;"> 


<% if (lista!=null && lista.size()>0) { %>


<table width="400" border="2" align="center" bgcolor="#F7F5F4">

       <tr>
          <td>
        
                <table width="734" align="center" class='tablaInferior' >
                
                <tr>
                <td aling='center' colspan='6'>

                   <table cellpadding='0' cellspacing='0' width='100%'>
                     <tr class="fila">
                      <td width='65%' class="subtitulo1">&nbsp Control de Cambios</td>
                      <td width='35' class="barratitulo"><img src="<%=BASEURL%>/images/titulo.gif" width="32" height="20"></td>
                    </tr>
                   </table>
                </td>
                </tr>
                
              <tr class='fila' >
                <td colspan='2'>Estandar </td >
                <td colspan='4' class='letra'><%= StdJobNo  %> - <%= StdJobDesc %></td >
              </tr>
              <tr class='fila'>  
                <td colspan='2'>Agencia  </td >
                <td colspan='4'><%= NombreAgencia %></td >
              </tr>
              <tr class='fila'>  
                <td colspan='2'>Cliente  </td >
                <td colspan='4'><%= NombreCliente %></td >
              </tr>
              <tr class='fila'>  
                <td colspan='2'>Ruta     </td >
                <td colspan='4'><%= Ruta %></td>
              </tr>

                  <tr class="tblTitulo">
                    <th width="50"  rowspan="2" class='bordereporte' >DIA</th>
                    <th width="106" rowspan="2" class='bordereporte'>VIAJES ACTUALES</th>
                    <th colspan="4" class='bordereporte'> DATOS PREVIOS </th>
                  </tr>
                  <tr class="tblTitulo">
                    <th width="90"  class="bordereporte" nowrap >VIAJES</th>
                    <th width="153" class="bordereporte" nowrap >FECHA</th>
                    <th width="107" class="bordereporte" nowrap >MODIFICADO</th>
                    <th width="200" class="bordereporte" nowrap >CAUSA REPROGRAMACION</th>
                  </tr>
                  <% for (int dia = 1 ; dia <= 31; dia++) { 
                       String fecha = Ano + "-" + Mes + "-" + UtilFinanzas.DiaFormat(dia);
                       int valorActual = Integer.parseInt(((DatosGeneral) lista.get(0)).getValor( String.valueOf(dia-1)));  
                       if (mostrar(dia,lista)){   %>
                  <tr class='<%= (dia%2==0?"filagris":"filaazul") %>' style='font-size:12px'>
                      <td class='bordereporte' align='center'><%= UtilFinanzas.DiaFormat(dia) %></td>
                      <td class='bordereporte' align='center'><%= valorActual         %></td>
                      <td class='bordereporte' colspan='4' align='center'>
                          <table  width="100%"  align="center" cellpadding="1" cellspacing="1">
                          <% if (lista.size()==1) { %>
                             <tr class='<%= (dia%2==0?"filagris":"filaazul") %>' style='font-size:12px'>
                                <td align='center' colspan='4' class='bordereporte'>Sin Historia</td>
                             </tr>
                          <% } else{ 
                                   int    memVal = Integer.parseInt(((DatosGeneral) lista.get(1)).getValor( String.valueOf(dia-1)));
                                   String memFec = ((DatosGeneral) lista.get(1)).getValor("FECHA"  );
                                   String memUsu = ((DatosGeneral) lista.get(1)).getValor("USUARIO");
                                   boolean sw = false;
                                   for (int i = 1; i < lista.size(); i++) { 
                                       int nextVal = Integer.parseInt(((DatosGeneral) lista.get(i)).getValor( String.valueOf(dia-1)));
                                       if ((nextVal != memVal && sw) || (nextVal != memVal && valorActual != memVal)) { 
                                          sw=true;  %>
                                          <tr class='<%= (dia%2==0?"filagris":"filaazul") %>' style='font-size:12px'>
                                            <td align='center' width='90'  class='bordereporte'><%= memVal %></td>
                                            <td align='center' width='153' class='bordereporte'><%= memFec %></td>
                                            <td align='center' width='107' class='bordereporte'><%= memUsu %></td>
                                            <td align='center' width='200' class='bordereporte'><%= obtenerModificaciones(fecha, memFec, listaRP) %></td>                    
                                          </tr>
                                <%    } 
                                          memVal = Integer.parseInt(((DatosGeneral) lista.get(i)).getValor( String.valueOf(dia-1)));
                                          memFec = ((DatosGeneral) lista.get(i)).getValor("FECHA"  );
                                          memUsu = ((DatosGeneral) lista.get(i)).getValor("USUARIO");
                                  } %>
                           <% if (memVal!=0) {%>
                                  <tr class='<%= (dia%2==0?"filagris":"filaazul") %>' style='font-size:12px'>
                                    <td align='center' width='90'  class='bordereporte'><%= memVal %></td>
                                    <td align='center' width='153' class='bordereporte'><%= memFec %></td>
                                    <td align='center' width='107' class='bordereporte'><%= memUsu %></td>
                                    <td align='center' width='200' class='bordereporte' ><%= obtenerModificaciones(fecha, memFec, listaRP) %></td>        
                                  </tr>
                          <% }else if (!sw){ %>
                          <tr class='<%= (dia%2==0?"filagris":"filaazul") %>' style='font-size:12px'>
                            <td align='center' colspan='3' class='bordereporte'>Sin Historia</td>
                          </tr>
                      <% } } %>
                    </table>
                  </tr>
                  <% } } %>
                </table>

        </td>
       </tr>
    </table> 

<% } else { %>
<br><br>
<br>
<br>
      <table border="2" align="center">
              <tr>
                <td><table width="100%"  border="1" align="center"  bordercolor="#F7F5F4" bgcolor="#FFFFFF">
                  <tr>
                    <td width="320" align="center" class="mensajes">No hay historial para mostrar</td>
                    <td width="29" background="<%=BASEURL%>/images/cuadronaranja.JPG">&nbsp;</td>
                    <td width="58">&nbsp;</td>
                  </tr>
                </table></td>
              </tr>
      </table>
<% } %>
<br>
<br>

<img src="<%=BASEURL%>/images/botones/salir.gif"     name="imgsalir"    onClick=" parent.close();"              onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" style="cursor:hand">&nbsp;
   
</center>
</body>
</html>


