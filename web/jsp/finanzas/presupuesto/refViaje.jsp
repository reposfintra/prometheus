<!--
- Autor : Ing. Diogenes Bastidas Morales
- Date  : 05 de Septiembre de 2006
- Copyrigth Notice : Fintravalores S.A. S.A
-->
<%--
-@(#)
--Descripcion : Pagina JSP, lista de ingresos

--%>

<%-- Declaracion de librerias--%>
<%@page contentType="text/html"%>
<%@page session="true"%>
<%@page errorPage="/error/ErrorPage.jsp"%>
<%@page import="java.util.*"%> 
<%@page import="com.tsp.util.*"%>
<%@page import="com.tsp.operation.model.*" %>
<%@page import="com.tsp.operation.model.beans.*" %>
<%@page import="com.tsp.finanzas.presupuesto.model.beans.*" %>
<%@include file="/WEB-INF/InitModel.jsp"%>

 
<html>
<head>
<title>Registrar Documento Rerenciador</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link href="<%= BASEURL %>/css/estilostsp.css" rel="stylesheet" type="text/css">
<link href="file:///C|/Tomcat5/webapps/slt%20ING/css/estilostsp.css" rel="stylesheet" type="text/css"> 
<script type='text/javascript' src="<%= BASEURL %>/js/reporte.js"></script>
<script type='text/javascript' src="<%= BASEURL %>/js/boton.js"></script>
<script type='text/javascript' src="<%= BASEURL %>/js/finanzas/presupuesto/dtpPto.js"></script>

</head>
<body onLoad="redimensionar();" onresize="redimensionar()">
<div id="capaSuperior" style="position:absolute; width:100%; height:100px; z-index:0; left: 0px; top: 0px;">
	<jsp:include page="/WEB-INF/toptsp.jsp?encabezado=Registrar Documento Rerenciador"/>
</div>
<div id="capaCentral" style="position:absolute; width:100%; height:87%; z-index:0; left: 0px; top: 100px; overflow: scroll;">

  <% 
    String tipo_doc = "", doc="";
    int viajes = Integer.parseInt(request.getParameter("viajes"));
	Vector vecref = modelpto.DPtoSvc.getVecRef_Viaje();
    int nroref = vecref.size();
	LinkedList tblres = model.tablaGenService.getTipoDocumentos();
	int items = (nroref > viajes)? nroref:viajes;

if(nroref > viajes){%>
<table width="38%"  align="center">
	<tr><td>
		<FIELDSET>
		<legend><span class="letraresaltada">Nota</span></legend>
		<table width="100%"  border="0" cellpadding="0" cellspacing="0" class="informacion">
			<tr>
			  <td nowrap>&nbsp;Existen <%=nroref%> Viajes Referenciados. Por favor borre <%=nroref-viajes%> documentos para que referencie los <%=viajes%> viajes </td>
			</tr>
		</table>
	</FIELDSET>
	</td></tr>
</table>
<%}%>
<br>
<form name="forma" method="post" action="<%= CONTROLLERPTO %>?ACCION=/Ref_viaje/Evento&opcion=Insertar">
  <table width="500" border="2" align="center">
    <tr>
      <td>
        <table width="100%" align="center">
          <tr>
            <td width="373" class="subtitulo1">&nbsp;Referenciar Viaje</td>
            <td width="427" class="barratitulo"><img src="<%=BASEURL%>/images/titulo.gif" width="32" height="20">
              <input name="Stdjob" type="hidden" id="Stdjob" value="<%=request.getParameter("Stdjob")%>">
              <input name="Ano" type="hidden" id="Ano" value="<%=request.getParameter("Ano")%>">
              <input name="Mes" type="hidden" id="Mes" value="<%=request.getParameter("Mes")%>">
              <input name="Dia" type="hidden" id="Dia" value="<%=request.getParameter("Dia")%>">
              <input name="Viajes" type="hidden" id="Viajes" value="<%=items%>">
              <input name="nrovj" type="hidden" id="nrovj" value="<%=viajes%>"></td>
          </tr>
        </table>
        <table width="100%" border="1" align="center" bordercolor="#999999" bgcolor="#F7F5F4">
          <tr class="tblTitulo">
            <td width="12%" align="center">Viaje</td>
            <td width="51%"  align="center">Tipo Documento </td>
            <td width="37%" align="center">Documennto</td>
          </tr>
          <% 
			  for (int i = 0; i <items; i++)	  {
				if( i < nroref ){
					Ref_viaje ref = (Ref_viaje) vecref.get(i);
					tipo_doc = ref.getTipo_doc();
					doc = ref.getDocumento();
				}else{
					tipo_doc = "";
					doc ="";
				}%>
    
          <tr class="<%=(i % 2 == 0 )?"filagris":"filaazul"%>">
            <td width="12%" class="bordereporte" align="left"><%=i+1%></td>
            <td width="51%" class="bordereporte"><select name="tipodoc<%=i%>" class="textbox" id="tipodoc<%=i%>" style="width:90%" >
              <% for(int j = 0; j<tblres.size(); j++){
                       TablaGen respa = (TablaGen) tblres.get(j); %>
              <option value="<%=respa.getTable_code()%>" <%=(respa.getTable_code().equals(tipo_doc))?"selected":""%> ><%=respa.getDescripcion()%></option>
              <%}%>
            </select></td>
            <td width="37%" class="bordereporte"><input name="doc<%=i%>" type="text" id="doc<%=i%>" value="<%=doc%>">
              <input name="nrodoc<%=i%>" type="hidden" id="nrodoc" value="<%=doc%>"></td>
          </tr>
          <%}%>
      </table></td>
    </tr>
  </table>
<br>
  <table width="500" align="center" border="0">
    <tr align="center">
      <td><img src="<%=BASEURL%>/images/botones/aceptar.gif" name="aceptar" style="cursor:hand"   onClick="validarRef();" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);">&nbsp;
			<img src="<%=BASEURL%>/images/botones/salir.gif" name="salir" style="cursor:hand"   onClick="parent.close();" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);"></td>
    </tr>
  </table>

<br>
<%if(request.getParameter("men")!=null){%>
            <br>
            <table border="2" align="center">
              <tr>
                <td><table width="100%"  border="1" align="center"  bordercolor="#F7F5F4" bgcolor="#FFFFFF">
                    <tr>
                      <td width="229" align="center" class="mensajes"><%=request.getParameter("men")%></td>
                      <td width="29" background="<%=BASEURL%>/images/cuadronaranja.JPG">&nbsp;</td>
                      <td width="58">&nbsp;</td>
                    </tr>
                </table></td>
              </tr>
            </table>
    <%  }%>	
</form>
<br>
</div>
</body>

</html>
