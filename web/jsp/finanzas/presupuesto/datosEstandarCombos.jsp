/*
<!--  
     - Author(s)       :      MARIO FONTALVO
     - Date            :      10/12/2005  
     - Copyright Notice:      TRANSPORTES SANCHEZ POLO S.A.
-->
<%--   
     - @(#)  
     - Description: Permite estandarizar campos
--%> 
*/
 

<%!
    public String convertir(String cadena){
        return (cadena!=null?cadena.replaceAll("'", ""):"");
    }
%>

<%= modelpto.DPtoSvc.getVarJSSeparador() %>
<%
    String SEPARADOR = "~";
    List ListaAgenciaClienteStandar = modelpto.DPtoSvc.getListadoAgenciasClientesStandar();
    out.print(" var datos = [ ");
    if (ListaAgenciaClienteStandar!=null){
        for (int i=0; i<ListaAgenciaClienteStandar.size(); i++){
            DatosEstandar dt = (DatosEstandar) ListaAgenciaClienteStandar.get(i);
            String item = "\n['"+ dt.getCodigoAgencia()  +SEPARADOR+ convertir(dt.getDescripcionAgencia())  + SEPARADOR +
            dt.getCodigoCliente()   + SEPARADOR + convertir(dt.getDescripcionCliente())   + SEPARADOR +
            dt.getCodigoEstandar()  + SEPARADOR + convertir(dt.getDescripcionEstandar())  + SEPARADOR +
            dt.getCodigoOrigen()    + SEPARADOR + convertir(dt.getDescripcionOrigen())    + SEPARADOR +
            dt.getCodigoDestino()   + SEPARADOR + convertir(dt.getDescripcionDestino())   +  "']";
            out.print (item);
            if ((i+1)!=ListaAgenciaClienteStandar.size()) out.print(",");
        }
    }
    out.print("];");
%>
