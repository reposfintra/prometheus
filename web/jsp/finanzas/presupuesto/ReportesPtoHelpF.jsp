<!--  
     - Author(s)       :      MARIO FONTALVO
     - Date            :      25/01/2006
     - Copyright Notice:      TRANSPORTES SANCHEZ POLO S.A.
-->
<%--   
     - @(#)  
     - Description: Reportes Viajes Presupuestado vs. Ejecutado
--%> 
<%@include file="/WEB-INF/InitModel.jsp"%>
<html>
<head>
    <title>GENERACION REPORTE VIAJES PROGRAMADOS VS PRESUPUESTADO</title>
<link href="../../../css/estilostsp.css" rel="stylesheet" type="text/css">
<link href="<%= BASEURL %>/css/estilostsp.css" rel="stylesheet" type="text/css">
</head>
<body>
<BR>
<table width="696"  border="2" align="center">
  <tr>
    <td width="811" >
      <table width="100%" border="0" align="center">
        <tr  class="subtitulo">
          <td width="674" height="24"><div align="center">REPORTE VIAJES PRESUPUESTADOS VS EJECUTADO.</div></td>
        </tr>
		<tr class="subtitulo1">
          <td> PARAMETROS DE GENERACION DE REPORTE</td>
        </tr>
		<tr class="ayudaHtmlTexto">
		<td>Formulario de Generacion de Reporte<br>
		    <div align="center"><img src="<%= BASEURL %>/images/ayuda/ReportePTO/dibujo01.JPG" align="absmiddle"></div>
                    <br>	
        	    Parametros generales para la generacion de el reporte de Viajes Presupuestados vs. Viajes Ejecutados.<br>												
        	    
        	    <br>
        	    <B>SALIDAS (TIPO DE REPORTES GENERADOS).</B><BR><BR>
        	    Estos reportes muestran la distribucion de los viajes presupuestados vs. los ejecutadados de un periodo dado.
        	    A la vez muestra el porcentaje de cumplimiento de estos viajes.
        	    
        	    
        	    REPORTE POR AGENCIA.<BR>
        	    <div align="center"><img src="<%= BASEURL %>/images/ayuda/ReportePTO/dibujo02.JPG" align="absmiddle"></div>
        	    
        	    REPORTE POR CLIENTE.<BR>
        	    <div align="center"><img src="<%= BASEURL %>/images/ayuda/ReportePTO/dibujo03.JPG" align="absmiddle"></div>
        	    
        	    REPORTE POR ESTANDAR.<BR>
        	    Reporte de ultimo nivel, es no profundizan en ma niveles como los otros reportes, y muestra la misma informacion de los 
        	    demas a nivel de estandar.
        	    
		  </td>
		</tr>
      </table>
    </td>
  </tr>
</table>
</body>
</html>
