<!--
- Autor : Ing. Tito Andr�s Maturana
- Date : 7 de febrero de 2006
- Copyrigth Notice : Fintravalores S.A. S.A
-->
<%--
-@(#)
--Descripcion : Pagina JSP, que sirve para ingresar en el archivo de presupuesto de gastos administartivos.
--%>

<%@page contentType="text/html"%>
<%@page session="true"%>
<%@page import="java.util.*"%>
<%@page import="com.tsp.operation.model.Model"%>
<%@page import="com.tsp.operation.model.beans.Usuario"%>
<%@page import="com.tsp.operation.controller.*"%>
<%@page import="com.tsp.util.*"%>
<%@include file="/WEB-INF/InitModel.jsp"%>
<%@ taglib uri="http://jakarta.apache.org/taglibs/input-0.90" prefix="input" %>
<%@page errorPage="/error/ErrorPage.jsp"%>
<html>
<head>
<title>Presupuesto Gastos Administrativos - Buscar</title>
<link href="<%= BASEURL %>/css/estilostsp.css" rel='stylesheet'>
<link href="../../../../css/estilostsp.css" rel="stylesheet" type="text/css">
<link href="/css/estilostsp.css" rel="stylesheet" type="text/css">
</head>
<script type='text/javascript' src="<%= BASEURL %>/js/validar.js"></script>
<script src='<%= BASEURL %>/js/date-picker.js'></script>
<script type='text/javascript' src="<%= BASEURL %>/js/boton.js"></script>
<script type='text/javascript' src="<%= BASEURL %>/js/funciones.js"></script>
<script>
	function cargar(){
		addOption(forma.ano, '', 'Todos');
		Llenar(forma.ano,null);	
	}	
</script>
<body onresize="redimensionar()" onload = 'redimensionar(); cargar();'>
<div id="capaSuperior" style="position:absolute; width:100%; height:100px; z-index:0; left: 0px; top: 0px;">
 <jsp:include page="/toptsp.jsp?encabezado=Presupuesto Gastos Administrativos - Buscar"/>
</div>
<div id="capaCentral" style="position:absolute; width:100%; height:87%; z-index:0; left: 0px; top: 100px; overflow: scroll;">
<%
	TreeMap cuenta = model.tblgensvc.getClaseCuenta();
  TreeMap unidad = model.tblgensvc.getClaseUnidadProyecto();
  TreeMap elemento = model.tblgensvc.getClaseElementoGasto();
  TreeMap area = model.tblgensvc.getClaseArea();
  TreeMap agencia = model.tblgensvc.getClaseAgencia();  
  TreeMap distritos = new TreeMap();
  TreeMap vlrs = new TreeMap();
  
  distritos.put(" Todos", "");
  distritos.put("COLOMBIA", "COL");
  distritos.put("TSP ECUADOR", "ECU");
  distritos.put("FINV", "FINV");
  distritos.put("VENEZUELA", "VEN");
  
  vlrs.put(" Todos ", "t"); /* Todos los Valores */
  vlrs.put("Ejecutados", "e");/* Valores Presupuestados */
  vlrs.put("Presupuestados", "p");/* Valores Ejecutados */
  
  unidad.remove(" Seleccione un Item");
  elemento.remove(" Seleccione un Item");
  area.remove(" Seleccione un Item");
  agencia.remove(" Seleccione un Item");
	cuenta.remove(" Seleccione un Item");
  
  unidad.put(" Todos", "");
  elemento.put(" Todos", "");
  area.put(" Todos", "");
  agencia.put(" Todos", "");
  cuenta.put(" Todas", "");
  
%>
<form name="forma" id="forma" method="post" action="<%= CONTROLLERPTO %>?ACCION=/PtoGastosAdmin/Search">
  <table width="356"  border="2" align="center" cellpadding="0" cellspacing="0">
    <tr>
      <td width="350"><table width="100%" class="tablaInferior">
        <tr>
          <td colspan="2" ><table width="100%"  border="0" cellpadding="0" cellspacing="1" class="barratitulo">
              <tr>
                <td width="50%" class="subtitulo1">&nbsp;Gastos Administrativos</td>
                <td width="50%" class="barratitulo"><img src="<%=BASEURL%>/images/titulo.gif" width="32" height="20"></td>
              </tr>
          </table></td>
        </tr>
		<tr class="fila">
		  <td> Cuenta </td>
		  <td ><input:select name="clase" attributesText="class=textbox" options="<%= cuenta %>" default=""/></td>
		  </tr>
		<tr class="fila">
          <td width="47%">Agencia </td>      
          <td width="53%" ><input:select name="agencia" attributesText="class=textbox" options="<%= agencia %>" default="NADA"/></td>
		  </tr>
		  <tr class="fila">
          <td>Unidad de Negocios</td>      
          <td ><input:select name="unidad" attributesText="class=textbox" options="<%= unidad %>" default=""/></td>
		  </tr>
		   <tr class="fila">
		     <td>Area </td>
		     <td ><input:select name="area" attributesText="class=textbox" options="<%= area %>" default=""/></td>
	        </tr>
		   <tr class="fila">
          <td>Elemento del gasto</td>      
          <td ><input:select name="elemento" attributesText="class=textbox" options="<%= elemento %>" default=""/></td>
		  </tr>
		   <tr class="fila">
		     <td >A&ntilde;o</td>
		     <td><span class="filaresaltada">
		       <select name="ano" class="select" id="ano" style="width:40%">
	            </select>
		     </span></td>
	        </tr>
		   <tr class="fila">
		     <td >Distrito</td>
		     <td><input:select name="distrito" attributesText="class=textbox" options="<%= distritos %>" default=""/></td>
	        </tr>
			<tr class="fila">
		     <td >Valores</td>
		     <td><input:select name="valores" attributesText="class=textbox" options="<%= vlrs %>" default=""/></td>
	        </tr>
      </table></td>
    </tr>
  </table>
  <br>
  <% if( request.getParameter("msg")!=null ){%>
  <p>
  <table border="2" align="center">
    <tr>
      <td><table width="100%"  border="1" align="center"  bordercolor="#F7F5F4" bgcolor="#FFFFFF">
          <tr>
            <td width="229" align="center" class="mensajes"><%=request.getParameter("msg").toString()%></td>
            <td width="29" background="<%=BASEURL%>/images/cuadronaranja.JPG">&nbsp;</td>
            <td width="58">&nbsp;</td>
          </tr>
      </table></td>
    </tr>
  </table>
  <p></p>
  <%} %>
  <br>
  <center>
    <img src="<%=BASEURL%>/images/botones/buscar.gif" name="c_aceptar" onClick="forma.submit();" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);"> 
	<img src="<%=BASEURL%>/images/botones/detalles.gif" name="c_cancelar" onClick="forma.action = forma.action + '&detalles=OK'; forma.submit();" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);"> 
	<img src="<%=BASEURL%>/images/botones/salir.gif" name="c_salir" onClick="parent.close();" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);">
  </center>
</form>
</div>
</body>
</html>

