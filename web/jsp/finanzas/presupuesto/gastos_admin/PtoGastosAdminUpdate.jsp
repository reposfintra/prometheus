<!--
- Autor : Ing. Tito Andr�s Maturana
- Date : 7 de febrero de 2006
- Copyrigth Notice : Fintravalores S.A. S.A
-->
<%--
-@(#)
--Descripcion : Pagina JSP, que sirve para actualizar un archivo en el archivo de presupuesto de gastos administartivos.
--%>

<%@page contentType="text/html"%>
<%@page session="true"%>
<%@page import="java.util.*"%>
<%@page import="com.tsp.operation.model.Model"%>
<%@page import="com.tsp.operation.model.beans.Usuario"%>
<%@page import="com.tsp.operation.controller.*"%>
<%@page import="com.tsp.finanzas.presupuesto.model.beans.PtoGastosAdmin"%>
<%@page import="com.tsp.util.*"%>
<%@include file="/WEB-INF/InitModel.jsp"%>
<%@ taglib uri="http://jakarta.apache.org/taglibs/input-0.90" prefix="input" %>
<%@page errorPage="/error/ErrorPage.jsp"%>
<html>
<head>
<title>Presupuesto Gastos Administrativos - Modificar</title>
<link href="<%= BASEURL %>/css/estilostsp.css" rel='stylesheet'>
<link href="../../../../css/estilostsp.css" rel="stylesheet" type="text/css">
<link href="/css/estilostsp.css" rel="stylesheet" type="text/css">
</head>
<script type='text/javascript' src="<%= BASEURL %>/js/validar.js"></script>
<script src='<%= BASEURL %>/js/date-picker.js'></script>
<script type='text/javascript' src="<%= BASEURL %>/js/boton.js"></script>
<script type='text/javascript' src="<%= BASEURL %>/js/funciones.js"></script>
<%
  PtoGastosAdmin p = (PtoGastosAdmin) request.getAttribute("pto");

  double[] vlr = p.getVlr_presupuestado();
  
%>
<body onresize="redimensionar()" onload = "redimensionar();">
<div id="capaSuperior" style="position:absolute; width:100%; height:100px; z-index:0; left: 0px; top: 0px;">  
 <jsp:include page="/toptsp.jsp?encabezado=Presupuesto Gastos Administrativos - Modificar"/>
</div>
<div id="capaCentral" style="position:absolute; width:100%; height:87%; z-index:0; left: 0px; top: 100px; overflow: scroll;">
<form name="forma" id="forma" method="post" action="<%= CONTROLLERPTO %>?ACCION=/PtoGastosAdmin/Update">
  <table width="298"  border="2" align="center" cellpadding="0" cellspacing="0">
    <tr>
      <td width="325"><table width="100%" class="tablaInferior">
        <tr>
          <td colspan="2" ><table width="100%"  border="0" cellpadding="0" cellspacing="1" class="barratitulo">
              <tr>
                <td width="50%" class="subtitulo1">&nbsp;Gastos Administrativos</td>
                <td width="50%" class="barratitulo"><img src="<%=BASEURL%>/images/titulo.gif" width="32" height="20"></td>
              </tr>
          </table></td>
        </tr>
		<tr class="fila">
		  <td>Cuenta</td>
		  <td ><%= p.getTipo_cuenta()%>
		    <input name="cuenta" type="hidden" id="cuenta" value="<%= p.getCuenta()%>"></td>
		  </tr>
		<tr class="fila">
          <td width="49%">Agencia </td>      
          <td width="51%" ><%= p.getAgencia()%>
            </td>
		</tr>
		  <tr class="fila">
          <td>Unidad de Negocios</td>      
          <td ><%= p.getUnidad()%>            </td>
		  </tr>
		   <tr class="fila">
		     <td>Area </td>
		     <td ><%= p.getArea()%>	          </td>
	        </tr>
		   <tr class="fila">
          <td>Elemento del Gasto</td>      
          <td ><%= p.getElemento()%>            </td>
		  </tr>
		   <tr class="fila">
		     <td >A&ntilde;o</td>
		     <td><input name="ano" type="hidden" id="ano" value="<%= p.getAno() %>">
               <%= p.getAno()%></td>
	        </tr>
		   <tr class="fila">
		     <td >Distrito</td>
		     <td><%= p.getNom_distrito()%>
	          <input name="distrito" type="hidden" id="distrito" value="<%= p.getDstrct() %>"></td>
	        </tr>
		   <tr class="fila">
		     <td >Enero</td>
		     <td><div align="right">
		         <input name="vlr_pto" type="text" class="textbox" id="vlr_pto" style="text-align:right" onFocus="this.value = this.value.replace(/,/g,'');"  onChange="formatear(this);" onKeyPress="soloDigitos(event,'decNo')" onBlur="formatear(this);" size="20" maxlength="12" value="<%= com.tsp.util.UtilFinanzas.customFormat(vlr[0]) %>">
	          </div></td>
	        </tr>
		   <tr class="fila">
		     <td >Febrero</td>
		     <td>
		       <div align="right">
		         <input name="vlr_pto" type="text" class="textbox" id="vlr_pto" style="text-align:right" onFocus="this.value = this.value.replace(/,/g,'');"  onChange="formatear(this);" onKeyPress="soloDigitos(event,'decNo')" onBlur="formatear(this);" size="20" maxlength="12" value="<%= com.tsp.util.UtilFinanzas.customFormat(vlr[1]) %>">
                </div></td>
	        </tr>
		   <tr class="fila">
		     <td > Marzo </td>
		     <td>
		       <div align="right">
		         <input name="vlr_pto" type="text" class="textbox" id="vlr_pto" style="text-align:right" onFocus="this.value = this.value.replace(/,/g,'');" onChange="formatear(this);" onKeyPress="soloDigitos(event,'decNo')" onBlur="formatear(this);" size="20" maxlength="12" value="<%= com.tsp.util.UtilFinanzas.customFormat(vlr[2]) %>">
                </div></td>
	        </tr>
		   <tr class="fila">
		     <td >Abril</td>
		     <td>
		       <div align="right">
		         <input name="vlr_pto" type="text" class="textbox" id="vlr_pto" style="text-align:right" onFocus="this.value = this.value.replace(/,/g,'');" onChange="formatear(this);" onKeyPress="soloDigitos(event,'decNo')" onBlur="formatear(this);" size="20" maxlength="12" value="<%= com.tsp.util.UtilFinanzas.customFormat(vlr[3]) %>">
                </div></td>
	        </tr>
		   <tr class="fila">
		     <td >Mayo</td>
		     <td>
		       <div align="right">
		         <input name="vlr_pto" type="text" class="textbox" id="vlr_pto" style="text-align:right" onFocus="this.value = this.value.replace(/,/g,'');" onChange="formatear(this);" onKeyPress="soloDigitos(event,'decNo')" onBlur="formatear(this);" size="20" maxlength="12" value="<%= com.tsp.util.UtilFinanzas.customFormat(vlr[4] )%>">
                </div></td>
	        </tr>
		   <tr class="fila">
		     <td >Junio</td>
		     <td>
		       <div align="right">
		         <input name="vlr_pto" type="text" class="textbox" id="vlr_pto" style="text-align:right" onFocus="this.value = this.value.replace(/,/g,'');" onChange="formatear(this);" onKeyPress="soloDigitos(event,'decNo')" onBlur="formatear(this);" size="20" maxlength="12" value="<%= com.tsp.util.UtilFinanzas.customFormat(vlr[5]) %>">
                </div></td>
	        </tr>
		   <tr class="fila">
		     <td >Julio</td>
		     <td>
		       <div align="right">
		         <input name="vlr_pto" type="text" class="textbox" id="vlr_pto" style="text-align:right" onFocus="this.value = this.value.replace(/,/g,'');" onChange="formatear(this);" onKeyPress="soloDigitos(event,'decNo')" onBlur="formatear(this);" size="20" maxlength="12" value="<%= com.tsp.util.UtilFinanzas.customFormat(vlr[6]) %>">
                </div></td>
	        </tr>
		   <tr class="fila">
		     <td >Agosto</td>
		     <td>
		       <div align="right">
		         <input name="vlr_pto" type="text" class="textbox" id="vlr_pto" style="text-align:right" onFocus="this.value = this.value.replace(/,/g,'');" onChange="formatear(this);" onKeyPress="soloDigitos(event,'decNo')" onBlur="formatear(this);" size="20" maxlength="12" value="<%= com.tsp.util.UtilFinanzas.customFormat(vlr[7]) %>">
                </div></td>
	        </tr>
		   <tr class="fila">
		     <td >Septiembre</td>
		     <td>
		       <div align="right">
		         <input name="vlr_pto" type="text" class="textbox" id="vlr_pto" style="text-align:right" onFocus="this.value = this.value.replace(/,/g,'');" onChange="formatear(this);" onKeyPress="soloDigitos(event,'decNo')" onBlur="formatear(this);" size="20" maxlength="12" value="<%= com.tsp.util.UtilFinanzas.customFormat(vlr[8] )%>">
                </div></td>
	        </tr>
		   <tr class="fila">
		     <td >Octubre</td>
		     <td>
		       <div align="right">
		         <input name="vlr_pto" type="text" class="textbox" id="vlr_pto" style="text-align:right" onFocus="this.value = this.value.replace(/,/g,'');" onChange="formatear(this);" onKeyPress="soloDigitos(event,'decNo')" onBlur="formatear(this);" size="20" maxlength="12" value="<%= com.tsp.util.UtilFinanzas.customFormat(vlr[9]) %>">
                </div></td>
	        </tr>
		   <tr class="fila">
		     <td >Noviembre</td>
		     <td>
		       <div align="right">
		         <input name="vlr_pto" type="text" class="textbox" id="vlr_pto" style="text-align:right" onFocus="this.value = this.value.replace(/,/g,'');" onChange="formatear(this);" onKeyPress="soloDigitos(event,'decNo')" onBlur="formatear(this);" size="20" maxlength="12" value="<%= com.tsp.util.UtilFinanzas.customFormat(vlr[10]) %>">
                </div></td>
	        </tr>
		   <tr class="fila">
		     <td >Diciembre</td>
		     <td>
		       <div align="right">
		         <input name="vlr_pto" type="text" class="textbox" id="vlr_pto" style="text-align:right" onFocus="this.value = this.value.replace(/,/g,'');" onChange="formatear(this);" onKeyPress="soloDigitos(event,'decNo')" onBlur="formatear(this);" size="20" maxlength="12" value="<%= com.tsp.util.UtilFinanzas.customFormat(vlr[11]) %>">
                </div></td>
	        </tr>

      </table></td>
    </tr>
  </table>
  <br>
  <% if( request.getParameter("msg")!=null ){%>
  <p>
  <table border="2" align="center">
    <tr>
      <td><table width="100%"  border="1" align="center"  bordercolor="#F7F5F4" bgcolor="#FFFFFF">
          <tr>
            <td width="229" align="center" class="mensajes"><%=request.getParameter("msg").toString()%></td>
            <td width="29" background="<%=BASEURL%>/images/cuadronaranja.JPG">&nbsp;</td>
            <td width="58">&nbsp;</td>
          </tr>
      </table></td>
    </tr>
  </table>
  <p></p>
  <%} %>
  <br>
  <center>
    <img src="<%=BASEURL%>/images/botones/modificar.gif" name="c_aceptar" onClick="if (validarTCamposLlenos()) forma.submit();" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);"> 
	<img src="<%=BASEURL%>/images/botones/anular.gif" name="c_cancelar" onClick="forma.action = forma.action + '&anular=OK'; forma.submit();" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);"> 
	<img src="<%=BASEURL%>/images/botones/salir.gif" name="c_salir" onClick="parent.close();" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);">
  </center>
</form>
</div>
</body>
</html>

