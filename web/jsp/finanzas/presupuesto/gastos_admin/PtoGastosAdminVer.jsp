<!--
- Autor : Ing. Tito Andrés Maturana
- Date : 08.02.2006
- Copyrigth Notice : Fintravalores S.A. S.A
-->
<%--
-@(#)
--Descripcion : Pagina JSP, que sirve para consultar el presupuesto de gastos de administración.
--%>
<%@page contentType="text/html"%>
<%@page session="true"%>
<%@page errorPage="/error/ErrorPage.jsp"%>
<%@page import="java.util.*"%>
<%@page import="com.tsp.operation.model.Model"%>
<%@page import="com.tsp.finanzas.presupuesto.model.beans.PtoGastosAdmin"%>
<%@page import="com.tsp.util.*"%>
<%@include file="/WEB-INF/InitModel.jsp"%>
<%@taglib uri="http://jsptags.com/tags/navigation/pager" prefix="pg" %>
<html>
<head>
<title>Presupuesto Gastos Administrativos - Consultar</title>
<script type='text/javascript' src="<%= BASEURL %>/js/validar.js"></script>
<script SRC='<%=BASEURL%>/js/boton.js'></script>
 <link href="../../../css/estilostsp.css" rel='stylesheet' type="text/css">
 <script type='text/javascript' src="<%= BASEURL %>/js/reporte.js"></script>
<script type='text/javascript' src="<%= BASEURL %>/js/boton.js"></script>
<script type='text/javascript' src="<%= BASEURL %>/js/funciones.js"></script>
<link href="../../../../css/estilostsp.css" rel="stylesheet" type="text/css">
</head>
<body  onresize="redimensionar()" onload = 'redimensionar()'>
<div id="capaSuperior" style="position:absolute; width:100%; height:100px; z-index:0; left: 0px; top: 0px;">
 <jsp:include page="/toptsp.jsp?encabezado=Presupuesto Gastos Administrativos - Consultar"/>
</div>
<div id="capaCentral" style="position:absolute; width:100%; height:87%; z-index:0; left: 0px; top: 100px; overflow: scroll;">
<% 	
	PtoGastosAdmin p = (PtoGastosAdmin) request.getAttribute("pto");
	double[] vlr_pto = p.getVlr_presupuestado();
	double[] vlr_ejec = p.getVlr_ejecutado();
%><table width="100%" border="2" align="center">    
    <td class="fila">
	<table width="100%">
        <tr>
          <td width="48%" class="subtitulo1">Detalles de la Cuenta</td>
          <td width="52%"  class="barratitulo"><img src="<%=BASEURL%>/images/titulo.gif" width="32" height="20"></td>
        </tr>
      </table>
	  <table class="fila">
	  	<tr>
			<td width="5%">Agencia</td>
			<td width="11%"><%= p.getAgencia()%></td>
		    <td width="6%">Unidad</td>
		    <td width="10%"><%= p.getUnidad()%></td>
		    <td width="4%">Area</td>
		    <td width="17%"><%= p.getArea()%></td>
	  	    <td width="6%">Elemento</td>
	  	    <td width="10%"><%= p.getElemento()%></td>
	  	    <td width="6%">Distrito</td>
	  	    <td width="9%"><%= p.getNom_distrito()%></td>
	  	    <td width="6%">Per&iacute;odo</td>
	  	    <td width="10%"><script>format('<%= p.getPeriodo()%>')</script></td>
	  	</tr>		
	  </table>
	 <table width="100%">
        <tr>
          <td width="48%" class="subtitulo1">Presupuesto de Gastos Administrativos</td>
          <td width="52%" class="barratitulo"><img src="<%=BASEURL%>/images/titulo.gif" width="32" height="20"></td>
        </tr>
      </table>
	  <table>
        <tr class="fila">
<%
	if( request.getParameter("vlr").compareTo("t")==0 ){
		for( int i=0; i<12; i++){
%>		
          <td nowrap><table width="240"  border="1" cellpadding="4" cellspacing="1" bordercolor="#999999" bgcolor="#F7F5F4" align="center">
            <tr  class="tblTitulo">
              <td colspan="2"><div align="center">Valor <%= (i + 1)%></div></td>
              </tr>
            <tr  class="tblTitulo" >
              <td width="120">Presupuestado</td>
              <td width="120">Ejecutado</td>
            </tr></table>
		<table  class="letra">
            <tr>
              <td width="120" class="bordereporte"><div align="right"><%= com.tsp.util.UtilFinanzas.customFormat(vlr_pto[i]) %></div></td>
              <td width="120" class="bordereporte"><div align="right"><%= com.tsp.util.UtilFinanzas.customFormat(vlr_ejec[i]) %></div></td>
            </tr>
          </table></td>
<%	
		}
	} else { 
		for( int i=0; i<12; i++){
%>
		<td nowrap><table width="120"  border="1" cellpadding="4" cellspacing="1" bordercolor="#999999" bgcolor="#F7F5F4" align="center">
            <tr  class="tblTitulo">
              <td colspan="2"><div align="center">Valor <%= (i + 1)%></div></td>
              </tr>
            <tr  class="tblTitulo" >
              <td width="120"><%= request.getParameter("vlr").compareTo("p")==0 ? "Presupuestado" : "Ejecutado" %></td>
            </tr></table>
		<table  class="letra">
            <tr>
              <td width="120"  class="bordereporte"><div align="right"><%= request.getParameter("vlr").compareTo("p")==0 ? 
			  com.tsp.util.UtilFinanzas.customFormat(vlr_pto[i]) :  com.tsp.util.UtilFinanzas.customFormat(vlr_ejec[i]) %></div></td>
            </tr>
          </table></td>
<%
		}
	}		
%>			  
          </tr>
      </table>
  </table>

<p>
<div align="center">
        <img src="<%=BASEURL%>/images/botones/regresar.gif" name="c_cancelar" onClick="history.back();" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);"> 
		<img src="<%=BASEURL%>/images/botones/salir.gif" name="c_salir" id="c_salir" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" onClick="parent.close();">
</div>
</div>
</body>
</html>
