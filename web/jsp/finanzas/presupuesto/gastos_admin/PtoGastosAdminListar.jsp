<!--
- Autor : Ing. Tito Andrés Maturana
- Date : 08.02.2006
- Copyrigth Notice : Fintravalores S.A. S.A
-->
<%--
-@(#)
--Descripcion : Pagina JSP, que sirve para listar el presupuesto de gastos de administración.
--%>
<%@page contentType="text/html"%>
<%@page session="true"%>
<%@page errorPage="/error/ErrorPage.jsp"%>
<%@page import="java.util.*"%>
<%@page import="com.tsp.operation.model.Model"%>
<%@page import="com.tsp.finanzas.presupuesto.model.beans.PtoGastosAdmin"%>
<%@page import="com.tsp.util.UtilFinanzas"%>
<%@page import="com.tsp.util.*"%>
<%@include file="/WEB-INF/InitModel.jsp"%>
<%@taglib uri="http://jsptags.com/tags/navigation/pager" prefix="pg" %>
<html>
<head>
<title>Presupuesto Gastos Administrativos - Listar</title>
<script type='text/javascript' src="<%= BASEURL %>/js/validar.js"></script>
<script SRC='<%=BASEURL%>/js/boton.js'></script>
 <link href="../../../css/estilostsp.css" rel='stylesheet' type="text/css">
 <script type='text/javascript' src="<%= BASEURL %>/js/reporte.js"></script>
<script type='text/javascript' src="<%= BASEURL %>/js/boton.js"></script>
<script type='text/javascript' src="<%= BASEURL %>/js/funciones.js"></script>
<link href="../../../../css/estilostsp.css" rel="stylesheet" type="text/css">
</head>
<body  onresize="redimensionar()" onload = 'redimensionar()'>
<div id="capaSuperior" style="position:absolute; width:100%; height:100px; z-index:0; left: 0px; top: 0px;">
 <jsp:include page="/toptsp.jsp?encabezado=Presupuesto Gastos Administrativos - Listar"/>
</div>
<div id="capaCentral" style="position:absolute; width:100%; height:87%; z-index:0; left: 0px; top: 100px; overflow: scroll;">
<% 	
	Vector vec = (Vector) request.getAttribute("vector");
    String style = "simple";
    String position =  "bottom";
    String index =  "center";
    int ancho = request.getParameter("vlrs").compareTo("t")==0 ? 220 : 120;
	String mostrar = request.getParameter("vlrs");
	int maxPageItems = 7;
    int maxIndexPages = 5;
	if (vec.size() > 0) {
%>
<br>
<table width="99%" border="2" align="center">
  <tr>
    <td class="barratitulo">
	  <table width="100%">
        <tr>
          <td width="48%" class="subtitulo1">Presupuesto de Gastos Administrativos</td>
          <td width="52%" ><img src="<%=BASEURL%>/images/titulo.gif" width="32" height="20"></td>
        </tr>
      </table>
	  <table width="100%" border="1" cellpadding="4" cellspacing="1" bordercolor="#999999" bgcolor="#F7F5F4" align="center">
        <tr class="tblTitulo" align="center">
          <td width="73" nowrap>Clase de Cuenta</td>
          <td width="125" nowrap>Agencia</td>
          <td width="125" nowrap>Unidad de Negocios</td>
          <td width="150" nowrap>Area</td>
          <td width="125" nowrap>Elemento del Gasto </td>
		  <td width="95" nowrap >Distrito</td>
		  <td width="50" nowrap >A&ntilde;o</td>
<%
	for( int j=0; j<12; j++){
		if ( mostrar.compareTo("t")==0 ){
%>
		<td width="<%= ancho%>" nowrap colspan="2">		  
		  	<table border="0">
            <tr>
              <td colspan="2" class="tblTitulo"><div align="center"><%= UtilFinanzas.NombreMes(j+1).toUpperCase() %></div></td>
              </tr>
            <tr class="tblTitulo">
              <td width="120"><div align="center">Presupuestado</div></td>
              <td width="120"><div align="center">Ejecutado</div></td>
            </tr>
          </table>
<%} else {%>
	<td width="<%= ancho%>" nowrap>	
		<div align="center"><%= UtilFinanzas.NombreMes(j+1).toUpperCase() %><br><%= mostrar.compareTo("p")==0 ? "Presupuestado" : "Ejecutado" %></div>
<%}%>		  
		  </td>
<%	}%>		  
		</tr>
    <pg:pager        
		items="<%=vec.size()%>"
        index="<%= index %>"
        maxPageItems="<%= maxPageItems %>"
        maxIndexPages="<%= maxIndexPages %>"
        isOffset="<%= true %>"
        export="offset,currentPageNumber=pageNumber"
        scope="request">
<%
    for (int i = offset.intValue(), l = Math.min(i + maxPageItems, vec.size()); i < l; i++){
        PtoGastosAdmin p = (PtoGastosAdmin) vec.elementAt(i);
		double[] vlr_pto = p.getVlr_presupuestado();
		double[] vlr_ejec = p.getVlr_ejecutado();
%>
        <pg:item>
        <tr class="<%=(i % 2 == 0 )?"filagris":"filaazul"%>" onMouseOver='cambiarColorMouse(this)' style="cursor:hand" title="Lista de Campos" onClick="window.open('<%= CONTROLLERPTO %>?ACCION=/PtoGastosAdmin/Obtener&ano=<%= p.getAno() %>&cuenta=<%= p.getCuenta() %>&dstrct=<%= p.getDstrct() %>','PtoGastosAdminUpdate','status=yes,scrollbars=no,width=800,height=600,resizable=yes');">
          <td class="bordereporte"><%= p.getTipo_cuenta()%></td>
          <td class="bordereporte"><%= p.getAgencia()%></td>
          <td class="bordereporte"><%= p.getUnidad()%></td>
          <td class="bordereporte"><%= p.getArea()%></td>
		  <td class="bordereporte"><%= p.getElemento()%></td>
		  <td class="bordereporte"><%= p.getNom_distrito()%></td>
		  <td class="bordereporte"><%= p.getAno()%></td>
<%
		for( int j=0; j<12; j++){			
			if ( mostrar.compareTo("t")==0 || mostrar.compareTo("p")==0 ){
%>		  
		  <td align="right" width="120" class="bordereporte"><%= UtilFinanzas.customFormat(vlr_pto[j])%></td> 
<%			} 
			if ( mostrar.compareTo("t")==0 || mostrar.compareTo("e")==0 ){
%>         

          <td align="right" width="120" class="bordereporte"><%= UtilFinanzas.customFormat(vlr_ejec[j])%></td>
<%
			}
		}
%>		
        </tr></pg:item>
<%  }%>
      
        <tr class="pie">
          <td td height="20" colspan="19" nowrap align="center">          <pg:index>
                <jsp:include page="/WEB-INF/jsp/googlesinimg.jsp" flush="true"/>
        </pg:index>
          </td>
        </tr>
    </pg:pager>        
      </table>
  </table>
<%}else {%>
    <table border="2" align="center">
      <tr>
        <td>
	      <table width="100%"  border="1" align="center"  bordercolor="#F7F5F4" bgcolor="#FFFFFF">
            <tr>
              <td width="229" align="center" class="mensajes">Su b&uacute;squeda no arroj&oacute; resultados. </td>
              <td width="29"><img src="<%=BASEURL%>/images/cuadronaranja.JPG"></td>
            </tr>
          </table>
	    </td>
      </tr>
    </table>
<%}%>
<p>
<div align="center">
        <img src="<%=BASEURL%>/images/botones/regresar.gif" name="c_cancelar" onClick="history.back();" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);"> 
		<img src="<%=BASEURL%>/images/botones/salir.gif" name="c_salir" id="c_salir" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" onClick="parent.close();">
</div>
</div>
</body>
</html>
