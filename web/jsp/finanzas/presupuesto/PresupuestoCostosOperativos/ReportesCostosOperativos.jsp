
<!--  
     - Author(s)       :      Ing. Juan M. Escandon Perez
     - Date            :      10/12/2005  
     - Copyright Notice:      TRANSPORTES SANCHEZ POLO S.A.
  -->
 <%--   
     - @(#)  
     - Description: Permite realizar Discrimininacion de los Costos operativos por Standard
 --%>
<%@page session="true"%>
<%@page errorPage="/error/ErrorPage.jsp"%>
<%@include file="/WEB-INF/InitModel.jsp"%>

<%@page import="java.util.*" %>
<%@page import="com.tsp.finanzas.presupuesto.model.beans.*" %>
<%@page import="com.tsp.util.UtilFinanzas" %>

<%
    String Ano = modelpto.ReportesSvc.getAno();
    String Mes = modelpto.ReportesSvc.getMes();
    int anchoCol = 29; 
    int restoCol = 400;
    
    String estiloPtoMyEjt = "style='color:red;        font-weight:bold; text-align:center; '";
    String estiloPtoMnEjt = "style='color:darkgreen;  font-weight:bold; text-align:center; '";
    
    int today = Integer.parseInt(UtilFinanzas.getFechaActual_String(8));
%>

<html>
    <head><title>Presupuesto Costos Operativos Por Estandar</title>
    <link href="<%= BASEURL %>/css/estilostsp.css" rel='stylesheet'>
	<script language="javaScript" type="text/javascript" src="<%=BASEURL%>/js/boton.js"></script>	
    </head>
    <body onResize="redimensionar()" onLoad="redimensionar()">
		<div id="capaSuperior" style="position:absolute; width:100%; height:100px; z-index:0; left: 1px; top: 0px;">
 			<jsp:include page="/toptsp.jsp?encabezado=Reporte Por Estandar"/>
		</div>
	<div id="capaCentral" style="position:absolute; width:100%; height:87%; z-index:0; left: 0px; top: 100px; overflow: scroll;">	
<% 
    List listado = modelpto.ReportesSvc.getListado();
    if (listado.size()>0){
%>        <!-- ******************************* AGENCIA  ************************************ -->		
			<%				
                for (int i_ag = 0 ; i_ag < listado.size(); i_ag++){
                    ViajesAgencia ag = (ViajesAgencia) listado.get(i_ag);
			%>
					
		  <!-- ******************************* CLIENTE  ************************************ -->
			<%
					for (int i_cl = 0 ; i_cl < ag.getListadoClientes().size(); i_cl++)      {
                        ViajesCliente cl = (ViajesCliente) ag.getListadoClientes().get(i_cl); 
            %>   
			<table border='2' width="60%" align="center">            
			<tr> 
				<td>
					
				</td> 
			</tr>  
            <tr><td>
                <!-- ******************************* ESTANDAR  ************************************ -->
                <%                      
                            for (int i_st = 0 ; i_st < cl.getListaStandar().size(); i_st++)      {
                                ViajesStandar st = (ViajesStandar) cl.getListaStandar().get(i_st);                      
                %>
                <table width="100%" border="0">                
				<tr> 
					<td>
						<table width="100%">
							<td width="70%" class="subtitulo1"><b>ESTANDAR [<%=  st.getStdJobNo()  %>] <%= st.getStdJobDesc() %></b></td>
							<td width="30%" class="barratitulo"><img src="<%=BASEURL%>/images/titulo.gif" width="32" height="20"></td>
						</table>
					</td> 
				</tr>
                <tr><td>
                <!-- **************************** ESTANDAR ********************************** -->
                <table border='0' width='100%' class="tablaInferior">
                        <tr class='tblTitulo'>
                        <th>Elemento </th>
						<th>Periodo</th>
                        <th>Valor</th>                        
                        </tr>
                        
                        <%
                            for (int i_co = 0 ; i_co < st.getListCostosOperativos().size(); i_co++)      {
                                CostosOperativos co = (CostosOperativos) st.getListCostosOperativos().get(i_co);
                        %>
                        <tr class=<%=(i_st % 2 == 0 )?"filagris":"filaazul"%>>
                            <td class='bordereporte' width='130' align='center'><%= co.getElemento() %></td>
							<td class='bordereporte' width='171' align='center'><%= co.getAno() + "-" + co.getMes()%></td>
                            <td class='bordereporte' width='279' align='center'><%= UtilFinanzas.customFormat(co.getValor())%></td>                            
                        </tr>
                        	<%  } %>
							<!-- *****************************  FIN COSTOS OPERATIVOS***************************** -->
                        <tr class='filagris'>
                            <td class='bordereporte' align='center' colspan='2'>Totales Acumulados por estandar </td>
                            <td class='bordereporte' align='center'><%= UtilFinanzas.customFormat(st.getValor_total_costos())%></td>                                       
                            <%  } %>
						<!-- *****************************  FIN ESTANDAR ***************************** -->
                        </tr>
                        
                    </table>
                
                </td></tr>
              </table>
                <%  } %>                
                <!-- *****************************  FIN CLIENTES  ************************************ -->
                </td>
            </tr>
            <tr>
            <td>
			<%  } %>
            <!-- FIN TOTALES AGENCIAS -->
            </td>
            </tr>
        </table>    
<%   }     // end if %>
<table>
<br>
<table width="60%" align="center">
<tr>
<td align="left">
<img src="<%=BASEURL%>/images/botones/salir.gif" width="90" height="21" onClick="parent.close();" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" style="cursor:hand"></tr>
</td>
</table>
</table>
    </div>
    </body>
</html>
