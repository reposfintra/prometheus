<!--  
     - Author(s)       :      Ing. Juan M. Escandon Perez
     - Date            :      10/12/2005  
     - Copyright Notice:      TRANSPORTES SANCHEZ POLO S.A.
  -->
 <%--   
     - @(#)  
     - Description: Muestra el Presupuesto de Costos operativos vs Ejecutado
 --%>
<%@page session="true"%>
<%@page errorPage="/error/ErrorPage.jsp"%>
<%@include file="/WEB-INF/InitModel.jsp"%>

<%@page import="java.util.*" %>
<%@page import="com.tsp.finanzas.presupuesto.model.beans.*" %>
<%@page import="com.tsp.util.UtilFinanzas" %>

<%
    String Ano = modelpto.ReportesSvc.getAno();
    String Mes = modelpto.ReportesSvc.getMes();
    String Distrito = request.getParameter("Distrito");
    String Agencia  = request.getParameter("Agencia");
	String Cliente  = request.getParameter("Cliente");
    int anchoCol = 29; 
    int restoCol = 400;
    
    String estiloPtoMyEjt = "style='color:red;        font-weight:bold; text-align:center; '";
    String estiloPtoMnEjt = "style='color:darkgreen;  font-weight:bold; text-align:center; '";
    
	String Mensaje       = (request.getParameter("Mensaje")!=null)?request.getParameter("Mensaje"):"";
	
    int today = Integer.parseInt(UtilFinanzas.getFechaActual_String(8));
%>

<html>
    <head><title>Presupuesto Costos Operativos Por Estandar</title>
    <link href="<%= BASEURL %>/css/estilostsp.css" rel='stylesheet'>
	<script language="javaScript" type="text/javascript" src="<%=BASEURL%>/js/boton.js"></script>
	<script>
            function newWindow(url, nombre){
              option="  width="+ (screen.width-30) +", height="+ (screen.height-80)  +",  scrollbars=no, statusbars=yes, resizable=yes, menubar=no ,top=10 , left=10 ";
              ventana=window.open('',nombre,option);
              ventana.location.href=url;
            }
        
            function go(Estandar){			
                var url = "<%= CONTROLLER %>?estado=Opciones&accion=Reportes&Opcion=GenerarCostos&Ano=<%= Ano %>&Mes=<%= Mes %>&Distrito=<%= Distrito %>&Agencia=<%= Agencia %>&Tipo=4&Cliente=<%=Cliente%>&Estandar=" + Estandar+"&marco=no";
                newWindow(url, 'PE'+Estandar);
            }    
    </script>
    </head>
    <body onResize="redimensionar()" onLoad="redimensionar()">
		<div id="capaSuperior" style="position:absolute; width:100%; height:100px; z-index:0; left: 1px; top: 0px;">
 			<jsp:include page="/toptsp.jsp?encabezado=Costos Operativos Ejec Vs Pres"/>
		</div>
	<div id="capaCentral" style="position:absolute; width:100%; height:87%; z-index:0; left: 0px; top: 100px; overflow: scroll;">
	<form action="<%= CONTROLLERPTO %>?ACCION=/Opciones/Reportes&Opcion=GenerarExcelVs&Ano=<%= Ano %>&Mes=<%= Mes %>&Distrito=<%= Distrito %>&Agencia=<%= Agencia %>&Tipo=8&Cliente=<%=Cliente%>"  method='post' name='formulario'>
<% 
    List listado = modelpto.ReportesSvc.getListado();
    if (listado.size()>0){
%>
            <%
                for (int i_ag = 0 ; i_ag < listado.size(); i_ag++){
                    ViajesAgencia ag = (ViajesAgencia) listado.get(i_ag);
            %>
			<table border='2' width="800">            
			<tr> 
				<td>
					<table width="100%">
						<td width="25%" class="subtitulo1">AGENCIA [<%= ag.getAgencia() %>] <%= ag.getAgenciaNombre() %></td>
						<td width="52%" class="barratitulo"><img src="<%=BASEURL%>/images/titulo.gif" width="32" height="20"></td>
					</table>
				</td> 
			</tr>  
            <tr><td>
                <!-- ******************************* CLIENTES   ************************************ -->
                <%
                    for (int i_cl = 0 ; i_cl < ag.getListadoClientes().size(); i_cl++)      {
                        ViajesCliente cl = (ViajesCliente) ag.getListadoClientes().get(i_cl); 
                %>
                <table width="100%" border="0">                
				<tr> 
					<td>
						<table width="100%">
							<td width="25%" class="subtitulo1"><b>CLIENTE [<%= cl.getCliente() %>] <%= cl.getClienteNombre() %></b></td>
							<td width="52%" class="barratitulo"><img src="<%=BASEURL%>/images/titulo.gif" width="32" height="20"></td>
						</table>
					</td> 
				</tr>
                <tr><td>
                <!-- **************************** ESTANDAR ********************************** -->
                <table border='0' cellpadding='1' cellspacing='1' width="100%">
                  <tr class='tblTitulo'>
                    <th>Estandar </th>
                    <th>Descripcion</th>
                    <th>Total Mensual </th>
                    <th>Ejecutado </th>
                    <th>Presupuestado</th>
                    <% //for (int ix_st = 1 ; ix_st <= 31 ; ix_st++ ) out.print("<th width='70'>"+ UtilFinanzas.DiaSemana(Ano + Mes + UtilFinanzas.DiaFormat(ix_st)) + " " + ix_st +"</th>"); %>
                  </tr>
                  <%
                            for (int i_st = 0 ; i_st < cl.getListaStandar().size(); i_st++)      {
                                ViajesStandar st = (ViajesStandar) cl.getListaStandar().get(i_st);
                        %>
                  <tr class=<%=(i_st % 2 == 0 )?"filagris":"filaazul"%>>
                    <td class='bordereporte' width='64' align='center'><%= st.getStdJobNo() %></td>
                    <td class='bordereporte' width='359'><%= st.getStdJobDesc()               %></td>
                    <td class='bordereporte' width='117' align='center'><%= UtilFinanzas.customFormat(st.getTotalCostosEj()+st.getTotalCostosPd())%></td>
                    <td class='bordereporte' width='117' align='center'><%= UtilFinanzas.customFormat(st.getTotalCostosEj())%></td>
                    <td class='bordereporte' width='117' align='center'><%= UtilFinanzas.customFormat(st.getTotalCostosPd())%></td>                    
                  </tr>
                  <%  } %>
                  <tr class='filagris'>
                    <td class='bordereporte' align='center' colspan='2'>Totales Acumulados por cliente</td>
                    <td class='bordereporte' align='center'><%= UtilFinanzas.customFormat(cl.getTotalCostosEj()+cl.getTotalCostosPd())%></td>
                    <td class='bordereporte' align='center'><%= UtilFinanzas.customFormat(cl.getTotalCostosEj())%></td>
                    <td class='bordereporte' align='center'><%= UtilFinanzas.customFormat(cl.getTotalCostosPd())%></td>                   
                  </tr>
                </table>
                <!-- *****************************  FIN ESTANDAR ***************************** -->
					</td>
                </tr>
              </table>
                <%  } %>
                
                <!-- *****************************  FIN CLIENTES  ************************************ -->
                </td>
            </tr>
            <tr>
            <td>
            <!-- TOTALES AGENCIAS -->                
				<br>
                <table width="50%" border='1'>
                <tr><td>
                <table border='0'>
                        <tr class='tblTitulo'>
                        <th colspan='2' >Agencia</th>
                        <th>Presup<br>Mensual</th>  
						<th>Ejecutado</th>
						<th>Presupuestado</th>                      
                        </tr>
                    <tr class='filagris'>
                        <td class='bordereporte'align='center' colspan='2'> <%= ag.getAgenciaNombre() %> </td>
                        <td class='bordereporte' width='109' align='center'><%= UtilFinanzas.customFormat(ag.getTotalCostosEj()+ag.getTotalCostosPd())%></td>                    
						<td align="center"><%= UtilFinanzas.customFormat(ag.getTotalCostosEj()) %></td>
						<td align="center"><%= UtilFinanzas.customFormat(ag.getTotalCostosPd())%></td>
                    </tr>
                </table>                
              </td></tr></table>
            <!-- FIN TOTALES AGENCIAS -->
            </td>
            </tr>
      </table>
        <br><br>
        <%  } // end for %>        
        
<%   }     // end if %>
<table>
<img src="<%=BASEURL%>/images/botones/regresar.gif" width="90" height="21" onClick="window.location='<%=BASEURL%>/jsp/finanzas/presupuesto/PresupuestoCostosOperativos/CostosEjecVsPdo.jsp'" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" style="cursor:hand">
<img src="<%=BASEURL%>/images/botones/exportarExcel.gif" width="90" height="21" onClick="formulario.submit();" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" style="cursor:hand">
</table>
<%if(!Mensaje.equals("")){%>
  <p><table border="2" align="center">
  <tr>
    <td><table width="100%"  border="1" align="center"  bordercolor="#F7F5F4" bgcolor="#FFFFFF">
      <tr>
        <td width="229" align="center" class="mensajes"><%=Mensaje%></td>
        <td width="29" background="<%=BASEURL%>/images/cuadronaranja.JPG">&nbsp;</td>
        <td width="58">&nbsp;</td>
      </tr>
    </table></td>
  </tr>
</table>
</p>
 <%}%>
</form>
    </div>
    </body>
</html>
