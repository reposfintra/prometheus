<!--
- Autor : Osvaldo P�rez Ferrer
- Date  : 25 de Junio de 2006
- Copyrigth Notice : Fintravalores S.A. S.A
-->
<%--
-@(#)
--Descripcion : Pagina JSP, que permite la creacion del archivo PDF
--%>

<%-- Declaracion de librerias--%>
<%@page contentType="text/html"%>
<%@page session="true"%>
<%@page errorPage="/error/ErrorPage.jsp"%>
<%@page import="java.util.*"%>
<%@page import="java.util.Vector"%>
<%@page import="java.text.*"%>
<%@page import="com.tsp.propiedades.pdf.*"%>
<%@page import="com.tsp.operation.model.beans.*"%>
<%@page import="com.tsp.util.*"%>
<%@page import="com.tsp.operation.model.*"%>
<%@ include file="/WEB-INF/InitModel.jsp"%>
<%@ page import="java.io.File"%>
<html>
<head>
<title>Formato de Impresi�n Libro Diario</title>
</head>
<%
  response.sendRedirect("pdf/LibroDiario.pdf");  
%>
</body>
</html>