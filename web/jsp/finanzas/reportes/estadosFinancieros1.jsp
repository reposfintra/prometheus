<%@ page session="true"%>
<%@page import="com.tsp.operation.model.beans.*,com.tsp.util.*"%>
<%@page import="java.util.*"%>
<%@ include file="/WEB-INF/InitModel.jsp"%>
<%@ taglib uri="http://jakarta.apache.org/taglibs/input-0.90" prefix="input" %>

<html>
<head>
    <title>Estados Financieros 1</title>
    <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
    <link href="/css/estilostsp.css" rel="stylesheet" type="text/css">
    <link href="<%=BASEURL%>/css/estilostsp.css" rel="stylesheet" type="text/css">

    <script language="JavaScript" type="text/javascript" src="<%=BASEURL%>/js/boton.js"></script>
    <script type="text/javascript">
        function soloNumeros(id) {
        var valor = document.getElementById(id).value;
        valor =  valor.replace(/[^0-9^.]+/gi,"");
        document.getElementById(id).value = valor;
        }
        

        }
    </script>


	<%
		String mensaje = (request.getParameter("Mensaje")!=null)?request.getParameter("Mensaje"):"";
		int anio = Util.AnoActual();
        %>
</head>
<body>
<div id="capaSuperior" style="position:absolute; width:100%; height:100px; z-index:0; left: 1px; top: 0px;">
 <jsp:include page="/toptsp.jsp?encabezado=Estados Financieros"/>
</div>
<div id="capaCentral" style="position:absolute; width:100%; height:87%; z-index:0; left: 0px; top: 100px; overflow: scroll;">

<form name="forma" method="post" action="<%=CONTROLLER%>?estado=Finanzas&accion=Reportes&evento=ESTADO_FINANCIERO_1">



    <table width="650" border="2" align="center">
      <tr>
        <td>
          <table width='100%' align='center' class='tablaInferior'>
            <tr>
              <td class="barratitulo" colspan='3' >
                <table cellpadding='0' cellspacing='0' width='100%'>
                  <tr class="fila">
                    <td width="50%" align="left" class="subtitulo1">&nbsp;Generacion del Estado Perdidas y Ganancias</td>
                    <td width="*"   align="left"><img src="<%= BASEURL %>/images/titulo.gif" width="32" height="20"  align="left"></td>
                  </tr>
              </table></td>
            </tr>
                    <tr class="fila">
                        <td width="40%">&nbsp;Periodo : </td>
                        <td><select id="mes" name="mes">
                            <option value="1">Ene</option>
                            <option value="2">Feb</option>
                            <option value="3">Mar</option>
                            <option value="4">Abr</option>
                            <option value="5">May</option>
                            <option value="6">Jun</option>
                            <option value="7">Jul</option>
                            <option value="8">Ago</option>
                            <option value="9">Sep</option>
                            <option value="10">Oct</option>
                            <option value="11">Nov</option>
                            <option value="12">Dic</option>
                            <option value="13">Fis</option>
                        </select>

                        <select id="anio" name="anio">
						<% for ( int i = 2007 ; i <= anio; i ++ ){%>
                        <option value="<%=i%>" <%if(i == anio){%>selected<%}%>><%=i%></option>
						<%}%>
                        </select>

                    </tr>

                    <tr class="fila">
                        <td width="40%">&nbsp;Formato :</td>
                        <td>

                            <div id="listaFormatos">

                                <!-- onmouseover = "localizaFormatos(this)" funcion ubicada temporalmente en boton.js -->

                                <select name="formato" class="textbox" id="formato" style="width:57%"  >
                                    <option value="EF_1" selected >PyG General</option>
                                    <option value="EF_3">PyG Unidad de negocio 001</option>
                                    <option value="EF_4">PyG Unidad de negocio 002</option>
                                    <option value="EF_5">PyG Unidad de negocio 003</option>
                                    <option value="EF_6">PyG Unidad de negocio 004</option>
                                    <option value="EF_7">PyG Unidad de negocio 005</option>
                                    <option value="EF_8">PyG Unidad de negocio 006</option>
                                </select>
                            </div>
                            
                        </td>
                    </tr>
                    <tr>
                        <td class="fila">&nbsp;Secuencia :</td>
                        <td class="fila"><input type="text" name="secuencia_inicio" id="secuencia_inicio" value="0" size="10" onkeyup="soloNumeros(this.id);" /> A
                            <input type="text" name="secuencia_final" id="secuencia_final"  value="0" size="10" onkeyup="soloNumeros(this.id);" />
                        </td>
                                      
                    </tr>

        </table></td>
      </tr>
    </table>
    <br>
	<table align="center">
<tr>
        <td colspan="2" nowrap align="center">
		  <img src="<%=BASEURL%>/images/botones/aceptar.gif" name="aceptar"  height="21" onClick="this.disabled=true;forma.submit()" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" style="cursor:hand">&nbsp;
		  <img src="<%=BASEURL%>/images/botones/salir.gif" name="mod"  height="21" onclick="window.close();" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" style="cursor:hand">
        </td>
      </tr>
</table>
    <br>
    <br>
    <table width="40%"  align="center">
      <tr>
        <td>
          <FIELDSET>
          <legend><span class="letraresaltada">Nota</span></legend>
          <table width="100%"  border="0" cellpadding="0" cellspacing="0" class="informacion">
            <tr>
              <td nowrap align="center">&nbsp; Para iniciar el proceso de generacion del estado financiero haga click en aceptar. </td>
            </tr>
          </table>
        </FIELDSET></td>
      </tr>
    </table>

<br>
<%if(!mensaje.equals("")){%>
<table border="2" align="center">
  <tr>
    <td><table width="100%" border="1" align="center"  bordercolor="#F7F5F4" bgcolor="#FFFFFF">
        <tr>
          <td width="262" align="center" class="mensajes"><%=mensaje%></td>
          <td width="32" background="<%=BASEURL%>/images/cuadronaranja.JPG">&nbsp;</td>
          <td width="44">&nbsp;</td>
        </tr>
    </table></td>
  </tr>
</table>
<%}%>

</form>
</div>
</body>
</html>