<!--  
	 - Author(s)       :      FFERNANDEZ
	 - Description     :      AYUDA FUNCIONAL - CLIENTE BUSCAR
	 - Date            :      3/11/2006
	 - Copyright Notice:      TRANSPORTES SANCHEZ POLO S.A.
-->
<%@ include file="/WEB-INF/InitModel.jsp"%>
<HTML>
<HEAD>
<TITLE>Funcionalidad del  Reporte de Corridas por transferencias</TITLE>
<META http-equiv=Content-Type content="text/html; charset=windows-1252">
<link href="/../../../css/estilostsp.css" rel="stylesheet" type="text/css"> 
<link href="<%=BASEURL%>/css/estilostsp.css" rel="stylesheet" type="text/css">
<script type='text/javascript' src="<%= BASEURL %>/js/boton.js"></script> 
</HEAD>
<BODY> 
<% String BASEIMG =  BASEURL +"/images/ayuda/Ctranferencias/"; %>
  <table width="95%"  border="2" align="center">
    <tr>
      <td height="117" >
        <table width="100%" border="0" align="center">
          <tr  class="subtitulo">
            <td height="20"><div align="center">MANUAL DE REPORTE WEB</div></td>
          </tr>
          <tr class="subtitulo1">
            <td>Descripci&oacute;n del funcionamiento del  Reporte de Corridas por transferencias </td>
          </tr>
          <tr>
            <td  class="ayudaHtmlTexto"><div align="center"></div></td>
          </tr>
<tr>
            <td  class="ayudaHtmlTexto"><p class="ayudaHtmlTexto">&nbsp;</p>
            <p class="ayudaHtmlTexto">En la siguiente pantalla  muestra los campos de filtros para buscar las Corridas por transferencia de Propietarios dependiendo:</p>
            <p class="ayudaHtmlTexto">Corrida: ingrese la Corrida para su busqueda de informacion de todos los propietarios exiostentes en esa corrida </p>
            <p class="ayudaHtmlTexto">Propietario:  ingrese la Cedula del propietario para su busqueda de informacion </p>
            <p class="ayudaHtmlTexto">&nbsp; </p>
            </td>
          </tr>
<tr>
            <td  class="ayudaHtmlTexto"><div align="center"><IMG height=146 src="<%=BASEIMG%>imagen_001.JPG" width=463 border=0 v:shapes="_x0000_i1143"></div></td>
          </tr>
 <tr>
  <td  class="ayudaHtmlTexto"><p>&nbsp;</p>
    <p>El sistema le muestra un Mensaje el usuario no ha digitado ningun valor </p></td>
</tr>
<tr>
  <td  class="ayudaHtmlTexto"><div align="center"> <img height=343 src="<%=BASEIMG%>imagen_002.JPG" width=463 border=0 v:shapes="_x0000_i1052"></div></td>
</tr>
<tr>
  <td  class="ayudaHtmlTexto"><p>&nbsp;</p>
    <p>El sistema le muestra un Mensaje Cuando no muestra ningun resultado de la Busqueda </p></td>
</tr>
<tr>
  <td  class="ayudaHtmlTexto"><div align="center"> <img height=119 src="<%=BASEIMG%>error_001.JPG" width=601 border=0 v:shapes="_x0000_i1052"></div></td>
</tr>
<tr>
  <td  class="ayudaHtmlTexto"><p>&nbsp;</p>
    <p>En esta pantalla muestra Cuando la consulta fue ejecutada y muestra el listado del Propietario en particular</p></td>
</tr>
<tr>
  <td  class="ayudaHtmlTexto"><p align="center"><IMG height=402 src="<%=BASEIMG%>imagen_004.JPG" width=944 border=0 v:shapes="_x0000_i1052"></p>    </td>
</tr>
<tr>
  <td  class="ayudaHtmlTexto"><p>&nbsp;</p>
    <p>En esta pantalla muestra Cuando la consulta fue ejecutada y muestra el listado de los Propietarios puede verlos dandole click en la paginaci&oacute;n</p></td>
</tr>
<tr>
  <td  class="ayudaHtmlTexto"><p align="center"><IMG height=480 src="<%=BASEIMG%>imagen_003.JPG" width=943 border=0 v:shapes="_x0000_i1052"></p>    </td>
</tr>
       </table></td>
    </tr>
  </table>
  <p></p>
<center>
<img src = "<%=BASEURL%>/images/botones/salir.gif" style = "cursor:hand" name = "imgsalir" onClick = "parent.close();" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);">
</center>
</BODY>
</HTML>
