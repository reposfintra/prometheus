<!--
- Autor : FFERNANDEZ
- Date  : 07 Novimebre 2006
- Copyrigth Notice : Fintravalores S.A. S.A
-->
<%--
The taglib directive below imports the JSTL library. If you uncomment it,
you must also add the JSTL library to the project. The Add Library... action
on Libraries node in Projects view can be used to add the JSTL 1.1 library.
--%>
<%-- Declaracion de librerias--%>
<%@page session="true"%>
<%@page errorPage="../error/ErrorPage.jsp"%>
<%@page import="com.tsp.operation.model.beans.*, java.util.*, java.text.*"%>
<%@ include file="/WEB-INF/InitModel.jsp"%>
<%@ taglib uri="http://jakarta.apache.org/taglibs/input-0.90" prefix="input" %>
<%@page import="com.tsp.util.*"%>
<%@taglib uri="http://jsptags.com/tags/navigation/pager" prefix="pg" %>
<% Util u = new Util(); %>
<html>
    <head>        
        <title>Reporte Corridas Tranferencias </title>
		<script src='<%=BASEURL%>/js/date-picker.js'></script>
		<script language="JavaScript" type="text/javascript" src="<%=BASEURL%>/js/boton.js"></script>
		<script type='text/javascript' src="<%=BASEURL%>/js/validar.js"></script> 
		<link href="../../estilostsp.css" rel="stylesheet" type="text/css">
		<link href="<%=BASEURL%>/css/estilostsp.css" rel="stylesheet" type="text/css">
		 <script>
		 	function Exportar (){
				
			    alert( 
				
					'Puede observar el seguimiento del proceso en la siguiente ruta del menu:' + '\n' + '\n' +
					'SLT -> PROCESOS -> Seguimiento de Procesos' + '\n' + '\n' + '\n' +
					'Y puede observar el archivo excel generado, en la siguiente parte:' + '\n' + '\n' +
					'En la pantalla de bienvenida de la Pagina WEB,' + '\n' +
					'vaya a la parte superior-derecha,' + '\n' +
					'y presione un click en el icono que le aparece.'
					
				);
				
			}
		</script>
    </head>
   <body onResize="redimensionar()" onLoad="redimensionar()">
	<div id="capaSuperior" style="position:absolute; width:100%; height:100px; z-index:0; left: 1px; top: 0px;">
	 <jsp:include page="/toptsp.jsp?encabezado=Reporte Corridas Tranferencias"/>
	</div>
	<div id="capaCentral" style="position:absolute; width:100%; height:87%; z-index:0; left: 0px; top: 100px; overflow: scroll;">
	<%
	   String style = "simple";
	   String position =  "bottom";
       String index =  "center";
       int maxPageItems = 20;
       int maxIndexPages = 10;
	   String msg = request.getParameter("msg");
		if ( msg!=null && !msg.equals("") ){%>							
		  <table border="2" align="center">
			<tr>
				<td>
					<table width="100%"  border="1" align="center"  bordercolor="#F7F5F4" bgcolor="#FFFFFF">
						<tr>
							<td width="229" align="center" class="mensajes"><%=msg%></td>
							<td width="29" background="<%=BASEURL%>/images/cuadronaranja.JPG">&nbsp;</td>
							<td width="58">&nbsp;</td>
						</tr>
					</table>
				</td>
			</tr>
		  </table>		  
		        <br>
		        <img src="<%=BASEURL%>/images/botones/regresar.gif" name="c_regresar" onclick="location.href='<%=BASEURL%>/jsp/finanzas/reportes/ReporteCTransferencia.jsp'" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" style="cursor:hand">
 	  <input name="salir" src="<%=BASEURL%>/images/botones/salir.gif" type="image" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" style="cursor:hand" class="boton" onclick="window.close();">
	  <%}
		else {
          Vector datos =model.corridaService.getVector();
		  			
		if( datos != null && datos.size() > 0 ){
			BeanGeneral beanGeneral = (BeanGeneral) datos.elementAt(0);
		%>		
		    <form name="form1" method="post" action="<%=CONTROLLER%>?estado=Corridas&accion=Reporte&opcion=2">
              <p>
	    <input name="Corridas" type="hidden" id="Corridas" value="<%=model.corridaService.getCorrida()%>">
	    </p>
              <p>
	      <input name="Propietario" type="hidden" id="Propietario" value="<%=model.corridaService.getPropietario()%>">
              </p>
             
              <p>
                <input name="Guardar2" src="<%=BASEURL%>/images/botones/exportarExcel.gif" type="image" class="boton" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" style="cursor:hand" onclick="Exportar();">
                <img src="<%=BASEURL%>/images/botones/regresar.gif" name="c_regresar" onclick="location.href='<%=BASEURL%>/jsp/finanzas/reportes/ReporteCTransferencia.jsp'"  onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" style="cursor:hand">
 	            <input name="salir" src="<%=BASEURL%>/images/botones/salir.gif" type="image" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" style="cursor:hand" class="boton" onclick="window.close();">
                                                                    </p>
		    </form>
		<table width="100%"  border="2">  
          <tr>
        	<td class="subtitulo1"><strong>
        	  <p>Reporte Corridas de Transferencias:</p> 
        	</strong>
			  
          </tr>
          <tr>
            <td>
              <table width="100%"  border="1" bordercolor="#999999" bgcolor="#F7F5F4" align="center">
                <tr class="tblTitulo" align="center">
				  <td nowrap width="5%" align="center">ITEM</td>
				  <td nowrap width="5%" align="center">CORRIDA</td>
                  <td nowrap width="5%" align="center">PROPIETARIO</td>
				  <td nowrap width="5%" align="center">NOMBRE</td>
                  <td nowrap width="5%" align="center">BANCO TRANSFERENCIA</td>
				  <td nowrap width="5%" align="center">SUCURSAL DEL BANCO</td>
				  <td nowrap width="5%" align="center">TIPO DE PAGO</td>
				  <td nowrap width="5%" align="center">VALOR</td>
                  <td nowrap width="5%" align="center">TIPO CUENTA </td>
				  <td nowrap width="5%" align="center">CEDULA CUENTA </td>
                  <td nowrap width="5%" align="center">NIT CUENTA </td>
				  <td nowrap width="5%" align="center">NOMBRE CUENTA </td>
				  
				         
                </tr>	 <pg:pager
         items="<%=datos.size()%>"
         index="<%= index %>"
         maxPageItems="<%= maxPageItems %>"
         maxIndexPages="<%= maxIndexPages %>"
         isOffset="<%= true %>"
         export="offset,currentPageNumber=pageNumber"
         scope="request">
        <%-- keep track of preference --%>
        <%
         for (int i = offset.intValue(), l = Math.min(i + maxPageItems,datos.size()); i < l; i++){
					
				BeanGeneral info = (BeanGeneral) datos.elementAt(i);
				
				//if (info.getValor_14().equals("0099-01-01 00:00:00")){ %>
				  <tr class="<%=( info.getValor_14().equals("0099-01-01 00:00:00") )?"filaroja":(i % 2 == 0 )?"filagris":"filaazul"%>" onMouseOver='cambiarColorMouse(this)'   style="cursor:hand"
        onClick="window.open('<%=CONTROLLER%>?estado=Corridas&accion=Reporte&opcion=3&Corridas=<%=info.getValor_01()%>&ced=<%=info.getValor_02()%>' ,'M','status=yes,scrollbars=no,width=780,height=650,resizable=yes');">
        
			 <pg:item>
	  
                  <td nowrap align="left" class="bordereporte"><%=(info.getValor_13()!=null)?info.getValor_13():""%>&nbsp;</td>
				  <td nowrap align="left" class="bordereporte"><%=(info.getValor_01()!=null)?info.getValor_01():""%>&nbsp;</td>
                  <td nowrap align="left" class="bordereporte"><%=(info.getValor_02()!=null)?info.getValor_02():""%>&nbsp;</td>
                  <td nowrap align="left" class="bordereporte"><%=(info.getValor_03()!=null)?info.getValor_03():""%>&nbsp;</td>
                  <td nowrap align="left" class="bordereporte"><%=(info.getValor_04()!=null)?info.getValor_04():""%>&nbsp;</td>
				  <td nowrap align="left" class="bordereporte"><%=(info.getValor_05()!=null)?info.getValor_05():""%>&nbsp;</td>
                  <td nowrap align="left" class="bordereporte"><%=(info.getValor_06()!=null)?info.getValor_06():""%>&nbsp;</td>
				  <td nowrap align="left" class="bordereporte"><%=(info.getValor_11()!=null)?info.getValor_11():""%>&nbsp;</td>
				  <td nowrap align="left" class="bordereporte"><%=(info.getValor_09()!=null)?info.getValor_09():""%>&nbsp;</td>
				  <td nowrap align="left" class="bordereporte"><%=(info.getValor_07()!=null)?info.getValor_07():""%>&nbsp;</td>
				  <td nowrap align="left" class="bordereporte"><%=(info.getValor_08()!=null)?info.getValor_08():""%>&nbsp;</td>
				  <td nowrap align="left" class="bordereporte"><%=(info.getValor_10()!=null)?info.getValor_10():""%>&nbsp;</td>
				  
				 </pg:item>
			 <%}%>
                </tr>
				  <tr>
				    <td colspan="13" align="left" nowrap class="bordereporte">
					<pg:index>
            <jsp:include page="/WEB-INF/jsp/googlesinimg.jsp" flush="true"/>      
           </pg:index> </td>
			    </tr>
				</pg:pager>         
            </table>
      </table></td>
      </tr>
		    </table>			
		   <form name="form1" method="post" action="<%=CONTROLLER%>?estado=Corridas&accion=Reporte&opcion=2">
              <p>
	    <input name="Corridas" type="hidden" id="Corridas" value="<%=model.corridaService.getCorrida()%>">
	    </p>
              <p>
	      <input name="Propietario" type="hidden" id="Propietario" value="<%=model.corridaService.getPropietario()%>">
              </p>
             
              <p>
                <input name="Guardar2" src="<%=BASEURL%>/images/botones/exportarExcel.gif" type="image" class="boton" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" style="cursor:hand" onclick="Exportar();">
                <img src="<%=BASEURL%>/images/botones/regresar.gif" name="c_regresar" onclick="location.href='<%=BASEURL%>/jsp/finanzas/reportes/ReporteCTransferencia.jsp'"  onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" style="cursor:hand">
 	            <input name="salir" src="<%=BASEURL%>/images/botones/salir.gif" type="image" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" style="cursor:hand" class="boton" onclick="window.close();">
                  </p>
		    </form>
	  <%}	  
	  }
	  %>	  
</body>
		  </div>
</html>