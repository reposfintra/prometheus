<%@ page session="true"%>
<%@page import="com.tsp.operation.model.beans.*,com.tsp.util.*"%>
<%@page import="java.util.*"%>
<%@ include file="/WEB-INF/InitModel.jsp"%>
<%@ taglib uri="http://jakarta.apache.org/taglibs/input-0.90" prefix="input" %>

<html>
<head>
    <title>Estados Financieros 2</title>
    <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
    <link href="/css/estilostsp.css" rel="stylesheet" type="text/css">
	<link href="<%=BASEURL%>/css/estilostsp.css" rel="stylesheet" type="text/css">
    <script language="JavaScript" type="text/javascript" src="<%=BASEURL%>/js/boton.js"></script>
	<%
		String mensaje = (request.getParameter("Mensaje")!=null)?request.getParameter("Mensaje"):"";
		int anio = Util.AnoActual();
    %>
</head>
<body>
<div id="capaSuperior" style="position:absolute; width:100%; height:100px; z-index:0; left: 1px; top: 0px;">
 <jsp:include page="/toptsp.jsp?encabezado=Estados Financieros"/>
</div>
<div id="capaCentral" style="position:absolute; width:100%; height:87%; z-index:0; left: 0px; top: 100px; overflow: scroll;">

<form name="forma" method="post" action="<%=CONTROLLER%>?estado=Finanzas&accion=Reportes&evento=ESTADO_FINANCIERO_2">



    <table width="650" border="2" align="center">
      <tr>
        <td>
          <table width='100%' align='center' class='tablaInferior'>
            <tr>
              <td class="barratitulo" colspan='2' >
                <table cellpadding='0' cellspacing='0' width='100%'>
                  <tr class="fila">
                    <td width="50%" align="left" class="subtitulo1">&nbsp;Generacion del Balance</td>
                    <td width="*"   align="left"><img src="<%= BASEURL %>/images/titulo.gif" width="32" height="20"  align="left"></td>
                  </tr>
              </table></td>
            </tr>
                    <tr class="fila">
                        <td width="40%">&nbsp;Periodo : </td>
                        <td><select id="mes" name="mes">
                            <option value="1">Ene</option>
                            <option value="2">Feb</option>
                            <option value="3">Mar</option>
                            <option value="4">Abr</option>
                            <option value="5">May</option>
                            <option value="6">Jun</option>
                            <option value="7">Jul</option>
                            <option value="8">Ago</option>
                            <option value="9">Sep</option>
                            <option value="10">Oct</option>
                            <option value="11">Nov</option>
                            <option value="12">Dic</option>
                            <option value="13">Fis</option>
                        </select>

                        <select id="anio" name="anio">
						<% for ( int i = 2007 ; i <= anio; i ++ ){%>
                        <option value="<%=i%>" <%if(i == anio){%>selected<%}%>><%=i%></option>
						<%}%>
                        </select>

                    </tr>
        </table></td>
      </tr>
    </table>
    <br>
	<table align="center">
<tr>
        <td colspan="2" nowrap align="center">
		  <img src="<%=BASEURL%>/images/botones/aceptar.gif" name="aceptar"  height="21" onClick="this.disabled=true;forma.submit()" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" style="cursor:hand">&nbsp;
		  <img src="<%=BASEURL%>/images/botones/salir.gif" name="mod"  height="21" onclick="window.close();" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" style="cursor:hand">
        </td>
      </tr>
</table>
    <br>
    <br>
    <table width="40%"  align="center">
      <tr>
        <td>
          <FIELDSET>
          <legend><span class="letraresaltada">Nota</span></legend>
          <table width="100%"  border="0" cellpadding="0" cellspacing="0" class="informacion">
            <tr>
              <td nowrap align="center">&nbsp; Para iniciar el proceso de generacion del estado financiero haga click en aceptar. </td>
            </tr>
          </table>
        </FIELDSET></td>
      </tr>
    </table>
  </center>
<br>
<%if(!mensaje.equals("")){%>
<table border="2" align="center">
  <tr>
    <td><table width="100%" border="1" align="center"  bordercolor="#F7F5F4" bgcolor="#FFFFFF">
        <tr>
          <td width="262" align="center" class="mensajes"><%=mensaje%></td>
          <td width="32" background="<%=BASEURL%>/images/cuadronaranja.JPG">&nbsp;</td>
          <td width="44">&nbsp;</td>
        </tr>
    </table></td>
  </tr>
</table>
<%}%>

</form>
</div>
</body>
</html>