  <!--
     - Author(s)       :      FERNEL VILLACOB DIAZ
     - Date            :      07/06/2006  
     - Copyright Notice:      TRANSPORTES SANCHEZ POLO S.A.
  -->
 <%--   - @(#)  
     - Description:  Vista  que muestra los  movimientos de cuentas auxiliar.
 --%>
<%@page session="true"%>
<%@page errorPage="../error/ErrorPage.jsp"%>
<%@page import="com.tsp.operation.model.beans.*, java.util.*, java.text.*"%>
<%@ include file="/WEB-INF/InitModel.jsp"%>
<%@ taglib uri="http://jakarta.apache.org/taglibs/input-0.90" prefix="input" %>
<%@page import="com.tsp.util.*"%>
<%@taglib uri="http://jsptags.com/tags/navigation/pager" prefix="pg" %>
<% Util u = new Util(); %>

<html>
<head>
        <title>Reporte de Corridas Transferencias</title>
        <link href="<%= BASEURL %>/css/estilostsp.css" rel="stylesheet" type="text/css"> 
        <script src='<%=BASEURL%>/js/date-picker.js'></script>
		<script language="JavaScript" type="text/javascript" src="<%=BASEURL%>/js/boton.js"></script>
		<script type='text/javascript' src="<%=BASEURL%>/js/validar.js"></script> 
		<link href="../../estilostsp.css" rel="stylesheet" type="text/css">
		<link href="<%=BASEURL%>/css/estilostsp.css" rel="stylesheet" type="text/css">
		 <script>
		 	function Exportar (){
				
			    alert( 
				
					'Puede observar el seguimiento del proceso en la siguiente ruta del menu:' + '\n' + '\n' +
					'SLT -> PROCESOS -> Seguimiento de Procesos' + '\n' + '\n' + '\n' +
					'Y puede observar el archivo excel generado, en la siguiente parte:' + '\n' + '\n' +
					'En la pantalla de bienvenida de la Pagina WEB,' + '\n' +
					'vaya a la parte superior-derecha,' + '\n' +
					'y presione un click en el icono que le aparece.'
					
				);
				
			}
		</script>
        
        
</head>
<body>



<%  String path    = application.getRealPath(model.menuService.getRealUrl(request.getRequestURI()));
    String datos[] = model.menuService.getContenidoMenu(BASEURL,request.getRequestURI(),path);    %>

     
<div id="capaSuperior" style="position:absolute; width:100%; height:100px; z-index:0; left: 0px; top: 0px;">
<jsp:include page="/WEB-INF/toptsp.jsp?encabezado=Consulta Corridas por Transferencia"/>
</div>

<div id="capaCentral" style="position:absolute; width:100%; height:85%; z-index:0; left: 0px; top: 100px; overflow: scroll;"> 
 <center>
 <%
	   String style = "simple";
	   String position =  "bottom";
       String index =  "center";
       int maxPageItems = 1;
       int maxIndexPages = 10;
	   String msg = request.getParameter("msg"); %>
  
 <%Vector datosCorridas =model.corridaService.getVector();
   Vector datosValor =model.corridaService.getVectorValor();
    String msj = (request.getParameter("msg")==null)?"":request.getParameter("msg");
    if( datosCorridas != null && datosCorridas.size() > 0 ){
      BeanGeneral beanGeneral = (BeanGeneral) datosCorridas.elementAt(0);
	  BeanGeneral beanValor= (BeanGeneral) datosValor.elementAt(0);
	  %>
	  <form aling= "left" name="form1" method="post" action="<%=CONTROLLER%>?estado=Corridas&accion=Reporte&opcion=2">
		  	<input  align="left" name="Corridas" type="hidden" id="Corridas" value="<%=beanGeneral.getValor_04()%>">
		  <input  name="Guardar22" src="<%=BASEURL%>/images/botones/exportarExcel.gif" type="image" class="boton" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" style="cursor:hand" onclick="Exportar();">
     		 <img src="<%=BASEURL%>/images/botones/regresar.gif" name="c_regresar" onclick="location.href='<%=BASEURL%>/jsp/finanzas/reportes/ReporteCTransferencia.jsp'" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" style="cursor:hand">
	  		<input name="salir2" src="<%=BASEURL%>/images/botones/salir.gif" type="image" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" style="cursor:hand" class="boton" onclick="window.close();">
  	</form>	
	   <pg:pager
         items="<%=datosCorridas.size()%>"
         index="<%= index %>"
         maxPageItems="<%= maxPageItems %>"
         maxIndexPages="<%= maxIndexPages %>"
         isOffset="<%= true %>"
         export="offset,currentPageNumber=pageNumber"
         scope="request">
	  
	  <%for (int i = offset.intValue(), l = Math.min(i + maxPageItems,datosCorridas.size()); i < l; i++)
         {
		 BeanGeneral info = (BeanGeneral) datosCorridas.elementAt(i);								
		%>		
	  <pg:item>
      
    
   
      <table width="98%" border="2" align="center">
       <tr>
          <td>        
			              
                <table width='100%' align='center' class='tablaInferior'>
                
                      <tr>
                            <td class="barratitulo" colspan='2' >
                                   <table cellpadding='0' cellspacing='0' width='100%'>
                                         <tr class="fila">
                                             <td width="50%" align="left" class="subtitulo1">&nbsp;<strong>Consulta Corridas Por Transferencias </strong></td>
                                             <td width="*"   align="left"><img src="<%= BASEURL %>/images/titulo.gif" width="32" height="20"  align="left"><%=datos[0]%></td>
                                        </tr>
                                   </table>   
                            </td>
                      </tr>
                     
                      <tr   class="letra">
                         <td colspan='2'>
                              <table cellpadding='0' cellspacing='0' width='100%'>
                                  <tr  class="letra">
                                       <td width='18%' class="fila" ><strong> Corridas </strong></td>
                                       <td width='82%' class="letra" ><%=info.getValor_04()%></td>
                                  </tr>                                 
                                       <tr><td width='18%' class="fila" ><strong> Propietario</strong></td>
                                       <td width='82%' class="letra"> <%=info.getValor_03()%> -  <%=info.getValor_01()%> </td>
                                       </tr>
                              </table>                         
                         </td>
              </table>
         </td>
        </tr>
          <tr>
                            <td class="barratitulo" colspan='2' >
                                   <table cellpadding='0' cellspacing='0' width='100%'>
                                         <tr class="fila">
                                             <td width="50%" align="left" class="subtitulo1"><strong>TRANSFERENCIAS</strong></td>
                                             <td width="*"   align="left"><img src="<%= BASEURL %>/images/titulo.gif" width="32" height="20"  align="left"></td>
                                        </tr>
                                   </table>   
                            </td>
        </tr>
                      
                      
                      
                      
                      <tr class="fila">
                              <td width='100%' colspan='2'>
                                      <table width='100%' border="1" bordercolor="#999999" bgcolor="#F7F5F4" align="center">
                                      
                                           
                                            <tr class="letra">
                                                  <td colspan='10' align='right'>&nbsp;</td>
                                        </tr>  
                                      
                                      
                                            <tr class="tblTitulo" >
                                                  <TH  width='7%'  title=''        nowrap style="font size:11; font weight: bold" >ITEM</TH>
                                                  <TH  width='9%'  title=''      nowrap style="font size:11; font weight: bold" >Nro FACTURA</TH>
                                                  <TH  width='10%' title=''          nowrap style="font size:11; font weight: bold" >BANCO</TH>
                                                  <TH  width='8%'  title=''       nowrap style="font size:11; font weight: bold" >Nro CUENTA </TH> 
												  <TH  width='8%'  title=''       nowrap style="font size:11; font weight: bold" >TIPO DE CUENTA </TH>               
                                                  <TH  width='15%' title='' nowrap style="font size:11; font weight: bold" >CEDULA DE LA CUENTA </TH>
                                                  <TH  width='5%'  title=''         nowrap style="font size:11; font weight: bold" >NOMBRE DE LA CUENTA</TH>               
                                                  <TH  width='10%' title=''       nowrap style="font size:11; font weight: bold" >VALOR</TH> 
                                                      <!-- Movimientos--> 
                                          
                                        <tr class="letra">
                                               <td colspan='8' nowrap style="font size:10" >&nbsp;</td>
                                        </tr> 
                                               
					                            <%
												  	for( int z = 0; z<info.getVec().size(); z++ ){
		 													BeanGeneral BeanEspecifico = (BeanGeneral) info.getVec().elementAt(z);								
		
														 %>
														  <tr class="<%=( BeanEspecifico.getValor_14().equals("0099-01-01 00:00:00") )?"filaroja":(z % 2 == 0 )?"filagris":"filaazul"%>" onMouseOver='cambiarColorMouse(this)'   style="cursor:hand"
												          onClick="window.open('<%=CONTROLLER%>?estado=Corridas&accion=Reporte&opcion=3&Tipo_factura=<%=BeanEspecifico.getValor_15()%>&Cedula=<%=BeanEspecifico.getValor_02()%>&Factura=<%=BeanEspecifico.getValor_05()%>' ,'M','status=yes,scrollbars=no,width=780,height=650,resizable=yes');">
												
													 	  
															  <td nowrap align="left" class="bordereporte"><%=(BeanEspecifico.getValor_13()!=null)?BeanEspecifico.getValor_13():""%>&nbsp;</td>
															  <td nowrap align="left" class="bordereporte"><%=(BeanEspecifico.getValor_05()!=null)?BeanEspecifico.getValor_05():""%>&nbsp;</td>
															  <td nowrap align="left" class="bordereporte"><%=(BeanEspecifico.getValor_04()!=null)?BeanEspecifico.getValor_04():""%>&nbsp;</td>
															  <td nowrap align="left" class="bordereporte"><%=(BeanEspecifico.getValor_07()!=null)?BeanEspecifico.getValor_07():""%>&nbsp;</td>
															  <%if (BeanEspecifico.getValor_09().equals("CC")) {
															  %>
															  		<td nowrap align="left" class="bordereporte">Cuenta Corriente&nbsp;</td>
															  <%}if (BeanEspecifico.getValor_09().equals("CA")) { %>
															  		<td nowrap align="left" class="bordereporte">Cuenta Ahorros&nbsp;</td><%
																 }if (BeanEspecifico.getValor_09().equals("")) { %> 	
																
																  <td nowrap align="left" class="bordereporte"><%=(BeanEspecifico.getValor_09()!=null)?BeanEspecifico.getValor_09():""%>&nbsp;</td>
																  <%}%>	
															  <td nowrap align="left" class="bordereporte"><%=(BeanEspecifico.getValor_08()!=null)?BeanEspecifico.getValor_08():""%>&nbsp;</td>
															  <td nowrap align="left" class="bordereporte"><%=(BeanEspecifico.getValor_10()!=null)?BeanEspecifico.getValor_10():""%>&nbsp;</td>
  														      <td nowrap align="right" class="bordereporte"><%=(BeanEspecifico.getValor_11()!=null)?Util.customFormat( Double.parseDouble( (String)BeanEspecifico.getValor_11()  ) ):""%>&nbsp;</td>
											
								</tr>
									<%}%>
							   <!-- Totales-->  
													

                                           <tr class="fila">
                                                  <td colspan='7'>&nbsp <strong>TOTAL</strong> <font style="font size:11; align : center"></font> </td>
                                                  <td align='right'  class="letra" title='Total saldo acumulado' >  <%= Util.customFormat( Double.parseDouble( (String)info.getValor_05()  ) ) %> </td>
                                           </tr>    
										                   
                          				 
                                      </table>
								  
                        </td>
                     </tr>
  </table> 
   </pg:item>              
  <%}%>	
  <table width="100%" border="0">
   <tr class="bordereporte">
          <td td height="20" colspan="10" nowrap align="center">
		   <pg:index>
            <jsp:include page="/WEB-INF/jsp/googlesinimg.jsp" flush="true"/>      
           </pg:index> 
	      </td>
        </tr>
  </table>        
  </pg:pager>     
  <!--botones -->   
    
   <form aling= "left" name="form1" method="post" action="<%=CONTROLLER%>?estado=Corridas&accion=Reporte&opcion=2">
		<input  align="left" name="Corridas" type="hidden" id="Corridas" value="<%=beanGeneral.getValor_04()%>">
		<input  name="Guardar22" src="<%=BASEURL%>/images/botones/exportarExcel.gif" type="image" class="boton" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" style="cursor:hand" onclick="Exportar();">
     		 <img src="<%=BASEURL%>/images/botones/regresar.gif" name="c_regresar" onclick="location.href='<%=BASEURL%>/jsp/finanzas/reportes/ReporteCTransferencia.jsp'" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" style="cursor:hand">
	  		<input name="salir2" src="<%=BASEURL%>/images/botones/salir.gif" type="image" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" style="cursor:hand" class="boton" onclick="window.close();">
  	</form>	
    <br>
   
	<%}%>   
 <br>
    <% if( ! msj.equals("") ){%>
          <table border="2" align="center">
                  <tr>
                    <td>
                        <table width="100%"  border="1" align="center"  bordercolor="#F7F5F4" bgcolor="#FFFFFF">
                              <tr>
                                    <td width="400" align="center" class="mensajes"><%=msj%></td>
                                    <td width="29" background="<%=BASEURL%>/images/cuadronaranja.JPG">&nbsp;</td>
                                    <td width="58">&nbsp; </td>
                              </tr>
                      </table>
                    </td>
                  </tr>
          </table>
          
          <!--botones -->   
		  <form aling= "left" name="form1" method="post" action="<%=CONTROLLER%>?estado=Corridas&accion=Reporte&opcion=2">
		  		<img src="<%=BASEURL%>/images/botones/regresar.gif" name="c_regresar" onclick="location.href='<%=BASEURL%>/jsp/finanzas/reportes/ReporteCTransferencia.jsp'" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" style="cursor:hand">
	  		<input name="salir2" src="<%=BASEURL%>/images/botones/salir.gif" type="image" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" style="cursor:hand" class="boton" onclick="window.close();">
  	</form>
      <%}%>
 
</div>
<%=datos[1]%>  
</body>
</html>
