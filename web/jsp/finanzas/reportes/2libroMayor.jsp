<!--
- Autor : Osvaldp P�rez Ferrer
- Date : 29 de Junio de 2006
- Copyrigth Notice : Fintravalores S.A. S.A
-->
<%--
-@(#)
--Descripcion : Pagina JSP, que sirve para generar el Reporte Libro Mayor
--%>

<%@page import="com.tsp.util.*"%>
<%@ include file="/WEB-INF/InitModel.jsp"%>
<%@ taglib uri="http://jakarta.apache.org/taglibs/input-0.90" prefix="input" %>
<%@page import="com.tsp.operation.model.*, com.tsp.operation.model.beans.*,java.util.*, java.text.*"%>

<%
    String mensaje = (String) request.getAttribute("mensaje");
    String path = application.getRealPath(model.menuService.getRealUrl(request.getRequestURI()));
    String datos[] = model.menuService.getContenidoMenu(BASEURL,request.getRequestURI(),path);
    int anio = Util.AnoActual();
    String mes = Util.mesFormat(Util.MesActual());
%>
<html>
<head>
    <title>Editor de Generaci�n de Reporte de Libro Mayor</title>
    <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">

    <script src="<%=BASEURL%>/js/utilidades.js"></script>
    <link href="../css/estilostsp.css" rel="stylesheet" type="text/css">
    <script type='text/javascript' src="<%= BASEURL %>/js/boton.js"></script>
    <link href="<%=BASEURL%>/css/estilostsp.css" rel="stylesheet" type="text/css">

</head>

<body>
    <div id="capaSuperior" style="position:absolute; width:100%; height:100px; z-index:0; left: 1px; top: 0px;">
        <jsp:include page="/toptsp.jsp?encabezado=Reporte de Libro Mayor"/>
    </div>
    <div id="capaCentral" style="position:absolute; width:100%; height:87%; z-index:0; left: 0px; top: 100px; overflow: scroll;">        
        <form id="form1" method="post" action="<%=CONTROLLERCONTAB%>?estado=Libro&accion=Mayor">
        <table width="50%"  border="2" align="center">
            <tr>
                <td><table width="100%"  border="1" align="center" class="tablaInferior">
                <tr>
                <td><table width="100%" border="0" cellpadding="0" cellspacing="0">
                <tr>
                <td height="22" colspan=2 class="subtitulo1"><div align="center" class="subtitulo1">
                    <strong>Libro Mayor</strong>                     
                </div></td>
                <td width="212" class="barratitulo">
                <img src="<%=BASEURL%>/images/titulo.gif" width="32" height="20" align="left">
                    <%=datos[0]%>
            </tr>
                </table>
                <table width="100%"  border="0" align="center" cellpadding="2" cellspacing="3" onclick="document.getElementById('noexiste').style.visibility='hidden';">
                
            
            
                    <tr class="fila">
                        <td width="31%">Periodo : </td>
                        <td><select id="mes" name="mes">
                            <option value="01">Ene</option>
                            <option value="02">Feb</option>
                            <option value="03">Mar</option>
                            <option value="04">Abr</option>
                            <option value="05">May</option>
                            <option value="06">Jun</option>
                            <option value="07">Jul</option>
                            <option value="08">Ago</option>
                            <option value="09">Sep</option>
                            <option value="10">Oct</option>
                            <option value="11">Nov</option>
                            <option value="12">Dic</option>
                        </select>
                        
                        <select id="anio" name="anio">
                        <option value="<%=String.valueOf(anio-1)%>"><%=String.valueOf(anio-1)%></option>
                        <option value="<%=String.valueOf(anio)%>" selected><%=String.valueOf(anio)%></option>
                        <option value="<%=String.valueOf(anio+1)%>"><%=String.valueOf(anio+1)%></option>
                        </select>
                        
                    </tr>
                  
                </table></td>
                </tr>
            </table></td>
            </tr>
        </table>
        <br>
        <div align="center">      
            <img id="baceptar" name="baceptar"  src="<%=BASEURL%>/images/botones/aceptar.gif" style="cursor:hand"  onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" onClick="form1.submit();">            
       
            <img src="<%=BASEURL%>/images/botones/salir.gif"  height="21" name="imgsalir"  onMouseOver="botonOver(this);" onClick="window.close();" onMouseOut="botonOut(this);" style="cursor:hand">            
        </div>
        </form>
        <br/>
	
        
	<div id="noexiste">
       <%if(mensaje != null){ %>
        
            <table border="2" align="center">
                <tr>                
                    <td height="45"><table width="410" height="41" border="1" align="center"  bordercolor="#F7F5F4" bgcolor="#FFFFFF">
                        <tr>
                            <td width="282" height="35" align="center" class="mensajes"><%=mensaje%></td>
                            <td width="28" background="<%=BASEURL%>/images/cuadronaranja.JPG">&nbsp;</td>
                            <td width="78">&nbsp;</td>
                        </tr>
                    </table></td>
                </tr>
            </table>        
               
       <%}%>     
       </div>
    	      
       
<%=datos[1]%>
</body>
</html>

<script>
form1.mes.value="<%=mes%>";
</script>
