<%@include file="/WEB-INF/InitModel.jsp"%>
<html>
<head>
<title>Descripcion Campos Tipo de Comprobantes y Series</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link href="/css/estilostsp.css" rel="stylesheet" type="text/css">
<link href="<%= BASEURL %>/css/estilostsp.css" rel='stylesheet'>
</head>

<body>
<br>


<table width="696"  border="2" align="center">
  <tr>
    <td width="811" >
      <table width="100%" border="0" align="center">
        <tr  class="subtitulo">
          <td height="24" colspan="2"><div align="center">Tipo de Comprobantes y Series </div></td>
        </tr>
        <tr class="subtitulo1">
          <td colspan="2">Tipo de Comprobantes y Series </td>
        </tr>
        <tr>
          <td width="149" class="fila">C&oacute;digo</td>
          <td width="525"  class="ayudaHtmlTexto">Campo donde se digita el c&oacute;digo del tipo de comprobante.</td>
        </tr>
        <tr>
          <td class="fila">C&oacute;digo Interno</td>
          <td  class="ayudaHtmlTexto">Campo donde se se selecciona el codigo de documento interno.</td>
        </tr>
        <tr>
          <td class="fila">Descripci&oacute;n</td>
          <td  class="ayudaHtmlTexto">Campo donde se digita la descripci&oacute;n. </td>
        </tr>
        <tr>
          <td class="fila"><div align="center">
            <input type="image" src = "<%=BASEURL%>/images/botones/buscar.gif" style="cursor:default ">
          </div></td>
          <td  class="ayudaHtmlTexto">Bot&oacute;n que inicia la b&uacute;squeda con los filtros digitados/seleccionados. </td>
        </tr>
        <tr>
          <td class="fila"><div align="center">
            <input type="image" src = "<%=BASEURL%>/images/botones/cancelar.gif" style="cursor:default ">
          </div></td>
          <td  class="ayudaHtmlTexto"> Bot&oacute;n que cancela todas los operaciones realizadas, y resetea la pagina llev&aacute;ndola a su estado inicial. </td>
        </tr>
        <tr>
          <td class="fila"><div align="center">
            <input type="image" src = "<%=BASEURL%>/images/botones/salir.gif" style="cursor:default ">
          </div></td>
          <td  class="ayudaHtmlTexto">Bot&oacute;n que cierra  la ventana. </td>
        </tr>
        
      </table>
    </td>
  </tr>
</table>
<p>&nbsp;</p>
<p>&nbsp;</p>

<p>&nbsp;</p>
<p>&nbsp; </p>
</body>
</html>
