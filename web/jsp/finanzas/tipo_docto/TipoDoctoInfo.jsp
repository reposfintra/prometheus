<!--
- Autor : Ing. Leonardo Parody Ponce
- Date  : 26 diciembre de 2005
- Copyrigth Notice : Fintravalores S.A. S.A
-->
<%--
-@(#)
--Descripcion : Pagina JSP, que sirve para Listar cuentas.
--%>
<%@page contentType="text/html"%>
<%@page session="true"%>
<%@page errorPage="/error/ErrorPage.jsp"%>
<%@page import="java.util.*"%>
<%@page import="com.tsp.finanzas.contab.model.*"%>
<%@page import="com.tsp.finanzas.contab.model.beans.*"%>
<%@page import="com.tsp.operation.model.*"%>
<%@page import="com.tsp.util.*"%>
<%@include file="/WEB-INF/InitModel.jsp"%>
<%@taglib uri="http://jsptags.com/tags/navigation/pager" prefix="pg" %>
<%String path = application.getRealPath(model.menuService.getRealUrl(request.getRequestURI()));
  String datos[] = model.menuService.getContenidoMenu(BASEURL,request.getRequestURI(),path);
%>
<html>
<head><title>Tipos de Comprobantes y Series</title> 
 <link href="<%= BASEURL %>/css/estilostsp.css" rel='stylesheet'>
<link href="/css/estilostsp.css" rel="stylesheet" type="text/css">
<script type='text/javascript' src="<%= BASEURL %>/js/reporte.js"></script>
<script type='text/javascript' src="<%= BASEURL %>/js/boton.js"></script>
<body onresize="redimensionar()" onload = 'redimensionar()'>
<div id="capaSuperior" style="position:absolute; width:100%; height:100px; z-index:0; left: 0px; top: 0px;">
 <jsp:include page="/toptsp.jsp?encabezado=Tipos de Comprobantes y Series"/>
</div>
<div id="capaCentral" style="position:absolute; width:100%; height:87%; z-index:0; left: 0px; top: 100px; overflow: scroll;">
<% 
    String style = "simple";
    String position =  "bottom";
    String index =  "center";
    int maxPageItems = 20;
    int maxIndexPages = 10;
	
    Vector info = (Vector) request.getAttribute("Info");
	
	if (info.size() > 0) {
%>
<br>
<table width="100%" border="2" align="center">
  <tr>
    <td height="162">
	  <table width="100%">
        <tr>
          <td width="50%" class="subtitulo1">Listado</td>
          <td width="50%" class="barratitulo"><img align="left" src="<%=BASEURL%>/images/titulo.gif" width="32" height="20"><%=datos[0]%></td>
        </tr>
      </table>
	  <table width="100%" border="1" cellpadding="4" cellspacing="1" bordercolor="#999999" bgcolor="#F7F5F4" align="center">
        <tr class="tblTitulo" align="center">
          <td nowrap ><div align="center">C&oacute;digo</div></td>
          <td nowrap ><div align="center">C&oacute;digo Interno </div></td>
		  <td nowrap ><div align="center">Descripci&oacute;n</div></td>
		  <td nowrap ><div align="center">Tercero</div></td>
		  <td nowrap >Diario</td>
		  <td nowrap ><div align="center">Maneja Serie </div></td>
		  <td nowrap ><div align="center">Serie Inicial </div></td>
		  <td nowrap ><div align="center">Serie Final </div></td>
		  <td nowrap ><div align="center">Serie Actual </div></td>
          <td nowrap ><div align="center">Longitud Serie </div></td>
        </tr>
    <pg:pager        
		items="<%=info.size()%>"
        index="<%= index %>"
        maxPageItems="<%= maxPageItems %>"
        maxIndexPages="<%= maxIndexPages %>"
        isOffset="<%= true %>"
        export="offset,currentPageNumber=pageNumber"
        scope="request">
<%
    for (int i = offset.intValue(), l = Math.min(i + maxPageItems, info.size()); i < l; i++){
        Tipo_Docto obj = (Tipo_Docto) info.get(i);
		
%>
        <pg:item>
        <tr class="<%=(i % 2 == 0 )?"filagris":"filaazul"%>" onMouseOver='cambiarColorMouse(this)' style="cursor:hand"  title="Lista de Paginas" 
		onClick="window.open('<%= CONTROLLERCONTAB %>?estado=TipoDocto&accion=Manager&opc=GET&code=<%= obj.getCodigo()%>','TCOMPROB','status=yes,scrollbars=yes,width=780,height=650,resizable=no');">
          <td><%= obj.getCodigo()%></td>
          <td><%= "[" + obj.getCodigo_interno() + "] " + obj.getDocument_name() %></td>
          <td><%= obj.getDescripcion() %></td>
          <td><%= obj.getTerceroN() %></td>
          <td><%= obj.getEsDiario() %></td>
          <td><%= obj.getManeja_serie() %></td>
          <td><%= obj.getSerie_ini() %></td>
          <td><%= obj.getSerie_fin() %></td>
          <td><%= obj.getSerie_act() %></td>
          <td><%= obj.getLong_serie() %></td>
        </tr>
      </pg:item>
<%  }%>
        <tr class="pie">
          <td td height="20" colspan="10" nowrap align="center">          <pg:index>
                <jsp:include page="/WEB-INF/jsp/googlesinimg.jsp" flush="true"/>
        </pg:index>
          </td>
        </tr>
    </pg:pager>        
      </table>	</td>
  </tr>  
</table>
<%} else {%>
    <table border="2" align="center">
      <tr>
        <td>
	      <table width="100%"  border="1" align="center"  bordercolor="#F7F5F4" bgcolor="#FFFFFF">
            <tr>
              <td width="229" align="center" class="mensajes">No se encontraron resultados.</td>
              <td width="29"><img src="<%=BASEURL%>/images/cuadronaranja.JPG"></td>
            </tr>
          </table>
	    </td>
      </tr>
    </table>
<%}%>
<p>
<center>
      <img src="<%=BASEURL%>/images/botones/regresar.gif" name="c_regresar" id="c_regresar" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" onClick="location.href='<%=BASEURL%>/jsp/finanzas/contab/tipo_docto/TipoDoctoSearch.jsp?'"  style="cursor:hand">
	  <img src="<%=BASEURL%>/images/botones/salir.gif" name="c_salir" onClick="parent.close();" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" style="cursor:hand">
</center>
</div>
<%=datos[1]%>
</body>
</html>
