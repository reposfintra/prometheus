<!--
- Autor : Ing. Andr�s Maturana De La Cruz
- Date : 8 de junio de 2006
- Copyrigth Notice : Fintravalores S.A. S.A
-->
<%--
-@(#)
--Descripcion : Pagina JSP, que sirve para ingresar en el archivo de presupuesto de gastos administartivos.
--%>

<%@page contentType="text/html"%>
<%@page session="true"%>
<%@page import="java.util.*"%>
<%@page import="com.tsp.operation.model.Model"%>
<%@page import="com.tsp.operation.model.beans.Usuario"%>
<%@page import="com.tsp.operation.controller.*"%>
<%@page import="com.tsp.operation.model.*"%>
<%@page import="com.tsp.util.*"%>
<%@include file="/WEB-INF/InitModel.jsp"%>
<%@ taglib uri="http://jakarta.apache.org/taglibs/input-0.90" prefix="input" %>
<%@page errorPage="/error/ErrorPage.jsp"%>
<%String path = application.getRealPath(model.menuService.getRealUrl(request.getRequestURI()));
  String datos[] = model.menuService.getContenidoMenu(BASEURL,request.getRequestURI(),path);
%>
<html>
<head>
<title>Tipos de Comprobantes y Series</title>
<link href="<%= BASEURL %>/css/estilostsp.css" rel='stylesheet'>
<link href="/css/estilostsp.css" rel="stylesheet" type="text/css">
<link href="/css/estilostsp.css" rel="stylesheet" type="text/css">
</head>
<script type='text/javascript' src="<%= BASEURL %>/js/validar.js"></script>
<script src='<%= BASEURL %>/js/date-picker.js'></script>
<script type='text/javascript' src="<%= BASEURL %>/js/boton.js"></script>
<script type='text/javascript' src="<%= BASEURL %>/js/funciones.js"></script>
<script>
	function habilitarSerie(obj){
		if( obj.value == 'S' ){
			forma.serie_ini.readOnly = false;
			forma.serie_fin.readOnly = false;
			forma.long_serie.readOnly = false;
		} else {
			forma.serie_ini.readOnly = true;
			forma.serie_fin.readOnly = true;
			forma.long_serie.readOnly = true;
			forma.serie_ini.value = '';
			forma.serie_fin.value = '';
			forma.long_serie.value = '';
		}
	}
	
	function validar(obj){
	
		if( obj.mserie.value == "S" ){
			if( isNaN(obj.serie_fin.value) ){
				alert('El valor del campo debe ser n�merico');
				obj.serie_fin.focus();
				return false;
			} else if ( isNaN(obj.serie_ini.value)) {
				alert('El valor del campo debe ser n�merico');
				obj.serie_ini.focus();
				return false;
			}  else if ( isNaN(obj.long_serie.value)) {
				alert('El valor del campo debe ser n�merico');
				obj.long_serie.focus();
				return false;
			}				
			
			var sfin = parseInt(obj.serie_fin.value);
			var sini = parseInt(obj.serie_ini.value);
			
			if( sfin<sini ){
				alert('El valor de la serie final no debe ser inferior al de la serie inicial.');
				obj.serie_fin.focus();
				return false;
			}
			
			if( CamposLlenos(obj) ) return true;
			else return false;
			
		} else {
			return CamposLlenos2(obj);
		}
	}
	
	function CamposLlenos(form){
		 for (i = 0; i < form.elements.length; i++){
			if (form.elements[i].value == "" && form.elements[i].name!="preano"
					&& form.elements[i].name!="premes"){
			  alert("No se puede procesar la informacion. Verifique que todos lo campos esten llenos.");
			  form.elements[i].focus();
			  return (false);
			}
		 }
		 
		 return true;
	}
	
	function CamposLlenos2(form){
		 for (i = 0; i < form.elements.length; i++){
			if (form.elements[i].value == "" && form.elements[i].name!="preano"
					&& form.elements[i].name!="premes"
					&& form.elements[i].name!="serie_ini"
					&& form.elements[i].name!="serie_fin"
					&& form.elements[i].name!="long_serie"){
			  alert("No se puede procesar la informacion. Verifique que todos lo campos esten llenos.");
			  form.elements[i].focus();
			  return (false);
			}
		 }
		 
		 return true;
	}
</script>
<body onresize="redimensionar()" onload = 'redimensionar(); Llenar(forma.ano,null);'>
<div id="capaSuperior" style="position:absolute; width:100%; height:100px; z-index:0; left: 0px; top: 0px;">
 <jsp:include page="/toptsp.jsp?encabezado=Tipos de Comprobantes y Series"/>
</div>
<div id="capaCentral" style="position:absolute; width:100%; height:87%; z-index:0; left: 0px; top: 100px; overflow: scroll;">
<%
  TreeMap cint = new TreeMap();
    
  cint = modelcontab.tipo_doctoSvc.getTreemap();
  cint.put(" TODOS","");
  
%>
<form name="forma" id="forma" method="post" action="<%=CONTROLLERCONTAB%>?estado=TipoDocto&accion=Manager&opc=SEARCH">
  <table width="726"  border="2" align="center" cellpadding="0" cellspacing="0">
    <tr>
      <td><table width="100%" class="tablaInferior">
        <tr>
          <td colspan="4" ><table width="100%"  border="0" cellpadding="0" cellspacing="1" class="barratitulo">
              <tr>
                <td width="50%" class="subtitulo1">Tipo de Comprobante</td>
                <td width="50%" class="barratitulo"><img align="left" src="<%=BASEURL%>/images/titulo.gif" width="32" height="20"><%=datos[0]%></td>
              </tr>
          </table></td>
        </tr>
		<tr class="fila">
		  <td width="22%"> C&oacute;digo</td>
		  <td width="12%" ><input name="code" type="text" id="code" size="8" maxlength="5"  onKeyPress="soloAlfa(event)"></td>
		  <td width="19%" >C&oacute;digo Interno</td>
		  <td width="47%" ><input:select name="cod_interno" attributesText="class=textbox" options="<%= cint%>" default=""/></td>
		</tr>
		<tr class="fila">
		  <td>Descripci&oacute;n</td>
		  <td colspan="3" ><input name="desc" type="text" id="desc2" size="50" maxlength="30"></td>
		  </tr>

      </table></td>
    </tr>
  </table>
  <br>
  <% if( request.getParameter("msg")!=null ){%>
  <p>
  <table border="2" align="center">
    <tr>
      <td><table width="100%"  border="1" align="center"  bordercolor="#F7F5F4" bgcolor="#FFFFFF">
          <tr>
            <td width="229" align="center" class="mensajes"><%=request.getParameter("msg").toString()%></td>
            <td width="29" background="<%=BASEURL%>/images/cuadronaranja.JPG">&nbsp;</td>
            <td width="58">&nbsp;</td>
          </tr>
      </table></td>
    </tr>
  </table>
  <p></p>
  <%} %>
  <br>
  <center>
    <img src="<%=BASEURL%>/images/botones/buscar.gif" name="c_aceptar" onClick="forma.submit();" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" style="cursor:hand"> 
	<img src="<%=BASEURL%>/images/botones/cancelar.gif" name="c_cancelar" onClick="forma.reset();" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" style="cursor:hand"> 
	<img src="<%=BASEURL%>/images/botones/salir.gif" name="c_salir" onClick="parent.close();" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" style="cursor:hand">
  </center>
</form>
</div>
<%=datos[1]%>
</body>
</html>

