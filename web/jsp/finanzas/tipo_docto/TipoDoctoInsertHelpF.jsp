<%@ include file="/WEB-INF/InitModel.jsp"%>

<HTML>
<HEAD>
<TITLE>Tipo de Impuestos </TITLE>
<META http-equiv=Content-Type content="text/html; charset=windows-1252">
<link href="/css/estilostsp.css" rel="stylesheet" type="text/css"> 
<link href="<%=BASEURL%>/css/estilostsp.css" rel="stylesheet" type="text/css"> 
</HEAD>
<BODY>  

  <table width="95%"  border="2" align="center">
    <tr>
      <td>
        <table width="100%" border="0" align="center">
          <tr  class="subtitulo">
            <td height="20"><div align="center">Tipo de Comprobantes y Series </div></td>
          </tr>
          <tr class="subtitulo1">
            <td>Descripci&oacute;n del funcionamiento del programa de Tipo de Comprobantes y Series  . </td>
          </tr>
          <tr>
            <td  height="18"  class="ayudaHtmlTexto">Formulario de Ingreso de Tipo de Comprobantes y Series . </td>
          </tr>
          <tr>
            <td height="18"  class="ayudaHtmlTexto"><div align="center"><img src="<%= BASEURL%>/images/ayuda/contabilidad/tipo_docto/Dibujo1.PNG"></div></td>
          </tr>
          <tr>
            <td  class="ayudaHtmlTexto">&nbsp;</td>
          </tr>
          <tr>
            <td  class="ayudaHtmlTexto"><div align="center"><img src="<%= BASEURL%>/images/ayuda/contabilidad/tipo_docto/Dibujo2.PNG"></div></td>
          </tr>
          <tr>
            <td  class="ayudaHtmlTexto">&nbsp;</td>
          </tr>
          <tr>
            <td  class="ayudaHtmlTexto"><div align="center"><img src="<%= BASEURL%>/images/ayuda/contabilidad/tipo_docto/Dibujo3.PNG"></div></td>
          </tr>
          <tr>
            <td  class="ayudaHtmlTexto">&nbsp;</td>
          </tr>
          <tr>
            <td  class="ayudaHtmlTexto">Al ingresar correctamente la informac&oacute;n el sistema indicar&aacute; que se ha ingresado exiosamente el tipo de comprobante. </td>
          </tr>
          <tr>
            <td  class="ayudaHtmlTexto"><div align="center"><img src="<%= BASEURL%>/images/ayuda/contabilidad/tipo_docto/Dibujo4.PNG"></div></td>
          </tr>
          <tr>
            <td  class="ayudaHtmlTexto">&nbsp;</td>
          </tr>
          <tr>
            <td  class="ayudaHtmlTexto">El sistema verifica q no se ingrese un registro con un c&oacute;digo existente.</td>
          </tr>
          <tr>
            <td  class="ayudaHtmlTexto"><div align="center"><img src="<%= BASEURL%>/images/ayuda/contabilidad/tipo_docto/Dibujo5.PNG"></div></td>
          </tr>
          <tr>
            <td  class="ayudaHtmlTexto">&nbsp;</td>
          </tr>
          <tr>
            <td  class="ayudaHtmlTexto">El sistema verifica que el c&oacute;digo interno no se asigne nuevamente al ingresar un nuevo tipo de comprobante no diario. </td>
          </tr>
          <tr>
            <td  class="ayudaHtmlTexto"><div align="center"><img src="<%= BASEURL%>/images/ayuda/contabilidad/tipo_docto/Dibujo7.PNG"></div></td>
          </tr>
          <tr>
            <td  class="ayudaHtmlTexto">&nbsp;</td>
          </tr>
          
          
          
      </table></td>
    </tr>
  </table>
</BODY>
</HTML>
