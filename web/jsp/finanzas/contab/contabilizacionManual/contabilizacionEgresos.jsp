<%@ page session="true"%>
<%@page import="com.tsp.operation.model.beans.*"%>
<%@page import="java.util.*"%>
<%@ include file="/WEB-INF/InitModel.jsp"%>
<%@ taglib uri="http://jakarta.apache.org/taglibs/input-0.90" prefix="input" %>

<html>
<head>
    <title>Contabilizacion Manual de Egresos de Anticipos</title>
    <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
    <link href="/css/estilostsp.css" rel="stylesheet" type="text/css">
	<link href="<%=BASEURL%>/css/estilostsp.css" rel="stylesheet" type="text/css">
    <script language="JavaScript" type="text/javascript" src="<%=BASEURL%>/js/boton.js"></script>
</head>
<body>
<div id="capaSuperior" style="position:absolute; width:100%; height:100px; z-index:0; left: 1px; top: 0px;">
 <jsp:include page="/toptsp.jsp?encabezado=Contabilizacion Manual de Egresos de Anticipos"/>
</div>
<div id="capaCentral" style="position:absolute; width:100%; height:87%; z-index:0; left: 0px; top: 100px; overflow: scroll;">

<form name="form2" method="post" action="controllercontab?estado=Contabilizacion&accion=Egresos">
<jsp:include page="/jsp/finanzas/contab/infoContab.jsp?tamanoTableEncabezado=500"/> 
  <center>
    <table width="500" border="2" align="center">
      <tr>
        <td>
          <table width='100%' align='center' class='tablaInferior'>
            <tr>
              <td class="barratitulo" colspan='2' >
                <table cellpadding='0' cellspacing='0' width='100%'>
                  <tr class="fila">
                    <td width="50%" align="left" class="subtitulo1">&nbsp;Proceso de Contabilizaci&oacute;n</td>
                    <td width="*"   align="left"><img src="<%= BASEURL %>/images/titulo.gif" width="32" height="20"  align="left"></td>
                  </tr>
              </table></td>
            </tr>
            <tr class="barratitulo">
              <td  class='informacion' height='60'> Permite realizar la contabilizaci&oacute;n de Egresos de anticipo, tomando todos los cheques de anticipos que no esten contabilizadas desde el dia anterior a la ejecuci&oacute;n del programa (ayer) hacia atras, y se contabilizar&aacute;n con el periodo al cual corresponda el dia anterior. </td>
            </tr>
        </table></td>
      </tr>
    </table>
    <br>
	<table align="center">
<tr>
        <td colspan="2" nowrap align="center">
		<%if(request.getParameter("msg")==null){%>
		  <img src="<%=BASEURL%>/images/botones/aceptar.gif" name="aceptar"  height="21" onClick="this.style.visibility='hidden';form2.submit()" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" style="cursor:hand">&nbsp;
		<%}%>		  
		  
		  <img src="<%=BASEURL%>/images/botones/salir.gif" name="mod"  height="21" onclick="window.close();" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" style="cursor:hand">
        </td>
      </tr>
</table>
    <br>
    <br>
    <table width="40%"  align="center">
      <tr>
        <td>
          <FIELDSET>
          <legend><span class="letraresaltada">Nota</span></legend>
          <table width="100%"  border="0" cellpadding="0" cellspacing="0" class="informacion">
            <tr>
              <td nowrap align="center">&nbsp; Para iniciar el proceso de contabilizacion haga click en aceptar. </td>
            </tr>
          </table>
        </FIELDSET></td>
      </tr>
    </table>
  </center>
<br>
<%if(request.getParameter("msg")!=null){%>
<table border="2" align="center">
  <tr>
    <td><table width="100%" border="1" align="center"  bordercolor="#F7F5F4" bgcolor="#FFFFFF">
        <tr>
          <td width="262" align="center" class="mensajes"><%=request.getParameter("msg")%> !</td>
          <td width="32" background="<%=BASEURL%>/images/cuadronaranja.JPG">&nbsp;</td>
          <td width="44">&nbsp;</td>
        </tr>
    </table></td>
  </tr>
</table>
<%}%>

</form>
</div>
</body>
</html>