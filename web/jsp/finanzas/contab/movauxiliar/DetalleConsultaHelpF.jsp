<%@ include file="/WEB-INF/InitModel.jsp"%>
<html>
<head>
  <title>Consulta Mov. Contable</title>
  <META http-equiv=Content-Type content="text/html; charset=windows-1252">
  <link href="<%=BASEURL%>/css/estilostsp.css" rel="stylesheet" type="text/css"> 
  <script type='text/javascript' src="<%= BASEURL %>/js/boton.js"></script>
</head>
<body>
<center>






<% String BASEIMG = BASEURL +"/images/ayuda/contabilidad/movauxiliar/"; %>

<table width="100%"  border="2" align="center">
    <tr>
      <td >
 
            <table width='99%' align="center" cellpadding='0' cellspacing='0'>
                 <tr class="subtitulo" ><td height="20"><div align="center">MANUAL CONSULTA DE MOVIMIENTO CONTABLE </div></td></tr>
                 <tr class="subtitulo1"> <td>Descripci&oacute;n del funcionamiento </td> </tr>

                 <tr>
                      <td  class="ayudaHtmlTexto" height='40'>
                         Permite mostrar el detalle del movimiento de la cuenta consultada. Presenta una vista con informaci�n de la cuenta
                         tal como lo indica la figura 1.
                      </td>
                 </tr>
                 
                 
                 <tr><td  align="center" > 
                     <br><br>
                     <img  src="<%= BASEIMG%>Detalle.JPG" >                  
                     <br>
                     <strong>Figura 1</strong>
                     <br><br>
                     <br><br>
                 </td>
                 </tr>
                 
                 
                 <tr>
                      <td  class="ayudaHtmlTexto" height='40'>
                         Permite exportar a Excel la consulta, para esto deber� dar cl�ck en el bot�n Exportar, el archivo generado lo podr� ver en
                         su directorio de usuario.
                      </td>
                 </tr>
                      
           </table>
            
      </td>
  </tr>
</table>
<br><br>
<img src="<%=BASEURL%>/images/botones/salir.gif"      name="exit"    height="21"  title='Salir'                onClick="parent.close();"                 onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" style="cursor:hand">          
   


</body>
</html>
