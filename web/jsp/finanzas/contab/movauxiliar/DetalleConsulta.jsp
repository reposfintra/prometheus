  <!--
     - Author(s)       :      FERNEL VILLACOB DIAZ
     - Date            :      07/06/2006  
     - Copyright Notice:      TRANSPORTES SANCHEZ POLO S.A.
  -->
 <%--   - @(#)  
     - Description:  Vista  que muestra los  movimientos de cuentas auxiliar.
 --%>
<%@page session   ="true"%> 
<%@page import    ="java.util.*" %>
<%@page errorPage ="/error/ErrorPage.jsp"%>
<%@page import    ="com.tsp.util.Util" %>
<%@include file   ="/WEB-INF/InitModel.jsp"%>
<!-- Para los botones -->
<%@ taglib uri="http://www.sanchezpolo.com/sot" prefix="tsp"%>



<html>
<head>
        <title>Movimientos Auxiliar</title>
        <link href="<%= BASEURL %>/css/estilostsp.css" rel="stylesheet" type="text/css"> 
        <script type='text/javascript' src="<%= BASEURL %>/js/boton.js"></script>
        
        <script>
            function desabilitar(element){
               element.innerHTML ='';
            }
        </script>
        
</head>
<body>



<%  String path    = application.getRealPath(model.menuService.getRealUrl(request.getRequestURI()));
    String datos[] = model.menuService.getContenidoMenu(BASEURL,request.getRequestURI(),path);    %>

     
<div id="capaSuperior" style="position:absolute; width:100%; height:100px; z-index:0; left: 0px; top: 0px;">
<jsp:include page="/WEB-INF/toptsp.jsp?encabezado=Consulta Movimientos de Cuentas Auxiliares"/>
</div>

<div id="capaCentral" style="position:absolute; width:100%; height:85%; z-index:0; left: 0px; top: 100px; overflow: scroll;"> 
 <center>
 
 
 
 <% List   listaCuentas = modelcontab.MovAuxiliarSvc.getListCuentas();
    String msj          = (request.getParameter("msj")==null)?"":request.getParameter("msj");
    if(listaCuentas.size()>0){
       Hashtable  cuenteDetalle  =  modelcontab.MovAuxiliarSvc.getCuentaPrevia();%>

      
      <jsp:include page="/jsp/finanzas/contab/infoContab.jsp?tamanoTableEncabezado=98p"/>
   
      <table width="98%" border="2" align="center">
       <tr>
          <td>        

               
                <table width='100%' align='center' class='tablaInferior'>
                
                      <tr>
                            <td class="barratitulo" colspan='2' >
                                   <table cellpadding='0' cellspacing='0' width='100%'>
                                         <tr class="fila">
                                             <td width="50%" align="left" class="subtitulo1">&nbsp;Consulta Movimientos Auxiliar</td>
                                             <td width="*"   align="left"><img src="<%= BASEURL %>/images/titulo.gif" width="32" height="20"  align="left"><%=datos[0]%></td>
                                        </tr>
                                   </table>   
                            </td>
                      </tr>
                      
                      
                      <tr   class="letra">
                         <td colspan='2'>
                              <table cellpadding='0' cellspacing='0' width='100%'>
                                  <tr  class="letra">
                                       <td width='15%' class="fila" > PERIODO CONSULTA</td>
                                       <td width='14%'> <%= modelcontab.MovAuxiliarSvc.getFechaIni() %> - <%= modelcontab.MovAuxiliarSvc.getFechaFin() %></td>                                 
                                       <td width='10%' class="fila" > DISTRITO </td>
                                       <td width='*'  >  <%= modelcontab.MovAuxiliarSvc.getDistrito() %> </td>
                                  </tr>
                              </table>                         
                         </td>
                      
                      
                       <tr  class="letra" valign='top'>
                           <td class="fila" width='15%' > INFORMACI�N DE LA CUENTA </td>
                           <td width='*' >  
                               <table cellpadding='0' cellspacing='0' width='100%' > 
                                  <tr   class="letra" valign='top'>
                                      <td width='50%'>
                                          <table  width='100%' align='center' class='tablaInferior'>
                                             <tr class="letra"><td width='30%'>CUENTA        </td><td width='*'>&nbsp<b><%= (String) cuenteDetalle.get("cuenta")            %></td></tr>
                                             <tr class="letra"><td            >NOMBRE CORTO  </td><td          >&nbsp<b><%= (String) cuenteDetalle.get("nombre_corto")      %></td></tr>
                                             <tr class="letra"><td            >NOMBRE LARGO  </td><td          >&nbsp<%= (String) cuenteDetalle.get("nombre_largo")      %></td></tr>                                             
                                             <tr class="letra"><td            >OBSERVACI�N   </td><td          >&nbsp<%= (String) cuenteDetalle.get("nombre_observacion")%></td></tr>                                             
                                          </table>
                                      </td>
                                      <td width='30%' style="font size:11">
                                          <% List arbol = (List) cuenteDetalle.get("arbol");
                                             for(int i=0;i<arbol.size();i++){
                                                 Hashtable padre  = (Hashtable)arbol.get(i);
                                                 String    cuenta = (String) padre.get("cuenta");
                                                 String    nombre = (String) padre.get("nombre_corto");%>
                                                 <b><%= cuenta %></b> - <%=nombre %> <br>  
                                           <%}%>                                         
                                      </td>
                                      <td width='*'>
                                            Mostra cuenta:
                                            <font id='listado'>
                                                <select title='Lista de cuentas de la consulta' style="width:100%" onchange="desabilitar(imprimir); desabilitar(excel); pantalla.innerHTML='Cargando cuenta.....';location.href='controllercontab?estado=Mov&accion=Auxiliar&evento=MOSTRAR&cuenta=' + this.value;">
                                                <% for(int i=0; i<listaCuentas.size();i++ ){
                                                      Hashtable tipo      = (Hashtable)listaCuentas.get(i);
                                                      String    cuenta    = (String) tipo.get("cuenta");
                                                      String    select    = (cuenta.equals(  (String) cuenteDetalle.get("cuenta")  ))?"selected":"";%>
                                                      <option  value='<%=cuenta%>' <%=select%> ><%=cuenta%></option>  
                                                <%}%>                                                   
                                                </select>
                                            </font>
                                      </td>
                                  </tr>
                               </table>
                           </td>
                      </tr>

                      
                      
                      <% if( !modelcontab.MovAuxiliarSvc.getAuxiliar().equals("") ){%>
                      <tr   class="letra">
                           <td class="fila" > AUXILIAR </td>
                           <td >   <%= modelcontab.MovAuxiliarSvc.getTipoAuxiliar()%> -  <%= modelcontab.MovAuxiliarSvc.getAuxiliar() %>  &nbsp&nbsp&nbsp&nbsp  <%= modelcontab.MovAuxiliarSvc.getNombreAuxiliar() %></td>
                      </tr>
                      <%}%>
                      
                      
                      
                      <tr>
                            <td class="barratitulo" colspan='2' >
                                   <table cellpadding='0' cellspacing='0' width='100%'>
                                         <tr class="fila">
                                             <td width="50%" align="left" class="subtitulo1">&nbsp;MOVIMIENTOS</td>
                                             <td width="*"   align="left"><img src="<%= BASEURL %>/images/titulo.gif" width="32" height="20"  align="left"></td>
                                        </tr>
                                   </table>   
                            </td>
                      </tr>
                      
                      
                      
                      
                      <tr class="fila">
                              <td width='100%' colspan='2'>
                                      <table width='100%' border="1" bordercolor="#999999" bgcolor="#F7F5F4" align="center">
                                      
                                           
                                            <tr class="letra">
                                                  <td colspan='9' align='right'>VALOR SALDO ANTERIOR&nbsp </td>
                                                  <td align='right' title='Valor del saldo anterior de la cuenta' >  <b><%= Util.customFormat( Double.parseDouble( (String)cuenteDetalle.get("saldoAnterior") )  ) %> </td>
                                            </tr>  
                                      
                                      
                                            <tr class="tblTitulo" >
                                                  <TH  width='7%'  title='Tipo de documento'        nowrap style="font size:11; font weight: bold" >TIPO DOC         </TH>
                                                  <TH  width='9%'  title='N�mero de documento'      nowrap style="font size:11; font weight: bold" >DOCUMENTO        </TH>
                                                  <TH  width='10%' title='Nit del tercero'          nowrap style="font size:11; font weight: bold" >TERCERO          </TH>
                                                  <TH  width='8%'  title='Fecha de documento'       nowrap style="font size:11; font weight: bold" >FECHA            </TH>               
                                                  <TH  width='25%' title='Descripci�n de documento' nowrap style="font size:11; font weight: bold" >DESCRIPCION      </TH>
                                                  <TH  width='5%'  title='Tipo de Auxiliar'         nowrap style="font size:11; font weight: bold" >TIPO AUX         </TH>               
                                                  <TH  width='10%' title='C�digo de Auxiliar'       nowrap style="font size:11; font weight: bold" >AUXILIAR         </TH> 
                                                  <TH  width='11%' title='Valor d�bito'             nowrap style="font size:11; font weight: bold" >MOV. DEBITO      </TH>               
                                                  <TH  width='11%' title='Valor cr�dito'            nowrap style="font size:11; font weight: bold" >MOV. CREDITO     </TH>          
                                                  <TH  width='*'   title='Saldo Acumulado'          nowrap style="font size:11; font weight: bold" >SALDO ACUM.      </TH>          
                                             </tr>

                                             
                                           <!-- Movimientos--> 
                                          <% List movimientos  = (List) cuenteDetalle.get("movimientos");
                                             String msjMov     = (movimientos.size()==0)?"No presenta registros en comprodet":"";
                                             String periodoAnt = "";
                                             for(int i=0;i<movimientos.size();i++){
                                                 Hashtable mov     = (Hashtable)movimientos.get(i);
                                                 String    periodo = (String) mov.get("periodo");  
                                                 int sw            = ( !periodo.equals(periodoAnt ) )?1: 0;
                                                 periodoAnt        = ( !periodo.equals(periodoAnt ) )?periodo: periodoAnt;
                                                 if(sw==1){%>
                                                    <tr class="letra">
                                                           <td colspan='10' nowrap style="font size:10" ><B>PERIODO : <%= periodoAnt  %> </td>
                                                    </tr> 
                                                <%}%>
                                                 
                                                 
                                                 
                                                 
                                                 
                                                 <tr class='<%= (i%2==0?"filagris":"filaazul") %>' style="cursor:hand; font size:12" onMouseOver='cambiarColorMouse(this)' onclick="window.open('<%=CONTROLLERCONTAB%>?estado=AdminComprobantes&accion=Manager&opc=ver&dstrct=<%= (String) mov.get("distrito")%>&tipodoc=<%=(String) mov.get("tipo_doc")%>&numdoc=<%=(String) mov.get("documento")%>&grupo=<%=(String)mov.get("grupo_transaccion")%>&fechaapply=<%= (String)mov.get("fecha")%>','Comprobante','HEIGHT=600,WIDTH=940,STATUS=YES,SCROLLBARS=YES,RESIZABLE=YES,top=0,left=50')" >
                                                         <td class="bordereporte" align='center' nowrap style="font size:11"> <%= (String) mov.get("tipo_doc")    %> </td>
                                                         <td class="bordereporte"                nowrap style="font size:11"> <%= (String) mov.get("documento")   %> </td>                                                         
                                                         <td class="bordereporte" align='right'  nowrap style="font size:11"> <%= (String) mov.get("tercero")     %> </td>
                                                         <td class="bordereporte" align='center' nowrap style="font size:11"> <%= (String) mov.get("fecha")       %> </td>
                                                         <td class="bordereporte"                nowrap style="font size:9" > <%= (String) mov.get("descripcion") %> </td>
                                                         <td class="bordereporte" align='center' nowrap style="font size:11"> <%= (String) mov.get("tipo_aux")    %> </td>
                                                         <td class="bordereporte" align='right'  nowrap style="font size:11" title='<%= (String) mov.get("nombre") %>'> <%= (String) mov.get("auxiliar")         %> </td>
                                                         <td class="bordereporte" align='right'  nowrap style="font size:10"> <%=  Util.customFormat( Double.parseDouble( (String) mov.get("vlrDebito") ) )      %> </td>
                                                         <td class="bordereporte" align='right'  nowrap style="font size:10"> <%=  Util.customFormat( Double.parseDouble( (String) mov.get("vlrCredito")) )      %> </td>
                                                         <td class="bordereporte" align='right'  nowrap style="font size:10"> <%=  Util.customFormat( Double.parseDouble( (String) mov.get("saldoAcumulado")) )  %> </td> 
                                                 </tr>    
                       
                                           <%}%>    
                                             
                                           <!-- Totales-->  
                                           <tr class="fila">
                                                  <td colspan='7'>&nbsp TOTAL <font style="font size:11; align : center"><%=msjMov%></font> </td>
                                                  <td align='right' title='Total d�bito'          > <b><font style="font size:10; align : center">  <%= Util.customFormat( Double.parseDouble( (String)cuenteDetalle.get("totalDebito") ) ) %> </td>                                                  
                                                  <td align='right' title='Total cr�dito'         > <b><font style="font size:10; align : center">  <%= Util.customFormat( Double.parseDouble( (String)cuenteDetalle.get("totalCredito")) ) %> </td>                                                  
                                                  <td align='right' title='Total saldo acumulado' > <b><font style="font size:10; align : center">  <%= Util.customFormat( Double.parseDouble( (String)cuenteDetalle.get("totalSaldo")  ) ) %> </td>
                                           </tr>                      
                          
                                      </table>
                               </td>
                     </tr>
                           
                     
               </table>               
               
          </td>
       </tr>
     </table>  
     
     
     
      <!--botones -->   
      <p>
           
           <font id='imprimir'> 
                   
           </font>          
           <font id='excel'>    <tsp:boton value="exportarExcel"    onclick="desabilitar(excel);    desabilitar(listado); desabilitar(imprimir); pantalla.innerHTML='Generaci�n a Excel ejecutandose.....'; location.href='controllercontab?estado=Mov&accion=Auxiliar&evento=EXCEL'"/> </font>
                                <tsp:boton value="regresar"         onclick="location.href='controllercontab?estado=Mov&accion=Auxiliar&evento=ATRAS'"/>                   
                                <tsp:boton value="salir"            onclick="parent.close();"/> 
      </p>
  
      
      
 <%}%>
 
    <font class='informacion' id='pantalla'></font>
    <br>
    <% if( ! msj.equals("") ){%>
          <table border="2" align="center">
                  <tr>
                    <td>
                        <table width="100%"  border="1" align="center"  bordercolor="#F7F5F4" bgcolor="#FFFFFF">
                              <tr>
                                    <td width="400" align="center" class="mensajes"><%=msj%></td>
                                    <td width="29" background="<%=BASEURL%>/images/cuadronaranja.JPG">&nbsp;</td>
                                    <td width="58">&nbsp; </td>
                              </tr>
                         </table>
                    </td>
                  </tr>
          </table>
          
          <!--botones -->   
         <% if(listaCuentas.size()==0){%>
          <p>
              <tsp:boton value="regresar"         onclick="location.href='controllercontab?estado=Mov&accion=Auxiliar&evento=ATRAS'"/>                   
              <tsp:boton value="salir"            onclick="parent.close();"/> 
          </p>
         <%}%>     
          
     <%}%>
 
      
    
      
 
     
     
 
</div>
<%=datos[1]%>  



</body>
</html>
