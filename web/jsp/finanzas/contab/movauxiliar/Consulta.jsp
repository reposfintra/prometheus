  <!--
     - Author(s)       :      FERNEL VILLACOB DIAZ
     - Date            :      07/06/2006  
     - Copyright Notice:      TRANSPORTES SANCHEZ POLO S.A.
  -->
 <%--   - @(#)  
     - Description:  Vista para permitir parametros de entradas para la consulta de
                     movimientos de cuentas auxiliar.
 --%>
<%@page session    ="true"%> 
<%@ page import    ="java.util.*" %>
<%@page errorPage  ="/error/ErrorPage.jsp"%>
<%@include file    ="/WEB-INF/InitModel.jsp"%>
<%@ page import    ="com.tsp.operation.model.beans.Usuario" %>
<!-- Para los botones -->
<%@ taglib uri="http://www.sanchezpolo.com/sot" prefix="tsp"%>



 
<html>
<head>
          <title>Consulta Mov Auxiliar</title>
          <link href="<%= BASEURL %>/css/estilostsp.css" rel="stylesheet" type="text/css"> 
          <script type='text/javascript' src="<%= BASEURL %>/js/boton.js"></script>
          <script type='text/javascript' src="<%= BASEURL %>/js/contabilidad.js"></script>
          
          
           <script>
            function addOption(Comb,valor,texto){
                var Ele = document.createElement("OPTION");
                Ele.value=valor;
                Ele.text=texto;
                Comb.add(Ele);
            }    
            function Llenar(CmbAnno, CmbMes){
                    var Meses    = new Array('Enero','Febrero','Marzo','Abril','Mayo','Junio','Julio','Agosto','Septiembre','Octubre','Noviembre','Diciembre');
                    var FechaAct = new Date();
                    CmbAnno.length = 0;
                    CmbMes.length  = 0;                
                    for (i=FechaAct.getYear()-10;i<=FechaAct.getYear()+10;i++) addOption(CmbAnno,i,i)
                    CmbAnno.value = FechaAct.getYear();
                    for (i=0;i<Meses.length;i++)
                        if ((i+1)<10) addOption(CmbMes,'0'+(i+1),Meses[i]);
                        else          addOption(CmbMes,(i+1),Meses[i]);                
                    CmbMes.value  = ((FechaAct.getMonth()+1)<10)?('0'+(FechaAct.getMonth()+1)):(FechaAct.getMonth()+1);	
            } 

            function validar(tform){
                var periodo1 = tform.Ano1.value + tform.Mes1.value;
                var periodo2 = tform.Ano2.value + tform.Mes2.value;
                if (parseInt(periodo1)>  parseInt(periodo2)){
                    alert ('El periodo final debe ser mayor o igual que el periodo inicial, por favor rectifique');
                    return false;
                }
                return true;
            }
            
            
            function send(theForm){
                  var sw = 0;
                  for(var i=0;i<theForm.length;i++){
                     var elem = theForm.elements[i];
                     if( elem.name=='txtTipo' ||  elem.name=='txtIdentificacion'  ){
                          if (  elem.name=='txtIdentificacion' &&  elem.value!=''   &&  theForm.txtTipo.value=='' ){
                                      alert('Si desea aplicar identificaci�n de auxiliar, deber� seleccionar su tipo');
                                      theForm.txtTipo.focus();
                                      sw=1;
                                      break;
                          }
                     }
                     else if ( elem.value=='' ){                         
                         alert('Deber� digitar el valor para el campo ' + elem.title );
                         elem.focus();
                         sw=1;
                         break; 
                     }
                 }
                 if(sw==0){
                    var fIni = theForm.txtAnoIni.value + theForm.txtMesIni.value;
                    var fFin = theForm.txtAnoFin.value + theForm.txtMesFin.value;
                      
                    if( parseInt(fFin) < parseInt(fIni) ){
                        alert('La fecha inicial deber� ser menor o igual a la final');
                        theForm.txtMesFin.focus();
                    }
                    else
                       theForm.submit();
                }
            }
           
        </script>   

        
</head>
<body>


<%  String path    = application.getRealPath(model.menuService.getRealUrl(request.getRequestURI()));
    String datos[] = model.menuService.getContenidoMenu(BASEURL,request.getRequestURI(),path);    %>

     
<div id="capaSuperior" style="position:absolute; width:100%; height:100px; z-index:0; left: 0px; top: 0px;">
<jsp:include page="/WEB-INF/toptsp.jsp?encabezado=Consulta Movimientos de Cuentas Auxiliares"/>
</div>

<div id="capaCentral" style="position:absolute; width:100%; height:85%; z-index:0; left: 0px; top: 100px; overflow: scroll;"> 
 <center>

  <%  Usuario usuario   = (Usuario) session.getAttribute("Usuario");
      String  distrito  = (String) session.getAttribute("Distrito");%>

   <form action='controllercontab?estado=Mov&accion=Auxiliar&evento=BUSCAR' method='post' name='formulario' onsubmit='return false;'> 
   
   
       <jsp:include page="/jsp/finanzas/contab/infoContab.jsp?tamanoTableEncabezado=500"/> 
   
       
       <table width="500" border="2" align="center">
       <tr>
          <td>   
          
                <table width='100%' align='center' class='tablaInferior'>
 
                      <tr>
                            <td class="barratitulo" colspan='2' >
                                   <table cellpadding='0' cellspacing='0' width='100%'>
                                         <tr class="fila">
                                             <td width="50%" align="left" class="subtitulo1">&nbsp;Parametros de Busquedas</td>
                                             <td width="*"   align="left"><img src="<%= BASEURL %>/images/titulo.gif" width="32" height="20"  align="left"><%=datos[0]%></td>
                                        </tr>
                                   </table>   
                            </td>
                      </tr>


                     <tr  class="fila">
                           <td width='23%' class="fila" > Distrito  </td>
                           <td>
                                <select name='txtDistrito' class="textbox" title='Distrito'>
                                      <option value='<%= distrito %>'><%= distrito %></option>
                                </select>                
                           </td>
                      </tr>


                      <tr  class="fila" >
                          <td > Cuentas  </td>
                     
                          <td  align='center'>
                                <table cellpadding='0' cellspacing='0' width='100%'>
                                      <tr   class="letra">
                                           
                                           <td  width='*'> <input type='text' name='cuentaIni' maxlength='25'  style='width:190' title='Cuenta Inicial'>
                                               <!-- <img src='<%=BASEURL%>/images/botones/iconos/lupa.gif' width="15" height="15"  style="cursor:hand"  title='Seleccionar Cuenta'   name='i_load'      onclick="treeViewCuentas(formulario.cuentaIni.name)" >  -->
                                           </td>
                                      </tr>
                                      <tr   class="letra">
                                           <td> <input type='text' name='cuentaFin'  maxlength='25' style='width:190' title='Cuenta Final'> 
                                                <!-- <img src='<%=BASEURL%>/images/botones/iconos/lupa.gif' width="15" height="15"   style="cursor:hand"  title='Seleccionar Cuenta'   name='i_load'      onclick="treeViewCuentas(formulario.cuentaFin.name)" > -->
                                           </td>
                                      </tr>
                                </table>
                          </td>
                      </tr>

                      
                      <tr  class="fila" >
                          <td > Periodo     </td>
                      
                          <td align='center'>
                           
                                   <table cellpadding='0' cellspacing='0' width='100%'>
                                      <tr   class="letra">                                            
                                           <td  > 
                                                <select name='txtAnoIni' class="textbox" title='A�o Inicial del periodo'></select> 
                                                <select name='txtMesIni' class="textbox" title='Mes Inicial del periodo'></select> 
                                           </td>
                                      
                                           <td> 
                                                <select  name='txtAnoFin' class="textbox" title='A�o Final del periodo'></select> 
                                                <select name='txtMesFin' class="textbox" title='Mes Final del periodo'></select> 
                                           </td>
                                     </tr>
                                </table>
                           
                          </td>
                      </tr>

                      
                      <tr  class="fila" >
                               <td  colspan='2'> Auxiliar   </td>
                       </tr>
    
                      <tr  class="fila" >
                          <td  colspan='2' align='center'>                          
                                       <table cellpadding='0' cellspacing='0' width='100%'>
                                              <tr   class='letra'>
                                                   <td width='23%' >&nbsp&nbsp&nbsp&nbsp&nbsp Tipo</td>
                                                   <td > 
                                                        <%  List    tiposAux  = modelcontab.MovAuxiliarSvc.getListTipoAuxiliar();  %>        
                                                            <select name='txtTipo' class='textbox' title='Tipo de identificaci�n de auxiliar'>
                                                                     <option value=''></option>          
                                                                    <% for(int i=0; i<tiposAux.size();i++ ){
                                                                           Hashtable tipo = (Hashtable)tiposAux.get(i);
                                                                           String code    = (String) tipo.get("codigo");%>
                                                                           <option value='<%=code%>'><%=code%></option>  
                                                                    <%}%>
                                                             </select> 
                                                   </td>
                                             </tr>
                                             <tr   class="letra">
                                                   <td>&nbsp&nbsp&nbsp&nbsp&nbsp N�mero</td>
                                                   <td> 
                                                        <input type='text' name='txtIdentificacion' title='Identificaci�n del auxiliar' maxlength='15' style='width:190' >   
                                                        <img src='<%=BASEURL%>/images/botones/iconos/lupa.gif' onclick="buscarLista('SQL_SUBLEDGER', formulario.txtTipo.value, formulario.txtIdentificacion.name,'LISTA DE AUXILIARES');"  width="15" height="15"  style="cursor:hand"  title='Seleccionar Auxiliar'   name='i_load'      onclick='' >
                                                   </td>
                                              </tr>
                                        </table>
                               </td>
                          </tr>
                      
                      

                </table>
            </td>
        </tr>
       </table>  
   </form>
   <br>
   
   
   <!--botones -->   
  <p>
        <tsp:boton value="buscar"    onclick="send( formulario )"/> 
        <tsp:boton value="cancelar"  onclick="location.href='controllercontab?estado=Mov&accion=Auxiliar&evento=ATRAS'"/>                   
        <tsp:boton value="salir"     onclick="parent.close();"/> 
  </p>
  <script> 
        Llenar(formulario.txtAnoIni,formulario.txtMesIni); 
        Llenar(formulario.txtAnoFin,formulario.txtMesFin);
        formulario.cuentaIni.focus();
  </script>

  
                                                   
</div>
<%=datos[1]%>  


</body>
</html>
