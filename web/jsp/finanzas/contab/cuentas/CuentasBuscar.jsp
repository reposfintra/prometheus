<!--
- Autor : Ing. Leonardo Parody Ponce
- Date  : 26 diciembre de 2005
- Copyrigth Notice : Fintravalores S.A. S.A
-->
<%--
-@(#)
--Descripcion : Pagina JSP, que sirve para buscar cuentas por codigo.
--%>
<%@page contentType="text/html"%>
<%@page session="true"%>
<%@page import="java.util.*"%>
<%@page import="com.tsp.operation.model.*"%>
<%@page import="com.tsp.operation.model.beans.*"%>
<%@page import="com.tsp.operation.controller.*"%>
<%@page import="com.tsp.util.*"%>
<%@include file="/WEB-INF/InitModel.jsp"%>
<%@ taglib uri="http://jakarta.apache.org/taglibs/input-0.90" prefix="input" %>
<html>
<head>
<title>Buscar Cuentas</title>
<link href="<%= BASEURL %>/css/estilostsp.css" rel='stylesheet'>
<link href="../../../../css/estilostsp.css" rel="stylesheet" type="text/css">
<link href="/css/estilostsp.css" rel="stylesheet" type="text/css">
</head>
<script type='text/javascript' src="<%= BASEURL %>/js/validar.js"></script>
<script src='<%= BASEURL %>/js/date-picker.js'></script>
<script type='text/javascript' src="<%= BASEURL %>/js/boton.js"></script>
<body onresize="redimensionar()" onload = 'redimensionar()'>
<div id="capaSuperior" style="position:absolute; width:100%; height:100px; z-index:0; left: 0px; top: 0px;">
 <jsp:include page="/toptsp.jsp?encabezado=Cuentas"/>
</div>
<div id="capaCentral" style="position:absolute; width:100%; height:87%; z-index:0; left: 0px; top: 100px; overflow: scroll;">
<form name="forma" id="forma" method="post">
  <table width="338" border="2" align="center" cellpadding="0" cellspacing="0">
    <tr>
      <td width="332" colspan="10">	  
	    <table width="100%" class="tablaInferior">
          <tr>
            <td colspan="10" >
              <table width="100%" class="barratitulo">
                <tr>
                  <td width="50%"class="subtitulo1" colspan="5">Buscar Cuentas </td>
                  <td width="50%" class="barratitulo" colspan="5"><img src="<%=BASEURL%>/images/titulo.gif" width="32" height="20"></td>
                </tr>
          	  </table>
		    </td>
          </tr>
		  <tr class="fila">
            <td>Inicio de Cuenta.
              <input name="iniciocuenta" type='text' class="textbox" id="iniciocuenta" style='width:120' onKeyPress="soloDigitos(event,'decNO')" value=''><img src="<%=BASEURL%>/images/botones/iconos/obligatorio.gif" width="10" height="10">
            </td>
          </tr>
        </table>	 
      </td>
    </tr>
  </table>
  <center>
    <p>
    <img src="<%=BASEURL%>/images/botones/buscar.gif" name="buscar" onClick="forma.action='<%=CONTROLLER%>?estado=Cuentas&accion=Listar';forma.submit();" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);">
    <img src="<%=BASEURL%>/images/botones/cancelar.gif" name="c_cancelar" onClick="forma.reset();" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);"> 
    <img src="<%=BASEURL%>/images/botones/salir.gif" name="c_salir" onClick="parent.close();" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);">
  </center>
</form>
<% if( !request.getParameter("msg").equals("") ){%>
<p>
<table border="2" align="center">
  <tr>
    <td>
	  <table width="100%"  border="1" align="center"  bordercolor="#F7F5F4" bgcolor="#FFFFFF">
        <tr>
          <td width="229" align="center" class="mensajes"><%=request.getParameter("msg").toString()%></td>
          <td width="29" background="<%=BASEURL%>/images/cuadronaranja.JPG">&nbsp;</td>
          <td width="58">&nbsp;</td>
        </tr>
      </table>
    </td>
  </tr>
</table>
</p>
 <%} %>
</div>
</body>
</html>

