<!--
- Autor : Ing. Leonardo Parody Ponce
- Date  : 26 diciembre de 2005
- Copyrigth Notice : Fintravalores S.A. S.A
-->
<%--
-@(#)
--Descripcion : Pagina JSP, que sirve para Listar cuentas.
--%>
<%@page contentType="text/html"%>
<%@page session="true"%>
<%@page errorPage="/error/ErrorPage.jsp"%>
<%@page import="java.util.*"%>
<%@page import="com.tsp.operation.model.*"%>
<%@page import="com.tsp.operation.model.beans.*"%>
<%@page import="com.tsp.util.*"%>
<%@include file="/WEB-INF/InitModel.jsp"%>
<%@taglib uri="http://jsptags.com/tags/navigation/pager" prefix="pg" %>
<html>
<head><title>Listar Cuentas</title> 
 <link href="<%= BASEURL %>/css/estilostsp.css" rel='stylesheet'>
<link href="/css/estilostsp.css" rel="stylesheet" type="text/css">
<script type='text/javascript' src="<%= BASEURL %>/js/reporte.js"></script>
<script type='text/javascript' src="<%= BASEURL %>/js/boton.js"></script>
<body onresize="redimensionar()" onload = 'redimensionar()'>
<div id="capaSuperior" style="position:absolute; width:100%; height:100px; z-index:0; left: 0px; top: 0px;">
 <jsp:include page="/toptsp.jsp?encabezado=Cuentas"/>
</div>
<div id="capaCentral" style="position:absolute; width:100%; height:87%; z-index:0; left: 0px; top: 100px; overflow: scroll;">
<% 
    String iniciocuenta = request.getParameter("iniciocuenta");
	List Cuentas = (List) request.getAttribute("Cuentas");
    String style = "simple";
    String position =  "bottom";
    String index =  "center";
    int maxPageItems = 20;
    int maxIndexPages = 10;
	
    if (Cuentas!=null){
       
    }
	else{
		Cuentas = model.cuentaService.ListarCuentas(request.getParameter("iniciocuenta"));   
    }
	if (Cuentas.size() > 0) {
%>
<br>
<table width="62%" border="2" align="center">
  <tr>
    <td height="162">
	  <table width="100%">
        <tr>
          <td width="373" class="subtitulo1">Cuentas</td>
          <td width="427" class="barratitulo"><img src="<%=BASEURL%>/images/titulo.gif" width="32" height="20"></td>
        </tr>
      </table>
	  <table width="100%" border="1" cellpadding="4" cellspacing="1" bordercolor="#999999" bgcolor="#F7F5F4" align="center">
        <tr class="tblTitulo" align="center">
          <td width="20" nowrap >Codigo</td>
          <td width="20" nowrap >Nombre Corto</td>
		  <td width="100" nowrap >Nombre Largo</td>
		  <td width="50" nowrap >Final de Periodo</td>
		  <td width="6" nowrap >Activa</td>
		  <td width="8" nowrap >Auxiliar</td>
		  <td width="7" nowrap >Modulo1</td>
		  <td width="7" nowrap >Modulo2</td>
		  <td width="7" nowrap >Modulo3</td>
		  <td width="7" nowrap >Modulo4</td>
		  <td width="7" nowrap >Modulo5</td>
		  <td width="7" nowrap >Modulo6</td>
		  <td width="7" nowrap >Modulo7</td>
		  <td width="7" nowrap >Modulo8</td>
		  <td width="7" nowrap >Modulo9</td>
		  <td width="8" nowrap >Modulo10</td>
		</tr>
    <pg:pager        
		items="<%=Cuentas.size()%>"
        index="<%= index %>"
        maxPageItems="<%= maxPageItems %>"
        maxIndexPages="<%= maxIndexPages %>"
        isOffset="<%= true %>"
        export="offset,currentPageNumber=pageNumber"
        scope="request">
<%
    for (int i = offset.intValue(), l = Math.min(i + maxPageItems, Cuentas.size()); i < l; i++){
        Cuentas cuenta = (Cuentas) Cuentas.get(i);%>
        <pg:item>
        <tr class="<%=(i % 2 == 0 )?"filagris":"filaazul"%>" onMouseOver='cambiarColorMouse(this)' style="cursor:hand"  title="Lista de Paginas" onClick="window.location.href='<%=CONTROLLER%>?estado=Cuentas&accion=Buscar&cuenta=<%=cuenta.getCuenta()%>&iniciocuenta=<%=iniciocuenta%>&listar=False','myWindow','status=no,scrollbars=no,width=450,height=190,resizable=yes';">
          <td><span class="style7"><%=cuenta.getCuenta()%></span></td>
          <td><span class="style7"><%=cuenta.getNombre_corto()%></span></td>
		  <td><span class="style7"><%=cuenta.getNombre_largo()%></span></td>
		  <td><span class="style7"><%=cuenta.getActiva()%></span></td>
          <td><span class="style7"><%=cuenta.getAuxiliar()%></span></td>
		  <td><span class="style7"><%=cuenta.getFin_de_periodo()%></span></td>
		  <td><span class="style7"><%=cuenta.getModulo1()%></span></td>
		  <td><span class="style7"><%=cuenta.getModulo2()%></span></td>
		  <td><span class="style7"><%=cuenta.getModulo3()%></span></td>
		  <td><span class="style7"><%=cuenta.getModulo4()%></span></td>
		  <td><span class="style7"><%=cuenta.getModulo5()%></span></td>
		  <td><span class="style7"><%=cuenta.getModulo6()%></span></td>
		  <td><span class="style7"><%=cuenta.getModulo7()%></span></td>
		  <td><span class="style7"><%=cuenta.getModulo8()%></span></td>
		  <td><span class="style7"><%=cuenta.getModulo9()%></span></td>
		  <td><span class="style7"><%=cuenta.getModulo10()%></span></td>
		</tr>
      </pg:item>
<%  }%>
        <tr class="pie">
          <td td height="20" colspan="10" nowrap align="center">          <pg:index>
                <jsp:include page="/WEB-INF/jsp/googlesinimg.jsp" flush="true"/>
        </pg:index>
          </td>
        </tr>
    </pg:pager>        
      </table>	</td>
  </tr>  
</table>
<%} else {%>
    <table border="2" align="center">
      <tr>
        <td>
	      <table width="100%"  border="1" align="center"  bordercolor="#F7F5F4" bgcolor="#FFFFFF">
            <tr>
              <td width="229" align="center" class="mensajes">No se encontraron cuentas relacionadas</td>
              <td width="29"><img src="<%=BASEURL%>/images/cuadronaranja.JPG"></td>
            </tr>
          </table>
	    </td>
      </tr>
    </table>
<%}%>
<p>
<center>
      <img src="<%=BASEURL%>/images/botones/regresar.gif" name="c_regresar" id="c_regresar" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" onClick="location.href='<%=BASEURL%>/jsp/cxpagar/cuentas/CuentasBuscar.jsp?msg='">
	  <img src="<%=BASEURL%>/images/botones/salir.gif" name="c_salir" onClick="parent.close();" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);">
</center>
</div>
</body>
</html>
