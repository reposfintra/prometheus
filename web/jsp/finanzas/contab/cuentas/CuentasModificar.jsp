<!--
- Autor : Ing. Leonardo Parody Ponce
- Date  : 26 diciembre de 2005
- Copyrigth Notice : Fintravalores S.A. S.A
-->
<%--
-@(#)
--Descripcion : Pagina JSP, que sirve para Modificar cuentas.
--%>
<%@page contentType="text/html"%>
<%@page session="true"%>
<%@page import="java.util.*"%>
<%@page import="com.tsp.operation.model.*"%>
<%@page import="com.tsp.operation.model.beans.*"%>
<%@page import="com.tsp.operation.controller.*"%>
<%@page import="com.tsp.util.*"%>
<%@include file="/WEB-INF/InitModel.jsp"%>
<%@ taglib uri="http://jakarta.apache.org/taglibs/input-0.90" prefix="input" %>
<html>
<head>
<title>Modificar Cuentas</title>
<link href="<%= BASEURL %>/css/estilostsp.css" rel='stylesheet'>
<link href="../../../../css/estilostsp.css" rel="stylesheet" type="text/css">
<link href="/css/estilostsp.css" rel="stylesheet" type="text/css">
</head>
<script type='text/javascript' src="<%= BASEURL %>/js/validar.js"></script>
<script src='<%= BASEURL %>/js/date-picker.js'></script>
<script type='text/javascript' src="<%= BASEURL %>/js/boton.js"></script>
<body onresize="redimensionar()" onload = 'redimensionar()'>
<div id="capaSuperior" style="position:absolute; width:100%; height:100px; z-index:0; left: 0px; top: 0px;">
 <jsp:include page="/toptsp.jsp?encabezado=Cuentas"/>
</div>
<div id="capaCentral" style="position:absolute; width:100%; height:87%; z-index:0; left: 0px; top: 100px; overflow: scroll;">
<%Cuentas cuenta = (Cuentas)request.getAttribute("cuenta");%>
<form name="forma" id="forma" method="post">
  <table width="680" border="2" align="center" cellpadding="0" cellspacing="0">
    <tr>
      <td >	  
	    <table width="100%" class="barratitulo">
          <tr>
            <td width="50%"class="subtitulo1" colspan="5">Ingresar cuentas</td>
            <td width="50%" class="barratitulo" colspan="5"><img src="<%=BASEURL%>/images/titulo.gif" width="32" height="20"></td>
          </tr>
        </table>
	    <table width="100%" class="tablainferior">
		  <tr class="fila">
            <td>Nro Cuenta.</td>
            <td nowrap colspan="6"><input name="cuenta" type='text' class="textbox" id="cuenta" style='width:120' value='<%=cuenta.getCuenta()%>' readonly>
            </td>
		  </tr>
          <tr class="fila">
            <td >Nombre Corto</td>
            <td nowrap colspan="2">
              <input name="nom_corto" type='text' class="textbox" id="nom_corto" style='width:120' value='<%=cuenta.getNombre_corto()%>'>
			</td>
		    <td colspan="2">Nombre Largo</td>
            <td nowrap colspan="2">
              <input name="nom_largo" type='text' class="textbox" id="nom_largo"  value='<%=cuenta.getNombre_largo()%>' size="55" maxlength="55">
			</td>
			<input name="iniciocuenta" type='text' class="textbox" id="iniciocuenta"  value='<%=request.getParameter("iniciocuenta")%>'  hydden>
          </tr>
          <tr class="fila">
            <td>final Periodo</td>
            <td>
              <select name="final_periodo" type='text' class="textbox" id="final_periodo" style='width:50' >              
			    <option <%if(cuenta.getFin_de_periodo().equalsIgnoreCase("A")){%> selected <%}%>>A</option>
                <option <%if(cuenta.getFin_de_periodo().equalsIgnoreCase("C")){%> selected <%}%>>C</option>
              </select>
            </td>
            <td>Auxiliar</td>
            <td>
              <select name="auxiliar" type='text' class="textbox" id="auxiliar" style='width:50' >
                <option <%if(cuenta.getAuxiliar().equalsIgnoreCase("A")){%> selected <%}%>>A</option>
                <option <%if(cuenta.getAuxiliar().equalsIgnoreCase("C")){%> selected <%}%>>C</option>
              </select>
            </td>
            <td>Activa</td>
            <td>
              <select name="activa" type='text' class="textbox" id="activa" style='width:50' >
                <option <%if(cuenta.getActiva().equalsIgnoreCase("S")){%> selected <%}%>>S</option>
                <option <%if(cuenta.getActiva().equalsIgnoreCase("N")){%> selected <%}%>>N</option>
              </select>
            </td>
          </tr>
		  <tr class="fila">
	        <td colspan="7">Observacion</td>
		   </tr>
		  <tr class="fila">
		  	 <td colspan="7">
			 <textarea name="observacion" cols="90" rows=""><%=cuenta.getObservacion()%></textarea>
			 </td>
		  </tr>
	    </table>
	    <table class="tablaInferior" width="100%">
          <tr>
            <td colspan="10" >
              <table width="100%" class="barratitulo">
                <tr>
                  <td width="50%" class="subtitulo1" colspan="3">&nbsp;Modulos Autorizados </td>
                  <td width="50%" class="barratitulo" colspan="3"><img src="<%=BASEURL%>/images/titulo.gif" width="32" height="20"></td>
                </tr>
          	  </table>
		    </td>
          </tr>
          <tr class="fila">
            <td width="69" height="26">Modulo 1</td>
            <td width="53" >
              <select name="Modulo1" type='text' class="textbox" id="Modulo1" style='width:50' >
                <option <%if(cuenta.getModulo1().equalsIgnoreCase("N")){%> selected <%}%>>N</option>
                <option <%if(cuenta.getModulo1().equalsIgnoreCase("S")){%> selected <%}%>>S</option>
              </select>
            </td>
            <td width="72">Modulo 2</td>
            <td width="53" >
              <select name="Modulo2" type='text' class="textbox" id="Modulo2" style='width:50' >
                <option <%if(cuenta.getModulo2().equalsIgnoreCase("N")){%> selected <%}%>>N</option>
                <option <%if(cuenta.getModulo2().equalsIgnoreCase("S")){%> selected <%}%>>S</option>
              </select>
            </td>
            <td width="68">Modulo 3</td>
            <td width="53" >
              <select name="Modulo3" type='text' class="textbox" id="Modulo3" style='width:50' >
                <option <%if(cuenta.getModulo3().equalsIgnoreCase("N")){%> selected <%}%>>N</option>
                <option <%if(cuenta.getModulo4().equalsIgnoreCase("S")){%> selected <%}%>>S</option>
              </select>
            </td>
		    <td width="71">Modulo 4</td>
            <td width="53" >
              <select name="Modulo4" type='text' class="textbox" id="Modulo4" style='width:50' >
                <option <%if(cuenta.getModulo4().equalsIgnoreCase("N")){%> selected <%}%>>N</option>
                <option <%if(cuenta.getModulo4().equalsIgnoreCase("S")){%> selected <%}%>>S</option>
              </select>
            </td>
		    <td width="79">Modulo 5</td>
            <td width="82" >
              <select name="Modulo5" type='text' class="textbox" id="Modulo5" style='width:50' >
                <option <%if(cuenta.getModulo5().equalsIgnoreCase("N")){%> selected <%}%>>N</option>
                <option <%if(cuenta.getModulo5().equalsIgnoreCase("S")){%> selected <%}%>>S</option>
              </select>
            </td>
          </tr>
		  <tr class="fila">
		    <td width="69" height="26">Modulo 6</td>
            <td width="53" >
              <select name="Modulo6" type='text' class="textbox" id="Modulo6" style='width:50' >
                <option <%if(cuenta.getModulo6().equalsIgnoreCase("N")){%> selected <%}%>>N</option>
                <option <%if(cuenta.getModulo6().equalsIgnoreCase("S")){%> selected <%}%>>S</option>
              </select>
            </td>
            <td width="72">Modulo 7</td>
            <td width="53" >
              <select name="Modulo7" type='text' class="textbox" id="Modulo7" style='width:50' >
                <option <%if(cuenta.getModulo7().equalsIgnoreCase("N")){%> selected <%}%>>N</option>
                <option <%if(cuenta.getModulo7().equalsIgnoreCase("S")){%> selected <%}%>>S</option>
              </select>
            </td>
            <td width="68">Modulo 8</td>
            <td width="53" >
              <select name="Modulo8" type='text' class="textbox" id="Modulo8" style='width:50' >
                <option <%if(cuenta.getModulo8().equalsIgnoreCase("N")){%> selected <%}%>>N</option>
                <option <%if(cuenta.getModulo8().equalsIgnoreCase("S")){%> selected <%}%>>S</option>
              </select>
            </td>
		    <td width="71">Modulo 9</td>
            <td width="53" >
              <select name="Modulo9" type='text' class="textbox" id="Modulo9" style='width:50' >
                <option <%if(cuenta.getModulo9().equalsIgnoreCase("N")){%> selected <%}%>>N</option>
                <option <%if(cuenta.getModulo9().equalsIgnoreCase("S")){%> selected <%}%>>S</option>
              </select>
            </td>
		    <td width="79">Modulo 10</td>
            <td width="82" >
              <select name="Modulo10" type='text' class="textbox" id="Modulo10" style='width:50' >
                <option <%if(cuenta.getModulo10().equalsIgnoreCase("N")){%> selected <%}%>>N</option>
                <option <%if(cuenta.getModulo10().equalsIgnoreCase("S")){%> selected <%}%>>S</option>
              </select>
            </td>
		  </tr>
        </table>	  </td>
    </tr>
  </table>
  <p>
  <center>
    <img src="<%=BASEURL%>/images/botones/modificar.gif" name="modificar" onClick="forma.action = '<%=CONTROLLER%>?estado=Cuentas&accion=Modificar'; forma.submit();" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);"> 
	<img src="<%=BASEURL%>/images/botones/anular.gif" name="anular" onClick="forma.action = '<%=CONTROLLER%>?estado=Cuentas&accion=Anular'; forma.submit();" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);"> 
	<img src="<%=BASEURL%>/images/botones/regresar.gif" name="regresar" onClick="forma.action='<%=CONTROLLER%>?estado=Cuentas&accion=Listar';forma.submit();" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);">
	<img src="<%=BASEURL%>/images/botones/salir.gif" name="c_salir" onClick="parent.close();" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);">
  </center>
</form>
<% if( !request.getParameter("msg").equals("") ){%>
<p>
<table border="2" align="center">
  <tr>
    <td>
	  <table width="100%"  border="1" align="center"  bordercolor="#F7F5F4" bgcolor="#FFFFFF">
        <tr>
          <td width="229" align="center" class="mensajes"><%=request.getParameter("msg").toString()%></td>
          <td width="29" background="<%=BASEURL%>/images/cuadronaranja.JPG">&nbsp;</td>
          <td width="58">&nbsp;</td>
        </tr>
      </table>
	</td>
  </tr>
</table>
</p>
<%} %>
</div>
</body>
</html>