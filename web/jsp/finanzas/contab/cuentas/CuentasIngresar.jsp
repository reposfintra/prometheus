<!--
- Autor : Ing. Leonardo Parody Ponce
- Date  : 26 diciembre de 2005
- Copyrigth Notice : Fintravalores S.A. S.A
-->
<%--
-@(#)
--Descripcion : Pagina JSP, que sirve para Ingresar cuentas.
--%>
<%@page contentType="text/html"%>
<%@page session="true"%>
<%@page import="java.util.*"%>
<%@page import="com.tsp.operation.model.*"%>
<%@page import="com.tsp.operation.model.beans.*"%>
<%@page import="com.tsp.operation.controller.*"%>
<%@page import="com.tsp.util.*"%>
<%@include file="/WEB-INF/InitModel.jsp"%>
<%@ taglib uri="http://jakarta.apache.org/taglibs/input-0.90" prefix="input" %>

<html>
<head>
<title>Ingresar Cuentas</title>
<link href="<%= BASEURL %>/css/estilostsp.css" rel='stylesheet'>
<link href="../../../../css/estilostsp.css" rel="stylesheet" type="text/css">
<link href="/css/estilostsp.css" rel="stylesheet" type="text/css">
</head>
<script type='text/javascript' src="<%= BASEURL %>/js/validar.js"></script>
<script src='<%= BASEURL %>/js/date-picker.js'></script>
<script type='text/javascript' src="<%= BASEURL %>/js/boton.js"></script>
<body onresize="redimensionar()" onload = 'redimensionar()'>
<div id="capaSuperior" style="position:absolute; width:100%; height:100px; z-index:0; left: 0px; top: 1px;">
<jsp:include page="/toptsp.jsp?encabezado=Cuentas"/></div>
<div id="capaCentral" style="position:absolute; width:100%; height:87%; z-index:0; left: -5px; top: 101px; overflow: scroll;">
<%TreeMap agencias = model.agenciaService.listar();%>
<form name="forma" id="forma" method="post">
  <table width="680" border="2" align="center" cellpadding="0" cellspacing="0">
    <tr>
      <td >	  
	    <table width="100%" class="barratitulo">
          <tr>
            <td width="50%"class="subtitulo1" colspan="5">Ingresar cuentas</td>
            <td width="50%" class="barratitulo" colspan="5"><img src="<%=BASEURL%>/images/titulo.gif" width="32" height="20"></td>
          </tr>
        </table>
	    <table width="100%" class="tablainferior">
          <tr class="fila">
            <td width="14%">Nro Cuenta :</td>
            <td colspan="6">
              <input name="cuenta" type='text' class="textbox" id="cuenta" style='width:120' onKeyPress="soloDigitos(event,'decNO')" value='' maxlength="14">
              <img src="<%=BASEURL%>/images/botones/iconos/obligatorio.gif" width="10" height="10"> </td>
          </tr>
          <tr class="fila">
            <td>Nombre Corto :</td>
            <td nowrap colspan="2">
              <input name="nom_corto" type='text' class="textbox" id="nom_corto" style='width:120' value='' maxlength="15">
              <img src="<%=BASEURL%>/images/botones/iconos/obligatorio.gif" width="10" height="10"></td>
            <td colspan="2" align="right">Nombre Largo : </td>
            <td width="43%" colspan="2" nowrap>
              <input name="nom_largo" type='text' class="textbox" id="nom_largo"  value='' size="55" maxlength="55">
              <img src="<%=BASEURL%>/images/botones/iconos/obligatorio.gif" width="10" height="10"></td>
          </tr>
          <tr class="fila">
            <td>final Periodo :</td>
            <td width="11%">
              <select name="final_periodo" type='text' class="textbox" id="final_periodo" style='width:50' >
                <option>A</option>
                <option>C</option>
              </select>
            </td>
            <td width="16%" align="right">Auxiliar :</td>
            <td width="10%">
              <select name="auxiliar" type='text' class="textbox" id="auxiliar" style='width:50' >
                <option>A</option>
                <option>C</option>
              </select>
            </td>
            <td width="6%" align="right">Activa :</td>
            <td>
              <select name="activa" type='text' class="textbox" id="activa" style='width:50' >
                <option>S</option>
                <option>N</option>
              </select>
            </td>
          </tr>
          <tr class="fila">
            <td colspan="2">Observacion</td>
			<td colspan="5">&nbsp;</td>
			</tr>
          <tr class="fila">
            <td colspan="7"><textarea name="observacion" cols="115" maxlength="200"></textarea></td>
          </tr>
        </table>
	    <table class="tablaInferior" width="100%">
          <tr>
            <td colspan="10" >
              <table width="100%" class="barratitulo">
                <tr>
                  <td width="50%" class="subtitulo1" colspan="3">&nbsp;Modulos Autorizados </td>
                  <td width="50%" class="barratitulo" colspan="3"><img src="<%=BASEURL%>/images/titulo.gif" width="32" height="20"></td>
                </tr>
          	  </table>
		    </td>
          </tr>
          <tr class="fila">
            <td width="66" height="26">Modulo 1</td>
            <td width="50" >
              <select name="Modulo1" type='text' class="textbox" id="Modulo1" style='width:50' >
                <option>N</option>
                <option>S</option>
              </select>
            </td>
            <td width="68">Modulo 2</td>
            <td width="50" >
              <select name="Modulo2" type='text' class="textbox" id="Modulo2" style='width:50' >
                <option>N</option>
                <option>S</option>
              </select>
            </td>
            <td width="65">Modulo 3</td>
            <td width="50" >
              <select name="Modulo3" type='text' class="textbox" id="Modulo3" style='width:50' >
                <option>N</option>
                <option>S</option>
              </select>
            </td>
		    <td width="67">Modulo 4</td>
            <td width="50" >
              <select name="Modulo4" type='text' class="textbox" id="Modulo4" style='width:50' >
                <option>N</option>
                <option>S</option>
              </select>
            </td>
		    <td width="75">Modulo 5</td>
            <td width="58" >
              <select name="Modulo5" type='text' class="textbox" id="Modulo5" style='width:50' >
                <option>N</option>
                <option>S</option>
              </select>
            </td>
          </tr>
		  <tr class="fila">
		    <td width="66" height="26">Modulo 6</td>
            <td width="50" >
              <select name="Modulo6" type='text' class="textbox" id="Modulo6" style='width:50' >
                <option>N</option>
                <option>S</option>
              </select>
            </td>
            <td width="68">Modulo 7</td>
            <td width="50" >
              <select name="Modulo7" type='text' class="textbox" id="Modulo7" style='width:50' >
                <option>N</option>
                <option>S</option>
              </select>
            </td>
            <td width="65">Modulo 8</td>
            <td width="50" >
              <select name="Modulo8" type='text' class="textbox" id="Modulo8" style='width:50' >
                <option>N</option>
                <option>S</option>
              </select>
            </td>
		    <td width="67">Modulo 9</td>
            <td width="50" >
              <select name="Modulo9" type='text' class="textbox" id="Modulo9" style='width:50' >
                <option>N</option>
                <option>S</option>
              </select>
            </td>
		    <td width="75">Modulo 10</td>
            <td width="58" >
              <select name="Modulo10" type='text' class="textbox" id="Modulo10" style='width:50' >
                <option>N</option>
                <option>S</option>
              </select>
            </td>
		  </tr>
        </table> 	  </td>
    </tr>
  </table>
  <br>
  <center>
    <img src="<%=BASEURL%>/images/botones/aceptar.gif" name="c_aceptar" onClick="return validarprecintos();soloDigitos(event,'decNO');" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);"> 
	<img src="<%=BASEURL%>/images/botones/cancelar.gif" name="c_cancelar" onClick="forma.reset();" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);"> 
	<img src="<%=BASEURL%>/images/botones/salir.gif" name="c_salir" onClick="parent.close();" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);">
  </center>
</form>
<% if( !request.getParameter("msg").equals("") ){%>
<p>
<table border="2" align="center">
  <tr>
    <td>
      <table width="100%"  border="1" align="center"  bordercolor="#F7F5F4" bgcolor="#FFFFFF">
        <tr>
          <td width="229" align="center" class="mensajes"><%=request.getParameter("msg").toString()%></td>
          <td width="29" background="<%=BASEURL%>/images/cuadronaranja.JPG">&nbsp;</td>
          <td width="58">&nbsp;</td>
        </tr>
      </table>
	</td>
  </tr>
</table>
</p>
 <%} %>
</div>
</body>
</html>
<script>
function validarprecintos(){
	if (document.forma.cuenta.value==''){
		alert('Escriba el Codigo de cuenta');
		return false;
	}else if (document.forma.nom_corto.value==''){
		alert('Especifique el nombre corto de la cuenta ');
		return false;	
	}else if (document.forma.nom_largo.value==''){
		alert('Especifique el nombre largo de la cuenta ');
		return false;	
	}else {
	        forma.action = '<%=CONTROLLER%>?estado=Cuentas&accion=Insert';
			forma.submit();
		}
	}	
}
</script>
