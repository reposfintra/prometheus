<%@ include file="/WEB-INF/InitModel.jsp"%>

<HTML>
<HEAD>
<TITLE>Descripci&oacute;n del funcionamiento del programa de Comprobantes de n&oacute;mina </TITLE>
<META http-equiv=Content-Type content="text/html; charset=windows-1252">
<link href="../../../../css/estilostsp.css" rel="stylesheet" type="text/css"> 
<link href="<%=BASEURL%>/css/estilostsp.css" rel="stylesheet" type="text/css"> 
<script type='text/javascript' src="<%=BASEURL%>/js/boton.js"></script>
</HEAD>
<BODY>  

  <table width="95%"  border="2" align="center">
    <tr>
      <td>
        <table width="100%" border="0" align="center">
          <tr  class="subtitulo">
            <td height="20"><div align="center">Interface Comprobantes N&oacute;mina </div></td>
          </tr>
          <tr class="subtitulo1">
            <td>Descripci&oacute;n del funcionamiento del programa de Comprobantes de n&oacute;mina </td>
          </tr>
          <tr>
            <td  height="18"  class="ayudaHtmlTexto">En este formulario se busca y selecciona el archivo que va a cargar el comprobante, se debe hacer click en el bot&oacute;n Examinar para buscar el directorio que contenga el archivo, finalmente se debe seleccionar el bot&oacute;n aceptar para enviar y cargar el archivo en el comprobante con su respectiva cabecera y detalle.</td>
          </tr>
          <tr>
            <td height="18"  class="ayudaHtmlTexto"><div align="center"><img src="<%=BASEURL%>/images/ayuda/contabilidad/ComprobanteNomina/img001.jpg" width="496" height="148"></div></td>
          </tr>
          <tr>
            <td  class="ayudaHtmlTexto">&nbsp;</td>
          </tr>
          <tr>
            <td  class="ayudaHtmlTexto">El archivo que se selecciona debe ser un txt, ya que estos son los par&aacute;metros que se requieren para poder procesar la informaci&oacute;n. Si el formato no es el indicado aparacer&aacute; un mensaje como el que se muestra a continuaci&oacute;n.</td>
          </tr>
          <tr>
            <td  class="ayudaHtmlTexto"><div align="center"><img src="<%=BASEURL%>/images/ayuda/contabilidad/ComprobanteNomina/img003.jpg" width="456" height="84"></div></td>
          </tr>
          <tr>
            <td  class="ayudaHtmlTexto">&nbsp;</td>
          </tr>
          <tr>
            <td  class="ayudaHtmlTexto"><p>Una vez se inicia el proceso, se despliega el siguiente mensaje de confirmaci&oacute;n:</p></td>
          </tr>
          <tr>
            <td  class="ayudaHtmlTexto"><div align="center"><img src="<%=BASEURL%>/images/ayuda/contabilidad/ComprobanteNomina/img002.jpg" width="444" height="74"></div></td>
          </tr>   
          
      </table></td>
    </tr>
  </table>
  <table width="416" align="center">
	<tr>
	<td align="center">
		<img src="<%=BASEURL%>/images/botones/salir.gif" style="cursor:pointer" name="c_salir" onClick="window.close();" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" align="absmiddle">
	</td>
	</tr>
</table>
</BODY>
</HTML>
