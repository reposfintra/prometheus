<%@ include file="/WEB-INF/InitModel.jsp" %>

<HTML>
<HEAD>
<TITLE>Manual de ingreso de interfaces contables</TITLE>
<META http-equiv=Content-Type content="text/html; charset=windows-1252">
<script type='text/javascript' src="<%=BASEURL%>/js/boton.js"></script>
<link href="../../../../css/estilostsp.css" rel="stylesheet" type="text/css"> 
<link href="<%=BASEURL%>/css/estilostsp.css" rel="stylesheet" type="text/css"> 
</HEAD>
<BODY> 
  <table width="95%"  border="2" align="center"> 
    <tr> 
      <td height="117" > 
        <table width="100%" border="0" align="center">
          <tr  class="subtitulo">
            <td height="20"><div align="center">INGRESAR INTERFACE CONTABLE </div></td>
          </tr>
          <tr class="subtitulo1">
            <td>Descripci&oacute;n del funcionamiento de ingreso de interface contable </td>
          </tr>
<tr>
            <td  class="ayudaHtmlTexto"><span class="ayudaHtmlTexto">En la siguiente pantalla se puede colocar el n&uacute;mero de la cuenta si se conoce, o utilizar la opci&oacute;n Consultar cuentas.</span></td>
          </tr>
<tr>
            <td  class="ayudaHtmlTexto"><div align="center"><IMG src="<%=BASEURL%>/images/ayuda/interfaceContable/img01.JPG" width="662" height="148"  
border=0 v:shapes="_x0000_i1143"></div></td>
          </tr>
<tr>
            <td  class="ayudaHtmlTexto"><span class="ayudaHtmlTexto">Si se ingresa por el link de Consultar cuentas, aparece la siguiente pantalla, en donde se coloca el nombre corto de la cuenta. Se puede utilizar el s&iacute;mbolo % como comod&iacute;n. Es decir que donde se coloque puede existir cualquier informaci&oacute;n adicional que lo reemplace.</span></td>
          </tr><tr>
            <td  class="ayudaHtmlTexto"><span class="ayudaHtmlTexto">Y se presiona BUSCAR</span></td>
          </tr>
<tr>
            <td  class="ayudaHtmlTexto"><div align="center"><IMG height=127 src="<%=BASEURL%>/images/ayuda/interfaceContable/img02.JPG" width=587 
border=0 v:shapes="_x0000_i1052"></div></td>
          </tr>
<tr>
            <td  class="ayudaHtmlTexto"><span class="ayudaHtmlTexto">En la pantalla aparecen los nombres de las cuentas que coincidan con la b&uacute;squeda realizada. Al nombre que se requiera, se le presiona CLICK</span></td>
          </tr>
<tr>
            <td  class="ayudaHtmlTexto"><div align="center"><IMG  src="<%=BASEURL%>/images/ayuda/interfaceContable/img03.JPG" width="543" height="183" 
border=0 v:shapes="_x0000_i1054"></div></td>
          </tr>
<tr>
            <td  class="ayudaHtmlTexto"><span class="ayudaHtmlTexto">En la siguiente pantalla se seleccionan los datos de las lista que se deseen agregar a la interface contable y finalmente de click en el bot&oacute;n aceptar.</span></td>
          </tr>
<tr>
            <td  class="ayudaHtmlTexto"><div align="center"><IMG src="<%=BASEURL%>/images/ayuda/interfaceContable/img04.JPG" width="509" height="92" 
border=0 v:shapes="_x0000_i1061"></div></td>
          </tr>
  </table>
<br>
<table width="416" align="center">
	<tr>
	<td align="center">
		<img src="<%=BASEURL%>/images/botones/salir.gif" style="cursor:pointer" name="c_salir" onClick="window.close();" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" align="absmiddle">
	</td>
	</tr>
</table>
</BODY>
</HTML>
