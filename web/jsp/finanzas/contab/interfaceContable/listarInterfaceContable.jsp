<%@page contentType="text/html"%>
<%@page session="true"%>
<%@page import="java.util.*"%>
<%@page import="com.tsp.finanzas.contab.model.*"%>
<%@page import="com.tsp.util.*"%>
<%@page import="com.tsp.finanzas.contab.model.beans.*, java.util.*"%>
<%@include file="/WEB-INF/InitModel.jsp"%>
<%@taglib uri="http://jsptags.com/tags/navigation/pager" prefix="pg" %>
<%
    String path = application.getRealPath(model.menuService.getRealUrl(request.getRequestURI()));
    String datos[] = model.menuService.getContenidoMenu(BASEURL,request.getRequestURI(),path);
%> 
<html>
<head>
    <title>Listado de Interfaces Contables</title>
    <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
    <link href="<%= BASEURL %>/css/estilostsp.css" rel="stylesheet" type="text/css">
    <script SRC='<%=BASEURL%>/js/boton.js'></script>
    <link href="../../../css/estilostsp.css" rel="stylesheet" type="text/css"> 
    <script type='text/javascript' src="<%= BASEURL %>/js/reporte.js"></script>
</head>

<body onload="window.moveTo(-4,-4);window.resizeTo(screen.availWidth + 8, screen.availHeight + 8);">
    <div id="capaSuperior" style="position:absolute; width:100%; height:100px; z-index:0; left: 0px; top: 0px;">
        <jsp:include page="/WEB-INF/toptsp.jsp?encabezado=Interface Contable"/>
    </div>
    <div id="capaCentral" style="position:absolute; width:100%; height:85%; z-index:0; left: 0px; top: 100px; overflow: scroll;">
    <p>&nbsp;</p>
    <p>
<%  
        String style = "simple";
        String position =  "bottom";
        String index =  "center";
        int maxPageItems = 15;
        int maxIndexPages = 10;
        Vector cmcdocs = modelcontab.cmcService.getVector();
        Cmc objCmc;

	if ( cmcdocs.size() > 0 ){  
%>
    </p>
    
    
        <table width="99%" border="2" align="center">
            <tr>
                <td>
                <table width="100%">
                    <tr>
                        <td width="373" class="subtitulo1">&nbsp;Datos de la Interface Contable</td>
                        <td class="barratitulo"><img src="<%=BASEURL%>/images/titulo.gif"align="left"><%=datos[0]%></td>
                    </tr>
                </table>
                <table width="100%" border="1" bordercolor="#999999" bgcolor="#F7F5F4">
                <tr class="tblTitulo">
                <td width="6%" align="center">Distrito</td>
                <td width="3%"  align="center">Tipo Documento</td>
                <td width="20%"  align="center">Descripci�n Documento</td>
                <td width="3%"  align="center">Cmc</td>
                <td width="30%"  align="center">Descripci�n Cmc</td>                
                <td width="18%" align="center">Cuenta</td>
                <td width="8%" align="center">Tipo Cuenta</td>
                <td width="12%" align="center">Tipo Transacci�n</td>                
            </tr>
                <pg:pager
                items="<%= cmcdocs.size()%>"
                index="<%= index %>"
                maxPageItems="<%= maxPageItems %>"
                maxIndexPages="<%= maxIndexPages %>"
                isOffset="<%= true %>"
                export="offset,currentPageNumber=pageNumber"
                scope="request">
                <%-- keep track of preference --%>
                <%
                      for (int i = offset.intValue(), l = Math.min(i + maxPageItems, cmcdocs.size()); i < l; i++)
                          {
                          objCmc = (Cmc) cmcdocs.elementAt( i );
                          String link = CONTROLLERCONTAB + "?estado=InterfaceContable&accion=Manager&opc=3&dstrct="+objCmc.getDstrct()+
                                                           "&tipodoc="+objCmc.getTipodoc()+"&cmc="+objCmc.getCmc();
                %>
                <pg:item>
                    <tr class="<%=(i % 2 == 0 )?"filagris":"filaazul"%>" onMouseOver='cambiarColorMouse(this)'   style="cursor:hand"
                    onClick="window.open('<%= link %>','','status=yes,scrollbars=yes,width=800,height=450,resizable=yes');" >
                    <td width="6%"  class="bordereporte"><%= objCmc.getDstrct() %>&nbsp;</td>
                    <td width="3%"  class="bordereporte"><%= objCmc.getTipodoc() %>&nbsp;</td>
                    <td width="20%"  class="bordereporte"><%= objCmc.getDescripcionTipodoc() %>&nbsp;</td>
                    <td width="3%"  class="bordereporte"><%= objCmc.getCmc() %>&nbsp;</td>
                    <td width="30%"  class="bordereporte"><%= objCmc.getDescripcionCmc() %>&nbsp;</td>
                    <td width="20%"  class="bordereporte"><%= objCmc.getCuenta() %>&nbsp;</td>
                    <td width="8%"  class="bordereporte"><%= (objCmc.getTipo_cuenta().equalsIgnoreCase("C"))? "Cuenta" : "Elemento" %>&nbsp;</td>
                    <td width="12%"  class="bordereporte"><%= (objCmc.getDbcr().equalsIgnoreCase("C"))? "Cr�dito" : "D�bito" %>&nbsp;</td>                    
                    </tr>
                </pg:item>
                <%}%>
                <tr   class="bordereporte">
                <td td height="20" colspan="8" nowrap align="center">
                <pg:index>
                    <jsp:include page="/WEB-INF/jsp/googlesinimg.jsp" flush="true"/>      
                </pg:index> 
            </td>
                    </tr>
                </pg:pager>
            </table></td>
            </tr>
        </table>
        <p>
              <%}
         else { %>
        </p>
        <table border="2" align="center">
            <tr>
                <td><table width="410" height="73" border="1" align="center"  bordercolor="#F7F5F4" bgcolor="#FFFFFF">
                    <tr>
                        <td width="282" align="center" class="mensajes">Su b&uacute;squeda no arroj&oacute; resultados!</td>
                        <td width="28" background="<%=BASEURL%>/images/cuadronaranja.JPG">&nbsp;</td>
                        <td width="78">&nbsp;</td>
                    </tr>
                </table></td>
            </tr>
        </table>
        <p>&nbsp; </p>
<%}%>
        <br>
        <table width="90%" border="0" align="center" cellpadding="0" cellspacing="0">
            <tr align="center">
                <td>
                    <img src="<%= BASEURL%>/images/botones/regresar.gif"  name="imgregresar"  onMouseOver="botonOver(this);" onClick="location.replace('<%=CONTROLLER%>?estado=Menu&accion=Cargar&carpeta=/jsp/finanzas/contab/interfaceContable&pagina=buscarInterfaceContable.jsp&marco=no&opcion=37')" onMouseOut="botonOut(this);"   style="cursor:hand ">
                    <img src="<%= BASEURL%>/images/botones/salir.gif"  name="imgsalir"  onMouseOver="botonOver(this);" onClick="parent.close();" onMouseOut="botonOut(this);"   style="cursor:hand ">
                </td>
            </tr>
        </table>
    </div>
    <%=datos[1]%>
</body>
</html>
