<%@page contentType="text/html"%>
<%@ page session="true"%>
<%@ page errorPage="../error/ErrorPage.jsp"%>
<%@page import="com.tsp.operation.model.beans.*, java.util.*"%>
<%@page import="com.tsp.operation.model.*"%>
<%@page import="com.tsp.finanzas.contab.model.*,com.tsp.finanzas.contab.model.beans.*"%>
<%@include file="/WEB-INF/InitModel.jsp"%>
<%@ taglib uri="http://jakarta.apache.org/taglibs/input-0.90" prefix="input" %>
<%
        //Se cargan las opciones de transaccion y tipo Cuenta
        TreeMap opc_tr = new TreeMap();
        opc_tr.put("D�bito","D");
        opc_tr.put("Cr�dito","C");
        opc_tr.put( "Todas", "" );
        
        TreeMap opc_tcuenta = new TreeMap();        
        opc_tcuenta.put("Elemento","E");
        opc_tcuenta.put("Cuenta","C");
        opc_tcuenta.put( "Todas", "" );
        
        TreeMap opc_cmc = modelcontab.cmcService.getTreemap();
        opc_cmc.put( "Todas", "" );
        
        TreeMap opc_docs = modelcontab.tipo_doctoSvc.getTreemap();
        opc_docs.put( "Todas", "" );
        
        String path = application.getRealPath(model.menuService.getRealUrl(request.getRequestURI()));
        String datos[] = model.menuService.getContenidoMenu(BASEURL,request.getRequestURI(),path);
%>
<html>
    <head>
        <title>Buscar interface contable</title>
        <link href="<%= BASEURL %>/css/estilostsp.css" rel="stylesheet" type="text/css">
        <link href="../../../css/estilostsp.css" rel="stylesheet" type="text/css">
        <script src='<%=BASEURL%>/js/boton.js'></script>
        <script language="javascript" src="<%=BASEURL%>/js/general.js"></script>
        <script language="javascript" src="<%=BASEURL%>/js/utilidades.js"></script>
    </head>
    <body>
        <div id="capaSuperior" style="position:absolute; width:100%; height:100px; z-index:0; left: 0px; top: 0px;">
            <jsp:include page="/WEB-INF/toptsp.jsp?encabezado=Interface Contable"/>
        </div>
        <div id="capaCentral" style="position:absolute; width:100%; height:85%; z-index:0; left: 0px; top: 100px; overflow: scroll;">
            <input type="hidden" name="baseurl" id="baseurl" value="<%=BASEURL%>" >
            <div id="working" class="letrasFrame" align="right" style="visibility:hidden"><img src="<%=BASEURL%>/images/cargando.gif">&nbsp;Cargando...&nbsp;&nbsp;</div>
            <br>
            <form name="form1" id="form1" method="post" action="<%=CONTROLLERCONTAB%>?estado=InterfaceContable&accion=Manager&opc=2" >
            <table width="650" border="2" align="center">
                <tr>
                    <td><table width="100%" align="center" cellpadding="3" cellspacing="2" class="tablaInferior">
                    <tr class="fila">
                        <td colspan="2" class="subtitulo1">Buscar Interface Contable</td>
                        <td colspan="2" class="barratitulo"><img src="<%=BASEURL%>//images/titulo.gif" align="left"><%=datos[0]%></td>
                    </tr>                    
                    <tr class="fila">
                        <td class="fila"><div align="left">CUENTA</div></td>
                        <td colspan="3" class="fila">
                            <input name="cuenta" type="text" class="textbox" id="cuenta" value="" onKeyPress="soloAlfa(event);" maxlength="25" >                                    
                        </td>
                    </tr>
                <tr class="fila">
                    <td width="120">Tipo de cuenta</td>
                    <td width="135">                        
                        <input:select name="tipo_cuenta"  options="<%= opc_tcuenta %>" attributesText="class='textbox' id='tipo_cuenta' style='width:120'" default=""/>
                    </td>
                    <td width="120">CMC</td>
                    <td>
                        <input:select name="cmc" options="<%= opc_cmc %>" attributesText="class='textbox' id='cmc' style='width:215'" default=""/>
                    </td>
                </tr>
                <tr class="fila">
                    <td>Tipo de transacci�n</td>
                    <td>
                        <input:select name="dbcr" options="<%= opc_tr %>" attributesText="class='textbox' id='dbcr' style='width:120'" default=""/>
                    </td>
                    <td width="120">Tipo de documento</td>
                    <td >
                        <input:select name="tipodoc" options="<%= opc_docs %>" attributesText="class='textbox' id='tipodoc' style='width:215'" default=""/>
                    </td>                
                </tr>                    
            </table>
            </td>
            </tr>
            </table>
            <div align="center"><br>
                <img src="<%= BASEURL%>/images/botones/buscar.gif"  name="imgaceptar" onClick="enviarPaginaForma('<%=CONTROLLERCONTAB%>?estado=InterfaceContable&accion=Manager&opc=2','form1');" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);"  style="cursor:hand ">&nbsp;                 
                <img src="<%= BASEURL%>/images/botones/salir.gif"  name="imgsalir"  onMouseOver="botonOver(this);" onClick="parent.close();" onMouseOut="botonOut(this);" style="cursor:hand ">
            </div>            
            </form>
            <div id="ResultadoFinal">
            </div>
        </div>
        <%=datos[1]%>
    </body>
</html>
