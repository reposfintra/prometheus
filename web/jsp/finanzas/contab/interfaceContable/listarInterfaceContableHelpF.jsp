<%@ include file="/WEB-INF/InitModel.jsp" %>

<HTML>
<HEAD>
<TITLE>Manual de para seleccion de datos y modificación</TITLE>
<META http-equiv=Content-Type content="text/html; charset=windows-1252">
<script type='text/javascript' src="<%=BASEURL%>/js/boton.js"></script>
<link href="../../../../css/estilostsp.css" rel="stylesheet" type="text/css"> 
<link href="<%=BASEURL%>/css/estilostsp.css" rel="stylesheet" type="text/css"> 
</HEAD>
<BODY> 
  <table width="95%"  border="2" align="center"> 
    <tr> 
      <td height="117" > 
        <table width="100%" border="0" align="center">
          <tr  class="subtitulo">
            <td height="20"><div align="center">LISTADO DE LA INTERFACE CONTABLE </div></td>
          </tr>
          <tr class="subtitulo1">
            <td>Descripci&oacute;n del funcionamiento del listado de la interface contable </td>
          </tr>
<tr>
            <td  class="ayudaHtmlTexto"><span class="ayudaHtmlTexto">En el siguiente listado aparecen el resultado de la consulta de la b&uacute;squeda de una interface contable. Se puede modificar cualquiera de los elementos haciendo click sobre cualquiera de ellos. </span></td>
          </tr>
<tr>
            <td  class="ayudaHtmlTexto"><div align="center"><IMG src="<%=BASEURL%>/images/ayuda/interfaceContable/img07.jpg" width="699" height="262"  
border=0 v:shapes="_x0000_i1143"></div></td>
          </tr>
<tr>
            <td  class="ayudaHtmlTexto"><span class="ayudaHtmlTexto">Al hacer lo anterior, aparece una ventana con el contenido presentado a continuaci&oacute;n</span></td>
          </tr>
<tr>
            <td  class="ayudaHtmlTexto"><div align="center"><IMG height=221 src="<%=BASEURL%>/images/ayuda/interfaceContable/img08.jpg" width=679 
border=0 v:shapes="_x0000_i1052"></div></td>
          </tr>
  </table>
<br>
<table width="416" align="center">
	<tr>
	<td align="center">
		<img src="<%=BASEURL%>/images/botones/salir.gif" style="cursor:pointer" name="c_salir" onClick="window.close();" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" align="absmiddle">
	</td>
	</tr>
</table>
</BODY>
</HTML>
