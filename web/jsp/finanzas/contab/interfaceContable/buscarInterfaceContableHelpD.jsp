<%@ include file="/WEB-INF/InitModel.jsp"%>

<html>
<head>
<title>Descripci&oacute;n de campos para la b&uacute;squeda de interfaces contables</title>

<script type='text/javascript' src="<%=BASEURL%>/js/boton.js"></script>
<link href="../../../../css/estilostsp.css" rel="stylesheet" type="text/css">
<link href="<%=BASEURL%>/css/estilostsp.css" rel="stylesheet" type="text/css"> 
</head>

<body>
<% String BASEIMG = BASEURL +"/images/botones/"; %> 
<br>
<table width="696"  border="2" align="center">
  <tr>
    <td width="811" >
      <table width="100%" border="0" align="center">
        <tr  class="subtitulo">
          <td height="24" colspan="2"><div align="center">BUSCAR INTERFACE CONTABLE </div></td>
        </tr>
        <tr class="subtitulo1">
          <td colspan="2">INFORMACI&Oacute;N DE LA INTERFACE CONTABLE </td>
        </tr>
        <tr>
          <td width="123" class="fila"> Cuenta </td>
          <td width="551"  class="ayudaHtmlTexto"> Campo de busqueda por la cuenta dada.</td>
        </tr>
        <tr>
          <td  class="fila">Tipo Cuenta</td>
          <td  class="ayudaHtmlTexto"> Lista de tipos de cuenta para el filtro por alguna de ellas, o todas. </td>
        </tr>
        <tr>
          <td width="123"  class="fila"> Tipo Transacci&oacute;n </td>
          <td width="551"  class="ayudaHtmlTexto">Lista de tipos de transacciones para la b&uacute;squeda por alguna de ellas. </td>
        </tr>
        <tr>
          <td width="123"  class="fila"> Cmc </td>
          <td width="551"  class="ayudaHtmlTexto">  Lista de C&oacute;digos de manejo contable.</td>
        </tr>
        <tr>
          <td width="123"  class="fila"> Tipo de documento</td>
          <td width="551"  class="ayudaHtmlTexto">Lista de tipos de documento.</td>
        </tr>				
		<tr>
          <td width="123"  class="fila"> Bot&oacute;n Buscar </td>
          <td width="551"  class="ayudaHtmlTexto">Bot&oacute;n que buscar las interfaces contables disponibles por los filtros dados. </td>
        </tr>
		<tr>
          <td width="123"  class="fila"> Bot&oacute;n salir </td>
          <td width="551"  class="ayudaHtmlTexto">Bot&oacute;n que cierra la ventana de b&uacute;squeda de interface contable. </td>
        </tr>		
      </table>
    </td>
  </tr>
</table>
<br>
<table width="416" align="center">
	<tr>
	<td align="center">
		<img src="<%=BASEURL%>/images/botones/salir.gif" style="cursor:pointer" name="c_salir" onClick="window.close();" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" align="absmiddle">
	</td>
	</tr>
</table>
<p>&nbsp;</p>

<p>&nbsp;</p>
<p>&nbsp; </p>
</body>
</html>
