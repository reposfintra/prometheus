<%@ include file="/WEB-INF/InitModel.jsp" %>

<HTML>
<HEAD>
<TITLE>Manual de b&uacute;squeda de interfaces contables</TITLE>
<META http-equiv=Content-Type content="text/html; charset=windows-1252">
<script type='text/javascript' src="<%=BASEURL%>/js/boton.js"></script>
<link href="../../../../css/estilostsp.css" rel="stylesheet" type="text/css"> 
<link href="<%=BASEURL%>/css/estilostsp.css" rel="stylesheet" type="text/css"> 
</HEAD>
<BODY> 
  <table width="95%"  border="2" align="center"> 
    <tr> 
      <td height="117" > 
        <table width="100%" border="0" align="center">
          <tr  class="subtitulo">
            <td height="20"><div align="center">BUSCAR INTERFACE CONTABLE </div></td>
          </tr>
          <tr class="subtitulo1">
            <td>Descripci&oacute;n del funcionamiento de b&uacute;squeda de interface contable </td>
          </tr>
<tr>
            <td  class="ayudaHtmlTexto"><span class="ayudaHtmlTexto">En la siguiente pantalla se selecciona los diferentes filtros por los cuales se desea hacer la consulta, si desea obtener todos las interfaces contables, se deja en blanco el campo &quot;cuenta&quot;, y en las lista se selcciona la opci&oacute;n &quot;todos&quot;.</span></td>
          </tr>
<tr>
            <td  class="ayudaHtmlTexto"><div align="center"><IMG src="<%=BASEURL%>/images/ayuda/interfaceContable/img05.JPG" width="674" height="178"  
border=0 v:shapes="_x0000_i1143"></div></td>
          </tr>
<tr>
            <td  class="ayudaHtmlTexto"><span class="ayudaHtmlTexto">Se presiona el bot&oacute;n &quot;Buscar&quot;, y a continuaci&oacute;n se muestra la lista con el resultado. </span></td>
          </tr>
<tr>
            <td  class="ayudaHtmlTexto"><div align="center"><IMG height=190 src="<%=BASEURL%>/images/ayuda/interfaceContable/img06.jpg" width=500 
border=0 v:shapes="_x0000_i1052"></div></td>
          </tr>
  </table>
<br>
<table width="416" align="center">
	<tr>
	<td align="center">
		<img src="<%=BASEURL%>/images/botones/salir.gif" style="cursor:pointer" name="c_salir" onClick="window.close();" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" align="absmiddle">
	</td>
	</tr>
</table>
</BODY>
</HTML>
