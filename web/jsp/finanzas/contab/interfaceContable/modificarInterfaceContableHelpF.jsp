<%@ include file="/WEB-INF/InitModel.jsp" %>

<HTML>
<HEAD>
<TITLE>Manual de modificaci&oacute;n de interfaces contables</TITLE>
<META http-equiv=Content-Type content="text/html; charset=windows-1252">
<script type='text/javascript' src="<%=BASEURL%>/js/boton.js"></script>
<link href="../../../../css/estilostsp.css" rel="stylesheet" type="text/css"> 
<link href="<%=BASEURL%>/css/estilostsp.css" rel="stylesheet" type="text/css"> 
</HEAD>
<BODY> 
  <table width="95%"  border="2" align="center"> 
    <tr> 
      <td height="117" > 
        <table width="100%" border="0" align="center">
          <tr  class="subtitulo">
            <td height="20"><div align="center">MODIFICAR INTERFACE CONTABLE </div></td>
          </tr>
          <tr class="subtitulo1">
            <td>Descripci&oacute;n del funcionamiento de la modificaci&oacute;n la de interface contable </td>
          </tr>
<tr>
            <td  class="ayudaHtmlTexto"> Despu&eacute;s seleccionar en el listado la interface contable deseada, es cargada la siguiente ventana con su contenido, se seleccionan y modifican los datos que deseen para finalmente, presionando el bot&oacute;n &quot;Modificar&quot;.</td>
          </tr>
<tr>
            <td  class="ayudaHtmlTexto"><div align="center"><IMG src="<%=BASEURL%>/images/ayuda/interfaceContable/img09.jpg" width="684" height="252"  
border=0 v:shapes="_x0000_i1143"></div></td>
          </tr>
<tr>
            <td  class="ayudaHtmlTexto"><span class="ayudaHtmlTexto">Una vez modificado se ver&aacute; el siguiente mensaje. </span></td>
          </tr>
<tr>
            <td  class="ayudaHtmlTexto"><div align="center"><IMG height=74 src="<%=BASEURL%>/images/ayuda/interfaceContable/img10.jpg" width=511 
border=0 v:shapes="_x0000_i1052"></div></td>
          </tr>
<tr>
            <td  class="ayudaHtmlTexto"><span class="ayudaHtmlTexto">Otra opci&oacute;n importante es la &quot;Anulaci&oacute;n&quot; de la interface contable. Para anular una interface contable solo es presionar el bot&oacute;n &quot;Anular&quot;, que se encuentra en la ventana modificar. </span></td>
          </tr>
<tr>
            <td  class="ayudaHtmlTexto"><div align="center"><IMG  src="<%=BASEURL%>/images/ayuda/interfaceContable/img11.jpg" width="679" height="221" 
border=0 v:shapes="_x0000_i1054"></div></td>
          </tr>
<tr>
            <td  class="ayudaHtmlTexto"><span class="ayudaHtmlTexto">Finalmente mostrar&aacute; un mensaje indicando la anulaci&oacute;n de la interface contable. </span></td>
          </tr>
<tr>
            <td  class="ayudaHtmlTexto"><div align="center"><IMG src="<%=BASEURL%>/images/ayuda/interfaceContable/img12.jpg" width="484" height="80" 
border=0 v:shapes="_x0000_i1061"></div></td>
          </tr>
  </table>
<br>
<table width="416" align="center">
	<tr>
	<td align="center">
		<img src="<%=BASEURL%>/images/botones/salir.gif" style="cursor:pointer" name="c_salir" onClick="window.close();" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" align="absmiddle">
	</td>
	</tr>
</table>
</BODY>
</HTML>
