<%@ include file="/WEB-INF/InitModel.jsp"%>

<html>
<head>
<title>Descripci&oacute;n de campos de ingreso de interface contable</title>

<script type='text/javascript' src="<%=BASEURL%>/js/boton.js"></script>
<link href="../../../../css/estilostsp.css" rel="stylesheet" type="text/css"> 
<link href="<%=BASEURL%>/css/estilostsp.css" rel="stylesheet" type="text/css"> 
</head>

<body>
<% String BASEIMG = BASEURL +"/images/botones/"; %> 
<br>
<table width="696"  border="2" align="center">
  <tr>
    <td width="811" >
      <table width="100%" border="0" align="center">
        <tr  class="subtitulo">
          <td height="24" colspan="2"><div align="center">INGRESAR INTERFACE CONTABLE </div></td>
        </tr>
        <tr class="subtitulo1">
          <td colspan="2">INFORMACI&Oacute;N DE LA INTERFACE CONTABLE </td>
        </tr>
        <tr>
          <td width="123" class="fila"> Cuenta </td>
          <td width="551"  class="ayudaHtmlTexto"> Campo para el ingreso de la cuenta, debe existir en la lista de cuentas.</td>
        </tr>
        <tr>
          <td  class="fila"> Consultar cuentas...</td>
          <td  class="ayudaHtmlTexto"> Link que muestra una pantalla para la busqueda de cuentas. </td>
        </tr>
        <tr>
          <td  class="fila">Tipo Cuenta</td>
          <td  class="ayudaHtmlTexto"> Lista de tipos de cuenta, Cuenta o Elemento.</td>
        </tr>
        <tr>
          <td width="123"  class="fila"> Tipo Transacci&oacute;n </td>
          <td width="551"  class="ayudaHtmlTexto">Lista de tipos de transacciones, D&eacute;bito o Cr&eacute;dito.</td>
        </tr>
        <tr>
          <td width="123"  class="fila"> Cmc </td>
          <td width="551"  class="ayudaHtmlTexto">  Lista de C&oacute;digos de manejo contable.</td>
        </tr>
        <tr>
          <td width="123"  class="fila"> Tipo de documento</td>
          <td width="551"  class="ayudaHtmlTexto">Lista de tipos de documento.</td>
        </tr>				
		<tr>
          <td width="123"  class="fila"> Bot&oacute;n Aceptar </td>
          <td width="551"  class="ayudaHtmlTexto">Bot&oacute;n que permite agregar la interface contable . </td>
        </tr>
		<tr>
          <td width="123"  class="fila"> Bot&oacute;n cancelar</td>
          <td width="551"  class="ayudaHtmlTexto">Bot&oacute;n que permite resetear el formulario.</td>
        </tr>
		<tr>
          <td width="123"  class="fila"> Bot&oacute;n salir </td>
          <td width="551"  class="ayudaHtmlTexto">Bot&oacute;n que cierra la ventana de inserci&oacute;n de interface contable. </td>
        </tr>		
      </table>
    </td>
  </tr>
</table>
<br>
<table width="416" align="center">
	<tr>
	<td align="center">
		<img src="<%=BASEURL%>/images/botones/salir.gif" style="cursor:pointer" name="c_salir" onClick="window.close();" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" align="absmiddle">
	</td>
	</tr>
</table>
<p>&nbsp;</p>

<p>&nbsp;</p>
<p>&nbsp; </p>
</body>
</html>
