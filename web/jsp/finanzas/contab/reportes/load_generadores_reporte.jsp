 <!--
     - Author(s)       :      Osvaldo P�rez Ferrer
     - Date            :      17-01-2007
     - Copyright Notice:      TRANSPORTES SANCHEZ POLO S.A.
  -->
 <%--   - @(#)  
     - Description: Vista que permite subir excel's para reportes contables
 --%>





<%@page session="true"%>
<%@page errorPage="/error/ErrorPage.jsp"%>
<%@page import="com.tsp.operation.model.*"%>
<%@page import="com.tsp.operation.model.beans.*"%>
<%@include file="/WEB-INF/InitModel.jsp"%>
<%@page import="java.util.*" %>
<%@page import="com.tsp.util.Util"%>

<html>
<head>
   <title>Cargar archivos Generador de Reportes</title>
   <link href="<%= BASEURL %>/css/estilotsp.css" rel="stylesheet" type="text/css"> 
   <script type='text/javascript' src="<%= BASEURL %>/js/Validaciones.js"></script>
   
   
   <script type='text/javascript' src="<%= BASEURL %>/js/boton.js"></script>
   <link href="<%= BASEURL %>/css/estilostsp.css" rel="stylesheet" type="text/css"> 
   
   
   <!-- Help -->
  <%  String path     = application.getRealPath(model.menuService.getRealUrl(request.getRequestURI()));
      String datos[]  = model.menuService.getContenidoMenu(BASEURL,request.getRequestURI(),path);
      
      String mensaje  = (String) request.getAttribute("mensaje");      
      Vector cargados = request.getAttribute("cargados")!=null? (Vector)request.getAttribute("cargados") : null;
      Object iniciado = request.getAttribute("iniciado");
      
      int anio = Util.AnoActual();
      String mes = Util.mesFormat(Util.MesActual());
      
      if( iniciado != null ){
          cargados =  null;
          mensaje  =  "El proceso ha iniciado con �xito...<br>Verifique el seguimiento de procesos<br>Consulte los archivos cuando el proceso haya finalizado";
      }
  %>


  
  
    
   <script>
        
       var cont = '<%=CONTROLLERCONTAB%>';     
     
       function eliminar( index ){
            location.replace( cont+"?estado=Generador&accion=Reportes&opcion=delete&index="+index );
       }
       
       function restablecer(){
            location.replace( cont+"?estado=Generador&accion=Reportes&opcion=reset" );
       }
       
       function procesar(){
       
            var yyyy = document.getElementById("anio").value;
            var mm   = document.getElementById("mes").value;
            
            location.replace( cont+"?estado=Generador&accion=Reportes&opcion=process&anio="+yyyy+"&mes="+mm );
       }
       
       function sendFile(theForm){ 
            
               var cont=0;
               for(var i=0;i<theForm.length;i++){
                  if( theForm.elements[i].type=='file'  &&  theForm.elements[i].value !='' )
                      cont++;
               }
               if( cont==0){

                  alert('Deber� seleccionar por lo menos un archivo');
               }
               else{    
                 document.btn_acep.style.visibility='hidden';
                 document.btn_rest.style.visibility='hidden';
                 document.btn_sali.style.visibility='hidden';
                 if( document.btn_acepp != null ){
                    document.btn_acepp.style.visibility='hidden';
                 }   
                 document.getElementById("loading").style.visibility = "visible";
                 theForm.submit();
               }
            
            
        }        
        
      
    </script>
  
</head>

<div id="capaSuperior" style="position:absolute; width:100%; height:100px; z-index:0; left: 1px; top: 0px;">
 <jsp:include page="/toptsp.jsp?encabezado=Generador de Reportes"/>
</div>

<div id="capaCentral" style="position:absolute; width:100%; height:87%; z-index:0; left: 0px; top: 100px; overflow: scroll;">        
<body>  


<center>
             
           <form id="formulario" method="post" action="<%=CONTROLLERCONTAB%>?estado=Generador&accion=Reportes" enctype="multipart/form-data">           
    <table width="50%"  border="2" align="center">
      <tr>
        <td><table width="100%"  border="1" align="center" class="tablaInferior">
          <tr>
            <td><table width="100%" border="0" cellpadding="0" cellspacing="0">
                <tr>
                  <td height="22" colspan=2 class="subtitulo1"><div align="center" class="subtitulo1" align="left">
                     <strong>Cargar Archivo</strong>                     
                  </div></td>
                  <td width="212" class="barratitulo">
                    <img src="<%=BASEURL%>/images/titulo.gif" width="32" height="20" align="left">
                    <%=datos[0]%>
                </tr>
              </table>
                <table width="100%"  border="0" align="center" cellpadding="2" cellspacing="3">                  
                  <tr class="fila">
                    <td width="31%">Seleccione Archivo: </td>
                    <td><input type="file" name='filename' style="width:100%" ></td>                        
                  </tr>                  
              </table></td>
          </tr>
        </table></td>
      </tr>
    </table>
           
           
             <br>

             <img src="<%=BASEURL%>/images/botones/agregar.gif"     name="btn_acep" id="btn_acep" onClick="sendFile(formulario);" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" style="cursor:hand">&nbsp;
             <img src="<%=BASEURL%>/images/botones/restablecer.gif" name="btn_rest" id="btn_rest"      onClick="restablecer();"   onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" style="cursor:hand">&nbsp;
             <img src="<%=BASEURL%>/images/botones/salir.gif"       name="btn_sali" id="btn_sali"   onClick="parent.close();" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" style="cursor:hand">&nbsp;

   
  </FORM>
  
  <% if( cargados != null && cargados.size() > 0 ){ %>
      <table width="50%"  border="2" align="center">
          <tr>
            <td><table width="100%"  border="0" align="center" class="tablaInferior">
              <tr>
                <td><table width="100%" border="1" cellpadding="0" cellspacing="0">
                    <tr>
                      <td height="22" colspan=2 class="subtitulo1"><div align="center" class="subtitulo1" align="left">
                         <strong>Archivos Cargados</strong>                     
                      </div></td>
                      <td width="212" class="barratitulo">
                        <img src="<%=BASEURL%>/images/titulo.gif" width="32" height="20" align="left">
                        <%=datos[0]%>
                    </tr>
                  </table>
                    <table width="100%" border="1" align="center" bordercolor="#999999" bgcolor="#F7F5F4">
                        <tr class="tblTitulo" align="center">
                            <td width="80%">Nombre </td>
                            <td>Eliminar</td>
                         </tr>
                      <% for( int i=0; i<cargados.size(); i++ ){ 
                            String file = (String) cargados.get(i);%>
                     </table>
                     <table width="100%">
                          <tr class="fila">
                            <td width="80%">&nbsp;<%=file%> </td>
                            <td align="center"> <img title='Remover archivo' style='cursor:hand' onclick="eliminar('<%=i%>');" src="<%=BASEURL%>/images/delete.gif"> </td>
                          </tr>
                      <%}%>
                      
                      <tr class="fila">
                            <td colspan='2'>&nbsp;Periodo : 
                            &nbsp;&nbsp;
                            <select id="mes" name="mes">
                                <option value="01">Ene</option>
                                <option value="02">Feb</option>
                                <option value="03">Mar</option>
                                <option value="04">Abr</option>
                                <option value="05">May</option>
                                <option value="06">Jun</option>
                                <option value="07">Jul</option>
                                <option value="08">Ago</option>
                                <option value="09">Sep</option>
                                <option value="10">Oct</option>
                                <option value="11">Nov</option>
                                <option value="12">Dic</option>
                            </select>
                            &nbsp;
                            <select id="anio" name="anio">
                            <option value="<%=String.valueOf(anio-1)%>"><%=String.valueOf(anio-1)%></option>
                            <option value="<%=String.valueOf(anio)%>" selected><%=String.valueOf(anio)%></option>                            
                            </select>
                          </td>  
                        </tr>
                  </table>
              </tr>
            </table></td>
          </tr>
        </table>
        
        <br>
        
        <table width="40%"  align="center">
          <tr>
            <td>
              <FIELDSET>
              <legend><span class="letraresaltada">Nota</span></legend>
              <table width="100%"  border="0" cellpadding="0" cellspacing="0" class="informacion">
                <tr>
                  <td nowrap align="center">&nbsp; Para procesar los archivos haga click en aceptar. </td>
                </tr>
              </table>
            </FIELDSET></td>
          </tr>
        </table>
        
        <br>
        
        <img src="<%=BASEURL%>/images/botones/aceptar.gif" name="btn_acepp" id="btn_acepp" onclick="procesar();" style='cursor:hand'>
    <%}%>

    <br>
    <div id="loading" style='visibility:hidden'>
        <table>
            <tr>
            <td class='informacion'> Cargando archivo &nbsp; <img src="<%=BASEURL%>/images/cargando.gif"> </td>
            <tr>
        </table>
    </div>    
    <br>
	
	
	
  <!-- Mensaje -->
  <%if(mensaje!=null && !mensaje.equals("")){%> 

      <table border="2" align="center">
      <tr>
        <td><table width="100%"  border="1" align="center"  bordercolor="#F7F5F4" bgcolor="#FFFFFF">
          <tr>
            <td width="350" align="center" class="mensajes"><%=mensaje%></td>
            <td width="29" background="<%=BASEURL%>/images/cuadronaranja.JPG">&nbsp;</td>
            <td width="58">&nbsp;</td>
          </tr>
        </table></td>
      </tr>
    </table>

    <%}%>

    
 
<%=datos[1]%>  

</div>
</body>
</html>

<script>
var m = document.getElementById("mes");
if (m != null){
    document.getElementById("mes").value="<%=mes%>";
}    
</script>
