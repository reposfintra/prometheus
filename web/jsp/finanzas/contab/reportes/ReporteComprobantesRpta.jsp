<%@ page session="true"%>
<%@ page errorPage="/error/error.jsp"%>
<%@page import="com.tsp.operation.model.beans.*, java.util.*, com.tsp.finanzas.contab.model.beans.*, com.tsp.finanzas.contab.model.*"%>
<%@ include file="/WEB-INF/InitModel.jsp"%>
<%@ taglib uri="http://jsptags.com/tags/navigation/pager" prefix="pg" %>
<%String path = application.getRealPath(model.menuService.getRealUrl(request.getRequestURI()));
  String datos[] = model.menuService.getContenidoMenu(BASEURL,request.getRequestURI(),path);
%>
<html>
<head>

<title>Consultar Comprobantes</title>
<script language="javascript" src="../../../js/validar.js"></script>
<link href="<%= BASEURL %>/css/estilostsp.css" rel="stylesheet" type="text/css">
<script SRC='<%=BASEURL%>/js/boton.js'></script>
<link href="../../../css/estilostsp.css" rel="stylesheet" type="text/css">
<script type='text/javascript' src="<%= BASEURL %>/js/reporte.js"></script>
</head>
<body onLoad="redimensionar();" onResize="redimensionar();">
<div id="capaSuperior" style="position:absolute; width:100%; height:100px; z-index:0; left: 0px; top: 0px;">
<jsp:include page="/WEB-INF/toptsp.jsp?encabezado=Consultar Comprobantes"/>
</div>
<div id="capaCentral" style="position:absolute; width:100%; height:85%; z-index:0; left: 0px; top: 100px; overflow: scroll;"> 

<%    
    Vector vec = (Vector) modelcontab.comprobanteService.getVector();
	
	String style = "simple";
    String position =  "bottom";
    String index =  "center";
    int maxPageItems = 50;
    int maxIndexPages = 10;
%>
<table width="100%"  border="2" align="center">
  <tr>
    <td><table width="100%">
      <tr>
        <td width="50%" class="subtitulo1">Resultado de la Consulta</td>
        <td width="50%" class="barratitulo"><img align="left" src="<%=BASEURL%>/images/titulo.gif"><%=datos[0]%></td>
      </tr>
    </table>      
      <table width="100%" border="1" align="center" bordercolor="#999999" bgcolor="#F7F5F4">
      <tr class="tblTitulo"  >
        <td nowrap><div align="center">Tipo Doc. </div></td>
        <td nowrap><div align="center">N&uacute;mero Doc. </div></td>
        <td nowrap><div align="center">Transacci&oacute;n</div></td>
        <td nowrap><div align="center">Fecha Doc. </div></td>
        <td nowrap> <div align="center">Concepto</div></td>
        <td nowrap><div align="center">Tercero</div></td>
        <td nowrap><div align="center">Moneda</div></td>
        <td nowrap><div align="center">Ttl. D&eacute;bito </div></td>
        <td nowrap><div align="center">Ttl. Cr&eacute;dito </div></td>
        <td nowrap><div align="center">Fecha Aplicaci&oacute;n</div></td>
        <td nowrap><div align="center">Usuario Aplicaci&oacute;n </div></td>
        <td nowrap><div align="center">Sucursal</div></td>
        <td nowrap><div align="center">Tipo de Operaci&oacute;n </div></td>
        <td nowrap><div align="center">Aprobador</div></td>
      </tr>
    <pg:pager        
		items="<%=vec.size()%>"
        index="<%= index %>"
        maxPageItems="<%= maxPageItems %>"
        maxIndexPages="<%= maxIndexPages %>"
        isOffset="<%= true %>"
        export="offset,currentPageNumber=pageNumber"
        scope="request">
<%
      double total = 0;
      for (int i = offset.intValue(), l = Math.min(i + maxPageItems, vec.size()); i < l; i++){
          Comprobantes doc = (Comprobantes) vec.elementAt(i);
%>
        <pg:item>
      <tr class="<%=(i % 2 == 0 )?"filagris":"filaazul"%>" onMouseOver='cambiarColorMouse(this)'   style="cursor:hand" title="Ver detalle del comprobante"
	  onClick="window.open('<%= CONTROLLERCONTAB %>?estado=Consulta&accion=Comprobantes&grupo=<%= doc.getGrupo_transaccion() %>&tipodoc=<%= doc.getTipodoc() %>&numdoc=<%= doc.getNumdoc().replaceAll("#", "-_-") %>&command=detalle','DETALL','status=yes,scrollbars=no,width=1000,height=650,resizable=yes');">
        <td class="bordereporte" nowrap><%= doc.getTipodoc_rel() %></td>
        <td class="bordereporte" nowrap><%= doc.getNumdoc() %></td>
        <td class="bordereporte" nowrap><%= doc.getGrupo_transaccion() %></td>
        <td class="bordereporte" nowrap><%= doc.getFechadoc() %></td>
        <td class="bordereporte" nowrap><%= doc.getDetalle() %></td>
        <td class="bordereporte" nowrap><%= doc.getTercero() %></td>
        <td class="bordereporte" nowrap><%= doc.getMoneda() %></td>
        <td class="bordereporte" nowrap><div align="right"><%= com.tsp.util.UtilFinanzas.customFormat2(doc.getTotal_debito()) %></div></td>
        <td class="bordereporte" nowrap><div align="right"><%= com.tsp.util.UtilFinanzas.customFormat2(doc.getTotal_credito()) %></div></td>
        <td class="bordereporte" nowrap><%= (doc.getFecha_aplicacion()!=null &&  !doc.getFecha_aplicacion().equals("0099-01-01 00:00:00") )? doc.getFecha_aplicacion().substring(0, 10) : "&nbsp;" %></td>
        <td class="bordereporte" nowrap><%= doc.getUsuario_aplicacion() %></td>
        <td class="bordereporte" nowrap><%= doc.getSucursal() %></td>
        <td class="bordereporte" nowrap><%= doc.getTipo_operacion() %></td>
        <td class="bordereporte" nowrap><div align="left"><%= doc.getAprobador() %></div></td>
      </tr>
      </pg:item>
<%
	}
%>
        <tr class="pie">
          <td td height="20" colspan="15" nowrap align="center">          <pg:index>
                <jsp:include page="/WEB-INF/jsp/googlesinimg.jsp" flush="true"/>
        </pg:index>
          </td>
        </tr>
    </pg:pager> 
    </table></td>
  </tr>
</table>
<br>  
  
<% if( request.getParameter("msg") != null ){%>
<p>
<table border="2" align="center">
  <tr>
    <td><table width="100%"  border="1" align="center"  bordercolor="#F7F5F4" bgcolor="#FFFFFF">
        <tr>
          <td width="229" align="center" class="mensajes"><%=request.getParameter("msg").toString()%></td>
          <td width="29" background="<%=BASEURL%>/images/cuadronaranja.JPG">&nbsp;</td>
          <td width="58">&nbsp;</td>
        </tr>
    </table></td>
  </tr>
</table>
<p></p>
<p>
    <%} %>
</p>
<table width="100%" border="0" align="center" cellpadding="0" cellspacing="0">
  <tr align="left">
    <td><img src="<%=BASEURL%>/images/botones/regresar.gif" name="c_regresar" id="c_regresar" style="cursor:hand " onClick="window.location.href = '<%= CONTROLLER %>?estado=Menu&accion=Cargar&carpeta=/jsp/finanzas/contab/reportes&pagina=ReporteComprobantes.jsp&marco=no';" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);">&nbsp; <img src="<%=BASEURL%>/images/botones/exportarExcel.gif" name="c_regresar" id="c_regresar" style="cursor:hand " onClick="window.open('<%= CONTROLLERCONTAB %>?estado=Consulta&accion=Comprobantes&command=excel','XLS','status=yes,scrollbars=no,width=500,height=200,resizable=yes');">  </tr>
</table>
</div>
<%=datos[1]%>
</body>
</html>



