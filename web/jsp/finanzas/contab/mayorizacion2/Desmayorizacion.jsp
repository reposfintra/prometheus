<%@ page session="true"%>
<%@ page errorPage="../error/ErrorPage.jsp"%>
<%@ include file="/WEB-INF/InitModel.jsp"%>
<%@ taglib uri="http://jakarta.apache.org/taglibs/input-0.90" prefix="input" %>
<%@page import="com.tsp.operation.model.*, com.tsp.operation.model.beans.*, com.tsp.util.*,java.util.*, java.text.*"%>

<html>
<head>
<title>Editor de Genaraci�n de Proceso de Desmayorizaci�n</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">

<script src="<%=BASEURL%>/js/utilidades.js"></script>
<link href="../css/estilostsp.css" rel="stylesheet" type="text/css">
<script type='text/javascript' src="<%= BASEURL %>/js/boton.js"></script>
<link href="<%=BASEURL%>/css/estilostsp.css" rel="stylesheet" type="text/css">
<%
    //String mensaje = (String) request.getAttribute("mensaje");//20100823
    String mensaje = (String) request.getParameter("mensaje");//20100823
    String path = application.getRealPath(model.menuService.getRealUrl(request.getRequestURI()));
    String datos[] = model.menuService.getContenidoMenu(BASEURL,request.getRequestURI(),path);
%>
</head>

<body>
<div id="capaSuperior" style="position:absolute; width:100%; height:100px; z-index:0; left: 1px; top: 0px;">
 <jsp:include page="/toptsp.jsp?encabezado=Proceso de Desmayorizaci�n"/>
</div>
<div id="capaCentral" style="position:absolute; width:100%; height:219px; z-index:0; left: 0px; top: 100px; overflow: scroll;">        
	<form id="form1" method="post" action="<%=CONTROLLERCONTAB%>?estado=Desmayorizacion&accion=Mensual">
    	<table width="300"  border="2" align="center" cellpadding="0" cellspacing="0">
            <tr class="fila">
              <td> Ecoja el periodo </td>              
            </tr>
            <tr class="fila">
              <td>A�o
                  <%
                   int a�oActual = Integer.parseInt( Util.getFechaActual_String(1) );
                   %>
                   <select name="a�obox">
                        <%
                        for(int i=a�oActual; i>=2000; i--){
                        %>
                        <option><%=i%></option>
                        <%
                        };
                        %>
                   </select>
                   Mes
                    <select name="mesbox">
                        <%
                        for(int i = 13; i>=1; i--){
                        %>
                        <option><%=i%></option>
                        <%
                        };
                        %>
                    </select>
              </td>              
            </tr>
          </table>
          <br>
            <div align="center">      
                <p>
                <%if(mensaje==null || mensaje.equals("")){//20100823%>
                    <img id="baceptar" name="baceptar" src="<%=BASEURL%>/images/botones/aceptar.gif" style="cursor:hand" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" onClick="this.style.visibility='hidden';form1.submit();">
                <%}//20100823%>
                    <img src="<%=BASEURL%>/images/botones/salir.gif" height="21" name="imgsalir" onMouseOver="botonOver(this);" onClick="window.close();" onMouseOut="botonOut(this);" style="cursor:hand">
                
                </p>
            </div>
	</form>    
	        
	<%if(mensaje!=null){%>
        <table border="2" align="center">
            <tr>                
                <td height="45"><table width="410" height="41" border="1" align="center"  bordercolor="#F7F5F4" bgcolor="#FFFFFF">
                    <tr>
                        <td width="282" height="35" align="center" class="mensajes"><%=mensaje%></td>
                        <td width="28" background="<%=BASEURL%>/images/cuadronaranja.JPG">&nbsp;</td>
                        <td width="78">&nbsp;</td>
                    </tr>
                </table></td>
            </tr>
        </table>
        <br>
        <%}%>
        
	
</div>	
<%=datos[1]%>
</body>
</html>
