<!--
- Nombre P�gina :                  EliminarPlanDeCuentas.jsp                 
- Descripci�n :                    Pagina JSP, que modifica una cuenta                     
- Autor :                          LREALES                        
- Fecha Creado :                   6 de Junio de 2006, 06:33 P.M.                        
- Versi�n :                        1.0                                       
- Copyright :                      Fintravalores S.A.                   
-->

<%-- Declaracion de librerias--%>
<%@page contentType="text/html"%>
<%@page session="true"%>
<%@page errorPage="../error/ErrorPage.jsp"%>
<%@page import="com.tsp.operation.model.*"%>
<%@page import="com.tsp.util.*"%>
<%@page import="com.tsp.operation.model.beans.*, java.util.*"%>
<%@page import="com.tsp.finanzas.contab.model.*"%>
<%@page import="com.tsp.finanzas.contab.model.beans.*, java.util.*"%>
<%@include file="/WEB-INF/InitModel.jsp"%>
<%@ taglib uri="http://jakarta.apache.org/taglibs/input-0.90" prefix="input" %>
<%
  String path = application.getRealPath(model.menuService.getRealUrl(request.getRequestURI()));
  String datos[] = model.menuService.getContenidoMenu(BASEURL,request.getRequestURI(),path);
%>
<html>
<head>
<title>Eliminar Plan De Cuentas</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link href="<%= BASEURL %>/css/estilostsp.css" rel="stylesheet" type="text/css">
<link href="../../css/estilostsp.css" rel="stylesheet" type="text/css"> 
<script type='text/javascript' src="<%= BASEURL %>/js/boton.js"></script>
</head>
<script>
		var controlador ="<%=CONTROLLERCONTAB%>";
		
		function Eliminar ( cuenta ){
		
			if( cuenta != "" ){
			
				document.forma.action = controlador+"?estado=Delete&accion=PlanDeCuentas&cuenta="+cuenta;
				document.forma.submit();
								
			} else{
			
				alert( "No Se Permiten Campos Vacios!" );
				
			}
		}
</script>

<body ONLOAD="redimensionar();" onresize="redimensionar()">
<div id="capaSuperior" style="position:absolute; width:100%; height:100px; z-index:0; left: 0px; top: 0px;">
	<jsp:include page="/WEB-INF/toptsp.jsp?encabezado=Plan De Cuentas"/>
</div>
<div id="capaCentral" style="position:absolute; width:100%; height:87%; z-index:0; left: 0px; top: 100px; overflow: scroll;">
<% 
	String e = request.getParameter("e");
%>
<form name="forma" method="post">
<% 
	if( e != null && e.equals("no") ){ 
%>
  <table width="95%"  border="2" align="center">
  	<tr>
    	<td>
			<table width="100%" align="center">
				<tr>
					<td colspan="5" nowrap class="subtitulo1">Informaci&oacute;n de la Cuenta a Eliminar </td>
					<td colspan="5" class="barratitulo"><img src="<%=BASEURL%>/images/titulo.gif" width="32" height="20" align="left"><%=datos[0]%></td>
				</tr>
				<%
				
				Vector vec_cuentas = modelcontab.planDeCuentasService.getVec_cuentas();
				
				PlanDeCuentas cuentas = ( PlanDeCuentas ) vec_cuentas.elementAt( 0 );
				
				String cue = cuentas.getCuenta();
				String nom_lar = cuentas.getNombre_largo();
				String nom_cor = cuentas.getNombre_corto();
				String obs = cuentas.getNombre_observacion();
				String aux = ( cuentas.getAuxiliar() == "S" )?"SI":"NO";
				String act = ( cuentas.getActiva() == "S" )?"SI":"NO";
				String mod1 = ( cuentas.getModulo1() == "S" )?"SI":"NO";
				String mod2 = ( cuentas.getModulo2() == "S" )?"SI":"NO";
				String mod3 = ( cuentas.getModulo3() == "S" )?"SI":"NO";
				String mod4 = ( cuentas.getModulo4() == "S" )?"SI":"NO";
				String mod5 = ( cuentas.getModulo5() == "S" )?"SI":"NO";
				String mod6 = ( cuentas.getModulo6() == "S" )?"SI":"NO";
				String mod7 = ( cuentas.getModulo7() == "S" )?"SI":"NO";
				String mod8 = ( cuentas.getModulo8() == "S" )?"SI":"NO";
				String mod9 = ( cuentas.getModulo9() == "S" )?"SI":"NO";
				String mod10 = ( cuentas.getModulo10() == "S" )?"SI":"NO";
				String cta_dep = cuentas.getCta_dependiente();
				String niv = cuentas.getNivel();
				String cta_cie = cuentas.getCta_cierre();
				String sub = ( cuentas.getSubledger() == "S" )?"SI":"NO";
				String ter = cuentas.getTercero();
								
				if ( ter.equals("N") )
					ter = "No Aplica";
				else if ( ter.equals("M") )
					ter = "Mandatorio";
				else if ( ter.equals("O") )
					ter = "Opcional";
				else
					ter = "No Aplica";
				
				%> 
				<tr>
				  <td width="11%" class="fila">Cuenta</td>
				  <td colspan="4" class="letra"><%=cue%></td>
			      <td class="fila">Activa?</td>				  
			      <td colspan="4" class="letra"><%=act%>
				  </td>
		      </tr>
				<tr>
				  <td class="fila">Nombre Largo </td>
				  <td colspan="4" class="letra"><%=nom_lar%></td>
				  <td width="10%" class="fila">Nombre Corto </td>
			      <td colspan="4" class="letra"><%=nom_cor%></td>
			  </tr>
				<tr>
				  <td class="fila">Observaci&oacute;n</td>
				  <td colspan="9" class="letra"><%=obs%></td>
			    </tr>
				<tr>
				  <td class="fila">Nivel N&deg; </td>
				  <td colspan="2" class="letra"><%=niv%>
				  </td>
				  <td width="8%" class="fila">Auxiliar?</td>
				  <td width="16%" class="letra"><%=aux%>
				  </td>
				  <td class="fila">Subledger?</td>
			      <td colspan="2" class="letra"><%=sub%>
				  </td>
			      <td width="9%" class="fila">Tercero?</td>
			      <td width="8%" class="letra"><%=ter%>
				  </td>
			  </tr>
				<tr>
				  <td colspan="3" class="fila">Cuenta Dependiente </td>
				  <td colspan="2" class="letra"><%=cta_dep%></td>
				  <td colspan="3" class="fila">Cuenta de Cierre </td>
			      <td colspan="2" class="letra"><%=cta_cie%></td>
				</tr>
				<tr>
				  <td class="fila">Modulo 1 </td>
				  <td width="7%" class="letra"><%=mod1%>
				  </td>
				  <td width="11%" class="fila">Modulo 2 </td>
				  <td class="letra"><%=mod2%>
				  </td>
				  <td class="fila">Modulo 3 </td>
				  <td class="letra"><%=mod3%>
				  </td>
				  <td width="11%" class="fila">Modulo 4 </td>
				  <td width="9%" class="letra"><%=mod4%>
				  </td>
				  <td class="fila">Modulo 5 </td>
				  <td class="letra"><%=mod5%>
				  </td>
			  </tr>
				<tr>
				  <td class="fila">Modulo 6</td>
				  <td class="letra"><%=mod6%>
				  </td>
				  <td class="fila">Modulo 7 </td>
				  <td class="letra"><%=mod7%>
				  </td>
				  <td class="fila">Modulo 8 </td>
				  <td class="letra"><%=mod8%>
				  </td>
			      <td class="fila">Modulo 9 </td>
			      <td class="letra"><%=mod9%>
				  </td>
			      <td class="fila">Modulo 10 </td>
			      <td class="letra"><%=mod10%>
				  </td>
			  </tr>
		  </table>
  		</td>
  	</tr>
  </table>
  <br>  
  <input type="hidden" id="cuenta" value="<%=cue%>">
  <table align=center width='450'>
    <tr>
        <td align="center" colspan="2">
            <img title='Anular Cuenta' style = "cursor:hand" src="<%= BASEURL %>/images/botones/anular.gif" onmouseover="botonOver(this);" onmouseout="botonOut(this);" onclick="Eliminar( cuenta.value );"></img>                        
			<img title='Regresar' src="<%= BASEURL %>/images/botones/regresar.gif" onClick = "window.location='<%=BASEURL%>/jsp/finanzas/contab/plan_de_cuentas/eliminar/BuscarParaEliminarPlanDeCuentas.jsp'; this.disabled=true;" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" style="cursor:hand"></img>
			<img title='Salir' style = "cursor:hand" src="<%= BASEURL %>/images/botones/salir.gif" onmouseover="botonOver(this);" onmouseout="botonOut(this);" onClick="window.close()"></img>
        </td>
    </tr>
  </table> 
  <br>  
  <%String msg = request.getParameter("msg");
	if ( msg!=null && !msg.equals("") ){%>
                        
	  <table border="2" align="center">
   		<tr>
        	<td>
            	<table width="100%"  border="1" align="center"  bordercolor="#F7F5F4" bgcolor="#FFFFFF">
                	<tr>
                    	<td width="229" align="center" class="mensajes"><%=msg%></td>
                        <td width="29" background="<%=BASEURL%>/images/cuadronaranja.JPG">&nbsp;</td>
                        <td width="58">&nbsp;</td>
                    </tr>
                </table>
            </td>
		</tr>
	  </table>
	<%}%>
	
	<%
}//cierro if ( e = no )
	if ( e != null && e.equals("si") ){
%>                        
<%
	out.print("<table width=379 border=2 align=center><tr><td><table width=100%  border=1 align=center  bordercolor=#F7F5F4 bgcolor=#FFFFFF><tr><td width=350 align=center class=mensajes>La Cuenta Ha Sido Eliminada Satisfactoriamente!</td><td width=100><img src="+BASEURL+"/images/cuadronaranja.JPG></td></tr></table></td></tr></table>");%>
	<br>
	<p align="center">
	  <img src="<%=BASEURL%>/images/botones/regresar.gif" name="img_regresar" onClick = "window.location='<%=CONTROLLER%>?estado=Menu&accion=Cargar&carpeta=/jsp/finanzas/contab/plan_de_cuentas/eliminar&pagina=BuscarParaEliminarPlanDeCuentas.jsp&marco=no&opcion=30'; this.disabled=true;" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" align="absmiddle" style="cursor:hand"> 
	  
	  <img src="<%=BASEURL%>/images/botones/salir.gif" name="img_salir" onClick="window.close();" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" align="absmiddle" style="cursor:hand">
	</p>
<%
	}//cierro if ( e = si )
%>
  </form>
</div>
<%=datos[1]%>
</body>
</html>