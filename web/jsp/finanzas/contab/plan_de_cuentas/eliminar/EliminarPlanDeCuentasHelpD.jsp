<!--  
	 - Author(s)       :      LREALES
	 - Description     :      AYUDA DESCRIPTIVA - Eliminar Plan De Cuentas
	 - Date            :      07/06/2006 
	 - Copyright Notice:      TRANSPORTES SANCHEZ POLO S.A.
-->
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN""http://www.w3.org/TR/html4/loose.dtd">
<%@page session="true"%> 
<%@page errorPage="/error/ErrorPage.jsp"%>
<%@page import="com.tsp.operation.model.beans.*, java.util.*"%>
<%@ include file="/WEB-INF/InitModel.jsp"%>
<%@ taglib uri="http://jakarta.apache.org/taglibs/input-0.90" prefix="input" %>
<html>
<head>
<title>AYUDA DESCRIPTIVA - Eliminar Plan De Cuentas</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link href="<%= BASEURL %>/css/estilostsp.css" rel="stylesheet" type="text/css">
<script type='text/javascript' src="<%= BASEURL %>/js/boton.js"></script>
</head>

<body>
<br>
<table width="696"  border="2" align="center">
  <tr>
    <td width="811" >
      <table width="100%" border="0" align="center">
        <tr  class="subtitulo">
          <td height="24" colspan="2"><div align="center">Plan De Cuentas</div></td>
        </tr>
        <tr class="subtitulo1">
          <td colspan="2">Eliminar Plan De Cuentas - Pantalla Preliminar</td>
        </tr>
		<tr class="subtitulo1">
          <td colspan="2">INFORMACION DE LA CUENTA </td>
        </tr>
        <tr>
          <td  class="fila">'Cuenta'</td>
          <td  class="ayudaHtmlTexto">Campo para digitar datos que contengan el c&oacute;digo de la cuenta que deseo eliminar. Este campo es de m&aacute;ximo 25 caracteres. </td>
        </tr>
        <tr>
          <td  class="fila">'CUENTAS A ELIMINAR' </td>
          <td  class="ayudaHtmlTexto">Lista de las cuentas existentes en la base de datos. Aqui se escoje la cuenta que se desea anular. </td>
        </tr>
        <tr>
          <td class="fila">Bot&oacute;n Buscar </td>
          <td  class="ayudaHtmlTexto">Bot&oacute;n que permite realizar la busqueda de las cuentas que contengan el c&oacute;digo ingresado.</td>
        </tr>
        <tr>
          <td class="fila">Bot&oacute;n Detalles </td>
          <td  class="ayudaHtmlTexto">Bot&oacute;n que realiza la busqueda de todas la cuentas existentes. </td>
        </tr>
        <tr>
          <td class="fila">Bot&oacute;n Anular </td>
          <td  class="ayudaHtmlTexto">Bot&oacute;n para confirmar y realizar la anulaci&oacute;n de la cuenta.</td>
        </tr>
		<tr>
		  <td class="fila">Bot&oacute;n Regresar </td>
		  <td  class="ayudaHtmlTexto">Bot&oacute;n que permite regresar a la vista anterior. </td>
	    </tr>
		<tr>
          <td width="200" class="fila">Bot&oacute;n Salir</td>
          <td width="474"  class="ayudaHtmlTexto">Bot&oacute;n para salir de la vista 'Eliminar Plan De Cuentas' y volver a la vista del men&uacute;.</td>
        </tr>
      </table>
    </td>
  </tr>
</table>
<p></p>
<center>
<img src = "<%=BASEURL%>/images/botones/salir.gif" style = "cursor:hand" name = "imgsalir" onClick = "parent.close();" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);">
</center>
</body>
</html>
