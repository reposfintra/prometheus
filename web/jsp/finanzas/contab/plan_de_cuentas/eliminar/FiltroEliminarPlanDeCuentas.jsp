<!--
- Nombre P�gina :                  FiltroEliminarPlanDeCuentas.jsp                 
- Descripci�n :                    Pagina JSP, que realiza el filtro de busqueda de las cuentas               
- Autor :                          LREALES                        
- Fecha Creado :                   9 de Junio de 2006, 04:11 P.M.                        
- Versi�n :                        1.0                                       
- Copyright :                      Fintravalores S.A.                   
-->

<%-- Declaracion de librerias--%>
<%@page contentType="text/html"%>
<%@ page session="true"%>
<%@ page errorPage="../error/error.jsp"%>
<%@page import="com.tsp.operation.model.beans.*, java.util.*"%>
<%@ include file="/WEB-INF/InitModel.jsp"%>
<%@page import="com.tsp.operation.model.*"%>
<%@page import="com.tsp.util.*"%>
<%@ taglib uri="http://jakarta.apache.org/taglibs/input-0.90" prefix="input" %>
<%
  String path = application.getRealPath(model.menuService.getRealUrl(request.getRequestURI()));
  String datos[] = model.menuService.getContenidoMenu(BASEURL,request.getRequestURI(),path);
%>
<html>
<head><title>Eliminar Plan De Cuentas</title>
<link href="<%= BASEURL %>/css/estilostsp.css" rel="stylesheet" type="text/css">
<script type='text/javascript' src="<%= BASEURL %>/js/boton.js"></script>
<script type='text/javascript' src="<%= BASEURL %>/js/validar.js"></script>
</head>
<body onLoad="redimensionar();" onResize="redimensionar();">
<div id="capaSuperior" style="position:absolute; width:100%; height:100px; z-index:0; left: 0px; top: 0px;">
<jsp:include page="/WEB-INF/toptsp.jsp?encabezado=Plan De Cuentas"/>
</div>

<div id="capaCentral" style="position:absolute; width:100%; height:85%; z-index:0; left: 0px; top: 100px; overflow: scroll;"> 
<%
    Usuario usuario = (Usuario) session.getAttribute("Usuario");   
%>
<script>
	var controlador ="<%=CONTROLLERCONTAB%>";
	
	function Buscar ( cuenta ){
	
		if( cuenta != "" ){
		
			document.form2.action = controlador+"?estado=SearchDelete&accion=PlanDeCuentas&listar=True&cuenta="+cuenta;
			document.form2.submit();
						
		} else{
		
			alert( "Por Favor No Deje Los Campos Obligatorios Vacios!" );
			
		}
	}
	
	function Detalles (){
	
		document.form2.action = controlador+"?estado=SearchDelete&accion=PlanDeCuentas&listar=Other";
		document.form2.submit();
						
	}
</script>

<form name="form2" method="post">
    <table width="50%" border="2" align="center">
      <tr>
        <td><table width="100%" align="center"  class="tablaInferior">
            <tr>
				<td width="50%" class="subtitulo1">Filtro para Eliminar una Cuenta </td>
				<td width="50%" class="barratitulo"><img src="<%=BASEURL%>/images/titulo.gif" width="32" height="20" align="left"><%=datos[0]%></td>
            </tr>
            <tr class="fila">
              <td width="50%" align="left" valign="middle" >Cuenta</td>
              <td width="50%" valign="middle"><input name="cuenta" type="text" class="textbox" id="cuenta" size="25" maxlength="25" onKeyPress="if (window.event.keyCode==13){Buscar ( this.value );}">
              <img src="<%=BASEURL%>/images/botones/iconos/obligatorio.gif"></td>
            </tr>		
        </table></td>
      </tr>
    </table>
<p>
<div align="center">
<p><img src="<%=BASEURL%>/images/botones/buscar.gif" style="cursor:hand" title="Buscar cuenta especifica" name="buscar"  onClick="Buscar ( cuenta.value );" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);">&nbsp;
      <img src="<%=BASEURL%>/images/botones/detalles.gif" style="cursor:hand" title="Buscar todas las cuentas" name="buscar"  onClick="Detalles ();" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);">&nbsp;
      <img src="<%=BASEURL%>/images/botones/salir.gif"  name="salir" style="cursor:hand" title="Salir al Menu Principal" onMouseOver="botonOver(this);" onClick="parent.close();" onMouseOut="botonOut(this);" ></p>
  <%String msg = request.getParameter("msg");
	if ( msg!=null && !msg.equals("") ){%>
                        
	  <table border="2" align="center">
   		<tr>
        	<td>
            	<table width="100%"  border="1" align="center"  bordercolor="#F7F5F4" bgcolor="#FFFFFF">
                	<tr>
                    	<td width="229" align="center" class="mensajes"><%=msg%></td>
                        <td width="29" background="<%=BASEURL%>/images/cuadronaranja.JPG">&nbsp;</td>
                        <td width="58">&nbsp;</td>
                    </tr>
                </table>
            </td>
		</tr>
	  </table>
	<%}%>
  </p>
</div>
</p>
</form>
</div>
<%=datos[1]%>
</body>
</html>