<!--  
	 - Author(s)       :      LREALES
	 - Description     :      AYUDA FUNCIONAL - Eliminar Plan De Cuentas
	 - Date            :      07/06/2006 
	 - Copyright Notice:      TRANSPORTES SANCHEZ POLO S.A.
-->
<%@ include file="/WEB-INF/InitModel.jsp"%>
<HTML>
<HEAD>
<TITLE>AYUDA FUNCIONAL - Eliminar Plan De Cuentas</TITLE>
<META http-equiv=Content-Type content="text/html; charset=windows-1252">
<link href="../css/estilostsp.css" rel="stylesheet" type="text/css"> 
<link href="<%=BASEURL%>/css/estilostsp.css" rel="stylesheet" type="text/css">
<script type='text/javascript' src="<%= BASEURL %>/js/boton.js"></script> 
</HEAD>
<BODY> 
<% String BASEIMG = BASEURL +"/images/ayuda/contabilidad/plan_de_cuentas/eliminar/"; %>
  <table width="95%"  border="2" align="center">
    <tr>
      <td height="117" >
        <table width="100%" border="0" align="center">
          <tr  class="subtitulo">
            <td height="20"><div align="center">MANUAL DE PLAN DE CUENTAS WEB</div></td>
          </tr>
          <tr class="subtitulo1">
            <td>Descripci&oacute;n del funcionamiento del programa para Eliminar Plan De Cuentas.</td>
          </tr>
          <tr>
            <td  class="ayudaHtmlTexto"><div align="center">
              <p>&nbsp;</p>
              </div></td>
          </tr>
          <tr>
            <td  class="ayudaHtmlTexto">En la siguiente pantalla se puede ingresar el n&uacute;mero de la cuenta, para realizar la busqueda especifica, o se puede escoger la busqueda detalla.</td>
          </tr>
          <tr>
            <td  class="ayudaHtmlTexto"><div align="center"><IMG height=122 src="<%=BASEIMG%>image000.JPG" width=493 border=0 v:shapes="_x0000_i1143"></div></td>
          </tr>
          <tr>
            <td  class="ayudaHtmlTexto"><p>&nbsp;</p>
            <p>Si se escoge el Bot&oacute;n 'Buscar' y el campo del n&uacute;mero de la cuenta esta vacio, en la pantalla le aparecer&aacute; el siguiente mensaje. </p></td>
          </tr>
          <tr>
            <td  class="ayudaHtmlTexto"><div align="center"><IMG height=126 src="<%=BASEIMG%>image_error001.JPG" width=320 border=0 v:shapes="_x0000_i1054"></div></td>
          </tr>
          <tr>
            <td  class="ayudaHtmlTexto"><p>&nbsp;</p>
            <p>Si se escoge el Bot&oacute;n 'Buscar' con alg&uacute;n dato en el campo de la cuenta, este realiza la busqueda de las cuentas que contengan el c&oacute;digo ingresado y aqui se escoge la cuenta que se desea eliminar.</p></td>
          </tr>
          <tr>
            <td  class="ayudaHtmlTexto"><div align="center"><IMG height=213 src="<%=BASEIMG%>image001.JPG" width=985 border=0 v:shapes="_x0000_i1143"></div></td>
          </tr>
          <tr>
            <td  class="ayudaHtmlTexto"><p>&nbsp;</p>
            <p>Si se escoge el Bot&oacute;n 'Detalles' y le aparece el siguiente mensaje, es por que no existen cuentas en la base de datos para eliminar.</p></td>
          </tr>
          <tr>
            <td  class="ayudaHtmlTexto"><div align="center"><IMG height=103 src="<%=BASEIMG%>image_error000.JPG" width=379 border=0 v:shapes="_x0000_i1143"></div></td>
          </tr>
          <tr>
            <td  class="ayudaHtmlTexto"><p class="ayudaHtmlTexto">&nbsp;</p>
            <p class="ayudaHtmlTexto">Si se escoge el Bot&oacute;n 'Detalles' en la siguiente pantalla aparece un lista con las cuentas existentes en la base de datos, aqui se escoge la cuenta que se desea anular.</p></td>
          </tr>
<tr>
            <td  class="ayudaHtmlTexto"><div align="center"><IMG height=416 src="<%=BASEIMG%>image002.JPG" width=987 border=0 v:shapes="_x0000_i1143"></div></td>
          </tr>
<tr>
  <td  class="ayudaHtmlTexto"><p>&nbsp;</p>
    <p>Luego de haber escogido la cuenta a eliminar, en la siguiente pantalla se verifican los datos de la cuenta a eliminar.</p></td>
</tr>
<tr>
  <td  class="ayudaHtmlTexto"><div align="center"><IMG height=228 src="<%=BASEIMG%>image003.JPG" width=937 border=0 v:shapes="_x0000_i1054"></div></td>
</tr>
<tr>
  <td  class="ayudaHtmlTexto"><p>&nbsp;</p>
    <p>Cuando este seguro que esa es la cuenta que desea anular, puede presionar el bot&oacute;n 'ANULAR' y en la pantalla el sistema nos mostrar&aacute; lo siguiente. </p></td>
</tr>
<tr>
            <td  class="ayudaHtmlTexto"><div align="center"><IMG height=108 src="<%=BASEIMG%>image004.JPG" width=380 border=0 v:shapes="_x0000_i1054"></div></td>
          </tr>
      </table></td>
    </tr>
  </table>
  <p></p>
<center>
<img src = "<%=BASEURL%>/images/botones/salir.gif" style = "cursor:hand" name = "imgsalir" onClick = "parent.close();" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);">
</center>
</BODY>
</HTML>
