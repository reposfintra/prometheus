<!--
- Autor : Ing. Andr�s Maturana
- Date  : 5 de Octubre de 2005
- Copyrigth Notice : Fintravalores S.A. S.A
-->
<%--
-@(#)
--Descripcion : Pagina JSP, que se encarga de realizar y mostrar busqueda de los tipos de impuestos
--%>

<%-- Declaracion de librerias--%>
<%@page contentType="text/html"%>
<%@page session="true"%>
<%@page import="java.util.*"%>
<%@page import="com.tsp.operation.model.*"%>
<%@page import="com.tsp.util.*"%>
<%@page import="com.tsp.operation.model.beans.*, java.util.*"%>
<%@include file="/WEB-INF/InitModel.jsp"%>
<%@taglib uri="http://jakarta.apache.org/taglibs/input-0.90" prefix="input" %>
<html>
<head><title>JSP Page</title>
<link href="/css/estilostsp.css" rel="stylesheet" type="text/css"> 
<link href="<%=BASEURL%>/css/estilostsp.css" rel="stylesheet" type="text/css"> 
<script type='text/javascript' src="<%=BASEURL%>/js/Validacion.js"></script>
<script type='text/javascript' src="<%=BASEURL%>/js/validar.js"></script> 
<script type='text/javascript' src="<%= BASEURL %>/js/boton.js"></script>
</head>
<body <%if ( request.getParameter("estado").compareTo("close")==0 ) { %> onLoad='window.close();' <%}%>>
<div id="capaSuperior" style="position:absolute; width:100%; height:100px; z-index:0; left: 0px; top: 0px;">
 <jsp:include page="/toptsp.jsp?encabezado=Elementos del Gasto "/>
</div>

<div id="capaCentral" style="position:absolute; width:100%; height:87%; z-index:0; left: 0px; top: 100px; overflow: scroll;">
<%String msg = request.getParameter("msg");
	if ( msg!=null && !msg.equals("") ){%>
  <table border="2" align="center">
    <tr>
      <td>
        <table width="100%"  border="1" align="center"  bordercolor="#F7F5F4" bgcolor="#FFFFFF">
          <tr>
            <td width="229" align="center" class="mensajes"><%=msg%></td>
            <td width="29" background="<%=BASEURL%>/images/cuadronaranja.JPG">&nbsp;</td>
            <td width="58">&nbsp;</td>
          </tr>
      </table></td>
    </tr>
  </table>
  <br>
  <table align=center width='450'>
    <tr>
      <td align="center" colspan="2">  <img title='Salir' style = "cursor:hand" src="<%= BASEURL %>/images/botones/salir.gif" onmouseover="botonOver(this);" onmouseout="botonOut(this);" onClick="window.close()"> </td>
    </tr>
  </table>
  <%} else {%>
<%
   LinkedList tablas = (LinkedList) request.getAttribute("tablas");
   Hashtable hashtable = modelcontab.planDeCuentasService.getHashtable();
   System.out.println(".........HASHTABLE: " + hashtable);
%>
<form name="forma" action="<%= CONTROLLERCONTAB %>?estado=PlanDeCuentas&accion=CargarELEM&command=setear" method="post">      
<table width="432" border="2" align="center">
  <tr>
    <td>
<table width="100%" align="center" > 
  <tr>
    <td width="50%" height="24"  class="subtitulo1"><p align="left">Elemento del Gasto</p></td>
    <td width="50%"  class="barratitulo"><img src="<%=BASEURL%>/images/titulo.gif"></td>
  </tr>
</table>
<table width="100%"  align="center" >
 <tr class="tblTitulo" id="titulos">
    <td width="20" align="center"></td>
    <td width="333" align="center">Descripci�n</td>
    <td width="114" align="center">C&oacute;digo</td>
 </tr>
<% 
    
    for(int i=0; i< tablas.size();i++){
        TablaGen obj = (TablaGen) tablas.get(i);
%>  
        <tr class="fila">
			<td align="center"><input type="checkbox" name="element" id="element" value="<%= obj.getTable_code()%>" 
				<% if ( hashtable!=null && hashtable.get(obj.getTable_code())!=null ) {%> checked <%}%>/></td>
			<td align="center"><%= obj.getDescripcion() %></td>
			<td align="center"><%= obj.getTable_code() %></td>
	    </tr>
<%
    }
%>  
</table>
</td>
</tr>
</table>
  <br>  
  <table align=center width='450'>
    <tr>
        <td align="center" colspan="2">
            <img title='Agregar' style = "cursor:hand" src="<%= BASEURL %>/images/botones/aceptar.gif" onmouseover="botonOver(this);" onmouseout="botonOut(this);" onclick="if ( TCamposLlenos() ) { forma.submit(); } "></img>                        
			<img title='Resetear' style = "cursor:hand" src="<%= BASEURL %>/images/botones/cancelar.gif" onmouseover="botonOver(this);" onmouseout="botonOut(this);" onClick="forma.reset();"></img>
            <img title='Salir' style = "cursor:hand" src="<%= BASEURL %>/images/botones/salir.gif" onmouseover="botonOver(this);" onmouseout="botonOut(this);" onClick="window.close()"></img>
        </td>
    </tr>
  </table> 
  <p>&nbsp;</p>
</form>
<% } %>
</div>
</html>
