<%-- 
    Document   : gestionHc
    Created on : 28/05/2010, 11:19:13 AM
    Author     : maltamiranda
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@page session    ="true"%>
<%@page errorPage  ="/error/ErrorPage.jsp"%>
<%@include file    ="/WEB-INF/InitModel.jsp"%>
<%@page import    ="java.util.*" %>
<%@page import    ="com.tsp.util.*" %>
<%@page import    ="com.tsp.operation.model.beans.Usuario" %>

<link href="<%= BASEURL%>/css/estilostsp.css" rel="stylesheet" type="text/css">
<script src="<%=BASEURL%>/js/prototype.js"      type="text/javascript"></script>
<script src="<%=BASEURL%>/js/boton.js"          type="text/javascript"></script>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">

<%  Usuario usuario = (Usuario) session.getAttribute("Usuario");
    String distrito = (String) session.getAttribute("Distrito");
    String msg = Util.coalesce((String) request.getAttribute("msg"), "");
%>
<script>
    function clean(name,i){
        while ($(name).length > i) {
            $(name).remove(i);
        }

    }


    function carga(url,tipo,p){
        if(tipo=='CARGA_INICIAL')
        {   new Ajax.Request(
            url,
            {   method: 'post',
                parameters: p,
                onComplete: completehcs
            });
        }
        else
        {   if(tipo=='CARGA_TIPO_DOCUMENTOS')
            {   new Ajax.Request(
                url,
                {   method: 'post',
                    parameters: p,
                    onComplete: completeTdocs
                });

            }
            else
            {   if(tipo=='CARGA_CUENTA')
                {   new Ajax.Request(
                    url,
                    {   method: 'post',
                        parameters: p,
                        onComplete: getCuenta
                    });

                }
                else
                {   if(tipo=='INSERTAR_CMC'){
                        new Ajax.Request(
                        url,
                        {   method: 'post',
                            parameters: p,
                            onComplete: mensajeDevuelto
                        });
                    }
                }
            }
        }
    }

    function mensajeDevuelto(response){
        alert(response.responseText);
        clean('tdocs',0);
        clean('tdoc',1);
        if($('desc').style.visibility=='visible')
        {   carga('<%=CONTROLLERCONTAB%>?estado=Gestionar&accion=Hc','CARGA_TIPO_DOCUMENTOS','evento=CARGA_TIPO_DOCUMENTOS&cmc='+$('hc').value);
        }
        else{
            carga('<%=CONTROLLERCONTAB%>?estado=Gestionar&accion=Hc','CARGA_TIPO_DOCUMENTOS','evento=CARGA_TIPO_DOCUMENTOS&cmc='+$('hcs').value);
        }
    }

    function getCuenta(response){
        resp=response.responseText;
        if(resp!=''){
            $('cuenta').readOnly=true;
        }
        else{
            $('cuenta').readOnly=false;
        }

        $('cuenta').value=resp;
    }

    function completehcs(response){
        clean('hcs',0);
        resp=response.responseText;
        myObject = resp.evalJSON();
        for(i=0;i<myObject.length;i++)
        {   y=document.createElement('option');
            y.text=myObject[i].cmc+"-"+myObject[i].descripcion;
            y.value=myObject[i].cmc;
            try
            {   $('hcs').add(y,null); // standards compliant
            }
            catch(ex)
            {   $('hcs').add(y); // IE only
            }
        }
    }
    function completeTdocs(response){
        clean('tdoc',1);
        clean('tdocs',0);
        resp=response.responseText;
        myObject = resp.evalJSON();
        for(i=0;i<myObject.length;i++)
        {   y=document.createElement('option');
            y.text=myObject[i].tipodoc+'-'+myObject[i].desc;
            y.value=myObject[i].tipodoc;
            try
            {   if(myObject[i].cmc=='')
                {   $('tdoc').add(y,null); // standards compliant
                }
                else{
                    $('tdocs').add(y,null); // standards compliant
                }
            }
            catch(ex)
            {   if(myObject[i].cmc=='')
                {   $('tdoc').add(y); // standards compliant
                }
                else{
                    $('tdocs').add(y); // standards compliant
                }
            }
        }
    }


    function validar (url,tipo,p){
        sw=false;
        if($('desc').style.visibility=='visible'){
            if($('descripcion').value==''){
                sw=true;
                alert('Debe llenar una descripcion para el nuevo Hc...');
            }
            if($('hc').value==''){
                sw=true;
                alert('Debe asignar un codigo de cuenta contable...');
            }
        }
        else{
            if($('hcs').value==''){
                sw=true;
                alert('Debe asignar un codigo de cuenta contable...');
            }
        }
        if($('tdoc').value=='...'){
            sw=true;
            alert('Debe escoger un tipo de documento...');
        }
        if($('cuenta').value==''){
            sw=true;
            alert('Debe asignar una cuenta contable valida');
        }
        if (!sw){
            carga(url,tipo,p);
        }


    }


    function controller (accion){
        if(accion=='hc')
        {   if($('hc').value!=''){
                $('tdocs').selectedIndex=-1;
                $('tdoc').selectedIndex=0;
                $('tdoc').selectedIndex=0;
                $('desc').style.visibility='visible';
                $('desc').style.display='table-row';
                $('trcuenta').style.visibility='hidden';
                $('trcuenta').style.display='none';
            }else{
                $('desc').style.visibility='hidden';
                $('desc').style.display='none';
            }
        }
        else{
            if(accion=='hcs'){
                $('hc').value='';
                $('desc').style.visibility='hidden';
                $('desc').style.display='none';
                $('tdocs').selectedIndex=-1;
                $('tdoc').selectedIndex=0;
                $('tdoc').selectedIndex=0;
                $('trcuenta').style.visibility='hidden';
                $('trcuenta').style.display='none';
            }
            else{
                if(accion=='tdoc'){
                    $('tdocs').selectedIndex=-1;
                    $('trcuenta').style.visibility='visible';
                    $('trcuenta').style.display='table-row';
                }
                else{
                    if(accion=='tdocs'){
                        $('tdoc').selectedIndex=0;
                        $('trcuenta').style.visibility='visible';
                        $('trcuenta').style.display='table-row';
                    }
                }

            }

        }
    }
    
    
</script>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
        <title>Gestion de Codigos de Manejo Contable</title>
    </head>
    <body onload="carga('<%=CONTROLLERCONTAB%>?estado=Gestionar&accion=Hc&evento=CARGA_INICIAL','CARGA_INICIAL','');">
        <div id="capaSuperior" style="position:absolute; width:100%; height:100px; z-index:0; left: 0px; top: 0px;">
            <jsp:include page="/WEB-INF/toptsp.jsp?encabezado=Gestion de Codigos de Manejo Contable"/>
        </div>
        <div id="capaCentral" style="position:absolute; width:100%; height:85%; z-index:0; left: 0px; top: 100px; overflow: scroll;">
            <div id="contenido">
            </div>
            <br>
            <center>
                <table border="2" width="403">
                    <tr>
                        <td width="391">
                            <table class="tablaInferior" border="0" width="100%">
                                <tr >
                                    <td width="70%" align="left" class="subtitulo1">&nbsp;Consulta y Creacion de Hcs</td>
                                    <td width="50%" align="left" class="barratitulo"><img alt="" src="<%= BASEURL%>/images/titulo.gif" width="32" height="20"  align="left"></td>
                                </tr>
                            </table>
                            <table class="tablaInferior" border="0" width="100%">
                                <tr class="fila" id="desc" style="visibility: hidden;display: none" >
                                    <td>Descripción</td>
                                    <td align="center" valign="top"><input type="text" name="descripcion" id="descripcion" value=""> </td>
                                </tr>
                                <tr class="fila">
                                    <td>Codigo de Manejo Contable</td>
                                    <td align="center">
                                        <table>
                                            <tr>
                                                <td align="center">
                                                    <input type="text" name="hc" id="hc" value="" style="width: 80px"  onclick="$('trcuenta').style.visibility='hidden';$('trcuenta').style.display='none';$('hcs').selectedIndex=-1;clean('tdocs',0);clean('tdoc',1);carga('<%=CONTROLLERCONTAB%>?estado=Gestionar&accion=Hc&evento=CARGA_TIPO_DOCUMENTOS&cmc='+$('hc').value,'CARGA_TIPO_DOCUMENTOS','');" onkeyup="controller(this.id);carga('<%=CONTROLLERCONTAB%>?estado=Gestionar&accion=Hc&evento=CARGA_TIPO_DOCUMENTOS&cmc='+$('hc').value,'CARGA_TIPO_DOCUMENTOS','');">
                                                </td>
                                            </tr>
                                            <tr>
                                                <td align="center">
                                                    <select id="hcs" multiple onkeyup="controller(this.id);carga('<%=CONTROLLERCONTAB%>?estado=Gestionar&accion=Hc&evento=CARGA_TIPO_DOCUMENTOS&cmc='+$('hcs').value,'CARGA_TIPO_DOCUMENTOS','');" onclick="controller(this.id);carga('<%=CONTROLLERCONTAB%>?estado=Gestionar&accion=Hc&evento=CARGA_TIPO_DOCUMENTOS&cmc='+$('hcs').value,'CARGA_TIPO_DOCUMENTOS','');" size="6"></select>
                                                </td>
                                            </tr>
                                        </table>

                                    </td>
                                </tr>
                                <tr class="fila">
                                    <td>Tipo Documento</td>
                                    <td align="center">
                                        <table>
                                            <tr>
                                                <td align="center">
                                                    <select id="tdoc" name="tdoc" onchange="controller(this.id);carga('<%=CONTROLLERCONTAB%>?estado=Gestionar&accion=Hc','CARGA_CUENTA','evento=CARGA_CUENTA&cmco='+$('hcs').value+'&cmcn='+$('hc').value+'&tdoc='+$('tdoc').value);">
                                                        <option value="...">...</option>
                                                    </select>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td align="center">
                                                    <select id="tdocs" name="tdocs" multiple onchange="controller(this.id);carga('<%=CONTROLLERCONTAB%>?estado=Gestionar&accion=Hc','CARGA_CUENTA','evento=CARGA_CUENTA&cmco='+$('hcs').value+'&cmcn='+$('hc').value+'&tdoc='+$('tdocs').value);">
                                                    </select>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                                <tr class="fila" id="trcuenta" style="visibility: hidden;display: none">
                                    <td>Cuenta Relacionada</td>
                                    <td align="center">
                                        <table>
                                            <tr>
                                                <td>
                                                    <input type="text" name="cuenta" id="cuenta" value="" style="width: 120px">
                                                </td>
                                            </tr>
                                        </table>
                                    </td>

                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
                <br>
                <img alt="" src = "<%=BASEURL%>/images/botones/aceptar.gif" style = "cursor:pointer" name = "imgaceptar" id="imgaceptar" onClick = "validar('<%=CONTROLLERCONTAB%>?estado=Gestionar&accion=Hc','INSERTAR_CMC','evento=INSERTAR_CMC&descripcion='+$('descripcion').value+'&hc='+$('hc').value+'&hcs='+$('hcs').value+'&tdoc='+$('tdoc').value+'&cuenta='+$('cuenta').value);" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);">
                <img alt="" src = "<%=BASEURL%>/images/botones/salir.gif" style = "cursor:pointer" name = "imgsalir" onClick = "window.close();" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);">
            </center>
        </div>
    </body>
</html>
