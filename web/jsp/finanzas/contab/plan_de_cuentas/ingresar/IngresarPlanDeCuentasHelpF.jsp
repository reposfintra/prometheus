<!--  
	 - Author(s)       :      LREALES
	 - Description     :      AYUDA FUNCIONAL - Ingresar Plan De Cuentas
	 - Date            :      06/06/2006 
	 - Copyright Notice:      TRANSPORTES SANCHEZ POLO S.A.
-->
<%@ include file="/WEB-INF/InitModel.jsp"%>
<HTML>
<HEAD>
<TITLE>AYUDA FUNCIONAL - Ingresar Plan De Cuentas</TITLE>
<META http-equiv=Content-Type content="text/html; charset=windows-1252">
<link href="../css/estilostsp.css" rel="stylesheet" type="text/css"> 
<link href="<%=BASEURL%>/css/estilostsp.css" rel="stylesheet" type="text/css">
<script type='text/javascript' src="<%= BASEURL %>/js/boton.js"></script> 
</HEAD>
<BODY> 
<% String BASEIMG = BASEURL +"/images/ayuda/contabilidad/plan_de_cuentas/ingresar/"; %>
  <table width="95%"  border="2" align="center">
    <tr>
      <td height="117" >
        <table width="100%" border="0" align="center">
          <tr  class="subtitulo">
            <td height="20"><div align="center">MANUAL DE PLAN DE CUENTAS WEB</div></td>
          </tr>
          <tr class="subtitulo1">
            <td>Descripci&oacute;n del funcionamiento del programa para Ingresar Plan De Cuentas.</td>
          </tr>
          <tr>
            <td  class="ayudaHtmlTexto"><div align="center">
              <p>&nbsp;</p>
              </div></td>
          </tr>
<tr>
            <td  class="ayudaHtmlTexto"><p class="ayudaHtmlTexto">En la siguiente pantalla se ingresan los datos de la cuenta a agregar.</p>
            </td>
          </tr>
<tr>
            <td  class="ayudaHtmlTexto"><div align="center"><IMG src="<%=BASEIMG%>Dibujo1.PNG"></div></td>
          </tr>
<tr>
  <td  class="ayudaHtmlTexto">&nbsp;</td>
</tr>
<tr>
  <td  class="ayudaHtmlTexto">Es posible agregar nuevas cuentas adicionales, variando el Elemento del Gasto. Para ello podemos hacer clic en el v&iacute;nculo '

 
    Adicionar otros elementos  ' y seleccionar los los elementos del gasto adicionales. Esto solo se prodr&aacute; llevar a cabo siempre y cuando la cuenta empoeze por 'I', 'C' o 'G' y la longitud sea 13. </td>
</tr>
<tr>
  <td  class="ayudaHtmlTexto"><div align="center"><IMG src="<%=BASEIMG%>Dibujo2.PNG"></div></td>
</tr>
<tr>
            <td  class="ayudaHtmlTexto"><p class="ayudaHtmlTexto">&nbsp;</p>
              <p class="ayudaHtmlTexto">El sistema verifica que todos los campos obligatorios esten llenos, si no lo estan le saldr&aacute; en la pantalla el siguiente mensaje. </p>
              </td>
          </tr>
<tr>
  <td  class="ayudaHtmlTexto"><div align="center"><IMG height=126 src="<%=BASEIMG%>image_error001.JPG" width=320 border=0 v:shapes="_x0000_i1054"></div></td>
</tr>
<tr>
  <td  class="ayudaHtmlTexto"><p>&nbsp;</p>
    <p>Si la cuenta es auxiliar y usted ingresa una cuenta dependiente el sistema le mostrar&aacute; el siguiente mensaje.</p></td>
</tr>
<tr>
  <td  class="ayudaHtmlTexto"><div align="center"><IMG height=126 src="<%=BASEIMG%>image_error002.JPG" width=460 border=0 v:shapes="_x0000_i1054"></div></td>
</tr>
<tr>
  <td  class="ayudaHtmlTexto"><p>&nbsp;</p>
    <p>El c&oacute;digo  debe ser numerico si comienza con un n&uacute;mero, &oacute; puede ser alfanumerico si comienza con
la letra 'I', 'C', &oacute; 'G'.<br>
De lo contrario en la pantalla nos saldr&aacute; lo siguiente.</p>    </td>
</tr>
<tr>
  <td  class="ayudaHtmlTexto"><div align="center"><IMG height=51 src="<%=BASEIMG%>image_error003.JPG" width=351 border=0 v:shapes="_x0000_i1054"></div></td>
</tr>
<tr>
  <td  class="ayudaHtmlTexto"><p>&nbsp;</p>
    <p>La cuenta dependiente puede ser vacia si la cuenta tiene un solo digito y este es numerico.<br>
      Por el contrario el sistema nos arrojara este mensaje.</p></td>
</tr>
<tr>
  <td  class="ayudaHtmlTexto"><div align="center"><IMG height=50 src="<%=BASEIMG%>image_error004.JPG" width=351 border=0 v:shapes="_x0000_i1054"></div></td>
</tr>
<tr>
  <td  class="ayudaHtmlTexto"><p>&nbsp;</p>
    <p>Si la cuenta comienza con 'I', 'C', 'G', '4', '5' &oacute; '6', tiene que tener cuenta de cierre.<br>
      Si no es asi, la pantalla nos mostrar&aacute; el mensaje siguiente.</p></td>
</tr>
<tr>
  <td  class="ayudaHtmlTexto"><div align="center"><IMG height=41 src="<%=BASEIMG%>image_error005.JPG" width=350 border=0 v:shapes="_x0000_i1054"></div></td>
</tr>
<tr>
  <td  class="ayudaHtmlTexto"><p>&nbsp;</p>
    <p>Si el c&oacute;digo digitado ya esta ingresado en nuestra base de datos, entonces el sistema le mostrar&aacute; el siguiente mensaje.</p></td>
</tr>
<tr>
  <td  class="ayudaHtmlTexto"><div align="center"><IMG height=51 src="<%=BASEIMG%>image_error006.JPG" width=350 border=0 v:shapes="_x0000_i1054"></div></td>
</tr>
<tr>
  <td  class="ayudaHtmlTexto"><p>&nbsp;</p>
    <p>Si la cuenta de cierre no existe en nuestra base de datos, el sistema arrojar&aacute; el siguiente mensaje.</p></td>
</tr>
<tr>
  <td  class="ayudaHtmlTexto"><p align="center"><IMG height=49 src="<%=BASEIMG%>image_error007.JPG" width=349 border=0 v:shapes="_x0000_i1054"></p>    </td>
</tr>
<tr>
  <td  class="ayudaHtmlTexto"><p>&nbsp;</p>
    <p>Si la cuenta dependiente no existe en nuestra base de datos, en la pantalla el sistema va a mostrar el siguiente mensaje.</p></td>
</tr>
<tr>
  <td  class="ayudaHtmlTexto"><div align="center"><IMG height=50 src="<%=BASEIMG%>image_error008.JPG" width=349 border=0 v:shapes="_x0000_i1054"></div></td>
</tr>
<tr>
  <td  class="ayudaHtmlTexto"><p>&nbsp;</p>
    <p>Si en la pantalla le sale el siguiente mensaje, es debido a que si la cuenta comienza con 'I', 'C', &oacute; 'G', siempre deben tener 13 caracteres. </p></td>
</tr>
<tr>
  <td  class="ayudaHtmlTexto"><div align="center"><IMG height=63 src="<%=BASEIMG%>image_error009.JPG" width=350 border=0 v:shapes="_x0000_i1054"></div></td>
</tr>
<tr>
  <td  class="ayudaHtmlTexto"><p>&nbsp;</p>
    <p>Si el c&oacute;digo de la agencia, que esta incluido en el c&oacute;digo de la cuenta, no existe en nuestra base de datos, entonces el sistema le mostrar&aacute; el siguiente mensaje. </p></td>
</tr>
<tr>
  <td  class="ayudaHtmlTexto"><div align="center"><IMG height=50 src="<%=BASEIMG%>image_error010.JPG" width=350 border=0 v:shapes="_x0000_i1054"></div></td>
</tr>
<tr>
  <td  class="ayudaHtmlTexto"><p>&nbsp;</p>
    <p>Si el c&oacute;digo del cliente o de la secci&oacute;n, que esta incluido en el c&oacute;digo de la cuenta, no existe en nuestra base de datos, entonces el sistema le mostrar&aacute; el siguiente mensaje. </p></td>
</tr>
<tr>
  <td  class="ayudaHtmlTexto"><div align="center"><IMG height=64 src="<%=BASEIMG%>image_error011.JPG" width=350 border=0 v:shapes="_x0000_i1054"></div></td>
</tr>
<tr>
  <td  class="ayudaHtmlTexto"><p>&nbsp;</p>
    <p>Si el c&oacute;digo de la unidad de negocio, que esta incluido en el c&oacute;digo de la cuenta, no existe en nuestra base de datos, entonces el sistema le mostrar&aacute; el siguiente mensaje. </p></td>
</tr>
<tr>
  <td  class="ayudaHtmlTexto"><div align="center"><IMG height=64 src="<%=BASEIMG%>image_error012.JPG" width=349 border=0 v:shapes="_x0000_i1054"></div></td>
</tr>
<tr>
  <td  class="ayudaHtmlTexto"><p>&nbsp;</p>
    <p>Si el c&oacute;digo del elemento del gasto, que esta incluido en el c&oacute;digo de la cuenta, no existe en nuestra base de datos, entonces el sistema le mostrar&aacute; el siguiente mensaje. </p></td>
</tr>
<tr>
  <td  class="ayudaHtmlTexto"><div align="center"><IMG height=63 src="<%=BASEIMG%>image_error013.JPG" width=350 border=0 v:shapes="_x0000_i1054"></div></td>
</tr>
<tr>
  <td  class="ayudaHtmlTexto"><p>&nbsp;</p>
    <p>Si en la pantalla le sale el siguiente mensaje, es debido a que si la cuenta comienza con 'I', 'C', &oacute; 'G', no debe tener ni auxiliar, ni subledger, ni tercero.</p></td>
</tr>
<tr>
  <td  class="ayudaHtmlTexto"><div align="center"><IMG height=49 src="<%=BASEIMG%>image_error014.JPG" width=349 border=0 v:shapes="_x0000_i1054"></div></td>
</tr>
<tr>
  <td  class="ayudaHtmlTexto"><p>&nbsp;</p>
    <p>Si en la pantalla le sale el siguiente mensaje, es debido a que si la cuenta comienza con 'I', 'C', &oacute; 'G', no debe tener cuenta dependiente.<br>
      &oacute; si la cuenta es igual a 1, 2, 3, 4, 5, 6, 7, 8, &oacute; 9 tampoco debe tener cuenta dependiente.
    </p>    </td>
</tr>
<tr>
  <td  class="ayudaHtmlTexto"><div align="center"><IMG height=50 src="<%=BASEIMG%>image_error015.JPG" width=349 border=0 v:shapes="_x0000_i1054"></div></td>
</tr>
<tr>
  <td  class="ayudaHtmlTexto"><p>&nbsp;</p>
    <p>Si en la pantalla le sale el siguiente mensaje, es debido a que la cuenta dependiente ingresada es una cuenta de detalle. </p>    </td>
</tr>
<tr>
  <td  class="ayudaHtmlTexto"><div align="center"><IMG height=85 src="<%=BASEIMG%>image_error016.JPG" width=406 border=0 v:shapes="_x0000_i1054"></div></td>
</tr>
<tr>
  <td  class="ayudaHtmlTexto"><p>&nbsp;</p>
    <p>Cuando alla echo el procedimiento de ingreso correctamente, en la pantalla el sistema nos mostrar&aacute; lo siguiente. </p></td>
</tr>
<tr>
            <td  class="ayudaHtmlTexto"><div align="center"><IMG height=92 src="<%=BASEIMG%>image002.JPG" width=936 border=0 v:shapes="_x0000_i1054"></div></td>
          </tr>
      </table></td>
    </tr>
  </table>
  <p></p>
<center>
<img src = "<%=BASEURL%>/images/botones/salir.gif" style = "cursor:hand" name = "imgsalir" onClick = "parent.close();" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);">
</center>
</BODY>
</HTML>
