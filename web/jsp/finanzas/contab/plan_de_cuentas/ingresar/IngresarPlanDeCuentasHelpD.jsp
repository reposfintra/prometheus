<!--  
	 - Author(s)       :      LREALES
	 - Description     :      AYUDA DESCRIPTIVA - Ingresar Plan De Cuentas
	 - Date            :      06/06/2006 
	 - Copyright Notice:      TRANSPORTES SANCHEZ POLO S.A.
-->
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN""http://www.w3.org/TR/html4/loose.dtd">
<%@page session="true"%> 
<%@page errorPage="/error/ErrorPage.jsp"%>
<%@page import="com.tsp.operation.model.beans.*, java.util.*"%>
<%@ include file="/WEB-INF/InitModel.jsp"%>
<%@ taglib uri="http://jakarta.apache.org/taglibs/input-0.90" prefix="input" %>
<html>
<head>
<title>AYUDA DESCRIPTIVA - Ingresar Plan De Cuentas</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link href="<%= BASEURL %>/css/estilostsp.css" rel="stylesheet" type="text/css">
<script type='text/javascript' src="<%= BASEURL %>/js/boton.js"></script>
</head>

<body>
<br>
<table width="696"  border="2" align="center">
  <tr>
    <td width="811" >
      <table width="100%" border="0" align="center">
        <tr  class="subtitulo">
          <td height="24" colspan="2"><div align="center">Plan De Cuentas</div></td>
        </tr>
        <tr class="subtitulo1">
          <td colspan="2">Ingresar Plan De Cuentas - Pantalla Preliminar</td>
        </tr>
		<tr class="subtitulo1">
          <td colspan="2">INFORMACION DE LA CUENTA </td>
        </tr>
        <tr>
          <td  class="fila">'Cuenta'</td>
          <td  class="ayudaHtmlTexto">Campo para digitar el c&oacute;digo de la cuenta que deseo agregar. Este campo es de m&aacute;ximo 25 caracteres. </td>
        </tr>
        <tr>
          <td  class="fila">'Nombre Largo'</td>
          <td  class="ayudaHtmlTexto">Campo de texto donde ingreso el nombre largo de la cuenta. Este campo es de m&aacute;ximo 30 caracteres. </td>
        </tr>
        <tr>
          <td  class="fila">'Nombre Corto'</td>
          <td  class="ayudaHtmlTexto">Campo de texto donde ingreso el nombre corto de la cuenta. Este campo es de m&aacute;ximo 15 caracteres. </td>
        </tr>
        <tr>
          <td  class="fila">'Observaci&oacute;n'</td>
          <td  class="ayudaHtmlTexto">Campo de texto donde ingreso la observacion de la cuenta. Este campo es de m&aacute;ximo 200 caracteres. </td>
        </tr>
        <tr>
          <td class="fila">'Nivel N&deg;' </td>
          <td  class="ayudaHtmlTexto">Campo de selecci&oacute;n donde escojo el nivel de la cuenta. Hasta el momento existen 6 niveles. </td>
        </tr>
        <tr>
          <td class="fila">'Auxiliar?'</td>
          <td  class="ayudaHtmlTexto">Campo de selecci&oacute;n donde escojo si la cuenta es o no una cuenta auxiliar. </td>
        </tr>
        <tr>
          <td class="fila">'Subledger?'</td>
          <td  class="ayudaHtmlTexto">Campo de selecci&oacute;n donde escojo si la cuenta posee o no subledgers. </td>
        </tr>
        <tr>
          <td class="fila">'Detalle?'</td>
          <td  class="ayudaHtmlTexto">Campo de selcci&oacute;n donde escojo si es una cuenta de detalle o no. </td>
        </tr>
        <tr>
          <td class="fila">'Tercero?'</td>
          <td  class="ayudaHtmlTexto">Campo de selecci&oacute;n donde escojo si para esta cuenta terceros 'no aplica', es 'mandatorio' u 'opcional'. </td>
        </tr>
        <tr>
          <td class="fila">'Cuenta Dependiente' </td>
          <td  class="ayudaHtmlTexto">Campo para digitar el c&oacute;digo de la cuenta dependiente. Este campo es de m&aacute;ximo 25 caracteres. </td>
        </tr>
        <tr>
          <td class="fila">'Cuenta de Cierre'</td>
          <td  class="ayudaHtmlTexto">Campo para digitar el c&oacute;digo de la cuenta de cierre. Este campo es de m&aacute;ximo 25 caracteres. </td>
        </tr>
        <tr>
          <td class="fila">'Modulo _' </td>
          <td  class="ayudaHtmlTexto">Campo de selecci&oacute;n donde escojo los modulos que posee la cuenta. </td>
        </tr>
        <tr>
          <td class="fila">Bot&oacute;n Agregar</td>
          <td  class="ayudaHtmlTexto">Bot&oacute;n para confirmar y realizar el ingreso de la cuenta.</td>
        </tr>
		<tr>
          <td class="fila">Bot&oacute;n Cancelar </td>
          <td  class="ayudaHtmlTexto">Bot&oacute;n que resetea la pantalla dejandola en su estado inicial de ingreso.</td>
        </tr>
		<tr>
          <td width="149" class="fila">Bot&oacute;n Salir</td>
          <td width="525"  class="ayudaHtmlTexto">Bot&oacute;n para salir de la vista 'Ingresar Plan De Cuentas' y volver a la vista del men&uacute;.</td>
        </tr>
      </table>
    </td>
  </tr>
</table>
<p></p>
<center>
<img src = "<%=BASEURL%>/images/botones/salir.gif" style = "cursor:hand" name = "imgsalir" onClick = "parent.close();" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);">
</center>
</body>
</html>
