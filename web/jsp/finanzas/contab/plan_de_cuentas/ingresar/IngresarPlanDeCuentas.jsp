<!--
- Nombre P�gina :                  IngresarPlanDeCuentas.jsp                 
- Descripci�n :                    Pagina JSP, que ingresa una cuenta                     
- Autor :                          LREALES                        
- Fecha Creado :                   6 de Junio de 2006, 10:30 A.M.                        
- Versi�n :                        1.0                                       
- Copyright :                      Fintravalores S.A.                   
-->                    

<%-- Declaracion de librerias--%>
<%@page contentType="text/html"%>
<%@page session="true"%>
<%@page errorPage="../error/ErrorPage.jsp"%>
<%@page import="com.tsp.operation.model.*"%>
<%@page import="com.tsp.operation.model.beans.*, java.util.*"%>
<%@page import="com.tsp.util.*"%>
<%@page import="com.tsp.finanzas.contab.model.*"%>
<%@page import="com.tsp.finanzas.contab.model.beans.*, java.util.*"%>
<%@include file="/WEB-INF/InitModel.jsp"%>
<%@ taglib uri="http://jakarta.apache.org/taglibs/input-0.90" prefix="input" %>
<%
  String path = application.getRealPath(model.menuService.getRealUrl(request.getRequestURI()));
  String datos[] = model.menuService.getContenidoMenu(BASEURL,request.getRequestURI(),path);
%>
<html>
<head>
<title>Ingresar Plan De Cuentas</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link href="<%= BASEURL %>/css/estilostsp.css" rel="stylesheet" type="text/css">
<link href="/css/estilostsp.css" rel="stylesheet" type="text/css"> 
<script type='text/javascript' src="<%= BASEURL %>/js/boton.js"></script>
<script>
function Completar(acount,BASEURL){

        var url = "?estado=InsertPlanDe&accion=Cuentas&acount="+acount+"&op=VERIFICAR_CUENTA";
	enviar(url);
        

}   
</script>
</head>
<script>
		var controlador ="<%=CONTROLLERCONTAB%>";
		
		function Agregar ( cuenta, nom_largo, nom_corto, observacion, cta_dependiente, cta_cierre, auxiliar ){
		
			if( ( cuenta != "" ) && ( nom_largo != "" ) && ( nom_corto != "" ) ){
			
				if ( ( ( cta_dependiente != "" ) && ( auxiliar == "N" ) ) || ( cta_dependiente == "" ) ) {
					document.forma.action = controlador+"?estado=Insert&accion=PlanDeCuentas&cuenta="+cuenta+"&nom_largo="+nom_largo+"&nom_corto="+nom_corto+"&observacion="+observacion+"&cta_dependiente="+cta_dependiente+"&cta_cierre="+cta_cierre;
					//document.forma.submit();
					return true;
				} else {
					alert( "La cuenta ingresada no debe ser auxiliar!, o no debe tener cuenta dependiente!" );
					return false;
				}
				
			} else{
			
				alert( "Por Favor No Deje Los Campos Obligatorios Vacios!" );
				return false;
			}
		}
</script>

<body onload="redimensionar();" onresize="redimensionar()">
<div id="capaSuperior" style="position:absolute; width:100%; height:100px; z-index:0; left: 0px; top: 0px;">
	<jsp:include page="/WEB-INF/toptsp.jsp?encabezado=Plan De Cuentas"/>
</div>
<div id="capaCentral" style="position:absolute; width:100%; height:87%; z-index:0; left: 0px; top: 100px; overflow: scroll;">
<%
	String cu = ( (String)session.getAttribute("cuenta") != null )?(String)session.getAttribute("cuenta"):"";
	String nl = ( (String)session.getAttribute("nom_largo") != null )?(String)session.getAttribute("nom_largo"):"";
	String nc = ( (String)session.getAttribute("nom_corto") != null )?(String)session.getAttribute("nom_corto"):"";
	String ob = ( (String)session.getAttribute("observacion") != null )?(String)session.getAttribute("observacion"):"";
	String au = ( (String)session.getAttribute("auxiliar") != null )?(String)session.getAttribute("auxiliar"):"N";
	String ni = ( (String)session.getAttribute("nivel") != null )?(String)session.getAttribute("nivel"):"1";
	String su = ( (String)session.getAttribute("subledger") != null )?(String)session.getAttribute("subledger"):"N";
	String te = ( (String)session.getAttribute("tercero") != null )?(String)session.getAttribute("tercero"):"N";
	String cd = ( (String)session.getAttribute("cta_dependiente") != null )?(String)session.getAttribute("cta_dependiente"):"";
	String cc = ( (String)session.getAttribute("cta_cierre") != null )?(String)session.getAttribute("cta_cierre"):"";
	String m1 = ( (String)session.getAttribute("modulo1") != null )?(String)session.getAttribute("modulo1"):"S";
	String m2 = ( (String)session.getAttribute("modulo2") != null )?(String)session.getAttribute("modulo2"):"S";
	String m3 = ( (String)session.getAttribute("modulo3") != null )?(String)session.getAttribute("modulo3"):"S";
	String m4 = ( (String)session.getAttribute("modulo4") != null )?(String)session.getAttribute("modulo4"):"S";
	String m5 = ( (String)session.getAttribute("modulo5") != null )?(String)session.getAttribute("modulo5"):"S";
	String m6 = ( (String)session.getAttribute("modulo6") != null )?(String)session.getAttribute("modulo6"):"S";
	String m7 = ( (String)session.getAttribute("modulo7") != null )?(String)session.getAttribute("modulo7"):"S";
	String m8 = ( (String)session.getAttribute("modulo8") != null )?(String)session.getAttribute("modulo8"):"S";
	String m9 = ( (String)session.getAttribute("modulo9") != null )?(String)session.getAttribute("modulo9"):"S";
	String m10 = ( (String)session.getAttribute("modulo10") != null )?(String)session.getAttribute("modulo10"):"S";
	
	String de = ( (String)session.getAttribute("detalle") != null )?(String)session.getAttribute("detalle"):"N";
%>

<form name="forma"  method="post">
  <table width="95%"  border="2" align="center">
  	<tr>
    	<td>
			<table width="100%" align="center">
				<tr>
					<td colspan="5" nowrap class="subtitulo1">Ingresar Informaci&oacute;n de la Cuenta</td>
					<td colspan="5" class="barratitulo"><img src="<%=BASEURL%>/images/titulo.gif" width="32" height="20" align="left"><%=datos[0]%></td>
				</tr>
				<tr class="fila">
				  <td width="12%">Cuenta</td>
				  <td colspan="9"><input name="cuenta" value="<%=cu%>" type="text" id="cuenta" size="25" maxlength="25" onBlur="Completar(this.value,'<%=BASEURL%>')">
				  <img src="<%=BASEURL%>/images/botones/iconos/obligatorio.gif"> <a href="JavaScript:void(0);" class="Simulacion_Hiper"  onClick="window.open('<%= CONTROLLERCONTAB %>?estado=PlanDeCuentas&accion=CargarELEM&cuenta=' + document.forma.cuenta.value,'DETALL','status=yes,scrollbars=no,width=650,height=650,resizable=yes');">Adicionar otros elementos </a></td>
			  </tr>
				<tr class="fila">
				  <td>Nombre Largo </td>
				  <td width="38%" colspan="4"><input name="nom_largo" value="<%=nl%>" type="text" id="nom_largo" size="55" maxlength="49">
			      <img src="<%=BASEURL%>/images/botones/iconos/obligatorio.gif"></td>
				  <td width="12%">Nombre Corto </td>
			      <td width="38%" colspan="4"><input name="nom_corto" value="<%=nc%>" type="text" id="nom_corto" size="30" maxlength="15">
		          <img src="<%=BASEURL%>/images/botones/iconos/obligatorio.gif"></td>
			  </tr>
				<tr class="fila">
				  <td>Observaci&oacute;n</td>
				  <td colspan="9"><input name="observacion" value="<%=ob%>" type="text" class="textbox" id="observacion" size="100" maxlength="200">
				    </td>
		        </tr>
				<tr class="fila">
				  <td>Nivel N&deg; </td>
				  <td>
				  <select name="nivel" id="nivel">
					  <%if (ni.equals("1")){%>
					  <option value="1" selected>1</option>
				      <option value="2">2</option>
				      <option value="3">3</option>
				      <option value="4">4</option>
				      <option value="5">5</option>
				      <option value="6">6</option>
					  <option value="7">7</option>
					  <option value="8">8</option>
					  <option value="9">9</option>
					  <%} else if (ni.equals("2")){%>
					  <option value="1">1</option>
				      <option value="2" selected>2</option>
				      <option value="3">3</option>
				      <option value="4">4</option>
				      <option value="5">5</option>
				      <option value="6">6</option>
					  <option value="7">7</option>
					  <option value="8">8</option>
					  <option value="9">9</option>
					  <%} else if (ni.equals("3")){%>
					  <option value="1">1</option>
				      <option value="2">2</option>
				      <option value="3" selected>3</option>
				      <option value="4">4</option>
				      <option value="5">5</option>
				      <option value="6">6</option>
					  <option value="7">7</option>
					  <option value="8">8</option>
					  <option value="9">9</option>
					  <%} else if (ni.equals("4")){%>
					  <option value="1">1</option>
				      <option value="2">2</option>
				      <option value="3">3</option>
				      <option value="4" selected>4</option>
				      <option value="5">5</option>
				      <option value="6">6</option>
					  <option value="7">7</option>
					  <option value="8">8</option>
					  <option value="9">9</option>
					  <%} else if (ni.equals("5")){%>
					  <option value="1">1</option>
				      <option value="2">2</option>
				      <option value="3">3</option>
				      <option value="4">4</option>
				      <option value="5" selected>5</option>
				      <option value="6">6</option>
					  <option value="7">7</option>
					  <option value="8">8</option>
					  <option value="9">9</option>
					  <%} else if (ni.equals("6")){%>
					  <option value="1">1</option>
				      <option value="2">2</option>
				      <option value="3">3</option>
				      <option value="4">4</option>
				      <option value="5">5</option>
				      <option value="6" selected>6</option>
					  <option value="7">7</option>
					  <option value="8">8</option>
					  <option value="9">9</option>
					  <%} else if (ni.equals("7")){%>
					  <option value="1">1</option>
				      <option value="2">2</option>
				      <option value="3">3</option>
				      <option value="4">4</option>
				      <option value="5">5</option>
				      <option value="6">6</option>
					  <option value="7" selected>7</option>
					  <option value="8">8</option>
					  <option value="9">9</option>
					  <%} else if (ni.equals("8")){%>
					  <option value="1">1</option>
				      <option value="2">2</option>
				      <option value="3">3</option>
				      <option value="4">4</option>
				      <option value="5">5</option>
				      <option value="6">6</option>
					  <option value="7">7</option>
					  <option value="8" selected>8</option>
					  <option value="9">9</option>
					  <%} else if (ni.equals("9")){%>
					  <option value="1">1</option>
				      <option value="2">2</option>
				      <option value="3">3</option>
				      <option value="4">4</option>
				      <option value="5">5</option>
				      <option value="6">6</option>
					  <option value="7">7</option>
					  <option value="8">8</option>
					  <option value="9" selected>9</option>
					  <%} else{%>
					  <option value="1" selected>1</option>
				      <option value="2">2</option>
				      <option value="3">3</option>
				      <option value="4">4</option>
				      <option value="5">5</option>
				      <option value="6">6</option>
					  <option value="7">7</option>
					  <option value="8">8</option>
					  <option value="9">9</option>
					  <%}%>
				  </select>
				  </td>
				  <td>Auxiliar?</td>
				  <td>
				  <select name="auxiliar" id="auxiliar">
					  <%if (au.equals("S")){%>
					  <option value="S" selected>SI</option>
                      <option value="N">NO</option>
					  <%} else if (au.equals("N")){%>
					  <option value="S">SI</option>
                      <option value="N" selected>NO</option>
					  <%} else{%>
					  <option value="S">SI</option>
                      <option value="N" selected>NO</option>
					  <%}%>
				  </select>				  </td>
				  <td>Subledger?
				  
				  </td>
				  <td>
				  <select name="subledger" id="subledger">
					  <%if (su.equals("S")){%>
					  <option value="S" selected>SI</option>
                      <option value="N">NO</option>
					  <%} else if (su.equals("N")){%>
					  <option value="S">SI</option>
                      <option value="N" selected>NO</option>
					  <%} else{%>
					  <option value="S">SI</option>
                      <option value="N" selected>NO</option>
					  <%}%>
				  </select>
				  </td>
			      <td>Detalle?
				  
				  </td>
			      <td>
				  <select name="detalle" id="detalle">
                    <%if (de.equals("S")){%>
                    <option value="S" selected>SI</option>
                    <option value="N">NO</option>
                    <%} else if (de.equals("N")){%>
                    <option value="S">SI</option>
                    <option value="N" selected>NO</option>
                    <%} else{%>
                    <option value="S">SI</option>
                    <option value="N" selected>NO</option>
                    <%}%>
                  </select>
				  </td>
			      <td>Tercero?</td>
			      <td><select name="tercero" id="tercero">
					  <%if (te.equals("N")){%>
					  <option value="N" selected>No Aplica</option>
			          <option value="M">Mandatorio</option>
			          <option value="O">Opcional</option>
					  <%} else if (te.equals("M")){%>
					  <option value="N">No Aplica</option>
			          <option value="M" selected>Mandatorio</option>
			          <option value="O">Opcional</option>
					  <%} else if (te.equals("O")){%>
					  <option value="N">No Aplica</option>
			          <option value="M">Mandatorio</option>
			          <option value="O" selected>Opcional</option>
					  <%} else{%>
					  <option value="N" selected>No Aplica</option>
			          <option value="M">Mandatorio</option>
			          <option value="O">Opcional</option>
					  <%}%>
				  </select></td>
			  </tr>
				<tr class="fila">
				  <td colspan="3">Cuenta Dependiente </td>
				  <td colspan="2"><input name="cta_dependiente" value="<%=cd%>" type="text" id="cta_dependiente" size="25" maxlength="25"></td>
				  <td colspan="3">Cuenta de Cierre </td>
			      <td colspan="2"><input name="cta_cierre" value="<%=cc%>" type="text" id="cta_cierre" size="25" maxlength="25"></td>
		      </tr>
				<tr class="fila">
				  <td>CD </td>
				  <td>
				  <select name="modulo1" id="modulo1">
					  <%if (m1.equals("S")){%>
					  <option value="S" selected>SI</option>
                      <option value="N">NO</option>
					  <%} else if (m1.equals("N")){%>
					  <option value="S">SI</option>
                      <option value="N" selected>NO</option>
					  <%} else{%>
					  <option value="S" selected>SI</option>
                      <option value="N">NO</option>
					  <%}%>
				  </select>
				  </td>
				  <td>CxP </td>
				  <td>
				  <select name="modulo2" id="modulo2">
					  <%if (m2.equals("S")){%>
					  <option value="S" selected>SI</option>
                      <option value="N">NO</option>
					  <%} else if (m2.equals("N")){%>
					  <option value="S">SI</option>
                      <option value="N" selected>NO</option>
					  <%} else{%>
					  <option value="S" selected>SI</option>
                      <option value="N">NO</option>
					  <%}%>
				  </select>
				  </td>
				  <td>CxC </td>
				  <td>
				  <select name="modulo3" id="modulo3">
					  <%if (m3.equals("S")){%>
					  <option value="S" selected>SI</option>
                      <option value="N">NO</option>
					  <%} else if (m3.equals("N")){%>
					  <option value="S">SI</option>
                      <option value="N" selected>NO</option>
					  <%} else{%>
					  <option value="S" selected>SI</option>
                      <option value="N">NO</option>
					  <%}%>
				  </select>
				  </td>
				  <td>Proveedor </td>
				  <td>
				  <select name="modulo4" id="modulo4">
					  <%if (m4.equals("S")){%>
					  <option value="S" selected>SI</option>
                      <option value="N">NO</option>
					  <%} else if (m4.equals("N")){%>
					  <option value="S">SI</option>
                      <option value="N" selected>NO</option>
					  <%} else{%>
					  <option value="S" selected>SI</option>
                      <option value="N">NO</option>
					  <%}%>
				  </select>
				  </td>
				  <td>Modulo 5 </td>
				  <td>
				  <select name="modulo5" id="modulo5">
					  <%if (m5.equals("S")){%>
					  <option value="S" selected>SI</option>
                      <option value="N">NO</option>
					  <%} else if (m5.equals("N")){%>
					  <option value="S">SI</option>
                      <option value="N" selected>NO</option>
					  <%} else{%>
					  <option value="S" selected>SI</option>
                      <option value="N">NO</option>
					  <%}%>
				  </select>
				  </td>
			  </tr>
				<tr class="fila">
				  <td>Modulo 6</td>
				  <td>
				  <select name="modulo6" id="modulo6">
					  <%if (m6.equals("S")){%>
					  <option value="S" selected>SI</option>
                      <option value="N">NO</option>
					  <%} else if (m6.equals("N")){%>
					  <option value="S">SI</option>
                      <option value="N" selected>NO</option>
					  <%} else{%>
					  <option value="S" selected>SI</option>
                      <option value="N">NO</option>
					  <%}%>
				  </select>
				  </td>
				  <td>Modulo 7 </td>
				  <td>
				  <select name="modulo7" id="modulo7">
					  <%if (m7.equals("S")){%>
					  <option value="S" selected>SI</option>
                      <option value="N">NO</option>
					  <%} else if (m7.equals("N")){%>
					  <option value="S">SI</option>
                      <option value="N" selected>NO</option>
					  <%} else{%>
					  <option value="S" selected>SI</option>
                      <option value="N">NO</option>
					  <%}%>
				  </select>
				  </td>
				  <td>Modulo 8 </td>
				  <td>
				  <select name="modulo8" id="modulo8">
					  <%if (m8.equals("S")){%>
					  <option value="S" selected>SI</option>
                      <option value="N">NO</option>
					  <%} else if (m8.equals("N")){%>
					  <option value="S">SI</option>
                      <option value="N" selected>NO</option>
					  <%} else{%>
					  <option value="S" selected>SI</option>
                      <option value="N">NO</option>
					  <%}%>
				  </select>
				  </td>
			      <td>Modulo 9 </td>
			      <td>
				  <select name="modulo9" id="modulo9">
					  <%if (m9.equals("S")){%>
					  <option value="S" selected>SI</option>
                      <option value="N">NO</option>
					  <%} else if (m9.equals("N")){%>
					  <option value="S">SI</option>
                      <option value="N" selected>NO</option>
					  <%} else{%>
					  <option value="S" selected>SI</option>
                      <option value="N">NO</option>
					  <%}%>
				  </select>
				  </td>
			      <td>Modulo 10 </td>
			      <td>
				  <select name="modulo10" id="modulo10">
					  <%if (m10.equals("S")){%>
					  <option value="S" selected>SI</option>
                      <option value="N">NO</option>
					  <%} else if (m10.equals("N")){%>
					  <option value="S">SI</option>
                      <option value="N" selected>NO</option>
					  <%} else{%>
					  <option value="S" selected>SI</option>
                      <option value="N">NO</option>
					  <%}%>
				  </select>
				  </td>
			  </tr>
		  </table>
  		</td>
  	</tr>
  </table>
  <br>  
  <table align=center width='450'>
    <tr>
        <td align="center" colspan="2">
            <!-- input type="image" title='Agregar Cuenta' style = "cursor:hand" src="<%= BASEURL %>/images/botones/agregar.gif" onmouseover="botonOver(this);" onmouseout="botonOut(this);" ></img -->                        
			<img title='Agregar Cuenta' style = "cursor:hand" src="<%= BASEURL %>/images/botones/aceptar.gif" onmouseover="botonOver(this);" onmouseout="botonOut(this);" onClick="if(Agregar( cuenta.value, nom_largo.value, nom_corto.value, observacion.value, cta_dependiente.value, cta_cierre.value, auxiliar.value )){ forma.submit(); }"></img>
			<img title='Resetear' style = "cursor:hand" src="<%= BASEURL %>/images/botones/cancelar.gif" onmouseover="botonOver(this);" onmouseout="botonOut(this);" onClick="forma.reset();"></img>
            <img title='Salir' style = "cursor:hand" src="<%= BASEURL %>/images/botones/salir.gif" onmouseover="botonOver(this);" onmouseout="botonOut(this);" onClick="window.close()"></img>
        </td>
    </tr>
  </table> 
  <br>  
  <%String msg = request.getParameter("msg");
	if ( msg!=null && !msg.equals("") ){%>
                        
	  <table border="2" align="center">
   		<tr>
        	<td>
            	<table width="100%"  border="1" align="center"  bordercolor="#F7F5F4" bgcolor="#FFFFFF">
                	<tr>
                    	<td width="229" align="center" class="mensajes"><%=msg%></td>
                        <td width="29" background="<%=BASEURL%>/images/cuadronaranja.JPG">&nbsp;</td>
                        <td width="58">&nbsp;</td>
                    </tr>
                </table>
            </td>
		</tr>
	  </table>
	<%}%>
  </form>
</div>
<%=datos[1]%>
</body>
<script>
    function enviar(url){
         var a = "<iframe name='ejecutor' style='visibility:hidden'  src='<%= CONTROLLERCONTAB %>" + url + "'> ";
         aa.innerHTML = a;
    }
</script>
<font id='aa'></font>
</html>
