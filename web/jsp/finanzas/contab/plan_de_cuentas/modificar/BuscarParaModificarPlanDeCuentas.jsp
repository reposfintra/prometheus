<!--
- Nombre P�gina :                  BuscarParaModificarPlanDeCuentas.jsp                 
- Descripci�n :                    Pagina JSP, que buscar las cuentas a modificar                     
- Autor :                          LREALES                        
- Fecha Creado :                   6 de Junio de 2006, 07:50 P.M.                        
- Versi�n :                        1.0                                       
- Copyright :                      Fintravalores S.A.                   
-->
<%@page contentType="text/html"%>
<%@page session="true"%>
<%@page errorPage="/error/ErrorPage.jsp"%>
<%@page import="java.util.*"%>
<%@page import="com.tsp.operation.model.*"%>
<%@page import="com.tsp.util.*"%>
<%@page import="com.tsp.operation.model.beans.*, java.util.*"%>
<%@page import="com.tsp.finanzas.contab.model.*"%>
<%@page import="com.tsp.finanzas.contab.model.beans.*, java.util.*"%>
<%@include file="/WEB-INF/InitModel.jsp"%>
<%@taglib uri="http://jsptags.com/tags/navigation/pager" prefix="pg" %>
<%
String path = application.getRealPath(model.menuService.getRealUrl(request.getRequestURI()));
String datos[] = model.menuService.getContenidoMenu(BASEURL,request.getRequestURI(),path);
%>
<html>
<head>
<title>Modificar Plan De Cuentas</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link href="<%= BASEURL %>/css/estilostsp.css" rel="stylesheet" type="text/css">
<link href="../../css/estilostsp.css" rel="stylesheet" type="text/css"> 
<script type='text/javascript' src="<%= BASEURL %>/js/reporte.js"></script> 
<script type='text/javascript' src="<%= BASEURL %>/js/boton.js"></script>
</head>
<body onresize="redimensionar()" onload = "window.moveTo(-4,-4);window.resizeTo(screen.availWidth + 8, screen.availHeight + 8);">
<div id="capaSuperior" style="position:absolute; width:100%; height:100px; z-index:0; left: 0px; top: 0px;">
	<jsp:include page="/toptsp.jsp?encabezado=Plan De Cuentas"/>
</div>
<div id="capaCentral" style="position:absolute; width:100%; height:87%; z-index:0; left: 0px; top: 100px; overflow: scroll;">
		  <% 
		        
			String d = (String)session.getAttribute("distrito");
			String c = (String)session.getAttribute("cuenta");
			modelcontab.planDeCuentasService.searchDetalleCuenta( d, c );
			
		  Vector vec_cuentas = modelcontab.planDeCuentasService.getVec_cuentas();
	 
		  String index =  "center";
   	      int maxPageItems = 18;
          int maxIndexPages = 10;
	 
	      if( vec_cuentas != null  && vec_cuentas.size() > 0 ){ %>
	  <table width="100%" border="2" align=center>
    <tr>
        <td>
            <table width="100%" align="center" class="tablaInferior"> 
                <tr>
                    <td colspan='2'>                                                
                        <table width="100%"  border="0" cellpadding="0" cellspacing="0" class="barratitulo">
                            <tr>
                                <td width="30%" class="subtitulo1" colspan='3'>CUENTAS A MODIFICAR</td>
                                <td width="70%" class="barratitulo" colspan='2'><img src="<%=BASEURL%>/images/titulo.gif" align="left"><%=datos[0]%></td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td>
					<div class="scroll" style=" overflow:auto ; WIDTH: 100%; HEIGHT:100%; " >
                        <table width="100%" border="1" align="center" bordercolor="#999999" bgcolor="#F7F5F4">
                            <tr class="tblTitulo">
                                <td width="10%" rowspan="2" nowrap><div align="center">Cuenta </div></td>
                                <td width="10%" rowspan="2" nowrap><div align="center">Nombre Corto </div></td>
                                <td width="20%" rowspan="2" nowrap><div align="center">Nombre Largo </div></td>
                                <td width="5%" rowspan="2" nowrap align="center">Nivel</td>
                                <td width="5%" rowspan="2" nowrap align="center">Activa?</td>
                                <td width="5%" rowspan="2" nowrap align="center">Auxiliar?</td>
                                <td width="5%" rowspan="2" nowrap align="center">Subledger?</td>
                                <td width="5%" rowspan="2" nowrap align="center">Tercero?</td>
                                <td width="10%" rowspan="2" nowrap align="center">Cuenta <br>Dependiente </td>
                                <td width="10%" rowspan="2" nowrap align="center">Cuenta <br>de Cierre </td>
                                <td width="15%" colspan="10" nowrap><div align="center"><p>Modulos</p></div></td>
                            </tr>
                            <tr class="tblTitulo">
                              <td nowrap align="center">1</td>
                              <td nowrap align="center">2</td>
                              <td nowrap align="center">3</td>
                              <td nowrap align="center">4</td>
                              <td nowrap align="center">5</td>
                              <td nowrap align="center">6</td>
                              <td nowrap align="center">7</td>
                              <td nowrap align="center">8</td>
                              <td nowrap align="center">9</td>
                              <td nowrap align="center">10</td>
                            </tr>
							<pg:pager
							items="<%=vec_cuentas.size()%>"
							index="<%= index %>"
							maxPageItems="<%= maxPageItems %>"
							maxIndexPages="<%= maxIndexPages %>"
							isOffset="<%= true %>"
							export="offset,currentPageNumber=pageNumber"
							scope="request">
							  <%
							    for (int i = offset.intValue(), l = Math.min(i + maxPageItems, vec_cuentas.size()); i < l; i++){
								//for( int i = 0; i < vec_cuentas.size(); i++ ){
									PlanDeCuentas cuentas = ( PlanDeCuentas ) vec_cuentas.elementAt( i );
									
									String cue = ( cuentas.getCuenta().equals("")?"&nbsp;":cuentas.getCuenta() );
									String nom_lar = ( cuentas.getNombre_largo().equals("")?"&nbsp;":cuentas.getNombre_largo() );
									String nom_cor = ( cuentas.getNombre_corto().equals("")?"&nbsp;":cuentas.getNombre_corto() );
									String aux = ( cuentas.getAuxiliar().equals("S")?"Si":"No" );
									String act = ( cuentas.getActiva().equals("S")?"Si":"No" );
									String mod1 = ( cuentas.getModulo1().equals("S")?"X":"&nbsp;" );
									String mod2 = ( cuentas.getModulo2().equals("S")?"X":"&nbsp;" );
									String mod3 = ( cuentas.getModulo3().equals("S")?"X":"&nbsp;" );
									String mod4 = ( cuentas.getModulo4().equals("S")?"X":"&nbsp;" );
									String mod5 = ( cuentas.getModulo5().equals("S")?"X":"&nbsp;" );
									String mod6 = ( cuentas.getModulo6().equals("S")?"X":"&nbsp;" );
									String mod7 = ( cuentas.getModulo7().equals("S")?"X":"&nbsp;" );
									String mod8 = ( cuentas.getModulo8().equals("S")?"X":"&nbsp;" );
									String mod9 = ( cuentas.getModulo9().equals("S")?"X":"&nbsp;" );
									String mod10 = ( cuentas.getModulo10().equals("S")?"X":"&nbsp;" );
									String cta_dep = ( cuentas.getCta_dependiente().equals("")?"&nbsp;":cuentas.getCta_dependiente() );
									String niv = ( cuentas.getNivel().equals("")?"&nbsp;":cuentas.getNivel() );
									String cta_cie = ( cuentas.getCta_cierre().equals("")?"&nbsp;":cuentas.getCta_cierre() );
									String sub = ( cuentas.getSubledger().equals("S")?"Si":"No" );
									String ter = cuentas.getTercero();
									if ( ter.equals("N") ){
										ter = "No Aplica";
									} else if ( ter.equals("M") ){
										ter = "Mandatorio";
									} else if ( ter.equals("O") ){
										ter = "Opcional";
									} else {
										ter = "No Aplica";
									}
							  %>
							    <pg:item>
									<tr class="<%=(i % 2 == 0 )?"filagris":"filaazul"%>" onMouseOver='cambiarColorMouse(this)' style="cursor:hand" title="Modificar Cuenta..." onClick="window.location='<%=CONTROLLERCONTAB%>?estado=SearchUpdate&accion=PlanDeCuentas&listar=False&cuenta=<%=cue%>'; this.disabled=true;">
									  <td nowrap  align="left" abbr="" class="bordereporte"><%=cue%></td>
									  <td align="left" abbr="" nowrap class="bordereporte"><%=nom_cor%></td>
									  <td align="left" nowrap class="bordereporte" abbr=""><%=nom_lar%></td>
									  <td align="center" nowrap class="bordereporte" abbr=""><%=niv%></td>
									  <td align="center" nowrap class="bordereporte" abbr=""><%=act%></td>
									  <td align="center" nowrap class="bordereporte" abbr=""><%=aux%></td>
									  <td align="center" nowrap class="bordereporte" abbr=""><%=sub%></td>
									  <td align="center" nowrap class="bordereporte" abbr=""><%=ter%></td>
									  <td align="left" nowrap class="bordereporte" abbr=""><%=cta_dep%></td>
									  <td align="left" nowrap class="bordereporte" abbr=""><%=cta_cie%></td>
									  <td align="center" nowrap class="bordereporte" abbr=""><%=mod1%></td>
									  <td align="center" nowrap class="bordereporte" abbr=""><%=mod2%></td>
									  <td align="center" nowrap class="bordereporte" abbr=""><%=mod3%></td>
									  <td align="center" nowrap class="bordereporte" abbr=""><%=mod4%></td>
									  <td align="center" nowrap class="bordereporte" abbr=""><%=mod5%></td>
									  <td align="center" nowrap class="bordereporte" abbr=""><%=mod6%></td>
									  <td align="center" nowrap class="bordereporte" abbr=""><%=mod7%></td>
									  <td align="center" nowrap class="bordereporte" abbr=""><%=mod8%></td>
									  <td align="center" nowrap class="bordereporte" abbr=""><%=mod9%></td>
									  <td align="center" nowrap class="bordereporte" abbr=""><%=mod10%></td>
									</tr>
						  		</pg:item>
							<%}%>
							<tr class="pie">
								<td td height="20" colspan="27" nowrap align="center">          
									<pg:index>
										<jsp:include page="/WEB-INF/jsp/googlesinimg.jsp" flush="true"/>
									</pg:index>
								</td>
							</tr>
							</pg:pager> 
                        </table>
					  </div>
                    </td>
                </tr>
            </table>
        </td>
    </tr>
 </table>
 <br>
	<%}
	 else { %>                    
	  <%out.print("<table width=379 border=2 align=center><tr><td><table width=100%  border=1 align=center  bordercolor=#F7F5F4 bgcolor=#FFFFFF><tr><td width=350 align=center class=mensajes>Su busqueda no arrojo resultados!!</td><td width=100><img src="+BASEURL+"/images/cuadronaranja.JPG></td></tr></table></td></tr></table>");%>
	  <br>
	<%}%>
<table width='95%' align=center>
  <tr class="titulo">
    <td align=right colspan='2'>
        <div align="center">		
		<img title='Regresar al Filtro' src="<%= BASEURL %>/images/botones/regresar.gif" onClick = "window.location='<%=BASEURL%>/jsp/finanzas/contab/plan_de_cuentas/modificar/FiltroModificarPlanDeCuentas.jsp'; this.disabled=true;" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" style="cursor:hand">
		<img title='Salir al Menu' src="<%=BASEURL%>/images/botones/salir.gif"  name="imgsalir"  onMouseOver="botonOver(this);" onClick="window.close();" onMouseOut="botonOut(this);" style="cursor:hand">
        </div></td>
  </tr>
</table>
</div>
<%=datos[1]%>
</body>
</html>