<!--
- Nombre P�gina :                  BuscarParaModificarPlanDeCuentas.jsp                 
- Descripci�n :                    Pagina JSP, que buscar las cuentas a modificar                     
- Autor :                          LREALES                        
- Fecha Creado :                   6 de Junio de 2006, 07:50 P.M.                        
- Versi�n :                        1.0                                       
- Copyright :                      Fintravalores S.A.                   
-->
<%@page contentType="text/html"%>
<%@page session="true"%>
<%@page errorPage="/error/ErrorPage.jsp"%>
<%@page import="java.util.*"%>
<%@page import="com.tsp.operation.model.*"%>
<%@page import="com.tsp.util.*"%>
<%@ page import="java.lang.*" %>
<%@page import="com.tsp.operation.model.beans.*, java.util.*"%>
<%@page import="com.tsp.finanzas.contab.model.*"%>
<%@page import="com.tsp.finanzas.contab.model.beans.*, java.util.*"%>
<%@include file="/WEB-INF/InitModel.jsp"%>
<%
response.setHeader("Cache-Control","");
String tipo = request.getParameter("tipo")!=null?request.getParameter("tipo"):"consulta";
if ( tipo.equals("exel") ) {
    response.setContentType("application/vnd.ms-excel");
} 
%>
<html>
<head>
<title>Consulta Plan De Cuentas</title>
<% String pathr = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+request.getContextPath(); %>
<link href='<%=pathr%>/css/estilostsp.css' rel='stylesheet' type='text/css'>
<script type='text/javascript' src="<%= BASEURL %>/js/reporte.js"></script> 
<script type='text/javascript' src="<%= BASEURL %>/js/boton.js"></script>
</head>
<%if ( !tipo.equals("exel") ) {%>
	<body onLoad="redimensionar();" onresize="redimensionar()">
	<div id="capaSuperior" style="position:absolute; width:100%; height:100px; z-index:0; left: 0px; top: 0px;">
		<jsp:include page="/toptsp.jsp?encabezado=Plan De Cuentas"/>
	</div>
	<div id="capaCentral" style="position:absolute; width:100%; height:87%; z-index:0; left: 0px; top: 100px; overflow: scroll;">
<%}else{%>
	<body>
<% }
LinkedList vec_cuentas = modelcontab.planDeCuentasService.getLista();
if( vec_cuentas != null  && vec_cuentas.size() > 0 ){ %>
	  <table width="100%" border="2" align=center>
    <tr>
        <td>
			<table width="100%" align="center" class="tablaInferior"> 
				<%if ( !tipo.equals("exel") ) {%>
					<tr>
						<td colspan='2'>                                                
							<table width="100%"  border="0" cellpadding="0" cellspacing="0" class="barratitulo">
								<tr>
									<td width="50%" class="subtitulo1" colspan='3'>CUENTAS A CONSULTAR</td>
									<td width="50%" class="barratitulo" colspan='2'><img src="<%=BASEURL%>/images/titulo.gif" align="left"></td>
								</tr>
							</table>
						</td>
					</tr>
				<%}%>
                <tr>
                    <td>
					
                        <table width="100%" border="1" align="left" bordercolor="#999999" bgcolor="#F7F5F4">
                            <tr class="tblTitulo">
                                <td width="10%" rowspan="2" nowrap><div align="center">Cuenta </div></td>
                                <td width="10%" rowspan="2" nowrap><div align="center">Nombre Corto </div></td>
                                <td width="20%" rowspan="2" nowrap><div align="center">Nombre Largo </div></td>
                                <td width="5%" rowspan="2" nowrap align="center">Nivel</td>
                                <td width="5%" rowspan="2" nowrap align="center">Activa?</td>
                                <td width="5%" rowspan="2" nowrap align="center">Auxiliar?</td>
                                <td width="5%" rowspan="2" nowrap align="center">Subledger?</td>
                                <td width="5%" rowspan="2" nowrap align="center">Tercero?</td>
                                <td width="10%" rowspan="2" nowrap align="center">Cuenta <br>Dependiente </td>
                                <td width="10%" rowspan="2" nowrap align="center">Cuenta <br>de Cierre </td>
                                <td width="15%" colspan="10" nowrap><div align="center"><p>Modulos</p></div></td>
                            </tr>
                            <tr class="tblTitulo">
                              <td nowrap align="center">1</td>
                              <td nowrap align="center">2</td>
                              <td nowrap align="center">3</td>
                              <td nowrap align="center">4</td>
                              <td nowrap align="center">5</td>
                              <td nowrap align="center">6</td>
                              <td nowrap align="center">7</td>
                              <td nowrap align="center">8</td>
                              <td nowrap align="center">9</td>
                              <td nowrap align="center">10</td>
                            </tr>
							<%
							    Iterator it = vec_cuentas.iterator();
                                                            int i = 0;
                                                            while(it.hasNext()){
                                                                PlanDeCuentas cuentas = ( PlanDeCuentas ) it.next();
									
									String cue = ( cuentas.getCuenta().equals("")?"&nbsp;":cuentas.getCuenta() );
									String nom_lar = ( cuentas.getNombre_largo().equals("")?"&nbsp;":cuentas.getNombre_largo() );
									String nom_cor = ( cuentas.getNombre_corto().equals("")?"&nbsp;":cuentas.getNombre_corto() );
									String aux = ( cuentas.getAuxiliar().equals("S")?"Si":"No" );
									String act = ( cuentas.getActiva().equals("S")?"Si":"No" );
									String mod1 = ( cuentas.getModulo1().equals("S")?"X":"&nbsp;" );
									String mod2 = ( cuentas.getModulo2().equals("S")?"X":"&nbsp;" );
									String mod3 = ( cuentas.getModulo3().equals("S")?"X":"&nbsp;" );
									String mod4 = ( cuentas.getModulo4().equals("S")?"X":"&nbsp;" );
									String mod5 = ( cuentas.getModulo5().equals("S")?"X":"&nbsp;" );
									String mod6 = ( cuentas.getModulo6().equals("S")?"X":"&nbsp;" );
									String mod7 = ( cuentas.getModulo7().equals("S")?"X":"&nbsp;" );
									String mod8 = ( cuentas.getModulo8().equals("S")?"X":"&nbsp;" );
									String mod9 = ( cuentas.getModulo9().equals("S")?"X":"&nbsp;" );
									String mod10 = ( cuentas.getModulo10().equals("S")?"X":"&nbsp;" );
									String cta_dep = ( cuentas.getCta_dependiente().equals("")?"&nbsp;":cuentas.getCta_dependiente() );
									String niv = ( cuentas.getNivel().equals("")?"&nbsp;":cuentas.getNivel() );
									String cta_cie = ( cuentas.getCta_cierre().equals("")?"&nbsp;":cuentas.getCta_cierre() );
									String sub = ( cuentas.getSubledger().equals("S")?"Si":"No" );
									String ter = cuentas.getTercero();
									if ( ter.equals("N") ){
										ter = "No Aplica";
									} else if ( ter.equals("M") ){
										ter = "Mandatorio";
									} else if ( ter.equals("O") ){
										ter = "Opcional";
									} else {
										ter = "No Aplica";
									}
							  %>
									<tr class="<%=(i % 2 == 0 )?"filagris":"filaazul"%>" onMouseOver='cambiarColorMouse(this)' style="cursor:hand" >
									  <td width="10%"  align="left" nowrap class="bordereporte" abbr=""><%=cue%></td>
									  <td width="10%" align="left" nowrap class="bordereporte" abbr=""><%=nom_cor%></td>
									  <td width="20%" align="left" nowrap class="bordereporte" abbr=""><%=nom_lar%></td>
									  <td width="5%" align="center" nowrap class="bordereporte" abbr=""><%=niv%></td>
									  <td width="5%" align="center" nowrap class="bordereporte" abbr=""><%=act%></td>
									  <td width="5%" align="center" nowrap class="bordereporte" abbr=""><%=aux%></td>
									  <td width="5%" align="center" nowrap class="bordereporte" abbr=""><%=sub%></td>
									  <td width="5%" align="center" nowrap class="bordereporte" abbr=""><%=ter%></td>
									  <td width="10%" align="left" nowrap class="bordereporte" abbr=""><%=cta_dep%></td>
									  <td width="10%" align="left" nowrap class="bordereporte" abbr=""><%=cta_cie%></td>
									  <td align="center" nowrap class="bordereporte" abbr=""><%=mod1%></td>
									  <td align="center" nowrap class="bordereporte" abbr=""><%=mod2%></td>
									  <td align="center" nowrap class="bordereporte" abbr=""><%=mod3%></td>
									  <td align="center" nowrap class="bordereporte" abbr=""><%=mod4%></td>
									  <td align="center" nowrap class="bordereporte" abbr=""><%=mod5%></td>
									  <td align="center" nowrap class="bordereporte" abbr=""><%=mod6%></td>
									  <td align="center" nowrap class="bordereporte" abbr=""><%=mod7%></td>
									  <td align="center" nowrap class="bordereporte" abbr=""><%=mod8%></td>
									  <td align="center" nowrap class="bordereporte" abbr=""><%=mod9%></td>
									  <td align="center" nowrap class="bordereporte" abbr=""><%=mod10%></td>
									</tr>
						  		
							<%i++;}%>
                        </table>
						
				</td>
				</tr>
            </table>
        </td>
    </tr>
 </table>
 <br>
	<%}
	 else { %>                    
	  <%out.print("<table width=379 border=2 align=center><tr><td><table width=100%  border=1 align=center  bordercolor=#F7F5F4 bgcolor=#FFFFFF><tr><td width=350 align=center class=mensajes>Su busqueda no arrojo resultados!!</td><td width=100><img src="+BASEURL+"/images/cuadronaranja.JPG></td></tr></table></td></tr></table>");%>
	  <br>
	<%}if ( !tipo.equals("exel") ) {%>
<table width='95%' align=center>
  <tr class="titulo">
    <td align=right colspan='2'>
        <div align="center">		
		<img title='Regresar al Filtro' src="<%= BASEURL %>/images/botones/regresar.gif" onClick = "window.location='<%=BASEURL%>/jsp/finanzas/contab/plan_de_cuentas/modificar/BuscarPlanDeCuentas.jsp'; this.disabled=true;" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" style="cursor:hand">
		<img title='Salir al Menu' src="<%=BASEURL%>/images/botones/salir.gif"  name="imgsalir"  onMouseOver="botonOver(this);" onClick="window.close();" onMouseOut="botonOut(this);" style="cursor:hand">
        </div></td>
  </tr>
</table>
</div>
<%}%>
</body>
</html>