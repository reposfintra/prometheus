<!--
- Nombre P�gina :                  BuscarPlanDeCuentas.jsp                 
- Descripci�n :                    Pagina JSP, que realiza el filtro de busqueda de las cuentas               
- Autor :                          JDELAROSA                        
- Fecha Creado :                   19 de Enero de 2006, 02:03 P.M.                        
- Versi�n :                        1.0                                       
- Copyright :                      Fintravalores S.A.                   
-->

<%-- Declaracion de librerias--%>
<%@page contentType="text/html"%>
<%@ page session="true"%>
<%@ page errorPage="../error/error.jsp"%>
<%@page import="com.tsp.operation.model.beans.*, java.util.*"%>
<%@ include file="/WEB-INF/InitModel.jsp"%>
<%@page import="com.tsp.operation.model.*"%>
<%@page import="com.tsp.util.*"%>
<%@ taglib uri="http://jakarta.apache.org/taglibs/input-0.90" prefix="input" %>
<%
  String path = application.getRealPath(model.menuService.getRealUrl(request.getRequestURI()));
  String datos[] = model.menuService.getContenidoMenu(BASEURL,request.getRequestURI(),path);
%>
<html>
<head><title>Consulta Plan De Cuentas</title>
<link href="<%= BASEURL %>/css/estilostsp.css" rel="stylesheet" type="text/css">
<script type='text/javascript' src="<%= BASEURL %>/js/boton.js"></script>
<script type='text/javascript' src="<%= BASEURL %>/js/validar.js"></script>
</head>
<body>
<div id="capaSuperior" style="position:absolute; width:100%; height:100px; z-index:0; left: 0px; top: 0px;">
<jsp:include page="/WEB-INF/toptsp.jsp?encabezado=Plan De Cuentas"/>
</div>

<div id="capaCentral" style="position:absolute; width:100%; height:85%; z-index:0; left: 0px; top: 100px; overflow: scroll;"> 
<%
    Usuario usuario = (Usuario) session.getAttribute("Usuario");  
	String Tip =(request.getParameter ("Tipo")!=null)?request.getParameter ("Tipo"):""; 
%>
<script>
	var controlador ="<%=CONTROLLERCONTAB%>";
	
	function Buscar (){
	
		    var plan = document.getElementById('Plan');
			var icg = document.getElementById('ICG');
			
			var Tipo = document.getElementById('Tipo');
			var cuenta = document.getElementById('cuenta');
			var cuenta2 = document.getElementById('cuenta2');
			var Agencia = document.getElementById('Agencia');
			var Unidad = document.getElementById('Unidad');
			var Cliente = document.getElementById('Cliente');
			var Elemento = document.getElementById('Elemento');
			var valor = 0;
			if( plan.checked == true && icg.checked == false){
				if( cuenta.value <= cuenta2.value ) {
			   	 if(cuenta.value != "" && cuenta2.value != "") {
					document.form2.action = controlador+"?estado=Consulta&accion=Cuentas&tipo=consulta";
					document.form2.submit();
				}
				else{
					alert( "Por Favor No Deje Los Campos Obligatorios Vacios!" );
					if( cuenta.value == "") cuenta.focus(); //AMATURANA 30.03.2007
					else cuenta2.focus();
				}
				} else {
					alert( "La Cuenta Inicial debe ser igual o menor que la Final!" );
					cuenta.focus();
				}
		   }
		   if(plan.checked == false && icg.checked == true){
				 if (Tipo.value == ""){
				    valor = 1;
					alert("Debe digitar Un Tipo de Cuenta para  Continuar...");
				}
				if(Agencia.value != ""){
					if ( Agencia.value.length < 2 ){ 
						alert( "Por Favor debe digitar la Agencia Completa" );
						valor = 1;
					}
				}
				if(Unidad.value != ""){		 
					 if ( Unidad.value.length < 3 ){ 
						alert( "Por Favor debe digitar la Unidad Completa" );
						valor = 1;
					}
				}
				if(Cliente.value != ""){		 	 
					if ( Cliente.value.length < 3 ){ 
						alert( "Por Favor debe digitar el Cliente Completo" );
						valor = 1;
					}
				}
				if(Elemento.value != ""){		 	 	 
					if ( Elemento.value.length < 4 ){ 
						alert( "Por Favor debe digitar el Nro de Elemento Completo" );
						valor = 1;
					}
				}
				if (valor < 1){		 
					document.form2.action = controlador+"?estado=Consulta&accion=Cuentas&tipo=consulta";
					document.form2.submit();
				}	
		  }
		  if(plan.checked == false && icg.checked == false){
		  		alert("Debe digitar por lo menos un Campo de Texto para Continuar...");
		  }		
	}
	
	function Detalles (){
	
		document.form2.action = controlador+"?estado=Consulta&accion=Cuentas&tipo=consulta&cuenta=";
		document.form2.submit();
		
	}
	
	function Exel (){
	        var plan = document.getElementById('Plan');
			var icg = document.getElementById('ICG');
			
			var Tipo = document.getElementById('Tipo');
			var cuenta = document.getElementById('cuenta');
			var cuenta2 = document.getElementById('cuenta2');
			var Agencia = document.getElementById('Agencia');
			var Unidad = document.getElementById('Unidad');
			var Cliente = document.getElementById('Cliente');
			var Elemento = document.getElementById('Elemento');
			var cadena = "";
			if( plan.checked == true && icg.checked == false){
				if( cuenta.value <= cuenta2.value ) {
			   	 if(cuenta.value != "" && cuenta2.value != "") {
					window.open(controlador+'?estado=Consulta&accion=Cuentas&tipo=exel&cuenta='+cuenta.value+'&cuenta2='+cuenta2.value+'&Plan=1','','menubar=yes,status=yes,scrollbars=no,width=950,height=600,resizable=yes');
				}
				else{
					alert( "Por Favor No Deje Los Campos Obligatorios Vacios!" );
					if( cuenta.value == "") cuenta.focus(); //AMATURANA 30.03.2007
					else cuenta2.focus();
				}
				} else {
					alert( "La Cuenta Inicial debe ser igual o menor que la Final!" );
					cuenta.focus();
				}
		   }
		   if(plan.checked == false && icg.checked == true){
		   
		   		if( (Tipo.value == "") && (Agencia.value == "") && (Unidad.value == "") && (Cliente.value == "") && (Elemento.value == "")) {
					alert("Debe digitar por lo menos un Campo de Texto para Continuar...");
				}
				else {
					window.open(controlador+'?estado=Consulta&accion=Cuentas&tipo=exel&cuenta='+Tipo.value+'&ICG=1&Tipo='+Tipo.value+'&Agencia='+Agencia.value+'&Unidad='+Unidad.value+'&Cliente='+Cliente.value+'&Elemento='+Elemento.value,'','menubar=yes,status=yes,scrollbars=no,width=950,height=600,resizable=yes');
				}	
		  }
		  if(plan.checked == false && icg.checked == false){
		  		alert("Debe digitar por lo menos un Campo de Texto para Continuar...");
		  }		

		
						
	}
	
	function chequeo(ch){
		var plan = document.getElementById('Plan');
		var icg = document.getElementById('ICG');
		var obj = document.getElementById('filtro_plan');
		var obj2 = document.getElementById('filtro_icg');	
		
		
		if ( ch.name=='Plan' && ch.checked ) {
  			obj.style.display = 'block';
			obj2.style.display = 'none';
			icg.checked = false;
			temp = 0;			
		} else if ( ch.name=='ICG' && ch.checked ) {
			obj2.style.display = 'block';
			obj.style.display = 'none';
			plan.checked = false;
			temp = 1;		
		} else {
			obj2.style.display = 'none';
			obj.style.display = 'none';		
		}
	}
	function cambios(){
	    var plan = document.getElementById('Plan');
		var icg = document.getElementById('ICG');
		
		if(icg.checked == true ){
			plan.checked = false;
			icg.checked = true;
			
		}else{
		    plan.checked = true;
			icg.checked = false;
			
		}
	}
</script>

<form name="form2" method="post">
    <table width="44%" border="2" align="center">
      <tr>
        <td><table width="100%" align="center"  class="tablaInferior">
            <tr>
              <td width="61%" class="subtitulo1">Filtro para Consultar una Cuenta </td>
              <td width="39%" class="barratitulo"><img src="<%=BASEURL%>/images/titulo.gif" width="32" height="20" align="left"><%=datos[0]%></td>
            </tr>
            <tr class="fila">
				<td colspan="2" >Tipo de Cuenta </td>
			</tr>
            <tr class="fila">
              <td align="left" valign="middle" ><input name="Plan" type="checkbox" id="Plan" onSelect="true" onClick="chequeo(this);" value="1" checked> 
              Plan de Cuenta </td>
              <td valign="middle"><input type="checkbox" name="ICG" id="ICG" value="1" onClick="chequeo(this); ">
              ICG</td>
            </tr>
            
        </table></td>
      </tr>
    </table>
	 <table width="44%" border="2" align="center" style="display:block " id="filtro_plan">
			<tr class="fila">
			  <td colspan="2" align="left" valign="middle" >Filtro por Numero de Cuenta </td>
	   </tr>
			<tr class="fila">
              <td width="50%" align="left" valign="middle" >Cuenta Inicial </td>
              <td width="50%" valign="middle"><input name="cuenta" type="text" class="textbox" id="cuenta" size="25" maxlength="25" onKeyPress="soloDigitos(event, 'decNO')">
              <img src="<%=BASEURL%>/images/botones/iconos/obligatorio.gif"></td>
            </tr>
			<tr class="fila">
			  <td align="left" valign="middle" >Cuenta Final </td>
			  <td valign="middle"><input name="cuenta2" type="text" class="textbox" id="cuenta2" size="25" maxlength="25" onKeyPress="soloDigitos(event, 'decNO')">
		      <img src="<%=BASEURL%>/images/botones/iconos/obligatorio.gif"></td>
	   </tr>		
	</table>
	
	 <table width="44%" border="2" align="center" style="display:none " id="filtro_icg">
	  <tr class="fila">
	    <td colspan="5">Filtro de Tipo de Cuenta ICG </td>
       </tr>
	  <tr class="fila">
		  <td ><select name="Tipo" id="Tipo" value="<%=Tip%>">
              <option value="" <%if(Tip.equals("")){%> selected <%}%>>SELECCIONE</option>
			  <option value="I" <%if(Tip.equals("S")){%> selected <%}%>>TIPO I</option>
              <option value="C" <%if(Tip.equals("N")){%> selected <%}%>>TIPO C</option>
			  <option value="G" <%if(Tip.equals("N")){%> selected <%}%>>TIPO G</option>
            </select></td>
		  <td><input type="text" name="Agencia" id="Agencia" size="10" maxlength="2" style="text-align:center;"></td>
	      <td><input type="text" name="Unidad" id="Unidad" size="10" maxlength="3" style="text-align:center;"></td>
	      <td><input type="text" name="Cliente" id="Cliente" size="10" maxlength="3" style="text-align:center;"></td>
	      <td><input type="text" name="Elemento" id="Elemento" size="10" maxlength="4" style="text-align:center;"></td>
	  </tr>
	 <tr class="fila">
		  <td width="14%"><div align="center">Tipo</div></td>
		  <td width="26%"><div align="center">Agencia</div></td>
	      <td width="20%"><div align="center">Unidad</div></td>
	      <td width="20%"><div align="center">Cliente</div></td>
	      <td width="20%"><div align="center">Elemento</div></td>
	 </tr>
	 </table>
<p>
<div align="center">
<p><img src="<%=BASEURL%>/images/botones/buscar.gif" style="cursor:hand" title="Buscar cuenta especifica" name="buscar"  onClick="Buscar ();" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);">      &nbsp;
	  <img src="<%=BASEURL%>/images/botones/exportarExcel.gif"  name="exel" style="cursor:hand" title="Exportar a exel" onMouseOver="botonOver(this);" onClick="Exel ();" onMouseOut="botonOut(this);" >&nbsp;
      <img src="<%=BASEURL%>/images/botones/salir.gif"  name="salir" style="cursor:hand" title="Salir al Menu Principal" onMouseOver="botonOver(this);" onClick="parent.close();" onMouseOut="botonOut(this);" >
</p>
  <%String msg = request.getParameter("msg");
	if ( msg!=null && !msg.equals("") ){%>
                        
	  <table border="2" align="center">
   		<tr>
        	<td>
            	<table width="100%"  border="1" align="center"  bordercolor="#F7F5F4" bgcolor="#FFFFFF">
                	<tr>
                    	<td width="229" align="center" class="mensajes">
							<%=msg%>
							<%if(request.getParameter("ruta")!=null){%><br>
							&nbsp;&nbsp;<a href="#" class="Simulacion_Hiper" onClick="window.open('<%= CONTROLLER %>?estado=Log&accion=Proceso&Estado=TODO','log','scroll=no, resizable=yes, width=800, height=600')" style="cursor:hand "><%=request.getParameter("ruta")%><%}%></a>
						</td>
                        <td width="29" background="<%=BASEURL%>/images/cuadronaranja.JPG">&nbsp;</td>
                        <td width="58">&nbsp;</td>
                    </tr>
                </table>
            </td>
		</tr>
	  </table>
	<%}%>
  </p>  
</div>
</p>
</form>
</div>
<%=datos[1]%>
</body>
</html>
