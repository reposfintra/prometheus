<!--
- Nombre P�gina :                  ModificarPlanDeCuentas.jsp                 
- Descripci�n :                    Pagina JSP, que modifica una cuenta                     
- Autor :                          LREALES                        
- Fecha Creado :                   6 de Junio de 2006, 06:33 P.M.                        
- Versi�n :                        1.0                                       
- Copyright :                      Fintravalores S.A.                   
-->

<%-- Declaracion de librerias--%>
<%@page contentType="text/html"%>
<%@page session="true"%>
<%@page errorPage="../error/ErrorPage.jsp"%>
<%@page import="com.tsp.operation.model.*"%>
<%@page import="com.tsp.util.*"%>
<%@page import="com.tsp.operation.model.beans.*, java.util.*"%>
<%@page import="com.tsp.finanzas.contab.model.*"%>
<%@page import="com.tsp.finanzas.contab.model.beans.*, java.util.*"%>
<%@include file="/WEB-INF/InitModel.jsp"%>
<%@ taglib uri="http://jakarta.apache.org/taglibs/input-0.90" prefix="input" %>
<%
  String path = application.getRealPath(model.menuService.getRealUrl(request.getRequestURI()));
  String datos[] = model.menuService.getContenidoMenu(BASEURL,request.getRequestURI(),path);
%>
<html>
<head>
<title>Modificar Plan De Cuentas</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link href="<%= BASEURL %>/css/estilostsp.css" rel="stylesheet" type="text/css">
<link href="../../css/estilostsp.css" rel="stylesheet" type="text/css"> 
<script type='text/javascript' src="<%= BASEURL %>/js/boton.js"></script>
</head>
<script>
		var controlador ="<%=CONTROLLERCONTAB%>";
		
		function Modificar ( cuenta, nom_largo, nom_corto, observacion, cta_dependiente, cta_cierre, auxiliar ){
		
			if( ( cuenta != "" ) && ( nom_largo != "" ) && ( nom_corto != "" ) ){
			
				if ( ( ( cta_dependiente != "" ) && ( auxiliar == "N" ) ) || ( cta_dependiente == "" ) ) {
					document.forma.action = controlador+"?estado=Update&accion=PlanDeCuentas&cuenta="+cuenta+"&nom_largo="+nom_largo+"&nom_corto="+nom_corto+"&observacion="+observacion+"&cta_dependiente="+cta_dependiente+"&cta_cierre="+cta_cierre;
					document.forma.submit();
				} else {
					alert( "La cuenta ingresada no debe ser auxiliar!, o no debe tener cuenta dependiente!" );
				}
				
			} else{
			
				alert( "Por Favor No Deje Los Campos Obligatorios Vacios!" );
				
			}
		}
</script>

<body ONLOAD="redimensionar();" onresize="redimensionar()">
<div id="capaSuperior" style="position:absolute; width:100%; height:100px; z-index:0; left: 0px; top: 0px;">
	<jsp:include page="/WEB-INF/toptsp.jsp?encabezado=Plan De Cuentas"/>
</div>
<div id="capaCentral" style="position:absolute; width:100%; height:87%; z-index:0; left: 0px; top: 100px; overflow: scroll;">
<% 
	String m = request.getParameter("m");
%>
<form name="forma" method="post">
<% 
	if( m != null && m.equals("no") ){ 
%>
  <table width="95%"  border="2" align="center">
  	<tr>
    	<td>
			<table width="100%" align="center">
				<tr>
					<td colspan="5" nowrap class="subtitulo1">Ingresar Informaci&oacute;n de la Cuenta a Modificar</td>
					<td colspan="5" class="barratitulo"><img src="<%=BASEURL%>/images/titulo.gif" width="32" height="20" align="left"><%=datos[0]%></td>
				</tr>
				<%
				
				Vector vec_cuentas = modelcontab.planDeCuentasService.getVec_cuentas();
				
				PlanDeCuentas cuentas = ( PlanDeCuentas ) vec_cuentas.elementAt( 0 );
				
				String cue = cuentas.getCuenta();
				String nom_lar = cuentas.getNombre_largo();
				String nom_cor = cuentas.getNombre_corto();
				String obs = cuentas.getNombre_observacion();
				String aux = cuentas.getAuxiliar();
				String act = cuentas.getActiva();
				String mod1 = cuentas.getModulo1();
				String mod2 = cuentas.getModulo2();
				String mod3 = cuentas.getModulo3();
				String mod4 = cuentas.getModulo4();
				String mod5 = cuentas.getModulo5();
				String mod6 = cuentas.getModulo6();
				String mod7 = cuentas.getModulo7();
				String mod8 = cuentas.getModulo8();
				String mod9 = cuentas.getModulo9();
				String mod10 = cuentas.getModulo10();
				String cta_dep = cuentas.getCta_dependiente();
				String niv = cuentas.getNivel();
				String cta_cie = cuentas.getCta_cierre();
				String sub = cuentas.getSubledger();
				String ter = cuentas.getTercero();
				String det = cuentas.getDetalle();
				
				%> 
				<tr>
				  <td width="12%" class="fila">Cuenta</td>
				  <td colspan="4" class="letra"><%=cue%></td>
			      <td class="fila">Activa?</td>				  
			      <td colspan="4" class="fila">
				  <select name="activa" id="activa">
					  <%if (act.equals("S")){%>
					  <option value="S" selected>SI</option>
                      <option value="N">NO</option>
					  <%} else if (act.equals("N")){%>
					  <option value="S">SI</option>
                      <option value="N" selected>NO</option>
					  <%} else{%>
					  <option value="S" selected>SI</option>
                      <option value="N">NO</option>
					  <%}%>
				  </select>
				  </td>
		      </tr>
				<tr class="fila">
				  <td>Nombre Largo </td>
				  <td width="38%" colspan="4"><input name="nom_largo" type="text" id="nom_largo" value="<%=nom_lar%>" size="55" maxlength="55">
			      <img src="<%=BASEURL%>/images/botones/iconos/obligatorio.gif"></td>
				  <td width="12%">Nombre Corto </td>
			      <td width="38%" colspan="4"><input name="nom_corto" type="text" id="nom_corto" value="<%=nom_cor%>" size="15" maxlength="15">
		          <img src="<%=BASEURL%>/images/botones/iconos/obligatorio.gif"></td>
			  </tr>
				<tr class="fila">
				  <td>Observaci&oacute;n</td>
				  <td colspan="9"><input name="observacion" type="text" class="textbox" id="observacion" value="<%=obs%>" size="100" maxlength="200">			      </td>
			    </tr>
				<tr class="fila">
				  <td>Nivel N&deg; </td>
				  <td>
				  <select name="nivel" id="nivel">
					  <%if (niv.equals("1")){%>
					  <option value="1" selected>1</option>
				      <option value="2">2</option>
				      <option value="3">3</option>
				      <option value="4">4</option>
				      <option value="5">5</option>
				      <option value="6">6</option>
					  <option value="7">7</option>
					  <option value="8">8</option>
					  <option value="9">9</option>
					  <%} else if (niv.equals("2")){%>
					  <option value="1">1</option>
				      <option value="2" selected>2</option>
				      <option value="3">3</option>
				      <option value="4">4</option>
				      <option value="5">5</option>
				      <option value="6">6</option>
					  <option value="7">7</option>
					  <option value="8">8</option>
					  <option value="9">9</option>
					  <%} else if (niv.equals("3")){%>
					  <option value="1">1</option>
				      <option value="2">2</option>
				      <option value="3" selected>3</option>
				      <option value="4">4</option>
				      <option value="5">5</option>
				      <option value="6">6</option>
					  <option value="7">7</option>
					  <option value="8">8</option>
					  <option value="9">9</option>
					  <%} else if (niv.equals("4")){%>
					  <option value="1">1</option>
				      <option value="2">2</option>
				      <option value="3">3</option>
				      <option value="4" selected>4</option>
				      <option value="5">5</option>
				      <option value="6">6</option>
					  <option value="7">7</option>
					  <option value="8">8</option>
					  <option value="9">9</option>
					  <%} else if (niv.equals("5")){%>
					  <option value="1">1</option>
				      <option value="2">2</option>
				      <option value="3">3</option>
				      <option value="4">4</option>
				      <option value="5" selected>5</option>
				      <option value="6">6</option>
					  <option value="7">7</option>
					  <option value="8">8</option>
					  <option value="9">9</option>
					  <%} else if (niv.equals("6")){%>
					  <option value="1">1</option>
				      <option value="2">2</option>
				      <option value="3">3</option>
				      <option value="4">4</option>
				      <option value="5">5</option>
				      <option value="6" selected>6</option>
					  <option value="7">7</option>
					  <option value="8">8</option>
					  <option value="9">9</option>
					  <%} else if (niv.equals("7")){%>
					  <option value="1">1</option>
				      <option value="2">2</option>
				      <option value="3">3</option>
				      <option value="4">4</option>
				      <option value="5">5</option>
				      <option value="6">6</option>
					  <option value="7" selected>7</option>
					  <option value="8">8</option>
					  <option value="9">9</option>
					  <%} else if (niv.equals("8")){%>
					  <option value="1">1</option>
				      <option value="2">2</option>
				      <option value="3">3</option>
				      <option value="4">4</option>
				      <option value="5">5</option>
				      <option value="6">6</option>
					  <option value="7">7</option>
					  <option value="8" selected>8</option>
					  <option value="9">9</option>
					  <%} else if (niv.equals("9")){%>
					  <option value="1">1</option>
				      <option value="2">2</option>
				      <option value="3">3</option>
				      <option value="4">4</option>
				      <option value="5">5</option>
				      <option value="6">6</option>
					  <option value="7">7</option>
					  <option value="8">8</option>
					  <option value="9" selected>9</option>
					  <%} else{%>
					  <option value="1" selected>1</option>
				      <option value="2">2</option>
				      <option value="3">3</option>
				      <option value="4">4</option>
				      <option value="5">5</option>
				      <option value="6">6</option>
					  <option value="7">7</option>
					  <option value="8">8</option>
					  <option value="9">9</option>
					  <%}%>
				  </select>
				  </td>
				  <td>Auxiliar?</td>
				  <td>
				  <select name="auxiliar" id="auxiliar">
					  <%if (aux.equals("S")){%>
					  <option value="S" selected>SI</option>
                      <option value="N">NO</option>
					  <%} else if (aux.equals("N")){%>
					  <option value="S">SI</option>
                      <option value="N" selected>NO</option>
					  <%} else{%>
					  <option value="S">SI</option>
                      <option value="N" selected>NO</option>
					  <%}%>
				  </select>				  </td>
				  <td>Subledger?
				  
				  </td>
				  <td><select name="subledger" id="subledger">
					  <%if (sub.equals("S")){%>
					  <option value="S" selected>SI</option>
                      <option value="N">NO</option>
					  <%} else if (sub.equals("N")){%>
					  <option value="S">SI</option>
                      <option value="N" selected>NO</option>
					  <%} else{%>
					  <option value="S">SI</option>
                      <option value="N" selected>NO</option>
					  <%}%>
				  </select>
				  </td>
			      <td>Detalle?
				  
				  </td>
			      <td>
				  <select name="detalle" id="detalle">
                    <%if (det.equals("S")){%>
                    <option value="S" selected>SI</option>
                    <option value="N">NO</option>
                    <%} else if (det.equals("N")){%>
                    <option value="S">SI</option>
                    <option value="N" selected>NO</option>
                    <%} else{%>
                    <option value="S">SI</option>
                    <option value="N" selected>NO</option>
                    <%}%>
                  </select>
				  </td>
			      <td>Tercero?</td>
			      <td>
				  <select name="tercero" id="tercero">
					  <%if (ter.equals("N")){%>
					  <option value="N" selected>No Aplica</option>
			          <option value="M">Mandatorio</option>
			          <option value="O">Opcional</option>
					  <%} else if (ter.equals("M")){%>
					  <option value="N">No Aplica</option>
			          <option value="M" selected>Mandatorio</option>
			          <option value="O">Opcional</option>
					  <%} else if (ter.equals("O")){%>
					  <option value="N">No Aplica</option>
			          <option value="M">Mandatorio</option>
			          <option value="O" selected>Opcional</option>
					  <%} else{%>
					  <option value="N" selected>No Aplica</option>
			          <option value="M">Mandatorio</option>
			          <option value="O">Opcional</option>
					  <%}%>
				  </select>
				  </td>
			  </tr>
				<tr class="fila">
				  <td colspan="3">Cuenta Dependiente </td>
				  <td colspan="2"><input name="cta_dependiente" type="text" id="cta_dependiente" size="25" value="<%=cta_dep%>" maxlength="25"></td>
				  <td colspan="3">Cuenta de Cierre </td>
			      <td colspan="2"><input name="cta_cierre" type="text" id="cta_cierre" size="25" value="<%=cta_cie%>" maxlength="25"></td>
		      </tr>
				<tr class="fila">
				  <td>CD </td>
				  <td>
				  <select name="modulo1" id="modulo1">
					  <%if (mod1.equals("S")){%>
					  <option value="S" selected>SI</option>
                      <option value="N">NO</option>
					  <%} else if (mod1.equals("N")){%>
					  <option value="S">SI</option>
                      <option value="N" selected>NO</option>
					  <%} else{%>
					  <option value="S">SI</option>
                      <option value="N" selected>NO</option>
					  <%}%>
				  </select>
				  </td>
				  <td>CxP </td>
				  <td>
				  <select name="modulo2" id="modulo2">
					  <%if (mod2.equals("S")){%>
					  <option value="S" selected>SI</option>
                      <option value="N">NO</option>
					  <%} else if (mod2.equals("N")){%>
					  <option value="S">SI</option>
                      <option value="N" selected>NO</option>
					  <%} else{%>
					  <option value="S">SI</option>
                      <option value="N" selected>NO</option>
					  <%}%>
				  </select>
				  </td>
				  <td>CxC </td>
				  <td>
				  <select name="modulo3" id="modulo3">
					  <%if (mod3.equals("S")){%>
					  <option value="S" selected>SI</option>
                      <option value="N">NO</option>
					  <%} else if (mod3.equals("N")){%>
					  <option value="S">SI</option>
                      <option value="N" selected>NO</option>
					  <%} else{%>
					  <option value="S">SI</option>
                      <option value="N" selected>NO</option>
					  <%}%>
				  </select>
				  </td>
				  <td>Proveedor </td>
				  <td>
				  <select name="modulo4" id="modulo4">
					  <%if (mod4.equals("S")){%>
					  <option value="S" selected>SI</option>
                      <option value="N">NO</option>
					  <%} else if (mod4.equals("N")){%>
					  <option value="S">SI</option>
                      <option value="N" selected>NO</option>
					  <%} else{%>
					  <option value="S">SI</option>
                      <option value="N" selected>NO</option>
					  <%}%>
				  </select>
				  </td>
				  <td>Modulo 5 </td>
				  <td>
				  <select name="modulo5" id="modulo5">
					  <%if (mod5.equals("S")){%>
					  <option value="S" selected>SI</option>
                      <option value="N">NO</option>
					  <%} else if (mod5.equals("N")){%>
					  <option value="S">SI</option>
                      <option value="N" selected>NO</option>
					  <%} else{%>
					  <option value="S">SI</option>
                      <option value="N" selected>NO</option>
					  <%}%>
				  </select>
				  </td>
			  </tr>
				<tr class="fila">
				  <td>Modulo 6</td>
				  <td>
				  <select name="modulo6" id="modulo6">
					  <%if (mod6.equals("S")){%>
					  <option value="S" selected>SI</option>
                      <option value="N">NO</option>
					  <%} else if (mod6.equals("N")){%>
					  <option value="S">SI</option>
                      <option value="N" selected>NO</option>
					  <%} else{%>
					  <option value="S">SI</option>
                      <option value="N" selected>NO</option>
					  <%}%>
				  </select>
				  </td>
				  <td>Modulo 7 </td>
				  <td>
				  <select name="modulo7" id="modulo7">
					  <%if (mod7.equals("S")){%>
					  <option value="S" selected>SI</option>
                      <option value="N">NO</option>
					  <%} else if (mod7.equals("N")){%>
					  <option value="S">SI</option>
                      <option value="N" selected>NO</option>
					  <%} else{%>
					  <option value="S">SI</option>
                      <option value="N" selected>NO</option>
					  <%}%>
				  </select>
				  </td>
				  <td>Modulo 8 </td>
				  <td>
				  <select name="modulo8" id="modulo8">
					  <%if (mod8.equals("S")){%>
					  <option value="S" selected>SI</option>
                      <option value="N">NO</option>
					  <%} else if (mod8.equals("N")){%>
					  <option value="S">SI</option>
                      <option value="N" selected>NO</option>
					  <%} else{%>
					  <option value="S">SI</option>
                      <option value="N" selected>NO</option>
					  <%}%>
				  </select>
				  </td>
			      <td>Modulo 9 </td>
			      <td>
				  <select name="modulo9" id="modulo9">
					  <%if (mod9.equals("S")){%>
					  <option value="S" selected>SI</option>
                      <option value="N">NO</option>
					  <%} else if (mod9.equals("N")){%>
					  <option value="S">SI</option>
                      <option value="N" selected>NO</option>
					  <%} else{%>
					  <option value="S">SI</option>
                      <option value="N" selected>NO</option>
					  <%}%>
				  </select>
				  </td>
			      <td>Modulo 10 </td>
			      <td>
				  <select name="modulo10" id="modulo10">
					  <%if (mod10.equals("S")){%>
					  <option value="S" selected>SI</option>
                      <option value="N">NO</option>
					  <%} else if (mod10.equals("N")){%>
					  <option value="S">SI</option>
                      <option value="N" selected>NO</option>
					  <%} else{%>
					  <option value="S">SI</option>
                      <option value="N" selected>NO</option>
					  <%}%>
				  </select>
				  </td>
			  </tr>
		  </table>
  		</td>
  	</tr>
  </table>
  <br>  
  <input type="hidden" id="cuenta" value="<%=cue%>">
  <table align=center width='450'>
    <tr>
        <td align="center" colspan="2">
            <img title='Modificar Cuenta' style = "cursor:hand" src="<%= BASEURL %>/images/botones/modificar.gif" onmouseover="botonOver(this);" onmouseout="botonOut(this);" onclick="Modificar( cuenta.value, nom_largo.value, nom_corto.value, observacion.value, cta_dependiente.value, cta_cierre.value, auxiliar.value );"></img>                        
			<img title='Resetear' style = "cursor:hand" src="<%= BASEURL %>/images/botones/cancelar.gif" onmouseover="botonOver(this);" onmouseout="botonOut(this);" onClick="forma.reset();"></img>
            <img title='Regresar al Filtro' src="<%= BASEURL %>/images/botones/regresar.gif" onClick = "window.location='<%=BASEURL%>/jsp/finanzas/contab/plan_de_cuentas/modificar/FiltroModificarPlanDeCuentas.jsp'; this.disabled=true;" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" style="cursor:hand"></img>
			<img title='Salir' style = "cursor:hand" src="<%= BASEURL %>/images/botones/salir.gif" onmouseover="botonOver(this);" onmouseout="botonOut(this);" onClick="window.close()"></img>
        </td>
    </tr>
  </table> 
  <br>  
  <%String msg = request.getParameter("msg");
	if ( msg!=null && !msg.equals("") ){%>
                        
	  <table border="2" align="center">
   		<tr>
        	<td>
            	<table width="100%"  border="1" align="center"  bordercolor="#F7F5F4" bgcolor="#FFFFFF">
                	<tr>
                    	<td width="229" align="center" class="mensajes"><%=msg%></td>
                        <td width="29" background="<%=BASEURL%>/images/cuadronaranja.JPG">&nbsp;</td>
                        <td width="58">&nbsp;</td>
                    </tr>
                </table>
            </td>
		</tr>
	  </table>
	<%}%>
	
	<%
}//cierro if ( m = no )
	if ( m != null && m.equals("si") ){
%>                        
<%
	out.print("<table width=379 border=2 align=center><tr><td><table width=100%  border=1 align=center  bordercolor=#F7F5F4 bgcolor=#FFFFFF><tr><td width=350 align=center class=mensajes>La Cuenta Ha Sido Modificada Satisfactoriamente!</td><td width=100><img src="+BASEURL+"/images/cuadronaranja.JPG></td></tr></table></td></tr></table>");%>
	<br>
	<p align="center">
	  <img title='Regresar al Filtro' src="<%= BASEURL %>/images/botones/regresar.gif" onClick = "window.location='<%=BASEURL%>/jsp/finanzas/contab/plan_de_cuentas/modificar/FiltroModificarPlanDeCuentas.jsp'; this.disabled=true;" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" style="cursor:hand"> 
	  	  
	  <img src="<%=BASEURL%>/images/botones/salir.gif" name="img_salir" onClick="window.close();" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" align="absmiddle" style="cursor:hand">
	</p>
<%
	}//cierro if ( m = si )
%>
  </form>
</div>
<%=datos[1]%>
</body>
</html>