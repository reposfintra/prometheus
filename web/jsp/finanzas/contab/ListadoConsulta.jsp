
<!--
     - Author(s)       :      FERNEL VILLACOB
     - Date            :      09/06/2006  
     - Copyright Notice:      TRANSPORTES SANCHEZ POLO S.A.
-->
<%--   
     - @(#)  
     - Description: Muetra listado de valores para seleccionar.
--%> 
<%@ page session   ="true"%> 
<%@ page errorPage ="/error/ErrorPage.jsp"%>
<%@ page import    ="java.util.*" %>
<%@ page import    ="com.tsp.util.*" %>
<%@ include file="/WEB-INF/InitModel.jsp"%>


<% Vector   lista    =  (Vector) request.getAttribute("listaBusquedaXXX123"); 
   String   elem     =  request.getParameter("elemento");
   String   titulo   =  request.getParameter("titulo"); %>
   
   

<html>
    <head>

        <title>Listado ....</title>
        <link   href="<%= BASEURL %>/css/estilostsp.css" rel="stylesheet" type="text/css"> 
        <script src="<%= BASEURL %>/js/boton.js"></script>
        <script type='text/javascript' src="<%= BASEURL %>/js/contabilidad.js"></script>
        

    </head>
    <body>
            <center>
            <font id='msj' class='informacion'>Cargando vista....</font>
            
            <table align="center"  border="2" width='100%'  class='fila' bgcolor="#F7F5F4" width='501'>
            <tr>
                <td width="100%" align='center' >
                <table class='tablaInferior' width='100%'>
                <tr>
                    <td >
                        <table border='0' width='100%' cellspacing='0'>
                            <tr>
                                <td class='subtitulo1'  width="50%">&nbsp; <%= titulo.toUpperCase()%></td>
                                <td class='barratitulo' width="25%">
                                    <img src="<%= BASEURL %>/images/titulo.gif" width="32" height="20" align='left'> 
                                </td>                  
                            </tr>
                        </table>             
                    </td> 
                </tr>    
                <tr class='fila'>
                <td><input type='text' name='tabla' style="width:100%" onkeyup="filtrarCombo (this.value, lista)" title='Buscar por nombre'></td>
                </tr>
                <tr class='fila'>
                        <td>
                            <select id='lista' name='lista' style='width:100%;' class='textbox'  size='15' onchange='tabla.value=this[this.selectedIndex].text' ondblclick="retornar(this,'<%=elem%>');"  title='Listado general'>
                               <% for(int i=0; i< lista.size(); i++){
                                         Hashtable dato = (Hashtable)lista.get(i);
                                             String nit  = (String) dato.get("codigo");
                                             String name = (String) dato.get("descripcion");%>
                                    <option value='<%= nit %>' >  <%= name.toUpperCase() %> </option>
                                <% }%>                                         
                            </select>			
                            </tr>
                            </table>
                        </td>
                </tr>   
               
            </table>


            <br >
            <img name="imgGrabar" src="<%=BASEURL%>/images/botones/aceptar.gif" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" style="cursor:hand"    onClick=" retornar(lista,'<%=elem%>'); ">
            &nbsp;
            <img name="imgSalir" src="<%=BASEURL%>/images/botones/salir.gif"    onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" style="cursor:hand"    onClick=" window.close(); " >
          
            <script>  
                    msj.innerHTML='';
                    tabla.focus(); 
            </script>
    </body>
</html>



