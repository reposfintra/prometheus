<!--
- Autor : Ing. Julio Ernesto Barros Rueda
- Date  : 14 de Mayo de 2007
- Copyrigth Notice : Fintravalores S.A. S.A

Descripcion : Pagina JSP, que maneja el ingreso de identidades
-->

<%-- Declaracion de librerias--%>
<%@page session="true"%>
<%@page import="java.util.*"%>
<%@page import="com.tsp.operation.model.*"%> 
<%@page import="com.tsp.operation.model.beans.*"%>
<%@page import="com.tsp.util.*"%>
<%@page import="com.tsp.util.Util.*"%>
<%@include file="/WEB-INF/InitModel.jsp"%>
<%@page import="com.tsp.operation.model.*"%>
<%@taglib uri="http://jakarta.apache.org/taglibs/input-0.90" prefix="input" %>

<%@page contentType="text/html" pageEncoding="ISO-8859-1"%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"

   "http://www.w3.org/TR/html4/loose.dtd">

<html>
    <head>
        <title>Cambio de periodo de comprobante</title>
      
        <link href="<%=BASEURL%>/css/estilostsp.css" rel="stylesheet" type="text/css">
    </head>
    
    <script type='text/javascript' src="<%=BASEURL%>/js/Validacion.js"></script>
    <script type='text/javascript' src="<%= BASEURL %>/js/boton.js"></script>
    <script type='text/javascript' src="<%= BASEURL %>/js/general.js"></script>
    <script src='<%=BASEURL%>/js/tools.js' language='javascript'></script>
    <script src='<%=BASEURL%>/js/date-picker.js'></script>
    <script src="<%= BASEURL %>/js/transferencias.js"></script>
    
    <%
    //int d=0;//cantidad de decimales
    Usuario usuario            = (Usuario) session.getAttribute("Usuario");
    
    //String valorcheq=request.getParameter("valorcillo");
    String exdoc=request.getParameter("doc");
	String extipodoc=request.getParameter("tipodoc");
	String exfecha=request.getParameter("fecha");
	String extransaccion=request.getParameter("transaccion");
	String experiodo=request.getParameter("periodo");
	if (exdoc==null){exdoc="";}
	if (extipodoc==null){extipodoc="";}
	if (exfecha==null){exfecha="";}
	if (extransaccion==null){extransaccion="";}
	if (experiodo==null){experiodo="";}
	
    String Retorno=request.getParameter("respuesta");
    if (Retorno==null){Retorno="";}
    //if (Retorno.equals("")){
        //model.creacionCompraCarteraSvc.cancelarCompraCartera();
    //}
    //TreeMap pro= model.Negociossvc.getProv();
    //pro.put(" Seleccione","");
    //model.clienteService.setTreeMapClientes2();
    //TreeMap pr = model.clienteService.getTreemap();
    //pr.put(" Seleccione","");
    //String fechainicio=request.getParameter("fechainicio");
    //if (fechainicio==null){
        //fechainicio=""+com.tsp.util.Util.AnoActual()+"-"+com.tsp.util.Util.getFechaActual_String(3)+"-"+com.tsp.util.Util.getFechaActual_String(5);
    //}
    
    //public static java.util.Calendar TraerFecha(int fila, int dia, int mes, int ano, String dias_primer_cheque){
    //java.util.Date x;
    //x=(Util.TraerFecha(2, 5, 1, 2008, "30")).getTime();
    //System.out.println("x.toString()="+x.toString());    
    //x=(Util.TraerFecha(3, 5, 1, 2008, "30")).getTime();
    //System.out.println("x.toString()="+x.toString());  
    //System.out.println("Calendar.MONTH="+Calendar.MONTH);
    /*String itemobtenible=request.getParameter("itemobtenible");
	String fechachequeobtenible=request.getParameter("fechachequeobtenible");
	String feccheqconsigobtenible=request.getParameter("feccheqconsigobtenible");
	String valorobtenible=request.getParameter("valorobtenible");*/
	
	%>
    
    <script>
                
        function enviarFormularioX(CONTROLLER,frm){	
			if (validar(frm)){
				frm.action='<%=CONTROLLER%>?estado=modificacion&accion=Comprobante&opcion=modificarperiodo';
				frm.submit();
			}else{
				alert("Por favor revise los datos.");
			}
        }    
                
		function parteOperativa(){					
		
			var tipodocx=formulario.tipodoc.value;
			//alert("tipodocx"+tipodocx);
			if (tipodocx=="ND"){//si es nd se pregunta si es cxc o cxp 
				filatipond.style.display="block";
				if (formulario.tipond.value=="CXP"){
					filaproveedor.style.display="block";
				}else{
					filaproveedor.style.display="none";
				}
				//alert("cxc o cxp?");
				//alert("se utilizara para la parte operativa documento,tipo_documento en caso de cxc y proveedor, tipo_documento, documento en caso de cxp ");
			}
							
			if (tipodocx=="FAP" || tipodocx=="NC"){
				filaproveedor.style.display="block";
				filatipond.style.display="none";
				//alert("falta dato proveedor si escogio fap o nc ");
			}
			
			if (tipodocx=="FAC" || tipodocx=="ING" || tipodocx=="EGR" || tipodocx=="ICA" || tipodocx=="ICR" || tipodocx=="IF" || tipodocx=="NEG" ||  tipodocx=="FAG"){
				filatipond.style.display="none";
				filaproveedor.style.display="none";
			}
		}
       
		function validar(frm){//incompleto
			var respuesta=true;
			if (frm.documento.value==null || frm.documento.value=='' || frm.fecha.value==null || frm.fecha.value=='' || frm.transaccion.value==null || frm.transaccion.value==''){
				respuesta=false;
				return respuesta;
			}
			//alert("displaytipond"+filatipond.style.display);
			if (((frm.tipodoc.value=="FAP" || frm.tipodoc.value=="NC") || (frm.tipodoc.value=="ND" && frm.tipond.value=="CXP" && filatipond.style.display=="block")) && (frm.proveedor.value==null || frm.proveedor.value=='') ){
				respuesta=false;
				return respuesta;
			}
			
			if ((frm.periodo.value.substring(0,4)!=frm.fecha.value.substring(0,4)) || (frm.periodo.value.substring(4,6)!=frm.fecha.value.substring(5,7))){
				respuesta=false;
				return respuesta;
			}
			
			//if (frm.tipodoc.value=="FAP" && (frm.proveedor.value=='' || frm.proveedor.value==null )){
				//respuesta=false;
			//}
			return respuesta;
		}				
				
    </script>
    
    <body onLoad="javascript:formulario.documento.focus">
        
        <div id="capaSuperior" style="position:absolute; width:100%; height:100px; z-index:0; left: 0px; top: 0px;">
            <jsp:include page="/toptsp.jsp?encabezado=CAMBIO DE PERIODO DE COMPROBANTE"/>
        </div>
        <div id="capaCentral" style="position:absolute; width:100%; height:87%; z-index:0; left: 0px; top: 100px; overflow: scroll;">
            
            <FORM name='formulario' method="post" >
                
                <table width="432" height="430"  border="2" align="center">
                    
                    <tr>
                        <td width="420" height="350">
                            <table width="100%" height="72%" class="tablaInferior" >
                                <tr class="fila">
                                    <td colspan="2" align="left" nowrap class="subtitulo">&nbsp;Periodo de comprobante&nbsp;&nbsp; </td>
                                    <td colspan="2" align="left" nowrap class="bordereporte"><img src="<%=BASEURL%>/images/fintra.gif" width="166" height="28" class="bordereporte"> </td>
                                </tr>
                                
                                <tr class="fila">
                                    <td colspan="2" >Documento </td>
                                    <td class="fila">
                                        
                                        
                                            <input name="documento" type="text" class="textbox" id="campo" style="width:200;" size="15"  value="<%=exdoc%>"     >                         
                                             
                                            
                                        
                                    </td>
                                </tr>
                                
								<tr class="fila">
                                    <td colspan="2" >Transaccion </td>
                                    <td class="fila">                                                                                
                                            <input name="transaccion" type="text" class="textbox" id="transaccion" style="width:200;" size="15" value="<%=extransaccion%>">              
                                        
                                    </td>
                                </tr>
                                
								
                                <tr class="fila">
                                    <td colspan="2" >Fecha de comprobante </td>
                                    <td class="fila">

                                        
                                    
                                        <input name='fecha' type='text' class="textbox" id="fechacheque" style='width:120' readonly value="<%=exfecha%>">
                                           <img src="<%=BASEURL%>/images/cal.gif" alt="" width="16" height="16" align="absmiddle" style="cursor: pointer" onClick="if(self.gfPop)gfPop.fPopCalendar(document.getElementById('fechacheque'));return false;" HIDEFOCUS>
                                        	
											                                    
                                        
                                    </td>
                                </tr>
                                
                               
                              <tr class="fila">
                                    <td colspan="2" >Periodo </td>
                                    <td>
                                        <%
                                                    Date ahora = new Date();
                                                    Calendar c = new GregorianCalendar();
                                                    int anio = (c.get(Calendar.YEAR));
                                                    int m = c.get(Calendar.MONTH) + 1;
                                                    String mes = "";
                                                    if (m < 10) {
                                                        mes = "0" + Integer.toString(m);
                                                    } else {
                                                        mes = Integer.toString(m);
                                                    }
                                        %>
                                        <select name='periodo'>
                                            <%
                                                        String periodo = "";
                                                        for (int i = anio - 5; i <= anio; i++) {
                                                            for (int j = 1; j <= 12; j++) {
                                                                if (i <= anio ) {
                                                                    if (j < 10) {
                                                                        mes = "0" + Integer.toString(j);
                                                                    } else {
                                                                        mes = Integer.toString(j);
                                                                    }
                                                                    periodo = i + "" + mes;

                                            %>
                                            <option value=<%=periodo%> <%if (experiodo.equals(periodo)) {%>selected<%}%>><%=i + "-" + mes%></option>
                                            <%}
                                                            }
                                                        }%>
                                        </select>

                                    </td> 
                                </tr>
								<tr class="fila">
                                    <td colspan="2" >Tipo de documento </td>
                                    <td>
                                        
                                        <select name='tipodoc' onChange="javascript:parteOperativa();">
                                            <option value='FAC'>FACTURA</option>
                                            <option value='ING'>INGRESO</option>
                                            <option value='EGR'>EGRESO</option>           
                                            <option value='FAP'>CXP</option>
                                            <option value='ICA'>NOTA AJUSTE</option>           
                                            <option value='ICR'>NOTA CREDITO ICR</option>           
                                            <option value='IF'>INGRESOS FENALCO</option>                                           
                                            <option value='ND'>NOTA DEBITO</option>                                           
                                            <option value='NC'>NOTA CREDITO NC</option>                                           
                                            <option value='NEG'>NEGOCIOS</option>
                                            <option value='FAG'>GEOFACTURA</option>
                                            <option value="IFA">INTERESES FACTORING</option>            
                                            <option value='AEF'>ANTICIPO EFECTIVO</option><!--091106-->
                                            <option value='AGA'>ANTICIPO GASOLINA</option><!--091106-->
                                            <option value='AET'>ANTICIPO TRANSFERENCIA</option><!--091106-->
                                            <option value='EXT'>PRONTO PAGO</option><!--091106-->
                                            <option value='CDIAR'>COMPROBANTE DIARIO</option><!--091106-->
                                            <option value='NDC'>NOTA DEVOLUCION</option><!--091106-->
                                            <option value='CM'>CUOTA DE ADMINISTRACIÓN</option><!--091106-->
                                            <option value='LI'>INGRESOS LIBRANZA</option><!--091106-->
											       																																	
                                        </select>
                                        
                                           
                                    </td>
                                </tr>
								<tr class="fila" id="filatipond" style="display:none"><!--style="display:none"-->
                                    <td colspan="2" >Tipo de ND </td>
                                    <td>                                        
                                        <select name='tipond' onChange="javascript:parteOperativa();" >
											
                                            <option value='CXP'>CXP</option>											
											<option value='CXC'>CXC</option>
                                        </select>                                      
                                           
                                    </td> 
                                </tr>
								<tr class="fila" id="filaproveedor" style="display:none ">
                                    <td colspan="2" >Nit de Proveedor </td>
                                    <td class="fila">
                                            <input name="proveedor" type="text" class="textbox" id="proveedor" style="width:200;" size="15"     >                         
                                    </td>
                                </tr>
								<!--<tr class="fila">
                                    <td colspan="2" >Proveedor (en caso de CXP)</td>
                                    <td class="fila">

                                        
                                            <input name="proveedor" type="text" class="textbox" id="campo" style="width:200;" size="15"     >                         

                                            
                                        
                                    </td>
                                </tr>-->

                </table>
                <br>
                <div align="center">
                    <img src="<%=BASEURL%>/images/botones/aceptar.gif"  name="imgaceptar" onClick="javascript:enviarFormularioX('<%=CONTROLLER%>',formulario);" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" style="cursor:hand">&nbsp;       &nbsp;
                    <img src="<%=BASEURL%>/images/botones/salir.gif"  name="imgsalir"  onMouseOver="botonOver(this);" onClick="parent.close();" onMouseOut="botonOut(this);" style="cursor:hand">&nbsp;       &nbsp;
                   
                </div>
                <br>
                <%
                String mensaje="";
                if( Retorno != null && !(Retorno.equals(""))){
                    if (Retorno.equals("actualizacionexitosa")){
                         mensaje="Actualización exitosa.";
                    }
                    if (Retorno.equals("noapareciocomprobante")){
                         mensaje="No apareció el comprobante.";
                    }
                    if (Retorno.equals("noapareciodocumento")){
                         mensaje="No apareció el documento.";
                    }
                     
                %>  
                <table border="2" align="center">
                    <tr>
                        <td>
                            <table width="100%"  border="1" align="center"  bordercolor="#F7F5F4" bgcolor="#FFFFFF">
                                <tr>
                                    <td width="229" align="center" class="mensajes"><%=mensaje%></td>
                                    <td width="29" background="<%=BASEURL%>/images/cuadronaranja.JPG">&nbsp;</td>
                                    <td width="58">&nbsp;</td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
                
				<%}  %>
				
				<br>				<br>
				
				<!--
		  <input name="consultablex" type="text" class="textbox" id="campo" style="width:200;" size="15" maxlength="15"   >
		  
          <img src="<%//=BASEURL%>/images/botones/buscar.gif"  name="imgbuscar"  
            onMouseOver="botonOver(this);"
            onClick="javascript:consultar(formulario);" 
            onMouseOut="botonOut(this);" style="cursor:hand" >

		  <img src="<%//=BASEURL%>/images/botones/modificar.gif"  name="imgmodificar"  
            onMouseOver="botonOver(this);" 
            onClick="javascript:modificar(formulario);" 
            onMouseOut="botonOut(this);" style="cursor:hand" >
			-->
                            
                <br>
            </form>
            
        </div>
        
       <iframe width=188 height=166 name="gToday:normal:agenda.js:gfPop:plugins.js" id="gToday:normal:agenda.js:gfPop:plugins.js" src="<%=BASEURL%>/js/Calendario/ipopeng.htm" scrolling="no" frameborder="0" style="visibility:visible; z-index:999; position:absolute; left:-500px; top:0px;"> </iframe> 
		
    </body>
</html>



