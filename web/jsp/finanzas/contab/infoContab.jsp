<!--
- Autor : Ing. Ivan Gomez
- Date  : 10 de Octubre de 2005
- Copyrigth Notice : Fintravalores S.A. S.A
-->
<%--
-@(#)
--Descripcion : Pagina JSP, Para cargar la tabla de informacion para 
                los programas de contabilidad
--%>
<%@page import="com.tsp.util.*"%>
<%@include file    ="/WEB-INF/InitModel.jsp"%>


<% String tamano = request.getParameter("tamanoTableEncabezado").replaceAll("p","%");
   String pe_rio_do = (String) session.getAttribute("periodo");
   String per     = Util.getPeriodo(2).toUpperCase();
   if(pe_rio_do!=null){
        String anio = pe_rio_do.substring(0,4);
        String mes  = Util.NombreMes(Integer.parseInt(pe_rio_do.substring(4,6))).substring(0,3);
		per = mes +"/"+ anio;
	}
%>

        <table width="<%= tamano%>" border="2" align="center">
              <tr>
                    <td class="barratitulo" colspan='2' >
                           <table cellpadding='0' cellspacing='0' width='100%'>
                                    <tr class="fila">
                                             <td width="50%" align="left" class="subtitulo1">
                                                   &nbsp;PERIODO CONTABLE  

                                                  
                                             
                                             </td>
                                             <td width="*"   align="left"><img src="<%= BASEURL %>/images/titulo.gif" width="32" height="20"  align="left"> </td><td><div align="right"><font class='encabezadoContab'><%=per.toUpperCase() %> &nbsp;&nbsp;&nbsp;</font></div></td>
                                    </tr>
                          </table>   
                    </td>
             </tr>
        </table>