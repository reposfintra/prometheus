<!--
- Nombre P�gina :                  CierreMensualMOC.jsp                
- Descripci�n :                    Pagina JSP, el cierre del periodo contable.             
- Autor :                          LREALES                        
- Fecha Creado :                   28 de Junio de 2006, 07:04 P.M.                        
- Versi�n :                        1.0                                       
- Copyright :                      Fintravalores S.A.                   
-->

<%-- Declaracion de librerias--%>
<%@page contentType="text/html"%>
<%@ page session="true"%>
<%@ page errorPage="../error/error.jsp"%>
<%@page import="com.tsp.operation.model.beans.*, java.util.*"%>
<%@ include file="/WEB-INF/InitModel.jsp"%>
<%@page import="com.tsp.operation.model.*"%>
<%@page import="com.tsp.util.*"%>
<%@ taglib uri="http://jakarta.apache.org/taglibs/input-0.90" prefix="input" %>
<%
  String path = application.getRealPath(model.menuService.getRealUrl(request.getRequestURI()));
  String datos[] = model.menuService.getContenidoMenu(BASEURL,request.getRequestURI(),path);
%>
<html>
<head><title>Cierre Mensual de los Modulos Operativos y Contables</title>
<link href="<%= BASEURL %>/css/estilostsp.css" rel="stylesheet" type="text/css">
<script src='<%=BASEURL%>/js/date-picker.js'></script>
<script type='text/javascript' src="<%= BASEURL %>/js/boton.js"></script>
<script type='text/javascript' src="<%= BASEURL %>/js/validar.js"></script>
</head>
<body onLoad="redimensionar();" onResize="redimensionar();">
<div id="capaSuperior" style="position:absolute; width:100%; height:100px; z-index:0; left: 0px; top: 0px;">
<jsp:include page="/WEB-INF/toptsp.jsp?encabezado=Contabilidad"/>
</div>

<div id="capaCentral" style="position:absolute; width:100%; height:85%; z-index:0; left: 0px; top: 100px; overflow: scroll;"> 
<%
    Usuario usuario = (Usuario) session.getAttribute("Usuario");   
	
	String fecha = Utility.getHoy("-"); 
	String anio1 = fecha.substring(0,4);
	int anio1int = Integer.parseInt ( anio1 );
	int anio2int = anio1int - 1;
	int anio3int = anio1int - 2;
	String anio2 = "" + anio2int;
	String anio3 = "" + anio3int;
%>
<script>
	var controlador ="<%=CONTROLLERCONTAB%>";
	
	function Cerrar ( anio, mes ){
		
		if( ( anio != "" ) && ( mes != "" ) ){
						
			document.form2.action = controlador+"?estado=CierreMensual&accion=MOC";
			document.form2.submit();
						
		} else{
		
			alert( "Por Favor No Deje Los Campos Obligatorios Vacios!" );
			
		}
	}
</script>

<form name="form2" method="post">
    <table width="75%" border="2" align="center">
      <tr>
        <td><table width="100%" align="center"  class="tablaInferior">
            <tr>
				<td width="50%" colspan="1" class="subtitulo1">Cierre Mensual de los Modulos Operativos y Contables</td>
				<td width="50%" colspan="1" class="barratitulo"><img src="<%=BASEURL%>/images/titulo.gif" width="32" height="20" align="left"><%=datos[0]%></td>
            </tr>
            <tr class="fila">
              <td width="50%" align="left" valign="middle" >Periodo</td>
              <td width="50%" align="left" valign="middle" >
			  <span class="Letras">
                <select name="anio" id="anio">
                  <option value="<%=anio3%>"><%=anio3%></option>
                  <option value="<%=anio2%>"><%=anio2%></option>
                  <option value="<%=anio1%>" selected><%=anio1%></option>
                </select>
                <select name="mes" id="mes">
                  <option value="1" selected>Enero</option>
                  <option value="2">Febrero</option>
                  <option value="3">Marzo</option>
                  <option value="4">Abril</option>
                  <option value="5">Mayo</option>
                  <option value="6">Junio</option>
                  <option value="7">Julio</option>
                  <option value="8">Agosto</option>
                  <option value="9">Septiembre</option>
                  <option value="10">Octubre</option>
                  <option value="11">Noviembre</option>
                  <option value="12">Diciembre</option>
                </select> 
                <img src="<%=BASEURL%>/images/botones/iconos/obligatorio.gif">              	
				</span>
				</td>             
            </tr>		
        </table></td>
      </tr>
    </table>
<p>
<div align="center">
<p><img src="<%=BASEURL%>/images/botones/aceptar.gif" style="cursor:hand" title="Cerrar Periodo.." name="aceptar"  onClick="Cerrar ( anio.value, mes.value );" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);">&nbsp;
   <img src="<%=BASEURL%>/images/botones/salir.gif"  name="salir" style="cursor:hand" title="Salir al Menu Principal" onMouseOver="botonOver(this);" onClick="parent.close();" onMouseOut="botonOut(this);" ></p>
  <%String msg = request.getParameter("msg");
	if ( msg!=null && !msg.equals("") ){%>
                        
	  <table border="2" align="center">
   		<tr>
        	<td>
            	<table width="100%"  border="1" align="center"  bordercolor="#F7F5F4" bgcolor="#FFFFFF">
                	<tr>
                    	<td width="229" align="center" class="mensajes"><%=msg%></td>
                        <td width="29" background="<%=BASEURL%>/images/cuadronaranja.JPG">&nbsp;</td>
                        <td width="58">&nbsp;</td>
                    </tr>
                </table>
            </td>
		</tr>
	  </table>
	<%}%>
  </p>
</div>
</p>
</form>
</div> 
<%=datos[1]%>
</body>
</html>