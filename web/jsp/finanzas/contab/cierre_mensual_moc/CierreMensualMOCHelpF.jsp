<!--  
	 - Author(s)       :      LREALES
	 - Description     :      AYUDA FUNCIONAL - Cierre Mensual de los Modulos Operativos y Contables
	 - Date            :      29/06/2006 
	 - Copyright Notice:      TRANSPORTES SANCHEZ POLO S.A.
-->
<%@ include file="/WEB-INF/InitModel.jsp"%>
<HTML>
<HEAD>
<TITLE>AYUDA FUNCIONAL - Cierre Mensual de los Modulos Operativos y Contables</TITLE>
<META http-equiv=Content-Type content="text/html; charset=windows-1252">
<link href="../css/estilostsp.css" rel="stylesheet" type="text/css"> 
<link href="<%=BASEURL%>/css/estilostsp.css" rel="stylesheet" type="text/css">
<script type='text/javascript' src="<%= BASEURL %>/js/boton.js"></script> 
</HEAD>
<BODY> 
<% String BASEIMG = BASEURL +"/images/ayuda/contabilidad/cierre_mensual_moc/"; %>
  <table width="95%"  border="2" align="center">
    <tr>
      <td height="117" >
        <table width="100%" border="0" align="center">
          <tr  class="subtitulo">
            <td height="20"><div align="center">MANUAL DE CONTABILIDAD WEB</div></td>
          </tr>
          <tr class="subtitulo1">
            <td>Descripci&oacute;n del funcionamiento del programa para Cerrar Mensualmente  los Modulos Operativos y Contables.</td>
          </tr>
          <tr>
            <td  class="ayudaHtmlTexto"><div align="center">
              <p>&nbsp;</p>
              </div></td>
          </tr>
          <tr>
            <td  class="ayudaHtmlTexto"><p>En la siguiente pantalla se puede escoger el periodo que deseo cerrar, seleccionando el a&ntilde;o y el mes de este mismo.</p>            </td>
          </tr>
          <tr>
            <td  class="ayudaHtmlTexto"><div align="center"><IMG height=206 src="<%=BASEIMG%>image001.JPG" width=879 border=0 v:shapes="_x0000_i1143"></div></td>
          </tr>
          <tr>
            <td  class="ayudaHtmlTexto"><p>&nbsp;</p>
            <p>El sistema verifica que todos los campos obligatorios esten seleccionados, si no lo estan le saldr&aacute; en la pantalla el siguiente mensaje. </p></td>
          </tr>
          <tr>
            <td  class="ayudaHtmlTexto"><div align="center"><IMG height=126 src="<%=BASEIMG%>image_error001.JPG" width=320 border=0 v:shapes="_x0000_i1054"></div></td>
          </tr>
<tr>
  <td  class="ayudaHtmlTexto"><p>&nbsp;</p>
    <p>Si le sale este error en la pantalla, es por que el periodo seleccionado, no existe en nuestra base de datos, y por lo tanto no puede cerrarse.</p></td>
</tr>
<tr>
  <td  class="ayudaHtmlTexto"><div align="center"><IMG height=64 src="<%=BASEIMG%>image_error002.JPG" width=380 border=0 v:shapes="_x0000_i1054"></div></td>
</tr>
<tr>
  <td  class="ayudaHtmlTexto"><p>&nbsp;</p>
    <p>Cuando alla echo el procedimiento  correctamente, en la pantalla el sistema nos mostrar&aacute; el siguiente mensaje.<br>
      Y
      el proceso de cierre del periodo, el proceso de validaci&oacute;n de movimientos contables y el proceso de actualizaci&oacute;n de mayorizaci&oacute;n, se estar&aacute;n ejecutando internamente.</p>    </td>
</tr>
<tr>
  <td  class="ayudaHtmlTexto"><div align="center"><IMG height=66 src="<%=BASEIMG%>image002.JPG" width=383 border=0 v:shapes="_x0000_i1054"></div></td>
</tr>
<tr>
  <td  class="ayudaHtmlTexto"><p>&nbsp;</p>
    <p>Si en la pantalla el sistema  arroja el siguiente mensaje, es por que ese periodo escogido ya habia sido cerrado con anterioridad, y no hay necesidad de volverlo a cerrar. Sin embargo el proceso de validaci&oacute;n de movimientos contables y el proceso de actualizaci&oacute;n de mayorizaci&oacute;n, si ser&aacute;n ejecutados internamente.</p></td>
</tr>
<tr>
            <td  class="ayudaHtmlTexto"><div align="center"><IMG height=74 src="<%=BASEIMG%>image003.JPG" width=380 border=0 v:shapes="_x0000_i1054"></div></td>
          </tr>
      </table></td>
    </tr>
  </table>
  <p></p>
<center>
<img src = "<%=BASEURL%>/images/botones/salir.gif" style = "cursor:hand" name = "imgsalir" onClick = "parent.close();" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);">
</center>
</BODY>
</HTML>
