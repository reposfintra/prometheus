<!--
- Nombre P�gina :                  ConsultasSaldosContables.jsp                 
- Descripci�n :                    Pagina JSP, que muestra los saldos contables buscados                   
- Autor :                          LREALES                        
- Fecha Creado :                   12 de Junio de 2006, 03:30 P.M.                        
- Versi�n :                        1.0                                       
- Copyright :                      Fintravalores S.A.                   
--> 
<%@page contentType="text/html"%>
<%@page session="true"%>
<%@page errorPage="/error/ErrorPage.jsp"%>
<%@page import="java.util.*"%>
<%@page import="com.tsp.operation.model.*"%>
<%@page import="com.tsp.util.*"%>
<%@page import="com.tsp.operation.model.beans.*, java.util.*"%>
<%@page import="com.tsp.finanzas.contab.model.*"%>
<%@page import="com.tsp.finanzas.contab.model.beans.*, java.util.*"%>
<%@include file="/WEB-INF/InitModel.jsp"%>
<%@taglib uri="http://jsptags.com/tags/navigation/pager" prefix="pg" %>
<%
String path = application.getRealPath(model.menuService.getRealUrl(request.getRequestURI()));
String datos[] = model.menuService.getContenidoMenu(BASEURL,request.getRequestURI(),path);
%>
<html>
<head>
<title>Consulta de Saldos Contables</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link href="<%= BASEURL %>/css/estilostsp.css" rel="stylesheet" type="text/css">
<link href="../../css/estilostsp.css" rel="stylesheet" type="text/css"> 
<script type='text/javascript' src="<%= BASEURL %>/js/reporte.js"></script> 
<script type='text/javascript' src="<%= BASEURL %>/js/boton.js"></script>
</head>
<body onresize="redimensionar()" onload = 'redimensionar()'>
<div id="capaSuperior" style="position:absolute; width:100%; height:100px; z-index:0; left: 0px; top: 0px;">
	<jsp:include page="/toptsp.jsp?encabezado=Saldos Contables"/>
</div>
<div id="capaCentral" style="position:absolute; width:100%; height:87%; z-index:0; left: 0px; top: 100px; overflow: scroll;">
		  <% 
		    				
			String d = (String)session.getAttribute("distrito");
			String ci = (String)session.getAttribute("cuenta_ini");
			String cf = (String)session.getAttribute("cuenta_fin");
			String ai = (String)session.getAttribute("anio_ini");
			String af = (String)session.getAttribute("anio_fin");
			String mi = (String)session.getAttribute("mes_ini");
			String mf = (String)session.getAttribute("mes_fin");
			String tp = (String)session.getAttribute("tipo_presentacion");
			String is = (String)session.getAttribute("incluye_subledger");
			
			int num_mi = Integer.parseInt( mi );
        	int num_mf = Integer.parseInt( mf );
			
			int num_ai = Integer.parseInt( ai );
        	int num_af = Integer.parseInt( af );
		
			int k = 0;
						
			double deb = 0;
			double cre = 0;
			double fdm = 0;
						
			Vector vec_saldos = new Vector();
						
			if ( is.equals ("no") ) {
				modelcontab.consultasSaldosContablesService.listaSaldosContables( d, ai, af, ci, cf );
				vec_saldos = modelcontab.consultasSaldosContablesService.getVec_saldos();
			}
			
			if ( is.equals ("si") ) {
				modelcontab.consultasSaldosContablesService.listaIncluyeSubledger( d, ai, af, ci, cf );
				vec_saldos = modelcontab.consultasSaldosContablesService.getVec_saldos();
			}
	 
		  String index =  "center";
   	      int maxPageItems = 15;
          int maxIndexPages = 15;
	   
	 %>
  <%String msg = request.getParameter("msg");
	if ( msg!=null && !msg.equals("") ){%>
                        
	  <table border="2" align="center">
   		<tr>
        	<td>
            	<table width="100%"  border="1" align="center"  bordercolor="#F7F5F4" bgcolor="#FFFFFF">
                	<tr>
                    	<td width="229" align="center" class="mensajes"><%=msg%></td>
                        <td width="29" background="<%=BASEURL%>/images/cuadronaranja.JPG">&nbsp;</td>
                        <td width="58">&nbsp;</td>
                    </tr>
                </table>
            </td>
		</tr>
	  </table>
	<%}%>
	   <%   if( vec_saldos != null  && vec_saldos.size() > 0 ){ %>

      <img src="<%=BASEURL%>/images/botones/regresar.gif" name="Regresar1" title='Regresar al Filtro' onClick = "window.location='<%=BASEURL%>/jsp/finanzas/contab/consultas_saldos_contables/FiltroConsultasSaldosContables.jsp'; this.disabled=true;" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" style="cursor:hand">
 	  <input name="Salir1" src="<%=BASEURL%>/images/botones/salir.gif" title="Salir al Menu Principal" type="image" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" style="cursor:hand" class="boton" onclick="window.close();">
<br>
	  		<table border="2" align="center">
    			<tr>
        			<td>
            			<table width="100%"  border="0" cellpadding="0" cellspacing="0" class="barratitulo">
                            <tr>
                                <td width="50%" class="subtitulo1">SALDOS CONTABLES </td>
                                <td width="50%" class="barratitulo"><img src="<%=BASEURL%>/images/titulo.gif" align="left"><%=datos[0]%></td>
                            </tr>
          				</table>
          				
						<table width="800" border="1" align="center" bordercolor="#999999" bgcolor="#F7F5F4">
                            <tr class="tblTitulo">
                                <td width="100" rowspan="2" nowrap><div align="center">Cuenta</div></td>
                                <td width="100" rowspan="2" nowrap><div align="center">Nombre Corto</div></td>
                                <td width="100" rowspan="2" nowrap><div align="center">Saldo Anterior</div></td>
                            <%
							num_ai = Integer.parseInt( ai );
        					num_af = Integer.parseInt( af );
							if ( num_ai == num_af ){
							  	for ( num_mi = Integer.parseInt( mi ); num_mi <= num_mf; num_mi++ ){
							%>							
							<%if ( tp.equals ("1") ){%>   
                                <td colspan="3" nowrap align="center"><p><%=num_ai%> - <%=num_mi%></p></td>
								<%}
								if ( tp.equals ("2") ){%>  
								<td colspan="2" nowrap align="center"><p><%=num_ai%> - <%=num_mi%></p></td>
								<%}
								if ( tp.equals ("3") ){%> 
								<td colspan="1" nowrap align="center"><p><%=num_ai%> - <%=num_mi%></p></td>  
								<%}%>
								<%}
							} else if ( num_ai < num_af ){
								for( num_mi = Integer.parseInt( mi ); num_mi <= 12; num_mi++ ){
									
								%>
										<%if ( tp.equals ("1") ){%>   
										<td colspan="3" nowrap align="center"><p><%=num_ai%> - <%=num_mi%></p></td>
										<%}
										if ( tp.equals ("2") ){%>  
										<td colspan="2" nowrap align="center"><p><%=num_ai%> - <%=num_mi%></p></td>
										<%}
										if ( tp.equals ("3") ){%> 
										<td colspan="1" nowrap align="center"><p><%=num_ai%> - <%=num_mi%></p></td>  
										<%}%>
								    <% if ( num_mi == 12 ){
											for( k = 1; k <= num_mf; k++ ){
								    %>
									<%if ( tp.equals ("1") ){%>   
										<td colspan="3" nowrap align="center"><p><%=num_af%> - <%=k%></p></td>
										<%}
										if ( tp.equals ("2") ){%>  
										<td colspan="2" nowrap align="center"><p><%=num_af%> - <%=k%></p></td>
										<%}
										if ( tp.equals ("3") ){%> 
										<td colspan="1" nowrap align="center"><p><%=num_af%> - <%=k%></p></td> 
										<%}%>
												
											<%}
										}
								}
							}%>
							<%if ( tp.equals ("2") ){%>
								<td width="100" rowspan="2" nowrap><div align="center">Saldo Actual</div></td>
							<%}%>
                            </tr>
							<tr class="tblTitulo">
							<%
							num_ai = Integer.parseInt( ai );
        					num_af = Integer.parseInt( af );
							if ( num_ai == num_af ){
							  	for ( num_mi = Integer.parseInt( mi ); num_mi <= num_mf; num_mi++ ){
							%>							
							<%if ( tp.equals ("1") || tp.equals ("2") ){%>  
                                  <td width="100" align="center" nowrap>Debito </td>
								  <td width="100" align="center" nowrap>Cr&eacute;dito </td>
								  <%} 
								  if ( tp.equals ("1") || tp.equals ("3") ){%>
								  	<td width="100" align="center" nowrap>Fin de Mes </td>
								  <%}%>
								<%}
							} else if ( num_ai < num_af ){
								for( num_mi = Integer.parseInt( mi ); num_mi <= 12; num_mi++ ){									
								%>
									<%if ( tp.equals ("1") || tp.equals ("2") ){%>  
										  <td width="100" align="center" nowrap>Debito </td>
										  <td width="100" align="center" nowrap>Cr&eacute;dito </td>
										  <%} 
								  			if ( tp.equals ("1") || tp.equals ("3") ){%>
										  <td width="100" align="center" nowrap>Fin de Mes </td>
										  <%}%>
								    <% if ( num_mi == 12 ){
											for( k = 1; k <= num_mf; k++ ){
								    %>
												<%if ( tp.equals ("1") || tp.equals ("2") ){%>  
												  <td width="100" align="center" nowrap>Debito </td>
												  <td width="100" align="center" nowrap>Cr&eacute;dito </td>
												  <%} 
								  					if ( tp.equals ("1") || tp.equals ("3") ){%>
												  <td width="100" align="center" nowrap>Fin de Mes </td>
												  <%}%>
											<%}
										}
								}
							}%>
                            </tr>
							<pg:pager
							items="<%=vec_saldos.size()%>"
							index="<%= index %>"
							maxPageItems="<%= maxPageItems %>"
							maxIndexPages="<%= maxIndexPages %>"
							isOffset="<%= true %>"
							export="offset,currentPageNumber=pageNumber"
							scope="request">
							  <%
								for (int i = offset.intValue(), l = Math.min(i + maxPageItems, vec_saldos.size()); i < l; i++){
								//for( int i = 0; i < vec_cuentas.size(); i++ ){
									SaldosContables saldos = ( SaldosContables ) vec_saldos.elementAt( i );
									
									String cue = ( saldos.getCuenta().equals("")?"&nbsp;":saldos.getCuenta() );
									String nom_cor = ( saldos.getNombre_corto().equals("")?"&nbsp;":saldos.getNombre_corto() );
									//double sal_ant = ( saldos.getSaldoant() != null?saldos.getSaldoant():0 );
									double sal_ant = saldos.getSaldoant();
									
									double sal_act = saldos.getSaldoact();
									
									int num_anio = Integer.parseInt( saldos.getAnio() );
							%>
							    <pg:item>
									<tr class="<%=(i % 2 == 0 )?"filagris":"filaazul"%>">
									<!-- <tr class="< %=(i % 2 == 0 )?"filagris":"filaazul"%>" onMouseOver='cambiarColorMouse(this)' style="cursor:hand" title="Modificar Cuenta..." onClick="window.location='< %=CONTROLLERCONTAB%>?estado=SearchUpdate&accion=PlanDeCuentas&listar=False&cuenta=< %=cue%>'; this.disabled=true;"> -->
									  <td nowrap  align="left" abbr="" class="bordereporte"><%=cue%></td>
									  <td align="left" abbr="" nowrap class="bordereporte"><%=nom_cor%></td>
									  
									  <%
									  if ( sal_ant < 0 ) {
									  %>
									  <td align="right" style="color:red;" nowrap class="bordereporte" abbr=""><%= Util.customFormat( sal_ant ) %></td>
									  <%
									  } else {
									  %>
									  <td align="right" nowrap class="bordereporte" abbr=""><%= Util.customFormat( sal_ant ) %></td>
									  <%
									  }
									  %>
									  
									  <%
										num_ai = Integer.parseInt( ai );
										num_af = Integer.parseInt( af );
										if ( num_ai == num_af ){
											for ( num_mi = Integer.parseInt( mi ); num_mi <= num_mf; num_mi++ ){		
											  if( num_ai == num_anio ){										
												if ( num_mi == 1 ){
													deb = saldos.getMovdeb01();
													cre = saldos.getMovcre01();
													fdm = saldos.getFindemes01();
												} else if ( num_mi == 2 ){
													deb = saldos.getMovdeb02();
													cre = saldos.getMovcre02();
													fdm = saldos.getFindemes02();
												} else if ( num_mi == 3 ){
													deb = saldos.getMovdeb03();
													cre = saldos.getMovcre03();
													fdm = saldos.getFindemes03();
												} else if ( num_mi == 4 ){
													deb = saldos.getMovdeb04();
													cre = saldos.getMovcre04();
													fdm = saldos.getFindemes04();
												} else if ( num_mi == 5 ){
													deb = saldos.getMovdeb05();
													cre = saldos.getMovcre05();
													fdm = saldos.getFindemes05();
												} else if ( num_mi == 6 ){
													deb = saldos.getMovdeb06();
													cre = saldos.getMovcre06();
													fdm = saldos.getFindemes06();
												} else if ( num_mi == 7 ){
													deb = saldos.getMovdeb07();
													cre = saldos.getMovcre07();
													fdm = saldos.getFindemes07();
												} else if ( num_mi == 8 ){
													deb = saldos.getMovdeb08();
													cre = saldos.getMovcre08();
													fdm = saldos.getFindemes08();
												} else if ( num_mi == 9 ){
													deb = saldos.getMovdeb09();
													cre = saldos.getMovcre09();
													fdm = saldos.getFindemes09();
												} else if ( num_mi == 10 ){
													deb = saldos.getMovdeb10();
													cre = saldos.getMovcre10();
													fdm = saldos.getFindemes10();
												} else if ( num_mi == 11 ){
													deb = saldos.getMovdeb11();
													cre = saldos.getMovcre11();
													fdm = saldos.getFindemes11();
												} else if ( num_mi == 12 ){
													deb = saldos.getMovdeb12();
													cre = saldos.getMovcre12();
													fdm = saldos.getFindemes12();
												}
											  }
										%>	<%if ( tp.equals ("1") || tp.equals ("2") ){%>  				
												  <%
												  if ( deb < 0 ) {
												  %>
												  <td align="right" style="color:red;" nowrap class="bordereporte" abbr=""><%= Util.customFormat( deb ) %></td>
												  <%
												  } else {
												  %>
												  <td align="right" nowrap class="bordereporte" abbr=""><%= Util.customFormat( deb ) %></td>
												  <%
												  }
												  %>		
												    
											  	  <%
												  if ( cre < 0 ) {
												  %>
												  <td align="right" style="color:red;" nowrap class="bordereporte" abbr=""><%= Util.customFormat( cre ) %></td>
												  <%
												  } else {
												  %>
												  <td align="right" nowrap class="bordereporte" abbr=""><%= Util.customFormat( cre ) %></td>
												  <%
												  }
												  %>												  
											  <%} 
								  				if ( tp.equals ("1") || tp.equals ("3") ){%>
													  <%
													  if ( fdm < 0 ) {
													  %>
													  <td align="right" style="color:red;" nowrap class="bordereporte" abbr=""><%= Util.customFormat( fdm ) %></td>
													  <%
													  } else {
													  %>
													  <td align="right" nowrap class="bordereporte" abbr=""><%= Util.customFormat( fdm ) %></td>
													  <%
													  }
													  %>											  
											  <%}%>
											<%}
										} else if ( num_ai < num_af ){
											for( num_mi = Integer.parseInt( mi ); num_mi <= 12; num_mi++ ){
											  //if( num_ai == num_anio ){
												if ( num_mi == 1 ){
													deb = saldos.getMovdeb01();
													cre = saldos.getMovcre01();
													fdm = saldos.getFindemes01();
												} else if ( num_mi == 2 ){
													deb = saldos.getMovdeb02();
													cre = saldos.getMovcre02();
													fdm = saldos.getFindemes02();
												} else if ( num_mi == 3 ){
													deb = saldos.getMovdeb03();
													cre = saldos.getMovcre03();
													fdm = saldos.getFindemes03();
												} else if ( num_mi == 4 ){
													deb = saldos.getMovdeb04();
													cre = saldos.getMovcre04();
													fdm = saldos.getFindemes04();
												} else if ( num_mi == 5 ){
													deb = saldos.getMovdeb05();
													cre = saldos.getMovcre05();
													fdm = saldos.getFindemes05();
												} else if ( num_mi == 6 ){
													deb = saldos.getMovdeb06();
													cre = saldos.getMovcre06();
													fdm = saldos.getFindemes06();
												} else if ( num_mi == 7 ){
													deb = saldos.getMovdeb07();
													cre = saldos.getMovcre07();
													fdm = saldos.getFindemes07();
												} else if ( num_mi == 8 ){
													deb = saldos.getMovdeb08();
													cre = saldos.getMovcre08();
													fdm = saldos.getFindemes08();
												} else if ( num_mi == 9 ){
													deb = saldos.getMovdeb09();
													cre = saldos.getMovcre09();
													fdm = saldos.getFindemes09();
												} else if ( num_mi == 10 ){
													deb = saldos.getMovdeb10();
													cre = saldos.getMovcre10();
													fdm = saldos.getFindemes10();
												} else if ( num_mi == 11 ){
													deb = saldos.getMovdeb11();
													cre = saldos.getMovcre11();
													fdm = saldos.getFindemes11();
												} else if ( num_mi == 12 ){
													deb = saldos.getMovdeb12();
													cre = saldos.getMovcre12();
													fdm = saldos.getFindemes12();
												}
											  //}
											%>
											<%if ( tp.equals ("1") || tp.equals ("2") ){%> 
													  <%
													  if ( deb < 0 ) {
													  %>
													  <td align="right" style="color:red;" nowrap class="bordereporte" abbr=""><%= Util.customFormat( deb ) %></td>
													  <%
													  } else {
													  %>
													  <td align="right" nowrap class="bordereporte" abbr=""><%= Util.customFormat( deb ) %></td>
													  <%
													  }
													  %>		
														
													  <%
													  if ( cre < 0 ) {
													  %>
													  <td align="right" style="color:red;" nowrap class="bordereporte" abbr=""><%= Util.customFormat( cre ) %></td>
													  <%
													  } else {
													  %>
													  <td align="right" nowrap class="bordereporte" abbr=""><%= Util.customFormat( cre ) %></td>
													  <%
													  }
													  %>
												<%} 
												if ( tp.equals ("1") || tp.equals ("3") ){%>
													  <%
													  if ( fdm < 0 ) {
													  %>
													  <td align="right" style="color:red;" nowrap class="bordereporte" abbr=""><%= Util.customFormat( fdm ) %></td>
													  <%
													  } else {
													  %>
													  <td align="right" nowrap class="bordereporte" abbr=""><%= Util.customFormat( fdm ) %></td>
													  <%
													  }
													  %>	
											    <%}%>
												<% if ( num_mi == 12 ){
														for( k = 1; k <= num_mf; k++ ){
													//if( num_ai == num_anio ){
														if ( num_mi == 1 ){
													deb = saldos.getMovdeb01();
													cre = saldos.getMovcre01();
													fdm = saldos.getFindemes01();
												} else if ( num_mi == 2 ){
													deb = saldos.getMovdeb02();
													cre = saldos.getMovcre02();
													fdm = saldos.getFindemes02();
												} else if ( num_mi == 3 ){
													deb = saldos.getMovdeb03();
													cre = saldos.getMovcre03();
													fdm = saldos.getFindemes03();
												} else if ( num_mi == 4 ){
													deb = saldos.getMovdeb04();
													cre = saldos.getMovcre04();
													fdm = saldos.getFindemes04();
												} else if ( num_mi == 5 ){
													deb = saldos.getMovdeb05();
													cre = saldos.getMovcre05();
													fdm = saldos.getFindemes05();
												} else if ( num_mi == 6 ){
													deb = saldos.getMovdeb06();
													cre = saldos.getMovcre06();
													fdm = saldos.getFindemes06();
												} else if ( num_mi == 7 ){
													deb = saldos.getMovdeb07();
													cre = saldos.getMovcre07();
													fdm = saldos.getFindemes07();
												} else if ( num_mi == 8 ){
													deb = saldos.getMovdeb08();
													cre = saldos.getMovcre08();
													fdm = saldos.getFindemes08();
												} else if ( num_mi == 9 ){
													deb = saldos.getMovdeb09();
													cre = saldos.getMovcre09();
													fdm = saldos.getFindemes09();
												} else if ( num_mi == 10 ){
													deb = saldos.getMovdeb10();
													cre = saldos.getMovcre10();
													fdm = saldos.getFindemes10();
												} else if ( num_mi == 11 ){
													deb = saldos.getMovdeb11();
													cre = saldos.getMovcre11();
													fdm = saldos.getFindemes11();
												} else if ( num_mi == 12 ){
													deb = saldos.getMovdeb12();
													cre = saldos.getMovcre12();
													fdm = saldos.getFindemes12();
												}
											//}
												%>
													<%if ( tp.equals ("1") || tp.equals ("2") ){%>
															  <%
															  if ( deb < 0 ) {
															  %>
															  <td align="right" style="color:red;" nowrap class="bordereporte" abbr=""><%= Util.customFormat( deb ) %></td>
															  <%
															  } else {
															  %>
															  <td align="right" nowrap class="bordereporte" abbr=""><%= Util.customFormat( deb ) %></td>
															  <%
															  }
															  %>		
																
															  <%
															  if ( cre < 0 ) {
															  %>
															  <td align="right" style="color:red;" nowrap class="bordereporte" abbr=""><%= Util.customFormat( cre ) %></td>
															  <%
															  } else {
															  %>
															  <td align="right" nowrap class="bordereporte" abbr=""><%= Util.customFormat( cre ) %></td>
															  <%
															  }
															  %>
														  <%} 
															if ( tp.equals ("1") || tp.equals ("3") ){%>
																  <%
																  if ( fdm < 0 ) {
																  %>
																  <td align="right" style="color:red;" nowrap class="bordereporte" abbr=""><%= Util.customFormat( fdm ) %></td>
																  <%
																  } else {
																  %>
																  <td align="right" nowrap class="bordereporte" abbr=""><%= Util.customFormat( fdm ) %></td>
																  <%
																  }
																  %>	
															  <%}%>
														<%}
													}
											}
										}%>
										<%if ( tp.equals ("2") ){%>
											  <%
											  if ( sal_act < 0 ) {
											  %>
											  <td align="right" style="color:red;" nowrap class="bordereporte" abbr=""><%= Util.customFormat( sal_act ) %></td>
											  <%
											  } else {
											  %>
											  <td align="right" nowrap class="bordereporte" abbr=""><%= Util.customFormat( sal_act ) %></td>
											  <%
											  }
											  %>
										<%}%>
									</tr>
						  		</pg:item>
							<%}%>
							<tr class="pie">
								<td td height="20" colspan="27" nowrap align="center">          
									<pg:index>
										<jsp:include page="/WEB-INF/jsp/googlesinimg.jsp" flush="true"/>
									</pg:index>
								</td>
							</tr>
							</pg:pager> 
        </table></td>
    </tr>
 </table>
 <br>
 <img src="<%=BASEURL%>/images/botones/regresar.gif" title='Regresar al Filtro' name="Regresar2" onClick = "window.location='<%=BASEURL%>/jsp/finanzas/contab/consultas_saldos_contables/FiltroConsultasSaldosContables.jsp'; this.disabled=true;" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" style="cursor:hand">
 	  <input name="Salir2" src="<%=BASEURL%>/images/botones/salir.gif" type="image" title="Salir al Menu Principal" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" style="cursor:hand" class="boton" onclick="window.close();">
<%}
	 else { %>                    
	  <%out.print("<table width=379 border=2 align=center><tr><td><table width=100%  border=1 align=center  bordercolor=#F7F5F4 bgcolor=#FFFFFF><tr><td width=350 align=center class=mensajes>Su busqueda no arrojo resultados!!</td><td width=100><img src="+BASEURL+"/images/cuadronaranja.JPG></td></tr></table></td></tr></table>");%>
	  <br>
	  <img title='Regresar al Filtro' src="<%= BASEURL %>/images/botones/regresar.gif" onClick = "window.location='<%=BASEURL%>/jsp/finanzas/contab/consultas_saldos_contables/FiltroConsultasSaldosContables.jsp'; this.disabled=true;" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" style="cursor:hand">
	  <img title='Salir al Menu' src="<%=BASEURL%>/images/botones/salir.gif" name="imgsalir"  onMouseOver="botonOver(this);" onClick="window.close();" onMouseOut="botonOut(this);" style="cursor:hand">
	<%}%>

</div>
<%=datos[1]%>
</body>
</html>