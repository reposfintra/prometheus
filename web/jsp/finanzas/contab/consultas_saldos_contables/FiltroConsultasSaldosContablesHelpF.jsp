<!--  
	 - Author(s)       :      LREALES
	 - Description     :      AYUDA FUNCIONAL - Consultar Saldos Contables
	 - Date            :      16/06/2006 
	 - Copyright Notice:      TRANSPORTES SANCHEZ POLO S.A.
-->
<%@ include file="/WEB-INF/InitModel.jsp"%>
<HTML>
<HEAD>
<TITLE>AYUDA FUNCIONAL - Consultar Saldos Contables</TITLE>
<META http-equiv=Content-Type content="text/html; charset=windows-1252">
<link href="../css/estilostsp.css" rel="stylesheet" type="text/css"> 
<link href="<%=BASEURL%>/css/estilostsp.css" rel="stylesheet" type="text/css">
<script type='text/javascript' src="<%= BASEURL %>/js/boton.js"></script> 
</HEAD>
<BODY> 
<% String BASEIMG = BASEURL +"/images/ayuda/contabilidad/consultas_saldos_contables/"; %>
  <table width="95%"  border="2" align="center">
    <tr>
      <td height="117" >
        <table width="100%" border="0" align="center">
          <tr  class="subtitulo">
            <td height="20"><div align="center">MANUAL DE CONTABILIDAD WEB</div></td>
          </tr>
          <tr class="subtitulo1">
            <td>Descripci&oacute;n del funcionamiento del programa para Consultar los Saldos Contables.</td>
          </tr>
          <tr>
            <td  class="ayudaHtmlTexto"><div align="center">
              <p>&nbsp;</p>
              </div></td>
          </tr>
          <tr>
            <td  class="ayudaHtmlTexto"><p>En la siguiente pantalla se pueden especificar los datos pedidos, para realizar la busqueda deseada.</p>            </td>
          </tr>
          <tr>
            <td  class="ayudaHtmlTexto"><div align="center"><IMG height=195 src="<%=BASEIMG%>image001.JPG" width=789 border=0 v:shapes="_x0000_i1143"></div></td>
          </tr>
          <tr>
            <td  class="ayudaHtmlTexto"><p>&nbsp;</p>
            <p>El sistema verifica que todos los campos obligatorios esten llenos, si no lo estan le saldr&aacute; en la pantalla el siguiente mensaje. </p></td>
          </tr>
          <tr>
            <td  class="ayudaHtmlTexto"><div align="center"><IMG height=126 src="<%=BASEIMG%>image_error001.JPG" width=320 border=0 v:shapes="_x0000_i1054"></div></td>
          </tr>
<tr>
  <td  class="ayudaHtmlTexto"><p>&nbsp;</p>
    <p>Si le sale este error en la pantalla, es por que el periodo inicial no puede ser mayor que el periodo final. </p></td>
</tr>
<tr>
  <td  class="ayudaHtmlTexto"><div align="center"><IMG height=126 src="<%=BASEIMG%>image_error002.JPG" width=349 border=0 v:shapes="_x0000_i1054"></div></td>
</tr>
<tr>
  <td  class="ayudaHtmlTexto"><p>&nbsp;</p>
    <p>El sistema nos arrojar&aacute; el siguiente mensaje, si en nuestra base de satos, no se encuentra ning&uacute;n saldo correspondiente a la busqueda realizada. </p>    </td>
</tr>
<tr>
  <td  class="ayudaHtmlTexto"><div align="center"><IMG height=82 src="<%=BASEIMG%>image_error003.JPG" width=721 border=0 v:shapes="_x0000_i1054"></div></td>
</tr>
<tr>
  <td  class="ayudaHtmlTexto"><p>&nbsp;</p>
    <p>Cuando alla echo el procedimiento de busqueda correctamente, en la pantalla el sistema nos mostrar&aacute; la informaci&oacute;n que deseaba encontrar. </p></td>
</tr>
<tr>
            <td  class="ayudaHtmlTexto"><div align="center"><IMG height=387 src="<%=BASEIMG%>image002.JPG" width=922 border=0 v:shapes="_x0000_i1054"></div></td>
          </tr>
      </table></td>
    </tr>
  </table>
  <p></p>
<center>
<img src = "<%=BASEURL%>/images/botones/salir.gif" style = "cursor:hand" name = "imgsalir" onClick = "parent.close();" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);">
</center>
</BODY>
</HTML>
