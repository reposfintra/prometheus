<!--  
	 - Author(s)       :      LREALES
	 - Description     :      AYUDA DESCRIPTIVA - Consultar Saldos Contables
	 - Date            :      16/06/2006 
	 - Copyright Notice:      TRANSPORTES SANCHEZ POLO S.A.
-->
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN""http://www.w3.org/TR/html4/loose.dtd">
<%@page session="true"%> 
<%@page errorPage="/error/ErrorPage.jsp"%>
<%@page import="com.tsp.operation.model.beans.*, java.util.*"%>
<%@ include file="/WEB-INF/InitModel.jsp"%>
<%@ taglib uri="http://jakarta.apache.org/taglibs/input-0.90" prefix="input" %>
<html>
<head>
<title>AYUDA DESCRIPTIVA - Consultar Saldos Contables</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link href="<%= BASEURL %>/css/estilostsp.css" rel="stylesheet" type="text/css">
<script type='text/javascript' src="<%= BASEURL %>/js/boton.js"></script>
</head>

<body>
<br>
<table width="696"  border="2" align="center">
  <tr>
    <td width="811" >
      <table width="100%" border="0" align="center">
        <tr  class="subtitulo">
          <td height="24" colspan="2"><div align="center">Saldos Contables</div></td>
        </tr>
        <tr class="subtitulo1">
          <td colspan="2">Consultar Saldos Contables - Pantalla Preliminar</td>
        </tr>
		<tr class="subtitulo1">
          <td colspan="2">INFORMACION DE LA CUENTA </td>
        </tr>
        <tr>
          <td  class="fila">'Tipo de Presentaci&oacute;n' </td>
          <td  class="ayudaHtmlTexto"><p>Existen 3 tipos de presentaciones, para la consulta:<br>
            1&deg; Movimientos y Saldos : Muestra toda la informaci&oacute;n de los Movimientos y Saldos de la busqueda realizada. <br>
            2&deg; Movimientos : Muestra SOLO los Movimientos de la busqueda realizada. <br>
            3&deg; Saldos
: Muestra SOLO los Saldos de la busqueda realizada.    </p>            </td>
        </tr>
        <tr>
          <td  class="fila">'Cuenta Inicial' </td>
          <td  class="ayudaHtmlTexto">Campo para digitar  el rango inicial del c&oacute;digo de la cuenta que deseo buscar. Este campo es de m&aacute;ximo 25 caracteres.</td>
        </tr>
        <tr>
          <td  class="fila">'Cuenta Final'</td>
          <td  class="ayudaHtmlTexto">Campo para digitar el rango final del c&oacute;digo de la cuenta que deseo buscar. Este campo es de m&aacute;ximo 25 caracteres.</td>
        </tr>
        <tr>
          <td  class="fila">'Periodo Inicial'</td>
          <td  class="ayudaHtmlTexto">Campo de selecci&oacute;n donde escojo el rango inicial del periodo que deseo buscar. </td>
        </tr>
        <tr>
          <td  class="fila">'Periodo Final'</td>
          <td  class="ayudaHtmlTexto">Campo de selecci&oacute;n donde escojo el rango final del periodo que deseo buscar. </td>
        </tr>
        <tr>
          <td  class="fila">'Incluye Subledger?'</td>
          <td  class="ayudaHtmlTexto">Campo de selecci&oacute;n donde se escoje si la busqueda incluye o no subledgers. </td>
        </tr>
        <tr>
          <td class="fila">'SALDOS CONTABLES' </td>
          <td  class="ayudaHtmlTexto">Lista de los saldos contables arrojados por la busqueda realizada. Aqui se escoje una cuenta para consultar su movimiento auxiliar. </td>
        </tr>
        <tr>
          <td class="fila">Bot&oacute;n Buscar </td>
          <td  class="ayudaHtmlTexto">Bot&oacute;n que permite realizar  la busqueda de los saldos contables, que aparezcan en los parametros especificados. </td>
        </tr>
		<tr>
          <td class="fila">Bot&oacute;n Cancelar </td>
          <td  class="ayudaHtmlTexto">Bot&oacute;n que resetea la pantalla dejandola en su estado inicial de consulta.</td>
        </tr>
		<tr>
		  <td class="fila">Bot&oacute;n Regresar </td>
		  <td  class="ayudaHtmlTexto">Bot&oacute;n que permite regresar a la vista anterior. </td>
	    </tr>
		<tr>
          <td width="200" class="fila">Bot&oacute;n Salir</td>
          <td width="474"  class="ayudaHtmlTexto">Bot&oacute;n para salir de la vista 'Consultar Saldos Contables' y volver a la vista del men&uacute;.</td>
        </tr>
      </table>
    </td>
  </tr>
</table>
<p></p>
<center>
<img src = "<%=BASEURL%>/images/botones/salir.gif" style = "cursor:hand" name = "imgsalir" onClick = "parent.close();" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);">
</center>
</body>
</html>
