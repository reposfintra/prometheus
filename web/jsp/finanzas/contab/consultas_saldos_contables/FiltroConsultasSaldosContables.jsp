<!--
- Nombre P�gina :                  FiltroConsultasSaldosContables.jsp                 
- Descripci�n :                    Pagina JSP, que realiza el filtro de busqueda de los saldos contables              
- Autor :                          LREALES                        
- Fecha Creado :                   12 de Junio de 2006, 01:33 P.M.                        
- Versi�n :                        1.0                                       
- Copyright :                      Fintravalores S.A.                   
-->

<%-- Declaracion de librerias--%>
<%@page contentType="text/html"%>
<%@ page session="true"%>
<%@ page errorPage="../error/error.jsp"%>
<%@page import="com.tsp.operation.model.beans.*, java.util.*"%>
<%@ include file="/WEB-INF/InitModel.jsp"%>
<%@page import="com.tsp.operation.model.*"%>
<%@page import="com.tsp.util.*"%>
<%@ taglib uri="http://jakarta.apache.org/taglibs/input-0.90" prefix="input" %>
<%
  String path = application.getRealPath(model.menuService.getRealUrl(request.getRequestURI()));
  String datos[] = model.menuService.getContenidoMenu(BASEURL,request.getRequestURI(),path);
%>
<html>
<head><title>Consulta de Saldos Contables</title>
<link href="<%= BASEURL %>/css/estilostsp.css" rel="stylesheet" type="text/css">
<script src='<%=BASEURL%>/js/date-picker.js'></script>
<script type='text/javascript' src="<%= BASEURL %>/js/boton.js"></script>
<script type='text/javascript' src="<%= BASEURL %>/js/validar.js"></script>
</head>
<body onLoad="redimensionar();" onResize="redimensionar();">
<div id="capaSuperior" style="position:absolute; width:100%; height:100px; z-index:0; left: 0px; top: 0px;">
<jsp:include page="/WEB-INF/toptsp.jsp?encabezado=Saldos Contables"/>
</div>

<div id="capaCentral" style="position:absolute; width:100%; height:85%; z-index:0; left: 0px; top: 100px; overflow: scroll;"> 
<%
    Usuario usuario = (Usuario) session.getAttribute("Usuario");   
	
	String fecha = Utility.getHoy("-"); 
	String anio1 = fecha.substring(0,4);
	int anio1int = Integer.parseInt ( anio1 );
	int anio2int = anio1int - 1;
	int anio3int = anio1int - 2;
	String anio2 = "" + anio2int;
	String anio3 = "" + anio3int;
%>
<script>
	var controlador ="<%=CONTROLLERCONTAB%>";
	
	function Buscar ( cuenta_ini, cuenta_fin, anio_inicial, anio_final, mes_inicial, mes_final ){
		
		if( ( cuenta_ini != "" ) && ( cuenta_fin != "" ) ){
						
			if ( ( anio_inicial < anio_final ) || ( ( anio_inicial == anio_final ) && ( mes_inicial <= mes_final ) ) ){
				
				document.form2.action = controlador+"?estado=Consultas&accion=SaldosContables";
				document.form2.submit();
			
			} else{ 
			
				alert( "El periodo inicial NO puede ser mayor que el periodo final!" );
				
			}			
						
		} else{
		
			alert( "Por Favor No Deje Los Campos Obligatorios Vacios!" );
			
		}
	}
</script>

<form name="form2" method="post">
    <table width="80%" border="2" align="center">
      <tr>
        <td><table width="100%" align="center"  class="tablaInferior">
            <tr>
				<td width="50%" colspan="2" class="subtitulo1">Filtro para Consultar los Saldos Contables </td>
				<td width="50%" colspan="2" class="barratitulo"><img src="<%=BASEURL%>/images/titulo.gif" width="32" height="20" align="left"><%=datos[0]%></td>
            </tr>
            <tr class="fila">
              <td align="left" valign="middle" >Tipo de Presentaci&oacute;n: </td>
              <td width="25%" align="left" valign="middle" >
			  <label><input type="radio" name="tipo_presentacion" value="1" checked>
			  Movimientos y Saldos </label></td>
              <td valign="middle">
			  <label><input type="radio" name="tipo_presentacion" value="2">
			  Movimientos</label></td>
              <td width="25%" valign="middle">
			  <label><input type="radio" name="tipo_presentacion" value="3">
			  Saldos</label></td>
            </tr>
            <tr class="fila">
              <td align="left" valign="middle" >Cuenta Inicial </td>
              <td align="left" valign="middle" ><input name="cuenta_ini" type="text" class="textbox" id="cuenta_ini3" onKeyPress="soloAlfa(event)" value="1" size="25" maxlength="25">
              <img src="<%=BASEURL%>/images/botones/iconos/obligatorio.gif"></td>
              <td valign="middle">Cuenta Final </td>
              <td valign="middle"><input name="cuenta_fin" type="text" class="textbox" id="cuenta_fin" onKeyPress="soloAlfa(event)" value="I999999999999" size="25" maxlength="25">
              <img src="<%=BASEURL%>/images/botones/iconos/obligatorio.gif"></td>
            </tr>
            <tr class="fila">
              <td align="left" valign="middle" >Periodo Inicial</td>
              <td align="left" valign="middle" ><span class="Letras">
                <select name="anio_inicial" id="anio_inicial">
                  <option value="<%=anio3%>"><%=anio3%></option>
                  <option value="<%=anio2%>"><%=anio2%></option>
                  <option value="<%=anio1%>" selected><%=anio1%></option>
                </select>
                <select name="mes_inicial" id="mes_inicial">
                  <option value="1" selected>Enero</option>
                  <option value="2">Febrero</option>
                  <option value="3">Marzo</option>
                  <option value="4">Abril</option>
                  <option value="5">Mayo</option>
                  <option value="6">Junio</option>
                  <option value="7">Julio</option>
                  <option value="8">Agosto</option>
                  <option value="9">Septiembre</option>
                  <option value="10">Octubre</option>
                  <option value="11">Noviembre</option>
                  <option value="12">Diciembre</option>
                </select>                
              	</span></td>
              <td valign="middle">Periodo Final</td>
              <td valign="middle"><span class="Letras">
                  <select name="anio_final" id="anio_final">
                    <option value="<%=anio3%>"><%=anio3%></option>
					<option value="<%=anio2%>"><%=anio2%></option>
					<option value="<%=anio1%>" selected><%=anio1%></option>
                  </select>
                  <select name="mes_final" id="select2">
                    <option value="1" selected>Enero</option>
                    <option value="2">Febrero</option>
                    <option value="3">Marzo</option>
                    <option value="4">Abril</option>
                    <option value="5">Mayo</option>
                    <option value="6">Junio</option>
                    <option value="7">Julio</option>
                    <option value="8">Agosto</option>
                    <option value="9">Septiembre</option>
                    <option value="10">Octubre</option>
                    <option value="11">Noviembre</option>
                    <option value="12">Diciembre</option>
                  </select>
                </span></td>
            </tr>
            <tr class="fila">
              <td width="25%" align="left" valign="middle" >Incluye Subledger?</td>
              <td colspan="3" align="left" valign="middle" ><select name="incluye_subledger">
                <option value="si">Si</option>
                <option value="no" selected>No</option>
              </select></td>
            </tr>		
        </table></td>
      </tr>
    </table>
<p>
<div align="center">
<p><img src="<%=BASEURL%>/images/botones/buscar.gif" style="cursor:hand" title="Buscar saldos contables" name="buscar"  onClick="Buscar ( cuenta_ini.value, cuenta_fin.value, anio_inicial.value, anio_final.value, mes_inicial.value, mes_final.value );" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);">&nbsp;
   <img src="<%=BASEURL%>/images/botones/cancelar.gif" style="cursor:hand" name="cancelar" title="Resetear" onClick="form2.reset();" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);">&nbsp;
   <img src="<%=BASEURL%>/images/botones/salir.gif"  name="salir" style="cursor:hand" title="Salir al Menu Principal" onMouseOver="botonOver(this);" onClick="parent.close();" onMouseOut="botonOut(this);" ></p>
  <%String msg = request.getParameter("msg");
	if ( msg!=null && !msg.equals("") ){%>
                        
	  <table border="2" align="center">
   		<tr>
        	<td>
            	<table width="100%"  border="1" align="center"  bordercolor="#F7F5F4" bgcolor="#FFFFFF">
                	<tr>
                    	<td width="229" align="center" class="mensajes"><%=msg%></td>
                        <td width="29" background="<%=BASEURL%>/images/cuadronaranja.JPG">&nbsp;</td>
                        <td width="58">&nbsp;</td>
                    </tr>
                </table>
            </td>
		</tr>
	  </table>
	<%}%>
  </p>
</div>
</p>
</form>
</div> 
<%=datos[1]%>
</body>
</html>