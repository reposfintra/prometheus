<!--  
	 - Author(s)       :      LREALES
	 - Description     :      AYUDA DESCRIPTIVA - Reporte de Facturas de Proveedor
	 - Date            :      28/06/2006 
	 - Copyright Notice:      TRANSPORTES SANCHEZ POLO S.A.
-->
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN""http://www.w3.org/TR/html4/loose.dtd">
<%@page session="true"%> 
<%@page errorPage="/error/ErrorPage.jsp"%>
<%@page import="com.tsp.operation.model.beans.*, java.util.*"%>
<%@ include file="/WEB-INF/InitModel.jsp"%>
<%@ taglib uri="http://jakarta.apache.org/taglibs/input-0.90" prefix="input" %>
<html>
<head>
<title>AYUDA DESCRIPTIVA - Reporte de Facturas de Proveedor</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link href="<%= BASEURL %>/css/estilostsp.css" rel="stylesheet" type="text/css">
<script type='text/javascript' src="<%= BASEURL %>/js/boton.js"></script>
</head>

<body>
<br>
<table width="696"  border="2" align="center">
  <tr>
    <td width="811" >
      <table width="100%" border="0" align="center">
        <tr  class="subtitulo">
          <td height="24" colspan="2"><div align="center">Contabilidad</div></td>
        </tr>
        <tr class="subtitulo1">
          <td colspan="2">Reporte de Facturas de Proveedor - Pantalla Preliminar</td>
        </tr>
		<tr class="subtitulo1">
          <td colspan="2">INFORMACION DEL REPORTE </td>
        </tr>
        <tr>
          <td  class="fila">'Fecha Inicial'</td>
          <td  class="ayudaHtmlTexto">Campo para escoger la fecha inicial del reporte. </td>
        </tr>
        <tr>
          <td  class="fila">'Fecha Final' </td>
          <td  class="ayudaHtmlTexto">Campo para escoger la fecha final del reporte. </td>
        </tr>
        <tr>
          <td  class="fila">'Usuario'</td>
          <td  class="ayudaHtmlTexto">Campo para ingresar el login del usuario.</td>
        </tr>
        <tr>
          <td class="fila">Bot&oacute;n Buscar </td>
          <td  class="ayudaHtmlTexto">Bot&oacute;n que realiza la busqueda de todos los usuarios existentes. Gracias a este bot&oacute;n le aparecer&aacute; una lista de los usuarios existentes en la base de datos, para que pueda escojer el usuario que desee.</td>
        </tr>
        <tr>
          <td class="fila">Bot&oacute;n Exportar </td>
          <td  class="ayudaHtmlTexto">Bot&oacute;n para confirmar y realizar la exportaci&oacute;n del reporte a excel. </td>
        </tr>
		<tr>
          <td class="fila">Bot&oacute;n Cancelar </td>
          <td  class="ayudaHtmlTexto">Bot&oacute;n que resetea la pantalla dejandola en su estado inicial de ingreso.</td>
        </tr>
		<tr>
		  <td class="fila">Bot&oacute;n Regresar </td>
		  <td  class="ayudaHtmlTexto">Bot&oacute;n que permite regresar a la vista anterior. </td>
	    </tr>
		<tr>
          <td width="200" class="fila">Bot&oacute;n Salir</td>
          <td width="474"  class="ayudaHtmlTexto">Bot&oacute;n para salir de la vista 'Reporte de Facturas de Proveedor' y volver a la vista del men&uacute;.</td>
        </tr>
      </table>
    </td>
  </tr>
</table>
<p></p>
<center>
<img src = "<%=BASEURL%>/images/botones/salir.gif" style = "cursor:hand" name = "imgsalir" onClick = "parent.close();" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);">
</center>
</body>
</html>
