<!--  
	 - Author(s)       :      LREALES
	 - Description     :      AYUDA FUNCIONAL - Reporte de Facturas de Proveedor
	 - Date            :      28/06/2006 
	 - Copyright Notice:      TRANSPORTES SANCHEZ POLO S.A.
-->
<%@ include file="/WEB-INF/InitModel.jsp"%>
<HTML>
<HEAD>
<TITLE>AYUDA FUNCIONAL - Reporte de Facturas de Proveedor</TITLE>
<META http-equiv=Content-Type content="text/html; charset=windows-1252">
<link href="../css/estilostsp.css" rel="stylesheet" type="text/css"> 
<link href="<%=BASEURL%>/css/estilostsp.css" rel="stylesheet" type="text/css">
<script type='text/javascript' src="<%= BASEURL %>/js/boton.js"></script> 
</HEAD>
<BODY> 
<% String BASEIMG = BASEURL +"/images/ayuda/contabilidad/reporte_facturas_proveedor/"; %>
  <table width="95%"  border="2" align="center">
    <tr>
      <td height="117" >
        <table width="100%" border="0" align="center">
          <tr  class="subtitulo">
            <td height="20"><div align="center">MANUAL DE CONTABILIDAD WEB</div></td>
          </tr>
          <tr class="subtitulo1">
            <td>Descripci&oacute;n del funcionamiento del programa para exportar un Reporte de Facturas de Proveedor.</td>
          </tr>
          <tr>
            <td  class="ayudaHtmlTexto"><div align="center">
              <p>&nbsp;</p>
              </div></td>
          </tr>
          <tr>
            <td  class="ayudaHtmlTexto"><p>En la siguiente pantalla se puede escoger la fecha inicial, la fecha final e ingresar el login del usuario, para realizar la exportacion a excel del reporte deseado.</p>            </td>
          </tr>
          <tr>
            <td  class="ayudaHtmlTexto"><div align="center"><IMG height=215 src="<%=BASEIMG%>image001.JPG" width=915 border=0 v:shapes="_x0000_i1143"></div></td>
          </tr>
          <tr>
            <td  class="ayudaHtmlTexto"><p>&nbsp;</p>
            <p>Si se desconoce el login del usuario, se puede escoger dandole click al Bot&oacute;n 'Buscar' o Lupa y  en una nueva pantalla le aparecer&aacute;n los usuarios, aqui podr&aacute; escoger el usuario que desee para su reporte. </p></td>
          </tr>
          <tr>
            <td  class="ayudaHtmlTexto"><div align="center"><IMG height=206 src="<%=BASEIMG%>image002.JPG" width=561 border=0 v:shapes="_x0000_i1054"></div></td>
          </tr>
          <tr>
            <td  class="ayudaHtmlTexto"><p>&nbsp;</p>
            <p>El sistema verifica que todos los campos obligatorios esten llenos, si no lo estan le saldr&aacute; en la pantalla el siguiente mensaje. </p></td>
          </tr>
          <tr>
            <td  class="ayudaHtmlTexto"><div align="center"><IMG height=125 src="<%=BASEIMG%>image_error001.JPG" width=319 border=0 v:shapes="_x0000_i1143"></div></td>
          </tr>
          <tr>
            <td  class="ayudaHtmlTexto"><p>&nbsp;</p>
            <p>Si el rango de fechas no concuerdan, el sistema le mostrar&aacute; el siguiente mensaje. </p></td>
          </tr>
          <tr>
            <td  class="ayudaHtmlTexto"><div align="center"><IMG height=124 src="<%=BASEIMG%>image_error002.JPG" width=335 border=0 v:shapes="_x0000_i1143"></div></td>
          </tr>
          <tr>
            <td  class="ayudaHtmlTexto"><p class="ayudaHtmlTexto">&nbsp;</p>
            <p class="ayudaHtmlTexto">Si se escoge el Bot&oacute;n 'Exportar' y le sale el siguiente mensaje en la pantalla, es por que no se encontro informaci&oacute;n con los filtros ingresados en la base de datos.</p></td>
          </tr>
<tr>
  <td  class="ayudaHtmlTexto"><div align="center"><IMG height=70 src="<%=BASEIMG%>image_error003.JPG" width=401 border=0 v:shapes="_x0000_i1054"></div></td>
</tr>
<tr>
  <td  class="ayudaHtmlTexto"><p>&nbsp;</p>
    <p>Cuando alla echo el procedimiento de exportaci&oacute;n correctamente, en la pantalla el sistema nos mostrar&aacute; lo siguiente. </p></td>
</tr>
<tr>
            <td  class="ayudaHtmlTexto"><div align="center"><IMG height=70 src="<%=BASEIMG%>image003.JPG" width=391 border=0 v:shapes="_x0000_i1054"></div></td>
          </tr>
		  <tr>
  <td  class="ayudaHtmlTexto"><p>&nbsp;</p>
    <p>Luego si desea observar, el archivo excel generado.. debe ir a la pantalla inicial del sitio, y escoger en la parte superior derecha, el icono que se se&ntilde;ala acontinuaci&oacute;n. </p></td>
</tr>
<tr>
            <td  class="ayudaHtmlTexto"><div align="center"><IMG height=555 src="<%=BASEIMG%>image010.JPG" width=811 border=0 v:shapes="_x0000_i1054"></div></td>
          </tr>
		  <tr>
  <td  class="ayudaHtmlTexto"><p>&nbsp;</p>
    <p>Despues en la pantalla le debe aparecer una lista con los archivos excel que usted posee, alli podra buscar y escoger el anterior reporte generado. </p></td>
</tr>
<tr>
            <td  class="ayudaHtmlTexto"><div align="center"><IMG height=467 src="<%=BASEIMG%>image020.JPG" width=699 border=0 v:shapes="_x0000_i1054"></div></td>
          </tr>
		  <tr>
  <td  class="ayudaHtmlTexto"><p>&nbsp;</p>
    <p>Luego de haber escogido el reporte, ya podra observarlo como se muestra acontinuaci&oacute;n.</p></td>
</tr>
<tr>
            <td  class="ayudaHtmlTexto"><div align="center"><IMG height=242 src="<%=BASEIMG%>image030.JPG" width=900 border=0 v:shapes="_x0000_i1054"></div></td>
          </tr>
      </table></td>
    </tr>
  </table>
  <p></p>
<center>
<img src = "<%=BASEURL%>/images/botones/salir.gif" style = "cursor:hand" name = "imgsalir" onClick = "parent.close();" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);">
</center>
</BODY>
</HTML>
