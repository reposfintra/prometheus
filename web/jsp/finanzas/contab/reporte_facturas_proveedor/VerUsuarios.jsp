<%@page contentType="text/html"%>
<%@page session="true"%>
<%@page errorPage="../error/ErrorPage.jsp"%>
<%@page import="java.util.*"%>
<%@page import="com.tsp.operation.model.*"%>
<%@page import="com.tsp.util.*"%>
<%@page import="com.tsp.operation.model.beans.*, java.util.*"%>
<%@include file="/WEB-INF/InitModel.jsp"%>
<%@taglib uri="http://jsptags.com/tags/navigation/pager" prefix="pg" %>

<html>
<head>
<title>Ver Usuarios</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link href="<%= BASEURL %>/css/estilostsp.css" rel="stylesheet" type="text/css"> 
<link href="../../../css/estilostsp.css" rel="stylesheet" type="text/css"> 
<script type='text/javascript' src="<%= BASEURL %>/js/reporte.js"></script> 
<script type='text/javascript' src="<%= BASEURL %>/js/boton.js"></script>
</head>
<body onresize="redimensionar()" onload = 'redimensionar()'>
<div id="capaSuperior" style="position:absolute; width:100%; height:100px; z-index:0; left: 0px; top: 0px;">
	<jsp:include page="/toptsp.jsp?encabezado=Reporte de Facturas de Proveedor"/>
</div>
<div id="capaCentral" style="position:absolute; width:100%; height:87%; z-index:0; left: 0px; top: 100px; overflow: scroll;">
	  <% LinkedList info = model.tablaGenService.getUsuarios();
	  
		  String index =  "center";
   	      int maxPageItems = 10;
          int maxIndexPages = 10;
		  
	  if( info != null  && info.size() > 0 ){ %>
 <table width="60%" border="2" align=center>
    <tr>
        <td>
            <table width="100%" align="center" class="tablaInferior"> 
                <tr>
                    <td colspan='2'>                                                
                        <table width="100%"  border="0" cellpadding="0" cellspacing="0" class="barratitulo">
                            <tr>
                                <td width="50%" class="subtitulo1" colspan='3'>Usuarios</td>
                                <td width="50%" class="barratitulo" colspan='2'><img src="<%=BASEURL%>/images/titulo.gif"></td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td>
					<div class="scroll" style=" overflow:auto ; WIDTH: 100%; HEIGHT:100%; " >
                        <table width="100%" border="1" align="center" bordercolor="#999999" bgcolor="#F7F5F4">
                            <tr class="tblTitulo">
                                <td nowrap width="30%"><div align="center">Login</div></td>
                                <td nowrap width="70%"><div align="center">Usuario</div></td>
                            </tr>
							<pg:pager
							items="<%=info.size()%>"
							index="<%= index %>"
							maxPageItems="<%= maxPageItems %>"
							maxIndexPages="<%= maxIndexPages %>"
							isOffset="<%= true %>"
							export="offset,currentPageNumber=pageNumber"
							scope="request">
							  <%
							  for (int i = offset.intValue(), l = Math.min(i + maxPageItems, info.size()); i < l; i++){
								//for( int i = 0; i < info.size(); i++ ){
									TablaGen inf = ( TablaGen ) info.get( i );
							  %>
							  <pg:item>
							<tr class="<%=(i % 2 == 0 )?"filagris":"filaazul"%>" onMouseOver='cambiarColorMouse(this)' style="cursor:hand" title="Escoger Usuario..." onClick="window.opener.document.form2.usuario_fac.value='<%=inf.getTable_code ()%>'; window.close();">
							  <td  nowrap  align="left" abbr="" class="bordereporte"><%=inf.getTable_code ()%></td>
							  <td  align="left" abbr="" nowrap class="bordereporte"><%=inf.getDescripcion ()%></td>
							</tr>
						  </pg:item>
								<%}%>								
								<tr class="pie">
								<td td height="20" colspan="27" nowrap align="center">          
									<pg:index>
										<jsp:include page="/WEB-INF/jsp/googlesinimg.jsp" flush="true"/>
									</pg:index>
								</td>
							</tr>
							</pg:pager> 
                        </table>
					  </div>
                    </td>
                </tr>
            </table>
        </td>
    </tr>
 </table>
 <br>
	<%}
	 else { %>                    
	  <%out.print("<table width=379 border=2 align=center><tr><td><table width=100%  border=1 align=center  bordercolor=#F7F5F4 bgcolor=#FFFFFF><tr><td width=350 align=center class=mensajes>No Existen Usuarios!</td><td width=100><img src="+BASEURL+"/images/cuadronaranja.JPG></td></tr></table></td></tr></table>");%>
	  <br>
	<%}%>

 <table width='95%' align=center>
  <tr class="titulo">
    <td align=right colspan='2'>
        <div align="center">
		<img src="<%=BASEURL%>/images/botones/salir.gif"  name="imgsalir"  onMouseOver="botonOver(this);" alt="Salir" onClick="parent.close();" onMouseOut="botonOut(this);" style="cursor:hand">
        </div></td>
  </tr>
</table>
</div>
</body>
</html>