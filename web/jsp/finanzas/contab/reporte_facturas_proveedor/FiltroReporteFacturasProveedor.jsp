<!--
- Nombre P�gina :                  FiltroReporteFacturasProveedor.jsp                 
- Descripci�n :                    Pagina JSP, que realiza el filtro de busqueda para el reporte
                                   de Facturas de Proveedor.             
- Autor :                          LREALES                        
- Fecha Creado :                   27 de Junio de 2006, 10:29 A.M.                        
- Versi�n :                        1.0                                       
- Copyright :                      Fintravalores S.A.                   
-->

<%-- Declaracion de librerias--%>
<%@page contentType="text/html"%>
<%@ page session="true"%>
<%//@ page errorPage="/error/error.jsp"%>
<%@page import="com.tsp.operation.model.beans.*, java.util.*"%>
<%@ include file="/WEB-INF/InitModel.jsp"%>
<%@page import="com.tsp.operation.model.*"%>
<%@page import="com.tsp.util.*"%>
<%@ taglib uri="http://jakarta.apache.org/taglibs/input-0.90" prefix="input" %>
<%
  String path = application.getRealPath(model.menuService.getRealUrl(request.getRequestURI()));
  String datos[] = model.menuService.getContenidoMenu(BASEURL,request.getRequestURI(),path);
%>
<html>
<head><title>Reporte de Facturas de Proveedor</title>
<link href="<%= BASEURL %>/css/estilostsp.css" rel="stylesheet" type="text/css">
<script src='<%=BASEURL%>/js/date-picker.js'></script>
<script type='text/javascript' src="<%= BASEURL %>/js/boton.js"></script>
<script type='text/javascript' src="<%= BASEURL %>/js/validar.js"></script>
</head>
<body onLoad="redimensionar();" onResize="redimensionar();">
<div id="capaSuperior" style="position:absolute; width:100%; height:100px; z-index:0; left: 0px; top: 0px;">
<jsp:include page="/WEB-INF/toptsp.jsp?encabezado=Contabilidad"/>
</div>

<div id="capaCentral" style="position:absolute; width:100%; height:85%; z-index:0; left: 0px; top: 100px; overflow: scroll;"> 
<%
    Usuario usuario = (Usuario) session.getAttribute("Usuario");  
	
	//String usu_fac = ( request.getParameter("usuario_factura") != null )?request.getParameter("usuario_factura"):"";	
%>
<script>
	var controlador ="<%=CONTROLLERCONTAB%>";
	
	function Exportar ( fecha_ini, fecha_fin, usuario_fac ){
		
		if( ( fecha_ini != "" ) && ( fecha_fin != "" ) && ( usuario_fac != "" ) ){
			
			var anio_ini = fecha_ini.substring(0,4);
			var anio_fin = fecha_fin.substring(0,4);
			var mes_ini = fecha_ini.substring(5,7);
			var mes_fin = fecha_fin.substring(5,7);
			var dia_ini = fecha_ini.substring(8,10);
			var dia_fin = fecha_fin.substring(8,10);
			
			if ( ( anio_ini < anio_fin ) || ( ( anio_ini == anio_fin ) && ( mes_ini < mes_fin ) ) || ( ( anio_ini == anio_fin ) && ( mes_ini == mes_fin ) && ( dia_ini <= dia_fin ) ) ){
			
				document.form2.action = controlador+"?estado=ReporteFacturasProveedor&accion=Excel";
				document.form2.submit();
			
			} else{ 
			
				alert( "La fecha inicial NO puede ser mayor que la fecha final!" );
				
			}			
						
		} else{
		
			alert( "Por Favor No Deje Los Campos Obligatorios Vacios!" );
			
		}
	}
</script>

<form name="form2" method="post">
    <table width="60%" border="2" align="center">
      <tr>
        <td><table width="100%" align="center"  class="tablaInferior">
            <tr>
				<td colspan="2" class="subtitulo1">Reporte de Facturas de Proveedor</td>
				<td colspan="2" class="barratitulo"><img src="<%=BASEURL%>/images/titulo.gif" width="32" height="20" align="left"><%=datos[0]%></td>
            </tr>
            <tr class="fila">
              <td align="left" valign="middle" >Fecha Inicial </td>
              <td width="30%" align="left" valign="middle" ><span class="Letras">
                <input name="fecha_ini" type="text" class="textbox" id="fecha_ini" size="10" readonly>
              <img src="<%=BASEURL%>/images/botones/iconos/obligatorio.gif" name="obligatorio" width="10" height="10">			  
			  <img src="<%=BASEURL%>/images/cal.gif" width="16" height="16" align="absmiddle" style="cursor:hand " onclick="if(self.gfPop)gfPop.fPopCalendar(document.form2.fecha_ini);return false;" HIDEFOCUS></span></td>
              <td width="20%" valign="middle">Fecha Final </td>
              <td width="30%" valign="middle"><span class="Letras">
                <input name="fecha_fin" type="text" class="textbox" id="fecha_fin" size="10" readonly>
				<a href="javascript:void(0)" onclick="jscript: show_calendar('fecha_fin');" HIDEFOCUS> </a>
                <img src="<%=BASEURL%>/images/botones/iconos/obligatorio.gif" name="obligatorio" width="10" height="10">			    
				<img src="<%=BASEURL%>/images/cal.gif" width="16" height="16" align="absmiddle" style="cursor:hand " onclick="if(self.gfPop)gfPop.fPopCalendar(document.form2.fecha_fin);return false;" HIDEFOCUS></span></td>
            </tr>
            <tr class="fila">
              <td width="20%" align="left" valign="middle" >Usuario</td>
              <td colspan="3" align="left" valign="middle" >
			    <input name="usuario_fac" type="text" class="textbox" id="usuario_fac" onKeyPress="soloAlfaSinEspacios(event)" size="30">              
			    <img src="<%=BASEURL%>/images/botones/iconos/obligatorio.gif" width="10" height="10"> 
				<img src="<%=BASEURL%>/images/botones/iconos/lupa.gif" alt="Buscar Usuario.." name="imglupa" width="20" height="20" style="cursor:hand" onClick="window.open('<%=CONTROLLERCONTAB%>?estado=Ver&accion=Usuarios','','status=no,scrollbars=no,width=500,height=500,resizable=yes');" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);">
			    
			  </td>
            </tr>		
        </table></td>
      </tr>
    </table>
<p>
<div align="center">
<p><img src="<%=BASEURL%>/images/botones/exportarExcel.gif" style="cursor:hand" title="Exportar Reporte a Excel" name="exportar" onClick="Exportar ( fecha_ini.value, fecha_fin.value, usuario_fac.value );" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);">&nbsp;
   <img src="<%=BASEURL%>/images/botones/cancelar.gif" name="cancelar" title="Resetear" onClick="form2.reset();" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);">&nbsp;
   <img src="<%=BASEURL%>/images/botones/salir.gif"  name="salir" style="cursor:hand" title="Salir al Menu Principal" onMouseOver="botonOver(this);" onClick="parent.close();" onMouseOut="botonOut(this);" ></p>
  <%String msg = request.getParameter("msg");
	if ( msg!=null && !msg.equals("") ){%>
                        
	  <table border="2" align="center">
   		<tr>
        	<td>
            	<table width="100%"  border="1" align="center"  bordercolor="#F7F5F4" bgcolor="#FFFFFF">
                	<tr>
                    	<td width="229" align="center" class="mensajes"><%=msg%></td>
                        <td width="29" background="<%=BASEURL%>/images/cuadronaranja.JPG">&nbsp;</td>
                        <td width="58">&nbsp;</td>
                    </tr>
                </table>
            </td>
		</tr>
	  </table>
	<%}%>
  </p>
</div>
</p>
</form>
</div>
	<iframe width=188 height=166 name="gToday:normal:agenda.js:gfPop:plugins.js" id="gToday:normal:agenda.js:gfPop:plugins.js" src="<%=BASEURL%>/js/Calendario/ipopeng.htm" scrolling="no" frameborder="0" style="visibility:visible; z-index:999; position:absolute; left:-500px; top:0px;"> </iframe>  
<%=datos[1]%>
</body>
</html>