<%@ page session="true"%>

<%@page import="com.tsp.finanzas.contab.model.beans.*, java.util.*"%>
<%@ include file="/WEB-INF/InitModel.jsp"%>
<%@ taglib uri="http://jsptags.com/tags/navigation/pager" prefix="pg" %>
<html> 
<head>
<link href="../../../../css/estilostsp.css" rel="stylesheet" type="text/css">
<link href="<%=BASEURL%>/css/estilostsp.css" rel="stylesheet" type="text/css">

<title>Buscar Id Subledger</title>
<script language="javascript" src="<%=BASEURL%>/js/validar.js"></script>
<script language="javascript" src="<%=BASEURL%>/js/boton.js"></script>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
</head>
<body onLoad="form1.valor.focus();">
<%String idcampo = request.getParameter("idcampo");
  String reset = request.getParameter("reset") != null ? request.getParameter("reset")  :"";%>
<script>
function asignarnombre(id, doc){
	opener.document.getElementById("auxiliar"+id).value = doc;
    window.self.close();
}
</script>

<form name="form1" method="post" action="<%=CONTROLLERCONTAB%>?estado=Subledger&accion=Buscar&carpeta=jsp/finanzas/contab/subledger&pagina=consultasIdSubledger.jsp" >
<table border="2" align="center" width="450">
  <tr>
    <td>
	<table width="100%"  align="center"   >
  <tr>
    <td width="208" height="30"  class="subtitulo1">Busque el Subledger por Identificacion o por nombre</td>
    <td width="218"  class="barratitulo"><img src="<%=BASEURL%>/images/titulo.gif" width="32" height="20">
      <input type="hidden" name="cuenta" value="<%=request.getParameter("cuenta")%>">
      <input type="hidden" name="tipo" value="<%=request.getParameter("tipo")%>"> 
      <input type="hidden" name="evento" value="BuscarId">
      <input type="hidden" name="idcampo" value="<%=idcampo%>"></td>
  </tr>
</table>
  <table width="100%" align="center" class="Letras">
    <tr >
      <td nowrap><table width="100%" border="0" cellpadding="0" cellspacing="0" class="fila">
        <tr>
          <td height="19">Escriba el codigo o nombre</td>
          <td><input name="valor" type="text" class="textbox" id="nit" size="35" maxlength="30"></td>
        </tr>
      </table>        </td>
    </tr>
  </table>
  </td>
  </tr>
  </table>
  <div align="center"><br>
    <img src="<%=BASEURL%>/images/botones/buscar.gif"  name="imgsalir"  height="21" onMouseOver="botonOver(this);" onClick="form1.submit();" onMouseOut="botonOut(this);" style="cursor:hand">&nbsp; <img src="<%=BASEURL%>/images/botones/salir.gif"  name="imgsalir"  height="21" onMouseOver="botonOver(this);" onClick="parent.close();" onMouseOut="botonOut(this);" style="cursor:hand">
  </div>
</form>
<%LinkedList lista = modelcontab.subledgerService.getListasubledger();
    Subledger sub;
if ( lista == null || reset.equals("ok") ){
   lista = new LinkedList();
}
if ( lista.size() > 0 ){ %>
<table width="450" border="2" align="center">
    <tr>
      <td >
	  <table width="100%" align="center">
              <tr>
                <td width="373" class="subtitulo1">&nbsp;Datos Subledger </td>
                <td width="427" class="barratitulo"><img src="<%=BASEURL%>/images/titulo.gif" width="32" height="20"></td>
              </tr>
        </table>
		  <table width="99%" border="1" align="center" bordercolor="#999999" bgcolor="#F7F5F4">
          <tr class="tblTitulo">
          <td width="29%" align="center">Identificaci&oacute;n</td>
          <td width="71%"  align="center">Nombre</td>
        </tr>
       <%for (int i = 0;   i < lista.size() ; i++)	  {
          sub = (Subledger) lista.get(i);%>
        <tr class="<%=(i % 2 == 0 )?"filagris":"filaazul"%>" onMouseOver='cambiarColorMouse(this)'   style="cursor:hand"
        onClick="asignarnombre('<%=idcampo%>','<%=sub.getId_subledger()%>');">
          <td width="29%" class="bordereporte"><%=sub.getId_subledger()%></td>
          <td width="71%" class="bordereporte"><%=sub.getNombre()%></td>
        </tr>
        <%}%>
      </table></td>
    </tr>
</table>
<%}else{
    if(!reset.equals("ok")){%>
<table border="2" align="center">
    <tr>
      <td><table width="410" height="40" border="1" align="center"  bordercolor="#F7F5F4" bgcolor="#FFFFFF">
          <tr>
            <td width="282" align="center" class="mensajes">Su b&uacute;squeda no arroj&oacute; resultados!</td>
            <td width="28" background="<%=BASEURL%>/images/cuadronaranja.JPG">&nbsp;</td>
            <td width="78">&nbsp;</td>
          </tr>
      </table></td>
    </tr>
  </table>
<%}
}%>

<iframe width=188 height=166 name="gToday:normal:agenda.js:gfPop:plugins.js" id="gToday:normal:agenda.js:gfPop:plugins.js" src="js/Calendario/ipopeng.htm" scrolling="no" frameborder="0" style="visibility:visible; z-index:999; position:absolute; left:-500px; top:0px;"> </iframe>

</body>
</html>

