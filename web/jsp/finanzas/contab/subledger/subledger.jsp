<!--
- Descripci�n :                    Pagina JSP, que maneja el ingreso de las cuentas tipo subledger                  
- Autor :                          Ing. Diogenes Bastidas                       
- Fecha Creado :                   5 de Junio de 2005                
- Versi�n :                        1.0                                       
- Copyright :                      Fintravalores S.A.                   
-->

<%-- Declaracion de librerias--%>
<%@page contentType="text/html"%>
<%@ page session="true"%>
<%@page import="com.tsp.operation.model.beans.*, java.util.*"%>
<%@page import="com.tsp.finanzas.contab.model.beans.*"%>
<%@ include file="/WEB-INF/InitModel.jsp"%>
<%@page import="com.tsp.operation.model.*"%>
<%@page import="com.tsp.finanzas.contab.model.*"%>
<%@page import="com.tsp.util.*"%>
<%@ taglib uri="http://jakarta.apache.org/taglibs/input-0.90" prefix="input" %>
<html>
<head>
<title>Insertar Subledger</title>
<link href="<%= BASEURL %>/css/estilostsp.css" rel="stylesheet" type="text/css">
<link href="../../../../css/estilostsp.css" rel="stylesheet" type="text/css">
<script type='text/javascript' src="<%= BASEURL %>/js/boton.js"></script>
</head>
<script type='text/javascript' src="<%= BASEURL %>/js/general.js"></script>
<script type='text/javascript' src="<%= BASEURL %>/js/subledger.js"></script>

<body onLoad="<%=(request.getParameter("modificado")!=null)?"parent.opener.location.reload();redimensionar();window.close();":"redimensionar();"%>" onResize="redimensionar();">
<% String path = application.getRealPath(model.menuService.getRealUrl(request.getRequestURI()));
   String datos[] = model.menuService.getContenidoMenu(BASEURL,request.getRequestURI(),path);
   String sw = (request.getParameter("sw")!=null) ? request.getParameter("sw") :"";
   String cuenta="",tipo="",id="",nombre="",accion="",nomtipo="";
   String mensaje = request.getParameter("mensaje"); 	
   TreeMap subledger = modelcontab.subledgerService.getCuentasTipoSubledger();
   if (subledger==null)
      subledger = new TreeMap();
   subledger.put("Seleccione un item","");
   if(!sw.equals("")){
       accion="Modificar";
      Subledger sub = modelcontab.subledgerService.getSubledger();
      cuenta = sub.getCuenta();
      tipo = sub.getTipo_subledger();
      nomtipo = sub.getDes_tipo();
	  id = sub.getId_subledger();
      nombre = sub.getNombre();
   }else{
      accion="Registrar";
	  cuenta = (request.getParameter("cuenta")!=null)?request.getParameter("cuenta"):"";
      tipo = (request.getParameter("tipo")!=null)?request.getParameter("tipo"):"";
	  id = (request.getParameter("id")!=null)?request.getParameter("id"):"";
      nombre = (request.getParameter("nombre")!=null)?request.getParameter("nombre"):"";
   }
%>
<% String pagina = "/toptsp.jsp?encabezado="+accion+" Subledger"; %>
<div id="capaSuperior" style="position:absolute; width:100%; height:100px; z-index:0; left: 0px; top: 0px;">
 <jsp:include page="<%=pagina%>"/>
</div>

<div id="capaCentral" style="position:absolute; width:100%; height:85%; z-index:0; left: 0px; top: 100px; overflow: scroll;"> 
<FORM name='forma' id='forma' method='POST' action='<%=CONTROLLERCONTAB%>?estado=Subledger&accion=<%=accion%>&carpeta=jsp/finanzas/contab/subledger&pagina=subledger.jsp'>
  <table width="400" border="2" align="center">
    <tr>
      <td>
        <table width="100%"  border="0">
          <tr>
            <td width="51%" class="subtitulo1">Informaci&oacute;n</td>
            <td width="49%" class="barratitulo"><img src="<%=BASEURL%>/images/titulo.gif" width="32" height="20" align="left"><%=datos[0]%></td>
          </tr>
        </table>
        <table width="100%" class="tablaInferior">
          <tr>
            <td width="36%" align="left" class="fila">Tipo</td>
            <td width="64%" valign="middle" class="letra"><%if(sw.equals("")){%>
              <select name="tipo" class="textbox" id="tipo" style="width:90% ">
                <option value="">Seleccione Un Item</option>
                <% LinkedList tbltipo = model.tablaGenService.obtenerTablas();
               for(int i = 0; i<tbltipo.size(); i++){
                       TablaGen tbgen = (TablaGen) tbltipo.get(i); %>
                <option value="<%=tbgen.getTable_code()%>" <%=(tbgen.getTable_code().equals(tipo))?"selected":""%> ><%=tbgen.getDescripcion()%></option>
                <%}%>
              </select><%}else{%><input name="tipo" type="hidden" id="tipo" value="<%=tipo%>"><%=nomtipo%><%}%></td>
          </tr>
          <tr>
            <td align="left" class="fila" >Identificaci&oacute;n</td>
            <td valign="middle" class="letra"><input name="id" type="<%=(!sw.equals(""))?"hidden":"text"%>" id="nit" size="15" maxlength="15" onKeyPress="soloAlfa(event)" value="<%=id%>"><%=(!sw.equals(""))?id:""%></td>
          </tr>
          <tr class="fila">
            <td align="left" >Nombre</td>
            <td valign="middle"><input name="nombre" type="text" id="nombre" size="30" maxlength="30" value="<%=nombre%>"></td>
          </tr>
      </table></td>
    </tr>
  </table>
<p>
<div align="center"><img src="<%=BASEURL%>/images/botones/<%=(sw.equals(""))?"aceptar.gif":"modificar.gif"%>" style="cursor:hand" name="agregar"  onclick="return TCamposLlenos(forma);" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);">&nbsp;
<%if(!sw.equals("")){%><img src="<%=BASEURL%>/images/botones/anular.gif" style="cursor:hand" name="anular"  onMouseOver="botonOver(this);" onClick="window.location='<%=CONTROLLERCONTAB%>?estado=Subledger&accion=Anular&cuenta=<%=cuenta%>&tipo=<%=tipo%>&id=<%=id%>'" onMouseOut="botonOut(this);" >&nbsp;
<%}else{%><img src="<%=BASEURL%>/images/botones/cancelar.gif" style="cursor:hand" name="cancelar"  onMouseOver="botonOver(this);" onClick="window.location='<%=CONTROLLER%>?estado=Menu&accion=Cargar&pagina=subledger.jsp&carpeta=/jsp/finanzas/contab/subledger&marco=no&opcion=cargar'" onMouseOut="botonOut(this);" >&nbsp;<%}%>
<img src="<%=BASEURL%>/images/botones/salir.gif" name="salir" style="cursor:hand"  onClick="parent.close();" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);"></div>
</p>
 
    <%  if(mensaje!=null){%>
            <br>
            <table border="2" align="center">
              <tr>
                <td><table width="100%"  border="1" align="center"  bordercolor="#F7F5F4" bgcolor="#FFFFFF">
                    <tr>
                      <td width="229" align="center" class="mensajes"><%=mensaje%></td>
                      <td width="29" background="<%=BASEURL%>/images/cuadronaranja.JPG">&nbsp;</td>
                      <td width="58">&nbsp;</td>
                    </tr>
                </table></td>
              </tr>
            </table>
    <%  }%>	
</FORM>
</div>
<%=datos[1]%>
</body>
</html>
