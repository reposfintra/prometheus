<!--
- Descripci�n :                    Pagina JSP, que maneja el ingreso de las cuentas tipo subledger                  
- Autor :                          Ing. Diogenes Bastidas                       
- Fecha Creado :                   5 de Junio de 2005                
- Versi�n :                        1.0                                       
- Copyright :                      Fintravalores S.A.                   
-->

<%-- Declaracion de librerias--%>
<%@page contentType="text/html"%>
<%@ page session="true"%>
<%@page import="com.tsp.operation.model.beans.*, java.util.*"%>
<%@ include file="/WEB-INF/InitModel.jsp"%>
<%@page import="com.tsp.operation.model.*"%>
<%@page import="com.tsp.util.*"%>
<%@ taglib uri="http://jakarta.apache.org/taglibs/input-0.90" prefix="input" %>
<html>
<head>
<title>Buscar Subledger</title>
<link href="<%= BASEURL %>/css/estilostsp.css" rel="stylesheet" type="text/css">
<link href="../../../../css/estilostsp.css" rel="stylesheet" type="text/css">
<script type='text/javascript' src="<%= BASEURL %>/js/boton.js"></script>
</head>
<script type='text/javascript' src="<%= BASEURL %>/js/validar.js"></script>
<script type='text/javascript' src="<%= BASEURL %>/js/subledger.js"></script>
<body onLoad="redimensionar();" onResize="redimensionar();">
<div id="capaSuperior" style="position:absolute; width:100%; height:100px; z-index:0; left: 0px; top: 0px;">
<jsp:include page="/WEB-INF/toptsp.jsp?encabezado=Buscar Subledger"/>
</div>
<% String path = application.getRealPath(model.menuService.getRealUrl(request.getRequestURI()));
   String datos[] = model.menuService.getContenidoMenu(BASEURL,request.getRequestURI(),path);
   TreeMap subledger = modelcontab.subledgerService.getCuentasTipoSubledger();
   if (subledger==null)
      subledger = new TreeMap();
   subledger.put("Seleccione un item","");
   %>
<div id="capaCentral" style="position:absolute; width:100%; height:85%; z-index:0; left: 0px; top: 100px; overflow: scroll;"> 
<FORM name='forma' id='forma' method='POST' action='<%=CONTROLLERCONTAB%>?estado=Subledger&accion=Buscar&carpeta=jsp/finanzas/contab/subledger&pagina=verSubledger.jsp'>
  <table width="400" border="2" align="center">
    <tr>
      <td>
        <table width="100%"  border="0">
          <tr>
            <td width="51%" class="subtitulo1">Buscar</td>
            <td width="49%" class="barratitulo"><img src="<%=BASEURL%>/images/titulo.gif" width="32" height="20" align="left"><%=datos[0]%></td>
          </tr>
        </table>
        <table width="100%" class="tablaInferior">
          <tr class="fila">
            <td width="36%" align="left" >Tipo Subledger </td>
            <td width="64%" valign="middle"><select name="tipo" class="textbox" id="tipo" style="width:90% ">
                  <option value=""></option>
               <% LinkedList tblgen = model.tablaGenService.obtenerTablas();
               for(int i = 0; i<tblgen.size(); i++){
                       TablaGen obj = (TablaGen) tblgen.get(i); %>
                       <option value="<%=obj.getTable_code()%>"  ><%=obj.getDescripcion()%></option>
             <%}%>
     </select></td>
          </tr>
          <tr class="fila">
            <td align="left" >Identificacion</td>
            <td valign="middle"><input name="id" type="text" id="id" size="25" maxlength="25">
              <input name="evento" type="hidden" id="evento" value="Listar">
              <input type="hidden" name="cuenta" value=""></td>
          </tr>
      </table></td>
    </tr>
  </table>
<p>
<div align="center"><img src="<%=BASEURL%>/images/botones/buscar.gif" style="cursor:hand"  name="agregar"  onclick="forma.submit()" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);">&nbsp;
<img src="<%=BASEURL%>/images/botones/detalles.gif" style="cursor:hand" name="cancelar"  onMouseOver="botonOver(this);" onClick="window.location='<%=CONTROLLERCONTAB%>?estado=Subledger&accion=Buscar&evento=Listar&carpeta=jsp/finanzas/contab/subledger&pagina=verSubledger.jsp&cuenta=&tipo=&id=&evento=Listar'" onMouseOut="botonOut(this);" >&nbsp;
<img src="<%=BASEURL%>/images/botones/salir.gif" name="salir" style="cursor:hand" title="Salir al Menu Principal" onClick="parent.close();" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);"></div>
</p>
</FORM>
</div>
<%=datos[1]%>
</body>
</html>
