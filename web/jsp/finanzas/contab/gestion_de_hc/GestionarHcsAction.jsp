<?xml version="1.0" ?>
<%@ page contentType="text/xml;" %>

<%--
    Document   : GestionarHcsAction
    Created on : 7/01/2011, 03:26:29 PM
    Author     : jpinedo
--%>
<%response.setHeader("Content-Type", "text/xml; utf-8");
response.setHeader("Pragma", "no-cache"); response.setHeader("Expires","0"); response.setHeader("Cache-Control", "no-cache");
response.setHeader("Cache-Control", "no-store"); response.setHeader("Cache-Control", "must-revalidate"); %>


<%@page session    ="true"%>
<%@page errorPage  ="/error/ErrorPage.jsp"%>
<%@include file    ="/WEB-INF/InitModel.jsp"%>
<%@page import    ="java.util.*" %>
<%@page import    ="com.tsp.util.*" %>
<%@page import    ="com.tsp.operation.model.beans.Usuario" %>
<%@page import    ="com.tsp.finanzas.contab.model.beans.Hc" %>
<%@page import    ="com.tsp.finanzas.contab.model.DAO.GestionarHcsDAO" %>
<%@page import    ="com.tsp.finanzas.contab.model.services.GestionarHcsService"%>
<%@page import    ="com.tsp.finanzas.contab.model.beans.Tipo_Docto"%>
<%@page import    ="com.tsp.finanzas.contab.model.DAO.Tipo_DoctoDAO"%>
<%@page import    ="com.tsp.finanzas.contab.model.services.Tipo_DoctoService"%>
<%@page import    ="com.tsp.finanzas.contab.model.beans.Cmc"%>
<%@page import    ="com.tsp.finanzas.contab.model.DAO.CmcDAO"%>
<%@page import    ="com.tsp.finanzas.contab.model.services.CuentasService"%>
<%@page import    ="java.util.List" %>
<%@page import="com.tsp.operation.model.services.*"%>






<%
CmcDAO CmcDAO=new CmcDAO();
Usuario usuario = (Usuario)session.getAttribute("Usuario");
List<Tipo_Docto> lista_tipo_doc = new LinkedList<Tipo_Docto>();
List<Cmc> lista_cmc =CmcDAO.lista_cmc_sesion_notnul(session);

  ClientesVerService clvsrv = new ClientesVerService(usuario.getBd());
  String perfil= clvsrv.getPerfil(usuario.getLogin());//20100621
  boolean sw_permiso= (Boolean)session.getAttribute("permiso");
  boolean nuevo=(Boolean)session.getAttribute("nuevo");

if(nuevo)
{
    sw_permiso=true;
}

if ((List)session.getAttribute("lista_tipo_doc") != null) {
            lista_tipo_doc= (List<Tipo_Docto>)session.getAttribute("lista_tipo_doc");
}


int opcion= Integer.parseInt(request.getParameter("opcion"));


String tipo_doc = request.getParameter("td");
String cuenta=request.getParameter("cuenta");
String tc=request.getParameter("tc");
String dc=request.getParameter("dc");


int  lz=0;
//String temp_size=(String)session.getAttribute("lz");
if(session.getAttribute("lz")!=null )
{
lz=(Integer)session.getAttribute("lz");
}
else
{
session.removeAttribute("lz");
}



%>

<%
switch (opcion)
{

case 1:/*Agregar documento  a una lista temporal */





lista_cmc=CmcDAO.llenar_lista(tipo_doc, "", cuenta, dc, tc,usuario.getLogin(), lista_cmc);
session.setAttribute("lista_cmc",lista_cmc);
%>
<table width="680" border="0">
<tr>
    <td width="55"  height="24" align="center" valign="middle" class="fila"><label onclick="Agrega_Documento('<%=BASEURL%>/jsp/finanzas/contab/gestion_de_hc/GestionarHcsAction.jsp',1);" > <img src='<%=BASEURL%>/images/botones/iconos/mas.gif'   alt="Agregar"   width='12' height='12' align="middle" style="cursor: pointer;" title="Agregar Documento"> </label>    </td>
    <td width="248" align="center" valign="middle" class="fila">
	<select id="tipo_doc_<%=0%>" name="tipo_doc_<%=0%>" onchange="Valida_Cmc(<%=0%>);" onfocus="temp(0);"  >
      <option value="0" selected="selected">Seleccione...</option>
      <%for (int i=0;i<lista_tipo_doc.size();i++){%>
      <option value="<%=lista_tipo_doc.get(i).getCodigo() %>"><%=lista_tipo_doc.get(i).getCodigo() %>&nbsp; - &nbsp; <%=lista_tipo_doc.get(i).getDescripcion() %> </option>
      <%}%>
    </select>    </td>
    <td width="144" align="center" valign="middle" class="fila"><input name="cuenta" type="text" id="cuenta_0" style="width:90px"

									 		onchange="Valida_Cuenta('<%=BASEURL%>/jsp/finanzas/contab/gestion_de_hc/GestionarHcsAction.jsp',0);"  maxlength="12" /></td>
    <td width="89" align="center" valign="middle" class="fila"><select name="dc_<%=0%>" id="dc_<%=0%>" size="1">
      <option value="C">C</option>
      <option value="D">D</option>
    </select></td>
    <td width="122" align="center" valign="middle" class="fila"><select name="tc_<%=0%>"  id="tc_<%=0%>"size="1">
      <option value="C">C</option>
      <option value="G">G</option>
    </select></td>
  </tr>


  <%for(int j=0;j<lista_cmc.size();j++){%>
  <tr>
    <td  height="24" align="center" valign="middle" class="fila">

    <% if(sw_permiso) { %>
	<label onclick="Elimina_Documento('<%=BASEURL%>/jsp/finanzas/contab/gestion_de_hc/GestionarHcsAction.jsp',2,<%=j%>);" > <img src='<%=BASEURL%>/images/botones/iconos/menos1.gif'   alt="Agregar"   width='12' height='12' align="middle" style="cursor: pointer;" title="Eliminar Documento"> </label>
	<%}%>    </td>
    <td 292 align="center" valign="middle" class="fila">
	<select id="tipo_doc_<%=j+1%>" name="tipo_doc_<%=j+1%>"  onchange="Valida_Cmc(<%=j+1%>);" onfocus="temp(<%=j+1%>);" <%if(!nuevo){%> disabled <%}%>  >
<option value="0" >Seleccione...</option>
      <%for (int i=0;i<lista_tipo_doc.size();i++){%>

      <option value="<%=lista_tipo_doc.get(i).getCodigo() %>"
              <%if(lista_tipo_doc.get(i).getCodigo().equals(lista_cmc.get(j).getTipodoc()))
          {%>  selected="selected"  <%}%> >
          <%=lista_tipo_doc.get(i).getCodigo() %>&nbsp; - &nbsp; <%=lista_tipo_doc.get(i).getDescripcion() %> </option>
      <%}%>
    </select>    </td>
    <td width="144" align="center" valign="middle" class="fila"><input name="cuenta_<%=j+1%>" type="text" id="cuenta_<%=j+1%>" maxlength="10" style="width:90px"
	 onchange="Edita_Documento('<%=BASEURL%>/jsp/finanzas/contab/gestion_de_hc/GestionarHcsAction.jsp',4,<%=j%>);"
     value="<%=lista_cmc.get(j).getCuenta()%>" /></td>
    <td width="89" align="center" valign="middle" class="fila"><select name="dc_<%=j+1%>" id="dc_<%=j+1%>" size="1"
    onchange="Edita_Documento('<%=BASEURL%>/jsp/finanzas/contab/gestion_de_hc/GestionarHcsAction.jsp',4,<%=j%>);">
      <option value="C" <%if(("C").equals(lista_cmc.get(j).getDbcr()))
       {%>  selected="selected"  <%}%> >C</option>
      <option value="D" <%if(("D").equals(lista_cmc.get(j).getDbcr()))
       {%>  selected="selected"  <%}%>>D</option>
    </select></td>
    <td width="122" align="center" valign="middle" class="fila"><select name="tc_<%=j+1%>"  id="tc_<%=j+1%>"size="1" onchange="Edita_Documento('<%=BASEURL%>/jsp/finanzas/contab/gestion_de_hc/GestionarHcsAction.jsp',4,<%=j+1%>);">
      >
      <option value="C" <%if(("C").equals(lista_cmc.get(j).getTipo_cuenta()))
       {%>  selected="selected"  <%}%> >C</option>
      <option value="G" <%if(("G").equals(lista_cmc.get(j).getTipo_cuenta()))
       {%>  selected="selected"  <%}%> >G</option>
    </select>
    </td>
  </tr>
  <%}%>
</table>
<input  type="hidden"  name="sw" id="sw"  value="<%=lista_cmc.size()%>" />
<%break;



case 2:/*Eliminar un cmc de la lista temporal*/

int index=Integer.parseInt(request.getParameter("index"));
lista_cmc=CmcDAO.deletelista(index, lista_cmc);
session.setAttribute("lista_cmc",lista_cmc);
%>
<table width="680" border="0">

<tr>
    <td width="55"   height="24" align="center" valign="middle" class="fila">
	<label onclick="Agrega_Documento('<%=BASEURL%>/jsp/finanzas/contab/gestion_de_hc/GestionarHcsAction.jsp',1);" > <img src='<%=BASEURL%>/images/botones/iconos/mas.gif'   alt="Agregar"   width='12' height='12' align="middle" style="cursor: pointer;" title="Agregar Documento"> </label>    </td>
    <td width=248 align="center" valign="middle" class="fila">
	<select id="tipo_doc_<%=0%>" name="tipo_doc_<%=0%>" onchange="Valida_Cmc(<%=0%>);" onfocus="temp(0);">
      <option value="0" selected="selected">Seleccione...</option>
      <%for (int i=0;i<lista_tipo_doc.size();i++){%>
      <option value="<%=lista_tipo_doc.get(i).getCodigo() %>"><%=lista_tipo_doc.get(i).getCodigo() %>&nbsp; - &nbsp; <%=lista_tipo_doc.get(i).getDescripcion() %> </option>
      <%}%>
    </select>    </td>
    <td width="144" align="center" valign="middle" class="fila"><input name="cuenta_" type="text" id="cuenta_0" style="width:90px"

									 		onchange="Valida_Cuenta('<%=BASEURL%>/jsp/finanzas/contab/gestion_de_hc/GestionarHcsAction.jsp',0);"  maxlength="12" /></td>
    <td width="89" align="center" valign="middle" class="fila"><select name="dc_<%=0%>" id="dc_<%=0%>" size="1">
      <option value="C">C</option>
      <option value="D">D</option>
    </select></td>
    <td width="122" align="center" valign="middle" class="fila"><select name="tc_<%=0%>"  id="tc_<%=0%>"size="1">
      <option value="C">C</option>
      <option value="G">G</option>
    </select></td>
  </tr>


  <%for(int j=0;j<lista_cmc.size();j++){%>
  <tr>
    <td  height="24" align="center" valign="middle" class="fila">

    <% if(sw_permiso) { %>
	<label onclick="Elimina_Documento('<%=BASEURL%>/jsp/finanzas/contab/gestion_de_hc/GestionarHcsAction.jsp',2,<%=j%>);" > <img src='<%=BASEURL%>/images/botones/iconos/menos1.gif'   alt="Agregar"   width='12' height='12' align="middle" style="cursor: pointer;" title="Eliminar Documento"> </label>
	<%}%>    </td>
    <td width=248 align="center" valign="middle" class="fila">
	<select id="tipo_doc_<%=j+1%>" name="tipo_doc_<%=j+1%>" onchange="Valida_Cmc(<%=j+1%>);"  <%if(!nuevo){%> disabled <%}%> >
<option value="0" >Seleccione...</option>
      <%for (int i=0;i<lista_tipo_doc.size();i++){%>
      <option value="<%=lista_tipo_doc.get(i).getCodigo() %>"
              <%if(lista_tipo_doc.get(i).getCodigo().equals(lista_cmc.get(j).getTipodoc()))
          {%>  selected="selected"  <%}%> >
          <%=lista_tipo_doc.get(i).getCodigo() %>&nbsp; - &nbsp; <%=lista_tipo_doc.get(i).getDescripcion() %> </option>
      <%}%>
    </select>    </td>
    <td width="144" align="center" valign="middle" class="fila"><input name="cuenta_<%=j+1%>" type="text" id="cuenta_<%=j+1%>" maxlength="10" style="width:90px"
	 onchange="Edita_Documento('<%=BASEURL%>/jsp/finanzas/contab/gestion_de_hc/GestionarHcsAction.jsp',4,<%=j%>);"
     value="<%=lista_cmc.get(j).getCuenta()%>" /></td>
    <td width="89" align="center" valign="middle" class="fila"><select name="dc_<%=j%>" id="dc_<%=j+1%>" size="1" onchange="Edita_Documento('<%=BASEURL%>/jsp/finanzas/contab/gestion_de_hc/GestionarHcsAction.jsp',4,<%=j%>);">
      <option value="C" <%if(("C").equals(lista_cmc.get(j).getDbcr()))
       {%>  selected="selected"  <%}%> >C</option>
      <option value="D" <%if(("D").equals(lista_cmc.get(j).getDbcr()))
       {%>  selected="selected"  <%}%>>D</option>
    </select></td>
    <td width="122" align="center" valign="middle" class="fila"><select name="tc_<%=j%>"  id="tc_<%=j+1%>"size="1" onchange="Edita_Documento('<%=BASEURL%>/jsp/finanzas/contab/gestion_de_hc/GestionarHcsAction.jsp',4,<%=j+1%>);">
      <option value="C" <%if(("C").equals(lista_cmc.get(j).getTipo_cuenta()))
       {%>  selected="selected"  <%}%> >C</option>
      <option value="G" <%if(("G").equals(lista_cmc.get(j).getTipo_cuenta()))
       {%>  selected="selected"  <%}%> >G</option>
    </select>
    </td>
  </tr>
  <%}%>
</table>
<input  type="hidden"  name="sw" id="sw" value="<%=lista_cmc.size()%>"/>
<%break;

case 3/* Validar que no se repita un hc   */:
    String codigo=request.getParameter("codigo"); 

 %>
 <datos>
     <sw><%= modelcontab.Hcsevice.ValidarHc(codigo) %></sw>
 </datos>
<%
break;
case 4:/*editar  un cmc de la lista temporal*/
index=Integer.parseInt(request.getParameter("index"));
lista_cmc.get(index).setTipodoc(tipo_doc);
lista_cmc.get(index).setCuenta(cuenta);
lista_cmc.get(index).setDbcr(dc);
lista_cmc.get(index).setTipo_cuenta(tc);
session.setAttribute("lista_cmc",lista_cmc);

break;

case 5/* Validar que Exista La Cuenta  */:
%>
<datos>
<sw><%=modelcontab.cuentaService.ExisteCuentas(cuenta)%></sw>
</datos>
<%
break;

case 10:/*eliminar un cmc de la lista temporal*/
	session.removeAttribute("lista_cmc");
	break;
}
%>
