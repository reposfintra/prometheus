<%@ include file="/WEB-INF/InitModel.jsp"%>

<HTML>
<HEAD>
<TITLE></TITLE>
<META http-equiv=Content-Type content="text/html; charset=windows-1252">
<link href="<%=BASEURL%>/jsp/cumplidos/css/estilostsp.css" rel="stylesheet" type="text/css"> 
<link href="<%=BASEURL%>/css/estilostsp.css" rel="stylesheet" type="text/css"> 
<script type='text/javascript' src="<%=BASEURL%>/js/boton.js"></script>
</HEAD>
<BODY>  

  <table width="95%"  border="2" align="center">
    <tr>
      <td>
        <table width="100%" border="0" align="center">
          <tr  class="subtitulo">
            <td height="20"><div align="center">Proceso de Mayorizaci&oacute;n</div></td>
          </tr>
          <tr class="subtitulo1">
            <td>Descripci&oacute;n del funcionamiento del programa de Mayorizaci&oacute;n </td>
          </tr>
          <tr>
            <td  height="18"  class="ayudaHtmlTexto">Formulario para definir fecha inicial y final para el proceso. Seleccione esta opci&oacute;n para realizar mayorizaci&oacute;n a los comprobantes que se encuenten en ese rango de fecha , debe tener en cuenta que la fecha inicial debe ser anterior a la fecha final. </td>
          </tr>
          <tr>
            <td height="18"  class="ayudaHtmlTexto"><div align="center"><img src="<%= BASEURL%>/images/ayuda/mayorizacion/img1.JPG" width="620" height="258"></div></td>
          </tr>
          <tr>
            <td  class="ayudaHtmlTexto">&nbsp;</td>
          </tr>
          <tr>
            <td  class="ayudaHtmlTexto">Seleccione esta opci&oacute;n para realizar mayorizaci&oacute;n a todos los comprobantes.</td>
          </tr>
          <tr>
            <td  class="ayudaHtmlTexto"><div align="center"><img src="<%= BASEURL%>/images/ayuda/mayorizacion/img2.JPG"></div></td>
          </tr>
          <tr>
            <td  class="ayudaHtmlTexto">&nbsp;</td>
          </tr>
          <tr>
            <td  class="ayudaHtmlTexto"><p>Una vez se inicia el proceso, se despliega el siguiente mensaje de confirmaci&oacute;n: </p></td>
          </tr>
          <tr>
            <td  class="ayudaHtmlTexto"><div align="center"><img src="<%= BASEURL%>/images/ayuda/mayorizacion/img3.JPG"></div></td>
          </tr>   
          
      </table></td>
    </tr>
  </table>
  <table width="416" align="center">
	<tr>
	<td align="center">
		<img src="<%=BASEURL%>/images/botones/salir.gif" style="cursor:pointer" name="c_salir" onClick="window.close();" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" align="absmiddle">
	</td>
	</tr>
</table>
</BODY>
</HTML>
