<%@ include file="/WEB-INF/InitModel.jsp"%>

<html>
<head>
<title>Descripci&oacute;n de campos Proceso de Mayorizaci&oacute;n</title>

<script type='text/javascript' src="<%=BASEURL%>/js/boton.js"></script>
<link href="<%=BASEURL%>/css/estilostsp.css" rel="stylesheet" type="text/css"> 
</head>

<body>
<% String BASEIMG = BASEURL +"/images/botones/"; %> 
<br>
<table width="696"  border="2" align="center">
  <tr>
    <td width="811" >
      <table width="100%" border="0" align="center">
        <tr  class="subtitulo">
          <td height="24" colspan="2"><div align="center">PROCESO DE MAYORIZACI&Oacute;N </div></td>
        </tr>
        <tr class="subtitulo1">
          <td colspan="2">GENARAR PROCESO</td>
        </tr>
        <tr>
          <td width="123" class="fila">Por periodo </td>
          <td width="551"  class="ayudaHtmlTexto"> Use esta opci&oacute;n para realizar el proceso de mayorizaci&oacute;n a los comprobantes que se encuentren dentro del periodo definido. </td>
        </tr>
        <tr>
          <td  class="fila"> Todos </td>
          <td  class="ayudaHtmlTexto"> Use esta opci&oacute;n si desea realizar el proceso a todos los comprobantes.</td>
        </tr>
        <tr>
          <td  class="fila">Periodo Inicial </td>
          <td  class="ayudaHtmlTexto"> Seleccione  mes y  a&ntilde;o para definir periodo inicial. </td>
        </tr>
        <tr>
          <td width="123"  class="fila"> Periodo Final </td>
          <td width="551"  class="ayudaHtmlTexto">Seleccione mes y  a&ntilde;o para definir periodo final. </td>
        </tr>
		<tr>
          <td width="123"  class="fila"> Bot&oacute;n Aceptar </td>
          <td width="551"  class="ayudaHtmlTexto">Inicia el proceso de mayorizaci&oacute;n. </td>
        </tr>
		<tr>
          <td width="123"  class="fila"> Bot&oacute;n Salir </td>
          <td width="551"  class="ayudaHtmlTexto">Cierra la ventana. </td>
        </tr>
		<tr>
          <td width="123"  class="fila"> Bot&oacute;n Cancelar </td>
          <td width="551"  class="ayudaHtmlTexto">Reinicia la p&aacute;gina, para volver a ingresar los datos. </td>
        </tr>
      </table>
    </td>
  </tr>
</table>
<br>
<table width="416" align="center">
	<tr>
	<td align="center">
		<img src="<%=BASEURL%>/images/botones/salir.gif" style="cursor:pointer" name="c_salir" onClick="window.close();" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" align="absmiddle">
	</td>
	</tr>
</table>
<p>&nbsp;</p>

<p>&nbsp;</p>
<p>&nbsp; </p>
</body>
</html>
