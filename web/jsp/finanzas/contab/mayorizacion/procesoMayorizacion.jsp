<%@ page session="true"%>
<%@ page errorPage="../error/ErrorPage.jsp"%>
<%@ include file="/WEB-INF/InitModel.jsp"%>
<%@ taglib uri="http://jakarta.apache.org/taglibs/input-0.90" prefix="input" %>
<%@page import="com.tsp.operation.model.*, com.tsp.operation.model.beans.*, com.tsp.util.*,java.util.*, java.text.*"%>

<html>
<head>
<title>Editor de Genaración de Proceso de Mayorización</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">

<script src="<%=BASEURL%>/js/utilidades.js"></script>
<link href="../css/estilostsp.css" rel="stylesheet" type="text/css">
<script type='text/javascript' src="<%= BASEURL %>/js/boton.js"></script>
<link href="<%=BASEURL%>/css/estilostsp.css" rel="stylesheet" type="text/css">
<%
    String mensaje = (String) request.getAttribute("mensaje");
    String path = application.getRealPath(model.menuService.getRealUrl(request.getRequestURI()));
    String datos[] = model.menuService.getContenidoMenu(BASEURL,request.getRequestURI(),path);
%>
</head>

<body>
<div id="capaSuperior" style="position:absolute; width:100%; height:100px; z-index:0; left: 1px; top: 0px;">
 <jsp:include page="/toptsp.jsp?encabezado=Proceso de Mayorización"/>
</div>
<div id="capaCentral" style="position:absolute; width:100%; height:87%; z-index:0; left: 0px; top: 100px; overflow: scroll;">        
	<form id="form1" method="post" action="<%=CONTROLLERCONTAB%>?estado=actualizar&accion=Mayorizacion">
    <table width="50%"  border="2" align="center" onclick="document.getElementById('errorFecha').style.visibility='hidden';">
      <tr>
        <td><table width="100%"  border="1" align="center" class="tablaInferior">
          <tr>
            <td><table width="100%" border="0" cellpadding="0" cellspacing="0">
                <tr>
                  <td height="22" colspan=2 class="subtitulo1"><div align="center" class="subtitulo1">
                     <strong>Generar Proceso </strong>                     
                  </div></td>
                  <td width="212" class="barratitulo">
                    <img src="<%=BASEURL%>/images/titulo.gif" width="32" height="20" align="left">
                    <%=datos[0]%>
                </tr>
              </table>
                <table width="100%"  border="0" align="center" cellpadding="2" cellspacing="3">
                  <tr class="fila">
                    <td width="31%"><input name="tipoQuery" id="tipoQuery1" type="radio" value="1" checked onClick="disableCombos(0);">
            Por periodo </td>
                    <td width="69%"><input name="tipoQuery" id="tipoQuery2" type="radio" value="2" onClick="disableCombos(1);">
            Todos  </td>
                  </tr>
                  
                  <% int anio = Util.AnoActual(); %>
                  
                  <tr class="fila">
                    <td width="31%">Periodo Inicial: </td>
                    <td><select id="mesini" name="mesini">
                            <option value="01">Ene</option>
                            <option value="02">Feb</option>
                            <option value="03">Mar</option>
                            <option value="04">Abr</option>
                            <option value="05">May</option>
                            <option value="06">Jun</option>
                            <option value="07">Jul</option>
                            <option value="08">Ago</option>
                            <option value="09">Sep</option>
                            <option value="10">Oct</option>
                            <option value="11">Nov</option>
                            <option value="12">Dic</option>
                        </select>
                        
                        <select id="anioini" name="anioini">
                            <option value="<%=String.valueOf(anio-2)%>"><%=String.valueOf(anio-2)%></option>
                            <option value="<%=String.valueOf(anio-1)%>"><%=String.valueOf(anio-1)%></option>
                            <option value="<%=String.valueOf(anio)%>" selected><%=String.valueOf(anio)%></option>
                            <option value="<%=String.valueOf(anio+1)%>"><%=String.valueOf(anio+1)%></option>
                        </select>
                        
                  </tr>
                  <tr class="fila">
                    <td><strong>Periodo Final: </strong></td>
                    <td><select id="mesfin" name="mesfin">
                            <option value="01">Ene</option>
                            <option value="02">Feb</option>
                            <option value="03">Mar</option>
                            <option value="04">Abr</option>
                            <option value="05">May</option>
                            <option value="06">Jun</option>
                            <option value="07">Jul</option>
                            <option value="08">Ago</option>
                            <option value="09">Sep</option>
                            <option value="10">Oct</option>
                            <option value="11">Nov</option>
                            <option value="12">Dic</option>
                        </select>

                        <select id="aniofin" name="aniofin">
                            <option value="<%=String.valueOf(anio-2)%>"><%=String.valueOf(anio-2)%></option>
                            <option value="<%=String.valueOf(anio-1)%>"><%=String.valueOf(anio-1)%></option>
                            <option value="<%=String.valueOf(anio)%>" selected><%=String.valueOf(anio)%></option>
                            <option value="<%=String.valueOf(anio+1)%>"><%=String.valueOf(anio+1)%></option>
                        </select>
                      
                  </tr>                  
              </table></td>
          </tr>
        </table></td>
      </tr>
    </table>
    <br>
    <div align="center">      
        <img id="baceptar" name="baceptar"  src="<%=BASEURL%>/images/botones/aceptar.gif" style="cursor:hand"  onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" onClick="validarF('<%=BASEURL%>');">            
        <%if(mensaje!=null){%>
            <img  src="<%=BASEURL%>/images/botones/cancelar.gif" style="cursor:hand"  onClick="location.replace('<%=CONTROLLER%>?estado=Menu&accion=Cargar&carpeta=/jsp/finanzas/contab/mayorizacion&pagina=procesoMayorizacion.jsp&marco=no');" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);">         
        <%}%>
        <img src="<%=BASEURL%>/images/botones/salir.gif"  height="21" name="imgsalir"  onMouseOver="botonOver(this);" onClick="window.close();" onMouseOut="botonOut(this);" style="cursor:hand">            
    </div>
	</form>
    <br/>
	        
	<div id="errorFecha" style="visibility:hidden">
          <table border="2" align="center">
            <tr>
              <td><table width="100%" border="1" align="center"  bordercolor="#F7F5F4" bgcolor="#FFFFFF">
                  <tr>
                    <td width="262" align="center" class="mensajes">El periodo inicial debe ser anterior al periodo final</td>
                    <td width="32" background="<%=BASEURL%>/images/cuadronaranja.JPG">&nbsp;</td>
                    <td width="44">&nbsp;</td>
                  </tr>
              </table></td>
            </tr>
          </table>          
        </div>
        
        <%if(mensaje!=null){%>
        <table border="2" align="center">
            <tr>                
                <td height="45"><table width="410" height="41" border="1" align="center"  bordercolor="#F7F5F4" bgcolor="#FFFFFF">
                    <tr>
                        <td width="282" height="35" align="center" class="mensajes"><%=mensaje%></td>
                        <td width="28" background="<%=BASEURL%>/images/cuadronaranja.JPG">&nbsp;</td>
                        <td width="78">&nbsp;</td>
                    </tr>
                </table></td>
            </tr>
        </table>
        <br>
        <%}%>
        
	
</div>	
<%=datos[1]%>
</body>
</html>