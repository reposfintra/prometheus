<!--
    /******************************************************************************
     * Nombre clase :                   FormularioImpresion.jsp                   *
     * Descripcion :                    Impresion de Comprobantes                 *
     * Autor :                          Mario Fontalvo                            *
     * Fecha Creado :                   03 de Julio de 2006                     *
     * Version :                        1.0                                       *
     * Copyright :                      Fintravalores S.A.                   *
     *****************************************************************************/
-->
<%@page contentType="text/html"%>
<%@page session="true"%>
<%@page import="java.util.*" %>
<%@page errorPage="/error/ErrorPage.jsp"%>
<%@page import="com.tsp.finanzas.contab.model.beans.*,com.tsp.util.*"%>
<%@include file="/WEB-INF/InitModel.jsp"%>
<%@taglib uri="http://jsptags.com/tags/navigation/pager" prefix="pg" %>
<%@taglib uri="http://jakarta.apache.org/taglibs/input-0.90" prefix="input"%>
<%
  String path = application.getRealPath(model.menuService.getRealUrl(request.getRequestURI()));
  String datos[] = model.menuService.getContenidoMenu(BASEURL,request.getRequestURI(),path);
  
  String dstrct = (String) session.getAttribute("Distrito");
  String msg    = (String) request.getAttribute("msg");
  
  TreeMap tipodoc = modelcontab.tipo_doctoSvc.getTreemap();
  tipodoc.put("********** TODOS LOS TIPOS **********","");
%>
<html>
    <head>
        <title>Impresion de Comprobantes Contables</title>
        <script src="<%= BASEURL %>/js/validar.js"></script>
        <script src="<%= BASEURL %>/js/boton.js"></script>
        <script>
            function addOption(Comb,valor,texto){
                var Ele = document.createElement("OPTION");
                Ele.value=valor;
                Ele.text=texto;
                Comb.add(Ele);
            }         
            function Llenar(CmbAnno, CmbMes){
                var Meses    = new Array('Enero','Febrero','Marzo','Abril','Mayo','Junio','Julio','Agosto','Septiembre','Octubre','Noviembre','Diciembre');
                var FechaAct = new Date();
                CmbAnno.length = 0;
                CmbMes.length  = 0;

                for (i=FechaAct.getYear()-7;i<=FechaAct.getYear()+7;i++) addOption(CmbAnno,i,i);
                addOption(CmbAnno,'','*****TODOS*****');
                CmbAnno.value = '';
                for (i=0;i<Meses.length;i++)
                if ((i+1)<10) addOption(CmbMes,'0'+(i+1),Meses[i]);
                else          addOption(CmbMes,(i+1),Meses[i]);                                
                CmbMes.style.display = 'none';
            }       
            
            function validarImpresionComprobante(forma){
                with(forma){
                    if (numtransaccion.value=='' && numdoc.value=='' && tipodoc.value==''){
                        alert ('No puede definir la combinación de TODOS en el tipo de documento y \ndejar vacio el numero de documento debe indicar uno de estos dos \nparametros, a menos de que defina directamente el numero de la \ntransacción.');
                        return false;
                    }
                }
                //alert ('ok');
                return true;
            }
        </script>
    </head>
    <body onload = 'Llenar(forma.Ano, forma.Mes); '>
        <div id="capaSuperior" style="position:absolute; width:100%; height:100px; z-index:0; left: 0px; top: 0px;">
            <jsp:include page="/toptsp.jsp?encabezado=Impresion de Comprobantes Contables"/>
        </div>
        <div id="capaCentral" style="position:absolute; width:100%; height:95%; z-index:0; left: 0px; top: 100px; overflow: scroll;">
            <center>

            <form action="<%= CONTROLLERCONTAB %>?estado=Impresion&accion=Comprobantes" method="post" name="forma">
            <table width="450" border="2">
                <tr>
                    <td colspan='2'>	
                    <!-- cabecera de la seccion -->
                    <table width="100%" align="center" class="tablaInferior" >
                        <tr>
                            <td class="subtitulo1"  width="50%">Impresión de Comprobantes</td>
                            <td class="barratitulo" width="50%"><img src="<%= BASEURL %>/images/titulo.gif" width="32" height="20" align="left"><%=datos[0]%></td>
                        </tr>
                    </table>		
                    <table width='100%'>
                        <tr class="fila">
                            <td width='35%'>Tipo Documento</td>
                            <td width='65%' colspan='2'><input:select name="tipodoc" attributesText="style='width:100%'"  options="<%= tipodoc %>" /></td>
                        </tr>
                        <tr class="fila">
                            <td>Numero Documento</td>
                            <td colspan='2'><input type='text' name='numdoc' maxlength='15' ></td>
                        </tr>
                        <tr class="fila">                        
                            <td>Grupo Transaccion</td>
                            <td colspan='2'><input type='text' name='numtransaccion' onkeypress="soloDigitos(event,'decNO');" ></td>
                        </tr>
                        <tr class="fila">                        
                            <td>Periodo</td>
                            <td><select class="select" style="width:100%" name="Ano" onchange="Mes.style.display = (this.value==''?'none':'block'); "></select></td>
                            <td><select class="select" style="width:100%; display:none" name="Mes"></select></td>
                        </tr>
                    </table>
                    <!-- fin cabecera -->	
                </td>
                </tr>
            </table>
            <br>
            <input type="hidden" name="Opcion" value="Imprimir">                
            <img src='<%=BASEURL%>/images/botones/imprimir.gif'  style='cursor:hand' onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" onclick=" if(validarImpresionComprobante(forma)) { forma.submit(); } ">
            
            <img src='<%=BASEURL%>/images/botones/cancelar.gif'  style='cursor:hand' onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" onclick=" forma.reset();  Ano.value=''; Mes.style.display = (Ano.value==''?'none':'block'); ">

            <img src='<%=BASEURL%>/images/botones/salir.gif'     style='cursor:hand' onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" onclick='window.close();'>        
            
            </form>
            
            <br>
            
            <% if (msg!=null && !msg.equals("")) { %>
            <table border="2" align="center">
                <tr>
                    <td><table width="100%"  border="1" align="center"  bordercolor="#F7F5F4" bgcolor="#FFFFFF">
                    <tr> 
                    <td width="350" align="center" class="mensajes"><%= msg  %></td>
                    <td width="29" background="<%=BASEURL%>/images/cuadronaranja.JPG">&nbsp;</td>
                    <td width="58">&nbsp;</td>
                </tr>
                </table></td>
                </tr>
            </table>                          
            <% } %>
		
		

            </center>
        </div>
        <%=datos[1]%>
    </body>
</html>
