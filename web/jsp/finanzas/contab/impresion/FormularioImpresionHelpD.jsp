<!--
     - Author(s)       :      MARIO FONTALVO
     - Date            :      25/01/2006
     - Copyright Notice:      TRANSPORTES SANCHEZ POLO S.A.
-->
<%--
     - @(#)
     - Description: Repoortes
--%>
<%@include file="/WEB-INF/InitModel.jsp"%>
<html>
<head>
<title>IMPRESION DE COMPROBANTES</title>
<link href="../../../css/estilostsp.css" rel="stylesheet" type="text/css">
<link href="<%= BASEURL %>/css/estilostsp.css" rel="stylesheet" type="text/css">
<script src ="<%= BASEURL %>/js/boton.js"></script>	
</head>
<body>
<BR>
<table width="696"  border="2" align="center">
  <tr>
    <td width="811" >
      <table width="100%" border="0" align="center">
        <tr  class="subtitulo">
          <td height="24" colspan="2"><div align="center">IMPRESION DE COMBROBANTES</div></td>
        </tr>
		<tr class="subtitulo1">
          <td colspan="2"> PARAMETROS DE IMPRESION DE COMPROBANTES</td>
        </tr>
        <tr>
          <td  class="fila" align='center'> Tipo de Documento </td>
          <td  class="ayudaHtmlTexto">Este es opcional si y solo si define el directamente el numero de la transaccion..</td>
        </tr>
        <tr>
          <td  class="fila" align='center'> Numero del documento</td>
          <td  class="ayudaHtmlTexto">Documento a consultar, este es opcional si y solo si se defina el numero de la transaccion</td>
        </tr>
        <tr>
          <td  class="fila" align='center'> Grupo de Transaccion</td>
          <td  class="ayudaHtmlTexto">Este parametro es opcional si y solo si se definen valores para los otros parametros de la busqueda.</td>
        </tr>
        <tr>
          <td  class="fila" align='center'> Periodo</td>
          <td  class="ayudaHtmlTexto">Periodo A�o Mes para la consulta de comprobantes contables, y tambien es opcional bajo las restricciones del tipo de Documento</td>
        </tr>
        
        <tr>
          <td class="fila" align='center'> <img src='<%= BASEURL %>/images/botones/imprimir.gif'>   </td>
          <td class="ayudaHtmlTexto">Boton de que inicia el proceso de impresion de comprobantes contables.</td>
        </tr>
        <tr>
          <td class="fila" align='center'> <img src='<%= BASEURL %>/images/botones/cancelar.gif'>   </td>
          <td class="ayudaHtmlTexto">Boton de REINICIO de los parametros definidos para la impresion</td>
        </tr>


      </table>
    </td>
  </tr>
</table>
</body>
<br>
<center> <img  name="imgSalir" src="<%=BASEURL%>/images/botones/salir.gif"       onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" style="cursor:hand" onClick=" window.close(); ">
</center>
</html>
