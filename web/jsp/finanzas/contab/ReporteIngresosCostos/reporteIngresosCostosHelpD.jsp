<%@ include file="/WEB-INF/InitModel.jsp"%>

<html>
<head>
<title>Descripci&oacute;n de reporte de ingresos y costos</title>

<script type='text/javascript' src="<%=BASEURL%>/js/boton.js"></script>
<link href="<%=BASEURL%>/css/estilostsp.css" rel="stylesheet" type="text/css">
<link href="../../../../css/estilostsp.css" rel="stylesheet" type="text/css">
</head>

<body>
<% String BASEIMG = BASEURL +"/images/botones/"; %> 
<br>
<table width="696"  border="2" align="center">
  <tr>
    <td width="811" >
      <table width="100%" border="0" align="center">
        <tr  class="subtitulo">
          <td height="24" colspan="2"><div align="center">DESCRIPCI&Oacute;N DE REPORTE DE INGRESOS Y COSTOS </div></td>
        </tr>
        <tr class="subtitulo1">
          <td colspan="2">GENARAR PROCESO</td>
        </tr>
        <tr>
          <td width="123" class="fila">Periodo </td>
          <td width="551"  class="ayudaHtmlTexto"> Seleccione el periodo en que se desea realizar el reporte</td>
        </tr>
        <tr>
          <td  class="fila">Fecha Inicial </td>
          <td  class="ayudaHtmlTexto"> Seleccione la fecha inicial teniendo en cuenta el periodo </td>
        </tr>
        <tr>
          <td width="123"  class="fila"> Fecha Final </td>
          <td width="551"  class="ayudaHtmlTexto">Seleccione la fecha final teniendo en cuenta el periodo </td>
        </tr>
		<tr>
          <td width="123"  class="fila"> Bot&oacute;n Aceptar </td>
          <td width="551"  class="ayudaHtmlTexto">Inicia el proceso de generaci&oacute;n del reporte</td>
        </tr>
		<tr>
          <td width="123"  class="fila"> Bot&oacute;n Salir </td>
          <td width="551"  class="ayudaHtmlTexto">Cierra la ventana. </td>
        </tr>
		<tr>
          <td width="123"  class="fila"> Bot&oacute;n Cancelar </td>
          <td width="551"  class="ayudaHtmlTexto">Reinicia la p&aacute;gina, para volver a ingresar los datos. </td>
        </tr>
      </table>
    </td>
  </tr>
</table>
<br>
<table width="416" align="center">
	<tr>
	<td align="center">
		<img src="<%=BASEURL%>/images/botones/salir.gif" style="cursor:pointer" name="c_salir" onClick="window.close();" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" align="absmiddle">
	</td>
	</tr>
</table>
<p>&nbsp;</p>

<p>&nbsp;</p>
<p>&nbsp; </p>
</body>
</html>
