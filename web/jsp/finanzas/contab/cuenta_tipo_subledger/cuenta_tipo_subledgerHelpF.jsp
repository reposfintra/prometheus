<%@ include file="/WEB-INF/InitModel.jsp"%>
<script type='text/javascript' src="<%= BASEURL %>/js/boton.js"></script>
<HTML>
<HEAD>
<TITLE></TITLE>
<META http-equiv=Content-Type content="text/html; charset=windows-1252">
<link href="../../../../css/estilostsp.css" rel="stylesheet" type="text/css"> 
<link href="<%=BASEURL%>/css/estilostsp.css" rel="stylesheet" type="text/css"> 
<style type="text/css">
<!--
.Estilo2 {
	font-size: 16pt;
	color: #FF0000;
}
-->
</style>
</HEAD>
<BODY> 

        <table width="90%" border="0" align="center">
          <tr  class="subtitulo">
            <td height="20" align="center">MANUAL DE CREACION CUENTAS TIPO SUBLEDGER</td>
          </tr>
          <tr class="subtitulo1">
            <td> Registrar cuentas tipo subledger</td>
          </tr>
          <tr>
            <td  height="8"  class="ayudaHtmlTexto">Para registrar el tipo de cuentas debe llenar los campos solicitados por el formulario.</td>
          </tr>
          <tr>
            <td height="18"  class="ayudaHtmlTexto" align="center"><div align="center"><img src="<%=BASEURL%>/images/ayuda/cuenta_tipo_subledger/imagen1.JPG" ></div></td>
          </tr>
          <tr>
            <td height="18"  class="ayudaHtmlTexto" align="center"><p>Si se digita un numero de cuenta que no aplica subledger el sistema le muestra el siguiente mensaje.<br>
            </p>
            </td>
          </tr>
          <tr>
            <td height="18"  class="ayudaHtmlTexto" align="center"><div align="center"><img src="<%=BASEURL%>/images/ayuda/cuenta_tipo_subledger/imagen2.JPG" >
            </div></td>
          </tr>
          <tr>
            <td height="18"  class="ayudaHtmlTexto" align="center">Luego de digitar la informaci&oacute;n del formulario procede a presionar aceptar para almacenar la informaci&oacute;n.</td>
          </tr>
          <tr>
            <td height="18"  class="ayudaHtmlTexto" align="center"><div align="center"><img src="<%=BASEURL%>/images/ayuda/cuenta_tipo_subledger/imagen3.JPG" ></div></td>
          </tr>
          <tr>
            <td height="18"  class="ayudaHtmlTexto" align="center">&nbsp;</td>
          </tr>
          <tr>
            <td height="18"  class="ayudaHtmlTexto" align="center">&nbsp;</td>
          </tr>
      </table>
        <div align="center"><br>
            <img src="<%=BASEURL%>/images/botones/salir.gif" name="salir" style="cursor:hand"  onClick="parent.close();" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);">
        </div>
</BODY>
</HTML>
