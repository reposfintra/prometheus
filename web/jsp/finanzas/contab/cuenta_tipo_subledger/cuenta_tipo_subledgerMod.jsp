<!--
- Descripci�n :                    Pagina JSP, que maneja el ingreso de las cuentas tipo subledger                  
- Autor :                          Ing. Diogenes Bastidas                       
- Fecha Creado :                   8 de Junio de 2005                
- Versi�n :                        1.0                                       
- Copyright :                      Fintravalores S.A.                   
-->

<%-- Declaracion de librerias--%>
<%@page contentType="text/html"%>
<%@ page session="true"%>
<%@page import="com.tsp.operation.model.beans.*, java.util.*"%>
<%@ include file="/WEB-INF/InitModel.jsp"%>
<%@page import="com.tsp.operation.model.*"%>
<%@page import="com.tsp.operation.model.*"%>
<%@page import="com.tsp.util.*"%>
<%@ taglib uri="http://jakarta.apache.org/taglibs/input-0.90" prefix="input" %>
<html>
<head>
<title>Modificar Cuentas Subledger</title>
<link href="<%= BASEURL %>/css/estilostsp.css" rel="stylesheet" type="text/css">
<link href="../../../../css/estilostsp.css" rel="stylesheet" type="text/css">
<script type='text/javascript' src="<%= BASEURL %>/js/boton.js"></script>
</head>
<script type='text/javascript' src="<%= BASEURL %>/js/validar.js"></script>
<body onLoad="<%=(request.getParameter("modificado")!=null)?"parent.opener.location.reload();redimensionar();window.close();":"redimensionar();"%>" onResize="redimensionar();">
<div id="capaSuperior" style="position:absolute; width:100%; height:100px; z-index:0; left: 0px; top: 0px;">
<jsp:include page="/WEB-INF/toptsp.jsp?encabezado=Insertar Cuentas Tipo Subledger"/>
</div>

<div id="capaCentral" style="position:absolute; width:100%; height:85%; z-index:0; left: 0px; top: 100px; overflow: scroll;"> 
<%String tip = (request.getParameter("tipo") != null)?request.getParameter("tipo"):"";
String ntip = (request.getParameter("ntipo") != null)?request.getParameter("ntipo"):"";
String num = (request.getParameter("cuenta") != null)?request.getParameter("cuenta"):"";%>
<FORM name='forma' id='forma' method='POST' action='<%=CONTROLLERCONTAB%>?estado=CuentaTipoSudledger&accion=Evento&carpeta=/jsp/finanzas/contab/cuenta_tipo_subledger&pagina=cuenta_tipo_subledgerMod.jsp' onSubmit="return CamposLlenos();">
  <table width="400" border="2" align="center">
    <tr>
      <td>
        <table width="100%"  border="0">
          <tr>
            <td width="51%"class="subtitulo1">Informaci&oacute;n</td>
            <td width="49%" class="barratitulo"><img src="<%=BASEURL%>/images/titulo.gif" width="32" height="20"></td>
          </tr>
        </table>
        <table width="100%" class="tablaInferior">
          <tr>
            <td width="45%" align="left" class="fila">&nbsp;Nro. Cuenta </td>
            <td width="55%" align="left" class="letra"><input name="cuenta" type="hidden" id="cuenta" size="25" maxlength="25" value="<%=num%>"><%=num%>
              <input name="evento" type="hidden" id="evento" value="Modificar"></td>
          </tr>
          <tr class="fila">
            <td align="left" >Tipo Subledger</td>
            <td valign="middle"><select name="ntipo" class="textbox" id="ntipo" style="width:90% ">
              <option value="">Seleccione Un Item</option>
              <% LinkedList tbltipo = model.tablaGenService.obtenerTablas();
               for(int i = 0; i<tbltipo.size(); i++){
                       TablaGen tipo = (TablaGen) tbltipo.get(i); %>
              <option value="<%=tipo.getTable_code()%>" <%=(tipo.getTable_code().equals(ntip))?"selected":""%> ><%=tipo.getDescripcion()%></option>
              <%}%>
            </select>
              <img src="<%=BASEURL%>/images/botones/iconos/obligatorio.gif" width="10" height="10">              <input name="tipo" type="hidden" id="tipo" value="<%=tip%>"></td>
          </tr>
      </table></td>
    </tr>
  </table>
<p>
<div align="center"><input type="image" src="<%=BASEURL%>/images/botones/modificar.gif" style="cursor:hand"  name="agregar"  onMouseOver="botonOver(this);" onMouseOut="botonOut(this);">&nbsp;
<img src="<%=BASEURL%>/images/botones/anular.gif" style="cursor:hand" name="cancelar"  onMouseOver="botonOver(this);" onClick="window.location='<%=CONTROLLERCONTAB%>?estado=CuentaTipoSudledger&accion=Evento&evento=Anular&carpeta=/jsp/trafico/mensaje&pagina=MsgAnulado.jsp&cuenta=<%=num%>&tipo=<%=ntip%>'" onMouseOut="botonOut(this);" >&nbsp;
<img src="<%=BASEURL%>/images/botones/salir.gif" name="salir" style="cursor:hand"   onClick="parent.close();" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);"></div>
</p>
  <%String mensaje = request.getParameter("mensaje"); 
 %>
    <%  if(mensaje!=null){%>
            <br>
            <table border="2" align="center">
              <tr>
                <td><table width="100%"  border="1" align="center"  bordercolor="#F7F5F4" bgcolor="#FFFFFF">
                    <tr>
                      <td width="229" align="center" class="mensajes"><%=mensaje%></td>
                      <td width="29" background="<%=BASEURL%>/images/cuadronaranja.JPG">&nbsp;</td>
                      <td width="58">&nbsp;</td>
                    </tr>
                </table></td>
              </tr>
            </table>
    <%  }%>	
</FORM>
</div>
</body>
</html>
