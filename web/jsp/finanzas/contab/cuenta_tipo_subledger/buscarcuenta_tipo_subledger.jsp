<!--
- Descripci�n :                    Pagina JSP, que maneja la buscaqueda de las cuentas tipo subledger                  
- Autor :                          Ing. Diogenes Bastidas                       
- Fecha Creado :                   5 de Junio de 2005                
- Versi�n :                        1.0                                       
- Copyright :                      Fintravalores S.A.                   
-->

<%-- Declaracion de librerias--%>
<%@page contentType="text/html"%>
<%@ page session="true"%>
<%@page import="com.tsp.operation.model.beans.*, java.util.*"%>
<%@ include file="/WEB-INF/InitModel.jsp"%>
<%@page import="com.tsp.operation.model.*"%>
<%@page import="com.tsp.util.*"%>
<%@ taglib uri="http://jakarta.apache.org/taglibs/input-0.90" prefix="input" %>
<html>
<head>
<title>Buscar Subledger</title>
<link href="<%= BASEURL %>/css/estilostsp.css" rel="stylesheet" type="text/css">
<link href="../../../../css/estilostsp.css" rel="stylesheet" type="text/css">
<script type='text/javascript' src="<%= BASEURL %>/js/boton.js"></script>
</head>
<script type='text/javascript' src="<%= BASEURL %>/js/validar.js"></script>
<script type='text/javascript' src="<%= BASEURL %>/js/subledger.js"></script>
<body onLoad="redimensionar();" onResize="redimensionar();">
<div id="capaSuperior" style="position:absolute; width:100%; height:100px; z-index:0; left: 0px; top: 0px;">
<jsp:include page="/WEB-INF/toptsp.jsp?encabezado=Buscar Cuenta Tipo Subledger"/>
</div>
<%String path = application.getRealPath(model.menuService.getRealUrl(request.getRequestURI()));
  String datos[] = model.menuService.getContenidoMenu(BASEURL,request.getRequestURI(),path);
 TreeMap subledger = modelcontab.subledgerService.getCuentasTipoSubledger();
   if (subledger==null)
      subledger = new TreeMap();
   subledger.put("Seleccione un item","");
   %>
<div id="capaCentral" style="position:absolute; width:100%; height:85%; z-index:0; left: 0px; top: 100px; overflow: scroll;"> 
<FORM name='forma' id='forma' method='POST' action='<%=CONTROLLERCONTAB%>?estado=CuentaTipoSudledger&accion=Evento&carpeta=/jsp/finanzas/contab/cuenta_tipo_subledger&pagina=verCuenta_tipo_subledger.jsp'>
  <table width="400" border="2" align="center">
    <tr>
      <td>
        <table width="100%"  border="0">
          <tr>
            <td width="51%" class="subtitulo1">Buscar</td>
            <td width="49%" class="barratitulo"><img src="<%=BASEURL%>/images/titulo.gif" width="32" height="20" align="left"><%=datos[0]%></td>
          </tr>
        </table>
        <table width="100%" class="tablaInferior">
          <tr class="fila">
            <td width="36%" align="left" >Nro. Cuenta </td>
            <td width="64%" align="left" >
               <input name="cuenta" type="text" id="cuenta" size="25" maxlength="25" >
               <input name="evento" type="hidden" id="evento" value="Busqueda"></td>
          </tr>
      </table></td>
    </tr>
  </table>
<p>
<div align="center"><img src="<%=BASEURL%>/images/botones/buscar.gif" style="cursor:hand"  name="agregar"  onclick="forma.submit()" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);">&nbsp;<img src="<%=BASEURL%>/images/botones/detalles.gif" style="cursor:hand" name="cancelar"  onMouseOver="botonOver(this);" onClick="window.location='<%=CONTROLLERCONTAB%>?estado=CuentaTipoSudledger&accion=Evento&evento=Busqueda&carpeta=/jsp/finanzas/contab/cuenta_tipo_subledger&pagina=verCuenta_tipo_subledger.jsp&cuenta='" onMouseOut="botonOut(this);" >&nbsp;
<img src="<%=BASEURL%>/images/botones/salir.gif" name="salir" style="cursor:hand" title="Salir al Menu Principal" onClick="parent.close();" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);"></div>
</p>
</FORM>
</div>
<%=datos[1]%>
</body>
</html>
