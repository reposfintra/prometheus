<!--
- Autor : Ing. Diogenes Bastidas Morales
- Date  : 28 de noviembre de 2005
- Copyrigth Notice : Fintravalores S.A. S.A
-->
<%--
-@(#)
--Descripcion : Pagina JSP, lista de Cuentas Tipo Subledger

--%>

<%-- Declaracion de librerias--%>
<%@page contentType="text/html"%>
<%@page session="true"%>
<%@page errorPage="/error/ErrorPage.jsp"%>
<%@page import="java.util.*"%> 
<%@page import="com.tsp.operation.model.*"%>
<%@page import="com.tsp.util.*"%>
<%@page import="com.tsp.operation.model.beans.*, java.util.*"%>
<%@include file="/WEB-INF/InitModel.jsp"%>
<%@taglib uri="http://jsptags.com/tags/navigation/pager" prefix="pg" %>
 
<html>
<head>
<title>Listado de Cuentas Tipo Subledger</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link href="<%= BASEURL %>/css/estilostsp.css" rel="stylesheet" type="text/css">
<link href="file:///C|/Tomcat5/webapps/slt%20ING/css/estilostsp.css" rel="stylesheet" type="text/css"> 
<script type='text/javascript' src="<%= BASEURL %>/js/reporte.js"></script>
<script type='text/javascript' src="<%= BASEURL %>/js/boton.js"></script>

</head>
<body onLoad="redimensionar();" onresize="redimensionar()">
<div id="capaSuperior" style="position:absolute; width:100%; height:100px; z-index:0; left: 0px; top: 0px;">
	<jsp:include page="/WEB-INF/toptsp.jsp?encabezado=Listado de Subledger"/>
</div>
<div id="capaCentral" style="position:absolute; width:100%; height:87%; z-index:0; left: 0px; top: 100px; overflow: scroll;">

  <%  String style = "simple";
    String position =  "bottom";
    String index =  "center";
    int maxPageItems = 10;
    int maxIndexPages = 10;
	
	LinkedList lista = modelcontab.subledgerService.getCuentastsubledger();
    TablaGen obj;
    System.out.println(lista.size()); 
	if ( lista.size() >0 ){  
%>
</p>
<table width="650" border="2" align="center">
    <tr>
      <td width="1009">
	  <table width="100%" align="center">
              <tr>
                <td width="373" class="subtitulo1">&nbsp;Datos</td>
                <td width="427" class="barratitulo"><img src="<%=BASEURL%>/images/titulo.gif" width="32" height="20"></td>
              </tr>
        </table>
		  <table width="99%" border="1" align="center" bordercolor="#999999" bgcolor="#F7F5F4">
          <tr class="tblTitulo">
          <td width="25%"  align="center">Cuenta</td>
          <td width="30%"  align="center">Descripci&oacute;n</td>
          <td width="8%"  align="center">Codigo</td>
          <td width="37%" align="center">Tipo</td>
        </tr>
        <pg:pager
         items="<%=lista.size()%>"
         index="<%= index %>"
         maxPageItems="<%= maxPageItems %>"
         maxIndexPages="<%= maxIndexPages %>"
         isOffset="<%= true %>"
         export="offset,currentPageNumber=pageNumber"
         scope="request">
        <%-- keep track of preference --%>
        <%
      for (int i = offset.intValue(), l = Math.min(i + maxPageItems, lista.size()); i < l; i++)
	  {
          obj = (TablaGen) lista.get(i);%>
        <pg:item>
        <tr class="<%=(i % 2 == 0 )?"filagris":"filaazul"%>" onMouseOver='cambiarColorMouse(this)'   style="cursor:hand"
        onClick="window.open('<%=CONTROLLERCONTAB%>?estado=CuentaTipoSudledger&accion=Evento&evento=Buscar&carpeta=/jsp/finanzas/contab/cuenta_tipo_subledger&pagina=cuenta_tipo_subledgerMod.jsp&evento=Buscar&cuenta=<%=obj.getTable_type()%>&tipo=<%=obj.getTable_code()%>' ,'M','status=yes,scrollbars=no,width=780,height=500,resizable=yes');">
          <td width="25%" class="bordereporte"><%=obj.getTable_type()%></td>
          <td width="30%" class="bordereporte"><%=obj.getDescuenta()%></td>
          <td width="8%" class="bordereporte" align="center"><%=obj.getTable_code()%></td>
          <td width="37%" class="bordereporte"><%=obj.getDescripcion()%></td>
        </tr>
        </pg:item>
        <%}%>
        <tr class="bordereporte">
          <td td height="20" colspan="12" nowrap align="center">
		   <pg:index>
            <jsp:include page="/WEB-INF/jsp/googlesinimg.jsp" flush="true"/>      
           </pg:index> 
	      </td>
        </tr>
        </pg:pager>
      </table></td>
    </tr>
</table>
  <p>
      <%}
 else { %>
</p>
  <table border="2" align="center">
    <tr>
      <td><table width="410" height="40" border="1" align="center"  bordercolor="#F7F5F4" bgcolor="#FFFFFF">
          <tr>
            <td width="282" align="center" class="mensajes">Su b&uacute;squeda no arroj&oacute; resultados!</td>
            <td width="28" background="<%=BASEURL%>/images/cuadronaranja.JPG">&nbsp;</td>
            <td width="78">&nbsp;</td>
          </tr>
      </table></td>
    </tr>
  </table>
  <p>&nbsp; </p>
  <%}%>
<table width="650" border="0" align="center">
   <tr>
     <td width="1515"><img src="<%=BASEURL%>/images/botones/regresar.gif"  name="imgaceptar" onClick="window.location='<%=CONTROLLER%>?estado=Menu&accion=Cargar&pagina=buscarcuenta_tipo_subledger.jsp&carpeta=/jsp/finanzas/contab/cuenta_tipo_subledger&marco=no'" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" style="cursor:hand"></td>
   </tr>
 </table>
</div> 
</body>

</html>
