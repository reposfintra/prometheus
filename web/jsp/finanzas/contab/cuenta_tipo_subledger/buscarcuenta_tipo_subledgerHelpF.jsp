<%@ include file="/WEB-INF/InitModel.jsp"%>

<HTML>
<HEAD>
<TITLE></TITLE>
<META http-equiv=Content-Type content="text/html; charset=windows-1252">
<link href="../../../../css/estilostsp.css" rel="stylesheet" type="text/css"> 
<link href="<%=BASEURL%>/css/estilostsp.css" rel="stylesheet" type="text/css"> 
<style type="text/css">
<!--
.Estilo2 {
	font-size: 16pt;
	color: #FF0000;
}
-->
</style>
</HEAD>
<BODY> 

        <table width="90%" border="0" align="center">
          <tr  class="subtitulo">
            <td height="20" align="center">MANUAL DE BUSQUEDA DE CUENTAS TIPO SUBLEDGER</td>
          </tr>
          <tr class="subtitulo1">
            <td> B&uacute;squedas de las cuentas tipo subledger</td>
          </tr>
          <tr>
            <td  height="8"  class="ayudaHtmlTexto">Para realizar las b&uacute;squedas de las cuentas tipo subledger, debe digitar el numero de la cuenta.</td>
          </tr>
          <tr>
            <td height="18"  class="ayudaHtmlTexto" align="center"><div align="center"><img src="<%=BASEURL%>/images/ayuda/cuenta_tipo_subledger/imagen4.JPG"  |></div></td>
          </tr>
          <tr>
            <td height="18"  class="ayudaHtmlTexto" align="center"><p>Luego de digitar el numero de la cuenta presiones buscar, el sistema le muestra un listado con las cuenta.<br>
            </p>
            </td>
          </tr>
          <tr>
            <td height="18"  class="ayudaHtmlTexto" align="center"><div align="center"><img src="<%=BASEURL%>/images/ayuda/cuenta_tipo_subledger/imagen5.JPG" >
            </div></td>
          </tr>
          <tr>
            <td height="18"  class="ayudaHtmlTexto" align="center">Para modificar o anular la informaci&oacute;n de una cuenta, solo presione clic sobre la cuenta.</td>
          </tr>
          <tr>
            <td height="18"  class="ayudaHtmlTexto" align="center"><div align="center"><img src="<%=BASEURL%>/images/ayuda/cuenta_tipo_subledger/imagen6.JPG"></div></td>
          </tr>
          <tr>
            <td height="18"  class="ayudaHtmlTexto" align="center">luego de haber modificado la informaci&oacute;n y presionar aceptar, el sistema le muestra la siguiente pantalla</td>
          </tr>
          <tr>
            <td height="18"  class="ayudaHtmlTexto" align="center"><div align="center"><img src="<%=BASEURL%>/images/ayuda/cuenta_tipo_subledger/imagen7.JPG" ></div></td>
          </tr>
          <tr>
            <td height="18"  class="ayudaHtmlTexto" align="center">&nbsp;</td>
          </tr>
      </table>
</BODY>
</HTML>
