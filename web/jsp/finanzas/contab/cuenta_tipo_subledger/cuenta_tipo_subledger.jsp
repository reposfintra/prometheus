<!--
- Descripci�n :                    Pagina JSP, que maneja el ingreso de las cuentas tipo subledger                  
- Autor :                          Ing. Diogenes Bastidas                       
- Fecha Creado :                   8 de Junio de 2005                
- Versi�n :                        1.0                                       
- Copyright :                      Fintravalores S.A.                   
-->

<%-- Declaracion de librerias--%>
<%@page contentType="text/html"%>
<%@ page session="true"%>
<%@ page errorPage="../error/error.jsp"%>
<%@page import="com.tsp.operation.model.beans.*, java.util.*"%>
<%@ include file="/WEB-INF/InitModel.jsp"%>
<%@page import="com.tsp.operation.model.*"%>
<%@page import="com.tsp.operation.model.*"%>
<%@page import="com.tsp.util.*"%>
<%@ taglib uri="http://jakarta.apache.org/taglibs/input-0.90" prefix="input" %>
<html>
<head>
<title>Insertar Cuentas Subledger</title>
<link href="<%= BASEURL %>/css/estilostsp.css" rel="stylesheet" type="text/css">
<link href="../../../../css/estilostsp.css" rel="stylesheet" type="text/css">
<script type='text/javascript' src="<%= BASEURL %>/js/boton.js"></script>
</head>
<script type='text/javascript' src="<%= BASEURL %>/js/general.js"></script>
<body onLoad="redimensionar();" onResize="redimensionar();">
<div id="capaSuperior" style="position:absolute; width:100%; height:100px; z-index:0; left: 0px; top: 0px;">
<jsp:include page="/WEB-INF/toptsp.jsp?encabezado=Insertar Cuentas Subledger"/>
</div>

<div id="capaCentral" style="position:absolute; width:100%; height:85%; z-index:0; left: 0px; top: 100px; overflow: scroll;"> 
<%String path = application.getRealPath(model.menuService.getRealUrl(request.getRequestURI()));
  String datos[] = model.menuService.getContenidoMenu(BASEURL,request.getRequestURI(),path);
String tip = (request.getParameter("tipo") != null)?request.getParameter("tipo"):"";
String num = (request.getParameter("cuenta") != null)?request.getParameter("cuenta"):"";%>
<FORM name='forma' id='forma' method='POST' onSubmit="return CamposLlenos(forma);" action='<%=CONTROLLERCONTAB%>?estado=CuentaTipoSudledger&accion=Evento&carpeta=/jsp/finanzas/contab/cuenta_tipo_subledger&pagina=cuenta_tipo_subledger.jsp'>
  <table width="400" border="2" align="center">
    <tr>
      <td>
        <table width="100%"  border="0">
          <tr>
            <td width="51%"class="subtitulo1">Informaci&oacute;n</td>
            <td width="49%" class="barratitulo"><img src="<%=BASEURL%>/images/titulo.gif" width="32" height="20" align="left"><%=datos[0]%></td>
          </tr>
        </table>
        <table width="100%" class="tablaInferior">
          <tr class="fila">
            <td width="46%" align="left">&nbsp;Nro. Cuenta </td>
            <td width="54%" align="left"><input name="cuenta" type="text" id="cuenta" size="25" maxlength="25" value="<%=num%>">
              <img src="<%=BASEURL%>/images/botones/iconos/obligatorio.gif" width="10" height="10">
              <input name="evento" type="hidden" id="evento" value="Ingresar"></td>
          </tr>
          <tr class="fila">
            <td align="left" >Tipo Subledger</td>
            <td valign="middle"><select name="tipo" class="textbox" id="tipo" style="width:90% ">
              <option value="">Seleccione Un Item</option>
              <% LinkedList tbltipo = model.tablaGenService.obtenerTablas();
               for(int i = 0; i<tbltipo.size(); i++){
                       TablaGen tipo = (TablaGen) tbltipo.get(i); %>
              <option value="<%=tipo.getTable_code()%>" <%=(tipo.getTable_code().equals(tip))?"selected":""%> ><%=tipo.getDescripcion()%></option>
              <%}%>
            </select>
              <img src="<%=BASEURL%>/images/botones/iconos/obligatorio.gif" width="10" height="10"></td>
          </tr>
      </table></td>
    </tr>
  </table>
<p>
<div align="center"><input type="image" src="<%=BASEURL%>/images/botones/aceptar.gif" style="cursor:hand"  name="agregar"   onMouseOver="botonOver(this);" onMouseOut="botonOut(this);">&nbsp;
<img src="<%=BASEURL%>/images/botones/cancelar.gif" style="cursor:hand" name="cancelar"  onMouseOver="botonOver(this);" onClick="forma.reset();" onMouseOut="botonOut(this);" >&nbsp;
<img src="<%=BASEURL%>/images/botones/salir.gif" name="salir" style="cursor:hand"   onClick="parent.close();" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);"></div>
</p>
  <%String mensaje = request.getParameter("mensaje"); 
 %>
    <%  if(mensaje!=null){%>
            <br>
            <table border="2" align="center">
              <tr>
                <td><table width="100%"  border="1" align="center"  bordercolor="#F7F5F4" bgcolor="#FFFFFF">
                    <tr>
                      <td width="229" align="center" class="mensajes"><%=mensaje%></td>
                      <td width="29" background="<%=BASEURL%>/images/cuadronaranja.JPG">&nbsp;</td>
                      <td width="58">&nbsp;</td>
                    </tr>
                </table></td>
              </tr>
            </table>
    <%  }%>	
</FORM>
</div>
<%=datos[1]%>
</body>
</html>
