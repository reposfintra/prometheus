<!--
- Autor : Ing. Andr�s Maturana De La Cruz
- Date : 8 de junio de 2006
- Copyrigth Notice : Fintravalores S.A. S.A
-->
<%--
-@(#)
--Descripcion : Pagina JSP, que sirve para ingresar en el archivo de presupuesto de gastos administartivos.
--%>

<%@page contentType="text/html"%>
<%@page session="true"%>
<%@page import="java.util.*"%>
<%@page import="com.tsp.operation.model.*"%>
<%@page import="com.tsp.operation.model.beans.Usuario"%>
<%@page import="com.tsp.finanzas.contab.model.*"%>
<%@page import="com.tsp.finanzas.contab.model.beans.*"%>
<%@page import="com.tsp.operation.controller.*"%>
<%@page import="com.tsp.util.*"%>
<%@include file="/WEB-INF/InitModel.jsp"%>
<%@ taglib uri="http://jakarta.apache.org/taglibs/input-0.90" prefix="input" %>
<%@page errorPage="/error/ErrorPage.jsp"%>
<%String path = application.getRealPath(model.menuService.getRealUrl(request.getRequestURI()));
  String datos[] = model.menuService.getContenidoMenu(BASEURL,request.getRequestURI(),path);
%>
<html>
<head>
<title>Tipos de Comprobantes y Series</title>
<link href="<%= BASEURL %>/css/estilostsp.css" rel='stylesheet'>
<link href="/css/estilostsp.css" rel="stylesheet" type="text/css">
<link href="/css/estilostsp.css" rel="stylesheet" type="text/css">
</head>
<script type='text/javascript' src="<%= BASEURL %>/js/validar.js"></script>
<script src='<%= BASEURL %>/js/date-picker.js'></script>
<script type='text/javascript' src="<%= BASEURL %>/js/boton.js"></script>
<script type='text/javascript' src="<%= BASEURL %>/js/funciones.js"></script>
<script>
	function habilitarSerie(obj){
		if( obj.value == 'S' ){
			forma.serie_ini.readOnly = false;
			forma.serie_fin.readOnly = false;
			forma.long_serie.readOnly = false;
		} else {
			forma.serie_ini.readOnly = true;
			forma.serie_fin.readOnly = true;
			forma.long_serie.readOnly = true;
			forma.serie_ini.value = '';
			forma.serie_fin.value = '';
			forma.long_serie.value = '';
		}
	}
	
	function validar(obj){
	
		if( obj.mserie.value == "S" ){
			if( isNaN(obj.serie_fin.value) ){
				alert('El valor del campo debe ser n�merico');
				obj.serie_fin.focus();
				return false;
			} else if ( isNaN(obj.serie_ini.value)) {
				alert('El valor del campo debe ser n�merico');
				obj.serie_ini.focus();
				return false;
			}  else if ( isNaN(obj.long_serie.value)) {
				alert('El valor del campo debe ser n�merico');
				obj.long_serie.focus();
				return false;
			}				
			
			var sfin = parseInt(obj.serie_fin.value);
			var sini = parseInt(obj.serie_ini.value);
			
			if( sfin<sini ){
				alert('El valor de la serie final no debe ser inferior al de la serie inicial.');
				obj.serie_fin.focus();
				return false;
			}
			
			if( CamposLlenos(obj) ) return true;
			else return false;
			
		} else {
			return CamposLlenos2(obj);
		}
	}
	
	function CamposLlenos(form){
		 for (i = 0; i < form.elements.length; i++){
			if (form.elements[i].value == "" && form.elements[i].name!="preano"
					&& form.elements[i].name!="premes"){
			  alert("No se puede procesar la informacion. Verifique que todos lo campos esten llenos.");
			  form.elements[i].focus();
			  return (false);
			}
		 }
		 
		 return true;
	}
	
	function CamposLlenos2(form){
		 for (i = 0; i < form.elements.length; i++){
			if (form.elements[i].value == "" && form.elements[i].name!="preano"
					&& form.elements[i].name!="premes"
					&& form.elements[i].name!="serie_ini"
					&& form.elements[i].name!="serie_fin"
					&& form.elements[i].name!="long_serie"){
			  alert("No se puede procesar la informacion. Verifique que todos lo campos esten llenos.");
			  form.elements[i].focus();
			  return (false);
			}
		 }
		 
		 return true;
	}
</script>
<body onresize="redimensionar()" onload = 'redimensionar(); Llenar(forma.ano,null);'>
<div id="capaSuperior" style="position:absolute; width:100%; height:100px; z-index:0; left: 0px; top: 0px;">
 <jsp:include page="/toptsp.jsp?encabezado=Tipos de Comprobantes y Series"/>
</div>
<div id="capaCentral" style="position:absolute; width:100%; height:87%; z-index:0; left: 0px; top: 100px; overflow: scroll;">
<%
  TreeMap cint = new TreeMap();
  TreeMap terc = new TreeMap();
  TreeMap mserie= new TreeMap();
  TreeMap prefa= new TreeMap();
  TreeMap prefm= new TreeMap();
  TreeMap dia= new TreeMap();
  
  cint = modelcontab.tipo_doctoSvc.getTreemap();
  
  prefa.put(" NINGUNO", "");
  prefa.put("A", "A");
  prefa.put("AA", "AA");
  prefa.put("AAAA", "AAAA");
  
  prefm.put(" NINGUNO","");
  prefm.put("MM","MM");
  
  terc.put("MANDATORIO","M");
  terc.put("NO APLICA","N");
  terc.put("OPCIONAL","O");
  
  mserie.put("SI","S");
  mserie.put("NO","N");
  
  dia.put("SI","S");
  dia.put("NO","N");
  
  Tipo_Docto obj = (Tipo_Docto) request.getAttribute("TDocto");
%>
<form name="forma" id="forma" method="post" action="<%=CONTROLLERCONTAB%>?estado=TipoDocto&accion=Insert">
  <table width="726"  border="2" align="center" cellpadding="0" cellspacing="0">
    <tr>
      <td><table width="100%" class="tablaInferior">
        <tr>
          <td colspan="4" ><table width="100%"  border="0" cellpadding="0" cellspacing="1" class="barratitulo">
              <tr>
                <td width="50%" class="subtitulo1">Tipo de Comprobante</td>
                <td width="50%" class="barratitulo"><img align="left" src="<%=BASEURL%>/images/titulo.gif" width="32" height="20"><%=datos[0]%></td>
              </tr>
          </table></td>
        </tr>
		<tr class="fila">
		  <td> C&oacute;digo</td>
		  <td ><input name="code" type="text" id="code"  onKeyPress="soloAlfa(event)" value="<%= obj.getCodigo() %>" size="8" maxlength="5"></td>
		  <td width="19%" >C&oacute;digo Interno</td>
		  <td width="47%" ><input:select name="cod_interno" attributesText="class=textbox" options="<%= cint%>" default=""/>
		  <script>document.forma.cod_interno.value = '<%= obj.getCodigo_interno()%>';</script></td>
		</tr>
		<tr class="fila">
		  <td>Descripci&oacute;n</td>
		  <td colspan="3" ><input name="desc" type="text" id="desc2" value="<%= obj.getDescripcion() %>" size="50" maxlength="30"></td>
		  </tr>
		<tr class="fila">
          <td width="22%">Tercero</td>      
          <td width="12%" ><input:select name="tercero" attributesText="class=textbox" options="<%= terc %>" default=""/></td>
		  <script>document.forma.tercero.value = '<%= obj.getTercero()%>';</script>
		  <td >Comprobante Diario</td>
		  <td ><input:select name="diario" attributesText="class=textbox" options="<%= dia%>" default=""/>
		  <script>document.forma.diario.value = '<%= obj.getEsDiario()%>';</script></td>
		</tr>
		  <tr class="fila">
		    <td colspan="4" class="subtitulo1">Serie</td>
		    </tr>
		  <tr class="fila">
          <td>Maneja Serie </td>      
          <td ><input:select name="mserie" attributesText="class=textbox onChange='habilitarSerie(this);'" options="<%= mserie%>" default=""/>
		  </td>
		  <td >Serie Inicial </td>
		  <td ><input name="serie_ini" type="text" id="serie_ini" onKeyPress="soloDigitos(event,'decNo')" value="<%= obj.getSerie_ini() %>" size="8" maxlength="4" readonly></td>
		  </tr>
		   <tr class="fila">
		     <td>Serie final </td>
		     <td ><input name="serie_fin" type="text" id="serie_fin" onKeyPress="soloDigitos(event,'decNo')" value="<%= obj.getSerie_fin()%>" size="8" maxlength="4" readonly></td>
	         <td >Longitud de la serie </td>
	         <td ><input name="long_serie" type="text" id="long_serie" onKeyPress="soloDigitos(event,'decNo')" value="<%= obj.getLong_serie() %>" size="6" maxlength="2" readonly>
			 <script>document.forma.mserie.value = '<%= obj.getManeja_serie()%>';habilitarSerie(document.forma.mserie);</script></td>
		   </tr>
		   <tr class="fila">
		     <td colspan="4" class="subtitulo1">Prefijo</td>
	        </tr>
		   <tr class="fila">
		     <td>Prefijo</td>
		     <td ><input name="prefijo" type="text" id="prefijo" onKeyPress="soloAlfa(event)" value="<%= obj.getPrefijo() %>" size="6" maxlength="3"></td>
		     <td >Prefijo A&ntilde;o </td>
		     <td ><input:select name="preano" attributesText="class=textbox" options="<%= prefa%>" default=""/>
			 <script>document.forma.preano.value = '<%= obj.getPrefijo_anio()%>';</script></td>
	        </tr>
		   <tr class="fila">
		     <td>Prefijo Mes</td>
		     <td ><input:select name="premes" attributesText="class=textbox" options="<%= prefm%>" default=""/>
			 <script>document.forma.premes.value = '<%= obj.getPrefijo_mes()%>';</script></td>
		     <td colspan="2" >&nbsp;</td>
	        </tr>

      </table></td>
    </tr>
  </table>
  <br>
  <% if( request.getParameter("msg")!=null ){%>
  <p>
  <table width="662" border="2" align="center">
    <tr>
      <td><table width="100%"  border="1" align="center"  bordercolor="#F7F5F4" bgcolor="#FFFFFF">
          <tr>
            <td align="center" class="mensajes"><%=request.getParameter("msg").toString()%></td>
            <td width="29" background="<%=BASEURL%>/images/cuadronaranja.JPG">&nbsp;</td>
            <td width="58">&nbsp;</td>
          </tr>
      </table></td>
    </tr>
  </table>
  <p></p>
  <%} %>
  <br>
  <center>
    <img src="<%=BASEURL%>/images/botones/aceptar.gif" name="c_aceptar" onClick="if(validar(forma)) forma.submit();" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);"> 
	<img src="<%=BASEURL%>/images/botones/cancelar.gif" name="c_cancelar" onClick="forma.reset();" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);"> 
	<img src="<%=BASEURL%>/images/botones/salir.gif" name="c_salir" onClick="parent.close();" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);">
  </center>
</form>
</div>
<%=datos[1]%>
</body>
</html>

