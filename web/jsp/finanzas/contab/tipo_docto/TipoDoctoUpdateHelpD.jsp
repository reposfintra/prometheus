<%@include file="/WEB-INF/InitModel.jsp"%>
<html>
<head>
<title>Descripcion CamposTipo de Comprobantes y Series </title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link href="/css/estilostsp.css" rel="stylesheet" type="text/css">
<link href="<%= BASEURL %>/css/estilostsp.css" rel='stylesheet'>
</head>

<body>
<br>


<table width="696"  border="2" align="center">
  <tr>
    <td width="811" >
      <table width="100%" border="0" align="center">
        <tr  class="subtitulo">
          <td height="24" colspan="2"><div align="center">Tipo de Comprobantes y Series </div></td>
        </tr>
        <tr class="subtitulo1">
          <td colspan="2">Tipo de Comprobantes y Series </td>
        </tr>
        <tr>
          <td width="149" class="fila">C&oacute;digo</td>
          <td width="525"  class="ayudaHtmlTexto">Campo donde se muestra el c&oacute;digo del tipo de comprobante. Este campo no es modificable. </td>
        </tr>
        <tr>
          <td class="fila">C&oacute;digo Interno</td>
          <td  class="ayudaHtmlTexto">Campo donde se se selecciona el codigo de documento interno.</td>
        </tr>
        <tr>
          <td class="fila">Descripci&oacute;n</td>
          <td  class="ayudaHtmlTexto">Campo donde se digita la descripci&oacute;n. </td>
        </tr>
        <tr>
          <td class="fila">Tercero</td>
          <td  class="ayudaHtmlTexto">Campo donde se selecciona si debe exigir nit o cedula del tercero al momento de grabar el comprobante. </td>
        </tr>
        <tr>
          <td class="fila">Comprobante Diario</td>
          <td  class="ayudaHtmlTexto">Campo donde se selecciona si el tipo de comprobante es diario o no. </td>
        </tr>
        <tr>
          <td class="fila">Maneja Serie </td>
          <td  class="ayudaHtmlTexto">Campo donde se selecciona si este tipo de documento debe manejar serie o se debe digitar manualmente.</td>
        </tr>
        <tr>
          <td class="fila">Serie Inicial </td>
          <td  class="ayudaHtmlTexto">Campo donde se digita el valor n&uacute;merico correspondiente </td>
        </tr>
        <tr>
          <td class="fila">Serie Final </td>
          <td  class="ayudaHtmlTexto">Campo donde se digita el valor n&uacute;merico correspondiente </td>
        </tr>
        <tr>
          <td class="fila">Longitud de la Serie </td>
          <td  class="ayudaHtmlTexto">Campo donde se digita el valor n&uacute;merico correspondiente </td>
        </tr>
        <tr>
          <td class="fila">Prefijo</td>
          <td  class="ayudaHtmlTexto">Campo donde se digita el prefijo general. </td>
        </tr>
        <tr>
          <td class="fila">Prefijo A&ntilde;o </td>
          <td  class="ayudaHtmlTexto">Campo donde se selecciona el prefijo del a&ntilde;o. </td>
        </tr>
        <tr>
          <td class="fila">Prefijo Mes </td>
          <td  class="ayudaHtmlTexto">Campo donde se selecciona el prefijo del mes. </td>
        </tr>
        <tr>
          <td class="fila"><div align="center">
            <input type="image" src = "<%=BASEURL%>/images/botones/modificar.gif" style="cursor:default ">
          </div></td>
          <td  class="ayudaHtmlTexto">Bot&oacute;n que valida la informaci&oacute;n digitada en los campos para modificar tipo de comprobante. </td>
        </tr>
        <tr>
          <td class="fila"><div align="center">
            <input type="image" src = "<%=BASEURL%>/images/botones/anular.gif" style="cursor:default ">
          </div></td>
          <td  class="ayudaHtmlTexto"> Bot&oacute;n que anula el la informaci&oacute;n del tipo de comprobante. </td>
        </tr>
        <tr>
          <td class="fila"><div align="center">
            <input type="image" src = "<%=BASEURL%>/images/botones/salir.gif" style="cursor:default ">
          </div></td>
          <td  class="ayudaHtmlTexto">Bot&oacute;n que cierra  la ventana. </td>
        </tr>
        
      </table>
    </td>
  </tr>
</table>
<p>&nbsp;</p>
<p>&nbsp;</p>

<p>&nbsp;</p>
<p>&nbsp; </p>
</body>
</html>
