<!--  
	 - Author(s)       :      LREALES
	 - Description     :      AYUDA DESCRIPTIVA - Reporte de Control
	 - Date            :      18/09/2006 
	 - Copyright Notice:      TRANSPORTES SANCHEZ POLO S.A.
-->
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN""http://www.w3.org/TR/html4/loose.dtd">
<%@page session="true"%> 
<%@page errorPage="/error/ErrorPage.jsp"%>
<%@page import="com.tsp.operation.model.beans.*, java.util.*"%>
<%@ include file="/WEB-INF/InitModel.jsp"%>
<%@ taglib uri="http://jakarta.apache.org/taglibs/input-0.90" prefix="input" %>
<html>
<head>
<title>AYUDA DESCRIPTIVA - Reporte de Control</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link href="<%= BASEURL %>/css/estilostsp.css" rel="stylesheet" type="text/css">
<script type='text/javascript' src="<%= BASEURL %>/js/boton.js"></script>
</head>

<body>
<br>
<table width="696"  border="2" align="center">
  <tr>
    <td width="811" >
      <table width="100%" border="0" align="center">
        <tr  class="subtitulo">
          <td height="24" colspan="2"><div align="center">Contabilidad</div></td>
        </tr>
        <tr class="subtitulo1">
          <td colspan="2">Reporte de Control - Pantalla Preliminar</td>
        </tr>
		<tr class="subtitulo1">
          <td colspan="2">INFORMACION DEL REPORTE DE CONTROL </td>
        </tr>
        <tr>
          <td  class="fila">'Nombre del Archivo' </td>
          <td  class="ayudaHtmlTexto">Campo de texto donde se carga el nombre del archivo escogido en la lista de busqueda especifica. </td>
        </tr>
        <tr>
          <td class="fila">Lupa &oacute; Bot&oacute;n de Busqueda </td>
          <td  class="ayudaHtmlTexto">Bot&oacute;n para realizar la busqueda especifica, del nombre del archivo al que quiero realizarle el reporte de control. </td>
        </tr>
        <tr>
          <td class="fila">Bot&oacute;n Exportar </td>
          <td  class="ayudaHtmlTexto">Bot&oacute;n que permite realizar la exportacion a un archivo excel del reporte de control. </td>
        </tr>
		<tr>
          <td width="200" class="fila">Bot&oacute;n Salir</td>
          <td width="474"  class="ayudaHtmlTexto">Bot&oacute;n para salir de la vista 'Reporte de Control' y volver a la vista del men&uacute;.</td>
        </tr>
      </table>
    </td>
  </tr>
</table>
<p></p>
<center>
<img src = "<%=BASEURL%>/images/botones/salir.gif" style = "cursor:hand" name = "imgsalir" onClick = "parent.close();" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);">
</center>
</body>
</html>
