<!--
- Nombre P�gina :                  verNombresArchivos.jsp                
- Descripci�n :                    Pagina JSP, muestra la lista de los archivos del usuario en session.            
- Autor :                          LREALES                        
- Fecha Creado :                   18 de Septiembre de 2006, 09:40 A.M.                        
- Versi�n :                        1.0                                       
- Copyright :                      Fintravalores S.A.                   
-->

<%-- Declaracion de librerias--%>
<%@page contentType="text/html"%>
<%@page session="true"%>
<%@page errorPage="../error/ErrorPage.jsp"%>
<%@page import="java.util.*"%>
<%@page import="com.tsp.operation.model.*"%>
<%@page import="com.tsp.util.*"%>
<%@page import="com.tsp.operation.model.beans.*, java.util.*"%>
<%@include file="/WEB-INF/InitModel.jsp"%>
<%@taglib uri="http://jsptags.com/tags/navigation/pager" prefix="pg" %>
<%@page import="java.util.Date" %>  
<%@page import="java.text.*" %>
<%@page import="com.tsp.util.Utility"%>

<html>
<head>
<title>Ver Nombres Archivos</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link href="<%= BASEURL %>/css/estilostsp.css" rel="stylesheet" type="text/css"> 
<link href="../../../css/estilostsp.css" rel="stylesheet" type="text/css"> 
<script type='text/javascript' src="<%= BASEURL %>/js/reporte.js"></script> 
<script type='text/javascript' src="<%= BASEURL %>/js/boton.js"></script>
<script language="JavaScript" type="text/javascript" src="<%=BASEURL%>/js/Validaciones.js"></script>
</head>
<body onresize="redimensionar()" onload = 'redimensionar()'>
<div id="capaSuperior" style="position:absolute; width:100%; height:100px; z-index:0; left: 0px; top: 0px;">
	<jsp:include page="/toptsp.jsp?encabezado=Reporte de Control"/>
</div>
<div id="capaCentral" style="position:absolute; width:100%; height:87%; z-index:0; left: 0px; top: 100px; overflow: scroll;">
	  <% 
	  ListadoArchivos directorio =  model.DirectorioSvc.getDirectorio();
      List            listado    = directorio.getListado();
	  
	  if( listado != null && listado.size() > 0 ) {
          Iterator it = listado.iterator(); 
	  %>
 <table width="95%" border="2" align=center>
    <tr>
        <td>
            <table width="100%" align="center" class="tablaInferior"> 
                <tr>
                    <td colspan='2'>                                                
                        <table width="100%"  border="0" cellpadding="0" cellspacing="0" class="barratitulo">
                            <tr>
                                <td width="50%" class="subtitulo1" colspan='3'>Informaci&oacute;n</td>
                                <td width="50%" class="barratitulo" colspan='2'><img src="<%=BASEURL%>/images/titulo.gif"></td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td>
					<div class="scroll" style=" overflow:auto ; WIDTH: 100%; HEIGHT:100%; " >
                        <table width="100%" border="1" align="center" bordercolor="#999999" bgcolor="#F7F5F4">
                            <tr class="tblTitulo">
                                <td nowrap width="60%"><div align="center">Archivo</div></td>
                                <td nowrap width="30%"><div align="center">Fecha</div></td>
                                <td nowrap width="10%"><div align="center">Tama&ntilde;o</div></td>
                            </tr>
							
							<%  
								int ele=1;
								while( it.hasNext() ) {
								    Archivo datos = (Archivo) it.next();
							%>
							
							<tr id='fila<%=ele%>' class="<%=(ele % 2 == 0 )?"filagris":"filaazul"%>" onMouseOver='cambiarColorMouse(this)' style="cursor:hand" onClick="window.opener.document.form2.archivo.value='<%=datos.getName()%>'; window.close();">
							<td  nowrap align="left" abbr="" class="bordereporte">&nbsp;<%=datos.getName()%></td>
							<td  align="center" abbr="" nowrap class="bordereporte">&nbsp;<%=Utility.convertirMilesgdos(datos.getFecha())%></td>
							<td  align="right" abbr="" nowrap class="bordereporte">&nbsp;<%=String.valueOf(datos.getTama�o()/1024)%>&nbsp;KB</td>
							</tr>
						  
								<%
								ele++;
								}
								%>
								
                        </table>
					  </div>
                    </td>
                </tr>
            </table>
        </td>
    </tr>
 </table>
 <br>
	<%}
	 else { %>                    
	  <%out.print("<table width=379 border=2 align=center><tr><td><table width=100%  border=1 align=center  bordercolor=#F7F5F4 bgcolor=#FFFFFF><tr><td width=350 align=center class=mensajes>NO PRESENTAS ARCHIVOS DISPONIBLES!</td><td width=100><img src="+BASEURL+"/images/cuadronaranja.JPG></td></tr></table></td></tr></table>");%>
	  <br>
	<%}%>

 <table width='95%' align=center>
  <tr class="titulo">
    <td align=right colspan='2'>
        <div align="center">
		<img src="<%=BASEURL%>/images/botones/salir.gif"  name="imgsalir"  onMouseOver="botonOver(this);" onClick="parent.close();" onMouseOut="botonOut(this);" style="cursor:hand">
        </div></td>
  </tr>
</table>
</div>
</body>
</html>