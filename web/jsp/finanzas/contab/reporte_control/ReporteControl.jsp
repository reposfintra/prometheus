<!--
- Nombre P�gina :                  ReporteControl.jsp                
- Descripci�n :                    Pagina JSP, genera el reporte de control de contabilidad.             
- Autor :                          LREALES                        
- Fecha Creado :                   22 de Agosto de 2006, 09:04 A.M.                        
- Versi�n :                        1.0                                       
- Copyright :                      Fintravalores S.A.                   
-->

<%-- Declaracion de librerias--%>
<%@page contentType="text/html"%>
<%@ page session="true"%>
<%@ page errorPage="../error/error.jsp"%>
<%@page import="com.tsp.operation.model.beans.*, java.util.*"%>
<%@ include file="/WEB-INF/InitModel.jsp"%>
<%@page import="com.tsp.operation.model.*"%>
<%@page import="com.tsp.util.*"%>
<%@ taglib uri="http://jakarta.apache.org/taglibs/input-0.90" prefix="input" %>
<%
  String path = application.getRealPath(model.menuService.getRealUrl(request.getRequestURI()));
  String datos[] = model.menuService.getContenidoMenu(BASEURL,request.getRequestURI(),path);
%>
<html>
<head><title>Reporte de Control del Reporte de Utilidad</title>
<link href="<%= BASEURL %>/css/estilostsp.css" rel="stylesheet" type="text/css">
<script src='<%=BASEURL%>/js/date-picker.js'></script>
<script type='text/javascript' src="<%= BASEURL %>/js/boton.js"></script>
<script type='text/javascript' src="<%= BASEURL %>/js/validar.js"></script>
</head>
<body onLoad="redimensionar();" onResize="redimensionar();">
<div id="capaSuperior" style="position:absolute; width:100%; height:100px; z-index:0; left: 0px; top: 0px;">
<jsp:include page="/WEB-INF/toptsp.jsp?encabezado=Reporte de Control"/>
</div>

<div id="capaCentral" style="position:absolute; width:100%; height:85%; z-index:0; left: 0px; top: 100px; overflow: scroll;"> 
<%

    Usuario usuario = (Usuario) session.getAttribute("Usuario");   
	
%>
<script>
	var controlador ="<%=CONTROLLER%>";
	
	function Exportar ( archivo ){
		
		if( ( archivo != "" ) ){
						
			document.form2.action = controlador + "?estado=Reporte&accion=Control";
			document.form2.submit();
						
			alert( 
		
				'Puede observar el seguimiento del proceso en la siguiente ruta del menu:' + '\n' + '\n' +
				'SLT -> PROCESOS -> Seguimiento de Procesos' + '\n' + '\n' + '\n' +
				'Y puede observar el archivo excel generado, en la siguiente parte:' + '\n' + '\n' +
				'En la pantalla de bienvenida de la Pagina WEB,' + '\n' +
				'vaya a la parte superior-derecha,' + '\n' +
				'y presione un click en el icono que le aparece.'
				
			);
		
		} else{
		
			alert( "Por Favor No Deje Los Campos Obligatorios Vacios!" );
			
		}
	}
</script>
<form name="form2" method="post">
    <table width="650" border="2" align="center">
      <tr>
        <td><table width="100%" align="center"  class="tablaInferior">
            <tr>
				<td width="30%" colspan="1" class="subtitulo1">Reporte de Control</td>
				<td width="70%" colspan="1" class="barratitulo"><img src="<%=BASEURL%>/images/titulo.gif" width="32" height="20" align="left"><%=datos[0]%></td>
            </tr>
            <tr class="fila">
              <td width="30%" align="left" valign="middle" >Nombre del Archivo</td>
              <td width="70%" align="left" valign="middle" >
			    <span class="Letras">
			      <input name="archivo" type="text" class="textbox" id="archivo" size="50" readonly>
				  <img src="<%=BASEURL%>/images/botones/iconos/obligatorio.gif">
                  <img src="<%=BASEURL%>/images/botones/iconos/lupa.gif" alt="Buscar Nombre del Archivo.." name="imglupa" width="20" height="20" style="cursor:hand" onClick="window.open('<%=CONTROLLER%>?estado=Ver&accion=NombresArchivos','','status=no,scrollbars=no,width=700,height=470,resizable=yes');" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);">				
				</span>
			  </td>    
            </tr>
        </table></td>
      </tr>
    </table>
		
<p>
<div align="center">
<p><img src="<%=BASEURL%>/images/botones/exportarExcel.gif" style="cursor:hand" title="Generar Reporte.." name="exportar"  onClick="Exportar ( archivo.value );" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);">&nbsp;
   <img src="<%=BASEURL%>/images/botones/salir.gif"  name="salir" style="cursor:hand" title="Salir al Menu Principal" onMouseOver="botonOver(this);" onClick="parent.close();" onMouseOut="botonOut(this);" ></p>
  <%String msg = request.getParameter("msg");
	if ( msg!=null && !msg.equals("") ){%>
                        
	  <table border="2" align="center">
   		<tr>
        	<td>
            	<table width="100%"  border="1" align="center"  bordercolor="#F7F5F4" bgcolor="#FFFFFF">
                	<tr>
                    	<td width="229" align="center" class="mensajes"><%=msg%></td>
                        <td width="29" background="<%=BASEURL%>/images/cuadronaranja.JPG">&nbsp;</td>
                        <td width="58">&nbsp;</td>
                    </tr>
                </table>
            </td>
		</tr>
	  </table>
	<%}%>
  </p>
</div>
</p>
</form>
</div> 
<%=datos[1]%>
</body>
</html>