<!--  
	 - Author(s)       :      LREALES
	 - Description     :      AYUDA FUNCIONAL - Reporte de Control
	 - Date            :      18/09/2006 
	 - Copyright Notice:      TRANSPORTES SANCHEZ POLO S.A.
-->
<%@ include file="/WEB-INF/InitModel.jsp"%>
<HTML>
<HEAD>
<TITLE>AYUDA FUNCIONAL - Reporte de Control</TITLE>
<META http-equiv=Content-Type content="text/html; charset=windows-1252">
<link href="../css/estilostsp.css" rel="stylesheet" type="text/css"> 
<link href="<%=BASEURL%>/css/estilostsp.css" rel="stylesheet" type="text/css">
<script type='text/javascript' src="<%= BASEURL %>/js/boton.js"></script> 
</HEAD>
<BODY> 
<% String BASEIMG = BASEURL +"/images/ayuda/contabilidad/reporte_control/"; %>
  <table width="95%"  border="2" align="center">
    <tr>
      <td height="117" >
        <table width="100%" border="0" align="center">
          <tr  class="subtitulo">
            <td height="20"><div align="center">MANUAL DE CONTABILIDAD WEB</div></td>
          </tr>
          <tr class="subtitulo1">
            <td>Descripci&oacute;n del funcionamiento del programa para generar el Reporte de Control.</td>
          </tr>
          <tr>
            <td  class="ayudaHtmlTexto"><div align="center">
              <p>&nbsp;</p>
              </div></td>
          </tr>
          <tr>
            <td  class="ayudaHtmlTexto"><p>En la siguiente pantalla se puede escoger el nombre del archivo del Reporte de Utilidad al cual se le desea hacer el Reporte de Control.</p>            </td>
          </tr>
          <tr>
            <td  class="ayudaHtmlTexto"><div align="center"><IMG height=172 src="<%=BASEIMG%>image001.JPG" width=833 border=0 v:shapes="_x0000_i1143"></div></td>
          </tr>
          <tr>
            <td  class="ayudaHtmlTexto"><p>&nbsp;</p>
            <p>El sistema verifica que todos los campos obligatorios esten llenos, si no lo estan le saldr&aacute; en la pantalla el siguiente mensaje. </p></td>
          </tr>
          <tr>
            <td  class="ayudaHtmlTexto"><div align="center"><IMG height=125 src="<%=BASEIMG%>image_error001.JPG" width=320 border=0 v:shapes="_x0000_i1054"></div></td>
          </tr>
<tr>
  <td  class="ayudaHtmlTexto"><p>&nbsp;</p>
    <p>Al darle click a la lupa o bot&oacute;n de busqueda, en la pantalla le aperecera la siguiente ventana donde usted podr&aacute; escoger el archivo que desea realizarle el reporte de control. </p></td>
</tr>
<tr>
  <td  class="ayudaHtmlTexto"><div align="center"><IMG height=180 src="<%=BASEIMG%>image002.JPG" width=661 border=0 v:shapes="_x0000_i1054"></div></td>
</tr>
<tr>
  <td  class="ayudaHtmlTexto"><p>&nbsp;</p>
    <p>Si en esa ventana le aparece el siguiente mensaje, es por que usted no ha generado reportes de utilidad y por lo tanto no podr&aacute; hacerles reporte de control. </p></td>
</tr>
<tr>
  <td  class="ayudaHtmlTexto"><div align="center"><IMG height=122 src="<%=BASEIMG%>image_error002.JPG" width=417 border=0 v:shapes="_x0000_i1054"></div></td>
</tr>
<tr>
  <td  class="ayudaHtmlTexto"><p>&nbsp;</p>
    <p>Cuando alla echo el procedimiento correctamente, en la pantalla el sistema nos mostrar&aacute; el siguiente mensaje. Indicandonos donde podr&aacute; observar el reporte de control generado. </p></td>
</tr>
<tr>
  <td  class="ayudaHtmlTexto"><div align="center"><IMG height=224 src="<%=BASEIMG%>image003.JPG" width=424 border=0 v:shapes="_x0000_i1054"></div></td>
</tr>
<tr>
  <td  class="ayudaHtmlTexto"><p><br>
  Y
      el proceso de Reporte de Control se estar&aacute; ejecutando internamente, como lo indica el siguiente mensaje. </p>    </td>
</tr>
<tr>
  <td  class="ayudaHtmlTexto"><div align="center"><IMG height=99 src="<%=BASEIMG%>image004.JPG" width=406 border=0 v:shapes="_x0000_i1054"></div></td>
</tr>
      </table></td>
    </tr>
  </table>
  <p></p>
<center>
<img src = "<%=BASEURL%>/images/botones/salir.gif" style = "cursor:hand" name = "imgsalir" onClick = "parent.close();" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);">
</center>
</BODY>
</HTML>
