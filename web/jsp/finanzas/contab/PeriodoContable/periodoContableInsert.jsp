<!--
- Autor : Osvaldo P�rez Ferrer
- Date  : 17 de Junio de 2006
- Copyrigth Notice : Fintravalores S.A. S.A
-->
<%--
-@(#)
--Descripcion : Pagina JSP, que sirve para Ingresar Periodos Contables.
--%>
<%@page contentType="text/html"%>
<%@page session="true"%>
<%@page import="com.tsp.operation.controller.*"%>
<%@page import="com.tsp.util.*"%>
<%@include file="/WEB-INF/InitModel.jsp"%>
<%@ taglib uri="http://jakarta.apache.org/taglibs/input-0.90" prefix="input" %>

<html>
<head>

<title>Ingresar Periodo Contable</title>
<link href="<%= BASEURL %>/css/estilostsp.css" rel='stylesheet'>
<link href="../../../../css/estilostsp.css" rel="stylesheet" type="text/css">
<link href="/css/estilostsp.css" rel="stylesheet" type="text/css">
<script type='text/javascript' src="<%= BASEURL %>/js/finanzas/contab/validacionesPeriodoContable.js"></script>
<script src='<%= BASEURL %>/js/date-picker.js'></script>
<script type='text/javascript' src="<%= BASEURL %>/js/boton.js"></script>
<script type='text/javascript' src="<%= BASEURL %>/js/boton.js"></script>

<%
String mensaje = (String) request.getAttribute("mensaje");
int anio = Util.AnoActual();
String mes = Util.mesFormat(Util.MesActual());
String hoy = Utility.getHoy("-");

String path = application.getRealPath(model.menuService.getRealUrl(request.getRequestURI()));
    String datos[] = model.menuService.getContenidoMenu(BASEURL,request.getRequestURI(),path);
%>
</head>


<body onresize="redimensionar()" onload = 'redimensionar()'>
<div id="capaSuperior" style="position:absolute; width:100%; height:100px; z-index:0; left: 0px; top: 1px;">
<jsp:include page="/toptsp.jsp?encabezado=Periodo Contable"/></div>
<div id="capaCentral" style="position:absolute; width:100%; height:87%; z-index:0; left: -5px; top: 101px; overflow: scroll;">

<form name="form1" id="form1" method="post" action="<%=CONTROLLERCONTAB%>?estado=Periodo&accion=Contable&opcion=add">
  
  <table width="380" height="261" border="2" align="center" cellpadding="3" cellspacing="2" onclick="cuadromsg.style.visibility='hidden';">
    <tr>
      <td width="444" >	  
	  
	    
	    <table width="370" height="248" class="tablainferior" align="center">
			<tr class="fila">
          <td colspan="2" class="subtitulo1">Ingresar Periodo Contable</td>
          <td align="left" class="barratitulo"><%=datos[0]%><img src="<%=BASEURL%>//images/titulo.gif"></td>
          </tr>
          <tr class="fila" >
            <td width="27%">A�o :</td>
            <td colspan="2">
                <select id="anio" name="anio" style="width:60px" onchange="updFechas()">                    
                    <option value="<%=anio-1%>"><%=anio-1%></option>
                    <option value="<%=anio%>" selected><%=anio%></option>
                    <option value="<%=anio+1%>"><%=anio+1%></option>                                        
                </select> 
            </td>     
			                       
          </tr>
          <tr class="fila">
            <td width="27%">Mes :</td>
            <td colspan="2">
                <select id="mes" name="mes" style="width:60px" onchange="updFechas()">                                        
                    <option value="01">Ene</option>
                    <option value="02">Feb</option>
                    <option value="03">Mar</option>                                        
                    <option value="04">Abr</option>
                    <option value="05">May</option>
                    <option value="06">Jun</option>
                    <option value="07">Jul</option>
                    <option value="08">Ago</option>
                    <option value="09">Sep</option>
                    <option value="10">Oct</option>
                    <option value="11">Nov</option>
                    <option value="12">Dic</option>
                    <option value="13">Fis</option>
                </select> 
            </td>                            
          </tr>
          <tr class="fila">
            <td>Abierto:</td>
            <td colspan="2"> Si
              <input name="abierto" type="radio" value="A"> 
              No 
              <input name="abierto" type="radio" value="C" checked>           
			</td>
          
            
          </tr>
		  <tr class="fila"><td height="25" colspan="2"><div align="center">Impuestos</div></td><td height="25"><div align="center">Fecha de presentaci&oacute;n</div></td>
		  </tr>
          <tr class="fila">
            <td >IVA:</td>
			<td width="23%" ><div align="center">Si
			      <input id="iva" name="iva" type="radio" value="S" onclick="disableFecha(this,1);"> 
			    No
			    <input id="iva" name="iva" type="radio" value="N" checked onclick="disableFecha(this,1);"> 
		      </div></td>
			    <td width="50%"><div align="center">
		          <input type="text" name="fpiva" id="fpiva" size="10" readonly value="<%=hoy%>" disabled> 
				  <a id="cal1" href="javascript:void(0)" onclick="if(self.gfPop)gfPop.fPopCalendar(document.form1.fpiva);return false;" HIDEFOCUS><img src="<%=BASEURL%>\js\Calendario\cal.gif" width="16" height="16" border="0" alt="Click aqui para escoger la fecha"></a>
		          </input>
		        </div></td>
			</tr>
          <tr class="fila">
            <td >Retencion:</td>
			<td ><div align="center">Si
			      <input id="retencion" name="retencion" type="radio" value="S" onclick="disableFecha(this,2);"> 
			    No
			    <input id="retencion" name="retencion" type="radio" value="N" checked onclick="disableFecha(this,2);"> 
		      </div></td>
			    <td width="50%"><div align="center">
		          <input type="text" name="fpret" id="fpret" size="10" readonly value="<%=hoy%>" disabled>
				  <a href="javascript:void(0)" onclick="if(self.gfPop)gfPop.fPopCalendar(document.form1.fpret);return false;" HIDEFOCUS><img src="<%=BASEURL%>\js\Calendario\cal.gif" width="16" height="16" border="0" alt="Click aqui para escoger la fecha"></a>
		          </input>
		        </div></td>
          </tr>
		   <tr class="fila">
            <td >Comercio:</td>
			<td ><div align="center">Si
			      <input id="comercio" name="comercio" type="radio" value="S" onclick="disableFecha(this,3);"> 
			    No
			    <input id="comercio" name="comercio" type="radio" value="N" checked onclick="disableFecha(this,3);"> 
		      </div></td>
			    <td width="50%"><div align="center">
		          <input type="text" name="fpcom" id="fpcom" size="10" readonly value="<%=hoy%>" disabled>
				  <a href="javascript:void(0)" onclick="if(self.gfPop)gfPop.fPopCalendar(document.form1.fpcom);return false;" HIDEFOCUS><img src="<%=BASEURL%>\js\Calendario\cal.gif" width="16" height="16" border="0" alt="Click aqui para escoger la fecha"></a>
		          </input>
		        </div></td>
          </tr>
		   <tr class="fila">
            <td >Renta:</td>
			<td ><div align="center">Si
			      <input id="renta" name="renta" type="radio" value="S" onclick="disableFecha(this,4);"> 
			    No
			    <input id="renta" name="renta" type="radio" value="N" checked onclick="disableFecha(this,4);"> 
		      </div></td>
			    <td width="50%"><div align="center">
		          <input type="text" name="fpren" id="fpren" size="10" readonly value="<%=hoy%>" disabled>
				  <a href="javascript:void(0)" onclick="if(self.gfPop)gfPop.fPopCalendar(document.form1.fpren);return false;" HIDEFOCUS><img src="<%=BASEURL%>\js\Calendario\cal.gif" width="16" height="16" border="0" alt="Click aqui para escoger la fecha"></a>
		          </input>
		        </div></td>
          </tr>
        </table>
	    </td>
    </tr>
  </table>
  <br>
  <center>
    <img src="<%=BASEURL%>/images/botones/aceptar.gif" name="c_aceptar" style="cursor:pointer" onClick="if(validateFechas()){form1.submit()};" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);"> 
	<img src="<%=BASEURL%>/images/botones/cancelar.gif" name="c_cancelar" style="cursor:pointer" onClick="form1.reset();" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);"> 
	<img src="<%=BASEURL%>/images/botones/salir.gif" name="c_salir" style="cursor:pointer" onClick="parent.close();" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);">
  </center>
</form>

<table border="2" align="center" id="cuadromsg">
<%if(mensaje != null){%>


  <tr>
    <td>
      <table width="100%"  border="1" align="center"  bordercolor="#F7F5F4" bgcolor="#FFFFFF">
        <tr>
          <td width="229" align="center" class="mensajes"><%=mensaje%></td>
          <td width="29" background="<%=BASEURL%>/images/cuadronaranja.JPG">&nbsp;</td>
          <td width="58">&nbsp;</td>
        </tr>
      </table>
	</td>
  </tr>

<%}%>
 </table>
</div>
<iframe width=188 height=166 name="gToday:normal:agenda.js:gfPop:plugins.js" id="gToday:normal:agenda.js:gfPop:plugins.js" src="<%=BASEURL%>/js/Calendario/ipopeng.htm" scrolling="no" frameborder="0" style="visibility:visible; z-index:999; position:absolute; left:-500px; top:0px;"> </iframe>
<%=datos[1]%>  
</body>
</html>

<script>
document.form1.mes.value="<%=mes%>";
</script>
