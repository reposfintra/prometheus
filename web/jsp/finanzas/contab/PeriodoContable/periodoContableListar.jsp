  <!--
     - Author(s)       :      Osvaldo P�rez Ferrer
     - Date            :      20 de Junio de 2006
     - Copyright Notice:      TRANSPORTES SANCHEZ POLO S.A.
  -->
 <%--   - @(#)  
     - Description:  Listado de Periodos Contables
 --%>


<%@page session="true"%>
<%@page errorPage="../error/ErrorPage.jsp"%>
<%@page import="com.tsp.finanzas.contab.model.beans.*, java.util.*, java.text.*,com.tsp.util.*"%>
<%@ include file="/WEB-INF/InitModel.jsp"%>
<%@ taglib uri="http://java.sun.com/jstl/core" prefix="c" %>
<%@ taglib uri="http://jakarta.apache.org/taglibs/input-0.90" prefix="input" %>
<html>
<head>
    <title>Listado de Periodos Contables</title>
    <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">    
    <script language="javascript" src="<%=BASEURL%>/js/general.js"></script>
    <script type='text/javascript' src="<%=BASEURL%>/js/boton.js"></script>
    <script type='text/javascript' src="<%= BASEURL %>/js/reporte.js"></script>
    <link href="<%=BASEURL%>/css/estilostsp.css" rel="stylesheet" type="text/css">    
  
</head>
<%               
    Vector periodos = (Vector)request.getAttribute( "periodos" ); 
    String anio2 = ( request.getAttribute("anio") != null )? (String)request.getAttribute("anio") : "";
    int anio = Util.AnoActual();
%>
<body>
    <div id="capaSuperior" style="position:absolute; width:100%; height:100px; z-index:0; left: 0px; top: 0px;">
        <jsp:include page="/WEB-INF/toptsp.jsp?encabezado=Periodos Contables"/>
    </div>    
    <div id="capaCentral" style="position:absolute; width:100%; height:85%; z-index:0; left: 0px; top: 100px; overflow: scroll;">                
        <br>
        
        <div id="working" class="letrasFrame" align="center" ><br></div>
        <br>
        <table width="40%"  border="2" align="center">
            <tr>
                <td><table width="100%"  border="1" align="center" class="tablaInferior">
                <tr>
                <td><table width="100%" border="0" cellpadding="0" cellspacing="0">
                <tr>
                <td height="22" colspan=2 class="subtitulo1"><div align="center" class="subtitulo1">
                    <strong>Consultar Periodos Contables</strong>                     
                </div></td>
                <td width="212" class="barratitulo">
                <img src="<%=BASEURL%>/images/titulo.gif" width="32" height="20" align="left">
                   
            </tr>
                </table>
                
                <table width="100%"  border="0" align="center" cellpadding="2" cellspacing="3" >
                
            
            
                    <tr class="fila">
                        <td width="31%">A�o : </td>
                        <td>
                        
                        <select id="anio" name="anio" onchange="consultar(this);">                        
                        
                        <%for(int i=anio-10; i<anio+11; i++){%>
                            <option value="<%=i%>"><%=i%></option>
                        <%}%>
                        </select>
                        
                    </tr>
                  
                </table></td>
                </tr>
            </table></td>
            </tr>
        </table>
             <br>
        <div id="listaPlanillas" align="center">
           
             <%                              
             if(periodos != null && periodos.size()>0){%>
             
             
            <table width="800"  border="2" align="center">
            
            
            <tr>
            <td width="790">
                <table width="790" class="tablaInferior">
                    <tr>
                        <td height="22" colspan=2 class="subtitulo1">Periodos Contables</td>
                        <td width="50%" class="barratitulo"><img src="<%=BASEURL%>/images/titulo.gif" width="32" height="20"></td>
                    </tr>
                </table>                                   
                <table width="790" border="1" align="center" bordercolor="#999999" bgcolor="#F7F5F4">    
                <tr class="tblTitulo" align="center">
                    <td >Distrito</td>
                    <td >A�o</td>
                    <td >Mes</td>                    
                    <td >Abierto</td>
                    <td >IVA</td>
                    <td >Fecha IVA</td>
                    <td >Retenci�n</td>
                    <td >Fecha Retenci�n</td>
                    <td >Comercio</td>
                    <td >Fecha Comercio</td>
                    <td >Renta</td>
                    <td >Fecha Renta</td>
                </tr>
                    <%                                    
                    for(int i=0; i<periodos.size(); i++){
                        PeriodoContable p = (PeriodoContable) periodos.elementAt(i);                                                                                                                                                                       
                        %>
                <tr class="<%=i%2==0?"filagris":"filaazul"%>" onMouseOver='cambiarColorMouse(this)' 
                    style="cursor:hand" 
                    onclick="window.open('<%=CONTROLLERCONTAB%>?estado=Periodo&accion=Contable&opcion=update&anio=<%=p.getAnio()%>&mes=<%=p.getMes()%>&distrito=<%=p.getDistrito()%>','myWindow','width=650,height=550,resizable=yes');"
                    title="Click para Modificar" align="center">    
                <td width="45" class="bordereporte"><%=p.getDistrito()%>&nbsp;</td>    
                <td width="65" class="bordereporte"><%=p.getAnio()%>&nbsp;</td>
                <td width="65" class="bordereporte"><%=p.getMes()%></td>
                <td width="65" class="bordereporte"><%=(p.getAc().equals("A")?"SI":"NO")%></td>
                <td width="65" class="bordereporte"><%=(p.getIva().equals("S")?"SI":"NO")%></td>
                <td width="100" class="bordereporte"><%=(p.getFec_pre_iva().equals("0099-01-01")?"":p.getFec_pre_iva())%></td>
                <td width="65" class="bordereporte"><%=(p.getRetencion().equals("S")?"SI":"NO")%></td>
                <td width="65" class="bordereporte"><%=(p.getFec_pre_retencion().equals("0099-01-01")?"":p.getFec_pre_retencion())%></td>
                <td width="65" class="bordereporte"><%=(p.getComercio().equals("S")?"SI":"NO")%></td>
                <td width="75" class="bordereporte"><%=(p.getFec_pre_comercio().equals("0099-01-01")?"":p.getFec_pre_comercio())%></td>
                <td width="55" class="bordereporte"><%=(p.getRenta().equals("S")?"SI":"NO")%></td>
                <td width="85" class="bordereporte"><%=(p.getFec_pre_renta().equals("0099-01-01")?"":p.getFec_pre_renta())%></td>
                </tr>            
                
              
            </td>
            </tr>            
                <%}%>
                      </table>                        
            </table>                        
            <%
              }      
                    else{
                %>                                           
            
            <table border="2" align="center">
                <tr>
                    <td><table width="100%"  border="1" align="center"  bordercolor="#F7F5F4" bgcolor="#FFFFFF">
                    <tr>
                    <td width="229" align="center" class="mensajes">No existen Periodos</td>
                    <td width="29" background="<%=BASEURL%>/images/cuadronaranja.JPG">&nbsp;</td>
                    <td width="58">&nbsp;</td>
                </tr>
                </table></td>
                </tr>
            </table>                   
          <%}%>
            <br>
            <img src="<%=BASEURL%>/images/botones/salir.gif" name="c_salir" style="cursor:pointer" onClick="window.close();" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" align="absmiddle">   </td>
            </tr>
            
        </div>
    </div>
    
</body>
</html>

<script>
    document.getElementById("anio").value="<%=anio2%>";
    function consultar(anio){        
        location.replace("<%=CONTROLLERCONTAB%>?estado=Periodo&accion=Contable&opcion=list&anio="+anio.value);
    }
</script>
