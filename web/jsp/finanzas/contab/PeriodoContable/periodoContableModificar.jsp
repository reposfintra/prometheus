<!--
- Autor : Osvaldo P�rez Ferrer
- Date  : 20 de Junio de 2006
- Copyrigth Notice : Fintravalores S.A. S.A
-->
<%--
-@(#)
--Descripcion : Pagina JSP, que sirve para Modificar Periodo Contable.
--%>
<%@page contentType="text/html"%>
<%@page session="true"%>
<%@page import="com.tsp.finanzas.contab.model.*"%>
<%@page import="com.tsp.finanzas.contab.model.beans.*"%>
<%@page import="com.tsp.util.*"%>
<%@include file="/WEB-INF/InitModel.jsp"%>
<%@ taglib uri="http://jakarta.apache.org/taglibs/input-0.90" prefix="input" %>

<html>
    <head>

        <title>Modificar Periodo Contable</title>
        <link href="<%= BASEURL %>/css/estilostsp.css" rel='stylesheet'>
        <link href="../../../../css/estilostsp.css" rel="stylesheet" type="text/css">
        <link href="/css/estilostsp.css" rel="stylesheet" type="text/css">
        <script type='text/javascript' src="<%= BASEURL %>/js/finanzas/contab/validacionesPeriodoContable.js"></script>
        <script src='<%= BASEURL %>/js/date-picker.js'></script>
        <script type='text/javascript' src="<%= BASEURL %>/js/boton.js"></script>


<%
String mensaje = (String) request.getAttribute("mensaje");
String anulado = (String) request.getAttribute("anulado");
String chk ="";
PeriodoContable p = modelcontab.periodoContableService.getP();
%>
    </head>


    <body onresize="redimensionar()" onload = "redimensionar();" >
        <div id="capaSuperior" style="position:absolute; width:100%; height:100px; z-index:0; left: 0px; top: 1px;">
        <jsp:include page="/toptsp.jsp?encabezado=Periodo Contable"/></div>
        <div id="capaCentral" style="position:absolute; width:100%; height:87%; z-index:0; left: -5px; top: 101px; overflow: scroll;">

            <form name="form1" id="form1" method="post" action="<%=CONTROLLERCONTAB%>?estado=Periodo&accion=Contable&opcion=save">
            
            <input type="hidden" name="anio" id="anio" value="<%=p.getAnio()%>"> 
            <input type="hidden" name="mes" id="mes" value="<%=p.getMes()%>"> 
            <%if( anulado == null ){%>
            <table width="380" height="261" border="2" align="center" cellpadding="3" cellspacing="2" onclick="cuadromsg.style.visibility='hidden';">
                <tr>
                <td width="444" >	  
	  
	    
                <table width="370" height="248" class="tablainferior" align="center">
                <tr class="fila">
                    <td colspan="2" class="subtitulo1">Modificar Periodo Contable</td>
                    <td  class="barratitulo"><img src="<%=BASEURL%>//images/titulo.gif"></td>
                </tr>
                <tr class="fila" >
                <td width="27%">A�o : <%=p.getAnio()%></td>
                <td >Mes: <%=p.getMes()%></td>     
                <td>Distrito: <%=p.getDistrito()%></td>                  
                </tr>
                <tr class="fila">
                    <td>Abierto:</td>
                    <td colspan="2"> Si              
                        <input name="abierto" id="abierto" type="radio" value="A" checked> 
                        No 
              <%if(p.getAc().equals("C")){chk = "checked";}%>  
                        <input name="abierto" type="radio" value="C" <%=chk%>>           
                    </td>                      
                </tr>
                <tr class="fila"><td height="25" colspan="2"><div align="center">Impuestos</div></td>
                    <td height="25"><div align="center">Fecha de presentaci&oacute;n</div></td>
                </tr>
                <tr class="fila">
                    <td >IVA:</td>
                    <td width="23%" ><div align="center">Si
                        <input id="iva" name="iva" type="radio" value="S" src="1" checked onclick="disableFecha(this,1);"> 
                 <%
                 chk= "";
                 if(p.getIva().equals("N")){ chk = "checked";}%>       
                        No                        
                        <input id="iva" name="iva" type="radio" value="N" src="1" onclick="disableFecha(this,1);" <%=chk%>> 
                    </div></td>
                    <td width="50%"><div align="center">
                        <input type="text" name="fpiva" id="fpiva" size="10" readonly value="<%=(p.getFec_pre_iva().equals("0099-01-01"))? p.getAnio()+"-"+p.getMes()+"-"+"01" : p.getFec_pre_iva()%>"> 
                            <a id="cal1" href="javascript:void(0)" onclick="if(self.gfPop)gfPop.fPopCalendar(document.form1.fpiva);return false;" HIDEFOCUS><img src="<%=BASEURL%>\js\Calendario\cal.gif" width="16" height="16" border="0" alt="Click aqui para escoger la fecha"></a>
                        </input>
                    </div></td>
                </tr>
                <tr class="fila">
                    <td >Retencion:</td>
                    <td ><div align="center">Si
                        <input name="retencion" type="radio" value="S" src="2" checked onclick="disableFecha(this,2);"> 
                  <%
                 chk= "";
                 if(p.getRetencion().equals("N")){ chk = "checked";}%>              
                        No
                        <input name="retencion" type="radio" value="N" src="2" onclick="disableFecha(this,2);" <%=chk%>> 
                    </div></td>
                    <td width="50%"><div align="center">
                        <input type="text" name="fpret" id="fpret" size="10" readonly value="<%=p.getFec_pre_retencion()%>">
                            <a href="javascript:void(0)" onclick="if(self.gfPop)gfPop.fPopCalendar(document.form1.fpret);return false;" HIDEFOCUS><img src="<%=BASEURL%>\js\Calendario\cal.gif" width="16" height="16" border="0" alt="Click aqui para escoger la fecha"></a>
                        </input>
                    </div></td>
                </tr>
                <tr class="fila">
                    <td >Comercio:</td>
                    <td ><div align="center">Si
                        <input name="comercio" type="radio" value="S" src="3" checked onclick="disableFecha(this,3);"> 
                    <%
                 chk= "";
                 if(p.getComercio().equals("N")){ chk = "checked";}%>            
                        No
                        <input name="comercio" type="radio" value="N" src="3" onclick="disableFecha(this,3);" <%=chk%>> 
                    </div></td>
                    <td width="50%"><div align="center">
                        <input type="text" name="fpcom" id="fpcom" size="10" readonly value="<%=p.getFec_pre_comercio()%>">
                            <a href="javascript:void(0)" onclick="if(self.gfPop)gfPop.fPopCalendar(document.form1.fpcom);return false;" HIDEFOCUS><img src="<%=BASEURL%>\js\Calendario\cal.gif" width="16" height="16" border="0" alt="Click aqui para escoger la fecha"></a>
                        </input>
                    </div></td>
                </tr>
                <tr class="fila">
                    <td >Renta:</td>
                    <td ><div align="center">Si
                        <input name="renta" type="radio" value="S" src="4" checked onclick="disableFecha(this,4);"> 
                 <%
                 chk= "";
                 if(p.getRenta().equals("N")){ chk = "checked";}%>               
                        No
                        <input name="renta" type="radio" value="N" src="4" onclick="disableFecha(this,4);" <%=chk%>> 
                    </div></td>
                    <td width="50%"><div align="center">
                        <input type="text" name="fpren" id="fpren" size="10" readonly value="<%=p.getFec_pre_renta()%>">
                            <a href="javascript:void(0)" onclick="if(self.gfPop)gfPop.fPopCalendar(document.form1.fpren);return false;" HIDEFOCUS><img src="<%=BASEURL%>\js\Calendario\cal.gif" width="16" height="16" border="0" alt="Click aqui para escoger la fecha"></a>
                        </input>
                    </div></td>
                </tr>
            </table>
            </td>
            </tr>
            </table>
            <%}%>
            <br>
            <center>
                <%if( anulado == null ){%><img src="<%=BASEURL%>/images/botones/modificar.gif" name="c_aceptar" style="cursor:pointer" onClick="if(validateFechas()){form1.submit()};" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);"> 
                <img src="<%=BASEURL%>/images/botones/anular.gif" name="c_cancelar" style="cursor:pointer" onClick="location.replace('<%=CONTROLLERCONTAB%>?estado=Periodo&accion=Contable&opcion=delete&anio=<%=p.getAnio()%>&mes=<%=p.getMes()%>');" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);"> <%}%>
                <img src="<%=BASEURL%>/images/botones/salir.gif" name="c_salir" style="cursor:pointer" onClick="parent.close();" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);">
            </center>
            </form>

            <table border="2" align="center" id="cuadromsg">
<%if(mensaje != null){%>
<script>parent.opener.location.reload();</script>
                <tr>
                    <td>
                        <table width="100%"  border="1" align="center"  bordercolor="#F7F5F4" bgcolor="#FFFFFF">
                            <tr>
                                <td width="229" align="center" class="mensajes"><%=mensaje%></td>
                                <td width="29" background="<%=BASEURL%>/images/cuadronaranja.JPG">&nbsp;</td>
                                <td width="58">&nbsp;</td>
                            </tr>
                        </table>
                    </td>
                </tr>

<%}%>
            </table>
        </div>
        <iframe width=188 height=166 name="gToday:normal:agenda.js:gfPop:plugins.js" id="gToday:normal:agenda.js:gfPop:plugins.js" src="<%=BASEURL%>/js/Calendario/ipopeng.htm" scrolling="no" frameborder="0" style="visibility:visible; z-index:999; position:absolute; left:-500px; top:0px;"> </iframe>
    </body>
</html>

<script>

   for(var  i=0;i<form1.length;i++){
      var ele  = form1.elements[i];      
      if(ele.type=='radio'  && ele.checked){
         disableFecha(ele,ele.src);         
      }       
   }


</script>
