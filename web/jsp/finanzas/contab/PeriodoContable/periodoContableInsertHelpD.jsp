<%@ include file="/WEB-INF/InitModel.jsp"%>

<html>
<head>
<title>Descripci&oacute;n de campos Proceso de Mayorizaci&oacute;n</title>

<script type='text/javascript' src="<%=BASEURL%>/js/boton.js"></script>
<link href="<%=BASEURL%>/css/estilostsp.css" rel="stylesheet" type="text/css"> 
</head>

<body>
<% String BASEIMG = BASEURL +"/images/botones/"; %> 
<br>
<table width="594"  border="2" align="center">
  <tr>
    <td width="635" >
      <table width="100%" border="0" align="center">
        <tr  class="subtitulo">
          <td height="24" colspan="2"><div align="center">INGRESAR PERIODO CONTABLE </div></td>
        </tr>
        <tr class="subtitulo1">
          <td colspan="2">&nbsp;</td>
        </tr>
        <tr>
          <td width="123" class="fila">A&ntilde;o</td>
          <td width="551"  class="ayudaHtmlTexto"> Permite seleccionar el a&ntilde;o para el periodo contable.</td>
        </tr>
        <tr>
          <td  class="fila"> Mes </td>
          <td  class="ayudaHtmlTexto"> Permite seleccionar el mes para el periodo contable.</td>
        </tr>
		<tr>
          <td width="123"  class="fila"> Abierto </td>
          <td width="551"  class="ayudaHtmlTexto">Indique si el periodo estar&aacute; abierto o cerrado.</td>
        </tr>
		<tr>
          <td width="123"  class="fila"> Bot&oacute;n Salir </td>
          <td width="551"  class="ayudaHtmlTexto">Cierra la ventana.</td>
        </tr>
		<tr>
		  <td colspan="2"  class="fila">Impuestos</td>
	    </tr>
		<tr>
          <td colspan="2"  class="ayudaHtmlTexto"> Indique si para el periodo en cuesti&oacute;n, se aplicar&aacute; cada uno de los impuestos presentados, y seleccione su respectiva fecha de aplicaci&oacute;n.</td>
        </tr>
		<tr>
          <td  class="fila"> Bot&oacute;n Aceptar </td>
          <td  class="ayudaHtmlTexto">Ingresa el periodo contable a la base de datos.</td>
	    </tr>
		<tr>
		  <td  class="fila">Bot&oacute;n Cancelar </td>
		  <td  class="ayudaHtmlTexto">Reinicia el formulario.</td>
	    </tr>
		<tr>
		  <td  class="fila">Bot&oacute;n Salir </td>
		  <td  class="ayudaHtmlTexto">Cierra la ventana.</td>
	    </tr>
      </table>
    </td>
  </tr>
</table>
<br>
<table width="416" align="center">
	<tr>
	<td align="center">
		<img src="<%=BASEURL%>/images/botones/salir.gif" style="cursor:pointer" name="c_salir" onClick="window.close();" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" align="absmiddle">
	</td>
	</tr>
</table>
<p>&nbsp;</p>

<p>&nbsp;</p>
<p>&nbsp; </p>
</body>
</html>
