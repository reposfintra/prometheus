<%@ include file="/WEB-INF/InitModel.jsp"%>

<HTML>
<HEAD>
<TITLE></TITLE>
<META http-equiv=Content-Type content="text/html; charset=windows-1252">
<link href="../../../css/estilostsp.css" rel="stylesheet" type="text/css"> 
<link href="<%=BASEURL%>/css/estilostsp.css" rel="stylesheet" type="text/css"> 
<script type='text/javascript' src="<%=BASEURL%>/js/boton.js"></script>
</HEAD>
<BODY>  

  <table width="95%"  border="2" align="center">
    <tr>
      <td>
        <table width="100%" border="0" align="center">
          <tr  class="subtitulo">
            <td height="20"><div align="center">INGRESAR PERIODO CONTABLE </div></td>
          </tr>
          <tr class="subtitulo1">
            <td>Descripci&oacute;n del funcionamiento de la p&aacute;gina para ingreso de periodo contable.</td>
          </tr>
          <tr>
            <td  height="18"  class="ayudaHtmlTexto"><p>&nbsp;</p>
            <p>Primero seleccione A&ntilde;o y Mes del periodo contable. Por defecto el a&ntilde;o y mes ser&aacute;n los correspondientes a la fecha actual. </p></td>
          </tr>
          <tr>
            <td height="18"  class="ayudaHtmlTexto"><div align="center"><img src="<%=BASEURL%>/images/ayuda/finanzas/periodoContable/img1.JPG" ></div></td>
          </tr>
          <tr>
            <td  class="ayudaHtmlTexto">&nbsp;</td>
          </tr>
          <tr>
            <td  class="ayudaHtmlTexto"><p>Selecione SI o NO para indicar si el per&iacute;odo estar&aacute; abierto o cerrado.</p>
            <p>Indique cu&aacute;les impuestos de presentar&aacute;n en este periodo, as&iacute; como su fecha de presentaci&oacute;n. </p></td>
          </tr>
          <tr>
            <td  class="ayudaHtmlTexto"><div align="center"><img src="<%=BASEURL%>/images/ayuda/finanzas/periodoContable/img2.JPG" ></div></td>
          </tr>
          <tr>
            <td  class="ayudaHtmlTexto">&nbsp;</td>
          </tr>
          <tr>
            <td  class="ayudaHtmlTexto"><p>Click en el bot&oacute;n Aceptar para agregar el periodo contable. Debe aparecer el siguente mensaje: </p></td>
          </tr>
          <tr>
            <td  class="ayudaHtmlTexto"><div align="center"><img src="<%=BASEURL%>/images/ayuda/finanzas/periodoContable/img3.JPG" ></div></td>
          </tr>   
          
      </table></td>
    </tr>
  </table>
  <table width="416" align="center">
	<tr>
	<td align="center">
		<img src="<%=BASEURL%>/images/botones/salir.gif" style="cursor:pointer" name="c_salir" onClick="window.close();" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" align="absmiddle">
	</td>
	</tr>
</table>
</BODY>
</HTML>
