<%@page contentType="text/html"%>
<%@page session="true"%> 
<%@page import="java.util.*" %>
<%@page errorPage="/error/ErrorPage.jsp"%>
<%@page import="com.tsp.operation.model.*"%>
<%@include file="/WEB-INF/InitModel.jsp"%>
<%@page import="com.tsp.operation.model.beans.*"%>
<%@ taglib uri="http://jakarta.apache.org/taglibs/input-0.90" prefix="input" %>
<html>
<head><title>MSF900 a movcon</title>
<link href="<%= BASEURL %>/css/estilostsp.css" rel='stylesheet'>
</head>
<script type='text/javascript' src="<%= BASEURL %>/js/boton.js"></script>
<script type='text/javascript' src="<%= BASEURL %>/js/movcon.js"></script>
<body onresize="redimensionar()" onload = 'redimensionar(); LlenarPeriodo(document.forma.ano,document.forma.mes);'>
<div id="capaSuperior" style="position:absolute; width:100%; height:100px; z-index:0; left: 0px; top: 1px;">
<jsp:include page="/toptsp.jsp?encabezado=Finanzas"/>
</div>
<div id="capaCentral" style="position:absolute; width:100%; height:87%; z-index:0; left: -5px; top: 101px; overflow: scroll;">
<form name="forma" method="post" action="<%=CONTROLLERCONTAB%>?estado=TablaMovcon&accion=Insert">
<table border="2" align="center" width="429">
  <tr>
    <td>
      <table width="99%" border="0" align="center" class="tablaInferior">
        <tr>
          <td width="64%"  class="subtitulo1"><p align="left">movimiento registros MSF900 movcon </p></td>
          <td width="36%"  class="barratitulo"><img src="<%=BASEURL%>/images/titulo.gif" width="32" height="20"></td>
        </tr>
      </table>
      <table width="99%" align="center" class="tablaInferior"> <!--Llenar(forma.ano,forma.mes); -->
        <tr class="fila" >
		  <td colspan="2"><input name="inicializar" type="checkbox" onClick=" ChequearPeriodo();">  Inicializacion de Datos</td>
        </tr>
		<tr class="fila">
		  <td colspan="2"><input name="filtro" type="radio" value="" disabled id="filtro1"  onClick=""> Inicializar tabla pto_gastos_admin </td>
		  
		</tr>
		<tr class="fila">
		  <td colspan="2"><input name="filtro" type="radio" value="" disabled id="filtro2"  onClick=""> Inicializar tablas movcon y pto_gastos_admin  </td>
		</tr>
		<tr class="fila">
		  <td colspan="2"> Periodo </td>
		</tr>
		<tr class="fila">
		  <td width="30%"> A&ntilde;o
		    <select name="ano" class="select" id="ano" disabled style="width:45%">
	            </select></td>
		  <td width="70%"> Mes 
		    <select name="mes" class="select" id="mes" disabled style="width:35%">
	            </select></td>
        </tr>
	  </table>
	</td>
  </tr>
</table>
<div align="center"><br>
    <img src="<%=BASEURL%>/images/botones/aceptar.gif"  name="imgsalir"  height="21" onMouseOver="botonOver(this);" onClick="ValidarPrograma();" onMouseOut="botonOut(this);" style="cursor:hand">&nbsp;
    <img src="<%=BASEURL%>/images/botones/salir.gif"  name="imgsalir"  height="21" onMouseOver="botonOver(this);" onClick="parent.close()" onMouseOut="botonOut(this);" style="cursor:hand"></div>
</form>
<% if( !request.getParameter("msg").equals("") ){%>
<p>
  <table border="2" align="center">
    <tr>
      <td>
	    <table width="100%"  border="1" align="center"  bordercolor="#F7F5F4" bgcolor="#FFFFFF">
          <tr>
            <td width="229" align="center" class="mensajes"><%=request.getParameter("msg").toString()%></td>
            <td width="29" background="<%=BASEURL%>/images/cuadronaranja.JPG">&nbsp;</td>
          </tr>
        </table>
	  </td>
    </tr>
  </table>
</p>
<%} %>
</div>
</body>
</html>
