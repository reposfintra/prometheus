<!--
- Autor : Ing. Mario Fontalvo
- Date  : 9 de Abril 2007
- Copyrigth Notice : Fintravalores S.A. S.A
-->
<%--
-@(#)
--Descripcion : Pagina JSP, que se encarga de consultar los posible documento 
                de un comprobante.
--%>

<%-- Declaracion de librerias--%>
<%@page contentType="text/html"%>
<%@page session="true"%>
<%@page import="java.util.*"%>
<%@page import="com.tsp.operation.model.*"%>
<%@page import="com.tsp.finanzas.contab.model.*"%>
<%@page import="com.tsp.util.*"%>
<%@page import="com.tsp.operation.model.beans.*, java.util.*"%>
<%@page errorPage="/error/ErrorPage.jsp"%>
<%@include file="/WEB-INF/InitModel.jsp"%>
<%@ taglib uri="http://jakarta.apache.org/taglibs/input-0.90" prefix="input" %>
<%
    String path = application.getRealPath(model.menuService.getRealUrl(request.getRequestURI()));
    String datos[] = model.menuService.getContenidoMenu(BASEURL,request.getRequestURI(),path);
	String tdoc = Util.coalesce(  request.getParameter("tdoc") , "");
	String doc  = Util.coalesce(  request.getParameter("doc")  , "");
	String id   = Util.coalesce(  request.getParameter("id")  , "");
	model.tblgensvc.buscarListaCodigo( "TDOC", "COMPROBANTE" );
	TreeMap documentos = model.tblgensvc.getLista_des();
%>
<html>
	<head>
		<title>Documentos Relacionados</title>
        <link href="<%=BASEURL%>/css/estilostsp.css" rel="stylesheet" type="text/css">
        <link href="../../../../css/estilostsp.css" rel="stylesheet" type="text/css"> 	
		<script type='text/javascript' src="<%= BASEURL %>/js/boton.js"></script>	
	    <style type="text/css">
			<!--
			.Estilo1 {color: #D0E8E8}
			-->
        </style>		
	</head>
	<body onresize="redimensionar()"> 
	<center>
	<br>
	<form name="formulario">
		<table border="2" width="400">
			<tr>
				<td>
					<table width="100%" align="center"  >
						<tr >
							<td width="50%"  class="subtitulo1"><p align="left">Documento Relacionado </p></td>
							<td width="50%"  class="barratitulo"><img src="<%=BASEURL%>/images/titulo.gif" class="Estilo1" height="20" align="left"><%=datos[0]%></td>
						</tr>
					</table>	
					<table width="100%" align="center"  >
						<tr class="fila">
							<td width="39%">Tipo de Documento</td>
							<td width="61%">
								<input:select name="tdocumento" options="<%= documentos%>" attributesText="class='textbox' id='tdocumento' style='width:200'" default="<%=tdoc%>" />
							</td>							
						</tr>
						<tr class="fila">
							<td>Documento</td>
							<td><input type="text" name="documento" value="<%= doc %>" maxlength="10" onBlur="if (formulario.documento.value!='' && formulario.tdocumento.value!='OTRO') { enviar('<%= CONTROLLERCONTAB %>?estado=Grabar&accion=Comprobante&OP=VERIFICAR_DOCREL&tdoc_rel=' + formulario.tdocumento.value + '&doc_rel=' + formulario.documento.value ); } "></td>							
						</tr>						
					</table>									
				</td>
			</tr>
		</table>
		<input type="hidden" name="estado" value="">
		</form>
			<img src='<%=BASEURL%>/images/botones/aceptar.gif' name='Aceptar' align="absmiddle" style='cursor:hand' onclick="asignar();" onMouseOver='botonOver(this);' onMouseOut='botonOut(this);'>
			<img src='<%=BASEURL%>/images/botones/restablecer.gif' name='restablecer' align="absmiddle" style='cursor:hand'   onClick="formulario.reset(); " onMouseOver='botonOver(this);' onMouseOut='botonOut(this);'>
			<img src='<%=BASEURL%>/images/botones/salir.gif' name='Buscar' align="absmiddle" style='cursor:hand'   onClick="window.close();" onMouseOver='botonOver(this);' onMouseOut='botonOut(this);'>		
	</center>		
	
	 <script>
        function enviar(url){ 
          var a = "<iframe name='ejecutor'  src='" + url + "' top='400px' style='visibility:hidden'> ";
          aa.innerHTML = a;
        }
    </script>
    <font id='aa'></font>	
	</body>
</html>

<script>
	function asignar(){		
		if (formulario.estado.value=='ok' || formulario.documento.value==''){	
			var tdoc = parent.opener.document.getElementById('tdoc_rel<%= id %>');
			var doc  = parent.opener.document.getElementById('doc_rel<%= id %>');
			var ldoc = parent.opener.document.getElementById('ldoc_rel<%= id %>');
			if (tdoc!=null && doc!= null){
			
				if (formulario.documento.value!=''){
					tdoc.value = formulario.tdocumento.value;
					doc.value  = formulario.documento.value;
					ldoc.value  = tdoc.value + '/' + doc.value;
				} else {
					tdoc.value = '';
					doc.value  = '';
					ldoc.value = '';
				}
			}
			parent.close();
		}
	}
</script>
