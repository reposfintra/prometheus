<!--
- Autor : Ing. Sandra Escalante
- Modificado: Ing Ivan Dario Gomez
- Date  : 02 Febrero 2006
- Copyrigth Notice : Fintravalores S.A. S.A
-->
<%--
-@(#)
--Descripcion : Vista para mostrar la informacion de las ayudas 
--%>
<%@ include file="/WEB-INF/InitModel.jsp"%>

<html>
<head>
<title>Descripcion de campos para la Creacion de Facturas</title>

<link href="../../../../css/estilostsp.css" rel="stylesheet" type="text/css"> 
<link href="<%=BASEURL%>/css/estilostsp.css" rel="stylesheet" type="text/css"> 
</head>

<body>
<br>
<table width="696"  border="2" align="center">
  <tr>
    <td width="811" >
      <table width="100%" border="0" align="center">
        <tr  class="subtitulo">
          <td height="24" colspan="2"><div align="center">CONTABILIDAD</div></td>
        </tr>
        <tr class="subtitulo1">
          <td colspan="2"> GRABACION DE COMPROBANTE </td>
        </tr>
		<tr class="subtitulo1">
          <td colspan="2"> CABECERA </td>
        </tr>
        <tr>
          <td width="131" class="fila"> Tipo Doc </td>
          <td width="543"  class="ayudaHtmlTexto">Campo donde se especifica el tipo de documento </td>
        </tr>
        <tr>
          <td  class="fila"> Numero Doc.</td>
          <td  class="ayudaHtmlTexto">Campo donde se especifica el numero del documento </td>
        </tr>
        <tr>
          <td  class="fila">Tercero</td>
          <td  class="ayudaHtmlTexto"> Campo deonde se especifica la identificacion del tercero si el tipo de documento lo requiere </td>
        </tr>
        <tr>
          <td  class="fila">Concepto</td>
          <td  class="ayudaHtmlTexto">Campo donde se especifica el concepto del comprobante </td>
        </tr>
        <tr>
          <td  class="fila">Fecha</td>
          <td  class="ayudaHtmlTexto">Fecha de generacion del comprobante </td>
        </tr>
        <tr>
          <td  class="fila">Total Debito </td>
          <td  class="ayudaHtmlTexto">Campo donde se calcula la sumatoria de todos los debitos del comprobante </td>
        </tr>
        <tr>
          <td  class="fila">Total Credito </td>
          <td  class="ayudaHtmlTexto">Campo donde se calcula la sumatoria de todos los creditos del comprobante </td>
        </tr>
        <tr class="subtitulo1">
          <td colspan="2"> DETALLE DEL DOCUMENTO </td>
        </tr>
        <tr>
          <td  class="fila"><img src='<%=BASEURL%>/images/botones/iconos/mas.gif'   width="12" height="12"  ></td>
          <td  class="ayudaHtmlTexto">Boton para agregar item a la factura </td>
        </tr>
        <tr>
          <td  class="fila"><img id="imgini" name="imgini" src='<%=BASEURL%>/images/botones/iconos/menos1.gif'   width="12" height="12"  ></td>
          <td  class="ayudaHtmlTexto">Boton para eliminar item </td>
        </tr>
        <tr>
          <td  class="fila">Cod Contable </td>
          <td  class="ayudaHtmlTexto">Campo donde se especifica el codigo de la cuenta contable </td>
        </tr>
        <tr>
          <td  class="fila">Auxiliar</td>
          <td  class="ayudaHtmlTexto">Campo donde se especifica el auxiliar si la cuenta lo requiere </td>
        </tr>
        <tr>
          <td  class="fila">Tercero</td>
          <td  class="ayudaHtmlTexto">Campo donde se especifica un tercero si la cuenta lo requiere </td>
        </tr>
        <tr>
          <td  class="fila">Descripcion</td>
          <td  class="ayudaHtmlTexto">Campo donde se guarda la descripcion del item</td>
        </tr>
        <tr>
          <td  class="fila">Valor Debito </td>
          <td  class="ayudaHtmlTexto">Campo donde se almacena el valor debito del item </td>
        </tr>
        <tr>
          <td  class="fila">Valor Credito </td>
          <td  class="ayudaHtmlTexto">Campo donde se almacena el valor credito del item </td>
        </tr>
        <tr>
          <td  class="fila"><img src='<%=BASEURL%>/images/botones/agregar.gif' name='Buscar' align="absmiddle"    ></td>
          <td  class="ayudaHtmlTexto">Boton para agregar item al comprobante </td>
        </tr>
        <tr>
          <td  class="fila"><img src='<%=BASEURL%>/images/botones/aceptar.gif' name='Buscar' align="absmiddle"   onMouseOver='botonOver(this);' onMouseOut='botonOut(this);'></td>
          <td  class="ayudaHtmlTexto">Boton para grabar el comprobante </td>
        </tr>
        <tr>
          <td  class="fila"><img src='<%=BASEURL%>/images/botones/restablecer.gif' name='restablecer' align="absmiddle"    ></td>
          <td  class="ayudaHtmlTexto">Boton para limpiar los datos digitados en la pantalla </td>
        </tr>
        <tr>
          <td  class="fila"><img src='<%=BASEURL%>/images/botones/salir.gif' name='Buscar' align="absmiddle" ></td>
          <td  class="ayudaHtmlTexto"> Boton para cerrar la ventana </td>
        </tr>
      </table>
    </td>
  </tr>
</table>

</body>
</html>
