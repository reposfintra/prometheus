<!--
- Autor : Ing. Ivan Dario Gomez Vanegas
- Date  : Noviembre de 2005
- Copyrigth Notice : Fintravalores S.A. S.A
-->
<%--
-@(#)
--Descripcion : Pagina JSP, que muestra los datos de entrada para 
--              la creacion de la boleta recoge como parametro la OC
--%>

<%@page contentType="text/html"%>
<%@page session="true"%>
<%@page errorPage="/error/ErrorPage.jsp"%>
<%@page import="java.util.*"%>
<%@page import="com.tsp.operation.model.beans.*"%>
<%@page import="com.tsp.util.*"%>
<%@page import="com.tsp.operation.model.*"%>
<%@page import="com.tsp.finanzas.contab.model.beans.*"%>
<%@ include file="/WEB-INF/InitModel.jsp"%>
<%String path = application.getRealPath(model.menuService.getRealUrl(request.getRequestURI()));
  String datos[] = model.menuService.getContenidoMenu(BASEURL,request.getRequestURI(),path);
%>


<html>
<head>

    <title>Modificacion de Comprobante</title>
    <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
    <link href="../css/estilostsp.css" rel="stylesheet" type="text/css">
    <link href="<%= BASEURL %>/css/estilostsp.css" rel="stylesheet" type="text/css">
    <script src='<%= BASEURL %>/js/validar.js'></script>
	<script type='text/javascript' src="<%= BASEURL %>/js/boton.js"></script>
	
</head>
<% String mensaje    = (request.getParameter("mensaje")!= null)?request.getParameter("mensaje"):"";
   List listTipoDoc  =  modelcontab.GrabarComprobanteSVC.getLista();
   List listComp     =  modelcontab.GrabarComprobanteSVC.getListaComp();
   String tipodoc    = (request.getParameter("tipodoc")!=null)?request.getParameter("tipodoc"):"";
   String numdoc     = (request.getParameter("numdoc")!=null)?request.getParameter("numdoc"):"";
%>
<body>
<div id="capaSuperior" style="position:absolute; width:100%; height:100px; z-index:0; left: 0px; top: 0px;">
<jsp:include page="/WEB-INF/toptsp.jsp?encabezado=Modificacion de Comprobantes"/>
</div>

             <div id="capaCentral" style="position:absolute; width:100%; height:85%; z-index:0; left: 0px; top: 100px; overflow: scroll;"> 
    <form name="form" method="post" action="<%= CONTROLLERCONTAB %>?estado=Grabar&accion=Comprobante&OP=MODIFICAR" onSubmit= "return Validar();" >
        <table width="60%" border="2" align="center">
			<tr>
                            <td>
                            <table width="99%" height="34" border="0" align="center" class="tablaInferior">
                            <tr>
                               <td width="190" height="24"  class="subtitulo1"><p align="left">Buscar Comprobante</p></td>
                                <td width="404"  class="barratitulo"><img src="<%=BASEURL%>/images/titulo.gif" width="32" height="20" align="left"><%=datos[0]%></td>
                                
			    </tr>     						
                                                                    
                                    <tr class="fila">
                                    <td class="letra_resaltada" align="left">&nbsp;&nbsp;Tipo Documento</td>
                                    <td align="left">&nbsp;&nbsp;<select name="tipodoc"  >
									     <%for(int i=0; i< listTipoDoc.size();i++){
                                               Hashtable fila = (Hashtable)listTipoDoc.get(i);
                                          %>
                                            <option value="<%=(String)fila.get("codigo")%>" <%if(tipodoc.equals((String)fila.get("codigo"))){%>selected <%}%>><%=(String)fila.get("descripcion")%></option>
										 <%}%>
                                       </select></td>            
                                    </tr>
									 <tr class="fila">
                                    <td class="letra_resaltada" align="left">&nbsp;&nbsp;Num Documento</td>
                                    <td align="left">&nbsp;
                                      <input type="text"  name="numdoc" class="textbox" maxlength="30" value="<%=numdoc%>" ></td>            
                                    </tr>
                              </table>
			</td>        
		      </tr>
        	
		</table>
		
		
		
        <table align="center">
          <tr align="center">
            <td colspan="2"> <img src="<%=BASEURL%>/images/botones/aceptar.gif"  name="imgaceptar" onClick="if(Validar()){form.submit();}" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" style="cursor:hand">  <img src="<%=BASEURL%>/images/botones/salir.gif"  name="imgsalir" onClick="parent.close();" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" style="cursor:hand"> </td>
          </tr>
        </table>
		<br><br>
		<%
		if( (listComp!= null )&& (!numdoc.equals("")) ){%>
		
		      <table width="100%" border="1" bordercolor="#999999" bgcolor="#F7F5F4" align="center">
					<tr class="tblTitulo" align="center">
						<td  align="center">Periodo           </td>
						<td  align="center">Grupo Trans.      </td>
						<td  align="center">Detalle           </td>
						<td  align="center">Fecha Creacion    </td>
						<td  align="center">Fecha Aplicacion  </td>
						<td  align="center">Usuario Aplicacion </td>
						<td  align="center">Valor Debito      </td>
						<td  align="center">Valor Credito     </td>
					</tr>
					<% 
					ComprobanteFacturas  comprobante;  
					for (int i = 0; i < listComp.size(); i++){		  
						comprobante = (ComprobanteFacturas)listComp.get(i);
						%>
						<tr class="<%=(i % 2 == 0 )?"filagris":"filaazul"%>" onMouseOver='cambiarColorMouse(this)'   style="cursor:hand" onClick="parent.location='<%=CONTROLLERCONTAB%>?estado=Grabar&accion=Comprobante&OP=BUSCAR_COMPROBANTE&dstrct=<%=comprobante.getDstrct()%>&tipodoc=<%=comprobante.getTipodoc()%>&numdoc=<%=comprobante.getNumdoc()%>&grupo=<%=comprobante.getGrupo_transaccion()%>'">
						    <td  align="center" class="bordereporte"><%=comprobante.getPeriodo()%> </td>
							<td  align="center" class="bordereporte"><%=comprobante.getGrupo_transaccion()%> </td>
							<td  align="center" class="bordereporte"><%=comprobante.getDetalle()%>           </td>
							<td  align="center" class="bordereporte"><%=comprobante.getFechaCreacion()%>    </td>
							<td  align="center" class="bordereporte"><%=comprobante.getFecha_aplicacion()%>  </td>
							<td  align="center" class="bordereporte"><%=comprobante.getUsuario_aplicacion()%>           </td>
							<td  align="center" class="bordereporte"><%=(comprobante.getTotal_debito()!=0)?UtilFinanzas.customFormat(comprobante.getTotal_debito()):""%>      </td>
							<td  align="center" class="bordereporte"><%=(comprobante.getTotal_credito()!=0)?UtilFinanzas.customFormat(comprobante.getTotal_credito()):""%>     </td>
						</tr>
					<%}%>
				</table>
		 
		<%}%>
		
        
    </form>
	<%if(!mensaje.equals("")){%> 
   <table border="2" align="center">
  <tr>
    <td><table width="410" height="73" border="1" align="center"  bordercolor="#F7F5F4" bgcolor="#FFFFFF">
        <tr>
          <td width="282" align="center" class="mensajes"><%=mensaje%></td>
          <td width="28" background="<%=BASEURL%>/images/cuadronaranja.JPG">&nbsp;</td>
          <td width="78">&nbsp;</td>
        </tr>
    </table></td>
  </tr>
</table>
<%}%>
	</div> 
  
</body>
<%=datos[1]%>
<script>
function Validar(){
  var form = document.form;
  if(form.numdoc.value == ""){
	    alert("Ingrese el numero del documento");
		return false;
	}else{
	  return true;
	}
   
   
}



</script>
</html>