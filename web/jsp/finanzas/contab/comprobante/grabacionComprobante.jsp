<!--
- Autor : Ing. Ivan Gomez
- Date  : 5 de Octubre de 2005
- Copyrigth Notice : Fintravalores S.A. S.A
-->
<%--
-@(#)
--Descripcion : Pagina JSP, que se encarga de gestionar el formulario para la 
                            grabacion del comprobante contable
--%>

<%-- Declaracion de librerias--%>
<%@page contentType="text/html"%>
<%@page session="true"%>
<%@page import="java.util.*"%>
<%@page import="com.tsp.operation.model.*"%>
<%@page import="com.tsp.finanzas.contab.model.*"%>
<%@page import="com.tsp.util.*"%>
<%@page import="com.tsp.operation.model.beans.*, java.util.*"%>
<%@page import="com.tsp.finanzas.contab.model.beans.*"%>
<%@page errorPage="/error/ErrorPage.jsp"%>
<%@include file="/WEB-INF/InitModel.jsp"%>
<%@ taglib uri="http://jakarta.apache.org/taglibs/input-0.90" prefix="input" %>
<% Usuario usuarioLogin  = (Usuario) session.getAttribute("Usuario"); 
   String empresa = usuarioLogin.getEmpresa();  
   String path = application.getRealPath(model.menuService.getRealUrl(request.getRequestURI()));
   String datos[] = model.menuService.getContenidoMenu(BASEURL,request.getRequestURI(),path);
   String PERIODO  = (String) session.getAttribute("periodo");
   String periodoActual = Util.getFechaActual_String(1) + Util.getFechaActual_String(3) ;
   String opcion = (request.getParameter("opcion")!=null)?request.getParameter("opcion"):"";  
   String maxfila                    =  (request.getParameter ("maxfila")==null?"1":request.getParameter ("maxfila")); 
   int    MaxFi                      =  Integer.parseInt(maxfila); 
   List listTipoDoc                  =  modelcontab.GrabarComprobanteSVC.getLista();
   ComprobanteFacturas comprobante   =  modelcontab.GrabarComprobanteSVC.getComprobante();
   String tipoDoc      ="";
   String numDoc       ="";
   String serie        =""; 
   String fecha        = (Integer.parseInt(PERIODO)<Integer.parseInt(periodoActual)?PERIODO.substring(0,4) + "-" + PERIODO.substring(4,6) + "-01": Util.getFechaActual_String(4));
   String terceroCab   ="";
   String concepto     ="";
   String fechaApli    ="0099-01-01 00:00:00";
   double totalDebito  =0;
   double totalCredito =0;
   List items          = null;
   int grupoT =0;  
   

   if (comprobante != null){
       tipoDoc      = comprobante.getTipodoc();
       numDoc       = comprobante.getNumdoc();
       serie        = (comprobante.getSerie()!=null)?comprobante.getSerie():"";  
       fecha        = comprobante.getFechadoc();
       terceroCab   = comprobante.getTercero();
       concepto     = comprobante.getDetalle();
       totalDebito  = comprobante.getTotal_debito();
       totalCredito = comprobante.getTotal_credito();
       items        = comprobante.getItems();
       grupoT       = comprobante.getGrupo_transaccion(); 
       fechaApli    = comprobante.getFecha_aplicacion();
       
       //sumar detalle comprobante
       double suma=0;
       for(int i=1; i<= items.size();i++){
        ComprobanteFacturas comproSuma = (ComprobanteFacturas) items.get(i-1); 
        suma= suma+comproSuma.getTotal_credito();
         }
       //valor real credito
       totalCredito=suma;  
           
        }
                 
   
   
   String estilo="";
   String clase ="";
   String readonly ="";
   if(opcion.equals("modificar")){
     estilo ="style='border:0'";
     clase="filaresaltada";
     readonly ="readonly" ;
   }
%>
<html>
    <head><title>Comprobantes Contables</title>
        
        <link href="<%=BASEURL%>/css/estilostsp.css" rel="stylesheet" type="text/css">
        <link href="../../../../css/estilostsp.css" rel="stylesheet" type="text/css"> 
        <link type="text/css" rel="stylesheet" href="./css/TransportadorasApi.css " />
<script type="text/javascript" src="./js/jquery-1.4.2.min"></script>
        <script type='text/javascript' src="<%= BASEURL%>/js/jquery/jquery-ui/jquery.min.js"></script> 
<script type="text/javascript" src="/fintra/js/jquery/jquery-ui/jquery.ui.min.js"></script>
        <!--<script type='text/javascript' src="<%=BASEURL%>/js/Validacion.js"></script> -->
        <script type='text/javascript' src="<%= BASEURL %>/js/boton.js"></script>
        <script type='text/javascript' src="<%= BASEURL %>/js/validar.js"></script>        
        <script type='text/javascript' src="<%= BASEURL %>/js/finanzas/contab/comprobante.js"></script>
		<script type='text/javascript' src="<%= BASEURL %>/js/finanzas/contab/grabacionComprobante.js"></script>
<!--        <script type='text/javascript' src="<%= BASEURL %>/js/contabilidad.js"></script>-->
<!--		<script language="javascript" src="<%= BASEURL %>/js/validarDocPorPagar.js"></script>-->
        <script>			
			var controlador = "<%=CONTROLLERCONTAB%>";
			var BASEURL     = "<%=BASEURL%>";
			var maxfila     = <%=maxfila%>;		
		</script>
        <style type="text/css">
            <!--
            .Estilo1 {color: #D0E8E8}
            -->
        </style>
    </head>
    <body>

        <div id="capaSuperior" style="position:absolute; width:100%; height:100px; z-index:0; left: 0px; top: 0px;">
            <jsp:include page="/toptsp.jsp?encabezado=Grabación del comprobante Contable"/>
        </div>
        <div id="capaCentral" style="position:absolute; width:100%; height:87%; z-index:0; left: 0px; top: 100px; overflow: scroll;">

            
            <form name="forma1" id="forma1" action="" method="post">  
<%
  String mensaje=""+request.getParameter("ms");
    if (! (mensaje.equals("")|| mensaje.equals("null"))){
%>
                
            <table border="2" align="center">
                <tr>
                    <td><table width="410" height="73" border="1" align="center"  bordercolor="#F7F5F4" bgcolor="#FFFFFF">
                        <tr>
                            <td width="282" align="center" class="mensajes"><%=mensaje%></td>
                            <td width="28" background="<%=BASEURL%>/images/cuadronaranja.JPG">&nbsp;</td>
                            <td width="78">&nbsp;</td>
                        </tr>
                    </table></td>
                </tr>
            </table>
            <br>
<%
    }   
%>
     


            <table width="900" align="center">
				
                <tr><td>
					
                <jsp:include page="/jsp/finanzas/contab/infoContab.jsp?tamanoTableEncabezado=100p"/> 
					
                <table border="2" width='100%'  >
                       
					   
                <tr>
                <td>
                <table width="100%" align="center"  >
								  
								    
                    <tr >
                        <td width="50%"  class="subtitulo1"><p align="left">Documento</p></td>
                        <td width="50%"  class="barratitulo"><img src="<%=BASEURL%>/images/titulo.gif" class="Estilo1" height="20" align="left"><%=datos[0]%></td>
                    </tr>
                </table>
                <table width="100%" align="center" cols="7">
                <tr class="fila">
                    <td width="9%"><div align="center">Tipo doc</div></td>
                    <td width="16%"><div align="center">Numero Doc </div></td>
                    <td width="13%"><div align="center">Fecha </div></td>
                    <td width="18%"> <div align="center">Tercero</div></td>
                    <td width="44%"><div align="center"></div></td>
                </tr>
                <tr class="fila">
                <td align="center"><input type="hidden" name="grupo" value="<%=grupoT%>">
                
                    <select id="TipoDoc" name="TipoDoc" <%if(opcion.equals("modificar")){%>disabled<%} %>   onchange=" enviar('controllercontab?estado=Grabar&accion=Comprobante&OP=VERRIFICAR_NUM_DOC&valor=' + this.value );" >
                    <option value=""></option>
                      <% String isComprobante = "";
                         for(int i=0; i< listTipoDoc.size();i++){
                            Hashtable fila = (Hashtable)listTipoDoc.get(i);
                            String es_comprobante = fila.get("comprodiario").toString();
                            String maneja_serie   = fila.get("maneja_serie").toString();
                            String prefijo        = fila.get("prefijo").toString();
                            String prefijo_anno   = fila.get("prefijo_anio").toString();
                            String prefijo_mes    = fila.get("prefijo_mes").toString();
                            String long_serie     = fila.get("long_serie").toString();
                            
                            if (tipoDoc.equals((String)fila.get("codigo"))) { 
                                isComprobante = ( es_comprobante.equals("S") ? "ok" : "no");
                            }
                            
                            if ( opcion.equals("modificar") || es_comprobante.equals("S") ) {
                      %>
                        <option value="<%=(String)fila.get("codigo")%>" <%=tipoDoc.equals((String)fila.get("codigo"))?"selected":""%> maneja_serie="<%= maneja_serie %>" prefijo="<%= prefijo %>"  prefijo_anno="<%= prefijo_anno %>" prefijo_mes="<%= prefijo_mes %>" long_serie="<%= long_serie %>"><%=(String)fila.get("descripcion")%></option> 
                      <%} } %>
                    </select>

                </td>
									
                <td><div align="center" id='TexNumDoc'>
                    <input type="text" name="NumDoc" id="NumDoc"  maxlength="10" value="<%=numDoc%>" <%=estilo%> class="<%=clase%>" <%=readonly%> style="text-align:center " onblur=" validarFormatoSerie('<%=PERIODO%>'); ">
                    <input type="hidden" name="serie" id="serie"  value="<%=serie%>" style="width:100; ">
                </div></td>  
                <td><div align="center">
                <input name="fecha_documento"  type="text" id="fecha_documento" size="11" value="<%=fecha%>" readonly>
                <span class="comentario"></span> <a href="javascript:void(0)" onClick="if(self.gfPop)gfPop.fPopCalendar(document.forma1.fecha_documento);return false;" hidefocus> <img src="<%=BASEURL%>\js\Calendario\cal.gif" width="16" height="16" border="0" alt="De click aqui para escoger la fecha"></a>							         </div></td>
                <td><div align="center"></div>
                <div align="center" id="TexterceroCabecera">
                    <%String visible= (terceroCab.equals("")?"hidden": "text");%>
                    <input type="<%=visible%>" name="terceroCabecera"  value="<%=terceroCab%>"readonly>&nbsp;<%if(visible.equals("text")){%><img src='<%=BASEURL%>/images/botones/iconos/lupa.gif'  width="15" height="15"  onClick="buscarLista('SQL_NIT', '', terceroCabecera.name,'LISTA DE TERCEROS');" title="Buscar" style="cursor:hand" ><%}%>
                </div></td>
                <td>
                    <input type="text" name="empresa" id="empresa"  maxlength="10" value="<%=empresa%>"readonly hidden >
                </td>
                <td></td>
                </tr>
                <tr class="fila">
                    <td>Concepto</td>
                    <td colspan="2"><textarea name="concepto" id="concepto" cols="60" rows="2" class="textbox"  ><%=concepto%></textarea></td>
                    <td colspan="2"><div align="right">
                        <table width="231" border="0" cellspacing="0" cellpadding="0">
                            <tr class="fila">
                                <td width="167"><div align="center">Total Debito </div></td>
                                <td width="131"><div align="center">Total Credito </div></td>
                            </tr>
                            <tr>
                                <td><div align="center">
                                    <input name="TotalDebito" id="TotalDebito"   value="<%=Util.customFormat(totalDebito)%>" class="filaresaltada" style="text-align:right; width:100; border:0;" type="text"  onChange="formatear(this);" size="16"  maxlength="11" align="right" readonly>
                                </div></td>
                                <td><div align="center">
                                    <input name="TotalCredito" id="TotalCredito"   value="<%=Util.customFormat(totalCredito)%>" class="filaresaltada" style="text-align:right;width:100; border:0; " type="text"  onChange="formatear(this);" size="16"  maxlength="11" align="right" readonly>
                                </div></td>
                            </tr>
                        </table>
                    </div></td>
                </tr>
            </table>
                </td>
                </tr>
                </table>
                <table width="100%" border="2" align="left">
                <tr>
                    <td >
                    <table width="100%" align="center"  >
                        <tr >
                            <td width="50%"   class="subtitulo1"><p align="left">Detalle del Documento </p></td>
                            <td width="50%"  class="barratitulo"><img src="<%=BASEURL%>/images/titulo.gif" class="Estilo1" height="20" ></td>
                        </tr>
                    </table>
                        
                    <table id="detalle" width="100%" >
                    <tr  id="fila1" class="tblTitulo">
                        <td width="8%" align="center" nowrap>&nbsp;&nbsp;Item&nbsp;&nbsp;  </td>
                        <td width="13%" align="center">Cod Contable </td>
                        <td width="21%" align="center">Auxiliar</td>
                        <td width="12%" align="center">Tercero      </td>
                        <td width="4%" align="center">ABC</td>
                        <td width="12%" align="center" nowrap>&nbsp;&nbsp;Doc. Rel.&nbsp;&nbsp;&nbsp;</td>
                        <!--<%if (empresa.equals("INYM")){%>
                        <td width="12%" align="center" nowrap>&nbsp;&nbsp;# MS&nbsp;&nbsp;&nbsp;</td>
                        <%}%>-->
                        <td width="12%" align="center">Descripcion</td>					    
                        <td width="10%" align="center">Valor Debito   </td>
                        <td width="10%" align="center">Valor Credito  </td>
                    </tr>

                <%
                 int x=1;
                if ( items == null){%>
                    <tr class="filaazul" id="filaItem<%=x%>" nowrap  bordercolor="#D1DCEB" > 
                        <td><table width="60" border="0" cellspacing="0" cellpadding="0" height="100%">
                        <tr>
                        <td width="60%" nowrap class="letraresaltada"><a id="n_i<%=x%>" ><%=x%></a>
                            <input name="cod_item<%=x%>" id="cod_item<%=x%>" value="<%=x%>" type="hidden" size="4" maxlength="5" border="0">
                        </td>
                        <td width="40%" nowrap><a onClick="insertarItem('<%=BASEURL%>');"  id="insert_item<%=x%>" style="cursor:hand" ><img src='<%=BASEURL%>/images/botones/iconos/mas.gif'   width="12" height="12"  ></a>
                        <a onClick="borrarItem('filaItem<%=x%>');"  value="1" id="borrarI<%=x%>" style="cursor:hand" ><img id="imgini" name="imgini" src='<%=BASEURL%>/images/botones/iconos/menos1.gif'   width="12" height="12"  > </a></span></td>
                    </tr>
                    </table></td>
					     
                    <td align="center"><input type="text" maxlength='25'  style='width:110; ' name='CodContable<%=x%>' id='CodContable<%=x%>' onKeyPress="Verificar('<%=x%>')"  onChange="Verificar2('<%=x%>')"> </td>
                    <td align="center" nowrap><div  id="Texsubledger<%=x%>">
                        <select name='subledger<%=x%>' style="width:40; " onChange="forma1.idSubledger<%=x%>.value='';">
                            <option value=''  > </option>
                        </select>
                        <input type="text" maxlength='25' readonly style='width:85' name='idSubledger<%=x%>' id='idSubledger<%=x%>'>
                        &nbsp;<img src='<%=BASEURL%>/images/botones/iconos/lupa.gif'  width="15" height="15"  onclick="" title="Buscar" style="cursor:hand" >
                        <input type="hidden"   name='ReqAuxiliar<%=x%>' id='ReqAuxiliar<%=x%>'>
                    </div> </td>
                    <td align="center" nowrap>
                    <div  id="Textercero<%=x%>">							
                        <input type="text" name='tercero<%=x%>' id='tercero<%=x%>' maxlength='15' style='width:85' readonly >
                        <img src='<%=BASEURL%>/images/botones/iconos/lupa.gif'  width="15" height="15"   title="Buscar" style="cursor:hand" >
                        <input type="hidden"   name='ReqTercero<%=x%>' id='ReqTercero<%=x%>'>
                    </div></td>
					<td align="center" nowrap><input type="text" name="abc<%=x%>" id="abc<%=x%>" size="4" maxlength="4" onBlur="validarABC(CodContable<%=x%>, this);" style="text-align:center "></td>
					<td align="center" nowrap>
						<input type="text"   name="ldoc_rel<%=x%>" id="ldoc_rel<%=x%>" size="6" maxlength="6"  readonly>
						<img src='<%=BASEURL%>/images/botones/iconos/lupa.gif' width="15" height="15"   title="Asignar"  style='cursor:hand; '   onClick=" openWin( 'Documento','<%= BASEURL %>/jsp/finanzas/contab/comprobante/documentoRelacionado.jsp?tdoc='+ tdoc_rel<%=x%>.value +'&doc='+ doc_rel<%=x%>.value +'&id=<%= x %>',430,180 ); ">
						<input type="hidden" name="tdoc_rel<%=x%>" id="tdoc_rel<%=x%>" size="5" maxlength="4" style="text-align:center ">
						<input type="hidden" name="doc_rel<%=x%>"  id="doc_rel<%=x%>"  size="5" maxlength="4" style="text-align:center ">						
					</td>
                                        <!--<%if (empresa.equals("INYM")){%>
                                        <td align="center" nowrap>
                                            <input type="text"   name="numos<%=x%>" id="numos<%=x%>" size="6" maxlength="6" style="width: 104px;" readonly>
                                            <img src='<%=BASEURL%>/images/botones/iconos/lupa.gif' id="buscarnumos" width="15" height="15"   title="Asignar"  style='cursor:hand;'  onClick="cargarMultiservicio(<%=x%>)">

                                        </td>
                                        <%}%>-->
					<td align="center" nowrap><textarea name="descripcion<%=x%>" id="descripcion<%=x%>" cols="35" rows="1" class="textbox" ></textarea></td>					
                    <td align="center"><input name="valorDebito<%=x%>" id="valorDebito<%=x%>" onFocus="this.select();"  onBlur="limpiar('<%=x%>','valorDebito<%=x%>');"   style="text-align:right;"  onKeyPress="soloDigitos(event, 'decOK')" onKeyUp="sumarDebito()"  type="text"  onChange="formatear(this);" size="16"  maxlength="11" align="right"></td>
                    <td align="center"><input name="valorCredito<%=x%>" id="valorCredito<%=x%>" onFocus="this.select(); " onBlur="limpiar('<%=x%>','valorCredito<%=x%>'); nuevoItem(this);"   style="text-align:right;"  onKeyUp="sumarCredito();" onKeyPress="soloDigitos(event, 'decOK')"  type="text"  onChange="formatear(this);" size="16"  maxlength="11" align="right"></td>
                </tr>   
				
                        
                  <%}else{
                       for(x=1; x<= items.size();x++){
					   	//System.out.println("I"+x + " : " + ( new java.util.Date() ));

                            ComprobanteFacturas comprodet = (ComprobanteFacturas) items.get(x-1); 
                            String cuenta       = comprodet.getCuenta();
                            String descripcion  = comprodet.getDetalle();
                            String tercero      = comprodet.getTercero();
							String abc          = comprodet.getAbc();
                            String idSubledger  = comprodet.getAuxiliar();
                            double valorDebito  = comprodet.getTotal_debito();
                            double valorCredito  = comprodet.getTotal_credito();
                            Vector tipSubledger  = comprodet.getCodSubledger(); 
							
							String tdocrel    = comprodet.getTdoc_rel();
							String docrel     = comprodet.getNumdoc_rel();
							String ldocrel    = (docrel.equals("")?"":tdocrel + "/" + docrel );
                       %>
                    <tr class="<%= (x%2==0?"filagris":"filaazul") %>" id="filaItem<%=x%>" nowrap  bordercolor="#D1DCEB"  > 
                        <td>
                        <table width="60" border="0" cellspacing="0" cellpadding="0" height="100%">
                            <tr>
                            <td width="60%" nowrap class="letraresaltada"><a id="n_i<%=x%>" ><%=x%></a>
                                <input name="cod_item<%=x%>" id="cod_item<%=x%>" value="<%=x%>" type="hidden" size="4" maxlength="5" border="0">
                            </td>
                            <td width="40%" nowrap><a onClick="insertarItem('<%=BASEURL%>');"  id="insert_item<%=x%>" style="cursor:hand" ><img src='<%=BASEURL%>/images/botones/iconos/mas.gif'   width="12" height="12"  ></a>
                            <a onClick="borrarItem('filaItem<%=x%>');"  value="1" id="borrarI<%=x%>" style="cursor:hand" ><img id="imgini" name="imgini" src='<%=BASEURL%>/images/botones/iconos/menos1.gif'   width="12" height="12"  > </a></span></td>
                            </tr>
                        </table>
                        </td>
					     
                        <td align="center">
                            <% String Error = (comprodet.isExisteCuenta() ? "" :  "background-color:#CC0000; color:#FFFFFF;") ; %>
                            <input type="text" maxlength='25' style='width:110;<%=Error%>' name='CodContable<%=x%>' id='CodContable<%=x%>' value="<%=cuenta%>" onKeyPress="Verificar('<%=x%>')"  onChange="Verificar2('<%=x%>')">
                        </td>
                        <td align="center" nowrap>
                            <div  id="Texsubledger<%=x%>">
                            <%if(tipSubledger == null){%>
                            <select name='subledger<%=x%>' style="width:40; " onChange="forma1.idSubledger<%=x%>.value='';"><option value='' > </option></select>                            
                            <%}else{ %>
                            <select name='subledger<%=x%>' style='width:40; ' onChange="forma1.idSubledger<%=x%>.value='';">
                                <% for(int i=0; i < tipSubledger.size();i++){ %>
                                <option value='<%=tipSubledger.get(i)%>' ><%=tipSubledger.get(i)%> </option>
                                <% } %>
                            </select>
                            <%}%>
                            <input type="text" maxlength='25' readonly style='width:85' name='idSubledger<%=x%>' id='idSubledger<%=x%>' value="<%=idSubledger%>">
                            &nbsp;<img src='<%=BASEURL%>/images/botones/iconos/lupa.gif'  width="15" height="15"  onclick="" title="Buscar" style="cursor:hand" >
                            <input type="hidden"   name='ReqAuxiliar<%=x%>' id='ReqAuxiliar<%=x%>'>
                        </div> 
                        </td>
                    
                        <td align="center" nowrap>							
                            <div  id="Textercero<%=x%>">
                            <%if(!tercero.equals("")){%> 
                                <input type="text" name='tercero<%=x%>' id='tercero<%=x%>' maxlength='15' style='width:85'  value="<%=tercero%>" readonly >
                                <img src='<%=BASEURL%>/images/botones/iconos/lupa.gif'  width="15" height="15"   title="Buscar" style="cursor:hand" onClick="buscarTercero('<%=BASEURL%>','tercero<%=x%>')" >
                                <input type="hidden"   name='ReqTercero<%=x%>' id='ReqTercero<%=x%>'>
                            <%}else{%>
                                <input type="hidden" name='tercero<%=x%>' id='tercero<%=x%>' maxlength='15' style='width:90'  value="<%=tercero%>" readonly >
                                <input type="hidden"   name='ReqTercero<%=x%>' id='ReqTercero<%=x%>'>
                            <%}%>
                        </div></td>
						
                    	<td align="center" nowrap><input type="text" name="abc<%=x%>" id="abc<%=x%>" value="<%= abc %>" size="5" maxlength="4"  onBlur="validarABC(CodContable<%=x%>, this);"  style="text-align:center "></td>
                    	<td align="center" nowrap>
							<input type="text"   name="ldoc_rel<%=x%>" id="ldoc_rel<%=x%>" size="6" maxlength="6"  readonly value="<%= ldocrel %>">
							<img src='<%=BASEURL%>/images/botones/iconos/lupa.gif' width="15" height="15"   title="Asignar" style='cursor:hand;'  onClick=" openWin( 'Documento','<%= BASEURL %>/jsp/finanzas/contab/comprobante/documentoRelacionado.jsp?tdoc='+ tdoc_rel<%=x%>.value +'&doc='+ doc_rel<%=x%>.value +'&id=<%= x %>',430,180 ); ">
							<input type="hidden" name="tdoc_rel<%=x%>" id="tdoc_rel<%=x%>" size="5" maxlength="4" style="text-align:center " value="<%= tdocrel %>">
							<input type="hidden" name="doc_rel<%=x%>"  id="doc_rel<%=x%>"  size="5" maxlength="4" style="text-align:center " value="<%= docrel %>">							
						</td>                                     
						<td align="center" nowrap><textarea name="descripcion<%=x%>" id="descripcion<%=x%>" cols="35" rows="1" class="textbox" ><%=descripcion%></textarea></td>
												
                        <td align="center"><input name="valorDebito<%=x%>" id="valorDebito<%=x%>" onFocus="this.select();"  onBlur="limpiar('<%=x%>','valorDebito<%=x%>'); " value="<%=(valorDebito!=0)?Util.customFormat(valorDebito):""%>"  style="text-align:right;"  onKeyUp="sumarDebito()" onKeyPress="soloDigitos(event, 'decOK')"  type="text"  onChange="formatear(this);" size="16"  maxlength="11" align="right"></td>
                        <td align="center"><input name="valorCredito<%=x%>" id="valorCredito<%=x%>" onFocus="this.select(); " onBlur="limpiar('<%=x%>','valorCredito<%=x%>'); nuevoItem(this);"  value="<%=(valorCredito!=0)?Util.customFormat(valorCredito):""%>"    style="text-align:right;"   onKeyUp="sumarCredito();" onKeyPress="soloDigitos(event, 'decOK')"  type="text"  onChange="formatear(this);" size="16"  maxlength="11" align="right"></td>
                    </tr> 
                   <%}%>
                    <script>
                        //organizar();
                    </script>   
					   <%}%>
                    </table>
                </td>
                </tr>
                <tr >
                    <td >
                        <table width="100%">
                            <tr  class="tblTitulo" >
                                <td width="82%" ><div align="right">TOTAL</div></td>
                                <td width="9%" class="bordereporte" ><input name="valorDebito" id="valorDebito" class="fila"  value="<%=Util.customFormat(totalDebito)%>" readonly style="text-align:right; width:105; border:0; "     type="text"  onChange="formatear(this);" size="16"  maxlength="11" align="right"></td>
                                <td width="9%" class="bordereporte"><input name="valorCredito" id="valorCredito" class="fila" value="<%=Util.customFormat(totalCredito)%>" readonly   style="text-align:right; width:100; border:0;"    type="text"  onChange="formatear(this);" size="16"  maxlength="11" align="right"></td>
                            </tr>
                        </table>
                    </td>
						  
                </tr>
                </table>
            </td>
            </tr>
            </table>
            
            
            <center>
                <% String disabledUpdate="Disable"; 
                   if (isComprobante.equals("") || ( isComprobante.equals("ok") && (!opcion.equals("modificar") || (fechaApli==null || fechaApli.equals("0099-01-01 00:00:00"))))){
                       disabledUpdate = "";
                   } %>
            
                
                &nbsp;<img src='<%=BASEURL%>/images/botones/agregar<%= disabledUpdate %>.gif' name='Buscar' align="absmiddle" style='cursor:hand'  <% if (disabledUpdate.equals("")) { %> onClick="insertarItem('<%=BASEURL%>');" onMouseOver='botonOver(this);' onMouseOut='botonOut(this);' <% } %>>				
                &nbsp;<img src='<%=BASEURL%>/images/botones/aceptar<%= disabledUpdate %>.gif' name='Buscar' align="absmiddle" style='cursor:hand'  <% if (disabledUpdate.equals("")) { %> onclick="if (validarFormulario('<%=PERIODO%>')) {enviar('controllercontab?estado=Grabar&accion=Comprobante&opcion=<%=opcion%>&OP=VALIDAR&maxfila=' + maxfila ); } " onMouseOver='botonOver(this);' onMouseOut='botonOut(this);' <% } %>>
                
                <%  if(!opcion.equals("modificar")) { %>
                &nbsp;<img src='<%=BASEURL%>/images/botones/restablecer.gif' name='restablecer' align="absmiddle" style='cursor:hand'   onClick="location.href='controllercontab?estado=Grabar&accion=Comprobante&OP=LISTAR_TIPO_DOC&periodo=<%=PERIODO%>'"  onMouseOver='botonOver(this);' onMouseOut='botonOut(this);'>
		<%}%>
                &nbsp;<img src='<%=BASEURL%>/images/botones/salir.gif' name='Buscar' align="absmiddle" style='cursor:hand'   onClick="window.close();" onMouseOver='botonOver(this);' onMouseOut='botonOut(this);'>
            </center>            
	
        </form>
    </div>
            <div id="dialogMsjmultiservicio" class="ui-widget" style="display:none;top: 14px;" >
                <table id="tablainterna"  >
                    <tr>
                        <td>
                            <label style="font-family: Tahoma,Arial; padding-left: 5px;font-size: 14px; ">Numero de multiservicio</label>
                            <input type="text" id="multiser"style="width: 119px;font-family: Tahoma,Arial;color: black;font-size: 13px;" > 
                        </td>
                    </tr>
                </table>

                <table id="tabla_multiservicio" >

                </table>
            </div>
    </body>
    <iframe  width=188 height=166 name="gToday:normal:agenda.js:gfPop:plugins.js" id="gToday:normal:agenda.js:gfPop:plugins.js" src="<%=BASEURL%>/js/Calendario/ipopeng.htm" scrolling="no" frameborder="0" style="visibility:visible; z-index:999; position:absolute; left:-500px; top:0px;"> </iframe>
	<%=datos[1]%>
    <script>

        function enviar(url){
			var a = "<iframe name='ejecutor'  src='" + url + "' top='400px'  style='visibility:hidden'> ";
			aa.innerHTML = a;
        }
    </script>
    <font id='aa'></font>
</html>
