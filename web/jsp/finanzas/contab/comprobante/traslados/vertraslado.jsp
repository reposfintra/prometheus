  <!--
     - Author(s)       :      FILY STEVEN FERNANDEZ
     - Date            :      16/01/2007  
     - Copyright Notice:      TRANSPORTES SANCHEZ POLO S.A.
  -->
 <%--   - @(#)  
     - Description:  Vista  que muestra los  movimientos de cuentas auxiliar.
 --%>
<%@page session="true"%>
<%@page errorPage="../error/ErrorPage.jsp"%>
<%@page import="com.tsp.operation.model.beans.*, java.util.*, java.text.*"%>
<%@ include file="/WEB-INF/InitModel.jsp"%>
<%@ taglib uri="http://jakarta.apache.org/taglibs/input-0.90" prefix="input" %>
<%@page import="com.tsp.util.*"%>
<%@taglib uri="http://jsptags.com/tags/navigation/pager" prefix="pg" %>
<% Util u = new Util(); %>

<html>
<head>
        <title>Reporte Traslados Bancarios</title>
        <link href="<%= BASEURL %>/css/estilostsp.css" rel="stylesheet" type="text/css"> 
        <script src='<%=BASEURL%>/js/date-picker.js'></script>
		<script language="JavaScript" type="text/javascript" src="<%=BASEURL%>/js/boton.js"></script>
		<script type='text/javascript' src="<%=BASEURL%>/js/validar.js"></script> 
		<link href="../../estilostsp.css" rel="stylesheet" type="text/css">
		<link href="<%=BASEURL%>/css/estilostsp.css" rel="stylesheet" type="text/css">
		 <script>
		 	function Exportar (){
				
			    alert( 
				
					'Puede observar el seguimiento del proceso en la siguiente ruta del menu:' + '\n' + '\n' +
					'SLT -> PROCESOS -> Seguimiento de Procesos' + '\n' + '\n' + '\n' +
					'Y puede observar el archivo excel generado, en la siguiente parte:' + '\n' + '\n' +
					'En la pantalla de bienvenida de la Pagina WEB,' + '\n' +
					'vaya a la parte superior-derecha,' + '\n' +
					'y presione un click en el icono que le aparece.'
					
				);
				
			}
		</script>
        
        
</head>
<body>



<%  String path    = application.getRealPath(model.menuService.getRealUrl(request.getRequestURI()));
    String datos[] = model.menuService.getContenidoMenu(BASEURL,request.getRequestURI(),path);   
	String fecI= (request.getParameter("fechaI")!= null)?request.getParameter("fechaI"):"" ;
    String fecF=(request.getParameter("fechaF")!= null)?request.getParameter("fechaF"):"" ;
	String banco_origen= (request.getParameter("banco_Origen")!= null)?request.getParameter("banco_Origen"):"" ;
    String banco_destino=(request.getParameter("banco_Destino")!= null)?request.getParameter("banco_Destino"):"" ;
    String codcli=(request.getParameter("codcli")!= null)?request.getParameter("codcli"):"" ; %>

     
<div id="capaSuperior" style="position:absolute; width:100%; height:100px; z-index:0; left: 0px; top: 0px;">
<jsp:include page="/WEB-INF/toptsp.jsp?encabezado=Traslados Bancarios"/>
</div>

<div id="capaCentral" style="position:absolute; width:100%; height:85%; z-index:0; left: 2px; top: 100px; overflow: scroll;"> 
 <center>
 <%
	   String style = "simple";
	   String position =  "bottom";
       String index =  "center";
       int maxPageItems = 20;
       int maxIndexPages = 10;
	   String msg = request.getParameter("msg"); %>
 
 
 <%Vector vectores =  model.trasladoService.getVector();
    String msj = (request.getParameter("msg")==null)?"":request.getParameter("msg");
    if( vectores != null && vectores.size() > 0 ){
      BeanGeneral beanGeneral = (BeanGeneral) vectores.elementAt(0);
	 
			%>		

   
    <form aling= "left" name="form1" method="post" action="<%=CONTROLLER%>?estado=Traslado&accion=Aprobacion&opcion=2&val=1">
		    <img src="<%=BASEURL%>/images/botones/regresar.gif" name="c_regresar" onclick="location.href='<%=BASEURL%>/jsp/finanzas/contab/comprobante/traslados/tasladoBuscar.jsp'" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" style="cursor:hand">
	  		<input name="Guardar" src="<%=BASEURL%>/images/botones/aceptar.gif"  type="image" id="Guardar"  onMouseOver="botonOver(this);" onMouseOut="botonOut(this);">    
		    <input name="salir2" src="<%=BASEURL%>/images/botones/salir.gif" type="image" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" style="cursor:hand" class="boton" onclick="window.close();">
		    
			<input name="fechai" type="hidden" id="fechai" size="10" readonly value="<%=fecI%>">
			<input name="fechaf" type="hidden" id="fechaf" size="10" readonly value="<%=fecF%>">
			<input name="banco_origen" type="hidden" id="banco_origen" size="10" readonly value="<%=banco_origen%>">
			<input name="banco_destino" type="hidden" id="banco_destino" size="10" readonly value="<%=banco_destino%>">
      <table width="98%" border="2" align="center">
       <tr>
          <td>        
			
               
                <table width='100%' align='center' class='tablaInferior'>
                
                      <tr>
                            <td class="barratitulo" colspan='2' >
                                   <table cellpadding='0' cellspacing='0' width='100%'>
                                         <tr class="fila">
                                             <td width="50%" align="left" class="subtitulo1">&nbsp;<strong>Consulta Traslado Bancarios </strong></td>
                                             <td width="*"   align="left"><img src="<%= BASEURL %>/images/titulo.gif" width="32" height="20"  align="left"><%=datos[0]%></td>
                                        </tr>
                                   </table>   
                            </td>
                      </tr>
                      
                      
                      <tr   class="letra">
                         <td colspan='2'>
                              <table cellpadding='0' cellspacing='0' width='100%'>
                                                                   
                                       <tr>
                                         <td width='18%' class="fila" ><strong> Desde <%=fecI%> - Hasta <%=fecF%>  </strong></td>
                                        
                                       </tr>
                              </table>                         
                         </td>
              </table>
         </td>
        </tr>
          <tr>
                            <td class="barratitulo" colspan='2' >
                                   <table cellpadding='0' cellspacing='0' width='100%'>
                                         <tr class="fila">
                                             <td width="50%" align="left" class="subtitulo1"><strong>LISTADO DE TRASLADO BANCARIOS</strong></td>
                                             <td width="*"   align="left"><img src="<%= BASEURL %>/images/titulo.gif" width="32" height="20"  align="left"></td>
                                        </tr>
                                   </table>   
                            </td>
        </tr>
                      
                      
                      
                      
                      <tr class="fila">
                              <td width='100%' colspan='2'>
                                      <table width='100%' border="1" bordercolor="#999999" bgcolor="#F7F5F4" align="center">
                                      
                                           
                                            <tr class="letra">
                                                  <td colspan='10' align='right'>&nbsp;</td>
                                        </tr>  
                                      
                                      
                                            <tr class="tblTitulo" >
                                                  <TH  width='7%'  title='as click para modificar'        nowrap style="font size:11; font weight: bold" >MODIFICAR ITEM</TH>
												  <TH  width='9%'  title=''      nowrap style="font size:11; font weight: bold" >FECHA</TH>
                                                  <TH  width='9%'  title=''      nowrap style="font size:11; font weight: bold" >BANCO DE ORIGEN </TH>
                                                  <TH  width='10%' title=''          nowrap style="font size:11; font weight: bold" >SUCURSAL DEL BANCO ORIGEN </TH>
                                                  <TH  width='8%'  title=''       nowrap style="font size:11; font weight: bold" >BANCO DE DESTINO </TH>
												  <TH  width='8%'  title=''       nowrap style="font size:11; font weight: bold" >SUCURSAL DEL BANCO DESTINO </TH>                    
                                                  <TH  width='15%' title='' nowrap style="font size:11; font weight: bold" >VALOR TRASLADO </TH>
                                                  <TH  width='5%'  title=''         nowrap style="font size:11; font weight: bold" >COMPROBANTE</TH>               
                                                  <TH  width='10%' title=''       nowrap style="font size:11; font weight: bold" >APROBADO
                                                      
                                              </TH> 
												         <!-- Movimientos--> 
                                          
																				  
										</tr>	
													<pg:pager
													 items="<%=vectores.size()%>"
													 index="<%= index %>"
													 maxPageItems="<%= maxPageItems %>"
													 maxIndexPages="<%= maxIndexPages %>"
													 isOffset="<%= true %>"
													 export="offset,currentPageNumber=pageNumber"
													 scope="request">					
											<%
												int cont = 1;
												for (int i = offset.intValue(), l = Math.min(i + maxPageItems, vectores.size()); i < l; i++){
													BeanGeneral info = (BeanGeneral) vectores.elementAt(i);
										  %>  	  
													
													<pg:item>
													  <tr class="<%=i%2==0?"filagris":"filaazul"%>" onMouseOver='cambiarColorMouse(this)' style="cursor:hand" title="Click" align="center" > 
													  <td nowrap align="center" class="bordereporte" title="as click para modificar" onClick="window.open('<%=CONTROLLER%>?estado=Traslado&accion=Modificar&opcion=1&bancoOri=<%=info.getValor_01()%>&SucBancoOri=<%=info.getValor_02()%>&bancoDes=<%=info.getValor_03()%>&SucBancoDes=<%=info.getValor_04()%>&valor=<%=info.getValor_05()%>&fecha=<%=info.getValor_06()%>&comprobante=<%=info.getValor_07()%>' ,'M','status=yes,scrollbars=no,width=780,height=650,resizable=yes');"> <%=cont%>&nbsp;</td>													  <td nowrap align="left" class="bordereporte"><%=(info.getValor_06()!=null)?info.getValor_06():""%>&nbsp;</td>
													  <td nowrap align="left" class="bordereporte"><%=(info.getValor_01()!=null)?info.getValor_01():""%>&nbsp;</td> 
													  <td nowrap align="left" class="bordereporte"><%=(info.getValor_02()!=null)?info.getValor_02():""%>&nbsp;</td>
													  <td nowrap align="left" class="bordereporte"><%=(info.getValor_03()!=null)?info.getValor_03():""%>&nbsp;</td>
													  <td nowrap align="left" class="bordereporte"><%=(info.getValor_04()!=null)?info.getValor_04():""%>&nbsp;</td>
													  <td nowrap align="right" class="bordereporte"><%=(info.getValor_05()!=null)?UtilFinanzas.customFormat( Double.parseDouble(info.getValor_05())):""%>&nbsp;</td>
													  <td nowrap align="left" class="bordereporte"><%=(info.getValor_07()!=null)?info.getValor_07():""%>&nbsp;</td>
													  <td nowrap align="left" class="bordereporte"><input name="aprobados" id="aprobado" type="checkbox"  value="<%=info.getValor_09()%>" <%if(info.getValor_08().equals("S")){%>checked="true" disabled><%}else{%> <%}%></td>
													    
													  
														
													</tr>
												  </pg:item>	
											<%cont = cont + 1; 
											}%>
											<tr class="bordereporte">
											  <td td height="20" colspan="10" nowrap align="center">
											   <pg:index>
												<jsp:include page="/WEB-INF/jsp/googlesinimg.jsp" flush="true"/>      
											   </pg:index> 
											  </td>
											</tr>
											</pg:pager>
								</table>
								  
                        </td>
                     </tr>
  </table>               
            
          </td>
       </tr>
     </table>  
     
		     
		    <img src="<%=BASEURL%>/images/botones/regresar.gif" name="c_regresar" onclick="location.href='<%=BASEURL%>/jsp/finanzas/contab/comprobante/traslados/tasladoBuscar.jsp'" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" style="cursor:hand">
			<input name="Guardar" src="<%=BASEURL%>/images/botones/aceptar.gif"  type="image" id="Guardar"  onMouseOver="botonOver(this);" onMouseOut="botonOut(this);">    
	  		<input name="salir2" src="<%=BASEURL%>/images/botones/salir.gif" type="image" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" style="cursor:hand" class="boton" onclick="window.close();">
  </form>	
	<%}%>   
 <br>
    <% if( ! msj.equals("") ){%>
          <table border="2" align="center">
                  <tr>
                    <td>
                        <table width="100%"  border="1" align="center"  bordercolor="#F7F5F4" bgcolor="#FFFFFF">
                              <tr>
                                    <td width="400" align="center" class="mensajes"><%=msj%></td>
                                    <td width="29" background="<%=BASEURL%>/images/cuadronaranja.JPG">&nbsp;</td>
                                    <td width="58">&nbsp; </td>
                              </tr>
                      </table>
                    </td>
                  </tr>
          </table>
          
          <!--botones -->   
		
      <%}%>
</div>
<%=datos[1]%>  
</body>
</html>