<!--  
	 - Author(s)       :      FFERNANDEZ
	 - Description     :      AYUDA DESCRIPTIVA - Consultar Saldos Contables
	 - Date            :      16/06/2006 
	 - Copyright Notice:      TRANSPORTES SANCHEZ POLO S.A.
-->
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN""http://www.w3.org/TR/html4/loose.dtd">
<%@page session="true"%> 
<%@page errorPage="/error/ErrorPage.jsp"%>
<%@page import="com.tsp.operation.model.beans.*, java.util.*"%>
<%@ include file="/WEB-INF/InitModel.jsp"%>
<%@ taglib uri="http://jakarta.apache.org/taglibs/input-0.90" prefix="input" %>
<html>
<head>
<title>AYUDA DESCRIPTIVA - ingresar Traslado Bancario</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link href="<%= BASEURL %>/css/estilostsp.css" rel="stylesheet" type="text/css">
<script type='text/javascript' src="<%= BASEURL %>/js/boton.js"></script>
</head>

<body>
<br>
<table width="696"  border="2" align="center">
  <tr>
    <td width="811" >
      <table width="100%" border="0" align="center">
        <tr  class="subtitulo">
          <td height="24" colspan="2"><div align="center">Traslados Bancario Adicionar </div></td>
        </tr>
        <tr class="subtitulo1">
          <td colspan="2">Ingresar traslado bancario - Pantalla Preliminar</td>
        </tr>
		<tr class="subtitulo1">
          <td colspan="2">INFORMACION DE TRASLADO BANCARIO </td>
        </tr>
        <tr>
          <td width="18%"  class="fila">Banco de Origen </td>
          <td width="82%"  class="ayudaHtmlTexto"><p> Para el banco de origen busque en la caja cerrada el banco que necesita para poder hacer el traslado </p>            </td>
        </tr>
        <tr>
          <td  class="fila">Banco Destino </td>
          <td  class="ayudaHtmlTexto">Para el banco de Destino busque en la caja cerrada el banco que necesita para poder hacer el traslado </td>
        </tr>
        <tr>
          <td  class="fila">Sucursal Origen </td>
          <td  class="ayudaHtmlTexto"> Para la sucursal se necesita saber el banco antes de cargar las sucursales en este caso el banco de Origen y as&iacute; podr&aacute; cargar en la caja cerrada la sucursal de Origen </td>
        </tr>
        <tr>
          <td  class="fila">Sucursal Destino </td>
          <td  class="ayudaHtmlTexto">Para la sucursal se necesita saber el banco antes de cargar las sucursales en este caso el banco de Destino y as&iacute; podr&aacute; cargar en la caja cerrada la sucursal de Destino </td>
        </tr>
        <tr>
          <td  class="fila">Valor</td>
          <td  class="ayudaHtmlTexto"><p>Ingrese el valor o el dineros a trasladar de banco </p></td>
        </tr>
        <tr>
          <td  class="fila">Fecha</td>
          <td  class="ayudaHtmlTexto"> Ingrese la fecha que se realiza el traslado de banco </td>
        </tr>
        
      </table>
    </td>
  </tr>
</table>
<p></p>
<center>
<img src = "<%=BASEURL%>/images/botones/salir.gif" style = "cursor:hand" name = "imgsalir" onClick = "parent.close();" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);">
</center>
</body>
</html>
