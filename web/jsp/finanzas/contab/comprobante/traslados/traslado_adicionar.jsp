<%@page contentType="text/html"%>

<%@page import="com.tsp.operation.model.beans.*, java.util.*, com.tsp.util.*"%>
<%@taglib uri="/WEB-INF/tlds/tagsAgencia.tld" prefix="ag"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib uri="http://jakarta.apache.org/taglibs/input-0.90" prefix="input" %>
<%@include file="/WEB-INF/InitModel.jsp"%>
<% Util u = new Util(); %>
<%
  String msg = (request.getParameter("msg")!=null)?request.getParameter("msg"):"";
  String path = application.getRealPath(model.menuService.getRealUrl(request.getRequestURI()));
  String datos[] = model.menuService.getContenidoMenu(BASEURL,request.getRequestURI(),path);
  String Est =(request.getParameter ("Estado")!=null)?request.getParameter ("Estado"):"";
  String apro =(request.getParameter ("aprobado")!=null)?request.getParameter ("aprobado"):"";
  String des =(request.getParameter ("des")!=null)?request.getParameter ("des"):"";
  TreeMap BancoOrigen = model.servicioBanco.getBanco();
  TreeMap SucBanco = model.servicioBanco.getSucursal();
  TreeMap SucBancoDes = model.servicioBanco.getSucursalDes();
  BancoOrigen.put( "Seleccione", "");
  SucBanco.put( "Seleccione", "");
  SucBancoDes.put( "Seleccione", "");
  Usuario usuario  = (Usuario) session.getAttribute("Usuario");
  String agen =usuario.getId_agencia();
  
  String defaultBancoOrigen     = Util.coalesce( ((String) request.getParameter("bancoOri")), "" );
  String defaultBancoDestino    = Util.coalesce( ((String) request.getParameter("bancoDes")), "" );  
  String defaultSucursalOrigen  = Util.coalesce( ((String) request.getParameter("SucBancoOri")), "" );
  String defaultSucursalDestino = Util.coalesce( ((String) request.getParameter("SucBancoDes")), "" );  
  String defaultvalor = Util.coalesce( ((String) request.getParameter("valor")), "" );  
  String defaultFecha = Util.coalesce( ((String) request.getParameter("fecha")), Utility.getHoy("-") );  
  
 %>
<html>
<head>
<title>Ingresar Traslado bancario</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link href="../../../css/estilostsp.css" rel="stylesheet" type="text/css"> 
<link href="<%=BASEURL%>/css/estilostsp.css" rel="stylesheet" type="text/css"> 
</head>
<script type='text/javascript' src="<%=BASEURL%>/js/boton.js"></script> 
<script type='text/javascript' src="<%= BASEURL %>/js/validar.js"></script>
<script type='text/javascript' src="<%= BASEURL %>/js/reporte.js"></script>
<script type='text/javascript' src="<%= BASEURL %>/js/general.js"></script>
<script language="JavaScript" type="text/javascript" src="<%=BASEURL%>/js/script.js"></script> 

<body onResize="redimensionar()" onLoad="redimensionar();" onKeyDown="onKeyDown();">
<div id="capaSuperior" style="position:absolute; width:100%; height:100px; z-index:0; left: 0px; top: 0px;">
<jsp:include page="/WEB-INF/toptsp.jsp?encabezado=Ingresar Datos de Traslados Bancarios"/>
</div>
<div id="capaCentral" style="position:absolute; width:100%; height:85%; z-index:0; left: 79px; top: 108px; overflow: scroll;"> 

<form name="formulario" id="formulario" action="<%=CONTROLLER%>?estado=TrasladoB&accion=Insertar" method="post">   
 	
  <table width="500" border="2" align="center">
    <tr>
      <td width="500">
		<table width="99%"   align="center"> 
		  <tr>
			<td width="339" height="22"  class="subtitulo1"><p align="left">Traslado bancario</p></td>
			<td width="138"  class="barratitulo"><img src="<%=BASEURL%>/images/titulo.gif" width="32" height="20" align="left"><%=datos[0]%></td>
		  </tr>
		</table>
		<table width="500" height="216"  align="center" >
           <tr><td colspan="5" height="5"  class="subtitulo1"><p align="left">Banco Origen</p></td>
		       
		   </tr>
          <tr class="fila">
            <td height="20">Banco                </td>
            <td height="20"><input:select name="bancoOri" attributesText="class=textbox onChange=\"SucBancoOri.value=''; loadSucursalesOri();\"" options="<%=BancoOrigen%>" default="<%= defaultBancoOrigen %>"/><img  src="<%=BASEURL%>/images/botones/iconos/obligatorio.gif" width="10" height="10"></td>
            <td height="20">Sucursal</td>
            <td><input:select name="SucBancoOri" attributesText="class=textbox" options="<%=SucBanco %>"  default="<%= defaultSucursalOrigen %>" /><img  src="<%=BASEURL%>/images/botones/iconos/obligatorio.gif" width="10" height="10"> </td>
          </tr>
		   <tr><td colspan="5" height="5"  class="subtitulo1"><p align="left">Banco Destino</p></td>
		    <input name="agenciaFact" type="hidden"class="textbox" value="<%=agen%>"  >   
		   </tr>
          <tr class="fila">
            <td height="20">Banco                </td>
            <td height="20"><input:select name="bancoDes" attributesText="class=textbox onChange=\"SucBancoDes.value=''; loadSucursalesDes();\"" options="<%=BancoOrigen%>" default="<%= defaultBancoDestino %>"/><img  src="<%=BASEURL%>/images/botones/iconos/obligatorio.gif" width="10" height="10"></td>
            <td height="20">Sucursal</td>
            <td><input:select name="SucBancoDes" attributesText="class=textbox" options="<%=SucBancoDes %>" default="<%= defaultSucursalDestino %>"/><img  src="<%=BASEURL%>/images/botones/iconos/obligatorio.gif" width="10" height="10"> </td>
          </tr>
		  
		   <tr><td colspan="5" height="5"  class="subtitulo1"><p align="left">Valores</p></td>
		       
		   </tr>
          <tr class="fila">
            <td height="20">Valor                </td>
			
		    <%	
                
			if(defaultvalor ==  null){
				defaultvalor = "0";
			}%>
			<%String valor_aux = (!defaultvalor.equals(""))?UtilFinanzas.customFormat(Double.parseDouble(defaultvalor.replace(",",""))):""; %>
			<%System.out.println("default2"+defaultvalor); %>
            <td height="20"><input name="valor" type="text" class="textbox" id="valor" size="20" maxlength="13" value="<%=valor_aux%>"  onKeyPress="soloDigitos(event,'decOK')" onkeyup="if(this.value!='')formatear(this);">
              <img  src="<%=BASEURL%>/images/botones/iconos/obligatorio.gif" width="10" height="10"> </td>
            <td height="20">Fecha </td>
            <td height="20"><input name="fecha" type="text" class="textbox" id="fecha" size="10" readonly value="<%= defaultFecha %>">
               <span class="Letras"><a href="javascript:void(0)" onClick="jscript: show_calendar('fecha');" hidefocus> </a><img src="<%=BASEURL%>/images/botones/iconos/obligatorio.gif" name="obligatorio"> <img src="<%=BASEURL%>/images/cal.gif" width="16" height="16" align="absmiddle" style="cursor:hand " onClick="if(self.gfPop)gfPop.fPopCalendar(document.formulario.fecha);return false;" hidefocus></span></td>
          </tr>
		
        </table></td>
    </tr>
  </table>
  <table width="699" border="0" align="center">
  <tr>
  <script>	
  	
		   
		   function sub(){
				var valor = formulario.valor.value;
				if( valor == '' ) valor = '0';
		         	var vlr  = parseFloat ( replaceALL(valor,',','') ); 
					//alert(vlr);
  					var s = formulario.action;
	                s += "&valor="+vlr;

    		       formulario.action = s;
		   			formulario.submit();
		   }

           
</script>
				
    <td colspan="2" nowrap align="center">
<img src="<%=BASEURL%>/images/botones/restablecer.gif" width="87" name="buss" height="21" align="absmiddle" style="cursor:hand"   onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" onClick="document.location='<%=CONTROLLER%>?estado=Menu&accion=Cargar&carpeta=/jsp/finanzas/contab/comprobante/Comprobantes&pagina=traslado_adicionar.jsp&marco=no&opcion=33&item=cargando'">
<img src="<%=BASEURL%>/images/botones/aceptar.gif"  name="Guardar" onClick="if( validar( formulario ) ){ sub(); }" style = "cursor:hand" align="middle" type="image" id="Guardar"  onMouseOver="botonOver(this);" onMouseOut="botonOut(this);">                  
<img src="<%=BASEURL%>/images/botones/salir.gif"  name="imgsalir" align="absmiddle" style="cursor:hand" onClick="parent.close();" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);"> </td>
    </tr>
</table>
</form>
<%if(!msg.equals("")){%>
	<p>
   <table width="407" border="2" align="center">
     <tr>
    <td><table width="100%"   align="center"  >
      <tr>
        <td width="229" align="center" class="mensajes"><%=msg%></td>
        <td width="29" background="<%=BASEURL%>/images/cuadronaranja.JPG">&nbsp;</td>
        <td width="58">&nbsp;</td>
      </tr>
    </table></td>
  </tr>
</table>
  </p>
   <%}%>
</div>
<iframe width=188 height=166 name="gToday:normal:agenda.js:gfPop:plugins.js" id="gToday:normal:agenda.js:gfPop:plugins.js" src="<%=BASEURL%>/js/Calendario/ipopeng.htm" scrolling="no" frameborder="0" style="visibility:visible; z-index:999; position:absolute; left:-500px; top:0px;"> </iframe>    
<%=datos[1]%>
</body>
</html>
<script>

function loadSucursalesOri(){
	formulario.action = '<%= CONTROLLER%>?estado=Cargar&accion=BancosDestino&tipo=ORI&opc=ing';
	formulario.submit();
}

	
function loadSucursalesDes(){
	formulario.action = '<%= CONTROLLER%>?estado=Cargar&accion=BancosDestino&tipo=DES&opc=ing';
	formulario.submit();
}

 function validar( formulario ){
   		if ( formulario.bancoOri.value == ''  ){
			alert( 'Debe Ingresar la nombre del banco de origen para continuar...' );
			return false;
		}
		
		if ( formulario.SucBancoOri.value == ''  ){
			alert( 'Debe Ingresar la sucursal del banco de origen para continuar...' );
			return false;
		}
        if ( formulario.bancoDes.value == '' ){
			alert( 'Debe Ingresar la nombre del banco de destino para continuar...' );
			return false;
		}
		if ( formulario.SucBancoDes.value == ''  ){
			alert( 'Debe Ingresar la sucursal del banco de Destino para continuar...' );
			return false;
		}
		 if ( formulario.valor.value == ''){
			alert( 'Debe Ingresar el valor para poder continuar...' );
			return false;
		}
		 if ( formulario.fecha.value == ''){
			alert( 'Debe Ingresar la fecha para poder continuar...' );
			return false;
		}
		return true;
}
</script>
