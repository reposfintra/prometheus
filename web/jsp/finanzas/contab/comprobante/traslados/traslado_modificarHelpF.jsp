<!--  
	 - Author(s)       :      LREALES
	 - Description     :      AYUDA FUNCIONAL - Consultar Saldos Contables
	 - Date            :      16/06/2006 
	 - Copyright Notice:      TRANSPORTES SANCHEZ POLO S.A.
-->
<%@ include file="/WEB-INF/InitModel.jsp"%>
<HTML>
<HEAD>
<TITLE>AYUDA FUNCIONAL - Consultar Saldos Contables</TITLE>
<META http-equiv=Content-Type content="text/html; charset=windows-1252">
<link href="../css/estilostsp.css" rel="stylesheet" type="text/css"> 
<link href="<%=BASEURL%>/css/estilostsp.css" rel="stylesheet" type="text/css">
<script type='text/javascript' src="<%= BASEURL %>/js/boton.js"></script> 
</HEAD>
<BODY> 
<% String BASEIMG = BASEURL +"/images/ayuda/finanzas/traslados/"; %>
  <table width="95%"  border="2" align="center">
    <tr>
      <td height="117" >
        <table width="100%" border="0" align="center">
          <tr  class="subtitulo">
            <td height="20"><div align="center">MANUAL DE CONTABILIDAD WEB</div></td>
          </tr>
          <tr class="subtitulo1">
            <td>Descripci&oacute;n del funcionamiento del programa para Traslados Bancarios </td>
          </tr>
          <tr>
            <td  class="ayudaHtmlTexto"><div align="center">
              <p>&nbsp;</p>
              </div></td>
          </tr>
          <tr>
            <td  class="ayudaHtmlTexto"><p>En la siguiente pantalla muestra el programa modificar con los campos cargados y listos para ser cambiados...</p>            </td>
          </tr>
          <tr>
            <td  class="ayudaHtmlTexto"><div align="center"><IMG height=293 src="<%=BASEIMG%>imagen006.JPG" width=559 border=0 v:shapes="_x0000_i1143"></div></td>
          </tr>
          <tr>
            <td  class="ayudaHtmlTexto"><p>&nbsp;</p>
            <p>El sistema muestra cuando se hizo modificado exitosamente </p></td>
          </tr>
          <tr>
            <td  class="ayudaHtmlTexto"><div align="center"><IMG height=149 src="<%=BASEIMG%>imagen007.JPG" width=559 border=0 v:shapes="_x0000_i1054"></div></td>
          </tr>
		   
      </table></td>
    </tr>
  </table>
  <p></p>
<center>
<img src = "<%=BASEURL%>/images/botones/salir.gif" style = "cursor:hand" name = "imgsalir" onClick = "parent.close();" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);">
</center>
</BODY>
</HTML>
