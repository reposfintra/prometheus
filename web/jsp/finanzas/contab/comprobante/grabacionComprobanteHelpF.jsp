<!--
- Autor : Ing. Sandra Escalante
- Modificado: Ing Ivan Dario Gomez
- Date  : 02 Febrero 2006
- Copyrigth Notice : Fintravalores S.A. S.A
-->
<%--
-@(#)
--Descripcion : Vista para mostrar la informacion de las ayudas
--%>
<%@ include file="/WEB-INF/InitModel.jsp"%>

<HTML>
<HEAD>
<TITLE></TITLE>
<META http-equiv=Content-Type content="text/html; charset=windows-1252">
<link href="../../../../css/estilostsp.css" rel="stylesheet" type="text/css">  
<link href="<%=BASEURL%>/css/estilostsp.css" rel="stylesheet" type="text/css"> 
</HEAD>
<BODY>
<% String BASEIMG = BASEURL +"/images/ayuda/finanzas/comprobante/"; %> 
  <table width="95%"  border="2" align="center">
    <tr>
      <td height="1" >      <table width="100%" border="0" align="center">
        <tr  class="subtitulo">
          <td height="20"><div align="center">MANUAL PARA LA GRABACION DE COMPROBANTES </div></td>
        </tr>
        <tr class="subtitulo1">
          <td>Descripci&oacute;n del funcionamiento del programa para la grabacion de comprobante contable </td>
        </tr>
        <tr>
          <td  height="18"  class="ayudaHtmlTexto">El programa para la grabacion de comprobantes contables consta de una cabecera y un detalle. En la cabecera se capturan datos tales como el tipo de documento, que es un combo que contiene los tipos de documentos que se pueden grabar en este programa.</td>
        </tr>
        <tr>
          <td height="18"  class="ayudaHtmlTexto">&nbsp;</td>
        </tr>
        <tr>
          <td  class="ayudaHtmlTexto"><div align="center"><img src="<%=BASEIMG%>figura1.JPG" width="937" height="411"> </div></td>
        </tr>
        <tr>
          <td  class="ayudaHtmlTexto">Dependiendo del tipo de documento los campos Numero Doc y Tercero se veran afectados: </td>
        </tr>
        <tr>
          <td  class="ayudaHtmlTexto"><p><strong> Numero Doc : </strong>si el tipo de documento tiene prefijos, el numero de documento se conforma de los prefijos dependiendo de como esten en la base de datos como se muestra en la figura 2. Si el tipo doc no maneja serie aparecera un campo para ingresar la serie ver figura 2, si maneja serie, el numero de documento no podra ser modificado. </p>
          </td>
        </tr>
        <tr>
          <td  class="ayudaHtmlTexto">&nbsp;</td>
        </tr>
        <tr>
          <td  class="ayudaHtmlTexto"><strong>Tercero: </strong>Si el tipo de documento no requiere de tercero el campo tercero no aparecera en la pantalla, como se muestra en la figura 2. </td>
        </tr>
        <tr>
          <td  class="ayudaHtmlTexto"><div align="center">
            <p><img src="<%=BASEIMG%>figura4.JPG" width="911" height="339"> </p>
            </div></td>
        </tr>
        <tr>
          <td  class="ayudaHtmlTexto"><strong>Busqueda de tercero </strong></td>
        </tr>
        <tr>
          <td  class="ayudaHtmlTexto"><div align="left">Para la busqueda del tercero debe dar click en la lupa y se desplegara una pantalla como lo muestra en la fugura 3.</div></td>
        </tr>
        <tr>
          <td  class="ayudaHtmlTexto"><div align="left">para buscar debe dar click en el boton Buscar o presionar Enter, puedo escribir la cedula o el nombre.</div></td>
        </tr>
        <tr>
          <td  class="ayudaHtmlTexto"><div align="center"><img src="<%=BASEIMG%>figura3.JPG" width="556" height="244"></div></td>
        </tr>
        <tr>
          <td  class="ayudaHtmlTexto"><div align="justify">si la busqueda es por nombre como se muestra en la figura 3, se le listaran todos los nombres que contengan lo que escribio, como se muestra en la figura 4. </div></td>
        </tr>
        <tr>
          <td  class="ayudaHtmlTexto">Para asignar el codigo en el Tercero debera dar click sobre el nombre. </td>
        </tr>
        <tr>
          <td  class="ayudaHtmlTexto"><div align="center"><img src="<%=BASEIMG%>figura5.JPG" width="610" height="316"></div></td>
        </tr>
        <tr>
          <td  class="ayudaHtmlTexto">&nbsp;</td>
        </tr>
        <tr>
          <td  class="ayudaHtmlTexto"><div align="justify"><strong>Detalle del Documento </strong></div></td>
        </tr>
        <tr>
          <td  class="ayudaHtmlTexto">En el detalle se encuentran los item del comprobante, en los cuales podemos grabar el numero de la cuenta y apartir de esta sabemmos si esta requiere de auxiliar y tercero, dependiendo de lo mensionado apareceran los campos. </td>
        </tr>
        <tr>
          <td  class="ayudaHtmlTexto">&nbsp;</td>
        </tr>
        <tr>
          <td  class="ayudaHtmlTexto">&nbsp;</td>
        </tr>
        <tr>
          <td  class="ayudaHtmlTexto"><div align="center"><img src="<%=BASEIMG%>figura6.JPG" width="946" height="430"></div></td>
        </tr>
        <tr>
          <td  class="ayudaHtmlTexto">&nbsp;</td>
        </tr>
        <tr>
          <td  class="ayudaHtmlTexto">&nbsp;</td>
        </tr>
        <tr>
          <td  class="ayudaHtmlTexto">&nbsp;</td>
        </tr>
        <tr>
          <td  class="ayudaHtmlTexto">&nbsp;</td>
        </tr>
        <tr>
          <td  class="ayudaHtmlTexto">&nbsp;</td>
        </tr>
        <tr>
          <td  class="ayudaHtmlTexto">&nbsp;</td>
        </tr>
        <tr>
          <td  class="ayudaHtmlTexto">&nbsp;</td>
        </tr>
        <tr>
          <td  class="ayudaHtmlTexto">&nbsp;</td>
        </tr>
        <tr>
          <td  class="ayudaHtmlTexto"><table cellspacing="0" cellpadding="0">
              <tr>
                <td width="960" class="ayudaHtmlTexto">&nbsp;</td>
              </tr>
          </table>
            <div align="center"></div></td>
        </tr>
      </table></td>
    </tr>
  </table>
</BODY>
</HTML>
