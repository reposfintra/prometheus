<%@page session="true"%> 
<%@page import="java.util.*" %>
<%@page import="com.tsp.util.*"%>
<%@page errorPage="/error/ErrorPage.jsp"%>
<%@include file="/WEB-INF/InitModel.jsp"%>

<% String idcampo = request.getParameter("campo");  
   String opcion  = request.getParameter("opcion");
   String codigo  = request.getParameter("codigo");
   String maxfila = (request.getParameter("maxfila")!= null)?request.getParameter("maxfila"):"";
   String periodo = request.getParameter("periodo");
   String op      = (request.getParameter("op")!=null)?request.getParameter("op"):"";
 %>
<script>
  
  var maxfila = '<%=maxfila%>';
  var html="";
  <% if (opcion.equals("TIPO")){
         List listTipoDoc = modelcontab.GrabarComprobanteSVC.getLista();
		 String cod ="";
		 
         for(int i=0; i< listTipoDoc.size();i++){
		 
              Hashtable fila = (Hashtable)listTipoDoc.get(i);
			  cod  = (String) fila.get("codigo");
			   
			  if(codigo.equals(cod)){
					 String descripcion    = (String) fila.get("descripcion");
					 String codigo_interno = (String) fila.get("codigo_interno");
					 String tercero        = (String) fila.get("tercero");
					 String maneja_serie   = (String) fila.get("maneja_serie");
					 String serie_ini      = (String) fila.get("serie_ini");
					 String serie_act      = (String) fila.get("serie_act");
 					 String serie_actual   = (String) fila.get("serie_actual");
					 String prefijo        = (String) fila.get("prefijo");
					 String prefijo_anio   = (String) fila.get("prefijo_anio");
					 String prefijo_mes    = (String) fila.get("prefijo_mes");
					 String serie          = "";
					 String periodoConta   = (String) session.getAttribute("periodo");
					 
					
					 
					 if(maneja_serie.equals("S")){
						 serie = prefijo;
						 if(prefijo_anio.length() == 2) {
							serie += periodoConta.substring(2,4);							
						 }else if(prefijo_anio.length() == 4){
						   serie += periodoConta; 
						 }  
						 if(prefijo_mes.length() == 2) {
							serie += periodoConta.substring(4,6);							
						 }  						 
						 serie += UtilFinanzas.rellenar(serie_actual,"0",3); 
					   %>parent.TexNumDoc.innerHTML ="<input type='text' name='NumDoc' value='<%=serie%>' id='NumDoc' style='text-align:center;' onblur='validarFormatoSerie()' >"<%
					 }else{
					   %>parent.TexNumDoc.innerHTML ="<input type='hidden' name='serie' value='<%=serie%>' style='border:0;width:80;' class='filaresaltada' readonly id='serie' ><input type='text' name='NumDoc' id='NumDoc'  maxlength='3' value='' style='text-align:center; '>"
					   <%
					 }
						
						
					 if(tercero.equals("M")|| tercero.equals("O") ){
					  %>
					    html = "<input type='text' name='terceroCabecera' readonly >&nbsp;<img src='<%=BASEURL%>/images/botones/iconos/lupa.gif'  width='15' height='15'   title='Buscar' style='cursor:hand'  onClick=\"buscarTercero('<%=BASEURL%>','terceroCabecera');\"> ";
						parent.TexterceroCabecera.innerHTML = html;
					  <%    
					 
					}else{
					%>
					  html = "<input type='text' name='terceroCabecera' style='border:0;text-align:center;'  class='filaresaltada'  readonly value=''>";
					  parent.TexterceroCabecera.innerHTML = html;<%
					}
				 
				
                
			  }// fin  if(codigo.equals(cod))
			  
			  
		 }// fin for%>
   
  
       
    
   <%}//FIn OPCION == TIPO
   
    if (opcion.equals("CUENTA")){
	    Hashtable cuenta       = modelcontab.GrabarComprobanteSVC.getCuenta();
		Vector    CodSubledger = modelcontab.subledgerService.getCodigos(); 
		String Codcuenta = (String)cuenta.get("cuenta");
        String subledger = (String)cuenta.get("subledger");
        String tercero   = (String)cuenta.get("tercero");
		if(tercero.equals("M")|| tercero.equals("O") || tercero.equals("S")){
		  %>
		    html = "<input type='text' name='tercero<%=idcampo%>' id='tercero<%=idcampo%>' maxlength='15' style='width:90' readonly >&nbsp;<img src='<%=BASEURL%>/images/botones/iconos/lupa.gif'  width='15' height='15'   title='Buscar' style='cursor:hand'  onClick=\"buscarTercero('<%=BASEURL%>','tercero<%=idcampo%>');\"><input type='hidden'   name='ReqTercero<%=idcampo%>' id='ReqTercero<%=idcampo%>' value='<%=tercero.equals("M")?"SI":""%>'> ";
			parent.Textercero<%=idcampo%>.innerHTML = html;
		  <%    
		 
		}else{
		%>
		  html = "<input type='hidden' name='tercero<%=idcampo%>' id='tercero<%=idcampo%>' maxlength='15' style='width:128;border:0;' class='filaresaltada' readonly><input type='hidden'   name='ReqTercero<%=idcampo%>' id='ReqTercero<%=idcampo%>' value=''> ";
		  parent.Textercero<%=idcampo%>.innerHTML = html;<%
		}
		
		if(subledger.equals("S")){
           %>html = "<select name='subledger<%=idcampo%>' style='width:40; ' onChange=\"forma1.idSubledger<%=idcampo%>.value='';\">";<%  		
		   for(int i=0; i < CodSubledger.size();i++){
		       
			   %>
			     html +="<option value='<%=CodSubledger.get(i)%>' ><%=CodSubledger.get(i)%> </option>";
				<%
           }
		   %>
		   
		   html +="</select><input type='text' maxlength='25' style='width:90' name='idSubledger<%=idcampo%>' id='idSubledger<%=idcampo%>' readonly>&nbsp;<img src='<%=BASEURL%>/images/botones/iconos/lupa.gif'  width='15' height='15'  onClick=\"buscarLista('SQL_SUBLEDGER', forma1.subledger<%=idcampo%>.value+'-_-   and cuenta = -_-'+ forma1.CodContable<%=idcampo%>.value, idSubledger<%=idcampo%>.name,'LISTA DE AUXILIARES');\" title='Buscar' style='cursor:hand' ><input type='hidden'   name='ReqAuxiliar<%=idcampo%>' id='ReqAuxiliar<%=idcampo%>' value='S'> ";
		   parent.Texsubledger<%=idcampo%>.innerHTML = html;
           <%
		}else{
		%>html = "<select name='subledger<%=idcampo%>'  style='width:40;border:0;visibility:hidden' class='filaresaltada'>"
		  html +="<option value='' ></option>"
		  html +="</select><input type='hidden' maxlength='25' style='width:90'  readonly name='idSubledger<%=idcampo%>' id='idSubledger<%=idcampo%>'><input type='hidden'   name='ReqAuxiliar<%=idcampo%>' id='ReqAuxiliar<%=idcampo%>' value=''> ";
		  parent.Texsubledger<%=idcampo%>.innerHTML = html; <%
		}
	
	}//FIN DE OPCION "CUENTA"
	
    if (opcion.equals("VALIDAR")){
	    %>
		
		var periodo = '<%=periodo%>';
		var error = "";
		var Tipo   =  parent.document.getElementById("TipoDoc");
		var NumDoc =  parent.document.getElementById("NumDoc");
		var Fecha  =  parent.document.getElementById("fecha_documento");
		var terCabecera = parent.document.getElementById("terceroCabecera");
		var concepto    = parent.document.getElementById("concepto");
		
		var fec = Fecha.value.replace(/-/gi,'').substr(0,6);
		
		if(Tipo.value==''){
		   alert("Debe seleccionar un tipo de documento")
		   Tipo.focus();
		   error = "SI";
		   
		}else if (NumDoc.value==''){
		    alert("El numero del documento no puede estar vacio..")
			NumDoc.focus();
		    error = "SI";
		}else if(Fecha.value=='' ){
		    alert("La fecha no puede estar vacia");
			Fecha.focus();
			error = "SI";
		}else if(fec != periodo){
		    alert("La fecha debe estare dentro del periodo "+periodo);
			Fecha.focus();
			error = "SI";
		}else if(concepto.value == ''){
		    alert("EL campo concepto no puede estar vacio");
			concepto.focus();
			error = "SI";
		}
		
		if(error != 'SI'){
		  <%
		    List listTipoDoc = modelcontab.GrabarComprobanteSVC.getLista();
		    String cod ="";
		    for(int i=0; i< listTipoDoc.size();i++){
		       Hashtable fila = (Hashtable)listTipoDoc.get(i);
			   cod  = (String) fila.get("codigo");
		  %> 
				if(Tipo.value == '<%=cod%>'){
					var tercero = '<%=(String) fila.get("tercero")%>';
					if((tercero =='M')&&(terCabecera.value =='')){
					   alert("Este tipo de documento requiere de tercero");
					   terCabecera.focus();
					   error = "SI";
					   
					}
					
				   
				}    
		  <%}%>
		 
		    if(error != 'SI'){
			   var Item =0;
			   for(var i =1; i <= maxfila; i++){
			      var CodContable =  parent.document.getElementById("CodContable"+i);
				  
				  if(CodContable != null){
				     Item ++;
				     var tercero     = parent.document.getElementById("tercero"+i);
					 var ReqTercero  = parent.document.getElementById("ReqTercero"+i);
				     var desc        = parent.document.getElementById("descripcion"+i);
					 var VlrDebito   = parent.document.getElementById("valorDebito"+i);
					 var VlrCredito  = parent.document.getElementById("valorCredito"+i);
					 var subledger   = parent.document.getElementById("subledger"+i);
					 var idSubledger = parent.document.getElementById("idSubledger"+i);
					 var ReqAuxiliar = parent.document.getElementById("ReqAuxiliar"+i);
					 var abc         = parent.document.getElementById("abc"+i);

					 
					
					
					 if(CodContable.value ==''){
					    alert("El codigo contable del item "+Item +" No puede estar vacio");
						CodContable.focus();
						error = "SI";
						break;
					 }else if(desc.value ==''){
					    alert("La descripcion del item "+Item +" No puede estar vacia");
						desc.focus();
						error = "SI"; 
						break;
					 }else if((VlrDebito.value=='')&&(VlrCredito.value == '')){
					    alert("Debe ingresar un valor debito o credito para el item "+Item+ " No pueden estar vacios los  dos campos");
						VlrDebito.focus();
						error = "SI"; 
						break;
					 }else if((ReqTercero.value=='SI')&&(tercero.value=='')){
					    alert("EL codigo de cuenta para el item "+Item+ " requiere de tercero, el campo tercero no puede estar vacio ");
						tercero.focus();
						error = "SI"; 
						break;
					 }else if((ReqAuxiliar.value=='S')&&(idSubledger.value=='')){
					    alert("El codigo de cuenta para el item "+Item+" requiere de auxiliar")
						error ="SI"; 
					 }else if((ReqAuxiliar.value=='S')&&(subledger.value=='')){
					    alert("En el campo auxiliar del item "+Item+" el combo tipo no puede estar vacio")
						error ="SI"; 
					 }
					 
					 /*if ((CodContable.value.charAt(0) == 'C' || CodContable.value.charAt(0) == 'G' || CodContable.value.charAt(0) == 'I' ) && abc.value==''){
					    alert("Se requiere codigo ABC en el item "+Item+" ");
						abc.focus();
						error ="SI"; 					 
					 }*/
					
					 
					  
				  }//fin del IF CodContable != null
			   }//fin del FOR 
			   
			   if(error != 'SI'){
			       Tipo.disabled = false; 
			      parent.document.forma1.action = "controllercontab?estado=Grabar&accion=Comprobante&OP=GUARDAR&maxfila="+maxfila+"&opcion=<%=op%>";
				  parent.document.forma1.submit();
			   }
			    
			    
			}//fin error != SI 
		}//fin error != SI
		
	   <%
	   
	
	}//fin de la OPCION == VALIDAR
	
	
	
	
	if (opcion.equals("MENSAJE")){
	   String mensaje =request.getParameter("mensaje");
	   %> 
	      html = "<select name='subledger<%=idcampo%>' style='width:40; '>"
		  html +="<option value='' ></option>"
		  html +="</select><input type='text' maxlength='25' readonly style='width:90' name='idSubledger<%=idcampo%>' id='idSubledger<%=idcampo%>'>&nbsp;<img src='<%=BASEURL%>/images/botones/iconos/lupa.gif'  width='15' height='15'  onClick='' title='Buscar' style='cursor:hand' ><input type='hidden'   name='ReqAuxiliar<%=idcampo%>' id='ReqAuxiliar<%=idcampo%>' value=''> ";
		  parent.Texsubledger<%=idcampo%>.innerHTML = html;
		  
		   
	      alert('<%=mensaje%>');
		  parent.document.forma1.CodContable<%=idcampo%>.select(); 
	   <%
	
	}
	
	else if (opcion.equals("VERIFICAR_ABC")){
		String status = (String) request.getParameter("status");
		String abcId  = (String) request.getParameter("id");
		if (status.equalsIgnoreCase("NO")){%>
  		    alert( 'Codigo ABC no valido, por favor verifiquelo' );
			parent.document.forma1.<%= abcId %>.focus();	
			parent.document.forma1.<%= abcId %>.select();				
			
			
	<%	} 
	
	} else if (opcion.equals("VERIFICAR_DOCREL")){
		String status = (String) request.getParameter("status");
		String tipo   = (String) request.getParameter("tdoc_rel");
		if (status.equalsIgnoreCase("NO")){%>
  		    alert( 'El documento relacionado no existe o no pudo ser encontrado, verifique <%= (tipo.equals("010") || tipo.equals("FAC") ? " el tercero ":"" ) %>por favor.' );
			parent.document.formulario.estado.value='';
	<% } else { %>
		    parent.document.formulario.estado.value='ok';
	<% } } %> 
   
   
   //parent.TexNumDoc.innerHTML =  html;
 
</script>

        
                   