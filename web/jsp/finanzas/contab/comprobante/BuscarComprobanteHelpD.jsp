<!--
- Autor : Ing. Sandra Escalante
- Modificado: Ing Ivan Dario Gomez
- Date  : 02 Febrero 2006
- Copyrigth Notice : Fintravalores S.A. S.A
-->
<%--
-@(#)
--Descripcion : Vista para mostrar la informacion de las ayudas 
--%>
<%@ include file="/WEB-INF/InitModel.jsp"%>

<html>
<head>
<title>Descripcion de campos para la Creacion de Facturas</title>

<link href="../../../../css/estilostsp.css" rel="stylesheet" type="text/css"> 
<link href="<%=BASEURL%>/css/estilostsp.css" rel="stylesheet" type="text/css"> 
</head>

<body>
<br>
<table width="696"  border="2" align="center">
  <tr>
    <td width="811" >
      <table width="100%" border="0" align="center">
        <tr  class="subtitulo">
          <td height="24" colspan="2"><div align="center">CONTABILIDAD</div></td>
        </tr>
        <tr class="subtitulo1">
          <td colspan="2"> MODIFICACION DE COMPROBANTES </td>
        </tr>
		<tr class="subtitulo1">
          <td colspan="2"> Buscar Comprobantes </td>
        </tr>
        <tr>
          <td width="131" class="fila"> Tipo Documento </td>
          <td width="543"  class="ayudaHtmlTexto">Campo donde se especifica el tipo de documento </td>
        </tr>
        <tr>
          <td  class="fila"> Num Documento</td>
          <td  class="ayudaHtmlTexto">Campo donde se especifica el numero del documento </td>
        </tr>
        <tr>
          <td  class="fila"><img src='<%=BASEURL%>/images/botones/aceptar.gif' name='Buscar' align="absmiddle"   onMouseOver='botonOver(this);' onMouseOut='botonOut(this);'></td>
          <td  class="ayudaHtmlTexto">Boton para buscar el comprobante </td>
        </tr>
        <tr>
          <td  class="fila"><img src='<%=BASEURL%>/images/botones/salir.gif' name='Buscar' align="absmiddle" ></td>
          <td  class="ayudaHtmlTexto"> Boton para cerrar la ventana </td>
        </tr>
      </table>
    </td>
  </tr>
</table>

</body>
</html>
