
<!--
     - Author(s)       :      IVAN GOMEZ
     - Date            :      13/06/2006  
     - Copyright Notice:      TRANSPORTES SANCHEZ POLO S.A.
-->
<%--   
     - @(#)  
     - Description: Permite escoger el periodo que desea grabar el comprobante
--%> 
<%@ page session   ="true"%> 
<%@ page errorPage ="/error/ErrorPage.jsp"%>
<%@ page import    ="java.util.*" %>
<%@ page import    ="com.tsp.util.*" %>
<%@ include file="/WEB-INF/InitModel.jsp"%>
<!-- Para los botones -->
<%@ taglib uri="http://www.sanchezpolo.com/sot" prefix="tsp"%>
<%String path = application.getRealPath(model.menuService.getRealUrl(request.getRequestURI()));
  String datos[] = model.menuService.getContenidoMenu(BASEURL,request.getRequestURI(),path);
%>
<html>
<head>
        <title>Periodo contable</title>
        <link   href="<%= BASEURL %>/css/estilostsp.css" rel="stylesheet" type="text/css"> 
        <script src="<%= BASEURL %>/js/boton.js"></script>
</head>
<body>
     
<div id="capaSuperior" style="position:absolute; width:100%; height:100px; z-index:0; left: 0px; top: 0px;">
<jsp:include page="/WEB-INF/toptsp.jsp?encabezado=Periodo contable"/>
</div>

<div id="capaCentral" style="position:absolute; width:100%; height:85%; z-index:0; left: 0px; top: 100px; overflow: scroll;"> 
 <center>

 <% String msj = (request.getParameter("msj")==null)?"":request.getParameter("msj");
    List listaPeriodos = modelcontab.GrabarComprobanteSVC.getPeriodos(); 
  %>
 
 
 <form action='controllercontab?estado=Grabar&accion=Comprobante&OP=LISTAR_TIPO_DOC' method='post' name='formulario' onsubmit='return false;'>
 
       
       <table width="350" border="2" align="center">
          <tr>
             <td> 
          
                <table width='100%' align='center' class='tablaInferior'>
                
                          <tr>
                                <td class="barratitulo" colspan='2' >
                                       <table cellpadding='0' cellspacing='0' width='100%'>
                                             <tr class="fila">
                                                 <td width="50%" align="left" class="subtitulo1">&nbsp;Indicar periodo contable</td>
                                                 <td width="*"   align="left"><img src="<%= BASEURL %>/images/titulo.gif" width="32" height="20" align="left"><%=datos[0]%></td>
                                            </tr>
                                       </table>   
                                </td>
                          </tr>
                          
                          <tr class="fila">
						         <td width="42%"> Periodos Contables</td> 
                                 <td width="58%"  ><select name="periodo"  >
									     
										 <%for(int i=0; i< listaPeriodos.size();i++){
                                               Hashtable fila = (Hashtable)listaPeriodos.get(i);
                                          %>
                                            <option value="<%=(String)fila.get("periodo")%>" ><%=(String)fila.get("periodo")%></option>
										 <%}%>
                                       </select>
                                           
                                           
                                  </td>
                          </tr>

 
                </table>
            </td>
        </tr>
    </table>  
   </form>
   <br>
   
   
   <!--botones -->   
  <p>
        <tsp:boton value="aceptar"   onclick="formulario.submit();"/>                 
        <tsp:boton value="salir"     onclick="parent.close();"/> 
  </p>
 
 
 
  <% if( ! msj.equals("") ){%>
          <table border="2" align="center">
                  <tr>
                    <td>
                        <table width="100%"  border="1" align="center"  bordercolor="#F7F5F4" bgcolor="#FFFFFF">
                              <tr>
                                    <td width="400" align="center" class="mensajes"><%=msj%></td>
                                    <td width="29" background="<%=BASEURL%>/images/cuadronaranja.JPG">&nbsp;</td>
                                    <td width="58">&nbsp; </td>
                              </tr>
                         </table>
                    </td>
                  </tr>
          </table>
   <%}%>
 
</div>

</body>
<%=datos[1]%>
</html>
