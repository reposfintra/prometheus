<!--
- Autor : Ing. Sandra Escalante
- Modificado: Ing Ivan Dario Gomez
- Date  : 02 Febrero 2006
- Copyrigth Notice : Fintravalores S.A. S.A
-->
<%--
-@(#)
--Descripcion : Vista para mostrar la informacion de las ayudas
--%>
<%@ include file="/WEB-INF/InitModel.jsp"%>

<HTML>
<HEAD>
<TITLE></TITLE>
<META http-equiv=Content-Type content="text/html; charset=windows-1252">
<link href="../../../../css/estilostsp.css" rel="stylesheet" type="text/css">  
<link href="<%=BASEURL%>/css/estilostsp.css" rel="stylesheet" type="text/css"> 
</HEAD>
<BODY>
<% String BASEIMG = BASEURL +"/images/ayuda/finanzas/comprobante/"; %> 
  <table width="95%"  border="2" align="center">
    <tr>
      <td height="1" >      <table width="100%" border="0" align="center">
        <tr  class="subtitulo">
          <td height="20"><div align="center">MANUAL PARA LA GRABACION DE COMPROBANTES </div></td>
        </tr>
        <tr class="subtitulo1">
          <td>Descripci&oacute;n del funcionamiento para ingresar a grabar el comprobante </td>
        </tr>
        <tr>
          <td  height="18"  class="ayudaHtmlTexto">Esta es la ventana para escoger el periodo contable en que se desea grabar el comprobante, debera selescionar un periodo y luego presionar el boton Aceptar para enpezar a grabar el comprobante. </td>
        </tr>
        <tr>
          <td height="18"  class="ayudaHtmlTexto">&nbsp;</td>
        </tr>
        <tr>
          <td  class="ayudaHtmlTexto"><div align="center"><img src="<%=BASEIMG%>figura7.JPG" width="622" height="354"> </div></td>
        </tr>
        <tr>
          <td  class="ayudaHtmlTexto">&nbsp;</td>
        </tr>
        <tr>
          <td  class="ayudaHtmlTexto">&nbsp;</td>
        </tr>
        <tr>
          <td  class="ayudaHtmlTexto"><div align="center"></div></td>
        </tr>
      </table></td>
    </tr>
  </table>
</BODY>
</HTML>
