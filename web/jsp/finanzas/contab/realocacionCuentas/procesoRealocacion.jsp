<%@ page session="true"%>
<%@ page errorPage="../error/ErrorPage.jsp"%>
<%@ include file="/WEB-INF/InitModel.jsp"%>
<%@ taglib uri="http://jakarta.apache.org/taglibs/input-0.90" prefix="input" %>
<%@page import="com.tsp.operation.model.*, com.tsp.operation.model.beans.*, com.tsp.util.*,java.util.*, java.text.*"%>

<html>
<head>
<title>Generación de Proceso de Realocación de Cuentas</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">

<link href="../css/estilostsp.css" rel="stylesheet" type="text/css">
<script type='text/javascript' src="<%= BASEURL %>/js/boton.js"></script>
<link href="<%=BASEURL%>/css/estilostsp.css" rel="stylesheet" type="text/css">
<%
    String mensaje = (String) request.getAttribute("mensaje");
    String path = application.getRealPath(model.menuService.getRealUrl(request.getRequestURI()));
    String datos[] = model.menuService.getContenidoMenu(BASEURL,request.getRequestURI(),path);
%>
</head>

<body>
<div id="capaSuperior" style="position:absolute; width:100%; height:100px; z-index:0; left: 1px; top: 0px;">
 <jsp:include page="/toptsp.jsp?encabezado=Proceso de Realocación de Cuentas"/>
</div>
<div id="capaCentral" style="position:absolute; width:100%; height:87%; z-index:0; left: 0px; top: 100px; overflow: scroll;">        
	<form id="form1" method="post" action="<%=CONTROLLERCONTAB%>?estado=realocacion&accion=Cuentas">
    <table width="50%"  border="2" align="center" >
      <tr>
        <td><table width="100%"  border="1" align="center" class="tablaInferior">
          <tr>
            <td><table width="100%" border="0" cellpadding="0" cellspacing="0">
                <tr>
                  <td height="22" colspan=2 class="subtitulo1"><div align="left" class="subtitulo1">
                     <strong>Generar Proceso </strong>                     
                  </div></td>
                  <td width="212" class="barratitulo">
                    <img src="<%=BASEURL%>/images/titulo.gif" width="32" height="20" align="left">
                    <%=datos[0]%>
                </tr>
              </table>
                <table width="100%"  border="0" align="center" cellpadding="2" cellspacing="3">
                  <tr class="fila">
                    <td width="31%"><strong>Periodo: </strong></td>
                    <td width="69%"><select id="mes" name="mes">
                            <option value="01">Ene</option>
                            <option value="02">Feb</option>
                            <option value="03">Mar</option>
                            <option value="04">Abr</option>
                            <option value="05">May</option>
                            <option value="06">Jun</option>
                            <option value="07">Jul</option>
                            <option value="08">Ago</option>
                            <option value="09">Sep</option>
                            <option value="10">Oct</option>
                            <option value="11">Nov</option>
                            <option value="12">Dic</option>
                        </select>
                        
                        <select id="anio" name="anio">
                            <option value="2005">2005</option>
                            <option value="2006" selected>2006</option>
                            <option value="2007">2007</option>
                        </select>
                      
                  </tr>                  
              </table></td>
          </tr>
        </table></td>
      </tr>
    </table>
    <br>
    <div align="center">      
        <img id="baceptar" name="baceptar"  src="<%=BASEURL%>/images/botones/aceptar.gif" style="cursor:hand"  onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" onClick="form1.submit();">
        <%if(mensaje!=null){%>
            <img  src="<%=BASEURL%>/images/botones/cancelar.gif" style="cursor:hand"  onClick="location.replace('<%=CONTROLLER%>?estado=Menu&accion=Cargar&carpeta=/jsp/finanzas/contab/realocacionCuentas&pagina=procesoRealocacion.jsp&marco=no');" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);">         
        <%}%>
        <img src="<%=BASEURL%>/images/botones/salir.gif"  height="21" name="imgsalir"  onMouseOver="botonOver(this);" onClick="window.close();" onMouseOut="botonOut(this);" style="cursor:hand">            
    </div>
	</form>
    <br/>
        
        <%if(mensaje!=null){%>
        <table border="2" align="center">
            <tr>                
                <td height="45"><table width="410" height="41" border="1" align="center"  bordercolor="#F7F5F4" bgcolor="#FFFFFF">
                    <tr>
                        <td width="282" height="35" align="center" class="mensajes"><%=mensaje%></td>
                        <td width="28" background="<%=BASEURL%>/images/cuadronaranja.JPG">&nbsp;</td>
                        <td width="78">&nbsp;</td>
                    </tr>
                </table></td>
            </tr>
        </table>
        <br>
        <%}%>
        
	
</div>	
<%=datos[1]%>
</body>
</html>