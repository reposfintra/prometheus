<%@ include file="/WEB-INF/InitModel.jsp"%>

<HTML>
<HEAD>
<TITLE>Proceso de Realocaci&oacute;n de Cuentas</TITLE>
<META http-equiv=Content-Type content="text/html; charset=windows-1252">
<link href="<%=BASEURL%>/css/estilostsp.css" rel="stylesheet" type="text/css"> 
<link href="../../../../css/estilostsp.css" rel="stylesheet" type="text/css"> 
<script type='text/javascript' src="<%=BASEURL%>/js/boton.js"></script>
</HEAD>
<BODY>  

  <table width="95%"  border="2" align="center">
    <tr>
      <td>
        <table width="100%" border="0" align="center">
          <tr  class="subtitulo">
            <td height="20"><div align="center">Proceso de Realocaci&oacute;n de Cuentas </div></td>
          </tr>
          <tr class="subtitulo1">
            <td>Descripci&oacute;n del funcionamiento del programa de Realocaci&oacute;n de Cuentas</td>
          </tr>
          <tr>
            <td  height="18"  class="ayudaHtmlTexto">A continuaci&oacute;n se presenta el formulario para definir el periodo (a&ntilde;o y mes) para el proceso de realocaci&oacute;n de cuentas. </td>
          </tr>
          <tr>
            <td height="18"  class="ayudaHtmlTexto"><div align="center"><img src="<%=BASEURL%>/images/ayuda/contabilidad/realocacionCuentas/img01.jpg" width="526" height="151"></div></td>
          </tr>
          <tr>
            <td  class="ayudaHtmlTexto">&nbsp;</td>
          </tr>
          <tr>
            <td  class="ayudaHtmlTexto">Se hace click en el bot&oacute;n Aceptar, para finalmente realizar el proceso. </td>
          </tr>
          <tr>
            <td  class="ayudaHtmlTexto"><div align="center"><img src="<%=BASEURL%>/images/ayuda/contabilidad/realocacionCuentas/img02.jpg" width="460" height="77"></div></td>
          </tr>   
          
      </table></td>
    </tr>
  </table>
  <table width="416" align="center">
	<tr>
	<td align="center">
		<img src="<%=BASEURL%>/images/botones/salir.gif" style="cursor:pointer" name="c_salir" onClick="window.close();" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" align="absmiddle">
	</td>
	</tr>
</table>
</BODY>
</HTML>
