<%@ include file="/WEB-INF/InitModel.jsp"%>

<html>
<head>
<title>Descripci&oacute;n de Administraci&oacute;n de Comprobantes</title>

<script type='text/javascript' src="<%=BASEURL%>/js/boton.js"></script>
<link href="<%=BASEURL%>/css/estilostsp.css" rel="stylesheet" type="text/css"> 
<link href="../../../../css/estilostsp.css" rel="stylesheet" type="text/css"> 

</head>

<body>
<% String BASEIMG = BASEURL +"/images/botones/"; %> 
<br>
<table width="696"  border="2" align="center">
  <tr>
    <td width="811" >
      <table width="100%" border="0" align="center">
        <tr  class="subtitulo">
          <td height="24" colspan="2"><div align="center">ADMINISTRACI&Oacute;N DE COMPROBANTES</div></td>
        </tr>
        <tr class="subtitulo1">
          <td colspan="2">OBTENER COMPROBANTES </td>
        </tr>
        <tr>
          <td  class="fila">Periodo </td>
          <td  class="ayudaHtmlTexto"> Seleccione  mes y  a&ntilde;o para definir el periodo.</td>
        </tr>
		<tr>
          <td  class="fila">Tipo de documento </td>
          <td  class="ayudaHtmlTexto"> Selecci&oacute;n  del tipo de documento.</td>
        </tr>
		<tr>
          <td  class="fila">Criterios</td>
          <td  class="ayudaHtmlTexto"> Selecci&oacute;n  el criterio de selecci&oacute;n de comprobante, no contabilizados, descuadrados o todos..</td>
        </tr>	
		<tr>
          <td  class="fila">Documento</td>
          <td  class="ayudaHtmlTexto"> N&uacute;mero del documento. </td>
        </tr>		
		<tr>
          <td width="123"  class="fila"> Bot&oacute;n Aceptar </td>
          <td width="551"  class="ayudaHtmlTexto">Inicia el proceso de realocaci&oacute;n de cuentas. </td>
        </tr>
		<tr>
          <td width="123"  class="fila"> Bot&oacute;n Salir </td>
          <td width="551"  class="ayudaHtmlTexto">Cierra la ventana. </td>
        </tr>
		<tr>
          <td width="123"  class="fila"> Bot&oacute;n Cancelar </td>
          <td width="551"  class="ayudaHtmlTexto">Reinicia la p&aacute;gina, para volver a ingresar los datos. </td>
        </tr>
      </table>
    </td>
  </tr>
</table>
<br>
<table width="416" align="center">
	<tr>
	<td align="center">
		<img src="<%=BASEURL%>/images/botones/salir.gif" style="cursor:pointer" name="c_salir" onClick="window.close();" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" align="absmiddle">
	</td>
	</tr>
</table>
<p>&nbsp;</p>

<p>&nbsp;</p>
<p>&nbsp; </p>
</body>
</html>
