<%-- Declaracion de librerias--%>
<%@page contentType="text/html"%>
<%@ page session="true"%>
<%@ page errorPage="../error/error.jsp"%>
<%@page import="com.tsp.operation.model.beans.*, java.util.*"%>
<%@ include file="/WEB-INF/InitModel.jsp"%>
<%@page import="com.tsp.operation.model.*"%>
<%@page import="com.tsp.finanzas.contab.model.*"%>
<%@page import="com.tsp.util.*"%>
<%@ taglib uri="http://jakarta.apache.org/taglibs/input-0.90" prefix="input" %>

<html>
<head>
<title></title>
<link href="<%= BASEURL %>/css/estilostsp.css" rel="stylesheet" type="text/css">
<script type='text/javascript' src="<%= BASEURL %>/js/boton.js"></script>
<script type='text/javascript' src="<%= BASEURL %>/js/general.js"></script>
<script src='<%=BASEURL%>/js/date-picker.js'></script>
<script type="text/javascript">

		function send(theForm,ruta){
			theForm.action=ruta;
			theForm.submit();		
		}
		
		function chooseAll(theForm,nombre){			
			for (i=0;i<theForm.length;i++){
				if (theForm.elements[i].type=='checkbox' && !formulario.elements[i].disabled){
					theForm.elements[i].checked=document.getElementById("All").checked;		
				}
			}
	    }
		
		function page(theForm){
			var offsets = document.getElementById("paginas").value;
			var temp = offsets.split(";");
			var ruta = "<%=CONTROLLER%>?estado=Provision&accion=Fenalco&opcion=1&offset_fenalco="+temp[0]+"&offset_fintra="+temp[1]+"&no_pagina="+temp[2];
			theForm.action=ruta;
			theForm.submit();
		}
		
	
</script>
</head>

<body onload="redimensionar();" onresize="redimensionar();">

<div id="capaSuperior" style="position:absolute; width:100%; height:100px; z-index:0; left: 0px; top: 0px;">
    <jsp:include page="/toptsp.jsp?encabezado=Ver detalle provision Fenalco"/>
</div>

<div id="capaCentral" style="position: relative;  width:100%; top:97px; bottom:0px; left:0px; overflow: auto;"> 



<table border="0">
<form action="" method="post" id="formu" name="formulario">

	<tr>
	
	<th></th>
	
	<th class="subtitulo2">N&uacute;mero aprobaci&oacute;n</th>
	<th class="subtitulo2">Item</th>
	<th class="subtitulo2">NIT Empresa</th>
	<th class="subtitulo2">Nombre Empresa</th>
	<th class="subtitulo2">NIT Cliente</th>
	<th class="subtitulo2">Nombre Cliente</th>
	<th class="subtitulo2">N&uacute;mero t&iacute;tulo </th>
	<th class="subtitulo2">Valor t&iacute;tulo </th>
	<th class="subtitulo2">Provisi&oacute;n Fenalco</th>
	
	<th class="subtitulo1">N&uacute;mero aprobaci&oacute;n</td>
	<th class="subtitulo1">NIT Empresa</th>
	<th class="subtitulo1">Nombre Empresa</th>
	<th class="subtitulo1">NIT Cliente</th>
	<th class="subtitulo1">Nombre Cliente</th>
	<th class="subtitulo1">Factura</th>
	<th class="subtitulo1">N&uacute;mero t&iacute;tulo</th>
	<th class="subtitulo1">Valor t&iacute;tulo</th>
	<th class="subtitulo1">Provisi&oacute;n Fenalco</th>
	<th class="subtitulo1">Negocio Asociado</th>
	
	<th class="provision_fenalco">Diferencia Valor t&iacute;tulo</th>
	<th class="provision_fenalco">N&uacute;mero de documento</th>
	</tr>

		<%		
		
		
		
		String offset_fenalco = request.getParameter("offset_fenalco");
		String offset_fintra = request.getParameter("offset_fintra");

		String[] tabla_fenalco = null;//fenalco
		String[] tabla_fintra = null;//fintra
		
		String[] pos_tabla_fenalco = null;//Primera posicion de fenalco
		String[] pos_tabla_fintra = null;//Primera posicion de fintra
		
		int cont_pos_tabla_fenalco = -1;//aval fenalco 1
		int cont_pos_tabla_fintra = -1;//aval fintra 2

		String[] tabla_fenalco_aval = null;
		String[] tabla_fintra_aval = null;
		
		
		//---------------------------------------------------------------------------------------------------------------------		

		double sumaProvisiones = 0;//Contendra el valor de la suma de las provisiones
		int i=0;
		int j = 0;//Indice fenalco
		int k = 0;//Indice fintra	

		int limit_fenalco=48;//request
		int limit_fintra=48;//request

		String[] datosFenalco = null;
		String[] datosFintra = null;
		String[] tempo = null;
		String[] tempo_fintra = null;
		String num_aval1 = null;//numero de aval datos fenalco
		String num_aval2 = null;//numero de aval datos fintra

		if(offset_fintra==null){

			tabla_fenalco = model.provisionFenalcoService.getInfoTitulosFenalco();//fenalco
			tabla_fintra = model.provisionFenalcoService.getInfoTitulosFintra();//fintra
			
			pos_tabla_fenalco = tabla_fenalco[0].split(";");//Primera posicion de fenalco
			pos_tabla_fintra = tabla_fintra[0].split(";");//Primera posicion de fintra
			
			cont_pos_tabla_fenalco = model.provisionFenalcoService.getCountProvisionService(1,pos_tabla_fenalco[0]);//aval fenalco 1
			cont_pos_tabla_fintra = model.provisionFenalcoService.getCountProvisionService(2,pos_tabla_fintra[0]);//aval fintra 2

			datosFenalco = model.provisionFenalcoService.getInfoTitulosService(1, cont_pos_tabla_fenalco, 0);//Info de fenalco
			datosFintra = model.provisionFenalcoService.getInfoTitulosService(2, cont_pos_tabla_fintra, 0);//info de fintra
			
			tempo = datosFenalco[0].split(";");
			tempo_fintra = datosFintra[0].split(";");
			
			offset_fenalco = cont_pos_tabla_fenalco+"";
			offset_fintra = cont_pos_tabla_fintra+"";
			
			num_aval1 = tempo[0];//numero de aval datos fenalco
			num_aval2 = tempo_fintra[0];//numero de aval datos fintra	

			
		}else{

			//tabla_fenalco = new String[model.provisionFenalcoService.getInfoTitulosFenalco().length+]
			tabla_fenalco = model.provisionFenalcoService.getInfoTitulosFenalco();//fenalco
			tabla_fintra = model.provisionFenalcoService.getInfoTitulosFintra();//fintra
			
			pos_tabla_fenalco = tabla_fenalco[Integer.parseInt(offset_fenalco)].split(";");//Primera posicion de fenalco
			pos_tabla_fintra = tabla_fintra[Integer.parseInt(offset_fintra)].split(";");//Primera posicion de fintra
			
			cont_pos_tabla_fenalco = model.provisionFenalcoService.getCountProvisionService(1,pos_tabla_fenalco[0]);//aval fenalco 1
			cont_pos_tabla_fintra = model.provisionFenalcoService.getCountProvisionService(2,pos_tabla_fintra[0]);//aval fintra 2
		
			datosFenalco = model.provisionFenalcoService.getInfoTitulosService(1, cont_pos_tabla_fenalco, Integer.parseInt(offset_fenalco));//Info de fenalco
			datosFintra = model.provisionFenalcoService.getInfoTitulosService(2, cont_pos_tabla_fintra, Integer.parseInt(offset_fintra));//info de fintra
			
			tempo = datosFenalco[0].split(";");
			tempo_fintra = datosFintra[0].split(";");
			
			num_aval1 = tempo[0];//numero de aval datos fenalco
			num_aval2 = tempo_fintra[0];//numero de aval datos fintra	

			offset_fenalco = cont_pos_tabla_fenalco+"";
			offset_fintra = cont_pos_tabla_fintra+"";

		}

		String claseFila = "fila_provision2";//CSS
		String fila_provision = "fila_provision";//CSS

		int estado = 0;
		int cont = 0;
		
		String negocio = "";
		String pagado_fenalco="";
		String informacion = "";
		String[] previewFintra = null;//contendra la posicion anterior de datosFintra
		String[] previewFenalco = null;//contendra la posicion anterior de datosFenalco

		Usuario user = (Usuario)session.getAttribute("Usuario");

		String numeroAprobacion = "";
				
		try{
			while(tempo[0].equals(tempo_fintra[0]) && (!tempo[0].equals("") || (!tempo_fintra[0].equals("")))){
				cont++;
				
				if(!(tempo[0].equals(""))){
					sumaProvisiones = sumaProvisiones + Double.parseDouble(tempo[7]);//Suma las provisiones de este negocio
					double p_fenalco = Double.parseDouble(tempo[7])*1.16;
					//nit_proveedor;item;no_factura;provision_fenalco+iva;usuario;num_aval;factura;nombre_proveedor
					informacion = "8901009858;"+tempo[9]+";"+tempo[10]+";"+p_fenalco+";"+user.getLogin()+";"+tempo[0]+";"+tempo_fintra[5]+";"+tempo[2]+";"+tempo[11]+";"+tempo[12];
					System.out.println("La fecha a enviar es: "+tempo[12] +" "+tempo[11]+"  "+tempo[8]);
					pagado_fenalco = tempo_fintra[9];
				}
				
				out.print("<tr>");

				if(pagado_fenalco.equals("null")){
				%>
					<th><input type='checkbox' name='titulo' value="<%=informacion%>"></th>
				<%
				}else{
				%>
					<th><input type='checkbox' value="<%=informacion%>" disabled></th>
				<%
				}

				if(!tempo[0].equals("")){
					
					out.print("<td class="+claseFila+">"+cont+": "+tempo[0]+"</td>");out.print("<td class="+claseFila+">"+tempo[9]+"</td>");	
					out.print("<td class="+claseFila+">"+tempo[1]+"</td>");
					out.print("<td class="+claseFila+">"+tempo[2]+"</td>");out.print("<td class="+claseFila+">"+tempo[3]+"</td>");
					out.print("<td class="+claseFila+">"+tempo[4]+"</td>");out.print("<td class="+claseFila+">"+tempo[5]+"</td>");
					out.print("<td class="+claseFila+">"+Util.customFormat(Double.parseDouble(tempo[6]))+"</td>");
					out.print("<td class="+claseFila+">"+Util.customFormat(Double.parseDouble(tempo[7]))+"</td>");
				}

				out.print("<td class="+fila_provision+">"+tempo_fintra[0]+"</td>");out.print("<td class="+fila_provision+">"+tempo_fintra[1]+"</td>");
				out.print("<td class="+fila_provision+">"+tempo_fintra[2]+"</td>");out.print("<td class="+fila_provision+">"+tempo_fintra[4]+"</td>");
				out.print("<td class="+fila_provision+">"+tempo_fintra[3]+"</td>");out.print("<td class="+fila_provision+">"+tempo_fintra[5]+"</td>");
				out.print("<td class="+fila_provision+">"+tempo_fintra[6]+"</td>");
				
				if(!tempo[0].equals("")){
					out.print("<td class="+fila_provision+">"+Util.customFormat(Double.parseDouble(tempo_fintra[7]))+"</td>");
				}
				
				out.print("<td class="+fila_provision+"></td>");
				if(!tempo[0].equals("")){
					out.print("<td class="+fila_provision+">"+tempo_fintra[8]+"</td>");
					out.print("<td class="+fila_provision+">"+Util.customFormat(Double.parseDouble(tempo[6])-Double.parseDouble(tempo_fintra[7]))+"</td>");
				}

				if(tempo_fintra[9].equals("null")){
					out.print("<td class="+fila_provision+"></td>");
				}else{
					out.print("<td class="+fila_provision+">"+tempo_fintra[9]+"</td>");
				}
				out.print("</tr>");
				j++;k++;//Aumenta de igual forma los indices
				
				if(!tempo[0].equals("")){
					numeroAprobacion = tempo[0];
				}
				
				try{
					if(!tempo.equals("")){
						tempo = datosFenalco[j].split(";");
					}
				}catch(Exception e){
					tempo = new String[13];
					for(int num=0;num<13;num++){
						tempo[num] = "";
					}
					estado = 1; //numaval1 > numaval2
				}
				try{
					if(!tempo_fintra.equals("")){
						tempo_fintra = datosFintra[k].split(";");
					}
				}catch(Exception e){
					tempo_fintra = new String[11];
					for(int num=0;num<11;num++){
						tempo_fintra[num] = ""; 
					}
					if(estado==1){
						estado = 0;//se acabo el ciclo
					}else{
						estado =2;//numaval1 < numaval2
					}
				}
				
				
			}
			
			if(estado==1){
				while(!tempo_fintra[0].equals("")){
					out.print("<tr>");
					%>
					<th><input type='checkbox' disabled></th>
					<%
					out.print("<td class="+claseFila+"></td>");out.print("<td class="+claseFila+"></td>");
					out.print("<td class="+claseFila+"></td>");out.print("<td class="+claseFila+"></td>");
					out.print("<td class="+claseFila+"></td>");out.print("<td class="+claseFila+"></td>");
					out.print("<td class="+claseFila+"></td>");out.print("<td class="+claseFila+"></td>");
					out.print("<td class="+claseFila+"></td>");
	
					out.print("<td class="+fila_provision+">"+tempo_fintra[0]+"</td>");out.print("<td class="+fila_provision+">"+tempo_fintra[1]+"</td>");
					out.print("<td class="+fila_provision+">"+tempo_fintra[2]+"</td>");out.print("<td class="+fila_provision+">"+tempo_fintra[4]+"</td>");
					out.print("<td class="+fila_provision+">"+tempo_fintra[3]+"</td>");out.print("<td class="+fila_provision+">"+tempo_fintra[5]+"</td>");
					
					out.print("<td class="+fila_provision+">"+tempo_fintra[6]+"</td>");out.print("<td class="+fila_provision+">"+Util.customFormat(Double.parseDouble(tempo_fintra[7]))+"</td>");
					out.print("<td class="+fila_provision+"></td>");
					out.print("<td class="+fila_provision+">"+tempo_fintra[8]+"</td>");
					
					if(!tempo[0].equals("")){
						out.print("<td class="+fila_provision+">"+Util.customFormat(Double.parseDouble(tempo[6]) - Double.parseDouble(tempo_fintra[7]))+"</td>");
					}else{
						out.print("<td class="+fila_provision+">"+Util.customFormat(Double.parseDouble(tempo_fintra[7]))+"</td>");
					}
					//si es nulo, no ha sido cancelado el titulo a fenalco
					if(tempo_fintra[9].equals("null")){
						out.print("<td class="+fila_provision+"></td>");
					}else{
						out.print("<td class="+fila_provision+">"+tempo_fintra[9]+"</td>");
					}
					out.print("</tr>");
				
					k++;//Aumenta el indice fintra
					try{
						tempo_fintra = datosFintra[k].split(";");
					}catch(Exception e){
						//out.print("tempo_fintra 2 vacio: "+e);
						tempo_fintra = new String[11];
						for(int num=0;num<11;num++){
							tempo_fintra[num] = "";
						}
					}
				}
			}else{
				if(estado==2){
				
					
					while(!tempo[0].equals("") && numeroAprobacion.equals(tempo[0])){
					
						sumaProvisiones = sumaProvisiones+ Double.parseDouble(tempo[7]);
						negocio = tempo[8];//Negocio asociado
						pagado_fenalco = tempo_fintra[9];	
						double p_fenalco = Double.parseDouble(tempo[7])*1.16;
						//nit_proveedor;item;no_factura;provision_fenalco+iva;usuario;num_aval;factura;nombre_proveedor
						informacion = tempo[1]+";"+tempo[9]+";"+tempo[10]+";"+p_fenalco+";"+user.getLogin()+";"+tempo[0]+";"+tempo_fintra[5]+";"+tempo[2]+";"+tempo[11]+";"+tempo[12];
						
						if(pagado_fenalco.equals("null")){
						%>
							<th><input type='checkbox' name='titulo' value="<%=informacion%>"></th>
						<%
						}else{
						%>
							<th><input type='checkbox' value="<%=informacion%>" disabled></th>
						<%
						}
						
						out.print("<td class="+claseFila+">"+tempo[0]+"</td>");out.print("<td class="+claseFila+">"+tempo[9]+"</td>");	
						out.print("<td class="+claseFila+">"+tempo[1]+"</td>");
						out.print("<td class="+claseFila+">"+tempo[2]+"</td>");out.print("<td class="+claseFila+">"+tempo[3]+"</td>");
						out.print("<td class="+claseFila+">"+tempo[4]+"</td>");out.print("<td class="+claseFila+">"+tempo[5]+"</td>");
						out.print("<td class="+claseFila+">"+Util.customFormat(Double.parseDouble(tempo[6]))+"</td>");
						out.print("<td class="+claseFila+">"+Util.customFormat(Double.parseDouble(tempo[7]))+"</td>");
						
		
						out.print("<td class="+fila_provision+"></td>");out.print("<td class="+fila_provision+"></td>");
						out.print("<td class="+fila_provision+"></td>");out.print("<td class="+fila_provision+"></td>");
						out.print("<td class="+fila_provision+"></td>");out.print("<td class="+fila_provision+"></td>");
						out.print("<td class="+fila_provision+"></td>");out.print("<td class="+fila_provision+"></td>");
						out.print("<td class="+fila_provision+"></td>");
						out.print("<td class="+fila_provision+"></td>");
						
						if(!tempo_fintra[0].equals("")){
							out.print("<td class="+fila_provision+">"+Util.customFormat(Double.parseDouble(tempo[6]) - Double.parseDouble(tempo_fintra[7]))+"</td>");
							if(tempo_fintra[9].equals("null")){
								out.print("<td class="+fila_provision+"></td>");
							}else{
								out.print("<td class="+fila_provision+">"+tempo_fintra[9]+"</td>");
							}						
						}else{
							out.print("<td class="+fila_provision+">"+Util.customFormat(Double.parseDouble(tempo[6])*-1)+"</td>");
							out.print("<td class="+fila_provision+"></td>");
						}
						
						
						out.print("</tr>");
						
						j++;//Aumenta el indice fenalco
						
						try{
							tempo = datosFenalco[j].split(";");
						}catch(Exception e){
							//out.print("tempo_fenalco 2 vacio: "+e);
							tempo = new String[13];
							for(int num=0;num<13;num++){
								tempo[num] = "";
							}
						}
					}
				}
			}

			//if(estado!=0){
				try{
					previewFintra = datosFintra[k-1].split(";");
				}catch(Exception e){
					//out.println("p_fintra" +e);
				}
	
				try{
					previewFenalco = datosFenalco[j-1].split(";");
				}catch(Exception e){
					//out.println("p_fenalco" +e);
				}
					
					
				%>
				<tr><td class="provision_fenalco"><input type='checkbox'  id='All' name="All" onclick="chooseAll(document.getElementById('formu'),'<%=previewFenalco[0]%>');"></td><td  class="provision_fenalco"><%=previewFenalco[0]%> </td>
				<%
				
				if(!previewFintra[10].equals("null")){
					out.print("<td colspan = \"6\" class=\"provision_fenalco\"></td><td class=\"provision_fenalco\">Total provisi&oacute;n + IVA: </td><td class=\"provision_fenalco\">"+Util.customFormat(sumaProvisiones*1.16)+"</td><td class=\"subtitulo1\">Total provisi&oacute;n: </td><td class=\"subtitulo1\">"+Util.customFormat(Double.parseDouble(previewFintra[10]))+"</td><td colspan = \"10\" class=\" subtitulo1\"></tr>");
				}else{
					out.print("<td colspan = \"6\" class=\"provision_fenalco\"></td><td class=\"provision_fenalco\">Total provisi&oacute;n + IVA: </td><td class=\"provision_fenalco\">"+Util.customFormat(sumaProvisiones*1.16)+"</td><td class=\"subtitulo1\">Total provisi&oacute;n: </td><td class=\"subtitulo1\">Pendiente</td><td colspan = \"10\" class=\" subtitulo1\"></td></tr>");
				}
			//}

	   	}catch(Exception e){
			out.print("externo: "+e);
		}

%>
</table>

<br>
<center>
<select name="paginas" id="paginas" onchange="page(formulario);">
<%
	String cxp = request.getParameter("cxp");
	if(cxp!=null){
		if(cxp.equals("yes")){
			%>
			<center><p>Exito al ingresar cxp</p></center>
			<script>alert("Exito al ingresar cxp");</script>
			<%
		}else{
			%>
			<center><p>No exito !!</p></center>
			<%
		}
	}

 	String[] avales = model.provisionFenalcoService.getAvalesService();//lista de avales
	int contador_fenalco = model.provisionFenalcoService.getCountProvisionService(1,avales[0]);
	int contador_fintra = model.provisionFenalcoService.getCountProvisionService(2,avales[0]);
	String value = contador_fenalco+";"+contador_fintra+";"+0;

	for(int num=1;num<avales.length;num++){
		%>
		<option value="<%=value%>"><%=num%></option>
		<%
		contador_fenalco = contador_fenalco + model.provisionFenalcoService.getCountProvisionService(1,avales[num]);//offset fenalco
		contador_fintra  = contador_fintra + model.provisionFenalcoService.getCountProvisionService(2,avales[num]);//offset fintra
		value = contador_fenalco+";"+contador_fintra+";"+num;
	}
%>
</select>
</center>
<br>
<center>
<%

	String numero_pagina = request.getParameter("no_pagina");
	if(numero_pagina!=null){
	%>
	<p class="<%=claseFila%>">P&aacute;gina actual: <%=numero_pagina%></p>
	<%
	}
%>
</center>
<br>
<center>
<img src="<%=BASEURL%>/images/botones/aceptar.gif" height="21"  title='Aceptar' onclick="send(formulario,'<%=CONTROLLER%>?estado=Provision&accion=Fenalco&opcion=0');" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" style="cursor:hand">        
<img src='<%=BASEURL%>/images/botones/salir.gif'style='cursor:hand' title='Salir...' name='i_salir' onclick='parent.close();' onMouseOver='botonOver(this);' onMouseOut='botonOut(this);'>
</center>
<br>

</form>
</div>
</body>
</html>

