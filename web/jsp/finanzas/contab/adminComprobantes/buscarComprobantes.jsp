<%@page contentType="text/html"%>
<%@ page session="true"%>
<%@ page errorPage="/fintra/error/ErrorPage.jsp"%>
<%@page import="com.tsp.operation.model.beans.*, java.util.*"%>
<%@page import="com.tsp.operation.model.*"%>
<%@page import="com.tsp.finanzas.contab.model.*,com.tsp.finanzas.contab.model.beans.*"%>
<%@page import="com.tsp.util.*"%>
<%@include file="/WEB-INF/InitModel.jsp"%>
<%@ taglib uri="http://jakarta.apache.org/taglibs/input-0.90" prefix="input" %>
<%@taglib uri="http://jsptags.com/tags/navigation/pager" prefix="pg" %>
<%
        String resultado = (String)session.getAttribute( "resultado" );
        String tipodoc = (String)session.getAttribute( "tipodoc" )!= null?(String)session.getAttribute( "tipodoc" ):"";
        String anio = (String)session.getAttribute( "anio" )!= null?(String)session.getAttribute( "anio" ):"2008";
        String mes = (String)session.getAttribute( "mes" )!= null?(String)session.getAttribute( "mes" ): Util.mesFormat( Util.MesActual() ) ;
        String numdoc = (String)session.getAttribute( "numdoc" )!= null?(String)session.getAttribute( "numdoc" ):"";
        String criterios = (String)session.getAttribute( "criterios" )!= null?(String)session.getAttribute( "criterios" ):"";
        String orden = (String)session.getAttribute( "orden" )!= null?(String)session.getAttribute( "orden" ):"";
        String fechaInicial = (String)session.getAttribute( "fechaInicial" )!= null?(String)session.getAttribute( "fechaInicial" ):"";
        String fechaFinal = (String)session.getAttribute( "fechaFinal" )!= null?(String)session.getAttribute( "fechaFinal" ):"";

        //Se cargan las opciones de transaccion y tipo Cuenta
        TreeMap opc_criterios = new TreeMap();
        opc_criterios.put("Contabilizados","C");
        opc_criterios.put("No Contabilizados","N");
        opc_criterios.put("Descuadrados","D");
        opc_criterios.put( "Todos", "" );

        //Se cargan las opciones de transaccion y tipo Cuenta
        TreeMap opc_orden = new TreeMap();
        opc_orden.put("Periodo","periodo");
        opc_orden.put("Usuario","creation_user");
        opc_orden.put("Fecha Aplicaci�n","fecha_aplicacion");
        opc_orden.put("Fecha Creaci�n","creation_date");
        opc_orden.put("Grupo Transacci�n","grupo_transaccion");
        opc_orden.put("Documento","numdoc");

        TreeMap opc_docs = modelcontab.tipo_doctoSvc.getTreemap();

        String path = application.getRealPath(model.menuService.getRealUrl(request.getRequestURI()));
        String datos[] = model.menuService.getContenidoMenu(BASEURL,request.getRequestURI(),path);
%>
<html>
    <head>
        <title>Administraci�n de Comprobantes</title>
        <link href="<%= BASEURL %>/css/estilostsp.css" rel="stylesheet" type="text/css">
        <link href="../../../css/estilostsp.css" rel="stylesheet" type="text/css">
        <script src='<%=BASEURL%>/js/boton.js'></script>
        <script language="javascript" src="<%=BASEURL%>/js/general.js"></script>
        <script language="javascript" src="<%=BASEURL%>/js/utilidades.js"></script>
        <style>
            A:visited {color:#FFFFFF;}
            A:hover {color:red;}
        </style>
        <script>
            function verifycheck(){
                var check = false;
                if( document.getElementById("todos").checked ){
                    check = true;
                }
                cant = document.getElementById("cantItems").value;
                for( var i = 0; i < cant; i++ ){
                    document.getElementById( "ck_"+ (i+1) ).checked = check;
                }
            }
            function validarTodos(){
                cant = document.getElementById("cantItems").value;
                for( var i = 0; i < cant; i++ ){
                    if( !document.getElementById( "ck_"+ (i+1) ).checked ){
						document.getElementById("todos").checked = false;
						return;
                    }
                }
                document.getElementById("todos").checked = true;
            }
            function enviarAccion( accion ){
                var mes = document.getElementById("mes").value;
                var anio = document.getElementById("anio").value;
                if( accion == "adicionar" ){
                    window.open('<%=CONTROLLERCONTAB%>?estado=Grabar&accion=Comprobante&OP=LISTAR_TIPO_DOC&periodo='+anio+mes,'','width=940,height=600,scrollbars=yes,resizable=yes,top=0,left=50,status=yes');
                    return;
                }
				var estado = false;
                cant = document.getElementById("cantItems").value;
                for( var i = 0; i < cant; i++ ){
                    if( document.getElementById( "ck_"+ (i+1) ).checked ){
						estado = true;
						break;
                    }
                }

				if (!estado){
					alert( "Debe seleccionar uno o varios comprobantes" );
					return;
				}

                document.getElementById("item").value = accion;
                formAccion.submit();
            }
            function validarBuscar(){
                if( formBuscar.fechaInicial.value != "" && formBuscar.fechaFinal.value != "" ){
                    if( formBuscar.fechaInicial.value > formBuscar.fechaFinal.value ){
                        alert( "La fecha fin no puede ser menor que la fecha inicio." );
                        return false;
                    }
                }
                enviarPaginaForma('<%=CONTROLLERCONTAB%>?estado=AdminComprobantes&accion=Manager&opc=buscar','formBuscar');
            }
        </script>
    </head>
    <body >
        <div id="capaSuperior" style="position:absolute; width:100%; height:100px; z-index:0; left: 0px; top: 0px;">
            <jsp:include page="/WEB-INF/toptsp.jsp?encabezado=Administraci�n de Comprobantes"/>
        </div>
        <div id="capaCentral" style="position:absolute; width:100%; height:85%; z-index:0; left: 0px; top: 100px; overflow: scroll;">            <br>
            <form name="formBuscar" id="formBuscar" method="post" action="<%=CONTROLLERCONTAB%>?estado=AdminComprobantes&accion=Manager&opc=buscar" >
            <table width="98%" border="2" align="center">
                <tr>
                 <td>
                    <table width="100%" align="center" cellpadding="3" cellspacing="2" class="tablaInferior">
                    <tr class="fila">
                        <td colspan="2" class="subtitulo1">Criterios de Selecci�n</td>
                        <td colspan="6" class="barratitulo"><img src="<%=BASEURL%>//images/titulo.gif" align="left"><%=datos[0]%></td>
                    </tr>
                    <tr class="fila">
                        <td width="120">Tipo de documento</td>
                        <td width="160">
                            <input:select name="tipodoc" options="<%= opc_docs %>" attributesText="class='textbox' id='tipodoc' style='width:160'" default="<%=tipodoc%>"/>
                        </td>
                        <td width="50">Periodo</td>
                        <td width="110">
                            <select id="mes" name="mes">
                                <option value="01">Ene</option>
                                <option value="02">Feb</option>
                                <option value="03">Mar</option>
                                <option value="04">Abr</option>
                                <option value="05">May</option>
                                <option value="06">Jun</option>
                                <option value="07">Jul</option>
                                <option value="08">Ago</option>
                                <option value="09">Sep</option>
                                <option value="10">Oct</option>
                                <option value="11">Nov</option>
                                <option value="12">Dic</option>
                            </select>
                            <select id="anio" name="anio">
								<%  int hoy = Integer.parseInt(UtilFinanzas.customFormatDate( new Date(), "yyyy" ));
									for (int i = hoy-4; i <= hoy+2; i++){ %>
										 <option value="<%= i %>"  <%= ( (i == hoy) ? " selected ":"") %> > <%= i %> </option>
								<%	} %>
                            </select>
                        </td>
                        <td width="50">Criterios</td>
                        <td width="120">
                            <input:select name="criterios" options="<%= opc_criterios %>" attributesText="class='textbox' id='criterios' style='width:120'" default="<%=criterios%>"/>
                        </td>
                        <td width="60">Documento</td>
                        <td width="90">
                            <input type="text" name="numdoc" id="numdoc" class="textbox" maxlength="30" value="<%=numdoc%>" >
                        </td>
                    </tr>
                    <tr class="fila">
                        <td width="120">Fecha Inicial</td>
                        <td width="160">
                            <input type="text" name="fechaInicial" id="fechaInicial" class='textbox' size="10" readonly value="<%=fechaInicial%>" style='width:100'>
			    <a href="javascript:void(0)" onclick="if(self.gfPop)gfPop.fPopCalendar(document.formBuscar.fechaInicial);return false;" HIDEFOCUS><img src="<%=BASEURL%>\js\Calendario\cal.gif" width="16" height="16" border="0" alt="Click aqui para escoger la fecha"></a>
                        </td>
                        <td width="160" colspan="2" >Fecha Final</td>
                        <td width="170" colspan="2">
                            <input type="text" name="fechaFinal" id="fechaFinal" class='textbox' size="10" readonly value="<%=fechaFinal%>" style='width:100'>
			    <a href="javascript:void(0)" onclick="if(self.gfPop)gfPop.fPopCalendar(document.formBuscar.fechaFinal);return false;" HIDEFOCUS><img src="<%=BASEURL%>\js\Calendario\cal.gif" width="16" height="16" border="0" alt="Click aqui para escoger la fecha"></a>
                        </td>
                        <td width="80">Ordenar Por</td>
                        <td width="120">
                            <input:select name="orden" options="<%= opc_orden %>" attributesText="class='textbox' id='orden' style='width:120'" default="<%= orden %>"/>
                        </td>
                    </tr>
                    </table>
                </td>
                </tr>
            </table>
            <div align="center"><br>
                <img src="<%= BASEURL%>/images/botones/buscar.gif"  name="imgaceptar" onClick="validarBuscar();" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);"  style="cursor:hand ">&nbsp;
                <img src="<%= BASEURL%>/images/botones/cancelar.gif"  name="imgcancelar"  onMouseOver="botonOver(this);" onclick="location.replace('<%=CONTROLLER%>?estado=Menu&accion=Cargar&carpeta=/jsp/finanzas/contab/adminComprobantes&pagina=buscarComprobantes.jsp&marco=no&opcion=37');" onMouseOut="botonOut(this);"   style="cursor:hand ">&nbsp;
                <img src="<%= BASEURL%>/images/botones/salir.gif"  name="imgsalir"  onMouseOver="botonOver(this);" onClick="parent.close();" onMouseOut="botonOut(this);" style="cursor:hand ">
            </div>
            </form>
            <%
                String mensaje = (String)request.getAttribute( "mensaje" );
                if( mensaje != null ){
            %>
            <table border="2" align="center">
                <tr>
                    <td><table width="100%"  border="1" align="center"  bordercolor="#F7F5F4" bgcolor="#FFFFFF">
                    <tr>
                    <td width="350" align="center" class="mensajes"><%= mensaje %></td>
                    <td width="29" background="<%=BASEURL%>/images/cuadronaranja.JPG">&nbsp;</td>
                    <td width="58">&nbsp;</td>
                </tr>
                </table></td>
                </tr>
            </table>
            <%}%>
            <%if( resultado != null ){%>
            <table width="98%" border="1" align="center">
                <tr>
                    <td>
                        <table width="100%" border="0" bgcolor="#4D71B0" align="center">
                          <tr>
                            <td>
                            &nbsp;<img src='<%=BASEURL%>/images/cuadrito.gif' width='9' height='9'>&nbsp; <a href="javascript:enviarAccion('imprimir');" class="encabezado">Imprimir</a>
                                &nbsp;<img src='<%=BASEURL%>/images/cuadrito.gif' width='9' height='9'>&nbsp; <a href="javascript:enviarAccion('contabilizar');" class="encabezado">Contabilizar</a>
                                &nbsp;<img src='<%=BASEURL%>/images/cuadrito.gif' width='9' height='9'>&nbsp; <a href="javascript:enviarAccion('descontabilizar');" class="encabezado">Descontabilizar</a>
                                &nbsp;<img src='<%=BASEURL%>/images/cuadrito.gif' width='9' height='9'> &nbsp; <a href="javascript:enviarAccion('validar');" class="encabezado">Validar</a>
                                &nbsp;<img src='<%=BASEURL%>/images/cuadrito.gif' width='9' height='9'> &nbsp; <a href="javascript:enviarAccion('adicionar');" class="encabezado">Adicionar</a>
                                &nbsp;<img src='<%=BASEURL%>/images/cuadrito.gif' width='9' height='9'> &nbsp; <a href="javascript:enviarAccion('reversar');" class="encabezado">Reversar</a>
                                &nbsp;<img src='<%=BASEURL%>/images/cuadrito.gif' width='9' height='9'>
                            </td>
                          </tr>
                        </table>
                        <div style="width:100%; height:350;overflow: auto;">
                        <table width="100%" border="1" bordercolor="#999999" bgcolor="#F7F5F4">
                            <tr class="tblTitulo">
                            <td width="5%" align="center"><input type="checkbox" onclick="verifycheck();" name="todos" id="todos" value="" ></td>
                            <td width="10%" align="center">Periodo</td>
                            <td width="10%" align="center">Tipo Documento</td>
                            <td width="11%" align="center">No.Documento</td>
                            <td width="10%" align="center">Grupo Transacci�n</td>
                            <td width="10%" align="center">Fecha Creaci�n</td>
                            <td width="10%" align="center">Fecha Aplicaci�n</td>
                            <td width="10%" align="center">Usuario</td>
                            <td width="12%" align="center">Valor D�bito</td>
                            <td width="12%" align="center">Valor Cr�dito</td>
                            </tr>
                            <form name="formAccion" id="formAccion" method="post" action="<%=CONTROLLERCONTAB%>?estado=AdminComprobantes&accion=Manager&opc=accion" >
                            <%
                                Vector comprobantes = modelcontab.comprobanteService.getVector();
                                  int j = 0;
                                  for (int i = 0; i < comprobantes.size(); i++){
                                      Comprobantes comprobante = (Comprobantes) comprobantes.elementAt( i );
                                      j++;
                                      String color = (i % 2 == 0 )?"filagris":"filaazul";
                                      if( comprobante.getFecha_aplicacion().equals("0099-01-01") ){
                                          color = "filaamarilla";
                                      }
                                      if( comprobante.getTotal_debito() != comprobante.getTotal_credito()){
                                          color = "filaroja";
                                      }
                            %>
                                <tr class="<%=color%>" style="cursor:default" onClick="" >
                                <td class="bordereporte" align="center"><input type="checkbox" name="comprobantes" id="ck_<%= j %>" onclick="validarTodos();" value="<%=comprobante.getTipodoc()+"&"+comprobante.getNumdoc()+"&"+comprobante.getGrupo_transaccion()+"&"+comprobante.getDstrct()%>" ></td>
                                <td class="bordereporte" align="center"><%= comprobante.getPeriodo() %>&nbsp;</td>
                                <td class="bordereporte" align="center"><%= comprobante.getTipodoc() %>&nbsp;</td>
                                <td class="bordereporte">
									<% if (comprobante.getTipodoc().equals("PLA")) { %>
										 <span class="Simulacion_Hiper" style="cursor:pointer" onClick="window.open('<%=CONTROLLER%>?estado=Consultar&accion=Planilla&general=ok&marco=no&numpla=<%= comprobante.getNumdoc() %>&placa=&origen=&destino=&despachador=&cedcon=&agencia=','','HEIGHT=600,WIDTH=940,STATUS=YES,SCROLLBARS=YES,RESIZABLE=YES,top=0,left=50')">
											<%= comprobante.getNumdoc() %>
										 </span>
									<% } else if (comprobante.getTipodoc().equals("REM")) { %>
										 <span class="Simulacion_Hiper" style="cursor:pointer" onClick="window.open('<%=CONTROLLER%>?estado=Consultar&accion=Remesa&remesa=<%= comprobante.getNumdoc() %>&fechaini=&fechafin=&cliente=&sj=&docint=&faccial=','','HEIGHT=600,WIDTH=940,STATUS=YES,SCROLLBARS=YES,RESIZABLE=YES,top=0,left=50')">
											<%= comprobante.getNumdoc() %>
										 </span>
									<% } else if (comprobante.getTipodoc().equals("FAC") || comprobante.getTipodoc().equals("FAA")) { %>
										 <span class="Simulacion_Hiper" style="cursor:pointer" onClick="window.open('<%=CONTROLLER%>?estado=Factura&accion=Detalle&prov=<%= comprobante.getTercero() %>&documento=<%= comprobante.getNumdoc() %>&tipo_doc=010','','HEIGHT=600,WIDTH=940,STATUS=YES,SCROLLBARS=YES,RESIZABLE=YES,top=0,left=50')">
											<%= comprobante.getNumdoc() %>
										 </span>
									<% } else if (comprobante.getTipodoc().equals("EGR")) { %>
										 <span class="Simulacion_Hiper" style="cursor:pointer" onClick="window.open('<%=CONTROLLERCONTAB%>?estado=Admin&accion=ComprobantesManager&opc=searchEgreso&document=<%= comprobante.getNumdoc() %>&transaction=<%= comprobante.getGrupo_transaccion() %>','','HEIGHT=600,WIDTH=940,STATUS=YES,SCROLLBARS=YES,RESIZABLE=YES,top=0,left=50')">
											<%= comprobante.getNumdoc() %>
										 </span>
									<% } else if (comprobante.getTipodoc().equals("RPL")) { %>
										 <span class="Simulacion_Hiper" style="cursor:pointer" onClick="window.open('<%=CONTROLLERCONTAB%>?estado=Admin&accion=ComprobantesManager&opc=searchRPL&document=<%= comprobante.getNumdoc() %>','','HEIGHT=600,WIDTH=940,STATUS=YES,SCROLLBARS=YES,RESIZABLE=YES,top=0,left=50')">
											<%= comprobante.getNumdoc() %>
										 </span>
									<% } else if (comprobante.getTipodoc().equals("ING") || comprobante.getTipodoc().equals("ICR") ) { %>
									<%
										String urlCliente = "";
										if ( comprobante.getNumdoc().startsWith("IM")  )
											urlCliente = CONTROLLER+"?estado=IngresoMiscelaneo&accion=Buscar&numero="+comprobante.getNumdoc()+"&evento=Cabecera&tipodoc="+comprobante.getTipodoc()+"&pagina=consulta";
										else
											urlCliente = CONTROLLER+"?estado=Cargar&accion=Varios&numero_ingreso="+comprobante.getNumdoc()+"&tipo_doc="+comprobante.getTipodoc()+"&carpeta=jsp/cxcobrar/ingreso_detalle&pagina=ListarItemsIngresoDetalle.jsp&sw=12" ;
									%>
										 <span class="Simulacion_Hiper" style="cursor:pointer" onClick="window.open('<%= urlCliente %>','','HEIGHT=600,WIDTH=940,STATUS=YES,SCROLLBARS=YES,RESIZABLE=YES,top=0,left=50')">
											<%= comprobante.getNumdoc() %>
										 </span>
									<% } else { %>
										<%= comprobante.getNumdoc() %>
									<% } %>
								&nbsp;
								</td>
                                <td class="bordereporte" align="center">
                                    <span class="Simulacion_Hiper" style="cursor:pointer" onClick="window.open('<%=CONTROLLERCONTAB%>?estado=AdminComprobantes&accion=Manager&opc=ver&dstrct=<%=comprobante.getDstrct()%>&tipodoc=<%=comprobante.getTipodoc()%>&numdoc=<%=comprobante.getNumdoc()%>&grupo=<%=comprobante.getGrupo_transaccion()%>&fechaapply=<%=comprobante.getFecha_aplicacion()%>','','HEIGHT=600,WIDTH=940,STATUS=YES,SCROLLBARS=YES,RESIZABLE=YES,top=0,left=50')">
                                        <%= comprobante.getGrupo_transaccion() %>
                                    </span>&nbsp;</td>
                                <td class="bordereporte" align="center"><%= comprobante.getFechadoc() %>&nbsp;</td>
                                <td class="bordereporte" align="center"><%= (!comprobante.getFecha_aplicacion().equals("0099-01-01")?comprobante.getFecha_aplicacion():"") %>&nbsp;</td>
                                <td class="bordereporte" align="center"><%= comprobante.getUsuario() %>&nbsp;</td>
                                <td class="bordereporte" align="right"><%= (comprobante.getTotal_debito()!=0)?Util.customFormat(comprobante.getTotal_debito()) : "" %>&nbsp;</td>
                                <td class="bordereporte" align="right"><%= (comprobante.getTotal_credito()!=0)?Util.customFormat(comprobante.getTotal_credito()) : "" %>&nbsp;</td>
                                </tr>
                            <%}%>
                            <input type="hidden" name="item" id="item" value="">
                            <input type="hidden" name="cantItems" id="cantItems" value="<%= j %>">
                            </form>
                            <tr   class="bordereporte">
                            <td colspan="10" nowrap align="center">
                            </td>
                            </tr>
                          </table>
                      </div>
                    </td>
                </tr>
          </table>
            <%}%>
    </div>
        <script>
            if(document.all){
                //document.getElementById("mes").value = "<%=mes%>";
               //document.getElementById("anio").value = "<%=anio%>";
            }
        </script>
        <iframe width=188 height=166 name="gToday:normal:agenda.js:gfPop:plugins.js" id="gToday:normal:agenda.js:gfPop:plugins.js" src="<%=BASEURL%>/js/Calendario/ipopeng.htm" scrolling="no" frameborder="0" style="visibility:visible; z-index:999; position:absolute; left:-500px; top:0px;"> </iframe>
        <%=datos[1]%>
    </body>
</html>
