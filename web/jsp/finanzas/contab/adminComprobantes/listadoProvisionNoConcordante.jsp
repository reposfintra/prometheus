<%-- Declaracion de librerias--%>
<%@page contentType="text/html"%>
<%@ page session="true"%>
<%@ page errorPage="../error/error.jsp"%>
<%@page import="com.tsp.operation.model.beans.*, java.util.*"%>
<%@ include file="/WEB-INF/InitModel.jsp"%>
<%@page import="com.tsp.operation.model.*"%>
<%@page import="com.tsp.finanzas.contab.model.*"%>
<%@page import="com.tsp.util.*"%>
<%@ taglib uri="http://jakarta.apache.org/taglibs/input-0.90" prefix="input" %>

<html>
<head>
<title></title>
<link href="<%= BASEURL %>/css/estilostsp.css" rel="stylesheet" type="text/css">
<script type='text/javascript' src="<%= BASEURL %>/js/boton.js"></script>
<script type='text/javascript' src="<%= BASEURL %>/js/general.js"></script>
<script src='<%=BASEURL%>/js/date-picker.js'></script>
<script type="text/javascript">

		function send(theForm,ruta){
			theForm.action=ruta;
			theForm.submit();		
		}
		
		function chooseAll(theForm,nombre){			
			for (i=0;i<theForm.length;i++){
				if (theForm.elements[i].type=='checkbox' && !formulario.elements[i].disabled){
					theForm.elements[i].checked=document.getElementById("All").checked;		
				}
			}
	    }
		
		function page(theForm){
			var offsets = document.getElementById("paginas").value;
			var temp = offsets.split(";");
			var ruta = "<%=CONTROLLER%>?estado=Provision&accion=Fenalco&opcion=1&offset_fenalco="+temp[0]+"&offset_fintra="+temp[1]+"&no_pagina="+temp[2];
			theForm.action=ruta;
			theForm.submit();
		}
		
	
</script>
</head>

<body onload="redimensionar();" onresize="redimensionar();">

<div id="capaSuperior" style="position:absolute; width:100%; height:100px; z-index:0; left: 0px; top: 0px;">
    <jsp:include page="/toptsp.jsp?encabezado=Ver detalle provision Fenalco"/>
</div>

<div id="capaCentral" style="position: relative;  width:100%; top:97px; bottom:0px; left:0px; overflow: auto;">   
<table border="0"> 

	<tr>
	
	<th></th>
	
	<th class="subtitulo2">N&uacute;mero aprobaci&oacute;n</th>
	<th class="subtitulo2">Item</th>
	<th class="subtitulo2">NIT Empresa</th>
	<th class="subtitulo2">Nombre Empresa</th>
	<th class="subtitulo2">NIT Cliente</th>
	<th class="subtitulo2">Nombre Cliente</th>
	<th class="subtitulo2">N&uacute;mero t&iacute;tulo </th>
	<th class="subtitulo2">Valor t&iacute;tulo </th>
	<th class="subtitulo2">Provisi&oacute;n Fenalco</th>
	
	<th class="subtitulo1">N&uacute;mero aprobaci&oacute;n</td>
	<th class="subtitulo1">NIT Empresa</th>
	<th class="subtitulo1">Nombre Empresa</th>
	<th class="subtitulo1">NIT Cliente</th>
	<th class="subtitulo1">Nombre Cliente</th>
	<th class="subtitulo1">Factura</th>
	<th class="subtitulo1">N&uacute;mero t&iacute;tulo</th>
	<th class="subtitulo1">Valor t&iacute;tulo</th>
	<th class="subtitulo1">Provisi&oacute;n Fenalco</th>
	<th class="subtitulo1">Negocio Asociado</th>
	
	<th class="provision_fenalco">Diferencia Valor t&iacute;tulo</th>
	<th class="provision_fenalco">N&uacute;mero de documento</th>
	</tr>
	
	<%
		String[] informacion = model.provisionFenalcoService.obtenerListadoFenalcoSinConcordar();
		int i=0;
		String claseFila = "fila_provision2";//CSS
		String fila_provision = "fila_provision";//CSS
		int cont=1;
		int estado = 0;
		double sumaProvisiones = 0;
		double sumaTotal = 0;
		String aval = informacion[1].split(";")[0];
		while(i<informacion.length){
		
			String[] tempo = informacion[i].split(";");
				
			
			out.print("<tr>");
			out.print("<td></td>");
			out.print("<td class="+claseFila+">"+cont+": "+tempo[0]+"</td>");
			out.print("<td class="+claseFila+">"+tempo[8]+"</td>");
			out.print("<td class="+claseFila+">"+tempo[1]+"</td>");
			out.print("<td class="+claseFila+">"+tempo[2]+"</td>");out.print("<td class="+claseFila+">"+tempo[3]+"</td>");
			out.print("<td class="+claseFila+">"+tempo[4]+"</td>");out.print("<td class="+claseFila+">"+tempo[5]+"</td>");
			out.print("<td class="+claseFila+">"+Util.customFormat(Double.parseDouble(tempo[6]))+"</td>");
			out.print("<td class="+claseFila+">"+Util.customFormat(Double.parseDouble(tempo[7]))+"</td>");
			out.print("<tr>");
			i++;
			cont++;
			sumaProvisiones = sumaProvisiones + Double.parseDouble(tempo[7])*1.16;
			sumaTotal = sumaTotal + Double.parseDouble(tempo[7])*1.16;
			if(i<informacion.length-1){
				if(!informacion[i].split(";")[0].equals(aval)){
					out.print("<td colspan = \"8\" class=\"provision_fenalco\"></td><td class=\"provision_fenalco\">Total provisi&oacute;n + IVA: </td><td class=\"provision_fenalco\">"+Util.customFormat(sumaProvisiones)+"</td></tr>");
					aval = informacion[i].split(";")[0];
					sumaProvisiones = 0;
					cont = 1;
					estado = 1;
				}
			}
			
		}	
		out.print("<td colspan = \"8\" class=\"provision_fenalco\"></td><td class=\"provision_fenalco\">Total provisi&oacute;n + IVA :  </td><td class=\"provision_fenalco\">"+Util.customFormat(sumaProvisiones)+"</td></tr>");
		out.print("<td colspan = \"8\" class=\"provision_fenalco\"></td><td class=\"provision_fenalco\">Total provisiones + IVA: </td><td class=\"provision_fenalco\">"+Util.customFormat(sumaTotal)+"</td></tr>");
	%>
	
</table>
		
</div>
</body>
</html>

