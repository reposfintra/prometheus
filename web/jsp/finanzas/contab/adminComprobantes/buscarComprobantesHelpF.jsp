<%@ include file="/WEB-INF/InitModel.jsp"%>

<HTML>
<HEAD>
<TITLE>Proceso de Administraci&oacute;n de comprobantes</TITLE>
<META http-equiv=Content-Type content="text/html; charset=windows-1252">
<link href="<%=BASEURL%>/css/estilostsp.css" rel="stylesheet" type="text/css"> 
<link href="../../../../css/estilostsp.css" rel="stylesheet" type="text/css"> 
<script type='text/javascript' src="<%=BASEURL%>/js/boton.js"></script>
</HEAD>
<BODY>  

  <table width="95%"  border="2" align="center">
    <tr>
      <td>
        <table width="100%" border="0" align="center">
          <tr  class="subtitulo">
            <td height="20"><div align="center">Administraci&oacute;n de comprobantes </div></td>
          </tr>
          <tr class="subtitulo1">
            <td>Descripci&oacute;n del funcionamiento del programa de Administraci&oacute;n de comprobantes </td>
          </tr>
          <tr>
            <td  height="18"  class="ayudaHtmlTexto">A continuaci&oacute;n se presenta el formulario para definir los filtros de b&uacute;squeda de comprobantes, Hacer click en buscar para realizar la b&uacute;squeda. </td>
          </tr>
          <tr>
            <td height="18"  class="ayudaHtmlTexto"><div align="center"><img src="<%=BASEURL%>/images/ayuda/contabilidad/administracionComprobantes/img001.jpg" width="718" height="110"></div></td>
          </tr>
          <tr>
            <td  class="ayudaHtmlTexto">&nbsp;</td>
          </tr>
          <tr>
            <td  class="ayudaHtmlTexto">En la siguiente lista se despliegan los resultados de la anterior b&uacute;squeda, con las diferentes acciones para aplicar a un comprobante, si desea ver el detalle del comprobante y modificarlo debe seleccionar sobre el n&uacute;mero de grupo de transacci&oacute;n, si desea grabar un nuevo comprobante, seleccione la opci&oacute;n Adicionar. para imprimir, contabilizar, descontabilizar, validar y reversar, debe seleccionar uno o varios comprobantes de la lista chequedo cada unos de los cuadros.</td>
          </tr>
          <tr>
            <td  class="ayudaHtmlTexto"><div align="center"><img src="<%=BASEURL%>/images/ayuda/contabilidad/administracionComprobantes/img002.jpg" width="772" height="171"></div></td>
          </tr>   
          <tr>
            <td  class="ayudaHtmlTexto">&nbsp;</td>
          </tr>
          <tr>
            <td  class="ayudaHtmlTexto">El c&oacute;digo de color para los comprobantes que estan descuadrados (valor d&eacute;bito y cr&eacute;dito diferentes) es rojo, y para los que a&uacute;n no han sido contabilizados es amarillo.</td>
          </tr>
          <tr>
            <td  class="ayudaHtmlTexto"><div align="center"><img src="<%=BASEURL%>/images/ayuda/contabilidad/administracionComprobantes/img003.jpg" width="779" height="138"></div></td>
          </tr>   
      </table></td>
    </tr>
  </table>
  <table width="416" align="center">
	<tr>
	<td align="center">
		<img src="<%=BASEURL%>/images/botones/salir.gif" style="cursor:pointer" name="c_salir" onClick="window.close();" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" align="absmiddle">
	</td>
	</tr>
</table>
</BODY>
</HTML>
