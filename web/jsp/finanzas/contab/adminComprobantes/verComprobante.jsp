<%-- Declaracion de librerias--%>
<%@page contentType="text/html"%>
<%@page session="true"%>
<%@page import="java.util.*"%>
<%@page import="com.tsp.operation.model.*"%>
<%@page import="com.tsp.finanzas.contab.model.*"%>
<%@page import="com.tsp.util.*"%>
<%@page import="com.tsp.operation.model.beans.*, java.util.*"%>
<%@page import="com.tsp.finanzas.contab.model.beans.*"%>
<%@include file="/WEB-INF/InitModel.jsp"%>
<%@ taglib uri="http://jakarta.apache.org/taglibs/input-0.90" prefix="input" %>
<%String path = application.getRealPath(model.menuService.getRealUrl(request.getRequestURI()));
  String datos[] = model.menuService.getContenidoMenu(BASEURL,request.getRequestURI(),path);
%>
<html>
    <head><title>Ver Detalle Comprobante</title>
        
        <link href="<%=BASEURL%>/css/estilostsp.css" rel="stylesheet" type="text/css">
        <link href="../../../../css/estilostsp.css" rel="stylesheet" type="text/css"> 
        <script type='text/javascript' src="<%=BASEURL%>/js/Validacion.js"></script> 
        <script type='text/javascript' src="<%= BASEURL %>/js/boton.js"></script>
        <script type='text/javascript' src="<%= BASEURL %>/js/finanzas/contab/comprobante.js"></script>
        <script type='text/javascript' src="<%= BASEURL %>/js/contabilidad.js"></script>
        
        <style type="text/css">
            <!--
            .Estilo1 {color: #D0E8E8}
            -->
        </style>
    </head>

<% String PERIODO  = (String) session.getAttribute("periodo");
   String opcion = (request.getParameter("opcion")!=null)?request.getParameter("opcion"):"";  
   String maxfila                    =  (request.getParameter ("maxfila")==null?"1":request.getParameter ("maxfila")); 
   int    MaxFi                      =  Integer.parseInt(maxfila); 
   List listTipoDoc                  =  modelcontab.GrabarComprobanteSVC.getLista();
   ComprobanteFacturas comprobante   =  modelcontab.GrabarComprobanteSVC.getComprobante();
   String tipoDoc      ="";
   String numDoc       ="";
   String fecha        ="";
   String terceroCab   ="";
   String concepto     ="";
   double totalDebito  =0;
   double totalCredito =0;
   List items          = null;
   int grupoT =0;  
   
   
   if (comprobante != null){
       tipoDoc      = comprobante.getTipodoc();
       numDoc       = comprobante.getNumdoc();
       fecha        = comprobante.getFechadoc();
       terceroCab   = comprobante.getTercero();
       concepto     = comprobante.getDetalle();
       totalDebito  = comprobante.getTotal_debito();
       totalCredito = comprobante.getTotal_credito();
       items        = comprobante.getItems();
	   grupoT       = comprobante.getGrupo_transaccion(); 
   
   
   }
   String estilo="";
   String clase ="";
   String readonly ="";
   if(opcion.equals("modificar")){
   
     estilo ="style='border:0'";
     clase="filaresaltada";
     readonly ="readonly" ;
   }
      
  %>
    <body onresize="redimensionar()" >

        <div id="capaSuperior" style="position:absolute; width:100%; height:100px; z-index:0; left: 0px; top: 0px;">
            <jsp:include page="/toptsp.jsp?encabezado=Ver detalle comprobante Contable"/>
        </div>
        <div id="capaCentral" style="position:absolute; width:100%; height:87%; z-index:0; left: 0px; top: 100px; overflow: scroll;">           
            <script>
                var controlador ="<%=CONTROLLERCONTAB%>";
                var maxfila = <%=maxfila%>

                function formato(numero){
           
                var tmp = parseInt(numero) ;
                    var factor = (tmp < 0 ? - 1 : 1);
                    tmp *= factor;

                    var num = '';
                    var pos = 0;
                    while(tmp>0){
                    if (pos%3==0 && pos!=0) num = ',' + num;
                    res  = tmp % 10;
                    tmp  = parseInt(tmp / 10);
                    num  = res + num  ;
                    pos++;
                    }
                    return (factor==-1 ? '-' : '' ) + num ;
                }      
            </script>
            <form name="forma1" id="forma1" action="" method="post">
            <table width="900" align="center">
				
                <tr><td>					
                <jsp:include page="/jsp/finanzas/contab/infoContab.jsp?tamanoTableEncabezado=100p"/> 					
                <table border="2" width='100%'  >
                <tr>
                <td>
                <table width="100%" align="center"  >
                    <tr >
                        <td width="50%"  class="subtitulo1"><p align="left">Documento</p></td>
                        <td width="50%"  class="barratitulo"><img src="<%=BASEURL%>/images/titulo.gif" class="Estilo1" height="20" align="left"><%=datos[0]%></td>
                    </tr>
                </table>
                <table width="100%" align="center" cols="7">
                <tr class="fila">
                    <td width="12%"><div align="center">Tipo<br>  Documento</div></td>
                    <td width="16%"><div align="center">N�mero<br>  Documento </div></td>
                    <td width="10%"><div align="center">Fecha<br> Registro</div></td>
                    <td width="18%"> <div align="center">Tercero</div></td>
                    <td width="44%"><div align="center">Concepto</div></td>
                </tr>
                <tr class="letra" align="center">
                <td nowrap >                
                     <%for(int i=0; i< listTipoDoc.size();i++){
                           Hashtable fila = (Hashtable)listTipoDoc.get(i);
                           if(tipoDoc.equals((String)fila.get("codigo"))){
                      %>
                        <%=(String)fila.get("descripcion")%>&nbsp;
                     <%    }
                       }%>
                </td>									
                <td><%=numDoc%>&nbsp;</td>  
                <td><%=fecha%>&nbsp;</td>
                <td><%=terceroCab%>&nbsp;</td>                
                <td align="left"><%=concepto%></td>                    
                </tr>
            </table>
                </td>
                </tr>
                </table>
                <table width="100%" border="2" align="left">
                <tr>
                    <td >
                    <table width="100%" align="center"  >
                        <tr >
                            <td width="50%"   class="subtitulo1"><p align="left">Detalle del Documento </p></td>
                            <td width="50%"  class="barratitulo"><img src="<%=BASEURL%>/images/titulo.gif" class="Estilo1" height="20" ></td>
                        </tr>
                    </table>
                        
                    <table id="detalle" width="100%" >
                    <tr  id="fila1" class="tblTitulo">
                        <td width="2%" align="center" nowrap>&nbsp;Item&nbsp;  </td>
                        <td width="12%" align="center">Cod Contable </td>
                        <td width="10%" align="center">Auxiliar</td>
                        <td width="10%" align="center">Tercero      </td>
						<td width="5%"  align="center">ABC</td>
						<td width="12%" align="center">Doc Rel.</td>
                        <td width="25%" align="center">Descripcion</td>
                        <td width="12%" align="center">Valor Debito   </td>
                        <td width="12%" align="center">Valor Credito  </td>
                    </tr>
				
				<%
                  int x=1;
 				  if ( items != null){
					  for(x=1; x<= items.size();x++){
						  ComprobanteFacturas comprodet = (ComprobanteFacturas) items.get(x-1);
                  %>							   
                    <tr class="<%= (x%2==0)? "filagris" : "filaazul" %>" style="cursor:default" id="filaItem<%=x%>" nowrap  bordercolor="#D1DCEB" align="center">
                    <td><a id="n_i<%=x%>" class="informacion"><%=x%>&nbsp;</a></td>
                    <td align="left" class="informacion" nowrap><%=comprodet.getCuenta()%>&nbsp;</td>
                    <td align="left" class="informacion" nowrap ><%=comprodet.getAuxiliar()%>&nbsp;</td>
                    <td align="center" class="informacion" nowrap><%=comprodet.getTercero()%>&nbsp;</td>
                    <td align="center" class="informacion" nowrap><%=comprodet.getAbc()%>&nbsp;</td>				
					<td align="center" class="informacion" nowrap><%= ( !comprodet.getNumdoc_rel().equals("")? comprodet.getTdoc_rel() +"/"+ comprodet.getNumdoc_rel() : "") %>&nbsp;</td>	
                    <td align="left" class="informacion" nowrap title="<%=(comprodet.getDetalle().length() >= 40)?comprodet.getDetalle():""%>"><%=(comprodet.getDetalle().length() >= 40)?comprodet.getDetalle().substring(0,37)+"...":comprodet.getDetalle()%></td>
                    <td align="right" nowrap><%=(comprodet.getTotal_debito()!=0)?Util.customFormat(comprodet.getTotal_debito()):""%></td>
                    <td align="right" nowrap><%=(comprodet.getTotal_credito()!=0)?Util.customFormat(comprodet.getTotal_credito()):""%></td>
                    </tr>				   
						   <%}%>
					<%}%>
                    </table>
                </td>
                </tr>
                <tr >
                    <td >
                        <table width="100%">
                            <tr  class="tblTitulo" >
                                <td width="76%" class="bordereporte" ><div align="right">TOTAL</div></td>
                                <td width="12%" class="bordereporte" align="right" style="font-size:11px "><%=Util.customFormat(totalDebito)%></td>
                                <td width="12%" class="bordereporte" align="right" style="font-size:11px "><%=Util.customFormat(totalCredito)%></td>
                            </tr>
                        </table>
                    </td>
						  
                </tr>
                </table>
            </td>
            </tr>
            </table>            
            <center>                
                &nbsp;<img src='<%=BASEURL%>/images/botones/salir.gif' name='Buscar' align="absmiddle" style='cursor:hand'   onClick="window.close();" onMouseOver='botonOver(this);' onMouseOut='botonOut(this);'>
            </center>

        </form>
		
		
 			 <%
                String mensaje = (String)request.getAttribute( "mensaje" );               
                if( mensaje != null && !mensaje.equals("")){
            %>    
            <table border="2" align="center">
                <tr>
                    <td><table width="100%"  border="1" align="center"  bordercolor="#F7F5F4" bgcolor="#FFFFFF">
                    <tr> 
                    <td width="350" align="center" class="mensajes"><%= mensaje %></td>
                    <td width="29" background="<%=BASEURL%>/images/cuadronaranja.JPG">&nbsp;</td>
                    <td width="58">&nbsp;</td>
                </tr>
                </table></td>
                </tr>
            </table>                   
            <%} %>
		
        </div>
    </body>    
<%=datos[1]%>
</html>s
