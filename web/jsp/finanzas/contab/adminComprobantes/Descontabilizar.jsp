<%@page contentType="text/html"%>
<%@ page session="true"%>
<%@ page errorPage="../error/ErrorPage.jsp"%>
<%@page import="com.tsp.operation.model.beans.*, java.util.*"%>
<%@page import="com.tsp.operation.model.*"%>
<%@page import="com.tsp.finanzas.contab.model.*,com.tsp.finanzas.contab.model.beans.*"%>
<%@page import="com.tsp.util.*"%>
<%@include file="/WEB-INF/InitModel.jsp"%>
<%@ taglib uri="http://jakarta.apache.org/taglibs/input-0.90" prefix="input" %>
<%@taglib uri="http://jsptags.com/tags/navigation/pager" prefix="pg" %>
<%   
        TreeMap opc_docs = modelcontab.tipo_doctoSvc.getTreemap();        
%>
<html>
    <head>
        <title>Descontabilizar</title>
        <link href="<%= BASEURL %>/css/estilostsp.css" rel="stylesheet" type="text/css">
        <script src='<%=BASEURL%>/js/boton.js'></script>
       
        <link href="../../../../css/estilostsp.css" rel="stylesheet" type="text/css">
    </head>
    <body >
        <div id="capaSuperior" style="position:absolute; width:100%; height:100px; z-index:0; left: 0px; top: 0px;">
            <jsp:include page="/WEB-INF/toptsp.jsp?encabezado=Descontabilizar"/>
        </div>
    <div id="capaCentral" style="position:absolute; width:100%; height:85%; z-index:0; left: 0px; top: 100px; overflow: scroll;">
            <br>
            <form name="formBuscar" id="formBuscar" method="post" action="<%=CONTROLLERCONTAB%>?estado=Descontabilizar&accion=Documento&opc=buscar" >
            <table width="50%" border="2" align="center">
                <tr>
                    <td>
                    <table width="100%" align="center" cellpadding="3" cellspacing="2" class="tablaInferior">
                    <tr >
                        <td colspan="2" class="subtitulo1">Criterios de Selecci�n</td>
                        <td colspan="2" class="barratitulo"><img src="<%=BASEURL%>//images/titulo.gif" align="left"></td>
                    </tr>                    
                    <tr class="fila">
                        <td width="120" height="63">Tipo de documento</td>
                        <td width="160">
                            <input:select name="tipodoc" options="<%= opc_docs %>" attributesText="class='textbox' id='tipodoc' style='width:160'" />
                        </td>
                        <td width="80">Documento</td>
                        <td width="120">
                            <input type="text" name="numdoc" id="numdoc" class="textbox" maxlength="10" value="<%=request.getParameter("numdoc")!=null?request.getParameter("numdoc"):""%>" >
                        </td>
                    </tr>
                    </table>
                </td>
                </tr>
            </table>     
            <div align="center"><br>
                <img src="<%= BASEURL%>/images/botones/buscar.gif"  name="imgaceptar" onClick="if(formBuscar.numdoc.value==''){alert('Escriba el numero del documento')}else{formBuscar.submit(); this.disabled=true;}" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);"  style="cursor:hand ">&nbsp;
                <img src="<%= BASEURL%>/images/botones/cancelar.gif"  name="imgcancelar"  onMouseOver="botonOver(this);" onclick="location.replace('<%=CONTROLLER%>?estado=Menu&accion=Cargar&carpeta=/jsp/finanzas/contab/adminComprobantes&pagina=buscarComprobantes.jsp&marco=no&opcion=37');" onMouseOut="botonOut(this);"   style="cursor:hand ">&nbsp;
                <img src="<%= BASEURL%>/images/botones/salir.gif"  name="imgsalir"  onMouseOver="botonOver(this);" onClick="parent.close();" onMouseOut="botonOut(this);" style="cursor:hand ">
            </div>
            </form>              
            <%
                String mensaje = request.getParameter("mensaje");     
                if( mensaje != null ){
            %>    
            <table border="2" align="center">
                <tr>
                    <td><table width="100%"  border="1" align="center"  bordercolor="#F7F5F4" bgcolor="#FFFFFF">
                    <tr> 
                    <td width="350" align="center" class="mensajes"><%= mensaje %></td>
                    <td width="29" background="<%=BASEURL%>/images/cuadronaranja.JPG">&nbsp;</td>
                    <td width="58">&nbsp;</td>
                </tr>
                </table></td>
                </tr>
            </table>                   
            <%}%>
          
    
 <%if( modelcontab.comprobanteService.getComprobante() != null ){%>
           <form name="form1" method="post" action="<%=CONTROLLERCONTAB%>?estado=Descontabilizar&accion=Documento&opc=accion">
              <table width="98%" border="1" align="center">
                <tr>
                  <td height="100%">
                    <table width="100%" border="0"  align="center">
                      <tr class="subtitulo1">
                        <td> &nbsp;Revalorizar Planilla </td>
                      </tr>
                    </table>
                  
                      
					  <% Comprobantes comprobante = modelcontab.comprobanteService.getComprobante();
					  %>
					    <table width="100%" border="1" bordercolor="#999999" bgcolor="#F7F5F4">
                        <tr class="tblTitulo">
                          <td width="9%" align="center">Tipo Documento</td>
                          <td width="14%" align="center">No.Documento</td>
                          <td width="17%" align="center">Cantidad Despacho </td>
                          <td width="16%" align="center">Valor Unitario Planilla</td>
                          <td width="16%" align="center"> Valor Actual Planilla </td>
                          <td width="28%" align="center">Valores Estandar</td>
                        </tr>
                        <tr  class="filagris">
                          <td class="bordereporte" align="center"><%= comprobante.getTipodoc() %>&nbsp;</td>
                          <td class="bordereporte"><%= comprobante.getNumdoc() %>&nbsp;</td>
                          <td class="bordereporte" align="center">
                              <input name="pesoreal" type="text" id="pesoreal" value="<%= comprobante.getCantidad() %>" size="10">
                          </td>
                          <td class="bordereporte" align="center"><input name="flete" type="radio"  value="<%=comprobante.getValor_unit()%>/<%=comprobante.getMoneda()%>" checked>                            <%=com.tsp.util.Util.customFormat( comprobante.getValor_unit() )%>  <%=comprobante.getMoneda()%>&nbsp;</td>
                          <td class="bordereporte" align="center"><%=com.tsp.util.Util.customFormat( comprobante.getValor_for()) %> <%=comprobante.getMoneda()%>&nbsp; </td>
                          <td class="bordereporte" align="center"><table width="100%" border="1" bordercolor="#999999" bgcolor="#F7F5F4">
                            <tr class="tblTitulo">
                              <td width="19%"><div align="center">Aplicar</div></td>
                              <td width="38%"><div align="center">Estandar</div></td>
                              <td width="43%"><div align="center">Valor</div></td>
                            </tr>
							<%Vector vec = comprobante.getLista();
							for(int i=0; i<vec.size(); i++){
								Comprobantes c = (Comprobantes) vec.elementAt(i);
							%>
                            <tr class="<%=i%2==0?"filagris":"filaazul"%>">
                              <td class="bordereporte">
                              <input name="flete" type="radio"  value="<%=c.getValor_unit()%>/<%=c.getMoneda()%>"></td>
                              <td class="bordereporte"><%=c.getSj()%>&nbsp;</td>
                              <td class="bordereporte"><%=com.tsp.util.Util.customFormat(c.getValor_unit())%> <%=c.getMoneda()%></td>
                            </tr>
							<%}%>
                          </table>
						  </td>
                        </tr>
                        <tr   class="bordereporte">
                          <td colspan="6" nowrap align="center"> </td>
                        </tr>
                  </table>				  </td>
                </tr>
             </table>
              <div align="center"><br>
                 <img src="<%= BASEURL%>/images/botones/aceptar.gif"  name="imgaceptar" onMouseOver="botonOver(this);" onClick="form1.submit();this.disabled=true" onMouseOut="botonOut(this);"  style="cursor:hand ">           </div>
           </form>
				 <%}%>       
        <iframe width=188 height=166 name="gToday:normal:agenda.js:gfPop:plugins.js" id="gToday:normal:agenda.js:gfPop:plugins.js" src="<%=BASEURL%>/js/Calendario/ipopeng.htm" scrolling="no" frameborder="0" style="visibility:visible; z-index:999; position:absolute; left:-500px; top:0px;"> </iframe>
       </div>
    </body>
</html>
