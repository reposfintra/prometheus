<!--
   - Author(s)       :      MARIO FONTALVO
   - Date            :      19/04/2007
   - Copyright Notice:      TRANSPORTES SANCHEZ POLO S.A.
-->
<%--   - @(#)
    - Description:  Vista para permitir parametros de entradas para la consulta de
                    movimientos de cuentas auxiliar.
--%>
<%@page session    ="true"%>
<%@page errorPage  ="/error/ErrorPage.jsp"%>
<%@include file    ="/WEB-INF/InitModel.jsp"%>
<%@page import    ="java.util.*" %>
<%@page import    ="com.tsp.util.*" %>
<%@page import    ="com.tsp.operation.model.beans.Usuario" %>

    <!-- Las siguientes librerias CSS y JS son para el manejo de
        DIVS dinamicos.
    -->

<script src="<%=BASEURL%>/js/boton.js"          type="text/javascript"></script>
<link rel="stylesheet" type="text/css" href="<%=BASEURL%>/css/jCal/jscal2.css" />
<link rel="stylesheet" type="text/css" href="<%=BASEURL%>/css/jCal/border-radius.css" />
<script type="text/javascript" src="<%=BASEURL%>/js/jCal/jscal2.js"></script>
<script type="text/javascript" src="<%=BASEURL%>/js/jCal/lang/es.js"></script>
<script src="<%=BASEURL%>/js/prototype.js"      type="text/javascript"></script>


<!-- Para los botones -->
<%@taglib uri="http://jakarta.apache.org/taglibs/input-0.90" prefix="input" %>
<%  Usuario usuario = (Usuario) session.getAttribute("Usuario");
    String distrito = (String) session.getAttribute("Distrito");
    String msg = Util.coalesce((String) request.getAttribute("msg"), "");
%>
<html>
    <head>
        <title>Reporte Movimiento Auxiliar</title>
        <link href="<%= BASEURL%>/css/estilostsp.css" rel="stylesheet" type="text/css">
        <script type='text/javascript' src="<%= BASEURL%>/js/contabilidad.js"></script>
        <script>
            function addOption(Comb,valor,texto){
                var Ele = document.createElement("OPTION");
                Ele.value=valor;
                Ele.text=texto;
                try{
                    $(Comb).add(Ele,null);
                }
                catch(e)
                {   $(Comb).add(Ele);
                }

            }

            function Llenar(CmbAnno, CmbMes){
                var Meses    = new Array('Enero','Febrero','Marzo','Abril','Mayo','Junio','Julio','Agosto','Septiembre','Octubre','Noviembre','Diciembre');
                var FechaAct = new Date();

                for (i=FechaAct.getFullYear()-10;i<=FechaAct.getFullYear();i++){
                    addOption(CmbAnno,i,i);
                    $(CmbAnno).value = FechaAct.getFullYear();
                }

                for (i=0;i<Meses.length;i++){
                    if ((i+1)<10){
                        addOption(CmbMes,'0'+(i+1),Meses[i]);
                    }
                    else{
                        addOption(CmbMes,(i+1),Meses[i]);
                        $(CmbMes).value  = ((FechaAct.getMonth()+1)<10)?('0'+(FechaAct.getMonth()+1)):(FechaAct.getMonth()+1);
                    }
                }
                Calendar.setup({
                    inputField : "fecha_ri",
                    trigger    : "calendar-trigger",
                    onSelect   : function() { this.hide() }
                });
                Calendar.setup({
                    inputField : "fecha_rf",
                    trigger    : "calendar-trigger2",
                    onSelect   : function() { this.hide() }
                });
            }


        </script>
    </head>

    <body>
        <div id="capaSuperior" style="position:absolute; width:100%; height:100px; z-index:0; left: 0px; top: 0px;">
            <jsp:include page="/WEB-INF/toptsp.jsp?encabezado=Consulta Movimientos de Cuentas Auxiliares"/>
        </div>
        <div id="capaCentral" style="position:absolute; width:100%; height:85%; z-index:0; left: 0px; top: 100px; overflow: scroll;">
            <center>


                <!-- formulario -->
                <form action="<%=CONTROLLERCONTAB%>?estado=Reporte&accion=MovimientoAuxiliar" method="post" name="formulario">
                    <table border="2" width="403">
                        <tr>
                            <td width="391">
                                <!-- encabezado -->
                                <table class="tablaInferior" border="0" width="100%">
                                    <tr >
                                        <td width="50%" align="left" class="subtitulo1">&nbsp;Parametros Reporte</td>
                                        <td width="50%" align="left" class="barratitulo"><img src="<%= BASEURL%>/images/titulo.gif" width="32" height="20"  align="left"></td>
                                    </tr>
                                </table>
                                <!-- datos del formulario -->
                                <table class="tablaInferior" border="0" width="100%">
                                    <tr class="fila">
                                        <td width="32%">&nbsp;Distrito</td>
                                        <td >&nbsp;<input type="text" name="distrito" id="distrito" value="<%= distrito%>" readonly> </td>
                                    </tr>
                                    <tr class="filaresaltada">
                                        <td colspan="2" >&nbsp;<input type="radio" name="x" onClick="$('c2').style.display= (this.checked?'none':''); $('c0').style.display= (this.checked?'':'none'); $('c1').style.display=$('c0').style.display ;" checked> &nbsp;Rango de Cuentas</td>
                                    </tr>
                                    <tr class="letra" id="c0">
                                        <td width="32%"  align="right">&nbsp;Cuenta Inicial&nbsp;</td>
                                        <td width="68%">&nbsp;<input type="text" name="cuenta_ri" value=""></td>
                                    </tr>
                                    <tr class="letra" id="c1">
                                        <td width="32%" align="right">&nbsp;Cuenta Final&nbsp;</td>
                                        <td width="68%">&nbsp;<input type="text" name="cuenta_rf" value="">
                                        </td>
                                    </tr>




                                    <tr class="filaresaltada">
                                        <td colspan="2">&nbsp;<input type="radio" name="x" onClick="$('c0').style.display= (this.checked?'none':''); $('c2').style.display= (this.checked?'':'none'); $('c1').style.display=$('c0').style.display ;"> &nbsp;Una Cuenta</td>
                                    </tr>
                                    <tr class="letra" id="c2" style="display:none " >
                                        <td width="32%"  align="right">&nbsp;Cuenta&nbsp;</td>
                                        <td width="68%">&nbsp;<input type="text" name="cuenta" value=""></td>
                                    </tr>




                                    <tr class="letra"  >
                                        <td colspan="2" >&nbsp;</td>
                                    </tr>


                                    <tr class="filaresaltada" >
                                        <td colspan="2">&nbsp;<input type="radio" name="t" onClick="$('p2').style.display= (this.checked?'none':''); $('p0').style.display= (this.checked?'':'none'); $('p1').style.display=$('p0').style.display ;">Rango de Fechas de Registro</td>
                                    </tr>
                                    <tr class="letra" id="p0" style="display:none ">
                                        <td width="32%" style="text-align:right ">&nbsp;Fecha Inicial&nbsp;</td>
                                        <td width="68%">&nbsp;
                                            <input id="fecha_ri" name="fecha_ri" readonly>
                                            <img name="popcal" style="cursor:pointer" id="calendar-trigger" align="absmiddle" src="<%=BASEURL%>/js/calendartsp/calbtn.gif" width="16" height="16" border="0" alt="">
                                        </td>
                                    </tr>
                                    <tr class="letra" id="p1" style="display:none ">
                                        <td width="32%" style="text-align:right ">&nbsp;Fecha Final&nbsp;</td>
                                        <td width="68%">&nbsp;
                                            <input id="fecha_rf" name="fecha_rf" readonly>
                                            <img name="popcal" style="cursor:pointer" id="calendar-trigger2" align="absmiddle" src="<%=BASEURL%>/js/calendartsp/calbtn.gif" width="16" height="16" border="0" alt="">
                                        </td>
                                    </tr>
                                    <tr class="filaresaltada">
                                        <td colspan="2">&nbsp;<input type="radio" name="t" checked onClick="$('p2').style.display=(this.checked?'':'none');  $('p0').style.display=(this.checked?'none':''); $('p1').style.display=$('p0').style.display ;">Busqueda de Comprobantes por periodo</td>
                                    </tr>
                                    <tr class="letra" id="p2">
                                        <td width="32%" style="text-align:right ">&nbsp;Periodo&nbsp;</td>
                                        <td width="68%">&nbsp;
                                            <select class="select" style="width:47%" name="Ano" id="Ano"></select>
                                            <select class="select" style="width:47%" name="Mes" id="Mes"></select>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                    </table>
                    <p>
                        <img src = "<%=BASEURL%>/images/botones/aceptar.gif" style = "cursor:hand" name = "imgaceptar" id="imgaceptar" onClick = "validar();" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);">
                        <img src = "<%=BASEURL%>/images/botones/salir.gif" style = "cursor:hand" name = "imgsalir" onClick = "window.close();" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);">
                    </p>
                    <input type="hidden" name="opcion"    value="REPORTE">
                    <input type="hidden" name="tipo1"      value="">
                    <input type="hidden" name="tipo2"      value="">
                </form>

                <% if (!msg.equals("")) {%>
                <table border="2" align="center">
                    <tr>
                        <td><table width="100%"  border="1" align="center"  bordercolor="#F7F5F4" bgcolor="#FFFFFF">
                                <tr>
                                    <td width="350" align="center" class="mensajes"><%= msg%></td>
                                    <td width="29" background="<%=BASEURL%>/images/cuadronaranja.JPG">&nbsp;</td>
                                    <td width="58">&nbsp;</td>
                                </tr>
                            </table></td>
                    </tr>
                </table>
                <%}%>


            </center>
        </div>

    </body>
</html>
<script>
    function validar(){
        with (formulario){
            // validacion de cuentas
            if (x[0].checked && cuenta_ri.value==''){
                alert('Ingrese la cuenta inicial');
                cuenta_ri.focus();
                return false;
            }
            if (x[0].checked && cuenta_rf.value==''){
                alert('Ingrese la cuenta final');
                cuenta_rf.focus();
                return false;
            }

            if (x[0].checked && cuenta_rf.value<cuenta_ri.value){
                alert('La cuenta final debe ser mayor que la cuenta Inicial');
                cuenta_rf.focus();
                return false;
            }


            if (x[1].checked && cuenta.value==''){
                alert('Ingrese la cuenta ');
                cuenta.focus();
                return false;
            }


            // validacion de fechas
            if (t[0].checked && fecha_ri.value==''){
                alert('Ingrese la fecha inicial');
                fecha_ri.focus();
                return false;
            }
            if (t[0].checked && fecha_rf.value==''){
                alert('Ingrese la fecha final');
                fecha_rf.focus();
                return false;
            }

            if (t[0].checked && fecha_rf.value<fecha_ri.value){
                alert('La fecha final debe ser mayor que la fecha Inicial');
                fecha_rf.focus();
                return false;
            }


            tipo1.value = (t[0].checked?'FECHAS':'PERIODO');
            tipo2.value = (x[0].checked?'RANGO':'CUENTA');

            submit();
        }
    }

    $("distrito").focus();
    Llenar("Ano","Mes");
</script>