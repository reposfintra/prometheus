<%@ include file="/WEB-INF/InitModel.jsp"%>
<html>
<head>
        <title>Ayudas Contabilización Facturas Proveedores</title>        
        <link href="/css/estilostsp.css" rel="stylesheet" type="text/css"> 
        <link href="<%=BASEURL%>/css/estilostsp.css" rel="stylesheet" type="text/css"> 
</head>
<body>
<center>


<% String BASEIMG = BASEURL +"/images/ayuda/contabilidad/contabilizacionProveedor/"; %>

<table width="100%"  border="2" align="center">
    <tr>
      <td >
 
            <table width='99%' align="center" cellpadding='0' cellspacing='0'>
                 <tr class="subtitulo" ><td height="20"><div align="center">MANUAL CONTABILIZACIÓN FACTURAS PROVEEDOR</div></td></tr>
                 <tr class="subtitulo1"> <td>Descripci&oacute;n del funcionamiento </td> </tr>
                 
                 
                 
                 <tr>
                      <td  class="ayudaHtmlTexto" height='40'>
                         El programa permitirá generar la contabilización de facturas a proveedores.
                         Incluye todas las facturas que no esten contabilizadas desde el dia anterior hacia atras,
                         genera un log de errores donde se ecribe las causas de los mismo si los hay.
                         El periodo de contabilización de las facturas será el periodo al cual pertenezca el dia 
                         anterior(ayer).
                       
                      </td>
                 </tr>
                         
                 <tr><td  align="center" > 
                     <br><br>
                     <img  src="<%= BASEIMG%>Conta_proveedores.JPG" >                  
                     <br>
                     <strong>Figura 1</strong>
                     <br><br>
                     <br><br>
                 </td>
                 </tr>
                 
                 
           </table>
            
      </td>
  </tr>
</table>


<br><br>
<img src="<%=BASEURL%>/images/botones/salir.gif"      name="exit"    height="21"  title='Salir'                onClick="parent.close();"                 onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" style="cursor:hand">          


</body>
</html>
