
<!--
     - Author(s)       :      FERNEL VILLACOB
     - Date            :      13/06/2006  
     - Copyright Notice:      TRANSPORTES SANCHEZ POLO S.A.
-->
<%--   
     - @(#)  
     - Description: Permite realizar la contabilizacion de facturas
--%> 
<%@ page session   ="true"%> 
<%@ page errorPage ="/error/ErrorPage.jsp"%>
<%@ page import    ="java.util.*" %>
<%@ page import    ="com.tsp.util.*" %>
<%@ include file="/WEB-INF/InitModel.jsp"%>
<!-- Para los botones -->
<%@ taglib uri="http://www.sanchezpolo.com/sot" prefix="tsp"%>

<html>
<head>
        <title>Contabilizacion Facturas Proveedor</title>
        <link   href="<%= BASEURL %>/css/estilostsp.css" rel="stylesheet" type="text/css"> 
        <script src="<%= BASEURL %>/js/boton.js"></script>
</head>
<body>
     
  
<%String path = application.getRealPath(model.menuService.getRealUrl(request.getRequestURI()));
  String datos[] = model.menuService.getContenidoMenu(BASEURL,request.getRequestURI(),path);%>


<div id="capaSuperior" style="position:absolute; width:100%; height:100px; z-index:0; left: 0px; top: 0px;">
<jsp:include page="/WEB-INF/toptsp.jsp?encabezado=Contabilización de Facturas Clientes"/>
</div>

<div id="capaCentral" style="position:absolute; width:100%; height:85%; z-index:0; left: 0px; top: 100px; overflow: scroll;"> 
 <center>

 <% String msj = (request.getParameter("msj")==null)?"":request.getParameter("msj"); %>
 
 
 <form action='controllercontab?estado=Contabilizacion&accion=Facturas&evento=CONTABILIZAR' method='post' name='formulario' onsubmit='return false;'>
 
       <jsp:include page="/jsp/finanzas/contab/infoContab.jsp?tamanoTableEncabezado=500"/> 
       
       <table width="500" border="2" align="center">
          <tr>
             <td> 
          
                <table width='100%' align='center' class='tablaInferior'>
                
                          <tr>
                                <td class="barratitulo" colspan='2' >
                                       <table cellpadding='0' cellspacing='0' width='100%'>
                                             <tr class="fila">
                                                 <td width="50%" align="left" class="subtitulo1">&nbsp;Proceso de Contabilización</td>
                                                 <td width="*"   align="left"><img src="<%= BASEURL %>/images/titulo.gif" width="32" height="20"  align="left"><%=datos[0]%></td>
                                            </tr>
                                       </table>   
                                </td>
                          </tr>
                          
                          <tr class="barratitulo">
                                 <td  class='informacion' height='60'>
                                           Permite realizar la contabilización de facturas de clientes, tomando todas las facturas
                                           que no esten contabilizadas  desde el dia anterior a la ejecución del programa (ayer) hacia atras, y 
                                           se contabilizarán con el periodo al cual corresponda el dia anterior.
                                           
                                  </td>
                          </tr>

 
                </table>
            </td>
        </tr>
    </table>  
   </form>
   <br>
   
   
   <!--botones -->   
  <p>
  <% if(  msj.equals("") ){%>
        <tsp:boton value="aceptar"   onclick="this.style.visibility='hidden';formulario.submit();"/>   
  <%}%>              
        <tsp:boton value="salir"     onclick="parent.close();"/> 
  </p>
 
 
 
  <% if( ! msj.equals("") ){%>
          <table border="2" align="center">
                  <tr>
                    <td>
                        <table width="100%"  border="1" align="center"  bordercolor="#F7F5F4" bgcolor="#FFFFFF">
                              <tr>
                                    <td width="400" align="center" class="mensajes"><%=msj%></td>
                                    <td width="29" background="<%=BASEURL%>/images/cuadronaranja.JPG">&nbsp;</td>
                                    <td width="58">&nbsp; </td>
                              </tr>
                         </table>
                    </td>
                  </tr>
          </table>
   <%}%>
 
</div>
<%=datos[1]%>

</body>
</html>
