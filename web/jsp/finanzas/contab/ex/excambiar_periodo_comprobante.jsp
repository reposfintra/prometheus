<!--
- Autor : Ing. Julio Ernesto Barros Rueda
- Date  : 14 de Mayo de 2007
- Copyrigth Notice : Fintravalores S.A. S.A

Descripcion : Pagina JSP, que maneja el ingreso de identidades
-->

<%-- Declaracion de librerias--%>
<%@page contentType="text/html;"%>
<%@page session="true"%>
<%@page import="java.util.*"%>
<%@page import="com.tsp.operation.model.*"%> 
<%@page import="com.tsp.operation.model.beans.*"%>
<%@page import="com.tsp.util.*"%>
<%@page import="com.tsp.util.Util.*"%>
<%@include file="/WEB-INF/InitModel.jsp"%>
<%@page import="com.tsp.operation.model.*"%>
<%@taglib uri="http://jakarta.apache.org/taglibs/input-0.90" prefix="input" %>

<html>
    <head>
        <title>Cambio de periodo de comprobante</title>
        <link href="../../../css/estilostsp.css" rel="stylesheet" type="text/css"> 
        <link href="<%=BASEURL%>/css/estilostsp.css" rel="stylesheet" type="text/css">
    </head>
    
    <script type='text/javascript' src="<%=BASEURL%>/js/Validacion.js"></script>
    <script type='text/javascript' src="<%= BASEURL %>/js/boton.js"></script>
    <script type='text/javascript' src="<%= BASEURL %>/js/general.js"></script>
    <script src='<%=BASEURL%>/js/tools.js' language='javascript'></script>
    <script src='<%=BASEURL%>/js/date-picker.js'></script>
    <script src="<%= BASEURL %>/js/transferencias.js"></script>
    
    <%
    //int d=0;//cantidad de decimales
    Usuario usuario            = (Usuario) session.getAttribute("Usuario");
    
    //String valorcheq=request.getParameter("valorcillo");
    
    String Retorno=request.getParameter("respuesta");
    if (Retorno==null){Retorno="";}
    //if (Retorno.equals("")){
        //model.creacionCompraCarteraSvc.cancelarCompraCartera();
    //}
    //TreeMap pro= model.Negociossvc.getProv();
    //pro.put(" Seleccione","");
    //model.clienteService.setTreeMapClientes2();
    //TreeMap pr = model.clienteService.getTreemap();
    //pr.put(" Seleccione","");
    //String fechainicio=request.getParameter("fechainicio");
    //if (fechainicio==null){
        //fechainicio=""+com.tsp.util.Util.AnoActual()+"-"+com.tsp.util.Util.getFechaActual_String(3)+"-"+com.tsp.util.Util.getFechaActual_String(5);
    //}
    
    //public static java.util.Calendar TraerFecha(int fila, int dia, int mes, int ano, String dias_primer_cheque){
    //java.util.Date x;
    //x=(Util.TraerFecha(2, 5, 1, 2008, "30")).getTime();
    //System.out.println("x.toString()="+x.toString());    
    //x=(Util.TraerFecha(3, 5, 1, 2008, "30")).getTime();
    //System.out.println("x.toString()="+x.toString());  
    //System.out.println("Calendar.MONTH="+Calendar.MONTH);  
    /*String itemobtenible=request.getParameter("itemobtenible");
	String fechachequeobtenible=request.getParameter("fechachequeobtenible");
	String feccheqconsigobtenible=request.getParameter("feccheqconsigobtenible");
	String valorobtenible=request.getParameter("valorobtenible");*/
	
	%>
    
    <script>
                
        function enviarFormularioX(CONTROLLER,frm){	
			if (validar(frm)){
				frm.action='<%=CONTROLLER%>?estado=modificacion&accion=Comprobante&opcion=modificarperiodo';
				frm.submit();
			}else{
				alert("Por favor revise los datos.");
			}
        }    
                
       
		function validar(frm){
			var respuesta=true;
			if (frm.documento.value==null || frm.documento.value=='' || frm.fecha.value==null || frm.fecha.value=='' ){
				respuesta=false;
			}
			if (frm.tipodoc.value=="FAP" && (frm.proveedor.value=='' || frm.proveedor.value==null )){
				respuesta=false;
			}
			return respuesta;
		}				
				
    </script>
    
    <body onLoad="javascript:formulario.documento.focus">
        
        <div id="capaSuperior" style="position:absolute; width:100%; height:100px; z-index:0; left: 0px; top: 0px;">
            <jsp:include page="/toptsp.jsp?encabezado=CAMBIO DE PERIODO DE COMPROBANTE"/>
        </div>
        <div id="capaCentral" style="position:absolute; width:100%; height:87%; z-index:0; left: 0px; top: 100px; overflow: scroll;">
            
            <FORM name='formulario' method="post" >
                
                <table width="432" height="430" border="2" align="center">
                    
                    <tr>
                        <td width="420" height="350">
                            <table width="100%" height="72%" class="tablaInferior" >
                                <tr class="fila">
                                    <td colspan="2" align="left" nowrap class="subtitulo">&nbsp;Periodo de comprobante&nbsp;&nbsp; </td>
                                    <td colspan="2" align="left" nowrap class="bordereporte"><img src="<%=BASEURL%>/images/fintra.gif" width="166" height="28" class="bordereporte"> </td>
                                </tr>
                                
                                <tr class="fila">
                                    <td colspan="2" >Documento </td>
                                    <td class="fila">
                                        
                                        
                                            <input name="documento" type="text" class="textbox" id="campo" style="width:200;" size="15"     >                         
                                             
                                            
                                        
                                    </td>
                                </tr>
                                
                                <tr class="fila">
                                    <td colspan="2" >Fecha de comprobante </td>
                                    <td class="fila">

                                        
                                    
                                        <input name='fecha' type='text' class="textbox" id="fechacheque" style='width:120' readonly>
                                            <a href="javascript:void(0);" class="link" onFocus="if(self.gfPop)gfPop.fPopCalendar(document.formulario.fechacheque);return false;"  HIDEFOCUS > <img src="<%=BASEURL%>\js\Calendario\cal.gif" width="16" height="16" border="0" alt="De click aqu&iacute; para ver el calendario.">
											
											</a> 
                                        	
											                                    
                                        
                                    </td>
                                </tr>
                                
                               
                                <tr class="fila">
                                    <td colspan="2" >Periodo </td>
                                    <td>
                                        
                                        <select name='periodo'>
											<option value='200711'>2007-11</option>
                                            <option value='200712'>2007-12</option>	
											<option value='200801'>2008-01</option>	
											<option value='200802'>2008-02</option>	
											<option value='200803'>2008-03</option>	
											<option value='200804'>2008-04</option>	
											<option value='200805'>2008-05</option>	
											<option value='200806'>2008-06</option>	
											<option value='200807'>2008-07</option>	
											<option value='200808'>2008-08</option>	
											<option value='200809'>2008-09</option>		
											<option value='200810'>2008-10</option>		
											<option value='200811'>2008-11</option>		
											<option value='200812'>2008-12</option>
											<option value='200901'>2009-01</option>		
																				                                           
                                        </select>
                                        
                                           
                                    </td> 
                                </tr>
								
								<tr class="fila">
                                    <td colspan="2" >Tipo de documento </td>
                                    <td>
                                        
                                        <select name='tipodoc'>
											<option value='FAC'>FACTURA</option>
                                            <option value='ING'>INGRESO</option>
											<option value='EGR'>EGRESO</option>           
											<option value='FAP'>CXP</option>           
											<option value='ICA'>NOTA AJUSTE</option>           
											<option value='ICR'>NOTA CREDITO ICR</option>           
											<option value='IF'>INGRESOS FENALCO</option>                                           
											<option value='ND'>NOTA DEBITO</option>                                           
											<option value='NC'>NOTA CREDITO NC</option>                                           
											<option value='NEG'>NEGOCIOS</option>                                           																																	
                                        </select>
                                        
                                           
                                    </td> 
                                </tr>
								
								<tr class="fila">
                                    <td colspan="2" >Proveedor (en caso de CXP)</td>
                                    <td class="fila">
                                        
                                        
                                            <input name="proveedor" type="text" class="textbox" id="campo" style="width:200;" size="15"     >                         
                                             
                                            
                                        
                                    </td>
                                </tr>
                          
                </table>
                <br>
                <div align="center">
                    <img src="<%=BASEURL%>/images/botones/aceptar.gif"  name="imgaceptar" onClick="javascript:enviarFormularioX('<%=CONTROLLER%>',formulario);" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" style="cursor:hand">&nbsp;       &nbsp;
                    <img src="<%=BASEURL%>/images/botones/salir.gif"  name="imgsalir"  onMouseOver="botonOver(this);" onClick="parent.close();" onMouseOut="botonOut(this);" style="cursor:hand">&nbsp;       &nbsp;
                   
                </div>
                <br>
                <%
                String mensaje="";
                if( Retorno != null && !(Retorno.equals(""))){
                    if (Retorno.equals("actualizacionexitosa")){
                         mensaje="Actualización exitosa.";
                    }
                    if (Retorno.equals("noapareciocomprobante")){
                         mensaje="No apareció el comprobante.";
                    }
                    
                     
                %>  
                <table border="2" align="center">
                    <tr>
                        <td>
                            <table width="100%"  border="1" align="center"  bordercolor="#F7F5F4" bgcolor="#FFFFFF">
                                <tr>
                                    <td width="229" align="center" class="mensajes"><%=mensaje%></td>
                                    <td width="29" background="<%=BASEURL%>/images/cuadronaranja.JPG">&nbsp;</td>
                                    <td width="58">&nbsp;</td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
                
				<%}  %>
				
				<br>				<br>
				
				<!--
		  <input name="consultablex" type="text" class="textbox" id="campo" style="width:200;" size="15" maxlength="15"   >
		  
          <img src="<%//=BASEURL%>/images/botones/buscar.gif"  name="imgbuscar"  
            onMouseOver="botonOver(this);" 
            onClick="javascript:consultar(formulario);" 
            onMouseOut="botonOut(this);" style="cursor:hand" >

		  <img src="<%//=BASEURL%>/images/botones/modificar.gif"  name="imgmodificar"  
            onMouseOver="botonOver(this);" 
            onClick="javascript:modificar(formulario);" 
            onMouseOut="botonOut(this);" style="cursor:hand" >
			-->
                            
                <br>
            </form>
            
        </div>
       
        <iframe width="188" height="166" name="gToday:normal:agenda.js:gfPop:plugins.js" id="gToday:normal:agenda.js:gfPop:plugins_24.js" src="<%=BASEURL%>/js/Calendario/ipopeng.htm" scrolling="yes" frameborder="0" style="visibility:visible; z-index:999; position:absolute; left:-500px; top:0px;">
        </iframe>
		
    </body>
</html>


