<!--
- Autor : Ing. Leonardo Parody Ponce
- Date : 27 diciembre de 2005
- Copyrigth Notice : Fintravalores S.A. S.A
-->
<%--
-@(#)
--Descripcion : Pagina JSP, que sirve para listar Cuentas Validas.
--%>
<%@page contentType="text/html"%>
<%@page session="true"%>
<%@page errorPage="/error/ErrorPage.jsp"%>
<%@page import="java.util.*"%>
<%@page import="com.tsp.finanzas.contab.model.Model"%>
<%@page import="com.tsp.operation.model.beans.Usuario"%>
<%@page import="com.tsp.finanzas.contab.model.beans.*"%>
<%@page import="com.tsp.finanzas.contab.controller.*"%>
<%@page import="com.tsp.util.*"%>
<%@include file="/WEB-INF/InitModel.jsp"%>
<%@taglib uri="http://jsptags.com/tags/navigation/pager" prefix="pg" %>
<html>
<head>
<title>Listar Cuentas Validas</title>
<script type='text/javascript' src="<%= BASEURL %>/js/validar.js"></script>
<script SRC='<%=BASEURL%>/js/boton.js'></script>
 <link href="../../../css/estilostsp.css" rel='stylesheet' type="text/css">
 <script type='text/javascript' src="<%= BASEURL %>/js/reporte.js"></script>
<script type='text/javascript' src="<%= BASEURL %>/js/boton.js"></script>
<link href="../../../../css/estilostsp.css" rel="stylesheet" type="text/css">
</head>
<body  onresize="redimensionar()" onload = 'redimensionar()'>
<div id="capaSuperior" style="position:absolute; width:100%; height:100px; z-index:0; left: 0px; top: 0px;">
 <jsp:include page="/toptsp.jsp?encabezado=Cuentas Validas"/>
</div>
<div id="capaCentral" style="position:absolute; width:100%; height:87%; z-index:0; left: 0px; top: 100px; overflow: scroll;">

<script type='text/javascript' src="<%= BASEURL %>/js/CuentasValidas.js"></script>

<% 	
	List vec = (List) request.getAttribute("cuentas");
    String style = "simple";
    String position =  "bottom";
    String index =  "center";
    String sw = request.getParameter("sw");
	int maxPageItems = 7;
    int maxIndexPages = 5;
	if (vec.size() > 0) {
%>
<br>
<table width="70%" border="2" align="center">
  <tr>
    <td class="barratitulo">
	  <table width="100%">
        <tr>
          <td width="373" class="subtitulo1">Cuentas validas</td>
          <td width="427" ><img src="<%=BASEURL%>/images/titulo.gif" width="32" height="20"></td>
        </tr>
      </table>
	  <table width="100%" border="1" cellpadding="4" cellspacing="1" bordercolor="#999999" bgcolor="#F7F5F4" align="center">
        <tr class="tblTitulo" align="center">
		  <td width="140" nowrap >Cuenta Valida</td>
          <td width="140" nowrap >Clase de Cuenta</td>
          <td width="140" nowrap >Agencia</td>
          <td width="140" nowrap >Unidad de negocios</td>
          <td width="140" nowrap >Cliente-Area</td>
		  <td width="140" nowrap >Elemento de gasto</td>
		  <td width="140" nowrap >Cuenta Equivalente</td>
		</tr>
    <pg:pager        
		items="<%=vec.size()%>"
        index="<%= index %>"
        maxPageItems="<%= maxPageItems %>"
        maxIndexPages="<%= maxIndexPages %>"
        isOffset="<%= true %>"
        export="offset,currentPageNumber=pageNumber"
        scope="request">
<%
    for (int i = offset.intValue(), l = Math.min(i + maxPageItems, vec.size()); i < l; i++){
        CuentaValida r = (CuentaValida) vec.get(i);
		if (r.getReg_status().equals("")){%>
        <pg:item>
        <tr class="<%=(i % 2 == 0 )?"filagris":"filaazul"%>" onMouseOver='cambiarColorMouse(this)' style="cursor:hand" title="Lista de Campos" onClick="window.open('<%=CONTROLLERCONTAB%>?estado=CuentaValida&accion=Update&clase=<%=r.getClase()%>&agencia=<%=r.getAgencia()%>&elemento=<%=r.getElemento()%>&cliente_area=<%=r.getClienteArea()%>&unidad=<%=r.getUnidad()%>&cuentaequivalente=<%=r.getCuentaEquivalente()%>&sw=Mostrar&listar=False','myWindow','status=yes,scrollbars=no,width=800,height=600,resizable=yes');">
          <td><span class="style7">&nbsp;<%=r.getClase()+r.getAgencia()+r.getUnidad()+r.getClienteArea()+r.getElemento()%></span></td>
		  <td><span class="style7">&nbsp;<%=r.getNombreClase()%></span></td>
          <td><span class="style7">&nbsp;<%=r.getNombreAgencia()%></span></td>
          <td><span class="style7">&nbsp;<%=r.getNombreUnidad()%></span></td>
		  <td><span class="style7">&nbsp;<%=r.getNombreClienteArea()%></span></td>
		  <td><span class="style7">&nbsp;<%=r.getNombreElemento()%></span></td>
   		  <td><span class="style7">&nbsp;<%=r.getCuentaEquivalente()%></span></td>
		</tr>
      </pg:item>
<%  }}%>
        <tr class="pie">
          <td td height="20" colspan="10" nowrap align="center">          <pg:index>
                <jsp:include page="/WEB-INF/jsp/googlesinimg.jsp" flush="true"/>
        </pg:index>
          </td>
        </tr>
    </pg:pager>        
      </table>
    </table>
<%}else {%>
    <table border="2" align="center">
      <tr>
        <td>
	      <table width="100%"  border="1" align="center"  bordercolor="#F7F5F4" bgcolor="#FFFFFF">
            <tr>
              <td width="229" align="center" class="mensajes">No se encontraron Cuentas Validas</td>
              <td width="29"><img src="<%=BASEURL%>/images/cuadronaranja.JPG"></td>
            </tr>
          </table>
	    </td>
      </tr>
    </table>
<%}%>
<p>
<center>
        <img src="<%=BASEURL%>/images/botones/regresar.gif" name="c_cancelar" onClick="history.back();" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);"> 
		<img src="<%=BASEURL%>/images/botones/salir.gif" name="c_salir" id="c_salir" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" onClick="parent.close();">
</center>
</div>
</body>
</html>
