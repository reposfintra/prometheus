<!--
- Autor : Ing. Leonardo Parody Ponce
- Date : 27 diciembre de 2005
- Copyrigth Notice : Fintravalores S.A. S.A
-->
<%--
-@(#)
--Descripcion : Pagina JSP, que sirve para modificar Cuentas Validas.
--%>

<%@page contentType="text/html"%>
<%@page session="true"%>
<%@page import="java.util.*"%>
<%@page import="com.tsp.operation.model.Model"%>
<%@page import="com.tsp.finanzas.contab.model.*"%>
<%@page import="com.tsp.operation.model.beans.Usuario"%>
<%@page import="com.tsp.finanzas.contab.controller.*"%>
<%@page import="com.tsp.util.*"%>
<%@include file="/WEB-INF/InitModel.jsp"%>
<%@ taglib uri="http://jakarta.apache.org/taglibs/input-0.90" prefix="input" %>

<html>
<head>
<title>Modificar Cuentas Validas</title>
<link href="<%= BASEURL %>/css/estilostsp.css" rel='stylesheet'>
<link href="../../../../css/estilostsp.css" rel="stylesheet" type="text/css">
<link href="/css/estilostsp.css" rel="stylesheet" type="text/css">
</head>
<script type='text/javascript' src="<%= BASEURL %>/js/validar.js"></script>
<script type='text/javascript' src="<%= BASEURL %>/js/CuentasValidas.js"></script>
<script src='<%= BASEURL %>/js/date-picker.js'></script>
<script type='text/javascript' src="<%= BASEURL %>/js/boton.js"></script>
<script src='<%=BASEURL%>/js/tools.js' language='javascript'></script>
<body <%if(request.getParameter("reload")!=null){%>onLoad="parent.opener.location.reload();"<%}%> onresize="redimensionar()" onload = 'redimensionar(); MostrarFiltroUpdate();'>
<div id="capaSuperior" style="position:absolute; width:100%; height:100px; z-index:0; left: 0px; top: 0px;">
 <jsp:include page="/toptsp.jsp?encabezado=Cuentas Validas"/>
</div>
<div id="capaCentral" style="position:absolute; width:100%; height:87%; z-index:0; left: 0px; top: 100px; overflow: scroll;">
<%
  TreeMap cuentasequivalentes = modelcontab.cuentaService.getCuentas(); //modelcontab.cuentaService.getCuentas();
  TreeMap cuentas = model.tblgensvc.getClaseCuenta();
  TreeMap unidades = model.tblgensvc.getClaseUnidadProyecto();
  TreeMap elementos = model.tblgensvc.getClaseElementoGasto();
  TreeMap areas = model.tblgensvc.getClaseArea();
  TreeMap agencias = model.tblgensvc.getClaseAgencia();
  Usuario user = (Usuario) session.getAttribute("Usuario");
  String clase = request.getParameter("clase");
  String agencia = request.getParameter("agencia");
  String unidad = request.getParameter("unidad");
  String elemento = request.getParameter("elemento");
  String cliente_area = request.getParameter("cliente_area");
  String msg = request.getParameter("msg");
  String cuentaequivalente = request.getParameter("cuentaequivalente");
  if (msg.equals("")||(msg.equals("Esa cuenta Valida ya Existe"))) {
%>
<form name="forma" id="forma" method="post" action="">
  <table width="401"  border="2" align="center" cellpadding="0" cellspacing="0">
    <tr>
      <td width="395"><table width="100%" class="tablaInferior">
        <tr>
          <td colspan="2" ><table width="100%"  border="0" cellpadding="0" cellspacing="1" class="barratitulo">
              <tr>
                <td width="75%" class="subtitulo1">&nbsp; Modificar Cuentas Validas </td>
                <td width="25%" class="barratitulo"><img src="<%=BASEURL%>/images/titulo.gif" width="32" height="20"></td>
              </tr>
          </table></td>
        </tr>
        <tr class="fila">
          <td>Clase de Cuenta</td>      
          <td ><input:select name="clase1" attributesText="class=textbox" options="<%= cuentas %>" default="<%=clase%>"/></td>
		  </tr>
		<tr class="fila">
          <td>Agencia </td>      
          <td ><input:select name="agencia1" attributesText="class=textbox" options="<%= agencias %>" default="<%=agencia%>"/></td>
		  </tr>
		  <tr class="fila">
          <td>Unidad de Negocios</td>      
          <td ><input:select name="unidad1" attributesText="class=textbox" options="<%= unidades %>" default="<%=unidad%>"/></td>
		  </tr>
		   <tr class="fila">
          <td>Elemento del gasto</td>      
          <td ><input:select name="elemento1" attributesText="class=textbox" options="<%= elementos %>" default="<%=elemento%>"/></td>
		  </tr>
		   <tr>
          <td colspan="2" ><table width="100%"  border="0" cellpadding="0" cellspacing="1" class="barratitulo">
              <tr>
                <td width="75%" class="subtitulo1">&nbsp; Seleccione entre Cliente y Area </td>
                <td width="25%" class="barratitulo"><img src="<%=BASEURL%>/images/titulo.gif" width="32" height="20"></td>
              </tr>
          </table></td>
        </tr>
		   <tr class="fila">
          <td><input name="filtro" type="radio" value="" id="filtro1"  onClick="validarFiltroUpdate();"> Cliente </td>      
          <td ><input name="text" type="text" class="textbox"  id="campo" style="width:200;" onKeyUp="buscar2(document.forma.clientes1,this)" ></td>
		  </tr>
		  <tr class="fila">
		  <td >&nbsp;</td>
		  <td><input:select name="clientes1" attributesText="class=textbox " options="<%=model.clienteService.listar(user)%>" default="<%=cliente_area%>"/></td>
		  </tr>
		  <tr class="fila">
          <td><input name="filtro" type="radio" value="" id="filtro2"  onClick="validarFiltroUpdate();"> Area </td>      
          <td ><input:select name="area1" attributesText="class=textbox " options="<%= areas %>" default="<%=cliente_area%>"/></td>
		  </tr>
		  <tr>
          <td colspan="2" ><table width="100%"  border="0" cellpadding="0" cellspacing="1" class="barratitulo">
              <tr>
                <td width="75%" class="subtitulo1">&nbsp; Seleccione la Cuenta Equivalente</td>
                <td width="25%" class="barratitulo"><img src="<%=BASEURL%>/images/titulo.gif" width="32" height="20"></td>
              </tr>
			</table>
		  </td>
        </tr>
		<tr class="fila">
		<td> Cuenta Equivalente </td> 
		<td ><input name="text" type="text" class="textbox" id="campo" style="width:90%;" onKeyPress="soloDigitos(event,'decNO')" onKeyUp="buscarcuentaequivalente(document.forma.cuentaequivalente1,this)" size="15" ></td>
		 </tr>
		 <tr class="fila">
		  <td >&nbsp;</td>
		  <td><input:select name="cuentaequivalente1" attributesText="class=textbox size=10 style='width:90%;'" options="<%=cuentasequivalentes%>" default="<%=cuentaequivalente%>"/></td>
		 </tr>
		  
      </table></td>
    </tr>
  </table>
  <br>
  <center>
    <img src="<%=BASEURL%>/images/botones/modificar.gif" name="modificar" onClick="validarCuentaValidaUpdate('Modificar','<%=cliente_area%>','<%=clase%>','<%=agencia%>','<%=unidad%>','<%=elemento%>','<%=cuentaequivalente%>');" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);"> 
	<img src="<%=BASEURL%>/images/botones/eliminar.gif" name="eliminar" onClick="validarCuentaValidaUpdate('Eliminar');" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);"> 
	<img src="<%=BASEURL%>/images/botones/salir.gif" name="c_salir" onClick="parent.close();" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);">
  </center>
</form>
  <% }
  if (!msg.equals("")) {%>
  <p><table border="2" align="center">
  <tr>
    <td><table width="100%"  border="1" align="center"  bordercolor="#F7F5F4" bgcolor="#FFFFFF">
      <tr>
        <td width="229" align="center" class="mensajes"><%=request.getParameter("msg").toString()%></td>
        <td width="29" background="<%=BASEURL%>/images/cuadronaranja.JPG">&nbsp;</td>
        <td width="58">&nbsp;</td>
      </tr>
    </table>
   </td>
  </tr>
</table>
<center>
	<img src="<%=BASEURL%>/images/botones/aceptar.gif" name="aceptar" onClick="parent.close();" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);">
</center>
</p>
 <%} %>
</div>
</body>
</html>
