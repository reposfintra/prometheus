<!--
- Autor : Ing. Leonardo Parody Ponce
- Date : 27 diciembre de 2005
- Copyrigth Notice : Fintravalores S.A. S.A
-->
<%--
-@(#)
--Descripcion : Pagina JSP, que sirve para ingresar Cuentas Validas.
--%>

<%@page contentType="text/html"%>
<%@page session="true"%>
<%@page import="java.util.*"%>
<%@page import="com.tsp.operation.model.Model"%>
<%@page import="com.tsp.finanzas.contab.model.*"%>
<%@page import="com.tsp.operation.model.beans.Usuario"%>
<%@page import="com.tsp.finanzas.contab.controller.*"%>
<%@page import="com.tsp.util.*"%>
<%@include file="/WEB-INF/InitModel.jsp"%>
<%@ taglib uri="http://jakarta.apache.org/taglibs/input-0.90" prefix="input" %>

<html>
<head>
<title>Insertar Cuentas Validas</title>
<link href="<%= BASEURL %>/css/estilostsp.css" rel='stylesheet'>
<link href="../../../../css/estilostsp.css" rel="stylesheet" type="text/css">
<link href="/css/estilostsp.css" rel="stylesheet" type="text/css">
</head>
<script type='text/javascript' src="<%= BASEURL %>/js/validar.js"></script>
<script src='<%= BASEURL %>/js/date-picker.js'></script>
<script type='text/javascript' src="<%= BASEURL %>/js/boton.js"></script>
<script src='<%=BASEURL%>/js/tools.js' language='javascript'></script>
<script type='text/javascript' src="<%= BASEURL %>/js/CuentasValidas.js"></script>
<body onresize="redimensionar()" onload = 'redimensionar()'>
<div id="capaSuperior" style="position:absolute; width:100%; height:100px; z-index:0; left: 0px; top: 0px;">
 <jsp:include page="/toptsp.jsp?encabezado=Cuentas Validas"/>
</div>
<div id="capaCentral" style="position:absolute; width:100%; height:87%; z-index:0; left: 0px; top: 100px; overflow: scroll;">
<script type='text/javascript' src="<%= BASEURL %>/js/CuentasValidas.js"></script>
<%TreeMap cuentasequivalentes = modelcontab.cuentaService.getCuentas();//(TreeMap)request.getAttribute("cuentasequivalentes"); 
  request.setAttribute("cuentasequivalentes", cuentasequivalentes);//modelcontab.cuentaService.getCuentas();
  TreeMap cuenta = model.tblgensvc.getClaseCuenta();
  TreeMap unidad = model.tblgensvc.getClaseUnidadProyecto();
  TreeMap elemento = model.tblgensvc.getClaseElementoGasto();
  TreeMap area = model.tblgensvc.getClaseArea();
  TreeMap agencia = model.tblgensvc.getClaseAgencia();
  Usuario user = (Usuario) session.getAttribute("Usuario");
%>
<form name="forma" id="forma" method="post" action="">
  <table width="439"  border="2" align="center" cellpadding="0" cellspacing="0">
    <tr>
      <td width="433"><table width="100%" class="tablaInferior">
        <tr>
          <td colspan="2" ><table width="100%"  border="0" cellpadding="0" cellspacing="1" class="barratitulo">
              <tr>
                <td width="75%" class="subtitulo1">&nbsp; Insertar Cuentas Validas </td>
                <td width="25%" class="barratitulo"><img src="<%=BASEURL%>/images/titulo.gif" width="32" height="20"></td>
              </tr>
          </table></td>
        </tr>
        <tr class="fila">
          <td width="39%">Clase de Cuenta</td>      
          <td width="61%" ><input:select name="clase" attributesText="class=textbox" options="<%= cuenta %>" default="NADA"/></td>
		  </tr>
		<tr class="fila">
          <td>Agencia </td>      
          <td ><input:select name="agencia" attributesText="class=textbox" options="<%= agencia %>" default="NADA"/></td>
		  </tr>
		  <tr class="fila">
          <td>Unidad de Negocios</td>      
          <td ><input:select name="unidad" attributesText="class=textbox" options="<%= unidad %>" default="NADA"/></td>
		  </tr>
		   <tr class="fila">
          <td>Elemento del gasto</td>      
          <td ><input:select name="elemento" attributesText="class=textbox" options="<%= elemento %>" default="NADA"/></td>
		  </tr>
		   <tr>
          <td colspan="2" ><table width="100%"  border="0" cellpadding="0" cellspacing="1" class="barratitulo">
              <tr>
                <td width="75%" class="subtitulo1">&nbsp; Seleccione entre Cliente y Area </td>
                <td width="25%" class="barratitulo"><img src="<%=BASEURL%>/images/titulo.gif" width="32" height="20"></td>
              </tr>
          </table></td>
        </tr>
		   <tr class="fila">
          <td><input name="filtro" type="radio" value="" id="filtro1"  onClick="validarFiltroinsertar();"> Cliente </td>      
          <td ><input name="text" type="text" disabled class="textbox" id="campo" style="width:200;" onKeyUp="buscar(document.forma.clientes,this)" size="15" ></td>
		   </tr>
		  <tr class="fila">
		  <td >&nbsp;</td>
		  <td>		    <input:select name="clientes" attributesText="class=textbox disabled" options="<%=model.clienteService.listar(user)%>" default="NADA"/></td>
		  </tr>
		  <tr class="fila">
          <td><input name="filtro" type="radio" value="" id="filtro2"  onClick="validarFiltroinsertar();"> Area </td>      
          <td ><input:select name="area" attributesText="class=textbox disabled" options="<%= area %>" default="NADA"/></td>
		  </tr>
		  <tr>
          <td colspan="2" ><table width="100%"  border="0" cellpadding="0" cellspacing="1" class="barratitulo">
              <tr>
                <td width="75%" class="subtitulo1">&nbsp; Seleccione la Cuenta Equivalente</td>
                <td width="25%" class="barratitulo"><img src="<%=BASEURL%>/images/titulo.gif" width="32" height="20"></td>
              </tr>
			</table>
		  </td>
        </tr>
		<tr class="fila">
		<td> Cuenta Equivalente </td> 
		<td ><input name="text1" type="text" class="textbox" id="campo1" style="width:90%;" onKeyPress="soloDigitos(event,'decNO')" onKeyUp="buscarcuentaequivalente(document.forma.cuentaequivalente,this)" size="15" maxlength="15"></td>
		 </tr>
		 <tr class="fila">
		  <td >&nbsp;</td>
		  <td><input:select name="cuentaequivalente" attributesText="class=textbox size=10 style='width:90%;'" options="<%=cuentasequivalentes%>" default=""/></td>
		 </tr>
	   </table>
	 </td>
    </tr>
  </table>
  <br>
  <center>
    <img src="<%=BASEURL%>/images/botones/aceptar.gif" name="c_aceptar" onClick="validarCuentaValidaInsert();" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);"> 
	<img src="<%=BASEURL%>/images/botones/cancelar.gif" name="c_cancelar" onClick="forma.reset();" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);"> 
	<img src="<%=BASEURL%>/images/botones/salir.gif" name="c_salir" onClick="parent.close();" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);">
  </center>
</form>
  <% if( !request.getParameter("msg").equals("") ){%>
  <p><table border="2" align="center">
  <tr>
    <td><table width="100%"  border="1" align="center"  bordercolor="#F7F5F4" bgcolor="#FFFFFF">
      <tr>
        <td width="229" align="center" class="mensajes"><%=request.getParameter("msg").toString()%></td>
        <td width="29" background="<%=BASEURL%>/images/cuadronaranja.JPG">&nbsp;</td>
        <td width="58">&nbsp;</td>
      </tr>
    </table></td>
  </tr>
</table>
</p>
 <%} %>
</div>
</body>
</html>

