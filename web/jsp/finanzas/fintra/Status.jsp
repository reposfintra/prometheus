<%@page session="true"%> 
<%@page errorPage="/error/ErrorPage.jsp"%>
<%@page import="java.*" %>
<%@page import="java.util.*" %>
<%@include file="/WEB-INF/InitModel.jsp"%>
<%@page import="com.tsp.util.Util"%>


<html>
<head>

        <title>Status</title>
        <link href="<%= BASEURL %>/css/estilostsp.css" rel="stylesheet" type="text/css"> 
        <script language="JavaScript" type="text/javascript" src="<%=BASEURL%>/js/boton.js"></script>
     
</head>
<body>



<div id="capaSuperior" style="position:absolute; width:100%; height:100px; z-index:0; left: 0px; top: 0px;">
<jsp:include page="/WEB-INF/toptsp.jsp?encabezado=Monitoreo"/>
</div>

<div id="capaCentral" style="position:absolute; width:100%; height:85%; z-index:0; left: 0px; top: 100px; overflow: scroll;"> 
<center>



   <FORM method='post' action="<%=CONTROLLER%>?estado=Status&accion=Barra" name='formCuenta'  id='formCuenta' >
      <table width="450" border="2" align="center">
       <tr>
          <td>  
               <table width='100%' align='center' class='tablaInferior'>

                      <tr class="barratitulo">
                        <td>
                           <table cellpadding='0' cellspacing='0' width='100%'>
                             <tr>
                              <td align="left" width='55%' class="subtitulo1">&nbsp;BARRA DE ESTADOS </td>
                              <td align="left" width='*'  ><img src="<%=BASEURL%>/images/titulo.gif" width="32" height="20"  height="20" align="left"></td>
                            </tr>
                           </table>
                        </td>
                     </tr>
                     
                     <tr class="fila">
                            <td width='100%' align='center'>
                        
                                  <FIELDSET style="width:90%">
                                     <LEGEND>ANTICIPOS</LEGEND>
                                     <table cellpadding='0' cellspacing='0' width='90%' align='center' class='informacion'>
                                          <tr><td>Pendientes por Aprobar       </td><td> <%= model.StatusBarraSvc.getCANT_ANTICIPOS_POR_APROBAR()    %> </td></tr>
                                          <tr><td>Pendientes por Transferir    </td><td> <%= model.StatusBarraSvc.getCANT_ANTICIPOS_POR_TRANSFERIR() %>  </td></tr>
                                     </table>                                     
                                  </FIELDSET>
                            
                                   <BR><BR>
                                  <FIELDSET style="width:90%">
                                     <LEGEND>LIQUIDACIONES</LEGEND>
                                     <table cellpadding='0' cellspacing='0' width='90%' align='center'  class='informacion'>
                                          <tr><td>Pendientes por Aprobar       </td><td> <%= model.StatusBarraSvc.getCANT_LIQ_POR_APROBAR()    %> </td></tr>
                                          <tr><td>Pendientes por Migrar        </td><td> <%= model.StatusBarraSvc.getCANT_LIQ_POR_MIGRAR()     %> </td></tr>
                                     </table> 
                                  </FIELDSET>
                     
                                  <BR>
                                  <BR>
                            </td>
                     </tr>
                     
             </table>
         </td>
      </tr>
   </table> 
         
      
 <p> 
      <img src="<%=BASEURL%>/images/botones/restablecer.gif"      height="21"  title='Refrescar'   onClick="formCuenta.submit()"     onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" style="cursor:hand">
      <img src="<%=BASEURL%>/images/botones/salir.gif"            height="21"  title='Salir'       onClick="window.close();"         onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" style="cursor:hand">           
 </p>

</div>

</body>
</html>
