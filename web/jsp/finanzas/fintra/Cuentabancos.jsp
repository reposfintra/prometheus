<%@page session="true"%> 
<%@page errorPage="/error/ErrorPage.jsp"%>
<%@page import="java.*" %>
<%@page import="java.util.*" %>
<%@include file="/WEB-INF/InitModel.jsp"%>
<%@page import="com.tsp.util.Util"%>
<%@page import="com.tsp.operation.model.beans.Usuario"%>

<html>
<head>
          <title>Bancos</title>
          <link href="<%= BASEURL %>/css/estilostsp.css" rel="stylesheet" type="text/css"> 
          <script language="JavaScript" type="text/javascript" src="<%=BASEURL%>/js/boton.js"></script>
     
          
          <script>
              function sendCuenta(theForm, evento){
                 var  sw    = 0;
                 var  url   =  theForm.action;
                 var  tipo  =  theForm.tipo.value;
				 var  emono = false;
                 
                 if(evento=='SAVE' ||  evento=='UPDATE' )
                   for(var i=0;i<theForm.length;i++){
                      if(tipo=='EF'){
                         if( theForm.elements[i].type=='text' &&  ( theForm.elements[i].name=='mims'  || theForm.elements[i].name=='nit'  ||  theForm.elements[i].name=='nombre_cuenta' ||  theForm.elements[i].name=='cedula_cuenta'  )  &&   theForm.elements[i].value=='' && theForm.elements[i].name!='e_mail' ){
                               sw = 1;
                               alert('Deber� llenar el campo ' + theForm.elements[i].title );
                               theForm.elements[i].focus();
                               break;
                         }
                      }
                      else if( theForm.elements[i].type=='text' &&  theForm.elements[i].name!='sucursal'  &&   theForm.elements[i].value==''){
                               sw = 1;
                               alert('Deber� llenar el campo ' + theForm.elements[i].title );
                               theForm.elements[i].focus();
                               break;
                      }
					  if( theForm.elements[i].name == 'e_mail' ){
					  		if( theForm.elements[i].value != '' && isEmail(theForm.elements[i].value) == false){
						    	theForm.elements[i].value='';
							}
					  }
						 
                   }
                    
                if(sw==0){
                    if(  evento=='LISTAR'   ){
                        var win = window.open(url += evento,'listaCuentas',' top=100,left=100, width=720, height=500, scrollbars=yes, status=yes, resizable=yes  ');                                                              
                        win.focus();   
                    }    
                    else{                    
                         if( isNaN( theForm.descuento.value ) ||  isNaN( theForm.mims.value )  ||  isNaN( theForm.nit.value )  ||  isNaN( theForm.cuenta.value )   ||  isNaN( theForm.cedula_cuenta.value ) )
                              alert('Deber� digitar solo n�meros en campos num�ricos, por favor varifique...');
                         else{
                                theForm.action += evento;
                                theForm.submit();
                                theForm.action  = url;  
                        }
                    }
                }
              }
			  function isEmail(s){
			  		var t = s; 
			  		var positionOfAt;
					var positionOfDot;
					positionOfAt  = s.indexOf('@',1);
					positionOfDot = t.lastIndexOf('.');
					if( (positionOfAt == -1) || (positionOfDot == -1) || (positionOfAt == (s.length-1)) || (positionOfDot == (s.length-1)) || (positionOfAt > positionOfDot) ){
						alert('Direcci�n e-mail en formato no v�lido, por lo tanto no se modificara el campo');
						return false;
					}
					return true;
			  }
          </script>
          
</head>
<body>
 
<div id="capaSuperior" style="position:absolute; width:100%; height:100px; z-index:0; left: 0px; top: 0px;">
<jsp:include page="/WEB-INF/toptsp.jsp?encabezado=Administrar Cuenta"/>
</div>

<div id="capaCentral" style="position:absolute; width:100%; height:85%; z-index:0; left: 0px; top: 100px; overflow: scroll;"> 
<center>
  

  <% Usuario   User           = (Usuario)session.getAttribute("Usuario");
     String    usuario        = User.getLogin();   
     String    distrito       = User.getDstrct();

     Hashtable cuenta         = model.CuentaBancoSvc.getCuentaPropietio();
     List      tcuenta        = model.CuentaBancoSvc.getTipoCuenta();     
     List      lBancos        = model.CuentaBancoSvc.getListaBancos();    
     String    mims           = (request.getParameter("mims")      == null)?"":request.getParameter("mims"); 
     String    nit            = (request.getParameter("nit")       == null)?"":request.getParameter("nit");  
     String    banco          = (request.getParameter("banco")     == null)?"":request.getParameter("banco");
     String    sucursal       = (request.getParameter("sucursal")  == null)?"":request.getParameter("sucursal");
     String    nameCta        = (request.getParameter("nameCta")   == null)?"":request.getParameter("nameCta");
     String    nitCta         = (request.getParameter("nitCta")    == null)?"":request.getParameter("nitCta");
     String    Cta            = (request.getParameter("noCta")     == null)?"":request.getParameter("noCta");
     String    tipoCta        = (request.getParameter("tipoCta")   == null)?"":request.getParameter("tipoCta");
     String    descuento      = (request.getParameter("descuento") == null)?"0":request.getParameter("descuento");
     String    secuencia      = (request.getParameter("secuencia") == null)?"0":request.getParameter("secuencia");
     String    primaria       = (request.getParameter("primaria")  == null)?"N":request.getParameter("primaria"); 
	 String    e_mail         = (request.getParameter("e_mail")    == null)?"":request.getParameter("e_mail");
     String    msj            = request.getParameter("msj"); 
	 %>
     
     
   <FORM method='post' action="<%= CONTROLLER %>?estado=Cuentas&accion=Propietarios&evento=" name='formCuenta'  id='formCuenta' >
   
    <table width="620" border="2" align="center">
       <tr>
          <td>  
               <table width='100%' align='center' class='tablaInferior'>

                      <tr class="barratitulo">
                        <td colspan='4' >
                           <table cellpadding='0' cellspacing='0' width='100%'>
                             <tr>
                              <td align="left" width='55%' class="subtitulo1">&nbsp;CUENTAS DE PAGO A PROPIETARIOS</td>
                              <td align="left" width='*'  ><img src="<%=BASEURL%>/images/titulo.gif" width="32" height="20"  height="20" align="left"></td>
                            </tr>
                           </table>
                        </td>
                     </tr>
                     
                     
                      <TR class='fila'>
                             <TD width='22%' >&nbsp <b>ID MIMS                     </TD>  
                             <TD width='32%'>
                                        <input   style="width:50%" class='comentario'   type='text' name='mims'  id='mims'  value='<%=mims %>'   title='ID MIMS'         maxlength='6'>
                                        <img src="<%=BASEURL%>/images/buscar.gif" title='Buscar por mims' onclick="if( mims.value!='') {var x = window.open('<%= CONTROLLER %>?estado=Cuentas&accion=Propietarios&evento=SEARCH&tipo=MIMS&id='+ mims.value,'Existe',' top=100,left=100, width=400, height=300, scrollbars=yes, status=yes, resizable=yes' );   x.focus(); }else{ alert('Deber� digitar el c�digo mims');  mims.focus();  }">
                             </TD>      
                             <TD width='20%' class='fila'>&nbsp <b>IDENTIFICACION  </TD>  
                             <TD width='*'  >
                                        <input   style="width:60%" class='comentario'  type='text' name='nit' id='nit'      value='<%=nit %>'    title='IDENTIFICACION'  maxlength='15'>
                                        <img src="<%=BASEURL%>/images/buscar.gif"                title='Buscar por Nit'              onclick="if( nit.value!='') {var x = window.open('<%= CONTROLLER %>?estado=Cuentas&accion=Propietarios&evento=SEARCH&tipo=NIT&id='+ nit.value,'Existe','  top=100,left=100, width=400, height=300, scrollbars=yes, status=yes, resizable=yes' );  x.focus();  }else{ alert('Deber� digitar el nit');  nit.focus();  }">
                                        <img src="<%=BASEURL%>/images/botones/iconos/new.gif"    title='Listar cuentas por Nit'      onclick="if( nit.value!='') {var x = window.open('<%= CONTROLLER %>?estado=Cuentas&accion=Propietarios&evento=LISTAR_NIT&nit='+ nit.value,'Bancoss',   '  top=100,left=100, width=600, height=500, scrollbars=yes, status=yes, resizable=yes' );  x.focus();  }else{ alert('Deber� digitar el nit');  nit.focus();  }">
                             </TD>
                 </TR>       
                            
                           
 
                       
                       
                       
                       
                       <TR  class='fila'>
                                <TD  class='comentario'>&nbsp <b>BANCO           </TD>   
                                <TD>

                                     <select   name='banco' title='BANCO'  class='comentario' style="width:100%" >
                                       <option  value='' ></option>
                                       <% for (int i=0;i<lBancos.size();i++){
                                               Hashtable  ht = (Hashtable)lBancos.get(i);
                                               String     se = ( ht.get("codigo").equals( banco ) )?"selected":"";%>
                                               <option  value='<%= ht.get("codigo") %>'  <%= se%> ><%= ht.get("descripcion")%></option>
                                        <%}%>
                                    </select>

                                </TD>      
                                <TD  class='comentario'>&nbsp <b>SUCURSAL        </TD>   <td><input value='<%= sucursal%>'  style="width:100%" class='comentario' type='text' name='sucursal'  title='LUGAR DE PAGO'   maxlength='30'></td>
                       </TR>
                           
                       
                       
                           <TR   class='fila'>
                                <TD class='comentario'>&nbsp <b>CUENTA          </TD>   <td><input value='<%= Cta%>'   style="width:100%" class='comentario' type='text' name='cuenta'    title='CUENTA'          maxlength='20'></td>
                                <TD class='comentario'>&nbsp <b>TIPO CUENTA     </TD>   
                                <td>
                                    <select   name='tipo' title='TIPO CUENTA'  class='comentario' style="width:100%" >
                                       <% for (int i=0;i<tcuenta.size();i++){
                                               Hashtable  ht = (Hashtable)tcuenta.get(i);
                                               String     se = ( ht.get("codigo").equals( tipoCta ) )?"selected":"";%>
                                               <option  value='<%= ht.get("codigo") %>'  <%= se%> ><%= ht.get("descripcion")%></option>
                                        <%}%>
                                    </select>
                                </td>
                           </TR>
                           
                           
                           <TR   class='fila'>
                                <TD height='25' class='comentario'>&nbsp <b>NOMBRE CUENTA   </TD>   
                                <td colspan='3'><input value='<%= nameCta%>'   style="width:100%" class='comentario' type='text' name='nombre_cuenta'   title='NOMBRE CUENTA' maxlength='100'></td>
                           </TR>
                           
                           
                           <TR   class='fila'>
                                <TD height='25' class='comentario'>&nbsp <b>CEDULA CUENTA   </TD>   
                                <td ><input value='<%= nitCta%>'   style="width:100%" class='comentario' type='text' name='cedula_cuenta'   title='CEDULA CUENTA' maxlength='15'></td>


                                <TD height='25' class='comentario'>&nbsp <b>DESCUENTO   </TD>   
                                <td ><input value='<%= descuento%>'   style="width:32%" class='comentario' type='text' name='descuento'   title='DESCUENTO' maxlength='4'></td>

                           </TR>

                           
                           <TR   class='fila'>
                                <TD height='25' class='comentario'>&nbsp <b>DEFAULT  </TD>   
                                <td colspan='1'>

                                    <select name='principal'  class='comentario' style="width:40">
                                        <% String  selN  = (  primaria.equals("N") )?"selected":"";
                                           String  selS  = (! primaria.equals("N") )?"selected":""; %>
                                        <option value='N' <%= selN %> >N
                                        <option value='S' <%= selS %> >S
                                    </select>

                                </td>
								<TD height='25' class='comentario'>&nbsp <b>E_MAIL  </TD>  
								<td colspan='1'>
														 <input value='<%=e_mail%>'   style="width:100%" class='comentario' type='text' name='e_mail' title='e_mail' maxlength='40'>
                          </td>
                           </TR>
              </table>
         </td>
      </tr>
   </table>   
   
   
     <input type='hidden' name='distrito'     value='<%= distrito   %>'> 
     <input type='hidden' name='usuario'      value='<%= usuario    %>'> 
     <input type='hidden' name='secuencia'    value='<%= secuencia  %>'> 
     
 </FORM>
 
 
 
 <p> 
         <img src="<%=BASEURL%>/images/botones/aceptar.gif"          height="21"  title='Guardar'             onClick="sendCuenta(formCuenta, 'SAVE')"      onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" style="cursor:hand">
         <img src="<%=BASEURL%>/images/botones/modificar.gif"        height="21"  title='Modificar'           onClick="sendCuenta(formCuenta, 'UPDATE')"    onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" style="cursor:hand">
         <img src="<%=BASEURL%>/images/botones/detalles.gif"         height="21"  title='Listar General'      onClick="sendCuenta(formCuenta, 'LISTAR')"    onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" style="cursor:hand">
          <img src="<%=BASEURL%>/images/botones/restablecer.gif"     height="21"  title='Limpiar formulario'  onclick="location.href ='<%=CONTROLLER%>?estado=Cuentas&accion=Propietarios&evento=INIT';"   onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" style="cursor:hand">  
         <img src="<%=BASEURL%>/images/botones/salir.gif"            height="21"  title='Salir'              onClick="window.close();"                    onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" style="cursor:hand">
           
 </p>
 
 
 
 
  
  
  <% if(msj!=null  &&  !msj.equals("") ){%>
                <br>
                <table border="2" align="center">
                      <tr>
                        <td>
                            <table width="100%"  border="1" align="center"  bordercolor="#F7F5F4" bgcolor="#FFFFFF">
                                  <tr>
                                        <td width="450" align="center" class="mensajes"><%= msj %></td>
                                        <td width="29" background="<%=BASEURL%>/images/cuadronaranja.JPG">&nbsp;</td>
                                        <td width="58">&nbsp; </td>
                                  </tr>
                          </table>
                        </td>
                      </tr>
                </table>           
    <%}%>
           
</div>


</body>
</html>
