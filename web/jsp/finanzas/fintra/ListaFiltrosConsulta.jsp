<!--
- Autor      : Ing. Fernell Villacob
- Date       : 23  Agosto 2006
- Copyrigth Notice : Fintravalores S.A. S.A
-->
<%--
-@(#)
--Descripcion : Vista que permite mostrar el listado de la consulta
--%>


<%-- Declaracion de librerias--%>
<%@page contentType="text/html"%>
<%@page session="true"%>
<%@page errorPage="../error/ErrorPage.jsp"%>
<%@page import="java.util.*"%>
<%@page import="com.tsp.util.*"%>
<%@page import="com.tsp.operation.model.beans.*"%>
<%@include file="/WEB-INF/InitModel.jsp"%>


<html>
<head>
        <title>Detalle Consulta Anticipos Pagos Terceros</title>
        <link href="<%= BASEURL %>/css/estilostsp.css" rel="stylesheet" type="text/css">
	<script language="JavaScript" type="text/javascript" src="<%=BASEURL%>/js/boton.js"></script>

        <!--inicio de 20101014-->
        <script src="<%=BASEURL%>/js/prototype.js"      type="text/javascript"></script>
        <script src="<%=BASEURL%>/js/effects.js"        type="text/javascript"></script>
        <script src="<%=BASEURL%>/js/window.js"         type="text/javascript"></script>
        <link href="<%=BASEURL%>/css/alert.css" rel="stylesheet" type="text/css"/>
        <link href="<%=BASEURL%>/css/default.css"       rel="stylesheet" type="text/css">
        <!--fin de 20101014-->

</head>
<body>



<div id="capaSuperior" style="position:absolute; width:100%; height:100px; z-index:0; left: 0px; top: 0px;">
<jsp:include page="/WEB-INF/toptsp.jsp?encabezado=Anticipos Pagos Terceros"/>
</div>

<div id="capaCentral" style="position:absolute; width:100%; height:85%; z-index:0; left: 0px; top: 100px; overflow: scroll;">
<center>




   <% List  lista     = model.ConsultaAnticiposTercerosSvc.getLista();
      String  msj     = request.getParameter("msj");
	  double  T_vlr   = 0;
	  double  T_vlrC  = 0;
	  double  T_vlrT  = 0;
	  double  T_vlrDP = 0;
          String conceptico="";//20100728
       %>

    <%String  semirepetidos=model.AnticiposPagosTercerosSvc.obtainAnticiposSemiRepetidos(lista);//20101013 se hallan los semirepetidos para la lista%>

    <table width="620" border="2" align="center">
               <tr>
                  <td>
                       <table width='100%' align='center' class='tablaInferior'>

                          <tr class="barratitulo">
                            <td colspan='11' >
                               <table cellpadding='0' cellspacing='0' width='100%'>
                                     <tr>
                                          <td align="left" width='70%' class="subtitulo1">&nbsp; RESULTADO DE BUSQUEDA </td>
                                          <td align="left" width='*'  ><img src="<%=BASEURL%>/images/titulo.gif" width="32" height="20"  height="20" align="left"></td>
                                    </tr>
                               </table>
                            </td>
                         </tr>

                          <tr class="fila">
                            <td width='100%' colspan='2'>
                                <table width='100%' border="1" bordercolor="#999999" bgcolor="#F7F5F4" align="center">

                                      <tr class="tblTitulo" >
                                         <TH   nowrap style="font-size:11; font-weight: bold" colspan='15' height='25' >DATOS ANTICIPO        </TH><!--20100728-->
                                         <TH   nowrap style="font-size:11; font-weight: bold; background:'#ECE9D8'"  colspan='2' >ESTADO                   </TH>
                                         <TH   nowrap style="font-size:11; font-weight: bold; background:'#cdcdf5'"  colspan='8' >DATOS DE TRANFERENCIA    </TH>
                                         <TH   nowrap style="font-size:11; font-weight: bold; background:'#ffa928'"  colspan='12' >ESTADO FACTURA EN MIMS  </TH>
                                      </tr>


                                      <tr class="tblTitulo" >
                                              <TH   nowrap style="font-size:11; font-weight: bold" height='25' >No </TH>
                                              <TH   nowrap style="font-size:11; font-weight: bold" >&nbsp ESTADO        &nbsp </TH>
                                              <TH   nowrap style="font-size:11; font-weight: bold" >&nbsp OBSERVACION        &nbsp </TH>
                                              <TH   nowrap style="font-size:11; font-weight: bold" >&nbsp AGENCIA        &nbsp </TH>
                                              <TH   nowrap style="font-size:11; font-weight: bold" >&nbsp CONDUCTOR      &nbsp </TH>
                                              <TH   nowrap style="font-size:11; font-weight: bold" >&nbsp PROPIETARIO    &nbsp </TH>
                                              <TH   nowrap style="font-size:11; font-weight: bold" >&nbsp PLACA          &nbsp </TH>
                                              <TH   nowrap style="font-size:11; font-weight: bold" >&nbsp PLANILLA       &nbsp </TH>

                                              <TH   nowrap style="font-size:11; font-weight: bold" >&nbsp VALOR          &nbsp </TH>
                                              <TH   nowrap style="font-size:11; font-weight: bold" >&nbsp FECHA          &nbsp </TH>
                                              <TH   nowrap style="font-size:11; font-weight: bold" title='Reanticipo' >&nbsp REANT    &nbsp </TH>
                                              <TH   nowrap style="font-size:11; font-weight: bold" title='Liquidación'>&nbsp No LIQ   &nbsp </TH>

                                              <TH   nowrap style="font size:11; font weight: bold" >&nbsp ASESOR         &nbsp </TH>  <!--20100728-->
                                              <TH   nowrap style="font size:11; font weight: bold" >&nbsp REFERENCIADO   &nbsp </TH>  <!--20100728-->
                                              <TH   nowrap style="font size:11; font weight: bold" >&nbsp USER_CREACION  &nbsp </TH>  <!--20100728-->

                                              <TH   nowrap style="font-size:11; font-weight: bold" title='Aprobado'   >&nbsp APROB    &nbsp </TH>
                                              <TH   nowrap style="font-size:11; font-weight: bold" title='Tranferido' >&nbsp TRANS    &nbsp </TH>


                                              <TH   nowrap style="font-size:11; font-weight: bold" >&nbsp BANCO           &nbsp </TH>
                                              <TH   nowrap style="font-size:11; font-weight: bold" >&nbsp CUENTA          &nbsp </TH>
                                              <TH   nowrap style="font-size:11; font-weight: bold" >&nbsp TIPO CTA        &nbsp </TH>
                                              <TH   nowrap style="font-size:11; font-weight: bold" >&nbsp NOMBRE CTA      &nbsp </TH>
                                              <TH   nowrap style="font-size:11; font-weight: bold" >&nbsp NIT CUENTA      &nbsp </TH>
                                              <TH   nowrap style="font-size:11; font-weight: bold" >&nbsp VLR CONSIGNADO  &nbsp </TH>
                                              <TH   nowrap style="font-size:11; font-weight: bold" >&nbsp FECHA TRANSFERENCIA &nbsp </TH>
                                              <TH   nowrap style="font-size:11; font-weight: bold" >&nbsp TRANSFERENCIA   &nbsp </TH>

                                              <TH   nowrap style="font-size:11; font-weight: bold; " >&nbsp FECHA MIG.      &nbsp </TH>
                                              <TH   nowrap style="font-size:11; font-weight: bold; " >&nbsp FACTURA         &nbsp </TH>

                                              <TH   nowrap style="font-size:11; font-weight: bold;  background:'f5dddd'" >&nbsp VLR TERCERO      &nbsp </TH>
                                              <TH   nowrap style="font-size:11; font-weight: bold;  background:'f5dddd'" >&nbsp ESTADO FAC TER   &nbsp </TH>
                                              <TH   nowrap style="font-size:11; font-weight: bold;  background:'f5dddd'" >&nbsp FEC PAGO TER     &nbsp </TH>
                                              <TH   nowrap style="font-size:11; font-weight: bold;  background:'f5dddd'" >&nbsp CHEQUE FAC TER   &nbsp </TH>
                                              <TH   nowrap style="font-size:11; font-weight: bold;  background:'f5dddd'" >&nbsp CORRIDA FAC TER  &nbsp </TH>
                                              <TH   nowrap style="font-size:11; font-weight: bold;  background:'#ECE9D8'" >&nbsp VLR DESC PROP    &nbsp </TH>
                                              <TH   nowrap style="font-size:11; font-weight: bold;  background:'#ECE9D8'" >&nbsp ESTADO FAC PRO   &nbsp </TH>
                                              <TH   nowrap style="font-size:11; font-weight: bold;  background:'#ECE9D8'" >&nbsp FEC DESC PRO     &nbsp </TH>
                                              <TH   nowrap style="font-size:11; font-weight: bold;  background:'#ECE9D8'" >&nbsp CHEQUE FAC PRO   &nbsp </TH>
                                              <TH   nowrap style="font-size:11; font-weight: bold;  background:'#ECE9D8'" >&nbsp CORRIDA FAC PRO  &nbsp </TH>
                                              <TH   nowrap style="font-size:11; font-weight: bold;  background:'#ECE9D8'" >&nbsp FECHA TRANSFERENCIA GASOLINA &nbsp</TH>
                                              <TH   nowrap style="font-size:11; font-weight: bold;  background:'#ECE9D8'" >&nbsp USUARIO TRANSFERENCIA GASOLINA  &nbsp </TH>
                                              <TH   nowrap style="font-size:11; font-weight: bold;  background:'#ECE9D8'" >&nbsp NOMBRE EDS  &nbsp </TH>

                                      </tr>





                                       <% for(int i=0;i<lista.size();i++){
                                                 AnticiposTerceros anticipo = (AnticiposTerceros) lista.get(i);
                                                 String colorAprobado    = (anticipo.getAprobado().equals("S"))   ?"background:'#99cc99'":"background:'#f5f5cd'";
                                                 String colorTransferido = (anticipo.getTransferido().equals("S"))?"background:'#99cc99'":"background:'#f5f5cd'";
                                                 String colorEstTer      = (anticipo.getEstado_pago_tercero().equals("50"))     ?"background:'#99cc99'":"background:'#f5f5cd'";
                                                 String colorEstPro      = (anticipo.getEstado_desc_propietario().equals("50")) ?"background:'#99cc99'":"background:'#f5f5cd'";
                                                 double vlrConsig        = (anticipo.getTransferido().equals("S"))?anticipo.getVlrConsignar():0;

												 T_vlr   += anticipo.getVlr() ;
												 T_vlrC  += vlrConsig ;
												 T_vlrT  += anticipo.getVlr_mims_tercero();
												 T_vlrDP += anticipo.getVlr_mims_propietario();

                                                 %>
                                                 <tr class='<%= (i%2==0?"filagris":"filaazul") %>'    style=" font-size:12"   >
                                                         <td class="bordereporte"                nowrap style="font-size:10"> <%=  i+1   %>                                 </td>
                                                         <td class="bordereporte"                nowrap style="font-size:10"> <%=  anticipo.getReg_status()              %> </td>
                                                         <td class="bordereporte"                nowrap style="font-size:10" title="<%=anticipo.getObs_anulacion()%>"> <%=((anticipo.getObs_anulacion().length()>18)?anticipo.getObs_anulacion().substring(0,17):anticipo.getObs_anulacion())+"..."%> </td>
                                                         <td class="bordereporte"                nowrap style="font-size:10"> <%=  anticipo.getNombreAgencia()           %> </td>
                                                         <td class="bordereporte"                nowrap style="font-size:10"> <%=  anticipo.getConductor()               %> - <%=  anticipo.getNombreConductor()         %> </td>
                                                         <td class="bordereporte"                nowrap style="font-size:10"> <%=  anticipo.getPla_owner()               %> - <%=  anticipo.getNombrePropietario()       %></td>
                                                         <td class="bordereporte" align='center' nowrap style="font-size:10"> <%=  anticipo.getSupplier()                %> </td>
                                                         <td class="bordereporte" align='center' nowrap style="font-size:9" > 
                                                             <%if (model.AnticiposPagosTercerosSvc.esAnticipoSemiRepetido(""+anticipo.getId()+"").equals("S")){  %><img width='14' title='semirepetido' style="cursor:hand" src='<%=BASEURL%>/images/alert-icon.jpg'>&nbsp; <%  }//20101013 si es semirepetido se pone color rojo%>
                                                             <%=  anticipo.getPlanilla()                %> </td>

                                                         <td class="bordereporte" align='right'  nowrap style="font-size:10"> <%=  Util.customFormat(anticipo.getVlr() ) %> </td>
                                                         <td class="bordereporte" align='center' nowrap style="font-size:10"> <%=  anticipo.getFecha_anticipo()          %> </td>
                                                         <td class="bordereporte" align='center' nowrap style="font-size:10"> <%=  anticipo.getReanticipo()              %> </td>
                                                         <td class="bordereporte" align='center' nowrap style="font-size:10"> <%=  anticipo.getLiquidacion()             %> </td>

                                                         <!--inicio de 20100728-->
                                                         <td class="bordereporte" align='center' nowrap style="font size:10"> <%=  anticipo.getAsesor()                  %> </td>
                                                         <td class="bordereporte" align='center' nowrap style="font size:10"> <%=  anticipo.getReferenciado()            %> </td>
                                                         <td class="bordereporte" align='center' nowrap style="font size:10"> <%=  anticipo.getUsuario_creacion()        %> </td>
                                                         <!--fin de 20100728-->


                                                         <td class="bordereporte" align='center' nowrap style="font-size:10;  <%=colorAprobado%>"    > <%=  anticipo.getAprobado()                %> </td>
                                                         <td class="bordereporte" align='center' nowrap style="font-size:10;  <%= colorTransferido%>"> <%=  anticipo.getTransferido()             %> </td>


                                                         <td class="bordereporte"  nowrap style="font-size:11">               <%= anticipo.getBanco()            %></td>
                                                         <td class="bordereporte" align='center' nowrap style="font-size:10"> <%= anticipo.getCuenta()           %> </td>
                                                         <td class="bordereporte" align='center' nowrap style="font-size:10"> <%= anticipo.getTipo_cuenta()      %> </td>
                                                         <td class="bordereporte" align='center' nowrap style="font-size:10"> <%= anticipo.getNombre_cuenta()    %> </td>
                                                         <td class="bordereporte" align='center' nowrap style="font-size:10"> <%= anticipo.getNit_cuenta()       %> </td>
                                                         <td class="bordereporte" align='right'  nowrap style="font-size:10"> <%= Util.customFormat( vlrConsig ) %> </td>
                                                         <td class="bordereporte" align='center' nowrap style="font-size:10"> <%= anticipo.getFecha_transferencia()   %> </td>
                                                         <td class="bordereporte" align='center' nowrap style="font-size:10"> <%= anticipo.getTransferencia()    %> </td>


                                                         <td class="bordereporte" align='center' nowrap style="font-size:10"> <%= anticipo.getFecha_migracion()        %> </td>
                                                         <td class="bordereporte" align='center' nowrap style="font-size:10"> <%= anticipo.getFactura_mims()           %> </td>


                                                         <td class="bordereporte" align='right' nowrap style="font-size:10; <%=colorEstTer%> "> <%= Util.customFormat(anticipo.getVlr_mims_tercero() )      %> </td>
                                                         <td class="bordereporte" align='center' nowrap style="font-size:10; <%=colorEstTer%> "> <%= anticipo.getEstado_pago_tercero()    %> </td>
                                                         <td class="bordereporte" align='center' nowrap style="font-size:10; <%=colorEstTer%> "> <%= anticipo.getFecha_pago_tercero()     %> </td>
                                                         <td class="bordereporte" align='center' nowrap style="font-size:10; <%=colorEstTer%> "> <%= anticipo.getCheque_pago_tercero()    %> </td>
                                                         <td class="bordereporte" align='center' nowrap style="font-size:10; <%=colorEstTer%> "> <%= anticipo.getCorrida_pago_tercero()   %> </td>


                                                         <td class="bordereporte" align='right' nowrap style="font-size:10; <%=colorEstPro%> "> <%= Util.customFormat(anticipo.getVlr_mims_propietario())     %> </td>
                                                         <td class="bordereporte" align='center' nowrap style="font-size:10; <%=colorEstPro%> "> <%= anticipo.getEstado_desc_propietario()  %> </td>
                                                         <td class="bordereporte" align='center' nowrap style="font-size:10; <%=colorEstPro%> "> <%= anticipo.getFecha_desc_propietario()   %> </td>
                                                         <td class="bordereporte" align='center' nowrap style="font-size:10; <%=colorEstPro%> "> <%= anticipo.getCheque_desc_propietario()  %> </td>
                                                         <td class="bordereporte" align='center' nowrap style="font-size:10; <%=colorEstPro%> "> <%= anticipo.getCorrida_desc_propietario() %> </td>
                                                         <td class="bordereporte" align='center' nowrap style="font-size:10; <%=colorEstPro%> "> <%= anticipo.getFecha_trans_gasolina()  %> </td>
                                                         <td class="bordereporte" align='center' nowrap style="font-size:10; <%=colorEstPro%> "> <%= anticipo.getUser_trans_gasolina() %> </td>
                                                         <td class="bordereporte" align='center' nowrap style="font-size:10; <%=colorEstPro%> "> <%= anticipo.getNombre_eds() %> </td>


                                               </tr>
                                          <%}%>
                                          <TR class='fila'>

                                                     <td class="bordereporte"                nowrap style="font-size:11" colspan='8'><b> TOTALES </b></td>
                                                     <td class="bordereporte" align='right'  nowrap style="font-size:11"><b> <%=  Util.customFormat( T_vlr ) %></b> </td>
                                                     <td class="bordereporte" align='center' nowrap style="font-size:11" colspan='10'><b> &nbsp </b></td>
													 <th class="bordereporte" align='right'  nowrap style="font-size:11"><b> <%=  Util.customFormat( T_vlrC ) %> </b></th>
													 <th class="bordereporte" align='right'  nowrap style="font-size:11" colspan='4'><b> &nbsp </b></th>
													 <th class="bordereporte" align='right'  nowrap style="font-size:11"><b> <%=  Util.customFormat( T_vlrT ) %> </b></th>
													 <th class="bordereporte" align='right'  nowrap style="font-size:11" colspan='4'><b> &nbsp </b></th>
													 <th class="bordereporte" align='right'  nowrap style="font-size:11"><b> <%=  Util.customFormat( T_vlrDP ) %> </b></th>
													 <th class="bordereporte" align='right'  nowrap style="font-size:11" colspan='4'><b> &nbsp </b></th>
                                  </TR>


                                </table>
                            </td>
                        </tr>


                   </table>
               </td>
            </tr>
     </table>

      <br>

      <img src="<%=BASEURL%>/images/botones/exportarExcel.gif"  height="21"  title='Exportar Excel'  onClick="location.href='<%=CONTROLLER%>?estado=Consulta&accion=AnticiposTerceros&evento=EXCEL'"        onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" style="cursor:hand">
      <img src="<%=BASEURL%>/images/botones/restablecer.gif"    height="21"  title='Regresar'        onClick="location.href='<%=CONTROLLER%>?estado=Consulta&accion=AnticiposTerceros&evento=ACTUALIZAR'"   onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" style="cursor:hand">
      <img src="<%=BASEURL%>/images/botones/regresar.gif"       height="21"  title='Regresar'        onClick="location.href='<%=CONTROLLER%>?estado=Consulta&accion=AnticiposTerceros'"                     onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" style="cursor:hand">
      <img src="<%=BASEURL%>/images/botones/salir.gif"          height="21"  title='Salir'           onClick="window.close();"                                                                              onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" style="cursor:hand">




   <!-- MENSAJE -->
   <% if(msj!=null  &&  !msj.equals("") ){%>
        <br><br>
        <table border="2" align="center">
              <tr>
                <td>
                    <table width="100%"  border="1" align="center"  bordercolor="#F7F5F4" bgcolor="#FFFFFF">
                          <tr>
                                <td width="450" align="center" class="mensajes"><%= msj %></td>
                                <td width="29" background="<%=BASEURL%>/images/cuadronaranja.JPG">&nbsp;</td>
                                <td width="58">&nbsp; </td>
                          </tr>
                     </table>
                </td>
              </tr>
        </table>

    <%}%>

</center>
</div>

<!--inicio de 20101014-->
<%if (semirepetidos!=null && !(semirepetidos.equals("0"))){%>
<script>
    Dialog.closeInfo();
    Dialog.alert('<br><center>Planillas aparentemente repetidas :&nbsp; <%=semirepetidos%> .</center>', {
                    width:300,
                    height:120
                });
</script>
<%}%>
<!--fin de 20101014-->

</body>
</html>