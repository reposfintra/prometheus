<%@page session="true"%> 
<%@page errorPage="/error/ErrorPage.jsp"%>
<%@page import="java.*" %>
<%@page import="java.util.*" %>
<%@page import="com.tsp.util.*"%>
<%@include file="/WEB-INF/InitModel.jsp"%>

<html>
<head>
       <title>Lista de producción</title>
       <link href="<%= BASEURL %>/css/estilostsp.css" rel="stylesheet" type="text/css"> 
       <script language="JavaScript" type="text/javascript" src="<%=BASEURL%>/js/boton.js"></script>
       
       
       <script>
         function send(theForm){
            theForm.submit();
         }
       </script>
       
</head>
<body>


<div id="capaSuperior" style="position:absolute; width:100%; height:100px; z-index:0; left: 0px; top: 0px;">
<jsp:include page="/WEB-INF/toptsp.jsp?encabezado=Reporte Producción"/>
</div>

<div id="capaCentral" style="position:absolute; width:100%; height:85%; z-index:0; left: 0px; top: 100px; overflow: scroll;"> 
<center>

   
 <% String  hoy       = Utility.getHoy("-");
    String  msj       = request.getParameter("msj");%>
 

  <FORM method='post' action="<%= CONTROLLER %>?estado=ReporteContable&accion=Anticipos&evento=BUSCAR" name='formulario'  id='formulario' >
   
    <table width="400" border="2" align="center">
       <tr>
          <td>  
               <table width='100%' align='center' class='tablaInferior'>

                      <tr class="barratitulo">
                        <td colspan='4' >
                           <table cellpadding='0' cellspacing='0' width='100%'>
                             <tr>
                              <td align="left" width='55%' class="subtitulo1">&nbsp;RANGO DE FECHAS</td>
                              <td align="left" width='*'  ><img src="<%=BASEURL%>/images/titulo.gif" width="32" height="20"  height="20" align="left"></td>
                            </tr>
                           </table>
                        </td>
                     </tr>
                     
                     <tr class="fila">
                        <td class='letrafila' width='35%'> Fecha Inicial</td>
                        <td width='*'>
                              <!-- Fecha Inicial -->
                              <input  name='fechaInicio' size="20" readonly="true" class="textbox" value='<%=hoy%>' style='width:50%'> 
                              <a href="javascript:void(0)" onClick="if(self.gfPop)gfPop.fPopCalendar(fechaInicio);return false;" hidefocus>
                              <img name="popcal" align="absmiddle" src="<%=BASEURL%>/js/calendartsp/calbtn.gif" width="16" height="16" border="0" alt="">
                              </a>
                        </td>
                     </tr>

                      <tr class="fila">
                        <td class='letrafila'>Fecha Final </td>
                        <td>

                             <!-- Fecha Final -->                   
                             <input  name='fechaFinal' size="20" readonly="true" class="textbox" value='<%=hoy%>' style='width:50%'> 
                             <a href="javascript:void(0)" onClick="if(self.gfPop)gfPop.fPopCalendar(fechaFinal);return false;" hidefocus>
                              <img name="popcal" align="absmiddle" src="<%=BASEURL%>/js/calendartsp/calbtn.gif" width="16" height="16" border="0" alt="">
                             </a>

                        </td>
                     </tr>
                     
              </table>
         </td>
      </tr>
   </table>   
   
 </FORM>
 
 
 <p> 
     <img src="<%=BASEURL%>/images/botones/aceptar.gif"          height="21"  title='Buscar'   onClick="send(formulario)"      onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" style="cursor:hand">
     <img src="<%=BASEURL%>/images/botones/salir.gif"            height="21"  title='Salir'    onClick="window.close();"       onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" style="cursor:hand">           
 </p>
          
        
 
 
 <!-- MENSAJE -->  
 <% if(msj!=null  &&  !msj.equals("") ){%>
        <br>
        <table border="2" align="center">
              <tr>
                <td>
                    <table width="100%"  border="1" align="center"  bordercolor="#F7F5F4" bgcolor="#FFFFFF">
                          <tr>
                                <td width="450" align="center" class="mensajes"><%= msj %></td>
                                <td width="29" background="<%=BASEURL%>/images/cuadronaranja.JPG">&nbsp;</td>
                                <td width="58">&nbsp; </td>
                          </tr>
                     </table>
                </td>
              </tr>
        </table>  
                
    <%}%>


</div>
<!-- Necesario para los calendarios-->
<iframe width="188" height="166" name="gToday:normal:<%=BASEURL%>/js/calendartsp/agenda.js:gfPop:<%=BASEURL%>/js/calendartsp/plugins.js" id="gToday:normal:<%=BASEURL%>/js/calendartsp/agenda.js:gfPop:<%=BASEURL%>/js/calendartsp/plugins.js" src="<%=BASEURL%>/js/calendartsp/ipopeng.htm" scrolling="no" frameborder="0" style="visibility:visible; z-index:999; position:absolute; left:-500px; top:0px;"></iframe>


</body>
</html>
