<%@page session="true"%> 
<%@page errorPage="/error/ErrorPage.jsp"%>
<%@page import="java.*" %>
<%@page import="java.util.*" %>
<%@page import="com.tsp.util.*"%>
<%@page import="com.tsp.operation.model.beans.*"%>
<%@include file="/WEB-INF/InitModel.jsp"%>

<html>
<head>
       <title>Lista de producción - Detalle</title>
       <link href="<%= BASEURL %>/css/estilostsp.css" rel="stylesheet" type="text/css"> 
       <script language="JavaScript" type="text/javascript" src="<%=BASEURL%>/js/boton.js"></script>
       
       
       <script>
          function send(theForm,evento){
             theForm.action += evento;
             theForm.submit();
          }
       </script>
       
</head>
<body>


<div id="capaSuperior" style="position:absolute; width:100%; height:100px; z-index:0; left: 0px; top: 0px;">
<jsp:include page="/WEB-INF/toptsp.jsp?encabezado=Reporte Producción"/>
</div>

<div id="capaCentral" style="position:absolute; width:100%; height:85%; z-index:0; left: 0px; top: 100px; overflow: scroll;"> 
<center>


   <% List  lista   = model.ReporteContableAnticiposSvc.getListReporte(); %>
   
   <FORM method='post' action="<%= CONTROLLER %>?estado=ReporteContable&accion=Anticipos&evento=" name='formulario'  id='formulario' >
   
    <table width="620" border="2" align="center">
       <tr>
          <td>  
               <table width='100%' align='center' class='tablaInferior'>

                      <tr class="barratitulo">
                        <td colspan='4' >
                           <table cellpadding='0' cellspacing='0' width='100%'>
                             <tr>
                              <td align="left" width='55%' class="subtitulo1">&nbsp;DETALLE</td>
                              <td align="left" width='*'  ><img src="<%=BASEURL%>/images/titulo.gif" width="32" height="20"  height="20" align="left"></td>
                            </tr>
                           </table>
                        </td>
                     </tr>
                     
                     
                      <tr class="fila">
                            <td width='100%' colspan='2'>
                                <table width='100%' border="1" bordercolor="#999999" bgcolor="#F7F5F4" align="center">
                                
                                    <TR  class="tblTitulo"  >  
                                            <TH nowrap style="font size:11; font weight: bold">FECHA          </TH>
                                            <TH nowrap style="font size:11; font weight: bold">AGENCIA        </TH>
                                            <TH nowrap style="font size:11; font weight: bold">CONDUCTOR      </TH>
                                            <TH nowrap style="font size:11; font weight: bold">PROPIETARIO    </TH>
                                            <TH nowrap style="font size:11; font weight: bold">PLACA          </TH>
                                            <TH nowrap style="font size:11; font weight: bold">PLANILLA       </TH>
                                            <TH nowrap style="font size:11; font weight: bold" width='100'>VALOR          </TH> 
                                            <TH nowrap style="font size:11; font weight: bold" width='50'>% DESC         </TH>
                                            <TH nowrap style="font size:11; font weight: bold" width='80'>VLR DESC       </TH>
                                            <TH nowrap style="font size:11; font weight: bold" width='100'>NETO           </TH>
                                            <TH nowrap style="font size:11; font weight: bold" width='80'>COMISION       </TH>
                                            <TH nowrap style="font size:11; font weight: bold" width='100'>VLR CONSIGNAR  </TH>
                                      </TR>  
      
                                      
                                      <% double vlr_anticipo   = 0;
                                         double vlr_descuento  = 0;
                                         double vlr_neto       = 0;
                                         double vlr_comision   = 0;
                                         double vlr_consignar  = 0;

                                          for(int i=0;i<lista.size();i++){
                                            AnticiposTerceros anticipo = (AnticiposTerceros)lista.get(i);
                                            vlr_anticipo   += anticipo.getVlr();
                                            vlr_descuento  += anticipo.getVlrDescuento();
                                            vlr_neto       += anticipo.getVlrNeto();
                                            vlr_comision   += anticipo.getVlrComision();
                                            vlr_consignar  += anticipo.getVlrConsignar();
                                            %>
                                            <TR class='<%= (i%2==0?"filagris":"filaazul") %>' >
                                                  
                                                     <td class="bordereporte" align='center' nowrap style="font size:10"> <%=  anticipo.getFecha_transferencia()      %> </td> 
                                                     <td class="bordereporte"                nowrap style="font size:10"> <%=  anticipo.getNombreAgencia()            %> </td>                                                      
                                                     <td class="bordereporte"                nowrap style="font size:10"> <%=  anticipo.getConductor()                %> - <%=  anticipo.getNombreConductor()         %> </td> 
                                                     <td class="bordereporte"                nowrap style="font size:10"> <%=  anticipo.getPla_owner()                %> - <%=  anticipo.getNombrePropietario()       %></td> 
                                                     <td class="bordereporte" align='center' nowrap style="font size:10"> <%=  anticipo.getSupplier()                 %> </td>
                                                     <td class="bordereporte" align='center' nowrap style="font size:9" > <%=  anticipo.getPlanilla()                 %> </td>                                                     
                                                     <td class="bordereporte" align='right'  nowrap style="font size:10"> <%=  Util.customFormat( anticipo.getVlr() ) %> </td>  
                                                     <td class="bordereporte" align='center' nowrap style="font size:10"> <%=  Util.customFormat( anticipo.getPorcentaje()   ) %> </td>                                                                
                                                     <td class="bordereporte" align='right'  nowrap style="font size:10"> <%=  Util.customFormat( anticipo.getVlrDescuento() ) %> </td>                                                                
                                                     <td class="bordereporte" align='right'  nowrap style="font size:10"> <%=  Util.customFormat( anticipo.getVlrNeto()      ) %> </td>
                                                     <td class="bordereporte" align='right'  nowrap style="font size:10"> <%=  Util.customFormat( anticipo.getVlrComision()  ) %> </td>
                                                     <td class="bordereporte" align='right'  nowrap style="font size:10"> <%=  Util.customFormat( anticipo.getVlrConsignar() ) %> </td> 
                                                       
                                           </TR>
                                        <%}%>
                                      
                                          <TR class='fila'>
                                                  
                                                     <td class="bordereporte"                nowrap style="font size:11" colspan='6'><b> TOTALES </td>                                                     
                                                     <td class="bordereporte" align='right'  nowrap style="font size:11"><b> <%=  Util.customFormat( vlr_anticipo ) %> </td>  
                                                     <td class="bordereporte" align='center' nowrap style="font size:11"><b> &nbsp </td>                                                                
                                                     <td class="bordereporte" align='right'  nowrap style="font size:11"><b> <%=  Util.customFormat( vlr_descuento ) %> </td>                                                                
                                                     <td class="bordereporte" align='right'  nowrap style="font size:11"><b> <%=  Util.customFormat( vlr_neto      ) %> </td>
                                                     <td class="bordereporte" align='right'  nowrap style="font size:11"><b> <%=  Util.customFormat( vlr_comision  ) %> </td>
                                                     <th class="bordereporte" align='right'  nowrap style="font size:11"><b> <%=  Util.customFormat( vlr_consignar ) %> </th> 
                                                       
                                           </TR>
                                      
                                      
                              </table>
                          </td>
                     </tr>
                     
                     
              </table>
         </td>
      </tr>
   </table>   
   
 </FORM>
 
 
 
 <p> 
     <img src="<%=BASEURL%>/images/botones/exportarExcel.gif"    height="21"  title='Exportar Excel'    onClick="send(formulario,'EXCEL')"    onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" style="cursor:hand">           
     <img src="<%=BASEURL%>/images/botones/salir.gif"            height="21"  title='Salir'             onClick="window.close();"             onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" style="cursor:hand">           
 </p>
            


</div>

</body>
</html>
