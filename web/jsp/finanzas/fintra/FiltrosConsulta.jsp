<!--
- Autor      : Ing. Fernell Villacob
- Date       : 23  Agosto 2006
- Copyrigth Notice : Fintravalores S.A. S.A
-->
<%--
-@(#)
--Descripcion : Vista que permite consultar anticipos pagos terceros
--%>


<%-- Declaracion de librerias--%>
<%@page contentType="text/html"%>
<%@page session="true"%>
<%@page errorPage="../error/ErrorPage.jsp"%>
<%@page import="java.util.*"%>
<%@page import="com.tsp.util.*"%>
<%@include file="/WEB-INF/InitModel.jsp"%>


<html>
<head>
        <title>Consulta Anticipos Pagos Terceros</title>
        <link href="<%= BASEURL %>/css/estilostsp.css" rel="stylesheet" type="text/css"> 
	<script language="JavaScript" type="text/javascript" src="<%=BASEURL%>/js/boton.js"></script> 
	<!-- Las siguientes librerias CSS y JS son para el manejo del calendario -->
        <link rel="stylesheet" type="text/css" href="<%=BASEURL%>/css/jCal/jscal2.css" />
        <link rel="stylesheet" type="text/css" href="<%=BASEURL%>/css/jCal/border-radius.css" />
        <script type="text/javascript" src="<%=BASEURL%>/js/jCal/jscal2.js"></script>
        <script type="text/javascript" src="<%=BASEURL%>/js/jCal/lang/es.js"></script>
	
	<script>
	   function send(theForm){
	           var sw   = 0;
	           var cont = 0;
	           for(var i=0;i<theForm.length;i++){
                      var ele = theForm.elements[i];
                      if(ele.type=='checkbox' && ele.checked  ){
                         cont++;
                         if( ele.name=='ckAgencia')         {  if( theForm.Agencia.value        ==''){  alert('Deber� seleccionar la agencia');                  sw=1; theForm.Agencia.focus();         break;  }  }
                         if( ele.name=='ckPropietario')     {  if( theForm.Propietario.value    ==''){  alert('Deber� establecer nit del propietario');          sw=1; theForm.Propietario.focus();     break;  }  } 
                         if( ele.name=='ckPlanilla')        {  if( theForm.Planilla.value       ==''){  alert('Deber� establecer la planilla');                  sw=1; theForm.Planilla.focus();        break;  }  }
                         if( ele.name=='ckPlaca')           {  if( theForm.Placa.value          ==''){  alert('Deber� establecer la placa');                     sw=1; theForm.Placa.focus();           break;  }  }                      
                         if( ele.name=='ckConductor')       {  if( theForm.Conductor.value      ==''){  alert('Deber� establecer nit del Conductor');            sw=1; theForm.Conductor.focus();       break;  }  }
                         if( ele.name=='ckReanticipo')      {  if( theForm.reanticipo.value     ==''){  alert('Deber� establecer clase de registro');            sw=1; theForm.reanticipo.focus();      break;  }  }                          
                         if( ele.name=='ckLiquidacion')     {  if( theForm.Liquidacion.value    ==''){  alert('Deber� establecer n�mero de la Liquidaci�n');     sw=1; theForm.Liquidacion.focus();     break;  }  } 
                         if( ele.name=='ckTransferencia')   {  if( theForm.Transferencia.value  ==''){  alert('Deber� establecer n�mero de la Transferencia');   sw=1; theForm.Transferencia.focus();   break;  }  } 
                         if( ele.name=='ckFactura')         {  if( theForm.Factura.value        ==''){  alert('Deber� establecer n�mero de la factura');         sw=1; theForm.Factura.focus();         break;  }  } 
                         
                         
                      }
                   }
                   if(cont==0){
	              sw=1;
	              alert('Deber� seleccionar filtros de busqueda');
	           }
                   
                   if(sw==0)
	              theForm.submit();
	   }
	</script>
	
</head>

<body onLoad="redimensionar();" onResize="redimensionar();">


<%  String path    = application.getRealPath(model.menuService.getRealUrl(request.getRequestURI()));
    String datos[] = model.menuService.getContenidoMenu(BASEURL,request.getRequestURI(),path);    %>

<div id="capaSuperior" style="position:absolute; width:100%; height:100px; z-index:0; left: 0px; top: 0px;">
<jsp:include page="/WEB-INF/toptsp.jsp?encabezado=Anticipos Pagos Terceros"/>
</div>

<div id="capaCentral" style="position:absolute; width:100%; height:85%; z-index:0; left: 0px; top: 100px; overflow: scroll;"> 
<center>
  

   <% List    agencias  =  model.AnticiposPagosTercerosSvc.getListAgencias(); 
      String  hoy       = Utility.getHoy("-");
      String  msj       = request.getParameter("msj");%>
      

   <form action="<%=CONTROLLER%>?estado=Consulta&accion=AnticiposTerceros&evento=CONSULTAR" method='post' name='formulario' >
 
   <table width="450" border="2" align="center">
       <tr>
          <td>  
               <table width='100%' align='center' class='tablaInferior'>

                  <tr class="barratitulo">
                    <td colspan='2' >
                       <table cellpadding='0' cellspacing='0' width='100%'>
                         <tr>
                          <td align="left" width='65%' class="subtitulo1">&nbsp;FILTROS DE CONSULTA</td>
                          <td align="left" width='*'  ><img src="<%=BASEURL%>/images/titulo.gif" width="32" height="20"  height="20" align="left"><%=datos[0]%></td>
                        </tr>
                       </table>
                    </td>
                 </tr>
                 

                 <tr  class="fila">
                       <td colspan='2' >
                       
                          <table class='tablaInferior' width='100%'>
                          
                               <tr  class="fila">
                                   <td width='7%'  > <input type='checkbox'   name='ckAgencia'>    </td>
                                   <td width='35%' > Agencia                                       </td>
                                   <td width='*'   >            
                                                     <select name='Agencia'>
                                                         <option value=''></option>
                                                         <% for(int i=0;i<agencias.size();i++){
                                                                Hashtable  agencia = (Hashtable)agencias.get(i);%>
                                                                <option value='<%=agencia.get("codigo") %>'>  <%=agencia.get("nombre") %>  </option>
                                                         <%}%>   
                                                     </select>
                                   </td>
                               </tr>
                               
                               <tr  class="fila">
                                   <td             > <input type='checkbox'   name='ckPropietario'>  </td>
                                   <td             > Propietario                                     </td>
                                   <td             > <input type='text' name='Propietario' title='Nit del Propietario' maxlength='15'></td>
                               </tr>
                               
                               <tr  class="fila">
                                   <td             > <input type='checkbox'   name='ckConductor'>  </td>
                                   <td             > Conductor                                      </td>
                                   <td             > <input type='text' name='Conductor' title='Nit del Conductor' maxlength='15'></td>
                               </tr>
                               
                               <tr  class="fila">
                                   <td             > <input type='checkbox'   name='ckPlaca'>      </td>
                                   <td             > Placa                                         </td>
                                   <td             > <input type='text' name='Placa' title='N�mero de Placa' maxlength='6' size='8'>       </td>
                               </tr>
                               
                               <tr  class="fila">
                                   <td             > <input type='checkbox'   name='ckPlanilla'>   </td>
                                   <td             > Planilla                                      </td>
                                   <td             > <input type='text' name='Planilla' title='N�mero de planilla' maxlength='8' size='8'> </td>
                               </tr>
                               
                               
                               
                               <tr  class="fila">
                                   <td             > <input type='checkbox'   name='ckTransferencia'>   </td>
                                   <td             > Transferencia                                 </td>
                                   <td             > <input type='text' name='Transferencia' title='N�mero de la transferencia' maxlength='15'> </td>
                               </tr>
                               
                               
                               <tr  class="fila">
                                   <td             > <input type='checkbox'   name='ckLiquidacion'>   </td>
                                   <td             > Liquidaci�n                                   </td>
                                   <td             > <input type='text' name='Liquidacion' title='N�mero de Liquidaci�n' maxlength='15'> </td>
                               </tr>
                               
                               <tr  class="fila">
                                   <td             > <input type='checkbox'   name='ckFactura'>   </td>
                                   <td             > Factura                                       </td>
                                   <td             > <input type='text' name='Factura' title='N�mero de Factura' maxlength='15'> </td>
                               </tr>
                               
                               
                               <tr  class="fila">
                                   <td             > <input type='checkbox'   name='ckReanticipo'>   </td>
                                   <td             > Reanticipo                                      </td>
                                   <td             > 
                                         <select name='reanticipo'>
                                            <option value=''>
                                            <option value='N'>NO
                                            <option value='S'>SI
                                         </select>
                                   </td>
                               </tr>
                               
                               
                               <tr  class="fila">
                                   <td             > <input type='checkbox'   name='ckFechas'>   </td>
                                   <td             > Fecha Anticipo                              </td>
                                   <td             > 
                                        
                                         <!-- Fecha Inicial -->
                                          <input id="fechaInicio" name='fechaInicio' size="20" readonly="true" class="textbox" value='<%=hoy%>' style='width:35%'> 
                                          <img src="<%=BASEURL%>/js/Calendario/cal.gif" id="imgFechaI" />
                                          <script type="text/javascript">
                                              Calendar.setup({
                                                  inputField : "fechaInicio",
                                                  trigger    : "imgFechaI",
                                                  onSelect   : function() {
                                                      this.hide();
                                                  }
                                              });
                                          </script>
                                          &nbsp
                                          <!-- Fecha Final -->                   
                                          <input type="text" id="fechaFinal" name='fechaFinal' size="20" readonly="true" class="textbox" value='<%=hoy%>' style='width:35%'> 
                                         <img src="<%=BASEURL%>/js/Calendario/cal.gif" id="imgFechaF" />
                                          <script type="text/javascript">
                                              Calendar.setup({
                                                  inputField : "fechaFinal",
                                                  trigger    : "imgFechaF",
                                                  onSelect   : function() {
                                                      this.hide();
                                                  }
                                              });
                                          </script>
                                   </td>
                               </tr>
                               <tr  class="fila">
                                 <td              >
                                 
                                 </td>
                                 <td >Exportar Excel 
                                 <input   name='op_excel' type='checkbox' id="op_excel"></td>
                               </tr>
                               
                          </table>
                       
                       </td>
                 </tr>
                 
                 

           </table>
         </td>
      </tr>
   </table> 
  
   <br>                 
   <img src="<%=BASEURL%>/images/botones/aceptar.gif"    height="21"  title='Consultar'  onclick='send(formulario);'   onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" style="cursor:hand">
   <img src="<%=BASEURL%>/images/botones/salir.gif"      height="21"  title='Salir'      onClick="window.close();"     onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" style="cursor:hand">
   
   
   
   
   
 
   <!-- MENSAJE -->  
   <% if(msj!=null  &&  !msj.equals("") ){%>
        <br><br>
        <table border="2" align="center">
              <tr>
                <td>
                    <table width="100%"  border="1" align="center"  bordercolor="#F7F5F4" bgcolor="#FFFFFF">
                          <tr>
                                <td width="450" align="center" class="mensajes"><%= msj %></td>
                                <td width="29" background="<%=BASEURL%>/images/cuadronaranja.JPG">&nbsp;</td>
                                <td width="58">&nbsp; </td>
                          </tr>
                     </table>
                </td>
              </tr>
        </table>  
                
    <%}%>

   
   
   
   
</div>


<%=datos[1]%>  


</body>
</html>

