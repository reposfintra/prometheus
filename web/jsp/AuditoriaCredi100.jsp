<%-- 
    Document   : AuditoriaCredi100
    Created on : 16/07/2016, 09:45:24 AM
    Author     : egonzalez
--%>

<%@page contentType="text/html" pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
        <link href="./css/popup.css" rel="stylesheet" type="text/css">
        <link type="text/css" rel="stylesheet" href="./css/jquery/jquery-ui/jquery-ui.css" />
        <link type="text/css" rel="stylesheet" href="./css/TransportadorasApi.css " />
        <script type="text/javascript" src="./js/jquery/jquery.jqGrid/js/jquery.min.js"></script>
        <script type="text/javascript" src="./js/jquery-ui-1.8.5.custom.min.js"></script>   
        <script type="text/javascript" src="./js/jquery/jquery.jqGrid-4.7.0/js/grid.locale-es.js"></script>
        <script type="text/javascript" src="./js/jquery/jquery.jqGrid-4.7.0/js/jquery.jqGrid.min.js"></script>
        <script type="text/javascript" src="./js/jquery/jquery.jqGrid-4.7.0/js/jquery.tabletojson.js"></script>    
        <script type="text/javascript" src="./js/AuditoriaCredi100.js"></script> 
        <title>AUDITORIA CREDI 100</title>
    </head>
    <body>
        <div id="capaSuperior" style="position:absolute; width:100%; height:100px; z-index:0; left: 0px; top: 0px;">
            <jsp:include page="/WEB-INF/toptsp.jsp?encabezado=AUDITORIA CREDI 100"/>
        </div>
        <style>
            #gs_entidad{
                width: 273px !important;
            }
            #gs_afiliado{
                width: 218px !important;
            }
        </style>
    <center>
        <div id="tablita" style="top: 170px;width: 1624px;height: 95px;" >
            <div class="ui-dialog-titlebar ui-widget-header ui-helper-clearfix">
                <span class="ui-dialog-title text-center" id="ui-dialog-title-listaGlobal">
                    &nbsp;
                    <label class="titulotablita" ><b>FILTRO DE BUSQUEDA</b></label>
                </span>
            </div>
            <div class="row" style="padding: 5px">
                <div class="col-md-12 ">
                    <table id="tablainterna" style="height: 53px; width: 1614px"  >
                        <tr>
                            <td>
                                <label>Fecha Credito</label>
                            </td>
                            <td>
                                <input type="datetime" id="fecha" name="fecha" style="height: 15px;width: 67px;color: #070708;" readonly>
                            </td>
                            <td>
                                <label style="margin-left: -6px;">-</label>
                            </td>
                            <td>
                                <input type="datetime" id="fechafin" name="fecha" style="height: 15px;width: 67px;color: #070708;margin-left: 4px;" readonly>
                            </td>
                            <td>
                                <label>Unidad de negocio</label>
                            </td>
                            <td>
                                <select id="linea_negocio" name="linea_negocio" style="height: 23px;width: 160px;color: #070708;"></select>
                            </td>
                            <td>
                                <label>Identificacion</label>
                            </td>
                            <td>
                                <input type="text" id="identificacion" name="identificacion" style="height: 15px;width: 88px;color: #070708;" >
                            </td>
                            <td>
                                <label>Numero Solicitud</label>
                            </td>
                            <td>
                                <input type="text" id="num_solicitud" name="num_solicitud" style="height: 15px;width: 67px;color: #070708;" >
                            </td>
                            <td>
                                <label>Estado Solicitud</label>
                            </td>
                            <td>
                                <select id="estado_solicitud" name="estado_solicitud" style="height: 23px;width: 118px;color: #070708;">
                                    <option value=""></option>
                                    <option value="P">ACEPTADO</option>
                                    <option value="A">APROBADO</option>
                                    <option value="V">AVALADO</option>
                                    <option value="D">DESISTIDO</option>
                                    <option value="R">RECHAZADO</option>
                                    <option value="T">TRANSFERIDO</option>
                                </select>
                            </td>
                            <td>
                                <label>Negocio</label>
                            </td>
                            <td>
                                <input type="text" id="cod_neg" name="cod_neg" style="height: 15px;width: 67px;color: #070708;" >
                            </td>
                            <td>
                                <hr style="width: 2px;height: 39px;top: -25px;margin-left: 13px;color: rgba(128, 128, 128, 0.39);" /> 
                            </td>
                            <td>
                                <div style="padding-top: 10px">
                                    <button id="buscar" class="ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only" style="margin-left: 15px;top: -7px;" > 
                                        <span class="ui-button-text">Buscar</span>
                                    </button>  
                                </div>
                            </td>
                            <td>
                                <div style="padding-top: 10px">
                                    <button id="exportar" class="ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only" style="margin-left: -5px;top: -7px;" > 
                                        <span class="ui-button-text">Exportar</span>
                                    </button>  
                                </div>
                            </td>
                            <td>
                                <div style="padding-top: 10px">
                                    <button id="limpiar" class="ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only" style="margin-left: -2px;top: -7px;width: 102px;" > 
                                        <span class="ui-button-text">limpiar fechas</span>
                                    </button>  
                                </div>
                            </td>
                        </tr>
                    </table>
                </div>
            </div>
        </div>
        <div style="position: relative;top: 231px;">
            <table id="tabla_AuditoriaCredi100" ></table>
            <div id="pager"></div>
        </div>
        <div id="info"  class="ventana" >
            <p id="notific">EXITO AL GUARDAR</p>
        </div>
        <div id="divSalidaEx" title="Exportacion" style=" display: block" >
            <p  id="msjEx" style=" display:  none"> Espere un momento por favor...</p>
            <center>
                <img id="imgloadEx" style="position: relative;  top: 7px; display: none " src="./images/cargandoCM.gif"/>
            </center>
            <div id="respEx" style=" display: none"></div>
        </div> 
    </center>
</body>
</html>
