<%-- 
    Document   : generar_documentos_digitales
    Created on : 11/12/2018, 08:39:36 AM
    Author     : mcamargo
--%>

<%@page contentType="text/html" pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
        
        <link href="./css/popup.css" rel="stylesheet" type="text/css">
        <link type="text/css" rel="stylesheet" href="./css/jquery/jquery-ui/jquery-ui.css" />
        <script type="text/javascript" src="./js/jquery/jquery.jqGrid/js/jquery.min.js"></script>
        <script type="text/javascript" src="./js/jquery-ui-1.8.5.custom.min.js"></script>   
        <script type="text/javascript" src="./js/jquery/jquery.jqGrid-4.7.0/js/grid.locale-es.js"></script>
        <script type="text/javascript" src="./js/jquery/jquery.jqGrid-4.7.0/js/jquery.jqGrid.min.js"></script>
        <script type="text/javascript" src="./js/jquery/jquery.jqGrid-4.7.0/js/jquery.tabletojson.js"></script> 
       
        <script type="text/javascript" src="./js/generar_documentos_digitales.js"></script>
         
        <style type="text/css">             
             .hide-close .ui-dialog-titlebar-close { display: none }
         </style>
          <title>Generar documentos digitales</title>
    </head>
    <body>
          
        <div id="capaSuperior" style="position:absolute; width:100%; height:100px; z-index:0; left: 0px; top: 0px;">
            <jsp:include page="/WEB-INF/toptsp.jsp?encabezado=Generar documentos digitales"/>
        </div>
        
        <center>
            <div id="tablita" style="top: 170px;width: 624px;height: 95px;  margin-top: 100px;" >

                <div class="ui-dialog-titlebar ui-widget-header ui-helper-clearfix">
                    <span class="ui-dialog-title text-center" id="ui-dialog-title-listaGlobal">
                        &nbsp;
                        <label  ><b>FILTRO DE BUSQUEDA</b></label>
                    </span>
                </div>

                <div  style="padding: 5px; background: white; border:1px solid #2A88C8">
                    <div class="col-md-12 ">
                        <table id="tablainterna" style="height: 53px; width: 400px"  >

                            <tr style = "text-align: middle;vertical-align: middle">

                                <td style = "text-align: middle;vertical-align: middle">
                                    
                                    <div style = "float: left;valign:middle">
                                     <label style="font-size: 14px;">Codigo Negocio:</label>
                                     <input type="text" id="negocio"  name="negocio" maxlength="10" style="height: 20px;width: 100px;color: #070708;" >
                                                                    
                                    </div> 
                                    
                                    <div style = "float: left;" >
                                        <button id="generar" class="ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only" style="margin-left: 15px;" > 
                                            <span class="ui-button-text">Buscar</span>
                                        </button>  
                                    </div>
                                </td> 
                            </tr>
                        </table>
                    </div>
                </div>
            </div>
            
            <div  style="margin-top: 20px; " >
                <table id="tabla_info_negocio" ></table>
                <div id="pager"></div>
            </div>

            <div id="info"  class="ventana" >
                <p id="notific"></p>
            </div>
            
            <div id="dialogLoading" style="display:none;">
                <p  style="font-size: 12px;text-align:justify;" id="msj2">Texto </p> <br/>
                <center>
                    <img src="./images/cargandoCM.gif"/>
                </center>
            </div>
            <div id="dialogMsj" title="Mensaje" style="display:none; .hide-close .ui-dialog-titlebar-close { display: none }">
                <p style="font-size: 12px;text-align:justify;" id="msj" > Texto </p>
            </div>
            
            
            <div id="divSalidaEx" title="Exportacion" style=" display: block" >
                <p  id="msjEx" style=" display:  none"> Espere un momento por favor...</p>
                <center>
                    <img id="imgloadEx" style="position: relative;  top: 7px; display: none " src="./images/cargandoCM.gif"/>
                </center>
               
            </div> 



        </center>
    </body>
</html>
