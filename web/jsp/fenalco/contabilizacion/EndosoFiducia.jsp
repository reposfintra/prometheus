<%-- 
    Document   : EndosoFiduciasFacturas
    Created on : 19/02/2016, 09:52:15 AM
    Author     : hcuello
--%>
<%@page session="true"%>
<%@page contentType="text/html" pageEncoding="ISO-8859-1"%>
<%@ include file="/WEB-INF/InitModel.jsp"%>
<%@page errorPage="/error/ErrorPage.jsp" %>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
        <title>Endosar Cartera</title>

        <link type="text/css" rel="stylesheet" href="./css/jquery/jquery-ui/jquery-ui.css" />
        <link href="./css/popup.css" rel="stylesheet" type="text/css">
        <link href="./css/endoso_fiducia.css" rel="stylesheet" type="text/css">
        <link type="text/css" rel="stylesheet" href="<%=BASEURL%>/css/style_azul.css" >
        
        <script type="text/javascript" src="./js/jquery/jquery.jqGrid/js/jquery.min.js"></script>
        <script type="text/javascript" src="./js/jquery-ui-1.8.5.custom.min.js"></script>
        <script type="text/javascript" src="./js/jquery/jquery.jqGrid-4.7.0/js/grid.locale-es.js"></script>
        <script type="text/javascript" src="./js/jquery/jquery.jqGrid-4.7.0/js/jquery.jqGrid.min.js"></script>

        <script type="text/javascript" src="./js/endoso_fiducia.js"></script>
    </head>
    <body>
        <div id="capaSuperior" style="position:absolute; width:100%; height:100px; z-index:0; left: 0px; top: 0px;">
            <jsp:include page="/WEB-INF/toptsp.jsp?encabezado=ENDOSO FIDUCIA CARTERA"/>
        </div>
        
        <!-- <div id="capaCentral" style="position:absolute; width:80%; height:80%; z-index:0; left: 170px; top: 100px;"> -->
        <div id="capaCentral" style="position:absolute; width:100%; height:80%; z-index:0; left: 0px; top: 100px;">    
            <br>
            
                <table width="100%" border="0" cellpadding="0" cellspacing="1" class="labels">

                    <tr>
                        <td width="75%">
                            <fieldset>
                            <legend class="labels">FILTRO DE FACTURAS</legend>

                                <table border="0" align="center" cellpadding="1" cellspacing="1" class="labels">
                                    <tr>
                                        <div id="busqueda" style="float: left"> 
                                            
                                            <td width="65%">
                                                <label>L�nea Negocio</label><span style="color: red"> *</span> &nbsp;
                                                <select id="unidad_negocio" name="unidad_negocio" onchange="onChangeLineasNegocio()">
                                                    <option value="">-Seleccione-</option>
                                                    <option value="MIROCREDITO">MICROCREDITO</option>
                                                    <option value="FENALCO_ATL">FENALCO ATLANTICO</option>
                                                    <option value="FENALCO_BOL">FENALCO BOLIVAR</option>
                                                    <option value="FINTRA">FINTRA</option>
                                                </select>  

                                                <label>Und Negocio</label><span style="color: red"> *</span> &nbsp;
                                                <select id="unds_negocio" onchange="FiltroInverso()">  </select> 

                                                <label>Cartera en</label><span style="color: red"> *</span> &nbsp;
                                                <select id="cartera_en" onchange="FiltroInverso();ValidarOnChange()">  </select> 

                                                <label>C�dula</label><span style="color: red"> *</span> &nbsp;
                                                <!-- <input type="text" id="fecha_inicio" class="inpt" readonly  style="width: 90px;"> -->
                                                <input type="number" min="1" max="11" id="cedula_cliente" class="solo-numero" style="width: 50px;" onclick="BloquearBusqueda()">

                                                <label>Negocio</label><span style="color: red"> *</span> &nbsp;
                                                <input type="text" id="Negocio" class="inpt" style="width: 50px;" onclick="BloquearBusqueda()">
                                            </td>
                                            
                                            <td width="10%">
                                                <label>Abonos(s/n)</label><span style="color: red"> *</span> &nbsp;
                                                <div class="onoffswitch" style="left:-25px;top:5px">
                                                     <input type="checkbox" id="estado" name="estado"  class="onoffswitch-checkbox" checked>
                                                     <label class="onoffswitch-label" for="estado">
                                                         <span class="onoffswitch-inner"></span>
                                                         <span class="onoffswitch-switch"></span>
                                                     </label>
                                                 </div>                                        
                                            </td>

                                            <td width="2%">    
                                                <button id="buscar" class="ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only" style="margin-left: 10px;margin-top: -3px;"
                                                        role="button" aria-disabled="false">
                                                    <span class="ui-button-text">Buscar</span>
                                                </button>
                                            </td>    
                                        </tr>    
                                    </div> 
                                </table>
                            </fieldset>
                        </td>
                        
                        <td width="9%">
                            <fieldset>

                                <legend class="labels">FILTRO AVANZADO</legend>

                                <table border="0" align="left" cellpadding="1" cellspacing="1" class="labels">
                                    <tr>
                                        <td>
                                            <label>Seleccione Su Archivo</label>
                                            <div id="busqueda" style="float: right">
                                                <!-- <button id="buscar" class="ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only del_disabled" style="margin-left: 10px;margin-top: -3px;border: solid 1px #fff;box-shadow: inset 1px 1px 2px 0 #707070;transition: box-shadow 0.3s;background: #dddddd;" -->
                                                <!-- class="botton_disabled" --> 
                                                <input type="file" id="archivo"  name="archivo">
                                                <button style="font-weight: bold; font-size: 12px;" id="aceptar_endoso" class="ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only">
                                                    <span class="ui-button-text">Aceptar</span>
                                                </button> 
                                            </div>                                             
                                        </td>    
                                    </tr>    

                                </table>
                            </fieldset>
                        </td>

                        <td width="15%">
                            <fieldset>

                                <legend class="labels">ENDOSAR A</legend>

                                <table border="0" align="left" cellpadding="1" cellspacing="1" class="labels">
                                    <tr>
                                        <td>
                                            
                                            <div id="busqueda" style="float: right">

                                                <div class="radio-toolbar" style=" float:left;" >
                                                    <select id="endosar_a" class="combo_150px_disabled">  </select> 
                                                </div>
                                                <!-- <button id="buscar" class="ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only del_disabled" style="margin-left: 10px;margin-top: -3px;border: solid 1px #fff;box-shadow: inset 1px 1px 2px 0 #707070;transition: box-shadow 0.3s;background: #dddddd;" -->
                                                <!-- class="botton_disabled" --> 
                                                <button id="btn_endosar" style="margin-left: 10px;margin-top: -3px;"
                                                        role="button" onclick="GoEndosoFacturas()">
                                                    <span class="ui-button-text">Endosar</span>
                                                </button>
                                            </div>                                             
                                        </td>    
                                    </tr>    

                                </table>
                            </fieldset>
                        </td>                        

                    </tr>
                    
                    <tr>
                        <td width="100%" colspan="2">
                            <fieldset>

                                <legend class="labels">RESULTADOS</legend>

                                <table border="0" align="center" cellpadding="1" cellspacing="1" class="labels">
                                    <tr>
                                        <td>
                                            <div style="position:relative;height:600px">
                                                <center>

                                                    <table id="tbl_facturas" align="center" ></table>
                                                    <div id="page_facturas"></div>

                                                </center>
                                            </div>                                            
                                        </td>    
                                    </tr>    
                                </table>
                                
                            </fieldset>
                        </td>                    
                </table>    
            
                <div id="dialogo" title="Mensaje"><p id="msj"></p></div>

                <div id="dialogLoading"  class="ventana" >
                    <p  id="msj2">texto </p> <br/>
                    <center><img src="<%=BASEURL%>/images/cargandoCM.gif"/></center>
                </div>

                <div id="divSalidaEx" title="Exportacion" style=" display: block" >
                    <p  id="msjEx" style=" display:  none"> Espere un momento por favor...</p>
                    <center>
                        <img id="imgloadEx" style="position: relative;  top: 7px; display: none " src="./images/cargandoCM.gif"/>
                    </center>
                    <div id="respEx" style=" display: none"></div>
                </div> 
                <div id="info"  class="ventana" >
                    <p id="notific">EXITO AL GUARDAR</p>
                </div>
            
            

        </div>
        
    </body>
</html>
