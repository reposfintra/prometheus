<!--
- Autor : Ing. Iris Vargas
- Date  : 13 de noviembre de 2012
-->

<%@page contentType="text/html"%>
<%@page session="true"%>
<%@page errorPage="/error/ErrorPage.jsp"%>
<%@page import="java.util.*"%> 
<%@page import="com.tsp.operation.model.*"%>
<%@page import="com.tsp.util.*"%>
<%@page import="com.tsp.operation.model.beans.*, java.util.*"%>
<%@include file="/WEB-INF/InitModel.jsp"%>
<%@taglib uri="http://jsptags.com/tags/navigation/pager" prefix="pg" %>

<html>
    <head>
        <title>Listado de Ingresos</title>
        <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
        <link href="<%= BASEURL%>/css/estilostsp.css" rel="stylesheet" type="text/css">
        <link href="../../../../css/estilostsp.css" rel="stylesheet" type="text/css">
        <script type='text/javascript' src="<%= BASEURL%>/js/reporte.js"></script>
        <script type='text/javascript' src="<%= BASEURL%>/js/boton.js"></script>

        <script type='text/javascript'>
            function procesar(){
                var elem    = document.getElementsByName('ingresos');
               var sw=false;
    
                for (i = 0  ; i < elem.length ; i++){
                    if (elem[i].checked){
                        sw=true;
                    }
                }
        
                if (!sw){
                    alert ('Seleccione al menos un ingreso');
                    return false;
                }
    
                   form1.submit();
                
            }

        </script>

    </head>
    <body onLoad="redimensionar();" onresize="redimensionar()">
        <div id="capaSuperior" style="position:absolute; width:100%; height:100px; z-index:0; left: 0px; top: 0px;">
            <jsp:include page="/WEB-INF/toptsp.jsp?encabezado=Listado de Ingresos"/>
        </div>
        <div id="capaCentral" style="position:absolute; width:100%; height:87%; z-index:0; left: 0px; top: 100px; overflow: scroll;">

            <%
                        String Mensaje = (request.getParameter("Mensaje") != null) ? request.getParameter("Mensaje") : "";

                        LinkedList lista = model.ingresoService.getListadoingreso();
                        Ingreso ing;

                        if (lista.size() > 0) {
            %>
            <p>
            <table width="1200" border="2" align="center">
                <tr>
                    <td width="100%">
                        <table width="100%" align="center">
                            <tr>
                                <td width="373" class="subtitulo1">&nbsp;Datos Ingresos</td>
                                <td width="427" class="barratitulo"><img src="<%=BASEURL%>/images/titulo.gif" width="32" height="20"></td>
                            </tr>
                        </table>
                               <form ACTION="<%=CONTROLLER%>?estado=Negocios&accion=Update&op=6" METHOD='post' id='form1' name='form1'>
                        <table width="99%" border="1" align="center" bordercolor="#999999" bgcolor="#F7F5F4">
                            <tr class="tblTitulo">
                                <td width="1%" height="46" align="center"></td>
                                <td width="3%" align="center">Tipo</td>
                                <td width="5%" align="center">Numero</td>
                                <td width="14%"  align="center">Cliente</td>
                                <td width="7%" align="center">Fecha Ingreso</td>
                                <td width="8%"  align="center">Fecha Consignación </td>
                                <td width="11%"  align="center">Consignado </td>
                                <td width="11%"  align="center">Saldo </td>
                                <td width="10%"  align="center">Banco </td>
                                <td width="11%"  align="center">Suscursal </td>
                                <td width="14%"  align="center">Cuenta </td>
                                <td width="6%"  align="center">Estado</td>
                            </tr>


                            <%
                                        for (int i = 0; i < lista.size(); i++) {
                                            ing = (Ingreso) lista.get(i);
                                            String vec[] = ing.getBank_account_no().split("/");
                                            String suc = "", cuen = "";
                                            if (vec != null && vec.length > 1) {
                                                suc = vec[0];
                                                cuen = vec[1];
                        }%>

                            <tr class="<%=(i % 2 == 0) ? "filagris" : "filaazul"%>"   style="cursor:hand">
                                <td class="bordereporte" title='Seleccione para impresion'><input type='checkbox' name='ingresos' value='<%=ing.getDstrct()%>;<%=ing.getTipo_documento()%>;<%=ing.getNum_ingreso()%>'></td>
                                <td width="3%" class="bordereporte" nowrap ><%=ing.getDestipo()%></td>
                                <td width="5%" class="bordereporte"><%=ing.getNum_ingreso()%></td>
                                <td width="14%" class="bordereporte" align="center"><%=ing.getNomCliente()%></td>
                                <td width="7%" class="bordereporte" align="center" ><%=ing.getFecha_ingreso()%></td>
                                <td width="8%" class="bordereporte" align="center" ><%=ing.getFecha_consignacion()%></td>
                                <td width="11%" class="bordereporte" align="right" ><%=(ing.getCodmoneda().equals("DOL")) ? UtilFinanzas.customFormat2(ing.getVlr_ingreso_me()) : UtilFinanzas.customFormat(ing.getVlr_ingreso_me())%> <%=ing.getCodmoneda()%></td>
                                <td width="11%" class="bordereporte" align="right" ><%=(ing.getCodmoneda().equals("DOL")) ? UtilFinanzas.customFormat2(ing.getVlr_saldo_ing()) : UtilFinanzas.customFormat(ing.getVlr_saldo_ing())%> <%=ing.getCodmoneda()%></td>
                                <td width="10%" class="bordereporte" align="center" ><%=!ing.getBranch_code().equals("") ? ing.getBranch_code() : "&nbsp;"%></td>
                                <td width="11%" class="bordereporte" align="center" ><%=!suc.equals("") ? suc : "&nbsp;"%></td>
                                <td width="14%" class="bordereporte" align="center" ><%=!cuen.equals("") ? cuen : "&nbsp;"%></td>
                                <td width="6%" class="bordereporte" align="center" ><%=(ing.getReg_status().equals("A")) ? "ANULADO" : ing.getVlr_saldo_ing() == 0 ? "Detallado Total" : ing.getVlr_saldo_ing() == ing.getVlr_ingreso_me() ? "Pendiente Por Detallar" : "Detallado Parcial"%></td>
                            </tr>

                            <%}%>
                            <tr class="bordereporte">
                                <td td height="20" colspan="12" nowrap align="center">


                                </td>
                            </tr>

                        </table></form></td>
                </tr>
            </table>
            <p>
                <%} else {%>
            </p>
            <table border="2" align="center">
                <tr>
                    <td><table width="410" height="40" border="1" align="center"  bordercolor="#F7F5F4" bgcolor="#FFFFFF">
                            <tr>
                                <td width="282" align="center" class="mensajes">Su b&uacute;squeda no arroj&oacute; resultados!</td>
                                <td width="28" background="<%=BASEURL%>/images/cuadronaranja.JPG">&nbsp;</td>
                                <td width="78">&nbsp;</td>
                            </tr>
                        </table></td>
                </tr>
            </table>
            <p>&nbsp; </p>
            <%}%>

            <% if (!Mensaje.equals("")) {%>
            <table border="2" align="center">
                <tr>
                    <td><table width="410" height="40" border="1" align="center"  bordercolor="#F7F5F4" bgcolor="#FFFFFF">
                            <tr>
                                <td width="320" align="center" class="mensajes"><%=Mensaje%></td>
                                <td width="28" background="<%=BASEURL%>/images/cuadronaranja.JPG">&nbsp;</td>
                                <td width="78">&nbsp;</td>
                            </tr>
                        </table></td>
                </tr>
            </table>
            <p>&nbsp; </p>
            <%}%>
            <table width="1200" border="0" align="center">
                <tr>
                    <td width="600" >  <img alt="" src = "<%=BASEURL%>/images/botones/aceptar.gif" style="cursor:pointer;" onclick="procesar();" name= "imgaceptar" id="imgaceptar"  onMouseOver="botonOver(this);" onMouseOut="botonOut(this);">
                   <img src="<%=BASEURL%>/images/botones/regresar.gif"  name="imgaceptar" onClick="window.location='<%=CONTROLLER%>?estado=Menu&accion=Cargar&carpeta=/jsp/fenalco/contabilizacion&pagina=devoluciones.jsp&marco=no'" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" style="cursor:hand"></td>

                </tr>
            </table>
        </div>
    </body>

</html>
