<%@page import="com.tsp.operation.model.services.GestionConveniosService"%>
<!--
- Autor : Ing. Iris Vargas
- Date  : 10 de noviembre de 2012
-->

<%@page session="true"%>
<%@page import="java.util.*" %>
<%@page import="com.tsp.operation.model.*"%>
<%@include file="/WEB-INF/InitModel.jsp"%>
<%@page import="com.tsp.operation.model.beans.*"%>
<%@page import="com.tsp.util.*"%>
<%@ taglib uri="http://jakarta.apache.org/taglibs/input-0.90" prefix="input" %>
<html>
    <head>
        <title>Devoluciones</title>
        <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
        <link href="<%=BASEURL%>/css/estilostsp.css" rel="stylesheet" type="text/css">
        <link href="../../../../css/estilostsp.css" rel="stylesheet" type="text/css">
        <script language="JavaScript" type="text/javascript" src="<%=BASEURL%>/js/validar.js"></script>
        <script type='text/javascript' src="<%= BASEURL%>/js/boton.js"></script>

        <script type='text/javascript' >
            function validarBusqueda(){
                if(form1.numero.value=='' && form1.factura.value==''){
                    if( form1.fechaInicio.value == '' || form1.fechaFinal.value == ''){
			alert("Debe Ingresar un rango de fechas");
			return (false);
                    }
                    else{
			if(form1.fechaInicio.value > form1.fechaFinal.value ){
                            alert("La fecha final debe ser mayor que la inicial");
                            return (false);
                        }
                    }
                }

                form1.submit();
            }

        </script>
    </head>
    <body onResize="redimensionar();" onLoad='redimensionar();' >

        <%String path = application.getRealPath(model.menuService.getRealUrl(request.getRequestURI()));
                    Usuario usuario = (Usuario) session.getAttribute("Usuario");
                    GestionConveniosService gcserv = new GestionConveniosService(usuario.getBd());
                    ArrayList<String> listacad = gcserv.buscarBancos("nombre", "");
                    String res = (request.getParameter("msg") != null) ? request.getParameter("msg") : "";%>

        <div id="capaSuperior" style="position:absolute; width:100%; height:100px; z-index:0; left: 0px; top: 0px;">
            <jsp:include page="/WEB-INF/toptsp.jsp?encabezado=Devoluciones"/>
        </div>
        <div id="capaCentral" style="position:absolute; width:100%; height:87%; z-index:0; left: 0px; top: 100px; overflow: scroll;">
            <form ACTION="<%=CONTROLLER%>?estado=Negocios&accion=Update&op=18" METHOD='post' id='form1' name='form1'>
                <table width="500" border="2" align="center">
                    <tr>
                        <td><table width="100%"  border="0">
                                <tr>
                                    <td width="50%" class="subtitulo1">&nbsp;Buscar Ingreso</td>
                                    <td width="50%" class="barratitulo"><img src="<%=BASEURL%>/images/titulo.gif" width="32" height="20" align="left"></td>
                                </tr>
                            </table>

                            <table width="100%" class="tablaInferior">

                                <tr class="fila">
                                    <td width="121">            Nro. Ingreso </td>
                                    <td><input  class='textbox' type='text' name='numero' size='15' maxlength='11' onKeyPress="soloAlfa(event,'decNO');"   >
                                    </td>
                                    <td width="107">Nro. Factura</td>
                                    <td width="96"><input  class='textbox' type='text' name='factura' size='15' maxlength='11' onKeyPress="soloAlfa(event,'decNO');"   ></td>
                                </tr>
                                <tr class="fila">
                                    <td width="121">            Tipo</td>
                                    <td colspan="3"><select name="tipodoc" class="textbox" id="select" style="width:90% ">
                                            <option value="">Seleccione un items</option>
                                            <% model.tablaGenService.buscarTipoIngreso();
                                                        LinkedList tbltipo = model.tablaGenService.getTipoIngresos();
                                                        for (int i = 0; i < tbltipo.size(); i++) {
                                                            TablaGen reg = (TablaGen) tbltipo.get(i);%>
                                            <option value="<%=reg.getTable_code()%>"><%=reg.getDescripcion()%></option>
                                            <%}%>
                                        </select></td>
                                </tr>
                                <tr  class="fila">
                                    <td>
                                        Banco</td>
                                    <td colspan="3">                                     <select id="banco_docs" name="banco_docs" style="width: 20em;">
                                            <option value="">...</option>
                                            <%
                                                        if (listacad != null && listacad.size() > 0) {
                                                            String[] splitcad = null;
                                                            for (int i = 0; i < listacad.size(); i++) {
                                                                splitcad = (listacad.get(i)).split(";_;");
                                            %>
                                            <option value="<%= splitcad[0]%>" ><%= splitcad[1]%></option>
                                            <%
                                                            }
                                                        }
                                            %>
                                        </select>

                                    </td></tr>

                                <tr class="fila">
                                    <td>            Fecha</td>
                                    <td><input  name='fechaInicio' size="11" readonly="true" class="textbox" >
                                        <a href="javascript:void(0);"  onFocus="if(self.gfPop)gfPop.fPopCalendar(document.form1.fechaInicio);" hidefocus><img alt="calendario" src="<%=BASEURL%>/images/cal.gif" width="16" height="16" align="absmiddle"></a></td>
                                    <td colspan="2"><input  name='fechaFinal' size="11" readonly="true" class="textbox" >
                                        <a href="javascript:void(0);"  onFocus="if(self.gfPop)gfPop.fPopCalendar(document.form1.fechaFinal);" hidefocus><img alt="calendario" src="<%=BASEURL%>/images/cal.gif" width="16" height="16" align="absmiddle"></a>
                                    </td>
                                </tr>
                                <input type="hidden" id="evento" name="evento" value="">
                            </table></td>
                    </tr>
                </table>
                <br>
                <div align="center"><img src="<%=BASEURL%>/images/botones/buscar.gif"  name="imgbuscar" onClick="validarBusqueda();" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" style="cursor:hand">&nbsp;<img src="<%=BASEURL%>/images/botones/salir.gif"  name="imgsalir"  onMouseOver="botonOver(this);" onClick="parent.close();" onMouseOut="botonOut(this);" style="cursor:hand"></div>
                <br>
                <%if (!res.equals("")) {%>
                <br>
                <table border="2" align="center">
                    <tr>
                        <td><table width="100%"  border="1" align="center"  bordercolor="#F7F5F4" bgcolor="#FFFFFF">
                                <tr>
                                    <td width="300" align="center" class="mensajes"><%=res%></td>
                                    <td width="29" background="<%=BASEURL%>/images/cuadronaranja.JPG">&nbsp;</td>
                                    <td width="58">&nbsp;</td>
                                </tr>
                            </table></td>
                    </tr>
                </table>
                <%}%>

            </form>
        </div>
        <iframe width=188 height=166 name="gToday:normal:agenda.js:gfPop:plugins.js" id="gToday:normal:agenda.js:gfPop:plugins.js" src="<%=BASEURL%>/js/Calendario/ipopeng.htm" scrolling="no" frameborder="0" style="visibility:visible; z-index:999; position:absolute; left:-500px; top:0px;"> </iframe>
    </body>
</html>