<!--
    Document   : AnalisisFacturaDetEndoso.jsp
    Created on : 8/07/2011, 08:29:24 AM
    Author     : Ing. Iris Vargas
-->
<%--
-@(#)
--Descripcion : Pagina JSP, que permite escoger las facturas que seran endozadas a Fenalco.
--%>
<%@page contentType="text/html"%>
<%@page session="true"%>
<%@page import="java.util.*"%>
<%@page import="java.text.*"%>
<%@page import="com.tsp.operation.model.*"%>
<%@page import="com.tsp.operation.model.beans.*"%>
<%@page import="com.tsp.operation.controller.*"%>
<%@page import="com.tsp.util.*"%>
<%@page import="com.tsp.operation.model.services.GestionConveniosService"%>
<%@page errorPage="/error/ErrorPage.jsp"%>
<%@include file="/WEB-INF/InitModel.jsp"%>
<%@ taglib uri="http://jakarta.apache.org/taglibs/input-0.90" prefix="input" %>
<%
            String path = application.getRealPath(model.menuService.getRealUrl(request.getRequestURI()));
            String datos[] = model.menuService.getContenidoMenu(BASEURL, request.getRequestURI(), path);
%>
<html>
    <head>
        <title>Envio de Facturas a Terceros</title>
        <link href="<%= BASEURL%>/css/estilostsp.css" rel='stylesheet'>
        <link href="/css/estilostsp.css" rel="stylesheet" type="text/css">
        <script type='text/javascript' src="<%= BASEURL%>/js/boton.js"></script>
        <script type='text/javascript' src="<%= BASEURL%>/js/fxHeader.js"></script>
        <script type="text/javascript">
            function validar( theForm ){
                var sw = 0;
                var cont=0;

                if( theForm.facturasC==null || theForm.facturasC.length==0){
                    alert('Deber� seleccionar al menos una factura');
                    sw=1;
                }
                 if( theForm.cliente.value ==''){
                    alert('Deber� seleccionar un cliente.');
                    sw=1;
                    theForm.cliente.focus();
                }
                if( theForm.fecini.value ==''){
                    alert('Deber� establecer la fecha de factura');
                    sw=1;
                    theForm.fecini.focus();
                }
                if( theForm.fecfin.value ==''){
                    alert('Deber� establecer la fecha de vencimiento');
                    sw=1;
                    theForm.fecfin.focus();
                }
                if ( theForm.fecini.value != "" && theForm.fecfin.value != "" ){
                    var fecha1 = theForm.fecini.value.replace( /-|:| /gi, '' );
                    var fecha2 = theForm.fecfin.value.replace( /-|:| /gi, '' );
                    var fech1 = parseFloat( fecha1 );
                    var fech2 = parseFloat( fecha2 );
                    if( fecha2 < fecha1 ){
                        alert( 'La fecha final debe ser mayor que la fecha inicial!' );
                        sw=1;
                    }
                }
                if(sw==0) theForm.submit();
            }
        </script>
    </head>
    <body onResize="redimensionar()" onload = 'redimensionar()'>
        <div id="capaSuperior" style="position:absolute; width:100%; height:100px; z-index:0; left: 0px; top: 0px;">
            <jsp:include page="/toptsp.jsp?encabezado=Envio de Facturas a Terceros"/>
        </div>
        <div id="capaCentral" style="position:absolute; width:100%; height:87%; z-index:0; left: 0px; top: 100px; overflow: scroll;">
            <%
                        Vector vec = (Vector) request.getAttribute("det");
                        if (vec.size() > 0) {
            %>            
            <table border="2" align="center" width="100%">
                <tr class="fila">
                    <td height="450" >
                        <form id="f1" name="f1" action='<%=CONTROLLER%>?estado=Negocios&accion=Update&op=7' method="post">
                            <table width="50%" border="1" align="center"  class="tablaInferior">                               
                                <%  String tercero = (request.getParameter("tercero") != null) ? request.getParameter("tercero") : ""; 
                                    GestionConveniosService gestion = new GestionConveniosService();
                                    String nomter = gestion.getEntidadRedescuentoNom(tercero);
            
                                %>
                                <tr class="fila">
                                    <td>Avalista</td>
                                    <td valign="middle" colspan="3">
                                        <label><%=nomter%></label>
                                        <input hidden="hidden" class="textbox" id="cliente" readonly name ="cliente" value="<%=tercero%>">
                                    </td>
                                </tr> 

                                <tr class="fila">
                                    <td width="17%" > Fecha de factura </td>
                                    <td valign="middle">
                                        <input name="fecini" type="text" class="textbox" id="fecini" size="10" readonly   value='<%=com.tsp.util.Util.AnoActual() + "-" + com.tsp.util.Util.getFechaActual_String(3) + "-" + com.tsp.util.Util.getFechaActual_String(5)%>'>
                                        <img alt="calendario" src="<%=BASEURL%>/images/cal.gif" width="16" height="16" align="absmiddle" style="cursor:pointer;" onClick="if(self.gfPop)gfPop.fPopCalendar(document.getElementById('fecini'));return false;">
                                        <img src="<%= BASEURL%>/images/botones/iconos/obligatorio.gif" width="10" height="10">
                                    </td>
                                    <td width="17%" >Fecha de vencimiento </td>
                                    <td valign="middle">
                                        <input name="fecfin" type="text" class="textbox" id="fecfin" size="10" readonly value='<%=com.tsp.util.Util.AnoActual() + "-" + com.tsp.util.Util.getFechaActual_String(3) + "-" + com.tsp.util.Util.getFechaActual_String(5)%>'>
                                        <img alt="calendario" src="<%=BASEURL%>/images/cal.gif" width="16" height="16" align="absmiddle" style="cursor:pointer;" onClick="if(self.gfPop)gfPop.fPopCalendar(document.getElementById('fecfin'));return false;">
                                        <img src="<%= BASEURL%>/images/botones/iconos/obligatorio.gif" width="10" height="10">
                                    </td>
                                </tr>

                            </table>
                            
                            <table width="98%" border="1" id="t1" bordercolor="#999999" bgcolor="#F7F5F4" align="left">
                                
                                <tr class="tblTitulo" align="center">
                                    <td width="20" nowrap></td>
                                    <td width="100" nowrap><div align="center">NEGOCIO<br>ASOCIADO</div></td>
                                    <td width="100" nowrap><div align="center">FACTURA</div></td>
                                    <td width="100" nowrap><div align="center">CONSECUTIVO</div></td>
                                    <td width="80" nowrap><div align="center">CODIGO CLI</div></td>
                                    <td width="80" nowrap><div align="center">CC CLI</div></td>
                                    <td width="250" nowrap><div align="center">CLIENTE</div></td>
                                    <td width="250" nowrap><div align="center">AFILIADO</div></td>
                                    <td width="80" nowrap><div align="center">FECHA FACTURA </div></td>
                                    <td width="80" nowrap><div align="center">FECHA VENCIMIENTO</div></td>
                                    <td width="80" nowrap><div align="center">No AVAL</div></td>
                                    <td width="130" nowrap><div align="center">VALOR FACTURA </div></td>
                                    <td width="130" nowrap><div align="center">VALOR SALDO FACTURA </div></td>
                                </tr>
                                
                                <%
                                                            for (int i = 0; i < vec.size(); i++) {
                                                                RepGral obj = (RepGral) vec.elementAt(i);
                                                                if (obj.getCodcli() != null) {
                                %>
                                <tr class="<%=(i % 2 == 0) ? "filagris" : "filaazul"%>">
                                    <td class="bordereporte"  width="20" nowrap>
                                           <input type="checkbox"  name="facturasC" id="<%= obj.getNegocio()%>" value="<%= obj.getFactura() + ";" + obj.getDocumento()%>"></td>
                                    <td class="bordereporte" nowrap><%= obj.getNegocio()%></td>
                                    <td class="bordereporte"  nowrap><%= obj.getFactura()%></td>
                                    <td class="bordereporte"  nowrap><%= obj.getDocu()%></td>
                                    <td class="bordereporte"   nowrap><%= obj.getCodcli()%></td>
                                    <td class="bordereporte"  nowrap><%= obj.getNitpro()%></td>
                                    <td class="bordereporte" nowrap><%= obj.getNomcli()%></td>
                                    <td class="bordereporte"  nowrap><%= obj.getNitter()%></td>
                                    <td class="bordereporte"  nowrap><div align="center"><%= obj.getFecha_fra()%></div></td>
                                    <td class="bordereporte"  nowrap><div align="center"><%= obj.getFecha_venc_fra()%></div></td>
                                    <td class="bordereporte"   nowrap><div align="center"><%= obj.getNumaval()%></div></td>
                                    <td class="bordereporte"  nowrap><div align="right"><%= com.tsp.util.UtilFinanzas.customFormat2(obj.getValor_factura())%></div></td>
                                    <td class="bordereporte"  nowrap><div align="right"><%= com.tsp.util.UtilFinanzas.customFormat2(obj.getVlr_saldo())%></div></td>
                                </tr>
                                <%
                                                                }//Fin Si
                                                            }//Fin Para
                                %>
                            </table>
                        </form>
            </table>
            <%
                                    } else {//No se encontraron Resultados
            %>
            <table width="70%" border="2" align="center">
                <tr>
                    <td>
                        <table width="100%"  border="1" align="center"  bordercolor="#F7F5F4" bgcolor="#FFFFFF">
                            <tr>
                                <td align="center" class="mensajes">No se encontraron resultados. </td>
                                <td width="29"><img src="<%=BASEURL%>/images/cuadronaranja.JPG"></td>
                            </tr>
                        </table>
                    </td>
                </tr>
            </table>
            <%}%>
            <table width="100%" align="center">
                <tr>
                    <td>
                        <img src="<%=BASEURL%>/images/botones/regresar.gif" name="c_regresar" id="c_regresar" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" onClick="window.location =  '<%= CONTROLLER%>?estado=Menu&accion=Cargar&carpeta=/jsp/fenalco/contabilizacion/&pagina=FiltroFacturaEndoso.jsp';">
                        <img src="<%=BASEURL%>/images/botones/aceptar.gif" name="c_aceptar" onClick="validar(f1);" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" style="cursor:hand">

                        <img src="<%=BASEURL%>/images/botones/salir.gif" name="c_salir" onClick="parent.close();" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" style="cursor:hand">

                    </td>
                </tr>
            </table>
        </div>
        <%=datos[1]%>
        <iframe width="188" height="166" name="gToday:normal:agenda.js:gfPop:plugins.js" id="gToday:normal:agenda.js:gfPop:plugins.js" src="<%=BASEURL%>/js/Calendario/ipopeng.htm" scrolling="yes" frameborder="0" style="visibility:visible; z-index:999; position:absolute; left:-500px; top:0px;">
        </iframe>
         <script type="text/javascript">
            fxheaderInit('t1',500,1,0);
            fxheader();
        </script>
    </body>
</html>

