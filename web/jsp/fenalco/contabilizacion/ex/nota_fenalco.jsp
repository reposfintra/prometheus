<!--
- Autor : Ing. Roberto Rocha	
- Date  : 07 de Nov de 2007
- Copyrigth Notice : Fintravalores S.A. S.A
-->
<%--
-@(#)
--Descripcion : Pagina JSP, que maneja la busqueda de las condicones de los negocios.
--%>

<%-- Declaracion de librerias--%>
<%@page contentType="text/html"%>
<%@ page session="true"%>
<%@ page errorPage="../error/error.jsp"%>
<%@page import="com.tsp.operation.model.beans.*, java.util.*"%>
<%@ include file="/WEB-INF/InitModel.jsp"%>
<%@page import="com.tsp.operation.model.*"%>
<%@page import="com.tsp.finanzas.contab.model.*"%>
<%@page import="com.tsp.util.*"%>
<%@ taglib uri="http://jakarta.apache.org/taglibs/input-0.90" prefix="input" %>
<html>
<head><title>Contabilizar Negocios</title>
<%String path = application.getRealPath(model.menuService.getRealUrl(request.getRequestURI()));
  String datos[] = model.menuService.getContenidoMenu(BASEURL,request.getRequestURI(),path);
  String ayer  =  Utility.convertirFecha( Utility.getHoy("-"), 0);
  String ften =  Utility.convertirFecha( Utility.getHoy("-"), 17);
%>

<link href="<%= BASEURL %>/css/estilostsp.css" rel="stylesheet" type="text/css">
<script type='text/javascript' src="<%= BASEURL %>/js/boton.js"></script>
<script type='text/javascript' src="<%= BASEURL %>/js/general.js"></script>
<script src='<%=BASEURL%>/js/date-picker.js'></script>
<script>
			function validar( theForm )
			{
			  var sw = 0;
	          var cont=0;
			
						 if( theForm.codfact.value =='')
						 {  alert('Deber� digitar el numero de la factura..'); 		   
							sw=1; 
							theForm.fecini.focus();   
						} 
						if( theForm.fecini.value =='')
						 {  alert('Deber� establecer la fecha inicial'); 		   
							sw=1; 
							theForm.fecini.focus();   
						} 
						if( theForm.fecfin.value =='')
						{  alert('Deber� establecer la fecha final'); 			   
						   sw=1; 
						   theForm.fecfin.focus();  
						}  
						if ( theForm.fecini.value != "" && theForm.fecfin.value != "" )
						{
								var fecha1 = theForm.fecini.value.replace( /-|:| /gi, '' );
								var fecha2 = theForm.fecfin.value.replace( /-|:| /gi, '' ); 
								var fech1 = parseFloat( fecha1 );
								var fech2 = parseFloat( fecha2 );
								if( fecha2 < fecha1 )
								{ 				   
									alert( 'La fecha final debe ser mayor que la fecha inicial!' );
									sw=1;	
								}				 
						 }
				  if(sw==0)
				  theForm.submit();
			}				
</script>

</head>
<body onLoad="redimensionar();doFieldFocus( form2 );" onResize="redimensionar();">
<div id="capaSuperior" style="position:absolute; width:100%; height:100px; z-index:0; left: 0px; top: 0px;">
<jsp:include page="/WEB-INF/toptsp.jsp?encabezado=Notas Debito"/>
</div>

<div id="capaCentral" style="position:absolute; width:100%; height:85%; z-index:0; left: 0px; top: 100px; overflow: scroll;"> 
<form name="form2" method="post"  id="form2" action='<%=CONTROLLER%>?estado=Negocios&accion=Update&op=7'>
    <table width="380" border="2" align="center">
      <tr>
        <td>
		<table width="100%" align="center"  class="tablaInferior">
			<tr>
				<td width="173" class="subtitulo1">Notas Debito</td>
				<td width="205" class="barratitulo"><img src="<%=BASEURL%>/images/titulo.gif" width="32" height="20" align="left"><%=datos[0]%></td>
			</tr> 
			<tr class="fila">
			  <td  >Digite la Factura Devuelta </td>
			  <td valign="middle">
			    <input type="text" name="codfact">
			    </a> <img src="<%= BASEURL%>/images/botones/iconos/obligatorio.gif" width="10" height="10">
			  </td> 
		    </tr>
			
			<tr class="fila">
			  <td  >Escoja la Fecha Inicial </td>
			  <td valign="middle">
			   <input name="fecini" type="text" class="textbox" id="fecini" size="10" readonly  value="<%= ayer %>">
			   <img src="<%=BASEURL%>/images/cal.gif" width="16" height="16" align="absmiddle" style="cursor:hand " onclick="if(self.gfPop)gfPop.fPopCalendar(document.form2.fecini);return false;" HIDEFOCUS>
			    <img src="<%= BASEURL%>/images/botones/iconos/obligatorio.gif" width="10" height="10">
			  </td> 
		    </tr>
			
			<tr class="fila">
			  <td  >Escoja la Fecha Final </td>
			  <td valign="middle">
			    <input name="fecfin" type="text" class="textbox" id="fecfin" size="10" readonly value="<%= ften%>">
              	<img src="<%=BASEURL%>/images/cal.gif" width="16" height="16" align="absmiddle" style="cursor:hand " onclick="if(self.gfPop)gfPop.fPopCalendar(document.form2.fecfin);return false;" HIDEFOCUS>
				<img src="<%= BASEURL%>/images/botones/iconos/obligatorio.gif" width="10" height="10">
			  </td> 
		    </tr>
			
			
        </table>
		</td>
      </tr>
	  <tr>
	  	<td align="center" class="filaazul">
			<img src="<%=BASEURL%>/images/botones/aceptar.gif" style="cursor:hand" title="Aceptar" name="aceptar"  onClick='validar(form2);' onMouseOver="botonOver(this);" onMouseOut="botonOut(this);">
		</td>
	  </tr>
    </table>


<%
String Retorno=(String)request.getParameter("msg");
if( Retorno != null ){%>  
  <table border="2" align="center">
  <tr>
    <td><table width="100%"  border="1" align="center"  bordercolor="#F7F5F4" bgcolor="#FFFFFF">
      <tr>
        <td width="229" align="center" class="mensajes"><%=Retorno%></td>
        <td width="29" background="<%=BASEURL%>/images/cuadronaranja.JPG">&nbsp;</td>
        <td width="58">&nbsp;</td>
      </tr>
    </table></td>
  </tr>
</table>
<%}%>
<p>
<div align="center"><img src="<%=BASEURL%>/images/botones/salir.gif"  name="regresar" style="cursor:hand" title="Salir al Menu Principal" onMouseOver="botonOver(this);" onClick="parent.close();" onMouseOut="botonOut(this);" ></div>
</p>		
</form>
</div>
<%=datos[1]%>
<iframe width="188" height="166" name="gToday:normal:agenda.js:gfPop:plugins.js" id="gToday:normal:agenda.js:gfPop:plugins_24.js" src="<%=BASEURL%>/js/Calendario/ipopeng.htm" scrolling="yes" frameborder="0" style="visibility:visible; z-index:999; position:absolute; left:-500px; top:0px;">
</iframe>
</body>
</html>
