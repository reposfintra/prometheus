<%@page contentType="text/html"%>
<%@page session="true"%>
<%@page errorPage="../error/ErrorPage.jsp"%>
<%@page import="java.util.*"%>
<%@page import="com.tsp.util.*"%>
<%@include file="/WEB-INF/InitModel.jsp"%>

<%
  String path = application.getRealPath(model.menuService.getRealUrl(request.getRequestURI()));
  String datos[] = model.menuService.getContenidoMenu(BASEURL,request.getRequestURI(),path);
%>
<html>
    <head>

        <title>Consultar Negocios</title>        
		<script src='<%=BASEURL%>/js/date-picker.js'></script>
		<script language="JavaScript" type="text/javascript" src="<%=BASEURL%>/js/boton.js"></script>
		<script type='text/javascript' src="<%=BASEURL%>/js/validar.js"></script> 
        <link href="../css/estilostsp.css" rel="stylesheet" type="text/css">
		<script>
			function validar( theForm ){
			  var sw = 0;
	          var cont=0;
			  for(var i=0;i<theForm.length;i++){
                      var ele = theForm.elements[i];
							 if( theForm.fecini.value =='')
							 {  alert('Deber� establecer la fecha inicial'); 		   
								sw=1; 
								theForm.fecini.focus();  break;  
							 } 
							if( theForm.fecfin.value =='')
							{  alert('Deber� establecer la fecha final'); 			   
							   sw=1; 
							   theForm.fecfin.focus();  break;  
							}  
							if ( theForm.fecini.value != "" && theForm.fecfin.value != "" ) {
									var fecha1 = theForm.fecini.value.replace( /-|:| /gi, '' );
									var fecha2 = theForm.fecfin.value.replace( /-|:| /gi, '' );
									 
									var fech1 = parseFloat( fecha1 );
									var fech2 = parseFloat( fecha2 );
									 
									if( fecha2 < fecha1 ) { 				   
										 alert( 'La fecha final debe ser mayor que la fecha inicial!' );
										 return false;					 
									}
								}
                   }	   
				  if(sw==0)
				  theForm.submit();

			}				

	
		</script>
    </head>
   <body onResize="redimensionar()" onLoad="redimensionar()">
<div id="capaSuperior" style="position:absolute; width:100%; height:100px; z-index:0; left: 1px; top: 0px;">
 <jsp:include page="/toptsp.jsp?encabezado=Consulta de Negocios"/>
</div>
<div id="capaCentral" style="position:absolute; width:100%; height:87%; z-index:0; left: 0px; top: 100px; overflow: scroll;">

        <form name='formulario' method='post' id="formulario" action="<%=CONTROLLER%>?estado=Negocios&accion=Ingreso&op=2" >
         <table width="80%"  border="2" align="center" >
              <tr>
                <td>
				<table width="100%"  border="0" class="tablaInferior">
                  
				  <tr>
                    <td class="subtitulo1" colspan="4">Consulta de Ingresos Fenalco </td>
          			<td colspan="3" class="barratitulo"><img src="<%=BASEURL%>/images/titulo.gif" width="32" height="20" align="left"><%=datos[0]%></td>
                  </tr>
               
				  
				   
				  <tr class="fila">
                    <td colspan="2" align="left">Fecha de Negocio</td>
                    <td width="15%" align="left">&nbsp;Fecha Inicial</td>
                    <td width="15%" align="left">
					
				  		<input name="fecini" type="text" class="textbox" id="fecini" size="10" readonly>
              			<span class="Letras"><img src="<%=BASEURL%>/images/cal.gif" width="16" height="16" align="absmiddle" style="cursor:hand " onclick="if(self.gfPop)gfPop.fPopCalendar(document.formulario.fecini);return false;" HIDEFOCUS></span>
						
					</td>
                    <td width="15%" align="left">&nbsp;Fecha Final</td>
                    <td width="19%" align="left">
					
              			<input name="fecfin" type="text" class="textbox" id="fecfin" size="10" readonly>
              			<span class="Letras"><a href="javascript:void(0)" onclick="jscript: show_calendar('fecfin');" HIDEFOCUS> </a> <img src="<%=BASEURL%>/images/cal.gif" width="16" height="16" align="absmiddle" style="cursor:hand " onclick="if(self.gfPop)gfPop.fPopCalendar(document.formulario.fecfin);return false;" HIDEFOCUS></span>
						
					</td>
                  </tr>
				  <tr class="fila">
				    <td colspan="2" align="left">&nbsp;</td>
				    <td align="left" colspan="4">&nbsp;</td>
			      </tr> 
				  </table>
			    </td>
              </tr>
		</table>
		 <p>&nbsp;</p>
		 <table align="center">
             <tr>
                <td colspan="2" nowrap align="center">
				<img src="<%=BASEURL%>/images/botones/aceptar.gif"    height="21"  title='Consultar'  onclick='validar(formulario);'   onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" style="cursor:hand">
				<img src="<%=BASEURL%>/images/botones/salir.gif"      height="21"  title='Salir'      onClick="window.close();"     onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" style="cursor:hand">
				</td>
			</tr>
        </table>
  </form > 
	</div>  
	<iframe width=188 height=166 name="gToday:normal:agenda.js:gfPop:plugins.js" id="gToday:normal:agenda.js:gfPop:plugins.js" src="<%=BASEURL%>/js/Calendario/ipopeng.htm" scrolling="no" frameborder="0" style="visibility:visible; z-index:999; position:absolute; left:-500px; top:0px;"> </iframe>    
    <%=datos[1]%>
	</body>
	</html>