<%-- 
    Document   : Mensaje
    Created on : 11/07/2011, 08:39:45 AM
    Author     : ivargas
--%>

 <%--   - @(#)
     - Description:  Vista que permite mostrar mensaje
 --%>

<%@page    session   ="true"%>
<%@page    errorPage ="/error/ErrorPage.jsp"%>
<%@include file      ="/WEB-INF/InitModel.jsp"%>
<%@page    import    ="com.tsp.util.*"%>
<%@page    import    ="java.util.*" %>


<html>
<head>
      <title>Mensaje</title>
      <script type='text/javascript' src="<%= BASEURL %>/js/boton.js"></script>
      <link href="<%= BASEURL %>/css/estilostsp.css" rel="stylesheet" type="text/css">

</head>
<body>


<div id="capaSuperior" style="position:absolute; width:100%; height:100px; z-index:0; left: 0px; top: 0px;">
<jsp:include page="/WEB-INF/toptsp.jsp?encabezado=Mensaje"/>
</div>

<div id="capaCentral" style="position:absolute; width:100%; height:85%; z-index:0; left: 0px; top: 100px; overflow: scroll;">
     <% String msj  = request.getParameter("msg");   %>

        <table width="600" border="2" align="center">
              <tr>
                 <td ALIGN='center'>
                         <table width='100%'   class='tablaInferior'   >
                                  <tr>
                                         <td colspan='2'>
                                               <table cellpadding='0' cellspacing='0' width='100%'>
                                                   <tr class="fila">
                                                          <td align="left" width='55%' class="subtitulo1">&nbsp MENSAJE</td>
                                                          <td align="left" width='*' class="barratitulo"><img src="<%=BASEURL%>/images/titulo.gif" width="32" height="20" align="left"></td>
                                                   </tr>
                                               </table>
                                        </td>
                                  </tr>
                                  <tr  class="fila">
                                      <td colspan='2' style="font-size:11pt;" >
                                            <%=msj%>
                                         </td>
                                  </tr>
                       </table>
                 </td>
              </tr>
       </table>
        <p>
           <img src="<%=BASEURL%>/images/botones/salir.gif"          name="exit"    height="21"          onClick="parent.close();">
           <img src="<%=BASEURL%>/images/botones/regresar.gif" name="c_regresar" id="c_regresar" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" onClick="window.location =  '<%= CONTROLLER%>?estado=Menu&accion=Cargar&carpeta=/jsp/fenalco/contabilizacion/&pagina=FiltroFacturaEndoso.jsp';">
                       
        </p>
</div>

</body>
</html>
