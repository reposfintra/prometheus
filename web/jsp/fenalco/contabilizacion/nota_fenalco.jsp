<!--
- Autor : Ing. Roberto Rocha	
- Date  : 07 de Nov de 2007
- Copyrigth Notice : Fintravalores S.A. S.A
-->
<%--
-@(#)
--Descripcion : Pagina JSP, que maneja la busqueda de las condicones de los negocios.
--%>

<%-- Declaracion de librerias--%>
<%@page contentType="text/html"%>
<%@ page session="true"%>
<%@ page errorPage="../error/error.jsp"%>
<%@page import="com.tsp.operation.model.beans.*, java.util.*"%>
<%@ include file="/WEB-INF/InitModel.jsp"%>
<%@page import="com.tsp.operation.model.*"%>
<%@page import="com.tsp.finanzas.contab.model.*"%>
<%@page import="com.tsp.util.*"%>
<%@ taglib uri="http://jakarta.apache.org/taglibs/input-0.90" prefix="input" %>

<html>
<head><title>Contabilizar Negocios</title>
<%String path = application.getRealPath(model.menuService.getRealUrl(request.getRequestURI()));
  String datos[] = model.menuService.getContenidoMenu(BASEURL,request.getRequestURI(),path);
  String ayer  =  Utility.convertirFecha( Utility.getHoy("-"), 0);
  String ften =  Utility.convertirFecha( Utility.getHoy("-"), 17);
%>

<link href="<%= BASEURL %>/css/estilostsp.css" rel="stylesheet" type="text/css">
<script type='text/javascript' src="<%= BASEURL %>/js/boton.js"></script>
<script type='text/javascript' src="<%= BASEURL %>/js/general.js"></script>
<script src='<%=BASEURL%>/js/date-picker.js'></script>

<script type="text/javascript">
		function validar( theForm ){
			var sw = 0;
	        var cont=0;
			
			if( theForm.codfact.value ==''){ 
				alert('Deber� digitar el numero de la factura..'); 		   
				sw=1; 
				theForm.fecini.focus();   
			} 
			if( theForm.fecini.value ==''){
				alert('Deber� establecer la fecha inicial'); 		   
				sw=1; 
				theForm.fecini.focus();   
			} 
			if( theForm.fecfin.value ==''){
				alert('Deber� establecer la fecha final'); 			   
			    sw=1; 
			    theForm.fecfin.focus();  
			}  
			if ( theForm.fecini.value != "" && theForm.fecfin.value != "" ){
				var fecha1 = theForm.fecini.value.replace( /-|:| /gi, '' );
				var fecha2 = theForm.fecfin.value.replace( /-|:| /gi, '' ); 
				var fech1 = parseFloat( fecha1 );
				var fech2 = parseFloat( fecha2 );
				if( fecha2 < fecha1 ){ 				   
					alert( 'La fecha final debe ser mayor que la fecha inicial!' );
					sw=1;	
				}				 
			}
			if(sw==0) theForm.submit();
		}
			
		function imprimirpdf(frm){
			frm.action='<%=CONTROLLER%>?estado=Pdf&accion=Imprimir&opcion=imprimir_reporte_fenalco';
			frm.submit();		
		}
					
</script>

</head>
<body onLoad="redimensionar();doFieldFocus( form2 );" onResize="redimensionar();">
<div id="capaSuperior" style="position:absolute; width:100%; height:100px; z-index:0; left: 0px; top: 0px;">
<jsp:include page="/WEB-INF/toptsp.jsp?encabezado=Notas Debito"/>
</div>

<div id="capaCentral" style="position:absolute; width:100%; height:85%; z-index:0; left: 0px; top: 100px; overflow: scroll;"> 
<%

	String noFactura = request.getParameter("noFactura");
	if(noFactura!=null){
		BeanGeneral beanGeneral = null;//Defino un objeto BeanGeneral
		try{
			beanGeneral = model.Negociossvc.getReporteFenalcoService(noFactura);
			//Obtengo los datos del cheque a devolver
			
			model.PdfImprimirSvc.setBeanGeneral(beanGeneral);
			//Paso el bean a la clase service para su posterior manipulacion
			
		}catch(Exception e){
			out.print(e.toString());
		}
	
%>

<form name="formularioFenalco" id="formularioFenalco" method="post" action="<%=CONTROLLER%>?estado=Pdf&accion=Imprimir&opcion=imprimir_reporte_fenalco">
<table border="0" width="100%">
<tr><td class="subtitulo1" colspan="4"><img src="<%=BASEURL%>/images/titulo.gif" width="32" height="20" align="left">REPORTE CHEQUES DEVUELTOS</td></tr>
<tr><td class="fila"> FECHA:</td>
<% 
	Calendar cal = Calendar.getInstance();
	int year = cal.get(Calendar.YEAR);
	int monthInt = cal.get(Calendar.MONTH);
	String month = "";
	int day = cal.get(Calendar.DAY_OF_MONTH);
	if(monthInt==0) month = "Enero";
	if(monthInt==1) month = "Febrero";
	if(monthInt==2) month = "Marzo";
	if(monthInt==3) month = "Abril";
	if(monthInt==4) month = "Mayo";
	if(monthInt==5) month = "Junio";
	if(monthInt==6) month = "Julio";
	if(monthInt==7) month = "Agosto";
	if(monthInt==8) month = "Septiembre";
	if(monthInt==9) month = "Octubre";
	if(monthInt==10) month = "Noviembre";
	if(monthInt==11) month = "Diciembre";
	out.print("<TD class=fila>"+day + " de "+month+" de "+year+"</tr>");
%>

</td><td class="fila" colspan="2">c&oacute;digo</td></tr>
<tr><td class="fila">NOMBRE DEL AFILIADO</td><td colspan="3" class="fila"><input type="text" value="FintraValores" size="50" disabled></td></tr>
<tr><td class="fila">NIT</td><td colspan="3" class="fila"><input type="text" value="802.022.016-1" size="50" disabled></td></tr>

<tr><td class="subtitulo1" colspan="4">DATOS DEL GIRADOR</td>
<tr><td class="fila">N&Uacute;MERO DEL DOCUMENTO</td><td class="fila" colspan="3"><input type="text"  disabled name="nit" size="50"value="<%out.print(beanGeneral.getValor_01());%>"></td></tr>
<tr><td class="fila">NOMBRE COMPLETO</td><td class="fila" colspan="3"><input type="text"  name="nombre" size="50"value="<%out.print(beanGeneral.getValor_02());%>"></td></tr>
<tr><td class="fila">DIRECCI&Oacute;N</td><td class="fila" colspan="3"><input type="text" name="direccion" size="50"value="<%out.print(beanGeneral.getValor_03());%>"></td></tr>
<tr><td class="fila">TELEFONOS</td><td class="fila" colspan="3">
<input type="text" name="telefono" size="22"value="<%out.print(beanGeneral.getValor_04());%>">
<input type="text" name="telcontacto" size="23" value="<%out.print(beanGeneral.getValor_05());%>">
</td></tr>

<tr><td class="subtitulo1" colspan="4">DATOS DEL CHEQUE</td>
<tr><td class="fila">C&Oacute;DIGO DEL BANCO</td><td class="fila"><input type="text"   name="banco" size="50"value="<%out.print(beanGeneral.getValor_06());%>"></td><td class="fila">N&Uacute;MERO DE LA CUENTA</td><td class="fila"><input type="text" name="cuenta_cheque" size="50"value="<%if(beanGeneral.getValor_08()==null){out.print("");}else{out.print(beanGeneral.getValor_08());}%>"></td></tr>
<tr><td class="fila">N&Uacute;MERO DEL CHEQUE</td><td class="fila"><input type="text"  name="num_cheque" size="50"value="<%out.print(beanGeneral.getValor_10());%>"></td><td class="fila">VALOR:</td><td class="fila"><input type="text" name="valor" size="50"value="<%out.print(beanGeneral.getValor_12());%>"></td></tr>
<tr><td class="fila">N&Uacute;MERO DE LA CONSULTA</td><td colspan="3" class="fila"><input type="text"   name="aval" size="50"value="<%out.print(beanGeneral.getValor_07());%>"></td></tr>
<tr><td class="fila">BENEFICIARIO</td><td colspan="3" class="fila"><input type="text" name="beneficiario" size="50"value="<%out.print(beanGeneral.getValor_09());%>"></td></tr>
<tr><td colspan="4" class="fila">NOMBRE DE OPERADOR</td></tr>
</table>
</form>

<script type="text/javascript">alert("Antes de imprimir verifique el valor del cheque y el numero de la cuenta");</script>
<img src="<%=BASEURL%>/images/imprime.gif" width="70" height="19" style="cursor:hand" onclick="imprimirpdf(formularioFenalco)">

<%}
	else{
%>

<form name="form2" method="post"  id="form2" action='<%=CONTROLLER%>?estado=Negocios&accion=Update&op=7'>
    <table width="380" border="2" align="center">
      <tr>
        <td>
		<table width="100%" align="center"  class="tablaInferior">
			<tr>
				<td width="173" class="subtitulo1">Notas Debito</td>
				<td width="205" class="barratitulo"><img src="<%=BASEURL%>/images/titulo.gif" width="32" height="20" align="left"><%=datos[0]%></td>
			</tr> 
			<tr class="fila">
			  <td  >Digite la Factura Devuelta </td>
			  <td valign="middle">
			    <input type="text" name="codfact">
			    </a> <img src="<%= BASEURL%>/images/botones/iconos/obligatorio.gif" width="10" height="10">
			  </td> 
		    </tr>
			
			<tr class="fila">
			  <td  >Escoja la Fecha Inicial </td>
			  <td valign="middle">
			   <input name="fecini" type="text" class="textbox" id="fecini" size="10" readonly  value="<%= ayer %>">
			   <img src="<%=BASEURL%>/images/cal.gif" width="16" height="16" align="absmiddle" style="cursor:hand " onclick="if(self.gfPop)gfPop.fPopCalendar(document.form2.fecini);return false;" HIDEFOCUS>
			    <img src="<%= BASEURL%>/images/botones/iconos/obligatorio.gif" width="10" height="10">
			  </td> 
		    </tr>
			
			<tr class="fila">
			  <td  >Escoja la Fecha Final </td>
			  <td valign="middle">
			    <input name="fecfin" type="text" class="textbox" id="fecfin" size="10" readonly value="<%= ften%>">
              	<img src="<%=BASEURL%>/images/cal.gif" width="16" height="16" align="absmiddle" style="cursor:hand " onclick="if(self.gfPop)gfPop.fPopCalendar(document.form2.fecfin);return false;" HIDEFOCUS>
				<img src="<%= BASEURL%>/images/botones/iconos/obligatorio.gif" width="10" height="10">
			  </td> 
		    </tr>
			
			
        </table>
		</td>
      </tr>
	  <tr>
	  	<td align="center" class="filaazul">
			<img src="<%=BASEURL%>/images/botones/aceptar.gif" style="cursor:hand" title="Aceptar" name="aceptar"  onClick='validar(form2);' onMouseOver="botonOver(this);" onMouseOut="botonOut(this);">
		</td>
	  </tr>
    </table>
<%}%>

<%
String Retorno=(String)request.getParameter("msg");
if( Retorno != null ){%>  
  <table border="2" align="center">
  <tr>
    <td><table width="100%"  border="1" align="center"  bordercolor="#F7F5F4" bgcolor="#FFFFFF">
      <tr>
        <td width="229" align="center" class="mensajes"><%=Retorno%></td>
        <td width="29" background="<%=BASEURL%>/images/cuadronaranja.JPG">&nbsp;</td>
        <td width="58">&nbsp;</td>
      </tr>
    </table></td>
  </tr>
</table>
<%}%>
<p>
<div align="center"><img src="<%=BASEURL%>/images/botones/salir.gif"  name="regresar" style="cursor:hand" title="Salir al Menu Principal" onMouseOver="botonOver(this);" onClick="parent.close();" onMouseOut="botonOut(this);" ></div>
</p>		
</form>
</div>
<%=datos[1]%>
<iframe width="188" height="166" name="gToday:normal:agenda.js:gfPop:plugins.js" id="gToday:normal:agenda.js:gfPop:plugins_24.js" src="<%=BASEURL%>/js/Calendario/ipopeng.htm" scrolling="yes" frameborder="0" style="visibility:visible; z-index:999; position:absolute; left:-500px; top:0px;">
</iframe>
</body>
</html>
