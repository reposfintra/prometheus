<%@page contentType="text/html"%>
<%@page session="true"%>
<%@page errorPage="../error/ErrorPage.jsp"%>
<%@page import="java.util.*"%>
<%@page import="com.tsp.util.*"%>
<%@include file="/WEB-INF/InitModel.jsp"%>

<%
  String path = application.getRealPath(model.menuService.getRealUrl(request.getRequestURI()));
  String datos[] = model.menuService.getContenidoMenu(BASEURL,request.getRequestURI(),path);
%>
<html>
    <head>
        <title>Consultar Negocios</title>        
		<script src='<%=BASEURL%>/js/date-picker.js'></script>
		<script language="JavaScript" type="text/javascript" src="<%=BASEURL%>/js/boton.js"></script>
		<script type='text/javascript' src="<%=BASEURL%>/js/validar.js"></script> 
		<script language='javascript'>	
			function exporta()
			{
				window.open('<%= CONTROLLER %>?estado=Negocios&accion=Ingreso&op=1','EXP','status=yes,scrollbars=yes,width=400,height=150,resizable=no');
			}
		</script>
        <link href="../css/estilostsp.css" rel="stylesheet" type="text/css">
    </head>
   <body onResize="redimensionar()" onLoad="redimensionar()">
<div id="capaSuperior" style="position:absolute; width:100%; height:100px; z-index:0; left: 1px; top: 0px;">
<jsp:include page="/toptsp.jsp?encabezado=Consulta de Negocios"/>
</div>
<div id="capaCentral" style="position:absolute; width:100%; height:85%; z-index:0; left: 0px; top: 100px; overflow: visibles;">
<%
Vector a=model.Negociossvc.VectIn();
Vector b=model.Negociossvc.VectCod();
if (b.size() > 0 )
{
		
		LinkedList mes=new LinkedList();
		LinkedList anio=new LinkedList();
		String fi= (String)a.get(0);
		String ff= (String)a.get(1);
		Vector head=new Vector();
		java.util.Calendar fin = Util.crearCalendarDate2(fi);
		java.util.Calendar ffin = Util.crearCalendarDate2(ff);%>
			<table align="center">
				<tr>
					<td align="center" class="subtitulo1" >Id Negocio</td>
					<td align="center" class="subtitulo1" width="200" nowrap>Afiliado</td>
					<td align="center" class="subtitulo1" width="200" nowrap>Cliente</td>
					<%
					head.add("Id Negocio");
					head.add("Afiliado");
					head.add("Cliente");
					while( fin.before(ffin) ){
						head.add( Util.NombreMes2(fin.get(2))+"-"+fin.get(1) );%>
						<td align="center" class="subtitulo1"><p><%=Util.NombreMes2(fin.get(2))%> </p>
						  <p><%=fin.get(1)%></p></td>
						<%
						   anio.add(	Integer.toString( fin.get(1))	);
						   mes.add(		Integer.toString( fin.get(2))	);
						   fin.add(2, 1);
						}
					%>
				</tr>
				<%
				String[][] body = new String[ Integer.parseInt((String)a.get(2)) ][head.size()];
				int cont=0;
				for(int i=0;i<b.size();i+=3){%>
				<tr  class='<%=(i%2==0?"filagris":"filaazul") %>'>
					<td align="center" ><%=(String)b.get(i)%></td>
					<td align="center" ><%=(String)b.get(i+2)%></td>
					<td align="center" ><%=(String)b.get(i+1)%></td>
					<%
						body[cont][0]=(String)b.get(i);
						body[cont][1]=(String)b.get(i+2);
						body[cont][2]=(String)b.get(i+1);
					for(int j=0;j<anio.size();j++){
						String an=(String)mes.get(j);
						int m=Integer.valueOf(an).intValue();
						m++;
						an=""+m;
						if (an.length()==1){an="0"+an;}
							double sh=model.Negociossvc.valor( (String)b.get(i) ,(String)anio.get(j) , an );
							if (sh==0)
							{%>
								<td align="center" ></td>
							<%}else{
								%>
								<td align="center" ><%=Util.customFormat(sh)%></td>
							<%}
							body[cont][j+3]=Double.toString(sh);
							%>
					<%}
					cont++;%>
				</tr>
				<%}
				session.setAttribute("head",head);
				session.setAttribute("body",body);
				body=null;
				head=null;
				%>
			</table>
			<p>&nbsp;</p>
			<div align="center">
			 <img src="<%=BASEURL%>/images/botones/exportarExcel.gif" name="exportar" id="exportar" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" onClick='javascript:exporta()' >
			 <img src="<%=BASEURL%>/images/botones/regresar.gif"  name="imgsalir" align="absmiddle" style="cursor:hand" onClick="history.back();" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);">
			 <img src="<%=BASEURL%>/images/botones/salir.gif"  name="imgsalir" align="absmiddle" style="cursor:hand" onClick="parent.close();" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);">
			</div>
<%}
else{
%>
<table border="2" align="center">
  <tr>
    <td><table width="100%"  border="1" align="center"  bordercolor="#F7F5F4" bgcolor="#FFFFFF">
      <tr>
        <td width="229" align="center" class="mensajes">La busqueda no arrojo resultados</td>
        <td width="29" background="<%=BASEURL%>/images/cuadronaranja.JPG">&nbsp;</td>
        <td width="58">&nbsp;</td>
      </tr>
    </table></td>
  </tr>
</table>
<div align="center">
			 <p><img src="<%=BASEURL%>/images/botones/regresar.gif"  name="imgsalir" align="absmiddle" style="cursor:hand" onClick="history.back();" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);">
			     <img src="<%=BASEURL%>/images/botones/salir.gif"  name="imgsalir" align="absmiddle" style="cursor:hand" onClick="parent.close();" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);">
             </p>
</div>
<%}%>
</div> 
<iframe width=188 height=166 name="gToday:normal:agenda.js:gfPop:plugins.js" id="gToday:normal:agenda.js:gfPop:plugins.js" src="<%=BASEURL%>/js/Calendario/ipopeng.htm" scrolling="no" frameborder="0" style="visibility:visible; z-index:999; position:absolute; left:-500px; top:0px;"> </iframe>    
<%=datos[1]%>
</body>
</html>