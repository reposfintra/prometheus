<%-- 
    Document   : IndemnizacionFacturas
    Created on : 27/11/2015, 09:52:15 AM
    Author     : egonzalez
--%>
<%@page session="true"%>
<%@page contentType="text/html" pageEncoding="ISO-8859-1"%>
<%@ include file="/WEB-INF/InitModel.jsp"%>
<%@page errorPage="/error/ErrorPage.jsp" %>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
        <title>Indemnizacion Facturas</title>

        <link type="text/css" rel="stylesheet" href="./css/jquery/jquery-ui/jquery-ui.css" />
        <link href="./css/popup.css" rel="stylesheet" type="text/css">
        <link href="./css/indemnizacion_factura.css" rel="stylesheet" type="text/css">
        <script type="text/javascript" src="./js/jquery/jquery.jqGrid/js/jquery.min.js"></script>
        <script type="text/javascript" src="./js/jquery-ui-1.8.5.custom.min.js"></script>
        <script type="text/javascript" src="./js/jquery/jquery.jqGrid-4.7.0/js/grid.locale-es.js"></script>
        <script type="text/javascript" src="./js/jquery/jquery.jqGrid-4.7.0/js/jquery.jqGrid.min.js"></script>

        <script type="text/javascript" src="./js/indemnizacion_facturas.js"></script>
    </head>
    <body>
        <div id="capaSuperior" style="position:absolute; width:100%; height:100px; z-index:0; left: 0px; top: 0px;">
            <jsp:include page="/WEB-INF/toptsp.jsp?encabezado=COMPRA CARTERA"/>
        </div>
        <div id="capaCentral" style="position:absolute; width:100%; height:80%; z-index:0; left: 0px; top: 100px; ">
            <br>
            <div id="busqueda">             
                <div id="divInterno">
                    <div style="float: left">
                        <label>Linea Negocio</label><span style="color: red"> *</span> &nbsp;
                        <select id="unidad_negocio">
                            <option value="FENALCO_ATL">Fenalco Atlantico</option>
                            <option value="FENALCO_BOL">Fenalco Bolivar</option>
                        </select>  
                        <label>Cartera en</label><span style="color: red"> *</span> &nbsp;
                        <select id="cartera_en">  </select> 
                        <label>Fecha corte</label><span style="color: red"> *</span> &nbsp;
                        <input type="text" id="fecha_inicio" class="inpt" readonly  style="width: 90px;">
                    </div>
                    <div class="radio-toolbar" style=" float:left;" >

                        <input type="radio" id="radio1" name="radios" value="1" checked>
                        <label for="radio1">Indemnizar</label>

                        <input type="radio" id="radio2" name="radios"value="2">
                        <label for="radio2">Desistimiento</label>

                    </div>

                    <div id="busqueda_avanzada">
                        <label>foto</label>
                        <input type="text" id="foto" value="" style="width: 55px;" maxlength="6">
                        <label>mora</label>
                        <select id="mora">
                            <option value="">Seleccione</option>
                        </select>                        
                    </div>
                    <button id="buscarAvanzada" class="ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only boton" style="margin-left: 10px;margin-top: -3px;"
                            role="button" aria-disabled="false">
                        <span id="mas" class="ui-button-text">Mas...</span>
                    </button>
                    <button id="buscar" class="ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only" style="margin-left: 10px;margin-top: -3px;"
                            role="button" aria-disabled="false">
                        <span class="ui-button-text">Buscar</span>
                    </button>

                    <button id="ver_facturas" class="ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only" style="margin-left: 10px;margin-top: -3px;"
                            role="button" aria-disabled="false">
                        <span class="ui-button-text">Ver indemnizadas</span>
                    </button>

                </div>  
            </div>
            <br>
            <div style="margin-top: 70px">
                <center>
                    <table id="tbl_facturas"></table>
                    <div id="page_facturas"></div>
                </center>
            </div>
            <div id="dialogo" title="Mensaje">
                <p id="msj"></p>

            </div>
            <div id="dialogLoading"  class="ventana" >
                <p  id="msj2">texto </p> <br/>
                <center>
                    <img src="<%=BASEURL%>/images/cargandoCM.gif"/>
                </center>
            </div>

            <div id="divSalidaEx" title="Exportacion" style=" display: block" >
                <p  id="msjEx" style=" display:  none"> Espere un momento por favor...</p>
                <center>
                    <img id="imgloadEx" style="position: relative;  top: 7px; display: none " src="./images/cargandoCM.gif"/>
                </center>
                <div id="respEx" style=" display: none"></div>
            </div> 

        </div>

    </body>
</html>
