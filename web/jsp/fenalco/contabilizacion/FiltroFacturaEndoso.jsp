<!--
- Autor : Ing. Iris Vargas
- Date  : 07-07-2011
- Copyrigth Notice : Geotech
-->
<%--
-@(#)
--Descripcion : Pagina JSP, que filtra las facturas a endosar.
--%>
<%@page contentType="text/html"%>
<%@page session="true"%>
<%@page import="java.util.*"%>
<%@page import="java.text.*"%>
<%@page import="com.tsp.operation.model.*"%>
<%@page import="com.tsp.operation.model.beans.*"%>
<%@page import="com.tsp.operation.controller.*"%>
<%@page import="com.tsp.util.*"%>
<%@page import="com.tsp.operation.model.services.GestionConveniosService"%>
<%@page errorPage="/error/ErrorPage.jsp"%>
<%@include file="/WEB-INF/InitModel.jsp"%>
<%@ taglib uri="http://jakarta.apache.org/taglibs/input-0.90" prefix="input" %>
<%String path = application.getRealPath(model.menuService.getRealUrl(request.getRequestURI()));
            String datos[] = model.menuService.getContenidoMenu(BASEURL, request.getRequestURI(), path);
            Usuario usuario = (Usuario) session.getAttribute("Usuario");
            ArrayList prefijos = model.gestionConveniosSvc.getPrefijos(usuario.getBd());
            model.clienteService.setTreeMapClientes();
            GestionConveniosService gest = new GestionConveniosService(usuario.getBd());
            ArrayList listent = gest.getEntidadRedescuento();
%>
<html>
    <head>

        <title>Envio de Facturas a Terceros</title>
        <link href="<%= BASEURL%>/css/estilostsp.css" rel='stylesheet'>
    </head>
    <script type='text/javascript' src="<%= BASEURL%>/js/validar.js"></script>
    <script src='<%= BASEURL%>/js/date-picker.js'></script>
    <script type='text/javascript' src="<%= BASEURL%>/js/boton.js"></script>
    <script src='<%=BASEURL%>/js/tools.js' language='javascript'></script>
    <script language="javascript" src="<%=BASEURL%>/js/general.js"></script>
    <script>
        function validarForma(){
            for (i = 0; i < forma.elements.length; i++){
                if (forma.elements[i].value == "" && forma.elements[i].name!="text" && forma.elements[i].name!="clientes"&& forma.elements[i].name!="proveedor" && forma.elements[i].name!="fecha"
                    && forma.elements[i].name!="factura" && forma.elements[i].name!="codcli" && forma.elements[i].name!="cccli" && forma.elements[i].name!="vence" && forma.elements[i].name!="lim1" && forma.elements[i].name!="lim2"){
                    forma.elements[i].focus();
                    alert('No se puede procesar la informaci�n. Campo Vac�o.');
                    return false;
                }

                if (forma.lim1.value  > forma.lim2.value)
                {
                    forma.elements[i].focus();
                    alert('El valor inicial no puede ser mayor que el final');
                    return false;
                }
                if ((forma.lim1.value==""  && forma.lim2.value!="")||(forma.lim1.value!=""  && forma.lim2.value==""))
                {
                    forma.elements[i].focus();
                    alert('No pueden quedar en blanco');
                    return false;
                }

            }
            return true;
        }
    </script>
    <body onresize="redimensionar()" onload = 'redimensionar()'>
        <div id="capaSuperior" style="position:absolute; width:100%; height:110px; z-index:0; left: 0px; top: 0px;">
            <jsp:include page="/toptsp.jsp?encabezado=Envio de Facturas a Terceros"/>
        </div>
        <div id="capaCentral" style="position:absolute; width:100%; height:87%; z-index:0; left: 0px; top: 109px; overflow: scroll;">
            <%
                        TreeMap pr = new TreeMap(); //model.clienteService.getTreemap();
                        Calendar FechaHoy = Calendar.getInstance();
                        Date d = FechaHoy.getTime();
                        SimpleDateFormat f = new SimpleDateFormat("yyyy-MM-dd");
                        TreeMap pro = new TreeMap(); //model.Negociossvc.getProv();
                        pro.put("     ...", "");
                        pr.put("     ...", "");
            %>
            <form name="forma" action='<%=CONTROLLER%>?estado=Analisis&accion=FacturasEndoso' id="forma" method="post">
                <table width="596"  border="2" align="center" cellpadding="0" cellspacing="0">
                    <tr>
                        <td><table width="100%" class="tablaInferior">
                                <tr>
                                    <td colspan="2" ><table width="100%"  border="0" cellpadding="0" cellspacing="1" class="barratitulo">
                                            <tr>
                                                <td width="56%" class="subtitulo1">&nbsp;Detalles<proveedor/td>
                                                <td width="44%" class="barratitulo"><img align="left" src="<%=BASEURL%>/images/titulo.gif" width="32" height="20"><%=datos[0]%></td>
                                                </tr>
                                        </table></td>
                                </tr>
                                <tr class="fila">
                                    <td>Avalista</td>
                                    <td nowrap>
                                        <select name="terceros" id="terceros">
                                            <option value="">Seleccione</option>
                                            <%
                                            String[] dato7 = null;
                                            for (int i = 0; i < listent.size(); i++) {
                                            dato7 = ((String) listent.get(i)).split("-"); %>
                                            <option value="<%=dato7[0]%>"><%=dato7[1]%></option>
                                            <%    }
                                                
                                            %> </select>
                                    </td>
                                </tr>
                                <tr class="fila">
                                    <td>Cliente</td>
                                    <td nowrap><table width="100%"  border="0" class="tablaInferior">
                                            <tr>
                                                <td><input name="text" type="text" class="textbox" id="campo" style="width:200;" onKeyUp="buscar(document.forma.clientes,this)" size="15" ></td>
                                            </tr>
                                            <tr>
                                                <td><input:select name="clientes" attributesText="class=textbox" options="<%=pr%>"/>&nbsp;</td>
                                            </tr>
                                        </table></td>
                                </tr>

                                <tr class="fila">
                                    <td valign="middle">Codigo</td>
                                    <td nowrap><input type="text" name="codcli"></td>
                                </tr>

                                <tr class="fila">
                                    <td valign="middle">CC Cliente</td>
                                    <td nowrap><input type="text" name="cccli"></td>
                                </tr>

                                <tr class="fila">
                                    <td>Afiliado</td>
                                    <td nowrap><table width="100%"  border="0" class="tablaInferior">
                                            <tr>
                                                <td><input name="text" type="text" class="textbox" id="campo" style="width:200;" onKeyUp="buscar(document.forma.proveedor,this)" size="15" ></td>
                                            </tr>
                                            <tr>
                                                <td><input:select name="proveedor" attributesText="class=textbox" options="<%=pro%>"/>&nbsp;</td>
                                            </tr>
                                        </table></td>
                                </tr>




                                <tr class="fila">
                                    <td width="27%" valign="middle">Vencimiento</td>
                                    <td width="73%" nowrap>
                                        <input name="fecha" type='text' class="textbox" id="fecha" style='width:120' value="" readonly>
                                        <img alt="calendario" src="<%=BASEURL%>/images/cal.gif" width="16" height="16" align="absmiddle" style="cursor:pointer;" onClick="if(self.gfPop)gfPop.fPopCalendar(document.getElementById('fecha'));return false;"></td>
                                </tr>

                                <tr class="fila">
                                    <td width="27%" valign="middle">Prefijo factura</td>
                                    <td width="73%" nowrap>
                                        <select name="tfac">
                                            <option value="...">Todas</option>
                                            <%for (int i = 0; i < prefijos.size(); i++) {
                                                                if (((String[]) prefijos.get(i))[0].equals("FAC")) {%>
                                            <option value="<%=((String[]) prefijos.get(i))[1]%>"><%=((String[]) prefijos.get(i))[1]%></option>
                                            <%  }
                                                            }%>
                                        </select></td>
                                </tr>
                                <tr class="fila">
                                    <td valign="middle">Vencimiento</td>
                                    <td nowrap> Entre
                                        <input name="lim1" type="text" class="textbox" onKeyPress="soloDigitos_signo(event,'decOK');" size="6" maxlength="6">
		    y
                                        <input name="lim2" type="text" class="textbox" onKeyPress="soloDigitos_signo(event,'decOK');" size="6" maxlength="6">
		    Dias</td>
                                </tr>

                                <tr class="fila">
                                    <td valign="middle">Factura</td>
                                    <td nowrap><input type="text" name="factura"></td>
                                </tr>



                            </table></td>
                    </tr>
                </table>
                <br>

                <br>
                <center>
                    <img src="<%=BASEURL%>/images/botones/aceptar.gif" name="c_aceptar" onClick="if (validarForma()) forma.submit(); " onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" style="cursor:hand">
                    <img src="<%=BASEURL%>/images/botones/cancelar.gif" name="c_cancelar" onClick="forma.reset();" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" style="cursor:hand">
                    <img src="<%=BASEURL%>/images/botones/salir.gif" name="c_salir" onClick="parent.close();" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" style="cursor:hand">
                </center>
            </form>
            <% if (request.getParameter("msg") != null) {%>
            <p><table border="2" align="center">
                <tr>
                    <td><table width="100%"  border="1" align="center"  bordercolor="#F7F5F4" bgcolor="#FFFFFF">
                            <tr>
                                <td width="363" align="center" class="mensajes"><%=request.getParameter("msg").toString()%></td>
                                <td width="29" background="<%=BASEURL%>/images/cuadronaranja.JPG">&nbsp;</td>
                                <td width="58">&nbsp;</td>
                            </tr>
                        </table></td>
                </tr>
            </table>
        </p>
        <%}%>
    </div>
    <%=datos[1]%>
    <iframe width="188" height="166" name="gToday:normal:agenda.js:gfPop:plugins.js" id="gToday:normal:agenda.js:gfPop:plugins.js" src="<%=BASEURL%>/js/Calendario/ipopeng.htm" scrolling="yes" frameborder="0" style="visibility:visible; z-index:999; position:absolute; left:-500px; top:0px;">
</iframe>
    </body>
</html>

