<%@page contentType="text/html"%>
<%@page session="true"%>
<%@page import="java.util.*"%>
<%@page import="java.text.*"%>
<%@page import="com.tsp.operation.model.*"%>
<%@page import="com.tsp.operation.model.beans.*"%>
<%@page import="com.tsp.operation.controller.*"%>
<%@page import="com.tsp.util.*"%>
<%@page errorPage="/error/ErrorPage.jsp"%>
<%@include file="/WEB-INF/InitModel.jsp"%>
<%@ taglib uri="http://jakarta.apache.org/taglibs/input-0.90" prefix="input" %>
<%String path = application.getRealPath(model.menuService.getRealUrl(request.getRequestURI()));
  String datos[] = model.menuService.getContenidoMenu(BASEURL,request.getRequestURI(),path);
%>
<html>
<head>
</script>
<title>Asignar Cliente</title>
<link href="<%= BASEURL %>/css/estilostsp.css" rel='stylesheet'>
<link href="../../../../css/estilostsp.css" rel="stylesheet" type="text/css">
<link href="/css/estilostsp.css" rel="stylesheet" type="text/css">
<style type="text/css">
<!--
.style4 {font-size: 10px; color: #333333; }
-->
</style>
</head>
<script type='text/javascript' src="<%= BASEURL %>/js/validar.js"></script>
<script src='<%= BASEURL %>/js/date-picker.js'></script>
<script type='text/javascript' src="<%= BASEURL %>/js/boton.js"></script>
<script src='<%=BASEURL%>/js/tools.js' language='javascript'></script>
<script>
function validarForma(){
	for (i = 0; i < forma.elements.length; i++){
			if (forma.elements[i].value == "..." && forma.elements[i].name=="clientes"){
                    forma.elements[i].focus();
                    alert('No se puede procesar la información. Campo Cliente Vacio.');
                    return false;	
            }
			if (forma.elements[i].value == "..." && forma.elements[i].name=="proveedor"){
                    forma.elements[i].focus();
                    alert('No se puede procesar la información. Campo Afiliado Vacio.');
                    return false;	
            }

	}
    return true;
}

function validarForma2(){
	for (i = 0; i < forma.elements.length; i++){
			if (forma.elements[i].value == "..." && forma.elements[i].name=="clientes"){
                    forma.elements[i].focus();
                    alert('No se puede procesar la información. Campo Cliente Vacio.');
                    return false;	
            }
			if (forma.elements[i].value == "..." && forma.elements[i].name=="proveedor"){
                    forma.elements[i].focus();
                    alert('No se puede procesar la información. Campo Afiliado Vacio.');
                    return false;	
            }
	}
	if (window.confirm("Seguro de desea continuar?")==true)
	{
   		 window.location='<%=CONTROLLER%>?estado=Negocios&accion=Update&op=4&clientes='+forma.clientes.value+'&proveedor='+forma.proveedor.value;
	}
}
</script>
<body onresize="redimensionar()" onload = 'redimensionar()'>
<div id="capaSuperior" style="position:absolute; width:100%; height:110px; z-index:0; left: 0px; top: 0px;">
 <jsp:include page="/toptsp.jsp?encabezado=Asignar Clientes"/>
</div>
<div id="capaCentral" style="position:absolute; width:100%; height:87%; z-index:0; left: 0px; top: 109px; overflow: scroll;">
<%
	TreeMap pr= model.Negociossvc.getClientes();
	pr.put(" Seleccione","..."); 
	TreeMap pro= model.Negociossvc.getProv();
	pro.put(" Seleccione","..."); 
%>
<form name="forma" action='<%=CONTROLLER%>?estado=Negocios&accion=Update&op=3' id="forma" method="post">
  <table width="596"  border="2" align="center" cellpadding="0" cellspacing="0">
    <tr>
      <td>
	  <table width="100%" class="tablaInferior">
        <tr>
          <td colspan="2" ><table width="100%"  border="0" cellpadding="0" cellspacing="1" class="barratitulo">
              <tr>
                <td width="56%" class="subtitulo1">&nbsp;Detalles</td>
                <td width="44%" class="barratitulo"><img align="left" src="<%=BASEURL%>/images/titulo.gif" width="32" height="20"><%=datos[0]%></td>
              </tr>
          </td>
		</tr> 
	  </table>
        <tr class="fila">
          <td>Cliente</td>
          <td nowrap>
		  <table width="100%"  border="0" class="tablaInferior">
              <tr>
                <td><input name="text" type="text" class="textbox" id="campo" style="width:200;" onKeyUp="buscar(document.forma.clientes,this)" size="15" >
                  <span class="style4"> Escriba el nombre del Cliente</span></td>
              </tr>
              <tr>
                <td><input:select name="clientes" attributesText="class=textbox" options="<%=pr %>"/>&nbsp;</td>
              </tr>
          </table>
		  </td>
        </tr>
		
		<tr class="fila">
          <td>Afiliado</td>
          <td nowrap>
		  <table width="100%"  border="0" class="tablaInferior">
              <tr>
                <td><input name="text" type="text" class="textbox" id="campo" style="width:200;" onKeyUp="buscar(document.forma.proveedor,this)" size="15" >
                  <span class="style4">Escriba el nombre del Afiliado</span></td>
              </tr>
              <tr>
                <td><input:select name="proveedor" attributesText="class=textbox" options="<%=pro %>"/>&nbsp;</td>
              </tr>
          </table>
		  </td>
        </tr>
  </table>

  <center>
    <p><img src="<%=BASEURL%>/images/botones/aceptar.gif" name="c_aceptar" onClick="if (validarForma()) forma.submit(); " onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" style="cursor:hand"> 
        <img src="<%=BASEURL%>/images/botones/anular.gif"  name="imgdetalles"  align="absmiddle" onMouseOver="botonOver(this);" onClick="validarForma2()" onMouseOut="botonOut(this);" style="cursor:hand">
	    <img src="<%=BASEURL%>/images/botones/cancelar.gif" name="c_cancelar" onClick="forma.reset();" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" style="cursor:hand"> 
        <img src="<%=BASEURL%>/images/botones/salir.gif" name="c_salir" onClick="parent.close();" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" style="cursor:hand">
</p>
    <p><a href="<%=BASEURL%>/jsp/fenalco/avales/AfiliadosListar.jsp">Ver Todos</a>     </p>
  </center>
</form>
<% if( request.getParameter("msg") != null ){%>
  <p><table border="2" align="center">
  <tr>
    <td><table width="100%"  border="1" align="center"  bordercolor="#F7F5F4" bgcolor="#FFFFFF">
      <tr>
        <td width="363" align="center" class="mensajes"><%=request.getParameter("msg").toString()%></td>
        <td width="29" background="<%=BASEURL%>/images/cuadronaranja.JPG">&nbsp;</td>
        <td width="58">&nbsp;</td>
      </tr>
    </table></td>
  </tr>
</table>
</p>
 <%} %>
</div>
</body>
</html>
