<%-- 
    Document   : solicitud_est
    Created on : 15/11/2012, 12:17:06 PM
    Author     : jpinedo
--%>

<%@page contentType="text/html" pageEncoding="ISO-8859-1"%>
<%@page import="com.tsp.util.Util"%>
<%@page import="java.text.SimpleDateFormat"%>
<%@page import="java.text.DateFormat"%>
<%@page session="true"%>
<%@page import="java.util.*"%>
<%@page import="com.tsp.operation.model.*"%>
<%@page import="com.tsp.operation.model.beans.*"%>
<%@page import="com.tsp.operation.model.services.*"%>
<%@include file="/WEB-INF/InitModel.jsp"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
   "http://www.w3.org/TR/html4/loose.dtd">

<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
        <title>JSP Page</title>
    </head>

    <%
    response.addHeader("Cache-Control","no-cache");
    response.addHeader("Pragma","No-cache");
    response.addDateHeader ("Expires", 0);
    String tipoconv =  "Consumo";
    String vista = request.getParameter("vista")!=null ? request.getParameter("vista") : "1";
    String opcion = request.getParameter("opcion")!=null ? request.getParameter("opcion") : "1";
    String identificacion = request.getParameter("identificacion")!=null ? request.getParameter("identificacion") : "1";
    Usuario usuario = (Usuario) session.getAttribute("Usuario");

    GestionSolicitudAvalService gservice = new GestionSolicitudAvalService(usuario.getBd());
    String numero_solicitud=gservice.getUltimaSolicitud(usuario.getCedula());

   %>
 <script>
   <%if(opcion.equals("1")){%>
  
       <%if(gservice.ValidarSolicitudCliente(usuario.getCedula())){%>
     location.href="<%=BASEURL%>/jsp/fenalco/avales/solicitud_aval.jsp?num_solicitud=<%=numero_solicitud%>&vista=20&tipoconv=Consumo";
     <%}else
         {%>
             alert("Usted tiene solicitudes en tramite.")
             window.close();
             <%}%>
  
   <%}%>
   
    </script>
   <script>
       <%if (opcion.equals("2")) {
              
               //validar que exita en la bd de preaprobados
               if (gservice.ValidarClientePreaprobado(identificacion)) {
                      
                   // validar si no tiene en tramite
                   if (gservice.ValidarSolicitudCliente(identificacion)) {
                       numero_solicitud = gservice.getUltimaSolicitud(identificacion);
                       if (numero_solicitud != null) {
       %>
                                    location.href="<%=BASEURL%>/jsp/fenalco/avales/solicitud_aval.jsp?num_solicitud=<%=numero_solicitud%>&vista=20&tipoconv=Consumo";
       <%
                                 }else{
                                 %>
                                     alert("No se encontro el numero de formulario.");
                                     location.href="<%=BASEURL%>/jsp/fenalco/avales/consultarPreaprobado.jsp";
                                 <%
                                 }
                                     
                             } else {%>
                                 alert("tiene solicitudes en tramite.")
                                 location.href="<%=BASEURL%>/jsp/fenalco/avales/consultarPreaprobado.jsp";
       <%}
                                       } else {%>
                                           alert("No presenta credito preaprobado.")
                                          location.href="<%=BASEURL%>/jsp/fenalco/avales/consultarPreaprobado.jsp";
       <%}
                                  }%>



   
     </script>
                     <body></body>
           
</html>
