<!--
- Autor : Ing. Roberto Rocha	
- Date  : 16 de Julio de 2007
- Copyrigth Notice : Fintravalores S.A. S.A
-->
<%--
-@(#)
--Descripcion : Pagina JSP, que maneja la busqueda de las condicones de los negocios.
--%>

<%-- Declaracion de librerias--%>
<%@page contentType="text/html"%>
<%@ page session="true"%>
<%@ page errorPage="../error/error.jsp"%>
<%@page import="com.tsp.operation.model.beans.*, java.util.*"%>
<%@ include file="/WEB-INF/InitModel.jsp"%>
<%@page import="com.tsp.operation.model.*"%>
<%@page import="com.tsp.util.*"%>
<%@ taglib uri="http://jakarta.apache.org/taglibs/input-0.90" prefix="input" %>
<html>
<head><title>Buscar Proveedor</title>
<%String path = application.getRealPath(model.menuService.getRealUrl(request.getRequestURI()));
  String datos[] = model.menuService.getContenidoMenu(BASEURL,request.getRequestURI(),path);
%>

<link href="<%= BASEURL %>/css/estilostsp.css" rel="stylesheet" type="text/css">
<script type='text/javascript' src="<%= BASEURL %>/js/boton.js"></script>
<script type='text/javascript' src="<%= BASEURL %>/js/general.js"></script>
</head>
<body onLoad="redimensionar();doFieldFocus( form2 );" onResize="redimensionar();">
<div id="capaSuperior" style="position:absolute; width:100%; height:100px; z-index:0; left: 0px; top: 0px;">
<jsp:include page="/WEB-INF/toptsp.jsp?encabezado=Consultar Condiciones"/>
</div>

<div id="capaCentral" style="position:absolute; width:100%; height:85%; z-index:0; left: 0px; top: 100px; overflow: scroll;"> 
<form name="form2" method="post" action="<%=CONTROLLER%>?estado=Consultar&accion=Avales&op=2">
    <table width="380" border="2" align="center">
      <tr>
        <td><table width="100%" align="center"  class="tablaInferior">
			<tr>
				<td width="173" class="subtitulo1">Consultar Condiciones </td>
				<td width="205" class="barratitulo"><img src="<%=BASEURL%>/images/titulo.gif" width="32" height="20" align="left"><%=datos[0]%></td>
			</tr>
            <tr class="fila">
              <td width="173" align="left" >Documento</td>
              <td width="205"><input name="c_nit" type="text" class="textbox" id="c_codigo" onKeyPress="soloDigitos(event,'decNO')" size="15" maxlength="15"></td>
            </tr>
        </table></td>
      </tr>
    </table>
<p>
<div align="center"><img src="<%=BASEURL%>/images/botones/buscar.gif" style="cursor:hand" title="Buscar Proveedor" name="buscar"  onClick="form2.submit();" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);">&nbsp;
<img src="<%=BASEURL%>/images/botones/salir.gif"  name="regresar" style="cursor:hand" title="Salir al Menu Principal" onMouseOver="botonOver(this);" onClick="parent.close();" onMouseOut="botonOut(this);" ></div>
</p>		
</form>
</div>
<%=datos[1]%>
</body>
</html>
