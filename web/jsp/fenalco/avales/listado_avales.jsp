<!--
* Autor : Ing. Roberto Rocha P..
* Date  : 10 de Julio de 2007
* Copyrigth Notice : Fintravalores S.A. S.A
* Version 1.0
-->
<%--
-@(#)
--Descripcion : Pagina JSP, que maneja los avales de Fenalco

--%>

<%-- Declaracion de librerias--%>
<%@page contentType="text/html;"%>
<%@page session="true"%>
<%@page import="java.util.*"%>
<%@page import="com.tsp.operation.model.*"%> 
<%@page import="com.tsp.operation.model.beans.*"%>
<%@page import="com.tsp.util.*"%>
<%@page import="com.tsp.util.Util.*"%>
<%@include file="/WEB-INF/InitModel.jsp"%>
<%@page    errorPage ="/error/ErrorPage.jsp"%>
<%@ taglib uri="http://jakarta.apache.org/taglibs/input-0.90" prefix="input" %>
<html>
<head>
<title>Avales de Fenalco</title>
<link href="../../../css/estilostsp.css" rel="stylesheet" type="text/css"> 
<link href="<%=BASEURL%>/css/estilostsp.css" rel="stylesheet" type="text/css"> 
</head>

<script type='text/javascript' src="<%=BASEURL%>/js/Validacion.js"></script>
<script type='text/javascript' src="<%= BASEURL %>/js/boton.js"></script>
<script type='text/javascript' src="<%= BASEURL %>/js/general.js"></script>
<script src='<%=BASEURL%>/js/date-picker.js'></script>
<script src="<%= BASEURL %>/js/transferencias.js"></script>

<script language="Javascript">
function imprSelec(nombre)
{
  var ficha = document.getElementById(nombre);
  var ventimp = window.open(' ', 'popimpr');
  ventimp.document.write( ficha.innerHTML );
  ventimp.document.close();
  ventimp.print( );
  ventimp.close();
} 
</script> 
<%String op="2";
 Usuario usuario = null;
 usuario = (Usuario) session.getAttribute("Usuario"); %>
<body > 
<div id="capaSuperior" style="position:absolute; width:100%; height:100px; z-index:0; left: 0px; top: 0px;" >
  <p>
    <jsp:include page="/WEB-INF/toptsp.jsp?encabezado=Avales de Fenalco"/>
  </p>
</div>
<div id="capaCentral" style="position:absolute; width:100%; height:85%; z-index:0; left: 0px; top: 100px; overflow: scroll;"> 
<form name="form1" >
<DIV ID="seleccion">
<a  href="javascript:imprSelec('seleccion')" ><img src="<%=BASEURL%>/images/imprime.gif" width="70" height="19" style="cursor:hand"></a>
<%
model.Avales.listado("",usuario.getCedula());
Vector af =  model.Avales.getListado(); 
	        if(af.size() > 0)
			{%>
 		<table width="50%" cellpadding='0' cellspacing='0' class="tablaInferior" align="center" border="1">
            <tr class="fila"> 
                <td>
                  <p align='center'><b>No Cheques </b></p>                                
                </td>
				<td>
                  <p align='center'><b>Cheque 30</b></p>                                
                </td>
				<td>
                  <p align='center'><b>Cheque 45</b></p>                                
                </td>
				<td>
                  <p align='center'><b>Letra 30</b></p>                                
                </td>
				<td>
                  <p align='center'><b>Letra 45</b></p>                                
                </td>
           </tr> 
           <% 
	       for (int i=0; i<af.size(); i++)
                {
                      Aval_Fenalco  aval = (Aval_Fenalco)af.get(i);
                      if (i % 2 == 0)
                          {%> <tr class="filagris"><%}
                      else
                          {%> <tr class="filaazul"><%}%>
                <td>
                  <p align='center'><%=aval.getNodoc()%> </p>                                
                </td>
				<td>
                  <p align='center'><%=aval.getTc30()%> %</p>                                
                </td>
				<td>
                  <p align='center'><%=aval.getTc45()%> %</p>                                
                </td>
				<td>
                  <p align='center'><%=aval.getTl30()%> %</p>                                
                </td>
				<td>
                  <p align='center'><%=aval.getTl45()%> %</p>                                
                </td>
           </tr>
            <%}%>
		</table>
        <p>
 	<%} else {%>
 <table border="2" align="center">
    <tr>
      <td><table width="410" height="73" border="1" align="center"  bordercolor="#F7F5F4" bgcolor="#FFFFFF">
          <tr>
            <td width="282" align="center" class="mensajes">Su b&uacute;squeda no arroj&oacute; resultados!</td>
            <td width="28" background="<%=BASEURL%>/images/cuadronaranja.JPG">&nbsp;</td>
            <td width="78">&nbsp;</td>
          </tr>
      </table></td>
    </tr>
  </table>
	<%}%>   
</DIV>	   
  </p>
 <table width="500" border="0" align="center" cellpadding="0" cellspacing="0">
  <tr align="left">
    <td>
	  <div align="center">
	  <%if (op.equals("2")){%>
	  		<img src="<%=BASEURL%>/images/botones/regresar.gif" name="c_regresar" id="c_regresar" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" onClick="window.location='<%=BASEURL%>/jsp/fenalco/avales/inicio_aval.jsp'" style="cursor:hand "> 
	  <%}else{%>
	  		<img src="<%=BASEURL%>/images/botones/regresar.gif" name="c_regresar" id="c_regresar" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" onClick="window.location='<%=BASEURL%>/jsp/fenalco/avales/inicio_avales.jsp'" style="cursor:hand "> 
	  <%}%>
	  <img src="<%=BASEURL%>/images/botones/salir.gif"  name="imgsalir"  onMouseOver="botonOver(this);" onClick="parent.close();" onMouseOut="botonOut(this);" style="cursor:hand">
      </div>
    </tr>
</table>
</form>
</div>
</body>
</html>


