<%-- 
    Document   : presolicitud
    Created on : 10/08/2018, 07:58:09 AM
    Author     : jbermudez
--%>
<%@page contentType="text/html" pageEncoding="ISO-8859-1"%>
<%@page import="com.tsp.operation.model.beans.Usuario"%>
<%  response.addHeader("Cache-Control", "no-cache");
    response.addHeader("Pragma", "No-cache");
    response.addDateHeader("Expires", 0);
    Usuario user = (Usuario) session.getAttribute("Usuario");
%>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="ISO-8859-1">
        <title>Presolicitud Microcr�dito</title>
        <link type="text/css" rel="stylesheet" href="./css/jquery/jquery-ui/jquery-ui.css"/>
        <!--<link type="text/css" rel="stylesheet" href="./css/jquery/jquery.jqGrid/ui.jqgrid.css"/>-->        
        <script type="text/javascript" src="./js/jquery/jquery-ui/jquery.min.js"></script>
        <script type="text/javascript" src="./js/jquery/jquery-ui/jquery.ui.min.js"></script>            
        <!--<script type="text/javascript" src="./js/jquery/jquery.jqGrid/js/grid.locale-es.js"></script>-->
        <!--<script type="text/javascript" src="./js/jquery/jquery.jqGrid/js/jquery.jqGrid.min.js"></script>-->
        <!--<link href="./css/style_azul.css" rel="stylesheet" type="text/css">-->
        <link href="./css/presolicitud.css" rel="stylesheet" type="text/css">
    </head>
    <body>
        <div id="capaSuperior" style="width:100%; height:100px; z-index:0; left: 0px; top: 0px;">
            <jsp:include page="/toptsp.jsp?encabezado=Presolicitud Microcr�dito"/>
        </div>
        <section id="formulario">
            <section class="section-wrapper ui-widget-content ui-corner-all" id="form-simulador">
                <div class="header ui-dialog-titlebar ui-widget-header ui-corner-all ui-helper-clearfix" style="text-align: center;">Solicitud</div>
                <div class="formulario">
                    <div class="form-elementos">
                        <label for="dpto">Departamentos</label>
                        <select id="dpto"></select>
                    </div>
                    <div class="form-elementos">
                        <label for="ciudad">Ciudad</label>
                        <select id="ciudad"></select>
                    </div>
                    <div class="form-elementos">
                        <label for="fpp">Fecha primer pago</label>
                        <select type="date" id="fpp"></select>
                    </div>
                    <div class="form-elementos">
                        <label for="valors">Valor solicitado</label>
                        <input type="number" id="valors" value="0">
                    </div>
                    <div class="form-elementos">
                        <label for="plazo">Plazo en meses</label>
                        <select id="plazo">
                            <option value="6">6</option>
                            <option value="7">7</option>
                            <option value="8">8</option>
                            <option value="9">9</option>
                            <option value="10">10</option>
                            <option value="11">11</option>
                            <option value="12">12</option>
                            <option value="13">13</option>
                            <option value="14">14</option>
                            <option value="15">15</option>
                            <option value="16">16</option>
                            <option value="17">17</option>
                            <option value="18">18</option>
                            <option value="19">19</option>
                            <option value="20">20</option>
                            <option value="21">21</option>
                            <option value="22">22</option>
                            <option value="23">23</option>
                            <option value="24">24</option>
                            <option value="30">30</option>
                            <option value="36">36</option>
                        </select>
                    </div>
                    <div class="form-elementos">
                        <label for="compra_cartera">Compra cartera</label>
                        <select id="compra_cartera">
                            <option value="N">No</option>
                            <option value="S">Si</option>
                        </select>
                    </div>
                    <div class="form-elementos" id="cuota-wrapper">
                        <p>Cuota aproximada de </p>
                        <p><output id="vcuota"></output></p>
                    </div>   
                    <div class="form-elementos">
                        <button id="siguiente" type="button" class="ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only">
                            <span class="ui-button-text">Siguiente</span>
                        </button>
                    </div>
                </div>
            </section>
            <section class="section-wrapper ui-widget-content ui-corner-all" id="form-solicitante">
                <div class="header ui-dialog-titlebar ui-widget-header ui-corner-all ui-helper-clearfix" style="text-align: center;">Solicitante</div>
                <div class="formulario">
                    <div class="form-elementos">
                        <label for="nombre">Primer nombre</label>
                        <input type="text" id="nombre" disabled="true" required>
                    </div>
                    <div class="form-elementos">
                        <label for="apellido">Primer apellido</label>
                        <input type="text" id="apellido" disabled="true" required>
                    </div>
                    <div class="form-elementos">
                        <label for="ti">Tipo identificacion</label>
                        <select id="ti" disabled="true" required>
                            <option value="CED">C�dula de ciudadan�a</option>
                            <option value="CEX">Cedula de extranjer�a</option>
                        </select>
                    </div>
                    <div class="form-elementos">
                        <label for="identificacion">Identificacion</label>
                        <input type="number" id="identificacion" disabled="true" required>
                    </div>
                    <div class="form-elementos">
                        <label for="fexp">Fecha de expedici�n</label>
                        <input type="date" id="fexp" disabled="true" onkeydown="return false" accept="" required>
                    </div>
                    <div class="form-elementos">
                        <label for="fnac">Fecha de nacimiento</label>
                        <input type="date" id="fnac" disabled="true" onkeydown="return false" >
                    </div>
                    <div class="form-elementos">
                        <label for="email">Correo electr�nico</label>
                        <input type="email" id="email" disabled="true" required>
                    </div>
                    <div class="form-elementos">
                        <label for="celular">Celular</label>
                        <input type="number" id="celular" disabled="true" required>
                    </div>
                    <div class="form-elementos">
                        <label for="celular">Tipo de cr�dito</label>                        
                        <select id="tipo_credito" disabled="true" required>
                            <option value="">Selecciona una opci�n</option>
                            <option value="NUE">Nuevo</option>
                            <option value="PRE">Preaprobado</option>
                            <option value="PAR">Paralelo</option>
                            <option value="REN">Renovaci�n</option>
                            <option value="CDP">Compra de cartera</option>
                        </select>
                    </div>
                    <div class="form-elementos">
                        <button id="solicitar" type="button" class="ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only" disabled="true">
                            <span class="ui-button-text">Solicitar cr�dito</span>
                        </button>
                    </div>
                </div>
            </section>
        </section>
        <div id="tWrapper" class="ventana">
            <table id="tNegocios">
                <thead>
                <th>NEGOCIO</th>
                <th>NOMBRE</th>
                <th>VALOR</th>
                <th>SALDO ACTUAL</th>
                <th>ESTADO</th>
                <th></th>
                </thead>
                <tbody></tbody>
            </table>
            <table id="tLiquidacion">
                <tbody>
                    <tr>
                        <th>Plazo</th>
                        <td id="plazo-cell"></td>
                    </tr>
                    <tr>
                        <th>Fecha de pago</th>
                        <td id="fpp-cell"></td>
                    </tr>
                    <tr>
                        <th>Valor solicitado</th>
                        <td id="valors-cell"></td>
                    </tr>
                    <tr>
                        <th>Saldo cr�dito actual</th>
                        <td id="saldo-cell" class="red-text"></td>
                    </tr>
                    <tr>
                        <th>Descuento aval aprox.</th>
                        <td id="dsto-aval-cell" class="red-text"></td>
                    </tr>
                    <tr>
                        <th>Descuento estudio de cr�dito aprox.</th>
                        <td id="dsto-estudio-cell" class="red-text"></td>
                    </tr>
                    <tr>
                        <th>Descuento comisi�n desembolso aprox.</th>
                        <td id="dsto-comision-cell" class="red-text"></td>
                    </tr>
                    <tr>
                        <th>Desembolso aproximado</th>
                        <td id="desembolso-cell"></td>
                    </tr>
                    <tr>
                        <th>Nuevo valor cuota mesual</th>
                        <td id="valor-cuota-cell"></td>
                    </tr>                    
                </tbody>
            </table>
        </div>
        <div id="info"  class="ventana" >
            <p id="notific"></p>
        </div>
        <div id="dialogLoading" class="ventana">
            <p  style="font-size: 12px;text-align:justify;" id="msj2">Texto </p> <br/>
            <center>
                <img src="./images/cargandoCM.gif"/>
            </center>
        </div>
        <script>
            const asesor = "<%=user.getLogin()%>";
            const TOKEN = "<%=user.getToken_api()%>";
        </script>
        <script type="text/javascript" src="./js/presolicitud.js"></script>
    </body>
</html>
