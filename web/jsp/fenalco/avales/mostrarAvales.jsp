<%--
    Document   : mostrarCuentasProveedor
    Created on : 15/11/2007, 03:58:14 PM
    Author     : iamorales
--%>

<%@ page session   ="true"%>
<%@ page errorPage ="/error/ErrorPage.jsp"%>
<%@ page import    ="java.util.*" %>
<%@ page import    ="com.tsp.util.*" %>

<%@ page import    ="com.tsp.operation.model.beans.*" %>
<%@ include file   ="/WEB-INF/InitModel.jsp"%>

<%
	String path = application.getRealPath(model.menuService.getRealUrl(request.getRequestURI()));
	String datos[] = model.menuService.getContenidoMenu(BASEURL,request.getRequestURI(),path);

	Usuario usuario = (Usuario) session.getAttribute("Usuario");
	String loginx=usuario.getLogin();

	 String fechaIni       =  request.getParameter("fechaInicio");
     String fechaFin       =  request.getParameter("fechaFinal");
%>
<html>
<head>
	<title>Avales</title>
    <link href="<%= BASEURL %>/css/estilostsp.css"     rel="stylesheet" type="text/css">
    <link href="/css/estilostsp.css" rel="stylesheet"  type="text/css">
    <script type='text/javascript' src="<%= BASEURL %>/js/boton.js"></script>
	<script>
	function validarSeleccion(){
		   var elementos = document.getElementsByName("idAval");

		   for (var i=0;i<elementos.length; i++){
		      if (elementos[i].checked){
			    return true;
                      }
		   }
		   alert ('Seleccione por lo menos un aval para iniciar el proceso de aprobaci�n.');
		   return false;
	}
	</script>

    <script>

		function send(theForm){
			if (validarSeleccion()){
				theForm.submit();
			}
		}

		function Sell_all_col(theForm,nombre){
			 for (i=0;i<theForm.length;i++)
					  if (theForm.elements[i].type=='checkbox' && theForm.elements[i].name==nombre)
						 theForm.elements[i].checked=theForm.All.checked;
	    }


    </script>
</head>
<body onLoad="redimensionar();" onResize="redimensionar();"><!--redimensionar-->

<div id="capaSuperior" style="position:absolute; width:100%; height:100px; z-index:0; left: 0px; top: 0px;">
<jsp:include page="/WEB-INF/toptsp.jsp?encabezado=Avales"/>
</div>

<div id="capaCentral" style="position:absolute; width:100%; height:85%; z-index:0; left: -1px; top: 100px; overflow: scroll;">



<%

   ArrayList avales ;
   try {
	   avales = model.avalesService.getAvales();
   }catch(Exception e ){
   		System.out.println("errorcillo en model.Service.getAvales()");
		avales=null;
   }
   if (avales!=null  ){
%>
    <!--<form action="<%//=CONTROLLER%>?estado=Anticipo&accion=Gasolina&opcion=facturarAnticipoGas" method="post" name="formulario">-->
    <%

    //String respuesta=request.getParameter("respuesta");
	//String valor_neto="";//request.getParameter("valor_neto");

    %>

    <form action="<%=CONTROLLER%>?estado=Gestion&accion=Avales&opcion=APROBARAVALES" method='post' name='formulario' >
	<input type="hidden" name="fechaInicio" value="<%=fechaIni%>">
	<input type="hidden" name="fechaFinal" value="<%=fechaFin%>">

    <table width="689"  border="2" align="center">
     <tr>
         <td >

                <table width="100%"  align="center">

                  <tr class='fila'>
                     <td>

							<table width="100%" border="1" align="center" bordercolor="#999999" bgcolor="#F7F5F4">
								<tr class="tblTitulo">


							 		<!--<th ><input type='checkbox'  id='All' onclick="Sell_all_col(formulario,'idAval');">   </th>-->
									<th>  NumAval  </th>
									<th>  Nit Afiliado  </th>
								 	 <th>  Nombre Afiliado  </th>
									<th>  Nombre Girador  </th>
									<th>  Num Doc  </th>
									<th>  Codigo Banco  </th>
									<th>  Numero Cuenta  </th>
									<th>  Titulo Valor  </th>
									<th>   Fecha Consignacion </th>
									<th>  Valor Titulo  </th>

									<%
									String ckMostrar=request.getParameter("ckMostrar");
									if (ckMostrar.equals("on")){
									%>
									<th > Seccional Afiliado  </th>
									<th > Consecutivo Afiliado</th>
									<th>  Sucursal Afiliado  </th>
									<th>  Nit Financiera </th>
									<th>  Codigo Servicio </th>


									<th>  Fecha Autorizacion  </th>
									<th>  Hora Autorizacion  </th>

									<th>  Tipo Doc  </th>


									<th>  Tel Girador  </th>

									<th>  Codigo Sucursal  </th>



									<th>  Cobertura Consulta  </th>
									<th>  Codigo Depto  </th>
									<th>  Codigo Ciudad  </th>
									<th>   Fecha Inicial </th>
									<th>  Fecha Final  </th>
									<th>   Fecha Proceso </th>
									<th>   Hora Proceso </th>
									<th>  Indicador Ratificado  </th>
									<th> Codigo Iata   </th>
									<th>  Compa�ia Vende  </th>
									<th>  Tipo Venta  </th>
									<th> Num Linea   </th>
									<%}%>
									<th> Fecha Sistema   </th>
									<th>  Usuario Sistema  </th>
									<th> RegStatus   </th>
									<th>   Archivo </th>
									<th>  Fecha Archivo  </th>
									<th>  Aprobaci�n  </th>
									<th>   Fecha Aprobacion </th>

                              </tr>
								<%

                                                                        for (int i=0; i<avales.size(); i++) {
																			Aval  ava=(Aval)avales.get(i);



                                                                        %>

								<tr class='<%= (i%2==0?"filagris":"filaazul") %>' id='fila<%=i%>'   style=" font size:12"   >

																				<!--<td class="bordereporte" nowrap align="center"><input type='checkbox' name='idAval' value='<%=ava.getNum()%>' >   </td>-->

 																				<td class="bordereporte" nowrap align="center"> <% if (ava.getNumAval().length()>=8){
                                                                                                                                                                                                                        out.print((ava.getNumAval()).substring(1,8));
                                                                                                                                                                                                                   }else{
                                                                                                                                                                                                                        out.print( ava.getNumAval());
                                                                                                                                                                                                                   }
                                                                                                                                                                                                               %></td>
																				<td class="bordereporte" nowrap align="center"> <%= ava.getNitAfiliado()  %></td>
																				<td class="bordereporte" nowrap align="center"> <%= ava.getNombreAfiliado()  %></td>
																				<td class="bordereporte" nowrap align="center"> <%= ava.getNombreGirador()  %></td>
																				<td class="bordereporte" nowrap align="center"> <%= ava.getNumDoc()  %></td>
																				<td class="bordereporte" nowrap align="center"> <%= ava.getCodigoBanco()  %></td>
																				<td class="bordereporte" nowrap align="center"> <%= ava.getNumeroCuenta()  %></td>
																				<td class="bordereporte" nowrap align="center"> <%= ava.getTituloValor()  %></td>
																				<td class="bordereporte" nowrap align="center"> <%= ava.getFechaConsignacion()  %></td>
																				<td class="bordereporte" nowrap align="center"> <%= ava.getValorTitulo()  %></td>


																				<%

																				if (ckMostrar.equals("on")){%>
                                                                                <td class="bordereporte" nowrap align="center"> <%= ava.getSeccionalAfiliado() %></td>
																				<td class="bordereporte" nowrap align="center"> <%= ava.getConsecutivoAfiliado() %></td>

																				<td class="bordereporte" nowrap align="center"> <%= ava.getSucursalAfiliado() %></td>
																				<td class="bordereporte" nowrap align="center"> <%= ava.getNitFinanciera() %></td>
																				<td class="bordereporte" nowrap align="center"> <%= ava.getCodigoServicio()  %></td>


																				<td class="bordereporte" nowrap align="center"> <%= ava.getFechaAutorizacion()  %></td>
																				<td class="bordereporte" nowrap align="center"> <%= ava.getHoraAutorizacion()  %></td>

																				<td class="bordereporte" nowrap align="center"> <%= ava.getTipoDoc()  %></td>


																				<td class="bordereporte" nowrap align="center"> <%= ava.getTelGirador()  %></td>

																				<td class="bordereporte" nowrap align="center"> <%= ava.getCodigoSucursal()  %></td>



																				<td class="bordereporte" nowrap align="center"> <%= ava.getCoberturaConsulta()  %></td>
																				<td class="bordereporte" nowrap align="center"> <%= ava.getCodigoDepto()  %></td>
																				<td class="bordereporte" nowrap align="center"> <%= ava.getCodigoCiudad()  %></td>
																				<td class="bordereporte" nowrap align="center"> <%= ava.getFechaInicial()  %></td>
																				<td class="bordereporte" nowrap align="center"> <%= ava.getFechaFinal()  %></td>
																				<td class="bordereporte" nowrap align="center"> <%= ava.getFechaProceso()  %></td>
																				<td class="bordereporte" nowrap align="center"> <%= ava.getHoraProceso()  %></td>
																				<td class="bordereporte" nowrap align="center"> <%= ava.getIndicadorRatificado()  %></td>
																				<td class="bordereporte" nowrap align="center"> <%= ava.getCodigoIata()  %></td>
																				<td class="bordereporte" nowrap align="center"> <%= ava.getCompa�iaVende()  %></td>
																				<td class="bordereporte" nowrap align="center"> <%= ava.getTipoVenta()  %></td>
																				<td class="bordereporte" nowrap align="center"> <%= ava.getNumLinea()  %></td>
																				<%}%>
																				<td class="bordereporte" nowrap align="center"> <%= ava.getFechaSistema()  %></td>
																				<td class="bordereporte" nowrap align="center"> <%= ava.getUsuarioSistema()  %></td>
																				<td class="bordereporte" nowrap align="center"> <%= ava.getRegStatus()  %></td>
																				<td class="bordereporte" nowrap align="center"> <%= ava.getArchivo()  %></td>
																			    <td class="bordereporte" nowrap align="center"> <%= ava.getFechaArchivo() %></td>
																				<%
																				String aprobacion=ava.getAprobado();
																				if (aprobacion.equals("S")){
																					aprobacion="Aprobado";
																				}else{
																					aprobacion="No";
																				}
																				%>
																				<td class="bordereporte" nowrap align="center"> <%= aprobacion%></td>
																				<td class="bordereporte" nowrap align="center"> <%= ava.getFechaAprobacion()  %></td>


                              </tr>




								<% } %>



							</table>

					 </td>
                  </tr>

                </table>

        </td>
      </tr>
      </table>
      </form>
        <br>
        <center>

		<!--<img src='<%//=BASEURL%>/images/botones/regresar.gif'   style=" cursor:hand"  title='Regresar...'     name='i_regresar'       onclick="document.location = '<%//= BASEURL %>/jsp/gasolina/filtro_consulta_anticipos_gasolina_estacion.jsp' "     onMouseOver='botonOver(this);' onMouseOut='botonOut(this);'> -->
		<!--<img src="<%=BASEURL%>/images/botones/aceptar.gif"    height="21"  title='Aceptar'  onclick='send(formulario);'   onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" style="cursor:hand">-->
        <img src='<%=BASEURL%>/images/botones/salir.gif'      style='cursor:hand'    title='Salir...'      name='i_salir'       onclick='parent.close();'           onMouseOver='botonOver(this);' onMouseOut='botonOut(this);'>
		</center>
	     <br>

 <!--</form>-->
 <br>

    <% }else{
    %>lista de avales vac�a. raro....<%
   }
%>
<%
String respuesta=request.getParameter("respuesta");
       if (respuesta!=null){//request.getParameter("resultado")!=null && request.getParameter("resultado").equals("ok") ){
            %>
            <br><br>
            <p><table border="2" align="center">
              <tr>
                <td><table width="100%"  border="1" align="center"  bordercolor="#F7F5F4" bgcolor="#FFFFFF">
                  <tr>
                    <td width="170" align="center" class="mensajes"><%=""+respuesta%></td>
                    <td width="29" background="<%=BASEURL%>/images/cuadronaranja.JPG">&nbsp;</td>
                    <td width="40">&nbsp;</td>
                  </tr>
                </table></td>
              </tr>
            </table>
            </p>

            <%
        }

  %>

</div>
<%=datos[1]%>

</body>
</html>
