<%-- 
    Document   : asociarNomenclaturaCiudad
    Created on : 26/01/2016, 11:32:10 AM
    Author     : mcastillo
--%>

<%@page contentType="text/html" pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
    <head>
         <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
        <title>Nomenclaturas Direcci�n</title>
        <link type="text/css" rel="stylesheet" href="./css/jquery/jquery-ui/jquery-ui.css"/>
        <link type="text/css" rel="stylesheet" href="./css/jquery/jquery.jqGrid/ui.jqgrid.css"/>        
        <script type="text/javascript" src="./js/jquery/jquery-ui/jquery.min.js"></script>
        <script type="text/javascript" src="./js/jquery/jquery-ui/jquery.ui.min.js"></script>            
        <script type="text/javascript" src="./js/jquery/jquery.jqGrid/js/grid.locale-es.js"></script>
        <script type="text/javascript" src="./js/jquery/jquery.jqGrid/js/jquery.jqGrid.min.js"></script>
        <link type="text/css" rel="stylesheet" href="./css/buttons/botonVerde.css"/>
        <link type="text/css" rel="stylesheet" href="./css/popup.css"/>
        <link href="./css/style_azul.css" rel="stylesheet" type="text/css">
        <link href="./css/requisiciones.css" rel="stylesheet" type="text/css"/>
        <script type="text/javascript" src="./js/nomenclaturas.js"></script>   
    </head>
    <body>
       <div id="capaSuperior" style="position:absolute; width:100%; height:100px; z-index:0; left: 0px; top: 0px;">
             <jsp:include page="/toptsp.jsp?encabezado=Asociar Nomenclaturas a Ciudades"/>
        </div>
       <div id="capaCentral" style="position:absolute; width:100%; height:83%; z-index:0; left: 0px; top: 100px; overflow:auto;">   
          <center>              
            <div id="div_asoc_nomenc_ciudad"  style="width: 870px">
                <div id="filtros" class="ui-jqgrid ui-widget ui-widget-content ui-corner-all" style="height:580px;padding: 0px 10px 0px 0px;">
                    <div id="encabezadotablita" style="width: 865px">
                        <label class="titulotablita"><b>ASOCIAR NOMENCLATURAS Y CIUDADES</b></label>
                    </div> 
                    </br>
                    <table aling="center" style=" width: 100%;margin-left:30px" >
                        <tr>  
                            <td style="width: 10%"><span>Departamento:</span></td>
                            <td style="width: 30%"> <select name="departamento" style="font-size: 14px;width:225px" id="departamento">
                                </select></td>
                            <td style="width: 10%"><span>Ciudad</span></td>
                            <td style="width: 30%"> <select name="ciudad" style="font-size: 14px;width:225px" id="ciudad"> 
                                </select></td>
                            <td style="width: 20%">
                                <div id="bt_asignar_default" title="Asignar nomenclaturas por defecto a las ciudades del dpto seleccionado" style="display:none;"> 
                                    <span class="form-submit-button form-submit-button-simple_green_apple"> config. defecto </span>
                                </div>  
                            </td>    
                        </tr>            
                    </table>
                             
               <div id="bt_asociar_nomenclatura" title="Asignar nomenclatura a ciudad" style="display:none;position:absolute; left: 400px; top: 270px; width: 60px;"> 
                    <span aling="center"  class="form-submit-button form-submit-button-simple_green_apple" onClick="asociarNomenclatura();"> >> </span>
                </div>  
                <div id="bt_desasociar_nomenclatura" title="Desasignar nomenclatura de la ciudad" style="display:none;position:absolute; left: 400px; top: 320px; width: 60px;"> 
                    <span aling="center"  class="form-submit-button form-submit-button-simple_green_apple" onClick="desasociarNomenclatura();"> << </span>
                </div>  
                <div id="div_nomenc_ciudad" style="display:none;position: absolute; left: 510px; top: 100px; width: 300px ">    
                    <table border="0"  align="center">
                        <tr>
                            <td>
                                <table id="nomenc_ciudad" ></table>
                            </td>
                        </tr>
                    </table>   
                </div>
                <div id="div_nomenclaturas_list" style="display:none;position:absolute; left: 40px; top: 100px; width: 300px ">    
                    <table border="0"  align="center">
                        <tr>
                            <td>
                                <table id="listNomenclaturas" ></table>
                                <div id="page_tabla_nomenclatura"></div>
                            </td>
                        </tr>
                    </table> 
                </div>
             </div>  
         </div> 
        </center>
        <div id="dialogMsg" title="Mensaje" style="display:none;">
            <p style="font-size: 12.5px;text-align:justify;" id="msj" > Texto </p>
        </div>   
       </div>                      
       
    </body>
</html>
