
<%-- 
    Document   : CondicionesFenalco
    Created on : 7/07/2010, 10:42:50 AM
    Author     : maltamiranda
    Modified by : rhonalf
--%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@page session    ="true"%>
<%@page errorPage  ="/error/ErrorPage.jsp"%>
<%@page import    ="java.util.*" %>
<%@page import    ="com.tsp.util.*" %>
<%@page import="com.tsp.operation.model.beans.*"%>
<%@page import="com.tsp.operation.model.services.*"%>
<%
    response.addHeader("Cache-Control","no-cache");
    response.addHeader("Pragma","No-cache");
    response.addDateHeader ("Expires", 0);
    String BASEURL = request.getContextPath();
    String CONTROLLER = BASEURL + "/controller";
%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">

<%
    Usuario usuario = (Usuario) session.getAttribute("Usuario");
    String distrito = (String) session.getAttribute("Distrito");
    String msg = Util.coalesce((String) request.getAttribute("msg"), "");
    System.out.println("msg: "+msg);
    if(msg!=null && !msg.equals("")){
%>
<script type="text/javascript">
    alert("<%=msg%>");
</script>
<%
    }
    String op = request.getParameter("vista")!=null ? request.getParameter("vista") : "1";
    boolean adding = false;
    if(op.equals("0")) adding = true;
%>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
        <link href="<%= BASEURL%>/css/estilostsp.css" rel="stylesheet" type="text/css">
        <script src="<%=BASEURL%>/js/prototype.js" type="text/javascript"></script>
        <script src="<%=BASEURL%>/js/boton.js" type="text/javascript"></script>
        <!-- Las siguientes librerias CSS y JS son para el manejo de DIVS dinamicos.-->
        <link href="<%=BASEURL%>/css/default.css" rel="stylesheet" type="text/css">
        <link href="<%=BASEURL%>/css/mac_os_x.css" rel="stylesheet" type="text/css">
        <script src="<%=BASEURL%>/js/effects.js" type="text/javascript"></script>
        <script src="<%=BASEURL%>/js/window.js" type="text/javascript"></script>
        <title>Gestion de Condiciones de Aval</title>
        <script type="text/javascript">
            var items=1;
            function soloNumeros(id) {
                 var precio = document.getElementById(id).value;
                 precio =  precio.replace(/[^0-9^.]+/gi,"");
                 document.getElementById(id).value=precio;
            }
            function formatNumber(num,prefix){
                prefix = prefix || '';
                num += '';
                var splitStr = num.split('.');
                var splitLeft = splitStr[0];
                var splitRight = splitStr.length > 1 ? '.' + splitStr[1] : '';
                splitRight = splitRight.substring(0,3);//090929
                var regx = /(\d+)(\d{3})/;
                while (regx.test(splitLeft)) {
                    splitLeft = splitLeft.replace(regx, '$1' + ',' + '$2');
                }
                return prefix + splitLeft + splitRight;
            }

            function TDEliminarRegistro(imagen,tablaId){
                var i=imagen.parentNode.parentNode.rowIndex;
                $(tablaId).deleteRow(i);
            }

            function TDAgregarRegistro(tablaId,prefix){
                myTable = $(tablaId);
                if(navigator.appName=='Microsoft Internet Explorer')
                {   oRow = myTable.insertRow();
                }else{
                    oRow = myTable.insertRow(-1);
                }
                var val = (document.getElementById('rf'+prefix+items).value)*1 + 1;
                items = items + 1;
                if(navigator.appName=='Microsoft Internet Explorer'){oCell = oRow.insertCell();}else{oCell = oRow.insertCell(-1);}oCell.align='center';oCell.style.backgroundColor='#EAFFEA';
                oCell.innerHTML = '<img style="cursor: pointer" onclick="TDAgregarRegistro(\''+tablaId+'\',\''+prefix+'\')" src=\'<%=BASEURL%>/images/botones/iconos/mas.gif\' width=\'12\' height=\'12\'><img style="cursor: pointer" onclick="TDEliminarRegistro(this,\''+tablaId+'\')" src=\'<%=BASEURL%>/images/botones/iconos/menos1.gif\' width=\'12\' height=\'12\' >';
                if(navigator.appName=='Microsoft Internet Explorer'){oCell = oRow.insertCell();}else{oCell = oRow.insertCell(-1);}oCell.align='left';oCell.style.backgroundColor='#EAFFEA';
                oCell.innerHTML = '<input  onkeyup="soloNumeros(this.id);" type="text" id="ri'+prefix+items+'" name="ri'+prefix+items+'" value='+val+'>';
                if(navigator.appName=='Microsoft Internet Explorer'){oCell = oRow.insertCell();}else{oCell = oRow.insertCell(-1);}oCell.align='left';oCell.style.backgroundColor='#EAFFEA';
                oCell.innerHTML = '<input  onkeyup="soloNumeros(this.id);validateValueAnt('+items+');" onblur="validateValue('+items+');validateValueAnt('+items+');" type="text" id="rf'+prefix+items+'" name="rf'+prefix+items+'">';
                if(navigator.appName=='Microsoft Internet Explorer'){oCell = oRow.insertCell();}else{oCell = oRow.insertCell(-1);}oCell.align='left';oCell.style.backgroundColor='#EAFFEA';
                oCell.innerHTML = '<input onkeyup="soloNumeros(this.id);this.value = formatNumber(this.value);" type="text" id="\'p'+prefix+'\''+items+'" name="p'+prefix+items+'">';
            }

            function pulsar(e){
                var mykey;
                if(e.which) mykey = e.which;// si es moz/webkit
                else mykey = e.keyCode;// internet exploiter
                if(mykey == 13){
                    buscardatos();
                }
            }

            function buscardatos(){
                borrarSeleccion();
                var texto = document.getElementById("nafil").value;
                var selected = document.getElementById("filtro").selectedIndex;
                var filtro = document.getElementById("filtro").options[selected].value;
                var url = "<%= CONTROLLER%>?estado=Gestion&accion=Condiciones";
                var p =  'opcion=buscacli&texto='+texto+'&filtro='+filtro;
                new Ajax.Request(
                    url,
                    {
                        method: 'post',
                        parameters: p,
                        onLoading: loading,
                        onComplete: llenarDiv
                    }
                );
            }

            function loading(){
                document.getElementById("spangen").innerHTML = '<img alt="cargando" src="<%= BASEURL%>/images/cargando.gif" name="imgload">';
            }

            function llenarDiv(response){
                document.getElementById("spangen").innerHTML = "";
                document.getElementById("selgen0").innerHTML = '<select id="selgen" name="selgen" style="width: 100%;">'+response.responseText+'</select>';//<!-- 2010111 -->
            }

            function unformatNumber(num) {
                return num.replace(/([^0-9\.\-])/g,'')*1;
            }

            function verConvs(){
                var selected = document.getElementById("selgen").selectedIndex;
                var filtro = document.getElementById("selgen").options[selected].value;
                var url = "<%= CONTROLLER%>?estado=Gestion&accion=Condiciones";
                var p =  'opcion=busconv&nitprov='+filtro;
                new Ajax.Request(
                    url,
                    {
                        method: 'post',
                        parameters: p,
                        onLoading: loading,
                        onComplete: openWin
                    }
                );
            }
            var win;
            function openWin(response){
                win= new Window(
                {   id: "diving",
                    title: "Nuevo",
                    width:$('contenido').width,
                    height:$('contenido').heigth,
                    showEffectOptions: {duration:1},
                    hideEffectOptions: {duration:1},
                    destroyOnClose: true,
                    onClose:closewin,
                    resizable: false
                });
                win.setContent('contenido', true, true);
                win.show(true);
                win.setLocation(7, 7);
                //win.showCenter();
                $("spangen").innerHTML = "";
                $('contenido').innerHTML=response.responseText;
                $('contenido').style.display='block';
                $('contenido').style.visibility='visible';
                //$('contenido').style.overlay="auto";
            }

            function closewin(){
                $('contenido').style.display='none';
                $('contenido').style.visibility='hidden';
                $('contenido').innerHTML="";
            }

            function colocar(convenio, sector, subsector, nomConvenio, nomSector, nomsubsector, facturaTercero, nitTercero, idProvConvenio){
                document.getElementById("hidIdProvConvenio").value = idProvConvenio;
                document.getElementById("convtext").value = nomConvenio;
                document.getElementById("convid").value = convenio;
                document.getElementById("secttext").value = nomSector;
                document.getElementById("sectid").value = sector;
                document.getElementById("subsecttext").value = nomsubsector;
                document.getElementById("subsectid").value = subsector;
                document.getElementById("hidFacturaTercero").value = facturaTercero;
                if(facturaTercero=="t"){
                    $("trPropietario").show();
                }else{
                    $("trPropietario").hide();
                    $("propietario").value = "false";
                }
                win.close();
            }

            function cerrarDiv(){
                win.close();
            }

            function viewRanks(){
                var selected = document.getElementById("selgen").selectedIndex;
                var filtro = document.getElementById("selgen").options[selected].value;
                var url = "<%= CONTROLLER%>?estado=Gestion&accion=Condiciones";
                var p = 'opcion=buscran&conv='+$F("hidIdProvConvenio")+'&tipotit='+$F("tipotitulo")+'&plazoprimer='+$F("plazop")+'&prop='+$F("propietario")+'&vista=<%= op %>';
                new Ajax.Request(
                    url,
                    {
                        method: 'post',
                        parameters: p,
                        onLoading: loading,
                        onComplete: fillDiv
                    }
                );
            }

            function fillDiv(response){
                document.getElementById("spangen").innerHTML = "";
                document.getElementById("divrank").innerHTML = response.responseText;
                items = (document.getElementById("tavales").rows.length * 1) - 1;
            }

            function exportar(){
                alert('Inicia la exportacion');
                var selected = document.getElementById("selgen").selectedIndex;
                var filtro = document.getElementById("selgen").options[selected].value;
                var tipotit = document.getElementById("tipotitulo");
                var tipotittext = tipotit.options[tipotit.selectedIndex].text;
                var url = "<%= CONTROLLER%>?estado=Gestion&accion=Condiciones";
                var p = 'opcion=export&afil='+filtro+
                            '&conv='+document.getElementById("hidIdProvConvenio").value+
                            '&convtext='+document.getElementById("convtext").value+
                            '&tipotit='+document.getElementById("tipotitulo").value+
                            '&tipotittext='+tipotittext+
                            '&plazoprimer='+document.getElementById("plazop").value+
                            '&prop='+document.getElementById("propietario").value+
                            '&sect='+document.getElementById("sectid").value+
                            '&secttext='+document.getElementById("secttext").value+
                            '&subsect='+document.getElementById("subsectid").value;
                            '&subsecttext='+document.getElementById("subsecttext").value;
                new Ajax.Request(
                    url,
                    {
                        method: 'post',
                        parameters: p,
                        onLoading: loading,
                        onComplete: alertar
                    }
                );
            }

            function alertar(response){
                document.getElementById("spangen").innerHTML = "";
                alert(response.responseText);
            }

            function validateValue(id){
                var vali = document.getElementById("riavales"+id).value * 1;
                var valf = document.getElementById("rfavales"+id).value * 1;
                if(valf<=vali){
                    document.getElementById("rfavales"+id).value = vali + 1;
                }
            }

            function validateValueAnt(id){
                if((id*1)>1){
                    var vali = document.getElementById("rfavales"+((id*1) - 1)).value * 1;
                    var valf = document.getElementById("riavales"+id).value * 1;
                    if(valf<=vali){
                        document.getElementById("riavales"+id).value = vali + 1;
                    }
                }
            }

            function save(){
                if(document.getElementById("rfavales1").value!="" && document.getElementById("convtext").value){
                    var selected = document.getElementById("selgen").selectedIndex;
                    var filtro = document.getElementById("selgen").options[selected].value;
                    var action = '&opcion=update&filas='+items+'&afil='+filtro+
                                '&convid='+document.getElementById("hidIdProvConvenio").value+
                                '&hidIdProvConvenio='+document.getElementById("hidIdProvConvenio").value+
                                '&tipotit='+document.getElementById("tipotitulo").value+
                                '&plazoprimer='+document.getElementById("plazop").value+
                                '&prop='+document.getElementById("propietario").value+
                                '&sect='+document.getElementById("secttext").value+
                                '&subsect='+document.getElementById("subsecttext").value;
                    document.getElementById("formulario").action = document.getElementById("formulario").action + action;
                    document.getElementById("formulario").submit();
                }
                else{
                    alert("Debe llenar los datos requeridos");
                }
            }

            function borrarSeleccion(){
                document.getElementById("convtext").value = "";
                document.getElementById("convid").value = "";
                document.getElementById("secttext").value = "";
                document.getElementById("sectid").value = "";
                document.getElementById("subsecttext").value = "";
                document.getElementById("subsectid").value = "";
                document.getElementById("hidFacturaTercero").value = "";
            }
        </script>
    </head>
    <body>
        <div id="capaSuperior" style="position:absolute; width:100%; height:100px; z-index:0; left: 0px; top: 0px;">
            <jsp:include page="/WEB-INF/toptsp.jsp?encabezado=Gestion de Condiciones Fenalco"/>
        </div>
        <div id="capaCentral" style="position:absolute; width:100%; height:85%; z-index:0; left: 0px; top: 100px; overflow: auto;">
          <br>
          
          <div id="contenido" style="visibility: hidden; display: none; width: 400px; height: 400px; background-color: #EEEEEE;"></div>
            <center>
                <table border="2">
                    <tr>
                        <td width="450">
                            <table class="tablaInferior" border="0" width="100%">
                                <tr>
                                    <td width="35%" align="left" class="subtitulo1">&nbsp;Condiciones Aval</td>
                                    <td width="65%" align="left" class="barratitulo">
                                        <img alt="" src="<%= BASEURL%>/images/titulo.gif" width="32" height="20"  align="left">
                                    </td>
                                </tr>
                            </table>
                            <table border="0" width="100%">
                                <tr>
                                    <td align="left" width="35%" class="fila">&nbsp;Afiliado:</td>
                                    <td align="left" width="65%" class="fila">
                                        <select id="filtro" name="filtro">
                                            <option value="nit" selected>Nit</option>
                                            <option value="payment_name">Nombre</option>
                                        </select>
                                        <input type="text" id="nafil" name="nafil" onkeypress="pulsar(event);">&nbsp;
                                        <script type="text/javascript">
                                            $("nafil").focus();
                                        </script>                                      
                                        <span id="spangen"></span>
                                        <br>

                                        <div id="selgen0" style="width: 240px;"><!-- 20101111 -->
                                            <select id="selgen" name="selgen" style="width: 100%;">
                                            <option value="">...</option>
                                        </select>
                                        </div>
                                        
                                    </td>
                                </tr>
                                <tr>
                                    <td align="left" width="35%" class="fila">&nbsp;Convenio</td>
                                    <td align="left" width="65%" class="fila">
                                        <input type="text" id="convtext" name="convtext" readonly value="1">
                                        <input type="hidden" id="convid" name="convid"value="">
                                        <input type="hidden" id="hidIdProvConvenio" name="hidIdProvConvenio" value="">
                                        <input type="hidden" id="hidFacturaTercero" name="hidFacturaTercero" value="">
                                        <img alt="buscar" src="<%=BASEURL%>/images/botones/iconos/lupa.gif" width="15" height="15" style="cursor:pointer;" onclick="verConvs();">
                                    </td>
                                </tr>
                                <tr>
                                    <td align="left" width="35%" class="fila">&nbsp;Sector</td>
                                    <td align="left" width="65%" class="fila">
                                        <input type="text" id="secttext" name="secttext" readonly value="1">
                                        <input type="hidden" id="sectid" name="sectid" value="">
                                    </td>
                                </tr>
                                <tr>
                                    <td align="left" width="35%" class="fila">&nbsp;Subsector</td>
                                    <td align="left" width="65%" class="fila">
                                        <input type="text" id="subsecttext" name="subsecttext" readonly value="1">
                                        <input type="hidden" id="subsectid" name="subsectid" value="">
                                    </td>
                                </tr>
                                <%
                                    GestionCondicionesService  gserv = new GestionCondicionesService(usuario.getBd());
                                    ArrayList<String> listatitulos = null;
                                    try {
                                        listatitulos = gserv.titulosValor();
                                    }
                                    catch (Exception e) {
                                        System.out.println("Error al obtener la lista de tipos de titulo valor: "+e.toString());
                                        e.printStackTrace();
                                    }
                                %>
                                <tr>
                                    <td align="left" width="35%" class="fila">&nbsp;Tipo Titulo Valor</td>
                                    <td align="left" width="65%" class="fila">
                                        <select id="tipotitulo" name="tipotitulo">
                                            <%
                                                if(listatitulos!=null && listatitulos.size()>0){
                                                    String row[] = null;
                                                    for(int i=0;i<listatitulos.size();i++){
                                                        row = (listatitulos.get(i)).split(";_;");
                                            %>
                                            <option value="<%= row[0] %>" ><%= row[1] %></option>
                                            <%
                                                    }
                                                }
                                                else{
                                            %>
                                            <option value="" selected>...</option>
                                            <%
                                                }
                                            %>
                                        </select>&nbsp;
                                    </td>
                                </tr>
                                <%
                                    ArrayList<String> listaplazos = null;
                                    try {
                                        listaplazos = gserv.plazoCuota();
                                    }
                                    catch (Exception e) {
                                        System.out.println("Error al obtener la lista de tipos de titulo valor: "+e.toString());
                                        e.printStackTrace();
                                    }
                                %>
                                <tr>
                                    <td align="left" width="35%" class="fila">&nbsp;Plazo Primer Titulo:</td>
                                    <td align="left" width="65%" class="fila">
                                        <select id="plazop" name="plazop">
                                            <%
                                                if(listaplazos!=null && listaplazos.size()>0){
                                                    String row[] = null;
                                                    for(int i=0;i<listaplazos.size();i++){
                                                        row = (listaplazos.get(i)).split(";_;");
                                            %>
                                            <option value="<%= row[0] %>" ><%= row[1] %></option>
                                            <%
                                                    }
                                                }
                                                else{
                                            %>
                                            <option value="" selected>...</option>
                                            <%
                                                }
                                            %>
                                        </select>&nbsp;
                                    </td>
                                </tr>
                                <tr id="trPropietario">
                                    <td align="left" width="35%" class="fila">&nbsp;Propietario</td>
                                    <td align="left" width="65%" class="fila">
                                        <select id="propietario" name="propietario">
                                            <option value="true" >Si</option>
                                            <option value="false" selected>No</option>
                                        </select>&nbsp;
                                    </td>
                                </tr>
                                <tr>
                                    <td align="center" class="fila" colspan="2">
                                        <img alt="detalles" src="<%=BASEURL%>/images/botones/detalles.gif" style="cursor:pointer" name="imgdetalles" id="imgdetalles" onclick="viewRanks();" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);">
                                        <img alt="exportar" src="<%=BASEURL%>/images/botones/exportarExcel.gif" style="cursor:pointer" name="imgexcel" id="imgexcel" onclick="exportar();" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);">
                                        <img alt="salir" src="<%=BASEURL%>/images/botones/salir.gif" style="cursor:pointer" name="imgsalir" onClick="window.close();" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);">
                                    </td>
                                </tr>
                            </table>
                          <form action="<%= CONTROLLER%>?estado=Gestion&accion=Condiciones" method="post" id="formulario" name="formulario">
                            <table class="tablaInferior" border="0" width="100%">
                                <tr>
                                    <td width="37%" align="left" class="subtitulo1">&nbsp;Rangos en dias</td>
                                    <td width="63%" align="left" class="barratitulo">
                                        <img alt="" src="<%= BASEURL%>/images/titulo.gif" width="32" height="20"  align="left">
                                    </td>
                                </tr>
                            </table>
                            <div id="divrank">
                                <table border="0" width="100%" id="tavales">
                                    <tr>
                                        <td align="left" width="20%" class="fila">&nbsp;</td>
                                        <td align="left" width="35%" class="fila">Inicio</td>
                                        <td align="left" width="15%" class="fila">Fin</td>
                                        <td align="left" width="25%" class="fila">Porcentaje</td>
                                    </tr>
                                    <tr>
                                        <td align="center" class="fila">
                                            <%
                                                if(adding==true){
                                            %>
                                            <img alt="+" style="cursor: pointer" onclick="TDAgregarRegistro('tavales','avales')" src='<%=BASEURL%>/images/botones/iconos/mas.gif' width='12' height='12' >&nbsp;<img alt="-"style="cursor: pointer" onclick="TDEliminarRegistro(this,'tavales')" src='<%=BASEURL%>/images/botones/iconos/menos1.gif' width='12' height='12' >&nbsp;
                                            <%
                                                }
                                            %>
                                        </td>
                                        <td align="left" class="fila">
                                            <input onkeyup="soloNumeros(this.id);" type="text" id="riavales1" name="riavales1" value="1" readonly>
                                        </td>
                                        <td align="left" class="fila">
                                            <input onkeyup="soloNumeros(this.id);" onblur="validateValue('1');" type="text" id="rfavales1" name="rfavales1">
                                        </td>
                                        <td align="left" class="fila">
                                            <input onkeyup="soloNumeros(this.id);this.value = formatNumber(this.value);" type="text" id="'pavales1" name="pavales1">
                                        </td>
                                    </tr>
                                </table>
                            </div>
                            
                          </form>

                        </td>
                    </tr>
                </table>
                <br>
                <%
                    if(adding==true){
                %>
                <img alt="" src = "<%=BASEURL%>/images/botones/modificar.gif" style = "cursor:pointer" name = "imgmodificar" id="imgmodificar" onclick="save();" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);">
                <%
                    }
                %>
                <img alt="" src = "<%=BASEURL%>/images/botones/restablecer.gif" style = "cursor:pointer" name = "imgrest" id="imgrest" onclick="location.reload();" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);">
            </center>
        </div>
    </body>
</html>
