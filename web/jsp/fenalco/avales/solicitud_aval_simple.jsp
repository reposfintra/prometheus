<%@page import="com.tsp.util.Util"%>
<%@page import="java.text.SimpleDateFormat"%>
<%@page import="java.text.DateFormat"%>
<%@page session="true"%>
<%@page import="java.util.*"%>
<%@page import="com.tsp.operation.model.*"%>
<%@page import="com.tsp.operation.model.beans.*"%>
<%@page import="com.tsp.operation.model.services.*"%>
<%@include file="/WEB-INF/InitModel.jsp"%>
<%
            response.addHeader("Cache-Control", "no-cache");
            response.addHeader("Pragma", "No-cache");
            response.addDateHeader("Expires", 0);
            GestionSolicitudAvalService gsaserv = new GestionSolicitudAvalService();
            WSHistCreditoService wsdatacred= new WSHistCreditoService();
            ArrayList optdep = gsaserv.listadoDeps();
            ArrayList optciu = new ArrayList();
            ArrayList tipo_id = gsaserv.busquedaGeneral("TIPID");
            ArrayList convenios=new ArrayList();
            ArrayList <Codigo> productos = wsdatacred.buscarTabla("H", "producto");
            ArrayList <Codigo> matricula = new ArrayList();
            ArrayList <Codigo> servicios = new ArrayList();
            ArrayList <Codigo> tipo_vivienda = wsdatacred.buscarTabla("H", "vivienda");
            String cod = request.getParameter("cod") != null ? request.getParameter("cod") : "N";
            String estado = "B";
            Usuario usuario = (Usuario) session.getAttribute("Usuario");
            String id_usuario = usuario.getLogin();
            String sel_afils = "";
            int num_solicitud = 1;
            DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
            Date dat = new Date();
            String fec_act = dateFormat.format(dat);
            SolicitudAval bean_sol = null;
            SolicitudEstudiante bean_est = null;
            SolicitudLaboral bean_lab = null;
            SolicitudPersona bean_pers = null;
            SolicitudPersona bean_perc = null;
            SolicitudPersona bean_pere = null;
            String[] tr = null;
            String[] tr2 = null;
            String vista = request.getParameter("vista") != null ? request.getParameter("vista") : "0";
            String[] dato1 = null;
            GestionConveniosService gcserv = new GestionConveniosService();
             ArrayList<String> titval = new ArrayList();
            GestionCondicionesService  gserv = new GestionCondicionesService();
            ArrayList<String> listaplazos = null;
                                                try {
                                                    listaplazos = gserv.plazoCuota();
                                    }
                                    catch (Exception e) {
                                        System.out.println("Error al obtener la lista de tipos de titulo valor: "+e.toString());
                                                    e.printStackTrace();
                                                }
            String login_into=(String)session.getAttribute("login_into");
           
            ArrayList<String> listacad = null;
            try {
                if (!vista.equals("0")) {
                    num_solicitud = (request.getParameter("num_solicitud") != null && request.getParameter("num_solicitud").equals("") == false)
                            ? Integer.parseInt(request.getParameter("num_solicitud")) : 0;
                    bean_sol = gsaserv.buscarSolicitud(num_solicitud);
                } else {
                    num_solicitud = Integer.parseInt(gsaserv.numeroSolc());
                }
                sel_afils = gsaserv.buscarAfilsUsuario(id_usuario);
                 titval = gserv.titulosValorConvenio(bean_sol.getIdConvenio());
                listacad = gcserv.buscarBancos("nombre", "");
                if (!vista.equals("0")) {
                    matricula=wsdatacred.buscarTablaRef("H", "matricula", bean_sol.getProducto());
                    servicios=wsdatacred.buscarTablaRef("H", "servicio", bean_sol.getProducto());
                    bean_pers = gsaserv.buscarPersona(num_solicitud, "S");
                    bean_perc = gsaserv.buscarPersona(num_solicitud, "C");
                    bean_pere = gsaserv.buscarPersona(num_solicitud, "E");

                    estado = bean_sol.getEstadoSol();

                    if (!bean_pers.getTipoPersona().equals("J")) {

                        bean_lab = gsaserv.datosLaboral(num_solicitud, "S");

                    }
                    sel_afils = gsaserv.buscarAfilsUsuario(id_usuario, bean_sol.getAfiliado());
                    bean_est = gsaserv.datosEstudiante(num_solicitud);
                    convenios=gsaserv.buscarConveniosAfil(id_usuario, bean_sol.getAfiliado());
                }


            } catch (Exception e) {
                System.out.println("Error al buscar listados: " + e.toString());
                e.printStackTrace();
            }
%>

<%@page contentType="text/html" pageEncoding="ISO-8859-1"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">
<!-- This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>. -->
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
        <link href="<%= BASEURL%>/css/estilostsp.css" rel='stylesheet'>
        <link href="<%=BASEURL%>/css/mac_os_x.css" rel="stylesheet" type="text/css">
        <link href="<%=BASEURL%>/css/default.css" rel="stylesheet" type="text/css">
        <style type="text/css">
            td{ white-space:nowrap;}
            b{
                color:white;
            }

            b{
                color:white;
            }

            .filableach{
                color: #000000;
                font-family: Tahoma, Arial;
                background-color:#FFFFFF;
                font-size: 12px;
            }

            .divs {
                font-family: Verdana, Tahoma, Arial, Helvetica, sans-serif;
                font-size: small;
                color: #000000;
                background-color:#006C3A;
                -moz-border-radius: 0.7em;
                -webkit-border-radius: 0.7em;
            }
            input.validation-failed, textarea.validation-failed {
                border: 1px solid #FF3300;
                color : #FF3300;
            }
        </style>
        <script type='text/javascript' src="<%= BASEURL%>/js/boton.js"></script>
        <script type='text/javascript' src="<%= BASEURL%>/js/prototype.js"></script>
        <script src="<%=BASEURL%>/js/effects.js" type="text/javascript"></script>
        <script src="<%=BASEURL%>/js/window.js" type="text/javascript"></script>
        <script src="<%=BASEURL%>/js/solicitud_aval.js" type="text/javascript"></script>
        <script src="<%=BASEURL%>/js/validation.js" type="text/javascript" ></script>
        <title>Solicitud de Aval</title>
        <script type="text/javascript">
            function xpand_div1(){
                var id_div = document.getElementById("tipo_p").value;
                if(id_div!=""){
                    $('contenido').innerHTML = $(id_div).innerHTML;
                    var check =document.getElementById("codeu").checked;
                    if(check==true){
                        $('contenido').innerHTML +=  $('codeudor').innerHTML;
                    }
                    var indice = document.getElementById("producto").selectedIndex;
                    var producto=document.getElementById("producto").options[indice].value;
                    if(producto!='02'){
                        $('servicio').disabled=true;
                        $('matricula').disabled=true;

                        if(producto=='01'){
                            $('contenido').innerHTML +=  $('estudiante').innerHTML;
                            var indice = document.getElementById("afiliado").selectedIndex;
                            document.getElementById("nom_uni").value=document.getElementById("afiliado").options[indice].text;
                            document.getElementById("nom_uni").readOnly="readOnly";
                        }

                    }else{
                        $('servicio').disabled=false;
                        $('matricula').disabled=false;                        
                    }

                    $('contenido').style.border="0.1em solid black";
                    $('contenido').style.padding ="0.3em";
            <%
                        if (!vista.equals("3") && !vista.equals("6")) {
            %>
                        $('imgaceptar').style.visibility= "visible";
                        if($("imgguardar")!=null){
                            $('imgguardar').style.visibility= "visible";
                        }
            <%}%>
                    }
                    else{
                        $('contenido').innerHTML = '';
                        $('contenido').style.border="0em solid black";
                        $('contenido').style.padding ="0em";
                        $('imgaceptar').style.visibility= "hidden";
                        if($("imgguardar")!=null){
                            $('imgguardar').style.visibility= "hidden";
                        }
                    }
                       valid = new Validation('formulario', {immediate : true, onFormValidate : formCallback});
                }              

                function generarPdf(){
                    var dato = $('num_solicitud').value;
                    var url = "<%=CONTROLLER%>?estado=GestionSolicitud&accion=Aval";
                    var p = "opcion=export_pdf&num_solicitud="+dato;
                    new Ajax.Request(url, { parameters: p, method: 'post', onComplete: completo } );
                }

                function guardar2(){

                var campos = "";
                var ncampos = "";

                campos = new Array("afiliado","convenio" );
                ncampos = new Array("Afiliado ","Convenio");

                for (i = 0; i < campos.length; i++){
                    campo = campos[i];
                    if (document.getElementById("formulario").elements[campo].value == ''){
                        alert("El campo "+ncampos[i]+" esta vacio.!");
                        document.getElementById("formulario").elements[campo].focus();
                        return (false);
                        break;
                    }
                }
                    var indice = document.getElementById("producto").selectedIndex;
                    var producto=document.getElementById("producto").options[indice].value;
                    var est=  producto=="01"?"S":"N";
                    var cod= document.getElementById("codeu").checked?"S":"N";
                    var accion = controller+"?estado=GestionSolicitud&accion=Aval";
                    var valid = new Validation('formulario');
                    if(valid.validate()){
                        $('formulario').action = accion + '&opcion=ins_data2&dato=B&est='+est+'&cod='+cod;
                        $('formulario').submit();
                    }else{
                        alert("una o mas fechas en el formulario son invalidas");
                    }
                }

                function deshab() {
                    frm = document.forms['formulario'];
                    for(i=0; ele=frm.elements[i]; i++)
                        ele.disabled=true;
                }
                function completo(resp){
                    alert(resp.responseText);
                }
            
        </script>
    </head>
    <body  onload="varcontr('<%=CONTROLLER%>');maximizar();<%=vista.equals("0") ? "" : "xpand_div1();"%> <%=!vista.equals("3") ? "" : "deshab();"%> ">
        <div id="capaSuperior" style="position:absolute; width:100%; height:100px; z-index:0; left: 0px; top: 0px;">
            <%--                        if (vista.equals("0")) {
                            if (login_into.equals("Fenalco")) {%>
            <jsp:include page="/toptsp.jsp?encabezado=Solicitud de Aval"/>
            <%} else {%>
            <jsp:include page="/toptsp.jsp?encabezado=Solicitud de Credito"/>
            <%}%>

            <%                                    } else {
                                        if (login_into.equals("Fenalco")) {%>
            <jsp:include page="/toptsp.jsp?encabezado=Modificar Solicitud de Aval"/>
            <%} else {%>
            <jsp:include page="/toptsp.jsp?encabezado=Modificar Solicitud de Credito"/>
            <%}%>

            <%                        }
            --%>
        </div>
        <div id="capaCentral" style="position:absolute; width:100%; height:83%; z-index:0; left: 0px; top: 100px; ">
        <div id="contenido2" style="visibility: hidden; display: none; height: 150px; width: 500px; background-color: #EEEEEE;"></div>
            <%
                        if ((estado.equals("B") == true && (vista.equals("1") || vista.equals("0")))) {
            %>
            <form id="formulario" name="formulario" action="<%=CONTROLLER%>?estado=GestionSolicitud&accion=Aval" method="post">
                <%
                                        } else {
                %>
                <form id="formulario" name="formulario" action="" method="post" >
                    <%                             }
                    %>
                    <div id="header">
                        <table id="tsup" style="border: 0em; border-collapse:collapse; width: 100%">
                            <thead>
                                 <tr class="filaazul">
                                  <%if(login_into.equals("Fenalco")){%>
                                        <th align="center" colspan="9">SOLICITUD DE AVAL</th>
                                        <%}else{%>
                                        <th align="center" colspan="9">SOLICITUD DE CREDITO</th>
                                        <%}%>
                                    <th  style="text-align:right; ">
                                        No. de Formulario
                                    </th>
                                    <th>
                                        <input name="num_solicitud" id="num_solicitud" style="background-color: transparent; border-bottom: 0px; border-left: 0px; border-right: 0px; border-top: 0px; width:40%; text-align:right; color:#990000; font-size:16px;" value="<%= num_solicitud%>" size="15" readonly="readonly">
                                    </th>
                                </tr>
                            </thead>
                            <tbody>
                               <tr class="filaazul">
                                    <td rowspan="6" width="9%" valign="top">
                                        <%if(login_into.equals("Fenalco")){%>
                                        <img alt="logofenalco" src="<%=BASEURL%>/images/fenalco_atlantico.jpg"  height="120">
                                        <%}else{%>
                                        <img alt="logofintra" src="<%=BASEURL%>/images/logo_fintra.jpg"  height="80">
                                        <%}%>
                                    </td>
                                    <td width="5%">
                                        Fecha
                                    </td>
                                    <td width="20%">
                                        <input type="text" id="fecha_cons" name="fecha_cons" size="15" readonly="readonly"  value="<%= fec_act%> ">
                                    </td>
                                    <td colspan="6">

                                    </td>
                                    <td width="8%">
                                        No.aprobacion
                                    </td>
                                    <td width="12%">
                                        <input type="text" id="num_aprobacion" name="num_aprobacion" value="<%=(vista.equals("0")) ? "" : bean_sol.getNumeroAprobacion()%>" disabled>
                                    </td>
                                </tr>
                                <tr class="filaazul">
                                    <td>
                                        Afiliado
                                    </td>
                                    <td colspan="3">
                                        <select name="afiliado" id="afiliado" onchange="limpiarConv();cargarCodigo(this.selectedIndex);" style="width:90%;" <%=(vista.equals("0") || estado.equals("B")) ? "" : "disabled"%> >
                                            <option value="">...</option>
                                            <%= sel_afils%>
                                        </select>
                                    </td>
                                    <td width="6%">
                                        Convenio
                                    </td>
                                    <td colspan="3" >
                                        <input type="text" id="convtext" name="convtext" readonly value="<%=(vista.equals("0")) ? "" : gcserv.nombreConvenio(bean_sol.getIdConvenio())%>" style="width:90%;">
                                        <img alt="buscar" src="<%=BASEURL%>/images/botones/iconos/lupa.gif" width="15" height="15" style="cursor:pointer;" onclick="verConvsUsuario('<%=CONTROLLER%>');">
                                        <input type="hidden" id="convenio" name="convenio"value="<%=(vista.equals("0")) ? "" : bean_sol.getIdConvenio()%>">
                                        <input type="hidden" id="redescuento" name="redescuento"value="<%=(vista.equals("0")) ? "" :gcserv.tieneRedescuento(bean_sol.getIdConvenio()) %>">

                                    </td>
                                    <td>
                                        Codigo
                                    </td>
                                    <td>
                                        <input name="codigo" onkeyup="soloNumeros(this.id);" type="text" id="codigo" maxlength="2" value="<%= (vista.equals("0")) ? "" : (bean_sol.getCodigo() == null || bean_sol.getCodigo().length() < 9 ? "" : bean_sol.getCodigo().substring(0, 2))%>" size="2" readonly>
                                        <input name="codigo1" onkeyup="soloNumeros(this.id);"type="text" id="codigo1" maxlength="5" value="<%= (vista.equals("0")) ? "" : (bean_sol.getCodigo() == null || bean_sol.getCodigo().length() < 9 ? "" : bean_sol.getCodigo().substring(2, 7))%>" size="5" readonly>
                                        <input name="codigo2" onkeyup="soloNumeros(this.id);"type="text" id="codigo2" maxlength="2"value="<%= (vista.equals("0")) ? "" : (bean_sol.getCodigo() == null || bean_sol.getCodigo().length() < 9 ? "" : bean_sol.getCodigo().substring(7, 9))%>" size="2" >
                                    </td>
                                </tr>

                                <tr class="filaazul">
                                   <td>Sector</td>
                                    <td   colspan="3">
                                        <input type="text" id="sector" name="sector" readonly value="<%=(vista.equals("0")) ? "" : (login_into.equals("Fenalco")? gcserv.nombreAlternoSector(bean_sol.getSector()):gcserv.nombreSector(bean_sol.getSector()))%>" style="width:90%;">
                                        <input type="hidden" id="sectid" name="sectid" value="<%=(vista.equals("0")) ? "" : bean_sol.getSector()%>">
                                    </td>
                                    <td  >Subsector</td>
                                    <td  colspan="3">
                                        <input type="text" id="subsecttext" name="subsecttext" readonly value="<%=(vista.equals("0")) ? "" : (login_into.equals("Fenalco")? gcserv.nombreAlternoSubSector(bean_sol.getSector(), bean_sol.getSubsector()):gcserv.nombreSubSector(bean_sol.getSector(), bean_sol.getSubsector()))%>"  style="width:90%;">
                                        <input type="hidden" id="subsectid" name="subsectid" value="<%=(vista.equals("0")) ? "" : bean_sol.getSubsector()%>">
                                    </td>

                                    <td>T&iacute;tulo valor</td>
                                    <td >
                                        <select name="cmbTituloValor" id="cmbTituloValor" onchange="validaraldia();" style="width:8em;" >
                                            <option value="">...</option>
                                            <%
                                                 if (titval != null && titval.size() > 0) {
                                                     String row[] = null;
                                                     for (int i = 0; i < titval.size(); i++) {
                                                         row = (titval.get(i)).split(";_;");
                                            %>
                                            <option value="<%= row[0]%>" <%=(row[0].equals((vista.equals("0")) ? " " : bean_sol.getTipoNegocio())) ? "selected" : ""%> ><%= row[1]%></option>
                                            <%
                                                     }
                                                 }
                                            %>
                                        </select>
                                        <input type="checkbox" id="aldia" name="aldia"  onclick="validaraldia();"  <%= (bean_sol != null) ? (bean_sol.getTipoNegocio().equals("01") ? "" : "disabled") : "disabled"%>>
                                        &nbsp;&nbsp;Al dia
                                    </td>
                                </tr>
                                <tr class="filaazul">
                                    <td>
                                        Producto
                                    </td>
                                    <td colspan="3">
                                        <select id="producto" name="producto" style="width:20em;" onchange="cargarServicios(this.selectedIndex);limpiarest('<%=estado%>');">
                                            <option value="" selected>...</option>
                                            <%
                                                 for (int i = 0; i < productos.size(); i++) {%>
                                            <option value="<%=productos.get(i).getCodigo()%>" <%=(productos.get(i).getCodigo().equals((vista.equals("0")) ? " " : bean_sol.getProducto())) ? "selected" : ""%> ><%=productos.get(i).getValor()%></option>
                                            <%  }%>
                                        </select>
                                    </td>
                                    <td>
                                        Servicio
                                    </td>
                                    <td width="11%">
                                        <div id="d_servicio">
                                            <select id="servicio" name="servicio" style="width:12em;" >
                                                <option value="" selected>...</option>
                                                <%
                                                 for (int i = 0; i < servicios.size(); i++) {%>
                                                <option value="<%=servicios.get(i).getCodigo()%>" <%=(servicios.get(i).getCodigo().equals((vista.equals("0")) ? " " : bean_sol.getServicio())) ? "selected" : ""%> ><%=servicios.get(i).getValor()%></option>
                                                <%  }%>
                                            </select>
                                        </div>
                                    </td>
                                    <td width="6%">
                                        Matricula
                                    </td>
                                    <td width="12%">
                                        <div id="d_matricula">
                                            <select id="matricula" name="matricula" style="width:12em;" >
                                                <option value="" selected>...</option>
                                                <%
                                                for (int i = 0; i < matricula.size(); i++) {%>
                                                <option value="<%=matricula.get(i).getEquivalencia()%>" <%=(matricula.get(i).getEquivalencia().equals((vista.equals("0")) ? " " : bean_sol.getCiudadMatricula())) ? "selected" : ""%> ><%=matricula.get(i).getValor()%></option>
                                                <%  }%>
                                            </select>
                                        </div>
                                    </td>
                                    <td>
                                        Valor producto $
                                    </td>
                                    <td>
                                        <input type="text" id="valor_producto" onkeyup="soloNumeros(this.id);" name="valor_producto" onblur="formato(this,0)" onkeyup="soloNumeros(this.id);" size="15" value="<%=(vista.equals("0")) ? "" : Util.customFormat(Double.parseDouble(bean_sol.getValorProducto()))%>" style="border-bottom: 1px solid; border-left: 0px; border-right: 0px; border-top: 0px; text-align:right;">
                                    </td>
                                </tr>

                                <tr class="filaazul">
                                    <td>
                                        Tipo
                                    </td>
                                    <td>

                                        <select id="tipo_p" name="tipo_p" style="width:12em;" onchange="xpand_div1();">
                                            <option value="">...</option>
                                            <option value="natural" <%= (vista.equals("0")) ? "" : bean_pers.getTipoPersona().equals("N") ? "selected" : ""%>>Persona natural</option>
                                            <option value="juridica" <%= (vista.equals("0")) ? "" : bean_pers.getTipoPersona().equals("J") ? "selected" : ""%>>Persona juridica</option>
                                        </select>&nbsp;
                                    </td>
                                    <td width="6%">
                                        Codeudor
                                    </td>
                                    <td width="5%">
                                        <input type="checkbox" id="codeu" name="codeu"  onclick="limpiarcod2('<%=estado%>');"<%= (bean_perc == null) ? (cod.equals("S") ? "checked" : "") : "checked"%>>
                                    </td>
                                    <td>
                                        Valor solicitado $
                                    </td>
                                    <td>

                                        <input type="text" id="valor_solicitado" onkeyup="soloNumeros(this.id);" name="valor_solicitado"  onblur="formato(this, 0); cargarValorProd();" onkeyup="soloNumeros(this.id);" size="15" value="<%=(vista.equals("0")) ? "" : Util.customFormat(Double.parseDouble(bean_sol.getValorSolicitado()))%>" style="border-bottom: 1px solid; border-left: 0px; border-right: 0px; border-top: 0px; text-align:right;">
                                    </td>
                                    <td>
                                        Plazo
                                    </td>
                                    <td>
                                        <input type="text" id="plazo" onkeyup="soloNumeros(this.id);" name="plazo"  onblur="formato(this, 0);" onkeyup="soloNumeros(this.id);" size="2" value="<%=(vista.equals("0")) ? "" : bean_sol.getPlazo()%>" style="border-bottom: 1px solid; border-left: 0px; border-right: 0px; border-top: 0px; text-align:right;">
                                    </td>
                                    <td>
                                        Plazo 1era cuota
                                    </td>
                                    <td valign="middle">
                                        <select name="forma_pago" id="forma_pago" class="listmenu" onChange="">
                                            <option value="" selected>...</option>
                                            <%
                                                 if (listaplazos != null && listaplazos.size() > 0) {
                                                     String row[] = null;
                                                     for (int i = 0; i < listaplazos.size(); i++) {
                                                         row = (listaplazos.get(i)).split(";_;");
                                            %>
                                            <option value="<%= row[0]%>"  <%=(row[0].equals((vista.equals("0")) ? " " : bean_sol.getPlazoPrCuota())) ? "selected" : ""%> ><%= row[1]%></option>
                                            <%
                                                     }
                                                 }
                                            %>
                                        </select>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                                    
                                    
                                    
                                    
                                    
                    <br>
                    <div id="contenido" class="divs">

                    </div>
                </form>
                <script type="text/javascript">
                    function formCallback(result, form) {
                        window.status = "valiation callback for form '" + form.id + "': result = " + result;
                    }

                </script>
                <br>
                <div align="center">
                    <%  if (estado.equals("B")) {%>
                    <img alt="" src = "<%=BASEURL%>/images/botones/aceptar.gif" style="cursor:pointer; visibility: hidden;" onclick="validar_solicitud_aval2();" name= "imgaceptar" id="imgaceptar"  onMouseOver="botonOver(this);" onMouseOut="botonOut(this);">
                    <%  }
                                if (estado.equals("B")) {%>

                    <img alt="" src = "<%=BASEURL%>/images/botones/guardar.gif" style="cursor:pointer; visibility: hidden;" onclick="guardar2();" name= "imgguardar" id="imgguardar"  onMouseOver="botonOver(this);" onMouseOut="botonOut(this);">
                    <%}%>
                    <img alt="" src = "<%=BASEURL%>/images/botones/salir.gif" style = "cursor:pointer" name = "imgsalir" onclick = "window.close();" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);">

                </div>
        </div>


<iframe width="188" height="166" name="gToday:normal:agenda.js:gfPop:plugins.js" id="gToday:normal:agenda.js:gfPop:plugins.js" src="<%=BASEURL%>/js/Calendario/ipopeng.htm" scrolling="yes" frameborder="0" style="visibility:visible; z-index:999; position:absolute; left:-500px; top:0px;">
</iframe>
<div id="natural" style="display:none; visibility: hidden;">
    <b>PERSONA NATURAL</b><br>
    <b>Informaci&oacute;n b&aacute;sica</b>
    <table id="tnatural" style="border-collapse:collapse; width:100%" border="1">
        <tr class="filaazul">
            <td>
                Identificacion<br>
                <select id="tipo_id_nat" name="tipo_id_nat">
                    <option value="" selected>...</option>
                    <%  dato1 = null;
                                for (int i = 0; i < tipo_id.size(); i++) {
                                    dato1 = ((String) tipo_id.get(i)).split(";_;");%>
                    <option value="<%=dato1[0]%>" <%=(dato1[0].equals((vista.equals("0")) ? " " : bean_pers.getTipoId())) ? "selected" : ""%> ><%=dato1[1]%></option>
                    <%  }%>
                </select>
                <input type="text" id="id_nat" onkeyup="soloNumeros(this.id);"  name="id_nat" value="<%= (vista.equals("0")) ? " " : bean_pers.getIdentificacion()%>" size="20" >
                <img alt="buscar" src="<%=BASEURL%>/images/botones/iconos/lupa.gif" width="15" height="15" style="cursor:pointer;" onclick="busqueda('id_nat');">
            </td>
            <td  width="17%">
                Primer apellido<br>
                <input type="text" id="pr_apellido_nat" name="pr_apellido_nat" value="<%= (vista.equals("0")) ? "" : bean_pers.getPrimerApellido()%>" size="25" >
            </td>
            <td class="filableach"  width="16%">
                Segundo Apellido<br>
                <input type="text" id="seg_apellido_nat" name="seg_apellido_nat" value="<%= (vista.equals("0")) ? "" : bean_pers.getSegundoApellido()%>" size="25">
            </td>
            <td  width="17%">
                Primer Nombre<br>
                <input type="text" id="pr_nombre_nat" name="pr_nombre_nat" value="<%= (vista.equals("0")) ? "" : bean_pers.getPrimerNombre()%>" size="25">
            </td>
            <td class="filableach"  width="17%">
                Segundo Nombre<br>
                <input type="text" id="seg_nombre_nat" name="seg_nombre_nat" value="<%= (vista.equals("0")) ? "" : bean_pers.getSegundoNombre()%>" size="25">
            </td>


        </tr>
        <tr class="filaazul">

            <td  width="16%">
                Genero<br>
                <select id="genero_nat" name="genero_nat">
                    <option value="">...</option>
                    <option value="M" <%= (vista.equals("0")) ? "" : bean_pers.getGenero().equals("M") ? "selected" : ""%>>Masculino</option>
                    <option value="F" <%= (vista.equals("0")) ? "" : bean_pers.getGenero().equals("F") ? "selected" : ""%>>Femenino</option>
                </select>
            </td>
            <td>
                Fecha de nacimiento (yyyy-MM-dd)<br>
                <input type="text" id="f_nac_nat" class="validate-date" name="f_nac_nat" onblur="validarEdad(this.id, 'Deudor')" value="<%= (vista.equals("0")) ? "" :Util.formatoFechaPostgres( bean_pers.getFechaNacimiento())%>" size="15" >
            </td>
            <td class="filableach">
                Celular<br>
                <input type="text" id="cel_nat" name="cel_nat" value="<%=(vista.equals("0")) ? "" : bean_pers.getCelular()%>" size="15" onkeyup="soloNumeros(this.id);">
            </td>
            <td class="filableach">
                E-mail<br>
                <input type="text" id="mail_nat" name="mail_nat" value="<%=(vista.equals("0")) ? "" : bean_pers.getEmail()%> " size="30">
            </td>
              <td>
                Tipo de vivienda<br>
                <select id="tipo_viv_nat" name="tipo_viv_nat" style="width:10em;">
                    <option value="">...</option>
                               <% for (int i = 0; i < tipo_vivienda.size(); i++) {%>
                    <option value="<%=tipo_vivienda.get(i).getCodigo()%>" <%=(tipo_vivienda.get(i).getCodigo().equals((vista.equals("0")) ? " " : bean_pers.getTipoVivienda())) ? "selected" : ""%> ><%=tipo_vivienda.get(i).getValor()%></option>
                    <%  }%>
                </select>
            </td>

        </tr>

        <tr class="filaazul">


            <td colspan="2">
                Direccion residencia<br>
                <input type="text" id="dir_nat" name="dir_nat" value="<%=(vista.equals("0")) ? "" : bean_pers.getDireccion()%>" onblur="dircobro(this.id, 'dir_cob_nat');" size="60">
            </td>
            <td >
                Departamento<br>
                <select id="dep_nat" name="dep_nat" style="width:20em;" onchange="cargarCiudades('dep_nat', 'ciu_nat');">
                    <option value="">...</option>
                    <%  dato1 = null;
                                for (int i = 0; i < optdep.size(); i++) {
                                    dato1 = ((String) optdep.get(i)).split(";_;");%>
                    <option value="<%=dato1[0]%>" <%=(dato1[0].equals((vista.equals("0")) ? " " : bean_pers.getDepartamento())) ? "selected" : ""%> ><%=dato1[1]%></option>
                    <%  }%>
                </select>
            </td>
            <td>
                Ciudad<br>
                <div id="d_ciu_nat">
                    <select id="ciu_nat" name="ciu_nat" style="width:20em;">
                        <option value="">...</option>
                        <%  if (!vista.equals("0") && bean_pers != null) {
                                        optciu = gsaserv.listadoCiudades(bean_pers.getDepartamento());
                                        for (int i = 0; i < optciu.size(); i++) {
                                            dato1 = ((String) optciu.get(i)).split(";_;");%>
                        <option value="<%=dato1[0]%>" <%=(dato1[0].equals(bean_pers.getCiudad())) ? "selected" : ""%> ><%=dato1[1]%></option>
                        <%  }
                                    }%>
                    </select>
                </div>
            </td>            
            <td>
                Telefono<br>
                <input type="text" id="tel_nat" name="tel_nat" value="<%=(vista.equals("0")) ? "" : bean_pers.getTelefono()%>" size="15" onkeyup="soloNumeros(this.id);">
            </td>
        </tr>
        <tr class="filaazul">
            <td colspan="2">
                Direccion Oficina<br>
                <input type="text" id="dir_emp_nat" name="dir_emp_nat" value="<%=(vista.equals("0") || bean_lab == null) ? "" : bean_lab.getDireccion()%>" size="60">
            </td>
            <td>
                Departamento<br>
                <select id="dep_emp_nat" name="dep_emp_nat" style="width:20em;" onchange="cargarCiudades('dep_emp_nat', 'ciu_emp_nat');">
                    <option value="">...</option>
                    <%  dato1 = null;
                                for (int i = 0; i < optdep.size(); i++) {
                                    dato1 = ((String) optdep.get(i)).split(";_;");%>
                    <option value="<%=dato1[0]%>" <%=(dato1[0].equals((vista.equals("0") || bean_lab == null) ? " " : bean_lab.getDepartamento())) ? "selected" : ""%> ><%=dato1[1]%></option>
                    <%  }%>
                </select>
            </td>
            <td>

                Ciudad<br>
                <div id="d_ciu_emp_nat">
                    <select id="ciu_emp_nat" name="ciu_emp_nat" style="width:20em;">
                        <option value="">...</option>
                        <%  if (!vista.equals("0") && bean_lab != null) {
                                        optciu = gsaserv.listadoCiudades(bean_lab.getDepartamento());
                                        for (int i = 0; i < optciu.size(); i++) {
                                            dato1 = ((String) optciu.get(i)).split(";_;");%>
                        <option value="<%=dato1[0]%>" <%=(dato1[0].equals(bean_lab.getCiudad())) ? "selected" : ""%> ><%=dato1[1]%></option>
                        <%  }
                                    }%>
                    </select>
                </div>
            </td>

            <td>
                Telefono &nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp Extension<br>
                <input type="text" id="tel_emp_nat" name="tel_emp_nat" value="<%= (vista.equals("0") || bean_lab == null) ? "" : bean_lab.getTelefono()%>" size="15" onkeyup="soloNumeros(this.id);">
                <input type="text" id="ext_emp_nat" name="ext_emp_nat" value="<%= (vista.equals("0") || bean_lab == null) ? "" : bean_lab.getExtension()%>" size="5" onkeyup="soloNumeros(this.id);">
            </td>

        </tr>
        <tr class="filaazul">
            <td colspan="2">
                Cargo<br>
                <input type="text" id="car_emp_nat" name="car_emp_nat" value="<%= (vista.equals("0") || bean_lab == null) ? "" : bean_lab.getCargo()%>" size="60" >
            </td>

            <td>
                Fecha de ingreso (yyyy-MM-dd)<br>
                <input type="text" id="f_ing_nat" class="validate-date" name="f_ing_nat" value="<%= (vista.equals("0") || bean_lab == null) ? "" : Util.formatoFechaPostgres(bean_lab.getFechaIngreso())%>" size="15" >
            </td>
            <td>
                Salario/Mesada/Ingreso mes<br>
                $ <input type="text" id="sal_nat" name="sal_nat" value="<%= (vista.equals("0") || bean_lab == null) ? "" : Util.customFormat(Double.parseDouble(bean_lab.getSalario()))%>" size="25" onkeyup="soloNumeros(this.id);" onblur="formato(this, 0)">
            </td>
            <td class="filableach">
                Otros ingresos<br>
                $ <input type="text" id="otros_nat" name="otros_nat" value="<%= (vista.equals("0") || bean_lab == null) ? "" : Util.customFormat(Double.parseDouble(bean_lab.getOtrosIngresos()))%>" size="25" onkeyup="soloNumeros(this.id);" onblur="formato(this, 0)">
            </td>
    </table>

</div>
<div id="codeudor" style="visibility: hidden;">
    <b>CODEUDOR</b><br>
    <b>Informaci&oacute;n b&aacute;sica</b>
    <table id="tcodeudor" style="border-collapse:collapse; width:100%" border="1">
        <tr class="filaazul">
            <td>
                Identificacion<br>
                <select id="tipo_id_cod" name="tipo_id_cod">
                    <option value="" selected>...</option>
                    <%  dato1 = null;
                                for (int i = 0; i < tipo_id.size(); i++) {
                                    dato1 = ((String) tipo_id.get(i)).split(";_;");%>
                    <option value="<%=dato1[0]%>" <%=(dato1[0].equals((vista.equals("0") || bean_perc == null) ? " " : bean_perc.getTipoId())) ? "selected" : ""%> ><%=dato1[1]%></option>
                    <%  }%>
                </select>
                <input type="text" id="id_cod" name="id_cod" value="<%=(vista.equals("0") || bean_perc == null) ? "" : bean_perc.getIdentificacion()%>" size="20" onkeyup="soloNumeros(this.id);">
                <img alt="buscar" src="<%=BASEURL%>/images/botones/iconos/lupa.gif" width="15" height="15" style="cursor:pointer;" onclick="busqueda('id_cod');">
            </td>

            <td width="17%">
                Primer apellido<br>
                <input type="text" id="pr_apellido_cod" name="pr_apellido_cod" value="<%=(vista.equals("0") || bean_perc == null) ? "" : bean_perc.getPrimerApellido()%>" size="25"  >
            </td>
            <td class="filableach" width="16%">
                Segundo Apellido<br>
                <input type="text" id="seg_apellido_cod" name="seg_apellido_cod" value="<%=(vista.equals("0") || bean_perc == null) ? "" : bean_perc.getSegundoApellido()%>" size="25"  >
            </td>
            <td width="17%">
                Primer Nombre<br>
                <input type="text" id="pr_nombre_cod" name="pr_nombre_cod" value="<%=(vista.equals("0") || bean_perc == null) ? "" : bean_perc.getPrimerNombre()%>" size="25 " >
            </td>
            <td class="filableach" width="17%">
                Segundo Nombre<br>
                <input type="text" id="seg_nombre_cod" name="seg_nombre_cod" value="<%=(vista.equals("0") || bean_perc == null) ? "" : bean_perc.getSegundoApellido()%>" size="25"  >
            </td>
        </tr>
        <tr class="filaazul" >

            <td width="16%" >
                Genero<br>
                <select id="genero_cod" name="genero_cod">
                    <option value="">...</option>
                    <option value="M" <%= (vista.equals("0") || bean_perc == null) ? "" : bean_perc.getGenero().equals("M") ? "selected" : ""%>>Masculino</option>
                    <option value="F" <%= (vista.equals("0") || bean_perc == null) ? "" : bean_perc.getGenero().equals("F") ? "selected" : ""%>>Femenino</option>
                </select>
            </td>
            <td>
                Fecha de nacimiento (yyyy-MM-dd)<br>
                <input type="text" id="f_nac_cod" class="validate-date" onblur="validarEdad(this.id, 'Codeudor')" name="f_nac_cod" value="<%=(vista.equals("0") || bean_perc == null) ? "" : Util.formatoFechaPostgres(bean_perc.getFechaNacimiento())%>" size="15" >
            </td>
            <td class="filableach">
                Celular<br>
                <input type="text" id="cel_cod" name="cel_cod" value="<%=(vista.equals("0") || bean_perc == null) ? "" : bean_perc.getCelular()%>" size="15" onkeyup="soloNumeros(this.id);">
            </td>
            <td class="filableach">
                E-mail<br>
                <input type="text" id="mail_cod" name="mail_cod" value="<%=(vista.equals("0") || bean_perc == null) ? "" : bean_perc.getEmail()%>" size="30">
            </td>
             <td>
                Tipo de vivienda<br>
                <select id="tipo_viv_cod" name="tipo_viv_cod" style="width:10em;">
                    <option value="">...</option>
                    <% for (int i = 0; i < tipo_vivienda.size(); i++) {%>
                    <option value="<%=tipo_vivienda.get(i).getCodigo()%>" <%=(tipo_vivienda.get(i).getCodigo().equals((vista.equals("0") || bean_perc == null) ? " " : bean_perc.getTipoVivienda())) ? "selected" : ""%> ><%=tipo_vivienda.get(i).getValor()%></option>
                    <%  }%>
                </select>
            </td>
        </tr>

        <tr class="filaazul">

            <td colspan="2">
                Direccion residencia<br>
                <input type="text" id="dir_cod" name="dir_cod" value="<%=(vista.equals("0") || bean_perc == null) ? "" : bean_perc.getDireccion()%>" size="60" onblur="dircobro(this.id, 'dir_cob_cod');">
            </td>
            <td >
                Departamento<br>
                <select id="dep_cod" name="dep_cod" style="width:20em;" onchange="cargarCiudades('dep_cod', 'ciu_cod');">
                    <option value="">...</option>
                    <%  dato1 = null;
                                for (int j = 0; j < optdep.size(); j++) {
                                    dato1 = ((String) optdep.get(j)).split(";_;");%>
                    <option value="<%=dato1[0]%>" <%=(dato1[0].equals((vista.equals("0") || bean_perc == null) ? " " : bean_perc.getDepartamento())) ? "selected" : ""%> ><%=dato1[1]%></option>
                    <%  }%>
                </select>
            </td>
            <td>
                Ciudad<br>
                <div id="d_ciu_cod">
                    <select id="ciu_cod" name="ciu_cod" style="width:20em;">
                        <option value="">...</option>
                        <%  if (!vista.equals("0") && bean_perc != null) {
                                        optciu = gsaserv.listadoCiudades(bean_perc.getDepartamento());
                                        for (int j = 0; j < optciu.size(); j++) {
                                            dato1 = ((String) optciu.get(j)).split(";_;");%>
                        <option value="<%=dato1[0]%>" <%=(dato1[0].equals(bean_perc.getCiudad())) ? "selected" : ""%> ><%=dato1[1]%></option>
                        <%  }
                                    }%>
                    </select>
                </div>
            </td>
            <td>
                Telefono<br>
                <input type="text" id="tel_cod" name="tel_cod" value="<%=(vista.equals("0") || bean_perc == null) ? "" : bean_perc.getTelefono()%>" size="15" onkeyup="soloNumeros(this.id);">
            </td>


        </tr>
  <%
                if (!vista.equals("0")) {
                    try {
                        bean_lab = gsaserv.datosLaboral(num_solicitud, "C");
                    } catch (Exception e) {
                        System.out.println("error: " + e.toString());
                        e.printStackTrace();
                    }
                }
    %>
        <tr class="filaazul">
            <td colspan="2">
                Direccion Oficina<br>
                <input type="text" id="dir_emp_cod" name="dir_emp_cod" value="<%= (vista.equals("0") || bean_lab == null) ? "" : bean_lab.getDireccion()%>" size="60">
            </td>
            <td>
                Departamento<br>
                <select id="dep_emp_cod" name="dep_emp_cod" style="width:20em;" onchange="cargarCiudades('dep_emp_cod', 'ciu_emp_cod');">
                    <option value="">...</option>
                    <%  dato1 = null;
                                for (int j = 0; j < optdep.size(); j++) {
                                    dato1 = ((String) optdep.get(j)).split(";_;");%>
                    <option value="<%=dato1[0]%>" <%=(dato1[0].equals((vista.equals("0") || bean_lab == null) ? " " : bean_lab.getDepartamento())) ? "selected" : ""%> ><%=dato1[1]%></option>
                    <%  }%>
                </select>
            </td>
            <td>
                Ciudad<br>
                <div id="d_ciu_emp_cod">
                    <select id="ciu_emp_cod" name="ciu_emp_cod" style="width:20em;">
                        <option value="">...</option>
                        <%  if (!vista.equals("0") && bean_lab != null) {
                                        optciu = gsaserv.listadoCiudades(bean_lab.getDepartamento());
                                        for (int j = 0; j < optciu.size(); j++) {
                                            dato1 = ((String) optciu.get(j)).split(";_;");%>
                        <option value="<%=dato1[0]%>" <%=(dato1[0].equals(bean_lab.getCiudad())) ? "selected" : ""%> ><%=dato1[1]%></option>
                        <%  }
                                    }%>
                    </select>
                </div>
            </td>
            <td>
                Telefono &nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp Extension<br>
                <input type="text" id="tel_emp_cod" name="tel_emp_cod" value="<%= (vista.equals("0") || bean_lab == null) ? "" : bean_lab.getTelefono()%>" size="15" onkeyup="soloNumeros(this.id);">
                <input type="text" id="ext_emp_cod" name="ext_emp_cod" value="<%= (vista.equals("0") || bean_lab == null) ? "" : bean_lab.getExtension()%>" size="5" onkeyup="soloNumeros(this.id);">
            </td>
        </tr>
        <tr class="filaazul">
            <td colspan="2">
                Cargo<br>
                <input type="text" id="car_emp_cod" name="car_emp_cod" value="<%= (vista.equals("0") || bean_lab == null) ? "" : bean_lab.getCargo()%>" size="60" >
            </td>
            <td>
                Fecha de ingreso (yyyy-MM-dd)<br>
                <input type="text" id="f_ing_cod" class="validate-date" name="f_ing_cod" value="<%= (vista.equals("0") || bean_lab == null) ? "" : Util.formatoFechaPostgres(bean_lab.getFechaIngreso())%>" size="15">
            </td>
            <td>
                Salario/Mesada/Ingreso mes<br>
                $ <input type="text" id="sal_cod" name="sal_cod" value="<%= (vista.equals("0") || bean_lab == null) ? "" : Util.customFormat(Double.parseDouble(bean_lab.getSalario()))%>" size="15" onkeyup="soloNumeros(this.id);" onblur="formato(this, 0)">
            </td>
            <td class="filableach">
                Otros ingresos<br>
                $ <input type="text" id="otros_cod" name="otros_cod" value="<%= (vista.equals("0") || bean_lab == null) ? "" : Util.customFormat(Double.parseDouble(bean_lab.getOtrosIngresos()))%>" size="15" onkeyup="soloNumeros(this.id);" onblur="formato(this, 0)">
            </td>
        </tr>

    </table>   
</div>
<div id="estudiante" style="visibility: hidden;">
    <b>ESTUDIANTE</b><br>
    <b>Informaci&oacute;n b&aacute;sica</b>
    <table id="testudiante" style="border-collapse:collapse; width:100%" border="1">
        <tr class="filaazul">
            <td>
                Identificacion<br>
                <select id="tipo_id_est" name="tipo_id_est">
                    <option value="" selected>...</option>
                    <%  dato1 = null;
                                for (int i = 0; i < tipo_id.size(); i++) {
                                    dato1 = ((String) tipo_id.get(i)).split(";_;");%>
                    <option value="<%=dato1[0]%>" <%=(dato1[0].equals((vista.equals("0") || bean_pere == null) ? " " : bean_pere.getTipoId())) ? "selected" : ""%> ><%=dato1[1]%></option>
                    <%  }%>
                </select>
                <input type="text" id="id_est" name="id_est" value="<%= (vista.equals("0") || bean_pere == null) ? "" : bean_pere.getIdentificacion()%>" size="20" onkeyup="soloNumeros(this.id);">
                <img alt="buscar" src="<%=BASEURL%>/images/botones/iconos/lupa.gif" width="15" height="15" style="cursor:pointer;" onclick="busqueda('id_est');">
            </td>

            <td width="17%">
                Primer Apellido<br>
                <input type="text" id="pr_apellido_est" name="pr_apellido_est" value="<%= (vista.equals("0") || bean_pere == null) ? "" : bean_pere.getPrimerApellido()%>" size="25" >
            </td>
            <td class="filableach" width="16%">
                Segundo Apellido<br>
                <input type="text" id="seg_apellido_est" name="seg_apellido_est" value="<%= (vista.equals("0") || bean_pere == null) ? "" : bean_pere.getSegundoApellido()%>" size="25" >
            </td>
            <td width="17%">
                Primer Nombre<br>
                <input type="text" id="pr_nombre_est" name="pr_nombre_est" value="<%= (vista.equals("0") || bean_pere == null) ? "" : bean_pere.getPrimerNombre()%>" size="25" >
            </td>
            <td class="filableach" width="17%">
                Segundo Nombre<br>
                <input type="text" id="seg_nombre_est" name="seg_nombre_est" value="<%= (vista.equals("0") || bean_pere == null) ? "" : bean_pere.getSegundoNombre()%>" size="25" >
            </td>

        </tr>
        <tr class="filaazul">

            <td width="16%">
                Genero<br>
                <select id="genero_est" name="genero_est">
                    <option value="">...</option>
                    <option value="M" <%= (vista.equals("0") || bean_pere == null) ? "" : bean_pere.getGenero().equals("M") ? "selected" : ""%>>Masculino</option>
                    <option value="F" <%= (vista.equals("0") || bean_pere == null) ? "" : bean_pere.getGenero().equals("F") ? "selected" : ""%>>Femenino</option>
                </select>
            </td>
            <td>
                Fecha de nacimiento (yyyy-MM-dd)<br>
                <input type="text" id="f_nac_est"  class="validate-date" name="f_nac_est" value="<%= (vista.equals("0") || bean_pere == null) ? "" : Util.formatoFechaPostgres( bean_pere.getFechaNacimiento())%>" size="15" >
            </td>
            <td class="filableach">
                Celular<br>
                <input type="text" id="cel_est" name="cel_est" value="<%= (vista.equals("0") || bean_pere == null) ? "" : bean_pere.getCelular()%>" size="20" onkeyup="soloNumeros(this.id);">
            </td>
            <td >
                E-mail<br>
                <input type="text" id="mail_est" name="mail_est" value="<%= (vista.equals("0") || bean_pere == null) ? "" : bean_pere.getEmail()%>" size="30">
            </td>
             <td>
                Tipo de vivienda<br>
                <select id="tipo_viv_est" name="tipo_viv_est" style="width:10em;">
                    <option value="">...</option>
                    <% for (int i = 0; i < tipo_vivienda.size(); i++) {%>
                    <option value="<%=tipo_vivienda.get(i).getCodigo()%>" <%=(tipo_vivienda.get(i).getCodigo().equals((vista.equals("0") || bean_pere == null) ? " " : bean_pere.getTipoVivienda())) ? "selected" : ""%> ><%=tipo_vivienda.get(i).getValor()%></option>
                    <%  }%>
                </select>
            </td>
        </tr>

        <tr class="filaazul">
            <td colspan="2">
                Direccion Residencia<br>
                <input type="text" id="dir_est" name="dir_est" value="<%= (vista.equals("0") || bean_pere == null) ? "" : bean_pere.getDireccion()%>" size="60">
            </td>
            <td>
                Departamento<br>
                <select id="dep_est" name="dep_est" style="width:20em;" onchange="cargarCiudades('dep_est', 'ciu_est');">
                    <option value="">...</option>
                    <%  dato1 = null;
                                for (int j = 0; j < optdep.size(); j++) {
                                    dato1 = ((String) optdep.get(j)).split(";_;");%>
                    <option value="<%=dato1[0]%>" <%=(dato1[0].equals((vista.equals("0") || bean_pere == null) ? " " : bean_pere.getDepartamento())) ? "selected" : ""%> ><%=dato1[1]%></option>
                    <%  }%>
                </select>
            </td>
            <td>
                Ciudad<br>
                <div id="d_ciu_est">
                    <select id="ciu_est" name="ciu_est" style="width:20em;">
                        <option value="">...</option>
                        <%  if (!vista.equals("0") && bean_pere != null) {
                                        optciu = gsaserv.listadoCiudades(bean_pere.getDepartamento());
                                        for (int j = 0; j < optciu.size(); j++) {
                                            dato1 = ((String) optciu.get(j)).split(";_;");%>
                        <option value="<%=dato1[0]%>" <%=(dato1[0].equals(bean_pere.getCiudad())) ? "selected" : ""%> ><%=dato1[1]%></option>
                        <%  }
                                    }%>
                    </select>
                </div>
            </td>
            <td>
                Telefono residencia<br>
                <input type="text" id="tel_est" name="tel_est" value="<%= (vista.equals("0") || bean_pere == null) ? "" : bean_pere.getTelefono()%>" size="15" onkeyup="soloNumeros(this.id);">
            </td>
        </tr>
        <tr class="filaazul">
            <td colspan="2">
                Nombre de la universidad<br>
                <input type="text" id="nom_uni" name="nom_uni" value="<%= (vista.equals("0") || bean_est == null) ? "" : bean_est.getUniversidad()%>" size="60" >
            </td>
            <td>
                Programa<br>
                <input type="text" id="nom_progr" name="nom_progr" value="<%= (vista.equals("0") || bean_est == null) ? "" : bean_est.getPrograma()%>" size="30" >
            </td>
            <td>
                Codigo alumno<br>
                <input type="text" id="cod_est" name="cod_est" value="<%=(vista.equals("0") || bean_est == null) ? "" : bean_est.getCodigo()%>" size="15">
            </td>
            <td>
                Semest. a cursar<br>
                <input type="text" id="sem_est" name="sem_est" value="<%= (vista.equals("0") || bean_est == null) ? "" : bean_est.getSemestre()%>" size="5" onkeyup="soloNumeros(this.id);">
            </td>

        </tr>

    </table>

</div>
<div id="juridica" style="display:none; visibility: hidden;">
    <b>PERSONA JURIDICA</b><br>
    <b>Informaci&oacute;n b&aacute;sica</b>
    <table id="tjuridica" style="border-collapse:collapse; width:100%" border="1">
        <tr class="filaazul">
            <td >
                Nit<br>
                <input type="text" id="nit" name="nit" value="<%= (vista.equals("0")) ? "" : bean_pers.getIdentificacion()%>" size="25" <%= vista.equals("8")?"readonly":""%> >
                <img alt="buscar" src="<%=BASEURL%>/images/botones/iconos/lupa.gif" width="15" height="15" style="cursor:pointer;" onclick="busqueda('nit');">
            </td>
            <td colspan="2">
                Razon social<br>
                <input type="text" id="razon_social" name="razon_social" value="<%= (vista.equals("0")) ? "" : bean_pers.getNombre()%>" size="60">
            </td>
            <td colspan="2">
                E-mail<br>
                <input type="text" id="mail_jur" name="mail_jur" value="<%= (vista.equals("0") || bean_pers == null) ? "" : bean_pers.getEmail()%>" size="60">
            </td>
        </tr>

        <tr class="filaazul">
            <td>
                Telefono 1<br>
                <input type="text" id="telefono1_jur" name="telefono1_jur" value="<%= (vista.equals("0") || bean_pers == null) ? "" : bean_pers.getTelefono()%>" size="25" onkeyup="soloNumeros(this.id);">
            </td>
            <td colspan="2">
                Direccion<br>
                <input type="text" id="dir_jur" name="dir_jur" value="<%= (vista.equals("0") || bean_pers == null) ? "" : bean_pers.getDireccion()%>" size="60">
            </td>
            <td>
                Departamento<br>
                <select id="dep_jur" name="dep_jur" style="width:20em;" onchange="cargarCiudades('dep_jur', 'ciu_jur');">
                    <option value="">...</option>
                    <%  dato1 = null;
                                for (int j = 0; j < optdep.size(); j++) {
                                    dato1 = ((String) optdep.get(j)).split(";_;");%>
                    <option value="<%=dato1[0]%>" <%=(dato1[0].equals((vista.equals("0") || bean_pers == null) ? " " : bean_pers.getDepartamento())) ? "selected" : ""%> ><%=dato1[1]%></option>
                    <%  }%>
                </select>
            </td>
            <td>
                Ciudad<br>
                <div id="d_ciu_jur">
                    <select id="ciu_jur" name="ciu_jur" style="width:20em;">
                        <option value="">...</option>
                        <%  if (!vista.equals("0") && bean_pers != null) {
                                        optciu = gsaserv.listadoCiudades(bean_pers.getDepartamento());
                                        for (int j = 0; j < optciu.size(); j++) {
                                            dato1 = ((String) optciu.get(j)).split(";_;");%>
                        <option value="<%=dato1[0]%>" <%=(dato1[0].equals(bean_pers.getCiudad())) ? "selected" : ""%> ><%=dato1[1]%></option>
                        <%  }
                                    }%>
                    </select>
                </div>
            </td>
        </tr>
        <tr class="filaazul">
            <td class="filableach">
                Celular<br>
                <input type="text" id="cel_jur" name="cel_jur" value="<%= (vista.equals("0") || bean_pers == null) ? "" : bean_pers.getCelular()%>" size="25" onkeyup="soloNumeros(this.id);">
            </td>
            <td colspan="2">
                Representante legal<br>
                <input type="text" id="rep_legal" name="rep_legal" value="<%= (vista.equals("0")) ? "" : bean_pers.getRepresentanteLegal()%>" size="50">
            </td>            
            <td >
                Identificacion<br>
                <select id="tipo_id_repr" name="tipo_id_repr">
                    <option value="">...</option>
                    <%  dato1 = null;
                                for (int i = 0; i < tipo_id.size(); i++) {
                                    dato1 = ((String) tipo_id.get(i)).split(";_;");%>
                    <option value="<%=dato1[0]%>" <%=(dato1[0].equals((vista.equals("0") || bean_pers == null) ? " " : bean_pers.getTipoIdRepresentante())) ? "selected" : ""%> ><%=dato1[1]%></option>
                    <%  }%>
                </select>
                <input type="text" id="id_repr" name="id_repr" value="<%= (vista.equals("0")) ? "" : bean_pers.getIdRepresentante()%>" size="20" onkeyup="soloNumeros(this.id);">
            </td>
            <td>
                Genero<br>
                <select id="gen_repr" name="gen_repr">
                    <option value="">...</option>
                    <option value="M" <%= ((vista.equals("0")) ? "" : bean_pers.getGeneroRepresentante().equals("M") ? "selected" : "")%>>Masculino</option>
                    <option value="F" <%= ((vista.equals("0")) ? "" : bean_pers.getGeneroRepresentante().equals("F") ? "selected" : "")%>>Femenino</option>
                </select></td>
        </tr>       
    </table>
    <br>

</div><br>
    </body>
</html>



