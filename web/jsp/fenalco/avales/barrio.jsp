<%-- 
    Document   : barrio
    Created on : 29/12/2016, 08:58:09 AM
    Author     : mcastillo
--%>

<%@page contentType="text/html" pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
        <title>Barrios</title>
        <link type="text/css" rel="stylesheet" href="./css/jquery/jquery-ui/jquery-ui.css"/>
        <link type="text/css" rel="stylesheet" href="./css/jquery/jquery.jqGrid/ui.jqgrid.css"/>        
        <script type="text/javascript" src="./js/jquery/jquery-ui/jquery.min.js"></script>
        <script type="text/javascript" src="./js/jquery/jquery-ui/jquery.ui.min.js"></script>            
        <script type="text/javascript" src="./js/jquery/jquery.jqGrid/js/grid.locale-es.js"></script>
        <script type="text/javascript" src="./js/jquery/jquery.jqGrid/js/jquery.jqGrid.min.js"></script>
        <link type="text/css" rel="stylesheet" href="./css/buttons/botonVerde.css"/>
        <link type="text/css" rel="stylesheet" href="./css/popup.css"/>
        <link href="./css/style_azul.css" rel="stylesheet" type="text/css">
<!--        <link href="./css/requisiciones.css" rel="stylesheet" type="text/css"/>  -->
       <link type="text/css" rel="stylesheet" href="./css/TransportadorasApi.css" />
        <script type="text/javascript" src="./js/barrios.js"></script> 
    </head>
    <body>
        <div id="capaSuperior" style="position:absolute; width:100%; height:100px; z-index:0; left: 0px; top: 0px;">
             <jsp:include page="/toptsp.jsp?encabezado=Ingresar barrios"/>
        </div>
        <div id="capaCentral" style="position:absolute; width:100%; height:83%; z-index:0; left: 0px; top: 100px; overflow:auto;">   
          <center>              
            <div id="div_asoc_barrio_ciudad"  style="width: 570px">
                <div id="filtros" class="ui-jqgrid ui-widget ui-widget-content ui-corner-all" style="height:650px;padding: 0px 10px 0px 0px;">
                    <div id="encabezadotablita" style="width: 565px">
                        <label class="titulotablita"><b>MAESTRO BARRIOS</b></label>
                    </div> 
                    </br>
                    <table aling="center" style=" width: 100%;margin-left:30px" >
                        <tr>  
                            <td style="width: 10%"><span>Departamento:</span></td>
                            <td style="width: 30%"> <select name="departamento" style="font-size: 14px;width:225px" id="departamento">
                                </select></td>                                               
                        </tr> 
                        <tr>
                            <td style="width: 10%"><span>Ciudad</span></td>
                            <td style="width: 30%"> <select name="ciudad" style="font-size: 14px;width:225px" id="ciudad"> 
                                </select>&nbsp;
<!--                            <img src="/fintra/images/file_new.png" width="26" height="23" border="0" align="ABSMIDDLE" onclick="AbrirDivCargarBarrios();" alt="Cargar barrios"  title="Cargar barrios" />-->
                            </td>       
                        </tr>                  
                    </table>   
                </div>
                <div id="div_barrios" style="display:none;position:absolute; padding: 10px 50px 30px 50px; top: 130px; width: 300px ">    
                        <table border="0"  align="center">
                            <tr>
                                <td>
                                    <table id="tabla_barrios" ></table>
                                    <div id="page_tabla_barrios"></div>
                                </td>
                            </tr>
                        </table> 
                    </div>                         
                </div>        
          </center>  
                    <div id="div_barrio" title="CREAR BARRIO" style="display: none; width: 300px;padding:10px;" >  
                        <label> Nombre <span style="color:red;">* &nbsp;</span></label> 
                        <input type="text" id="nombre" name="nombre" style=" width: 450px">
                    </div> 
                    <div id="div_editar_barrio" title="EDITAR BARRIO" style="display: none; width: 300px;padding:10px;" >  
                        <input type="hidden" id="idBarrio" name="idBarrio"> 
                        <label> Nombre <span style="color:red;">* &nbsp;</span></label> 
                        <input type="text" id="nombreEdit" name="nombre" style=" width: 450px">
                    </div> 
    </div>
          
            <div id="dialogUploadFile" style="display:none;">
                <table border="0" style="width:850px">
                    <tr>                      
                        <td style="font-size: 12px;width:117px;padding-top: 5px">
                            Seleccione archivo
                        </td>
                        <td style="padding-top:12px">
                            <form id="formulario" name="formulario">
                                <input type="file" id="examinar" name="examinar"  style="font-size: 11px;width:230px" >
                            </form>
                        </td>
                        <td style="padding-top:  12px">
                            <div id ='botones'>
                                <button id="subirArchivo" class="ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only" 
                                        role="button" aria-disabled="false" >
                                    <span class="ui-button-text">Subir Archivo</span>
                                </button> 
                            </div>
                        </td>
                    </tr>
                </table>
            </div>        
                
            <div id="dialogMsg" title="Mensaje" style="display:none;">
                <p style="font-size: 12.5px;text-align:justify;" id="msj" > Texto </p>
            </div>  
    </body>
</html>
