<%@page import="java.util.ArrayList"%>
<%@page import="com.tsp.operation.model.services.TrayectoriaClienteService"%>
<%@page contentType="text/html" pageEncoding="ISO-8859-1"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">

<%  response.setHeader("Content-Type", "text/html; charset=windows-1252");
            response.setHeader("Pragma", "no-cache");
            response.setHeader("Expires", "0");
            response.setHeader("Cache-Control", "no-cache");
            response.setHeader("Cache-Control", "no-store");
            response.setHeader("Cache-Control", "must-revalidate");%>


<%@page session    ="true"%>
<%@page errorPage  ="/error/ErrorPage.jsp"%>
<%@include file    ="/WEB-INF/InitModel.jsp"%>
<%@page import    ="java.util.List" %>

<%@page session="true"%>
<%@page import="java.util.*"%>
<%@page import="com.tsp.operation.model.*"%>
<%@page import="com.tsp.operation.model.beans.*"%>
<%@page import="com.tsp.operation.model.services.*"%>
<%@page import="java.text.DecimalFormat"%>
<%@page import="com.tsp.util.*"%>



<%

            String vista = (request.getParameter("vista") == null) ? "" : request.getParameter("vista");
            ArrayList<String> avalistas = new ArrayList();
            GenerarFacturasAvalService factserv=new  GenerarFacturasAvalService();
            avalistas=factserv.buscarAvalistas();
            int corte = 0;
            if (vista.equals("1")) {
                corte = Integer.parseInt(model.tablaGenService.getDato("FCHCXCAVAL", "01"));//Fecha de corte para la generación de la CxC de aval
            } else if (vista.equals("2")) {
                corte = Integer.parseInt(model.tablaGenService.getDato("FCHCXPAVAL", "01"));//Fecha de corte para la generación de la CxP de aval
            }


            session = request.getSession();



%>


<script src="<%=BASEURL%>/js/prototype.js"      type="text/javascript"></script>
<script src="<%=BASEURL%>/js/effects.js"        type="text/javascript"></script>
<script src="<%=BASEURL%>/js/window.js"         type="text/javascript"></script>
<script src="<%=BASEURL%>/js/window_effects.js" type="text/javascript"></script>
<script src="<%=BASEURL%>/js/boton.js"          type="text/javascript"></script>
<script src="<%=BASEURL%>/js/GenerarFacturasAval.js"          type="text/javascript"></script>

<link href="<%= BASEURL%>/css/estilostsp.css" rel="stylesheet" type="text/css">





<script type="text/javascript" language="javascript">
    /*jpinedo*/

    function Enviar()
    {

        if(document.getElementById('id').value!='')
        {
            document.forms[0].submit();
        }
        else
        {
            alert("Digite El Numero De Identificacion");
        }
    }




	
</script>



<style type="text/css">
    .fila{text-align:left; font-size:12px; }
    .fila x {text-align: justify; font-size:15px; color:#F00 }
    /*jpinedo*/
    .filax{ font-size:11px; font-family:Georgia, "Times New Roman", Times, serif; text-align:left; background-color: #E8FFE8}

    #tabla_datos{ border-color:#000}
    .tr_ver{font-size:11px; font-family:Verdana, Geneva, sans-serif;  text-align:left; background-color: #093; border-color: #000; color:#FFF; cursor:pointer}


</style>

<head>
    <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
    <title><%=vista.equals("1") ? "Generar CxC de Aval" : (vista.equals("2") ? "Generar CxP de Aval" : "")%></title>

</head>

<body   onload="inizializarvar('<%=CONTROLLER%>','<%= BASEURL%>');" >
    <div id="capaSuperior" style="position:absolute; width:100%; height:100px; z-index:0; left: 0px; top: 0px;">
        <%if (vista.equals("1")) {%>
        <jsp:include page="/toptsp.jsp?encabezado=Generar CxC de Aval"/>
        <%} else if (vista.equals("2")) {%>
        <jsp:include page="/toptsp.jsp?encabezado=Generar CxP de Aval"/>
        <%}%>
    </div>

    <div id="capaCentral" style="position:absolute; width:100%; height:85%; z-index:0; left: 0px; top: 100px; overflow: scroll;">
        <div id="contenido" style="width: 600px; height: 300px; display: none; visibility: hidden; background-color: #EEEEEE;">

            <br>
            <div id="divres" style="width: 100%; height: 200px; overflow: auto;"></div>
            <br>
            <div align="center">
                <img alt="" src="<%=BASEURL%>/images/botones/salir.gif" style = "cursor:pointer" name = "imgsalirx" onclick = "cerrarDiv();" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);">
            </div>
        </div>
        <br>

        <form  id="form" name="form" method="post" action="<%=CONTROLLER%>?estado=Trayectoria&accion=Cliente&opcion=1">
            <table width="90%" border="2" align="center">
                <tr>
                    <td>
                        <table width="100%" align="center" cellpadding="3" cellspacing="2" class="tablaInferior">
                            <tr class="fila">
                                <td colspan="2" class="subtitulo1">Criterios de Selección<input id="vista" name="vista" type="hidden" value="<%=vista%>"/></td>
                                <td colspan="6" class="barratitulo"><img src="<%=BASEURL%>/images/titulo.gif" align="left"></td>

                            </tr>
                            <tr class="fila">
                                <td width="10%">Periodo</td>
                                <td width="22%">
                                    <input id="dia" name="dia" type="hidden" value="<%=corte%>"/>
                                    <select id="mes" name="mes" onchange="actualizarCorte();">
                                        <option value="1">Ene</option>
                                        <option value="2">Feb</option>
                                        <option value="3">Mar</option>
                                        <option value="4">Abr</option>
                                        <option value="5">May</option>
                                        <option value="6">Jun</option>
                                        <option value="7">Jul</option>
                                        <option value="8">Ago</option>
                                        <option value="9">Sep</option>
                                        <option value="10">Oct</option>
                                        <option value="11">Nov</option>
                                        <option value="12">Dic</option>
                                    </select>
                                    <select id="anio" name="anio" onchange="actualizarCorte();">
                                        <%  int hoy = Integer.parseInt(UtilFinanzas.customFormatDate(new Date(), "yyyy"));
                                                    for (int i = hoy - 2; i <= hoy + 2; i++) {%>
                                        <option value="<%= i%>"  <%= ((i == hoy) ? " selected " : "")%> > <%= i%> </option>
                                        <%	}%>
                                    </select>
                                </td>

                                <td width="10%">Fecha Inicial</td>
                                <td width="22%">
                                    <input type="text" name="fechaInicial" id="fechaInicial" class='textbox' size="10" readonly value="2012-01-<%=corte%>" style='width:100'>
                                </td>
                                <td width="10%">Fecha Final</td>
                                <td width="22%">
                                    <input type="text" name="fechaFinal" id="fechaFinal" class='textbox' size="10" readonly value="2012-02-<%=(corte - 1)%>" style='width:100'></td>

                            </tr>
                            <tr class="fila">
                                <%if (vista.equals("1")) {%>
                                <td>Aval a Nombre</td>
                                <td>
                                    <select id="anombre" name="anombre" onchange="cargarConvenios(this.selectedIndex);" >
                                        <option value="" selected >..</option>
                                        <option value="true">Si</option>
                                        <option value="false">No</option>
                                    </select>
                                </td>
                               <%} else if (vista.equals("2")) {%>
                               <td>Avalista</td>
                                <td colspan="3">
                                    <select id="avalista" name="avalista" onchange="cargarConvenios(this.selectedIndex);" >
                                        <option value="" selected >..</option>
                                         <%
                                                 if (avalistas != null && avalistas.size() > 0) {
                                                     String row[] = null;
                                                     for (int i = 0; i < avalistas.size(); i++) {
                                                         row = (avalistas.get(i)).split(";_;");
                                            %>
                                            <option value="<%= row[0]%>" ><%= row[1]%></option>
                                            <%
                                                     }
                                                 }
                                            %>
                                    </select>
                                </td>
                                <%}%>
                                <td>Convenio</td>
                                <td>
                                    <div id="d_convenio">

                                        <select id="convenio" name="convenio" style='width:20em;'>
                                        </select>
                                    </div>
                                </td>
                                <%if (vista.equals("1")) {%>
                                <td >Afiliado</td>
                                <td >
                                    <div id="d_afiliado">
                                        <select id="afiliado" name="afiliado" style='width:20em;' >
                                        </select>
                                    </div>
                                </td>
                                 <%}%>
                            </tr>
                        </table>
                    </td>
                </tr>
            </table>

            <br/>

            <div align="center">
                <img style="cursor:pointer" src="<%= BASEURL%>/images/botones/aceptar.gif" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" onClick="cargarNegocios()"/>
                <img style="cursor:pointer" src="<%= BASEURL%>/images/botones/salir.gif" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" onClick="parent.close();"/>
                <br/>
                <br/>
                <div align="center" id="d_contenido" style=" width: 90%;" />
            </div>
        </form>

    </div>

    <iframe width=188 height=166 name="gToday:normal:agenda.js:gfPop:plugins.js" id="gToday:normal:agenda.js:gfPop:plugins.js" src="<%=BASEURL%>/js/Calendario/ipopeng.htm" scrolling="no" frameborder="0" style="visibility:visible; z-index:999; position:absolute; left:-500px; top:0px;"> </iframe>

</body>
</html>

