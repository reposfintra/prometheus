<%--
    Document   : solicitud_aval
    Created on : 01/03/2011, 09:36:47 AM
    Author     : ivargas
--%>


<%@page import="com.tsp.util.Util"%>
<%@page import="java.text.SimpleDateFormat"%>
<%@page import="java.text.DateFormat"%>
<%@page session="true"%>
<%@page import="java.util.*"%>
<%@page import="com.tsp.operation.model.*"%>
<%@page import="com.tsp.operation.model.beans.*"%>
<%@page import="com.tsp.operation.model.services.*"%>
<%@page import="com.tsp.operation.controller.PresolicitudesMicrocreditoBeans"%>
<%@page import="com.google.gson.JsonArray" %>
<%@page import="com.google.gson.JsonObject"%>
<%@include file="/WEB-INF/InitModel.jsp"%>
<%
            response.addHeader("Cache-Control", "no-cache");
            response.addHeader("Pragma", "No-cache");
            response.addDateHeader("Expires", 0);
            Usuario usuario = (Usuario) session.getAttribute("Usuario");
            GestionSolicitudAvalService gsaserv = new GestionSolicitudAvalService(usuario.getBd());
            WSHistCreditoService wsdatacred= new WSHistCreditoService(usuario.getBd());
            ArrayList optdep = gsaserv.listadoDeps();
            ArrayList optciu = new ArrayList();
            ArrayList optbarrio = new ArrayList();
            ArrayList est_civil = gsaserv.busquedaGeneral("ESTCIV");
            ArrayList nivel_estudio = gsaserv.busquedaGeneral("NIVEST");
            ArrayList <Codigo> tipo_vivienda = wsdatacred.buscarTabla("H", "vivienda");
            ArrayList act_eco = gsaserv.busquedaGeneral("ACTECO");
            ArrayList ocupacion = new ArrayList();
            ArrayList tipo_contrato = gsaserv.busquedaGeneral("TIPCON");
            ArrayList tipo_bien = gsaserv.busquedaGeneral("TIPBIE");
            ArrayList tipo_vehiculo = gsaserv.busquedaGeneral("TIPVEH");
            ArrayList caracter_emp = gsaserv.busquedaGeneral("CAREMP");
            ArrayList tipo_carrera = gsaserv.busquedaGeneral("TIPCAR");
            ArrayList tipo_cuenta = gsaserv.busquedaGeneral("TIPCUE");
            ArrayList <Codigo> productos = wsdatacred.buscarTabla("H", "producto");
            ArrayList <Codigo> matricula = new ArrayList();
            ArrayList <Codigo> servicios = new ArrayList();
            ArrayList parentesco = gsaserv.busquedaGeneral("PARENT");
            ArrayList tipafil = gsaserv.busquedaGeneral("TIPAFIL");
            ArrayList eps = gsaserv.busquedaGeneral("EPS");
            ArrayList tipo_id = gsaserv.busquedaGeneral("TIPID");
            ArrayList convenios = new ArrayList();
            ArrayList <Asesores> asesores = new ArrayList();
            ArrayList sectores = model.proveedorService.buscarSects();
            ArrayList<String> titval = new ArrayList();
            GestionCondicionesService  gserv = new GestionCondicionesService(usuario.getBd());
            ArrayList<String> listaplazos = null;
                                                try {
                                                    listaplazos = gserv.plazoCuota();
                                    }
                                    catch (Exception e) {
                                        System.out.println("Error al obtener la lista de tipos de titulo valor: "+e.toString());
                                                    e.printStackTrace();
                                                }
            String login_into=(String)session.getAttribute("login_into");
            String estadoDireccion = (login_into.equals("Fenalco_bol"))? "" : "readonly";

            String est = request.getParameter("est") != null ? request.getParameter("est") : "N";
            String cod = request.getParameter("cod") != null ? request.getParameter("cod") : "N";
            String cod2 = request.getParameter("cod2") != null ? request.getParameter("cod2") : "N";
            String msj = request.getParameter("msj") != null ? request.getParameter("msj") : "";
            String estado = "B";
            String actividad = "";
            String tipo_proceso="";
            String negocio = "";
            String id_usuario = usuario.getLogin();
            String sel_afils = "";
            int num_solicitud = 1;
            DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
            Date dat = new Date();
            String fec_act = dateFormat.format(dat);
            SolicitudAval bean_sol = null;
            ArrayList<SolicitudCuentas> lista_cuentas = null;
            ArrayList<SolicitudReferencias> lista_referencias = null;
            ArrayList<SolicitudVehiculo> lista_vehiculos = null;
            ArrayList<SolicitudHijos> lista_hijos_s = null;
            ArrayList<SolicitudHijos> lista_hijos_c = null;
            ArrayList<SolicitudHijos> lista_hijos_e = null;
            ArrayList<ObligacionesCompra> obligacionesList=null;
            SolicitudEstudiante bean_est = null;
            SolicitudLaboral bean_lab = null;
            SolicitudPersona bean_pers = null;
            SolicitudPersona bean_perc = null;
            SolicitudPersona bean_pere = null;
            SolicitudNegocio bean_neg = null;
            String vista = request.getParameter("vista") != null ? request.getParameter("vista") : "0";
            String tipoconv = request.getParameter("tipoconv") != null ? request.getParameter("tipoconv") : "Consumo";
            String trans = request.getParameter("trans") != null ? request.getParameter("trans") : "N";
            String[] dato1 = null;
            
            String cp=request.getParameter("compra_cartera") != null ? request.getParameter("compra_cartera") : "N";

            String salariomin="0";
            String montomax="0";
            String montomin="0";
            GestionConveniosService gcserv = new GestionConveniosService(usuario.getBd());
            AsesoresService aserv= new AsesoresService(usuario.getBd());
            ArrayList<String> listacad = null;
            boolean redesc=true;
            String currentDate="0099-01-01";
            try {

                if (!vista.equals("0")) {
                    num_solicitud = (request.getParameter("num_solicitud") != null && request.getParameter("num_solicitud").equals("") == false)
                            ? Integer.parseInt(request.getParameter("num_solicitud")) : 0;
                    bean_sol = gsaserv.buscarSolicitud(num_solicitud);
                    if (vista.equals("20")) bean_sol.setValorSolicitado(String.valueOf(Double.parseDouble(bean_sol.getValorSolicitado())*1.10));
                    lista_cuentas = gsaserv.buscarCuentas(num_solicitud);
                    if(bean_sol!=null)
                    redesc=gcserv.tieneRedescuento(bean_sol.getIdConvenio());
                } else {
                    num_solicitud = Integer.parseInt(gsaserv.numeroSolc());
                    }
                if(tipoconv.equals("Consumo")){
                sel_afils = gsaserv.buscarAfilsUsuario(id_usuario);

                }
                listacad = gcserv.buscarBancos("nombre", "");
                
                //validacion solo para completar formulario de presolicitudes
                if(vista.equals("30")){ 
                         PresolicitudesMicrocreditoBeans pre_solicitud = (PresolicitudesMicrocreditoBeans) session.getAttribute("PresolicitudesMicrocreditoBeans");           
                         bean_sol=pre_solicitud.getSa();
                         bean_sol.setCreationDate(fec_act);
                         bean_sol.setCat(true);                         
                         bean_pers=pre_solicitud.getSp();
                }  

                if(tipoconv.equals("Microcredito")||tipoconv.equals("Multiservicio")){
                    if (vista.equals("0")) {
                          convenios = gcserv.buscarConveniosTipo(tipoconv, trans.equals("S") ? false : true);
                      } else {
                             trans=bean_sol.isCat()?"N":"S";
                             convenios = gcserv.buscarConveniosTipo(tipoconv, bean_sol.isCat());
                    }
//                            asesores=aserv.buscarAsesoresActivos();
                            salariomin=model.tablaGenService.obtenerTablaGen("SALARIOMIN", "01").getDato();
                 }
                if (!vista.equals("0")) {
                    if(tipoconv.equals("Microcredito")||tipoconv.equals("Multiservicio")){
                        bean_neg=gsaserv.buscarNegocio(num_solicitud);
                        BeanGeneral montos = model.gestionConveniosSvc.obtenerMontos(bean_sol.getIdConvenio());
                        montomax=montos.getValor_02();
                        montomin=montos.getValor_01();
                        obligacionesList=gsaserv.buscarObligacionesForm(num_solicitud);
                    }else{
                        titval = gserv.titulosValorConvenio(bean_sol.getIdConvenio());
                    }
                    matricula=wsdatacred.buscarTablaRef("H", "matricula", bean_sol.getProducto());
                    servicios=wsdatacred.buscarTablaRef("H", "servicio", bean_sol.getProducto());
                    bean_pers = vista.equals("30")? bean_pers : gsaserv.buscarPersona(num_solicitud, "S");
                    bean_perc = gsaserv.buscarPersona(num_solicitud, "C");
                    bean_pere = gsaserv.buscarPersona(num_solicitud, "E");
                    lista_hijos_s = gsaserv.buscarHijos(num_solicitud, "S");
                    lista_hijos_c = gsaserv.buscarHijos(num_solicitud, "C");
                    lista_hijos_e = gsaserv.buscarHijos(num_solicitud, "E");
                    estado = bean_sol.getEstadoSol();
                    actividad = bean_sol.getActividadNegocio() != null ? bean_sol.getActividadNegocio() : "";
                    negocio = bean_sol.getCodNegocio();
                    tipo_proceso=model.Negociossvc.getTipoProceso(negocio);
                    if (!bean_pers.getTipoPersona().equals("J")) {
                        lista_referencias = gsaserv.buscarReferencias(num_solicitud, "S", "P");
                        lista_vehiculos = gsaserv.buscarVehiculos(num_solicitud);
                        bean_lab = gsaserv.datosLaboral(num_solicitud, "S");
                    }

                    if (vista.equals("4")||vista.equals("8")||vista.equals("20")) {
                         if(tipoconv.equals("Consumo")){
                            sel_afils = gsaserv.buscarAfils(bean_sol.getAfiliado());
                            convenios = gsaserv.buscarConveniosAfil(bean_sol.getAfiliado());

                            }
                    } else {
                         if(tipoconv.equals("Consumo")){
                        sel_afils = gsaserv.buscarAfilsUsuario(id_usuario, bean_sol.getAfiliado());
                        convenios = gsaserv.buscarConveniosAfil(id_usuario, bean_sol.getAfiliado());
                        }
                        }
                    bean_est = gsaserv.datosEstudiante(num_solicitud);

                }

                if(vista.equals("20"))
                    {
                       model.tablaGenService.buscarDatos("CONVENIO", "FENALCO_ATLANTICO");
                       TablaGen tConvenio = model.tablaGenService.getTblgen();
                       bean_sol.setIdConvenio(tConvenio.getReferencia());
                    }
                
           /* //fecha actual para la renovacion....
                    Date dNow = new Date();
                    SimpleDateFormat ft = new SimpleDateFormat ("MM/dd/yyyy");
                    currentDate = ft.format(dNow);*/
                   
            } catch (Exception e) {
                System.out.println("Error al buscar listados: " + e.toString());
                e.printStackTrace();
            }
%>

<%@taglib uri="http://jakarta.apache.org/taglibs/input-0.90" prefix="input" %>
<%@page contentType="text/html" pageEncoding="ISO-8859-1"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">
<!-- This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>. -->
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
        <link href="<%= BASEURL%>/css/estilostsp.css" rel='stylesheet'>
        <link href="<%=BASEURL%>/css/mac_os_x.css" rel="stylesheet" type="text/css">
        <link href="<%=BASEURL%>/css/default.css" rel="stylesheet" type="text/css">
        <style type="text/css">
            td{ white-space:nowrap;}
            b{
                color:white;
            }

            b{
                color:white;
            }

            .filableach{
                color: #000000;
                font-family: Tahoma, Arial;
                background-color:#FFFFFF;
                font-size: 12px;
            }

            .divs {
                font-family: Verdana, Tahoma, Arial, Helvetica, sans-serif;
                font-size: small;
                color: #000000;
                background-color:#2A88C8;
                -moz-border-radius: 0.7em;
                -webkit-border-radius: 0.7em;
                width: 1512px;
            }
            input.validation-failed, textarea.validation-failed {
                border: 1px solid #FF3300;
                color : #FF3300;
            }
            
            .validation-failed {
                border:2px solid red;
            }
            
            .popup {
                left: 0;
                position: absolute;
                top: 0;
                width: 100%;
                z-index: 1001;
            }
            
            
            .popup_hcg {
                left: 0;
                position: absolute;
                top: 0;
                width: 100%;
                z-index: 1002;
            }            

            .content-popup {
                margin:0px auto;
                margin-top:180px;
                padding:10px;
                width:270px;
                min-height:140px;
                border-radius:4px;
                background-color:#FFFFFF;
                box-shadow: 0 2px 5px #666666;
            }

            .close {
                position:relative;
                left:260px;
               }
               
            .filaazul2 {
                color: #000 !important;
                font-family: Tahoma,Arial;
                font-size: 12px;
                background-color: rgba(255, 255, 255, 0.93) !important;
            }
               
            .titulo_ventana {
                font-family: "Trebuchet MS", Verdana, Arial, Helvetica, sans-serif;
                font-size: 10px;
                border: 1px solid #2A88C8;
                background: #2A88C8;
                color: #ffffff; 
                font-weight: bold;
                -moz-border-radius: 5px; -webkit-border-radius: 5px; border-radius: 5px; 
                padding: .5em 1em .3em; 
                position: relative;
                cursor: move; 	
            }
            .botonHeader:hover {
                 background-color: #2a88c8 !important;
            }
            
        </style>
        <script type='text/javascript' src="<%= BASEURL%>/js/boton.js"></script>
        <script type='text/javascript' src="<%= BASEURL%>/js/prototype.js"></script>
        <script src="<%=BASEURL%>/js/effects.js" type="text/javascript"></script>
        <script src="<%=BASEURL%>/js/window.js" type="text/javascript"></script>
              
        <script type="text/javascript" src="/fintra/js/jquery/jquery-ui/jquery.min.js"></script>
        <script language="JavaScript" type="text/javascript" src="/fintra/js/jquery-ui-1.8.5.custom.min.js"></script>
        <script src="<%=BASEURL%>/js/solicitud_aval.js" type="text/javascript"></script>
        <script src="<%=BASEURL%>/js/validation.js" type="text/javascript" ></script>
        <script type="text/javascript">
            jQuery.noConflict();
            var $j = jQuery;
        </script>
        <title>Solicitud de Credito</title>

        <script type="text/javascript">

 $j(document).ready(function() {    
    cargarVias('BQ', "via_princip_dir");
    cargarVias('BQ', "via_genera_dir");
    $j("#dep_dir").change(function() {
        resetAddressValues();
        var op = $j(this).find("option:selected").val();
        cargarCiudad(op, "ciu_dir"); 
//      cargarVias('BQ', "via_princip_dir");
//      cargarVias('BQ', "via_genera_dir");
    });
    $j("#ciu_dir").change(function() {
        resetAddressValues();
        var op = $j(this).find("option:selected").val();
        cargarVias(op, "via_princip_dir");
        cargarVias(op, "via_genera_dir");
    });
    $j("#via_princip_dir").change(function() {
          $j("#via_genera_dir").val('');
    });   
 });
 
 function resetAddressValues(){
       $j("#dir_resul").val('');
       $j("#nom_princip_dir").val('');
       $j("#nom_genera_dir").val('');
       $j("#placa_dir").val('');
       $j("#cmpl_dir").val('');
 }
 
function abrirVentana(){
    var check =document.getElementById("renovacion").checked;
    var contenedor = document.getElementById("popup");
    if(check == true){
        contenedor.style.display = "block";
        document.body.style.opacity = "0.5";
        return false;
    }
    
} 

function cerrarVentana(){
    var contenedor = document.getElementById("popup");
    document.getElementById("renovacion").checked=false;
        contenedor.style.display = "none";
       // contenedor.style.opacity = "0.5"
        document.body.style.opacity = "1";
        return false;
  
} 


function validarMontorenovacion(field){
    var check =document.getElementById("renovacion").checked;
    var check2 =document.getElementById("preAprobado").checked;
   // var contenedor = document.getElementById("popup");
   if(check === true || check2 === true){
            var monto =parseInt(document.getElementById("motonRenovacion").value);
            var valor_preaprobado=parseInt(replaceAll(field.value,",",""));
        //alert("valor_preaprobado:"+valor_preaprobado+"monto :"+monto);

//            if(valor_preaprobado > monto){
//               document.getElementById("valor_solicitado").value=monto;
//               document.getElementById("valor_producto").value=monto;
//               alert("El valor solicitado no puede ser superior a : $"+monto)
//                
//            }
       // return false;
      }
        
}

function abrirVentanaPreaprobado(){
    var check =document.getElementById("preAprobado").checked;
    var contenedor = document.getElementById("popup2");
    if(check == true){
        contenedor.style.display = "block";
        document.body.style.opacity = "0.5";
        return false;
    }
    
} 

function cerrarVentanaPreaprobado(){
    var contenedor = document.getElementById("popup2");
    document.getElementById("preAprobado").checked=false;
        contenedor.style.display = "none";
       // contenedor.style.opacity = "0.5"
        document.body.style.opacity = "1";
        return false;
  
}

function isNumberKey(evt)
          {


var charCode = (evt.which) ? evt.which : event.keyCode

if (charCode > 31 && (charCode < 48 || charCode > 57))
return false;
return true;

}

    function soloLetras(e) {
        key = e.keyCode || e.which;
        tecla = String.fromCharCode(key).toLowerCase();
        letras = " �����abcdefghijklmn�opqrstuvwxyz";
        especiales = [8, 9, 37, 39, 46];
        
        tecla_especial = false
        for(var i in especiales) {
            if(key == especiales[i]) {
                tecla_especial = true;
                break;
            }
        }
        
        if(letras.indexOf(tecla) == -1 && !tecla_especial)
            return false;
    }
    
    function validaEmail(campo){ 
        var email=campo.value;
        var re  = /^([a-zA-Z0-9_.-])+@(([a-zA-Z0-9-])+.)+([a-zA-Z0-9]{2,4})+$/; 
        if (!re.test(email)) { 
            alert ("Direcci�n de email inv�lida"); 
                    
           return false; 
        } 
       // return true; 
    }




            var valid;
            function xpand_div1(){
                var id_div = document.getElementById("tipo_p").value;
                var tipoconv=document.getElementById("tipoconv").value;
                //validar que titulo valor es.
                
                var combox = document.getElementById("cmbTituloValor");
                var selected = (combox!=null) ? combox.options[combox.selectedIndex].text :"...";
                if(id_div!="" && (selected !="..." || tipoconv=='Microcredito'||tipoconv=='Multiservicio')){
                    $('contenido').innerHTML = $(id_div).innerHTML +
                        "<br>"// + $('cuentas').innerHTML;
                    //validar que titulo valor es.
                  
                    if(selected == "Cheque"){
                        $('contenido').innerHTML += $('cuentas').innerHTML;
                    }
                    var check =document.getElementById("codeu").checked;
                    var check2 =document.getElementById("codeu2")===null ? false : document.getElementById("codeu2").checked;
                    if(check==true || check2==true){
                        $('contenido').innerHTML +=  $('codeudor').innerHTML;
                        if( check2==true ) {
                            document.getElementById('contenido').children['tcodeudor'].innerHTML =  $('tcodeudor2').innerHTML;
                        }
                    } 

                   // var tipoconv=document.getElementById("tipoconv").value;
                    var trans=document.getElementById("trans").value;

                    if(tipoconv=='Microcredito'){
                        if(trans=='N'){
                            $('contenido').innerHTML += "<b>NEGOCIO</b><br>";
                            if(id_div=="juridica"){
                                $('contenido').innerHTML +=  $('negocio2').innerHTML+$('obligaciones_micro').innerHTML;
                            }else{
                                $('contenido').innerHTML +=  $('negocio1').innerHTML+ $('negocio2').innerHTML+ $('refnegocio').innerHTML+$('obligaciones_micro').innerHTML ;
                            }
                        }


                    }else if(tipoconv=='Consumo'){
                        var indice = document.getElementById("producto").selectedIndex;
                        var producto=document.getElementById("producto").options[indice].value;

                        if(producto!='02'){
                            $('servicio').disabled=true;
                            $('matricula').disabled=true;

                            if(producto=='01'){
                                $('contenido').innerHTML +=  $('estudiante').innerHTML;
                                var indice = document.getElementById("afiliado").selectedIndex;
                                document.getElementById("nom_uni").value=document.getElementById("afiliado").options[indice].text;
                                document.getElementById("nom_uni").readOnly="readOnly";
                            }

                        }else{
                            $('servicio').disabled=false;
                            $('matricula').disabled=false;

                                
                                if(<%=vista.equals("8")%>){
                                    $('servicio').disabled=true;
                                    $('matricula').disabled=true;
                                }
                                
                            }
                        }

                    $('contenido').style.border="0.1em solid black";
                    $('contenido').style.padding ="0.3em";
                    <%
                                if (!vista.equals("3") && !vista.equals("8")) {
                    %>
                        $('imgaceptar').style.visibility= "visible";
                                if($("imgguardar")!=null){
                            $('imgguardar').style.visibility= "visible";
                                }
            <%}%>
                    }
                    else{
                        $('contenido').innerHTML = '';
                        $('contenido').style.border="0em solid black";
                        $('contenido').style.padding ="0em";
                        $('imgaceptar').style.visibility= "hidden";
                        if($("imgguardar")!=null){
                            $('imgguardar').style.visibility= "hidden";
                        }
                    }
                    valid = new Validation('formulario', {immediate : true, onFormValidate : formCallback});

                }

                function generarPdf(){
                    var dato = $('num_solicitud').value;
                    var url = "<%=CONTROLLER%>?estado=GestionSolicitud&accion=Aval";
                    var p = "opcion=export_pdf&num_solicitud="+dato;
                    new Ajax.Request(url, { parameters: p, method: 'post', onComplete: completo } );
                }

                function deshab() {
                    frm = document.forms['formulario'];
                    for(i=0; ele=frm.elements[i]; i++)
                        ele.disabled=true;
                }
                function completo(resp){
                    alert(resp.responseText);
                }

<% int op_valida_campos=0;
            if (gsaserv.ValidarSolicitudDevuelta(num_solicitud + "", "SOL", "REF")) {
                op_valida_campos = 1;
            }

            if (gsaserv.ValidarSolicitudDevuelta(num_solicitud + "", "SOL", "RAD")) {
                op_valida_campos = 1;
            }
            if (gsaserv.ValidarSolicitudDevuelta(num_solicitud + "", "SOL", "FOR")) {
                op_valida_campos = 1;
            }
                           
            if (gsaserv.ValidarSolicitudDevuelta(num_solicitud + "", "SOL", "ANA")) {
                op_valida_campos = 2;
            }
%>



function validar_monto()
{
    var valor_preaprobado=parseInt(replaceAll(document.getElementById('valor_producto').value,",",""));
    var valor_solicitado= parseInt(replaceAll(document.getElementById('valor_solicitado').value,",",""));
        
   
    if(valor_solicitado>valor_preaprobado)
    {
           alert("Supera --->mostrar mensaje");
    }
}


function calcular_monto()
{
    var valor_preaprobado=parseInt(replaceAll(document.getElementById('valor_producto').value,",",""));
    var valor_solicitado= parseInt(replaceAll(document.getElementById('valor_solicitado').value,",",""));


    if(valor_solicitado>valor_preaprobado)
    {
           alert("Supera --->mostrar mensaje");
    }
}


function replaceAll( text, busca, reemplaza ){

  while (text.toString().indexOf(busca) != -1)

      text = text.toString().replace(busca,reemplaza);

  return text;

}

   function conMayusculas(field) {
          field.value = field.value.toUpperCase()
}

  function conMinusculas(field) {
          field.value = field.value.toLowerCase();
         }
        </script>
    </head>
    <body  onload="varcontr('<%=CONTROLLER%>');maximizar();<%=vista.equals("0") ? "" : "xpand_div1();"%> <%= (!(vista.equals("3"))||vista.equals("8")) ? "" : "deshab();"%> ; valida_campos(<%=op_valida_campos%>); bloquearDivLoad()">
        <div id="capaSuperior" style="position:absolute; width:100%; height:100px; z-index:0; left: 0px; top: 0px;">
            <%
                        if (vista.equals("0")) {
                            if (login_into.equals("Fenalco")||login_into.equals("Fenalco_bol")) {%>
            <jsp:include page="/toptsp.jsp?encabezado=Solicitud de Credito"/>
            <%} else {%>
            <jsp:include page="/toptsp.jsp?encabezado=Solicitud de Credito"/>
            <%}%>

            <%                                    } else {
                                        if (login_into.equals("Fenalco")) {%>
            <jsp:include page="/toptsp.jsp?encabezado=Modificar Solicitud de Credito"/>
            <%} else {%>
            <jsp:include page="/toptsp.jsp?encabezado=Modificar Solicitud de Credito"/>
            <%}%>

            <%                        }
            %>
        </div>
       
        <div id="capaCentral" style="position:absolute; width:80%; height:83%; z-index:0; left: 5%; top: 120px; ">
            <div id="contenido2" style="visibility: hidden; display: none; height: 150px; width: 500px; background-color: #EEEEEE;"></div>

            <%

                        if ((estado.equals("B") == true && (vista.equals("1") || vista.equals("0"))) || ((estado.equals("P") || estado.equals("Q")|| estado.equals("V")) && vista.equals("4") && actividad.equals("LIQ"))) {
            %>
            <form id="formulario" name="formulario" action="<%=CONTROLLER%>?estado=GestionSolicitud&accion=Aval" method="post">
                <%
                                        } else {
                %>
                <form id="formulario" name="formulario" action="" method="post" >
                    <%                             }
                    %>
                    <div id="header">
                        <table id="tsup" style="border: 0em;  width: 100%; border: 6px solid  #2A88C8">
                            <thead>
                                <tr class="filaazul">
                                    <%if (login_into.equals("Fenalco")) {%>
                                    <th align="center" colspan="9">SOLICITUD DE AVAL</th>
                                    <%} else {%>
                                    <th align="center" colspan="9">SOLICITUD DE CREDITO</th>
                                    <%}%>
                                    <th  style="text-align:right; ">
                                        No. de Formulario
                                    </th>
                                    <th>
                                        <input name="num_solicitud" id="num_solicitud" style="background-color: transparent; border-bottom: 0px; border-left: 0px; border-right: 0px; border-top: 0px; text-align:center; color:#990000; font-size:16px;" value="<%= vista.equals("20")?Integer.parseInt(gsaserv.numeroSolc()): num_solicitud %>"  readonly="readonly">
                                        <input name="actividad" type="hidden" id="actividad" value="<%= actividad%>">
                                        <input name="negocio" type="hidden" id="negocio" value="<%= negocio%>">
                                        <input name="tipoconv" type="hidden" id="tipoconv" value="<%= tipoconv%>">
                                        <input name="trans" type="hidden" id="trans" value="<%= trans%>">
                                        <input name="vista" type="hidden" id="vista" value="<%= vista%>">
                                    </th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr class="filaazul">
                                    <td rowspan="5" width="9%" valign="top">
                                        <%if (login_into.equals("Fenalco")) {%>
                                        <img alt="logofenalco" src="<%=BASEURL%>/images/fenalco_atlantico.jpg"  height="120">
                                        <%} else if(login_into.equals("Fenalco_bol")){%>
                                        <img alt="logofenalcobol" src="<%=BASEURL%>/images/fenalco_bolivar.jpg"  height="120">
                                        <%}else {%>
                                        <img alt="logofintra" src="<%=BASEURL%>/images/logo_fintra.jpg"  height="80">
                                        <%}%>
                                    </td>
                                    <td width="5%">
                                        Fecha
                                    </td>
                                    <td width="20%">
                                        <input type="text" id="fecha_cons" name="fecha_cons" size="15" readonly="readonly"  value="<%=(vista.equals("0")) ?  fec_act :  Util.formatoFechaPostgres(bean_sol.getCreationDate())%>">
                                    </td>
                                  <% if (tipoconv.equals("Microcredito")) {%>
                                    <td width="5%">
                                        Fianza
                                    </td>
                                    <td>
                                        <select name="fianza" id="fianza" onchange="if(<%=!tipoconv.equals("Microcredito")%>) document.getElementById('plazo').value='';" style="width:75px;" >
                                            <option value="S" <%= (bean_sol != null ? ((bean_sol.getFianza().equals("S") ? "selected" : "")) : "selected") %>>SI</option>
                                            <option value="N" <%= (bean_sol != null ? ((bean_sol.getFianza().equals("N") ? "selected" : "")) : "") %>>NO</option>
                                        </select>
                                    </td>
                                  <% } %>
                                    <td colspan="4">
                   
                                    </td>
                                    <td width="8%">
                                        No.aprobacion
                                    </td>
                                    <td width="12%">
                                        <input type="text" id="num_aprobacion" name="num_aprobacion" value="<%=(vista.equals("0")) ? "" : bean_sol.getNumeroAprobacion()%>" disabled>
                                    </td>
                                </tr>
                                <% if (bean_perc != null) {
                                    if (bean_perc.getTipoPersona().equals("N"))  {
                                        cod = "S"; cod2="N";
                                    } else if (bean_perc.getTipoPersona().equals("J"))  {
                                        cod2 = "S"; cod="N";
                                    }
                                } %>
                                <% if (tipoconv.equals("Consumo")) {%>
                                <tr class="filaazul">
                                    <td>
                                        Afiliado
                                    </td>
                                    <td colspan="3">

                                        <select name="afiliado" id="afiliado" onchange="limpiarConv();cargarCodigo(this.selectedIndex);" style="width:90%;" <%=(vista.equals("0") || estado.equals("B")) ? "" : "disabled"%> >
                                            <option value="">...</option>
                                            <%= sel_afils%>
                                        </select>

                                    </td>
                                    <td width="6%">
                                        Convenio
                                    </td>
                                    <td colspan="3" >
                                        <input type="text" id="convtext" name="convtext" readonly value="<%=(vista.equals("0")) ? "" : gcserv.nombreConvenio(bean_sol.getIdConvenio())%>" style="width:90%;">
                                        <img alt="buscar" src="<%=BASEURL%>/images/botones/iconos/lupa.gif" width="15" height="15" style="cursor:pointer; <%= vista.equals("8")?"display: none;":""%> " onclick="<%=(vista.equals("4") || vista.equals("8")) ? "verConvs('" + CONTROLLER + "');" : "verConvsUsuario('" + CONTROLLER + "');"%>">
                                        <input type="hidden" id="convenio" name="convenio"value="<%=(vista.equals("0")) ? "" : bean_sol.getIdConvenio()%>">
                                        <input type="hidden" id="redescuento" name="redescuento"value="<%=(vista.equals("0")) ? "" : redesc%>">

                                    </td>
                                    <td>
                                        Codigo
                                    </td>
                                    <td>
                                        <input name="codigo" onkeyup="soloNumeros(this.id);" type="text" id="codigo" maxlength="2" value="<%= (vista.equals("0")) ? "" : (bean_sol.getCodigo() == null || bean_sol.getCodigo().length() < 9 ? "" : bean_sol.getCodigo().substring(0, 2))%>" size="2" readonly>
                                        <input name="codigo1" onkeyup="soloNumeros(this.id);"type="text" id="codigo1" maxlength="5" value="<%= (vista.equals("0")) ? "" : (bean_sol.getCodigo() == null || bean_sol.getCodigo().length() < 9 ? "" : bean_sol.getCodigo().substring(2, 7))%>" size="5" readonly>
                                        <input name="codigo2" onkeyup="soloNumeros(this.id);"type="text" id="codigo2" maxlength="2"value="<%= (vista.equals("0")) ? "" : (bean_sol.getCodigo() == null || bean_sol.getCodigo().length() < 9 ? "" : bean_sol.getCodigo().substring(7, 9))%>" size="2" <%=vista.equals("8")?"disabled":""%> >
                                    </td>
                                </tr>
                                <tr class="filaazul">
                                    <td>Sector</td>
                                    <td   colspan="3">
                                        <input type="text" id="sector" name="sector" readonly value="<%=(vista.equals("0")) ? "" : (login_into.equals("Fenalco")? gcserv.nombreAlternoSector(bean_sol.getSector()):gcserv.nombreSector(bean_sol.getSector()))%>" style="width:88%;">
                                        <input type="hidden" id="sectid" name="sectid" value="<%=(vista.equals("0")) ? "" : bean_sol.getSector()%>">
                                    </td>
                                    <td  >Subsector</td>
                                    <td  colspan="3">
                                        <input type="text" id="subsecttext" name="subsecttext" readonly value="<%=(vista.equals("0")) ? "" :(login_into.equals("Fenalco")? gcserv.nombreAlternoSubSector(bean_sol.getSector(), bean_sol.getSubsector()):gcserv.nombreSubSector(bean_sol.getSector(), bean_sol.getSubsector()))%>"  style="width:90%;">
                                        <input type="hidden" id="subsectid" name="subsectid" value="<%=(vista.equals("0")) ? "" : bean_sol.getSubsector()%>">
                                    </td>

                                    <td>T&iacute;tulo valor</td>
                                    <td >
                                        <select name="cmbTituloValor" id="cmbTituloValor" onchange="mantenerDatos('<%=estado%>');validaraldia();xpand_div1();" style="width:8em;" <%= vista.equals("8")?"disabled":""%> >
                                            <option value="">...</option>
                                            <%
                                                 if (titval != null && titval.size() > 0) {
                                                     String row[] = null;
                                                     for (int i = 0; i < titval.size(); i++) {
                                                         row = (titval.get(i)).split(";_;");
                                            %>
                                            <option value="<%= row[0]%>" <%=(row[0].equals((vista.equals("0")) ? " " : bean_sol.getTipoNegocio())) ? "selected" : ""%> ><%= row[1]%></option>
                                            <%
                                                     }
                                                 }
                                            %>
                                        </select>
                                        <input type="checkbox" id="aldia" name="aldia"  onclick="validaraldia();"  <%= (bean_sol != null) ? (!redesc&&bean_sol.getTipoNegocio().equals("01")&&bean_sol.getPlazo().equals("1")&&bean_sol.getPlazoPrCuota().equals("1") ? "checked" : "") : ""%> <%= (bean_sol != null) ? (bean_sol.getTipoNegocio().equals("01")&&!redesc ? "" : "disabled") : "disabled"%>>
                                        &nbsp;&nbsp;Al dia
                                    </td>
                                </tr>
                                <tr class="filaazul">
                                    <td>
                                        Producto
                                    </td>
                                    <td colspan="3">
                                        <select id="producto" name="producto" style="width:20em;" onchange="cargarServicios(this.selectedIndex);" <%= vista.equals("8")||vista.equals("20")?"disabled":""%>>
                                            <option value="" selected>...</option>
                                            <%
                                                 for (int i = 0; i < productos.size(); i++) {%>
                                            <option value="<%=productos.get(i).getCodigo()%>" <%=(productos.get(i).getCodigo().equals((vista.equals("0")) ? " " : bean_sol.getProducto())) ? "selected" : ""%> ><%=productos.get(i).getValor()%></option>
                                            <%  }%>
                                        </select>
                                    </td>
                                    <td>
                                        Servicio
                                    </td>
                                    <td width="11%">
                                        <div id="d_servicio">
                                            <select id="servicio"  name="servicio" style="width:12em;" <%=vista.equals("8")?"disabled":""%> disabled="disabled" >
                                                <option value="" selected>...</option>
                                                <%
                                                 for (int i = 0; i < servicios.size(); i++) {%>
                                                <option value="<%=servicios.get(i).getCodigo()%>" <%=(servicios.get(i).getCodigo().equals((vista.equals("0")) ? " " : bean_sol.getServicio())) ? "selected" : ""%> ><%=servicios.get(i).getValor()%></option>
                                                <%  }%>
                                            </select>
                                        </div>
                                    </td>
                                    <td width="6%">
                                        Matricula
                                    </td>
                                    <td width="12%">
                                        <div id="d_matricula">
                                            <select id="matricula" name="matricula" style="width:12em;" <%= vista.equals("8")?"disabled":""%> >
                                                <option value="" selected>...</option>
                                                <%
                                                for (int i = 0; i < matricula.size(); i++) {%>
                                                <option value="<%=matricula.get(i).getEquivalencia()%>" <%=(matricula.get(i).getEquivalencia().equals((vista.equals("0")) ? " " : bean_sol.getCiudadMatricula())) ? "selected" : ""%> ><%=matricula.get(i).getValor()%></option>
                                                <%  }%>
                                            </select>
                                        </div>
                                    </td>
                                    <td>
                                    <%= (vista.equals("20"))?"Valor Pre-aprobado $":"Valor producto $"%>
                                    </td>
                                    <td>

                                        
                                          <%if(vista.equals("20")){%>

                                <input type="text" id="valor_producto" onkeyup="soloNumeros(this.id);" name="valor_producto"    size="15" readonly value="<%=(!vista.equals("20")) ? "" : Util.customFormat(Double.parseDouble(bean_sol.getValorSolicitado())*1.10)%>"  <%= vista.equals("8")?"disabled":""%> style="border-bottom: 1px solid; border-left: 0px; border-right: 0px; border-top: 0px; text-align:right;">

                                <%}else
                                    {%>

                                        <input type="text" id="valor_producto" onkeyup="soloNumeros(this.id);" name="valor_producto" onblur="formato(this,0);cargarValorSolicitado()" onkeyup="soloNumeros(this.id);" size="15" value="<%=(vista.equals("0")) ? "" : Util.customFormat(Double.parseDouble(bean_sol.getValorProducto()))%>" style="border-bottom: 1px solid; border-left: 0px; border-right: 0px; border-top: 0px; text-align:right;" <%= vista.equals("8")?"disabled":""%>>
                                    <%}%>
                                      
                                    </td>
                                </tr>

                                <tr class="filaazul">
                                    <td>
                                        Tipo
                                    </td>
                                    <td>

                                        <select id="tipo_p" name="tipo_p" style="width:12em;" onchange="xpand_div1();" <%= vista.equals("8")?"disabled":""%> >
                                           <option value="">...</option>
                                           <option value="natural" <%= (vista.equals("0")) ? "" : bean_pers.getTipoPersona().equals("N") ? "selected" : ""%>>Persona natural</option>
                                           <option value="juridica" <%= (vista.equals("0")) ? "" : bean_pers.getTipoPersona().equals("J") ? "selected" : ""%>>Persona juridica</option>
                                        </select>&nbsp;
                                    </td>
                                    <td width="6%">
                                        Codeudor Natural
                                        <input type="checkbox" id="codeu" name="codeu"  onclick="limpiarcod('<%=estado%>');" <%= cod.equals("S") ? "checked" : ""%>  <%= vista.equals("8")?"disabled":""%>>
                                    </td>
                                    <td width="6%">
                                        Codeudor Juridico
                                        <input type="checkbox" id="codeu2" name="codeu2"  onclick="limpiarcodj('<%=estado%>');" <%= cod2.equals("S") ? "checked" : ""%>  <%= vista.equals("8")?"disabled":""%>>
                                    </td>
                                    <td>
                                        Valor solicitado $
                                    </td>
                                    <td>

                                        <input type="text" id="valor_solicitado" onkeyup="soloNumeros(this.id);" name="valor_solicitado"  onblur="formato(this, 0); <%=vista.equals("20")?"validar_monto();":"cargarValorProd();"%>" onkeyup="soloNumeros(this.id);" size="15" value="<%=(vista.equals("0")) ? "" : Util.customFormat(Double.parseDouble(bean_sol.getValorSolicitado()))%>" style="border-bottom: 1px solid; border-left: 0px; border-right: 0px; border-top: 0px; text-align:right;">
                                    </td>
                                    <td>
                                        Plazo
                                    </td>
                                    <td>
                                        <input type="text" id="plazo" name="plazo"  onblur="formato(this, 0);validaraldia();" onkeyup="soloNumeros(this.id);" size="2" value="<%=(vista.equals("0")) ? "" : bean_sol.getPlazo()%>"  <%= (bean_sol != null) ? (bean_sol.getTipoNegocio().equals("01")&&bean_sol.getPlazo().equals("1")&&bean_sol.getPlazoPrCuota().equals("1") ? "disabled" : "") : ""%> style="border-bottom: 1px solid; border-left: 0px; border-right: 0px; border-top: 0px; text-align:right;" <%= vista.equals("8")?"disabled":""%>>
                                    </td>
                                    <td>
                                        Plazo 1era cuota
                                    </td>
                                    <td valign="middle">
                                        <select name="forma_pago" id="forma_pago" class="listmenu" onChange="validaraldia();"  <%= (bean_sol != null) ? (bean_sol.getTipoNegocio().equals("01")&&bean_sol.getPlazo().equals("1")&&bean_sol.getPlazoPrCuota().equals("1") ? "disabled" : "") : ""%> <%= vista.equals("8")?"disabled":""%>>
                                            <option value="" selected>...</option>
                                            <%
                                                 if (listaplazos != null && listaplazos.size() > 0) {
                                                     String row[] = null;
                                                     for (int i = 0; i < listaplazos.size(); i++) {
                                                         row = (listaplazos.get(i)).split(";_;");
                                            %>
                                            <option value="<%= row[0]%>"  <%=(row[0].equals((vista.equals("0")) ? " " : bean_sol.getPlazoPrCuota())) ? "selected" : ""%> ><%= row[1]%></option>
                                            <%
                                                     }
                                                 }
                                            %>
                                        </select>
                                    </td>
                                </tr>
                                <%} if (tipoconv.equals("Microcredito")||tipoconv.equals("Multiservicio")){%>
                                <tr class="filaazul">
                                    <td>
                                        Convenio
                                    </td>
                                    <td colspan="3">
                                        <div id="convenios">
                                            <select id="convenio" name="convenio" style="width:30em;" <%=(vista.equals("0") || estado.equals("B")) ? "" : "disabled"%> onchange="cargarMontos(this.selectedIndex);" >
                                                <option value="">...</option>
                                                <%
                                                            for (int j = 0; j < convenios.size(); j++) {
                                                                dato1 = ((String) convenios.get(j)).split(";_;");%>
                                                <option value="<%=dato1[0]%>" <%=(dato1[0].equals(bean_sol!=null?bean_sol.getIdConvenio():"")) ? "selected" : ""%> ><%=dato1[1]%></option>
                                                <%  }
                                                %>
                                            </select>
                                            <input type="hidden" id="salariomin" name="salariomin"value="<%=salariomin%>">
                                            <input type="hidden" id="montomin" name="montomin"value="<%=montomin%>">
                                            <input type="hidden" id="montomax" name="montomax"value="<%=montomax%>">
                                            <input id="producto" name="producto" type="hidden" value="<%=tipoconv.equals("Multiservicio")?"03":"04"%>" />
                                            <input id="conv" name="conv" type="hidden" value="<%=(vista.equals("0")) ? "" : bean_sol.getIdConvenio()%>" />
                                            <input id="fecha_priemra_cuota" name="fecha_priemra_cuota" type="hidden" value="<%=currentDate %>" />
                                            <input id="motonRenovacion" name="motonRenovacion" type="hidden" value="0" />
                                            <input id="codNegocioRenovado" name="codNegocioRenovado" type="hidden" value="" />
                                        </div>
                                    </td>
                                    <td>
                                        Asesor
                                    </td>
                                    <td colspan="5">
                                        <div id="asesor">
                                            <select id="asesor" name="asesor" style="width:30em;" <%= vista.equals("8")?"disabled":""%> >
                                                <option value="">...</option>
                                                <%
                                                           asesores = aserv.buscarAsesoresActivos(bean_sol!=null?bean_sol.getIdConvenio():"-"); 
                                                            for (int j = 0; j < asesores.size(); j++) {
                                                                Asesores asesor=asesores.get(j);%>
                                                <option value="<%=asesor.getIdusuario()%>" <%=(asesor.getIdusuario().equals(bean_sol!=null?bean_sol.getAsesor():"")) ? "selected" : ""%> ><%=asesor.getNombre()%></option>
                                                <%  }
                                                %>
                                            </select>
                                        </div>
                                    </td>
                                    <td></td>
                                </tr>
                                <tr class="filaazul">
                                    <td>
                                        Tipo
                                    </td>
                                    <td>

                                        <select id="tipo_p" name="tipo_p" style="width:12em;" onchange="xpand_div1();" <%= vista.equals("8")?"disabled":""%>>
                                            <option value="">...</option>
                                            <option value="natural" <%= (vista.equals("0")) ? "" : bean_pers.getTipoPersona().equals("N") ? "selected" : ""%>>Persona natural</option>
                                            <option value="juridica" <%= (vista.equals("0")) ? "" : bean_pers.getTipoPersona().equals("J") ? "selected" : ""%>>Persona juridica</option>
                                        </select>&nbsp;
                                    </td>
                                    <td><label> Codeudor Natural</label>
                                        <input type="checkbox" id="codeu" name="codeu"  onclick="limpiarcod('<%=estado%>');"<%= (cod.equals("S") ? "checked" : "")%>>
                                    </td>
                                    <td><label> Codeudor Juridico</label>
                                        <input type="checkbox" id="codeu2" name="codeu2"  onclick="limpiarcodj('<%=estado%>');"<%= (cod2.equals("S") ? "checked" : "")%>>
                                    </td>
                                    <!--Campo para renovaciones mic -->
                                    <% if(tipoconv.equals("Microcredito")){%>

                                    <td><label> Renovacion </label>
                                        <input type="checkbox" id="renovacion" name="renovacion" value="S"  onclick="abrirVentana();"   <%= (bean_sol != null) ? (bean_sol.getRenovacion().equals("S") ? "checked" : "") : ""%> disabled>
                                        
                                    </td>
                                    <td> <label> Pre-Aprobado </label>
                                        <input type="checkbox" id="preAprobado" name="preAprobado" value="S"  onclick="abrirVentanaPreaprobado();"   <%= (bean_sol != null) ? (bean_sol.getPreAprobadoMicro().equals("S") ? "checked" : "") : ""  %> disabled>
                                        
                                    </td>
                                    <% } %>
                                    <!--Campo para renovaciones mic -->
                                    <td>
                                        Valor solicitado $
                                </td>
                                    <td colspan="2">

                                        <input type="text" id="valor_solicitado" onkeyup="soloNumeros(this.id);" name="valor_solicitado"  onblur="formato(this, 0);validarMontorenovacion(this); cargarValorProd();" onkeyup="soloNumeros(this.id);" size="15" value="<%=(vista.equals("0")) ? "" : Util.customFormat(Double.parseDouble(bean_sol.getValorSolicitado()))%>"  <%= vista.equals("8")?"disabled":""%> style="border-bottom: 1px solid; border-left: 0px; border-right: 0px; border-top: 0px; text-align:right;" <%= vista.equals("8") || vista.equals("30") ?"readonly":""%>>
                                </td>
                                <td> 
                                      Valor producto $
                                </td>
                                    <td colspan="2">
                                        <input type="text" id="valor_producto" onkeyup="soloNumeros(this.id);" name="valor_producto" onblur="formato(this,0);validarMontorenovacion(this);cargarValorSolicitado();" onkeyup="soloNumeros(this.id);" size="15" value="<%=(vista.equals("0")) ? "" : Util.customFormat(Double.parseDouble(bean_sol.getValorProducto()))%>"  <%= vista.equals("8")?"disabled":""%> style="border-bottom: 1px solid; border-left: 0px; border-right: 0px; border-top: 0px; text-align:right;">
                                    </td>
                                </tr>
                                <tr class="filaazul">
                                    <td>
                                    </td>
                                    <td>
                                    </td>
                                    <td> <h4>Destino Credito:</h4></td>
                                     <td>
                                         <label> Capital De Trabajo  </label>
                                        <input type="checkbox" id="id_capital_trabajo" name="id_capital_trabajo" value="S" <%= bean_sol != null && bean_sol.isCapitalDeTrabajo() ? "checked" : ""%>>
                                     </td>
                                     <td>
                                        <label> Activo Fijo </label>
                                        <input type="checkbox" id="id_activo_fijo" name="id_activo_fijo" value="S" <%= bean_sol != null && bean_sol.isActivoFijo() ? "checked" : ""%>>
                                     </td>
<!--                                     <td style="display:none;">
                                          <label> Compra Cartera  </label>
                                        <input type="checkbox" id="id_compra_cartera" name="id_compra_cartera" value="S" <%= bean_sol != null && bean_sol.isCompraCartera() ? "checked" : ""%>>
                                     </td>-->
                                      </td>
                                    <td colspan="2">
                                         <label> Cuota M�xima:</label>
                                        <input type="text" id="id_cuota_maxima" onkeyup="soloNumeros(this.id);" name="id_cuota_maxima"  onblur="formato(this, 0);" onkeyup="soloNumeros(this.id);" size="15"  style="border-bottom: 1px solid; border-left: 0px; border-right: 0px; border-top: 0px; text-align:right;" value="<%=(vista.equals("0")) ? "" : Util.customFormat(Double.parseDouble(bean_sol.getCuotaMaxima()))%>"  <%= vista.equals("8")?"disabled":""%>>
                                    </td>
                                    
                                     <td colspan="2" style="display:none;">
                                         <label> Valor Renovacion: </label>
                                        <input type="text" id="id_valor_renovacion" name="id_valor_renovacion"  size="15" style="border-bottom: 1px solid; border-left: 0px; border-right: 0px; border-top: 0px; text-align:right;" value="<%= bean_sol !=null  ? bean_sol.getValor_renovacion():0 %>">
                                    </td>
                                    <td colspan="2" style="display:none;">
                                         <label> Politica: </label>
                                        <input type="text" id="id_politica"  name="id_politica"  size="15"  style="border-bottom: 1px solid; border-left: 0px; border-right: 0px; border-top: 0px; text-align:right;" value="<%= bean_sol != null  ? bean_sol.getPolitica(): ""%>">
                                    </td>
                                    <td colspan="2" style="">
                                         <label> Nit Fondo: </label>
                                         <input type="text" disabled id="fg_nit_fondo"  name="fg_nit_fondo"  size="15"  style="border-bottom: 1px solid; border-left: 0px; border-right: 0px; border-top: 0px; text-align:right;" value="<%= bean_sol != null  ? bean_sol.getNit_fondo(): ""%>">
                                    </td>
                                    <td colspan="2" style="">
                                         <label> Producto Fondo </label>
                                        <input type="text" id="fg_producto" disabled name="fg_producto"   size="15"  style="border-bottom: 1px solid; border-left: 0px; border-right: 0px; border-top: 0px; text-align:right;" value="<%= bean_sol != null  ? bean_sol.getProducto_fondo(): "0"%>">
                                    </td>
                                    <td colspan="2" style="">
                                         <label> Cobertura Fondo: </label>
                                        <input type="text" id="fg_cobertura" disabled name="fg_cobertura"  size="15"  style="border-bottom: 1px solid; border-left: 0px; border-right: 0px; border-top: 0px; text-align:right;" value="<%= bean_sol != null  ? bean_sol.getCobertura_fondo(): "0"%>">
                                    </td>
                                </tr>
                                <%}%>
                            </tbody>
                        </table>
                    </div>
                    <br>
                    <div id="contenido" class="divs">
                    </div>
                </form>
                <script type="text/javascript">
                    function formCallback(result, form) {
                        window.status = "valiation callback for form '" + form.id + "': result = " + result;
                    }

                </script>
                <br>
                <% if (!msj.equals("")) {%>
                <table border="2" align="center">
                    <tr>
                        <td><table width="100%"  border="1" align="center"  bordercolor="#F7F5F4" bgcolor="#FFFFFF">
                                <tr>
                                    <td width="229" align="center" class="mensajes">
                                        <%=msj%>

                                    </td>
                                    <td width="29" background="<%=BASEURL%>/images/cuadronaranja.JPG">&nbsp;</td>
                                    <td width="58">&nbsp;</td>
                                </tr>
                            </table></td>
                    </tr>
                </table>
                <%}%>
                <div align="center">
                     <%  if (vista.equals("8")&&(((estado.equals("P")||estado.equals("V"))&& actividad.equals("RAD")&&tipo_proceso.equals("PCR"))||(actividad.equals("SOL")&&tipo_proceso.equals("PSR")&&estado.equals("P"))||(actividad.equals("RAD")&&tipo_proceso.equals("PSR")&&estado.equals("P"))||((actividad.equals("SOL")||actividad.equals("RAD"))&&tipo_proceso.equals("DSR")&&estado.equals("P")))) {%>
                    <img alt="" src = "<%=BASEURL%>/images/botones/referenciar.gif" style="cursor:pointer;" onclick="referenciar('<%=BASEURL%>','<%=negocio%>', '<%=num_solicitud%>');" name= "imgreferenciar" id="imgreferenciar"  onMouseOver="botonOver(this);" onMouseOut="botonOut(this);">
                    <%} %>
                    <%  if (estado.equals("B") || ((estado.equals("P") || estado.equals("Q")|| estado.equals("V")) && (vista.equals("4")||vista.equals("8")||vista.equals("1")) && (actividad.equals("LIQ")||actividad.equals("SOL")||actividad.equals("RAD")||actividad.equals("STBY")))) {%>
                    <img alt="" src = "<%=BASEURL%>/images/botones/aceptar.gif" style="cursor:pointer;" onclick="validar_solicitud_aval();" name= "imgaceptar" id="imgaceptar"  onMouseOver="botonOver(this);" onMouseOut="botonOut(this);">
                    <%  }
                                 if (!estado.equals("B")) {%>
                    <img alt="" src = "<%=BASEURL%>/images/botones/generarPdf.gif" style="cursor:pointer;" onclick="generarPdf();" name= "imgexportar" id="imgexportar"  onMouseOver="botonOver(this);" onMouseOut="botonOut(this);">
                    <%} else {%>
                    <img alt="" src = "<%=BASEURL%>/images/botones/guardar.gif" style="cursor:pointer; visibility: hidden;" onclick="guardar();" name= "imgguardar" id="imgguardar"  onMouseOver="botonOver(this);" onMouseOut="botonOut(this);">
                    <%}%>

                     <%  if (vista.equals("20") || vista.equals("30")) {%>
                    <img alt="" src = "<%=BASEURL%>/images/botones/aceptar.gif" style="cursor:pointer;" onclick="validar_solicitud_aval();" name= "imgaceptar" id="imgaceptar"  onMouseOver="botonOver(this);" onMouseOut="botonOut(this);">
                    <img alt="" src = "<%=BASEURL%>/images/botones/guardar.gif" style="cursor:pointer; visibility: hidden;" onclick="guardar();" name= "imgguardar" id="imgguardar"  onMouseOver="botonOver(this);" onMouseOut="botonOut(this);">
                    <% }%>
                    <img alt="" src = "<%=BASEURL%>/images/botones/salir.gif" style = "cursor:pointer" name = "imgsalir" onclick = "window.close();" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);">



                </div>
        </div>
        
        <div id="popup" class="popup" style="display: none;">
            <div class="content-popup">
                <div class="close" ><a href="#" id="close" onclick="cerrarVentana()" ><img   src="<%=BASEURL%>/images/close.png"/></a></div>
                <div>

                    <h4>Validar Renovaci&oacute;n de cr&eacute;dito</h4>
                    <label>Identificaci&oacute;n </label> <input type="text" id="inpRenovacion"  onkeypress="return isNumberKey(event)"  maxlength="10" style="width: 100px">
                    <img  width="18" height="18" src ="<%=BASEURL%>/images/botones/iconos/lupa.gif" style="cursor:pointer;" onclick="validarRenovacion()" name= "imgLupa" id="imgLupa">
                </div>
            </div>
        </div> 
        
        <div id="popup2" class="popup" style="display: none;">
            <div class="content-popup">
                <div class="close" ><a href="#" id="close" onclick="cerrarVentanaPreaprobado()" ><img   src="<%=BASEURL%>/images/close.png"/></a></div>
                <div>

                    <h4>Validaci&oacute;n de cr&eacute;dito pre aprobado</h4>
                    <label>Identificaci&oacute;n </label> <input type="text" id="inpPreaprobado"  onkeypress="return isNumberKey(event)"  maxlength="10" style="width: 100px">
                    <img  width="18" height="18" src ="<%=BASEURL%>/images/botones/iconos/lupa.gif" style="cursor:pointer;" onclick="validarPreaprobado()" name= "imgLupa" id="imgLupa">
                </div>
            </div>
        </div>
                
        <div id="popup3" class="popup" style="display: none;">
            <div class="content-popup">
                <div class="close" ><a href="#" id="close" onclick="cerrarVentanaNegocio()" ><img   src="<%=BASEURL%>/images/close.png"/></a></div>
                <div>
                    <h4>Validaci&oacute;n de Direcci&oacute;n</h4>
                    <p>Esta direccion ya se encuentra asociada a otro cliente</p>
                    <p style="font-size: 12px;text-align:justify;" id="msj" > Texto </p>
                </div>
            </div>
        </div>
              
        <!-- <div id="direccion_dialogo" class="popup" style="display:none;" style="display:none;z-index:510;position:absolute;border:1px solid #165FB6; background-color:#FFFFFF; padding: 2px 2px 2px 2px;"> -->        
<!--        <div id="direccion_dialogo" style="display:none;left: 0; position: relative; top: 0; width: 400px; z-index: 1002; padding: 2px 2px 2px 2px;">-->
            <div id="direccion_dialogo" style="display:none;left: 0; position: absolute; top: 0;padding: 1em 1.2em; margin:0px auto; margin-top:10px; padding:10px; width:370px; min-height:140px; border-radius:4px; background-color:#FFFFFF; box-shadow: 0 2px 5px #666666;"> <!-- class="content-popup" style="padding: 1em 1.2em; width: 370px;"-->
                <div>
                <table style="width: 100%;">
                    
                    <tr>
                        <td class="titulo_ventana" id="drag_direcciones" colspan="3">
                            <div style="float:center">FORMATO DIRECCIONES<input type="hidden" name="id_usuario" id="id_usuario" value="coco"/></div> 
                        </td>
                    </tr>                    
                    <tr>  
                        <td><span>Departamento:</span></td>                     
                        <td colspan="2"> <select id="dep_dir" name="dep_dir" style="width:20em;">
                                <option value="">...</option>
                                <%  dato1 = null;
                                       String default_dep = (login_into.equals("Fenalco_bol")) ? "BOL" : "ATL";
                                            for (int i = 0; i < optdep.size(); i++) {
                                                dato1 = ((String) optdep.get(i)).split(";_;");%>
                                <option value="<%=dato1[0]%>" <%=(dato1[0].equals(default_dep)) ? "selected" : ""%>><%=dato1[1]%></option>
                                <%  }%>
                            </select>
                        </td>
                    </tr>
                    <tr>
                        <td><span>Ciudad:</span></td>     
                        <td colspan="2">                            
                            <div id="d_ciu_dir">
                                <select id="ciu_dir" name="ciu_dir" style="width:20em;">
                                    <option value="">...</option>
                                    <%  String default_city = (default_dep.equals("BOL")) ? "CG" : (default_dep.equals("ATL")) ? "BQ" : "";
                                        optciu = gsaserv.listadoCiudades(default_dep);
                                                    for (int i = 0; i < optciu.size(); i++) {
                                                    dato1 = ((String) optciu.get(i)).split(";_;");%>
                                    <option value="<%=dato1[0]%>" <%=(dato1[0].equals(default_city)) ? "selected" : ""%>><%=dato1[1]%></option>
                                    <%  
                                          }%>
                                </select>
                            </div>
                        </td>
                    </tr>     
                  
                    <tr>
                        <td>Via Principal</td>
                        <td>
                            <select id="via_princip_dir" onchange="setDireccion(2)">                                
                            </select>
                        </td>
                        <td><input type="text" id="nom_princip_dir" style="width: 87%;" onchange="setDireccion(1)"/></td>
                    </tr>
                    <tr>
                       
                        <td>Numero</td>
                        <td>
                            <select id="via_genera_dir" onchange="setDireccion(1)">                               
                            </select>
                        </td>
                        
                        <td>
                            <table width="100%" border="0">
                                <tr>
                                    <td align="center" width="49%">
                                        <input type="text" id="nom_genera_dir" style="width: 50px;" onchange="setDireccion(1)"/>
                                    </td>
                                    <td width="2%" align="center"> - </td>
                                    <td align="center" width="49%">
                                        <input type="text" id="placa_dir" style="width: 50px;" onchange="setDireccion(1)"/>
                                    </td>    
                                </tr>
                            </table>                        
                        </td>
                    </tr>
                   
                    <tr>
                        <td>Complemento</td>
                        <td colspan="2"><input type="text" id="cmpl_dir" style="width: 92%;" onchange="setDireccion(1)"/></td>
                    </tr>
                    <tr>
                        <td colspan="3"><input type="text" id="dir_resul" name="" style="width: 95%;" readonly/></td>
                    </tr>
                    <tr>
                        <td colspan="3">
                            <button onclick="setDireccion(3);">Aceptar</button>
                            <button onclick="setDireccion(0);">Salir</button>
                        </td>
                    </tr>
                </table>
                </div>
            </div>
<!--        </div>-->
    </body>
</html>

<iframe width="188" height="166" name="gToday:normal:agenda.js:gfPop:plugins.js" id="gToday:normal:agenda.js:gfPop:plugins.js" src="<%=BASEURL%>/js/Calendario/ipopeng.htm" scrolling="yes" frameborder="0" style="visibility:visible; z-index:999; position:absolute; left:-500px; top:0px;">
</iframe>
<div id="natural" style="display:none; visibility: hidden;">
    <b>DATOS PERSONALES DEL DEUDOR</b><br>
    <b>Informaci�n b�sica</b>
    <table id="tnatural" style="border-collapse:collapse; width:100%" border="1">
        <tr class="filaazul2">
            <td>
                Identificacion<br>
                <select id="tipo_id_nat" name="tipo_id_nat" <%= vista.equals("8")?"disabled":""%>>
                    <option value="" selected>...</option>
                    <%  dato1 = null;
                                 for (int i = 0; i < tipo_id.size(); i++) {
                                     dato1 = ((String) tipo_id.get(i)).split(";_;");%>
                    <option value="<%=dato1[0]%>" <%=(dato1[0].equals((vista.equals("0")) ? " " : bean_pers.getTipoId())) ? "selected" : ""%> ><%=dato1[1]%></option>
                    <%  }%>
                </select>
                <input type="text" maxlength="15" id="id_nat" onkeypress="return isNumberKey(event)"   name="id_nat" value="<%=(vista.equals("0")) ? "" : bean_pers.getIdentificacion()%>" size="20" <%= vista.equals("8")?"disabled":""%>>
                <img alt="buscar" src="<%=BASEURL%>/images/botones/iconos/lupa.gif" width="15" height="15" style="cursor:pointer;" onclick="busqueda('id_nat');">
            </td>
            <td  width="17%">
                Primer apellido<br>
                <input type="text" id="pr_apellido_nat" name="pr_apellido_nat" value="<%= (vista.equals("0")) ? "" : bean_pers.getPrimerApellido()%>" size="25" <%= vista.equals("8")?"disabled":""%> onChange="conMayusculas(this)"  onkeypress="return soloLetras(event)" >
            </td>
            <td class="filableach"  width="16%">
                Segundo Apellido<br>
                <input type="text" id="seg_apellido_nat" name="seg_apellido_nat" value="<%= (vista.equals("0")) ? "" : bean_pers.getSegundoApellido()%>" size="25" onChange="conMayusculas(this)" onkeypress="return soloLetras(event)">
            </td>
            <td  width="17%">
                Primer Nombre<br>
                <input type="text" id="pr_nombre_nat" name="pr_nombre_nat" value="<%= (vista.equals("0")) ? "" : bean_pers.getPrimerNombre()%>" size="25" onChange="conMayusculas(this)" onkeypress="return soloLetras(event)">
            </td>
            <td class="filableach"  width="17%">
                Segundo Nombre<br>
                <input type="text" id="seg_nombre_nat" name="seg_nombre_nat" value="<%= (vista.equals("0")) ? "" : bean_pers.getSegundoNombre()%>" size="25" onChange="conMayusculas(this)" onkeypress="return soloLetras(event)">
            </td>
            <td  width="16%">
                Genero<br>
                <select id="genero_nat" name="genero_nat">
                    <option value="">...</option>
                    <option value="M" <%= (vista.equals("0")) ? "" : bean_pers.getGenero().equals("M") ? "selected" : ""%>>Masculino</option>
                    <option value="F" <%= (vista.equals("0")) ? "" : bean_pers.getGenero().equals("F") ? "selected" : ""%>>Femenino</option>
                </select>
            </td>
            
        </tr>
        <tr class="filaazul2">
           <td  width="17%">
                Estado civil<br>
                <select id="est_civil_nat" name="est_civil_nat" <%= vista.equals("8")?"disabled":""%>>
                    <option value="">...</option>
                    <%  dato1 = null;
                                for (int i = 0; i < est_civil.size(); i++) {
                                    dato1 = ((String) est_civil.get(i)).split(";_;");%>
                    <option value="<%=dato1[0]%>" <%=(dato1[0].equals((vista.equals("0")) ? " " : bean_pers.getEstadoCivil())) ? "selected" : ""%> ><%=dato1[1]%></option>
                    <%  }%>
                </select>
            </td>
            <td>
                Fecha de expedicion (yyyy-MM-dd)<br>
                <input type="text" class="validate-date" id="f_exp_nat" name="f_exp_nat" value="<%= (vista.equals("0")) ? "" : Util.formatoFechaPostgres(bean_pers.getFechaExpedicionId())%>" size="15"<%= vista.equals("8")?"disabled":""%>>
            </td>
            <td>
                Departamento<br>
                <select id="dep_exp_nat" name="dep_exp_nat" style="width:20em;" onchange="cargarCiudades('dep_exp_nat', 'ciu_exp_nat');">
                    <option value="">...</option>
                    <%  dato1 = null;
                                dato1 = null;
                                for (int i = 0; i < optdep.size(); i++) {
                                    dato1 = ((String) optdep.get(i)).split(";_;");%>
                    <option value="<%=dato1[0]%>" <%=(dato1[0].equals((vista.equals("0")) ? " " : bean_pers.getDptoExpedicionId())) ? "selected" : ""%> ><%=dato1[1]%></option>

                    <%  }%>
                </select>
            </td>
            <td>
                Ciudad<br>
                <div id="d_ciu_exp_nat">
                    <select id="ciu_exp_nat" name="ciu_exp_nat" style="width:20em;">
                        <option value="">...</option>
                        <%  if (!vista.equals("0") && bean_pers != null) {
                                        optciu = gsaserv.listadoCiudades(bean_pers.getDptoExpedicionId());
                                        for (int i = 0; i < optciu.size(); i++) {
                                        dato1 = ((String) optciu.get(i)).split(";_;");%>
                        <option value="<%=dato1[0]%>" <%=(dato1[0].equals(bean_pers.getCiudadExpedicionId())) ? "selected" : ""%> ><%=dato1[1]%></option>
                        <%  }
                                          }%>
                    </select>
                </div>
            </td>
            <td >
                Nivel de estudio<br>
                <select id="niv_est_nat" name="niv_est_nat" style="width:20em;">
                    <option value="">...</option>
                    <%  dato1 = null;
                                for (int i = 0; i < nivel_estudio.size(); i++) {
                                    dato1 = ((String) nivel_estudio.get(i)).split(";_;");%>
                    <option value="<%=dato1[0]%>" <%=(dato1[0].equals((vista.equals("0")) ? " " : bean_pers.getNivelEstudio())) ? "selected" : ""%> ><%=dato1[1]%></option>
                    <%  }%>
                </select>
            </td>
            <td class="filableach" >
                Profesion<br>
                <input type="text" id="prof_nat" name="prof_nat" value="<%= (vista.equals("0")) ? "" : bean_pers.getProfesion()%>" size="30"  >
            </td>

        </tr>
        <tr class="filaazul2">
            <td>
                Fecha de nacimiento (yyyy-MM-dd)<br>
                <input type="text" id="f_nac_nat" class="validate-date" name="f_nac_nat" onblur="validarEdad(this.id, 'Deudor')" value="<%= (vista.equals("0")) ? "" : Util.formatoFechaPostgres(bean_pers.getFechaNacimiento())%>" size="15">
            </td>
            <td>
                Departamento<br>
                <select id="dep_nac_nat" name="dep_nac_nat" style="width:20em;" onchange="cargarCiudades('dep_nac_nat', 'ciu_nac_nat');">
                    <option value="">...</option>
                    <%  dato1 = null;
                                for (int i = 0; i < optdep.size(); i++) {
                                    dato1 = ((String) optdep.get(i)).split(";_;");%>
                    <option value="<%=dato1[0]%>" <%=(dato1[0].equals((vista.equals("0")) ? " " : bean_pers.getDptoNacimiento())) ? "selected" : ""%> ><%=dato1[1]%></option>
                    <%  }%>
                </select>
            </td>
            <td>
                Ciudad<br>
                <div id="d_ciu_nac_nat">
                    <select id="ciu_nac_nat" name="ciu_nac_nat" style="width:20em;">
                        <option value="">...</option>
                        <%  if (!vista.equals("0") && bean_pers != null) {
                                        optciu = gsaserv.listadoCiudades(bean_pers.getDptoNacimiento());
                                        for (int i = 0; i < optciu.size(); i++) {
                                        dato1 = ((String) optciu.get(i)).split(";_;");%>
                        <option value="<%=dato1[0]%>" <%=(dato1[0].equals(bean_pers.getCiudadNacimiento())) ? "selected" : ""%> ><%=dato1[1]%></option>
                        <%  }
                                          }%>
                    </select>
                </div>
            </td>
            <td>
                Personas a cargo<br>
                <input type="text" id="pcargo_nat" name="pcargo_nat" value="<%= (vista.equals("0")) ? "" : bean_pers.getPersonasACargo()%>" onkeyup="soloNumeros(this.id);" size="15">
            </td>
            <td>
                N&ordm; de hijos<br>
                <input type="text" id="nhijos_nat" name="nhijos_nat" value="<%= (vista.equals("0")) ? "" : bean_pers.getNumHijos()%>" onkeyup="soloNumeros(this.id);" onblur="fhijos('nat');" size="15">
            </td>
            <td>
                Total grupo familiar<br>
                <input type="text" id="ngrupo_nat" name="ngrupo_nat" value="<%=(vista.equals("0")) ? "" : bean_pers.getTotalGrupoFamiliar()%>" onkeyup="soloNumeros(this.id);" size="15">
            </td>
        </tr>
        <tr class="filaazul2">

            <td colspan="2">
                Direccion residencia<br>
                <table width="100%">
                    <tr>
                        <td width="30%">
                            <input type="text" id="dir_nat" name="dir_nat" <%=estadoDireccion%> value="<%=(vista.equals("0")) ? "" : bean_pers.getDireccion()%>" size="60">
                        </td>
                        <td>
                            
                            <img src="/fintra/images/Direcciones.png" width="26" height="23" border="0" align="middle" onclick="genDireccion('dir_nat',event);" alt="Direcciones"  title="Direcciones" />
                        </td>    
                    </tr>
                </table>    
            </td>
            
            <td>
                Departamento<br>
                <select id="dep_nat" name="dep_nat" style="width:20em;" onchange="cargarCiudadesBarrio('dep_nat', 'ciu_nat','barrio_nat');">
                    <option value="">...</option>
                    <%  dato1 = null;
                                for (int i = 0; i < optdep.size(); i++) {
                                    dato1 = ((String) optdep.get(i)).split(";_;");%>
                    <option value="<%=dato1[0]%>" <%=(dato1[0].equals((vista.equals("0")) ? " " : bean_pers.getDepartamento())) ? "selected" : ""%> ><%=dato1[1]%></option>
                    <%  }%>
                </select>
            </td>
            <td>
                Ciudad<br>
                <div id="d_ciu_nat">
                    <select id="ciu_nat" name="ciu_nat" style="width:20em;" onchange="cargarBarrios('ciu_nat', 'barrio_nat');">
                        <option value="">...</option>
                        <%  if (!vista.equals("0") && bean_pers != null) {
                                        optciu = gsaserv.listadoCiudades(bean_pers.getDepartamento());
                                        for (int i = 0; i < optciu.size(); i++) {
                                        dato1 = ((String) optciu.get(i)).split(";_;");%>
                        <option value="<%=dato1[0]%>" <%=(dato1[0].equals(bean_pers.getCiudad())) ? "selected" : ""%> ><%=dato1[1]%></option>
                        <%  }
                                          }%>
                    </select>
                </div>
            </td>
            <td >
                Barrio<br>
                
                        <div id="d_barrio_nat">
                            <select id="barrio_nat" name="barrio_nat" style="width:20em;">
                                <option value="">...</option>
                                <%  if (!vista.equals("0") && bean_pers != null) {
                                                optbarrio = gsaserv.listadoBarrios(bean_pers.getCiudad());
                                                for (int i = 0; i < optbarrio.size(); i++) {
                                                dato1 = ((String) optbarrio.get(i)).split(";_;");%>
                                <option value="<%=dato1[0]%>" <%=(dato1[0].equals(bean_pers.getBarrio())) ? "selected" : ""%> ><%=dato1[1]%></option>
                                <%  }
                                                  }%>
                            </select>
                        </div>
               
                    
               
            </td>
            <td>
                Estrato<br>
                <select name="estr_nat" id="estr_nat" style="width:10em;">
                    <option value="">...</option>
                    <%
                                for (int i = 1; i < 7; i++) {
                                    out.print("<option value='" + i + "'" + ((vista.equals("0")) ? "" : bean_pers.getEstrato().equals("" + i) ? " selected" : "") + ">" + i + "</option>");
                                }
                    %>
                </select>
            </td>
        </tr>
        <tr class="filaazul2">
            <td  colspan="2">
                E-mail<br>
                <input type="text" id="mail_nat" class="validate-email" name="mail_nat" value="<%=(vista.equals("0")) ? "" : bean_pers.getEmail()%>" size="60" onblur="validaEmail(this);conMinusculas(this);">
            </td>
            <td>
                Tipo de vivienda<br>
                <select id="tipo_viv_nat" name="tipo_viv_nat" style="width:10em;" <%= vista.equals("8")?"disabled":""%>>
                    <option value="">...</option>
                    <% for (int i = 0; i < tipo_vivienda.size(); i++) {%>
                    <option value="<%=tipo_vivienda.get(i).getCodigo()%>" <%=(tipo_vivienda.get(i).getCodigo().equals((vista.equals("0")) ? " " : bean_pers.getTipoVivienda())) ? "selected" : ""%> ><%=tipo_vivienda.get(i).getValor()%></option>
                    <%  }%>
                </select>
            </td>
            <td>
                Tiempo residencia<br>
                <% String[] tr = null;
                            if (!vista.equals("0") && bean_pers.getTiempoResidencia() != null) {
                                tr = bean_pers.getTiempoResidencia().split("A�os");
                                if (tr.length > 1) {
                                   tr[1] = tr[1].replaceAll("Meses", "").trim();
                                   tr[0]=tr[0].trim();
                                }
                            }%>
                <input type="text" id="tres_nat" name="tres_nat" value="<%= (vista.equals("0")) ? "" : (tr.length > 1 ? tr[0] : "")%>" size="5" onkeyup="soloNumeros(this.id);"> A�os
                <input type="text" id="tres_nat1" name="tres_nat1" value="<%= (vista.equals("0")) ? "" : (tr.length > 1 ? tr[1] : "")%>" size="5" onkeyup="soloNumeros(this.id);"> Meses
            </td>

            <td>
                Telefono<br>
                <input type="text" maxlength="7" id="tel_nat" name="tel_nat" value="<%=(vista.equals("0")) ? "" : bean_pers.getTelefono()%>" size="15" onkeyup="soloNumeros(this.id);">
            </td>
            <td class="filableach">
                Celular<br>
                <input type="text" maxlength="10" id="cel_nat" name="cel_nat" value="<%=(vista.equals("0")) ? "" : bean_pers.getCelular()%>" size="15" onkeyup="soloNumeros(this.id);">
            </td>

        </tr>
        <tr class="filaazul2">
            <td><h4> Autorizo el envio de extracto de pago a mi:</h4></td>
        <td colspan="6">
        <label>Correo Electronico </label>
            <input type="checkbox" id="id_correo_electronico" name="id_correo_electronico" value="S" <%=bean_pers == null ? "checked" : (bean_pers.isEnviarExtractoEmail() ? "checked" : "")%>>

        <label>Direcci&oacute;n de correspondencia </label>
        <input type="checkbox" id="id_correspondencia" name="id_correspondencia" value="S" <%=bean_pers == null ? "" : (bean_pers.isEnviarExtractoCorrespondecia() ? "checked" : "")%>>
        </td>        
       </tr>
    </table>
    <br>
    <b>Referencias personales</b>
    <table id="treferenciapersnat" style="border-collapse:collapse; width:100%" border="1">
        <%
                    for (int i = 1; i < 3; i++) {
                        SolicitudReferencias b_ref = null;
                        if (lista_referencias != null && lista_referencias.size() > i-1) {
                            b_ref = lista_referencias.get(i - 1);
                        }
        %>
        <tr class="filableach">
            <td>
                Primer Apellido<br>
                <input type="text" id="pr_apellido_refp_nat<%= i%>" name="pr_apellido_refp_nat<%= i%>" value="<%= (vista.equals("0") || b_ref == null) ? "" : b_ref.getPrimerApellido()%>" size="25" onkeypress="return soloLetras(event)">
            </td>
            <td>
                Segundo Apellido<br>
                <input type="text" id="seg_apellido_refp_nat<%= i%>" name="seg_apellido_refp_nat<%= i%>" value="<%=(vista.equals("0") || b_ref == null) ? "" : b_ref.getSegundoApellido()%>" size="25" onkeypress="return soloLetras(event)">
            </td>
            <td>
                Primer Nombre<br>
                <input type="text" id="pr_nombre_refp_nat<%= i%>" name="pr_nombre_refp_nat<%= i%>" value="<%=(vista.equals("0") || b_ref == null) ? "" : b_ref.getPrimerNombre()%>" size="25" onkeypress="return soloLetras(event)" >
            </td>
            <td>
                Segundo Nombre<br>
                <input type="text" id="seg_nombre_refp_nat<%= i%>" name="seg_nombre_refp_nat<%= i%>" value="<%= (vista.equals("0") || b_ref == null) ? "" : b_ref.getSegundoNombre()%>" size="25"  onkeypress="return soloLetras(event)">
            </td>
            <td>
                Tiempo de conocido<br>
                <input type="text" id="tconocido_refp_nat<%= i%>" maxlength="2" name="tconocido_refp_nat<%= i%>" value="<%=(vista.equals("0") || b_ref == null) ? "" : b_ref.getTiempoConocido()%>" size="15" onkeyup="soloNumeros(this.id);"> A�os
            </td>
            <td>
                Celular<br>
                <input type="text" maxlength="10" id="cel_refp_nat<%= i%>" name="cel_refp_nat<%= i%>" value="<%=(vista.equals("0") || b_ref == null) ? "" : b_ref.getCelular()%>" size="15" onkeyup="soloNumeros(this.id);">
            </td>
        </tr>
        <tr class="filableach">
            <td>
                Telefono 1<br>
                <input type="text" maxlength="10" id="tel1_refp_nat<%= i%>" name="tel1_refp_nat<%= i%>" value="<%= (vista.equals("0") || b_ref == null) ? "" : b_ref.getTelefono()%>" size="15" onkeyup="soloNumeros(this.id);">
            </td>
            <td>
                Telefono 2 &nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp  Extension <br>
                <input type="text" maxlength="7" id="tel2_refp_nat<%= i%>" name="tel2_refp_nat<%= i%>" value="<%= (vista.equals("0") || b_ref == null) ? "" : b_ref.getTelefono2()%>" size="15" onkeyup="soloNumeros(this.id);">
                <input type="text" maxlength="4" id="ext_refp_nat<%= i%>" name="ext_refp_nat<%= i%>" value="<%= (vista.equals("0") || b_ref == null) ? "" : b_ref.getExtension()%>" size="5" onkeyup="soloNumeros(this.id);">
            </td>
            <td>
                Departamento<br>
                <select id="dep_refp_nat<%= i%>" name="dep_refp_nat<%= i%>" style="width:20em;" onchange="cargarCiudades('dep_refp_nat<%= i%>', 'ciu_refp_nat<%= i%>');">
                    <option value="">...</option>
                    <%  dato1 = null;
                                            for (int j = 0; j < optdep.size(); j++) {
                                                dato1 = ((String) optdep.get(j)).split(";_;");%>
                    <option value="<%=dato1[0]%>" <%=(dato1[0].equals((vista.equals("0") || b_ref == null) ? " " : b_ref.getDepartamento())) ? "selected" : ""%> ><%=dato1[1]%></option>
                    <%  }%>
                </select>
            </td>
            <td>
                Ciudad<br>
                <div id="d_ciu_refp_nat<%= i%>">
                    <select id="ciu_refp_nat<%= i%>" name="ciu_refp_nat<%= i%>" style="width:20em;">
                        <option value="">...</option>
                        <%  if (!vista.equals("0") && b_ref != null) {
                                                    optciu = gsaserv.listadoCiudades(b_ref.getDepartamento());
                                                    for (int j = 0; j < optciu.size(); j++) {
                                                     dato1 = ((String) optciu.get(j)).split(";_;");%>
                        <option value="<%=dato1[0]%>" <%=(dato1[0].equals(b_ref.getCiudad())) ? "selected" : ""%> ><%=dato1[1]%></option>
                        <%  }
                                                      }%>
                    </select>
                </div>
            </td>
            <td style="display:none;">
                E-mail<br>
                <input type="text" id="mail_refp_nat<%= i%>"  name="mail_refp_nat<%= i%>" value="<%= (vista.equals("0") || b_ref == null) ? "" : b_ref.getEmail()%>">
             </td>
            <td colspan="6">
                Direccion<br>
                <table width="100%">
                    <tr>
                        <td width="30%" >
                            <input type="text" id="dir_ref_nat<%= i%>" name="dir_ref_nat<%= i%>" <%=estadoDireccion%> value="<%= (vista.equals("0") || b_ref == null) ? "" : b_ref.getDireccion()%>" size="40">
                        </td>
                        <td>                      
                            <img src="/fintra/images/Direcciones.png" width="26" height="23" border="0" align="middle" onclick="genDireccion('dir_ref_nat<%= i%>',event);" alt="Direcciones"  title="Direcciones" />
                        </td>    
                    </tr>
                </table> 
            </td>
        </tr>
        <%
                    }
        %>
    </table>
    <br>
    <b>Referencias familiares</b>
    <table id="treferenciafamnat" style="border-collapse:collapse; width:100%" border="1">
        <%
                    if (!vista.equals("0")) {
                        try {
                            lista_referencias = gsaserv.buscarReferencias(num_solicitud, "S", "F");
                        } catch (Exception e) {
                            System.out.println("error: " + e.toString());
                            e.printStackTrace();
                        }
                    }
                    for (int i = 1; i < 3; i++) {
                        SolicitudReferencias b_ref = null;
                        if (lista_referencias != null && lista_referencias.size() > i-1) {
                            b_ref = lista_referencias.get(i-1);
                        }
        %>
        <tr class="filableach">
            <td>
                Primer Apellido<br>
                <input type="text" id="pr_apellido_refam_nat<%= i%>" name="pr_apellido_refam_nat<%= i%>" value="<%= (vista.equals("0") || b_ref == null) ? "" : b_ref.getPrimerApellido()%>" size="25" onkeypress="return soloLetras(event)" >
            </td>
            <td>
                Segundo Apellido<br>
                <input type="text" id="seg_apellido_refam_nat<%= i%>" name="seg_apellido_refam_nat<%= i%>" value="<%=(vista.equals("0") || b_ref == null) ? "" : b_ref.getSegundoApellido()%>" size="25" onkeypress="return soloLetras(event)" >
            </td>
            <td>
                Primer Nombre<br>
                <input type="text" id="pr_nombre_refam_nat<%= i%>" name="pr_nombre_refam_nat<%= i%>" value="<%= (vista.equals("0") || b_ref == null) ? "" : b_ref.getPrimerNombre()%>" size="25" onkeypress="return soloLetras(event)" >
            </td>
            <td>
                Segundo Nombre<br>
                <input type="text" id="seg_nombre_refam_nat<%= i%>" name="seg_nombre_refam_nat<%= i%>" value="<%= (vista.equals("0") || b_ref == null) ? "" : b_ref.getSegundoNombre()%>" size="25"  >
            </td>
            <td>
                Parentesco<br>
                <select id="parent_refam_nat<%= i%>" name="parent_refam_nat<%= i%>">
                    <option value="">...</option>
                    <%  dato1 = null;
                                            for (int j = 0; j < parentesco.size(); j++) {
                                                dato1 = ((String) parentesco.get(j)).split(";_;");%>
                    <option value="<%=dato1[0]%>" <%=(dato1[0].equals((vista.equals("0") || b_ref == null) ? " " : b_ref.getParentesco())) ? "selected" : ""%> ><%=dato1[1]%></option>
                    <%  }%>
                </select>
            </td>
            <td>
                Celular<br>
                <input type="text" maxlength="10" id="cel_refam_nat<%= i%>" name="cel_refam_nat<%= i%>" value="<%=(vista.equals("0") || b_ref == null) ? "" : b_ref.getCelular()%>" size="15" onkeyup="soloNumeros(this.id);">
            </td>
        </tr>
        <tr class="filableach">
            <td>
                Telefono 1<br>
                <input type="text" maxlength="10" id="tel1_refam_nat<%= i%>" name="tel1_refam_nat<%= i%>" value="<%=(vista.equals("0") || b_ref == null) ? "" : b_ref.getTelefono()%>" size="15" onkeyup="soloNumeros(this.id);">
            </td>
            <td>
                Telefono 2 &nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp  Extension <br>
                <input type="text" maxlength="7" id="tel2_refam_nat<%= i%>" name="tel2_refam_nat<%= i%>" value="<%= (vista.equals("0") || b_ref == null) ? "" : b_ref.getTelefono2()%>" size="15" onkeyup="soloNumeros(this.id);">
                <input type="text" maxlength="4" id="ext_refam_nat<%= i%>" name="ext_refam_nat<%= i%>" value="<%= (vista.equals("0") || b_ref == null) ? "" : b_ref.getExtension()%>" size="5" onkeyup="soloNumeros(this.id);">
            </td>
            <td>
                Departamento<br>
                <select id="dep_refam_nat<%= i%>" name="dep_refam_nat<%= i%>" style="width:20em;" onchange="cargarCiudades('dep_refam_nat<%= i%>', 'ciu_refam_nat<%= i%>');">
                    <option value="">...</option>
                    <%  dato1 = null;
                                            for (int j = 0; j < optdep.size(); j++) {
                                                dato1 = ((String) optdep.get(j)).split(";_;");%>
                    <option value="<%=dato1[0]%>" <%=(dato1[0].equals((vista.equals("0") || b_ref == null) ? " " : b_ref.getDepartamento())) ? "selected" : ""%> ><%=dato1[1]%></option>
                    <%  }%>
                </select>
            </td>
            <td>
                Ciudad<br>
                <div id="d_ciu_refam_nat<%= i%>">
                    <select id="ciu_refam_nat<%= i%>" name="ciu_refam_nat<%= i%>" style="width:20em;">
                        <option value="">...</option>
                        <%  if (!vista.equals("0") && b_ref != null) {
                                                    optciu = gsaserv.listadoCiudades(b_ref.getDepartamento());
                                                    for (int j = 0; j < optciu.size(); j++) {
                                                    dato1 = ((String) optciu.get(j)).split(";_;");%>
                        <option value="<%=dato1[0]%>" <%=(dato1[0].equals(b_ref.getCiudad())) ? "selected" : ""%> ><%=dato1[1]%></option>
                        <%  }
                                                      }%>
                    </select>
                </div>
            </td>
            <td style="display:none;">
                E-mail<br>
                <input type="text" id="mail_refam_nat<%= i%>"  name="mail_refam_nat<%= i%>" value="<%=(vista.equals("0") || b_ref == null) ? "" : b_ref.getEmail()%> ">
           </td>           
            <td colspan="6">
                Direccion<br>
                <table width="100%">
                    <tr>
                        <td width="30%">
                            <input type="text" id="dir_refam_nat<%= i%>" name="dir_refam_nat<%= i%>" <%=estadoDireccion%> value="<%=(vista.equals("0") || b_ref == null) ? "" : b_ref.getDireccion()%> " size="40">
                        </td>
                        <td>                      
                            <img src="/fintra/images/Direcciones.png" width="26" height="23" border="0" align="middle" onclick="genDireccion('dir_refam_nat<%= i%>',event);" alt="Direcciones"  title="Direcciones" />
                        </td>    
                    </tr>
                </table> 
            </td>
        </tr>
        <%
                    }
        %>
    </table>
    <br>
<%
       SolicitudReferencias b_ref = null;
       try {
            lista_referencias = gsaserv.buscarReferencias(num_solicitud, "S", "C");
        } catch (Exception ec) {
            System.out.println("error: " + ec.toString());
            ec.printStackTrace();
        }
    %>
    <b>Referencias comerciales</b>
    <table id="trefjuridica" style="border-collapse:collapse; width:100%" border="1">
        <%
                    for (int i = 1; i < 2; i++) {

                        b_ref = null;
                        if (lista_referencias != null) {
                            try {
                                if (lista_referencias.size() >= i) {
                                    b_ref = lista_referencias.get(i - 1);
                                }
                            } catch (Exception exc) {
                                System.out.println("errror2: " + exc.toString());
                            }

        %>
        <tr class="filaazul2">
            <td width="17%">
                Nombre o razon social<br>
                <input type="text" id="raz_soc_ref<%= i%>" name="raz_soc_ref<%= i%>" value="<%=(vista.equals("0") || b_ref == null) ? "" : b_ref.getNombre()%>" size="30" >
            </td>
            <td class="filableach" width="16%">
                Direccion<br>
                <table width="100%">
                    <tr>
                        <td width="90%">
                            <input type="text" id="raz_soc_dir<%= i%>" name="raz_soc_dir<%= i%>" <%=estadoDireccion%> value="<%=(vista.equals("0") || b_ref == null) ? "" : b_ref.getDireccion()%>" size="30">
                        </td>
                        <td>                         
                            <img src="/fintra/images/Direcciones.png" width="26" height="23" border="0" align="middle" onclick="genDireccion('raz_soc_dir<%= i%>',event);" alt="Direcciones"  title="Direcciones" />
                        </td>    
                    </tr>
                </table> 
            </td>
            <td width="17%">
                Departamento<br>
                <select id="dep_raz_soc_<%= i%>" name="dep_raz_soc_<%= i%>" style="width:20em;" onchange="cargarCiudades('dep_raz_soc_<%= i%>', 'ciu_raz_soc_<%= i%>');">
                    <option value="">...</option>
                    <%  dato1 = null;
                                                for (int j = 0; j < optdep.size(); j++) {
                                                    dato1 = ((String) optdep.get(j)).split(";_;");%>
                    <option value="<%=dato1[0]%>" <%=(dato1[0].equals((vista.equals("0") || b_ref == null) ? " " : b_ref.getDepartamento())) ? "selected" : ""%> ><%=dato1[1]%></option>
                    <%  }%>
                </select>
            </td>
            <td width="17%">
                Ciudad<br>
                <div id="d_ciu_raz_soc_<%= i%>">
                    <select id="ciu_raz_soc_<%= i%>" name="ciu_raz_soc_<%= i%>" style="width:20em;">
                        <option value="">...</option>
                        <%  if (!vista.equals("0") && b_ref != null) {
                                                        optciu = gsaserv.listadoCiudades(b_ref.getDepartamento());
                                                        for (int j = 0; j < optciu.size(); j++) {
                                                            dato1 = ((String) optciu.get(j)).split(";_;");%>
                        <option value="<%=dato1[0]%>" <%=(dato1[0].equals(b_ref.getCiudad())) ? "selected" : ""%> ><%=dato1[1]%></option>
                        <%  }
                                                    }%>
                    </select>
                </div>
            </td>
            <td width="16%">
                Telefono 1<br>
                <input type="text" maxlength="10" id="raz_soc_tel1<%= i%>" name="raz_soc_tel1<%= i%>" value="<%=(vista.equals("0") || b_ref == null) ? "" : b_ref.getTelefono()%>" size="15" onkeyup="soloNumeros(this.id);">
            </td>
            <td class="filableach" width="17%">
                Telefono 2<br>
                <input type="text" maxlength="10" id="raz_soc_tel2<%= i%>" name="raz_soc_tel2<%= i%>" value="<%=(vista.equals("0") || b_ref == null) ? "" : b_ref.getTelefono2()%>" size="15" onkeyup="soloNumeros(this.id);">
            </td>
             <td>
                Tipo referencia<br>
                <div id="id_tipo_refc_div">
                    <select id="id_tipo_refc_de<%= i%>" name="id_tipo_refc_de<%= i%>" style="width:10em;">
                        <option value="">...</option>
                        <option value="P" <%= b_ref != null && b_ref.getReferenciaComercial().equals("P") ? "selected" : "" %>>Proveedor</option>
                        <option value="C" <%= b_ref != null && b_ref.getReferenciaComercial().equals("C") ? "selected" : "" %>>Cliente</option>
                    </select>
                </div>
            </td>
        </tr>
        <%
                        }
                    }
        %>
    </table>    
    <br>
    <b>Informaci�n del conyugue</b>
    <table id="tconyugenat" style="border-collapse:collapse; width:100%" border="1">
        <tr class="filableach" >
            <td width="17%">
                Primer apellido<br>
                <input type="text" id="pr_apellido_con_nat" name="pr_apellido_con_nat" value="<%= (vista.equals("0")) ? "" : bean_pers.getPrimerApellidoCony()%>" size="25" onkeypress="return soloLetras(event)">
            </td>
            <td class="filableach" width="16%">
                Segundo Apellido<br>
                <input type="text" id="seg_apellido_con_nat" name="seg_apellido_con_nat" value="<%= (vista.equals("0")) ? "" : bean_pers.getSegundoApellidoCony()%>" size="25" onkeypress="return soloLetras(event)">
            </td>
            <td width="17%">
                Primer Nombre<br>
                <input type="text" id="pr_nombre_con_nat" name="pr_nombre_con_nat" value="<%= (vista.equals("0")) ? "" : bean_pers.getPrimerNombreCony()%>" size="25" onkeypress="return soloLetras(event)" >
            </td>
            <td class="filableach" width="17%">
                Segundo Nombre<br>
                <input type="text" id="seg_nombre_con_nat" name="seg_nombre_con_nat" value="<%= (vista.equals("0")) ? "" : bean_pers.getSegundoNombreCony()%>" size="25" onkeypress="return soloLetras(event)" >
            </td>
            <td width="16%">
                Identificacion<br>
                <select id="tipo_id_con_nat" name="tipo_id_con_nat">
                    <option value="" selected>...</option>
                    <%  dato1 = null;
                                for (int i = 0; i < tipo_id.size(); i++) {
                                    dato1 = ((String) tipo_id.get(i)).split(";_;");%>
                    <option value="<%=dato1[0]%>" <%=(dato1[0].equals((vista.equals("0")) ? " " : bean_pers.getTipoIdentificacionCony())) ? "selected" : ""%> ><%=dato1[1]%></option>
                    <%  }%>
                </select>
                    <input type="text" maxlength="15" id="id_con_nat" name="id_con_nat" value="<%= (vista.equals("0")) ? "" : bean_pers.getIdentificacionCony()%>" size="20" onkeyup="soloNumeros(this.id);">
            </td>
            <td class="filableach" width="17%">
                Celular<br>
                <input type="text" maxlength="10" id="cel_con_nat" name="cel_con_nat" value="<%= (vista.equals("0")) ? "" : bean_pers.getCelularCony()%>" size="15" onkeyup="soloNumeros(this.id);">
            </td>
        </tr>
        <tr class="filableach">
            <td>
                Empresa donde labora<br>
                <input type="text" id="emp_con_nat" name="emp_con_nat" value="<%= (vista.equals("0")) ? "" : bean_pers.getEmpresaCony()%>" size="25" >
            </td>
            <td class="filableach">
                Direccion empresa<br>
                <table width="100%">
                    <tr>
                        <td width="90%">
                            <input type="text" id="emp_dir_con_nat" name="emp_dir_con_nat" <%=estadoDireccion%> value="<%= (vista.equals("0")) ? "" : bean_pers.getDireccionEmpresaCony()%>" size="25">
                        </td>
                        <td>                         
                            <img src="/fintra/images/Direcciones.png" width="26" height="23" border="0" align="middle" onclick="genDireccion('emp_dir_con_nat',event);" alt="Direcciones"  title="Direcciones" />
                        </td>    
                    </tr>
                </table>    
            </td>
            <td>
                Telefono<br>
                <input type="text" maxlength="7" id="emp_tel_con_nat" name="emp_tel_con_nat" value="<%= (vista.equals("0")) ? "" : bean_pers.getTelefonoCony()%>" size="25" onkeyup="soloNumeros(this.id);">
            </td>
            <td style="display:none;">
                Cargo<br>
                <input type="text" id="emp_car_con_nat" name="emp_car_con_nat" value="<%= (vista.equals("0")) ? "" : bean_pers.getCargoCony()%>" size="25" >
            </td>
            <td>
                Salario/Mesada/Ingreso mes<br>
                $ <input type="text" id="emp_sal_con_nat" name="emp_sal_con_nat" value="<%= (vista.equals("0")) ? "" : Util.customFormat(Double.parseDouble(bean_pers.getSalarioCony()))%>" size="25" onkeyup="soloNumeros(this.id);" onchange="formato(this, 0)" >
            </td>
            <td class="filableach" colspan="6" >
                E-mail<br>
                <input type="text" id="mail_con_nat"  name="mail_con_nat" value="<%= (vista.equals("0")) ? "" : bean_pers.getEmailCony()%>" size="30" >
            </td>
        </tr>
    </table>
    <br>
    <b>Informaci�n laboral y econ�mica</b>
    <table id="tlaboralnat" style="border-collapse:collapse; width:100%" border="1">
        <tr class="filaazul2">
            <td width="17%">
                Actividad economica<br>
                <select id="act_econ_nat" name="act_econ_nat" style="width:20em;" onchange="cargarOcupaciones('act_econ_nat', 'ocup_nat');">
                    <option value="">...</option>
                    <%  dato1 = null;
                                for (int i = 0; i < act_eco.size(); i++) {
                                    dato1 = ((String) act_eco.get(i)).split(";_;");%>
                    <option value="<%=dato1[0]%>" <%=(dato1[0].equals((vista.equals("0") || bean_lab == null) ? " " : bean_lab.getActividadEconomica()))? "selected" : ""%> ><%=dato1[1]%></option>
                    <%  }%>
                </select>
            </td>
            <td width="16%">
                Ocupacion<br>
                <div id="d_ocup_nat">
                    <select id="ocup_nat" name="ocup_nat" style="width:20em;">
                        <option value="">...</option>
                        <%  if (!vista.equals("0") && bean_lab != null) {
                                        ocupacion = gsaserv.listadoOcupaciones(bean_lab.getActividadEconomica());
                                        for (int i = 0; i < ocupacion.size(); i++) {
                                        dato1 = ((String) ocupacion.get(i)).split(";_;");%>
                        <option value="<%=dato1[0]%>" <%=(dato1[0].equals((vista.equals("0") || bean_lab == null) ? " " : bean_lab.getOcupacion())) ? "selected" : ""%> ><%=dato1[1]%></option>
                        <%  }
                                          }%>
                    </select>
                </div>
            </td>
            <td class="filableach" width="17%">
                Nit/Rut (para independientes)<br>
                <% String[] tr2 = null;
                            if (!vista.equals("0") && bean_lab != null) {
                                tr2 = bean_lab.getNit().split("-");
                                if (tr2.length > 1) {
                                   tr2[1] = tr2[1].replaceAll("-", "").trim();
                                   tr2[0]=tr[0].trim();
                                }
                            }%>
                <input type="text" id="nit_emp_nat" name="nit_emp_nat" value="<%= (vista.equals("0")|| bean_lab == null) ? "" : (tr2.length > 0 ? tr2[0] : "")%>" size="20"
              onkeypress="return isNumberKey(event)">
                <input type="text" id="nit_emp_nat2" name="nit_emp_nat2" value="<%= (vista.equals("0")|| bean_lab == null) ? "" : (tr2.length > 1 ? tr2[1] : "")%>" size="2"
                oonkeypress="return isNumberKey(event)">
            </td>
            <td colspan="6">
                Nombre de la empresa<br>
                <input type="text" id="nom_emp_nat" name="nom_emp_nat" value="<%= (vista.equals("0") || bean_lab == null) ? "" : bean_lab.getNombreEmpresa()%>" size="60" onchange="completarDatosNegocio(this.id)">
            </td>
            <td width="17%"style="display:none;" >
                Cargo<br>
                <input type="text" id="car_emp_nat" name="car_emp_nat" value="<%= (vista.equals("0") || bean_lab == null) ? "" : bean_lab.getCargo()%>" size="30" >
             </td>
        </tr>
        <tr class="filaazul2">
            <td style="display:none;">
                Fecha de Inicio (yyyy-MM-dd)<br>
                <input type="text" class="validate-date" id="f_ing_nat" name="f_ing_nat" value="<%= (vista.equals("0") || bean_lab == null) ? "" : Util.formatoFechaPostgres(bean_lab.getFechaIngreso())%>" size="15">
            </td>
            <td style="display:none;">
                Tipo de contrato<br>
                <select id="tipo_cont_nat" name="tipo_cont_nat" style="width:10em;">
                    <option value="">...</option>
                    <%  dato1 = null;
                                  for (int i = 0; i < tipo_contrato.size(); i++) {
                                      dato1 = ((String) tipo_contrato.get(i)).split(";_;");%>
                    <option value="<%=dato1[0]%>" <%=(dato1[0].equals((vista.equals("0")|| bean_lab == null) ? " " : bean_lab.getTipoContrato())) ? "selected" : ""%> ><%=dato1[1]%></option>
                    <%  }%>
                </select>
            </td>
            <td class="filableach" style="display:none;">
                Eps &nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp Afiliacion<br>
                <select id="eps_nat" name="eps_nat" style="width:15em;">
                    <option value="">...</option>
                    <%  dato1 = null;
                                                         for (int i = 0; i < eps.size(); i++) {
                                                             dato1 = ((String) eps.get(i)).split(";_;");%>
                    <option value="<%=dato1[0]%>" <%=(dato1[0].equals((vista.equals("0")|| bean_lab == null) ? " " : bean_lab.getEps())) ? "selected" : ""%> ><%=dato1[1]%></option>
                    <%  }%>
                </select>
                <select id="tip_afil_nat" name="tip_afil_nat" style="width:5em;">
                    <option value="">...</option>
                    <%  dato1 = null;
                                                        for (int i = 0; i < tipafil.size(); i++) {
                                                            dato1 = ((String) tipafil.get(i)).split(";_;");%>
                    <option value="<%=dato1[0]%>" <%=(dato1[0].equals((vista.equals("0")|| bean_lab == null) ? " " : bean_lab.getTipoAfiliacion())) ? "selected" : ""%> ><%=dato1[1]%></option>
                    <%  }%>
                </select>
            </td>
            <td>
                Telefono &nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp Extension<br>
                <input type="text" maxlength="7" id="tel_emp_nat" name="tel_emp_nat" value="<%= (vista.equals("0") || bean_lab == null) ? "" : bean_lab.getTelefono()%>" size="15" onkeyup="soloNumeros(this.id);">
                <input type="text" maxlength="4" id="ext_emp_nat" name="ext_emp_nat" value="<%= (vista.equals("0") || bean_lab == null) ? "" : bean_lab.getExtension()%>" size="5" onkeyup="soloNumeros(this.id);">
            </td>
            <td class="filableach">
                Celular<br>
                <input type="text" maxlength="10" id="cel_emp_nat" name="cel_emp_nat" value="<%=(vista.equals("0") || bean_lab == null) ? "" : bean_lab.getCelular()%>" size="15" onkeyup="soloNumeros(this.id);" onchange="completarDatosNegocio(this.id)">
            </td>
            <td class="filableach" colspan="6" >
                E-mail<br>
                <input type="text" id="mail_empr_nat"  name="mail_empr_nat" value="<%=(vista.equals("0") || bean_lab == null) ? "" : bean_lab.getEmail()%> " size="30" >
            </td>
        </tr>
        <tr class="filaazul2">
            <td colspan="2">
                Direccion<br>
                <table width="100%">
                    <tr>
                        <td width="90%">
                            <input type="text" id="dir_emp_nat" name="dir_emp_nat" <%=estadoDireccion%> value="<%=(vista.equals("0") || bean_lab == null) ? "" : bean_lab.getDireccion()%>" size="60" onchange="completarDatosNegocio(this.id) onblur="completarDatosNegocio(this.id)">
                        </td>
                        <td>                         
                            <img src="/fintra/images/Direcciones.png" width="26" height="23" border="0" align="middle" onclick="genDireccion('dir_emp_nat',event);" alt="Direcciones"  title="Direcciones" />
                        </td>    
                    </tr>
                </table>   
            </td>
            <td>
                Departamento<br>
                <select id="dep_emp_nat" name="dep_emp_nat" style="width:20em;" onchange="cargarCiudades('dep_emp_nat', 'ciu_emp_nat'); completarDatosNegocio(this.id)">
                    <option value="">...</option>
                    <%  dato1 = null;
                                for (int i = 0; i < optdep.size(); i++) {
                                    dato1 = ((String) optdep.get(i)).split(";_;");%>
                    <option value="<%=dato1[0]%>" <%=(dato1[0].equals((vista.equals("0") || bean_lab == null) ? " " : bean_lab.getDepartamento())) ? "selected" : ""%> ><%=dato1[1]%></option>
                    <%  }%>
                </select>
            </td>
            <td>

                Ciudad<br>
                <div id="d_ciu_emp_nat">
                    <select id="ciu_emp_nat" name="ciu_emp_nat" style="width:20em;" onchange="completarDatosNegocio(this.id)">
                        <option value="">...</option>
                        <%  if (!vista.equals("0") && bean_lab != null) {
                                        optciu = gsaserv.listadoCiudades(bean_lab.getDepartamento());
                                        for (int i = 0; i < optciu.size(); i++) {
                                        dato1 = ((String) optciu.get(i)).split(";_;");%>
                        <option value="<%=dato1[0]%>" <%=(dato1[0].equals(bean_lab.getCiudad())) ? "selected" : ""%> ><%=dato1[1]%></option>
                        <%  }
                                          }%>
                    </select>
                </div>
            </td>
            <td colspan="2"></td>
        </tr>
        <tr class="filaazul2">
            <td>
                Ingresos<br>
                $ <input type="text" id="sal_nat" maxlength="9" name="sal_nat" value="<%= (vista.equals("0") || bean_lab == null) ? "" : Util.customFormat(Double.parseDouble(bean_lab.getSalario()))%>" size="25" onkeyup="soloNumeros(this.id);" onblur="formato(this, 0)">
            </td>
            <td class="filableach">
                Otros ingresos<br>
                $ <input type="text" id="otros_nat" maxlength="9" name="otros_nat" value="<%= (vista.equals("0") || bean_lab == null) ? "0" : Util.customFormat(Double.parseDouble(bean_lab.getOtrosIngresos()))%>" size="25" onkeyup="soloNumeros(this.id);" onblur="formato(this, 0)">
            </td>
            <td class="filableach">
                Concepto otros ingresos<br>
                <input type="text" id="conc_otros_nat" name="conc_otros_nat" value="<%= (vista.equals("0") || bean_lab == null) ? "" : bean_lab.getConceptoOtrosIng()%>" size="25"  >
            </td>
            <td >
                Egresos<br>
                $ <input type="text" id="manuten_nat" maxlength="9" name="manuten_nat" value="<%= (vista.equals("0") || bean_lab == null) ? "" : Util.customFormat(Double.parseDouble(bean_lab.getGastosManutencion()))%>" size="25" onkeyup="soloNumeros(this.id);" onblur="formato(this, 0)">
            </td>
            <td>
                Gastos por creditos<br>
                $ <input type="text" id="cred_nat" maxlength="9" name="cred_nat" value="<%= (vista.equals("0") || bean_lab == null) ? "0" : Util.customFormat(Double.parseDouble(bean_lab.getGastosCreditos()))%>" size="25" onkeyup="soloNumeros(this.id);" onblur="formato(this, 0)">
            </td>
            <td >
                Gastos por arriendo o cuota de vivienda<br>
                $ <input type="text" id="arr_nat" name="arr_nat" maxlength="9" value="<%= (vista.equals("0") || bean_lab == null) ? "0" : Util.customFormat(Double.parseDouble(bean_lab.getGastosArriendo()))%>" size="25" onkeyup="soloNumeros(this.id);" onblur="formato(this, 0); completarDatosNegocio('dir_emp_nat');"> 
            </td>
        </tr>        
    </table>
</div>
<div id="refnegocio" style="display:none; visibility: hidden;">
    
</div>
<div id="codeudor" style="visibility: hidden;">
    <b>DEUDOR SOLIDARIO</b><br>
    <b>Informaci�n b�sica</b>
    <table id="tcodeudor" style="border-collapse:collapse; width:100%" border="1">
        <tr class="filaazul2" >
            <td width="17%">
                Primer apellido<br>
                <input type="text" id="pr_apellido_cod" name="pr_apellido_cod" value="<%=(vista.equals("0") || bean_perc == null) ? "" : bean_perc.getPrimerApellido()%>" size="25" <%= vista.equals("8")?"disabled":""%> onChange="conMayusculas(this)" onkeypress="return soloLetras(event)" >
            </td>
            <td class="filableach" width="16%">
                Segundo Apellido<br>
                <input type="text" id="seg_apellido_cod" name="seg_apellido_cod" value="<%=(vista.equals("0") || bean_perc == null) ? "" : bean_perc.getSegundoApellido()%>" size="25" onChange="conMayusculas(this)" onkeypress="return soloLetras(event)" >
            </td>
            <td width="17%">
                Primer Nombre<br>
                <input type="text" id="pr_nombre_cod" name="pr_nombre_cod" value="<%=(vista.equals("0") || bean_perc == null) ? "" : bean_perc.getPrimerNombre()%>" size="25"onChange="conMayusculas(this)" onkeypress="return soloLetras(event)" >
            </td>
            <td class="filableach" width="17%">
                Segundo Nombre<br>
                <input type="text" id="seg_nombre_cod" name="seg_nombre_cod" value="<%=(vista.equals("0") || bean_perc == null) ? "" : bean_perc.getSegundoNombre() %>" size="25" onChange="conMayusculas(this)" onkeypress="return soloLetras(event)" >
            </td>
            <td width="16%" >
                Genero<br>
                <select id="genero_cod" name="genero_cod">
                    <option value="">...</option>
                    <option value="M" <%= (vista.equals("0") || bean_perc == null) ? "" : bean_perc.getGenero().equals("M") ? "selected" : ""%>>Masculino</option>
                    <option value="F" <%= (vista.equals("0") || bean_perc == null) ? "" : bean_perc.getGenero().equals("F") ? "selected" : ""%>>Femenino</option>
                </select>
            </td>
            <td width="17%" >
                Estado civil<br>
                <select id="est_civil_cod" name="est_civil_cod" <%= vista.equals("8")?"disabled":""%>>
                    <option value="">...</option>
                    <%  dato1 = null;
                                for (int i = 0; i < est_civil.size(); i++) {
                                    dato1 = ((String) est_civil.get(i)).split(";_;");%>
                    <option value="<%=dato1[0]%>" <%=(dato1[0].equals((vista.equals("0") || bean_perc == null) ? " " : bean_perc.getEstadoCivil())) ? "selected" : ""%> ><%=dato1[1]%></option>
                    <%  }%>
                </select>
            </td>
        </tr>
        <tr class="filaazul2">
            <td>
                Identificacion<br>
                <select id="tipo_id_cod" name="tipo_id_cod" <%= vista.equals("8")?"disabled":""%>>
                    <option value="" selected>...</option>
                    <%  dato1 = null;
                                for (int i = 0; i < tipo_id.size(); i++) {
                                    dato1 = ((String) tipo_id.get(i)).split(";_;");%>
                    <option value="<%=dato1[0]%>" <%=(dato1[0].equals((vista.equals("0")|| bean_perc == null) ? " " : bean_perc.getTipoId())) ? "selected" : ""%> ><%=dato1[1]%></option>
                    <%  }%>
                </select>
                    <input type="text" maxlength="15" id="id_cod" name="id_cod" value="<%=(vista.equals("0") || bean_perc == null) ? "" : bean_perc.getIdentificacion()%>" size="20" onkeyup="soloNumeros(this.id);" <%= vista.equals("8")?"disabled":""%>>
                <img alt="buscar" src="<%=BASEURL%>/images/botones/iconos/lupa.gif" width="15" height="15" style="cursor:pointer;" onclick="busqueda('id_cod');">
            </td>
            <td>
                Fecha de expedicion (yyyy-MM-dd)<br>
                <input type="text" id="f_exp_cod" class="validate-date" name="f_exp_cod" value="<%=(vista.equals("0") || bean_perc == null) ? "" : Util.formatoFechaPostgres(bean_perc.getFechaExpedicionId())%>" size="15">
            </td>
            <td>
                Departamento<br>
                <select id="dep_exp_cod" name="dep_exp_cod" style="width:20em;" onchange="cargarCiudades('dep_exp_cod', 'ciu_exp_cod');">
                    <option value="">...</option>
                    <%  dato1 = null;
                                for (int j = 0; j < optdep.size(); j++) {
                                    dato1 = ((String) optdep.get(j)).split(";_;");%>
                    <option value="<%=dato1[0]%>" <%=(dato1[0].equals((vista.equals("0") || bean_perc == null) ? " " : bean_perc.getDptoExpedicionId())) ? "selected" : ""%> ><%=dato1[1]%></option>
                    <%  }%>
                </select>
            </td>
            <td>
                Ciudad<br>
                <div id="d_ciu_exp_cod">
                    <select id="ciu_exp_cod" name="ciu_exp_cod" style="width:20em;">
                        <option value="">...</option>
                        <%  if (!vista.equals("0") && bean_perc != null) {
                                        optciu = gsaserv.listadoCiudades(bean_perc.getDptoExpedicionId());
                                        for (int j = 0; j < optciu.size(); j++) {
                                        dato1 = ((String) optciu.get(j)).split(";_;");%>
                        <option value="<%=dato1[0]%>" <%=(dato1[0].equals(bean_perc.getCiudadExpedicionId())) ? "selected" : ""%> ><%=dato1[1]%></option>
                        <%  }
                                          }%>
                    </select>
                </div>
            </td>
            <td >
                Nivel de estudio<br>
                <select id="niv_est_cod" name="niv_est_cod" style="width:10em;">
                    <option value="">...</option>
                    <%  dato1 = null;
                                for (int i = 0; i < nivel_estudio.size(); i++) {
                                    dato1 = ((String) nivel_estudio.get(i)).split(";_;");%>
                    <option value="<%=dato1[0]%>" <%=(dato1[0].equals((vista.equals("0") || bean_perc == null) ? " " : bean_perc.getNivelEstudio())) ? "selected" : ""%> ><%=dato1[1]%></option>
                    <%  }%>
                </select>
            </td>
            <td class="filableach" >
                Profesion<br>
                <input type="text" id="prof_cod" name="prof_cod" value="<%=(vista.equals("0") || bean_perc == null) ? "" : bean_perc.getProfesion()%>" size="30"  >
            </td>
        </tr>
        <tr class="filaazul2">
            <td>
                Fecha de nacimiento (yyyy-MM-dd)<br>
                <input type="text" id="f_nac_cod" class="validate-date" name="f_nac_cod" onblur="validarEdad(this.id, 'Codeudor')" value="<%=(vista.equals("0") || bean_perc == null) ? "" : Util.formatoFechaPostgres(bean_perc.getFechaNacimiento())%>" size="15">
            </td>
            <td>
                Departamento<br>
                <select id="dep_nac_cod" name="dep_nac_cod" style="width:20em;" onchange="cargarCiudades('dep_nac_cod', 'ciu_nac_cod');">
                    <option value="">...</option>
                    <%  dato1 = null;
                                 for (int j = 0; j < optdep.size(); j++) {
                                     dato1 = ((String) optdep.get(j)).split(";_;");%>
                    <option value="<%=dato1[0]%>" <%=(dato1[0].equals((vista.equals("0") || bean_perc == null) ? " " : bean_perc.getDptoNacimiento())) ? "selected" : ""%> ><%=dato1[1]%></option>
                    <%  }%>
                </select>
            </td>
            <td>
                Ciudad<br>
                <div id="d_ciu_nac_cod">
                    <select id="ciu_nac_cod" name="ciu_nac_cod" style="width:20em;">
                        <option value="">...</option>
                        <%  if (!vista.equals("0") && bean_perc != null) {
                                        optciu = gsaserv.listadoCiudades(bean_perc.getDptoNacimiento());
                                        for (int j = 0; j < optciu.size(); j++) {
                                        dato1 = ((String) optciu.get(j)).split(";_;");%>
                        <option value="<%=dato1[0]%>" <%=(dato1[0].equals(bean_perc.getCiudadNacimiento())) ? "selected" : ""%> ><%=dato1[1]%></option>
                        <%  }
                                          }%>
                    </select>
                </div>
            </td>
            <td colspan="12">
                Personas a cargo<br>
                <input type="text" id="pcargo_cod" name="pcargo_cod" value="<%=(vista.equals("0") || bean_perc == null) ? "" : bean_perc.getPersonasACargo()%>" size="15" onkeyup="soloNumeros(this.id);">
            </td>
            <td style="display: none;">
                N&ordm; de hijos<br>
                <input type="text" id="nhijos_cod" name="nhijos_cod" value="<%=(vista.equals("0") || bean_perc == null) ? "" : bean_perc.getNumHijos()%>" size="15" onblur="fhijos('cod');" onkeyup="soloNumeros(this.id);">
            </td>
            <td style="display: none;">
                Total grupo familiar<br>
                <input type="text" id="ngrupo_cod" name="ngrupo_cod" value="<%=(vista.equals("0") || bean_perc == null) ? "" : bean_perc.getTotalGrupoFamiliar()%>" size="15" onkeyup="soloNumeros(this.id);">
            </td>
        </tr>
        <tr class="filaazul2">
            <td colspan="2">
                Direccion residencia<br>
                <table width="100%">
                    <tr>
                        <td width="90%">
                            <input type="text" id="dir_cod" name="dir_cod" <%=estadoDireccion%> value="<%=(vista.equals("0") || bean_perc == null) ? "" : bean_perc.getDireccion()%>" size="60">
                      </td>
                        <td>                         
                            <img src="/fintra/images/Direcciones.png" width="26" height="23" border="0" align="middle" onclick="genDireccion('dir_cod',event);" alt="Direcciones"  title="Direcciones" />
                        </td>    
                    </tr>
                </table>  
            </td>
            <td >
                Departamento<br>
                <select id="dep_cod" name="dep_cod" style="width:20em;" onchange="cargarCiudadesBarrio('dep_cod', 'ciu_cod','barrio_cod');">
                    <option value="">...</option>
                    <%  dato1 = null;
                                 for (int j = 0; j < optdep.size(); j++) {
                                     dato1 = ((String) optdep.get(j)).split(";_;");%>
                    <option value="<%=dato1[0]%>" <%=(dato1[0].equals((vista.equals("0") || bean_perc == null) ? " " : bean_perc.getDepartamento())) ? "selected" : ""%> ><%=dato1[1]%></option>
                    <%  }%>
                </select>
            </td>
            <td>
                Ciudad<br>
                <div id="d_ciu_cod">
                    <select id="ciu_cod" name="ciu_cod" style="width:20em;" onchange="cargarBarrios('ciu_cod', 'barrio_cod');">
                        <option value="">...</option>
                        <%  if (!vista.equals("0") && bean_perc != null) {
                                        optciu = gsaserv.listadoCiudades(bean_perc.getDepartamento());
                                        for (int j = 0; j < optciu.size(); j++) {
                                        dato1 = ((String) optciu.get(j)).split(";_;");%>
                        <option value="<%=dato1[0]%>" <%=(dato1[0].equals(bean_perc.getCiudad())) ? "selected" : ""%> ><%=dato1[1]%></option>
                        <%  }
                                          }%>
                    </select>
                </div>
            </td>
            <td>
                Barrio<br>
                <%  if(tipoconv.equals("Microcredito")){%>
                        <div id="d_barrio_cod">
                            <select id="barrio_cod" name="barrio_cod" style="width:20em;">
                                <option value="">...</option>
                                <%  if (!vista.equals("0") && bean_perc != null) {
                                                optbarrio = gsaserv.listadoBarrios(bean_perc.getCiudad());
                                                for (int j = 0; j < optbarrio.size(); j++) {
                                                dato1 = ((String) optbarrio.get(j)).split(";_;");%>
                                <option value="<%=dato1[0]%>" <%=(dato1[0].equals(bean_perc.getBarrio())) ? "selected" : ""%> ><%=dato1[1]%></option>
                                <%  }
                                                  }%>
                            </select>
                        </div>   
                <%  }else{%>
                      <input type="text" id="barrio_cod" name="barrio_cod" value="<%=(vista.equals("0") || bean_perc == null) ? "" : bean_perc.getBarrio()%>" size="25">
                 <% }%>
            </td>
            <td>
                Estrato<br>
                <select name="estr_cod" id="estr_cod">
                    <option value="">...</option>
                    <%
                                for (int i = 1; i < 7; i++) {
                                    out.print("<option value='" + i + "'" + ((vista.equals("0")|| bean_perc == null) ? "" : bean_perc.getEstrato().equals("" + i) ? " selected" : "") + ">" + i + "</option>");
                                }
                    %>
                </select>
                Parentesco  titular:
                <input type="text" id="id_parentesco_cod_titular" name="id_parentesco_cod_titular" size="15" value="<%=(vista.equals("0") || bean_perc == null) ? "" : bean_perc.getParentescoDeudorSolidario()%>">
            </td>
        </tr>
        <tr class="filaazul2">
            <td class="filableach" colspan="2">
                E-mail<br>
                <input type="text" id="mail_cod" name="mail_cod" class="validate-email" value="<%=(vista.equals("0") || bean_perc == null) ? "" : bean_perc.getEmail()%>" size="60" onChange="validaEmail(this);conMinusculas(this);">
            </td>
            <td>
                Tipo de vivienda<br>
                <select id="tipo_viv_cod" name="tipo_viv_cod" style="width:10em;<%= vista.equals("8")?"disabled":""%>">
                    <option value="">...</option>
                    <% for (int i = 0; i < tipo_vivienda.size(); i++) {%>
                    <option value="<%=tipo_vivienda.get(i).getCodigo()%>" <%=(tipo_vivienda.get(i).getCodigo().equals((vista.equals("0") || bean_perc == null) ? " " : bean_perc.getTipoVivienda())) ? "selected" : ""%> ><%=tipo_vivienda.get(i).getValor()%></option>
                    <%  }%>
                </select>
            </td>
            <td>
                Tiempo residencia<br>
                <% tr = null;
                            if (!vista.equals("0") && bean_perc != null) {
                                tr = bean_perc.getTiempoResidencia().split("A�os");
                                if (tr.length > 1) {
                                   tr[1] = tr[1].replaceAll("Meses", "").trim();
                                   tr[0]=tr[0].trim();
                                }
                            }%>
                <input type="text" id="tres_cod" name="tres_cod" value="<%= (vista.equals("0")|| bean_perc == null) ? "" : (tr.length > 1 ? tr[0] : "")%>" size="5"onkeyup="soloNumeros(this.id);"> A�os
                <input type="text" id="tres_cod1" name="tres_cod1" value="<%= (vista.equals("0")|| bean_perc == null) ? "" : (tr.length > 1 ? tr[1] : "")%>" size="5"onkeyup="soloNumeros(this.id);"> Meses
            </td>
            <td>
                Telefono<br>
                <input type="text" maxlength="7" id="tel_cod" name="tel_cod" value="<%=(vista.equals("0") || bean_perc == null) ? "" : bean_perc.getTelefono()%>" size="15" onkeyup="soloNumeros(this.id);">
            </td>
            <td class="filableach">
                Celular<br>
                <input type="text" maxlength="10" id="cel_cod" name="cel_cod" value="<%=(vista.equals("0") || bean_perc == null) ? "" : bean_perc.getCelular()%>" size="15" onkeyup="soloNumeros(this.id);">               
            </td>
        </tr>
    </table>
    <br>
    <b>Informaci�n laboral y econ�mica</b>
    <%
                if (!vista.equals("0")) {
                    try {
                        bean_lab = gsaserv.datosLaboral(num_solicitud, "C");
                    } catch (Exception e) {
                        System.out.println("error: " + e.toString());
                        e.printStackTrace();
                    }
                }
    %>
    <table id="tlaboralcod" style="border-collapse:collapse; width:100%" border="1">
        <tr class="filaazul2">
            <td width="17%">
                Actividad economica<br>
                <select id="act_econ_cod" name="act_econ_cod" style="width:20em;" onchange="cargarOcupaciones('act_econ_cod', 'ocup_cod');">
                    <option value="">...</option>
                    <%  dato1 = null;
                                for (int i = 0; i < act_eco.size(); i++) {
                                    dato1 = ((String) act_eco.get(i)).split(";_;");%>
                    <option value="<%=dato1[0]%>" <%=(dato1[0].equals((vista.equals("0") || bean_lab == null) ? " " : bean_lab.getActividadEconomica()))? "selected" : ""%> ><%=dato1[1]%></option>
                    <%  }%>
                </select>
            </td>
            <td width="16%">
                Ocupacion<br>
                <div id="d_ocup_cod">
                    <select id="ocup_cod" name="ocup_cod" style="width:20em;">
                        <option value="">...</option>
                        <%  if (!vista.equals("0") && bean_lab != null) {
                                        ocupacion = gsaserv.listadoOcupaciones(bean_lab.getActividadEconomica());
                                        for (int i = 0; i < ocupacion.size(); i++) {
                                        dato1 = ((String) ocupacion.get(i)).split(";_;");%>
                        <option value="<%=dato1[0]%>" <%=(dato1[0].equals((vista.equals("0") || bean_lab == null) ? " " : bean_lab.getOcupacion())) ? "selected" : ""%> ><%=dato1[1]%></option>
                        <%  }
                                          }%>
                    </select>
                </div>
            </td>
            <td width="17%" class="filableach" >
                Nit/Rut (para independientes)<br>
                <%  tr2 = null;
                            if (!vista.equals("0") && bean_lab != null) {
                                tr2 = bean_lab.getNit().split("-");
                                if (tr2.length > 1) {
                                   tr2[1] = tr2[1].replaceAll("-", "").trim();
                                   tr2[0]=tr[0].trim();
                                }
                            }%>
                <input type="text" id="nit_emp_cod" name="nit_emp_cod" value="<%= (vista.equals("0")|| bean_lab == null) ? "" : (tr2.length > 0 ? tr2[0] : "")%>" size="20" onkeyup="soloNumeros(this.id);">
                <input type="text" id="nit_emp_cod2" name="nit_emp_cod2" value="<%= (vista.equals("0")|| bean_lab == null) ? "" : (tr2.length > 1 ? tr2[1] : "")%>" size="2" onkeyup="soloNumeros(this.id);">
            </td>
            <td colspan="6">
                Nombre de la empresa<br>
                <input type="text" id="nom_emp_cod" name="nom_emp_cod" value="<%= (vista.equals("0") || bean_lab == null) ? "" : bean_lab.getNombreEmpresa()%>" size="60" >
            </td>
            <td width="17%" style="display:none;"> 
                Cargo<br>
                <input type="text" id="car_emp_cod" name="car_emp_cod" value="<%= (vista.equals("0") || bean_lab == null) ? "" : bean_lab.getCargo()%>" size="25" >
            </td>

        </tr>
        <tr class="filaazul2">
            <td style="display:none;">
                Fecha de ingreso (yyyy-MM-dd)<br>
                <input type="text" id="f_ing_cod" class="validate-date" name="f_ing_cod" value="<%= (vista.equals("0") || bean_lab == null) ? "" : Util.formatoFechaPostgres(bean_lab.getFechaIngreso())%>"  size="15">
            </td>
            <td style="display:none;">
                Tipo de contrato<br>
                <select id="tipo_cont_cod" name="tipo_cont_cod" style="width:10em;">
                    <option value="">...</option>
                    <%  dato1 = null;
                                  for (int i = 0; i < tipo_contrato.size(); i++) {
                                      dato1 = ((String) tipo_contrato.get(i)).split(";_;");%>
                    <option value="<%=dato1[0]%>" <%=(dato1[0].equals((vista.equals("0") || bean_lab == null) ? " " : bean_lab.getTipoContrato())) ? "selected" : ""%> ><%=dato1[1]%></option>
                    <%  }%>
                </select>
            </td>
            <td class="filableach" style="display:none;">
                Eps &nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp Afiliacion<br>
                <select id="eps_cod" name="eps_cod" style="width:15em;">
                    <option value="">...</option>
                    <%  dato1 = null;
                                                        for (int i = 0; i < eps.size(); i++) {
                                                            dato1 = ((String) eps.get(i)).split(";_;");%>
                    <option value="<%=dato1[0]%>" <%=(dato1[0].equals((vista.equals("0")|| bean_lab == null) ? " " : bean_lab.getEps())) ? "selected" : ""%> ><%=dato1[1]%></option>
                    <%  }%>
                </select>
                <select id="tip_afil_cod" name="tip_afil_cod" style="width:5em;">
                    <option value="">...</option>
                    <%  dato1 = null;
                                                        for (int i = 0; i < tipafil.size(); i++) {
                                                            dato1 = ((String) tipafil.get(i)).split(";_;");%>
                    <option value="<%=dato1[0]%>" <%=(dato1[0].equals((vista.equals("0")|| bean_lab == null) ? " " : bean_lab.getTipoAfiliacion())) ? "selected" : ""%> ><%=dato1[1]%></option>
                    <%  }%>
                </select>
            </td>
            <td>
                Telefono &nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp Extension<br>
                <input type="text" maxlength="7" id="tel_emp_cod" name="tel_emp_cod" value="<%= (vista.equals("0") || bean_lab == null) ? "" : bean_lab.getTelefono()%>" size="15" onkeyup="soloNumeros(this.id);">
                <input type="text" maxlength="4" id="ext_emp_cod" name="ext_emp_cod" value="<%= (vista.equals("0") || bean_lab == null) ? "" : bean_lab.getExtension()%>" size="5" onkeyup="soloNumeros(this.id);">
            </td>
            <td class="filableach" width="16%">
                Celular<br>
                <input type="text" maxlength="10" id="cel_emp_cod" name="cel_emp_cod" value="<%= (vista.equals("0") || bean_lab == null) ? "" : bean_lab.getCelular()%>" size="15" onkeyup="soloNumeros(this.id);">
            </td>
            <td class="filableach" colspan="6" >
                E-mail<br>
                <input type="text" id="mail_emp_cod"  name="mail_emp_cod" value="<%= (vista.equals("0") || bean_lab == null) ? "" : bean_lab.getEmail()%>" size="30" >
            </td>
        </tr>
        <tr class="filaazul2">
            <td colspan="2">
                Direccion<br>
                <table width="100%">
                    <tr>
                        <td width="90%">
                            <input type="text" id="dir_emp_cod" name="dir_emp_cod" <%=estadoDireccion%> value="<%= (vista.equals("0") || bean_lab == null) ? "" : bean_lab.getDireccion()%>" size="60">
                        </td>
                        <td>                         
                            <img src="/fintra/images/Direcciones.png" width="26" height="23" border="0" align="middle" onclick="genDireccion('dir_emp_cod',event);" alt="Direcciones"  title="Direcciones" />
                        </td>    
                    </tr>
                </table>   
            </td>
            <td>
                Departamento<br>
                <select id="dep_emp_cod" name="dep_emp_cod" style="width:20em;" onchange="cargarCiudades('dep_emp_cod', 'ciu_emp_cod');">
                    <option value="">...</option>
                    <%  dato1 = null;
                                 for (int j = 0; j < optdep.size(); j++) {
                                     dato1 = ((String) optdep.get(j)).split(";_;");%>
                    <option value="<%=dato1[0]%>" <%=(dato1[0].equals((vista.equals("0") || bean_lab == null) ? " " : bean_lab.getDepartamento())) ? "selected" : ""%> ><%=dato1[1]%></option>
                    <%  }%>
                </select>
            </td>
            <td>
                Ciudad<br>
                <div id="d_ciu_emp_cod">
                    <select id="ciu_emp_cod" name="ciu_emp_cod" style="width:20em;">
                        <option value="">...</option>
                        <%  if (!vista.equals("0") && bean_lab != null) {
                                        optciu = gsaserv.listadoCiudades(bean_lab.getDepartamento());
                                        for (int j = 0; j < optciu.size(); j++) {
                                        dato1 = ((String) optciu.get(j)).split(";_;");%>
                        <option value="<%=dato1[0]%>" <%=(dato1[0].equals(bean_lab.getCiudad())) ? "selected" : ""%> ><%=dato1[1]%></option>
                        <%  }
                                          }%>
                    </select>
                </div>
            </td>
            <td colspan="2"></td>            
        </tr>
        <tr class="filaazul2">
            <td>
                Ingresos<br>
                $ <input type="text" id="sal_cod" name="sal_cod" value="<%= (vista.equals("0") || bean_lab == null) ? "" : Util.customFormat(Double.parseDouble(bean_lab.getSalario()))%>" size="15" onkeyup="soloNumeros(this.id);" onblur="formato(this, 0)">
            </td>
            <td class="filableach">
                Otros ingresos<br>
                $ <input type="text" id="otros_cod" name="otros_cod" value="<%= (vista.equals("0") || bean_lab == null) ? "0" : Util.customFormat(Double.parseDouble(bean_lab.getOtrosIngresos()))%>" size="15" onkeyup="soloNumeros(this.id);" onblur="formato(this, 0)">
            </td>
            <td class="filableach" >
                Concepto otros ingresos<br>
                <input type="text" id="conc_otros_cod" name="conc_otros_cod" value="<%= (vista.equals("0") || bean_lab == null) ? "" : bean_lab.getConceptoOtrosIng()%>" size="25"   >
            </td>
            <td >
                Egresos<br>
                $ <input type="text" id="manuten_cod" name="manuten_cod" value="<%= (vista.equals("0") || bean_lab == null) ? "" : Util.customFormat(Double.parseDouble(bean_lab.getGastosManutencion()))%>" size="25" onkeyup="soloNumeros(this.id);" onblur="formato(this, 0)">
            </td>
            <td >
                Gastos por creditos<br>
                $ <input type="text" id="cred_cod" name="cred_cod" value="<%= (vista.equals("0") || bean_lab == null) ? "0" : Util.customFormat(Double.parseDouble(bean_lab.getGastosCreditos()))%>" size="25" onkeyup="soloNumeros(this.id);" onblur="formato(this, 0)">
            </td>
            <td >
                Gastos por arriendo o cuota de vivienda<br>
                $ <input type="text" id="arr_cod" name="arr_cod" value="<%= (vista.equals("0") || bean_lab == null) ? "0" : Util.customFormat(Double.parseDouble(bean_lab.getGastosArriendo()))%>" size="25" onkeyup="soloNumeros(this.id);" onblur="formato(this, 0)">
            </td>
        </tr>
    </table>
    <br>
    <b>Referencias personales</b>
    <%if (!vista.equals("0")) {
                    try {
                        lista_referencias = gsaserv.buscarReferencias(num_solicitud, "C", "P");
                    } catch (Exception e) {
                        System.out.println("error: " + e.toString());
                        e.printStackTrace();
                    }
                }
    %>
    <table id="treferenciaperscod" style="border-collapse:collapse; width:100%" border="1">
        <%
      for (int i = 1; i < 3; i++) {
                        if (lista_referencias != null && lista_referencias.size() > i-1) {
                            b_ref = lista_referencias.get(i - 1);
                        }
        %>
        <tr class="filaazul2">
            <td width="17%" >
                Primer Apellido<br>
                <input type="text" id="pr_apellido_refp_cod<%= i%>" name="pr_apellido_refp_cod<%= i%>" value="<%= (vista.equals("0") || b_ref == null) ? "" : b_ref.getPrimerApellido()%>" size="25" >
            </td>
            <td class="filableach" width="16%" >
                Segundo Apellido<br>
                <input type="text" id="seg_apellido_refp_cod<%= i%>" name="seg_apellido_refp_cod<%= i%>" value="<%=(vista.equals("0") || b_ref == null) ? "" : b_ref.getSegundoApellido()%>" size="25" >
            </td>
            <td width="17%">
                Primer Nombre<br>
                <input type="text" id="pr_nombre_refp_cod<%= i%>" name="pr_nombre_refp_cod<%= i%>" value="<%= (vista.equals("0") || b_ref == null) ? "" : b_ref.getPrimerNombre()%>" size="25" >
            </td>
            <td class="filableach" width="17%">
                Segundo Nombre<br>
                <input type="text" id="seg_nombre_refp_cod<%= i%>" name="seg_nombre_refp_cod<%= i%>" value="<%= (vista.equals("0") || b_ref == null) ? "" : b_ref.getSegundoNombre()%>" size="25" >
            </td>
            <td width="16%" class="filableach" >
                Tiempo de conocido<br>
                <input type="text" id="tconocido_refp_cod<%= i%>" name="tconocido_refp_cod<%= i%>" value="<%=(vista.equals("0") || b_ref == null) ? "" : b_ref.getTiempoConocido()%>" size="15" onkeyup="soloNumeros(this.id);"> A�os
            </td>
            <td class="filableach" width="17%" colspan="12">
                Celular<br>
                <input type="text" maxlength="10" id="cel_refp_cod<%= i%>" name="cel_refp_cod<%= i%>" value="<%= (vista.equals("0") || b_ref == null) ? "" : b_ref.getCelular()%>" size="15" onkeyup="soloNumeros(this.id);">
            </td>
        </tr>
        <tr class="filaazul2">
            <td>
                Telefono 1<br>
                <input type="text" maxlength="10" id="tel1_refp_cod<%= i%>" name="tel1_refp_cod<%= i%>" value="<%=(vista.equals("0") || b_ref == null) ? "" : b_ref.getTelefono()%>" size="15" onkeyup="soloNumeros(this.id);">
            </td>
            <td class="filableach">
                Telefono 2 &nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp Extension<br>
                <input type="text" maxlength="7" id="tel2_refp_cod<%= i%>" name="tel2_refp_cod" value="<%= (vista.equals("0") || b_ref == null) ? "" : b_ref.getTelefono2()%>" size="15" onkeyup="soloNumeros(this.id);">
                <input type="text" maxlength="4" id="ext_refp_cod<%= i%>" name="ext_refp_cod" value="<%=(vista.equals("0") || b_ref == null) ? "" : b_ref.getExtension()%>" size="5" onkeyup="soloNumeros(this.id);">
            </td>
            <td>
                Departamento<br>
                <select id="dep_refp_cod<%= i%>" name="dep_refp_cod<%= i%>" style="width:20em;" onchange="cargarCiudades('dep_refp_cod<%= i%>', 'ciu_refp_cod<%= i%>');">
                    <option value="">...</option>
                    <%  dato1 = null;
                                for (int j = 0; j < optdep.size(); j++) {
                                    dato1 = ((String) optdep.get(j)).split(";_;");%>
                    <option value="<%=dato1[0]%>" <%=(dato1[0].equals((vista.equals("0") || b_ref == null) ? " " : b_ref.getDepartamento())) ? "selected" : ""%> ><%=dato1[1]%></option>
                    <%  }%>
                </select>
            </td>
            <td>
                Ciudad<br>
                <div id="d_ciu_refp_cod<%= i%>">
                    <select id="ciu_refp_cod<%= i%>" name="ciu_refp_cod<%= i%>" style="width:20em;">
                        <option value="">...</option>
                        <%  if (!vista.equals("0") && b_ref != null) {
                                        optciu = gsaserv.listadoCiudades(b_ref.getDepartamento());
                                        for (int j = 0; j < optciu.size(); j++) {
                                        dato1 = ((String) optciu.get(j)).split(";_;");%>
                        <option value="<%=dato1[0]%>" <%=(dato1[0].equals(b_ref.getCiudad())) ? "selected" : ""%> ><%=dato1[1]%></option>
                        <%  }
                                          }%>
                    </select>
                </div>
            </td>
            <td class="filableach" style="display:none;">
                E-mail<br>
                <input type="text" id="mail_refp_cod<%= i%>" name="mail_refp_cod<%= i%>" value="<%= (vista.equals("0") || b_ref == null) ? "" : b_ref.getEmail()%>">
            </td>
            
            <td colspan="6">
                Direccion<br>
                <table width="100%">
                    <tr>
                        <td width="30%">
                            <input type="text" id="dir_ref_cod<%= i%>" name="dir_ref_cod<%= i%>" <%=estadoDireccion%> value="<%=(vista.equals("0") || b_ref == null) ? "" : b_ref.getDireccion()%>">
                        </td>
                        <td>                      
                            <img src="/fintra/images/Direcciones.png" width="26" height="23" border="0" align="middle" onclick="genDireccion('dir_ref_cod<%= i%>',event);" alt="Direcciones"  title="Direcciones" />
                        </td>    
                    </tr>
                </table>
            </td>         
                        <%}%>
   </table>    
    <br>
    <b>Referencias familiares</b>
    <%
                try {
                    lista_referencias = gsaserv.buscarReferencias(num_solicitud, "C", "F");
                } catch (Exception e) {
                    System.out.println("error: " + e.toString());
                    e.printStackTrace();
                }
                
    %>
    <table id="treferenciafamcod" style="border-collapse:collapse; width:100%" border="1">
        <%
                 for (int i = 1; i < 3; i++) {
                        if (lista_referencias != null && lista_referencias.size() > i-1) {
                            b_ref = lista_referencias.get(i - 1);
                        }
        %>
        <tr class="filaazul2">
            <td width="17%">
                Primer Apellido<br>
                <input type="text" id="pr_apellido_refam_cod<%= i%>" name="pr_apellido_refam_cod<%= i%>" value="<%= (vista.equals("0") || b_ref == null) ? "" : b_ref.getPrimerApellido()%>" size="25" >
            </td>
            <td class="filableach" width="16%">
                Segundo Apellido<br>
                <input type="text" id="seg_apellido_refam_cod<%= i%>" name="seg_apellido_refam_cod<%= i%>" value="<%= (vista.equals("0") || b_ref == null) ? "" : b_ref.getSegundoApellido()%>" size="25" >
            </td>
            <td width="17%">
                Primer Nombre<br>
                <input type="text" id="pr_nombre_refam_cod<%= i%>" name="pr_nombre_refam_cod<%= i%>" value="<%= (vista.equals("0") || b_ref == null) ? "" : b_ref.getPrimerNombre()%>" size="25" >
            </td>
            <td class="filableach" width="17%">
                Segundo Nombre<br>
                <input type="text" id="seg_nombre_refam_cod<%= i%>" name="seg_nombre_refam_cod<%= i%>" value="<%= (vista.equals("0") || b_ref == null) ? "" : b_ref.getSegundoNombre()%>" size="25" >
            </td>
            <td width="16%">
                Parentesco<br>
                <select id="parent_refam_cod<%= i%>" name="parent_refam_cod<%= i%>" style="width:20em;" >
                    <option value="">...</option>
                    <%  dato1 = null;
                                            for (int j = 0; j < parentesco.size(); j++) {
                                                dato1 = ((String) parentesco.get(j)).split(";_;");%>
                    <option value="<%=dato1[0]%>" <%=(dato1[0].equals((vista.equals("0") || b_ref == null) ? " " : b_ref.getParentesco())) ? "selected" : ""%> ><%=dato1[1]%></option>
                    <%  }%>
                </select>
            </td>
            <td class="filableach" width="17%" colspan="12">
                Celular<br>
                <input type="text" maxlength="10" id="cel_refam_cod<%= i%>" name="cel_refam_cod<%= i%>" value="<%= (vista.equals("0") || b_ref == null) ? "" : b_ref.getExtension()%>" size="15" onkeyup="soloNumeros(this.id);">
            </td>
        </tr>
        <tr class="filaazul2">
            <td>
                Telefono 1<br>
                <input type="text" maxlength="10" id="tel1_refam_cod<%= i%>" name="tel1_refam_cod<%= i%>" value="<%= (vista.equals("0") || b_ref == null) ? "" : b_ref.getTelefono()%>" size="15" onkeyup="soloNumeros(this.id);">
            </td>
            <td class="filableach">
                Telefono 2 &nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp Extension<br>
                <input type="text" maxlength="7" id="tel2_refam_cod<%= i%>" name="tel2_refam_cod" value="<%=(vista.equals("0") || b_ref == null) ? "" : b_ref.getTelefono2()%>" size="15" onkeyup="soloNumeros(this.id);">
                <input type="text" maxlength="4" id="ext_refam_cod<%= i%>" name="ext_refam_cod" value="<%= (vista.equals("0") || b_ref == null) ? "" : b_ref.getExtension()%>" size="5" onkeyup="soloNumeros(this.id);">
            </td>
            <td>
                Departamento<br>
                <select id="dep_refam_cod<%= i%>" name="dep_refam_cod<%= i%>" style="width:20em;" onchange="cargarCiudades('dep_refam_cod<%= i%>', 'ciu_refam_cod<%= i%>');">
                    <option value="">...</option>
                    <%  dato1 = null;
                                for (int j = 0; j < optdep.size(); j++) {
                                    dato1 = ((String) optdep.get(j)).split(";_;");%>
                    <option value="<%=dato1[0]%>" <%=(dato1[0].equals((vista.equals("0") || b_ref == null) ? " " : b_ref.getDepartamento())) ? "selected" : ""%> ><%=dato1[1]%></option>
                    <%  }%>
                </select>
            </td>
            <td>
                Ciudad<br>
                <div id="d_ciu_refam_cod<%= i%>">
                    <select id="ciu_refam_cod<%= i%>" name="ciu_refam_cod<%= i%>" style="width:20em;">
                        <option value="">...</option>
                        <%  if (!vista.equals("0") && b_ref != null) {
                                        optciu = gsaserv.listadoCiudades(b_ref.getDepartamento());
                                        for (int j = 0; j < optciu.size(); j++) {
                                        dato1 = ((String) optciu.get(j)).split(";_;");%>
                        <option value="<%=dato1[0]%>" <%=(dato1[0].equals(b_ref.getCiudad())) ? "selected" : ""%> ><%=dato1[1]%></option>
                        <%  }
                                          }%>
                    </select>
                </div>
            </td>
            <td class="filableach" style="display:none;">
                E-mail<br>
                <input type="text" id="mail_refam_cod<%= i%>"  name="mail_refam_cod<%= i%>" value="<%= (vista.equals("0") || b_ref == null) ? "" : b_ref.getEmail()%> ">
            </td>
            <td colspan="6">
                Direccion<br>
                <table width="100%">
                    <tr>
                        <td width="30%">
                            <input type="text" id="dir_refam_cod<%= i%>" name="dir_refam_cod<%= i%>" <%=estadoDireccion%> value="<%=(vista.equals("0") || b_ref == null) ? "" : b_ref.getDireccion()%>">
                        </td>
                        <td>                      
                            <img src="/fintra/images/Direcciones.png" width="26" height="23" border="0" align="middle" onclick="genDireccion('dir_refam_cod<%= i%>',event);" alt="Direcciones"  title="Direcciones" />
                        </td>    
                    </tr>
                </table>
            </td>
        </tr>
<% }%>
    </table>
    <%
                try {
                    lista_referencias = gsaserv.buscarReferencias(num_solicitud, "C", "C");
                } catch (Exception e) {
                    System.out.println("error: " + e.toString());
                    e.printStackTrace();
                }
                
    %>
    <b>Referencias comerciales</b>
    <table id="trefjuridica" style="border-collapse:collapse; width:100%" border="1">
        <%
                    for (int i = 1; i < 2; i++) {

                        b_ref = null;
                        if (lista_referencias != null) {
                            try {
                                if (lista_referencias.size() >= i) {
                                    b_ref = lista_referencias.get(i - 1);
                                }
                            } catch (Exception exc) {
                                System.out.println("errror2: " + exc.toString());
                            }

        %>
        <tr class="filaazul2">
            <td width="17%">
                Nombre o razon social<br>
                <input type="text" id="raz_soc_ref_cod<%= i%>" name="raz_soc_ref_cod<%= i%>" value="<%=(vista.equals("0") || b_ref == null) ? "" : b_ref.getNombre()%>" size="30" >
            </td>
            <td class="filableach" width="16%">
                Direccion<br>
                <table width="100%">
                    <tr>
                        <td width="90%">
                            <input type="text" id="raz_soc_dir_cod<%= i%>" name="raz_soc_dir_cod<%= i%>" <%=estadoDireccion%> value="<%=(vista.equals("0") || b_ref == null) ? "" : b_ref.getDireccion()%>" size="30">
                        </td>
                        <td>                         
                            <img src="/fintra/images/Direcciones.png" width="26" height="23" border="0" align="middle" onclick="genDireccion('raz_soc_dir_cod<%= i%>',event);" alt="Direcciones"  title="Direcciones" />
                        </td>    
                    </tr>
                </table> 
            </td>
            <td width="17%">
                Departamento<br>
                <select id="dep_raz_soc_cod<%= i%>" name="dep_raz_soc_cod<%= i%>" style="width:20em;" onchange="cargarCiudades('dep_raz_soc_cod<%= i%>', 'ciu_raz_soc_cod<%= i%>');">
                    <option value="">...</option>
                    <%  dato1 = null;
                                                for (int j = 0; j < optdep.size(); j++) {
                                                    dato1 = ((String) optdep.get(j)).split(";_;");%>
                    <option value="<%=dato1[0]%>" <%=(dato1[0].equals((vista.equals("0") || b_ref == null) ? " " : b_ref.getDepartamento())) ? "selected" : ""%> ><%=dato1[1]%></option>
                    <%  }%>
                </select>
            </td>
            <td width="17%">
                Ciudad<br>
                <div id="d_ciu_raz_soc_cod<%= i%>">
                    <select id="ciu_raz_soc_cod<%= i%>" name="ciu_raz_soc_cod<%= i%>" style="width:20em;">
                        <option value="">...</option>
                        <%  if (!vista.equals("0") && b_ref != null) {
                                                        optciu = gsaserv.listadoCiudades(b_ref.getDepartamento());
                                                        for (int j = 0; j < optciu.size(); j++) {
                                                            dato1 = ((String) optciu.get(j)).split(";_;");%>
                        <option value="<%=dato1[0]%>" <%=(dato1[0].equals(b_ref.getCiudad())) ? "selected" : ""%> ><%=dato1[1]%></option>
                        <%  }
                                                    }%>
                    </select>
                </div>
            </td>

            <td width="16%">
                Telefono 1<br>
                <input type="text" maxlength="10" id="raz_soc_tel_cod1<%= i%>" name="raz_soc_tel_cod1<%= i%>" value="<%=(vista.equals("0") || b_ref == null) ? "" : b_ref.getTelefono()%>" size="15" onkeyup="soloNumeros(this.id);">
            </td>
            <td class="filableach" width="17%">
                Telefono 2<br>
                <input type="text" maxlength="10" id="raz_soc_tel_cod2<%= i%>" name="raz_soc_tel_cod2<%= i%>" value="<%=(vista.equals("0") || b_ref == null) ? "" : b_ref.getTelefono2()%>" size="15" onkeyup="soloNumeros(this.id);">
            </td>
            <td>
                Tipo referencia<br>
                <div id="id_tipo_div">
                    <select id="id_tipo_refc_de_cod<%= i%>" name="id_tipo_refc_de_cod<%= i%>" style="width:10em;">
                        <option value="">...</option>
                        <option value="P" <%= b_ref == null ? "" : (b_ref.getReferenciaComercial().equals("P") ? "selected" : "")%>>Proveedor</option>
                        <option value="C" <%= b_ref == null ? "" : (b_ref.getReferenciaComercial().equals("C") ? "selected" : "")%>>Cliente</option>
                    </select>                
                </div>                    
            </td>
        </tr>
        <%
                        }
                    }
        %>
    </table>
</div>
<div id="estudiante" style="visibility: hidden;">
    <b>ESTUDIANTE</b><br>
    <b>Informaci&oacute;n b&aacute;sica</b>
    <table id="testudiante" style="border-collapse:collapse; width:100%" border="1">
        <tr class="filaazul2">
            <td width="17%">
                Primer Apellido<br>
                <input type="text" id="pr_apellido_est" name="pr_apellido_est" value="<%= (vista.equals("0") || bean_pere == null) ? "" : bean_pere.getPrimerApellido()%>" size="25" onChange="conMayusculas(this)" onkeypress="return soloLetras(event)" >
            </td>
            <td class="filableach" width="16%">
                Segundo Apellido<br>
                <input type="text" id="seg_apellido_est" name="seg_apellido_est" value="<%= (vista.equals("0") || bean_pere == null) ? "" : bean_pere.getSegundoApellido()%>" size="25" onChange="conMayusculas(this)" onkeypress="return soloLetras(event)" >
            </td>
            <td width="17%">
                Primer Nombre<br>
                <input type="text" id="pr_nombre_est" name="pr_nombre_est" value="<%= (vista.equals("0") || bean_pere == null) ? "" : bean_pere.getPrimerNombre()%>" size="25" onChange="conMayusculas(this)" onkeypress="return soloLetras(event)" >
            </td>
            <td class="filableach" width="17%">
                Segundo Nombre<br>
                <input type="text" id="seg_nombre_est" name="seg_nombre_est" value="<%= (vista.equals("0") || bean_pere == null) ? "" : bean_pere.getSegundoNombre()%>" size="25" onChange="conMayusculas(this)" onkeypress="return soloLetras(event)"  >
            </td>
            <td width="16%">
                Genero<br>
                <select id="genero_est" name="genero_est">
                    <option value="">...</option>
                    <option value="M" <%= (vista.equals("0") || bean_pere == null) ? "" : bean_pere.getGenero().equals("M") ? "selected" : ""%>>Masculino</option>
                    <option value="F" <%= (vista.equals("0") || bean_pere == null) ? "" : bean_pere.getGenero().equals("F") ? "selected" : ""%>>Femenino</option>
                </select>
            </td>
            <td width="17%" >
                Estado civil<br>
                <select id="est_civil_est" name="est_civil_est">
                    <option value="">...</option>
                    <%  dato1 = null;
                                for (int i = 0; i < est_civil.size(); i++) {
                                    dato1 = ((String) est_civil.get(i)).split(";_;");%>
                    <option value="<%=dato1[0]%>" <%=(dato1[0].equals((vista.equals("0") || bean_pere == null) ? " " : bean_pere.getEstadoCivil())) ? "selected" : ""%> ><%=dato1[1]%></option>
                    <%  }%>
                </select>
            </td>

        </tr>
        <tr class="filaazul2">
            <td>
                Identificacion<br>
                <select id="tipo_id_est" name="tipo_id_est">
                    <option value="" selected>...</option>
                    <%  dato1 = null;
                                for (int i = 0; i < tipo_id.size(); i++) {
                                    dato1 = ((String) tipo_id.get(i)).split(";_;");%>
                    <option value="<%=dato1[0]%>" <%=(dato1[0].equals((vista.equals("0")|| bean_pere == null) ? " " : bean_pere.getTipoId())) ? "selected" : ""%> ><%=dato1[1]%></option>
                    <%  }%>
                </select>
                    <input type="text" maxlength="15" id="id_est" name="id_est" value="<%= (vista.equals("0") || bean_pere == null) ? "" : bean_pere.getIdentificacion()%>" size="20" onkeyup="soloNumeros(this.id);">
                <img alt="buscar" src="<%=BASEURL%>/images/botones/iconos/lupa.gif" width="15" height="15" style="cursor:pointer;" onclick="busqueda('id_est');">
            </td>
            <td>
                Fecha de expedicion (yyyy-MM-dd)<br>
                <input type="text" id="f_exp_est" class="validate-date" name="f_exp_est" value="<%= (vista.equals("0") || bean_pere == null) ? "" : Util.formatoFechaPostgres(bean_pere.getFechaExpedicionId())%>" size="15" >
            </td>
            <td>
                Departamento<br>
                <select id="dep_exp_est" name="dep_exp_est" style="width:20em;" onchange="cargarCiudades('dep_exp_est', 'ciu_exp_est');">
                    <option value="">...</option>
                    <%  dato1 = null;
                                for (int j = 0; j < optdep.size(); j++) {
                                    dato1 = ((String) optdep.get(j)).split(";_;");%>
                    <option value="<%=dato1[0]%>" <%=(dato1[0].equals((vista.equals("0") || bean_pere == null) ? " " : bean_pere.getDptoExpedicionId())) ? "selected" : ""%> ><%=dato1[1]%></option>
                    <%  }%>
                </select>
            </td>
            <td>
                Ciudad<br>
                <div id="d_ciu_exp_est">
                    <select id="ciu_exp_est" name="ciu_exp_est" style="width:20em;">
                        <option value="">...</option>
                        <%  if (!vista.equals("0") && bean_pere != null) {
                                        optciu = gsaserv.listadoCiudades(bean_pere.getDptoExpedicionId());
                                        for (int j = 0; j < optciu.size(); j++) {
                                        dato1 = ((String) optciu.get(j)).split(";_;");%>
                        <option value="<%=dato1[0]%>" <%=(dato1[0].equals(bean_pere.getCiudadExpedicionId())) ? "selected" : ""%> ><%=dato1[1]%></option>
                        <%  }
                                          }%>
                    </select>
                </div>
            </td>
            <td colspan="2">
                Parentesco con el girador<br>
                <select id="parentesco" name="parentesco" style="width:20em;" onchange="datsolic();" >
                    <option value="">...</option>
                    <%  dato1 = null;
                                            for (int j = 0; j < parentesco.size(); j++) {
                                                dato1 = ((String) parentesco.get(j)).split(";_;");%>
                    <option value="<%=dato1[0]%>" <%=(dato1[0].equals((vista.equals("0") || bean_est == null) ? " " : bean_est.getParentescoGirador()) )? "selected" : ""%> ><%=dato1[1]%></option>
                    <%  }%>
                </select>
            </td>
        </tr>
        <tr class="filaazul2">
            <td>
                Fecha de nacimiento (yyyy-MM-dd)<br>
                <input type="text" id="f_nac_est" class="validate-date" name="f_nac_est" value="<%= (vista.equals("0") || bean_pere == null) ? "" : Util.formatoFechaPostgres(bean_pere.getFechaNacimiento())%>" size="15" >
            </td>
            <td>
                Departamento<br>
                <select id="dep_nac_est" name="dep_nac_est" style="width:20em;" onchange="cargarCiudades('dep_nac_est', 'ciu_nac_est');">
                    <option value="">...</option>
                    <%  dato1 = null;
                                 for (int j = 0; j < optdep.size(); j++) {
                                     dato1 = ((String) optdep.get(j)).split(";_;");%>
                    <option value="<%=dato1[0]%>" <%=(dato1[0].equals((vista.equals("0") || bean_pere == null) ? " " : bean_pere.getDptoNacimiento())) ? "selected" : ""%> ><%=dato1[1]%></option>
                    <%  }%>
                </select>
            </td>
            <td>
                Ciudad<br>
                <div id="d_ciu_nac_est">
                    <select id="ciu_nac_est" name="ciu_nac_est" style="width:20em;">
                        <option value="">...</option>
                        <%  if (!vista.equals("0") && bean_pere != null) {
                                        optciu = gsaserv.listadoCiudades(bean_pere.getDptoNacimiento());
                                        for (int j = 0; j < optciu.size(); j++) {
                                         dato1 = ((String) optciu.get(j)).split(";_;");%>
                        <option value="<%=dato1[0]%>" <%=(dato1[0].equals(bean_pere.getCiudadNacimiento())) ? "selected" : ""%> ><%=dato1[1]%></option>
                        <%  }
                                          }%>
                    </select>
                </div>
            </td>
            <td class="filableach">
                Personas a cargo<br>
                <input type="text" id="pcargo_est" name="pcargo_est" value="<%= (vista.equals("0") || bean_pere == null) ? "" : bean_pere.getPersonasACargo()%>" size="15" onkeyup="soloNumeros(this.id);">
            </td>
            <td class="filableach">
                N&ordm; de hijos<br>
                <input type="text" id="nhijos_est" name="nhijos_est" value="<%= (vista.equals("0") || bean_pere == null) ? "" : bean_pere.getNumHijos()%>" size="15" onblur="fhijos('est');" onkeyup="soloNumeros(this.id);">
            </td>
            <td class="filableach">
                Total grupo familiar<br>
                <input type="text" id="ngrupo_est" name="ngrupo_est" value="<%= (vista.equals("0") || bean_pere == null) ? "" : bean_pere.getTotalGrupoFamiliar()%>" size="15" onkeyup="soloNumeros(this.id);">
            </td>
        </tr>
        <tr class="filaazul2">
            <td colspan="2">
                Direccion Residencia<br>
                <table width="100%">
                    <tr>
                        <td width="90%">
                            <input type="text" id="dir_est" name="dir_est" <%=estadoDireccion%> value="<%= (vista.equals("0") || bean_pere == null) ? "" : bean_pere.getDireccion()%>" size="60">
                        </td>
                        <td>                         
                            <img src="/fintra/images/Direcciones.png" width="26" height="23" border="0" align="middle" onclick="genDireccion('dir_est',event);" alt="Direcciones"  title="Direcciones" />
                        </td>    
                    </tr>
                </table>  
            </td>
            <td>
                Departamento<br>
                <select id="dep_est" name="dep_est" style="width:20em;" onchange="cargarCiudadesBarrio('dep_est', 'ciu_est','barrio_est');">
                    <option value="">...</option>
                    <%  dato1 = null;
                                 for (int j = 0; j < optdep.size(); j++) {
                                     dato1 = ((String) optdep.get(j)).split(";_;");%>
                    <option value="<%=dato1[0]%>" <%=(dato1[0].equals((vista.equals("0") || bean_pere == null) ? " " : bean_pere.getDepartamento())) ? "selected" : ""%> ><%=dato1[1]%></option>
                    <%  }%>
                </select>
            </td>
            <td>
                Ciudad<br>
                <div id="d_ciu_est">
                    <select id="ciu_est" name="ciu_est" style="width:20em;" onchange="cargarBarrios('ciu_est', 'barrio_est');"> 
                        <option value="">...</option>
                        <%  if (!vista.equals("0") && bean_pere != null) {
                                        optciu = gsaserv.listadoCiudades(bean_pere.getDepartamento());
                                        for (int j = 0; j < optciu.size(); j++) {
                                         dato1 = ((String) optciu.get(j)).split(";_;");%>
                        <option value="<%=dato1[0]%>" <%=(dato1[0].equals(bean_pere.getCiudad())) ? "selected" : ""%> ><%=dato1[1]%></option>
                        <%  }
                                          }%>
                    </select>
                </div>
            </td>
            <td class="filableach">
                Barrio<br>
            <%  if(tipoconv.equals("Microcredito")){%>
                    <div id="d_barrio_est">
                        <select id="barrio_est" name="barrio_est" style="width:20em;">
                            <option value="">...</option>
                            <%  if (!vista.equals("0") && bean_pere != null) {
                                            optbarrio = gsaserv.listadoBarrios(bean_pere.getCiudad());
                                            for (int j = 0; j < optbarrio.size(); j++) {
                                             dato1 = ((String) optbarrio.get(j)).split(";_;");%>
                            <option value="<%=dato1[0]%>" <%=(dato1[0].equals(bean_pere.getBarrio())) ? "selected" : ""%> ><%=dato1[1]%></option>
                            <%  }
                                              }%>
                        </select>
                    </div>
            <%  }else{%>
                    <input type="text" id="barrio_est" name="barrio_est" value="<%= (vista.equals("0") || bean_pere == null) ? "" : bean_pere.getBarrio()%>" size="25">
            <%  }%>
            </td>
            <td>
                Estrato<br>
                <select name="estr_est" id="estr_est" style="width:10em;">
                    <option value="">...</option>
                    <%
                                for (int i = 1; i < 7; i++) {
                                    out.print("<option value='" + i + "'" + ((vista.equals("0")|| bean_pere == null) ? "" : bean_pere.getEstrato().equals("" + i) ? " selected" : "") + ">" + i + "</option>");
                                }
                    %>
                </select>
            </td>
        </tr>
        <tr class="filaazul2">
            <td colspan="2">
                E-mail<br>
                <input type="text" id="mail_est" name="mail_est" class="validate-email" value="<%= (vista.equals("0") || bean_pere == null) ? "" : bean_pere.getEmail()%>" size="60" onChange="validaEmail(this);conMinusculas(this)">
            </td>
            <td class="filableach">
                Tipo de vivienda<br>
                <select id="tipo_viv_est" name="tipo_viv_est" style="width:10em;">
                    <option value="">...</option>
                    <% for (int i = 0; i < tipo_vivienda.size(); i++) {%>
                    <option value="<%=tipo_vivienda.get(i).getCodigo()%>" <%=(tipo_vivienda.get(i).getCodigo().equals((vista.equals("0") || bean_pere == null) ? " " : bean_pere.getTipoVivienda())) ? "selected" : ""%> ><%=tipo_vivienda.get(i).getValor()%></option>
                    <%  }%>
                </select>
            </td>
            <td class="filableach">
                Tiempo residencia<br>
                <% tr = null;
                           if (!vista.equals("0") && bean_pere != null) {
                               tr = bean_pere.getTiempoResidencia().split("A�os");
                               if (tr.length > 1) {
                                  tr[1] = tr[1].replaceAll("Meses", "").trim();
                                  tr[0]=tr[0].trim();
                               }
                           }%>
                <input type="text" id="tres_est" name="tres_est" value="<%= (vista.equals("0")|| bean_pere == null) ? "" : (tr.length > 1 ? tr[0] : "")%>" size="5"onkeyup="soloNumeros(this.id);"> A�os
                <input type="text" id="tres_est1" name="tres_est1" value="<%= (vista.equals("0")|| bean_pere == null) ? "" : (tr.length > 1 ? tr[1] : "")%>" size="5"onkeyup="soloNumeros(this.id);"> Meses

            </td>
            <td>
                Telefono residencia<br>
                <input type="text" maxlength="7" id="tel_est" name="tel_est" value="<%= (vista.equals("0") || bean_pere == null) ? "" : bean_pere.getTelefono()%>" size="15" onkeyup="soloNumeros(this.id);">
            </td>
            <td class="filableach">
                Celular<br>
                <input type="text" maxlength="10" id="cel_est" name="cel_est" value="<%= (vista.equals("0") || bean_pere == null) ? "" : bean_pere.getCelular()%>" size="20" onkeyup="soloNumeros(this.id);">
            </td>
        </tr>
        <tr class="filaazul2">
            <td>
                Nombre de la universidad<br>
                <input type="text" id="nom_uni" name="nom_uni" value="<%= (vista.equals("0") || bean_est == null) ? "" : bean_est.getUniversidad()%>" size="30" >
            </td>
            <td>
                Programa<br>
                <input type="text" id="nom_progr" name="nom_progr" value="<%= (vista.equals("0") || bean_est == null) ? "" : bean_est.getPrograma()%>" size="30" >
            </td>
            <td class="filableach">
                Fecha ingreso programa (yyyy-MM-dd)<br>
                <input type="text" id="f_ing_est" class="validate-date" name="f_ing_est" value="<%= (vista.equals("0") || bean_est == null) ? "" : Util.formatoFechaPostgres(bean_est.getFechaIngresoPrograma())%>" size="15">
            </td>
            <td>
                Codigo alumno<br>
                <input type="text" id="cod_est" name="cod_est" value="<%=(vista.equals("0") || bean_est == null) ? "" : bean_est.getCodigo()%>" size="15">
            </td>
            <td>
                Semest. a cursar<br>
                <input type="text" id="sem_est" maxlength="2" name="sem_est" value="<%= (vista.equals("0") || bean_est == null) ? "" : bean_est.getSemestre()%>" size="5" onkeyup="soloNumeros(this.id);" onblur="validarMax(this.id, 10)">
            </td>
            <td class="filableach">
                Valor del semestre<br>
                $ <input type="text" id="val_sem_est" name="val_sem_est" value="<%=(vista.equals("0") || bean_est == null) ? "" : Util.customFormat(Double.parseDouble(bean_est.getValorSemestre()))%>" size="15" onkeyup="soloNumeros(this.id);" onblur="formato(this, 0)">
            </td>
        </tr>
        <tr class="filaazul2">
            <td>
                Tipo de carrera<br>
                <select id="tipo_carrera" name="tipo_carrera" style="width:10em;">
                    <option value="">...</option>
                    <%  dato1 = null;
                                  for (int i = 0; i < tipo_carrera.size(); i++) {
                                      dato1 = ((String) tipo_carrera.get(i)).split(";_;");%>
                    <option value="<%=dato1[0]%>" <%=(dato1[0].equals((vista.equals("0") || bean_est == null) ? " " : bean_est.getTipoCarrera())) ? "selected" : ""%> ><%=dato1[1]%></option>
                    <%  }%>
                </select>
            </td>
            <td>
                Trabaja<br>
                <select id="trabaja" name="trabaja">
                    <option value="">...</option>
                    <option value="S" <%= (vista.equals("0") || bean_est == null) ? "" : bean_est.getTrabaja().equals("S") ? "selected" : ""%>>Si</option>
                    <option value="N" <%= (vista.equals("0") || bean_est == null) ? "" : bean_est.getTrabaja().equals("N") ? "selected" : ""%>>No</option>
                </select>
            </td>
            <td class="filableach">
                Nombre empresa<br>
                <input type="text" id="emp_est" name="emp_est" value="<%= (vista.equals("0") || bean_est == null) ? "" : bean_est.getNombreEmpresa()%>" size="30" >
            </td>
            <td class="filableach" >
                Direccion<br>
                <table width="100%">
                    <tr>
                        <td width="90%">
                            <input type="text" id="dir_emp_est" name="dir_emp_est" <%=estadoDireccion%> value="<%= (vista.equals("0") || bean_est == null) ? "" : bean_est.getDireccionEmpresa()%>" size="30">
                        </td>
                        <td>                         
                            <img src="/fintra/images/Direcciones.png" width="26" height="23" border="0" align="middle" onclick="genDireccion('dir_emp_est',event);" alt="Direcciones"  title="Direcciones" />
                        </td>    
                    </tr>
                </table>  
            </td>
            <td  class="filableach">
                Telefono<br>
                <input type="text" maxlength="7" id="tel_emp_est" name="tel_emp_est" value="<%= (vista.equals("0") || bean_est == null) ? "" : bean_est.getTelefonoEmpresa()%>" size="15" onkeyup="soloNumeros(this.id);">
            </td>
            <td  class="filableach">
                Salario/Mesada/Ingreso mes<br>
                <input type="text" id="sal_est" name="sal_est" value="<%= (vista.equals("0") || bean_est == null) ? "" : Util.customFormat(Double.parseDouble(bean_est.getSalario()))%>" size="15" onkeyup="soloNumeros(this.id);" onblur="formato(this, 0)">
            </td>
        </tr>
    </table>
</div>
<div id="juridica" style="display:none; visibility: hidden;">
    <b>PERSONA JURIDICA</b><br>
    <b>Informaci&oacute;n b&aacute;sica</b>
    <table id="tjuridica" style="border-collapse:collapse; width:100%" border="1">
        <tr class="filaazul2">
            <td colspan="2">
                Razon social<br>
                <input type="text" id="razon_social" name="razon_social" value="<%= (vista.equals("0")) ? "" : bean_pers.getNombre()%>" size="60"  <%= vista.equals("8")?"disabled":""%>>
            </td>
            <td width="17%">
                Nit<br>
                <input type="text" maxlength="11" id="nit" name="nit" value="<%= (vista.equals("0")) ? "" : bean_pers.getIdentificacion()%>" size="25" onkeypress="return isNumberKey(event)" <%= vista.equals("8")?"disabled":""%>>
                <img alt="buscar" src="<%=BASEURL%>/images/botones/iconos/lupa.gif" width="15" height="15" style="cursor:pointer;" onclick="busqueda('nit');">
            </td>
            <td width="17%">
                Ciiu<br>
                <input type="text" id="ciiu" name="ciiu" value="<%= (vista.equals("0")) ? "" : bean_pers.getCiiu()%>" size="25"  <%= vista.equals("8")?"disabled":""%>>
            </td>
            <td width="16%">
                Caracter de la empresa<br>
                <select id="caracter" name="caracter" style="width:10em;"    <%= vista.equals("8")?"disabled":""%>>
                    <option value="">..</option>
                    <%  dato1 = null;
                                for (int i = 0; i < caracter_emp.size(); i++) {
                                    dato1 = ((String) caracter_emp.get(i)).split(";_;");%>
                    <option value="<%=dato1[0]%>" <%=(dato1[0].equals((vista.equals("0")) ? " " : bean_pers.getTipoEmpresa())) ? "selected" : ""%> ><%=dato1[1]%></option>
                    <%  }%>
                </select>
            </td>
            <td width="17%">
                Fecha constitucion (yyyy-MM-dd)<br>
                <input type="text" id="f_const" class="validate-date" name="f_const" value="<%= (vista.equals("0")) ? "" : Util.formatoFechaPostgres(bean_pers.getFechaConstitucion())%>" size="15" >
            </td>
        </tr>
        <tr class="filaazul2">
            <td colspan="2">
                E-mail<br>
                <input type="text" id="mail_jur" name="mail_jur" class="validate-email" value="<%= (vista.equals("0") || bean_pers == null) ? "" : bean_pers.getEmail()%>" size="60" onChange="validaEmail(this);">
            </td>
            <td>
                Telefono 1<br>
                <input type="text" maxlength="10" id="telefono1_jur" name="telefono1_jur" value="<%= (vista.equals("0") || bean_pers == null) ? "" : bean_pers.getTelefono()%>" size="25" onkeyup="soloNumeros(this.id);">
            </td>
            <td class="filableach">
                Telefono 2<br>
                <input type="text" maxlength="10" id="telefono2_jur" name="telefono2_jur" value="<%= (vista.equals("0") || bean_pers == null) ? "" : bean_pers.getTelefono2()%>" size="25" onkeyup="soloNumeros(this.id);">
            </td>
            <td class="filableach">
                Fax<br>
                <input type="text" id="fax" name="fax" value="<%= (vista.equals("0") || bean_pers == null) ? "" : bean_pers.getFax()%>" size="25" onkeyup="soloNumeros(this.id);">
            </td>
            <td>
            </td>
        </tr>
        <tr class="filaazul2">
            <td colspan="2">
                Direccion<br>
                <table width="100%">
                    <tr>
                        <td width="90%">
                            <input type="text" id="dir_jur" name="dir_jur" <%=estadoDireccion%> value="<%= (vista.equals("0") || bean_pers == null) ? "" : bean_pers.getDireccion()%>" size="60">
                        </td>
                        <td>                      
                            <img src="/fintra/images/Direcciones.png" width="26" height="23" border="0" align="middle" onclick="genDireccion('dir_jur',event);" alt="Direcciones"  title="Direcciones" />
                        </td>    
                    </tr>
                </table>  
            </td>
            <td>
                Departamento<br>
                <select id="dep_jur" name="dep_jur" style="width:20em;" onchange="cargarCiudades('dep_jur', 'ciu_jur');">
                    <option value="">...</option>
                    <%  dato1 = null;
                                 for (int j = 0; j < optdep.size(); j++) {
                                     dato1 = ((String) optdep.get(j)).split(";_;");%>
                    <option value="<%=dato1[0]%>" <%=(dato1[0].equals((vista.equals("0") || bean_pers == null) ? " " : bean_pers.getDepartamento())) ? "selected" : ""%> ><%=dato1[1]%></option>
                    <%  }%>
                </select>
            </td>
            <td>
                Ciudad<br>
                <div id="d_ciu_jur">
                    <select id="ciu_jur" name="ciu_jur" style="width:20em;">
                        <option value="">...</option>
                        <%  if (!vista.equals("0") && bean_pers != null) {
                                        optciu = gsaserv.listadoCiudades(bean_pers.getDepartamento());
                                        for (int j = 0; j < optciu.size(); j++) {
                                         dato1 = ((String) optciu.get(j)).split(";_;");%>
                        <option value="<%=dato1[0]%>" <%=(dato1[0].equals(bean_pers.getCiudad())) ? "selected" : ""%> ><%=dato1[1]%></option>
                        <%  }
                                          }%>
                    </select>
                </div>
            </td>

            <td class="filableach">
                Celular<br>
                <input type="text" maxlength="10" id="cel_jur" name="cel_jur" value="<%= (vista.equals("0") || bean_pers == null) ? "" : bean_pers.getCelular()%>" size="25" onkeyup="soloNumeros(this.id);">
            </td>
            <td>
            </td>

        </tr>
        <tr class="filaazul2">

            <td colspan="2">
                Representante legal &nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp
                &nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp Genero<br>
                <input type="text" id="rep_legal" name="rep_legal" value="<%= (vista.equals("0")) ? "" : bean_pers.getRepresentanteLegal()%>" size="50">
                <select id="gen_repr" name="gen_repr">
                    <option value="">...</option>
                    <option value="M" <%= ((vista.equals("0")) ? "" : bean_pers.getGeneroRepresentante().equals("M") ? "selected" : "")%>>Masculino</option>
                    <option value="F" <%= ((vista.equals("0")) ? "" : bean_pers.getGeneroRepresentante().equals("F") ? "selected" : "")%>>Femenino</option>
                </select>
            </td>
            <td width="17%">
                Identificacion<br>
                <select id="tipo_id_repr" name="tipo_id_repr">
                    <option value="">...</option>
                    <%  dato1 = null;
                                for (int i = 0; i < tipo_id.size(); i++) {
                                    dato1 = ((String) tipo_id.get(i)).split(";_;");%>
                    <option value="<%=dato1[0]%>" <%=(dato1[0].equals((vista.equals("0")|| bean_pers == null) ? " " : bean_pers.getTipoIdRepresentante())) ? "selected" : ""%> ><%=dato1[1]%></option>
                    <%  }%>
                </select>
                    <input type="text" maxlength="15" id="id_repr" name="id_repr" value="<%= (vista.equals("0")) ? "" : bean_pers.getIdRepresentante()%>" size="20" onkeyup="soloNumeros(this.id);">
            </td>
            <td colspan="2" >
                Nombre de la persona autorizada para firmar cheques  &nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp Genero<br>
                <input type="text" id="autorizado" name="autorizado" value="<%=(vista.equals("0")) ? "" : bean_pers.getFirmadorCheques()%>" size="50">
                <select id="gen_autor" name="gen_autor">
                    <option value="">...</option>
                    <option value="M" <%= ((vista.equals("0")) ? "" : bean_pers.getGeneroFirmador().equals("M") ? "selected" : "")%>>Masculino</option>
                    <option value="F" <%= ((vista.equals("0")) ? "" : bean_pers.getGeneroFirmador().equals("F") ? "selected" : "")%>>Femenino</option>
                </select>
            </td>
            <td width="17%">
                Identificacion<br>
                <select id="tipo_id_autor" name="tipo_id_autor">
                    <option value="">...</option>
                    <%  dato1 = null;
                                for (int i = 0; i < tipo_id.size(); i++) {
                                    dato1 = ((String) tipo_id.get(i)).split(";_;");%>
                    <option value="<%=dato1[0]%>" <%=(dato1[0].equals((vista.equals("0")|| bean_pers == null) ? " " : bean_pers.getTipoIdFirmador())) ? "selected" : ""%> ><%=dato1[1]%></option>
                    <%  }%>
                </select>
                    <input type="text" maxlength="15" id="id_autor" name="id_autor" value="<%= (vista.equals("0")) ? "" : bean_pers.getIdFirmador()%>" size="20" onkeyup="soloNumeros(this.id);">
            </td>
        </tr>
    </table>
    <br>
    <%
                try {
                    lista_referencias = gsaserv.buscarReferencias(num_solicitud, "S", "C");
                } catch (Exception ec) {
                    System.out.println("error: " + ec.toString());
                    ec.printStackTrace();
                }
    %>
    <b>Referencias comerciales(M&iacute;nimo una referencia)</b>
    <table id="trefjuridica" style="border-collapse:collapse; width:100%" border="1">
        <%
                    for (int i = 1; i < 3; i++) {

                        b_ref = null;
                        if (lista_referencias != null) {
                            try {
                                if (lista_referencias.size() >= i) {
                                    b_ref = lista_referencias.get(i - 1);
                                }
                            } catch (Exception exc) {
                                System.out.println("errror2: " + exc.toString());
                            }

        %>
        <tr class="filaazul2">
            <td width="17%">
                Nombre o razon social<br>
                <input type="text" id="raz_soc_ref<%= i%>" name="raz_soc_ref<%= i%>" value="<%=(vista.equals("0") || b_ref == null) ? "" : b_ref.getNombre()%>" size="30" >
            </td>
            <td class="filableach" width="16%">
                Direccion<br>
                <table width="100%">
                    <tr>
                        <td width="90%">
                            <input type="text" id="raz_soc_dir<%= i%>" name="raz_soc_dir<%= i%>" <%=estadoDireccion%> value="<%=(vista.equals("0") || b_ref == null) ? "" : b_ref.getDireccion()%>" size="30">
                        </td>
                        <td>                      
                            <img src="/fintra/images/Direcciones.png" width="26" height="23" border="0" align="middle" onclick="genDireccion('raz_soc_dir<%= i%>',event);" alt="Direcciones"  title="Direcciones" />
                        </td>    
                    </tr>
                </table> 
            </td>
            <td width="17%">
                Departamento<br>
                <select id="dep_raz_soc_<%= i%>" name="dep_raz_soc_<%= i%>" style="width:20em;" onchange="cargarCiudades('dep_raz_soc_<%= i%>', 'ciu_raz_soc_<%= i%>');">
                    <option value="">...</option>
                    <%  dato1 = null;
                                                 for (int j = 0; j < optdep.size(); j++) {
                                                     dato1 = ((String) optdep.get(j)).split(";_;");%>
                    <option value="<%=dato1[0]%>" <%=(dato1[0].equals((vista.equals("0") || b_ref == null) ? " " : b_ref.getDepartamento())) ? "selected" : ""%> ><%=dato1[1]%></option>
                    <%  }%>
                </select>
            </td>
            <td width="17%">
                Ciudad<br>
                <div id="d_ciu_raz_soc_<%= i%>">
                    <select id="ciu_raz_soc_<%= i%>" name="ciu_raz_soc_<%= i%>" style="width:20em;">
                        <option value="">...</option>
                        <%  if (!vista.equals("0") && b_ref != null) {
                                                        optciu = gsaserv.listadoCiudades(b_ref.getDepartamento());
                                                        for (int j = 0; j < optciu.size(); j++) {
                                                        dato1 = ((String) optciu.get(j)).split(";_;");%>
                        <option value="<%=dato1[0]%>" <%=(dato1[0].equals(b_ref.getCiudad())) ? "selected" : ""%> ><%=dato1[1]%></option>
                        <%  }
                                                          }%>
                    </select>
                </div>
            </td>

            <td width="16%">
                Telefono 1<br>
                <input type="text" maxlength="10" id="raz_soc_tel1<%= i%>" name="raz_soc_tel1<%= i%>" value="<%=(vista.equals("0") || b_ref == null) ? "" : b_ref.getTelefono()%>" size="15" onkeyup="soloNumeros(this.id);">
            </td>
            <td class="filableach" width="17%">
                Telefono 2<br>
                <input type="text" maxlength="10" id="raz_soc_tel2<%= i%>" name="raz_soc_tel2<%= i%>" value="<%=(vista.equals("0") || b_ref == null) ? "" : b_ref.getTelefono2()%>" size="15" onkeyup="soloNumeros(this.id);">
            </td>
        </tr>
        <%

                        }

                    }
        %>

    </table>
</div>
<div id="negocio1" style="display:none; visibility: hidden;">
    <table id="tnegocio" style="border-collapse:collapse; width:100%" border="1">
        <tr class="filaazul2">
            <td width="15%">
                Nombre<br>
                <input type="text" id="nombre_neg" name="nombre_neg" value="<%= (bean_neg==null) ? "" : bean_neg.getNombre()%>" size="60" >
            </td>
            <td width="17%">
                Tipo Actividad<br>
                <div id="id_tipo_div">
                    <select id="id_tipo_actividad" name="id_tipo_actividad" style="width:15em;">
                        <option value="">...</option>
                        <option value="P" <%= bean_neg != null && bean_neg.getTipoActividad().equals("P") ? "selected" : "" %>>PRODUCCI&Oacute;N</option>
                        <option value="C" <%= bean_neg != null && bean_neg.getTipoActividad().equals("C") ? "selected" : "" %>>COMERCIO</option>
                        <option value="S" <%= bean_neg != null && bean_neg.getTipoActividad().equals("S") ? "selected" : "" %>>SERVICIO</option>
                    </select>
                
                </div>
            </td>
            <td width="17%">
                �Tiene Socios?<br>
                <div id="id_tipo_div">
                    <select id="id_tienesocios" name="id_tienesocios" required onchange="tieneSocio(event)">
<!--                        // <option value="">...</option>-->
                        <option value="N" <%= bean_neg != null && bean_neg.getTieneSocios().equals("N") ? "selected" : "" %>>NO</option>
                        <option value="S" <%= bean_neg != null && bean_neg.getTieneSocios().equals("S") ? "selected" : "" %>>SI</option>
                        
                    </select>
                
                </div>
            </td>
            <td >
                % De participaci&oacute;n<br>
                <input type="text" id="id_parcitipacion" size="3" name="id_parcitipacion" <%= bean_neg == null ? "disabled" : ""%> required value="<%= bean_neg == null ? "" : bean_neg.getPorcentajeParticipacion()%>">
            </td>
            <td>
                C�dula socio<br>
                <input type="text" onkeypress="return isNumberKey(event)" id="cedula_socio" name="cedula_socio" maxlength="15" <%= bean_neg == null ? "disabled" : ""%> required value="<%= bean_neg == null ? "" : bean_neg.getCedulaSocio()%>">
            </td>
            <td>
                Nombre del socio<br>
                <input type="text" id="id_nomb_socio" name="id_nomb_socio" size="20" <%= bean_neg == null ? "disabled" : ""%> required value="<%= bean_neg == null ? "" : bean_neg.getNombreSocio()%>">
            </td>            
        </tr>
        <tr class="filaazul2">
        <td width="33%">
                Direccion Socio<br>
                <table width="100%">
                    <tr>
                        <td width="90%">
                            <input type="text" id="dir_neg_socio" name="dir_neg_socio" <%=estadoDireccion%> value="<%= bean_neg == null ? "" : bean_neg.getDireccionSocio()%>" size="60">
                        </td>
                        <td>                         
                            <img id="img_dir_socio" src="/fintra/images/Direcciones.png" width="26" height="23" border="0" align="middle" onclick="genDireccion('dir_neg_socio',event);" alt="Direcciones"  title="Direcciones" disabled/>
                        </td>    
                    </tr>
                </table>   
            </td>           
            <td>
                Telefono socio<br>
                <input type="text" id="id_tel_socio" name="id_tel_socio" <%= bean_neg == null ? "disabled" : ""%> required value="<%= bean_neg == null ? "" : bean_neg.getTelefonoSocio()%>">
            </td>
            <td colspan="2">
                �Posee camara comercio?<br>
                <div id="id_cam_comercio_div">
                    <select id="id_cam_comercio" name="camara_comercio" style="width:10em;">
                        <option value="">...</option>
                        <option value="S" <%= bean_neg != null && bean_neg.getTieneCamaraComercio().equals("S") ? "selected" : "" %>>SI</option>
                        <option value="N" <%= bean_neg != null && bean_neg.getTieneCamaraComercio().equals("N") ? "selected" : "" %>>NO</option>
                    </select>   
                
                </div>
            </td>
            <td>
                Local<br>
                <div id="id_cam_comercio_div">
                    <select id="id_local" name="id_local" style="width:10em;">
                        <option value="">...</option>
                        <option value="P" <%= bean_neg != null && bean_neg.getLocal().equals("P") ? "selected" : "" %>>PROPIO</option>
                        <option value="A" <%= bean_neg != null && bean_neg.getLocal().equals("A") ? "selected" : "" %>>ARRENDADO</option>
                        <option value="O" <%= bean_neg != null && bean_neg.getLocal().equals("O") ? "selected" : "" %>>OTRO</option>
                    </select>   
                
                </div>
            </td>
            <td>
                Tipo Negocio<br>
                <div id="id_cam_comercio_div">
                    <select id="id_tipo_negocio" name="id_tipo_negocio" style="width:15em;">
                        <option value="">...</option>
                        <option value="C" <%= bean_neg != null && bean_neg.getTipoNegocio().equals("C") ? "selected" : "" %>>EN CASA</option>
                        <option value="A" <%= bean_neg != null && bean_neg.getTipoNegocio().equals("A") ? "selected" : "" %>>AMBULANTE</option>
                        <option value="E" <%= bean_neg != null && bean_neg.getTipoNegocio().equals("E") ? "selected" : "" %>>ESTACIONARIO</option>
                        <option value="L" <%= bean_neg != null && bean_neg.getTipoNegocio().equals("L") ? "selected" : "" %>>LOCAL</option>
                    </select>   
                
                </div>
            </td>
            
                 
        </tr>
        
        <tr class="filaazul2">
            <td width="33%">
                Direccion<br>
                <table width="100%">
                    <tr>
                        <td width="90%">
                            <input type="text" id="dir_neg" name="dir_neg" <%=estadoDireccion%> value="<%= (bean_neg == null) ? "" : bean_neg.getDireccion()%>" size="60" onclick="completarDatosNegocio('dir_emp_nat')">
                        </td>
                        <td>                         
                            <img src="/fintra/images/Direcciones.png" width="26" height="23" border="0" align="middle" onclick="genDireccion('dir_neg',event);" alt="Direcciones"  title="Direcciones" />
                        </td>    
                    </tr>
                </table>   
            </td>
            <td width="17%">
                Departamento<br>
                <select id="dep_neg" name="dep_neg" style="width:20em;" onchange="cargarCiudadesBarrio('dep_neg', 'ciu_neg', 'barrio_neg');">
                    <option value="">...</option>
                    <%  dato1 = null;
                                for (int j = 0; j < optdep.size(); j++) {
                                    dato1 = ((String) optdep.get(j)).split(";_;");%>
                    <option value="<%=dato1[0]%>" <%=(dato1[0].equals((bean_neg== null) ? " " : bean_neg.getDepartamento())) ? "selected" : ""%> ><%=dato1[1]%></option>
                    <%  }%>
                </select>
            </td>
            <td width="17%" colspan="2">
                Ciudad<br>
                <div id="d_ciu_neg">
                    <select id="ciu_neg" name="ciu_neg" style="width:20em;" onchange="cargarBarrios('ciu_neg', 'barrio_neg');">
                        <option value="">...</option>
                        <%  if (bean_neg != null) {
                                        optciu = gsaserv.listadoCiudades(bean_neg.getDepartamento());
                                        for (int j = 0; j < optciu.size(); j++) {
                                            dato1 = ((String) optciu.get(j)).split(";_;");%>
                        <option value="<%=dato1[0]%>" <%=(dato1[0].equals(bean_neg.getCiudad())) ? "selected" : ""%> ><%=dato1[1]%></option>
                        <%  }
                                    }%>
                    </select>
                </div>
            </td>
            <td>
                Barrio<br>
            <%  if(tipoconv.equals("Microcredito")){%>
                    <div id="d_barrio_neg">
                        <select id="barrio_neg" name="barrio_neg" style="width:20em;">
                            <option value="">...</option>
                            <%  if (bean_neg != null) {
                                            optbarrio = gsaserv.listadoBarrios(bean_neg.getCiudad());
                                            for (int j = 0; j < optbarrio.size(); j++) {
                                                dato1 = ((String) optbarrio.get(j)).split(";_;");%>
                            <option value="<%=dato1[0]%>" <%=(dato1[0].equals(bean_neg.getBarrio())) ? "selected" : ""%> ><%=dato1[1]%></option>
                            <%  }
                                        }%>
                        </select>
                    </div>
            <%  }else{%>
                   <input type="text" id="barrio_neg" name="barrio_neg" value="<%= (bean_neg == null) ? "" : bean_neg.getBarrio()%>" size="25" >
            <%  }%>

            </td>
            <td width="16%">
                Telefono<br>
                <input type="text" maxlength="10" id="telefono_neg" name="telefono_neg" value="<%= (bean_neg == null) ? "" : bean_neg.getTelefono()%>" size="25" onkeyup="soloNumeros(this.id);">
            </td>
        </tr>
        <tr class="filaazul2">
        <td>
            Activos<br>
            $ <input type="text" id="id_activos" name="activos" maxlength="12"  size="25" onkeyup="soloNumeros(this.id);" onblur="formato(this, 0);" value="<%= (vista.equals("0") || bean_neg == null) ? "" : Util.customFormat(Double.parseDouble(bean_neg.getActivos()))%>"> 
        </td>
        <td colspan="12">
            Pasivos<br>
            $ <input type="text" id="id_pasivos" name="pasivos" maxlength="12"  size="25" onkeyup="soloNumeros(this.id);" onblur="formato(this, 0);" value="<%= (vista.equals("0") || bean_neg == null) ? "" : Util.customFormat(Double.parseDouble(bean_neg.getPasivos()))%>"> 
        </td>
        </tr>
    </table>
</div>
<div id="negocio2" style="display:none; visibility: hidden;">
    <table id="tnegocio" style="border-collapse:collapse; width:100%" border="1">
        <tr class="filaazul2">
            <td width="33%" >
                Sector<br>
                <select name="sector" id="sector" style="width: 100%;" onchange="buscarSubs();">
                    <option value="">...</option>
                    <%
                                if (sectores != null) {
                                    String[] split = null;
                                    for (int i = 0; i < sectores.size(); i++) {
                                        split = ((String) (sectores.get(i))).split(";_;");
                                        out.print("<option value='" + split[0] + "'" + ((bean_neg != null&&bean_neg.getSector()!=null ? bean_neg.getSector() : "").equals(split[0]) ? "selected='selected'" : "") + ">" + split[1] + "</option>");
                                    }
                                }
                    %>
                </select>
            </td>
            <td width="34%" >
                Subsector<br>
                <div id="subsectores">
                    <select name="subsector" id="subsector" style="width: 100%;">
                        <% if (bean_neg != null&&bean_neg.getSector()!=null) {
                                        model.sectorsubsectorSvc.buscar_subsector(bean_neg.getSector(), bean_neg.getSubsector(), usuario.getBd());
                        %>
                        <option  value="<%=bean_neg.getSubsector()%>"><%=model.sectorsubsectorSvc.getNombre_sector()%></option>
                        <%} else {%>
                        <option value="">...</option>
                        <%}%>
                    </select>
                </div>
            </td>

            <td>
                A&ntilde;os local actual<br>
                <input type="text" id="tiempo_local" name="tiempo_local" value="<%= (bean_neg == null) ? "" : bean_neg.getTiempoLocal()%>" onkeyup="soloNumeros(this.id);" size="3" onblur="validarMax(this.id, 50)">
            </td>
            <td>
                A&ntilde;os exp. negocio actual<br>
                <input type="text" id="num_exp_negocio" name="num_exp_negocio" value="<%= (bean_neg == null) ? "" : bean_neg.getNumExpNeg()%>" onkeyup="soloNumeros(this.id);"   size="3" onblur="validarMax(this.id, 50)">
            </td>
            <td>
                A&ntilde;os microempresario<br>
                <input type="text" id="tiempo_microempresario" name="tiempo_microempresario" value="<%=(bean_neg == null) ? "" : bean_neg.getTiempoMicroempresario()%>" onkeyup="soloNumeros(this.id);" size="3" onblur="validarMax(this.id, 50)">
            </td>

            <td>
                N&ordm; trabajadores<br>
                <input type="text" id="num_trabajadores" name="num_trabajadores" value="<%=(bean_neg == null) ? "" : bean_neg.getNumTrabajadores()%>" onkeyup="soloNumeros(this.id);" onblur="validarMax(this.id, 10)" size="3">
            </td>
        </tr>

    </table>
</div>




<div id="cuentas" style="display:none; visibility: hidden;">
    <b>CUENTAS</b><br>
    <table id="tcuentas" style="border-collapse:collapse; width:100%" border="1">
        <%
                    for (int i = 1; i < 4; i++) {
                        SolicitudCuentas bgenx = null;
                        if (lista_cuentas != null) {
                            try {
                                if (lista_cuentas.size() >= i) {
                                    bgenx = lista_cuentas.get(i - 1);
                                } else {
                                    bgenx = null;
                                }

                            } catch (Exception exc) {
                                System.out.println("errror1: " + exc.toString());
                            }

                        }

        %>
        <tr class="<%=(i>1)?"filableach":"filaazul2"%>">
            <td width="17%">
                Tipo de cuenta<br>
                <select id="tipo_cuenta<%= i%>" name="tipo_cuenta<%= i%>" style="width:20em;" <%= vista.equals("8")?"disabled":""%> >
                    <option value="">...</option>
                    <%  dato1 = null;
                                             for (int j = 0; j < tipo_cuenta.size(); j++) {
                                                 dato1 = ((String) tipo_cuenta.get(j)).split(";_;");%>
                    <option value="<%=dato1[0]%>" <%=(dato1[0].equals((vista.equals("0") || bgenx == null) ? " " : bgenx.getTipo())) ? "selected" : ""%> ><%=dato1[1]%></option>
                    <%  }%>
                </select>
            </td>
            <td  width="16%">
                Banco<br> <select id="banco<%= i%>" name="banco<%= i%>" style="width:20em;" <%= vista.equals("8")?"disabled":""%>>
                    <%
                                            if (listacad != null && listacad.size() > 0) {
                    %>
                    <option value="" selected>...</option>
                    <%
                                                                    String[] splitcad = null;
                                                                    for (int k = 0; k < listacad.size(); k++) {
                                                                        splitcad = (listacad.get(k)).split(";_;");
                                                                        String v12 = (vista.equals("0") || bgenx == null) ? "" : bgenx.getBanco() != null ? bgenx.getBanco() : "";
                    %>
                    <option value="<%= splitcad[0]%>" <%= (v12.equals(splitcad[0]) ? "selected" : "")%>><%= splitcad[1]%></option>
                    <%
                                                                    }
                                                                } else {
                    %>
                    <option value="" selected>...</option>
                    <%                                                        }
                    %>
                </select>
            </td>
            <td width="17%">
                Numero de cuenta<br>
                <input type="text" id="cuenta<%= i%>" name="cuenta<%= i%>" value="<%=(vista.equals("0") || bgenx == null) ? "" : bgenx.getCuenta()%>" size="30" onkeyup="soloNumeros(this.id);" <%= vista.equals("8")?"readonly":""%>>
            </td>
            <td width="17%" class="filableach">
                Fecha apertura (yyyy-MM-dd)<br>
                <input type="text"  id="fecha_apertura<%= i%>" class="validate-date" name="fecha_apertura<%= i%>" value="<%=(vista.equals("0") || bgenx == null) ? "" : bgenx.getFechaApertura().substring(0, 10)%>" value="" size="15" <%= vista.equals("8")?"readonly":""%>>
            </td>
            <td width="16%" class="filableach">
                Numero de tarjeta<br>
                <input type="text" id="num_tarjeta<%= i%>" name="num_tarjeta<%= i%>" value="<%=(vista.equals("0") || bgenx == null) ? "" : bgenx.getNumeroTarjeta()%>" size="30" onkeyup="soloNumeros(this.id);"<%= vista.equals("8")?"readonly":""%> >
            </td>
            <td></td>
        </tr>
        <%


                    }
        %>
    </table>
    <div id="codeudor2" style="display:none; visibility: hidden;">
            <table id="tcodeudor2" style="border-collapse:collapse; width:100%" border="1">
                <tr class="filaazul2">
                    <td>
                        Razon social<br>
                        <input type="text" id="nombre_cod_jur" name="nombre_cod_jur" value="<%= (vista.equals("0") || bean_perc == null) ? "" : bean_perc.getNombre()%>" size="60"  <%= vista.equals("8")?"disabled":""%>>
                        <input type="text" id="seg_apellido_cod_jur" name="seg_apellido_cod_jur" value="" style="display: none;" >
                        <input type="text" id="pr_nombre_cod_jur" name="pr_nombre_cod_jur" value="" style="display: none;">
                        <input type="text" id="seg_nombre_cod_jur" name="seg_nombre_cod_jur" value="" style="display: none;">
                        <select id="genero_cod_jur" name="genero_cod_jur" style="display: none;"><option value="">...</option></select>
                        <select id="est_civil_cod_jur" name="est_civil_cod_jur"  style="display: none;"><option value="">...</option></select>
                    </td>
                    <td width="17%">
                        Nit<br>
                        <input type="text" maxlength="11" id="id_cod_jur" name="id_cod_jur" value="<%= (vista.equals("0") || bean_perc == null) ? "" : bean_perc.getIdentificacion()%>" size="25" onkeypress="return isNumberKey(event)" <%= vista.equals("8")?"disabled":""%>>
                        <img alt="buscar" src="<%=BASEURL%>/images/botones/iconos/lupa.gif" width="15" height="15" style="cursor:pointer;" onclick="busqueda('id_cod_jur');">
                        <select id="tipo_id_cod_jur" name="tipo_id_cod_jur" style="visibility: hidden;"> <option value="NIT" selected>NIT</option> </select>
                    </td>
                   
                    <td width="16%">
                        Caracter de la empresa<br>
                        <select id="caracter_cod_jur" name="caracter_cod_jur" style="width:10em;"    <%= vista.equals("8")?"disabled":""%>>
                            <option value="">..</option>
                            <%  dato1 = null;
                                        for (int i = 0; i < caracter_emp.size(); i++) {
                                            dato1 = ((String) caracter_emp.get(i)).split(";_;");%>
                            <option value="<%=dato1[0]%>" <%=(dato1[0].equals((vista.equals("0") || bean_perc == null) ? " " : bean_perc.getTipoEmpresa())) ? "selected" : ""%> ><%=dato1[1]%></option>
                            <%  }%>
                        </select>
                    </td>
                    <td width="17%">
                        Fecha constitucion (yyyy-MM-dd)<br>
                        <input type="text" id="f_exp_cod_jur" class="validate-date" name="f_exp_cod_jur" value="<%= (vista.equals("0") || bean_perc == null) ? "" : Util.formatoFechaPostgres(bean_perc.getFechaConstitucion())%>" size="15" >
                    </td>
                       

                </tr>
                <tr class="filaazul2">
                    <td>
                        E-mail<br>
                        <input type="text" id="mail_cod_jur" name="mail_cod_jur" class="validate-email" value="<%= (vista.equals("0") || bean_perc == null) ? "" : bean_perc.getEmail()%>" size="60" onChange="validaEmail(this);">
                    </td>
                    <td>
                        Telefono<br>
                        <input type="text" maxlength="10" id="tel_cod_jur" name="tel_cod_jur" value="<%= (vista.equals("0") || bean_perc == null) ? "" : bean_perc.getTelefono()%>" size="25" onkeyup="soloNumeros(this.id);">
                    </td>
                    <td class="filableach">
                        Celular<br>
                        <input type="text" maxlength="10" id="cel_cod_jur" name="cel_cod_jur" value="<%= (vista.equals("0") || bean_perc == null) ? "" : bean_perc.getCelular()%>" size="25" onkeyup="soloNumeros(this.id);">
                    </td>
                    <td>
                    </td>

                </tr>
                <tr class="filaazul2">
                    <td>
                        Direccion<br>
                        <table width="100%">
                            <tr>
                                <td width="90%">
                                    <input type="text" id="dir_cod_jur" name="dir_cod_jur" <%=estadoDireccion%> value="<%= (vista.equals("0") || bean_perc == null) ? "" : bean_perc.getDireccion()%>" size="60">
                                </td>
                                <td>                         
                                    <img src="/fintra/images/Direcciones.png" width="26" height="23" border="0" align="middle" onclick="genDireccion('dir_cod_jur',event);" alt="Direcciones"  title="Direcciones" />
                                </td>    
                            </tr>
                        </table>  
                    </td>
                    
                    <td>
                        Departamento<br>
                        <select id="dep_cod_jur" name="dep_cod_jur" style="width:20em;" onchange="cargarCiudadesBarrio('dep_cod_jur', 'ciu_cod_jur','barrio_cod_jur');">
                            <option value="">...</option>
                            <%  dato1 = null;
                                         for (int j = 0; j < optdep.size(); j++) {
                                             dato1 = ((String) optdep.get(j)).split(";_;");%>
                            <option value="<%=dato1[0]%>" <%=(dato1[0].equals((vista.equals("0") || bean_perc == null) ? " " : bean_perc.getDepartamento())) ? "selected" : ""%> ><%=dato1[1]%></option>
                            <%  }%>
                        </select>
                    </td>
                    <td>
                        Ciudad<br>
                        <div id="d_ciu_cod_jur">
                            <select id="ciu_cod_jur" name="ciu_cod_jur" style="width:20em;" onchange="cargarBarrios('ciu_cod_jur', 'barrio_cod_jur');">
                                <option value="">...</option>
                                <%  if (!vista.equals("0") && bean_perc != null) {
                                                optciu = gsaserv.listadoCiudades(bean_perc.getDepartamento());
                                                for (int j = 0; j < optciu.size(); j++) {
                                                 dato1 = ((String) optciu.get(j)).split(";_;");%>
                                <option value="<%=dato1[0]%>" <%=(dato1[0].equals(bean_perc.getCiudad())) ? "selected" : ""%> ><%=dato1[1]%></option>
                                <%  }
                                                  }%>
                            </select>
                        </div>
                    </td>

                    <td class="filableach">
                        Barrio<br>
                    <%  if(tipoconv.equals("Microcredito")){%>
                            <div id="d_barrio_cod_jur">
                                <select id="barrio_cod_jur" name="barrio_cod_jur" style="width:20em;">
                                    <option value="">...</option>
                                    <%  if (!vista.equals("0") && bean_perc != null) {
                                                    optbarrio = gsaserv.listadoBarrios(bean_perc.getCiudad());
                                                    for (int j = 0; j < optbarrio.size(); j++) {
                                                     dato1 = ((String) optbarrio.get(j)).split(";_;");%>
                                    <option value="<%=dato1[0]%>" <%=(dato1[0].equals(bean_perc.getBarrio())) ? "selected" : ""%> ><%=dato1[1]%></option>
                                    <%  }
                                                      }%>
                                </select>
                            </div>
                    <%  }else{%>
                         <input type="text" maxlength="10" id="barrio_cod_jur" name="barrio_cod_jur" value="<%= (vista.equals("0") || bean_perc == null) ? "" : bean_perc.getBarrio()%>" size="25">
                    <%  }%>
                    </td>

                    
                </tr>
                <tr class="filaazul2">

                    <td>
                        Representante legal &nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp
                        &nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp Genero<br>
                        <input type="text" id="rep_legal_cod_jur" name="rep_legal_cod_jur" value="<%= (vista.equals("0") || bean_perc == null) ? "" : bean_perc.getRepresentanteLegal()%>" size="50">
                        <select id="gen_repr_cod_jur" name="gen_repr_cod_jur">
                            <option value="">...</option>
                            <option value="M" <%= ((vista.equals("0") || bean_perc == null) ? "" : bean_perc.getGeneroRepresentante().equals("M") ? "selected" : "")%>>Masculino</option>
                            <option value="F" <%= ((vista.equals("0") || bean_perc == null) ? "" : bean_perc.getGeneroRepresentante().equals("F") ? "selected" : "")%>>Femenino</option>
                        </select>
                    </td>
                    <td width="17%">
                        Identificacion<br>
                        <select id="tipo_id_repr_cod_jur" name="tipo_id_repr_cod_jur">
                            <option value="">...</option>
                            <%  dato1 = null;
                                        for (int i = 0; i < tipo_id.size(); i++) {
                                            dato1 = ((String) tipo_id.get(i)).split(";_;");%>
                            <option value="<%=dato1[0]%>" <%=(dato1[0].equals((vista.equals("0")|| bean_perc == null) ? " " : bean_perc.getTipoIdRepresentante())) ? "selected" : ""%> ><%=dato1[1]%></option>
                            <%  }%>
                        </select>
                            <input type="text" maxlength="15" id="id_repr_cod_jur" name="id_repr_cod_jur" value="<%= (vista.equals("0") || bean_perc == null) ? "" : bean_perc.getIdRepresentante()%>" size="20" onkeyup="soloNumeros(this.id);">
                    </td>
                    <td colspan="2" ></td>
                </tr>
            </table>
    </div>
    <br>
</div>

                            
<div id="obligaciones_micro" style="display:none; visibility: hidden;">
    <b>COMPRA CARTERA</b>&nbsp <input type="checkbox" id="compra_cartera" name="compra_cartera" onclick="bloquearDiv()" <%=((vista.equals("0")|| obligacionesList.size() == 0 ) && cp.equals("N")) ? "" : "checked"%>/>
    <div id='contenido_cc'>
    <table id="tfinanat" style="border-collapse:collapse; width:100%" border="1">
        <tbody>
        <%  JsonArray jsonarray=gsaserv.arrayEntidadesCartera(usuario);
            int io = 1;
            for (int i = 1; i <= 20; i++) { 

                ObligacionesCompra bgenx = null;
                if (obligacionesList != null) {
                    try {
                        if (obligacionesList.size() >= i) {
                            bgenx = obligacionesList.get(i - 1);
                            io = i;
                        } else {
                            bgenx = null;
                        }

                    } catch (Exception exc) {
                        System.out.println("errror1: " + exc.toString());
                    }

                }
            %>

            <tr class="filaazul2" id="obligacion_<%=i%>" style="display:<%=(bgenx == null && i > 1)?"none":""%>">
                <td width="33%" >
                    Entidad<br>
                    <select id="entidad<%=i%>" name="form.solicitante.obligaciones[<%=i%>].entidad" style="width:20em;" onchange="buscarInformacionEntidad(<%=i%>)"  >
                        <option value="">seleccionar</option>
                        <% for(int j=0 ; j< jsonarray.size();j++) {
                           JsonObject  objeto = jsonarray.get(j).getAsJsonObject(); %>
                           <option value="<%= objeto.get("nit_entidad").getAsString()%>"  <%=(objeto.get("nit_entidad").getAsString().equals((vista.equals("0") || bgenx == null) ? " " : bgenx.getNit_proveedor())) ? "selected" : ""%> >
                               <%= objeto.get("nombre_entidad").getAsString()%>
                           </option>
                       <%} %>

                    </select>                                

                  
                </td>
                <td>
                    Nit<br>
                    <input type="text" id="nit_prov<%=i%>" name="form.solicitante.obligaciones[<%=i%>].nit_prov" value="<%=(vista.equals("0") || bgenx == null) ? "" : bgenx.getNit_proveedor()%>" readonly>
                </td>

                <td>
                    Tipo de Cuenta<br>
                    <input type="text" id="tcuenta_prov<%=i%>" name="form.solicitante.obligaciones[<%=i%>].tcuenta_prov" value="<%=(vista.equals("0") || bgenx == null) ? "" : bgenx.getTipo_cuenta()%>" readonly>
                </td>
                <td>
                    No de Cuenta<br>
                    <input type="text" id="cuenta_prov<%=i%>" name="form.solicitante.obligaciones[<%=i%>].cuenta_prov" value="<%=(vista.equals("0") || bgenx == null) ? "" : bgenx.getNumero_cuenta()%>" size="25" readonly>
                </td>
                <td>
                    Valor a Recoger<br>
                    <table width="100%">
                        <tr>
                            <td width="90%">
                                <input type="text" id="valor_recoger<%=i%>" name='form.solicitante.obligaciones[<%=i%>].valor_recoger' 
                                       value="<%=(vista.equals("0") || bgenx == null) ? "" : Util.customFormat(bgenx.getValor_comprar()) %>"
                                       onkeyup="soloNumeros(this.id);" onblur="formato(this, 0)" maxlength="10" style="width:90%;">
                            </td>
                            <td>
                                <img src="/fintra/images/close.png" width="16" height="16" border="0" align="middle" 
                                     onclick="addObligs(false, <%=i%>, event);" alt="Eliminar"  title="Eliminar" />
                            </td>    
                        </tr>
                    </table>
                </td>
            </tr><%

        } %>
        </tbody>
        <input type="hidden" id="max_obligaciones" name="max_obligaciones" value="<%=io%>">
        <button id="btn_new_oblig" class="botonHeader" href="#" onclick="addObligs(true, 0,event);">nueva</button>
    </table>
    <br> 
    </div>

</div>


