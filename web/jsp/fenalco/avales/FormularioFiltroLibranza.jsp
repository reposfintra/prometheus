<%-- 
    Document   : FormularioFiltroLibranza
    Created on : 26-mar-2016, 10:31:15
    Author     : root
--%>

<%@page contentType="text/html" pageEncoding="ISO-8859-9"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-9">
        <title>Filtro de Creditos</title>

        <link type="text/css" rel="stylesheet" href="/fintra/css/jquery/jquery-ui/jquery-ui.css" />
        <link rel="stylesheet" type="text/css" href="/fintra/css/jCal/jscal2.css" />

        <script type="text/javascript" src="/fintra/js/jquery/jquery-ui/jquery.min.js"></script>
        <script type="text/javascript" src="/fintra/js/jquery/jquery-ui/jquery.ui.min.js"></script>
        <script type="text/javascript" src="/fintra/js/FormularioFiltroLibranza.js"></script>
        <script type="text/javascript" src="/fintra/js/jCal/jscal2.js"></script>
        <script type="text/javascript" src="/fintra/js/jCal/lang/es.js"></script>
    </head>
    <style>
        .ui-jqgrid input, .alicuotas td {
            padding: 0.2em 0.5em;
        }
        .alicuotas{
            border: 1em; 
            border-collapse:collapse; 
            width: 100%; 
            margin-top: 0.2em; 
            text-align: right;
        }
        .alicuotas th {
            text-align: center;
        }
    </style>
    <body>
        <jsp:include page="/toptsp.jsp?encabezado=Filtro de creditos"/>
        <div id="capaCentral" style="text-align: center;">
            <center>
                <table style="width: 100%">
                    <tr>
                        <td>
                            <div class="ui-jqgrid ui-widget ui-widget-content ui-corner-all"
                                 style="width: 550px; padding: 0.5em 2em; margin: 0.2em; float: right">
                                <div class="ui-dialog-titlebar ui-widget-header ui-corner-all ui-helper-clearfix" 
                                     style="text-align: center; margin:0em -1.6em">Solicitante</div>
                                <table style="width: 100%">
                                    <tr>
                                        <td>Identificaci�n <span style="color:red;">*</span></td>
                                        <td><input maxlength="15" id="id_sol"
                                                   value="" size="15" type="text" required >
                                            <img alt="buscar" src="/fintra/images/botones/iconos/lupa.gif" 
                                                 style="cursor:pointer;" onclick="buscarPersona();" height="15px" width="15px">
                                            <input id="id_filtro" type="hidden">
                                        </td>
                                        <td>Fecha Nacimiento <span style="color:red;">*</span></td>
                                        <td><input maxlength="15" id="fec_nac" style="text-align: center;"
                                                   value="" size="12" type="text" readonly required >
                                            <img src="/fintra/images/cal.gif" id="imgFec_nac" alt="fecha" title="Seleccion de fecha"/>
                                            <script type="text/javascript">
                                                var f = document.getElementById("fec_nac");
                                                if (f.value === '0099-01-01')
                                                    f.value = '';
                                                Calendar.setup({
                                                    inputField: "fec_nac",
                                                    trigger: "imgFec_nac",
                                                    align: "top",
                                                    max: Calendar.dateToInt(new Date()),
                                                    onSelect: function () {
                                                        cal_edad();
                                                        this.hide();
                                                    }
                                                });
                                            </script>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>Estado Civil<span style="color:red;">*</span></td>
                                        <td>
                                            <select id="est_civil" style="width: 100%;">
                                            </select>
                                        </td>
                                        <td>Edad</td>
                                        <td><input maxlength="3" id="edad" style="text-align: right;"
                                                   value="" size="3" type="text" readonly required > A�os
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>Primer Apellido <span style="color:red;">*</span></td>
                                        <td><input maxlength="15" id="pr_apel" 
                                                   value="" size="20" type="text" required onchange="conMayusculas(this)">
                                        </td>
                                        <td>Segundo Apellido</td>
                                        <td><input maxlength="15" id="sg_apel" 
                                                   value="" size="20" type="text" onchange="conMayusculas(this)">
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>Primer Nombre <span style="color:red;">*</span></td>
                                        <td><input maxlength="15" id="pr_nom" 
                                                   value="" size="20" type="text"  required onchange="conMayusculas(this)">
                                        </td>
                                        <td>Segundo Nombre</td>
                                        <td><input maxlength="15" id="sg_nom"
                                                   value="" size="20" type="text" onchange="conMayusculas(this)">
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>Telefono</td>
                                        <td><input maxlength="15" id="tel" 
                                                   value="" size="20" type="text" class="solo-numero">
                                        </td>
                                        <td>Celular <span style="color:red;">*</span></td>
                                        <td><input maxlength="15" id="cel" required
                                                   value="" size="20" type="text" class="solo-numero">
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>Ocupaci�n laboral <span style="color:red;">*</span></td>
                                        <td><select id="ocups" style="width: 100%"
                                                    onchange="refrescarVista('OCUPACION');" onblur="refrescarVista('OCUPACION');">
                                            </select>
                                        </td>
                                        <td>Fecha inicio ocupaci�n <span style="color:red;">*</span></td>
                                        <td><input maxlength="15" id="fec_ocu" style="text-align: center;"
                                                   value="" size="12" type="text" readonly required>
                                            <img src="/fintra/images/cal.gif" id="imgFec_ocu" alt="fecha" title="Seleccion de fecha"/>
                                            <script type="text/javascript">
                                                var f = document.getElementById("fec_ocu");
                                                if (f.value === '0099-01-01')
                                                    f.value = '';
                                                Calendar.setup({
                                                    inputField: "fec_ocu",
                                                    trigger: "imgFec_ocu",
                                                    align: "top",
                                                    max: Calendar.dateToInt(new Date()),
                                                    onSelect: function () {
                                                        this.hide();
                                                    }
                                                });
                                            </script>
                                        </td>
                                    </tr>
                                    <tr><td>&nbsp;</td></tr>
                                    <tr>
                                        <td>Convenio <span style="color:red;">*</span></td>
                                        <td colspan="3">
                                            <select id="convenios" style="width: 100%" required 
                                                    onchange="refrescarVista('CONVENIO');" onblur="refrescarVista('CONVENIO');">
                                            </select>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>Empresa <span style="color:red;">*</span></td>
                                        <td colspan="3">
                                            <select id="empresas_pagaduria" style="width: 100%" required </select>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>Tasa Anual <span style="color:red;">*</span></td>
                                        <td><input maxlength="3" id="tasa_anual" style="text-align: right;"
                                                   value="" size="5" type="text" readonly required > %
                                        </td>
                                        <td>Periodos de Gracia <span style="color:red;">*</span></td>
                                        <td><input maxlength="3" id="per_gracia" style="text-align: right;"
                                                   value="" size="5" type="text" readonly required >
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>Colchon <span style="color:red;">*</span></td>
                                        <td><input maxlength="8" id="colchon" style="text-align: right;"
                                                   value="" size="10" type="text" readonly required >
                                        </td>
                                        <td>Descuento <span style="color:red;">*</span></td>
                                        <td><input maxlength="3" id="porc_desc" style="text-align: right;"
                                                   value="" size="5" type="text" readonly required > %
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>Extraprima <span style="color:red;">*</span></td>
                                        <td><input maxlength="3" id="extraprima" style="text-align: right;"
                                                   value="" size="5" type="text" readonly required > %
                                        </td>
                                        <td>Factor Seguro <span style="color:red;">*</span></td>
                                        <td><input maxlength="10" id="fac_seg" style="text-align: right;" 
                                                   onchange="refrescarVista('SEGURO');" onblur="refrescarVista('SEGURO');"
                                                   size="10" type="text" required >
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>Salario o Pensi�n <span style="color:red;">*</span></td>
                                        <td><input maxlength="15" id="salario" size="18" type="text" required 
                                                    style="text-align: right;" onkeyup="this.value = formato(this.value).moneda;"
                                                    onchange="refrescarVista('SALARIO');" onblur="refrescarVista('SALARIO');">
                                        </td>
                                        <td>Descuentos de Ley <span style="color:red;">*</span></td>
                                        <td><input maxlength="3" id="desc_ley" style="text-align: right;"
                                                   value="" size="5" type="text" readonly required> %
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>Otros ingresos</td>
                                        <td><input maxlength="15" id="otro_ingr" style="text-align: right;" 
                                                   value="" size="18" type="text" onkeyup="this.value = formato(this.value).moneda;"
                                                   onchange="cal_ingresos();" onblur="cal_ingresos();">
                                        </td>
                                        <td>Total Descuentos <span style="color:red;">*</span></td>
                                        <td><input maxlength="15" id="tot_desc" style="text-align: right;" 
                                                   value="" size="18" type="text" readonly>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>Total ingresos <span style="color:red;">*</span></td>
                                        <td><input maxlength="15" id="tot_ingr" style="text-align: right;" 
                                                   value="" size="18" type="text" readonly >
                                        </td>
                                        <td>Monto Consumible <span style="color:red;">*</span></td>
                                        <td><input maxlength="15" id="vlr_cons" style="text-align: right;"  
                                                   value="" size="18" type="text" readonly >
                                        </td>
                                    </tr>
                                    <tr>                                        
                                        <td>Valor Solicitado <span style="color:red;">*</span></td>
                                        <td colspan="3">
                                            <input maxlength="15" id="vlr_sol" style="text-align: right;" onkeyup="this.value = formato(this.value).moneda"
                                                     size="18" type="text" onchange="refrescarVista('VALOR');" onblur="refrescarVista('VALOR');" required>
                                        </td
                                    </tr>
                                </table>
                            </div>
                        </td>
                        <td rowspan="2">
                            <div class="ui-jqgrid ui-widget ui-widget-content ui-corner-all"
                                 style="width: 550px; padding: 0.5em 2em; margin: 0.2em; height: 100%; top: 0.2em;">
                                <div class="ui-dialog-titlebar ui-widget-header ui-corner-all ui-helper-clearfix" 
                                     style="text-align: center; margin:0em -1.6em">ALICUOTAS</div>
                                <table border=1 class="alicuotas">
                                    <thead>
                                        <th>Cuota</th>
                                        <th>Valor</th>
                                        <th></th>
                                        <th>Cuota</th>
                                        <th>Valor</th>
                                        <th></th>
                                        <th>Cuota</th>
                                        <th>Valor</th>
                                    </thead>
                                    <tbody id="lisCuotas"></tbody>
                                </table>
                            </div>
                        </td></tr>
                    <tr>
                        <td>
                            <div class="ui-jqgrid ui-widget ui-widget-content ui-corner-all"
                                 style="width: 550px; padding: 0.5em 2em; margin: 0.2em; float: right">

                                <div class="ui-dialog-titlebar ui-widget-header ui-corner-all ui-helper-clearfix" 
                                     style="text-align: center; margin:0em -1.6em">RESUMEN</div>
                                <table>
                                    <tr>
                                        <td><b>Valor Cr�dito:</b></td>
                                        <td><input maxlength="15" id="valor" 
                                                   value="" size="20" type="text" required readonly></td>
                                        <td><b>CUOTA:</b></td>
                                        <td><label id="vlr_cuota"></label></td>
                                    </tr><tr>
                                        <td><b>Plazo</b></td>
                                        <td><input maxlength="3" id="plazo"  
                                                   value="" size="3" type="text" required readonly></td>
                                        <td><b>SEGURO</b></td>
                                        <td><label id="vlr_seguro"></label></td>
                                    </tr>
<!--                                <tr>
                                        <td><b>CUOTA:</b></td>
                                        <td><label id="vlr_cuota"></label></td>
                                    </tr><tr>
                                        <td><b>SEGURO</b></td>
                                        <td><label id="vlr_seguro"></label></td>
                                    </tr><tr>
                                        <td><b>EXTRAPRIMA: </b></td>
                                        <td><label id="vlr_extra"></label></td>
                                    </tr><tr>
                                        <td><b>TOTAL:</b></td>
                                        <td><label id="total_cuota"></label></td>
                                    </tr>-->
                                    <tr>
                                        <td><b>VIABLE:</b></td>
                                        <td><select name="viable" id="viable">
                                            <option value="S">SI</option>
                                            <option value="N">NO</option>                                         
                                        </select></td>
                                        <td><b>EXTRAPRIMA: </b></td>
                                        <td><label id="vlr_extra"></label></td>
                                    </tr>
                                    <tr>
                                        <td colspan="2"></td>
                                        <td><b>TOTAL:</b></td>
                                        <td><label id="total_cuota"></label></td>
                                    </tr>
                                </table>
                                <div class="ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only" 
                                     role="button" aria-disabled="false" onclick="confirmacionGuardar();">
                                    <span class="ui-button-text">Guardar y Consultar</span>
                                </div>
                                <div class="ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only" 
                                     role="button" aria-disabled="false" id="btn_hdc">
                                    <span class="ui-button-text">Ver Historia DC</span>
                                </div>
                                <div class="ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only" 
                                     role="button" aria-disabled="false" onclick="genPDF();">
                                    <span class="ui-button-text">Exportar PDF</span>
                                </div>
                                <div class="ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only" 
                                     role="button" aria-disabled="false" onclick="AbrirDivDocumentosRequeridos();">
                                    <span class="ui-button-text">Documentos Requeridos</span>
                                </div>
                            </div></td>
                    </tr>
                </table>
            </center>
        </div>
        <div id="dialogo_docs_requeridos" class="ventana" style="display:none;">           
        </div>
        <div id="dialogo" class="ventana" style="display:none;">
            <p id="msj"></p>
        </div>
        <script>
            init();
        </script>
    </body>
</html>
