<%-- 
    Document   : formulario_libranza
    Created on : 14/03/2016, 03:32:50 PM
    Author     : hcuello
--%>

<%@page import="com.tsp.operation.model.beans.SolicitudNegocio"%>
<%@page import="com.tsp.util.Util"%>
<%@page import="com.tsp.operation.model.services.GestionSolicitudAvalService"%>
<%@page import="com.tsp.operation.model.beans.SolicitudBienes"%>
<%@page import="com.tsp.operation.model.beans.SolicitudVehiculo"%>
<%@page import="java.util.ArrayList"%>
<%@page import="com.tsp.operation.model.beans.Usuario"%>
<%@page import="java.util.Date"%>
<%@page import="java.text.SimpleDateFormat"%>
<%@page import="java.text.DateFormat"%>
<%@page import="com.tsp.operation.model.beans.SolicitudPersona"%>
<%@page import="com.tsp.operation.model.beans.SolicitudAval"%>
<%@page import="com.google.gson.JsonArray"%>
<%@page import="com.google.gson.JsonObject"%>
<%@page import="com.tsp.operation.controller.FormularioLibranzaAction"%>
<%@page contentType="text/html" pageEncoding="ISO-8859-1"%>
<%@page session="true"%>
<%@include file="/WEB-INF/InitModel.jsp"%>

<%
    ArrayList<SolicitudVehiculo> lista_vehiculos = null;
    ArrayList<SolicitudBienes> lista_bienes = null;
    String[] dato1 = null;
    Usuario usuario = (Usuario) session.getAttribute("Usuario");
    GestionSolicitudAvalService gsaserv = new GestionSolicitudAvalService(usuario.getBd());
    ArrayList tipo_bien = gsaserv.busquedaGeneral("TIPBIE");
    String vista = request.getParameter("vista") != null ? request.getParameter("vista") : "0";
    ArrayList tipo_vehiculo = gsaserv.busquedaGeneral("TIPVEH");
    String login_into=(String)session.getAttribute("login_into");
    String estadoDireccion = (login_into.equals("Fenalco_bol"))? "" : "readonly";
    SolicitudNegocio bean_neg = null;
%>    


<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
        <title>Formulario creditos de libranzas</title>

        <link type="text/css" rel="stylesheet" href="./css/jquery/jquery-ui/jquery-ui.css" />
        <link type="text/css" rel="stylesheet" href="./css/jquery/jquery.jqGrid/ui.jqgrid.css" />
        <link rel="stylesheet" type="text/css" href="./css/jCal/jscal2.css" />

        <script type="text/javascript" src="/fintra/js/jquery/jquery-ui/jquery.min.js"></script>
        <script type="text/javascript" src="/fintra/js/jquery/jquery-ui/jquery.ui.min.js"></script>
        <script type="text/javascript" src="./js/FormularioLibranza.js"></script>
        <script type="text/javascript" src="/fintra/js/jquery/jquery.jqGrid-4.7.0/js/jquery.jqGrid.min.js"></script>
        <script type="text/javascript" src="/fintra/js/jquery/jquery.jqGrid-4.7.0/js/grid.locale-es.js"></script>
        <script type="text/javascript" src="/fintra/js/ex/form2object.js"></script>
        <script type="text/javascript" src="/fintra/js/jCal/jscal2.js"></script>
        <script type="text/javascript" src="/fintra/js/jCal/lang/es.js"></script>
        
        <style type="text/css">
            td { white-space:nowrap; }
            b { color:white; }
            .divs {
                font-family: Verdana, Tahoma, Arial, Helvetica, sans-serif;
                font-size: small;
                color: #000000;
                background-color: #2A88C8;
                width: 1512px;
                -moz-border-radius: 0.7em;
                -webkit-border-radius: 0.7em;
            }
            /* turn the border green when it becomes valid */
            input:required:valid, textarea:required:valid, select:required:valid {
                border:1px solid green;
                background:#fff;
            }
            /*turn the border red if not valid */
            input:required:invalid, textarea:required:invalid, 
                select:required:invalid, .validation-failed {
                border:1px solid red;
            }
            form#formulario .filaazul {
                color: #000 !important;
                font-family: Tahoma,Arial;
                font-size: 12px;
                background-color: rgba(255, 255, 255, 0.93) !important;
            }
        </style>

        <%
            boolean nuevo = true;
            String num_solicitud = "0000";

            //Usuario usuario = (Usuario) session.getAttribute("Usuario");
            FormularioLibranzaAction accion = new FormularioLibranzaAction();
            accion.init(usuario);

            JsonArray combo;
            JsonObject totalCombos = new JsonObject(),
            objeto = new JsonObject(),
            personaLista = new JsonObject(),
            solicitante = null,
            codeudor_1 = null,
            codeudor_2 = null;

            SolicitudAval bean_sol = null;

            try {

                num_solicitud = (request.getParameter("num_solicitud") != null && request.getParameter("num_solicitud").equals("") == false) ? request.getParameter("num_solicitud") : "0000";

                //validar si es nuevo formulario
                nuevo = (num_solicitud.equalsIgnoreCase("0000"));
                
                if (!nuevo) {
                    
                    bean_sol = accion.buscarSolicitud(num_solicitud);
                    
                    if (bean_sol != null) {
                        // num_solicitud = bean_sol.getNumeroSolicitud();
                        personaLista = accion.buscarPersonas(num_solicitud);

                        solicitante = personaLista.getAsJsonObject("solicitante");
                        //codeudor_1 = personaLista.getAsJsonObject("codeudor_1");
                        //codeudor_2 = personaLista.getAsJsonObject("codeudor_2");
                    }
                    
                } else {
                    
                    bean_sol = new SolicitudAval();
                    
                }

                totalCombos = accion.buscarFiltros(usuario);
                
            } catch (Exception e) {
                e.printStackTrace();
            }
        %>
    </head>
    
    <body onload="window.moveTo(0, 0); window.resizeTo(screen.width, screen.height);">
        <jsp:include page="/toptsp.jsp?encabezado=Formulario creditos de Libranza"/>
        <script>console.log(<%=personaLista%>);</script>

        <form id="formulario" name="formulario" style="position:absolute; width:80%; height:83%; z-index:0; left: 12%; top: 110px; ">
            
            <!-- CABECERA -->
            <div id="header">
                <table id="tsup" border=0 style="border: 0em; border-collapse:collapse; width: 100%" style="border: 0em; border-collapse:collapse; width: 1521px; border: 4px solid  #2A88C8;">
                    
                    <thead>
                        <tr class="filaazul">
                            <th align="center" colspan="9" style="text-align:center; ">  CREDITO DE LIBRANZA </th>
                            <th  style="text-align:right; "> No. de Formulario </th>
                            <th>
                                <input name="form.cabecera.num_solicitud" id="num_solicitud" value="<%=num_solicitud%>"  readonly="readonly" style="background-color: transparent; border-bottom: 0px; border-left: 0px; border-right: 0px; border-top: 0px; text-align:center; color:#990000; font-size:16px;" >
                                <input name="form.cabecera.actividad" type="hidden" id="actividad" value="<%=(nuevo) ? "" : (bean_sol == null || (bean_sol != null && bean_sol.getActividadNegocio() == null)) ? "" : bean_sol.getActividadNegocio()%>">
                                <input name="form.cabecera.negocio" type="hidden" id="negocio" value="<%=(nuevo || bean_sol == null || (bean_sol != null && bean_sol.getCodNegocio() == null)) ? "" : bean_sol.getCodNegocio()%>">
                                <input name="form.cabecera.tipoconv" type="hidden" id="tipoconv" value="Libranza">
                                <input name="form.cabecera.est_sol" type="hidden" id="est_sol" value="<%=(nuevo || bean_sol == null || (bean_sol != null && bean_sol.getEstadoSol() == null)) ? "B" : bean_sol.getEstadoSol()%>">
                            </th>
                        </tr>
                    </thead>
                    
                    <tbody>
                        
                        <tr class="filaazul">
                            <td rowspan="6" width="9%" valign="top">
                                <img alt="logofintra" src="/fintra/images/fintra_new.png" onclick="getNegociosLegalizar(true);" height="219" width="240">
                            </td>
                            <td width="5%">  Fecha  </td>
                            <td width="20%">
                                <input type="text" id="fecha_cons" name="form.cabecera.fecha_cons" maxlength="10" size="15" readonly  
                                       value="<%=(nuevo) || (bean_sol == null || (bean_sol != null && bean_sol.getFechaConsulta() == null)) ? new SimpleDateFormat("yyyy-MM-dd").format(new Date()) : bean_sol.getFechaConsulta().substring(0, 10)%>">
                                <% if (nuevo) { %>
                                <img src="/fintra/images/cal.gif" id="imgFec_cons" alt="fecha" title="Seleccion de fecha" />
                                <script type="text/javascript">
                                    Calendar.setup({
                                        inputField: "fecha_cons",
                                        trigger: "imgFec_cons",
                                        align: "top",
                                        max: Calendar.dateToInt(new Date()),
                                        onSelect: function () {
                                            this.hide();
                                        }
                                    });
                                </script> 
                                <% }%>
                            </td>
                            <td colspan="6">
                                <table border="0">
                                    <tr>
                                        <td>Desembolso</td>
                                        <td>
                                            <input type="radio" name="form.cabecera.tipo_desembolso" id="TipoDesemCheque" value="Cheque" checked onclick="ValidarTipoDesembolso(this.id)">Cheque
                                            <input type="radio" name="form.cabecera.tipo_desembolso" id="TipoDesemCuenta" value="Cuenta" onclick="ValidarTipoDesembolso(this.id)">Cuenta
                                        </td>
                                        <td>&nbsp;</td>
                                        <td>&nbsp;</td>
                                        <td>Entidad</td>
                                        <td>&nbsp;</td>
                                        <td>&nbsp;</td>
                                        <td>
                                            <select name="form.cabecera.entidadDesembolso" id="EntidadDesembolso" style="width:20em;" disabled="" required><%
                                                combo = accion.buscarEntidadesComprar("obligaciones_recoger").getAsJsonArray();
                                                for ( int j = 0; j < combo.size(); j++ ) {
                                                    objeto = combo.get(j).getAsJsonObject(); %>
                                                    <option value="<%= objeto.get("codigo").getAsString()%>"><%= objeto.get("valor").getAsString()%></option><%  
                                                }%>
                                            </select>
                                        </td>
                                        <td>&nbsp;</td>
                                        <td>&nbsp;</td>
                                        <td>&nbsp;</td>
                                    </tr>
                                </table>    
                            </td>
                            
                            <td width="8%">  No.aprobacion  </td>
                            <td width="12%">
                                <input type="text" id="num_aprobacion" name="form.cabecera.num_aprobacion" value="<%=(nuevo || bean_sol == null || (bean_sol != null && bean_sol.getNumeroAprobacion() == null)) ? "" : bean_sol.getNumeroAprobacion()%>" disabled>
                            </td>
                        </tr>
                        
                        <tr class="filaazul">
                            <td>  Pagaduria  </td>
                            <td colspan="3">
                                <select name="form.cabecera.pagaduria" id="pagaduria" 
                                        style="width:90%;" onchange="setEmpresaSolicitante()" required>
                                    <%
                                        combo = totalCombos.get("pagadurias").getAsJsonArray();
                                        for (int i = 0; i < combo.size(); i++) {
                                            objeto = combo.get(i).getAsJsonObject();
                                    %>
                                    <option value="<%= objeto.get("codigo").getAsString()%>"
                                            <%=(!nuevo && bean_sol != null && (bean_sol.getAfiliado().equalsIgnoreCase(objeto.get("codigo").getAsString()))) ? "selected" : ""%> >
                                        <%= objeto.get("valor").getAsString()%>
                                    </option>
                                    <%  }%>
                                </select>
                            </td>
                            <td width="6%">  Convenio  </td>
                            <td colspan="3" >
                                <input type="text" readonly value="Convenio Libranza" style="width:90%;">
                                <input type="hidden" id="convenio" name="form.cabecera.convenio" value="<%= (nuevo) ? "38" : (bean_sol == null || bean_sol.getIdConvenio() == null) ? "" : bean_sol.getIdConvenio()%>">
                            </td>
                            <td>  Codigo  </td>
                            <td>
                                <input name="codigo" onkeyup="soloNumeros(this.id);" type="text" id="form.cabecera.codigo" maxlength="9" size="9" style="width:90%;"
                                       value="<%= (nuevo) ? "" : (bean_sol == null || bean_sol.getCodigo() == null) ? "" : bean_sol.getCodigo()%>" >
                            </td>
                        </tr>
                        
                        <tr class="filaazul">
                            <td> Sector </td>
                            <td   colspan="3">
                                <input type="text" readonly value="Consumo" style="width:90%;">
                                <input type="hidden" id="sectid" name="form.cabecera.sectid" value="<%= (nuevo) ? "L00" : (bean_sol == null || bean_sol.getSector() == null) ? "" : bean_sol.getSector()%>">
                                <input type="hidden" id="subsectid" name="form.cabecera.subsectid" value="<%= (nuevo) ? "01" : (bean_sol == null || bean_sol.getSubsector() == null) ? "" : bean_sol.getSubsector()%>">
                            </td>
                            <td>  Producto </td>
                            <td colspan="3">
                                <input type="text" style="width:90%;" value="Libranza" disabled>
                                <input type="hidden" id="producto" name="form.cabecera.producto" style="width:90%;" value="<%=(bean_sol == null || bean_sol.getProducto() == null) ? "" : bean_sol.getProducto()%>" disabled>
                            </td>

                            <td>T&iacute;tulo valor</td>
                            <td >
                                <input type="text" style="width:90%;" value="Pagare" disabled >
                                <input type="hidden" name="form.cabecera.cmbTituloValor" id="cmbTituloValor" style="width:90%;" value="<%=bean_sol == null || bean_sol.getTipoNegocio() == null ? "03" : bean_sol.getTipoNegocio()%>" disabled >
                            </td>
                        </tr>
                        
                        <tr class="filaazul">
                            <td>Ocupaci�n</td>
                            <td>
                                <table border=0 width="100%" >
                                    <tr>
                                        <td>
                                            <select name="form.cabecera.ocupaciones" id="ocupations" style="width:11em;" onchange="xxx();" required>
                                                    <option value=""> ... </option>
                                                    <option value="EPLDO"> EMPLEADO </option>
                                                    <option value="PENSI"> PENSIONADO </option>
                                            </select>                                            
                                           
                                        </td>
                                        <td align="right">Tipo de contrato</td>
                                    </tr>
                                </table>

                            </td>
                            
                            <td colspan="2">
                                <select id="tipo_cont_nat" name="form.cabecera.tipo_cont" style="width:10em;" required>
                                    <%
                                        combo = totalCombos.get("tipo_contrato").getAsJsonArray();
                                        for (int i = 0; i < combo.size(); i++) {
                                            objeto = combo.get(i).getAsJsonObject();
                                    %>
                                    <option value="<%= objeto.get("codigo").getAsString()%>"  
                                            <%=(solicitante != null && solicitante.getAsJsonObject("laboral").get("tipo_contrato").getAsString().equalsIgnoreCase(objeto.get("codigo").getAsString())) ? "selected" : ""%> >
                                        <%= objeto.get("valor").getAsString()%>
                                    </option>
                                    <%  }%>  
                                </select>
                            </td>
                            
                            <td>Cargo &nbsp;&nbsp;</td>
                            <td>
                                <input size="30" type="text" id="car_emp_nat" name="form.cabecera.car_emp" value="<%=solicitante != null ? solicitante.getAsJsonObject("laboral").get("cargo").getAsString() : ""%>" required>
                            </td>
                                        
                            <td colspan="3">
                                <table width="100%" border=0>
                                    <tr>
                                        <td>&nbsp;&nbsp;Destino&nbsp;&nbsp;</td>
                                        
                                        <td>
                                            <input type="checkbox" id="lib_inv" name="form.cabecera.lib_inv" value="lib_inv">Libre Inversi�n
                                            <input type="checkbox" id="com_car" name="form.cabecera.com_car" value="com_car" onclick="ValidarCompraCartera()">Compra Cartera
                                            <input type="checkbox" id="represt" name="form.cabecera.represt" value="represteo" disabled="">Represteo
                                        </td>                                        
                                    </tr>
                                </table>
                            </td>  
                            <td>&nbsp;</td>
                            
                        </tr>
                        
                        <tr class="filaazul">
                            <td>  Tipo  </td>
                            <td>
                                <input type="text" style="width:12em;" value="Persona natural" disabled >
                                <input type="hidden" id="tipo_p" name="form.cabecera.tipo_p" style="width:12em;" value="<%=(nuevo) ? "N" : (bean_sol == null || bean_sol.getTipoPersona() == null) ? "" : bean_sol.getTipoPersona()%>" disabled >
                            </td>
                            
                            <td>  Valor solicitado $  </td>
                            <td>
                                <input id="valor_solicitado" onkeyup="soloNumeros(this.id);" name="form.cabecera.valor_solicitado"  
                                       onblur="formato(this, 0);" onkeyup="soloNumeros(this.id);" size="15" required
                                       pattern="^(((\d{1,3})(,\d{3})*)|(\d+))(.\d+)?$" type="numeric" 
                                       value="<%=(nuevo) ? "" : (bean_sol == null || bean_sol.getValorSolicitado() == null) ? "" : bean_sol.getValorSolicitado()%>" 
                                       style="border-bottom: 1px solid; border-left: 0px; border-right: 0px; border-top: 0px; text-align:right;">
                            </td>
                            <td>   Plazo  </td>
                            <td>
                                <input type="number" min="1" id="plazo" name="form.cabecera.plazo" onblur="formato(this, 0);" 
                                       onkeyup="soloNumeros(this.id);" size="2" required
                                       value="<%=(nuevo) ? "" : (bean_sol == null || bean_sol.getPlazo() == null) ? "" : bean_sol.getPlazo()%>" 
                                       style="border-bottom: 1px solid; border-left: 0px; border-right: 0px; border-top: 0px; text-align:right;">
                            </td>
                            <td>  Plazo 1era cuota  </td>
                            <td valign="middle">
                                <input type="text" disabled value="30 d�as"/>
                                <input type="hidden" name="form.cabecera.forma_pago" id="forma_pago" class="listmenu" value="30" </input>
                            </td>
                            <td>&nbsp;</td>
                            <td>&nbsp;</td>
                        </tr>
                    </tbody>
                </table>
            </div>
            
            <!-- SOLICITANTE -->                           
            <div style="border: 0.1em solid black; padding: 0.3em; overflow-y: scroll; overflow-x: hidden; max-height: 83%;" id="contenido" class="divs">
                
                <!-- <b>PERSONA NATURAL</b><br> -->
                
                <b>INFORMACION BASICA</b>
                <table id="tnatural" style="border-collapse:collapse; width:100%" border="1">
                    <tbody>
                        <tr class="filaazul">
                            <td> Identificacion<br>
                                <select id="tipo_id_nat" name="form.solicitante.basica.tipo_id" required>
                                    <%
                                        combo = totalCombos.get("tipo_id").getAsJsonArray();
                                        for (int i = 0; i < combo.size(); i++) {
                                            objeto = combo.get(i).getAsJsonObject();
                                    %>
                                    <option value="<%= objeto.get("codigo").getAsString()%>"  
                                            <%=(solicitante != null && solicitante.get("basica").getAsJsonObject().get("tipo_id").getAsString().equalsIgnoreCase(objeto.get("codigo").getAsString())) ? "selected" : ""%> >
                                        <%= objeto.get("valor").getAsString()%>
                                    </option>
                                    <%  }%>
                                </select>
                                <input maxlength="15" id="id_nat" onkeypress="return isNumberKey(event)" name="form.solicitante.basica.id" 
                                       value="<%=solicitante != null ? solicitante.getAsJsonObject("basica").get("identificacion").getAsString() : ""%>" 
                                       size="20" type="text" required <%=(!nuevo) ? " disabled " : " "%>>
                                <img alt="buscar" src="/fintra/images/botones/iconos/lupa.gif" style="cursor:pointer;" onclick="busquedaPersona('nat');" height="15" width="15">
                            </td>
                            <td width="17%"> Primer apellido<br>
                                <input id="pr_apellido_nat" name="form.solicitante.basica.pr_apellido" required <%=(!nuevo) ? " disabled " : " "%>
                                       value="<%=solicitante != null ? solicitante.getAsJsonObject("basica").get("primer_apellido").getAsString() : ""%>" 
                                       size="25" maxlength="25" onchange="conMayusculas(this)" onkeypress="return soloLetras(event)" type="text">
                            </td>
                            <td class="filableach" width="16%"> Segundo Apellido<br>
                                <input id="seg_apellido_nat" name="form.solicitante.basica.seg_apellido" required
                                       value="<%=solicitante != null ? solicitante.getAsJsonObject("basica").get("segundo_apellido").getAsString() : ""%>" 
                                       size="25" maxlength="25" onchange="conMayusculas(this)" onkeypress="return soloLetras(event)" type="text">
                            </td>
                            <td width="17%"> Primer Nombre<br>
                                <input id="pr_nombre_nat" name="form.solicitante.basica.pr_nombre" required <%=(!nuevo) ? " disabled " : " "%>
                                       value="<%=solicitante != null ? solicitante.getAsJsonObject("basica").get("primer_nombre").getAsString() : ""%>"
                                       size="25" maxlength="25" onchange="conMayusculas(this)" onkeypress="return soloLetras(event)" type="text">
                            </td>
                            <td class="filableach" width="17%">  Segundo Nombre<br>
                                <input id="seg_nombre_nat" name="form.solicitante.basica.seg_nombre" 
                                       value="<%=solicitante != null ? solicitante.getAsJsonObject("basica").get("segundo_nombre").getAsString() : ""%>"
                                       size="25" maxlength="25" onchange="conMayusculas(this)" onkeypress="return soloLetras(event)" type="text">
                            </td>
                            <td width="16%"> Genero<br>
                                <select id="genero_nat" name="form.solicitante.basica.genero" required>
                                    <option value="">...</option>
                                    <option value="M" <%=(solicitante != null && solicitante.getAsJsonObject("basica").get("genero").getAsString().equalsIgnoreCase("M") ? "selected" : "")%>>Masculino</option>
                                    <option value="F" <%=(solicitante != null && solicitante.getAsJsonObject("basica").get("genero").getAsString().equalsIgnoreCase("F") ? "selected" : "")%>>Femenino</option>
                                </select>
                            </td> 
                        </tr>
                        <tr class="filaazul">
                            <td width="17%"> Estado civil<br>
                                <select id="est_civil_nat" name="form.solicitante.basica.est_civil" required>
                                    <%
                                        combo = totalCombos.get("estado_civil").getAsJsonArray();
                                        for (int i = 0; i < combo.size(); i++) {
                                            objeto = combo.get(i).getAsJsonObject();
                                    %>
                                    <option value="<%= objeto.get("codigo").getAsString()%>"  
                                            <%=(solicitante != null && solicitante.getAsJsonObject("basica").get("estado_civil").getAsString().equalsIgnoreCase(objeto.get("codigo").getAsString())) ? "selected" : ""%> >
                                        <%= objeto.get("valor").getAsString()%>
                                    </option>
                                    <%  }%>
                                </select>
                            </td>
                            <td> Fecha de expedicion<br>
                                <input class="validate-date" id="f_exp_nat" name="form.solicitante.basica.f_exp" size="15" type="text" readonly
                                       value="<%=solicitante != null ? solicitante.getAsJsonObject("basica").get("fecha_expedicion_id").getAsString() : ""%>">
                                <img src="/fintra/images/cal.gif" id="imgFecExp_nat" alt="fecha" title="Seleccion de fecha"  required/>
                                <script type="text/javascript">
                                    var f = document.getElementById("f_exp_nat");
                                    if (f.value === '0099-01-01')
                                        f.value = '';
                                    Calendar.setup({
                                        inputField: "f_exp_nat",
                                        trigger: "imgFecExp_nat",
                                        align: "top",
                                        max: Calendar.dateToInt(new Date()),
                                        onSelect: function () {
                                            this.hide();
                                        }
                                    });
                                </script> 
                            </td>
                            <td> Departamento<br>
                                <select id="dep_exp_nat" name="form.solicitante.basica.dep_exp" required style="width:20em;" onchange="cargarCiudades(this.value, 'ciu_exp_nat', '');">
                                    <%
                                        combo = totalCombos.get("departamentos").getAsJsonArray();
                                        for (int i = 0; i < combo.size(); i++) {
                                            objeto = combo.get(i).getAsJsonObject();
                                    %>
                                    <option value="<%= objeto.get("codigo").getAsString()%>"  
                                            <%=(solicitante != null && solicitante.getAsJsonObject("basica").get("depto_expedicion_id").getAsString().equalsIgnoreCase(objeto.get("codigo").getAsString())) ? "selected" : ""%> >
                                        <%= objeto.get("valor").getAsString()%>
                                    </option>
                                    <%  }%>
                                </select>
                            </td>
                            <td> Ciudad<br>
                                <div id="d_ciu_exp_nat">
                                    <select id="ciu_exp_nat" name="form.solicitante.basica.ciu_exp" required style="width:20em;">
                                        <%
                                            combo = accion.buscarCiudades(solicitante != null ? solicitante.getAsJsonObject("basica").get("depto_expedicion_id").getAsString() : "");
                                            for (int i = 0; i < combo.size(); i++) {
                                                objeto = combo.get(i).getAsJsonObject();
                                        %>
                                        <option value="<%= objeto.get("codigo").getAsString()%>"  
                                                <%=(solicitante != null && solicitante.getAsJsonObject("basica").get("ciudad_expedicion_id").getAsString().equalsIgnoreCase(objeto.get("codigo").getAsString())) ? "selected" : ""%> >
                                            <%= objeto.get("valor").getAsString()%>
                                        </option>
                                        <%  }%>
                                    </select>
                                </div>
                            </td>
                            <td>  Nivel de estudio<br>
                                <select id="niv_est_nat" name="form.solicitante.basica.niv_est" required style="width:20em;">
                                    <%
                                        combo = totalCombos.get("nivel_estudio").getAsJsonArray();
                                        for (int i = 0; i < combo.size(); i++) {
                                            objeto = combo.get(i).getAsJsonObject();
                                    %>
                                    <option value="<%= objeto.get("codigo").getAsString()%>"  
                                            <%=(solicitante != null && solicitante.getAsJsonObject("basica").get("nivel_estudio").getAsString().equalsIgnoreCase(objeto.get("codigo").getAsString())) ? "selected" : ""%> >
                                        <%= objeto.get("valor").getAsString()%>
                                    </option>
                                    <%  }%>
                                </select>
                            </td>
                            <td class="filableach"> Profesion<br>
                                <input id="prof_nat" name="form.solicitante.basica.prof" maxlength="30" type="text" required 
                                       value="<%=solicitante != null ? solicitante.getAsJsonObject("basica").get("profesion").getAsString() : ""%>">
                            </td>
                        </tr>
                        <tr class="filaazul">
                            <td> Fecha de nacimiento<br>
                                <input id="f_nac_nat" class="validate-date" name="form.solicitante.basica.f_nac" 
                                       value="<%=solicitante != null ? solicitante.getAsJsonObject("basica").get("fecha_nacimiento").getAsString() : ""%>"
                                       size="15" maxlength="15" type="text" readonly required>
                                <img src="/fintra/images/cal.gif" id="imgFecNac_nat" alt="fecha" title="Seleccion de fecha" />
                                <script type="text/javascript">
                                    var f = document.getElementById("f_nac_nat");
                                    if (f.value === '0099-01-01')
                                        f.value = '';
                                    var minimo = new Date();
                                    minimo.setYear(minimo.getYear() - 75);
                                    var maximo = new Date();
                                    maximo.setYear(maximo.getYear() - 18);

                                    Calendar.setup({
                                        inputField: "f_nac_nat",
                                        trigger: "imgFecNac_nat",
                                        align: "top",
                                        min: Calendar.dateToInt(minimo),
                                        max: Calendar.dateToInt(maximo),
                                        onSelect: function () {
                                            this.hide();
                                        }
                                    });
                                </script> 
                            </td>
                            <td> Departamento<br>
                                <select id="dep_nac_nat" name="form.solicitante.basica.dep_nac" required style="width:20em;" onchange="cargarCiudades(this.value, 'ciu_nac_nat', '');">
                                    <%
                                        combo = totalCombos.get("departamentos").getAsJsonArray();
                                        for (int i = 0; i < combo.size(); i++) {
                                            objeto = combo.get(i).getAsJsonObject();
                                    %>
                                    <option value="<%= objeto.get("codigo").getAsString()%>"  
                                            <%=(solicitante != null && solicitante.getAsJsonObject("basica").get("depto_nacimiento").getAsString().equalsIgnoreCase(objeto.get("codigo").getAsString())) ? "selected" : ""%> >
                                        <%= objeto.get("valor").getAsString()%>
                                    </option>
                                    <%  }%>
                                </select>
                            </td>
                            <td> Ciudad<br>
                                <div id="d_ciu_nac_nat">
                                    <select id="ciu_nac_nat" name="form.solicitante.basica.ciu_nac" required style="width:20em;">
                                        <%
                                            combo = accion.buscarCiudades(solicitante != null ? solicitante.getAsJsonObject("basica").get("depto_nacimiento").getAsString() : "");
                                            for (int i = 0; i < combo.size(); i++) {
                                                objeto = combo.get(i).getAsJsonObject();
                                        %>
                                        <option value="<%= objeto.get("codigo").getAsString()%>"  
                                                <%=(solicitante != null && solicitante.getAsJsonObject("basica").get("ciudad_nacimiento").getAsString().equalsIgnoreCase(objeto.get("codigo").getAsString())) ? "selected" : ""%> >
                                            <%= objeto.get("valor").getAsString()%>
                                        </option>
                                        <%  }%>
                                    </select>
                                </div>
                            </td>
                            <td>  Personas a cargo<br>
                                <input id="pcargo_nat" name="form.solicitante.basica.pcargo" required min="0"
                                       value="<%=solicitante != null ? solicitante.getAsJsonObject("basica").get("personas_a_cargo").getAsString() : "0"%>" 
                                       onkeyup="soloNumeros(this.id);" size="15" maxlength="15" type="number">
                            </td>
                            <td>  N� de hijos<br>
                                <input id="nhijos_nat" name="form.solicitante.basica.nhijos" required min="0"
                                       value="<%=solicitante != null ? solicitante.getAsJsonObject("basica").get("num_hijos").getAsString() : "0"%>" 
                                       onkeyup="soloNumeros(this.id);" onblur="fhijos('nat');" size="15" maxlength="15" type="number">
                            </td>
                            <td>  Total grupo familiar<br>
                                <input id="ngrupo_nat" name="form.solicitante.basica.ngrupo" required min="0"
                                       value="<%=solicitante != null ? solicitante.getAsJsonObject("basica").get("grupo_familia").getAsString() : "0"%>" 
                                       onkeyup="soloNumeros(this.id);" size="15" maxlength="15" type="number">
                            </td>
                        </tr>
                        <tr class="filaazul"> 
                            <td colspan="2"> Direccion residencia<br>
                                <label class="ui-icon ui-icon-note" style="float: left; margin-right: 4px;" onclick="genDireccion('dir_nat');"></label>
                                <input id="dir_nat" name="form.solicitante.basica.dir" required readonly
                                       value="<%=solicitante != null ? solicitante.getAsJsonObject("basica").get("direccion").getAsString() : ""%>" 
                                       size="60" maxlength="160" type="text">
                            </td>
                            <td> Departamento<br>
                                <select id="dep_nat" name="form.solicitante.basica.dep" required style="width:20em;" onchange="cargarCiudades(this.value, 'ciu_nat', '');">
                                    <%
                                        combo = totalCombos.get("departamentos").getAsJsonArray();
                                        for (int i = 0; i < combo.size(); i++) {
                                            objeto = combo.get(i).getAsJsonObject();
                                    %>
                                    <option value="<%= objeto.get("codigo").getAsString()%>"  
                                            <%=(solicitante != null && solicitante.getAsJsonObject("basica").get("departamento").getAsString().equalsIgnoreCase(objeto.get("codigo").getAsString())) ? "selected" : ""%> >
                                        <%= objeto.get("valor").getAsString()%>
                                    </option>
                                    <%  }%>
                                </select>
                            </td>
                            <td> Ciudad<br>
                                <div id="d_ciu_nat">
                                    <select id="ciu_nat" name="form.solicitante.basica.ciu" required style="width:20em;">
                                        <%
                                            combo = accion.buscarCiudades(solicitante != null ? solicitante.getAsJsonObject("basica").get("departamento").getAsString() : "");
                                            for (int i = 0; i < combo.size(); i++) {
                                                objeto = combo.get(i).getAsJsonObject();
                                        %>
                                        <option value="<%= objeto.get("codigo").getAsString()%>"  
                                                <%=(solicitante != null && solicitante.getAsJsonObject("basica").get("ciudad").getAsString().equalsIgnoreCase(objeto.get("codigo").getAsString())) ? "selected" : ""%> >
                                            <%= objeto.get("valor").getAsString()%>
                                        </option>
                                        <%  }%>
                                    </select>
                                </div>
                            </td>
                            <td> Barrio<br>
                                <input id="barrio_nat" name="form.solicitante.basica.barrio" required 
                                       value="<%=solicitante != null ? solicitante.getAsJsonObject("basica").get("barrio").getAsString() : ""%>"
                                       size="20" maxlength="100" type="text">
                            </td>
                            <td> Estrato<br>
                                <select name="form.solicitante.basica.estr" id="estr_nat" style="width:10em;" required>
                                    <%
                                        combo = totalCombos.get("estrato").getAsJsonArray();
                                        for (int i = 0; i < combo.size(); i++) {
                                            objeto = combo.get(i).getAsJsonObject();
                                    %>
                                    <option value="<%= objeto.get("codigo").getAsString()%>"  
                                            <%=(solicitante != null && solicitante.getAsJsonObject("basica").get("estrato").getAsString().equalsIgnoreCase(objeto.get("codigo").getAsString())) ? "selected" : ""%> >
                                        <%= objeto.get("valor").getAsString()%>
                                    </option>
                                    <%  }%>
                                </select>
                            </td>
                        </tr>
                        <tr class="filaazul">
                            <td colspan="2">  E-mail<br>
                                <input id="mail_nat" name="form.solicitante.basica.mail" required
                                       value="<%=solicitante != null ? solicitante.getAsJsonObject("basica").get("email").getAsString() : ""%>" 
                                       size="60" type="email" pattern="^[A-Za-z0-9._%+-]+@[A-Za-z0-9.-]+\.[A-Za-z]{2,4}$">
                            </td>
                            <td> Tipo de vivienda<br>
                                <select id="tipo_viv_nat" name="form.solicitante.basica.tipo_viv" style="width:10em;" required>
                                    <%
                                        combo = totalCombos.get("tipo_vivienda").getAsJsonArray();
                                        for (int i = 0; i < combo.size(); i++) {
                                            objeto = combo.get(i).getAsJsonObject();
                                    %>
                                    <option value="<%= objeto.get("codigo").getAsString()%>"  
                                            <%=(solicitante != null && solicitante.getAsJsonObject("basica").get("tipo_vivienda").getAsString().equalsIgnoreCase(objeto.get("codigo").getAsString())) ? "selected" : ""%> >
                                        <%= objeto.get("valor").getAsString()%>
                                    </option>
                                    <%  }%>
                                </select>
                            </td>
                            <td> Tiempo residencia<br> 
                                <input id="an_res_nat" name="form.solicitante.basica.an_res" size="5" onkeyup="soloNumeros(this.id);" type="number" required min="0"
                                       value="<%=solicitante != null ? solicitante.getAsJsonObject("basica").get("anos_residencia").getAsString() : "0"%>"> A�os
                                <input id="mes_res_nat" name="form.solicitante.basica.mes_res" size="5" onkeyup="soloNumeros(this.id);" type="number" required min="0"
                                       value="<%=solicitante != null ? solicitante.getAsJsonObject("basica").get("meses_residencia").getAsString() : "0"%>"> Meses
                            </td>

                            <td> Telefono<br>
                                <input maxlength="7" id="tel_nat" name="form.solicitante.basica.tel" required 
                                       value="<%=solicitante != null ? solicitante.getAsJsonObject("basica").get("telefono").getAsString() : ""%>" 
                                       size="15" maxlength="15" onkeyup="soloNumeros(this.id);" type="text">
                            </td>
                            <td class="filableach"> Celular<br>
                                <input maxlength="15" id="cel_nat" name="form.solicitante.basica.cel" size="15" onkeyup="soloNumeros(this.id);" type="text"
                                       value="<%=solicitante != null ? solicitante.getAsJsonObject("basica").get("celular").getAsString() : ""%>" >
                            </td>

                        </tr>
                        
                        <!-- XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXx -->
                        
                        <tr class="filaazul">
                            <td> Identificacion Conyuge<br>
                                <select id="tipo_id_nat_conyuge" name="form.solicitante.basica.tipo_id_conyuge" required>
                                    <%
                                        combo = totalCombos.get("tipo_id").getAsJsonArray();
                                        for (int i = 0; i < combo.size(); i++) {
                                            objeto = combo.get(i).getAsJsonObject();
                                    %>
                                    <option value="<%= objeto.get("codigo").getAsString()%>"  
                                            <%=(solicitante != null && solicitante.get("basica").getAsJsonObject().get("tipo_id").getAsString().equalsIgnoreCase(objeto.get("codigo").getAsString())) ? "selected" : ""%> >
                                        <%= objeto.get("valor").getAsString()%>
                                    </option>
                                    <%  }%>
                                </select>&nbsp;&nbsp;&nbsp;
                                <input maxlength="15" id="id_nat_conyuge" onkeypress="return isNumberKey(event)" name="form.solicitante.basica.id_conyuge" 
                                       value="<%=solicitante != null ? solicitante.getAsJsonObject("basica").get("identificacion").getAsString() : ""%>" 
                                       size="20" type="text" required <%=(!nuevo) ? " disabled " : " "%>>
                            </td>
                            <td width="17%"> Primer apellido Conyuge<br>
                                <input id="pr_apellido_nat_conyuge" name="form.solicitante.basica.pr_apellido_conyuge" required <%=(!nuevo) ? " disabled " : " "%>
                                       value="<%=solicitante != null ? solicitante.getAsJsonObject("basica").get("primer_apellido").getAsString() : ""%>" 
                                       size="25" maxlength="25" onchange="conMayusculas(this)" onkeypress="return soloLetras(event)" type="text">
                            </td>
                            <td class="filableach" width="16%"> Segundo Apellido Conyuge<br>
                                <input id="seg_apellido_nat_conyuge" name="form.solicitante.basica.seg_apellido_conyuge" required
                                       value="<%=solicitante != null ? solicitante.getAsJsonObject("basica").get("segundo_apellido").getAsString() : ""%>" 
                                       size="25" maxlength="25" onchange="conMayusculas(this)" onkeypress="return soloLetras(event)" type="text">
                            </td>
                            <td width="17%">Nombre Conyuge<br>
                                <input id="pr_nombre_nat_conyuge" name="form.solicitante.basica.pr_nombre_conyuge" required <%=(!nuevo) ? " disabled " : " "%>
                                       value="<%=solicitante != null ? solicitante.getAsJsonObject("basica").get("primer_nombre").getAsString() : ""%>"
                                       size="25" maxlength="25" onchange="conMayusculas(this)" onkeypress="return soloLetras(event)" type="text">
                            </td>
                            <td> Telefono Conyuge<br>
                                <input maxlength="7" id="tel_nat_conyuge" name="form.solicitante.basica.tel_conyuge" required 
                                       value="<%=solicitante != null ? solicitante.getAsJsonObject("basica").get("telefono").getAsString() : ""%>" 
                                       size="15" maxlength="15" onkeyup="soloNumeros(this.id);" type="text">
                            </td>
                            <td class="filableach"> Celular Conyuge<br>
                                <input maxlength="15" id="cel_nat_conyuge" name="form.solicitante.basica.cel_conyuge" size="15" onkeyup="soloNumeros(this.id);" type="text"
                                       value="<%=solicitante != null ? solicitante.getAsJsonObject("basica").get("celular").getAsString() : ""%>" >
                            </td>
                        </tr>
                        
                        <!-- XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXx -->
                        
                    </tbody>
                </table>
                <br>
                
                
                <b>OBLIGACIONES A RECOGER</b>
                <table id="tfinanat" style="border-collapse:collapse; width:100%" border="1">
                    <tbody><%
                
                    for (int i = 1; i < 4; i++) { %>

                        <tr class="filaazul">
                            <td width="33%" >
                                Entidad<br>
                                <select id="entidad<%=i%>" name="form.solicitante.obligaciones[<%=i%>].entidad" style="width:20em;" required onchange="cargarDetalleObligaciones(this.value, <%=i%>);"><%
                                    //combo = totalCombos.get("obligaciones_recoger").getAsJsonArray();
                                    combo = accion.buscarEntidadesComprar("obligaciones_recoger").getAsJsonArray();
                                    
                                    for ( int j = 0; j < combo.size(); j++ ) {
                                        objeto = combo.get(j).getAsJsonObject(); %>
                                        <option value="<%= objeto.get("codigo").getAsString()%>"><%= objeto.get("valor").getAsString()%></option><%  
                                    }%>
                                </select>                                

                                <!-- <input id="act_econ_nat" type="hidden" name="form.solicitante.laboral.act_econ" value="EPLDO"/> -->
                            </td>
                            <td>
                                Nit<br>
                                <input type="text" id="nit_prov<%=i%>" name="form.solicitante.obligaciones[<%=i%>].nit_prov" value="" readonly="" required>
                            </td>

                            <td>
                                Tipo de Cuenta<br>
                                <input type="text" id="tcuenta_prov<%=i%>" name="form.solicitante.obligaciones[<%=i%>].tcuenta_prov" value="" readonly="" required>
                            </td>
                            <td>
                                No de Cuenta<br>
                                <input type="text" id="cuenta_prov<%=i%>" name="form.solicitante.obligaciones[<%=i%>].cuenta_prov" value="" size="25" readonly="" required>
                            </td>
                            <td>
                                Valor a Recoger<br>
                                <input type="text" id="valor_recoger<%=i%>" name="form.solicitante.obligaciones[<%=i%>].valor_recoger" value="0" required>
                            </td>
                        </tr><%

                    } %>
                    </tbody>
                </table>
                <br>                
                
                <!-- <b>Informaci�n laboral y econ�mica</b> -->
                <!--
                <table id="tlaboralnat" style="border-collapse:collapse; width:100%" border="1">
                    <tbody><tr class="filaazul">
                            <td width="17%">  Actividad economica<br>
                                <input value="EMPLEADO" disabled="" type="text"/>
                                <input id="act_econ_nat" type="hidden" name="form.solicitante.laboral.act_econ" value="EPLDO"/>
                                <input id="ocup_nat" type="hidden" name="form.solicitante.laboral.ocup" value=""/>
                            </td>
                            <td width="17%"> Cargo<br>
                                <input id="car_emp_nat" name="form.solicitante.laboral.car_emp"  required
                                       value="<%=solicitante != null ? solicitante.getAsJsonObject("laboral").get("cargo").getAsString() : ""%>" size="30" type="text">
                            </td>
                            <td class="filableach" width="17%">  Nit/Rut (para independientes)<br>
                                <input id="nit_emp_nat" name="form.solicitante.laboral.nit_emp" size="20" disabled type="text" maxlength="13" required
                                       value="<%=solicitante != null ? solicitante.getAsJsonObject("laboral").get("nit").getAsString() : ""%>">
                                <input id="dig_nit_emp_nat" name="form.solicitante.laboral.dig_nit_emp" size="2" disabled type="text" maxlength="2"
                                       value="<%=solicitante != null ? solicitante.getAsJsonObject("laboral").get("digito_verificacion").getAsString() : ""%>">
                            </td>
                            <td colspan="3"> Nombre de la empresa<br>
                                <input id="nom_emp_nat" name="form.solicitante.laboral.nom_emp" size="60" type="text" disabled maxlength="150" required
                                       value="<%=solicitante != null ? solicitante.getAsJsonObject("laboral").get("nombre_empresa").getAsString() : ""%>">
                            </td>

                        </tr>
                        <tr class="filaazul">
                            <td> Fecha de ingreso<br>
                                <input class="validate-date" id="f_ing_nat" name="form.solicitante.laboral.f_ing" size="15" maxlength="15" type="text" required
                                       value="<%=solicitante != null ? solicitante.getAsJsonObject("laboral").get("fecha_ingreso").getAsString() : ""%>" readonly>
                                <img src="/fintra/images/cal.gif" id="imgFecIng_nat" alt="fecha" title="Seleccion de fecha" />
                                <script type="text/javascript">
                                    var f = document.getElementById("f_ing_nat");
                                    if (f.value === '0099-01-01')
                                        f.value = '';
                                    Calendar.setup({
                                        inputField: "f_ing_nat",
                                        trigger: "imgFecIng_nat",
                                        align: "top",
                                        max: Calendar.dateToInt(new Date()),
                                        onSelect: function () {
                                            this.hide();
                                        }
                                    });
                                </script> 
                            </td>
                            <td> Tipo de contrato<br>
                                <select id="tipo_cont_nat" name="form.solicitante.laboral.tipo_cont" style="width:10em;" required>
                                    <%
                                        combo = totalCombos.get("tipo_contrato").getAsJsonArray();
                                        for (int i = 0; i < combo.size(); i++) {
                                            objeto = combo.get(i).getAsJsonObject();
                                    %>
                                    <option value="<%= objeto.get("codigo").getAsString()%>"  
                                            <%=(solicitante != null && solicitante.getAsJsonObject("laboral").get("tipo_contrato").getAsString().equalsIgnoreCase(objeto.get("codigo").getAsString())) ? "selected" : ""%> >
                                        <%= objeto.get("valor").getAsString()%>
                                    </option>
                                    <%  }%>  
                                </select>
                            </td>
                            <td class="filableach">
                                Eps &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Afiliacion<br>
                                <select id="eps_nat" name="form.solicitante.laboral.eps" style="width:15em;" required>
                                    <%
                                        combo = totalCombos.get("eps").getAsJsonArray();
                                        for (int i = 0; i < combo.size(); i++) {
                                            objeto = combo.get(i).getAsJsonObject();
                                    %>
                                    <option value="<%= objeto.get("codigo").getAsString()%>"  
                                            <%=(solicitante != null && solicitante.getAsJsonObject("laboral").get("eps").getAsString().equalsIgnoreCase(objeto.get("codigo").getAsString())) ? "selected" : ""%> >
                                        <%= objeto.get("valor").getAsString()%>
                                    </option>
                                    <%  }%> 
                                </select>
                                <select id="tip_afil_nat" name="form.solicitante.laboral.tip_afil" required>
                                    <%
                                        combo = totalCombos.get("tipo_afiliacion").getAsJsonArray();
                                        for (int i = 0; i < combo.size(); i++) {
                                            objeto = combo.get(i).getAsJsonObject();
                                    %>
                                    <option value="<%= objeto.get("codigo").getAsString()%>"  
                                            <%=(solicitante != null && solicitante.getAsJsonObject("laboral").get("tipo_afiliacion").getAsString().equalsIgnoreCase(objeto.get("codigo").getAsString())) ? "selected" : ""%> >
                                        <%= objeto.get("valor").getAsString()%>
                                    </option>
                                    <%  }%>  
                                </select>
                            </td>
                            <td>
                                Telefono &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Extension<br>
                                <input maxlength="7" id="tel_emp_nat" name="form.solicitante.laboral.tel_emp" size="15" onkeyup="soloNumeros(this.id);" type="text"
                                       value="<%=solicitante != null ? solicitante.getAsJsonObject("laboral").get("telefono").getAsString() : ""%>" required>
                                <input maxlength="4" id="ext_emp_nat" name="form.solicitante.laboral.ext_emp" size="5" onkeyup="soloNumeros(this.id);" type="text"
                                       value="<%=solicitante != null ? solicitante.getAsJsonObject("laboral").get("extension").getAsString() : ""%>">
                            </td>
                            <td class="filableach">  Celular<br>
                                <input maxlength="10" id="cel_emp_nat" name="form.solicitante.laboral.cel_emp" size="15" onkeyup="soloNumeros(this.id);" type="text"
                                       value="<%=solicitante != null ? solicitante.getAsJsonObject("laboral").get("celular").getAsString() : ""%>">
                            </td>
                            <td class="filableach">  E-mail<br>
                                <input id="mail_empr_nat" name="form.solicitante.laboral.mail_empr" size="30" maxlength="100" type="email" pattern="^[A-Za-z0-9._%+-]+@[A-Za-z0-9.-]+\.[A-Za-z]{2,4}$"
                                       value="<%=solicitante != null ? solicitante.getAsJsonObject("laboral").get("email").getAsString() : ""%>">
                            </td>
                        </tr>
                        <tr class="filaazul">
                            <td colspan="2"> Direccion<br>
                                <label class="ui-icon ui-icon-note" style="float: left; margin-right: 4px;" onclick="genDireccion('dir_emp_nat');"></label>
                                <input id="dir_emp_nat" name="form.solicitante.laboral.dir_emp" maxlength="100" size="50" type="text" required readonly
                                       value="<%=solicitante != null ? solicitante.getAsJsonObject("laboral").get("direccion").getAsString() : ""%>">
                            </td>
                            <td> Departamento<br>
                                <select id="dep_emp_nat" name="form.solicitante.laboral.dep_emp" required style="width:20em;" onchange="cargarCiudades(this.value, 'ciu_emp_nat', '');">
                                    <%
                                        combo = totalCombos.get("departamentos").getAsJsonArray();
                                        for (int i = 0; i < combo.size(); i++) {
                                            objeto = combo.get(i).getAsJsonObject();
                                    %>
                                    <option value="<%= objeto.get("codigo").getAsString()%>"  
                                            <%=(solicitante != null && solicitante.getAsJsonObject("laboral").get("departamento").getAsString().equalsIgnoreCase(objeto.get("codigo").getAsString())) ? "selected" : ""%> >
                                        <%= objeto.get("valor").getAsString()%>
                                    </option>
                                    <%  }%>
                                </select>
                            </td>
                            <td>  Ciudad <br>
                                <div id="d_ciu_emp_nat">
                                    <select id="ciu_emp_nat" name="form.solicitante.laboral.ciu_emp" required style="width:20em;">
                                        <%
                                            combo = accion.buscarCiudades(solicitante != null ? solicitante.getAsJsonObject("laboral").get("departamento").getAsString() : "");
                                            for (int i = 0; i < combo.size(); i++) {
                                                objeto = combo.get(i).getAsJsonObject();
                                        %>
                                        <option value="<%= objeto.get("codigo").getAsString()%>"  
                                                <%=(solicitante != null && solicitante.getAsJsonObject("laboral").get("ciudad").getAsString().equalsIgnoreCase(objeto.get("codigo").getAsString())) ? "selected" : ""%> >
                                            <%= objeto.get("valor").getAsString()%>
                                        </option>
                                        <%  }%>
                                    </select>
                                </div>
                            </td>

                            <td colspan="2"> Direccion de Cobro<br>
                                <label class="ui-icon ui-icon-note" style="float: left; margin-right: 4px;" onclick="genDireccion('dir_cob_nat');"></label>
                                <input id="dir_cob_nat" name="form.solicitante.laboral.dir_cob" size="50" required maxlength="100" type="text" readonly
                                       value="<%=solicitante != null ? solicitante.getAsJsonObject("laboral").get("direccion_cobro").getAsString() : ""%>">
                            </td>
                        </tr>
                    <input id="sal_nat" name="form.solicitante.laboral.sal" value="0" type="hidden">
                    <input id="otros_nat" name="form.solicitante.laboral.otros" value="0" type="hidden">
                    <input id="conc_otros_nat" name="form.solicitante.laboral.conc_otros" value="" type="hidden">
                    <input id="manuten_nat" name="form.solicitante.laboral.manuten" value="0" type="hidden">
                    <input id="cred_nat" name="form.solicitante.laboral.cred" value="0" type="hidden">
                    <input id="arr_nat" name="form.solicitante.laboral.arr" value="0" type="hidden">

                    </tbody></table>
                <br>
                -->
                
                <b>INFORMACION DE ACTIVOS</b>
                <table id="tbienesveh" style="border-collapse:collapse; width:100%" border="1">
                    <%
                
                    for (int i = 1; i < 2; i++) {
                        
                        SolicitudBienes b_bien;
                        if (lista_bienes != null && lista_bienes.size() > i-1) {
                            b_bien = lista_bienes.get(i-1);
                        } else {
                            b_bien = null;
                        }%>
                        
                        <tr class="filaazul">
                            <td width="17%">
                                Tipo de bien<br>
                                <select id="tipo_bien<%= i%>_nat" name="form.solicitante.activos[<%=i%>].tipo_bien" style="width:10em;" required> <!-- name="tipo_bien<%= i%>_nat" -->
                                    <option value="">...</option><%
                                    dato1 = null;
                                    for (int j = 0; j < tipo_bien.size(); j++) {
                                        dato1 = ((String) tipo_bien.get(j)).split(";_;");%>
                                        <option value="<%=dato1[0]%>" <%=(dato1[0].equals((vista.equals("0") || b_bien == null) ? " " : b_bien.getTipoBien())) ? "selected" : ""%> ><%=dato1[1]%></option> <%
                                      }%>
                                </select>
                            </td>
                            <td colspan="2">
                                Hipoteca &nbsp&nbsp A favor de
                                <br>
                                <select id="hipoteca<%= i%>" name="form.solicitante.activos[<%=i%>].hipoteca" required > <!-- name="hipoteca<%= i%>" -->
                                    <option value="">...</option>
                                    <option value="S" <%= (vista.equals("0") || b_bien == null) ? "" : b_bien.getHipoteca().equals("S") ? "selected" : ""%>>Si</option>
                                    <option value="N" <%= (vista.equals("0") || b_bien == null) ? "" : b_bien.getHipoteca().equals("N") ? "selected" : ""%>>No</option>
                                </select>
                                <input type="text" id="favor_de<%= i%>_nat" name="form.solicitante.activos[<%=i%>].favorde" value="<%=(vista.equals("0") || b_bien == null) ? "" : b_bien.getaFavorDe()%>" size="50" required> <!-- name="favor_de<%= i%>_nat" -->
                            </td>
                            <td width="17%">
                                Valor comercial<br>
                                $ <input type="text" id="valor_com<%= i%>_nat" name="form.solicitante.activos[<%=i%>].valor_com" onkeyup="soloNumeros(this.id);" value="<%= (vista.equals("0") || b_bien == null) ? "" : Util.customFormat(Double.parseDouble(b_bien.getValorComercial()))%>" size="25" onblur="formato(this, 0)" required> <!-- name="valor_com<%= i%>_nat" -->
                            </td>
                            <td colspan="2">
                                Direccion<br>
                                <table width="100%">
                                    <tr>
                                        <td width="90%">
                                            <input type="text" id="dir_bien<%= i%>_nat" name="form.solicitante.activos[<%=i%>].dir_bien" <%=estadoDireccion%> value="<%= (vista.equals("0") || b_bien == null) ? "" : b_bien.getDireccion()%>" size="60" required> <!-- name="dir_bien<%= i%>_nat" -->
                                        </td>
                                        <td>                      
                                            <img src="/fintra/images/Direcciones.png" width="26" height="23" border="0" align="middle" onclick="genDireccion('dir_bien<%= i%>_nat',event);" alt="Direcciones"  title="Direcciones" />
                                        </td>    
                                    </tr>
                                </table>  
                            </td>
                        </tr><%

                    }
                    
                    for (int i = 1; i < 3; i++) {
                        
                        SolicitudVehiculo b_veh = null;
                        if (lista_vehiculos != null && lista_vehiculos.size() > i-1) {
                            b_veh = lista_vehiculos.get(i-1);
                        } %>
                        
                        <tr class="filaazul">
                            <td width="17%">
                                Marca del vehiculo<br>
                                <input type="text" id="marca_veh<%= i%>_nat" name="form.solicitante.activos[<%=i%>].marca_veh" value="<%=(vista.equals("0") || b_veh == null) ? "" : b_veh.getMarca()%>" size="25"  > <!-- name="marca_veh<%= i%>_nat" -->
                            </td>
                            <td width="16%">
                                Tipo de vehiculo<br>
                                <select id="tvehiculo<%= i%>_nat" name="form.solicitante.activos[<%=i%>].marca_veh" style="width:10em;"> <!-- name="tvehiculo<%= i%>_nat" -->
                                    <option value="">...</option> <%
                                    dato1 = null;
                                    for (int j = 0; j < tipo_vehiculo.size(); j++) {
                                        dato1 = ((String) tipo_vehiculo.get(j)).split(";_;");%>
                                        <option value="<%=dato1[0]%>" <%=(dato1[0].equals((vista.equals("0") || b_veh == null) ? " " : b_veh.getTipoVehiculo())) ? "selected" : ""%> ><%=dato1[1]%></option> <%
                                    }%>
                                </select>
                            </td>
                            <td width="17%">
                                Placa &nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp Modelo <br>
                                <input type="text" id="placa_veh<%= i%>_nat" name="form.solicitante.activos[<%=i%>].placa_veh" value="<%=(vista.equals("0") || b_veh == null) ? "" : b_veh.getPlaca()%>" size="5"> <!--  name="placa_veh<%= i%>_nat" -->
                                <input type="text" id="modelo_veh<%= i%>_nat" name="form.solicitante.activos[<%=i%>].modelo_veh" value="<%=(vista.equals("0") || b_veh == null) ? "" : b_veh.getModelo()%>" size="5"> <!-- name="modelo_veh<%= i%>_nat" -->
                            </td>
                            <td width="17%">
                                Valor comercial<br>
                                $ <input type="text" id="valor_veh<%= i%>_nat" name="form.solicitante.activos[<%=i%>].valor_veh" onkeyup="soloNumeros(this.id);" value="<%=(vista.equals("0") || b_veh == null) ? "0" : Util.customFormat(Double.parseDouble(b_veh.getValorComercial()))%>" size="25" onblur="formato(this, 0)"> <!-- name="valor_veh<%= i%>_nat" -->
                            </td>
                            <td width="16%">
                                Cuota mensual<br>
                                $ <input type="text" id="cuota_veh<%= i%>_nat" name="form.solicitante.activos[<%=i%>].cuota_veh" onkeyup="soloNumeros(this.id);" value="<%= (vista.equals("0") || b_veh == null) ? "0" : Util.customFormat(Double.parseDouble(b_veh.getCuotaMensual()))%>" size="25" onblur="formato(this, 0)"> <!-- name="cuota_veh<%= i%>_nat" -->
                            </td>
                            <td width="17%">
                                Pignorado a favor de<br>
                                <input type="text" id="pign_veh<%= i%>_nat" name="form.solicitante.activos[<%=i%>].pign_veh" value="<%=(vista.equals("0") || b_veh == null) ? "" : b_veh.getPignoradoAFavorDe()%>" size="30"> <!-- name="pign_veh<%= i%>_nat" -->
                            </td>
                        </tr><%
                    } %>

                </table>
                <br>                
                
                <!-- <b>Referencias personales</b> -->
                <table id="treferenciapersnat" style="border-collapse:collapse; width:100%" border="1">
                    <tbody><%
                         
                        int indice = 0, j = 0;
                        JsonArray refers = solicitante != null && solicitante.getAsJsonArray("referencias_personales").size() > 0 ? solicitante.getAsJsonArray("referencias_personales") : null;
                        //solicitante != null ? solicitante.getAsJsonObject("finanza").get("total_patrimonio").getAsString() : "0";
                        do { %>
                            <!--
                            <input type="hidden" name="form.solicitante.referencia[<%=indice%>].tipo_referencia" value="P"/>
                            <tr class="filaazul">
                                <td width="17%"> Primer Apellido<br>
                                    <input id="pr_apellido_refp_nat<%=j%>" name="form.solicitante.referencia[<%=indice%>].pr_apellido" size="25" onkeypress="return soloLetras(event)" type="text"
                                           value="<%=refers != null ? refers.get(j).getAsJsonObject().get("primer_apellido").getAsString() : ""%>" required>
                                </td>
                                <td class="filableach" width="16%"> Segundo Apellido<br>
                                    <input id="seg_apellido_refp_nat<%=j%>" name="form.solicitante.referencia[<%=indice%>].seg_apellido" size="25" onkeypress="return soloLetras(event)" type="text"
                                           value="<%=refers != null ? refers.get(j).getAsJsonObject().get("segundo_apellido").getAsString() : ""%>" required >
                                </td>
                                <td width="17%"> Primer Nombre<br>
                                    <input id="pr_nombre_refp_nat<%=j%>" name="form.solicitante.referencia[<%=indice%>].pr_nombre" size="25" onkeypress="return soloLetras(event)" type="text"
                                           value="<%=refers != null ? refers.get(j).getAsJsonObject().get("primer_nombre").getAsString() : ""%>"  required>
                                </td>
                                <td class="filableach" width="17%"> Segundo Nombre<br>
                                    <input id="seg_nombre_refp_nat<%=j%>" name="form.solicitante.referencia[<%=indice%>].seg_nombre" size="25" onkeypress="return soloLetras(event)" type="text"
                                           value="<%=refers != null ? refers.get(j).getAsJsonObject().get("segundo_nombre").getAsString() : ""%>" >
                                </td>
                                <td class="filableach" width="16%"> Tiempo de conocido<br>
                                    <input id="tconocido_refp_nat<%=j%>" maxlength="2" name="form.solicitante.referencia[<%=indice%>].tconocido" size="15" onkeyup="soloNumeros(this.id);" type="text"
                                           value="<%=refers != null ? refers.get(j).getAsJsonObject().get("tiempo_conocido").getAsString() : ""%>" required > A�os
                                </td>
                                <td class="filableach" width="17%"> Celular<br>
                                    <input maxlength="10" id="cel_refp_nat<%=j%>" name="form.solicitante.referencia[<%=indice%>].cel" size="15" onkeyup="soloNumeros(this.id);" type="text"
                                           value="<%=refers != null ? refers.get(j).getAsJsonObject().get("celular").getAsString() : ""%>"  >
                                </td>
                            </tr>
                            <tr class="filaazul">
                                <td> Telefono 1<br>
                                    <input maxlength="10" id="tel1_refp_nat<%=j%>" name="form.solicitante.referencia[<%=indice%>].tel1" size="15" onkeyup="soloNumeros(this.id);" type="text"
                                           value="<%=refers != null ? refers.get(j).getAsJsonObject().get("telefono").getAsString() : ""%>" required>
                                </td>
                                <td class="filableach"> Telefono 2 &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;  Extension <br>
                                    <input maxlength="7" id="tel2_refp_nat<%=j%>" name="form.solicitante.referencia[<%=indice%>].tel2" size="15" onkeyup="soloNumeros(this.id);" type="text"
                                           value="<%=refers != null ? refers.get(j).getAsJsonObject().get("telefono_2").getAsString() : ""%>"  >
                                    <input maxlength="4" id="ext_refp_nat<%=j%>" name="form.solicitante.referencia[<%=indice%>].ext" size="5" onkeyup="soloNumeros(this.id);" type="text"
                                           value="<%=refers != null ? refers.get(j).getAsJsonObject().get("extension").getAsString() : ""%>"  >
                                </td>
                                <td> Departamento<br>
                                    <select id="dep_refp_nat<%=j%>" name="form.solicitante.referencia[<%=indice%>].dep" style="width:20em;" required onchange="cargarCiudades(this.value, 'ciu_refp_nat<%=indice%>', '');">
                                        <%
                                            combo = totalCombos.get("departamentos").getAsJsonArray();
                                            for (int i = 0; i < combo.size(); i++) {
                                                objeto = combo.get(i).getAsJsonObject();
                                        %>
                                        <option value="<%= objeto.get("codigo").getAsString()%>"  
                                                <%=(refers != null && refers.get(j).getAsJsonObject().get("departamento").getAsString().equalsIgnoreCase(objeto.get("codigo").getAsString())) ? "selected" : ""%> >
                                            <%= objeto.get("valor").getAsString()%>
                                        </option>
                                        <%  }%>
                                    </select>
                                </td>
                                <td> Ciudad<br>
                                    <div id="d_ciu_refp_nat<%=j%>">
                                        <select id="ciu_refp_nat<%=j%>" name="form.solicitante.referencia[<%=indice%>].ciu" style="width:20em;" required>
                                            <%
                                                combo = accion.buscarCiudades(refers != null ? refers.get(j).getAsJsonObject().get("departamento").getAsString() : "");
                                                for (int i = 0; i < combo.size(); i++) {
                                                    objeto = combo.get(i).getAsJsonObject();
                                            %>
                                            <option value="<%= objeto.get("codigo").getAsString()%>"  
                                                    <%=(refers != null && refers.get(j).getAsJsonObject().get("ciudad").getAsString().equalsIgnoreCase(objeto.get("codigo").getAsString())) ? "selected" : ""%> >
                                                <%= objeto.get("valor").getAsString()%>
                                            </option>
                                            <%  }%>
                                        </select>
                                    </div>
                                </td>
                                <td class="filableach" colspan="2">  E-mail<br>
                                    <input id="mail_refp_nat<%=j%>" name="form.solicitante.referencia[<%=indice%>].mail" size="60" 
                                           type="email" pattern="^[A-Za-z0-9._%+-]+@[A-Za-z0-9.-]+\.[A-Za-z]{2,4}$" 
                                           value="<%=refers != null ? refers.get(j).getAsJsonObject().get("email").getAsString() : ""%>" >
                                </td>
                            </tr> -->
                                            
                            <% indice++;
                            j++;
                        } while (solicitante != null && (refers != null && j < refers.size()));%>
                    </tbody>
                </table>
                <!-- <br> -->
                
                <b>REFERENCIAS (Que no vivan con el titular)</b> <!-- <b>Referencias familiares</b> -->
                <table id="treferenciafamnat" style="border-collapse:collapse; width:100%" border="1">  
                    <tbody><%
                        
                        //int indice = 0, j = 0;
                        //JsonArray refers = solicitante != null && solicitante.getAsJsonArray("referencias_personales").size() > 0 ? solicitante.getAsJsonArray("referencias_personales") : null;

                        j = 0;
                        refers = solicitante != null && solicitante.getAsJsonArray("referencias_familiares").size() > 0 ? solicitante.getAsJsonArray("referencias_familiares") : null;
                        do { %>
                        
                            <input type="hidden" name="form.solicitante.referencia[<%=indice%>].tipo_referencia" value="F"/>
                    
                            <tr class="filaazul">
                                <td width="17%"> Primer Apellido<br>
                                    <input id="pr_apellido_refam_nat<%=j%>" name="form.solicitante.referencia[<%=indice%>].pr_apellido" size="25" onkeypress="return soloLetras(event)" type="text"
                                           value="<%=refers != null ? refers.get(j).getAsJsonObject().get("primer_apellido").getAsString() : ""%>" required>
                                </td>
                                <td class="filableach" width="16%"> Segundo Apellido<br>
                                    <input id="seg_apellido_refam_nat<%=j%>" name="form.solicitante.referencia[<%=indice%>].seg_apellido" size="25" onkeypress="return soloLetras(event)" type="text"
                                           value="<%=refers != null ? refers.get(j).getAsJsonObject().get("segundo_apellido").getAsString() : ""%>" required>
                                </td>
                                <td width="17%">  Primer Nombre<br>
                                    <input id="pr_nombre_refam_nat<%=j%>" name="form.solicitante.referencia[<%=indice%>].pr_nombre" size="25" onkeypress="return soloLetras(event)" type="text"
                                           value="<%=refers != null ? refers.get(j).getAsJsonObject().get("primer_nombre").getAsString() : ""%>" required>
                                </td>
                                <td class="filableach" width="17%"> Segundo Nombre<br>
                                    <input id="seg_nombre_refam_nat<%=j%>" name="form.solicitante.referencia[<%=indice%>].seg_nombre" size="25" type="text"
                                           value="<%=refers != null ? refers.get(j).getAsJsonObject().get("segundo_nombre").getAsString() : ""%>">
                                </td>
                                <td width="16%"> Parentesco<br>
                                    <select id="parent_refam_nat<%=j%>" name="form.solicitante.referencia[<%=indice%>].parent" style="width:20em;" required>
                                        <%
                                            combo = totalCombos.get("parentesco").getAsJsonArray();
                                            for (int i = 0; i < combo.size(); i++) {
                                                objeto = combo.get(i).getAsJsonObject();
                                        %>
                                        <option value="<%= objeto.get("codigo").getAsString()%>"  
                                                <%=(refers != null && refers.get(j).getAsJsonObject().get("parentesco").getAsString().equalsIgnoreCase(objeto.get("codigo").getAsString())) ? "selected" : ""%> >
                                            <%= objeto.get("valor").getAsString()%>
                                        </option>
                                        <%  }%>
                                    </select>
                                </td>

                                <!--
                                <td class="filableach" width="17%"> Celular<br>
                                    <input maxlength="10" id="cel_refam_nat<%=j%>" name="form.solicitante.referencia[<%=indice%>].cel" size="15" onkeyup="soloNumeros(this.id);" type="text"
                                           value="<%=refers != null ? refers.get(j).getAsJsonObject().get("celular").getAsString() : ""%>">
                                </td> -->
                            </tr>
                            
                            <tr class="filaazul">
                                <td> Telefono <br>
                                    <input maxlength="10" id="tel1_refam_nat<%=j%>" name="form.solicitante.referencia[<%=indice%>].tel1" size="15" onkeyup="soloNumeros(this.id);" type="text"
                                           value="<%=refers != null ? refers.get(j).getAsJsonObject().get("telefono").getAsString() : ""%>" required>
                                </td>
                                <!--
                                <td class="filableach"> Telefono 2 &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;  Extension <br>
                                    <input maxlength="7" id="tel2_refam_nat<%=j%>" name="form.solicitante.referencia[<%=indice%>].tel2" size="15" onkeyup="soloNumeros(this.id);" type="text"
                                           value="<%=refers != null ? refers.get(j).getAsJsonObject().get("telefono_2").getAsString() : ""%>" >
                                    <input maxlength="4" id="ext_refam_nat<%=j%>" name="form.solicitante.referencia[<%=indice%>].ext" size="5" onkeyup="soloNumeros(this.id);" type="text"
                                           value="<%=refers != null ? refers.get(j).getAsJsonObject().get("extension").getAsString() : ""%>" >
                                </td> -->
                                <td class="filableach" width="17%"> Celular<br>
                                    <input maxlength="10" id="cel_refam_nat<%=j%>" name="form.solicitante.referencia[<%=indice%>].cel" size="15" onkeyup="soloNumeros(this.id);" type="text"
                                           value="<%=refers != null ? refers.get(j).getAsJsonObject().get("celular").getAsString() : ""%>">
                                </td>
                                <td> Departamento<br>
                                    <select id="dep_refam_nat<%=j%>" name="form.solicitante.referencia[<%=indice%>].dep" style="width:20em;" required onchange="cargarCiudades(this.value, 'ciu_refam_nat<%=j%>', '');">
                                        <%
                                            combo = totalCombos.get("departamentos").getAsJsonArray();
                                            for (int i = 0; i < combo.size(); i++) {
                                                objeto = combo.get(i).getAsJsonObject();
                                        %>
                                        <option value="<%= objeto.get("codigo").getAsString()%>"  
                                                <%=(refers != null && refers.get(j).getAsJsonObject().get("departamento").getAsString().equalsIgnoreCase(objeto.get("codigo").getAsString())) ? "selected" : ""%> >
                                            <%= objeto.get("valor").getAsString()%>
                                        </option>
                                        <%  }%>
                                    </select>
                                </td>
                                <td> Ciudad<br>
                                    <div id="d_ciu_refam_nat<%=j%>">
                                        <select id="ciu_refam_nat<%=j%>" name="form.solicitante.referencia[<%=indice%>].ciu" style="width:20em;" required>
                                            <%
                                                combo = accion.buscarCiudades(refers != null ? refers.get(j).getAsJsonObject().get("departamento").getAsString() : "");
                                                for (int i = 0; i < combo.size(); i++) {
                                                    objeto = combo.get(i).getAsJsonObject();
                                            %>
                                            <option value="<%= objeto.get("codigo").getAsString()%>"  
                                                    <%=(refers != null && refers.get(j).getAsJsonObject().get("ciudad").getAsString().equalsIgnoreCase(objeto.get("codigo").getAsString())) ? "selected" : ""%> >
                                                <%= objeto.get("valor").getAsString()%>
                                            </option>
                                            <%  }%>           
                                        </select>
                                    </div>
                                </td>
                                <td class="filableach" colspan="2"> E-mail<br>
                                    <input id="mail_refam_nat<%=j%>" name="form.solicitante.referencia[<%=indice%>].mail" size="30" type="email" pattern="^[A-Za-z0-9._%+-]+@[A-Za-z0-9.-]+\.[A-Za-z]{2,4}$"
                                    value="<%=refers != null ? refers.get(j).getAsJsonObject().get("email").getAsString() : ""%>" >
                                </td>
                            </tr>
                    
                            <% indice++;
                            j++;
                        } while (solicitante != null && (refers != null && j < refers.size()));%>
                    </tbody>
                </table>
                <br>                

                <b>INFORMACION FINANCIERA</b>
                <table id="tfinanat" style="border-collapse:collapse; width:100%" border="1">
                    <tbody>
                        <tr class="filaazul">
                            <td> Salario / Mesada / Ingreso mes <br> &nbsp; $ &nbsp;
                                <input id="sal_fin_nat" name="form.solicitante.financiera.salario" size="25" onkeyup="soloNumeros(this.id);"
                                       value="<%=solicitante != null ? solicitante.getAsJsonObject("finanza").get("salario").getAsString() : "0"%>" 
                                       pattern="^(((\d{1,3})(,\d{3})*)|(\d+))(.\d+)?$" type="numeric" onblur="calcularTotales(); formato(this, 0);" >
                            </td>
                            <td> Descuento de nomina mensual <br> &nbsp; $ &nbsp;
                                <input id="des_fin_nat" name="form.solicitante.financiera.descuento_nomina" size="25" onkeyup="soloNumeros(this.id);"
                                       value="<%=solicitante != null ? solicitante.getAsJsonObject("finanza").get("descuento_nomina").getAsString() : "0"%>" 
                                       pattern="^(((\d{1,3})(,\d{3})*)|(\d+))(.\d+)?$" type="numeric" onblur="calcularTotales(); formato(this, 0);" >
                            </td>
                            <td> Total Activos <br> &nbsp; $ &nbsp;
                                <input id="tac_fin_nat" name="form.solicitante.financiera.activos" size="25" onkeyup="soloNumeros(this.id);"
                                       value="<%=solicitante != null ? solicitante.getAsJsonObject("finanza").get("activos").getAsString() : "0"%>" 
                                       pattern="^(((\d{1,3})(,\d{3})*)|(\d+))(.\d+)?$" type="numeric" onblur="calcularTotales(); formato(this, 0);" >
                            </td>
                        </tr>
                        <tr class="filaazul">
                            <td> Honorarios <br> &nbsp; $ &nbsp;
                                <input id="hon_fin_nat" name="form.solicitante.financiera.honorarios" size="25" onkeyup="soloNumeros(this.id);"
                                       value="<%=solicitante != null ? solicitante.getAsJsonObject("finanza").get("honorarios").getAsString() : "0"%>" 
                                       pattern="^(((\d{1,3})(,\d{3})*)|(\d+))(.\d+)?$" type="numeric" onblur="calcularTotales(); formato(this, 0);" >
                            </td>
                            <td> Arriendo / Cuota hipotecaria <br> &nbsp; $ &nbsp;
                                <input id="hip_fin_nat" name="form.solicitante.financiera.gastos_arriendo" size="25" onkeyup="soloNumeros(this.id);"
                                       value="<%=solicitante != null ? solicitante.getAsJsonObject("finanza").get("gastos_arriendo").getAsString() : "0"%>" 
                                       pattern="^(((\d{1,3})(,\d{3})*)|(\d+))(.\d+)?$" type="numeric" onblur="calcularTotales(); formato(this, 0);" >
                            </td>
                            <td> &nbsp; </td>
                        </tr>
                        <tr class="filaazul">
                            <td> Otros ingresos <br> &nbsp; $ &nbsp;
                                <input id="otro_fin_nat" name="form.solicitante.financiera.otros_ingresos" size="25" onkeyup="soloNumeros(this.id);"
                                       value="<%=solicitante != null ? solicitante.getAsJsonObject("finanza").get("otros_ingresos").getAsString() : "0"%>" 
                                       pattern="^(((\d{1,3})(,\d{3})*)|(\d+))(.\d+)?$" type="numeric" onblur="calcularTotales(); formato(this, 0);" >
                            </td>
                            <td> Cuotas prestamos <br> &nbsp; $ &nbsp;
                                <input id="pres_fin_nat" name="form.solicitante.financiera.gastos_creditos" size="25" onkeyup="soloNumeros(this.id);"
                                       value="<%=solicitante != null ? solicitante.getAsJsonObject("finanza").get("gastos_creditos").getAsString() : "0"%>" 
                                       pattern="^(((\d{1,3})(,\d{3})*)|(\d+))(.\d+)?$" type="numeric" onblur="calcularTotales(); formato(this, 0);" >
                            </td>
                            <td> Total Pasivos <br> &nbsp; $ &nbsp;
                                <input id="tpas_fin_nat" name="form.solicitante.financiera.pasivos" size="25" onkeyup="soloNumeros(this.id);"
                                       value="<%=solicitante != null ? solicitante.getAsJsonObject("finanza").get("pasivos").getAsString() : "0"%>" 
                                       pattern="^(((\d{1,3})(,\d{3})*)|(\d+))(.\d+)?$" type="numeric" onblur="calcularTotales(); formato(this, 0);" >
                            </td>
                        </tr>
                        <tr class="filaazul">
                            <td> &nbsp; </td>
                            <td> Gastos familiares y otros <br> &nbsp; $ &nbsp;
                                <input id="gas_fin_nat" name="form.solicitante.financiera.otros_gastos" size="25" onkeyup="soloNumeros(this.id);"
                                       value="<%=solicitante != null ? solicitante.getAsJsonObject("finanza").get("otros_gastos").getAsString() : "0"%>" 
                                       pattern="^(((\d{1,3})(,\d{3})*)|(\d+))(.\d+)?$" type="numeric" onblur="calcularTotales(); formato(this, 0);" >
                            </td>
                            <td> &nbsp; </td>
                        </tr>
                        <tr class="filaazul">
                            <td> Total Ingresos <br> &nbsp; $ &nbsp;
                                <input id="ting_fin_nat" name="form.solicitante.financiera.total_ingresos" size="25" readonly
                                       value="<%=solicitante != null ? solicitante.getAsJsonObject("finanza").get("total_ingresos").getAsString() : "0"%>"
                                       pattern="^(((\d{1,3})(,\d{3})*)|(\d+))(.\d+)?$" type="numeric" >
                            </td>
                            <td> Total Egresos <br> &nbsp; $ &nbsp;
                                <input id="tegr_fin_nat" name="form.solicitante.financiera.total_egresos" size="25" readonly
                                       value="<%=solicitante != null ? solicitante.getAsJsonObject("finanza").get("total_egresos").getAsString() : "0"%>" 
                                       pattern="^(((\d{1,3})(,\d{3})*)|(\d+))(.\d+)?$" type="numeric" >
                            </td>
                            <td> Total Patrimonio <br> &nbsp; $ &nbsp;
                                <input id="tpat_fin_nat" name="form.solicitante.financiera.total_patrimonio" size="25" readonly
                                       value="<%=solicitante != null ? solicitante.getAsJsonObject("finanza").get("total_patrimonio").getAsString() : "0"%>" 
                                       pattern="^(((\d{1,3})(,\d{3})*)|(\d+))(.\d+)?$" type="numeric" >
                            </td>
                        </tr>
                    </tbody>
                </table>
                <br>


            </div>
            
            <!-- BOTONES -->                
            <div align="center" style="padding-top: 1em; padding-bottom: 1em;">
                <%if (!nuevo) {
                  if ((bean_sol != null && bean_sol.getActividadNegocio().equalsIgnoreCase("RAD"))) { %>
                <div id="referenciar_btn" class="ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only" role="button" aria-disabled="false"
                        onclick="win_concepto('<%=num_solicitud%>','<%=bean_sol.getCodNegocio()%>', 'REF');">
                    <span class="ui-button-text">Referenciar</span>
                </div>
                <%} if ((bean_sol != null && bean_sol.getActividadNegocio().equalsIgnoreCase("REF"))) {%>
                <div id="analizar_btn" class="ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only" role="button" aria-disabled="false"
                        onclick="win_concepto('<%=num_solicitud%>','<%=bean_sol.getCodNegocio()%>', 'ANA');">
                    <span class="ui-button-text">Analizar</span>
                </div>
                <%} }%>
                <div id="gen_pdf_btn" class="ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only" role="button" aria-disabled="false"
                     onclick="">
                    <span class="ui-button-text">Generar PDF</span>
                </div>
                <div id="aceptar_btn" class="ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only" role="button" aria-disabled="false"
                     onclick="validar('P')">
                    <span class="ui-button-text">Aceptar</span>
                </div>
                <!--% if (nuevo || (!nuevo && bean_sol != null && !bean_sol.getEstadoSol().equalsIgnoreCase("P"))) { %-->
                <!--
                <div id="guardar_btn" class="ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only"
                     onclick="validar('B')">
                    <span class="ui-button-text">Guardar</span>
                </div> -->
                <div id="salir_btn" class="ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only" role="button" aria-disabled="false"
                     onclick="window.close();">
                    <span class="ui-button-text">Salir</span>
                </div>
            </div>

        </form>

        <div id="direccion_dialogo" class="ventana" style="display:none;">
            <table style="width: 100%;">
                <tr>
                    <td colspan="3"><input type="text" id="dir_resul" style="width: 100%;" readonly/></td>
                </tr>
                <tr>
                    <td>Via Principal</td>
                    <td>
                        <select id="via_princip_dir" onchange="setDireccion(2)">
                            <option value=""></option>
                            <option value="KR">Carrera</option>
                            <option value="CL">Calle</option>
                            <option value="TR">Transversal</option>
                            <option value="DL">Diagonal</option>
                        </select>
                    </td>
                    <td><input type="text" id="nom_princip_dir" style="width: 100%;" onchange="setDireccion(1)"/></td>
                </tr>
                <tr>
                    <td>Via Generadora</td>
                    <td>
                        <select id="via_genera_dir" onchange="setDireccion(1)">
                            <option value=""></option>
                            <option value="KR">Carrera</option>
                            <option value="CL">Calle</option>
                            <option value="TR">Transversal</option>
                            <option value="DL">Diagonal</option>
                        </select>
                    </td>
                    <td><input type="text" id="nom_genera_dir" style="width: 100%;" onchange="setDireccion(1)"/></td>
                </tr>
                <tr>
                    <td>Placa</td>
                    <td colspan="2"><input type="text" id="placa_dir" style="width: 100%;" onchange="setDireccion(1)"/></td>
                </tr>
                <tr>
                    <td>Complemento</td>
                    <td colspan="2"><input type="text" id="cmpl_dir" style="width: 100%;" onchange="setDireccion(1)"/></td>
                </tr>
            </table>
        </div>
                
        <div id="dialogo" class="ventana" style="display:none;">
            <p id="msj"></p>
            <ol id="lSolicitudes"></ol>
            <div id="liquidador">
                <table style="width: 100%;">
                    <tbody>
                        <tr class="fila">
                            <td>Formulario</td>
                            <td><input id="liq_num_sol" value="" disabled/></td>
                            <input type="hidden" id="liq_convenio" value="38" disabled style="width: 95%;"/>
                        </tr>
                        <tr class="fila">
                            <td>Negocio</td>
                            <td><input id="liq_cod_neg" value="" disabled style="width: 95%;"/></td>
                        </tr>
                        <tr class="fila">
                            <td>Tipo cuota</td>
                            <td><input type="hidden" id="liq_tipo_cuota" value="CTFCPV"/>
                            <input value="Cuota Fija Capital Variable" disabled style="width: 95%;"/></td>
                        </tr>
                        <tr class="fila">
                            <td>Numero cuotas</td>
                            <td><input id="liq_cuotas" value="" disabled style="width: 95%;"/></td>
                        </tr>
                        <tr class="fila">
                            <td>Valor a financiar</td>
                            <td><input id="liq_valor_negocio" value="" disabled style="width: 95%;"/></td>
                        </tr>
                        <tr class="fila">
                            <td>Titulo de valor</td>
                            <td><input id="liq_titulo_valor" value="PAGARE" disabled style="width: 95%;"/></td>
                        </tr>
                        <tr class="fila">
                            <td>Fecha del negocio</td>
                            <td>
                                <input id="liq_fec_negocio" value="" disabled style="width: 95%;"/>
                                <img src="/fintra/images/cal.gif" id="imgLiq_fec_negocio" alt="fecha" title="Seleccion de fecha" />
                                <script type="text/javascript">

                                    Calendar.setup({
                                        inputField: "liq_fec_negocio",
                                        trigger: "imgLiq_fec_negocio",
                                        align: "top",
                                        max: Calendar.dateToInt(new Date()),
                                        onSelect: function () {
                                            this.hide();
                                        }
                                    });
                                </script> 
                            </td>
                        </tr>
                        <tr class="fila">
                            <td>Plazo primera cuota (Dias)</td>
                            <td>
                                <input id="liq_pl_cuota_1" value="" disabled style="width: 95%;"/>
                            </td>
                        </tr>
                        <tr class="fila">
                            <td> Fecha de Pago </td>
                            <td>
                                <select id="liq_fecha_pago" style="width: 95%;"></select>
                            </td>
                        </tr>
                    </tbody>
                </table>
            </div>
        </div>
            
        <div id="grid_liquidacion" class="ventana" style="display:none;">
            <table id="tabla_simulador_Credito"></table>
        </div>
<!--
    <script>
        init(<%=nuevo%>);
    </script> 
-->    
    
<script>
        init(<%=nuevo%>);
    <%
        String id = request.getParameter("id_filtro") != null ? request.getParameter("id_filtro") : "";
        if (!id.equalsIgnoreCase("") && !id.equalsIgnoreCase("undefined")) {
    %>
        busquedaFiltro('<%=id%>');
    <%
        }
    %>
</script>    
    
    </body>
</html>
