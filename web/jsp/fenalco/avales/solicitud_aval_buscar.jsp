<%-- 
    Document   : solicitud_aval_buscar
    Created on : 27/11/2010, 09:13:44 AM
    Author     : rhonalf
--%>
<%@page session="true"%>
<%@page import="java.util.*"%>
<%@page import="com.tsp.operation.model.*"%>
<%@page import="com.tsp.operation.model.beans.*"%>
<%@page import="com.tsp.operation.model.services.*"%>
<%@page contentType="text/html" pageEncoding="ISO-8859-1"%>
<%
    response.addHeader("Cache-Control","no-cache");
    response.addHeader("Pragma","No-cache");
    response.addDateHeader ("Expires", 0);
    String BASEURL = request.getContextPath();
    String CONTROLLER = BASEURL + "/controller";
   String tipoconv = request.getParameter("tipoconv")!=null ? request.getParameter("tipoconv") : "Consumo"; 
   String trans = request.getParameter("trans") != null ? request.getParameter("trans") : "N";
   String vista = request.getParameter("vista")!=null ? request.getParameter("vista") : "1";

   %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
   "http://www.w3.org/TR/html4/loose.dtd">
<!-- This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>. -->
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
        <link href="<%= BASEURL %>/css/estilostsp.css" rel='stylesheet'>
        <script type='text/javascript' src="<%= BASEURL %>/js/boton.js"></script>
        <script type='text/javascript' src="<%= BASEURL %>/js/prototype.js"></script>
        <title>Solicitud de Aval - Buscar</title>
        <script type="text/javascript">

            function soloNumeros(id) {
                var valor = $(id).value;
                valor =  valor.replace(/[^0-9^.]+/gi,"");
                $(id).value = valor;
            }
            
            function busqueda(id_field){
                var dato = $(id_field).value;
                if(dato==''){
                    alert('Para consultar debe escribir algo');
                }
                else{
                    var url = $('formulario').action;
                    <%
                        if(!vista.equals("2")&&!vista.equals("5")){
                    %>
                  var p = "opcion=src_sol&dato="+dato+"&vista=<%=vista%>"+"&tipoconv=<%=tipoconv%>"+"&trans=<%=trans%>";
                    <%
                        }
                        else {
                    %>
                    var p = "opcion=src_sol_docs&dato="+dato+"&vista=<%=vista%>";
                    <%
                        }
                    %>
                    new Ajax.Request(url, { parameters: p, method: 'post', onComplete: completo } );
                }
            }

            function completo(resp){
                var txt = resp.responseText;
                if(txt.charAt(0) == '/'){
                    location.href = '<%= BASEURL %>'+txt;
                }
                else{
                    $('busqueda').innerHTML = txt;
                }
            }
        </script>
    </head>
    <body>
        <div id="capaSuperior" style="position:absolute; width:100%; height:100px; z-index:0; left: 0px; top: 0px;">
            <jsp:include page="/toptsp.jsp?encabezado=Buscar solicitud"/>
        </div>
        <div id="capaCentral" style="position:absolute; width:100%; height:83%; z-index:0; left: 0px; top: 100px; overflow:scroll;">
            <form id="formulario" name="formulario" action="<%=CONTROLLER%>?estado=GestionSolicitud&accion=Aval" method="post">
                <table align="center" width="520" border="1">
                    <tbody>
                        <tr>
                            <td>
                                <table id="tbusqueda" border="0" style="border-collapse: collapse; width: 100%;">
                                    <tr class="fila">
                                        <td class="subtitulo1">
                                            Numero de solicitud
                                        </td>
                                        <td>
                                            <input type="text" id="num_solicitud" name="num_solicitud" value="" size="15" onkeyup="soloNumeros(this.id);">
                                            <img alt="buscar" src="<%=BASEURL%>/images/botones/iconos/lupa.gif" width="15" height="15" style="cursor:pointer;" onclick="busqueda('num_solicitud');">
                                        </td>
                                    </tr>
                                    <tr class="fila">
                                        <td colspan="2" align="center">
                                            <div id="busqueda"></div>
                                        </td>
                                    </tr>
                                    <tr class="fila">
                                        <td colspan="2" align="center">
                                            <img alt="" src = "<%=BASEURL%>/images/botones/salir.gif" style = "cursor:pointer" name = "imgsalir" onclick = "window.close();" onmouseover="botonOver(this);" onmouseout="botonOut(this);">
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                    </tbody>
                </table>
            </form>
        </div>
    </body>
</html>
