<%-- 
    Document   : nomenclaturas
    Created on : 25/01/2016, 10:59:45 AM
    Author     : mcastillo
--%>

<%@page contentType="text/html" pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
    <head>
         <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
        <title>Nomenclaturas Direcci�n</title>
        <link type="text/css" rel="stylesheet" href="./css/jquery/jquery-ui/jquery-ui.css"/>
        <link type="text/css" rel="stylesheet" href="./css/jquery/jquery.jqGrid/ui.jqgrid.css"/>        
        <script type="text/javascript" src="./js/jquery/jquery-ui/jquery.min.js"></script>
        <script type="text/javascript" src="./js/jquery/jquery-ui/jquery.ui.min.js"></script>            
        <script type="text/javascript" src="./js/jquery/jquery.jqGrid/js/grid.locale-es.js"></script>
        <script type="text/javascript" src="./js/jquery/jquery.jqGrid/js/jquery.jqGrid.min.js"></script>
        <link type="text/css" rel="stylesheet" href="./css/buttons/botonVerde.css"/>
        <link type="text/css" rel="stylesheet" href="./css/popup.css"/>
        <link href="./css/style_azul.css" rel="stylesheet" type="text/css">      
        <script type="text/javascript" src="./js/nomenclaturas.js"></script>   
    </head>
    <body>
       <div id="capaSuperior" style="position:absolute; width:100%; height:100px; z-index:0; left: 0px; top: 0px;">
             <jsp:include page="/toptsp.jsp?encabezado=Maestro de Nomenclatura Direcciones"/>
        </div>
       <div id="capaCentral" style="position:absolute; width:100%; height:83%; z-index:0; left: 0px; top: 100px; overflow:auto;">    
           <center>
               <div id="div_nomenclaturas"  style="width: 800px">
           
                <br>
                <table id="tabla_nomenclaturas"></table>
                <div id="page_tabla_nomenclaturas"></div>    
          
           <div id="div_nomenclatura" title="CREAR NOMENCLATURA" style="display: none; width: 800px" >  
               <div id="filtros" class="ui-jqgrid ui-widget ui-widget-content ui-corner-all" style="height: 130px;padding: 0px 10px 5px 10px">
                   </br>
                   <table aling="center" style=" width: 100%" >
                       <tr>           
                           <td style="width: 10%"><span>Nombre</span></td>
                           <td style="width: 30%"><input type="text" id="nombre" name="nombre" style=" width: 222px" </td>
                           <td style="width: 25%"><span>Nomenclatura por defecto</span></td>   
                           <td style="width: 25%;padding:10px">
                               <input type="radio" name="is_default" value="S">Si
                               <input type="radio" name="is_default" value="N" checked> No
                           </td>  
                       </tr>
                       <tr>
                           <td style="width: 10%"><span>Descripcion:</span></td>   
                           <td style="width: 90%" colspan="4"><textarea id ="descripcion" name="descripcion" style="width:525px;resize:none"  rows="2" maxlength="300"></textarea></td>                        
                       </tr>
                                 
                   </table>
               </div>   
               </br> 
           </div> 
           <div id="div_editar_nomenclatura"  style="display: none; width: 800px" >
               <div id="filtros" class="ui-jqgrid ui-widget ui-widget-content ui-corner-all" style="height: 145px;padding: 0px 10px 5px 10px">
                   </br>
                   <table aling="center" style=" width: 100%" >
                       <tr>           
                           <input type="hidden" id="idNomenclatura" name="idNomenclatura"> 
                           <td style="width: 10%"><span>Nombre</span></td>
                           <td style="width: 30%"><input type="text" id="nombreEdit" name="nombreEdit" style=" width: 222px" </td>
                           <td style="width: 25%"><span>Nomenclatura por defecto</span></td>   
                           <td style="width: 25%;padding:10px">
                               <input type="radio" name="is_defaultEdit" value="S">Si
                               <input type="radio" name="is_defaultEdit" value="N" checked> No
                           </td>  
                       </tr>
                       <tr>
                           <td style="width: 10%"><span>Descripcion:</span></td>   
                           <td style="width: 90%" colspan="4"><textarea id ="descEdit" name="descEdit" style="width:525px;resize:none"  rows="2" maxlength="300"></textarea></td>                        
                       </tr>
                   </table>
               </div>   
               </br> 
           </div>
        </div>
                 </center>
        <div id="dialogMsg" title="Mensaje" style="display:none;">
            <p style="font-size: 12.5px;text-align:justify;" id="msj" > Texto </p>
        </div>
        <div id="dialogLoading" style="display:none;">
            <p  style="font-size: 12px;text-align:justify;" id="msj2">Texto </p> <br/>
            <center>
                <img src="./images/cargandoCM.gif"/>
            </center>
        </div>
       </div>                      
       
    </body>
</html>
