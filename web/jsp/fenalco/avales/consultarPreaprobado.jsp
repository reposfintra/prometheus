<%-- 
    Document   : consultarPreapobado
    Created on : 28-may-2013, 11:01:02
    Author     : egonzalez
--%>

<%@page session="true"%>
<%@page import="java.util.*"%>
<%@page import="com.tsp.operation.model.*"%>
<%@page import="com.tsp.operation.model.beans.*"%>
<%@page import="com.tsp.operation.model.services.*"%>
<%@page contentType="text/html" pageEncoding="ISO-8859-1"%>

<%
    response.addHeader("Cache-Control", "no-cache");
    response.addHeader("Pragma", "No-cache");
    response.addDateHeader("Expires", 0);
    String BASEURL = request.getContextPath();
    String CONTROLLER = BASEURL + "/controller";
%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
        <link href="<%= BASEURL %>/css/estilostsp.css" rel='stylesheet' >
        <script type='text/javascript' src="<%= BASEURL %>/js/boton.js"></script>
        <script type='text/javascript' src="<%= BASEURL %>/js/prototype.js"></script>
        <title>Consultar Preaprobado</title>
         <script type="text/javascript">


            ventanaX = 800;
            ventanaY = 400; 
            self.resizeTo(ventanaX,ventanaY);
            function soloNumeros(id) {
                var valor = $(id).value;
                valor =  valor.replace(/[^0-9^.]+/gi,"");
                $(id).value = valor;
            }
            
            function busqueda(id_field){
                var dato = $(id_field).value;
                if(dato==''){
                    alert('Para consultar debe escribir un numero de documento');
                }
                else{
                    var accion ="<%=BASEURL%>/jsp/fenalco/avales/solicitud_est.jsp?opcion=2";
                    $("formulario").action = accion;
                    document.getElementById("formulario").submit();
                }
            }
            
           
        </script>
    </head>
    <body  onResize="window.resizeTo(800,400)">
         <div id="capaSuperior" style="position:absolute; width:100%; height:100px; z-index:0; left: 0px; top: 0px;">
            <jsp:include page="/toptsp.jsp?encabezado=Consultar Pre-aprobados"/>
        </div>
            <div id="capaCentral" style="position:absolute; width:100%; height:50%; z-index:0; left: 0px; top: 170px;">
                <form id="formulario"  method="post" name="formulario" action="">
                    <table align="center" width="350" border="1">
                        <tbody>
                            <tr>
                                <td>
                                    <table id="tbusqueda" border="0" style="border-collapse: collapse; width:100%;">
                                        <tr class="fila">
                                            <td class="subtitulo1">
                                                <label style="font-size:15px;">Identificacion</label>
                                            </td>
                                            <td>
                                                <input type="text" id="identificacion" style="font-size:15px;text-align: center" name="identificacion" value="" maxlength="11" size="20"  onkeyup="soloNumeros(this.id);">
                                                
                                            </td>
                                        </tr>
                                        <tr class="fila">
                                            <td colspan="2" align="center">
                                            </td>
                                        </tr>
                                        <tr class="fila">
                                            <td colspan="2" align="center">
                                            </td>
                                        </tr>
                                        <tr class="fila">
                                            <td colspan="2" align="center">
                                                <img alt="" src = "<%=BASEURL%>/images/botones/aceptar.gif" style = "cursor:pointer" name = "imgaceptar" onclick = " busqueda('identificacion');" onmouseover="botonOver(this);" onmouseout="botonOut(this);">
                                                <img alt="" src = "<%=BASEURL%>/images/botones/salir.gif" style = "cursor:pointer" name = "imgsalir" onclick = "window.close();" onmouseover="botonOver(this);" onmouseout="botonOut(this);">
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </form>
            </div>
    </body>
</html>
