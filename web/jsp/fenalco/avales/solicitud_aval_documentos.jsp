<%-- 
    Document   : solicitud_aval_documentos
    Created on : 17/12/2010, 08:17:13 AM
    Author     : rhonalf
--%>

<%@page import="java.util.ArrayList"%>
<%@page import="com.tsp.util.Util"%>
<%@page session="true"%>
<%@ include file="/WEB-INF/InitModel.jsp" %>
<%@page import="com.tsp.operation.model.*"%>
<%@page import="com.tsp.operation.model.beans.*"%>
<%@page import="com.tsp.operation.model.services.*"%>
<%@page contentType="text/html" pageEncoding="ISO-8859-1"%>
<%
            response.addHeader("Cache-Control", "no-cache");
            response.addHeader("Pragma", "No-cache");
            response.addDateHeader("Expires", 0);
            Usuario usuario = (Usuario) session.getAttribute("Usuario");
            boolean sw_import=   (request.getParameter("import")!=null?false:true);
            int num_solicitud = request.getParameter("num_solicitud") != null ? Integer.parseInt(request.getParameter("num_solicitud")) : 0;
            String msj = request.getParameter("msj") != null ? request.getParameter("msj") : "";
            String tipo_proceso="";
             String vista = request.getParameter("vista") != null ? request.getParameter("vista") : "2";
            GestionSolicitudAvalService gsaserv = new GestionSolicitudAvalService(usuario.getBd());
            SolicitudAval bean_sol = null;
            String max_cuota = gsaserv.nombreTablagen("MAX_CUOTA","1"); 
            String consulta=   request.getParameter("consulta")!=null?request.getParameter("consulta"):"";
            String estado = "B";
            SolicitudPersona bean_pers = null;
            try {
                bean_sol = gsaserv.buscarSolicitud(num_solicitud);
                estado = bean_sol.getEstadoSol();
                if (bean_sol == null) {
                    bean_sol = new SolicitudAval();
                }
                bean_pers = gsaserv.buscarPersona(num_solicitud, "S");
            } catch (Exception e) {
                System.out.println("error al buscar solicitud: " + e.toString());
                e.printStackTrace();
            }
            int limite=(bean_sol.getPlazo()!=null&&!bean_sol.getPlazo().equals(""))?Integer.parseInt(bean_sol.getPlazo()):(!max_cuota.equals("")?Integer.parseInt(max_cuota):0);
            GestionCondicionesService gserv = new GestionCondicionesService(usuario.getBd());
            GestionConveniosService gcserv = new GestionConveniosService(usuario.getBd());
            boolean redesc =gcserv.tieneRedescuento(bean_sol.getIdConvenio());
             Convenio convenio = gcserv.buscar_convenio(usuario.getBd(), bean_sol.getIdConvenio());
               bean_sol = gsaserv.buscarSolicitud(num_solicitud);
            ArrayList<String> listatitulos = null;
            ArrayList<String> listacad = null;
            ArrayList<SolicitudDocumentos> lista_docs = null;
            int tam_docs = 0;
            try {
                listatitulos = gserv.titulosValor();
                listacad = gcserv.buscarBancos("nombre", "");
                lista_docs = gsaserv.buscarDocsSolicitud(num_solicitud);
                tam_docs = lista_docs.size();
            } catch (Exception e) {
                System.out.println("Error al obtener la lista de tipos de titulo valor: " + e.toString());
                e.printStackTrace();
            }
%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">
<!-- This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>. -->
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
        


        <%if(sw_import){%>
        <script type='text/javascript' src="<%= BASEURL%>/js/prototype.js"></script>
                <link href="<%= BASEURL%>/css/estilostsp.css" rel='stylesheet'>
        <script type='text/javascript' src="<%= BASEURL%>/js/boton.js"></script>
        <%}%>
        <title>Solicitud de Aval - Documentos</title>
        <style type="text/css">
            td{ white-space:nowrap;}

            .filableach{
                color: #000000;
                font-family: Tahoma, Arial;
                background-color:#FFFFFF;
                font-size: 12px;
            }

            .divs {
                font-family: Verdana, Tahoma, Arial, Helvetica, sans-serif;
                font-size: small;
                color: #000000;
                background-color:#9CF49C;
                -moz-border-radius: 0.7em;
                -webkit-border-radius: 0.7em;
            }
        </style>
        <script type="text/javascript">
            function soloNumeros(id) {
                var valor = $(id).value;
                valor =  valor.replace(/[^0-9^.]+/gi,"");
                $(id).value = valor;
            }

            function colocarCod(indice, id_src, id_dest){
                var valor = $(id_src).options[indice].value;
                $(id_dest).value = valor;
            }

            function enviar(){
                if(!validarCheques()){
                    var n_solic = $('num_solicitud').value;
                    var tabla = $("tabladocs");
                    var numrow = tabla.rows.length;
                    $('formulario').action += '&opcion=ins_docs&num_solicitud=' + n_solic + '&num_docs=' + numrow;
                    $('formulario').submit(); 

                }
                
            }
            
            function validarCheques(){
                
                var tipoNegocio=$('tipo_neg').value;
                var estado=false;
                //validamos que los numeros de los cheques sean digitados 
                //correctamente por los afiliados
               //  alert('Titulo Valor: '+tipoNegocio);
                if(tipoNegocio=='01'){
                    
                    var tabla = $("tabladocs");
                    var numfilas = tabla.rows.length-1;
                    
                    for (var i= 0 ; i < numfilas ; i++ ){
                        var nombreCampo='num_titulo'+i;
                        var aux=$(nombreCampo).value;
                       // alert('numero de digitos fila'+i+': '+aux.length);
                       if(aux.length == 1 || aux.length==0 ){
                         estado=true;
                         alert("Por favor digite los numeros de cheques");   
                         $(nombreCampo).focus();
                         break;    
                        }else if( aux.length == 2 && numfilas > 9){
                         alert("Por favor digite los numeros de cheques"); 
                         estado=true;
                         $(nombreCampo).focus(); 
                         break; 
                        }else if( aux.length == 2 && numfilas < 9){
                         alert("Por favor digite los numeros de cheques"); 
                         estado=true;
                         $(nombreCampo).focus(); 
                         break; 
                        }
                        
                    }
                       
                }
                   return estado;             
            }
            
           
          

            function inhabch(){
                if(document.getElementById("tipo_neg").value=="01"){
                    document.getElementById("banco_docs_ncheq").disabled=false;
                    document.getElementById("banco_docs").disabled=false;
                    document.getElementById("ciudad_cheque").disabled=false;
                }else{                    
                    document.getElementById("banco_docs_ncheq").value="";
                    document.getElementById("banco_docs_ncheq").disabled=true;
                    document.getElementById("banco_docs").disabled=false;
                    if(document.getElementById("tipo_neg").value=="03"){
                    document.getElementById("banco_docs_cod").value=99;
                    document.getElementById("banco_docs").value='';
                    document.getElementById("banco_docs").disabled=true;
                    document.getElementById("ciudad_cheque").value='';
                    document.getElementById("ciudad_cheque").disabled=true;
                     }

                }
            }

                    function devolver(){
                        var sw =false;
                        var estado="";
                       if( formulario.ckestado==null){
                            alert('Deber� seleccionar al menos un titulo valor');
                            
                        }else{
                            if(formulario.ckestado.length!=null){
                                for (i=0; i<formulario.ckestado.length; i++){
                                    if (formulario.ckestado[i].checked==true){
                                        sw=true;
                                        estado+=formulario.ckestado[i].value+"#";
                                    }
                                }
                            }else{
                                if (formulario.ckestado0.checked==true){
                                    sw=true;
                                    estado+=formulario.ckestado0.value+"#";
                                }
                            }

                            if(sw){

                                var num_sol=  $('num_solicitud').value;
                                var comentario=  $('comentario').value;
                                var negocio=$('cod_neg').value;

                                url = $('formulario').action;

                                p = "opcion=devolver&ckestado="+estado+"&form="+num_sol+"&comentario="+comentario+"&negocio="+negocio;

                                new Ajax.Request(url,
                                {
                                    parameters: p,
                                    method: 'post',
                                    onComplete: function (resp){

                                        var txt = resp.responseText;

                                        if(txt.charAt(0) == '/'){
                                            location.href = "<%= BASEURL%>"+txt;
                                        }
                                        else{
                                            alert( txt);
                                        }
                                    }
                                } );
                            }else{
                                alert('Deber� seleccionar al menos un titulo valor');
                            }
                        }
                    }

                     function indemnizar(){
                        var sw =false;
                        var estado="";
                        if( formulario.ckestado==null){
                            alert('Deber� seleccionar al menos un titulo valor');

                        }else{
                            if(formulario.ckestado.length!=null){
                                for (i=0; i<formulario.ckestado.length; i++){
                                    if (formulario.ckestado[i].checked==true){
                                        sw=true;
                                        estado+=formulario.ckestado[i].value+"#";
                                    }
                                }

                            }else{
                                if (formulario.ckestado0.checked==true){
                                    sw=true;
                                    estado+=formulario.ckestado0.value+"#";
                                }
                            }
                            if(sw){

                                var num_sol=  $('num_solicitud').value;
                                var comentario=  $('comentario').value;
                                var negocio=$('cod_neg').value;

                                url = $('formulario').action;

                                p = "opcion=indemnizar&ckestado="+estado+"&form="+num_sol+"&comentario="+comentario+"&negocio="+negocio;

                                new Ajax.Request(url,
                                {
                                    parameters: p,
                                    method: 'post',
                                    onComplete: function (resp){

                                        var txt = resp.responseText;

                                        if(txt.charAt(0) == '/'){
                                            location.href = "<%= BASEURL%>"+txt;
                                        }
                                        else{
                                            alert( txt);
                                        }
                                    }
                                } );
                            }else{
                                alert('Deber� seleccionar al menos un titulo valor');
                            }
                        }
                    }
                    var fecha_tit="";
                    function calcularfechas(limite){
                        if($('fecha_titulo0').value!=""&&$('fecha_titulo0').value!=fecha_tit){
                            fecha_tit=$('fecha_titulo0').value;
                            var fecha=new Date(fecha_tit.replace(/-/ig, "/"));
                            for(i=1;i<limite;i++){
                                var arreglo = $('fecha_titulo'+(i-1)).value.split("-");
                                var mes = arreglo[1];
                                var anno = arreglo[0];
                                var dia=dias(mes, anno);
                                fecha.setDate(fecha.getDate() + dia );
                                var curr_date = fecha.getDate();
                                var curr_month = fecha.getMonth();
                                    curr_month++;
                                var curr_year = fecha.getFullYear();
                                $('fecha_titulo'+i).value= curr_year + "-" + ((curr_month>9)?curr_month:'0'+curr_month )+ "-" + ((curr_date>9)?curr_date:'0'+curr_date );
                            }
                        }
                    }

                    function dias(mes, anno) {
                        mes = parseInt(mes,10);
                        anno = parseInt(anno,10);
                        switch (mes) {
                            case 1 : case 3 : case 5 : case 7 : case 8 : case 10 : case 12 : return 31;
                            case 2 : return (anno % 4 == 0) ? 29 : 28;
                        }
                        return 30;
                    }
        </script>
    </head>
    <body  >
         <%if (!consulta.equals("S")){%>
        <div id="capaSuperior" style="position:absolute; width:100%; height:100px; z-index:0; left: 0px; top: 0px;">
            <jsp:include page="/toptsp.jsp?encabezado=Solicitud de Aval - Documentos"/>
        </div>       
        <div id="capaCentral" style="position:absolute; width:100%; height:83%; z-index:0; left: 0px; top: 100px; overflow:scroll;">
                <%}%>
            <form id="formulario" name="formulario" action="<%=CONTROLLER%>?estado=GestionSolicitud&accion=Aval"  method="post">
             
                <table align="center" border="1" width="70%">
                    <tbody>
                        <tr>
                            <td style="padding: 0.3em;">                                
                                <div id="docs" style="width: 100%;">
                                    <table style="border-collapse:collapse; width:100%;" border="0">
                                        <tr class="filaazul">
                                            <td colspan="2">
                                                Cliente: <br>
                                                <input type="text" id="nom_cliente" name="nom_cliente" value="<%=bean_pers.getNombre()%> (<%= bean_pers.getIdentificacion()%>)" size="50" readonly="readonly" >
                                            </td>
                                            <th colspan="2">
                                                <input name="num_solicitud" id="num_solicitud" style="background-color: transparent; border-bottom: 0px; border-left: 0px; border-right: 0px; border-top: 0px; width:40%; text-align:right; color:#990000; font-size:16px;" value="<%= num_solicitud%>" size="15" readonly="readonly">
                                                <input name="cod_neg" id="cod_neg" type="hidden" value="<%=(bean_sol.getCodNegocio() != null) ? bean_sol.getCodNegocio() : ""%>">
                                                <input name="redesc" type="hidden" value="<%=redesc%>">
                                            </th>
                                        </tr>
                                        <tr class="filaazul">
                                            <td>
                                                Vr. solicitado<br>
                                                <input type="text" id="val_solicitado" name="val_solicitado" value="<%= (bean_sol.getValorSolicitado() != null ? Util.customFormat(Double.parseDouble(bean_sol.getValorSolicitado())) : "0")%>" size="15" readonly="readonly" style="text-align: right;">
                                            </td>                                            
                                            <td>
                                                T&iacute;tulo valor<br>
                                                <select id="tipo_neg" name="tipo_neg" style="width: 20em;" onfocus="<%=(bean_sol.getCodNegocio() != null) ? "this.blur();" : ""%>" onchange="inhabch();"   <%=consulta.equals("S")?"disabled":""%>>
                                                    <option value="">...</option>
                                                    <%
                                                                if (listatitulos != null && listatitulos.size() > 0) {
                                                                    String row[] = null;
                                                                    for (int i = 0; i < listatitulos.size(); i++) {
                                                                        row = (listatitulos.get(i)).split(";_;");
                                                                        String v10 = (bean_sol.getTipoNegocio() != null ? bean_sol.getTipoNegocio() : "");
                                                    %>
                                                    <option value="<%= row[0]%>" <%= (v10.equals(row[0]) ? "selected" : "")%>><%= row[1]%></option>
                                                    <%
                                                                    }
                                                                }
                                                    %>
                                                </select>
                                            </td>
                                            <td>
                                                N&ordm; cuenta<br>
                                                <input type="text" id="n_tipo_neg" name="n_tipo_neg" value="<%= (bean_sol.getNumTipoNegocio() != null ? bean_sol.getNumTipoNegocio() : "")%>" size="15"  <%=consulta.equals("S")||(bean_sol.getTipoNegocio().equals("01")&&!redesc)?"readonly":""%> >
                                            </td>
                                            <td>
                                                Ciudad cheque<br>
                                                <input type="text" id="ciudad_cheque" name="ciudad_cheque" value="<%= (bean_sol.getCiudadCheque()!= null  ? bean_sol.getCiudadCheque() : "")%>" maxlength="4" size="5" onkeyup="soloNumeros(this.id);" style="text-align: right;" <%=(bean_sol.getTipoNegocio() != null) ? (!bean_sol.getTipoNegocio().equals("01") && !bean_sol.getTipoNegocio().equals("") ? "readonly" : "") : ""%>>
                                            </td>
                                        </tr>
                                        <tr class="filaazul">
                                            <td>
                                                Banco<br>
                                                <select id="banco_docs" name="banco_docs" style="width: 20em;" onchange="colocarCod(this.selectedIndex,this.id,'banco_docs_cod');" onfocus="<%=(bean_sol.getCodNegocio() != null) ? "this.blur();" : ""%>"  <%=consulta.equals("S")||(!bean_sol.getTipoNegocio().equals("01") && !bean_sol.getTipoNegocio().equals(""))?"disabled":""%>>
                                                    <option value="">...</option>

                                                    <%
                                                                if (listacad != null && listacad.size() > 0) {
                                                                                                            String[] splitcad = null;
                                                                                                            for (int i = 0; i < listacad.size(); i++) {
                                                                                                                splitcad = (listacad.get(i)).split(";_;");
                                                                                                                String v12 = (bean_sol.getBanco()) != null ? bean_sol.getBanco() : "";
                                                    %>
                                                    <option value="<%= splitcad[0]%>" <%= (v12.equals(splitcad[0]) ? "selected" : "")%>><%= splitcad[1]%></option>
                                                    <%
                                                                                                            }
                                                                                                        } 
                                                    %>
                                                </select>
                                                <!--<input type="text" id="banco_docs" name="banco_docs" value="" size="20">-->
                                            </td>
                                            <td>
                                                Cod. banco<br>
                                                <input type="text" id="banco_docs_cod" name="banco_docs_cod" value="<%= (bean_sol.getBanco() != null ? bean_sol.getBanco() : "")%>" size="10" readonly="readonly">
                                            </td>
                                            <td>
                                                Sucursal<br>
                                                <input type="text" id="banco_docs_suc" name="banco_docs_suc" value="<%= (bean_sol.getSucursal() != null ? bean_sol.getSucursal() : "")%>" size="25"  <%=consulta.equals("S")||(!bean_sol.getTipoNegocio().equals("01") && !bean_sol.getTipoNegocio().equals(""))?"disabled":""%>>
                                            </td>
                                            <td>
                                                N&ordm; de chequera<br>
                                                <input type="text" id="banco_docs_ncheq" name="banco_docs_ncheq" value="<%= (bean_sol.getNumChequera() != null ? bean_sol.getNumChequera() : "")%>" size="15" <%=(bean_sol.getTipoNegocio() != null) ? (!bean_sol.getTipoNegocio().equals("01") && !bean_sol.getTipoNegocio().equals("") ? "readonly" : "") : ""%>  <%=consulta.equals("S")?"disabled":""%>>
                                            </td>
                                        </tr>
                                    </table>
                                    <table style="border-collapse:collapse; width:100%;" border="0">
                                        <tr class="filableach">
                                            <td width="100%">
                                                <table id="tabladocs" style="border-collapse:collapse; width:100%;" border="1">
                                                    <thead>
                                                        <tr class="filaazul">
                                                           <%if(vista.equals("14")){%>  <th>&nbsp;</th><%}%>
                                                            <th>
                                                                No. de titulo
                                                            </th>
                                                            <th>
                                                                Valor
                                                            </th>
                                                            <th>
                                                                Fecha (AAAA-MM-DD)
                                                            </th>
                                                            <th>
                                                                Estado Indemnizaci&oacute;n
                                                            </th>
                                                            <th>
                                                                Devoluciones
                                                            </th>
                                                        </tr>
                                                    </thead>
                                                    <%
                                                    if(bean_sol.getCodNegocio() != null){
                                                        tipo_proceso=model.Negociossvc.getTipoProceso(bean_sol.getCodNegocio());
                                                    }
                                                                if (bean_sol.getCodNegocio() != null&&tam_docs>0) {                                                                    
                                                                    limite = tam_docs;
                                                                } 
                                                                for (int i = 0; i < limite; i++) {
                                                    %>
                                                    <tr class="filableach">
                                                         <%if(vista.equals("14")){%>
                                                        <td width="5%" align="left">&nbsp; <input name="ckestado" type="checkbox" id="ckestado<%= i%>" value="<%= (i >= tam_docs) ? (i+1)+";0" :lista_docs.get(i).getNumTitulo()+ ";"+lista_docs.get(i).getDevuelto()%>" <%=(i >= tam_docs) ? "" :( lista_docs.get(i).getEstIndemnizacion().equals("VIG")?"":"disabled")%> ></td>
                                                        <%}%>
                                                        <td>                                                            
                                                            <input type="text" id="num_titulo<%= i%>" name="num_titulo<%= i%>" value="<%=(i >= tam_docs) ? (!bean_sol.getTipoNegocio().equals("01") && !bean_sol.getTipoNegocio().equals(""))?i+1:"" : lista_docs.get(i).getNumTitulo()%>" size="25" <%=(bean_sol.getTipoNegocio() != null) ? (!bean_sol.getTipoNegocio().equals("01") && !bean_sol.getTipoNegocio().equals("") ? "readonly" : "") : ""%>  onFocus="<%=(i==0&&tipo_proceso.equals("PSR"))?"calcularfechas("+limite+")":""%>" <%=consulta.equals("S")?"disabled":""%>>
                                                        </td>
                                                        <td>                                                           
                                                            <input type="text" id="valor_titulo<%= i%>" name="valor_titulo<%= i%>" value="<%=(i >= tam_docs) ? (!redesc)?Util.customFormat(Double.parseDouble(bean_sol.getValorSolicitado())):"" : Util.customFormat(Double.parseDouble(lista_docs.get(i).getValor()))%>" size="25" onkeyup="soloNumeros(this.id);" style="text-align: right;" <%=(bean_sol.getCodNegocio() != null) ? "readonly" : ""%>  >

                                                        </td>
                                                        <td>                                                            
                                                            <input type="text" id="fecha_titulo<%= i%>" name="fecha_titulo<%= i%>" value="<%= (i >= tam_docs) ? (i==0&&tipo_proceso.equals("DSR"))?bean_sol.getFechaConsulta().substring(0,10):"" : lista_docs.get(i).getFecha()%>"  size="25" readonly>
                                                            <%if(!consulta.equals("S")){if(!tipo_proceso.equals("DSR")&&(i==0&&tipo_proceso.equals("PSR"))){%>  <a href="javascript:void(0);"  onFocus="if(self.gfPop)gfPop.fPopCalendar(document.formulario.fecha_titulo<%= i%>);" hidefocus><img alt="calendario" src="<%=BASEURL%>/images/cal.gif" width="16" height="16" align="absmiddle"></a><%}}%>
                                                        </td>
                                                        <td align="center">
                                                            <%= (i >= tam_docs) ? "" : gsaserv.nombreTablagen("ESTINDEMNI",lista_docs.get(i).getEstIndemnizacion())%>
                                                        </td>
                                                        <td align="center">
                                                            <%= (i >= tam_docs) ? "0" : lista_docs.get(i).getDevuelto()%>
                                                        </td>
                                                    </tr>
                                                    <%
                                                                }
                                                    %>
                                                </table>
                                            </td>

                                        </tr>
                                    </table>
                                        
                                </div>

                            </td>
                        </tr>
                         <tr class="filaazul">
                            <td align="left" >
                                  <% TablaGen tOpcion=null;
                                model.tablaGenService.buscarDatos("VAL_OPCION", "TITULOS_VALORES");
                             tOpcion = model.tablaGenService.getTblgen();
                            if(  (!consulta.equals("S")) && (model.usuarioService.ValidarOpcion(usuario.getLogin(), tOpcion.getReferencia()) ))
				{
				%>
&iquest;Desea adjuntar documentos? 
<label>
  <select name="radicacion" id="radicacion">
    <option value="S" selected>SI</option>
    <option value="N">NO</option>
  </select>
</label>
<%}%>
                            </td>
                      </tr>            
                        <tr class="filaazul">
                            <td align="center" >
                                Comentarios: &nbsp;<textarea id="comentario" name="comentario" cols="100" ></textarea>
                            </td>
                        </tr> 
                                                 
                  </tbody>
                </table>
                 <%if (!consulta.equals("S")){%>
                    <div align="center">
                        <br>
                        <%
                                if (estado.equals("P")||estado.equals("Q")||(estado.equals("V")&&vista.equals("5"))) {
                        %>
                        <img alt="" src = "<%=BASEURL%>/images/botones/aceptar.gif" style = "cursor:pointer" name = "imgaceptar" onclick = "enviar();" onmouseover="botonOver(this);" onmouseout="botonOut(this);">
                        <%
                            }
                        %>
                        <img alt="" src = "<%=BASEURL%>/images/botones/salir.gif" style = "cursor:pointer" name = "imgsalir" onclick = "window.close();" onmouseover="botonOver(this);" onmouseout="botonOut(this);">
                    </div>
                  <%}%>
                <div align="center">                                          
                    <br>
                    <%  if (vista.equals("14")) {%>
                    <img alt="" src = "<%=BASEURL%>/images/botones/indemnizar.gif" style="cursor:pointer;" onclick="indemnizar()" name= "imgconcepto" id="imgconcepto"  onMouseOver="botonOver(this);" onMouseOut="botonOut(this);">
                    <img alt="" src = "<%=BASEURL%>/images/botones/devolver.gif" style="cursor:pointer;" onclick="devolver();" name= "imgconcepto" id="imgconcepto"  onMouseOver="botonOver(this);" onMouseOut="botonOut(this);">
                    <%}%>
                </div>
               <% if( msj != null && !msj.equals("") ){%>
                  <table width="30%" border="2" align="center">
                  <tr>
                    <td><table width="100%"  border="1" align="center"  bordercolor="#F7F5F4" bgcolor="#FFFFFF">
                      <tr>
                        <td width="229" align="center" class="mensajes"><%=msj%><br/></td>
                        <td width="29" background="<%=BASEURL%>/images/cuadronaranja.JPG">&nbsp;</td>
                        <td width="58">&nbsp;</td>
                      </tr>
                    </table></td>
                  </tr>
                </table>
                <%}%>
                                    
            </form> <%if (!consulta.equals("S")){%>
        </div>
            <%}%>
          <iframe  width=188 height=166 name="gToday:normal:agenda.js:gfPop:plugins.js" id="gToday:normal:agenda.js:gfPop:plugins.js" src="<%=BASEURL%>/js/Calendario/ipopeng.htm" scrolling="no" frameborder="0" style="visibility:visible; z-index:999; position:absolute; left:-500px; top:0px;"> </iframe>
         </body>
</html>
