<%-- 
    Document   : SolicitudesDevueltas
    Created on : 18/01/2012, 11:41:11 AM
    Author     : ivargas
--%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@page session="true"%>
<%@page import="java.util.*"%>
<%@page import="com.tsp.operation.model.*"%>
<%@page import="com.tsp.operation.model.beans.*"%>
<%@page import="com.tsp.operation.model.services.*"%>
<%@page import="com.tsp.util.*"%>
<%@include file="/WEB-INF/InitModel.jsp"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
        <title>Solicitudes Devueltas</title>
        <script src="<%=BASEURL%>/js/prototype.js"      type="text/javascript"></script>
        <script src="<%=BASEURL%>/js/boton.js"          type="text/javascript"></script>
        <link href="<%= BASEURL%>/css/estilostsp.css" rel="stylesheet" type="text/css">

        <%-- 
        <%
                    Usuario usuario = (Usuario) session.getAttribute("Usuario");
                    String tipoconv = request.getParameter("tipoconv") != null ? request.getParameter("tipoconv") : "Consumo";
                    ArrayList<BeanGeneral> resultado = new ArrayList();
                    GestionSolicitudAvalService gsaserv = new GestionSolicitudAvalService();
                    if (tipoconv.equals("Consumo")) {
                        resultado = gsaserv.solicitudesDevueltasConsumo(usuario.getLogin());
                    } else if (tipoconv.equals("Microcredito")) {
                        resultado = gsaserv.solicitudesDevueltasMicrocredito();
                    }
        %>

        --%>


        <%-- Aqui comienza--%>
        <%
            Usuario usuario = (Usuario) session.getAttribute("Usuario");
            String tipoconv = request.getParameter("tipoconv") != null ? request.getParameter("tipoconv") : "Consumo";
            String op = request.getParameter("op") != null ? request.getParameter("op") : "";
            String act_liq = request.getParameter("act_liq") != null ? request.getParameter("act_liq") : "";
            ArrayList<BeanGeneral> resultado = new ArrayList();
            GestionSolicitudAvalService gsaserv = new GestionSolicitudAvalService(usuario.getBd());
            if (op.equals("1")) {
                resultado = gsaserv.solicitudesDevueltasRad();
            } else {
                if (tipoconv.equals("Consumo")) {

                    resultado = gsaserv.solicitudesDevueltasConsumo(usuario.getLogin());					

                } else if (tipoconv.equals("Microcredito")) {
                    resultado = gsaserv.solicitudesDevueltasMicrocredito();
                } else if (tipoconv.equals("Multiservicio")) {
                                    resultado = gsaserv.solicitudesDevueltasMultiservicio();
                }


                 if (!act_liq.equals(""))
                     {
                         if (act_liq.equals("PREA")) {
                             resultado = gsaserv.getListaPreaprobados();
                         }else{
                             resultado = gsaserv.SolicitudesDevueltas("SOL", act_liq);
                         }
                     }

            }
			
			
			
        %>

    </head>
    <script>
	function reliquidar (convenio,numero_solicitud,act)
	{
           
            var liq=0;
           if(convenio=="Consumo"){
               liq=1;
           }
           if(convenio=="Microcredito"){
               liq=2;
           }

            switch (liq)
            {
                case 1:
                    location.href="<%=BASEURL%>/jsp/fenalco/liquidadores/liquidadorNegocios.jsp?tipo_liq=STANDAR&numero_solicitud="+numero_solicitud+"&act_liq="+act;
                    break;
                case 2:
                    location.href="<%=BASEURL%>/jsp/fenalco/liquidadores/liquidadorNegMicrocredito.jsp?tipo_liq=STANDAR&numero_solicitud="+numero_solicitud+"&op_liq="+act;
                    break;
            }
	}
	
	</script>
    <body>
        <div id="capaSuperior" style="position:absolute; width:100%; height:100px; z-index:0; left: 0px; top: 0px;">
            <%if (op.equals("1")) {%>
            <jsp:include page="/toptsp.jsp?encabezado=Devueltas a Radicación"/>
            <%} else {
                if (tipoconv.equals("Consumo") && !act_liq.equals("PREA")) {%>
            <jsp:include page="/toptsp.jsp?encabezado=Solicitudes Devueltas"/>
            <%} else if (tipoconv.equals("Microcredito")) {%>
            <jsp:include page="/toptsp.jsp?encabezado=Solicitudes Devueltas Microcredito"/>
            <%} else if (tipoconv.equals("Multiservicio")) {%>
            <jsp:include page="/toptsp.jsp?encabezado=Solicitudes Devueltas Aires AAE"/>
            <%}else if (tipoconv.equals("Consumo") && act_liq.equals("PREA")){%>
             <jsp:include page="/toptsp.jsp?encabezado=Liquidar Pre-Aprobado"/>
            <%  }
                }%>
        </div>

        <div id="contenido" style="visibility: hidden; display: none; width: 600px; height: 300px; background-color: #EEEEEE;">

        </div>

        <div id="capaCentral" style="position:absolute; width:100%; height:85%; z-index:0; left: 0px; top: 100px; overflow:auto;">
            <form action="" id="formulario" name="formulario" method="post">
                <div align="center">
                    <table border="1" width="80%">
                        <tr class="filaazul">
                            <td>
                                <div style="width: 100%;  overflow: auto;" id="tablaconvs">
                                    <table border="1" width="100%" id="tablaasesor">
                                        <tr class="subtitulo1" align="center">
                                            <td>Formulario</td>
                                            <td>Negocio</td>
                                            <td>Cliente</td>


                                            <%--
                                            <%if (tipoconv.equals("Consumo")) {%>
                                            <td>Afiliado</td>
                                            <%} else if (tipoconv.equals("Microcredito")) {%>
                                            <td>Asesor</td>
                                            <%}%>
                                            --%>

                                            <%-- Comienzo de linea --%>
                                            <%if (tipoconv.equals("Consumo")) {%>
                                            <td>Afiliado/Asesor</td>
                                            <%} else {
                                                if (tipoconv.equals("Consumo")) {%>
                                            <td>Afiliado</td>
                                            <%} else if (tipoconv.equals("Microcredito")||tipoconv.equals("Multiservicio")) {%>
                                            <td>Asesor</td>
                                            <%}}%>
                                            <%-- Termina linea --%>


                                        <td>Fecha</td>
                                        <td>Comentarios</td>

                                     
                                        <td> </td>
                                    </tr>
                                    <% if (resultado.size() > 0) {
                                            for (int i = 0; i < resultado.size(); i++) {%>
                                        <tr class="filaazul">
                                            <td>
                                                <%=resultado.get(i).getValor_01()%>
                                            </td>
                                            <td>
                                                <%=resultado.get(i).getValor_02()%>
                                            </td>
                                            <td>
                                                <%=resultado.get(i).getValor_03()%>
                                            </td>
                                            <td>
                                                <%=resultado.get(i).getValor_04()%>
                                            </td>
                                            <td>
                                                <%=resultado.get(i).getValor_05()%>
                                            </td>
                                            <td>
                                                <%=resultado.get(i).getValor_06()%>
                                            </td>
                                         <td>  
                                         <img width="25" src="<%=BASEURL%>/images/botones/iconos/detalle.png" name="detalle" id="detalle"  onClick="reliquidar('<%=resultado.get(i).getValor_07()%>','<%=resultado.get(i).getValor_01()%>','<%=act_liq%>');" >
                                    
									</td>
                                        </tr>
                                        <%  }
                                    } else {%>
                                        <tr class="filaazul" align="center">
                                            <td colspan="6">
                                                No hay Solicitudes
                                            </td>
                                        </tr>
                                        <%}%>

                                    </table>
                                </div>

                            </td>
                        </tr>
                    </table>
                    <br>
                    <div align="center">
                        <img alt="" src = "<%=BASEURL%>/images/botones/salir.gif" style = "cursor:pointer" name = "imgsalir" onClick = "window.close();" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);">
                    </div>
                </div>
            </form>
        </div>
    </body>
</html>
