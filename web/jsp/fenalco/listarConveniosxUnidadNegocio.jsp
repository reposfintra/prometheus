<%-- 
    Document   : listarConveniosxUnidadNegocio
    Created on : 1/07/2016, 03:05:24 PM
    Author     : dvalencia
--%>
<%@page import="com.tsp.operation.model.beans.CmbGeneralScBeans"%>
<%@page import="com.tsp.operation.model.beans.Usuario"%>
<%@page import="java.util.ArrayList"%>
<%@page import="com.tsp.operation.model.services.GestionConveniosService"%>
<%@page contentType="text/html" pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<%
    Usuario usuario = (Usuario) session.getAttribute("Usuario");
    GestionConveniosService rqservice= new GestionConveniosService(usuario.getBd());
    ArrayList listaCombo =  rqservice.GetComboGenerico("SQL_OBTENER_UNIDAD_NEGOCIO2","id","descripcion","");
        
    String LoginUsuario = usuario.getLogin();

%>

<html>
    <head>
        <head>
        <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
        <title>Convenios por Unidad de Negocio</title>
        <link type="text/css" rel="stylesheet" href="./css/jquery/jquery-ui/jquery-ui-2.css"/>
        <link type="text/css" rel="stylesheet" href="./css/jquery/jquery.jqGrid/ui.jqgrid.css"/>        
        <script type="text/javascript" src="./js/jquery/jquery-ui/jquery.min.js"></script>
        <script type="text/javascript" src="./js/jquery/jquery-ui/jquery.ui.min.js"></script>            
        <script type="text/javascript" src="./js/jquery/jquery.jqGrid/js/grid.locale-es.js"></script>
        <script type="text/javascript" src="./js/jquery/jquery.jqGrid/js/jquery.jqGrid.min.js"></script>
        <link type="text/css" rel="stylesheet" href="./css/buttons/botonVerde.css"/>
        <link type="text/css" rel="stylesheet" href="./css/popup.css"/>
<!--    <link href="./css/style_azul.css" rel="stylesheet" type="text/css">-->
        <script type="text/javascript" src="./js/convenioXunidadNegocio.js"></script>  
        
         <!--css logica de negocio-->
<!--     <link href="./css/asobancaria.css" rel="stylesheet" type="text/css">-->

        <!--jqgrid--> 
        <script type="text/javascript" src="./js/jquery/jquery.jqGrid-4.7.0/js/grid.locale-es.js"></script>
        <script type="text/javascript" src="./js/jquery/jquery.jqGrid-4.7.0/js/jquery.jqGrid.min.js"></script>
        <script type="text/javascript" src="./js/jquery/jquery.jqGrid-4.7.0/plugins/jquery.contextmenu.js"></script>
        <script type="text/javascript" src="./js/jquery/jquery.jqGrid-4.7.0/js/jquery.tabletojson.js"></script>  
    </head>
    <body>
        <div id="capaSuperior" style="position:absolute; width:100%; height:100px; z-index:0; left: 0px; top: 0px;">
            <jsp:include page="/toptsp.jsp?encabezado=Convenios por Unidad de Negocio"/>
        </div>
        <div id="capaCentral" style="position:absolute; width:100%; height:100%; z-index:0; left: 0px; top: 130px; ">
        <style>
            .onoffswitch-inner:before {
                content: "A";
                padding-left: 14px;
                background-color: #34A7C1; color: #FFFFFF;
            }
            .onoffswitch-inner:after {
                content: "I";
                padding-right: 11px;
                background-color: #C20F0F; color: #F5F0F0;
                text-align: right;
            }
        </style>    
           
        <center>
                <table width="400" border="0" cellpadding="0" cellspacing="1" class="labels" id="tbl_hys">
                    
                    <tr>
                        <td colspan="2">
                                                        
                            <table width="400" border="0" align="left" cellpadding="1" cellspacing="1" class="labels">
                                  <tr>
                                      
                                    <td width="300">
                                        <fieldset style="border:1px solid #00a0f0">
                                            <legend>UNIDADES DE NEGOCIO</legend>
                                            <table border="0" align="center" cellpadding="1" cellspacing="1" class="labels">
                                                <tr>
                                                    <td>
                                                        <select name="unidad_negocio" class="combo_180px" id="unidad_negocio">
                                                            <option value="" selected>< -Escoger- ></option><%
                                                            for (int i = 0; i < listaCombo.size(); i++) {
                                                                CmbGeneralScBeans CmbContenido = (CmbGeneralScBeans) listaCombo.get(i);%>
                                                                 <option value="<%=CmbContenido.getIdCmb() %>"><%=CmbContenido.getDescripcionCmb() %></option><%
                                                            }%>
                                                        </select>
                                                    </td>
                                                </tr>
                                            </table>
                                        </fieldset>
                                    </td>
                                    <td>
                                        
                                    </td>
                                       
                                    <td width="100">  
                                    <span  class="form-submit-button form-submit-button-simple_green_apple" id="filtrar" onclick="cargarConveniosxUnidadNegocio();" />Filtrar </span>
                                    
                                    </td>
                                  </tr>
                            </table>
                        </td>
                    </tr>  
                    <td>
                        <br>                  
                    </td>  
                    <div id="box" style="top:281px; left:417px; position:absolute; z-index:1000; width: 43px; height: 29px;"></div>
                    <td>
                        <br>                  
                    </td>  
                </table>
            <div style="position: relative;top: 10px"> 
                <table id="tabla_convenios"></table>
                <div id="page_tabla_convenios"></div>
            </div>
        </div>         
            <div id="dialogConvenios"  class="ventana"  hidden>
                <div id="tablainterna" style="width: 720px;" >
                    <table id="tablainterna"  >
                        <tr>
                            <td>
                                <input type="text" id="id_convenio" style="width: 137px;color: black;" hidden>
                            </td>
                        </tr>
                        <tr >
                            <td colspan="3">
                                <label>Nombre convenio</label>
                                <input type="text" id="convenio" value="Solo lectura" readonly style="width: 370px;color: black; ">
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <label>Tasa de Interes</label>
                                <input type="text" id="tasa_interes" style="width: 80px;color: black;">
                            </td>
                            <td>
                                <label>Tasa de Interes Mora</label>
                                <input type="text" id="tasa_usura" style="width: 80px;color: black;">
                            </td>
                            <td>
                                <label>Tasa Compra Cartera</label>
                                <input type="number" id="tasa_compra_cartera" style="width: 80px;color: black;" maxlength="6">
                            </td>
                            <td>
                                <label>Tasa Aval</label>
                                <input type="number" id="tasa_aval" style="width: 80px;color: black;" maxlength="6">
                            </td>
                            <td>
                                <label>Tasa Max Fintra</label>
                                <input type="number" id="tasa_max_fintra" style="width: 80px;color: black;" maxlength="6">
                            </td>
                            <td>
                                <label>Tasa SIC EA</label>
                                <input type="number" id="tasa_sic_ea" style="width: 80px;color: black;" maxlength="6">
                            </td>
                        </tr>
                    </table>
                </div>
            </div>
                                                        
            <div id="dialogConvenios2"  class="ventana" hidden>
                <div id="tablainterna2" style="width: 720px;" >
                    <table id="tablainterna2"  >
                        <tr align="center">
                            <td>
                                <label>Tasa de Interes</label>
                                <input type="text" id="tasa_interesd" style="width: 80px;color: black;">
                            </td>
                            <td>
                                <label>Tasa de I. Mora</label>
                                <input type="text" id="tasa_usurad" style="width: 80px;color: black;">
                            </td>
                            <td>
                                <label>Tasa C. Cartera</label>
                                <input type="text" id="tasa_compra_carterac" style="width: 80px;color: black;">
                            </td>
                            <td>
                                <label>Tasa Aval</label>
                                <input type="text" id="tasa_avalc" style="width: 80px;color: black;">
                            </td>
                            <td>
                                <label>Tasa Max Fintra</label>
                                <input type="text" id="tasa_max_fintrac" style="width: 80px;color: black;">
                            </td>
                            <td>
                                <label>Tasa SIC EA</label>
                                <input type="text" id="tasa_sic_eac" style="width: 80px;color: black;">
                            </td>
                        </tr>
                    </table>
                </div>
            </div>  
           
            <div id="info"  class="ventana" hidden>
            <p id="notific">EXITO AL ACTUALIZAR</p>
            </div>    
        </center>
    </body> 
</html>
