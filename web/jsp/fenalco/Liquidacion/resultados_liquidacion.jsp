<%@page contentType="text/html"%>
<%@page session="true"%>
<%@page errorPage="/error/ErrorPage.jsp"%>
<%@ page import="java.sql.*" %>
<%@ page import="java.util.*" %>
<%@ page import="java.io.*" %>
<%@ page import="java.lang.*" %>
<%@page import="java.util.*"%>
<%@page import="com.tsp.operation.model.*"%> 
<%@page import="com.tsp.operation.model.beans.*"%>
<%@page import="com.tsp.util.*"%>
<%@page import="com.tsp.util.Util.*"%>
<%@include file="/WEB-INF/InitModel.jsp"%>
<%@ taglib uri="http://jakarta.apache.org/taglibs/input-0.90" prefix="input" %>
<html>
<head>
<title>Liquidador Maestro</title>

<link href="<%=BASEURL%>/css/estilostsp.css" rel="stylesheet" type="text/css">   
<script language="Javascript">
function imprSelec(nombre)
{
  var ficha = document.getElementById(nombre);
  var ventimp = window.open(' ', 'popimpr');
  ventimp.document.write( ficha.innerHTML);
  ventimp.document.close();
  ventimp.print( );
  ventimp.close();
} 
</script>      
<style type="text/css">
<!--
.style1 {font-size: 12px}
-->
</style>        
    </head>  
    <body > 
	<div id="capaSuperior" style="position:absolute; width:100%; height:100px; z-index:0; left: 0px; top: 0px;">
<jsp:include page="/WEB-INF/toptsp.jsp?encabezado=Liquidador Maestro,"/>

</div>
<div id="capaCentral" style="position:absolute; width:100%; height:85%; z-index:0; left: 0px; top: 100px; overflow: scroll;"> 
<script type='text/javascript' src="<%=BASEURL%>/js/Validacion.js"></script>
<script type='text/javascript' src="<%= BASEURL %>/js/boton.js"></script>
<script type='text/javascript' src="<%= BASEURL %>/js/general.js"></script>   			
<%				
				boolean mostrarTodo=false;//true 	false

				double valor_operation=0;//Cells(5, "B")=Cells(5, "B")	
				double custodiax=0;
				
				Propietario  Propietario  = new Propietario();				
    			Propietario = model.FenalcoFintraSvc.getPropietario();
				Usuario usuario            = (Usuario) session.getAttribute("Usuario");
				Vector Aval=null;
                String ParNumero_Cheques = (String)request.getParameter("numero_cheques");
                String ParValor_Negocio = (String)request.getParameter("valor_desembolso");
                String Forma_Pago = (String)request.getParameter("forma_pago");
				String Estado_Remesa = (String)request.getParameter("estado_remesas");
                String fechai = (String)request.getParameter("fechainicio");
                String propietario   = request.getParameter("Nit");
				String tipo_negocio  = request.getParameter("tipnegocio");
				
				String aval   = request.getParameter("aval");
				String custodia   = request.getParameter("custodia");
				String remesas  = request.getParameter("remesas");
				String colorfila = "";
				
                int Numero_Cheques = Integer.parseInt(ParNumero_Cheques);
				
				double[] diasremix=new double[Numero_Cheques+2];
								    
                double Valor_Negocio = Double.parseDouble(ParValor_Negocio);
                
                double Tasa = Propietario.getTasa_fenalco()/100;
                double Custodia_Cheque = Propietario.getCustodiacheque();
				double porrem=Propietario.getRem()/100;//esta remesa hay que traerla
                java.util.Calendar Fecha_Sugerida = null;
                java.util.Calendar Fecha_Negocio = Util.crearCalendarDate(fechai);
				java.util.Calendar Fecha_Sugerida_2 = null;
				java.util.Calendar Fecha_Sugerida_3 = null;
               			   
                int dia_valido=0;
                int dia=0; 
				int mes=0; 
				int ano=0;
				double totalAvalMostrable=-1.0;
				int dia_s=0; 
				int mes_s=0; 
				int ano_s=0;
				
				dia = Integer.parseInt(fechai.substring(8,10));
    			mes = Integer.parseInt(fechai.substring(5,7));    
    			ano = Integer.parseInt(fechai.substring(0,4));
				Fecha_Negocio.set(ano, mes-1, dia);
				
                double Formula=0;
                double Suma=0;
                double Valor_Cheque=0;
				double Valor_Cheque_X=0;
                double Desembolso=0;
                double Valor_Capital=0;
                double Valor_Interes=0;
				double Valor_Remesa=0;
                double Saldo_Final=0;
                double Saldo_Inicial=0;
                double Valor_Doc=0;
                double Formulas[] ;
				double ValorNum=0;
                double TotalAval=0;
				double ValorAval=0;
				double ValorAvalIva=0;
				 double valor_1=0;
                double valor_2=0;
                double valor_3=0;
				double valor_4=0;
				double dias=0;
                Suma = 0;
				double lim=11655;
                
				double TotalRemesa_x=0;
								
                String Filas_Tabla_Liquidacion   = "";
                String Filas_Tabla_Liquidacion_2 = "";
                String Dias_Primer_Cheque        =  Forma_Pago ;
                Saldo_Final = 0;

                Formula = (((Math.pow((1+Tasa),12))-1));
				
                Formulas= new double[Numero_Cheques+1];
				
				ArrayList chequesx=new ArrayList();
				chequeCartera chequeCarterax=new chequeCartera();
                for (int fila = 1; fila <= Numero_Cheques; fila++){//se recorrera cada cheque para armarle su fila
				
					chequeCarterax=new chequeCartera();
					
					if (fila%2==0){
					colorfila = "filagris";
					}else{
					colorfila = "filaazul";}
				
                    Filas_Tabla_Liquidacion = Filas_Tabla_Liquidacion +"<tr class='"+colorfila+"'>";
                    //Numero del documento
                    Filas_Tabla_Liquidacion = Filas_Tabla_Liquidacion +"    <td>";
                    Filas_Tabla_Liquidacion = Filas_Tabla_Liquidacion +"    <p align='center'>"+ String.valueOf(fila) +"</p>";
                    Filas_Tabla_Liquidacion = Filas_Tabla_Liquidacion +"    </td>";
					
					chequeCarterax.setItem(""+fila);
					
                    // Fechas 
					if (fila == 1){
						
						if (!Forma_Pago.equals("30")){
							Fecha_Sugerida = Util.TraerFecha(fila, dia, mes-1, ano, Forma_Pago);
							
							Fecha_Sugerida_2 = Util.TraerFecha(fila, dia, mes-1, ano, Forma_Pago);
							
							Fecha_Sugerida_3 = Util.TraerFecha(fila, dia, mes-1, ano, Forma_Pago);
							
						}else{
						
							Fecha_Sugerida = Util.TraerFecha(fila, dia, mes, ano, Forma_Pago);
							
							Fecha_Sugerida_2 = Util.TraerFecha(fila, dia, mes, ano, Forma_Pago);
							
							Fecha_Sugerida_3 = Util.TraerFecha(fila, dia, mes, ano, Forma_Pago);
						}
						
					}else{
					
						if (!Forma_Pago.equals("45")){
							
							Fecha_Sugerida = Util.TraerFecha(fila, dia, mes, ano, Forma_Pago);
							
							Fecha_Sugerida_2 = Util.TraerFecha(fila, dia, mes, ano, Forma_Pago);
							
							Fecha_Sugerida_3 = Util.TraerFecha(fila, dia, mes, ano, Forma_Pago);
							
						}else{
							
							Fecha_Sugerida = Util.TraerFecha(fila, dia, mes-1, ano, Forma_Pago);
								
							Fecha_Sugerida_2 = Util.TraerFecha(fila, dia, mes-1, ano, Forma_Pago);
							
							Fecha_Sugerida_3 = Util.TraerFecha(fila, dia, mes-1, ano, Forma_Pago);
														
						}
						
					}	
					
					if (!Forma_Pago.equals("30")){
                        if (fila == 1) 
                        {
                            dia = Fecha_Sugerida.get(Calendar.DATE);
                            mes = Fecha_Sugerida.get(Calendar.MONTH);
                            ano = Fecha_Sugerida.get(Calendar.YEAR);
                        }
                    
					}
					
					dia_s = Fecha_Sugerida_2.get(Calendar.DATE);
					mes_s = Fecha_Sugerida_2.get(Calendar.MONTH);
					ano_s = Fecha_Sugerida_2.get(Calendar.YEAR);											
                    
					if( mes_s == 12 ){
							ano_s=ano_s-1;}
					if( mes_s == 0 ){
						mes_s = 1;}
					else if (mes_s!=1){
						mes_s =mes_s+1;
					
					}else if (mes_s==1){
						mes_s =mes_s=2;
					}
					int j=0;
												
					while (model.FenalcoFintraSvc.buscarFestivo(ano_s+"-"+mes_s+"-"+dia_s)){
												
						mes_s=mes_s-1;
						Fecha_Sugerida_2.set(ano_s, mes_s, dia_s);
						Fecha_Sugerida_2.add(Calendar.DAY_OF_YEAR,1);
						
						dia_s = Fecha_Sugerida_2.get(Calendar.DATE);
						mes_s = Fecha_Sugerida_2.get(Calendar.MONTH);
						ano_s = Fecha_Sugerida_2.get(Calendar.YEAR);				
							mes_s=mes_s+1;
						
						
						j=1;
						
					}
					
					if (j==1)
					{
						if (mes_s!=0){
							Fecha_Sugerida_2.set(ano_s, mes_s-1, dia_s);
						}else{
							Fecha_Sugerida_2.set(ano_s, 1, dia_s);
						}
					}
					if (fila != 1){
						if( mes_s != 12 ){
							Fecha_Sugerida_3.set(ano_s, mes_s-1, dia_s);
						}
					}else{
						Fecha_Sugerida_3.set(ano_s, mes_s, dia_s);
					}			
							       				
					Filas_Tabla_Liquidacion = Filas_Tabla_Liquidacion +"    <td>";
					Filas_Tabla_Liquidacion = Filas_Tabla_Liquidacion +"    <p align='right' >"+ Util.ObtenerFechaCompleta(Fecha_Sugerida) +"</p>";
					Filas_Tabla_Liquidacion = Filas_Tabla_Liquidacion +"    </td>";				
					
					String diaremix="",mesremix="";
					if (Fecha_Sugerida.get(java.util.Calendar.MONTH)<9){
						mesremix="0"+(Fecha_Sugerida.get(java.util.Calendar.MONTH)+1);
					}else{
						mesremix=""+(Fecha_Sugerida.get(java.util.Calendar.MONTH)+1);
					}
					if (Fecha_Sugerida.get(java.util.Calendar.DAY_OF_MONTH)<10){
						diaremix="0"+Fecha_Sugerida.get(java.util.Calendar.DAY_OF_MONTH);
					}else{
						diaremix=""+Fecha_Sugerida.get(java.util.Calendar.DAY_OF_MONTH);
					}
					
					String fechitaremix=(Fecha_Sugerida.get(java.util.Calendar.YEAR)) + "-" +mesremix+"-"+diaremix;
                	
					chequeCarterax.setFechaCheque(""+fechitaremix);
					
					if (fila != 1){
						dias = Util.diasTranscurridos(Fecha_Negocio, Fecha_Sugerida_2);
					}else{
						dias = Util.diasTranscurridos(Fecha_Negocio, Fecha_Sugerida_2);
					}
					
					if (mostrarTodo){
						Filas_Tabla_Liquidacion = Filas_Tabla_Liquidacion +"    <td>";
						Filas_Tabla_Liquidacion = Filas_Tabla_Liquidacion +"    <p align='right' >"+dias +"</p>";
						Filas_Tabla_Liquidacion = Filas_Tabla_Liquidacion +"    </td>";
					}
					
					diasremix[fila-1]=dias;
					
					chequeCarterax.setDias(""+dias);

					valor_1=(dias/365);
					valor_2=(1+Formula);
					valor_3 =Math.pow(valor_2, valor_1);
					valor_4 = ((1/valor_3));
		
					double val = Util.redondear(valor_4,9);
					//double valor_5 = Double.parseDouble(val);
					Formulas[fila]= val;
					
					//Formulas[fila]= Util.obtenerValor(Util.diasTranscurridos(Fecha_Negocio, Fecha_Sugerida_2),Formula);
					//System.out.println(" formulaa = "+fila+":"+Formulas[fila]+" suma"+Suma+"");
					
					Suma = Suma + Formulas[fila];
					Filas_Tabla_Liquidacion = Filas_Tabla_Liquidacion +"</tr>";
					if (j==1)
					{
						
						Fecha_Sugerida_2.set(ano_s, mes_s+1, dia_s);
						dia_s = Fecha_Sugerida_2.get(Calendar.DATE);
						mes_s = Fecha_Sugerida_2.get(Calendar.MONTH);
						ano_s = Fecha_Sugerida_2.get(Calendar.YEAR);
					}
		chequesx.add(chequeCarterax);
    } 
	//System.out.println("sumaaaaaaaa = "+Suma +"Valor_Negocio"+Valor_Negocio);
	//Suma=5.54022128812142 ;
	//Suma=	5.5402213;//600000000/108298923
	Valor_Cheque = Util.redondear((Valor_Negocio/(Suma)),0);
	//System.out.println(" Valor_Cheque = "+Valor_Cheque+"Valor_Negocio"+Valor_Negocio+" suma"+Suma+"");
	//Valor_Cheque=1804982;

	//hasta aca debe estar bien porque no se ha preguntado por remesa ni aval ni custodia y se dijo que el caso sin dichas variables esta bien
	
	//si el aval lo asume el establecimiento diremos erroneamente que no hay aval y si la custodia es dino hay remesa diremos 
	
	//Valor_Cheque = Suma;    
	custodia="1";
    //System.out.println("aval"+aval+"custodia"+custodia+"remesas"+remesas+"Estado_Remesa"+Estado_Remesa+"Valor_Cheque"+Valor_Cheque+"Valor_Negocio"+Valor_Negocio+"Suma"+Suma);
		//aval 0: sin aval. //custodia 0: sin custodia //Estado_Remesa 1: sin remesa //Estado_Remesa 0 y remesa 0: con remesa el establecimiento. //Estado_Remesa 0 y remesa 1: con remesa el cliente.
		
	if (custodia.equals("1") && aval.equals("1") && Estado_Remesa.equals("1")){ //remesas.equals("0")){//__a//si hay custodia y hay aval y no hay remesa //creo que ok		
		
		Aval = model.FenalcoFintraSvc.getAvales();		
		for (int fila = 0; fila <= Numero_Cheques-1; fila++){//_//se calculara el valor inicial del aval
			
				if (   diasremix[fila]>((fila+1)*30)   )                       {
					ValorNum = (Double.parseDouble(""+Aval.get(fila+1)))/100;//_
				}else{
					ValorNum = (Double.parseDouble(""+Aval.get(fila)))/100;//_
				}					
				//ValorNum = (Double.parseDouble(""+Aval.get(fila)))/100;//_
				
				ValorAval = (Valor_Cheque * ValorNum);//_
				ValorAvalIva = (ValorAval* 0.19);//_
				TotalAval = TotalAval+(ValorAval + ValorAvalIva);//_
				//System.out.println("ValorNum"+ValorNum+"Valor_Cheque"+Valor_Cheque+"totalaval"+TotalAval);
		}//_
		//System.out.println("----------------------------------------------------------------------------------------------------------------------------------");		
		custodiax=0;
		custodiax=(Custodia_Cheque* Numero_Cheques);//custodia
		valor_operation=Valor_Negocio+TotalAval;//valor operacion inicial
		//System.out.println("operacion"+valor_operation+"Valor_Negocio"+Valor_Negocio+"TotalAval"+TotalAval);
		Valor_Cheque = /*Math.round*/(((valor_operation)/Suma) );//_//valor segundo del cheque
		Valor_Cheque_X = /*Math.round*/(((valor_operation)/Suma));//_
		TotalAval=0;
		for (int fila = 0; fila <= Numero_Cheques-1; fila++){//_//se calculara el segundo valor del aval
		
				if (   diasremix[fila]>((fila+1)*30)   )                       {
					ValorNum = (Double.parseDouble(""+Aval.get(fila+1)))/100;//_
				}else{
					ValorNum = (Double.parseDouble(""+Aval.get(fila)))/100;//_
				}					
				//ValorNum = (Double.parseDouble(""+Aval.get(fila)))/100;//_
				
				ValorAval = (Valor_Cheque * ValorNum);//_
				ValorAvalIva = (ValorAval* 0.19);//_
				TotalAval = TotalAval+(ValorAval + ValorAvalIva);//_
				//System.out.println("ValorNum"+ValorNum+"Valor_Cheque"+Valor_Cheque+"totalaval"+TotalAval);
		}//_		
		//System.out.println("Valor_Negocio +Valor_Cheque+ TotalAval - valor_operation__________"+Valor_Negocio +"_"+Valor_Cheque+"_"+ TotalAval +"_"+ valor_operation+"custodiax"+custodiax);
		while ((Valor_Negocio +custodiax+ TotalAval - valor_operation )>= 0.5){//se calculara el valor del aval,el valor del cheque
			valor_operation=Valor_Negocio+custodiax+TotalAval;//Cells(5, "B") = Cells(5, "B") + Cells(73, "M")
			Valor_Cheque = /*Math.round*/(((valor_operation)/Suma) );//_
			Valor_Cheque_X = /*Math.round*/(((valor_operation)/Suma));//_
			//System.out.println("Valor_Negocio +Valor_Cheque+ TotalAval - valor_operation__"+Valor_Negocio +"_"+Valor_Cheque+"_"+ TotalAval +"_"+ valor_operation);
			TotalAval=0;
			for (int fila = 0; fila <= Numero_Cheques-1; fila++){
			
				if (   diasremix[fila]>((fila+1)*30)   ){	
					ValorNum = (Double.parseDouble(""+Aval.get(fila+1)))/100;
				}else{
					ValorNum = (Double.parseDouble(""+Aval.get(fila)))/100;				
				}						
				//ValorNum = (Double.parseDouble(""+Aval.get(fila)))/100;
				
				ValorAval = (Valor_Cheque * ValorNum);
				ValorAvalIva = (ValorAval* 0.19);
				TotalAval = TotalAval+(ValorAval + ValorAvalIva);
				//System.out.println("Valor_Negocio +Valor_Cheque+ TotalAval - valor_operation:"+Valor_Negocio +"_"+Valor_Cheque+"_"+ TotalAval +"_"+ valor_operation);
			}			
		}		

	}
	//aval 0: sin aval. //custodia 0: sin custodia //Estado_Remesa 1: sin remesa //Estado_Remesa 0 y remesa 0: con remesa el establecimiento. //Estado_Remesa 0 y remesa 1: con remesa el cliente.
	
	//si custodia asumida por cliente(1) se suma a valor del cheque , sino no	
	if (custodia.equals("1") && aval.equals("0") && Estado_Remesa.equals("1")  ){//remesas.equals("0")){//_//si hay custodia y no hay aval y no hay remesa //creo que ok		
		custodiax=0;
		custodiax=(Custodia_Cheque* Numero_Cheques);
		//System.out.println("Custodia_Cheque"+Custodia_Cheque);
		Valor_Cheque = /*Math.round*/(((Valor_Negocio)/Suma));// + (Custodia_Cheque));
		Valor_Cheque_X = /*Math.round*/(((Valor_Negocio)/Suma));// + (Custodia_Cheque));
	}

	//si custodia asumida por cliente(1) se suma a valor del cheque , sino no	
	if (custodia.equals("1") && aval.equals("0") && Estado_Remesa.equals("1") && (1==2)){//remesas.equals("0")){//_//si hay custodia y no hay aval y no hay remesa //creo que ok		
		custodiax=0;
		custodiax=(Custodia_Cheque* Numero_Cheques);
		//System.out.println("Custodia_Cheque"+Custodia_Cheque);
		Valor_Cheque = /*Math.round*/(((Valor_Negocio)/Suma) + (Custodia_Cheque));
		Valor_Cheque_X = /*Math.round*/(((Valor_Negocio)/Suma) + (Custodia_Cheque));
	}
		
	if (custodia.equals("1") && aval.equals("0") && Estado_Remesa.equals("1") && (1==2)){//remesas.equals("0")){//_//si hay custodia y no hay aval y no hay remesa //creo que ok		
		custodiax=0;
		//custodiax=(Custodia_Cheque* Numero_Cheques);
		//System.out.println("Custodia_Cheque"+Custodia_Cheque);
		Valor_Cheque = /*Math.round*/(((Valor_Negocio)/Suma)) ;//+ (Custodia_Cheque));
		Valor_Cheque_X = /*Math.round*/(((Valor_Negocio)/Suma)) ;//+ (Custodia_Cheque));
	}
		
	if (custodia.equals("1") && aval.equals("1") && ( Estado_Remesa.equals("0") && remesas.equals("1"))){//caso ok a//si hay custodia y hay aval y la remesa la asume el cliente
		//System.out.println("puro 1"+TotalAval);
		Aval = model.FenalcoFintraSvc.getAvales();			
				
		for (int fila = 0; fila <= Numero_Cheques-1; fila++){//_
				
				if (   diasremix[fila]>((fila+1)*30)   ){	
					ValorNum = (Double.parseDouble(""+Aval.get(fila+1)))/100;
				}else{
					ValorNum = (Double.parseDouble(""+Aval.get(fila)))/100;				
				}							
				//ValorNum = (Double.parseDouble(""+Aval.get(fila)))/100;//_
				
				ValorAval = (Valor_Cheque * ValorNum);//_
				ValorAvalIva = (ValorAval* 0.19);//_
				TotalAval = TotalAval+(ValorAval + ValorAvalIva);//_
				//System.out.println("ValorNum"+ValorNum+"ValorAval"+ValorAval+"ValorAvalIva"+ValorAvalIva+"TotalAval"+TotalAval);
				//System.out.println("ValorNum"+ValorNum+"Valor_Cheque"+Valor_Cheque+"totalaval"+TotalAval);
		}//_
		//Valor_Remesa = Valor_Cheque * porrem;//valor_remesa
		TotalRemesa_x= Valor_Cheque * porrem * Numero_Cheques;//valor_remesa		
		if (TotalRemesa_x<(lim*Numero_Cheques)){				TotalRemesa_x=(lim*Numero_Cheques);			}	
		//System.out.println("ValorNum"+ValorNum+"ValorAval"+ValorAval+"ValorAvalIva"+ValorAvalIva+"TotalAval"+TotalAval);
		//System.out.println("----------------------------------------------------------------------------------------------------------------------------------");		
		custodiax=0;
		custodiax=(Custodia_Cheque* Numero_Cheques);
		valor_operation=Valor_Negocio+TotalAval+TotalRemesa_x;
		//System.out.println("Valor_Cheque"+Valor_Cheque);
		Valor_Cheque = /*Math.round*/(((valor_operation)/Suma) );//_
		Valor_Cheque_X = /*Math.round*/(((valor_operation)/Suma));//_
		TotalAval=0;
		for (int fila = 0; fila <= Numero_Cheques-1; fila++){//_
				
				if (   diasremix[fila]>((fila+1)*30)   ){	
					ValorNum = (Double.parseDouble(""+Aval.get(fila+1)))/100;
				}else{
					ValorNum = (Double.parseDouble(""+Aval.get(fila)))/100;				
				}					
				//ValorNum = (Double.parseDouble(""+Aval.get(fila)))/100;//_
				
				ValorAval = (Valor_Cheque * ValorNum);//_
				ValorAvalIva = (ValorAval* 0.19);//_
				TotalAval = TotalAval+(ValorAval + ValorAvalIva);//_
				//System.out.println("ValorNum"+ValorNum+"Valor_Cheque"+Valor_Cheque+"totalaval"+TotalAval);
		}//_		
		TotalRemesa_x= Valor_Cheque * porrem * Numero_Cheques;//valor_remesa	
		if (TotalRemesa_x<(lim*Numero_Cheques)){				TotalRemesa_x=(lim*Numero_Cheques);			}	
		//System.out.println("Valor_Negocio +Valor_Cheque+ TotalAval - valor_operation__________"+Valor_Negocio +"_"+Valor_Cheque+"_"+ TotalAval +"_"+ valor_operation);
		while ((Valor_Negocio +custodiax+ TotalAval - valor_operation + TotalRemesa_x )>= 0.5){
			valor_operation=Valor_Negocio+custodiax+TotalAval+TotalRemesa_x;//Cells(5, "B") = Cells(5, "B") + Cells(73, "M")
			Valor_Cheque = /*Math.round*/(((valor_operation)/Suma) );//_
			Valor_Cheque_X = /*Math.round*/(((valor_operation)/Suma));//_
			//System.out.println("Valor_Negocio +Valor_Cheque+ TotalAval - valor_operation__"+Valor_Negocio +"_"+Valor_Cheque+"_"+ TotalAval +"_"+ valor_operation);
			TotalAval=0;
			for (int fila = 0; fila <= Numero_Cheques-1; fila++){
			
				if (   diasremix[fila]>((fila+1)*30)   ){	
					ValorNum = (Double.parseDouble(""+Aval.get(fila+1)))/100;
				}else{
					ValorNum = (Double.parseDouble(""+Aval.get(fila)))/100;				
				}					
				//ValorNum = (Double.parseDouble(""+Aval.get(fila)))/100;
				
				ValorAval = (Valor_Cheque * ValorNum);
				ValorAvalIva = (ValorAval* 0.19);
				TotalAval = TotalAval+(ValorAval + ValorAvalIva);
				//System.out.println("Valor_Negocio +Valor_Cheque+ TotalAval - valor_operation:"+Valor_Negocio +"_"+Valor_Cheque+"_"+ TotalAval +"_"+ valor_operation);
			}		
			TotalRemesa_x= Valor_Cheque * porrem * Numero_Cheques;//valor_remesa	
			if (TotalRemesa_x<(lim*Numero_Cheques)){				TotalRemesa_x=(lim*Numero_Cheques);			}		
		}		
	}//endif
	//System.out.println("TotalAval"+TotalAval);
	
	if (custodia.equals("1") && aval.equals("0") && ( Estado_Remesa.equals("0") && remesas.equals("1"))){//caso ok a//si hay custodia y no hay aval y la remesa la asume el cliente
		Aval = model.FenalcoFintraSvc.getAvales();							
		//Valor_Remesa = Valor_Cheque * porrem;//valor_remesa
		TotalRemesa_x= Valor_Cheque * porrem * Numero_Cheques;//valor_remesa		
		if (TotalRemesa_x<(lim*Numero_Cheques)){				TotalRemesa_x=(lim*Numero_Cheques);			}	
		//System.out.println("ValorNum"+ValorNum+"ValorAval"+ValorAval+"ValorAvalIva"+ValorAvalIva+"TotalAval"+TotalAval+"TotalRemesa_x"+TotalRemesa_x);
		//System.out.println("----------------------------------------------------------------------------------------------------------------------------------");		
		custodiax=0;
		custodiax=(Custodia_Cheque* Numero_Cheques);
		TotalAval=0;
		valor_operation=Valor_Negocio+TotalAval+TotalRemesa_x;
		//System.out.println("Valor_Cheque"+Valor_Cheque+"TotalRemesa_x"+TotalRemesa_x);
		Valor_Cheque = /*Math.round*/(((valor_operation)/Suma) );//_
		Valor_Cheque_X = /*Math.round*/(((valor_operation)/Suma));//_
		TotalRemesa_x= Valor_Cheque * porrem * Numero_Cheques;//valor_remesa
		if (TotalRemesa_x<(lim*Numero_Cheques)){				TotalRemesa_x=(lim*Numero_Cheques);			}	
		//System.out.println("Valor_Negocio +Valor_Cheque+ TotalAval - valor_operation__________"+Valor_Negocio +"_"+Valor_Cheque+"_"+ TotalAval +"_"+ valor_operation);
		//System.out.println("Valor_Cheque"+Valor_Cheque+"TotalRemesa_x"+TotalRemesa_x);
		while ((Valor_Negocio +custodiax+ TotalAval - valor_operation + TotalRemesa_x )>= 1.0){
			valor_operation=Valor_Negocio+custodiax+TotalAval+TotalRemesa_x;//Cells(5, "B") = Cells(5, "B") + Cells(73, "M")
			Valor_Cheque = /*Math.round*/(((valor_operation)/Suma) );//_
			Valor_Cheque_X = /*Math.round*/(((valor_operation)/Suma));//_
			//System.out.println("Valor_Negocio +Valor_Cheque+ TotalAval - valor_operation__"+Valor_Negocio +"_"+Valor_Cheque+"_"+ TotalAval +"_"+ valor_operation);			
			TotalRemesa_x= Valor_Cheque * porrem * Numero_Cheques;//valor_remesa	
			if (TotalRemesa_x<(lim*Numero_Cheques)){				TotalRemesa_x=(lim*Numero_Cheques);			}	
			//System.out.println("Valor_Cheque"+Valor_Cheque+"TotalRemesa_x"+TotalRemesa_x);
		}				
	}
	
	//System.out.println("TotalAval"+TotalAval);
	
	if (custodia.equals("1") && aval.equals("1") && ( Estado_Remesa.equals("0") && remesas.equals("0"))){//caso ok a//si hay custodia y hay aval y la remesa la asume el establecimiento
		//System.out.println("puro 1"+TotalAval);
		Aval = model.FenalcoFintraSvc.getAvales();							
		for (int fila = 0; fila <= Numero_Cheques-1; fila++){//_
		
				if (   diasremix[fila]>((fila+1)*30)   ){	
					ValorNum = (Double.parseDouble(""+Aval.get(fila+1)))/100;
				}else{
					ValorNum = (Double.parseDouble(""+Aval.get(fila)))/100;				
				}					
				//ValorNum = (Double.parseDouble(""+Aval.get(fila)))/100;//_
				
				ValorAval = (Valor_Cheque * ValorNum);//_
				ValorAvalIva = (ValorAval* 0.19);//_
				TotalAval = TotalAval+(ValorAval + ValorAvalIva);//_
				//System.out.println("ValorNum"+ValorNum+"ValorAval"+ValorAval+"ValorAvalIva"+ValorAvalIva+"TotalAval"+TotalAval);
				//System.out.println("ValorNum"+ValorNum+"Valor_Cheque"+Valor_Cheque+"totalaval"+TotalAval);
		}//_
		//Valor_Remesa = Valor_Cheque * porrem;//valor_remesa
		//TotalRemesa_x= Valor_Cheque * porrem * Numero_Cheques;//valor_remesa		
		TotalRemesa_x=0;
		//System.out.println("ValorNum"+ValorNum+"ValorAval"+ValorAval+"ValorAvalIva"+ValorAvalIva+"TotalAval"+TotalAval);
		//System.out.println("----------------------------------------------------------------------------------------------------------------------------------");		
		custodiax=0;
		custodiax=(Custodia_Cheque* Numero_Cheques);
		valor_operation=Valor_Negocio+TotalAval+TotalRemesa_x;
		//System.out.println("Valor_Cheque"+Valor_Cheque);
		Valor_Cheque = /*Math.round*/(((valor_operation)/Suma) );//_
		Valor_Cheque_X = /*Math.round*/(((valor_operation)/Suma));//_
		TotalAval=0;
		for (int fila = 0; fila <= Numero_Cheques-1; fila++){//_
		
				if (   diasremix[fila]>((fila+1)*30)   ){	
					ValorNum = (Double.parseDouble(""+Aval.get(fila+1)))/100;
				}else{
					ValorNum = (Double.parseDouble(""+Aval.get(fila)))/100;				
				}					
				//ValorNum = (Double.parseDouble(""+Aval.get(fila)))/100;//_
				
				ValorAval = (Valor_Cheque * ValorNum);//_
				ValorAvalIva = (ValorAval* 0.19);//_
				TotalAval = TotalAval+(ValorAval + ValorAvalIva);//_
				//System.out.println("ValorNum"+ValorNum+"Valor_Cheque"+Valor_Cheque+"totalaval"+TotalAval);
		}//_		
		//TotalRemesa_x= Valor_Cheque * porrem * Numero_Cheques;//valor_remesa			
		//System.out.println("Valor_Negocio +Valor_Cheque+ TotalAval - valor_operation__________"+Valor_Negocio +"_"+Valor_Cheque+"_"+ TotalAval +"_"+ valor_operation);
		while ((Valor_Negocio +custodiax+ TotalAval - valor_operation + TotalRemesa_x )>= 1.0){
			valor_operation=Valor_Negocio+custodiax+TotalAval+TotalRemesa_x;//Cells(5, "B") = Cells(5, "B") + Cells(73, "M")
			Valor_Cheque = /*Math.round*/(((valor_operation)/Suma) );//_
			Valor_Cheque_X = /*Math.round*/(((valor_operation)/Suma));//_
			//System.out.println("Valor_Negocio +Valor_Cheque+ TotalAval - valor_operation__"+Valor_Negocio +"_"+Valor_Cheque+"_"+ TotalAval +"_"+ valor_operation);
			TotalAval=0;
			for (int fila = 0; fila <= Numero_Cheques-1; fila++){
			
				if (   diasremix[fila]>((fila+1)*30)   ){	
					ValorNum = (Double.parseDouble(""+Aval.get(fila+1)))/100;
				}else{
					ValorNum = (Double.parseDouble(""+Aval.get(fila)))/100;				
				}					
				//ValorNum = (Double.parseDouble(""+Aval.get(fila)))/100;
				
				ValorAval = (Valor_Cheque * ValorNum);
				ValorAvalIva = (ValorAval* 0.19);
				TotalAval = TotalAval+(ValorAval + ValorAvalIva);
				//System.out.println("Valor_Negocio +Valor_Cheque+ TotalAval - valor_operation:"+Valor_Negocio +"_"+Valor_Cheque+"_"+ TotalAval +"_"+ valor_operation);
			}		
			//TotalRemesa_x= Valor_Cheque * porrem * Numero_Cheques;//valor_remesa		
		}		
		TotalRemesa_x= Valor_Cheque * porrem * Numero_Cheques;//valor_remesa	
		if (TotalRemesa_x<(lim*Numero_Cheques)){
			TotalRemesa_x=(lim*Numero_Cheques);
		}
	}//endif
	
	if (custodia.equals("1") && aval.equals("0") && ( Estado_Remesa.equals("0") && remesas.equals("0")) ){//caso ok a//si hay custodia y no hay aval y la remesa la asume el establecimiento			
		custodiax=0;
		custodiax=(Custodia_Cheque* Numero_Cheques);
		Valor_Cheque = /*Math.round*/(((Valor_Negocio)/Suma));// + (Custodia_Cheque));
		Valor_Cheque_X = /*Math.round*/(((Valor_Negocio)/Suma));// + (Custodia_Cheque));
		//////////////////////////////////////////////////////////////////////////////////////
		TotalAval=0;
		TotalRemesa_x= (Valor_Cheque+Custodia_Cheque) * porrem * Numero_Cheques;//valor_remesa	
		if (TotalRemesa_x<(lim*Numero_Cheques)){
			TotalRemesa_x=(lim*Numero_Cheques);
		}
	}

	if (custodia.equals("1") && aval.equals("0") && ( Estado_Remesa.equals("0") && remesas.equals("0")) && (1==2)){//caso ok a//si hay custodia y no hay aval y la remesa la asume el establecimiento			
		custodiax=0;
		custodiax=(Custodia_Cheque* Numero_Cheques);
		Valor_Cheque = Math.round(((Valor_Negocio)/Suma) + (Custodia_Cheque));
		Valor_Cheque_X = Math.round(((Valor_Negocio)/Suma) + (Custodia_Cheque));
		//////////////////////////////////////////////////////////////////////////////////////
		TotalAval=0;
		TotalRemesa_x= Valor_Cheque * porrem * Numero_Cheques;//valor_remesa	
		if (TotalRemesa_x<(lim*Numero_Cheques)){
			TotalRemesa_x=(lim*Numero_Cheques);
		}
	}
	
	///
	
	///
	
	//System.out.print("TotalRemesa_x"+TotalRemesa_x+"lim*Numero_Cheques"+(lim*Numero_Cheques)+"porrem"+porrem);
	
	//Valor_Remesa = (Valor_Cheque * porrem);
	if ((Valor_Cheque * porrem) < lim){//si el valor de la remesa es menor que el limite
		Valor_Remesa = lim;//valor_remesa
	}else{//si el valor de la remesa es mayor o igual que el limite
		Valor_Remesa = Valor_Cheque * porrem;//valor_remesa
	}
	
	//si custodia es asumida por establecimiento se resta a desembolso , sino no
	//si aval es asumido por establecimiento se ignora, sino se suma a desembolso
	//si remesa es asumida por establecimiento se resta a desembolso , si no no
	//ojo con remesa
	
//	if (custodia.equals("1") && aval.equals("1") /*&& remesas.equals("0")*/){//si hay aval y custodia
//		Desembolso = Valor_Negocio + TotalAval /*-(Valor_Remesa*Numero_Cheques)*/;//valor_desembolso
//	}else if (aval.equals("1") && custodia.equals("0") /*&& remesas.equals("0")*/){//no
//		Desembolso = (Valor_Negocio+TotalAval)-(Custodia_Cheque* Numero_Cheques)/*-(Valor_Remesa*Numero_Cheques)*/; //valor_desembolso
//	}else if (custodia.equals("1") && aval.equals("0") /*&& remesas.equals("0")*/){
//		Desembolso = Valor_Negocio  /*-(Valor_Remesa*Numero_Cheques)*/;//valor_desembolso
//	}else if (custodia.equals("0") && aval.equals("0") /*&& remesas.equals("0")*/){//no
//		Desembolso = Valor_Negocio-(Custodia_Cheque* Numero_Cheques) /*-(Valor_Remesa*Numero_Cheques)*/;//valor_desembolso
//	}else if (custodia.equals("1") && aval.equals("1") /*&& remesas.equals("1")*/){//caso ok
//		Desembolso = Valor_Negocio + TotalAval;//valor_desembolso
//	}else if (aval.equals("1") && custodia.equals("0") /*&& remesas.equals("1")*/){//no
//		Desembolso = (Valor_Negocio+TotalAval)-(Custodia_Cheque* Numero_Cheques); //valor_desembolso
//	}else if (custodia.equals("1") && aval.equals("0") /*&& remesas.equals("1")*/){
//		Desembolso = Valor_Negocio;//valor_desembolso
//	}else if (custodia.equals("0") && aval.equals("0") /*&& remesas.equals("1")*/){//no
//		Desembolso = Valor_Negocio-(Custodia_Cheque* Numero_Cheques);//valor_desembolso
//	}

	if (aval.equals("0") ){//si no hay aval 
		Desembolso=Valor_Negocio;
	}
	if (aval.equals("1") ){//si hay aval 
		//System.out.println("Valor_Negocio"+Valor_Negocio+"TotalAval"+TotalAval);
		Desembolso=Valor_Negocio+TotalAval;
	}
			
	if (custodia.equals("1") && aval.equals("1") && ( Estado_Remesa.equals("0") && remesas.equals("0"))){//caso ok a//si hay custodia y hay aval y la remesa la asume el establecimiento			
		Desembolso=Desembolso-TotalRemesa_x;
	}
	
	if (custodia.equals("1") && aval.equals("0") && ( Estado_Remesa.equals("0") && remesas.equals("0"))){//caso ok a//si hay custodia y hay aval y la remesa la asume el establecimiento			
		Desembolso=Desembolso-TotalRemesa_x;
	}
	
	if (Estado_Remesa.equals("0") && remesas.equals("0")){//si hay remesa y la asume el establecimiento
		//Desembolso=Desembolso-(Valor_Remesa*Numero_Cheques);//se le resta al desembolso
	}	
	if (Estado_Remesa.equals("0") && remesas.equals("1")){//si hay remesa y la asume el cliente 
		//se le suma al cheque
		//Valor_Cheque = Valor_Cheque+Valor_Remesa;		
		//Valor_Cheque_X = Valor_Cheque_X+Valor_Remesa;		
	}
    
    for (int fila = 1; fila <= Numero_Cheques; fila++){//para cada cheque
		chequeCartera chequeCarterax2=(chequeCartera)chequesx.get(fila-1);
		if (fila%2==0){
			colorfila = "filagris";
			}else{
			colorfila = "filaazul";}
    	
        Filas_Tabla_Liquidacion_2 = Filas_Tabla_Liquidacion_2 +"<tr class='"+colorfila+"'>";
        
        //Valor de los intereses
        //System.out.println("Valor_Cheque_X"+Valor_Cheque_X+"Formulas[fila]"+Formulas[fila]);
        Valor_Capital = (Valor_Cheque_X*Formulas[fila]);//capitalito
        Valor_Interes = (Valor_Cheque_X-Valor_Capital);//interesito
        Valor_Doc = /*Math.round*/(Valor_Capital+Valor_Interes);//valor_cheque
		if (custodia.equals("1") && aval.equals("0") && Estado_Remesa.equals("1") ){
			Valor_Capital = (Valor_Cheque_X*Formulas[fila])+Custodia_Cheque;//capitalito
			Valor_Interes = (Valor_Cheque_X-Valor_Capital)+Custodia_Cheque;//interesito
			Valor_Doc = /*Math.round*/(Valor_Capital+Valor_Interes)/*+Custodia_Cheque*/;//valor_cheque
		}
		
		if (custodia.equals("1") && aval.equals("0") && ( Estado_Remesa.equals("0") && remesas.equals("0")) ){
			Valor_Capital = (Valor_Cheque_X*Formulas[fila])+Custodia_Cheque;//capitalito
			Valor_Interes = (Valor_Cheque_X-Valor_Capital)+Custodia_Cheque;//interesito
			Valor_Doc = /*Math.round*/(Valor_Capital+Valor_Interes)/*+Custodia_Cheque*/;//valor_cheque
		}
		
		/*if( Estado_Remesa.equals("0")&& remesas.equals("1") ){//si hay remesa de cliente
			if ((Valor_Doc * porrem) < lim){//si el valor de la remesa es menor que el limite
				Valor_Doc = Valor_Doc + lim;//valor_cheque
			}else{//si el valor de la remesa es mayor o igual que el limite
				Valor_Doc = Valor_Doc +(Valor_Doc * porrem);//valor_cheque
			}
		}*/
		//Valor_Cheque = Suma; 
       
        if (fila == 1) 
        {
			//si el aval lo asume el cliente se le suma al saldo, sino no
            Saldo_Inicial = /*Math.round*/(Valor_Negocio);//saldo inicial
			/*if (custodia.equals("1") && aval.equals("1")){//caso ok  ya
				 Saldo_Inicial = /*Math.round(Valor_Negocio+ TotalAval);
			}else if (aval.equals("1") && custodia.equals("0")){
				 Saldo_Inicial = /*Math.round(Valor_Negocio+ TotalAval);
			}else if (custodia.equals("1") && aval.equals("0")){
			 	Saldo_Inicial = /*Math.round(Valor_Negocio);
			}else if (custodia.equals("0") && aval.equals("0")){
			 	Saldo_Inicial = /*Math.round(Valor_Negocio);
			}*/

			if (aval.equals("1")){//si hay aval
				Saldo_Inicial = /*Math.round*/(Valor_Negocio+ TotalAval);//saldo inicial
			}
			if (aval.equals("0")){//si no hay aval
				Saldo_Inicial = /*Math.round*/(Valor_Negocio);//saldo inicial
			}
			Saldo_Inicial=Saldo_Inicial+custodiax;
			if (custodia.equals("1") && aval.equals("0") && ( Estado_Remesa.equals("0") && remesas.equals("1"))){
				Saldo_Inicial=Saldo_Inicial+TotalRemesa_x;
			}
			
			if (custodia.equals("1") && aval.equals("1") && ( Estado_Remesa.equals("0") && remesas.equals("1"))){
				Saldo_Inicial=Saldo_Inicial+TotalRemesa_x;
			}
			
        }else{
            Saldo_Inicial = /*Math.round*/(Saldo_Final);//nuevo saldo inicial
        }
        
        Saldo_Final = /*Math.round*/(Saldo_Inicial - Valor_Capital);//saldo final
           
		//parece que aca termina la logica y lo que viene es visual       
        
        Filas_Tabla_Liquidacion_2 = Filas_Tabla_Liquidacion_2 +"    <td>";
        Filas_Tabla_Liquidacion_2 = Filas_Tabla_Liquidacion_2 +"    <p align='right' >"+ String.valueOf(Util.customFormat(Saldo_Inicial)) +"</p>";
        Filas_Tabla_Liquidacion_2 = Filas_Tabla_Liquidacion_2 +"    </td>";

		chequeCarterax2.setSaldoInicial(""+Saldo_Inicial);

		if (mostrarTodo){		
			Filas_Tabla_Liquidacion_2 = Filas_Tabla_Liquidacion_2 +"    <td>";
			Filas_Tabla_Liquidacion_2 = Filas_Tabla_Liquidacion_2 +"    <p align='center' >"+ String.valueOf(Util.customFormat(Valor_Capital)) +""+""+"</p>";///
			Filas_Tabla_Liquidacion_2 = Filas_Tabla_Liquidacion_2 +"    </td>";
		}
		
		chequeCarterax2.setValorCapital(""+Valor_Capital);

		double valor_aval99=0;
		String porci="0";
		if (mostrarTodo){
			Filas_Tabla_Liquidacion_2 = Filas_Tabla_Liquidacion_2 +"    <td>";
			Filas_Tabla_Liquidacion_2 = Filas_Tabla_Liquidacion_2 +"    <p align='center' >"+ String.valueOf(Util.customFormat(Valor_Interes)) +"</p>";
			Filas_Tabla_Liquidacion_2 = Filas_Tabla_Liquidacion_2 +"    </td>";
			if (Aval!=null){			
				valor_aval99=Valor_Doc*1.16*(Double.parseDouble(""+Aval.get(fila)))/100;
				porci=(""+valor_aval99) +"_"+Aval.get(fila);
			}			
			Filas_Tabla_Liquidacion_2 = Filas_Tabla_Liquidacion_2 +"    <td>";
			Filas_Tabla_Liquidacion_2 = Filas_Tabla_Liquidacion_2 +"    <p align='center' >"+porci +"</p>";			
			Filas_Tabla_Liquidacion_2 = Filas_Tabla_Liquidacion_2 +"    </td>";
			
		}else{
			if (Aval!=null){			
				valor_aval99=Valor_Doc*1.16*(Double.parseDouble(""+Aval.get(fila)))/100;
				porci=(""+valor_aval99) +"_"+Aval.get(fila);
			}			
		}
		        
		chequeCarterax2.setValorInteres(""+Valor_Interes);
		chequeCarterax2.setNoAval(""+valor_aval99);//090605
		
        Filas_Tabla_Liquidacion_2 = Filas_Tabla_Liquidacion_2 +"    <td>";
        Filas_Tabla_Liquidacion_2 = Filas_Tabla_Liquidacion_2 +"    <p align='right' >"+ String.valueOf(Util.customFormat(Valor_Doc)) +"</p>";//clave
        Filas_Tabla_Liquidacion_2 = Filas_Tabla_Liquidacion_2 +"    </td>";
		
		chequeCarterax2.setValor(""+Valor_Doc);
		
		Filas_Tabla_Liquidacion_2 = Filas_Tabla_Liquidacion_2 +"    <td>";
		if (fila == Numero_Cheques){
			Filas_Tabla_Liquidacion_2 = Filas_Tabla_Liquidacion_2 +"    <p align='right' >0.00</p>";
			chequeCarterax2.setSaldoFinal("0");
		}else{
        	Filas_Tabla_Liquidacion_2 = Filas_Tabla_Liquidacion_2 +"    <p align='right' >"+ String.valueOf(Util.customFormat(Saldo_Final)) +"</p>";
			chequeCarterax2.setSaldoFinal(""+Saldo_Final);
		}
		
        Filas_Tabla_Liquidacion_2 = Filas_Tabla_Liquidacion_2 +"    </td>";
        
        Filas_Tabla_Liquidacion_2 = Filas_Tabla_Liquidacion_2 +"</tr>";
		
		//chequesx.add(chequeCarterax);
    } 
        
%>
<a onClick="javascript:imprSelec('seleccion')"><img src="<%=BASEURL%>/images/imprime.gif" width="70" height="19" style="cursor:hand"></a> 
<DIV ID="seleccion">
<table align="center" width="81%" border="0" cellpadding='0' cellspacing='0'>
        <tr>
            <td >
    
                <table align="center" width="100%" cellpadding='0' cellspacing='0' class="tablaInferior" border="1" >

					<tr class="fila">
                        <td colspan = "2" align="center">
                            <p><b>Información del Negocio</b></p>
                        </td>
                        
                    </tr>
			
                    <tr class="filagris">
                        <td width='50%'>
                            <p><b>Valor del negocio</b></p>
                        </td>
                        <td width='50%'>
                            <p ><%=Util.customFormat(Valor_Negocio)%></p>
                        </td>
                    </tr>
                    <tr class="filaazul">
                        <td >
                            <p><b># Doc.</b></p>
                        </td>
                        <td>
                            <p ><%=Numero_Cheques%></p>
                        </td>
                    </tr>
                    <tr class="filagris">
                        <td >
                            <p><b>Valor desembolso</b></p>
                        </td>
                        <td>
                            <p ><%=Util.customFormat(Desembolso)%></p>
                        </td>
                    </tr>
                    <tr class="filaazul">
                        <td >
                            <p><b>Valor del Doc.</b></p>
                        </td>
                        <td>
						<%if ((custodia.equals("1") && aval.equals("0") && Estado_Remesa.equals("1")  ) || (custodia.equals("1") && aval.equals("0") && ( Estado_Remesa.equals("0") && remesas.equals("0")) )){ %>						
                            <p ><%=Util.customFormat(Valor_Cheque+Custodia_Cheque)%></p>
						<%}else{%>
							<p ><%=Util.customFormat(Valor_Cheque)%></p>
						<%}%>
                        </td>
                    </tr>
                    <tr class="filagris">
                        <td >
                            <p><b>Fecha negocio</b></p>
                        </td>
                        <td>
                            <p ><%=Util.ObtenerFechaCompleta(Fecha_Negocio)%></p>
                        </td>
                    </tr>
                </table>
            </td>
            <td >
                <table align="center" width="100%" cellpadding='0' cellspacing='0' class="tablaInferior" border="1" >

					<tr class="fila">
						<td width='25%' align="center">
                            <p><b></b></p>
                        </td>
                        <td width='50%' align="center">
                            <p><b>Establecimiento de Comercio</b></p>
                        </td>
						<td width='25%' align="center">
                            <p><b>Cliente</b></p>
                        </td>
                        
                    </tr>
			
                    <tr class="filagris">
                        <td width='25%' align="center">
                            <p><b>Custodia</b></p>
                        </td>
                        <td width='50%' align="center">
                            <%if (custodia.equals("0")){
									%><p><b>SI</b></p><%}else{%><p><b>NO</b></p><%}%>
                        </td>
						<td width='25%' align="center">
                            <p><b><%if (custodia.equals("0")){
									%><p><b>NO</b></p><%}else{%><p><b>SI</b></p><%}%>
                        </td>
                    </tr>
                    <tr class="filaazul">
                       <td width='25%' align="center">
                            <p><b>Aval</b></p>
                      </td>
                        <td width='50%' align="center">
                            <%if (aval.equals("0")){
									%><p><b>SI</b></p><%}else{%><p><b>NO</b></p><%}%>
                        </td>
						<td width='25%' align="center">
                            <p><b><%if (aval.equals("0")){
									%><p><b>NO</b></p><%}else{%><p><b>SI</b></p><%}%>
                        </td>
                    </tr>
					<%if (Estado_Remesa.equals("0")){
									%><tr class="filagris">
                       <td width='25%' align="center">
                            <p><b>Remesas</b></p>
                      </td>
                        <td width='50%' align="center">
                            <%if (remesas.equals("0")){
									%><p><b>SI</b></p><%}else{%><p><b>NO</b></p><%}%>
                        </td>
						<td width='25%' align="center">
                            <p><b><%if (remesas.equals("0")){
									%><p><b>NO</b></p><%}else{%><p><b>SI</b></p><%}%>
                        </td>
                    </tr>
                        <%}else{%>
					<tr class="filagris">
                       <td width='25%' align="center">
                            <p><b>Remesas</b></p>
                      </td>
                        <td width='50%' align="center">
                            <p><b>NO</b></p>
                        </td>
						<td width='25%' align="center">
                            <p><b><p><b>NO</b></p>
                        </td>
                    </tr>
					<%}%>               
                </table>
            </td>
        </tr>
        <tr>
            <td colspan='2'>
                <br><br>
            </td> 
        </tr>
        <tr>
            <td width='40%'>
            
                <table width="100%" cellpadding='0' cellspacing='0' class="tablaInferior" border="1">
                    <tr class="fila">
                        <td>
                            <p align='center'><b># Doc.</b></p>
                        </td>
                        <td>
                            <p align='center'><b>Fecha</b></p>                                
                        </td>
						<%if (mostrarTodo){%>
						   <td bgcolor='green'>
								<p align='center'><b>Dias</b></p>
							</td>						
							<!--<td width='0' bgcolor='green'>
								<p align='center'><b>Formula</b></p>
							</td>-->
						<%}%>
                    </tr>
                    <%=Filas_Tabla_Liquidacion%> 
					<tr >
						<td> </td>
						<td align="center" class="fila style1"> Total a Pagar   </td>
					</tr> 
                </table>
                
          </td>
          <td width='40%'>
                <table width="100%" cellpadding='0' cellspacing='0' class="tablaInferior" border="1">
                    <tr class="fila">
                        <td>
                            <p align='center'><b>Saldo inicial</b></p>
                        </td>
						<%if (mostrarTodo){%>
							<td bgcolor='green'>
								<p align='center'><b>Capital</b></p>
							</td>
							<td bgcolor='green'>
								<p align='center'><b>Intereses</b></p>
							</td>
							<td bgcolor='green'>
								<p align='center'><b>Aval</b></p>
							</td>
						<%}%>
                        <td>
                            <p align='center'><b>Vr <%if(tipo_negocio.equals("01")){%>Cheques<%}else{%>Letras<%}%></b></p>
                        </td>
					
                        <td>
                            <p align='center'><b>Saldo final</b></p>
                        </td>
                    </tr>
                    <%=Filas_Tabla_Liquidacion_2%>
					<tr >
						<td align="right" colspan="2" class="fila style1"> <%=Util.customFormat(Valor_Doc*Numero_Cheques)%></td>
					</tr> 
                </table>
				</DIV>	
          </td>
        </tr>
		
        <tr>
            <td colspan='2'>
                <p>&nbsp;</p>
                <p align='center'> 
				<%
				if(aval.equals("0") && custodia.equals("0") && remesas.equals("0")){
					aval="0";
					custodia="0";
					if(Estado_Remesa.equals("0")){
						remesas="0";
					}else{
						remesas="1";
					}
				%>
					<!--<img src="<%//=BASEURL%>/images/botones/Custodia.gif"   height="21"  title='Custodia'  onClick="location.href='<%//=CONTROLLER%>?estado=Fenalco&accion=Fintra3&opcion=CalcularCustodia&numero_cheques=<%=ParNumero_Cheques%>&valor_desembolso=<%=ParValor_Negocio%>&forma_pago=<%=Forma_Pago%>&fechainicio=<%=fechai%>&Nit=<%=propietario%>&custodia=<%=custodia%>&aval=<%=aval%>&tipo_negocio=<%=tipo_negocio%>&remesas=<%=remesas%>&remesa=<%=Estado_Remesa%>'" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" style="cursor:hand">-->
					<img src="<%=BASEURL%>/images/botones/Aval.gif"   height="21"  title='Aval'  onClick="location.href='<%=CONTROLLER%>?estado=Fenalco&accion=Fintra3&opcion=CalcularAval&numero_cheques=<%=ParNumero_Cheques%>&valor_desembolso=<%=ParValor_Negocio%>&forma_pago=<%=Forma_Pago%>&fechainicio=<%=fechai%>&Nit=<%=propietario%>&custodia=<%=custodia%>&aval=<%=aval%>&tipo_negocio=<%=tipo_negocio%>&remesas=<%=remesas%>&remesa=<%=Estado_Remesa%>&liquidador=Xno'" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" style="cursor:hand" >
					<%if(Estado_Remesa.equals("0")){%>
						<img src="<%=BASEURL%>/images/botones/Remesas.gif"   height="21"  title='Remesas'  onClick="location.href='<%=CONTROLLER%>?estado=Fenalco&accion=Fintra3&opcion=CalcularRemesas&numero_cheques=<%=ParNumero_Cheques%>&valor_desembolso=<%=ParValor_Negocio%>&forma_pago=<%=Forma_Pago%>&fechainicio=<%=fechai%>&Nit=<%=propietario%>&custodia=<%=custodia%>&aval=<%=aval%>&tipo_negocio=<%=tipo_negocio%>&remesas=<%=remesas%>&remesa=<%=Estado_Remesa%>&liquidador=Xno'" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" style="cursor:hand" >
					<%}%>
				<%}else if(aval.equals("1") && custodia.equals("0") && remesas.equals("0")){
					aval="1";
					custodia="0";
					if(Estado_Remesa.equals("0")){
						remesas="0";
					}else{
						remesas="1";
					}
				%>	
					<!--<img src="<%//=BASEURL%>/images/botones/Custodia.gif"   height="21"  title='Custodia'  onClick="location.href='<%//=CONTROLLER%>?estado=Fenalco&accion=Fintra3&opcion=CalcularCustodia&numero_cheques=<%=ParNumero_Cheques%>&valor_desembolso=<%=ParValor_Negocio%>&forma_pago=<%=Forma_Pago%>&fechainicio=<%=fechai%>&Nit=<%=propietario%>&custodia=<%=custodia%>&aval=<%=aval%>&tipo_negocio=<%=tipo_negocio%>&remesas=<%=remesas%>&remesa=<%=Estado_Remesa%>'" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" style="cursor:hand">-->
					<img src="<%=BASEURL%>/images/botones/Aval.gif"   height="21"  title='Aval'  onClick="location.href='<%=CONTROLLER%>?estado=Fenalco&accion=Fintra3&opcion=CalcularAval&numero_cheques=<%=ParNumero_Cheques%>&valor_desembolso=<%=ParValor_Negocio%>&forma_pago=<%=Forma_Pago%>&fechainicio=<%=fechai%>&Nit=<%=propietario%>&custodia=<%=custodia%>&aval=<%=aval%>&tipo_negocio=<%=tipo_negocio%>&remesas=<%=remesas%>&remesa=<%=Estado_Remesa%>&liquidador=Xno'" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" style="cursor:hand">
					<%if(Estado_Remesa.equals("0")){%>
						<img src="<%=BASEURL%>/images/botones/Remesas.gif"   height="21"  title='Remesas'  onClick="location.href='<%=CONTROLLER%>?estado=Fenalco&accion=Fintra3&opcion=CalcularRemesas&numero_cheques=<%=ParNumero_Cheques%>&valor_desembolso=<%=ParValor_Negocio%>&forma_pago=<%=Forma_Pago%>&fechainicio=<%=fechai%>&Nit=<%=propietario%>&custodia=<%=custodia%>&aval=<%=aval%>&tipo_negocio=<%=tipo_negocio%>&remesas=<%=remesas%>&remesa=<%=Estado_Remesa%>&liquidador=Xno'" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" style="cursor:hand" >
					<%}%>
				<%}else if(custodia.equals("1")  && aval.equals("0") && remesas.equals("0")){
					aval="0";
					custodia="1";
					if(Estado_Remesa.equals("0")){
						remesas="0";
					}else{
						remesas="1";
					}
				%>
					<!--<img src="<%//=BASEURL%>/images/botones/Custodia.gif"   height="21"  title='Custodia'  onClick="location.href='<%//=CONTROLLER%>?estado=Fenalco&accion=Fintra3&opcion=CalcularCustodia&numero_cheques=<%=ParNumero_Cheques%>&valor_desembolso=<%=ParValor_Negocio%>&forma_pago=<%=Forma_Pago%>&fechainicio=<%=fechai%>&Nit=<%=propietario%>&custodia=<%=custodia%>&aval=<%=aval%>&tipo_negocio=<%=tipo_negocio%>&remesas=<%=remesas%>&remesa=<%=Estado_Remesa%>'" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" style="cursor:hand">-->
					<img src="<%=BASEURL%>/images/botones/Aval.gif"   height="21"  title='Aval'  onClick="location.href='<%=CONTROLLER%>?estado=Fenalco&accion=Fintra3&opcion=CalcularAval&numero_cheques=<%=ParNumero_Cheques%>&valor_desembolso=<%=ParValor_Negocio%>&forma_pago=<%=Forma_Pago%>&fechainicio=<%=fechai%>&Nit=<%=propietario%>&custodia=<%=custodia%>&aval=<%=aval%>&tipo_negocio=<%=tipo_negocio%>&remesas=<%=remesas%>&remesa=<%=Estado_Remesa%>&liquidador=Xno'" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" style="cursor:hand">
					<%if(Estado_Remesa.equals("0")){%>
						<img src="<%=BASEURL%>/images/botones/Remesas.gif"   height="21"  title='Remesas'  onClick="location.href='<%=CONTROLLER%>?estado=Fenalco&accion=Fintra3&opcion=CalcularRemesas&numero_cheques=<%=ParNumero_Cheques%>&valor_desembolso=<%=ParValor_Negocio%>&forma_pago=<%=Forma_Pago%>&fechainicio=<%=fechai%>&Nit=<%=propietario%>&custodia=<%=custodia%>&aval=<%=aval%>&tipo_negocio=<%=tipo_negocio%>&remesas=<%=remesas%>&remesa=<%=Estado_Remesa%>&liquidador=Xno'" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" style="cursor:hand" >
					<%}%>
					
				 <%}else if(aval.equals("1") && custodia.equals("1") && remesas.equals("0")){
				 	aval="1";
				    custodia="1";
 				    if(Estado_Remesa.equals("0")){
   				    	remesas="0";
					}else{
						remesas="1";
					}
					%>  
					<!--<img src="<%//=BASEURL%>/images/botones/Custodia.gif"   height="21"  title='Custodia'  onClick="location.href='<%//=CONTROLLER%>?estado=Fenalco&accion=Fintra3&opcion=CalcularCustodia&numero_cheques=<%=ParNumero_Cheques%>&valor_desembolso=<%=ParValor_Negocio%>&forma_pago=<%=Forma_Pago%>&fechainicio=<%=fechai%>&Nit=<%=propietario%>&custodia=<%=custodia%>&aval=<%=aval%>&tipo_negocio=<%=tipo_negocio%>&remesas=<%=remesas%>&remesa=<%=Estado_Remesa%>'" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" style="cursor:hand">-->
					<img src="<%=BASEURL%>/images/botones/Aval.gif"   height="21"  title='Aval'  onClick="location.href='<%=CONTROLLER%>?estado=Fenalco&accion=Fintra3&opcion=CalcularAval&numero_cheques=<%=ParNumero_Cheques%>&valor_desembolso=<%=ParValor_Negocio%>&forma_pago=<%=Forma_Pago%>&fechainicio=<%=fechai%>&Nit=<%=propietario%>&custodia=<%=custodia%>&aval=<%=aval%>&tipo_negocio=<%=tipo_negocio%>&remesas=<%=remesas%>&remesa=<%=Estado_Remesa%>&liquidador=Xno'" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" style="cursor:hand">
					<%if(Estado_Remesa.equals("0")){%>
						<img src="<%=BASEURL%>/images/botones/Remesas.gif"   height="21"  title='Remesas'  onClick="location.href='<%=CONTROLLER%>?estado=Fenalco&accion=Fintra3&opcion=CalcularRemesas&numero_cheques=<%=ParNumero_Cheques%>&valor_desembolso=<%=ParValor_Negocio%>&forma_pago=<%=Forma_Pago%>&fechainicio=<%=fechai%>&Nit=<%=propietario%>&custodia=<%=custodia%>&aval=<%=aval%>&tipo_negocio=<%=tipo_negocio%>&remesas=<%=remesas%>&remesa=<%=Estado_Remesa%>&liquidador=Xno'" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" style="cursor:hand" >
					<%}%>
				<%}else if( aval.equals("0") && custodia.equals("0") && remesas.equals("1")){
					aval="0";
					custodia="0";
					if(Estado_Remesa.equals("0")){
						remesas="1";
					}else{
						remesas="1";
					}
					%>  
					<!--<img src="<%//=BASEURL%>/images/botones/Custodia.gif"   height="21"  title='Custodia'  onClick="location.href='<%//=CONTROLLER%>?estado=Fenalco&accion=Fintra3&opcion=CalcularCustodia&numero_cheques=<%=ParNumero_Cheques%>&valor_desembolso=<%=ParValor_Negocio%>&forma_pago=<%=Forma_Pago%>&fechainicio=<%=fechai%>&Nit=<%=propietario%>&custodia=<%=custodia%>&aval=<%=aval%>&tipo_negocio=<%=tipo_negocio%>&remesas=<%=remesas%>&remesa=<%=Estado_Remesa%>'" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" style="cursor:hand">-->
					<img src="<%=BASEURL%>/images/botones/Aval.gif"   height="21"  title='Aval'  onClick="location.href='<%=CONTROLLER%>?estado=Fenalco&accion=Fintra3&opcion=CalcularAval&numero_cheques=<%=ParNumero_Cheques%>&valor_desembolso=<%=ParValor_Negocio%>&forma_pago=<%=Forma_Pago%>&fechainicio=<%=fechai%>&Nit=<%=propietario%>&custodia=<%=custodia%>&aval=<%=aval%>&tipo_negocio=<%=tipo_negocio%>&remesas=<%=remesas%>&remesa=<%=Estado_Remesa%>&liquidador=Xno'" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" style="cursor:hand">
					<%if(Estado_Remesa.equals("0")){%>
						<img src="<%=BASEURL%>/images/botones/Remesas.gif"   height="21"  title='Remesas'  onClick="location.href='<%=CONTROLLER%>?estado=Fenalco&accion=Fintra3&opcion=CalcularRemesas&numero_cheques=<%=ParNumero_Cheques%>&valor_desembolso=<%=ParValor_Negocio%>&forma_pago=<%=Forma_Pago%>&fechainicio=<%=fechai%>&Nit=<%=propietario%>&custodia=<%=custodia%>&aval=<%=aval%>&tipo_negocio=<%=tipo_negocio%>&remesas=<%=remesas%>&remesa=<%=Estado_Remesa%>&liquidador=Xno'" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" style="cursor:hand" >
					<%}%>
				<%}else if(aval.equals("1") && custodia.equals("0") && remesas.equals("1")){
					aval="1";
					custodia="0";
					if(Estado_Remesa.equals("0")){
						remesas="1";
					}else{
						remesas="1";
					}
					%>	
					<!--<img src="<%//=BASEURL%>/images/botones/Custodia.gif"   height="21"  title='Custodia'  onClick="location.href='<%//=CONTROLLER%>?estado=Fenalco&accion=Fintra3&opcion=CalcularCustodia&numero_cheques=<%=ParNumero_Cheques%>&valor_desembolso=<%=ParValor_Negocio%>&forma_pago=<%=Forma_Pago%>&fechainicio=<%=fechai%>&Nit=<%=propietario%>&custodia=<%=custodia%>&aval=<%=aval%>&tipo_negocio=<%=tipo_negocio%>&remesas=<%=remesas%>&remesa=<%=Estado_Remesa%>'" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" style="cursor:hand">-->
					<img src="<%=BASEURL%>/images/botones/Aval.gif"   height="21"  title='Aval'  onClick="location.href='<%=CONTROLLER%>?estado=Fenalco&accion=Fintra3&opcion=CalcularAval&numero_cheques=<%=ParNumero_Cheques%>&valor_desembolso=<%=ParValor_Negocio%>&forma_pago=<%=Forma_Pago%>&fechainicio=<%=fechai%>&Nit=<%=propietario%>&custodia=<%=custodia%>&aval=<%=aval%>&tipo_negocio=<%=tipo_negocio%>&remesas=<%=remesas%>&remesa=<%=Estado_Remesa%>&liquidador=Xno'" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" style="cursor:hand">
					<%if(Estado_Remesa.equals("0")){%>
						<img src="<%=BASEURL%>/images/botones/Remesas.gif"   height="21"  title='Remesas'  onClick="location.href='<%=CONTROLLER%>?estado=Fenalco3&accion=Fintra&opcion=CalcularRemesas&numero_cheques=<%=ParNumero_Cheques%>&valor_desembolso=<%=ParValor_Negocio%>&forma_pago=<%=Forma_Pago%>&fechainicio=<%=fechai%>&Nit=<%=propietario%>&custodia=<%=custodia%>&aval=<%=aval%>&tipo_negocio=<%=tipo_negocio%>&remesas=<%=remesas%>&remesa=<%=Estado_Remesa%>&liquidador=Xno'" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" style="cursor:hand" >
					<%}%>
				<%}else if(custodia.equals("1")  && aval.equals("0") && remesas.equals("1")){
					aval="0";
					custodia="1";
					if(Estado_Remesa.equals("0")){
						remesas="1";
					}else{
						remesas="1";
					}
					%>
					<!--<img src="<%//=BASEURL%>/images/botones/Custodia.gif"   height="21"  title='Custodia'  onClick="location.href='<%//=CONTROLLER%>?estado=Fenalco&accion=Fintra3&opcion=CalcularCustodia&numero_cheques=<%=ParNumero_Cheques%>&valor_desembolso=<%=ParValor_Negocio%>&forma_pago=<%=Forma_Pago%>&fechainicio=<%=fechai%>&Nit=<%=propietario%>&custodia=<%=custodia%>&aval=<%=aval%>&tipo_negocio=<%=tipo_negocio%>&remesas=<%=remesas%>&remesa=<%=Estado_Remesa%>'" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" style="cursor:hand">-->
					<img src="<%=BASEURL%>/images/botones/Aval.gif"   height="21"  title='Aval'  onClick="location.href='<%=CONTROLLER%>?estado=Fenalco&accion=Fintra3&opcion=CalcularAval&numero_cheques=<%=ParNumero_Cheques%>&valor_desembolso=<%=ParValor_Negocio%>&forma_pago=<%=Forma_Pago%>&fechainicio=<%=fechai%>&Nit=<%=propietario%>&custodia=<%=custodia%>&aval=<%=aval%>&tipo_negocio=<%=tipo_negocio%>&remesas=<%=remesas%>&remesa=<%=Estado_Remesa%>&liquidador=Xno'" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" style="cursor:hand">
					<%if(Estado_Remesa.equals("0")){%>
						<img src="<%=BASEURL%>/images/botones/Remesas.gif"   height="21"  title='Remesas'  onClick="location.href='<%=CONTROLLER%>?estado=Fenalco&accion=Fintra3&opcion=CalcularRemesas&numero_cheques=<%=ParNumero_Cheques%>&valor_desembolso=<%=ParValor_Negocio%>&forma_pago=<%=Forma_Pago%>&fechainicio=<%=fechai%>&Nit=<%=propietario%>&custodia=<%=custodia%>&aval=<%=aval%>&tipo_negocio=<%=tipo_negocio%>&remesas=<%=remesas%>&remesa=<%=Estado_Remesa%>&liquidador=Xno'" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" style="cursor:hand" >
					<%}%>
				<%}else if(aval.equals("1") && custodia.equals("1") && remesas.equals("1")){
					aval="1";
					custodia="1";
					if(Estado_Remesa.equals("0")){
						remesas="1";
					}else{
						remesas="1";
					}	
					%>  
					<!--<img src="<%//=BASEURL%>/images/botones/Custodia.gif"   height="21"  title='Custodia'  onClick="location.href='<%//=CONTROLLER%>?estado=Fenalco&accion=Fintra3&opcion=CalcularCustodia&numero_cheques=<%=ParNumero_Cheques%>&valor_desembolso=<%=ParValor_Negocio%>&forma_pago=<%=Forma_Pago%>&fechainicio=<%=fechai%>&Nit=<%=propietario%>&custodia=<%=custodia%>&aval=<%=aval%>&tipo_negocio=<%=tipo_negocio%>&remesas=<%=remesas%>&remesa=<%=Estado_Remesa%>'" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" style="cursor:hand">-->
					<img src="<%=BASEURL%>/images/botones/Aval.gif"   height="21"  title='Aval'  onClick="location.href='<%=CONTROLLER%>?estado=Fenalco&accion=Fintra3&opcion=CalcularAval&numero_cheques=<%=ParNumero_Cheques%>&valor_desembolso=<%=ParValor_Negocio%>&forma_pago=<%=Forma_Pago%>&fechainicio=<%=fechai%>&Nit=<%=propietario%>&custodia=<%=custodia%>&aval=<%=aval%>&tipo_negocio=<%=tipo_negocio%>&remesas=<%=remesas%>&remesa=<%=Estado_Remesa%>&liquidador=Xno'" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" style="cursor:hand">
					<%if(Estado_Remesa.equals("0")){%>
						<img src="<%=BASEURL%>/images/botones/Remesas.gif"   height="21"  title='Remesas'  onClick="location.href='<%=CONTROLLER%>?estado=Fenalco&accion=Fintra3&opcion=CalcularRemesas&numero_cheques=<%=ParNumero_Cheques%>&valor_desembolso=<%=ParValor_Negocio%>&forma_pago=<%=Forma_Pago%>&fechainicio=<%=fechai%>&Nit=<%=propietario%>&custodia=<%=custodia%>&aval=<%=aval%>&tipo_negocio=<%=tipo_negocio%>&remesas=<%=remesas%>&remesa=<%=Estado_Remesa%>&liquidador=Xno'" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" style="cursor:hand" >
					<%}%>
				<%}%>    
                &nbsp;&nbsp;
				<img src="<%=BASEURL%>/images/botones/regresar.gif" name="c_salir" onClick="location.href='<%=BASEURL%>/jsp/fenalco/Liquidacion/inicio_liquidacion.jsp';" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" align="absmiddle" style="cursor:hand ">
                <img src="<%=BASEURL%>/images/botones/salir.gif"  name="imgsalir"  onMouseOver="botonOver(this);" onClick="parent.close();" onMouseOut="botonOut(this);" style="cursor:hand">
				
          </td> 
        </tr>
		<%if (mostrarTodo){%>
			<tr>Aval: <%=Util.customFormat(TotalAval)%></tr>
			
			<tr>Remesa: <%=Util.customFormat(TotalRemesa_x)%></tr>

			<tr>valor_operation: <%=Util.customFormat(valor_operation)%></tr>			
			
			
		<%}%>	
		<tr>
			<td colspan="2" align="center"><p>&nbsp;</p>
		    <form name="form1" method="post" action="<%=CONTROLLER%>?estado=Negocios&accion=Insert">
				<input name="nit" type="hidden" value="<%=(String)session.getAttribute("nitcm")%>">
				<input name="vrnegocio" type="hidden" value="<%=ParValor_Negocio%>">
				<input name="nodocs" type="hidden" value="<%=Numero_Cheques%>">
				<input name="vrdesem" type="hidden" value="<%=Desembolso%>">
				<input name="vraval" type="hidden" value="<%=Tasa*100%>">
				<input name="vrcust" type="hidden" value="<%=Custodia_Cheque%>">
				<input name="rem" type="hidden" value="<%=Estado_Remesa%>">
				<input name="porcrem" type="hidden" value="<%=porrem%>">
				<input name="cust1" type="hidden" value="<%=custodia%>">
				<input name="aval1" type="hidden" value="<%=aval%>">
				<input name="formpago" type="hidden" value="<%=Forma_Pago%>">
				<input name="tneg" type="hidden" value="<%=tipo_negocio%>">
				<input name="codtab" type="hidden" value="<%=model.FenalcoFintraSvc.getcodtab()%>">
				<input name="remesas" type="hidden" value="<%=remesas%>">
				<input name="fechaneg" type="hidden" value="<%=fechai%>">
				<input name="nitp" type="hidden" value="<%=propietario%>">
				<input name="tpag" type="hidden" value="<%=(Valor_Doc*Numero_Cheques)%>">
				<!--<input name="liquidador" type="hidden" value="X">//=(Valor_Cheque*Numero_Cheques)-->
				
				<input name="valor_aval" type="hidden" value="<%=Math.round(TotalAval)%>">
				<input name="valor_remesa" type="hidden" value="<%=Math.round(TotalRemesa_x)%>">				
				
				<%
				
				model.Negociossvc.setCheques(chequesx);
				
				java.util.Calendar Fecha_Final = null;
				//System.out.println("Numero de Dias = "+Util.diasTranscurridos(Fecha_Negocio, Fecha_Negocio.add(Fecha_Negocio.MONTH, Numero_Cheques)));
				%>
			<p><img src="<%=BASEURL%>/images/botones/Aceptar_neg.gif"  name="aceptarneg"  onMouseOver="botonOver(this);" onClick="javascript:form1.submit()" onMouseOut="botonOut(this);" style="cursor:hand"></p>
			</form>
			
			</td>
		</tr>
  </table>
    </div>
    </body>
</html>