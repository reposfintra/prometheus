<%-- 
    Document   : EditarClientes
    Created on : 21/05/2014, 10:16:56 AM
    Author     : egonzalez
--%>

<%@page contentType="text/html" pageEncoding="ISO-8859-1"%>
<%@include file="/WEB-INF/InitModel.jsp"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
        <title>Editar Clientes</title>

        <link type="text/css" rel="stylesheet" href="<%=BASEURL%>/css/jquery/jquery-ui/jquery-ui.css"/>
        <link type="text/css" rel="stylesheet" href="<%=BASEURL%>/css/editarClientesOriginal.css"/>

        <script type="text/javascript" src="<%=BASEURL%>/js/jquery/jquery.jqGrid/js/jquery.min.js"></script>
        <script type="text/javascript" src="<%=BASEURL%>/js/jquery-ui-1.8.5.custom.min.js"></script>
        <script type="text/javascript" src="<%=BASEURL%>/js/jquery/jquery.jqGrid/js/grid.locale-es.js"></script>
        <script type="text/javascript" src="<%=BASEURL%>/js/jquery/jquery.jqGrid/js/jquery.jqGrid.min.js"></script>
        <script type="text/javascript" src="<%=BASEURL%>/js/editarClientesOriginal.js"></script>
    </head>
    <style>
        .error{
            border-color: #FF9494;
            border-width: 2px;
        }
        
    </style>

    <body>
        <div id="capaSuperior" style="position:absolute; width:100%; height:100px; z-index:0; left: 1px; top: 0px;">
            <jsp:include page="/toptsp.jsp?encabezado=Editar Clientes"/>
        </div>
        <div id="capaCentral" style="position:absolute; width:100%; height:87%; z-index:0; left: 0px; top: 100px; overflow: scroll;">
            <center>
                <div class="form_editar_cliente">
                    <div class="header_cliente"><span class="titulo">ACTUALIZAR CLIENTE</span> </div>
                    <div id="contenido">
                     <table border="0" class="tabla">
                            <tr>
                                <td colspan="3">
                                    <table>
                                        <tr>
                                            <td class="td">
                                                <label for="codigo">Codigo</label><br>
                                                <input id="codigo" name="codigo" size="10" type="text" readonly />
                                            </td>
                                            <td class="td">

                                                <label for="nit">Nit � Cedula</label> <br>
                                                <input id="nit" name="nit" size="12" maxlength="10" type="text" />
                                                &nbsp;
                                                <img src="<%=BASEURL%>/images/botones/iconos/buscar.gif" onclick="buscarCliente()"  />

                                            </td>
                                            <td class="td">
                                                <label for="name">Nombre Cliente</label><br>
                                                <input  id="name" name="name" size="53" type="text" />
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                            <tr>
                                <td class="td">
                                    <label for="direccion">Direccion</label><br/>
                                    <input id="direccion" name="direccion" size="35" type="text" readonly/> 
                                    &nbsp;
                                    <img src="/fintra/images/Direcciones.png" width="26" height="23" border="0" align="ABSMIDDLE" onclick="genDireccion('direccion',event);" alt="Direcciones"  title="Direcciones" />
                                </td>
                                <td class="td">
                                    <label for="">Barrio</label><br/>
                                    <input id="barrio" name="barrio" size="20" type="text" />
                                </td>
                                <td class="td">
                                    <label for="cooddpto">Departamento</label><br/>
                                    <select id="cooddpto" name="cooddptos" ></select>
                                </td>
                            </tr>
                            <tr>
                                <td class="td" colspan="3">
                                    <table>
                                        <tr>
                                            <td>  <label for="codciu">Ciudad</label><br/>
                                                <select id="codciu" name="codciu"></select>
                                            </td>
                                            <td class="td">
                                                <label for="telefono">Telefono</label><br/>
                                                <input id="telefono" name="telefono" size="20" type="text" />
                                            </td>
                                            <td class="td">
                                                <label style="margin-left:25px " for="celular">Celular</label><br/>
                                                <input  style="margin-left:25px " id="celular" name="celular" size="20" type="text" />
                                            </td>

                                        </tr>

                                    </table>
                                </td> 
                            </tr>
                             <tr>
                                <td class="td" colspan="3">
                                    <table>
                                        <tr>                                            
                                            <td class="td">
                                                <label for="email">Email</label><br/>
                                                <input id="email" name="email" size="60" onblur="validateMail()" type="text"/>
                                            </td>                                           

                                        </tr>

                                    </table>
                                </td> 
                            </tr>
                            <tr>
                                <td class="td" colspan="10">
                                    <label >Observaciones</label>
                                    <textarea id="observaciones" name="observaciones"  type="text" onchange="conMayusculas(this)" style="width:60em;height: 5em;" > </textarea>
                                </td>
                            </tr>
                            <tr>
                                <td class="td" colspan="10">
                                <table id="tabla_negocios">
                            <tbody id="tabla_negocios">
                                 <thead>
                                <tr>
                                    <th class="th1">Negocio</th>
                                    <th class="th1">Numero Solicitud</th>
                                    <th class="th1">Extracto Email</th>
                                    <th class="th1">Extracto Correspondencia</th>
                                </tr>
                            </thead>
                            </tbody>
                        </table>
                                </td>
                            </tr>
                            
                        </table>                                                
                        <br>
                        <hr>
                        <div style="text-align: right;margin-right: 28px">
                            <button id="actulizar" class="ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only" role="button" aria-disabled="false">
                                <span class="ui-button-text">Actualizar</span>
                            </button>
                            
                            <button id="limpiar" class="ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only" role="button" aria-disabled="false">
                                <span class="ui-button-text">Limpiar</span>
                            </button>

                            <button id="salir" class="ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only" role="button" aria-disabled="false">
                                <span class="ui-button-text">Salir</span>
                            </button>
                        </div>
                    </div>
            </center>
        </div>         
        <div id="dialogo" class="ventana" title="Mensaje">
            <p  id="msj">texto </p>
        </div>
        <div id="dialogo2" class="ventana">
            <p  id="msj2">texto </p> <br/>
            <center>
                <img src="<%=BASEURL%>/images/cargandoCM.gif"/>
            </center>
        </div>
        <div id="direccion_dialogo" style="display:none;left: 0; position: absolute; top: 0;padding: 1em 1.2em; margin:0px auto; margin-top:10px; padding:10px; width:370px; min-height:140px; border-radius:4px; background-color:#FFFFFF; box-shadow: 0 2px 5px #666666;"> 
                <div>
                <table style="width: 100%;">
                    
                    <tr>
                        <td class="titulo_ventana" id="drag_direcciones" colspan="3">
                            <div style="float:center">FORMATO DIRECCIONES<input type="hidden" name="id_usuario" id="id_usuario" value="coco"/></div> 
                        </td>
                    </tr>                    
                    <tr>  
                        <td><span>Departamento:</span></td>                     
                        <td colspan="2"> <select id="dep_dir" name="dep_dir" style="width:20em;"></select>
                        </td>
                    </tr>
                    <tr>
                        <td><span>Ciudad:</span></td>     
                        <td colspan="2">                            
                            <div id="d_ciu_dir">
                                <select id="ciu_dir" name="ciu_dir" style="width:20em;"></select>
                            </div>
                        </td>
                    </tr>  
                    <tr>
                        <td>Via Principal</td>
                        <td>
                            <select id="via_princip_dir" onchange="setDireccion(2)">                                
                            </select>
                        </td>
                        <td><input type="text" id="nom_princip_dir" style="width: 100%;" onchange="setDireccion(1)"/></td>
                    </tr>
                    <tr>
                        <!-- <td>Via Generadora</td> -->
                        <td>Numero</td>
                        <td>
                            <select id="via_genera_dir" onchange="setDireccion(1)">                               
                            </select>
                        </td>
                        
                        <td>
                            <table width="100%" border="0">
                                <tr>
                                    <td align="center" width="49%">
                                        <input type="text" id="nom_genera_dir" style="width: 90%;" onchange="setDireccion(1)"/>
                                    </td>
                                    <td width="2%" align="center"> - </td>
                                    <td align="center" width="49%">
                                        <input type="text" id="placa_dir" style="width: 100%;" onkeypress="setDireccion(1)" onchange="setDireccion(1)"/>
                                    </td>    
                                </tr>
                            </table>                        
                        </td>
                    </tr>
                    <tr>
                        <td>Complemento</td>
                        <td colspan="2"><input type="text" id="cmpl_dir" style="width: 100%;" onkeypress="setDireccion(1)" onchange="setDireccion(1)"/></td>
                    </tr>
                    <tr>
                        <td colspan="3"><input type="text" id="dir_resul" name="" style="width: 100%;" readonly/></td>
                    </tr>
                    <tr>
                        <td colspan="3">
                            <button onclick="setDireccion(3);">Aceptar</button>
                            <button onclick="setDireccion(0);">Salir</button>
                        </td>
                    </tr>
                </table>
                </div>
        </div>
    </body>
</html>
