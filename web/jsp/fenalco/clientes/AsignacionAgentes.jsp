<%-- 
    Document   : AsignacionAgentes
    Created on : 30/07/2014, 09:12:41 AM
    Author     : jpacosta
--%>

<%@page contentType="text/html" pageEncoding="ISO-8859-1"%>
<%@page session="true"%>
<%@include file="/WEB-INF/InitModel.jsp"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
        <title>Asignacion Agentes de Campo</title>
        <link href="/fintra/css/estilostsp.css" rel="stylesheet" type="text/css">
        
        <link type="text/css" rel="stylesheet" href="./css/jquery/jquery-ui/jquery-ui.css"/>
        <link type="text/css" rel="stylesheet" href="./css/jquery/jquery.jqGrid/ui.jqgrid.css"/>        
        <script type="text/javascript" src="./js/jquery/jquery-ui/jquery.min.js"></script>
        <script type="text/javascript" src="./js/jquery/jquery-ui/jquery.ui.min.js"></script>            
        <script type="text/javascript" src="./js/jquery/jquery.jqGrid/js/grid.locale-es.js"></script>
        <script type="text/javascript" src="./js/jquery/jquery.jqGrid/js/jquery.jqGrid.min.js"></script>
        <style type="text/css">
            button {
                border: rgb(128,128,128) 1px solid;
                border-color: black;
                font-size: 10pt; 
                font-family: Verdana; 
                text-shadow: -1px 0 black, 0 1px black, 1px 0 black, 0 -1px black;
                background-color: #398b56; 
                color:white;
                background-repeat: no-repeat;
                padding-top: 2px; 
            }
            button:hover {
                text-shadow: -1px 0 white, 0 1px black, 1px 0 black, 0 -1px white;
            }
            .ventana{
                display:none;    
                font-family:Arial, Helvetica, sans-serif;
                color:#808080;
                line-height:28px;
                font-size:12px;
                text-align:justify;
            }
        </style>
    </head>
    <body align="center">
        <div id="capaSuperior" style="position:absolute; width:100%; height:100px; z-index:0; left: 0px; top: 0px;">
            <jsp:include page="/toptsp.jsp?encabezado=Asignación Agentes de Campo"/>
        </div>

        <div id="capaCentral" style="position:absolute; width:100%; height:100%; z-index:0; left: 0px; top: 100px;">
            <table width='80%' align="center" style="border: green solid 1px;">
                    <tr>
                        <th colspan='4' class="barratitulo">
                            <table width="100%">
                                <tr>
                                    <td width="40%" class="subtitulo1">Filtros de consulta y asignación</td>
                                    <td width="60%"><img src="<%=BASEURL%>/images/titulo.gif" width="32" height="20" align="left"></td>
                                </tr>
                            </table>
                        </th>
                    </tr>
                    <tr class='fila'>
                        <td>Periodo</td>
                        <td><input id="periodos" name="periodos" style="width: 1 em;"/></td>
                    </tr>
                    <tr class='fila'>
                        <td>Unidad de Negocio</td>
                        <td><select id="uNegocios" name="uNegocios" class="grande" onchange="mostrarAgencia()"></select></td>
                    </tr>
                    <tr class='fila' id="fila-agencia">
                        <td>Agencia</td>
                        <td><select id="agencias" name="agencias" class="grande">
                                <option value="undefined">...</option>
                            </select></td>
                    </tr>
                    <tr class='fila'>
                        <td>Rango Mayor</td>
                        <td><select id="vencMayor" name="vencMayor" class="grande"></select></td>
                    </tr>
                    <tr class='fila'>
                        <td>Asignar a</td>
                        <td><select id="agentes" name="agentes" class="grande"></select></td>
                    </tr>
                    <tr class='fila'>
                        <td>Reasignacion   <input type="checkbox" id="reasignacion" name="reasignacion"></td>
                        <td><select id="asesoresR" name="asesoresR" class="grande" style="display:none;"></select></td>
                    </tr>
                    <tr>
                        <td colspan="2" class='fila'>
                            <fieldset >
                                    <legend class="labels">Filtro Generacion Fichas</legend>
                                    <table id="tabla_filtros">
                                        <tr>
                                            <td>Negocio juridica :</td>
                                            <td><input id="juridica" name="juridica" type="checkbox" ></td>
                                       
                                            <td>Ciclo :</td>
                                            <td>
                                                <select id="numciclo" name="numciclo"> 
                                                    <option value="" selected="true"></option>
                                                    <option value="1">1</option>
                                                    <option value="2">2</option>
                                                    <option value="3">3</option>
                                                    <option value="4">4</option>
                                                </select>
                                            </td>
<!--                                             <td>Tramo Anterior</td>
                                            <td><select id="tramoAnterior" name="tramoAnterior" class="grande"></select></td>-->
                                        </tr>
                                    </table>
                                
                            </fieldset>
                        </td>
                    </tr>
            </table>

            <br/>
            <div align="center">
                <img id="buscar" name="buscar" style="cursor:hand" src="<%= BASEURL%>/images/botones/buscar.gif" align="absmiddle" onmouseover="botonOver(this);" onmouseout="botonOut(this);">
                <button id="fichas">Generar fichas</button>
                <img id="aceptar" name="aceptar" style="cursor:hand" src="<%= BASEURL%>/images/botones/aceptar.gif" align="absmiddle" onmouseover="botonOver(this);" onmouseout="botonOut(this);">
                <button onclick="resumen('agente_campo')">Resumen de asignaciones</button>
                <img style="cursor:hand" src="<%= BASEURL%>/images/botones/salir.gif" align="absmiddle" onmouseover="botonOver(this);" onmouseout="botonOut(this);" onClick="parent.close();">
            </div>
            </br>
            
            <div align="center">
                <table id="negocios"></table>
                <div id="page"></div>
            </div>
            
            <div id="dialogo" class="ventana" title="Resumen"></div>
            <div id="dialogo2" class="ventana">
                <p id="msj2">texto </p> <br/>
                <center>
                    <img src="./images/cargandoCM.gif"/>
                </center>
            </div>
        </div>
            
            <div id="ventamaEmergente" title="Previsualizacion fichas" style="display:none;">
            <div>
            <table id="tabla_fichas"></table>
            <div id="pager_fichas"></div>
            </div>
        </div>   
        <script type="text/javascript" src="<%=BASEURL%>/js/asignacionCartera.js"></script>
        <script type="text/javascript">initAgen();</script>
        
    </body>
</html>