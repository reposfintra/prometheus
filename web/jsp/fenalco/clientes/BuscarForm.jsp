<%--  
    Document   : BuscarForm
    Created on : 2/09/2011, 10:14:43 AM
    Author     : ing. Iris Vargas
--%>

<%@page import="com.tsp.operation.model.services.GestionCarteraService"%>
<%@page import="com.tsp.operation.model.beans.BeanGeneral"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@page session    ="true"%>
<%@page errorPage  ="/error/ErrorPage.jsp"%>
<%@include file    ="/WEB-INF/InitModel.jsp"%>
<%@page import    ="java.util.*" %>
<%@page import    ="com.tsp.util.*" %>
<%@page import    ="com.tsp.operation.model.beans.Usuario" %>

<script src="<%=BASEURL%>/js/prototype.js"      type="text/javascript"></script>
<script src="<%=BASEURL%>/js/boton.js"          type="text/javascript"></script>
<script src="<%=BASEURL%>/js/effects.js"        type="text/javascript"></script>
<script src="<%=BASEURL%>/js/window.js"         type="text/javascript"></script>
<script src="<%=BASEURL%>/js/window_effects.js" type="text/javascript"></script>

<link href="<%= BASEURL%>/css/estilostsp.css" rel="stylesheet" type="text/css">
<link href="<%=BASEURL%>/css/mac_os_x.css"      rel="stylesheet" type="text/css">
<link href="<%=BASEURL%>/css/default.css"       rel="stylesheet" type="text/css">
<link href="<%=BASEURL%>/css/alert.css" rel="stylesheet" type="text/css"/>
<link href="<%=BASEURL%>/css/alphacube.css" rel="stylesheet" type="text/css"/>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">

<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
        <title>Formularios Cliente</title>
        <script type="text/javascript">


            function busqueda(id_field){
                var dato = $(id_field).value;
                if(dato==''){
                    alert('Para consultar debe escribir algo');
                }
                else{
                    var url = "<%=CONTROLLER%>?estado=GestionSolicitud&accion=Aval";

                    var p = "opcion=src_sol&dato="+dato+"&vista=6";

                    new Ajax.Request(url, { parameters: p, method: 'post', onComplete: completo } );
                }
            }

            function completo(resp){
                var txt = resp.responseText;
                if(txt.charAt(0) == '/'){
                  location.href = '<%= BASEURL%>'+txt;
                }
                else{
                    $('busqueda').innerHTML = txt;
                }
            }


        </script>
    </head>
    <body>
        <form id="formulario" name="formulario" action="" method="post">
            <div id="capaSuperior" style="position:absolute; width:100%; height:100px; z-index:0; left: 0px; top: 0px;">
                <jsp:include page="/WEB-INF/toptsp.jsp?encabezado=Formularios"/>
            </div>
            <div id="capaCentral" style="position:absolute; width:100%; height:85%; z-index:0; left: 0px; top: 100px; overflow: scroll;">
                <center>
                    <div id="contenido" align="center" style=" width: 400px;height: 300px; visibility: hidden; background-color:#F7F5F4; display: none"></div>
                    <table width="1000" border="2">
                        <tr>
                            <td>                                
                                <div align="center">
                                    <table border="1" style="border-collapse: collapse;" width="100%">
                                        <thead>
                                            <tr class="subtitulo1">
                                                <th>Fecha Formulario</th>
                                                <th>Numero Formulario</th>
                                                <th>Negocio</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <%
                                                        String negocio = (request.getParameter("negocio") != null) ? request.getParameter("negocio") : "";
                                                        GestionCarteraService gserv = null;
                                                        gserv = new GestionCarteraService();
                                                        ArrayList<BeanGeneral> listaform = null;
                                                        BeanGeneral form = null;
                                                        try {

                                                            listaform = gserv.buscarForm(negocio);
                                                            if (listaform != null && listaform.size() > 0) {
                                                                for (int i = 0; i < listaform.size(); i++) {
                                                                    form = listaform.get(i);
                                            %>
                                            <tr class="filaazul" onclick="busqueda('num_solicitud<%=i%>');" >
                                                <td align="center"><% out.print(form.getValor_01());%>
                                                <input type="hidden" id="num_solicitud<%=i%>" name="num_solicitud<%=i%>" value="<% out.print(form.getValor_02());%>">
                                                </td>
                                                <td align="center"><% out.print(form.getValor_02());%></td>
                                                <td align="center"><% out.print(form.getValor_03());%></td>
                                            </tr>
                                            <%
                                                                                                        }
                                                                                                    } else {
                                            %>
                                            <tr class="filaazul">
                                                <td colspan="9" align="center">No se encontraron registros</td>
                                            </tr>
                                            <%                                                                                                }

                                                                                            } catch (Exception e) {
                                            %>
                                            <tr class="filaazul">
                                                <td colspan="9" align="center">No se encontraron registros</td>
                                            </tr>
                                            <%
                                                            System.out.println("Error al obtener los formularios del cliente en BuscarForm.jsp: " + e.toString());
                                                            e.printStackTrace();
                                                        }
                                            %>
                                            <tr class="fila">
                                                <td colspan="2" align="center">
                                                    <div id="busqueda"></div>
                                                </td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>
                            </td>
                        </tr>
                    </table>
                    <br>
                    <br>
                    <img alt="" src = "<%=BASEURL%>/images/botones/salir.gif" style = "cursor:pointer" name = "imgsalir" onClick = "window.close();" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);">
                    <br>
                    <br>           
                </center>
            </div>
        </form>
    </body>
</html>
