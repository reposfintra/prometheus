<%-- 
    Document   : ver_obs
    Created on : 31/07/2010, 09:53:37 AM
    Author     : rhonalf
--%>
<%@page session="true"%>
<%@page import="java.util.*"%>
<%@page import="com.tsp.operation.model.*"%>
<%@page import="com.tsp.operation.model.beans.*"%>
<%@page import="com.tsp.operation.model.services.*"%>
<%@page import="com.tsp.util.*"%>
<%
    response.addHeader("Cache-Control","no-cache");
    response.addHeader("Pragma","No-cache");
    response.addDateHeader ("Expires", 0);
    String BASEURL = request.getContextPath();
    String CONTROLLER = BASEURL + "/controller";
%>
<%@ taglib uri="http://jakarta.apache.org/taglibs/input-0.90" prefix="input" %>
<%@page contentType="text/html" pageEncoding="ISO-8859-1"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
   "http://www.w3.org/TR/html4/loose.dtd">
   
   
 <script type='text/javascript' src="<%= BASEURL %>/js/boton.js"></script>
 <script type='text/javascript' src="<%= BASEURL %>/js/prototype.js"></script>
 <script type='text/javascript' src="<%= BASEURL %>/js/tablesorter.js"></script>
 <script src='<%=BASEURL%>/js/date-picker.js'></script>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
        <title>Ver Dato</title>
        <%
            String codigo = request.getParameter("codigo")!=null ? request.getParameter("codigo") : "1";
            String tipo = request.getParameter("tipo")!=null ? request.getParameter("tipo") : "1";
			String estado = request.getParameter("estado")!=null ? request.getParameter("estado") : "";
	    int opcion = Integer.parseInt(request.getParameter("opcion")!=null ? request.getParameter("opcion") : "1");
            ArrayList<String> temp=null;
            String[] dato = null;
            GestionCarteraService gserv = null;
            
        %>
        
		<script type="text/javascript" language="javascript">
        
			function cerrTop()
			{
				top.Windows.close(top.Windows.focusedWindow.getId());
			}
			
			
			   function cerrar()		   
			   {
				   parent.location.reload()	 
			   }

						
            function Actualizar_dato(codigo,tipo)
			{            
                var url = "<%= CONTROLLER%>?estado=Gestion&accion=Cartera";//document.getElementById("formulario").action;//mod new            				
				var dato=  document.getElementById('dato').value;
                var p =  'opcion=actualiza_dato&tipo='+tipo+'&codigo='+codigo+'&dato='+dato;
                new Ajax.Request(
                    url,
                    {
                        method: 'post',parameters: p, onLoading: parent.cargando,onComplete: cargarDatos

                    }

                );

            }
			
			
			function insertar_dato()
			{            
                var url = "<%= CONTROLLER%>?estado=Gestion&accion=Cartera";//document.getElementById("formulario").action;//mod new            				
				var dato=  document.getElementById('dato').value;
				var tipo=document.getElementById('tipo').value
                var p =  'opcion=inserta_dato&tipo='+tipo+'&dato='+dato+'&tipo='+tipo;
                new Ajax.Request(
                    url,
                    {
                        method: 'post',parameters: p, onLoading: parent.cargando,onComplete: cargarDatos

                    }

                );

            }
			
			
			
			
			
			/*function cargando(){

                document.getElementById("salida").innerHTML ='<span class="letra">Procesando... </span><img alt="cargando" src="<%= BASEURL%>/images/cargando.gif" name="imgload">';

            }*/



            function cargarDatos(response)
			{
                document.getElementById("salida").innerHTML = "<img id='img_icono' align='absbottom' src = <%=BASEURL%>/images/info.jpg >  "+response.responseText;
            }
       </script>
        
        <style type="text/css">
            .asc{
                color: #000000;
                font-family: Tahoma, Arial;
                background-color:#FFFFFF;
                font-size: 12px;
            }
            .curved{
                padding: 5px;
                border: 1px black solid;
                -moz-border-radius: 7px;
                -webkit-border-radius: 7px;
            }
        .curved1 {                padding: 5px;
                border: 1px black solid;
                -moz-border-radius: 7px;
                -webkit-border-radius: 7px;
}

		#salida{ color: #F00;font-size:12px; font-family:Georgia, "Times New Roman", Times, serif;}
		#img_icono{ width:20px; height:20px; }
        .curved11 {padding: 5px;
                border: 1px black solid;
                -moz-border-radius: 7px;
                -webkit-border-radius: 7px;
}
        </style>
    </head>
    <body class="asc" onunload=" window.location.reload();">
    
    
    
<%switch ( opcion )
{

 case 2 /* visualizar para insertar  */:	

 %>



    <table width="350" border="0">
      <tr>
        <td width="58" height="24">Tabla</td>
        <td width="276"><select name="tipo" id="tipo" style="color: black; width: 22.5em;" >
          <option value="ESTCLIENT" <%if(tipo.equals("ESTCLIENT")){%>  selected <%}%>>Estado Del Cliente</option>
          <option value="PRXACCION" <%if(tipo.equals("PRXACCION")){%>  selected <%}%>   >Proxima Accion</option>
          <option value="TIPOGEST"  <%if(tipo.equals("TIPOGEST")){%>  selected <%}%> >Gestion</option>
        </select></td>
      </tr>
      <tr>
        <td>Dato</td>
        <td><textarea name="dato" cols="43" rows="6" class="curved11" id="dato" ></textarea></td>
      </tr>
      <tr>
        <td colspan="2" align="center"><img src = "<%=BASEURL%>/images/botones/aceptar.gif" alt="" name= "imgaceptar"  id="imgaceptar2"style="cursor:pointer" 
					onclick="insertar_dato();"  onMouseOver="botonOver(this);"
                    onMouseOut="botonOut(this);"> <img src = "<%=BASEURL%>/images/botones/salir.gif" alt="" name = "imgsalir"  style = "cursor:pointer" onClick = "cerrar()"
                     onMouseOver="botonOver(this);" onMouseOut="botonOut(this);"></td>
      </tr>
      <tr>
        <td colspan="2" align="center"><span id="salida"></span></td>
      </tr>
    </table>
    <%break;
      		
}%>

    
    
    
    
    </body>
</html>