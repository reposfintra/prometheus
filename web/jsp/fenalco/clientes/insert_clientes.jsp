<%@page contentType="text/html"%>
<%@page import="com.tsp.operation.model.beans.*, java.util.*, com.tsp.util.*"%>
<%@taglib uri="/WEB-INF/tlds/tagsAgencia.tld" prefix="ag"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://jakarta.apache.org/taglibs/input-0.90" prefix="input" %>
<%@ include file="/WEB-INF/InitModel.jsp"%>
<%@page session="true"%>
<%@page errorPage="../../../error/ErrorPage.jsp"%>
<%@page import="com.tsp.util.*"%>
<%@page import="java.util.*"%>
<%@page import="com.tsp.operation.model.*"%>
<script src="<%=BASEURL%>/js/prototype.js"      type="text/javascript"></script>

<%
           String msg = (request.getParameter("msg") != null) ? request.getParameter("msg") : "";
           String path = application.getRealPath(model.menuService.getRealUrl(request.getRequestURI()));
           String datos[] = model.menuService.getContenidoMenu(BASEURL, request.getRequestURI(), path);
           String nomc = (request.getParameter("NomCliente") != null) ? request.getParameter("NomCliente") : "";
           String ced = (request.getParameter("CedCliente") != null) ? request.getParameter("CedCliente") : "";
           String DirC = (request.getParameter("Direccion") != null) ? request.getParameter("Direccion") : "";
           String Telef = (request.getParameter("Telefono") != null) ? request.getParameter("Telefono") : "";
           model.ciudadService.listarCiudadGeneral();
           Vector a = model.ciudadService.obtCiudades();
           Ciudad ciu = new Ciudad();
%>
<html>
    <head>
        <title>Ingresar Cliente</title>
        <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
        <link href="../../../css/estilostsp.css" rel="stylesheet" type="text/css">
        <link href="<%=BASEURL%>/css/estilostsp.css" rel="stylesheet" type="text/css">
    </head>
    <script type='text/javascript' src="<%=BASEURL%>/js/boton.js"></script>
    <script type='text/javascript' src="<%= BASEURL%>/js/validar.js"></script>
    <script type='text/javascript' src="<%= BASEURL%>/js/reporte.js"></script>
    <link   href="<%= BASEURL%>/css/estilostsp.css" rel='stylesheet'>
    <link   href="<%= BASEURL%>/css/EstilosReportes.css" rel='stylesheet'>
    <body onResize="redimensionar()" onLoad="redimensionar()">
        <div id="capaSuperior" style="position:absolute; width:100%; height:100px; z-index:0; left: 0px; top: 0px;">
            <p>
                <jsp:include page="/WEB-INF/toptsp.jsp?encabezado=Ingresar Datos del Cliente"/>
            </p>
        </div>
        <div id="capaCentral" style="position:center; width:100%; height:85%; z-index:0; left: -75px; top: 99px; ">
            <form name="frCliente" id="frCliente" action="<%=CONTROLLER%>?estado=Clientefen&accion=Insertar" method="post" >
                <p>&nbsp;</p>
                <p>&nbsp;</p>
                <p>&nbsp;</p>
                <table width="860" border="2" align="center">
                    <tr>
                        <td width="1084" height="150">
                            <table width="99%"   align="center">
                                <tr>
                                    <td width="50%" height="22"  class="subtitulo1"><p align="left">Ingresar datos del Cliente</p></td>
                                    <td width="50%"  class="barratitulo"><img src="<%=BASEURL%>/images/titulo.gif" width="32" height="20" align="left"><%=datos[0]%></td>
                                </tr>
                            </table>
                            <table width="99%"  align="center" >
                                <tr class="fila">
                                    <td height="20" colspan="2" ></td>
                                    <td height="20">
                                        <div align="left">Persona
                                            <input name="chk1" id="chk1" type="checkbox" onClick="dest(chk2),dest(chk3),dest(chk4),oculta(),cambio2(frCliente),check(chk1)" value="p" checked>
                                        </div>
                                    </td>
                                    <td width="200" height="20">
                                        <div align="left">Empresa
                                            <input name="chk2" id="chk2" type="checkbox" value="e" onClick="campo(),dest(chk1),dest(chk3),dest(chk4),cambio(frCliente),check(chk2)">
                                        </div>
                                    </td>
                                    <td width="200" height="20">
                                        <div align="left">Codeudor
                                            <input name="chk3" id="chk3" type="checkbox" value="c" onClick="campo2(),dest(chk1),dest(chk2),dest(chk4),cambio2(frCliente),check(chk3)">
                                        </div>
                                    </td>
                                    <td height="20" colspan="4"></td>
                                </tr>


                                <tr class="fila">
                                    <td height="20" colspan="2">Nombre o Razon Social </td>
                                    <td height="20" colspan="6"><input name="NomCliente" value="<%=nomc%>" type="text" class="textbox" id="NomCliente" size="90" maxlength="100">
                                        <img  src="<%=BASEURL%>/images/botones/iconos/obligatorio.gif" width="10" height="10"></td>
                                </tr>
                                <tr class="fila">
                                    <td width="95" height="20">Tipo Id </td>
                                    <td width="120"><select name="tipoid" id="tipoid" >
                                            <option value="CED">CC</option>
                                            <option value="NIT">NIT</option>
                                        </select>
                                        <img  src="<%=BASEURL%>/images/botones/iconos/obligatorio.gif" width="10" height="10"></td>
                                    <td width="157" height="20">No Documento </td>
                                    <td height="20" colspan="2"><input name="CedCliente" type="text" class="textbox"  value="<%=ced%>"id="CedCliente" size="15"  maxlength="15" onKeyPress="soloDigitos(event,'decNO')" onChange="ajaxSrchCli();"><img  src="<%=BASEURL%>/images/botones/iconos/obligatorio.gif" width="10" height="10"></td>
                                    <td width="30" height="20">Expedicion</td>
                                    <td width="250" height="20" colspan="1"><select name="expcc" id="select">
                                            <%for (int i = 0; i < a.size(); i++) {
                                             ciu = (Ciudad) a.elementAt(i);
                                             if (ciu.getcodciu().equals("BQ")) {%>
                                            <option value="<%=ciu.getcodciu()%>" selected><%=ciu.getnomciu()%></option>
                                            <%} else {%>
                                            <option value="<%=ciu.getcodciu()%>" ><%=ciu.getnomciu()%></option>
                                          <%}
                                             ciu = null;
                                        }%>
                                        </select>
                                        <img  src="<%=BASEURL%>/images/botones/iconos/obligatorio.gif" width="10" height="10"></td>

                                </tr>

                                <tr class="fila">
                                    <td width="95" height="20">Celular</td>
                                    <td width="120"><input name="Celular" type="text" class="textbox" id="CellCliente" size="15"  maxlength="15" > </td>
                                    <td height="20" colspan="6"></td>
                                </tr>

                            </table>
                            <table width="99%"  align="center" >
                                <tr class="fila">

                                    <td width="59" height="20">Direccion </td>
                                    <td width="200" colspan="2"><input name="Direccion" type="text" class="textbox" id="Direccion" size="30" maxlength="30"  value="<%=DirC%>" >
                                        <img  src="<%=BASEURL%>/images/botones/iconos/obligatorio.gif" width="10" height="10"> &nbsp; </td>
                                    <td width="89" height="20">Ciudad</td>
                                    <td colspan="3">
                                        <select name="c_dir" id="c_dir">
                                            <%for (int i = 0; i < a.size(); i++) {
                                      ciu = (Ciudad) a.elementAt(i);
                                      if (ciu.getcodciu().equals("BQ")) {%>
                                            <option value="<%=ciu.getcodciu()%>" selected><%=ciu.getnomciu()%></option>
                                            <%} else {%>
                                            <option value="<%=ciu.getcodciu()%>" ><%=ciu.getnomciu()%></option>
                                            <%}
               ciu = null;
           }%>
                                        </select> <img  src="<%=BASEURL%>/images/botones/iconos/obligatorio.gif" width="10" height="10"></td>
                                </tr>
                            </table>
                            <table width="99%" align="center">
                                <tr class="fila">
                                    <td width="95" height="7">Telefono</td>
                                    <td width="249" ><input name="Telefono" type="text" class="textbox" value="<%=Telef%>" id="Telefono2" size="20" maxlength="40">
                                        <img  src="<%=BASEURL%>/images/botones/iconos/obligatorio.gif" width="10" height="10"></td>
                                    <td width="200" id="celda4"  height="20">
                                        <div align="left">Estudiante S/N
                                            <input name="chk4" type="checkbox" value="f" onClick="campo3()">
                                        </div>
                                    </td>
                                    <td  colspan="6">&nbsp;</td>
                                </tr>
                            </table>
                            <table width="99%" align="center" border="0">
                                <tr class="fila" id="celda" style="display:none ">
                                    <td width="95" height="25">Representante Legal</td>
                                    <td ><input name="replegal" type="text" class="textbox" id="replegal" size="45" maxlength="100" >
                                        <img  src="<%=BASEURL%>/images/botones/iconos/obligatorio.gif" width="10" height="10"> </td>
                                    <td width="95" height="20" ><div align="right">C&eacute;dula</div></td>
                                    <td width="298"  colspan="4"><input name="ccreplegal" type="text" class="textbox" id="ccreplegal" onKeyPress="soloDigitos(event,'decNO')" >
                                        <img  src="<%=BASEURL%>/images/botones/iconos/obligatorio.gif" width="10" height="10"> </td>
                                </tr>
                                <tr class="fila" id="celda2" style="display:none ">
                                    <td width="80" height="25">Nombre Universidad</td>
                                    <td nowrap><select name="universidad" id="universidad">
                                            <option value="FUNDACION UNIVERSIDAD DEL NORTE">FUNDACION UNIVERSIDAD DEL NORTE</option>
                                            <option value="UNIVERSIDAD SIMON BOLIVAR">UNIVERSIDAD SIMON BOLIVAR</option>
                                            <option value="UNIVERSIDAD AUTONOMA DEL CARIBE">UNIVERSIDAD AUTONOMA DEL CARIBE</option>
                                            <option value="POLITECNICO DE LA COSTA ATLANTICA">POLITECNICO DE LA COSTA ATLANTICA</option>
                                            <option value="UNIVERSIDAD LIBRE">UNIVERSIDAD LIBRE</option>
                                            <option value="CORPORACION UNIVERSITARIA DE LA COSTA">CORPORACION UNIVERSITARIA DE LA COSTA</option>
                                            <option value="FUNDAION UNIVERSITARIA TECNOLOGICO COMFENALCO">FUNDAION UNIVERSITARIA TECNOLOGICO COMFENALCO</option>
                                            <option value="UNIVERSIDAD DE SAN BUENAVENURA">UNIVERSIDAD DE SAN BUENAVENURA</option>
                                            <option value="CORPORACION UNIVERSITARIA RAFAEL NU�EZ">CORPORACION UNIVERSITARIA RAFAEL NU�EZ</option>

                                            <option value="CORPORACION UNIVERSITARIA LATINOAMERICANA">CORPORACION UNIVERSITARIA LATINOAMERICANA</option>
                                            <option value="PONTIFICIA UNIVERSIDAD JAVERIANA">PONTIFICIA UNIVERSIDAD JAVERIANA</option>
                                            <option value="UNIVERSIDAD METROPOLITANA">UNIVERSIDAD METROPOLITANA</option>
					    <option value="INSTITUTO TECNOLOGICO DE SOLEDAD ITSA">INSTITUTO TECNOLOGICO DE SOLEDAD ITSA</option>
                                            <option value="FUNDACION TECNOLOGICA ANTONIO DE AREVALO TECNAR">FUNDACION TECNOLOGICA ANTONIO DE AREVALO TECNAR</option>
                                        </select>
                                        <img  src="<%=BASEURL%>/images/botones/iconos/obligatorio.gif" width="10" height="10">
                                    </td>
                                    <td width="100" height="20" ><div align="right">Nombre Programa</div></td>
                                    <td nowrap><input name="carrera" type="text" class="textbox" id="carrera" size="45" maxlength="100"><img  src="<%=BASEURL%>/images/botones/iconos/obligatorio.gif" width="10" height="10"> </td>
                                </tr>
                                <tr class="fila" id="celda3" style="display:none ">
                                    <td width="95" height="25">Semestre a Cursar</td>
                                    <td ><input name="semestre" type="text" class="textbox" id="semestre" size="25" maxlength="2" onKeyPress="soloDigitos(event,'decNO')">
                                        <img  src="<%=BASEURL%>/images/botones/iconos/obligatorio.gif" width="10" height="10"> </td>
                                    <td width="149" height="20" ><div align="right">Codigo Estudiante</div></td>
                                    <td ><input name="codigo" type="text" class="textbox" id="codigo" size="25" maxlength="100">
                                        <img  src="<%=BASEURL%>/images/botones/iconos/obligatorio.gif" width="10" height="10"> </td>                                 
                                       
                                </tr>
                                                                <tr class="fila" id="celda5" style="display:none ">
                                    <td width="95" height="25">Numero Orden</td>
                                    <td ><input name="id_orden" type="text" class="textbox" id="id_orden" size="25" maxlength="11" onKeyPress="soloDigitos(event,'decNO')">
                                        <img  src="<%=BASEURL%>/images/botones/iconos/obligatorio.gif" width="10" height="10"> </td>
                                    <td width="149" height="20" >&nbsp;</td>
                                    <td >&nbsp;</td>                                 
                                       
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
                <p align="center">
                    <a href="<%=BASEURL%>/jsp/fenalco/clientes/consultaClientesRespuesta.jsp">Mis Clientes</a>
                    <a href="<%=BASEURL%>/jsp/fenalco/clientes/codeudor_buscar.jsp">Codeudores</a>
                </p>
                <table width="699" border="0" align="center">
                    <tr>
                        <td colspan="2" nowrap align="center">
                            <img  name="Guardar" src="<%=BASEURL%>/images/botones/aceptar.gif"  style = "cursor:hand" align="middle" type="image" id="Guardar"  onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" onClick="return validar(frCliente)">
                            <img src="<%=BASEURL%>/images/botones/restablecer.gif" width="87" name="buss" height="21" align="absmiddle" style="cursor:hand"   onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" onClick="document.location='<%=BASEURL%>/jsp/fenalco/clientes/insert_clientes.jsp'">
                            <img src="<%=BASEURL%>/images/botones/salir.gif"  name="imgsalir" align="absmiddle" style="cursor:hand" onClick="parent.close();" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);"> </td>
                    </tr>
                </table>
            </form>
            <%if (!msg.equals("")) {%>
            <table width="407" border="2" align="center">
                <tr>
                    <td><table width="100%"   align="center"  >
                            <tr>
                                <td width="307" align="center" class="mensajes"><%=msg%></td>
                                <td width="10" background="<%=BASEURL%>/images/cuadronaranja.JPG"></td>
                            </tr>
                        </table></td>
                </tr>
            </table>

            <%}%>
        </div>
    </body>
</html>
<script>
    function validar( forma ){
        if(forma.chk1.checked==false && forma.chk2.checked==false && forma.chk3.checked==false)
        {
            alert( 'Debe Ingresar Escojer si es Persona o Empresa continuar...' );
            return false;
        }
        if ( forma.NomCliente.value == '' ){
            alert( 'Debe Ingresar el nombre del cliente para continuar...' );
            forma.NomCliente.focus();
            return false;
        }
        if ( forma.CedCliente.value == '' ){
            alert( 'Debe Ingresar el documento del cliente para continuar...' );
            forma.CedCliente.focus();
            return false;
        }
        if ( forma.Direccion.value == '' ){
            alert( 'Debe Ingresar la direccion del cliente para continuar...' );
            forma.Direccion.focus();
            return false;
        }
        if ( forma.Telefono.value == '' ){
            alert( 'Debe Ingresar el telefono del cliente para continuar...' );
            forma.Telefono.focus();
            return false;
        }
        if(forma.chk2.checked==true)
        {
            if ( forma.replegal.value == '' ){
                alert( 'Debe Ingresar el Representante Legal para continuar...' );
                forma.replegal.focus();
                return false;
            }

            if ( forma.ccreplegal.value == '' ){
                alert( 'Debe Ingresar la cedula del Representante Legal para continuar...' );
                forma.ccreplegal.focus();
                return false;
            }
        }
        if(forma.chk4.checked==true)
        {
            if ( forma.universidad.value == '' ){
                alert( 'Debe Ingresar la Universidad para continuar...' );
                forma.replegal.focus();
                return false;
            }

            if ( forma.semestre.value == '' ){
                alert( 'Debe Ingresar el semestre para continuar...' );
                forma.ccreplegal.focus();
                return false;
            }
			
            if ( forma.carrera.value == '' ){
                alert( 'Debe Ingresar la carrera para continuar...' );
                forma.replegal.focus();
                return false;
            }

            if ( forma.codigo.value == '' ){
                alert( 'Debe Ingresar el codigo del estudiante para continuar...' );
                forma.ccreplegal.focus();
                return false;
            }
			
			if ( forma.id_orden.value == '' ){
            alert( 'Debe Ingresar el numero de orden financiera para continuar...' );
           forma.ccreplegal.focus();
           return false;
            }
        }

        forma.submit();
    }

    function campo()
    {
        celda.style.display="block";
        celda2.style.display="none";
        celda3.style.display="none";
        celda4.style.display="none";
		celda5.style.display="none";
    }
    function campo2()
    {   celda.style.display="none";
        celda2.style.display="none";
        celda3.style.display="none";
		celda5.style.display="none";
        celda4.style.display="block";
		//celda5.style.display="block";
    }
    function campo3()
    {   if(celda2.style.display!="block")
        {   celda2.style.display="block";
            celda3.style.display="block";
			celda5.style.display="block";
        }
        else
        {   celda2.style.display="none";
            celda3.style.display="none";
			 celda5.style.display="none";
        }
    }
    function dest(a)
    {
        a.checked=false;
    }
    function check(a)
    {
        a.checked=true;
    }
    function oculta()
    {
        celda.style.display="none";
        celda2.style.display="none";
        celda3.style.display="none";
        celda5.style.display="none";
    }
    function cambio(forma)
    {
        forma.tipoid.value="NIT";
    }
    function cambio2(forma)
    {
        forma.tipoid.value="CED";
    }
    oculta();check(frCliente.chk1);dest(frCliente.chk2);dest(frCliente.chk3);dest(frCliente.chk4);


function ajaxSrchCli(){//forma.chk3.checked
        if($('chk3').checked==false){
            var nitafil='<%=((Usuario)((request.getSession()).getAttribute("Usuario"))).getCedula()%>';
            var p = 'opcion=valida&nit='+$('CedCliente').value+'&nitafil='+nitafil;

            new Ajax.Request(
            '<%=CONTROLLER%>?estado=Clientefen&accion=Insertar',
            {
                method: 'get',
                parameters: p,
               // onLoading: whenLoading,
                onComplete: EndSrchCli
            });
        }
    }

function ajaxAsocCli(){//forma.chk3.checked
            frCliente.submit();
            document.location='<%=BASEURL%>/jsp/fenalco/clientes/edit_clientes.jsp?cod='+$('CedCliente').value;

   }

    function EndSrchCli(response){
        txt=response.responseText;
        //alert(txt.length);
        if(txt.length>2){
            var r=confirm(txt);
            if(r==true){
                ajaxAsocCli();
                alert('El cliente ha sido asociado exitosamente, por favor verifique los datos');
            }
        }
    }


</script>
