<!--
    Document   : facturas_cartera
    Created on : 19/07/2010, 04:50:41 PM
    Author     : rhonalf
-->
<%@page session="true"%>
<%@page import="java.util.*"%>
<%@page import="com.tsp.operation.model.*"%>
<%@page import="com.tsp.operation.model.beans.*"%>
<%@page import="com.tsp.operation.model.services.*"%>
<%@page import="java.text.DecimalFormat"%>
<%@page import="com.tsp.util.*"%>
<%
    response.addHeader("Cache-Control","no-cache");
    response.addHeader("Pragma","No-cache");
    response.addDateHeader ("Expires", 0);
    String BASEURL = request.getContextPath();
    String CONTROLLER = BASEURL + "/controller";
%>
<%@ taglib uri="http://jakarta.apache.org/taglibs/input-0.90" prefix="input" %>
<%@page contentType="text/html" pageEncoding="ISO-8859-1"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
   "http://www.w3.org/TR/html4/loose.dtd">
<!-- This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>. -->
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
        <script type='text/javascript' src="<%= BASEURL %>/js/boton.js"></script>
        <script type='text/javascript' src="<%= BASEURL %>/js/prototype.js"></script>
        <link href="<%= BASEURL %>/css/estilostsp.css" rel="stylesheet" type="text/css">
        <!-- Las siguientes librerias CSS y JS son para el manejo de DIVS dinamicos.-->
        <link href="<%=BASEURL%>/css/default.css" rel="stylesheet" type="text/css">
        <link href="<%=BASEURL%>/css/mac_os_x.css" rel="stylesheet" type="text/css">
        <script src="<%=BASEURL%>/js/effects.js" type="text/javascript"></script>
        <script src="<%=BASEURL%>/js/window.js" type="text/javascript"></script>
        <title>Cobranzas</title><!-- mod -->
        <style type="text/css">
            .curved{
                color: #FFFFFF;
                border-top: 1px black solid;
                border-left: 1px black solid;
                border-right: 1px black solid;
                text-align: center;
                background-color: #03A70C;
                font-family: Verdana, Arial, Helvetica, sans-serif;
                font-size: small;
                font-weight: bold;
                padding: 0.1em;
                /*font-size:12px;*/
                border-top-left-radius: 7px;
                border-top-right-radius: 7px;
                -moz-border-radius-topleft: 7px;
                -moz-border-radius-topright: 7px;
                -webkit-border-top-left-radius: 7px;
                -webkit-border-top-right-radius: 7px;
                cursor:pointer;
            }
            .filablanca{
             color: #000000;
             font-family: Tahoma, Arial;
             font-size: 12px;
             background-color:#FFFFFF;
}
            .curved1{
                color: #FFFFFF;
                border-top: 1px black solid;
                border-left: 1px black solid;
                border-right: 1px black solid;
                text-align: center;
                background-color: #00D20C;
                font-family: Verdana, Arial, Helvetica, sans-serif;
                font-size: small;
                font-weight: bold;
                padding: 0.1em;
                /*font-size:12px;*/
                border-top-left-radius: 7px;
                border-top-right-radius: 7px;
                -moz-border-radius-topleft: 7px;
                -moz-border-radius-topright: 7px;
                -webkit-border-top-left-radius: 7px;
                -webkit-border-top-right-radius: 7px;
                cursor:pointer;
            }
            .tab{
                background-color: #FFFFFF;
                border-top: 0px;
                border-left: 1px black solid;
                border-right: 1px black solid;
                border-bottom: 1px black solid;
                overflow: auto;
                height: 350px;
            }
            .xfila{
                color: #000000;
                font-family: Tahoma, Arial;
                background-color:#FFFFFF;
                font-size: 12px;
            }
            td{
                white-space: nowrap;
            }
            .head{
                background:url(<%=BASEURL%>/images/sort.gif) 7px no-repeat;
                font-family: Verdana, Arial, Helvetica, sans-serif;
                font-size: small;
                color: #FFFFFF;
                background-color:#03a70c;
                font-weight: bold;
                font-size:12px;
                cursor: pointer;
            }
            .asc{
                background:url(<%=BASEURL%>/images/asc.gif) 7px no-repeat;
                font-family: Verdana, Arial, Helvetica, sans-serif;
                font-size: small;
                color: #FFFFFF;
                background-color:#03a70c;
                font-weight: bold;
                font-size:12px;
                cursor: pointer;
            }
            .desc{
                background:url(<%=BASEURL%>/images/desc.gif) 7px no-repeat;
                font-family: Verdana, Arial, Helvetica, sans-serif;
                font-size: small;
                color: #FFFFFF;
                background-color:#03a70c;
                font-weight: bold;
                font-size:12px;
                cursor: pointer;
            }
            .gestionspan{
                color: white;
                cursor: pointer;
                padding: 1px;
                border: 1px black solid;
                text-align: center;
                background-color: #00D20C;
                border-bottom-left-radius: 7px;
                -moz-border-radius-bottomleft: 7px;
                -webkit-border-bottom-left-radius: 7px;
            }
        </style>
        <script type="text/javascript">
            function enviarForm(){
                alert('under construction');
            }

            function load(id_div){
                if(id_div=='facturacion'){
                    clases('fact','ing','est','estc');
                }
                if(id_div=='ingresos'){
                    clases('ing','fact','est','estc');
                }
                if(id_div=='estado'){
                    clases('est','ing','fact', 'estc');
                }
                if(id_div=='estado_clie'){
                    clases('estc','ing','fact', 'est');
                }
                $("divgen").innerHTML = $(id_div).innerHTML;
            }
             function load2(id_div){
                if(id_div=='facturacion'){
                    clases2('fact','ing');
                }
                if(id_div=='ingresos'){
                    clases2('ing','fact');
                }

                $("divgen").innerHTML = $(id_div).innerHTML;
            }

            function clases2(id1,id2){
                $(id1).className='curved';
                $(id2).className='curved1';
            }

            function clases(id1,id2,id3,id4){
                $(id1).className='curved';
                $(id2).className='curved1';
                $(id3).className='curved1';
                $(id4).className='curved1';
            }

            function facturafiltro(codeneg,indice){
                var valorfiltro = $("filtro").options[indice].value;
                var url = "<%= CONTROLLER%>?estado=Gestion&accion=Cartera";
                var p =  'opcion=facturafiltro&filtro='+valorfiltro+'&cadena='+codeneg;
                new Ajax.Request(
                    url,
                    {
                        method: 'post',
                        parameters: p,
                        onLoading: loading,
                        onComplete: llenarDiv
                    }
                );
            }

            function facturafiltro2(indice){
                var valorfiltro = $("filtro").options[indice].value;
                location.href="<%= BASEURL %>/jsp/fenalco/clientes/facturas_cartera.jsp?verpfs=true&filt="+valorfiltro;
            }

            function loading(){
                $("facturacion").innerHTML ='<span class="letra">Procesando </span><img alt="cargando" src="<%= BASEURL%>/images/cargando.gif" name="imgload">';
                $("divgen").innerHTML = $("facturacion").innerHTML;
            }

            function llenarDiv(response){
                $("facturacion").innerHTML = response.responseText;
                $("divgen").innerHTML = $("facturacion").innerHTML;
            }

            function calculate(valor,fila){
                valor = valor / 100;
                var vcuot = $("vcuot"+fila).value;
                vcuot = vcuot *(1+valor);
                vcuot = vcuot + $("mora"+fila).value*1;
                vcuot = Math.round(vcuot);
                $("vx"+fila).value = formatNumber(vcuot);
            }

            function generarDoc(tipodoc,docneg){
                alert('Iniciando la generacion del  '+tipodoc);

                var url = "<%= CONTROLLER%>?estado=Gestion&accion=Cartera";
                var p =  'opcion=print'+tipodoc+'&cadena='+docneg+'&carterita=<%= Double.parseDouble(request.getParameter("cartera")!=null ? request.getParameter("cartera"): "0") %>';
                new Ajax.Request(
                    url,
                    {
                        method: 'post',
                        parameters: p,
                        onLoading: procesando,
                        onComplete: finishDiv
                    }
                );
            }

            function procesando(){
                $("mensaje").innerHTML ='<span class="letra">Procesando </span><img alt="cargando" src="<%= BASEURL%>/images/cargando.gif" name="imgload">';
            }

            function finishDiv(response){
                $("mensaje").innerHTML = response.responseText;
            }

            var filaAct=0;
            function expandir(fila,doc){
                if($("diving"+fila).innerHTML==""){
                    $("imgexpand"+fila).src="<%= BASEURL%>/images/botones/iconos/collapse.gif";
                    $("diving"+fila).innerHTML = '<span class="letra">Procesando </span><img alt="cargando" src="<%= BASEURL%>/images/cargando.gif" name="imgload">';
                    var url = "<%= CONTROLLER%>?estado=Gestion&accion=Cartera";
                    var p =  'opcion=ingfactura&cadena='+doc;
                    filaAct = fila;
                    new Ajax.Request(
                        url,
                        {
                            method: 'post',
                            parameters: p,
                            onLoading: cargando,
                            onComplete: fillDiv
                        }
                    );
                }
                else{
                    $("imgexpand"+fila).src="<%= BASEURL%>/images/botones/iconos/expand.gif";
                    $("diving"+fila).innerHTML="";
                }
            }

            function cargando(){
                $('diving'+filaAct).innerHTML = '<span class="letra">Procesando </span><img alt="cargando" src="<%= BASEURL%>/images/cargando.gif" name="imgload">';
            }

            function fillDiv(response){
                $('diving'+filaAct).innerHTML = response.responseText;
                $('diving'+filaAct).style.display='block';
            }

            function expandir2(fila,doc){
                if($("divfacts"+fila).innerHTML==""){
                    $("imgexpandx"+fila).src="<%= BASEURL%>/images/botones/iconos/collapse.gif";
                    $("divfacts"+fila).innerHTML = '<span class="letra">Procesando </span><img alt="cargando" src="<%= BASEURL%>/images/cargando.gif" name="imgload">';
                    var url = "<%= CONTROLLER%>?estado=Gestion&accion=Cartera";
                    var p =  'opcion=facturasing&cadena='+doc;
                    filaAct = fila;
                    new Ajax.Request(
                        url,
                        {
                            method: 'post',
                            parameters: p,
                            onLoading: cargando2,
                            onComplete: fillDiv2
                        }
                    );
                }
                else{
                    $("imgexpandx"+fila).src="<%= BASEURL%>/images/botones/iconos/expand.gif";
                    $("divfacts"+fila).innerHTML="" ;
                }
            }

            function cargando2(){
                $('divfacts'+filaAct).innerHTML = '<span class="letra">Procesando </span><img alt="cargando" src="<%= BASEURL%>/images/cargando.gif" name="imgload">';
            }

            function fillDiv2(response){
                $('divfacts'+filaAct).innerHTML = response.responseText;
                $('divfacts'+filaAct).style.display='block';
            }

            function winGestionFact(factura, valor){
                var win = new Window({/*className: "mac_os_x",*/
                            id: "gestcart",
                            title: ".: Gestion de cartera :.",
                            width:1200,
                            height:550,
                            /*showEffectOptions: {duration:1},num_facts
                            hideEffectOptions: {duration:1},*/
                            destroyOnClose: true,
                            url:'<%=BASEURL%>/jsp/fenalco/clientes/gestion_cartera.jsp?documento='+factura+'&facts='+factura+';_;'+'&valor='+valor+';_;',
                            recenterAuto: false
                        });
                win.showCenter(true);
                win.maximize();
            }

            function winGestionFact2(){
                var facts = '';
                var num_facts = $('num_facts').value;
                var x = false;
                var sumavalor=0;
                for(var i=0;i<num_facts;i++){
                    var fact = $('fact'+i);
                    if(fact!=null && fact.checked){
                        facts += fact.value+';_;';
                        sumavalor+=$('vx'+i).value+';_;';
                        x = true;
                    }
                }
                if(x==true){
                    var win = new Window({
                            id: "gestcart_multi",
                            title: ".: Gestion de cartera :.",
                            width:1200,
                            height:550,
                            destroyOnClose: true,
                            url:'<%=BASEURL%>/jsp/fenalco/clientes/gestion_cartera.jsp?facts='+facts+'&valor='+sumavalor,
                            recenterAuto: false
                        });
                    win.showCenter(true);
                    win.maximize();
                }
                else{
                    alert('Seleccione por lo menos un documento');
                }
            }

            function formatNumber(num,prefix){
                prefix = prefix || '';
                num += '';
                var splitStr = num.split('.');
                var splitLeft = splitStr[0];
                var splitRight = splitStr.length > 1 ? '.' + splitStr[1] : '';
                splitRight = splitRight.substring(0,3);//090929
                var regx = /(\d+)(\d{3})/;
                while (regx.test(splitLeft)) {
                    splitLeft = splitLeft.replace(regx, '$1' + ',' + '$2');
                }
                return prefix + splitLeft + splitRight;
            }

            function unformatNumber(num) {
                return num.replace(/([^0-9\.\-])/g,'')*1;
            }

            function organizar(codneg,col,orden,num){
                var indice = $("filtro").selectedIndex;
                var valorfiltro = $("filtro").options[indice].value;
                var url = "<%= CONTROLLER%>?estado=Gestion&accion=Cartera";
                var p =  'opcion=facturafiltrosrt&filtro='+valorfiltro+'&cadena='+codneg+'&col='+col+'&orden='+orden+'&num='+num;
                $('col').value = col;
                $('orden').value = orden;
                new Ajax.Request(url,{ method: 'post',parameters: p,onLoading: loading,onComplete: llenarDiv});
            }

            function generarDoc2(tipodoc,docneg){
                alert('Iniciando la generacion del  '+tipodoc);
                var url = "<%= CONTROLLER%>?estado=Gestion&accion=Cartera";
                var col = $('col').value;
                var orden = $('orden').value;
                var filtro = $('filtro').value;
                var p =  'opcion=print2'+tipodoc+'&cadena='+docneg+'&col='+col+'&orden='+orden+'&filtro='+filtro;
                new Ajax.Request(url,{method: 'post',parameters: p,onLoading: procesando2,onComplete: finishDiv2});
            }

            function generarDoc4(tipodoc,docneg){
                alert('Iniciando la generacion del  '+tipodoc);
                var url = "<%= CONTROLLER%>?estado=Gestion&accion=Cartera";
                var p =  'opcion=print4'+tipodoc+'&cadena='+docneg;
                new Ajax.Request(url,{method: 'post',parameters: p,onLoading: procesando2,onComplete: finishDiv2});
            }

            function procesando2(){
                $("mensaje2").innerHTML ='<span class="letra">Procesando </span><img alt="cargando" src="<%= BASEURL%>/images/cargando.gif" name="imgload">';
            }

            function finishDiv2(response){
                $("mensaje2").innerHTML = "";
                alert(response.responseText);
            }

            function soloNumeros(id) {
                 var valor = $(id).value;
                 valor =  valor.replace(/[^0-9^.]+/gi,"");
                 $(id).value = valor;
            }

            function organizar2(col,orden,num){
                var indice = $("filtro").selectedIndex;
                var valorfiltro = $("filtro").options[indice].value;
                var url = "<%= CONTROLLER%>?estado=Gestion&accion=Cartera";
                var p =  'opcion=facturafiltropf&filtro='+valorfiltro+'&col='+col+'&orden='+orden+'&num='+num;
                $('col').value = col;
                $('orden').value = orden;
                new Ajax.Request(url,{method: 'post',parameters: p,onLoading: loading,onComplete: llenarDiv});
            }

            function generarDoc3(tipodoc){
                alert('Iniciando la generacion del  '+tipodoc);

                var url = "<%= CONTROLLER%>?estado=Gestion&accion=Cartera";
                var p =  'opcion=print3'+tipodoc;
                new Ajax.Request(url,{method: 'post',parameters: p,onLoading: procesando2,onComplete: finishDiv2});
            }

            function sortIngresos(codneg,col,orden,num){
                var url = "<%= CONTROLLER%>?estado=Gestion&accion=Cartera";
                var p =  'opcion=sortingresos&cadena='+codneg+'&col='+col+'&orden='+orden+'&num='+num;
                new Ajax.Request(url,{method: 'post',parameters: p,onLoading: loading2,onComplete: llenarDiv2});
            }

            function sortIngresos2(col,orden,num){
                var url = "<%= CONTROLLER%>?estado=Gestion&accion=Cartera";
                var p =  'opcion=sortingresos2&col='+col+'&orden='+orden+'&num='+num;
                new Ajax.Request(url,{method: 'post',parameters: p,onLoading: loading2,onComplete: llenarDiv2});
            }

            function loading2(){
                $("ingresos").innerHTML ='<span class="letra">Procesando </span><img alt="cargando" src="<%= BASEURL%>/images/cargando.gif" name="imgload">';
                $("divgen").innerHTML = $("ingresos").innerHTML;
            }

            function llenarDiv2(response){
                $("ingresos").innerHTML = response.responseText;
                $("divgen").innerHTML = $("ingresos").innerHTML;
            }

            function ordenarEstadoCartera(orden,col,codneg,num){
                var url = "<%= CONTROLLER%>?estado=Gestion&accion=Cartera";
                var p =  'opcion=sortestado&cadena='+codneg+'&col='+col+'&orden='+orden+'&num='+num;
                new Ajax.Request(url,{method: 'post',parameters: p,onLoading: loading3,onComplete: llenarDiv3});
            }

            function ordenarEstadoCartera2(orden,col,num){
                var url = "<%= CONTROLLER%>?estado=Gestion&accion=Cartera";
                var p =  'opcion=sortestadopf&col='+col+'&orden='+orden+'&num='+num;
                new Ajax.Request(url,{method: 'post',parameters: p,onLoading: loading3,onComplete: llenarDiv3});
            }

            function loading3(){
                $("estado").innerHTML ='<span class="letra">Procesando </span><img alt="cargando" src="<%= BASEURL%>/images/cargando.gif" name="imgload">';
                $("divgen").innerHTML = $("estado").innerHTML;
            }

            function llenarDiv3(response){
                $("estado").innerHTML = response.responseText;
                $("divgen").innerHTML = $("estado").innerHTML;
            }
        </script>
    </head>
    <body>
        <div id="capaSuperior" style="position:absolute; width:100%; height:100px; z-index:0; left: 0px; top: 0px;">
            <jsp:include page="/toptsp.jsp?encabezado=Cobranzas"/>
        </div>
        <div id="capaCentral" style="position:absolute; width:100%; height:85%; z-index:0; left: 0px; top: 100px; overflow:auto;">
        <%
            Usuario usuario = (Usuario) request.getSession().getAttribute("Usuario");
            GestionCarteraService cserv = null;
            cserv = new GestionCarteraService(usuario.getBd());
            String codigoneg = request.getParameter("codigoneg")!=null ? request.getParameter("codigoneg"): "" ;
            String sald[] = cserv.saldoNegocio(codigoneg).split(";");
            String tot_vigente = "0";
            String tot_vencido = "0";
            String gastos_cobranza = "0";
            String tot_intereses = "0";
            String saldo_total = "0";
            
            if(codigoneg.equals("")==false){
        %>

        <%
            ArrayList<String> listahead = null;
            ArrayList listaf = null;
            int num_facts = 0;
            try{
                
                listahead = cserv.cabeceraTabla(codigoneg);
                if(sald.length>0){
                double vigente = Double.parseDouble(sald[0]);
                tot_vigente = Util.customFormat(vigente);
                double vencido = Double.parseDouble(sald[1]);
                tot_vencido = Util.customFormat(vencido);
                double intereses = Double.parseDouble(sald[2]);
                tot_intereses = Util.customFormat(intereses);
                double gastos = vencido*0.2;
                gastos_cobranza = Util.customFormat(gastos);
                saldo_total = Util.customFormat(vigente+vencido+intereses+gastos);
                }
                //listaf = cserv.facturasNegocio(codigoneg);//comentar
                //num_facts = listaf.size();//old
                num_facts = cserv.conteoFacturasNegocio(codigoneg);//nuevo
            }
            catch(Exception e){
                System.out.println("Error en facturas_cartera.jsp en head: "+e.toString());
                e.printStackTrace();
            }
            DecimalFormat form = new DecimalFormat("#,###,###,###.##");           
          
        %>
            <table id="tablatitulo" border="0" style="border-collapse: collapse; width: 100%">
                <tbody>
                    <tr>
                        <td valign="top">
                            <table border="1" style="border-collapse: collapse;">
                                <thead>
                                    <tr class="subtitulo1">
                                        <td colspan="4" align="center">Datos del Cliente</td>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr>
                                        <td width="10%" class="fila">Nombre</td>
                                        <td class="filablanca" colspan="2">
                                            <input type="hidden" id="orden" value="ASC">
                                            <input type="hidden" id="col" value="f.fecha_vencimiento">
                                            <%
                                                if(listahead!=null && listahead.size()>0){
                                                    out.print(listahead.get(0));
                                                }
                                                else{
                                                    out.print("No se encontro");
                                                }
                                            %>
                                        </td>
                                        <td class="filablanca"><%
                                                if(listahead!=null && listahead.size()>0){
                                                    out.print(listahead.get(1));
                                                }
                                                else{
                                                    out.print("No se encontro");
                                                }
                                            %></td>
                                    </tr>
                                    <tr>
                                        <td class="fila">Telefono</td>
                                        <td  class="filablanca">
                                            <%
                                                if(listahead!=null && listahead.size()>0){
                                                    out.print(listahead.get(2));
                                                }
                                                else{
                                                    out.print("No se encontro");
                                                }
                                            %>
                                        </td>
                                        <td class="fila">Celular</td>
                                        <td  class="filablanca">
                                            <%
                                                if(listahead!=null && listahead.size()>0){
                                                    out.print(listahead.get(3));
                                                }
                                                else{
                                                    out.print("No se encontro");
                                                }
                                            %>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="fila">Email</td>
                                        <td colspan="3" class="filablanca">
                                            <%
                                                if(listahead!=null && listahead.size()>0){
                                                    out.print(listahead.get(4));
                                                }
                                                else{
                                                    out.print("No se encontro");
                                                }
                                            %>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="fila">Direccion</td>
                                        <td class="filablanca" colspan="2"><%
                                                if(listahead!=null && listahead.size()>0){
                                                    out.print(listahead.get(5));
                                                }
                                                else{
                                                    out.print("No se encontro");
                                                }
                                            %></td>
                                        <td class="filablanca"><%
                                                if(listahead!=null && listahead.size()>0){
                                                    out.print(listahead.get(6));
                                                }
                                                else{
                                                    out.print("No se encontro");
                                                }
                                            %></td>
                                    </tr>
                                    <tr>
                                        <td class="fila">Negocio</td>
                                        <td class="filablanca">
                                            <%
                                                if(listahead!=null && listahead.size()>0){
                                                    out.print(listahead.get(7));
                                                }
                                                else{
                                                    out.print("No se encontro");
                                                }
                                            %>
                                        </td>
                                        <td class="fila">Tipo</td>
                                        <td class="filablanca">
                                            <%
                                                if(listahead!=null && listahead.size()>0){
                                                    out.print(listahead.get(8));
                                                }
                                                else{
                                                    out.print("No se encontro");
                                                }
                                            %>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="fila">Afiliado</td>
                                        <td class="filablanca" colspan="3">
                                            <%
                                                if(listahead!=null && listahead.size()>0){
                                                    out.print(listahead.get(9));
                                                }
                                                else{
                                                    out.print("No se encontro");
                                                }
                                            %>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="fila">Sector</td>
                                        <td class="filablanca" colspan="3">
                                            <%
                                                if(listahead!=null && listahead.size()>0){
                                                    out.print(listahead.get(10));
                                                }
                                                else{
                                                    out.print("No se encontro");
                                                }
                                            %>
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                        </td>
                        <td valign="top">&nbsp;
                            
                        </td>
                        <td valign="top" align="right">
                            <table border="1" style="border-collapse: collapse;">
<!--                                <thead>
                                    <tr class="subtitulo1">
                                        <td colspan="2">Codeudor</td>
                                    </tr>
                                </thead>-->
                                <tbody>
                                    <tr>
                                        <td class="subtitulo1" colspan="2" align="center">Resumen</td>
                                    </tr>
                                    <tr>
                                        <td class="fila">Saldo Total a Pagar</td>
                                        <td class="filagris" align="right">
                                            <%= saldo_total %>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="fila">Saldo Corriente</td>
                                        <td class="filablanca" align="right">
                                            <%= tot_vigente %>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="fila">Saldo Vencido</td>
                                        <td class="filablanca" align="right">
                                            <%= tot_vencido %>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="fila">Intereses x Mora</td>
                                        <td class="filablanca" align="right">
                                            <%= tot_intereses %>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="fila">Gastos de Cobranza</td>
                                        <td class="filablanca" align="right">
                                            <%= gastos_cobranza %>
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                        </td>
                    </tr>
                </tbody>
            </table>
            <div style='height:0.2em;'></div>
            <span id="fact" class="curved" onclick="load('facturacion');">Facturacion</span>
            <span id="ing" class="curved1" onclick="load('ingresos');">Ingresos</span>
            <span id="est" class="curved1" onclick="load('estado');">Estado de cuenta</span>
            <span id="estc" class="curved1" onclick="load('estado_clie');">Estado de cuenta Cliente</span>
            <input type="hidden" id="num_facts" name="num_facts" value="<%= num_facts %>">
            <div id="divgen" class="tab">
                <div align="center"><span class="letra">Procesando ... 10% </span><img alt="cargando" src="<%= BASEURL%>/images/cargando.gif" name="imgload"></div>
            </div>
            <div style='height:0.2em;'></div>
            <div align="center">
                <img alt="salir" src="<%= BASEURL%>/images/botones/salir.gif" name="imgsalir" onMouseOver="botonOver(this);" onClick="parent.close();" onMouseOut="botonOut(this);" style="cursor:pointer">
            </div>
        </div>
    </body>

</html>

<div id="facturacion" style="visibility: hidden; display: none;  height: 0px;">
    <div>
        <table border="0" width="100%" style="border-collapse:collapse">
                <tr class="fila">
                    <th width="7%">Filtro</th>
                    <th colspan="11" align="left">
                        <select id="filtro" name="filtro" onchange="facturafiltro('<%=codigoneg%>',this.selectedIndex);"><!-- mod -->
                            <option value="TODAS">Todas</option>
                            <option value="VIGENTE" selected="selected">Vigentes</option>
                            <option value="CANCELADA">Canceladas</option>
                            <option value="SINIESTRADA">Siniestrada</option>
                            <option value="NEGOCIADA">Negociada</option>
                        </select>
                        <span class="gestionspan" onclick="winGestionFact2();">Gestionar Factura(s)</span>
                    </th>
                </tr>
                <tr class="subtitulo1" align="center" style="cursor:pointer;">
                    <th width="8%" onclick="organizar('<%=codigoneg%>','f.documento','ASC',1);" class="head">Factura</th>
                    <th width="7%" onclick="organizar('<%=codigoneg%>','fd.numero_remesa','ASC',2);" class="head">Relacionada</th>
                    <th width="6%" onclick="organizar('<%=codigoneg%>','f.num_doc_fen','ASC',3);" class="head">No. titulo</th>
                    <th width="7%" onclick="organizar('<%=codigoneg%>','f.fecha_vencimiento','DESC',4);" class="asc">Vencimiento</th>
                    <th width="7%" onclick="organizar('<%=codigoneg%>','dias_mora','ASC',5);" class="head">Dias mora</th>
                    <th width="9%" onclick="organizar('<%=codigoneg%>','ndiasf','ASC',6);" class="head">Venc. prejuridico</th>
                    <th width="6%" onclick="organizar('<%=codigoneg%>','estadofact','ASC',7);" class="head">Estado</th>
                    <th width="7%" onclick="organizar('<%=codigoneg%>','f.fecha_ultimo_pago','ASC',8);" class="head">Ult. abono</th>
                    <th width="7%" onclick="organizar('<%=codigoneg%>','f.valor_factura','ASC',10);" class="head">Valor</th>
                    <th width="7%" onclick="organizar('<%=codigoneg%>','f.valor_abono','ASC',9);" class="head">Abonos</th>
                    <th width="8%" onclick="organizar('<%=codigoneg%>','f.valor_saldo','ASC',11);" class="head">Saldo</th>
                    <th width="6%">% cobro</th>
                    <th width="8%">Int. mora</th>
                    <th width="14%">Vr. cobro</th>
                </tr>
            </table>
    </div>
    <div style="overflow: auto; height: 265px;">

        <table border="0" width="100%" style="border-collapse: collapse;">
            <tbody>
                <script type="text/javascript">
                    $("divgen").innerHTML ='<div align="center"><span class="letra">Procesando ... 30%</span><img alt="cargando" src="<%= BASEURL%>/images/cargando.gif" name="imgload"></div>';
                </script>
                <%
                    ArrayList<factura> listafacts = null;
                    factura fact = null;
                    try{
                        listafacts = cserv.facturasNegocioSort(codigoneg, "VIGENTE", "f.fecha_vencimiento", "ASC");
                        //listafacts = cserv.facturasNegocio(codigoneg);
                        if(listafacts!=null && listafacts.size()>0){
                            for(int i=0;i<listafacts.size();i++){
                                fact = listafacts.get(i);
                               double cobro=0;
                               double tcobro=0;
                             if(fact.getDias_mora()<=0){
                                 cobro=0;
                                 }else if(fact.getDias_mora()>0&&fact.getDias_mora()<4){
                                 cobro=2;
                                 }
                               else if(fact.getDias_mora()>3&&fact.getDias_mora()<8){
                                 cobro=15;
                                 }
                               else if(fact.getDias_mora()>7&&fact.getDias_mora()<51){
                                 cobro=20;
                                 }
                               else if(fact.getDias_mora()>50&&fact.getDias_mora()<91){
                                 cobro=25;
                                 }
                               else if(fact.getDias_mora()>90){
                                 cobro=40;
                                 }
                               tcobro = (cobro / 100)*fact.getValor_saldo();
                %>
                <tr class="xfila">
                    <td width="7%">
                        <input type="checkbox" id="fact<%= i%>" name="fact<%= i%>" value="<%= fact.getDocumento()%>">
                        <img alt="mostrar ingresos" src="<%= BASEURL%>/images/botones/iconos/expand.gif" name="imgexpand<%=i%>" id="imgexpand<%=i%>" onclick="expandir(<%=i%>,'<%=fact.getDocumento()%>')" style="cursor:pointer">
                        <span style="cursor:pointer;" onclick="winGestionFact('<% out.print(fact.getDocumento()); %>', '<% out.print(Math.ceil(fact.getValor_saldo()+fact.getValor_saldome()+tcobro)); %>');"><% out.print(fact.getDocumento()); %></span>
                    </td>
                    <td width="8%" align="center"><% out.print(fact.getFactura()); %></td>
                    <td width="7%" align="center"><% out.print(fact.getFisico()); %></td>
                    <td width="9%" align="center"><% out.print(fact.getFecha_vencimiento()); %></td>
                    <td width="7%" align="center"><% out.print(fact.getDias_mora()); %></td>
                    <td width="11%" align="center"><% out.print(fact.getFecha_ven_prejurid()); %></td>
                    <td width="6%" align="center"><% out.print(fact.getEstado()); %></td>
                    <td width="9%" align="center"><% out.print(fact.getFecha_ultimo_pago()); %></td>
                    <td width="9%" align="right">$ <% out.print(Util.customFormat(fact.getValor_factura())); %><input type="hidden" id="vcuot<%=i%>" name="vcuot<%=i%>" value="<% out.print(fact.getValor_saldo()); %>"> </td>
                    <td width="7%" align="right">$ <% out.print(Util.customFormat(fact.getValor_abono())); %></td>
                    <td width="9%" align="right">$ <% out.print(Util.customFormat(fact.getValor_saldo())); %></td>
                    <td width="8%" align="right"><input type="text" id="porc<%=i%>" name="porc<%=i%>" value="<%= cobro %>" style="text-align: right;" onkeyup="soloNumeros(this.id);calculate(this.value,<%=i%>);" size="3"> </td>
                    <td width="10%" align="right"><input type="text" id="morax<%=i%>" name="morax<%=i%>" value="<%= Util.customFormat(Math.ceil(fact.getValor_saldome())) %>" size="10" style="text-align: right;" readonly><input type="hidden" id="mora<%=i%>" name="mora<%=i%>" value="<% out.print(fact.getValor_saldome()); %>"></td>
                    <td width="12%" align="right"><input type="text" id="vx<%=i%>" name="vx<%=i%>" value="<%= Util.customFormat(Math.ceil(fact.getValor_saldo()+fact.getValor_saldome()+tcobro)) %>" size="10" style="text-align: right;" readonly></td>
                </tr>
                <tr>
                    <td></td>
                    <td colspan="13"><div id="diving<%=i%>" style="display:none; padding:0px;"></div></td>
                </tr>

                <%
                            }
                        }
                        else{
                %>
                <tr class="xfila">
                    <td colspan="14" align="center">No se encontraron registros</td>
                </tr>
                <%
                        }
                    }
                    catch(Exception e) {
                %>
                <tr class="xfila">
                    <td colspan="14" align="center">No se encontraron registros</td>
                </tr>
                <%
                        System.out.println("Error al listar las facturas en facturas_cartera.jsp: "+e.toString());
                        e.printStackTrace();
                    }
                %>
            </tbody>
        </table>
    </div>
    <%
        if(listafacts!=null && listafacts.size()>0){
    %>
    <div align="center">
        <img alt="generar xls" src="<%= BASEURL%>/images/botones/exportarExcel.gif" id="imgexc2" name="imgxls" onMouseOver="botonOver(this);" onClick="generarDoc2('xls','<%=codigoneg%>');" onMouseOut="botonOut(this);" style="cursor:pointer">
        <img alt="generar pdf" src="<%= BASEURL%>/images/botones/generarPdf.gif" id="imgpdf2" name="imgpdf" onMouseOver="botonOver(this);" onClick="generarDoc2('pdf','<%=codigoneg%>');" onMouseOut="botonOut(this);" style="cursor:pointer">
    </div>
    <div id="mensaje2" align="center"></div>
    <%
        }
    %>
    </div>
    <script type="text/javascript">
        $("divgen").innerHTML ='<div align="center"><span class="letra">Procesando ... 70%</span><img alt="cargando" src="<%= BASEURL%>/images/cargando.gif" name="imgload"></div>';
    </script>
    <div id="ingresos" style="visibility: hidden; display: none;  height: 0px;">
      <div>
          <table border="0" width="60%" style="border-collapse: collapse;">
            <tr class="subtitulo1" align="center">
                <th width="6%" class="head" onclick="sortIngresos('<%=codigoneg%>','descripcion','ASC','1');">Tipo</th>
                <th width="15%" class="head" onclick="sortIngresos('<%=codigoneg%>','num_ingreso','ASC','2');">Consecutivo</th>
                <th width="14%" class="head" onclick="sortIngresos('<%=codigoneg%>','fecha_creacion','ASC','3');">Creacion</th>
                <th width="17%" class="desc" onclick="sortIngresos('<%=codigoneg%>','fecha_consignacion','DESC','4');">Consignacion</th>
                <th width="27%" class="head" onclick="sortIngresos('<%=codigoneg%>','branch_code','ASC','5');">Banco</th>
                <th width="12%" class="head" onclick="sortIngresos('<%=codigoneg%>','vlr_ingreso','ASC','6');">Valor</th>
                <th width="9%" class="head" onclick="sortIngresos('<%=codigoneg%>','estado','ASC','7');">Estado</th>
            </tr>
          </table>
      </div>
      <div style="overflow: auto; height: 320px;">
        <table border="0" width="60%" style="border-collapse: collapse;">
            <tbody>
                <%
                    ArrayList<Ingreso> lista = null;
                    Ingreso ingreso = null;
                    try{
                        lista = cserv.ingresosNegocio(codigoneg);
                        if(lista!=null && lista.size()>0){
                            for(int i=0;i<lista.size();i++){
                                ingreso = lista.get(i);
                %>
                <tr class="xfila">
                    <td width="6%" align="center"><% out.print(ingreso.getConcepto()); %></td>
                    <td width="15%" align="center">
                        <img alt="mostrar facturas" src="<%= BASEURL%>/images/botones/iconos/expand.gif" id="imgexpandx<%=i%>" name="imgexpandx<%=i%>" onclick="expandir2(<%=i%>,'<%=ingreso.getNum_ingreso()%>')" style="cursor:pointer">
                        <% out.print(ingreso.getNum_ingreso()); %>
                    </td>
                    <td width="14%" align="center"><% out.print(ingreso.getCreation_date()); %></td>
                    <td width="17%" align="center"><% out.print(ingreso.getFecha_consignacion()); %></td>
                    <td width="27%" align="center"><% out.print(ingreso.getBranch_code()); %></td>
                    <td width="12%" align="right">$ <% out.print(Util.customFormat(ingreso.getVlr_ingreso())); %></td>
                    <td width="9%" align="center"><% out.print(ingreso.getReg_status()); %></td>
                </tr>
                <tr><td colspan="7"><div id="divfacts<%=i%>" style="display:none; padding:0px;"></div></td></tr>
                <%
                                ingreso = null;
                            }
                        }
                        else{
                %>
                <tr class="xfila">
                    <td colspan="7" align="center">No se encontraron registros</td>
                </tr>
                <%
                        }
                    }
                    catch(Exception e){
                %>
                <tr class="xfila">
                    <td colspan="7" align="center">No se encontraron registros</td>
                </tr>
                <%
                        System.out.println("Error al listar los ingresos en facturas_cartera.jsp: "+e.toString());
                        e.printStackTrace();
                    }
                %>
            </tbody>
        </table>
      </div>
    </div>
    <script type="text/javascript">
        $("divgen").innerHTML ='<div align="center"><span class="letra">Procesando ... 80%</span><img alt="cargando" src="<%= BASEURL%>/images/cargando.gif" name="imgload"></div>';
    </script>
    <div id="estado" style="visibility: hidden; display: none;  height: 0px;">
        <div>
            <table border="0" width="100%" style="border-collapse: collapse;">

                    <tr class="subtitulo1">
                        <td class="asc" width="117px" align="center" onclick="ordenarEstadoCartera('DESC','fecha_documento','<%= codigoneg %>',1);">Fecha</td>
                        <td class="head" width="100px" align="center" onclick="ordenarEstadoCartera('ASC','documento','<%= codigoneg %>',2);">Documento</td>
                        <td class="head" width="100px" align="center" onclick="ordenarEstadoCartera('ASC','tipo','<%= codigoneg %>',3);">Tipo</td>
                        <td class="head" width="120px" align="center" onclick="ordenarEstadoCartera('ASC','valor_factura','<%= codigoneg %>',4);">Valor factura</td>
                        <td class="head" width="120px" align="center" onclick="ordenarEstadoCartera('ASC','valor_ingreso','<%= codigoneg %>',5);">Valor ingreso</td>
                        <td class="head" width="160px" align="center" onclick="ordenarEstadoCartera('ASC','vr_ajuste_ingreso','<%= codigoneg %>',6);">Vr. ajuste ingreso</td>
                        <td class="head" width="120px" align="center" onclick="ordenarEstadoCartera('ASC','valor_aplicado','<%= codigoneg %>',7);">Vr. aplicado</td>
                        <td class="head" width="260px" align="center" onclick="ordenarEstadoCartera('ASC','cuenta_ajuste','<%= codigoneg %>',8);">Cuenta de ajuste</td>
                        <td class="head" align="center" onclick="ordenarEstadoCartera('ASC','cuenta_cabecera','<%= codigoneg %>',9);">Cuenta cabecera</td>
                    </tr>
<!--                </tbody>-->
            </table>
        </div>
        <%
            double suma1=0;
            double suma2=0;
            double suma3=0;
            double suma4=0;
            ArrayList<BeanGeneral> estado_cuenta = null;
            BeanGeneral beang = null;
            try {
                estado_cuenta = cserv.estadoCuenta(codigoneg, "fecha_documento,substring(documento FROM '[0-9]+')", "");
            }
            catch (Exception e) {
                System.out.println("error: "+e.toString());
                e.printStackTrace();
            }
        %>
        <div style="overflow: auto; height: 250px;">
            <table border="0" width="100%" style="border-collapse: collapse;">
                <tbody>
                    <%
                        if(estado_cuenta!=null && estado_cuenta.size()>0){
                            for(int i=0;i<estado_cuenta.size();i++){
                                beang = estado_cuenta.get(i);
                                suma1 += Double.parseDouble(beang.getValor_04().replaceAll(",", ""));
                                suma2 += Double.parseDouble(beang.getValor_05().replaceAll(",", ""));
                                suma3 += Double.parseDouble(beang.getValor_06().replaceAll(",", ""));
                                suma4 += Double.parseDouble(beang.getValor_07().replaceAll(",", ""));
                    %>
                    <tr class="xfila">
                        <td width="119px" align="center"><%= beang.getValor_01() %></td>
                        <td width="100px" align="center"><%= beang.getValor_02() %></td>
                        <td width="100px" align="center"><%= beang.getValor_03() %></td>
                        <td width="120px" align="right"><%= beang.getValor_04() %></td>
                        <td width="120px" align="right"><%= beang.getValor_05() %></td>
                        <td width="160px" align="right"><%= beang.getValor_06() %></td>
                        <td width="120px" align="right"><%= beang.getValor_07() %></td>
                        <td width="260px" align="center"><%= beang.getValor_08() %></td>
                        <td align="center"><%= beang.getValor_09() %></td>
                    </tr>
                    <%
                            }
                        }
                        else{
                    %>
                    <tr class="xfila">
                        <td colspan="9" align="center"> No se encontraron registros </td>
                    </tr>
                    <%
                        }
                    %>
                </tbody>
            </table>
        </div>
        <div style='height:0.2em;'></div>
        <div id="totales">
            <table width="100%" style="border-collapse: collapse;">
                <tr class="xfila">
                    <td width="117px" align="center" style="border: 0px solid black;">&nbsp;</td>
                    <td width="100px" align="center" style="border: 0px solid black;">&nbsp;</td>
                    <td width="100px" align="center" style="border: 1px solid black;">Totales</td>
                    <td width="120px" align="right" style="border: 1px solid black;"><%= Util.customFormat(suma1) %></td>
                    <td width="120px" align="right" style="border: 1px solid black;"><%= Util.customFormat(suma2) %></td>
                    <td width="160px" align="right" style="border: 1px solid black;"><%= Util.customFormat(suma3) %></td>
                    <td width="120px" align="right" style="border: 1px solid black;"><%= Util.customFormat(suma4) %></td>
                    <td width="260px" align="center" style="border: 0px solid black;">&nbsp;</td>
                    <td align="center" style="border: 0px solid black;">&nbsp;</td>
                </tr>
            </table>
        </div>
        <div style='height:0.2em;'></div>
    <!--    <div align="center">
            <div id="mensaje" align="center"></div>
            <img alt="generar xls" src="<%= BASEURL%>/images/botones/exportarExcel.gif" id="imgexc" name="imgxls" onMouseOver="botonOver(this);" onClick="generarDoc('xls','<%=codigoneg%>');" onMouseOut="botonOut(this);" style="cursor:pointer">
            <img alt="generar pdf" src="<%= BASEURL%>/images/botones/generarPdf.gif" id="imgpdf" name="imgpdf" onMouseOver="botonOver(this);" onClick="generarDoc('pdf','<%=codigoneg%>');" onMouseOut="botonOut(this);" style="cursor:pointer">
        </div>-->

    </div>
    <div id="estado_clie" style="visibility: hidden; display: none;  height: 0px;">
    <div>
        <table border="0" width="100%" style="border-collapse:collapse">
               
                <tr class="subtitulo1" align="center" style="cursor:pointer;">
                    <th width="8%"  class="head">Cuota</th>
                    <th width="10%"  class="head">Fecha Vencimiento</th>
                    <th width="10%"  class="head">Tipo</th>
                    <th width="10%"  class="asc">Estado</th>
                    <th width="10%"  class="head">Dias</th>
                    <th width="14%"  class="head">Vr Factura</th>
                    <th width="14%"  class="head">Vr Saldo Factura</th>
                    <th width="14%"  class="head">Vr a Pagar</th>
                    <th width="10%"  class="head">Fecha Ultimo Pago</th>
                    <th width="10%"  class="head">Dias retraso</th>
            </table>
    </div>
    <div style="overflow: auto; height: 265px;">

        <table border="0" width="100%" style="border-collapse: collapse;">
            <tbody>
                <script type="text/javascript">
                    $("divgen").innerHTML ='<div align="center"><span class="letra">Procesando ... 30%</span><img alt="cargando" src="<%= BASEURL%>/images/cargando.gif" name="imgload"></div>';
                </script>
                <%
                 ArrayList<factura> listafactscli = null;
                    factura factcli = null;
                    double sum1=0,sum2=0, sum3=0;

                    try{
                        listafactscli = cserv.facturasNegocioClie(codigoneg);
                        if(listafactscli!=null && listafactscli.size()>0){
                            for(int i=0;i<listafactscli.size();i++){
                                factcli = listafactscli.get(i);
                                sum1+=factcli.getValor_factura();
                                sum2+=factcli.getValor_saldo();
                                sum3+=factcli.getValor_facturame();
                               
                %>
                <tr class="xfila">                   
                    <td width="8%" align="center"><% out.print(i+1); %></td>
                    <td width="10%" align="center"><% out.print(factcli.getFecha_vencimiento()); %></td>
                    <td width="10%"align="center"><% out.print(factcli.getTipo_documento()); %></td>
                    <td width="10%" align="center"><% out.print(factcli.getEstado()); %></td>
                    <td width="10%" align="center"><% out.print(factcli.getValor_facturame()>0?factcli.getDias_mora():0); %></td>
                    <td width="14%" align="right">$<% out.print(Util.customFormat(factcli.getValor_factura())); %></td>
                    <td width="14%" align="right">$<% out.print(Util.customFormat(factcli.getValor_saldo())); %></td>
                    <td width="14%" align="right">$ <% out.print(Util.customFormat(factcli.getValor_facturame())); %> </td>
                    <td width="10%" align="center"><% out.print(factcli.getFecha_ultimo_pago()); %></td>
                    <td width="10%" align="center"><% out.print(factcli.getDiferencia()); %></td>

                    </tr>
                <tr>
                    <td></td>
                    <td colspan="13"><div id="diving<%=i%>" style="display:none; padding:0px;"></div></td>
                </tr>

                <%
                            }
                        }
                        else{
                %>
                <tr class="xfila">
                    <td colspan="14" align="center">No se encontraron registros</td>
                </tr>
                <%
                        }
                    }
                    catch(Exception e) {
                %>
                <tr class="xfila">
                    <td colspan="14" align="center">No se encontraron registros</td>
                </tr>
                <%
                        System.out.println("Error al listar las facturas en facturas_cartera.jsp: "+e.toString());
                        e.printStackTrace();
                    }
                %>
            </tbody>
        </table>
    </div>
       <div id="totales">
            <table width="100%" style="border-collapse: collapse;">
                <tr class="xfila">
                    <td align="center" style="border: 1px solid black;">TOTALES</td>
                    <td width="14%" align="right" style="border: 1px solid black;">$<%= Util.customFormat(sum1) %></td>
                    <td width="14%" align="right" style="border: 1px solid black;">$<%= Util.customFormat(sum2) %></td>
                    <td width="14%" align="right" style="border: 1px solid black;">$<%= Util.customFormat(sum3) %></td>
                    <td width="10%" align="center" style="border: 1px solid black;">&nbsp;</td>
                </tr>
            </table>
        </div>
    <%
        if(listafacts!=null && listafacts.size()>0){
    %>
<!--    <div align="center">
        <img alt="generar pdf" src="<%= BASEURL%>/images/botones/generarPdf.gif" id="imgpdf2" name="imgpdf" onMouseOver="botonOver(this);" onClick="generarDoc4('pdf','<%=codigoneg%>');" onMouseOut="botonOut(this);" style="cursor:pointer">
    </div>
    <div id="mensaje2" align="center"></div>-->
    <%
        }
    %>
    <!--se pidio que pareciera el pdf aun sin cuotas pendientes-->
    <div align="center">
        <img alt="generar pdf" src="<%= BASEURL%>/images/botones/generarPdf.gif" id="imgpdf2" name="imgpdf" onMouseOver="botonOver(this);" onClick="generarDoc4('pdf','<%=codigoneg%>');" onMouseOut="botonOut(this);" style="cursor:pointer">
    </div>
    <div id="mensaje2" align="center"></div>
    </div>
    <script type="text/javascript">
        $("divgen").innerHTML ='<div align="center"><span class="letra">Procesando ... 90%</span><img alt="cargando" src="<%= BASEURL%>/images/cargando.gif" name="imgload"></div>';
    </script>
<script type="text/javascript">
    $("divgen").innerHTML = $("facturacion").innerHTML;
</script>
<%
            }
            String verpf = request.getParameter("verpfs")!=null? request.getParameter("verpfs"): "false";
            boolean ver = Boolean.parseBoolean(verpf);
            int num_facts = 0;
            if(ver==true){
                String filt=request.getParameter("filt")!=null? request.getParameter("filt"): "";
                ArrayList<factura> listafacts = null;
                cserv = new GestionCarteraService(usuario.getBd());
                DecimalFormat form = new DecimalFormat("#,###,###,###.##");
                String sal="";

                ArrayList<String> listahead = null;
                try{
                    cserv = new GestionCarteraService(usuario.getBd());
                    listahead = cserv.cabeceraTabla("NG00017");
                }
                catch(Exception e){
                    System.out.println("Error en facturas_cartera.jsp en head: "+e.toString());
                    e.printStackTrace();
                }
                String nit="";
                String direccion="";
                String ciudad="";
                try{
                    String[] vec = (cserv.nitNegocio("NG00017")).split(";_;");
                    nit = vec[0];
                    direccion = vec[1];
                    ciudad = vec[2];
                    if (filt.equals( "VIGENTE")) {
                                            listafacts = cserv.facturasPf2(filt, "ASC", "f.fecha_vencimiento");
                                        } else {
                                            listafacts = cserv.facturasPf2(filt, "DESC", "f.fecha_factura");
                                        }
                }
                catch(Exception e){
                    System.out.println("Error al buscar datos para estado de cuenta cabecera: "+e.toString());
                    e.printStackTrace();
                }
%>
            <table id="tablatitulo" border="0" style="border-collapse: collapse; width: 100%">
                <tbody>
                    <tr>
                        <td valign="top">
                            <table border="1" style="border-collapse: collapse; width: 100%">
                                <thead>
                                    <tr class="subtitulo1">
                                        <td colspan="4">Cliente</td>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr>
                                        <td width="10%" class="fila">Nombre</td>
                                        <td class="filablanca" colspan="2">
                                            <%
                                                if(listahead!=null && listahead.size()>0){
                                                    out.print(listahead.get(0));
                                                }
                                                else{
                                                    out.print("No se encontro");
                                                }
                                            %>
                                        </td>
                                        <td class="filablanca"><% out.print(nit); %></td>
                                    </tr>
                                    <tr>
                                        <td class="fila">Telefono</td>
                                        <td colspan="3" class="filablanca">
                                            <%
                                                if(listahead!=null && listahead.size()>0){
                                                    out.print(listahead.get(1));
                                                }
                                                else{
                                                    out.print("No se encontro");
                                                }
                                            %>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="fila">Email</td>
                                        <td colspan="3" class="filablanca">
                                            <%
                                                if(listahead!=null && listahead.size()>0){
                                                    out.print(listahead.get(2));
                                                }
                                                else{
                                                    out.print("No se encontro");
                                                }
                                            %>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="fila">Direccion</td>
                                        <td class="filablanca" colspan="2"><% out.print(direccion); %></td>
                                        <td class="filablanca"><% out.print(ciudad); %></td>
                                    </tr>
                                    <tr>
                                        <td class="fila">Negocio</td>
                                        <td class="filablanca">&nbsp;</td>
                                        <td class="fila">Titulo valor</td>
                                        <td class="filablanca">&nbsp;</td>
                                    </tr>
                                </tbody>
                            </table>
                        </td>
                        <td valign="top">&nbsp;
                            
                        </td>
                        <td valign="top">&nbsp;
                            
                        </td>
                    </tr>
                </tbody>
            </table>
            <br><!--
            <div class="subtitulo1" align="center">Facturas PF y FF</div>
            <br> -->
            <span id="fact" class="curved" onclick="load2('facturacion');">Facturacion</span>
            <span id="ing" class="curved1" onclick="load2('ingresos');">Ingresos</span>
            <input type="hidden" id="num_facts" name="num_facts" value="<%= num_facts %>">
            <div id="divgen" class="tab">
                <div align="center"><span class="letra">Procesando ... 10% </span><img alt="cargando" src="<%= BASEURL%>/images/cargando.gif" name="imgload"></div>
            </div>
            <br>
            <div align="center">
                <img alt="salir" src="<%= BASEURL%>/images/botones/salir.gif" name="imgsalir" onMouseOver="botonOver(this);" onClick="parent.close();" onMouseOut="botonOut(this);" style="cursor:pointer">
            </div>       
    </body>

</html>
<div id="facturacion" style="visibility: hidden; display: none;  height: 0px;">
    <div>
        <table border="0" width="100%" style="border-collapse:collapse">
                <tr class="fila">
                    <th width="7%">Filtro</th>
                    <th colspan="13" align="left">
                        <select id="filtro" name="filtro" onchange="facturafiltro2(this.selectedIndex);" >
                            <option value="TODAS" <%= filt.equals("TODAS")? "selected": "" %> >Todas</option>
                            <option value="VIGENTE" <%= filt.equals("VIGENTE")? "selected": "" %> >Vigentes</option>
                            <option value="CANCELADA" <%= filt.equals("CANCELADA")? "selected": "" %> >Canceladas</option>
                        </select>
                        <span  class="gestionspan" onclick="winGestionFact2();">Gestionar Factura(s)</span>
                    </th>
                </tr>
                <tr class="subtitulo1" align="center" style="cursor:pointer;">
                    <th width="8%" onclick="organizar2('f.documento','ASC',1);" style="background:url(<%=BASEURL%>/images/asc.gif) 7px no-repeat;">Factura</th>
                    <th width="7%" onclick="organizar2('fd.numero_remesa','ASC',2);" style="background:url(<%=BASEURL%>/images/sort.gif) 7px no-repeat;">Relacionada</th>
                    <th width="6%" onclick="organizar2('f.num_doc_fen','ASC',3);" style="background:url(<%=BASEURL%>/images/sort.gif) 7px no-repeat;">No. titulo</th>
                    <th width="7%" onclick="organizar2('f.fecha_vencimiento','ASC',4);" style="background:url(<%=BASEURL%>/images/sort.gif) 7px no-repeat;">Vencimiento</th>
                    <th width="7%" onclick="organizar2('dias_mora','ASC',5);" style="background:url(<%=BASEURL%>/images/sort.gif) 7px no-repeat;">Dias mora</th>
                    <th width="9%" onclick="organizar2('ndiasf','ASC',6);" style="background:url(<%=BASEURL%>/images/sort.gif) 7px no-repeat;">Venc. prejuridico</th>
                    <th width="6%" onclick="organizar2('estadofact','ASC',7);" style="background:url(<%=BASEURL%>/images/sort.gif) 7px no-repeat;">Estado</th>
                    <th width="7%" onclick="organizar2('f.fecha_ultimo_pago','ASC',8);" style="background:url(<%=BASEURL%>/images/sort.gif) 7px no-repeat;">Ult. abono</th>
                    <th width="7%" onclick="organizar2('f.valor_factura','ASC',10);" style="background:url(<%=BASEURL%>/images/sort.gif) 7px no-repeat;">Valor</th>
                    <th width="7%" onclick="organizar2('f.valor_abono','ASC',9);" style="background:url(<%=BASEURL%>/images/sort.gif) 7px no-repeat;">Abonos</th>
                    <th width="8%" onclick="organizar2('f.valor_saldo','ASC',11);" style="background:url(<%=BASEURL%>/images/sort.gif) 7px no-repeat;">Saldo</th>
                    <th width="6%">% cobro</th>
                    <th width="8%">Int. mora</th>
                    <th width="14%">Vr cobro</th>
                </tr>
            </table>
    </div>
    <div style="overflow: auto; height: 265px;">
        <table border="0" width="100%" style="border-collapse: collapse;">
            <tbody>
                <script type="text/javascript">
                    $("divgen").innerHTML ='<div align="center"><span class="letra">Procesando ... 30%</span><img alt="cargando" src="<%= BASEURL%>/images/cargando.gif" name="imgload"></div>';
                </script>
                <%

                    factura fact = null;
                    try{

                        if(listafacts!=null && listafacts.size()>0){
                            for(int i=0;i<listafacts.size();i++){
                                fact = listafacts.get(i);
                %>
                <tr class="xfila">
                    <td width="7%">
                        <input type="checkbox" id="fact<%= i%>" name="fact<%= i%>" value="<%= fact.getDocumento()%>">
                        <img alt="mostrar ingresos" src="<%= BASEURL%>/images/botones/iconos/expand.gif" name="imgexpand<%=i%>" id="imgexpand<%=i%>" onclick="expandir(<%=i%>,'<%=fact.getDocumento()%>')" style="cursor:pointer">
                        <span style="cursor:pointer;" onclick="winGestionFact('<% out.print(fact.getDocumento()); %>', '<% out.print(Math.ceil(fact.getValor_saldo()+fact.getValor_saldome())); %>');"><% out.print(fact.getDocumento()); %></span>
                    </td>
                    <td width="8%" align="center"><% out.print(fact.getFactura()); %></td>
                    <td width="7%" align="center"><% out.print(fact.getFisico()); %></td>
                    <td width="9%" align="center"><% out.print(fact.getFecha_vencimiento()); %></td>
                    <td width="7%" align="center"><% out.print(fact.getDias_mora()); %></td>
                    <td width="11%" align="center"><% out.print(fact.getFecha_ven_prejurid()); %></td>
                    <td width="6%" align="center"><% out.print(fact.getEstado()); %></td>
                    <td width="9%" align="center"><% out.print(fact.getFecha_ultimo_pago()); %></td>
                    <td width="9%" align="right">$ <% out.print(Util.customFormat(fact.getValor_factura())); %><input type="hidden" id="vcuot<%=i%>" name="vcuot<%=i%>" value="<% out.print(fact.getValor_saldo()); %>"> </td>
                    <td width="7%" align="right">$ <% out.print(Util.customFormat(fact.getValor_abono())); %></td>
                    <td width="9%" align="right">$ <% out.print(Util.customFormat(fact.getValor_saldo())); %></td>
                    <td width="8%" align="right"><input type="text" id="porc<%=i%>" name="porc<%=i%>" value="0" style="text-align: right;" onkeyup="soloNumeros(this.id);calculate(this.value,<%=i%>);" size="3"> </td>
                    <td width="10%" align="right"><input type="text" id="morax<%=i%>" name="morax<%=i%>" value="<%= Util.customFormat(Math.ceil(fact.getValor_saldome())) %>" size="10" style="text-align: right;" readonly><input type="hidden" id="mora<%=i%>" name="mora<%=i%>" value="<% out.print(fact.getValor_saldome()); %>"></td>
                    <td width="12%" align="right"><input type="text" id="vx<%=i%>" name="vx<%=i%>" value="<%= Util.customFormat(Math.ceil(fact.getValor_saldo()+fact.getValor_saldome())) %>" size="10" style="text-align: right;" readonly></td>
                </tr>
                <tr class="xfila">
                    <td></td>
                    <td colspan="13"><div id="diving<%=i%>" style="display:none; padding:0px;"></div></td>
                </tr>

                <%
                            }
                        }
                        else{
                %>
                <tr class="xfila">
                    <td colspan="14" align="center">No se encontraron registros</td>
                </tr>
                <%
                        }
                    }
                    catch(Exception e) {
                %>
                <tr class="xfila">
                    <td colspan="14" align="center">No se encontraron registros</td>
                </tr>
                <%
                        System.out.println("Error al listar las facturas en facturas_cartera.jsp: "+e.toString());
                        e.printStackTrace();
                    }
                %>
            </tbody>
        </table>
    </div>
    <%
        if(listafacts!=null && listafacts.size()>0){
    %>
    <div align="center">
        <img alt="generar xls" src="<%= BASEURL%>/images/botones/exportarExcel.gif" id="imgexc3" name="imgxls" onMouseOver="botonOver(this);" onClick="generarDoc3('xls');" onMouseOut="botonOut(this);" style="cursor:pointer">
        <img alt="generar pdf" src="<%= BASEURL%>/images/botones/generarPdf.gif" id="imgpdf3" name="imgpdf" onMouseOver="botonOver(this);" onClick="generarDoc3('pdf');" onMouseOut="botonOut(this);" style="cursor:pointer">
    </div>
    <div id="mensaje2" align="center"></div>
    <%
        }
    %>
    </div>
    <script type="text/javascript">
        $("divgen").innerHTML ='<div align="center"><span class="letra">Procesando ... 70%</span><img alt="cargando" src="<%= BASEURL%>/images/cargando.gif" name="imgload"></div>';
    </script>
    <div id="ingresos" style="visibility: hidden; display: none;  height: 0px;">
      <div>
          <table border="0" width="60%" style="border-collapse: collapse;">
            <tr class="subtitulo1" align="center">
                <th width="6%" class="head" onclick="sortIngresos2('tdc.descripcion','ASC','1');">Tipo</th>
                <th width="15%" class="head" onclick="sortIngresos2('i.num_ingreso','ASC','2');">Consecutivo</th>
                <th width="14%" class="head" onclick="sortIngresos2('fecha_creacion','ASC','3');">Creacion</th>
                <th width="17%" class="desc" onclick="sortIngresos2('i.fecha_consignacion','DESC','4');">Consignacion</th>
                <th width="27%" class="head" onclick="sortIngresos2('i.branch_code','ASC','5');">Banco</th>
                <th width="12%" class="head" onclick="sortIngresos2('i.vlr_ingreso','ASC','6');">Valor</th>
                <th width="9%" class="head" onclick="sortIngresos2('estado','ASC','7');">Estado</th>
            </tr>
          </table>
      </div>
      <div style="overflow: auto; height: 320px;">
        <table border="0" width="60%" style="border-collapse: collapse;">
            <tbody>
                <%
                    ArrayList<Ingreso> lista = null;
                    Ingreso ingreso = null;
                    try{
                        lista = cserv.ingresosPf();
                        if(lista!=null && lista.size()>0){
                            for(int i=0;i<lista.size();i++){
                                ingreso = lista.get(i);
                %>
                <tr class="xfila">
                    <td width="6%" align="center"><% out.print(ingreso.getConcepto()); %></td>
                    <td width="15%" align="center">
                        <img alt="mostrar facturas" src="<%= BASEURL%>/images/botones/iconos/expand.gif" id="imgexpandx<%=i%>" name="imgexpandx<%=i%>" onclick="expandir2(<%=i%>,'<%=ingreso.getNum_ingreso()%>')" style="cursor:pointer">
                        <% out.print(ingreso.getNum_ingreso()); %>
                    </td>
                    <td width="14%" align="center"><% out.print(ingreso.getCreation_date()); %></td>
                    <td width="17%" align="center"><% out.print(ingreso.getFecha_consignacion()); %></td>
                    <td width="27%" align="center"><% out.print(ingreso.getBranch_code()); %></td>
                    <td width="12%" align="right">$ <% out.print(Util.customFormat(ingreso.getVlr_ingreso())); %></td>
                    <td width="9%" align="center"><% out.print(ingreso.getReg_status()); %></td>
                </tr>
                <tr><td colspan="7"><div id="divfacts<%=i%>" style="display:none; padding:0px;"></div></td></tr>
                <%
                                ingreso = null;
                            }
                        }
                        else{
                %>
                <tr class="xfila">
                    <td colspan="7" align="center">No se encontraron registros</td>
                </tr>
                <%
                        }
                    }
                    catch(Exception e){
                %>
                <tr class="xfila">
                    <td colspan="7" align="center">No se encontraron registros</td>
                </tr>
                <%
                        System.out.println("Error al listar los ingresos en facturas_cartera.jsp: "+e.toString());
                        e.printStackTrace();
                    }
                %>
            </tbody>
        </table>
      </div>
    </div>
    <script type="text/javascript">
        $("divgen").innerHTML ='<div align="center"><span class="letra">Procesando ... 80%</span><img alt="cargando" src="<%= BASEURL%>/images/cargando.gif" name="imgload"></div>';
    </script>
    <div id="estado" style="visibility: hidden; display: none;  height: 0px;">
        <div>
            <table border="0" width="100%" style="border-collapse: collapse;">
<!--                <thead>
                    <tr align="left" class="fila">
                        <th width="9%">Saldo total negocio</th>
                        <th colspan="8" align="left"><% out.print(sal); %></th>
                    </tr>
                </thead>-->
                <tbody>
                    <tr class="subtitulo1">
                        <td class="asc" width="117px" align="center" onclick="ordenarEstadoCartera2('DESC','fecha_documento',1);">Fecha</td>
                        <td class="head" width="100px" align="center" onclick="ordenarEstadoCartera2('ASC','documento',2);">Documento</td>
                        <td class="head" width="100px" align="center" onclick="ordenarEstadoCartera2('ASC','tipo',3);">Tipo</td>
                        <td class="head" width="120px" align="center" onclick="ordenarEstadoCartera2('ASC','valor_factura',4);">Valor factura</td>
                        <td class="head" width="120px" align="center" onclick="ordenarEstadoCartera2('ASC','valor_ingreso',5);">Valor ingreso</td>
                        <td class="head" width="160px" align="center" onclick="ordenarEstadoCartera2('ASC','vr_ajuste_ingreso',6);">Vr. ajuste ingreso</td>
                        <td class="head" width="120px" align="center" onclick="ordenarEstadoCartera2('ASC','valor_aplicado',7);">Vr. aplicado</td>
                        <td class="head" width="260px" align="center" onclick="ordenarEstadoCartera2('ASC','cuenta_ajuste',8);">Cuenta de ajuste</td>
                        <td class="head" align="center" onclick="ordenarEstadoCartera2('ASC','cuenta_cabecera',9);">Cuenta cabecera</td>
                    </tr>
                </tbody>
            </table>
        </div>
        <%
            double suma1=0;
            double suma2=0;
            double suma3=0;
            double suma4=0;
            ArrayList<BeanGeneral> estado_cuenta = null;
            BeanGeneral beang = null;
            try {
         //       estado_cuenta = cserv.estadoCuentaPf("fecha_documento,substring(documento FROM '[0-9]+')", "");
            }
            catch (Exception e) {
                System.out.println("error: "+e.toString());
                e.printStackTrace();
            }
        %>
        <div style="overflow: auto; height: 250px;">
            <table border="0" width="100%" style="border-collapse: collapse;">
                <tbody>
                    <%
                        if(estado_cuenta!=null && estado_cuenta.size()>0){
                            for(int i=0;i<estado_cuenta.size();i++){
                                beang = estado_cuenta.get(i);
                                suma1 += Double.parseDouble(beang.getValor_04().replaceAll(",", ""));
                                suma2 += Double.parseDouble(beang.getValor_05().replaceAll(",", ""));
                                suma3 += Double.parseDouble(beang.getValor_06().replaceAll(",", ""));
                                suma4 += Double.parseDouble(beang.getValor_07().replaceAll(",", ""));
                    %>
                    <tr class="xfila">
                        <td width="119px" align="center"><%= beang.getValor_01() %></td>
                        <td width="100px" align="center"><%= beang.getValor_02() %></td>
                        <td width="100px" align="center"><%= beang.getValor_03() %></td>
                        <td width="120px" align="right"><%= beang.getValor_04() %></td>
                        <td width="120px" align="right"><%= beang.getValor_05() %></td>
                        <td width="160px" align="right"><%= beang.getValor_06() %></td>
                        <td width="120px" align="right"><%= beang.getValor_07() %></td>
                        <td width="260px" align="center"><%= beang.getValor_08() %></td>
                        <td align="center"><%= beang.getValor_09() %></td>
                    </tr>
                    <%
                            }
                        }
                        else{
                    %>
                    <tr class="xfila">
                        <td colspan="9" align="center"> No se encontraron registros </td>
                    </tr>
                    <%
                        }
                    %>
                </tbody>
            </table>
        </div>
        <div style='height:0.2em;'></div>
        <div id="totales">
            <table width="100%" style="border-collapse: collapse;">
                <tr class="xfila">
                    <td width="117px" align="center" style="border: 0px solid black;">&nbsp;</td>
                    <td width="100px" align="center" style="border: 0px solid black;">&nbsp;</td>
                    <td width="100px" align="center" style="border: 1px solid black;">Totales</td>
                    <td width="120px" align="right" style="border: 1px solid black;"><%= Util.customFormat(suma1) %></td>
                    <td width="120px" align="right" style="border: 1px solid black;"><%= Util.customFormat(suma2) %></td>
                    <td width="160px" align="right" style="border: 1px solid black;"><%= Util.customFormat(suma3) %></td>
                    <td width="120px" align="right" style="border: 1px solid black;"><%= Util.customFormat(suma4) %></td>
                    <td width="260px" align="center" style="border: 0px solid black;">&nbsp;</td>
                    <td align="center" style="border: 0px solid black;">&nbsp;</td>
                </tr>
            </table>
        </div>
        <div style='height:0.2em;'></div>
        <!--
        <br>
        <div align="center">
            <img alt="generar xls" src="<%= BASEURL%>/images/botones/exportarExcel.gif" id="imgexc" name="imgxls" onMouseOver="botonOver(this);" onClick="generarDoc('xls','<%=codigoneg%>');" onMouseOut="botonOut(this);" style="cursor:pointer">
            <img alt="generar pdf" src="<%= BASEURL%>/images/botones/generarPdf.gif" id="imgpdf" name="imgpdf" onMouseOver="botonOver(this);" onClick="generarDoc('pdf','<%=codigoneg%>');" onMouseOut="botonOut(this);" style="cursor:pointer">
        </div>
        <br> -->
        <div id="mensaje" align="center"></div>
    </div>
    <script type="text/javascript">
        $("divgen").innerHTML ='<div align="center"><span class="letra">Procesando ... 90%</span><img alt="cargando" src="<%= BASEURL%>/images/cargando.gif" name="imgload"></div>';
    </script>
<script type="text/javascript">
    $("divgen").innerHTML = $("facturacion").innerHTML;
</script>
<%
            }
%>