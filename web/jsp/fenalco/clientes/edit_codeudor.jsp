<%-- 
    Document   : edit_codeudor
    Created on : 19/05/2010, 10:08:10 AM
    Author     : rhonalf
--%>

<%@page session="true"%>
<%@page import="java.util.*"%>
<%@page import="com.tsp.operation.model.*"%>
<%@page import="com.tsp.operation.model.beans.*"%>
<%@page import="com.tsp.operation.model.services.*"%>
<%@page import="com.tsp.util.*"%>
<%@include file="/WEB-INF/InitModel.jsp"%>
<%@ taglib uri="http://jakarta.apache.org/taglibs/input-0.90" prefix="input" %>
<%@page contentType="text/html" pageEncoding="iso-8859-1"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
   "http://www.w3.org/TR/html4/loose.dtd">
<!-- This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>. -->
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
        <script type='text/javascript' src="<%= BASEURL %>/js/boton.js"></script>
        <script type='text/javascript' src="<%= BASEURL %>/js/prototype.js"></script>
        <title>Codeudor - Editar</title>
        <link href="<%= BASEURL %>/css/estilostsp.css" rel="stylesheet" type="text/css">
        <%
            String ced = "";
            Codeudor codeudor = null;
            CodeudorService cserv = null;
            try{
                ced = request.getParameter("ced")!=null ? request.getParameter("ced") : "";
                cserv = new CodeudorService();
                codeudor = cserv.buscarDatosCodeudor("id", ced);
            }
            catch(Exception e){
                System.out.println("Error al buscar los datos en el jsp edit_codeudor: "+e.toString());
                e.printStackTrace();
            }
        %>
        <script type="text/javascript">
            function enviarForm(){
                document.getElementById("formulario").submit();
            }
        </script>
    </head>
    <body>
        <div id="capaSuperior" style="position:absolute; width:100%; height:100px; z-index:0; left: 0px; top: 0px;">
            <jsp:include page="/toptsp.jsp?encabezado=Codeudor - Editar"/>
        </div>
        <div id="capaCentral" style="position:absolute; width:100%; height:85%; z-index:0; left: 0px; top: 100px; overflow:visible;">
            <form action="<%= CONTROLLER %>?estado=Codeudor&accion=Gestion&opcion=modificar" method="POST" id="formulario" name="formulario">
                <table border="1"  width="100%" style="border-collapse: collapse; padding: 2px;">
                    <thead>
                        <tr class="subtitulo1">
                            <th>Dato</th>
                            <th>Valor</th>
                        </tr>
                    </thead>
                    <tbody>
                        <%
                            if(codeudor!=null){
                        %>
                        <tr class="fila">
                            <td>Identificacion</td>
                            <td> <input type="text" name="id" id="id" value="<%= codeudor.getId() %>" size="100" readonly> </td>
                        </tr>
                        <tr class="fila">
                            <td>Tipo Identificacion</td>
                            <td>
                                <select id="tipo_id" name="tipo_id">
                                    <option value="CED" <% if(codeudor.getTipo_id().equals("CED")){ out.print(" selected"); } %> >Cedula</option>
                                    <option value="NIT" <% if(codeudor.getTipo_id().equals("NIT")){ out.print(" selected"); } %> >NIT</option>
                                </select>
                            </td>
                        </tr>
                        <tr class="fila">
                            <td>Lugar de expedicion</td>
                            <td>
                                <select id="expedicion" name="expedicion">
                                    <%
                                        try{
                                            CiudadService cius = new CiudadService();
                                            Ciudad ciu = cius.obtenerCiudad(codeudor.getExpedicion_id());
                                            Ciudad ciu2=null;
                                            cius.listarCiudadGeneral();
                                            Vector listac = cius.obtCiudades();
                                            for(int i=0;i<listac.size();i++){
                                                ciu2 = (Ciudad)listac.get(i);
                                     %>
                                     <option value="<%= ciu2.getcodciu() %>" <%if((ciu2.getcodciu()).equals(ciu.getcodciu())){ out.print(" selected"); }%>><%= ciu2.getnomciu() %></option>
                                     <%
                                            }
                                        }
                                        catch(Exception e){
                                            System.out.println("Error listando los datos de ciudades en edit_codeudor.jsp: "+e.toString());
                                            e.printStackTrace();
                                        }
                                    %>
                                </select>
                            </td>
                        </tr>
                        <tr class="fila">
                            <td>Nombre</td>
                            <td> <input type="text" name="nombre" id="nombre" value="<%= codeudor.getNombre() %>" size="100"> </td>
                        </tr>
                        <tr class="fila">
                            <td>Telefono</td>
                            <td> <input type="text" name="telefono" id="telefono" value="<%= codeudor.getTelefono() %>" size="100"> </td>
                        </tr>
                        <tr class="fila">
                            <td>Celular</td>
                            <td> <input type="text" name="celular" id="celular" value="<%= codeudor.getCel() %>" size="100"> </td>
                        </tr>
                        <tr class="fila">
                            <td>Direccion</td>
                            <td> <input type="text" name="direccion" id="direccion" value="<%= codeudor.getDireccion() %>" size="100"> </td>
                        </tr>
                        <tr class="fila">
                            <td>Ciudad</td>
                            <td>
                                <select id="ciudad" name="ciudad">
                                    <%
                                        try{
                                            CiudadService cius = new CiudadService();
                                            Ciudad ciu = cius.obtenerCiudad(codeudor.getCiudad());
                                            Ciudad ciu2=null;
                                            cius.listarCiudadGeneral();
                                            Vector listac = cius.obtCiudades();
                                            for(int i=0;i<listac.size();i++){
                                                ciu2 = (Ciudad)listac.get(i);
                                     %>
                                     <option value="<%= ciu2.getcodciu() %>" <%if((ciu2.getcodciu()).equals(ciu.getcodciu())){ out.print(" selected"); }%>><%= ciu2.getnomciu() %></option>
                                     <%
                                            }
                                        }
                                        catch(Exception e){
                                            System.out.println("Error listando los datos de ciudades en edit_codeudor.jsp: "+e.toString());
                                            e.printStackTrace();
                                        }
                                    %>
                                </select>
                            </td>
                        </tr>
                        <tr class="fila">
                            <td>Universidad</td>
                            <td>
                                <select name="universidad" id="universidad">
                                    <option value="FUNDACION UNIVERSIDAD DEL NORTE" <% if(codeudor.getUniversidad().equals("FUNDACION UNIVERSIDAD DEL NORTE")){ out.print(" selected"); } %> >FUNDACION UNIVERSIDAD DEL NORTE</option>
                                    <option value="UNIVERSIDAD SIMON BOLIVAR" <% if(codeudor.getUniversidad().equals("UNIVERSIDAD SIMON BOLIVAR")){ out.print(" selected"); } %> >UNIVERSIDAD SIMON BOLIVAR</option>
                                    <option value="UNIVERSIDAD AUTONOMA DEL CARIBE" <% if(codeudor.getUniversidad().equals("UNIVERSIDAD AUTONOMA DEL CARIBE")){ out.print(" selected"); } %> >UNIVERSIDAD AUTONOMA DEL CARIBE</option>
                                    <option value="POLITECNICO DE LA COSTA ATLANTICA" <% if(codeudor.getUniversidad().equals("POLITECNICO DE LA COSTA ATLANTICA")){ out.print(" selected"); } %> >POLITECNICO DE LA COSTA ATLANTICA</option>
                                    <option value="UNIVERSIDAD LIBRE" <% if(codeudor.getUniversidad().equals("UNIVERSIDAD LIBRE")){ out.print(" selected"); } %> >UNIVERSIDAD LIBRE</option>
                                    <option value="CORPORACION UNIVERSITARIA DE LA COSTA" <% if(codeudor.getUniversidad().equals("CORPORACION UNIVERSITARIA DE LA COSTA")){ out.print(" selected"); } %> >CORPORACION UNIVERSITARIA DE LA COSTA</option>
                                    <option value="FUNDAION UNIVERSITARIA TECNOLOGICO COMFENALCO" <% if(codeudor.getUniversidad().equals("FUNDAION UNIVERSITARIA TECNOLOGICO COMFENALCO")){ out.print(" selected"); } %> >FUNDAION UNIVERSITARIA TECNOLOGICO COMFENALCO</option>
                                    <option value="UNIVERSIDAD DE SAN BUENAVENURA" <% if(codeudor.getUniversidad().equals("UNIVERSIDAD DE SAN BUENAVENURA")){ out.print(" selected"); } %> >UNIVERSIDAD DE SAN BUENAVENURA</option>
                                    <option value="CORPORACION UNIVERSITARIA RAFAEL NU&Ntilde;EZ" <% if(codeudor.getUniversidad().equals("CORPORACION UNIVERSITARIA RAFAEL NU\u00d1EZ")){ out.print(" selected"); } %> >CORPORACION UNIVERSITARIA RAFAEL NU&Ntilde;EZ</option>
                                </select>
                            </td>
                        </tr>
                        <tr class="fila">
                            <td>Carrera</td>
                            <td> <input type="text" name="carrera" id="carrera" value="<%= codeudor.getCarrera() %>" size="100"> </td>
                        </tr>
                        <tr class="fila">
                            <td>Semestre</td>
                            <td> <input type="text" name="semestre" id="semestre" value="<%= codeudor.getSemestre() %>" size="100"> </td>
                        </tr>
                        <tr class="fila">
                            <td>Codigo</td>
                            <td> <input type="text" name="codigo" id="codigo" value="<%= codeudor.getCod() %>" size="100"> </td>
                        </tr>
                        <%
                            }
                            else{
                        %>
                        <tr class="fila">
                            <td colspan="2">No se han podido encontrar los datos...</td>
                        </tr>
                        <%
                            }
                        %>
                    </tbody>
                </table>
            </form>
            <br>
            <div align="center">
                <img alt="aceptar" src="<%= BASEURL%>/images/botones/aceptar.gif" name="imgaceptar" onMouseOver="botonOver(this);" onClick="enviarForm();" onMouseOut="botonOut(this);" style="cursor:pointer">
                <img alt="salir" src="<%= BASEURL%>/images/botones/salir.gif" name="imgsalir" onMouseOver="botonOver(this);" onClick="parent.close();" onMouseOut="botonOut(this);" style="cursor:pointer">
            </div>
        </div>
    </body>
</html>
<%
    String mensaje ="";
    mensaje = request.getParameter("mens")!=null? request.getParameter("mens"): "";
    if(!mensaje.equals("") && mensaje!=null){
%>
<script type="text/javascript">
    alert('<%= mensaje %>');
</script>
<%
    }
%>