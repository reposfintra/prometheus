<%-- 
    Document   : EditarClientes
    Created on : 21/05/2014, 10:16:56 AM
    Author     : egonzalez
--%>

<%@page contentType="text/html" pageEncoding="ISO-8859-1"%>
<%@include file="/WEB-INF/InitModel.jsp"%>
<!DOCTYPE html>
<html>

    
<script type="text/javascript" src="./js/editarClientesSC.js"></script>
<form class="jotform-form" method="post" name="form" id="form" >

    <table width="765" height="18" border='0' align='center' cellpadding="0" cellspacing="0" class="tablas" id="tbl_proceso">

            <tr>
                <td class="titulo_ventana" id="drag_actualizar_datos" colspan="18">
                    <div style="float:left">ACTUALIZAR CLIENTE<input type="hidden" name="id_usuario" id="id_usuario" value="coco"/></div> 
                    <div style="float:right" onClick="$('#div_actualizar_datos').fadeOut('slow');"><a class="ui-widget-header ui-corner-all"><span>X</span></a></div>
                </td>
            </tr>    

    </table> 

    <input type="hidden" name="formID" value="41403657525655" />
     <div id="contenido">
                     <table border="0" class="tabla">
                            <tr>
                                <td colspan="3">
                                    <table>
                                        <tr>
                                            <td class="td">
                                                <label for="codigo">Codigo</label><br>
                                                <input id="codigo" name="codigo" size="10" type="text" readonly />
                                            </td>
                                            <td class="td">

                                                <label for="nit">Nit � Cedula</label> <br>
                                                <input id="nit" name="nit" size="12" maxlength="10" type="text" value="<%= request.getParameter("cedula")%>"/>
                                            </td>
                                            <td class="td">
                                                <label for="name">Nombre Cliente</label><br>
                                                <input  id="name" name="name" class="inpt" size="53" type="text" />
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                            <tr>
                                <td class="td">
                                    <label for="direccion">Direccion</label><br/>
                                    <input id="direccion" name="direccion" class="inpt" size="35" type="text" readonly/>   
                                    &nbsp;
                                    <img src="/fintra/images/Direcciones.png" width="26" height="23" border="0" align="ABSMIDDLE" onclick="genDireccion('direccion',event);" alt="Direcciones"  title="Direcciones" />
                             </td>
                                <td class="td">
                                    <label for="">Barrio</label><br/>
                                    <input id="barrio" name="barrio" class="inpt" size="20" type="text" />
                                </td>
                                <td class="td">
                                    <label for="cooddpto">Departamento</label><br/>
                                    <select id="cooddpto" name="cooddptos" ></select>
                                </td>
                            </tr>
                            <tr>
                                <td class="td" colspan="3">
                                    <table>
                                        <tr>
                                            <td class="td">  <label for="codciu">Ciudad</label><br/>
                                                <select id="codciu" name="codciu"></select>
                                            </td>
                                            <td class="td">
                                                <label for="telefono">Telefono</label><br/>
                                                <input id="telefono" name="telefono" class="inpt" size="20" type="text" />
                                            </td>
                                            <td class="td">
                                                <label style="margin-left:25px " for="celular">Celular</label><br/>
                                                <input  style="margin-left:25px " id="celular" name="celular" class="inpt" size="20" type="text" />
                                            </td>

                                        </tr>

                                    </table>
                                </td> 
                            </tr>
                        </table>
                        <br>
                        <hr>
                       <div style="text-align: right;margin-right: 28px">                          
                                    <button id="input_2" type="button" onclick="guardar();" class="form-submit-button form-submit-button-simple_green_apple">
                                        Modificar
                                    </button>                               
                           
                                    <button id="input_9" type="button" onClick="$('#div_actualizar_datos').fadeOut('slow');limpiar();" class="form-submit-button form-submit-button-simple_rose">
                                        Salir
                                    </button>
                        </div>
                    </div>
                    <div id="dialogo" class="ventana" title="Mensaje">
                        <p  id="msj">texto </p>
                    </div>
                    <div id="dialogo2" class="ventana">
                        <p  id="msj2">texto </p> <br/>
                        <center>
                            <img src="/fintra/images/cargandoCM.gif"/>
                        </center>
                    </div>
                    <div id="direccion_dialogo" style="display:none;left: 0; position: absolute; top: 0;padding: 1em 1.2em; margin:0px auto; margin-top:10px; padding:10px; width:370px; min-height:140px; border-radius:4px; background-color:#FFFFFF; box-shadow: 0 2px 5px #666666;"> 
                            <div>
                            <table style="width: 100%;">

                                <tr>
                                    <td class="titulo_ventana" id="drag_direcciones" colspan="3">
                                        <div style="float:center">FORMATO DIRECCIONES<input type="hidden" name="id_usuario" id="id_usuario" value="coco"/></div> 
                                    </td>
                                </tr>                    
                                <tr>  
                                    <td><span>Departamento:</span></td>                     
                                    <td colspan="2"> <select id="dep_dir" name="dep_dir" style="width:20em;"></select>
                                    </td>
                                </tr>
                                <tr>
                                    <td><span>Ciudad:</span></td>     
                                    <td colspan="2">                            
                                        <div id="d_ciu_dir">
                                            <select id="ciu_dir" name="ciu_dir" style="width:20em;"></select>
                                        </div>
                                    </td>
                                </tr> 
                                <tr>
                                    <td>Via Principal</td>
                                    <td>
                                        <select id="via_princip_dir" onchange="setDireccion(2)">                                
                                        </select>
                                    </td>
                                    <td><input type="text" id="nom_princip_dir" class="inpt" style="width: 100%;" onchange="setDireccion(1)"/></td>
                                </tr>
                                <tr>
                                    <!-- <td>Via Generadora</td> -->
                                    <td>Numero</td>
                                    <td>
                                        <select id="via_genera_dir" onchange="setDireccion(1)">                               
                                        </select>
                                    </td>

                                    <td>
                                        <table width="100%" border="0">
                                            <tr>
                                                <td align="center" width="49%">
                                                    <input type="text" id="nom_genera_dir" class="inpt" style="width: 90%;" onchange="setDireccion(1)"/>
                                                </td>
                                                <td width="2%" align="center"> - </td>
                                                <td align="center" width="49%">
                                                    <input type="text" id="placa_dir" class="inpt" style="width: 100%;" onkeypress="setDireccion(1)" onchange="setDireccion(1)"/>
                                                </td>    
                                            </tr>
                                        </table>                        
                                    </td>
                                </tr>
                                <tr>
                                    <td>Complemento</td>
                                    <td colspan="2"><input type="text" id="cmpl_dir" class="inpt" style="width: 100%;" onkeypress="setDireccion(1)" onchange="setDireccion(1)"/></td>
                                </tr>
                                <tr>
                                    <td colspan="3"><input type="text" id="dir_resul" name="" style="width: 100%;" readonly/></td>
                                </tr>
                                <tr>
                                    <td colspan="3">
                                        <button id="accept_dir" type="button" onClick="setDireccion(3);">
                                            Aceptar
                                        </button>
                                        <button id="close_dir" type="button" onClick="setDireccion(0);">
                                            Salir
                                        </button>
                                    </td>
                                </tr>
                            </table>
                            </div>
                    </div>

</form

</html>
