<%@page contentType="text/html" pageEncoding="ISO-8859-1"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">

<%response.setHeader("Content-Type", "text/html; charset=windows-1252");
    response.setHeader("Pragma", "no-cache");
    response.setHeader("Expires", "0");
    response.setHeader("Cache-Control", "no-cache");
    response.setHeader("Cache-Control", "no-store");
    response.setHeader("Cache-Control", "must-revalidate");%>


<%@page session    ="true"%>
<%@page errorPage  ="/error/ErrorPage.jsp"%>
<%@include file    ="/WEB-INF/InitModel.jsp"%>
<%@page import    ="java.util.List" %>

<%@page session="true"%>
<%@page import="java.util.*"%>
<%@page import="com.tsp.operation.model.*"%>
<%@page import="com.tsp.operation.model.beans.*"%>
<%@page import="com.tsp.operation.model.services.*"%>
<%@page import="java.text.DecimalFormat"%>
<%@page import="com.tsp.util.*"%>



<%
    Usuario usuario = (Usuario) session.getAttribute("Usuario");
    GestionCarteraService cserv = new GestionCarteraService(usuario.getBd());
    ArrayList<BeanGeneral> listagest = null;
    BeanGeneral gest = null;
    boolean pasa = true;
    try {
        //Modificamos la consulta de gestiones pendientes...      
        listagest = cserv.gestionesPendientes(usuario.getLogin());
    } catch (Exception e) {
        pasa = false;
        System.out.println("Error al ver la gestion del negocio: " + e.toString());
        e.printStackTrace();
    }
%>


<script src="<%=BASEURL%>/js/prototype.js"      type="text/javascript"></script>
<script src="<%=BASEURL%>/js/effects.js"        type="text/javascript"></script>
<script src="<%=BASEURL%>/js/window.js"         type="text/javascript"></script>
<script src="<%=BASEURL%>/js/window_effects.js" type="text/javascript"></script>  
<script src="<%=BASEURL%>/js/boton.js"          type="text/javascript"></script>

<script  src="<%=BASEURL%>/js/finanzas/contab/GestionHc.js" type="text/javascript"></script>
<script src="<%=BASEURL%>/js/tablekit.js" type="text/javascript"></script>

<link href="<%= BASEURL%>/css/estilostsp.css" rel="stylesheet" type="text/css">
<link href="<%=BASEURL%>/css/mac_os_x.css"      rel="stylesheet" type="text/css">
<link href="<%=BASEURL%>/css/default.css"       rel="stylesheet" type="text/css">
<link href="<%=BASEURL%>/css/alert.css" rel="stylesheet" type="text/css"/>
<link href="<%=BASEURL%>/css/alphacube.css" rel="stylesheet" type="text/css"/>


<script type="text/javascript" language="javascript">
    /*jpinedo*/
	
			
    function verObs(codigo){
        var win2 = new Window({/*className: "mac_os_x",*/
            id: "obscart",
            title: "Observacion",
            width:400,
            height:150,
            /*showEffectOptions: {duration:1},
                            hideEffectOptions: {duration:1},*/
            destroyOnClose: true,
            url:'<%=BASEURL%>/jsp/fenalco/clientes/ver_obs.jsp?codigo='+codigo,
            recenterAuto: false
        });
        win2.showCenter();
    }

    function winGestionFact(factura){
        var win = new Window({
            id: "gestcart",
            title: ".: Gestion de cartera :.",
            width:1200,
            height:550,
            destroyOnClose: true,
            url:'<%=BASEURL%>/jsp/fenalco/clientes/gestion_cartera.jsp?documento='+factura+'&facts='+factura+';_;'+'&valor=0;_;',
            recenterAuto: false
        });
        win.showCenter(true);
        win.maximize();
    }
			
			
						
</script>


<head>
    <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
    <title>Gestiones Pendientes </title>

</head>

<body>
    <div>
        <table width="99%" border="1" style="border-collapse: collapse;" id="tabGestiones" class="sortable">
            <thead>
                <tr class="subtitulo1">
                    <th width="2%">Item</th>
                    <th width="10%" class="date-iso">Fecha</th>
                    <th width="5%">Usuario</th>
                    <th width="5%">Factura</th>
                    <th width="14%">Cliente</th>
                    <th width="14%">Gestion</th>
                    <th width="14%">Prox. accion </th>
                    <th width="10%" class="date-iso">Fecha prox. accion</th>
                    <th width="6%">Tipo</th>
                    <th width="10%">Dato</th>
                    <th width="10%" class="nosort"> Observacion</th>
                </tr>
            </thead>
            <tbody>
              <%if (listagest.size() > 0) {
                   for (int i = 0; i < listagest.size(); i++) {
                       gest = listagest.get(i);%>
                <tr class="fila">
                    <td width="2%" align="center"><%=i + 1%></td>
                    <td width="10%" align="center"><%=gest.getValor_02()%></td>
                    <td width="5%" align="center"><%=gest.getValor_07()%></td>
                    <td width="5%" align="center"> <span onclick="winGestionFact('<%=gest.getValor_06()%>');"  style="text-decoration: none; color: green; cursor: pointer;" ><%=gest.getValor_06()%> </span></td>
                    <td width="14%" align="center"><%=gest.getValor_11()%></td>
                    <td width="14%" align="center"><%=cserv.nombreGestion(gest.getValor_03(), "TIPOGEST")%></td>
                    <td width="14%" align="center"><%=cserv.nombreGestion(gest.getValor_05(), "PRXACCION")%></td>
                    <td width="10%" align="center"><%=gest.getValor_04()%></td>
                    <td width="6%" align="center"><%=cserv.nombreGestion(gest.getValor_09(), "TIPO_DATO")%></td>
                    <td width="10%" align="center"><%=gest.getValor_10()%></td>
                    <td width="10%" align="center"> <span onclick="verObs(<%=gest.getValor_08()%>);"  style="text-decoration: none; color: green; cursor: pointer;" >Clic para ver</span></td>
                </tr>
                <%}
                }%>

            </tbody>
        </table>

    </div>
</body>
</html>



















