<%@ page session="true"%>
<%@ page errorPage="../../../error/error.jsp"%>
<%@page import="com.tsp.operation.model.beans.*, java.util.*"%>
<%@ include file="/WEB-INF/InitModel.jsp"%>
<%@ taglib uri="http://jsptags.com/tags/navigation/pager" prefix="pg" %>
<%@page import="com.tsp.operation.model.*"%>
<html>
<head>
<link href="<%=BASEURL%>/css/estilostsp.css" rel="stylesheet" type="text/css">
<title>Buscar Cliente</title>
<script language="javascript" src="../../../js/validar.js"></script>
<script SRC='<%=BASEURL%>/js/boton.js'></script>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link href="../css/estilostsp.css" rel="stylesheet" type="text/css">
</head>
<body onResize="redimensionar()" onLoad="redimensionar()">
<div id="capaSuperior" style="position:absolute; width:100%; height:100px; z-index:0; left: 0px; top: 0px;">
	<jsp:include page="/WEB-INF/toptsp.jsp?encabezado=Datos sus Cliente"/>
</div>

<%
    String style = "simple";
    String position =  "bottom";
    String index =  "center";
    int maxPageItems = 10;
    int maxIndexPages = 20;  
	Usuario usuario= null;    
    usuario = (Usuario) session.getAttribute("Usuario"); 
	model.clienteService.consultarClienteAll(usuario.getCedula());
    Vector nits = model.clienteService.getClientesVec();
%>
<div id="capaCentral" style="position:center; width:100%; height:85%; z-index:0; left: -75px; top: 99px; ">
<p>&nbsp;</p>
<p>&nbsp;</p>
<p>&nbsp;</p> 
<%if(nits.size()>0){%>
<table width="80%"  border="2" align="center">
  <tr>
    <td><table width="100%" class="tablaInferior">
  <tr>
    <td height="24" colspan="2" nowrap><div align="center" class="titulo">
      <table width="100%"  border="0" class="barratitulo">
        <tr>
          <td width="33%" class="subtitulo1">LISTA DE CLIENTES</td>
          <td width="67%"><img src="<%=BASEURL%>/images/titulo.gif"></td>
        </tr>
      </table>
      </div></td>
  </tr>
  <tr class="subtitulos">
  <td  nowrap colspan="6">
	  <table width="100%" border="1" bordercolor="#999999" bgcolor="#F7F5F4">
  		<tr class="tblTitulo">
  			<td width="154" >CEDULA</td>
	    	<td width="620">NOMBRE</td>
  		</tr>
  <pg:pager
    items="<%=nits.size()%>"
    index="<%= index %>"
    maxPageItems="<%= maxPageItems %>"
    maxIndexPages="<%= maxIndexPages %>"
    isOffset="<%= true %>"
    export="offset,currentPageNumber=pageNumber"
    scope="request">
  <%-- keep track of preference --%>
  <%
      for (int i = offset.intValue(),
	         l = Math.min(i + maxPageItems, nits.size());
	     i < l; i++)
	{
        Cliente cond = (Cliente) nits.elementAt(i);%>
  <pg:item>
	  <tr height="30" class="<%=(i % 2 == 0 )?"filagris":"filaazul"%>" onMouseOver='cambiarColorMouse(this)' nowrap style="cursor:hand" title="Seleccionar Cliente..." onClick="copiarCliente3('<%=cond.getNit()%>','<%=BASEURL%>');">
	    <td class="bordereporte" ><%=cond.getNit()%></td>
    	<td class="bordereporte"><%=cond.getNomcli()%></td>
	  </tr>
  </pg:item>
  <%}
  %>
	  <tr bgcolor="#FFFFFF" class="fila">
    	<td height="30" colspan="2" nowrap><pg:index>
	      <jsp:include page="/WEB-INF/jsp/googlesinimg.jsp" flush="true"/>
    	</pg:index></td>
	  </tr>
  </pg:pager>
 	</table>
</td>
</tr>
</table>
</td>  
</table>
<%}else{%>
<br>  
<table border="2" align="center">
    <tr>
      <td><table width="410" height="73" border="1" align="center"  bordercolor="#F7F5F4" bgcolor="#FFFFFF">
          <tr>
            <td width="282" align="center" class="mensajes">Su b&uacute;squeda no arroj&oacute; resultados!</td>
            <td width="28" background="<%=BASEURL%>/images/cuadronaranja.JPG">&nbsp;</td>
            <td width="78">&nbsp;</td>
          </tr>
      </table></td>
    </tr>
  </table>
<%}%>
<p align="center">
<img src="<%=BASEURL%>/images/botones/regresar.gif"  name="imgsalir" align="absmiddle" style="cursor:hand" onClick="history.back();" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);">
<img src="<%=BASEURL%>/images/botones/salir.gif"  name="imgsalir" align="absmiddle" style="cursor:hand" onClick="parent.close();" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);"></p>
</div>
<%nits=null;%>
</body>
</html>



