<%@page contentType="text/html"%>
<%@page import="com.tsp.operation.model.beans.*, java.util.*, com.tsp.util.*"%>
<%@taglib uri="/WEB-INF/tlds/tagsAgencia.tld" prefix="ag"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://jakarta.apache.org/taglibs/input-0.90" prefix="input" %>
<%@ include file="/WEB-INF/InitModel.jsp"%>
<%@page session="true"%>
<%@page errorPage="../../../error/ErrorPage.jsp"%>
<%@page import="com.tsp.util.*"%>
<%@page import="java.util.*"%> 
<%@page import="com.tsp.operation.model.*"%>
<%
  String msg = (request.getParameter("msg")!=null)?request.getParameter("msg"):"";
  String path = application.getRealPath(model.menuService.getRealUrl(request.getRequestURI()));
  String datos[] = model.menuService.getContenidoMenu(BASEURL,request.getRequestURI(),path);
  String nomc =(request.getParameter ("NomCliente")!=null)?request.getParameter ("NomCliente"):"";
  String ced=(request.getParameter ("CedCliente")!=null)?request.getParameter ("CedCliente"):"";
  String DirC=(request.getParameter ("Direccion")!=null)?request.getParameter ("Direccion"):"";
  String Telef=(request.getParameter ("Telefono")!=null)?request.getParameter ("Telefono"):"";
  String Ciu="";
  String TIden="";
  String codciu="";
  String codcli="";
  String celu=(request.getParameter ("Celular")!=null)?request.getParameter ("Celular"):"";
  String ciuexp="";
  String repleg="";
  String ccrep="";
  String coddos="";
  model.ciudadService.listarCiudadGeneral();
  Vector a = model.ciudadService.obtCiudades(); 
  Ciudad ciu=new Ciudad();
  String cod = (request.getParameter("cod")!=null)?request.getParameter("cod"):"00";
  model.clienteService.ClientesList(cod);
  Vector nits = model.clienteService.getClientesVec();
	if(nits.size() > 0)
	{
	  codcli=((BeanGeneral)nits.get(0)).getValor_01();
	  nomc=((BeanGeneral)nits.get(0)).getValor_02();
	  ced=((BeanGeneral)nits.get(0)).getValor_03();
	  DirC=((BeanGeneral)nits.get(0)).getValor_04();
	  Telef=((BeanGeneral)nits.get(0)).getValor_05();
	  Ciu=((BeanGeneral)nits.get(0)).getValor_09();
	  TIden=((BeanGeneral)nits.get(0)).getValor_10();
	  codciu=((BeanGeneral)nits.get(0)).getValor_06();
	  celu=((BeanGeneral)nits.get(0)).getValor_07();
	  ciuexp=((BeanGeneral)nits.get(0)).getValor_11();
	  repleg=((BeanGeneral)nits.get(0)).getValor_12();
	  ccrep=((BeanGeneral)nits.get(0)).getValor_13();
	  coddos=((BeanGeneral)nits.get(0)).getValor_14();
	}
%>
<html>
<head>
<title>Ingresar Cliente</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link href="../../../css/estilostsp.css" rel="stylesheet" type="text/css"> 
<link href="<%=BASEURL%>/css/estilostsp.css" rel="stylesheet" type="text/css"> 
</head>
<script type='text/javascript' src="<%=BASEURL%>/js/boton.js"></script> 
<script type='text/javascript' src="<%= BASEURL %>/js/validar.js"></script>
<script type='text/javascript' src="<%= BASEURL %>/js/reporte.js"></script>
<link   href="<%= BASEURL %>/css/estilostsp.css" rel='stylesheet'>
<link   href="<%= BASEURL %>/css/EstilosReportes.css" rel='stylesheet'>
<body onResize="redimensionar()" onLoad="redimensionar()">
<div id="capaSuperior" style="position:absolute; width:100%; height:100px; z-index:0; left: 0px; top: 0px;">
	<jsp:include page="/WEB-INF/toptsp.jsp?encabezado=Modificar Datos del Cliente"/>
</div>
<div id="capaCentral" style="position:center; width:100%; height:85%; z-index:0; left: -75px; top: 99px; "> 
<form name="frCliente" id="frCliente" action="<%=CONTROLLER%>?estado=Clientefen&accion=Update&op=1" method="post" >
<p>&nbsp;</p>
<p>&nbsp;</p>
<p>&nbsp;</p>  
<table width="900" border="2" align="center">
  <tr>
    <td width="1084" height="150">
		<table width="99%"   align="center"> 
		  <tr>
			<td width="50%" height="22"  class="subtitulo1"><p align="left">Modificacion datos del Cliente</p></td>
			<td width="50%"  class="barratitulo"><img src="<%=BASEURL%>/images/titulo.gif" width="32" height="20" align="left"><%=datos[0]%></td>
		  </tr>
		</table>
		<table width="99%"  align="center" >
		 <tr class="fila">
			<td height="20" colspan="2" ></td>
			<td height="20"><div align="left">Persona
			      <input name="chk1" type="checkbox" onClick="dest(chk2),oculta(),cambio2(frCliente)" value="p" <%if(TIden.equals("CED")) {%>checked<%}%> >
			  </div></td><td width="108" height="20"><div align="right">Empresa
			      <input name="chk2" type="checkbox" value="e" onClick="campo(),dest(chk1),cambio(frCliente)"  <%if(TIden.equals("NIT")) {%>checked<%}%> >
			  </div></td>
			  <td height="20" colspan="5"></td>
			</tr>	
			
		  <tr class="fila">
			<td height="20" colspan="2">Nombre o Razon Social </td>
			<td height="20" colspan="7"><input name="NomCliente" value="<%=nomc%>" type="text" class="textbox" id="NomCliente" size="100" maxlength="100">			  
			<img  src="<%=BASEURL%>/images/botones/iconos/obligatorio.gif" width="10" height="10"></td>
			</tr>	
			<tr class="fila">
			<td width="116" height="20">Tipo Id </td>
			<td width="97"><select name="tipoid">
			  <%if(TIden.equals("CED"))
			  {%>
			  <option value="CED" selected>CC</option>
			  <option value="NIT">NIT</option>
			  <%}else{%>
			  <option value="CED" >CC</option>
			  <option value="NIT" selected>NIT</option>
			  <%}%>
			  </select>
			  <img  src="<%=BASEURL%>/images/botones/iconos/obligatorio.gif" width="10" height="10"></td>
			<td width="101" height="20">No Documento </td>
			<td height="20" colspan="3"><input name="CedCliente" type="text" class="filaresaltada"  value="<%=ced%>"id="CedCliente" size="15"  maxlength="15" onKeyPress="soloDigitos(event,'decNO')" disabled>			 </td>
			<td width="71" height="20">Expedicion</td>
			<td width="344" height="20" colspan="2">
			<select name="expcc" id="select">
			<option value="<%=coddos%>" selected><%=ciuexp%></option>
              <%for(int i=0;i<a.size();i++)
				{ciu=(Ciudad)a.elementAt (i);
				%>
              <option value="<%=ciu.getcodciu()%>" ><%=ciu.getnomciu()%></option>
              <%}ciu=null;%>
            </select>
			  <img  src="<%=BASEURL%>/images/botones/iconos/obligatorio.gif" width="10" height="10"></td>
			 
			</tr>
			
			<tr class="fila">
			<td width="116" height="20">Celular</td>
			<td width="97"><input name="Celular" type="text" class="textbox" id="CellCliente" size="15"  maxlength="15" value="<%=celu%>" > </td>
			 <td height="20" colspan="7"></td>
			</tr>
				
		  </table>
		  <table width="99%"  align="center" >
		  <tr class="fila">
			<td width="59" height="20">Direccion </td>
			<td width="200" nowrap ><input name="Direccion" type="text" class="textbox" id="Direccion" size="30" maxlength="30"  value="<%=DirC%>" >
                             <img  src="<%=BASEURL%>/images/botones/iconos/obligatorio.gif" width="10" height="10"> &nbsp; </td>
			<td width="89" height="20">Ciudad</td>
			<td colspan="3">
			<select name="c_dir" id="c_dir">
			<option value="<%=codciu%>" selected><%=Ciu%></option>
			<%for(int i=0;i<a.size();i++)
				{
				ciu=(Ciudad)a.elementAt (i);
				%>
					<option value="<%=ciu.getcodciu()%>"><%=ciu.getnomciu()%></option>
				<%ciu=null;
				}%>
			</select> <img  src="<%=BASEURL%>/images/botones/iconos/obligatorio.gif" width="10" height="10"></td>
		  </tr>
		  </table>
		  <table width="99%" align="center">
			<tr class="fila">
				<td width="106" height="7">Telefono</td>
				<td width="350" ><input name="Telefono" type="text" class="textbox" value="<%=Telef%>" id="Telefono2" size="20" maxlength="40">
				  <img  src="<%=BASEURL%>/images/botones/iconos/obligatorio.gif" width="10" height="10"></td>
				<td width="502"  colspan="6">&nbsp;</td>
			</tr>
			<tr class="fila" id="celda" style="display: <% if(TIden.equals("NIT")){ %>block <%}else{%>none <%}%>">
				<td width="95" height="25">Representante Legal</td>
			  <td colspan="2"><input name="replegal" type="text" class="textbox" id="replegal" size="45" maxlength="100" value="<%=repleg%>" >
			    <img  src="<%=BASEURL%>/images/botones/iconos/obligatorio.gif" width="10" height="10"> </td>
				<td width="149" height="20" ><div align="right">C&eacute;dula</div></td>
			  <td width="298"  colspan="4"><input name="ccreplegal" type="text" class="textbox" id="ccreplegal" onKeyPress="soloDigitos(event,'decNO')" value="<%=ccrep%>" >
			    <img  src="<%=BASEURL%>/images/botones/iconos/obligatorio.gif" width="10" height="10"> </td>
			</tr>
	    </table>
		</td>
</tr>
</table>
 <p align="center">&nbsp;</p>
<table width="699" border="0" align="center">
  <tr>
    <td colspan="2" nowrap align="center">
      <img src="<%=BASEURL%>/images/botones/regresar.gif"  name="imgsalir" align="absmiddle" style="cursor:hand" onClick="history.back();" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);">
	  <img  name="modificar" src="<%=BASEURL%>/images/botones/modificar.gif"  style = "cursor:hand" align="middle" type="image" id="Guardar"  onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" onClick="return validar(frCliente)">                  
	  <img src="<%=BASEURL%>/images/botones/restablecer.gif" width="87" name="buss" height="21" align="absmiddle" style="cursor:hand"   onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" onClick="document.location='<%=BASEURL%>/jsp/fenalco/clientes/edit_clientes.jsp'">
	  <img src="<%=BASEURL%>/images/botones/eliminar.gif" width="87" name="eliminar" height="21" align="absmiddle" style="cursor:hand"   onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" onClick="document.location='<%=CONTROLLER%>?estado=Clientefen&accion=Update&op=2&codc=<%=ced%>'">
	  <img src="<%=BASEURL%>/images/botones/salir.gif"  name="imgsalir" align="absmiddle" style="cursor:hand" onClick="parent.close();" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);"> 
	 </td> 
    </tr>
</table>
<input name="codc" type="hidden" value="<%=codcli%>">
</form>
<%if(!msg.equals("")){%>
    <table width="407" border="2" align="center">
      <tr>
        <td><table width="100%"   align="center"  >
            <tr>
              <td width="229" align="center" class="mensajes"><%=msg%></td>
              <td width="29" background="<%=BASEURL%>/images/cuadronaranja.JPG">&nbsp;</td>
              <td width="58">&nbsp;</td>
            </tr>
        </table></td>
      </tr>
    </table>

<%}%>
</div>   
</body>
</html>
<script>
 function validar( forma ){
 		if(forma.chk1.checked==false && forma.chk2.checked==false)
		{
			alert( 'Debe Ingresar Escojer si es Persona o Empresa continuar...' );
			return false;
		}
		if ( forma.NomCliente.value == '' ){
			alert( 'Debe Ingresar el nombre del cliente para continuar...' );
			forma.NomCliente.focus();
			return false;
		}
		if ( forma.CedCliente.value == '' ){
			alert( 'Debe Ingresar el documento del cliente para continuar...' );
			forma.CedCliente.focus();
			return false;
		}
		if ( forma.Direccion.value == '' ){
			alert( 'Debe Ingresar la direccion del cliente para continuar...' );
			forma.Direccion.focus();
			return false;
		}
		if ( forma.Telefono.value == '' ){
			alert( 'Debe Ingresar el telefono del cliente para continuar...' );
			forma.Telefono.focus();
			return false;
		}
		if(forma.chk2.checked==true)
		{
			if ( forma.replegal.value == '' ){
			alert( 'Debe Ingresar el Representante Legal para continuar...' );
			forma.replegal.focus();
			return false;
			}
			
			if ( forma.ccreplegal.value == '' ){
			alert( 'Debe Ingresar la cedula del Representante Legal para continuar...' );
			forma.ccreplegal.focus();
			return false;
			}
		}
forma.submit();
}

function campo()
{
	celda.style.display="block";
}
function dest(a)
{
	a.checked=false;
}
function oculta()
{
	celda.style.display="none";
}
function cambio(forma)
{
	forma.tipoid.value="NIT";
}
function cambio2(forma)
{
	forma.tipoid.value="CED";
}
</script>
