<%-- 
    Document   : codeudor_buscar
    Created on : 18/05/2010, 04:54:14 PM
    Author     : rhonalf
--%>

<%@page session="true"%>
<%@page import="java.util.*"%>
<%@page import="com.tsp.operation.model.*"%>
<%@page import="com.tsp.operation.model.beans.*"%>
<%@page import="com.tsp.operation.model.services.*"%>
<%@page import="com.tsp.util.*"%>
<%@include file="/WEB-INF/InitModel.jsp"%>
<%@ taglib uri="http://jakarta.apache.org/taglibs/input-0.90" prefix="input" %>
<%@page contentType="text/html" pageEncoding="iso-8859-1"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
   "http://www.w3.org/TR/html4/loose.dtd">
<!-- This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>. -->

<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
        <script type='text/javascript' src="<%= BASEURL %>/js/boton.js"></script>
        <script type='text/javascript' src="<%= BASEURL %>/js/prototype.js"></script>
        <title>Codeudor - Buscar</title>
        <link href="<%= BASEURL %>/css/estilostsp.css" rel="stylesheet" type="text/css">
        <style type="text/css">
            .curved{
                padding: 5px;
                border: 1px black solid;
                text-align: center;
                background-color: white;
                -moz-border-radius: 7px;
                -webkit-border-radius: 7px;
            }
        </style>
        <script type="text/javascript">
            function enviarForm(){
                var indice = document.getElementById("filtro").selectedIndex;
                var filtro = document.getElementById("filtro").options[indice].value;
                var cadena = document.getElementById("cadena").value;
                var p = 'opcion=buscar&filtro='+filtro+'&cadena='+cadena+'';
                var url = document.getElementById("formulario").action;
                new Ajax.Request(
                    url,
                    {
                        method: 'get',
                        parameters: p,
                        onLoading: loading,
                        onComplete: llenarDiv
                    }
                );
            }

            function loading(){
                document.getElementById("divbusqueda").className = '';
                document.getElementById("divbusqueda").innerHTML ='<span class="letra">Cargando </span><img alt="cargando" src="<%= BASEURL%>/images/cargando.gif" name="imgload">';
            }

            function llenarDiv(response){
                document.getElementById("divbusqueda").innerHTML = response.responseText;
                document.getElementById("divbusqueda").className = 'curved';
            }

        </script>
    </head>
    <body>
        <div id="capaSuperior" style="position:absolute; width:100%; height:100px; z-index:0; left: 0px; top: 0px;">
            <jsp:include page="/toptsp.jsp?encabezado=Codeudor - Buscar"/>
        </div>
        <div id="capaCentral" style="position:absolute; width:100%; height:85%; z-index:0; left: 0px; top: 100px; overflow:visible;">
            <form action="<%= CONTROLLER %>?estado=Codeudor&accion=Gestion" id="formulario" method="POST">
                <table border="1" width="50%" style="border-collapse: collapse; padding: 2px;">
                    <thead>
                        <tr class="subtitulo1">
                            <th>Filtro</th>
                            <th>Cadena a buscar</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr class="fila">
                            <td>
                                <select id="filtro" name="filtro">
                                    <option value="nombre" selected>Nombre</option>
                                    <option value="id">Cedula</option>
                                </select>
                            </td>
                            <td> <input type="text" id="cadena" name="cadena" value=""> </td>
                        </tr>
                        <tr class="fila">
                            <td colspan="2">
                                <img alt="aceptar" src="<%= BASEURL%>/images/botones/aceptar.gif" name="imgaceptar" onMouseOver="botonOver(this);" onClick="enviarForm();" onMouseOut="botonOut(this);" style="cursor:pointer">
                                <img alt="salir" src="<%= BASEURL%>/images/botones/salir.gif" name="imgsalir" onMouseOver="botonOver(this);" onClick="parent.close();" onMouseOut="botonOut(this);" style="cursor:pointer">
                            </td>
                        </tr>
                    </tbody>
                </table>
            </form>
            <br>
            <div id="res" class="curved"><span class="letra">Resultados de la busqueda</span></div>
            <br>
            <div id="divbusqueda"></div>
        </div>
    </body>
</html>
