<%-- 
    Document   : ver_obs
    Created on : 31/07/2010, 09:53:37 AM
    Author     : rhonalf
--%>
<%@page session="true"%>
<%@page import="java.util.*"%>
<%@page import="com.tsp.operation.model.*"%>
<%@page import="com.tsp.operation.model.beans.*"%>
<%@page import="com.tsp.operation.model.services.*"%>
<%@page import="com.tsp.util.*"%>
<%
    response.addHeader("Cache-Control","no-cache");
    response.addHeader("Pragma","No-cache");
    response.addDateHeader ("Expires", 0);
    String BASEURL = request.getContextPath();
    String CONTROLLER = BASEURL + "/controller";
%>
<%@ taglib uri="http://jakarta.apache.org/taglibs/input-0.90" prefix="input" %>
<%@page contentType="text/html" pageEncoding="ISO-8859-1"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
   "http://www.w3.org/TR/html4/loose.dtd">
<!-- This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>. -->
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
        <title>Ver observacion</title>
        <%
            String codigo = request.getParameter("codigo")!=null ? request.getParameter("codigo") : "1";
            String obs = "";
            GestionCarteraService gserv = null;
            try{
                gserv = new GestionCarteraService();
                obs = gserv.verObs(codigo);
            }
            catch(Exception e){
                System.out.println("Error al visualizar la observacion: "+e.toString());
                e.printStackTrace();
            }
        %>
        <style type="text/css">
            .asc{
                color: #000000;
                font-family: Tahoma, Arial;
                background-color:#FFFFFF;
                font-size: 12px;
            }
            .curved{
                padding: 5px;
                border: 1px black solid;
                -moz-border-radius: 7px;
                -webkit-border-radius: 7px;
            }
        </style>
    </head>
    <body class="asc">
        <textarea name="obs" rows="7" cols="43" class="curved"><% out.print(obs); %></textarea>
    </body>
</html>
