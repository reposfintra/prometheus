<%--  
    Document   : VerCartas
    Created on : 5/09/2011, 10:14:43 AM
    Author     : ing. Iris Vargas
--%>

<%@page import="com.tsp.operation.model.services.GestionCarteraService"%>
<%@page import="com.tsp.operation.model.beans.BeanGeneral"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@page session    ="true"%>
<%@page errorPage  ="/error/ErrorPage.jsp"%>
<%@include file    ="/WEB-INF/InitModel.jsp"%>
<%@page import    ="java.util.*" %>
<%@page import    ="com.tsp.util.*" %>
<%@page import    ="com.tsp.operation.model.beans.Usuario" %>

<script src="<%=BASEURL%>/js/prototype.js"      type="text/javascript"></script>
<script src="<%=BASEURL%>/js/boton.js"          type="text/javascript"></script>
<script src="<%=BASEURL%>/js/effects.js"        type="text/javascript"></script>
<script src="<%=BASEURL%>/js/window.js"         type="text/javascript"></script>
<script src="<%=BASEURL%>/js/window_effects.js" type="text/javascript"></script>

<link href="<%= BASEURL%>/css/estilostsp.css" rel="stylesheet" type="text/css">
<link href="<%=BASEURL%>/css/mac_os_x.css"      rel="stylesheet" type="text/css">
<link href="<%=BASEURL%>/css/default.css"       rel="stylesheet" type="text/css">
<link href="<%=BASEURL%>/css/alert.css" rel="stylesheet" type="text/css"/>
<link href="<%=BASEURL%>/css/alphacube.css" rel="stylesheet" type="text/css"/>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">

<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
        <title>Formularios Cliente</title>
        <script type="text/javascript">


            function busqueda(id_field){
                var dato = $(id_field).value;
                if(dato==''){
                    alert('Para consultar debe escribir algo');
                }
                else{
                    var url = "<%=CONTROLLER%>?estado=GestionSolicitud&accion=Aval";

                    var p = "opcion=src_sol&dato="+dato+"&vista=6";

                    new Ajax.Request(url, { parameters: p, method: 'post', onComplete: completo } );
                }
            }

            function completo(resp){
                var txt = resp.responseText;
                if(txt.charAt(0) == '/'){
                    location.href = '<%= BASEURL%>'+txt;
                }
                else{
                    $('busqueda').innerHTML = txt;
                }
            }

            function generarCarta(cod_carta, neg, saldo, mora, cobro, dias){
                alert('Iniciando la generacion de la carta '+cod_carta);
                var url = "<%= CONTROLLER%>?estado=Gestion&accion=Cartera";
                var p =  'opcion=printcarta&cod_carta='+cod_carta+'&neg='+neg
                    +'&saldo='+saldo+'&mora='+mora+'&cobro='+cobro+'&dias='+dias;
                new Ajax.Request(url,{method: 'post',parameters: p,onComplete: finish});
            }          

            function finish(response){
                alert(response.responseText);                
            }



        </script>
    </head>
    <body>

                                              
                                <div align="center">
                                    <table border="1" style="border-collapse: collapse;" width="100%">
                                        <thead>
                                            <tr class="subtitulo1">
                                                <th>Nombre</th>
                                        </thead>
                                        <tbody>
                                            <%

                                                        GestionCarteraService gserv = null;
                                                        gserv = new GestionCarteraService();
                                                        String neg =  request.getParameter("neg")!=null ? request.getParameter("neg") : "";
                                                        String saldo =  request.getParameter("saldo")!=null ? request.getParameter("saldo") : "";
                                                        String mora =  request.getParameter("mora")!=null ? request.getParameter("mora") : "";
                                                        String cobro =  request.getParameter("cobro")!=null ? request.getParameter("cobro") : "";
                                                        String dias =  request.getParameter("dias")!=null ? request.getParameter("dias") : "";
                                                        ArrayList<String> listadato = null;
                                                        String[] temp2 = null;
                                                        try {
                                                            listadato = gserv.listaGestion("CART_COBRO", "");
                                                        } catch (Exception e) {
                                                            System.out.println("Error obtenieindo la lista de tipos de dato : " + e.toString());
                                                            e.printStackTrace();
                                                        }
                                                        if (listadato != null && listadato.size() > 0) {
                                                            for (int i = 0; i < listadato.size(); i++) {
                                                                temp2 = (listadato.get(i)).split(";_;");
                                            %>
                                            <tr class="filaazul" onclick="generarCarta('<%=temp2[0]%>', '<%=neg%>', '<%=saldo%>', '<%=mora%>', '<%=cobro%>', '<%=dias%>');" >
                                                <td align="left"><% out.print(temp2[1]);%></td>
                                            </tr>
                                            <%
                                                                                                            }
                                                                                                        } else {
                                            %>
                                            <tr class="filaazul">
                                                <td  align="center">No se encontraron registros</td>
                                            </tr>
                                            <%                                                                                                }


                                            %>

                                            <tr class="fila">
                                                <td colspan="2" align="center">
                                                    <div id="busqueda"></div>
                                                </td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>
                           
                    <br>
                    <br>
                    
                    <br>
                    
</html>
