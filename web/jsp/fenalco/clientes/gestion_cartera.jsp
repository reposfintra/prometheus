<%--
    Document   : gestion_cartera
    Created on : 28/07/2010, 03:20:02 PM
    Author     : rhonalf
--%>
<%@page session="true"%>
<%@page import="java.util.*"%>
<%@page import="com.tsp.operation.model.*"%>
<%@page import="com.tsp.operation.model.beans.*"%>
<%@page import="com.tsp.operation.model.services.*"%>
<%@page import="com.tsp.opav.model.services.ClientesVerService"%>
<%@page import="java.text.DecimalFormat"%>
<%@page import="com.tsp.util.*"%>
<%
            response.addHeader("Cache-Control", "no-cache");
            response.addHeader("Pragma", "No-cache");
            response.addDateHeader("Expires", 0);
            String BASEURL = request.getContextPath();
            String CONTROLLER = BASEURL + "/controller";
%>
<%
            String documento = request.getParameter("documento") != null ? request.getParameter("documento") : "";
            GestionCarteraService gserv = null;
            BeanGeneral bean = null;
            String[] facturas = null;
            String[] valortot = null;
            String cad_facts = "";
            String cad_fct = "";
            String valor = "";
            double sumafacts = 0;
            double sumavalortot = 0;
            double sumafacts_neto = 0;
            double sumafacts_int = 0;
             Usuario usuario = (Usuario) session.getAttribute("Usuario");
             ClientesVerService clvsrv = new ClientesVerService(usuario.getBd());
             String perfil = clvsrv.getPerfil(usuario.getLogin());
            try {
                cad_facts = request.getParameter("facts") != null ? request.getParameter("facts") : "";
                valor = request.getParameter("valor") != null ? request.getParameter("valor") : "";
                facturas = cad_facts.split(";_;");
                valortot = valor.replaceAll(",", "").split(";_;");
                for (int i = 0; i < valortot.length; i++) {
                    sumavalortot += Double.parseDouble(valortot[i]);
                }
                gserv = new GestionCarteraService(usuario.getBd());
            } catch (Exception e) {
                System.out.println("Error al buscar datos de encabezado en gestion_cartera.jsp: " + e.toString());
                e.printStackTrace();
            }
%>
<%@ taglib uri="http://jakarta.apache.org/taglibs/input-0.90" prefix="input" %>
<%@page contentType="text/html" pageEncoding="ISO-8859-1"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">
<!-- This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>. -->
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
        <script type='text/javascript' src="<%= BASEURL%>/js/boton.js"></script>
        <script type='text/javascript' src="<%= BASEURL%>/js/prototype.js"></script>
        <link href="<%= BASEURL%>/css/estilostsp.css" rel="stylesheet" type="text/css">
        <!-- Las siguientes librerias CSS y JS son para el manejo de DIVS dinamicos.-->
        <link href="<%=BASEURL%>/css/default.css" rel="stylesheet" type="text/css">
        <link href="<%=BASEURL%>/css/mac_os_x.css" rel="stylesheet" type="text/css">
        <script src="<%=BASEURL%>/js/effects.js" type="text/javascript"></script>
        <script src="<%=BASEURL%>/js/window.js" type="text/javascript"></script>
        <title>Gestion de cartera</title>
        <style type="text/css">
            .curved{
                padding: 5px;
                border: 1px black solid;
                border-radius: 7px;
                -moz-border-radius: 7px;
                -webkit-border-radius: 7px;
                height: 450px;
            }
            .curved1{
                padding: 1px;
                border: 0px black solid;
                font-family: Tahoma, Arial;
                color: #000000;
                font-size: 12px;
                border-radius: 7px;
                -moz-border-radius: 7px;
                -webkit-border-radius: 7px;
                height: 361px;
                overflow: auto;
            }
            .curved2{
                padding: 5px;
                border: 1px black solid;
                border-radius: 7px;
                -moz-border-radius: 7px;
                -webkit-border-radius: 7px;
                height: 450px;
                width: 98%;
                overflow: auto;
            }
        </style>
        <script type="text/javascript">
            function enviarForm(){
                $("imgaceptar").onclick = function x(){alert('Ya hiciste clic en el boton');}
                var accion =  '<%= CONTROLLER%>?estado=Gestion&accion=Cartera&opcion=insert&facts=<%= cad_facts%>&valor=<%= valor%>';
                $("formulario").action = accion;
                $('fecha_prox_gestion').value += ' '+$('hora').value+':'+$('minutos').value+':00';
                $("formulario").submit();
            }

            function verTodo(codneg){
                var url = "<%= CONTROLLER%>?estado=Gestion&accion=Cartera";
                var p =  'opcion=vertodo&documento='+codneg;
                new Ajax.Request(url,{method: 'post', parameters: p,onLoading: loading,onComplete: llenarDiv});
            }

            function verCartas(){
                var win = new Window({
                    id: "vercartas",
                    title: ".: Cartas Cobro Prejuridico :.",
                    width:350,
                    height:250,
                    destroyOnClose: true,
                    url:'<%=BASEURL%>/jsp/fenalco/clientes/VerCartas.jsp?neg='+$("negocio").value+'&saldo='
                        +$("saldo_neto").value+'&mora='+$("int_mora").value+'&cobro='+$("gastos_cobro0").value+'&dias='+$("dias_mora0").value,
                    recenterAuto: false
                });
                win.showCenter(true);
            }

            function loading(){
                $("bitacora").innerHTML = '<span class="letra">Procesando </span><img alt="cargando" src="<%= BASEURL%>/images/cargando.gif" name="imgload">';
            }

            function llenarDiv(response){
                $("bitacora").innerHTML = response.responseText;
            }

            function verObs(codigo){
                var win2 = new Window({/*className: "mac_os_x",*/
                    id: "obscart",
                    title: "Observacion",
                    width:400,
                    height:150,
                    /*showEffectOptions: {duration:1},
                            hideEffectOptions: {duration:1},*/
                    destroyOnClose: true,
                    url:'<%=BASEURL%>/jsp/fenalco/clientes/ver_obs.jsp?codigo='+codigo,
                    recenterAuto: false
                });
                win2.showCenter();
            }

            function cerrTop(){
                top.Windows.close(top.Windows.focusedWindow.getId());
            }

            function soloNumeros(id) {
                var valor = $(id).value;
                valor =  valor.replace(/[^0-9^.]+/gi,"");
                $(id).value = valor;
            }

            function calcular(valor,fila){
                var vcuot = unformatNumber($("saldo_neto").value);
                valor = valor / 100;
                vcuot = vcuot *(valor);
                var gc = Math.round(vcuot);
                $("gastos_cobro"+fila).value = formatNumber(gc);
                vcuot = unformatNumber($("saldo_neto").value);
                var int_mora=unformatNumber($("int_mora").value);
                tc = gc+vcuot+int_mora;
                tc = Math.round(tc);
				
                $("valor_cobro"+fila).value = formatNumber(tc);
                $("saldo0").value = formatNumber(tc);
            }

            function formatNumber(num,prefix){
                prefix = prefix || '';
                num += '';
                var splitStr = num.split('.');
                var splitLeft = splitStr[0];
                var splitRight = splitStr.length > 1 ? '.' + splitStr[1] : '';
                splitRight = splitRight.substring(0,3);
                var regx = /(\d+)(\d{3})/;
                while (regx.test(splitLeft)) {
                    splitLeft = splitLeft.replace(regx, '$1' + ',' + '$2');
                }
                return prefix + splitLeft + splitRight;
            }

            function unformatNumber(num) {
                return num.replace(/([^0-9\.\-])/g,'')*1;
            }

            function enviarEstadoCuenta(negocio,email){
                alert("Iniciando el envio del correo");
                var url = "<%= CONTROLLER%>?estado=Gestion&accion=Cartera";
                var p =  'opcion=enviarestado&negocio='+negocio+'&email='+email;
                new Ajax.Request(url,{method: 'post',parameters: p,onComplete: finish});
            }

            function finish(response){
                alert(response.responseText);
            }

        </script>
    </head>
    <body>
        <div id="capaSuperior" style="position:absolute; width:100%; height:100px; z-index:0; left: 0px; top: 0px;">
            <jsp:include page="/toptsp.jsp?encabezado=Gestion de cartera"/>
        </div>
        <div id="capaCentral" style="position:absolute; width:100%; height:83%; z-index:0; left: 0px; top: 100px; overflow:auto;">
            <div style="width: 100%; padding: 0.1em;">
                <table border="1" style="border-collapse: collapse;" width="99%">
                    <thead>
                        <tr class="subtitulo1">
                            <th width="16%">Factura</th>
                            <th width="10%">Vencimiento</th>
                            <th width="5%">Dias mora</th>
                            <th width="12%">Saldo</th>
                            <th width="12%">Int x mora </th>
                            <th width="12%">Saldo + Int x mora</th>
                            <th width="5%">% cobro</th>
                            <th width="13%">Gastos cobranza</th>
                            <th width="14%">Valor cobro</th>
                        </tr>
                    </thead>
                    <tbody>
                        <%
                                    if (facturas.length == 0) {
                        %>
                        <tr class="filaazul">
                            <%
                                                            try {
                                                                bean = gserv.datosHeader(documento);
                                                                sumafacts = Double.parseDouble(bean.getValor_03());
                                                                sumafacts += Double.parseDouble(bean.getValor_10());
                                                                cad_fct = bean.getValor_05();
                                                                sumafacts_neto += Double.parseDouble(bean.getValor_03());
                                                                sumafacts_int += Double.parseDouble(bean.getValor_10());
                                                            } catch (Exception e) {
                                                                System.out.println("error: " + e.toString());
                                                                e.printStackTrace();
                                                            }
                            %>
                            <td align="center">
                                <img alt="" src="<%=BASEURL%>/images/desc.gif">
                                <input type="text" id="factura0" name="factura0" value="<%= bean.getValor_05()%>" style="border: 0em transparent; background-color: transparent; text-align: center;" readonly="readonly">
                            </td>
                            <td align="center">
                                <input type="text" id="vencimiento0" name="vencimiento0" value="<%= bean.getValor_06()%>" style="border: 0em transparent; background-color: transparent; text-align: center;" readonly="readonly" size="10">
                            </td>
                            <td align="center">
                                <input type="text" id="dias_mora0" name="dias_mora0" value="<%= bean.getValor_07()%>" style="border: 0em transparent; background-color: transparent; text-align: center;" readonly="readonly" size="8">
                            </td>
                            <td align="right">
                                <input type="text" id="saldo_neto" name="saldo_neto" value="<%=sumafacts_neto%>"
                                       style="border: 0em transparent; background-color: transparent; text-align: right;" readonly="readonly" >
                            </td>
                            <td align="right">
                                <input type="text" id="int_mora" name="int_mora" onKeyUp="soloNumeros(this.id);calcular(this.value,'');" size="10" value="<%=sumafacts_int%>"
                                       style="border: 0em transparent; background-color: transparent; text-align: right;" readonly="readonly" ></td>

                            <td align="right"><input type="text" id="saldo" name="saldo" value="" style="border: 0em transparent; background-color: transparent; text-align: right;" readonly="readonly"></td>
                            <td align="right">
                                <input type="text" id="p_cobro" name="p_cobro" value="0" style="text-align: right;" onKeyUp="soloNumeros(this.id);calcular(this.value,'');" size="6">
                            </td>
                            <td align="right">
                                <input type="text" id="gastos_cobro0" name="gastos_cobro0" value="0" style="border: 0em transparent; background-color: transparent; text-align: right;" readonly="readonly">
                            </td>
                            <td align="right">
                                <input type="text" id="valor_cobro0" name="valor_cobro0" value="" style="border: 0em transparent; background-color: transparent; text-align: right;" readonly="readonly">
                                <script type="text/javascript">
                                    $('valor_cobro0').value = formatNumber(<%= sumafacts%>);
                                </script>
                            </td>                            
                        </tr>
                        <%
                                                    } else {
                                                        String fven_mayor = "2170-12-31";
                                                        String fven_norm = "";
                                                        String dias_v = "0";
                                                        for (int i = 0; i < facturas.length; i++) {
                                                            try {
                                                                bean = gserv.datosHeader(facturas[i]);
                                                                if (i == 0) {
                                                                    cad_fct = bean.getValor_05();
                                                                } else {
                                                                    cad_fct += "," + bean.getValor_05();
                                                                }


                                                                sumafacts += Double.parseDouble(bean.getValor_03());
                                                                sumafacts += Double.parseDouble(bean.getValor_10());
                                                                sumafacts_neto += Double.parseDouble(bean.getValor_03());
                                                                sumafacts_int += Double.parseDouble(bean.getValor_10());

                                                                fven_norm = bean.getValor_06();
                                                                if (fven_norm.compareTo(fven_mayor) < 0) {
                                                                    fven_mayor = fven_norm;
                                                                    dias_v = bean.getValor_07();
                                                                }
                                                            } catch (Exception e) {
                                                                System.out.println("error iteracion " + i + " : " + e.toString());
                                                                e.printStackTrace();
                                                            }
                                                        }
                                                        if (sumavalortot == 0) {
                                                            sumavalortot = sumafacts;
                                                        }
                        %>
                        <tr class="filaazul">
                            <%

                            %>
                            <td align="center">
                                <input type="text" id="factura0" name="factura0" value="<%= cad_fct%>" style="border: 0em transparent; background-color: transparent; text-align: center;" readonly="readonly">
                            </td>
                            <td align="center">
                                <input type="text" id="vencimiento0" name="vencimiento0" value="<%= fven_mayor%>" style="border: 0em transparent; background-color: transparent; text-align: center;" readonly="readonly" size="10">
                            </td>
                            <td align="center">
                                <input type="text" id="dias_mora0" name="dias_mora0" value="<%= dias_v%>" style="border: 0em transparent; background-color: transparent; text-align: center;" readonly="readonly" size="8">
                            </td>
                            <td align="right">
                                <input type="text" id="saldo_neto" name="saldo_neto" value="<%=sumafacts_neto%>"
                                       style="border: 0em transparent; background-color: transparent; text-align: right;" readonly="readonly" >
                            </td>
                            <td align="right">
                                <input type="text" id="int_mora" name="int_mora"  onKeyUp="soloNumeros(this.id);calcular(this.value,0);" size="10" value="<%=sumafacts_int%>"
                                       style="border: 0em transparent; background-color: transparent; text-align: right;" readonly="readonly"></td>



                            <td align="right"><input type="text" id="saldo" name="saldo" value="" style="border: 0em transparent; background-color: transparent; text-align: right;" readonly="readonly"></td>
                            <td align="right">
                                <input type="text" id="p_cobro0" name="p_cobro0" value="0" style="text-align: right;" onkeyup="soloNumeros(this.id);calcular(this.value,0);" size="6">
                            </td>
                            <td align="right">
                                <input type="text" id="gastos_cobro0" name="gastos_cobro0" value="" style="border: 0em transparent; background-color: transparent; text-align: right;" readonly="readonly">
                                <script type="text/javascript">
                                    $('gastos_cobro0').value = formatNumber(<%= sumavalortot - sumafacts%>);
                                </script>
                            </td>
                            <td align="right">
                                <input type="text" id="valor_cobro0" name="valor_cobro0" value="" style="border: 0em transparent; background-color: transparent; text-align: right;" readonly="readonly">
                                <script type="text/javascript">
                                    $('valor_cobro0').value = formatNumber(<%= sumavalortot%>);
                                </script>
                            </td>
                        </tr>
                        <%
                                        try {
                                            bean = gserv.datosHeader(facturas[0]);
                                        } catch (Exception e) {
                                            System.out.println("error iteracion 0f : " + e.toString());
                                            e.printStackTrace();
                                        }
                                    }
                        %>
                    </tbody>
                </table>
            </div>
            <div style="height: 0.2em"></div>
            <table border="0" style="border-collapse: collapse;" width="100%">
                <tbody>
                    <tr>
                        <td width="28%" style="padding: 1px;" valign="top">
                            <div id="formgestion" class="curved">
                                <form id="formulario" action="<%= CONTROLLER%>?estado=Gestion&accion=Cartera" method="post">
                                    <table border="0" style="border-collapse: collapse; text-align: left;" width="100%">
                                        <thead>
                                            <tr class="subtitulo1">
                                                <th colspan="3" align="center">Datos de gestion</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <tr class="fila">
                                                <td>Cliente</td>
                                                <td colspan="2"  >
                                                    <input style="color: black;" type="text" id="cliente" name="cliente" value="<% out.print(bean.getValor_02());%>" size="35" readonly="readonly" >
                                                    <input type="hidden" id="codcli" name="codcli" value="<%= bean.getValor_09()%>">                                                    
                                                </td>
                                            </tr>
                                             <tr class="fila">
                                                <td>Negocio</td>
                                                <td>
                                                    <input style="color: black;" type="text" name="negocio" id="negocio" value="<% out.print(bean.getValor_01());%>" readonly="readonly">
                                                </td>
                                                <td>
                                                    <a href="<%= BASEURL%>/jsp/applus/mostrar_archivos.jsp?num_osx=<%= bean.getValor_01()%>&tipito=negocio" target="_blank" style="text-decoration: none; cursor: pointer; color: green;" onclick="">Archivos</a>
                                                </td>
                                            </tr>
                                            <tr class="fila">
                                                <td>Factura</td>
                                                <td  >
                                                    <input style="color: black;" type="text" id="factura" name="factura" value="<% out.print(cad_fct);%>" readonly="readonly">
                                                </td>
                                                <td >
                                                    <a href="<%= BASEURL%>/jsp/fenalco/clientes/BuscarForm.jsp?negocio=<%= bean.getValor_01()%>" target="_blank" style="text-decoration: none; cursor: pointer; color: green;" onclick="">Formularios</a>
                                                </td>
                                            </tr>
                                            <tr class="fila">
                                                <td>Saldo</td>
                                                <td >
                                                    <input style="color: black;" type="text" id="saldo0" name="saldo0" value="<% out.print(sumavalortot);%>" readonly="readonly">
                                                    <script type="text/javascript">
                                                        $('saldo0').value = formatNumber(<%= sumavalortot%>);
                                                    </script>
                                                </td>
                                                <td >
                                                    <span style="text-decoration: none; cursor: pointer; color: green;" onclick="verCartas();">Cartas de Cobro</span><br>
                                                </td>
                                            </tr>
                                            <tr class="fila">
                                                <td>Telefono</td>
                                                <td >
                                                    <input style="color: black;" type="text" id="tel0" name="tel0" value="<% out.print(bean.getValor_11());%>" readonly="readonly">
                                                </td>
                                                <td >
                                                    <span style="text-decoration: none; cursor: pointer; color: green;" onclick="enviarEstadoCuenta('<%=bean.getValor_01()%>','<%=bean.getValor_13()%>');">Env. Estado Cuenta</span>
                                                </td>
                                            </tr>                                            <tr class="fila">
                                                <td>Celular</td>
                                                <td colspan="2">
                                                    <input style="color: black;" type="text" id="cel0" name="cel0" value="<% out.print(bean.getValor_12());%>" readonly="readonly">

                                                </td>
                                            </tr>
                                            <tr class="fila">
                                                <td>Actualizar</td>
                                                <td colspan="2">
                                                    <select id="tipo_dato" name="tipo_dato" style="color: black; width: 9.5em;">
                                                        <option value=''></option>
                                                        <%
                                                                    ArrayList<String> listadato = null;
                                                                    String[] temp2 = null;
                                                                    try {
                                                                        listadato = gserv.listaGestion("TIPO_DATO", "");
                                                                    } catch (Exception e) {
                                                                        System.out.println("Error obtenieindo la lista de tipos de dato : " + e.toString());
                                                                        e.printStackTrace();
                                                                    }
                                                                    if (listadato != null && listadato.size() > 0) {
                                                                        for (int i = 0; i < listadato.size(); i++) {
                                                                            temp2 = (listadato.get(i)).split(";_;");
                                                                            out.print("<option value='" + temp2[0] + "'>" + temp2[1] + "</option>");
                                                                        }
                                                                    }
                                                        %>
                                                    </select>
                                                    <input style="color: black;" type="text" id="dato" name="dato" style="color: black; width: 23.5em;" >
                                                </td>
                                            </tr>

                                            <tr class="fila">
                                                <td>Gestion</td>
                                                <td colspan="2">
                                                    <select id="tipo_gestion" name="tipo_gestion" style="color: black; width: 23.5em;">
                                                        <%
                                                                    ArrayList<String> listagestion = null;
                                                                    String[] temp = null;
                                                                    try {
                                                                        listagestion = gserv.listaGestion("TIPOGEST", "");
                                                                    } catch (Exception e) {
                                                                        System.out.println("Error obtenieindo la lista de gestiones en gestion_cartera.jsp : " + e.toString());
                                                                        e.printStackTrace();
                                                                    }
                                                                    if (listagestion != null && listagestion.size() > 0) {
                                                                        for (int i = 0; i < listagestion.size(); i++) {
                                                                            temp = (listagestion.get(i)).split(";_;");
                                                                            out.print("<option value='" + temp[0] + "'>" + temp[1] + "</option>");
                                                                        }
                                                                    }
                                                        %>
                                                    </select>
                                                </td>
                                            </tr>
                                            <tr class="fila">
                                                <td colspan="3">Observaciones</td>
                                            </tr>
                                            <tr class="fila">
                                                <td colspan="3">
                                                    <textarea name="observacion" rows="6" cols="49"></textarea>
                                                </td>
                                            </tr>
                                            <tr class="fila">
                                                <td>Estado cliente</td>
                                                <td colspan="2">
                                                    <select id="estado_cliente" name="estado_cliente" style="color: black; width: 23.5em;">
                                                        <option value="">...</option>
                                                        <%
                                                                    try {
                                                                        listagestion = gserv.listaGestion("ESTCLIENT", "");
                                                                    } catch (Exception e) {
                                                                        System.out.println("Error obteniendo la lista de gestiones en gestion_cartera.jsp : " + e.toString());
                                                                        e.printStackTrace();
                                                                    }
                                                                    if (listagestion != null && listagestion.size() > 0) {
                                                                        for (int i = 0; i < listagestion.size(); i++) {
                                                                            temp = (listagestion.get(i)).split(";_;");
                                                                            String sel = "";
                                                                            if (bean.getValor_08().equals(temp[0])) {
                                                                                sel = " selected='selected'";
                                                                            }
                                                                            out.print("<option value='" + temp[0] + "'" + sel + ">" + temp[1] + "</option>");
                                                                        }
                                                                    }
                                                        %>
                                                    </select>
                                                </td>
                                            </tr>
                                            <tr class="fila">
                                                <td>Prox. accion</td>
                                                <td colspan="2">
                                                    <select id="prox_accion" name="prox_accion" style="color: black; width: 23.5em;">
                                                        <%
                                                                    try {
                                                                        listagestion = gserv.listaGestion("PRXACCION", "");
                                                                    } catch (Exception e) {
                                                                        System.out.println("Error obtenieindo la lista de gestiones en gestion_cartera.jsp : " + e.toString());
                                                                        e.printStackTrace();
                                                                    }
                                                                    if (listagestion != null && listagestion.size() > 0) {
                                                                        for (int i = 0; i < listagestion.size(); i++) {
                                                                            temp = (listagestion.get(i)).split(";_;");
                                                                            out.print("<option value='" + temp[0] + "'>" + temp[1] + "</option>");
                                                                        }
                                                                    }
                                                        %>
                                                    </select>
                                                </td>
                                            </tr>
                                            <tr class="fila">
                                                <td>Fecha</td>
                                                <td colspan="2">
                                                    <input style="color: black;" type="text" id="fecha_prox_gestion" name="fecha_prox_gestion" value="" readonly="readonly" size="8">
                                                    <img alt="calendario" src="<%=BASEURL%>/images/cal.gif" width="16" height="16" align="absmiddle" style="cursor:pointer " onClick="if(self.gfPop)gfPop.fPopCalendar(document.getElementById('fecha_prox_gestion'));return false;">
                                                    Hora:
                                                    <select id="hora" name="hora">
                                                        <%
                                                                    for (int i = 0; i < 24; i++) {
                                                                        String cad = i < 10 ? "0" + i : "" + i;
                                                        %>
                                                        <option value="<%= cad%>"><%= cad%></option>
                                                        <%
                                                                    }
                                                        %>
                                                    </select>
                                                    Minutos:
                                                    <select id="minutos" name="minutos">
                                                        <%
                                                                    for (int i = 0; i < 60; i++) {
                                                                        String cad = i < 10 ? "0" + i : "" + i;
                                                        %>
                                                        <option value="<%= cad%>"><%= cad%></option>
                                                        <%
                                                                    }
                                                        %>
                                                    </select>
                                                    <!-- <input id="hora" name="hora" type="text" style="color: black;" value="00:00" size="8"> -->
                                                </td>
                                            </tr>
                                        </tbody>
                                    </table>
                                    <div align="center">
                                       
                                    <%if(clvsrv.ispermitted(perfil, "61")){%>
                                        <img alt="aceptar" src="<%= BASEURL%>/images/botones/aceptar.gif" id="imgaceptar" name="imgaceptar" onMouseOver="botonOver(this);" onClick="enviarForm();" onMouseOut="botonOut(this);" style="cursor:pointer">
                                        <%}%>

                                        <img alt="salir" src="<%= BASEURL%>/images/botones/salir.gif" name="imgsalir" onMouseOver="botonOver(this);" onClick="cerrTop();" onMouseOut="botonOut(this);" style="cursor:pointer">
                                    </div>
                                </form>
                            </div>
                        </td>
                        <td width="72%" style="padding: 1px;" valign="top">
                            <div id="divbitacora" class="curved2">
                                <table border="0" style="border-collapse: collapse;" width="100%">
                                    <thead>
                                        <tr class="subtitulo1">
                                            <th align="center">Historial de gestiones</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr class="fila">
                                            <td align="center">
                                                <span style="text-decoration: none; color: green; cursor: pointer;" onclick="verTodo('<% out.print(bean.getValor_01());%>')">Ver gestiones del negocio</span>
                                            </td>
                                        </tr>
                                    </tbody>
                                </table>
                                <div id="bitacora" class="curved1">
                                    <table border="1" style="border-collapse: collapse;" width="100%">
                                        <thead>
                                            <tr class="subtitulo1">
                                                <th>Fecha</th>
                                                <th>Usuario</th>
                                                <th>Factura</th>
                                                <th>Gestion</th>
                                                <th>Prox. accion</th>
                                                <th>Fecha prox. accion</th>
                                                <th>Tipo</th>
                                                <th>Dato</th>
                                                <th>Observacion</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <%
                                                        ArrayList<BeanGeneral> listaobs = null;
                                                        BeanGeneral obs = null;
                                                        try {
                                                            if (facturas.length == 0) {
                                                                listaobs = gserv.observacionesFact(documento);
                                                                if (listaobs != null && listaobs.size() > 0) {
                                                                    for (int i = 0; i < listaobs.size(); i++) {
                                                                        obs = listaobs.get(i);
                                            %>
                                            <tr class="filaazul">
                                                <td><% out.print(obs.getValor_02());%></td>
                                                <td><% out.print(obs.getValor_07());%></td>
                                                <td><% out.print(obs.getValor_06());%></td>
                                                <td><% out.print(gserv.nombreGestion(obs.getValor_03(), "TIPOGEST"));%></td>
                                                <td><% out.print(gserv.nombreGestion(obs.getValor_05(), "PRXACCION"));%></td>
                                                <td><% out.print(obs.getValor_04());%></td>
                                                <td><% out.print(gserv.nombreGestion(obs.getValor_09(), "TIPO_DATO"));%></td>
                                                <td><% out.print(obs.getValor_10());%></td>
                                                <td><span onclick='verObs("<% out.print(obs.getValor_08());%>");' style='text-decoration: none; color: green; cursor: pointer;'>Clic para ver</span></td><!-- 01 -->
                                            </tr>
                                            <%
                                                                                                        }
                                                                                                    } else {
                                            %>
                                            <tr class="filaazul">
                                                <td colspan="9" align="center">No se encontraron registros</td>
                                            </tr>
                                            <%                                                                                                    }
                                                                                                } else {
                                                                                                    for (int k = 0; k < facturas.length; k++) {
                                                                                                        listaobs = gserv.observacionesFact(facturas[k]);
                                                                                                        if (listaobs != null && listaobs.size() > 0) {
                                                                                                            for (int i = 0; i < listaobs.size(); i++) {
                                                                                                                obs = listaobs.get(i);
                                            %>
                                            <tr class="filaazul">
                                                <td><% out.print(obs.getValor_02());%></td>
                                                <td><% out.print(obs.getValor_07());%></td>
                                                <td><% out.print(obs.getValor_06());%></td>
                                                <td><% out.print(gserv.nombreGestion(obs.getValor_03(), "TIPOGEST"));%></td>
                                                <td><% out.print(gserv.nombreGestion(obs.getValor_05(), "PRXACCION"));%></td>
                                                <td><% out.print(obs.getValor_04());%></td>
                                                <td><% out.print(gserv.nombreGestion(obs.getValor_09(), "TIPO_DATO"));%></td>
                                                <td><% out.print(obs.getValor_10());%></td>
                                                <td><span onclick='verObs("<% out.print(obs.getValor_08());%>");' style='text-decoration: none; color: green; cursor: pointer;'>Clic para ver</span></td><!-- 01 -->
                                            </tr>
                                            <%
                                                                                                            }
                                                                                                        } else {
                                            %>
                                            <tr class="filaazul">
                                                <td colspan="9" align="center">No se encontraron registros para <%= facturas[k]%></td>
                                            </tr>
                                            <%
                                                                                                        }
                                                                                                    }
                                                                                                }
                                                                                            } catch (Exception e) {
                                            %>
                                            <tr class="filaazul">
                                                <td colspan="9" align="center">No se encontraron registros</td>
                                            </tr>
                                            <%
                                                            System.out.println("Error al obtener observaciones de facturas en gestion_cartera.jsp: " + e.toString());
                                                            e.printStackTrace();
                                                        }
                                            %>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </td>
                    </tr>
                </tbody>
            </table>
        </div>

        <script type="text/javascript">
                                       $('saldo').value =formatNumber(<%= sumafacts%>);
                                       $('saldo_neto').value = formatNumber(<%= sumafacts_neto%>);
                                       $('int_mora').value = formatNumber(<%= sumafacts_int%>);
        </script>


    </body>
</html>
<iframe width="188" height="166" name="gToday:normal:agenda.js:gfPop:plugins.js" id="gToday:normal:agenda.js:gfPop:plugins.js" src="<%=BASEURL%>/js/Calendario/ipopeng.htm" scrolling="yes" frameborder="0" style="visibility:visible; z-index:999; position:absolute; left:-500px; top:0px;">
</iframe>
