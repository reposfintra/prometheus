<%-- 
    Document   : AsignacionCartera
    Created on : 26/02/2014, 03:01:25 PM
    Author     : jpacosta
--%>

<%@page contentType="text/html" pageEncoding="ISO-8859-1"%>
<%@page session="true"%>
<%@include file="/WEB-INF/InitModel.jsp"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
        <title>Asignacion de Cartera</title>
        <link href="/fintra/css/estilostsp.css" rel="stylesheet" type="text/css">
        
        <link type="text/css" rel="stylesheet" href="./css/jquery/jquery-ui/jquery-ui.css"/>
        <link type="text/css" rel="stylesheet" href="./css/jquery/jquery.jqGrid/ui.jqgrid.css"/>        
        <script type="text/javascript" src="./js/jquery/jquery-ui/jquery.min.js"></script>
        <script type="text/javascript" src="./js/jquery/jquery-ui/jquery.ui.min.js"></script>            
        <script type="text/javascript" src="./js/jquery/jquery.jqGrid/js/grid.locale-es.js"></script>
        <script type="text/javascript" src="./js/jquery/jquery.jqGrid/js/jquery.jqGrid.min.js"></script>
        <style type="text/css">
            button {
                border: rgb(128,128,128) 1px solid;
                border-color: black;
                font-size: 10pt; 
                font-family: Verdana; 
                text-shadow: -1px 0 black, 0 1px black, 1px 0 black, 0 -1px black;
                background-color: #398b56; 
                color:white;
                background-repeat: no-repeat;
                padding-top: 2px; 
            }
            button:hover {
                text-shadow: -1px 0 white, 0 1px black, 1px 0 black, 0 -1px white;
            }
            .ventana{
                display:none;    
                font-family:Arial, Helvetica, sans-serif;
                color:#808080;
                line-height:28px;
                font-size:15px;
                text-align:justify;
            }
        </style>
    </head>
    <body align="center">
        <div id="capaSuperior" style="position:absolute; width:100%; height:100px; z-index:0; left: 0px; top: 0px;">
            <jsp:include page="/toptsp.jsp?encabezado=Asignación Cartera"/>
        </div>

        <div id="capaCentral" style="position:absolute; width:100%; height:100%; z-index:0; left: 0px; top: 100px;">
            <table width='80%' align="center" style="border: green solid 1px;">
                    <tr>
                        <th colspan='4' class="barratitulo">
                            <table width="100%">
                                <tr>
                                    <td width="40%" class="subtitulo1">Filtros de consulta y asignación</td>
                                    <td width="60%"><img src="<%=BASEURL%>/images/titulo.gif" width="32" height="20" align="left"></td>
                                </tr>
                            </table>
                        </th>
                    </tr>
                    <tr class='fila'>
                        <td>Periodo</td>
                        <td><input id="periodos" name="periodos" style="width: 1 em;"/></td>
                    </tr>
                    <tr class='fila'>
                        <td>Unidad de Negocio</td>
                        <td><select id="uNegocios" name="uNegocios" class="grande" onchange="mostrarAgencia()"></select></td>
                    </tr>
                    <tr class='fila' id="fila-agencia">
                        <td>Agencia</td>
                        <td><select id="agencias" name="agencias" class="grande">
                            <option value="undefined">...</option>
                            </select></td>
                    </tr>
                    <tr class='fila'>
                        <td>Rango Mayor</td>
                        <td><select id="vencMayor" name="vencMayor" class="grande"></select></td>
                    </tr>
                    <tr class='fila'>
                        <td>Asignar a</td>
                        <td><select id="asesores" name="asesores" class="grande"></select></td>
                    </tr>
                    <tr class='fila'>
                        <td>Reasignacion   <input type="checkbox" id="reasignacion" name="reasignacion"></td>
                        <td><select id="asesoresR" name="asesoresR" class="grande" style="display:none;"></select></td>
                    </tr>
            </table>

            <br/>
            <div align="center">
                <img id="buscar" name="buscar" style="cursor:hand" src="<%= BASEURL%>/images/botones/buscar.gif" align="absmiddle" onmouseover="botonOver(this);" onmouseout="botonOut(this);">
                <button onclick="resumen('agente')">Resumen de asignaciones</button>
                <img id="aceptar" name="aceptar" style="cursor:hand" src="<%= BASEURL%>/images/botones/aceptar.gif" align="absmiddle" onmouseover="botonOver(this);" onmouseout="botonOut(this);">
                <img style="cursor:hand" src="<%= BASEURL%>/images/botones/salir.gif" align="absmiddle" onmouseover="botonOver(this);" onmouseout="botonOut(this);" onClick="parent.close();">
            </div>
            </br>
            
            <div align="center">
                <table id="negocios"></table>
                <div id="page"></div>
            </div>
            
            <div id="dialogo" class="ventana" title="Resumen"></div>
        </div>
        <script type="text/javascript" src="<%=BASEURL%>/js/asignacionCartera.js"></script>
        <script type="text/javascript">initAsig();</script>
    </body>
</html>
