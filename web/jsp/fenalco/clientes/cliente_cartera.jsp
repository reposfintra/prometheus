<%-- 

    Document   : cliente_cartera

    Created on : 19/07/2010, 03:32:52 PM

    Author     : rhonalf

--%>

<%@page session="true"%>

<%@page import="java.util.*"%>

<%@page import="com.tsp.operation.model.*"%>

<%@page import="com.tsp.operation.model.beans.*"%>
<%@page import="com.tsp.opav.model.services.ClientesVerService"%>

<%@page import="com.tsp.operation.model.services.*"%>

<%@page import="com.tsp.util.*"%>

<%@include file="/WEB-INF/InitModel.jsp"%>



<%response.setHeader("Content-Type", "text/html; charset=windows-1252");
response.setHeader("Pragma", "no-cache"); response.setHeader("Expires","0"); response.setHeader("Cache-Control", "no-cache");
response.setHeader("Cache-Control", "no-store"); response.setHeader("Cache-Control", "must-revalidate"); %>



<%

    ArrayList lista = null;

    ArrayList lista2 = null;

    ArrayList lista3 = null;

    ArrayList<String> listatitulos = null;
    Usuario usuario = (Usuario) session.getAttribute("Usuario");
    ClientesVerService clvsrv = new ClientesVerService(usuario.getBd());
    String perfil = clvsrv.getPerfil(usuario.getLogin());

    try{

      lista = model.proveedorService.buscarConvs();

      lista2 = model.proveedorService.buscarSects();

      lista3=model.proveedorService.listaAfils();

        GestionCarteraService  gserv = new GestionCarteraService(usuario.getBd());

        listatitulos = gserv.titulosValor();

    }

    catch(Exception e){

        System.out.println("error: "+e.toString());

        e.printStackTrace();

    }

%>

<%@ taglib uri="http://jakarta.apache.org/taglibs/input-0.90" prefix="input" %>

<%@page contentType="text/html" pageEncoding="ISO-8859-1"%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"

   "http://www.w3.org/TR/html4/loose.dtd">

<!-- This program is free software: you can redistribute it and/or modify

    it under the terms of the GNU General Public License as published by

    the Free Software Foundation, either version 3 of the License, or

    (at your option) any later version.



    This program is distributed in the hope that it will be useful,

    but WITHOUT ANY WARRANTY; without even the implied warranty of

    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the

    GNU General Public License for more details.



    You should have received a copy of the GNU General Public License

    along with this program.  If not, see <http://www.gnu.org/licenses/>. -->

<html>

    <head>

        <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">



       
        <script type='text/javascript' src="<%= BASEURL %>/js/boton.js"></script>
        <script type='text/javascript' src="<%= BASEURL %>/js/prototype.js"></script>
        <link href="<%= BASEURL %>/css/estilostsp.css" rel="stylesheet" type="text/css">
        <!-- Las siguientes librerias CSS y JS son para el manejo de DIVS dinamicos.-->
        <link href="<%=BASEURL%>/css/default.css" rel="stylesheet" type="text/css">
        <link href="<%=BASEURL%>/css/mac_os_x.css" rel="stylesheet" type="text/css">
        <script src="<%=BASEURL%>/js/effects.js" type="text/javascript"></script>
        <script src="<%=BASEURL%>/js/window.js" type="text/javascript"></script>

        <script type='text/javascript' src="<%= BASEURL %>/js/tablesorter.js"></script>
        <script src='<%=BASEURL%>/js/date-picker.js'></script>

        <link href="<%= BASEURL %>/css/estilostsp.css" rel="stylesheet" type="text/css">

        <title>Modulo de Cobranzas y Gestion de cartera</title><!-- mod -->

        <style type="text/css">

            .curved{

                padding: 5px;

                border: 1px black solid;/*<!-- mod -->*/

                text-align: center;

                background-color: #03A70C;

                border-radius: 7px;

                -moz-border-radius: 7px;

                -webkit-border-radius: 7px;

            }
			
			.tr_ver 
			{	
			/*color: #FFF;
			background-color:#0C3;
			font-family: Tahoma, Arial;             
			font-size: 12px;*/
			}

            .xfila{

                color: #000000;

                font-family: Tahoma, Arial;

                background-color:#FFFFFF;

                font-size: 12px;

            }

            /*//<!-- mod -->*/

            .head{

                background:url(<%=BASEURL%>/images/sort.gif) 7px no-repeat;

                font-family: Verdana, Arial, Helvetica, sans-serif;

                font-size: small;

                color: #FFFFFF;

                background-color:#03a70c;

                font-weight: bold;

                font-size:12px;

            }

            .asc{

                background:url(<%=BASEURL%>/images/asc.gif) 7px no-repeat;

                font-family: Verdana, Arial, Helvetica, sans-serif;

                font-size: small;

                color: #FFFFFF;

                background-color:#03a70c;

                font-weight: bold;

                font-size:12px;

            }

            .desc{

                background:url(<%=BASEURL%>/images/desc.gif) 7px no-repeat;

                font-family: Verdana, Arial, Helvetica, sans-serif;

                font-size: small;

                color: #FFFFFF;

                background-color:#03a70c;

                font-weight: bold;

                font-size:12px;

            }

           /* td{

                white-space: nowrap;				
            }*/
			
			.celda{ padding-left:20px; color:#C00}
			
			

            div.fixedHeaderTable {

                position: relative;

                /*height: 200px;*/

            }

            div.fixedHeaderTable tbody {

                height: 200px;

                vertical-align: top;

                overflow-y: scroll;

                overflow-x: hidden;

            }

            div.fixedHeaderTable thead td, div.fixedHeaderTable thead th {

                vertical-align: top;

                position:relative;

            }



            /* IE7 hacks */

            div.fixedHeaderTable {

                *position: relative;

                *height: 200px;

                *overflow-y: scroll;

                *overflow-x: scroll;

                *padding-right:16px;

            }



            div.fixedHeaderTable thead tr {

                *position: relative;

                _position: absolute;

                *top: expression(this.offsetParent.scrollTop-2);

                *background:none;

                color: white;

                border-color: silver;

            }



            div.fixedHeaderTable tbody {

                *vertical-align: top;

                *height: auto;

                *position:absolute;

                *top:50px;

            }

            /* IE6 hacks */

            div.fixedHeaderTable {

                _width:100%;

                _overflow: auto;

                _overflow-y: scroll;

                _overflow-x: hidden;

            }

            div.fixedHeaderTable thead tr {

                _position: relative

            }
			
	.codigo{color:#004600;}

        </style>

        <script type="text/javascript">

            //<!-- mod -->

            var sorter;

            var sorter2;
			
			function Tr_Format(tr)
			{ 
			//document.getElementById(tr).className="tr_ver"
			}
			
			function Tr_No_Format(tr)
			{
			//	document.getElementById(tr).className="xfila"
			}
			
			
			




            function enviarForm(){

                var url = "<%= CONTROLLER%>?estado=Gestion&accion=Cartera";//document.getElementById("formulario").action;//mod new

                var filtro = $("filtro").value;

                var cadena = $("cadena").value;

                var titulo = $("titulo").value;

                var convenio = $('convenio').value;

                var sector = $('sector').value;

                var subsector = $('subsector').value;

                var tiposal = $('saldo').value;

				var fecha = $('fecha').value;

                var afiliado=$("afiliado").value;

                var p = 'opcion=buscacli&filtro='+filtro+'&cadena='+cadena+'&titulo='+titulo+'&convenio='+convenio+'&sector='+sector+'&subsector='+

				subsector+'&tiposal='+tiposal+'&fecha='+fecha+'&afiliado='+afiliado;

                new Ajax.Request(url,{method: 'post',parameters: p,onLoading: loading,onComplete: llenarDiv});

            }



            function loading(){

                document.getElementById("divtabla").innerHTML ='<div align="center" style="height: 200px;"><br><img alt="cargando" src="<%= BASEURL%>/images/cargando.gif" name="imgload"><span class="letra">Buscando ... por favor espere</span></div>';

            }

          
            function alerta_gestiones(){


                var win = new Window({

                    id: "gestalert",
                    title: ".: Gestiones Pendientes :.",
                    width:1200,
                    height:450,
                    destroyOnClose: true,
                    url:'<%=BASEURL%>/jsp/fenalco/clientes/gestionesPendientes.jsp',

                    recenterAuto: false
                });
                win.showCenter(true);
                win.maximize();
            }
		  

		  
			function cargarHistorial(nit,codcli){
                
                        
                var win = new Window({/*className: "mac_os_x",*/

                            id: "gestcartx",
                            title: ".: Historial de Gestion :.",
                            width:1200,
                            height:450,
                            /*showEffectOptions: {duration:1},num_facts
                            hideEffectOptions: {duration:1},**/
                            destroyOnClose: true,
                            url:'<%=BASEURL%>/jsp/fenalco/clientes/historial_gestion.jsp?documento='+nit+'&codcli='+codcli,
							
                            recenterAuto: false
                        });
                win.showCenter();
               // win.maximize();
            }
			
			

			
			
			
			
			
			

            function llenarDiv(response){

                document.getElementById("divtabla").innerHTML = response.responseText;

                var colind = 0;

                if(document.getElementById("filtro").selectedIndex * 1 == 0){

                    colind = 1;

                }

                if(document.getElementById("filtro").selectedIndex * 1 == 1){

                    colind = 2;

                }

                sorter = new TINY.table.sorter("sorter");

                sorter.head = "head";

                sorter.asc = "asc";

                sorter.desc = "desc";

                sorter.even = "xfila";

                sorter.odd = "xfila";

                sorter.evensel = "xfila";

                sorter.oddsel = "xfila";

                sorter.paginate = false;

                sorter.currentid = "currentpage";

                sorter.limitid = "pagelimit";

                sorter.init("tabla",colind);//<!-- mod -->

            }



            function cargarNegocios(nit,nomcli){


                document.getElementById("clname").value = nomcli;

                var url = "<%= CONTROLLER%>?estado=Gestion&accion=Cartera";//document.getElementById("formulario").action;//mod new

                var titulo = document.getElementById("titulo").value;
				
				var fecha = $('fecha').value;

                var p =  'opcion=buscaneg&nit='+nit+'&titulo='+titulo+'&fecha='+fecha;

                new Ajax.Request(

                    url,

                    {

                        method: 'post',

                        parameters: p,

                        onLoading: cargando,

                        onComplete: cargarDatos

                    }

                );

            }



            function cargando(){

                document.getElementById("negbody").innerHTML ='<span class="letra">Procesando </span><img alt="cargando" src="<%= BASEURL%>/images/cargando.gif" name="imgload">';

            }



            function cargarDatos(response){

                document.getElementById("negbody").innerHTML = response.responseText;

                sorter2 = new TINY.table.sorter("sorter2");

                sorter2.head = "head";

                sorter2.asc = "asc";

                sorter2.desc = "desc";

                sorter2.even = "xfila";

                sorter2.odd = "xfila";

                sorter2.evensel = "xfila";

                sorter2.oddsel = "xfila";

                sorter2.paginate = false;

                sorter2.currentid = "currentpage";

                sorter2.limitid = "pagelimit";

                sorter2.init("tablaneg",6);//new

            }



            function buscarSubs(){

                var sel = document.getElementById("sector").selectedIndex;

                var valor = document.getElementById("sector").options[sel].value;

                var url = "<%= CONTROLLER%>?estado=Proveedor&accion=Ingresar";

                var p =  'opcion=buscarsub&idsect='+valor;

                new Ajax.Request(url,{method: 'post',parameters: p,onComplete: llenarDivx});

            }



            function llenarDivx(response){

                document.getElementById("subsectorx").innerHTML = '<select name="subsector" id="subsector" style="width: 8em;">'+response.responseText+'</select>';

            }



            function activeEnter(e){

                var codigo;

                if (e.keyCode) {

                    codigo = e.keyCode;

                }
				

                else{

                    codigo = e.which;

                }

                if(codigo==9){

                    alert('siii');

                    $('imgaceptar').focus();

                }

            }
			
			
			function lipia_campos()
			{
			
			 document.getElementById('fecha').value="";
			}

        </script>

    </head>

        <body onLoad="lipia_campos(); <%if(clvsrv.ispermitted(perfil, "61")){%> alerta_gestiones(); <%}%> " >

        <div id="capaSuperior" style="position:absolute; width:100%; height:100px; z-index:0; left: 0px; top: 0px;">

            <jsp:include page="/toptsp.jsp?encabezado=Modulo de Cobranzas y Gestion de cartera"/><!-- mod -->

        </div>

        <div id="capaCentral" style="position:absolute; width:100%; height:83%; z-index:0; left: 0px; top: 100px; overflow:auto;">

<table width="100%"  border="0" align="center"  class="tablaInferior">
			<tr>
				<td width="20%" class="subtitulo1">Gestiones Pendientes</td>
                                <td width="70%" class="fila"><img onclick="alerta_gestiones();" src="<%=BASEURL%>/images/titulo.gif" width="32" height="20" align="left"></td>
			</tr>
        </table>

<table id="tablacons" border="0" style="border-collapse: collapse; width: 100%; ">

                    <tbody>

<tr class="fila">

                            <td width="7%" align="center">Tipo de dato</td>

                            <td width="2%" align="center">&nbsp;</td>

                            <td width="10%" align="center">  Dato</td>

                            <td width="1%" align="center">&nbsp;</td>

                            <td width="9%" align="center">

                                Afiliado

                            </td>

                            <td width="9%" align="center">

                                Convenio

                            </td>

                            <td width="9%" align="center">

                                Sector

                            </td>

                            <td width="11%" align="center">

                            Subsector</td>

                            <td width="10%" align="center">

                                Saldo fact.

                            </td>

                            <td width="10%" align="center">Titulo valor</td>

                            <td width="1%" align="center">&nbsp;</td>

                            <td width="8%" align="center">

                            Fven.<span class="Letras"><img src="<%=BASEURL%>/images/cal.gif" alt="" width="16" height="16" align="absmiddle" style="cursor: pointer" onClick="if(self.gfPop)gfPop.fPopCalendar(document.getElementById('fecha'));return false;" HIDEFOCUS></span></td>

                            <td width="22%"><!--                          <img alt="aceptar" src="<%= BASEURL%>/images/botones/aceptar.gif" name="imgaceptar" id="imgaceptar" onMouseOver="botonOver(this);" onClick="enviarForm();" onMouseOut="botonOut(this);" style="cursor:pointer">-->

<!--                          <img alt="salir" src="<%= BASEURL%>/images/botones/salir.gif" name="imgsalir" onMouseOver="botonOver(this);" onClick="parent.close();" onMouseOut="botonOut(this);" style="cursor:pointer">--></td>

                      </tr>
<tr class="fila">
  <td align="center"><select name="filtro" id="filtro">
    <option value="nit" selected>Nit/cedula</option>
    <option value="nomcli">Nombre</option>
    <option value="codcli">Codigo</option>
  </select></td>
  <td align="center">&nbsp;</td>
  <td align="center"><input type="text" name="cadena" id="cadena" value=""></td>
  <td align="center">&nbsp;</td>
    <td align="center"><select id="afiliado" name="afiliado" style="width: 8em;">
    <option selected="selected" value="">...</option>
    <%

                                        if(lista3!=null){

                                            String[] split = null;

                                            for(int i=0;i<lista3.size();i++){

                                                split = ((String)(lista3.get(i))).split(";_;");

                                                out.print("<option value='"+split[0]+"'>"+split[1]+"</option>");

                                            }

                                        }

                                    %>
  </select></td>
  <td align="center"><select id="convenio" name="convenio" style="width: 8em;">
    <option selected="selected" value="">...</option>
    <%

                                        if(lista!=null){

                                            String[] split = null;

                                            for(int i=0;i<lista.size();i++){

                                                split = ((String)(lista.get(i))).split(";_;");

                                                out.print("<option value='"+split[0]+"'>"+split[1]+"</option>");

                                            }

                                        }

                                    %>
  </select></td>
  <td align="center"><select id="sector" name="sector" style="width: 8em;" onChange="buscarSubs();">
    <option selected="selected" value="">...</option>
    <%

                                        if(lista2!=null){

                                            String[] split = null;

                                            for(int i=0;i<lista2.size();i++){

                                                split = ((String)(lista2.get(i))).split(";_;");

                                                out.print("<option value='"+split[0]+"'>"+split[1]+"</option>");

                                            }

                                        }

                                    %>
  </select></td>
  <td align="center">
      <div id="subsectorx" >
          <select name="subsector" id="subsector" style="width: 8em;">
    <option selected="selected" value="">...</option>
   
  </select>
      </div></td>
  <td align="center"><select id="saldo" name="saldo" style="width: 8em;">
    <option value="1" selected="selected">Con saldos vigentes</option>
    <option value="2">Todos</option>
  </select></td>
  <td align="center"><select name="titulo" id="titulo" style="width: 8em;">
    <%

                                        if(listatitulos!=null && listatitulos.size()>0){

                                    %>
    <option value="" selected>...</option>
    <%

                                            String row[] = null;

                                            for(int i=0;i<listatitulos.size();i++){

                                                row = (listatitulos.get(i)).split(";_;");

                                    %>
    <option value="<%= row[0] %>"><%= row[1] %></option>
    <%

                                            }

                                        }

                                        else{

                                    %>
    <option value="" selected>...</option>
    <%

                                        }

                                    %>
  </select></td>
  <td align="center">&nbsp;</td>
  <td align="center"><input type="text" name="fecha" id="fecha"  width="20px" readonly></td>
  <td align="left"><input type="button" value="" style="background:url('images/botones/aceptar.gif') no-repeat; border:none; width:90px; height:21px;" onClick="enviarForm();">
    <input type="button" value="" style="background:url('images/botones/salir.gif') no-repeat; border:none; width:90px; height:21px;" onClick="parent.close();"></td>
</tr>

    </tbody>

                </table>

<!--            </form>-->

<div id="neghead" class="curved">

                <span class="subtitulo1">Datos de clientes</span>

            </div>

            <div style='height:0.2em;'></div>

            <div id="divtabla" class="fixedHeaderTable">

                <div style="height: 200px;">
                
                


                
                
                
                </div>

            </div>

            <div style='height:0.2em;'></div>

            <div id="divng">

                <!--<div id="neghead" class="curved">

                    <span class="subtitulo1">Negocios del cliente</span>

                </div>//mod new

                <br>-->


                <div id="negheader" class="curved">

                    <table border="0" style="border-collapse: collapse;" width="100%">

                        <tr align="left">

                            <td><span class="subtitulo1">Negocios del cliente</span></td>

                            <td>

                                <input type="text" name="clname" id="clname" value="nombre del cliente" readonly="readonly" size="100">

                            </td>

                        </tr>

                    </table>

                </div>

                <!-- <br> -->

                <div id="negbody" style="overflow: auto; height: 220px; padding: 0.1em;"></div>

            </div>

        </div>
	<iframe width=188 height=166 name="gToday:normal:agenda.js:gfPop:plugins.js" id="gToday:normal:agenda.js:gfPop:plugins.js" src="<%=BASEURL%>/js/Calendario/ipopeng.htm" scrolling="no" frameborder="0" style="visibility:visible; z-index:999; position:absolute; left:-500px; top:0px;"> </iframe> 
    </body>

</html>

<script type="text/javascript">

    $('cadena').focus();

</script>
