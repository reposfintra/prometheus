<%--
    Document   : facturaCrearEca
    Created on : 5/03/2009, 11:06:55 PM
    Author     : Alvaro
--%>

<%-- Declaracion de librerias--%>
<%@page contentType="text/html"%>
<%@page session="true"%>
<%@page errorPage="../error/ErrorPage.jsp"%>
<%@page import="java.util.*"%>
<%@page import="com.tsp.util.*"%>
<%@include file="/WEB-INF/InitModel.jsp"%>
<%
	String cerrar=request.getParameter("cerrar");
	if (cerrar!=null && (cerrar.equals("si"))){
	%>
		<script>
		//alert("se va a cerra");
			window.close();
		</script>
	<%	
	}
%>
<html>
<head>
        <title>Seleccion de ofertas para facturar  </title>
        <link href="<%= BASEURL %>/css/estilostsp.css" rel="stylesheet" type="text/css">
	<script language="JavaScript" type="text/javascript" src="<%=BASEURL%>/js/boton.js"></script>

	<script>
	   function send(theForm){
	              theForm.submit();
	   }
	</script>
	<script>

		function borrar()	{
			window.close();
			//window.open('<%= CONTROLLER %>?estado=Negocios&accion=Applus&opcion=borrar2','na','status=yes,scrollbars=yes,width=400,height=150,resizable=no');
		} 

    </script>

</head>

<body onLoad="redimensionar();" onResize="redimensionar();">


<%  String path    = application.getRealPath(model.menuService.getRealUrl(request.getRequestURI()));
    String datos[] = model.menuService.getContenidoMenu(BASEURL,request.getRequestURI(),path);    %>

<div id="capaSuperior" style="position:absolute; width:100%; height:100px; z-index:0; left: 0px; top: 0px;">
<jsp:include page="/WEB-INF/toptsp.jsp?encabezado=Gestion de Avales"/>
</div>

<div id="capaCentral" style="position:absolute; width:100%; height:85%; z-index:0; left: 0px; top: 100px; ">


   <% String  msj           = request.getParameter("msj");
      String aceptarDisable = request.getParameter("aceptarDisable");
   %>

 <center>

   <form action="<%=CONTROLLER%>?estado=Importe&accion=Texto&opcion=procesar" method='post' name='formulario' >


   <table width="450" border="2" align="center">
       <tr>
          <td>
               <table width='100%' align='center' class='tablaInferior'>

                  <tr class="barratitulo">
                    <td colspan='2' >
                       <table cellpadding='0' cellspacing='0' width='100%'>
                         <tr>
                          <td align="left" width='65%' class="subtitulo1">&nbsp;RECEPCIÓN DE AVALES</td>
                          <td align="left" width='*'  ><img src="<%=BASEURL%>/images/titulo.gif" width="32" height="20"  height="20" align="left"><%=datos[0]%></td>
                        </tr>
                       </table>
                    </td>
                 </tr>
				 

                 <tr  class="fila">
                       <td colspan='2' >
                          <table class='tablaInferior' width='100%'>
						  	   <tr  class="fila">
                                   <td align="center" >&nbsp;  </td>
                               </tr>
                               <%if( ! model.importeTextoService.isProcess() ){%><!--20100706-->
						 		<tr  class="fila">
                                   <td align="center" > <a target="_blank" href="<%=BASEURL%>/jsp/applus/importar.jsp?num_osx=aval&tipito=aval"> Cargar nuevo archivo de avales</a></td>
                               </tr>
							   <tr  class="fila">
                                   <td align="center" >&nbsp;  </td>
                               </tr>
							   <tr  class="fila">
                                   <td align="center" > <a target="_blank" href="<%=BASEURL%>/jsp/applus/mostrar_archivos.jsp?num_osx=aval&tipito=aval"> Ver archivos cargados </a></td>
                               </tr>
							   
							   <tr  class="fila">
                                   <td align="center" >&nbsp;  </td>
                               </tr>
                               <tr  class="fila">
                                   <td align="center" > Presione ACEPTAR para procesar los nuevos archivos de avales </td>
                               </tr>
                               <%}else{%><!--20100706-->
                                   <tr  class="fila">
                                        <td align="center" > Proceso ejecutándose. </td>
                                   </tr>
                                <%}%><!--20100706-->
                          </table>
                       </td>
                 </tr>

           </table>
         </td>
      </tr>
   </table>

   <br>



   <%
     if( ! model.importeTextoService.isProcess() ){//20100706
      if ( (aceptarDisable == null) || (aceptarDisable.equals(""))  ) { %>
        <img src="<%=BASEURL%>/images/botones/aceptar.gif"    height="21"  title='Aceptar'  onclick='this.style.visibility="hidden";send(formulario);'   onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" style="cursor:hand">
   <% }
      else { %>
        <img src="<%=BASEURL%>/images/botones/aceptarDisable.gif"    height="21"  style="cursor:hand">
   <% }
     }
   %>

   <img src="<%=BASEURL%>/images/botones/salir.gif"      height="21"  title='Salir'     onClick="borrar();"     onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" style="cursor:hand">


   <!-- MENSAJE -->
   <% if(msj!=null  &&  !msj.equals("") ){%>
        <br><br>
        <table border="2" align="center">
              <tr>
                <td>
                    <table width="100%"  border="1" align="center"  bordercolor="#F7F5F4" bgcolor="#FFFFFF">
                          <tr>
                                <td width="450" align="center" class="mensajes"><%= msj %></td>
                                <td width="29" background="<%=BASEURL%>/images/cuadronaranja.JPG">&nbsp;</td>
                                <td width="58">&nbsp; </td>
                          </tr>
                     </table>
                </td>
              </tr>
        </table>

    <%}%>



      </form >

</center>

</div>


<%=datos[1]%>


</body>
</html>
