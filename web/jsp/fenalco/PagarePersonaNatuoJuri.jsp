<%-- 
    Document   : PagarePersonaNatuoJuri
    Created on : 26/07/2012, 09:44:16 AM
    Author     : Ing. Jose Avila
--%>


<%@page contentType="text/html" pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>

<%@page session    ="true"%>
<%@page errorPage  ="/error/ErrorPage.jsp"%>
<%@include file    ="/WEB-INF/InitModel.jsp"%>


<%@page session="true"%>
<%@page import="java.util.*"%>



<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
        <title>JSP Page</title>
        <%
         String pagarePerNatural = (request.getParameter("pagarePerNatural") != "") ? request.getParameter("pagarePerNatural") : "";
        
        %>

 <script type = "text/javascript">
     /*Funcion que abre el pdf de pagare */
 function abrirpdfPagare(){
    
     window.open("<%= BASEURL%>/pdf/pagaren.pdf" , "Informe" , "width=620,height=400,fullscreen=yes,scrollbars=NO") 
     parent.opener=top;
     opener.close();
     window.close();
        
 }
 </script> 
 
 
  <script type = "text/javascript">
     /*Funcion que abre los pdf de los pagare de persona natural o juridia y FacturaAval */
 function abrirpdfPagare2(){
    
     window.open("<%= BASEURL%>/pdf/pagaren.pdf" , "Informe" , "width=620,height=400,fullscreen=yes,scrollbars=NO")
     window.open("<%= BASEURL%>/pdf/pagarePersonaNatural.pdf" , "Informe2" , "width=620,height=400,fullscreen=yes,scrollbars=NO")
     parent.opener=top;
     opener.close();
     window.close();
    
        
 }
 </script> 
 

    </head>
    
    <body <% if(pagarePerNatural.equals("PersonaNatural")){ %> onload="abrirpdfPagare2();" <% }else if(pagarePerNatural.equals("0")){ %> onload="abrirpdfPagare()" <% } %> >
        

     
    </body>
</html>




