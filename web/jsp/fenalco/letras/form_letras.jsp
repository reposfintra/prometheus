<!--
- Autor : Ing. Roberto Rocha
- Date  : 14 de Nov de 2007
- Copyrigth Notice : Fintravalores S.A. S.A
-->
<%--
-@(#)
--Descripcion : Pagina JSP, que maneja los pagares

--%>

<%@page contentType="text/html"%>
<%@page import="com.tsp.operation.model.beans.*, java.util.*, com.tsp.util.*"%>
<%@taglib uri="/WEB-INF/tlds/tagsAgencia.tld" prefix="ag"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://jakarta.apache.org/taglibs/input-0.90" prefix="input" %>
<%@ include file="/WEB-INF/InitModel.jsp"%>
<%@page session="true"%>
<%@page errorPage="../../../error/ErrorPage.jsp"%>
<%@page import="com.tsp.util.*"%>
<%@page import="java.util.*"%> 
<%@page import="com.tsp.operation.model.*"%>
<%
  String msg = (request.getParameter("msg")!=null)?request.getParameter("msg"):"";
  String path = application.getRealPath(model.menuService.getRealUrl(request.getRequestURI()));
  String datos[] = model.menuService.getContenidoMenu(BASEURL,request.getRequestURI(),path);
  String neg=(request.getParameter ("var")!=null)?request.getParameter ("var"):"";
  model.ciudadService.listarCiudadGeneral();
  Vector a = model.ciudadService.obtCiudades(); 
  Ciudad ciu=new Ciudad();
  
  Usuario usuario = (Usuario) session.getAttribute("Usuario");            
  String loginx=usuario.getLogin();
  
%>
<html>
<head>
<title>Generador de Letras</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link href="../../../css/estilostsp.css" rel="stylesheet" type="text/css"> 
<link href="<%=BASEURL%>/css/estilostsp.css" rel="stylesheet" type="text/css"> 
</head>
<script type='text/javascript' src="<%=BASEURL%>/js/boton.js"></script> 
<script type='text/javascript' src="<%= BASEURL %>/js/validar.js"></script>
<script type='text/javascript' src="<%= BASEURL %>/js/reporte.js"></script>
<link   href="<%= BASEURL %>/css/estilostsp.css" rel='stylesheet'>
<link   href="<%= BASEURL %>/css/EstilosReportes.css" rel='stylesheet'>
<body onResize="redimensionar()" onLoad="redimensionar()">
<div id="capaSuperior" style="position:absolute; width:100%; height:100px; z-index:0; left: 0px; top: 0px;">
	<p>
	    <jsp:include page="/WEB-INF/toptsp.jsp?encabezado=Ingresar Datos de las Letras"/>
    </p>
</div>
<div id="capaCentral" style="position:center; width:100%; height:85%; z-index:0; left: -75px; top: 99px; "> 
<form name="frCliente" id="frCliente" action="<%=CONTROLLER%>?estado=Negocios&accion=Update&op=10" method="post" >   
<p>&nbsp;</p>
<p>&nbsp;</p>
<p>&nbsp;</p>
<table width="655" border="2" align="center">
  <tr>
    <td width="1084" height="150">
		<table width="99%"   align="center"> 
		  <tr>
			<td width="50%" height="22"  class="subtitulo1"><p align="left">Ingresar datos de las Letras</p></td>
			<td width="50%"  class="barratitulo"><img src="<%=BASEURL%>/images/titulo.gif" width="32" height="20" align="left"><%=datos[0]%></td>
		  </tr>
		</table>
		<table width="99%"  align="center" >
		  <tr class="fila">
			<td height="20" colspan="2">Nombre de Coodeudor  </td>
			<td height="20" colspan="6"><input name="NomCood"  type="text" class="textbox" id="NomCliente" size="50" maxlength="50"></td>
			</tr>	
			<tr class="fila">
			
			<td  height="20" colspan="2">No Identificacion </td>
			<td  height="20" colspan="6"><input name="CedCliente" type="text" class="textbox"  id="CedCliente2" size="15"  maxlength="15" onKeyPress="soloDigitos(event,'decNO')">			  </td>
			
			 
			</tr>
			
			<tr class="fila">
			<td  height="20" colspan="2">Codigo de Afiliado </td>
			 <td height="20" colspan="6"><input name="Celular" type="text" class="textbox" id="Celular" size="15"  maxlength="15" >
		      <img  src="<%=BASEURL%>/images/botones/iconos/obligatorio.gif" width="10" height="10"></td>
			</tr>
			
			<tr class="fila">
			<td  height="20" colspan="2">No Aprobacion </td>
			 <td height="20" colspan="6"><input name="aprob" type="text" class="textbox" id="aprob" size="15"  maxlength="15" >
		      <img  src="<%=BASEURL%>/images/botones/iconos/obligatorio.gif" width="10" height="10"></td>
			</tr>
			
			<tr class="fila">
			<td  height="20" colspan="2">No Identificacion (Endoso) </td>
			 <td height="20" colspan="6"><input name="ccaut" type="text" class="textbox" id="ccaut" size="15"  maxlength="15" >
		      <img  src="<%=BASEURL%>/images/botones/iconos/obligatorio.gif" width="10" height="10"></td>
			</tr>
			
			<tr class="fila"  >
				<td  height="20" colspan="2">Domicilio de Pago </td>
				<td width="387" colspan="6" ><select name="c_dir" id="select">
                  <%for(int i=0;i<a.size();i++)
				{
				ciu=(Ciudad)a.elementAt (i);
				if(ciu.getcodciu().equals("BQ"))
				{%>
                  <option value="<%=ciu.getnomciu()%>" selected><%=ciu.getnomciu()%></option>
                  <%}else{%>
                  <option value="<%=ciu.getnomciu()%>" ><%=ciu.getnomciu()%></option>
                  <%}
				ciu=null;
				}%>
                </select>
                  <img  src="<%=BASEURL%>/images/botones/iconos/obligatorio.gif" width="10" height="10"> </td>
			</tr>
			<tr class="fila">
			<td  height="20" colspan="2">Ciudad de Otorgamiento </td>
				<td width="387" colspan="6" ><select name="c_otor" id="select">
                  <%for(int i=0;i<a.size();i++)
				{
				ciu=(Ciudad)a.elementAt (i);
				if(ciu.getcodciu().equals("BQ"))
				{%>
                  <option value="<%=ciu.getnomciu()%>" selected><%=ciu.getnomciu()%></option>
                  <%}else{%>
                  <option value="<%=ciu.getnomciu()%>" ><%=ciu.getnomciu()%></option>
                  <%}
				ciu=null;
				}%>
                </select>
                  <img  src="<%=BASEURL%>/images/botones/iconos/obligatorio.gif" width="10" height="10"> </td>
		  </tr>
				
		  </table>

		</td>
</tr>
</table>
 <p align="center">&nbsp;</p>
 <table width="699" border="0" align="center">
  <tr>
    <td colspan="2" nowrap align="center">
	
		<%	//if (loginx.equals("NAVI") || loginx.equals("TMOLINA")  || loginx.equals("AVENDRIES")){%>
		  <!--new<img  name="Generar" src="<%//=BASEURL%>/images/botones/aceptar.gif"  style = "cursor:hand" align="middle" type="image" id="Generar"  onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" onClick="return generarPagare(frCliente)">-->
		<%//}%>

	
      <img  name="Guardar" src="<%=BASEURL%>/images/botones/aceptar.gif"  style = "cursor:hand" align="middle" type="image" id="Guardar"  onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" onClick="return validar(frCliente)">                  
	  <img src="<%=BASEURL%>/images/botones/restablecer.gif" width="87" name="buss" height="21" align="absmiddle" style="cursor:hand"   onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" onClick="document.location='<%=BASEURL%>/jsp/fenalco/clientes/insert_clientes.jsp'">
	  <img src="<%=BASEURL%>/images/botones/salir.gif"  name="imgsalir" align="absmiddle" style="cursor:hand" onClick="parent.close();" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);"> </td>
    </tr>
</table>
<input name="negocio" type="hidden" value="<%=neg%>">
</form>
<%if(!msg.equals("")){%>
    <table width="407" border="2" align="center">
      <tr>
        <td><table width="100%"   align="center"  >
            <tr>
              <td width="307" align="center" class="mensajes"><%=msg%></td>
              <td width="10" background="<%=BASEURL%>/images/cuadronaranja.JPG"></td>
            </tr>
        </table></td>
      </tr>
    </table>

<%}%>
</div>   
</body>
</html>
<script>
 function validar( forma ){
		if ( forma.Celular.value == '' ){
			alert( 'Debe Ingresar el codigo del afiliado para continuar...' );
			forma.Celular.focus();
			return false;
		}
		if ( forma.aprob.value == '' ){
			alert( 'Debe Ingresar el codigo de aprobacion para continuar...' );
			forma.aprob.focus();
			return false;
		}
		if ( forma.ccaut.value == '' ){
			alert( 'Debe Ingresar la cedula autoriazada...' );
			forma.ccaut.focus();
			return false;
		}
forma.submit();
}

function generarPagare( forma ){
		if ( forma.Celular.value == '' ){
			alert( 'Debe Ingresar el codigo del afiliado para continuar...' );
			forma.Celular.focus();
			return false;
		}
		if ( forma.aprob.value == '' ){
			alert( 'Debe Ingresar el codigo de aprobacion para continuar...' );
			forma.aprob.focus();
			return false;
		}
		if ( forma.ccaut.value == '' ){
			alert( 'Debe Ingresar la cedula autoriazada...' );
			forma.ccaut.focus();
			return false;
		}
		forma.action='<%=CONTROLLER%>?estado=Pdf&accion=Imprimir&opcion=imprimirpdfpagare';				
		forma.submit();
}

</script>