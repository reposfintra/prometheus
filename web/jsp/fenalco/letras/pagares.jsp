<!--
- Autor : Ing. Roberto Rocha
- Date  : 14 de Nov de 2007
- Copyrigth Notice : Fintravalores S.A. S.A
-->
<%--
-@(#)
--Descripcion : Pagina JSP, que maneja los pagares

--%>

<%-- Declaracion de librerias--%>
<%@page contentType="text/html;"%>
<%@page session="true"%>
<%@page import="java.util.*"%>
<%@page import="com.tsp.operation.model.*"%> 
<%@page import="com.tsp.operation.model.beans.*"%>
<%@page import="com.tsp.util.*"%>
<%@page import="com.tsp.util.Util.*"%>
<%@include file="/WEB-INF/InitModel.jsp"%>
<%@page import="com.tsp.operation.model.*"%>

<html>
<head>
<title>GENERADOR DE PAGARES</title>

<link href="../../../css/estilostsp.css" rel="stylesheet" type="text/css"> 
<link href="<%=BASEURL%>/css/estilostsp.css" rel="stylesheet" type="text/css"> 

<style type="text/css">
<!--
.style5 {font-size: 14px}
.style8 {font-size: 9px}
-->
</style>
</head>

<script type='text/javascript' src="<%=BASEURL%>/js/Validacion.js"></script>
<script type='text/javascript' src="<%= BASEURL %>/js/boton.js"></script>
<script type='text/javascript' src="<%= BASEURL %>/js/general.js"></script>
<script src='<%=BASEURL%>/js/date-picker.js'></script>
<script src="<%= BASEURL %>/js/transferencias.js"></script>
<script language="Javascript">
function imprSelec(nombre)
{
  var ficha = document.getElementById(nombre);
  var ventimp = window.open(' ', 'popimpr');
  ventimp.document.write( ficha.innerHTML );
  ventimp.document.close();
  ventimp.print( );
  ventimp.close();
} 
</script> 

<%  Usuario usuario            = (Usuario) session.getAttribute("Usuario");
	/*String Nombre= "FERNADA MARGARITA DIAZGRANADOS BETANCOURTH";
	String cc = "72854785";
	String emp="ENERGAS INTERNACIONAL DE COLOMBIA";
	double valor    = 5824784;
	String  fecha      ="2007-10-02";
	String ap="03-13133-000001 ";
	String nopag="222222";
	RMCantidadEnLetras ejem= new RMCantidadEnLetras();
	  String[] a;
	  a=ejem.getTexto(115824784);
	  String res="";
	  for(int i=0;i<a.length;i++)
	  {
		res=res+(String)a[i];
	  }*/
	String nopag=(request.getParameter ("nopag")!=null)?request.getParameter ("nopag"):"";
	String aprob=(request.getParameter ("aprob")!=null)?request.getParameter ("aprob"):"";
	String n1=(request.getParameter ("n1")!=null)?request.getParameter ("n1"):"";
	String cc1=(request.getParameter ("cc1")!=null)?request.getParameter ("cc1"):"";
	String ccod=(request.getParameter ("ccod")!=null)?request.getParameter ("ccod"):"";
%>
<body   >
<div id="capaSuperior" style="position:absolute; width:100%; height:100px; z-index:0; left: 0px; top: 0px;">
	<jsp:include page="/toptsp.jsp?encabezado=GENERADOR DE PAGARES"/>
</div>
<div id="capaCentral" style="position:absolute; width:100%; height:87%; z-index:0; left: 0px; top: 100px; overflow: scroll;"><img src="<%=BASEURL%>/images/imprime.gif" width="70" height="19" style="cursor:hand" onClick="javascript:imprSelec('seleccion')">
<DIV ID="seleccion" >
<style type="text/css">
<!--
.style5 {font-size: 14px}
-->
</style>  
	<table width="659" height="410" border="1" align="CENTER" bordercolor="#000000" class="style5">

				<tr> 
					<td width="649"><table width="649" cellpadding="0" cellspacing="0">
                      <tr>
						  <td width="80" valign="bottom"><img name="logo" type="image" src="<%=BASEURL%>/images/FEN.bmp" align="top"></td>
					  </tr>
					  <tr >
                        <td width="80" valign="bottom"><p>Pagar&eacute; No </p></td>
                        <td colspan="7" valign="bottom"><p align="center"><strong><%=nopag%> </strong></p></td>
                        <td colspan="3" valign="bottom"><p>Aprobaci&oacute;n </p></td>
                        <td colspan="5" valign="bottom"><p align="center"><strong><%=aprob%> </strong></p></td>
                      </tr>
                      <tr>
                        <td width="80" valign="bottom"><p>&nbsp; </p></td>
                        <td width="150" valign="bottom"><p>&nbsp; </p></td>
                        <td colspan="2" valign="bottom"><p>&nbsp; </p></td>
                        <td colspan="2" valign="bottom"><p>&nbsp; </p></td>
                        <td colspan="2" valign="bottom"><p>&nbsp; </p></td>
                        <td colspan="3" valign="bottom"><p>&nbsp; </p></td>
                        <td colspan="2" valign="bottom"><p>&nbsp; </p></td>
                        <td colspan="3" valign="bottom"><p>&nbsp; </p></td>
                      </tr>
                      <tr>
                        <td width="80" valign="bottom"><p>&nbsp; </p></td>
                        <td width="150" valign="bottom"><p>&nbsp; </p></td>
                        <td colspan="8" valign="bottom"><p align="center"><strong>PAGAR&Eacute; A LA ORDEN </strong></p></td>
                       
                        <td colspan="2" valign="bottom"><p>&nbsp; </p></td>
                        <td colspan="3" valign="bottom"><p>&nbsp; </p></td>
                      </tr>
                      <tr>
                        <td width="80" valign="bottom"><p>&nbsp; </p></td>
                        <td width="150" valign="bottom"><p>&nbsp; </p></td>
                        <td colspan="2" valign="bottom"><p>&nbsp; </p></td>
                        <td colspan="2" valign="bottom"><p>&nbsp; </p></td>
                        <td colspan="2" valign="bottom"><p>&nbsp; </p></td>
                        <td colspan="3" valign="bottom"><p>&nbsp; </p></td>
                        <td colspan="2" valign="bottom"><p>&nbsp; </p></td>
                        <td colspan="3" valign="bottom"><p>&nbsp; </p></td>
                      </tr>
                      <tr>
                        <td colspan="16" valign="bottom"><p align="left"><strong><%=n1%> </strong></p></td>
                        <td width="152" colspan="1" valign="bottom" class="style5"><p>identificado(a) </p></td>
                      </tr>
                      <tr  class="style5">
                        <td colspan="9" valign="bottom"><p>con c&eacute;dula de ciudadan&iacute;a No. <strong><%=Util.PuntoDeMil(cc1)%> </strong></p></td>
                       
                        <td colspan="8" valign="bottom"><p>,<span class="style5"> actuando en representaci&oacute;n, y como codeudor </span></p></td>
                      </tr>
                      <tr  class="style5"  >
                        <td  valign="bottom"><p>de la empresa </p></td>
                        <td colspan="9" ><p align="center">____________________________________________ </p></td>
                        <td colspan="7" valign="bottom"><p>; o como girador, declaro que</p></td>
                      </tr>
                      <tr  class="style5">
                        <td colspan="17" valign="bottom"><p>por virtud del presente t&iacute;tulo valor pagar&eacute; incondicionalmente, a la orden de la FEDERACION NACIONAL </p></td>
                      </tr>
                      <tr  class="style5">
                        <td colspan="17" valign="bottom"><p>DE COMERCIANTES SECCIONAL BOGOT&Aacute; CUNDINAMARCA -FENALCO BOGOT&Aacute; -, </p></td>
                      </tr>
                      <tr  class="style5">
                        <td colspan="7" valign="bottom"><p>en Bogot&aacute;</p></td>
                        <td colspan="10" valign="bottom"><p>y en la fecha de vencimiento indicada, la suma de</p></td>
                      </tr>
                      <tr   class="style5">
                        <td colspan="17" valign="bottom"><p align="left">___________________________________________________________________________________________ </p></td>
                      </tr>
 
                      <tr  class="style5">
                        <td colspan="17" valign="bottom"><div align="justify">pesos moneda legal Colombiana ($ 
   __________________ ) pagaderos en una �nica 
   
cuota por el valor total indicado, el d�a ________
     
 de _______________________
     
 de ___________.
     
 
  Expresamente declaro excusada la presentaci�n para el pago, el aviso de rechazo y protesto. 
   
Autorizo al tenedor para dar por terminado el plazo de la obligaci�n y cobrarla judicial o 
   
extrajudicialmente, inclusive por la devoluci�n de uno de los t�tulos valores otorgados y/o girados 
   
a un afiliado a Fenalco. El presente t�tulo valor generar� intereses al m�ximo autorizado por la ley 
   
comercial; Los gastos de cobranza, inclusive extrajudicial, ser�n de 20% del total adeudado al momento del pago. 
   
Los honorarios aceptados son tambi�n del 20% del total adeudado al momento del pago. Estos tambi�n ser�n a mi cargo. Autorizo a Fenalco para que la informaci�n 
   
suministrada en el presente documento sea verificada con terceras personas incluyendo las 
   
entidades financieras y las bases de datos, igualmente para que la misma sea usada y puesta 
   
en circulaci�n con fines estrictamente comerciales. As� mismo autorizo a FENALCO para que por 
   
medio de informaci�n y/o registros sistematizados, consulte, reporte, procese, y mantenga 
   
actualizados los datos referentes a mi comportamiento crediticio en atenci�n a la obligaci�n 
   
contenida en el presente t�tulo valor, lo cual incluye la facultad, en caso de incumplimiento, 
   
de reportar mi documento de identificaci�n en cualquier base de datos de deudores morosos o 
   
centrales de riesgos. El presente pagar� se suscribe en blanco, pero el acreedor esta autorizado 
   
de conformidad con el art�culo 622 del C�digo de Comercio para llenar sin previo aviso y en 
   
cualquier tiempo, los espacios que figuran en blanco en el presente t�tulo valor, es decir, los 
   
relativos a: nombre, empresa, cuant�a, fechas de creaci�n y vencimiento de acuerdo con las 
   
siguientes instrucciones: 1� La cuant�a ser� igual al monto de todas las sumas que el suscrito se 
   
encuentre debiendo a Fenalco Bogot�, y especialmente por concepto de t�tulos valores que por 
   
cualquier raz�n no fueren pagados y le fueren remitidos por los comerciantes afiliados al servicio 
   
Fenalcheque, mismos sobre los que renuncio a su t�rmino de prescripci�n y/o caducidad, 
   
acogiendo el t�rmino de prescripci�n del presente pagar�. 2� La fecha de creaci�n corresponder� 
   
a la de suscripci�n de este pagar�. 3� La fecha de vencimiento ser� la del d�a en que sea llenado. 
   
El pagar� as� llenado ser� exigible inmediatamente y prestar� m�rito ejecutivo sin m�s requisitos 
   
ni requerimientos.Bogot&aacute;, Fecha ________________</div></td>
                      </tr>
                     
                      <tr  class="style5">
                        <td colspan="5" valign="bottom" >&nbsp;</td>
                        <td colspan="4" valign="bottom"><p align="justify">&nbsp; </p></td>
                        <td width="154" valign="bottom"><p align="justify">&nbsp; </p></td>
                        <td colspan="5" valign="bottom"><p align="justify">&nbsp; </p></td>
                        <td colspan="2" valign="bottom"><p align="justify">&nbsp; </p></td>
                      </tr>
                      <tr  class="style5">
                        <td colspan="3" valign="bottom">&nbsp;</td>
                        <td colspan="2" valign="bottom">&nbsp;</td>
                        <td colspan="2" valign="bottom">&nbsp;</td>
                        <td colspan="2" valign="bottom">&nbsp;</td>
                        <td width="154" valign="bottom">&nbsp;</td>
                        <td colspan="2" valign="bottom">&nbsp;</td>
                        <td colspan="2" valign="bottom">&nbsp;</td>
                        <td width="95" valign="bottom">&nbsp;</td>
                        <td colspan="2" valign="bottom">&nbsp;</td>
                      </tr>
					   
					  <tr  class="style5">
                        <td colspan="3" valign="bottom"><p>&nbsp; </p></td>
                        <td colspan="2" valign="bottom"><p>&nbsp; </p></td>
                        <td colspan="2" valign="bottom"><p>&nbsp; </p></td>
                        <td colspan="2" valign="bottom"><p>&nbsp; </p></td>
                        <td width="154" valign="bottom"><p>&nbsp; </p></td>
                        <td colspan="2" valign="bottom"><p>&nbsp; </p></td>
                        <td colspan="2" valign="bottom"><p>&nbsp; </p></td>
                        <td width="95" valign="bottom"><p>&nbsp; </p></td>
                        <td colspan="2" valign="bottom"><p>&nbsp; </p></td>
                      </tr>
                      <tr>
                        <td colspan="10" valign="bottom"><p>Representante legal y codeudor o girador </p></td>
                        <td colspan="5" valign="bottom"><p align="center">&nbsp; </p></td>
                        <td colspan="2" valign="bottom"><p align="center">Huella </p></td>
                      </tr>
                      <tr>
                        <td colspan="3" valign="bottom"><p>CC. <strong><%=Util.PuntoDeMil(cc1)%> </strong></p></td>
                        <td colspan="4" valign="bottom"><p align="center"><strong> </strong></p></td>
                        <td colspan="2" valign="bottom"><p>&nbsp; </p></td>
                        <td width="154" valign="bottom"><p>&nbsp; </p></td>
                        <td colspan="2" valign="bottom"><p>&nbsp; </p></td>
                        <td colspan="2" valign="bottom"><p>&nbsp; </p></td>
                        <td width="95" valign="bottom"><p>&nbsp; </p></td>
                        <td colspan="2" valign="bottom"><p>&nbsp; </p></td>
                      </tr>
                      <tr>
                        <td colspan="3" valign="bottom"><p>&nbsp; </p></td>
                        <td colspan="2" valign="bottom"><p>&nbsp; </p></td>
                        <td colspan="2" valign="bottom"><p>&nbsp; </p></td>
                        <td colspan="2" valign="bottom"><p>&nbsp; </p></td>
                        <td width="154" valign="bottom"><p>&nbsp; </p></td>
                        <td colspan="2" valign="bottom"><p>&nbsp; </p></td>
                        <td colspan="2" valign="bottom"><p>&nbsp; </p></td>
                        <td width="95" valign="bottom"><p>&nbsp; </p></td>
                        <td colspan="2" valign="bottom"><p>&nbsp; </p></td>
                      </tr>
					  <tr  class="style5">
                        <td colspan="3" valign="bottom"><p>&nbsp; </p></td>
                        <td colspan="2" valign="bottom"><p>&nbsp; </p></td>
                        <td colspan="2" valign="bottom"><p>&nbsp; </p></td>
                        <td colspan="2" valign="bottom"><p>&nbsp; </p></td>
                        <td width="154" valign="bottom"><p>&nbsp; </p></td>
                        <td colspan="2" valign="bottom"><p>&nbsp; </p></td>
                        <td colspan="2" valign="bottom"><p>&nbsp; </p></td>
                        <td width="95" valign="bottom"><p>&nbsp; </p></td>
                        <td colspan="2" valign="bottom"><p>&nbsp; </p></td>
                      </tr>
                      <tr>
                        <td colspan="7" valign="bottom"><p>Codeudor Solidario </p></td>
                        <td colspan="8" valign="bottom"><p align="center">&nbsp; </p></td>
                        <td colspan="2" valign="bottom"><p align="center">Huella </p></td>
                      </tr>
                      <tr>
                        <td colspan="3" valign="bottom"><p>CC.  <strong><%=Util.PuntoDeMil(ccod)%> </strong></p></p></td>
                        <td colspan="4" valign="bottom"><p align="center">&nbsp; </p></td>
                        <td colspan="2" valign="bottom"><p>&nbsp; </p></td>
                        <td width="154" valign="bottom"><p>&nbsp; </p></td>
                        <td colspan="2" valign="bottom"><p>&nbsp; </p></td>
                        <td colspan="2" valign="bottom"><p>&nbsp; </p></td>
                        <td width="95" valign="bottom"><p>&nbsp; </p></td>
                        <td colspan="2" valign="bottom"><p>&nbsp; </p></td>
                      </tr>
                      <tr>
                        <td colspan="3" valign="bottom"><p>&nbsp; </p></td>
                        <td colspan="2" valign="bottom"><p>&nbsp; </p></td>
                        <td colspan="2" valign="bottom"><p>&nbsp; </p></td>
                        <td colspan="2" valign="bottom"><p>&nbsp; </p></td>
                        <td width="154" valign="bottom"><p>&nbsp; </p></td>
                        <td colspan="2" valign="bottom"><p>&nbsp; </p></td>
                        <td colspan="2" valign="bottom"><p>&nbsp; </p></td>
                        <td width="95" valign="bottom"><p>&nbsp; </p></td>
                        <td colspan="2" valign="bottom"><p>&nbsp; </p></td>
                      </tr>
                    </table>
				  </td>
				</tr>
    </table>	
</div>
</div>
<iframe width="188" height="166" name="gToday:normal:agenda.js:gfPop:plugins.js" id="gToday:normal:agenda.js:gfPop:plugins_24.js" src="<%=BASEURL%>/js/Calendario/ipopeng.htm" scrolling="yes" frameborder="0" style="visibility:visible; z-index:999; position:absolute; left:-500px; top:0px;">
</iframe>
</body>
</html>


