<!--
- Autor : Ing. Roberto Rocha
- Date  : 14 de Nov de 2007
- Copyrigth Notice : Fintravalores S.A. S.A
-->
<%--
-@(#)
--Descripcion : Pagina JSP, que maneja los pagares

--%>

<%-- Declaracion de librerias--%>
<%@page contentType="text/html;"%>
<%@page session="true"%>
<%@page import="java.util.*"%>
<%@page import="com.tsp.operation.model.*"%> 
<%@page import="com.tsp.operation.model.beans.*"%>
<%@page import="com.tsp.util.*"%>
<%@page import="com.tsp.util.Util.*"%>
<%@include file="/WEB-INF/InitModel.jsp"%>
<%@page import="com.tsp.operation.model.*"%>

<html>
<head>
<title>GENERADOR DE ENDOSOS LETRAS</title>

<link href="../../../css/estilostsp.css" rel="stylesheet" type="text/css"> 
<link href="<%=BASEURL%>/css/estilostsp.css" rel="stylesheet" type="text/css"> 

</head>

<script type='text/javascript' src="<%=BASEURL%>/js/Validacion.js"></script>
<script type='text/javascript' src="<%= BASEURL %>/js/boton.js"></script>
<script type='text/javascript' src="<%= BASEURL %>/js/general.js"></script>
<script src='<%=BASEURL%>/js/date-picker.js'></script>
<script language="Javascript">
function imprSelec(nombre)
{
  var ficha = document.getElementById(nombre);
  var ventimp = window.open(' ', 'popimpr');
  ventimp.document.write( ficha.innerHTML );
  ventimp.document.close();
  ventimp.print( );
  ventimp.close();
} 
</script> 
<script src="<%= BASEURL %>/js/transferencias.js"></script>

<%  Usuario usuario            = (Usuario) session.getAttribute("Usuario");
	String no=(request.getParameter ("no")!=null)?request.getParameter ("no"):"";
	String cc1=(request.getParameter ("cc1")!=null)?request.getParameter ("cc1"):"";
	String n1=(request.getParameter ("n1")!=null)?request.getParameter ("n1"):"";
	String cc2=(request.getParameter ("cc2")!=null)?request.getParameter ("cc2"):"";
%>
<body >
<div id="capaSuperior" style="position:absolute; width:100%; height:100px; z-index:0; left: 0px; top: 0px;">
  <jsp:include page="/toptsp.jsp?encabezado=GENERADOR DE ENDOSOS LETRAS"/>
</div>
<div id="capaCentral" style="position:absolute; width:100%; height:87%; z-index:0; left: 0px; top: 100px; overflow: scroll;">
<img src="<%=BASEURL%>/images/imprime.gif" width="70" height="19" style="cursor:hand" onClick="javascript:imprSelec('seleccion')">
<DIV ID="seleccion" >  
<%for(int i=0;i<Integer.valueOf(no).intValue();i++){%>
<table >
	<tr>
		<td>
				<table width="857" height="504"  border="0" align="left" bordercolor="#000000">
				<tr> 
				  <td width="820" height="500"><p style="font-size: 14px">&nbsp;</p>
				    <p style="font-size: 14px">&nbsp;</p>
				    <p style="font-size: 18px"><em>“Endosamos en propiedad con responsabilidad el presente titulo valor a favor de la Fiduciaria Corficolombiana S.A Fideicomiso Fintravalores nit 800.256.768-6. En consecuencia el pago del mismo debe hacerse directamente a favor de la Fiduciaria Corficolombiana S.A Fideicomiso Fintravalores o a su orden en la fecha de su vencimiento en la cuenta corriente No. 800-57617-5 del Banco de Occidente. </em></p>
				    <p style="font-size: 18px">&nbsp;</p>
				    <table width="819" cellpadding="0" cellspacing="0">
                      <tr>
                        <td width="11" valign="bottom"><p><em>Firma </em></p></td>
                        <td width="499" valign="bottom"><p><em>__________________________________ </em></p></td>
                      </tr>
                      <tr>
                        <td width="49" valign="bottom"><p>C.C </p></td>
                        <td width="471" valign="bottom"><p><strong><em><%=Util.PuntoDeMil(cc1)%> </em></strong></p></td>
                      </tr>
                      <tr>
                        <td width="49" valign="bottom"><p>&nbsp; </p></td>
                        <td width="471" valign="bottom"><p><em>En nombre y representaci&oacute;n de <%=n1%> </em></p></td>
                      </tr>
                      <tr>
                        <td width="49" valign="bottom"><p>&nbsp; </p></td>
                        <td width="471" valign="bottom"><p>&nbsp; </p></td>
                      </tr>
                      <tr>
                        <td width="49" valign="bottom"><p>&nbsp; </p></td>
                        <td width="471" valign="bottom"><p><em>&quot;En se&ntilde;al de aceptaci&oacute;n de lo anterior: </em></p></td>
                      </tr>
                      <tr>
                        <td width="49" valign="bottom"><p>&nbsp; </p></td>
                        <td width="471" valign="bottom"><p><strong>&nbsp; </strong></p></td>
                      </tr>
                      <tr>
                        <td width="49" valign="bottom"><p>&nbsp; </p></td>
                        <td width="471" valign="bottom"><p><em>El deudor, </em></p></td>
                      </tr>
                      <tr>
                        <td width="49" valign="bottom"><p>&nbsp; </p></td>
                        <td width="471" valign="bottom"><p>&nbsp; </p></td>
                      </tr>
					  <tr>
                        <td width="49" valign="bottom"><p>&nbsp; </p></td>
                        <td width="471" valign="bottom"><p>&nbsp; </p></td>
                      </tr>
					  <tr>
                        <td width="49" valign="bottom"><p>&nbsp; </p></td>
                        <td width="471" valign="bottom"><p>&nbsp; </p></td>
                      </tr>
					  <tr>
                        <td width="49" valign="bottom"><p>&nbsp;</p></td>
                        <td width="471" valign="bottom"><p>&nbsp; </p></td>
                      </tr> 
					  <tr>
                        <td width="49" valign="bottom"><p>&nbsp;</p></td>
                        <td width="471" valign="bottom"><p>&nbsp; </p></td>
                      </tr> 
					  <tr>
                        <td width="49" valign="bottom"><p><em>Firma </em></p></td>
                        <td width="471" valign="bottom"><p><em>__________________________________</em> </p></td>
                      </tr> 
					  <tr>
                        <td width="49" valign="bottom"><p>C.C </p></td>
                        <td width="471" valign="bottom"><p><strong><%=Util.PuntoDeMil(cc2)%></strong> </p></td>
                      </tr> 
                      <tr>
                        <td width="49" valign="bottom"><p>&nbsp;</p></td>
                        <td width="471" valign="bottom"><p>&nbsp;</p></td>
                      </tr>
                      <tr>
                        <td width="49" valign="bottom"><p>&nbsp;</p></td>
                        <td width="471" valign="bottom"><p><strong> </strong></p></td>
                      </tr>
                  </table>				    
			      <p>&nbsp;</p>
			      <p>&nbsp;</p></td>
				</tr>
		  </table>
		</td>
	</tr>
	<% if( i%2 != 0){%>
	<tr>
		<td><p>&nbsp;</p></td>
	</tr>
	
	<%}%>
</table>
<%}%>
 </div>
</div>
<iframe width="188" height="166" name="gToday:normal:agenda.js:gfPop:plugins.js" id="gToday:normal:agenda.js:gfPop:plugins_24.js" src="<%=BASEURL%>/js/Calendario/ipopeng.htm" scrolling="yes" frameborder="0" style="visibility:visible; z-index:999; position:absolute; left:-500px; top:0px;">
</iframe>
</body>
</html>


