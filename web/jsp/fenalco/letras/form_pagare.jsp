<!--
- Autor : Ing. Roberto Rocha
- Date  : 14 de Nov de 2007
- Copyrigth Notice : Fintravalores S.A. S.A
-->
<%--
-@(#)
--Descripcion : Pagina JSP, que maneja los pagares

--%>

<%@page contentType="text/html"%>
<%@page import="com.tsp.operation.model.beans.*, java.util.*, com.tsp.util.*"%>
<%@taglib uri="/WEB-INF/tlds/tagsAgencia.tld" prefix="ag"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://jakarta.apache.org/taglibs/input-0.90" prefix="input" %>
<%@ include file="/WEB-INF/InitModel.jsp"%>
<%@page session="true"%>
<%@page errorPage="../../../error/ErrorPage.jsp"%>
<%@page import="com.tsp.util.*"%>
<%@page import="java.util.*"%> 
<%@page import="com.tsp.operation.model.*"%>
<%
  String msg = (request.getParameter("msg")!=null)?request.getParameter("msg"):"";
  String path = application.getRealPath(model.menuService.getRealUrl(request.getRequestURI()));
  String datos[] = model.menuService.getContenidoMenu(BASEURL,request.getRequestURI(),path);
  String neg=(request.getParameter ("var")!=null)?request.getParameter ("var"):"";
  model.ciudadService.listarCiudadGeneral();
  Vector a = model.ciudadService.obtCiudades(); 
  Ciudad ciu=new Ciudad();
 
  Usuario usuario = (Usuario) session.getAttribute("Usuario");            
  String loginx=usuario.getLogin();
  String datos_pagare=model.Negociossvc.getDatos_pagare(neg);
  String aval=datos_pagare.split("-_-")[0];
  String idendoso=datos_pagare.split("-_-")[1];
  String cp_pagare=datos_pagare.split("-_-")[2];
  String co_pagare=datos_pagare.split("-_-")[3];
  String d_afiliado=datos_pagare.split("-_-")[4];
  String a_favor=datos_pagare.split("-_-")[5];
  String id_codeudor=datos_pagare.split("-_-")[6];
  String estado_neg=datos_pagare.split("-_-")[7];
  if(cp_pagare.equals(""))
  { cp_pagare="BQ";
  }
  if(co_pagare.equals(""))
  { co_pagare="BQ";
  }
  if(d_afiliado.equals(""))
  { d_afiliado="BQ";
  }
  %>
<html>
<head>
<title>Generador de Pagares</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link href="../../../css/estilostsp.css" rel="stylesheet" type="text/css"> 
<link href="<%=BASEURL%>/css/estilostsp.css" rel="stylesheet" type="text/css"> 
</head>
<script type='text/javascript' src="<%=BASEURL%>/js/boton.js"></script> 
<script type='text/javascript' src="<%= BASEURL %>/js/validar.js"></script>
<script type='text/javascript' src="<%= BASEURL %>/js/reporte.js"></script>
<link   href="<%= BASEURL %>/css/estilostsp.css" rel='stylesheet'>
<link   href="<%= BASEURL %>/css/EstilosReportes.css" rel='stylesheet'>
<body onResize="redimensionar()" onLoad="redimensionar();">
<div id="capaSuperior" style="position:absolute; width:100%; height:100px; z-index:0; left: 0px; top: 0px;"> 
	<p>
	    <jsp:include page="/WEB-INF/toptsp.jsp?encabezado=Ingresar Datos del Pagar�"/>
    </p>
</div>
<div id="capaCentral" style="position:center; width:100%; height:85%; z-index:0; left: -75px; top: 99px; "> 
<form name="frCliente" id="frCliente"  action="<%=CONTROLLER%>?estado=Negocios&accion=Update&op=10" method="post" >
<script>   var vec=new Array();
           var vec2=new Array();
           var i;
</script>
<p>&nbsp;</p>
<p>&nbsp;</p>
<p>&nbsp;</p>
<%  ArrayList <String []> code=model.Negociossvc.getDatos("CODEUDOR", neg);
%>
<table width="655" border="2" align="center">
  <tr>
    <td width="1084" height="150">
		<table width="99%"   align="center"> 
		  <tr>
			<td width="50%" height="22"  class="subtitulo1"><p align="left">Ingresar datos del Pagar�</p></td>
			<td width="50%"  class="barratitulo"><img src="<%=BASEURL%>/images/titulo.gif" width="32" height="20" align="left"><%=datos[0]%></td>
		  </tr>
		</table>
		<table width="99%"  align="center" >
		  <tr class="fila">
			<td height="20" colspan="2">Nombre de Coodeudor  </td>
                     <td height="20" colspan="6">
                         <select id="NomCood" name="NomCood" style="width:12em;">
                                            <option value="_____________-_-_____________-_-_____________-_-_____________">...</option>
                                            <%
                                                         for (int i = 0; i < code.size(); i++) {
                                                             String [] cod=(code.get(i)[1]).split("_-_-");
                                                          %>
                                                          <option value="<%=code.get(i)[1]%>" <%=(cod[0].equals(id_codeudor)) ? " " :"selected"%> ><%=code.get(i)[0]%></option>
                                            <%  }%>
                          </select>
                     </td>
			</tr>	
			<!--<tr class="fila">
			
			<td  height="20" colspan="2">No Identificacion </td>
			<td  height="20" colspan="6"><input name="CedCliente" type="text" class="textbox"  id="CedCliente2" size="15"  maxlength="15" onKeyPress="soloDigitos(event,'decNO')">			  </td>
			
			 
			</tr>-->
			
			<!--<tr class="fila">
			<td  height="20" colspan="2">Codigo de Afiliado </td>
			 <td height="20" colspan="6"><input name="Celular" type="text" class="textbox" id="Celular" size="15"  maxlength="15" >
		      <img  src="<%//=BASEURL%>/images/botones/iconos/obligatorio.gif" width="10" height="10"></td>
			</tr>-->
			
			<tr class="fila">
			<td  height="20" colspan="2">No Aprobacion </td>
                        <td height="20" colspan="6"><input name="aprob" type="text" class="textbox" id="aprob" size="15"  maxlength="15"  value="<%=(!aval.equals("0"))?aval:""%>" >
		      <img  src="<%=BASEURL%>/images/botones/iconos/obligatorio.gif" width="10" height="10"></td>
			</tr>		
			
			<tr class="fila"  >
				<td  height="20" colspan="2">Ciudad de Pago del Pagar� </td>
				<td width="387" colspan="6" ><select name="c_dir" id="select">
                  <%for(int i=0;i<a.size();i++)
				{
				ciu=(Ciudad)a.elementAt (i);
				if(ciu.getcodciu().equals(cp_pagare)||ciu.getnomciu().equals(cp_pagare))
				{%>   <option value="<%=ciu.getnomciu()%>" selected><%=ciu.getnomciu()%></option>
                            <%}else{%>
                                  <option value="<%=ciu.getnomciu()%>" ><%=ciu.getnomciu()%></option>
                            <%}
				ciu=null;
				}%>
                </select>
                  <img  src="<%=BASEURL%>/images/botones/iconos/obligatorio.gif" width="10" height="10"> </td>
			</tr>
			<tr class="fila">
			<td  height="20" colspan="2">Ciudad de Otorgamiento del Pagar� </td>
				<td width="387" colspan="6" ><select name="c_otor" id="select">
                  <%for(int i=0;i<a.size();i++)
				{
				ciu=(Ciudad)a.elementAt (i);
				if(ciu.getcodciu().equals(co_pagare)||ciu.getnomciu().equals(co_pagare))
				{%>   <option value="<%=ciu.getnomciu()%>" selected><%=ciu.getnomciu()%></option>
                            <%}else{%>
                                  <option value="<%=ciu.getnomciu()%>" ><%=ciu.getnomciu()%></option>
                            <%}
				ciu=null;
				}%>
                </select>
                  <img  src="<%=BASEURL%>/images/botones/iconos/obligatorio.gif" width="10" height="10"> </td>
		  </tr>
		  
		  <tr class="fila">
			<td  height="20" colspan="2">Domicilio del Afiliado </td>
				<td width="387" colspan="6" ><select name="domi2" id="select">
                  <%for(int i=0;i<a.size();i++)
				{
				ciu=(Ciudad)a.elementAt (i);
				if(ciu.getcodciu().equals(d_afiliado)||ciu.getnomciu().equals(d_afiliado))
				{%> <option value="<%=ciu.getnomciu()%>" selected><%=ciu.getnomciu()%></option>
                            <%}else{%>
                                <option value="<%=ciu.getnomciu()%>" ><%=ciu.getnomciu()%></option>
                            <%}
				ciu=null;
				}%>
                </select>
                  <img  src="<%=BASEURL%>/images/botones/iconos/obligatorio.gif" width="10" height="10"> </td>
		  </tr>
		<!--
		  <tr class="fila">
			<td  height="20" colspan="2">Domicilio de Coodeudor </td>
				<td width="387" colspan="6" ><select name="domi_cood" id="select">
                  <%for(int i=0;i<a.size();i++)
				{
				ciu=(Ciudad)a.elementAt (i);
				if(ciu.getcodciu().equals("BQ"))
				{%>
                  <option value="<%=ciu.getnomciu()%>" selected><%=ciu.getnomciu()%></option>
                  <%}else{%>
                  <option value="<%=ciu.getnomciu()%>" ><%=ciu.getnomciu()%></option>
                  <%}
				ciu=null;
				}%>
                </select>
				<img  src="<%=BASEURL%>/images/botones/iconos/obligatorio.gif" width="10" height="10">
                  <!--<img  src="<%//=BASEURL%>/images/botones/iconos/obligatorio.gif" width="10" height="10"></td>
		  </tr>-->
                <!--AGREGADO POR MALTAMIRANDA-->
                <tr class="fila">
			<td  height="20" colspan="2">Pagare a Favor de </td>
				<td width="387" colspan="6" ><select name="afavor" id="afavor">
                			 <option value="<%=usuario.getLogin()%>" <%=(usuario.getLogin().equals(a_favor))?"selected":""%> >AFILIADO </option>
                			 <option value="FINTRA" <%=("FINTRA".equals(a_favor))?"selected":""%> >FINTRA S.A</option>
                			 <option value="PROVINT" <%=("PROVINT".equals(a_favor))?"selected":""%> >PROVINTEGRAL</option>

                </select>
				<img  src="<%=BASEURL%>/images/botones/iconos/obligatorio.gif" width="10" height="10">
                  <!--<img  src="<%//=BASEURL%>/images/botones/iconos/obligatorio.gif" width="10" height="10">--> </td>
		  </tr>

                  <tr class="fila">
			<td  height="20" colspan="2">No Identificacion (Endoso) </td>
			 <td height="20" colspan="6"><input name="ccaut" type="text" class="textbox" id="ccaut" size="15"  maxlength="15"  value="<%=idendoso%>" >
		      <img  src="<%=BASEURL%>/images/botones/iconos/obligatorio.gif" width="10" height="10"></td>
			</tr>
                <!--AGREGADO POR MALTAMIRANDA-->
		  				
		  </table>

		</td>
</tr>
</table>
 <p align="center">&nbsp;</p>
 <table width="699" border="0" align="center">
  <tr>
    <td colspan="2" nowrap align="center">	
	<%	//if (loginx.equals("NAVI") || loginx.equals("TMOLINA")){%>
	  <img  name="Generar" src="<%=BASEURL%>/images/botones/aceptar.gif"  style = "cursor:hand" align="middle" type="image" id="Generar"  onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" onClick="return generarPagare(frCliente)">                  	  
	<%//}%>
      <!--<img  name="Guardar" src="<%//=BASEURL%>/images/botones/aceptar.gif"  style = "cursor:hand" align="middle" type="image" id="Guardar"  onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" onClick="return validar(frCliente)">                  -->
	  <img src="<%=BASEURL%>/images/botones/restablecer.gif" width="87" name="buss" height="21" align="absmiddle" style="cursor:hand"   onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" onClick="document.location='<%=BASEURL%>/jsp/fenalco/clientes/insert_clientes.jsp'">
	  <img src="<%=BASEURL%>/images/botones/salir.gif"  name="imgsalir" align="absmiddle" style="cursor:hand" onClick="parent.close();" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);"> 
	 
	  </td>
    </tr>
</table>
<input name="negocio" type="hidden" value="<%=neg%>">
</form>
<%if(!msg.equals("")){%>
    <table width="407" border="2" align="center">
      <tr>
        <td><table width="100%"   align="center"  >
            <tr>
              <td width="307" align="center" class="mensajes"><%=msg%></td>
              <td width="10" background="<%=BASEURL%>/images/cuadronaranja.JPG"></td>
            </tr>
        </table></td>
      </tr>
    </table>

<%}%>
</div>   
</body>
</html>
<script>
 function validar( forma ){
		/*if ( forma.Celular.value == '' ){
			alert( 'Debe Ingresar el codigo del afiliado para continuar...' );
			forma.Celular.focus();
			return false;
		}*/
		if ( forma.aprob.value == '' ){
			alert( 'Debe Ingresar el codigo de aprobacion para continuar...' );
			forma.aprob.focus();
			return false;
		}
		if ( forma.ccaut.value == '' ){
			alert( 'Debe Ingresar la cedula autoriazada...' );
			forma.ccaut.focus();
			return false;
		}
forma.submit();
}

function generarPagare( forma ){
		/*if ( forma.Celular.value == '' ){
			alert( 'Debe Ingresar el codigo del afiliado para continuar...' );
			forma.Celular.focus();
			return false;
		}*/
		if ( forma.aprob.value == '' ){
			alert( 'Debe Ingresar el codigo de aprobacion para continuar...' );
			forma.aprob.focus();
			return false;
		}
		if ( forma.ccaut.value == '' ){
			alert( 'Debe Ingresar la cedula autoriazada...' );
			forma.ccaut.focus();
			return false;
		}
		forma.action='<%=CONTROLLER%>?estado=Pdf&accion=Imprimir&opcion=imprimirpdfpagare';				
		forma.submit();
}
    function load_select(sel,filtro){
        while(sel.length > 0) sel.options[0]=null;
        i = 0;
        sel.clear;
        eval(sel.options[i++]=new Option("","_____________-_-_____________-_-_____________-_-_____________"));
        for(jj=0;jj<vec.length;jj++)
        {   if((vec[jj].toUpperCase()).indexOf((filtro.toUpperCase()))!=-1)
            {   var selOpcion;
                selOpcion=new Option(vec[jj],vec2[jj]);
                if(vec2[jj].split('_-_-',4)[0]=='<%=id_codeudor%>')
                {   selOpcion.selected=true;
                }
                eval(sel.options[i++]=selOpcion);
            }
        }
    }
</script>
