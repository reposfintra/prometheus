<!--
- Autor : Ing. Roberto Rocha
- Date  : 14 de Nov de 2007
- Copyrigth Notice : Fintravalores S.A. S.A
-->
<%--
-@(#)
--Descripcion : Pagina JSP, que maneja los pagares

--%>

<%-- Declaracion de librerias--%>
<%@page contentType="text/html;"%>
<%@page session="true"%>
<%@page import="java.util.*"%>
<%@page import="com.tsp.operation.model.*"%> 
<%@page import="com.tsp.operation.model.beans.*"%>
<%@page import="com.tsp.util.*"%>
<%@page import="com.tsp.util.Util.*"%>
<%@include file="/WEB-INF/InitModel.jsp"%>
<%@page import="com.tsp.operation.model.*"%>

<html>
<head>
<title>GENERADOR DE LETRAS</title>

<link href="../../../css/estilostsp.css" rel="stylesheet" type="text/css"> 
<link href="<%=BASEURL%>/css/estilostsp.css" rel="stylesheet" type="text/css"> 

<style type="text/css">
<!--
.style5 {font-size: 12px}
.style6 {font-size: 14px}
-->
</style>

<% 
	BeanGeneral bg =  model.Negociossvc.RetLet();
	String 	aprob=		bg.getValor_17();
	String  cotorg =	bg.getValor_19();
	String  fechai=		bg.getValor_09();
	String  conslet= 	bg.getValor_04();;
	String  nom=		bg.getValor_02();
	String  cc=			bg.getValor_06();
	String  exp=		bg.getValor_12();
	String  emp=		bg.getValor_10();
	String  nomcod=		bg.getValor_14();
	String  ccod=		bg.getValor_15();;
	String  afil=		bg.getValor_03();
	double valor= 		Double.valueOf(bg.getValor_05()).doubleValue();
	double cced=		Double.valueOf(bg.getValor_06()).doubleValue();
	String nodoc=		bg.getValor_01();
	String domipago=	bg.getValor_18();
	String nopagare=	bg.getValor_16()+"-"+bg.getValor_08();
	String ccaut=		bg.getValor_21();
	System.out.println("HH "+fechai);
	java.util.Calendar Fecha_Negocio = Util.crearCalendarDate(fechai);
	java.util.Calendar Fecha_Temp = Util.crearCalendarDate(fechai);
	
	//Cantidad en Letras
	RMCantidadEnLetras ejem= new RMCantidadEnLetras();
	  String[] a;
	  a=ejem.getTexto(valor);
	  String res="";
	  for(int i=0;i<a.length;i++)
	  {
		res=res+((String)a[i]).replace('-',' ');
	  }
	 //Fin Cant Letras
	
%>
<script language="Javascript">
function imprSelec(nombre)
{
  var ficha = document.getElementById(nombre);
  var ventimp = window.open(' ', 'popimpr');
  ventimp.document.write( ficha.innerHTML );
  ventimp.document.close();
  ventimp.print( );
  ventimp.close();
} 

		function imprimirpdf(frm){
			<%
			model.PdfImprimirSvc.setBeanGeneral(bg);
			%>
    		frm.action='<%=CONTROLLER%>?estado=Pdf&accion=Imprimir&opcion=imprimir_letras';
			//alert("a");
            frm.submit();
			
		}
		
		function imprimirpdfendosocodeudor(frm){
    		frm.action='<%=CONTROLLER%>?estado=Pdf&accion=Imprimir&opcion=imprimirpdfendosocodeudor&num_letras=<%=nodoc%>&cc_representante=<%=ccaut%>&representada=<%=afil%>&cc_deudor=<%=cc%>&cc_codeudor=<%=ccod%>';				
			frm.submit();
		}
		
		function imprimirpdfendoso(frm){
    		frm.action='<%=CONTROLLER%>?estado=Pdf&accion=Imprimir&opcion=imprimirpdfendoso&num_letras=<%=nodoc%>&cc_representante=<%=ccaut%>&representada=<%=afil%>&cc_deudor=<%=cc%>';				
			frm.submit();
		}
				
</script> 
<style type="text/css">
<!--
.style5 {font-size: 12px}
.style6 {font-size: 14px}
.style61 {font-size: 14px}
.style61 {font-size: 14px}
-->
</style>
</head>

<script type='text/javascript' src="<%=BASEURL%>/js/Validacion.js"></script>
<script type='text/javascript' src="<%= BASEURL %>/js/boton.js"></script>
<script type='text/javascript' src="<%= BASEURL %>/js/general.js"></script>

<body   >
<div id="capaSuperior" style="position:absolute; width:100%; height:100px; z-index:0; left: 0px; top: 0px;">
	<jsp:include page="/toptsp.jsp?encabezado=GENERADOR DE LETRAS"/>
</div>
<div id="capaCentral" style="position:absolute; width:100%; height:87%; z-index:0; left: 0px; top: 100px; overflow: scroll;">

<img src="<%=BASEURL%>/images/imprime.gif" width="70" height="19" style="cursor:hand" onClick="javascript:imprimirpdf(formulario)">

<a href="#" onClick="javascript:imprimirpdfendosocodeudor(formulario)">Endoso Con Coodeudor</a> 

<a href="#" onClick="javascript:imprimirpdfendoso(formulario)"> Endoso Sin Coodeudor</a> 

<a href="#" onClick="javascript:window.open('<%=BASEURL%>/jsp/fenalco/letras/pagares.jsp?nopag=<%=nopagare%>&aprob=<%=aprob%>&n1=<%=nom%>&cc1=<%=cc%>&ccod=<%=ccod%>')">Pagares</a>
<DIV ID="seleccion">
<form name="formulario" target="_blank" action="<%=CONTROLLER%>?estado=Pdf&accion=Imprimir&opcion=imprimir_letras"  method="post">
<%for(int i=0;i<Integer.valueOf(nodoc).intValue();i++){%>
<table>
 <tr>
		<td>
			<table width="857" height="466" border="0" align="left" bordercolor="#000000"  >
				<tr>
					<td>
							<table width="99%"   align="center">
							<tr> 
								<td width="76"><img name="logo" type="image" src="<%=BASEURL%>/images/FEN.bmp" align="top"></td>
								<td width="434"><div align="center"><br>
								LETRA DE CAMBIO SIN PROTESTO</br>
								</div></td>
								<td width="244" colspan="2">No Aprobacion <b style="text-decoration:underline "><%=aprob%></b></td>
							</tr>
							</table>
								
					</td>
				</tr>
				<tr>
					<td>
							<table width="99%"   align="center">
							<tr> 
							  <td width="43%"><span class="style5 ">(Ciudad y Fecha de Otorgamiento) <%=cotorg%> </span></td>
								<td width="20%"><span style="font-size: 12px"><b style="text-decoration:underline "><%=Util.crearStringFechaDateLetra2(Fecha_Negocio)%> </b></span></td>
								<%Fecha_Temp.add(2,	i+1);%>
								<td width="25%"><span style="font-size: 12px">Fecha de Vencimiento</span></td>
								<%String fmost=Util.crearStringFechaDateLetra2(Fecha_Temp);%>
								<td width="12%"><span style="font-size: 12px"><b style="text-decoration:underline "><%=fmost%></b></span></td>
							</tr>
							</table>
								
					</td>
				</tr>
				<tr>
					<td>
							<table width="99%"   align="center">
							<tr> 
								<td width="24%"><span class="style6">(Consecutivo de Letras) </span></td>
								<td width="76%"><span style="font-size: 12px"><b style="text-decoration:underline "><%=conslet%>-<%=i+1%></b></span></td>
							</tr>
							</table>
								
					</td>
				</tr>
				<tr>
					<td>
							<table width="99%" height="323"   align="center">
							<tr nowrap> 
								<td width="24%"><p align="justify" class="style6" >Yo <b style="text-decoration:underline "><%=nom%></b>  (Deudor) identificado con la c&eacute;dula de ciudadania No. <b ><b style="text-decoration:underline "><%=Util.PuntoDeMil(cc)%></b></b> expedida en <b ><b style="text-decoration:underline "><%=exp%></b></b> en calidad de girador y aceptante (art. 673 c�digo de comercio) obrando en nombre propio y como representante legal de 
								<%if(emp.equals("") ){%>_____________________________________<%}else{%><b style="text-decoration:underline "> <%=emp%></b><%}%> y como codeudor de &eacute;sta, y,  <%if(nomcod.equals("") ){%>__________________________________<%}else{%><b style="text-decoration:underline "> <%=nomcod%></b><%}%>  
								C.C. No.  
								<%if(ccod.equals("") ){%>_________________<%}else{%><b style="text-decoration:underline "> <%=Util.PuntoDeMil(ccod)%></b><%}%> 
								en calidad de codeudor, y en la fecha cierta: dia <strong><%=fmost.substring(0,2)%></strong> del mes de <strong><%=Util.NombreMes3(Integer.valueOf(fmost.substring(3,5)).intValue())%></strong> del a&ntilde;o <strong><%=fmost.substring(6,10)%></strong> pagar&eacute; solidaria e incondicionalmente en la ciudad de Barranquilla (art. 677, 683 y 876. C&oacute;digo de comercio) a la orden de:   <b style="text-decoration:underline "><%=afil%></b> o a su(s) tenedor(es) legitimo(s), la suma de:<b style="text-decoration:underline "> <%=res%> ($ <%=Util.customFormat(valor)%>) </b>  mas intereses remuneratorios liquidables a la tasa m&aacute;xima de interes corriente bancario y moratorios a la m&aacute;xima tasa legal vigente. En el evento de mora ser&aacute;n de mi cargo los gastos de cobranza tasados al 20% del total adeudado al momento del pago y los honorarios igualmente liquidados al 20% del total adecuado. Los giradores, aceptantes de esta letra nos obligamos solidariamente y renunciamos a la presentaci&oacute;n para la aceptaci&oacute;n y pago, y facultamos a <b style="text-decoration:underline "><%=afil%></b> o a su(s) tenedor(es) legitimo(s) para rehusar el pago parcial y los avisos de rechazo. Domicilio de pago <b style="text-decoration:underline "><%=domipago%></b>
                                  <p class="style6" >							  </td>
								
							</tr>
							<tr>
							  <td><p align="center" class="style6" ><strong>Otorgada y Aceptada (art 676 y 685 C&oacute;digo de Comercio)</strong></p>
						      </td>
							  </tr>
							<tr>
							  <td><p class="style6">&nbsp;</p>
						      </td>
							  </tr>
							<tr>
							  <td><p class="style6"><span class="style61">Firma Deudor_______________________ Documento de Identidad <b style="text-decoration:underline "><%=Util.PuntoDeMil(cc)%></b> Huella </span></p>
							    <p class="style6">Firma Codeudor_______________________ Documento de Identidad 
							      <%if(ccod.equals("") ){%>
							      _______________
							      <%}else{%>
							      <b style="text-decoration:underline "> <%=Util.PuntoDeMil(ccod)%></b>
							      <%}%>
							      Huella </p>
							    <p align="right" class="style6">Afiliado</p></td>
							  </tr>
					  </table>
								
					</td>
				</tr>
		  </table>
		</td>
	</tr>
	<tr>
		<td><p>&nbsp;</p></td>
	</tr>
</table>
<%
Fecha_Temp = Util.crearCalendarDate(fechai);
}%>
</form>
 </div>
</div>
<iframe width="188" height="166" name="gToday:normal:agenda.js:gfPop:plugins.js" id="gToday:normal:agenda.js:gfPop:plugins_24.js" src="<%=BASEURL%>/js/Calendario/ipopeng.htm" scrolling="yes" frameborder="0" style="visibility:visible; z-index:999; position:absolute; left:-500px; top:0px;">
</iframe>
</body>
</html>


