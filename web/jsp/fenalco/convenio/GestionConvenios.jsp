<%-- 
    Document   : GestionConvenios
    Created on : 5/08/2010, 08:17:42 AM
    Author     : maltamiranda
--%>


<%@page import="com.tsp.operation.model.DAOS.impl.AdministracionEdsImpl"%>
<%@page import="com.tsp.operation.model.DAOS.AdministracionEdsDAO"%>
<%@page import="com.tsp.operation.model.services.GestionConveniosService"%>
<%@page import="com.tsp.operation.model.beans.Tipo_impuesto"%>
<%@page import="com.tsp.operation.model.beans.ConvenioComision"%>
<%@page import="com.tsp.operation.model.beans.ConveniosRemesas"%>
<%@page import="com.tsp.operation.model.beans.ConvenioCxc"%>
<%@page import="com.tsp.operation.model.beans.ConvenioFiducias"%>
<%@page import="com.tsp.operation.model.beans.ConvenioCxcFiducias"%>
<%@page contentType="text/html" pageEncoding="ISO-8859-1"%>
<%@page session    ="true"%>
<%@page errorPage  ="/error/ErrorPage.jsp"%>
<%@include file    ="/WEB-INF/InitModel.jsp"%>
<%@page import    ="java.util.*" %>
<%@page import    ="com.tsp.util.*" %>
<%@page import    ="com.tsp.operation.model.beans.Usuario" %>
<%@page import="com.tsp.operation.model.beans.Convenio"%>

<script src="<%=BASEURL%>/js/prototype.js"      type="text/javascript"></script>
<script src="<%=BASEURL%>/js/boton.js"          type="text/javascript"></script>
<script src="<%=BASEURL%>/js/effects.js"        type="text/javascript"></script>
<script src="<%=BASEURL%>/js/window.js"         type="text/javascript"></script>
<script src="<%=BASEURL%>/js/window_effects.js" type="text/javascript"></script>

<link href="<%= BASEURL%>/css/estilostsp.css" rel="stylesheet" type="text/css">
<link href="<%=BASEURL%>/css/mac_os_x.css"      rel="stylesheet" type="text/css">
<link href="<%=BASEURL%>/css/default.css"       rel="stylesheet" type="text/css">
<link href="<%=BASEURL%>/css/alert.css" rel="stylesheet" type="text/css"/>
<link href="<%=BASEURL%>/css/alphacube.css" rel="stylesheet" type="text/css"/>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">


<html>
    <style type="text/css">
        div.fixedHeaderTable {
            position: relative;
        }
        div.fixedHeaderTable tbody {
            height: 100px;
            overflow-y: scroll;
            overflow-x: hidden;
        }
        div.fixedHeaderTable thead td, div.fixedHeaderTable thead th {
            position:relative;
        }

        /* IE7 hacks */
        div.fixedHeaderTable {
            *position: relative;
            *height: 100px;
            *overflow-y: scroll;
            *overflow-x: scroll;
            *padding-right:16px;
        }

        div.fixedHeaderTable thead tr {
            *position: relative;
            _position: absolute;
            *top: expression(this.offsetParent.scrollTop-2);
            *background:none;
            color: white;
            border-color: silver;
        }

        div.fixedHeaderTable tbody {
            *height: auto;
            *position:absolute;
            *top:50px;
        }
        /* IE6 hacks */
        div.fixedHeaderTable {
            _width:100%;
            _overflow: auto;
            _overflow-y: scroll;
            _overflow-x: hidden;
        }
        div.fixedHeaderTable thead tr {
            _position: relative
        }
    </style>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
        <title>Gestion de Convenios</title>
        <script type="text/javascript">
            var items=1;
            var tamano_tabla_comisiones=1;
            var tamano_tabla_remesas=1;
            var tamano_tabla_cxc=1;
            var tamano_tabla_fiducia=1;
            var radioChecked;
            function init(){
                $('tasa_interes').value=formatNumber($('tasa_interes').value);
                if($('tipoconv').value=='Consumo'){
                    $('valor_custodia').value=formatNumber($('valor_custodia').value);
                    for(i=0;i<tamano_tabla_comisiones;i++){
                        $('porcentaje_tabla_comisiones_'+i).value=formatNumber($('porcentaje_tabla_comisiones_'+i).value);
                    }
                }
                if($('tipoconv').value=='Microcredito'){
                    $('valor_central').value=formatNumber($('valor_central').value);
                    $('valor_com_central').value=formatNumber($('valor_com_central').value);
                    $('valor_capacitacion').value=formatNumber($('valor_capacitacion').value);
                    $('valor_seguro').value=formatNumber($('valor_seguro').value);
                    $('porcentaje_com_seguro').value=formatNumber($('porcentaje_com_seguro').value);
                    $('porcentaje_cat').value=formatNumber($('porcentaje_cat').value);
                }
            }
                
            function soloNumerosformateado(id) {
                var precio = document.getElementById(id).value;
                precio =  precio.replace(/[^0-9^.]+/gi,"");
                document.getElementById(id).value = formatNumber(precio);
            }
            function soloNumeros(id) {
                var precio = document.getElementById(id).value;
                precio =  precio.replace(/[^0-9^.]+/gi,"");
                document.getElementById(id).value = precio;
            }
            function formatNumber(num,prefix){
                prefix = prefix || '';
                num += '';
                var splitStr = num.split('.');
                var splitLeft = splitStr[0];
                var splitRight = splitStr.length > 1 ? '.' + splitStr[1] : '';
                //splitRight = splitRight.substring(0,3);//090929
                var regx = /(\d+)(\d{3})/;
                while (regx.test(splitLeft)) {
                    splitLeft = splitLeft.replace(regx, '$1' + ',' + '$2');
                }
                return prefix + splitLeft + splitRight;
            }
            function guardar_cambios(form){
                var sw=true;
                var generaRemesa = false;
                if(!validar_convenio()){
                    sw=false;
                    Dialog.alert("<HTML>Debe llenar todos los datos requeridos para la creacion o modificacion del convenio</HTML>", {
                        width:250,
                        height:100,
                        windowParameters: {className: "alphacube"}
                    });
                }
                if(!validar_tabla_comisiones()){
                    sw=false;
                    Dialog.alert("<HTML>Debe llenar todos los datos requeridos para la creacion o modificacion de comisiones del convenio</HTML>", {
                        width:250,
                        height:100,
                        windowParameters: {className: "alphacube"}
                    });
                }
                 if(!validar_tabla_fiducias()){
                    sw=false;
                    Dialog.alert("<HTML>Debe llenar todos los datos requeridos para la creacion o modificacion de Fiducias del convenio</HTML>", {
                        width:250,
                        height:100,
                        windowParameters: {className: "alphacube"}
                    });
                }
                if(!validar_tabla_cxc()){
                    sw=false;
                    Dialog.alert("<HTML>Debe llenar todos los datos requeridos para la creacion o modificacion de CXCs del convenio</HTML>", {
                        width:250,
                        height:100,
                        windowParameters: {className: "alphacube"}
                    });
                }
                if(generaRemesa && !validar_tabla_remesas()){
                    sw=false;
                    Dialog.alert("<HTML>Debe llenar todos los datos requeridos para la creacion o modificacion de remesas del convenio</HTML>", {
                        width:250,
                        height:100,
                        windowParameters: {className: "alphacube"}
                    });
                }

                if(sw){                    
                    if($('id_convenio').value==''){
                        $('tipo').value="NUEVO_CONVENIO";
                    }
                    else{
                        $('tipo').value="MODIFICAR_CONVENIO";
                        Dialog.alert("<HTML>Recuerde que todos los negocios generados a partir de este momento con este convenio asignado tomaran esta parametrizacion</HTML>", {
                            width:250,
                            height:100,
                            windowParameters: {className: "alphacube"}
                        });
                    }
                    $('tamano_tabla_comisiones').value=tamano_tabla_comisiones;
                    $('tamano_tabla_remesas').value=tamano_tabla_remesas;
                    $('tamano_tabla_cxc').value=tamano_tabla_cxc;
                     if($('tipoconv').value=='Consumo'){
                         $('tamano_tabla_fiducia').value=tamano_tabla_fiducia;
                     }
                    $(form).submit();
                }
            }
            function validar_tabla_comisiones(){
                sw=true;
                if($('tipoconv').value=='Consumo'){
                    var swTercero = false;
                    for(i=0;i<tamano_tabla_comisiones&&sw;i++){
                        if( i != tamano_tabla_comisiones-1 || (i==(tamano_tabla_comisiones-1) && $('nombre_tabla_comisiones_'+i).value!="")){
                            if($('nombre_tabla_comisiones_'+i).value==""||$('porcentaje_tabla_comisiones_'+i).value==""||$('cuenta_tabla_comisiones_'+i).value=="" || ($("chkInd_"+i).checked && $("txtContrapartida_"+i).value=="") ){
                                sw=false;
                            }

                            if((!$("rdoTerceroFactura_"+i).checked)&&(!$("chkInd_"+i).checked )){
                                sw=false;
                            }

                        }
                    
                        if($("rdoTerceroFactura_"+i).checked){
                            swTercero = true;
                        }
                    }
                    if($("factura_tercero").value=="true" && swTercero==false){
                        sw=false;
                    }
                }
                return sw;
            }
            function validar_tabla_remesas(){
                sw=true;
                if($('tipoconv').value=='Consumo'){
                    for(i=0;i<tamano_tabla_remesas &&sw;i++){
                        if(generaRemesa){
                            if(i==0  || i != (tamano_tabla_remesas-1) || (i==(tamano_tabla_remesas-1) && $('ciudad_tabla_remesas_'+i).value!="")){
                                if($('ciudad_tabla_remesas_'+i).value==""||$('bancoch_tabla_remesas_'+i).value==""||$('cuenta_tabla_remesas_'+i).value==""||$('porcentaje_tabla_remesas_'+i).value==""||$('remesa_tabla_remesas_'+i).value=="")
                                { sw=false;
                                }
                            }
                        }else{
                            if($('ciudad_tabla_remesas_'+i).value!=""){
                                if($('ciudad_tabla_remesas_'+i).value==""||$('bancoch_tabla_remesas_'+i).value==""||$('cuenta_tabla_remesas_'+i).value==""||$('porcentaje_tabla_remesas_'+i).value==""||$('remesa_tabla_remesas_'+i).value=="")
                                { sw=false;
                                }
                            }
                        }
                    }
                }
                return sw;
            }

            function validar_tabla_fiducias(){
                sw=true;
                if($('tipoconv').value=='Consumo'){
                    for(i=0;i<tamano_tabla_fiducia &&sw;i++){

                        if($('nit_fiducia'+i).value==""||$('prefijo_dif_fiducia'+i).value==""||$('hc_dif_fiducia'+i).value==""||$('cuenta_dif_fiducia'+i).value==""||$('prefijo_end_fiducia'+i).value==""||$('hc_end_fiducia'+i).value=="")
                        { sw=false;
                            break;
                        }
                    }
                }
                return sw;
            }
            function validar_tabla_cxc(){
                sw=true;
                for(i=0;i<tamano_tabla_cxc &&sw;i++){
                 
                        if($('tipo_documento_tabla_cxc_'+i).value==""||$('prefijo_tabla_cxc_'+i).value==""||$('cuenta_tabla_cxc_'+i).value==""||$('hc_tabla_cxc_'+i).value==""||($('tipoconv').value=='Consumo'&&$('gen_remesa_'+i).value==""))
                        { sw=false;
                        }
                        if($('tipoconv').value=='Consumo'){
                            if($('gen_remesa_'+i).value == "true"){
                                generaRemesa = true;
                            }
                            if(($('redescuento').value=="false")){
                                if($('cuenta_prov_cxc_'+i).value=="" || $('cuenta_prov_cxp_'+i).value==""){
                                    sw=false;
                                }
                            }
                        
                    }
                    if(fiducias[0].length>0&&$('tipoconv').value=='Consumo'){
                       for(j=0;j<$('tamano_tabla_cxc_fiducia'+i).value &&sw;j++){
//                            if($('nit_fiducia_tabla_cxc_'+i+''+j).value==""||$('prefijo_tabla_cxc_fiducia_'+i+''+j).value==""||$('hc_tabla_cxc_fiducia_'+i+''+j).value==""||$('cuenta_tabla_cxc_fiducia_'+i+''+j).value=="")
//                            {
//                                sw=false;
//                            }
                       }
                    }else{
                        //$('tamano_tabla_cxc_fiducia'+i).value=0;
                    }
                   
                }
                return sw;
            }         
    
            function validar_convenio(){
                sw=false;
                if($('tipoconv').value=='Consumo'){                   
                    if($('nombre').value!=''&&$('descripcion').value!=''&&(($('aval_tercero').value=="false" && ($('aval_anombre').value=="false" || ($('aval_anombre').value=="true"&&$('nit_anombre').value!='')))|| ($('aval_tercero').value=="true"&&$('nit_convenio').value!=''))&&$('factura_tercero').value!=''&&$('redescuento').value!=''&&$('mediador_aval').value!=''&&
                        ($('factura_tercero').value=="false" || ($('factura_tercero').value=="true"&&$('nit_tercero').value!=''))&&($('mediador_aval').value=="false" || ($('mediador_aval').value=="true"&&$('nit_mediador').value!=''))&&
                        $('tasa_interes').value!=''&&$('cuenta_interes').value!=''&&$('valor_custodia').value!=''&&$('cuenta_custodia').value!=''&&
                        $('prefijo_negocio').value!=''&&$('prefijo_cxp').value!=''&&$('hc_cxp').value!=''&&$('cuenta_cxp').value!=''&&$('prefijo_diferidos').value!=''&&
                        $('hc_diferidos').value!=''&&$('cuenta_diferidos').value!=''&&$('prefijo_endoso').value!=''&&
                        $('hc_endoso').value!=''&& (($('redescuento').value=="false" && $('prefijo_cxc_aval').value!=''&& $('hc_cxc_aval').value!=''&& $('cuenta_cxc_aval').value!='' && $('cctrl_db_cxc_aval').value!='' && $('aval_anombre').value!="" && $('cctrl_cr_cxc_aval').value!=''&&$('cctrl_iva_cxc_aval').value!='') || $('redescuento').value=="true") &&
                        $('impuesto').value!=''&&$('cuenta_ajuste').value!=''&&$('cuenta_aval').value!=''){//la cuenta del aval es un campo obligatorio

                        if(!$('descuenta_gmf').checked || ($('descuenta_gmf').checked&&$('cuota_gmf').value!=''&&$('cuenta_gmf').value!=''&&$('cuenta_gmf2').value!=''&&$('porc_gravamen').value!=''&&$('porc_gravamen2').value!=''&&$('prefijo_gmf').value!='')){
                            if((!$('descuenta_aval').checked)||($('descuenta_aval').checked&&$('prefijo_aval').value!='')){
                                if((!$('cxp_avalista').checked)||($('cxp_avalista').checked &&$('prefijo_cxp_avalista').value!=''&&$('hc_cxp_avalista').value!=''&&$('cuenta_cxp_avalista').value!='')){
                                    sw=true;
                                }
                            }
                        }
                    }
                }
                if($('tipoconv').value=='Microcredito'){
                    if($('nombre').value!=''&&$('descripcion').value!=''&&($('central').value=="false" || ($('central').value=="true"&&$('nit_central').value!=''&&$('prefijo_cxp_central').value!=''&&$('cuenta_central').value!=''
                        &&$('valor_central').value!=''&&$('valor_com_central').value!=''&&$('cuenta_com_central').value!=''))
                        &&($('capacitacion').value=="false" || ($('capacitacion').value=="true"&&$('nit_capacitador').value!=''&&$('cuenta_capacitacion').value!=''&&$('valor_capacitacion').value!=''))
                        &&($('seguro').value=="false" || ($('seguro').value=="true"&&$('nit_asegurador').value!=''&&$('cuenta_seguro').value!=''&&$('cuenta_com_seguro').value!=''&&$('valor_seguro').value!=''&&$('porcentaje_com_seguro').value!=''))
                        &&$('prefijo_cxc_interes').value!=''&&$('tasa_interes').value!=''&&$('cuenta_interes').value!=''&& ($('cat').value=="false" || ($('cat').value=="true"&&$('prefijo_cxc_cat').value!=''&&$('cuenta_cat').value!=''&&$('porcentaje_cat').value!=''))&&
                        $('prefijo_negocio').value!=''&&$('prefijo_cxp').value!=''&&$('hc_cxp').value!=''&&$('cuenta_cxp').value!=''&&$('monto_minimo').value!=''&&$('monto_maximo').value!=''&&$('plazo_maximo').value!=''){
                        if($('cat').value=="false" || ($('cat').value=="true"&&(!$('descuenta_gmf').checked || ($('descuenta_gmf').checked&&$('cuota_gmf').value!=''&&$('cuenta_gmf').value!=''&&$('cuenta_gmf2').value!=''&&$('porc_gravamen').value!=''&&$('porc_gravamen2').value!=''&&$('prefijo_gmf').value!='')))){
                            sw=true;
                        }
              
                    }
                }
                return sw;
       
            }

            function delrow_cxc(tipoconv){
                var myTable = $("tabla_cxc");
                if(tipoconv=='Consumo'){
                myTable.deleteRow(myTable.rows.length-2);
                }
                myTable.deleteRow(myTable.rows.length-1);
                tamano_tabla_cxc--;
            }

            function delrow_cxc_fiducia(id){
                var myTable = $("tabla_cxc_fiducia"+id);
                myTable.deleteRow(myTable.rows.length-1);
                $('tamano_tabla_cxc_fiducia'+id).value=parseInt(parseInt($('tamano_tabla_cxc_fiducia'+id).value)-1);
            }

            function delrow_fiducia(){
                var myTable = $("tabla_fiducias");
                myTable.deleteRow(myTable.rows.length-3);
                myTable.deleteRow(myTable.rows.length-2);
                myTable.deleteRow(myTable.rows.length-1);
                tamano_tabla_fiducia--
            }

            function agregar_cxc(tablaId, tipoconv){
              
                var id = tamano_tabla_cxc;
                myTable = $(tablaId);
                if(navigator.appName=='Microsoft Internet Explorer')
                {   oRow = myTable.insertRow();
                }else{
                    oRow = myTable.insertRow(-1);
                }
                oCell=oRow.insertCell(0);

                oCell.align='center';
                oCell.className='fila';
                if(tipoconv=='Consumo'){
                oCell.rowSpan='2';
                }
                oCell.innerHTML="<img style='cursor: pointer' onclick='agregar_cxc(\""+tablaId+"\", \""+tipoconv+"\");' src='<%=BASEURL%>/images/botones/iconos/mas.gif'   width='12' height='12' >"+
                    '<br/><img alt="-"style="cursor: pointer" '+"onclick='delrow_cxc(\""+tipoconv+"\");'"+' src="<%=BASEURL%>/images/botones/iconos/menos1.gif" width="12" height="12" >';

                oCell=oRow.insertCell(1);
                oCell.align='left';
                oCell.className='fila';
                option="";
                for(i=0;i<tipo_documento[0].length;i++){
                    option=option+"<option value='"+tipo_documento[0][i]+"'>"+tipo_documento[1][i]+"</option>"
                }
                oCell.innerHTML="<select id='tipo_documento_tabla_cxc_"+id+"' name='tipo_documento_tabla_cxc_"+id+"'><option value=''>...</option>"+option+"</select>";
                if(tipoconv=='Consumo'){
                    oCell=oRow.insertCell(2);
                    oCell.align='left';
                    oCell.className='fila';
                    oCell.innerHTML="<select id='gen_remesa_"+id+"' name='gen_remesa_"+id+"'>"+
                        "<option value=''>...</option>"+
                        "<option value='true'>SI</option>"+
                        "<option value='false'>NO</option>"+
                        "</select>";
                }
                if(tipoconv=='Consumo'){
                    oCell=oRow.insertCell(3);
                }else{
                    oCell=oRow.insertCell(2);
                }
                oCell.align='left';
                oCell.className='fila';

                option="";
                for(i=0;i<prefijos[0].length;i++){
                    if(prefijos[0][i]=='FAC')
                        option=option+"<option value='"+prefijos[2][i]+"'>"+prefijos[1][i]+"</option>"
                }
                oCell.innerHTML="<select id='prefijo_tabla_cxc_"+id+"' name='prefijo_tabla_cxc_"+id+"'><option value=''>...</option>"+option+"</select>";

                if(tipoconv=='Consumo'){
                    oCell=oRow.insertCell(4);
                }else{
                    oCell=oRow.insertCell(3);
                }
                oCell.align='left';
                oCell.className='fila';
                oCell.innerHTML="<input style='width: 60px' type='text' maxlength='6' id='hc_tabla_cxc_"+id+"' name='hc_tabla_cxc_"+id+"'>";

                if(tipoconv=='Consumo'){
                    oCell=oRow.insertCell(5);
                }else{
                    oCell=oRow.insertCell(4);
                }
                oCell.align='left';
                oCell.className='fila';
                oCell.innerHTML="<input style='width: 100px' maxlength='30' type='text' id='cuenta_tabla_cxc_"+id+"' name='cuenta_tabla_cxc_"+id+"' onchange='validarcuenta("+'"cuenta_tabla_cxc_'+id+'"'+")'>";
                if(tipoconv=='Consumo'){
                    oCell=oRow.insertCell(6);
           
                    var dis=$('redescuento').value=="true"?"disabled":"";
                    oCell.align='left';
                    oCell.className='fila';
                    oCell.innerHTML="<input style='width: 100px' maxlength='30' type='text' id='cuenta_prov_cxc_"+id+"' name='cuenta_prov_cxc_"+id+"'  onchange='validarcuenta("+'"cuenta_prov_cxc_'+id+'"'+")'"+dis+">";

                    oCell.align='left';
                    oCell.className='fila';
                    oCell.innerHTML="<input style='width: 100px' maxlength='30' type='text' id='cuenta_prov_cxp_"+id+"' name='cuenta_prov_cxp_"+id+"'  onchange='validarcuenta("+'"cuenta_prov_cxp_'+id+'"'+")'"+dis+">";

                    if(navigator.appName=='Microsoft Internet Explorer')
                    {   oRow = myTable.insertRow();
                    }else{
                        oRow = myTable.insertRow(-1);
                    }
                    oCell=oRow.insertCell(0);
                    oCell.align='left';
                    oCell.colSpan='7';
                    oCell.className='fila';
                    option="";
                    for(i=0;i<prefijos[0].length;i++){
                        if(prefijos[0][i]=='FAC')
                            option=option+"<option value='"+prefijos[2][i]+"'>"+prefijos[1][i]+"</option>"
                    }
                    option2="";
                    for(i=0;i<fiducias[0].length;i++){
                        option2=option2+"<option value='"+fiducias[0][i]+"'>"+fiducias[1][i]+"</option>";
                    }
                    oCell.innerHTML='<table border="1" width="100%" id="tabla_cxc_fiducia'+id+'" cellspacing="0" >'+
                        '<tr style="height: 20px;">'+
                        ' <td align="left"  width="5%" class="fila">&nbsp;<input type="hidden" name="tamano_tabla_cxc_fiducia'+id+'" id="tamano_tabla_cxc_fiducia'+id+'" value="1"></td>'+
                        ' <td align="left"  class="fila">Fiducia</td>'+
                        ' <td align="left"  class="fila">Prefijo cxc</td>'+
                        '<td align="left" class="fila">Hc cxc</td>'+
                        '<td align="center"  class="fila">Cuenta cxc</td>'+
                        '<td align="left"  class="fila">Prefijo cxc endoso</td>'+
                        '<td align="left" class="fila">Hc cxc endoso</td>'+
                        '<td align="left" class="fila">Cuenta cxc endoso</td>'+
                        ' </tr>'+
                        '<tr>'+
                        '<td  align="center" class="fila">'+
                        "<img style='cursor: pointer' onclick='agregar_cxc_fiducia(\"tabla_cxc_fiducia"+id+"\", \""+id+"\");' src='<%=BASEURL%>/images/botones/iconos/mas.gif'   width='12' height='12' >"+
                        '</td> <td align="left" class="fila"><select id="nit_fiducia_tabla_cxc_'+id+'0" name="nit_fiducia_tabla_cxc_'+id+'0">'+
                        '<option value="">...</option>'+option2+'</select></td><td align="left" class="fila">'+
                        '<select id="prefijo_tabla_cxc_fiducia_'+id+'0" name="prefijo_tabla_cxc_fiducia_'+id+'0">'+
                        " <option value=''>...</option>"+option+"</select>"+
                        ' </td>  <td align="left" class="fila">'+
                        '<input style="width: 60px" type="text" maxlength="6" id="hc_tabla_cxc_fiducia_'+id+'0" name="hc_tabla_cxc_fiducia_'+id+'0" value="">'+
                        '</td>                <td align="left" class="fila">'+
                        '<input style="width: 100px" maxlength="30" type="text" id="cuenta_tabla_cxc_fiducia_'+id+'0" name="cuenta_tabla_cxc_fiducia_'+id+'0" value="" '+"onchange='validarcuenta(\"cuenta_tabla_cxc_fiducia_"+id+"0\")'>"+
                        '</td> '+
                        '<td align="left" class="fila"> <select id="prefijo_tabla_cxc_endoso_'+id+'0" name="prefijo_tabla_cxc_endoso_'+id+'0">'+
                        " <option value=''>...</option>"+option+"</select> </td>"+
                        '<td align="left" class="fila">'+
                        '<input style="width: 100px" maxlength="30" type="text" id="hc_tabla_cxc_endoso_'+id+'0" name="hc_tabla_cxc_endoso_'+id+'0" value="" '+"onchange='validarcuenta(\"cuenta_tabla_cxc_fiducia_"+id+"0\")'>"+
                        '</td>'+
                        '<td align="left" class="fila">'+
                        '<input style="width: 100px" maxlength="30" type="text" id="cuenta_tabla_cxc_endoso_'+id+'0" name="cuenta_tabla_cxc_endoso_'+id+'0" value="" '+"onchange='validarcuenta(\"cuenta_tabla_cxc_fiducia__"+id+"0\")'>"+
                        '</td>'+
                        '</tr></table>';
                    
                 }
                tamano_tabla_cxc++;


            }

            function agregar_remesa(tablaId){
                var id = tamano_tabla_remesas;
                myTable = $(tablaId);
                if(navigator.appName=='Microsoft Internet Explorer')
                {   oRow = myTable.insertRow();
                }else{
                    oRow = myTable.insertRow(-1);
                }
                oCell=oRow.insertCell(0);
                oCell.align='center';
                oCell.className='fila';
                oCell.innerHTML="<img style='cursor: pointer' onclick='agregar_remesa(\""+tablaId+"\");' src='<%=BASEURL%>/images/botones/iconos/mas.gif'   width='12' height='12' >"+
                    "<input type='hidden' name='id_tabla_remesas_"+id+"' id='id_tabla_remesas_"+id+"' value=''>";
                oCell=oRow.insertCell(1);
                oCell.align='left';
                oCell.className='fila';
                oCell.innerHTML="<input style='width: 85%' type='text' id='ciudad_tabla_remesas_"+id+"' name='ciudad_tabla_remesas_"+id+"' readonly>"+
                    "<img  src='<%=BASEURL%>/images/botones/iconos/lupa.gif' width='15' height='15' style='cursor: pointer' onclick='openWin(\"ciudades\",\"ciudad_tabla_remesas_"+id+"\",\"Ciudad Fintravalores\");'>";
                oCell=oRow.insertCell(2);
                oCell.align='left';
                oCell.className='fila';
                oCell.innerHTML="<input style='width: 85%' type='text' id='bancoch_tabla_remesas_"+id+"' name='bancoch_tabla_remesas_"+id+"' readonly>"+
                    "<img  src='<%=BASEURL%>/images/botones/iconos/lupa.gif' width='15' height='15' style='cursor: pointer' onclick='openWin(\"bancos\",\"bancoch_tabla_remesas_"+id+"\",\"Banco Titulo\");'>";
                oCell=oRow.insertCell(3);
                oCell.align='left';
                oCell.className='fila';
                oCell.innerHTML="<input style='width: 85%' type='text' id='ciudadch_tabla_remesas_"+id+"' name='ciudadch_tabla_remesas_"+id+"' onkeyup='soloNumeros(this.id);' maxlength='4'>";
                oCell=oRow.insertCell(4);
                oCell.align='left';
                oCell.className='fila';
                oCell.innerHTML="<select id='remesa_tabla_remesas_"+id+"' name='remesa_tabla_remesas_"+id+"' onchange='deshabilitarPorcentaje("+id+")' ><option value=''>...</option><option value='true'>SI</option><option value='false'>NO</option></select>";
                oCell=oRow.insertCell(5);
                oCell.align='left';
                oCell.className='fila';
                oCell.innerHTML="<input onkeyup='soloNumeros(this.id);' style='width: 120px' type='text' id='porcentaje_tabla_remesas_"+id+"' name='porcentaje_tabla_remesas_"+id+"'>";
                oCell=oRow.insertCell(6);
                oCell.align='left';
                oCell.className='fila';
                oCell.innerHTML="<input style='width: 100%' type='text' maxlength='30' id='cuenta_tabla_remesas_"+id+"' name='cuenta_tabla_remesas_"+id+"' onchange='validarcuenta("+'"cuenta_tabla_remesas_'+id+'"'+")'>";
                tamano_tabla_remesas++;
            }
            
            
            function agregar_cxc_fiducia(tablaId, fil ){
                var id = $('tamano_tabla_cxc_fiducia'+fil).value;
                myTable = $(tablaId);
                if(navigator.appName=='Microsoft Internet Explorer')
                {   oRow = myTable.insertRow();
                }else{
                    oRow = myTable.insertRow(-1);
                }
                oCell=oRow.insertCell(0);
                oCell.align='center';
                oCell.className='fila';
                oCell.innerHTML="<img style='cursor: pointer' onclick='agregar_cxc_fiducia(\""+tablaId+"\", \""+fil+"\");' src='<%=BASEURL%>/images/botones/iconos/mas.gif'   width='12' height='12' >"+
                    '<br/><img alt="-"style="cursor: pointer" onclick="delrow_cxc_fiducia('+fil+' );" src="<%=BASEURL%>/images/botones/iconos/menos1.gif" width="12" height="12" >'+
                    "<input type='hidden' name='id_tabla_cxc_"+id+"' id='id_tabla_cxc_"+id+"' value=''>";
                option="";
                for(i=0;i<fiducias[0].length;i++){
                    option=option+"<option value='"+fiducias[0][i]+"'>"+fiducias[1][i]+"</option>";
                }
                oCell=oRow.insertCell(1);
                oCell.align='left';
                oCell.className='fila';
                oCell.innerHTML='<select id="nit_fiducia_tabla_cxc_'+fil+''+id+'" name="nit_fiducia_tabla_cxc_'+fil+''+id+'">><option value="">...</option>'+option+'</select>';
                option="";
                for(i=0;i<prefijos[0].length;i++){
                    if(prefijos[0][i]=='FAC')
                        option=option+"<option value='"+prefijos[2][i]+"'>"+prefijos[1][i]+"</option>"
                }
                oCell=oRow.insertCell(2);                
                oCell.align='left';
                oCell.className='fila';
                oCell.innerHTML='<select id="prefijo_tabla_cxc_fiducia_'+fil+''+id+'" name="prefijo_tabla_cxc_fiducia_'+fil+''+id+'">><option value="">...</option>'+option+'</select>';
                 
                oCell=oRow.insertCell(3);
              
                oCell.align='left';
                oCell.className='fila';
                oCell.innerHTML='<input style="width: 60px" type="text" maxlength="6" id="hc_tabla_cxc_fiducia_'+fil+''+id+'" name="hc_tabla_cxc_fiducia_'+fil+''+id+'" value="">';
                            
                oCell=oRow.insertCell(4);
        
                oCell.align='left';
                oCell.className='fila';
                oCell.innerHTML='<input style="width: 100px" maxlength="30" type="text" id="cuenta_tabla_cxc_fiducia_'+fil+''+id+'" name="cuenta_tabla_cxc_fiducia_'+fil+''+id+'" value="" '+"onchange='validarcuenta(\"cuenta_tabla_cxc_fiducia_"+fil+""+id+"\")'>";
                
                oCell=oRow.insertCell(5);
                oCell.align='left';
                oCell.className='fila';
                oCell.innerHTML='<select id="prefijo_tabla_cxc_endoso_'+fil+''+id+'" name="prefijo_tabla_cxc_endoso_'+fil+''+id+'"><option value="">...</option>'+option+'</select> ';
                
                oCell=oRow.insertCell(6);
                oCell.align='left';
                oCell.className='fila';
                oCell.innerHTML='<input style="width: 100px" maxlength="30" type="text" id="hc_tabla_cxc_endoso_'+fil+''+id+'" name="hc_tabla_cxc_endoso_'+fil+''+id+'" value="" '+"onchange='validarcuenta(\"cuenta_tabla_cxc_fiducia_"+fil+""+id+"\")'>";
                
                oCell=oRow.insertCell(7);
                oCell.align='left';
                oCell.className='fila';
                oCell.innerHTML='<input style="width: 100px" maxlength="30" type="text" id="cuenta_tabla_cxc_endoso_'+fil+''+id+'" name="cuenta_tabla_cxc_endoso_'+fil+''+id+'" value="" '+"onchange='validarcuenta(\"cuenta_tabla_cxc_fiducia_"+fil+""+id+"\")'>";
                
                $('tamano_tabla_cxc_fiducia'+fil).value=parseInt(parseInt(id)+1);

            }

            function agregar_fiducia(tablaId){
                var id = tamano_tabla_fiducia;
                myTable = $(tablaId);
                if(navigator.appName=='Microsoft Internet Explorer')
                {   oRow = myTable.insertRow();
                }else{
                    oRow = myTable.insertRow(-1);
                }
                oCell=oRow.insertCell(0);
                oCell.align='center';
                oCell.className='fila';
                oCell.rowSpan='3';
                oCell.innerHTML="<img style='cursor: pointer' onclick='agregar_fiducia(\""+tablaId+"\");' src='<%=BASEURL%>/images/botones/iconos/mas.gif'   width='12' height='12' >"+
                    '<br/><img alt="-"style="cursor: pointer" onclick="delrow_fiducia();" src="<%=BASEURL%>/images/botones/iconos/menos1.gif" width="12" height="12" >'+
                    "<input type='hidden' name='id_tabla_cxc_"+id+"' id='id_tabla_cxc_"+id+"' value=''>";

                oCell=oRow.insertCell(1);
                oCell.align='left';
                oCell.className='fila';
                oCell.innerHTML="Fiducia";
                oCell=oRow.insertCell(2);
                oCell.align='left';
                oCell.className='fila';
                oCell.colSpan='3';
                oCell.innerHTML='<input type="text" id="nit_fiducia'+id+'" maxlength="15" name="nit_fiducia'+id+'" value="" readonly>'+
                    '<img alt="" id="nt_fiducia" src="<%=BASEURL%>/images/botones/iconos/lupa.gif" width="15" height="15"  style="cursor: pointer;"'+" onclick='openWin(\"contenido\",\"nit_fiducia"+id+"\",\"Fiducia\",\"nombre_fiducia"+id+"\");'>";


                oCell=oRow.insertCell(3);

                oCell.align='left';
                oCell.className='fila';
                oCell.innerHTML='<input type="text" id="nombre_fiducia'+id+'" style="width: 100%" name="nombre_fiducia'+id+'" value="" readonly>';
                oCell.colSpan='3';

                if(navigator.appName=='Microsoft Internet Explorer')
                {   oRow = myTable.insertRow();
                }else{
                    oRow = myTable.insertRow(-1);
                }
                oCell=oRow.insertCell(0);

                oCell.align='left';
                oCell.className='fila';
                oCell.innerHTML="&nbsp;Ingresos diferidos";

                oCell=oRow.insertCell(1);

                oCell.align='left';
                oCell.className='fila';
                oCell.innerHTML="&nbsp;Prefijo";


                oCell=oRow.insertCell(2);
                oCell.align='left';
                oCell.className='fila';

                option="";
                for(i=0;i<prefijos[0].length;i++){
                    if(prefijos[0][i]=='IP')
                        option=option+"<option value='"+prefijos[2][i]+"'>"+prefijos[1][i]+"</option>"
                }
                oCell.innerHTML='<select id="prefijo_dif_fiducia'+id+'" name="prefijo_dif_fiducia'+id+'" style="width: 45px;"><option value="">...</option>'+option+'</select>';

                oCell=oRow.insertCell(3);
                oCell.align='left';
                oCell.className='fila';
                oCell.innerHTML="&nbsp;HC";

                oCell=oRow.insertCell(4);
                oCell.align='left';
                oCell.className='fila';
                oCell.innerHTML='<input style="width: 60px" type="text" maxlength="6" id="hc_dif_fiducia'+id+'" name="hc_dif_fiducia'+id+'" value="">';

                oCell=oRow.insertCell(5);
                oCell.align='left';
                oCell.className='fila';
                oCell.innerHTML='&nbsp;Cuenta';

                oCell=oRow.insertCell(6);
                oCell.align='left';
                oCell.className='fila';
                oCell.innerHTML='<input style="width: 100%" type="text" maxlength="30" id="cuenta_dif_fiducia'+id+'" name="cuenta_dif_fiducia'+id+'" value=""'+" onchange='validarcuenta(\"cuenta_dif_fiducia"+id+"\");'>";

                if(navigator.appName=='Microsoft Internet Explorer')
                {   oRow = myTable.insertRow();
                }else{
                    oRow = myTable.insertRow(-1);
                }
                oCell=oRow.insertCell(0);
                oCell.align='left';
                oCell.className='fila';
                oCell.innerHTML="&nbsp;Endoso";

                oCell=oRow.insertCell(1);
                oCell.align='left';
                oCell.className='fila';
                oCell.innerHTML="&nbsp;Prefijo";

                oCell=oRow.insertCell(2);
                oCell.align='left';
                oCell.className='fila';

                option="";
                for(i=0;i<prefijos[0].length;i++){
                    if(prefijos[0][i]=='FAC')
                        option=option+"<option value='"+prefijos[2][i]+"'>"+prefijos[1][i]+"</option>"
                }
                oCell.innerHTML='<select id="prefijo_end_fiducia'+id+'" name="prefijo_end_fiducia'+id+'" style="width: 45px;"><option value="">...</option>'+option+'</select>';



                oCell=oRow.insertCell(3);
                oCell.align='left';
                oCell.className='fila';
                oCell.innerHTML="&nbsp;HC";

                oCell=oRow.insertCell(4);
                oCell.align='left';
                oCell.className='fila';
                oCell.innerHTML='<input style="width: 60px" maxlength="6" type="text" id="hc_end_fiducia'+id+'" name="hc_end_fiducia'+id+'" value="">';

                oCell=oRow.insertCell(5);
                oCell.align='left';
                oCell.className='fila';
                oCell.colSpan='2';
                oCell.innerHTML="&nbsp;";
                
                tamano_tabla_fiducia++;

            }

            function agregar_remesa(tablaId){
                var id = tamano_tabla_remesas;
                myTable = $(tablaId);
                if(navigator.appName=='Microsoft Internet Explorer')
                {   oRow = myTable.insertRow();
                }else{
                    oRow = myTable.insertRow(-1);
                }
                oCell=oRow.insertCell(0);
                oCell.align='center';
                oCell.className='fila';
                oCell.innerHTML="<img style='cursor: pointer' onclick='agregar_remesa(\""+tablaId+"\");' src='<%=BASEURL%>/images/botones/iconos/mas.gif'   width='12' height='12' >"+
                    "<input type='hidden' name='id_tabla_remesas_"+id+"' id='id_tabla_remesas_"+id+"' value=''>";
                oCell=oRow.insertCell(1);
                oCell.align='left';
                oCell.className='fila';
                oCell.innerHTML="<input style='width: 85%' type='text' id='ciudad_tabla_remesas_"+id+"' name='ciudad_tabla_remesas_"+id+"' readonly>"+
                    "<img  src='<%=BASEURL%>/images/botones/iconos/lupa.gif' width='15' height='15' style='cursor: pointer' onclick='openWin(\"ciudades\",\"ciudad_tabla_remesas_"+id+"\",\"Ciudad Fintravalores\");'>";
                oCell=oRow.insertCell(2);
                oCell.align='left';
                oCell.className='fila';
                oCell.innerHTML="<input style='width: 85%' type='text' id='bancoch_tabla_remesas_"+id+"' name='bancoch_tabla_remesas_"+id+"' readonly>"+
                    "<img  src='<%=BASEURL%>/images/botones/iconos/lupa.gif' width='15' height='15' style='cursor: pointer' onclick='openWin(\"bancos\",\"bancoch_tabla_remesas_"+id+"\",\"Banco Titulo\");'>";
                oCell=oRow.insertCell(3);
                oCell.align='left';
                oCell.className='fila';
                oCell.innerHTML="<input style='width: 85%' type='text' id='ciudadch_tabla_remesas_"+id+"' name='ciudadch_tabla_remesas_"+id+"' onkeyup='soloNumeros(this.id);' maxlength='4'>";
                oCell=oRow.insertCell(4);
                oCell.align='left';
                oCell.className='fila';
                oCell.innerHTML="<select id='remesa_tabla_remesas_"+id+"' name='remesa_tabla_remesas_"+id+"' onchange='deshabilitarPorcentaje("+id+")' ><option value=''>...</option><option value='true'>SI</option><option value='false'>NO</option></select>";
                oCell=oRow.insertCell(5);
                oCell.align='left';
                oCell.className='fila';
                oCell.innerHTML="<input onkeyup='soloNumeros(this.id);' style='width: 120px' type='text' id='porcentaje_tabla_remesas_"+id+"' name='porcentaje_tabla_remesas_"+id+"'>";
                oCell=oRow.insertCell(6);
                oCell.align='left';
                oCell.className='fila';
                oCell.innerHTML="<input style='width: 100%' type='text' maxlength='30' id='cuenta_tabla_remesas_"+id+"' name='cuenta_tabla_remesas_"+id+"' onchange='validarcuenta("+'"cuenta_tabla_remesas_'+id+'"'+")'>";
                tamano_tabla_remesas++;
            }
            function agregar_comision(tablaId){
                var id = tamano_tabla_comisiones;
                myTable = $(tablaId);
                if(navigator.appName=='Microsoft Internet Explorer')
                {   oRow = myTable.insertRow();
                }else{
                    oRow = myTable.insertRow(-1);
                }
                oCell=oRow.insertCell(0);
                oCell.align='center';
                oCell.className='fila';
                oCell.innerHTML="<img style='cursor: pointer' onclick='agregar_comision(\""+tablaId+"\");' src='<%=BASEURL%>/images/botones/iconos/mas.gif'   width='12' height='12' >"+
                    "<input type='hidden' name='id_tabla_comisiones_"+id+"' id='id_tabla_comisiones_"+id+"' value=''>";
                oCell=oRow.insertCell(1);
                oCell.align='left';
                oCell.className='fila';
                oCell.innerHTML="<input style='width: 90%' maxlength='20' type='text' id='nombre_tabla_comisiones_"+id+"' name='nombre_tabla_comisiones_"+id+"' value=''>";
                oCell=oRow.insertCell(2);
                oCell.align='left';
                oCell.className='fila';
                oCell.innerHTML="<input style='width: 70px' onkeyup='soloNumerosformateado(this.id);' type='text' id='porcentaje_tabla_comisiones_"+id+"' name='porcentaje_tabla_comisiones_"+id+"' value=''>";
                oCell=oRow.insertCell(3);
                oCell.align='left';
                oCell.className='fila';
                oCell.innerHTML="<input style='width: 100px' maxlength='30' type='text' id='cuenta_tabla_comisiones_"+id+"' name='cuenta_tabla_comisiones_"+id+"'value='' onchange='validarcuenta("+'"cuenta_tabla_comisiones_'+id+'"'+")'>";
                oCell=oRow.insertCell(4);
                oCell.align='left';
                oCell.className='fila';
                oCell.innerHTML="<input type='radio' name='rdoTerceroFactura' id='rdoTerceroFactura_"+id+"' value='"+id+"' onmousedown='guardarEstado("+id+")' onclick='deshabilitarIndicador("+id+")'>";
                oCell=oRow.insertCell(5);
                oCell.align='left';
                oCell.className='fila';
                oCell.innerHTML="<input type='checkbox' name='chkInd_"+id+"' id='chkInd_"+id+"' onclick='deshabilitarContrapartida("+id+")' >";
                oCell=oRow.insertCell(6);
                oCell.align='left';
                oCell.className='fila';
                oCell.innerHTML="<input type='text' style='width: 100px' maxlength='30' name='txtContrapartida_"+id+"' id='txtContrapartida_"+id+"' onchange='validarcuenta("+'"txtContrapartida_'+id+'"'+")' disabled>";

                tamano_tabla_comisiones++;
            }

            var win;
            var fila;
            var varname;
            var division;
            var divload;
            function openWin(id_div,idopener,titulo,idname){
                fila = idopener;
                division = id_div;
                varname=idname;
                win= new Window(
                {   id: "diving",
                    title: '<b>'+titulo+'</b>',
                    width:$(id_div).width,
                    height:$(id_div).heigth,
                    showEffectOptions: {duration:1},
                    hideEffectOptions: {duration:1},
                    destroyOnClose: true,
                    onClose:closewin,
                    resizable: false
                });
                win.setContent(id_div, true, true);
                win.show(true);
                //win.setLocation(7, 7);
                win.showCenter();
                //$("spangen").innerHTML = "";
                //$('contenido').innerHTML=response.responseText;
                $(id_div).style.display='block';
                $(id_div).style.visibility='visible';
                //$('contenido').style.overlay="auto";
            }

            function closewin(){
                $(division).style.display='none';
                $(division).style.visibility='hidden';
                //$(divid).innerHTML="";
            }

            function cerrarDiv(){
                win.close();
            }

            function buscarTerc(){
                var indi = $('filtro').selectedIndex;
                var filtro = $('filtro').options[indi].value;
                var cadena = $('cadena').value;
                if(cadena != ""){
                    var url = "<%= CONTROLLER%>?estado=Gestion&accion=Convenios";
                    var p =  'tipo=buscterc&filtro='+filtro+'&cadena='+cadena+'';
                    divload = "divres";
                    new Ajax.Request(
                    url,
                    {
                        method: 'post',
                        parameters: p,
                        onLoading: loading,
                        onComplete: llenarDiv
                    }
                );
                }else{
                    alert("Debe ingresar un dato para realizar la busqueda");
                }
            }

            function validarcuenta(campo){
                var cuenta = $(campo).value;
                if(cuenta != ""){
                    var url = "<%= CONTROLLER%>?estado=Gestion&accion=Convenios";
                    var p =  'tipo=buscarcuenta&cuenta='+cuenta;
                    new Ajax.Request(
                    url,
                    {
                        method: 'post',
                        parameters: p,
                        onLoading: loading,
                        onComplete: function (resp){
                            var msj=resp.responseText.replace(/^\s+/g,'').replace(/\s+$/g,'');
                            if(msj=="false"){
                                $(campo).value="";
                                alert("La cuenta digitada no existe o esta inactiva");
                            }
                        }
                    });
                }
                
            }

            function loading(){
                var interno = '<div width="100%" class="fila">Procesando <img alt="cargando" src="<%= BASEURL%>/images/cargando.gif" name="imgload"></div>';
                $(divload).innerHTML = interno;
            }

            function llenarDiv(response){
                $(divload).innerHTML = response.responseText;
            }

            function asignarValor2(idopener,valor,varname, name){
                $(idopener).value = valor;
                $(varname).value = name;
            }

            function asignarValor(idopener,valor){
                $(idopener).value = valor;
            }

            function buscarCius(){
                var indi = $('filtroc').selectedIndex;
                var filtro = $('filtroc').options[indi].value;
                var cadena = $('cadenac').value;
                var url = "<%= CONTROLLER%>?estado=Gestion&accion=Convenios";
                var p =  'tipo=busciudad&filtroc='+filtro+'&cadenac='+cadena+'';
                divload = "divresc";
                new Ajax.Request(
                url,
                {
                    method: 'post',
                    parameters: p,
                    onLoading: loading,
                    onComplete: llenarDiv
                }
            );
            }

            function buscarBan(){
                var indi = $('filtrob').selectedIndex;
                var filtro = $('filtrob').options[indi].value;
                var cadena = $('cadenab').value;
                var url = "<%= CONTROLLER%>?estado=Gestion&accion=Convenios";
                var p =  'tipo=busbanco&filtrob='+filtro+'&cadenab='+cadena+'';
                divload = "divresb";
                new Ajax.Request(
                url,
                {
                    method: 'post',
                    parameters: p,
                    onLoading: loading,
                    onComplete: llenarDiv
                }
            );
            }

            //<!-- mod -->
            function enableText(indice,texto1,texto2,texto3, texto4, texto5, texto6, texto7, texto8, texto9){
                var valor = $(texto1).options[indice].value;
                if(valor=="true"){
                    if(texto1=='redescuento'){
                        $(texto5).disabled=false;
                        $(texto6).value="";
                        $(texto6).disabled=true;


                        if($('aval_anombre').value=="true")
                        {   alert("true");
                            $('factura_tercero').value="false";
                            $('factura_tercero').disabled=true;
                        }

                    }else{
                        $(texto2).style.visibility="visible";

                        if(texto1=='factura_tercero'){
                            $(texto5).checked=false;
                            $(texto5).disabled=true;
                            $(texto6).value="";
                            $(texto6).disabled=true;
                        }
                        if(texto1=='central'){
                            $(texto5).value="";
                            $(texto6).value="";
                            $(texto7).value="";
                            $(texto8).value="";
                            $(texto9).value="";
                            $(texto5).disabled=false;
                            $(texto6).disabled=false;
                            $(texto7).disabled=false;
                            $(texto8).disabled=false;
                            $(texto9).disabled=false;
                        }
                        if(texto1=='capacitacion'){
                            $(texto5).value="";
                            $(texto6).value="";
                            $(texto5).disabled=false;
                            $(texto6).disabled=false;
                        }

                        if(texto1=='seguro'){
                            $(texto5).value="";
                            $(texto6).value="";
                            $(texto7).value="";
                            $(texto8).value="";
                            $(texto5).disabled=false;
                            $(texto6).disabled=false;
                            $(texto7).disabled=false;
                            $(texto8).disabled=false;
                        }
                        if(texto1=='aval_tercero')
                        {
                            $(texto5).disabled=true;
                            $(texto6).value="";
                            $(texto6).disabled=true;
                            $(texto7).style.visibility="hidden";
                            $(texto8).value="";
                        }
                    }
                }
                else{
                    $(texto2).style.visibility="hidden";
                    $(texto3).value="";
                    $(texto4).value="";
                    if(texto1=='redescuento'){
                        $(texto5).value=false;
                        $(texto5).disabled=true;
                        $(texto6).disabled=false;
                            
                        $('factura_tercero').value="true";
                        $('factura_tercero').disabled=false;
                
                    }
                    if(texto1=='factura_tercero'){
                        $(texto5).disabled=false;
                        $(texto6).disabled=false;
                    }
                    if(texto1=='central'){
                        $(texto5).disabled=true;
                        $(texto6).disabled=true;
                        $(texto7).disabled=true;
                        $(texto8).disabled=true;
                        $(texto9).disabled=true;
                    }
                    if(texto1=='capacitacion'){
                        $(texto5).disabled=true;
                        $(texto6).disabled=true;
                    }
                    if(texto1=='seguro'){
                        $(texto5).disabled=true;
                        $(texto6).disabled=true;
                        $(texto7).disabled=true;
                        $(texto8).disabled=true;
                    }
                    if(texto1=='aval_tercero')
                    {
                        $(texto5).disabled=false;
                        $(texto6).disabled=false;
                    }
                }
                if(texto1=='aval_anombre'){
                    if(($('redescuento').value=="true") && ($('aval_anombre').value=="true"))
                    {
                        $('factura_tercero').value="false";
                        $('factura_tercero').disabled=true;
                    }
                    else
                    {
                        $('factura_tercero').disabled=false;
                    }
                }
            }

            function guardarEstado(fila){
                radioChecked = $("rdoTerceroFactura_"+fila).checked;
            }

            /*
             * Deshabilita el indicador en las comisiones cuando se selecciona el radiobutton
             **/
            function deshabilitarIndicador(fila){
                //habilta todos
                $$("input[id^='chkInd_']").each(function(elem){
                    elem.disabled = false;
                });
                $$("input[id^='txtContrapartida_']").each(function(elem){
                    var id = (elem.id.split("_")[1]);
                    if($("chkInd_"+id).checked){
                        elem.disabled = false;
                    }
                });

                if(radioChecked){
                    $("rdoTerceroFactura_"+fila).checked=false;
                }else{
                    //limpia y deshabilita solo los de la fila del radio seleccionado
                    $("chkInd_"+fila).checked=false;
                    $("chkInd_"+fila).disabled = true;
                    $("txtContrapartida_"+fila).value="";
                    $("txtContrapartida_"+fila).disabled = true;
                }
            }
            
            /*
             * habilita o deshabilita el campo de contrapartida cuando se selecciona el indicador
             **/
            function deshabilitarContrapartida(fila){
                if($("chkInd_"+fila).checked){
                    $("txtContrapartida_"+fila).disabled = false;
                }else{
                    $("txtContrapartida_"+fila).disabled = true;
                }
            }

            /**
             * habilita o deshabilita el campo de porcentaje dependiendo de si genera remesa o no
             */
            function deshabilitarPorcentaje(fila){
                if($("remesa_tabla_remesas_"+fila).value == "true"){
                    $("porcentaje_tabla_remesas_"+fila).readOnly = false;
                }else{
                    $("porcentaje_tabla_remesas_"+fila).value = 0;
                    $("porcentaje_tabla_remesas_"+fila).readOnly = true;
                }
            }
            function deshab() {
                frm = document.forms['formulario'];
                for(i=0; ele=frm.elements[i]; i++)
                    ele.disabled=true;
            }
        
            function cxc_aval(){
                if ($('aval_tercero').value=="false" && $('redescuento').value=="false"){
                    $('prefijo_cxc_aval').disabled=false;
                    $('hc_cxc_aval').disabled=false;
                    $('cuenta_cxc_aval').disabled=false;
                    $('cctrl_db_cxc_aval').disabled=false;
                    $('cctrl_cr_cxc_aval').disabled=false;
                    $('cctrl_iva_cxc_aval').disabled=false;
                }else{
                    $('prefijo_cxc_aval').value="";
                    $('prefijo_cxc_aval').disabled=true;
                    $('hc_cxc_aval').value="";
                    $('hc_cxc_aval').disabled=true;
                    $('cuenta_cxc_aval').value="";
                    $('cuenta_cxc_aval').disabled=true;
                    $('cctrl_db_cxc_aval').value="";
                    $('cctrl_db_cxc_aval').disabled=true;
                }
            }

            function provcxc(){
                for(i=0;i<tamano_tabla_cxc;i++){
                   
                    if(($('redescuento').value=="false")){
                        $('cuenta_prov_cxc_'+i).disabled=false;
                        $('cuenta_prov_cxp_'+i).disabled=false;
                    }else{
                        $('cuenta_prov_cxc_'+i).value="";
                        $('cuenta_prov_cxc_'+i).disabled=true;
                        $('cuenta_prov_cxp_'+i).value="";
                        $('cuenta_prov_cxp_'+i).disabled=true;
                    }
                }
            }
            
            
            function cat_bloqueos(){
                if ($('cat').value=="false"){
                    $('descuenta_gmf').disabled=false;
                    $('prefijo_gmf').disabled=false;
                    $('cuota_gmf').disabled=false;
                    $('porc_gravamen').disabled=false;
                    $('cuenta_gmf').disabled=false;
                    $('porc_gravamen2').disabled=false;
                    $('cuenta_gmf2').disabled=false;
                    $('prefijo_cxc_cat').value="";
                    $('prefijo_cxc_cat').disabled=true;
                    $('cuenta_cat').value="";
                    $('cuenta_cat').disabled=true;
                    $('porcentaje_cat').value="";
                    $('porcentaje_cat').disabled=true;
                }else{
                    $('descuenta_gmf').checked=false;
                    $('descuenta_gmf').disabled=true;
                    $('prefijo_gmf').value="";
                    $('prefijo_gmf').disabled=true;
                    $('cuota_gmf').value="";
                    $('cuota_gmf').disabled=true;
                    $('porc_gravamen').value="";
                    $('porc_gravamen').disabled=true;
                    $('cuenta_gmf').value="";
                    $('cuenta_gmf').disabled=true;
                    $('porc_gravamen2').value="";
                    $('porc_gravamen2').disabled=true;
                    $('cuenta_gmf2').value="";
                    $('cuenta_gmf2').disabled=true;
                    $('prefijo_cxc_cat').disabled=false;
                    $('cuenta_cat').disabled=false;
                    $('porcentaje_cat').disabled=false;
                }
            }
            
            

        </script>
    </head>
    <%String modificar = (request.getParameter("modificar") != null) ? request.getParameter("modificar") : "";
    String tipoconv = (request.getParameter("tipoconv") != null) ? request.getParameter("tipoconv") : "";
    String tipo = (request.getParameter("tipo") != null) ? request.getParameter("tipo") : "";
    String[] dato1 = null;
%>
    <body onload="init();<%=(modificar.equals("SI")) ? "" : "deshab();"%>">
        <form action="<%=CONTROLLER%>?estado=Gestion&accion=Convenios" id="formulario" name="formulario" method="post" >

            <div id="capaSuperior" style="position:absolute; width:100%; height:100px; z-index:0; left: 0px; top: 0px;">
                <jsp:include page="/WEB-INF/toptsp.jsp?encabezado=Gestion de Convenios"/>
            </div>
            <div id="capaCentral" style="position:absolute; width:100%; height:85%; z-index:0; left: 0px; top: 100px; overflow: scroll;">
                <div id="contenido" style="width: 600px; height: 300px; display: none; visibility: hidden; background-color: #EEEEEE;">
                    <table border="0" width="100%">
                        <tr class="fila">
                            <td>Filtro</td>
                            <td>
                                <select name="filtro" id="filtro">
                                    <option value="nombre" selected>Nombre</option>
                                    <option value="cedula">Nit</option>
                                </select>
                            </td>
                            <td>Dato</td>
                            <td>
                                <input type="text" name="cadena" id="cadena" value="">
                                <img alt="" src="<%=BASEURL%>/images/botones/iconos/lupa.gif" width="15" height="15" style="cursor: pointer" onclick="buscarTerc();">
                            </td>
                        </tr>
                    </table>
                    <br>
                    <div id="divres" style="width: 100%; height: 200px; overflow: auto;"></div>
                    <br>
                    <div align="center">
                        <img alt="" src="<%=BASEURL%>/images/botones/salir.gif" style = "cursor:pointer" name = "imgsalirx" onclick = "cerrarDiv();" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);">
                    </div>
                </div>
                <div id="ciudades" style="width: 600px; height: 300px; display: none; visibility: hidden; background-color: #EEEEEE;">
                    <table border="0" width="100%">
                        <tr class="fila">
                            <td>Filtro</td>
                            <td>
                                <select name="filtro" id="filtroc">
                                    <option value="nomciu" selected>Nombre</option>
                                    <option value="codciu">Codigo</option>
                                </select>
                            </td>
                            <td>Dato</td>
                            <td>
                                <input type="text" name="cadena" id="cadenac" value="">
                                <img alt="" src="<%=BASEURL%>/images/botones/iconos/lupa.gif" width="15" height="15" style="cursor: pointer" onclick="buscarCius();">
                            </td>
                        </tr>
                    </table>
                    <br>
                    <div id="divresc" style="width: 100%; height: 200px; overflow: auto;"></div>
                    <br>
                    <div align="center">
                        <img alt="" src="<%=BASEURL%>/images/botones/salir.gif" style = "cursor:pointer" name = "imgsalirx" onclick = "cerrarDiv();" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);">
                    </div>
                </div>
                <div id="bancos" style="width: 600px; height: 300px; display: none; visibility: hidden; background-color: #EEEEEE;">
                    <table border="0" width="100%">
                        <tr class="fila">
                            <td>Filtro</td>
                            <td>
                                <select name="filtro" id="filtrob">
                                    <option value="nombre" selected>Nombre</option>
                                    <option value="codigo">Codigo</option>
                                </select>
                            </td>
                            <td>Dato</td>
                            <td>
                                <input type="text" name="cadena" id="cadenab" value="">
                                <img alt="" src="<%=BASEURL%>/images/botones/iconos/lupa.gif" width="15" height="15" style="cursor: pointer" onclick="buscarBan();">
                            </td>
                        </tr>
                    </table>
                    <br>
                    <div id="divresb" style="width: 100%; height: 200px; overflow: auto;"></div>
                    <br>
                    <div align="center">
                        <img alt="" src="<%=BASEURL%>/images/botones/salir.gif" style="cursor:pointer" name="imgsalirx" onclick="cerrarDiv();" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);">
                    </div>
                </div>
                <br>
                <center>
                    <%

                                Usuario usuario = (Usuario) session.getAttribute("Usuario");
                                Convenio convenio = (Convenio) session.getAttribute("convenio");
                                session.removeAttribute("convenio");
                                ArrayList agencias =  model.gestionConveniosSvc.getAgencias();
                                
                                ArrayList prefijos = model.gestionConveniosSvc.getPrefijos(usuario.getBd());
                                ArrayList tipo_documento = model.gestionConveniosSvc.getTipo_documento(usuario.getBd());
                                Vector impuestos = model.TimpuestoSvc.buscarImpuestoCliente("IVA", Util.getFechaActual_String(4), usuario.getDstrct(), "OP");
                                ArrayList convenioCxc, conveniosRemesas, convenioComision, convenioFiducias;
                                convenioCxc = (convenio != null) ? convenio.getConvenioCxc() : new ArrayList();
                                conveniosRemesas = (convenio != null) ? convenio.getConveniosRemesas() : new ArrayList();
                                convenioComision = (convenio != null) ? convenio.getConvenioComision() : new ArrayList();
                                convenioFiducias = (convenio != null) ? convenio.getConvenioFiducias() : new ArrayList();
                                String pref0 = "", pref1 = "", pref2 = "", td0 = "", td1 = "", fid0 = "", fid1 = "";
                                for (int i = 0; i < prefijos.size(); i++) {
                                    pref0 = pref0 + "'" + ((String[]) prefijos.get(i))[0] + "'";
                                    pref1 = pref1 + "'" + ((String[]) prefijos.get(i))[1] + "'";
                                    pref2 = pref2 + "'" + ((String[]) prefijos.get(i))[2] + "'";
                                    if (i != prefijos.size() - 1) {
                                        pref0 = pref0 + ",";
                                        pref1 = pref1 + ",";
                                        pref2 = pref2 + ",";
                                    }
                                }
                                for (int i = 0; i < tipo_documento.size(); i++) {
                                    td0 = td0 + "'" + ((String[]) tipo_documento.get(i))[0] + "'";
                                    td1 = td1 + "'" + ((String[]) tipo_documento.get(i))[1] + "'";
                                    if (i != tipo_documento.size() - 1) {
                                        td0 = td0 + ",";
                                        td1 = td1 + ",";
                                    }
                                }
                                for (int i = 0; i < convenioFiducias.size(); i++) {
                                    fid0 = fid0 + "'" + ((ConvenioFiducias) convenioFiducias.get(i)).getNit_fiducia() + "'";
                                    fid1 = fid1 + "'" + ((ConvenioFiducias) convenioFiducias.get(i)).getNombre_fiducia() + "'";
                                    if (i != convenioFiducias.size() - 1) {
                                        fid0 = fid0 + ",";
                                        fid1 = fid1 + ",";
                                    }
                                }
                                
                    %>
                    <script type="text/javascript">
                        tamano_tabla_comisiones=<%=convenioComision.size() + 1%>;
                        tamano_tabla_remesas=<%=conveniosRemesas.size() + 1%>;
                        tamano_tabla_cxc=<%=convenioCxc.size()==0?1:convenioCxc.size()%>;
                        tamano_tabla_fiducia=<%=convenioFiducias.size()==0?1:convenioFiducias.size()%>;
                        var tipo_documento= new Array ();
                        tipo_documento[0]=new Array (<%=td0%>);
                        tipo_documento[1]=new Array (<%=td1%>);
                        var fiducias= new Array ();
                        fiducias[0]=new Array (<%=fid0%>);
                        fiducias[1]=new Array (<%=fid1%>);
                        var prefijos= new Array ();
                        prefijos[0]=new Array (<%=pref0%>);
                        prefijos[1]=new Array (<%=pref1%>);
                        prefijos[2]=new Array (<%=pref2%>);
                    </script>
                    <table border="2" width="800">
                        <tr>
                            <td  >
                                <input type="hidden" id="modificar" name="modificar" value="<%=modificar%>">
                                <input type="hidden" id="tipo" name="tipo" value="">
                                <input type="hidden" id="tipoconv" name="tipoconv" value="<%=tipoconv%>">
                                <input type="hidden" id="id_convenio" name="id_convenio" value="<%=(convenio != null) ? convenio.getId_convenio() : ""%>">
                                <table class="tablaInferior" border="0" width="100%">
                                    <tr >
                                        <td width="30%" align="left" class="subtitulo1">&nbsp;Datos Generales</td>
                                        <td width="70%" align="left" class="barratitulo">
                                            <img alt="" src="<%= BASEURL%>/images/titulo.gif" width="32" height="20"  align="left">
                                        </td>
                                    </tr>
                                </table>
                                <table border="0" width="100%">
                                    <tr>
                                        <td align="left" width="25%"  class="fila">&nbsp;Nombre</td><!-- mod -->
                                        <td align="left" width="75%" colspan="2" class="fila">
                                            <input maxlength="200" style="width: 100%" type="text" id="nombre" name="nombre" value="<%=(convenio != null) ? convenio.getNombre() : ""%>">
                                        </td>
                                    </tr>
                                    <tr>
                                        <td align="left" width="25%"  class="fila" valign="top">&nbsp;Descripcion</td><!-- mod -->
                                        <td align="left" width="75%"colspan="2"  class="fila">
                                            <textarea style="width: 100%" id="descripcion" name="descripcion" rows="3"><%=(convenio != null) ? convenio.getDescripcion() : ""%></textarea>
                                        </td>
                                    </tr>
                                    <% 
    GestionConveniosService gcs = new GestionConveniosService(usuario.getBd());
    ArrayList listaente = gcs.getEntidadRedescuento();
    String entidadred ="";
  if(!tipo.equals("NUEVO") && tipoconv.equals("Consumo")){ entidadred = gcs.getRedescuentoConvenio(convenio.getId_convenio());}  
    if (tipoconv.equals("Consumo")) {%>
                                    <tr>
                                        <td align="left" width="10%"  class="fila">&nbsp;Tercero</td>
                                        <td align="left" width="10%"  class="fila">
                                            <select id="entidad" name="entidad">
                                                        <option value="">...</option>
                                                <% 
                                                String[] dato4 = null;
                                                for (int i = 0; i < listaente.size(); i++) {
                                                dato4 = ((String) listaente.get(i)).split("-"); %>
                                                <option value="<%=dato4[0] %>"<%= (convenio != null && (dato4[0].equals(entidadred))) ? "selected" : ""%> ><%= dato4[1]%></option>
                                                <%    }

                                                %>
                                                   </select> 
                                            
                                        </td>
                                    </tr>
                                    
                                    <tr>
                                        <td align="left" width="10%"  class="fila">&nbsp;Redescuento</td><!-- mod -->
                                        <td align="left" width="35%"  colspan="2" class="fila">
                                            <select id="redescuento" name="redescuento" onchange="enableText(this.selectedIndex,'redescuento','nt_convenio','nit_convenio','nombre_convenio','aval_tercero');cxc_aval();provcxc();">
                                                <option value="">...</option>
                                                <option value="true" <%=(convenio != null && convenio.isRedescuento()) ? "selected" : ""%>>SI</option>
                                                <option value="false" <%=(convenio != null && !convenio.isRedescuento()) ? "selected" : ""%>>NO</option>
                                            </select>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td align="left" width="15%" class="fila">&nbsp;Aval a traves de tercero</td><!-- mod -->
                                        <td align="left" width="25%" class="fila">
                                            <select id="aval_tercero" name="aval_tercero" onchange="enableText(this.selectedIndex,'aval_tercero','nt_convenio','nit_convenio','nombre_convenio', 'aval_anombre','nit_anombre','nt_anombre','nombre_anombre'  );cxc_aval();">
                                                <option value="">...</option>
                                                <option value="true" <%=(convenio != null && convenio.isAval_tercero()) ? "selected" : ""%>>SI</option>
                                                <option value="false" <%=(convenio != null && !convenio.isAval_tercero()) ? "selected" : ""%>>NO</option>
                                            </select>
                                            <input type="text" id="nit_convenio" maxlength="15" name="nit_convenio" value="<%=(convenio != null) ? convenio.getNit_convenio() : ""%>" readonly>
                                            <img alt="" id="nt_convenio" src="<%=BASEURL%>/images/botones/iconos/lupa.gif" width="15" height="15"  style="cursor: pointer; visibility: <%=(convenio != null && convenio.isAval_tercero()) ? "visible" : "hidden"%>;" onclick="openWin('contenido','nit_convenio','Tercero convenio','nombre_convenio');">
                                        </td>
                                        <td align="left" width="60%" class="fila">
                                            <input type="text" id="nombre_convenio" style="width: 100%" name="nombre_convenio" value="<%=(convenio != null) ? model.gestionConveniosSvc.buscarNombreTercero(convenio.getNit_convenio()) : ""%>" readonly>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td align="left" width="15%" class="fila">&nbsp;Aval a nombre de tercero</td>
                                        <td align="left" width="25%" class="fila">
                                            <select id="aval_anombre" name="aval_anombre" onchange="enableText(this.selectedIndex,'aval_anombre','nt_anombre','nit_anombre','nombre_anombre');cxc_aval();" <%=(convenio != null && !convenio.isAval_tercero()) ? "" : "disabled"%>>
                                                <option value="">...</option>
                                                <option value="true" <%=(convenio != null && convenio.isAval_anombre()) ? "selected" : ""%>>SI</option>
                                                <option value="false" <%=(convenio != null && !convenio.isAval_anombre()) ? "selected" : ""%>>NO</option>
                                            </select>
                                            <input type="text" id="nit_anombre" maxlength="15" name="nit_anombre" value="<%=(convenio != null) ? convenio.getNit_anombre() : ""%>" readonly>
                                            <img alt="" id="nt_anombre" src="<%=BASEURL%>/images/botones/iconos/lupa.gif" width="15" height="15"  style="cursor: pointer; visibility: <%=(convenio != null && convenio.isAval_anombre()) ? "visible" : "hidden"%>;" onclick="openWin('contenido','nit_anombre','Tercero a nombre','nombre_anombre');">
                                        </td>
                                        <td align="left" width="60%" class="fila">
                                            <input type="text" id="nombre_anombre" style="width: 100%" name="nombre_anombre" value="<%=(convenio != null) ? model.gestionConveniosSvc.buscarNombreTercero(convenio.getNit_anombre()) : ""%>" readonly>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td align="left" width="10%" class="fila">&nbsp;Factura a traves de tercero</td><!-- mod -->
                                        <td align="left" width="35%" class="fila">
                                            <select id="factura_tercero" name="factura_tercero" onchange="enableText(this.selectedIndex,'factura_tercero','nt_tercero','nit_tercero','nombre_tercero','descuenta_aval', 'prefijo_aval');">
                                                <option value="">...</option>
                                                <option value="true" <%=(convenio != null && convenio.isFactura_tercero()) ? "selected" : ""%>>SI</option>
                                                <option value="false" <%=(convenio != null && !convenio.isFactura_tercero()) ? "selected" : ""%>>NO</option>
                                            </select>                                       
                                            <input type="text" id="nit_tercero" maxlength="15" name="nit_tercero" value="<%=(convenio != null) ? convenio.getNit_tercero() : ""%>" readonly>
                                            <img alt="" id="nt_tercero" src="<%=BASEURL%>/images/botones/iconos/lupa.gif" width="15" height="15" style="cursor: pointer; visibility: <%=(convenio != null && convenio.isFactura_tercero()) ? "visible" : "hidden"%>;" onclick="openWin('contenido','nit_tercero','Tercero que factura','nombre_tercero');">
                                        </td>
                                        <td align="left" width="55%" class="fila">
                                            <input type="text" id="nombre_tercero" style="width: 100%" name="nombre_tercero" value="<%=(convenio != null) ? model.gestionConveniosSvc.buscarNombreTercero(convenio.getNit_tercero()) : ""%>" readonly>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td align="left" width="10%"  class="fila">&nbsp;Intermediario Aval</td><!-- mod -->
                                        <td align="left" width="35%"  class="fila">
                                            <select id="mediador_aval" name="mediador_aval" onchange="enableText(this.selectedIndex,'mediador_aval','nt_mediador','nit_mediador','nombre_mediador');">
                                                <option value="">...</option>
                                                <option value="true" <%=(convenio != null && convenio.isMediador_aval()) ? "selected" : ""%>>SI</option>
                                                <option value="false" <%=(convenio != null && !convenio.isMediador_aval()) ? "selected" : ""%>>NO</option>
                                            </select>
                                            <input type="text" id="nit_mediador" maxlength="15" name="nit_mediador" value="<%=(convenio != null) ? convenio.getNit_mediador() : ""%>" readonly>
                                            <img alt="" id="nt_mediador" src="<%=BASEURL%>/images/botones/iconos/lupa.gif" width="15" height="15" style="cursor: pointer; visibility: <%=(convenio != null && convenio.isMediador_aval()) ? "visible" : "hidden"%>;" onclick="openWin('contenido','nit_mediador','Intermediario Aval','nombre_mediador');">
                                        </td>
                                        <td align="left" width="55%" class="fila">
                                            <input type="text" id="nombre_mediador" style="width: 100%" name="nombre_mediador" value="<%=(convenio != null) ? model.gestionConveniosSvc.buscarNombreTercero(convenio.getNit_mediador()) : ""%>" readonly>
                                        </td>
                                    </tr>
                                    <%}
                                                if (tipoconv.equals("Microcredito")) {%>
                                    <tr>
                                        <td class="fila">
                                            <label>&nbsp;Agencia</label>
                                        </td>
                                        <td>
         
                                            <select id="agencia" name="agencia">
                                                <option value="">...</option>
                                                <%for (int i = 0; i < agencias.size(); i++) { %>
                                                    <option value="<%=((String[]) agencias.get(i))[0]%>" <%=(convenio != null && (((String[]) agencias.get(i))[0].equals(convenio.getAgencia()))) ? "selected" : ""%>><%=((String[]) agencias.get(i))[1]%></option>
                                                <% }%>
                                                
                                            </select>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td align="left" width="10%"  class="fila">&nbsp;Ley Mipyme</td><!-- mod -->
                                        <td align="left" width="35%"  colspan="2" class="fila">
                                            <select id="cat" name="cat" onchange="cat_bloqueos();">
                                                <option value="">...</option>
                                                <option value="true" <%=(convenio != null && convenio.isCat()) ? "selected" : ""%>>SI</option>
                                                <option value="false" <%=(convenio != null && !convenio.isCat()) ? "selected" : ""%>>NO</option>
                                            </select>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td align="left" width="10%"  class="fila">&nbsp;Central Riesgo</td><!-- mod -->
                                        <td align="left" width="35%"  class="fila">
                                            <select id="central" name="central" onchange="enableText(this.selectedIndex,'central','nt_central','nit_central','nombre_central','prefijo_cxp_central', 'valor_central', 'cuenta_central', 'valor_com_central', 'cuenta_com_central');">
                                                <option value="">...</option>
                                                <option value="true" <%=(convenio != null && convenio.isCentral()) ? "selected" : ""%>>SI</option>
                                                <option value="false" <%=(convenio != null && !convenio.isCentral()) ? "selected" : ""%>>NO</option>
                                            </select>
                                            <input type="text" id="nit_central" maxlength="15" name="nit_central" value="<%=(convenio != null) ? convenio.getNit_central() : ""%>" readonly>
                                            <img alt="" id="nt_central" src="<%=BASEURL%>/images/botones/iconos/lupa.gif" width="15" height="15" style="cursor: pointer; visibility: <%=(convenio != null && convenio.isCentral()) ? "visible" : "hidden"%>;" onclick="openWin('contenido','nit_central','Central de Riesgo','nombre_central');">
                                        </td>
                                        <td align="left" width="55%" class="fila">
                                            <input type="text" id="nombre_central" style="width: 100%" name="nombre_central" value="<%=(convenio != null) ? model.gestionConveniosSvc.buscarNombreTercero(convenio.getNit_central()) : ""%>" readonly>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td align="left" width="10%"  class="fila">&nbsp;Capacitaci&oacute;n</td><!-- mod -->
                                        <td align="left" width="35%"  class="fila">
                                            <select id="capacitacion" name="capacitacion" onchange="enableText(this.selectedIndex,'capacitacion','nt_capacitador','nit_capacitador','nombre_capacitador', 'valor_capacitacion', 'cuenta_capacitacion');">
                                                <option value="">...</option>
                                                <option value="true" <%=(convenio != null && convenio.isCapacitacion()) ? "selected" : ""%>>SI</option>
                                                <option value="false" <%=(convenio != null && !convenio.isCapacitacion()) ? "selected" : ""%>>NO</option>
                                            </select>
                                            <input type="text" id="nit_capacitador" maxlength="15" name="nit_capacitador" value="<%=(convenio != null) ? convenio.getNit_capacitador() : ""%>" readonly>
                                            <img alt="" id="nt_capacitador" src="<%=BASEURL%>/images/botones/iconos/lupa.gif" width="15" height="15" style="cursor: pointer; visibility: <%=(convenio != null && convenio.isCapacitacion()) ? "visible" : "hidden"%>;" onclick="openWin('contenido','nit_capacitador','Capacitacion','nombre_capacitador');">
                                        </td>
                                        <td align="left" width="55%" class="fila">
                                            <input type="text" id="nombre_capacitador" style="width: 100%" name="nombre_capacitador" value="<%=(convenio != null) ? model.gestionConveniosSvc.buscarNombreTercero(convenio.getNit_capacitador()) : ""%>" readonly>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td align="left" width="10%"  class="fila">&nbsp;Seguro</td><!-- mod -->
                                        <td align="left" width="35%"  class="fila">
                                            <select id="seguro" name="seguro" onchange="enableText(this.selectedIndex,'seguro','nt_asegurador','nit_asegurador','nombre_asegurador', 'valor_seguro', 'cuenta_seguro', 'porcentaje_com_seguro', 'cuenta_com_seguro');">
                                                <option value="">...</option>
                                                <option value="true" <%=(convenio != null && convenio.isSeguro()) ? "selected" : ""%>>SI</option>
                                                <option value="false" <%=(convenio != null && !convenio.isSeguro()) ? "selected" : ""%>>NO</option>
                                            </select>
                                            <input type="text" id="nit_asegurador" maxlength="15" name="nit_asegurador" value="<%=(convenio != null) ? convenio.getNit_asegurador() : ""%>" readonly>
                                            <img alt="" id="nt_asegurador" src="<%=BASEURL%>/images/botones/iconos/lupa.gif" width="15" height="15" style="cursor: pointer; visibility: <%=(convenio != null && convenio.isSeguro()) ? "visible" : "hidden"%>;" onclick="openWin('contenido','nit_asegurador','seguro','nombre_asegurador');">
                                        </td>
                                        <td align="left" width="55%" class="fila">
                                            <input type="text" id="nombre_asegurador" style="width: 100%" name="nombre_asegurador" value="<%=(convenio != null) ? model.gestionConveniosSvc.buscarNombreTercero(convenio.getNit_asegurador()) : ""%>" readonly>
                                        </td>
                                    </tr>
                                    <%}%>
                                </table>
                                <table class="tablaInferior" border="0" width="100%">
                                    <tr >
                                        <td width="30%" align="left" class="subtitulo1">&nbsp;Condiciones</td><!-- mod -->
                                        <td width="70%" align="left" class="barratitulo">
                                            <img alt="" src="<%= BASEURL%>/images/titulo.gif" width="32" height="20"  align="left">
                                        </td>
                                    </tr>
                                </table>
                                <table border="0" width="100%">
                                    <tr>
                                        <td align="left" width="16%" <%=tipoconv.equals("Consumo") ? "colspan='3'" : ""%>  class="fila">&nbsp;Inter�s</td>
                                        <%if (tipoconv.equals("Microcredito")) {%>
                                        <td align="left" width="11%"  class="fila">&nbsp;Prefijo CxC</td>
                                        <td align="left" width="5%" class="fila">
                                            <select id="prefijo_cxc_interes"  name="prefijo_cxc_interes" style="width: 45px;">
                                                <option value="">...</option>
                                                <%for (int i = 0; i < prefijos.size(); i++) {
                                                        if (((String[]) prefijos.get(i))[0].equals("FAC")) {%>
                                                <option value="<%=((String[]) prefijos.get(i))[2]%>" <%=(convenio != null && (((String[]) prefijos.get(i))[2].equals(convenio.getPrefijo_cxc_interes()))) ? "selected" : ""%>><%=((String[]) prefijos.get(i))[1]%></option>
                                                <%  }
                                                    }%>
                                            </select>                                        </td>
                                            <%}%>
                                        <td align="left" width="13%"  class="fila">&nbsp;Tasa</td>
                                        <td align="left" width="13%" class="fila">
                                            <input onkeyup="soloNumerosformateado(this.id);" style="width: 70px" type="text" id="tasa_interes" name="tasa_interes" value="<%=(convenio != null) ? convenio.getTasa_interes() : ""%>">&nbsp;

                                        </td>
                                        <td align="left" width="13%"  class="fila">&nbsp;Cuenta</td><!-- mod -->
                                        <td align="left" width="12%" class="fila">
                                            <input type="text" id="cuenta_interes" maxlength="30" name="cuenta_interes" style="width: 100%"  value="<%=(convenio != null) ? convenio.getCuenta_interes() : ""%>" onchange="validarcuenta('cuenta_interes')">

                                        </td>
                                    </tr>
                                    <% if (tipoconv.equals("Microcredito")) {%>
                                    <tr>
                                        <td align="left" width="16%" class="fila">&nbsp;Central riesgo</td>
                                        <td align="left" width="11%" class="fila">&nbsp;Prefijo CxP </td>
                                        <td align="left" width="5%" class="fila">
                                            <select id="prefijo_cxp_central" name="prefijo_cxp_central" <%=(convenio != null && convenio.isCentral()) ? "" : "disabled"%> style="width: 45px;">
                                                <option value="">...</option>
                                                <%for (int i = 0; i < prefijos.size(); i++) {
                                                         if (((String[]) prefijos.get(i))[0].equals("FAP")) {%>
                                                <option value="<%=((String[]) prefijos.get(i))[2]%>" <%=(convenio != null && (((String[]) prefijos.get(i))[2].equals(convenio.getPrefijo_cxp_central()))) ? "selected" : ""%>><%=((String[]) prefijos.get(i))[1]%></option>
                                                <%  }
                                                     }%>
                                            </select>                                        </td>
                                        <td align="left"  width="13%" class="fila">&nbsp;Valor </td>
                                        <td align="left" width="13%"class="fila">
                                            <input onkeyup="soloNumerosformateado(this.id);" style="width: 70px" type="text" id="valor_central" name="valor_central" value="<%=(convenio != null) ? convenio.getValor_central() : ""%>" <%=(convenio != null && convenio.isCentral()) ? "" : "disabled"%>>&nbsp;

                                        </td>
                                        <td align="left" width="13%" class="fila">&nbsp;Cuenta</td><!-- mod -->
                                        <td align="left" width="14%" class="fila">
                                            <input type="text" id="cuenta_central" maxlength="30" name="cuenta_central" style="width: 100%" value="<%=(convenio != null) ? convenio.getCuenta_central() : ""%>" onchange="validarcuenta('cuenta_central')" <%=(convenio != null && convenio.isCentral()) ? "" : "disabled"%>>

                                        </td>
                                    </tr>
                                    <tr>
                                        <td align="left" colspan="3"   class="fila">&nbsp;Comisi�n central de riesgo</td>

                                        <td align="left" width="13%" class="fila">&nbsp;Valor </td>
                                        <td align="left" width="13%" class="fila">
                                            <input onkeyup="soloNumerosformateado(this.id);" style="width: 70px" type="text" id="valor_com_central" name="valor_com_central" value="<%=(convenio != null) ? convenio.getValor_com_central() : ""%>" <%=(convenio != null && convenio.isCentral()) ? "" : "disabled"%>>&nbsp;

                                        </td>
                                        <td align="left" width="13%" class="fila">&nbsp;Cuenta</td><!-- mod -->
                                        <td align="left" width="14%" class="fila">
                                            <input type="text" id="cuenta_com_central" maxlength="30" name="cuenta_com_central" style="width: 100%" value="<%=(convenio != null) ? convenio.getCuenta_com_central() : ""%>" onchange="validarcuenta('cuenta_com_central')" <%=(convenio != null && convenio.isCentral()) ? "" : "disabled"%>>

                                        </td>
                                    </tr>
                                    <tr>
                                        <td align="left" width="16%" class="fila">&nbsp;Comisi�n ley mipyme</td>
                                        <td align="left" width="11%"  class="fila">&nbsp;Prefijo CxC </td>
                                        <td align="left" width="5%" class="fila">
                                            <select id="prefijo_cxc_cat" name="prefijo_cxc_cat" style="width: 45px;"  <%=(convenio != null && convenio.isCat()) ? "" : "disabled"%>>
                                                <option value="">...</option>
                                                <%for (int i = 0; i < prefijos.size(); i++) {
                                                         if (((String[]) prefijos.get(i))[0].equals("FAC")) {%>
                                                <option value="<%=((String[]) prefijos.get(i))[2]%>" <%=(convenio != null && (((String[]) prefijos.get(i))[2].equals(convenio.getPrefijo_cxc_cat()))) ? "selected" : ""%>><%=((String[]) prefijos.get(i))[1]%></option>
                                                <%  }
                                                     }%>
                                            </select>                                        </td>
                                        <td align="left" width="13%"  class="fila">&nbsp;Porcentaje </td>
                                        <td align="left" width="13%" class="fila">
                                            <input onkeyup="soloNumerosformateado(this.id);" style="width: 70px" type="text" id="porcentaje_cat" name="porcentaje_cat" value="<%=(convenio != null) ? convenio.getPorcentaje_cat() : ""%>" <%=(convenio != null && convenio.isCat()) ? "" : "disabled"%> &nbsp;

                                        </td>
                                        <td align="left" width="13%" class="fila">&nbsp;Cuenta</td><!-- mod -->
                                        <td align="left" width="14%" class="fila">
                                            <input type="text" id="cuenta_cat" maxlength="30" name="cuenta_cat" style="width: 100%" value="<%=(convenio != null) ? convenio.getCuenta_cat() : ""%>" onchange="validarcuenta('cuenta_cat')" <%=(convenio != null && convenio.isCat()) ? "" : "disabled"%>

                                        </td>
                                    </tr>
                                    <tr>
                                        <td align="left"  colspan="3"  class="fila">&nbsp;Capacitaci&oacute;n</td>

                                        <td align="left" width="13%" class="fila">&nbsp;Valor </td>
                                        <td align="left" width="13%" class="fila">
                                            <input onkeyup="soloNumerosformateado(this.id);" style="width: 70px" type="text" id="valor_capacitacion" name="valor_capacitacion" value="<%=(convenio != null) ? convenio.getValor_capacitacion() : ""%>" <%=(convenio != null && convenio.isCapacitacion()) ? "" : "disabled"%>>&nbsp;

                                        </td>
                                        <td align="left" width="13%" class="fila">&nbsp;Cuenta</td><!-- mod -->
                                        <td align="left" width="14%" class="fila">
                                            <input type="text" id="cuenta_capacitacion" maxlength="30" name="cuenta_capacitacion" style="width: 100%" value="<%=(convenio != null) ? convenio.getCuenta_capacitacion() : ""%>" onchange="validarcuenta('cuenta_capacitacion')" <%=(convenio != null && convenio.isCapacitacion()) ? "" : "disabled"%>>

                                        </td>
                                    </tr>
                                    <tr>
                                        <td align="left"  colspan="3"  class="fila">&nbsp;Seguro</td>
                                        <td align="left" width="13%"   class="fila">&nbsp;Valor </td>
                                        <td align="left" width="13%" class="fila">
                                            <input onkeyup="soloNumerosformateado(this.id);" style="width: 70px" type="text" id="valor_seguro" name="valor_seguro" value="<%=(convenio != null) ? convenio.getValor_seguro() : ""%>" <%=(convenio != null && convenio.isSeguro()) ? "" : "disabled"%>>&nbsp;

                                        </td>
                                        <td align="left" width="13%" class="fila">&nbsp;Cuenta</td><!-- mod -->
                                        <td align="left" width="14%" class="fila">
                                            <input type="text" id="cuenta_seguro" maxlength="30" name="cuenta_seguro" style="width: 100%" value="<%=(convenio != null) ? convenio.getCuenta_seguro() : ""%>" onchange="validarcuenta('cuenta_seguro')" <%=(convenio != null && convenio.isSeguro()) ? "" : "disabled"%>>

                                        </td>
                                    </tr>
                                    <tr>
                                        <td align="left"  colspan="3"  class="fila">&nbsp;Comisi�n seguro</td>
                                        <td align="left" width="13%"  class="fila">&nbsp;Porcentaje</td>
                                        <td align="left" width="13%"class="fila">
                                            <input onkeyup="soloNumerosformateado(this.id);" style="width: 70px" type="text" id="porcentaje_com_seguro" name="porcentaje_com_seguro" value="<%=(convenio != null) ? convenio.getPorcentaje_com_seguro() : ""%>" <%=(convenio != null && convenio.isSeguro()) ? "" : "disabled"%>>&nbsp;

                                        </td>
                                        <td align="left" width="13%" class="fila">&nbsp;Cuenta</td><!-- mod -->
                                        <td align="left" width="14%" class="fila">
                                            <input type="text" id="cuenta_com_seguro" maxlength="30" name="cuenta_com_seguro" style="width: 100%" value="<%=(convenio != null) ? convenio.getCuenta_com_seguro() : ""%>" onchange="validarcuenta('cuenta_com_seguro')" <%=(convenio != null && convenio.isSeguro()) ? "" : "disabled"%>>

                                        </td>
                                    </tr>
<!--AQUI-->
                                    <tr>
                                        <%}
                                                    if (tipoconv.equals("Consumo")) {%>
                                    <tr>
                                        <td align="left" colspan="3" class="fila">&nbsp;Custodia</td>
                                        <td align="left"width="13%"  class="fila">&nbsp;Valor</td><!-- mod -->
                                        <td align="left" width="13%" class="fila">
                                            <input onkeyup="soloNumerosformateado(this.id);" style="width: 70px" type="text" id="valor_custodia" name="valor_custodia" value="<%=(convenio != null) ? convenio.getValor_custodia() : ""%>">&nbsp;
                                        </td>
                                        <td align="left" width="13%"  class="fila">&nbsp;Cuenta</td><!-- mod -->
                                        <td align="left" width="12%"  class="fila">
                                            <input type="text" id="cuenta_custodia" maxlength="30" name="cuenta_custodia" style="width: 100%"  value="<%=(convenio != null) ? convenio.getCuenta_custodia() : ""%>" onchange="validarcuenta('cuenta_custodia')" >

                                        </td>
                                    </tr>
<!--AQUI-->
                                    <%}%>
                                    <tr>
                                        <td align="left" colspan="" class="fila">&nbsp;Cuota Administracion</td>
                                        <td colspan="" class="fila">&nbsp;Prefijo </td>
                                        <td>
                                            <select type="text" id="prefijo_cuota_admin" maxlength="30" name="prefijo_cuota_admin" style="width: 70px">
                                                <option value="">...</option>
                                                <%for (int i = 0; i < prefijos.size(); i++) {
                                                        if (((String[]) prefijos.get(i))[0].equals("CM")) {%>
                                                <option value="<%=((String[]) prefijos.get(i))[2]%>" <%=(convenio != null && (((String[]) prefijos.get(i))[2].equals(convenio.getPrefijo_cuota_admin()))) ? "selected" : ""%>><%=((String[]) prefijos.get(i))[1]%></option>
                                                <%  }
                                                    }%>
                                            </select>
                                        </td>
                                        <td colspan="" class="fila">&nbsp;HC </td>
                                        <td>
                                            <input type="text" id="hc_cuota_admin" maxlength="30" name="hc_cuota_admin" style="width: 70px" value="<%=(convenio != null) ? convenio.getHc_cuota_admin() : ""%>">
                                        </td>
                                        <td colspan="" class="fila">&nbsp;Cuenta</td>
                                        <td>
                                            <input type="text" id="cuenta_administracion" maxlength="30" name="cuenta_administracion" style="width: 100%"  value="<%=(convenio != null) ? convenio.getCuenta_cuota_administracion() : ""%>" onchange="validarcuenta('cuenta_administracion')">
                                        </td>
                                    </tr>
                                    <tr>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                        <td align="left" colspan="2" class="fila" style="text-align: right;">&nbsp;Cta cuota admin Diferido</td>
                                        <td>
                                            <input type="text" id="cta_cuota_admin_diferido" maxlength="30" name="cta_cuota_admin_diferido" style="width: 100%"  value="<%=(convenio != null) ? convenio.getCta_cuota_admin_diferido() : ""%>" onchange="validarcuenta('cuenta_administracion')">
                                        </td>
                                    </tr>
                                </table>

                                <table class="tablaInferior" border="0" width="100%">
                                    <tr >
                                        <td width="30%" align="left" class="subtitulo1">&nbsp;Documentos generados</td><!-- mod -->
                                        <td width="70%" align="left" class="barratitulo">
                                            <img alt="" src="<%= BASEURL%>/images/titulo.gif" width="32" height="20"  align="left">
                                        </td>
                                    </tr>
                                </table>
                                <table border="0" width="100%">
                                    <tr>
                                        <td align="left" width="16%" class="fila">&nbsp;Negocio</td>
                                        <td width="11%" align="left" class="fila">&nbsp;Prefijo </td><!-- mod -->
                                        <td width="5%" align="left" class="fila">
                                            <select id="prefijo_negocio" name="prefijo_negocio" style="width: 45px;">
                                                <option value="">...</option>
                                                <%for (int i = 0; i < prefijos.size(); i++) {
                                                                if (((String[]) prefijos.get(i))[0].equals("NEG")) {%>
                                                <option value="<%=((String[]) prefijos.get(i))[2]%>" <%=(convenio != null && (((String[]) prefijos.get(i))[2].equals(convenio.getPrefijo_negocio()))) ? "selected" : ""%>><%=((String[]) prefijos.get(i))[1]%></option>
                                                <%  }
                                                            }%>
                                            </select>
                                        </td>
                                        <td width="13%" align="left" class="fila">&nbsp;Impuesto</td>
                                        <td width="13%" align="left" class="fila">
                                            <select name="impuesto" id="impuesto" style="width: 110px;">
                                                <option value="">...</option>
                                                <%
                                                            for (int i = 0; i < impuestos.size(); i++) {
                                                                Tipo_impuesto imp = (Tipo_impuesto) impuestos.get(i);
                                                                if (!imp.getDescripcion().equals("")) {
                                                %>
                                                <option value="<%=imp.getCodigo_impuesto()%>" <%=(convenio != null && (imp.getCodigo_impuesto().equals(convenio.getImpuesto()))) ? "selected" : ""%> ><%=imp.getDescripcion()%></option>
                                                <%  }
                                                            }%>
                                            </select>
                                        </td>
                                        <td width="13%" align="left" class="fila">&nbsp;Cuenta ajuste</td>
                                        <td width="12%" align="left" class="fila"><input name="cuenta_ajuste" type="text" id="cuenta_ajuste" maxlength="30" style="width: 100%" value="<%=(convenio != null) ? convenio.getCuenta_ajuste() : ""%>" onchange="validarcuenta('cuenta_ajuste')"></td>
                                    </tr>
                                    <tr>
                                        <td align="left" width="16%" class="fila">&nbsp;Desembolso</td>
                                        <td align="left" class="fila">&nbsp;Prefijo CXP</td><!-- mod -->
                                        <td align="left" class="fila">
                                            <select id="prefijo_cxp" name="prefijo_cxp" style="width: 45px;">
                                                <option value="">...</option>
                                                <%for (int i = 0; i < prefijos.size(); i++) {
                                                                if (((String[]) prefijos.get(i))[0].equals("FAP")) {%>
                                                <option value="<%=((String[]) prefijos.get(i))[2]%>" <%=(convenio != null && (((String[]) prefijos.get(i))[2].equals(convenio.getPrefijo_cxp()))) ? "selected" : ""%>><%=((String[]) prefijos.get(i))[1]%></option>
                                                <%  }
                                                            }%>
                                            </select>                                        </td>
                                        <td align="left" class="fila">&nbsp;HC</td><!-- mod -->
                                        <td align="left" class="fila"><input style="width: 60px" maxlength="6" type="text" id="hc_cxp" name="hc_cxp" value="<%=(convenio != null) ? convenio.getHc_cxp() : ""%>"></td>
                                        <td align="left" class="fila">&nbsp;Cuenta</td><!-- mod -->
                                        <td align="left" class="fila"><input style="width: 100%" type="text" maxlength="30" id="cuenta_cxp" name="cuenta_cxp" value="<%=(convenio != null) ? convenio.getCuenta_cxp() : ""%>" onchange="validarcuenta('cuenta_cxp')"></td>
                                    </tr>                                   
                                    <% if (tipoconv.equals("Consumo")) {%>
                                 
                                    <tr>
                                        <td align="left" width="16%" class="fila">&nbsp;Aval</td>
                                        <td align="left" class="fila">&nbsp;Prefijo CXC</td>
                                        <td align="left" class="fila">
                                            <select id="prefijo_cxc_aval" name="prefijo_cxc_aval" style="width: 45px;" <%=((convenio != null) && !convenio.isAval_tercero() && !convenio.isRedescuento()) ? "" : "disabled"%>>
                                                <option value="">...</option>
                                                <%for (int i = 0; i < prefijos.size(); i++) {
                                                         if (((String[]) prefijos.get(i))[0].equals("FAC")) {%>
                                                <option value="<%=((String[]) prefijos.get(i))[2]%>" <%=(convenio != null && (((String[]) prefijos.get(i))[2].equals(convenio.getPrefijo_cxc_aval()))) ? "selected" : ""%>><%=((String[]) prefijos.get(i))[1]%></option>
                                                <%  }
                                                     }%>
                                            </select>                                        </td>
                                        <td align="left" class="fila">&nbsp;HC</td>
                                        <td align="left" class="fila"><input style="width: 60px" maxlength="6" type="text" id="hc_cxc_aval" name="hc_cxc_aval" value="<%=(convenio != null) ? convenio.getHc_cxc_aval() : ""%>" <%=((convenio != null) && !convenio.isAval_tercero() && !convenio.isRedescuento()) ? "" : "disabled"%>></td>
                                        <td align="left" class="fila">&nbsp;Cuenta</td>
                                        <td align="left" class="fila"><input style="width: 100%" type="text" maxlength="30" id="cuenta_cxc_aval" name="cuenta_cxc_aval" value="<%=(convenio != null) ? convenio.getCuenta_cxc_aval() : ""%>" onchange="validarcuenta('cuenta_cxc_aval')" <%=((convenio != null) && !convenio.isAval_tercero() && !convenio.isRedescuento()) ? "" : "disabled"%>></td>
                                    </tr>
                                    <tr>
                                        <td align="left" width="16%" class="fila">&nbsp;</td>
                                        <td align="left" class="fila" rowspan="2" colspan="2">&nbsp;Contigencia</td>
                                        <td align="left" class="fila">&nbsp;Cuenta DB</td>
                                        <td align="left" class="fila"><input style="width: 100%" type="text" maxlength="30" id="cctrl_db_cxc_aval" name="cctrl_db_cxc_aval" value="<%=(convenio != null) ? convenio.getCctrl_db_cxc_aval() : ""%>" onchange="validarcuenta('cctrl_db_cxc_aval')" <%=((convenio != null) && !convenio.isAval_tercero() && !convenio.isRedescuento()) ? "" : "disabled"%>></td>
                                        <td align="left" class="fila">&nbsp;Cuenta CR</td>
                                        <td align="left" class="fila"><input style="width: 100%" type="text" maxlength="30" id="cctrl_cr_cxc_aval" name="cctrl_cr_cxc_aval" value="<%=(convenio != null) ? convenio.getCctrl_cr_cxc_aval() : ""%>" onchange="validarcuenta('cctrl_cr_cxc_aval')" <%=((convenio != null) && !convenio.isAval_tercero() && !convenio.isRedescuento()) ? "" : "disabled"%>></td>
                                    </tr>
                                    <tr>
                                        <td align="left" width="16%" class="fila">&nbsp;</td>
                                        <td align="left" colspan="2" class="fila">&nbsp;</td>
                                        <td align="left" class="fila">&nbsp;Cuenta IVA</td>
                                        <td align="left" class="fila"><input style="width: 100%" type="text" maxlength="30" id="cctrl_iva_cxc_aval" name="cctrl_iva_cxc_aval" value="<%=(convenio != null) ? convenio.getCctrl_iva_cxc_aval() : ""%>" onchange="validarcuenta('cctrl_iva_cxc_aval')" <%=((convenio != null) && !convenio.isAval_tercero() && !convenio.isRedescuento()) ? "" : "disabled"%>></td>

                                    </tr>
                                    
                                    <tr>
                                        <td align="left" class="fila">
                                            <table>
                                                <tr>
                                                    <td class="fila">
                                                        <input type="checkbox" id="cxp_avalista" name="cxp_avalista" <%=(convenio != null && convenio.isCxp_avalista()) ? "checked" : ""%>>
                                                    </td>
                                                    <td class="fila">CxP Avalista</td>
                                                </tr>
                                            </table>
                                        </td>
                                        <td align="left" class="fila">&nbsp;Prefijo</td>
                                        <td align="left" class="fila">
                                            <select id="prefijo_cxp_avalista" name="prefijo_cxp_avalista" style="width: 45px;" >
                                                <option value="">...</option>
                                                <%for (int i = 0; i < prefijos.size(); i++) {
                                                         if (((String[]) prefijos.get(i))[0].equals("FAP")) {%>
                                                <option value="<%=((String[]) prefijos.get(i))[2]%>" <%=(convenio != null && (((String[]) prefijos.get(i))[2].equals(convenio.getPrefijo_cxp_avalista()))) ? "selected" : ""%>><%=((String[]) prefijos.get(i))[1]%></option>
                                                <%  }
                                                     }%>
                                            </select>                                        </td>
                                        <td align="left" class="fila">&nbsp;HC</td>
                                        <td align="left" class="fila">&nbsp;<input style="width: 100%" type="text" maxlength="30" id="hc_cxp_avalista" name="hc_cxp_avalista" value="<%=(convenio != null) ? convenio.getHc_cxp_avalista() : ""%>"></td>
                                        <td align="left" class="fila">&nbsp;Cuenta</td>
                                        <td align="left" class="fila"><input style="width: 100%" type="text" maxlength="30" id="cuenta_cxp_avalista" name="cuenta_cxp_avalista" value="<%=(convenio != null) ? convenio.getCuenta_cxp_avalista() : ""%>" onchange="validarcuenta('cuenta_cxp_avalista')" ></td>
                                    </tr>
                                    <%}%>
                                    <tr>
                                        <td align="left" class="fila"  rowspan="2">
                                            <table>
                                                <tr>
                                                    <td class="fila">
                                                        <input type="checkbox" id="descuenta_gmf" name="descuenta_gmf" <%=(convenio != null && convenio.isDescuenta_gmf()) ? "checked" : ""%> <%=tipoconv.equals("Microcredito") && convenio != null && convenio.isCat() ? "disabled" : ""%> >
                                                    </td>
                                                    <td class="fila">Descuenta grav mvto financiero
                                                    </td>
                                                </tr>
                                            </table>
                                        </td>
                                        <td align="left" class="fila"rowspan="2" >&nbsp;Prefijo</td>
                                        <td align="left" class="fila" rowspan="2">
                                            <select id="prefijo_gmf" name="prefijo_gmf" style="width: 45px;" <%=tipoconv.equals("Microcredito") && convenio != null && convenio.isCat() ? "disabled" : ""%>>
                                                <option value="">...</option>
                                                <%for (int i = 0; i < prefijos.size(); i++) {
                                                                if (((String[]) prefijos.get(i))[0].equals("NC")) {%>
                                                <option value="<%=((String[]) prefijos.get(i))[2]%>" <%=(convenio != null && (((String[]) prefijos.get(i))[2].equals(convenio.getPrefijo_nc_gmf()))) ? "selected" : ""%>><%=((String[]) prefijos.get(i))[1]%></option>
                                                <%  }
                                                            }%>
                                            </select>                                        </td>
                                        <td align="left" class="fila" rowspan="2">
                                            Cuota
                                            <input id="cuota_gmf" maxlength="2" name="cuota_gmf" type="text" onkeyup="soloNumerosformateado(this.id);" style="width: 45px" value="<%=(convenio != null) ? convenio.getCuota_gmf() : ""%>" <%=tipoconv.equals("Microcredito") && convenio != null && convenio.isCat() ? "disabled" : ""%>>                                        </td><!-- mod -->
                                        <td align="left" class="fila">
                                            <input type="text" id="porc_gravamen" name="porc_gravamen" style="width: 60px" value="<%=(convenio != null) ? convenio.getPorc_gmf() : ""%>" onkeyup="soloNumerosformateado(this.id);" <%=tipoconv.equals("Microcredito") && convenio != null && convenio.isCat() ? "disabled" : ""%>>
                                            %                                        </td>
                                        <td align="left" class="fila">&nbsp;Cuenta</td><!-- mod -->
                                        <td align="left" class="fila"><input style="width: 100%" type="text" maxlength="30" id="cuenta_gmf" name="cuenta_gmf" value="<%=(convenio != null) ? convenio.getCuenta_gmf() : ""%>" onchange="validarcuenta('cuenta_gmf')" <%=tipoconv.equals("Microcredito") && convenio != null && convenio.isCat() ? "disabled" : ""%>></td>
                                    </tr>
                                    <tr>                                       
                                        <td align="left" class="fila">
                                            <input type="text" id="porc_gravamen2" name="porc_gravamen2" style="width: 60px" value="<%=(convenio != null) ? convenio.getPorc_gmf2() : ""%>" onkeyup="soloNumerosformateado(this.id);" <%=tipoconv.equals("Microcredito") && convenio != null && convenio.isCat() ? "disabled" : ""%>>
                                            %                                        </td>
                                        <td align="left" class="fila">&nbsp;Cuenta</td><!-- mod -->
                                        <td align="left" class="fila"><input style="width: 100%" type="text" maxlength="30" id="cuenta_gmf2" name="cuenta_gmf2" value="<%=(convenio != null) ? convenio.getCuenta_gmf2() : ""%>" onchange="validarcuenta('cuenta_gmf2')" <%=tipoconv.equals("Microcredito") && convenio != null && convenio.isCat() ? "disabled" : ""%>></td>
                                    </tr>
                                    <% if (tipoconv.equals("Consumo")) {%>
                                    <tr>
                                        <td align="left" class="fila">
                                            <table>
                                                <tr>
                                                    <td class="fila">
                                                        <input type="checkbox" id="descuenta_aval" name="descuenta_aval" <%=(convenio != null && !convenio.isFactura_tercero() && convenio.isDescuenta_aval()) ? "checked" : ""%> <%=(convenio != null && convenio.isFactura_tercero()) ? "disabled" : ""%>>
                                                    </td>
                                                    <td class="fila">Descuenta aval</td>
                                                </tr>
                                            </table>
                                        </td>
                                        <td align="left" class="fila">&nbsp;Prefijo</td>
                                        <td align="left" class="fila">
                                            <select id="prefijo_aval" name="prefijo_aval" style="width: 45px;" <%=(convenio != null && convenio.isFactura_tercero()) ? "disabled" : ""%>>
                                                <option value="">...</option>
                                                <%for (int i = 0; i < prefijos.size(); i++) {
                                                         if (((String[]) prefijos.get(i))[0].equals("NC")) {%>
                                                <option value="<%=((String[]) prefijos.get(i))[2]%>" <%=(convenio != null && (((String[]) prefijos.get(i))[2].equals(convenio.getPrefijo_nc_aval()))) ? "selected" : ""%>><%=((String[]) prefijos.get(i))[1]%></option>
                                                <%  }
                                                     }%>
                                            </select>                                        </td>
                                        <td align="left" class="fila">&nbsp;</td>
                                        <td align="left" class="fila">&nbsp;</td>
                                        <td align="left" class="fila">&nbsp;Cuenta</td><!-- mod -->
                                        <td align="left" class="fila"><input style="width: 100%" type="text" maxlength="30" id="cuenta_aval" name="cuenta_aval" value="<%=(convenio != null) ? convenio.getCuenta_aval() : ""%>" onchange="validarcuenta('cuenta_aval')"></td>
                                    </tr>
                                    
                                    <tr>
                                        <td align="left" width="16%" class="fila">&nbsp;Ingresos diferidos</td>
                                        <td align="left" class="fila">&nbsp;Prefijo </td><!-- mod -->
                                        <td align="left" class="fila">
                                            <select id="prefijo_diferidos" name="prefijo_diferidos" style="width: 45px;">
                                                <option value="">...</option>
                                                <%for (int i = 0; i < prefijos.size(); i++) {
                                                         if (((String[]) prefijos.get(i))[0].equals("IF")) {%>
                                                <option value="<%=((String[]) prefijos.get(i))[2]%>" <%=(convenio != null && (((String[]) prefijos.get(i))[2].equals(convenio.getPrefijo_diferidos()))) ? "selected" : ""%>><%=((String[]) prefijos.get(i))[1]%></option>
                                                <%  }
                                                     }%>
                                            </select>                                        </td>
                                        <td align="left" class="fila">&nbsp;HC</td><!-- mod -->
                                        <td align="left" class="fila"><input style="width: 60px" type="text" maxlength="6" id="hc_diferidos" name="hc_diferidos" value="<%=(convenio != null) ? convenio.getHc_diferidos() : ""%>"></td>
                                        <td align="left" class="fila">&nbsp;Cuenta</td><!-- mod -->
                                        <td align="left" class="fila"><input style="width: 100%" type="text" maxlength="30" id="cuenta_diferidos" name="cuenta_diferidos" value="<%=(convenio != null) ? convenio.getCuenta_diferidos() : ""%>" onchange="validarcuenta('cuenta_diferidos')"></td>
                                    </tr>
                                    <tr>
                                        <td align="left" width="16%" class="fila">&nbsp;Endoso</td>
                                        <td align="left" class="fila">&nbsp;Prefijo</td><!-- mod -->
                                        <td align="left" class="fila">
                                            <select id="prefijo_endoso" name="prefijo_endoso" style="width: 45px;">
                                                <option value="">...</option>
                                                <%for (int i = 0; i < prefijos.size(); i++) {
                                                         if (((String[]) prefijos.get(i))[0].equals("FAC")) {%>
                                                <option value="<%=((String[]) prefijos.get(i))[2]%>" <%=(convenio != null && (((String[]) prefijos.get(i))[2].equals(convenio.getPrefijo_endoso()))) ? "selected" : ""%>><%=((String[]) prefijos.get(i))[1]%></option>
                                                <%  }
                                                     }%>
                                            </select>                                        </td>
                                        <td align="left" class="fila">&nbsp;HC</td>
                                        <td align="left" class="fila"><input style="width: 60px" maxlength="6" type="text" id="hc_endoso" name="hc_endoso" value="<%=(convenio != null) ? convenio.getHc_endoso() : ""%>"></td>
                                        <td align="left" class="fila" colspan="2">&nbsp;</td>
                                    </tr>
                                    <%}%>
                                </table>
                                <% if (tipoconv.equals("Consumo")) {%>
                                <table class="tablaInferior" border="0" width="100%">
                                    <tr>
                                        <td width="30%" align="left" class="subtitulo1">&nbsp;Fiducias</td>
                                        <td width="70%" align="left" class="barratitulo">
                                            <img alt="" src="<%= BASEURL%>/images/titulo.gif" width="32" height="20"  align="left">
                                            <input type="hidden" name="tamano_tabla_fiducia" id="tamano_tabla_fiducia" value="<%=convenioFiducias.size()%>">
                                        </td>
                                    </tr>
                                </table>
                                <table id="tabla_fiducias" border="0" width="100%">
                                    <% if (convenioFiducias.size() > 0) {
                                             for (int j = 0; j < convenioFiducias.size(); j++) {%>
                                    <tr>
                                        <td align="left" width="2%" class="fila" rowspan="3" >
                                            <img alt="" style="cursor: pointer;" onclick="agregar_fiducia('tabla_fiducias');" src='<%=BASEURL%>/images/botones/iconos/mas.gif'   width='12' height='12' >
                                            <%if (j != 0) {%>
                                            <br/>
                                            <img alt="-"style="cursor: pointer" onclick="delrow_fiducia();" src="<%=BASEURL%>/images/botones/iconos/menos1.gif" width="12" height="12" >
                                            <%}%></td>
                                        <td align="left" width="13%" class="fila">&nbsp;Fiducia</td>
                                        <td align="left" colspan="3" width="25%" class="fila">
                                            <input type="text" id="nit_fiducia<%=j%>" maxlength="15" name="nit_fiducia<%=j%>" value="<%=((ConvenioFiducias) convenioFiducias.get(j)).getNit_fiducia()%>" readonly>
                                            <img alt="" id="nt_fiducia" src="<%=BASEURL%>/images/botones/iconos/lupa.gif" width="15" height="15"  style="cursor: pointer;" onclick="openWin('contenido','nit_fiducia<%=j%>','Fiducia','nombre_fiducia<%=j%>');">
                                        </td>
                                        <td align="left" colspan="3" width="60%" class="fila">
                                            <input type="text" id="nombre_fiducia<%=j%>" style="width: 100%" name="nombre_fiducia<%=j%>" value="<%=((ConvenioFiducias) convenioFiducias.get(j)).getNombre_fiducia()%>" readonly>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td align="left" width="14%" class="fila">&nbsp;Ingresos diferidos</td>
                                        <td align="left" class="fila" width="11%">&nbsp;Prefijo</td><!-- mod -->
                                        <td align="left" class="fila" width="5%">
                                            <select id="prefijo_dif_fiducia<%=j%>" name="prefijo_dif_fiducia<%=j%>" style="width: 45px;">
                                                <option value="">...</option>
                                                <%for (int i = 0; i < prefijos.size(); i++) {
                                                                                                     if (((String[]) prefijos.get(i))[0].equals("IP")) {%>
                                                <option value="<%=((String[]) prefijos.get(i))[2]%>" <%=(convenio != null && (((String[]) prefijos.get(i))[2].equals(((ConvenioFiducias) convenioFiducias.get(j)).getPrefijo_dif_fiducia()))) ? "selected" : ""%>><%=((String[]) prefijos.get(i))[1]%></option>
                                                <%  }
                                                                                                 }%>
                                            </select>                                        </td>
                                        <td align="left" class="fila" width="13%" >&nbsp;HC</td><!-- mod -->
                                        <td align="left" class="fila" width="13%"><input style="width: 60px" type="text" maxlength="6" id="hc_dif_fiducia<%=j%>" name="hc_dif_fiducia<%=j%>" value="<%=((ConvenioFiducias) convenioFiducias.get(j)).getHc_dif_fiducia()%>"></td>
                                        <td align="left" class="fila"  width="13%">&nbsp;Cuenta</td><!-- mod -->
                                        <td align="left" class="fila"  width="12%"><input style="width: 100%" type="text" maxlength="30" id="cuenta_dif_fiducia<%=j%>" name="cuenta_dif_fiducia<%=j%>" value="<%=((ConvenioFiducias) convenioFiducias.get(j)).getCuenta_dif_fiducia()%>" onchange="validarcuenta('cuenta_dif_fiducia<%=j%>')"></td>
                                    </tr>
                                    <tr>
                                        <td align="left" width="14%" class="fila">&nbsp;Endoso</td>
                                        <td align="left" class="fila">&nbsp;Prefijo</td><!-- mod -->
                                        <td align="left" class="fila">
                                            <select id="prefijo_end_fiducia<%=j%>" name="prefijo_end_fiducia<%=j%>" style="width: 45px;">
                                                <option value="">...</option>
                                                <%for (int i = 0; i < prefijos.size(); i++) {
                                                                                                     if (((String[]) prefijos.get(i))[0].equals("FAC")) {%>
                                                <option value="<%=((String[]) prefijos.get(i))[2]%>" <%=(convenio != null && (((String[]) prefijos.get(i))[2].equals(((ConvenioFiducias) convenioFiducias.get(j)).getPrefijo_end_fiducia()))) ? "selected" : ""%>><%=((String[]) prefijos.get(i))[1]%></option>
                                                <%  }
                                                                                                 }%>
                                            </select>                                        </td>
                                        <td align="left" class="fila">&nbsp;HC</td>
                                        <td align="left" class="fila"><input style="width: 60px" maxlength="6" type="text" id="hc_end_fiducia<%=j%>" name="hc_end_fiducia<%=j%>" value="<%=((ConvenioFiducias) convenioFiducias.get(j)).getHc_end_fiducia()%>"></td>
                                        <td align="left" class="fila" colspan="2">&nbsp;</td>
                                    </tr>
                                    <%}
                                     } else {%>
                                    <tr>
                                        <td align="left" width="2%" class="fila" rowspan="3" > <img alt="" style="cursor: pointer;" onclick="agregar_fiducia('tabla_fiducias');" src='<%=BASEURL%>/images/botones/iconos/mas.gif'   width='12' height='12' ></td>

                                        <td align="left" width="13%" class="fila">&nbsp;Fiducia</td>

                                        <td align="left" colspan="3" width="25%" class="fila">

                                            <input type="text" id="nit_fiducia0" maxlength="15" name="nit_fiducia0" value="" readonly>

                                            <img alt="" id="nt_fiducia" src="<%=BASEURL%>/images/botones/iconos/lupa.gif" width="15" height="15"  style="cursor: pointer;" onclick="openWin('contenido','nit_fiducia0','Fiducia','nombre_fiducia0');">
                                        </td>
                                        <td align="left" colspan="3" width="60%" class="fila">
                                            <input type="text" id="nombre_fiducia0" style="width: 100%" name="nombre_fiducia0" value="" readonly>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td align="left" width="14%" class="fila">&nbsp;Ingresos diferidos</td>
                                        <td align="left" class="fila" width="11%">&nbsp;Prefijo</td><!-- mod -->
                                        <td align="left" class="fila" width="5%">
                                            <select id="prefijo_dif_fiducia0" name="prefijo_dif_fiducia0" style="width: 45px;">
                                                <option value="">...</option>
                                                <%for (int i = 0; i < prefijos.size(); i++) {
                                                         if (((String[]) prefijos.get(i))[0].equals("IP")) {%>
                                                <option value="<%=((String[]) prefijos.get(i))[2]%>" ><%=((String[]) prefijos.get(i))[1]%></option>
                                                <%  }
                                                     }%>
                                            </select>                                        </td>
                                        <td align="left" class="fila" width="13%" >&nbsp;HC</td><!-- mod -->
                                        <td align="left" class="fila" width="13%"><input style="width: 60px" type="text" maxlength="6" id="hc_dif_fiducia0" name="hc_dif_fiducia0" value=""></td>
                                        <td align="left" class="fila"  width="13%">&nbsp;Cuenta</td><!-- mod -->
                                        <td align="left" class="fila"  width="12%"><input style="width: 100%" type="text" maxlength="30" id="cuenta_dif_fiducia0" name="cuenta_dif_fiducia0" value="" onchange="validarcuenta('cuenta_dif_fiducia0')"></td>
                                    </tr>
                                    <tr>
                                        <td align="left" width="14%" class="fila">&nbsp;Endoso</td>
                                        <td align="left" class="fila">&nbsp;Prefijo</td><!-- mod -->
                                        <td align="left" class="fila">
                                            <select id="prefijo_end_fiducia0" name="prefijo_end_fiducia0" style="width: 45px;">
                                                <option value="">...</option>
                                                <%for (int i = 0; i < prefijos.size(); i++) {
                                                         if (((String[]) prefijos.get(i))[0].equals("FAC")) {%>
                                                <option value="<%=((String[]) prefijos.get(i))[2]%>"><%=((String[]) prefijos.get(i))[1]%></option>
                                                <%  }
                                                     }%>
                                            </select>                                        </td>
                                        <td align="left" class="fila">&nbsp;HC</td>
                                        <td align="left" class="fila"><input style="width: 60px" maxlength="6" type="text" id="hc_end_fiducia0" name="hc_end_fiducia0" value=""></td>
                                        <td align="left" class="fila" colspan="2">&nbsp;</td>
                                    </tr>
                                </table>
                                <%}
                                            }%>
                                <% if (tipoconv.equals("Microcredito")) {%>
                                <table class="tablaInferior" border="0" width="100%">
                                    <tr >
                                        <td width="30%" align="left" class="subtitulo1">&nbsp;Criterios</td>
                                        <td width="70%" align="left" class="barratitulo">
                                            <img alt="" src="<%= BASEURL%>/images/titulo.gif" width="32" height="20"  align="left">
                                        </td>
                                    </tr>
                                </table>
                                <table border="0" width="100%">
                                    <tr>
                                        <td align="left" class="fila" >&nbsp;Plazo m�ximo (meses)</td><!-- mod -->
                                        <td align="left" class="fila"  ><input  type="text" maxlength="6" size ="6" id="plazo_maximo" name="plazo_maximo" value="<%=(convenio != null) ? convenio.getPlazo_maximo() : ""%>" onkeyup="soloNumerosformateado(this.id);"></td>

                                        <td align="left" class="fila" >&nbsp;Monto m�nimo (SMLV)</td><!-- mod -->
                                        <td align="left" class="fila" ><input type="text" maxlength="3" id="monto_minimo" size ="6" name="monto_minimo" value="<%=(convenio != null) ? convenio.getMonto_minimo() : ""%>" onkeyup="soloNumerosformateado(this.id);"></td>

                                        <td align="left" class="fila" >&nbsp;Monto m�ximo (SMLV)</td><!-- mod -->
                                        <td align="left" class="fila"  ><input  type="text" maxlength="3" id="monto_maximo" size ="6" name="monto_maximo" value="<%=(convenio != null) ? convenio.getMonto_maximo() : ""%>" onkeyup="soloNumerosformateado(this.id);"></td>
                                    </tr>
                                </table>
                                <%}%>
                                <table class="tablaInferior" border="0" width="100%">
                                    <tr >
                                        <td nowrap id="op1" style="cursor: pointer; background-color: green" align="left" class="subtitulo1" onclick="this.style.backgroundColor='green';$('op2').style.backgroundColor='#03a70c';$('op4').style.backgroundColor='#03a70c';$('divfacturas').style.visibility='visible';$('divremesas').style.visibility='hidden';$('divcomisiones').style.visibility='hidden';$('divfacturas').style.display='block';$('divremesas').style.display='none';$('divcomisiones').style.display='none';">&nbsp;Titulos Valores</td>
                                        <% if (tipoconv.equals("Consumo")) {%>
                                        <td nowrap id="op2" style="cursor: pointer" align="left" class="subtitulo1" onclick="this.style.backgroundColor='green';$('op1').style.backgroundColor='#03a70c';$('op4').style.backgroundColor='#03a70c';$('divremesas').style.visibility='visible';$('divfacturas').style.visibility='hidden';$('divcomisiones').style.visibility='hidden';$('divremesas').style.display='block';$('divfacturas').style.display='none';$('divcomisiones').style.display='none';">&nbsp;Remesas</td>
                                        <td nowrap id="op4" style="cursor: pointer" align="left" class="subtitulo1" onclick="this.style.backgroundColor='green';$('op2').style.backgroundColor='#03a70c';$('op1').style.backgroundColor='#03a70c';$('divcomisiones').style.visibility='visible';$('divremesas').style.visibility='hidden';$('divfacturas').style.visibility='hidden';$('divcomisiones').style.display='block';$('divremesas').style.display='none';$('divfacturas').style.display='none';">&nbsp;Comisiones</td>
                                        <td nowrap id="op5" width="50%" align="left" class="barratitulo"></td>
                                        <%}%>
                                    </tr>
                                </table>
                                <div id="divfacturas" class="fixedHeaderTable">
                                    <table border="0" width="100%" id="tabla_cxc">
                                        <thead>
                                            <tr>
                                                <td align="left"  width="3%" class="fila">&nbsp;<input type="hidden" name="tamano_tabla_cxc" id="tamano_tabla_cxc" value="<%=convenioCxc.size()%>"></td>
                                                <td align="left"  class="fila">Tipo Titulo Valor</td><!-- mod -->
                                                <% if (tipoconv.equals("Consumo")) {%>
                                                <td align="left"  class="fila">Remesa</td>
                                                <%}%>
                                                <td align="left"  class="fila">Prefijo CXC</td><!-- mod -->
                                                <td align="left"  class="fila">HC</td><!-- mod -->
                                                <td align="left" class="fila">Cuenta</td><!-- mod -->
                                                <% if (tipoconv.equals("Consumo")) {%>
                                                <td align="center"  class="fila">Cuenta Provicional CxC</td>
                                                 <td align="center" class="fila">&nbsp;Cuenta Provicional CXP</td>
                                                 <%}%>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <%if (convenioCxc.size() > 0) {
                                                            for (int i = 0; i < convenioCxc.size(); i++) {%>
                                            <tr>
                                                <td align="center" <%= (tipoconv.equals("Consumo"))?"rowspan='2'":""%> class="fila">
                                                    <img alt="" style="cursor: pointer;" onclick="agregar_cxc('tabla_cxc','<%=tipoconv%>');" src='<%=BASEURL%>/images/botones/iconos/mas.gif'   width='12' height='12' >
                                                    <%if (i != 0) {%>
                                                    <br/>
                                                    <img alt="-"style="cursor: pointer" onclick="delrow_cxc('<%=tipoconv%>');" src="<%=BASEURL%>/images/botones/iconos/menos1.gif" width="12" height="12" >
                                                    <%}%>
                                                </td>
                                                <td align="left" class="fila">
                                                    <select id="tipo_documento_tabla_cxc_<%=i%>" name="tipo_documento_tabla_cxc_<%=i%>">
                                                        <option value="">...</option>
                                                        <%for (int j = 0; j < tipo_documento.size(); j++) {%>
                                                        <option value="<%=((String[]) tipo_documento.get(j))[0]%>" <%=(((String[]) tipo_documento.get(j))[0].equals(((ConvenioCxc) convenioCxc.get(i)).getTitulo_valor())) ? "selected" : ""%>><%=((String[]) tipo_documento.get(j))[1]%></option>
                                                        <%                                                                                                                            }%>
                                                    </select>
                                                </td>
                                                <% if (tipoconv.equals("Consumo")) {%>
                                                <td align="left" class="fila">
                                                    <select id="gen_remesa_<%=i%>" name="gen_remesa_<%=i%>">
                                                        <option value="">...</option>
                                                        <option value="true" <%=((ConvenioCxc) convenioCxc.get(i)).isGenRemesa() ? "selected" : ""%> >SI</option>
                                                        <option value="false"<%=((ConvenioCxc) convenioCxc.get(i)).isGenRemesa() ? "" : "selected"%>>NO</option>
                                                    </select>
                                                </td>
                                                <%}%>
                                                <td align="left" class="fila">
                                                    <select id="prefijo_tabla_cxc_<%=i%>" name="prefijo_tabla_cxc_<%=i%>">
                                                        <option value="">...</option>
                                                        <%for (int j = 0; j < prefijos.size(); j++) {
                                                                                                                            if (((String[]) prefijos.get(j))[0].equals("FAC")) {%>
                                                        <option value="<%=((String[]) prefijos.get(j))[2]%>" <%=((String[]) prefijos.get(j))[2].equals(((ConvenioCxc) convenioCxc.get(i)).getPrefijo_factura()) ? "selected" : ""%>><%=((String[]) prefijos.get(j))[1]%></option>
                                                        <%  }
                                                                                                                        }%>
                                                    </select>
                                                </td>
                                                <td align="left" class="fila">
                                                    <input style="width: 60px" type="text" maxlength="6" id="hc_tabla_cxc_<%=i%>" name="hc_tabla_cxc_<%=i%>" value="<%=((ConvenioCxc) convenioCxc.get(i)).getHc_cxc()%>">
                                                </td>
                                                <td align="left" class="fila">
                                                    <input style="width: 100px" maxlength="30" type="text" id="cuenta_tabla_cxc_<%=i%>" name="cuenta_tabla_cxc_<%=i%>" value="<%=((ConvenioCxc) convenioCxc.get(i)).getCuenta_cxc()%>" onchange="validarcuenta('cuenta_tabla_cxc_<%=i%>')">
                                                </td>
                                                <% if (tipoconv.equals("Consumo")) {%>
                                                <td align="left" class="fila">
                                                    <input style="width: 100px" maxlength="30" type="text" id="cuenta_prov_cxc_<%=i%>" name="cuenta_prov_cxc_<%=i%>" value="<%=((ConvenioCxc) convenioCxc.get(i)).getCuenta_prov_cxc()%>" onchange="validarcuenta('cuenta_prov_cxc_<%=i%>')" <%=(convenio != null && convenio.isRedescuento()) ? "disabled" : ""%>>
                                                </td>
                                                <td align="left" class="fila">
                                                    <input style="width: 100px" type="text" maxlength="30" id="cuenta_prov_cxp_<%=i%>" name="cuenta_prov_cxp_<%=i%>" value="<%=((ConvenioCxc) convenioCxc.get(i)).getCuenta_prov_cxp() != null ? ((ConvenioCxc) convenioCxc.get(i)).getCuenta_prov_cxp() : ""%>" onchange="validarcuenta('cuenta_prov_cxp_<%=i%>')" <%=((convenio != null) && !convenio.isRedescuento()) ? "" : "disabled"%>>
                                                </td>
                                                <%}%>
                                            </tr>
                                            <% if (tipoconv.equals("Consumo")) {%>
                                            <tr>
                                                <td align="left"  colspan="7">
                                                    <table border="1" width="100%" id="tabla_cxc_fiducia<%=i%>" cellspacing="0" >
                                                        <tr style="height: 20px;">
                                                            <td align="left"  width="5%" class="fila">&nbsp;<input type="hidden" name="tamano_tabla_cxc_fiducia<%=i%>" id="tamano_tabla_cxc_fiducia<%=i%>" value="<%=(((ConvenioCxc) convenioCxc.get(i)).getConvenioCxcFiducias().size())==0?1:(((ConvenioCxc) convenioCxc.get(i)).getConvenioCxcFiducias().size())%>"></td>
                                                            <td align="left"  class="fila">Fiducia</td>
                                                            <td align="left"  class="fila">Prefijo cxc</td>
                                                            <td align="left" class="fila">Hc cxc</td>
                                                            <td align="left" class="fila">Cuenta cxc</td>
                                                            <!-- nuevas columnas -->
                                                            <td align="left"  class="fila">Prefijo cxc endoso</td>
                                                            <td align="left" class="fila">Hc cxc endoso</td>
                                                            <td align="left" class="fila">Cuenta cxc endoso</td>
                                                            <!-- fin nuevas columnas -->
                                                        </tr>
                                                        <%if ((((ConvenioCxc) convenioCxc.get(i)).getConvenioCxcFiducias().size()) > 0) {
                                                                 for (int k = 0; k < (((ConvenioCxc) convenioCxc.get(i)).getConvenioCxcFiducias().size()); k++) {%>
                                                        <tr>
                                                            <td  align="center" class="fila"><img alt="" style="cursor: pointer;" onclick="agregar_cxc_fiducia('tabla_cxc_fiducia<%=i%>', '<%=i%>');" src='<%=BASEURL%>/images/botones/iconos/mas.gif'   width='12' height='12' >
                                                                <%if (k != 0) {%>
                                                                <br/>
                                                                <img alt="-"style="cursor: pointer" onclick="delrow_cxc_fiducia('<%=i%>');" src="<%=BASEURL%>/images/botones/iconos/menos1.gif" width="12" height="12" >
                                                                <%}%></td>
                                                            <td align="left" class="fila"><select id="nit_fiducia_tabla_cxc_<%=i + "" + k%>" name="nit_fiducia_tabla_cxc_<%=i + "" + k%>">
                                                                    <option value="">...</option>
                                                                    <%for (int j = 0; j < convenioFiducias.size(); j++) {%>
                                                                    <option value="<%=((ConvenioFiducias) convenioFiducias.get(j)).getNit_fiducia()%>" <%=(((ConvenioFiducias) convenioFiducias.get(j)).getNit_fiducia().equals(((ConvenioCxc) convenioCxc.get(i)).getConvenioCxcFiducias().get(k).getNit_fiducia())) ? "selected" : ""%>><%=(((ConvenioFiducias) convenioFiducias.get(j)).getNombre_fiducia())%></option>
                                                                    <%} %>
                                                                </select></td>
                                                            <td align="left" class="fila">
                                                                <select id="prefijo_tabla_cxc_fiducia_<%=i + "" + k%>" name="prefijo_tabla_cxc_fiducia_<%=i + "" + k%>">
                                                                    <option value="">...</option>
                                                                    <%for (int j = 0; j < prefijos.size(); j++) {
                                                                                                                                             if (((String[]) prefijos.get(j))[0].equals("FAC")) {%>
                                                                    <option value="<%=((String[]) prefijos.get(j))[2]%>" <%=((String[]) prefijos.get(j))[2].equals(((ConvenioCxc) convenioCxc.get(i)).getConvenioCxcFiducias().get(k).getPrefijo_cxc_fiducia()) ? "selected" : ""%> ><%=((String[]) prefijos.get(j))[1]%></option>
                                                                    <%}
                                                                                                                                         }%>
                                                                </select>
                                                            </td>
                                                            <td align="left" class="fila">
                                                                <input style="width: 60px" type="text" maxlength="6" id="hc_tabla_cxc_fiducia_<%=i + "" + k%>" name="hc_tabla_cxc_fiducia_<%=i + "" + k%>" value="<%=((ConvenioCxc) convenioCxc.get(i)).getConvenioCxcFiducias().get(k).getHc_cxc_fiducia()%>">
                                                            </td>
                                                            <td align="left" class="fila">
                                                                <input style="width: 100px" maxlength="30" type="text" id="cuenta_tabla_cxc_fiducia_<%=i + "" + k%>" name="cuenta_tabla_cxc_fiducia_<%=i + "" + k%>" value="<%=((ConvenioCxc) convenioCxc.get(i)).getConvenioCxcFiducias().get(k).getCuenta_cxc_fiducia()%>" onchange="validarcuenta('cuenta_tabla_cxc_fiducia_<%=i + "" + k%>')">
                                                            </td>
                                                            <!-- nuevos campos -->
                                                            <td align="left" class="fila">
                                                                <select id="prefijo_tabla_cxc_endoso_<%=i + "" + k%>" name="prefijo_tabla_cxc_endoso_<%=i + "" + k%>">
                                                                    <option value="">...</option>
                                                                    <%for (int j = 0; j < prefijos.size(); j++) {
                                                                            if (((String[]) prefijos.get(j))[0].equals("FAC")) {%>
                                                                    <option value="<%=((String[]) prefijos.get(j))[2]%>" <%=((String[]) prefijos.get(j))[2].equals(((ConvenioCxc) convenioCxc.get(i)).getConvenioCxcFiducias().get(k).getPrefijo_cxc_endoso()) ? "selected" : ""%> ><%=((String[]) prefijos.get(j))[1]%></option>
                                                                    <%}
                                                                        }%>
                                                                </select>
                                                            </td>
                                                            <td align="left" class="fila">
                                                                <input style="width: 100px" maxlength="30" type="text" id="hc_tabla_cxc_endoso_<%=i + "" + k%>" name="hc_tabla_cxc_endoso_<%=i + "" + k%>" value="<%=((ConvenioCxc) convenioCxc.get(i)).getConvenioCxcFiducias().get(k).getHc_cxc_endoso()%>" onchange="validarcuenta('cuenta_tabla_cxc_fiducia_<%=i + "" + k%>')">
                                                            </td>
                                                            <td align="left" class="fila">
                                                                <input style="width: 100px" maxlength="30" type="text" id="cuenta_tabla_cxc_endoso_<%=i + "" + k%>" name="cuenta_tabla_cxc_endoso_<%=i + "" + k%>" value="<%=((ConvenioCxc) convenioCxc.get(i)).getConvenioCxcFiducias().get(k).getCuenta_cxc_endoso()%>" onchange="validarcuenta('cuenta_tabla_cxc_fiducia_<%=i + "" + k%>')">
                                                            </td>
                                                            <!-- fin nuevos campos -->
                                                        </tr>
                                                        <%}
                                                        } else {%>
                                                        <tr>
                                                            <td  align="center" class="fila"><img alt="" style="cursor: pointer;" onclick="agregar_cxc_fiducia('tabla_cxc_fiducia<%=i%>', '<%=i%>');" src='<%=BASEURL%>/images/botones/iconos/mas.gif'   width='12' height='12' >
                                                            </td>
                                                            <td align="left" class="fila"><select id="nit_fiducia_tabla_cxc_<%=i + "0"%>" name="nit_fiducia_tabla_cxc_<%=i + "0"%>">
                                                                    <option value="">...</option>
                                                                    <%for (int j = 0; j < convenioFiducias.size(); j++) {%>
                                                                    <option value="<%=((ConvenioFiducias) convenioFiducias.get(j)).getNit_fiducia()%>"><%=(((ConvenioFiducias) convenioFiducias.get(j)).getNombre_fiducia())%></option>
                                                                    <%}%>
                                                                </select></td>
                                                            <td align="left" class="fila">
                                                                <select id="prefijo_tabla_cxc_fiducia_<%=i + "0"%>" name="prefijo_tabla_cxc_fiducia_<%=i + "0"%>">
                                                                    <option value="">...</option>
                                                                    <%for (int j = 0; j < prefijos.size(); j++) {
                                                                            if (((String[]) prefijos.get(j))[0].equals("FAC")) {%>
                                                                    <option value="<%=((String[]) prefijos.get(j))[2]%>" ><%=((String[]) prefijos.get(j))[1]%></option>
                                                                    <%  }
                                                                        }%>
                                                                </select>
                                                            </td>
                                                            <td align="left" class="fila">
                                                                <input style="width: 60px" type="text" maxlength="6" id="hc_tabla_cxc_fiducia_<%=i + "0"%>" name="hc_tabla_cxc_fiducia_<%=i + "0"%>" value="">
                                                            </td>
                                                            <td align="left" class="fila">
                                                                <input style="width: 100px" maxlength="30" type="text" id="cuenta_tabla_cxc_fiducia_<%=i + "0"%>" name="cuenta_tabla_cxc_fiducia_<%=i + "0"%>" value="" onchange="validarcuenta('cuenta_tabla_cxc_fiducia_<%=i + "0"%>')">
                                                            </td>
                                                            <!-- nuevos campos -->
                                                            <td align="left" class="fila">
                                                                <select id="prefijo_tabla_cxc_endoso_<%=i + "" + "0"%>" name="prefijo_tabla_cxc_endoso_<%=i + "" + "0"%>">
                                                                    <option value="">...</option>
                                                                    <%for (int j = 0; j < prefijos.size(); j++) {
                                                                            if (((String[]) prefijos.get(j))[0].equals("FAC")) {%>
                                                                    <option value="<%=((String[]) prefijos.get(j))[2]%>" ><%=((String[]) prefijos.get(j))[1]%></option>
                                                                    <%}
                                                                        }%>
                                                                </select>
                                                            </td>
                                                            <td align="left" class="fila">
                                                                <input style="width: 100px" maxlength="30" type="text" id="hc_tabla_cxc_endoso_<%=i + "0"%>" name="hc_tabla_cxc_endoso_<%=i + "0"%>" value="" onchange="validarcuenta('cuenta_tabla_cxc_fiducia_<%=i + "0"%>')">
                                                            </td>
                                                            <td align="left" class="fila">
                                                                <input style="width: 100px" maxlength="30" type="text" id="cuenta_tabla_cxc_endoso_<%=i + "0"%>" name="cuenta_tabla_cxc_endoso_<%=i + "0"%>" value="" onchange="validarcuenta('cuenta_tabla_cxc_fiducia_<%=i + "0"%>')">
                                                            </td>
                                                            <!-- fin nuevos campos -->
                                                        </tr>
                                                        <%}%>
                                                    </table>
                                                </td>
                                            </tr>
                                            <%
                                                    }
                                                }
                                            } else {%>
                                            <tr>
                                                <td rowspan="2" align="center" class="fila">
                                                    <input type="hidden" name="id_tabla_cxc_0" id="id_tabla_cxc_0" value="">
                                                    <img alt="" style="cursor: pointer;" onclick="agregar_cxc('tabla_cxc','<%=tipoconv%>');" src='<%=BASEURL%>/images/botones/iconos/mas.gif'   width='12' height='12' >
                                                </td>
                                                <td align="left" class="fila">
                                                    <select id="tipo_documento_tabla_cxc_0" name="tipo_documento_tabla_cxc_0">
                                                        <option value="">...</option>
                                                        <%for (int i = 0; i < tipo_documento.size(); i++) {%>
                                                        <option value="<%=((String[]) tipo_documento.get(i))[0]%>"><%=((String[]) tipo_documento.get(i))[1]%></option>
                                                        <%
                                                            }%>
                                                    </select>
                                                </td>
                                                <% if (tipoconv.equals("Consumo")) {%>
                                                <td align="left" class="fila">
                                                    <select id="gen_remesa_0" name="gen_remesa_0">
                                                        <option value="">...</option>
                                                        <option value="true">SI</option>
                                                        <option value="false">NO</option>
                                                    </select>
                                                </td>
                                                <%}%>
                                                <td align="left" class="fila">
                                                    <select id="prefijo_tabla_cxc_0" name="prefijo_tabla_cxc_0">
                                                        <option value="">...</option>
                                                        <%for (int i = 0; i < prefijos.size(); i++) {
                                                                if (((String[]) prefijos.get(i))[0].equals("FAC")) {%>
                                                        <option value="<%=((String[]) prefijos.get(i))[2]%>"><%=((String[]) prefijos.get(i))[1]%></option>
                                                        <%  }
                                                            }%>
                                                    </select>
                                                </td>
                                                <td align="left" class="fila">
                                                    <input style="width: 60px" type="text" maxlength="6" id="hc_tabla_cxc_0" name="hc_tabla_cxc_0">
                                                </td>
                                                <td align="left" class="fila">
                                                    <input style="width: 100px" maxlength="30" type="text" id="cuenta_tabla_cxc_0" name="cuenta_tabla_cxc_0" onchange="validarcuenta('cuenta_tabla_cxc_0')">
                                                </td>
                                                <% if (tipoconv.equals("Consumo")) {%>
                                                <td align="left" class="fila">
                                                    <input style="width: 100px" maxlength="30" type="text" id="cuenta_prov_cxc_0" name="cuenta_prov_cxc_0" onchange="validarcuenta('cuenta_prov_cxc_0')" <%=(convenio != null && convenio.isRedescuento()) ? "disabled" : ""%>>
                                                </td>
                                                 <td align="left" class="fila">
                                                    <input style="width: 100px" type="text" maxlength="30" id="cuenta_prov_cxp_0" name="cuenta_prov_cxp_0" onchange="validarcuenta('cuenta_prov_cxp_0')" <%=((convenio != null) && convenio.isRedescuento()) ? "disabled" : ""%>>
                                                </td>
                                                <%}%>
                                            </tr>
                                            <% if (tipoconv.equals("Consumo")) {%>
                                            <tr>
                                                <td align="left" class="fila" colspan="7">
                                                    <table border="1" width="100%" id="tabla_cxc_fiducia0" cellspacing="0" >
                                                        <tr>
                                                            <td align="left"  width="5%" class="fila">&nbsp;<input type="hidden" name="tamano_tabla_cxc_fiducia0" id="tamano_tabla_cxc_fiducia0" value="1"></td>
                                                            <td align="left"  class="fila">Fiducia</td>
                                                            <td align="left"  class="fila">Prefijo cxc</td>
                                                            <td align="left" class="fila">Hc cxc</td>
                                                            <td align="center"  class="fila">Cuenta cxc</td>
                                                             <!-- nuevas columnas -->
                                                            <td align="left"  class="fila">Prefijo cxc endoso</td>
                                                            <td align="left" class="fila">Hc cxc endoso</td>
                                                            <td align="left" class="fila">Cuenta cxc endoso</td>
                                                            <!-- fin nuevas columnas -->
                                                        </tr>
                                                        <tr>
                                                            <td  align="center" class="fila"><img alt="" style="cursor: pointer;" onclick="agregar_cxc_fiducia('tabla_cxc_fiducia0','0');" src='<%=BASEURL%>/images/botones/iconos/mas.gif'   width='12' height='12' >
                                                            </td>
                                                            <td align="left" class="fila"><select id="nit_fiducia_tabla_cxc_00" name="nit_fiducia_tabla_cxc_00">
                                                                    <option value="">...</option>
                                                                    <%for (int j = 0; j < convenioFiducias.size(); j++) {
                                                                             if (((String[]) prefijos.get(j))[0].equals("FAC")) {%>
                                                                    <option value="<%=((ConvenioFiducias) convenioFiducias.get(j)).getNit_fiducia()%>"><%=(((ConvenioFiducias) convenioFiducias.get(j)).getNombre_fiducia())%></option>
                                                                    <%}
                                                                         }%>
                                                                </select></td>
                                                            <td align="left" class="fila">
                                                                <select id="prefijo_tabla_cxc_fiducia_00" name="prefijo_tabla_cxc_fiducia_00">
                                                                    <option value="">...</option>
                                                                    <%for (int j = 0; j < prefijos.size(); j++) {
                                                                             if (((String[]) prefijos.get(j))[0].equals("FAC")) {%>
                                                                    <option value="<%=((String[]) prefijos.get(j))[2]%>"><%=((String[]) prefijos.get(j))[1]%></option>
                                                                    <%  }
                                                                         }%>
                                                                </select>
                                                            </td>
                                                            <td align="left" class="fila">
                                                                <input style="width: 60px" type="text" maxlength="6" id="hc_tabla_cxc_fiducia_00" name="hc_tabla_cxc_fiducia_00">
                                                            </td>
                                                            <td align="left" class="fila">
                                                                <input style="width: 100px" maxlength="30" type="text" id="cuenta_tabla_cxc_fiducia_00" name="cuenta_tabla_cxc_fiducia_00" onchange="validarcuenta('cuenta_tabla_cxc_fiducia_00')">
                                                            </td>
                                                            <!-- nuevos campos -->
                                                            <td align="left" class="fila">
                                                                <select id="prefijo_tabla_cxc_endoso_00" name="prefijo_tabla_cxc_endoso_00">
                                                                    <option value="">...</option>
                                                                    <%for (int j = 0; j < prefijos.size(); j++) {
                                                                            if (((String[]) prefijos.get(j))[0].equals("FAC")) {%>
                                                                    <option value="<%=((String[]) prefijos.get(j))[2]%>" ><%=((String[]) prefijos.get(j))[1]%></option>
                                                                    <%}
                                                                        }%>
                                                                </select>
                                                            </td>
                                                            <td align="left" class="fila">
                                                                <input style="width: 100px" maxlength="30" type="text" id="hc_tabla_cxc_endoso_00" name="hc_tabla_cxc_endoso_00" value="" onchange="validarcuenta('cuenta_tabla_cxc_fiducia_00')">
                                                            </td>
                                                            <td align="left" class="fila">
                                                                <input style="width: 100px" maxlength="30" type="text" id="cuenta_tabla_cxc_endoso_00" name="cuenta_tabla_cxc_endoso_00" value="" onchange="validarcuenta('cuenta_tabla_cxc_fiducia_00')">
                                                            </td>
                                                            <!-- fin nuevos campos -->
                                                            
                                                        </tr>
                                                    </table>
                                                </td>
                                            </tr>
                                            <%}
                                                        }%>
                                        </tbody>
                                    </table>
                                </div>
                                <div id="divremesas" style="visibility: hidden;display: none;" class="fixedHeaderTable" >
                                    <table border="0" width="100%" id="tabla_remesas">
                                        <thead>
                                            <tr>
                                                <td align="left" width="3%" class="fila">&nbsp;<input type="hidden" name="tamano_tabla_remesas" id="tamano_tabla_remesas" value="<%=conveniosRemesas.size()%>"</td>
                                                <td align="left" class="fila">Ciudad oficina</td><!-- mod -->
                                                <td align="left" class="fila">Banco titulo</td><!-- mod -->
                                                <td align="left" class="fila">Ciudad titulo</td><!-- mod -->
                                                <td align="left" class="fila">Remesa</td><!-- mod -->
                                                <td align="left" class="fila">Porcentaje</td><!-- mod -->
                                                <td align="left" class="fila">Cuenta</td><!-- mod -->
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <%for (int i = 0; i < conveniosRemesas.size(); i++) {
                                                            ConveniosRemesas remesas = (ConveniosRemesas) conveniosRemesas.get(i);%>
                                            <tr>
                                                <td align="center" class="fila">
                                                    <input type="hidden" name="id_tabla_remesas_<%=i%>" id="id_tabla_remesas_<%=i%>" value="<%=remesas.getId_remesa()%>"
                                                </td>
                                                <td align="left" class="fila">
                                                    <input style="width: 85%" type="text" id="ciudad_tabla_remesas_<%=i%>" name="ciudad_tabla_remesas_<%=i%>" value="<%=remesas.getCiudad_sede()%>" readonly>
                                                    <img alt=""  src="<%=BASEURL%>/images/botones/iconos/lupa.gif" width="15" height="15" style="cursor: pointer" onclick="openWin('ciudades','ciudad_tabla_remesas_<%=i%>','Ciudad oficina');">
                                                </td>
                                                <td align="left" class="fila">
                                                    <input style="width: 85%" type="text" id="bancoch_tabla_remesas_<%=i%>" name="bancoch_tabla_remesas_<%=i%>" value="<%=remesas.getBanco_titulo()%>" readonly>
                                                    <img alt="" src="<%=BASEURL%>/images/botones/iconos/lupa.gif" width="15" height="15" style="cursor: pointer" onclick="openWin('bancos','bancoch_tabla_remesas_<%=i%>','Banco titulo');">
                                                </td>
                                                <td align="left" class="fila">
                                                    <input style="width: 85%" type="text" maxlength="4" id="ciudadch_tabla_remesas_<%=i%>" name="ciudadch_tabla_remesas_<%=i%>" value="<%=remesas.getCiudad_titulo()%>" onkeyup="soloNumeros(this.id);">
                                                </td>
                                                <td align="left" class="fila">
                                                    <select id="remesa_tabla_remesas_<%=i%>" name="remesa_tabla_remesas_<%=i%>" onchange="deshabilitarPorcentaje(<%=i%>)">
                                                        <option value="">...</option>
                                                        <option value="true" <%=(remesas.isGenera_remesa()) ? "selected" : ""%> >SI</option>
                                                        <option value="false" <%=(!remesas.isGenera_remesa()) ? "selected" : ""%> >NO</option>
                                                    </select>
                                                </td>
                                                <td align="left" class="fila">
                                                    <input onkeyup="soloNumeros(this.id);" style="width: 120px" type="text" id="porcentaje_tabla_remesas_<%=i%>" name="porcentaje_tabla_remesas_<%=i%>" value="<%=remesas.getPorcentaje_remesa()%>" >
                                                </td>
                                                <td align="left" class="fila">
                                                    <input style="width: 100%" type="text"  maxlength='30' id="cuenta_tabla_remesas_<%=i%>" name="cuenta_tabla_remesas_<%=i%>" value="<%=remesas.getCuenta_remesa()%>" onchange="validarcuenta('cuenta_tabla_remesas_<%=i%>')" >
                                                </td>
                                            </tr>
                                            <%}%>
                                            <tr>
                                                <td align="center" class="fila">
                                                    <input type="hidden" name="id_tabla_remesas_<%=conveniosRemesas.size()%>" id="id_tabla_remesas_<%=conveniosRemesas.size()%>" value="">
                                                    <img alt="" style="cursor: pointer;" onclick="agregar_remesa('tabla_remesas');" src='<%=BASEURL%>/images/botones/iconos/mas.gif'   width='12' height='12' >
                                                </td>
                                                <td align="left" class="fila">
                                                    <input style="width: 85%" type="text" id="ciudad_tabla_remesas_<%=conveniosRemesas.size()%>" name="ciudad_tabla_remesas_<%=conveniosRemesas.size()%>" readonly>
                                                    <img alt=""  src="<%=BASEURL%>/images/botones/iconos/lupa.gif" width="15" height="15" style="cursor: pointer" onclick="openWin('ciudades','ciudad_tabla_remesas_<%=conveniosRemesas.size()%>','Ciudad Fintravalores');">
                                                </td>
                                                <td align="left" class="fila">
                                                    <input style="width: 85%" type="text" id="bancoch_tabla_remesas_<%=conveniosRemesas.size()%>" name="bancoch_tabla_remesas_<%=conveniosRemesas.size()%>" readonly>
                                                    <img alt="" src="<%=BASEURL%>/images/botones/iconos/lupa.gif" width="15" height="15" style="cursor: pointer" onclick="openWin('bancos','bancoch_tabla_remesas_<%=conveniosRemesas.size()%>','Banco Titulo');">
                                                </td>
                                                <td align="left" class="fila">
                                                    <input style="width: 85%" type="text" id="ciudadch_tabla_remesas_<%=conveniosRemesas.size()%>" name="ciudadch_tabla_remesas_<%=conveniosRemesas.size()%>" onkeyup="soloNumeros(this.id);" maxlength="4">
                                                </td>
                                                <td align="left" class="fila">
                                                    <select id="remesa_tabla_remesas_<%=conveniosRemesas.size()%>" name="remesa_tabla_remesas_<%=conveniosRemesas.size()%>" onchange="deshabilitarPorcentaje(<%=conveniosRemesas.size()%>)" >
                                                        <option value="">...</option>
                                                        <option value="true">SI</option>
                                                        <option value="false">NO</option>
                                                    </select>
                                                </td>
                                                <td align="left" class="fila">
                                                    <input onkeyup="soloNumeros(this.id);" style="width: 120px" type="text" id="porcentaje_tabla_remesas_<%=conveniosRemesas.size()%>" name="porcentaje_tabla_remesas_<%=conveniosRemesas.size()%>">
                                                </td>
                                                <td align="left" class="fila">
                                                    <input style="width: 100%" type="text" maxlength="30" id="cuenta_tabla_remesas_<%=conveniosRemesas.size()%>" name="cuenta_tabla_remesas_<%=conveniosRemesas.size()%>" onchange="validarcuenta('cuenta_tabla_remesas_<%=conveniosRemesas.size()%>')">
                                                </td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>
                                <div id="divcomisiones" style="visibility: hidden;display: none;" class="fixedHeaderTable">
                                    <table border="0" width="100%" id="tabla_comisiones">
                                        <thead>
                                            <tr>
                                                <td align="left" width="3%" class="fila">&nbsp;<input type="hidden" name="tamano_tabla_comisiones" id="tamano_tabla_comisiones" value="<%=convenioComision.size()%>" /></td>
                                                <td align="left" class="fila">Nombre</td>
                                                <td align="left" class="fila">Porcentaje</td>
                                                <td align="left" class="fila">Cuenta</td>
                                                <td align="left" class="fila">Tercero factura</td>
                                                <td align="left" class="fila">Ind</td>
                                                <td align="left" class="fila">Contrapartida</td>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <%for (int i = 0; i < convenioComision.size(); i++) {
                                                            ConvenioComision comision = (ConvenioComision) convenioComision.get(i);%>
                                            <tr>
                                                <td align="center" class="fila">
                                                    <input type="hidden" name="id_tabla_comisiones_<%=i%>" id="id_tabla_comisiones_<%=i%>" value="<%=comision.getId_comision()%>" />
                                                </td>
                                                <td align="left" class="fila">
                                                    <input maxlength="20" type="text" id="nombre_tabla_comisiones_<%=i%>" name="nombre_tabla_comisiones_<%=i%>" value="<%=comision.getNombre()%>">
                                                </td>
                                                <td align="left" class="fila">
                                                    <input style="width: 70px" onkeyup="soloNumerosformateado(this.id);"  type="text" id="porcentaje_tabla_comisiones_<%=i%>" name="porcentaje_tabla_comisiones_<%=i%>" value="<%=comision.getPorcentaje_comision()%>">
                                                </td>
                                                <td align="left" class="fila">
                                                    <input style="width: 100px" type="text" maxlength="30" id="cuenta_tabla_comisiones_<%=i%>" name="cuenta_tabla_comisiones_<%=i%>"value="<%=comision.getCuenta_comision()%>" onchange="validarcuenta('cuenta_tabla_comisiones_<%=i%>')">
                                                </td>
                                                <td align="left" class="fila"><input type="radio" name="rdoTerceroFactura" id="rdoTerceroFactura_<%=i%>" value="<%=i%>" onmousedown="guardarEstado(<%=i%>)" onclick="deshabilitarIndicador(<%=i%>)" <%=(comision.isComision_tercero()) ? "checked" : ""%>></td>
                                                <td align="left" class="fila"><input type="checkbox" name="chkInd_<%=i%>" id="chkInd_<%=i%>" onclick="deshabilitarContrapartida(<%=i%>)" <%=(comision.isIndicador_contra()) ? "checked" : ""%> ></td>
                                                <td align="left" class="fila"><input type="text" style="width: 100px" maxlength="30" name="txtContrapartida_<%=i%>" id="txtContrapartida_<%=i%>" value="<%=comision.getCuenta_contra()%>" <%=(comision.isIndicador_contra()) ? "" : "disabled"%> onchange="validarcuenta('txtContrapartida_<%=i%>')"></td>
                                            </tr>
                                            <%}%>
                                            <tr>
                                                <td align="center" class="fila">
                                                    <img alt="" style="cursor: pointer;" onclick="agregar_comision('tabla_comisiones');" src='<%=BASEURL%>/images/botones/iconos/mas.gif'   width='12' height='12' >
                                                    <input type="hidden" name="id_tabla_comisiones_<%=convenioComision.size()%>" id="id_tabla_comisiones_<%=convenioComision.size()%>" value="">
                                                </td>
                                                <td align="left" class="fila">
                                                    <input type="text" maxlength="20" id="nombre_tabla_comisiones_<%=convenioComision.size()%>" name="nombre_tabla_comisiones_<%=convenioComision.size()%>" value="">
                                                </td>
                                                <td align="left" class="fila">
                                                    <input style="width: 70px" onkeyup="soloNumerosformateado(this.id);"  type="text" id="porcentaje_tabla_comisiones_<%=convenioComision.size()%>" name="porcentaje_tabla_comisiones_<%=convenioComision.size()%>" value="">
                                                </td>
                                                <td align="left" class="fila">
                                                    <input style="width: 100px" type="text" maxlength="30" id="cuenta_tabla_comisiones_<%=convenioComision.size()%>" name="cuenta_tabla_comisiones_<%=convenioComision.size()%>"value="" onchange="validarcuenta('cuenta_tabla_comisiones_<%=convenioComision.size()%>')">
                                                </td>
                                                <td align="left" class="fila"><input type="radio" name="rdoTerceroFactura" id="rdoTerceroFactura_<%=convenioComision.size()%>" value="<%=convenioComision.size()%>" onmousedown="guardarEstado(<%=convenioComision.size()%>)" onclick="deshabilitarIndicador(<%=convenioComision.size()%>)" ></td>
                                                <td align="left" class="fila"><input type="checkbox" name="chkInd_<%=convenioComision.size()%>" id="chkInd_<%=convenioComision.size()%>" onclick="deshabilitarContrapartida(<%=convenioComision.size()%>)" ></td>
                                                <td align="left" class="fila"><input type="text" style="width: 100px" maxlength="30" name="txtContrapartida_<%=convenioComision.size()%>" id="txtContrapartida_<%=convenioComision.size()%>" onchange="validarcuenta('txtContrapartida_<%=convenioComision.size()%>')" disabled></td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>
                            </td>
                        </tr>
                    </table>
                    <br>
                    <br>
                    <%if (modificar.equals("SI")) {
                                    if (tipo.equals("NUEVO")) {%>
                    <img alt="" src = "<%=BASEURL%>/images/botones/aceptar.gif" style="cursor:pointer" onclick="guardar_cambios('formulario');" name= "imgaceptar" id="imgaceptar"  onMouseOver="botonOver(this);" onMouseOut="botonOut(this);">
                    <%} else {%>
                    <img alt="" src = "<%=BASEURL%>/images/botones/modificar.gif" style = "cursor:pointer" onclick="guardar_cambios('formulario');" name = "imgmodificar" id="imgmodificar"  onMouseOver="botonOver(this);" onMouseOut="botonOut(this);">
                    <%}%>
                    <%}%>
                    <img alt="" src = "<%=BASEURL%>/images/botones/regresar.gif" style = "cursor:pointer" name = "imgsalir" onClick = "window.location='<%=BASEURL%>/jsp/fenalco/convenio/Convenios.jsp?modificar=<%=modificar%>&tipoconv=<%=tipoconv%>'" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);">
                    <img alt="" src = "<%=BASEURL%>/images/botones/salir.gif" style = "cursor:pointer" name = "imgsalir" onClick = "window.close();" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);">
                </center>
            </div>
        </form>
    </body>
</html>
