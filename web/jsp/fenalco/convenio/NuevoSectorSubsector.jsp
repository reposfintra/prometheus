<%-- 
    Document   : NuevoSectorSubsector
    Created on : 3/08/2010, 09:37:34 AM
    Author     : maltamiranda
--%>


<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@page session    ="true"%>
<%@page errorPage  ="/error/ErrorPage.jsp"%>
<%@include file    ="/WEB-INF/InitModel.jsp"%>
<%@page import    ="java.util.*" %>
<%@page import    ="com.tsp.util.*" %>
<%@page import    ="com.tsp.operation.model.beans.Usuario" %>

<link href="<%= BASEURL%>/css/estilostsp.css" rel="stylesheet" type="text/css">
<script src="<%=BASEURL%>/js/prototype.js"      type="text/javascript"></script>
<script src="<%=BASEURL%>/js/boton.js"          type="text/javascript"></script>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">

<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">        
        <title>Nuevo de Sector Subsector</title>        
    </head>
    <body>
        
        <%  String nombre="",detalle="",id="",tipo="",idsub="",codigo="", activo="", reliquida="", codigoalterno="", nombrealterno="";
            tipo=(request.getParameter("tipo")!=null)?request.getParameter("tipo"):"";
            if(tipo.equals("VENTANA_EDITAR")||tipo.equals("VENTANA_EDITAR_SUBSECTOR")){
                nombre=model.sectorsubsectorSvc.getNombre_sector();
                detalle=model.sectorsubsectorSvc.getDetalle_sector();
                codigo=model.sectorsubsectorSvc.getCodigo_sector();
                activo=model.sectorsubsectorSvc.getActivo();
                id=(request.getParameter("id")!=null)?request.getParameter("id"):"";
                idsub=(request.getParameter("idsub")!=null)?request.getParameter("idsub"):"";
                reliquida=model.sectorsubsectorSvc.getReliquida();
                nombrealterno=model.sectorsubsectorSvc.getNombre_alterno();
            }
//            out.print(id);

        %>
        <br>
        <br>
        <center>
            <table width="90%" border="2">
                <tr>
                    <td>
                        <table class="tablaInferior" border="0" width="100%">
                            <tr >
                                <td width="50%" align="left" class="subtitulo1">&nbsp;<%=(tipo.equals("VENTANA_EDITAR")||tipo.equals("NUEVO_SECTOR"))?"Datos del sector":"Datos del subsector"%></td><!-- mod -->
                                <td width="50%" align="left" class="barratitulo">
                                    <img alt="" src="<%= BASEURL%>/images/titulo.gif" width="32" height="20"  align="left">
                                </td>
                            </tr>
                        </table>
                        <table border="0" width="100%">
                            <tr>
                                <td align="left" width="40%" class="fila">&nbsp;Codigo</td><!-- mod -->
                                <td align="left" width="60%" class="fila">
                                    <input type="text" id="codigo" name="codigo" <%=(!tipo.equals("VENTANA_EDITAR")&&!tipo.equals("VENTANA_EDITAR_SUBSECTOR"))?"":"readonly"%> value="<%=codigo%>" maxlength="3" onkeyup="soloNumeros(this.id);">
       
                                </td>
                            </tr>
                            <tr>
                                <td align="left" width="40%" class="fila">&nbsp;Nombre</td><!-- mod -->
                                <td align="left" width="60%" class="fila">
                                    <input type="text" id="nombre" name="nombre" value="<%=nombre%>" size="40" maxlength="70" <%=(!tipo.equals("VENTANA_EDITAR")&&!tipo.equals("VENTANA_EDITAR_SUBSECTOR"))?"":"readonly"%>><!-- mod -->
                                </td>
                            </tr>
                            <tr>
                                <td align="left" width="40%" class="fila">&nbsp;Detalle</td><!-- mod -->
                                <td align="left" width="60%" class="fila">
                                    <!-- <input type="text" id="detalle" name="detalle" value="<%=detalle%>"> --> <!-- mod -->
                                    <textarea name="detalle" id="detalle" rows="4" cols="40"><%=detalle%></textarea> <!-- mod -->
                                </td>
                            </tr>
                            <tr>
                                <td align="left" width="40%" class="fila">&nbsp;Activo</td>
                                <td align="left" width="60%" class="fila">
                                    <input type="checkbox" name="activo" id="activo" value="<%=activo%>"  <%=  (activo.equals(""))?"checked='checked'":"" %> onchange="activo();" >
                                </td>
                            </tr>
                  <%  if((tipo.equals("VENTANA_EDITAR")||tipo.equals("NUEVO_SECTOR"))){%>
                          <tr>
                                <td align="left" width="40%" class="fila">&nbsp;Codigo Alterno</td><!-- mod -->
                                <td align="left" width="60%" class="fila">
                                    <input type="text" id="codigo_alterno" name="codigo_alterno" value="<%=codigoalterno%>" maxlength="2" onkeyup="soloNumeros(this.id);">
                                </td>
                            </tr>                           
                            <tr>
                                <td align="left" width="40%" class="fila">&nbsp;Reliquida?</td>
                                <td align="left" width="60%" class="fila">
                                    <input type="checkbox" name="reliquida" id="reliquida" value="<%=reliquida%>"  <%=  (reliquida.equals("S"))?"checked='checked'":"" %> onchange="reliquida();" >
                                </td>
                            </tr>
                          <% }%>
                          <tr>
                                <td align="left" width="40%" class="fila">&nbsp;Nombre Alterno</td>
                                <td align="left" width="60%" class="fila">
                                    <input type="text" id="nombre_alterno" name="nombre_alterno" size="40" value="<%=nombrealterno%>" maxlength="70" >
                                </td>
                          </tr>
                        </table>
                    </td>
                </tr>
            </table>
            <br>
            <%if(!tipo.equals("VENTANA_EDITAR")&&!tipo.equals("VENTANA_EDITAR_SUBSECTOR")){
                if(tipo.equals("NUEVO_SECTOR")){%>
                    <img alt="" src = "<%=BASEURL%>/images/botones/aceptar.gif" style = "cursor:pointer" name = "imgaceptar" id="imgaceptar"  onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" onclick='sendAction("<%=CONTROLLER%>?estado=Sector&accion=Subsector","AGREGAR_SECTOR","",event);'>
                <%} else{%>
                    <img alt="" src = "<%=BASEURL%>/images/botones/aceptar.gif" style = "cursor:pointer" name = "imgaceptar" id="imgaceptar"  onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" onclick='sendAction("<%=CONTROLLER%>?estado=Sector&accion=Subsector","AGREGAR_SUBSECTOR","",event);'>
                <%}
            }else{
                if(tipo.equals("VENTANA_EDITAR")){%>
                    <img alt="" src = "<%=BASEURL%>/images/botones/modificar.gif" style = "cursor:pointer" name = "imgaceptar" id="imgaceptar"  onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" onclick='sendAction("<%=CONTROLLER%>?estado=Sector&accion=Subsector","EDITAR","<%=id%>",event);'>
                <%} else{%>
                    <img alt="" src = "<%=BASEURL%>/images/botones/modificar.gif" style = "cursor:pointer" name = "imgaceptar" id="imgaceptar"  onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" onclick='sendAction("<%=CONTROLLER%>?estado=Sector&accion=Subsector&idsub=<%=idsub%>","EDITAR_SUBSECTOR","<%=id%>",event);'>
                <%}
            }
            %>            
            <img alt="" src="<%=BASEURL%>/images/botones/salir.gif" style="cursor:pointer" name="imgsalir" id="imgsalir" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" onclick='closewin();'>
        </center>

    </body>
</html>
