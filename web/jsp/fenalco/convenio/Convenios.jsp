<%--  
    Document   : Convenios
    Created on : 12/08/2010, 11:14:43 AM
    Author     : maltamiranda
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@page session    ="true"%>
<%@page errorPage  ="/error/ErrorPage.jsp"%>
<%@include file    ="/WEB-INF/InitModel.jsp"%>
<%@page import    ="java.util.*" %>
<%@page import    ="com.tsp.util.*" %>
<%@page import    ="com.tsp.operation.model.beans.Usuario" %>

<script src="<%=BASEURL%>/js/prototype.js"      type="text/javascript"></script>
<script src="<%=BASEURL%>/js/boton.js"          type="text/javascript"></script>
<script src="<%=BASEURL%>/js/effects.js"        type="text/javascript"></script>
<script src="<%=BASEURL%>/js/window.js"         type="text/javascript"></script>
<script src="<%=BASEURL%>/js/window_effects.js" type="text/javascript"></script>

<link href="<%= BASEURL%>/css/estilostsp.css" rel="stylesheet" type="text/css">
<link href="<%=BASEURL%>/css/mac_os_x.css"      rel="stylesheet" type="text/css">
<link href="<%=BASEURL%>/css/default.css"       rel="stylesheet" type="text/css">
<link href="<%=BASEURL%>/css/alert.css" rel="stylesheet" type="text/css"/>
<link href="<%=BASEURL%>/css/alphacube.css" rel="stylesheet" type="text/css"/>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
   "http://www.w3.org/TR/html4/loose.dtd">

<html>
    <style type="text/css">
        div.fixedHeaderTable {
                position: relative;
        }
        div.fixedHeaderTable tbody {
            height: 100px;
            overflow-y: scroll;
            overflow-x: hidden;
        }
        div.fixedHeaderTable thead td, div.fixedHeaderTable thead th {
                position:relative;
        }

        /* IE7 hacks */
        div.fixedHeaderTable {
            *position: relative;
            *height: 100px;
            *overflow-y: scroll;
            *overflow-x: scroll;
            *padding-right:16px;
        }

        div.fixedHeaderTable thead tr {
            *position: relative;
            _position: absolute;
            *top: expression(this.offsetParent.scrollTop-2);
            *background:none;
            color: white;
            border-color: silver;
        }

        div.fixedHeaderTable tbody {
            *height: auto;
            *position:absolute;
            *top:50px;
        }
        /* IE6 hacks */
        div.fixedHeaderTable {
                _width:100%;
                _overflow: auto;
            _overflow-y: scroll;
            _overflow-x: hidden;
        }
        div.fixedHeaderTable thead tr {
                _position: relative
        }
    </style>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
        <title>Convenios</title>
        <script>
            function color_fila(id,color){
                $(id).style.backgroundColor=color;
            }

          function sendAction(url,tipo,id,ev,modificar,tipoconv){
                sw=false;sw2=true;
                try{
                    if(ev.target.type!='radio'){
                        sw=true;
                    }
                }catch(e){
                    if(ev.srcElement.type!='radio'){
                        sw=true;
                    }
                }
                if(tipo=='MODIFICAR'){
                    if(id==''){
                        for (i=0;i<$('formulario').length && sw2;i++){
                            if ($('formulario').elements[i].type=='radio' && $('formulario').elements[i].checked==true){
                                sw2=false;
                                id=$('formulario').elements[i].id;
                            }
                        }
                    }
                    else{
                        $(id).checked=true;
                    }
                    if(id!=''){
                        if(sw){
                            p="&tipo="+tipo+"&id="+id+"&modificar="+modificar+"&tipoconv="+tipoconv;
                            $('formulario').action=url+p;
                            $('formulario').submit();
                        }
                    }
                    else{
                        Dialog.alert("Debe escoger al menos un registro ...", {
                            width:250,
                            height:100,
                            windowParameters: {className: "alphacube"}
                        });
                    }
                }else{
                    if(tipo=='NUEVO'){
                            $('tipo').value=tipo;
                            $('formulario').action=url;
                            $('formulario').submit();
                    }
                }
            }

            function exportar(){
                var url = "<%= CONTROLLER%>?estado=Gestion&accion=Convenios";
                var p =  'tipo=exportar';
                new Ajax.Request(
                    url,
                    {
                        method: 'post',
                        parameters: p,
                        onLoading: loading,
                        onComplete: llenarDiv
                    }
                );
            }
            
            function loading(){
                $('divload').innerHTML = "<span class='fila'>Procesando peticion ... <img alt='cargando' src='<%= BASEURL%>/images/cargando.gif' name='imgload'></span>";
            }
            
            function unload(){
                $('divload').innerHTML = "";
            }

            function llenarDiv(response){
                unload();
                $('divload').innerHTML = response.responseText;
            }
        </script>
    </head>
    <body>
        <form id="formulario" name="formulario" action="" method="post">
            <div id="capaSuperior" style="position:absolute; width:100%; height:100px; z-index:0; left: 0px; top: 0px;">
                <jsp:include page="/WEB-INF/toptsp.jsp?encabezado=Convenios"/>
            </div>
            <div id="capaCentral" style="position:absolute; width:100%; height:85%; z-index:0; left: 0px; top: 100px; overflow: scroll;">
            
            <%

                Usuario usuario = (Usuario)session.getAttribute("Usuario");
                String tipoconv = (request.getParameter("tipoconv") != null) ? request.getParameter("tipoconv") : "";
                ArrayList arl=model.gestionConveniosSvc.getConvenios(tipoconv);
                String modificar = (request.getParameter("modificar") != null) ? request.getParameter("modificar") : "";

            %>
                <center>
                    <input type="hidden" id="modificar" name="modificar" value="<%=modificar%>">
                    <input type="hidden" id="tipo" name="tipo" value="">
                    <input type="hidden" id="tipoconv" name="tipoconv" value="<%=tipoconv%>">
                    <div id="contenido" align="center" style=" width: 400px;height: 300px; visibility: hidden; background-color:#F7F5F4; display: none"></div>
                    <table width="1000" border="2">
                        <tr>
                            <td>
                                <table class="tablaInferior" border="0" width="100%">
                                    <tr >
                                        <td width="30%" align="left" class="subtitulo1">&nbsp;Convenios:</td>
                                        <td width="70%" align="left" class="barratitulo">
                                            <img alt="" src="<%= BASEURL%>/images/titulo.gif" width="32" height="20"  align="left">
                                        </td>
                                    </tr>
                                </table>
                                <div align="center">
                                    <table id="convenio" width="1200px" border="1"><%
                                    for (int i=0;i<arl.size();i++){
                                        ArrayList arl2=((ArrayList)arl.get(i));
                                            %><tr id="fila<%=(String)arl2.get(0)%>"<%= (i==0)?" class='fila' align='center' style='color: white;background-color:#03A70C;'":"onclick=\"sendAction('"+CONTROLLER+"?estado=Gestion&accion=Convenios','MODIFICAR','"+(String)arl2.get(0)+"',event,'"+modificar+"','"+tipoconv+"');\" class='fila_provision' align='center' style='cursor: pointer'  onmouseover=\"color_fila('fila"+(String)arl2.get(0)+"','orange');\" onmouseout=\"color_fila('fila"+(String)arl2.get(0)+"','#EAFFEA');\"" %>>
                                        <%for(int j=-1;j< (arl2.size() - 2);j++){
                                            if(j==-1 && i!=0){
                                            %><td align="center" width="2%"><input type="radio" id="<%=(String)arl2.get(0)%>" name="convenios"></td><%
                                            }
                                            else{
                                                if(j!=-1)
                                                {   %><td align="<%=(i==0)?"center":"left"%>" width="<%=(j==0)?"2%":((j==1)?"10%":((j==2)?"20%":((j==3)?"48%":((j==4)?"10%":"10%"))))%>">&nbsp;<%=(String)arl2.get(j) %></td><%
                                                }
                                                else
                                                {   %><td></td><%
                                                }
                                            }
                                        }
                                        %></tr><%
                                    }
                                    %></table>
                                </div>
                            </td>
                        </tr>
                    </table>
                    <br>
                    <br>
                    <img alt="" src = "<%=BASEURL%>/images/botones/detalles.gif" style = "cursor:pointer" onclick='sendAction("<%=CONTROLLER%>?estado=Gestion&accion=Convenios","MODIFICAR","",event,"<%=modificar%>","<%=tipoconv%>");' name = "imgaceptar" id="imgaceptar"  onMouseOver="botonOver(this);" onMouseOut="botonOut(this);">
                    <%if(modificar != null && modificar.equals("SI")){%>
                    <img alt="" src = "<%=BASEURL%>/images/botones/nuevo.gif" style = "cursor:pointer" onclick='sendAction("<%=CONTROLLER%>?estado=Gestion&accion=Convenios","NUEVO","",event,"<%=modificar%>","<%=tipoconv%>");' name = "imgaceptar" id="imgaceptar"  onMouseOver="botonOver(this);" onMouseOut="botonOut(this);">
                    <%}else{%>
                    <img alt="" src = "<%=BASEURL%>/images/botones/exportarExcel.gif" style = "cursor:pointer" onclick="exportar();" name = "imgaceptar" id="imgaceptar"  onMouseOver="botonOver(this);" onMouseOut="botonOut(this);">
                    <%}%>
                    <img alt="" src = "<%=BASEURL%>/images/botones/restablecer.gif" style = "cursor:pointer" name = "imgsalir" onClick = "location.reload()" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);">
                    <img alt="" src = "<%=BASEURL%>/images/botones/salir.gif" style = "cursor:pointer" name = "imgsalir" onClick = "window.close();" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);">
                    <br>
                    <br>
                    <div id="divload"></div>
                </center>
            </div>
        </form>
    </body>
</html>
