<%--
    Document   : gSectores
    Created on : 2/08/2010, 10:09:32 AM
    Author     : maltamiranda
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@page session    ="true"%>
<%@page errorPage  ="/error/ErrorPage.jsp"%>
<%@include file    ="/WEB-INF/InitModel.jsp"%>
<%@page import    ="java.util.*" %>
<%@page import    ="com.tsp.util.*" %>
<%@page import    ="com.tsp.operation.model.beans.Usuario" %>

<script src="<%=BASEURL%>/js/prototype.js"      type="text/javascript"></script>
<script src="<%=BASEURL%>/js/boton.js"          type="text/javascript"></script>
<script src="<%=BASEURL%>/js/effects.js"        type="text/javascript"></script>
<script src="<%=BASEURL%>/js/window.js"         type="text/javascript"></script>
<script src="<%=BASEURL%>/js/window_effects.js" type="text/javascript"></script>

<link href="<%= BASEURL%>/css/estilostsp.css" rel="stylesheet" type="text/css">
<link href="<%=BASEURL%>/css/mac_os_x.css"      rel="stylesheet" type="text/css">
<link href="<%=BASEURL%>/css/default.css"       rel="stylesheet" type="text/css">
<link href="<%=BASEURL%>/css/alert.css" rel="stylesheet" type="text/css"/>
<link href="<%=BASEURL%>/css/alphacube.css" rel="stylesheet" type="text/css"/>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
   "http://www.w3.org/TR/html4/loose.dtd">

<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
        <title>Gestion de Sectores</title>
    </head>
    <body>
        <script type="text/javascript">
            function soloNumeros(id) {
                var valor = document.getElementById(id).value;
                valor =  valor.replace(/[^0-9^.]+/gi,"");
                document.getElementById(id).value = valor;
            }
        </script>
        <script>
            var win=null;
            function color_fila(id,color){
                $(id).style.backgroundColor=color;
            }
            function sendAction(url,tipo,id,ev,modificar){
                sw=false;formx='formulario';sw2=true;
                try{
                    if(ev.target.type!='radio'){
                        sw=true;
                    }
                }catch(e){
                    if(ev.srcElement.type!='radio'){
                        sw=true;
                    }
                }
                if(tipo=='MODIFICAR'){
                    if(id==''){
                        for (i=0;i<$(formx).length && sw2;i++){
                            if ($(formx).elements[i].type=='radio' && $(formx).elements[i].checked==true){
                                sw2=false;
                                id=$(formx).elements[i].id;
                            }
                        }
                    }
                    else{
                        $(id).checked=true;
                    }
                    if(id!=''){
                        if(sw){
                            p="tipo="+tipo+"&id="+id+"&modificar="+modificar;
                            win = new Window(
                            {   id: "detalleSector",
                                title: "Subsectores",//<!-- mod -->
                                width:1100,
                                height:400,
                                showEffect:Effect.BlindDown,
                                hideEffect: Effect.Fade,
                                destroyOnClose: true,
                                onClose:closewin,
                                url: url+'&'+p
                            });
                            win.showCenter();
                            win.show(true);
                        }
                    }
                    else{
                        Dialog.alert("Debe escojer al menos un sector ...", {
                            width:250,
                            height:100,
                            windowParameters: {className: "alphacube"}
                        });
                    }
                }
                else{
                    if(tipo=='NUEVO_SECTOR'){
                        p="tipo="+tipo;
                        new Ajax.Request(
                        url,
                        {   method: 'post',
                            parameters: p,
                            onComplete: openwin
                        });
                    }
                    else{
                        if(tipo=='VENTANA_EDITAR'){
                            sw=true;
                            for (i=0;i<$(formx).length && sw;i++){
                                if ($(formx).elements[i].type=='radio' && $(formx).elements[i].checked==true){
                                    sw=false;
                                    id=$(formx).elements[i].id;
                                }
                            }
                            if(id!=''){
                                p="tipo="+tipo+"&id="+id;
                                new Ajax.Request(
                                url,
                                {   method: 'post',
                                    parameters: p,
                                    onComplete: openwin
                                });
                            }
                            else{
                                Dialog.alert("Debe escojer al menos un sector ...", {
                                    width:250,
                                    height:100,
                                    windowParameters: {className: "alphacube"}
                                });
                            }
                        }
                        else{
                            if(tipo=='EDITAR'){
                                if($('detalle').value!='' &&  $('nombre').value!=''){
                                    p="tipo="+tipo+"&nombre="+$('nombre').value+"&detalle="+$('detalle').value+"&codigo="+$('codigo').value+"&id="+id+"&activo="+$('activo').value+"&reliquida="+$('reliquida').value+"&codigo_alterno="+$('codigo_alterno').value+"&nombre_alterno="+$('nombre_alterno').value;
                                    new Ajax.Request(
                                    url,
                                    {   method: 'post',
                                        parameters: p,
                                        onComplete: complete
                                    });
                                }
                                else
                                {   Dialog.alert("Debe llenar todos los campos ...", {
                                        width:250,
                                        height:100,
                                        windowParameters: {className: "alphacube"}
                                    });
                                }
                            }
                            else
                            {   if(tipo=='NUEVO_SUBSECTOR'){
                                    p="tipo="+tipo;
                                    new Ajax.Request(
                                    url,
                                    {   method: 'post',
                                        parameters: p,
                                        onComplete: openwin
                                    });
                                }
                                else{
                                    if(tipo=='AGREGAR_SECTOR'){
                                        if($('detalle').value!='' &&  $('nombre').value!=''){
                                            openInfoDialog('<b> Ejecutando<br/>Por favor espere...</b>');
                                            p="tipo="+tipo+"&nombre="+$('nombre').value+"&detalle="+$('detalle').value+"&codigo="+$('codigo').value+"&activo="+$('activo').value+"&reliquida="+$('reliquida').value+"&codigo_alterno="+$('codigo_alterno').value+"&nombre_alterno="+$('nombre_alterno').value;
                                            new Ajax.Request(
                                            url,
                                            {   method: 'post',
                                                parameters: p,
                                                onComplete: addrow
                                            });
                                        }
                                        else
                                        {   Dialog.alert("Debe llenar todos los campos ...", {
                                                width:250,
                                                height:100,
                                                windowParameters: {className: "alphacube"}
                                            });
                                        }
                                        //closewin();
                                    }
                                }
                            }
                        }
                    }

                }

            }
            function complete(response){
                Dialog.alert(response.responseText, {
                    width:250,
                    height:100,
                    windowParameters: {className: "alphacube"}
                });
            }
            function addrow(response){
                try{
                    setTimeout('', 1000, '', '');
                    Dialog.closeInfo();
                    if(response.responseText.indexOf('ERROR')==-1){
                        myTable = $('sectores');
                        if(navigator.appName=='Microsoft Internet Explorer'){
                            oRow = myTable.insertRow();
                        }
                        else{
                            oRow = myTable.insertRow(-1);
                        }
                        oRow.id='fila'+response.responseText;
                        if(navigator.appName=='Microsoft Internet Explorer'){
                            oRow.onclick=function clic(){
                                             sendAction('<%=CONTROLLER%>?estado=Sector&accion=Subsector','MODIFICAR',response.responseText,event);
                                         };
                        }
                        else{
                            oRow.onclick=function clic(event){
                                             sendAction('<%=CONTROLLER%>?estado=Sector&accion=Subsector','MODIFICAR',response.responseText,event);
                                         };
                        }
                        oRow.className='fila';
                        oRow.align='center';
                        oRow.style.cursor='pointer';
                        oRow.onmouseover=function mouseover(){
                                            color_fila('fila'+response.responseText,'orange');
                                        };
                        oRow.onmouseout=function mouseout(){
                                            color_fila('fila'+response.responseText,'EAFFEA');
                                        };
                        oCell=oRow.insertCell(0);
                        oCell.align='center';
                        oCell.innerHTML='<input type="radio" id="'+response.responseText+'" name="sectores">';
                        oCell=oRow.insertCell(1);
                        oCell.align='left';
                        oCell.innerHTML=response.responseText;
                        oCell=oRow.insertCell(2);
                        oCell.align='left';
                        oCell.innerHTML=$('nombre').value;
                        oCell=oRow.insertCell(3);
                        oCell.align='left';
                        oCell.innerHTML=$('detalle').value;
                        oCell=oRow.insertCell(4);
                        oCell.align='left';
                        oCell.innerHTML="ACTIVO";
                        closewin();
                    }
                    else
                    {  Dialog.alert(response.responseText, {
                    width:250,
                    height:100,
                    windowParameters: {className: "alphacube"}
                });

                    }
                }catch(e){
                    Dialog.closeInfo();
                }
            }
            function closewin(){
                location.reload();
                win.close();
                win.destroy();
                $('contenido').style.display='none';
                $('contenido').innerHTML="";
                $('contenido').style.visibility='hidden';
            }
            function openwin(response){
                $('contenido').innerHTML=response.responseText;
                $('contenido').style.display='block';
                $('contenido').style.visibility='visible';
                win= new Window(
                {   id: "nuevo",
                    title: "Nuevo/Modificar",//<!-- mod -->
                    width:$('contenido').width,
                    height:$('contenido').heigth,
                    destroyOnClose: true,
                    onClose:closewin,
                    resizable: false
                });
                win.setContent('contenido', true, true);
                win.show(true);
                win.showCenter();
            }
            function openInfoDialog(mensaje) {
                Dialog.info(mensaje, {
                    width:250,
                    height:100,
                    showProgress: true,
                    windowParameters: {className: "alphacube"}
                });
            }

            function activo() {
               if( document.getElementById("activo").checked){
                   document.getElementById("activo").value="";
               }else{
                   document.getElementById("activo").value="A";
               }
            }
             function reliquida() {
               if( document.getElementById("reliquida").checked){
                   document.getElementById("reliquida").value="S";
               }else{
                   document.getElementById("reliquida").value="N";
               }
            }
        </script>
        <form id="formulario" action="">
            <div id="capaSuperior" style="position:absolute; width:100%; height:100px; z-index:0; left: 0px; top: 0px;">
                <jsp:include page="/WEB-INF/toptsp.jsp?encabezado=Gestion de Sectores y SubSectores"/>
            </div>
            <div id="capaCentral" style="position:absolute; width:100%; height:85%; z-index:0; left: 0px; top: 100px; overflow: scroll;">
            <%
                Usuario usuario = (Usuario)session.getAttribute("Usuario");
                ArrayList arl=model.sectorsubsectorSvc.getSectores(usuario.getBd());
                String modificar = (request.getParameter("modificar") != null) ? request.getParameter("modificar") : "";

            %>
                <center>
                    <div id="contenido" align="center" style=" width: 500px;height: 350px; visibility: hidden; background-color:#F7F5F4; display: none"></div>
                    <table width="90%" border="2">
                        <tr>
                            <td>
                                <table class="tablaInferior" border="0" width="100%">
                                    <tr >
                                        <td width="30%" align="left" class="subtitulo1">&nbsp;Sectores</td><!-- mod -->
                                        <td width="70%" align="left" class="barratitulo">
                                            <img alt="" src="<%= BASEURL%>/images/titulo.gif" width="32" height="20"  align="left">
                                        </td>
                                    </tr>
                                </table>
                                <div align="center">
                                    <table id="sectores" width="100%" border="1">
                                        <%
                                    for (int i=0;i<arl.size();i++){
                                        ArrayList arl2=((ArrayList)arl.get(i));
                                        %><tr id="fila<%=(String)arl2.get(0)%>"<%= (i==0)?" class='fila' align='center' style='color: white;background-color:green;'":"onclick=\"sendAction('"+CONTROLLER+"?estado=Sector&accion=Subsector','MODIFICAR','"+(String)arl2.get(0)+"',event,'"+modificar+"');\" class='fila' align='center' style='cursor: pointer'  onmouseover=\"color_fila('fila"+(String)arl2.get(0)+"','orange');\" onmouseout=\"color_fila('fila"+(String)arl2.get(0)+"','#EAFFEA');\"" %>>
                                        <%for(int j=-1;j<arl2.size();j++){
                                            if(j==-1 && i!=0){
                                            %><td align="center" width="2%"><input type="radio" id="<%=(String)arl2.get(0)%>" name="sectores"></td><%
                                            }
                                            else{
                                                if(j!=-1)
                                                {   %><td align="<%=(i==0)?"center":"left"%>" width="<%=(j==0)?"6%":((j==1)?"20%":((j==2)?"30%":((j==3)?"8%":((j==4)?"6%":((j==5)?"20%":"10%")))))%>">&nbsp;<%=(String)arl2.get(j) %></td><%
                                                }
                                                else
                                                {   %><td width="5%"></td><%//<!-- mod -->
                                                }
                                            }
                                        }
                                        %></tr><%
                                    }
                                    %></table>
                                </div>
                            </td>
                        </tr>
                    </table>
                    <br>
                    <br>
                    <img alt="" src = "<%=BASEURL%>/images/botones/detalles.gif" style = "cursor:pointer" onclick='sendAction("<%=CONTROLLER%>?estado=Sector&accion=Subsector","MODIFICAR","",event,"<%=modificar%>");' name = "imgaceptar" id="imgaceptar"  onMouseOver="botonOver(this);" onMouseOut="botonOut(this);">
                    <%if(modificar != null && modificar.equals("SI")){%>
                    <img alt="" src = "<%=BASEURL%>/images/botones/nuevo.gif" style = "cursor:pointer" onclick='sendAction("<%=CONTROLLER%>?estado=Sector&accion=Subsector","NUEVO_SECTOR","",event,"<%=modificar%>");' name = "imgaceptar" id="imgaceptar"  onMouseOver="botonOver(this);" onMouseOut="botonOut(this);">
                    <img alt="" src = "<%=BASEURL%>/images/botones/modificar.gif" style = "cursor:pointer" onclick='sendAction("<%=CONTROLLER%>?estado=Sector&accion=Subsector","VENTANA_EDITAR","",event,"<%=modificar%>");' name = "imgaceptar" id="imgaceptar"  onMouseOver="botonOver(this);" onMouseOut="botonOut(this);">
                    <%}else{%>
                    <img alt="" src = "<%=BASEURL%>/images/botones/exportarExcel.gif" style = "cursor:pointer" name = "imgaceptar" id="imgaceptar"  onMouseOver="botonOver(this);" onMouseOut="botonOut(this);">
                    <%}%>
                    <img alt="" src = "<%=BASEURL%>/images/botones/restablecer.gif" style = "cursor:pointer" name = "imgsalir" onClick = "location.reload()" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);">
                    <img alt="" src = "<%=BASEURL%>/images/botones/salir.gif" style = "cursor:pointer" name = "imgsalir" onClick = "window.close();" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);">

                </center>
            </div>
        </form>
    </body>
</html>
