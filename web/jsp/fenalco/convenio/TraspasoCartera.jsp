<%-- 
    Document   : TraspasoCartera.jsp
    Created on : 20/09/2011, 03:37:13 PM
    Author     : Iris Vargas
--%>

<%@page import="java.util.ArrayList"%>
<%@page import="com.tsp.util.Util"%>
<%@page session="true"%>
<%@ include file="/WEB-INF/InitModel.jsp" %>
<%@page import="com.tsp.operation.model.*"%>
<%@page import="com.tsp.operation.model.beans.*"%>
<%@page import="com.tsp.operation.model.services.*"%>
<%@page contentType="text/html" pageEncoding="ISO-8859-1"%>
<%
            response.addHeader("Cache-Control", "no-cache");
            response.addHeader("Pragma", "No-cache");
            response.addDateHeader("Expires", 0);
            Usuario usuario = (Usuario) session.getAttribute("Usuario");
            AsesoresService aserv = new AsesoresService(usuario.getBd());
            ArrayList<Asesores> asesores = new ArrayList();
            asesores = aserv.buscarAsesoresActivos();
%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">
<!-- This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>. -->
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">

        <script type='text/javascript' src="<%= BASEURL%>/js/prototype.js"></script>
        <link href="<%= BASEURL%>/css/estilostsp.css" rel='stylesheet'>
        <script type='text/javascript' src="<%= BASEURL%>/js/boton.js"></script>


        <title>Traspaso de Cartera</title>

        <style type="text/css">
            td{ white-space:nowrap;}

            .filableach{
                color: #000000;
                font-family: Tahoma, Arial;
                background-color:#FFFFFF;
                font-size: 12px;
            }

            .divs {
                font-family: Verdana, Tahoma, Arial, Helvetica, sans-serif;
                font-size: small;
                color: #000000;
                background-color:#9CF49C;
                -moz-border-radius: 0.7em;
                -webkit-border-radius: 0.7em;
            }
        </style>

        <script type="text/javascript">

            function negociosAsesor(){
                var asesor= $("asesor").value;
                var p = "";
                if(asesor!=""){
                    var url = "<%=CONTROLLER%>?estado=Gestion&accion=Asesores";
                    p = "opcion=NEGOCIOS_ASESOR&asesor="+asesor;
                    new Ajax.Request(url, {
                        parameters: p,
                        asynchronous: false,
                        method: 'post',
                        onLoading: loading,
                        onComplete:  function (resp){
                            document.getElementById("negocios").innerHTML = resp.responseText;
                        }
                    });
                }
            }


            function loading(){
                document.getElementById("spangen").innerHTML = '<img alt="cargando" src="'+baseurl+'/images/cargando.gif" name="imgload">';
            }

            function aceptarCambios(){
                var asesor= $("asesor").value;
                var asesor_new= $("asesor_new").value;

                if(asesor_new!=""){
                    if(asesor_new!=asesor){
              
                        var url = "<%=CONTROLLER%>?estado=Gestion&accion=Asesores";
                   
                        var    p = "opcion=ACUALIZAR_ASESOR_NEGOCIOS&asesor_new="+asesor_new;
                    
                        new Ajax.Request(url, {
                            parameters: p,
                            asynchronous: false,
                            method: 'post',
                            onLoading: loading,
                            onComplete:  function (resp){
                                alert(resp.responseText);
                                document.getElementById("negocios").innerHTML = "";
                                $("asesor").value="";
                                $("asesor_new").value="";
                            }
                        });
                    }else{
                        alert ("Los asesores seleccionado no pueden ser iguales");
                    }
                }else{
                    alert("debe seleccionar un nuevo asesor");
                }
               
            }
        </script>


    </head>
    <body>
        <div id="capaSuperior" style="position:absolute; width:100%; height:100px; z-index:0; left: 0px; top: 0px;">

            <jsp:include page="/toptsp.jsp?encabezado=Traspaso de Cartera"/>

        </div>       
        <div id="capaCentral" style="position:absolute; width:100%; height:83%; z-index:0; left: 0px; top: 100px; overflow:scroll;">
            <form action='' name='formulario' method="post" >
                <table align="center" border="1" width="70%">
                    <tbody>
                        <tr>
                            <td style="padding: 0.3em;">
                                <div id="docs" style="width: 100%;">
                                    <table style="border-collapse:collapse; width:100%;" border="0">
                                        <tr class="filaazul">
                                            <td>
                                                Asesor
                                            </td>
                                            <td>
                                                <select id="asesor" name="asesor" style="width:20em;" onchange="negociosAsesor();" >
                                                    <option value="">...</option>
                                                    <%
                                                                for (int j = 0; j < asesores.size(); j++) {
                                                                    Asesores asesor = asesores.get(j);%>
                                                    <option value="<%=asesor.getIdusuario()%>" ><%=asesor.getNombre()%></option>
                                                    <%  }
                                                    %>
                                                </select>
                                                <span id="spangen"></span>

                                            </td>
                                            <td>
                                                Nuevo Asesor
                                            </td>
                                            <td>
                                                <select id="asesor_new" name="asesor_new" style="width:20em;">
                                                    <option value="">...</option>
                                                    <%
                                                                for (int j = 0; j < asesores.size(); j++) {
                                                                    Asesores asesor = asesores.get(j);%>
                                                    <option value="<%=asesor.getIdusuario()%>" ><%=asesor.getNombre()%></option>
                                                    <%  }
                                                    %>
                                                </select>
                                                <span id="spangen"></span>

                                            </td>
                                        </tr>
                                        <tr class="filaazul">
                                            <td colspan="4">
                                                <br>
                                                <br>
                                            </td>
                                        </tr>
                                    </table>

                                    <table style="border-collapse:collapse; width:100%;" border="0">
                                        <tr class="filableach">
                                            <td width="100%">
                                                <div id="negocios"></div>
                                            </td>

                                        </tr>
                                    </table>

                                </div>

                            </td>
                        </tr>
                    </tbody>
                </table>
            </form>
            <div align="center">
                <br>

                <img alt="" src = "<%=BASEURL%>/images/botones/aceptar.gif" style = "cursor:pointer" name = "imgaceptar" onclick = "aceptarCambios();" onmouseover="botonOver(this);" onmouseout="botonOut(this);">

                <img alt="" src = "<%=BASEURL%>/images/botones/salir.gif" style = "cursor:pointer" name = "imgsalir" onclick = "window.close();" onmouseover="botonOver(this);" onmouseout="botonOut(this);">
            </div>

        </div>

        <iframe  width=188 height=166 name="gToday:normal:agenda.js:gfPop:plugins.js" id="gToday:normal:agenda.js:gfPop:plugins.js" src="<%=BASEURL%>/js/Calendario/ipopeng.htm" scrolling="no" frameborder="0" style="visibility:visible; z-index:999; position:absolute; left:-500px; top:0px;"> </iframe>
    </body>
</html>
