<%-- 
    Document   : BuscarAfiliadoSede
    Created on : 02/04/2011, 012:03:01 PM
    Author     : Iris Vargas
--%>

<%@page contentType="text/html" pageEncoding="ISO-8859-1"%>
<%@include file="/WEB-INF/InitModel.jsp"%>
<%@page session="true"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">

<html>
    <head>

        <script src="<%=BASEURL%>/js/prototype.js"      type="text/javascript"></script>
        <script src="<%=BASEURL%>/js/boton.js"          type="text/javascript"></script>
        <script src="<%=BASEURL%>/js/effects.js"        type="text/javascript"></script>
        <script src="<%=BASEURL%>/js/window.js"         type="text/javascript"></script>
        <script src="<%=BASEURL%>/js/window_effects.js" type="text/javascript"></script>
        <link href="<%= BASEURL%>/css/estilostsp.css" rel="stylesheet" type="text/css">
        <link href="<%=BASEURL%>/css/mac_os_x.css"      rel="stylesheet" type="text/css">
        <link href="<%=BASEURL%>/css/default.css"       rel="stylesheet" type="text/css">
        <link href="<%=BASEURL%>/css/alert.css" rel="stylesheet" type="text/css"/>
        <link href="<%=BASEURL%>/css/alphacube.css" rel="stylesheet" type="text/css"/>
        <% String vista = request.getParameter("vista") != null ? request.getParameter("vista") : "1";
           String trans = request.getParameter("trans") != null ? request.getParameter("trans") : "N";
        %>
        <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
        <%if (trans.equals("S")) {%>
        <title>Buscar Transportador</title>
        <%} else {%>
        <title>Buscar Afiliado/Sede</title>
        <%}%>
        <script type="text/javascript">
            function buscarAfils(){
                var indi = $('filtro').selectedIndex;
                var filtro = $('filtro').options[indi].value;
                var cadena = $('cadena').value;
                var url = "<%= CONTROLLER%>?estado=Gestion&accion=Convenios";
                var p =  'tipo=buscarafised&filtro='+filtro+'&cadena='+cadena+'&trans=<%=trans%>';
             
                new Ajax.Request(
                url,
                {
                    method: 'post',
                    parameters: p,
                    onLoading: loading,
                    onComplete: llenarDiv
                }
            );
            }

            function llenarDiv(response){
                $("divres").innerHTML = response.responseText;
            }
            
            function loading(){
                var interno = '<div width="100%" align="center" class="fila">Procesando <img alt="cargando" src="<%= BASEURL%>/images/cargando.gif" name="imgload"></div>';
                $("divres").innerHTML = interno;
            }
            function asignarconv(id){
                document.getElementById("forma").action="<%=BASEURL%>/jsp/fenalco/convenio/AsignarConvenioAfiliado.jsp?id="+id+"&vista=<%=vista%>&trans=<%=trans%>";
                document.getElementById("forma").submit();
            }
            function detalle(){
                var id=document.getElementById("afi").value.replace(/^\s+/g,'').replace(/\s+$/g,'');
                    asignarconv(id);
                              
            }
        </script>
    </head>
    <body>
        <div id="capaSuperior" style="position:absolute; width:100%; height:100px; z-index:0; left: 0px; top: 0px;">
            <%if(!trans.equals("S")){%>
                <jsp:include page="/toptsp.jsp?encabezado=Buscar Afiliado/Sede"/>
            <%}else{%>
                <jsp:include page="/toptsp.jsp?encabezado=Buscar Transportador"/>
            <%}%>
        </div>
        <div id="capaCentral" style="position:absolute; width:100%; height:87%; z-index:0; left: 0px; top: 100px; overflow: scroll;">
            <div align="center">
                <form name="forma" id="forma" method="post" action="" >
                <table  border="1"width="50%" align="center">
                    <tr>
                        <td width="173" class="subtitulo1">&nbsp;Informaci&oacute;n</td>
                        <td width="205" class="barratitulo"><img src="<%=BASEURL%>/images/titulo.gif" width="32" height="20" align="left"></td>
                    </tr>
                    <tr class="fila">
                        <td colspan="2">Filtro: &nbsp;&nbsp;&nbsp;&nbsp;
                            <select name="filtro" id="filtro">
                                <option value="payment_name" selected>Nombre</option>
                                <option value="nit">Nit</option>
                            </select>&nbsp;&nbsp;&nbsp;&nbsp;
                            <input type="text" name="cadena" id="cadena" value="">
                            <img alt="" src="<%=BASEURL%>/images/botones/iconos/lupa.gif" width="15" height="15" style="cursor: pointer" onclick="buscarAfils();">

                        </td>

                    </tr>
                </table>
                <br>
                <div id="divres" style=" width: 90%;" align="center" ></div>
                <br>
                </form>
                
                <img alt="" src = "<%=BASEURL%>/images/botones/detalles.gif" style = "cursor:pointer" onclick='detalle();' name = "imgaceptar" id="imgaceptar"  onMouseOver="botonOver(this);" onMouseOut="botonOut(this);">
                <img alt="" src="<%=BASEURL%>/images/botones/salir.gif" style = "cursor:pointer" name = "imgsalirx" onclick = "window.close();" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);">
            </div>
        </div>
    </body>
</html>
