<%-- 
    Document   : AsignarConvenioAfiliado
    Created on : 4/04/2011, 10:11:51 AM
    Author     : Ivargas
--%>

<%@page import="com.tsp.operation.model.beans.AfiliadoConvenioRangos"%>
<%@page import="com.tsp.operation.model.beans.Usuario"%>
<%@page import="com.tsp.operation.model.beans.AfiliadoConvenio"%>
<%@page import="java.util.ArrayList"%>
<%@page contentType="text/html" pageEncoding="ISO-8859-1"%>
<%@include file="/WEB-INF/InitModel.jsp"%>
<%@page session="true"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">

<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
        <title>Asignar Convenio</title>

        <script src="<%=BASEURL%>/js/prototype.js"      type="text/javascript"></script>
        <script src="<%=BASEURL%>/js/boton.js"          type="text/javascript"></script>
        <script src="<%=BASEURL%>/js/effects.js"        type="text/javascript"></script>
        <script src="<%=BASEURL%>/js/window.js"         type="text/javascript"></script>
        <script src="<%=BASEURL%>/js/window_effects.js" type="text/javascript"></script>
        <link href="<%= BASEURL%>/css/estilostsp.css" rel="stylesheet" type="text/css">
        <link href="<%=BASEURL%>/css/mac_os_x.css"      rel="stylesheet" type="text/css">
        <link href="<%=BASEURL%>/css/default.css"       rel="stylesheet" type="text/css">
        <link href="<%=BASEURL%>/css/alert.css" rel="stylesheet" type="text/css"/>
        <link href="<%=BASEURL%>/css/alphacube.css" rel="stylesheet" type="text/css"/>
        <%

                    ArrayList lista = null;
                    ArrayList lista2 = null;
                    String id = request.getParameter("id") != null ? request.getParameter("id") : "";
                    String vista = request.getParameter("vista") != null ? request.getParameter("vista") : "1";
                    String trans = request.getParameter("trans") != null ? request.getParameter("trans") : "N";
                    ArrayList<AfiliadoConvenio> list = model.gestionConveniosSvc.buscarConvenios(id.trim());
                    String[] dato = model.proveedorService.obtenerDatAfi(id).split(";_;");
                    String idafi = dato[0];
                    String nombre = dato[1];
                    String sede = (dato[2].equals("S") ? "Si" : "No");
                    if (sede.equals("Si")) {
                        lista2 = model.proveedorService.buscarSects(idafi);
                        lista = model.proveedorService.buscarConvs(idafi);
                    } else {
                        lista2 = model.proveedorService.buscarSects();
                        if(trans.equals("S")){
                            lista = model.gestionConveniosSvc.buscarConveniosTipo("Microcredito",false);
                        }else{
                            lista = model.proveedorService.buscarConvs();
                        }
                    }

                    Usuario usuario = (Usuario)session.getAttribute("Usuario");

        %>
        <script type="text/javascript">
            var fily = 1;
            var maxfilas = 1;
            var indicador = 1;
            function buscarSubs(filind){
                indicador = filind;
                var sel = document.getElementById("sector"+filind).selectedIndex;
                var valor = document.getElementById("sector"+filind).options[sel].value;
                var url = "<%= CONTROLLER%>?estado=Proveedor&accion=Ingresar";
                var p =  'opcion=buscarsub&idsect='+valor;
                new Ajax.Request(
                url,
                {
                    method: 'post',
                    parameters: p,
                    onComplete: llenarDiv
                }
            );

            }
            function guardar (){
                document.getElementById("forma").action="<%=CONTROLLER%>?estado=Gestion&accion=Convenios&tipo=asignarconv&id=<%=id%>&maxfilas="+maxfilas+"&fils="+fily+"&vista=<%=vista%>&trans=<%=trans%>";
                document.getElementById("forma").submit();

            }

            function calculador(idv1,idv2){
                document.getElementById(idv2).value = 100 - unformatNumber(document.getElementById(idv1).value) * 1;
            }

            function llenarDiv(response){
                document.getElementById("subsector_"+indicador).innerHTML = '<select name="subsector'+indicador+'" id="subsector'+indicador+'" style="width: 100%;">'+response.responseText+'</select>';
            }

            function buscarDats(idfil){
                indicador = idfil;
                var sel = document.getElementById("conv"+idfil).selectedIndex;
                var valor = document.getElementById("conv"+idfil).options[sel].value;
                var url = "<%= CONTROLLER%>?estado=Proveedor&accion=Ingresar";
                var p =  'opcion=viewdats&idconv='+valor;
                new Ajax.Request(
                url,
                {
                    method: 'post',
                    parameters: p,
                    onComplete: llenarDiv2
                }
            );
            }

            function llenarDiv2(response){
                var texto = response.responseText;
                var array = texto.split(";_;");
                document.getElementById("tasa"+indicador).value = array[0];
                document.getElementById("custodia"+indicador).value = formatNumber(array[1]);
            }
            function soloNumeros(id) {
                var precio = document.getElementById(id).value;
                precio =  precio.replace(/[^0-9^.]+/gi,"");
                document.getElementById(id).value=precio;
            }
            function validateValue(id){
                var vali = document.getElementById("ricomision"+id).value * 1;
                var valf = document.getElementById("rfcomision"+id).value * 1;
                if(valf<=vali){
                    document.getElementById("rfcomision"+id).value = vali + 1;
                }
            }

            function validateValueAnt(id){
                var indic = id.substring(id.indexOf("_") + 1);
                var prev = id.substring(0,id.indexOf("_"));
                if((id.substring(id.indexOf("_") + 1 )*1)>1){
                    var vali = document.getElementById("rfcomision"+ prev + '_' + (indic - 1)).value * 1;
                    var valf = document.getElementById("ricomision"+id).value * 1;
                    if(valf<=vali){
                        document.getElementById("ricomision"+id).value = vali + 1;
                    }
                }
            }
            function unformatNumber(num) {
                return num.replace(/([^0-9\.\-])/g,'')*1;
            }
            function formatNumber(num,prefix){
                prefix = prefix || '';
                num += '';
                var splitStr = num.split('.');
                var splitLeft = splitStr[0];
                var splitRight = splitStr.length > 1 ? '.' + splitStr[1] : '';
                splitRight = splitRight.substring(0,3);//090929
                var regx = /(\d+)(\d{3})/;
                while (regx.test(splitLeft)) {
                    splitLeft = splitLeft.replace(regx, '$1' + ',' + '$2');
                }
                return prefix + splitLeft + splitRight;
            }
            function uncheck(checkElement,fila){
                if(checkElement.checked==true){
                    document.getElementById('porc_afil'+fila).readOnly=false;
                    document.getElementById('cuenta'+fila).readOnly=false;
                }
                else{
                    document.getElementById('porc_afil'+fila).readOnly=true;
                    document.getElementById('porc_afil'+fila).value='0';
                    document.getElementById('porc_ven'+fila).value='0';
                    document.getElementById('cuenta'+fila).readOnly=true;
                    document.getElementById('cuenta'+fila).value='';
                }
            }
            function regresar(){
                document.getElementById("forma").action="<%=BASEURL%>/jsp/fenalco/convenio/BuscarAfiliadoSede.jsp?vista=<%=vista%>";
                document.getElementById("forma").submit();

            }

            function TDEliminarRegistro(imagen,tablaId){
                var i=imagen.parentNode.parentNode.rowIndex;
                $(tablaId).deleteRow(i);
                items = items - 1;
            }

            function TDAgregarRegistro(tablaId,prefix){
                myTable = $(tablaId);
                //alert(tablaId);
                items = (myTable.rows.length)*1 - 1;
                //alert('rf'+prefix+items);
                var val = (document.getElementById('rf'+prefix+items).value)*1 + 1;
                if(navigator.appName=='Microsoft Internet Explorer')
                {   oRow = myTable.insertRow();
                }else{
                    oRow = myTable.insertRow(-1);
                }
                items = items + 1;
                maxfilas = items>maxfilas? items: maxfilas;
                if(navigator.appName=='Microsoft Internet Explorer'){oCell = oRow.insertCell();}else{oCell = oRow.insertCell(-1);}oCell.align='center';oCell.style.backgroundColor='#EAFFEA';
                oCell.innerHTML = '<img style="cursor: pointer" onclick="TDAgregarRegistro(\''+tablaId+'\',\''+prefix+'\')" src=\'<%=BASEURL%>/images/botones/iconos/mas.gif\' width=\'12\' height=\'12\'><img style="cursor: pointer" onclick="TDEliminarRegistro(this,\''+tablaId+'\')" src=\'<%=BASEURL%>/images/botones/iconos/menos1.gif\' width=\'12\' height=\'12\' >';
                if(navigator.appName=='Microsoft Internet Explorer'){oCell = oRow.insertCell();}else{oCell = oRow.insertCell(-1);}oCell.align='left';oCell.style.backgroundColor='#EAFFEA';
                oCell.innerHTML = '<input  onkeyup="soloNumeros(this.id);" type="text" id="ri'+prefix+items+'" name="ri'+prefix+items+'" value='+val+' maxlength="5" size="5" style="text-align: right;">';
                if(navigator.appName=='Microsoft Internet Explorer'){oCell = oRow.insertCell();}else{oCell = oRow.insertCell(-1);}oCell.align='left';oCell.style.backgroundColor='#EAFFEA';
                oCell.innerHTML = '<input  onkeyup="soloNumeros(this.id);validateValueAnt(\''+fily+"_"+items+'\');" onblur="validateValue(\''+fily+"_"+items+'\');validateValueAnt(\''+fily+"_"+items+'\');" type="text" id="rf'+prefix+items+'" name="rf'+prefix+items+'" maxlength="5" size="5" style="text-align: right;">';
                if(navigator.appName=='Microsoft Internet Explorer'){oCell = oRow.insertCell();}else{oCell = oRow.insertCell(-1);}oCell.align='left';oCell.style.backgroundColor='#EAFFEA';
                oCell.innerHTML = '<input onkeyup="soloNumeros(this.id);this.value = formatNumber(this.value);" type="text" id="p'+prefix+items+'" name="p'+prefix+items+'" maxlength="5" size="5" style="text-align: right;">';//items++;
            }
            
            function delrow(row){
                var myTable = $("tconvs");
                var i=row.parentNode.parentNode.rowIndex;
                myTable.deleteRow(i);
                           <%if(!trans.equals("S")){%>
                myTable.deleteRow(i);
                          <%}%>
            }
            function insertcelda(rowobject,dato){
                var celdita = null;
                if(navigator.appName=='Microsoft Internet Explorer') celdita = rowobject.insertCell();
                else celdita = rowobject.insertCell(-1);
                celdita.innerHTML = dato;
                return celdita;
            }

            function addrow(fit){
                fily += 1;
                fit = fily;
                fit -= 1;
                var celda = '<img alt="+" style="cursor: pointer" onclick="addrow('+fily+');" src="<%=BASEURL%>/images/botones/iconos/mas.gif" width="12" height="12" >'
                celda += '<img alt="-"style="cursor: pointer" onclick="delrow(this);" src="<%=BASEURL%>/images/botones/iconos/menos1.gif" width="12" height="12" >'
                var myTable = $("tconvs");
                var myRow = null;
                if(navigator.appName=='Microsoft Internet Explorer') myRow = myTable.insertRow();
                else myRow = myTable.insertRow(-1);
                myRow.className = "fila";
                var myCell = null;
                myCell = insertcelda(myRow,celda);
                myCell.style.width="26px";
                celda = '<select name="conv'+fily+'" id="conv'+fily+'" style="width: 100%;"  onchange="buscarDats('+fily+');">' +
                    '<option selected="selected" value="">...</option>'+
            <%
                        if (lista != null) {
                            String[] split = null;
                            for (int i = 0; i < lista.size(); i++) {
                                split = ((String) (lista.get(i))).split(";_;");
            %>
                    '<option value="<%=split[0]%>"><%=split[1]%></option>' +
            <%
                            }
                        }
            %>
                    '</select>';
                    myCell = insertcelda(myRow,celda);
                    myCell.style.width="87px";
                    celda = '<select name="sector'+fily+'" id="sector'+fily+'" style="width: 100%;" onchange="buscarSubs('+fily+');">' +
                        '<option selected="selected" value="">...</option>'+
            <%
                        if (lista2 != null) {
                            String[] split = null;
                            for (int i = 0; i < lista2.size(); i++) {
                                split = ((String) (lista2.get(i))).split(";_;");
            %>
                    '<option value="<%=split[0]%>"><%=split[1]%></option>' +
            <%
                            }
                        }
            %>
                    '</select>';
                    myCell = insertcelda(myRow,celda);
                    myCell.style.width="85px";
                    celda = '<div id="subsector_'+fily+'"><select name="subsector'+fily+'" id="subsector'+fily+'" style="width: 100%;">' +
                        '<option selected="selected" value="">...</option>'+
                        '</select></div>';//<!-- 20101111 -->
                    myCell = insertcelda(myRow,celda);
                    <%if(!trans.equals("S")){%>
                    myCell.style.width="87px";
                    celda = '<input type="text" value="0" id="cobertura'+fily+'" name="cobertura'+fily+
                        '" onkeyup="soloNumeros(this.id);this.value = formatNumber(this.value);" style="text-align: right; width: 100%">';
                    myCell = insertcelda(myRow,celda);
                    myCell.style.width="77px";
                    celda = '<input type="text" size="5" value="0" id="cobert_flotante'+fily+'" name="cobert_flotante'+fily+
                        '" onkeyup="soloNumeros(this.id);this.value = formatNumber(this.value);" style="text-align: right; width: 100%">' ;
                    myCell = insertcelda(myRow,celda);
                    <%}%>
                    myCell.style.width="45px";
                    celda = '<input type="text" size="5" value="0"  id="tasa'+fily+'" name="tasa'+fily+ '" onfocus="<%=(!vista.equals("2")?"this.blur();":"")%>"'+
                        ' onkeyup="soloNumeros(this.id);this.value = formatNumber(this.value);" style="text-align: right; width: 100%">' ;
                    myCell = insertcelda(myRow,celda);
                    <%if(!trans.equals("S")){%>
                    myCell.style.width="45px";
                    celda = '<input type="text" size="5" value="0" id="custodia'+fily+'" name="custodia'+fily+ '" onfocus="<%=(!vista.equals("2")?"this.blur();":"")%>"'+
                        ' onkeyup="soloNumeros(this.id);this.value = formatNumber(this.value);" style="text-align: right; width: 100%">' ;
                    myCell = insertcelda(myRow,celda);
                    myCell.style.width="45px";
                    celda = '<input name="comision'+fily+'" id="comision'+fily+'" value="true" type="checkbox" onclick="uncheck(this,\''+fily+'\');">';
                    myCell = insertcelda(myRow,celda);
                    myCell.style.width="45px";
                    myCell.align = "center";
                    /*celda = '<select name="cuenta'+fily+'" id="cuenta'+fily+'" style="width: 100%;">' +
                                '<option selected="selected" value="">...</option>'+
                            '</select>';*/
                    celda = '<input type="text" id="cuenta'+fily+'" name="cuenta'+fily+'" readonly value="" style="text-align: right; width: 100%">';
                    myCell = insertcelda(myRow,celda);
                    //myCell.style.width="85px";
                    myCell.style.width="80px";
                    celda = '<input type="text" size="5" value="0" id="porc_afil'+fily+'" name="porc_afil'+fily+
                        '" readonly onkeyup="soloNumeros(this.id);this.value = formatNumber(this.value);calculador(\'porc_afil'+fily+'\',\'porc_ven'+fily+'\');" style="text-align: right; width: 100%">' ;
                    myCell = insertcelda(myRow,celda);
                    myCell.style.width="50px";
                    celda = '<input type="text" size="5" value="0" id="porc_ven'+fily+'" name="porc_ven'+fily+
                        '" onkeyup="soloNumeros(this.id);this.value = formatNumber(this.value);" readonly style="text-align: right; width: 100%">' ;
                    myCell = insertcelda(myRow,celda);
                    myCell.style.width="50px";
                    if(navigator.appName=='Microsoft Internet Explorer') myRow = myTable.insertRow();
                    else myRow = myTable.insertRow(-1);
                    myRow.className = "fila";
                    celda = '<table border="0" width="100%" id="tcomision'+fily+'" style="text-align: left; margin-left: 30px; margin-right: auto; width: 200px;">' +
                        '<tr>' +
                        '<td align="left" width="20%" class="fila">&nbsp;</td>' +
                        '<td align="left" width="35%" class="fila">Inicio</td>' +
                        '<td align="left" width="15%" class="fila">Fin</td>' +
                        '<td align="left" width="25%" class="fila">Porcentaje</td>' +
                        '</tr>' +
                        '<tr>' +
                        '<td align="center" class="fila">' +
                        '<img alt="+" style="cursor: pointer" onclick="TDAgregarRegistro(\'tcomision'+fily+'\',\'comision'+fily+'_\')" src="<%=BASEURL%>/images/botones/iconos/mas.gif" width="12" height="12" ><img alt="-" style="cursor: pointer" onclick="TDEliminarRegistro(this,\'tcomision'+fily+'\')" src="<%=BASEURL%>/images/botones/iconos/menos1.gif" width="12" height="12" >' +
                        '</td>' +
                        '<td align="left" class="fila">' +
                        '<input onkeyup="soloNumeros(this.id);" type="text" id="ricomision'+fily+'_1" name="ricomision'+fily+'_1" value="1" readonly maxlength="5" size="5" style="text-align: right;">' +
                        '</td>' +
                        '<td align="left" class="fila">' +
                        '<input onkeyup="soloNumeros(this.id);" onblur="validateValue(\''+fily+'_1\');" type="text" id="rfcomision'+fily+'_1" name="rfcomision'+fily+'_1" maxlength="5" size="5" style="text-align: right;">' +
                        '</td>' +
                        '<td align="left" class="fila">' +
                        '<input onkeyup="soloNumeros(this.id);this.value = formatNumber(this.value);" type="text" id="pcomision'+fily+'_1" name="pcomision'+fily+'_1" maxlength="5" size="5" style="text-align: right;">' +
                        '</td>' +
                        '</tr>' +
                        '</table>';
                    myCell = insertcelda(myRow,celda);
                    myCell.colSpan = "12";
                     <%}%>
                    
                }

                function deshab() {
                  var   frm = document.forms['forma'];
                    for(i=0; ele=frm.elements[i]; i++)
                        ele.disabled=true;
                }
        </script>
    </head>
    <body onload="<%=(vista.equals("1")?"deshab();":"")%>" >
        <div id="capaSuperior" style="position:absolute; width:100%; height:100px; z-index:0; left: 0px; top: 0px;">
            <jsp:include page="/toptsp.jsp?encabezado=Asignar Convenio"/>
        </div>
        <div id="capaCentral" style="position:absolute; width:100%; height:87%; z-index:0; left: 0px; top: 100px; overflow: scroll;">
            <div align="center">

                <form name="forma" id="forma" method="post" action="" >
                    <table  border="1"width="90%" align="center">
                        <tr>
                            <td  class="subtitulo1">&nbsp;Afiliado</td>
                            <td colspan="2" class="barratitulo"><img src="<%=BASEURL%>/images/titulo.gif" width="32" height="20" align="left"></td>
                        </tr>
                        <tr class="fila">
                            <td >Identificacion: &nbsp;&nbsp;&nbsp;&nbsp; <%=id%></td>
                            <td >Nombre: &nbsp;&nbsp;&nbsp;&nbsp; <%=nombre%></td>
                            <td >Sede: &nbsp;&nbsp;&nbsp;&nbsp; <%=sede%></td>
                        </tr>
                    </table>
                    <br>
                    <table id="tconvs" style="text-align: left; width: 90%; " border="0">
                        <tr class="subtitulo1">
                             <% if (!vista.equals("1")){%>
                            <th style="vertical-align: top; width: 31px;">&nbsp;</th>
                            <%}%>
                            <th style="vertical-align: top; width: 105px;">Convenio
                            </th>
                            <th style="vertical-align: top; width: 100px;">Sector
                            </th>
                            <th style="vertical-align: top; width: 102px;">Subsector
                            </th>
                            <%if(!trans.equals("S")){%>
                            <th style="vertical-align: top; width: 95px;">Cobertura
                            </th>
                            <th style="vertical-align: top; width: 55px;">% flotante
                            </th>
                            <%}%>
                            <th style="vertical-align: top; width: 50px;">Interes
                            </th>
                            <%if(!trans.equals("S")){%>
                            <th style="vertical-align: top; width: 40px;">Custodia
                            </th>
                            <th style="vertical-align: top; width: 45px;">Comision
                            </th>
                            <th style="vertical-align: top; width: 95px;">Cuenta
                            </th>
                            <th style="vertical-align: top; width: 55px;">% afiliado
                            </th>
                            <th style="vertical-align: top; width: 50px;">% vendedor
                            </th>
                             <%}%>
                        </tr>
                        <tbody>
                            <% int tam;
                                        tam = list.size() > 0 ? list.size() : 1;
                             %>
                        <script type="text/javascript" >
                            fily=<%=tam%>;
                        </script>
                            <% 
                            for (int j = 1; j <= tam; j++) {
                                            AfiliadoConvenio aficon=list.size() > 0 ? list.get(j-1) : null;
                            %>
                            <tr class="fila">
                                 <% if (!vista.equals("1")){%>
                                <td style="vertical-align: top; width: 26px;">
                                    <input type="hidden" name="idprovconv<%=j%>"  id="idprovconv<%=j%>" value="<%=aficon!=null?aficon.getIdProvConvenio():0%>">
                                    <img alt="+" style="cursor: pointer" onclick="addrow(<%=j%>);" src='<%=BASEURL%>/images/botones/iconos/mas.gif' width='12' height='12' ><img alt="-"style="cursor: pointer" onclick="delrow(this);" src='<%=BASEURL%>/images/botones/iconos/menos1.gif' width='12' height='12' >
                                </td>
                                <%}%>
                                <td style="vertical-align: top; width: 87px;">
                                    <select name="conv<%=j%>" id="conv<%=j%>" style="width: 100%;" onchange="buscarDats(<%=j%>);">
                                        <option  value="">...</option>
                                        <%
                                                                        if (lista != null) {
                                                                            String[] split = null;
                                                                            for (int i = 0; i < lista.size(); i++) {
                                                                                split = ((String) (lista.get(i))).split(";_;");
                                                                                out.print("<option value='" + split[0] + "'"+((aficon!=null?aficon.getIdConvenio():"").equals(split[0])?"selected='selected'":"")+">" + split[1] + "</option>");
                                                                            }
                                                                        }
                                        %>
                                    </select>
                                </td>
                                <td style="vertical-align: top; width: 85px;">
                                    <select name="sector<%=j%>" id="sector<%=j%>" style="width: 100%;" onchange="buscarSubs(<%=j%>);">
                                        <option value="">...</option>
                                        <%
                                                                        if (lista2 != null) {
                                                                            String[] split = null;
                                                                            for (int i = 0; i < lista2.size(); i++) {
                                                                                split = ((String) (lista2.get(i))).split(";_;");
                                                                                out.print("<option value='" + split[0] + "'"+((aficon!=null?aficon.getCodSector():"").equals(split[0])?"selected='selected'":"")+">" + split[1] + "</option>");
                                                                            }
                                                                        }
                                        %>
                                    </select>

                                </td>
                                <td style="vertical-align: top; width: 87px;"><!-- 20101111 -->
                                    <div id="subsector_<%=j%>">
                                        <select name="subsector<%=j%>" id="subsector<%=j%>" style="width: 100%;">
                                            <% if(aficon!=null){
                                                model.sectorsubsectorSvc.buscar_subsector(aficon.getCodSector(), aficon.getCodSubsector(), usuario.getBd());
                                                 %>
                                            <option  value="<%=aficon.getCodSubsector()%>"><%=model.sectorsubsectorSvc.getNombre_sector()%></option>
                                            <%}else{%>
                                            <option value="">...</option>
                                            <%}%>
                                        </select>
                                    </div>
                                </td>
                                <%if(!trans.equals("S")){%>
                                <td style="vertical-align: top; width: 77px;">
                                    <input type="text"value="<%=aficon!=null?aficon.getValorCobertura():0%>" id="cobertura<%=j%>" name="cobertura<%=j%>" onkeyup="soloNumeros(this.id);this.value = formatNumber(this.value);" style="text-align: right; width: 100%">
                                </td>
                                <td style="vertical-align: top; width: 45px;">
                                    <input type="text" size="5" value="<%=aficon!=null?aficon.getPorcCoberturaFlotante():0%>" id="cobert_flotante<%=j%>" name="cobert_flotante<%=j%>" onkeyup="soloNumeros(this.id);this.value = formatNumber(this.value);" style="text-align: right; width: 100%">
                                </td>
                                <%}%>
                                <td style="vertical-align: top; width: 45px;">
                                    <input type="text" size="5" name="tasa<%=j%>" onfocus="<%=(!vista.equals("2")?"this.blur();":"")%>" id="tasa<%=j%>" value="<%=aficon!=null?aficon.getTasaInteres():0%>" style="text-align: right; width: 100%" onkeyup="soloNumeros(this.id);">
                                </td>
                                <%if(!trans.equals("S")){%>
                                <td style="vertical-align: top; width: 45px;">
                                    <input type="text" size="5" name="custodia<%=j%>"  onfocus="<%=(!vista.equals("2")?"this.blur();":"")%>" id="custodia<%=j%>" value="<%=aficon!=null?aficon.getValorCustodia():0%>" style="text-align: right; width: 100%" onkeyup="soloNumeros(this.id);this.value = formatNumber(this.value);">
                                </td>
                                <td style="vertical-align: top; width: 45px;" align="center">
                                    <input id="comision<%=j%>" name="comision<%=j%>" value="true" type="checkbox" onclick="uncheck(this,'<%=j%>');" <%=aficon!=null?(aficon.getComision().equals("t")?"checked":""):""%>>
                                </td>
                                <td style="vertical-align: top; width: 80px;">
                                    <input <%=aficon!=null?(aficon.getComision().equals("t")?"":"readonly='readonly'"):"readonly='readonly'"%>  type="text" id="cuenta<%=j%>" name="cuenta<%=j%>" value="<%=aficon!=null?aficon.getCuentaComision():""%>" style="text-align: right; width: 100%">
                                </td>
                                <td style="vertical-align: top; width: 50px;">
                                    <input <%=aficon!=null?(aficon.getComision().equals("t")?"":"readonly='readonly'"):"readonly='readonly'"%> type="text" size="5" id="porc_afil<%=j%>" name="porc_afil<%=j%>" onkeyup="soloNumeros(this.id);this.value = formatNumber(this.value);calculador('porc_afil1','porc_ven1');" value="<%=aficon!=null?aficon.getPorcentajeAfiliado():0%>" style="text-align: right; width: 100%">
                                </td>
                                <td style="vertical-align: top; width: 50px;">
                                    <input type="text" readonly="readonly" size="5" id="porc_ven<%=j%>" name="porc_ven<%=j%>" onkeyup="soloNumeros(this.id);this.value = formatNumber(this.value);" value="<%=aficon!=null?100-Integer.parseInt(aficon.getPorcentajeAfiliado()):0%>" style="text-align: right; width: 100%">
                                </td>
                                <%}%>
                            </tr>
                            <%if(!trans.equals("S")){%>
                            <tr class="fila">
                                <td colspan="12">
                                    <table border="0" width="100%" id="tcomision<%=j%>" style="text-align: left; margin-left: 30px; margin-right: auto; width: 200px;">
                                        <tr>
                                             <% if (!vista.equals("1")){%>
                                            <td align="left" width="20%" class="fila">&nbsp;</td>
                                            <%}%>
                                            <td align="left" width="35%" class="fila">Inicio</td>
                                            <td align="left" width="15%" class="fila">Fin</td>
                                            <td align="left" width="25%" class="fila">Porcentaje</td>
                                        </tr>
                                        <%int tam2;
                                                                        tam2 = (aficon!=null?aficon.getRangos().size():0) > 0 ? aficon.getRangos().size() : 1;
                                                                        for (int k = 1; k <= tam2; k++) {
                                                                            AfiliadoConvenioRangos aficonran=(aficon!=null?(aficon.getRangos().size()>0?aficon.getRangos().get(k-1):null) : null);
                                    %>
                                        <tr>
                                             <% if (!vista.equals("1")){%>
                                            <td align="center" class="fila">
                                                <img alt="+" style="cursor: pointer" onclick="TDAgregarRegistro('tcomision<%=j%>','comision<%=j%>_')" src='<%=BASEURL%>/images/botones/iconos/mas.gif' width='12' height='12' ><img alt="-"style="cursor: pointer" onclick="TDEliminarRegistro(this,'tcomision<%=j%>')" src='<%=BASEURL%>/images/botones/iconos/menos1.gif' width='12' height='12' >
                                            </td>
                                            <%}%>
                                            <td align="left" class="fila">
                                                <input onkeyup="soloNumeros(this.id);" type="text" id="ricomision<%=j%>_<%=k%>" name="ricomision<%=j%>_<%=k%>" value="<%=aficonran!=null?aficonran.getCuotaIni():1%>" readonly maxlength="5" size="5" style="text-align: right;">
                                            </td>
                                            <td align="left" class="fila">
                                                <input onkeyup="soloNumeros(this.id);" onblur="validateValue('<%=j%>_<%=k%>');" type="text" id="rfcomision<%=j%>_<%=k%>" value="<%=aficonran!=null?aficonran.getCuotaFin():""%>" name="rfcomision<%=j%>_<%=k%>" maxlength="5" size="5" style="text-align: right;">
                                            </td>
                                            <td align="left" class="fila">
                                                <input onkeyup="soloNumeros(this.id);this.value = formatNumber(this.value);" type="text" id="pcomision<%=j%>_<%=k%>"  value="<%=aficonran!=null?aficonran.getPorcentajeComision():""%>"name="pcomision<%=j%>_<%=k%>" maxlength="5" size="5" style="text-align: right;">
                                            </td>
                                        </tr>
                                        <%}%>
                                    </table>
                                </td>
                            </tr>
                            <%}}%>
                        </tbody>
                    </table>
                    <br>
                    <br>
                </form>
                <img alt="regresar" src="<%= BASEURL%>/images/botones/regresar.gif"  name="imgregresar"   onMouseOver="botonOver(this);" onClick="regresar();" onMouseOut="botonOut(this);"  style="cursor:pointer ">
                <% if (!vista.equals("1")){%>
                <img alt="" src="<%= BASEURL%>/images/botones/aceptar.gif"  name="imgaceptar" onClick="guardar();" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" style="cursor:pointer;">
                <%}%>
                &nbsp; <img alt="" src="<%= BASEURL%>/images/botones/salir.gif"  name="imgsalir"  onMouseOver="botonOver(this);" onClick="parent.close();" onMouseOut="botonOut(this);"  style="cursor:pointer;">
            </div>
        </div>
    </body>
</html>
