<%-- 
    Document   : Asesores
    Created on :26/12/2011, 09:45:11 AM
    Author     : ivargas
--%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@page session="true"%>
<%@page import="java.util.*"%>
<%@page import="com.tsp.operation.model.*"%>
<%@page import="com.tsp.operation.model.beans.*"%>
<%@page import="com.tsp.operation.model.services.*"%>
<%@page import="com.tsp.util.*"%>
<%@include file="/WEB-INF/InitModel.jsp"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
        <title>Asesores</title>
        <script src="<%=BASEURL%>/js/prototype.js"      type="text/javascript"></script>
        <script src="<%=BASEURL%>/js/boton.js"          type="text/javascript"></script>
        <link href="<%= BASEURL%>/css/estilostsp.css" rel="stylesheet" type="text/css">
        <%
                    String agregar = request.getParameter("agregar") != null ? request.getParameter("agregar") : "N";
                    ArrayList<Asesores> asesores = new ArrayList();
                    AsesoresService aserv = new AsesoresService();
                    asesores = aserv.buscarAsesores();
                    int tam = asesores.size();
                    String msg = request.getParameter("msg") != null ? request.getParameter("msg") : "";
        %>
        <script type="text/javascript">
      
            function buscarUsu(item){
                var codigo = $('codigousuario'+item).value;
                var url = "<%= CONTROLLER%>?estado=Gestion&accion=Asesores";
                var p =  'opcion=BUSCAR_USUA&usuariocod='+codigo;
                new Ajax.Request(
                url,
                {
                    method: 'post',
                    parameters: p,
                    onComplete: function (response){
                        var array = response.responseText.split(";_;");
                        if(array[0]!=""){
                        $('id'+item).value = codigo;
                        $('nitusuario'+item).value = array[0];
                        $('nomusuario'+item).value = array[1];
                        }
                    }
                }
            );
            }           

            var filaconds = <%=tam%>;
            function agregarFila(tablaId){
                var myTable = $(tablaId);
                var oRow;
                if(navigator.appName=='Microsoft Internet Explorer'){
                    oRow = myTable.insertRow();
                }else{
                    oRow = myTable.insertRow(-1);
                }
                oRow.className='fila';
                filaconds += 1;
                var oCell=oRow.insertCell(0);
                oCell.innerHTML = '<input type="hidden" id="id'+filaconds+'" name="id'+filaconds+'" value="">'+
                    '<img alt="+" src="<%=BASEURL%>/images/botones/iconos/mas.gif" width="15" height="15" style="cursor: pointer" onclick="agregarFila(\'tablaasesor\')">';
                oCell=oRow.insertCell(1);
                oCell.innerHTML = '<input type="text" name="codigousuario'+filaconds+'" id="codigousuario'+filaconds+'" value="" size="10">'+
                    '<img alt="" src="<%=BASEURL%>/images/botones/iconos/lupa.gif" width="15" height="15" style="cursor: pointer" onclick="buscarUsu('+filaconds+');">';
                oCell=oRow.insertCell(2);
                oCell.innerHTML = '<input type="text" name="nitusuario'+filaconds+'" id="nitusuario'+filaconds+'" value="" size="15" readonly="readonly">';
                oCell=oRow.insertCell(3);
                oCell.innerHTML = '<input type="text" name="nomusuario'+filaconds+'" id="nomusuario'+filaconds+'" value="" size="40" readonly="readonly">';
                oCell=oRow.insertCell(4);
                oCell.innerHTML = '<input type="checkbox" name="activo'+filaconds+'" id="activo'+filaconds+'" value="S" checked="checked">';
            }

            function enviar(){
                var sizetabla = $('tablaasesor').rows.length;
                for (var i=0; i<sizetabla-1;i++){
                    if($('id'+i).value!=$('codigousuario'+i).value){
                        alert("Debe confirmar que exista el usuario de la fila "+ (i+1));
                        return false;
                    }
                }

                var action = $('formulario').action + "&opcion=GRABAR&filastabla="+(sizetabla - 1);
                $('formulario').action = action;
                $('formulario').submit();

            }

            
        </script>

    </head>
    <body>
        <div id="capaSuperior" style="position:absolute; width:100%; height:100px; z-index:0; left: 0px; top: 0px;">
            <jsp:include page="/toptsp.jsp?encabezado=Asesores"/>
        </div>
        <div id="contenido" style="visibility: hidden; display: none; width: 600px; height: 300px; background-color: #EEEEEE;">

        </div>

        <div id="capaCentral" style="position:absolute; width:100%; height:85%; z-index:0; left: 0px; top: 100px; overflow:auto;">
            <form action="<%=CONTROLLER%>?estado=Gestion&accion=Asesores" id="formulario" name="formulario" method="post">
                <div align="center">
                    <table border="2" width="70%">
                        <tr class="fila">
                            <td>                               
                                <div style="width: 100%;  overflow: auto;" id="tablaconvs">
                                    <table border="0" width="100%" id="tablaasesor">
                                        <tr class="subtitulo1">
                                            <td></td>
                                            <td>Usuario</td>
                                            <td>Identificaci&oacute;n</td>
                                            <td>Nombre</td>
                                            <td>Activo</td><!-- wtf? -->
                                        </tr>
                                        <% for (int i = 0; i < asesores.size(); i++) {%>
                                        <tr class="fila">
                                            <td>
                                                <input type="hidden" id="id<%=i%>" name="id<%=i%>" value="<%=asesores.get(i).getIdusuario()%>">

                                            </td>
                                            <td>
                                                <input type="text" name="codigousuario<%=i%>" id="codigousuario<%=i%>" value="<%=asesores.get(i).getIdusuario()%>" size="10" readonly="readonly">
                                            </td>
                                            <td>
                                                <input type="text" name="nitusuario<%=i%>" id="nitusuario<%=i%>" value="<%=asesores.get(i).getIdentificacion()%>" size="15" readonly="readonly">
                                            </td>
                                            <td>
                                                <input type="text" name="nomusuario<%=i%>" id="nomusuario<%=i%>" value="<%=asesores.get(i).getNombre()%>" size="40" readonly="readonly">
                                            </td>
                                            <td>
                                                <input type="checkbox" name="activo<%=i%>" id="activo<%=i%>" value="S" <%=asesores.get(i).getRegStatus().equals("") ? "checked" : ""%> <%=  (agregar.equals("S")) ? "" : "disabled"%> >
                                            </td>
                                        </tr>

                                        <%        }

                                                    if (agregar.equals("S")) {
                                        %>
                                        <tr class="fila">
                                            <td>
                                                <input type="hidden" id="id<%=tam%>" name="id<%=tam%>" value="">

                                                <img alt="+" src="<%=BASEURL%>/images/botones/iconos/mas.gif" width="15" height="15" style="cursor: pointer" onclick="agregarFila('tablaasesor')">

                                            </td>
                                            <td>
                                                <input type="text" name="codigousuario<%=tam%>" id="codigousuario<%=tam%>" value="" size="10">
                                                <img alt="" src="<%=BASEURL%>/images/botones/iconos/lupa.gif" width="15" height="15" style="cursor: pointer" onclick="buscarUsu(<%=tam%>);">

                                            </td>
                                            <td>
                                                <input type="text" name="nitusuario<%=tam%>" id="nitusuario<%=tam%>" value="" size="15" readonly="readonly">
                                            </td>
                                            <td>
                                                <input type="text" name="nomusuario<%=tam%>" id="nomusuario<%=tam%>" value="" size="40" readonly="readonly">
                                            </td>
                                            <td>
                                                <input type="checkbox" name="activo<%=tam%>" id="activo<%=tam%>" value="S" checked="checked" <%=  (agregar.equals("S")) ? "" : "disabled"%> >
                                            </td>
                                        </tr>
                                        <%}%>
                                    </table>
                                </div>                                
                                <div align="center"  class="fila">
                                    <br>
                                    <%
                                                if (agregar.equals("S")) {
                                    %>
                                    <img alt="" src = "<%=BASEURL%>/images/botones/aceptar.gif" style="cursor:pointer" onclick="enviar();" name= "imgaceptar" id="imgaceptar"  onMouseOver="botonOver(this);" onMouseOut="botonOut(this);">
                                    <%
                                                }
                                    %>
                                    <img alt="" src = "<%=BASEURL%>/images/botones/salir.gif" style = "cursor:pointer" name = "imgsalir" onClick = "window.close();" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);">
                                </div>
                            </td>
                        </tr>
                    </table>
                    <% if (!msg.equals("")) {%>
                    <table border="2" align="center">
                        <tr>
                            <td><table width="100%"  border="1" align="center"  bordercolor="#F7F5F4" bgcolor="#FFFFFF">
                                    <tr>
                                        <td width="229" align="center" class="mensajes"><%=msg%></td>
                                        <td width="29" background="<%=BASEURL%>/images/cuadronaranja.JPG">&nbsp;</td>
                                        <td width="58">&nbsp;</td>
                                    </tr>
                                </table></td>
                        </tr>
                    </table>
                    <%}%>
                </div>
            </form>
        </div>
    </body>
</html>
