<%-- 
    Document   : gSectores
    Created on : 2/08/2010, 10:09:32 AM
    Author     : maltamiranda
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@page session    ="true"%>
<%@page errorPage  ="/error/ErrorPage.jsp"%>
<%@include file    ="/WEB-INF/InitModel.jsp"%>
<%@page import    ="java.util.*" %>
<%@page import    ="com.tsp.util.*" %>
<%@page import    ="com.tsp.operation.model.beans.Usuario" %>

<script src="<%=BASEURL%>/js/prototype.js"      type="text/javascript"></script>
<script src="<%=BASEURL%>/js/boton.js"          type="text/javascript"></script>
<script src="<%=BASEURL%>/js/effects.js"        type="text/javascript"></script>
<script src="<%=BASEURL%>/js/window.js"         type="text/javascript"></script>
<script src="<%=BASEURL%>/js/window_effects.js" type="text/javascript"></script>

<link href="<%= BASEURL%>/css/estilostsp.css" rel="stylesheet" type="text/css">
<link href="<%=BASEURL%>/css/mac_os_x.css"      rel="stylesheet" type="text/css">
<link href="<%=BASEURL%>/css/default.css"       rel="stylesheet" type="text/css">
<link href="<%=BASEURL%>/css/alert.css" rel="stylesheet" type="text/css"/>
<link href="<%=BASEURL%>/css/alphacube.css" rel="stylesheet" type="text/css"/>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">

<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
        <title>Detalle de Sector</title>
    </head>
    <body>
    <form id="formulario2" action="">
        <%  String id = request.getParameter("id");
            String nombre = request.getParameter("nombre");
            String detalle = request.getParameter("detalle");
            String tipo = request.getParameter("tipo");
            ArrayList arl = (ArrayList) session.getAttribute("subsectores");
            String modificar=request.getParameter("modificar");

        %>
        <script type="text/javascript">
            function soloNumeros(id) {
                var valor = document.getElementById(id).value;
                valor =  valor.replace(/[^0-9^.]+/gi,"");
                document.getElementById(id).value = valor;
            }
        </script>
        <script>
            function sendAction(url,tipo,id,ev){
                sw=false;formx='formulario2';
                try{
                    if(ev.target.type!='radio'){
                        sw=true;
                    }
                }catch(e){
                    if(ev.srcElement.type!='radio'){
                        sw=true;
                    }
                }
                if(tipo=='NUEVO_SUBSECTOR'){
                    p="tipo="+tipo+"&id="+id;
                    new Ajax.Request(
                    url,
                    {   method: 'post',
                        parameters: p,
                        onComplete: openwin
                    });
                }
                else{
                    if(tipo=='AGREGAR_SUBSECTOR'){
                        if($('detalle').value!='' &&  $('nombre').value!=''){
                            //openInfoDialog('<b> Ejecutando<br/>Por favor espere...</b>');
                            p="tipo="+tipo+"&nombre="+$('nombre').value+"&detalle="+$('detalle').value+"&codigo="+$('codigo').value+"&id=<%=id%>"+"&activo="+$('activo').value+"&nombre_alterno="+$('nombre_alterno').value;
                            new Ajax.Request(
                            url,
                            {   method: 'post',
                                parameters: p,
                                onComplete: function(response){
                                    addrow(response,$F('nombre'),$F('detalle'));                                    
                                }
                            });
                        }
                        else
                        {   Dialog.alert("Debe llenar todos los campos ...", {
                                width:250,
                                height:100,
                                windowParameters: {className: "alphacube"}
                            });
                        }
                    }
                    else{
                        if(tipo=='VENTANA_EDITAR_SUBSECTOR'){
                            sw=true;
                            for (i=0;i<$(formx).length && sw;i++){
                                if ($(formx).elements[i].type=='radio' && $(formx).elements[i].checked==true){
                                    sw=false;
                                    id=$(formx).elements[i].id;
                                }
                            }
                            if(id!=''){
                                p="tipo="+tipo+"&idsub="+id+"&id=<%=id%>";
                                new Ajax.Request(
                                url,
                                {   method: 'post',
                                    parameters: p,
                                    onComplete: openwin
                                });
                                closewin();
                            }
                            else{
                                Dialog.alert("Debe escojer al menos un sector ...", {
                                    width:250,
                                    height:100,
                                    windowParameters: {className: "alphacube"}
                                });
                            }
                        }
                        else{
                            if(tipo=='EDITAR_SUBSECTOR'){
                                if($('detalle').value!='' &&  $('nombre').value!=''){
                                    p="tipo="+tipo+"&nombre="+$('nombre').value+"&detalle="+$('detalle').value+"&codigo="+$('codigo').value+"&id="+id+"&activo="+$('activo').value+"&nombre_alterno="+$('nombre_alterno').value;
                                    new Ajax.Request(
                                    url,
                                    {   method: 'post',
                                        parameters: p,
                                        onComplete: complete
                                    });
                                    
                                }
                                else
                                {   Dialog.alert("Debe llenar todos los campos ...", {
                                        width:250,
                                        height:100,
                                        windowParameters: {className: "alphacube"}
                                    });
                                }
                            }
                            
                        }
                    }
                }
            }
            function closewin(){
                $('contenido').innerHTML="";
                $('contenido').style.visibility='hidden';
                win.close();
                win.destroy();
                location.reload();
            }
            function openwin(response){
                $('contenido').innerHTML=response.responseText;
                $('contenido').style.display='block';
                $('contenido').style.visibility='visible';
                win= new Window(
                {   id: "nuevo",
                    title: "Nuevo",
                    width:$('contenido').width,
                    height:$('contenido').heigth,
                    destroyOnClose: true,
                    onClose:closewin,
                    resizable: false
                });
                win.setContent('contenido', true, true);
                win.show(false);
                win.showCenter();
            }
            function openInfoDialog(mensaje) {
                Dialog.info(mensaje, {
                    width:250,
                    height:100,
                    showProgress: true,
                    windowParameters: {className: "alphacube"}
                });
            }

            function color_fila(id,color){
                $(id).style.backgroundColor=color;
            }

            function addrow(response, nombre, detalle){
                try{
                    setTimeout('', 1000, '', '');
                    Dialog.closeInfo();
                    if(response.responseText.indexOf('ERROR')==-1){
                        myTable = $('subsector');
                        if(navigator.appName=='Microsoft Internet Explorer'){
                            oRow = myTable.insertRow();
                        }
                        else{
                            oRow = myTable.insertRow(-1);
                        }
                        oRow.id='fila'+response.responseText;
                        oRow.className='fila';
                        oRow.align='center';
                        oRow.style.cursor='pointer';
                        oRow.onmouseover=function mouseover2(){
                                            color_fila('fila'+response.responseText,'orange');
                                        };
                        oRow.onmouseout=function mouseout2(){
                                            color_fila('fila'+response.responseText,'EAFFEA');
                                        };

                        oCell=oRow.insertCell(0);
                        oCell.innerHTML='<input type="radio" id="'+response.responseText+'" name="subsectores">';
                        oCell=oRow.insertCell(1);
                        oCell.innerHTML=response.responseText;
                        oCell=oRow.insertCell(2);
                        oCell.innerHTML=nombre;
                        oCell=oRow.insertCell(3);
                        oCell.innerHTML=detalle;
                        oCell=oRow.insertCell(4);
                        oCell.innerHTML="ACTIVO";
                        closewin();
                    }
                    else
                    {   Dialog.alert(response.responseText, {
                            width:250,
                            height:100,
                            windowParameters: {className: "alphacube"}
                        });
                    }
                }catch(e){
                    Dialog.closeInfo();
                }
            }
            function complete(response){
                Dialog.alert(response.responseText, {
                    width:250,
                    height:100,
                    windowParameters: {className: "alphacube"}
                });
                closewin();
            }

            function activo() {
               if( document.getElementById("activo").checked){
                   document.getElementById("activo").value="";
               }else{
                   document.getElementById("activo").value="A";
               }
            }           

        </script>
        <div id="capaCentral" style="position:absolute; width:100%; height:100%; z-index:0; left: 0px;">
            <br>
            <br>
            <center>
                <div id="contenido" align="center" style=" width: 80%;height: 300px; visibility: hidden; background-color:#F7F5F4; display: none"></div>
                <table border="2" width="90%">
                    <tr>
                        <td>
                            <table class="tablaInferior" border="0" width="100%">
                                <tr >
                                    <td width="30%" align="left" class="subtitulo1">&nbsp;SubSectores</td><!-- mod -->
                                    <td width="70%" align="left" class="barratitulo" colspan="2">
                                        <img alt="" src="<%= BASEURL%>/images/titulo.gif" width="32" height="20"  align="left">
                                    </td>
                                </tr>
                                <tr class="barratitulo" style="font-size: 11px;font-weight: bold;">
                                    <td  align="left" >&nbsp;SECTOR:&nbsp;&nbsp;<%=id%></td>
                                    <td  align="left" >&nbsp; <%=nombre%></td>
                                    <td  align="left" >&nbsp;<%=detalle%></td>
                                </tr>
                            </table>
                            <div align="center">
                                <table id="subsector" border="1" width="100%"><%
                                    for (int i = 0; i < arl.size(); i++) {
                                        ArrayList arl2 = ((ArrayList) arl.get(i));
                                        %><tr id="fila<%=i%>"<%= (i == 0) ? " class='fila' align='center' style='color: white;background-color:green;'" : "class='fila' align='center' style='cursor: pointer'  onmouseover=\"color_fila('fila" + i + "','orange');\" onmouseout=\"color_fila('fila" + i + "','#EAFFEA');\""%>>
                                        <%for (int j = -1; j < arl2.size(); j++) {
                                            if (j == -1 && i != 0) {
                                                %><td align="center"><input type="radio" id="<%=(String) arl2.get(0)%>" name="subsectores"></td><%
                                            }
                                            else {
                                                if (j != -1) {
                                                    %><td>&nbsp;<%=(String) arl2.get(j)%></td><%
                                                }
                                                else {%>
                                                    <td></td>
                                                <%}
                                            }
                                        }
                                        %></tr><%
                                    }%>
                                </table>
                            </div>
                        </td>
                    </tr>
                </table>
                <br>
                <br>
                <%if(modificar != null && modificar.equals("SI")){%>
                <img alt="" src = "<%=BASEURL%>/images/botones/nuevo.gif" style = "cursor:pointer" name = "imgaceptar" id="imgaceptar"  onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" onclick='sendAction("<%=CONTROLLER%>?estado=Sector&accion=Subsector","NUEVO_SUBSECTOR","<%=id%>",event);'>
                <img alt="" src = "<%=BASEURL%>/images/botones/modificar.gif" style = "cursor:pointer" name = "imgaceptar" id="imgaceptar"  onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" onclick='sendAction("<%=CONTROLLER%>?estado=Sector&accion=Subsector","VENTANA_EDITAR_SUBSECTOR","",event);'>
                <%}%>
                <img alt="" src = "<%=BASEURL%>/images/botones/restablecer.gif" style = "cursor:pointer" name = "imgsalir" onClick = "location.reload()" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);">
                <!-- mod -->
                <img alt="" src="<%=BASEURL%>/images/botones/salir.gif" style="cursor:pointer" name="imgsalir" id="imgsalir" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" onclick='parent.closewin();'>
            </center>
        </div>
        </form>
    </body>
</html>
