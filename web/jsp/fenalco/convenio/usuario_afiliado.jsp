<%-- 
    Document   : usuario_afiliado
    Created on : 4/09/2010, 09:45:11 AM
    Author     : rhonalf
--%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@page session="true"%>
<%@page import="java.util.*"%>
<%@page import="com.tsp.operation.model.*"%>
<%@page import="com.tsp.operation.model.beans.*"%>
<%@page import="com.tsp.operation.model.services.*"%>
<%@page import="com.tsp.util.*"%>
<%@include file="/WEB-INF/InitModel.jsp"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
   "http://www.w3.org/TR/html4/loose.dtd">
<!-- This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>. -->
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
        <title>Usuario - Asignacion de afiliado</title>
       <script src="<%=BASEURL%>/js/prototype.js"      type="text/javascript"></script>
<script src="<%=BASEURL%>/js/boton.js"          type="text/javascript"></script>
<script src="<%=BASEURL%>/js/effects.js"        type="text/javascript"></script>
<script src="<%=BASEURL%>/js/window.js"         type="text/javascript"></script>
<script src="<%=BASEURL%>/js/window_effects.js" type="text/javascript"></script>

<link href="<%= BASEURL%>/css/estilostsp.css" rel="stylesheet" type="text/css">
<link href="<%=BASEURL%>/css/mac_os_x.css"      rel="stylesheet" type="text/css">
<link href="<%=BASEURL%>/css/default.css"       rel="stylesheet" type="text/css">
<link href="<%=BASEURL%>/css/alert.css" rel="stylesheet" type="text/css"/>
<link href="<%=BASEURL%>/css/alphacube.css" rel="stylesheet" type="text/css"/>
        <%
		String agregar = request.getParameter("agregar")!=null? request.getParameter("agregar"):"N";
	%>
        <script type="text/javascript">
            var win;
            var fila;
            var division;
            var divload;
            function openWin(id_div,idopener,titulo){
                fila = idopener;
                division = id_div;
                win= new Window(
                {   id: "diving",
                    title: '<b>.: '+titulo+' :.</b>',
                    width:$(id_div).width,
                    height:$(id_div).heigth,
                    showEffectOptions: {duration:1},
                    hideEffectOptions: {duration:1},
                    destroyOnClose: true,
                    onClose:closewin,
                    resizable: false
                                            });
                win.setContent(id_div, true, true);
                win.show(true);
                //win.setLocation(7, 7);
                win.showCenter();
                //$("spangen").innerHTML = "";
                //$('contenido').innerHTML=response.responseText;
                $(id_div).style.display='block';
                $(id_div).style.visibility='visible';
                //$('contenido').style.overlay="auto";
            }

            function closewin(){
                $(division).style.display='none';
                $(division).style.visibility='hidden';
                //$(divid).innerHTML="";
            }

            function cerrarDiv(){
                win.close();
            }

            function loading(){
                var interno = '<div width="100%" class="fila">Procesando <img alt="cargando" src="<%= BASEURL%>/images/cargando.gif" name="imgload"></div>';
                $(divload).innerHTML = interno;
            }

            function llenarDiv(response){
                $(divload).innerHTML = response.responseText;
            }

            function asignarValor(idopener,valor){
                $(idopener).value = valor;
            }

            function buscarAfils(){
                var indi = $('filtro').selectedIndex;
                var filtro = $('filtro').options[indi].value;
                var cadena = $('cadena').value;
                var url = "<%= CONTROLLER%>?estado=Gestion&accion=Convenios";
                var p =  'tipo=buscarafil&filtro='+filtro+'&cadena='+cadena+'';
                divload = "divres";
                new Ajax.Request(
                    url,
                    {
                        method: 'post',
                        parameters: p,
                        onLoading: loading,
                        onComplete: llenarDiv
                    }
                );
            }

            function asignarValor2(val1,val2){
                $('afiliado').value = val1;
                $('nombreafil').value = val2;
                if($('codigousuario').value!=""){
                    buscarConvSect();
                }
            }

            var numfilac = 1;
            function verConvs(numfila){
                numfilac = numfila;
                var filtro = document.getElementById("afiliado").value;
                if(filtro==""){
                    alert('Debe escoger un afiliado');
                }
                else{
                    var url = "<%= CONTROLLER%>?estado=Gestion&accion=Convenios";
                    var p =  'tipo=busconv&nitprov='+filtro;
                    divload = 'contenido';
                    new Ajax.Request(
                        url,
                        {
                            method: 'post',
                            parameters: p,
                            onLoading: loading,
                            onComplete: abrirWin
                        }
                    );
                }
            }

            function abrirWin(response){
                win= new Window(
                {   id: "diving2",
                    title: "<b>.: Convenios, sectores, subsectores :.</b>",
                    width:$('contenido').width,
                    height:$('contenido').heigth,
                    showEffectOptions: {duration:1},
                    hideEffectOptions: {duration:1},
                    destroyOnClose: true,
                    onClose:closewin,
                    resizable: false
                });
                division = 'contenido';
                win.setContent('contenido', true, true);
                win.show(true);
                //win.setLocation(7, 7);
                win.showCenter();
                //$("spangen").innerHTML = "";
                $('contenido').innerHTML= '<div style="height: 200px;">'
                                      + response.responseText + '</div>'
                                      + '<br><div align="center"><img alt="" src="<%=BASEURL%>/images/botones/salir.gif" '
                                      +'style = "cursor:pointer" name = "imgsaliry" onclick = "cerrarDiv();" '
                                      +'onMouseOver="botonOver(this);" onMouseOut="botonOut(this);"></div>';
                                  $('contenido').style.display='block';
                                    $('contenido').style.visibility='visible';
                //$('contenido').style.overlay="auto";
            }

            function asignarValor3(id1,id2,id3,id4){
                $('id_provc'+numfilac).value=id1;
                $('conv'+numfilac).value=id2;
                $('sect'+numfilac).value=id3;
                $('subsect'+numfilac).value=id4;
            }

            function buscarUsu(){
                var codigo = $('codigousuario').value;
                var url = "<%= CONTROLLER%>?estado=Gestion&accion=Convenios";
                var p =  'tipo=buscarusu&usuariocod='+codigo;
                new Ajax.Request(
                    url,
                    {
                        method: 'post',
                        parameters: p,
                        //onLoading: loading,
                        onComplete: asignarDatos
                    }
                );
            }

            function asignarDatos(response){
                var array = response.responseText.split(";_;");
                $('nitusuario').value = array[0];
                $('nomusuario').value = array[1];
                if($('afiliado').value!=""){
                    buscarConvSect();
                }
            }

            var filaconds = 1;
            function agregarFila(tablaId){
                var myTable = $(tablaId);
                var oRow;
                if(navigator.appName=='Microsoft Internet Explorer'){
                    oRow = myTable.insertRow();
                }else{
                    oRow = myTable.insertRow(-1);
                }
                oRow.className='fila';
                filaconds += 1;
                var oCell=oRow.insertCell(0);
                oCell.innerHTML = '<input type="hidden" id="id_provc'+filaconds+'" name="id_provc'+filaconds+'" value="">'+
                                        '<img alt="+" src="<%=BASEURL%>/images/botones/iconos/mas.gif" width="15" height="15" style="cursor: pointer" onclick="agregarFila(\'tablaconvs1\')">';
                oCell=oRow.insertCell(1);
                oCell.innerHTML = '<input type="text" name="conv'+filaconds+'" id="conv'+filaconds+'" value="" readonly="readonly">'+
                                        '<img alt="" src="<%=BASEURL%>/images/botones/iconos/lupa.gif" width="15" height="15" style="cursor: pointer" onclick="verConvs('+filaconds+');">';
                oCell=oRow.insertCell(2);
                oCell.innerHTML = '<input type="text" name="sect'+filaconds+'" id="sect'+filaconds+'" value="" readonly="readonly">';
                oCell=oRow.insertCell(3);
                oCell.innerHTML = '<input type="text" name="subsect'+filaconds+'" id="subsect'+filaconds+'" value="" readonly="readonly">';
                oCell=oRow.insertCell(4);
                oCell.innerHTML = '<input type="checkbox" name="activo'+filaconds+'" id="activo'+filaconds+'" value="S" checked="checked">';
            }

            function enviar(){
                var codusus = $('codigousuario').value;
                var nitf = $('afiliado').value;
                var idpc1 = $('id_provc1').value;
                if(codusus==""){
                    alert('Debe colocar un usuario');
                }
                else{
                    if(nitf==""){
                        alert('Debe escoger un afiliado');
                    }
                    else{
                        if(idpc1==""){
                            alert('Debe asignar un convenio/sector/subsector');
                        }
                        else{
                            //alert(codusus+'_'+nitf+'_'+idpc1);
                            var action = $('formulario').action + "&tipo=insertrel&filastabla="+($('tablaconvs1').rows.length - 1);
                            $('formulario').action = action;
                            $('formulario').submit();
                        }
                    }
                }
            }

            function buscarConvSect(){
                var codigo = $('codigousuario').value;
                var nitprov = $('afiliado').value;
                var url = "<%= CONTROLLER%>?estado=Gestion&accion=Convenios";
                var p =  'tipo=datosasign&codusuario='+codigo+'&nitprov='+nitprov+'&agregar=<%= agregar%>';
                divload = 'tablaconvs';
                new Ajax.Request(
                    url,
                    {
                        method: 'post',
                        parameters: p,
                        //onLoading: loading,
                        onComplete: llenarDiv
                    }
                );
            }
        </script>
	
    </head>
    <body>
        <div id="capaSuperior" style="position:absolute; width:100%; height:100px; z-index:0; left: 0px; top: 0px;">
            <jsp:include page="/toptsp.jsp?encabezado=Usuario - Asignacion de afiliado"/>
        </div>
        <div id="contenido" style="visibility: hidden; display: none; width: 600px; height: 300px; background-color: #EEEEEE;">
            
        </div>
        <div id="afiliados" style="visibility: hidden; display: none; width: 600px; height: 300px; background-color: #EEEEEE;">
            <table style="border-collapse: collapse; width: 100%;">
                <tr class="fila">
                    <td>Filtro</td>
                    <td>
                        <select name="filtro" id="filtro">
                            <option value="payment_name" selected>Nombre</option>
                            <option value="nit">Nit</option>
                        </select>
                    </td>
                    <td>Dato</td>
                    <td>
                        <input type="text" name="cadena" id="cadena" value="">
                        <img alt="" src="<%=BASEURL%>/images/botones/iconos/lupa.gif" width="15" height="15" style="cursor: pointer" onclick="buscarAfils();">
                    </td>
                </tr>
            </table>
            <br>
            <div id="divres" style="width: 100%; height: 200px; overflow: auto;"></div>
            <br>
            <div align="center">
                <img alt="" src="<%=BASEURL%>/images/botones/salir.gif" style = "cursor:pointer" name = "imgsalirx" onclick = "cerrarDiv();" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);">
            </div>
        </div>
        <div id="capaCentral" style="position:absolute; width:100%; height:85%; z-index:0; left: 0px; top: 100px; overflow:auto;">
            <form action="<%=CONTROLLER%>?estado=Gestion&accion=Convenios" id="formulario" name="formulario" method="post">
                <div align="center">
                    <table border="2" width="700px">
                        <tr>
                            <td>
                                <table border="0" width="100%">
                                    <tr class="subtitulo1">
                                        <td colspan="4">Asignacion</td>
                                    </tr>
                                    <tr class="fila">
                                        <td>Usuario</td>
                                        <td colspan="3">
                                            <input type="text" name="codigousuario" id="codigousuario" value="" size="15">
                                            <img alt="" src="<%=BASEURL%>/images/botones/iconos/lupa.gif" width="15" height="15" style="cursor: pointer" onclick="buscarUsu();">
                                        </td>
                                    </tr>
                                    <tr class="fila">
                                        <td>Nit usuario</td>
                                        <td>
                                            <input type="text" name="nitusuario" id="nitusuario" value="" size="15" readonly="readonly">
                                        </td>
                                        <td>Nombre usuario</td>
                                        <td>
                                            <input type="text" name="nomusuario" id="nomusuario" value="" size="40" readonly="readonly">
                                        </td>
                                    </tr>
                                    <tr class="fila">
                                        <td>Nit afiliado</td>
                                        <td>
                                            <input type="text" name="afiliado" id="afiliado" value="" size="15" readonly="readonly">
                                            <img alt="" src="<%=BASEURL%>/images/botones/iconos/lupa.gif" width="15" height="15" style="cursor: pointer" onclick="openWin('afiliados','conv1','Afiliado');">
                                        </td>
                                        <td>Nombre afiliado</td>
                                        <td>
                                            <input type="text" name="nombreafil" id="nombreafil" value="" size="40" readonly="readonly">
                                        </td>
                                    </tr>
                                </table>
                                <div style="width: 100%; height: 100px; overflow: auto;" id="tablaconvs">
                                    <table border="0" width="100%" id="tablaconvs1">
                                        <tr class="subtitulo1">
                                            <td></td>
                                            <td>Convenio</td>
                                            <td>Sector</td>
                                            <td>Subsector</td>
                                            <td>Activo</td><!-- wtf? -->
                                        </tr>
                                        <tr class="fila">
                                            <td>
                                                <input type="hidden" id="id_provc1" name="id_provc1" value="">
                                                 <%
                                    	if(agregar.equals("S")){
                                     %>
                                                <img alt="+" src="<%=BASEURL%>/images/botones/iconos/mas.gif" width="15" height="15" style="cursor: pointer" onclick="agregarFila('tablaconvs1')">
                                            <%}%>
                                            </td>
                                            <td>
                                                <input type="text" name="conv1" id="conv1" value="" readonly="readonly">
                                              <%  if(agregar.equals("S")){ %>                                    
                                                <img alt="" src="<%=BASEURL%>/images/botones/iconos/lupa.gif" width="15" height="15" style="cursor: pointer" onclick="verConvs(1);">
                                                <%}%>
                                            </td>
                                            <td>
                                                <input type="text" name="sect1" id="sect1" value="" readonly="readonly">
                                            </td>
                                            <td>
                                                <input type="text" name="subsect1" id="subsect1" value="" readonly="readonly">
                                            </td>
                                            <td>
                                                <input type="checkbox" name="activo" id="activo" value="S" checked="checked" <%=  (agregar.equals("S"))?"":"disabled" %> >
                                            </td>
                                        </tr>
                                    </table>
                                </div>
                                <br>
                                <div align="center">
                                    <%
                                    	if(agregar.equals("S")){
                                    %>
                                    <img alt="" src = "<%=BASEURL%>/images/botones/aceptar.gif" style="cursor:pointer" onclick="enviar();" name= "imgaceptar" id="imgaceptar"  onMouseOver="botonOver(this);" onMouseOut="botonOut(this);">
                                    <%
                                    	}
                                    %>
                                    <img alt="" src = "<%=BASEURL%>/images/botones/cancelar.gif" style="cursor:pointer" onclick="location.href='<%=BASEURL%>/jsp/fenalco/convenio/usuario_afiliado.jsp';" name= "imgcancel" id="imgcancel"  onMouseOver="botonOver(this);" onMouseOut="botonOut(this);">
                                    <img alt="" src = "<%=BASEURL%>/images/botones/salir.gif" style = "cursor:pointer" name = "imgsalir" onClick = "window.close();" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);">
                                </div>
                            </td>
                        </tr>
                    </table>
                </div>
            </form>
        </div>
    </body>
</html>
