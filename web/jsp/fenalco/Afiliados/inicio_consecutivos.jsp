<!--
- Autor : Ing. Roberto Rocha P
- Date  : 14 de Mayo de 2007
- Copyrigth Notice : Fintravalores S.A. S.A
-->
<%--
-@(#)
--Descripcion : Pagina JSP, que maneja el ingreso de identidades

--%>

<%-- Declaracion de librerias--%>
<%@page contentType="text/html;"%>
<%@page session="true"%>
<%@page import="java.util.*"%>
<%@page import="com.tsp.operation.model.*"%> 
<%@page import="com.tsp.operation.model.beans.*"%>
<%@page import="com.tsp.util.*"%>
<%@page import="com.tsp.util.Util.*"%>
<%@include file="/WEB-INF/InitModel.jsp"%>
<%@page import="com.tsp.operation.model.*"%>
<%@ taglib uri="http://jakarta.apache.org/taglibs/input-0.90" prefix="input" %>

<html>
<head>
<title>Liquidador</title>

<link href="../../../css/estilostsp.css" rel="stylesheet" type="text/css"> 
<link href="<%=BASEURL%>/css/estilostsp.css" rel="stylesheet" type="text/css"> 

<style type="text/css">
<!--
.style1 {font-size: 9px}
.style2 {
	font-size: 10px;
	color: #666666;
}
.style3 {color: #666666}
-->
</style>
</head>

<script type='text/javascript' src="<%=BASEURL%>/js/Validacion.js"></script>
<script type='text/javascript' src="<%= BASEURL %>/js/boton.js"></script>
<script type='text/javascript' src="<%= BASEURL %>/js/general.js"></script>
<script src='<%=BASEURL%>/js/tools.js' language='javascript'></script>
<script src='<%=BASEURL%>/js/date-picker.js'></script>
<script src="<%= BASEURL %>/js/transferencias.js"></script>

<%  Usuario usuario            = (Usuario) session.getAttribute("Usuario");
	Propietario  Propietario  = new Propietario();
	TreeMap pro= model.Negociossvc.getProv();
	pro.put(" Seleccione","...");
%>

<script>
function abrirVentanaBusq( an, al, url, pag ) {
			
        		parent.open( url, 'Conductor', 'width=' + an + ', height=' + al + ',scrollbars=no,resizable=no,top=10,left=65,status=yes' );
				
    		}
			
	   function enviarFormulario(CONTROLLER,frm){
				
				if ( formulario.proveedor.value == '...' ){
					alert( 'Debe escojer el afiliado para continuar...' );
					return false;
				}
				document.imgaceptar.src='<%=BASEURL%>/images/botones/aceptarDisable.gif';
				document.imgaceptar.onmouseover = new Function('');
				document.imgaceptar.onmouseout  = new Function('');
				document.imgaceptar.onclick     = new Function('');
				frm.submit();
	   }

	</script>




<body onLoad="javascript:formulario.text.focus">
<div id="capaSuperior" style="position:absolute; width:100%; height:100px; z-index:0; left: 0px; top: 0px;">
	<jsp:include page="/toptsp.jsp?encabezado=CONSECUTIVOS"/>
</div>
<div id="capaCentral" style="position:absolute; width:100%; height:87%; z-index:0; left: 0px; top: 100px; overflow: scroll;">

<FORM action='<%=CONTROLLER%>?estado=Negocios&accion=Update&op=8' name='formulario' method="post" >
  
	<table width="512" height="177" border="2" align="center">
		<tr>
		  <td width="420" height="100">
		    <table width="100%" height="98%" class="tablaInferior" >
			  <tr class="fila">
			    <td align="left" nowrap class="subtitulo">&nbsp;Consecutivos </td>
				  <td colspan="2" align="left" nowrap class="bordereporte"><img src="<%=BASEURL%>/images/fintra.gif" width="166" height="28" class="bordereporte"> </td>
			  </tr>
			  
			  <tr class="fila">
			    <td >Escoja el Afiliado </td>
				  <td width="49%" class="fila">
				  <input name="text" type="text" class="textbox" id="campo" style="width:200;" onKeyUp="buscar(document.formulario.proveedor,this)" size="15" >
					<input:select name="proveedor" attributesText="class=textbox" options="<%=pro %>"/>	      
			    </td>
			  </tr>
		    </table>
		  	    
		  </td>
		</tr>
		<tr>
			<td align="center">
			<img src="<%=BASEURL%>/images/botones/aceptar.gif"  name="imgaceptar" onClick="javascript:enviarFormulario('<%=CONTROLLER%>',formulario);" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" style="cursor:hand">&nbsp;       &nbsp;
    		<img src="<%=BASEURL%>/images/botones/salir.gif"  name="imgsalir"  onMouseOver="botonOver(this);" onClick="parent.close();" onMouseOut="botonOut(this);" style="cursor:hand">
  
			</td>
		</tr>
	</table>

<%
String Retorno=(String)request.getParameter("msg");
if( Retorno != null ){%>  
  <table border="2" align="center">
  <tr>
    <td><table width="100%"  border="1" align="center"  bordercolor="#F7F5F4" bgcolor="#FFFFFF">
      <tr>
        <td width="229" align="center" class="mensajes"><%=Retorno%></td>
        <td width="29" background="<%=BASEURL%>/images/cuadronaranja.JPG">&nbsp;</td>
        <td width="58">&nbsp;</td>
      </tr>
    </table></td>
  </tr>
</table>
<%}%>

</p>
 <br>
</form>
</div>

<iframe width="188" height="166" name="gToday:normal:agenda.js:gfPop:plugins.js" id="gToday:normal:agenda.js:gfPop:plugins_24.js" src="<%=BASEURL%>/js/Calendario/ipopeng.htm" scrolling="yes" frameborder="0" style="visibility:visible; z-index:999; position:absolute; left:-500px; top:0px;">
</iframe>
</body>
</html>


