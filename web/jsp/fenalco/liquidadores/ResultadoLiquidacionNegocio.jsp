<%--
    Document   : LiquidacionMicrocredito.jsp
    Created on : 11/07/2012, 04:37:55 PM
    Author     : JESUS PINEDO
--%>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<%@page import="com.tsp.opav.model.services.ClientesVerService"%>
<%@page contentType="text/html"%>
<%@page session="true"%>
<%@page errorPage="/error/ErrorPage.jsp"%>
<%@ page import="java.sql.*" %>
<%@ page import="java.util.*" %>
<%@ page import="java.io.*" %>
<%@ page import="java.lang.*" %>
<%@page import="java.util.*"%>
<%@page import="com.tsp.operation.model.*"%>
<%@page import="com.tsp.operation.model.beans.*"%>
<%@page import="com.tsp.util.*"%>
<%@page import="com.tsp.util.Util.*"%>
<%@page import="com.tsp.operation.model.services.GestionCondicionesService"%>
<%@include file="/WEB-INF/InitModel.jsp"%>
<%@ taglib uri="http://jakarta.apache.org/taglibs/input-0.90" prefix="input" %>

<%

  Usuario usuario = (Usuario) session.getAttribute("Usuario");
      ArrayList<DocumentosNegAceptado> liquidacion= model.Negociossvc.getLiquidacion();
      Negocios neg =model.Negociossvc.getNegocio();
      Convenio convenio = model.gestionConveniosSvc.buscar_convenio(usuario.getBd(), neg.getId_convenio() + "");

    String numero_form= (String)request.getAttribute("numsolc");
    String tipo_liq = (String)request.getAttribute("tipo_liq");
    String custodia = (String)request.getAttribute("custodia");
    String tipo_negocio = (String) request.getAttribute("tipo_negocio");
    String fechai = neg.getFecha_neg();
    String banco=(String) request.getAttribute("banco");
    String ciudadCh=(String) request.getAttribute("ciudadCh");
    String act_liq=(String)(request.getAttribute("act_liq")!=null?request.getAttribute("act_liq"):"");
    double Desembolso = 100000;
    double Valor_Doc = 1000;
    double Custodia_Cheque = 0;
    double tcuota=0;
    double tcapital=0;
    double tinteres=0;
    String negocio="";



	 for(int i=0;i<liquidacion.size();i++){
                                        negocio=liquidacion.get(i).getCod_neg();
                                        tcuota+=liquidacion.get(i).getValor();
                                        tcapital += liquidacion.get(i).getCapital();
                                        tinteres += liquidacion.get(i).getInteres();
                                        }

%>

<html>

    <head>
  <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1"/>
        <link href="<%=BASEURL%>/css/estilostsp.css" rel="stylesheet" type="text/css">
           <script type='text/javascript' src="<%=BASEURL%>/js/validar.js"></script>
            <script type='text/javascript' src="<%= BASEURL%>/js/boton.js"></script>
            <script type='text/javascript' src="<%= BASEURL%>/js/general.js"></script>
          <script type='text/javascript' src="<%= BASEURL%>/js/jquery/jquery-ui/jquery.min.js"></script>
          <script type='text/javascript' src="<%= BASEURL%>/js/jquery/jquery-ui/ui.dialogr.js"></script>
        <script type='text/javascript' src="<%= BASEURL%>/js/jquery/jquery-ui/jquery.ui.min.js"></script>

         <link href="<%= BASEURL%>/css/estilostsp.css" rel="stylesheet" type="text/css">
        <link href="<%= BASEURL%>/css/jquery/jquery-ui/jquery-ui.css" rel="stylesheet" type="text/css"/>
        <link href="<%= BASEURL%>/css/jquery/jquery-ui/jquery.ui.selectmenu.css" rel="stylesheet" type="text/css"/>
                <link href="<%= BASEURL%>/css/jquery/jquery.jqGrid/ui.jqgrid.css" rel="stylesheet" type="text/css"/>

        <script type='text/javascript' src="<%= BASEURL%>/js/boton.js"></script>
        <script type='text/javascript' src="<%= BASEURL%>/js/jquery/jquery-ui/jquery.min.js"></script>
        <script type='text/javascript' src="<%= BASEURL%>/js/jquery/jquery-ui/jquery.ui.min.js"></script>
        <script type='text/javascript' src="<%= BASEURL%>/js/jquery/jquery.jqGrid/js/grid.locale-es.js"></script>
        <script type='text/javascript' src="<%= BASEURL%>/js/jquery/jquery.jqGrid/js/jquery.jqGrid.min.js"></script>
        <script type='text/javascript' src="<%= BASEURL%>/js/jquery/jquery-ui/jquery.ui.selectmenu.js"></script>
                      <link href="<%= BASEURL%>/css/estilosFintra.css" rel='stylesheet'/>
        <script language="Javascript">
            function imprSelec(nombre)
            {
                var ficha = document.getElementById(nombre);
                var ventimp = window.open(' ', 'popimpr');
                ventimp.document.write( ficha.innerHTML);
                ventimp.document.close();
                ventimp.print( );
                ventimp.close();
            }

function cerrar()
{
    $("#divSalida").dialog('close')
}


function financiarAval(sw)
{
    var url ="<%=CONTROLLER%>?estado=Liquidador&accion=Negocios";
    $("#divLiquidacionAval").html("<div id='resp'></div>");
	$("#divLiquidacionAval").dialog("option", "position", "center");
     cargando("divLiquidacionAval","Cargando");

	//descontar aval de total a pagar

	var aval =parseFloat($("#valor_aval").val());
	var total_pagar =parseFloat($("#tpag").val());
	if(sw)
	{
     $("#divLiquidacionAval").dialog("open");
    $("#tpag").val(total_pagar-aval);
	$("#valor_aval").val(0);
	}
	else
	{
		var aval_tem =parseFloat($("#valor_aval_tem").val());
		$("#tpag").val(total_pagar+aval_tem);
		$("#valor_aval").val(aval_tem);
		 $("#divLiquidacionAval").dialog("close");

	}




   $.ajax({
        dataType: 'html',
        url:url,
        data: {
            opcion : 'financiarAval'
        },
        method: 'post',
        success:  function (resp){
            
				if(sw)
	{
		       var tipo_liq= $("#tipo_liq").val();
			   var url = $("#BASEURL").val()+"/jsp/fenalco/liquidadores/LiquidacionAval.jsp?tipo_liq="+tipo_liq;
			  $("#divLiquidacionAval").load(url);
	}
        },
        error: function(resp){
            alert("Ocurrio un error enviando los datos al servidor");
        }

    });
}






                $(document).ready(
                function() {
		$("#tabla_datos").styleTable();
		$("#datos_totales").styleTable();





                    jQuery("#liquidacion").jqGrid({

                        caption: "Liquidacion" ,
                        url: "<%=CONTROLLER%>?estado=Liquidador&accion=Negocios&opcion=getLiquidacion&tipo=1",
                        datatype: "json",
                        height: 250,
                        width: 1050,
                     colNames:['Cuota','Fecha','Saldo Inicial','Capital','Interes','Custodia','Seguro','Remesa', 'Cuota Admin', 'Valor Cuota','Saldo Final'],
                       colModel:[
						    {name:'item',index:'item', sortable:false,align:'center',width: 50 },
                            {name:'fecha',index:'fecha', sortable:false,align:'center',width: 100 },
                            {name:'saldo_inicial',index:'saldo_inicial', sortable:false, align:'right',width: 100,
		            formatter:'currency', formatoptions:{decimalSeparator:",", thousandsSeparator: ".", decimalPlaces: 0, prefix: "$ "}} ,
					      {name:'capital',index:'capital', sortable:false, align:'right',width: 100,
		            formatter:'currency', formatoptions:{decimalSeparator:",", thousandsSeparator: ".", decimalPlaces: 0, prefix: "$ "}} ,

					      {name:'interes',index:'interes', sortable:false, align:'right',width: 100,
		            formatter:'currency', formatoptions:{decimalSeparator:",", thousandsSeparator: ".", decimalPlaces: 0, prefix: "$ "}} ,
					      {name:'custodia',index:'custodia', sortable:false, align:'right',width: 100,
		            formatter:'currency', formatoptions:{decimalSeparator:",", thousandsSeparator: ".", decimalPlaces: 0, prefix: "$ "}} ,
					      {name:'seguro',index:'seguro', sortable:false, align:'right',width: 100,
		            formatter:'currency', formatoptions:{decimalSeparator:",", thousandsSeparator: ".", decimalPlaces: 0, prefix: "$ "}} ,
					      {name:'remesa',index:'remesa', sortable:false, align:'right',width: 100,
		            formatter:'currency', formatoptions:{decimalSeparator:",", thousandsSeparator: ".", decimalPlaces: 0, prefix: "$ "}} ,
                                              {name:'cuota_manejo',index:'cuota_manejo', sortable:false, align:'right',width: 100,
		            formatter:'currency', formatoptions:{decimalSeparator:",", thousandsSeparator: ".", decimalPlaces: 0, prefix: "$ "}} ,
					      {name:'valor',index:'valor', sortable:false, align:'right',width: 100,
		            formatter:'currency', formatoptions:{decimalSeparator:",", thousandsSeparator: ".", decimalPlaces: 0, prefix: "$ "}} ,
					      {name:'saldo_final',index:'saldo_final', sortable:false, align:'right',width: 100,
		            formatter:'currency', formatoptions:{decimalSeparator:",", thousandsSeparator: ".", decimalPlaces: 0, prefix: "$ "}}
                    ],
                        rowNum:100000,
                        rowTotal: 500000,
                        loadonce:true,
                        rownumWidth: 40,
                        gridview: true,
                        viewrecords: true,
                        hidegrid: false,
						footerrow : true, userDataOnFooter : true, altRows : true ,
                        jsonReader : {
                           root:"rows",
                            repeatitems: false,
                            id: "0",
                            total: "name"

                        },
                        loadError: function(xhr, status, error){
                            alert(error);
                        }
                    });






     });


	 $(document).ready(function(){
                $("#divLiquidacionAval").dialog({
                    autoOpen: false,
                    height: 600,
                    width: 1000,
                    modal: true,
                    autoResize: false,
                    resizable: false,
	            closeOnEscape: false,
		    draggable:true,
                    position: "center"
                }).parent('.ui-dialog').find('.ui-dialog-titlebar-close').hide();
            });

(function ($) {
    $.fn.styleTable = function (options) {
        var defaults = {
            css: 'styleTable'
        };
        options = $.extend(defaults, options);

        return this.each(function () {
            input = $(this);
            input.addClass(options.css);
            input.find("th").addClass("ui-state-default");
            input.find("td").addClass("ui-widget-content");

            input.find("tr").each(function () {
                $(this).children("td:not(:first)").addClass("first");
                $(this).children("th:not(:first)").addClass("first");
            });
        });
    };
})(jQuery);


 $(document).ready(function(){
                $("#divSalida").dialog({
                    autoOpen: false,
                    height: "auto",
                    width: "35%",
                    modal: true,
                    autoResize: true,
                    resizable: false,
                    position: "center",
                    closeOnEscape: false


                });
              
            });

 function Exportar()
{
    var controller =$("#CONTROLLER").val();
    var BASEURL =$("#BASEURL").val();
    $("#divSalida").dialog("open");
    $("#divSalida").css('height', 'auto');
    $("#divSalida").dialog("option", "position", "center");
   var url ="<%=CONTROLLER%>?estado=Liquidador&accion=Negocios";
    $.ajax({
        dataType: 'html',
        url:url,
        data: {
            opcion : 'exportarPlanPagos',
			numsolc:$("#numsolc").val()
        },
        method: 'post',
        success:  function (resp){
            var boton = "<div style='text-align:center'><img src='" + BASEURL +"/images/botones/salir.gif'  onMouseOver='botonOver(this);' onMouseOut='botonOut(this);'  onclick='cerrar()' /></div>"
            document.getElementById('divSalida').innerHTML = resp+ "<br/><br/><br/><br/>"+boton
        },
        error: function(){
            alert("Ocurrio un error enviando los datos al servidor");
        }

    });

}

        </script>

                <style type="text/css">
           div.ui-dialog{
               font-size:11px;

            }

        </style>


    </head>


<body >

        <div id="capaSuperior" style="position:absolute; width:100%; height:100px; z-index:0; left: 0px; top: 0px;">

            <jsp:include page="/toptsp.jsp?encabezado=LIQUIDADOR DE NEGOCIOS"/>


        </div>
        <div id="capaCentral" style="position:absolute; width:100%; height:85%; z-index:0; left: -2px; top: 102px; overflow: scroll;">
 <form id="form" name="form" method="post" action="<%=CONTROLLER%>?estado=Negocios&accion=Insert">
<input name="BASEURL" type="hidden" id="BASEURL" value="<%=BASEURL%>">
            <input name="CONTROLLER" type="hidden" id="CONTROLLER" value="<%=CONTROLLER%>">

<table  width="40%" align="center" id="tabla_datos" class="tableTextUI" >
                              <tr>
                                <th align="left" colspan="2" ><strong> Datos Liquidacion</strong></th>
                                <th align="left" colspan="2" > <img alt="" src="<%= BASEURL%>/images/titulo.gif" width="32" height="20" align="left"/> </th>
                              </tr>
                              <tr >
                                <th width="150">Tasa Interes M.V.</th>
                                <td width="111" align="center"><%=neg.getTasa()%>%</td>
                                <th width="163">Fecha negocio</th>
                                <td width="120" align="center"><%=neg.getFecha_neg() %></td>
                              </tr>
                              <tr >
                                <th>No.Cuotas</th>
                                <td align="center"><%=neg.getNodocs() %></td>
                                <th>Fecha liquidaci&oacute;n</th>
                                <td align="center"><%=neg.getFecha_liquidacion()%></td>
                              </tr>
                              <tr >
                                <th>Custodia</th>
                                <td align="center"><%=neg.getVr_cust()%></td>
                                <th>Valor del negocio</th>
                                <td align="center">$ <%=Util.customFormat(neg.getVr_negocio())%></td>
                              </tr>
                              <tr >
                                <th>Remesa</th>
                                <td align="center"><%=Util.customFormat(neg.getPor_rem())%>%</td>
                                <th>Valor Aval</th>
                                <td align="center">$ <%=Util.customFormat(neg.getVr_aval() )%></td>
                              </tr>

                              <tr >
                                <th>Seguro</th>
                                <td align="center"><%=Util.customFormat(neg.getValor_seguro())%></td>
                                <th>Total a Pagar</th>
                                <td align="center">$ <%=Util.customFormat(neg.getVr_aval() + tcuota )%></td>
                              </tr>
                            </table>
<br/>

                <table border="0"  align="center">
                  <tr>
                    <td>
                     <table id="liquidacion" ></table>
                    </td>
                    </tr>
                    <tr>
                        <td>
                            <% TablaGen tOpcion=null;
                       model.tablaGenService.buscarDatos("VAL_OPCION", "LIQ_NEGOCIO");
                    tOpcion = model.tablaGenService.getTblgen();
                   if(  (act_liq.equals("")) && (model.usuarioService.ValidarOpcion(usuario.getLogin(), tOpcion.getReferencia()) ))
                       {
                            %>
                            &iquest;Desea adjuntar documentos? 
                            <label>
                                <select name="radicacion" id="radicacion">
                                    <option value="S" selected>SI</option>
                                    <option value="N">NO</option>
                                </select>
                            </label>
                            <%}%>
                        </td>
                    </tr>
                </table>
                <br/>
                <br/>

    <div id="divLiquidacionAval" title="Financiacion De Aval" style=" display: none"  class="ui-dialog"></div>
     <div id="divSalida" title="Plan de pagos" style=" display: none"  class="ui-dialog"></div>



    <div align="center">           
           <% if(/*!convenio.getId_convenio().equals("17") && */convenio.getTipo().equals("Consumo") && convenio.isRedescuento() && convenio.isAval_anombre() && !convenio.isMediador_aval() && !convenio.getId_convenio().equals("35")){%>
        <img src="<%=BASEURL%>/images/botones/financiar_aval.gif" alt="Financiar Aval" id="financiar_Aval" style="cursor: pointer "  onClick="financiarAval(true)"  />

        <%}%>
        <%
		  if (!tipo_liq.equals("SIMULADOR")) {%>
        <img src="<%=BASEURL%>/images/botones/aceptar_negocio.gif" alt="" id="aceptar negocio" style="cursor:hand " onclick="javascript:form.submit()" /></span>
        <%}%>
        
       <% 
        //aqui validamos el boton de regresar para direccionar a la pagina correcta 
 if (tipo_liq.equals("SIMULADOR")) {%>
            <img src="<%=BASEURL%>/images/botones/regresar.gif" name="c_salir" onClick="location.href='<%=BASEURL%>/jsp/fenalco/liquidadores/liquidadorNegocios.jsp?tipo_liq=SIMULADOR';" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" style="cursor:hand ">
                
       <% } else {%>                      
            <img src="<%=BASEURL%>/images/botones/regresar.gif" name="c_salir" onClick="location.href='<%=BASEURL%>/jsp/fenalco/liquidadores/liquidadorNegocios.jsp';" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" style="cursor:hand ">
                    
       <%}%>
      
       <img src="<%=BASEURL%>/images/botones/salir.gif"  name="imgsalir"  onMouseOver="botonOver(this);" onClick="parent.close();" onMouseOut="botonOut(this);" style="cursor:hand">

   </div>




<br/>


                               <input name="vrnegocio" type="hidden" value="<%=neg.getVr_negocio()%>">
                               <input name="numsolc"  type="hidden" id="numsolc" value="<%=numero_form%>">
                               <input name="banco" type="hidden" value="<%=banco%>">
                               <input name="ciudadCh" type="hidden" value="<%=ciudadCh%>">
                               <input name="nodocs" type="hidden" value="<%=neg.getNodocs() %>">
                               <input name="vrdesem" type="hidden" value="<%=neg.getVr_negocio()%>">
                               <input name="tasa" type="hidden" value="<%=neg.getTasa() %>">
                               <input name="vrcust" type="hidden" value="<%=neg.getVr_cust() %>">
                               <input name="rem" type="hidden" value="<%=neg.getMod_rem()%>">
                               <input name="porcrem" type="hidden" value="<%=neg.getPor_rem()/100%>">
                               <input name="formpago" type="hidden" value="<%=neg.getFpago() %>">
                               <input name="tneg" type="hidden" value="<%=tipo_negocio%>">
                               <input name="codtab" type="hidden" value="<%=model.FenalcoFintraSvc.getcodtab()%>">
                               <input name="remesas" type="hidden" value="<%=neg.getMod_cust() %>">
                               <input name="fechaneg" type="hidden" value="<%=fechai%>">
                                <input name="tneg" type="hidden" value="<%=neg.getTneg()%>">
                               <input name="valor_aval" type="hidden" id="valor_aval"  value="<%=Math.round(neg.getVr_aval())%>">
                               <input name="valor_remesa" type="hidden" value="<%=Math.round(Double.parseDouble(neg.getvalor_remesa()))%>">
                               <input name="convenio" type="hidden" value="<%=neg.getId_convenio() %>">
                               <input name="idRemesa" type="hidden" value="<%=neg.getId_remesa() %>">
                               <input name="idsect" type="hidden" value="<%=neg.getId_sector() %>">
                                <input name="tpag" type="hidden" id="tpag"  value="<%=(neg.getVr_aval() + tcuota)%>">
                               <input name="idsubsect" type="hidden" value="<%=neg.getId_subsector() %>">
                                <input name="nit" type="hidden" value="<%=neg.getCod_cli() %>">
                                <input name="aval1" type="hidden" value="<%=0%>">
                                 <input name="nitp" type="hidden" value="<%=neg.getNit_tercero() %>">
                               <input name="valor_aval_tem" type="hidden" id="valor_aval_tem"  value="<%=Math.round(neg.getVr_aval())%>">
                               <input name="opcion" type="hidden" id="opcion"  value="<%=(String)(request.getAttribute("opcion")!=null?request.getAttribute("opcion"):"")%>">
                                <input name="act_liq" type="hidden" id="act_liq"  value="<%=act_liq%>">
 </form>
      </div>

 </body>
</html>
