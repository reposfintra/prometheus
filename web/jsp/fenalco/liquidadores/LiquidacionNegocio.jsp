<%--
    Document   : LiquidacionMicrocredito.jsp
    Created on : 17/07/2011, 04:37:55 PM
    Author     : jpinedo
--%>


<%@page import="com.tsp.opav.model.services.ClientesVerService"%>
<%@page contentType="text/html"%>
<%@page session="true"%>
<%@page errorPage="/error/ErrorPage.jsp"%>
<%@ page import="java.sql.*" %>
<%@ page import="java.util.*" %>
<%@ page import="java.io.*" %>
<%@ page import="java.lang.*" %>
<%@page import="java.util.*"%>
<%@page import="com.tsp.operation.model.*"%>
<%@page import="com.tsp.operation.model.beans.*"%>
<%@page import="com.tsp.util.*"%>
<%@page import="com.tsp.util.Util.*"%>
<%@page import="com.tsp.operation.model.services.GestionCondicionesService"%>
<%@include file="/WEB-INF/InitModel.jsp"%>
<%@ taglib uri="http://jakarta.apache.org/taglibs/input-0.90" prefix="input" %>

<%
    //Se reciben los parametros del liquidador

    String numsolc=    request.getParameter("numsolc");
    String fechanegocio=  request.getParameter("fechanegocio").substring(0, 10);
    String fechaliqui=  request.getParameter("fechaliqui").substring(0, 10);
    String fechatrans=request.getParameter("fechatrans");
    String convid= request.getParameter("convid");
    String estadoneg = request.getParameter("state");
    String nit= request.getParameter("nit");
    String tneg=   request.getParameter("tneg");
    String consulta=   request.getParameter("consulta")!=null?request.getParameter("consulta"):"";
    String vista=   request.getParameter("vista")!=null?request.getParameter("vista"):"";
    int numcuotas= Integer.parseInt(request.getParameter("numcuotas"));
    int fpago= Integer.parseInt(request.getParameter("fpago"));
    double valornegocio=Double.parseDouble(request.getParameter("valornegocio"));
    double tasainteres=Double.parseDouble(request.getParameter("tasainteres"));
    double tcuota=0;
    double tcapital=0;
    double tcustodia=0;
    double tseguro=0;
    double tcuota_manejo=0;
    double tinteres=0;
    double tremesa=0;
    double taval = 0;
    String negocio="";

      ArrayList<DocumentosNegAceptado> liquidacion= model.Negociossvc.getLiquidacion();
      Negocios neg =model.Negociossvc.getNegocio();

%>
<html>
    <head>

        <link href="<%=BASEURL%>/css/estilostsp.css" rel="stylesheet" type="text/css">

        <script language="Javascript">
            function imprSelec(nombre)
            {
                var ficha = document.getElementById(nombre);
                var ventimp = window.open(' ', 'popimpr');
                ventimp.document.write( ficha.innerHTML);
                ventimp.document.close();
                ventimp.print( );
                ventimp.close();
            }
            function generarRecibos(negocio){
                var url ="<%=CONTROLLER%>?estado=LiquidadorNeg&accion=Microcredito";
                var p = "opcion=generarRecibos&negocio="+negocio;
                new Ajax.Request(url, {
                    parameters: p,
                    method: 'post',
                    onComplete:  function (resp){
                        alert(resp.responseText);
                    }
                });
            }
			
			
			
			






			
			
			
        </script>
        <style type="text/css">
            <!--
            .style1 {font-size: 12px}
            -->
        </style>
    </head>
    <body >
        <%if (!consulta.equals("S")){%>
        <div id="capaSuperior" style="position:absolute; width:100%; height:100px; z-index:0; left: 0px; top: 0px;">
  
            <jsp:include page="/toptsp.jsp?encabezado=LIQUIDACION DE NEGOCIOS"/>

        </div>
      
            <script type='text/javascript' src="<%=BASEURL%>/js/Validacion.js"></script>
            <script type='text/javascript' src="<%= BASEURL%>/js/boton.js"></script>
            <script type='text/javascript' src="<%= BASEURL%>/js/general.js"></script>
        
     
         
            <div id="seleccion"><%}%>
                <table align="center" width="81%" border="0" cellpadding='0' cellspacing='0'>
                    <tr>
                        <td >

                            <table align="center" width="100%" cellpadding='0' cellspacing='0' class="tablaInferior" border="1" >

                                <tr class="fila">
                                    <td colspan = "2" align="center">
                                        <p><b>Información del Negocio</b></p>                                        
                                    </td>
                                </tr>

                                <tr class="filagris">
                                    <td width='50%'>
                                        <p><b>Valor del negocio</b></p>
                                    </td>
                                    <td width='50%'>
                                        <p ><%=Util.customFormat(valornegocio)%></p>
                                    </td>
                                </tr>
                                <tr class="filaazul">
                                    <td >
                                        <p><b>Tasa Interes</b></p>
                                    </td>
                                    <td>
                                        <p > <%=neg.getTasa()%>%</p>
                                    </td>
                                </tr>
                                <tr class="filagris">
                                    <td >
                                        <p><b>No.Cuotas</b></p>
                                    </td>
                                    <td>
                                        <p ><%=neg.getNodocs() %></p>
                                    </td>
                                </tr>
                                <tr class="filaazul">
                                    <td >
                                        <p><b>Remesa</b></p>
                                  </td>
                                    <td>
                                        <p ><%=Util.customFormat(neg.getPor_rem())%> %</p>
                                    </td>
                                </tr>
                                <tr class="filagris">
                                    <td >
                                        <p><b>Custodia</b></p>
                                    </td>
                                    <td>
                                        <p ><%=neg.getVr_cust()%></p>
                                    </td>
                                </tr>
                                <tr class="filaazul">
                                    <td >
                                        <p><b>Seguro</b></p>
                                    </td>
                                    <td>
                                        <p ><%=Util.customFormat(neg.getValor_seguro())%></p>
                                    </td>
                                </tr>
                                <%if(Double.parseDouble(neg.getvalor_aval())>0)
                                {%>
                                                                <tr class="filaazul">
                                    <td >
                                        <p><b>Valor Aval</b></p>
                                    </td>
                                    <td>
                                        <p ><%=Util.customFormat(Double.parseDouble(neg.getvalor_aval()))%></p>
                                    </td>
                                </tr>
                                <%}%>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td colspan='2'>
                            <br><br>
                        </td>
                    </tr>
                    <tr>
                        <td width='60%'>
                            <table width="100%" cellpadding='0' cellspacing='0' class="tablaInferior" border="1">
                                <tr class="fila">
                                    <td>
                                        <p align='center'><b>Fecha</b></p>
                                    </td>
                                    <td>
                                        <p align='center'><b>No. Cuota</b></p>
                                    </td>
                                    <td>
                                        <p align='center'><b>Saldo Inicial</b></p>
                                    </td>
                                    <td>
                                        <p align='center'><b>Capital</b></p>
                                    </td>
                                    <td>
                                        <p align='center'><b>Inter&eacute;s</b></p>
                                    </td>
                                    <td>
                                        <p align='center'><b>Custodia</b></p>
                                    </td>
                                    <td>
                                        <p align='center'><b>Remesa</b></p>
                                    </td>
                                    <td>
                                        <p align='center'><b>Seguro</b></p>
                                    </td>
                                    <td>
                                        <p align='center'><b>Intermediación Financiera</b></p>
                                    </td>
                                    <td>
                                        <p align='center'><b>Aval</b></p>
                                    </td>
                                    <td><p align='center'><b>Valor Cuota</b></p></td>
                                    <td>
                                        <p align='center'><b>Saldo Final</b></p>
                                    </td>
                                </tr>
                                <% for(int i=0;i<liquidacion.size();i++){
                                        negocio=liquidacion.get(i).getCod_neg();
                                        tcuota+=liquidacion.get(i).getValor();
                                        tcapital += liquidacion.get(i).getCapital();
                                        tcustodia += liquidacion.get(i).getCustodia();
                                        tseguro += liquidacion.get(i).getSeguro();
                                        tcuota_manejo += liquidacion.get(i).getCuota_manejo();
                                        tinteres += liquidacion.get(i).getInteres();
                                        tremesa +=liquidacion.get(i).getRemesa();
                                        taval +=liquidacion.get(i).getAval();
                                %>
                                 <tr class="filaazul">
                                    <td>
                                        <p align='center'><%=liquidacion.get(i).getFecha().substring(0, 10)%></p>
                                    </td>
                                    <td>
                                        <p align='center'><%=liquidacion.get(i).getItem()%></p>
                                    </td>
                                    <td>
                                        <p align='center'><%=Util.customFormat(liquidacion.get(i).getSaldo_inicial())%></p>
                                    </td>
                                    <td>
                                        <p align='center'><%=Util.customFormat(liquidacion.get(i).getCapital())%></p>
                                    </td>
                                    <td>
                                        <p align='center'><%=Util.customFormat(liquidacion.get(i).getInteres())%></p>
                                    </td>
                                    <td>
                                        <p align='center'><%=Util.customFormat(liquidacion.get(i).getCustodia())%></p>
                                    </td>
                                    <td>
                                        <p align='center'><%=Util.customFormat(liquidacion.get(i).getRemesa())%></p>
                                    </td>
                                    <td>
                                        <p align='center'><%=Util.customFormat(liquidacion.get(i).getSeguro())%></p>
                                    </td>
                                    <td>
                                        <p align='center'><%=Util.customFormat(liquidacion.get(i).getCuota_manejo())%></p>
                                    </td>
                                    <td>
                                        <p align='center'><%=Util.customFormat(liquidacion.get(i).getAval())%></p>
                                    </td>
                                    <td><p align='center'><%=Util.customFormat(liquidacion.get(i).getValor())%></p></td>
                                    <td>
                                        <p align='center'><%=Util.customFormat(liquidacion.get(i).getSaldo_final())%></p>
                                    </td>
                                </tr>

                                <%}%>
                               
                                <tr >
                                    <td> </td>
                                    <td> </td>
                                    <td> </td>
                                    <td align="center" class="fila style1"><%=Util.customFormat( tcapital)%></td>
                                    <td align="center" class="fila style1"><%=Util.customFormat( tinteres)%></td>
                                    <td align="center" class="fila style1"><%=Util.customFormat( tcustodia)%></td>
                                    <td align="center" class="fila style1"><%=Util.customFormat( tremesa)%></td>
                                    <td align="center" class="fila style1"><%=Util.customFormat( tseguro)%></td>
                                    <td align="center" class="fila style1"><%=Util.customFormat( tcuota_manejo)%></td>
                                    <td align="center" class="fila style1"><%=Util.customFormat( taval)%></td>
                                    <td align="center" class="fila style1"><%=Util.customFormat( tcuota)%></td>
                                    <td> </td>
                                </tr>
                            </table>
                        </td>
                       
                    </tr>
                   
                    <%if(!consulta.equals("S")){%>
                    <tr>
                        <td colspan='2'>
                            <p>&nbsp;</p>
                            <p align='center'></td>
                    </tr>
                    <%}%>
                    
                    <tr>
                        <td colspan="2" align="center"><p>&nbsp;</p>
                            <form id="form2" name="form2" method="post" action="<%=CONTROLLER%>?estado=Negocios&accion=Insert">
                              <br/>
                              
                              <input name="numsolc"  type="hidden" id="numsolc" value="<%=numsolc%>">
                              <input name="nodocs" type="hidden" value="<%=neg.getNodocs() %>">
                              <input name="vrdesem" type="hidden" value="<%=neg.getVr_desem() %>">
                              <input name="tasa" type="hidden" value="<%=neg.getTasa() %>">
                              <input name="vrcust" type="hidden" value="<%=neg.getVr_cust() %>">
                              <input name="rem" type="hidden" value="<%=neg.getMod_rem()%>">
                              <input name="porcrem" type="hidden" value="<%=neg.getPor_rem()/100%>">
                              <input name="formpago" type="hidden" value="<%=neg.getFpago() %>">
                      
                              <input name="codtab" type="hidden" value="<%=model.FenalcoFintraSvc.getcodtab()%>">
                              <input name="remesas" type="hidden" value="<%=neg.getMod_cust() %>">
                      
                              <input name="tneg" type="hidden" value="<%=neg.getTneg()%>">
                              <input name="valor_aval" type="hidden" id="valor_aval"  value="<%=Math.round(neg.getVr_aval())%>">
                              <input name="valor_remesa" type="hidden" value="0">
                              <input name="convenio" type="hidden" value="<%=neg.getId_convenio() %>">
                              <input name="idRemesa" type="hidden" value="<%=neg.getId_remesa() %>">
                              <input name="idsect" type="hidden" value="<%=neg.getId_sector() %>">
                              <input name="tpag" type="hidden" id="tpag"  value="<%=(neg.getVr_aval() + tcuota)%>">
                              <input name="idsubsect" type="hidden" value="<%=neg.getId_subsector() %>">
                              <input name="nit" type="hidden" value="<%=neg.getCod_cli() %>">
                              <input name="aval1" type="hidden" value="<%=0%>">
                              <input name="valor_aval_tem" type="hidden" id="valor_aval_tem"  value="<%=Math.round(neg.getVr_aval())%>">
                              <p>&nbsp;</p>
                            </form>
                        </td>
                    </tr>
                </table>
                   
    </body>
</html>