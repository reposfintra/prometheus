
<%@page import="com.tsp.opav.model.services.ClientesVerService"%>
<%@page contentType="text/html"%>
<%@page session="true"%>
<%@page errorPage="/error/ErrorPage.jsp"%>
<%@ page import="java.sql.*" %>
<%@ page import="java.util.*" %>
<%@ page import="java.io.*" %>
<%@ page import="java.lang.*" %>
<%@page import="java.util.*"%>
<%@page import="com.tsp.operation.model.*"%>
<%@page import="com.tsp.operation.model.beans.*"%>
<%@page import="com.tsp.util.*"%>
<%@page import="com.tsp.util.Util.*"%>
<%@page import="com.tsp.operation.model.services.GestionCondicionesService"%>
<%@include file="/WEB-INF/InitModel.jsp"%>
<%@ taglib uri="http://jakarta.apache.org/taglibs/input-0.90" prefix="input" %>

<%
    //Se reciben los parametros del liquidador
    String opcion=(String)(request.getAttribute("opcion")!=null?request.getAttribute("opcion"):"");
    String idsector=(String)request.getAttribute("idsect");
    String idsubsector=(String)request.getAttribute("idsubsect");
    String numero_form= (String)request.getAttribute("numsolc");
    String tipo_liq = (String)request.getAttribute("tipo_liq");
    String custodia = (String)request.getAttribute("custodia");
    String aval = (String)request.getAttribute("aval");
    String Estado_Remesa = (String)request.getAttribute("Estado_Remesa");
    String remesas = (String)request.getAttribute("remesas");
    String tipo_negocio = (String) request.getAttribute("tipo_negocio");
    String ParValor_Negocio = (String) request.getAttribute("ParValor_Negocio");
    String Forma_Pago = (String) request.getAttribute("Forma_Pago");
    String fechai = (String)request.getAttribute("fechai");
    String propietario = (String) request.getAttribute("propietario");
    String convenio = (String) request.getAttribute("convenio");
    String banco=(String) request.getAttribute("banco");
    String ciudadCh=(String) request.getAttribute("ciudadCh");
    String numero_solicitud = (String) request.getAttribute("numero_solicitud");
    String ciclo = (String) request.getAttribute("ciclo");
    String prop = (String) request.getAttribute("prop");
    double Valor_Negocio = (Double)request.getAttribute("Valor_Negocio");
    int Numero_Cheques = (Integer)request.getAttribute("Numero_Cheques");
    double Desembolso = (Double)request.getAttribute("Desembolso");
    double Valor_Doc = (Double)request.getAttribute("Valor_Doc");
    Calendar Fecha_Negocio = (Calendar)request.getAttribute("Fecha_Negocio");
    String Aux = (String) request.getAttribute("Aux");
    double Tasa = (Double)request.getAttribute("Tasa");
    double Custodia_Cheque = (Double) request.getAttribute("Custodia_Cheque");
    double porrem = (Double) request.getAttribute("porrem");
    double TotalRemesa_x = (Double)request.getAttribute("TotalRemesa_x");
    double TotalAval = (Double)request.getAttribute("TotalAval");
    String idRemesa = (String) request.getAttribute("idRemesa");
    ArrayList chequesx = (ArrayList) request.getAttribute("chequesx");
    String parametros = (String)request.getAttribute("parametros");

    String Filas_Tabla_Liquidacion = (String)request.getAttribute("Filas_Tabla_Liquidacion");
    String Filas_Tabla_Liquidacion_2 = (String)request.getAttribute("Filas_Tabla_Liquidacion_2");
    Usuario usuario = (Usuario) session.getAttribute("Usuario");
    String act_liq=(String)(request.getAttribute("act_liq")!=null?request.getAttribute("act_liq"):"");
    ClientesVerService clvsrv = new ClientesVerService(usuario.getBd());
      String perfil = clvsrv.getPerfil(usuario.getLogin());
     boolean sw_ver_saldos=clvsrv.ispermitted(perfil, "60");

%>
<html>
    <head>
        <title>Liquidador Maestro</title>

        <link href="<%=BASEURL%>/css/estilostsp.css" rel="stylesheet" type="text/css">
        <script language="Javascript">
            function imprSelec(nombre)
            {
                var ficha = document.getElementById(nombre);
                var ventimp = window.open(' ', 'popimpr');
                ventimp.document.write( ficha.innerHTML);
                ventimp.document.close();
                ventimp.print( );
                ventimp.close();
            }
        </script>
        <style type="text/css">
            <!--
            .style1 {font-size: 12px}
            -->
        </style>
    </head>
    <body >
        <div id="capaSuperior" style="position:absolute; width:100%; height:100px; z-index:0; left: 0px; top: 0px;">
            <jsp:include page="/WEB-INF/toptsp.jsp?encabezado=Liquidador Maestro."/>
        </div>
        <div id="capaCentral" style="position:absolute; width:100%; height:85%; z-index:0; left: 0px; top: 100px; overflow: scroll;">
            <script type='text/javascript' src="<%=BASEURL%>/js/Validacion.js"></script>
            <script type='text/javascript' src="<%= BASEURL%>/js/boton.js"></script>
            <script type='text/javascript' src="<%= BASEURL%>/js/general.js"></script>
          <%if (!tipo_liq.equals("SIMULADOR")){%>
            <a onClick="javascript:imprSelec('seleccion')"><img src="<%=BASEURL%>/images/imprime.gif" width="70" height="19" style="cursor:hand"></a>
            <%}%>
          
            
            
            <div id="seleccion">
                <table align="center" width="81%" border="0" cellpadding='0' cellspacing='0'>
                    <tr>
                        <td >

                            <table align="center" width="100%" cellpadding='0' cellspacing='0' class="tablaInferior" border="1" >

                                <tr class="fila">
                                    <td colspan = "2" align="center">
                                        <p><b>Información del Negocio</b></p>                                        
                                    </td>
                                </tr>

                                <tr class="filagris">
                                    <td width='50%'>
                                        <p><b>Valor del negocio</b></p>
                                    </td>
                                    <td width='50%'>
                                        <p ><%=Util.customFormat(Valor_Negocio)%></p>
                                    </td>
                                </tr>
                                <tr class="filaazul">
                                    <td >
                                        <p><b># Doc.</b></p>
                                    </td>
                                    <td>
                                        <p ><%=Numero_Cheques%></p>
                                    </td>
                                </tr>
                                <tr class="filagris">
                                    <td >
                                        <p><b>Valor desembolso</b></p>
                                    </td>
                                    <td>
                                        <p ><%=Util.customFormat(Desembolso)%></p>
                                    </td>
                                </tr>
                                <tr class="filaazul">
                                    <td >
                                        <p><b>Valor del Doc.</b></p>
                                    </td>
                                    <td>
                                        <p ><%=Util.customFormat(Valor_Doc)%></p>
                                    </td>
                                </tr>
                                <tr class="filagris">
                                    <td >
                                        <p><b>Fecha negocio</b></p>
                                    </td>
                                    <td>
                                        <p ><%=Util.ObtenerFechaCompleta(Fecha_Negocio)%></p>
                                    </td>
                                </tr>
                            </table>
                        </td>
                        <td >
                            <table align="center" width="100%" cellpadding='0' cellspacing='0' class="tablaInferior" border="1" >
                                <tr class="fila">
                                    <td width='25%' align="center">
                                        <p><b></b></p>
                                    </td>
                                    <td width='50%' align="center">
                                        <p><b>Establecimiento de Comercio</b></p>
                                    </td>
                                    <td width='25%' align="center">
                                        <p><b>Cliente</b></p>
                                    </td>
                                </tr>
                                <tr class="filagris">
                                    <td width='25%' align="center">
                                        <p><b>Custodia</b></p>
                                    </td>
                                    <td width='50%' align="center">
                                        <%if (custodia.equals("0")) {
                                        %><p><b>SI</b></p><%} else {%><p><b>NO</b></p><%}%>
                                    </td>
                                    <td width='25%' align="center">
                                        <%if (custodia.equals("0")) {
                                        %><p><b>NO</b></p><%} else {%><p><b>SI</b></p><%}%>
                                    </td>
                                </tr>
                                <tr class="filaazul">
                                    <td width='25%' align="center">
                                        <p><b>Aval</b></p>
                                    </td>
                                    <td width='50%' align="center">
                                        <%if (aval.equals("0")) {
                                        %><p><b>SI</b></p><%} else {%><p><b>NO</b></p><%}%>
                                    </td>
                                    <td width='25%' align="center">
                                        <%if (aval.equals("0")) {
                                        %><p><b>NO</b></p><%} else {%><p><b>SI</b></p><%}%>
                                    </td>
                                </tr>
                                <%if (Estado_Remesa.equals("0")) { %>
                                <tr class="filagris">
                                    <td width='25%' align="center">
                                        <p><b>Remesas</b></p>
                                    </td>
                                    <td width='50%' align="center">
                                        <%if (remesas.equals("0")) {
                                        %><p><b>SI</b></p><%} else {%><p><b>NO</b></p><%}%>
                                    </td>
                                    <td width='25%' align="center">
                                        <%if (remesas.equals("0")) {
                                        %><p><b>NO</b></p><%} else {%><p><b>SI</b></p><%}%>
                                    </td>
                                </tr>
                                <%} else {%>
                                <tr class="filagris">
                                    <td width='25%' align="center">
                                        <p><b>Remesas</b></p>
                                    </td>
                                    <td width='50%' align="center">
                                        <p><b>NO</b></p>
                                    </td>
                                    <td width='25%' align="center">
                                        <p><p><b>NO</b></p>
                                    </td>
                                </tr>
                                <%}%>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td colspan='2'>
                            <br><br>
                        </td>
                    </tr>
                    <tr>
                        <td width='40%'>
                            <table width="100%" cellpadding='0' cellspacing='0' class="tablaInferior" border="1">
                                <tr class="fila">
                                    <td>
                                        <p align='center'><b># Doc.</b></p>
                                    </td>
                                    <td>
                                        <p align='center'><b>Fecha</b></p>
                                    </td>
                                </tr>
                                <%=Filas_Tabla_Liquidacion%>
                                <tr >
                                    <td> </td>
                                    <td align="center" class="fila style1"> Total a Pagar   </td>
                                </tr>
                            </table>
                        </td>
                        <td width='40%'>
                            <table width="100%" cellpadding='0' cellspacing='0' class="tablaInferior" border="1">
                                <tr class="fila">
                                    <% if(sw_ver_saldos) { %><td>
                                      <p align='center'><b>Saldo inicial</b></p>
                                    </td>
                                    <%}%>
                                    <td>
                                        <p align='center'><b>Vr <%if (tipo_negocio.equals("01")) {%>Cheques<%} else {%>Letras<%}%></b></p>
                                    </td>
                                     <% if(sw_ver_saldos) { %>
                                    <td>
                                       <p align='center'><b>Saldo final</b></p>
                                    </td>
                                    <%}%>
                                </tr>
                                <%=Filas_Tabla_Liquidacion_2%>
                                <tr >
                                    <td align="right" colspan="2" class="fila style1"> <%=Util.customFormat(Valor_Doc * Numero_Cheques)%></td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                     <tr class="filaazul">
                        <td align="left" >
                            <% TablaGen tOpcion=null;
                          model.tablaGenService.buscarDatos("VAL_OPCION", "TITULOS_VALORES");
                       tOpcion = model.tablaGenService.getTblgen();
                      if(  (act_liq.equals("")) && (model.usuarioService.ValidarOpcion(usuario.getLogin(), tOpcion.getReferencia()) ))
                          {
                            %>
                            &iquest;Desea adjuntar documentos? 
                            <label>
                                <select name="radicacion" id="radicacion">
                                    <option value="S" selected>SI</option>
                                    <option value="N">NO</option>
                                </select>
                            </label>
                            <%}%>
                        </td>
                    </tr>
                    <tr>
                        <td colspan='2'>
                            <p>&nbsp;</p>
                            <p align='center'>
                                <img src="<%=BASEURL%>/images/botones/Aval.gif" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" style="cursor:hand" height="21" title='Aval'
                                     onClick="location.href='<%=CONTROLLER%>?tipoCalculo=calculoAval<%=parametros%>&aval=<%=aval%>&remesas=<%=remesas%>&Estado_Remesa=<%=Estado_Remesa + Aux%>'">
                                <img src="<%=BASEURL%>/images/botones/regresar.gif" name="c_salir" onClick="location.href='<%=BASEURL%>/jsp/fenalco/liquidadores/liquidadorNegocios.jsp?tipo_liq=<%=tipo_liq%>';" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" style="cursor:hand ">
                                <img src="<%=BASEURL%>/images/botones/salir.gif"  name="imgsalir"  onMouseOver="botonOver(this);" onClick="parent.close();" onMouseOut="botonOut(this);" style="cursor:hand">

                        </td>
                    </tr>
                    <tr>
                        <td colspan="2" align="center"><p>&nbsp;</p>
                            <form name="form1" method="post" action="<%=CONTROLLER%>?estado=Negocios&accion=Insert">
                                <input name="nit" type="hidden" value="<%=(String) session.getAttribute("nitcm")%>">
                                <input name="vrnegocio" type="hidden" value="<%=ParValor_Negocio%>">
                                <input name="numsolc" type="hidden" value="<%=numero_form%>">
                                <input name="banco" type="hidden" value="<%=banco%>">
                                <input name="ciudadCh" type="hidden" value="<%=ciudadCh%>">
                                <input name="nodocs" type="hidden" value="<%=Numero_Cheques%>">
                                <input name="vrdesem" type="hidden" value="<%=Desembolso%>">
                                <input name="vraval" type="hidden" value="<%=Tasa * 100%>">
                                <input name="vrcust" type="hidden" value="<%=Custodia_Cheque%>">
                                <input name="rem" type="hidden" value="<%=Estado_Remesa%>">
                                <input name="porcrem" type="hidden" value="<%=porrem%>">
                                <input name="cust1" type="hidden" value="<%=custodia%>">
                                <input name="aval1" type="hidden" value="<%=aval%>">
                                <input name="formpago" type="hidden" value="<%=Forma_Pago%>">
                                <input name="tneg" type="hidden" value="<%=tipo_negocio%>">
                                <input name="codtab" type="hidden" value="<%=model.FenalcoFintraSvc.getcodtab()%>">
                                <input name="remesas" type="hidden" value="<%=remesas%>">
                                <input name="fechaneg" type="hidden" value="<%=fechai%>">
                                <input name="nitp" type="hidden" value="<%=propietario%>">
                                <input name="tpag" type="hidden" value="<%=(Valor_Doc * Numero_Cheques)%>">
                                <input name="valor_aval" type="hidden" value="<%=Math.round(TotalAval)%>">
                                <input name="valor_remesa" type="hidden" value="<%=Math.round(TotalRemesa_x)%>">
                                <input name="convenio" type="hidden" value="<%=convenio%>">
                                <input name="idRemesa" type="hidden" value="<%=idRemesa%>">
                                <input name="idsect" type="hidden" value="<%=idsector%>">
                                <input name="idsubsect" type="hidden" value="<%=idsubsector%>">
                                <input name="opcion" id="opcion" type="hidden" value="<%=opcion%>">
                                <input name="act_liq" type="hidden" id="act_liq"  value="<%=act_liq%>">
                                <%
                                    model.Negociossvc.setCheques(chequesx);
                                    if (numero_solicitud != null) {%>
                                    <input name="numero_solicitud" type="hidden" value="<%=numero_solicitud%>">
                                    <input name="ciclo" type="hidden" value="<%=ciclo%>">
                                    <input name="prop" type="hidden" value="<%=prop%>">
                                <%  }
                                    java.util.Calendar Fecha_Final = null;
                                    if (!tipo_liq.equals("SIMULADOR")) {%>
                                        <p><img src="<%=BASEURL%>/images/botones/Aceptar_neg.gif"  name="aceptarneg"  onMouseOver="botonOver(this);" onClick="javascript:form1.submit()" onMouseOut="botonOut(this);" style="cursor:hand"></p>
                                    <%}%>
                            </form>
                        </td>
                    </tr>
                </table>
            </div>
        </div>
    </body>
</html>
