<%--
    Document   : LiquidacionMicrocredito.jsp
    Created on : 11/07/2012, 04:37:55 PM
    Author     : JESUS PINEDO
--%>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<%@page import="com.tsp.opav.model.services.ClientesVerService"%>
<%@page contentType="text/html"%>
<%@page session="true"%>
<%@page errorPage="/error/ErrorPage.jsp"%>
<%@ page import="java.sql.*" %>
<%@ page import="java.util.*" %>
<%@ page import="java.io.*" %>
<%@ page import="java.lang.*" %>
<%@page import="java.util.*"%>
<%@page import="com.tsp.operation.model.*"%>
<%@page import="com.tsp.operation.model.beans.*"%>
<%@page import="com.tsp.util.*"%>
<%@page import="com.tsp.util.Util.*"%>
<%@page import="com.tsp.operation.model.services.GestionCondicionesService"%>
<%@include file="/WEB-INF/InitModel.jsp"%>
<%@ taglib uri="http://jakarta.apache.org/taglibs/input-0.90" prefix="input" %>


<%

      
      ArrayList<DocumentosNegAceptado> liquidacionAval= model.Negociossvc.getLiquidacionAval();
      Negocios negAval =model.Negociossvc.getNegocioAval();
	  String tipo_liq = (String)request.getParameter("tipo_liq");
      double tcuota=0;
      for(int i=0;i<liquidacionAval.size();i++)
      {
          tcuota+=liquidacionAval.get(i).getValor();
       }

%>

<html>

    <head>
      <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
  <link href="<%=BASEURL%>/css/estilostsp.css" rel="stylesheet" type="text/css">
          <link href="<%= BASEURL%>/css/jquery/jquery-ui/jquery-ui.css" rel="stylesheet" type="text/css"/>
        <link href="<%= BASEURL%>/css/jquery/jquery-ui/jquery.ui.selectmenu.css" rel="stylesheet" type="text/css"/>
                <link href="<%= BASEURL%>/css/jquery/jquery.jqGrid/ui.jqgrid.css" rel="stylesheet" type="text/css"/>
        <script language="Javascript">
			
		

      
        
                $(document).ready(
                function() {
		$("#tabla_datos_aval").styleTable();
		
		

		 
                    
                    jQuery("#liquidacion_aval").jqGrid({
												
                        caption: "Liquidacion" ,
                        url: "<%=CONTROLLER%>?estado=Liquidador&accion=Negocios&opcion=getLiquidacion&tipo=2",
                        datatype: "json",
                        height: 250,
                        width: 950,
                     colNames:['Cuota','Fecha','Saldo Inicial','Capital','Interes','Custodia','Seguro','Remesa','Valor Cuota','Saldo Final'],
                       colModel:[
						    {name:'item',index:'item', sortable:false,align:'center',width: 50 },		 
                            {name:'fecha',index:'fecha', sortable:false,align:'center',width: 100 },										  
                            {name:'saldo_inicial',index:'saldo_inicial', sortable:false, align:'right',width: 100,
		            formatter:'currency', formatoptions:{decimalSeparator:",", thousandsSeparator: ".", decimalPlaces: 0, prefix: "$ "}} ,
					      {name:'capital',index:'capital', sortable:false, align:'right',width: 100,
		            formatter:'currency', formatoptions:{decimalSeparator:",", thousandsSeparator: ".", decimalPlaces: 0, prefix: "$ "}} ,

					      {name:'interes',index:'interes', sortable:false, align:'right',width: 100,
		            formatter:'currency', formatoptions:{decimalSeparator:",", thousandsSeparator: ".", decimalPlaces: 0, prefix: "$ "}} ,
					      {name:'custodia',index:'custodia', sortable:false, align:'right',width: 100,
		            formatter:'currency', formatoptions:{decimalSeparator:",", thousandsSeparator: ".", decimalPlaces: 0, prefix: "$ "}} ,
					      {name:'seguro',index:'seguro', sortable:false, align:'right',width: 100,
		            formatter:'currency', formatoptions:{decimalSeparator:",", thousandsSeparator: ".", decimalPlaces: 0, prefix: "$ "}} ,
					      {name:'remesa',index:'remesa', sortable:false, align:'right',width: 100,
		            formatter:'currency', formatoptions:{decimalSeparator:",", thousandsSeparator: ".", decimalPlaces: 0, prefix: "$ "}} ,
					      {name:'valor',index:'valor', sortable:false, align:'right',width: 100,
		            formatter:'currency', formatoptions:{decimalSeparator:",", thousandsSeparator: ".", decimalPlaces: 0, prefix: "$ "}} ,
					      {name:'saldo_final',index:'saldo_final', sortable:false, align:'right',width: 100,
		            formatter:'currency', formatoptions:{decimalSeparator:",", thousandsSeparator: ".", decimalPlaces: 0, prefix: "$ "}}                                                     
                    ],
                        rowNum:100000,
                        rowTotal: 500000,
                        loadonce:true,
                        rownumWidth: 40,
                        gridview: true,
                        viewrecords: true,
                       hidegrid: false, 	footerrow : true, userDataOnFooter : true, altRows : true ,
									
                        jsonReader : {						
                            root:"rows",
                            repeatitems: false,
                            id: "0"
							
                        },
                        loadError: function(xhr, status, error){
                            alert(error);
                        }
                    });          


     });


		
			
        </script>
               

    </head>
    
             
<body >
<table  width="40%" align="center" id="tabla_datos_aval"  >
<tr>
                <th align="left" colspan="2" ><strong> Datos Liquidacion   Aval</strong></th>
                <th align="left" colspan="2" > <img alt="" src="<%= BASEURL%>/images/titulo.gif" width="32" height="20" align="left"/></th>
              </tr>
              <tr >
                <th width="128">Valor Aval</th>
                <td width="159"><%=Util.customFormat(negAval.getVr_negocio())%></td>
                <th width="148">Seguro</th>
                <td width="109"><%=Util.customFormat(negAval.getValor_seguro())%></td>
              </tr>
              <tr >
                <th>Tasa Interes</th>
                <td><%=negAval.getTasa()%></td>
                <th>Fecha negocio</th>
                <td><%=negAval.getFecha_neg() %></td>
              </tr>
              <tr >
                <th>No.Cuotas</th>
                <td><%=negAval.getNodocs() %></td>
                <th>Fecha liquidaci&oacute;n</th>
                <td><%=negAval.getFecha_liquidacion()%></td>
              </tr>
              <tr >
                <th>Custodia</th>
                <td><%=negAval.getVr_cust()%></td>
                <th>Total a Pagar</th>
                <td><%=Util.customFormat(negAval.getVr_aval() + tcuota )%></td>
              </tr>
              <tr >
                <th>Remesa</th>
                <td><%=Util.customFormat(negAval.getPor_rem())%>%</td>
                <th>&nbsp;</th>
                <td>&nbsp;</td>
              </tr>
            </table>
<br/>
                
                <table border="0"  align="center">
                  <tr>
                    <td>
                     <table id="liquidacion_aval" ></table>
                    </td>
                    </tr>
                </table>
                <br/>

    <div align="center">
    		 <% if (!tipo_liq.equals("SIMULADOR")) {%>
        <img src="<%=BASEURL%>/images/botones/aceptar_negocio.gif" alt="" id="aceptar negocio" style="cursor:hand " onclick="javascript:form.submit()" />
        <%}%>

                                <img src="<%=BASEURL%>/images/botones/cancelar.gif"  name="imgsalir"  onMouseOver="botonOver(this);" onClick="financiarAval(false)"   onMouseOut="botonOut(this);" style="cursor:hand">

   </div>                                               


                
 </body>
</html>