<%--
    Document   : LiquidacionMicrocredito.jsp
    Created on : 27/12/2011, 04:37:55 PM
    Author     : Iris Vargas
--%>


<%@page import="com.tsp.opav.model.services.ClientesVerService"%>
<%@page contentType="text/html"%>
<%@page session="true"%>
<%@page errorPage="/error/ErrorPage.jsp"%>
<%@ page import="java.sql.*" %>
<%@ page import="java.util.*" %>
<%@ page import="java.io.*" %>
<%@ page import="java.lang.*" %>
<%@page import="java.util.*"%>
<%@page import="com.tsp.operation.model.*"%>
<%@page import="com.tsp.operation.model.beans.*"%>
<%@page import="com.tsp.util.*"%>
<%@page import="com.tsp.util.Util.*"%>
<%@page import="com.tsp.operation.model.services.GestionCondicionesService"%>
<%@include file="/WEB-INF/InitModel.jsp"%>
<%@ taglib uri="http://jakarta.apache.org/taglibs/input-0.90" prefix="input" %>

<%
    //Se reciben los parametros del liquidador

    String numsolc=    request.getParameter("numsolc");
    String fechanegocio=  request.getParameter("fechanegocio").substring(0, 10);
    String fechaliqui=  request.getParameter("fechaliqui").substring(0, 10);
    
    String fechatrans=request.getParameter("fechatrans");
    String fechaPrimeraCuota=request.getParameter("fechaItem");
    String convid= request.getParameter("convid");
    String estadoneg = request.getParameter("state");
    String nit= request.getParameter("nit");
    String tneg=   request.getParameter("tneg");
    String consulta=   request.getParameter("consulta")!=null?request.getParameter("consulta"):"";
    String vista=   request.getParameter("vista")!=null?request.getParameter("vista"):"";
    int numcuotas= Integer.parseInt(request.getParameter("numcuotas"));
    int fpago= Integer.parseInt(request.getParameter("fpago"));
    double valornegocio=Double.parseDouble(request.getParameter("valornegocio"));
    double tasainteres=Double.parseDouble(request.getParameter("tasainteres"));
    double cat=Double.parseDouble( request.getParameter("cat"));
    double capacitacion=Double.parseDouble( request.getParameter("capacitacion"));
    double central=Double.parseDouble( request.getParameter("central"));
    double tasa_sic_ea=Double.parseDouble( request.getParameter("tasa_sic_ea")!=null?request.getParameter("tasa_sic_ea"):"0");
    double tasa_max_fintra=Double.parseDouble( request.getParameter("tasa_max_fintra")!=null?request.getParameter("tasa_max_fintra"):"0");
    double tcuota=0;
    double tcapital=0;
    double tcapacitacion=0;
    double tseguro=0;
    double tinteres=0;
    double tcat=0;
    double tcuota_admin=0;
    double tvalorcuotapoliza=0;
    double tvaloraval=0;
    double tvalorkaval=0;
    double tvaloriaval=0;
    double valor_cuota=0;
    double total_liq=0;
    double tir =0;
    double taval =0;
    String negocio="";
      ArrayList<DocumentosNegAceptado> liquidacion= model.Negociossvc.getLiquidacion();
  String trans = request.getParameter("trans") != null ? request.getParameter("trans") : "N";
  double valorfianza=Double.parseDouble(request.getParameter("valorfianza"));
  double valor_total_poliza=Double.parseDouble(request.getParameter("valor_total_poliza"));
  double porc_dto_aval=Double.parseDouble(request.getParameter("porc_dto_aval")!=null?request.getParameter("porc_dto_aval"):"0");
  double porc_fin_aval=Double.parseDouble(request.getParameter("porc_fin_aval")!=null?request.getParameter("porc_fin_aval"):"0");
   
%>
<html>
    <head>
        <% if(trans.equals("S")){%>
        <title>Liquidacion de negocios Microcredito</title>
        <%}else{%>
         <title>Liquidacion de negocios Cupo Express</title>
        <%}%>

        <link href="<%=BASEURL%>/css/estilostsp.css" rel="stylesheet" type="text/css">
        <script language="Javascript">
            function imprSelec(nombre)
            {
                var ficha = document.getElementById(nombre);
                var ventimp = window.open(' ', 'popimpr');
                ventimp.document.write( ficha.innerHTML);
                ventimp.document.close();
                ventimp.print( );
                ventimp.close();
            }
            function generarRecibos(negocio){
                var url ="<%=CONTROLLER%>?estado=LiquidadorNeg&accion=Microcredito";
                var p = "opcion=generarRecibos&negocio="+negocio;
                new Ajax.Request(url, {
                    parameters: p,
                    method: 'post',
                    onComplete:  function (resp){
                        alert(resp.responseText);
                    }
                });
            }
        </script>
        <style type="text/css">
            <!--
            .style1 {font-size: 12px}
            -->
            .anchop{
                width: 100px;
            }
        </style>
    </head>
    <body >
        <%if (!consulta.equals("S")){%>
        <div id="capaSuperior" style="position:absolute; width:100%; height:100px; z-index:0; left: 0px; top: 0px;">
            <% if (trans.equals("S")) {%>
            <jsp:include page="/toptsp.jsp?encabezado=LIQUIDADOR DE NEGOCIOS CUPO EXPRESS"/>
            <%} else {%>
            <jsp:include page="/toptsp.jsp?encabezado=LIQUIDADOR DE NEGOCIOS MICROCREDITO"/>

            <%}%>
        </div>
        <div id="capaCentral" style="position:absolute; width:100%; height:85%; z-index:0; left: 0px; top: 100px; overflow: scroll;">
            <script type='text/javascript' src="<%=BASEURL%>/js/Validacion.js"></script>
            <script type='text/javascript' src="<%= BASEURL%>/js/boton.js"></script>
            <script type='text/javascript' src="<%= BASEURL%>/js/general.js"></script>
        
            <a onClick="javascript:imprSelec('seleccion')"><img src="<%=BASEURL%>/images/imprime.gif" width="70" height="19" style="cursor:hand"></a>
         
            <div id="seleccion"><%}%>
                <table align="center" width="81%" border="0" cellpadding='0' cellspacing='0'>
                    <tr>
                        <td >

                            <table align="center" width="100%" cellpadding='0' cellspacing='0' class="tablaInferior" border="1" >

                                <tr class="fila">
                                    <td colspan = "2" align="center">
                                        <p><b>Información del Negocio</b></p>                                        
                                    </td>
                                </tr>

                                <tr class="filagris">
                                    <td width='50%'>
                                        <p><b>Valor del negocio</b></p>
                                    </td>
                                    <td width='50%'>
                                        <p ><%=Util.customFormat(valornegocio)%></p>
                                    </td>
                                </tr>
                                <tr class="filaazul">
                                    <td >
                                        <p><b>Tasa Interes</b></p>
                                    </td>
                                    <td>
                                        <p ><%=tasainteres%> %</p>
                                    </td>
                                </tr>
                                <tr class="filagris">
                                    <td >
                                        <p><b>No.Cuotas</b></p>
                                    </td>
                                    <td>
                                        <p ><%=numcuotas%></p>
                                    </td>
                                </tr>
                                <tr class="filaazul">
                                    <td >
                                        <p><b>Porcentaje Comisi&oacute;n Ley mipyme</b></p>
                                    </td>
                                    <td>
                                        <p ><%=cat%> %</p>
                                    </td>
                                </tr>
                                <tr class="filagris">
                                    <td >
                                        <p><b>Valor Central</b></p>
                                    </td>
                                    <td>
                                        <p ><%=Util.customFormat(central)%></p>
                                    </td>
                                </tr>
                                <tr class="filaazul">
                                    <td >
                                        <p><b>Capacitaci&oacute;n</b></p>
                                    </td>
                                    <td>
                                        <p ><%=Util.customFormat(capacitacion)%></p>
                                    </td>
                                </tr>
                                <tr class="filagris">
                                    <td >
                                        <p><b>Fecha negocio</b></p>
                                    </td>
                                    <td>
                                        <p ><%=fechanegocio%></p>
                                    </td>
                                </tr>
                                <tr class="filaazul">
                                    <td >
                                        <p><b>Fecha liquidaci&oacute;n</b></p>
                                    </td>
                                    <td>
                                        <p ><%=fechaliqui%></p>
                                    </td>
                                </tr>
                                <tr class="filaazul">
                                    <td >
                                        <p><b>Valor Aval</b></p>
                                    </td>
                                    <td>
                                        <p ><%=Util.customFormat(valorfianza)%></p>
                                    </td>
                                </tr>
                                <tr class="filaazul" >
                                    <td >
                                        <p><b>Porcentaje Descuento Aval</b></p>
                                    </td>
                                    <td>
                                        <p ><%=Util.customFormat(porc_dto_aval)%> %</p>
                                    </td>
                                </tr>   
                                 <tr class="filaazul" >
                                    <td >
                                        <p><b>Valor Descuento Aval</b></p>
                                    </td>
                                    <td>
                                        <p ><%=Util.customFormat(valorfianza*(porc_dto_aval/100))%></p>
                                    </td>
                                </tr>  
                                <tr class="filaazul">
                                    <td >
                                        <p><b>Valor Total Poliza</b></p>
                                    </td>
                                    <td>
                                        <p ><%=Util.customFormat(valor_total_poliza)%></p>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td colspan='2'>
                            <br><br>
                        </td>
                    </tr>
                    <tr>
                        <td width='40%'>
                            <table width="100%" cellpadding='0' cellspacing='0' class="tablaInferior" border="1">
                                <tr class="fila">
                                    <td>
                                        <p align='center' class="anchop"><b>Fecha</b></p>
                                    </td>
                                    <td>
                                        <p align='center' class="anchop"><b>No. Cuota</b></p>
                                    </td>
                                    <td>
                                        <p align='center' class="anchop"><b>Saldo Inicial</b></p>
                                    </td>
                                    <td>
                                        <p align='center' class="anchop"><b>Valor Cuota</b></p>
                                    </td>
                                    <td>
                                        <p align='center' class="anchop"><b>Capital</b></p>
                                    </td>
                                    <td>
                                        <p align='center' class="anchop"><b>Inter&eacute;s</b></p>
                                    </td>
                                    <td>
                                        <p align='center' class="anchop"><b>Capacitaci&oacute;n</b></p>
                                    </td>
                                    <td>
                                        <p align='center' class="anchop"><b>Comisi&oacute;n Ley mipyme</b></p>
                                    </td>
                                    <td>
                                        <p align='center' class="anchop"><b>Seguro</b></p>
                                    </td>
                                    <td>
                                        <p align='center' class="anchop"><b>Intermediación Financiera</b></p>
                                    </td>
                                    <td>
                                        <p align='center' class="anchop"><b>Aval</b></p>
                                    </td>
                                    <td>
                                        <p align='center' class="anchop"><b>Saldo Final</b></p>
                                    </td>
                                    <td>
                                        <p align='center' class="anchop"><b>Capital Aval</b></p>
                                    </td>
                                    <td>
                                        <p align='center' class="anchop"><b>Interes Aval</b></p>
                                    </td>
                                    <td>
                                        <p align='center' class="anchop"><b>Valor Aval</b></p>
                                    </td>
                                    
                                </tr>
                                <% for(int i=0;i<liquidacion.size();i++){
                                        negocio=liquidacion.get(i).getCod_neg();
                                        tcuota+=liquidacion.get(i).getValor();
                                        tcapital += liquidacion.get(i).getCapital();
                                        tcapacitacion += liquidacion.get(i).getCapacitacion();
                                        tseguro += liquidacion.get(i).getSeguro();
                                        tinteres += liquidacion.get(i).getInteres();
                                        tcat +=liquidacion.get(i).getCat();
                                        tcuota_admin+=liquidacion.get(i).getCuota_manejo();
                                        tvalorcuotapoliza+=liquidacion.get(i).getCapital_Poliza();
                                        tvaloraval+=liquidacion.get(i).getValor_aval();
                                        tvalorkaval+=liquidacion.get(i).getCapital_aval();
                                        tvaloriaval+=liquidacion.get(i).getInteres_aval();
                                        taval+=liquidacion.get(i).getAval();
                                %>
                                 <tr class="filaazul">
                                    <td>
                                        <p align='center'><%=liquidacion.get(i).getFecha().substring(0, 10)%></p>
                                    </td>
                                    <td>
                                        <p align='center'><%=liquidacion.get(i).getItem()%></p>
                                    </td>
                                    <td>
                                        <p align='center'><%=Util.customFormat(liquidacion.get(i).getSaldo_inicial())%></p>
                                    </td>
                                    <td>
                                        <p align='center'><%=Util.customFormat(liquidacion.get(i).getValor())%></p>
                                    </td>
                                    <td>
                                        <p align='center'><%=Util.customFormat(liquidacion.get(i).getCapital())%></p>
                                    </td>
                                    <td>
                                        <p align='center'><%=Util.customFormat(liquidacion.get(i).getInteres())%></p>
                                    </td>
                                    <td>
                                        <p align='center'><%=Util.customFormat(liquidacion.get(i).getCapacitacion())%></p>
                                    </td>
                                    <td>
                                        <p align='center'><%=Util.customFormat(liquidacion.get(i).getCat())%></p>
                                    </td>
                                    <td>
                                        <p align='center'><%=Util.customFormat(liquidacion.get(i).getSeguro())%></p>
                                    </td>
                                    <td>
                                        <p align='center'><%=Util.customFormat(liquidacion.get(i).getCuota_manejo())%></p>
                                    </td>
                                    <td>
                                        <p align='center'><%=Util.customFormat(liquidacion.get(i).getAval())%></p>
                                    </td>
                                    <td>
                                        <p align='center'><%=Util.customFormat(liquidacion.get(i).getSaldo_final())%></p>
                                    </td>
                                    <td>
                                        <p align='center'><%=Util.customFormat(liquidacion.get(i).getCapital_aval())%></p>
                                    </td>
                                    <td>
                                        <p align='center'><%=Util.customFormat(liquidacion.get(i).getInteres_aval())%></p>
                                    </td>
                                    <td>
                                        <p align='center'><%=Util.customFormat(liquidacion.get(i).getValor_aval())%></p>
                                    </td>                                    
                                </tr>
                                
                                
                                
                                <%
                                    if (liquidacion.get(i).getItem().equals("1")) {
                                      valor_cuota = liquidacion.get(i).getCapital()+liquidacion.get(i).getInteres();
                                      total_liq= liquidacion.get(i).getSaldo_inicial();
                                    }
                                 }
                                
                                %>
                               
                                <tr >
                                    <td> </td>
                                    <td> </td>
                                    <td> </td>
                                    <td align="center" class="fila style1"><%=Util.customFormat( tcuota)%></td>
                                    <td align="center" class="fila style1"><%=Util.customFormat( tcapital)%></td>
                                    <td align="center" class="fila style1"><%=Util.customFormat( tinteres)%></td>
                                    <td align="center" class="fila style1"><%=Util.customFormat( tcapacitacion)%></td>
                                    <td align="center" class="fila style1"><%=Util.customFormat( tcat)%></td>
                                    <td align="center" class="fila style1"><%=Util.customFormat( tseguro)%></td>
                                    <td align="center" class="fila style1"><%=Util.customFormat( tcuota_admin)%></td>
                                    <td align="center" class="fila style1"><%=Util.customFormat( taval)%></td>
                                    <td> </td>
                                    <td align="center" class="fila style1"><%=Util.customFormat( tvalorkaval)%></td>
                                    <td align="center" class="fila style1"><%=Util.customFormat( tvaloriaval)%></td>
                                    <td align="center" class="fila style1"><%=Util.customFormat( tvaloraval)%></td>                                    
                                    <td> </td>
                                </tr>
                            </table>
                        </td>
                       
                    </tr>
                   
                    <%if(!consulta.equals("S")){
                     tir = model.Negociossvc.obtenerTir(numcuotas,valor_cuota,total_liq,fechaliqui,fechaPrimeraCuota);
                    %>
                    
                    <tr>
                        <td colspan='2'>
                            <p>&nbsp;</p>
                            <p align='center'>
                                <label style="color: red; font-size: 18px;">       Tasa Max Fintra:  </label><label style="color: red; font-size: 18px;"><%=tasa_max_fintra%></label><br>
                                 <label style="color: red; font-size: 18px;">TIR:  </label><label style="color: red; font-size: 18px;"><%=tir%></label>
                                <br>
                                <br> 
                                <img src="<%=BASEURL%>/images/botones/regresar.gif" name="c_salir" onClick="location.href='<%=BASEURL%>/jsp/fenalco/liquidadores/liquidadorNegMicrocredito.jsp';" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" style="cursor:hand ">
                                <img src="<%=BASEURL%>/images/botones/salir.gif"  name="imgsalir"  onMouseOver="botonOver(this);" onClick="parent.close();" onMouseOut="botonOut(this);" style="cursor:hand">

                        </td>
                    </tr>
                    <%}%>
                    
                    <tr>
                        <td colspan="2" align="center"><p>&nbsp;</p>
                            <form id="form2" name="form2" method="post" action="<%=CONTROLLER%>?estado=Negocios&accion=Insert">
                                <input name="vrnegocio" type="hidden" value="<%=valornegocio%>"/>
                                <input name="vrdesem" type="hidden" value="<%=tcapital%>"/>
                                <input name="nodocs" type="hidden" value="<%=numcuotas%>"/>
                                <input name="tneg" type="hidden" value="<%=tneg%>"/>
                                <input name="fechaneg" type="hidden" value="<%=fechanegocio%>"/>
                                <input name="fechaliq" type="hidden" value="<%=fechaliqui%>"/>
                                <input name="tpag" type="hidden" value="<%=tcuota%>"/>
                                <input name="numsolc" id="numsolc" type="hidden" value="<%=numsolc%>"/>
                                <input name="tipoconv" type="hidden" value="Microcredito"/>
                                <input name="convenio" type="hidden" value="<%=convid%>"/>
                                <input name="nit" type="hidden" value="<%=nit%>"/>
                                <input name="tasa" type="hidden" value="<%=tasainteres%>"/>
                                <input name="capacitacion" type="hidden" value="<%=capacitacion%>"/>
                                <input name="central" type="hidden" value="<%=central%>"/>
                                <input name="porc_cat" type="hidden" value="<%=cat%>"/>
                                <input name="formpago" type="hidden" value="<%=fpago%>"/>
                                <input name="seguro" type="hidden" value="<%=tseguro%>"/>
                                <input name="vista" type="hidden" value="<%=vista%>"/>
                                
                                <input name="fechatrans" type="hidden" value="<%=fechatrans%>"/>
                                <input  name="state" type="hidden" value="<%=estadoneg%>"/>
                                <input  name="nomcli"  type="hidden" value="<%=request.getParameter("nomcli")%>"/>
                                <input  name="pagarex" type="hidden" value="<%=request.getParameter("pagarex")%>"/>
                                <input  name="tipo_cuota" type="hidden" value="<%=request.getParameter("tipo_cuota")%>"/>
                                <input name="vrfianza" type="hidden" value="<%=valorfianza%>"/>
                                <input name="vrpoliza" type="hidden" value="<%=valor_total_poliza%>"/>
                                <%if (!consulta.equals("S")&& tir<tasa_max_fintra ) {%>
                                <p><img src="<%=BASEURL%>/images/botones/Aceptar_neg.gif"  name="aceptarneg"  onMouseOver="botonOver(this);" onClick="javascript:form2.submit()" onMouseOut="botonOut(this);" style="cursor:hand"></p>
                               <%}%>

                                <%if (consulta.equals("S") && vista.equals("12")) {%>

                                <p>&nbsp;</p>
                                <p align='center'>
                                    
                                    <img src="<%=BASEURL%>/images/botones/generar_pdf.gif"  name="imggenerar" onclick="generarRecibos('<%=negocio%>');"  onMouseOver="botonOver(this);" onClick="" onMouseOut="botonOut(this);" style="cursor:hand">
                                </p>

                                <%}%>
                            </form>
                        </td>
                    </tr>
                </table><%if(!consulta.equals("S")){%>
            </div>
        </div><%}%>
    </body>
</html>