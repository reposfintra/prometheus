<%--
    Document   : liquidadorNegMicrocredito
    Created on : 27/12/2011, 04:37:55 PM
    Author     : Iris Vargas
--%>

<%@page import="com.tsp.operation.model.services.GestionConveniosService"%>
<%@page import="com.tsp.operation.model.beans.Propietario"%>
<%@page import="com.tsp.operation.model.services.SectorSubsectorService"%>
<%@page import="com.tsp.operation.model.beans.Convenio"%>
<%@page import="com.tsp.operation.model.beans.Negocios"%>
<%@page import="com.tsp.operation.model.beans.SolicitudPersona"%>
<%@page import="com.tsp.operation.model.beans.SolicitudAval"%>
<%@page import="com.tsp.operation.model.services.GestionSolicitudAvalService"%>
<%@page import="com.tsp.operation.model.beans.BeanGeneral"%>
<%@page import="com.tsp.operation.model.beans.Proveedor"%>
<%@page import="com.tsp.operation.model.beans.Usuario"%>
<%@page import="com.tsp.operation.model.beans.TablaGen"%>
<%@page import="com.tsp.operation.model.services.GestionCondicionesService"%>
<%@page contentType="text/html" pageEncoding="ISO-8859-1"%>
<%@page import="com.tsp.util.*"%>
<%@page import="java.util.ArrayList"%>
<%@page import="com.tsp.operation.model.*"%>
<%@include file="/WEB-INF/InitModel.jsp"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">

<%
String trans = request.getParameter("trans") != null ? request.getParameter("trans") : "N";
 String op_liq = request.getParameter("op_liq") == null ? "" : request.getParameter("op_liq");
 String vista = request.getParameter("vista") == null ? "" : request.getParameter("vista");
    %>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
        <script type='text/javascript' src="<%= BASEURL %>/js/boton.js"></script>
        <% if(trans.equals("S")){%>
        <title>Liquidador de negocios Cupo Express</title>
        <%}else{%>
        <title>Liquidador de negocios Microcredito</title>
        <%}%>

        <script src="<%=BASEURL%>/js/prototype.js" type="text/javascript"></script>
        <!-- Las siguientes librerias CSS y JS son para el manejo de DIVS dinamicos.-->
        <link href="<%=BASEURL%>/css/default.css" rel="stylesheet" type="text/css">
        <link href="<%=BASEURL%>/css/alert.css" rel="stylesheet" type="text/css"/>
        <link href="<%=BASEURL%>/css/alphacube.css" rel="stylesheet" type="text/css"/>
        <script src="<%=BASEURL%>/js/effects.js" type="text/javascript"></script>
        <script src="<%=BASEURL%>/js/window.js" type="text/javascript"></script>
        <script src="<%=BASEURL%>/js/liquidadorNegMicrocredito.js" type='text/javascript'></script>
        <script src="<%=BASEURL%>/js/validar.js" type="text/javascript"></script>
    </head>
    <%

        String diaslimit= model.tablaGenService.obtenerTablaGen("DIASLIMIT", "01").getDato();
        String salariomin= model.tablaGenService.obtenerTablaGen("SALARIOMIN", "01").getDato();
        ArrayList tipo_cuota = model.gestionConveniosSvc.busquedaGeneral("TIPO_CUOTA");
        ArrayList convenios= model.gestionConveniosSvc.buscarConveniosTipo("Microcredito",trans.equals("S")?false:true);


            Usuario usuario = (Usuario) session.getAttribute("Usuario");
            String s=(String)request.getParameter("fecha_pc");
            String fecha_pc = (s == null || s.equals("0099-01-01") )? (com.tsp.util.Util.AnoActual() + "-" + com.tsp.util.Util.getFechaActual_String(3) + "-" + com.tsp.util.Util.getFechaActual_String(5)) : request.getParameter("fecha_pc");
            String numero_solicitud = request.getParameter("numero_solicitud") == null ? "" : request.getParameter("numero_solicitud");
            GestionCondicionesService gserv = new GestionCondicionesService(usuario.getBd());
            
            GestionConveniosService convenioserv = new GestionConveniosService(usuario.getBd());

            String msj = (request.getParameter("msj") == null) ? "" : request.getParameter("msj");
            SolicitudAval solicitud = null;
            SolicitudPersona solicitud_persona = null;
            Negocios negocio = null;
            Convenio convenio = null;
            GestionSolicitudAvalService gsaserv = new GestionSolicitudAvalService(usuario.getBd());
            SectorSubsectorService sector = new SectorSubsectorService(usuario.getBd());
            SectorSubsectorService sub_sector = new SectorSubsectorService(usuario.getBd());

            if (!op_liq.equals("")) {

                solicitud = gsaserv.buscarSolicitud(Integer.parseInt(numero_solicitud));

                sector.buscar_sector(solicitud.getSector(), usuario.getBd());
                solicitud_persona = gsaserv.buscarPersona(Integer.parseInt(numero_solicitud), "S");
                sub_sector.buscar_subsector(solicitud.getSector(), solicitud.getSubsector(), usuario.getBd());
                negocio = model.Negociossvc.buscarNegocio(solicitud.getCodNegocio());
                convenio = convenioserv.buscar_convenio(usuario.getBd(), negocio.getId_convenio() + "");
            }


    %>

   <script>
        function cargar()
{
     <%if(!op_liq.equals("")){%>

datosform(
    '<%=solicitud.getNumeroSolicitud()%>', '<%=solicitud_persona.getIdentificacion()%>', '<%=Util.customFormat(   Double.parseDouble(""+( solicitud!=null?solicitud.getValorSolicitado():negocio!=null? negocio.getVr_negocio():""))) %>',
        '<%=convenio.getId_convenio()%>', '<%=convenio.getNombre()%>', '<%=convenio.isFactura_tercero()?"t":"f" %>', '<%=solicitud.getAfiliado()%>',
        '<%=solicitud!=null?solicitud.getPlazo():negocio!=null? negocio.getNodocs():"" %>', '<%=solicitud.getPlazoPrCuota() %>',
        '<%=solicitud.getTipoNegocio()+"_"+convenioserv.valida_remesa(convenio.getId_convenio(), solicitud.getTipoNegocio()) %>', '<%=sector.getCodigo_sector() %>', '<%=sub_sector.getCodigo_sector() %>', '<%=sector.getNombre_sector()%>',
        '',  '');
        <%}%>
}


function replaceAll( text, busca, reemplaza ){
  while (text.toString().indexOf(busca) != -1)
      text = text.toString().replace(busca,reemplaza);
  return text;
}


function carcularFecha(tipo){
    // alert(document.getElementById("fechainicio").value);
    var fecha1="";
      if(tipo=="N"){
            fecha1=document.getElementById("fechainicio").value;
     }else{
         
         fecha1=document.getElementById("fechaCalculadaPC").value;
     }
    
    var aux=replaceAll(fecha1,"-","/");
    var date= new Date(aux);
    var days=date.getDate();
    var fecha="0099-01-01";
    var mes = date.getMonth() + 1;
    var anio = date.getFullYear();
    
      
    if (days >= 1 && days <= 2) {

        if (mes === 12) {

            fecha = (anio + 1) + "-" + "01" + "-02";
            opcion0 = new Option(fecha, fecha, "defauldSelected");
            fecha = (anio + 1) + "-" + "01" + "-12";
            opcion1 = new Option(fecha, fecha);
            fecha = (anio + 1) + "-" + "01" + "-17";
            opcion2 = new Option(fecha, fecha);
            //fecha = (anio + 1) + "-" + "01" + "-22";
            //opcion3 = new Option(fecha, fecha);


        } else {

            fecha = date.getFullYear() + "-" + ((date.getMonth() + 2) > 9 ? (date.getMonth() + 2) : "0" + (date.getMonth() + 2)) + "-02";
            opcion0 = new Option(fecha, fecha, "defauldSelected");
            fecha = date.getFullYear() + "-" + ((date.getMonth() + 2) > 9 ? (date.getMonth() + 2) : "0" + (date.getMonth() + 2)) + "-12";
            opcion1 = new Option(fecha, fecha);
            fecha = date.getFullYear() + "-" + ((date.getMonth() + 2) > 9 ? (date.getMonth() + 2) : "0" + (date.getMonth() + 2)) + "-17";
            opcion2 = new Option(fecha, fecha);
            //fecha = date.getFullYear() + "-" + ((date.getMonth() + 2) > 9 ? (date.getMonth() + 2) : "0" + (date.getMonth() + 2)) + "-22";
            //opcion3 = new Option(fecha, fecha);


        }

    }

    if (days > 2 && days <= 12) {

        if (mes === 12) {

            fecha = (anio + 1) + "-" + "01" + "-12";
            opcion0 = new Option(fecha, fecha, "defauldSelected");
            fecha = (anio + 1) + "-" + "01" + "-17";
            opcion1 = new Option(fecha, fecha);
            //fecha = (anio + 1) + "-" + "01" + "-22";
            //opcion2 = new Option(fecha, fecha);
            fecha = (anio + 1) + "-" + "02" + "-02";
            opcion2 = new Option(fecha, fecha);


        } else {

            fecha = date.getFullYear() + "-" + ((date.getMonth() + 2) > 9 ? (date.getMonth() + 2) : "0" + (date.getMonth() + 2)) + "-12";
            opcion0 = new Option(fecha, fecha, "defauldSelected");
            fecha = date.getFullYear() + "-" + ((date.getMonth() + 2) > 9 ? (date.getMonth() + 2) : "0" + (date.getMonth() + 2)) + "-17";
            opcion1 = new Option(fecha, fecha);
            //fecha = date.getFullYear() + "-" + ((date.getMonth() + 2) > 9 ? (date.getMonth() + 2) : "0" + (date.getMonth() + 2)) + "-22";
            //opcion2 = new Option(fecha, fecha);
             if (mes === 11) {
              fecha = (anio + 1) + "-" + "01" + "-02";
              opcion2 = new Option(fecha, fecha);
             }else{
            fecha = date.getFullYear() + "-" + ((date.getMonth() + 3) > 9 ? (date.getMonth() + 3) : "0" + (date.getMonth() + 3)) + "-02";
            opcion2 = new Option(fecha, fecha);
             }

        }
    }

    if (days > 12 && days <= 17) {

        if (mes === 12) {

            fecha = (anio + 1) + "-" + "01" + "-17";
            opcion0 = new Option(fecha, fecha, "defauldSelected");
            //fecha = (anio + 1) + "-" + "01" + "-22";
            //opcion1 = new Option(fecha, fecha);
            fecha = (anio + 1) + "-" + "02" + "-02";
            opcion1 = new Option(fecha, fecha);
            fecha = (anio + 1) + "-" + "02" + "-12";
            opcion2 = new Option(fecha, fecha);


        } else {
            fecha = date.getFullYear() + "-" + ((date.getMonth() + 2) > 9 ? (date.getMonth() + 2) : "0" + (date.getMonth() + 2)) + "-17";
            opcion0 = new Option(fecha, fecha, "defauldSelected");
            //fecha = date.getFullYear() + "-" + ((date.getMonth() + 2) > 9 ? (date.getMonth() + 2) : "0" + (date.getMonth() + 2)) + "-22";
            //opcion1 = new Option(fecha, fecha);
             
            if (mes === 11) {
                
                fecha = (anio + 1) + "-" + "01" + "-02"; 
                opcion1 = new Option(fecha, fecha);
                fecha = (anio + 1) + "-" + "01" + "-12";
                opcion2 = new Option(fecha, fecha);
                 
             }else{
            
            fecha = date.getFullYear() + "-" + ((date.getMonth() + 3) > 9 ? (date.getMonth() + 3) : "0" + (date.getMonth() + 3)) + "-02";
            opcion1 = new Option(fecha, fecha);
            fecha = date.getFullYear() + "-" + ((date.getMonth() + 3) > 9 ? (date.getMonth() + 3) : "0" + (date.getMonth() + 3)) + "-12";
            opcion2 = new Option(fecha, fecha);
            }
        }

    }

    if (days > 17 && days <= 22) {

        if (mes === 12) {

            //fecha = (anio + 1) + "-" + "01" + "-22";
            //opcion0 = new Option(fecha, fecha, "defauldSelected");
            fecha = (anio + 1) + "-" + "02" + "-02";
            opcion0 = new Option(fecha, fecha);
            fecha = (anio + 1) + "-" + "02" + "-12";
            opcion1 = new Option(fecha, fecha);
            fecha = (anio + 1) + "-" + "02" + "-17";
            opcion2 = new Option(fecha, fecha);


        } else {

            //fecha = date.getFullYear() + "-" + ((date.getMonth() + 2) > 9 ? (date.getMonth() + 2) : "0" + (date.getMonth() + 2)) + "-22";
            //opcion0 = new Option(fecha, fecha, "defauldSelected");
            
            if (mes === 11) {
                
                fecha = (anio + 1) + "-" + "01" + "-02";
                opcion0 = new Option(fecha, fecha);
                fecha = (anio + 1) + "-" + "01" + "-12";
                opcion1 = new Option(fecha, fecha);
                fecha = (anio + 1) + "-" + "01" + "-17";
                opcion2 = new Option(fecha, fecha);
                
            }else{
                
            fecha = date.getFullYear() + "-" + ((date.getMonth() + 3) > 9 ? (date.getMonth() + 3) : "0" + (date.getMonth() + 3)) + "-02";
            opcion0 = new Option(fecha, fecha);
            fecha = date.getFullYear() + "-" + ((date.getMonth() + 3) > 9 ? (date.getMonth() + 3) : "0" + (date.getMonth() + 3)) + "-12";
            opcion1 = new Option(fecha, fecha);
            fecha = date.getFullYear() + "-" + ((date.getMonth() + 3) > 9 ? (date.getMonth() + 3) : "0" + (date.getMonth() + 3)) + "-17";
            opcion2 = new Option(fecha, fecha);
            }
        }

    }


    if (days > 22 && days <= 31) {

        if (mes === 12) {

            fecha = (anio + 1) + "-" + "02" + "-02";
            opcion0 = new Option(fecha, fecha, "defauldSelected");
            fecha = (anio + 1) + "-" + "02" + "-12";
            opcion1 = new Option(fecha, fecha);
            fecha = (anio + 1) + "-" + "02" + "-17";
            opcion2 = new Option(fecha, fecha);
            //fecha = (anio + 1) + "-" + "02" + "-22";
            //opcion3 = new Option(fecha, fecha);


        } else {
            
            if(mes === 11){
                
                fecha = (anio + 1) + "-" + "01" + "-02";
                opcion0 = new Option(fecha, fecha, "defauldSelected");
                fecha = (anio + 1) + "-" + "01" + "-12";
                opcion1 = new Option(fecha, fecha);
                fecha = (anio + 1) + "-" + "01" + "-17";
                opcion2 = new Option(fecha, fecha);
                //fecha = (anio + 1) + "-" + "01" + "-22";
                //opcion3 = new Option(fecha, fecha);

                                
            }else {

            fecha = date.getFullYear() + "-" + ((date.getMonth() + 3) > 9 ? (date.getMonth() + 3) : "0" + (date.getMonth() + 3)) + "-02";
            opcion0 = new Option(fecha, fecha, "defauldSelected");
            fecha = date.getFullYear() + "-" + ((date.getMonth() + 3) > 9 ? (date.getMonth() + 3) : "0" + (date.getMonth() + 3)) + "-12";
            opcion1 = new Option(fecha, fecha);
            fecha = date.getFullYear() + "-" + ((date.getMonth() + 3) > 9 ? (date.getMonth() + 3) : "0" + (date.getMonth() + 3)) + "-17";
            opcion2 = new Option(fecha, fecha);
            //fecha = date.getFullYear() + "-" + ((date.getMonth() + 3) > 9 ? (date.getMonth() + 3) : "0" + (date.getMonth() + 3)) + "-22";
            //opcion3 = new Option(fecha, fecha);
            
            }

        }
    }
    
    document.formulario.primeracuota.options[0]=opcion0;
    document.formulario.primeracuota.options[1]=opcion1;
    document.formulario.primeracuota.options[2]=opcion2; 
    //document.formulario.primeracuota.options[3]=opcion3; 
   
    
}

    </script>


        <body onload="inicializar('<%=BASEURL%>','<%=CONTROLLER%>'); <% if(s == null || s.equals("9999-99-99")) {%> carcularFecha('N') <%} else {%> carcularFecha('S') <%}%> ; <%if (!op_liq.equals("")) {%>  cargar()<%}%>  ">
        <div id="capaSuperior" style="position:absolute; width:100%; height:100px; z-index:0; left: 0px; top: 0px;">
            <% if (trans.equals("S")) {%>
            <jsp:include page="/toptsp.jsp?encabezado=LIQUIDADOR DE NEGOCIOS CUPO EXPRESS"/>
            <%} else {%>
            <jsp:include page="/toptsp.jsp?encabezado=LIQUIDADOR DE NEGOCIOS MICROCREDITO"/>

            <%}%>
        </div>
        <div id="capaCentral" style="position:absolute; width:100%; height:90%; z-index:0; left: 0px; top: 100px;">
            <div id="contenido" style="visibility: hidden; display: none; height: 100px; width: 400px; background-color: #EEEEEE;"></div>
            <div id="contenido2" style="visibility: hidden; display: none; height: 200px; width: 500px; background-color: #EEEEEE;"></div>
            <form action='<%=CONTROLLER%>?estado=Liquidador&accion=NegMicrocredito&opcion=calcularLiquidacion' name='formulario' id='formulario' method="post" >
                <table width="600" height="167" border="2"align="center">
                    <tr>
                        <td width="600" height="159">
                            <table width="100%" height="72%" class="tablaInferior">
                                <tr class="fila">
                                    <td align="left" nowrap class="subtitulo1">LIQUIDADOR</td>
                                    <td align="left" nowrap class="barratitulo"><img src="<%=BASEURL%>/images/titulo.gif" class="bordereporte">
                                    <input type="hidden" id="diaslimit" name="diaslimit"value="<%=diaslimit%>">
                                    <input type="hidden" id="salariomin" name="salariomin"value="<%=salariomin%>">
                                    <input type="hidden" id="trans" name="trans"value="<%=trans%>">
                                    <input type="hidden" id="fechaCalculadaPC" name="fechaCalculadaPC" value="<%=fecha_pc%>">
                                    </td>
                                </tr>
                                <tr class="fila">
                                <td align="left" width="30%" class="fila">Formulario</td>
                                <td align="left" width="70%" class="fila">
                                    <input type="text" id="numsolc" name="numsolc" readonly value="">
                                    <img alt="buscar" src="<%=BASEURL%>/images/botones/iconos/lupa.gif" width="15" height="15" style="cursor:pointer;" onclick="verForms('<%=trans%>');">
                                    <span id="spangen"></span>
                                </td>
                                </tr>
                                 <tr>
                                    <td align="left" width="30%" class="fila">Convenio</td>
                                    <td align="left" width="70%" class="fila">

                                        <select name="convid" id="convid" style="width: 100%;" onchange="datosConvenio()"  <%=solicitud!=null ? " disabled" :""%> >
                                        <option  value="">...</option>
                                        <%
                                            if (convenios != null) {
                                                String[] split = null;
                                                for (int i = 0; i < convenios.size(); i++) {
                                                    split = ((String) (convenios.get(i))).split(";_;");
                                                    out.print("<option value='" + split[0] + "'>" + split[1] + "</option>");
                                                }
                                            }
                                        %>
                                    </select>
                                        <input type="hidden" id="tasaconv" name="tasaconv"value="">
                                        <input type="hidden" id="montomin" name="montomin"value="">
                                        <input type="hidden" id="montomax" name="montomax"value="">
                                        <input type="hidden" id="plazomax" name="plazomax"value="">
                                        <input type="hidden" id="renovacion" name="renovacion"value="">
                                        <input type="hidden" id="fianza" name="fianza"value="">
                                        <input type="hidden" id="pre_aprobado" name="pre_aprobado"value="">
                                    </td>
                                </tr>

                                <tr class="fila">
                                    <td > Cliente </td>
                                    <td class="fila"><input name="nit" type="text" maxlength="12" id="nit" readonly ></td>
                                </tr>


                               <tr class="fila">
                                <td>&nbsp;Tipo cuota</td><!-- mod -->
                                        <td  valign="middle">
                                            <select id="tipo_cuota" name="tipo_cuota" style="width: 150px;"  <%=solicitud!=null?"disabled":""%> >
                                                <option value="">...</option>
                                                <% String [] dato1 = null;
                                                     for (int j = 0; j < tipo_cuota.size(); j++) {
                                                         dato1 = ((String) tipo_cuota.get(j)).split(";_;");%>
                                                         <option value="<%=dato1[0]%>"   <%=(solicitud!=null && negocio.getTipo_cuota().equals(dato1[0]))?"selected":"" %> ><%=dato1[1]%></option>
                                                <%  }%>
                                            </select>                                        </td>
                                 </tr>
                                <tr class="fila">
                                    <td > Nro de Cuotas</td>
                                    <td width="49%" valign="middle"><input name="txtNumCuotas" type="text" class="textbox" id="txtNumCuotas" onkeyup="soloNumeros(this.id);" value="<%=negocio!=null? negocio.getNodocs():solicitud!=null?solicitud.getPlazo():"" %>" maxlength="2"></td>
                                </tr>
                                <tr class="fila">
                                    <td >Valor a financiar</td>
                                    <td valign="middle">
                                        <span class="style3">
                                            <input name="txtValor" type="text" class="textbox" id="txtValor" onkeyup="soloNumeros(this.id);" value=''  maxlength="20" <%= vista.equals("1") ?"disabled":""%>>
                                        </span>
                                    </td>
                                </tr>
                                <tr class="fila">
                                    <td >Fecha del Negocio </td>
                                    <td valign="middle">
                                        <input name='fechainicio' type='text' class="textbox" id="fechainicio" style='width:120' value='<%=com.tsp.util.Util.AnoActual() + "-" + com.tsp.util.Util.getFechaActual_String(3) + "-" + com.tsp.util.Util.getFechaActual_String(5)%>' >
                                    </td>
                                </tr>
                                <tr class="fila">
                                    <td >Fecha Primera Cuota </td>

                                    <td>
                                        <select name="primeracuota" id="primeracuota">

                                        </select>

                                    </td>
<!--
                                    <td valign="middle">
                                        <input name='primeracuota' type='text' class="textbox" id="primeracuota" style='width:120' value='<%=fecha_pc%>' readonly>
                                        <a href="javascript:void(0);" onFocus="if(self.gfPop)gfPop.fPopCalendar(document.formulario.primeracuota);return false;" hidefocus> <img src="<%=BASEURL%>/js/Calendario/cal.gif" width="16" height="16" border="0" alt="De click aqu&iacute; para ver el calendario."></a>
                                    </td>-->
                                
                                </tr>
                                <tr id="trTituloValor" class="fila">
                                    <td>Tipo de t&iacute;tulo valor</td>
                                    <td class="fila">
                                        <select name="cmbTituloValor" id="cmbTituloValor" class="listmenu" >
                                        </select>
                                    </td>
                                </tr>

                            </table>
                        </td>
                    </tr>
                </table>
                <br>
                <div align="center">
                    <img src="<%=BASEURL%>/images/botones/aceptar.gif"  name="imgaceptar" onClick="enviarFormLiquidador();" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" style="cursor:hand">
                    <img src="<%=BASEURL%>/images/botones/salir.gif"  name="imgsalir"  onMouseOver="botonOver(this);" onClick="parent.close();" onMouseOut="botonOut(this);" style="cursor:hand">
                </div>
            </form>
        </div>
        <iframe width=188 height=166 name="gToday:normal:agenda.js:gfPop:plugins.js" id="gToday:normal:agenda.js:gfPop:plugins.js" src="<%=BASEURL%>/js/Calendario/ipopeng.htm" scrolling="no" frameborder="0" style="visibility:visible; z-index:999; position:absolute; left:-500px; top:0px;"> </iframe>
    </body>
</html>
