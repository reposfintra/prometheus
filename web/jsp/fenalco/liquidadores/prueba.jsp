
<!--
- Autor : Ing. Roberto Rocha	
- Date  : 16 de Julio de 2007
- Copyrigth Notice : Fintravalores S.A. S.A
-->
<%--
-@(#)
--Descripcion : Pagina JSP, que maneja la busqueda de las condicones de los negocios.
--%>

<%-- Declaracion de librerias--%>
<%@page contentType="text/html"%>
<%@ page session="true"%>
<%@ page errorPage="../error/error.jsp"%>
<%@page import="com.tsp.operation.model.beans.*, java.util.*"%>
<%@ include file="/WEB-INF/InitModel.jsp"%>
<%@page import="com.tsp.operation.model.*"%>
<%@page import="com.tsp.util.*"%>
<%@ taglib uri="http://jakarta.apache.org/taglibs/input-0.90" prefix="input" %>
<html>
<head><title>Negocios Aceptados</title>
<%String path = application.getRealPath(model.menuService.getRealUrl(request.getRequestURI()));
  String datos[] = model.menuService.getContenidoMenu(BASEURL,request.getRequestURI(),path);
  System.out.println("remesas-----------> ");
%>

<link href="<%= BASEURL %>/css/estilostsp.css" rel="stylesheet" type="text/css">
<script type='text/javascript' src="<%= BASEURL %>/js/boton.js"></script>
<script type='text/javascript' src="<%= BASEURL %>/js/general.js"></script>
</head>
<body>
<div id="capaSuperior" style="position:absolute; width:100%; height:100px; z-index:0; left: 0px; top: 0px;">
<jsp:include page="/WEB-INF/toptsp.jsp?encabezado=Mensaje Confirmacion"/>
</div>

<div id="capaCentral" style="position:absolute; width:100%; height:85%; z-index:0; left: 0px; top: 100px; overflow: scroll;"> 

<% String Retorno= request.getParameter("mensaje");
			if( Retorno != null ){%>  
					  <table border="2" align="center">
					  <tr>
						<td><table width="100%"  border="1" align="center"  bordercolor="#F7F5F4" bgcolor="#FFFFFF">
						  <tr>
							<td width="229" align="center" class="mensajes"><%=Retorno%></td>
							<td width="29" background="<%=BASEURL%>/images/cuadronaranja.JPG">&nbsp;</td>
							<td width="58">&nbsp;</td>
						  </tr>
						</table></td>
					  </tr>
					</table>
			<%}else{%>
				 <table border="2" align="center">
					  <tr>
						<td><table width="100%"  border="1" align="center"  bordercolor="#F7F5F4" bgcolor="#FFFFFF">
						  <tr>
							<td width="229" align="center" class="mensajes">Negocio Continua Proceso </td>
							<td width="29" background="<%=BASEURL%>/images/cuadronaranja.JPG">&nbsp;</td>
							<td width="58">&nbsp;</td>
						  </tr>
						</table></td>
					  </tr>
  </table>
			     <p>&nbsp;
  </p>
			     <p align="center">
		             <%}%>
                   <!--<img src="<%//=BASEURL%>/images/botones/regresar.gif" name="c_salir" onClick="location.href='<%//=BASEURL%>/jsp/fenalco/liquidadores/inicio_liquidador.jsp';" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" align="absmiddle" style="cursor:hand ">-->
				    <img src="<%=BASEURL%>/images/botones/salir.gif"  name="imgsalir"  onMouseOver="botonOver(this);" onClick="parent.close();" onMouseOut="botonOut(this);" style="cursor:hand">
                 </p>
</div>
<%=datos[1]%>
</body>
</html>
