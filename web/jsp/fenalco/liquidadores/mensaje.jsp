<%@page contentType="text/html"%>
<%@ page session="true"%>
<%@ page errorPage="../error/error.jsp"%>
<%@page import="com.tsp.operation.model.beans.*, java.util.*"%>
<%@ include file="/WEB-INF/InitModel.jsp"%>
<%@page import="com.tsp.operation.model.*"%>
<%@page import="com.tsp.util.*"%>
<%@ taglib uri="http://jakarta.apache.org/taglibs/input-0.90" prefix="input" %>

<!DOCTYPE html>
<html>
<head><title>Negocios</title>


<link href="<%= BASEURL %>/css/estilostsp.css" rel="stylesheet" type="text/css">
        <link href="<%= BASEURL%>/css/jquery/jquery-ui/jquery-ui.css" rel="stylesheet" type="text/css"/>
        <link href="<%= BASEURL%>/css/jquery/jquery-ui/jquery.ui.selectmenu.css" rel="stylesheet" type="text/css"/>
        <script type='text/javascript' src="<%= BASEURL%>/js/jquery/jquery-ui/jquery.min.js"></script>
        <script type='text/javascript' src="<%= BASEURL%>/js/jquery/jquery-ui/jquery.ui.min.js"></script>
        <script type='text/javascript' src="<%= BASEURL%>/js/jquery/jquery.jqGrid/js/grid.locale-es.js"></script>
        <script type='text/javascript' src="<%= BASEURL%>/js/jquery/jquery.jqGrid/js/jquery.jqGrid.min.js"></script>
        <script type='text/javascript' src="<%= BASEURL%>/js/jquery/jquery-ui/jquery.ui.selectmenu.js"></script>
     

       
         <link href="<%= BASEURL%>/css/estilosFintra.css" rel='stylesheet'/>
        
        <script>
		
                $(document).ready(
                function() {
                    $("#tabla_info").styleTable();
                    $("input[type='button']").button();
                     $("input[type='submit']").button();
                   
                });			
			
			
(function ($) {
    $.fn.styleTable = function (options) {
        var defaults = {
            css: 'styleTable'
        };
        options = $.extend(defaults, options);

        return this.each(function () {

            input = $(this);
            input.addClass(options.css);

                       
            input.find("th").addClass("ui-state-default");
            input.find("td").addClass("ui-widget-content");

            input.find("tr").each(function () {
                $(this).children("td:not(:first)").addClass("first");
                $(this).children("th:not(:first)").addClass("first");
            });
        });
    };
})(jQuery);				
		
		
		</script>


<style>
body{
    font-family: 'Lucida Grande','Lucida Sans',Arial,sans-serif;
    font-size: 0.7em;
}
</style>
</head>
<body>
<div id="capaSuperior" style="position:absolute; width:100%; height:100px; z-index:0; left: 0px; top: 0px;">
<jsp:include page="/WEB-INF/toptsp.jsp?encabezado=Mensaje Confirmacion"/>
</div>

<div id="capaCentral" style="position:absolute; width:100%; height:85%; z-index:0; left: 0px; top: 100px; overflow: scroll;"> 

<% 

String Retorno= request.getParameter("mensaje");
String monto_sujerido= request.getParameter("monto_sujerido");
String plazo= request.getParameter("plazo");
String cod_neg= request.getParameter("cod_neg");
String tipo_liq = request.getParameter("tipo_liq")!=null?"MASTER":"MASTER";
%>
<form action='<%=CONTROLLER%>?estado=Liquidador&accion=Negocios&opcion=calcularReliquidacion&custodia=0&opcion=2&tipo_liq=<%=tipo_liq%>&cod_neg=<%=cod_neg%>&monto_sujerido=<%=monto_sujerido%>' name='formulario' method="post" >
<table width="30%" align="center" id="tabla_info" >
  <tr>
    <th height="55" colspan="2"><p>Negocio Rechazado Por Maximo Limite De Endeudamiento.</p>
      <p>&iquest;Desea Realizar La Operacion Con Las Siguientes Condiciones?</p></th>
    </tr>
  <tr>
    <th>Monto Sugerido</th>
    <td><%= Util.customFormat(Double.parseDouble(monto_sujerido)) %></td>
  </tr>
  <tr>
    <th width="125">plazo</th>
    <td width="285"><%=plazo%>>
  </tr>

</table>
<br/>
<div align="center">
<input name="Aceptar" type="submit" id="Aceptar" value="Aceptar">

  <input name="Salir" type="button" id="Salir" value="Salir" onClick="parent.close();">

</div>
</form>
		
</div>
</body>
</html>
