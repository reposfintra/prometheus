<%--
    Document   : liquidadorNegocios
    Created on : 4/09/2010, 11:37:55 AM
    Author     : geotech
--%>
<%@page session="true" %>
<%@page import="com.tsp.operation.model.beans.Propietario"%>
<%@page import="com.tsp.operation.model.beans.SolicitudPersona"%>
<%@page import="java.lang.String"%>
<%@page import="com.tsp.operation.model.services.SectorSubsectorService"%>
<%@page import="com.tsp.operation.model.services.GestionConveniosService"%>
<%@page import="com.tsp.operation.controller.neg"%>
<%@page import="com.tsp.operation.model.beans.Convenio"%>
<%@page import="com.tsp.operation.model.beans.Negocios"%>
<%@page import="com.tsp.operation.model.services.GestionSolicitudAvalService"%>
<%@page import="com.tsp.operation.model.beans.SolicitudAval"%>
<%@page import="com.tsp.operation.model.beans.BeanGeneral"%>
<%@page import="com.tsp.operation.model.beans.Proveedor"%>
<%@page import="com.tsp.operation.model.beans.Usuario"%>
<%@page import="com.tsp.operation.model.beans.TablaGen"%>
<%@page import="com.tsp.operation.model.services.GestionCondicionesService"%>
<%@page contentType="text/html" pageEncoding="ISO-8859-1"%>
<%@page import="com.tsp.util.*"%>
<%@page import="java.util.ArrayList"%>
<%@page import="com.tsp.operation.model.*"%>
<%@include file="/WEB-INF/InitModel.jsp"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">

<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
        <script type='text/javascript' src="<%= BASEURL%>/js/boton.js"></script>
        <title>Liquidador de negocios</title>
        <script type='text/javascript' src="<%= BASEURL%>/js/jquery/jquery-ui/jquery.min.js"></script>
        <script src="<%=BASEURL%>/js/prototype.js" type="text/javascript"></script>
        <!-- Las siguientes librerias CSS y JS son para el manejo de DIVS dinamicos.-->
        <link href="<%=BASEURL%>/css/default.css" rel="stylesheet" type="text/css">
        <link href="<%=BASEURL%>/css/alert.css" rel="stylesheet" type="text/css"/>
        <link href="<%=BASEURL%>/css/alphacube.css" rel="stylesheet" type="text/css"/>
        <script src="<%=BASEURL%>/js/effects.js" type="text/javascript"></script>
        <script src="<%=BASEURL%>/js/window.js" type="text/javascript"></script>
        <script src="<%=BASEURL%>/js/liquidadorNegocios.js" type='text/javascript'></script>
        <script src="<%=BASEURL%>/js/validar.js" type="text/javascript"></script>
        <script type='text/javascript'>

            function buscardatos() {
                var texto = document.getElementById("nafil").value;
                var selected = document.getElementById("filtro").selectedIndex;
                var filtro = document.getElementById("filtro").options[selected].value;
                var url = "<%= CONTROLLER%>?estado=Gestion&accion=Condiciones";
                var p = 'opcion=buscacli&texto=' + texto + '&filtro=' + filtro;
                new Ajax.Request(
                        url,
                        {
                            method: 'post',
                            parameters: p,
                            onLoading: loading,
                            onComplete: llenarDiv
                        }
                );
            }

            function loading() {
                document.getElementById("spangen").innerHTML = '<img alt="cargando" src="<%= BASEURL%>/images/cargando.gif" name="imgload">';
            }

            function cargarTitulosValor(convenio, titulo) {
                var url = "<%=CONTROLLER%>?estado=Liquidador&accion=Negocios";
                var p = 'opcion=buscarTitVlrConv&idConvenio=' + convenio;
                new Ajax.Request(
                        url,
                        {
                            method: 'post',
                            parameters: p,
                            onLoading: loading,
                            onSuccess: function(transport) {
                                $("cmbTituloValor").update(transport.responseText);
                                document.getElementById("spangen").innerHTML = "";

                                if (titulo != null) {
                                    document.getElementById("cmbTituloValor").value = titulo;
                                    mostrarCamposRemesa();
                                }

                            },
                            onFailure: errorEnvio
                        }
                );
            }

            function cargarCiclos(nitTercero, convenio) {
                var url = "<%=CONTROLLER%>?estado=Liquidador&accion=Negocios";
                var p = 'opcion=buscarCiclosTercero&nitTercero=' + nitTercero;
                new Ajax.Request(
                        url,
                        {
                            method: 'post',
                            parameters: p,
                            onLoading: loading,
                            onSuccess: function(transport) {
                                $("cmbCiclo").update(transport.responseText);
                                document.getElementById("spangen").innerHTML = "";
                                cargarTitulosValor(convenio);
                            },
                            onFailure: errorEnvio
                        }
                );
            }

            function replaceAll(text, busca, reemplaza) {
                while (text.toString().indexOf(busca) != - 1)
                    text = text.toString().replace(busca, reemplaza);
                return text;
            }


            function carcularFecha(fecha1) {

                var sumarDias = parseInt(document.getElementById("forma_pago").value);
                var aux = replaceAll(fecha1, "-", "/");
                // var date= new Date(aux);
                var date = new Date(aux);
                date.setDate(date.getDate() + sumarDias);
                var days = date.getDate();
                var fecha = "0099-01-01";
                var mes = date.getMonth() + 1;
                var anio = date.getFullYear();

                if (days >= 1 && days <= 2) {
                    
                    fecha = date.getFullYear() + "-" + ((date.getMonth() + 1) > 9 ? (date.getMonth() + 1) : "0" + (date.getMonth() + 1)) + "-02";
                    opcion0 = new Option(fecha, fecha, "defauldSelected");
                    fecha = date.getFullYear() + "-" + ((date.getMonth() + 1) > 9 ? (date.getMonth() + 1) : "0" + (date.getMonth() + 1)) + "-12";
                    opcion1 = new Option(fecha, fecha);
                    fecha = date.getFullYear() + "-" + ((date.getMonth() + 1) > 9 ? (date.getMonth() + 1) : "0" + (date.getMonth() + 1)) + "-17";
                    opcion2 = new Option(fecha, fecha);
                    fecha = date.getFullYear() + "-" + ((date.getMonth() + 1) > 9 ? (date.getMonth() + 1) : "0" + (date.getMonth() + 1)) + "-22";
                    opcion3 = new Option(fecha, fecha);
          
                 }
              
                


                if (days > 2 && days <= 12) {

                    fecha = date.getFullYear() + "-" + ((date.getMonth() + 1) > 9 ? (date.getMonth() + 1) : "0" + (date.getMonth() + 1)) + "-12";
                    opcion0 = new Option(fecha, fecha, "defauldSelected");
                    fecha = date.getFullYear() + "-" + ((date.getMonth() + 1) > 9 ? (date.getMonth() + 1) : "0" + (date.getMonth() + 1)) + "-17";
                    opcion1 = new Option(fecha, fecha);
                    fecha = date.getFullYear() + "-" + ((date.getMonth() + 1) > 9 ? (date.getMonth() + 1) : "0" + (date.getMonth() + 1)) + "-22";
                    opcion2 = new Option(fecha, fecha);
                    
                    if(mes === 12){
                        
                        fecha = (anio +1) + "-" +"01" + "-02";
                        opcion3 = new Option(fecha, fecha);
                        
                    }else{
                        
                    fecha = date.getFullYear() + "-" + ((date.getMonth() + 2) > 9 ? (date.getMonth() + 2) : "0" + (date.getMonth() + 2)) + "-02";
                    opcion3 = new Option(fecha, fecha);
                   
                    }
           
                }

                if (days > 12 && days <= 17) {

                    fecha = date.getFullYear() + "-" + ((date.getMonth() + 1) > 9 ? (date.getMonth() + 1) : "0" + (date.getMonth() + 1)) + "-17";
                    opcion0 = new Option(fecha, fecha, "defauldSelected");
                    fecha = date.getFullYear() + "-" + ((date.getMonth() + 1) > 9 ? (date.getMonth() + 1) : "0" + (date.getMonth() + 1)) + "-22";
                    opcion1 = new Option(fecha, fecha);
                    
                     if(mes === 12){
                        
                        fecha = (anio +1) + "-" +"01" + "-02";
                        opcion2 = new Option(fecha, fecha);
                        fecha = (anio +1) + "-" +"01" + "-12";
                        opcion3 = new Option(fecha, fecha);
                        
                    }else{
                    
                    fecha = date.getFullYear() + "-" + ((date.getMonth() + 2) > 9 ? (date.getMonth() + 2) : "0" + (date.getMonth() + 2)) + "-02";
                    opcion2 = new Option(fecha, fecha);
                    fecha = date.getFullYear() + "-" + ((date.getMonth() + 2) > 9 ? (date.getMonth() + 2) : "0" + (date.getMonth() + 2)) + "-12";
                    opcion3 = new Option(fecha, fecha);
                    
                    }

                }

                if (days > 17 && days <= 22) {

                    fecha = date.getFullYear() + "-" + ((date.getMonth() + 1) > 9 ? (date.getMonth() + 1) : "0" + (date.getMonth() + 1)) + "-22";
                    opcion0 = new Option(fecha, fecha, "defauldSelected");
                    
                    if(mes === 12){
                        
                    fecha = (anio +1) + "-" + "01" + "-02";
                    opcion1 = new Option(fecha, fecha);
                    fecha = (anio +1) + "-" +  "01" + "-12";
                    opcion2 = new Option(fecha, fecha);
                    fecha = (anio +1) + "-" +  "01" + "-17";
                    opcion3 = new Option(fecha, fecha);
                    
                    }else{
                    
                    fecha = date.getFullYear() + "-" + ((date.getMonth() + 2) > 9 ? (date.getMonth() + 2) : "0" + (date.getMonth() + 2)) + "-02";
                    opcion1 = new Option(fecha, fecha);
                    fecha = date.getFullYear() + "-" + ((date.getMonth() + 2) > 9 ? (date.getMonth() + 2) : "0" + (date.getMonth() + 2)) + "-12";
                    opcion2 = new Option(fecha, fecha);
                    fecha = date.getFullYear() + "-" + ((date.getMonth() + 2) > 9 ? (date.getMonth() + 2) : "0" + (date.getMonth() + 2)) + "-17";
                    opcion3 = new Option(fecha, fecha);
                    
                    }



                }


                if (days > 22 && days <= 31) {
                    
                    
                   if(mes === 12){
                       
                    fecha = (anio +1) + "-" + "01" + "-02";
                    opcion0 = new Option(fecha, fecha, "defauldSelected");
                    fecha = (anio +1) + "-" + "01"  + "-12";
                    opcion1 = new Option(fecha, fecha);
                    fecha = (anio +1) + "-" + "01"  + "-17";
                    opcion2 = new Option(fecha, fecha);
                    fecha = (anio +1) + "-" + "01"  + "-22";
                    opcion3 = new Option(fecha, fecha);
                           
                           
                   }else{

                    fecha = date.getFullYear() + "-" + ((date.getMonth() + 2) > 9 ? (date.getMonth() + 2) : "0" + (date.getMonth() + 2)) + "-02";
                    opcion0 = new Option(fecha, fecha, "defauldSelected");
                    fecha = date.getFullYear() + "-" + ((date.getMonth() + 2) > 9 ? (date.getMonth() + 2) : "0" + (date.getMonth() + 2)) + "-12";
                    opcion1 = new Option(fecha, fecha);
                    fecha = date.getFullYear() + "-" + ((date.getMonth() + 2) > 9 ? (date.getMonth() + 2) : "0" + (date.getMonth() + 2)) + "-17";
                    opcion2 = new Option(fecha, fecha);
                    fecha = date.getFullYear() + "-" + ((date.getMonth() + 2) > 9 ? (date.getMonth() + 2) : "0" + (date.getMonth() + 2)) + "-22";
                    opcion3 = new Option(fecha, fecha);

                   }
       
                }


                document.formulario.fechainicio.options[0] = opcion0;
                document.formulario.fechainicio.options[1] = opcion1;
                document.formulario.fechainicio.options[2] = opcion2;
                document.formulario.fechainicio.options[3] = opcion3;


            }
            
            function obtenerFechaUltimoPago(identificacion) {
                var url = "<%=CONTROLLER%>?estado=Liquidador&accion=Negocios";
                var p = 'opcion=obtenerFechaPago&identificacion=' + identificacion;
                new Ajax.Request(
                        url,
                        {
                            method: 'post',
                            parameters: p,
                            asynchronous: false,
                            onLoading: loading,
                            onSuccess: function(transport) {                              
                               document.getElementById('fechaPrimeraCuota').value = transport.responseText;
                            },
                            onFailure: errorEnvio
                        }
                );
            }

        </script>
    </head>
    <%      
        String fechaNegocio = (com.tsp.util.Util.AnoActual() + "-" + com.tsp.util.Util.getFechaActual_String(3) + "-" + com.tsp.util.Util.getFechaActual_String(5));
        String tipo_liq = request.getParameter("tipo_liq") == null ? "" : request.getParameter("tipo_liq");
        String act_liq = request.getParameter("act_liq") == null ? "" : request.getParameter("act_liq");
        String numero_solicitud = request.getParameter("numero_solicitud") == null ? "" : request.getParameter("numero_solicitud");       
        Usuario usuario = (Usuario) session.getAttribute("Usuario");
        GestionCondicionesService gserv = new GestionCondicionesService(usuario.getBd());
        GestionConveniosService convenioserv = new GestionConveniosService(usuario.getBd());

        String msj = (request.getParameter("msj") == null) ? "" : request.getParameter("msj");
        SolicitudAval solicitud = null;
        SolicitudPersona solicitud_persona = null;
        Negocios negocio = null;
        Convenio convenio = null;
        GestionSolicitudAvalService gsaserv = new GestionSolicitudAvalService(usuario.getBd());
        SectorSubsectorService sector = new SectorSubsectorService();
        SectorSubsectorService sub_sector = new SectorSubsectorService();
        Propietario Propietario = null;

        if (!act_liq.equals("")) {

            solicitud = gsaserv.buscarSolicitud(Integer.parseInt(numero_solicitud));

            sector.buscar_sector(solicitud.getSector(), usuario.getBd());
            solicitud_persona = gsaserv.buscarPersona(Integer.parseInt(numero_solicitud), "S");
            sub_sector.buscar_subsector(solicitud.getSector(), solicitud.getSubsector(), usuario.getBd());
            negocio = model.Negociossvc.buscarNegocio(solicitud.getCodNegocio());

            convenio = convenioserv.buscar_convenio(usuario.getBd(), solicitud.getIdConvenio() + "");


            Propietario = model.gestionConveniosSvc.buscarProvConvenio(solicitud.getAfiliado(), solicitud.getIdConvenio(), sector.getCodigo_sector(), sub_sector.getCodigo_sector());
        }

        ArrayList<Proveedor> listaAfiliados = null;

        if (!tipo_liq.equals("MASTER")) {
            if (!act_liq.equals("")) {
                listaAfiliados = gserv.buscarProv("", "nit");
            } else {
                listaAfiliados = gserv.buscarAfiliadosUsuario(usuario.getLogin());
            }
        }


    %>
    <script type='text/javascript'>

        function cargar()
        {
        <%if (!act_liq.equals("")) {%>
            datosform(
                    '<%=solicitud.getNumeroSolicitud()%>', '<%=solicitud_persona.getIdentificacion()%>', '<%=Util.customFormat(Double.parseDouble("" + (solicitud != null ? solicitud.getValorSolicitado() : negocio != null ? negocio.getVr_negocio() : "")))%>',
                    '<%=convenio.getId_convenio()%>', '<%=convenio.getNombre()%>', '<%=convenio.isFactura_tercero() ? "t" : "f"%>', '<%=solicitud.getAfiliado()%>',
                    '<%=solicitud != null ? solicitud.getPlazo() : negocio != null ? negocio.getNodocs() : ""%>', '<%=solicitud.getPlazoPrCuota()%>',
                    '<%=solicitud.getTipoNegocio() + "_" + convenioserv.valida_remesa(convenio.getId_convenio(), solicitud.getTipoNegocio())%>', '<%=sector.getCodigo_sector()%>', '<%=sub_sector.getCodigo_sector()%>', '<%=sector.getNombre_sector()%>',
                    '<%=sub_sector.getNombre_sector()%>', '<%=Propietario.getInicio()%>');
        <%}%>
        }

    </script>

    <body       <%if (!act_liq.equals("")) {%>  onload=" cargar()"<%}%>>
        <div id="capaSuperior" style="position:absolute; width:100%; height:100px; z-index:0; left: 0px; top: 0px;">
            <jsp:include page="/toptsp.jsp?encabezado=LIQUIDADOR DE NEGOCIOS"/>
        </div>
        <div id="capaCentral" style="position:absolute; width:100%; height:90%; z-index:0; left: 0px; top: 100px;">
            <div id="contenido" style="visibility: hidden; display: none; height: 100px; width: 400px; background-color: #EEEEEE;"></div>
            <div id="contenido2" style="visibility: hidden; display: none; height: 200px; width: 500px; background-color: #EEEEEE;"></div>
            <form action='<%=CONTROLLER%>?estado=Liquidador&accion=Negocios&opcion=calcularLiquidacion&custodia=0&tipo_liq=<%=tipo_liq%>' name='formulario' method="post" >
                <input type="hidden" id="tipo_liq" value="<%=tipo_liq%>" />
                <table width="600" height="167" border="2"align="center">
                    <tr>
                        <td width="600" height="159">
                            <table width="100%" height="72%" class="tablaInferior">
                                <tr class="fila">
                                    <td align="left" nowrap class="subtitulo1">LIQUIDADOR</td>
                                    <td align="left" nowrap class="barratitulo"><img src="<%=BASEURL%>/images/titulo.gif" class="bordereporte"></td>
                                </tr>
                                <tr>
                                    <td align="left" width="30%" class="fila">Afiliado</td>
                                    <td align="left" width="70%" class="fila">
                                        <%if (tipo_liq.equals("MASTER")) {%>
                                        <select id="filtro" name="filtro">
                                            <option value="nit" selected>Nit</option>
                                            <option value="payment_name">Nombre</option>
                                        </select>
                                        <input type="text" id="nafil" name="nafil" onkeypress="pulsar(event);" size="50%">&nbsp;
                                        <script type="text/javascript">
        $("nafil").focus();
                                        </script>
                                        <%}%>
                                        <span id="spangen"></span>
                                        <br>
                                        <select id="cmbAfiliado" name="cmbAfiliado" style="width: 90%;" onchange="borrarSeleccion()" <%=(!act_liq.equals("")) ? "disabled" : ""%>   >
                                            <%if (tipo_liq.equals("MASTER")) {%>
                                            <option value="">...</option>
                                            <%} else {
                                                for (int i = 0; i < listaAfiliados.size(); i++) {
                                                    Proveedor prov = listaAfiliados.get(i);
                                            %>
                                            <option value='<%=prov.getC_nit()%>' <%=(solicitud != null && solicitud.getAfiliado().equals(prov.getC_nit())) ? " selected='selected'" : ""%> ><%=prov.getC_payment_name()%></option>
                                            <%  }
                                                }%>
                                        </select>
                                    </td>
                                </tr>



                                <tr class="fila" <%=tipo_liq.equals("SIMULADOR") ? "style='display:none'" : ""%>>
                                    <td align="left" width="30%" class="fila">Formulario</td>
                                    <td align="left" width="70%" class="fila">
                                        <input type="text" id="numsolc" name="numsolc" readonly>
                                        <input type="hidden" id="tasa" name="tasa" value="">
                                        <%if (solicitud == null) {%>
                                        <img alt="buscar" src="<%=BASEURL%>/images/botones/iconos/lupa.gif" width="15" height="15" style="cursor:pointer;" onclick="verForms('<%=CONTROLLER%>');">
                                        <%}%>
                                    </td>
                                </tr>
                                <tr>
                                    <td align="left" width="30%" class="fila">Convenio</td>
                                    <td align="left" width="70%" class="fila">
                                        <input type="text" id="convtext" name="convtext" readonly size="50%">
                                        <img <%=!tipo_liq.equals("SIMULADOR") ? "style='display:none'" : ""%> alt="buscar" src="<%=BASEURL%>/images/botones/iconos/lupa.gif" width="15" height="15" style="cursor:pointer;" onclick="verConvs('<%=CONTROLLER%>', '<%=tipo_liq%>');">
                                        <input type="hidden" id="convid" name="convid"value="<%=solicitud != null ? solicitud.getIdConvenio() : ""%>">
                                        <input type="hidden" id="hidIdProvConvenio" name="hidIdProvConvenio"value="<%=solicitud != null ? "" : ""%>">
                                        <input type="hidden" id="hidFacturaTercero" name="hidFacturaTercero"value="<%=solicitud != null ? convenio.isFactura_tercero() : ""%>">
                                    </td>
                                </tr>
                                <tr>
                                    <td align="left" width="30%" class="fila">Sector</td>
                                    <td align="left" width="70%" class="fila">
                                        <input type="text" id="secttext" name="secttext" readonly size="50%">

                                        <input type="hidden" id="sectid" name="sectid" value="<%=solicitud != null ? sector.getCodigo_sector() : ""%>">
                                    </td>
                                </tr>
                                <tr>
                                    <td align="left" width="30%" class="fila">Subsector</td>
                                    <td align="left" width="70%" class="fila">
                                        <input type="text" id="subsecttext" name="subsecttext" readonly size="50%">
                                        <input type="hidden" id="subsectid" name="subsectid" value="<%=solicitud != null ? sub_sector.getCodigo_sector() : ""%>">
                                    </td>
                                </tr>


                                <tr class="fila" <%=tipo_liq.equals("SIMULADOR") ? "style='display:none'" : ""%> >
                                    <td>Cliente </td>
                                    <td class="fila"><input name="nit" id="nit" type="text" size="12" maxlength="12" readonly>
                                    </td>
                                </tr>
                                <tr class="fila">
                                    <td > Plazo</td>
                                    <td width="49%" valign="middle"><input name="txtNumCuotas" type="text" class="textbox" id="txtNumCuotas" onkeyup="soloNumeros(this.id);" maxlength="2">
                                                                    <input type="hidden" id="fianza" name="fianza" value="">
                                    </td>
                                </tr>
                                <tr class="fila">
                                    <td >Valor a financiar</td>
                                    <td valign="middle">
                                        <span class="style3">
                                            <input name="txtValor" type="text" class="textbox" id="txtValor" onkeyup="soloNumeros(this.id);"  maxlength="20" <%if (solicitud != null && act_liq.equals("PERF")) {%> onChange="validar_monto()" <%}%> >
                                        </span>
                                    </td>
                                </tr>
                                <tr id="trPlazo" style="display:none" class="fila">
                                    <%
                                        ArrayList<String> listaplazos = null;
                                        try {
                                            listaplazos = gserv.plazoCuota();
                                        } catch (Exception e) {
                                            System.out.println("Error al obtener la lista de tipos de titulo valor: " + e.toString());
                                            e.printStackTrace();
                                        }
                                    %>
                                    <td>Plazo primera cuota</td>
                                    <td valign="middle">
                                        <input type="hidden" id="fechaPrimeraCuota" name="fechaPrimeraCuota" value="<%=fechaNegocio%>">
                                        <select name="forma_pago" id="forma_pago" class="listmenu" onChange="carcularFecha(document.getElementById('fechaPrimeraCuota').value)"  >
                                            <option value="" selected="selected">...</option>
                                            <%
                                                if (listaplazos != null && listaplazos.size() > 0) {
                                                    String row[] = null;
                                                    for (int i = 0; i < listaplazos.size(); i++) {
                                                        row = (listaplazos.get(i)).split(";_;");
                                            %>
                                            <option value="<%= row[0]%>" ><%= row[1]%></option>
                                            <%
                                                    }
                                                }
                                            %>
                                        </select>
                                    </td>
                                </tr>
                                <tr class="fila">
                                    <td >Fecha primera cuota </td>
                                    <td valign="middle">


                                        <select name="fechainicio" id="fechainicio">

                                        </select>
                                        <input type="hidden" id="fechaNegocio" name="fechaNegocio" value="<%=fechaNegocio%>">
                                        <!--                                        <input name='fechainicio' type='text' class="textbox" id="fechainicio" style='width:120'   <%if (solicitud != null && act_liq.equals("PERF")) {%>
                                                                                    onChange="validar_fecha()" <%}%>  readonly
                                                                                    value="<%=negocio != null ? negocio.getFecha_liquidacion().substring(0, 10) : ""%>">
                                                                                <a href="javascript:void(0);" onFocus="if(self.gfPop)gfPop.fPopCalendar(document.formulario.fechainicio);return false;" hidefocus>
                                                                                 <img src="<%=BASEURL%>/js/Calendario/cal.gif" width="16" height="16" border="0" alt="De click aqu&iacute; para ver el calendario."></a>-->
                                    </td>
                                </tr>
                                <tr id="trTituloValor" class="fila">
                                    <td>Tipo de t&iacute;tulo valor</td>
                                    <td class="fila">
                                        <select name="cmbTituloValor" id="cmbTituloValor" class="listmenu" onChange="mostrarCamposRemesa()" <%=(!act_liq.equals("")) ? "disabled" : ""%>  >
                                        </select>
                                    </td>
                                </tr>
                                <tr id="trBanco" style="display:none" class="fila">
                                    <td>Banco del cheque</td>
                                    <td valign="middle">
                                        <select name="cmbBanco" id="cmbBanco" >
                                            <option value="" selected>...</option>
                                            <%
                                                ArrayList<BeanGeneral> listaBancos = gserv.obtenerBancos();
                                                for (int i = 0; i < listaBancos.size(); i++) {
                                                    BeanGeneral bg = listaBancos.get(i);
                                            %>
                                            <option value="<%=bg.getValor_01()%>"  <%=(solicitud != null ? (solicitud.getBanco() != null ? (solicitud.getBanco().equals(bg.getValor_01()) ? " selected" : "") : "") : "")%>    ><%=bg.getValor_02()%></option>
                                            <%}%>
                                        </select>
                                    </td>
                                </tr>
                                <tr id="trCiudadCh" style="display:none" class="fila">
                                    <td>Ciudad del cheque</td>
                                    <td valign="middle"><input type="text" name="txtCiudadCheque" id="txtCiudadCheque" maxlength="4" value="<%=solicitud != null ? solicitud.getCiudadCheque() : ""%>" onKeyPress="soloDigitos(event, 'decNO')"></td>
                                </tr>
                                <tr id="trTipoCli" style="display:none" class="fila">
                                    <td>Tipo de cliente</td>
                                    <td valign="middle">
                                        <select name="cmbTipoCliente" id="cmbTipoCliente">
                                            <option value="">...</option>
                                            <option value="30">Propietario</option>
                                            <option value="45">No propietario</option>
                                        </select>
                                    </td>
                                </tr>
                                <tr id="trSolicitud" style="display:none" class="fila">
                                    <td>Solicitud</td>
                                    <td valign="middle"><input type="text" name="txtSolicitud" id="txtSolicitud" onKeyPress="soloDigitos(event, 'decNO')"></td>
                                </tr>
                                <tr id="trCiclo" style="display:none" class="fila">
                                    <td>Ciclo</td>
                                    <td valign="middle">
                                        <select name="cmbCiclo" id="cmbCiclo">
                                        </select>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
                <br>
                <div align="center">
                    <img src="<%=BASEURL%>/images/botones/aceptar.gif"  name="imgaceptar" onClick="enviarFormLiquidador('<%=act_liq%>');" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" style="cursor:hand">
                    <img src="<%=BASEURL%>/images/botones/salir.gif"  name="imgsalir"  onMouseOver="botonOver(this);" onClick="parent.close();" onMouseOut="botonOut(this);" style="cursor:hand">
                    <%if (!msj.equals("")) {%>
                    <div id ="div_mensaje">
                        <table width="500"  border="1" align="center"  bordercolor="#F7F5F4" bgcolor="#FFFFFF">
                            <tr>
                                <td width="550" align="center" class="mensajes"><%= msj%></td>
                                <td width="29" background="<%=BASEURL%>/images/cuadronaranja.JPG">&nbsp;</td>
                            </tr>
                        </table>
                    </div>
                    <%}%>
                </div>
                <input name="monto"  type="hidden" class="textbox" id="monto" onkeyup="soloNumeros(this.id);"   value="<%=negocio != null ? negocio.getVr_negocio() : solicitud != null ? solicitud.getValorSolicitado() : ""%>" >
                <input name='fecha'  type="hidden"class="textbox" id="fecha" style='width:120' readonly  value="<%=negocio != null ? negocio.getFecha_liquidacion().substring(0, 10) : ""%>">
                <input name='CONTROLLER'  type="hidden" id="CONTROLLER"  value=" <%= CONTROLLER%>" >
                <input name='act_liq'  type="hidden" id="act_liq"  value="<%= act_liq%>">



            </form>
        </div>
        <iframe width=188 height=166 name="gToday:normal:agenda.js:gfPop:plugins.js" id="gToday:normal:agenda.js:gfPop:plugins.js" src="<%=BASEURL%>/js/Calendario/ipopeng.htm" scrolling="no" frameborder="0" style="visibility:visible; z-index:999; position:absolute; left:-500px; top:0px;"> </iframe>
    </body>
</html>

