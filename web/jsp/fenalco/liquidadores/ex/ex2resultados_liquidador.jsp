<%@page contentType="text/html"%>
<%@page session="true"%>
<%@page errorPage="/error/ErrorPage.jsp"%>
<%@ page import="java.sql.*" %>
<%@ page import="java.util.*" %>
<%@ page import="java.io.*" %>
<%@ page import="java.lang.*" %>
<%@page import="java.util.*"%>
<%@page import="com.tsp.operation.model.*"%> 
<%@page import="com.tsp.operation.model.beans.*"%>
<%@page import="com.tsp.util.*"%>
<%@page import="com.tsp.util.Util.*"%>
<%@include file="/WEB-INF/InitModel.jsp"%>
<%@ taglib uri="http://jakarta.apache.org/taglibs/input-0.90" prefix="input" %>
<html>
<head>
<title>Liquidador Maestro</title>

<link href="<%=BASEURL%>/css/estilostsp.css" rel="stylesheet" type="text/css">         
<style type="text/css">
<!--
.style1 {font-size: 12px}
-->
</style>        
    </head>  
    <body > 
	<div id="capaSuperior" style="position:absolute; width:100%; height:100px; z-index:0; left: 0px; top: 0px;">
<jsp:include page="/WEB-INF/toptsp.jsp?encabezado=Liquidador Maestro"/>

</div>
<script language="Javascript">
function imprSelec(nombre)
{
  var ficha = document.getElementById(nombre);
  var ventimp = window.open(' ', 'popimpr');
  ventimp.document.write( ficha.innerHTML);
  ventimp.document.close();
  ventimp.print( );
  ventimp.close();
} 
</script> 
<div id="capaCentral" style="position:absolute; width:100%; height:85%; z-index:0; left: 0px; top: 100px; overflow: scroll;"> 
<script type='text/javascript' src="<%=BASEURL%>/js/Validacion.js"></script>
<script type='text/javascript' src="<%= BASEURL %>/js/boton.js"></script>
<script type='text/javascript' src="<%= BASEURL %>/js/general.js"></script>   			
<%				

				//double totalAvalMostrable=-1.0;
				//double tem=0;
				double valor_operation=0;//Cells(5, "B")=Cells(5, "B")	
				double custodiax=0;
				//double portex =0;
				
				Propietario  Propietario  = new Propietario();
				//System.out.println("aqui--------------------------");
    			Propietario = model.FenalcoFintraSvc.getPropietario();
				Usuario usuario            = (Usuario) session.getAttribute("Usuario");
				
                String ParNumero_Cheques = (String)request.getParameter("numero_cheques");
                String ParValor_Negocio = (String)request.getParameter("valor_desembolso");
                String Forma_Pago = (String)request.getParameter("forma_pago");
				String Estado_Remesa = (String)request.getParameter("estado_remesas");
                String fechai = (String)request.getParameter("fechainicio");
                String propietario   = request.getParameter("Nit");
				String tipo_negocio  = request.getParameter("tipnegocio");
				
				String aval   = request.getParameter("aval");
				String custodia   = request.getParameter("custodia");
				String remesas  = request.getParameter("remesas");
				String colorfila = "";
				
                int Numero_Cheques = Integer.parseInt(ParNumero_Cheques);    
                double Valor_Negocio = Double.parseDouble(ParValor_Negocio);
                
                double Tasa = Propietario.getTasa_fenalco()/100;
                double Custodia_Cheque = Propietario.getCustodiacheque();
				double porrem=Propietario.getRem()/100;//esta remesa hay que traerla
				
				//System.out.println("remesas--888888---------> "+ porrem);
                
                java.util.Calendar Fecha_Sugerida = null;
                java.util.Calendar Fecha_Negocio = Util.crearCalendarDate(fechai);
				java.util.Calendar Fecha_Sugerida_2 = null;
				java.util.Calendar Fecha_Sugerida_3 = null;
               			   
                int dia_valido=0;
                int dia=0; 
				int mes=0; 
				int ano=0;
				
				int dia_s=0; 
				int mes_s=0; 
				int ano_s=0;
				
				dia = Integer.parseInt(fechai.substring(8,10));
    			mes = Integer.parseInt(fechai.substring(5,7));    
    			ano = Integer.parseInt(fechai.substring(0,4));
				Fecha_Negocio.set(ano, mes-1, dia);
				
                double Formula=0;
                double Suma=0;
                double Valor_Cheque=0;
				double Valor_Cheque_X=0;
                double Desembolso=0;
                double Valor_Capital=0;
                double Valor_Interes=0;
				double Valor_Remesa=0;
                double Saldo_Final=0;
                double Saldo_Inicial=0;
                double Valor_Doc=0;
                double Formulas[] ;
				double ValorNum=0;
                double TotalAval=0;
				double ValorAval=0;
				double ValorAvalIva=0;
				 double valor_1=0;
                double valor_2=0;
                double valor_3=0;
				double valor_4=0;
				double dias=0;
                Suma = 0;
				double lim=11655;
				
                
                String Filas_Tabla_Liquidacion   = "";
                String Filas_Tabla_Liquidacion_2 = "";
                String Dias_Primer_Cheque        =  Forma_Pago ;
                Saldo_Final = 0;

                Formula = (((Math.pow((1+Tasa),12))-1));
				
                Formulas= new double[Numero_Cheques+1];

                for (int fila = 1; fila <= Numero_Cheques; fila++){
				
					if (fila%2==0){
					colorfila = "filagris";
					}else{
					colorfila = "filaazul";}
				
                    Filas_Tabla_Liquidacion = Filas_Tabla_Liquidacion +"<tr class='"+colorfila+"'>";

                    //Numero del documento

                    Filas_Tabla_Liquidacion = Filas_Tabla_Liquidacion +"    <td>";
                    Filas_Tabla_Liquidacion = Filas_Tabla_Liquidacion +"    <p align='center'>"+ String.valueOf(fila) +"</p>";
                    Filas_Tabla_Liquidacion = Filas_Tabla_Liquidacion +"    </td>";

                    // Fechas 
					if (fila == 1){
						
						if (!Forma_Pago.equals("30")){
							Fecha_Sugerida = Util.TraerFecha(fila, dia, mes-1, ano, Forma_Pago);
							
							Fecha_Sugerida_2 = Util.TraerFecha(fila, dia, mes-1, ano, Forma_Pago);
							
							Fecha_Sugerida_3 = Util.TraerFecha(fila, dia, mes-1, ano, Forma_Pago);
							
						}else{
						
							Fecha_Sugerida = Util.TraerFecha(fila, dia, mes, ano, Forma_Pago);
							
							Fecha_Sugerida_2 = Util.TraerFecha(fila, dia, mes, ano, Forma_Pago);
							
							Fecha_Sugerida_3 = Util.TraerFecha(fila, dia, mes, ano, Forma_Pago);
						}
						
					}else{
					
						if (!Forma_Pago.equals("45")){
							
							Fecha_Sugerida = Util.TraerFecha(fila, dia, mes, ano, Forma_Pago);
							
							Fecha_Sugerida_2 = Util.TraerFecha(fila, dia, mes, ano, Forma_Pago);
							
							Fecha_Sugerida_3 = Util.TraerFecha(fila, dia, mes, ano, Forma_Pago);
							
						}else{
							
							Fecha_Sugerida = Util.TraerFecha(fila, dia, mes-1, ano, Forma_Pago);
								
							Fecha_Sugerida_2 = Util.TraerFecha(fila, dia, mes-1, ano, Forma_Pago);
							
							Fecha_Sugerida_3 = Util.TraerFecha(fila, dia, mes-1, ano, Forma_Pago);
														
						}
						
					}	
					
					if (!Forma_Pago.equals("30")){
                        if (fila == 1) 
                        {
                            dia = Fecha_Sugerida.get(Calendar.DATE);
                            mes = Fecha_Sugerida.get(Calendar.MONTH);
                            ano = Fecha_Sugerida.get(Calendar.YEAR);
                        }
                    
					}
					
					dia_s = Fecha_Sugerida_2.get(Calendar.DATE);
					mes_s = Fecha_Sugerida_2.get(Calendar.MONTH);
					ano_s = Fecha_Sugerida_2.get(Calendar.YEAR);
							
					//System.out.println("dia= "+dia_s+"mes="+mes_s+"a�o"+ano_s);
                    
					if( mes_s == 12 ){
							ano_s=ano_s-1;}
					if( mes_s == 0 ){
						mes_s = 1;}
					else if (mes_s!=1){
						mes_s =mes_s+1;
					
					}else if (mes_s==1){
						mes_s =mes_s=2;
					}
					int j=0;
					
					//System.out.println("Fecha Antes de Festivo= "+Util.ObtenerFechaCompleta(Fecha_Sugerida_2));
		
					while (model.FenalcoFintraSvc.buscarFestivo(ano_s+"-"+mes_s+"-"+dia_s)){
						
					    //dia_s = dia_s + 1;
						//dia_s  = Util.buscarDiaFechaValida(dia_s, mes_s, ano_s);
						
						mes_s=mes_s-1;
						Fecha_Sugerida_2.set(ano_s, mes_s, dia_s);
						Fecha_Sugerida_2.add(Calendar.DAY_OF_YEAR,1);
						//System.out.println("Fecha Antes de Festivo= "+Util.ObtenerFechaCompleta(Fecha_Sugerida_2));
						//Fecha_Sugerida_2.set(ano_s, mes_s, dia_s);
						dia_s = Fecha_Sugerida_2.get(Calendar.DATE);
						mes_s = Fecha_Sugerida_2.get(Calendar.MONTH);
						ano_s = Fecha_Sugerida_2.get(Calendar.YEAR);
						
						
						//if( mes_s == 0 ){
							//ano_s=ano_s-1;
						//}
						//if( mes_s == 0 ){
							//mes_s = 12;}
						//}else{
							mes_s=mes_s+1;
						//}
						
						j=1;
						
					}
					
					//System.out.println("Fecha Despues de Festivo= "+Util.ObtenerFechaCompleta(Fecha_Sugerida_2));
					
					if (j==1)
					{
						if (mes_s!=0){
							Fecha_Sugerida_2.set(ano_s, mes_s-1, dia_s);
						}else{
							Fecha_Sugerida_2.set(ano_s, 1, dia_s);
						}
					}
					if (fila != 1){
						if( mes_s != 12 ){
							Fecha_Sugerida_3.set(ano_s, mes_s-1, dia_s);
						}
					}else{
						Fecha_Sugerida_3.set(ano_s, mes_s, dia_s);
					}
					
       				
					Filas_Tabla_Liquidacion = Filas_Tabla_Liquidacion +"    <td>";
					Filas_Tabla_Liquidacion = Filas_Tabla_Liquidacion +"    <p align='right' >"+ Util.ObtenerFechaCompleta(Fecha_Sugerida) +"</p>";
					Filas_Tabla_Liquidacion = Filas_Tabla_Liquidacion +"    </td>";
        
        // Dias transcurridos
		/*System.out.println("fila= "+fila);
		System.out.println("negocio= "+Util.ObtenerFechaCompleta(Fecha_Negocio));
		System.out.println("33333333333333= "+Util.ObtenerFechaCompleta(Fecha_Sugerida_3));
		System.out.println("222222222222222 "+Util.ObtenerFechaCompleta(Fecha_Sugerida_2));
		System.out.println("1111111111111= "+Util.ObtenerFechaCompleta(Fecha_Sugerida));*/
        
        /*Filas_Tabla_Liquidacion = Filas_Tabla_Liquidacion +"    <td>";
		if (fila != 1){
        Filas_Tabla_Liquidacion = Filas_Tabla_Liquidacion +"    <p align='right' >"+ Util.diasTranscurridos(Fecha_Negocio, Fecha_Sugerida_2) +"</p>";
		}else{
		Filas_Tabla_Liquidacion = Filas_Tabla_Liquidacion +"    <p align='right' >"+ Util.diasTranscurridos(Fecha_Negocio, Fecha_Sugerida_2) +"</p>";
		}
        Filas_Tabla_Liquidacion = Filas_Tabla_Liquidacion +"    </td>";
        
        // Valor de la formula
        
        /*Filas_Tabla_Liquidacion = Filas_Tabla_Liquidacion +"    <td witdh = '0'>";
        Filas_Tabla_Liquidacion = Filas_Tabla_Liquidacion +"    <p align='right' >"+ obtenerValor(diasTranscurridos(Fecha_Negocio, Fecha_Sugerida),Formula) +"</p>";
        Filas_Tabla_Liquidacion = Filas_Tabla_Liquidacion +"    </td>";  
		Filas_Tabla_Liquidacion = Filas_Tabla_Liquidacion +"    <td witdh = '0'>";
        Filas_Tabla_Liquidacion = Filas_Tabla_Liquidacion +"    <p align='right' >"+ Util.ObtenerFechaCompleta(Fecha_Sugerida_2) +"</p>";
        Filas_Tabla_Liquidacion = Filas_Tabla_Liquidacion +"    </td>";  */
		
		if (fila != 1){
			dias = Util.diasTranscurridos(Fecha_Negocio, Fecha_Sugerida_2);
		}else{
			dias = Util.diasTranscurridos(Fecha_Negocio, Fecha_Sugerida_2);
		}
        valor_1=(dias/365);
		valor_2=(1+Formula);
		valor_3 =Math.pow(valor_2, valor_1);
		valor_4 = ((1/valor_3));
		
		double val = Util.redondear(valor_4,9);
		//double valor_5 = Double.parseDouble(val);
		Formulas[fila]= val;
        //Formulas[fila]= Util.obtenerValor(Util.diasTranscurridos(Fecha_Negocio, Fecha_Sugerida_2),Formula);
		//System.out.println("formulaaaaa = "+Formulas[fila]);
        Suma = Suma + Formulas[fila];
        Filas_Tabla_Liquidacion = Filas_Tabla_Liquidacion +"</tr>";
		if (j==1)
		{
			
			Fecha_Sugerida_2.set(ano_s, mes_s+1, dia_s);
			dia_s = Fecha_Sugerida_2.get(Calendar.DATE);
			mes_s = Fecha_Sugerida_2.get(Calendar.MONTH);
			ano_s = Fecha_Sugerida_2.get(Calendar.YEAR);
		}
    } 
	//System.out.println("sumaaaaaaaa = "+Suma );
	Valor_Cheque = Util.redondear((Valor_Negocio/(Suma)),0);
	
	//Valor_Cheque = Suma;    
    
	if (custodia.equals("1") && aval.equals("1") && remesas.equals("0")){
		Vector Aval = model.FenalcoFintraSvc.getAvales();
	
		for (int fila = 0; fila <= Numero_Cheques-1; fila++){
			ValorNum = (Double.parseDouble(""+Aval.get(fila)))/100;
			ValorAval = (Valor_Cheque * ValorNum);
			ValorAvalIva = (ValorAval* 0.19);
			TotalAval = TotalAval+(ValorAval + ValorAvalIva);
		
		}
		Valor_Cheque = Math.round(((Valor_Negocio+TotalAval)/Suma) + (Custodia_Cheque));
		Valor_Cheque_X = Math.round(((Valor_Negocio+TotalAval)/Suma)+ (Custodia_Cheque));
	}else if (aval.equals("1") && custodia.equals("0") && remesas.equals("0")){
		Vector Aval = model.FenalcoFintraSvc.getAvales();
		for (int fila = 0; fila <= Numero_Cheques-1; fila++){
				
				ValorNum = (Double.parseDouble(""+Aval.get(fila)))/100;
				ValorAval = (Valor_Cheque * ValorNum);
				ValorAvalIva = (ValorAval* 0.19);
				TotalAval = TotalAval+(ValorAval + ValorAvalIva);
				
		
		}
		Valor_Cheque = Math.round((Valor_Negocio+TotalAval)/Suma);
		Valor_Cheque_X = Math.round((Valor_Negocio+TotalAval)/Suma);
	}else if (custodia.equals("1") && aval.equals("0") && remesas.equals("0")){
		
		Valor_Cheque = Math.round(((Valor_Negocio)/Suma) + (Custodia_Cheque));
		Valor_Cheque_X = Math.round(((Valor_Negocio)/Suma) + (Custodia_Cheque));
	}else if (custodia.equals("0") && aval.equals("0") && remesas.equals("0")){
		Valor_Cheque = Math.round((Valor_Negocio)/Suma);
		Valor_Cheque_X = Math.round((Valor_Negocio)/Suma);
	}if (custodia.equals("1") && aval.equals("1") && remesas.equals("1")){
		Vector Aval = model.FenalcoFintraSvc.getAvales();
	
		//iniciox
		//System.out.println("holax");
		//double TotalAval2=0;
		for (int fila = 0; fila <= Numero_Cheques-1; fila++){//_
				ValorNum = (Double.parseDouble(""+Aval.get(fila)))/100;//_
				ValorAval = (Valor_Cheque * ValorNum);//_
				ValorAvalIva = (ValorAval* 0.19);//_
				TotalAval = TotalAval+(ValorAval + ValorAvalIva);//_
				System.out.println("ValorNum"+ValorNum+"Valor_Cheque"+Valor_Cheque+"totalaval"+TotalAval);
		}//_
		System.out.println("----------------------------------------------------------------------------------------------------------------------------------");
		
		custodiax=0;
		custodiax=(Custodia_Cheque* Numero_Cheques);
		valor_operation=Valor_Negocio+TotalAval;
		System.out.println("Valor_Cheque"+Valor_Cheque);
		Valor_Cheque = Math.round(((valor_operation)/Suma) );//_
		Valor_Cheque_X = Math.round(((valor_operation)/Suma));//_
		TotalAval=0;
		for (int fila = 0; fila <= Numero_Cheques-1; fila++){//_
				ValorNum = (Double.parseDouble(""+Aval.get(fila)))/100;//_
				ValorAval = (Valor_Cheque * ValorNum);//_
				ValorAvalIva = (ValorAval* 0.19);//_
				TotalAval = TotalAval+(ValorAval + ValorAvalIva);//_
				System.out.println("ValorNum"+ValorNum+"Valor_Cheque"+Valor_Cheque+"totalaval"+TotalAval);
		}//_
		
		//valor_operation=valor_operation+TotalAval;//Cells(5, "B") = Cells(5, "B") + Cells(73, "M")
		System.out.println("valor_operation"+valor_operation);
		//Valor_Cheque = Math.round(((Valor_Negocio+TotalAval)/Suma) );//_
		//Valor_Cheque_X = Math.round(((Valor_Negocio+TotalAval)/Suma));//_
		
		System.out.println("Valor_Negocio +Valor_Cheque+ TotalAval - valor_operation__________"+Valor_Negocio +"_"+Valor_Cheque+"_"+ TotalAval +"_"+ valor_operation);
		while ((Valor_Negocio +custodiax+ TotalAval - valor_operation )>= 1.0){
			valor_operation=Valor_Negocio+custodiax+TotalAval;//Cells(5, "B") = Cells(5, "B") + Cells(73, "M")
			Valor_Cheque = Math.round(((valor_operation)/Suma) );//_
			Valor_Cheque_X = Math.round(((valor_operation)/Suma));//_
			System.out.println("Valor_Negocio +Valor_Cheque+ TotalAval - valor_operation__"+Valor_Negocio +"_"+Valor_Cheque+"_"+ TotalAval +"_"+ valor_operation);
			TotalAval=0;
			for (int fila = 0; fila <= Numero_Cheques-1; fila++){
				ValorNum = (Double.parseDouble(""+Aval.get(fila)))/100;
				ValorAval = (Valor_Cheque * ValorNum);
				ValorAvalIva = (ValorAval* 0.19);
				TotalAval = TotalAval+(ValorAval + ValorAvalIva);
				System.out.println("Valor_Negocio +Valor_Cheque+ TotalAval - valor_operation:"+Valor_Negocio +"_"+Valor_Cheque+"_"+ TotalAval +"_"+ valor_operation);
			}
			//Valor_Cheque = Math.round(((Valor_Negocio+TotalAval)/Suma) );//_
			//Valor_Cheque_X = Math.round(((Valor_Negocio+TotalAval)/Suma));//_
			//Cells(5, "B") = Cells(5, "B") + (cuValorCustodia + Cells(73, "M")) - Cells(5, "B")
			//valor_operation=Valor_Negocio+TotalAval;
		}
		System.out.println("Valor_Negocio +Valor_Cheque+ TotalAval - valor_operation:"+Valor_Negocio +"_"+Valor_Cheque+"_"+ TotalAval +"_"+ valor_operation+"xxxxxxxxxxxxxxxxxx");
		//totalAvalMostrable=TotalAval;
		//finx
	
		/*
		for (int fila = 0; fila <= Numero_Cheques-1; fila++){
			ValorNum = (Double.parseDouble(""+Aval.get(fila)))/100;
			ValorAval = (Valor_Cheque * ValorNum);
			ValorAvalIva = (ValorAval* 0.19);
			TotalAval = TotalAval+(ValorAval + ValorAvalIva);
		
		}
		Valor_Cheque = Math.round(((Valor_Negocio+TotalAval)/Suma) + (Custodia_Cheque));
		Valor_Cheque_X = Math.round(((Valor_Negocio+TotalAval)/Suma)+ (Custodia_Cheque));*/
		
	}else if (aval.equals("1") && custodia.equals("0") && remesas.equals("1")){
		Vector Aval = model.FenalcoFintraSvc.getAvales();
		for (int fila = 0; fila <= Numero_Cheques-1; fila++){
				
				ValorNum = (Double.parseDouble(""+Aval.get(fila)))/100;
				ValorAval = (Valor_Cheque * ValorNum);
				ValorAvalIva = (ValorAval* 0.19);
				TotalAval = TotalAval+(ValorAval + ValorAvalIva);
				
		}
		Valor_Cheque = Math.round((Valor_Negocio+TotalAval)/Suma);
		Valor_Cheque_X = Math.round((Valor_Negocio+TotalAval)/Suma);
		
	}else if (custodia.equals("1") && aval.equals("0") && remesas.equals("1")){
		
		Valor_Cheque = Math.round(((Valor_Negocio)/Suma) + (Custodia_Cheque));
		Valor_Cheque_X = Math.round(((Valor_Negocio)/Suma) + (Custodia_Cheque));
	}else if (custodia.equals("0") && aval.equals("0") && remesas.equals("1")){
		Valor_Cheque = Math.round((Valor_Negocio)/Suma);
		Valor_Cheque_X = Math.round((Valor_Negocio)/Suma);
	}
	
	//Valor_Remesa = (Valor_Cheque * porrem);
	if ((Valor_Cheque * porrem) < lim){
					Valor_Remesa = lim;
				}else{
					Valor_Remesa = Valor_Cheque * porrem;
				}
		
	if (custodia.equals("1") && aval.equals("1") && remesas.equals("0")){
		Desembolso = Valor_Negocio + TotalAval -(Valor_Remesa*Numero_Cheques);
	}else if (aval.equals("1") && custodia.equals("0") && remesas.equals("0")){
		Desembolso = (Valor_Negocio+TotalAval)-(Custodia_Cheque* Numero_Cheques)-(Valor_Remesa*Numero_Cheques); 
	}else if (custodia.equals("1") && aval.equals("0") && remesas.equals("0")){
		Desembolso = Valor_Negocio  -(Valor_Remesa*Numero_Cheques);
	}else if (custodia.equals("0") && aval.equals("0") && remesas.equals("0")){
		Desembolso = Valor_Negocio-(Custodia_Cheque* Numero_Cheques) -(Valor_Remesa*Numero_Cheques);
	}else if (custodia.equals("1") && aval.equals("1") && remesas.equals("1")){
		Desembolso = Valor_Negocio + TotalAval;
	}else if (aval.equals("1") && custodia.equals("0") && remesas.equals("1")){
		Desembolso = (Valor_Negocio+TotalAval)-(Custodia_Cheque* Numero_Cheques); 
	}else if (custodia.equals("1") && aval.equals("0") && remesas.equals("1")){
		Desembolso = Valor_Negocio;
	}else if (custodia.equals("0") && aval.equals("0") && remesas.equals("1")){
		Desembolso = Valor_Negocio-(Custodia_Cheque* Numero_Cheques);
	}
    
    for (int fila = 1; fila <= Numero_Cheques; fila++){
	
		if (fila%2==0){
			colorfila = "filagris";
			}else{
			colorfila = "filaazul";}
    	
        Filas_Tabla_Liquidacion_2 = Filas_Tabla_Liquidacion_2 +"<tr class='"+colorfila+"'>";
        
        //Valor de los intereses
        
        Valor_Capital = (Valor_Cheque_X*Formulas[fila]);
        Valor_Interes = (Valor_Cheque_X-Valor_Capital);
        Valor_Doc = Math.round(Valor_Capital+Valor_Interes);
		if( Estado_Remesa.equals("0")&& remesas.equals("1") ){
			if ((Valor_Doc * porrem) < lim){
				Valor_Doc = Valor_Doc + lim;
			}else{
				Valor_Doc = Valor_Doc +(Valor_Doc * porrem);
			}
		}
		//Valor_Cheque = Suma; 
       
        if (fila == 1) 
        {
            Saldo_Inicial = Math.round(Valor_Negocio);
			if (custodia.equals("1") && aval.equals("1")){
				 Saldo_Inicial = Math.round(Valor_Negocio+ TotalAval);
			}else if (aval.equals("1") && custodia.equals("0")){
				 Saldo_Inicial = Math.round(Valor_Negocio+ TotalAval);
			}else if (custodia.equals("1") && aval.equals("0")){
			 	Saldo_Inicial = Math.round(Valor_Negocio);
			}else if (custodia.equals("0") && aval.equals("0")){
			 	Saldo_Inicial = Math.round(Valor_Negocio);
			}
        }else{
            Saldo_Inicial = Math.round(Saldo_Final);
        }
        
        Saldo_Final = Math.round(Saldo_Inicial - Valor_Capital);
               
        
        Filas_Tabla_Liquidacion_2 = Filas_Tabla_Liquidacion_2 +"    <td>";
        Filas_Tabla_Liquidacion_2 = Filas_Tabla_Liquidacion_2 +"    <p align='right' >"+ String.valueOf(Util.customFormat(Saldo_Inicial)) +"</p>";
        Filas_Tabla_Liquidacion_2 = Filas_Tabla_Liquidacion_2 +"    </td>";

        /*Filas_Tabla_Liquidacion_2 = Filas_Tabla_Liquidacion_2 +"    <td>";
        Filas_Tabla_Liquidacion_2 = Filas_Tabla_Liquidacion_2 +"    <p align='center' >"+ String.valueOf(Valor_Capital) +"</p>";
        Filas_Tabla_Liquidacion_2 = Filas_Tabla_Liquidacion_2 +"    </td>";*/

        /*Filas_Tabla_Liquidacion_2 = Filas_Tabla_Liquidacion_2 +"    <td>";
        Filas_Tabla_Liquidacion_2 = Filas_Tabla_Liquidacion_2 +"    <p align='center' >"+ String.valueOf(Valor_Interes) +"</p>";
        Filas_Tabla_Liquidacion_2 = Filas_Tabla_Liquidacion_2 +"    </td>";*/
        
        Filas_Tabla_Liquidacion_2 = Filas_Tabla_Liquidacion_2 +"    <td>";
        Filas_Tabla_Liquidacion_2 = Filas_Tabla_Liquidacion_2 +"    <p align='right' >"+ String.valueOf(Util.customFormat(Valor_Doc)) +"</p>";
        Filas_Tabla_Liquidacion_2 = Filas_Tabla_Liquidacion_2 +"    </td>";
		Filas_Tabla_Liquidacion_2 = Filas_Tabla_Liquidacion_2 +"    <td>";
		if (fila == Numero_Cheques){
			Filas_Tabla_Liquidacion_2 = Filas_Tabla_Liquidacion_2 +"    <p align='right' >0.00</p>";
		}else{
        	Filas_Tabla_Liquidacion_2 = Filas_Tabla_Liquidacion_2 +"    <p align='right' >"+ String.valueOf(Util.customFormat(Saldo_Final)) +"</p>";
		}
		
        Filas_Tabla_Liquidacion_2 = Filas_Tabla_Liquidacion_2 +"    </td>";
        
        Filas_Tabla_Liquidacion_2 = Filas_Tabla_Liquidacion_2 +"</tr>";
		
		
    } 
        
%>
<a onClick="javascript:imprSelec('seleccion')"><img src="<%=BASEURL%>/images/imprime.gif" width="70" height="19" style="cursor:hand"></a> 
 <DIV ID="seleccion">
<table align="center" width="81%" border="0" cellpadding='0' cellspacing='0'>
        <tr>
            <td >
    
                <table align="center" width="100%" cellpadding='0' cellspacing='0' class="tablaInferior" border="1" >

					<tr class="fila">
                        <td colspan = "2" align="center">
                            <p><b>Informaci�n del Negocio</b></p>
                        </td>
                        
                    </tr>
			
                    <tr class="filagris">
                        <td width='50%'>
                            <p><b>Valor del negocio</b></p>
                        </td>
                        <td width='50%'>
                            <p ><%=Util.customFormat(Valor_Negocio)%></p>
                        </td>
                    </tr>
                    <tr class="filaazul">
                        <td >
                            <p><b>No de <%if(tipo_negocio.equals("01")){%>Cheques<%}else{%>Letras<%}%></b></p>
                        </td>
                        <td>
                            <p ><%=Numero_Cheques%></p>
                        </td>
                    </tr>
                    <tr class="filagris">
                        <td >
                            <p><b>Valor desembolso</b></p>
                        </td>
                        <td>
                            <p ><%=Util.customFormat(Desembolso)%></p>
                        </td>
                    </tr>
                    <tr class="filaazul">
                        <td >
                            <p><b>Valor de <%if(tipo_negocio.equals("01")){%>los Cheques<%}else{%>las Letras<%}%></b></p>
                        </td>
                        <td>
                            <p ><%=Util.customFormat(Valor_Cheque)%></p>
                        </td>
                    </tr>
                    <tr class="filagris">
                        <td >
                            <p><b>Fecha de Negocio</b></p>
                        </td>
                        <td>
                            <p ><%=Util.ObtenerFechaCompleta(Fecha_Negocio)%></p>
                        </td>
                    </tr>
                </table>
            </td>
            <td >
                <table align="center" width="100%" cellpadding='0' cellspacing='0' class="tablaInferior" border="1" >

					<tr class="fila">
						<td width='25%' align="center">
                            <p><b></b></p>
                        </td>
                        <td width='50%' align="center">
                            <p><b>Establecimiento de comercio</b></p>
                        </td>
						<td width='25%' align="center">
                            <p><b>Cliente</b></p>
                        </td>
                        
                    </tr>
			
                    <tr class="filagris">
                        <td width='25%' align="center">
                            <p><b>Custodia</b></p>
                        </td>
                        <td width='50%' align="center">
                            <%if (custodia.equals("0")){
									%><p><b>SI</b></p><%}else{%><p><b>NO</b></p><%}%>
                        </td>
						<td width='25%' align="center">
                            <p><b><%if (custodia.equals("0")){
									%><p><b>NO</b></p><%}else{%><p><b>SI</b></p><%}%>
                        </td>
                    </tr>
                    <tr class="filaazul">
                       <td width='25%' align="center">
                            <p><b>Aval</b></p>
                      </td>
                        <td width='50%' align="center">
                            <%if (aval.equals("0")){
									%><p><b>SI</b></p><%}else{%><p><b>NO</b></p><%}%>
                        </td>
						<td width='25%' align="center">
                            <p><b><%if (aval.equals("0")){
									%><p><b>NO</b></p><%}else{%><p><b>SI</b></p><%}%>
                        </td>
                    </tr>
					<%if (Estado_Remesa.equals("0")){
									%><tr class="filagris">
                       <td width='25%' align="center">
                            <p><b>Remesas</b></p>
                      </td>
                        <td width='50%' align="center">
                            <%if (remesas.equals("0")){
									%><p><b>SI</b></p><%}else{%><p><b>NO</b></p><%}%>
                        </td>
						<td width='25%' align="center">
                            <p><b><%if (remesas.equals("0")){
									%><p><b>NO</b></p><%}else{%><p><b>SI</b></p><%}%>
                        </td>
                    </tr>
                        <%}%>               
                </table>
            </td>
        </tr>
        <tr>
            <td colspan='2'>
                <br><br>
            </td> 
        </tr>
        <tr>
            <td width='40%'>
            
                <table width="100%" cellpadding='0' cellspacing='0' class="tablaInferior" border="1">
                    <tr class="fila">
                        <td>
                            <p align='center'><b># Doc.</b></p>
                        </td>
                        <td>
                            <p align='center'><b>Fecha</b></p>                                
                        </td>
                       <!-- <td bgcolor='green'>
                            <p align='center'><b>Dias</b></p>
                        </td>
                        <td width='0' bgcolor='green'>
                            <p align='center'><b>Formula</b></p>
                        </td>!-->
                    </tr>
                    <%=Filas_Tabla_Liquidacion%> 
					<tr >
						<td> </td>
						<td align="center" class="fila style1"> Total a Pagar   </td>
					</tr> 
                </table>
                
          </td>
          <td width='40%'>
                <table width="100%" cellpadding='0' cellspacing='0' class="tablaInferior" border="1">
                    <tr class="fila">
                        <td>
                            <p align='center'><b>Saldo inicial</b></p>
                        </td>
                        <!--<td bgcolor='green'>
                            <p align='center'><b>Capital</b></p>
                        </td>
                        <td bgcolor='green'>
                            <p align='center'><b>Intereses</b></p>
                        </td>-->
                        <td>
                            <p align='center'><b>Vr <%if(tipo_negocio.equals("01")){%>Cheques<%}else{%>Letras<%}%></b></p>
                        </td>
					
                        <td>
                            <p align='center'><b>Saldo final</b></p>
                        </td>
                    </tr>
                    <%=Filas_Tabla_Liquidacion_2%>
					<tr >
						<td align="right" colspan="2" class="fila style1"> <%=Util.customFormat(Valor_Doc*Numero_Cheques)%></td>
					</tr> 
                </table>
          </td>
        </tr>
        <tr>
            <td colspan='2'>
                <p>&nbsp;</p>
                <p align='center'>
				
					
				<%
				if(aval.equals("0") && custodia.equals("0") && remesas.equals("0")){
				aval="0";
				custodia="0";
				if(Estado_Remesa.equals("0")){
				remesas="0";
				}else{
				remesas="1";
				}
				%>
					<img src="<%=BASEURL%>/images/botones/Custodia.gif"   height="21"  title='Custodia'  onClick="location.href='<%=CONTROLLER%>?estado=Fenalco&accion=Fintra&opcion=CalcularCustodia&numero_cheques=<%=ParNumero_Cheques%>&valor_desembolso=<%=ParValor_Negocio%>&forma_pago=<%=Forma_Pago%>&fechainicio=<%=fechai%>&Nit=<%=propietario%>&custodia=<%=custodia%>&aval=<%=aval%>&tipo_negocio=<%=tipo_negocio%>&remesas=<%=remesas%>&remesa=<%=Estado_Remesa%>'" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" style="cursor:hand">
					<img src="<%=BASEURL%>/images/botones/Aval.gif"   height="21"  title='Aval'  onClick="location.href='<%=CONTROLLER%>?estado=Fenalco&accion=Fintra&opcion=CalcularAval&numero_cheques=<%=ParNumero_Cheques%>&valor_desembolso=<%=ParValor_Negocio%>&forma_pago=<%=Forma_Pago%>&fechainicio=<%=fechai%>&Nit=<%=propietario%>&custodia=<%=custodia%>&aval=<%=aval%>&tipo_negocio=<%=tipo_negocio%>&remesas=<%=remesas%>&remesa=<%=Estado_Remesa%>'" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" style="cursor:hand" >
				<%if(Estado_Remesa.equals("0")){%>
					<img src="<%=BASEURL%>/images/botones/Remesas.gif"   height="21"  title='Remesas'  onClick="location.href='<%=CONTROLLER%>?estado=Fenalco&accion=Fintra&opcion=CalcularRemesas&numero_cheques=<%=ParNumero_Cheques%>&valor_desembolso=<%=ParValor_Negocio%>&forma_pago=<%=Forma_Pago%>&fechainicio=<%=fechai%>&Nit=<%=propietario%>&custodia=<%=custodia%>&aval=<%=aval%>&tipo_negocio=<%=tipo_negocio%>&remesas=<%=remesas%>&remesa=<%=Estado_Remesa%>'" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" style="cursor:hand" >
				<%}%>
				<%}else if(aval.equals("1") && custodia.equals("0") && remesas.equals("0")){
				aval="1";
				custodia="0";
				if(Estado_Remesa.equals("0")){
				remesas="0";
				}else{
				remesas="1";
				}
				%>	
					<img src="<%=BASEURL%>/images/botones/Custodia.gif"   height="21"  title='Custodia'  onClick="location.href='<%=CONTROLLER%>?estado=Fenalco&accion=Fintra&opcion=CalcularCustodia&numero_cheques=<%=ParNumero_Cheques%>&valor_desembolso=<%=ParValor_Negocio%>&forma_pago=<%=Forma_Pago%>&fechainicio=<%=fechai%>&Nit=<%=propietario%>&custodia=<%=custodia%>&aval=<%=aval%>&tipo_negocio=<%=tipo_negocio%>&remesas=<%=remesas%>&remesa=<%=Estado_Remesa%>'" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" style="cursor:hand">
					<img src="<%=BASEURL%>/images/botones/Aval.gif"   height="21"  title='Aval'  onClick="location.href='<%=CONTROLLER%>?estado=Fenalco&accion=Fintra&opcion=CalcularAval&numero_cheques=<%=ParNumero_Cheques%>&valor_desembolso=<%=ParValor_Negocio%>&forma_pago=<%=Forma_Pago%>&fechainicio=<%=fechai%>&Nit=<%=propietario%>&custodia=<%=custodia%>&aval=<%=aval%>&tipo_negocio=<%=tipo_negocio%>&remesas=<%=remesas%>&remesa=<%=Estado_Remesa%>'" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" style="cursor:hand">
				<%if(Estado_Remesa.equals("0")){%>
					<img src="<%=BASEURL%>/images/botones/Remesas.gif"   height="21"  title='Remesas'  onClick="location.href='<%=CONTROLLER%>?estado=Fenalco&accion=Fintra&opcion=CalcularRemesas&numero_cheques=<%=ParNumero_Cheques%>&valor_desembolso=<%=ParValor_Negocio%>&forma_pago=<%=Forma_Pago%>&fechainicio=<%=fechai%>&Nit=<%=propietario%>&custodia=<%=custodia%>&aval=<%=aval%>&tipo_negocio=<%=tipo_negocio%>&remesas=<%=remesas%>&remesa=<%=Estado_Remesa%>'" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" style="cursor:hand" >
				<%}%>
				<%}else if(custodia.equals("1")  && aval.equals("0") && remesas.equals("0")){
				aval="0";
				custodia="1";
				if(Estado_Remesa.equals("0")){
				remesas="0";
				}else{
				remesas="1";
				}
				%>
					<img src="<%=BASEURL%>/images/botones/Custodia.gif"   height="21"  title='Custodia'  onClick="location.href='<%=CONTROLLER%>?estado=Fenalco&accion=Fintra&opcion=CalcularCustodia&numero_cheques=<%=ParNumero_Cheques%>&valor_desembolso=<%=ParValor_Negocio%>&forma_pago=<%=Forma_Pago%>&fechainicio=<%=fechai%>&Nit=<%=propietario%>&custodia=<%=custodia%>&aval=<%=aval%>&tipo_negocio=<%=tipo_negocio%>&remesas=<%=remesas%>&remesa=<%=Estado_Remesa%>'" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" style="cursor:hand">
					<img src="<%=BASEURL%>/images/botones/Aval.gif"   height="21"  title='Aval'  onClick="location.href='<%=CONTROLLER%>?estado=Fenalco&accion=Fintra&opcion=CalcularAval&numero_cheques=<%=ParNumero_Cheques%>&valor_desembolso=<%=ParValor_Negocio%>&forma_pago=<%=Forma_Pago%>&fechainicio=<%=fechai%>&Nit=<%=propietario%>&custodia=<%=custodia%>&aval=<%=aval%>&tipo_negocio=<%=tipo_negocio%>&remesas=<%=remesas%>&remesa=<%=Estado_Remesa%>'" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" style="cursor:hand">
				<%if(Estado_Remesa.equals("0")){%>
					<img src="<%=BASEURL%>/images/botones/Remesas.gif"   height="21"  title='Remesas'  onClick="location.href='<%=CONTROLLER%>?estado=Fenalco&accion=Fintra&opcion=CalcularRemesas&numero_cheques=<%=ParNumero_Cheques%>&valor_desembolso=<%=ParValor_Negocio%>&forma_pago=<%=Forma_Pago%>&fechainicio=<%=fechai%>&Nit=<%=propietario%>&custodia=<%=custodia%>&aval=<%=aval%>&tipo_negocio=<%=tipo_negocio%>&remesas=<%=remesas%>&remesa=<%=Estado_Remesa%>'" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" style="cursor:hand" >
				<%}%>
					
				 <%}else if(aval.equals("1") && custodia.equals("1") && remesas.equals("0")){
				aval="1";

				custodia="1";
				if(Estado_Remesa.equals("0")){
				remesas="0";
				}else{
				remesas="1";
				}
				%>  
					<img src="<%=BASEURL%>/images/botones/Custodia.gif"   height="21"  title='Custodia'  onClick="location.href='<%=CONTROLLER%>?estado=Fenalco&accion=Fintra&opcion=CalcularCustodia&numero_cheques=<%=ParNumero_Cheques%>&valor_desembolso=<%=ParValor_Negocio%>&forma_pago=<%=Forma_Pago%>&fechainicio=<%=fechai%>&Nit=<%=propietario%>&custodia=<%=custodia%>&aval=<%=aval%>&tipo_negocio=<%=tipo_negocio%>&remesas=<%=remesas%>&remesa=<%=Estado_Remesa%>'" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" style="cursor:hand">
					<img src="<%=BASEURL%>/images/botones/Aval.gif"   height="21"  title='Aval'  onClick="location.href='<%=CONTROLLER%>?estado=Fenalco&accion=Fintra&opcion=CalcularAval&numero_cheques=<%=ParNumero_Cheques%>&valor_desembolso=<%=ParValor_Negocio%>&forma_pago=<%=Forma_Pago%>&fechainicio=<%=fechai%>&Nit=<%=propietario%>&custodia=<%=custodia%>&aval=<%=aval%>&tipo_negocio=<%=tipo_negocio%>&remesas=<%=remesas%>&remesa=<%=Estado_Remesa%>'" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" style="cursor:hand">
				<%if(Estado_Remesa.equals("0")){%>
					<img src="<%=BASEURL%>/images/botones/Remesas.gif"   height="21"  title='Remesas'  onClick="location.href='<%=CONTROLLER%>?estado=Fenalco&accion=Fintra&opcion=CalcularRemesas&numero_cheques=<%=ParNumero_Cheques%>&valor_desembolso=<%=ParValor_Negocio%>&forma_pago=<%=Forma_Pago%>&fechainicio=<%=fechai%>&Nit=<%=propietario%>&custodia=<%=custodia%>&aval=<%=aval%>&tipo_negocio=<%=tipo_negocio%>&remesas=<%=remesas%>&remesa=<%=Estado_Remesa%>'" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" style="cursor:hand" >
				<%}%>
				<%}else if( aval.equals("0") && custodia.equals("0") && remesas.equals("1")){
				aval="0";
				custodia="0";
				if(Estado_Remesa.equals("0")){
				remesas="1";
				}else{
				remesas="1";
				}
				%>  
					<img src="<%=BASEURL%>/images/botones/Custodia.gif"   height="21"  title='Custodia'  onClick="location.href='<%=CONTROLLER%>?estado=Fenalco&accion=Fintra&opcion=CalcularCustodia&numero_cheques=<%=ParNumero_Cheques%>&valor_desembolso=<%=ParValor_Negocio%>&forma_pago=<%=Forma_Pago%>&fechainicio=<%=fechai%>&Nit=<%=propietario%>&custodia=<%=custodia%>&aval=<%=aval%>&tipo_negocio=<%=tipo_negocio%>&remesas=<%=remesas%>&remesa=<%=Estado_Remesa%>'" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" style="cursor:hand">
					<img src="<%=BASEURL%>/images/botones/Aval.gif"   height="21"  title='Aval'  onClick="location.href='<%=CONTROLLER%>?estado=Fenalco&accion=Fintra&opcion=CalcularAval&numero_cheques=<%=ParNumero_Cheques%>&valor_desembolso=<%=ParValor_Negocio%>&forma_pago=<%=Forma_Pago%>&fechainicio=<%=fechai%>&Nit=<%=propietario%>&custodia=<%=custodia%>&aval=<%=aval%>&tipo_negocio=<%=tipo_negocio%>&remesas=<%=remesas%>&remesa=<%=Estado_Remesa%>'" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" style="cursor:hand">
				<%if(Estado_Remesa.equals("0")){%>
					<img src="<%=BASEURL%>/images/botones/Remesas.gif"   height="21"  title='Remesas'  onClick="location.href='<%=CONTROLLER%>?estado=Fenalco&accion=Fintra&opcion=CalcularRemesas&numero_cheques=<%=ParNumero_Cheques%>&valor_desembolso=<%=ParValor_Negocio%>&forma_pago=<%=Forma_Pago%>&fechainicio=<%=fechai%>&Nit=<%=propietario%>&custodia=<%=custodia%>&aval=<%=aval%>&tipo_negocio=<%=tipo_negocio%>&remesas=<%=remesas%>&remesa=<%=Estado_Remesa%>'" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" style="cursor:hand" >
				<%}%>
				<%}else if(aval.equals("1") && custodia.equals("0") && remesas.equals("1")){
				aval="1";
				custodia="0";
				if(Estado_Remesa.equals("0")){
				remesas="1";
				}else{
				remesas="1";
				}
				%>	
					<img src="<%=BASEURL%>/images/botones/Custodia.gif"   height="21"  title='Custodia'  onClick="location.href='<%=CONTROLLER%>?estado=Fenalco&accion=Fintra&opcion=CalcularCustodia&numero_cheques=<%=ParNumero_Cheques%>&valor_desembolso=<%=ParValor_Negocio%>&forma_pago=<%=Forma_Pago%>&fechainicio=<%=fechai%>&Nit=<%=propietario%>&custodia=<%=custodia%>&aval=<%=aval%>&tipo_negocio=<%=tipo_negocio%>&remesas=<%=remesas%>&remesa=<%=Estado_Remesa%>'" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" style="cursor:hand">
					<img src="<%=BASEURL%>/images/botones/Aval.gif"   height="21"  title='Aval'  onClick="location.href='<%=CONTROLLER%>?estado=Fenalco&accion=Fintra&opcion=CalcularAval&numero_cheques=<%=ParNumero_Cheques%>&valor_desembolso=<%=ParValor_Negocio%>&forma_pago=<%=Forma_Pago%>&fechainicio=<%=fechai%>&Nit=<%=propietario%>&custodia=<%=custodia%>&aval=<%=aval%>&tipo_negocio=<%=tipo_negocio%>&remesas=<%=remesas%>&remesa=<%=Estado_Remesa%>'" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" style="cursor:hand">
				<%if(Estado_Remesa.equals("0")){%>
					<img src="<%=BASEURL%>/images/botones/Remesas.gif"   height="21"  title='Remesas'  onClick="location.href='<%=CONTROLLER%>?estado=Fenalco&accion=Fintra&opcion=CalcularRemesas&numero_cheques=<%=ParNumero_Cheques%>&valor_desembolso=<%=ParValor_Negocio%>&forma_pago=<%=Forma_Pago%>&fechainicio=<%=fechai%>&Nit=<%=propietario%>&custodia=<%=custodia%>&aval=<%=aval%>&tipo_negocio=<%=tipo_negocio%>&remesas=<%=remesas%>&remesa=<%=Estado_Remesa%>'" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" style="cursor:hand" >
				<%}%>
				<%}else if(custodia.equals("1")  && aval.equals("0") && remesas.equals("1")){
				aval="0";
				custodia="1";
				if(Estado_Remesa.equals("0")){
					remesas="1";
				}else{
					remesas="1";
				}
				%>
					<img src="<%=BASEURL%>/images/botones/Custodia.gif"   height="21"  title='Custodia'  onClick="location.href='<%=CONTROLLER%>?estado=Fenalco&accion=Fintra&opcion=CalcularCustodia&numero_cheques=<%=ParNumero_Cheques%>&valor_desembolso=<%=ParValor_Negocio%>&forma_pago=<%=Forma_Pago%>&fechainicio=<%=fechai%>&Nit=<%=propietario%>&custodia=<%=custodia%>&aval=<%=aval%>&tipo_negocio=<%=tipo_negocio%>&remesas=<%=remesas%>&remesa=<%=Estado_Remesa%>'" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" style="cursor:hand">
					<img src="<%=BASEURL%>/images/botones/Aval.gif"   height="21"  title='Aval'  onClick="location.href='<%=CONTROLLER%>?estado=Fenalco&accion=Fintra&opcion=CalcularAval&numero_cheques=<%=ParNumero_Cheques%>&valor_desembolso=<%=ParValor_Negocio%>&forma_pago=<%=Forma_Pago%>&fechainicio=<%=fechai%>&Nit=<%=propietario%>&custodia=<%=custodia%>&aval=<%=aval%>&tipo_negocio=<%=tipo_negocio%>&remesas=<%=remesas%>&remesa=<%=Estado_Remesa%>'" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" style="cursor:hand">
				<%if(Estado_Remesa.equals("0")){%>
					<img src="<%=BASEURL%>/images/botones/Remesas.gif"   height="21"  title='Remesas'  onClick="location.href='<%=CONTROLLER%>?estado=Fenalco&accion=Fintra&opcion=CalcularRemesas&numero_cheques=<%=ParNumero_Cheques%>&valor_desembolso=<%=ParValor_Negocio%>&forma_pago=<%=Forma_Pago%>&fechainicio=<%=fechai%>&Nit=<%=propietario%>&custodia=<%=custodia%>&aval=<%=aval%>&tipo_negocio=<%=tipo_negocio%>&remesas=<%=remesas%>&remesa=<%=Estado_Remesa%>'" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" style="cursor:hand" >
				<%}%>
				 <%}else if(aval.equals("1") && custodia.equals("1") && remesas.equals("1")){
				aval="1";
				custodia="1";
				if(Estado_Remesa.equals("0")){
					remesas="1";
				}else{
					remesas="1";
				}
				%>  
					<img src="<%=BASEURL%>/images/botones/Custodia.gif"   height="21"  title='Custodia'  onClick="location.href='<%=CONTROLLER%>?estado=Fenalco&accion=Fintra&opcion=CalcularCustodia&numero_cheques=<%=ParNumero_Cheques%>&valor_desembolso=<%=ParValor_Negocio%>&forma_pago=<%=Forma_Pago%>&fechainicio=<%=fechai%>&Nit=<%=propietario%>&custodia=<%=custodia%>&aval=<%=aval%>&tipo_negocio=<%=tipo_negocio%>&remesas=<%=remesas%>&remesa=<%=Estado_Remesa%>'" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" style="cursor:hand">
					<img src="<%=BASEURL%>/images/botones/Aval.gif"   height="21"  title='Aval'  onClick="location.href='<%=CONTROLLER%>?estado=Fenalco&accion=Fintra&opcion=CalcularAval&numero_cheques=<%=ParNumero_Cheques%>&valor_desembolso=<%=ParValor_Negocio%>&forma_pago=<%=Forma_Pago%>&fechainicio=<%=fechai%>&Nit=<%=propietario%>&custodia=<%=custodia%>&aval=<%=aval%>&tipo_negocio=<%=tipo_negocio%>&remesas=<%=remesas%>&remesa=<%=Estado_Remesa%>'" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" style="cursor:hand">
				<%if(Estado_Remesa.equals("0")){%>
					<img src="<%=BASEURL%>/images/botones/Remesas.gif"   height="21"  title='Remesas'  onClick="location.href='<%=CONTROLLER%>?estado=Fenalco&accion=Fintra&opcion=CalcularRemesas&numero_cheques=<%=ParNumero_Cheques%>&valor_desembolso=<%=ParValor_Negocio%>&forma_pago=<%=Forma_Pago%>&fechainicio=<%=fechai%>&Nit=<%=propietario%>&custodia=<%=custodia%>&aval=<%=aval%>&tipo_negocio=<%=tipo_negocio%>&remesas=<%=remesas%>&remesa=<%=Estado_Remesa%>'" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" style="cursor:hand" >
				<%}%>
				<%}%>    
                &nbsp;&nbsp;
				<img src="<%=BASEURL%>/images/botones/regresar.gif" name="c_salir" onClick="location.href='<%=BASEURL%>/jsp/fenalco/liquidadores/inicio_liquidador.jsp';" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" align="absmiddle" style="cursor:hand ">
                <img src="<%=BASEURL%>/images/botones/salir.gif"  name="imgsalir"  onMouseOver="botonOver(this);" onClick="parent.close();" onMouseOut="botonOut(this);" style="cursor:hand">
				
          </td> 
        </tr>
		<tr>
			<td colspan="2" align="center"><p>&nbsp;</p>
		    <form name="form1" method="post" action="<%=CONTROLLER%>?estado=Negocios&accion=Insert">
				<input name="nit" type="hidden" value="<%=(String)session.getAttribute("nitcm")%>">
				<input name="vrnegocio" type="hidden" value="<%=ParValor_Negocio%>">
				<input name="nodocs" type="hidden" value="<%=Numero_Cheques%>">
				<input name="vrdesem" type="hidden" value="<%=Desembolso%>">
				<input name="vraval" type="hidden" value="<%=Tasa*100%>">
				<input name="vrcust" type="hidden" value="<%=Custodia_Cheque%>">
				<input name="rem" type="hidden" value="<%=Estado_Remesa%>">
				<input name="porcrem" type="hidden" value="<%=porrem%>">
				<input name="cust1" type="hidden" value="<%=custodia%>">
				<input name="aval1" type="hidden" value="<%=aval%>">
				<input name="formpago" type="hidden" value="<%=Forma_Pago%>">
				<input name="tneg" type="hidden" value="<%=tipo_negocio%>">
				<input name="codtab" type="hidden" value="<%=model.FenalcoFintraSvc.getcodtab()%>">
				<input name="remesas" type="hidden" value="<%=remesas%>">
				<input name="fechaneg" type="hidden" value="<%=fechai%>">
				<input name="nitp" type="hidden" value="<%=propietario%>">
				<input name="tpag" type="hidden" value="<%=(Valor_Cheque*Numero_Cheques)%>">
				<%
				//System.out.println("nit por aca = "+(String)session.getAttribute("nitcm"));
				%>
			<p><img src="<%=BASEURL%>/images/botones/Aceptar_neg.gif"  name="aceptarneg"  onMouseOver="botonOver(this);" onClick="javascript:form1.submit()" onMouseOut="botonOut(this);" style="cursor:hand"></p>
			</form>
			
			</td>
		</tr>
  </table>
  </div>
    </div>
    </body>
</html>