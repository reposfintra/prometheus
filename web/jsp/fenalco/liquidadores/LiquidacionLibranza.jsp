<%-- 
    Document   : formulario_libranza
    Created on : 14/03/2016, 03:32:50 PM
    Author     : hcuello 
--%>

<%@page import="com.tsp.util.Util"%>
<%@page import="com.tsp.operation.model.beans.DocumentosNegAceptado"%>
<%@page import="java.util.ArrayList"%>
<%@page import="com.tsp.operation.controller.FormularioLibranzaAction"%>
<%@page import="com.tsp.operation.model.beans.Usuario"%>
<%@page contentType="text/html"%>
<%@page session="true"%>
<%@page errorPage="/error/ErrorPage.jsp"%>
<%@ taglib uri="http://jakarta.apache.org/taglibs/input-0.90" prefix="input" %>

<%
    //Se reciben los parametros del liquidador
    Usuario usuario = (Usuario) session.getAttribute("Usuario");

    String codneg = request.getParameter("codneg");
    String numsolc = request.getParameter("numsolc");
    String fechanegocio = request.getParameter("fechanegocio").substring(0, 10);
    String fechaliqui = request.getParameter("fechaliqui").substring(0, 10);

    String fechatrans = request.getParameter("fechatrans");
    String convid = request.getParameter("convid");
    String estadoneg = request.getParameter("state");
    String nit = request.getParameter("nit");
    String tneg = request.getParameter("tneg");
    String consulta = request.getParameter("consulta") != null ? request.getParameter("consulta") : "";
    String vista = request.getParameter("vista") != null ? request.getParameter("vista") : "";
    int numcuotas = Integer.parseInt(request.getParameter("numcuotas"));
    int fpago = Integer.parseInt(request.getParameter("fpago"));
    double valornegocio = Double.parseDouble(request.getParameter("valornegocio"));
    double tasainteres = Double.parseDouble(request.getParameter("tasainteres"));
    double cat = Double.parseDouble(request.getParameter("cat"));
    double capacitacion = Double.parseDouble(request.getParameter("capacitacion"));
    double central = Double.parseDouble(request.getParameter("central"));
    double tcuota = 0;
    double tcapital = 0;
    double tcapacitacion = 0;
    double tseguro = 0;
    double tinteres = 0;
    double tcat = 0;
    double cuotaManejo = 0;
    double aval = 0;
    String negocio = "";

    FormularioLibranzaAction f = new FormularioLibranzaAction();
    f.init(usuario);
    ArrayList<DocumentosNegAceptado> liquidacion = f.buscarLiquidacion(codneg);

%>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
        <title>Liquidacion de negocios Libranza</title>
        <link href="/fintra/css/estilostsp.css" rel="stylesheet" type="text/css">
    </head>
    <body>

        <div id="capaCentral" style="position:relative; width:100%; height:75%; z-index:0; left: 0px; top: 0px; overflow: scroll;">
            <div id="seleccion">
            <table align="center" width="81%" border="0" cellpadding='0' cellspacing='0'>
                
                <tr>
                    <td >
                        <table align="center" width="100%" cellpadding='0' cellspacing='0' class="tablaInferior" border="1" >

                            <tr class="fila">
                                <td colspan = "2" align="center">
                                    <p><b>Información del Negocio</b></p>                                        
                                </td>
                            </tr>

                            <tr class="filagris">
                                <td width='50%'>
                                    <p><b>Valor del negocio</b></p>
                                </td>
                                <td width='50%'>
                                    <p ><%=Util.customFormat(valornegocio)%></p>
                                </td>
                            </tr>
                            <tr class="filaazul">
                                <td >
                                    <p><b>Tasa Interes</b></p>
                                </td>
                                <td>
                                    <p ><%=tasainteres%> %</p>
                                </td>
                            </tr>
                            <tr class="filagris">
                                <td >
                                    <p><b>No.Cuotas</b></p>
                                </td>
                                <td>
                                    <p ><%=numcuotas%></p>
                                </td>
                            </tr>
                            <tr class="filaazul">
                                <td >
                                    <p><b>Porcentaje Comisi&oacute;n Ley mipyme</b></p>
                                </td>
                                <td>
                                    <p ><%=cat%> %</p>
                                </td>
                            </tr>
                            <tr class="filagris">
                                <td >
                                    <p><b>Valor Central</b></p>
                                </td>
                                <td>
                                    <p ><%=Util.customFormat(central)%></p>
                                </td>
                            </tr>
                            <tr class="filaazul">
                                <td >
                                    <p><b>Capacitaci&oacute;n</b></p>
                                </td>
                                <td>
                                    <p ><%=Util.customFormat(capacitacion)%></p>
                                </td>
                            </tr>
                            <tr class="filagris">
                                <td >
                                    <p><b>Fecha negocio</b></p>
                                </td>
                                <td>
                                    <p ><%=fechanegocio%></p>
                                </td>
                            </tr>
                            <tr class="filaazul">
                                <td >
                                    <p><b>Fecha liquidaci&oacute;n</b></p>
                                </td>
                                <td>
                                    <p ><%=fechaliqui%></p>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
                
                <tr>
                    <td colspan='2'>
                        <br><br>
                    </td>
                </tr>
                
                <tr>
                    <td width='40%'>
                        <table width="100%" cellpadding='0' cellspacing='0' class="tablaInferior" border="1">
                            <tr class="fila">
                                <td>
                                    <p align='center'><b>Fecha</b></p>
                                </td>
                                <td>
                                    <p align='center'><b>No. Cuota</b></p>
                                </td>
                                <td>
                                    <p align='center'><b>Saldo Inicial</b></p>
                                </td>
                                <td>
                                    <p align='center'><b>Valor Cuota</b></p>
                                </td>
                                <td>
                                    <p align='center'><b>Capital</b></p>
                                </td>
                                <td>
                                    <p align='center'><b>Inter&eacute;s</b></p>
                                </td>
                                <td>
                                    <p align='center'><b>Capacitaci&oacute;n</b></p>
                                </td>
                                <td>
                                    <p align='center'><b>Comisi&oacute;n Ley mipyme</b></p>
                                </td>
                                <td>
                                    <p align='center'><b>Intermediaci&oacute;n</b></p>
                                </td>
                                <td>
                                    <p align='center'><b>Seguro</b></p>
                                </td>
                                <td>
                                    <p align='center'><b>Aval</b></p>
                                </td>
                                <td>
                                    <p align='center'><b>Saldo Final</b></p>
                                </td>
                            </tr>
                            <% for(int i=0;i<liquidacion.size();i++){
                                    negocio=liquidacion.get(i).getCod_neg();
                                    tcuota+=liquidacion.get(i).getValor();
                                    tcapital += liquidacion.get(i).getCapital();
                                    tcapacitacion += liquidacion.get(i).getCapacitacion();
                                    tseguro += liquidacion.get(i).getSeguro();
                                    tinteres += liquidacion.get(i).getInteres();
                                    tcat +=liquidacion.get(i).getCat();
                                    aval +=liquidacion.get(i).getAval();
                                    cuotaManejo +=liquidacion.get(i).getCuota_manejo();
                            %>
                             <tr class="filaazul">
                                <td>
                                    <p align='center'><%=liquidacion.get(i).getFecha().substring(0, 10)%></p>
                                </td>
                                <td>
                                    <p align='center'><%=liquidacion.get(i).getItem()%></p>
                                </td>
                                <td>
                                    <p align='center'><%=Util.customFormat(liquidacion.get(i).getSaldo_inicial())%></p>
                                </td>
                                <td>
                                    <p align='center'><%=Util.customFormat(liquidacion.get(i).getValor())%></p>
                                </td>
                                <td>
                                    <p align='center'><%=Util.customFormat(liquidacion.get(i).getCapital())%></p>
                                </td>
                                <td>
                                    <p align='center'><%=Util.customFormat(liquidacion.get(i).getInteres())%></p>
                                </td>
                                <td>
                                    <p align='center'><%=Util.customFormat(liquidacion.get(i).getCapacitacion())%></p>
                                </td>
                                <td>
                                    <p align='center'><%=Util.customFormat(liquidacion.get(i).getCat())%></p>
                                </td>
                                <td>
                                    <p align='center'><%=Util.customFormat(liquidacion.get(i).getCuota_manejo())%></p>
                                </td>
                                <td>
                                    <p align='center'><%=Util.customFormat(liquidacion.get(i).getSeguro())%></p>
                                </td>
                                <td>
                                    <p align='center'><%=Util.customFormat(liquidacion.get(i).getAval())%></p>
                                </td>
                                <td>
                                    <p align='center'><%=Util.customFormat(liquidacion.get(i).getSaldo_final())%></p>
                                </td>
                            </tr>

                            <%}%>

                            <tr >
                                <td> </td>
                                <td> </td>
                                <td> </td>
                                <td align="center" class="fila style1"><%=Util.customFormat( tcuota)%></td>
                                <td align="center" class="fila style1"><%=Util.customFormat( tcapital)%></td>
                                <td align="center" class="fila style1"><%=Util.customFormat( tinteres)%></td>
                                <td align="center" class="fila style1"><%=Util.customFormat( tcapacitacion)%></td>
                                <td align="center" class="fila style1"><%=Util.customFormat( tcat)%></td>
                                <td align="center" class="fila style1"><%=Util.customFormat( cuotaManejo)%></td>
                                <td align="center" class="fila style1"><%=Util.customFormat( tseguro)%></td>
                                <td align="center" class="fila style1"><%=Util.customFormat( aval)%></td>
                                <td> </td>
                            </tr>
                        </table>
                    </td>

                </tr>                

            </table>
                  </div>  
        </div>
    </body>
</html>
