<!--
- Autor : Ing. Roberto Rocha	
- Date  : 27 de AGOSTO de 2007
- Copyrigth Notice : Fintravalores S.A. S.A
-->
<%--
-@(#)
--Descripcion : Pagina JSP, que maneja la busqueda de las condicones de los negocios.
--%>

<%-- Declaracion de librerias--%>
<%@page contentType="text/html"%>
<%@ page session="true"%>
<%@ page errorPage="../error/error.jsp"%>
<%@page import="com.tsp.operation.model.beans.*, java.util.*"%>
<%@ include file="/WEB-INF/InitModel.jsp"%>
<%@page import="com.tsp.operation.model.*"%>
<%@page import="com.tsp.util.*"%>
<%@ taglib uri="http://jakarta.apache.org/taglibs/input-0.90" prefix="input" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01//EN" "http://www.w3.org/TR/html4/strict.dtd">
<html>
<head><title>Transferencias Bancarias</title>
<%	
  String path = application.getRealPath(model.menuService.getRealUrl(request.getRequestURI()));
  String datos[] = model.menuService.getContenidoMenu(BASEURL,request.getRequestURI(),path);
%>

        <link href="<%= BASEURL%>/css/estilostsp.css" rel='stylesheet'/>
        <link href="<%= BASEURL%>/css/letras.css" rel='stylesheet'/>
        <link href="<%=BASEURL%>/css/default.css"       rel="stylesheet" type="text/css"/>
        <script type='text/javascript' src="<%= BASEURL%>/js/boton.js"></script>
        <script type='text/javascript' src="<%= BASEURL %>/js/tranferencias.js"></script>

        <script type='text/javascript' src="<%= BASEURL%>/js/captaciones.js"></script>
        <script type='text/javascript' src="<%=BASEURL%>/js/validar.js"></script>
         <link href="<%= BASEURL%>/css/estilosFintra.css" rel='stylesheet'/>





         <link href="<%= BASEURL%>/css/estilostsp.css" rel="stylesheet" type="text/css">
        <link href="<%= BASEURL%>/css/jquery/jquery-ui/jquery-ui.css" rel="stylesheet" type="text/css"/>
        <link href="<%= BASEURL%>/css/jquery/jquery.jqGrid/ui.jqgrid.css" rel="stylesheet" type="text/css"/>

        <script type='text/javascript' src="<%= BASEURL%>/js/boton.js"></script>
        <script type='text/javascript' src="<%= BASEURL%>/js/jquery/jquery-ui/jquery.min.js"></script>
        <script type='text/javascript' src="<%= BASEURL%>/js/jquery/jquery-ui/jquery.ui.min.js"></script>
        <script type='text/javascript' src="<%= BASEURL%>/js/jquery/jquery.jqGrid/js/grid.locale-es.js"></script>
        <script type='text/javascript' src="<%= BASEURL%>/js/jquery/jquery.jqGrid/js/jquery.jqGrid.min.js"></script>
<script>



            function seleccionar(){
                var keys = jQuery("#Lista").jqGrid('getGridParam','selarrrow');
                if(keys != ""){
                    var ind = 0;
                    var param="";
                    var ArrayKeys = keys.toString().split(",");
                    $.each(ArrayKeys, function (index, key) {
                        param += "&nit"+ind+"="+jQuery("#Lista").getCell(key,'cedula_cuenta');
                        ind++;
                    });
                    param += "&tam="+ind;                    
                    llamarasincrono_v2('<%=CONTROLLER%>?estado=Negocios&accion=Transf&evento=1&nit='+param+'&tam='+ind+'&val='+$("#bco").val()+'&fechaini='+$("#fechaInicio").val()+'&fechafin='+$("#fechaFin").val()+'','tabla')
                      $('#VentanaCuentas').dialog('close');
                }else{
                    alert("Debe seleccionar la placa que desea asignar al transpote");
                }
            }










 $(document).ready(function(){
                $("#VentanaCuentas").dialog({
                    autoOpen: false,
                    height: "auto",
                    width: '35%',
                    modal: true,
                    autoResize: true,
                    resizable: false,
                    position: "center"
                });
            });
 
 
 function filtrar()
 {
     var check=$("input[name='radio']:checked").val(); 
     var fecha1=$("#fechaInicio").val();
     var fecha2=$("#fechaFin").val();
    if(check == "2" && fecha1 != "" && fecha2 != "" ){
	  var url = $("#BASEURL").val()+"/jsp/fenalco/negocios/filtroTransProv.jsp";
          $("#VentanaCuentas").html("");
	 $("#VentanaCuentas").load(url).dialog("open");
    }else{
        
        alert("debe seleccionar la opcion proveedor e ingresar una fecha...")
 }
 }
 

</script>
   <style type="text/css">
            div.ui-dialog{
                font-size:12px;
            }
            

       </style>
</head>
<body >
<div id="capaSuperior" style="position:absolute; width:100%; height:100px; z-index:0; left: 0px; top: 0px;">
<jsp:include page="/WEB-INF/toptsp.jsp?encabezado=Transferencias Bancarias"/>
</div>
<%String list=(String)request.getParameter("list");
%>
<div id="capaCentral" style="position:absolute; width:100%; height:85%; z-index:0; left: 0px; top: 100px; overflow: scroll;"> 
<form name="form2" method="post" action="<%=CONTROLLER%>?estado=Negocios&accion=Transf&evento=3" id="form2" >
    <table width="510" border="2" align="center" >
      <tr>
        <td><table width="100%" align="center"  class="tablaInferior">
			<tr>
				<td width="205" class="subtitulo1">Consultar Negocios </td>
                <td width="205" class="barratitulo" colspan="2"><img src="<%=BASEURL%>/images/titulo.gif" width="32" height="20" align="left"><%=datos[0]%>
                 <input name="BASEURL" type="hidden" id="BASEURL" value="<%=BASEURL%>"></td>
			</tr>
            <tr class="fila">
              <td width="215" align="left" >Escoja el banco girador </td>
              <td width="215">
			  <%
			  Vector vb=new Vector();
			  model.Negociossvc.listb("todos");
			  vb=model.Negociossvc.getListNeg();
			  %>
			    <select name="bco" id="bco" onChange="llamarasincrono('<%=CONTROLLER%>?estado=Negocios&accion=Transf&evento=1&val='+bco.value+'','tabla')">
					<option value="...">Seleccione</option>
					<%for(int j=0;j<vb.size();j++){
						Negocios  neg = (Negocios)vb.get(j);%>
						<option value="<%=neg.getNom_cli()%>,<%=neg.getCod_cli()%>,<%=neg.getCod_tabla()%>"><%=neg.getEstado()%></option>
                                                <%if(neg.getNom_cli().equals("BANCOLOMBIA")){%>
                                                    <option value="BANCOLOMBIAPAB,<%=neg.getCod_cli()%>,<%=neg.getCod_tabla()%>">BANCOLOMBIA PAB <%=neg.getCod_cli()%> <%=neg.getCod_tabla()%></option>                                                
                                                <%}%>
					<%}%>
	          </select>
			   </td>
               <td></td>
            </tr>
            <tr class="fila">
              <td align="left" >Filtrar Por Proveedor</td>
                <td>
                    <label>Fecha Inicial</label> <input name="fechaInicio" type='text' class="textbox" id="fechaInicio" style='width:80px'  readonly value="">
                    <a href="javascript:void(0);" onFocus="if(self.gfPop)gfPop.fPopCalendar(document.form2.fechaInicio);return false;" hidefocus>
                        <img src="<%=BASEURL%>/js/Calendario/cal.gif" width="16" height="16" border="0" alt="De click aqu&iacute; para ver el calendario."></a>
                        &nbsp;&nbsp;
                    <label>Fecha Final</label> <input name="fechaFin" type='text' class="textbox" id="fechaFin" style='width:80px'  readonly value="">
                    <a href="javascript:void(0);" onFocus="if(self.gfPop)gfPop.fPopCalendar(document.form2.fechaFin);return false;" hidefocus>
                        <img src="<%=BASEURL%>/js/Calendario/cal.gif" width="16" height="16" border="0" alt="De click aqu&iacute; para ver el calendario."></a>


                </td>
                <td>
                    
                    <img src="<%=BASEURL%>/images/botones/iconos/lupa.gif" onClick="filtrar();" alt="" width="22" height="20" align="left">
                        
                </td>
                      
            </tr>
            <tr class="fila">
                <td align="left" colspan="3" >
                    <div id="busquedad" align="center">
                        <input type="radio"  name="radio"  value="1"  checked/> <label >Banco</label>
                        &nbsp;&nbsp;&nbsp;
                        <input type="radio" name="radio"  value="2" /> <label >Proveedor</label>
                  </div>
              </td>
              
            </tr>
        </table></td>
      </tr>
    </table>
	<p>
	<div id="tabla"></div>
	<div id="imgworking" align="center" style="visibility:hidden"><img src="<%=BASEURL%>/images/cargando.gif"></div>
<div align="center"><img src="<%=BASEURL%>/images/botones/aceptar.gif" style="cursor:hand" title="Buscar Proveedor" name="buscar"  onClick="send(form2)" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);">&nbsp;
<img src="<%=BASEURL%>/images/botones/salir.gif"  name="regresar" style="cursor:hand" title="Salir al Menu Principal" onClick="javascript:window.close()" onMouseOver="botonOver(this);"   onMouseOut="botonOut(this);"  ></div>		
</form>
</div>

<div id="VentanaCuentas" title="Listado Cuentas" style=" display: none" ></div>
<iframe width=188 height=166 name="gToday:normal:agenda.js:gfPop:plugins.js" id="gToday:normal:agenda.js:gfPop:plugins.js" src="<%=BASEURL%>/js/Calendario/ipopeng.htm" scrolling="no" frameborder="0" style="visibility:visible; z-index:999; position:absolute; left:-500px; top:0px;"> </iframe>
</body>
</html>
