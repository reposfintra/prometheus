<%--
    Document   : NegocioTrazabilidad
    Created on : 05/12/2011, 04:43:44 PM
    Author     : ivargas
--%>
<%@page import="com.tsp.util.Util"%>
<%@page session="true"%>
<%@page import="java.util.*"%>
<%@page import="com.tsp.operation.model.*"%>
<%@page import="com.tsp.operation.model.beans.*"%>
<%@page import="com.tsp.operation.model.services.*"%>
<%@page contentType="text/html" pageEncoding="ISO-8859-1"%>
<%
            response.addHeader("Cache-Control", "no-cache");
            response.addHeader("Pragma", "No-cache");
            response.addDateHeader("Expires", 0);
            Usuario usuario = (Usuario) session.getAttribute("Usuario");
            String BASEURL = request.getContextPath();
            String CONTROLLER = BASEURL + "/controller";
            
			
			
	    String form = request.getParameter("form") != null ? request.getParameter("form") : "";
            NegocioTrazabilidadService serv = new NegocioTrazabilidadService(usuario.getBd());
            ArrayList<NegocioTrazabilidad> trazas = serv.buscarNegocioTrazabilidadRechazado(Integer.parseInt(form));

%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
        <link href="<%= BASEURL%>/css/estilostsp.css" rel='stylesheet'>
        <script type='text/javascript' src="<%= BASEURL%>/js/boton.js"></script>
        <script type='text/javascript' src="<%= BASEURL%>/js/prototype.js"></script>
        <script type='text/javascript' src="<%= BASEURL%>/js/negocioTrazabilidad.js"></script>
        <title>Trazabilidad del Negocio</title>
    </head>
    <body onload="inicializar('<%=CONTROLLER%>', '<%= BASEURL%>');">
        
        <%-- aqui cambio de linea --%>        
     <div id="capaCentral" style="position:absolute; width:100%; height:100%; z-index:0; left: 0px; top: 0px; overflow:auto;">
         <form id="formulario" name="formulario" action="" method="post">
             <%-- aqui terminan --%>
             
                <table align="center" width="90%" border="0">
                    <tbody>
                        <tr>
                            <td>
                            <%if(trazas.size()>0)
                             {%>                                                     
                                <table width="99%" border="1" style="border-collapse: collapse;" id="traza" >
                                    <thead>

                                        <tr class="subtitulo1">
                                            <th width="2%">Item</th>
                                            <th width="8%">Formulario</th>
                                            <th width="10%">Actividad</th>
                                            <th width="10%">Usuario</th>
                                            <th width="10%"class="date-iso">Fecha</th>
                                            <th width="10%">Negocio</th>
                                            <th width="30%">Comentarios</th>
                                            <th width="10%" >Concepto</th>
                                            <th width="10%">Causal</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                              <%
                              for (int i = 0; i < trazas.size(); i++) {%>
                                        <tr class="fila">
                                            <td width="2%" align="center"><%=i + 1%></td>
                                            <td width="8%" align="center"><%=trazas.get(i).getNumeroSolicitud()%></td>
                                            <td width="10%" align="center"><%=trazas.get(i).getActividad()%></td>
                                            <td width="10%" align="center"> <%=trazas.get(i).getUsuario()%></td>
                                            <td width="10%" align="center"><%=trazas.get(i).getFecha()%></td>
                                            <td width="10%" align="center"><%=trazas.get(i).getCodNeg()%></td>
                                            <td width="30" align="center"><%=trazas.get(i).getComentarios()%></td>
                                            <td width="10%" align="center"><%=trazas.get(i).getConcepto()%></td>
                                            <td width="10%" align="center"><%=trazas.get(i).getCausal()%></td>
                                        </tr>
                                            </tbody>
                                </table>
                                        <%}
                            }else{                                
                                
                                                                
                                   trazas = serv.buscarNegocioTrazabilidadDatacredito(Integer.parseInt(form));
                                   
                                   
                                    if(trazas.size()>0)
                             {%>
                            
                            
                                <table width="99%" border="1" style="border-collapse: collapse;" id="traza" >
                                    <thead>

                                        <tr class="subtitulo1">
                                            <th width="65">Item</th>
                                            <th width="128">Formulario</th>
                                            <th width="153">Actividad</th>
                                            <th width="153"class="date-iso">Fecha</th>
                                            <th width="427">Comentarios</th>
                                            <th width="132" >Concepto</th>
                                            <th width="154">Causal</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                              <%
                              for (int i = 0; i < trazas.size(); i++) {%>
                                        <tr class="fila">
                                            <td width="65" align="center"><%=i + 1%></td>
                                            <td width="128" align="center"><%=trazas.get(i).getNumeroSolicitud()%></td>
                                            <td width="153" align="center"><%=trazas.get(i).getActividad()%></td>
                                            <td width="153" align="center"><%=trazas.get(i).getFecha()%></td>
                                            <td width="427" align="center"><%=trazas.get(i).getComentarios()%></td>
                                            <td width="132" align="center"><%=trazas.get(i).getConcepto()%></td>
                                            <td width="154" align="center"><%=trazas.get(i).getCausal()%></td>
                                        </tr>
                     
                                        <%}%>
                                 </tbody>
                                </table>
                            <%}
                                   else
                                   {
                                   SiniestrosService fserv = new SiniestrosService(usuario.getBd());
                                   ArrayList<Siniestros> siniestros = fserv.ListarSiniestro(form);


                                    if(siniestros.size()>0)
                                     {%>
                            <table width="99%" border="1" style="border-collapse: collapse;" id="traza2" >
                              <thead>
                                <tr class="subtitulo1">
                                  <th width="31">Item</th>
                                  <th width="30">Tipo</th>
                                  <th width="188">Nombre</th>
                                  <th width="78"class="date-iso">Direccion</th>
                                  <th width="132">Telefono</th>
                                  <th width="81" >Celular</th>
                                  <th width="120">Banco</th>
                                  <th width="89">N&deg; Cuenta</th>
                                  <th width="100">N&deg; Cheque</th>
                                  <th width="107">Valor Cheque</th>
                                  <th width="111">Fecha Consig.</th>
                                  <th width="115">Fec. Recuperacion</th>
                                  <th width="10">Dias Recup.</th
                                </tr>
                              </thead>
                              <tbody>
                                <%
                              for (int i = 0; i < siniestros.size(); i++) {%>
                                <tr class="fila">
                                  <td width="31" align="center"><%=i + 1%></td>
                                  <td width="30" align="center"><%=siniestros.get(i).getTipo() %></td>
                                  <td width="188" align="center"><%=siniestros.get(i).getNombre_siniestro() %></td>
                                  <td width="78" align="center"><%=siniestros.get(i).getDireccion() %></td>
                                  <td width="132" align="center"><%=siniestros.get(i).getTelefono1() %>  <%if(!siniestros.get(i).getTelefono2().equals("")){ %>  <%=" - "+siniestros.get(i).getTelefono2()%>  <%}%></td>
                                  <td width="81" align="center"><%=siniestros.get(i).getCelular() %></td>
                                  <td width="120" align="center"><%=siniestros.get(i).getBanco() %></td>
                                  <td width="89" align="center"><%=siniestros.get(i).getNumero_cuenta_siniestro() %></td>
                                  <td width="100" align="center"><%=siniestros.get(i).getNumero_cheuqe_siniestro() %></td>
                                  <td width="107" align="center"><%=Util.customFormat(siniestros.get(i).getValor_cheque_siniestro()) %></td>
                                  <td width="111" align="center"><%=siniestros.get(i).getFecha_consignacion_siniestro() %></td>
                                  <td width="115" align="center"><%=siniestros.get(i).getFecha_recuperacion() %></td>
                                  <td width="10" align="center"><%=siniestros.get(i).getDias_recuperacion()!=null?siniestros.get(i).getDias_recuperacion():"" %></td>
                                </tr>
                                <%}
                            }else{%>
                                <tr class="fila">
                                  <td  colspan="12">No hay registros</td>
                                </tr>
                                <%}%>
                              </tbody>
                            </table>
                        <%}
                                   





}%>
                                <br/>
                                <div align="center">

                                    <img alt="" src = "<%=BASEURL%>/images/botones/salir.gif" style = "cursor:pointer" name = "imgsalir" onclick = "Javascript:parent.closewin()" onmouseover="botonOver(this);" onmouseout="botonOut(this);">

                                </div>

                            </td>
                        </tr>
                    </tbody>
                </table>
            </form>
        </div>
    </body>
</html>

