<%-- 
    Document   : ver_negocios_StandBy
    Created on : 4/05/2016, 02:44:13 PM
    Author     : user
--%>

<%@page import="com.tsp.operation.model.beans.CmbGeneralScBeans"%>
<%@page import="java.util.ArrayList"%>
<%@page import="com.tsp.operation.model.services.NegocioTrazabilidadService"%>
<%@page import="com.tsp.operation.model.beans.Usuario"%>
<%@page contentType="text/html" pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<%
    Usuario usuario = (Usuario) session.getAttribute("Usuario");
    NegocioTrazabilidadService rqservice= new NegocioTrazabilidadService(usuario.getBd());
    ArrayList listaCombo =  rqservice.GetComboGenerico("SQL_OBTENER_UNIDAD_NEGOCIO1","id","descripcion","");
        
    String LoginUsuario = usuario.getLogin();

%>


<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
        <title>Negocios en StandBy</title>
        <link type="text/css" rel="stylesheet" href="./css/jquery/jquery-ui/jquery-ui-2.css"/>
        <link type="text/css" rel="stylesheet" href="./css/jquery/jquery.jqGrid/ui.jqgrid.css"/>        
        <script type="text/javascript" src="./js/jquery/jquery-ui/jquery.min.js"></script>
        <script type="text/javascript" src="./js/jquery/jquery-ui/jquery.ui.min.js"></script>            
        <script type="text/javascript" src="./js/jquery/jquery.jqGrid/js/grid.locale-es.js"></script>
        <script type="text/javascript" src="./js/jquery/jquery.jqGrid/js/jquery.jqGrid.min.js"></script>
        <link type="text/css" rel="stylesheet" href="./css/buttons/botonVerde.css"/>
        <link type="text/css" rel="stylesheet" href="./css/popup.css"/>
<!--    <link href="./css/style_azul.css" rel="stylesheet" type="text/css">-->
        <script type="text/javascript" src="./js/StandBy.js"></script>  
        
         <!--css logica de negocio-->
<!--     <link href="./css/asobancaria.css" rel="stylesheet" type="text/css">-->

        <!--jqgrid--> 
        <script type="text/javascript" src="./js/jquery/jquery.jqGrid-4.7.0/js/grid.locale-es.js"></script>
        <script type="text/javascript" src="./js/jquery/jquery.jqGrid-4.7.0/js/jquery.jqGrid.min.js"></script>
        <script type="text/javascript" src="./js/jquery/jquery.jqGrid-4.7.0/plugins/jquery.contextmenu.js"></script>
        <script type="text/javascript" src="./js/jquery/jquery.jqGrid-4.7.0/js/jquery.tabletojson.js"></script>  
    </head>
    <body>
        <div id="capaSuperior" style="position:absolute; width:100%; height:100px; z-index:0; left: 0px; top: 0px;">
            <jsp:include page="/toptsp.jsp?encabezado=Negocios en StandBy"/>
        </div>
        <div id="capaCentral" style="position:absolute; width:100%; height:100%; z-index:0; left: 0px; top: 160px; ">
            <br>
            <center>
                <table width="400" border="0" cellpadding="0" cellspacing="1" class="labels" id="tbl_hys">
                    
                    <tr>
                        <td colspan="2">
                            <fieldset>
                                
                                <legend class="labels"> FILTRO </legend>
                                
                                <table width="400" border="0" align="left" cellpadding="1" cellspacing="1" class="labels">
                                  <tr>
                                      
                                    <td width="300">
                                        <fieldset>
                                            <legend>UNIDADES DE NEGOCIO</legend>
                                            <table border="0" align="center" cellpadding="1" cellspacing="1" class="labels">
                                                <tr>
                                                    <td>
                                                        <select name="unidad_negocio" class="combo_180px" id="unidad_negocio">
                                                            <option value="" selected>< -Escoger- ></option><%
                                                            for (int i = 0; i < listaCombo.size(); i++) {
                                                                CmbGeneralScBeans CmbContenido = (CmbGeneralScBeans) listaCombo.get(i);%>
                                                                 <option value="<%=CmbContenido.getIdCmb() %>"><%=CmbContenido.getDescripcionCmb() %></option><%
                                                            }%>
                                                        </select>
                                                    </td>
                                                </tr>
                                            </table>
                                        </fieldset>
                                    </td>
                                    <td>
                                        
                                    </td>
                                       
                                    <td width="100">  
                                    <span  class="form-submit-button form-submit-button-simple_green_apple" id="filtrar" onclick="listarNegociosStandBy();" />Filtrar </span>
                                    
                                    </td>
                                  </tr>
                                </table>
                            </fieldset>
                        </td>
                    </tr>  
                    <td>
                        <br>                  
                    </td>  
                    <div id="box" style="top:281px; left:417px; position:absolute; z-index:1000; width: 43px; height: 29px;"></div>
                    <td>
                        <br>                  
                    </td>  
                </table>
                <table id="tabla_negocios_standby"></table>
                <div id="page_tabla_negocios_standby"></div>              
            </center>                  
            <!-- Dialogo ventana quitar StandBy> -->
            <div id="dialogAddComent" style="display:none;">       
                <br>                          
                <table border="0" align="center" cellpadding="1" cellspacing="1" class="labels">                      
                    <tr>
                        <td style="width: 20%"><span>Comentario:</span></td>   
                        <td style="width: 80%" colspan="4"><textarea id ="comment" name="comment" cols="55"  rows="4" maxlength="300" onchange="conMayusculas(this)"></textarea></td>                        
                    </tr>
                </table> 
            </div>
            <div id="dialogLoading" style="display:none;">
                <p  style="font-size: 12px;text-align:justify;" id="msj2">Texto </p> <br/>
                <center>
                    <img src="./images/cargandoCM.gif"/>
                </center>
            </div>
            <div id="dialogMsj" title="Mensaje" style="display:none;">
                <p style="font-size: 12.5px;text-align:justify;" id="msj" > Texto </p>
            </div>  
        </div>
        
        <div id="divSalidaEx" title="Exportacion" style=" display: block" >
                <p  id="msjEx" style=" display:  none"> Espere un momento por favor...</p>
                <center>
                    <img id="imgloadEx" style="position: relative;  top: 7px; display: none " src="./images/cargandoCM.gif"/>
                </center>
                <div id="respEx" style=" display: none"></div>
        </div> 
        
        
        <script type="text/javascript">
            listarNegociosStandBy();
        </script>
    </body>
</html>
