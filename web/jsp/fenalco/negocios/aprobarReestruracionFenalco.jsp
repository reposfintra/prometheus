<%-- 
    Document   : aprobarReestruracionFenalco
    Created on : 10/02/2015, 08:16:17 PM
    Author     : egonzalez
--%>

<%@page contentType="text/html" pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
        <link href="./css/reestructuracion.css" rel="stylesheet" type="text/css" >
        <link type="text/css" rel="stylesheet" href="./css/jquery/jquery-ui/jquery-ui.css" />
        <link href="./css/popup.css" rel="stylesheet" type="text/css">

        <script type="text/javascript" src="./js/jquery/jquery.jqGrid/js/jquery.min.js"></script>
        <script type="text/javascript" src="./js/jquery-ui-1.8.5.custom.min.js"></script>
        <script type="text/javascript" src="./js/jquery/jquery.jqGrid-4.7.0/js/grid.locale-es.js"></script>
        <script type="text/javascript" src="./js/jquery/jquery.jqGrid-4.7.0/js/jquery.jqGrid.min.js"></script>
        <script type="text/javascript" src="./js/jquery/jquery.jqGrid-4.7.0/js/jquery.tabletojson.js"></script>
        <script type="text/javascript" src="./js/AprobarRestructuracion.js"></script>
        <title>Aprobar</title>
    </head>
    <body>
        <div id="capaSuperior"  style="position:absolute; width:2320px; height:100px; z-index:0; left: 0px; top: 0px;">
            <jsp:include page="/WEB-INF/toptsp.jsp?encabezado=APROBAR NEGOCIOS"/>
        </div>
        <div id="capaCentral" style="position:absolute; width:1700px; height:100%; z-index:0; left: 0px; top: 100px; ">
            <div class="negociosap">
                <h3 style="width: 921px;">LISTA NEGOCIOS POR APROBAR</h3>
                <table id="tabla_negocios"></table>
                <div id="page_negocios"></div>
            </div>
            
            <div id="ventanaIzq" class="ventanaIzq">
                 <h3 style="width: 1288px;margin-left:2% ">RESUMEN SALDOS POR NEGOCIOS</h3>
                 <div class="izquierda">
                 <table id="tabla_saldos"></table><br>
                 <h3 style="width: 1288px;margin-left:0% ">RESUMEN PAGO CUOTA INICIAL</h3>
                 <table id="tabla_inicial"></table>
                 </div>
                 <div class="valorTotal" >
                    <input type="hidden" name="id_rop" id="id_rop" value="" />
                    <input type="hidden" name="estado" id="estado" value="N" />
                    <input type="hidden" name="permiso" id="permiso" value="" />
                    <fieldset style="border-radius: inherit;height:100px"><legend>Valor Reestructuracion</legend>
                        <p id="totalReestructuracion" class="simulador2">0.00</p>
                         <button id="aprobar" class="ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only" role="button" aria-disabled="false">
                                  <span class="ui-button-text">Aprobar Reestructuracion</span>
                         </button>
                         <button id="rechazar" class="ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only" role="button" aria-disabled="false">
                                  <span class="ui-button-text">Rechazar</span>
                         </button>
                         <button id="imprimir" class="ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only" role="button" aria-disabled="false">
                                  <span class="ui-button-text">imprimir</span>
                         </button>
                    </fieldset>
                </div> 
            </div>           

        </div>
        
        <div id="ver_liquidacion" title="Liquidacion" class="ventana">
                <center>
                    <table class="tablaLiquidador">

                        <tr>
                            <th align="left" colspan="2" class="ui-state-default"><strong> Datos Liquidacion</strong></th>
                            <th align="left" colspan="2" class="ui-state-default first"> <img width="32" height="20" align="left" src="/fintra/images/titulo.gif" alt=""> </th>
                        </tr>
                        <tr>
                            <th  class="ui-state-default">Tasa Interes M.V.</th>
                            <td  align="center" class="ui-widget-content first"><label id="interes"></label></td>
                            <th  class="ui-state-default first">Fecha negocio</th>
                            <td  align="center" class="ui-widget-content first"><label id="fneg"></label></td>
                        </tr>
                        <tr>
                            <th class="ui-state-default">No.Cuotas</th>
                            <td align="center" class="ui-widget-content first"><label id="cuota1"></label></td>
                            <th class="ui-state-default first">Remesa</th>
                            <td align="center" class="ui-widget-content first"><label id="remesa"></label></td>
                        </tr>
                        <tr>
                            <th class="ui-state-default">Valor Seguro</th>
                            <td align="center" class="ui-widget-content first"><label id="vlr_seguro"></label></td>
                            <th class="ui-state-default first">Valor del negocio</th>
                            <td align="center" class="ui-widget-content first"><label id="vlr_negocio"></label></td>
                        </tr>

                    </table>
                </center>
            <hr/>
                <div id="liquidador_credito">
                    <table id="tabla_liquidador_neg"></table>
                </div>
        </div>

        <div id="dialogo" class="ventana" title="Mensaje">
            <p  id="msj">texto </p>
        </div>
    </body>
</html>
