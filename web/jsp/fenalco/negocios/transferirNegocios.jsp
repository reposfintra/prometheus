<%-- 
    Document   : transferirNegocios
    Created on : 10/07/2014, 04:42:28 PM
    Author     : desarrollo
--%>

<%@ page import="com.tsp.operation.model.services.SeguimientoCarteraService"%>
<%@ page contentType="text/html"%>
<%@ page session="true"%>
<%@ page errorPage ="/error/ErrorPage.jsp"%>
<%@ page import="com.tsp.operation.model.beans.*, java.util.*"%>
<%@ include file="/WEB-INF/InitModel.jsp"%>
<%@ page import="com.tsp.operation.model.*"%>
<%@ page import="com.tsp.util.*"%>

<%   
     ArrayList listaCombo = model.Negociossvc.cargarUnidadesNegocio("GESTION FINANCIERA");

%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
        <title>Transferir Negocios</title>

        <link rel="stylesheet" type="text/css" href="<%=BASEURL%>/css/jCal/jscal2.css" />
        <link rel="stylesheet" type="text/css" href="<%=BASEURL%>/css/jCal/border-radius.css" />
        <link href="<%=BASEURL%>/css/estilostsp.css" rel="stylesheet" type="text/css">
        <link href="<%=BASEURL%>/css/default.css" rel="stylesheet" type="text/css">
        <link type="text/css" rel="stylesheet" href="<%=BASEURL%>/css/jquery/jquery-ui/jquery-ui.css"/>

        <link href="<%=BASEURL%>/css/popup.css" rel="stylesheet" type="text/css">
        <link type="text/css" rel="stylesheet" href="<%=BASEURL%>/css/buttons/botonRose.css"/>
        <link type="text/css" rel="stylesheet" href="<%=BASEURL%>/css/buttons/botonVerde.css"/>
        <link href="<%=BASEURL%>/css/style_azul.css" rel="stylesheet" type="text/css">
        <script type="text/javascript" src="<%=BASEURL%>/js/jCal/jscal2.js"></script>
        <script type="text/javascript" src="<%=BASEURL%>/js/jCal/lang/es.js"></script>

        <script type="text/javascript" src="<%=BASEURL%>/js/jquery-1.4.2.min.js"></script>
        <script language="JavaScript" type="text/javascript" src="<%=BASEURL%>/js/jquery-ui-1.8.5.custom.min.js"></script>
        <script type="text/javascript" src="<%=BASEURL%>/js/jquery/jquery.jqGrid/js/grid.locale-es.js"></script>
        <script type="text/javascript" src="<%=BASEURL%>/js/jquery/jquery.jqGrid/js/jquery.jqGrid.min.js"></script>
        <script type="text/javascript" src="<%=BASEURL%>/js/tranferencias.js"></script>


    </head>
    <body>
        <div id="capaSuperior" style="position:absolute; width:100%; height:100px; z-index:0; left: 0px; top: 0px;">
            <jsp:include page="/WEB-INF/toptsp.jsp?encabezado=Transferencias Bancarias"/>
        </div>
        <br>
        <div id="capaCentral" style="position:absolute; width:100%; height:85%; z-index:0; left: 0px; top: 100px; overflow: auto;">
            <form name="form2" method="post" action="<%=CONTROLLER%>?estado=Negocios&accion=Transf&evento=3" id="form2" >
                <input name="BASEURL" type="hidden" id="BASEURL" value="<%=BASEURL%>">
                <input name="CONTROLLER" type="hidden" id="CONTROLLER" value="<%=CONTROLLER%>">   
                <input type="hidden" name="pdfBuffer" id="pdfBuffer" value="" />
                <input type="hidden" name="fileName" id="fileName" value="TransferenciaNegocios" />
                <input type="hidden" name="fileType" id="fileType" value="" />
                <br/>
                <div  align="center" style="margin-top: 1%">
                    <table align="center" width="800px" border="0" cellpadding="2" cellspacing="5" class="labels" id="tbl_hys">
                        <tr>
                            <td width="40%" align="center" >
                                <fieldset>
                                    <legend>BUSCAR POR:</legend>   
                                    <table border="0" align="center" cellpadding="1" cellspacing="1" class="labels">
                                        <tr>
                                            <td align="left" colspan="3" >
                                                <div id="busquedad" >
                                                    <input type="checkbox" disabled  name="neg" id="neg" value="1" checked   /> <label style=" font-size: 12px">Unidad de Negocio</label>
                                                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                                    <input type="checkbox" name="prov" id="prov" value="2" /> <label style=" font-size: 12px">Proveedor</label>
                                                </div>
                                            </td>

                                        </tr>
                                    </table>
                                </fieldset>
                            </td>
                            <td width="25%">
                                <fieldset>
                                    <legend>UNIDADES DE NEGOCIO</legend>
                                    <table border="0" align="center" cellpadding="1" cellspacing="1" class="labels">
                                        <tr>
                                            <td>
                                                <select name="unidad_negocio" class="combo_180px" style="font-size: 14px;" id="unidad_negocio">
                                                    <option value="" selected>Seleccione</option><%
                                                        for (int i = 0; i < listaCombo.size(); i++) {
                                                            CmbGeneralScBeans CmbContenido = (CmbGeneralScBeans) listaCombo.get(i);%>
                                                    <option value="<%=CmbContenido.getIdCmb()%>"><%=CmbContenido.getDescripcionCmb()%></option><%
                                                        }%>
                                                </select>
                                            </td>
                                        </tr>
                                    </table>
                                </fieldset>
                            </td>
                            <td width="25%">
                                <fieldset>
                                    <legend>BANCO A TRANSFERIR</legend>
                                    <table border="0" align="center" cellpadding="1" cellspacing="1" class="labels">
                                        <td width="215">
                                            <%
                                                Vector vb = new Vector();
                                                model.Negociossvc.listb("negocios");
                                                vb = model.Negociossvc.getListNeg();
                                            %>
                                            <select name="banco" id="bco" class="combo_180px" style="font-size: 14px;">
                                                <option value="...">Seleccione</option>
                                                <%for (int j = 0; j < vb.size(); j++) {
                                                        Negocios neg = (Negocios) vb.get(j);%>
                                                <%if (neg.getNom_cli().equals("BANCOLOMBIA")) {%>
                                                <option value="BANCOLOMBIAPAB,<%=neg.getCod_cli()%>,<%=neg.getCod_tabla()%>">BANCOLOMBIA PAB <%=neg.getCod_cli()%></option>                                                
                                                <%}else{%>        
                                                <option value="<%=neg.getNom_cli()%>,<%=neg.getCod_cli()%>,<%=neg.getCod_tabla()%>"><%=neg.getNom_cli()%> <%=neg.getCod_cli()%></option>
                                                
                                                <%}}%>
                                            </select>
                                        </td>
                                    </table>
                                </fieldset>
                            </td>
                            <td width="10%">
                                <span  id="buscar" class="form-submit-button form-submit-button-simple_green_apple" onClick="buscarNegocios()" >Buscar </span>
                            </td>
                        </tr>
                    </table>
                </div>                         
                <br/><br/>
                <table border="0"  align="center">
                    <tr>
                        <td>
                            <table id="Negocios" class="center_div" ></table>
                        </td>
                    </tr>
                </table>
<!--                <table border="0"  align="center">
                    <tr>
                        <td>
                            <table id="Negocios1" class="center_div" ></table>
                        </td>
                    </tr>
                </table>-->
                <br> 
                <div align="center" id="div_botones" style=" display:  none" >
                    <table border="0" style=" width: 300px" align="center">
                        <tr>
                            <td>
                                <span  class="form-submit-button form-submit-button-simple_green_apple" onClick="transferir()" />Transferir </span>
                            </td>
                            <td>
                                <span  class="form-submit-button form-submit-button-simple_green_apple" onClick="exportarTablaExcel()" />Exportar </span>
                            </td>
                            <td>
                                <span  class="form-submit-button form-submit-button-simple_rose" onClick="javascript:window.close()" />Salir </span>
                            </td>
                        </tr>
                    </table>
                </div>		
            </form> 
       

        <div id="VentanaCuentas" title="Listado Proveedores" style=" display: block" ></div>
        <div id="div_espera" style="display:none;z-index:1000; position:absolute"></div>
        <div id="divSalidaEx" title="Exportacion" style=" display: block" >
                <p  id="msjEx" style=" display:  none"> Espere un momento por favor...</p>
                    <center><img id="imgloadEx" style="position: relative;  top: 7px; display: none " src="<%=BASEURL%>/images/cargandoCM.gif"/></center>
                    <div id="respEx" style=" display: none"></div>
        </div>
        </div>
    <center>
        <div id="info"  class="ventana" style="width: 325px;" >
            <p id="notific">mensaje</p>
        </div>
    </center>
    </body>
</html>
