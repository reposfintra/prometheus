<%--
    Document   : Conceptos
    Created on : 02/12/2011, 04:43:44 PM
    Author     : ivargas
--%>
<%@page session="true"%>
<%@page import="java.util.*"%>
<%@page import="com.tsp.util.Util"%>
<%@page import="com.tsp.operation.model.*"%>
<%@page import="com.tsp.operation.model.beans.*"%>
<%@page import="com.tsp.operation.model.services.*"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%
    response.addHeader("Cache-Control", "no-cache");
    response.addHeader("Pragma", "No-cache");
    response.addDateHeader("Expires", 0);
    Usuario usuario = (Usuario) session.getAttribute("Usuario");
    String nitcliente=(String) session.getAttribute("cod_cli"); 
    String BASEURL = request.getContextPath();
    String CONTROLLER = BASEURL + "/controller";
    String negocio = request.getParameter("negocio") != null ? request.getParameter("negocio") : "";
    String form = request.getParameter("form") != null ? request.getParameter("form") : "";
    String act = request.getParameter("act") != null ? request.getParameter("act") : "";
    String redi = request.getParameter("redi") != null ? request.getParameter("redi") : "";
    String tipoconv = request.getParameter("tipoconv") != null ? request.getParameter("tipoconv") : "";
    double cuotaneg = request.getParameter("cuota") != null ? Double.parseDouble(request.getParameter("cuota")) : 0;
    String msg = request.getParameter("msg") != null ? request.getParameter("msg") : "";   
    GestionSolicitudAvalService gsaserv = new GestionSolicitudAvalService(usuario.getBd());
    WSHistCreditoService wsdatacred = new WSHistCreditoService(usuario.getBd());
    NegocioTrazabilidadService trazaserv = new NegocioTrazabilidadService(usuario.getBd());
    NegocioTrazabilidad negtrazaref = null;
    NegocioTrazabilidad negtrazaana = null;
    NegocioTrazabilidad negtrazadec = null;
    NegocioTrazabilidad negtrazaliq = null;
    NegocioTrazabilidad negtrazasol = null;
    NegocioTrazabilidad negulttraza = null;
    SolicitudLaboral bean_lab = null;
    SolicitudPersona bean_pers = null;
    SolicitudLaboral bean_labc = null;
    SolicitudPersona bean_perc = null;
    SolicitudAval solicitud_aval = null;
    double totalIngreso = 0;
    double cuota = 0;
    double gastosfam = 0;
    double gastosarr = 0;
    double totalegreso = 0;
    double capacidadpago = 0;
    int disponible = 0;
    double porcgfam = 0;
    double porcgfamcod = 0;
    double totalIngresocod = 0;
    double cuotacod = 0;
    double gastosfamcod = 0;
    double gastosarrcod = 0;
    double totalegresocod = 0;
    double capacidadpagocod = 0;
    int disponiblecod = 0;
    ArrayList conceptos = null;
    String[] dato1 = null;
    GestionConveniosService convserv = new GestionConveniosService(usuario.getBd());
    Convenio convenio = convserv.buscar_convenio(negocio);
 
    String nitEmpresa = "";
    if (convenio.isAval_anombre()) {
        nitEmpresa = convenio.getNit_anombre();
    } else {
        CompaniaService ciaserv = new CompaniaService(usuario.getBd());
        ciaserv.buscarCia(usuario.getDstrct());
        Compania cia = ciaserv.getCompania();
        nitEmpresa = cia.getnit();
    }
    NegocioTrazabilidad sw = trazaserv.buscarTraza(Integer.parseInt(form), act);

    if (act.equals("ANA")) {
        conceptos = trazaserv.listadotablagen("CONCEPNEG", "%A%");
    }
    if (act.equals("ANA") || act.equals("DEC")) {
        if (!form.equals("")) {
            bean_lab = gsaserv.datosLaboral(Integer.parseInt(form), "S");
            bean_pers = gsaserv.buscarPersona(Integer.parseInt(form), "S");
            negtrazaref = trazaserv.buscarTraza(Integer.parseInt(form), "REF");
            negtrazaliq = trazaserv.buscarTraza(Integer.parseInt(form), "LIQ");
            negtrazasol = trazaserv.buscarTraza(Integer.parseInt(form), "SOL");
            negulttraza = trazaserv.buscarUltimaActivida(Integer.parseInt(form));
            bean_labc = gsaserv.datosLaboral(Integer.parseInt(form), "C");
            bean_perc = gsaserv.buscarPersona(Integer.parseInt(form), "C");
            solicitud_aval=gsaserv.buscarSolicitud(negocio);            

        }
        totalIngreso = (bean_lab != null ? (Double.parseDouble(bean_lab.getSalario()) + Double.parseDouble(bean_lab.getOtrosIngresos())) : 0);
        cuota = bean_pers != null ? wsdatacred.buscarSumCuotas(bean_pers.getIdentificacion(),nitEmpresa) : 0;
        porcgfam = (bean_pers != null ? (Double.parseDouble(bean_pers.getSalarioCony()) > 0 && (bean_pers.getEstadoCivil().equals("C") || bean_pers.getEstadoCivil().equals("U")) ? 0.3 : 0.4) : 0);
        gastosfam = totalIngreso * porcgfam;
        gastosarr = (bean_lab != null ? (Double.parseDouble(bean_lab.getGastosArriendo())) : 0);
        totalegreso = cuota + gastosfam + gastosarr;
        capacidadpago = (totalIngreso - totalegreso);
        disponible = (totalIngreso < 1) ? 100 : Math.round(((float) ((totalegreso + cuotaneg) / totalIngreso) * 100));
        totalIngresocod = (bean_labc != null ? (Double.parseDouble(bean_labc.getSalario()) + Double.parseDouble(bean_labc.getOtrosIngresos())) : 0);
        cuotacod = bean_perc != null ? wsdatacred.buscarSumCuotas(bean_perc.getIdentificacion(),nitEmpresa) : 0;
        porcgfamcod = (bean_perc != null ? (Double.parseDouble(bean_perc.getSalarioCony()) > 0 && (bean_perc.getEstadoCivil().equals("C") || bean_perc.getEstadoCivil().equals("U")) ? 0.3 : 0.4) : 0);
        gastosfamcod = totalIngresocod * porcgfamcod;
        gastosarrcod = (bean_labc != null ? (Double.parseDouble(bean_labc.getGastosArriendo())) : 0);
        totalegresocod = cuotacod + gastosfamcod + gastosarrcod;
        capacidadpagocod = (totalIngresocod - totalegresocod);
        disponiblecod = (totalIngresocod < 1) ? 100 : Math.round((float) (((totalegresocod + cuotaneg) / totalIngresocod) * 100));
    }

    if (act.equals("DEC")) {
        conceptos = trazaserv.listadotablagen("CONCEPNEG", "%D%");
        if (!form.equals("")) {
            negtrazaref = trazaserv.buscarTraza(Integer.parseInt(form), "REF");
            negtrazaana = trazaserv.buscarTraza(Integer.parseInt(form), "ANA");
            solicitud_aval=gsaserv.buscarSolicitud(negocio);
        }
    }
    if (act.equals("FOR")) {
        if (!form.equals("")) {
            negtrazadec = trazaserv.buscarTraza(Integer.parseInt(form), "DEC");
        }
        conceptos = trazaserv.listadotablagen("CONCEPNEG", "%F%");
    }
    if (act.equals("RAD")) {
        conceptos = trazaserv.listadotablagen("CONCEPNEG", "%R%");
    }
    if (act.equals("REF")) {
        conceptos = trazaserv.listadotablagen("CONCEPNEG", "%L%");
         solicitud_aval=gsaserv.buscarSolicitud(negocio);
        if (!form.equals("")) {
                negulttraza = trazaserv.buscarUltimaActivida(Integer.parseInt(form));
        }
          
    }

        if (act.equals("PERF")) {
        if (!form.equals("")) {
            negtrazadec = trazaserv.buscarTraza(Integer.parseInt(form), "DEC");
        }
        conceptos = trazaserv.listadotablagen("CONCEPNEG", "%P%");
    }
%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">

        <script type='text/javascript' src="<%= BASEURL%>/js/boton.js"></script>
        <script type='text/javascript' src="<%= BASEURL%>/js/prototype.js"></script>
       
        <link href="<%= BASEURL%>/css/estilostsp.css" rel="stylesheet" type="text/css">
        <link href="<%=BASEURL%>/css/default.css" rel="stylesheet" type="text/css">
        <link href="<%=BASEURL%>/css/alert.css" rel="stylesheet" type="text/css"/>
        <script src="<%=BASEURL%>/js/effects.js"        type="text/javascript"></script>
        <script src="<%=BASEURL%>/js/window.js"         type="text/javascript"></script>
        
        <link href="<%=BASEURL%>/css/popup.css" rel="stylesheet" type="text/css">
        <link href="<%=BASEURL%>/css/jquery/jquery-ui/jquery-ui-2.css" type="text/css" rel="stylesheet" />
        <script type="text/javascript" src="<%=BASEURL%>/js/jquery/jquery.jqGrid/js/jquery.min.js"></script>
        <script type="text/javascript" src="<%=BASEURL%>/js/jquery-ui-1.8.5.custom.min.js"></script>
        <script type="text/javascript" src="<%=BASEURL%>/js/jquery/jquery.jqGrid/js/grid.locale-es.js"></script>
        <script type="text/javascript" src="<%=BASEURL%>/js/jquery/jquery.jqGrid/js/jquery.jqGrid.min.js"></script>
       
        <script type="text/javascript">
                jQuery.noConflict();
                var $j = jQuery;
        </script>
        <script type='text/javascript' src="<%= BASEURL%>/js/negocioTrazabilidad.js"></script>
   
        <style type="text/css">
            b{
                color:white;
            }
            .divs {
                font-family: Verdana, Tahoma, Arial, Helvetica, sans-serif;
                font-size: small;
                color: #000000;
                background-color:#397EAD;
                -moz-border-radius: 0.7em;
                -webkit-border-radius: 0.7em;
            }
        </style>
        <title>Conceptos</title>
    </head>
    <body onload="inicializar('<%=CONTROLLER%>', '<%= BASEURL%>');xpand_div('<%=act%>');redi('<%=redi%>','<%=act%>')">
        <div id="capaCentral" style="position:absolute; width:100%; height:83%; z-index:0; left: 0px; top: 10px;">
            <form id="formulario" name="formulario" action="<%=CONTROLLER%>?estado=Negocio&accion=Trazabilidad" method="post" enctype='multipart/form-data'>
                <table align="center" width="520" border="0">
                    <tbody>
                        <tr>
                            <td>
                                <input id="negocio" name="negocio" type="hidden" value="<%=negocio%>" />
                                <input id="form" name="form" type="hidden" value="<%=form%>" />
                                <input id="act" name="act" type="hidden" value="<%=act%>" />
                                <input id="tipoconv" name="tipoconv" type="hidden" value="<%=tipoconv%>" />
                                <input id="sw" name="sw" type="hidden" value="<%=sw%>" />
                                <span style="color: red"><%=sw != null ? "REPROCESO" : ""%></span>
                                <input id="sw" name="sw" type="hidden" value="<%=sw%>" />
                                <span style="color: red"><%=sw != null ? "REPROCESO" : ""%></span>

                                <div id="contenido" class="divs">

                                </div>
                                <div id="busqueda"><%=msg%></div>
                                <br/>
                                <div align="center">
                                    <img alt="" src = "<%=BASEURL%>/images/botones/aceptar.gif" style="cursor:pointer" onclick="this.disabled=true;guardar();" name= "imgaceptar" id="imgaceptar"  onMouseOver="botonOver(this);" onMouseOut="botonOut(this);">
                                    <img alt="" src = "<%=BASEURL%>/images/botones/salir.gif" style = "cursor:pointer" name = "imgsalir" onclick = "Javascript:parent.closewin()" onmouseover="botonOver(this);" onmouseout="botonOut(this);">
                                </div>
                            </td>
                        </tr>
                    </tbody>
                </table>
            </form>
        </div>


        <div id="referenciar" style="display:none; visibility: hidden;">
            <b>REFERENCIACI&Oacute;N</b><br>
            <table id="referenciar" border="0" style="border-collapse: collapse; width: 100%;">
                <tr class="fila">
                    <td>Recomendaci&oacute;n</td>
                    <td>
                        <select id="refconcepto" name="refconcepto" onchange="cargarlista('refconcepto','refcausal','refcausales','CAUSACONCP');showHideCommentStby('refconcepto','refcomment_stby','refcomentarios_stby')">
                            <option value="">...</option>
                            <%  dato1 = null;
                                for (int i = 0; i < conceptos.size(); i++) {
                                    dato1 = ((String) conceptos.get(i)).split(";_;");%>
                            <option value="<%=dato1[0]%>" ><%=dato1[1]%></option>
                            <%  }%>
                        </select>
                        <a href="javascript:detalle_mod_form('<%=BASEURL%>','<%=negocio%>');"> Detalle</a>
                    </td>
                </tr>
                <tr class="fila">
                    <td> Causal</td>
                    <td>
                        <div id="refcausales">
                            <select id="refcausal" name="refcausal" style="width:20em;">
                                <option value="">...</option>
                            </select>
                        </div>
                    </td>
                </tr>
                <tr class="fila">
                    <td> Comentarios</td>
                    <td>
                        <textarea id ="refcomentarios" name="refcomentarios" cols="70" rows="3"></textarea>

                    </td>
                </tr>
                <tr class="fila" id="refcomment_stby" style="display:none">
                    <td> Comentario StandBy</td>
                    <td>
                        <textarea id ="refcomentarios_stby" name="refcomentarios_stby" cols="70" rows="3"></textarea>
                    </td>
                </tr>
                <tr class="fila">
                    <td>Seleccione</td>
                    <td>
                        <input type='file' name='archivo' id='archivo' style='width:100%'>
                    </td>
                </tr>
            </table>
        </div>
      
        <div id="ref_standby" style="display:none; visibility: hidden;">
            <b>STANDBY</b><br>
            <table id="ref_standby" border="0" style="border-collapse: collapse; width: 100%;">
                <tr class="fila">
                    <td> Usuario   </td>
                    <td>
                        <input type="hidden" id="refconcepto_last_traza" name="refconcepto_last_traza" value="<%=negulttraza != null ? negulttraza.getConcepto() : ""%>" readonly>
                        <input type="text" id="refusuariostby" name="refusuariostby" value="<%=negulttraza != null ? negulttraza.getUsuario() : ""%>" size="25" readonly >
                    </td>
                </tr>
                <tr class="fila">
                    <td> Fecha</td>
                    <td>
                        <input type="text" id="reffechastby" name="reffechastby" value="<%=negulttraza != null ? negulttraza.getFecha() : ""%>" size="25" readonly >
                    </td>
                </tr>
                <tr class="fila">
                    <td> Comentarios</td>
                    <td>
                        <textarea id ="refcomentariosstby" name="refcomentariosstby" readonly cols="70" rows="3"><%=negulttraza != null ? negulttraza.getComentarios() : ""%></textarea>
                    </td>
                </tr>

            </table>
        </div>            

        <div id="capacidad" style="display:none; visibility: hidden;">
            <b>CAPACIDAD DE PAGO</b><br>
            <table id="ingresos" border="0" style="border-collapse: collapse; width: 100%;">
                <thead>
                    <tr>
                        <th colspan="2"><b> Ingresos Deudor</b><input id="porcgfam" name="porcgfam" type="hidden" value="<%=porcgfam%>" /></th>
                        <th colspan="2"><b> Ingresos Codeudor</b><input id="porcgfamcod" name="porcgfamcod" type="hidden" value="<%=porcgfamcod%>" /></th>
                    </tr></thead>
                <tr class="fila">
                    <td width="20%"> Ingresos laborales</td>
                    <td width="30%">
                        $<input type="text" id="salario" name="salario" value="<%= (bean_lab == null) ? "0" : Util.customFormat(Double.parseDouble(bean_lab.getSalario()))%>" size="20" onkeyup="soloNumeros(this.id);" onblur="calculos();" >
                    </td>
                    <td width="20%"> Ingresos laborales</td>
                    <td width="30%">
                        $<input type="text" id="salariocod" name="salariocod" value="<%= (bean_labc == null) ? "0" : Util.customFormat(Double.parseDouble(bean_labc.getSalario()))%>" size="20" onkeyup="soloNumeros(this.id);" onblur="calculos2();" >
                    </td>
                </tr>
                <tr class="fila">
                    <td>Honorarios y comisiones</td>
                    <td>
                        $<input type="text" id="honorarios" name="honorarios" value="0" size="20" onkeyup="soloNumeros(this.id);" onblur="calculos();" >
                    </td>
                    <td>Honorarios y comisiones</td>
                    <td>
                        $<input type="text" id="honorarioscod" name="honorarioscod" value="0" size="20" onkeyup="soloNumeros(this.id);" onblur="calculos2();" >
                    </td>
                </tr>
                <tr class="fila">
                    <td> Otros Ingresos</td>
                    <td>
                        $<input type="text" id="otrosingresos" name="otrosingresos" value="<%= (bean_lab == null) ? "0" : Util.customFormat(Double.parseDouble(bean_lab.getOtrosIngresos()))%>" size="20" onkeyup="soloNumeros(this.id);" onblur="calculos();" >
                    </td>
                    <td> Otros Ingresos</td>
                    <td>
                        $<input type="text" id="otrosingresoscod" name="otrosingresoscod" value="<%= (bean_labc == null) ? "0" : Util.customFormat(Double.parseDouble(bean_labc.getOtrosIngresos()))%>" size="20" onkeyup="soloNumeros(this.id);" onblur="calculos2();" >
                    </td>
                </tr>
                <tr class="fila">
                    <td> TOTAL INGRESOS BRUTO</td>
                    <td>
                        $<input type="text" id="totalingreso" name="totalingreso" value="<%=Util.customFormat(totalIngreso)%>" size="20" readonly >
                    </td>
                    <td> TOTAL INGRESOS BRUTO</td>
                    <td>
                        $<input type="text" id="totalingresocod" name="totalingresocod" value="<%=Util.customFormat(totalIngresocod)%>" size="20" readonly >
                    </td>
                </tr>
                <tr class="fila">
                    <td> Descuento nomina</td>
                    <td>
                        $<input type="text" id="descuento" name="descuento" value="0" size="20" onkeyup="soloNumeros(this.id);" onblur="calculos();" >
                    </td>
                    <td> Descuento nomina</td>
                    <td>
                        $<input type="text" id="descuentocod" name="descuentocod" value="0" size="20" onkeyup="soloNumeros(this.id);" onblur="calculos2();" >
                    </td>
                </tr>
                <tr class="fila">
                    <td> INGRESOS NETO</td>
                    <td>
                        $<input type="text" id="ingresoneto" name="ingresoneto" value="<%= Util.customFormat(totalIngreso)%>" size="20" readonly >
                    </td>
                    <td> INGRESOS NETO</td>
                    <td>
                        $<input type="text" id="ingresonetocod" name="ingresonetocod" value="<%= Util.customFormat(totalIngresocod)%>" size="20" readonly >
                    </td>
                </tr>
            </table>
            <table id="egresos" border="0" style="border-collapse: collapse; width: 100%;">
                <thead>
                    <tr>
                        <th colspan="2"><b> Egresos Deudor</b></th>
                        <th colspan="2"><b> Egresos Codeudor</b></th>
                    </tr></thead>
                <tr class="fila">
                    <td width="20%" > Gastos Familiares</td>
                    <td width="30%">
                        $<input type="text" id="gastosfam" name="gastosfam" value="<%=Util.customFormat(gastosfam)%>" size="20" onkeyup="soloNumeros(this.id);" onblur="calculos('S');" >
                    </td>
                    <td width="20%"> Gastos Familiares</td>
                    <td width="30%">
                        $<input type="text" id="gastosfamcod" name="gastosfamcod" value="<%=Util.customFormat(gastosfamcod)%>" size="20" onkeyup="soloNumeros(this.id);" onblur="calculos2('S');" >
                    </td>
                </tr>
                <tr class="fila">
                    <td> Cuotas centrales de riesgo</td>
                    <td>
                        $<input type="text" id="cuota" name="cuota" value="<%=Util.customFormat(cuota)%>" size="20" onkeyup="soloNumeros(this.id);" onblur="calculos();" >
                    </td>
                    <td> Cuotas centrales de riesgo</td>
                    <td>
                        $<input type="text" id="cuotacod" name="cuotacod" value="<%=Util.customFormat(cuotacod)%>" size="20" onkeyup="soloNumeros(this.id);" onblur="calculos2();" >
                    </td>
                </tr>
                <tr class="fila">
                    <td> Gastos arriendo</td>
                    <td>
                        $<input type="text" id="gastosarr" name="gastosarr" value="<%=Util.customFormat(gastosarr)%>" size="20" onkeyup="soloNumeros(this.id);" onblur="calculos();" >
                    </td>
                    <td> Gastos arriendo</td>
                    <td>
                        $<input type="text" id="gastosarrcod" name="gastosarrcod" value="<%=Util.customFormat(gastosarrcod)%>" size="20" onkeyup="soloNumeros(this.id);" onblur="calculos2();" >
                    </td>
                </tr>
                <tr class="fila">
                    <td> Otros gastos</td>
                    <td>
                        $<input type="text" id="otrosgastos" name="otrosgastos" value="0" size="20" onkeyup="soloNumeros(this.id);" onblur="calculos();" >
                    </td>
                    <td> Otros gastos</td>
                    <td>
                        $<input type="text" id="otrosgastoscod" name="otrosgastoscod" value="0" size="20" onkeyup="soloNumeros(this.id);" onblur="calculos2();" >
                    </td>
                </tr>
                <tr class="fila">
                    <td> TOTAL EGRESOS</td>
                    <td>
                        $<input type="text" id="totalegreso" name="totalegreso" value="<%= Util.customFormat(totalegreso)%>" size="20" readonly >
                    </td>
                    <td> TOTAL EGRESOS</td>
                    <td>
                        $<input type="text" id="totalegresocod" name="totalegresocod" value="<%= Util.customFormat(totalegresocod)%>" size="20" readonly >
                    </td>
                </tr>
            </table>
                    
                    <div id="score" style="display:none; visibility: hidden;">
<!--                        <b>NOMBRE DEL TITULO </b><br>-->
                        <table id="titulo" border="0" style="border-collapse: collapse; width: 100%;">
                            <thead>
                                <tr>
                                    <th colspan="2" style="color: white;text-align:left">SCORE</th>
                                </tr>
                            </thead>
                            <tr class="fila">
                                <td> Score bur&oacute;</td>
                                <td>
                                    <input type="text" id="score_buro" name="score_buro" style="width: 40px;" value="<%= solicitud_aval != null ? solicitud_aval.getScore_buro(): ""%>" size="25" readonly >
                                </td>
                                <td> Score propio</td>
                                <td>
                                    <input type="text" id="score_lisim" name="score_lisim" style="width: 40px;" value="<%= solicitud_aval != null ? solicitud_aval.getScore_lisim(): ""%>" size="25" readonly >
                                </td>
                                <td> Score total</td>
                                <td>
                                    <input type="text" id="score_total" name="score_total" style="width: 40px;" value="<%= solicitud_aval != null ? solicitud_aval.getScore_total() : ""%>" size="25" readonly >
                                </td>
                                <td>Accion sugerida</td>
                                <td>
                                    <input type="text" id="accion_sugerida" name="accion_sugerida" style="width: 100px;" value="<%= solicitud_aval != null ? solicitud_aval.getAccion_sugerida(): ""%>" size="25" readonly >
                                </td>
                            </tr>
                        </table>
                    </div>  
                    <div id="tipocliente" style="display:none; visibility: hidden;">
                        <table id="titulo" border="0" style="border-collapse: collapse; width: 100%;">
                            <thead>
                                <tr>
                                    <th colspan="2" style="color: white;text-align:left">TIPO CLIENTE</th>
                                </tr>
                            </thead>
                            <tr class="fila">
                                <td width="20%" > Tipo de cliente</td>
                                <td width="80%">
                                    <input type="text" id="tipo_cliente" name="tipo_cliente" style="width: 159px;" value="<%= solicitud_aval != null ? solicitud_aval.getTipo_cliente() : ""%>" size="25" readonly >
                                </td>
                            </tr>
                        </table>
                    </div>  
            <table id="capacidad" border="0" style="border-collapse: collapse; width: 100%;">
                <thead>
                    <tr>
                        <th colspan="2"><b> Capacidad Pago Deudor</b></th>
                        <th colspan="2"><b> Capacidad Pago Codeudor</b></th>
                    </tr></thead>
                <tr class="fila">
                    <td width="20%"> CAPACIDAD DE PAGO</td>
                    <td width="30%">
                        $<input type="text" id="capacidadpago" name="capacidadpago" value="<%= Util.customFormat(capacidadpago)%>" size="20" readonly >
                    </td>
                    <td width="20%"> CAPACIDAD DE PAGO</td>
                    <td width="30%">
                        $<input type="text" id="capacidadpagocod" name="capacidadpagocod" value="<%= Util.customFormat(capacidadpagocod)%>" size="20" readonly >
                    </td>
                </tr>
                <tr class="fila">
                    <td> CUOTA EN TRAMITE</td>
                    <td>
                        $<input type="text" id="cuotaneg" name="cuotaneg" value="<%= Util.customFormat(cuotaneg)%>" size="20" readonly >
                    </td>
                    <td> CUOTA EN TRAMITE</td>
                    <td>
                        $<input type="text" id="cuotanegcod" name="cuotanegcod" value="<%= Util.customFormat(cuotaneg)%>" size="20" readonly >
                    </td>
                </tr>
                <tr class="fila">
                    <td> Limite Max. Endeudamiento </td>
                    <td>
                        <input type="text" id="disponible" name="disponible" <%= disponible > 90 ? "style='color: red;'" : "style='color: #003399;'"%> value="<%= disponible%>" size="20" readonly >%
                    </td>
                    <td> Limite Max. Endeudamiento </td>
                    <td>
                        <input type="text" id="disponiblecod" name="disponiblecod" <%= disponiblecod > 90 ? "style='color: red;'" : "style='color: #003399;'"%> value="<%= disponiblecod%>" size="20" readonly >%
                    </td>
                </tr>
                <tr class="fila">
                    <td> Capacidad de Pago </td>
                    <td>
                        <input type="text" id="capacidad_endeudamiento" name="capacidad_endeudamiento" value="<%= solicitud_aval != null ? solicitud_aval.getCapacidad_endeudamiento(): ""%>" size="20" readonly >
                    </td>
                    <td> Capacidad de Pago </td>
                    <td>
                        <input type="text" id="capacidad_endeudamiento_cod" name="capacidad_endeudamiento"  value="0%" size="20" readonly >
                    </td>
                </tr>
            </table>
            <div id="info_cartera" style="display:none; visibility: hidden;">
                <center><b>INFORMACIÓN DE CARTERA</b></center>
                <table id="altura_mora" border="0" style="border-collapse: collapse; width: 100%;">
                    <thead>
                        <tr>
                            <th colspan="2"><b> Altura mora Deudor</b></th>
                            <th colspan="2"><b> Altura mora Codeudor</b></th>
                        </tr>
                    </thead>
                    <tr class="fila">
                        <td width="20%"> Actual</td>
                        <td width="30%">
                            <input type="text" id="altura_actual" name="altura_actual" value="<%= solicitud_aval != null ? solicitud_aval.getAltura_actual_titular(): ""%>" size="20" readonly>
                        </td>
                        <td width="20%"> Actual</td>
                        <td width="30%">
                            <input type="text" id="altura_actualcod" name="altura_actualcod" value="<%= solicitud_aval != null ? solicitud_aval.getAltura_actual_codeudor(): ""%>" size="20" readonly>
                        </td>
                    </tr>                    
                    <tr class="fila">
                        <td width="20%"> Hist. Ult. Año</td>
                        <td width="30%">
                            <input type="text" id="altura_hist" name="altura_hist" value="<%= solicitud_aval != null ? solicitud_aval.getAltura_hist_titular(): ""%>" size="20" readonly>
                        </td>
                        <td width="20%"> Hist. Ult. Año</td>
                        <td width="30%">
                            <input type="text" id="altura_histcod" name="altura_histcod" value="<%= solicitud_aval != null ? solicitud_aval.getAltura_hist_codeudor(): ""%>" size="20" readonly>
                        </td>
                    </tr>
                    <tr class="fila">
                        <td width="20%"> Cuotas Pendientes</td>
                        <td width="30%">
                            <input type="text" id="cuotas_pendientes" name="cuotas_pendientes" value="<%= solicitud_aval != null ? solicitud_aval.getCuotas_pendientes(): ""%>" size="20" readonly>
                        </td> 
                        <td colspan="2"></td>
                    </tr>
                </table>
            </div>        
        </div>

        <div id="referenciacion" style="display:none; visibility: hidden;">
            <b>REFERENCIACI&Oacute;N</b><br>
            <table id="referenciacion" border="0" style="border-collapse: collapse; width: 100%;">
                <tr class="fila">
                    <td> Usuario   </td>
                    <td>
                        <input type="text" id="usuarioref" name="usuarioref" value="<%=negtrazaref != null ? negtrazaref.getUsuario() : ""%>" size="25" readonly >
                        <a href="javascript:detalle_mod_form('<%=BASEURL%>','<%=negocio%>');"> Detalle</a>
                    </td>
                </tr>
                <tr class="fila">
                    <td> Fecha</td>
                    <td>
                        <input type="text" id="fecharef" name="fecharef" value="<%=negtrazaref != null ? negtrazaref.getFecha() : ""%>" size="25" readonly >
                    </td>
                </tr>
                <tr class="fila">
                    <td> Comentarios</td>
                    <td>
                        <textarea id ="comentariosref" name="comentariosref" readonly cols="70" rows="3"><%=negtrazaref != null ? negtrazaref.getComentarios() : ""%></textarea>
                    </td>
                </tr>

            </table>
        </div>
        
        <div id="standby" style="display:none; visibility: hidden;">
            <b>STANDBY</b><br>
            <table id="standby" border="0" style="border-collapse: collapse; width: 100%;">
                <tr class="fila">
                    <td> Usuario   </td>
                    <td>
                        <input type="hidden" id="concepto_last_traza" name="concepto_last_traza" value="<%=negulttraza != null ? negulttraza.getConcepto(): ""%>" readonly>
                        <input type="text" id="usuariostby" name="usuariostby" value="<%=negulttraza != null ? negulttraza.getUsuario() : ""%>" size="25" readonly >
<!--                        <a href="javascript:detalle_mod_form('<%=BASEURL%>','<%=negocio%>');"> Detalle</a>-->
                    </td>
                </tr>
                <tr class="fila">
                    <td> Fecha</td>
                    <td>
                        <input type="text" id="fechastby" name="fechastby" value="<%=negulttraza != null ? negulttraza.getFecha() : ""%>" size="25" readonly >
                    </td>
                </tr>
                <tr class="fila">
                    <td> Comentarios</td>
                    <td>
                        <textarea id ="comentariosstby" name="comentariosstby" readonly cols="70" rows="3"><%=negulttraza != null ? negulttraza.getComentarios() : ""%></textarea>
                    </td>
                </tr>

            </table>
        </div>
                    
        <div id="liquidacion" style="display:none; visibility: hidden;">
            <%if (negtrazaliq != null) {%>
            <b>LIQUIDACI&Oacute;N</b><br>
            <table id="liquidacion" border="0" style="border-collapse: collapse; width: 100%;">
                <tr class="fila">
                    <td> Usuario</td>
                    <td>
                        <input type="text" id="usuarioliq" name="usuarioliq" value="<%=negtrazaliq != null ? negtrazaliq.getUsuario() : ""%>" size="25" readonly >
                    </td>
                </tr>
                <tr class="fila">
                    <td> Fecha</td>
                    <td>
                        <input type="text" id="fechaliq" name="fechaliq" value="<%=negtrazaliq != null ? negtrazaliq.getFecha() : ""%>" size="25" readonly >
                    </td>
                </tr>
                <tr class="fila">
                    <td>Recomendaci&oacute;n</td>
                    <td>
                        <input type="text" id="conceptoliq" name="conceptoliq" value="<%=negtrazaliq != null ? negtrazaliq.getConcepto() : ""%>" size="25" readonly >
                    </td>
                </tr>
                <tr class="fila">
                    <td> Comentarios</td>
                    <td>
                        <textarea id ="comentariosliq" name="comentariosliq" readonly cols="70" rows="3"><%=negtrazaliq != null ? negtrazaliq.getComentarios() : ""%></textarea>
                    </td>
                </tr>
            </table>

            <%} else {%>
            <b>SOLICITUD</b><br>
            <table id="solicitud" border="0" style="border-collapse: collapse; width: 100%;">
                <tr class="fila">
                    <td> Usuario</td>
                    <td>
                        <input type="text" id="usuariosol" name="usuariosol" value="<%=negtrazasol != null ? negtrazasol.getUsuario() : ""%>" size="25" readonly >
                    </td>
                </tr>
                <tr class="fila">
                    <td> Fecha</td>
                    <td>
                        <input type="text" id="fechasol" name="fechasol" value="<%=negtrazasol != null ? negtrazasol.getFecha() : ""%>" size="25" readonly >
                    </td>
                </tr>
                <tr class="fila">
                    <td>Recomendaci&oacute;n</td>
                    <td>
                        <input type="text" id="conceptosol" name="conceptosol" value="<%=negtrazasol != null ? negtrazasol.getConcepto() : ""%>" size="25" readonly >
                    </td>
                </tr>
                <tr class="fila">
                    <td> Comentarios</td>
                    <td>
                        <textarea id ="comentariossol" name="comentariossol" readonly cols="70" rows="3"><%=negtrazasol != null ? negtrazasol.getComentarios() : ""%></textarea>
                    </td>
                </tr>
            </table>
            <%}%>
        </div>

        <div id="analizar" style="display:none; visibility: hidden;">
            <b>CONCEPTO DE ANALISIS</b><br>
            <table id="analizar" border="0" style="border-collapse: collapse; width: 100%;">
                <tr class="fila">
                    <td> Recomendaci&oacute;n</td>
                    <td>
                        <select id="anaconcepto" name="anaconcepto" onchange="cargarlista('anaconcepto','anacausal','anacausales','CAUSACONCP');showHideCommentStby('anaconcepto','anacomment_stby','anacomentarios_stby');" >
                            <option value="">...</option>
                            <%  dato1 = null;
                                for (int i = 0; i < conceptos.size(); i++) {
                                    dato1 = ((String) conceptos.get(i)).split(";_;");%>
                            <option value="<%=dato1[0]%>" ><%=dato1[1]%></option>
                            <%  }%>
                        </select>
                    </td>
                </tr>
                <tr class="fila">
                    <td> Causal</td>
                    <td>
                        <div id="anacausales">
                            <select id="anacausal" name="anacausal" style="width:20em;">
                                <option value="">...</option>
                            </select>
                        </div>
                    </td>
                </tr>
                <tr class="fila">
                    <td> Comentarios</td>
                    <td>
                        <textarea id ="anacomentarios" name="anacomentarios" cols="70" rows="3"></textarea>
                    </td>
                </tr>
                <tr class="fila" id="anacomment_stby" style="display:none">
                    <td> Comentario StandBy</td>
                    <td>
                        <textarea id ="anacomentarios_stby" name="anacomentarios_stby" cols="70" rows="3"></textarea>
                    </td>
                </tr>

            </table>
        </div>

        <div id="analisis" style="display:none; visibility: hidden;">
            <b>CONCEPTO DE ANALISIS</b><br>
            <table id="analisis" border="0" style="border-collapse: collapse; width: 100%;">
                <tr class="fila">
                    <td> Usuario</td>
                    <td>
                        <input type="text" id="usuarioana" name="usuarioana" value="<%=negtrazaana != null ? negtrazaana.getUsuario() : ""%>" size="25" readonly >
                    </td>
                </tr>
                <tr class="fila">
                    <td> Fecha</td>
                    <td>
                        <input type="text" id="fechaana" name="fechaana" value="<%=negtrazaana != null ? negtrazaana.getFecha() : ""%>" size="25" readonly >
                    </td>
                </tr>

                <tr class="fila">
                    <td>Recomendaci&oacute;n</td>
                    <td>
                        <input type="text" id="conceptoana" name="conceptoana" value="<%=negtrazaana != null ? negtrazaana.getConcepto() : ""%>" size="25" readonly >
                    </td>
                </tr>
                <tr class="fila">
                    <td> Causal</td>
                    <td>
                        <input type="text" id="causalana" name="causalana" value="<%=negtrazaana != null ? negtrazaana.getCausal() : ""%>" size="35" readonly >
                    </td>
                </tr>
                <tr class="fila">
                    <td> Comentarios</td>
                    <td>
                        <textarea id ="comentariosana" name="comentariosana" readonly cols="70" rows="3"><%=negtrazaana != null ? negtrazaana.getComentarios() : ""%></textarea>
                    </td>
                </tr>

            </table>
        </div>

        <div id="decidir" style="display:none; visibility: hidden;">
            <b>CONCEPTO DE DECISI&Oacute;N</b><br>
            <table id="decidir" border="0" style="border-collapse: collapse; width: 100%;">
                <tr class="fila">
                    <td> Decisi&oacute;n</td>
                    <td>
                        <select id="decconcepto" name="decconcepto" onchange="cargarlista('decconcepto','deccausal','deccausales','CAUSACONCP'); showHideCommentStby('decconcepto','deccomment_stby','deccomentarios_stby');" >
                            <option value="">...</option>
                            <%  dato1 = null;
                                for (int i = 0; i < conceptos.size(); i++) {
                                    dato1 = ((String) conceptos.get(i)).split(";_;");%>
                            <option value="<%=dato1[0]%>" ><%=dato1[1]%></option>
                            <%  }%>
                        </select>
                    </td>
                </tr>
                <tr class="fila">
                    <td> Causal</td>
                    <td>
                        <div id="deccausales">
                            <select id="deccausal" name="deccausal" style="width:20em;">
                                <option value="">...</option>
                            </select>
                        </div>
                    </td>
                </tr>
                <tr class="fila">
                    <td> Comentarios</td>
                    <td>
                        <textarea id ="deccomentarios" name="deccomentarios" cols="70" rows="3"></textarea>
                    </td>
                </tr>
                <tr class="fila" id="deccomment_stby" style="display:none">
                    <td> Comentario StandBy</td>
                    <td>
                        <textarea id ="deccomentarios_stby" name="deccomentarios_stby" cols="70" rows="3"></textarea>
                    </td>
                </tr>
                 <tr class="fila">
                    <td> Perfeccionar</td>
                    <td>
                       <select id="perfeccionar" name="perfeccionar" >
                           <option value="S" <%=(solicitud_aval!=null && solicitud_aval.getProducto().equals("02"))?"selected":""%> >S</option>
                                <option value="N" <%=(solicitud_aval!=null && !solicitud_aval.getProducto().equals("02"))?"selected":""%> >N</option>
                            </select>
                    </td>
                </tr>

            </table>
        </div>

        <div id="decision" style="display:none; visibility: hidden;">
            <%if (negtrazadec != null) {%>
            <b>CONCEPTO DE DECISI&Oacute;N</b><br>
            <table id="decision" border="0" style="border-collapse: collapse; width: 100%;">
                <tr class="fila">
                    <td> Usuario</td>
                    <td>
                        <input type="text" id="usuariodec" name="usuariodec" value="<%=negtrazadec != null ? negtrazadec.getUsuario() : ""%>" size="25" readonly >
                    </td>
                </tr>
                <tr class="fila">
                    <td> Fecha</td>
                    <td>
                        <input type="text" id="fechadec" name="fechadec" value="<%=negtrazadec != null ? negtrazadec.getFecha() : ""%>" size="25" readonly >
                    </td>
                </tr>

                <tr class="fila">
                    <td>Decisi&oacute;n</td>
                    <td>
                        <input type="text" id="conceptodec" name="conceptodec" value="<%=negtrazadec != null ? negtrazadec.getConcepto() : ""%>" size="25" readonly >
                    </td>
                </tr>
                <tr class="fila">
                    <td> Causal</td>
                    <td>
                        <input type="text" id="causaldec" name="causaldec" value="<%=negtrazadec != null ? negtrazadec.getCausal() : ""%>" size="35" readonly >
                    </td>
                </tr>
                <tr class="fila">
                    <td> Comentarios</td>
                    <td>
                        <textarea id ="comentariosdec" name="comentariosdec" readonly cols="70" rows="3"><%=negtrazadec != null ? negtrazadec.getComentarios() : ""%></textarea>
                    </td>
                </tr>

            </table>
            <%}%>
        </div>

        <div id="formalizar" style="display:none; visibility: hidden;">
            <b>CONCEPTO DE FORMALIZACI&Oacute;N</b><br>
            <table id="formalizar" border="0" style="border-collapse: collapse; width: 100%;">
                <tr class="fila">
                    <td>Concepto</td>
                    <td>
                        <select id="forconcepto" name="forconcepto" onchange="asociar_Negocio('<%= BASEURL%>','forconcepto',<%=Integer.parseInt(convenio.getId_convenio())%>,<%=nitcliente%>);cargarlista('forconcepto','forcausal','forcausales','CAUSASTBY');showHideCommentStby('forconcepto','forcomment_stby','forcomentarios_stby')">
                            <option value="">...</option>
                            <%  dato1 = null;
                                for (int i = 0; i < conceptos.size(); i++) {
                                    dato1 = ((String) conceptos.get(i)).split(";_;");%>
                            <option value="<%=dato1[0]%>" ><%=dato1[1]%></option>
                            <%  }%>
                        </select>
                    </td>
                </tr>
                <tr class="fila">
                    <td> Causal</td>
                    <td>
                        <div id="forcausales">
                            <select id="forcausal" name="forcausal" style="width:20em;">
                                <option value="">...</option>
                            </select>
                        </div>
                    </td>
                </tr>
                <tr class="fila">
                    <td> Comentarios</td>
                    <td>
                        <textarea id ="forcomentarios" name="forcomentarios" cols="70" rows="3"></textarea>
                    </td>
                </tr>
                <tr class="fila" id="forcomment_stby" style="display:none">
                    <td> Comentario StandBy</td>
                    <td>
                        <textarea id ="forcomentarios_stby" name="forcomentarios_stby" cols="70" rows="3"></textarea>
                    </td>
                </tr>

            </table>
        </div>

        <div id="radicar" style="display:none; visibility: hidden;">
            <b>RADICACI&Oacute;N</b><br>
            <table id="radicar" border="0" style="border-collapse: collapse; width: 100%;">
                <tr class="fila">
                    <td>Recomendaci&oacute;n</td>
                    <td>
                        <select id="radconcepto" name="radconcepto" >
                            <option value="">...</option>
                            <%  dato1 = null;
                                for (int i = 0; i < conceptos.size(); i++) {
                                    dato1 = ((String) conceptos.get(i)).split(";_;");%>
                            <option value="<%=dato1[0]%>" ><%=dato1[1]%></option>
                            <%  }%>
                        </select>
                    </td>
                </tr>
                <tr class="fila">
                    <td> Comentarios</td>
                    <td>
                        <textarea id ="radcomentarios" name="radcomentarios" cols="70" rows="3"></textarea>

                    </td>
                </tr>
            </table>
        </div>


  <div id="_perfeccionar" style="display:none; visibility: hidden;">
            <b>PERFECCIONAMIENTO</b><br>
            <table id="_perfeccionar" border="0" style="border-collapse: collapse; width: 100%;">
                <tr class="fila">
                    <td>Recomendaci&oacute;n</td>
                    <td>
                        <select id="perfconcepto" name="perfconcepto" onchange="cargarlista('perfconcepto','perfcausal','perfcausales','CAUSACONCP')">
                            <option value="">...</option>
                            <%  dato1 = null;
                                for (int i = 0; i < conceptos.size(); i++) {
                                    dato1 = ((String) conceptos.get(i)).split(";_;");%>
                            <option value="<%=dato1[0]%>" ><%=dato1[1]%></option>
                            <%  }%>
                        </select>                       
                    </td>
                </tr>
                 <tr class="fila">
                    <td> Causal</td>
                    <td>
                        <div id="perfcausales">
                            <select id="perfcausal" name="perfcausal" style="width:20em;">
                                <option value="">...</option>
                            </select>
                        </div>
                    </td>
                </tr>
                <tr class="fila">
                    <td> Comentarios</td>
                    <td>
                        <textarea id ="perfcomentarios" name="perfcomentarios" cols="70" rows="3"></textarea>

                    </td>
                </tr>
                <tr class="fila">
                    <td>Seleccione</td>
                    <td>
                        <input type='file' name='archivo' id='archivo' style='width:100%'>
                    </td>
                </tr>
            </table>
        </div>
                    
        <div id="dialogo" title="Asociar negocio" style="display:none; visibility: hidden;">
            <fieldset>               

                <tbody>
                <legend> <b style="color:black;">TIPO RELACI&Oacute;N</b></legend>   
                <tr>
                    <td> Negocio </td>
                    <td>
                        <input type="text" id="negociobase" name="negociobase" style="text-align:center;" value="<%=negocio%>" size="10" readonly>
                    </td>
                </tr>
                <tr>
                    <td>
                        <input type="radio" name="tipo" value="seguro"  checked>Seguro
                        <input type="radio" name="tipo" value="gps"> GPS
                    </td>  
                </tr>   
                </tbody>

            </fieldset>
            <center>
                <br>
                <table id="tabla_negocios_rel"></table>
                <div id="page_tabla_negocios_rel"></div>
                <br>         
            </center>
        </div>
        
        <div id="dialogMsg" title="Mensaje" style="display:none; visibility: hidden;">
            <p style="font-size: 12px;text-align:justify;" id="msj" > Texto </p>
        </div>               

    </body>
</html>