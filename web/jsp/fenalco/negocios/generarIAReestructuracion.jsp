<%-- 
    Document   : generarIAReestructuracion
    Created on : 23/02/2015, 05:18:04 PM
    Author     : egonzalez
--%>

<%@page contentType="text/html" pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">

        <link href="./css/reestructuracion.css" rel="stylesheet" type="text/css" >
        <link type="text/css" rel="stylesheet" href="./css/jquery/jquery-ui/jquery-ui.css" />

        <script type="text/javascript" src="./js/jquery/jquery.jqGrid/js/jquery.min.js"></script>
        <script type="text/javascript" src="./js/jquery-ui-1.8.5.custom.min.js"></script>
        <script type="text/javascript" src="./js/jquery/jquery.jqGrid-4.7.0/js/grid.locale-es.js"></script>
        <script type="text/javascript" src="./js/jquery/jquery.jqGrid-4.7.0/js/jquery.jqGrid.min.js"></script>
        <script type="text/javascript" src="./js/jquery/jquery.jqGrid-4.7.0/js/jquery.tabletojson.js"></script>
        <script type="text/javascript" src="./js/generaIA.js"></script>
        <title>Generar ia Reestructuracion</title>
    </head>
    <body>
        <div id="capaSuperior"  style="position:absolute; width:100%; height:100%; z-index:0; left: 0px; top: 0px;">
            <jsp:include page="/WEB-INF/toptsp.jsp?encabezado=APROBAR NEGOCIOS"/>
        </div>
        <div id="capaCentral" style="position:absolute; width:1700px; height:100%; z-index:0; left: 0px; top: 100px; ">

            <label for="nro_extracto">Numero Extracto</label>
            <input type="text" value="" id="nro_extracto" name="nro_extracto" />
            <button id="generaria" class="ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only" role="button" aria-disabled="false">
                <span class="ui-button-text">Generar</span>
            </button>
        </div>

        <div id="dialogo" class="ventana" title="Mensaje">
            <p  id="msj">texto </p>
        </div>
    </body>
</html>
