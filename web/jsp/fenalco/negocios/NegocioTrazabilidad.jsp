<%--
    Document   : NegocioTrazabilidad
    Created on : 05/12/2011, 04:43:44 PM
    Author     : ivargas
--%>
<%@page session="true"%>
<%@page import="java.util.*"%>
<%@page import="com.tsp.operation.model.*"%>
<%@page import="com.tsp.operation.model.beans.*"%>
<%@page import="com.tsp.operation.model.services.*"%>
<%@page contentType="text/html" pageEncoding="ISO-8859-1"%>
<%
            response.addHeader("Cache-Control", "no-cache");
            response.addHeader("Pragma", "No-cache");
            response.addDateHeader("Expires", 0);
            String BASEURL = request.getContextPath();
            String CONTROLLER = BASEURL + "/controller";
            Usuario usuario = (Usuario) session.getAttribute("Usuario");
            String form = request.getParameter("form") != null ? request.getParameter("form") : "";
            NegocioTrazabilidadService serv = new NegocioTrazabilidadService(usuario.getBd());
            ArrayList<NegocioTrazabilidad> trazas = serv.buscarNegocioTrazabilidad(Integer.parseInt(form));

%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
        <link href="<%= BASEURL%>/css/estilostsp.css" rel='stylesheet'>
        <script type='text/javascript' src="<%= BASEURL%>/js/boton.js"></script>
        <script type='text/javascript' src="<%= BASEURL%>/js/prototype.js"></script>
        <script type='text/javascript' src="<%= BASEURL%>/js/negocioTrazabilidad.js"></script>
        <title>Trazabilidad del Negocio</title>
    </head>
    <body onload="inicializar('<%=CONTROLLER%>', '<%= BASEURL%>');">
       
        
        <%-- Cambio de lineas 
        <div id="capaSuperior" style="position:absolute; width:100%; height:100px; z-index:0; left: 0px; top: 0px;">
            <jsp:include page="/toptsp.jsp?encabezado=Trazabilidad del Negocio"/>
        </div>
        <div id="capaCentral" style="position:absolute; width:100%; height:83%; z-index:0; left: 0px; top: 100px; overflow:auto;">
        --%>    
            
        <div id="capaCentral" style="position:absolute; width:100%; height:100%; z-index:0; left: 0px; top: 0px; overflow:auto;">    
            
            
            <form id="formulario" name="formulario" action="<%=CONTROLLER%>?estado=Negocios&accion=Trazabilidad" method="post">
                <table align="center" width="90%" border="0">
                    <tbody>
                        <tr>
                            <td>
                                <table width="99%" border="1" style="border-collapse: collapse;" id="traza" >
                                    <thead>

                                        <tr class="subtitulo1">
                                            <th width="2%">Item</th>
                                            <th width="8%">Formulario</th>
                                            <th width="10%">Actividad</th>
                                            <th width="10%">Usuario</th>
                                            <th width="10%"class="date-iso">Fecha</th>
                                            <th width="10%">Negocio</th>
                                            <th width="30%">Comentarios</th>
                                            <th width="30%">Comentario StandBy</th>
                                            <th width="10%" >Concepto</th>
                                            <th width="10%">Causal</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <%if (trazas.size() > 0) {
                              for (int i = 0; i < trazas.size(); i++) {%>
                                        <tr class="fila">
                                            <td width="2%" align="center"><%=i + 1%></td>
                                            <td width="8%" align="center"><%=trazas.get(i).getNumeroSolicitud()%></td>
                                            <td width="10%" align="center"><%=trazas.get(i).getActividad()%></td>
                                            <td width="10%" align="center"> <%=trazas.get(i).getUsuario()%></td>
                                            <td width="10%" align="center"><%=trazas.get(i).getFecha()%></td>
                                            <td width="10%" align="center"><%=trazas.get(i).getCodNeg()%></td>
                                            <td width="20" align="center"><%=trazas.get(i).getComentarios()%></td>
                                            <td width="20" align="center"><%=trazas.get(i).getComentario_standby()%></td>
                                            <td width="10%" align="center"><%=trazas.get(i).getConcepto()%></td>
                                            <td width="10%" align="center"><%=trazas.get(i).getCausal()%></td>
                                        </tr>
                                        <%}
                            }else{%>
                            <tr class="fila">
                                            <td  colspan="9">No hay registros</td>
                                        </tr>
<%}%>
                                    </tbody>
                                </table>
                                        <br/>
                                <div align="center">

                                    <img alt="" src = "<%=BASEURL%>/images/botones/salir.gif" style = "cursor:pointer" name = "imgsalir" onclick = "Javascript:parent.closewin()" onmouseover="botonOver(this);" onmouseout="botonOut(this);">

                                </div>

                            </td>
                        </tr>
                    </tbody>
                </table>
            </form>
        </div>
    </body>
</html>

