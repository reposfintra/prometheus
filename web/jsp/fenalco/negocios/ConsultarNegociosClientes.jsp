<%@page import="com.tsp.operation.model.beans.Usuario"%>
<%@page import="com.tsp.operation.model.services.GestionSolicitudAvalService"%>
<%@page contentType="text/html"%>
<%@page session="true"%>
<%@page import="java.util.*"%>
<%@page import="com.tsp.util.*"%>
<%@include file="/WEB-INF/InitModel.jsp"%>

<%
           Usuario usuario = (Usuario) request.getSession().getAttribute("Usuario");
           String path = application.getRealPath(model.menuService.getRealUrl(request.getRequestURI()));
           String datos[] = model.menuService.getContenidoMenu(BASEURL, request.getRequestURI(), path);
           GestionSolicitudAvalService gsaserv = new GestionSolicitudAvalService(usuario.getBd());
           String vista=request.getParameter("vista")!=null?request.getParameter("vista"):"";
		   String mensaje=request.getParameter("mensaje")!=null?request.getParameter("mensaje"):"";
           ArrayList est_neg = gsaserv.busquedaGeneral("ESTADO_NEG");
%>
<html>
    <head>

        <title>Consultar Negocios</title>        
        <script src='<%=BASEURL%>/js/date-picker.js'></script>
        <script language="JavaScript" type="text/javascript" src="<%=BASEURL%>/js/boton.js"></script>
        <script type='text/javascript' src="<%=BASEURL%>/js/validar.js"></script>
        <link href="../css/estilostsp.css" rel="stylesheet" type="text/css">
        <script>
            function validar( theForm ){
                var sw = 0;
                var cont=0;
                for(var i=0;i<theForm.length;i++){
                    var ele = theForm.elements[i];
                    if(ele.type=='checkbox' && ele.checked  ){
                        cont++;
                        if( ele.name=='ckestado'){
                            if( theForm.estados.value     =='...'){
                                alert('Deber� seleccionar el estado');
                                sw=1;
                                theForm.estados.focus();
                                break;
                            }
                        }
                        if( ele.name=='ckdate'){
                            if( theForm.fecini.value =='')
                            {  alert('Deber� establecer la fecha inicial');
                                sw=1;
                                theForm.fecini.focus();  break;
                            }
                            if( theForm.fecfin.value =='')
                            {  alert('Deber� establecer la fecha final');
                                sw=1;
                                theForm.fecfin.focus();  break;
                            }
                            if ( theForm.fecini.value != "" && theForm.fecfin.value != "" ) {
                                var fecha1 = theForm.fecini.value.replace( /-|:| /gi, '' );
                                var fecha2 = theForm.fecfin.value.replace( /-|:| /gi, '' );
									 
                                var fech1 = parseFloat( fecha1 );
                                var fech2 = parseFloat( fecha2 );
									 
                                if( fecha2 < fecha1 ) {
                                    alert( 'La fecha final debe ser mayor que la fecha inicial!' );
                                    return false;
                                }
                            }
                        }
                        if( ele.name=='ckfap'){
                            if( theForm.fecini2.value =='')
                            {  alert('Deber� establecer la fecha inicial');
                                sw=1;
                                theForm.fecini2.focus();  break;
                            }
                            if( theForm.fecfin2.value =='')
                            {  alert('Deber� establecer la fecha final');
                                sw=1;
                                theForm.fecfin2.focus();  break;
                            }
                            if ( theForm.fecini2.value != "" && theForm.fecfin2.value != "" ) {
                                var fecha1 = theForm.fecini2.value.replace( /-|:| /gi, '' );
                                var fecha2 = theForm.fecfin2.value.replace( /-|:| /gi, '' );
									 
                                var fech1 = parseFloat( fecha1 );
                                var fech2 = parseFloat( fecha2 );
									 
                                if( fecha2 < fecha1 ) {
                                    alert( 'La fecha final debe ser mayor que la fecha inicial!' );
                                    return false;
                                }
                            }
                        }
							
                        if( ele.name=='ckfde')  		{
                            if( theForm.fecini3.value =='')
                            {  alert('Deber� establecer la fecha inicial');
                                sw=1;
                                theForm.fecini3.focus();  break;
                            }
                            if( theForm.fecfin3.value =='')
                            {  alert('Deber� establecer la fecha final');
                                sw=1;
                                theForm.fecfin3.focus();  break;
                            }
                            if ( theForm.fecini3.value != "" && theForm.fecfin3.value != "" ) {
                                var fecha1 = theForm.fecini3.value.replace( /-|:| /gi, '' );
                                var fecha2 = theForm.fecfin3.value.replace( /-|:| /gi, '' );
									 
                                var fech1 = parseFloat( fecha1 );
                                var fech2 = parseFloat( fecha2 );
									 
                                if( fecha2 < fecha1 ) {
                                    alert( 'La fecha final debe ser mayor que la fecha inicial!' );
                                    return false;
                                }
                            }
                        }
                        if( ele.name=='cknita'){
                            if( theForm.nita.value ==''){
                                alert('Deber� establecer nit del afiliado');
                                sw=1; theForm.nita.focus();
                                break;
                            }
                        }
                        if( ele.name=='cknoma'){
                            if( theForm.noma.value    =='')
                            {   alert('Deber� establecer el nombre del afiliado');
                                sw=1;
                                theForm.noma.focus();
                                break;
                            }
                        }
                        if( ele.name=='cknitc'){
                            if( theForm.nitc.value       ==''){
                                alert('Deber� establecer el nit del cliente');
                                sw=1; theForm.nitc.focus();
                                break;
                            }
                        }
                        if( ele.name=='cknomc')
                        {  if( theForm.nomc.value   =='')
                            {  alert('Deber� establecer el nombre del cliente');
                                sw=1;
                                theForm.nomc.focus();
                                break;
                            }
                        }
                        if( ele.name=='cknitcod'){
                            if( theForm.nitcod.value       ==''){
                                alert('Deber� establecer el nit del codeudor');
                                sw=1;
                                theForm.nitc.focus();
                                break;
                            }
                        }
                        if( ele.name=='cknomcod')
                        {  if( theForm.nomcod.value   =='')
                            {  alert('Deber� establecer el nombre del codeudor');
                                sw=1;
                                theForm.nomc.focus();
                                break;
                            }
                        }
                    }
                }
                if(cont==0)
                {
                    alert('Deber� seleccionar una opcion');
                    sw=1;
                }
                if(sw==0)
                    theForm.submit();
            }

	
        </script>
    </head>
    <body onResize="redimensionar()" onLoad="redimensionar()">
        <div id="capaSuperior" style="position:absolute; width:100%; height:100px; z-index:0; left: 1px; top: 0px;">
            <jsp:include page="/toptsp.jsp?encabezado=Consulta de Negocios"/>
        </div>
        <div id="capaCentral" style="position:absolute; width:100%; height:87%; z-index:0; left: 0px; top: 100px; overflow: scroll;">

            <form name='formulario' method='post' id="formulario" action="<%=CONTROLLER%>?estado=Negocios&accion=Ver&op=1&vista=<%=vista%>" >
                <table width="80%"  border="2" align="center">
                    <tr>
                        <td><table width="100%"  border="0" class="tablaInferior">

                                <tr>
                                    <td class="subtitulo1" colspan="4">Consulta de Negocios </td>
                                    <td colspan="3" class="barratitulo"><img src="<%=BASEURL%>/images/titulo.gif" width="32" height="20" align="left"><%=datos[0]%></td>
                                </tr>

                                <tr class="fila">
                                    <td width="5%" align="left">&nbsp;
                                        <input name="ckestado" type="checkbox" id="ckestado"  <%=vista.equals("15")?"checked disabled ":"" %> ></td>
                                    <td width="20%" colspan="2" align="left">Estado</td>
                                    <td width="15%" colspan="4" align="left">
                                        <select name="estados" id="estados" <%if (vista.equals("15")){%> disabled  <%} %>>
                                            <option value="">...</option>
                                            <%String  dato[] = null;
                                                         for (int i = 0; i < est_neg.size(); i++) {
                                                             dato = ((String) est_neg.get(i)).split(";_;");%>
                                            <option value="<%=dato[0]%>" <%if (dato[0].equals("R")&& vista.equals("15")){%> selected <%} %> ><%=dato[1]%></option>
                                            <%  }%>
                                        </select>
                                    </td>

                                </tr>

                                <tr class="fila">
                                    <td align="left">&nbsp;
                                        <input name="ckdate" type="checkbox" id="ckdate" ></td>
                                    <td colspan="2" align="left">Fecha de Negocio</td>
                                    <td width="15%" align="left">&nbsp;Fecha Inicial</td>
                                    <td width="15%" align="left">

                                        <input name="fecini" type="text" class="textbox" id="fecini" size="10" readonly>
                                        <span class="Letras"><img src="<%=BASEURL%>/images/cal.gif" width="16" height="16" align="absmiddle" style="cursor:hand " onClick="if(self.gfPop)gfPop.fPopCalendar(document.formulario.fecini);return false;" HIDEFOCUS></span>

                                    </td>
                                    <td width="15%" align="left">&nbsp;Fecha Final</td>
                                    <td width="15%" align="left">

                                        <input name="fecfin" type="text" class="textbox" id="fecfin" size="10" readonly>
                                        <span class="Letras"><a href="javascript:void(0)" onClick="jscript: show_calendar('fecfin');" HIDEFOCUS> </a> <img src="<%=BASEURL%>/images/cal.gif" width="16" height="16" align="absmiddle" style="cursor:hand " onClick="if(self.gfPop)gfPop.fPopCalendar(document.formulario.fecfin);return false;" HIDEFOCUS></span>

                                    </td>
                                </tr>

                                <tr class="fila">
                                    <td align="left">&nbsp;
                                        <input name="ckfap" type="checkbox" id="ckfap" ></td>
                                    <td colspan="2" align="left">Fecha de Aprobacion </td>
                                    <td width="15%" align="left">&nbsp;Fecha Inicial</td>
                                    <td width="15%" align="left">

                                        <input name="fecini2" type="text" class="textbox" id="fecini2" size="10" readonly>
                                        <span class="Letras"><img src="<%=BASEURL%>/images/cal.gif" width="16" height="16" align="absmiddle" style="cursor:hand " onClick="if(self.gfPop)gfPop.fPopCalendar(document.formulario.fecini2);return false;" HIDEFOCUS></span>

                                    </td>
                                    <td width="15%" align="left">&nbsp;Fecha Final</td>
                                    <td width="15%" align="left">

                                        <input name="fecfin2" type="text" class="textbox" id="fecfin2" size="10" readonly>
                                        <span class="Letras"><a href="javascript:void(0)" onClick="jscript: show_calendar('fecfin2');" HIDEFOCUS> </a> <img src="<%=BASEURL%>/images/cal.gif" width="16" height="16" align="absmiddle" style="cursor:hand " onClick="if(self.gfPop)gfPop.fPopCalendar(document.formulario.fecfin2);return false;" HIDEFOCUS></span>

                                    </td>
                                </tr>

                                <tr class="fila">
                                    <td align="left">&nbsp;
                                        <input name="ckfde" type="checkbox" id="ckfde" ></td>
                                    <td colspan="2" align="left">Fecha de Desembolso </td>
                                    <td width="15%" align="left">&nbsp;Fecha Inicial</td>
                                    <td width="15%" align="left">

                                        <input name="fecini3" type="text" class="textbox" id="fecini3" size="10" readonly>
                                        <span class="Letras"><img src="<%=BASEURL%>/images/cal.gif" width="16" height="16" align="absmiddle" style="cursor:hand " onClick="if(self.gfPop)gfPop.fPopCalendar(document.formulario.fecini3);return false;" HIDEFOCUS></span>

                                    </td>
                                    <td width="15%" align="left">&nbsp;Fecha Final</td>
                                    <td width="15%" align="left">

                                        <input name="fecfin3" type="text" class="textbox" id="fecfin3" size="10" readonly>
                                        <span class="Letras"><a href="javascript:void(0)" onClick="jscript: show_calendar('fecfin3');" HIDEFOCUS> </a> <img src="<%=BASEURL%>/images/cal.gif" width="16" height="16" align="absmiddle" style="cursor:hand " onClick="if(self.gfPop)gfPop.fPopCalendar(document.formulario.fecfin3);return false;" HIDEFOCUS></span>

                                    </td>
                                </tr>


                                <tr class="fila">
                                    <td align="left">&nbsp;
                                        <input name="cknita" type="checkbox" id="cknita" ></td>
                                    <td colspan="2" align="left">&nbsp;Nit o CC Afililado</td>
                                    <td colspan="4" align="left"><input name="nita" id="nita" type="text" class="textbox" size="20" maxlength="20"></td>
                                </tr>

                                <tr class="fila">
                                    <td align="left">&nbsp;
                                        <input name="cknoma" type="checkbox" id="cknoma" ></td>
                                    <td colspan="2" align="left">&nbsp;Nombre Afililado</td>
                                    <td colspan="4" align="left"><input name="noma" id="noma" type="text" class="textbox" size="40" maxlength="40"></td>
                                </tr>

                                <tr class="fila">
                                    <td align="left">&nbsp;
                                        <input name="cknitc" type="checkbox" id="cknitc"></td>
                                    <td colspan="2" align="left">&nbsp;Nit o CC Cliente</td>
                                    <td colspan="4" align="left"><input name="nitc" id="nitc" type="text" class="textbox" size="20" maxlength="20"></td>
                                </tr>
                                <tr class="fila">
                                    <td align="left">&nbsp;
                                        <input name="cknomc" type="checkbox" id="cknomc" ></td>
                                    <td colspan="2" align="left">&nbsp;Nombre Cliente</td>
                                    <td colspan="4" align="left"><input name="nomc" id="nomc" type="text" class="textbox" size="40" maxlength="40"></td>
                                </tr>

                                <tr class="fila">
                                    <td align="left">&nbsp;
                                        <input name="cknitcod" type="checkbox" id="cknitcod"></td>
                                    <td colspan="2" align="left">&nbsp;Nit o CC Codeudor</td>
                                    <td colspan="4" align="left"><input name="nitcod" id="nitcod" type="text" class="textbox" size="20" maxlength="20"></td>
                                </tr>
                                <tr class="fila">
                                    <td align="left">&nbsp;
                                        <input name="cknomcod" type="checkbox" id="cknomcod" ></td>
                                    <td colspan="2" align="left">&nbsp;Nombre Codeudor</td>
                                    <td colspan="4" align="left"><input name="nomcod" id="nomcod" type="text" class="textbox" size="40" maxlength="40"></td>
                                </tr>
                                <tr class="fila">
                                  <td align="left">&nbsp;
                                        <input name="ckcodxc" type="checkbox" id="ckcodxc" ></td>
                                  <td colspan="2" align="left">&nbsp;C&oacute;digo</td>
                                  <td colspan="4" align="left"><input name="codxc" id="codxc" type="text" class="textbox" size="40" maxlength="10"></td>
                                </tr>
                                <tr class="fila">
                                    <td align="left">&nbsp;
                                        <input name="exportar" type="checkbox" id="exportar" ></td>
                                    <td colspan="2" align="left">Exportar Excel</td>
                                    <td colspan="4" align="left">&nbsp;</td>
                                </tr>
                            </table></td>
                    </tr></table>
                <table align="center">
                    <tr>
                        <td colspan="2" nowrap align="center">
                            <img src="<%=BASEURL%>/images/botones/aceptar.gif"    height="21"  title='Consultar'  onclick='validar(formulario);'   onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" style="cursor:hand">
                            <img src="<%=BASEURL%>/images/botones/salir.gif"      height="21"  title='Salir'      onClick="window.close();"     onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" style="cursor:hand">
                    </tr>

                </table>
            </form >
                        <%if(!mensaje.equals("")){%>
           <table width="367" border="2" align="center">
      <tr>
        <td>

	      <table width="100%"  border="1" align="center"  bordercolor="#F7F5F4" bgcolor="#FFFFFF">
            <tr>
              <td align="center" class="mensajes">Se ha iniciado la exportaci&oacute;n del reporte <br>
              en formato MS Excel. </td>
              <td width="29"><img src="<%=BASEURL%>/images/cuadronaranja.JPG"></td>
            </tr>
          </table>
	    </td>
      </tr>
</table>
            <%}%>
            
            
</div>
        <iframe width=188 height=166 name="gToday:normal:agenda.js:gfPop:plugins.js" id="gToday:normal:agenda.js:gfPop:plugins.js" src="<%=BASEURL%>/js/Calendario/ipopeng.htm" scrolling="no" frameborder="0" style="visibility:visible; z-index:999; position:absolute; left:-500px; top:0px;"> </iframe>
        <%=datos[1]%>
    </body>
</html>
