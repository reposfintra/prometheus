<!--
- Autor : Alejandro Payares
- Date  : Diciembre 24 del 2005
- Copyrigth Notice : Fintravalores S.A. S.A
- Descripcion : Pagina JSP que permite editar la información de un usuario.
- Ultima modificación: Julio 27 de 2007
-->
<%@ page session="true" %>
<%@ page errorPage="/ErrorPage.jsp" %>
<%@ page import="java.util.*" %>
<%@ page import="com.tsp.operation.model.beans.*" %>
<%@ include file="/WEB-INF/InitModel.jsp" %>
<%@ taglib uri="http://jakarta.apache.org/taglibs/input-0.90" prefix="input" %>
<%@ taglib uri="http://www.sanchezpolo.com/sot" prefix="tsp"%>
<%@page import="com.tsp.util.*"%>
<%@page import="com.tsp.util.Util.*"%>

<head>
    <title>.: Datos del Negocio :.</title>
    <link href='<%=BASEURL%>/css/estilostsp.css' rel='stylesheet' type='text/css'>
    <link href='../css/estilostsp.css' rel='stylesheet' type='text/css'>
    <script src='<%=BASEURL%>/js/boton.js' language='javascript'></script>
	<script language='javascript'>	
	function exporta()
	{
		window.open('<%= CONTROLLER %>?estado=Negocios&accion=Ver&op=3','EXP','status=yes,scrollbars=yes,width=400,height=150,resizable=no');
	}
</script>
</head>
<body onresize="redimensionar()" onload='redimensionar()'>
    <div id="capaSuperior" style="position:absolute; width:100%; z-index:0; left: 0px; top: 0px;">
        <jsp:include page="/WEB-INF/toptsp.jsp?encabezado=VER DATOS DEL NEGOCIO"/>
    </div>
<div id="capaCentral" style="position:absolute; width:100%; height:87%; z-index:0; left: 0px; top: 100px; overflow: scroll;"> 
<p>&nbsp;</p>
<%Usuario loggedUser = (Usuario) session.getAttribute("Usuario");
Vector af =  model.Negociossvc.getListNeg();
session.setAttribute("vneg",af);
			double sum1=0,sum2=0;
			double sum_tot_pagado=0;
			int dia=0; 
			int mes=0; 
			int ano=0;
			String fechai=null;
			if(af.size() > 0)
			{
	       		%>      
            <table  border = '2' align = 'center'  width="100%">
				<tr>
					 <td align="center" class="subtitulo1">Item</td>
					 <td align="center" class="subtitulo1">Cod Negocio</td>
					 <td align="center" class="subtitulo1">Generar Letras </td>
					 <td align="center" class="subtitulo1">Afiliado</td>
					 <td align="center" class="subtitulo1">Cliente</td>
					 <td align="center" class="subtitulo1">Id_Cliente</td>
					 <td align="center" class="subtitulo1">Vr_Negocio</td>
					 <td align="center" class="subtitulo1">Vr_Desembolso</td>
					 <td align="center" class="subtitulo1">Vr_Documentos</td>
					 <td align="center" class="subtitulo1">Estado</td>
					  <%if( !loggedUser.getTipo().equals("CLIENTETSP") ){%>
					 <td align="center" class="subtitulo1">Observaciones</td>
					 <%}%>
					  <td align="center" class="subtitulo1">Ver Liquidacion</td>
					 <td align="center" class="subtitulo1">Fecha Negocio</td>
					 <td align="center" class="subtitulo1">Fecha Desembolso</td>
					 <td align="center" class="subtitulo1">Tipo Doc</td>
					 <td align="center" class="subtitulo1">No_Docs</td>
					 <td align="center" class="subtitulo1">Plazo</td>
					 <td align="center" class="subtitulo1">Modo Aval</td>
					 <td align="center" class="subtitulo1">Modo Custodia</td>
					 <td align="center" class="subtitulo1">Modo Remesa</td>
					 <td align="center" class="subtitulo1">Aval</td>
				</tr>
				<%for (int i=0; i<af.size(); i++)
					{
				Negocios  neg = (Negocios)af.get(i);
				sum1=sum1+(neg.getVr_negocio());
				sum_tot_pagado=sum_tot_pagado+neg.getTotpagado();
				sum2=sum2+(neg.getVr_desem());%>
				<tr class='<%= (i%2==0?"filagris":"filaazul") %>'>
					 <td align="center"  class="bordereporte"  nowrap style="font size:12"><%=i+1%></td>
					 <td  class="bordereporte"  nowrap style="font size:12"><%=neg.getCod_negocio()%></td>
					 <td  class="bordereporte"  nowrap style="font size:12" align="center">
					 <%if((neg.getTneg()).equals("02")){%>
					   <img src="<%=BASEURL%>/images/botones/iconos/modificar.gif" name="exportar" id="exportar" onClick="javascript:window.open('<%=CONTROLLER%>?estado=Negocios&accion=Update&op=9&codn=<%=neg.getCod_negocio()%>')" title="Generar Letras" style="cursor:hand ">
					   <%}%>
					 </td>
					 <td  class="bordereporte"  nowrap style="font size:12"><%=neg.getNitp()%></td>
					 <td  class="bordereporte"  nowrap style="font size:12"><%=neg.getNom_cli()%></td>
					 <td class="bordereporte"  nowrap style="font size:12"><%=neg.getCod_cli()%></td>
					 <td  class="bordereporte"  nowrap style="font size:12"><%if(neg.getCmc().equals("01")){%><%=Util.customFormat(neg.getVr_negocio())%><%}else{%><%=Util.customFormat(neg.getVr_desem())%><%}%></td>
					 <td class="bordereporte"  nowrap style="font size:12"><%=Util.customFormat(neg.getVr_desem())%></td>
					 <td class="bordereporte"  nowrap style="font size:12"><%=Util.customFormat(neg.getTotpagado())%></td>
					 <td class="bordereporte"  nowrap style="font size:12"><%=neg.getEstado()%></td>
					 <%if( !loggedUser.getTipo().equals("CLIENTETSP") ){%>
					 <td class="bordereporte"  nowrap style="font size:12"><%=neg.getObs()%></td>
					 <%}%>
					   <%if (neg.getCmc().equals("01")){%>
					 	<td  class="bordereporte"  nowrap style="font size:12"><a href="#" onClick="javascript:window.open('<%=CONTROLLER%>?estado=Fenalco&accion=Fintra&opcion=Ejecutar&numero_cheques=<%=neg.getNodocs()%>&valor_desembolso=<%=neg.getVr_negocio()%>&forma_pago=<%=neg.getFpago()%>&fechainicio=<%=neg.getFecha_neg()%>&nit=<%=neg.getCod_cli()%>&custodia=<%=neg.getMod_cust()%>&aval=<%=neg.getMod_aval()%>&tipo_negocio=<%=neg.getTneg()%>&cod_neg=<%=neg.getCod_negocio()%>&remesas=<%=neg.getEsta()%>&codtb=<%=neg.getCod_tabla()%>&remesa=<%=neg.getMod_rem()%>&nomcli=<%=neg.getNom_cli()%>&nitp=<%=neg.getBcod()%>&state=<%=neg.getEstado()%>&conlet=<%=neg.getConlet()%>&val_des_remix=<%=neg.getVr_desem()%>&pagarex=<%=neg.getPagare()%>')">Ver Liquidacion</a></td>
					 <%}else{%>
					  <td  class="bordereporte"  nowrap style="font size:12"><a href="#" onClick="javascript:window.open('<%=CONTROLLER%>?estado=creacion&accion=CompraCartera&opcion=consultar&consultablex=<%=neg.getCod_negocio()%>&nit=<%=neg.getCod_cli()%>&proveedor=<%=neg.getBcod()%>&est=<%=neg.getEstado()%>')">Ver Compra</a></td>
					 <%}%>
					 <td  class="bordereporte"  nowrap style="font size:12"><%=Util.MFecha(neg.getFecha_neg())%></td>
					 <td  class="bordereporte"  nowrap style="font size:12"><%=Util.MFecha(neg.getFechatran())%></td>
					 <td  class="bordereporte"  nowrap style="font size:12"><%if((neg.getTneg()).equals("01")){%>Cheque<%}else{%><a href="#" onClick="javascript:window.open('<%=CONTROLLER%>?estado=Negocios&accion=Update&op=9&codn=<%=neg.getCod_negocio()%>')"  title="Generar Letras">Letra</a><%}%></td>
					 <td  class="bordereporte"  nowrap style="font size:12"><%=neg.getNodocs()%></td>
					 <%if (neg.getCmc().equals("01")){%>
					 <td  class="bordereporte"  nowrap style="font size:12"><%=neg.getFpago()%> Dias</td>
					  <%}else{%>
					   <td  class="bordereporte"  nowrap style="font size:12"></td>
					  <%}%>
					 <%if (neg.getCmc().equals("01")){%>
					 <td  class="bordereporte"  nowrap style="font size:12"><%if((neg.getMod_aval()).equals("0")){%>Asume establecimiento de comercio<%}else{%>Asume Cliente<%}%></td>
					 <%}else{%>
					 <td  class="bordereporte"  nowrap style="font size:12"></td>
					  <%}%>
					 <td  class="bordereporte"  nowrap style="font size:12"><%if((neg.getMod_cust()).equals("0")){%>Asume establecimiento de comercio<%}else{%>Asume Cliente<%}%></td>
					 <td  class="bordereporte"  nowrap style="font size:12"><%if((neg.getMod_rem()).equals("0")){%>Con Remesa<%}else{%>Sin Remesa<%}%></td>
					 <td  class="bordereporte"  nowrap style="font size:12"><%=neg.getNumaval()%></td>
					 
				</tr>
				<%}%>
				 	<td align="right" class="bordereporte" colspan="5"><strong>Totales</strong></td>
					<td align="center" class="bordereporte" colspan="1"></td>
				 	<td align="center" class="bordereporte" style="font size:12"><strong><%=Util.customFormat(sum1)%></strong></td>
					<td align="center" class="bordereporte" style="font size:12"><strong><%=Util.customFormat(sum2)%></strong></td>
					<td align="center" class="bordereporte" style="font size:12"><strong><%=Util.customFormat(sum_tot_pagado)%></strong></td>
					<td align="center" class="bordereporte" colspan="9"></td>
			</table>

		<div align="center">
			<%if( !loggedUser.getTipo().equals("CLIENTETSP") ){%>
				<img src="<%=BASEURL%>/images/botones/exportarExcel.gif" name="exportar" id="exportar" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" onClick='javascript:exporta()' >
			 <%}%>
			<tsp:boton name="regresar" value="regresar" onclick="javascript:history.back(1)" />
			<tsp:boton name="salir" value="salir" onclick="javascript:window.close()" />
		</div>
            <%}else{%>
	 <table border="2" align="center">
    <tr>
      <td><table width="410" height="73" border="1" align="center"  bordercolor="#F7F5F4" bgcolor="#FFFFFF">
          <tr>
            <td width="282" align="center" class="mensajes">Su b&uacute;squeda no arroj&oacute; resultados!</td>
            <td width="28" background="<%=BASEURL%>/images/cuadronaranja.JPG">&nbsp;</td>
            <td width="78">&nbsp;</td>
          </tr>
      </table></td>
    </tr>
  </table>
     <div align="center"><tsp:boton name="regresar" value="regresar" onclick="javascript:history.back(1)" />
       <tsp:boton name="salir" value="salir" onclick="javascript:window.close()" />
       <%}%>
     </div>
</div>
</body>
</html>
