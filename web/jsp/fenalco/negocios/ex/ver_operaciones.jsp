<!--
- Autor : Alejandro Payares
- Date  : Diciembre 24 del 2005
- Copyrigth Notice : Fintravalores S.A. S.A
- Descripcion : Pagina JSP que permite editar la información de un usuario.
- Ultima modificación: Julio 27 de 2007
-->
<%@ page session="true" %>
<%@ page errorPage="/ErrorPage.jsp" %>
<%@ page import="java.util.*" %>
<%@ page import="com.tsp.operation.model.beans.*" %>
<%@ include file="/WEB-INF/InitModel.jsp" %>
<%@ taglib uri="http://jakarta.apache.org/taglibs/input-0.90" prefix="input" %>
<%@ taglib uri="http://www.sanchezpolo.com/sot" prefix="tsp"%>
<%@page import="com.tsp.util.*"%>
<%@page import="com.tsp.util.Util.*"%>

<head>
    <title>.: Datos del Negocio :.</title>
    <link href='<%=BASEURL%>/css/estilostsp.css' rel='stylesheet' type='text/css'>
    <link href='../css/estilostsp.css' rel='stylesheet' type='text/css'>
    <script src='<%=BASEURL%>/js/boton.js' language='javascript'></script>
	<script language='javascript'>	
	function exporta()
	{
		window.open('<%= CONTROLLER %>?estado=Operaciones&accion=Ver&op=3','EXP','status=yes,scrollbars=yes,width=400,height=150,resizable=no');
	}
</script>
</head>
<body onresize="redimensionar()" onload='redimensionar()'>
    <div id="capaSuperior" style="position:absolute; width:100%; z-index:0; left: 0px; top: 0px;">
        <jsp:include page="/WEB-INF/toptsp.jsp?encabezado=ORDENES PENDIENTES"/>
    </div>
    <div id="capaCentral" style="position:absolute; width:100%; height:87%; z-index:0; left: 0px; top: 100px; overflow: scroll;"> 
    <p>&nbsp;</p>
<%  Usuario loggedUser = (Usuario) session.getAttribute("Usuario");
    ArrayList af =  model.Negociossvc.getOperaciones("2020-12-12","2000-12-11");
    session.setAttribute("cont",af);
    ArrayList cab= new ArrayList();
    cab.add("ORDEN_TRABAJO");cab.add("COD_SUC");cab.add("BENEFICIARIO");cab.add("CIUDAD_GIRAD");cab.add("NOMBRE");
    cab.add("CED_GIRADOR");cab.add("NUM_CHEQUE");cab.add("CTA_GIRADOR");cab.add("COD_ENT_CQUE");cab.add("VALOR_CHEQUE");
    cab.add("PLAZA_CHEQUE");cab.add("FEC_CONSIGNA");cab.add("CTA_CONSIGNA");cab.add("NOM_CONSIGNA");cab.add("NUM_OBL_CHEQ");
    cab.add("DIR_DESTINO");cab.add("CIUD_CUSTOD");
    session.setAttribute("cab",cab);
    double sum1=0,sum2=0;
    double sum_tot_pagado=0;
    int dia=0;
    int mes=0;
    int ano=0;
    String fechai=null;
    if(af.size() > 0)
    {%> <table  border = '2' align = 'center'  width="100%">
        <tr>
            <td align="center" class="subtitulo1">ORDEN_TRABAJO</td>
            <td align="center" class="subtitulo1">COD_SUC</td>
            <td align="center" class="subtitulo1">BENEFICIARIO</td>
            <td align="center" class="subtitulo1">CIUDAD_GIRAD</td>
            <td align="center" class="subtitulo1">NOMBRE</td>
            <td align="center" class="subtitulo1">CED_GIRADOR</td>
            <td align="center" class="subtitulo1">NUM_CHEQUE</td>
            <td align="center" class="subtitulo1">CTA_GIRADOR</td>
            <td align="center" class="subtitulo1">COD_ENT_CQUE</td>
            <td align="center" class="subtitulo1">VALOR_CHEQUE</td>
            <td align="center" class="subtitulo1">PLAZA_CHEQUE</td>
            <td align="center" class="subtitulo1">FEC_CONSIGNA</td>
            <td align="center" class="subtitulo1">CTA_CONSIGNA</td>
            <td align="center" class="subtitulo1">NOM_CONSIGNA</td>
            <td align="center" class="subtitulo1">NUM_OBL_CHEQ</td>
            <td align="center" class="subtitulo1">DIR_DESTINO</td>
            <td align="center" class="subtitulo1">CIUD_CUSTOD</td>
            </tr>
<%      for (int i=0; i<af.size(); i++)
        {   ArrayList ord= (ArrayList)af.get(i);%>
            <tr class='<%= (i%2==0?"filagris":"filaazul") %>'>
<%          for(int k=0;k<ord.size();k++)
            {%><td  class="bordereporte"  nowrap style="font size:12"><%=(String)ord.get(k)%></td>
<%          }%>
            </tr>
<%      }%>
        </table>
        <div align="center">
            <img src="<%=BASEURL%>/images/botones/exportarExcel.gif" name="exportar" id="exportar" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" onClick='javascript:exporta()' >
            <tsp:boton name="regresar" value="regresar" onclick="javascript:history.back(1)" />
            <tsp:boton name="salir" value="salir" onclick="javascript:window.close()" />
        </div>            
<%  }else
    {%> <table border="2" align="center">
            <tr>
                <td>
                    <table width="410" height="73" border="1" align="center"  bordercolor="#F7F5F4" bgcolor="#FFFFFF">
                        <tr>
                            <td width="282" align="center" class="mensajes">Su b&uacute;squeda no arroj&oacute; resultados!</td>
                            <td width="28" background="<%=BASEURL%>/images/cuadronaranja.JPG">&nbsp;</td>
                            <td width="78">&nbsp;</td>
                        </tr>
                    </table>
                </td>
            </tr>
        </table>
        <div align="center"><tsp:boton name="regresar" value="regresar" onclick="javascript:history.back(1)" />
            <tsp:boton name="salir" value="salir" onclick="javascript:window.close()" />
<%  }%>
        </div>
</div>
</body>
</html>
