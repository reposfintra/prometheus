<%-- 
    Document   : FormalizarReestructuracion
    Created on : 2/09/2014, 09:40:59 AM
    Author     : egonzalez
--%>

<%@page contentType="text/html" pageEncoding="ISO-8859-1"%>
<%@ include file="/WEB-INF/InitModel.jsp"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
        <link href="<%=BASEURL%>/css/estilostsp.css" rel="stylesheet" type="text/css">
        <link href="<%=BASEURL%>/css/reestructuracion.css" type="text/css" rel="stylesheet" />
        <link href="<%=BASEURL%>/css/buttons/botonVerde.css" type="text/css" rel="stylesheet" />
        <link href="<%=BASEURL%>/css/popup.css" rel="stylesheet" type="text/css">
        <link type="text/css" rel="stylesheet" href="<%=BASEURL%>/css/jquery/jquery-ui/jquery-ui.css"/>

        <script type="text/javascript" src="<%=BASEURL%>/js/jquery/jquery.jqGrid/js/jquery.min.js"></script>
        <script type="text/javascript" src="<%=BASEURL%>/js/jquery-ui-1.8.5.custom.min.js"></script>
        <script type="text/javascript" src="<%=BASEURL%>/js/jquery/jquery.jqGrid/js/grid.locale-es.js"></script>
        <script type="text/javascript" src="<%=BASEURL%>/js/jquery/jquery.jqGrid/js/jquery.jqGrid.min.js"></script>
        <script type="text/javascript" src="<%=BASEURL%>/js/formalizarReestructuracion.js"></script>

        <title>Formalizar</title>
    </head>
    <body>

        <div id="capaSuperior" style="position:absolute; width:100%; height:100px; z-index:0; left: 0px; top: 0px;">
            <jsp:include page="/WEB-INF/toptsp.jsp?encabezado=FORMALIZAR NEGOCIOS"/>
        </div>
        <div id="capaCentral" style="position:absolute; width:100%; height:85%; z-index:0; left: 0px; top: 100px; overflow:auto">

            <center>
                <br><br>
                <div id="listaNegocios">
                    <table id="lista_tabla_negocios"></table>
                    <div id="page_lista_negocios"></div>
                </div>

                <div id="dialogo" class="ventana" title="Mensaje">
                    <p  id="msj">texto </p>

                </div>
                <div id="dialogo2" class="ventana">
                    <p  id="msj2">texto </p> <br/>
                    <center>
                        <img src="<%=BASEURL%>/images/cargandoCM.gif"/>
                    </center>
                </div>
                <div id="observaciones" class="ventana" title="Observaciones">
                    <p  id="obser" style="font-size:13px">texto </p>

                </div>
            </center>

            <div id="ver_liquidacion" title="Liquidacion" class="ventana">
                <center>
                    <table class="tablaLiquidador">

                        <tr>
                            <th align="left" colspan="2" class="ui-state-default"><strong> Datos Liquidacion</strong></th>
                            <th align="left" colspan="2" class="ui-state-default first"> <img width="32" height="20" align="left" src="/fintra/images/titulo.gif" alt=""> </th>
                        </tr>
                        <tr>
                            <th  class="ui-state-default">Tasa Interes M.V.</th>
                            <td  align="center" class="ui-widget-content first"><label id="interes"></label></td>
                            <th  class="ui-state-default first">Fecha negocio</th>
                            <td  align="center" class="ui-widget-content first"><label id="fneg"></label></td>
                        </tr>
                        <tr>
                            <th class="ui-state-default">No.Cuotas</th>
                            <td align="center" class="ui-widget-content first"><label id="cuota1"></label></td>
                            <th class="ui-state-default first">CAT</th>
                            <td align="center" class="ui-widget-content first"><label id="cat"></label></td>
                        </tr>
                        <tr>
                            <th class="ui-state-default">Valor Seguro</th>
                            <td align="center" class="ui-widget-content first"><label id="vlr_seguro"></label></td>
                            <th class="ui-state-default first">Valor del negocio</th>
                            <td align="center" class="ui-widget-content first"><label id="vlr_negocio"></label></td>
                        </tr>

                    </table>
                </center>
                <div id="liquidador_credito">
                    <table id="tabla_liquidador_neg"></table>
                </div>
            </div>

            <div id="ver_traza" title="Trazabilidad" class="ventana ventana1">
                <center>
                    <div id="div_trazabilidad" ></div>              

                </center>
                <hr/>
                <div>
                    <label for="textarea"><b>Observación</b></label>
                    <textarea id="textarea" cols="85" rows="6" class="textarea2"></textarea>
                </div>
            </div>

            <form id="form_expr" action="" method="post" name="form_expr">
                <input type="hidden" name="pdfBuffer" id="pdfBuffer" value="" />
                <input type="hidden" name="fileName" id="fileName" value="Liquidador" />
                <input type="hidden" name="fileType" id="fileType" value="" />
            </form>

        </div>   

    </body>
</html>
