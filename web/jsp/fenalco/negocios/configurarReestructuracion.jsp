<%-- 
    Document   : configurarReestructuracion
    Created on : 19/03/2015, 10:35:33 AM
    Author     : mcastillo
--%>

<%@page contentType="text/html" pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
        <title>Configuracion reestructuracion</title>

        <link type="text/css" rel="stylesheet" href="./css/jquery/jquery-ui/jquery-ui.css" />
        <link href="./css/popup.css" rel="stylesheet" type="text/css">
        <script type="text/javascript" src="./js/jquery/jquery.jqGrid/js/jquery.min.js"></script>
        <script type="text/javascript" src="./js/jquery-ui-1.8.5.custom.min.js"></script>
        <script type="text/javascript" src="./js/jquery/jquery.jqGrid/js/grid.locale-es.js"></script>
        <script type="text/javascript" src="./js/jquery/jquery.jqGrid/js/jquery.jqGrid.min.js"></script>
        <script type="text/javascript" src="./js/configurarReestructuracion.js"></script>
    </head>
    <body>

        <div id="capaSuperior"  style="position:absolute; width:100%; height:100px; z-index:0; left: 0px; top: 0px;">
            <jsp:include page="/WEB-INF/toptsp.jsp?encabezado=CONFIGURAR REESTRUCTURACION"/>
        </div>
        <div id="capaCentral" style="position:absolute; width:100%; height:100%; z-index:0; left: 0px; top: 100px; ">
            
            <center>
                <br>
                <table id="tabla_config_descuentos"></table>
                <div id="page_tabla_config_descuentos"></div>
                <br><br>         
               <table id="tabla_config_pago_inicial"></table>
            </center>
            
            <div id="dialogMsj" title="Mensaje" style="display:none; visibility: hidden;">
                <p style="font-size: 12px;text-align:justify;" id="msj" > Texto </p>
            </div>  
            
        </div>

    </body>
</html>
