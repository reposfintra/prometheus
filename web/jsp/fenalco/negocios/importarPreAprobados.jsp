<%-- 
    Document   : importarPreAprobados
    Created on : 18/12/2015, 04:54:14 PM
    Author     : mcastillo
--%>

<%@page contentType="text/html" pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
     <head>
        <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
        <link href="./css/popup.css" rel="stylesheet" type="text/css">
        <link type="text/css" rel="stylesheet" href="./css/jquery/jquery-ui/jquery-ui.css" />       
        <link type="text/css" rel="stylesheet" href="./css/TransportadorasApi.css" />
        <script type="text/javascript" src="./js/jquery/jquery.jqGrid/js/jquery.min.js"></script>
        <script type="text/javascript" src="./js/jquery-ui-1.8.5.custom.min.js"></script>   
        <script type="text/javascript" src="./js/jquery/jquery.jqGrid-4.7.0/js/grid.locale-es.js"></script>
        <script type="text/javascript" src="./js/jquery/jquery.jqGrid-4.7.0/js/jquery.jqGrid.min.js"></script>
        <script type="text/javascript" src="./js/jquery/jquery.jqGrid-4.7.0/js/jquery.tabletojson.js"></script>    
        <script type="text/javascript" src="./js/importarPreAprobados.js"></script> 

        <title>IMPORTAR PREAPROBADOS</title>
    </head>
    <body>
        <div id="capaSuperior" style="position:absolute; width:100%; height:100px; z-index:0; left: 0px; top: 0px;">
            <jsp:include page="/WEB-INF/toptsp.jsp?encabezado=IMPORTAR PREAPROBADOS"/>
        </div>
        <div id="capaCentral" style="position:absolute; width:100%; height:80%; z-index:0; left: 0px; top: 100px; ">
            <center>

                <div style="background-color: #FFF;
                     width: 850px;
                     height: 95px;
                     border-radius: 4px;
                     padding: 1px;
                     border: 1px solid #2A88C8;
                     margin: 15px auto 0px; " >

                    <div id="encabezadotablita" style="width: 845px">
                        <label class="titulotablita"><b>IMPORTAR PREAPROBADOS </b></label>
                    </div> 
                    <table border="0" style="width:850px">
                        <tr>
                            <td >
                                <label for="unidad_negocio">Und. de Negocio :</label>                               
                                <select id="unidad_negocio" name="unidad_negocio">
                                </select>                             
                            </td>   
                            <td style="font-size: 12px;width:117px;padding-top: 5px">
                                Seleccione archivo
                            </td>
                            <td style="padding-top:12px">
                                <form id="formulario" name="formulario">
                                    <input type="file" id="examinar" name="examinar"  style="font-size: 11px;width:230px" >
                                </form>
                            </td>
                            <td style="padding-top:  12px">
                                <div id ='botones'>
                                    <button id="subirArchivo" class="ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only" 
                                            role="button" aria-disabled="false" >
                                        <span class="ui-button-text">Subir Archivo</span>
                                    </button> 
                                </div>
                            </td>
                        </tr>
                    </table>
                </div>
                </br></br>
                </br>      
                <div id="div_preaprobados_univ" style="display: none;">  
                    <table border="0"  align="center">
                        <tr>
                            <td>
                                <table id="tabla_preaprobados_univ" ></table>
                                <div id="page_tabla_preaprobados_univ"></div>
                            </td>
                        </tr>
                    </table>
                </div>
                <div id="div_preaprobados" style="display: none;">  
                    <table border="0"  align="center">
                        <tr>
                            <td>
                                <table id="tabla_preaprobados" ></table>
                                <div id="page_tabla_preaprobados"></div>
                            </td>
                        </tr>
                    </table>
                </div>
                <div id="dialogLoading" style="display:none;">
                    <p  style="font-size: 12px;text-align:justify;" id="msj2">Texto </p> <br/>
                    <center>
                        <img src="./images/cargandoCM.gif"/>
                    </center>
                </div>
                <div id="dialogMsj" title="Mensaje" style="display:none;">
                    <p style="font-size: 12.5px;text-align:justify;" id="msj" > Texto </p>
                </div>      
            </center>
        </div>
    </body>
</html>
