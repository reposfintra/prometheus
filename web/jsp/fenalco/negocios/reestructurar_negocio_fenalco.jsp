<%--
Document   : reestructurar_negocio_fenalco
Created on : 12/12/2014, 02:19:58 PM
Author     : egonzalez
--%>

<%@page contentType="text/html" pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
        <title>Reestructurar Negocio fenalco</title>
        <link href="./css/reestructuracion.css" rel="stylesheet" type="text/css" >
        <link type="text/css" rel="stylesheet" href="./css/jquery/jquery-ui/jquery-ui.css" />
        <link href="./css/popup.css" rel="stylesheet" type="text/css">

        <script type="text/javascript" src="./js/jquery/jquery.jqGrid/js/jquery.min.js"></script>
        <script type="text/javascript" src="./js/jquery-ui-1.8.5.custom.min.js"></script>
        <script type="text/javascript" src="./js/jquery/jquery.jqGrid-4.7.0/js/grid.locale-es.js"></script>
        <script type="text/javascript" src="./js/jquery/jquery.jqGrid-4.7.0/js/jquery.jqGrid.min.js"></script>
        <script type="text/javascript" src="./js/jquery/jquery.jqGrid-4.7.0/js/jquery.tabletojson.js"></script>
        <script type="text/javascript" src="./js/reestructurarFenalco.js"></script>
    </head>
<body>
    <div id="capaSuperior"  style="position:absolute; width:2320px; height:100px; z-index:0; left: 0px; top: 0px;">
        <jsp:include page="/WEB-INF/toptsp.jsp?encabezado=REESTRUCTURAR NEGOCIOS"/>
    </div>
    <div id="capaCentral" style="position:absolute; width:100%; height:100%; z-index:0; left: 0px; top: 100px; ">
        <div id="divBuscar">
            <div id="divBuscar1">
                <input id="inptBuscar" type="text" name="inptBuscar" value="" placeholder="nro identificacion" maxlength="11" />
                <!--
                <select id="unidad_negocio" name="unidad_negocio" >                    
                </select>
                -->
                <button id="buscar" class="ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only" >
                    <span class="ui-button-text">Buscar</span>
                </button>
                <button id="limpiar" class="ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only" >
                    <span class="ui-button-text">Borrar</span>
                </button>
                <hr>
            </div>
        </div>
        <div style="position: relative;top:106px">
            <div style="overflow: auto">
                <div id ="btnoculto" class="divOcultar">
                    <a href="#" id="boton"  >Ver / Ocultar</a>
                    <input name="campoVisible" id="campoVisible" type="hidden" value="0" />
                </div>
                <div id="divListaNegocios">
                    <h3 style="width:1156px">LISTA NEGOCIOS ACTIVOS</h3>
                    <div id="listNegocios">
                        <table id="tabla_negocios_clientes"  ></table>
                        <div id="page_negocios_clientes"></div>
                        <button id="calLiquidacion" class="ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only" role="button" aria-disabled="false">
                            <span class="ui-button-text">Calcular Saldos</span>
                        </button>
                    </div>
                    <div id="divResumenSaldos">
                        <h3 style="width:1156px">RESUMEN SALDOS POR NEGOCIOS</h3>
                        <form id="form_resumen_saldos" action="">                        
                        <table id="resumenSaldos"  class="tablaSaldos">
                            <thead>
                                <tr>
                                    <th class="ui-state-default">Negocio</th>
                                    <th class="ui-state-default">Estado</th>
                                    <th class="ui-state-default">Tipo_neg</th>
                                    <th class="ui-state-default">Capital</th>
                                    <th class="ui-state-default">Interes</th>
                                    <th class="ui-state-default">IntxMora</th>
                                    <th class="ui-state-default">GaC</th>
                                    <th class="ui-state-default">Sub_Total</th>
                                    <th class="ui-state-default">Dcto_capital</th>
                                    <th class="ui-state-default">Dcto_interes</th>
                                    <th class="ui-state-default">Dcto_intxMora</th>
                                    <th class="ui-state-default">Dcto_gac</th>
                                    <th class="ui-state-default">Total_Dcto</th>
                                    <th class="ui-state-default">Total_Items</th>
                                </tr>
                            </thead>
                            <tbody id="cuerpoTablaConsolidado">
                            </tbody>
                            <tfoot>
                                <tr class='miTdtotal'>
                                    <td colspan='7' ><b><span>Total<span><b></td>
                                    <td><input class='resultadoSaldo' value="" id="totalSubResumen" name='totalSubResumen' type='text'/></td>
                                    <td class='resultadoSaldo' colspan='5'>Totales con descuentos</td>
                                    <td><input class='resultadoSaldo'  value="" id="totalResumen" name='totalResumen' type='text'/></td>
                                </tr>
                            </tfoot>
                        </table>
                        </form>    
                        <div id="camposOcultos"></div>
                        <br/>
                        <input type="hidden" name="totalItem" id="totalItem" value="0" />
                        <input type="hidden" name="idconvenio" id="idconvenio" value="0" />
                        <input type="hidden" name="afiliado" id="afiliado" value="0" />
             
                        <h3 style="width: 1156px;">RESUMEN PAGO CUOTA INICIAL</h3>
                            <table id="tablaPagoInicial" class="tablaSaldosInicial">
                                <thead>
                                    <tr>
                                        <th  class="ui-state-default">negocio</th>
                                        <th class="ui-state-default">Tipo_neg</th>
                                        <th  class="ui-state-default">capital</th>
                                        <th  class="ui-state-default">interes</th>
                                        <th  class="ui-state-default">intXmora</th>
                                        <th  class="ui-state-default">GAC</th>
                                        <th  class="ui-state-default">Total</th>
                                        <th  class="ui-state-default">pctPagar</th>
                                        <th  class="ui-state-default">ValorPagar</th>
                                        <th  class="ui-state-default">SaldoVencido</th>
                                        <th  class="ui-state-default">SaldoCorriente</th>
                                    </tr>
                                </thead>
                                <tbody id="cuerpoTablaInicial">
                                </tbody>
                                <tfoot>
                                    <tr class='miTdtotal'>
                                    <td colspan="6"><b><span>Totales<span><b></td>
                                    <td><input class='resultadoSaldo' value="" id="totalResumenPagoInicial" name='totalResumenPagoInicial' type='text'/></td>
                                    <td><b><span><span><b></td>
                                    <td><input class='resultadoSaldo' value="" id="totalaPagar" name='totalaPagar' type='text'/></td>
                                    <td><input class='resultadoSaldo' value="" id="totalsaldoVencidoRee" name='totalsaldoVencidoRee' type='text'/></td>
                                    <td><input class='resultadoSaldo' value="" id="totalsaldoCorrienteRee" name='totalsaldoCorrienteRee' type='text'/></td>
                                    </tr>
                                </tfoot>
                            </table>
                        <input name="porcentajeDescuento" id="porcentajeDescuento"  value="" type="hidden" />
                        
                            <div class="divValorTotal" >
                                <fieldset style="border-radius: inherit;height:100px"><legend>Valor Reestructuracion</legend>
                                    <p id="totalReestructuracion" class="simulador">0.00</p>
                                    <button id="simuladoCuotas" class="ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only" role="button" aria-disabled="false">
                                        <span class="ui-button-text">Simulador Cuotas</span>
                                    </button>
                                </fieldset>
                            </div>   
                    </div>
                </div>

                <div id="divSaldos">
                    <div id="divSaldoNegocio">
                        <h3 style="width: 1240px;">DETALLES SALDOS POR NEGOCIOS</h3>
                        <table id="tabla_saldos_negocio_padre"></table><br>
                        <table id="tabla_saldos_negocio_aval"></table> <br>
                        <table id="tabla_saldos_negocio_seguro"></table><br>
                        <table id="tabla_saldos_negocio_gps"></table><br>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div id="simulador_credito" class="ventana" title="Simulador Credito">
        <div id="desplegable">
            <center>
                <table id="tabla">
                    <tr>
                        <td class="td">
                            <label for="cuota">Cuotas :</label>
                            <input name="cuota" type="number" id="cuota"  min="1" max="48" required="required"/>
                        </td>
                        <td class="td">
                            <label for="valor_negocio">Valor a financiar :</label>
                            <input type="text" required="required" readonly="true" name="valor_negocio" id="valor_negocio" value="" maxlength="20" />
                        </td>
                        <td class="td">
                            <label for="primeracuota">Primera Cuota :</label>
                            <form id="formulario" name="formulario">
                                <select id="primeracuota" name="primeracuota">
                                </select>
                            </form>
                        </td>
                        <td class="td">
                            <label for="">Titulo valor :</label>
                            <select id="titulo_valor">
                                <option value="03_f">Pagare</option>
                            </select>
                        </td >
                    </tr>
                </table>
            </center>
        </div>
        <hr class="ui-widget-content">
        <div id="grid_liquidacion">
            <table id="tabla_simulador_Credito"></table>
            <div id="page_simulador_Credito"></div>
        </div>
    </div>

    <div id="dialogo" class="ventana" title="Mensaje">
        <p  id="msj">texto </p>
    </div>

    <div id="dialogo2" class="ventana">
        <p id="msj2">texto </p> <br/>
        <center>
            <img src="./images/cargandoCM.gif"/>
        </center>
    </div>
</body>
</html>
