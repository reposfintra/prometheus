<%-- 
    Document   : generarPreaprobados
    Created on : 26/04/2016, 10:22:46 AM
    Author     : user
--%>

<%@page contentType="text/html" pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
        <link href="./css/popup.css" rel="stylesheet" type="text/css">
        <link type="text/css" rel="stylesheet" href="./css/jquery/jquery-ui/jquery-ui.css" />       
        <link type="text/css" rel="stylesheet" href="./css/TransportadorasApi.css" />
        <script type="text/javascript" src="./js/jquery/jquery.jqGrid/js/jquery.min.js"></script>
        <script type="text/javascript" src="./js/jquery-ui-1.8.5.custom.min.js"></script>   
        <script type="text/javascript" src="./js/jquery/jquery.jqGrid-4.7.0/js/grid.locale-es.js"></script>
        <script type="text/javascript" src="./js/jquery/jquery.jqGrid-4.7.0/js/jquery.jqGrid.min.js"></script>
        <script type="text/javascript" src="./js/jquery/jquery.jqGrid-4.7.0/js/jquery.tabletojson.js"></script>    
        <script type="text/javascript" src="./js/generarPreAprobados.js"></script> 

        <title>GENERAR PREAPROBADOS FINTRA CREDIT</title>     
    </head>
    <body>
        <div id="capaSuperior" style="position:absolute; width:100%; height:100px; z-index:0; left: 0px; top: 0px;">
            <jsp:include page="/WEB-INF/toptsp.jsp?encabezado=GENERAR PREAPROBADOS FINTRA CREDIT"/>
        </div>
        <div id="capaCentral" style="position:absolute; width:100%; height:80%; z-index:0; left: 0px; top: 100px; ">
            <center>

                <div style="background-color: #FFF;
                     width: 650px;
                     height: 95px;
                     border-radius: 4px;
                     padding: 1px;
                     border: 1px solid #2A88C8;
                     margin: 15px auto 0px; " >

                    <div id="encabezadotablita" style="width: 645px">
                        <label class="titulotablita"><b>GENERAR PREAPROBADOS </b></label>
                    </div> 
                    <table border="0" style="width:650px">
                        <tr>
                            <td >
                                <label for="unidad_negocio">Und. de Negocio :</label>                               
                                <select id="unidad_negocio" name="unidad_negocio">
                                </select>                             
                            </td>        
                            <td>
                                <label for="periodo">Periodo:</label>
                                <input id="periodo_preaprob" name="periodo" type="text" maxlength="6" class="solo-numero"/>                                   
                            </td> 
                            <td style="padding-top:  12px">
                                <div id ='botones'>
                                    <button id="consultar_preaprobados" class="ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only" 
                                            role="button" aria-disabled="false" >
                                        <span class="ui-button-text">Consultar</span>
                                    </button> 
                                    <button id="generar" class="ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only" 
                                            role="button" aria-disabled="false" >
                                        <span class="ui-button-text">Generar</span>
                                    </button> 
                                </div>
                            </td>
                        </tr>
                    </table>
                </div>
                </br></br>
                </br>  
                <div id="div_preaprobados" style="display: none;">  
                    <table border="0"  align="center">
                        <tr>
                            <td>
                                <table id="tabla_preaprobados" ></table>
                                <div id="page_tabla_preaprobados"></div>
                            </td>
                        </tr>
                    </table>
                </div>
                <div id="dialogLoading" style="display:none;">
                    <p  style="font-size: 12px;text-align:justify;" id="msj2">Texto </p> <br/>
                    <center>
                        <img src="./images/cargandoCM.gif"/>
                    </center>
                </div>
                <div id="divSalidaEx" title="Exportacion" style=" display: block" >
                    <p  id="msjEx" style=" display:  none"> Espere un momento por favor...</p>
                    <center>
                        <img id="imgloadEx" style="position: relative;  top: 7px; display: none " src="./images/cargandoCM.gif"/>
                    </center>
                    <div id="respEx" style=" display: none"></div>
                </div> 
                <div id="dialogMsj" title="Mensaje" style="display:none;">
                    <p style="font-size: 12.5px;text-align:justify;" id="msj" > Texto </p>
                </div>      
            </center>
        </div>
    </body>
</html>
