<%--
    Document   : reestructurarNegociosMicro
    Created on : 11/08/2014, 09:49:36 AM
    Author     : EGONZALEZ
--%>

<%@page contentType="text/html" pageEncoding="ISO-8859-1"%>
<%@ include file="/WEB-INF/InitModel.jsp"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">

        <link href="<%=BASEURL%>/css/estilostsp.css" rel="stylesheet" type="text/css">
        <link href="<%=BASEURL%>/css/reestructuracion.css" type="text/css" rel="stylesheet" />
        <link href="<%=BASEURL%>/css/popup.css" rel="stylesheet" type="text/css">
        <link type="text/css" rel="stylesheet" href="<%=BASEURL%>/css/jquery/jquery-ui/jquery-ui.css"/>

        <script type="text/javascript" src="<%=BASEURL%>/js/jquery/jquery.jqGrid/js/jquery.min.js"></script>
        <script type="text/javascript" src="<%=BASEURL%>/js/jquery-ui-1.8.5.custom.min.js"></script>
        <script type="text/javascript" src="<%=BASEURL%>/js/jquery/jquery.jqGrid/js/grid.locale-es.js"></script>
        <script type="text/javascript" src="<%=BASEURL%>/js/jquery/jquery.jqGrid/js/jquery.jqGrid.min.js"></script>
        <script type="text/javascript" src="<%=BASEURL%>/js/reestructurarNegocio.js"></script>

        <title>Reestructurar Negocios</title>
    </head>
    <body>
        <div id="capaSuperior" style="position:absolute; width:100%; height:100px; z-index:0; left: 0px; top: 0px;">
            <jsp:include page="/WEB-INF/toptsp.jsp?encabezado=REESTRUCTURAR NEGOCIOS"/>
        </div>


        <div id="capaCentral" style="position:absolute; width:100%; height:85%; z-index:0; left: 0px; top: 100px; overflow:auto">
            <center>
                <table>
                    <tr>
                        <td>
                            <div class="k-block">
                                <div class="k-header"><span class="titulo">BUSCAR NEGOCIOS - CLIENTES</span> </div>
                                <div id="contenido">
                                    <table border="0" class="table"  >
                                        <tr>

                                            <td class="td"><label for="cedula"> Cedula </label></td>
                                            <td class="td"><input type="text" required="true" name="cedula" id="cedula" value="" placeholder="Nro cedula" maxlength="10" /></td>
                                            <td class="td"><label for="codneg"> Codigo Negocio </label></td>
                                            <td class="td"><input type="text"  required="true" name="codneg" id="codneg" value="" placeholder="Codigo del negocio" size="15" /></td>
                                            <td class="td">
                                                <button id="buscar" class="ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only" role="button" aria-disabled="false">
                                                    <span class="ui-button-text">Buscar</span>
                                                </button>
<!--                                                <span class="form-submit-button-simple_green_apple" id="buscar"> Buscar </span>-->
                                            </td>
                                        </tr>
                                    </table>

                                </div>
                            </div>
                        </td>
                    </tr>
                </table>
            </center>
            <div class="grid" id="grid_1">
                <center>

                    <table id="tabla_negocios_clientes"  ></table>
                    <div id="page_negocios_clientes"></div>
                    <br/>
                    <br/>
                    <table id="tabla_facturas_negocio"></table>
                    <div id="page_facturas_negocio"></div>
                    <br/><br/>
                    <div id="divSimulador" class="simuldor">

                        <button id="simular"  class="ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only" role="button" aria-disabled="false">
                            <span class="ui-button-text">Simular Credito</span>
                        </button>
<!--                        <span class="form-submit-button-simple_green_apple" id="simular" > Simular </span>-->
                    </div>

                </center>

            </div>
            <div id="dialogo" class="ventana" title="Mensaje">
                <p  id="msj">texto </p>

            </div>

            <div id="dialogo2" class="ventana">
                <p  id="msj2">texto </p> <br/>
                <center>
                    <img src="<%=BASEURL%>/images/cargandoCM.gif"/>
                </center>
            </div>


        </div>

        <div id="simulador_credito" class="ventana" title="Simulador Credito">
            <div id="desplegable">
                <center>
                    <table id="tabla">
                        <tr>
                            <td class="td">
                                <label for="tipo_cuota">Tipo cuota : </label><br/>
                                <select id="tipo_cuota">
                                    <option value="">...</option>
                                    <option value="CPFCTV">Capital Fijo Cuota Variable</option>
                                    <option value="CTFCPV">Cuota Fija Capital Variable</option>
                                </select>
                            </td>
                            <td class="td">
                                <label for="cuota">Cuotas :</label><br/>
                                <input name="cuota" type="number" id="cuota"  min="1" max="48" required="required"/>

                            </td>
                            <td class="td">
                                <label for="valor_negocio">Valor a financiar :</label>
                                <input type="text" required="required" readonly="true" name="valor_negocio" id="valor_negocio" value="" maxlength="20" />
                                <input type="hidden" name="cod_negocio" id="cod_negocio" value="MC" />
                                <input type="hidden" name="saldo_capital" id="saldo_capital" value="0" />
                                <input type="hidden" name="saldo_interes" id="saldo_interes" value="0" />
                                <input type="hidden" name="saldo_cat" id="saldo_cat" value="0" />
                                <input type="hidden" name="saldo_seguro" id="saldo_seguro" value="0" />
                                <input type="hidden" name="intxmora" id="intxmora" value="0" />
                                <input type="hidden" name="gac" id="gac" value="0" />
                            </td>
                            <td class="td">
                                <label for="primeracuota">Primera Cuota :</label>
                                <form id="formulario" name="formulario">
                                    <select id="primeracuota" name="primeracuota">
                                    </select>
                                </form>
                            </td>
                            <td class="td">
                                <label for="">Titulo valor :</label>
                                <select id="titulo_valor">
                                    <option selected="" value="">...</option>
                                    <option value="03_f">Pagare</option>
                                    <option value="02_f">Letra</option>
                                </select>
                            </td >

                        </tr>
                    </table>
                </center>

            </div>
            <div style="text-align: right">
                <a href="#" id="boton"  >Ver / Ocultar</a>
            </div>
            <hr class="ui-widget-content">

            <div id="grid_liquidacion">
                <table id="tabla_simulador_Credito"></table>
                <div id="page_simulador_Credito"></div>
            </div>

        </div>

        <div id="editar_form" class="ventana" title="Actualizar Inforamcion">

            <form id="formulario_cliente" action=""> 
                <div class="form">            
                    <div class="header"><span class="titulo">Informacion Basica</span> </div>
                    <br>
                    <div id="contenido2">
                        <center>
                            <table border="0"  >

                                <tr>
                                    <td>
                                        <table class="tabla">
                                            <tr>
                                                <td class="td" >
                                                    <label for="identificacion">Cedula</label><br>
                                                    <input type="text" class="input-form" id="identificacion" name="identificacion" value="" size="12" pattern="^[0-9]*" required autofocus readonly/>
                                                    <input type="hidden" name="numero_solicitud" id="numero_solicitud" value="" />

                                                </td>
                                                <td class="td">
                                                    <label for="pr_apellido">Primer Apellido</label><br>
                                                    <input type="text" class="input-form" name="pr_apellido" id="pr_apellido" value="" size="17" pattern="^[A-Z\s]*$" required autofocus title="Este campo es obligatorio"/>

                                                </td>
                                                <td class="td">
                                                    <label for="sg_apellido">Segundo Apellido</label><br>
                                                    <input type="text" class="input-form"  id="sg_apellido" name="sg_apellido" value="" size="17" pattern="^[A-Z\s]*$"  required autofocus title="Este campo es obligatorio"/>

                                                </td>
                                                <td class="td">
                                                    <label for="pr_nombre">Primer Nombre</label><br>
                                                    <input type="text" class="input-form"  id="pr_nombre" name="pr_nombre" value="" size="17" pattern="^[A-Z\s]*$"  required autofocus title="Este campo es obligatorio"/>

                                                </td>
                                                <td class="td">
                                                    <label for="sg_nombre">Segundo Nombre</label><br>
                                                    <input type="text" class="input-form"  id="sg_nombre" name="sg_nombre" value="" size="17" pattern="^[A-Z\s]*$"  required autofocus title="Este campo es obligatorio"/>

                                                </td>
                                                <td class="td">
                                                    <label for="telefono">Telefono</label><br>
                                                    <input type="tel" class="input-form"  id="telefono" name="telefono" value="" size="15" pattern="^[0-9]*"  maxlength="7" />

                                                </td>
                                                <td class="td">
                                                    <label for="celular">Celular</label><br>
                                                    <input type="tel" class="input-form"  id="celular" name="celular" value="" size="15" pattern="^[0-9]*" maxlength="10"/>

                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <table>
                                            <tr>
                                                <td class="td">
                                                    <label for="direccion">Direccion</label><br>
                                                    <input type="text" class="input-form"  id="direccion" name="direccion" value="" size="33"  required autofocus title="Este campo es obligatorio" />
                                                </td>
                                                <td class="td">
                                                    <label for="departamento">Departamento</label><br>
                                                    <select name="departamento" id="departamento">
                                                    </select>
                                                </td>
                                                <td class="td">
                                                    <label for="ciudad">Ciudad</label><br>
                                                    <select name="ciudad" id="ciudad">
                                                    </select>
                                                </td>
                                                <td class="td">
                                                    <label for="barrio">Barrio</label><br>
                                                    <input type="text" class="input-form"  id="barrio" name="barrio" value="" size="13" pattern="^[A-Z\s]*$"  required autofocus title="Este campo es obligatorio"/>
                                                </td>
                                                <td class="td">
                                                    <label for="email">Email</label><br>
                                                    <input type="email" class="input-form"  id="email" name="email" value="" size="34"/>
                                                </td>

                                            </tr>
                                        </table>
                                    </td>

                                </tr>

                            </table>
                        </center>
                    </div>
                </div>

                <div class="form_cy">
                    <div class="header_cy"><span class="titulo">Informacion Conyugue</span> </div>
                    <br>
                    <div id="contenido3">

                        <center>
                            <table border="0" >

                                <tr>
                                    <td>
                                        <table>
                                            <tr>
                                                <td class="td" >
                                                    <label for="identificacion_cy" >Cedula</label><br>
                                                    <input type="text" class="input-form" id="identificacion_cy" name="identificacion_cy" value="" size="15" pattern="^[0-9]*" />

                                                </td>
                                                <td class="td">
                                                    <label for="pr_apellido_cy">Primer Apellido</label><br>
                                                    <input type="text" class="input-form"  id="pr_apellido_cy"  name="pr_apellido_cy" value=""  pattern="^[A-Z\s]*$"  size="17" />


                                                </td>
                                                <td class="td">
                                                    <label for="sg_apellido_cy">Segundo Apellido</label><br>
                                                    <input type="text" class="input-form"  id="sg_apellido_cy" name="sg_apellido_cy" value="" pattern="^[A-Z\s]*$"  size="17" />

                                                </td>
                                                <td class="td">
                                                    <label for="pr_nombre_cy">Primer Nombre</label><br>
                                                    <input type="text" class="input-form"  id="pr_nombre_cy" name="pr_nombre_cy" value="" pattern="^[A-Z\s]*$"  size="17" r/>

                                                </td>
                                                <td class="td">
                                                    <label for="sg_nombre_cy">Segundo Nombre</label><br>
                                                    <input type="text" class="input-form"  id="sg_nombre_cy" name="sg_nombre_cy" value="" pattern="^[A-Z\s]*$"  size="17" />

                                                </td>
                                                <td class="td">
                                                    <label for="telefono_cy">Telefono</label><br>
                                                    <input type="tel" class="input-form"  id="telefono_cy" name="telefono_cy" value="" size="15" pattern="^[0-9]*"  maxlength="7" />

                                                </td>

                                            </tr>
                                            <tr>
                                                <td class="td">
                                                    <label for="celular_cy">Celular</label><br>
                                                    <input type="tel" class="input-form"  id="celular_cy" name="celular_cy" value="" size="15" maxlength="10" pattern="^[0-9]*" />

                                                </td>

                                                <td class="td" colspan="2">
                                                    <label for="direcion_cy">Direccion</label><br>
                                                    <input type="text" class="input-form"  id="direcion_cy" name="direcion_cy" value="" size="42" pattern="^[0-9]*" />
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>

                            </table>
                        </center>
                    </div>
                </div>

                <div class="form_ng">
                    <div class="header_ng"><span class="titulo">Informacion Negocio</span> </div>
                    <br>
                    <div id="contenido4">
                        <center>
                            <table border="0" >

                                <tr>
                                    <td>
                                        <table>
                                            <tr>
                                                <td class="td" >
                                                    <label for="nombre_negocio">Nombre</label><br>
                                                    <input type="text" class="input-form" id="nombre_negocio" name="nombre_negocio" value="" size="25" required autofocus title="Este campo es obligatorio"/>

                                                </td>
                                                <td class="td">
                                                    <label for="direccion_ng">Direccion</label><br>
                                                    <input type="text" class="input-form"  id="direccion_ng" name="direccion_ng" value="" size="25" required autofocus title="Este campo es obligatorio" />
                                                </td>
                                                <td class="td">
                                                    <label for="departamento_ng">Departamento</label><br>
                                                    <select name="departamento_ng" id="departamento_ng">
                                                    </select>
                                                </td>
                                                <td class="td">
                                                    <label for="ciudad_ng">Ciudad</label><br>
                                                    <select name="ciudad_ng" id="ciudad_ng">
                                                    </select>
                                                </td>
                                                <td class="td">
                                                    <label for="barrio_ng">Barrio</label><br>
                                                    <input type="text" class="input-form"  id="barrio_ng" name="barrio_ng" value="" pattern="^[A-Z\s]*$" size="12"/>
                                                </td>

                                                <td class="td">
                                                    <label for="telefono_ng">Telefono</label><br>
                                                    <input type="tel" class="input-form"  id="telefono_ng" name="telefono_ng" value="" size="13" pattern="^[0-9]*"  maxlength="10" />

                                                </td>

                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                            </table>
                        </center>
                    </div>
                </div>

                <div class="form_ob">
                    <div class="header_ob"><span class="titulo">Observacion</span> </div>
                    <br>
                    <div id="contenido4">
                        <center>
                            <textarea class="textarea" id="observacion" name="observacion" rows="4" cols="100" required autofocus title="Este campo es obligatorio">                                
                            </textarea> 
                        </center>
                    </div>
                </div>
            </form>
        </div>
    </body>
</html>
