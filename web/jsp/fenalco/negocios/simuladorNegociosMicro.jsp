<%-- 
    Document   : simuladorNegociosMicro
    Created on : 4/01/2016, 03:29:59 PM
    Author     : mcastillo
--%>

<%@page contentType="text/html" pageEncoding="ISO-8859-1"%>
<%@ include file="/WEB-INF/InitModel.jsp"%>
<!DOCTYPE html>
<html>
    <head>
          <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">

        <link href="<%=BASEURL%>/css/estilostsp.css" rel="stylesheet" type="text/css">
        <link href="<%=BASEURL%>/css/reestructuracion.css" type="text/css" rel="stylesheet" />
        <link href="<%=BASEURL%>/css/popup.css" rel="stylesheet" type="text/css">
        <link type="text/css" rel="stylesheet" href="<%=BASEURL%>/css/jquery/jquery-ui/jquery-ui.css"/>

        <script type="text/javascript" src="<%=BASEURL%>/js/jquery/jquery.jqGrid/js/jquery.min.js"></script>
        <script type="text/javascript" src="<%=BASEURL%>/js/jquery-ui-1.8.5.custom.min.js"></script>
        <script type="text/javascript" src="<%=BASEURL%>/js/jquery/jquery.jqGrid/js/grid.locale-es.js"></script>
        <script type="text/javascript" src="<%=BASEURL%>/js/jquery/jquery.jqGrid/js/jquery.jqGrid.min.js"></script>
        <script type="text/javascript" src="<%=BASEURL%>/js/simuladorCredito.js"></script>

        <title>Simulador Microcrédito</title>
    </head>
    <body>
          <div id="capaSuperior" style="position:absolute; width:100%; height:100px; z-index:0; left: 0px; top: 0px;">
            <jsp:include page="/WEB-INF/toptsp.jsp?encabezado=SIMULADOR MICROCREDITOS"/>
        </div>
        
        <div id="capaCentral" style="position:absolute; width:100%; height:85%; z-index:0; left: 0px; top: 110px; overflow:auto">
                   
                <center>       
                    <div id="div_simulador_microcredito">  
<!--                    <label for="cod_negocio">Negocio :</label>
                    <input type="text" name="cod_negocio" id="cod_negocio" value="" maxlength="20" />    
                    <hr>-->
                    <table id="tabla">     
                        <tr>
                            <td class="td" colspan="7">
                                <label for="cod_negocio">Negocio :</label>
                                <input type="text" name="cod_negocio" id="cod_negocio" value="" maxlength="20" style="text-transform: uppercase;"/> 
                                <input type="hidden" name="aut_reliquidar_MC" id="aut_reliquidar" value="N" />
                                <img src='<%=BASEURL%>/images/botones/iconos/lupa.gif' name='btn_search' id='btn_search' width='20' height='20' title ='Buscar' style="cursor:hand;">
                                <img src='<%=BASEURL%>/images/botones/iconos/clear2.png' name='btn_clear' id='btn_clear' width='20' height='20' title ='Limpiar' style="cursor:hand;">
                                <br><hr>
                            </td>                            
                        </tr>
                        <tr>                           
                            <td class="td">
                                <label for="tipo_cuota">Tipo cuota : </label><br/>
                                <select id="tipo_cuota">
                                    <option value="">...</option>
                                    <option value="CPFCTV">Capital Fijo Cuota Variable</option>
                                    <option value="CTFCPV">Cuota Fija Capital Variable</option>
                                </select>
                            </td>
                            <td class="td">
                                <label for="cuota">Cuotas :</label><br/>
                                <input name="cuota" type="number" id="cuota" class="solo-numero"  min="1" max="48"/>

                            </td>
                            <td class="td">
                                <label for="valor_negocio">Valor a financiar :</label>
                                <input type="text" required="required" name="valor_negocio" id="valor_negocio" class="solo-numero" value="" maxlength="20" />                               
                            </td>
                            <td class="td">
                                <label for="fecha_liquidacion">Fecha Liquidación :</label>
                                <input type="text" id="fecha_liquidacion" name="fecha_liquidacion" required="required" style="font-weight: bold;height:24px;" />                                
                            </td>
                            <td class="td">
                                <label for="primeracuota">Primera Cuota :</label>
                                <form id="formulario" name="formulario">
                                    <select id="primeracuota" name="primeracuota">
                                    </select>
                                </form>
                            </td>
<!--                        <td class="td">
                                <label for="">Titulo valor :</label>
                                <select id="titulo_valor">
                                    <option selected="" value="">...</option>
                                    <option value="03_f">Pagare</option>
                                    <option value="02_f">Letra</option>
                                </select>
                            </td >-->
                            <td class="td">
                                <label for="convenio">Convenio :</label>                               
                                    <select id="convenio" name="convenio">
                                    </select>                             
                            </td>                          
                            <td class="td">
                                <button id="simular" class="ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only" role="button" aria-disabled="false">
                                    <span class="ui-button-text">Simular</span>
                                </button>                               
                            </td>
                        </tr>
                    </table>                                    
                   </div>
                   <br><br>  
                   <div id="grid_liquidacion">
                       <table id="tabla_simulador_Credito"></table>
                       <div id="page_simulador_Credito"></div>
                   </div>
                </center>

          
<!--            <div style="text-align: right">
                <a href="#" id="boton"  >Ver / Ocultar</a>
            </div>
            <hr class="ui-widget-content">-->

           
       
        <div id="dialogMsg" class="ventana" title="Mensaje">
            <p  id="msj">texto </p>

        </div>
        <div id="dialogLoading"  class="ventana" >
            <p  id="msj2">texto </p> <br/>
            <center>
                <img src="<%=BASEURL%>/images/cargandoCM.gif"/>
            </center>
        </div>

        <div id="divSalidaEx" title="Exportacion" style=" display: block" >
            <p  id="msjEx" style=" display:  none"> Espere un momento por favor...</p>
            <center>
                <img id="imgloadEx" style="position: relative;  top: 7px; display: none " src="./images/cargandoCM.gif"/>
            </center>
            <div id="respEx" style=" display: none"></div>
        </div> 
            
     </div>
    </body>
</html>
