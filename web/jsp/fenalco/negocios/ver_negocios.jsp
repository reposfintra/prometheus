<!--
- Autor : Alejandro Payares
- Date  : Diciembre 24 del 2005
- Copyrigth Notice : Fintravalores S.A. S.A
- Descripcion : Pagina JSP que permite editar la informaci�n de un usuario.
- Ultima modificaci�n: Julio 27 de 2007
-->
<%@ page session="true" %>
<%@ page import="java.util.*" %>
<%@ page import="com.tsp.operation.model.beans.*" %>
<%@ include file="/WEB-INF/InitModel.jsp" %>
<%@ taglib uri="http://jakarta.apache.org/taglibs/input-1.0" prefix="input" %>
<%@ taglib uri="http://www.sanchezpolo.com/sot" prefix="tsp"%>
<%@page import="com.tsp.util.*"%>
<%@page import="com.tsp.util.Util.*"%>

<%@page import="com.tsp.operation.model.services.ClientesVerService"%><!--20100621-->
<%@page import="com.tsp.operation.model.services.NegocioTrazabilidadService"%>

<head>
    <title>.: Datos del Negocio :.</title>
    <link href='<%=BASEURL%>/css/estilostsp.css' rel='stylesheet' type='text/css'>
    <link href='../css/estilostsp.css' rel='stylesheet' type='text/css'>
    <link href="<%=BASEURL%>/css/mac_os_x.css" rel="stylesheet" type="text/css">
    <link href="<%=BASEURL%>/css/default.css" rel="stylesheet" type="text/css">
    <script src="<%=BASEURL%>/js/negocioTrazabilidad.js" type="text/javascript"></script>
    
    <script type='text/javascript' src="<%= BASEURL%>/js/prototype.js"></script>
    <script src="<%=BASEURL%>/js/effects.js" type="text/javascript"></script>
    <script src="<%=BASEURL%>/js/window.js" type="text/javascript"></script>
    <script src='<%=BASEURL%>/js/boton.js' language='javascript'></script>
    
    <link href="<%= BASEURL%>/css/jquery/jquery-ui/jquery-ui.css" rel="stylesheet" type="text/css"/>
    <script type='text/javascript' src="<%= BASEURL%>/js/jquery/jquery-ui/jquery.min.js"></script>
    <script type='text/javascript' src="<%= BASEURL%>/js/jquery/jquery-ui/jquery.ui.min.js"></script>

    <link type="text/css" rel="stylesheet" href="./css/jquery/jquery-ui/jquery-ui.css" /> 
    <link href="<%=BASEURL%>/css/popup.css" rel="stylesheet" type="text/css">
    <script type="text/javascript" src="./js/jquery/jquery.jqGrid-4.7.0/js/grid.locale-es.js"></script>
    <script type="text/javascript" src="./js/jquery/jquery.jqGrid-4.7.0/js/jquery.jqGrid.min.js"></script>
    <script type="text/javascript" src="./js/jquery/jquery.jqGrid-4.7.0/js/jquery.tabletojson.js"></script>
      
    <script language='javascript'>
        function exporta()
        {
            window.open('<%= CONTROLLER%>?estado=Negocios&accion=Ver&op=3','EXP','status=yes,scrollbars=yes,width=400,height=150,resizable=no');
                    }
        
        //jpinedo
	function cargarReporte(nit,fecha_desm){
                var win = new Window({

                            id: "soporte_pagos",
                            title: ".: Reporte De Pagos :.",
                            width:1000,
                            height:550,
                            destroyOnClose: true,
                           url:'<%=BASEURL%>/jsp/fenalco/negocios/reporte_pago.jsp?documento='+nit+'&fecha='+fecha_desm,

                            recenterAuto: false
                        });
                win.showCenter(true);
                //win.maximize();
            }
            
            
            
            
            			
var j = jQuery.noConflict();		
			
function cerrar()
{
    j("#divSalida").dialog('close')
}

 j(document).ready(function(){
                j("#divSalida").dialog({
                    autoOpen: false,
                    height: "auto",
                    width: "35%",
                    modal: true,
                    autoResize: true,
                    resizable: false,
                    position: "center"               

                });
                
                j("#fe_expedicion").datepicker({
                    dateFormat: 'yy-mm-dd',
                    changeMonth: true,
                    minDate: new Date((new Date().getFullYear() - 90), new Date().getMonth(), new Date().getDate()),
                    maxDate: new Date(new Date().getFullYear(), new Date().getMonth(), new Date().getDate()),
                    defaultDate: new Date(new Date().getFullYear(), new Date().getMonth(), new Date().getDate())
                });
                
                 j("#fe_vencimiento").datepicker({
                    dateFormat: 'yy-mm-dd',
                    changeMonth: true,
                    minDate: new Date((new Date().getFullYear() - 90), new Date().getMonth(), new Date().getDate()),
                    //maxDate: new Date(new Date().getFullYear(), new Date().getMonth(), new Date().getDate()),
                    defaultDate: new Date(new Date().getFullYear(), new Date().getMonth(), new Date().getDate())
                });
              
       });
  
 function Exportar(negocio,pagare)
{
    
   
    j("#divSalida").dialog("open");
    j("#divSalida").css('height', 'auto');
   j("#divSalida").dialog("option", "position", "center");
   j("#divSalida").html("");
   var url ="<%=CONTROLLER%>?estado=Pdf&accion=Imprimir";
   var base ="<%=BASEURL%>"
    j.ajax({
        dataType: 'html',
        url:url,
        data: {
            opcion : 'imprimirpdfpagare',
	    codigo_negocio:negocio,
            pagare:pagare
        },
        method: 'post',
        success:  function (resp){
            var boton = "<div style='text-align:center'><img src='"+base+"/images/botones/salir.gif'  onMouseOver='botonOver(this);' onMouseOut='botonOut(this);'  onclick='cerrar()' /></div>"
            document.getElementById('divSalida').innerHTML = resp+ "<br/><br/><br/><br/>"+boton
        },
        error: function(){
            alert("Ocurrio un error enviando los datos al servidor");
        }

    });

}	

  j(function() {
      var mi;
    j( "#marca" ).autocomplete({
      source: function( request, response ) {
        j.ajax({
          url: "./controller?estado=Garantias&accion=Creditos",
          dataType: "json",
          data: {
            q: request.term,
            opcion:2
          },
          success: function( data ) {
           // response( data );
         response( j.map( data, function( item ) {
                return {
                    label: item.label,
                    value: item.label,
                    mivar:item.value
                }
            }));
          }
        });
      },
      minLength: 1,
      select: function( event, ui ) {
        cargarComboxOption(3,ui.item.mivar);
        console.log( ui.item ?
          "Selected: " + ui.item.label :
          "Nothing selected, input was " + this.value);
      },
      open: function() {
        //j( this ).removeClass( "ui-corner-all" ).addClass( "ui-corner-top" );
      },
      close: function() {
          
      // j( this ).removeClass( "ui-corner-top" ).addClass( "ui-corner-all" );
      }
    });
 
 });
            
    </script>


<style type="text/css">
    div.ui-dialog{
        font-size:11px;

     }

     select{
         /*background:#7c7 ;*/
         background-position: 280px center;
         background-repeat: no-repeat;
         padding:3px;
         font-size: 11px !important;
         color:rgba(10, 50, 10, 1) !important;
         font-weight: normal;
         font-family:Arial, Helvetica, sans-serif;
         border-radius: 3px;
         -webkit-transition: all 0.4s;
         -moz-transition: all 0.4s;
         transition: all 0.4s;
         -webkit-box-shadow: 0 1px 2px rgba(0,0,0,0.3);
         width: 140px;
     }
     select:hover{
         background-color: #bfcaba;
     }
     label{
         
         width:120px 
     }
  
     .td { 
         padding: 3px;
         /*width: 150px;*/
         font-family:Arial, Helvetica, sans-serif;
         font-weight: bold;
         font-size: 12px;
         color:   rgba(10, 50, 10, 1);
         line-height: 1.3 ;
         white-space: nowrap;
     }
     
     input{
          color: rgba(10, 50, 10, 1) !important;
          font-size: 12px !important;
     }
     
     .th{
         background-color: #5E7B8F;
         color: floralwhite;
         padding:5px 5px 5px 5px; 
     }
     


</style>
</head>
<body onResize="redimensionar()" onload='redimensionar()'>
    
        <%Usuario loggedUser = (Usuario) session.getAttribute("Usuario");
        NegocioTrazabilidadService trazaserv = new NegocioTrazabilidadService(loggedUser.getBd());

                    ClientesVerService clvsrv = new ClientesVerService(loggedUser.getBd());//20100621
                    String perfil = clvsrv.getPerfil(loggedUser.getLogin());//20100621
                    String vista = request.getParameter("vista") != null ? request.getParameter("vista") : "";
                    Vector af = model.Negociossvc.getListNeg();
                    session.setAttribute("vneg", af);
                    double sum1 = 0, sum2 = 0;
                    double sum_tot_pagado = 0;
                    int dia = 0;
                    int mes = 0;
                    int ano = 0;
                    String fechai = null;
                    String actividad=vista.equals("6")?"RADICACION":(vista.equals("7")?"ANALISIS":(vista.equals("8")?"DECISI&Oacute;N":(vista.equals("9")?"FORMALIZACI&Oacute;N":(vista.equals("10")?"REFERENCIACI&Oacute;N":(vista.equals("14")?"INDEMNIZACI&Oacute;N":"")))));
                    if (af.size() > 0) {%>
    <div id="capaSuperior" style="position:absolute; width:100%; z-index:0; left: 0px; top: 0px;">
         <jsp:include page="/WEB-INF/toptsp.jsp?encabezado=VER DATOS DEL NEGOCIO"/>
    </div>
    <div id="capaCentral" style="position:absolute; width:100%; height:87%; z-index:0; left: 0px; top: 100px; overflow: scroll;">        
        <p>&nbsp;<%=!actividad.equals("")?actividad:""%></p>
        <table  border = '2' align = 'center'  width="100%">
            <tr>
                <%					 String comefrom = (String) request.getParameter("cld");%>
                
                <%if (vista.equals("6") || vista.equals("7") || vista.equals("8") || vista.equals("9")|| vista.equals("10")){%>
                <td align="center" class="subtitulo1"></td>
                <%}%>
                
                
                <td align="center" class="subtitulo1">Item</td>
                <td align="center" class="subtitulo1">Cod Negocio</td>
                <td align="center" class="subtitulo1">Reporte Pago</td>


                <% if (vista.equals("7")){%>  <td align="center" class="subtitulo1">Analista</td> <%}%>               
                <% 
                //colocar el id usuario + una accion y en tabla gen modificar secure_eca
                String accion=loggedUser.getCedula();
                //System.out.println("accion:"+accion) ;
               // System.out.println( clvsrv.ispermitted(loggedUser.getTipo(),accion));                                
    if (comefrom == null && (vista.equals("6")||loggedUser.getTipo().equals("CLIENTETSP")||vista.equals("13")||clvsrv.ispermitted(loggedUser.getTipo(),accion))) {%><td align="center" class="subtitulo1">Generar Letras </td>



                <td align="center" class="subtitulo1">Generar Pagar� </td>
                <%					 }%>                  
                <td align="center" class="subtitulo1">Afiliado</td>
                <td align="center" class="subtitulo1">Tipo Cliente</td>
                <td align="center" class="subtitulo1">Cliente</td>
                <td align="center" class="subtitulo1">Id_Cliente</td>
                <td align="center" class="subtitulo1">Codeudor</td>
                <td align="center" class="subtitulo1">Id Codeudor</td>
                <td align="center" class="subtitulo1">Codigo Estudiante</td>
                <td align="center" class="subtitulo1">Programa</td>
                <%                                       if (comefrom == null) {%> <td align="center" class="subtitulo1">Vr_Negocio solicitado</td><td align="center" class="subtitulo1">Vr_Negocio</td>
                <td align="center" class="subtitulo1">Vr_Desembolso</td>
                <td align="center" class="subtitulo1">Vr_Documentos</td>
                <td align="center" class="subtitulo1">Vr_Renovacion</td>
                <td align="center" class="subtitulo1">Pre_aprobado Ex_cliente</td>
                <td align="center" class="subtitulo1">Tipo cr�dito</td>
                <td align="center" class="subtitulo1">Agencia</td>
                <td align="center" class="subtitulo1">Estado</td>
                <%                                          if (!loggedUser.getTipo().equals("CLIENTETSP")) {%><td align="center" class="subtitulo1">Observaciones</td>
                <%                                          }
                            }%>
                <td align="center" class="subtitulo1">Detalle</td>
                <td align="center" class="subtitulo1">Fecha Negocio</td>
                <%                                       if (comefrom == null) {%><td align="center" class="subtitulo1">Fecha Desembolso</td>
                <td align="center" class="subtitulo1">Tipo Doc</td>
                <td align="center" class="subtitulo1">No_Docs Solicitado</td>
                <td align="center" class="subtitulo1">No_Docs</td>
                <td align="center" class="subtitulo1">Plazo</td>
                <td align="center" class="subtitulo1">Modo Aval</td>
                <td align="center" class="subtitulo1">Modo Custodia</td>
                <td align="center" class="subtitulo1">Modo Remesa</td>
                <%                                       }%>
                <td align="center" class="subtitulo1">Aval</td>
                <td align="center" class="subtitulo1">Convenio</td>
                <td align="center" class="subtitulo1">Formulario</td>
                <td align="center" class="subtitulo1">Solicitud</td>
                <td align="center" class="subtitulo1">Ciclo</td>
                <td align="center" class="subtitulo1">Usuario Creador</td>
                <%if (vista.equals("5")) {%>
                <td align="center" class="subtitulo1">Asesor Asignado</td>
                <%}%> 
                <%if (vista.equals("5")) {%>
                <td align="center" class="subtitulo1">Trazabilidad</td>
                
                <%}%>            
                <td align="center" class="subtitulo1">Reestructuracion</td>
                
                <%if (vista.equals("17")||actividad.equals("")) {%>
                <td align="center" class="subtitulo1">Agregar Garantias</td>                
                <%}%>
                
				<%if (vista.equals("7")||vista.equals("8")||vista.equals("10")) {%>
                  <td align="center" class="subtitulo1">Analista Asignado</td>                
                <%}%>																							  					 
            </tr>
            <%boolean generar;
              for (int i = 0; i < af.size(); i++) {
                             Negocios neg = (Negocios) af.get(i);
                             generar = model.Negociossvc.permisoGeneraPagare(neg);
                                                          
                             String act=vista.equals("6")?"RAD":(vista.equals("7")?"ANA":(vista.equals("8")?"DEC":(vista.equals("9")?"FOR":(vista.equals("10")?"REF":""))));
                                            NegocioTrazabilidad sw = trazaserv.buscarTraza(Integer.parseInt(neg.getFormulario()), act);
                             
                                             if(neg.getTipoConv()==null){
                                                neg.setTipoConv("Consumo");
                                            }                                           
                                            sum1 = sum1 + (neg.getVr_negocio());
                                            sum_tot_pagado = sum_tot_pagado + neg.getTotpagado();
                                                                sum2 = sum2 + (neg.getVr_desem());%>
                                                                
                                                              
                
               
                
            <tr class='<%= (i % 2 == 0 ? "filagris" : "filaazul")%>'>
                  <%if (vista.equals("6") || vista.equals("7") || vista.equals("8") || vista.equals("9")|| vista.equals("10")){%>
                <td align="center" class="bordereporte"><span style="color: red"><%=sw!=null?"R":""%></span></td>
                 <%}%>
                 
                 

                <td align="center"  class="bordereporte"  nowrap style="font-size:12">
                    <%if (vista.equals("5")||vista.equals("6")||vista.equals("7")||vista.equals("8")||vista.equals("9")||vista.equals("10")||vista.equals("11")||vista.equals("14")) {%>
                    <a  href="<%=BASEURL%>/jsp/applus/importar.jsp?num_osx=<%=neg.getCod_negocio()%>&tipito=negocio&form=<%=neg.getFormulario()%>&tipoconv=<%=neg.getTipoConv()%>&vista=<%=vista%>"> <%=i + 1%> </a>
                    <%} else {%>
                        <%if (neg.getTipoConv().equals("Microcredito")) {%>
                        <a target="_blank" title='Ver Archivos' href="<%=BASEURL%>/jsp/applus/mostrar_archivos.jsp?num_sol=<%=neg.getFormulario()%>&tipito=negocio"><%=i + 1%></a>
                        <%} else {%>
                        <%=i + 1%>
                        <%}%>
                    <%}%>    
                </td>
                
                 

                <td  class="bordereporte"  nowrap style="font-size:12"><a target="_blank" href="<%=BASEURL%>/jsp/applus/mostrar_archivos.jsp?num_osx=<%=neg.getCod_negocio()%>&tipito=negocio&numero_solicitud=<%=neg.getFormulario()%>&tipoConv=<%=neg.getTipoConv()%>"> <%=neg.getCod_negocio()%> </a></td>

                <td  class="bordereporte"  nowrap style="font-size:12" align="center" >
<img src="<%=BASEURL%>/images/botones/iconos/aceptar_ot.gif" name="exportar"  width="15" height="15" align="middle" id="exportar" style="cursor:pointer" title="Ver Reporte Pago"
onClick=" cargarReporte('<%=neg.getBcod()%>','<%=neg.getFechatran().substring(0, 18)%>'); " >
</td>

                <% if (comefrom == null && (vista.equals("6")||loggedUser.getTipo().equals("CLIENTETSP")||vista.equals("13") ||clvsrv.ispermitted(loggedUser.getTipo(),accion))) {%>
                <td  class="bordereporte"  nowrap style="font-size:12" align="center">
                    <%if ((neg.getTneg()).equals("02")&&neg.getTipoConv().equals("Consumo")) {
                       
                      %>

                    <img src="<%=BASEURL%>/images/botones/iconos/modificar.gif" name="exportar" id="exportar" onClick="javascript:var hWnd = window.open('<%=CONTROLLER%>?estado=Negocios&accion=Update&op=9&codn=<%=neg.getCod_negocio()%>','genletras','');   if ( document.window != null && !hWnd.opener )   { hWnd.opener = document.window;}" title="Generar Letras" style="cursor:hand ">


                    <%                                         }%>
                </td>
                <td  class="bordereporte"  nowrap style="font-size:12" align="center">
                    <% 
                    if(!neg.getEstado().equals("RECHAZADOS")){
                    
                    
                    if (((neg.getTneg()).equals("03")&&neg.getTipoConv().equals("Consumo"))||(((neg.getTneg()).equals("01")&&neg.getTipoConv().equals("Consumo")))) {
                        
        
                      Convenio convenio = model.gestionConveniosSvc.buscar_convenio(loggedUser.getBd(), neg.getId_convenio() + "");                        
                      if (convenio.getTipo().equals("Consumo") && convenio.isAval_anombre() ) { %>
                      
                        <img  src="<%=BASEURL%>/images/botones/iconos/modificar.gif" name="exportar" id="exportar" onClick=" Exportar('<%=neg.getCod_negocio()%>','<%=(generar)?"S":"N"%>');" title="Generar Pagare" style="cursor:hand ">
                            
                      <%}else{
                     %>


                    <img  src="<%=BASEURL%>/images/botones/iconos/modificar.gif" name="exportar" id="exportar" onClick="javascript:var hWnd = window.open('<%=CONTROLLER%>?estado=Negocios&accion=Update&op=99&codigo_negocio=<%=neg.getCod_negocio()%>','genletras','');   if ( document.window != null && !hWnd.opener )   { hWnd.opener = document.window;}" title="Generar Pagare" style="cursor:hand "/>

                    <%} 
                    }else if(neg.getTipoConv().equals("Microcredito")){ %>
                        
                        <img  src="<%=BASEURL%>/images/botones/iconos/modificar.gif" name="exportar" id="exportar" onClick=" Exportar('<%=neg.getCod_negocio()%>','S');" title="Generar Pagare" style="cursor:hand ">
   
                    <% }else if(neg.getTipoConv().equals("Libranza")){ %>
                        
                        <img  src="<%=BASEURL%>/images/botones/iconos/modificar.gif" name="exportar" id="exportar" onClick=" Exportar('<%=neg.getCod_negocio()%>','S');" title="Generar Pagare" style="cursor:hand ">
   
                    <%
                    }
}%>
                    
                    
                    
                    
                </td>
                <%}%>

                <% if (vista.equals("7")){%> <td  class="bordereporte"  nowrap style="font-size:12"><%=neg.getAnalista()!=null?neg.getAnalista():""%></td> <%}%>
             
                <td  class="bordereporte"  nowrap style="font-size:12"><%=neg.getNitp()!=null?neg.getNitp():""%></td>
                <td class="bordereporte" align="center" ><%=model.Negociossvc.obtenerTipoCliente(neg.getCod_cli())%></td>
                <td  class="bordereporte"  nowrap style="font-size:12"><%=neg.getNom_cli()%></td>
                <td class="bordereporte"  nowrap style="font-size:12"><%=neg.getCod_cli()%></td>
                <td  class="bordereporte"  nowrap style="font-size:12"><%=neg.getNom_cod()%></td>
                <td class="bordereporte"  nowrap style="font-size:12"><%=neg.getCod_cod()%></td>
                <td class="bordereporte"  nowrap style="font-size:12"><%=neg.getCodigou()%></td>
                <td class="bordereporte"  nowrap style="font-size:12"><%=neg.getPrograma()%></td>
                <%                                      if (comefrom == null) {%><td  class="bordereporte"  nowrap style="font-size:12"><%if (neg.getCmc().equals("01") && neg.getVr_negocio_solicitado() != 0.0d) {%><%=Util.customFormat(neg.getVr_negocio_solicitado())%><%} else {%><%=Util.customFormat(neg.getVr_negocio())%><%}%></td>
                <td  class="bordereporte"  nowrap style="font-size:12"><%if (neg.getCmc().equals("01")) {%><%=Util.customFormat(neg.getVr_negocio())%><%} else {%><%=Util.customFormat(neg.getVr_desem())%><%}%></td>
                <td class="bordereporte"  nowrap style="font-size:12"><%=Util.customFormat(neg.getVr_desem())%></td>
                <td class="bordereporte"  nowrap style="font-size:12"><%=Util.customFormat(neg.getTotpagado())%></td>
                <td class="bordereporte"  nowrap style="font-size:12"><%=Util.customFormat(neg.getValor_renovacion())%></td>
                <td class="bordereporte"  nowrap style="font-size:12"><%=neg.getPreaprobado_ex_cliente()%></td>
                <td class="bordereporte"  nowrap style="font-size:12"><%=neg.getTipo_credito()%></td>
                <td class="bordereporte"  nowrap style="font-size:12"><%=neg.getAgencia()%></td>
                <td class="bordereporte"  nowrap style="font-size:12">
                    <%if (vista.equals("5") && neg.getEstado().equals("RECHAZADOS")){%>
                    <span style="cursor:pointer;text-decoration: underline; color: #00F" onClick="verTrazaRechazado('<%=BASEURL%>','<%=neg.getFormulario()%>')" > <%=neg.getEstado()%></span><%}
                   else
                   {%>
                    <%=neg.getEstado()%>
                    <%}%>                
                </td>
                <%if (!loggedUser.getTipo().equals("CLIENTETSP")) {%><td class="bordereporte"  nowrap style="font-size:12"><%=neg.getObs()%></td>
                <%                                          }
                                    }%>
                <%                                       if (neg.getCmc().equals("01")) {%><td  class="bordereporte"  nowrap style="font-size:12">

                    <a href="<%=CONTROLLER%>?estado=Liquidador&accion=Negocios&opcion=Ejecutar&nit=<%=neg.getCod_cli()%>&cod_neg=<%=neg.getCod_negocio()%>&nomcli=<%=neg.getNom_cli()%>&state=<%=neg.getEstado()%>&pagarex=<%=neg.getPagare()%>&vista=<%=vista%>&form=<%=neg.getFormulario()%>&tipoconv=<%=neg.getTipoConv()%>&valor_total_poliza=<%=neg.getValor_total_poliza()%>"><img width="25" src="<%=BASEURL%>/images/botones/iconos/detalle.png" name="detalle" id="detalle" </a>

                </td>

                <%                                       } else {%>
                <td  class="bordereporte"  nowrap style="font-size:12">

                    <a href="#" onClick="javascript:var hWnd = window.open('<%=CONTROLLER%>?estado=creacion&accion=CompraCartera&opcion=consultar&consultablex=<%=neg.getCod_negocio()%>&nit=<%=neg.getCod_cli()%>&proveedor=<%=neg.getBcod()%>&est=<%=neg.getEstado()%>','vercompra','');   if ( document.window != null && !hWnd.opener )   { hWnd.opener = document.window;}">Ver Compra</a>                                         </td>

                <%                                       }%>
                <td  class="bordereporte"  nowrap style="font-size:12"><%=Util.MFecha(neg.getFecha_neg())%></td>
                <%                                       if (comefrom == null) {%><td  class="bordereporte"  nowrap style="font-size:12"><%=Util.MFecha(neg.getFechatran())%></td>

                <td  class="bordereporte"  nowrap style="font-size:12">
                    <%if ((neg.getTneg()).equals("01")) {%>Cheque<%} else {
                                                                                                    if ((neg.getTneg()).equals("03")) {%>Pagare<%} else {%>


                    <a href="#" onClick="javascript:var hWnd = window.open('<%=CONTROLLER%>?estado=Negocios&accion=Update&op=9&codn=<%=neg.getCod_negocio()%>','genletras','');   if ( document.window != null && !hWnd.opener )   { hWnd.opener = document.window;}"  title="Generar Letras">Letra
                    </a>

                    <%}
                                                                                                                                             }%>

                </td>

                <td  class="bordereporte"  nowrap style="font-size:12"><%if (neg.getNodocs_solicitado() != 0){%><%=neg.getNodocs_solicitado()%><%} else {%><%=neg.getNodocs()%><%}%></td>
                <td  class="bordereporte"  nowrap style="font-size:12"><%=neg.getNodocs()%></td>
                <%                                          if (neg.getCmc().equals("01")) {%><td  class="bordereporte"  nowrap style="font-size:12"><%=neg.getFpago()%> Dias</td>
                <%                                          } else {%><td  class="bordereporte"  nowrap style="font-size:12"></td>
                <%                                          }%>
                <%                                          if (neg.getCmc().equals("01")) {%><td  class="bordereporte"  nowrap style="font-size:12"><%if ((neg.getMod_aval()).equals("0")) {%>Asume establecimiento de comercio<%} else {%>Asume Cliente<%}%></td>
                <%                                          } else {%><td  class="bordereporte"  nowrap style="font-size:12"></td>
                <%                                          }%>
                <td  class="bordereporte"  nowrap style="font-size:12"><%if ((neg.getMod_cust()).equals("0")) {%>Asume establecimiento de comercio<%} else {%>Asume Cliente<%}%></td>
                <td  class="bordereporte"  nowrap style="font-size:12"><%if ((neg.getMod_rem()).equals("0")) {%>Con Remesa<%} else {%>Sin Remesa<%}%></td>
                <%                                       }%>
                <td  class="bordereporte"  nowrap style="font-size:12"><%=neg.getNumaval()%></td>
                <td  class="bordereporte"  nowrap style="font-size:12"><%=neg.getId_convenio() == 0 ? "" : neg.getId_convenio()%></td>
                <td  class="bordereporte"  nowrap style="font-size:12">
                    <%if  (!neg.getFormulario().equals("0")) {%>
                    <% if(neg.getTipoConv().equalsIgnoreCase("Libranza")) { %>
                    <a  href="/fintra/jsp/fenalco/avales/formulario_libranza.jsp?num_solicitud=<%=neg.getFormulario()%>"><%=neg.getFormulario()%></a>
                    <% } else {%>
                    <a  href="<%=BASEURL%>/jsp/fenalco/avales/solicitud_aval.jsp?num_solicitud=<%=neg.getFormulario()%>&vista=<%=(vista.equals("10"))? "8" : "3"%>&tipoconv=<%=neg.getTipoConv()%>"><%=neg.getFormulario()%></a>
                    <%} %>
                    <%} else {%>
                    <%=neg.getFormulario().equals("0") ? "" : neg.getFormulario()%>
                    <%}%>
                </td>
                <td  class="bordereporte"  nowrap style="font-size:12"><%=neg.getNo_solicitud()%></td>
                <td  class="bordereporte"  nowrap style="font-size:12"><%=neg.getCiclo()%></td>
                <td  class="bordereporte"  nowrap style="font-size:12" ><%=neg.getCreation_user()%></td>
                
                <%if (vista.equals("5")) {%>
                     <td  class="bordereporte" nowrap style="font-size:12px"><%= neg.getAsesor_asignado() %></td>
                 <%}%>  
                
                <%if (vista.equals("5")) {%>
                <td class="bordereporte" align="center" >
                    <img width="25" src="<%=BASEURL%>/images/botones/iconos/trazabilidad.gif" name="trazabilidad" id="trazabilidad" onClick="javascript:verTraza('<%=BASEURL%>','<%=neg.getFormulario()%>')" >
                </td>
                <%}%>              
                <td class="bordereporte" align="center"><%=model.Negociossvc.estadoReestructuracion(neg.getCod_negocio())%></td>
                <!--Empanada garantias -->
                <%if ((vista.equals("17")||actividad.equals(""))&&(neg.getId_convenio()==16 ||neg.getId_convenio()==26 || neg.getId_convenio()==40) && clvsrv.ispermitted(loggedUser.getTipo(),loggedUser.getCedula())) {%>
                <td align="center" class="bordereporte">
                    <button id="linkGarantias"  onclick="abrirGrarantias('<%=neg.getCod_negocio()%>','<%=neg.getCod_cli()%>','<%=neg.getNom_cli()%>')"
                            class="ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only"
                                    role="button" aria-disabled="false"><span class="ui-button-text">Agregar</span></button></td>
                <%}else if(actividad.equals("")){%>
                 <td align="center" class="bordereporte"></td>
                <%}%>

                <%if (vista.equals("7")||vista.equals("8")||vista.equals("10")) {%>
                  <td align="center" class="bordereporte"><%=neg.getAnalista_asignado()%></td>                
                <%}%>	 
               
            </tr>
            <%                              }%>
            <%                              if (comefrom == null) {%>
            <td align="center" class="bordereporte" colspan="1"></td>
            <td align="right" class="bordereporte" colspan="5"><strong>Totales</strong></td>
            <td align="center" class="bordereporte" colspan="1"></td>
            <td align="center" class="bordereporte" style="font-size:12"><strong><%=Util.customFormat(sum1)%></strong></td>
            <td align="center" class="bordereporte" style="font-size:12"><strong><%=Util.customFormat(sum2)%></strong></td>
            <td align="center" class="bordereporte" style="font-size:12"><strong><%=Util.customFormat(sum_tot_pagado)%></strong></td>
            <td align="center" class="bordereporte" colspan="9"></td>
            <%                              }%>

        </table>

        <div align="center">
            <%if (!(vista.equals("6") || vista.equals("7") || vista.equals("8") || vista.equals("9") || vista.equals("10") || vista.equals("12")||vista.equals("14"))) {%>
            <img src="<%=BASEURL%>/images/botones/exportarExcel.gif" name="exportar" id="exportar" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" onClick='javascript:exporta()' >
            <tsp:boton name="regresar" value="regresar" onclick="javascript:history.back(1)" />
            <%}%>
            <tsp:boton name="salir" value="salir" onclick="javascript:window.close()" />
        </div>
        <%} else {%>
        <table border="2" align="center">
            <tr>
                <td><table width="410" height="73" border="1" align="center"  bordercolor="#F7F5F4" bgcolor="#FFFFFF">
                        <tr>
                            <td width="282" align="center" class="mensajes">Esta actividad no posee negocios pendientes!</td>
                            <td width="28" background="<%=BASEURL%>/images/cuadronaranja.JPG">&nbsp;</td>
                            <td width="78">&nbsp;</td>
                        </tr>
                    </table></td>
            </tr>
        </table>

       <div align="center">
            <%if (!(vista.equals("6") || vista.equals("7") || vista.equals("8") || vista.equals("9")|| vista.equals("10") || vista.equals("12")||vista.equals("14"))) {%>
            
            <tsp:boton name="regresar" value="regresar" onclick="javascript:history.back(1)" />
            <%}%>
            <tsp:boton name="salir" value="salir" onclick="javascript:window.close()" />
            <%}%>
        </div>
    </div>
        <div id="divSalida" title="Pagare y Factura De Venta" style=" display: none"  class="ui-dialog"></div>
        <div id="divFormularioGarantias"   class="ui-dialog" style=" display: none" title="Agregar Garantias">
            <center>
                <table border="0" cellspacing="7" style="width:700px">
                    <thead>
                        <tr>
                            <th colspan="10" class="th">Informacion del Vehiculo</th>                        
                        </tr>
                    </thead>
                    <tbody>
                        <!--Informacion del cliente -->
                        <tr>
                            <td class="td" style="width: 100px">Negocio </td>
                            <td><input id="negocio" value="" size="8" readonly style="text-align: center"/></td>
                            <td class="td" style="width: 100px">Identificacion </td>
                            <td><input id="identificacion_cliente" value="" size="16" readonly style="text-align: center"/></td>
                            <td class="td" style="width: 100px">Nombre Cliente </td>
                            <td colspan="4"><input id="nombre_cliente" value="" size="53" readonly/></td>
                        </tr>
                         <!--Informacion del vehiculo -->
                        <tr>
                            <td  class="td" style="width: 100px">Tarjeta Propiedad</td>
                            <td><input id="nr_propiedad" value="" size="15"></td>                 
                            <td class="td" style="width: 100px">Chasis</td>
                            <td><input id="chasis" value="" size="16"></td>
                            <td  class="td" style="width: 100px">Motor</td>
                            <td><input id="motor" value="" size="16"></td>
                            <td  class="td" style="width: 100px">Nro. Poliza<span style="color: red"> *</span></td>
                            <td><input id="nro_poliza" value="" size="16"></td>
                        </tr>
                        <tr>
                            <td  class="td" style="width: 100px">Fecha Expedicion<span style="color: red"> *</span></td>
                            <td><input id="fe_expedicion" value="" size="15" readonly></td>                 
                            <td class="td" style="width: 100px">Fecha Vencimiento<span style="color: red"> *</span></td>
                            <td><input id="fe_vencimiento" value="" size="16" readonly></td>
                            <td  class="td" style="width: 100px">Negocio Poliza</td>
                            <td><select id="neg_poliza" name="neg_poliza"></select></td>
                            <td  class="td" style="width: 100px"><-relacionados-></td>
                            <td><input id="negocio" value="" size="16"></td>
                            
                        </tr>
                        <tr>
                            <td class="td" style="width: 100px">Placa<span style="color: red"> *</span></td>
                            <td class="td" > 
                                <input id="placa" value="" maxlength="6" size="15" />
                            </td>
                            <td class="td">Marca<span style="color: red"> *</span></td>
                            <td>
                                <input id="marca" size="16" />
                            </td>
                            <td class="td">Clase <span style="color: red"> *</span></td>
                            <td>
                                <select id="clase" name="clase" onchange="cargarComboxOption(6,'','')"> </select> 
                            </td>
                            <td class="td" >Servicio <span style="color: red"> *</span></td>
                            <td>
                               <input id="servicio" value="" name="servicio" size="16" readonly/>
                            </td>    
                        </tr>
                        <tr>
                            <td class="td">Referencia 1 <span style="color: red"> *</span></td>
                            <td>
                                <select id="referencia1"  onchange="cargarComboxOption(7,'','')"></select>       

                            </td>
                            <td class="td" >Referencia 2 <span style="color: red"> *</span></td>
                            <td>
                                <select id="referencia2"  onchange="cargarComboxOption(8,'','')"></select>       
                            </td>
                            <td class="td" ><label>Referencia 3 <span style="color: red"> *</span></label></td>
                            <td>
                                <select id="referencia3" onchange="listarInfoFasecolda()"></select>       
                            </td>
                            <td class="td" >Pais <span style="color: red"> *</span></td>
                            <td>
                                <input id="pais" value="" name="pais" size="16" readonly/>    

                            </td>
                        </tr>
                        <tr>                        
                            <td class="td">Modelo<span style="color: red"> *</span></td>
                            <td><input id="modelo" value="" size="15"  onkeyup="soloNumeros(this.id)" maxlength="4"/></td>
                            <td class="td" >Aseguradora <span style="color: red"> *</span></td>
                            <td>
                                <select id="aseguradora"></select>      
                            </td>
                            <td class="td" >Codigo fasecolda <span style="color: red"> *</span></td>
                            <td><input id="codigo_fasecolda" value="" size="16" readonly/> </td>
                            <td class="td">Valor Fasecolda <span style="color: red"> *</span></td>
                            <td><input id="valor_fasecolda" value="" readonly="true"  onkeyup="formato(this)"  style="font-size:13px;text-align: right; " size="16"/></td>

                        </tr>
                    </tbody>
                </table>
                <hr/>
                <table id="infoFasecolda"></table>
                <div id="page_infoFasecolda"></div>
            </center>

        </div>        
        <div id="divListaGarantias"   class="ui-dialog" style=" display: none" title="Garantias Automotor">
            <br>
            <center>
                <table id="lista_garantias_automotor"></table>
                <div id="page_garantias_automotor"></div>
            </center>
        </div>
        <div id="dialogMsg" title="Mensaje">
            <p id="msj" style="font-size: 12px !important;"></p>
        </div>
</body>
</html>