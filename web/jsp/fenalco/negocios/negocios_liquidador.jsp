<%@page import="com.tsp.operation.model.services.GestionSolicitudAvalService"%>
<%@page import="com.tsp.opav.model.services.ClientesVerService"%>
<%@page contentType="text/html"%>
<%@page session="true"%>
<%@page errorPage="/error/ErrorPage.jsp"%>
<%@ page import="java.sql.*" %>
<%@ page import="java.util.*" %>
<%@ page import="java.io.*" %>
<%@ page import="java.lang.*" %>
<%@page import="java.util.*"%>
<%@page import="com.tsp.operation.model.*"%>
<%@page import="com.tsp.operation.model.beans.*"%>
<%@page import="com.tsp.operation.controller.FormularioLibranzaAction"%>
<%@page import="com.tsp.util.*"%>
<%@page import="com.tsp.util.Util.*"%>
<%@include file="/WEB-INF/InitModel.jsp"%>
<%@taglib uri="http://jakarta.apache.org/taglibs/input-0.90" prefix="input" %>

<%
    String codnegx = (String) session.getAttribute("codrech"); //codigo del negocio
    String estadoneg = request.getParameter("state");
    Usuario usuario = (Usuario) session.getAttribute("Usuario");
    Usuario loggedUser = (Usuario) session.getAttribute("Usuario");
    Negocios negocio = model.Negociossvc.buscarNegocio(codnegx);
    session.setAttribute("cod_cli", negocio.getCod_cli());
    model.Negociossvc.setNegocio(negocio);
    Convenio convenio = model.gestionConveniosSvc.buscar_convenio(usuario.getBd(), negocio.getId_convenio() + "");
    boolean redescuento= model.gestionConveniosSvc.tieneRedescuento(negocio.getId_convenio()+"");

    ArrayList detNegocio = new ArrayList();

    String tipoconv=request.getParameter("tipoconv")!=null?request.getParameter("tipoconv"):"Consumo";
    if (tipoconv.equals("Libranza")) {
        FormularioLibranzaAction f = new FormularioLibranzaAction();
        f.init(usuario);
        detNegocio = f.buscarLiquidacion(codnegx);
    } else {
        detNegocio = model.Negociossvc.buscarDetallesNegocio(codnegx);
    }

    String tipo_negocio = negocio.getTneg();
    GestionSolicitudAvalService gsaserv = new GestionSolicitudAvalService(usuario.getBd());
    SolicitudAval sol =gsaserv.buscarSolicitud(codnegx);
    double intereses[] = new double[negocio.getNodocs() + 1];
    Calendar[] fechas_cheques = new Calendar[negocio.getNodocs()];
    String[] valores_cheques = new String[negocio.getNodocs()];
    String fpdoc[] = new String[negocio.getNodocs() + 1];
    String numche[] = new String[negocio.getNodocs() + 1];
    String fvdoc[] = new String[negocio.getNodocs() + 1];
    String vista=request.getParameter("vista")!=null?request.getParameter("vista"):"";
    String form=request.getParameter("form")!=null?request.getParameter("form"):"";
    String nomcli=request.getParameter("nomcli");
    String pagarex =request.getParameter("pagarex");
    String actividad=vista.equals("6")?"RADICACION":(vista.equals("7")?"ANALISIS":(vista.equals("8")?"DECISION":(vista.equals("9")?"FORMALIZACION":(vista.equals("10")?"REFERENCIACION":""))));

    //Datos negocio para domesa
    ArrayList datos_negocio1 = new ArrayList();
    ArrayList datos_negocio = new ArrayList();
    if (estadoneg.equals("ACEPTADO_DOMESA") || estadoneg.equals("RECHAZADO_DOMESA") || estadoneg.equals("APROBADO_DOMESA")) {
        datos_negocio1 = model.Negociossvc.getdatosnegocio(request.getParameter("numaval"));
        datos_negocio = (ArrayList) datos_negocio1.get(0);
    }

    // Esto es para llenar una matriz de intereses
    double diaant = Double.parseDouble(negocio.getFecha_neg().substring(8, 10));
    double diades = 0;
    if (diaant < 30) {
        diades = 30 - diaant;
    } else {
        diaant = 30;
    }
    double mat[][] = new double[3][negocio.getNodocs() + 1];
    int des = negocio.getNodocs();    
    mat[0][0] = 0;
    mat[1][0] = intereses[des] * (diades / 30);
    mat[2][0] = Math.round(mat[0][0] + mat[1][0]);
    for (int k = 0; k < negocio.getNodocs(); k++) {
        mat[0][k + 1] = intereses[des] * (diaant / 30);
        mat[1][k + 1] = intereses[des - 1] * (diades / 30);
        mat[2][k + 1] = Math.round(mat[0][k + 1]) + (mat[1][k + 1]);
        des = des - 1;
    }
    double sumi = 0;
    for (int k = 0; k < negocio.getNodocs() + 1; k++) {
        sumi = sumi + mat[2][k];
    }



    ClientesVerService clvsrv = new ClientesVerService(usuario.getBd());
    String perfil = clvsrv.getPerfil(usuario.getLogin());
    boolean sw_ver_saldos=clvsrv.ispermitted(perfil, "60");
    //String tipoconv=request.getParameter("tipoconv")!=null?request.getParameter("tipoconv"):"Consumo";
    if(tipoconv.equals("Microcredito")){
        model.Negociossvc.setLiquidacion(detNegocio);
    }
		
    if(tipoconv.equals("Consumo") && redescuento && convenio.isAval_anombre()){
        model.Negociossvc.setLiquidacion(detNegocio);
    }

%>

<html>
    <head>
        <title>Consulta de negocios</title>

        <link href="<%=BASEURL%>/css/estilostsp.css" rel="stylesheet" type="text/css">
        <style type="text/css">
            <!--
            .style1 {font-size: 12px}
            .style3 {font-weight: bold; font-size: 16px;}
            -->
        </style>
        <script type='text/javascript' src="<%=BASEURL%>/js/Validacion.js"></script>

        <link href="<%=BASEURL%>/css/mac_os_x.css" rel="stylesheet" type="text/css">
        <link href="<%=BASEURL%>/css/default.css" rel="stylesheet" type="text/css">
        <script src="<%=BASEURL%>/js/negocioTrazabilidad.js" type="text/javascript"></script>
        <script type='text/javascript' src="<%= BASEURL%>/js/prototype.js"></script>
        <script src="<%=BASEURL%>/js/effects.js" type="text/javascript"></script>
        <script src="<%=BASEURL%>/js/window.js" type="text/javascript"></script>


        <script type='text/javascript' src="<%=BASEURL%>/js/validar.js"></script>
        <script type='text/javascript' src="<%= BASEURL%>/js/boton.js"></script>
        <script type='text/javascript' src="<%= BASEURL%>/js/general.js"></script>

        <script type='text/javascript' src="<%= BASEURL%>/js/jquery/jquery-ui/jquery.ui.min.js"></script>

        <link href="<%= BASEURL%>/css/estilostsp.css" rel="stylesheet" type="text/css">
        <link href="<%= BASEURL%>/css/jquery/jquery-ui/jquery-ui.css" rel="stylesheet" type="text/css"/>


        <script type='text/javascript' src="<%= BASEURL%>/js/boton.js"></script>
        <script type='text/javascript' src="<%= BASEURL%>/js/jquery/jquery-ui/jquery.min.js"></script>
        <script type='text/javascript' src="<%= BASEURL%>/js/jquery/jquery-ui/jquery.ui.min.js"></script>
        <link href="<%= BASEURL%>/css/estilosFintra.css" rel='stylesheet'/>


        <script language="javascript">
            var tasa="<%=negocio.getNuevaTasa()!=null?negocio.getNuevaTasa():"S"%>";
            function validar(formulario,tipo)
            {   exform=formulario.action;
                formulario.action=formulario.action+tipo;
                var sw=0;
                var cont=0;
                if(tipo!=15 && tipo!=14)
                {   for(var i=0;i<form1.length;i++)
                    {   var ele = form1.elements[i];
                        if(tipo==2)
                        {   if(ele.type=='text' && ele.value=='' && ele.name!='no_cta' && ele.name!='plaza' && ele.name!='sucursal_afiliado' && ele.name!='c_fintra' && ele.name!='c_custodia')
                            {   cont++;
                            }
                        }
                        else
                        {   if(ele.type=='text' && ele.value=='' )
                            {   cont++;
                            }
                        }
                    }
                }
                if (cont == 0)
                {

                    if (confirm("�Desea Realizar la Operacion?"))
                    {
                        form1.submit()
                    }
                }
                else
                {   alert ("No puede dejar datos en blanco");
                }
                formulario.action=exform;
            }
            function llenar(a)
            {   var cont=0;
                var lonc=a.length;
                var lonp=a.length;
                var camb=0;
                var sw=0;
                var mid=2;
                mid=a.indexOf('-');
                tempb=a.substring(mid+1,lonc);
                tempf=a.substring(0,mid+1);

                if ( tempb.indexOf('000000') == 0)
                {   tempb=a.substring(mid+7,lonc)
                    tempf=a.substring(0,mid+7);
                }
                if ( tempb.indexOf('00000') == 0)
                {   tempb=a.substring(mid+6,lonc)
                    tempf=a.substring(0,mid+6);
                }
                if ( tempb.indexOf('0000') == 0)
                {   tempb=a.substring(mid+5,lonc)
                    tempf=a.substring(0,mid+5);
                }
                if ( tempb.indexOf('000') == 0)
                {   tempb=a.substring(mid+4,lonc)
                    tempf=a.substring(0,mid+4);
                }
                if ( tempb.indexOf('00') == 0)
                {   tempb=a.substring(mid+3,lonc)
                    tempf=a.substring(0,mid+3);
                }
                if ( tempb.indexOf('0') == 0)
                {   tempb=a.substring(mid+2,lonc)
                    tempf=a.substring(0,mid+2);
                }
                a=parseInt(tempb);
                for(var i=1;i<form1.length;i++)
                {   var ele = form1.elements[i];
                    if(ele.type=='text' && ele.id=='do' )
                    {   var aux=a+i;
                        ele.value=tempf+aux;
                    }
                }
            }

            function imprSelec(nombre)
            {   var ficha = document.getElementById(nombre);
                var ventimp = window.open(' ', 'popimpr');
                ventimp.document.write( ficha.innerHTML );
                ventimp.document.close();
                ventimp.print( );
                ventimp.close();
            }
            function habreliquidar(){
                if(document.getElementById("reliquida").checked){
                    document.getElementById("tasa").disabled=false;
                    document.getElementById("imgaceptar").style.visibility= "visible";
                }else{
                    document.getElementById("tasa").disabled=true;
                    document.getElementById("imgaceptar").style.visibility= "hidden";
                }
            }
            function cambiartasa(){
                tasa=document.getElementById("tasa").value;
            }
            
            function avalar(){
                var aval=document.getElementById("num_aval").value;
                if(aval!=''&&aval!=0){
                    if (confirm("�Esta seguro que desea avalar el negocio de forma manual?"))
                    {
                        location.href='<%=CONTROLLER%>?estado=Negocios&accion=Update&op=17&cod=<%=codnegx%>&aval='+aval
                    }


                }else{
                    alert("debe digitar el numero de aval");
                }
            }
            
			
			
			
			
            var j = jQuery.noConflict();		
			
            function cerrar()
            {
                j("#divSalida").dialog('close')
            }


            j(document).ready(function(){
                j("#divSalida").dialog({
                    autoOpen: false,
                    height: "auto",
                    width: "35%",
                    modal: true,
                    autoResize: true,
                    resizable: false,
                    position: "center"               

                });
              
            });

            function Exportar()
            {
   
   
                j("#divSalida").dialog("open");
                j("#divSalida").css('height', 'auto');
                j("#divSalida").dialog("option", "position", "center");
                var url ="<%=CONTROLLER%>?estado=Liquidador&accion=Negocios";
                var base ="<%=BASEURL%>"
                j.ajax({
                    dataType: 'html',
                    url:url,
                    data: {
                        opcion : 'exportarPlanPagos',
			numsolc:j("#numsolc").val()
                    },
                    method: 'post',
                    success:  function (resp){
                        var boton = "<div style='text-align:center'><img src='"+base+"/images/botones/salir.gif'  onMouseOver='botonOver(this);' onMouseOut='botonOut(this);'  onclick='cerrar()' /></div>"
                        document.getElementById('divSalida').innerHTML = resp+ "<br/><br/><br/><br/>"+boton
                    },
                    error: function(){
                        alert("Ocurrio un error enviando los datos al servidor");
                    }

                });

            }	
            
            function Exportarplandepagofintra()
            {
   
   
                j("#divSalida").dialog("open");
                j("#divSalida").css('height', 'auto');
                j("#divSalida").dialog("option", "position", "center");
                var url ="<%=CONTROLLER%>?estado=Liquidador&accion=Negocios";
                var base ="<%=BASEURL%>"
                j.ajax({
                    dataType: 'html',
                    url:url,
                    data: {
                        opcion : 'ExportarPlanPagosFintra',
			numsolc:j("#numsolc").val()
                    },
                    method: 'post',
                    success:  function (resp){
                        var boton = "<div style='text-align:center'><img src='"+base+"/images/botones/salir.gif'  onMouseOver='botonOver(this);' onMouseOut='botonOut(this);'  onclick='cerrar()' /></div>"
                        document.getElementById('divSalida').innerHTML = resp+ "<br/><br/><br/><br/>"+boton
                    },
                    error: function(){
                        alert("Ocurrio un error enviando los datos al servidor");
                    }

                });

            }	


            j(document).ready(function(){
                j("#divSalidaConvenios").dialog({
                    autoOpen: false,
                    height: "auto",
                    width: "50%",
                    modal: true,
                    autoResize: true,
                    resizable: false,
                    position: "center"

                });

            });


            function seleccionar_convenio(baseurl,negocio,tipoconv)
            {
                j("#divSalidaConvenios").dialog("open");
                j("#divSalidaConvenios").css('height', 'auto');

                var base ="<%=BASEURL%>";
                var url=base+'/jsp/fenalco/convenio/ListaConvenios.jsp?negocio='+negocio+"&tipoconv="+tipoconv;
                j("#divSalidaConvenios").load(url);
            }

            function reactivar_negocio(neg,form)
            {


                j("#divSalida").dialog("open");
                j("#divSalida").css('height', 'auto');
                j("#divSalida").dialog("option", "position", "center");
                var url ="<%=CONTROLLER%>?estado=Liquidador&accion=Negocios";
                var base ="<%=BASEURL%>"
                j.ajax({
                    dataType: 'html',
                    url:url,
                    data: {
                        opcion : 'reactivar_negocio',
                        numero_solicitud:form,
                        cod_neg:neg
                    },
                    method: 'post',
                    success:  function (resp){
                        var boton = "<div style='text-align:center'><img src='"+base+"/images/botones/salir.gif'  onMouseOver='botonOver(this);' onMouseOut='botonOut(this);'  onclick='cerrar()' /></div>"
                        document.getElementById('divSalida').innerHTML = resp+ "<br/><br/><br/><br/>"+boton
                    },
                    error: function(){
                        alert("Ocurrio un error enviando los datos al servidor");
                    }

                });

            }
		
			
        </script>

        <style type="text/css">
            div.ui-dialog{
                font-size:11px;

            }

        </style>
    </head>
    <body onLoad="<%=(vista.equals("3")&&(estadoneg.equals("ACEPTADO")||estadoneg.equals("PRE_ACEPTADO")))?"habreliquidar();":""%>">
        <div id="capaSuperior" style="position:absolute; width:100%; height:100px; z-index:0; left: 0px; top: 0px;">
            <jsp:include page="/WEB-INF/toptsp.jsp?encabezado=Consultar Negocio"/>

        </div>
        <div id="capaCentral" style="position:absolute; width:100%; height:85%; z-index:0; left: 0px; top: 100px; overflow: scroll;">
            <p>
                <a onClick="javascript:imprSelec('seleccion')"><img src="<%=BASEURL%>/images/imprime.gif" width="70" height="19" style="cursor:hand"></a>
            <div id="seleccion">                
                <p align="center" class="style3"> CLIENTE : <%=nomcli%>&nbsp;&nbsp;(&nbsp;<%=request.getParameter("Nit")%>&nbsp;)</p>
                <p align="center" class="style3"> FORMULARIO : <%=form%> <%if (vista.equals("6") || vista.equals("7") || vista.equals("8") || vista.equals("9")|| vista.equals("10")){%>
                    - ACTIVIDAD : <%=actividad%>
                    <%}%></p>                
                
                <form name="form1" id="form1" method="post" action="<%=CONTROLLER%>?estado=Negocios&accion=Update&codneg=<%=session.getAttribute("codrech")%>&op=">
                    <% if(tipoconv.equals("Consumo") && !convenio.isAval_anombre()){%>
                    <table align="center" width="78%" border="0" cellpadding='0' cellspacing='0'>
                        <tr>
                            <td >
                                <table align="center" width="100%" cellpadding='0' cellspacing='0' class="tablaInferior" border="1" >
                                    <tr class="fila">
                                        <td colspan = "2" align="center">
                                            <p>Informaci�n del Negocio</p>
                                        </td>
                                    </tr>
                                    <tr class="filagris">
                                        <td width='50%'>
                                            <p>Valor del negocio</p>
                                        </td>
                                        <td width='50%'>
                                            <p><%=Util.customFormat(negocio.getVr_negocio())%></p>
                                        </td>
                                    </tr>
                                    <tr class="filaazul">
                                        <td >
                                            <p>No de<%if (negocio.getTneg().equals("01")) {%> Cheques
                                                <%} else if (tipo_negocio.equals("02")) {%> Letras
                                                <%} else {%> Pagares
                                                <%}%>

                                            </p>
                                        </td>
                                        <td>
                                            <p ><%=negocio.getNodocs()%></p>
                                        </td>
                                    </tr>
                                    <tr class="filagris">
                                        <td >
                                            <p>Valor desembolso</p>
                                        </td>
                                        <td>
                                            <p><%=Util.customFormat(negocio.getVr_desem())%></p>
                                        </td>
                                    </tr>
                                    <tr class="filaazul">
                                        <td>
                                            <p>Valor de <%if (negocio.getTneg().equals("01")) {%>los Cheques
                                                <%} else if (negocio.getTneg().equals("02")) {%>las Letras
                                                <%} else {%>los Pagares
                                                <%}%>
                                            </p>
                                        </td>
                                        <td>
                                            <p><%=detNegocio.size()>0?Util.customFormat(((DocumentosNegAceptado) detNegocio.get(0)).getValor()):0%></p>
                                        </td>
                                    </tr>
                                    <tr class="filagris">
                                        <td >
                                            <p>Fecha negocio</p>
                                        </td>
                                        <td>
                                            <p ><%=Util.ObtenerFechaCompleta(Util.crearCalendar(negocio.getFecha_neg()))%></p>
                                        </td>
                                    </tr>
                                    <% if (pagarex != null && !(pagarex.equals("0"))) {%>
                                    <tr class="filagris">
                                        <td>
                                            <p> Pagar�</p>
                                        </td>
                                        <td>
                                            <p><%=pagarex%></p>
                                        </td>
                                    </tr>
                                    <%}%>
                                </table>
                            </td>
                            <td>
                                <table align="center" width="100%" cellpadding='0' cellspacing='0' class="tablaInferior" border="1" >
                                    <tr class="fila">
                                        <td width='25%' align="center">
                                            <p></p>
                                        </td>
                                        <td width='50%' align="center">
                                            <p>Establecimiento de comercio</p>
                                        </td>
                                        <td width='25%' align="center">
                                            <p>Cliente</p>
                                        </td>
                                    </tr>
                                    <tr class="filagris">
                                        <td width='25%' align="center">
                                            <p>Custodia</p>
                                        </td>
                                        <td width='50%' align="center">
                                            <p>
                                                <%=negocio.getMod_cust().equals("0")?"SI":"NO"%>
                                            </p>
                                        </td>
                                        <td width='25%' align="center">
                                            <p>
                                                <%=negocio.getMod_cust().equals("0")?"NO":"SI"%>
                                            </p>
                                        </td>
                                    </tr>
                                    <tr class="filaazul">
                                        <td width='25%' align="center">
                                            <p>Aval</p>
                                        </td>
                                        <td width='50%' align="center">
                                            <p>
                                                <%=negocio.getMod_aval().equals("0")?"SI":"NO"%>
                                            </p>
                                        </td>
                                        <td width='25%' align="center">
                                            <p>
                                                <%=negocio.getMod_aval().equals("0")?"NO":"SI"%>
                                            </p>
                                        </td>
                                    </tr>
                                    <%if (negocio.getMod_rem().equals("0")) {%>
                                    <tr class="filagris">
                                        <td width='25%' align="center">
                                            <p>Remesas</p>
                                        </td>
                                        <td width='50%' align="center">
                                            <p>
                                                <%=negocio.getEsta().equals("0")?"SI":"NO"%>
                                            </p>
                                        </td>
                                        <td width='25%' align="center">
                                            <p>
                                                <%=negocio.getEsta().equals("0")?"NO":"SI"%>
                                            </p>
                                        </td>
                                    </tr>
                                    <%}%>
                                </table>
                            </td>
                        </tr>
                        <tr>
                            <td colspan='2'>
                                <br><br>
                            </td>
                        </tr>                        
                        <tr>
                            <td colspan="2">
                                <br/>
                                <table class="tablaInferior" align="center" width="100%" border="1">
                                    <tr class="fila">
                                        <th># Doc</th>
                                        <th>Fecha</th>
                                        <% if(sw_ver_saldos) { %><th> Saldo inicial</th><%}%>
                                        <th>Valor</th>
                                        <% if(sw_ver_saldos) { %><th>Saldo final</th><%}%>
                                        <th>Consecutivo</th>
                                    </tr>
                                    <%for (int i = 0; i < detNegocio.size(); i++) {
                                        DocumentosNegAceptado doc = (DocumentosNegAceptado) detNegocio.get(i);
                                        Calendar cal = Util.crearCalendar(doc.getFecha());
                                        fechas_cheques[i] = cal;
                                        valores_cheques[i] = Util.customFormat(doc.getValor());
                                        intereses[i+1] = Math.round(doc.getInteres());
                                        fpdoc[i+1] = doc.getFecha();
                                        numche[i+1] = doc.getItem();
                                        fvdoc[i+1] = (negocio.getFecha_neg());
                                    %>
                                    <tr class='<%= (i % 2 == 0) ? "filaazul" : "filagris"%>' align="right">
                                        <td align="center"><%=doc.getItem()%></td>
                                        <td><%=Util.ObtenerFechaCompleta(cal)%></td>
                                        <%if(sw_ver_saldos) { %> <td><%=Util.customFormat(doc.getSaldo_inicial())%></td><% }%>
                                        <td><%=Util.customFormat(doc.getValor())%></td>
                                        <%if(sw_ver_saldos) { %><td><%=Util.customFormat(doc.getSaldo_final())%></td><%}%>
                                        <td><%=i+1%></td>
                                    </tr>
                                    <%}%>
                                    <tr class="fila" align="right">
                                        <td></td>
                                        <td>Total a pagar</td>
                                        <td colspan="2"><%=Util.customFormat(negocio.getTotpagado())%></td>
                                        <td></td>
                                        <td></td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <strong>
                                    <%if (estadoneg.equals("AVALADO") || estadoneg.equals("ACEPTADO_DOMESA") || estadoneg.equals("RECHAZADO_DOMESA")) {
                                    %>
                                    No de Aval : <%=negocio.getNumaval()%>
                                    <input  type="hidden" name="aval" value="<%=negocio.getNumaval()%>" readonly>
                                    <p class="style3">N&uacute;mero de la cuenta&nbsp;:<%=sol.getNumTipoNegocio()%>
                                        <input type="hidden" name="cuenta_cheque" value="<%=(estadoneg.equals("AVALADO"))?sol.getNumTipoNegocio():((negocio.getCuenta_cheque()!=null)?negocio.getCuenta_cheque():"")%>"></p>


                                    <%

                                      }
                                      if (tipo_negocio.equals("CH")) {%>
                                    Banco de Cheque : <%=negocio.getBcocode()!=null?negocio.getBcocode():""%>
                                    <input type="hidden" name="bcofid" value='<%=negocio.getCodigoBanco()%>'>
                                    <%  } else {%>
                                    <input type="hidden" name="bcofid" value='170'>
                                    <%}%>
                                </strong>
                            </td>
                        </tr>                       
                    </table>

                    <%if (!(loggedUser.getTipo().equals("CLIENTETSP") || ((vista.equals("3") || (vista.equals("4"))) && (estadoneg.equals("ACEPTADO") || estadoneg.equals("PRE_ACEPTADO"))))) {%>

                    <input name="nit" type="hidden" value="<%=negocio.getCod_cli()%>">
                    <input name="cod_neg" type="hidden" value="<%=negocio.getCod_negocio()%>">
                    <input name="nodocs" type="hidden" value="<%=negocio.getNodocs()%>">
                    <input name="vrdocs" type="hidden" value="<%=detNegocio.size()>0?((DocumentosNegAceptado) detNegocio.get(0)).getValor():0%>">
                    <input name="codtab" type="hidden" value="<%=negocio.getCod_tabla()%>">
                    <input name="aux" type="hidden" value="<%=negocio.getNit_tercero()%>">
                    <input name="vrnego" type="hidden" value="<%=negocio.getTotpagado()%>">
                    <input name="vrdesembolso" type="hidden" value="<%=negocio.getVr_desem()%>">
                    <input name="fechaneg" type="hidden" value="<%=negocio.getFecha_neg()%>">
                    <input name="vrnegocio" type="hidden" value="<%=negocio.getVr_negocio()%>">
                    <input name="tituloValor" type="hidden" value="<%=negocio.getTneg()%>">
                    <input name="convenio" type="hidden" value="<%=negocio.getId_convenio()%>">
                    <input name="vlraval" type="hidden" value="<%=negocio.getvalor_aval()%>">

                    <%
                        //Verificar si es necesario cambiar esto
                        if (negocio.getTneg().equals("01")) {
                            tipo_negocio = "CH";
                        } else {
                            if (negocio.getTneg().equals("02")) {
                                tipo_negocio = "LT";
                            } else {
                                tipo_negocio = "PG";
                            }
                        }
                        ArrayList afiliado = model.Negociossvc.getAfiliado(negocio.getNit_tercero());
                    %>
                    <input name="tneg" type="hidden" value="<%=tipo_negocio%>">
                    <%
                        session.setAttribute("afiliado", afiliado);
                        session.setAttribute("nom_cli", nomcli);
                        session.setAttribute("cod_cli", request.getParameter("cod_cli"));
                        session.removeAttribute("nombe");
                        session.setAttribute("valores_cheques", valores_cheques);
                        session.setAttribute("fechas_cheques", fechas_cheques);//?
                        session.setAttribute("fvdoc", fvdoc);//?
                        session.setAttribute("fpdoc", fpdoc);//?
                        session.setAttribute("numche", numche);//?
                        session.setAttribute("inter", mat);//?
                        session.setAttribute("cuenta_cheque", (estadoneg.equals("AVALADO")) ? sol.getNumTipoNegocio() : ((negocio.getCuenta_cheque() != null) ? negocio.getCuenta_cheque() : ""));
                        if (estadoneg.equals("RECHAZADO_DOMESA") || estadoneg.equals("ACEPTADO_DOMESA")) {
                            session.setAttribute("ex_numaval", request.getParameter("numaval"));
                            session.setAttribute("ex_no_orden", (String) model.Negociossvc.getno_orden((String) request.getParameter("numaval")));
                        }
                    %>
                    <br/>


                    <%  session.removeAttribute("codrech");
                            }%>
                    <%}%>
                </form>
                
                <% if (tipoconv.equals("Libranza")) {%>
                <div align="center" >
                    <jsp:include page="/jsp/fenalco/liquidadores/LiquidacionLibranza.jsp">
                        <jsp:param name="codneg" value="<%=codnegx%>" />
                        <jsp:param name="numsolc" value="<%=form%>" />
                        <jsp:param name="fechanegocio" value="<%=negocio.getFecha_neg()%>" />
                        <jsp:param name="nit" value="<%=negocio.getCod_cli()%>" />
                        <jsp:param name="convid" value="<%=negocio.getId_convenio()%>" />
                        <jsp:param name="tneg" value="<%=negocio.getTneg()%>" />
                        <jsp:param name="numcuotas" value="<%=negocio.getNodocs()%>" />
                        <jsp:param name="fpago" value="<%=negocio.getFpago()%>" />
                        <jsp:param name="valornegocio" value="<%=negocio.getVr_negocio()%>" />
                        <jsp:param name="tasainteres" value="<%=negocio.getTasa()%>" />
                        <jsp:param name="cat" value="<%=negocio.getPorcentaje_cat()%>" />
                        <jsp:param name="capacitacion" value="<%=negocio.getValor_capacitacion()%>" />
                        <jsp:param name="central" value="<%=negocio.getValor_central()%>" />
                        <jsp:param name="consulta" value="S" />
                        <jsp:param name="vista" value="<%=vista%>" />
                        <jsp:param name="fechaliqui" value="<%=negocio.getFecha_liquidacion()%>" />
                        <jsp:param name="fechatrans" value="<%=negocio.getFechatran()%>" />
                        <jsp:param name="state" value="<%=estadoneg%>" />
                        <jsp:param name="nomcli" value="<%=nomcli%>" />
                        <jsp:param name="pagarex" value="<%=pagarex%>" />
                    </jsp:include>
                </div>
                <%}%>

                <% if (tipoconv.equals("Microcredito")) {%>
                <div align="center" >
                    <jsp:include page="/jsp/fenalco/liquidadores/LiquidacionMicrocredito.jsp">
                        <jsp:param name="numsolc" value="<%=form%>" />
                        <jsp:param name="fechanegocio" value="<%=negocio.getFecha_neg()%>" />
                        <jsp:param name="nit" value="<%=negocio.getCod_cli()%>" />
                        <jsp:param name="convid" value="<%=negocio.getId_convenio()%>" />
                        <jsp:param name="tneg" value="<%=negocio.getTneg()%>" />
                        <jsp:param name="numcuotas" value="<%=negocio.getNodocs()%>" />
                        <jsp:param name="fpago" value="<%=negocio.getFpago()%>" />
                        <jsp:param name="valornegocio" value="<%=negocio.getVr_negocio()%>" />
                        <jsp:param name="tasainteres" value="<%=negocio.getTasa()%>" />
                        <jsp:param name="cat" value="<%=negocio.getPorcentaje_cat()%>" />
                        <jsp:param name="capacitacion" value="<%=negocio.getValor_capacitacion()%>" />
                        <jsp:param name="central" value="<%=negocio.getValor_central()%>" />
                        <jsp:param name="consulta" value="S" />
                        <jsp:param name="vista" value="<%=vista%>" />
                        <jsp:param name="fechaliqui" value="<%=negocio.getFecha_liquidacion()%>" />
                        <jsp:param name="fechatrans" value="<%=negocio.getFechatran()%>" />
                        <jsp:param name="state" value="<%=estadoneg%>" />
                        <jsp:param name="nomcli" value="<%=nomcli%>" />
                        <jsp:param name="pagarex" value="<%=pagarex%>" />
                        <jsp:param name="valorfianza" value="<%=negocio.getValor_fianza()%>" />
                        <jsp:param name="valor_total_poliza" value="<%=negocio.getValor_total_poliza()%>" />
                        <jsp:param name="porc_dto_aval" value="<%=negocio.getPorc_dto_aval()%>" />
                        <jsp:param name="porc_fin_aval" value="<%= negocio.getPorc_fin_aval()%>" />
                    </jsp:include>
                </div>
                <%}%>



                <% if(tipoconv.equals("Consumo") && redescuento && convenio.isAval_anombre()){%>
                <div align="center" >

                    <jsp:include page="/jsp/fenalco/liquidadores/LiquidacionNegocio.jsp">
                        <jsp:param name="numsolc" value="<%=form%>" />
                        <jsp:param name="fechanegocio" value="<%=negocio.getFecha_neg()%>" />
                        <jsp:param name="nit" value="<%=negocio.getCod_cli()%>" />
                        <jsp:param name="convid" value="<%=negocio.getId_convenio()%>" />
                        <jsp:param name="tneg" value="<%=negocio.getTneg()%>" />
                        <jsp:param name="numcuotas" value="<%=negocio.getNodocs()%>" />
                        <jsp:param name="fpago" value="<%=negocio.getFpago()%>" />
                        <jsp:param name="valornegocio" value="<%=negocio.getVr_negocio()%>" />
                        <jsp:param name="tasainteres" value="<%=negocio.getTasa()%>" />
                        <jsp:param name="cat" value="<%=negocio.getPorcentaje_cat()%>" />
                        <jsp:param name="capacitacion" value="<%=negocio.getValor_capacitacion()%>" />
                        <jsp:param name="central" value="<%=negocio.getValor_central()%>" />
                        <jsp:param name="consulta" value="S" />
                        <jsp:param name="vista" value="<%=vista%>" />
                        <jsp:param name="fechaliqui" value="<%=negocio.getFecha_liquidacion()%>" />
                        <jsp:param name="fechatrans" value="<%=negocio.getFechatran()%>" />
                        <jsp:param name="state" value="<%=estadoneg%>" />
                        <jsp:param name="nomcli" value="<%=nomcli%>" />
                        <jsp:param name="pagarex" value="<%=pagarex%>" />
                    </jsp:include>
                </div>
                <%
                    
                    for (int i = 0; i < detNegocio.size(); i++) {
                        try{
                        DocumentosNegAceptado doc = (DocumentosNegAceptado) detNegocio.get(i);
                        Calendar cal = Util.crearCalendar(doc.getFecha());
                        fechas_cheques[i] = cal;
                        valores_cheques[i] = Util.customFormat(doc.getValor());
                        intereses[i+1] = Math.round(doc.getInteres());
                        fpdoc[i+1] = doc.getFecha();
                        numche[i+1] = doc.getItem();
                        fvdoc[i+1] = (negocio.getFecha_neg());
                        }catch(Exception e){
                            e.printStackTrace();
                        }
                    }

                    session.setAttribute("fvdoc", fvdoc);//?
                    session.setAttribute("fpdoc", fpdoc);//?
                    session.setAttribute("numche", numche);//?
                    session.setAttribute("inter", mat);//?
                }%>
                


                <% if (tipoconv.equals("Consumo")&&!redescuento) {%>
                <br/>
                <div align="center" >
                    <jsp:include page="/jsp/fenalco/avales/solicitud_aval_documentos.jsp">
                        <jsp:param name="num_solicitud" value="<%=form%>" />
                        <jsp:param name="vista" value="<%=vista%>" />
                        <jsp:param name="consulta" value="S" />
                    </jsp:include>
                </div>
                <%}%>
                
                
                <% if(tipoconv.equals("Consumo") && redescuento){%>
          
                <%

                    for (int i = 0; i < detNegocio.size(); i++) {
                        DocumentosNegAceptado doc = (DocumentosNegAceptado) detNegocio.get(i);
                        Calendar cal = Util.crearCalendar(doc.getFecha());
                        fechas_cheques[i] = cal;
                        valores_cheques[i] = Util.customFormat(doc.getValor());
                        intereses[i+1] = Math.round(doc.getInteres());
                        fpdoc[i+1] = doc.getFecha();
                        numche[i+1] = doc.getItem();
                        fvdoc[i+1] = (negocio.getFecha_neg());
                    }

                    session.setAttribute("fvdoc", fvdoc);//?
                    session.setAttribute("fpdoc", fpdoc);//?
                    session.setAttribute("numche", numche);//?
                    session.setAttribute("inter", mat);//?
                }%>

                <table align="center" >
                    <tr>
                        <td colspan="2" align="center"><p>&nbsp;</p>
                            <%if (vista.equals("3") && (estadoneg.equals("ACEPTADO") || estadoneg.equals("PRE_ACEPTADO"))&&negocio.getActividad().equals("LIQ")) {%>
                            <p class="style3">Habilitar Re-liquidacion? &nbsp;<input type="checkbox" name="reliquida" id="reliquida" value=""  <%=  (negocio.getEstado().equals("Q")) ? "checked='checked'" : ""%> onChange="habreliquidar();" ></p>
                            <p>
                                Nueva tasa?
                                <select name="tasa" id="tasa" onChange="cambiartasa();">
                                    <option value="S" <%= (negocio.getNuevaTasa() != null && negocio.getNuevaTasa().equals("S")) ? "selected" : ""%>>Si</option>
                                    <option value="N" <%= (negocio.getNuevaTasa() != null && negocio.getNuevaTasa().equals("N")) ? "selected" : ""%>>No</option>
                                </select>
                            </p>
                            <img alt="" src = "<%=BASEURL%>/images/botones/aceptar.gif" style = "cursor:pointer" name = "imgaceptar" id="imgaceptar"  onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" onClick="location.href='<%=CONTROLLER%>?estado=Negocios&accion=Update&op=16&cod=<%=codnegx%>&tasa='+tasa">

                            <%}%>

                            <!--   Proceso de Aval Manual -ivargas
                            <%if (vista.equals("4") && (estadoneg.equals("ACEPTADO") || estadoneg.equals("PRE_ACEPTADO"))) {%>
                               <p class="style3">N&uacute;mero de Aval &nbsp;</p>
                              <input  type="text" name="num_aval" id="num_aval" value="<%=negocio.getNumaval()%>" ><br><br>

                              <img alt="" src = "<%=BASEURL%>/images/botones/aceptar.gif" style = "cursor:pointer" name = "imgaceptar" id="imgaceptar"  onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" onclick="avalar();">

                            <%}%> -->

                            <%  if ((vista.equals("7")||vista.equals("10")) && (estadoneg.equals("ACEPTADO") || estadoneg.equals("PRE_ACEPTADO")) && ( negocio.getActividad().equals("REF") || ((tipoconv.equals("Libranza") || tipoconv.equals("Microcredito"))&& negocio.getActividad().equals("RAD")))) {%>
                            <img alt="" src = "<%=BASEURL%>/images/botones/concepto.gif" style="cursor:pointer;" onClick="concepto('<%=BASEURL%>','<%=negocio.getCod_negocio()%>', '<%=form%>','ANA', '<%= negocio.getTotpagado() / negocio.getNodocs()%>', '<%=tipoconv%>');" name= "imgconcepto" id="imgconcepto"  onMouseOver="botonOver(this);" onMouseOut="botonOut(this);">
                            <%}%>

                            <%  if (vista.equals("8") && (estadoneg.equals("ACEPTADO") || estadoneg.equals("PRE_ACEPTADO")) && (negocio.getActividad().equals("ANA") || negocio.getActividad().equals("REF"))) {%>
                            <img alt="" src = "<%=BASEURL%>/images/botones/concepto.gif" style="cursor:pointer;" onClick="concepto('<%=BASEURL%>','<%=negocio.getCod_negocio()%>', '<%=form%>','DEC','<%= negocio.getTotpagado() / negocio.getNodocs()%>', '<%=tipoconv%>');" name= "imgconcepto" id="imgconcepto"  onMouseOver="botonOver(this);" onMouseOut="botonOut(this);">
                            <%}%>
                            <%  if (vista.equals("9") && estadoneg.equals("AVALADO")) {%>
                            <img alt="" src = "<%=BASEURL%>/images/botones/concepto.gif" style="cursor:pointer;" onClick="concepto('<%=BASEURL%>','<%=negocio.getCod_negocio()%>', '<%=form%>','FOR','0', '<%=tipoconv%>');" name= "imgconcepto" id="imgconcepto"  onMouseOver="botonOver(this);" onMouseOut="botonOut(this);">
                            <%}%>
                            <%  if (vista.equals("15") && (estadoneg.equals("RECHAZADOS"))) {%>
                            <img alt="" src = "<%=BASEURL%>/images/botones/concepto.gif" style="cursor:pointer;" onClick="seleccionar_convenio('<%=BASEURL%>','<%=negocio.getCod_negocio()%>','<%=tipoconv%>');" name= "imgconcepto" id="imgconcepto"  onMouseOver="botonOver(this);" onMouseOut="botonOut(this);">
                            <%}%>
                            <%  if (vista.equals("16") && (estadoneg.equals("RECHAZADOS"))) {%>
                            <img alt="" src = "<%=BASEURL%>/images/botones/concepto.gif" style="cursor:pointer;" onClick="reactivar_negocio('<%=negocio.getCod_negocio()%>','<%=form%>');" name= "imgconcepto" id="imgconcepto"  onMouseOver="botonOver(this);" onMouseOut="botonOut(this);">
                            <%}%>
                            <%  if (vista.equals("17") && estadoneg.equals("AVALADO")) {%>
                            <img alt="" src = "<%=BASEURL%>/images/botones/concepto.gif" style="cursor:pointer;" onClick="concepto('<%=BASEURL%>','<%=negocio.getCod_negocio()%>', '<%=form%>','PERF','0', '<%=tipoconv%>');" name= "imgconcepto" id="imgconcepto"  onMouseOver="botonOver(this);" onMouseOut="botonOut(this);">
                            <%}%>


                        </td>
                    </tr>
                    <%if (loggedUser.getTipo().equals("CLIENTETSP") || ((vista.equals("3") || (vista.equals("4"))) && (estadoneg.equals("ACEPTADO") || estadoneg.equals("PRE_ACEPTADO")))) {%>
                    <tr>
                        <td colspan="2" align="center">
                            <p>
                                <img src="<%=BASEURL%>/images/botones/salir.gif"  name="salir" style="cursor:hand" title="Salir al Menu Principal" onMouseOver="botonOver(this);" onClick="parent.close();" onMouseOut="botonOut(this);"                                 <% if(tipoconv.equals("Consumo") || tipoconv.equals("Microcredito")){
                                                                              
                                        //Preguntar si el negocio es un aval o no. 
                                         //  if(!model.Negociossvc.isNegocioAval(negocio.getCod_negocio())){%>

                               <img  src = "<%=BASEURL%>/images/botones/plan_pagos.gif" alt="" name = "regresar" hidden style = "cursor:hand" onclick = "Exportar();"><br/>
                                <img  src = "<%=BASEURL%>/images/botones/plan_pagos_nuevo.gif" alt="" name = "plannuevo" style = "cursor:hand" onclick = "Exportarplandepagofintra();">
                              
                                                                                           

                                <%}%>
                            </p>
                        </td>
                    </tr>
                    <%} else {%>
                    <tr>
                        <td colspan="2" align="center">
                            <p>
                                <img src="<%=BASEURL%>/images/botones/salir.gif"  name="regresar" style="cursor:hand" title="Salir al Menu Principal" onMouseOver="botonOver(this);" onClick="parent.close();" onMouseOut="botonOut(this);" >
                                <img  src = "<%=BASEURL%>/images/botones/regresar.gif" name = "regresar" style = "cursor:hand" onclick = "history.back(1)" onmouseover = "botonOver(this);" onmouseout = "botonOut(this);">

                                <% if(tipoconv.equals("Consumo") && redescuento && convenio.isAval_anombre()){
                                          
                                             //Preguntamos si el negocio es el aval del negocio
                                             if(!model.Negociossvc.isNegocioAval(negocio.getCod_negocio())){%>


                                             <img  src = "<%=BASEURL%>/images/botones/plan_pagos.gif" alt="" name = "regresar" hidden style = "cursor:hand" onclick = "Exportar();"><br>
                                <img  src = "<%=BASEURL%>/images/botones/plan_pagos_nuevo.gif" alt="" name = "plannuevo" style = "cursor:hand" onclick = "Exportarplandepagofintra();">
                             

                                <% }%>

                                <%}%>
                            </p>
                        </td>
                    </tr>
                    <%}%>
                </table>                  

            </div>
        </div>
        <div id="divSalidaConvenios" title="Convenios" style=" display: none"  class="ui-dialog"></div>
        <div id="divSalida" title="Plan De Pagos" style=" display: none"  class="ui-dialog"></div>              
    </body>
</html>