<%-- 
    Document   : filtrocuponesmicro
    Created on : 07-may-2013, 15:30:34
    Author     : Egonzalez
--%>
<%@page session="true"%>
<%@page import="java.util.*"%>
<%@page import="com.tsp.operation.model.*"%>
<%@page import="com.tsp.operation.model.beans.*"%>
<%@page import="com.tsp.operation.model.services.*"%>
<%@page contentType="text/html" pageEncoding="ISO-8859-1"%>
<%
    response.addHeader("Cache-Control", "no-cache");
    response.addHeader("Pragma", "No-cache");
    response.addDateHeader("Expires", 0);
    String BASEURL = request.getContextPath();
    String CONTROLLER = BASEURL + "/controller";
%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
        <link href="<%= BASEURL %>/css/estilostsp.css" rel='stylesheet'>
        <script type='text/javascript' src="<%= BASEURL %>/js/boton.js"></script>
        <script type='text/javascript' src="<%= BASEURL %>/js/prototype.js"></script>
        <title>JSP Page</title>
        <script type="text/javascript">

            function soloNumeros(id) {
                var valor = $(id).value;
                valor =  valor.replace(/[^0-9^.]+/gi,"");
                $(id).value = valor;
            }
            
            function busqueda(id_field){
                var dato = $(id_field).value;
                if(dato==''){
                    alert('Para consultar debe escribir algo');
                }
                else{
                   
                    document.getElementById("formulario").submit();
                }
            }
            
           
        </script>
    </head>
    <body>
        <div id="capaSuperior" style="position:absolute; width:100%; height:100px; z-index:0; left: 0px; top: 0px;">
            <jsp:include page="/toptsp.jsp?encabezado=Generacion Recibos De Pago"/>
        </div>
        
        <div id="capaCentral" style="position:absolute; width:100%; height:83%; z-index:0; left: 0px; top: 130px; overflow:scroll;">
            <form id="formulario" name="formulario" action="<%=CONTROLLER%>?estado=Negocios&accion=Ver&op=5&vista=12" method="post">
                <table align="center" width="520" border="1">
                    <tbody>
                        <tr>
                            <td>
                                <table id="tbusqueda" border="0" style="border-collapse: collapse; width: 100%;">
                                    <tr class="fila">
                                        <td class="subtitulo1">
                                            Periodo Trasnferencia
                                        </td>
                                        <td>
                                            <input type="text" id="periodo" name="periodo" value="" maxlength="6" size="16" onkeyup="soloNumeros(this.id);">
                                            
                                        </td>
                                    </tr>
                                      <tr class="fila">
                                        <td colspan="2" align="center">
                                        </td>
                                    </tr>
                                      <tr class="fila">
                                        <td colspan="2" align="center">
                                       </td>
                                    </tr>
                                    <tr class="fila">
                                        <td colspan="2" align="center">
                                             <img alt="" src = "<%=BASEURL%>/images/botones/aceptar.gif" style = "cursor:pointer" name = "imgaceptar" onclick = " busqueda('periodo');" onmouseover="botonOver(this);" onmouseout="botonOut(this);">
                                            <img alt="" src = "<%=BASEURL%>/images/botones/salir.gif" style = "cursor:pointer" name = "imgsalir" onclick = "window.close();" onmouseover="botonOver(this);" onmouseout="botonOut(this);">
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                    </tbody>
                </table>
            </form>
        </div>
    </body>
</html>
