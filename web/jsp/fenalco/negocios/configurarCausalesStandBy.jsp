<%-- 
    Document   : configurarCausalesStandBy
    Created on : 6/05/2016, 09:38:34 AM
    Author     : user
--%>

<%@page contentType="text/html" pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
        <title>Configuracion Causales StandBy</title>
        <link type="text/css" rel="stylesheet" href="./css/jquery/jquery-ui/jquery-ui-2.css"/>
        <link type="text/css" rel="stylesheet" href="./css/jquery/jquery.jqGrid/ui.jqgrid.css"/>        
        <script type="text/javascript" src="./js/jquery/jquery-ui/jquery.min.js"></script>
        <script type="text/javascript" src="./js/jquery/jquery-ui/jquery.ui.min.js"></script>            
        <script type="text/javascript" src="./js/jquery/jquery.jqGrid/js/grid.locale-es.js"></script>
        <script type="text/javascript" src="./js/jquery/jquery.jqGrid/js/jquery.jqGrid.min.js"></script>
        <link type="text/css" rel="stylesheet" href="./css/buttons/botonVerde.css"/>
        <link type="text/css" rel="stylesheet" href="./css/popup.css"/>
        <script type="text/javascript" src="./js/StandBy.js"></script>  

        <!--jqgrid--> 
        <script type="text/javascript" src="./js/jquery/jquery.jqGrid-4.7.0/js/grid.locale-es.js"></script>
        <script type="text/javascript" src="./js/jquery/jquery.jqGrid-4.7.0/js/jquery.jqGrid.min.js"></script>
        <script type="text/javascript" src="./js/jquery/jquery.jqGrid-4.7.0/plugins/jquery.contextmenu.js"></script>
        <script type="text/javascript" src="./js/jquery/jquery.jqGrid-4.7.0/js/jquery.tabletojson.js"></script>  
    </head>
    <body>
        <div id="capaSuperior" style="position:absolute; width:100%; height:100px; z-index:0; left: 0px; top: 0px;">
            <jsp:include page="/toptsp.jsp?encabezado=Configuracion Causales StandBy"/>
        </div>
        <div id="capaCentral" style="position:absolute; width:100%; height:100%; z-index:0; left: 0px; top: 160px; ">
            <br>
            <center>
                <table id="tabla_causales_standby"></table>
                <div id="page_tabla_causales_standby"></div>
            </center>
            <!-- Dialogo ventana agregar causal StandBy> -->
            <div id="div_causales"  style="display: none; width: 800px" >   
                    </br>
                    <table aling="center" style=" width: 100%" >
                        <tr>                      
                        <td style="width: 10%"><span>Descripcion</span></td>                          
                        <td style="width: 90%"><input type="text" id="descripcion" name="descripcion" style=" width: 450px;color:#000"></td>  
                        </tr>                       
                        <tr>
                            <td style="width: 10%"><span>Plazo</span></td>                          
                            <td style="width: 90%"><input type="text" id="plazo" name="plazo" class="solo-numero" style=" width: 90px;color:#000" ></td>                      
                        </tr>
                    </table>  
                </br> 
            </div>
            <div id="dialogMsj" title="Mensaje" style="display:none;">
                <p style="font-size: 12.5px;text-align:justify;" id="msj" > Texto </p>
            </div>  
        </div>
        <script type="text/javascript">
           listarCausalesStandBy();
        </script>
    </body>
</html>
