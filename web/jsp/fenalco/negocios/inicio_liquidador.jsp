<!--
- Autor : Ing. Julio Ernesto Barros Rueda
- Date  : 14 de Mayo de 2007
- Copyrigth Notice : Fintravalores S.A. S.A
-->
<%--
-@(#)
--Descripcion : Pagina JSP, que maneja el ingreso de identidades

--%>

<%-- Declaracion de librerias--%>
<%@page contentType="text/html;"%>
<%@page session="true"%>
<%@page import="java.util.*"%>
<%@page import="com.tsp.operation.model.*"%> 
<%@page import="com.tsp.operation.model.beans.*"%>
<%@page import="com.tsp.util.*"%>
<%@page import="com.tsp.util.Util.*"%>
<%@include file="/WEB-INF/InitModel.jsp"%>
<%@page import="com.tsp.operation.model.*"%>

<html>
<head>
<title>Liquidador</title>

<link href="../../../css/estilostsp.css" rel="stylesheet" type="text/css"> 
<link href="<%=BASEURL%>/css/estilostsp.css" rel="stylesheet" type="text/css"> 

<style type="text/css">
<!--
.style1 {font-size: 9px}
.style2 {
	font-size: 10px;
	color: #666666;
}
.style3 {color: #666666}
-->
</style>
</head>

<script type='text/javascript' src="<%=BASEURL%>/js/Validacion.js"></script>
<script type='text/javascript' src="<%= BASEURL %>/js/boton.js"></script>
<script type='text/javascript' src="<%= BASEURL %>/js/general.js"></script>
<script src='<%=BASEURL%>/js/date-picker.js'></script>
<script src="<%= BASEURL %>/js/transferencias.js"></script>

<%  Usuario usuario            = (Usuario) session.getAttribute("Usuario");
	Propietario  Propietario  = new Propietario();
	String Retorno   = "";
	String Captacion = "";
	String Reembolso = "";
	String  vista    = "";
	String  Nit      =" ";
	model.FenalcoFintraSvc.loadClientes(usuario.getCedula());
    Vector clientes =model.FenalcoFintraSvc.getClientes();

%>

<script>
function abrirVentanaBusq( an, al, url, pag ) {
			
        		parent.open( url, 'Conductor', 'width=' + an + ', height=' + al + ',scrollbars=no,resizable=no,top=10,left=65,status=yes' );
				
    		}
	   function enviarFormulario(CONTROLLER,frm){
				var nche = parseInt (formulario.numero_cheques.value);
				if ( formulario.nit.value == '' ){
					alert( 'Debe escojer el cliente para continuar...' );
					return false;
				}
				if (  formulario.numero_cheques.value == '' ){
					alert( 'Digite la cantidad de documentos55...' );
					formulario.numero_cheques.focus();
					return false;
				}
				if ( formulario.valor_desembolso.value == '' ){
					alert( 'Debe digitar el desembolso para continuar...' );
					formulario.valor_desembolso.focus();
					return false;
				}
				if ( formulario.tipo_negocio.value == '' ){
					alert( 'Debe escoger el tipo de negocio para continuar...' );
					return false;
				}
				if ( formulario.forma_pago.value == '' ){
					alert( 'Debe escoger la forma de pago para continuar...' );
					return false;
				}
				if ( formulario.remesa.value == '' ){
					alert( 'Debe escoger la modalidad de remesa para continuar...' );
					return false;
				}
				if( (nche<=0) || (nche>60) )
				{
					alert( 'Digite una cantidad valida de documentos debe estar entre 1 y 60...' );
					formulario.numero_cheques.focus();
					return false;
				}
				document.imgaceptar.src='<%=BASEURL%>/images/botones/aceptarDisable.gif';
				document.imgaceptar.onmouseover = new Function('');
				document.imgaceptar.onmouseout  = new Function('');
				document.imgaceptar.onclick     = new Function('');
				frm.submit();
	   }

	</script>




<body   >
<div id="capaSuperior" style="position:absolute; width:100%; height:100px; z-index:0; left: 0px; top: 0px;">
	<jsp:include page="/toptsp.jsp?encabezado=LIQUIDADOR MAESTRO"/>
</div>
<div id="capaCentral" style="position:absolute; width:100%; height:87%; z-index:0; left: 0px; top: 100px; overflow: scroll;">

<FORM action='<%=CONTROLLER%>?estado=Fenalco&accion=Fintra&opcion=EjecutarOperacion&custodia="0"&aval="0"&remesas="0"' name='formulario' method="post" >
  
	<table width="432" height="167" border="2"align="center">
		
		<tr>
		  <td width="420" height="159">
		  <table width="100%" height="72%" class="tablaInferior" >
		  	<tr class="fila">
			  <td colspan="2" align="left" nowrap class="subtitulo">&nbsp;Liquidador Maestro </td>
			  <td colspan="2" align="left" nowrap class="bordereporte"><img src="<%=BASEURL%>/images/fintra.gif" width="166" height="28" class="bordereporte"> </td>
			</tr>
		  
			<tr class="fila">
			  <td colspan="2" >Escoja el Cliente </td>
			  <img src='<%=BASEURL%>/images/botones/iconos/lupa.gif' width="15" height="15" onClick="window.open('<%=BASEURL%>/jsp/fenalco/liquidadores/consultasClientes.jsp','','HEIGHT=200,WIDTH=600,SCROLLBARS=YES,RESIZABLE=YES,STATUS=NO')" title="Buscar" style="cursor:hand" > 
			  <a href="<%=BASEURL%>/jsp/fenalco/clientes/insert_clientes.jsp">Nuevo Cliente</a>		
			</tr>
			<tr class="fila">
			  <td colspan="2" > Nro de Cheques o Letras</td>
			  <td width="49%" valign="middle"><input name="numero_cheques" type="text" class="textbox" value='' maxlength="2" onKeyPress="soloDigitos(event,'decNO')">
			    <span class="style2">			  Ej: 10 </span>
              </td>
		    </tr>
			<tr class="fila">
			  <td colspan="2" >Valor del Negocio </td>
			  <td valign="middle"> <span class="style3">
			    <input name="valor_desembolso" type="text" class="textbox" onKeyPress="soloDigitos(event,'decNO')" value=''  maxlength="20"> 
		      <span class="style1">Ej:1000000</span></span></td>
			</tr>
			<tr class="fila">
			  <td colspan="2" >Fecha del Negocio </td>
			  <td valign="middle">
			  <input name='fechainicio' type='text' class="textbox" id="fechainicio" style='width:120' value='<%=com.tsp.util.Util.AnoActual()+"-"+com.tsp.util.Util.getFechaActual_String(3)+"-"+com.tsp.util.Util.getFechaActual_String(5)%>' readonly>
							  <a href="javascript:void(0);" class="link" onFocus="if(self.gfPop)gfPop.fPopCalendar(document.formulario.fechainicio);return false;"  HIDEFOCUS > <img src="<%=BASEURL%>\js\Calendario\cal.gif" width="16" height="16"
								border="0" alt="De click aqu&iacute; para ver el calendario."></a> <img src="<%= BASEURL%>/images/botones/iconos/obligatorio.gif" width="10" height="10">
			  </td> 
		    </tr>
			<tr class="fila">
			  <td colspan="2" >Tipo de Documento </td>
			  <td class="fila"> 
                     <select name="tipo_negocio" class="listmenu" onChange="">
						<option value="" selected> - - </option>
						<option value="01">CHEQUE</option>
						<option value="02">LETRA</option>
			    </select></span>
		      </td>
			</tr>
			<tr class="fila">
			  <td colspan="2" >Plazo 1er Cheque </td>
			  <td valign="middle">
			  		 <select name="forma_pago" class="listmenu" onChange="">
						<option value="" selected> - - </option>
						<option value="30">A 30 DIAS</option>
						<option value="45">A 45 DIAS</option>
			    </select></span>
			  </td>
			</tr>
			<tr class="fila">
			  <td colspan="2" >Genera Remesa </td>
			  <td valign="middle">
			  		 <select name="remesa" class="listmenu" onChange="">
						<option value="" selected> - - </option>
						<option value="1">SIN REMESA</option>
						<option value="0">CON REMESA</option>
			    </select></span>
			  </td>
			</tr>
		  </table>	    
		  </td>
		</tr>
	</table>
  <p><br>
    </p>
  <div align="center">
	<img src="<%=BASEURL%>/images/botones/aceptar.gif"  name="imgaceptar" onClick="javascript:enviarFormulario('<%=CONTROLLER%>',formulario);" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" style="cursor:hand">&nbsp;       &nbsp;
    <img src="<%=BASEURL%>/images/botones/salir.gif"  name="imgsalir"  onMouseOver="botonOver(this);" onClick="parent.close();" onMouseOut="botonOut(this);" style="cursor:hand">
   </div>

  
  
  </p>
  <p>
<%if( Retorno != null ){%>  
  <table border="2" align="center">
  <tr>
    <td><table width="100%"  border="1" align="center"  bordercolor="#F7F5F4" bgcolor="#FFFFFF">
      <tr>
        <td width="229" align="center" class="mensajes"><%=Retorno%></td>
        <td width="29" background="<%=BASEURL%>/images/cuadronaranja.JPG">&nbsp;</td>
        <td width="58">&nbsp;</td>
      </tr>
    </table></td>
  </tr>
</table>
<%}%>

</p>
 <br>
</form>
</div>

<iframe width="188" height="166" name="gToday:normal:agenda.js:gfPop:plugins.js" id="gToday:normal:agenda.js:gfPop:plugins_24.js" src="<%=BASEURL%>/js/Calendario/ipopeng.htm" scrolling="yes" frameborder="0" style="visibility:visible; z-index:999; position:absolute; left:-500px; top:0px;">
</iframe>
</body>
</html>


