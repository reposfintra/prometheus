<!--
- Autor : Ing. Iris Vargas
- Date  : 11 Mayo 2012
- Copyrigth Notice : Geotech S.A
-->
<%--
-@(#)
--Descripcion : Pagina JSP, realiza de forma manual el proceso de descargue de titulos valores con tiempo para indemnizar vencido
--%>

<%-- Declaracion de librerias--%>
<%@page contentType="text/html"%>
<%@ page session="true"%>
<%@ page errorPage="../error/error.jsp"%>
<%@page import="com.tsp.operation.model.beans.*, java.util.*"%>
<%@ include file="/WEB-INF/InitModel.jsp"%>
<%@page import="com.tsp.operation.model.*"%>
<%@page import="com.tsp.finanzas.contab.model.*"%>
<%@page import="com.tsp.util.*"%>
<%@ taglib uri="http://jakarta.apache.org/taglibs/input-0.90" prefix="input" %>
<html>
<head><title>Descarga de titulos vencidos para indemnizacion</title>
<%String path = application.getRealPath(model.menuService.getRealUrl(request.getRequestURI()));
  String datos[] = model.menuService.getContenidoMenu(BASEURL,request.getRequestURI(),path);
  String ayer  =  Utility.convertirFecha( Utility.getAyer("-"), 0);
%>

<link href="<%= BASEURL %>/css/estilostsp.css" rel="stylesheet" type="text/css">
<script type='text/javascript' src="<%= BASEURL %>/js/boton.js"></script>
<script type='text/javascript' src="<%= BASEURL %>/js/general.js"></script>
</head>
<body onLoad="redimensionar();doFieldFocus( form2 );" onResize="redimensionar();">
<div id="capaSuperior" style="position:absolute; width:100%; height:100px; z-index:0; left: 0px; top: 0px;">
<jsp:include page="/WEB-INF/toptsp.jsp?encabezado=Descarga de titulos vencidos para indemnizacion"/>
</div>

<div id="capaCentral" style="position:absolute; width:100%; height:85%; z-index:0; left: 0px; top: 100px; overflow: scroll;"> 
<form name="form2" method="post" action="<%=CONTROLLER%>?estado=Descarga&accion=TitulosIndem">
    <table width="380" border="2" align="center">
      <tr>
        <td>
		<table width="100%" align="center"  class="tablaInferior">
			 
			<tr class="fila">
			  <td  >Fecha</td>
			  <td valign="middle">
			  <input name='fecha' type='text' class="textbox" id="fechaCorte" style='width:120' value='<%=ayer%>' readonly>
                          <a href="javascript:void(0);" onFocus="if(self.gfPop)gfPop.fPopCalendar(document.form2.fecha);" hidefocus><img alt="calendario" src="<%=BASEURL%>/images/cal.gif" width="16" height="16" align="absmiddle"></a>
							  <img src="<%= BASEURL%>/images/botones/iconos/obligatorio.gif" width="10" height="10">
			  </td> 
		    </tr>
        </table>
		</td>
      </tr>
    </table>


<%
String Retorno=(String)request.getParameter("msg");
if( Retorno != null ){%>  
  <table border="2" align="center">
  <tr>
    <td><table width="100%"  border="1" align="center"  bordercolor="#F7F5F4" bgcolor="#FFFFFF">
      <tr>
        <td width="229" align="center" class="mensajes"><%=Retorno%></td>
        <td width="29" background="<%=BASEURL%>/images/cuadronaranja.JPG">&nbsp;</td>
        <td width="58">&nbsp;</td>
      </tr>
    </table></td>
  </tr>
</table>
<%}%>
<br/>
<div align="center">
    <%if( (String)request.getParameter("msg") == null ){%>  
			<img src="<%=BASEURL%>/images/botones/aceptar.gif" style="cursor:hand" title="Aceptar" name="buscar"  onClick="this.style.visibility='hidden';form2.submit();" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);">
		<%}%>
    <img src="<%=BASEURL%>/images/botones/salir.gif"  name="regresar" style="cursor:hand" title="Salir al Menu Principal" onMouseOver="botonOver(this);" onClick="parent.close();" onMouseOut="botonOut(this);" ></div>
	
</form>
</div>
<%=datos[1]%>
 <iframe width=188 height=166 name="gToday:normal:agenda.js:gfPop:plugins.js" id="gToday:normal:agenda.js:gfPop:plugins.js" src="<%=BASEURL%>/js/Calendario/ipopeng.htm" scrolling="no" frameborder="0" style="visibility:visible; z-index:999; position:absolute; left:-500px; top:0px;"> </iframe>
     
</body>
</html>
