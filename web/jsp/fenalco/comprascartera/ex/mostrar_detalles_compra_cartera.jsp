<!--
- Autor : Ing. nAVi
- Date  : 14 de Mayo de 2007
- Copyrigth Notice : Fintravalores S.A. S.A

Descripcion : Pagina JSP, que maneja el ingreso de identidades
-->

<%-- Declaracion de librerias--%>
<%@page contentType="text/html;"%>
<%@page session="true"%>
<%@page import="java.util.*"%>
<%@page import="com.tsp.operation.model.*"%> 
<%@page import="com.tsp.operation.model.beans.*"%>
<%@page import="com.tsp.util.*"%>
<%@page import="com.tsp.util.Util.*"%>
<%@include file="/WEB-INF/InitModel.jsp"%>
<%@page import="com.tsp.operation.model.*"%>
<%@taglib uri="http://jakarta.apache.org/taglibs/input-0.90" prefix="input" %>

<html>
    <head>
        <title>Consulta de Liquidador de Cartera</title>
		
		<%TreeMap pro= model.Negociossvc.GetBcos();%>
		<%pro.put("LETRAS", "99");%>
		
        <link href="../../../css/estilostsp.css" rel="stylesheet" type="text/css"> 
        <link href="<%=BASEURL%>/css/estilostsp.css" rel="stylesheet" type="text/css">
    </head>
    
    <script type='text/javascript' src="<%=BASEURL%>/js/Validacion.js"></script>
    <script type='text/javascript' src="<%= BASEURL %>/js/boton.js"></script>
    <script type='text/javascript' src="<%= BASEURL %>/js/general.js"></script>
    <script src='<%=BASEURL%>/js/tools.js' language='javascript'></script>
    <script src='<%=BASEURL%>/js/date-picker.js'></script>
    <script src="<%= BASEURL %>/js/transferencias.js"></script>
    
    <%
    Usuario usuario            = (Usuario) session.getAttribute("Usuario");
	Usuario loggedUser = (Usuario) session.getAttribute("Usuario");
    String item=request.getParameter("item");
    String codigo_negocio=request.getParameter("codigo_negocio");
	String cliente=request.getParameter("nit");
	String proveedor=request.getParameter("proveedor");
	String estado=request.getParameter("est");
	String fven[];
	double fval[];
	double mat[];
	String finte[];
	int mes,dia,ano;
	ArrayList detalle=null;
	detalle=model.creacionCompraCarteraSvc.obtenerCheques(codigo_negocio);
	//System.out.println("en jsp con detalle");
	try{
	model.creacionCompraCarteraSvc.setSwImprimir(false);
	
    %>
    
    <script>
		function validar()
		{
			form1.submit();
		}
        function enviarFormularioParaAgregar(CONTROLLER,frm){	
            frm.action='<%=CONTROLLER%>?estado=creacion&accion=CompraCartera&opcion=AgregarCheque';
            frm.submit();
        }
        
        function enviarFormularioX(CONTROLLER,frm){	
            frm.action='<%=CONTROLLER%>?estado=creacion&accion=CompraCartera&opcion=AceptarConjuntoDcheques';
            frm.submit();
        }    
        
        function cancelarCompraCartera(frm){
            frm.action='<%=CONTROLLER%>?estado=creacion&accion=CompraCartera&opcion=cancelarCompraCartera';
            frm.submit();
        }
		function llenar(a)
		{
			var cont=0;
			var lonc=a.length;
			var lonp=a.length;
			var camb=0;
			var sw=0;
			var mid=2;
			mid=a.indexOf('-');
			tempb=a.substring(mid+1,lonc);
			tempf=a.substring(0,mid+1);
			
			if ( tempb.indexOf('000000') == 0){
				tempb=a.substring(mid+7,lonc)
				tempf=a.substring(0,mid+7);
			}
			if ( tempb.indexOf('00000') == 0){
				tempb=a.substring(mid+6,lonc)
				tempf=a.substring(0,mid+6);
			}
			if ( tempb.indexOf('0000') == 0){
				tempb=a.substring(mid+5,lonc)
				tempf=a.substring(0,mid+5);
			}
			if ( tempb.indexOf('000') == 0){
				tempb=a.substring(mid+4,lonc)
				tempf=a.substring(0,mid+4);
			}
			if ( tempb.indexOf('00') == 0){
				tempb=a.substring(mid+3,lonc)
				tempf=a.substring(0,mid+3);
			}
			if ( tempb.indexOf('0') == 0){
				tempb=a.substring(mid+2,lonc)
				tempf=a.substring(0,mid+2);
			}
			a=parseInt(tempb);
			for(var i=1;i<form1.length;i++){
				var ele = form1.elements[i];
				if(ele.type=='text' && ele.id=='do' ){
					var aux=a+i;
					ele.value=tempf+aux;
				}
			}
		}
		
		function imprimirDet(frm){
			frm.action='<%=CONTROLLER%>?estado=creacion&accion=CompraCartera&opcion=imprimirDetalle&codigo_negocio=<%=codigo_negocio%>';
            frm.submit();
		}
		
		imprimirx();
		
		function imprimirx(){
			<%
			String imprime =request.getParameter("imprimir");
			if (detalle!=null && detalle.size()>0 && imprime!=null && imprime.equals("si")) {
//			&& model.creacionCompraCarteraSvc.getSwImprimir()==true
				//model.creacionCompraCarteraSvc.imprimirDetalleCompraCartera(detalle);
				String pagina=model.creacionCompraCarteraSvc.getArchivoPdf();
				%>//alert("pag:<%=pagina%>");<%
				//<%=pagina%>
				window.open("<%=BASEURL%>/<%=pagina%>",'windowx','');<%
			}else{
				%>//alert("no hay datos.");<%
			}
			%>
			//alert("espere");
        }
		
		
        
    </script>
    
    <body ><!--onLoad="javascript:formulario.textbox.focus"-->
        
        <div id="capaSuperior" style="position:absolute; width:100%; height:100px; z-index:0; left: 0px; top: 0px;">
            <jsp:include page="/toptsp.jsp?encabezado=CONSULTA DE LIQUIDADOR DE CARTERA"/>
        </div>
        <div id="capaCentral" style="position:absolute; width:100%; height:87%; z-index:0; left: 0px; top: 100px; overflow: scroll;">
             <%
	     	 
                //detalle=model.creacionCompraCarteraSvc.obtenerCheques(codigo_negocio);
                fven=new String [detalle.size()+1];
				fval=new double [detalle.size()+1];
				mat=new double [50];
				finte=new String[50];
				int d=3;//cantidad de decimales
                %>
                <%
                if (detalle!=null && detalle.size()>0) {
                %>
            <form name="form1" method="post" action="<%=CONTROLLER%>?estado=Negocios&accion=Update&codneg=<%=((chequeCartera)detalle.get(0)).getCodigoNegocio()%>&op=12">
               
                <br>
                <table id="detalle" width="400" >
                    <tr class="filaazul" style="cursor:default" bordercolor="#D1DCEB" align="center">
                        <td align="center">
                            <strong>C�digo de negocio
                        </td>
                        <td align="center">
                            <%=((chequeCartera)detalle.get(0)).getCodigoNegocio()%>
                        </td>
                    </tr>
                    <tr class="filagris" style="cursor:default" bordercolor="#D1DCEB" align="center">
                        <td align="center">
                            <strong>Fecha de desembolso</strong>
                        </td>
                        <td align="center">
                          <%=((chequeCartera)detalle.get(0)).getFechaDesembolso().substring(0,10)%> </td>
					</tr>
                    <tr class="filaazul" style="cursor:default" bordercolor="#D1DCEB" align="center">
                        <td align="center">
                            <strong>Afiliado
                        </td>
                        <td align="center">
                            <%=((chequeCartera)detalle.get(0)).getNombreProveedor()%>
                        </td>
                    </tr>
                    <tr class="filagris" style="cursor:default" bordercolor="#D1DCEB" align="center">
                        <td align="center">
                            <strong>Cliente
                        </td>
                        <td align="center">
                            <%= ((chequeCartera)detalle.get(0)).getNombreNit()%>
                        </td>
                    </tr>
                    <tr class="filaazul" style="cursor:default" bordercolor="#D1DCEB" align="center">
                        <td align="center">
                            <strong>Modalidad de remesa
                        </td>
                        <td align="center">
                            <%
                            String modremes=  ((chequeCartera)detalle.get(0)).getModRemesa();
							String modavalx=  ((chequeCartera)detalle.get(0)).getModAval();
							
							
                            if (modremes.equals("0")){
							    if (modavalx.equals("0")){
                                	%>Establecimiento<%
								}
                                if (modavalx.equals("1")){
                                	%>Cliente<%
								}								
                            }
                            
                            if (modremes.equals("1")){
                                %>Sin remesa<%
                            }
                            %>
                        </td>
                    </tr>
                    <tr class="filagris" style="cursor:default" bordercolor="#D1DCEB" align="center">
                        <td align="center">
                            <strong>Modalidad de custodia
                        </td>
                        <td align="center">
                            <%
                            String modcustodi=  ((chequeCartera)detalle.get(0)).getModCustodia();
                            if (modcustodi.equals("1")){
                                %>Cliente<%
                            }
                            if (modcustodi.equals("0")){
                                %>Establecimiento<%
                            }
                            
                            %>
                            <%//=((chequeCartera)detalle.get(0)).getModCustodia()%>
                        </td>
                      <tr class="filaazul" style="cursor:default" bordercolor="#D1DCEB" align="center">
                        <td align="center">
                            <strong>Cantidad de docs
                        </td>
                        <td align="center">
                            <%=detalle.size()%>
                            <%//=((chequeCartera)detalle.get(0)).getModCustodia()%>
                        </td>
						
                      </tr>
					  <tr class="filaazul" style="cursor:default" bordercolor="#D1DCEB" align="center">
                        <td align="center">
                            <strong>Tipo de documento
                        </td>
                        <td align="center">
                            <%
							String TipoDoc=	((chequeCartera)detalle.get(0)).getTipoDoc();
							if (TipoDoc.equals("01")){
								TipoDoc="Cheque";
							}
							if (TipoDoc.equals("02")){
								TipoDoc="Letra";
							}
							out.print(""+TipoDoc);
							%>
                            <%//=((chequeCartera)detalle.get(0)).getModCustodia()%>
                        </td>
						
                      </tr>
					  
                    </tr>
                </table>
                <%}%>
                <br>
                
                <table id="detalle" width="100%" >
                    <tr  id="fila1" class="tblTitulo">
                        <td align="center">Item  </td>
                        <td align="center">Fecha de documento </td>
                        <td align="center">Fecha de consignaci�n</td>                                   
                        <td align="center">&nbsp;Valor&nbsp;</td>
                        <td align="center">&nbsp;Porte&nbsp;</td>
                        <td align="center">&nbsp;D�as&nbsp;</td>
                        <td align="center">&nbsp;Factor&nbsp;</td>
                        <td align="center">&nbsp;Remesa&nbsp;</td>                        
                        <td align="center">&nbsp;Descuento&nbsp;</td>
                        <td align="center">&nbsp;Valor sin custodia&nbsp;</td>
                        <td align="center">&nbsp;Custodia&nbsp;</td>
                        <td align="center">&nbsp;Valor sin custodia - Custodia&nbsp;</td>
                        <td align="center">&nbsp;Remesa+Porte&nbsp;</td>
                        <td align="center">&nbsp;Valor a girar&nbsp;</td>
						<%if ( estado!=null && estado.equals("ACEPTADO") ) {%> 
						<td align="center">No Cheque</td>
                        <%}%>
                    </tr>				
                    <%
                    double total_valor_cheque=0.0;
                    double total_descuento=0.0;
                    double total_valor_cheque_sincustodia=0.0;
                    double total_custodia=0.0;
                    double total_valor_sincustodia_custodia=0.0;
                    double total_remesamasporte=0.0;
                    double total_valorgirable=0.0;
                    double total_remesa=0.0;
                    double total_porte=0.0;
                                           
                    chequeCartera chc ;
                    int i = 0;
					int sum1=0;
					//System.out.println("antes de for detalle"+detalle);
                    for( i = 0; i < detalle.size(); i++ ){
                        chc=(chequeCartera)detalle.get(i);
                        
                    %>	
                    <tr class="<%= (i%2==0)? "filagris" : "filaazul" %>" style="cursor:default" bordercolor="#D1DCEB" align="center">                            
                        <td align="center" ><%=chc.getItem()%></td>
                        <td align="center"   ><%=chc.getFechaCheque().substring(0,10)%></td>
                        <td align="center" ><%=chc.getFechaConsignacion().substring(0,10)%></td>
						<%fven[i]=chc.getFechaConsignacion().substring(0,10);%>
                        <td align="center" ><%=Util.customFormat(Util.roundByDecimal(Double.parseDouble(chc.getValor()),d))%></td>
                        <%fval[i]=Util.roundByDecimal(Double.parseDouble(chc.getValor()),d);
						total_valor_cheque=total_valor_cheque+Double.parseDouble(chc.getValor());%>
                        <td align="center" ><%=Util.customFormat(Util.roundByDecimal(Double.parseDouble(chc.getPorte()),d))%></td>
                        <%total_porte=total_porte+Double.parseDouble(chc.getPorte());%>
                        <td align="center" ><%=chc.getDias()%></td>
                        <td align="center" ><%=(Util.roundByDecimal(Double.parseDouble(chc.getFactor()),5))%></td>
                        <td align="center" ><%=Util.customFormat(Util.roundByDecimal(Double.parseDouble(chc.getRemesa()),d))%></td>
                        <%total_remesa=total_remesa+Double.parseDouble(chc.getRemesa());%>
                        <td align="center" ><%=Util.customFormat(Util.roundByDecimal(Double.parseDouble(chc.getDescuento()),d))%></td>
                        <%total_descuento=total_descuento+Double.parseDouble(chc.getDescuento());%>
                        <td align="center" ><%=Util.customFormat(Util.roundByDecimal(Double.parseDouble(chc.getValorSinCustodia()),d))%></td>
                        <%total_valor_cheque_sincustodia=total_valor_cheque_sincustodia+Double.parseDouble(chc.getValorSinCustodia());%>
                        <td align="center" ><%=Util.customFormat(Util.roundByDecimal(Double.parseDouble(chc.getCustodia()),d))%></td>
                        <%total_custodia=total_custodia+Double.parseDouble(chc.getCustodia());%>
                        <td align="center" ><%=Util.customFormat(Util.roundByDecimal(Double.parseDouble(chc.getValorSinCustodia())-Double.parseDouble(chc.getCustodia()),d))%></td>
                        <%//total_valor_cheque_sincustodia=total_valor_cheque_sincustodia+Double.parseDouble(chc.getValorSinCustodia())-Double.parseDouble(chc.getCustodia());%>
                        <td align="center" ><%=Util.customFormat(Util.roundByDecimal(Double.parseDouble(chc.getRemesa())+Double.parseDouble(chc.getPorte()),d))%></td>
                        <%total_remesamasporte=total_remesamasporte+Double.parseDouble(chc.getRemesa())+Double.parseDouble(chc.getPorte());%>
                        <td align="center" ><%=Util.customFormat(Util.roundByDecimal(Double.parseDouble(chc.getValorGirable()),d))%></td>
                        <%total_valorgirable=total_valorgirable+Double.parseDouble(chc.getValorGirable());%>
						<%if (estado!=null && estado.equals("ACEPTADO")) {%> 
							<td align="center" >
							<%if (i==0)//mirar si puedo reducir
							{%>
								<input name='numche' type='text' width='50' size='10' height='5' id='do' onchange=llenar(this.value) >
							<%}else{%>
								<input name='numche' type='text' width='50' size='10' height='5' id='do' >
							<%}%>
							</td>
						<%}%>
                    </tr>
                    <%}%>
                    
                    <tr class="<%= (i%2==0)? "filagris" : "filaazul" %>" style="cursor:default" bordercolor="#D1DCEB" align="center">                            
                        <td align="center" >.</td>
                        <td align="center"   ></td>
                        <td align="center" ></td>
                        <td align="center" ></td>
                        
                        <td align="center" ></td>
                        <td align="center" ></td>
                        <td align="center" ></td>
                        <td align="center" ></td>
                        <td align="center" ></td>
                        
                        <td align="center" ></td>
                        
                        <td align="center" ></td>
                        
                        <td align="center" ></td>
                        
                        <td align="center" ></td>
                        
                        <td align="center" ></td>
                        
                    </tr>
                    <%i=i+1;%>
                    <tr class="<%= (i%2==0)? "filagris" : "filaazul" %>" style="cursor:default" bordercolor="#D1DCEB" align="center">                            
                        <td align="center" ></td>
                        <td align="center"   ></td>
                        <td align="center" ></td>
                        <td align="center" ><%=Util.customFormat(Util.roundByDecimal(total_valor_cheque,d))%></td>
                        <input name="total_valor_cheque" type="hidden" value='<%=total_valor_cheque%>'>
                        <td align="center" ><%=Util.customFormat(Util.roundByDecimal(total_porte,d))%></td>
                        <td align="center" ></td>
                        <td align="center" ></td>
                        <td align="center" ><%=Util.customFormat(Util.roundByDecimal(total_remesa,d))%></td>
                        <td align="center" ><%=Util.customFormat(Util.roundByDecimal(total_descuento,d))%></td>
                        
                        <td align="center" ><%=Util.customFormat(Util.roundByDecimal(total_valor_cheque_sincustodia,d))%></td>
                        
                        <td align="center" ><%=Util.customFormat(Util.roundByDecimal(total_custodia,d))%></td>
                        
                        <td align="center" ><%=Util.customFormat(Util.roundByDecimal(total_valor_sincustodia_custodia,d))%></td>
                        
                        <td align="center" ><%=Util.customFormat(Util.roundByDecimal(total_remesamasporte,d))%></td>
                        
                        <td align="center" ><%=Util.customFormat(Util.roundByDecimal(total_valorgirable,d))%></td>
                        <input type="hidden" value='<%=total_valorgirable%>' name="total_valorgirable">
                    </tr>
                    
                </table>
                <br>
                
                <br>

		
		
				
				<% 
				//System.out.println("despues de girable"+detalle);
				chequeCartera chn ;
				chequeCartera chw;
				int anoi=Integer.parseInt(((chequeCartera)detalle.get(0)).getFechaDesembolso().substring(0,4));
				int mesi=Integer.parseInt(((chequeCartera)detalle.get(0)).getFechaDesembolso().substring(5,7));
				int diai=Integer.parseInt(((chequeCartera)detalle.get(0)).getFechaDesembolso().substring(8,10));
				double bolsa=0;
				java.util.Calendar fd = Calendar.getInstance();
				fd.set(anoi, mesi, diai);
				int diasm=Util.diasMesB(anoi,mesi);
				int dias_mes;
				java.util.Calendar fmd = Calendar.getInstance();
				fmd.set(anoi, mesi, diasm);
				int dt=Util.diasTranscurridos(fd,fmd);
				dt++;
				int dtf=dt;
				mesi=mesi-1;
				//System.out.println("antes de for2 "+detalle);
				for( int k = 0; k < detalle.size() ; k++ )
				{

					 chn=(chequeCartera)detalle.get(k);//siempre va de primero
					 fmd.add(Calendar.DAY_OF_MONTH, 1);//le sumo un dia a la fecha del ultimo dia del mes
					 bolsa=Double.parseDouble(chn.getDias());
					 int cont=0;
					 java.util.Calendar fp = Calendar.getInstance();
					 while (bolsa > 0)
					 {
						 //chw=(chequeCartera)detalle.get(cont);
						 fp.set(anoi, mesi, diai);
						 fp.add(Calendar.MONTH,cont);
						 finte[cont]=Util.crearStringFecha(fp);
						 int dmes=Util.diasMesB(fp.get(Calendar.YEAR),fp.get(Calendar.MONTH)+1);
						 if (cont==0)
						 {
						 	dt=dtf;
							//System.out.println("A0 bl-"+mat[cont]);
							mat[cont]=mat[cont]+((dtf*Double.parseDouble(chn.getDescuento()))/(Double.parseDouble(chn.getDias()))	);
						 }
						 else
						 {

							 dt=dmes;
							 if ( (bolsa-dt)  <=  0 )
							 {
							 	dt=Integer.parseInt(chn.getFechaConsignacion().substring(8,10));
							 }
							 mat[cont]=mat[cont]+((dt*Double.parseDouble(chn.getDescuento()))/(Double.parseDouble(chn.getDias()))	);

						 }
						 bolsa=bolsa-dt;
						 cont++;
					 }//fin del while
					 
				}//fin del para
				int pos=0;
				/*while (mat[pos]!=0)
				{
					System.out.println("Pos "+pos+"-"+mat[pos]+"-"+finte[pos]);
					pos++;
				}*/
				%>
				  <input name="nit" type="hidden" value="<%=proveedor%>">
				  <input name="cod_neg" type="hidden" value="<%=((chequeCartera)detalle.get(0)).getCodigoNegocio()%>">
				  <input name="nodocs" type="hidden" value="<%=detalle.size()%>">
				  <input name="aux" type="hidden" value="<%=cliente%>">
				  <input name="vrnego" type="hidden" value="<%=Util.roundByDecimal(total_valor_cheque,d)%>"><!-- Vr Desembolso-->
				  <input name="vrdesembolso" type="hidden" value="<%=Util.roundByDecimal(total_valorgirable,d)%>"><!-- Vr Desembolso-->
				  <input name="fechaneg" type="hidden" value="<%=((chequeCartera)detalle.get(0)).getFechaDesembolso().substring(0,10)%>"><!-- Fecha Negocio-->
				  <input name="tneg" type="hidden" value=01>
				  <%
				session.setAttribute("fval",fval); 
				session.setAttribute("fven",fven); 
				session.setAttribute("intec",mat);
				session.setAttribute("finte",finte);
				%>
				<%if( loggedUser.getTipo().equals("CLIENTETSP") ){%>
				<p align="center">
					<img src="<%=BASEURL%>/images/botones/salir.gif"  name="salir" style="cursor:hand" title="Salir al Menu Principal" onMouseOver="botonOver(this);" onClick="parent.close();" onMouseOut="botonOut(this);" >
					<img src="<%=BASEURL%>/images/botones/imprimir.gif"  name="imprimir" style="cursor:hand" title="Imprimir" onMouseOver="botonOver(this);" onClick="imprimirDet(form1);" onMouseOut="botonOut(this);" >				
				</p>
					  			
				<%}else{
					if(estado!=null && estado.equals("ACEPTADO")){%>
					
					<table id="detalle" width="400" align="center" >
                    <tr class="filagris" style="cursor:default" bordercolor="#D1DCEB" align="center">
                        <td align="center">
                            <strong>No de Aval</strong>
                        </td>
                        <td align="center">
                            <input type="text" name="aval">
                        </td>
                    </tr>
                    <tr class="filagris" style="cursor:default" bordercolor="#D1DCEB" align="center">
                        <td align="center">
                            <strong>Banco de Cheque</strong>
                        </td>
                        <td align="center">
                          <input:select name="bcofid" attributesText="class=textbox" options="<%=pro %>"/>
	            </tr>
                </table>
				</p>
		
					
					<p class="style3" align="center">Observaciones Generales</p>
					<p align="center" >
					  <textarea name="textarea" cols="70" rows="3"></textarea> 
					</p>
	
					 <p align="center"><img src="<%=BASEURL%>/images/botones/Aprobar_neg.gif"  name="aceptarneg"  onMouseOver="botonOver(this);" onClick="javascript:validar()" onMouseOut="botonOut(this);" style="cursor:hand">
					  <img src="<%=BASEURL%>/images/botones/Rechazar_neg.gif"  name="rechazarneg"  onClick="location.href='<%=CONTROLLER%>?estado=Negocios&accion=Update&textarea='+textarea.value+'&codneg=<%=((chequeCartera)detalle.get(0)).getCodigoNegocio()%>&op=1'" style="cursor:hand"></p>
					  <p align="center">
					  
					  	<img src="<%=BASEURL%>/images/botones/salir.gif"  name="regresar" style="cursor:hand" title="Salir al Menu Principal" onMouseOver="botonOver(this);" onClick="parent.close();" onMouseOut="botonOut(this);" >
					  	<img src="<%=BASEURL%>/images/botones/imprimir.gif"  name="imprimir" style="cursor:hand" title="Imprimir" onMouseOver="botonOver(this);" onClick="imprimirDet(form1);" onMouseOut="botonOut(this);" >
					  
					  </p>
					<%}else{%>
						<p align="center">
						
						<img src="<%=BASEURL%>/images/botones/salir.gif"  name="salir" style="cursor:hand" title="Salir al Menu Principal" onMouseOver="botonOver(this);" onClick="parent.close();" onMouseOut="botonOut(this);" >
						<img src="<%=BASEURL%>/images/botones/imprimir.gif"  name="imprimir" style="cursor:hand" title="Imprimir" onMouseOver="botonOver(this);" onClick="imprimirDet(form1);" onMouseOut="botonOut(this);" >
							
						</p>
					<%}
					}%>
					
		    </form>
			<%}catch(Exception ww){
				System.out.println("en jsp error:"+ww.toString()+"___"+ww.getMessage());
			}
			%>
        </div>
        <iframe width="188" height="166" name="gToday:normal:agenda.js:gfPop:plugins.js" id="gToday:normal:agenda.js:gfPop:plugins_24.js" src="<%=BASEURL%>/js/Calendario/ipopeng.htm" scrolling="yes" frameborder="0" style="visibility:visible; z-index:999; position:absolute; left:-500px; top:0px;">
        </iframe>
    </body>
</html>
