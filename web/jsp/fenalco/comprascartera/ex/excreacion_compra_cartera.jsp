<!--
- Autor : Ing. Julio Ernesto Barros Rueda
- Date  : 14 de Mayo de 2007
- Copyrigth Notice : Fintravalores S.A. S.A

Descripcion : Pagina JSP, que maneja el ingreso de identidades
-->

<%-- Declaracion de librerias--%>
<%@page contentType="text/html;"%>
<%@page session="true"%>
<%@page import="java.util.*"%>
<%@page import="com.tsp.operation.model.*"%> 
<%@page import="com.tsp.operation.model.beans.*"%>
<%@page import="com.tsp.util.*"%>
<%@page import="com.tsp.util.Util.*"%>
<%@include file="/WEB-INF/InitModel.jsp"%>
<%@page import="com.tsp.operation.model.*"%>
<%@taglib uri="http://jakarta.apache.org/taglibs/input-0.90" prefix="input" %>

<html>
    <head>
        <title>Liquidador de Cartera</title>
        <link href="../../../css/estilostsp.css" rel="stylesheet" type="text/css"> 
        <link href="<%=BASEURL%>/css/estilostsp.css" rel="stylesheet" type="text/css">
    </head>
    
    <script type='text/javascript' src="<%=BASEURL%>/js/Validacion.js"></script>
    <script type='text/javascript' src="<%= BASEURL %>/js/boton.js"></script>
    <script type='text/javascript' src="<%= BASEURL %>/js/general.js"></script>
    <script src='<%=BASEURL%>/js/tools.js' language='javascript'></script>
    <script src='<%=BASEURL%>/js/date-picker.js'></script>
    <script src="<%= BASEURL %>/js/transferencias.js"></script>
    
    <%
    int d=0;//cantidad de decimales
    Usuario usuario            = (Usuario) session.getAttribute("Usuario");
    
    String valorcheq=request.getParameter("valorcillo");
    
    String Retorno=request.getParameter("respuesta");
    if (Retorno==null){Retorno="";}
    if (Retorno.equals("")){
        model.creacionCompraCarteraSvc.cancelarCompraCartera();
    }
    TreeMap pro= model.Negociossvc.getProv();
    pro.put(" Seleccione","");
    model.clienteService.setTreeMapClientes2();
    TreeMap pr = model.clienteService.getTreemap();
    pr.put(" Seleccione","");
    String fechainicio=request.getParameter("fechainicio");
    if (fechainicio==null){
        fechainicio=""+com.tsp.util.Util.AnoActual()+"-"+com.tsp.util.Util.getFechaActual_String(3)+"-"+com.tsp.util.Util.getFechaActual_String(5);
    }
    
    //public static java.util.Calendar TraerFecha(int fila, int dia, int mes, int ano, String dias_primer_cheque){
    java.util.Date x;
    x=(Util.TraerFecha(2, 5, 1, 2008, "30")).getTime();
    System.out.println("x.toString()="+x.toString());    
    x=(Util.TraerFecha(3, 5, 1, 2008, "30")).getTime();
    System.out.println("x.toString()="+x.toString());  
    System.out.println("Calendar.MONTH="+Calendar.MONTH);  
    String itemobtenible=request.getParameter("itemobtenible");
	String fechachequeobtenible=request.getParameter("fechachequeobtenible");
	String feccheqconsigobtenible=request.getParameter("feccheqconsigobtenible");
	String valorobtenible=request.getParameter("valorobtenible");
	
	%>
    
    <script>
        function enviarFormularioParaAgregar(CONTROLLER,frm){	
			if (validar(frm)==true){
				frm.action='<%=CONTROLLER%>?estado=creacion&accion=CompraCartera&opcion=AgregarCheque';
				frm.submit();
			}else{
				alert("Por favor revise los datos.");
			}
			
        }
        
        function enviarFormularioX(CONTROLLER,frm){	
			frm.action='<%=CONTROLLER%>?estado=creacion&accion=CompraCartera&opcion=AceptarConjuntoDcheques';
			frm.submit();
        }    
        
        function cancelarCompraCartera(frm){
            frm.action='<%=CONTROLLER%>?estado=creacion&accion=CompraCartera&opcion=cancelarCompraCartera';
            frm.submit();
        }
        
        function consultar(frm){
            frm.action='<%=CONTROLLER%>?estado=creacion&accion=CompraCartera&opcion=consultar';
            frm.submit();
        }
        
		function obtenercheque(ite,frm){
            frm.action='<%=CONTROLLER%>?estado=creacion&accion=CompraCartera&opcion=obtenercheque&itemobtenible='+ite;
            frm.submit();
        }
		
		function obtenerfechaconsig(frm){
			frm.action='<%=CONTROLLER%>?estado=creacion&accion=CompraCartera&opcion=obtenerfechaconsig';
            frm.submit();
        }
		
		function modificar(frm){
            frm.action='<%=CONTROLLER%>?estado=creacion&accion=CompraCartera&opcion=modificar';
            frm.submit();
        }
				
		function validar(frm){
			var respuesta=true;
			if (!(frm.itemcillo.value!=null && frm.itemcillo.value!='')){
				respuesta=false;
			}
			if (!(frm.proveedor.value!=null && frm.proveedor.value!='Seleccione' && frm.proveedor.value!='')){
				//alert("false");
				respuesta=false;
			}
			if (!(frm.nit.value!=null && frm.nit.value!='Seleccione' && frm.nit.value!='')){
				//alert("false");
				respuesta=false;
			}
			if (!(frm.valorcillo.value!=null && frm.valorcillo.value!='')){
				//alert("false");
				respuesta=false;
			}
			<%if (model.creacionCompraCarteraSvc.getCheques().size()<=0){%> 
				//alert("empezando");
				if (!(frm.cantidadcheques.value!=null && frm.cantidadcheques.value!='')){
					//alert("false");
					respuesta=false;
				}
				
				
			<%}else{%>
				//alert("normal");
			<%}%>
			return respuesta;
		}				
				
    </script>
    
    <body onLoad="javascript:formulario.textbox.focus">
        
        <div id="capaSuperior" style="position:absolute; width:100%; height:100px; z-index:0; left: 0px; top: 0px;">
            <jsp:include page="/toptsp.jsp?encabezado=LIQUIDADOR DE CARTERA"/>
        </div>
        <div id="capaCentral" style="position:absolute; width:100%; height:87%; z-index:0; left: 0px; top: 100px; overflow: scroll;">
            
            <FORM name='formulario' method="post" >
                
                <table width="432" height="167" border="2"align="center">
                    
                    <tr>
                        <td width="420" height="159">
                            <table width="100%" height="72%" class="tablaInferior" >
                                <tr class="fila">
                                    <td colspan="2" align="left" nowrap class="subtitulo">&nbsp;Liquidador De Cartera </td>
                                    <td colspan="2" align="left" nowrap class="bordereporte"><img src="<%=BASEURL%>/images/fintra.gif" width="166" height="28" class="bordereporte"> </td>
                                </tr>
                                
                                <tr class="fila">
                                    <td colspan="2" >Escoja el Afiliado </td>
                                    <td class="fila">
                                        
                                        <%if (model.creacionCompraCarteraSvc.getCheques().size()<=0){%> 
                                            <input name="textbox" type="text" class="textbox" id="campo" style="width:200;" onKeyUp="buscar(document.formulario.proveedor,this)" size="15" >                            
                                            <input:select  name="proveedor" attributesText="class=textbox" options="<%=pro %>"  />	      
                                        <%}else{%>
                                            <input name="textbox" type="text" class="textbox" id="campo" style="width:200;" size="15" readonly                             
                                             value='<%=((chequeCartera)model.creacionCompraCarteraSvc.getCheques().get(0)).getNombreProveedor()%>' >
                                            <input type="hidden" name="proveedor" value='<%=((chequeCartera)model.creacionCompraCarteraSvc.getCheques().get(0)).getProveedor()%>'>
                                        <%}%>
                                        
                                    </td>
                                </tr>
                                
                                <tr class="fila">
                                    <td colspan="2" >Escoja el Cliente </td>
                                    <td class="fila">

                                        <%if (model.creacionCompraCarteraSvc.getCheques().size()<=0){%> 
                                            <input name="textbox2" type="text" class="textbox" id="campo" style="width:200;" onKeyUp="buscar(document.formulario.nit,this)" size="15"  >
                                            <div id="tabla">
                                                <input:select name="nit" attributesText="class=textbox" options="<%=pr %>"/>
                                            </div>
                                        <%}else{%>
                                            <input name="textbox2" type="text" class="textbox" id="campo" style="width:200;" size="15" readonly                             
                                             value='<%=((chequeCartera)model.creacionCompraCarteraSvc.getCheques().get(0)).getNombreNit()%>' >
                                            <input type="hidden" name="nit" value='<%=((chequeCartera)model.creacionCompraCarteraSvc.getCheques().get(0)).getNit()%>'>
                                        <%}%>
                                    
                                    
                                        
                                    </td>
                                </tr>
                                
                                <tr class="fila">
                                    <td colspan="2" >Fecha de Desembolso </td>
                                    <td valign="middle">
                                        <input name='fechainicio' type='text' class="textbox" id="fechainicio" style='width:120' value='<%=fechainicio%>' readonly>
                                            <%if (model.creacionCompraCarteraSvc.getCheques().size()<=0){%> 
                                            <a href="javascript:void(0);" class="link" onFocus="if(self.gfPop)gfPop.fPopCalendar(document.formulario.fechainicio);return false;"  HIDEFOCUS > 
                                                <img src="<%=BASEURL%>\js\Calendario\cal.gif" width="16" height="16"
                                                    border="0" alt="De click aqu&iacute; para ver el calendario.">
                                            </a> 
                                            <img src="<%= BASEURL%>/images/botones/iconos/obligatorio.gif" width="10" height="10">
                                            <%}%>
                                        
                                    </td> 
                                </tr>
                                <tr class="fila">
                                    <td colspan="2" >Modalidad de custodia </td>
                                    <td>
                                        <%if (model.creacionCompraCarteraSvc.getCheques().size()<=0){%> 
                                        <select name='modalidadcustodia'>
                                            <option value='1'>Establecimiento</option>
                                            <option value='0'>Cliente</option>
                                        </select>
                                        <%}else{%>
                                            <input name="modcust2" type="text" class="textbox" id="campo" style="width:200;" size="15" readonly 
                                             <% 
                                             String modalidadcustodia2=request.getParameter("modalidadcustodia");
                                             System.out.println("modalidadcustodiajsp2"+modalidadcustodia2);
                                             if (modalidadcustodia2.equals("0")){
                                                 %> value="Cliente" <%
                                             }else{
                                                 %> value="Establecimiento"<%
                                             }
                                             %>
                                             >
                                            <input type="hidden" name="modalidadcustodia2" value="<%=modalidadcustodia2%>">
                                        <%}%>
                                    </td> 
                                </tr>
                                <tr class="fila">
                                    <td colspan="2" >Modalidad de remesa </td>
                                    <td>
                                        <%if (model.creacionCompraCarteraSvc.getCheques().size()<=0){%> 
                                        <select name='modalidadremesa'>
                                            <option value='1'>Establecimiento</option>
                                            <option value='2'>Sin remesa</option>
                                            <option value='0'>Cliente</option>
                                        </select>
                                        <%}else{%>
                                            <input name="modrem2" type="text" class="textbox" id="campo" style="width:200;" size="15" readonly 
                                             <% 
                                             String modalidadremesa2=request.getParameter("modalidadremesa");
                                             System.out.println("modalidadremesajsp2"+modalidadremesa2);
                                             if (modalidadremesa2.equals("1")){
                                                 %> value="Establecimiento"<%
                                             }
                                             if (modalidadremesa2.equals("2")){
                                                 %> value="Sin remesa"<%
                                             }
                                             if (modalidadremesa2.equals("0")){
                                                 %> value="Cliente"<%
                                             }
                                             %>
                                             >
                                            <input type="hidden" name="modalidadremesa2" value="<%=modalidadremesa2%>">
                                        <%}%>
                                    </td> 
                                </tr>
                                <%if (model.creacionCompraCarteraSvc.getCheques().size()<=0){%> 
                                <tr class="fila">
                                    <td colspan="2" >Cantidad de cheques</td>
                                    <td colspan="1" >
                                        <input name="cantidadcheques" type="text" class="textbox" maxlength="3" onKeyPress="soloDigitos(event,'decNO')">
                                    </td> 
                                </tr> 
                                <%}%>
                                <tr class="fila">
                                    <td colspan="3" bgcolor='009900' > </td>
                                </tr>
                                <tr class="fila">
                                    <td colspan="2" > Item</td>
									<%if (itemobtenible==null){
								    	itemobtenible =model.creacionCompraCarteraSvc.getNextItem();
									}%>			
                                    <td width="49%" valign="middle"><input value="<%=itemobtenible%>" name="itemcillo" type="text" class="textbox" value='' maxlength="6" onKeyPress="soloDigitos(event,'decNO')">
                                        <span class="style2">			  Ej: 2 </span>
                                    </td>
                                </tr>
                                
                                <tr class="fila">
                                    <td colspan="2" >Fecha del Cheque </td>
                                    <td valign="middle">
									<%if (fechachequeobtenible==null){
								    	fechachequeobtenible =com.tsp.util.Util.AnoActual()+"-"+com.tsp.util.Util.getFechaActual_String(3)+"-"+com.tsp.util.Util.getFechaActual_String(5);
									}%>			
                                        <input name='fechacheque' type='text' class="textbox" id="fechacheque" style='width:120' value='<%=fechachequeobtenible%>' readonly>
                                            <a href="javascript:void(0);" class="link" onFocus="if(self.gfPop)gfPop.fPopCalendar(document.formulario.fechacheque);return false;"  HIDEFOCUS > <img src="<%=BASEURL%>\js\Calendario\cal.gif" width="16" height="16" border="0" alt="De click aqu&iacute; para ver el calendario."></a> 
                                        <img src="<%= BASEURL%>/images/botones/iconos/obligatorio.gif" width="10" height="10">
										<a href="JavaScript:obtenerfechaconsig(formulario)">--></a>
                                    </td> 
                                </tr>
                                <tr class="fila">
                                    <td colspan="2" >Fecha de consignaci�n </td>
                                    <td valign="middle">
									<%if (feccheqconsigobtenible==null){
								    	feccheqconsigobtenible =com.tsp.util.Util.AnoActual()+"-"+com.tsp.util.Util.getFechaActual_String(3)+"-"+com.tsp.util.Util.getFechaActual_String(5);
									}%>			
                                        <input name='fechaconsignacion' type='text' class="textbox" id="fechaconsignacion" style='width:120' value='<%=feccheqconsigobtenible%>' readonly>
                                            <a href="javascript:void(0);" class="link" onFocus="if(self.gfPop)gfPop.fPopCalendar(document.formulario.fechaconsignacion);return false;"  HIDEFOCUS > <img src="<%=BASEURL%>\js\Calendario\cal.gif" width="16" height="16" border="0" alt="De click aqu&iacute; para ver el calendario."></a> 
                                        <img src="<%= BASEURL%>/images/botones/iconos/obligatorio.gif" width="10" height="10">
                                    </td> 
                                </tr>
                                <tr class="fila">
                                    <td colspan="2" > Valor del cheque </td>
                                    <td width="49%" valign="middle">
									<%if (valorobtenible==null){
										if (valorcheq==null || valorcheq.equals("")){
											valorobtenible="";
										}else{
											valorobtenible=valorcheq;
										}
								    											
									}%>	
                                        <input name="valorcillo" type="text" class="textbox" 
                                            value='<%=valorobtenible%>'                                            	
                                            maxlength="10" onKeyPress="soloDigitos(event,'decNO')">
                                        <span class="style2">			  Ej: 10000 </span>
                                    </td>
                                </tr>
                                <!--
                                <tr class="fila">
                                    <td colspan="2" > Porte</td>
                                    <td width="49%" valign="middle">
                                        <input name="porte" type="text" class="textbox" value='0' maxlength="9" onKeyPress="soloDigitos(event,'decNO')">
                                        <span class="style2">			  Ej: 10000 </span>
                                    </td>
                                </tr>
                                -->
                                
                            </table>
                            
                        </td>
                    </tr>
                </table>
                <br>
                <div align="center">
                    <img src="<%=BASEURL%>/images/botones/agregar.gif"  name="imgaceptar" onClick="javascript:enviarFormularioParaAgregar('<%=CONTROLLER%>',formulario);" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" style="cursor:hand">
                </div>
                
                <br>
                <table id="detalle" width="100%" >
                    <tr  id="fila1" class="tblTitulo">
                        <td align="center">Item  </td>
                        <td align="center">Fecha de Cheque </td>
                        <td align="center">Fecha de consignaci�n</td>                                   
                        <td align="center">&nbsp;Valor&nbsp;</td>
                        <td align="center">&nbsp;Porte&nbsp;</td>
                        <td align="center">&nbsp;D�as&nbsp;</td>
                        <td align="center">&nbsp;Factor&nbsp;</td>
                        <td align="center">&nbsp;Remesa&nbsp;</td>                        
                        <td align="center">&nbsp;Descuento&nbsp;</td>
                        <td align="center">&nbsp;Valor sin custodia&nbsp;</td>
                        <td align="center">&nbsp;Custodia&nbsp;</td>
                        <td align="center">&nbsp;Valor sin custodia - Custodia&nbsp;</td>
                        <td align="center">&nbsp;Remesa+Porte&nbsp;</td>
                        <td align="center">&nbsp;Valor a girar&nbsp;</td>
                        <td align="center">Item  </td>
                    </tr>				
                    <% 
                      
                    double total_valor_cheque=0.0;
                    double total_descuento=0.0;
                    double total_valor_cheque_sincustodia=0.0;
                    double total_custodia=0.0;
                    double total_valor_sincustodia_custodia=0.0;
                    double total_remesamasporte=0.0;
                    double total_valorgirable=0.0;
                    double total_remesa=0.0;
                    double total_porte=0.0;
                    ArrayList detalle=model.creacionCompraCarteraSvc.getCheques();
                    chequeCartera chc ;
                    System.out.println("hola_size"+detalle.size());
                    int i=0;
                    
                    
                    
                    for(  i = 0; i < detalle.size(); i++ ){
                        chc=(chequeCartera)detalle.get(i);
                        System.out.println("chc.getValorGirableSinRemesaNiPorte()="+chc.getValorGirableSinRemesaNiPorte()); 
                    %>	
                    <tr class="<%= (i%2==0)? "filagris" : "filaazul" %> style="cursor:default" bordercolor="#D1DCEB" align="center"> 					   
                        <td align="center" > <a href="JavaScript:obtenercheque(<%=chc.getItem()%>,formulario)"><%=chc.getItem()%></a></td>
                        <td align="center"   ><%=chc.getFechaCheque().substring(0,10)%></td>
                        <td align="center" ><%=chc.getFechaConsignacion().substring(0,10)%></td>
                        <td align="center" ><%=Util.customFormat(Util.roundByDecimal(Double.parseDouble( chc.getValor()),d))%></td>
                        <%total_valor_cheque=total_valor_cheque+Double.parseDouble(chc.getValor());%>
                        <td align="center" ><%=Util.customFormat(Util.roundByDecimal(Double.parseDouble( chc.getPorte()),d))%></td>
                        <%total_porte=total_porte+Double.parseDouble(chc.getPorte());%>
                        <td align="center" ><%=chc.getDias()%></td>
                        <td align="center" ><%=(Util.roundByDecimal(Double.parseDouble( chc.getFactor()),5))%></td>
                        <td align="center" ><%=Util.customFormat(Util.roundByDecimal(Double.parseDouble( chc.getRemesa()),d))%></td>
						<%System.out.println("getRemesa="+chc.getRemesa()); %>
                        <%total_remesa=total_remesa+Double.parseDouble(chc.getRemesa());%>
						<%System.out.println("getDescuento="+chc.getDescuento()); %>
                        <td align="center" ><%=Util.customFormat(Util.roundByDecimal(Double.parseDouble( chc.getDescuento()),d))%></td>
						<%System.out.println("x1"); %>
                        <%total_descuento=total_descuento+Double.parseDouble(chc.getDescuento());%>
                        <td align="center" ><%=Util.customFormat(Util.roundByDecimal(Double.parseDouble( chc.getValorSinCustodia()),d))%></td>
												<%System.out.println("x2"); %>
                        <%total_valor_cheque_sincustodia=total_valor_cheque_sincustodia+Double.parseDouble(chc.getValorSinCustodia());%>
                        <td align="center" ><%=Util.customFormat(Util.roundByDecimal(Double.parseDouble( chc.getCustodia()),d))%></td>
												<%System.out.println("x3"); %>
                        <%total_custodia=total_custodia+Double.parseDouble(chc.getCustodia());%>
												<%System.out.println("xlast3"); %>
												
						<td align="center" ><%=Util.customFormat(Util.roundByDecimal(Double.parseDouble(chc.getValorSinCustodia())-Double.parseDouble(chc.getCustodia()),d))%></td>												
												
                        <!-- <td align="center" ><%//=Util.customFormat(Util.roundByDecimal(Double.parseDouble( chc.getValorGirableSinRemesaNiPorte()),d))%></td> -->
												<%System.out.println("x4"); %>
                        <%//total_valor_sincustodia_custodia=total_valor_sincustodia_custodia+Double.parseDouble(chc.getValorGirableSinRemesaNiPorte());%>
						<%total_valor_sincustodia_custodia=total_valor_sincustodia_custodia+(Double.parseDouble(chc.getValorSinCustodia())-Double.parseDouble(chc.getCustodia()));%>
                        <td align="center" ><%=Util.customFormat(Util.roundByDecimal( Double.parseDouble(chc.getRemesa())+Double.parseDouble(chc.getPorte()),d))%></td>
												<%System.out.println("x5"); %>
                        <%total_remesamasporte=total_remesamasporte+Double.parseDouble(chc.getRemesa())+Double.parseDouble(chc.getPorte());%>
                        <td align="center" ><%=Util.customFormat(Util.roundByDecimal(Double.parseDouble( chc.getValorGirable()),d))%></td>
												<%System.out.println("x6"); %>
                        <%total_valorgirable=total_valorgirable+Double.parseDouble(chc.getValorGirable());%>
						<td align="center" > <%=chc.getItem()%> </td>
						<%System.out.println("despues de valor girable"); %>
                    </tr>
                    <%}%>
					<input name="totdesc" type="hidden" value="<%=total_descuento%>">
					<input name="totrp" type="hidden" value="<%=total_remesamasporte%>">
                    <tr class="<%= (i%2==0)? "filagris" : "filaazul" %>" style="cursor:default" bordercolor="#D1DCEB" align="center">                            
                        <td align="center" >.</td>
                        <td align="center"   ></td>
                        <td align="center" ></td>
                        <td align="center" ></td>
                        
                        <td align="center" ></td>
                        <td align="center" ></td>
                        <td align="center" ></td>
                        <td align="center" ></td>
                        <td align="center" ></td>
                        
                        <td align="center" ></td>
                        
                        <td align="center" ></td>
                        
                        <td align="center" ></td>
                        
                        <td align="center" ></td>
                        
                        <td align="center" ></td>
                        
                    </tr>
                    <%i=i+1;%>
                    <tr class="<%= (i%2==0)? "filagris" : "filaazul" %>" style="cursor:default" bordercolor="#D1DCEB" align="center">                            
                        <td align="center" ></td>
                        <td align="center"   ></td>
                        <td align="center" ></td>
                        <td align="center" ><%=Util.customFormat(Util.roundByDecimal(total_valor_cheque,d))%></td>
                        <input name="total_valor_cheque" type="hidden" value='<%=total_valor_cheque%>'>
                        <td align="center" ><%=Util.roundByDecimal(total_porte,d)%></td>
                        <td align="center" ></td>
                        <td align="center" ></td>
                        <td align="center" ><%=Util.customFormat(Util.roundByDecimal(total_remesa,d))%></td>
                        <td align="center" ><%=Util.customFormat(Util.roundByDecimal(total_descuento,d))%></td>
                        
                        <td align="center" ><%=Util.customFormat(Util.roundByDecimal(total_valor_cheque_sincustodia,d))%></td>
                        
                        <td align="center" ><%=Util.customFormat(Util.roundByDecimal(total_custodia,d))%></td>
                        
                        <td align="center" ><%=Util.customFormat(Util.roundByDecimal(total_valor_sincustodia_custodia,d))%></td>
                        
                        <td align="center" ><%=Util.customFormat(Util.roundByDecimal(total_remesamasporte,d))%></td>
                        
                        <td align="center" ><%=Util.customFormat(Util.roundByDecimal(total_valorgirable,d))%></td>
                        <input type="hidden" value='<%=total_valorgirable%>' name="total_valorgirable">
                    </tr>
                </table>
                <br>
                <div align="center">
                    <img src="<%=BASEURL%>/images/botones/aceptar.gif"  name="imgaceptar" onClick="javascript:enviarFormularioX('<%=CONTROLLER%>',formulario);" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" style="cursor:hand">&nbsp;       &nbsp;
                    <img src="<%=BASEURL%>/images/botones/salir.gif"  name="imgsalir"  onMouseOver="botonOver(this);" onClick="parent.close();" onMouseOut="botonOut(this);" style="cursor:hand">&nbsp;       &nbsp;
                    <img src="<%=BASEURL%>/images/botones/cancelar.gif"  name="imgcancelar"  onMouseOver="botonOver(this);" onClick="javascript:cancelarCompraCartera(formulario);" onMouseOut="botonOut(this);" style="cursor:hand">&nbsp;       &nbsp;
                    <%//<img src="<%=BASEURL%->/images/botones/imprimir.gif"  name="imgimprimir"  onMouseOver="botonOver(this);" onClick="javascript:imprimir();" onMouseOut="botonOut(this);" style="cursor:hand">%>
                </div>
                <br>
                <%
                String mensaje="";
                if( Retorno != null && !(Retorno.equals(""))){
                    if (Retorno.equals("chequeagregado")){
                         mensaje="Cheque agregado.";
                    }
                    if (Retorno.equals("chequenoagregado")){
                       mensaje="Error al agregar cheque.";
                    }
                    if (Retorno.equals("chequesaceptados")){
                       mensaje="Negocio aceptado.";
                    }
                    if (Retorno.equals("chequesnoaceptados")){
                       mensaje="Error al aceptar Negocio.";
                    }
                        
                %>  
                <table border="2" align="center">
                    <tr>
                        <td>
                            <table width="100%"  border="1" align="center"  bordercolor="#F7F5F4" bgcolor="#FFFFFF">
                                <tr>
                                    <td width="229" align="center" class="mensajes"><%=mensaje%></td>
                                    <td width="29" background="<%=BASEURL%>/images/cuadronaranja.JPG">&nbsp;</td>
                                    <td width="58">&nbsp;</td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
                <%}%>
				
				
				<br>				<br>
				
				<!--
		  <input name="consultablex" type="text" class="textbox" id="campo" style="width:200;" size="15" maxlength="15"   >
		  
          <img src="<%//=BASEURL%>/images/botones/buscar.gif"  name="imgbuscar"  
            onMouseOver="botonOver(this);" 
            onClick="javascript:consultar(formulario);" 
            onMouseOut="botonOut(this);" style="cursor:hand" >

		  <img src="<%//=BASEURL%>/images/botones/modificar.gif"  name="imgmodificar"  
            onMouseOver="botonOver(this);" 
            onClick="javascript:modificar(formulario);" 
            onMouseOut="botonOut(this);" style="cursor:hand" >
			-->
                            
                <br>
            </form>
            
        </div>
       
        <iframe width="188" height="166" name="gToday:normal:agenda.js:gfPop:plugins.js" id="gToday:normal:agenda.js:gfPop:plugins_24.js" src="<%=BASEURL%>/js/Calendario/ipopeng.htm" scrolling="yes" frameborder="0" style="visibility:visible; z-index:999; position:absolute; left:-500px; top:0px;">
        </iframe>
		
    </body>
</html>


