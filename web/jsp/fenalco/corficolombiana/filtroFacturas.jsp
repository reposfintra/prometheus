<!--
- Autor : Ing. Andr�s Maturana De La Cruz 
- Date  : .05.2006
- Modificacion: TMolina
- Copyrigth Notice : Fintravalores S.A. S.A
-->
<%--
-@(#)
--Descripcion : Pagina JSP, que maneja la depuraci�n masiva de tramos.
--%>
<%@page contentType="text/html"%>
<%@page session="true"%>
<%@page import="java.util.*"%>
<%@page import="java.text.*"%>
<%@page import="com.tsp.operation.model.*"%>
<%@page import="com.tsp.operation.model.beans.*"%>
<%@page import="com.tsp.operation.controller.*"%>
<%@page import="com.tsp.util.*"%>
<%@page errorPage="/error/ErrorPage.jsp"%>
<%@include file="/WEB-INF/InitModel.jsp"%>
<%@ taglib uri="http://jakarta.apache.org/taglibs/input-0.90" prefix="input" %>
<%String path = application.getRealPath(model.menuService.getRealUrl(request.getRequestURI()));
  String datos[] = model.menuService.getContenidoMenu(BASEURL,request.getRequestURI(),path);
  Usuario usuario = (Usuario) session.getAttribute("Usuario");
  ArrayList prefijos = model.gestionConveniosSvc.getPrefijosCXC(usuario.getBd());
ArrayList fiducias = model.gestionConveniosSvc.getFiducias();
%>
<html>
<head>

<title>Cargar a Fiducia</title>
<link href="<%= BASEURL %>/css/estilostsp.css" rel='stylesheet'>
<link href="../../../../css/estilostsp.css" rel="stylesheet" type="text/css">
<link href="/css/estilostsp.css" rel="stylesheet" type="text/css">
</head>
<script type='text/javascript' src="<%= BASEURL %>/js/validar.js"></script>
<script src='<%= BASEURL %>/js/date-picker.js'></script>
<script type='text/javascript' src="<%= BASEURL %>/js/boton.js"></script>
<script src='<%=BASEURL%>/js/tools.js' language='javascript'></script>
<script language="javascript" src="<%=BASEURL%>/js/general.js"></script>
<script>
function validarForma(){
	for (i = 0; i < forma.elements.length; i++){
     if (forma.elements[i].value == "" && forma.elements[i].name!="text" && forma.elements[i].name!="clientes"&& forma.elements[i].name!="proveedor"
					&& forma.elements[i].name!="agencia" && forma.elements[i].name!="nit_fiducia" && forma.elements[i].name!="agencia_d" && forma.elements[i].name!="agencia_c" && forma.elements[i].name!="cccli" && forma.elements[i].name!="vence" && forma.elements[i].name!="lim1" && forma.elements[i].name!="lim2"){
 
                    forma.elements[i].focus();
                    alert('No se puede procesar la informaci�n. Campo Vac�o.');
                    return false;	
            }
			
			if (forma.lim1.value  > forma.lim2.value)
			{
			   forma.elements[i].focus();
			   alert('El valor inicial no puede ser mayor que el final');
               return false;
			}
			if ((forma.lim1.value==""  && forma.lim2.value!="")||(forma.lim1.value!=""  && forma.lim2.value==""))
			{
			   forma.elements[i].focus();
			   alert('No pueden quedar en blanco');
               return false;
			}
			
    }
   if( forma.nit_fiducia.value==""){
                              alert('Debe seleccionar una fiducia');
                              return false;

                          }
                          return true;
                      }
</script>
<body onresize="redimensionar()" onload = 'redimensionar()'>
<div id="capaSuperior" style="position:absolute; width:100%; height:110px; z-index:0; left: 0px; top: 0px;">
 <jsp:include page="/toptsp.jsp?encabezado=Cargar a Fiducia"/>
</div>
<div id="capaCentral" style="position:absolute; width:100%; height:87%; z-index:0; left: 0px; top: 109px; overflow: scroll;">
<%
	TreeMap pr = model.clienteService.getTreemap();         
	pr.put("     ...","");
	TreeMap agcs = model.ciudadService.getCiudadTM();         
	agcs.put(" Todos","");
	Calendar FechaHoy = Calendar.getInstance();
	Date d = FechaHoy.getTime();
	SimpleDateFormat f = new SimpleDateFormat("yyyy-MM-dd");
	TreeMap pro= model.Negociossvc.getProv();
	pro.put("     ...",""); 
%>
<form name="forma" action='<%=CONTROLLER%>?estado=Analisis&accion=FacturasCorficolombiana' id="forma" method="post">
  <table width="596"  border="2" align="center" cellpadding="0" cellspacing="0">
    <tr>
      <td><table width="100%" class="tablaInferior">
        <tr>
          <td colspan="2" ><table width="100%"  border="0" cellpadding="0" cellspacing="1" class="barratitulo">
              <tr>
                <td width="56%" class="subtitulo1">&nbsp;Detalles</td>
                <td width="44%" class="barratitulo"><img align="left" src="<%=BASEURL%>/images/titulo.gif" width="32" height="20"><%=datos[0]%></td>
              </tr>
          </table></td>
        </tr>
        <tr class="fila">
          <td>Cliente</td>
          <td nowrap><table width="100%"  border="0" class="tablaInferior">
              <tr>
                <td><input name="text" type="text" class="textbox" id="campo" style="width:200;" onKeyUp="buscar(document.forma.clientes,this)" size="15" ></td>
              </tr>
              <tr>
                <td><input:select name="clientes" attributesText="class=textbox" options="<%=pr %>"/>&nbsp;</td>
              </tr>
          </table></td>
        </tr>
		 <tr class="fila">
          <td>Afiliado</td>
          <td nowrap><table width="100%"  border="0" class="tablaInferior">
              <tr>
                <td><input name="text" type="text" class="textbox" id="campo" style="width:200;" onKeyUp="buscar(document.forma.proveedor,this)" size="15" ></td>
              </tr>
              <tr>
                <td><input:select name="proveedor" attributesText="class=textbox" options="<%=pro %>"/>&nbsp;</td>
              </tr>
          </table></td>
        </tr>
		
		
        <tr class="fila">
          <td valign="middle">CC Cliente</td>
          <td nowrap><input type="text" name="cccli"></td>
        </tr>
		
		<tr class="fila">
          <td valign="middle">Agencia de Facturaci&oacute;n</td>
          <td nowrap><input:select name="agencia" attributesText="class=textbox" options="<%=agcs %>"/></td>
        </tr>
		
        <tr class="fila">
          <td valign="middle">Agencia Due&ntilde;a </td>
          <td nowrap><input:select name="agencia_d" attributesText="class=textbox" options="<%=agcs %>"/></td>
        </tr>
        <tr class="fila">
          <td valign="middle">Agencia de Cobro </td>
          <td nowrap><input:select name="agencia_c" attributesText="class=textbox" options="<%=agcs %>"/></td>
        </tr>
        <tr class="fila">
          <td width="27%" valign="middle">Fecha</td>
          <td width="73%" nowrap><input name="fecha" type='text' class="textbox" id="fecha" style='width:120' value='<%= f.format(d) %>' readonly>
            <a href="javascript:void(0)" onclick="if(self.gfPop)gfPop.fPopCalendar(document.forma.fecha);return false;" HIDEFOCUS><img src="<%=BASEURL%>\js\Calendario\cal.gif" width="16" height="16"
               border="0" alt="De click aqu&iacute; para escoger la fecha"></a></td>
        </tr>
		
		<tr class="fila">
          <td width="27%" valign="middle">Vencimiento</td>
          <td width="73%" nowrap>
		  <select name="vence">
		    <!--Mod TMolina 2008-08-05-->
		  	<option value="0">Todas</option> 
			<option value="1">Vencida</option> 
			<!-- End Mod -->
			<option value="2">Vigente</option>
          </select></td>
        </tr>
		
				
		<tr class="fila">
          <td width="27%" valign="middle">Factura</td>
          <td width="73%" nowrap>
		  <select name="tfac">
		    <option value="...">Todas</option>
		  	<%for (int i = 0; i < prefijos.size(); i++) {%>
                                            <option value="<%=((String[]) prefijos.get(i))[1]%>"><%=((String[]) prefijos.get(i))[1]%></option>
                                            <%  }%>
          </select></td>
        </tr>
		
		<tr class="fila">
          <td width="27%" valign="middle">HC</td>
          <td width="73%" nowrap>
		  <select name="hc">
		    <option value="...">Todas</option>
		  	<option value="01">Fenalco Gral</option>
			<option value="02">Fenalco</option>
			<option value="PO">ProvIntegral</option>
			<option value="GE">Geotech</option>
			<option value="FO">FVC OutSourcing</option>
			<option value="CP">CrediPago</option>
			<option value="PA">Particulares</option>
			<option value="AI">Anticipos</option>
			<option value="MB">Mail Boxes</option>
          </select></td>
        </tr>
		<tr class="fila">
		  <td valign="middle">Vencimiento</td>
		  <td nowrap> Entre
		    <input name="lim1" type="text" class="textbox" onKeyPress="soloDigitos_signo(event,'decOK');" size="6" maxlength="6"> 
		    y 
		    <input name="lim2" type="text" class="textbox" onKeyPress="soloDigitos_signo(event,'decOK');" size="6" maxlength="6">
		    Dias</td>
		  </tr>
                  <tr class="fila">
                      <td width="27%" valign="middle">Fiduciaria</td>
                      <td width="73%" nowrap>
                          <select name="nit_fiducia">
                              <option value="">...</option>
                              <%for (int i = 0; i < fiducias.size(); i++) {%>
                              <option value="<%=((String[])fiducias.get(i))[0]%>"><%=((String[]) fiducias.get(i))[1]%></option>
                              <%  }%>
                          </select></td>
                  </tr>
		
      </table></td>
    </tr>
  </table>
  <br>
  
  <br>
  <center>
    <img src="<%=BASEURL%>/images/botones/aceptar.gif" name="c_aceptar" onClick="if (validarForma()) forma.submit(); " onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" style="cursor:hand"> 
    <img src="<%=BASEURL%>/images/botones/cancelar.gif" name="c_cancelar" onClick="forma.reset();" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" style="cursor:hand"> 
    <img src="<%=BASEURL%>/images/botones/salir.gif" name="c_salir" onClick="parent.close();" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" style="cursor:hand">
  </center>
</form>
<% if( request.getParameter("msg") != null ){%>
  <p><table border="2" align="center">
  <tr>
    <td><table width="100%"  border="1" align="center"  bordercolor="#F7F5F4" bgcolor="#FFFFFF">
      <tr>
        <td width="363" align="center" class="mensajes"><%=request.getParameter("msg").toString()%></td>
        <td width="29" background="<%=BASEURL%>/images/cuadronaranja.JPG">&nbsp;</td>
        <td width="58">&nbsp;</td>
      </tr>
    </table></td>
  </tr>
</table>
</p>
 <%} %>
</div>
<%=datos[1]%>
<iframe width=188 height=166 name="gToday:normal:agenda.js:gfPop:plugins.js" id="gToday:normal:agenda.js:gfPop:plugins_24.js" src="js/Calendario/ipopeng.htm" scrolling="no" frameborder="0" style="visibility:visible; z-index:999; position:absolute; left:-500px; top:0px;"> </iframe>
</body>
</html>
