<%-- 
    Document   : importacion_ciclos
    Created on : 28/01/2011, 02:26:29 PM
    Author     : rhonalf
--%>
<%@page session="true"%>
<%@page import="java.util.*"%>
<%@page import="com.tsp.operation.model.*"%>
<%@page import="com.tsp.operation.model.beans.*"%>
<%@page import="com.tsp.operation.model.services.*"%>
<%@page import="com.tsp.util.*"%>
<%
    response.addHeader("Cache-Control","no-cache");
    response.addHeader("Pragma","No-cache");
    response.addDateHeader ("Expires", 0);
    String BASEURL = request.getContextPath();
    String CONTROLLER = BASEURL + "/controller";
%>
<%@ taglib uri="http://jakarta.apache.org/taglibs/input-0.90" prefix="input" %>
<%@page contentType="text/html" pageEncoding="ISO-8859-1"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
   "http://www.w3.org/TR/html4/loose.dtd">
<!-- This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>. -->
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
        <script type='text/javascript' src="<%= BASEURL %>/js/boton.js"></script>
        <script type='text/javascript' src="<%= BASEURL %>/js/prototype.js"></script>
        <link href="<%= BASEURL %>/css/estilostsp.css" rel="stylesheet" type="text/css">
        <title>Ciclos de facturacion - Agregar/Editar</title>
        <script type="text/javascript">
            function enviarForm(){
                var ind = $('anio_importacion').selectedIndex;
                var nombreFile = $('archivo').value;
                if(ind>0 && nombreFile!=''){
                    if(validarExtension('archivo') == true){
                        var anio = $('anio_importacion').options[ind].value;
                        var actividad = $('actividad').value;
                        $('formulario').action += '&opcion=importar&anio='+anio+'&actividad='+actividad;
                        $('formulario').submit();
                    }
                    else{
                        alert('Seleccione un archivo v\u00e1lido');
                    }
                }
                else{
                    alert('Seleccione un archivo y un a\u00f1o v\u00e1lidos');
                }
            }

            function validarExtension(id){
                var enviar = /.(xls)$/i.test($(id).value);
                if (!enviar){
                    $(id).value = '';
                }
                return enviar;
            }

            function verificarAnio(id, ind){
                if(ind>0){
                    var anio = $(id).options[ind].value;
                    var url = $('formulario').action;
                    var p =  'opcion=verificar_anio&dato='+anio;
                    new Ajax.Request(url,{method: 'post', parameters: p,onLoading: loading,onComplete: llenarSpan});
                }
                else{
                    alert('Seleccione un a\u00f1o v\u00e1lido');
                    $('process').innerHTML = '';
                    $('actividad').value = '';
                }
            }

            function loading(){
                $('process').innerHTML = '<img alt="cargando" src="<%= BASEURL%>/images/cargando.gif" name="imgload">Buscando ... por favor espere';
            }

            function llenarSpan(response){
                $('process').innerHTML = 'Para el a\u00f1o se efectuara: '+response.responseText;
                $('actividad').value = response.responseText;
            }
        </script>
    </head>
    <body>
        <div id="capaSuperior" style="position:absolute; width:100%; height:100px; z-index:0; left: 0px; top: 0px;">
            <jsp:include page="/toptsp.jsp?encabezado=Ciclos de facturacion - Agregar/Editar"/>
        </div>
        <div id="capaCentral" style="position:absolute; width:100%; height:83%; z-index:0; left: 0px; top: 100px; overflow:auto;">
            <div align="center">
                <div id="divload" style="width: 35em; border: 0.1em black double;">
                    <table width="100%" border="0">
                        <thead>
                            <tr>
                                <th class="subtitulo1" colspan="2">Ciclos de facturacion - Agregar/Editar</th>
                            </tr>
                        </thead>
                        <tbody>
<!--                                <tr class="fila">
                                <td>Tipo</td>
                                <td>
                                    <input type="radio" name="tipo" value="insercion" checked="checked">
                                    Inserci&oacute;n
                                    <input type="radio" name="tipo" value="actualizacion">
                                    Actualizaci&oacute;n
                                </td>
                            </tr>-->
                            <tr class="fila">
                                <td>Seleccione el a&ntilde;o&nbsp;</td>
                                <td>
                                    <input type="hidden" id="actividad" name="actividad" value="">
                                    <select id="anio_importacion" name="anio_importacion" onchange="verificarAnio(this.id,this.selectedIndex);" style="width: 12em;">
                                        <option value="" selected="selected">...</option>
                                        <%
                                            for(int i = 2000;i<2051;i++){
                                                out.print("<option value='"+i+"'>"+i+"</option>");
                                            }
                                        %>
                                    </select>
                                    &nbsp;
                                    <span id="process" class="letra"></span>
                                </td>
                            </tr>
                            <tr class="fila">
                                <td>Archivo</td>
                                <td>
                                    <form action="<%= CONTROLLER%>?estado=Ciclos&accion=Facturacion" method="post" id="formulario" name="formulario" enctype="multipart/form-data">
                                        <input type="file" id="archivo" name="archivo">
                                    </form>
                                </td>
                            </tr>
                            <tr class="fila">
                                <td colspan="2" align="center">
                                    <img alt="aceptar" src="<%= BASEURL%>/images/botones/aceptar.gif" id="imgaceptar" name="imgaceptar" onMouseOver="botonOver(this);" onClick="enviarForm();" onMouseOut="botonOut(this);" style="cursor:pointer">
                                    <img alt="salir" src="<%= BASEURL%>/images/botones/salir.gif" name="imgsalir" onMouseOver="botonOver(this);" onClick="window.close();" onMouseOut="botonOut(this);" style="cursor:pointer">
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </body>
</html>
<%
    String msj = session.getAttribute("msj")!=null? (String)session.getAttribute("msj"):"";
    if(msj!=null && msj.equals("")==false){
%>
<script type="text/javascript">
    alert('<%= msj %>');
</script>
<%
        session.removeAttribute("msj");
    }
%>