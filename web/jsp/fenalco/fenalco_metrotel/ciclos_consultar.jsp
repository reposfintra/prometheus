<%-- 
    Document   : ciclos_consultar
    Created on : 28/01/2011, 04:37:52 PM
    Author     : rhonalf
--%>
<%@page session="true"%>
<%@page import="java.util.*"%>
<%@page import="com.tsp.operation.model.*"%>
<%@page import="com.tsp.operation.model.beans.*"%>
<%@page import="com.tsp.operation.model.services.*"%>
<%@page import="com.tsp.util.*"%>
<%
    response.addHeader("Cache-Control","no-cache");
    response.addHeader("Pragma","No-cache");
    response.addDateHeader ("Expires", 0);
    String BASEURL = request.getContextPath();
    String CONTROLLER = BASEURL + "/controller";
%>
<%@ taglib uri="http://jakarta.apache.org/taglibs/input-0.90" prefix="input" %>
<%@page contentType="text/html" pageEncoding="ISO-8859-1"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
   "http://www.w3.org/TR/html4/loose.dtd">
<!-- This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>. -->
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
        <script type='text/javascript' src="<%= BASEURL %>/js/boton.js"></script>
        <script type='text/javascript' src="<%= BASEURL %>/js/prototype.js"></script>
        <link href="<%= BASEURL %>/css/estilostsp.css" rel="stylesheet" type="text/css">
        <title>Ciclos de facturacion - Consultar</title>
        <style type="text/css">
            td{
                white-space: nowrap;
            }
            div.fixedHeaderTable {
                position: relative;
                border: 0.1em black solid;
                padding: 0.1em;
                background-color: white;
                /*height: 200px;*/
            }
            div.fixedHeaderTable tbody {
                height: 200px;
                vertical-align: top;
                overflow-y: scroll;
                overflow-x: hidden;
            }
            div.fixedHeaderTable thead td, div.fixedHeaderTable thead th {
                vertical-align: top;
                position:relative;
            }

            /* IE7 hacks */
            div.fixedHeaderTable {
                *position: relative;
                *height: 200px;
                *overflow-y: scroll;
                *overflow-x: scroll;
                *padding-right:16px;
            }

            div.fixedHeaderTable thead tr {
                *position: relative;
                _position: absolute;
                *top: expression(this.offsetParent.scrollTop-2);
                *background:none;
                color: white;
                border-color: silver;
            }

            div.fixedHeaderTable tbody {
                *vertical-align: top;
                *height: auto;
                *position:absolute;
                *top:50px;
            }
            /* IE6 hacks */
            div.fixedHeaderTable {
                _width:100%;
                _overflow: auto;
                _overflow-y: scroll;
                _overflow-x: hidden;
            }
            div.fixedHeaderTable thead tr {
                _position: relative;
            }
        </style>
        <script type="text/javascript">
            function verificarAnio(id){
                 var ind = $(id).selectedIndex;
                 if(ind>0){
                    var anio = $(id).options[ind].value;
                    var url = "<%= CONTROLLER%>?estado=Ciclos&accion=Facturacion";
                    var p =  'opcion=buscar_ciclos&dato='+anio;
                    new Ajax.Request(url,{method: 'post', parameters: p,onLoading: loading,onComplete: llenarSpan});
                }
                else{
                    alert('Seleccione un a\u00f1o v\u00e1lido');
                }
            }

            function loading(){
                $('divtabla').innerHTML = '<div align="center" style="height: 200px;"><img alt="cargando" src="<%= BASEURL%>/images/cargando.gif" name="imgload">'
                    +'Buscando ... por favor espere</div>';
            }

            function llenarSpan(response){
                $('divtabla').innerHTML = response.responseText;
            }
        </script>
    </head>
    <body>
        <div id="capaSuperior" style="position:absolute; width:100%; height:100px; z-index:0; left: 0px; top: 0px;">
            <jsp:include page="/toptsp.jsp?encabezado=Ciclos de facturacion - Consultar"/>
        </div>
        <div id="capaCentral" style="position:absolute; width:100%; height:83%; z-index:0; left: 0px; top: 100px; overflow:auto;">
            <div align="center">
                <div id="divload" style="width: 51em; border: 0.1em black double;">
                    <table width="100%" border="0">
                        <thead>
                            <tr>
                                <th class="subtitulo1" colspan="2">Ciclos de facturacion - Consultar</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr class="fila">
                                <td colspan="2">Seleccione el a&ntilde;o&nbsp;
                                    <select id="anio_importacion" name="anio_importacion" style="width: 12em;">
                                        <option value="" selected="selected">...</option>
                                        <%
                                            for(int i = 2000;i<2051;i++){
                                        %>
                                        <option value='<%= i %>'><%= i %></option>
                                        <%
                                            }
                                        %>
                                    </select>
                                    <img alt="" src="<%=BASEURL%>/images/botones/iconos/buscar.gif" onclick="verificarAnio('anio_importacion');" style="cursor:pointer">
                                </td>
                            </tr>
                            <tr class="filaazul">
                                <td colspan="2">
                                    Resultados de la busqueda
                                    <div id="divtabla" class="fixedHeaderTable">
                                        <div style="height: 200px;"></div>
                                    </div>
                                </td>
                            </tr>
                            <tr class="fila">
                                <td colspan="2" align="center">
<!--                                    <img alt="aceptar" src="<%= BASEURL%>/images/botones/aceptar.gif" id="imgaceptar" name="imgaceptar" onMouseOver="botonOver(this);" onClick="enviarForm();" onMouseOut="botonOut(this);" style="cursor:pointer">-->
                                    <img alt="salir" src="<%= BASEURL%>/images/botones/salir.gif" name="imgsalir" onMouseOver="botonOver(this);" onClick="window.close();" onMouseOut="botonOut(this);" style="cursor:pointer">
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </body>
</html>