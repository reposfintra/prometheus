<%-- 
    Document   : cargar_fasecolda
    Created on : 28-sep-2015, 14:38:03
    Author     : edgargonzalezmendoza
--%>

<%@page contentType="text/html" pageEncoding="ISO-8859-1"%>
<%@ include file="/WEB-INF/InitModel.jsp"%>
<%@page errorPage="/error/ErrorPage.jsp" %>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
        <title>Cargar Fasecolda</title

        <link type="text/css" rel="stylesheet" href="./css/jquery/jquery-ui/jquery-ui.css" />
        <link href="./css/popup.css" rel="stylesheet" type="text/css">
        <link href="./css/fasecolda.css" rel="stylesheet" type="text/css">
        <script type="text/javascript" src="./js/jquery/jquery.jqGrid/js/jquery.min.js"></script>
        <script type="text/javascript" src="./js/jquery-ui-1.8.5.custom.min.js"></script>
        <script type="text/javascript" src="./js/jquery/jquery.jqGrid-4.7.0/js/grid.locale-es.js"></script>
        <script type="text/javascript" src="./js/jquery/jquery.jqGrid-4.7.0/js/jquery.jqGrid.min.js"></script>
        <script type="text/javascript" src="./js/jquery/jquery.jqGrid-4.7.0/js/jquery.tabletojson.js"></script>

        <script type="text/javascript" src="./js/cargar_garantias.js"></script>

    </head>
    <body>
        <div id="capaSuperior" style="position:absolute; width:100%; height:100px; z-index:0; left: 0px; top: 0px;">
            <jsp:include page="/WEB-INF/toptsp.jsp?encabezado=IMPORTAR ARCHIVO"/>
        </div>
        <div id="capaCentral" style="position:absolute; width:100%; height:100%; z-index:0; left: 0px; top: 100px; ">

            <div id="frm_loadfile" >
                <h3>Esta ventana le ayudara en el proceso de carga de archivo y creaci�n de maestros  fasecolda.</h3>
                <hr>
                <table>
                    <tr>
                        <td class="letra_resaltada" align="center">&nbsp; Seleccione el archivo </td>
                        <td align="left">
                            <form id="formulario" name="formulario">
                                <input type="file" id="archivo"  name="archivo">
                            </form>
                        </td>
                        <td>
                            <button id="cargarArchivo" class="ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only"
                                    role="button" aria-disabled="false">
                                <span class="ui-button-text">Subir Archivo</span>
                            </button>
                            <input type="hidden" value=""  id="ctrl_carga" name="ctrl_carga"/>
                        </td>
                    </tr>
                </table>
            </div>
            <br>
            <div id="contenedor">
                <center>
                    <table id="archivo_fasecolda"></table>
                    <div id="page_archivo_fasecolda"></div>
                </center>     
            </div>
            <div id="dialogo" title="Mensaje">
                <p id="msj"></p>

            </div>
            <div id="dialogLoading"  class="ventana" >
                <p  id="msj2">texto </p> <br/>
                <center>
                    <img src="<%=BASEURL%>/images/cargandoCM.gif"/>
                </center>
            </div>
        </div>
    </body>
</html>
