
<!--
     - Author(s)       :      FERNEL VILLACOB
     - Date            :      13/06/2006
     - Copyright Notice:      TRANSPORTES SANCHEZ POLO S.A.
-->
<%--
     - @(#)
     - Description: Permite realizar la contabilizacion de facturas
--%>
<%@ page session   ="true"%>
<%@ page errorPage ="/error/ErrorPage.jsp"%>
<%@ page import    ="java.util.*" %>
<%@ page import    ="com.tsp.util.*" %>
<%@ include file="/WEB-INF/InitModel.jsp"%>
<!-- Para los botones -->
<%@ taglib uri="http://www.sanchezpolo.com/sot" prefix="tsp"%>
   <script src="<%=BASEURL%>/js/prototype.js"      type="text/javascript"></script>
    <link href="<%=BASEURL%>/css/mac_os_x.css"      rel="stylesheet" type="text/css">

    <script src="<%=BASEURL%>/js/effects.js"        type="text/javascript"></script>
    <script src="<%=BASEURL%>/js/window.js"         type="text/javascript"></script>
    <script src="<%=BASEURL%>/js/window_effects.js" type="text/javascript"></script>
    <link href="<%=BASEURL%>/css/default.css"       rel="stylesheet" type="text/css">
    <link href="<%=BASEURL%>/css/alert.css" rel="stylesheet" type="text/css"/>
    <link href="<%=BASEURL%>/css/alphacube.css" rel="stylesheet" type="text/css"/>


<html>
<head>
        <title>Reporte a Corficolombiana</title>
        <link   href="<%= BASEURL %>/css/estilostsp.css" rel="stylesheet" type="text/css">
        <script src="<%= BASEURL %>/js/boton.js"></script>
</head>
<body>

<div id="capaSuperior" style="position:absolute; width:100%; height:100px; z-index:0; left: 0px; top: 0px;">
<jsp:include page="/WEB-INF/toptsp.jsp?encabezado=REPORTE A CORFICOLOMBIANA"/>
</div>

<div id="capaCentral" style="position:absolute; width:100%; height:85%; z-index:0; left: 0px; top: 100px; overflow: scroll;">

 <% String msj = (request.getParameter("msj")==null)?"":request.getParameter("msj");
    ArrayList reportes=model.corficolombianaSvc.getReportes();

%>


 <form action='' method='post' name='formulario' onsubmit='return false;'>

       
       <table border="2" align="center">
           <tr class="fila">
              <td>&nbsp;Periodo&nbsp;&nbsp;<input name="periodo" id="periodo" value="" type="text" class="textbox" maxlength="6"></td>
              <td>&nbsp;Reporte&nbsp;&nbsp;
                <select name="reporte" id="reporte">
                    <%for(int i=0;i<reportes.size();i++){%>
                    <option value="<%=((String[])reportes.get(i))[0]%>;;;<%=((String[])reportes.get(i))[1]%>"><%=((String[])reportes.get(i))[1]%></option>
                    <%}%>
                </select>
             </td>
          </tr>
       </table><br><br><br>



                <div id="tabla_generica" align="center"></div>
   </form>
   <br>


   <!--botones -->
   <p align="center">
       <img src="<%=BASEURL%>/images/botones/aceptar.gif"  onMouseOver="botonOver(this);" onClick="sendAction('<%=CONTROLLER%>?estado=Reporte&accion=Corficolombiana','sql_totales','');" onMouseOut="botonOut(this);" style="cursor:hand">
       <img src="<%=BASEURL%>/images/botones/generar.gif"  onMouseOver="botonOver(this);" onClick="sendAction('<%=CONTROLLER%>?estado=Reporte&accion=Corficolombiana','sql_gtxt','GENERAR_REPORTE');" onMouseOut="botonOut(this);" style="cursor:hand">
       <tsp:boton value="salir"     onclick="parent.close();"/>
  </p>


  <% if( ! msj.equals("") ){%>
          <table border="2" align="center">
                  <tr>
                    <td>
                        <table width="100%"  border="1" align="center"  bordercolor="#F7F5F4" bgcolor="#FFFFFF">
                              <tr>
                                    <td width="400" align="center" class="mensajes"><%=msj%></td>
                                    <td width="29" background="<%=BASEURL%>/images/cuadronaranja.JPG">&nbsp;</td>
                                    <td width="58">&nbsp; </td>
                              </tr>
                         </table>
                    </td>
                  </tr>
          </table>
   <%}%>

    <script>
        function sendAction(url,opcion,parametro){
            var p = "reporte=" + $('reporte').value + "&periodo=" + $('periodo').value + "&tipo="+opcion+ "&parametro="+parametro;
            if(parametro==''){
                openInfoDialog('<b> Ejecutando<br/>Por favor espere...</b>');
                new Ajax.Request(
                url,
                {
                    method: 'post',
                    parameters: p,
                    onComplete: showTable
                });
            }
            else{
                if(parametro=='GENERAR_REPORTE'){
                openInfoDialog('<b> El proceso ha empezado<br/>Por favor espere...</b>');
                new Ajax.Request(
                    url,
                    {
                        method: 'post',
                        parameters: p,
                        onComplete: showReport
                    });
                }
                else{
                    var win = new Window(
                    {   id: "detalles",
                        title: "DETALLES CUENTA",
                        width:800,
                        height:600,
                        showEffect:Effect.BlindDown,
                        hideEffect: Effect.Fade,
                        destroyOnClose: true,
                        url: url+'&'+p
                    });
                    win.showCenter()
                    win.show(true);
                }
            }
        }
        function showReport (response){
            Dialog.closeInfo();
                Dialog.alert("Proceso Terminado ...", {
                    width:250,
                    height:100,
                    windowParameters: {className: "alphacube"}
                });
        }

        function showTable (response){
                Dialog.closeInfo();
                $('tabla_generica').innerHTML=response.responseText;
        }

       function openInfoDialog(mensaje) {
                Dialog.info(mensaje, {
                    width:250,
                    height:100,
                    showProgress: true,
                    windowParameters: {className: "alphacube"}
                });
            }

      
    </script>

    <script>
        function color_fila(id,color){
            $(id).style.backgroundColor=color;
        }
    </script>

</div>
</body>
</html>
 
