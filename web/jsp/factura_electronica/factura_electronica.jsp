<%-- 
    Document   : Factura Electronica
    Created on : 18-dic-2018, 16:43:15
    Author     : HaroldC
    http://localhost:8094/fintra/controller?estado=Menu&accion=Cargar&carpeta=/jsp/factura_electronica&pagina=factura_electronica.jsp
--%>

<%@page contentType="text/html" pageEncoding="ISO-8859-9"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-9">
        <title>Filtro de Creditos</title>

        <link type="text/css" rel="stylesheet" href="/fintra/css/jquery/jquery-ui/jquery-ui.css" />
        <link rel="stylesheet" type="text/css" href="/fintra/css/jCal/jscal2.css" />

        <script type="text/javascript" src="/fintra/js/jquery/jquery-ui/jquery.min.js"></script>
        <script type="text/javascript" src="/fintra/js/jquery/jquery-ui/jquery.ui.min.js"></script>
        <script type="text/javascript" src="/fintra/js/FacturaElectronica.js"></script>
        <script type="text/javascript" src="/fintra/js/jCal/jscal2.js"></script>
        <script type="text/javascript" src="/fintra/js/jCal/lang/es.js"></script>
    </head>
    <style>
        .ui-jqgrid input, .alicuotas td {
            padding: 0.2em 0.5em;
        }
        .alicuotas{
            border: 1em; 
            border-collapse:collapse; 
            width: 100%; 
            margin-top: 0.2em; 
            text-align: right;
        }
        .alicuotas th {
            text-align: center;
        }
    </style>
    <body>
        <jsp:include page="/toptsp.jsp?encabezado=Factura Electronica"/>
        <div id="capaCentral" style="text-align: center;">
            <center>
                <table style="width: 100%" align="center">
                    <tr>
                        <td>
                            <div class="ui-jqgrid ui-widget ui-widget-content ui-corner-all" style="width: 550px; padding: 0.5em 2em; margin: 0.2em; float: right">

                                <div class="ui-dialog-titlebar ui-widget-header ui-corner-all ui-helper-clearfix" 
                                     style="text-align: center; margin:0em -1.6em">RESUMEN</div>
                                <table>
                                    <tr>
                                        <td><b>Valor Cr�dito:</b></td>
                                        <td><input maxlength="15" id="valor" 
                                                   value="" size="20" type="text" required readonly></td>
                                        <td><b>CUOTA:</b></td>
                                        <td><label id="vlr_cuota"></label></td>
                                    </tr><tr>
                                        <td><b>Plazo</b></td>
                                        <td><input maxlength="3" id="plazo"  
                                                   value="" size="3" type="text" required readonly></td>
                                        <td><b>SEGURO</b></td>
                                        <td><label id="vlr_seguro"></label></td>
                                    </tr>
                                    <tr>
                                        <td><b>VIABLE:</b></td>
                                        <td><select name="viable" id="viable">
                                            <option value="S">SI</option>
                                            <option value="N">NO</option>                                         
                                        </select></td>
                                        <td><b>EXTRAPRIMA: </b></td>
                                        <td><label id="vlr_extra"></label></td>
                                    </tr>
                                    <tr>
                                        <td colspan="2"></td>
                                        <td><b>TOTAL:</b></td>
                                        <td><label id="total_cuota"></label></td>
                                    </tr>
                                </table>
                                <div class="ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only" 
                                     role="button" aria-disabled="false" onclick="GuardarInicialHCG();">
                                    <span class="ui-button-text">Guardar y Consultar</span>
                                </div>
                            </div></td>
                    </tr>
                </table>
            </center>
        </div>
        <div id="dialogo_docs_requeridos" class="ventana" style="display:none;"></div>
        <div id="dialogo" class="ventana" style="display:none;"><p id="msj"></p></div>
        <script>
            init();
        </script>
    </body>
</html>
