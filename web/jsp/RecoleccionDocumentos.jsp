<%-- 
    Document   : RecoleccionDocumentos
    Created on : 19/07/2016, 12:23:54 PM
    Author     : egonzalez
--%>

<%@page contentType="text/html" pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
        <link href="./css/popup.css" rel="stylesheet" type="text/css">
        <link type="text/css" rel="stylesheet" href="./css/jquery/jquery-ui/jquery-ui.css" />
        <link type="text/css" rel="stylesheet" href="./css/TransportadorasApi.css " />
        <script type="text/javascript" src="./js/jquery/jquery.jqGrid/js/jquery.min.js"></script>
        <script type="text/javascript" src="./js/jquery-ui-1.8.5.custom.min.js"></script>   
        <script type="text/javascript" src="./js/jquery/jquery.jqGrid-4.7.0/js/grid.locale-es.js"></script>
        <script type="text/javascript" src="./js/jquery/jquery.jqGrid-4.7.0/js/jquery.jqGrid.min.js"></script>
        <script type="text/javascript" src="./js/jquery/jquery.jqGrid-4.7.0/js/jquery.tabletojson.js"></script>    
        <script type="text/javascript" src="./js/RecoleccionDocumentos.js"></script> 
        <title>PROGRAMACION DE MENSAJERIA</title>
    </head>
    <body>
        <div id="capaSuperior" style="position:absolute; width:100%; height:100px; z-index:0; left: 0px; top: 0px;">
            <jsp:include page="/WEB-INF/toptsp.jsp?encabezado=PROGRAMACION DE MENSAJERIA"/>
        </div>
        <style>
            input{    
                width: 09px;
                color: black;
            }
            .gbox_tabla_reportePlizas{
                margin-top: -12px;
            }
        </style>

    <center>
        <div id="tablita" style="top: 170px;width: 1315px;height: 95px;" >
            <div class="ui-dialog-titlebar ui-widget-header ui-helper-clearfix">
                <span class="ui-dialog-title text-center" id="ui-dialog-title-listaGlobal">
                    &nbsp;
                    <label class="titulotablita" ><b>FILTRO DE BUSQUEDA</b></label>
                </span>
            </div>
            <div class="row" style="padding: 5px">
                <div class="col-md-12 ">
                    <table id="tablainterna" style="height: 53px; width: 1305px"  >
                        <tr>
                            <td>
                                <label>Fecha inicio</label>
                            </td>
                            <td>
                                <input type="datetime" id="fecha" name="fecha" style="height: 15px;width: 88px;color: #070708;" readonly>
                            </td>
                            <td>
                                <label>Fecha fin</label>
                            </td>
                            <td>
                                <input type="datetime" id="fechafin" name="fecha" style="height: 15px;width: 88px;color: #070708;" readonly>
                            </td>
                            <td>
                                <label>Unidad de negocio</label>
                            </td>
                            <td>
                                <select id="linea_negocio" name="linea_negocio" style="height: 23px;width: 160px;color: #070708;"></select>
                            </td>
                            <td>
                                <label>Pendientes por recepcion</label>
                            </td>
                            <td>
                                <input type="checkbox"  id="visualizar"  style="margin-right: -13px;margin-left: 2px;" />
                                <!---->
                            </td>
                            <td>
                                <label id="emp" style="margin-left: 20px;">Empresa</label>
                            </td>
                            <td>
                                <select id="empresa_envio" name="empresa_envio" style="height: 23px;width: 160px;color: #070708;"></select>
                            </td>
                            <td>
                                <hr style="width: 2px;height: 39px;top: -25px;margin-left: 13px;color: rgba(128, 128, 128, 0.39);" /> 
                            </td>
                            <td>
                                <div style="padding-top: 10px">
                                    <button id="buscar" class="ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only" style="margin-left: 15px;top: -7px;" > 
                                        <span class="ui-button-text">Buscar</span>
                                    </button>  
                                </div>
                            </td>
                            <td>
                                <div style="padding-top: 10px">
                                    <button id="limpiar" class="ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only" style="margin-left: 15px;top: -7px;" > 
                                        <span class="ui-button-text">limpiar fechas</span>
                                    </button>  
                                </div>
                            </td>
                        </tr>
                    </table>
                </div>
            </div>
        </div>
        <div style="position: relative;top: 231px;">
            <table id="tabla_recoleccionDocumentos" ></table>
            <div id="pager"></div>
        </div>
        <div id="info"  class="ventana" >
            <p id="notific">EXITO AL GUARDAR</p>
        </div>
        <div id="divSalidaEx" title="Exportacion" style=" display: block" >
            <p  id="msjEx" style=" display:  none"> Espere un momento por favor...</p>
            <center>
                <img id="imgloadEx" style="position: relative;  top: 7px; display: none " src="./images/cargandoCM.gif"/>
            </center>
            <div id="respEx" style=" display: none"></div>
        </div>
        <div id="dialogo_docs_requeridos" class="ventana" style="display:none;">           
        </div>
        <div id="dialogMsjEmpresas" style="position: relative;"class="ventana">
            <fieldset style="height: 68%;width: 310px;margin-top: 10px;">
                <!--<legend>CUADRO DE CONTROL </legend>--> 
                <table>
                    <tr>
                        <td>
                            <label>Por favor escoger la empresa de mensajeria</label> 
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <label>Empresa envio</label> 
                        </td>
                        <td>
                            <select id="empresa_envio_2" name="empresa_envio_2" style="height: 23px;width: 160px;color: #070708;margin-left: -155px;"></select>
                        </td> 
                    </tr>
                </table>
            </fieldset>
        </div>

        <div id="dialogMsjObservaciones" style="position: relative;"class="ventana">
            <input type="text" id="id_solicitud" style="width: 62px;" hidden>
            <fieldset style="height: 89%;width: 310px;margin-top: 10px;border-top: 1px solid #b9a8a880;border-bottom: none;border-right: none;border-left: none;">
                <legend>OBSERVACIONES</legend> 
                <textarea id="observaciones" cols="106" rows="34" style="width: 610px;height: 163px;"></textarea>
            </fieldset>
        </div>
    </center>
</body>
</html>
