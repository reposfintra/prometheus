<!--
- Autor : Ing. Jose de la rosa
- Date  : 9 de Octubre de 2005
- Copyrigth Notice : Fintravalores S.A. S.A
-->
<%--
-@(#)
--Descripcion : Pagina JSP, que permite ingresar los productos
--%>

<%-- Declaracion de librerias--%>
<%@page contentType="text/html"%>
<%@page session="true"%>
<%@page import="java.util.*"%>
<%@page import="com.tsp.operation.model.*"%>
<%@page import="com.tsp.operation.model.beans.*"%>
<%@page import="com.tsp.util.*"%>
<%@include file="/WEB-INF/InitModel.jsp"%>
<%@ taglib uri="http://jakarta.apache.org/taglibs/input-0.90" prefix="input" %>
<html>
<head>
<title>Productos</title>
<link href="<%= BASEURL %>/css/estilostsp.css" rel="stylesheet" type="text/css">
<script type='text/javascript' src="<%= BASEURL %>/js/boton.js"></script>
</head>
<script type='text/javascript' src="<%= BASEURL %>/js/validar.js"></script>
<script type='text/javascript' src="<%= BASEURL %>/js/funciones.js"></script>
<body onLoad="redimensionar();form1.c_codigo.focus();" onResize="redimensionar();">
<div id="capaSuperior" style="position:absolute; width:100%; height:100px; z-index:0; left: 0px; top: 0px;">
<jsp:include page="/WEB-INF/toptsp.jsp?encabezado=Ingresar Producto"/>
</div>

<div id="capaCentral" style="position:absolute; width:100%; height:85%; z-index:0; left: 0px; top: 100px; overflow: scroll;"> 
<%	String mensaje = (String) request.getAttribute("msg");
	boolean sw = false;
	sw = (request.getParameter("lupa")!=null)?true:false;
	String cliente = request.getParameter("client")!=null?request.getParameter("client"):"";
%>
<FORM name='form1' id="form1" method='POST'  action="<%=CONTROLLER%>?estado=Producto&accion=Insert">
  <table width="600" border="2" align="center">          
    <tr>
      <td>
	  <table width="100%" class="tablaInferior">
        <tr class="fila">
          <td colspan="2" align="left" class="subtitulo1">&nbsp;Productos</td>
          <td colspan="2" align="left" class="barratitulo"><img src="<%=BASEURL%>/images/titulo.gif" width="32" height="20"></td>
        </tr>
        <tr class="fila">
          <td width="9%" align="left" >Codigo</td>
          <td width="27%" valign="middle"><input name="c_codigo" type="text" class="textbox" id="c_codigo" size="10" maxlength="15">
          <img src="<%=BASEURL%>/images/botones/iconos/obligatorio.gif"></td>
          <td width="14%"  valign="middle">Unidad</td>
          <td width="50%"  valign="middle">
		  <select name="c_unidad" id="c_unidad" class="textbox">
			<%  Vector vecu2 = model.unidadService.listarUnidades(); 
			for(int i = 0; i<vecu2.size(); i++){	
				Unidad u2 = (Unidad) vecu2.elementAt(i);
				%>
				<option value="<%=u2.getCodigo()%>"><%=u2.getDescripcion()%> </option>
				<%
			}%>
		  </select><img src="<%=BASEURL%>/images/botones/iconos/obligatorio.gif"></td>
        </tr>
			<tr class="fila">
				<td colspan="1">Cliente</td>
				<td><input name="client" type="text" class="textbox" <%=sw==true?"readonly":""%> id="codcli" size="10" maxlength="10" value="<%=sw==true?cliente:""%>"> <img src="<%=BASEURL%>/images/botones/iconos/obligatorio.gif"><%if(sw==false){%> <a onClick="window.open('<%=CONTROLLER%>?estado=Menu&accion=Cargar&pagina=consultasCliente.jsp&marco=no&carpeta=/consultas','','status=yes,scrollbars=no,width=550,height=150,resizable=yes')" style="cursor:hand"><img height="20" width="25" src="<%=BASEURL%>/images/botones/iconos/lupa.gif"></a><%}%></td>
			    <td>Descripci&oacute;n</td>
			    <td><input name="c_descripcion" type="text" class="textbox" id="c_descripcion" value="" size="45" maxlength="45"><img src="<%=BASEURL%>/images/botones/iconos/obligatorio.gif"></td>
			</tr>
      </table>
	  </td>
    </tr>
  </table>
  <%if(sw==true){%>
	  <input name="lupa" type="hidden" value="ok">
  <%}%>
  <p align="center">
<img src="<%=BASEURL%>/images/botones/aceptar.gif" style="cursor:hand" title="Agregar un producto" name="ingresar"  onclick="return valiTCamposLlenos(form1);" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);">&nbsp;
<img src="<%=BASEURL%>/images/botones/cancelar.gif" style="cursor:hand" name="regresar" title="Limpiar todos los registros" onMouseOver="botonOver(this);" onClick="form1.reset();" onMouseOut="botonOut(this);" >&nbsp;
<img src="<%=BASEURL%>/images/botones/salir.gif" style="cursor:hand" name="salir" title="Salir al Menu Principal" onClick="parent.close();" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);">&nbsp;
<%if(sw==true){%><img src="<%=BASEURL%>/images/botones/detalles.gif" style="cursor:hand" title="Buscar todos los productos" name="buscar"  onClick="window.location='<%=CONTROLLER%>?estado=Producto&accion=Serch&listar=True&sw=True&pagina=ProductoListarDiscrepancia.jsp&carpeta=/jsp/cumplidos/producto&c_codigo=&c_descripcion=&c_unidad=&codcli=<%=cliente%>';" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);"><%}%>
</p>  
  <% if(!mensaje.equals("")){%>
  <p><table border="2" align="center">
  <tr>
    <td><table width="100%"  border="1" align="center"  bordercolor="#F7F5F4" bgcolor="#FFFFFF">
      <tr>
        <td width="229" align="center" class="mensajes"><%=mensaje%></td>
        <td width="29" background="<%=BASEURL%>/images/cuadronaranja.JPG">&nbsp;</td>
        <td width="58">&nbsp;</td>
      </tr>
    </table></td>
  </tr>
</table>
</p>
 <%}%>
</form>
</div>
</body>
</html>
