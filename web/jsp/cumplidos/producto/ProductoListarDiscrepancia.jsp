<!--
- Autor : Ing. Jose de la rosa
- Date  : 12 de Diciembre de 2005
- Copyrigth Notice : Fintravalores S.A. S.A
-->
<%--
-@(#)
--Descripcion : Pagina JSP, que lista los productos
--%>

<%-- Declaracion de librerias--%>
<%@page contentType="text/html"%>
<%@page session="true"%>
<%@page errorPage="/error/ErrorPage.jsp"%>
<%@page import="java.util.*"%>
<%@page import="com.tsp.operation.model.*"%>
<%@page import="com.tsp.operation.model.beans.*"%>
<%@page import="com.tsp.util.*"%>
<%@include file="/WEB-INF/InitModel.jsp"%>
<%@taglib uri="http://jsptags.com/tags/navigation/pager" prefix="pg" %>
<html>
<head><title>Listar Producto</title>
<link href="<%= BASEURL %>/css/estilostsp.css" rel="stylesheet" type="text/css">
<script type='text/javascript' src="<%= BASEURL %>/js/boton.js"></script>
<script type='text/javascript' src="<%= BASEURL %>/js/reporte.js"></script>
</head>
<script>
function ir( variable ){
	window.opener.forma2.c_cod_producto.value= variable; 
	window.close();
}
</script>
<body onLoad="redimensionar();" onResize="redimensionar();">
<div id="capaSuperior" style="position:absolute; width:100%; height:100px; z-index:0; left: 0px; top: 0px;">
<jsp:include page="/WEB-INF/toptsp.jsp?encabezado=Listar Productos"/>
</div>

<div id="capaCentral" style="position:absolute; width:100%; height:85%; z-index:0; left: 0px; top: 100px; overflow: scroll;"> 
<%  
    String style = "simple";
    String position =  "bottom";
    String index =  "center";
    int maxPageItems = 10;
    int maxIndexPages = 10;
	
	String co = (String)session.getAttribute("codigo");
    String d = (String)session.getAttribute("descripcion");
    String u = (String)session.getAttribute("unidad");
	String cl = (String)session.getAttribute("cliente");
	model.productoService.searchDetalleProductos(co,d,u,cl);
    Vector vec = model.productoService.getProductos();
	if ( vec.size() >0 ){%>
<table width="750" border="2" align="center">
    <tr>
      <td>
	  	  <table width="100%" align="center">
              <tr>
                <td width="373" class="subtitulo1">&nbsp;Producto</td>
                <td width="427" class="barratitulo"><img src="<%=BASEURL%>/images/titulo.gif" width="32" height="20"></td>
              </tr>
        </table>	
	<table width="100%" border="1" bordercolor="#999999" bgcolor="#F7F5F4" align="center">
		<tr class="tblTitulo" align="center">
			<td width="72" nowrap >Codigo</td>
			<td width="337" nowrap >Descripcion</td>
			<td width="87" nowrap >Unidad</td>
			<td width="164" nowrap >Cliente</td>
		</tr>
		<pg:pager
			items="<%=vec.size()%>"
			index="<%= index %>"
			maxPageItems="<%= maxPageItems %>"
			maxIndexPages="<%= maxIndexPages %>"
			isOffset="<%= true %>"
			export="offset,currentPageNumber=pageNumber"
			scope="request">
	<%
		for (int i = offset.intValue(), l = Math.min(i + maxPageItems, vec.size()); i < l; i++){
			Producto p = (Producto) vec.elementAt(i);%>
			<pg:item>
				<tr class="<%=(i % 2 == 0 )?"filagris":"filaazul"%>" onMouseOver='cambiarColorMouse(this)'   style="cursor:hand" onClick="ir('<%=p.getCodigo()%>');" >
					<td align="center" class="bordereporte"><%=p.getCodigo()%></td>
					<td align="center" class="bordereporte"><%=p.getDescripcion()%></td>
					<td align="center" class="bordereporte">
					<%  Vector vecu2 = model.unidadService.listarUnidades(); 
						for(int j = 0; j<vecu2.size(); j++){	
							Unidad u2 = (Unidad) vecu2.elementAt(j);
                                                        if(u2.getCodigo().equals(p.getUnidad())){%> <%=u2.getDescripcion()%><%
							}
						}%></td>						
					<td align="center" class="bordereporte"><%model.clienteService.datosCliente (p.getCliente());
            												Cliente cli = model.clienteService.getCliente ();%><%=cli.getNomcli()%></td>						
				</tr>				
		  </pg:item>
	<%  }%>
		<tr class="pie">
			<td td height="20" colspan="10" nowrap align="center">          <pg:index>
					<jsp:include page="/WEB-INF/jsp/googlesinimg.jsp" flush="true"/>
			</pg:index>
			</td>
		</tr>
    	</pg:pager>        
	 </table>
	</td>
  </tr>
</table>
<br>
<%}else { %>
</p>
  <table border="2" align="center">
    <tr>
      <td><table width="410" height="73" border="1" align="center"  bordercolor="#F7F5F4" bgcolor="#FFFFFF">
          <tr>
            <td width="282" align="center" class="mensajes">Su b&uacute;squeda no arroj&oacute; resultados!</td>
            <td width="28" background="<%=BASEURL%>/images/cuadronaranja.JPG">&nbsp;</td>
            <td width="78">&nbsp;</td>
          </tr>
      </table></td>
    </tr>
  </table>
  <p>&nbsp; </p>
  <%}%>
 <table width="750" border="0" align="center">
    <tr>
      <td>
	  	<img src="<%=BASEURL%>/images/botones/salir.gif" style="cursor:hand" name="salir" title="Salir al Menu Principal" onClick="parent.close();" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);">
	  </td>
	</tr>
</table>
</div>
</body>
</html>
