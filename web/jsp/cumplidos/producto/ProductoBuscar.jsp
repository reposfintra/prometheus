<!--
- Autor : Ing. Jose de la rosa
- Date  : 9 de Octubre de 2005
- Copyrigth Notice : Fintravalores S.A. S.A
-->
<%--
-@(#)
--Descripcion : Pagina JSP, que busca un producto
--%>

<%-- Declaracion de librerias--%>
<%@page contentType="text/html"%>
<%@page session="true"%>
<%@page errorPage="/error/ErrorPage.jsp"%>
<%@page import="java.util.*"%>
<%@page import="com.tsp.operation.model.*"%>
<%@page import="com.tsp.operation.model.beans.*"%>
<%@page import="com.tsp.util.*"%>
<%@include file="/WEB-INF/InitModel.jsp"%>
<html>
<head><title>Buscar Producto</title>
<link href="<%= BASEURL %>/css/estilostsp.css" rel="stylesheet" type="text/css">
<script type='text/javascript' src="<%= BASEURL %>/js/boton.js"></script>
</head>
<body onLoad="redimensionar();" onResize="redimensionar();">
<div id="capaSuperior" style="position:absolute; width:100%; height:100px; z-index:0; left: 0px; top: 0px;">
<jsp:include page="/WEB-INF/toptsp.jsp?encabezado=Buscar Productos"/>
</div>

<div id="capaCentral" style="position:absolute; width:100%; height:85%; z-index:0; left: 0px; top: 100px; overflow: scroll;"> 
<form name="form2" method="post" action="<%=CONTROLLER%>?estado=Producto&accion=Serch&listar=True&sw=True&pagina=ProductoListar.jsp&carpeta=/jsp/cumplidos/producto">
 
    <table width="500" border="2" align="center">
      <tr>
        <td><table width="100%" align="center"  class="tablaInferior">
			  <tr>
				<td width="173" class="subtitulo1">Producto</td>
				<td width="205" class="barratitulo"><img src="<%=BASEURL%>/images/titulo.gif" width="32" height="20"></td>
			  </tr>
            <tr class="fila">
              <td width="171" align="left" valign="middle" >Codigo</td>
              <td width="197" valign="middle"><input name="c_codigo" type="text" class="textbox" id="c_codigo" size="15" maxlength="6"></td>
            </tr>
            <tr class="fila">
              <td width="171" align="left" valign="middle" >Descripci&oacute;n</td>
              <td width="197" valign="middle"><input name="c_descripcion" type="text" class="textbox" id="c_descripcion" size="15" maxlength="45"></td>
            </tr>
            <tr class="fila">
              <td width="171" valign="middle" >Unidad </td>
              <td valign="middle"><select name="c_unidad" id="c_unidad" class="textbox">
				  <option value=""></option>
				<%  Vector vecu2 = model.unidadService.listarUnidades(); 
				for(int i = 0; i<vecu2.size(); i++){	
					Unidad u2 = (Unidad) vecu2.elementAt(i);%>
					<option value="<%=u2.getCodigo()%>"><%=u2.getDescripcion()%> </option>
                                <%}%>
		  </select></td>
            </tr>
			<tr class="fila">
			<td>Cliente</td>
			<td><input name="codcli" type="text" class="textbox" id="codcli" size="15" maxlength="15"></td>
			</tr>
        </table></td>
      </tr>
    </table>
<p>
<div align="center"><img src="<%=BASEURL%>/images/botones/buscar.gif" style="cursor:hand" title="Buscar un producto" name="buscar"  onClick="form2.submit();" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);">&nbsp;
<img src="<%=BASEURL%>/images/botones/detalles.gif" style="cursor:hand" title="Buscar todos los productos" name="buscar"  onClick="window.location='<%=CONTROLLER%>?estado=Producto&accion=Serch&listar=True&sw=True&pagina=ProductoListar.jsp&carpeta=/jsp/cumplidos/producto&c_codigo=&c_descripcion=&c_unidad=&codcli='" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);">&nbsp;
<img src="<%=BASEURL%>/images/botones/salir.gif"  name="regresar" style="cursor:hand" title="Salir al Menu Principal" onMouseOver="botonOver(this);" onClick="parent.close();" onMouseOut="botonOut(this);" ></div>
</p>
</form>
</div>
</body>
</html>
