<!--
- Autor : Ing. Jose de la rosa
- Date  : 9 de Octubre de 2005
- Copyrigth Notice : Fintravalores S.A. S.A
-->
<%--
-@(#)
--Descripcion : Pagina JSP, que modifica un producto
--%>

<%-- Declaracion de librerias--%>
<%@page contentType="text/html"%>
<%@page session="true"%>
<%@page import="java.util.*"%>
<%@page import="com.tsp.operation.model.*"%>
<%@page import="com.tsp.operation.model.beans.*"%>
<%@page import="com.tsp.util.*"%>
<%@include file="/WEB-INF/InitModel.jsp"%>
<html>
<head>
<title>Modificar Producto</title>
<link href="<%= BASEURL %>/css/estilostsp.css" rel="stylesheet" type="text/css">
<script type='text/javascript' src="<%= BASEURL %>/js/boton.js"></script>
</head>
<script type='text/javascript' src="<%= BASEURL %>/js/validar.js"></script>
<body <%if(request.getParameter("reload")!=null){%>onLoad="parent.opener.location.reload();redimensionar();"<%}%> onResize="redimensionar();">
<div id="capaSuperior" style="position:absolute; width:100%; height:100px; z-index:0; left: 0px; top: 0px;">
<jsp:include page="/WEB-INF/toptsp.jsp?encabezado=Modificar Producto"/>
</div>

<div id="capaCentral" style="position:absolute; width:100%; height:85%; z-index:0; left: 0px; top: 100px; overflow: scroll;"> 
<%
    Usuario usuario = (Usuario) session.getAttribute("Usuario");
    Producto p = (Producto) model.productoService.getProducto();    
    String mensaje = (String) request.getAttribute("msg"); 
    Cliente cli = model.clienteService.getCliente ();
%>
<FORM name='form1' id='form1' method='POST' action='<%=CONTROLLER%>?estado=Producto&accion=Update'>
	<table width="500" border="2" align="center">
      <tr>
        <td>
    	<table width="100%" class="tablaInferior">
		<tr class="fila">
		  <td align="left" class="subtitulo1">&nbsp;Modificar Producto</td>
		  <td align="left" class="barratitulo"><img src="<%=BASEURL%>/images/titulo.gif" width="32" height="20"></td>
		</tr>	
        <tr class="fila">
            <td width="158">Codigo</td>
            <td width="238"><%=p.getCodigo()%><input name="c_codigo" type="hidden"  id="c_codigo" value="<%=p.getCodigo()%>"></td>
        </tr>
		<tr class="fila">
			<td colspan="1">Cliente</td>
			<td colspan="3"><input type="hidden" name="codcli" class="textbox" id="codcli" value="<%=p.getCliente()%>" size="10" maxlength="10"><%=model.clienteService.getCliente().getNomcli()%></td>
        </tr>				

        <tr class="fila">
            <td>Unidad</td>
            <td><select name="c_unidad" id="c_unidad" class="textbox">
			<%  Vector vecu2 = model.unidadService.listarUnidades();
			for(int i = 0; i<vecu2.size(); i++){
				Unidad u2 = (Unidad) vecu2.elementAt(i);
				%>
				<option value="<%=u2.getCodigo()%>" <% if(u2.getCodigo().equals(p.getUnidad())){%> selected <%}%> ><%=u2.getDescripcion()%> </option>
				<%
			}%>
		  </select></td>
        </tr>
        <tr class="fila">
            <td>Descripción</td>
            <td><input name="c_descripcion" type="text" class="textbox" id="c_descripcion" value="<%=p.getDescripcion()%>" size="45" maxlength="45"></td>
        </tr>      		         
    </table>
		</td>
      </tr>
    </table>
	<p>
	<div align="center"><img src="<%=BASEURL%>/images/botones/modificar.gif" style="cursor:hand" title="Modificar un producto" name="modificar" onclick="return valiTCamposLlenos(form1);" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);">&nbsp;
	<img src="<%=BASEURL%>/images/botones/anular.gif" style="cursor:hand" title="Anular un producto" name="anular"  onClick="window.location='<%=CONTROLLER%>?estado=Producto&accion=Anular&c_codigo=<%=p.getCodigo()%>&codcli=<%=p.getCliente()%>'" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);">&nbsp;
	<img src="<%=BASEURL%>/images/botones/salir.gif" style="cursor:hand" name="salir" title="Salir al Menu Principal" onMouseOver="botonOver(this);" onClick="parent.close();" onMouseOut="botonOut(this);" ></div>
	</p>
	<%  if(mensaje!=null){%>
		<table border="2" align="center">
		  <tr>
			<td><table width="100%"  border="1" align="center"  bordercolor="#F7F5F4" bgcolor="#FFFFFF">
			  <tr>
				<td width="229" align="center" class="mensajes"><%=mensaje%></td>
				<td width="29" background="<%=BASEURL%>/images/cuadronaranja.JPG">&nbsp;</td>
				<td width="58">&nbsp;</td>
			  </tr>
			</table></td>
		  </tr>
		</table>
	<%  }%>

</FORM>
</div>
</body>
</html>
