<!--
- Autor : Ing. Rodrigo Salazar
- Date  : 10 de Noviembre de 2005
- Copyrigth Notice : Fintravalores S.A. S.A
-->
<%--
-@(#)
--Descripcion : Pagina JSP, que maneja el ingreso cumplido de planillas, remesas
                y documentos cumplidos.
--%>

<%-- Declaracion de librerias--%>
<%@page contentType="text/html"%>
<%@page session="true"%>
<%@page import="java.util.*"%>
<%@page import="com.tsp.operation.model.*"%>
<%@page import="com.tsp.operation.model.beans.*"%>
<%@page import="com.tsp.operation.controller.*"%>
<%@page import="com.tsp.util.*"%>
<%@include file="/WEB-INF/InitModel.jsp"%>
<html>
    <head>
        <title>Cumplido</title>
        <link href="<%= BASEURL %>/css/estilostsp.css" rel="stylesheet" type="text/css">
        <script type='text/javascript' src="<%= BASEURL %>/js/boton.js"></script>
    </head>
    <script type='text/javascript' src="<%= BASEURL %>/js/validar.js"></script>
    <%-- Inicio Body --%>
    <body> 
    <% 
    String sw=request.getParameter("sw");
    String cod_pla="",cantidad="",coment="", unit_cont="",placa="",origen="",destino="", usuario="",fecha="", estado = "ACTIVO",plastatus="", conductor="";
    String discre = "";
    float cont=0;
	String vlr="0";
	String sj="";
	
    Cumplido pla_cumplido=null;
    List lst= (List) request.getAttribute("remesas");
    Planilla planilla = (Planilla)model.planillaService.getPlanilla();
	if ((sw.equals("1"))&&(planilla!=null)){
        System.out.println("IF"+sw);
        cod_pla += planilla.getNumpla();
		//model.planillaService.obtenerValorStandarPlanilla(cod_pla);
        cont += planilla.getPesoreal();
        unit_cont += planilla.getUnit_vlr();
        cantidad=""+cont;
        placa += planilla.getPlaveh();
        origen += planilla.getNomori();
        destino += planilla.getNomdest();
        conductor += planilla.getNomCond();
        usuario += planilla.getDespachador();
        fecha += String.valueOf(planilla.getFecpla()).substring(0,11);
        System.out.println("NUMPLA"+cod_pla);
        plastatus +=planilla.getReg_status();
        System.out.println("stado"+plastatus);
        if(plastatus.equals("C")){
        pla_cumplido=model.cumplidoService.buscarCumplido("001",cod_pla);				
        cantidad=""+pla_cumplido.getCantidad();
        coment = pla_cumplido.getComent();
        estado = "CUMPLIDO";		
		vlr = ""+planilla.getVlrpla();
		sj = planilla.getSj();
        }
        System.out.println("DISCRE"+discre);
        if(request.getParameter("discre").equals("true")){
        discre = " - Con Discrepancia";
        }
		
    }
	//
	
    %>
    <div id="capaSuperior" style="position:absolute; width:100%; height:100px; z-index:0; left: 0px; top: 0px;">
    <jsp:include page="/WEB-INF/toptsp.jsp?encabezado=Cumplido"/>
    </div>

    <div id="capaCentral" style="position:absolute; width:100%; height:85%; z-index:0; left: 0px; top: 100px; overflow: scroll;">    
    <%-- Tabla cumplir planilla --%>
    <table width="800" border="2" align="center"> 
        <tr> 
            <td > 
            <div align="center">
            <table width="100%" class="tablaInferior"> 
                <tr> 
                    <td height="22" colspan=2 class="subtitulo1">Cumplir por Planilla</td> 
                    <td width="368" class="barratitulo"><img src="<%=BASEURL%>/images/titulo.gif" width="32" height="20"></td>
                </tr> 
            </table>
            <table width="100%" class="tablaInferior"> 
            <%-- Formulario de de busqueda de planilla a cumplido --%>
            <form action="<%=CONTROLLER%>?estado=Cumplido&accion=Cargar&carpeta=cumplido&pagina=Cumplidos.jsp&sw=1"  method="post" name="forma" id="forma" > 
            <tr class="fila" > 
                <td colspan="7"><table width="100%"  border="0" cellspacing="0" cellpadding="0">
                <tr class="fila">
                <td width="9%"><input name="c_tdoc" type="radio" value="001" checked>Planilla </td>
                <td width="11%"><input type="radio" name="c_tdoc" value="002">Remesa </td>
                <td width="59%"><input name="c_cod" type="text"  class="textbox" size="10" value="<%= cod_pla %>" width="40">
                    <img src="<%=BASEURL%>/images/botones/iconos/lupa.gif" width="20" height="16" style="cursor:hand" title="Buscar" name="buscar"  onClick="return TCamposLlenos();"></td>
                <td width="21%"><%if(!cod_pla.equals("")){%><a href="<%=CONTROLLER%>?estado=Sancion&accion=Serch&listar=True&sw=True&c_numpla=<%=cod_pla%>&c_cod_sancion=&c_tipo_sancion=" target="_blank" class="Simulacion_Hiper"  >Sanciones</a><br>
				&nbsp;<a href="javascript:abrir('<%=CONTROLLER%>','<%=cod_pla%>');"  class="Simulacion_Hiper">Registrar Discrepancia</a><%}%></td>
            </tr>
            </table></td> 
        </tr> 
            </form>
                <%-- Formulario de cumplir una planilla a cumplido --%>
                <form action=""  method="post" name="form1" id="form1" > 
                <tr class="fila"> 
                    <td width="64" >Planilla:</td> 
                    <td width="113" ><%= cod_pla %><input name="c_numpla" type="hidden" value="<%= cod_pla %>" size="10"  readonly="">
                        <%if(!cod_pla.equals("")){%>
				<img src="<%=BASEURL%>/images/botones/iconos/documento.gif" alt="Adjuntar Imagen" width="15" height="15"
				onClick="window.open('<%=CONTROLLER%>?estado=Imagen&accion=Control&documento=<%=cod_pla%>&actividad=014&tipoDocumento=001', '','width=680,height=600,scrollbars=yes,resizable=yes,top=10,left=65,status=yes');" style="cursor:hand">
                        <%}%>
                    </td>
                    <td width="50" >Placa: </td>
                    <td width="134" ><%= placa %>
                        <input name="c_placa" type="hidden" id="c_placa" value="<%= placa %>"></td>
                    <td>Conductor:</td> 
                    <td><%= conductor %>
                        <input name="c_conductor" type="hidden" value="<%= conductor %>">
                    </td> 
                </tr> 
                <tr class="fila"> 
                    <td>Origen:</td> 
                    <td colspan="3"><%= origen %><input name="c_origen" type="hidden" id="c_origen" value="<%= origen %>"></td>
                    <td width="174">Fecha de Elaboraci&oacute;n:</td> 
                    <td width="249"><%= fecha %>
                    <input name="c_felab" type="hidden" id="c_felab" value="<%= fecha %>"></td>
                </tr> 
                <tr class="fila"> 
                    <td>Destino:</td>
                    <td colspan="3"><%= destino %><input name="c_destino" type="hidden" id="c_destino" value="<%= destino %>"></td>
                    <td>Elaborada por:</td> 
                    <td><%= usuario %>
                    <input name="c_usuario" type="hidden" id="c_usuario" value="<%= usuario %>"></td>
                </tr> 
                <tr class="fila"> 
                    <td colspan="2">Cantidad despachada: </td> 
                    <td colspan="2"><%= cont %>
                    <input name="c_cantidad_desp" type="hidden" value="<%= cont %>"> <%= unit_cont %>
                    <input name="c_unidad" type="hidden" id="c_unidad"  value="<%= unit_cont %>"></td>
                    <td>Cantidad entregada: </td> 
                    <td>
                        <%if(unit_cont.equals("VIA") || plastatus.equals("C")){%><%= cantidad %><input name="c_cantidad" id="c_cantidad" type="hidden" value="<%= cantidad %>"><%}else{%><input name="c_cantidad" id="c_cantidad" type="text"  class="textbox"  onKeyPress="soloDigitos(event,'decOK')" value="<%= cantidad %>" size="10" maxlength="10"><%}%> <%= unit_cont %>
                        <input name="c_unidad_ton" type="hidden" id="c_unidad_ton"  value="<%= unit_cont %>">
                    </td>
                </tr>
                <tr class="fila">
                    <td  colspan="4">Comentario:</td>
                    <td >Estado:</td>
                    <td ><div align="left"><%=estado %><a href="javascript:abrir_discrepancia('<%=CONTROLLER%>','<%=cod_pla%>');" class="Simulacion_Hiper"><%=discre %></a></div></td>
                </tr>
                <tr class="fila">
                    <td colspan="6" align="center"><textarea name="c_coment" cols="130" class="textbox" rows="4" id="coment"><%= coment%></textarea></td>
                </tr>
                    <%if(!cod_pla.equals("")){%>			
                            <tr class="fila">
                                    <td colspan="4" align="center"><a style="cursor:hand " class="Simulacion_Hiper" onClick="window.open('<%=BASEURL%>/colpapel/extrafletes.jsp?sj=<%=sj%>&tipo=CUMPLIDO&voc=<%=vlr%>&extraflete='+form1.extraflete.value,'Extrafletes','height=400,width=800,dependent=yes,resizable=yes,scrollbars=yes,status=yes');" >Ingresar / Modificar Extrafletes</a>
									<BR><a style="cursor:hand " class="Simulacion_Hiper" onClick="window.open('<%=BASEURL%>/colpapel/costosRembolsables.jsp?sj=<%=sj%>&voc=<%=vlr%>&cr='+form1.cr.value,'Costos','height=400,width=800,dependent=yes,resizable=yes,scrollbars=yes,status=yes');" >Ingresar / Modificar Costos Reembolsables</a></td>
                                    <td>Valor total en extrafletes </td>
                                    <td><input name="extraflete" type="text" id="extraflete" value="0" size="12" readonly> <input name="cr" type="hidden" id="cr" value=""></td>                
                            </tr>		
							<tr class="fila">
									<td colspan="6" align="center"><a href="javascript:liquidarOC('<%=CONTROLLER%>','<%=cod_pla%>');"  class="Simulacion_Hiper">Liquidar OC</a></td>
							</tr>
                            <%
                    }%>			
                    <tr class="fila">
                    <td height="28" colspan="6"><div align="center">
                        <%if(!cod_pla.equals("")){%>
                            <%if(!plastatus.equals("C")){%>
                                <img src="<%=BASEURL%>/images/botones/cumplirPlanilla.gif" style="cursor:hand" title="Cumplir una planilla" name="ingresar"  onclick="if(form1.c_cantidad.value=='.'||form1.c_cantidad.value==''){form1.c_cantidad.value=0;}validarMasDeUnPunto('<%=CONTROLLER%>?estado=Cumplido&accion=Insert&tdoc=001','c_cantidad');" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);">
                            <%}
                            else{%>
                                <img src="<%=BASEURL%>/images/botones/cumplirPlanillaDisable.gif" title="Cumplir una planilla" name="ingresar">
                            <%}%>
                        <%}%></div>
                    </td>
                </tr> 	      
                <%if(sw.equals("2")){%>
                    <table border="2" align="center">
                        <tr>
                            <td>
                                <table width="100%"  border="1" align="center"  bordercolor="#F7F5F4" bgcolor="#FFFFFF">
                                    <tr>
                                        <td width="229" align="center" class="mensajes"><%= "El cumplido fue almacenado satisfactoriamente" %></td>
                                        <td width="29"><img src="<%=BASEURL%>/images/cuadronaranja.JPG"></td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                    </table>		      
                <%}%>
            </table>
        <div align="center"></div></td>
        </tr> 
        <%if ((sw.equals("1"))&&(planilla!=null)){%>
            <tr> 
                <td> 
                    <div align="center">
                        <table width="100%" class="tablaInferior"> 
                            <tr> 
                                <td height="22" colspan=2 class="subtitulo1"><p align="left">Datos Remesa </p></td> 
                                <td width="358" class="barratitulo"><img src="<%=BASEURL%>/images/titulo.gif" width="32" height="20"></td> 
                            </tr> 
                        </table>
                        <table width="100%" border="1" bordercolor="#999999" bgcolor="#F7F5F4" align="center"> 
                            <tr class="tblTitulo" align="center"> 
                                <td   align="center">Remesa</td> 
                                <td   align="center">Cliente</td> 
                                <td   align="center">Cantidad</td> 
                                <td   align="center">Unidades</td> 
                                <td  align="center">Recibido</td> 
                                <td   align="center">Estado</td> 

                            </tr> 
                            <% if (lst != null){
                                Remesa rem;  
                                Cumplido rem_cumplido;
                                String cant="";
                                String desab = "";
								int numRemesas = lst.size();								
                                for (int i = 0; i < lst.size(); i++){	
                                    desab = "f";
                                    coment = "";  
                                    rem = (Remesa)lst.get(i);
                                    estado=rem.getReg_status();
                                    rem_cumplido = null;
                                    cant = ""+rem.getPesoReal();
                                    if(estado.equals("C")){
                                            rem_cumplido=model.cumplidoService.buscarCumplido("002",rem.getNumrem());
                                            cant=""+rem_cumplido.getCantidad();
                                            coment = rem_cumplido.getComent();
                                    }%>
                                    <input type="hidden" value="<%= rem.getNumrem()%>" name="c_numrem<%=i%>" >
                                    <tr class="fila"> 
                                        <td  width="69" align="center" class="bordereporte"><%= rem.getNumrem()%></td> 
                                        <td  width="275" align="center" class="bordereporte"><%= rem.getCliente()%></td> 
                                        <td  width="89" align="center" class="bordereporte"><%= cant%><input type="hidden" name="c_cantidad_rem<%=i%>" value="<%= cant%>"></td> 
                                        <td  width="116" align="center" class="bordereporte"><%= rem.getUnitOfWork()%></td> 
                                        <td width="105" align="center" class="bordereporte"><input name="c_cargrem<%=i%>" id="c_cargrem<%=i%>" onKeyPress="soloDigitos(event,'decOK')" class="textbox" type="text"  value="<%=cant %>" size="7">
                                        </td> 
                                        <td  width="119" align="center" class="bordereporte"><% if(estado.equals("C")){out.print("CUMPLIDO");}else{out.print("ACTIVO");}%></td> 
                                            <input type="hidden" name="c_unidad<%=i%>" value="<%= rem.getUnitOfWork()%>">
                                    </tr>
                                    <tr class="fila">
                                        <td height="14" colspan="4" align="center" class="bordereporte">
                                            <textarea name="c_coment<%=i%>" class="textbox" cols="90" rows="3" ><%= coment%></textarea></td>
                                        <td height="30" colspan="2" rowspan="2" align="center" class="bordereporte"> 
                                            <% if(estado.equals("C")){%>
                                                    <img src="<%=BASEURL%>/images/botones/cumplirRemesaDisable.gif" title="Cumplir una Remesa" name="ingresar">
                                            <%}else{%>
                                                    <img src="<%=BASEURL%>/images/botones/cumplirRemesa.gif" style="cursor:hand" title="Cumplir una remesa" name="ingresarremesa"  onclick="validarCumplirRemesa('<%=CONTROLLER%>?estado=Cumplido&accion=Insert&tdoc=002&rem=<%=i%>','c_cargrem','<%=i%>');" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);">
                                            <%}%>
                                        </td>
                                    </tr>
                                    <tr class="fila">
                                      <td height="14" colspan="4" align="left" class="bordereporte"><a href="javascript:abrirDocumentos('<%=CONTROLLER%>','<%=cod_pla%>','<%=rem.getNumrem()%>','<%=model.remesaService.remesaStandarJob(rem.getNumrem())%>');" class="Simulacion_Hiper">Documentos Cumplidos</a></td>
                                    </tr>
                                <%}
                            }%>
                        </table>
                    </div>
                </td> 
            </tr> 
        <%}%>
    </table> 
    </form>
	<table width="800" align="center"  border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td><img src="<%=BASEURL%>/images/botones/salir.gif" style="cursor:hand" title="Salir" name="i_salir"  onclick="parent.close();" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);"></td>
  </tr>
</table>
     <%if((sw.equals("1"))&&(planilla==null)){%>
        <table width="600" border="2" align="center">
            <tr>
                <td>
                    <table width="100%"  border="1" align="center"  bordercolor="#F7F5F4" bgcolor="#FFFFFF">
                        <tr>
                            <td width="80%" align="center" class="mensajes"><%= "No se encontro una planilla con ese codigo" %></td>
                            <td width="20%"><img src="<%=BASEURL%>/images/cuadronaranja.JPG"></td>
                        </tr>
                    </table>
                </td>
            </tr>
        </table>		      
    <%}%>
    <%String mensaje = (String) request.getAttribute("mensaje"); 
    if(mensaje!=null){%>
    <br>
    <table border="2" align="center">
        <tr>
            <td><table width="100%"  border="1" align="center"  bordercolor="#F7F5F4" bgcolor="#FFFFFF">
                <tr>
                    <td width="229" align="center" class="mensajes"><%=mensaje%></td>
                    <td width="29" background="<%=BASEURL%>/images/cuadronaranja.JPG">&nbsp;</td>
                    <td width="58">&nbsp;</td>
                </tr>
            </table></td>
        </tr>
    </table>
    <%  } %>	
    </div>
    </body>
</html>
<script>	
    function AdjuntarImagenes(BASEURL,acti,tipo) {
    var pag="";	
    pag = "/imagen/Manejo.jsp?documento="+forma.c_numpla.value+"&actividad="+acti+"&tipoDocumento="+tipo;
    window.open(BASEURL+"/Marcostsp.jsp?encabezado=Agregar Imagen&dir="+pag,'Trafico','width=680,height=350,scrollbars=no,resizable=yes,top=10,left=65');
    }
    function abrir(CONTROLLER,codP) {
        window.open(CONTROLLER+'?estado=Informacion&accion=Planilla_discrepancia&tipo=I&msg=&numpla='+codP ,'','status=no,scrollbars=no,width=780,height=650,resizable=yes');
    }
    function liquidarOC(CONTROLLER,codP) {
        window.open(CONTROLLER+'?estado=Liquidar&accion=OC&c_oc='+codP ,'','status=no,scrollbars=no,width=780,height=500,resizable=yes');
    }
    function abrirDocumentos(CONTROLLER,codpla,numrem,standar){
            window.open(CONTROLLER+"?estado=Cumplido_doc&accion=Cargar&rem="+numrem+"&pla="+codpla,'','status=no,scrollbars=no,width=780,height=450,resizable=yes');
			//CONTROLLER+'?estado=Menu&accion=Cargar&carpeta=/jsp/cumplidos/cumplido&marco=no&pagina=cumplido_DocumentosInsertar.jsp?sw=1&numpla='+codpla+'&numrem='+numrem+'&standar='+standar,'','status=no,scrollbars=no,width=780,height=450,resizable=yes');
    }
	function abrir_discrepancia(CONTROLLER,codP){
		window.open(CONTROLLER+'?estado=Discrepancia_listar_todas&accion=Serch&listar=True&planilla='+codP,'','status=no,scrollbars=no,width=1000,height=650,resizable=yes');
	}
</script>
