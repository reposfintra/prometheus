<!--  
	 - Author(s)       :      Jose de la Rosa
	 - Description     :      AYUDA FUNCIONAL - Buscar Acuerdos especiales
	 - Date            :      08/06/2006 
	 - Copyright Notice:      TRANSPORTES SANCHEZ POLO S.A.
-->
<%@ include file="/WEB-INF/InitModel.jsp"%>
<HTML>
<HEAD>
<TITLE>AYUDA FUNCIONAL - Cumplidos</TITLE>
<META http-equiv=Content-Type content="text/html; charset=windows-1252">
<link href="../css/estilostsp.css" rel="stylesheet" type="text/css"> 
<link href="<%=BASEURL%>/css/estilostsp.css" rel="stylesheet" type="text/css">
<script type='text/javascript' src="<%= BASEURL %>/js/boton.js"></script> 
</HEAD>
<BODY> 
<% String BASEIMG_ACU = BASEURL +"/images/ayuda/equipos/acuerdo_especial/"; %>
<% String BASEIMG = BASEURL +"/images/ayuda/cumplido/cumplido/"; %>
  <table width="95%"  border="2" align="center">
    <tr>
      <td height="117" >
        <table width="100%" border="0" align="center">
          <tr  class="subtitulo">
            <td height="20"><div align="center">MANUAL DE CUMPLIDOS </div></td>
          </tr>
          <tr class="subtitulo1">
            <td>Descripci&oacute;n del funcionamiento del programa para realizar Cumplidos </td>
          </tr>
          <tr>
            <td  class="ayudaHtmlTexto"><div align="center">
              <p>&nbsp;</p>
              </div></td>
          </tr>
            <tr>
            <td  class="ayudaHtmlTexto"><p class="ayudaHtmlTexto">En la siguiente pantalla tiene dos forma de busqueda,  una por planilla  que se utiliza presionando en el radio de planilla y luego presionando el boton de la LUPA, en esta busqueda aparecera como se ilustra en la Imagen1.</p>
          </td>
          </tr>
            <tr>
            <td  class="ayudaHtmlTexto"><div align="center">
              <p><IMG src="<%=BASEIMG%>imagenCumplidos.JPG" border=0 ></p>
              <p><strong>Imagen1</strong></p>
            </div></td>
          </tr>
		  
		<tr>
		<td  class="ayudaHtmlTexto"><p class="ayudaHtmlTexto">&nbsp;</p>
		<p class="ayudaHtmlTexto">Si no existen datos correspondientes a la busqueda de la planilla aparecer&aacute; como se ilustra en la Imagen2. </p>
		</td>
		</tr>
		<tr>
		<td  class="ayudaHtmlTexto"><div align="center">
		  <p><IMG src="<%=BASEIMG%>MensajeNoExistePlanilla.JPG" border=0 ></p>
		  <p><strong>Imagen2</strong></p>
		</div></td>
		</tr>		  
		<tr>
		<td  class="ayudaHtmlTexto"><p class="ayudaHtmlTexto">&nbsp;</p>
		<p class="ayudaHtmlTexto">Si  cumple la planilla aparecer&aacute; como se ilustra en la Imagen3. </p>
		</td>
		</tr>
		<tr>
		<td  class="ayudaHtmlTexto"><div align="center">
		  <p><IMG src="<%=BASEIMG%>MensajePlanillaCumplida.JPG" border=0 ></p>
		  <p><strong>Imagen3</strong></p>
		</div></td>
		</tr>				  
		  
		  
		<tr>
		<td  class="ayudaHtmlTexto"><p class="ayudaHtmlTexto">&nbsp;</p>
		<p class="ayudaHtmlTexto">La otra busqueda es por remesas, que utiliza presioando el radio donde aparece &quot;remesa&quot; y luego presioando el boton de la LUPA como se ilustra en la Imagen1, el resultado de esta busqueda aparecera como se ilustra en la Imagen4. Al presionar en cualquier campo de la lista de planillas asociadas a la remesa el numero de planilla aparecer&aacute; en la Imagen1 con sus datos correspondientes. </p>
		</td>
		</tr>
		<tr>
		<td  class="ayudaHtmlTexto"><div align="center">
		  <p><IMG src="<%=BASEIMG%>listadoCumplidos.JPG" border=0 ></p>
		  <p><strong>Imagen4</strong></p>
		</div></td>
		</tr>
		
		<tr>
		<td  class="ayudaHtmlTexto"><p class="ayudaHtmlTexto">&nbsp;</p>
		<p class="ayudaHtmlTexto">Si no existen datos correspondientes a la  busqueda de la remesa  aparecer&aacute; como se ilustra en la Imagen5. </p>
		</td>
		</tr>
		<tr>
		<td  class="ayudaHtmlTexto"><div align="center">
		  <p><IMG src="<%=BASEIMG_ACU%>ErrorListado.JPG" border=0 ></p>
		  <p><strong>Imagen5</strong></p>
		</div></td>
		</tr>
		<tr>
		<td  class="ayudaHtmlTexto"><p class="ayudaHtmlTexto">&nbsp;</p>
		<p class="ayudaHtmlTexto">Si  cumple la remesa aparecer&aacute; como se ilustra en la Imagen6. Y si el cumplido de la remesa tiene diferencia de la cantidad con el recibido aparecer&aacute; como se ilustra en la Imagen7. </p>
		</td>
		</tr>
		<tr>
		<td  class="ayudaHtmlTexto"><div align="center">
		  <p><IMG src="<%=BASEIMG%>MensajeRemesaCumplida.JPG" border=0 ></p>
		  <p><strong>Imagen6</strong></p>
		  <p><IMG src="<%=BASEIMG%>MensajeDiferenciaRemesas.JPG" border=0 ></p>
		  <p><strong>Imagen7</strong></p>
		</div></td>
		</tr>			
		<tr>
		<td  class="ayudaHtmlTexto"><p class="ayudaHtmlTexto">&nbsp;</p>
		<p class="ayudaHtmlTexto">Si no llena el campo de busqueda y presiona el boton LUPA aparecer&aacute; como se ilustra en la Imagen8. </p>
		</td>
		</tr>
		<tr>
		<td  class="ayudaHtmlTexto"><div align="center">
		  <p><IMG src="<%=BASEIMG_ACU%>MensajeErrorCamposLLenos.JPG" border=0 ></p>
		  <p><strong>Imagen8</strong></p>
		</div></td>
		</tr>
				
      </table></td>
    </tr>
  </table>
  <p></p>
<center>
<img src = "<%=BASEURL%>/images/botones/salir.gif" style = "cursor:hand" name = "imgsalir" onClick = "parent.close();" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);">
</center>
</BODY>
</HTML>
