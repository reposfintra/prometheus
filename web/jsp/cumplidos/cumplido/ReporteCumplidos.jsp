<!--
- Autor : Ing. Rodrigo Salazar
- Date  : 22 de Noviembre de 2005
- Copyrigth Notice : Fintravalores S.A. S.A
-->
<%--
-@(#)
--Descripcion : Pagina JSP, que maneja la migracion de cumplidos dada una fecha
--%>

<%-- Declaracion de librerias--%>
<%@page contentType="text/html"%>
<%@page session="true"%>
<%@page errorPage="../error/ErrorPage.jsp"%>
<%@page import="java.util.*"%>
<%@page import="com.tsp.operation.model.*"%>
<%@page import="com.tsp.util.*"%>
<%@page import="com.tsp.operation.model.beans.*, java.util.*"%>
<%@include file="/WEB-INF/InitModel.jsp"%>

<html>
    <head>
        <title>Migrar Cumplidos</title>
        <link href="<%= BASEURL %>/css/estilostsp.css" rel="stylesheet" type="text/css">
        <script type='text/javascript' src="<%= BASEURL %>/js/boton.js"></script>
<% String msg = (String) request.getParameter("msg");%>
    </head>
    <%-- Inicio Body --%>
    <body onLoad="redimensionar();" onResize="redimensionar();">
        <div id="capaSuperior" style="position:absolute; width:100%; height:100px; z-index:0; left: 0px; top: 0px;">
            <jsp:include page="/WEB-INF/toptsp.jsp?encabezado=Migración Cumplidos"/>
        </div>
	
        <div id="capaCentral" style="position:absolute; width:100%; height:85%; z-index:0; left: 0px; top: 100px; overflow: scroll;">  
            <%-- Inicio Formulario --%>		
            <form name="forma" method="post" >
            <%-- Inicio Tabla Principal --%>		
            <table width="390" border="2" align="center">
                <tr>
                    <td>
                        <%-- Inicio Tabla Secundaria --%>
                        <table width="100%" class="tablaInferior">
                            <tr>
                                <td width="50%" class="subtitulo1">&nbsp;Migración cumplido</td>
                                <td width="50%" class="barratitulo"><img src="<%=BASEURL%>/images/titulo.gif" width="32" height="20"></td>
                            </tr>
                            <tr class="fila">
                                <td width="232" align="left" > &nbsp;&nbsp;&nbsp;&nbsp;Fecha Inicio </td>
                              <td width="235" valign="middle" >
                                <input name="c_fecha" type="text" readonly class="textbox" id="c_fecha" size="12">
								<a href="javascript:void(0)" onclick="if(self.gfPop)gfPop.fPopCalendar(document.forma.c_fecha);return false;" HIDEFOCUS>
								<img name="popcal" align="absmiddle" src="<%=BASEURL%>/js/Calendario/cal.gif" border="0" alt=""></a></td>
                            </tr> 		
                        </table>
                    </td>
                </tr>
            </table>
            <p align="center">
            <img src="<%=BASEURL%>/images/botones/exportarExcel.gif" title="Migrar Cumplidos" style="cursor:hand" name="migrar"  onclick="forma.action='<%=CONTROLLER%>?estado=Reporte_cumplido&accion=Migracion';if (c_fecha.value == ''){alert('Debe indicar la fecha para poder continuar');}else{forma.submit();}" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);">&nbsp;
            <img src="<%=BASEURL%>/images/botones/salir.gif" style="cursor:hand" name="salir" title="Salir al Menu Principal" onMouseOver="botonOver(this);" onClick="parent.close();" onMouseOut="botonOut(this);" >
            </p>
            </form>
            <% if(!msg.equals("")){%>
                <br>
                <table border="2" align="center">
                    <tr>
                        <td>
                            <table width="100%"  border="1" align="center"  bordercolor="#F7F5F4" bgcolor="#FFFFFF">
                                <tr>
                                    <td width="229" align="center" class="mensajes"><%=msg%></td>
                                    <td width="29" background="<%=BASEURL%>/images/cuadronaranja.JPG">&nbsp;</td>
                                    <td width="58">&nbsp;</td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
            <%}%>
        </div>
		<iframe width=188 height=166 name="gToday:normal:agenda.js:gfPop:plugins.js" id="gToday:normal:agenda.js:gfPop:plugins.js" src="<%=BASEURL%>/js/Calendario/ipopeng.htm" scrolling="no" frameborder="0" style="visibility:visible; z-index:999; position:absolute; left:-500px; top:0px;"> </iframe>
    </body>
</html>
