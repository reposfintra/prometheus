<!--
- Autor : Ing. Rodrigo Salazar
- Date  : 10 de Noviembre de 2005
- Copyrigth Notice : Fintravalores S.A. S.A
-->
<%--
-@(#)
--Descripcion : Pagina JSP, que maneja las listas de las remesas de cumplidos .
--%>

<%-- Declaracion de librerias--%>
<%@page contentType="text/html"%>
<%@page session="true"%>
<%@page import="java.util.*"%>
<%@page import="com.tsp.operation.model.*"%>
<%@page import="com.tsp.operation.model.beans.*"%>
<%@page import="com.tsp.operation.controller.*"%>
<%@page import="com.tsp.util.*"%>
<%@include file="/WEB-INF/InitModel.jsp"%>
<html>
    <head>
        <title>Untitled Document</title>
        <link href="<%= BASEURL %>/css/estilostsp.css" rel="stylesheet" type="text/css">
        <script type='text/javascript' src="<%= BASEURL %>/js/boton.js"></script>
        <script type='text/javascript' src="<%= BASEURL %>/js/reporte.js"></script>
    </head>
    <%-- Inicio Body --%>
    <body> 
        <div id="capaSuperior" style="position:absolute; width:100%; height:100px; z-index:0; left: 0px; top: 0px;">
            <jsp:include page="/WEB-INF/toptsp.jsp?encabezado=Lista Remesas"/>
        </div>

        <div id="capaCentral" style="position:absolute; width:100%; height:85%; z-index:0; left: 0px; top: 100px; overflow: scroll;">    
            <% List lst =(List) request.getAttribute("planillas");
            if (lst.size()>0){
            %>
                <%-- Inicio Tabla Principal --%>
                <table width="600" border="2" align="center">
                    <tr>
                        <td>
                        <%-- Inicio Tabla Cabecera --%>
                        <table width="100%" align="center">
                            <tr>
                                <td height="50%" colspan=2 class="subtitulo1"><p align="left">Datos Remesa </p></td>
                                <td width="50%" class="barratitulo"><img src="<%=BASEURL%>/images/titulo.gif" width="32" height="20"></td>
                            </tr>
                        </table>
                        <%-- Inicio Tabla Secundaria --%>
                        <table width="100%" border="1" bordercolor="#999999" bgcolor="#F7F5F4" align="center">
                            <tr class="tblTitulo" align="center">
                                <td  width="110">Planilla</td>
                                <td  width="229">Origen</td>
                                <td  width="143">Destino</td>
                                <td  width="92">Usuario</td>
                            </tr>
                            <% 
                            Planilla planilla;  
                            for (int i = 0; i < lst.size(); i++){		  
                                planilla = (Planilla)lst.get(i);
                                %>
                                <tr class="<%=(i % 2 == 0 )?"filagris":"filaazul"%>" onMouseOver='cambiarColorMouse(this)'   style="cursor:hand" onClick="parent.location='<%=CONTROLLER%>?estado=Cumplido&accion=Cargar&carpeta=cumplido&pagina=Cumplidos.jsp&sw=1&c_cod=<%=planilla.getNumpla()%>&c_tdoc=001'">
                                    <td  width="110" align="center" class="bordereporte"><%= planilla.getNumpla()%></td>
                                    <td  width="229" align="center" class="bordereporte"><%= model.ciudadService.obtenerNombreCiudad(planilla.getOripla())%></td>
                                    <td  width="143" align="center" class="bordereporte"><%= model.ciudadService.obtenerNombreCiudad(planilla.getDespla())%></td>
                                    <td  width="92" align="center" class="bordereporte"><%= planilla.getDespachador()%></td>
                                </tr>
                            <%}%>
                        </table>
                        </td>
                    </tr>
                </table>
                <br>
            <%}else{%>
                <table border="2" align="center">
                    <tr>
                        <td>
                            <table width="410" height="73" border="1" align="center"  bordercolor="#F7F5F4" bgcolor="#FFFFFF">
                                <tr>
                                    <td width="282" align="center" class="mensajes">Su b&uacute;squeda no arroj&oacute; resultados!</td>
                                    <td width="28" background="<%=BASEURL%>/images/cuadronaranja.JPG">&nbsp;</td>
                                    <td width="78">&nbsp;</td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table> 
                <br>
            <%}%>
            <table width="600" border="0" align="center">
                <tr>
                    <td>
                        <img src="<%=BASEURL%>/images/botones/regresar.gif" style="cursor:hand" title="Volver" name="buscar"  onClick="window.location='<%=CONTROLLER%>?estado=Menu&accion=Cargar&carpeta=/jsp/cumplidos/cumplido&marco=no&pagina=Cumplidos.jsp?sw=0'" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);">
                    </td>
                </tr>
            </table>   
        </div>
    </body>
</html>
