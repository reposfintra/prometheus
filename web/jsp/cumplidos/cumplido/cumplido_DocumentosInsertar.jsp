<!--
- Autor : Ing. Jose de la rosa
- Date  : 10 de Noviembre de 2005
- Copyrigth Notice : Fintravalores S.A. S.A
-->
<%--
-@(#)
--Descripcion : Pagina JSP, que maneja el ingreso de los documentos cumplidos a l programa de cumplidos
--%>

<%-- Declaracion de librerias--%>
<%String sw = request.getParameter("sw");
if(sw.equals("0")){%>
<script>
    parent.close();
</script><%}%>
<%@page contentType="text/html"%>
<%@page session="true"%>
<%@page errorPage="/error/ErrorPage.jsp"%>
<%@page import="java.util.*"%>
<%@page import="com.tsp.operation.model.beans.*"%>
<%@page import="com.tsp.util.*"%>
<%@page import="com.tsp.operation.model.*"%>
<%@ include file="/WEB-INF/InitModel.jsp"%>
<%
String standar = "";
standar = request.getParameter("standar");
model.stdjob_tbldocService.list_Stdjob(standar); 
List Lista = model.stdjob_tbldocService.getList();
String numpla = "";
String numrem = "";
numpla = request.getParameter("numpla");
numrem = request.getParameter("numrem");
if(Lista.size()==0) { response.sendRedirect(CONTROLLER+"?estado=Cumplido_doc&accion=Cargar&rem="+numrem+"&pla="+numpla); }
%>

<html>
    <head>
        <title>Cumplidos Documentos</title>
        <link href="<%= BASEURL %>/css/estilostsp.css" rel="stylesheet" type="text/css">
        <script type='text/javascript' src="<%= BASEURL %>/js/boton.js"></script>
        <script type='text/javascript' src="<%= BASEURL %>/js/reporte.js"></script>
        <script>
            controler = '<%=CONTROLLER%>';
            function SelAll(){
                for(i=0;i<FormularioListado.length;i++)
                    FormularioListado.elements[i].checked=FormularioListado.All.checked;
            }
            function ActAll(){
                FormularioListado.All.checked = true;
                for(i=1;i<FormularioListado.length;i++)	
                    if (!FormularioListado.elements[i].checked){
                        FormularioListado.All.checked = false;
                        break;
                    }
            }      
            function verificar_checkbox_c(){    
                for(i=0;i<=FormularioListado.length-1;i++)    
                    if(FormularioListado.elements[i].type=='checkbox' && FormularioListado.elements[i].checked)       
                        return true;	   			
                    alert('Debe seleccionar por lo menos un item...')                                    
                    return false;
            }
        </script>
    </head>
    <%-- Inicio Body --%>
    <body onLoad="redimensionar();" onResize="redimensionar();">
        <div id="capaSuperior" style="position:absolute; width:100%; height:100px; z-index:0; left: 0px; top: 0px;">
            <jsp:include page="/WEB-INF/toptsp.jsp?encabezado=Impresi�n de Planillas"/>
        </div>

        <div id="capaCentral" style="position:absolute; width:100%; height:85%; z-index:0; left: 0px; top: 100px; overflow: scroll;"> 
            <%-- Inicio Formulario --%>
            <form action="<%=CONTROLLER%>?estado=Cumplidos_Documentos&accion=Insert" method='post' name='FormularioListado' id="FormularioListado">
            <% if(Lista!=null && Lista.size()>0) { %>   
                <%-- Inicio Tabla Principal --%>       
                <table width="450" border="2" align="center">
                    <tr>
                        <td>
                            <%-- Inicio Tabla Secundaria --%>
                            <table width="100%" class="tablaInferior">
                                <tr>
                                    <td width="50%" class="subtitulo1">&nbsp;Tipo Documeccntos</td>
                                    <td width="50%" class="barratitulo"><img src="<%=BASEURL%>/images/titulo.gif" width="32" height="20"></td>
                                </tr>
                                <tr>
                                <td colspan="2">
                                </td>
                            </table>
                            <table width="100%" border="1" bordercolor="#999999" bgcolor="#F7F5F4" align="center">
                                <tr class="tblTitulo" align="center">
                                    <th width='8%' align="center">N�</th>
                                    <th width='8%' align="center"><input type='checkbox' name='All' onclick='jscript: SelAll();'></th>
                                    <th width='30%' align="center">Tipo Documento </th>
                                    <th width='54%' align="center">Nombre Tipo Documento</th>			
                                </tr>
                                <%
                                int Cont = 1,i=0;
                                Iterator it2 = Lista.iterator();
                                while(it2.hasNext()){
                                    Stdjob_tbldoc dat = (Stdjob_tbldoc) it2.next();
                                    if(dat.getStd_job_no().equals(standar)){ %>
                                        <tr class="<%=(i % 2 == 0 )?"filagris":"filaazul"%>" >
                                            <td  align='center' class="bordereporte"><span class='comentario'><%=Cont++%></span></td>
                                            <th align="center" class="bordereporte"><input type='checkbox' name='LOV' value='<%= numpla+","+numrem+","+dat.getDocument_type()+","+dat.getStd_job_no()+","+dat.getDstrct() %>' ></th>
                                            <td align="center" class="bordereporte"><%=dat.getDocument_type()%></td>
                                            <td align="center" class="bordereporte"><%=model.stdjob_tbldocService.nombreDocumento(dat.getDocument_type())%></td>			
                                        </tr>
                                    <% } i++;
                                }%>        
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td class="pie" align="center">
                            <input type="hidden" name="numpla" value="<%=numpla%>">
                            <input type="hidden" name="numrem" value="<%=numrem%>">
                            <input type="hidden" name="standar" value="<%=standar%>">
                        </td>
                    </tr>
                </table>
                <p>
                    <div align="center">
                    <img src="<%=BASEURL%>/images/botones/aceptar.gif" style="cursor:hand" title="Agregar un documento cumplido" name="ingresar"  onclick="if (verificar_checkbox_c()==true){FormularioListado.submit();}" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);">&nbsp;
                    <img src="<%=BASEURL%>/images/botones/salir.gif" style="cursor:hand" name="salir" title="Salir al Menu Principal" onClick="parent.close();" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);"></div>
                </p> 
                </form>
                <% }else{ %>
                    <br>
                    <table border="2" align="center">
                        <tr>
                            <td>
                            <table width="100%"  border="1" align="center"  bordercolor="#F7F5F4" bgcolor="#FFFFFF">
                            <tr>
                            <td width="229" align="center" class="mensajes">No se encontraron documentos cumplidos</td>
                            <td width="29" background="<%=BASEURL%>/images/cuadronaranja.JPG">&nbsp;</td>
                            <td width="58">&nbsp;</td>
                        </tr>
                            </table>
                        </td>
                        </tr>
                    </table>
                <%}%>
                <%String mensaje = (String) request.getAttribute("mensaje"); 
                if(mensaje!=null){%>
                    <br>
                    <table border="2" align="center">
                        <tr>
                            <td>
                                <table width="100%"  border="1" align="center"  bordercolor="#F7F5F4" bgcolor="#FFFFFF">
                                    <tr>
                                        <td width="229" align="center" class="mensajes"><%=mensaje%></td>
                                        <td width="29" background="<%=BASEURL%>/images/cuadronaranja.JPG">&nbsp;</td>
                                        <td width="58">&nbsp;</td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                    </table>
                <% } %>	
            </table>
        </div>
    </body>
</html>
