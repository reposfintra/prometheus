<!--  
	 - Author(s)       :      Jose de la rosa
	 - Description     :      AYUDA DESCRIPTIVA - cumplidos
	 - Date            :      08/06/2006 
	 - Copyright Notice:      TRANSPORTES SANCHEZ POLO S.A.
-->
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN""http://www.w3.org/TR/html4/loose.dtd">
<%@page session="true"%> 
<%@page errorPage="/error/ErrorPage.jsp"%>
<%@page import="com.tsp.operation.model.beans.*, java.util.*"%>
<%@ include file="/WEB-INF/InitModel.jsp"%>
<%@ taglib uri="http://jakarta.apache.org/taglibs/input-0.90" prefix="input" %>
<html>
<head>
<title>AYUDA DESCRIPTIVA - Cumplidos</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link href="<%= BASEURL %>/css/estilostsp.css" rel="stylesheet" type="text/css">
<script type='text/javascript' src="<%= BASEURL %>/js/boton.js"></script>
</head>

<body>
<br>
<table width="750"  border="2" align="center">
  <tr>
    <td width="100%" >
      <table width="100%" border="0" align="center">
        <tr  class="subtitulo">
          <td height="24" colspan="2"><div align="center">Cumplidos</div></td>
        </tr>
        <tr class="subtitulo1">
          <td colspan="2">Cumplir Planillas o Remesas</td>
        </tr>
		<tr class="subtitulo1">
          <td colspan="2">INFORMACION DEL CUMPLIDO </td>
        </tr>
        <tr>
          <td  class="fila">Planilla / Remesa </td>
          <td  class="ayudaHtmlTexto">Campo para digitar la planilla o la remesa a buscar de acuerdo a la seleccion escogida. Este campo es de m&aacute;ximo 10 caracteres. </td>
        </tr>
        <tr>
          <td  class="fila">Cantidad Entregada</td>
          <td  class="ayudaHtmlTexto">Campo para digitar la cantidad entregada de la planilla a cumplir. Este campo es de m&aacute;ximo 10 caracteres. </td>
        </tr>
        <tr>
          <td  class="fila">Valor Total Extrafletes</td>
          <td  class="ayudaHtmlTexto">Campo que se calcula autometicamente cuando se ingresan extrafletes. Este campo es de m&aacute;ximo 12 caracteres. </td>
        </tr>
        <tr>
          <td  class="fila">Cometario</td>
          <td  class="ayudaHtmlTexto">Campo para digitar los comentarios del cumplido de  la planilla.</td>
        </tr>						
        <tr>
          <td  class="fila">Recibido</td>
          <td  class="ayudaHtmlTexto">Campo para digitar la cantidad recibida de la remesa a cumplir. Este campo es de m&aacute;ximo 10 caracteres. </td>
        </tr>
        <tr>
          <td  class="fila">Descripcion</td>
          <td  class="ayudaHtmlTexto">Campo para digitar los comentarios del cumplido de  la remesa.</td>
        </tr>
		
        <tr>
          <td  class="fila">Link Sanciones</td>
          <td  class="ayudaHtmlTexto">Enlace para abrir la vista de consulta de sanciones de la planilla digitada.</td>
        </tr>		
        <tr>
          <td  class="fila">Link Registrar Discrepancia</td>
          <td  class="ayudaHtmlTexto">Enlace para abrir la vista de ingresar discrepancia de la planilla digitada.</td>
        </tr>		
        <tr>
          <td  class="fila">Link -Con Discrepancia</td>
          <td  class="ayudaHtmlTexto">Enlace para abrir la vista de consulta de discrepancia de la planilla digitada.</td>
        </tr>	
        <tr>
          <td  class="fila">Link Ingresar / Modificar Extrafletes </td>
          <td  class="ayudaHtmlTexto">Enlace para abrir la vista de ingresar extrafletes.</td>
        </tr>	
        <tr>
          <td  class="fila">Link Ingresar/ Modificar Costos Reembolsables </td>
          <td  class="ayudaHtmlTexto">Enlace para abrir la vista de ingresar costos reembolsables.</td>
        </tr>	
        <tr>
          <td  class="fila">Link Liquidar OC </td>
          <td  class="ayudaHtmlTexto">Enlace para abrir la vista de consulta de liquidación de OC.</td>
        </tr>								
        <tr>
          <td  class="fila">Link Documentos Cumplidos </td>
          <td  class="ayudaHtmlTexto">Enlace para abrir la vista de ingresar o consultar documentos cumplidos.</td>
        </tr>		
          <td class="fila">Bot&oacute;n <img src="<%=BASEURL%>/images/botones/iconos/lupa.gif" width="20" height="16" title="Buscar" name="buscar" ></td>
          <td  class="ayudaHtmlTexto">Bot&oacute;n para confirmar y realizar la busqueda del acuerdo especial.</td>
        </tr>
		<tr>
          <td class="fila">Bot&oacute;n Cumplir Planilla </td>
          <td  class="ayudaHtmlTexto">Bot&oacute;n que permite cumplir la planilla.</td>
        </tr>
		<tr>
          <td class="fila">Bot&oacute;n Cumplir Remesa </td>
          <td  class="ayudaHtmlTexto">Bot&oacute;n que permite cumplir la remesa.</td>
        </tr>		
		<tr>
          <td width="35%" class="fila">Bot&oacute;n Salir</td>
          <td width="65%"  class="ayudaHtmlTexto">Bot&oacute;n para salir de la vista 'Cumplidos' y volver a la vista del men&uacute;.</td>
        </tr>
      </table>
    </td>
  </tr>
</table>
<p></p>
<center>
<img src = "<%=BASEURL%>/images/botones/salir.gif" style = "cursor:hand" name = "imgsalir" onClick = "parent.close();" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);">
</center>
</body>
</html>
