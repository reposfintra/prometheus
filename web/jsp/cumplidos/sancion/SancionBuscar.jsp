<!--
- Autor : Ing. Jose de la rosa
- Date  : 10 de Noviembre de 2005
- Copyrigth Notice : Fintravalores S.A. S.A
-->
<%--
-@(#)
--Descripcion : Pagina JSP, que maneja la busqueda de las sanciones.
--%>

<%-- Declaracion de librerias--%>
<%@page contentType="text/html"%>
<%@page session="true"%>
<%@page errorPage="../error/ErrorPage.jsp"%>
<%@page import="java.util.*"%>
<%@page import="com.tsp.operation.model.*"%>
<%@page import="com.tsp.util.*"%>
<%@page import="com.tsp.operation.model.beans.*, java.util.*"%>
<%@include file="/WEB-INF/InitModel.jsp"%>

<html>
    <head>
        <title>Buscar Sanci&oacute;n</title>
        <link href="<%= BASEURL %>/css/estilostsp.css" rel="stylesheet" type="text/css">
        <script type='text/javascript' src="<%= BASEURL %>/js/boton.js"></script>
        <script type='text/javascript' src="<%= BASEURL %>/js/validar.js"></script>
    </head>
    <%-- Inicio Body --%>
    <body>
        <% String tipid="";%>
        <%-- Inicio Formulario --%>
        <form name="forma" method="post" action="<%=CONTROLLER%>?estado=Sancion&accion=Serch&listar=True&sw=True">
        <%-- Inicio Tabla Principal --%>
        <table width="400" border="2" align="center">
            <tr>
                <td>
                    <%-- Inicio Tabla Secundaria --%>
                    <table width="100%" align="center"  class="tablaInferior">
                    <tr>
                        <td class="subtitulo1">&nbsp;Informaci&oacute;n</td>
                        <td  class="barratitulo"><img src="<%=BASEURL%>/images/titulo.gif" width="32" height="20"></td>
                    </tr>
                    <tr class="fila">
                        <td width="171" align="left" valign="middle" >Numero Planilla </td>
                        <td width="197" valign="middle"><input name="c_numpla" type="text" class="textbox" id="c_numpla" size="15" maxlength="10" ></td>
                    </tr>
                    <tr class="fila">
                        <td >Tipo Sanci&oacute;n </td>
                        <td ><select name="c_tipo_sancion" id="c_tipo_sancion" class="textbox">
                            <option value=""></option>
                            <% model.tipo_sancionService.listTipo_sanciones(); 
                            Vector vec = model.tipo_sancionService.getTipo_sanciones();
                            for(int i = 0; i<vec.size(); i++){	
                                Tipo_sancion t = (Tipo_sancion) vec.elementAt(i);%>
                                <option value="<%=t.getCodigo()%>"><%=t.getDescripcion()%> </option>
                            <%}%>
                        </select></td>
                    </tr>
                </table></td>
            </tr>
        </table>
        <p>
            <div align="center"><img src="<%=BASEURL%>/images/botones/buscar.gif" style="cursor:hand" title="Buscar sanciones" name="buscar"  onClick="forma.submit();" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);">&nbsp;
            <img src="<%=BASEURL%>/images/botones/detalles.gif" style="cursor:hand" title="Buscar todos las sanciones" name="buscar"  onClick="window.location='<%=CONTROLLER%>?estado=Sancion&accion=Serch&listar=True&sw=False'" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);">&nbsp;
            <img src="<%=BASEURL%>/images/botones/salir.gif"  name="regresar" style="cursor:hand" title="Salir al Menu Principal" onMouseOver="botonOver(this);" onClick="parent.close();" onMouseOut="botonOut(this);" ></div>
        </p>
        </form>
    </body>
</html>
