<!--
- Autor : Ing. Jose de la rosa
- Date  : 9 de Octubre de 2005
- Copyrigth Notice : Fintravalores S.A. S.A
-->
<%--
-@(#)
--Descripcion : Pagina JSP, que sirve para autorizar sanciones
--%>

<%-- Declaracion de librerias--%>
<%@page contentType="text/html"%>
<%@page session="true"%>
<%@page import="java.util.*"%>
<%@page import="com.tsp.operation.model.*"%>
<%@page import="com.tsp.operation.model.beans.*"%>
<%@page import="com.tsp.util.*"%>
<%@include file="/WEB-INF/InitModel.jsp"%>
<%@ taglib uri="http://jakarta.apache.org/taglibs/input-0.90" prefix="input" %>
<html>
    <head>
        <title>Discrepancia Modificar</title>
        <link href="<%= BASEURL %>/css/estilostsp.css" rel="stylesheet" type="text/css">
        <script type='text/javascript' src="<%= BASEURL %>/js/validar.js"></script>
        <script type='text/javascript' src="<%= BASEURL %>/js/boton.js"></script>
        <script>
            function validarSancion_mod(){
            if(forma.c_valor_autorizado.value==""){
            alert("El campo del valor a autorizar de la sanci�n no puede ser vacio!");
            return (false);
            }
            else{
            forma.submit();
            }	
            }
        </script>
    </head>
    <body <%if(request.getParameter("reload")!=null){%>onLoad="parent.opener.location.reload();"<%}%>>
<%  String mensaje = (String) request.getAttribute("msg");
    Sancion s = model.sancionService.getSancion();        
    String res = "";
    InfoPlanilla info = model.planillaService.obtenerInformacionPlanilla(s.getNumpla());
    info.setPropietario(model.planillaService.obtenerNombrePropietario(s.getNumpla()));
    String conductor = info.getCedulaConductor() + " " + info.getNombreConductor();
    String ruta = info.getOrigen() + " - " + info.getDestino();
    String cliente = info.getCliente();
    String fecha = info.getFecha();
    String placa = info.getVehiculo();
    String propietario = info.getPropietario();
%>
        <FORM name='forma' method='POST'  action="<%=CONTROLLER%>?estado=Sancion&accion=Autorizar">
        <table width="700" border="2" align="center">          
            <tr>
                <td>
                    <table width="100%" class="tablaInferior">
                        <tr class="fila">
                            <td colspan="2" align="left" class="subtitulo1">&nbsp;Detalle de la Planilla </td>
                            <td colspan="2" align="left" class="barratitulo"><img src="<%=BASEURL%>/images/titulo.gif" width="32" height="20"></td>
                        </tr>
                        <tr class="fila">
                            <td width="14%">No.:</td>
                            <td width="36%"><%=s.getNumpla()%></td>
                            <td width="11%">Fecha:</td>
                            <td width="39%"><%=fecha %></td>
                        </tr>
                        <tr class="fila">
                            <td>Conductor:</td>
                            <td><%=conductor %></td>
                            <td>Placa:</td>
                            <td><%=placa %></td>
                        </tr>
                        <tr class="fila">
                            <td>Ruta:</td>
                            <td><%=ruta %></td>
                            <td>Propietario:</td>
                            <td><%= propietario %></td>
                        </tr>
                        <tr class="fila">
                            <td>Cliente:</td>
                            <td colspan=3><%=cliente %></td>
                        </tr>
                    </table>
                </td>
            </tr>
        </table>
        <table width="700" border="2" align="center">          
            <tr>
                <td>
                    <table width="100%" class="tablaInferior">
                        <tr class="fila">
                            <td colspan="2" align="left" class="subtitulo1">&nbsp;Sanci&oacute;n</td>
                            <td colspan="2" align="left" class="barratitulo"><img src="<%=BASEURL%>/images/titulo.gif" width="32" height="20"></td>
                        </tr>
                        <tr class="fila">
                            <td align="left" >Codigo Sanci&oacute;n </td>
                            <td width="164" valign="middle"><%=s.getCod_sancion()%><input name="c_cod_sancion" type="hidden" value="<%=s.getCod_sancion()%>" id="c_cod_sancion"></td>
                            <td width="112" valign="middle">Tipo Sanci&oacute;n </td>
                            <td width="198" valign="middle">
                                <% model.tipo_sancionService.listTipo_sanciones(); 
                                Vector vec = model.tipo_sancionService.getTipo_sanciones();
                                for(int i = 0; i<vec.size(); i++){	
                                        Tipo_sancion t = (Tipo_sancion) vec.elementAt(i);
                                        if(t.getCodigo().equals(s.getTipo_sancion())){%> 
                                                <%=t.getDescripcion()%>
                                        <%}
                                }%></td>
                        </tr>
                        <tr class="fila">
                            <td width="140" align="left" >Valor</td>
                            <td valign="middle"><%=s.getValor()%></td>
                            <td valign="middle">Observaci&oacute;n </td>
                            <td valign="middle"><%=s.getObservacion()%></td>
                        </tr>
                        <tr class="fila">
                            <td width="140" align="left" >Valor Autorizar</td>
                            <td colspan="3" valign="middle"><input name="c_valor_autorizado" type="text" class="textbox" id="c_valor_autorizado" size="13" maxlength="13" onKeyPress="soloDigitos(event,'decNO')"></td>
                        </tr>		
                    </table>
                </td>
            </tr>
        </table>
        <input name="c_numpla" type="hidden" value="<%=s.getNumpla()%>">
        <p>
            <div align="center"><img src="<%=BASEURL%>/images/botones/aceptar.gif" style="cursor:hand" title="Autorizar una sanci�n" name="modificar"  onclick="return validarSancion_mod();" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);">&nbsp;
            <img src="<%=BASEURL%>/images/botones/cancelar.gif" style="cursor:hand" name="regresar" title="Limpiar todos los registros" onMouseOver="botonOver(this);" onClick="forma.reset();" onMouseOut="botonOut(this);" >&nbsp;
            <img src="<%=BASEURL%>/images/botones/salir.gif" style="cursor:hand" name="salir" title="Salir al Menu Principal" onMouseOver="botonOver(this);" onClick="parent.close();" onMouseOut="botonOut(this);" ></div>
        </p>  
        <% if(mensaje!=null){%>
            <p>
                <table border="2" align="center">
                    <tr>
                        <td>
                            <table width="100%"  border="1" align="center"  bordercolor="#F7F5F4" bgcolor="#FFFFFF">
                                <tr>
                                    <td width="229" align="center" class="mensajes"><%=mensaje%></td>
                                    <td width="29" background="<%=BASEURL%>/images/cuadronaranja.JPG">&nbsp;</td>
                                    <td width="58">&nbsp;</td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
            </p>
        <%}%>
        </form>
    </body>
</html>
