<!--
- Autor : Ing. Jose de la rosa
- Date  : 10 de Noviembre de 2005
- Copyrigth Notice : Fintravalores S.A. S.A
-->
<%--
-@(#)
--Descripcion : Pagina JSP, que maneja el ingreso de las sanciones.
--%>

<%-- Declaracion de librerias--%>
<%@page contentType="text/html"%>
<%@page session="true"%>
<%@page import="java.util.*"%>
<%@page import="com.tsp.operation.model.*"%>
<%@page import="com.tsp.util.*"%>
<%@page import="com.tsp.operation.model.beans.*, java.util.*"%>
<%@include file="/WEB-INF/InitModel.jsp"%>
<%@taglib uri="http://jsptags.com/tags/navigation/pager" prefix="pg" %>
<html>
    <head>
        <title>Sanci&oacute;n Ingresar</title>
        <link href="<%= BASEURL %>/css/estilostsp.css" rel="stylesheet" type="text/css">
        <script type='text/javascript' src="<%= BASEURL %>/js/boton.js"></script>
    </head>
    <script type='text/javascript' src="<%= BASEURL %>/js/validar.js"></script>
    <%-- Inicio Body --%>
    <body>
<%  String mensaje = (String) request.getAttribute("msg");
    String res = "";
    String numpla = request.getParameter("c_numpla");
    String sw = request.getParameter("sw");
    InfoPlanilla info = model.planillaService.obtenerInformacionPlanilla(numpla);
    info.setPropietario(model.planillaService.obtenerNombrePropietario(numpla));
    String conductor = info.getCedulaConductor() + " " + info.getNombreConductor();
    String ruta = info.getOrigen() + " - " + info.getDestino();
    String fecha = info.getFecha();
    String placa = info.getVehiculo();
    String propietario = info.getPropietario();
%>
    <%-- Inicio Formulario --%>
    <FORM name='forma' method='POST'  action="<%=CONTROLLER%>?estado=Sancion&accion=Insert">
        <%-- Inicio Tabla Principal --%>
        <table width="700" border="2" align="center">          
            <tr>
                <td>
                    <%-- Inicio Tabla Secundaria --%>
                    <table width="100%" class="tablaInferior">
                        <tr class="fila">
                            <td colspan="2" align="left" class="subtitulo1">&nbsp;Detalle de la Planilla </td>
                            <td colspan="2" align="left" class="barratitulo"><img src="<%=BASEURL%>/images/titulo.gif" width="32" height="20"></td>
                        </tr>
                        <tr class="fila">
                            <td width="13%">No.:</td>
                            <td width="37%"><%=numpla %></td>
                            <td width="13%">Fecha:</td>
                            <td width="37%"><%=fecha %></td>
                        </tr>
                        <tr class="fila">
                            <td>Conductor:</td>
                            <td><%=conductor %></td>
                            <td>Placa:</td>
                            <td><%=placa %></td>
                        </tr>
                        <tr class="fila">
                            <td>Ruta:</td>
                            <td><%=ruta %></td>
                            <td>Propietario:</td>
                            <td><%= propietario %></td>
                        </tr>
                    </table>
                </td>
            </tr>
        </table>
        <table width="700" border="2" align="center">          
            <tr>
                <td>
                    <table width="100%" class="tablaInferior">
                        <tr class="fila">
                            <td colspan="2" align="left" class="subtitulo1">&nbsp;Sanci&oacute;n</td>
                            <td colspan="2" align="left" class="barratitulo"><img src="<%=BASEURL%>/images/titulo.gif" width="32" height="20"></td>
                        </tr>
                        <tr class="fila">
                            <td align="left" >Tipo Sanción</td>
                            <td width="217" valign="middle">
                                <select name="c_tipo_sancion" id="c_tipo_sancion" class="textbox">
                                    <% model.tipo_sancionService.listTipo_sanciones(); 
                                    Vector vec = model.tipo_sancionService.getTipo_sanciones();
                                    for(int i = 0; i<vec.size(); i++){	
                                        Tipo_sancion t = (Tipo_sancion) vec.elementAt(i);%>
                                            <option value="<%=t.getCodigo()%>"><%=t.getDescripcion()%> </option>
                                    <%}%>
                                </select>
                                <img src="<%=BASEURL%>/images/botones/iconos/obligatorio.gif"></td>
                            <td width="96" valign="middle">Valor </td>
                            <td width="235" valign="middle"><input name="c_valor" type="text" class="textbox" id="c_valor" onKeyPress="soloDigitos(event,'decNO')" size="13" maxlength="13">
                                <img src="<%=BASEURL%>/images/botones/iconos/obligatorio.gif"></td>
                        </tr>
                        <tr class="fila" id="cantidad">
                            <td width="105" align="left" >Observación</td>
                            <td colspan="3" valign="middle"><textarea name="c_observacion" cols="90" rows="3" class="textbox" id="c_observacion"></textarea>            </td>
                        </tr>
                    </table>
                </td>
            </tr>
        </table>
        <input name="c_numpla" type="hidden" value="<%=numpla%>">
        <p>
        <div align="center"><img src="<%=BASEURL%>/images/botones/aceptar.gif" style="cursor:hand" title="Agregar una sanción" name="ingresar"  onclick="return validarSancion();" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);">&nbsp;
        <img src="<%=BASEURL%>/images/botones/cancelar.gif" style="cursor:hand" name="regresar" title="Limpiar todos los registros" onMouseOver="botonOver(this);" onClick="forma.reset();" onMouseOut="botonOut(this);" >&nbsp;
        <img src="<%=BASEURL%>/images/botones/salir.gif" style="cursor:hand" name="salir" title="Salir al Menu Principal" onClick="parent.close();" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);"></div>
        </p>  
        <% if(mensaje!=null){%>
            <p><table border="2" align="center">
                <tr>
                    <td><table width="100%"  border="1" align="center"  bordercolor="#F7F5F4" bgcolor="#FFFFFF">
                        <tr>
                            <td width="229" align="center" class="mensajes"><%=mensaje%></td>
                            <td width="29" background="<%=BASEURL%>/images/cuadronaranja.JPG">&nbsp;</td>
                            <td width="58">&nbsp;</td>
                        </tr>
                    </table></td>
                </tr>
            </table>
            </p>
        <%}%>
    </form>
    </body>
</html>
