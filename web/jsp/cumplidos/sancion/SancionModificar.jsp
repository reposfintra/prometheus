<!--
- Autor : Ing. Jose de la rosa
- Date  : 10 de Noviembre de 2005
- Copyrigth Notice : Fintravalores S.A. S.A
-->
<%--
-@(#)
--Descripcion : Pagina JSP, que maneja las modificaciónes de las sanciones.
--%>

<%-- Declaracion de librerias--%>
<%@page contentType="text/html"%>
<%@page session="true"%>
<%@page import="java.util.*"%>
<%@page import="com.tsp.operation.model.*"%>
<%@page import="com.tsp.operation.model.beans.*"%>
<%@page import="com.tsp.util.*"%>
<%@include file="/WEB-INF/InitModel.jsp"%>
<%@ taglib uri="http://jakarta.apache.org/taglibs/input-0.90" prefix="input" %>
<html>
    <head>
        <title>Discrepancia Modificar</title>
        <link href="<%= BASEURL %>/css/estilostsp.css" rel="stylesheet" type="text/css">
        <script type='text/javascript' src="<%= BASEURL %>/js/boton.js"></script>
    </head>
    <script type='text/javascript' src="<%= BASEURL %>/js/validar.js"></script>
    <%-- Inicio Body --%>
    <body <%if(request.getParameter("reload")!=null){%>onLoad="parent.opener.location.reload();"<%}%>>
        <%
        String mensaje = (String) request.getAttribute("msg");
        Sancion s = model.sancionService.getSancion();        
        String res = "";
        InfoPlanilla info = model.planillaService.obtenerInformacionPlanilla(s.getNumpla());
        info.setPropietario(model.planillaService.obtenerNombrePropietario(s.getNumpla()));
        String conductor = info.getCedulaConductor() + " " + info.getNombreConductor();
        String ruta = info.getOrigen() + " - " + info.getDestino();
        String cliente = info.getCliente();
        String fecha = info.getFecha();
        String placa = info.getVehiculo();
        String propietario = info.getPropietario();
        %>
        <%-- Inicio Formulario --%>
        <FORM name='forma' method='POST'  action="<%=CONTROLLER%>?estado=Sancion&accion=Update">
        <%-- Inicio Tabla Principal --%>
        <table width="700" border="2" align="center">          
            <tr>
                <td>
                    <%-- Inicio Tabla Secundaria --%>
                    <table width="100%" class="tablaInferior">
                        <tr class="fila">
                            <td colspan="2" align="left" class="subtitulo1">&nbsp;Detalle de la Planilla </td>
                            <td colspan="2" align="left" class="barratitulo"><img src="<%=BASEURL%>/images/titulo.gif" width="32" height="20"></td>
                        </tr>
                        <tr class="fila">
                            <td width="14%">No.:</td>
                            <td width="36%"><%=s.getNumpla()%></td>
                            <td width="11%">Fecha:</td>
                            <td width="39%"><%=fecha %></td>
                        </tr>
                        <tr class="fila">
                            <td>Conductor:</td>
                            <td><%=conductor %></td>
                            <td>Placa:</td>
                            <td><%=placa %></td>
                        </tr>
                        <tr class="fila">
                            <td>Ruta:</td>
                            <td><%=ruta %></td>
                            <td>Propietario:</td>
                            <td><%= propietario %></td>
                        </tr>
                        <tr class="fila">
                            <td>Cliente:</td>
                            <td colspan=3><%=cliente %></td>
                        </tr>
                    </table>
                </td>
            </tr>
        </table>
        <table width="700" border="2" align="center">          
            <tr>
                <td>
                    <table width="100%" class="tablaInferior">
                        <tr class="fila">
                            <td colspan="2" align="left" class="subtitulo1">&nbsp;Sanci&oacute;n</td>
                            <td colspan="2" align="left" class="barratitulo"><img src="<%=BASEURL%>/images/titulo.gif" width="32" height="20"></td>
                        </tr>
                        <tr class="fila">
                            <td align="left" >Tipo Sanci&oacute;n </td>
                            <td width="164" valign="middle"><select name="c_tipo_sancion" id="c_tipo_sancion" class="textbox">
                            <% model.tipo_sancionService.listTipo_sanciones(); 
                            Vector vec = model.tipo_sancionService.getTipo_sanciones();
                            for(int i = 0; i<vec.size(); i++){	
                                Tipo_sancion t = (Tipo_sancion) vec.elementAt(i);%>
                                <option value="<%=t.getCodigo()%>" <%if(t.getCodigo().equals(s.getTipo_sancion())){%> selected <%}%>><%=t.getDescripcion()%> </option>
                            <%}%>
                            </select></td>
                            <td width="112" valign="middle">Valor</td>
                            <td width="198" valign="middle"><input id="c_valor3" name="c_valor" type="text" class="textbox" maxlength="15" value="<%=s.getValor()%>">
                            <img src="<%=BASEURL%>/images/botones/iconos/obligatorio.gif"></td>
                        </tr>
                        <tr class="fila">
                            <td width="140" align="left" >Observaci&oacute;n </td>
                            <td colspan="3" valign="middle"><textarea name="c_observacion" cols="90" rows="3" class="textbox" id="textarea"><%=s.getObservacion()%></textarea></td>
                        </tr>
                    </table>
                </td>
            </tr>
        </table>
        <input name="c_numpla" type="hidden" value="<%=s.getNumpla()%>">
        <input name="c_cod_sancion" type="hidden" value="<%=s.getCod_sancion()%>">
        <p>
            <div align="center"><img src="<%=BASEURL%>/images/botones/modificar.gif" style="cursor:hand" title="Modificar una sanción" name="modificar"  onclick="return validarSancion();" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);">&nbsp;
            <img src="<%=BASEURL%>/images/botones/anular.gif" style="cursor:hand" title="Anular una sanción" name="anular"  onClick="window.location='<%=CONTROLLER%>?estado=Sancion&accion=Anular&c_numpla=<%=s.getNumpla()%>&c_cod_sancion=<%=s.getCod_sancion()%>'" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);">&nbsp;
            <img src="<%=BASEURL%>/images/botones/salir.gif" style="cursor:hand" name="salir" title="Salir al Menu Principal" onMouseOver="botonOver(this);" onClick="parent.close();" onMouseOut="botonOut(this);" ></div>
        </p>  
        <% if(mensaje!=null){%>
            <p>
                <table border="2" align="center">
                    <tr>
                        <td>
                            <table width="100%"  border="1" align="center"  bordercolor="#F7F5F4" bgcolor="#FFFFFF">
                                <tr>
                                    <td width="229" align="center" class="mensajes"><%=mensaje%></td>
                                    <td width="29" background="<%=BASEURL%>/images/cuadronaranja.JPG">&nbsp;</td>
                                    <td width="58">&nbsp;</td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
            </p>
        <%}%>
        </form>
    </body>
</html>
