<!--
- Autor : Ing. Jose de la rosa
- Date  : 10 de Noviembre de 2005
- Copyrigth Notice : Fintravalores S.A. S.A
-->
<%--
-@(#)
--Descripcion : Pagina JSP, que maneja la busqueda de las sanciones migradas.
--%>

<%-- Declaracion de librerias--%>
<%@page contentType="text/html"%>
<%@page session="true"%>
<%@page errorPage="../error/ErrorPage.jsp"%>
<%@page import="java.util.*"%>
<%@page import="com.tsp.operation.model.*"%>
<%@page import="com.tsp.util.*"%>
<%@page import="com.tsp.operation.model.beans.*, java.util.*"%>
<%@include file="/WEB-INF/InitModel.jsp"%>

<html>
    <head>
        <title>Buscar Discrepancia</title>
        <link href="<%= BASEURL %>/css/estilostsp.css" rel="stylesheet" type="text/css">
        <script type='text/javascript' src="<%= BASEURL %>/js/boton.js"></script>
        <script src='<%=BASEURL%>/js/date-picker.js'></script>
        <script type='text/javascript' src="<%= BASEURL %>/js/validar.js"></script>
    </head>
    <%-- Inicio Body --%>
    <body>
<% String tipid="";%>
        <%-- Inicio Formulario --%>
        <form name="forma" method="post" action="<%=CONTROLLER%>?estado=Sancion_listar_migrados&accion=Serch">
        <%-- Inicio Tabla Principal --%>
        <table width="500" border="2" align="center">
            <tr>
                <td>
                    <%-- Inicio Tabla Secundaria --%>
                    <table width="100%" align="center"  class="tablaInferior">
                        <tr>
                            <td colspan="2" width="50%" class="subtitulo1">&nbsp;Buscar sanciones migrados</td>
                            <td colspan="2" class="barratitulo"><img src="<%=BASEURL%>/images/titulo.gif" width="32" height="20"></td>
                        </tr>
                        <tr class="fila">
                        <td colspan="4" align="center" >Seleccione 
                            <select name="c_tipo" class="textbox" onChange="CamposDiscrep();" id="c_tipo">
                                <option value="C">Con Fecha de Migración</option>
                                <option value="S">Sin Fecha de Migración</option>
                            </select></tr>	
                        <tr class="fila" id="fechas">
                            <td width="89" align="left" > Fecha Inicio </td>
                            <td width="140" valign="middle" >
                            <input name="c_fecha_inicio" type="text" readonly class="textbox" id="c_fecha_inicio" size="12">
                            <span class="Letras"><a href="javascript:void(0)" onclick="jscript: show_calendar('c_fecha_inicio');" HIDEFOCUS><img name="popcal" align="absmiddle" src="<%=BASEURL%>/js/Calendario/calbtn.gif" width="34" height="22" border="0" alt=""></a></span></td>
                            <td width="84" valign="middle">Fecha Fin </td>
                            <td width="142" valign="middle">
                            <input name="c_fecha_fin" type="text" class="textbox" readonly id="c_fecha_fin" size="12">
                            <span class="Letras"><a href="javascript:void(0)" onclick="jscript: show_calendar('c_fecha_fin');" HIDEFOCUS><img name="popcal" align="absmiddle" src="<%=BASEURL%>/js/Calendario/calbtn.gif" width="34" height="22" border="0" alt=""></a> </span></td>
                        </tr> 		
                    </table>
                </td>
            </tr>
        </table>
        <p>
            <div align="center"><img src="<%=BASEURL%>/images/botones/buscar.gif" style="cursor:hand" title="Buscar sanciones Migradas" name="buscar"  onClick="forma.submit();" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);">&nbsp;
            <img src="<%=BASEURL%>/images/botones/salir.gif"  name="regresar" style="cursor:hand" title="Salir al Menu Principal" onMouseOver="botonOver(this);" onClick="parent.close();" onMouseOut="botonOut(this);" ></div>
        </p>   
        </form>
    </body>
</html>
