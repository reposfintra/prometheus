<!--
- Autor : Ing. Jose de la rosa
- Date  : 9 de Octubre de 2005
- Copyrigth Notice : Fintravalores S.A. S.A
-->
<%--
-@(#)
--Descripcion : Pagina JSP, que lista las sanciones a autorizarce
--%>

<%-- Declaracion de librerias--%>
<%@page contentType="text/html"%>
<%@page session="true"%>
<%@page errorPage="../error/ErrorPage.jsp"%>
<%@page import="java.util.*"%>
<%@page import="com.tsp.operation.model.*"%>
<%@page import="com.tsp.util.*"%>
<%@page import="com.tsp.operation.model.beans.*, java.util.*"%>
<%@include file="/WEB-INF/InitModel.jsp"%>
<%@taglib uri="http://jsptags.com/tags/navigation/pager" prefix="pg" %>
 
<html>
<head>
<title>Listado Sanci&oacute;n Autorizar</title>
<link href="<%= BASEURL %>/css/estilostsp.css" rel="stylesheet" type="text/css">
<script type='text/javascript' src="<%= BASEURL %>/js/boton.js"></script>
<script type='text/javascript' src="<%= BASEURL %>/js/reporte.js"></script>
</head>
<body>
  <%  String style = "simple";
    String position =  "bottom";
    String index =  "center";
	String tipo = "";
    int maxPageItems = 10;
    int maxIndexPages = 10;
	String tp = (String)session.getAttribute("tipo_sancion");
	String n = (String)session.getAttribute("numpla");
    model.sancionService.searchDetalleSancion(n, tp);
	Vector vec = model.sancionService.getSanciones();
	Sancion s;
	if ( vec.size() >0 ){  
%>
<table width="1100" border="2" align="center">
    <tr>
      <td>
	  <table width="100%" align="center">
              <tr>
                <td width="373" class="subtitulo1">&nbsp;Datos Sanci&oacute;n</td>
                <td width="427" class="barratitulo"><img src="<%=BASEURL%>/images/titulo.gif" width="32" height="20"></td>
              </tr>
        </table>
	<table width="100%" border="1" bordercolor="#999999" bgcolor="#F7F5F4" align="center">
          <tr class="tblTitulo" align="center">
          <td width="6%" align="center">Numero Planilla</td>
          <td width="6%"  align="center">Codigo Sanci&oacute;n </td>
          <td width="15%" align="center">Tipo Sanci&oacute;n</td>
		  <td width="9%" align="center">Valor Sanci&oacute;n </td>
		  <td width="9%" align="center">Valor Aprobado </td>
		  <td width="9%" align="center">Fecha Aprobaci&oacute;n </td>		  
          <td width="10%" align="center">Usuario Aprobaci&oacute;n </td>
          <td width="9%" align="center">Fecha Migraci&oacute;n</td>
          <td width="10%" align="center">Usuario Migraci&oacute;n </td>
          <td width="17%" align="center">Observaci&oacute;n</td>
          </tr>
        <pg:pager
         items="<%=vec.size()%>"
         index="<%= index %>"
         maxPageItems="<%= maxPageItems %>"
        maxIndexPages="<%= maxIndexPages %>"
    isOffset="<%= true %>"
    export="offset,currentPageNumber=pageNumber"
    scope="request">
        <%-- keep track of preference --%>
        <%
      for (int i = offset.intValue(), l = Math.min(i + maxPageItems, vec.size()); i < l; i++)
	  {
          s = (Sancion) vec.elementAt(i);%>
        <pg:item>
        <tr class="<%=(i % 2 == 0 )?"filagris":"filaazul"%>"<%if(s.getFecha_aprobacion().equals("0099-01-01 00:00:00")){%> onMouseOver='cambiarColorMouse(this)'   style="cursor:hand"
        onClick="window.open('<%=CONTROLLER%>?estado=Sancion&accion=Obtener&c_numpla=<%=s.getNumpla()%>&c_cod_sancion=<%=s.getCod_sancion()%>','','status=yes,scrollbars=yes,width=780,height=450,resizable=yes');" <%}%> >
          <td width="6%" align="center" class="bordereporte"><%=s.getNumpla()%></td>
          <td width="6%" align="center" class="bordereporte"><%=s.getCod_sancion()%></td>
            <% model.tipo_sancionService.listTipo_sanciones(); 
			Vector vect = model.tipo_sancionService.getTipo_sanciones();
			for(int j = 0; j<vect.size(); j++){	
				Tipo_sancion t = (Tipo_sancion) vect.elementAt(j);
				if(t.getCodigo().equals(s.getTipo_sancion())){ tipo = t.getDescripcion(); }%>
			<%}%>

          <td width="15%" align="center" class="bordereporte"><%=tipo%></td>
		  <td width="9%" align="center" class="bordereporte"><%=s.getValor()%></td>
		  <td width="9%" align="center" class="bordereporte"><%=s.getValor_aprobado()%></td>
		  <td width="9%" align="center" class="bordereporte"><%=s.getFecha_aprobacion().substring(0,10)%></td>
          <td width="10%" align="center" class="bordereporte">&nbsp;<%=s.getUsuario_aprobacion()%></td>
          <td width="9%" align="center" class="bordereporte"><%=s.getFecha_migracion().substring(0,10)%></td>
          <td width="10%" align="center" class="bordereporte">&nbsp;<%=s.getUsuario_migracion()%></td>
          <td width="17%" align="center" class="bordereporte"><%=s.getObservacion()%>&nbsp;</td>
        </tr>
        </pg:item>
        <%}%>
        <tr  class="pie" align="center">
          <td td height="20" colspan="10" nowrap align="center">
		   <pg:index>
            <jsp:include page="/WEB-INF/jsp/googlesinimg.jsp" flush="true"/>      
           </pg:index> 
	      </td>
        </tr>
        </pg:pager>
      </table></td>
    </tr>
</table>
<br><%}
 else { %>

  <table border="2" align="center">
    <tr>
      <td><table width="410" height="73" border="1" align="center"  bordercolor="#F7F5F4" bgcolor="#FFFFFF">
          <tr>
            <td width="282" align="center" class="mensajes">Su b&uacute;squeda no arroj&oacute; resultados!</td>
            <td width="28" background="<%=BASEURL%>/images/cuadronaranja.JPG">&nbsp;</td>
            <td width="78">&nbsp;</td>
          </tr>
      </table></td>
    </tr>
  </table>
  <%}%>
   <table width="1100" border="0" align="center">
    <tr>
      <td>
	  <img src="<%=BASEURL%>/images/botones/regresar.gif" style="cursor:hand" title="Volver" name="buscar"  onClick="window.location='<%=CONTROLLER%>?estado=Menu&accion=Cargar&carpeta=/jsp/cumplidos/sancion&titulo=Autorizar Sanci�n&pagina=buscarSancionPlanilla.jsp?msg='" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);">
	  </td>
	</tr>
</table>
</body>
</html>
