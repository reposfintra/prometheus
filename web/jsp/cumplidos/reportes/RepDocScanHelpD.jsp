<%@include file="/WEB-INF/InitModel.jsp"%>
<html>
<head>
<title>Descripcion Campos agregar planilla</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link href="../../../css/estilostsp.css" rel="stylesheet" type="text/css">
<link href="<%= BASEURL %>/css/estilostsp.css" rel='stylesheet'>
</head>

<body>
<br>


<table width="696"  border="2" align="center">
  <tr>
    <td width="811" >
      <table width="100%" border="0" align="center">
        <tr  class="subtitulo">
          <td height="24" colspan="2"><div align="center">Documentos Escaneados de Cumplidos </div></td>
        </tr>
        <tr class="subtitulo1">
          <td colspan="2">Reporte de documentos escaneados de cumplidos. </td>
        </tr>
        <tr>
          <td width="149" class="fila">Cliente</td>
          <td width="525"  class="ayudaHtmlTexto">Campo para seleccionar el cliente al cual se le consultar&aacute; los documentos de cumplidos. En el campo de texto superior puede digitar parte o la totalidad del nombre del cliente que busca y este aparecer&aacute; seleccionado. </td>
        </tr>
        <tr>
          <td class="fila">Fecha inicial </td>
          <td  class="ayudaHtmlTexto">Campo para seleccionar la fecha de inicio del per&iacute;odo del reporte de documentos escaneados de cumplidos. </td>
        </tr>
        <tr>
          <td class="fila">Fecha inicial </td>
          <td  class="ayudaHtmlTexto">Campo para seleccionar la fecha de final del per&iacute;odo del reporte de  de documentos escaneados de cumplidos.</td>
        </tr>
        <tr>
          <td class="fila"><div align="center">
            <input type="image" src = "<%=BASEURL%>/images/botones/aceptar.gif" style="cursor:default ">
          </div></td>
          <td  class="ayudaHtmlTexto">Bot&oacute;n que valida el la informaci&oacute;n digitada en los campos para al busqueda de la informaci&oacute;n. </td>
        </tr>
        <tr>
          <td class="fila"><div align="center">
            <input type="image" src = "<%=BASEURL%>/images/botones/cancelar.gif" style="cursor:default ">
          </div></td>
          <td  class="ayudaHtmlTexto"> Bot&oacute;n que cancela todas los operaciones realizadas, y resetea la pagina llevandola a su estado inicial. </td>
        </tr>
        <tr>
          <td class="fila"><div align="center">
            <input type="image" src = "<%=BASEURL%>/images/botones/salir.gif" style="cursor:default ">
          </div></td>
          <td  class="ayudaHtmlTexto">Bot&oacute;n que cierra  la ventana. </td>
        </tr>
        
      </table>
    </td>
  </tr>
</table>
<p>&nbsp;</p>
<p>&nbsp;</p>

<p>&nbsp;</p>
<p>&nbsp; </p>
</body>
</html>
