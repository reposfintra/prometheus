<%@ include file="/WEB-INF/InitModel.jsp"%>

<HTML>
<HEAD>
<TITLE></TITLE>
<META http-equiv=Content-Type content="text/html; charset=windows-1252">
<link href="../css/estilostsp.css" rel="stylesheet" type="text/css"> 
<link href="<%=BASEURL%>/css/estilostsp.css" rel="stylesheet" type="text/css"> 
</HEAD>
<BODY>  

  <table width="95%"  border="2" align="center">
    <tr>
      <td>
        <table width="100%" border="0" align="center">
          <tr  class="subtitulo">
            <td height="20"><div align="center">Documentos Escaneados de Cumplidos </div></td>
          </tr>
          <tr class="subtitulo1">
            <td>Descripci&oacute;n del funcionamiento del programa de reporte de documentos escaneados de cumplidos. </td>
          </tr>
          <tr>
            <td  height="18"  class="ayudaHtmlTexto">Formulario de reporte de b&uacute;squeda de documentos escaneados de cumplidos. . </td>
          </tr>
          <tr>
            <td height="18"  class="ayudaHtmlTexto"><div align="center"><img src="<%= BASEURL%>/images/ayuda/cumplido/docscan/Dibujo4.JPG"></div></td>
          </tr>
          <tr>
            <td  class="ayudaHtmlTexto">&nbsp;</td>
          </tr>
          <tr>
            <td  class="ayudaHtmlTexto">Al hacer clic en el &iacute;cono <img src="<%= BASEURL %>/images/iconplus.gif" alt="Ver documentos." width="18" style="cursor:text" > 'Ver documentos' , nos presentar&aacute; una ventana con los documentos internos de la remesa, o en su defectp el mensaje '


 No registra documentos internos.

' . </td>
          </tr>
          <tr>
            <td  class="ayudaHtmlTexto"><div align="center"><img src="<%= BASEURL%>/images/ayuda/cumplido/docscan/Dibujo5.JPG"></div></td>
          </tr>
          <tr>
            <td  class="ayudaHtmlTexto">&nbsp;</td>
          </tr>
          <tr>
            <td  class="ayudaHtmlTexto"><p>Al hacer clic en el &iacute;cono <img src='<%= BASEURL%>/images/botones/iconos/documento.gif' style='cursor:text' > 'Ver documentos digitalizados' , nos presentar&aacute; una ventana con los documentos digitalizados de la planilla de la remesa, o en su defecto el mensaje ' No registra documentos internos digitalizados. ' . </p></td>
          </tr>
          <tr>
            <td  class="ayudaHtmlTexto"><div align="center"><img src="<%= BASEURL%>/images/ayuda/cumplido/docscan/Dibujo6.JPG"></div></td>
          </tr>
          <tr>
            <td  class="ayudaHtmlTexto">Al hacer clic sobre el nombre de la imagen, se abrir&aacute; una nueva ventana donde se visulizar&aacute; la imagen seleccionada. </td>
          </tr>
          <tr>
            <td  class="ayudaHtmlTexto">&nbsp;</td>
          </tr>
          
          
          
      </table></td>
    </tr>
  </table>
</BODY>
</HTML>
