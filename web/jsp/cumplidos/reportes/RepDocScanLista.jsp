
<!--
- Autor : Ing. Andr�s Maturana De La Cruz
- Date  : 27 mayo del 2006
- Copyrigth Notice : Fintravalores S.A. S.A
-->
<%--
-@(#)
--Descripcion : Pagina JSP, que maneja la generaci�n del reporte de documentos escaneados.
--%>
<%@ page session="true"%>
<%@ page errorPage="/error/error.jsp"%>
<%@page import="com.tsp.operation.model.beans.*, java.util.*"%>
<%@ include file="/WEB-INF/InitModel.jsp"%>
<%@ taglib uri="http://jsptags.com/tags/navigation/pager" prefix="pg" %>
<%String path = application.getRealPath(model.menuService.getRealUrl(request.getRequestURI()));
  String datos[] = model.menuService.getContenidoMenu(BASEURL,request.getRequestURI(),path);
%>
<html>
<head>

<title>Documentos Escaneados de Cumplidos</title>
<script language="javascript" src="../../../js/validar.js"></script>
<link href="<%= BASEURL %>/css/estilostsp.css" rel="stylesheet" type="text/css">
<script SRC='<%=BASEURL%>/js/boton.js'></script>
<link href="../../../css/estilostsp.css" rel="stylesheet" type="text/css">
<script type='text/javascript' src="<%= BASEURL %>/js/reporte.js"></script>
<script type='text/javascript' src="<%= BASEURL %>/js/docscan.js"></script>
<script>maximizar();</script>
</head>
<body onLoad="redimensionar();" onResize="redimensionar();">
<div class="fila" style="position:absolute; visibility:hidden; width:350px; height:100px; z-index:1; border: 2 outset #003399; " id="msg">
</div>
<div id="capaSuperior" style="position:absolute; width:100%; height:100px; z-index:0; left: 0px; top: 0px;">
<jsp:include page="/WEB-INF/toptsp.jsp?encabezado=Documentos Escaneados de Cumplidos"/>
</div>
<div id="capaCentral" style="position:absolute; width:100%; height:85%; z-index:0; left: 0px; top: 100px; overflow: scroll;"> 
<%    
    //String idprov = (String) request.getAttribute("idprov");
    Vector info =  model.cumplido_docService.getCumplido_docs();
	String ancho = "100%";
	
	String scan = request.getParameter("scan");
	if( scan!=null && scan.compareTo("OK")==0 ){
		String imgs = (String) request.getAttribute("docsImg");
		String numrem = request.getParameter("numrem");
		out.println("<script>showInfoScan (\"" + imgs + "\", \"" + numrem + "\", \"" + BASEURL + "\", \"" + CONTROLLER + "\");</script>");
	}
	
	
	if( info.size()>0 ){
%>
<table width="100%"  border="2" align="center">
  <tr>
    <td><table width="100%">
      <tr>
        <td class="subtitulo1">Lista  de Documentos </td>
        <td class="barratitulo"><img align="left" src="<%=BASEURL%>/images/titulo.gif"><%=datos[0]%></td>
      </tr>
    </table>      
      <table width="1552" border="1" align="center" bordercolor="#999999" bgcolor="#F7F5F4">
      <tr class="tblTitulo" valign="top">
        <td width="90"><div align="center">Documentos Internos</div></td>
        <td width="90"><div align="center">Documentos Digitalizados</div></td>
        <td width="80"><div align="center">Remesa</div></td>
        <td width="80" ><div align="center">Planilla</div></td>
        <td width="70" ><div align="center">Placa</div></td>
        <td width="300" ><div align="center">Cliente</div></td>
        <td width="300" ><div align="center">Descripci&oacute;n</div></td>
        <td width="300" > <div align="center">Destinatario/s</div></td>
        <td width="115" ><div align="center">Fecha Entrega </div></td>
        <td width="115" ><div align="center">Fecha Cumplido</div></td>
        </tr>
<%
      for (int i = 0; i < info.size(); i++){
          Hashtable ht = (Hashtable) info.elementAt(i);
		  Vector docs = (Vector) ht.get("docs");
		  Vector tdocs = (Vector) ht.get("tdocs");
		  String info_docs = "";
		  for( int j = 0; j<docs.size(); j++){
		  		info_docs += "<tr class=filagris><td>" + docs.elementAt(j).toString() + "</td>" 
						+ "<td>" + tdocs.elementAt(j).toString() + "</td></tr>"	;
		  }
		  info_docs = info_docs.length()==0 ? "<span class=letraTitulo>No registra documentos internos.</span>" 
		  		: "<table width=100% align=center border=1 cellpadding=2 cellspacing=1 bordercolor=#999999 bgcolor=#F7F5F4>" + 
				"<tr class=tblTitulo><td align=center >Documento</td><td align=center >Tipo</td></tr>" + info_docs + "</table>";
		  //
%>
      <tr valign="top" class="<%=(i % 2 == 0 )?"filagris":"filaazul"%>">
        <td valign="middle" class="bordereporte"><div align="center"><img src="<%= BASEURL %>/images/iconplus.gif" alt="Ver documentos." width="18" style="cursor:hand " onClick="showInfoChq ('<%= info_docs %>', '<%= ht.get("numrem") %>', '<%= BASEURL %>', '<%= CONTROLLER %>');" onMouseMove=" //showInfoChq (<%= "" %>); " onMouseOut=" //hiddenMsg(); "></div></td>
        <td class="bordereporte" valign="middle"><div align="center"><img src='<%= BASEURL%>/images/botones/iconos/documento.gif' onClick="window.location.href = '<%= CONTROLLER %>?estado=Cumplido&accion=DocScanIMG&numpla=<%= ht.get("numpla")%>&numrem=<%= ht.get("numrem")%>';" onMouseOver='' onMouseOut='' style='cursor:hand' alt='Ver documentos digitalizados.'></div></td>
        <td class="bordereporte"><%= ht.get("numrem") %></td>
        <td class="bordereporte"><%= ht.get("numpla") %></td>
        <td class="bordereporte"><%= ht.get("placa") %></td>
        <td class="bordereporte"><%= ht.get("nomcli") %></td>
        <td class="bordereporte"><%= ht.get("sj_desc") %></td>
        <td class="bordereporte"><%= ht.get("destin")  %></td>
        <td class="bordereporte"><%= ht.get("fecent") %></td>
        <td class="bordereporte"><%= ht.get("feccum") %></td>
        </tr>
<%
	}
%>      <tr>
        <td  colspan="10"><div class="letraresaltada" align="right"></div></td>
        </tr>
    </table></td>
  </tr>
</table>
<br>  
  
<% 
	} else {
		ancho = "316";
%>	
	<p>
<table border="2" align="center">
  <tr>
    <td><table width="100%"  border="1" align="center"  bordercolor="#F7F5F4" bgcolor="#FFFFFF">
        <tr>
          <td width="229" align="center" class="mensajes">La b�squeda no arroj� resultados.</td>
          <td width="29" background="<%=BASEURL%>/images/cuadronaranja.JPG">&nbsp;</td>
          <td width="58">&nbsp;</td>
        </tr>
    </table></td>
  </tr>
</table>
<%	}
	
	if( request.getParameter("msg") != null ){%>
<p>
<table border="2" align="center">
  <tr>
    <td><table width="100%"  border="1" align="center"  bordercolor="#F7F5F4" bgcolor="#FFFFFF">
        <tr>
          <td width="229" align="center" class="mensajes"><%=request.getParameter("msg").toString()%></td>
          <td width="29" background="<%=BASEURL%>/images/cuadronaranja.JPG">&nbsp;</td>
          <td width="58">&nbsp;</td>
        </tr>
    </table></td>
  </tr>
</table>
<p></p>
<p>
    <%} %>
</p>
<table width="<%= ancho %>" border="0" align="center" cellpadding="0" cellspacing="0">
  <tr align="left">
    <td><img src="<%=BASEURL%>/images/botones/regresar.gif" name="c_regresar" id="c_regresar" style="cursor:hand " onClick="window.location.href = '<%= CONTROLLER%>?estado=Menu&accion=Cargar&carpeta=/jsp/cumplidos/reportes&pagina=RepDocScan.jsp&marco=no&opcion=33&item=5';" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);">  </tr>
</table>
</div>
<%=datos[1]%>
</body>
</html>



