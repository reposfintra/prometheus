<%@ include file="/WEB-INF/InitModel.jsp"%>
<html>
<head>
<title>.: Descripción del programa de orden de carga :.</title>
<link href="<%=BASEURL%>/css/estilostsp.css" rel="stylesheet" type="text/css"> 
</head>

<body>
<br>
<table width="696"  border="2" align="center">
  <tr>
    <td width="811" >
      <table width="100%" border="0" align="center">
        <tr  class="subtitulo">
          <td height="24" colspan="2"><div align="center">Reporte Aduana </div></td>
        </tr>
        <tr class="subtitulo1">
          <td colspan="2"> Control Aduanas </td>
        </tr>
        <tr>
          <td width="149" class="fila"> Pais </td>
          <td width="525"  class="ayudaHtmlTexto">Campo para seleccionar el pais del que se quiere buscar las fronteras </td>
        </tr>
        <tr>
          <td class="fila">Frontera</td>
          <td  class="ayudaHtmlTexto">Fronteras del pais seleccionado </td>
        </tr>
        <tr>
          <td class="fila">Cliente</td>
          <td  class="ayudaHtmlTexto">Codigo del cliente de la remesa </td>
        </tr>
        <tr>
          <td class="fila">Trafico</td>
          <td  class="ayudaHtmlTexto">Estado del ultimo reporte en trafico </td>
        </tr>
        <tr>
          <td class="fila">Tipo</td>
          <td  class="ayudaHtmlTexto">Tipo del viaje, exportacion o importacion </td>
        </tr>
        <tr>
          <td class="fila">Faccial</td>
          <td  class="ayudaHtmlTexto">Numero de la factura comercial </td>
        </tr>
      </table></td>
  </tr>
</table>
<p>&nbsp;</p>
<p>&nbsp;</p>

<p align="center" class="fuenteAyuda"> Fintravalores S. A. </p>
<p>&nbsp;</p>
<p>&nbsp;</p>

</body>
</html>