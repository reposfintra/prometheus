<!--  
     - Author(s)       :      Karen Reales
     - Date            :      06/05/2006
     - Copyright Notice:      TRANSPORTES SANCHEZ POLO S.A.
-->
<%--   
     - @(#)  
     - Description: Ayuda
--%> 
<%@include file="/WEB-INF/InitModel.jsp"%>


<html>
    <head>
        <title>.: Descripción del programa de orden de carga :.</title>
        <link href="<%=BASEURL%>/css/estilostsp.css" rel="stylesheet" type="text/css">
    </head>
    <body>
        <BR>
        <table width="696"  border="2" align="center">
            <tr>
            <td width="811" valign="top" >
                <table width="100%" border="0" align="center">
                    <tr  class="subtitulo">
                        <td height="24" align="center">
                        Reporte Aduana </td>
                    </tr>
                    <tr class="subtitulo1">
                        <td> Control de Aduanas </td>
                    </tr>
                    <tr class="ayudaHtmlTexto">
                      <td><div align="left">
                        <p align="left">Al entrar en el men&uacute; Aduanas, aparece la siguiente pantalla: </p>
                        <p><strong><img src="<%=BASEURL%>/images/ayuda/aduanas/Aduanas%201.PNG" width="1001" height="298"></strong></p>
                        <p>Para buscar un cliente espec&iacute;fico, hacer clic en consultar clientes: </p>
                        <p align="center"><img src="<%=BASEURL%>/images/ayuda/aduanas/Aduanas%202.PNG" width="595" height="261" align="middle"></p>
                        <p>Al hacer clic en buscar, luego de seleccionar el pa&iacute;s y la frontera respectivos, se tiene lo siguiente: </p>
                        <p align="center"><img src="<%=BASEURL%>/images/ayuda/aduanas/Aduanas%203.JPG" width="947" height="510"></p>
                        <p>Al hacer clic en la &uacute;ltima actividad de un viaje espec&iacute;fico, el programa arroja la siguiente pantalla: </p>
                        <p align="center"><img src="<%=BASEURL%>/images/ayuda/aduanas/Aduanas%204.PNG" width="779" height="411"></p>
                        <p>Al hacer clic en el n&uacute;mero de la remesa de un viaje espec&iacute;fico, el programa arroja la siguiente pantalla: </p>
                        <p align="center"><img src="<%=BASEURL%>/images/ayuda/aduanas/Aduanas%205.PNG" width="1000" height="388"></p>
                        <p>Al hacer clic en la planilla que arroja el programa: </p>
                        <p align="center"><img src="<%=BASEURL%>/images/ayuda/aduanas/Aduanas%206a.PNG" width="938" height="521"></p>
                        <p align="center"><img src="<%=BASEURL%>/images/ayuda/aduanas/Aduanas%206b.PNG" width="855" height="360"></p>
                        <p>Al hacer clic en &Uacute;ltimo puesto de control: </p>
                        <p align="center"><img src="<%=BASEURL%>/images/ayuda/aduanas/Aduanas%208.PNG" width="1004" height="588"></p>
                        <p>Y Al hacer clic en la planilla reexpedida de un viaje espec&iacute;fico: </p>
                        <p align="center"><img src="<%=BASEURL%>/images/ayuda/aduanas/Aduanas%209.PNG" width="858" height="242"></p>
                        <br>
						  </div>                        
                        <div align="center">
					  </div>                       
					  <br>                     </td>
                    </tr>
                </table>
            </td>
            </tr>
        </table>
        <p align="center" class="fuenteAyuda"> Fintravalores S. A. </p>
    </body>

</html>