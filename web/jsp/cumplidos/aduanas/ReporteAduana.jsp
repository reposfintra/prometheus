<%@ page session="true"%>
<%@ page errorPage="../error/ErrorPage.jsp"%>
<%@page import="com.tsp.operation.model.beans.*, java.util.*"%>
<%@ include file="/WEB-INF/InitModel.jsp"%>
<%@ taglib uri="http://jakarta.apache.org/taglibs/input-0.90" prefix="input" %>

<html>
<head>
<title>Control de Aduanas</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link href="/css/estilostsp.css" rel="stylesheet" type="text/css">
<link href="<%=BASEURL%>/css/estilostsp.css" rel="stylesheet" type="text/css">
<script type='text/javascript' src="<%=BASEURL%>/js/boton.js"></script> 
</script>
<script type='text/javascript' src="<%=BASEURL%>/js/validarDespacho.js"></script> 
</script>

<%
	String path = application.getRealPath(model.menuService.getRealUrl(request.getRequestURI()));
	String datos[] = model.menuService.getContenidoMenu(BASEURL,request.getRequestURI(),path);
%>

</head>

<body>
<div id="capaSuperior" style="position:absolute; width:100%; height:100px; z-index:0; left: 0px; top: 0px;">
 <jsp:include page="/WEB-INF/toptsp.jsp?encabezado=CONTROL ADUANAS"/>
</div>
<div id="capaCentral" style="position:absolute; width:100%; height:85%; z-index:0; left: 0px; top: 100px; overflow: scroll;"> 
  <form name="form1" method="post" action="<%=CONTROLLER%>?estado=Aduana&accion=Control">
    <table width="100%" border="2" align="center">
      <tr>
        <td>
		<table width="100%" height="100%" align="center">
            <tr class="fila">
              <td colspan="4" align="left" class="subtitulo1">Consulta Aduanas</td>
              <td colspan="8" align="left" class="barratitulo"><img src="<%=BASEURL%>/images/titulo.gif" width="32" height="20" align="left">
              <%=datos[0]%></td>
            </tr>
            <tr class="fila">
			<td width="26" rowspan="2" align="left" ><input name="controller" type="hidden" id="controller" value="<%=CONTROLLER%>"><strong>Pais</strong></td>
              <td width="87" rowspan="2" align="left" ><%
			  TreeMap pais= model.paisservice.getListpais(); %> <input:select name="pais" options="<%=pais%>" attributesText="style='width:100%'; onchange=buscarFronteras(this.value); class='informacion'" default="" /></td>
              <td width="52" rowspan="2" align="left" ><strong>Frontera</strong></td>
              <td width="117" rowspan="2" align="left" ><%
			  TreeMap ciudades= model.ciudadService.getTreMapCiudades(); %> <input:select name="frontera" options="<%=ciudades%>" attributesText="style='width:100%;' class='informacion'" default="" /> </td>
              <td width="46" rowspan="2" align="left" >Cliente:</td>
              <td width="143" align="left" ><input name="cliente" class="textbox" type="text" id="cliente" size="12" maxlength="10">
                <span class="Simulacion_Hiper" style="cursor:hand " onClick="window.open('<%=BASEURL%>/consultas/consultasClientes.jsp','','HEIGHT=200,WIDTH=600,SCROLLBARS=YES,RESIZABLE=YES')"> <br>
              </span></td>
              <td width="45" rowspan="2" align="left" >Trafico:</td>
              <td width="89" rowspan="2" align="left" ><input:select name="tiporep" options="<%=model.tblgensvc.getLista_des()%>" default="" /></td>
              <td width="38" rowspan="2" align="left" ><span class="Letras"><strong>Tipo:</strong></span></td>
              <td width="124" rowspan="2" align="left" ><select name="tipo" id="select2">
                <option value="" selected>Seleccione un item</option>
                <option value="COVE">Exportacion</option>
                <option value="VECO">Importacion</option>
              </select></td>
              <td width="42" rowspan="2" align="left" >Faccial:</td>
              <td width="120" rowspan="2" align="left" ><input name="faccial" type="text" class="textbox" id="faccial" style="width:100% "></td>
              
            </tr>
            <tr class="fila">
              <td align="left" ><span class="Simulacion_Hiper" style="cursor:hand " onClick="window.open('<%=BASEURL%>/consultas/consultasClientes.jsp','','HEIGHT=200,WIDTH=600,SCROLLBARS=YES,RESIZABLE=YES')">Consultar clientes...</span></td>
            </tr>
        </table></td>
      </tr>
    </table>
    <div align="center">
      <img src="<%=BASEURL%>/images/botones/buscar.gif" style="cursor:hand" title="Buscar Planillas" name="buscar"  onClick="if(form1.frontera.value==''){alert('Primero escoja un Pais y luego escoja una Frontera')}else{form1.submit(); this.disabled=true}" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);">&nbsp;
	  <img src="<%=BASEURL%>/images/botones/salir.gif"  name="regresar" style="cursor:hand" title="Salir al Menu Principal" onMouseOver="botonOver(this);" onClick="parent.close();" onMouseOut="botonOut(this);" >
    </div>
  </form>
    <%if(model.remesaService.getRemes()!=null){%>
    <table width="1850"  border="2" align="center">
    <tr>
      <td>
	<table width="100%"  border="0" align="center" cellpadding="0" cellspacing="0" class="barratitulo">
      <tr>
        <td width="50%" class="subtitulo1" colspan='3'>LISTA DE REMESAS ADUANA</td>
        <td width="50%" class="barratitulo" colspan='2'><img src="<%=BASEURL%>/images/titulo.gif"></td>
      </tr>
    </table>
    <table width="100%" border="1" align="center" bordercolor="#999999" class="informacion">
      <tr class="tblTitulo">
	  	<td width="6%" align="center" nowrap>&nbsp;Ult. Actividad</td>
		<td width="21%" align="center" nowrap>&nbsp;Obs. Actividad</td>
		<td width="5%" align="center" nowrap>&nbsp;Factura Comercial</td>
		<td width="20%" align="center" nowrap>&nbsp;Cliente</td>	
		<td width="10%" align="center" nowrap>&nbsp;Destinatarios</td>
		<td width="2%" align="center" nowrap>&nbsp;Ult. Pto. Control</td>
		<td width="2%" align="center" nowrap>&nbsp;Ult. Observacion</td>
		<td width="2%" align="center" nowrap>&nbsp;Fecha Ult. Reporte</td>			        
		<td width="2%" align="center" nowrap>&nbsp;Llegada Frontera</td>	
		<td width="2%" align="center" nowrap>&nbsp;Age. Aduana Exp.</td>
		<td width="2%" align="center" nowrap>&nbsp;Age. Aduana Imp.</td>
		<td width="2%" align="center" nowrap>&nbsp;Almacenadora</td>	
		<td width="2%" align="center" nowrap>&nbsp;Planilla</td>
		<td width="2%" align="center" nowrap>&nbsp;Placa Vehiculo</td>
		<td width="2%" align="center" nowrap>&nbsp;Placa Trailer</td>		
		<td width="2%" align="center" nowrap>&nbsp;Remesa</td>
        <td width="2%" align="center" nowrap>&nbsp;Origen </td>
        <td width="2%" align="center" nowrap>&nbsp;Destino</td>	
        <td width="2%" align="center" nowrap>&nbsp;AG</td>  
		<td width="2%" align="center" nowrap>&nbsp;F</td>	
        <td width="2%" align="center" nowrap>&nbsp;Remitente</td>
		<td width="2%" align="center" nowrap>&nbsp;Tipo Reexpedicion</td>			
        <td width="4%" align="center" nowrap>&nbsp;Planilla-Placa Reexp</td>
      </tr>
      <% Vector remesas = model.remesaService.getRemes();
	  
	  	for(int i = 0; i<remesas.size(); i++){
	  		Aduana a = (Aduana) remesas.elementAt(i);
		%>
      <tr class="<%=i%2==0?"filagris":"filaazul"%>" >
	  	<td class="bordereporte informacion" onClick="window.open('<%=CONTROLLER%>?estado=Infoact&accion=Buscaractclin&carpeta=jsp/trafico/actividad/Infoactividad&pagina=DatosActividad.jsp&cliente=<%=a.getCodcliente()%>&tipo=<%=a.getTipo_viaje()%>&numpla=<%=a.getNumpla()%>&remesa=<%=a.getNumrem()%>&nomcliente=<%=a.getCliente()%>&est=reporte','','status=yes,scrollbars=no,resizable=yes')" style="cursor:hand "  onMouseOver='cambiarColorMouse(this)'  >
	  	  <%=a.getUlt_act_aduana()%>&nbsp;
	  	  <br>
	  	  <%=a.getFec_act_aduana()%>&nbsp;
		</td>
        <td class="bordereporte informacion"><%=a.getObs_act_aduana()%>&nbsp;</td>
		
		<td class="bordereporte informacion" onClick="window.open('<%=BASEURL%>/jsp/aduanas/ModificarFacturaComercial.jsp?numrem=<%=a.getNumrem()%>&coddest=<%=a.getCodigo()%>&factcial=<%=a.getFaccial()%>','','status=yes,scrollbars=no,resizable=yes');" onMouseOver='cambiarColorMouse(this)' style="cursor:hand" title="Modificar factura comercial.."><%=a.getFaccial()%>&nbsp;</td>
		
		<td class="bordereporte informacion"><%=a.getCliente()%>&nbsp;</td>
		<td class="bordereporte informacion" onClick="window.open('<%=CONTROLLER%>?estado=Aduana&accion=Control&opcion=InfoDestinatarios&codcli=<%=a.getCodcliente()%>&numrem=<%=a.getNumrem()%>&factcial=<%=a.getFaccial()%>','','status=yes,scrollbars=no,resizable=yes');" onMouseOver='cambiarColorMouse(this)' style="cursor:hand" title="Modificar destinatarios.." nowrap><%=a.getDestinatarios()%>&nbsp;</td>
		<td class="bordereporte informacion" onClick="window.open('<%=CONTROLLER%>?estado=MovimientoTrafico&accion=BuscarPlanilla&cmd=show&numpla=<%=a.getNumpla()%>','','status=yes,scrollbars=no,resizable=yes');" onMouseOver='cambiarColorMouse(this)' style="cursor:hand" title="Ver datos trafico" nowrap><%=a.getUlt_pto_control()%>&nbsp;</td>
		<td class="bordereporte informacion" nowrap><%=a.getUlt_observacion()%>&nbsp;</td>
		<td class="bordereporte informacion" nowrap><%=a.getFecha_ult_reporte()%>&nbsp;</td>	
		<td class="bordereporte informacion" nowrap><%=a.getFecposllegadafron().indexOf("0099-01")>=0?"":a.getFecposllegadafron()%>&nbsp;</td>
		<td class="bordereporte informacion" onClick="window.open('<%=BASEURL%>/jsp/aduanas/ModificarAgenteAduaneroExportacion.jsp?numrem=<%=a.getNumrem()%>&ageexp=<%=a.getAgeexp()%>&factcial=<%=a.getFaccial()%>','','status=yes,scrollbars=no,resizable=yes');" onMouseOver='cambiarColorMouse(this)' style="cursor:hand" title="Modificar agente exportacion.."><%=a.getAgeexp()%>&nbsp;</td>
		<td class="bordereporte informacion" onClick="window.open('<%=BASEURL%>/jsp/aduanas/ModificarAgenteAduaneroImportacion.jsp?numrem=<%=a.getNumrem()%>&ageimp=<%=a.getAgeimp()%>&factcial=<%=a.getFaccial()%>','','status=yes,scrollbars=no,resizable=yes');" onMouseOver='cambiarColorMouse(this)' style="cursor:hand" title="Modificar agente importacion.."><%=a.getAgeimp()%>&nbsp;</td>
		<td class="bordereporte informacion" onClick="window.open('<%=BASEURL%>/jsp/aduanas/ModificarAlmacenadora.jsp?numrem=<%=a.getNumrem()%>&almacenadora=<%=a.getAlmacenadora()%>&factcial=<%=a.getFaccial()%>','','status=yes,scrollbars=no,resizable=yes');" onMouseOver='cambiarColorMouse(this)' style="cursor:hand" title="Modificar almacenadora.."><%=a.getAlmacenadora()%>&nbsp;</td>
		<td class="bordereporte informacion" onClick="window.open('<%=CONTROLLER%>?estado=Consultar&accion=Planilla&general=ok&numpla=<%=a.getNumpla()%>&placa=&origen=&destino=&despachador=','','status=yes,scrollbars=no,resizable=yes');" onMouseOver='cambiarColorMouse(this)' style="cursor:hand" title="Ver datos Planilla" nowrap><%=a.getNumpla()%>&nbsp;</td>
        <td class="bordereporte informacion" nowrap><%=a.getPlaca()%>&nbsp;</td>
		<td class="bordereporte informacion" onClick="window.open('<%=CONTROLLER%>?estado=Aduana&accion=Control&opcion=InfoTrailer&numpla=<%=a.getNumpla()%>&platlr=<%=a.getPlatlr()%>&numrem=<%=a.getNumrem()%>&tipo_reexp=<%=a.getTipo_reexp()%>&factcial=<%=a.getFaccial()%>','','status=yes,scrollbars=no,resizable=yes');" onMouseOver='cambiarColorMouse(this)' style="cursor:hand" title="Modificar Trailer.." nowrap><%=a.getPlatlr()%>&nbsp;</td>
		<td class="bordereporte informacion" onClick="window.open('<%=CONTROLLER%>?estado=Consultar&accion=Remesa&remesa=<%=a.getNumrem()%>&fechaini=&fechafin=&cliente','','status=yes,scrollbars=no,resizable=yes');"  onMouseOver='cambiarColorMouse(this)' style="cursor:hand" title="Ver datos Remesa" nowrap><%=a.getNumrem()%>&nbsp;</td>
        <td class="bordereporte informacion" nowrap><%=a.getOrirem()%>&nbsp;</td>
        <td class="bordereporte informacion" nowrap><%=a.getDestrem()%>&nbsp;</td> 
        <td class="bordereporte informacion" nowrap><%=a.getAgencia()%>&nbsp;</td> 
		<td class="bordereporte informacion" nowrap><%=a.getFrontera()%>&nbsp;</td>
		<td class="bordereporte informacion" nowrap><%=a.getRemitente()%>&nbsp;</td>  
        <td class="bordereporte informacion" nowrap><%=a.getTipo_reexp()%>&nbsp;</td>		
		<td class="bordereporte informacion" wrap><%=a.getReexpedicion()%>&nbsp;</td>		
		<!-- <td class="bordereporte informacion" < %if(!a.getNumplareex().equals("") && !a.getNumplareex().equals(a.getNumpla())  ){%>onClick="window.open('< %=CONTROLLER%>?estado=Consultar&accion=Planilla&general=ok&numpla=< %=a.getNumplareex()%>&placa=&origen=&destino=&despachador=','','status=yes,scrollbars=no,resizable=yes');" onMouseOver='cambiarColorMouse(this)' style="cursor:hand" title="Ver datos Planilla reexpedicion"< %}%> nowrap>< %=a.getNumplareex().equals(a.getNumpla())?"":a.getNumplareex()%>&nbsp;</td> -->
        <!-- <td class="bordereporte informacion" nowrap>< %=a.getNumplareex().equals(a.getNumpla())?"":a.getPlacareex()%>&nbsp;</td> -->
 </tr>
      <%}
  %>
    </table>
	</td>
      </tr>
  </table>
    <%}%>

</div>
<%=datos[1]%>
</body>
</html>
