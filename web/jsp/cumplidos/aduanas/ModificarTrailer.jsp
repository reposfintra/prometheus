<!--
- Autor            : LREALES
- Date             : 22 de enero de 2007
- Copyrigth Notice : Fintravalores S.A. S.A
-->
<%--
-@(#)
--Descripcion : Pagina JSP, que se encarga de modificar el trailer de una planilla y el tipo de reexpedicion
--%>
<%--
The taglib directive below imports the JSTL library. If you uncomment it,
you must also add the JSTL library to the project. The Add Library... action
on Libraries node in Projects view can be used to add the JSTL 1.1 library.
--%>
<%-- Declaracion de librerias--%>
<%@page contentType="text/html"%>
<%@page import="com.tsp.operation.model.*"%>
<%@page import="com.tsp.util.*"%>
<%@page import="com.tsp.operation.model.beans.*, java.util.*"%>
<%@include file="/WEB-INF/InitModel.jsp"%>
<%@taglib uri="http://jakarta.apache.org/taglibs/input-0.90" prefix="input" %>
<html>
<head>
<script type='text/javascript' src="<%= BASEURL %>/js/boton.js"></script>
<script type='text/javascript' src="<%= BASEURL %>/js/reporte.js"></script>
<script>	
function validar( form ) {

	if ( form.tipo_reexp.value == '' ) {
		alert ( 'Defina el Tipo de Reexpedicion para poder continuar..' );
		return false;
	}
	
	return true;
	
}	
</script>				
<title>Modificar Placa del Trailer</title>
<link href="<%= BASEURL %>/css/estilostsp.css" rel="stylesheet" type="text/css">
<link href="../../../css/estilostsp.css" rel="stylesheet" type="text/css">
</head>
<body onresize="redimensionar()" onload = 'redimensionar()'>
<div id="capaSuperior" style="position:absolute; width:100%; height:100px; z-index:0; left: 0px; top: 0px;">
 <jsp:include page="/toptsp.jsp?encabezado=Modificar informacion"/>
</div>
<div id="capaCentral" style="position:absolute; width:100%; height:87%; z-index:0; left: 0px; top: 100px; overflow: scroll;">
<%
  String msg = (request.getParameter("msg")!=null)?request.getParameter("msg"):"";
  String numrem = (request.getParameter("numrem")!=null)?request.getParameter("numrem"):"";
  String numpla = (request.getParameter("numpla")!=null)?request.getParameter("numpla"):"";
  String trailer = (request.getParameter("trailer")!=null)?request.getParameter("trailer"):"";
  String reexp = (request.getParameter("tipo_reexp")!=null)?request.getParameter("tipo_reexp"):"";
  String fac = (request.getParameter("factcial")!=null)?request.getParameter("factcial"):"";
  
  if ( msg == null || msg.equals("") ) {
%>
<form name="forma1"  method="post" action="<%=CONTROLLER%>?estado=Aduana&accion=Control&opcion=ModificarTrailer" onSubmit="return validar(this);">      
    <table border="2" align="center" width="400">
        <tr>
            <td>
                <table width="100%" class="tablaInferior">
                    <tr>
                        <td width="40%" class="subtitulo1" align="left">Modificar Trailer</td>
                        <td width="60%" class="barratitulo"><img src="<%=BASEURL%>/images/titulo.gif" width="32" height="20"></td>
                    </tr>
                </table>        
                <table width="100%" border="0" align="center" class="tablaInferior">
                    <tr class="fila">
                      <td width="40%">
					  	Placa del Trailer :
					  </td>
                      <td width="60%">
					    <input name="platlr" id="platlr" type="text" class="textbox" size="10" maxlength="7" value="<%=trailer%>">
						<input type="hidden" name="numrem" id="numrem" value="<%=numrem%>">
						<input type="hidden" name="numpla" id="numpla" value="<%=numpla%>">
						<input type="hidden" name="factvieja" id="factvieja" value="<%=fac%>">
					  </td>
                    </tr>
                    <tr class="fila">                        
            		  <td>
					    Tipo de Reexpedici&oacute;n :
					  </td>
                      <td> <%Vector datos = model.remesaService.getVector();%>
                        <!-- <input name="tipo_reexp" id="tipo_reexp" type="text" class="textbox" size="30"> -->
						<select name="tipo_reexp" id="tipo_reexp" class="textbox">
						  <option value="">Seleccione</option>
						    <%
							  for ( int i = 0; i < datos.size(); i ++ ) {
								Aduana a = ( Aduana ) datos.elementAt( i );
							%>
						  <option value="<%=a.getTipo_reexp()%>"><%=a.getTipo_reexp()%></option>
						    <%
							  }
							%>
					    </select>
						<script>
							forma1.tipo_reexp.value = '<%=reexp%>';
						</script>
					  </td>
                    </tr>
              </table>
            </td>
        </tr>
    </table>
<br>
<div align="center">
  <input src="<%=BASEURL%>/images/botones/modificar.gif" type="image" align="middle" name="modificar" title="Modificar.." style="cursor:hand" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);">
  &nbsp;
  <img name="salir" src="<%=BASEURL%>/images/botones/salir.gif" align="middle" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" style="cursor:hand" class="boton" onclick="window.close();">
</div>
</form>
<%
  } else {
%>
<table border="2" align="center">
  <tr>
	<td>
		<table width="100%"  border="1" align="center"  bordercolor="#F7F5F4" bgcolor="#FFFFFF">
			<tr>
				<td width="229" align="center" class="mensajes"><%=msg%></td>
				<td width="29" background="<%=BASEURL%>/images/cuadronaranja.JPG">&nbsp;</td>
				<td width="58">&nbsp;</td>
			</tr>
		</table>
	</td>
  </tr>
</table>		  
<br>
<div align="center">
<input name="salir" src="<%=BASEURL%>/images/botones/salir.gif" type="image" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" style="cursor:hand" class="boton" onclick="parent.opener.location.reload(); window.close();">	  
</div>
<%
  }
%>
</div>
</bodY>
</html>