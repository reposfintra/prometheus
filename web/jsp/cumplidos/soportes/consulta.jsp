<!--
- Autor : Ing. Mario Fontalvo Solano
- Date  : 07 Marzo del 2007
- Copyrigth Notice : Fintravalores S.A. S.A
-->
<%--
-@(#)
--Descripcion : Pagina JSP, para la busqueda de los soportes a asignar.
--%>
<%@page contentType="text/html"%>
<%@ page session   ="true"%> 
<%@ page errorPage ="/error/ErrorPage.jsp"%>
<%@page import="java.util.*,com.tsp.util.*"%>
<%@include file="/WEB-INF/InitModel.jsp"%>
<%
  String path = application.getRealPath(model.menuService.getRealUrl(request.getRequestURI()));
  String datos[] = model.menuService.getContenidoMenu(BASEURL,request.getRequestURI(),path);
  String msg      = (String) request.getAttribute("msg");
  
  
  model.SoporteClienteSvc.buscarClientes();
  model.SoporteClienteSvc.buscarAgencias();
  String agencias = model.SoporteClienteSvc.obtenerCombo( model.SoporteClienteSvc.getListaAgencias(), "name='agcfac' style='width:99%;' ", "" );
  String clientes = model.SoporteClienteSvc.obtenerCombo( model.SoporteClienteSvc.getListaClientes(), "name='codcli' style='width:99%;' ", "" );

  
%>
<html>
<head>
	<title>.: Consulta de Fechas de Soporte de Remesas</title>
	<link href="../../../css/estilostsp.css" rel="stylesheet">
	<script type='text/javascript' src="<%= BASEURL %>/js/validar.js"></script>
	<script type='text/javascript' src="<%= BASEURL %>/js/validarRegistroCheque.js"></script>
	<script type='text/javascript' src="<%= BASEURL %>/js/boton.js"></script>
</head>
<body onresize="redimensionar()" onload = 'redimensionar()'>
<div id="capaSuperior" style="position:absolute; width:100%; height:110px; z-index:0; left: 0px; top: 0px;">
 <jsp:include page="/toptsp.jsp?encabezado=Fechas (Envio y Recibido) Soportes Remesa"/>
</div>
<div id="capaCentral" style="position:absolute; width:100%; height:87%; z-index:0; left: 0px; top: 109px; overflow: scroll;">
<center>
<!--<form name="Formulario" action="<%=BASEURL%>/jsp/general/importacion/parametros.jsp" method="post">-->
<form name="Formulario" action="<%= CONTROLLER %>?estado=Asignacion&accion=FecDocSopRem&opcion=Buscar" method="post">
	<table border="2" width="500">
		<tr>
			<td>			
				<table width="100%"  border="0" cellpadding="0" cellspacing="1">
					<tr>
						<td width="50%" class="subtitulo1">&nbsp;Busqueda de Soportes</td>
						<td class="barratitulo"><img align="left" src="<%=BASEURL%>/images/titulo.gif" width="32" height="20"></td>
					</tr>
				</table>
				<table width="100%"  border="0">
				
					<tr class="filaazul">
                        <td class="bordereporte" colspan="3">
						
						<span style="width:33%; "><input type="radio"  name="tipoB" value="0" checked onClick="mostrarOpciones(this);">&nbsp;Remesa</span>
						<span style="width:33%; "><input type="radio"  name="tipoB" value="1" onClick="mostrarOpciones(this);">&nbsp;Cliente</span>
						<span style="width:32%; "><input type="radio"  name="tipoB" value="2" onClick="mostrarOpciones(this);">&nbsp;Agencia Facturadora</span>
						
						</td>
					</tr>								
				
					<tr class="fila" id="remesa">
                        <td >&nbsp;Remesa</td>
                        <td width="62%"><input name="numrem" type="text" onKeyPress="soloAlfa(event);" maxlength="15" >&nbsp;</td>						
					</tr>				
				
					<tr class="fila" id="cliente">
                        <td >&nbsp;Cliente</td>
                        <td width="62%" align="left" ><%= clientes %></td>						
					</tr>						
							
					<tr class="fila" id="agencia">
                        <td >&nbsp;Agencia facturadora</td>
                        <td width="62%" ><%= agencias %></td>
					</tr>								
														
					<tr class="fila" style="display:none " id="fechas">
						<td >&nbsp;Fecha Cumplido</td>
                        <td >
							<input type="text" name="fecIni" readonly style="text-align:center " onKeyUp=" _keys(this); ">
			                <a href='javascript:void(0)' onclick='if(self.gfPop)gfPop.fPopCalendar(document.Formulario.fecIni);return false;' HIDEFOCUS><img src='<%= BASEURL %>/js/Calendario/cal.gif' width='16' height='16' border='0' alt='De click aqui para escoger la fecha'></a>
							<input type="text" name="fecFin"  readonly style="text-align:center " onKeyUp=" _keys(this); ">
			                <a href='javascript:void(0)' onclick='if(self.gfPop)gfPop.fPopCalendar(document.Formulario.fecFin);return false;' HIDEFOCUS><img src='<%= BASEURL %>/js/Calendario/cal.gif' width='16' height='16' border='0' alt='De click aqui para escoger la fecha'></a>
						</td>
					</tr>			
					<tr class="fila">
						<td >&nbsp;Tipo de fecha a Asignar</td>
                        <td >
							<select style="width:125; " name="tipo">
								<option value="ENVIO" selected>Envio</option>
								<option value="RECIBIDO">Recibido</option>
							</select>
						</td>
					</tr>															
				</table>
				
			</td>
		</tr>
	</table>	
	
	<input type="hidden" name="clase" value="TODAS">
</form>	


<img src='<%=BASEURL%>/images/botones/aceptar.gif'      style='cursor:hand' onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" onclick='validarForma();'>
<img src='<%=BASEURL%>/images/botones/restablecer.gif'  style='cursor:hand' onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" onclick='Formulario.reset();  mostrarOpciones(Formulario.tipo[0]);'>
<img src='<%=BASEURL%>/images/botones/salir.gif'        style='cursor:hand' onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" onclick='window.close();'>



 
   <% if(msg!=null && !msg.trim().equals("")) { %>
        <p>
            <table border="2" align="center">
                <tr>
                    <td>
                        <table width="100%"  border="1" align="center"  bordercolor="#F7F5F4" bgcolor="#FFFFFF">
                            <tr>
                                <td width="229" align="center" class="mensajes"><%=msg%></td>
                                <td width="29" background="<%=BASEURL%>/images/cuadronaranja.JPG">&nbsp;</td>
                                <td width="58">&nbsp;</td>
                            </tr>
                        </table>                     
                    </td>
                </tr>
            </table>
        </p>
  <%}%>	
 
</center>
</div>
<iframe width=188 height=166 name="gToday:normal:<%=BASEURL%>/js/calendartsp/agenda.js:gfPop:<%=BASEURL%>/js/calendartsp/plugins_24.js" id="gToday:normal:<%=BASEURL%>/js/calendartsp/agenda.js:gfPop:<%=BASEURL%>/js/calendartsp/plugins_24.js" src="<%=BASEURL%>/js/calendartsp/ipopeng.htm" scrolling="no" frameborder="0" style="visibility:visible; z-index:999; position:absolute; left:-500px; top:0px;">
</iframe>
</body>
</html>
<script>
	function mostrarOpciones(opcion){ 
		remesa.style.display  = (opcion.value==0 ? 'block' : 'none' );
		cliente.style.display = (opcion.value==1 ? 'block' : 'none' );
		agencia.style.display = (opcion.value==2 ? 'block' : 'none' );		
		fechas.style.display  = (opcion.value!=0 ? 'block' : 'none' );
	}
	function validarForma(){
	 	with (Formulario) {
			if (tipoB[0].checked) {
				if (numrem.value==''){
					alert ('Ingrese el numero de la remesa para poder continuar');
					return false;
				}
			}
			else if (tipoB[1].checked) {
				if (codcli.value==''){
					alert ('Seleccione un cliente para poder continuar');
					return false;
				}
			}
			else if (tipoB[2].checked) {
				if (agcfac.value==''){
					alert ('Seleccione una agencia para poder continuar');
					return false;
				}			
			}
			
			
			if (!tipoB[0].checked){
				if (fecIni.value == "" ){
					alert ('Indique la fecha inicial para poder continuar.');			
					return false;
				}
				if (fecFin.value == "" ){
					alert ('Indique la fecha final para poder continuar.');			
					return false;
				}			
				if (fecIni.value > fecFin.value ){
					alert ('La fecha final debe ser mayor que la fecha inicial');
					return false;
				}
			}
			submit();
		}
	}
	mostrarOpciones(Formulario.tipoB[0]);
	
</script>