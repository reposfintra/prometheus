<!--
- Autor : Ing. Mario Fontalvo Solano
- Date  : 07 Marzo del 2007
- Copyrigth Notice : Fintravalores S.A. S.A
-->
<%--
-@(#)
--Descripcion : Pagina JSP, para la busqueda de los soportes a asignar.
--%>
<%@page contentType="text/html"%>
<%@page session   ="true"%> 
<%@page errorPage ="/error/ErrorPage.jsp"%>
<%@page import="java.util.*,com.tsp.util.*"%>
<%@page import="com.tsp.operation.model.beans.Cumplido,com.tsp.operation.model.beans.Usuario" %>
<%@include file="/WEB-INF/InitModel.jsp"%>
<%@taglib uri="http://jsptags.com/tags/navigation/pager" prefix="pg" %>
<html>
<head>
	<title>Soportes Remesa</title>
	<link href="../../../css/estilostsp.css" rel="stylesheet">
	<script type='text/javascript' src="<%= BASEURL %>/js/validar.js"></script>
	<script type='text/javascript' src="<%= BASEURL %>/js/validarRegistroCheque.js"></script>
	<script type='text/javascript' src="<%= BASEURL %>/js/boton.js"></script>	
</head>
<body onresize="redimensionar()" onload = 'redimensionar()'>
<div id="capaSuperior" style="position:absolute; width:100%; height:110px; z-index:0; left: 0px; top: 0px;">
 <jsp:include page="/toptsp.jsp?encabezado=Consulta Soportes Remesa"/>
</div>
<div id="capaCentral" style="position:absolute; width:100%; height:87%; z-index:0; left: 0px; top: 109px; overflow: scroll;">
<center>

 <% String msg        = (String) request.getAttribute("msg");   
    Vector datos      = (Vector) request.getAttribute("datos");   
    if(datos!=null && !datos.isEmpty()) {		
 %>
	
	<table border="2" width="1400">
		<tr>	
			<td>
			
			<table width="100%"  border="0" cellpadding="0" cellspacing="1">
				<tr>
					<td width="50%" class="subtitulo1">&nbsp;Fechas de Envio y Recibido de los Soportes</td>
					<td class="barratitulo"><img align="left" src="<%=BASEURL%>/images/titulo.gif" width="32" height="20"></td>
				</tr>
			</table>
			<table width="100%" class="tablaInferior" >
				<tr class="tblTitulo" align="center">					
					<td width="336" align="center"  class="bordereporte">Soporte</td>				
					<td width="310" align="center"  class="bordereporte">Agencia Envio</td>
					<td width="149" align="center"  class="bordereporte">Fecha Envio <br>Logico</td>
					<td width="149" align="center"  class="bordereporte">U Envio<br>Logico</td>
					<td width="126" align="center"  class="bordereporte">Fecha Envio  <br>Fisico</td>
					<td width="149" align="center"  class="bordereporte">U Envio<br>Fisico</td>
					<td width="166" align="center"  class="bordereporte">Fecha Recibido <br>Logico</td>					
					<td width="149" align="center"  class="bordereporte">U Recibido<br>Logico</td>
					<td width="122" align="center"  class="bordereporte">Fecha Recibido <br> Fisico</td>
					<td width="149" align="center"  class="bordereporte">U Recibido<br>Fisico</td>
				</tr>
				
				<%
				for (int i = 0; i < datos.size(); i++) {
					Cumplido c        = (Cumplido) datos.get(i);
					String   estilo   = (i % 2 == 0 )?"filagris":"filaazul";
				%>
				<tr class="<%= estilo %>" align="center">					
					<td class="bordereporte" align="left" nowrap >&nbsp;<%= c.getComent() %></td>
					<td class="bordereporte" align="left" nowrap >&nbsp;<%= c.getEstado() %></td>				
 				    <td class="bordereporte" align="center" nowrap><%= (c.getFecha_envio_logico().equals("0099-01-01 00:00")?"":c.getFecha_envio_logico() ) %></td>
					<td class="bordereporte" align="center" nowrap><%= c.getUr_envio_logico() %></td>
					<td class="bordereporte" align="center" nowrap><%= (c.getFecha_envio().equals("0099-01-01 00:00")?"":c.getFecha_envio() ) %></td>
					<td class="bordereporte" align="center" nowrap><%= c.getUr_envio() %></td>
					<td class="bordereporte" align="center" nowrap><%= (c.getFecha_recibido_logico().equals("0099-01-01 00:00")?"":c.getFecha_recibido_logico() ) %></td>
					<td class="bordereporte" align="center" nowrap><%= c.getUr_recibido_logico() %></td>
					<td class="bordereporte" align="center" nowrap><%= (c.getFecha_recibido().equals("0099-01-01 00:00")?"":c.getFecha_recibido() ) %></td>
					<td class="bordereporte" align="center" nowrap><%= c.getUr_recibido() %></td>
				</tr>				
				<% } %>		
			  </table>			
			</td>
		</tr>
	</table>	
	

 <% } %>
 <br>
 <img src="<%=BASEURL%>/images/botones/salir.gif"      name="imgsalir"     onclick="window.close();" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" style="cursor:hand">
 
 
   <% if(msg!=null && !msg.trim().equals("")) { %>
        <p>
            <table border="2" align="center">
                <tr>
                    <td>
                        <table width="100%"  border="1" align="center"  bordercolor="#F7F5F4" bgcolor="#FFFFFF">
                            <tr>
                                <td width="229" align="center" class="mensajes"><%=msg%></td>
                                <td width="29" background="<%=BASEURL%>/images/cuadronaranja.JPG">&nbsp;</td>
                                <td width="58">&nbsp;</td>
                            </tr>
                        </table>                     
                    </td>
                </tr>
            </table>
        </p>
  <%}%>	 
</center>
</div>
</body>
</html>