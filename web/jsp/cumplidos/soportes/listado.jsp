<!--
- Autor : Ing. Mario Fontalvo Solano
- Date  : 07 Marzo del 2007
- Copyrigth Notice : Fintravalores S.A. S.A
-->
<%--
-@(#)
--Descripcion : Pagina JSP, para la busqueda de los soportes a asignar.
--%>
<%@page contentType="text/html"%>
<%@page session   ="true"%> 
<%@page errorPage ="/error/ErrorPage.jsp"%>
<%@page import="java.util.*,com.tsp.util.*"%>
<%@page import="com.tsp.operation.model.beans.Cumplido,com.tsp.operation.model.beans.Usuario" %>
<%@include file="/WEB-INF/InitModel.jsp"%>
<%@taglib uri="http://jsptags.com/tags/navigation/pager" prefix="pg" %>
<html>
<head>
	<title>Soportes Remesa</title>
	<link href="../../../css/estilostsp.css" rel="stylesheet">
	<script type='text/javascript' src="<%= BASEURL %>/js/validar.js"></script>
	<script type='text/javascript' src="<%= BASEURL %>/js/validarRegistroCheque.js"></script>
	<script type='text/javascript' src="<%= BASEURL %>/js/boton.js"></script>	
</head>
<body onresize="redimensionar()" onload = 'redimensionar()'>
<div id="capaSuperior" style="position:absolute; width:100%; height:110px; z-index:0; left: 0px; top: 0px;">
 <jsp:include page="/toptsp.jsp?encabezado=Asignacion Fechas Soportes Remesa"/>
</div>
<div id="capaCentral" style="position:absolute; width:100%; height:87%; z-index:0; left: 0px; top: 109px; overflow: scroll;">
<center>

 <% String msg        = (String) request.getAttribute("msg");   
    Vector datos      = model.SoporteClienteSvc.getSoportes(); 
	int maxRowInView  = 0;
	String numrem     = (String) request.getParameter("numrem");
	String agcfac     = (String) request.getParameter("agcfac");
	String codcli     = (String) request.getParameter("codcli");
	String fecIni     = (String) request.getParameter("fecIni");
	String fecFin     = (String) request.getParameter("fecFin");
	String tipo       = (String) request.getParameter("tipo");
	String clase      = (String) request.getParameter("clase");	
	String tipoB      = (String) request.getParameter("tipoB");	
	Usuario user      = (Usuario) session.getAttribute("Usuario");
	String agencia    = (String) session.getAttribute("Agencia");
    if(datos!=null && !datos.isEmpty()) {
		String style      = "simple";
		String position   = "bottom";
		String index      = "center";	
		int maxPageItems  = 15;
		int maxIndexPages = 10;
		
		int cols = 0;		
        if (tipo.equalsIgnoreCase("ENVIO")   && (clase.equalsIgnoreCase("LOGICA") || clase.equalsIgnoreCase("TODAS"))) cols++;
	    if (tipo.equalsIgnoreCase("ENVIO")   && (clase.equalsIgnoreCase("FISICA") || clase.equalsIgnoreCase("TODAS"))) cols++;
	    if (tipo.equalsIgnoreCase("RECIBIDO")&& (clase.equalsIgnoreCase("LOGICA") || clase.equalsIgnoreCase("TODAS"))) cols++;
	    if (tipo.equalsIgnoreCase("RECIBIDO")&& (clase.equalsIgnoreCase("FISICA") || clase.equalsIgnoreCase("TODAS"))) cols++;
 %>
	
	<pg:pager
		 items         ="<%= datos.size()%>"
		 index         ="<%= index %>"
		 maxPageItems  ="<%= maxPageItems %>"
		 maxIndexPages ="<%= maxIndexPages %>"
		 isOffset      ="<%= true %>"
		 export        ="offset,currentPageNumber=pageNumber"
		 scope         ="request">
			
	<pg:param name="numrem"  value="<%= numrem %>"/>	
	<pg:param name="agcfac"  value="<%= agcfac %>"/>	
	<pg:param name="codcli"  value="<%= codcli %>"/>	
	<pg:param name="fecIni"  value="<%= fecIni %>"/>	
	<pg:param name="fecFin"  value="<%= fecFin %>"/>	
	<pg:param name="tipoB"   value="<%= tipoB  %>"/>			
	<pg:param name="tipo"    value="<%= tipo   %>"/>	
	<pg:param name="clase"   value="<%= clase  %>"/>		
	
	<form name="forma" action="<%= CONTROLLER %>?estado=Asignacion&accion=FecDocSopRem&opcion=Guardar" method="post">
	<table border="2" width="<%= 580+ (cols*150) +100%>">
		<tr>	
			<td>
			
			<table width="100%"  border="0" cellpadding="0" cellspacing="1">
				<tr>
					<td width="50%" class="subtitulo1">&nbsp;Asignacion de Fechas</td>
					<td class="barratitulo"><img align="left" src="<%=BASEURL%>/images/titulo.gif" width="32" height="20"></td>
				</tr>
			</table>
			<table width="100%" class="tablaInferior" >
				<tr class="tblTitulo" align="center">					
					<td width="71" align="center" class="bordereporte" >Remesa</td>		
					<td width="266" align="center"  class="bordereporte">Soporte</td>				
					<td width="243" align="center"  class="bordereporte">Agencia Envio</td>
					<% if (tipo.equalsIgnoreCase("ENVIO")   && (clase.equalsIgnoreCase("LOGICA") || clase.equalsIgnoreCase("TODAS"))) { %>
					<td width="150" align="center"  class="bordereporte">Fecha Envio <br>Logico
					<input type="hidden" name="afel" value="" readonly>
					<a href='javascript:void(0)' onclick='if(self.gfPop)gfPop.fPopCalendar(afel);return false;' HIDEFOCUS><img src='<%= BASEURL %>/js/Calendario/cal.gif' width='13' height='13' border='0' alt='De click aqui para escoger la fecha'></a>
					<img src="<%= BASEURL %>/images/botones/iconos/obligatorio.gif" style="cursor:hand; " onClick=" aplicar('fel',afel.value); " >
					</td>
					<% } %>				
					<% if (tipo.equalsIgnoreCase("ENVIO")   && (clase.equalsIgnoreCase("FISICA") || clase.equalsIgnoreCase("TODAS"))) { %>
					<td width="150" align="center"  class="bordereporte">Fecha Envio  <br>Fisico  
					<input type="hidden" name="afef" value="" readonly>
					<a href='javascript:void(0)' onclick='if(self.gfPop)gfPop.fPopCalendar(afef);return false;' HIDEFOCUS><img src='<%= BASEURL %>/js/Calendario/cal.gif' width='13' height='13' border='0' alt='De click aqui para escoger la fecha'></a>					
					<img src="<%= BASEURL %>/images/botones/iconos/obligatorio.gif" style="cursor:hand; " onClick=" aplicar('fef',afef.value); " >
					</td>
					<% } %>				
					<% if (tipo.equalsIgnoreCase("RECIBIDO")&& (clase.equalsIgnoreCase("LOGICA") || clase.equalsIgnoreCase("TODAS"))) { %>
					<td width="150" align="center"  class="bordereporte">Fecha Recibido <br>
					  Logico
					<input type="hidden" name="afrl" value="" readonly>
					<a href='javascript:void(0)' onclick='if(self.gfPop)gfPop.fPopCalendar(afrl);return false;' HIDEFOCUS><img src='<%= BASEURL %>/js/Calendario/cal.gif' width='13' height='13' border='0' alt='De click aqui para escoger la fecha'></a>					
					<img src="<%= BASEURL %>/images/botones/iconos/obligatorio.gif" style="cursor:hand; " onClick=" aplicar('frl',afrl.value); " >
					</td>					
					<% } %>				
					<% if (tipo.equalsIgnoreCase("RECIBIDO")&& (clase.equalsIgnoreCase("FISICA") || clase.equalsIgnoreCase("TODAS"))) { %>
					<td width="150" align="center"  class="bordereporte">Fecha Recibido <br> Fisico
					<input type="hidden" name="afrf" value="" readonly >
					<a href='javascript:void(0)' onclick='if(self.gfPop)gfPop.fPopCalendar(afrf);return false;' HIDEFOCUS><img src='<%= BASEURL %>/js/Calendario/cal.gif' width='13' height='13' border='0' alt='De click aqui para escoger la fecha'></a>
					<img src="<%= BASEURL %>/images/botones/iconos/obligatorio.gif" style="cursor:hand; " onClick=" aplicar('frf',afrf.value); " >
					</td>
					<% } %>							
				</tr>
				
				<%
				for (int i = offset.intValue(), l = Math.min(i + maxPageItems, datos.size()); i < l; i++) {
					Cumplido c        = (Cumplido) datos.get(i);
					String   estilo   = (i % 2 == 0 )?"filagris":"filaazul";
					//String   agencias = (c.getAgencia_envio().equals("")?model.SoporteClienteSvc.obtenerCombo( model.SoporteClienteSvc.getListaAgencias(), "name='agencia' style='width:99%;' id='agencia"+i+"'  ", c.getAgencia_envio()  ) : "&nbsp;" + c.getEstado() +" <input type='hidden' name='agencia' id='agencia"+i+"' value='"+ c.getAgencia_envio() +"' > ");
					String   agencias =  "&nbsp;" + (!c.getAgencia_envio().equals("")?c.getEstado():agencia) + " <input type='hidden' name='agencia' id='agencia"+i+"' value='"+ (c.getAgencia_envio().equals("")?user.getId_agencia():c.getAgencia_envio()) +"' > ";
					maxRowInView++;
				%>
				<pg:item>				
				<tr class="<%= estilo %>" align="center">					
					<td class="bordereporte" align="center"><%= c.getRemesa() %></td>		
					<td class="bordereporte" align="left" nowrap >&nbsp;<%= c.getComent() %></td>				
					<td class="bordereporte" align="left" ><%= agencias %></td>
						
					<% if (tipo.equalsIgnoreCase("ENVIO") && (clase.equalsIgnoreCase("LOGICA") || clase.equalsIgnoreCase("TODAS")) ) { %>
						<td class="bordereporte" align="left" ><input type="text" name="fel" id="fel<%=i%>" value="<%= (c.getFecha_envio_logico().equals("0099-01-01 00:00")?"":c.getFecha_envio_logico() ) %>" <%= ( c.getFecha_envio_logico().equals("0099-01-01 00:00")?"onKeyUp=' _keys(this);'":"" ) %> style="text-align:center " size="18" readonly>&nbsp;
						<% if(c.getFecha_envio_logico().equals("0099-01-01 00:00")) {  %><a href='javascript:void(0)' onclick='if(self.gfPop)gfPop.fPopCalendar(fel<%= i %>);return false;' HIDEFOCUS><img src='<%= BASEURL %>/js/Calendario/cal.gif' width='16' height='16' border='0' alt='De click aqui para escoger la fecha'></a></td>	<% } %>
					<% } %>
					<% if (tipo.equalsIgnoreCase("ENVIO") && (clase.equalsIgnoreCase("FISICA") || clase.equalsIgnoreCase("TODAS"))) { %>					
						<td class="bordereporte" align="left" ><input type="text" name="fef" id="fef<%=i%>" value="<%= (c.getFecha_envio().equals("0099-01-01 00:00")?"":c.getFecha_envio() ) %>" <%= ( c.getFecha_envio().equals("0099-01-01 00:00")?"onKeyUp=' _keys(this);'":"" ) %>style="text-align:center " size="18" readonly>&nbsp;
						<% if(c.getFecha_envio().equals("0099-01-01 00:00")) {  %><a href='javascript:void(0)' onclick='if(self.gfPop)gfPop.fPopCalendar(fef<%= i %>);return false;' HIDEFOCUS><img src='<%= BASEURL %>/js/Calendario/cal.gif' width='16' height='16' border='0' alt='De click aqui para escoger la fecha'></a></td><% } %>
					<% } %>
					
					<% if (tipo.equalsIgnoreCase("RECIBIDO") && (clase.equalsIgnoreCase("LOGICA") || clase.equalsIgnoreCase("TODAS"))) { %>					
						<td class="bordereporte" align="left" ><input type="text" name="frl" id="frl<%=i%>" value="<%= (c.getFecha_recibido_logico().equals("0099-01-01 00:00")?"":c.getFecha_recibido_logico() ) %>" <%= ( c.getFecha_recibido_logico().equals("0099-01-01 00:00")?"onKeyUp=' _keys(this);'":"" ) %>style="text-align:center " size="18" readonly>&nbsp;
						<% if(c.getFecha_recibido_logico().equals("0099-01-01 00:00")) {  %><a href='javascript:void(0)' onclick='if(self.gfPop)gfPop.fPopCalendar(frl<%= i %>);return false;' HIDEFOCUS><img src='<%= BASEURL %>/js/Calendario/cal.gif' width='16' height='16' border='0' alt='De click aqui para escoger la fecha'></a></td><% } %>				
					<% } %>
					<% if (tipo.equalsIgnoreCase("RECIBIDO") && (clase.equalsIgnoreCase("FISICA") || clase.equalsIgnoreCase("TODAS"))) { %>					
						<td class="bordereporte"  align="left"><input type="text" name="frf" id="frf<%=i%>" value="<%= (c.getFecha_recibido().equals("0099-01-01 00:00")?"":c.getFecha_recibido() ) %>" <%= ( c.getFecha_recibido().equals("0099-01-01 00:00")?"onKeyUp=' _keys(this);'":"" ) %>style="text-align:center " size="18" readonly>&nbsp;
						<% if(c.getFecha_recibido().equals("0099-01-01 00:00")) {  %><a href='javascript:void(0)' onclick='if(self.gfPop)gfPop.fPopCalendar(frf<%= i %>);return false;' HIDEFOCUS><img src='<%= BASEURL %>/js/Calendario/cal.gif' width='16' height='16' border='0' alt='De click aqui para escoger la fecha'></a></td><% } %>									
					<% } %>
					
					
				</tr>			
				
				
				<input type="hidden" name="dstrct"  id="dstrct<%=i%>" value="<%=  c.getDistrict() %>" >
				<input type="hidden" name="remesa"  id="remesa<%=i%>" value="<%=  c.getRemesa() %>" >
				<input type="hidden" name="soporte" id="soporte<%=i%>" value="<%= c.getSoporte() %>" >
				<input type="hidden" name="cagencia" id="cagencia<%=i%>" value="<%= c.getAgencia_envio() %>" >
				<input type="hidden" name="cfel"    id="cfel<%=i%>" value="<%= (c.getFecha_envio_logico().equals("0099-01-01 00:00")?"":c.getFecha_envio_logico() ) %>" >
				<input type="hidden" name="cfef"    id="cfef<%=i%>" value="<%= (c.getFecha_envio().equals("0099-01-01 00:00")?"":c.getFecha_envio() ) %>">
				<input type="hidden" name="cfrl"    id="cfrl<%=i%>" value="<%= (c.getFecha_recibido_logico().equals("0099-01-01 00:00")?"":c.getFecha_recibido_logico() ) %>">
				<input type="hidden" name="cfrf"    id="cfrf<%=i%>" value="<%= (c.getFecha_recibido().equals("0099-01-01 00:00")?"":c.getFecha_recibido() ) %>" >									
					
				<% if (tipo.equalsIgnoreCase("RECIBIDO")) { %>					
					<input type="hidden" name="fel" id="fel<%=i%>" value="<%= (c.getFecha_envio_logico().equals("0099-01-01 00:00")?"":c.getFecha_envio_logico() ) %>" >
					<input type="hidden" name="fef" id="fef<%=i%>" value="<%= (c.getFecha_envio().equals("0099-01-01 00:00")?"":c.getFecha_envio() ) %>">
				<% }  else { %>					
					<input type="hidden" name="frl" id="frl<%=i%>" value="<%= (c.getFecha_recibido_logico().equals("0099-01-01 00:00")?"":c.getFecha_recibido_logico() ) %>">
					<input type="hidden" name="frf" id="frf<%=i%>" value="<%= (c.getFecha_recibido().equals("0099-01-01 00:00")?"":c.getFecha_recibido() ) %>" >					
				<% } %>
					
					
				</pg:item>
				<% } %>
	            <tr>
    	            <td colspan="7" align="center" class="tblTitulo" >
                        <pg:index><jsp:include page="/WEB-INF/jsp/googlesinimg.jsp" flush="true"/></pg:index> 
					</td>
				</tr>				
			  </table>			
			</td>
		</tr>
	</table>	
	
	<input type="hidden" name="numrem" value="<%= numrem %>">
	<input type="hidden" name="agcfac" value="<%= agcfac %>">
	<input type="hidden" name="codcli" value="<%= codcli %>">
	<input type="hidden" name="fecIni" value="<%= fecIni %>">
	<input type="hidden" name="fecFin" value="<%= fecFin %>">
	<input type="hidden" name="tipoB"  value="<%= tipoB  %>">
	<input type="hidden" name="tipo"   value="<%= tipo   %>">
	<input type="hidden" name="clase"  value="<%= clase  %>">
	</form>
	</pg:pager>
 <% } %>
 <br>
 <% if (datos!=null && !datos.isEmpty()) { %><img src="<%=BASEURL%>/images/botones/modificar.gif"  name="imgmodificar" onclick=" if (validar('<%= tipo %>')) { forma.submit(); } " onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" style="cursor:hand"><% } %>
 <img src="<%=BASEURL%>/images/botones/regresar.gif"   name="imgregresar"  onclick="window.location='<%=BASEURL %>/jsp/cumplidos/soportes/consulta.jsp'" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" style="cursor:hand">
 <img src="<%=BASEURL%>/images/botones/salir.gif"      name="imgsalir"     onclick="window.close();" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" style="cursor:hand">
 
 
   <% if(msg!=null && !msg.trim().equals("")) { %>
        <p>
            <table border="2" align="center">
                <tr>
                    <td>
                        <table width="100%"  border="1" align="center"  bordercolor="#F7F5F4" bgcolor="#FFFFFF">
                            <tr>
                                <td width="229" align="center" class="mensajes"><%=msg%></td>
                                <td width="29" background="<%=BASEURL%>/images/cuadronaranja.JPG">&nbsp;</td>
                                <td width="58">&nbsp;</td>
                            </tr>
                        </table>                     
                    </td>
                </tr>
            </table>
        </p>
  <%}%>	 
</center>
</div>
<iframe width=188 height=166 name="gToday:normal:<%=BASEURL%>/js/calendartsp/agenda.js:gfPop:<%=BASEURL%>/js/calendartsp/plugins_24.js" id="gToday:normal:<%=BASEURL%>/js/calendartsp/agenda.js:gfPop:<%=BASEURL%>/js/calendartsp/plugins_24.js" src="<%=BASEURL%>/js/calendartsp/ipopeng.htm" scrolling="no" frameborder="0" style="visibility:visible; z-index:999; position:absolute; left:-500px; top:0px;">
</iframe>
</body>
</html>

<script>
       var maxRow = <%= maxRowInView %>;
	   
	   
	    function aplicar(id, value){ 
	   		if (maxRow > 0){
				for (var i = 0; i < maxRow ; i++){
					var FEF = document.getElementById(id+i);	
					FEF.value = value;   
				}
			}
		}
	   
 	   function validar(tipo){ 
	   		if (maxRow > 0){
			    var estadoG = false;
				for (var i = 0; i < maxRow ; i++){
					var agencia = document.getElementById('agencia'+i);
					if( agencia.parentNode.parentNode.className == 'filaroja')
						agencia.parentNode.parentNode.className = (i%2==0?'filagris':'filaazul');
					var FEL = document.getElementById('fel'+i);
					var FEF = document.getElementById('fef'+i);
					var FRL = document.getElementById('frl'+i);
					var FRF = document.getElementById('frf'+i);			
					var estado = validarFila (tipo, agencia, FEL, FEF, FRL, FRF) ; 
					if ( estado == -1 ){
						agencia.parentNode.parentNode.className = 'filaroja';
						return false;
					} else if (estado == 1) {
						estadoG = true;
					}
				}
				
				
				return estadoG;
			} else  return false;
	   }

       function validarFila (tipo, agencia, FEL, FEF, FRL, FRF){

   			if (tipo == 'ENVIO'){
                
				 if (/*agencia.value=='' &&*/ FEL.value=='' && FEF.value==''){
				 	return 0;
				 }
				
				 /*if (agencia.value==''){
					  alert ('Debe poder asignar una agencia de envio para poder continuar');
					  return -1;             
				 }	*/							 
				 if (FEF.value==''){
					alert ('Defina la fecha de envio fisico para poder continuar');
					return -1;
 				 }             
             } 
			 
             else if (tipo == 'RECIBIDO'){
                
				
				 if ( FRL.value=='' && FRF.value==''){
				 	return 0;
				 }
				 				
				 if (agencia.value==''){
					  alert ('Debe poder asignar una agencia de envio para poder continuar');
					  return -1;             
				 }								 
				 if (FEF.value==''){
					alert ('Defina la fecha de envio fisico para poder continuar');
					return -1;
 				 }             
				 if (FRF.value==''){
					alert ('Defina la fecha de recibido fisico para poder continuar');
					return -1;
 				 }             				 
				 if (FRF.value < FEF.value ){
					alert ('La fecha de recibido fisico debe ser mayor que la fecha de envio fisico.');
					return -1;
 				 }     
				 
				 if( FEL.value!='' && FRL.value!='' ){
					 if (FRL.value < FEL.value ){
						alert ('La fecha de recibido logico debe ser mayor que la fecha de envio logico.');
						return -1;
					 }     				 
				 }
             }					 
           
          return 1;
       }
       


</script>