<%@page contentType="text/html"%>
<%@page session="true"%>
<%@page import="java.util.*"%>
<%@page import="com.tsp.operation.model.*"%>
<%@page import="com.tsp.operation.model.beans.*"%>
<%@page import="com.tsp.operation.controller.*"%>
<%@page import="com.tsp.util.*"%>
<%@include file="/WEB-INF/InitModel.jsp"%>
<html>
<head>
<title>Lista Documentos</title>
<link href="<%= BASEURL %>/css/estilostsp.css" rel="stylesheet" type="text/css">
</head>

<script type='text/javascript' src="<%= BASEURL %>/js/validar.js"></script>
<script type='text/javascript' src="<%= BASEURL %>/js/boton.js"></script>
<script type='text/javascript' src="<%= BASEURL %>/js/reporte.js"></script>
<body > 
<div id="capaSuperior" style="position:absolute; width:100%; height:100px; z-index:0; left: 0px; top: 0px;">
<jsp:include page="/WEB-INF/toptsp.jsp?encabezado=Lista Documentos"/>
</div>

<div id="capaCentral" style="position:absolute; width:100%; height:85%; z-index:0; left: 0px; top: 100px; overflow: scroll;"> 
<FORM name='forma' id='forma' method='POST' action='<%=CONTROLLER%>?estado=Cumplido_doc&accion=Insertar'>
<%
	String path = application.getRealPath(model.menuService.getRealUrl(request.getRequestURI()));
	String datos[] = model.menuService.getContenidoMenu(BASEURL,request.getRequestURI(),path);
	Usuario usuario = (Usuario) session.getAttribute ("Usuario");
	String distrito = (String) session.getAttribute ("Distrito");
	List lst =(List) request.getAttribute("documentos");
	if (lst.size()>0){
	remesa_docto rem_doc;
%>

  <table align="center" width="950" border="2">
  <tr>
  	<td>
  <table width="100%" align="center"> 
          <tr> 
            <td height="50%" class="subtitulo1">Documentos Remesa: <%=request.getParameter("rem")%></td> 
            <td width="50%" class="barratitulo"><img src="<%=BASEURL%>/images/titulo.gif" width="32" height="20" align="left"><%=datos[0]%></td> 
          </tr> 
  </table> 
     <table width="100%" border="2">
	 <tr>
	 <td>
		<table width="100%" border="1" bordercolor="#999999" bgcolor="#F7F5F4" align="center"> 
          <tr class="tblTitulo" align="center"> 
            <td  width="41"> Cump </td> 
            <td  width="74">Tipo de Documento </td> 
            <td  width="78">Documento</td> 
            <td  width="109">Tipo de Documento Relacionado</td> 
            <td  width="87">Documento Relacionado </td> 
          </tr> 
        </table>
	</td>
	<td>
		<table width="100%" border="1" bordercolor="#999999" bgcolor="#F7F5F4" align="center"> 
          <tr class="tblTitulo" align="center"> 
            <td  width="41">Cump</td> 
            <td  width="74">Tipo de Documento </td> 
            <td  width="78">Documento</td> 
            <td  width="109">Tipo de Documento Relacionado</td> 
            <td  width="87">Documento Relacionado</td>
          </tr> 
		  
		</table>
		
	  </td>
	</tr>
	 <tr>
	   <td>
	   		<table width="100%" border="1" bordercolor="#999999" bgcolor="#F7F5F4" align="center">
				<% 
				for (int i = 0; i < (lst.size()+1)/2; i++){		  
					rem_doc = (remesa_docto)lst.get(i);
					%>
					<tr class="<%=(i % 2 == 0 )?"filagris":"filaazul"%>"> 
						<td  width="41" align="center" class="bordereporte">
						<% if(model.cumplido_docService.searchCumplido_doc (distrito,request.getParameter("rem"),request.getParameter("pla"),rem_doc.getTipo_doc(),rem_doc.getDocumento(),rem_doc.getTipo_doc_rel(),rem_doc.getDocumento_rel())){%>
							<img src="<%=BASEURL%>/images/botones/iconos/chulo.gif">
							<%}else{%><input name="<%= i %>" type="checkbox" id="<%= i %>" value="checked" ><%}%></td> 
						<td  width="74" align="center" class="bordereporte"><%= rem_doc.getDocument_name()%></td> 
						<td  width="78" align="center" class="bordereporte"><%= rem_doc.getDocumento()%></td> 
						<td  width="109" align="center" class="bordereporte"><%= ((rem_doc.getDocument_type())==null)?"&nbsp;":rem_doc.getDocument_type() %></td> 
						<td  width="87" align="center" class="bordereporte"><%= ((rem_doc.getDocumento_rel()).length()==0)?"&nbsp;":rem_doc.getDocumento_rel() %></td>                         
					</tr>
				<%}%>
			</table>
	   </td>
	   <td>
			<table width="100%" border="1" bordercolor="#999999" bgcolor="#F7F5F4" align="center">
				<%
				for (int i = ((lst.size()+1)/2); i < lst.size(); i++){		  
					rem_doc = (remesa_docto)lst.get(i);
					%>
					<tr class="<%=(i % 2 == 0 )?"filagris":"filaazul"%>"> 
						<td  width="41" align="center" class="bordereporte">
						<% if(model.cumplido_docService.searchCumplido_doc (distrito,request.getParameter("rem"),request.getParameter("pla"),rem_doc.getTipo_doc(),rem_doc.getDocumento(),rem_doc.getTipo_doc_rel(),rem_doc.getDocumento_rel())){%>
							<img src="<%=BASEURL%>/images/botones/iconos/chulo.gif">
							<%}else{%><input name="<%= i %>" type="checkbox" id="<%= i %>" value="checked" ><%}%></td> 
									<td  width="74" align="center" class="bordereporte"><%= rem_doc.getDocument_name()%></td> 
									<td  width="78" align="center" class="bordereporte"><%= rem_doc.getDocumento()%></td> 
									<td  width="109" align="center" class="bordereporte"><%= ((rem_doc.getDocument_type())==null)?"&nbsp;":rem_doc.getDocument_type() %></td> 
									<td  width="87" align="center" class="bordereporte"><%= ((rem_doc.getDocumento_rel()).length()==0)?"&nbsp;":rem_doc.getDocumento_rel() %></td>                         
								</tr>
							<%}%>
			</table>
		</td>
	 </tr>	
</table>
</td>
</tr>
</table>

<input name="c_pla" type="hidden" id="c_pla" value="<%=request.getParameter("pla")%>">
<input name="c_rem" type="hidden" id="c_rem" value="<%=request.getParameter("rem")%>">
<p>
<div align="center"><img src="<%=BASEURL%>/images/botones/aceptar.gif" style="cursor:hand" title="Enviar un documento" name="ingresar"  onclick="forma.submit();" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);">&nbsp;
<img src="<%=BASEURL%>/images/botones/salir.gif" style="cursor:hand" name="salir" title="Salir al Menu Principal" onClick="parent.close();" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);"></div>
</p> 
<%}else{%>
<br>
		<table border="2" align="center">
			<tr>
				<td>
					<table width="410" height="73" border="1" align="center"  bordercolor="#F7F5F4" bgcolor="#FFFFFF">
						<tr>
							<td width="282" align="center" class="mensajes">Su b&uacute;squeda no arroj&oacute; resultados!</td>
							<td width="28" background="<%=BASEURL%>/images/cuadronaranja.JPG">&nbsp;</td>
							<td width="78">&nbsp;</td>
						</tr>
					</table>
				</td>
			</tr>
		</table><br><div align="center">
		<img src="<%=BASEURL%>/images/botones/salir.gif" style="cursor:hand" name="salir" title="Salir al Menu Principal" onClick="parent.close();" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);">
		</div>
		<br>
	<%}%>
</form>
<%String mensaje = (String) request.getAttribute("mensaje"); 
if(mensaje!=null){%>
	<br>
	<table border="2" align="center">
		<tr>
			<td><table width="100%"  border="1" align="center"  bordercolor="#F7F5F4" bgcolor="#FFFFFF">
				<tr>
					<td width="229" align="center" class="mensajes"><%=mensaje%></td>
					<td width="29" background="<%=BASEURL%>/images/cuadronaranja.JPG">&nbsp;</td>
					<td width="58">&nbsp;</td>
				</tr>
			</table></td>
		</tr>
	</table>
<% } %>	
</div>
<%=datos[1]%>
</body>
</html>
