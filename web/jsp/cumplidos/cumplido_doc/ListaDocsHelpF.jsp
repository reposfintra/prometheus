<!--  
	 - Author(s)       :      Jose de la Rosa
	 - Description     :      AYUDA FUNCIONAL - Docuemntos Cumplidos
	 - Date            :      10/06/2006 
	 - Copyright Notice:      TRANSPORTES SANCHEZ POLO S.A.
-->
<%@ include file="/WEB-INF/InitModel.jsp"%>
<HTML>
<HEAD>
<TITLE>AYUDA FUNCIONAL - Lista Documentos</TITLE>
<META http-equiv=Content-Type content="text/html; charset=windows-1252">
<link href="../css/estilostsp.css" rel="stylesheet" type="text/css"> 
<link href="<%=BASEURL%>/css/estilostsp.css" rel="stylesheet" type="text/css">
<script type='text/javascript' src="<%= BASEURL %>/js/boton.js"></script> 
</HEAD>
<BODY> 
<% String BASEIMG = BASEURL +"/images/ayuda/cumplido/cumplido_doc/"; %>
  <table width="95%"  border="2" align="center">
    <tr>
      <td height="117" >
        <table width="100%" border="0" align="center">
          <tr  class="subtitulo">
            <td height="20"><div align="center">MANUAL DE DOCUMENTOS CUMPLIDOS </div></td>
          </tr>
          <tr class="subtitulo1">
            <td>Descripci&oacute;n del funcionamiento del programa para cumplir documentos de la remesa </td>
          </tr>
          <tr>
            <td  class="ayudaHtmlTexto"><div align="center">
              <p>&nbsp;</p>
              </div></td>
          </tr>
            <tr>
            <td  class="ayudaHtmlTexto"><p class="ayudaHtmlTexto">En la siguiente pantalla tiene permite cumplir los documentos de la remesa esta vista aparecer&aacute; como se ilustra en la Imagen1.</p>
          </td>
          </tr>
            <tr>
            <td  class="ayudaHtmlTexto"><div align="center">
              <p><IMG src="<%=BASEIMG%>ListaDocumentos.JPG" border=0 ></p>
              <p><strong>Imagen1</strong></p>
            </div></td>
          </tr>		  
		<tr>
		<td  class="ayudaHtmlTexto"><p class="ayudaHtmlTexto">&nbsp;</p>
		<p class="ayudaHtmlTexto">Si  cumple los documentos de la remesa  aparecer&aacute; un mensaje como se ilustra en la Imagen2. </p>
		</td>
		</tr>
		<tr>
		<td  class="ayudaHtmlTexto"><div align="center">
		  <p><IMG src="<%=BASEIMG%>MensajeAgregacionDocumentos.JPG" border=0 ></p>
		  <p><strong>Imagen2</strong></p>
		</div></td>
		</tr>				  
		  
		  
		<tr>
          <td  class="ayudaHtmlTexto"><p class="ayudaHtmlTexto">&nbsp;</p>
              <p class="ayudaHtmlTexto">Si no se chequean documentos y presiona el boton ACEPTAR aparecer&aacute; un mensaje como se ilustra en la Imagen3. </p></td>
		  </tr>
		<tr>
          <td  class="ayudaHtmlTexto"><div align="center">
              <p><IMG src="<%=BASEIMG%>MensajeSeleccioneDocumento.JPG" border=0 ></p>
              <p><strong>Imagen3</strong></p>
          </div></td>
		  </tr>

      </table></td>
    </tr>
  </table>
  <p></p>
<center>
<img src = "<%=BASEURL%>/images/botones/salir.gif" style = "cursor:hand" name = "imgsalir" onClick = "parent.close();" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);">
</center>
</BODY>
</HTML>
