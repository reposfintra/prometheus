<!--  
	 - Author(s)       :      Jose de la rosa
	 - Description     :      AYUDA DESCRIPTIVA - cumplidos
	 - Date            :      08/06/2006 
	 - Copyright Notice:      TRANSPORTES SANCHEZ POLO S.A.
-->
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN""http://www.w3.org/TR/html4/loose.dtd">
<%@page session="true"%> 
<%@page errorPage="/error/ErrorPage.jsp"%>
<%@page import="com.tsp.operation.model.beans.*, java.util.*"%>
<%@ include file="/WEB-INF/InitModel.jsp"%>
<%@ taglib uri="http://jakarta.apache.org/taglibs/input-0.90" prefix="input" %>
<html>
<head>
<title>AYUDA DESCRIPTIVA - Lista Documentos</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link href="<%= BASEURL %>/css/estilostsp.css" rel="stylesheet" type="text/css">
<script type='text/javascript' src="<%= BASEURL %>/js/boton.js"></script>
</head>

<body>
<br>
<table width="750"  border="2" align="center">
  <tr>
    <td width="100%" >
      <table width="100%" border="0" align="center">
        <tr  class="subtitulo">
          <td height="24" colspan="2"><div align="center">Lista Documentos </div></td>
        </tr>
        <tr class="subtitulo1">
          <td colspan="2">Cumplir documentos de la remesa </td>
        </tr>
		<tr class="subtitulo1">
          <td colspan="2">INFORMACION DE LOS DOCUMENTOS </td>
        </tr>
        <tr>
          <td  class="fila">Cump</td>
          <td  class="ayudaHtmlTexto">Campo para chequear los documentos de la remesa que desea cumplir.</td>
        </tr>
		<tr>
          <td class="fila">Bot&oacute;n Aceptar </td>
          <td  class="ayudaHtmlTexto">Bot&oacute;n que permite cumplir los documento de la remesa.</td>
        </tr>		
		<tr>
          <td width="35%" class="fila">Bot&oacute;n Salir</td>
          <td width="65%"  class="ayudaHtmlTexto">Bot&oacute;n para salir de la vista 'Cumplidos' y volver a la vista del men&uacute;.</td>
        </tr>
      </table>
    </td>
  </tr>
</table>
<p></p>
<center>
<img src = "<%=BASEURL%>/images/botones/salir.gif" style = "cursor:hand" name = "imgsalir" onClick = "parent.close();" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);">
</center>
</body>
</html>
