<%@ include file="/WEB-INF/InitModel.jsp"%>

<html>
<head>
<title>Descripción de campos de inventarios de clientes por ubicación</title>

<script type='text/javascript' src="<%=BASEURL%>/js/boton.js"></script>
<link href="<%=BASEURL%>/css/estilostsp.css" rel="stylesheet" type="text/css"> 
</head>

<body>
<% String BASEIMG = BASEURL +"/images/botones/"; %> 
<br>
<table width="696"  border="2" align="center">
  <tr>
    <td width="811" >
      <table width="100%" border="0" align="center">
        <tr  class="subtitulo">
          <td height="24" colspan="2"><div align="center">INVENTARIOS CLIENTES POR UBICACION </div></td>
        </tr>
        <tr class="subtitulo1">
          <td colspan="2">INFORMACION DEL CLIENTE </td>
        </tr>
        <tr>
          <td width="123" class="fila"> Cliente </td>
          <td width="551"  class="ayudaHtmlTexto"> Campo para ingresar un c&oacute;digo de seis n&uacute;meros que representa un cliente al que se le desea buscar su inventario de discrepancias.</td>
        </tr>
        <tr>
          <td  class="fila"> Consultar clientes...</td>
          <td  class="ayudaHtmlTexto"> Link que muestra una pantalla para la busqueda de clientes. </td>
        </tr>
        <tr>
          <td  class="fila">Bot&oacute;n busqueda </td>
          <td  class="ayudaHtmlTexto"> Bot&oacute;n en forma de lupa que permite la b&uacute;squeda del cliente y sus posibles ubicaciones.</td>
        </tr>
        <tr>
          <td width="123"  class="fila"> Bot&oacute;n ubicaci&oacute;n</td>
          <td width="551"  class="ayudaHtmlTexto">Bot&oacute;n que permite seleccionar la ubicaci&oacute;n donde se desea consultar el inventario por discrepancias</td>
        </tr>
		<tr>
          <td width="123"  class="fila"> Bot&oacute;n cancelar</td>
          <td width="551"  class="ayudaHtmlTexto">Bot&oacute;n que permite resetear el formulario y cancela la operaci&oacute;n de b&uacute;squeda realizada.</td>
        </tr>
		<tr>
          <td width="123"  class="fila"> Bot&oacute;n salir </td>
          <td width="551"  class="ayudaHtmlTexto">Bot&oacute;n que cierra la ventana de b&uacute;squeda.</td>
        </tr>
		<tr>
          <td width="123"  class="fila"> Bot&oacute;n exportar </td>
          <td width="551"  class="ayudaHtmlTexto">Bot&oacute;n que exporta a excel la consulta de los productos por discrepancia que se tienen en inventario </td>
        </tr>
      </table>
    </td>
  </tr>
</table>
<br>
<table width="416" align="center">
	<tr>
	<td align="center">
		<img src="<%=BASEURL%>/images/botones/salir.gif" style="cursor:pointer" name="c_salir" onClick="window.close();" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" align="absmiddle">
	</td>
	</tr>
</table>
<p>&nbsp;</p>

<p>&nbsp;</p>
<p>&nbsp; </p>
</body>
</html>
