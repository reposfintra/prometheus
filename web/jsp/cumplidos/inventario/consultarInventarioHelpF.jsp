<%@ include file="/WEB-INF/InitModel.jsp"%>

<HTML>
<HEAD>
<TITLE>Manual de Consulta de inventarios por discrepancia de los clientes</TITLE>
<META http-equiv=Content-Type content="text/html; charset=windows-1252">
<script type='text/javascript' src="<%=BASEURL%>/js/boton.js"></script>
<link href="../css/estilostsp.css" rel="stylesheet" type="text/css"> 
<link href="<%=BASEURL%>/css/estilostsp.css" rel="stylesheet" type="text/css"> 
</HEAD>
<BODY> 
  <table width="95%"  border="2" align="center"> 
    <tr>
      <td height="117" >
        <table width="100%" border="0" align="center">
          <tr  class="subtitulo">
            <td height="20"><div align="center">CONSULTA DE INVENTARIOS POR DISCREPANCIA DE CLIENTES </div></td>
          </tr>
          <tr class="subtitulo1">
            <td>Descripci&oacute;n del funcionamiento del consulta de inventario de discrepancias de los clientes </td>
          </tr>
<tr>
            <td  class="ayudaHtmlTexto"><span class="ayudaHtmlTexto">En la siguiente pantalla se puede colocar el c&oacute;digo del cliente, si se conoce o utilizar la opci&oacute;n Consultar clientes.</span></td>
          </tr>
<tr>
            <td  class="ayudaHtmlTexto"><div align="center"><IMG src="<%=BASEURL%>/images/ayuda/inventario/img001.JPG" width="650" height="197"  
border=0 v:shapes="_x0000_i1143"></div></td>
          </tr>
<tr>
            <td  class="ayudaHtmlTexto"><span class="ayudaHtmlTexto">Si se ingresa por el link de Consultar clientes, aparece la siguiente pantalla, en donde se coloca el nombre del cliente. Se puede utilizar el s&iacute;mbolo % como comod&iacute;n. Es decir que donde se coloque puede existir cualquier informaci&oacute;n adicional que lo reemplace.</span></td>
          </tr><tr>
            <td  class="ayudaHtmlTexto"><span class="ayudaHtmlTexto">Y se presiona BUSCAR</span></td>
          </tr>
<tr>
            <td  class="ayudaHtmlTexto"><div align="center"><IMG height=105 src="<%=BASEURL%>/images/ayuda/inventario/img002.jpg" width=533 
border=0 v:shapes="_x0000_i1052"></div></td>
          </tr>
<tr>
            <td  class="ayudaHtmlTexto"><span class="ayudaHtmlTexto">En la pantalla aparecen los nombres de los clientes que coincidan con la b&uacute;squeda realizada. Al nombre que se requiera, se le presiona CLICK</span></td>
          </tr>
<tr>
            <td  class="ayudaHtmlTexto"><div align="center"><IMG  src="<%=BASEURL%>/images/ayuda/inventario/img003.jpg" 
border=0 v:shapes="_x0000_i1054"></div></td>
          </tr>
<tr>
            <td  class="ayudaHtmlTexto"><span class="ayudaHtmlTexto">En la siguiente pantalla se presiona el icono Buscar (con icono lupa) y se escoge la ubicaci&oacute;n para listar los productos en discrepancia.</span></td>
          </tr>
<tr>
            <td  class="ayudaHtmlTexto"><div align="center"><IMG src="<%=BASEURL%>/images/ayuda/inventario/img004.JPG" 
border=0 v:shapes="_x0000_i1061"></div></td>
          </tr>
<tr>
            <td  class="ayudaHtmlTexto"><span class="ayudaHtmlTexto">Una vez seleccionada la ubicaci&oacute;n, aparecen todos los productos en inventario por discrepancia que se encuentran en esta. Para exportar el listado a un archivo Excel, se debe presionar el bot&oacute;n &quot;Exportar&quot;.</span></td>
          </tr>
<tr>
            <td  class="ayudaHtmlTexto"><div align="center"><IMG height=380 src="<%=BASEURL%>/images/ayuda/inventario/img005.JPG" width=646 
border=0 v:shapes="_x0000_i1083"></div></td>
          </tr>
<tr>
            <td  class="ayudaHtmlTexto">Una vez exportado el listado, aparecer&aacute; el siguiente mensaje de confirmaci&oacute;n.</td>
          </tr>
<tr>
            <td  class="ayudaHtmlTexto"><div align="center"><IMG height=202 src="<%=BASEURL%>/images/ayuda/inventario/img006.JPG" width=648></div></td>
          </tr>
		  </td>
		  </tr>
		  </table>
  </table>
<br>
<table width="416" align="center">
	<tr>
	<td align="center">
		<img src="<%=BASEURL%>/images/botones/salir.gif" style="cursor:pointer" name="c_salir" onClick="window.close();" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" align="absmiddle">
	</td>
	</tr>
</table>
</BODY>
</HTML>
