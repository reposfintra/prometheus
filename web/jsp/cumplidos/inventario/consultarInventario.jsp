<%@page session="true"%>
<%@page errorPage="../error/ErrorPage.jsp"%>
<%@page import="com.tsp.operation.model.beans.*, java.util.*, java.text.*"%>
<%@ include file="/WEB-INF/InitModel.jsp"%>
<%@ taglib uri="http://java.sun.com/jstl/core" prefix="c" %>
<%@ taglib uri="http://jakarta.apache.org/taglibs/input-0.90" prefix="input" %>
<html>
<head>
    <title>Consultar Inventarios</title>
    <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">    
    <script language="javascript" src="<%=BASEURL%>/js/general.js"></script>
    <script type='text/javascript' src="<%=BASEURL%>/js/boton.js"></script>
    <script language="javascript" src="<%=BASEURL%>/js/utilidades.js"></script>   
    <script type='text/javascript' src="<%=BASEURL%>/js/validar.js"></script>
    <link href="<%=BASEURL%>/css/estilostsp.css" rel="stylesheet" type="text/css">    

</head>
<%
    String path = application.getRealPath(model.menuService.getRealUrl(request.getRequestURI()));
    String datos[] = model.menuService.getContenidoMenu(BASEURL,request.getRequestURI(),path);
    String accion = CONTROLLER+"?estado=Consulta&accion=Inventario";        
    String codCliente = request.getParameter( "cliente" );
    String cliente = (codCliente == null)? "": codCliente.trim();
    String codigoUbicacion = request.getParameter("codubi");
    String ubicacion = (codigoUbicacion == null)? "": codigoUbicacion.trim();
    Cliente objCliente = null;
    String mensaje = (String) request.getAttribute("mensaje");    
%>
<body onload="form1.cliente.focus()">
    <div id="capaSuperior" style="position:absolute; width:100%; height:100px; z-index:0; left: 0px; top: 0px;">
        <jsp:include page="/WEB-INF/toptsp.jsp?encabezado=Inventarios Clientes"/>
    </div>    
    <div id="capaCentral" style="position:absolute; width:100%; height:85%; z-index:0; left: 0px; top: 100px; overflow: scroll;">                
        <table width="610"  border="2" align="center">
            <tr>
            <td>	     
                <table width="100%" align="center" class="tablaInferior">
                    <tr>
                        <th colspan="3"  scope="row">
                        <table width="100%" border="0" cellpadding="0" cellspacing="0">
                            <tr>
                                <td height="22" colspan=2 class="subtitulo1">Inventario de clientes por ubicaci�n</td>
                                <td width="232" class="barratitulo"><img src="<%=BASEURL%>/images/titulo.gif" width="32" height="20" align="left"><%=datos[0]%></td>                                
                            </tr>
                        </table>
                        </th>
                    </tr>
                    <tr class="subtitulo1">
                        <th colspan="3"  scope="row"><div align="left">INFORMACION DEL CLIENTE</div></th>
                    </tr>   
                    <form name="form1" id="form1" method="post" action="<%=accion%>&opc=1">
                    <tr class="fila">
                        <td class="fila" width="220" rowspan="2"><div align="left">CLIENTE</div></td>
                        <td width="380">
                            <p><span class="Simulacion_Hiper" style="cursor:hand " onClick="window.open('<%=BASEURL%>/jsp/cumplidos/inventario/buscarCliente.jsp','','HEIGHT=200,WIDTH=600,STATUS=YES,SCROLLBARS=YES,RESIZABLE=YES')">Consultar clientes...</span></p>
                        </td>
                    </tr>
                    <tr>
                    <td colspan="2" class="fila">                                                                                        
                        <input name="cliente" type="text" class="textbox" id="cliente" value="<%=cliente%>" onKeyPress="soloAlfa(event);" onkeyup="validarEnter(event,'form1');" maxlength="6">
                        <input type="image" alt="Haga click para buscar el cliente" src="<%=BASEURL%>/images/botones/iconos/lupa.gif" width="15" height="15" style="cursor:hand">
                        <br>
                        <table width="100%" border="0" cellpadding="3" cellspacing="2" bordercolor="#CCCCCC" class="fila" >
                            <tr class="fila"> 
                            <td class="fila">
                                <strong>
                                    <%
                                        if(codCliente != null && !codCliente.trim().equals("") ){
                                            objCliente = model.clienteService.getCliente();
                                            if(objCliente != null){
                                                out.print( objCliente.getNomcli() );
                                            }                                        
                                        }
                                    %>
                                </strong>
                            </td>
                            </tr>                            
                        </table>                                                           
                    </td>                    
                    </tr>
                    </form>
                <%                    
                    TreeMap lista = null; 
                    if(codCliente != null && !codCliente.trim().equals("") && !objCliente.getCodcli().equals("")  ){
                %>
                    <tr class="fila">
                    <td class="fila" width="220"><div align="left">UBICACION</div></td>
                    <td class="fila" width="380">
                        <div id="lstub">
                                    <%
                                            codCliente = codCliente.trim();                                            
                                            lista = model.cliente_ubicacionService.getUbicacion_prod();                                        
                                    %>
                            <input:select name="ubicaciones" options="<%=lista%>" attributesText="<%= " id='ubicaciones' class='textbox' style='width:230' onChange= \\"location.replace('"+accion+"&opc=2&cliente='+form1.cliente.value+'&codubi='+ubicaciones.value)\\" " %>" default="<%=ubicacion%>" /> 
                            
                        </div>
                    </td>
                    </tr>
                <%  }
                %>        
                </table>
            </td>
            </tr>
        </table>
        <br>
        <table width="416" align="center">
            <tr>
            <td align="center">
                <%
                    if(codCliente != null && !codCliente.trim().equals("") ){
                %>
                <img src="<%=BASEURL%>/images/botones/cancelar.gif" style="cursor:pointer" name="c_cancelar" onClick="location.replace('<%=CONTROLLER%>?estado=Menu&accion=Cargar&carpeta=/jsp/cumplidos/inventario&pagina=consultarInventario.jsp&marco=no');" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" align="absmiddle">
                <%
                    }
                %>
                <img src="<%=BASEURL%>/images/botones/salir.gif" style="cursor:pointer" name="c_salir" onClick="window.close();" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" align="absmiddle">
            </td>
            </tr>
        </table>
        <div id="working" class="letrasFrame" align="center" ><br></div>
        <br>
        <%if(mensaje!=null){%>
        <table border="2" align="center">
            <tr>
                <td height="45"><table width="410" height="41" border="1" align="center"  bordercolor="#F7F5F4" bgcolor="#FFFFFF">
                    <tr>
                        <td width="282" height="35" align="center" class="mensajes"><%=mensaje%></td>
                        <td width="28" background="<%=BASEURL%>/images/cuadronaranja.JPG">&nbsp;</td>
                        <td width="78">&nbsp;</td>
                    </tr>
                </table></td>
            </tr>
        </table>
        <br>
    <%}%>
        <div id="listaProductos" align="center">
            <%  if( codCliente != null && codigoUbicacion != null ){
                    codCliente = codCliente.trim();
                    codigoUbicacion = codigoUbicacion.trim();
					Ubicacion ubc = model.ubService.getUb();
            %>
            <table width="600" align="center">
                <tr>
                    <td align="center">
                        <img alt="Haga click para exportar a excel" src="<%=BASEURL%>/images/botones/iconos/excel.gif" style="cursor:pointer" name="c_exportar" onClick="location.replace('<%=CONTROLLER%>?estado=ReporteInventario&accion=DiscrepanciaCliente&cliente='+form1.cliente.value+'&codubi='+ubicaciones.value+'&nomubicacion=<%=ubc.getDescripcion()%>');">
                    </td>
                </tr>
            </table>
			<%Vector productos = model.productoService.getProductos();
  			  if( productos.size() > 0 ){%>
            <table width="610"  border="2" align="center">
                <tr>
                <td>
                    <table width="600" class="tablaInferior">
                        <tr>
                            <td height="22" colspan=2 class="subtitulo1">Inventarios de Productos en <%=ubc.getDescripcion()%> <input type="hidden" value="<%=ubc.getDescripcion()%>" id="nomubicacion" name="nomubicacion"></td>
                            <td width="232" class="barratitulo"><img src="<%=BASEURL%>/images/titulo.gif" width="32" height="20"></td>
                        </tr>
                    </table>
                    <table width="600" border="1" align="center" bordercolor="#999999" bgcolor="#F7F5F4">    
                        <tr class="tblTitulo">
                            <td >C�digo</td>
                            <td >Producto</td>
                            <td >Cantidad </td>
                            <td >Nota D�bito</td>
                        </tr>
                <%      
                        
                        if(productos == null){
                                productos=new Vector();
                        }
                        for(int i=0; i<productos.size(); i++){
                            Producto p = (Producto) productos.elementAt(i);	
                        %>
                        <tr class="<%=i%2==0?"filagris":"filaazul"%>">    
                        <td width="60" class="bordereporte"><%=p.getCodigo()%>&nbsp;</td>
                        <td width="240" class="bordereporte">
                        <span class="Letras"><%=p.getDescripcion()%>&nbsp;<strong>

                        </strong></span></td>
                        <td width="60" class="bordereporte"><%=p.getUnidad()%>&nbsp;</td>

                        <td width="240" class="bordereporte"><%=p.getNota_debito()%>&nbsp;</td>
                        </tr>            
                <%    }
                 	 if( productos.size() == 0 ){%>
                        <tr class="filaresaltada">
                        <td colspan="4"><div class="letrasFrame" align="center">No existen registros</div></td>
                        </tr>
                <% 
                      }
                %>
                        <tr class="filaresaltada">
                        <td colspan="4"><div class="letrasFrame" align="center"></div></td>
                        </tr>
                    </table>                        
                </td>
                </tr>
            </table>
            <br>
            <%     if( productos.size() > 14 ){%>
            <table width="416" align="center">
                <tr>
                <td align="center">
                    <img src="<%=BASEURL%>/images/botones/exportarExcel.gif" style="cursor:pointer" name="c_exportar" onClick="location.replace('<%=CONTROLLER%>?estado=ReporteInventario&accion=DiscrepanciaCliente&cliente='+form1.cliente.value+'&codubi='+ubicaciones.value);" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);">
                    <%
                        if(codCliente != null && !codCliente.trim().equals("") ){
                    %>
                    <img src="<%=BASEURL%>/images/botones/cancelar.gif" name="c_salir" style="cursor:pointer" onClick="location.replace('<%=CONTROLLER%>?estado=Menu&accion=Cargar&carpeta=/jsp/cumplidos/inventario&pagina=consultarInventario.jsp&marco=no');" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" >
                    <%
                        }
                    %>
                    
                    <img src="<%=BASEURL%>/images/botones/salir.gif" name="c_salir" style="cursor:pointer" onClick="window.close();" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);">
                </td>
                </tr>
            </table>
            <%     }
			     }
				 else{%>
            <table border="2" align="center">
                <tr>
                    <td><table width="100%"  border="1" align="center"  bordercolor="#F7F5F4" bgcolor="#FFFFFF">
                    <tr>
                    <td width="229" align="center" class="mensajes">No existen registros</td>
                    <td width="29" background="<%=BASEURL%>/images/cuadronaranja.JPG">&nbsp;</td>
                    <td width="58">&nbsp;</td>
                </tr>
                </table></td>
                </tr>
            </table>
          <%     }
			  }
            %>            
        </div>
    </div>
    <%=datos[1]%>
</body>
</html>
