<%@ page session="true"%>
<%@ page errorPage="../error/error.jsp"%>
<%@page import="com.tsp.operation.model.beans.*, java.util.*"%>
<%@ include file="/WEB-INF/InitModel.jsp"%>
<%@ taglib uri="http://jsptags.com/tags/navigation/pager" prefix="pg" %>
<html>
<head>
<title>Buscar Cliente</title>
<script language="javascript" src="js/validar.js"></script>
<script SRC='<%=BASEURL%>/js/boton.js'></script>
<link href="<%=BASEURL%>/css/estilostsp.css" rel="stylesheet" type="text/css">
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link href="../css/estilostsp.css" rel="stylesheet" type="text/css">
</head>
<body onLoad="form1.nomcli.focus();">
<%String accion =CONTROLLER+"?estado=ConsultarBuscar&accion=Cliente";
if(request.getParameter("despacho")!=null){
	accion = accion +"&despacho=ok";
}%>
<form name="form1" method="post" action="<%=accion%>" >
<table width="473"  border="2" align="center">
  <tr>
    <td><table width="100%" class="tablaInferior">
    <tr>
      <td colspan="2" nowrap><table width="100%"  border="0" class="barratitulo">
          <tr>
            <td width="37%" class="subtitulo1">BUSQUE EL CLIENTE</td>
            <td width="63%"><img src="<%=BASEURL%>/images/titulo.gif"></td>
          </tr>
        </table></td>
    </tr>
    <tr class="fila">
      <td width="231" nowrap><strong>Escriba el nombre</strong></td>
      <td width="326" nowrap><input name="nomcli" type="text" id="nomcli" size="35" maxlength="30" style="text-transform: uppercase; "></td>
    </tr>
    <tr class="fila">
      <td colspan="2" nowrap><div align="center">
        <input type="image" src="<%=BASEURL%>/images/botones/buscar.gif" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" name="Submit" >
      </div></td>
    </tr>
  </table></td>
  </tr>
</table>

  
</form>
<%if(request.getParameter("mensaje")!=null){%>
<table border="2" align="center">
      <tr>
        <td height="45"><table width="410" height="41" border="1" align="center"  bordercolor="#F7F5F4" bgcolor="#FFFFFF">
            <tr>
              <td width="282" height="35" align="center" class="mensajes">No se encontraron resultados en la busqueda.</td>
              <td width="28" background="<%=BASEURL%>/images/cuadronaranja.JPG">&nbsp;</td>
              <td width="78">&nbsp;</td>
            </tr>
        </table></td>
      </tr>
    </table>
<%}%>

<iframe width=188 height=166 name="gToday:normal:agenda.js:gfPop:plugins.js" id="gToday:normal:agenda.js:gfPop:plugins.js" src="js/Calendario/ipopeng.htm" scrolling="no" frameborder="0" style="visibility:visible; z-index:999; position:absolute; left:-500px; top:0px;"> </iframe>

</body>
</html>

