
<!--
- Autor : Ing. Juan Manuel Escandon Perez
- Date  : Noviembre de 2005
- Modificado por : LREALES
- Date  : Octubre de 2006
- Copyrigth Notice : Fintravalores S.A. S.A
-->
<%--
-@(#)
--Descripcion : Pagina JSP, que muestra los datos de entrada para 
--              la inicializacion del Reporte de cumplidos
--%>
<%@page contentType="text/html"%>
<%@page session="true"%>
<%@page import="com.tsp.operation.model.beans.*"%>
<%@page import="com.tsp.util.*"%>
<%@page import="java.util.*"%>
<%@include file="/WEB-INF/InitModel.jsp"%>
<%@ taglib uri="http://jakarta.apache.org/taglibs/input-0.90" prefix="input" %>
<%@ taglib uri="http://www.sanchezpolo.com/sot" prefix="tsp"%>
<% String Mensaje       = (request.getParameter("Mensaje")!=null)?request.getParameter("Mensaje"):""; %>
<html>
    <head>
        <title>Reporte Cumplidos</title>
        <link href="<%= BASEURL %>/css/estilostsp.css" rel='stylesheet'>
        <link href="../../../../css/estilostsp.css" rel="stylesheet" type="text/css">
    </head>
    <script type='text/javascript' src="<%= BASEURL %>/js/validarReporteCumplido.js"></script>
    <script src='<%= BASEURL %>/js/date-picker.js'></script>
    <script type='text/javascript' src="<%= BASEURL %>/js/boton.js"></script>
    <script language="JavaScript" type="text/javascript" src="<%=BASEURL%>/js/finanzas/presupuesto/move.js"></script>
	<script>
		function validar( form ){
		
			if ( form.c_agencia.value == 'NADA' ){
				alert( 'Debe seleccionar alguna agencia para poder continuar...' );
				return false;
			}
			
			if ( form.FechaI.value == '' ){
				alert( 'Defina la fecha de inicio de busqueda para poder continuar...' );
				return false;
			}
			
			if ( form.FechaF.value == '' ){
				alert( 'Defina la fecha de fin de busqueda para poder continuar...' );
				return false;
			}
			
			var fecha1 = form.FechaI.value.replace( /-|:| /gi, '' );
			var fecha2 = form.FechaF.value.replace( /-|:| /gi, '' );
			 
			var fech1 = parseFloat( fecha1 );
			var fech2 = parseFloat( fecha2 );
			 
			if( fecha2 < fecha1 ) { 				   
				 alert( 'La fecha final debe ser mayor que la fecha inicial!' );
				 return false;					 
			}
			
			return true;
			
		}				
	</script>
    <body  onLoad="Recorrido();redimensionar();" onResize="redimensionar()" onMouseMove="_Move('rev');" onMouseUp="_stopMove();">
        <form name="forma" id="forma" method="post" action="<%=CONTROLLER%>?estado=Reporte&accion=Cumplido" onSubmit="return validar(this);">
        <div id="capaSuperior" style="position:absolute; width:100%; height:100px; z-index:0; left: 0px; top: 0px;">
            <jsp:include page="/WEB-INF/toptsp.jsp?encabezado=Parametros de Busqueda"/> 
        </div>

        <div id="capaCentral" style="position:absolute; width:100%; height:85%; z-index:0; left: 0px; top: 100px; overflow: scroll;"> 

           
			<%TreeMap agencias = model.agenciaService.listar();%>

            <table width="300"  border="2" align="center" cellpadding="0" cellspacing="0">
                <tr>
                    <td>
                    <table width="100%" class="tablaInferior">
                    <tr>
                        <td colspan="2" >
                            <table width="100%"  border="0" cellpadding="0" cellspacing="1" class="barratitulo">
                                <tr>
                                    <td width="56%" class="subtitulo1">&nbsp;Reporte Cumplidos </td>
                                    <td width="44%" class="barratitulo"><img src="<%=BASEURL%>/images/titulo.gif" width="32" height="20"></td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr class="fila">
                        <td>Fecha Inicial</td>
                        <td><tsp:Date id="FechaI" formulario="forma"  name="FechaI" fechaInicial="hoy"/></td>
                    </tr>
                    <tr class="fila">
                        <td>Fecha Final</td>
                        <td><tsp:Date id="FechaF" formulario="forma"  name="FechaF" fechaInicial="hoy"/></td>
                    </tr>
                    <tr class="fila">
                    <td>Agencia </td>      
                    <td ><input:select name="c_agencia" attributesText="class='textbox'" options="<%= agencias %>" default=""/></td>
                </tr>
                    <tr class="fila">
                        <td>Usuario</td>      
                        <td ><input name="usuario" type="text" class="textbox" id="usuario" ></td>
                    </tr>
                    </table>
                </td>
                </tr>
            </table>

            <br>
            <center>
                <input src="<%=BASEURL%>/images/botones/aceptar.gif" name="c_aceptar" id="c_aceptar" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" type="image" style="cursor:hand"> 
                <img src="<%=BASEURL%>/images/botones/cancelar.gif" name="c_cancelar" onClick="forma.reset();" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" style="cursor:hand"> 
                <img src="<%=BASEURL%>/images/botones/salir.gif" name="c_salir" onClick="parent.close();" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" style="cursor:hand">	
                <img src="<%=BASEURL%>/images/botones/detalles.gif" title='Agregar Columnas visibles'   name="imgmod"  onClick="Show();"  style="cursor:hand">
            </center>
            <br>
            <%if(!Mensaje.equals("")){%>
            <table border="2" align="center">
                <tr>
                    <td>
                        <table width="100%"  border="1" align="center"  bordercolor="#F7F5F4" bgcolor="#FFFFFF">
                            <tr>
                                <td width="229" align="center" class="mensajes"><%=Mensaje%></td>
                                <td width="29" background="<%=BASEURL%>/images/cuadronaranja.JPG">&nbsp;</td>
                                <td width="58">&nbsp;</td>
                            </tr>
                        </table>
                    </td>
                </tr>
            </table>            
			<%}%>
		
        </div>
        <!--  Tabla de Columnas  -->
        <div  id='rev'  style=" position:absolute;  visibility:hidden; ">  
 
            <script language="JavaScript" type="text/javascript" src="<%=BASEURL%>/js/script.js"></script> 
            <table width="600" border="1" align="center" bordercolor="#123456" cellpadding='0' cellspacing='0'>
                <tr class="barratitulo">
                    <td colspan='3' >
                        <table  width='100%' cellpadding='0' cellspacing='0'>
                            <tr>
                                <td align="left" width='55%' class="subtitulo1">&nbsp;Columnas Visibles</td>
                                <td align="left" width='*'  ><img src="<%= BASEURL %>/images/titulo.gif" width="32" height="20"></td>
                            </tr>
                        </table>
                    </td>
                </tr>             
            </table>
            <table width="600" border="1" align="center" bordercolor="#123456"  cellpadding='0' cellspacing='0'>
                <tr class='fila'>
                    <td align='center'>                     
                    <table  width='100%'  class='tablaInferior'>
                    
                    <tr class="fila">
                        <td align="center" width="30%"></td>
                        <td align="center"><input type='checkbox' name='All' onclick='jscript: SeleccionarAll();' style="cursor:default" title='Haga click para seleccionar todas las columnas'></td>
                    </tr>
                    <!-- Columnas -->
                    <tr class="fila">
                        <td align="left">&nbsp;Numero de planillas</td>
                        <td align="center"><input type='checkbox' name='LOV' value='numpla' onclick='jscript: ActivarAll();'  style="cursor:default"></td>
                        
                        <td align="left">&nbsp;Fecha de Despacho</td>
                        <td align="center"><input type='checkbox' name='LOV' value='fecdesp' onclick='jscript: ActivarAll();'  style="cursor:default"></td>
                        
                        <td align="left">&nbsp;Origen</td>
                        <td align="center"><input type='checkbox' name='LOV' value='origen' onclick='jscript: ActivarAll();'  style="cursor:default"></td>
                    </tr>
						 						 
						 
                    <tr class="fila">
                        <td align="left">&nbsp;Destino</td>
                        <td align="center"><input type='checkbox' name='LOV' value='destino' onclick='jscript: ActivarAll();'  style="cursor:default"></td>
                        
                        <td align="left">&nbsp;Agencia despacho planilla</td>
                        <td align="center"><input type='checkbox' name='LOV' value='agcdesppla' onclick='jscript: ActivarAll();'  style="cursor:default"></td>
                        
                        <td align="left">&nbsp;Fecha cumplido planilla</td>
                        <td align="center"><input type='checkbox' name='LOV' value='feccumpla' onclick='jscript: ActivarAll();'  style="cursor:default"></td>
                    </tr>
			
						 
                    <tr class="fila">
                    <td align="left">&nbsp;Fecha entrega planilla</td>
                    <td align="center"><input type='checkbox' name='LOV' value='fecentpla' onclick='jscript: ActivarAll();'  style="cursor:default"></td>

                    <td align="left">&nbsp;Dif Cump - Ent Pla</td>
                    <td align="center"><input type='checkbox' name='LOV' value='diffec' onclick='jscript: ActivarAll();'  style="cursor:default"></td>

                    <td align="left">&nbsp;Cantidad origen planilla</td>
                    <td align="center"><input type='checkbox' name='LOV' value='cantpla' onclick='jscript: ActivarAll();'  style="cursor:default"></td>
                </tr>
 
                        <tr class="fila">
                            <td align="left">&nbsp;Cantidad cumplida planilla</td>
                            <td align="center"><input type='checkbox' name='LOV' value='cantcumppla' onclick='jscript: ActivarAll();'  style="cursor:default"></td>
                            
                            <td align="left">&nbsp;Dif cantidad planilla</td>
                            <td align="center"><input type='checkbox' name='LOV' value='difcantpla' onclick='jscript: ActivarAll();'  style="cursor:default"></td>
                        
                            <td align="left">&nbsp;Agencia cumplido planilla</td>
                            <td align="center"><input type='checkbox' name='LOV' value='agccumppla' onclick='jscript: ActivarAll();'  style="cursor:default"></td>
                        </tr>
			 
                        <tr class="fila">
                            <td align="left">&nbsp;Usuario cumplido planilla</td>
                            <td align="center"><input type='checkbox' name='LOV' value='usucumppla' onclick='jscript: ActivarAll();'  style="cursor:default"></td>
                        
							<td align="left">&nbsp;Placa del Vehiculo</td>
                            <td align="center"><input type='checkbox' name='LOV' value='placa' onclick='jscript: ActivarAll();'  style="cursor:default"></td>
							
                            <td align="left">&nbsp;Numero remesa</td>
                            <td align="center"><input type='checkbox' name='LOV' value='numrem' onclick='jscript: ActivarAll();'  style="cursor:default"></td>
                        </tr>

			 
                        <tr class="fila">
                            <td align="left">&nbsp;Fecha remesa</td>
                            <td align="center"><input type='checkbox' name='LOV' value='fecrem' onclick='jscript: ActivarAll();'  style="cursor:default"></td>
                        
                            <td align="left">&nbsp;Agencia remesa</td>
                            <td align="center"><input type='checkbox' name='LOV' value='agcrem' onclick='jscript: ActivarAll();'  style="cursor:default"></td>
                        
                            <td align="left">&nbsp;Origen remesa</td>
                            <td align="center"><input type='checkbox' name='LOV' value='orirem' onclick='jscript: ActivarAll();'  style="cursor:default"></td>
                        </tr>
						                         						                       
						 
                        <tr class="fila">
                            <td align="left">&nbsp;Agencia despacho remesa</td>
                            <td align="center"><input type='checkbox' name='LOV' value='agcdesprem' onclick='jscript: ActivarAll();'  style="cursor:default"></td>
                            
                            <td align="left">&nbsp;Cliente</td>
                            <td align="center"><input type='checkbox' name='LOV' value='cliente' onclick='jscript: ActivarAll();'  style="cursor:default"></td>
                        
                            <td align="left">&nbsp;Fecha cumplido remesa</td>
                            <td align="center"><input type='checkbox' name='LOV' value='feccumprem' onclick='jscript: ActivarAll();'  style="cursor:default"></td>
                        </tr>
			 
                        <tr class="fila">
                        <td align="left">&nbsp;Cantidad origen remesa</td>
                        <td align="center"><input type='checkbox' name='LOV' value='cantorirem' onclick='jscript: ActivarAll();'  style="cursor:default"></td>
                        
                        <td align="left">&nbsp;Cantidad cumplida remesa</td>
                        <td align="center"><input type='checkbox' name='LOV' value='cantcumprem' onclick='jscript: ActivarAll();'  style="cursor:default"></td>
                            
                        <td align="left">&nbsp;Dif cantidad remesa</td>
                        <td align="center"><input type='checkbox' name='LOV' value='difcantrem' onclick='jscript: ActivarAll();'  style="cursor:default"></td>
                        </tr>
						 
						 
                        <tr class="fila">
                            <td align="left">&nbsp;Agencia cumplido remesa</td>
                            <td align="center"><input type='checkbox' name='LOV' value='agccumprem' onclick='jscript: ActivarAll();'  style="cursor:default"></td>
                            
                            <td align="left">&nbsp;Usuario cumplido remesa</td>
                            <td align="center"><input type='checkbox' name='LOV' value='usucumprem' onclick='jscript: ActivarAll();'  style="cursor:default"></td>
                            
                            <td align="left">&nbsp;Documentos internos</td>
                            <td align="center"><input type='checkbox' name='LOV' value='docint' onclick='jscript: ActivarAll();'  style="cursor:default"></td>
                        </tr>
						 
                        <tr class="fila">
                            <td align="left">&nbsp;Docu. internos x Cumplir</td>
                            <td align="center"><input type='checkbox' name='LOV' value='docintxcump' onclick='jscript: ActivarAll();'  style="cursor:default"></td>
							
							<td align="left">&nbsp;Tiene Discrepancia?</td>
                            <td align="center"><input type='checkbox' name='LOV' value='tienedisc' onclick='jscript: ActivarAll();'  style="cursor:default"></td>
                        
                            <td align="left">&nbsp;Numero discrepancia</td>
                            <td align="center"><input type='checkbox' name='LOV' value='numdisp' onclick='jscript: ActivarAll();'  style="cursor:default"></td>
                        </tr>
						                    
						 <tr class="fila">
                            <td align="left">&nbsp;Agencia de Facturaci&oacute;n</td>
                            <td align="center"><input type='checkbox' name='LOV' value='agefac' onclick='jscript: ActivarAll();'  style="cursor:default"></td>							
                        </tr>
						
                    </table>
                    <img src='<%=BASEURL%>/images/botones/aceptar.gif'    style='cursor:hand'  title='Guardar Columnas'  name='i_aceptar'     onclick="javascript:if(validarCumplido(forma)){rev.style.visibility='hidden'}"   onMouseOver='botonOver(this);' onMouseOut='botonOut(this);'> 
                    <img src='<%=BASEURL%>/images/botones/cancelar.gif'   style='cursor:hand'    title='Cancelar...'       name='i_cancel'      onclick="rev.style.visibility='hidden'"  onMouseOver='botonOver(this);' onMouseOut='botonOut(this);'>
                    <br>
                    <br>                              
                </td>
                </tr>
            </table>  
        </div>
        </form>
        <tsp:InitCalendar/>
        
    </body>
</html>
