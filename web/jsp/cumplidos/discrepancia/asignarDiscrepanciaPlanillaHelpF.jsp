<!--  
	 - Author(s)       :      LREALES
	 - Description     :      AYUDA FUNCIONAL - Buscar Descuentos de Equipos
	 - Date            :      23/05/2006 
	 - Copyright Notice:      TRANSPORTES SANCHEZ POLO S.A.
-->
<%@ include file="/WEB-INF/InitModel.jsp"%>
<HTML>
<HEAD>
<TITLE>AYUDA FUNCIONAL - Asignar Planilla a Discrepancia</TITLE>
<META http-equiv=Content-Type content="text/html; charset=windows-1252">
<link href="../css/estilostsp.css" rel="stylesheet" type="text/css"> 
<link href="<%=BASEURL%>/css/estilostsp.css" rel="stylesheet" type="text/css">
<script type='text/javascript' src="<%= BASEURL %>/js/boton.js"></script> 
</HEAD>
<BODY> 
<% String BASEIMG = BASEURL +"/images/ayuda/cumplido/discrepancia/"; %>
  <table width="95%"  border="2" align="center">
    <tr>
      <td height="117" >
        <table width="100%" border="0" align="center">
          <tr  class="subtitulo">
            <td height="20"><div align="center">MANUAL DE ASIGNACION DE PLANILLA </div></td>
          </tr>
          <tr class="subtitulo1">
            <td>Descripci&oacute;n del funcionamiento del programa para Asignar Planilla a las Discrepancias.</td>
          </tr>
          <tr>
            <td  class="ayudaHtmlTexto"><div align="center">
              <p>&nbsp;</p>
              </div></td>
          </tr>
<tr>
            <td  class="ayudaHtmlTexto"><p class="ayudaHtmlTexto">En la siguiente pantalla se digita el numero de la planilla a asignarle a la discrepancia.</p>
            </td>
          </tr>
<tr>
            <td  class="ayudaHtmlTexto"><div align="center"><IMG src="<%=BASEIMG%>ImagenAsignarDiscrepancia.JPG" border=0 v:shapes="_x0000_i1143"></div></td>
          </tr>
<tr>
  <td  class="ayudaHtmlTexto"><p>&nbsp;</p>
    <p>Si la planilla digitada no existe en nuestra base de datos, entonces el sistema le mostrar&aacute; el siguiente mensaje. </p></td>
</tr>
<tr>
  <td  class="ayudaHtmlTexto"><div align="center"><IMG src="<%=BASEIMG%>MensajePlanillaInexistente.JPG" border=0 v:shapes="_x0000_i1054"></div></td>
</tr>
      </table></td>
    </tr>
  </table>
  <p></p>
<center>
<img src = "<%=BASEURL%>/images/botones/salir.gif" style = "cursor:hand" name = "imgsalir" onClick = "parent.close();" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);">
</center>
</BODY>
</HTML>
