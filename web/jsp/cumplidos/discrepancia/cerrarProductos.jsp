<!--
- Autor : Ing. Jose de la rosa
- Date  : 9 de Nobiembre de 2005
- Copyrigth Notice : Fintravalores S.A. S.A
-->
<%--
-@(#)
--Descripcion : Pagina JSP, que cierra los productos de una discrepancia
--%>

<%-- Declaracion de librerias--%>
<%@page contentType="text/html"%>
<%@page session="true"%>
<%@page import="java.util.*"%>
<%@page import="com.tsp.operation.model.*"%>
<%@page import="com.tsp.operation.model.beans.*"%>
<%@page import="com.tsp.util.*"%>
<%@include file="/WEB-INF/InitModel.jsp"%>
<%@ taglib uri="http://jakarta.apache.org/taglibs/input-0.90" prefix="input" %>
<html>
<head>
<title>Cierre de Productos</title>
<link href="<%= BASEURL %>/css/estilostsp.css" rel="stylesheet" type="text/css">
<script type='text/javascript' src="<%= BASEURL %>/js/boton.js"></script>
<script type='text/javascript' src="<%= BASEURL %>/js/validar.js"></script>
</head>
<%-- Inicio Body --%>
<body <%if(request.getParameter("reload")!=null){%>onLoad="parent.opener.location.reload();"<%}%>>
<%	String mensaje = (String) request.getAttribute("msg");
	String num_discre = request.getParameter("c_num_discre");
	String cod_discrepancia = request.getParameter("c_cod_discrepancia");
	String numpla = request.getParameter("c_numpla").toUpperCase();
	String numrem = request.getParameter("c_numrem").toUpperCase();
	String tipo_doc = request.getParameter("c_tipo_doc").toUpperCase();
	String documento = request.getParameter("c_documento").toUpperCase();
	String tipo_doc_rel = request.getParameter("c_tipo_doc_rel").toUpperCase();
	String documento_rel = request.getParameter("c_documento_rel").toUpperCase();	
	String fecha_creacion = request.getParameter("c_fecha_creacion").toUpperCase();
	String cod_producto = request.getParameter("c_cod_producto").toUpperCase();
%>
<%-- Inicio Formulario --%>
<div id="capaSuperior" style="position:absolute; width:100%; height:100px; z-index:0; left: 0px; top: 0px;">
<jsp:include page="/WEB-INF/toptsp.jsp?encabezado=Cerrar Producto"/>
</div>

<div id="capaCentral" style="position:absolute; width:100%; height:85%; z-index:0; left: 0px; top: 100px; overflow: scroll;"> 
<FORM name='forma' method='POST'  action="<%=CONTROLLER%>?estado=Discrepancia&accion=Cierre">
	<table width="500" border="2" align="center">          
		<tr>
			<td>
				<table width="100%" class="tablaInferior">
						<tr class="fila">
						<td colspan="2" align="left" class="subtitulo1">&nbsp;Cierre de Productos</td>
						<td colspan="2" align="left" class="barratitulo"><img src="<%=BASEURL%>/images/titulo.gif" width="32" height="20"></td>
					</tr>
					<tr class="fila">
						<td width="74" align="left" >Referencia</td>
						<td width="158" valign="middle"><input name="c_referencia" type="text" class="textbox" id="c_referencia" size="10" maxlength="10"></td>
						<td width="101" valign="middle">Nota Contable </td>
						<td width="135" valign="middle"><input name="c_nota_contable" type="text" class="textbox" id="c_nota_contable" size="10" maxlength="10"></td>
					</tr>
					<tr class="fila">
					    <td colspan="2" align="right">Valor Aprobado&nbsp;&nbsp; </td>
					    <td colspan="2" align="left" > &nbsp;&nbsp;<input name="c_cantidad" type="text" class="textbox" id="c_cantidad" size="10" maxlength="10" onKeyPress="soloDigitos(event,'decOK')"></td>
				    </tr>
				</table>
			</td>
		</tr>
	</table>
	<input name="c_num_discre" type="hidden" value="<%=num_discre%>">
	<input name="c_numpla" type="hidden" value="<%=numpla%>">
	<input name="c_numrem" type="hidden" value="<%=numrem%>">
	<input name="c_tipo_doc" type="hidden" value="<%=tipo_doc%>">
	<input name="c_documento" type="hidden" value="<%=documento%>">
	<input name="c_tipo_doc_rel" type="hidden" value="<%=tipo_doc_rel%>">
	<input name="c_documento_rel" type="hidden" value="<%=documento_rel%>">	
	<input name="c_cod_discrepancia" type="hidden" value="<%=cod_discrepancia%>">
	<input name="c_fecha_creacion" type="hidden" value="<%=fecha_creacion%>">
	<input name="c_cod_producto" type="hidden" value="<%=cod_producto%>">
	<p>
	<div align="center"><input type="image" src="<%=BASEURL%>/images/botones/aceptar.gif" title="Cerrar un producto" style="cursor:hand" name="Aceptar_" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);">&nbsp;
	<img src="<%=BASEURL%>/images/botones/cancelar.gif" style="cursor:hand" name="regresar" title="Limpiar todos los registros" onMouseOver="botonOver(this);" onClick="forma.reset();" onMouseOut="botonOut(this);" >&nbsp;
	<img src="<%=BASEURL%>/images/botones/salir.gif" style="cursor:hand" name="salir" title="Salir al Menu Principal" onClick="parent.close();" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);"></div>
	</p>  
	<% if(!mensaje.equals("")){%>
		<br>
		<%--Mensaje de informacion--%>
		<table border="2" align="center">
			<tr>
				<td>
					<table width="100%"  border="1" align="center"  bordercolor="#F7F5F4" bgcolor="#FFFFFF">
						<tr>
							<td width="229" align="center" class="mensajes"><%=mensaje%></td>
							<td width="29" background="<%=BASEURL%>/images/cuadronaranja.JPG">&nbsp;</td>
							<td width="58">&nbsp;</td>
						</tr>
					</table>
				</td>
			</tr>
		</table>
	<%}%>
</form>
</div>
</body>
</html>
