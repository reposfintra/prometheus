<%@page contentType="text/html"%>
<%@ page session="true"%>
<%@ page errorPage="../error/error.jsp"%>
<%@page import="com.tsp.operation.model.beans.*, java.util.*"%>
<%@ include file="/WEB-INF/InitModel.jsp"%>
<%@page import="com.tsp.operation.model.*"%>
<%@page import="com.tsp.util.*"%>
<%@ taglib uri="http://jakarta.apache.org/taglibs/input-0.90" prefix="input" %>
<html>
<head>
<title>Anulacion</title>
<link href="<%= BASEURL %>/css/estilotsp.css" rel='stylesheet'>
<link href="../../../css/estilotsp.css" rel="stylesheet" type="text/css">
<%
	String numpla = "";
	String numrem = "";
	String tipo_docu = "";
	String documen = "";
	String tipo_docu_rel = "";
	String documen_rel = "";	
	String nom_doc = (request.getParameter("nom_doc")!=null)?request.getParameter("nom_doc"):"";
	String client = (request.getParameter("client")!=null)?request.getParameter("client"):"";
	Discrepancia d = model.discrepanciaService.getDiscrepancia();
	numpla = d.getNro_planilla();
	numrem = d.getNro_remesa();
	tipo_docu = d.getTipo_doc();
	documen = d.getDocumento();
	tipo_docu_rel = d.getTipo_doc_rel();
	documen_rel = d.getDocumento_rel();	
%>
<script type='text/javascript' src="<%= BASEURL %>/js/validar.js"></script>
<body <%if(request.getParameter("reload")!=null){%>onLoad="parent.opener.location.replace('<%=CONTROLLER%>?estado=Discrepancia&accion=Serch&c_numrem=<%=numrem%>&sw=true&c_numpla=<%=numpla%>&c_tipo_doc=<%=tipo_docu%>&c_documento=<%=documen%>&c_tipo_doc_rel=<%=tipo_docu_rel%>&c_documento_rel=<%=documen_rel%>&paso=false&nom_doc=<%=nom_doc%>&client=<%=client%>&modi=ok');"<%}%>>

</head>
<table border="2" align="center">
    <tr>
      <td><table width="410" height="73" border="1" align="center"  bordercolor="#F7F5F4" bgcolor="#FFFFFF">
          <tr>
            <td width="282" align="center" class="mensajes"> DATOS ANULADOS EXITOSAMENTE !</td>
            <td width="28" background="<%=BASEURL%>/images/cuadronaranja.JPG">&nbsp;</td>
            <td width="78">&nbsp;</td>
          </tr>
      </table></td>
    </tr>
</table>
<p align="center">
  <img src="<%=BASEURL%>/images/botones/salir.gif"  name="imgsalir"  onMouseOver="botonOver(this);" onClick="parent.close();" onMouseOut="botonOut(this);" style="cursor:hand">
</p>
</body>
</html>
