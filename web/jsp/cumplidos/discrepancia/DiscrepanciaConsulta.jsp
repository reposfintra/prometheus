<!--
- Autor : Ing. Jose de la rosa
- Date  : 9 de Nobiembre de 2005
- Copyrigth Notice : Fintravalores S.A. S.A
-->
<%--
-@(#)
--Descripcion : Pagina JSP, que lista las discrepancias para cerrar un producto
--%>

<%-- Declaracion de librerias--%>
<%@page contentType="text/html"%>
<%@page session="true"%>
<%@page import="java.util.*"%>
<%@page import="com.tsp.operation.model.*"%>
<%@page import="com.tsp.util.*"%>
<%@page import="com.tsp.operation.model.beans.*, java.util.*"%>
<%@include file="/WEB-INF/InitModel.jsp"%>
<%@taglib uri="http://jsptags.com/tags/navigation/pager" prefix="pg" %>
<html>
<head>
<title>Discrepancia Consulta</title>
<link href="<%= BASEURL %>/css/estilostsp.css" rel="stylesheet" type="text/css">
<script type='text/javascript' src="<%= BASEURL %>/js/boton.js"></script>
</head>
<script src='<%=BASEURL%>/js/date-picker.js'></script>
<script type='text/javascript' src="<%= BASEURL %>/js/validar.js"></script>
<script type='text/javascript' src="<%= BASEURL %>/js/reporte.js"></script>
<%-- Inicio Body --%>
<body>
<%	String mensaje = (String) request.getAttribute("msg");
	String res = "";
    String numpla = request.getParameter("c_numpla");
	String sw = request.getParameter("sw");
    InfoPlanilla info = model.planillaService.getInfoPlanilla();
    info.setPropietario(model.planillaService.getNomPropie());
    String conductor = info.getCedulaConductor() + " " + info.getNombreConductor();
    String ruta = info.getOrigen() + " - " + info.getDestino();
    String fecha = info.getFecha();
    String placa = info.getVehiculo();
	String propietario = info.getPropietario();
	String exis = (sw.equals("true"))?request.getParameter("exis"):"";
	String tipo_docu = request.getParameter("c_tipo_doc");
	String documen = request.getParameter("c_documento");
	String tipo_docu_rel = request.getParameter("c_tipo_doc_rel");
	String documen_rel = request.getParameter("c_documento_rel");		
	String planilla = "";
	String remesa = request.getParameter("c_numrem");
	String fecha_dev = "";
	String numero_rech = "";
	String contac = "";
	String report = "";
	String ubica = "";
	String retencion = "";
	String num_discre = request.getParameter("c_num_discre");
	int nu = Integer.parseInt(num_discre);
	String paso = "";
	String cod = "";
	String desc = "";
	String uni = "";
	String nom_doc = (request.getParameter("nom_doc")!=null)?request.getParameter("nom_doc"):"";
	int num_d = 0;
%>
<div id="capaSuperior" style="position:absolute; width:100%; height:100px; z-index:0; left: 0px; top: 0px;">
<jsp:include page="/WEB-INF/toptsp.jsp?encabezado=Consulta Discrepancia"/>
</div>

<div id="capaCentral" style="position:absolute; width:100%; height:85%; z-index:0; left: 0px; top: 100px; overflow: scroll;"> 
<%-- Tabla del detalle de la planilla --%>
<table width="1000" border="2" align="center">          
	<tr>
		<td>
			<table width="100%" class="tablaInferior">
				<tr>
				<td width="50%" class="subtitulo1">&nbsp;Detalle de la Planilla</td>
				<td width="50%" class="barratitulo"><img src="<%=BASEURL%>/images/titulo.gif" width="32" height="20"></td>
				</tr>
			</table>
			<table width="100%" class="tablaInferior">
				<tr class="fila">
					<td width="14%" >No.:</td>
					<td width="36%" ><%=numpla %></td>
					<td width="14%" >Fecha:</td>
					<td width="36%" ><%=fecha %></td>
				</tr>
				<tr class="fila">
					<td >Conductor:</td>
					<td ><%=conductor %></td>
					<td >Placa:</td>
					<td ><%=placa %></td>
				</tr>
				<tr class="fila">
					<td >Ruta:</td>
					<td ><%=ruta %></td>
					<td >Propietario:</td>
					<td ><%= propietario %></td>
				</tr>
			</table>
		</td>
	</tr>
</table>
<%
Vector vec = model.planillaService.ReporteVector();
Planilla p;
if ( vec.size() >0 ){ %>
<%-- Tabla de la remesa --%>
<table width="1000" border="2" align="center">
	<tr>
		<td>
			<table width="100%" align="center">
				<tr>
					<td width="50%" class="subtitulo1">&nbsp;Remesa</td>
					<td width="50%" class="barratitulo"><img src="<%=BASEURL%>/images/titulo.gif" width="32" height="20"></td>
				</tr>
			</table>
			<table width="100%" border="1" bordercolor="#999999" bgcolor="#F7F5F4" align="center">
				<tr class="tblTitulo" align="center">
					<td width="16%" align="center">Remesa</td>
					<td width="27%" align="center">Cliente</td>
					<td width="19%" align="center">Fecha</td>
					<td width="19%" align="center">Origen Remesa </td>
					<td width="19%" align="center">Destino Remesa </td>
				</tr><%
				for(int i = 0; i<vec.size(); i++){	
					p = (Planilla) vec.elementAt(i);%>
					<tr class="<%=(i % 2 == 0 )?"filagris":"filaazul"%>">
						<td width="16%" class="bordereporte" align="center"><%=p.getNumrem()%></td>
						<td width="27%" class="bordereporte" align="center"><%=p.getNitpro()%></td>
						<td width="19%" class="bordereporte" align="center"><%=p.getFecdsp()%></td>
						<td width="19%" class="bordereporte" align="center"><%=p.getNomori()%></td>
						<td width="19%" class="bordereporte" align="center"><%=p.getNomdest()%></td>
					</tr>
				<%}%>
				<tr class="pie" align="center">
					<td td height="20" colspan="12" nowrap align="center">
					</td>
				</tr>
			</table>
		</td>
	</tr>
</table>
<%}
String numrem = request.getParameter("c_numrem");
String campos = request.getParameter("campos");
Vector Vecrem = model.remesaService.getRemes();
Remesa r;
if ( Vecrem.size() >0 ){ %>
	<%-- Tabla de los documentos relacionados con la remesa --%>
	<table width="1000" border="2" align="center">
		<tr>
			<td>
				<table width="100%" align="center">
					<tr>
						<td width="50%" class="subtitulo1">&nbsp;Documentos Relacionados a la Remesa </td>
						<td width="50%" class="barratitulo"><img src="<%=BASEURL%>/images/titulo.gif" width="32" height="20"></td>
					</tr>
				</table>
				<table width="100%" border="1" bordercolor="#999999" bgcolor="#F7F5F4" align="center">
					<tr class="tblTitulo" align="center">
						<td width="20%" align="center">Tipo Documento </td>
						<td width="17%"  align="center">Documento</td>
						<td width="20%" align="center">Tipo Doc Relacionado </td>
						<td width="17%" align="center">Documento Relacionado</td>
						<td width="26%" align="center">Nombre Destinatario </td>
					</tr><%
					for(int i = 0; i<Vecrem.size(); i++){	
						r = (Remesa) Vecrem.elementAt(i);%>
						<tr class="<%=(i % 2 == 0 )?"filagris":"filaazul"%>" >
							<td width="20%" class="bordereporte" align="center"><%=r.getNombre_tipo_doc()%></td>
							<td width="17%" class="bordereporte" align="center"><%=r.getDocumento()%></td>
							<td width="20%" class="bordereporte" align="center"><%=r.getNombre_tipo_doc_rel()%></td>
							<td width="17%" class="bordereporte" align="center"><%=r.getDocumento_rel()%></td>
							<td width="26%" class="bordereporte" align="center"><%=r.getNom_destinatario()%></td>
					</tr>
					<%}%>
					<tr class="pie" align="center">
						<td td height="20" colspan="12" nowrap align="center">
						</td>
					</tr>
				</table>
			</td>
		</tr>
	</table>
<%}
Discrepancia dis = (Discrepancia) model.discrepanciaService.getDiscrepancia();
planilla = dis.getNro_planilla();
remesa = dis.getNro_remesa();
tipo_docu = dis.getTipo_doc();
documen = dis.getDocumento();
fecha_dev = dis.getFecha_devolucion().substring(0,10);
numero_rech = String.valueOf(dis.getNumero_rechazo());
contac = dis.getContacto();
report = dis.getReportado();
tipo_docu_rel = dis.getTipo_doc_rel();
documen_rel = dis.getDocumento_rel();	
retencion = dis.getRetencion();
ubica = dis.getUbicacion();
num_discre = String.valueOf(dis.getNro_discrepancia());
num_d = dis.getNro_discrepancia();%>
<%-- Tabla de las discrepancias --%>
<table width="1000" border="2" align="center">
	<tr>
		<td>
			<table width="100%" align="center">
				<tr>
					<td width="50%" class="subtitulo1">&nbsp;Discrepancia</td>
					<td width="50%" class="barratitulo"><img src="<%=BASEURL%>/images/titulo.gif" width="32" height="20"></td>
				</tr>
			</table>
			<table width="100%" border="1" bordercolor="#999999" bgcolor="#F7F5F4" align="center">
				<tr class="tblTitulo" align="center">
					<td width="13%" align="center">Fecha Devoluci&oacute;n</td>
					<td width="14%"  align="center">Retenci&oacute;n Pago </td>
					<td width="18%" align="center">Numero Rechazo Cliente </td>
					<td width="19%" align="center">Contacto del Cliente</td>
					<td width="17%" align="center">Reportado Por </td>
					<td width="19%" align="center">Ubicaci&oacute;n Referencia</td>
				</tr>
					<tr class="filagris" >
						<td width="13%" class="bordereporte" align="center"><%=fecha_dev%></td>
						<td width="14%" class="bordereporte" align="center"><% if(retencion.equals("S")){%>Si<%}else{%>No<%}%></td>
						<td width="18%" class="bordereporte" align="center"><%=numero_rech%></td>
						<td width="19%" class="bordereporte" align="center"><%=contac%></td>
						<td width="17%" class="bordereporte" align="center"><%=report%></td>
						<td width="19%" class="bordereporte" align="center"><% Ubicacion ubi = model.ubService.getUb ();%><%=(ubi!=null)?ubi.getDescripcion ():""%></td></td>
					</tr>
				<tr class="pie" align="center">
					<td td height="20" colspan="14" nowrap align="center">
				</td>
				</tr>		
				<tr lass="pie" align="center">
					<td td height="20" colspan="14" nowrap align="center">
						<% 
						Vector Vecdis = model.discrepanciaService.getDiscrepancias();
						Discrepancia d; 
						%>
						<%-- Tabla de los productos de las discrepancias --%>
						<table width="100%" border="2" align="center">
							<tr>
								<td>
									<table width="100%" align="center">
										<tr>
											<td width="50%" class="subtitulo1">&nbsp;Productos de Discrepancia</td>
											<td width="50%" class="barratitulo"><img src="<%=BASEURL%>/images/titulo.gif" width="32" height="20"></td>
										</tr>
									</table>
									<table width="100%" border="1" bordercolor="#999999" bgcolor="#F7F5F4" align="center">
										<tr class="tblTitulo" align="center">
											<td width="9%" align="center">Codigo Producto </td>
											<td width="33%" align="center">Descripci&oacute;n</td>
											<td width="10%" align="center">Valor</td>
											<td width="10%" align="center">Cantidad</td>
											<td width="10%" align="center">Unidad</td>
											<td width="13%" align="center">Tipo Discrepancia </td>
											<td width="14%" align="center">Causa Devoluci&oacute;n </td>
											<td width="11%" align="center">Responsable</td>
										</tr><%	
										if ( Vecdis.size() >0 ){
											for(int i = 0; i<Vecdis.size(); i++){	
												d = (Discrepancia) Vecdis.elementAt(i);%>
												<tr class="<%=(i % 2 == 0 )?"filagris":"filaazul"%>" >
													<td width="9%" align="center" class="bordereporte"><%=d.getCod_producto()%></td>
													<td width="33%" align="center" class="bordereporte"><%=d.getDescripcion()%></td>
													<td width="10%" align="center" class="bordereporte"><%=d.getValor()%></td>
													<td width="10%" align="center" class="bordereporte"><%=d.getCantidad()%></td>
													<td width="10%" align="center" class="bordereporte"><%model.unidadService.buscarUnidad(d.getUnidad());
                                                                                                                                                                Unidad unid = model.unidadService.getUnidad(); %>
                                                                                                                                                                <%=unid!=null?unid.getDescripcion():""%></td>
													<td width="13%" align="center" class="bordereporte"><%model.codigo_discrepanciaService.serchCodigo_discrepancia(d.getCod_discrepancia());
														Codigo_discrepancia cd = model.codigo_discrepanciaService.getCodigo_discrepancia(); %>
														<%=cd.getDescripcion()%></td>
													<td width="14%" align="center" class="bordereporte"><%Vector Vecc2 = model.causadiscreService.listarCausas();
													for(int j = 0; j<Vecc2.size(); j++){	
														CausaDiscrepancia cd2 = (CausaDiscrepancia) Vecc2.elementAt(j);	
														if(d.getCausa_dev().equals(cd2.getCodigo())){%>
															<%=cd2.getDescripcion()%><%
														}
													}%></td>
													<td width="11%" align="center" class="bordereporte">	<% if (d.getResponsable().equals("CL")){%>CLIENTE<%}else{%>CONDUCTOR<%}%></td>
												</tr>
											<%}
										}%>
										<tr class="pie" align="center">
											<td td height="20" colspan="16" nowrap align="center">
											</td>
										</tr>
									</table>
								</td>
							</tr>						
						</table>
					</td>
				</tr>		
			</table>
		</td>
	</tr>
</table>	
<br>					  
<%if(mensaje!=null){%>
	<%--Mensaje de informacion--%>
	<table border="2" align="center">
		<tr>
			<td>
				<table width="100%"  border="1" align="center"  bordercolor="#F7F5F4" bgcolor="#FFFFFF">
					<tr>
						<td width="229" align="center" class="mensajes"><%=mensaje%></td>
						<td width="29" background="<%=BASEURL%>/images/cuadronaranja.JPG">&nbsp;</td>
						<td width="58">&nbsp;</td>
					</tr>
				</table>
			</td>
		</tr>
	</table>
<%}%>
<%--tabla para el boton de salir--%>
<table width="1000" border="0" align="center">
	<tr>
		<td>
			<img src="<%=BASEURL%>/images/botones/salir.gif" style="cursor:hand" name="salir" title="Salir al Menu Principal" onClick="parent.close();" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);">
		</td>
	</tr>
</table>
</DIV>
</body>
</html>
