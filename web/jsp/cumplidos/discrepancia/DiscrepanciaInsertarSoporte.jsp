<!--
- Autor : Ing. Jose de la rosa
- Date  : 18 de Noviembre de 2005
- Copyrigth Notice : Fintravalores S.A. S.A
-->
<%--
-@(#)
--Descripcion : Pagina JSP, que maneja el ingreso de los controles de soporte
--%>

<%-- Declaracion de librerias--%>
<%@page contentType="text/html"%>
<%@page session="true"%>
<%@page import="java.util.*"%>
<%@page import="com.tsp.operation.model.*"%>
<%@page import="com.tsp.util.*"%>
<%@page import="com.tsp.operation.model.beans.*, java.util.*"%>
<%@include file="/WEB-INF/InitModel.jsp"%>
<%@taglib uri="http://jsptags.com/tags/navigation/pager" prefix="pg" %>
<html>
<head>
<title>Soporte Incompleto</title>
<link href="<%= BASEURL %>/css/estilostsp.css" rel="stylesheet" type="text/css">
<script type='text/javascript' src="<%= BASEURL %>/js/boton.js"></script>
</head>
<script src='<%=BASEURL%>/js/date-picker.js'></script>

<script type='text/javascript' src="<%= BASEURL %>/js/reporte.js"></script>
<%
	String path = application.getRealPath(model.menuService.getRealUrl(request.getRequestURI()));
	String datos[] = model.menuService.getContenidoMenu(BASEURL,request.getRequestURI(),path);
 	String style = "simple";
    String position =  "bottom";
    String index =  "center";
    int maxPageItems = 10;
    int maxIndexPages = 10;
    Discrepancia d;
    String numpla = request.getParameter("c_numpla");
    InfoPlanilla info = model.planillaService.getInfoPlanilla();
    info.setPropietario(model.planillaService.getNomPropie());
    String conductor = info.getCedulaConductor() + " " + info.getNombreConductor();
    String ruta = info.getOrigen() + " - " + info.getDestino();
    String fecha = info.getFecha();
    String placa = info.getVehiculo();
    String propietario = info.getPropietario();
    Vector vec = model.discrepanciaService.getDiscrepancias();
    String sw = (request.getParameter("sw")!=null)?request.getParameter("sw"):"false";
    String numrem = (request.getParameter("numrem")!=null)?request.getParameter("numrem"):"";
%>
<%-- Inicio Body --%>
<body>
<div id="capaSuperior" style="position:absolute; width:100%; height:100px; z-index:0; left: 0px; top: 0px;">
 <jsp:include page="/WEB-INF/toptsp.jsp?encabezado=Soporte Incompleto"/>
</div>

 <div id="capaCentral" style="position:absolute; width:100%; height:85%; z-index:0; left: 0px; top: 100px; overflow: scroll;"> 
 
<%-- Tabla del detalle de la planilla --%>
<table width="900" border="2" align="center">          
	<tr>
		<td>
			<table width="100%" class="tablaInferior">
				<tr>
					<td width="50%" colspan="2" class="subtitulo1">&nbsp;Detalle de la Planilla</td>
					<td width="50%" colspan="2" class="barratitulo"><img src="<%=BASEURL%>/images/titulo.gif" width="32" height="20" align="left"><%=datos[0]%></td>
				</tr>
				<tr>
					<td width="14%" class="fila">No.:</td>
					<td width="36%" class="letra"><%=numpla %></td>
					<td width="14%" class="fila">Fecha:</td>
					<td width="36%" class="letra"><%=fecha %></td>
				</tr>
				<tr>
					<td class="fila">Conductor:</td>
					<td class="letra"><%=conductor %></td>
					<td class="fila">Placa:</td>
					<td class="letra"><%=placa %></td>
				</tr>
				<tr>
					<td class="fila">Ruta:</td>
					<td class="letra"><%=ruta %></td>
					<td class="fila">Propietario:</td>
					<td class="letra"><%= propietario %></td>
				</tr>
			</table>
		</td>
	</tr>
</table>
<% 
	Vector vecP = model.planillaService.ReporteVector();
	Planilla p;
	if ( vecP.size() >0 ){ %>
		<%-- Tabla de la remesa --%>
		<BR>
		<table width="700" border="2" align="center">
			<tr>
				<td>
					<table width="100%" align="center">
						<tr>
							<td width="50%" class="subtitulo1">&nbsp;Remesas</td>
							<td width="50%" class="barratitulo"><img src="<%=BASEURL%>/images/titulo.gif" width="32" height="20" align="left"></td>
						</tr>
					</table>
					<table width="100%" border="1" bordercolor="#999999" bgcolor="#F7F5F4" align="center">
						<tr class="tblTitulo" align="center">
							<td width="40%" align="center">Remesa</td>
							<td width="60%" align="center">Cliente</td>
						</tr>
						<% for(int K = 0; K<vecP.size(); K++){	
							p = (Planilla) vecP.elementAt(K);%>
							<tr class="<%=(K % 2 == 0 )?"filagris":"filaazul"%>" onMouseOver='cambiarColorMouse(this)'   style="cursor:hand" onClick="window.location='<%=CONTROLLER%>?estado=Discrepancia&accion=InsertSoporte&c_numrem=<%=p.getNumrem()%>&sw=false&opc=true&c_numpla=<%=numpla%>'">
								<td class="bordereporte" align="center"><%=p.getNumrem()%></td>
								<td class="bordereporte" align="center"><%=p.getNitpro()%></td>
							</tr>
						<%}%>
						<tr  class="pie">
							<td td height="20" colspan="12" nowrap align="center">
							</td>
						</tr>
					</table>
				</td>
			</tr>
		</table>
	<%}
%>
<FORM  method='POST' action="<%=CONTROLLER%>?estado=DiscrepanciaInsert&accion=Soporte&opc=false" name='FormularioListado' id="FormularioListado">	
	<%if(sw.equals("true")){
	Vector veccump = model.cumplidoService.getCumplidos ();%>
	<%-- Tabla de las discrepancias --%>
	<table width="700" border="2" align="center">          
		<tr>
			<td>
			<table width="100%" align="center">
				<tr>
					<td width="50%" class="subtitulo1">&nbsp;Soporte Incompleto</td>
					<td width="50%" class="barratitulo"><img src="<%=BASEURL%>/images/titulo.gif" width="32" height="20"></td>
				</tr>
			</table>
				<table width="100%" border="1" bordercolor="#999999" bgcolor="#F7F5F4" align="center">
					<%-- Formulario de ingreso y modificacion de discrepancia --%>
						
					<tr class="tblTitulo" align="center">
						<td width="26%" align="center">Entregado Por Conductor</td>
						<td width="60%" align="center">Nombre Documento Soporte</td>
					    <td width="14%" align="center">No Aplica </td>
					</tr>
					<%
					if(veccump!=null){
						for(int j=0; j<veccump.size ();j++){
							Cumplido c = (Cumplido)veccump.elementAt (j);
							%>							
							<tr class="<%=(j % 2 == 0 )?"filagris":"filaazul"%>">
								<td align="center" class="bordereporte"><input type='checkbox' id="LOV<%=j%>" name='LOV' value='<%= c.getSoporte()+","+c.getComent()%>' onclick='if (LOV<%=j%>.checked) { LOVC<%=j%>.checked=!LOV<%=j%>.checked; }'></td>
								<td align="center" class="bordereporte"><%=c.getComent()%></td>
							    <td align="center" class="bordereporte"><input type='checkbox' id="LOVC<%=j%>" name='LOVC' value='<%= c.getSoporte()+","+c.getComent()%>' onclick='if (LOVC<%=j%>.checked) { LOV<%=j%>.checked=!LOVC<%=j%>.checked; }'></td>
							</tr>		
						<%}
					}%>			
				</table>
			</td>
		</tr>
	</table>
	<%}%>
	<input name="c_numpla" id="c_numpla" value="<%=numpla%>" type="hidden">
	<input name="c_numrem" id="c_numrem" value="<%=numrem%>" type="hidden">
	<p align="center">
		<img src="<%=BASEURL%>/images/botones/aceptar.gif" title="Agregar un control de soporte" style="cursor:hand" name="soporte" onclick="FormularioListado.submit();"  onMouseOver="botonOver(this);" onMouseOut="botonOut(this);">&nbsp;
		<img src="<%=BASEURL%>/images/botones/salir.gif" style="cursor:hand" name="salir" title="Salir al Menu Principal" onMouseOver="botonOver(this);" onClick="parent.close();" onMouseOut="botonOut(this);" >&nbsp;
		<img src="<%=BASEURL%>/images/botones/regresar.gif" style="cursor:hand" title="Volver" name="buscar"  onClick="window.location='<%=CONTROLLER%>?estado=Informacion&accion=Planilla_discrepancia&tipo=G&numpla=<%=numpla%>'" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);">
	</p>
	<table width="900" border="2" align="center">
      <tr>
        <td>
          <table width="100%" align="center">
            <tr>
              <td width="373" class="subtitulo1">&nbsp;Listar Discrepancias Generales </td>
              <td width="427" class="barratitulo"><img src="<%=BASEURL%>/images/titulo.gif" width="32" height="20"></td>
            </tr>
          </table>
          <table width="100%" border="1" bordercolor="#999999" bgcolor="#F7F5F4" align="center">
			<tr class="tblTitulo" align="center">
				<td width="11%" align="center">Numero Planilla</td>
				<td width="8%"  align="center">Retenci&oacute;n Pago </td>
				<td width="14%" align="center">Tipo Discrepancia </td>
				<td width="15%" align="center">Cantidad</td>
				<td width="13%" align="center">Valor</td>
				<td width="39%" align="center">Observaci&oacute;n</td>
			</tr>
			<pg:pager
					items="<%=vec.size()%>"
					index="<%= index %>"
					maxPageItems="<%= maxPageItems %>"
					maxIndexPages="<%= maxIndexPages %>"
					isOffset="<%= true %>"
					export="offset,currentPageNumber=pageNumber"
					scope="request"><%
			for (int i = offset.intValue(), l = Math.min(i + maxPageItems, vec.size()); i < l; i++){
				d = (Discrepancia) vec.elementAt(i);%>
				<tr class="<%=(i % 2 == 0 )?"filagris":"filaazul"%>" onMouseOver='cambiarColorMouse(this)'   style="cursor:hand"
					onClick="window.location='<%=CONTROLLER%>?estado=Informacion&accion=Planilla_discrepancia&tipo=M&c_num_discre=<%=d.getNro_discrepancia()%>&numpla=<%=d.getNro_planilla()%>'">
					<td width="11%" align="center" class="bordereporte"><%=d.getNro_planilla()%></td>
					<td width="8%" align="center" class="bordereporte"><% if(d.getRetencion().equals("S")){%>SI<%}else{%>NO<%}%></td>
					<td width="14%" align="center" class="bordereporte"><%if(!d.getCod_discrepancia().equals("")){model.codigo_discrepanciaService.serchCodigo_discrepancia(d.getCod_discrepancia());
													Codigo_discrepancia cd = model.codigo_discrepanciaService.getCodigo_discrepancia(); %><%=cd.getDescripcion()%><%}%></td>
					<td width="15%" align="center" class="bordereporte"><%=d.getCantidad()%> <%if(!d.getUnidad().equals("")){model.unidadService.buscarUnidad(d.getUnidad()); Unidad uni = model.unidadService.getUnidad();%><%=uni.getDescripcion()%><%}%></td>
					<td width="13%" align="center" class="bordereporte"><%=d.getValor()%> </td>
					<td width="39%" align="center" class="bordereporte"><%=d.getObservacion()%></td>
				</tr>
            <%}%>
			</pg:pager>
        </table></td>
      </tr>
    </table>
	<br>
</form>	
<%
if(request.getAttribute("msg")!=null){%>
	<%--Mensaje de informacion--%>
	<table border="2" align="center">
		<tr>
			<td>
				<table width="100%"  border="1" align="center"  bordercolor="#F7F5F4" bgcolor="#FFFFFF">
					<tr>
						<td width="229" align="center" class="mensajes"><%=request.getAttribute("msg")%></td>
						<td width="29" background="<%=BASEURL%>/images/cuadronaranja.JPG">&nbsp;</td>
						<td width="58">&nbsp;</td>
					</tr>
				</table>
			</td>
		</tr>
	</table>
<%}%>
</div>
<%=datos[1]%>
</body>
</html>
<script>
function cargarDiscrepancia ( BASEURL , numpla){
	if(forma3.c_cod_discrepancia.value=='SI'){
		window.open(BASEURL+"?estado=Informacion&accion=Planilla_discrepancia&numpla="+numpla+"&tipo=S",'Discrepancia','status=yes,scrollbars=no,width=720,height=400,resizable=yes');
	}	
}	
</script>
