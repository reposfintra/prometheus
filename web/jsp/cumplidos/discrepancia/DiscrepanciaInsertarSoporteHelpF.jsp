<!--  
	 - Author(s)       :      Jose de la Rosa
	 - Description     :      AYUDA FUNCIONAL - Buscar Acuerdos especiales
	 - Date            :      08/06/2006 
	 - Copyright Notice:      TRANSPORTES SANCHEZ POLO S.A.
-->
<%@ include file="/WEB-INF/InitModel.jsp"%>
<HTML>
<HEAD>
<TITLE>AYUDA FUNCIONAL - Cumplidos</TITLE>
<META http-equiv=Content-Type content="text/html; charset=windows-1252">
<link href="../css/estilostsp.css" rel="stylesheet" type="text/css"> 
<link href="<%=BASEURL%>/css/estilostsp.css" rel="stylesheet" type="text/css">
<script type='text/javascript' src="<%= BASEURL %>/js/boton.js"></script> 
</HEAD>
<BODY> 
<% String BASEIMG_ACU = BASEURL +"/images/ayuda/equipos/acuerdo_especial/"; %>
<% String BASEIMG = BASEURL +"/images/ayuda/cumplido/discrepancia/"; %>
  <table width="95%"  border="2" align="center">
    <tr>
      <td height="117" >
        <table width="100%" border="0" align="center">
          <tr  class="subtitulo">
            <td height="20"><div align="center">MANUAL DE DISCREPANCIA GENERAL </div></td>
          </tr>
          <tr class="subtitulo1">
            <td>Descripci&oacute;n del funcionamiento del programa para realizar Cumplidos </td>
          </tr>
          <tr>
            <td  class="ayudaHtmlTexto"><div align="center">
              <p>&nbsp;</p>
              </div></td>
          </tr>
            <tr>
            <td  class="ayudaHtmlTexto"><p class="ayudaHtmlTexto">En la siguiente pantalla  como se ilustra en la Imagen1  como se seleccionan los documentos que se entregaron completos e incompletos.</p>
          </td>
          </tr>
            <tr>
            <td  class="ayudaHtmlTexto"><div align="center">
              <p><IMG src="<%=BASEIMG%>DiscrepanciaSoporteIncompleto.JPG" border=0 ></p>
              <p><strong>Imagen1</strong></p>
            </div></td>
          </tr>
		  
		<tr>
		<td  class="ayudaHtmlTexto"><p class="ayudaHtmlTexto">&nbsp;</p>
		<p class="ayudaHtmlTexto">El sistema verifica que  existan soportes almacenados en la Base de Datos y no se selecciona ningun documento aparecer&aacute; como se ilustra en la Imagen2. </p>
		</td>
		</tr>
		<tr>
		<td  class="ayudaHtmlTexto"><div align="center">
		  <p><IMG src="<%=BASEIMG%>MensajeExisteAgregado.JPG" border=0 ></p>
		  <p><strong>Imagen2</strong></p>
		</div></td>
		</tr>		  
		<tr>
		<td  class="ayudaHtmlTexto"><p class="ayudaHtmlTexto">&nbsp;</p>
		<p class="ayudaHtmlTexto">El sistema verifica que no existan registos almacenados en la Base de Datos aparecer&aacute; como se ilustra en la Imagen3. </p>
		</td>
		</tr>
		<tr>
		<td  class="ayudaHtmlTexto"><div align="center">
		  <p><IMG src="<%=BASEIMG%>MensajeNoExisteAgregado.JPG" border=0 ></p>
		  <p><strong>Imagen3</strong></p>
		  </div></td>
		</tr>
				
      </table></td>
    </tr>
  </table>
  <p></p>
<center>
<img src = "<%=BASEURL%>/images/botones/salir.gif" style = "cursor:hand" name = "imgsalir" onClick = "parent.close();" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);">
</center>
</BODY>
</HTML>
