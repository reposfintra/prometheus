<!--
	 - Author(s)       :      Jose de la Rosa
	 - Description     :      AYUDA FUNCIONAL - Discrepancia
	 - Date            :      08/06/2006 
	 - Copyright Notice:      TRANSPORTES SANCHEZ POLO S.A.
-->
<%@ include file="/WEB-INF/InitModel.jsp"%>
<HTML>
<HEAD>
<TITLE>AYUDA FUNCIONAL - Discrepancia</TITLE>
<META http-equiv=Content-Type content="text/html; charset=windows-1252">
<link href="../css/estilostsp.css" rel="stylesheet" type="text/css"> 
<link href="<%=BASEURL%>/css/estilostsp.css" rel="stylesheet" type="text/css">
<script type='text/javascript' src="<%= BASEURL %>/js/boton.js"></script> 
</HEAD>
<BODY> 
<% String BASEIMG_ACU = BASEURL +"/images/ayuda/equipos/acuerdo_especial/"; %>
<% String BASEIMG = BASEURL +"/images/ayuda/cumplido/discrepancia/"; %>
  <table width="95%"  border="2" align="center">
    <tr>
      <td height="117" >
        <table width="100%" border="0" align="center">
          <tr  class="subtitulo">
            <td height="20"><div align="center">MANUAL DE DISCREPANCIA </div></td>
          </tr>
          <tr class="subtitulo1">
            <td>Descripci&oacute;n del funcionamiento del programa para realizar Discrepancia </td>
          </tr>
          <tr>
            <td  class="ayudaHtmlTexto"><div align="center">
              <p>&nbsp;</p>
              </div></td>
          </tr>
            <tr>
            <td  class="ayudaHtmlTexto"><p class="ayudaHtmlTexto">En la siguiente pantalla  como se ilustra en la Imagen1 se muestra la información de la planilla que se le va a registrar la discrepancia.</p>
          </td>
          </tr>
            <tr>
            <td  class="ayudaHtmlTexto"><div align="center">
              <p><IMG src="<%=BASEIMG%>EncabezadoPlanilla.JPG" border=0 ></p>
              <p><strong>Imagen1</strong></p>
            </div></td>
          </tr>
		  
		<tr>
		<td  class="ayudaHtmlTexto"><p class="ayudaHtmlTexto">&nbsp;</p>
		<p class="ayudaHtmlTexto">En la siguiente pantalla como se ilustra en la Imagen2 se muestra la informaci&oacute;n de las remesas asociadas a la planilla que se le va a registrar la discrepancia, en este modulo se debe presionar click sobre la fila de datos para seleccionar la remesa.</p>
		</td>
		</tr>
		<tr>
		<td  class="ayudaHtmlTexto"><div align="center">
		  <p><IMG src="<%=BASEIMG%>InformacionRemesa.JPG" border=0 ></p>
		  <p><strong>Imagen2</strong></p>
		</div></td>
		</tr>		  
		<tr>
		<td  class="ayudaHtmlTexto"><p class="ayudaHtmlTexto">&nbsp;</p>
		<p class="ayudaHtmlTexto">En la siguiente pantalla como se ilustra en la Imagen3 se muestra la informaci&oacute;n de los documentos asociados a la remesa que se le va a registrar la discrepancia, en este modulo se debe presionar click sobre la fila de datos para seleccionar el documento.</p>
		</td>
		</tr>
		<tr>
		<td  class="ayudaHtmlTexto"><div align="center">
		  <p><IMG src="<%=BASEIMG%>InformacionDocumentosdelaRemesa.JPG" border=0 ></p>
		  <p><strong>Imagen3</strong></p>
		</div></td>
		</tr>				
		<tr>
		<td  class="ayudaHtmlTexto"><p class="ayudaHtmlTexto">&nbsp;</p>
		<p class="ayudaHtmlTexto">En la siguiente pantalla como se ilustra en la Imagen4 se muestra los datos para ingresar la discrepancia.</p>
		<table width="100%"  border="0" cellpadding="0" cellspacing="0" class="ayudaHtmlTexto">
          <tr>
            <td width="50%">0 - Link para agregar imagen a la planilla.</td>
            <td width="50%">6 - Informaci&oacute;n de la remesa y  del documentos relacionado.</td>
          </tr>
          <tr>
            <td>1 - Información de la planilla y el tipo de documentos relacionado.</td>
            <td>7 - Campo para seleccionar si se la va a retener el pago al conductor. </td>
          </tr>
          <tr>
            <td>2 - Campo para escoger la fecha de devoluci&oacute;n. </td>
            <td>8 - Campo para digitar el contacto del cliente. </td>
          </tr>
          <tr>
            <td>3 - Campo para digitar el numero de devoluci&oacute;n </td>
            <td>9 - Campo para seleccionar la ubicaci&oacute;n de la mercancia. </td>
          </tr>
          <tr>
            <td>4 - Campo para digitar quien reporto la discrepancia. </td>
            <td>10 - Boton para guardar los datos. </td>
          </tr>
          <tr>
            <td>5 - Campo para digitar la observaci&oacute;n </td>
            <td>11 - Boton para salir de la vista. </td>
          </tr>
        </table></td>
		</tr>
		<tr>
		<td  class="ayudaHtmlTexto"><div align="center">
		  <p><IMG src="<%=BASEIMG%>CreacionDiscrepancia.JPG" border=0 ></p>
		  <p><strong>Imagen4</strong></p>
		</div></td>
		</tr>			
		<tr>
		<td  class="ayudaHtmlTexto"><p class="ayudaHtmlTexto">&nbsp;</p>
		<p class="ayudaHtmlTexto">El sistema verifica que el campo de la fecha  esta lleno, si no lo esta  aparecer&aacute; como se ilustra en la Imagen5. </p>
		</td>
		</tr>
		<tr>
		<td  class="ayudaHtmlTexto"><div align="center">
		  <p><IMG src="<%=BASEIMG%>MensajeNoFecha.JPG" border=0 ></p>
		  <p><strong>Imagen5</strong></p>
		</div></td>
		</tr>
		<tr>
		<td  class="ayudaHtmlTexto"><p class="ayudaHtmlTexto">&nbsp;</p>
		<p class="ayudaHtmlTexto">El sistema verifica que la fecha de discrepancia no sea menor a la fecha actual, si no lo es aparecer&aacute; como se ilustra en la Imagen6. </p>
		</td>
		</tr>
		<tr>
		<td  class="ayudaHtmlTexto"><div align="center">
		  <p><IMG src="<%=BASEIMG%>MensajeFechaMayor.JPG" border=0 ></p>
		  <p><strong>Imagen6</strong></p>
		</div></td>
		</tr>		
		<tr>
		<td  class="ayudaHtmlTexto"><p class="ayudaHtmlTexto">&nbsp;</p>
		<p class="ayudaHtmlTexto">El sistema verifica que el campo de la ubicaci&oacute;n de la mercancia este lleno, si no lo esta  aparecer&aacute; como se ilustra en la Imagen7. </p>
		</td>
		</tr>
		<tr>
		<td  class="ayudaHtmlTexto"><div align="center">
		  <p><IMG src="<%=BASEIMG%>MensajeNoFecha.JPG" border=0 ></p>
		  <p><strong>Imagen7</strong></p>
		</div></td>
		</tr>				  
		<tr>
		<td  class="ayudaHtmlTexto"><p class="ayudaHtmlTexto">&nbsp;</p>
		<p class="ayudaHtmlTexto">Si crea una discrepancia la planilla exitosamente aparecer&aacute; como se ilustra en la Imagen8. </p>
		</td>
		</tr>
		<tr>
		<td  class="ayudaHtmlTexto"><div align="center">
		  <p><IMG src="<%=BASEIMG%>MensajeDiscrepanciaAgregada.JPG" border=0 ></p>
		  <p><strong>Imagen8</strong></p>
		</div></td>
		</tr>			  
		<tr>
		<td  class="ayudaHtmlTexto"><p class="ayudaHtmlTexto">&nbsp;</p>
		<p class="ayudaHtmlTexto">Si modifica una discrepancia aparecer&aacute; como se ilustra en la Imagen9. </p>
		</td>
		</tr>
		<tr>
		<td  class="ayudaHtmlTexto"><div align="center">
		  <p><IMG src="<%=BASEIMG%>MensajeDiscrepanciaModificada.JPG" border=0 ></p>
		  <p><strong>Imagen9</strong></p>
		</div></td>
		</tr>		
		<tr>
		<td  class="ayudaHtmlTexto"><p class="ayudaHtmlTexto">&nbsp;</p>
		<p class="ayudaHtmlTexto">Si anula una discrepancia aparecer&aacute; como se ilustra en la Imagen10. </p>
		</td>
		</tr>
		<tr>
		<td  class="ayudaHtmlTexto"><div align="center">
		  <p><IMG src="<%=BASEIMG%>MensajeDiscrepanciaModificada.JPG" border=0 ></p>
		  <p><strong>Imagen10</strong></p>
		</div></td>
		</tr>		  
		  
		<tr>
		<td  class="ayudaHtmlTexto"><p class="ayudaHtmlTexto">&nbsp;</p>
		<p class="ayudaHtmlTexto">En la siguiente pantalla como se ilustra en la Imagen11 se muestra los datos correspondientes para ingresar un producto a la discrepancia. en este modulo primero debemos colocar el codigo del producto o buscarlos con el boton 12.</p>
		<table width="100%"  border="0" cellpadding="0" cellspacing="0" class="ayudaHtmlTexto">
          <tr>
            <td width="50%">1 - Campo para digitar el codigo del producto. </td>
            <td width="50%">7 - Campo de informacion del producto. </td>
          </tr>
          <tr>
            <td>2 - Campo para digitar la cantidad del producto. </td>
            <td>8 - Campo para seleccionar el responsable del producto.</td>
          </tr>
          <tr>
            <td>3 - Campo para seleccionar el tipo de discrepancia del producto. </td>
            <td>9 - Campo para digitar el valor del producto.</td>
          </tr>
          <tr>
            <td>4 - Campo para seleccionar la cusa de la discrepancia del producto.</td>
            <td>10 - Boton para salir de la vista. </td>
          </tr>
          <tr>
            <td>5 - Boton para guardar los datos. </td>
            <td>11 - Lista de datos de los productos asociados a la discrepancia. </td>
          </tr>
          <tr>
            <td>6 - Boton para reiniciar los valores a su estado inicial. </td>
            <td>13 - Boton para buscar el produto. </td>
          </tr>
          <tr>
            <td>12 - Boton para  crear o escoger productos.</td>
            <td>&nbsp;</td>
          </tr>
        </table>		<p class="ayudaHtmlTexto">&nbsp;</p></td>
		</tr>
		<tr>
		<td  class="ayudaHtmlTexto"><div align="center">
		  <p><IMG src="<%=BASEIMG%>CreacionProductoDiscrepancia.JPG" border=0 ></p>
		  <p><strong>Imagen11</strong></p>
		  </div></td>
		</tr>
		
		<tr>
		<td  class="ayudaHtmlTexto"><p class="ayudaHtmlTexto">&nbsp;</p>
		<p class="ayudaHtmlTexto">En la siguiente pantalla como se ilustra en la Imagen12, permitir&aacute; crear productos o listar todos los  productos del cliente ya creados. En el listado de los productos de los clientes al darle click en el dato se le coloca el codigo del producto asignado en el campo de &quot;codigo del producto&quot; de la vista de discrepancia como se ilustra en la Imagen13</p>
		</td>
		</tr>
		<tr>
		<td  class="ayudaHtmlTexto"><div align="center">
		  <p><IMG src="<%=BASEIMG%>CreacionProducto.JPG" border=0 ></p>
		  <p><strong>Imagen12</strong></p>
		</div></td>
		</tr>
		<tr>
		<td  class="ayudaHtmlTexto"><p class="ayudaHtmlTexto">&nbsp;</p>
		<p class="ayudaHtmlTexto">Al escoger un datos como se ilustra en el item 11 de la Imagen11. presenta la Imagen13 para modificar o anular el producto de discrepancia. </p>
		</td>
		</tr>
		<tr>
		<td  class="ayudaHtmlTexto"><div align="center">
		  <p><IMG src="<%=BASEIMG%>ModificarProductoDiscrepancia.JPG" border=0 ></p>
		  <p><strong>Imagen13</strong></p>
		  </div></td>
		</tr>
<tr>
		<td  class="ayudaHtmlTexto"><p class="ayudaHtmlTexto">&nbsp;</p>
		<p class="ayudaHtmlTexto">Si crea un producto exitosamente aparecer&aacute; como se ilustra en la Imagen14. </p>
		</td>
		</tr>
		<tr>
		<td  class="ayudaHtmlTexto"><div align="center">
		  <p><IMG src="<%=BASEIMG%>MensajeProductoAgregado.JPG" border=0 ></p>
		  <p><strong>Imagen14</strong></p>
		</div></td>
		</tr>			  
		<tr>
		<td  class="ayudaHtmlTexto"><p class="ayudaHtmlTexto">&nbsp;</p>
		<p class="ayudaHtmlTexto">Si modifica un producto de discrepancia aparecer&aacute; como se ilustra en la Imagen15. </p>
		</td>
		</tr>
		<tr>
		<td  class="ayudaHtmlTexto"><div align="center">
		  <p><IMG src="<%=BASEIMG%>MensajeProductoModificado.JPG" border=0 ></p>
		  <p><strong>Imagen15</strong></p>
		</div></td>
		</tr>		
		<tr>
		<td  class="ayudaHtmlTexto"><p class="ayudaHtmlTexto">&nbsp;</p>
		<p class="ayudaHtmlTexto">Si anula una discrepancia aparecer&aacute; como se ilustra en la Imagen16. </p>
		</td>
		</tr>
		<tr>
		<td  class="ayudaHtmlTexto"><div align="center">
		  <p><IMG src="<%=BASEIMG_ACU%>MensajeAnular.JPG" border=0 ></p>
		  <p><strong>Imagen16</strong></p>
		</div></td>
		</tr>		
					
		<tr>
		<td  class="ayudaHtmlTexto"><p class="ayudaHtmlTexto">&nbsp;</p>
		<p class="ayudaHtmlTexto">Si no llena el campo del codigo del producto  y presiona el boton 13 como ilustra en la Imagen11 aparecer&aacute; como se ilustra en la Imagen17. </p>
		</td>
		</tr>
		<tr>
		<td  class="ayudaHtmlTexto"><div align="center">
		  <p><IMG src="<%=BASEIMG%>MensajeNoCodigoProducto.JPG" border=0 ></p>
		  <p><strong>Imagen17</strong></p>
		</div></td>
		</tr>
		<tr>
		<td  class="ayudaHtmlTexto"><p class="ayudaHtmlTexto">&nbsp;</p>
		<p class="ayudaHtmlTexto"><span class="ayudaHtmlTexto">El sistema verifica que el campo de la cantidad este lleno, si no lo esta  aparecer&aacute; como se ilustra en la Imagen18.</span>. </p>
		</td>
		</tr>
		<tr>
		<td  class="ayudaHtmlTexto"><div align="center">
		  <p><IMG src="<%=BASEIMG%>MensajeCantidad.JPG" border=0 ></p>
		  <p><strong>Imagen18</strong></p>
		</div></td>
		</tr>		
      </table></td>
    </tr>
  </table>
  <p></p>
<center>
<img src = "<%=BASEURL%>/images/botones/salir.gif" style = "cursor:hand" name = "imgsalir" onClick = "parent.close();" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);">
</center>
</BODY>
</HTML>
