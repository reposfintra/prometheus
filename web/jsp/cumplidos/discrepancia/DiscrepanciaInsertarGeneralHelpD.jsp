<!--  
	 - Author(s)       :      Jose de la rosa
	 - Description     :      AYUDA DESCRIPTIVA - discrepancia
	 - Date            :      11/06/2006 
	 - Copyright Notice:      TRANSPORTES SANCHEZ POLO S.A.
-->
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN""http://www.w3.org/TR/html4/loose.dtd">
<%@page session="true"%> 
<%@page errorPage="/error/ErrorPage.jsp"%>
<%@page import="com.tsp.operation.model.beans.*, java.util.*"%>
<%@ include file="/WEB-INF/InitModel.jsp"%>
<%@ taglib uri="http://jakarta.apache.org/taglibs/input-0.90" prefix="input" %>
<html>
<head>
<title>AYUDA DESCRIPTIVA - Discrepancia General</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link href="<%= BASEURL %>/css/estilostsp.css" rel="stylesheet" type="text/css">
<script type='text/javascript' src="<%= BASEURL %>/js/boton.js"></script>
</head>

<body>
<br>
<table width="750"  border="2" align="center">
  <tr>
    <td width="100%" >
      <table width="100%" border="0" align="center">
        <tr  class="subtitulo">
          <td height="24" colspan="2"><div align="center">Discrepancia General </div></td>
        </tr>
        <tr class="subtitulo1">
          <td colspan="2">Crear Discrepancia General </td>
        </tr>
		<tr class="subtitulo1">
          <td colspan="2">INFORMACION DE LA DISCREPANCIA GENERAL </td>
        </tr>
        <tr>
          <td  class="fila">Retención Pago</td>
          <td  class="ayudaHtmlTexto">Campo para seleccionar si se le va a aretener el pago o no.</td>
        </tr>		
		<tr>
          <td  class="fila">Tipo Discrepancia</td>
          <td  class="ayudaHtmlTexto">Campo para seleccionar el tipo de discrepancia. </td>
        </tr>	
		<tr>
          <td  class="fila">Cantidad</td>
          <td  class="ayudaHtmlTexto">Campo para digitar la cantidad del producto. Este campo es de m&aacute;ximo 10 caracteres. </td>
        </tr>	
		<tr>
          <td  class="fila">Valor</td>
          <td  class="ayudaHtmlTexto">Campo para digitar el valor del producto. Este campo es de m&aacute;ximo 10 caracteres. </td>
        </tr>
        <tr>
          <td  class="fila">Observación</td>
          <td  class="ayudaHtmlTexto">Campo para digitar los comentarios del cumplido de  la remesa.</td>
        </tr>

        <tr>		
          <td class="fila">Bot&oacute;n Aceptar.</td>
          <td  class="ayudaHtmlTexto">Bot&oacute;n para guardar los  datos creados a la discrepancia. </td>
        </tr>
		<tr>
          <td class="fila">Bot&oacute;n Cancelar </td>
          <td  class="ayudaHtmlTexto">Bot&oacute;n que resetea la pantalla dejandola en su estado inicial de ingreso.</td>
	    </tr>		
		<tr>
          <td width="25%" class="fila">Bot&oacute;n Salir.</td>
          <td width="75%"  class="ayudaHtmlTexto">Bot&oacute;n para salir de la vista 'Discrepancia' y volver a la vista del men&uacute;.</td>
        </tr>		
		<tr>
          <td class="fila">Bot&oacute;n Regresar.</td>
          <td class="ayudaHtmlTexto">Bot&oacute;n para salir de la vista 'Discrepancia' y volver a la vista de la planilla.</td>
        </tr>		
      </table>
    </td>
  </tr>
</table>
<p></p>
<center>
<img src = "<%=BASEURL%>/images/botones/salir.gif" style = "cursor:hand" name = "imgsalir" onClick = "parent.close();" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);">
</center>
</body>
</html>
