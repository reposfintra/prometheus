<!--
- Autor : Ing. Jose de la rosa
- Date  : 18 de Noviembre de 2005
- Copyrigth Notice : Fintravalores S.A. S.A
-->
<%--
-@(#)
--Descripcion : Pagina JSP, que maneja el ingreso de las discrepancias generales
--%>

<%-- Declaracion de librerias--%>
<%@page contentType="text/html"%>
<%@page session="true"%>
<%@page import="java.util.*"%>
<%@page import="com.tsp.operation.model.*"%>
<%@page import="com.tsp.util.*"%>
<%@page import="com.tsp.operation.model.beans.*, java.util.*"%>
<%@include file="/WEB-INF/InitModel.jsp"%>
<%@taglib uri="http://jsptags.com/tags/navigation/pager" prefix="pg" %>
<html>
<head>
<title>Discrepancia General</title>
<link href="<%= BASEURL %>/css/estilostsp.css" rel="stylesheet" type="text/css">
<script type='text/javascript' src="<%= BASEURL %>/js/boton.js"></script>
</head>
<script src='<%=BASEURL%>/js/date-picker.js'></script>
<script type='text/javascript' src="<%= BASEURL %>/js/validar.js"></script>
<script type='text/javascript' src="<%= BASEURL %>/js/reporte.js"></script>
<%
	String path = application.getRealPath(model.menuService.getRealUrl(request.getRequestURI()));
	String datos[] = model.menuService.getContenidoMenu(BASEURL,request.getRequestURI(),path);
 	String style = "simple";
    String position =  "bottom";
    String index =  "center";
    int maxPageItems = 10;
    int maxIndexPages = 10;
	Discrepancia d;
    String numpla = request.getParameter("c_numpla");
    InfoPlanilla info = model.planillaService.getInfoPlanilla();
    info.setPropietario(model.planillaService.getNomPropie());
    String conductor = info.getCedulaConductor() + " " + info.getNombreConductor();
    String ruta = info.getOrigen() + " - " + info.getDestino();
    String fecha = info.getFecha();
    String placa = info.getVehiculo();
    String propietario = info.getPropietario();
    Vector vec = model.discrepanciaService.getDiscrepancias();
%>
<%-- Inicio Body --%>
<body>
<div id="capaSuperior" style="position:absolute; width:100%; height:100px; z-index:0; left: 0px; top: 0px;">
 <jsp:include page="/WEB-INF/toptsp.jsp?encabezado=Ingresar Discrepancia General"/>
</div>

 <div id="capaCentral" style="position:absolute; width:100%; height:85%; z-index:0; left: 0px; top: 100px; overflow: scroll;"> 
 
<%-- Tabla del detalle de la planilla --%>
<table width="900" border="2" align="center">          
	<tr>
		<td>
			<table width="100%" class="tablaInferior">
				<tr>
					<td width="50%" colspan="2" class="subtitulo1">&nbsp;Detalle de la Planilla</td>
					<td width="50%" colspan="2" class="barratitulo"><img src="<%=BASEURL%>/images/titulo.gif" width="32" height="20" align="left"><%=datos[0]%></td>
				</tr>
				<tr>
					<td width="14%" class="fila">No.:</td>
					<td width="36%" class="letra"><%=numpla %></td>
					<td width="14%" class="fila">Fecha:</td>
					<td width="36%" class="letra"><%=fecha %></td>
				</tr>
				<tr>
					<td class="fila">Conductor:</td>
					<td class="letra"><%=conductor %></td>
					<td class="fila">Placa:</td>
					<td class="letra"><%=placa %></td>
				</tr>
				<tr>
					<td class="fila">Ruta:</td>
					<td class="letra"><%=ruta %></td>
					<td class="fila">Propietario:</td>
					<td class="letra"><%= propietario %></td>
				</tr>
			</table>
		</td>
	</tr>
</table>
<FORM name='forma3' method='POST' action="" onSubmit="return validarDiscrepanciaGeneral();" id="forma3">	
	<%-- Tabla de las discrepancias --%>
	<table width="900" border="2" align="center">          
		<tr>
			<td>
				<table width="100%" class="tablaInferior">
					<%-- Formulario de ingreso y modificacion de discrepancia --%>
					<tr class="fila">
						<td colspan="2" class="subtitulo1">&nbsp;Discrepancia</td>
						<td colspan="2" class="barratitulo"><img src="<%=BASEURL%>/images/titulo.gif" width="32" height="20"></td>
					</tr>								
					<tr class="fila">
						<td width="14%">Retenci&oacute;n Pago </td>
						<td width="36%"><select name="c_retencion" class="textbox" id="c_retencion">
						  <option value="S">Si</option>
						  <option value="N">No</option>
						</select></td>
						<td width="14%">Tipo Discrepancia </td>
						<td width="36%"><select name="c_cod_discrepancia" class="textbox"  id="c_cod_discrepancia" onChange="cargarDiscrepancia('<%=CONTROLLER%>','<%=numpla%>');">
						  <%Vector vecd = model.codigo_discrepanciaService.listarCodigo_discrepancias(); 
							for(int i = 0; i<vecd.size(); i++){	
								Codigo_discrepancia c = (Codigo_discrepancia) vecd.elementAt(i);	%>
								<option value="<%=c.getCodigo()%>"><%=c.getDescripcion()%></option>
						  <%}%>
						</select>
						  <img src="<%=BASEURL%>/images/botones/iconos/obligatorio.gif"> </td>
					</tr>
					<tr class="fila">
						<td align="left"> Cantidad </td>
					  <td><input name="c_cantidad" type="text" class="textbox" id="c_cantidad" size="10" maxlength="10" onKeyPress="soloDigitos(event,'decOK')">
						  <select name="c_unidad" id="c_unidad" class="textbox" style='width:70%;'>
							<%  Vector vecu2 = model.unidadService.listarUnidades(); 
							for(int i = 0; i<vecu2.size(); i++){	
								Unidad u2 = (Unidad) vecu2.elementAt(i);%>
								<option value="<%=u2.getCodigo()%>"><%=u2.getDescripcion()%> </option>
                                                    <%}%>
						  </select><img src="<%=BASEURL%>/images/botones/iconos/obligatorio.gif"></td>
						<td valign="middle">Valor</td>
						<td valign="middle"><input name="c_valor" type="text" class="textbox" id="c_valor" size="10" maxlength="10" onKeyPress="soloDigitos(event,'decOK')">
						</td>
					</tr>
					<tr class="fila">
						<td> Observaci&oacute;n </td>
						<td colspan="3"><textarea name="c_observacion" cols="150" rows="3" class="textbox" id="c_observacion"></textarea></td>
					</tr>					
				</table>
			</td>
		</tr>
	</table>
	<input name="c_numpla" id="c_numpla" value="<%=numpla%>" type="hidden">
	<p align="center">
		<input type="image" src="<%=BASEURL%>/images/botones/aceptar.gif" title="Agregar una discrepancia" style="cursor:hand" name="modificar" onClick="forma3.action='<%=CONTROLLER%>?estado=Discrepancia_general&accion=Insert'"  onMouseOver="botonOver(this);" onMouseOut="botonOut(this);">&nbsp;
		<img src="<%=BASEURL%>/images/botones/cancelar.gif" style="cursor:hand" name="regresar" title="Limpiar todos los registros" onMouseOver="botonOver(this);" onClick="forma3.reset();" onMouseOut="botonOut(this);" >&nbsp;	
		<img src="<%=BASEURL%>/images/botones/salir.gif" style="cursor:hand" name="salir" title="Salir al Menu Principal" onMouseOver="botonOver(this);" onClick="parent.close();" onMouseOut="botonOut(this);" >&nbsp;
		<img src="<%=BASEURL%>/images/botones/regresar.gif" style="cursor:hand" title="Volver" name="buscar"  onClick="window.location='<%=CONTROLLER%>?estado=Menu&accion=Cargar&carpeta=/jsp/cumplidos/discrepancia&pagina=asignarDiscrepanciaPlanilla.jsp?msg=&marco=no'" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);">
	</p>
	<table width="900" border="2" align="center">
      <tr>
        <td>
          <table width="100%" align="center">
            <tr>
              <td width="373" class="subtitulo1">&nbsp;Listar Discrepancias Generales </td>
              <td width="427" class="barratitulo"><img src="<%=BASEURL%>/images/titulo.gif" width="32" height="20"></td>
            </tr>
          </table>
          <table width="100%" border="1" bordercolor="#999999" bgcolor="#F7F5F4" align="center">
			<tr class="tblTitulo" align="center">
				<td width="11%" align="center">Numero Planilla</td>
				<td width="8%"  align="center">Retenci&oacute;n Pago </td>
				<td width="14%" align="center">Tipo Discrepancia </td>
				<td width="15%" align="center">Cantidad</td>
				<td width="13%" align="center">Valor</td>
				<td width="39%" align="center">Observaci&oacute;n</td>
			</tr>
			<pg:pager
					items="<%=vec.size()%>"
					index="<%= index %>"
					maxPageItems="<%= maxPageItems %>"
					maxIndexPages="<%= maxIndexPages %>"
					isOffset="<%= true %>"
					export="offset,currentPageNumber=pageNumber"
					scope="request"><%
			for (int i = offset.intValue(), l = Math.min(i + maxPageItems, vec.size()); i < l; i++){
				d = (Discrepancia) vec.elementAt(i);%>
				<tr class="<%=(i % 2 == 0 )?"filagris":"filaazul"%>" onMouseOver='cambiarColorMouse(this)'   style="cursor:hand"
					onClick="window.location='<%=CONTROLLER%>?estado=Informacion&accion=Planilla_discrepancia&tipo=M&c_num_discre=<%=d.getNro_discrepancia()%>&numpla=<%=d.getNro_planilla()%>'">
					<td width="11%" align="center" class="bordereporte"><%=d.getNro_planilla()%></td>
					<td width="8%" align="center" class="bordereporte"><% if(d.getRetencion().equals("S")){%>SI<%}else{%>NO<%}%></td>
					<td width="14%" align="center" class="bordereporte"><%if(!d.getCod_discrepancia().equals("")){model.codigo_discrepanciaService.serchCodigo_discrepancia(d.getCod_discrepancia());
													Codigo_discrepancia cd = model.codigo_discrepanciaService.getCodigo_discrepancia(); %><%=cd.getDescripcion()%><%}%></td>
					<td width="15%" align="center" class="bordereporte"><%=Util.PuntoDeMil(String.valueOf ( Math.round ( d.getCantidad() ) ) )%> 
													<%if(!d.getUnidad().equals("")){model.unidadService.buscarUnidad(d.getUnidad()); Unidad uni = model.unidadService.getUnidad();%><%=uni.getDescripcion()%><%}%></td>
					<td width="13%" align="center" class="bordereporte"><%=Util.PuntoDeMil(String.valueOf ( Math.round ( d.getValor() ) ) )%> </td>
					<td width="39%" align="center" class="bordereporte"><%=d.getObservacion()%></td>
				</tr>
            <%}%>
			</pg:pager>
        </table></td>
      </tr>
    </table>
	<p align="center">&nbsp;	</p>
</form>	
<%
if(request.getAttribute("msg")!=null){%>
	<%--Mensaje de informacion--%>
	<table border="2" align="center">
		<tr>
			<td>
				<table width="100%"  border="1" align="center"  bordercolor="#F7F5F4" bgcolor="#FFFFFF">
					<tr>
						<td width="229" align="center" class="mensajes"><%=request.getAttribute("msg")%></td>
						<td width="29" background="<%=BASEURL%>/images/cuadronaranja.JPG">&nbsp;</td>
						<td width="58">&nbsp;</td>
					</tr>
				</table>
			</td>
		</tr>
	</table>
<%}%>
</div>
<%=datos[1]%>
</body>
</html>
<script>
function cargarDiscrepancia ( BASEURL , numpla){
	if(forma3.c_cod_discrepancia.value=='SI'){
		window.location=BASEURL+"?estado=Informacion&accion=Planilla_discrepancia&numpla="+numpla+"&tipo=S";
	}
}	
</script>
