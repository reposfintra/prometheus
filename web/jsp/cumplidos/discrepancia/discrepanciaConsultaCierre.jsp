<!--
- Autor : Ing. Jose de la rosa
- Date  : 9 de Nobiembre de 2005
- Copyrigth Notice : Fintravalores S.A. S.A
-->
<%--
-@(#)
--Descripcion : Pagina JSP, que mustra la informaci�n sobre los productos de cierre
--%>

<%-- Declaracion de librerias--%>
<%@page contentType="text/html"%>
<%@page session="true"%>
<%@page import="java.util.*"%>
<%@page import="com.tsp.operation.model.*"%>
<%@page import="com.tsp.util.*"%>
<%@page import="com.tsp.operation.model.beans.*, java.util.*"%>
<%@include file="/WEB-INF/InitModel.jsp"%>
<%@taglib uri="http://jsptags.com/tags/navigation/pager" prefix="pg" %>
<html>
<head>
<title>Discrepancia Consulta</title>
<link href="<%= BASEURL %>/css/estilostsp.css" rel="stylesheet" type="text/css">
<script type='text/javascript' src="<%= BASEURL %>/js/boton.js"></script>
<script src='<%=BASEURL%>/js/date-picker.js'></script>
<script type='text/javascript' src="<%= BASEURL %>/js/validar.js"></script>
<script type='text/javascript' src="<%= BASEURL %>/js/reporte.js"></script>
</head>
<%-- Inicio Body --%>
<body>
<%  String mensaje = (String) request.getAttribute("msg");
    String mensajeDis = (String) request.getAttribute("msgDiscrepancia");
    String res = "";
    Usuario usuario = (Usuario) session.getAttribute ("Usuario");
    String numpla = request.getParameter("c_numpla");
    String sw = request.getParameter("sw");
    InfoPlanilla info = model.planillaService.getInfoPlanilla();
    info.setPropietario(model.planillaService.getNomPropie());
    String conductor = info.getCedulaConductor() + " " + info.getNombreConductor();
    String ruta = info.getOrigen() + " - " + info.getDestino();
    String fecha = info.getFecha();
    String placa = info.getVehiculo();
	String propietario = info.getPropietario();
	String exis = (sw.equals("true"))?request.getParameter("exis"):"";
	String tipo_docu = request.getParameter("c_tipo_doc");
	String documen = request.getParameter("c_documento");
	String tipo_docu_rel = request.getParameter("c_tipo_doc_rel");
	String documen_rel = request.getParameter("c_documento_rel");	
	String planilla = "";
	String remesa = request.getParameter("c_numrem");
	String fecha_dev = "";
	String numero_rech = "";
	String contac = "";
	String report = "";
	String ubica = "";
	String retencion = "";
	String num_discre = request.getParameter("c_num_discre");
	int nu = Integer.parseInt(num_discre);
	String paso = "";
	String cod = "";
	String desc = "";
	String uni = "";
	String nom_doc = (request.getParameter("nom_doc")!=null)?request.getParameter("nom_doc"):"";
	int num_d = 0;
	String cliente = "";
	String fechadsp = "";
	String orirem = "";
	String desrem = "";
	String nomtipodoc = "";
	String documento = "";
	String tdocrel = "";
	String docrel = "";
	String destinatario = "";
	//nuevo
	String fecha_cierre = "";
	String observacion_cierre = "";
	String referencia = "";
	String nota_debito = "";
	String nota_credito = "";
	String valor = "";
	Vector vec = model.planillaService.ReporteVector();
    Planilla p;
	if ( vec.size() >0 ){  
      	for(int i = 0; i<vec.size(); i++){	
	  		p = (Planilla) vec.elementAt(i);
			remesa=p.getNumrem();
			cliente=p.getNitpro();
			fechadsp=p.getFecdsp();
			orirem=p.getNomori();
			desrem=p.getNomdest();
		}
		String numrem = request.getParameter("c_numrem");
		String campos = request.getParameter("campos");
		model.remesaService.consultaRemesaDiscrepancia(numpla, remesa, tipo_docu, documen, nu);
		Vector Vecrem = model.remesaService.getRemes();
		Remesa r;
		if ( Vecrem.size() >0 ){
			for(int i = 0; i<Vecrem.size(); i++){	
				r = (Remesa) Vecrem.elementAt(i);
				nomtipodoc=r.getNombre_tipo_doc();
				documento=r.getDocumento();
				tdocrel=r.getNombre_tipo_doc_rel();
				docrel=r.getDocumento_rel();
				destinatario=r.getNom_destinatario();
			}
			Discrepancia dis = (Discrepancia) model.discrepanciaService.getDiscrepancia();
			planilla = dis.getNro_planilla();
			remesa = dis.getNro_remesa();
			tipo_docu = dis.getTipo_doc();
			documen = dis.getDocumento();
			tipo_docu_rel = dis.getTipo_doc_rel();
			documen_rel = dis.getDocumento_rel();			
			fecha_dev = dis.getFecha_devolucion().substring(0,10);
			numero_rech = String.valueOf(dis.getNumero_rechazo());
			contac = dis.getContacto();
			report = dis.getReportado();
			retencion = dis.getRetencion();
			ubica = dis.getUbicacion();
			num_discre = String.valueOf(dis.getNro_discrepancia());
			num_d = dis.getNro_discrepancia();
			
			fecha_cierre = (dis.getFecha_cierre ()!=null)?dis.getFecha_cierre ():"";
			observacion_cierre = (dis.getObservacion_cierre ()!=null)?dis.getObservacion_cierre ():"";
			referencia = (dis.getReferencia ()!=null)?dis.getReferencia ():"";
			nota_debito = (dis.getNota_debito ()!=null)?dis.getNota_debito ():"";
			nota_credito = (dis.getNota_credito ()!=null)?dis.getNota_credito ():"";
			valor = String.valueOf(dis.getValor_autorizado ());
				

			Vector Vecdis = model.discrepanciaService.getDiscrepancias();
			Discrepancia d; %>
			<%-- Tabla del detalles de la discrepancia --%>
			<div id="capaSuperior" style="position:absolute; width:100%; height:100px; z-index:0; left: 0px; top: 0px;">
			<jsp:include page="/WEB-INF/toptsp.jsp?encabezado=Cierre Discrepancia Por Documentos"/>
			</div>
			
			<div id="capaCentral" style="position:absolute; width:100%; height:85%; z-index:0; left: 0px; top: 100px; overflow: scroll;">
			<table width="1000" border="2" align="center">
				<tr>
					<td width="50%">
						<table width="100%" class="tablaInferior">
							<tr>
								<td width="50%" class="subtitulo1">&nbsp;Detalle Discrepancia</td>
								<td width="50%" class="barratitulo"><img src="<%=BASEURL%>/images/titulo.gif" width="32" height="20"></td>
							</tr>
						</table>
						<table width="100%"  border="0" cellpadding="0" cellspacing="0">
							<tr>
								<td width="50%" rowspan="2">
									<table width="100%"  border="2">
										<tr>
											<td>
												<table width="100%" height="150" class="tablaInferior">
													<tr class="fila">
														<td width="22%" >No Planilla </td>
														<td width="78%" ><%=numpla %></td>
													</tr>
													<tr class="fila">
														<td >Conductor</td>
														<td ><%=conductor %></td>
													</tr>
													<tr class="fila">
														<td >Ruta</td>
														<td ><%=ruta %></td>
													</tr>
													<tr class="fila">
														<td >Fecha</td>
														<td ><%=fecha %></td>
													</tr>
													<tr class="fila">
														<td >Placa</td>
														<td ><%=placa %></td>
													</tr>
													<tr class="fila">
														<td >Propietario</td>
														<td ><%= propietario %></td>
													</tr>
												</table>
											</td>
										</tr>
									</table>
								</td>
								<td width="50%">
									<table width="100%"  border="2" align="center">
										<tr>
											<td>
												<table width="100%" height="150" class="tablaInferior">
													<tr class="fila">
														<td width="25%">No Remesa</td>
														<td width="75%"><%=remesa%></td>
													</tr>
													<tr class="fila">
														<td>Cliente</td>
														<td><%=cliente%></td>
													</tr>
													<tr class="fila">
														<td>Fecha</td>
														<td><%=fechadsp%></td>
													</tr>
													<tr class="fila">
														<td>Origen Remesa </td>
														<td><%=orirem%></td>
													</tr>
													<tr class="fila">
														<td>Destino Remesa </td>
														<td><%=desrem%></td>
													</tr>
												</table>
											</td>
										</tr>
									</table>
								</td>
							</tr>
							<tr>
								<td rowspan="2">
									<table width="100%"  border="2">
										<tr>
											<td>
												<table width="100%" height="150" align="left" class="tablaInferior">
													<tr class="fila">
														<td width="31%">Fecha Devoluci&oacute;n</td>
														<td width="69%"><%=fecha_dev%></td>
													</tr>
													<tr class="fila">
														<td>Retenci&oacute;n Pago </td>
														<td><% if(retencion.equals("S")){%>Si<%}else{%>No<%}%></td>
													</tr>
													<tr class="fila">
														<td>Numero Devoluci&oacute;n </td>
														<td><%=numero_rech%></td>
													</tr>
													<tr class="fila">
														<td>Contacto del Cliente</td>
														<td><%=contac%></td>
													</tr>
													<tr class="fila">
														<td>Reportado Por </td>
														<td><%=report%></td>
													</tr>
													<tr class="fila">
														<td>Ubicaci&oacute;n Mercancia </td>
														<% Ubicacion u = model.ubService.getUb ();%>
                                                                                                		<td><%=u.getDescripcion ()%></td>
													</tr>
												</table>
											</td>
										</tr>
									</table>
								</td>
							</tr>
							<tr>
								<td>
									<table width="100%"  border="2">
										<tr>
											<td>
												<table width="100%" height="150" align="left" class="tablaInferior">
													<tr class="fila">
														<td width="36%">Tipo Documento </td>
														<td width="64%"><%=nomtipodoc%></td>
													</tr>
													<tr class="fila">
														<td>Documento</td>
														<td><%=documento%></td>
													</tr>
														<tr class="fila">
														<td>Tipo Doc Relacionado</td>
														<td><%=tdocrel%></td>
													</tr>
													<tr class="fila">
														<td>Documento Relacionado</td>
														<td><%=docrel%></td>
													</tr>
													<tr class="fila">
														<td>Nombre Destinatario </td>
														<td><%=destinatario%></td>
													</tr>
												</table>
											</td>
										</tr>
									</table>
								</td>
							</tr>
						</table>	  
					</td>
				</tr>
			</table>
			<%-- Tabla del producto de la discrepancia --%>
			<FORM name='forma' method='POST'  action="<%=CONTROLLER%>?estado=Discrepancia&accion=Cierre"> 
            <table width="1000" border="2" align="center">
              <tr>
                <td>
				
                  <table width="100%" class="tablaInferior">
                    <%-- Formulario de ingreso y modificacion de discrepancia --%>
                    <tr class="fila">
                      <td colspan="2" class="subtitulo1">&nbsp;Cierre Discrepancia </td>
                      <td colspan="2" class="barratitulo"><img src="<%=BASEURL%>/images/titulo.gif" width="32" height="20"></td>
                    </tr>
                    <tr class="fila">
                      <td width="13%" >Nota Debito </td>
                      <td width="38%" ><input name="c_nota_debito" type="text" class="textbox" <%=(nota_debito.equals(""))?"":"readonly"%> id="c_nota_debito" value="<%=nota_debito%>" size="10" maxlength="10"></td>
                      <td width="10%" >Nota Credito </td>
                      <td width="39%" ><input name="c_nota_credito" type="text" class="textbox" <%=(nota_credito.equals(""))?"":"readonly"%> id="c_nota_credito" value="<%=nota_credito%>" size="10" maxlength="10"></td>
                    </tr>
                    <tr class="fila">
                      <td> Valor Aprobado</td>
                      <td><input name="c_cantidad" type="text" class="textbox" id="c_cantidad" size="10" value="<%=valor%>" maxlength="10" onKeyPress="soloDigitos(event,'decOK')"></td>
                      <td>Referencia</td>
                      <td><input name="c_referencia" type="text" class="textbox" id="c_referencia" size="10" value="<%=referencia%>" maxlength="10"></td>
                    </tr>
                    <tr class="fila">
                      <td>Observaci&oacute;n</td>
                      <td colspan="3"><textarea name="c_observacion" cols="169" rows="5" class="textbox" id="c_observacion"><%=observacion_cierre%></textarea></td>
                    </tr>
                </table></td>
              </tr>
            </table>			
				<p align="center">
					<img src="<%=BASEURL%>/images/botones/aceptar.gif" title="Cerrar Discrepancia" onClick="validar_campos();" style="cursor:hand" name="Aceptar_" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);">&nbsp;
					<img src="<%=BASEURL%>/images/botones/salir.gif" style="cursor:hand" name="salir" title="Salir al Menu Principal" onMouseOver="botonOver(this);" onClick="parent.close();" onMouseOut="botonOut(this);" >
				</p>
				<input name="c_num_discre" type="hidden" value="<%=num_discre%>">
				<input name="c_numpla" type="hidden" value="<%=numpla%>">
				<input name="c_numrem" type="hidden" value="<%=numrem%>">
				<input name="c_tipo_doc" type="hidden" value="<%=tipo_docu%>">
				<input name="c_documento" type="hidden" value="<%=documen%>">
				<input name="c_tipo_doc_rel" type="hidden" value="<%=tipo_docu_rel%>">
				<input name="c_documento_rel" type="hidden" value="<%=documen_rel%>">	
		</form>
		<% if(mensajeDis!=null){%>
		<%--Mensaje de informacion--%>
		<table border="2" align="center">
			<tr>
				<td>
					<table width="100%"  border="1" align="center"  bordercolor="#F7F5F4" bgcolor="#FFFFFF">
						<tr>
							<td width="229" align="center" class="mensajes"><%=mensajeDis%></td>
							<td width="29" background="<%=BASEURL%>/images/cuadronaranja.JPG">&nbsp;</td>
							<td width="58">&nbsp;</td>
						</tr>
					</table>
				</td>
			</tr>
		</table>
		<br>
	    <%}%>
            <table width="1000" border="2" align="center">
				<tr>
					<td>
						<table width="100%" align="center">
							<tr>
								<td width="50%" class="subtitulo1">&nbsp;Productos de Discrepancia</td>
								<td width="50%" class="barratitulo"><img src="<%=BASEURL%>/images/titulo.gif" width="32" height="20"></td>
							</tr>
						</table>
						<table width="100%" border="1" bordercolor="#999999" bgcolor="#F7F5F4" align="center">
							<tr class="tblTitulo" align="center">
								<td width="8%" align="center">Codigo Producto </td>
								<td width="28%" align="center">Descripci&oacute;n</td>
								<td width="10%" align="center">Valor</td>
								<td width="9%" align="center">Cantidad</td>
								<td width="10%" align="center">Unidad</td>
								<td width="11%" align="center">Tipo Discrepancia </td>
								<td width="12%" align="center">Causa Devoluci&oacute;n </td>
								<td width="14%" align="center">Responsable</td>
							</tr><%	
							if ( Vecdis.size() >0 ){
								for(int i = 0; i<Vecdis.size(); i++){	
									d = (Discrepancia) Vecdis.elementAt(i);%>
									<tr class="<%=(i % 2 == 0 )?"filagris":"filaazul"%>" >
          									<td width="8%" align="center" class="bordereporte"><%=d.getCod_producto()%></td>
										<td width="28%" align="center" class="bordereporte"><%=d.getDescripcion()%></td>
										<td width="10%" align="center" class="bordereporte"><%=Util.PuntoDeMil(String.valueOf ( Math.round ( d.getValor() ) ) )%></td>
										<td width="9%" align="center" class="bordereporte"><%=Util.PuntoDeMil(String.valueOf ( Math.round ( d.getCantidad() ) ) )%></td>
										<td width="10%" align="center" class="bordereporte">
                                                                                    <%  model.unidadService.buscarUnidad( d.getUnidad() ); 
                                                                                    Unidad unid = model.unidadService.getUnidad();%> 
                                                                                    <%=unid!=null?unid.getDescripcion():""%></td>
										<td width="11%" align="center" class="bordereporte">
											<%model.codigo_discrepanciaService.serchCodigo_discrepancia(d.getCod_discrepancia());
											Codigo_discrepancia cd = model.codigo_discrepanciaService.getCodigo_discrepancia(); %>
											<%=cd.getDescripcion()%></td>
										<td width="12%" align="center" class="bordereporte">
											<%Vector Vecc2 = model.causadiscreService.listarCausas();
											for(int j = 0; j<Vecc2.size(); j++){	
												CausaDiscrepancia cd2 = (CausaDiscrepancia) Vecc2.elementAt(j);	
												if(d.getCausa_dev().equals(cd2.getCodigo())){%>
													<%=cd2.getDescripcion()%><%
												}
											}%></td>
										<td width="14%" align="center" class="bordereporte"><% if (d.getResponsable().equals("CL")){%>CLIENTE<%}else{%>CONDUCTOR<%}%></td>
									</tr>
								<%}
							}%>
							<tr class="pie" align="center">
								<td td height="20" colspan="16" nowrap align="center">
								</td>
							</tr>
						</table>
					</td>
				</tr>						
			</table>
			<br>					  
		<%}
	}
if(mensaje!=null){%>
<%--Mensaje de informacion--%>
<table border="2" align="center">
	<tr>
		<td>
			<table width="100%"  border="1" align="center"  bordercolor="#F7F5F4" bgcolor="#FFFFFF">
				<tr>
					<td width="229" align="center" class="mensajes"><%=mensaje%></td>
					<td width="29" background="<%=BASEURL%>/images/cuadronaranja.JPG">&nbsp;</td>
					<td width="58">&nbsp;</td>
				</tr>
			</table>
		</td>
	</tr>
</table>
<br>
<%}%>
<%--tabla para el boton de salir--%>
<table width="1000" border="0" align="center">
	<tr>
		<td>
			<img src="<%=BASEURL%>/images/botones/regresar.gif" style="cursor:hand" title="Volver" name="buscar"  onClick="window.location='<%=CONTROLLER%>?estado=Discrepancia&accion=Planilla_buscar&numpla=<%=numpla%>&tipo=D'" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);">
		</td>
	</tr>
</table>
</div>
</body>
</html>
<script>
function validar_campos(){
	if(forma.c_nota_debito.value==""&&forma.c_nota_credito.value!=""){
		alert("Debe llenar el campo de la nota debito!");
	}
	else{
		forma.submit();
	}
}
</script>
