<!--
- Autor : Ing. Jose de la rosa
- Date  : 9 de Nobiembre de 2005
- Copyrigth Notice : Fintravalores S.A. S.A
-->
<%--
-@(#)
--Descripcion : Pagina JSP, que modifica los productos de las discrepancias
--%>

<%-- Declaracion de librerias--%>
<%@page contentType="text/html"%>
<%@page session="true"%>
<%@page import="java.util.*"%>
<%@page import="com.tsp.operation.model.*"%>
<%@page import="com.tsp.operation.model.beans.*"%>
<%@page import="com.tsp.util.*"%>
<%@include file="/WEB-INF/InitModel.jsp"%>
<%@ taglib uri="http://jakarta.apache.org/taglibs/input-0.90" prefix="input" %>
<html>
<head>
<title>Producto Modificar</title>
<link href="<%= BASEURL %>/css/estilostsp.css" rel="stylesheet" type="text/css">
<script type='text/javascript' src="<%= BASEURL %>/js/boton.js"></script>
<script type='text/javascript' src="<%= BASEURL %>/js/validar.js"></script>
</head>
<%	String mensaje = (String) request.getAttribute("msg");
	String numpla = "";
	String numrem = "";
	String tipo_docu = "";
	String documen = "";
	String tipo_docu_rel = "";
	String documen_rel = "";	
	double val = 0;
	String nom_doc = (request.getParameter("nom_doc")!=null)?request.getParameter("nom_doc"):"";
	String client = (request.getParameter("client")!=null)?request.getParameter("client"):"";
	String x = (request.getParameter("x")!=null)?request.getParameter("x"):"";
	Discrepancia d = model.discrepanciaService.getDiscrepancia();
	numpla = d.getNro_planilla();
	numrem = d.getNro_remesa();
	tipo_docu = d.getTipo_doc();
	documen = d.getDocumento();
	tipo_docu_rel = d.getTipo_doc_rel();
	documen_rel = d.getDocumento_rel();	
	val = d.getValor();
%>
<%-- Inicio Body --%>
<body <%if(request.getParameter("reload")!=null){%>onLoad="parent.opener.location.replace('<%=CONTROLLER%>?estado=Discrepancia&accion=Serch&c_numrem=<%=numrem%>&sw=true&exis=true&campos=true&c_numpla=<%=numpla%>&c_tipo_doc=<%=tipo_docu%>&c_documento=<%=documen%>&c_tipo_doc_rel=<%=tipo_docu_rel%>&c_documento_rel=<%=documen_rel%>&paso=false&nom_doc=<%=nom_doc%>&client=<%=client%>&modi=ok');"<%}%>>
<%-- Formulario de modificacion de discrepancia --%>
<div id="capaSuperior" style="position:absolute; width:100%; height:100px; z-index:0; left: 0px; top: 0px;">
 <jsp:include page="/WEB-INF/toptsp.jsp?encabezado=Modificar Discrepancia"/>
</div>

 <div id="capaCentral" style="position:absolute; width:100%; height:85%; z-index:0; left: 0px; top: 100px; overflow: scroll;"> 
 
<FORM name='forma3' method='POST'  action="<%=CONTROLLER%>?estado=Discrepancia_producto&accion=Update">
	<table width="700"  border="2" align="center">          
		<tr>
			<td>
				<table width="100%" class="tablaInferior">
					<tr class="fila">
						<td colspan="2" align="left" class="subtitulo1">&nbsp;Producto</td>
						<td colspan="2" align="left" class="barratitulo"><img src="<%=BASEURL%>/images/titulo.gif" width="32" height="20"></td>
					</tr>
					<tr class="fila">
						<td align="left" >Codigo Producto</td>
						<td valign="middle"><%=d.getCod_producto()%>
						<input name="c_cod_producto" type="hidden" id="c_cod_producto" value="<%=d.getCod_producto()%>"></td>
						<td valign="middle">Descripci&oacute;n</td>
						<td valign="middle"><%=d.getDescripcion()%><input type="hidden" value="<%=d.getDescripcion()%>" name="c_descripcion" id="c_descripcion"></td>
					</tr>
					<tr class="fila" id="cantidad">
						<td width="118" align="left" >Cantidad </td>
						<td width="164" valign="middle">
						<input name="c_cantidad" type="text" class="textbox" id="c_cantidad" onKeyPress="soloDigitos(event,'decOK')" size="13" value="<%=d.getCantidad()%>" maxlength="13">            </td>
						<td width="72" valign="middle">Unidad</td>
						<td width="214" valign="middle">
						<%  model.unidadService.buscarUnidad( d.getUnidad() ); 
                                                Unidad unid = model.unidadService.getUnidad();%> 
                                                <%=unid!=null?unid.getDescripcion():""%>
						<input name="c_unidad"  type="hidden" id="c_unidad" value="<%=d.getUnidad()%>"></td>
					</tr>
					<tr class="fila" id="cantidad">
						<td width="118" align="left" >Tipo Discrepancia</td>
						<td width="164" valign="middle">
						<%Vector vecd = model.codigo_discrepanciaService.listarCodigo_discrepancias(); 
						for(int i = 0; i<vecd.size(); i++){	
							Codigo_discrepancia c = (Codigo_discrepancia) vecd.elementAt(i);	%>
							<% if(d.getCod_discrepancia().equals(c.getCodigo())){%> 
								<%=c.getDescripcion()%> 
							<%}%>
						<%}%>
						<input name="c_cod_discrepancia" value="<%=d.getCod_discrepancia()%>" type="hidden"></td>
						<td width="72" valign="middle">Responsable</td>
						<td width="214" valign="middle">
						<select name="c_responsable" class="textbox" id="c_responsable" onChange="cargarResponsable('<%=CONTROLLER%>');">
							<%model.responsableService.listResponsable();
							Vector Vecre = model.responsableService.getResponsables();
							for(int h = 0; h<Vecre.size(); h++){	
								Responsable resa = (Responsable) Vecre.elementAt(h);	%>
								<option value="<%=resa.getCodigo()%>"><%=resa.getDescripcion()%></option>
							<%}%>
						</select> </td>
					</tr>
					<tr class="fila">
						<td width="118" align="left" >Causa Devoluci&oacute;n</td>
						<td valign="middle">
							<select name="c_causa" class="textbox" id="c_causa" onChange="cargarDiscrepancia('<%=CONTROLLER%>');">
								<%Vector Vecc = model.causadiscreService.listarCausas();
								for(int i = 0; i<Vecc.size(); i++){	
									CausaDiscrepancia cd = (CausaDiscrepancia) Vecc.elementAt(i);	%>
									<option value="<%=cd.getCodigo()%>" <% if(d.getCausa_dev().equals(cd.getCodigo())){%> selected <%}%> ><%=cd.getDescripcion()%></option>
								<%}%>
							</select></td>
						<td valign="middle">Valor</td>
						<td valign="middle"><input name="c_valor" type="text" class="textbox" id="c_valor" onKeyPress="soloDigitos(event,'decOK')" value="<%=val%>" size="13" maxlength="13"></td>
					</tr>
					<tr class="pie">
						<td colspan="4" align="center">
						<input name="c_documento" type="hidden" value="<%=d.getDocumento()%>">
						<input name="c_fecha_creacion" value="<%=d.getFecha_creacion()%>" type="hidden">
						<input name="nom_doc" value="<%=nom_doc%>" type="hidden">
						<input name="c_cod_dicrepancia" type="hidden" value="<%=d.getCod_discrepancia()%>">
						<input name="c_num_discre" value="<%=d.getNro_discrepancia()%>" type="hidden">
						<input name="c_numpla" value="<%=d.getNro_planilla()%>" type="hidden">
						<input name="c_numrem" value="<%=d.getNro_remesa()%>" type="hidden">
						<input name="client" value="<%=client%>" type="hidden">
						<input name="x" value="<%=x%>" type="hidden">
						<input name="c_tipo_doc" type="hidden" value="<%=d.getTipo_doc()%>">
						<input name="c_tipo_doc_rel" type="hidden" value="<%=d.getTipo_doc_rel()%>">
						<input name="c_documento_rel" type="hidden" value="<%=d.getDocumento_rel()%>">
						</td>
					</tr>
				</table>
			</td>
		</tr>
	</table>
	<p>
		<div align="center"><img src="<%=BASEURL%>/images/botones/modificar.gif" style="cursor:hand" title="Agregar un codigo de discrepancia" name="modificar"  onclick="return validarDiscrepanciaProducto();" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);">&nbsp;
		<img src="<%=BASEURL%>/images/botones/anular.gif" style="cursor:hand" title="Anular un producto de discrepancia" name="anular"  onClick="window.location='<%=CONTROLLER%>?estado=Discrepancia_producto&accion=Anular&nom_doc=<%=nom_doc%>&client=<%=client%>&x=<%=x%>'" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);">&nbsp;
		<img src="<%=BASEURL%>/images/botones/salir.gif" style="cursor:hand" name="salir" title="Salir al Menu Principal" onMouseOver="botonOver(this);" onClick="parent.close();" onMouseOut="botonOut(this);" ></div>
	</p>
	<% if(mensaje!=null){%>
		<%--Mensaje de informacion--%>
		<table border="2" align="center">
			<tr>
				<td>
					<table width="100%"  border="1" align="center"  bordercolor="#F7F5F4" bgcolor="#FFFFFF">
						<tr>
							<td width="229" align="center" class="mensajes"><%=mensaje%></td>
							<td width="29" background="<%=BASEURL%>/images/cuadronaranja.JPG">&nbsp;</td>
							<td width="58">&nbsp;</td>
						</tr>
					</table>
				</td>
			</tr>
		</table>
	<%}%>
</form>
</div>
</body>
</html>
<%--Funciones Javascript --%>
<script>
function cargarDiscrepancia ( BASEURL ){
	if(forma3.c_causa.value=='OT'){
		window.open(BASEURL+"?estado=Menu&accion=Cargar&carpeta=/jsp/masivo/tblotros_conceptos&titulo=Definicion Otros Conceptos&pagina=otrconceptDefinir.jsp?tabla=causa_discrepancia-_-campo=Causa Discrepancia",'Definicion_Otros_Conceptos','status=yes,scrollbars=yes,width=720,height=400,resizable=yes');
	}	
}
function cargarResponsable ( BASEURL ){
	if(forma3.c_responsable.value=='OT'){
		window.open(BASEURL+"?estado=Menu&accion=Cargar&carpeta=/jsp/masivo/tblotros_conceptos&titulo=Definicion Otros Conceptos&pagina=otrconceptDefinir.jsp?tabla=responsable-_-campo=Responsables",'Definicion_Otros_Conceptos','status=yes,scrollbars=yes,width=720,height=400,resizable=yes');
	}	
}	
function AdjuntarImagenes(BASEURL,acti,tipo) {
	var pag="";	
	pag = "/imagen/Manejo.jsp?documento="+forma.c_numpla.value+"-_-actividad="+acti+"-_-tipoDocumento="+tipo;
	window.open(BASEURL+"/Marcostsp.jsp?encabezado=Agregar Imagen&dir="+pag,'Trafico','width=680,height=700,scrollbars=yes,resizable=yes,top=10,left=65');
}
</script>
<%--Fin de funciones Javascript--%>