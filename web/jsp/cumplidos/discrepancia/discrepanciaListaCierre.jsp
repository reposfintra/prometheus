<!--
- Autor : Ing. Jose de la rosa
- Date  : 9 de Nobiembre de 2005
- Copyrigth Notice : Fintravalores S.A. S.A
-->
<%--
-@(#)
--Descripcion : Pagina JSP, que muestra el listado de las discrepancias que se pueden cerrar
--%>

<%-- Declaracion de librerias--%>
<%@page contentType="text/html"%>
<%@page session="true"%>
<%@page errorPage="../error/ErrorPage.jsp"%>
<%@page import="java.util.*"%>
<%@page import="com.tsp.operation.model.*"%>
<%@page import="com.tsp.util.*"%>
<%@page import="com.tsp.operation.model.beans.*, java.util.*"%>
<%@include file="/WEB-INF/InitModel.jsp"%>
<%@taglib uri="http://jsptags.com/tags/navigation/pager" prefix="pg" %>
 
<html>
<head>
<title>Listado de Discrepancia</title>
<link href="<%= BASEURL %>/css/estilostsp.css" rel="stylesheet" type="text/css">
<script type='text/javascript' src="<%= BASEURL %>/js/boton.js"></script>
<script type='text/javascript' src="<%= BASEURL %>/js/reporte.js"></script>
</head>
<%-- Inicio Body --%>
<body>
	<div id="capaSuperior" style="position:absolute; width:100%; height:100px; z-index:0; left: 0px; top: 0px;">
	<jsp:include page="/WEB-INF/toptsp.jsp?encabezado=Listar Discrepancia"/>
	</div>
	
	<div id="capaCentral" style="position:absolute; width:100%; height:85%; z-index:0; left: 0px; top: 100px; overflow: scroll;"> 	
<%  String style = "simple";
    String position =  "bottom";
    String index =  "center";
    int maxPageItems = 10;
    int maxIndexPages = 10;
	Discrepancia d;
	Vector vec = model.discrepanciaService.getDiscrepancias();
	if ( vec.size() >0 ){  %>
	<%-- Tabla de datos de discrepancia --%>
	<table width="1050" border="2" align="center">
		<tr>
			<td>
				<table width="100%" align="center">
					<tr>
						<td width="373" class="subtitulo1">&nbsp;Datos Discrepancia </td>
						<td width="427" class="barratitulo"><img src="<%=BASEURL%>/images/titulo.gif" width="32" height="20"></td>
					</tr>
				</table>
				<table width="100%" border="1" bordercolor="#999999" bgcolor="#F7F5F4" align="center">
					<tr class="tblTitulo" align="center">
						
						<td width="6%" align="center">Numero Planilla</td>
						<td width="6%"  align="center">Numero Remesa </td>
						<td width="8%" align="center">Tipo Documento </td>
						<td width="8%" align="center">Documento</td>
						<td width="15%" align="center">Nombre Cliente </td>
						<td width="17%" align="center">Nombre Conductor </td>
						<td width="6%" align="center">Placa</td>
						<td width="9%" align="center">Fecha Discrepancia </td>
						<td width="7%" align="center">Estado </td>
						<td width="9%" align="center">Fecha Cierre </td>
						<td width="9%" align="center">Usuario Cierre </td>
					</tr>
					<pg:pager
					items="<%=vec.size()%>"
					index="<%= index %>"
					maxPageItems="<%= maxPageItems %>"
					maxIndexPages="<%= maxIndexPages %>"
					isOffset="<%= true %>"
					export="offset,currentPageNumber=pageNumber"
					scope="request"><%
						for (int i = offset.intValue(), l = Math.min(i + maxPageItems, vec.size()); i < l; i++){
							d = (Discrepancia) vec.elementAt(i);%>
							<pg:item>
								<tr class="<%=(i % 2 == 0 )?"filagris":"filaazul"%>"  onMouseOver='cambiarColorMouse(this)'   style="cursor:hand"
								onClick="window.location='<%=CONTROLLER%>?estado=Discrepancia_listar&accion=Cierre&c_num_discre=<%=d.getNro_discrepancia()%>&c_tipo_doc=<%=d.getTipo_doc()%>&c_documento=<%=d.getDocumento()%>&c_tipo_doc_rel=<%=d.getTipo_doc_rel()%>&c_documento_rel=<%=d.getDocumento_rel()%>&c_numpla=<%=d.getNro_planilla()%>&c_numrem=<%=d.getNro_remesa()%>&msg='">

									<td width="6%" align="center" class="bordereporte"><%=d.getNro_planilla()%></td>
									<td width="6%" align="center" class="bordereporte"><%=d.getNro_remesa()%></td>
									<td width="8%" align="center" class="bordereporte"><%=d.getNom_documento()%></td>
									<td width="8%" align="center" class="bordereporte"><%=d.getDocumento()%></td>
									<td width="15%" align="center" class="bordereporte"><%=d.getCliente()%> </td>
									<td width="17%" align="center" class="bordereporte"><%=d.getNom_conductor()%></td>
									<td width="6%" align="center" class="bordereporte"><%=d.getPlaca()%></td>
									<td width="9%" align="center" class="bordereporte"><%=d.getFecha_creacion().substring(0,10)%></td>
									<td width="7%" align="center" class="bordereporte"><%=d.getUsuario_cierre().equals("")?"ABIERTA":"CERRADA"%></td>
								<td width="9%" align="center" class="bordereporte"><%=d.getUsuario_cierre().equals("")?"&nbsp;":d.getFecha_cierre().substring(0,10)%></td>
								<td width="9%" align="center" class="bordereporte"><%=d.getUsuario_cierre().equals("")?"&nbsp;":d.getUsuario_cierre()%></td>
								</tr>
							</pg:item>
						<%}%>
						<tr class="pie">
							<td td height="20" colspan="11" nowrap align="center">
								<pg:index>
									<jsp:include page="/WEB-INF/jsp/googlesinimg.jsp" flush="true"/>      
								</pg:index> 
							</td>
						</tr>
					</pg:pager>
		  		</table>
			</td>
		</tr>
	</table>
	<%}
 	else { %>
		<br>
		<%--Mensaje de informacion--%>
		<table border="2" align="center">
			<tr>
				<td>
					<table width="410" height="73" border="1" align="center"  bordercolor="#F7F5F4" bgcolor="#FFFFFF">
						<tr>
							<td width="282" align="center" class="mensajes">La planilla no tiene discrepancias asociadas</td>
							<td width="28" background="<%=BASEURL%>/images/cuadronaranja.JPG">&nbsp;</td>
							<td width="78">&nbsp;</td>
						</tr>
					</table>
				</td>
			</tr>
		</table>
  	<%}%>
	<br>
	<%--tabla para el boton de volver--%>
	<table width="1050" border="0" align="center" cellpadding="0" cellspacing="0">
		<tr style="cursor:hand">
			<td align="left"><img src="<%=BASEURL%>/images/botones/regresar.gif" style="cursor:hand" title="Volver" name="buscar"  onClick="window.location='<%=CONTROLLER%>?estado=Menu&accion=Cargar&carpeta=/jsp/cumplidos/discrepancia&titulo=Buscar Discrepancias&marco=no&pagina=buscarDiscrepanciaPlanilla.jsp?msg='" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);"></td>
		</tr>
	</table>
	</div>
</body>
</html>
