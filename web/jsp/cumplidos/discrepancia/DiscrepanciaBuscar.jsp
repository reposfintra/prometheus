<!--
- Autor : Ing. Jose de la rosa
- Date  : 9 de Nobiembre de 2005
- Copyrigth Notice : Fintravalores S.A. S.A
-->
<%--
-@(#)
--Descripcion : Pagina JSP, que busca las discrepancias por fecha de cierre o sin fecha de cierre
--%>

<%-- Declaracion de librerias--%>
<%@page contentType="text/html"%>
<%@page session="true"%>
<%@page errorPage="../error/ErrorPage.jsp"%>
<%@page import="java.util.*"%>
<%@page import="com.tsp.operation.model.*"%>
<%@page import="com.tsp.util.*"%>
<%@page import="com.tsp.operation.model.beans.*, java.util.*"%>
<%@include file="/WEB-INF/InitModel.jsp"%>

<html>
<head>
<title>Buscar Discrepancia</title>
<link href="<%= BASEURL %>/css/estilostsp.css" rel="stylesheet" type="text/css">
<script type='text/javascript' src="<%= BASEURL %>/js/boton.js"></script>
<script src='<%=BASEURL%>/js/date-picker.js'></script>
<script type='text/javascript' src="<%= BASEURL %>/js/validar.js"></script>
</head>
<%-- Inicio Body --%>
<body>
<div id="capaSuperior" style="position:absolute; width:100%; height:100px; z-index:0; left: 0px; top: 0px;">
<jsp:include page="/WEB-INF/toptsp.jsp?encabezado=Buscar Discrepancia"/>
</div>

<div id="capaCentral" style="position:absolute; width:100%; height:85%; z-index:0; left: 0px; top: 100px; overflow: scroll;"> 
<%-- Inicio Formulario --%>
<form name="forma" method="post" action="<%=CONTROLLER%>?estado=Discrepancia_listar&accion=Serch&listar=True">
	<%-- Inicio Tabla Principal --%>
        <table width="500" border="2" align="center">
		<tr>
			<td>
                                <%-- Inicio Tabla Secundaria --%>
				<table width="100%" align="center"  class="tablaInferior">
					<tr class="fila">
						<td colspan="2" class="subtitulo1" width="50%">&nbsp;Informaci&oacute;n</td>
						<td colspan="2" class="barratitulo" width="50%"><img src="<%=BASEURL%>/images/titulo.gif" width="32" height="20"></td>
					</tr>
					<tr class="fila">
						<td colspan="4" align="center" width="100%">Tipo Registro &nbsp;&nbsp;&nbsp;
							<select name="c_tipo" class="textbox" onChange="CamposDiscrep1();" id="c_tipo">
								<option value="C">Con Fecha de Cierre</option>
								<option value="S">Sin Fecha de Cierre</option>
								<option value="P">Por Planilla</option>
							</select></td>
					</tr>	
					<tr class="fila" id="fechas">
						<td width="20%" align="left" > Fecha Inicio </td>
						<td width="30%" valign="middle" >
							<input name="c_fecha_inicio" type="text" readonly class="textbox" id="c_fecha_inicio" size="12">
							<a href="javascript:void(0)" onclick="if(self.gfPop)gfPop.fPopCalendar(document.forma.c_fecha_inicio);return false;" HIDEFOCUS><img src="<%=BASEURL%>\js\Calendario\cal.gif" width="16" height="16" border="0" alt="De click aqui para escoger la fecha"></a></td>
						<td width="20%" valign="middle">Fecha Fin </td>
						<td width="30%" valign="middle">
							<input name="c_fecha_fin" type="text" class="textbox" readonly id="c_fecha_fin" size="12">
							<a href="javascript:void(0)" onclick="if(self.gfPop)gfPop.fPopCalendar(document.forma.c_fecha_fin);return false;" HIDEFOCUS><img src="<%=BASEURL%>\js\Calendario\cal.gif" width="16" height="16" border="0" alt="De click aqui para escoger la fecha"></a></td>
					</tr> 
					<tr class="fila" id="planilla" style="display:none">
						<td colspan="2" align="right" width="50%">Numero De Planilla&nbsp;&nbsp;</td>
					    <td colspan="2" width="50%">&nbsp;&nbsp;<input name="numpla" type="text" class="textbox" maxlength="10" size="10" id="numpla"></td>
				    </tr>		
				</table>
			</td>
		</tr>
	</table>
	<p>
		<div align="center"><img src="<%=BASEURL%>/images/botones/buscar.gif" style="cursor:hand" title="Buscar Discrepancias" name="buscar"  onClick="validarSalida();" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);">&nbsp;
		<img src="<%=BASEURL%>/images/botones/salir.gif"  name="salir" style="cursor:hand" title="Salir al Menu Principal" onMouseOver="botonOver(this);" onClick="parent.close();" onMouseOut="botonOut(this);" ></div>
	</p>	  
</form>
</div>
<iframe width=188 height=166 name="gToday:normal:agenda.js:gfPop:plugins.js" id="gToday:normal:agenda.js:gfPop:plugins.js" src="<%=BASEURL%>/js/Calendario/ipopeng.htm" scrolling="no" frameborder="0" style="visibility:visible; z-index:999; position:absolute; left:-500px; top:0px;"> </iframe>
</body>
</html>
<script>
function CamposDiscrep1(){
	if (forma.c_tipo.value == "S"){
			fechas.style.display="none";
			planilla.style.display="none";
	}
	if (forma.c_tipo.value == "C" ){
			fechas.style.display="block";
			planilla.style.display="none";
	}
	if (forma.c_tipo.value == "P"){
			fechas.style.display="none";
			planilla.style.display="block";
	}	
}
function validarSalida(){
	if( forma.c_tipo.value == "P" ){
		if(forma.numpla.value == "" ){
			alert("Debe llenar el campo de la planilla");
			forma.numpla.focus();
			return false;
		}
		else{
			forma.submit();
		}
	}
	else if (forma.c_tipo.value == "C" ){
		if( forma.c_fecha_inicio.value == "" || forma.c_fecha_fin.value == "" ){
			alert("Debe llenar los campos de las fechas");
			return (false);
		}
		if( forma.c_fecha_inicio.value > forma.c_fecha_fin.value ) {     
			alert('La FECHA INICIAL debe ser igual o menor que la FECHA FINAL');
			return (false);
		}
		else{
			forma.submit();
		}
	}
	else{
		forma.submit();
	}
}
</script>