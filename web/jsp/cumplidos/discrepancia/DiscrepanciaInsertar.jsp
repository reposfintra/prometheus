<!--
- Autor : Ing. Jose de la rosa
- Date  : 5 de Octubre de 2005
- Copyrigth Notice : Fintravalores S.A. S.A
-->
<%--
-@(#)
--Descripcion : Pagina JSP, que maneja el ingreso de las discrepancias y
                a los productos de discrepancia a las tabla discrepancia y discrepancia_producto
--%>

<%-- Declaracion de librerias--%>
<%@page contentType="text/html"%>
<%@page session="true"%>
<%@page import="java.util.*"%>
<%@page import="com.tsp.operation.model.*"%>
<%@page import="com.tsp.util.*"%>
<%@page import="com.tsp.operation.model.beans.*, java.util.*"%>
<%@include file="/WEB-INF/InitModel.jsp"%>
<%@ taglib uri="http://jakarta.apache.org/taglibs/input-0.90" prefix="input"%>
<html>
<head>
<title>Discrepancia</title>
<link href="<%= BASEURL %>/css/estilostsp.css" rel="stylesheet" type="text/css">
<script type='text/javascript' src="<%= BASEURL %>/js/boton.js"></script>
</head>
<script src='<%=BASEURL%>/js/date-picker.js'></script>
<script type='text/javascript' src="<%= BASEURL %>/js/validar.js"></script>
<script type='text/javascript' src="<%= BASEURL %>/js/reporte.js"></script>
<% 	String path = application.getRealPath(model.menuService.getRealUrl(request.getRequestURI()));
	String datos[] = model.menuService.getContenidoMenu(BASEURL,request.getRequestURI(),path);
	String sw = request.getParameter("sw");
	String exis = (sw.equals("true"))?request.getParameter("exis"):""; 
	String onload = "";
	String f_hoy          = Utility.getHoy("-");
	//condicion para colocarle el foco al campo de producto en la busqueda de un producto de discrepancia
	if ( exis.equals("true") ){
		onload = "onload = 'document.forma2.c_cod_producto.focus();'";
	}	
	String mensaje = (String) request.getAttribute("msg");
	String res = "";
	String numpla = request.getParameter("c_numpla");
	InfoPlanilla info = model.planillaService.getInfoPlanilla();
	info.setPropietario(model.planillaService.getNomPropie());
	String conductor = info.getCedulaConductor() + " " + info.getNombreConductor();
	String ruta = info.getOrigen() + " - " + info.getDestino();
	String fecha = info.getFecha();
	String placa = info.getVehiculo();
	String propietario = info.getPropietario();
	String tipo_docu = "";
	String documen = "";
	String tipo_docu_rel = "";
	String documen_rel = "";	
	String planilla = "";
	String remesa = "";
	String fecha_dev = "";
	String numero_rech = "";
	String contac = "";
	String report = "";
	String ubica = "";
	String retencion = "";
	String num_discre = "";
	String paso = "";
	String cod = "";
	String desc = "";
	String observa = "";
	String fecha_cierre = "";
	String uni = "";
	String client = (request.getParameter("client")!=null)?request.getParameter("client"):"";
	String nom_doc = (request.getParameter("nom_doc")!=null)?request.getParameter("nom_doc"):"";
	String nom_des = (request.getParameter("c_nomdes")!=null)?request.getParameter("c_nomdes"):"";
	int num_d = 0;
%>
<%-- Inicio Body --%>
<body <%=onload%>>
<div id="capaSuperior" style="position:absolute; width:100%; height:100px; z-index:0; left: 0px; top: 0px;">
 <jsp:include page="/WEB-INF/toptsp.jsp?encabezado=Ingresar Discrepancia"/>
</div>

 <div id="capaCentral" style="position:absolute; width:100%; height:85%; z-index:0; left: 0px; top: 100px; overflow: scroll;"> 
 
<%-- Tabla del detalle de la planilla --%>
<table width="1000" border="2" align="center">          
	<tr>
		<td>
			<table width="100%" class="tablaInferior">
				<tr>
					<td width="50%" colspan="2" class="subtitulo1">&nbsp;Detalle de la Planilla</td>
					<td width="50%" colspan="2" class="barratitulo"><img src="<%=BASEURL%>/images/titulo.gif" width="32" height="20" align="left"><%=datos[0]%></td>
				</tr>
				<tr class="letra">
					<td width="14%" class="fila">No.:</td>
					<td width="36%"><%=numpla %></td>
					<td width="14%" class="fila">Fecha:</td>
					<td width="36%"><%=fecha %></td>
				</tr>
				<tr class="letra">
					<td class="fila">Conductor:</td>
					<td ><%=conductor %></td>
					<td class="fila">Placa:</td>
					<td ><%=placa %></td>
				</tr>
				<tr class="letra">
					<td class="fila">Ruta:</td>
					<td ><%=ruta %></td>
					<td class="fila">Propietario:</td>
					<td ><%= propietario %></td>
				</tr>
			</table>
		</td>
	</tr>
</table>
<%
Vector vec = model.planillaService.ReporteVector();
Planilla p;
if ( vec.size() >0 ){ %>
<%-- Tabla de la remesa --%>
<table width="1000" border="2" align="center">
	<tr>
		<td>
			<table width="100%" align="center">
				<tr>
					<td width="50%" class="subtitulo1">&nbsp;Remesa</td>
					<td width="50%" class="barratitulo"><img src="<%=BASEURL%>/images/titulo.gif" width="32" height="20"></td>
				</tr>
			</table>
			<table width="100%" border="1" bordercolor="#999999" bgcolor="#F7F5F4" align="center">
				<tr class="tblTitulo" align="center">
					<td width="16%" align="center">Remesa</td>
					<td width="27%" align="center">Cliente</td>
					<td width="19%" align="center">Fecha</td>
					<td width="19%" align="center">Origen Remesa </td>
					<td width="19%" align="center">Destino Remesa </td>
				</tr>
				<% for(int i = 0; i<vec.size(); i++){	
					p = (Planilla) vec.elementAt(i);%>
					<tr class="<%=(i % 2 == 0 )?"filagris":"filaazul"%>" onMouseOver='cambiarColorMouse(this)'   style="cursor:hand" onClick="window.location='<%=CONTROLLER%>?estado=Discrepancia&accion=Serch&c_numrem=<%=p.getNumrem()%>&sw=false&campos=false&c_numpla=<%=numpla%>&client=<%=p.getClientes()%>'">
						<td width="16%" class="bordereporte" align="center"><%=p.getNumrem()%></td>
						<td width="27%" class="bordereporte" align="center"><%=p.getNitpro()%></td>
						<td width="19%" class="bordereporte" align="center"><%=p.getFecdsp()%></td>
						<td width="19%" class="bordereporte" align="center"><%=p.getNomori()%></td>
						<td width="19%" class="bordereporte" align="center"><%=p.getNomdest()%></td>
					</tr>
				<%}%>
				<tr  class="pie">
					<td td height="20" colspan="12" nowrap align="center">
					</td>
				</tr>
			</table>
		</td>
	</tr>
</table>
<%} if(sw.equals("true")){ 
String numrem = request.getParameter("c_numrem");
String campos = request.getParameter("campos");
Vector Vecrem = model.remesaService.getRemes();
Remesa r;
if ( Vecrem.size() >0 ){ %>
<%-- Tabla de los documentos de la remesa --%>
<table width="1000" border="2" align="center">
	<tr>
		<td>
			<table width="100%">
				<tr>
					<td width="50%" class="subtitulo1">&nbsp;Documentos Remesa </td>
					<td width="50%" class="barratitulo"><img src="<%=BASEURL%>/images/titulo.gif" width="32" height="20"></td>
				</tr>
			</table>
			<table width="100%" border="1" bordercolor="#999999" bgcolor="#F7F5F4" align="center">
				<tr class="tblTitulo" align="center">
					<td width="20%" align="center">Tipo Documento </td>
					<td width="17%"  align="center">Documento</td>
					<td width="20%" align="center">Tipo Doc Relacionado </td>
					<td width="17%" align="center">Documento Relacionado</td>
					<td width="26%" align="center">Nombre Destinatario </td>
				</tr>
				<% for(int i = 0; i<Vecrem.size(); i++){	
					r = (Remesa) Vecrem.elementAt(i);%>
					<tr class="<%=(i % 2 == 0 )?"filagris":"filaazul"%>" onMouseOver='cambiarColorMouse(this)'   style="cursor:hand" onClick="window.location='<%=CONTROLLER%>?estado=Discrepancia&accion=Serch&c_numrem=<%=numrem%>&sw=true&c_numpla=<%=numpla%>&c_tipo_doc=<%=r.getTipo_documento()%>&c_documento=<%=r.getDocumento()%>&nom_doc=<%=r.getNombre_tipo_doc()%>&c_tipo_doc_rel=<%=r.getTipo_doc_rel()%>&c_documento_rel=<%=r.getDocumento_rel()%>&c_nomdes=<%=r.getNom_destinatario()%>&client=<%=client%>'">
					<td width="20%" class="bordereporte" align="center"><%=r.getNombre_tipo_doc()%></td>
					<td width="17%" class="bordereporte" align="center"><%=r.getDocumento()%></td>
					<td width="20%" class="bordereporte" align="center"><%=r.getNombre_tipo_doc_rel()%></td>
					<td width="17%" class="bordereporte" align="center"><%=r.getDocumento_rel()%></td>
					<td width="26%" class="bordereporte" align="center"><%=r.getNom_destinatario()%></td>
					</tr>
				<%}%>
				<tr  class="pie">
					<td td height="20" colspan="12" nowrap align="center"></td>
				</tr>
			</table>
		</td>
	</tr>
</table>
<%} if( Vecrem.size() <=0 ){%>
<%--Mensaje de informacion--%>
<br>
<table border="2" align="center">
	<tr>
		<td>
			<table width="100%"  border="1" align="center"  bordercolor="#F7F5F4" bgcolor="#FFFFFF">
				<tr>
					<td width="229" align="center" class="mensajes">No hay documentos disponibles</td>
					<td width="29" background="<%=BASEURL%>/images/cuadronaranja.JPG">&nbsp;</td>
					<td width="58">&nbsp;</td>
				</tr>
			</table>
		</td>
	</tr>
</table>
<%}if(campos.equals("true")){
	if(exis.equals("true")){ 
		Discrepancia d = model.discrepanciaService.getCabDiscrepancia ();		
		planilla = d.getNro_planilla();
		remesa = d.getNro_remesa();
		tipo_docu = d.getTipo_doc();
		documen = d.getDocumento();
		tipo_docu_rel = d.getTipo_doc_rel();
		documen_rel = d.getDocumento_rel();
		fecha_dev = d.getFecha_devolucion().substring(0,10);
		numero_rech = String.valueOf(d.getNumero_rechazo());
		contac = d.getContacto();
		report = d.getReportado();
		retencion = d.getRetencion();
		observa = d.getObservacion();
		ubica = d.getUbicacion();
		fecha_cierre = d.getFecha_cierre();
		num_discre = String.valueOf(d.getNro_discrepancia());
		num_d = d.getNro_discrepancia();
	}
	if(exis.equals("false")){
		tipo_docu_rel = request.getParameter("c_tipo_doc_rel");
		documen_rel = request.getParameter("c_documento_rel");	
		tipo_docu = request.getParameter("c_tipo_doc");
		documen = request.getParameter("c_documento");
		planilla = numpla;
		remesa = numrem;
		fecha_dev = "";
		numero_rech = "";
		contac = "";
		fecha_cierre="";
		observa = "";
		report = "";
		fecha_cierre = "";
		ubica = "";
	}%>
	<%-- Tabla de las discrepancias --%>
	<table width="1000" border="2" align="center">          
		<tr>
			<td>
				<table width="100%" class="tablaInferior">
					<%-- Formulario de ingreso y modificacion de discrepancia --%>
					<FORM name='forma' method='POST' <%if(sw.equals("true")){ if(exis.equals("true")){%> action="<%=CONTROLLER%>?estado=Discrepancia&accion=Update" <%}else{ %>action="<%=CONTROLLER%>?estado=Discrepancia&accion=Insert" <% } }%> >
					<tr class="fila">
						<td colspan="2" align="left" class="subtitulo1">&nbsp;Discrepancia</td>
						<td colspan="2" align="left" class="barratitulo"><img src="<%=BASEURL%>/images/titulo.gif" width="32" height="20"></td>
					</tr>
					<tr id="cantidad">
						<td width="23%" class="fila"> Numero Planilla </td>
						<td width="27%" class="letra"> <%=planilla%> <input name="c_numpla" type="hidden" id="c_numpla" value="<%=planilla%>">
						<img src="<%=BASEURL%>/images/botones/iconos/documento.gif" alt="Adjuntar Imagen" width="15" height="15" 
        						onClick="window.open('<%=CONTROLLER%>?estado=Imagen&accion=Control&documento=<%=planilla%>&actividad=013&tipoDocumento=001', '','width=680,height=600,scrollbars=yes,resizable=yes,top=10,left=65,status=yes');" style="cursor:hand">
						</td>
						<td width="25%" class="fila">Numero Remesa						</td>
						<td width="25%" class="letra"><%=remesa%><input name="c_numrem" type="hidden" id="c_numrem" value="<%=remesa%>"></td>
					</tr>
					<tr id="cantidad">
						<td align="left" class="fila"> Tipo Documento </td>
						<td  valign="middle" class="letra"> <%=nom_doc%> <input name="nom_doc" type="hidden" id="nom_doc" value="<%=nom_doc%>">
						<input name="c_tipo_doc" type="hidden"value="<%=tipo_docu%>"></td>
						<td valign="middle" class="fila">Documento						</td>
						<td valign="middle" class="letra"> <%=documen%> <input name="c_documento" type="hidden" id="c_documento" value="<%=documen%>"></td>
					</tr>								
					<tr class="fila">
						<td align="left" >Fecha Devoluci&oacute;n</td>
						<td valign="middle"><input name="c_fecha_devolucion" type="text" readonly class="textbox" id="c_fecha_devolucion" size="12" value="<%=fecha_dev%>">&nbsp;<a href="javascript:void(0)" onclick="if(self.gfPop)gfPop.fPopCalendar(document.forma.c_fecha_devolucion);return false;" HIDEFOCUS><img src="<%=BASEURL%>\js\Calendario\cal.gif" width="16" height="16" border="0" alt="De click aqui para escoger la fecha"></a>&nbsp;<img src="<%=BASEURL%>/images/botones/iconos/obligatorio.gif"></td>
						<td valign="middle">Retenci&oacute;n Pago </td>
						<td valign="middle">
							<select name="c_retencion" class="textbox" id="c_retencion">
								<option value="S" <% if(retencion.equals("S")){%> selected <%}%>>Si</option>
								<option value="N" <% if(retencion.equals("N")){%> selected <%}%>>No</option>
							</select></td>
					</tr>
					<tr class="fila" id="cantidad">
						<td align="left" > Numero Devoluci&oacute;n </td>
						<td  valign="middle"><input name="c_nro_rechazo" type="text" class="textbox" id="c_nro_rechazo" value="<%=numero_rech%>" size="30" maxlength="30"></td>
						<td valign="middle">Contacto del Cliente						  </td>
						<td valign="middle"><input name="c_contacto" type="text" class="textbox" id="c_contacto" value="<%=contac%>" size="40" maxlength="40"></td>
					</tr>
					<tr class="fila">
						<td align="left" > Reportado Por </td>
						<td  valign="middle"><input name="c_reportado" type="text" class="textbox" id="c_reportado" value="<%=report%>" size="30" maxlength="30"></td>
						<td valign="middle">Ubicaci&oacute;n Mercancia </td>
						<td valign="middle"><select name="c_ubicacion" class="textbox"  id="c_ubicacion" >
							<% 
							Vector vecubic = model.ubService.getUbicaciones ();
							for(int i = 0; i<vecubic.size(); i++){	
								Ubicacion ubic = (Ubicacion) vecubic.elementAt(i);%>
									<option value="<%=ubic.getCod_ubicacion()%>" <%if( ubic.getCod_ubicacion().equals(ubica) ){%> selected <%}%>><%=ubic.getDescripcion()%></option>
							<%}%>
						</select><img src="<%=BASEURL%>/images/botones/iconos/obligatorio.gif"></td>
					</tr>
					<tr class="fila">
						<td align="left" > Observaci&oacute;n </td>
						<td colspan="3" valign="middle"><textarea name="c_observacion" cols="134" rows="3" class="textbox" id="c_observacion"><%=observa%></textarea></td>
					</tr>					
					<tr class="fila">
						<td colspan="4" align="left" >&nbsp;</td>
					</tr>	
					<tr class="fila">
						<td colspan="4" align="center" >
						<input name="c_tipo_doc_rel" type="hidden" value="<%=tipo_docu_rel%>">
						<input name="c_documento_rel" type="hidden" value="<%=documen_rel%>">
						<input name="c_nomdes" type="hidden" value="<%=nom_des%>">
						<input name="client" value="<%=client%>" type="hidden">
						<input name="c_contacto" type="hidden" value="<%=contac%>">
						<input name="c_num_discre" id="c_num_discre" value="<%=num_discre%>" type="hidden"><input name="nom_doc" value="<%=nom_doc%>" type="hidden">
						<img <%=( sw.equals("true") && exis.equals("true") )? "src='"+BASEURL+"/images/botones/modificar.gif' title='Modificar una discrepancia'":"src='"+BASEURL+"/images/botones/aceptar.gif' title='Agregar una discrepancia'"%> style="cursor:hand" name="modificar"  onclick="return validarDiscrepancia('<%=f_hoy%>');" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);">
						<% if(sw.equals("true")){ if(exis.equals("true")){%> <img src="<%=BASEURL%>/images/botones/anular.gif" style="cursor:hand" title="Agregar un codigo de demora" name="anular"  onClick="window.location='<%=CONTROLLER%>?estado=Discrepancia&accion=Anular&c_numpla=<%=numpla%>&c_numrem=<%=numrem%>&c_tipo_doc=<%=tipo_docu%>&c_documento=<%=documen%>&c_num_discre=<%=num_discre%>&nom_doc=<%=nom_doc%>&c_tipo_doc_rel=<%=tipo_docu_rel%>&c_documento_rel=<%=documen_rel%>&client=<%=client%>'" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);"><% } } %>
						<img src="<%=BASEURL%>/images/botones/salir.gif" style="cursor:hand" name="salir" title="Salir al Menu Principal" onMouseOver="botonOver(this);" onClick="parent.close();" onMouseOut="botonOut(this);" ></td>
					</tr>	
					</form>	
					<tr class="pie">
						<td colspan="4" align="left" >
							<% if(exis.equals("true")){
								Vector Vecdis = model.discrepanciaService.getItemsDiscrepancia ();
								Discrepancia dis;
								paso = request.getParameter("paso"); 
								if(paso.equals("true")){
									Producto pro = model.productoService.getProducto();
									cod = pro.getCodigo();
									desc = pro.getDescripcion();
									uni = pro.getUnidad();
								}
								else{
									cod = "";
									desc = "";
									uni = "";
								} %>
								<%-- Tabla de los productos de las discrepancias --%>
								<table width="100%" border="2" align="center">
									<tr>
										<td>
											<table width="100%" class="tablaInferior">
												<tr>
													<td colspan="2" class="subtitulo1">&nbsp;Productos de Discrepancia</td>
													<td colspan="2" class="barratitulo"><img src="<%=BASEURL%>/images/titulo.gif" width="32" height="20"></td>
												</tr>        
												<form name='forma2' method='POST' action="<%=CONTROLLER%>?estado=Producto_discreapancia&accion=Serch" >
												<tr>
													<td width="16%" align="left" class="fila">Codigo Producto</td>
													<td width="34%" valign="middle" class="fila"><input name="c_cod_producto" type="text" class="textbox" id="c_cod_producto" size="10" value="<%=cod%>" maxlength="15">
														<img src="<%=BASEURL%>/images/botones/iconos/lupa.gif" width="20" height="20" style="cursor:hand" title="Buscar un producto de discrepancia" name="buscar"  onClick="return validarProductoDis();" >
														<a onClick="window.open('<%=CONTROLLER%>?estado=Menu&accion=Cargar&pagina=ProductoInsertar.jsp&marco=no&carpeta=/jsp/cumplidos/producto&client=<%=client%>&lupa=ok','','status=yes,scrollbars=no,width=650,height=350,resizable=yes')" style="cursor:hand"><img src="<%=BASEURL%>/images/botones/iconos/new.gif"></a>
														<input name="c_documento" type="hidden" value="<%=documen%>">
														<input name="c_num_discre" value="<%=num_discre%>" type="hidden">
														<input name="c_numpla" value="<%=numpla%>" type="hidden">
														<input name="c_numrem" value="<%=numrem%>" type="hidden">
														<input name="client" value="<%=client%>" type="hidden">
														<input name="nom_doc" type="hidden" value="<%=nom_doc%>">														
														<input name="c_contacto" type="hidden" class="textbox" id="c_contacto" value="<%=contac%>" size="40" maxlength="40">
														<input name="c_tipo_doc" type="hidden" value="<%=tipo_docu%>">
														<input name="c_nomdes" type="hidden" id="c_nomdes" value="<%=nom_des%>">
														<input name="c_tipo_doc_rel" type="hidden" id="c_tipo_doc_rel" value="<%=tipo_docu_rel%>">
														<input name="c_documento_rel" type="hidden" id="c_documento_rel" value="<%=documen_rel%>">                                                        </td>
													<td width="16%" valign="middle" class="fila">Descripci&oacute;n</td>
													<td width="34%" valign="middle" class="letra"><%=desc%><INPUT name="c_descripcion" type="hidden" cols="50" class="textbox" id="c_descripcion" value="<%=desc%>"></td>
												</tr>
												</form>
												<FORM name='forma3' method='POST' action="<%=CONTROLLER%>?estado=Discrepancia_producto&accion=Insert">
												<tr id="cantidad">
													<td align="left" class="fila">Cantidad </td>
													<td valign="middle" class="fila"><input name="c_cantidad" type="text" class="textbox" id="c_cantidad" size="13" maxlength="13" onKeyPress="soloDigitos(event,'decOK')"> 
													<img src="<%=BASEURL%>/images/botones/iconos/obligatorio.gif"></td>
													<td valign="middle" class="fila">Unidad</td>
													<td valign="middle" class="letra">
													<%Vector vecu2 = model.unidadService.listarUnidades(); 
													for(int i = 0; i<vecu2.size(); i++){	
														Unidad u2 = (Unidad) vecu2.elementAt(i);
														if(uni.equals(u2.getCodigo())){%> <%=u2.getDescripcion()%> 
														<%}%>
													<%}%></td>
												</tr>
												<tr class="fila" id="cantidad">
													<td align="left" >Tipo Discrepancia </td>
													<td valign="middle">
														<select name="c_cod_discrepancia" class="textbox"  id="c_cod_discrepancia" onChange="cargarDiscrepancia('<%=CONTROLLER%>');">
															<%Vector vecd = model.codigo_discrepanciaService.listarCodigo_discrepancias(); 
															for(int i = 0; i<vecd.size(); i++){	
																Codigo_discrepancia c = (Codigo_discrepancia) vecd.elementAt(i);	
																if(!c.getCodigo().equals("DE")){%>
																	<option value="<%=c.getCodigo()%>"><%=c.getDescripcion()%></option>
																<%}
															}%>
														</select> <img src="<%=BASEURL%>/images/botones/iconos/obligatorio.gif">
													</td>
													<td valign="middle">Responsable</td>
													<td valign="middle">
														<select name="c_responsable" class="textbox" id="c_responsable" onChange="cargarResponsable('<%=CONTROLLER%>');">
															<%
															Vector Vecre = model.responsableService.getResponsables();
															for(int h = 0; h<Vecre.size(); h++){	
																Responsable resa = (Responsable) Vecre.elementAt(h);	%>
																<option value="<%=resa.getCodigo()%>"><%=resa.getDescripcion()%></option>
															<%}%>
														</select> <img src="<%=BASEURL%>/images/botones/iconos/obligatorio.gif"></td>
												</tr>
												<tr class="fila">
													<td align="left" >Causa Devoluci&oacute;n </td>
													<td valign="middle">										
														<select name="c_causa" class="textbox" id="c_causa" onChange="cargarDiscrepancia('<%=CONTROLLER%>');">
															<%Vector Vecc = model.causadiscreService.listarCausas();
															for(int i = 0; i<Vecc.size(); i++){	
																CausaDiscrepancia cd = (CausaDiscrepancia) Vecc.elementAt(i);	%>
																<option value="<%=cd.getCodigo()%>"><%=cd.getDescripcion()%></option>
															<%}%>
														</select> <img src="<%=BASEURL%>/images/botones/iconos/obligatorio.gif"></td>
													<td valign="middle">Valor</td>
												  <td valign="middle"><input name="c_valor" type="text" class="textbox" id="c_valor" size="13" maxlength="13" onKeyPress="soloDigitos(event,'decOK')"></td>
												</tr>
												<tr class="fila" id="grupo_tr" style=" display:none">
													<td>Grupos del Cliente</td>
													<td colspan="3"><% TreeMap grupo = model.tablaGenService.obtenerTreeMapLista( model.tablaGenService.getGrupo() ); %>
												        <input:select name="grupo" options="<%=grupo%>" default="" attributesText="style='width:40%;' id='grupo'  onchange ='cargarGruposDiscrepancia(this.value);'"/></td>
												  </tr>
												<tr class="fila">
													<td colspan="4" align="left" >&nbsp;</td>
												</tr>
												<tr class="fila">
													<td colspan="4" align="center"><input name="c_descripcion" type="hidden" value="<%=desc%>">
														<input name="c_cod_producto" type="hidden" value="<%=cod%>">
														<input type="hidden" name="controller" value="<%=CONTROLLER%>">
														<input name="c_documento" type="hidden" value="<%=documen%>">
														<input name="c_tipo_doc_rel" type="hidden" id="c_tipo_doc_rel" value="<%=tipo_docu_rel%>">
														<input name="c_documento_rel" type="hidden" id="c_documento_rel" value="<%=documen_rel%>">
														<input name="c_unidad" value="<%=uni%>" type="hidden">
														<input name="c_num_discre" value="<%=num_discre%>" type="hidden">
														<input name="c_contacto" type="hidden" class="textbox" id="c_contacto" value="<%=contac%>" size="40" maxlength="40">
														<input name="c_numpla" value="<%=numpla%>" type="hidden">
														<input name="c_numrem" value="<%=numrem%>" type="hidden">
														<input name="c_tipo_doc" type="hidden" value="<%=tipo_docu%>">
														<input name="client" value="<%=client%>" type="hidden">
														<input name="nom_doc" type="hidden" value="<%=nom_doc%>">
														<input name="c_nomdes" type="hidden" id="c_nomdes" value="<%=nom_des%>">
														<input name="c_ubicacion" type="hidden" id="c_ubicacion" value="<%=ubica%>">
														<input name="opcion" type="hidden">
														<%if(model.discrepanciaService.getItemsDiscrepancia () != null && model.discrepanciaService.getItemsDiscrepancia ().size ()>0){ if(fecha_cierre.equals("0099-01-01 00:00:00")){%><img src="<%=BASEURL%>/images/botones/aceptar.gif" style="cursor:hand" title="Guardar Los Cambios de la Discrepancia" name="Ingresar3"  onclick="forma3.opcion.value = '1';forma3.submit();" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);"><%}else{%><img src="<%=BASEURL%>/images/botones/aceptarDisable.gif" style="cursor:hand" title="Guardar Los Cambios de la Discrepancia" name="Ingresar3"><%}}%>&nbsp;
														<%if(fecha_cierre.equals("0099-01-01 00:00:00")){%><img src="<%=BASEURL%>/images/botones/agregar.gif" style="cursor:hand" title="Ingresar Productos de Discrepancia" name="Ingresar"  onclick="return validarDiscrepanciaProducto();" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);"><%}else{%><img src="<%=BASEURL%>/images/botones/agregarDisable.gif" style="cursor:hand" title="Ingresar Productos de Discrepancia" name="Ingresar"><%}%>&nbsp;
														<img src="<%=BASEURL%>/images/botones/cancelar.gif" style="cursor:hand" name="regresar" title="Limpiar todos los registros" onMouseOver="botonOver(this);" onClick="forma3.reset();" onMouseOut="botonOut(this);" >&nbsp;															
														<img src="<%=BASEURL%>/images/botones/salir.gif" style="cursor:hand" name="salir" title="Salir al Menu Principal" onClick="parent.close();" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);"></td>
												</tr>
												</form>
											</table>
										</td>
									</tr>
								</table>										
								<table width="100%" border="1" bordercolor="#999999" bgcolor="#F7F5F4" align="center">
									<tr class="tblTitulo" align="center">
										<td width="9%" align="center">Codigo Producto </td>
										<td width="33%" align="center">Descripci&oacute;n</td>
										<td width="10%" align="center">Valor</td>
										<td width="10%" align="center">Cantidad</td>
										<td width="10%" align="center">Unidad</td>
										<td width="13%" align="center">Tipo Discrepancia </td>
										<td width="14%" align="center">Causa Devoluci&oacute;n </td>
										<td width="11%" align="center">Responsable</td>
									</tr>
									<% if ( Vecdis != null && Vecdis.size() >0 ){
										for(int i = 0; i<Vecdis.size(); i++){	
											dis = (Discrepancia) Vecdis.elementAt(i);%>
											<tr class="<%=(i % 2 == 0 )?"filagris":"filaazul"%>" onMouseOver='cambiarColorMouse(this)' style="cursor:hand" onClick="window.open('<%=CONTROLLER%>?estado=Discrepancia_producto&accion=Serch&c_num_discre=<%=num_d%>&c_numpla=<%=numpla%>&c_numrem=<%=numrem%>&c_tipo_doc=<%=tipo_docu%>&c_documento=<%=documen%>&c_cod_producto=<%=dis.getCod_producto()%>&c_cod_discrepancia=<%=dis.getCod_discrepancia()%>&nom_doc=<%=nom_doc%>&c_fecha_creacion=<%=dis.getFecha_creacion()%>&c_tipo_doc_rel=<%=dis.getTipo_doc_rel()%>&c_documento_rel=<%=dis.getDocumento_rel()%>&client=<%=client%>&x=<%=i%>','','status=yes,scrollbars=no,width=720,height=400,resizable=yes');">
												<td width="9%" align="center" class="bordereporte"><%=dis.getCod_producto()%></td>
												<td width="33%" align="center" class="bordereporte"><%=dis.getDescripcion()%></td>
												<td width="10%" align="center" class="bordereporte"><%=Util.PuntoDeMil(String.valueOf ( Math.round ( dis.getValor() ) ) )%></td>
												<td width="10%" align="center" class="bordereporte"><%=Util.PuntoDeMil(String.valueOf ( Math.round ( dis.getCantidad() ) ) )%></td>
												<td width="10%" align="center" class="bordereporte"><%model.unidadService.serchUnidad(dis.getUnidad(),"P");
													Unidad un = model.unidadService.getUnidad(); %><%=un.getDescripcion()%> </td>
												<td width="13%" align="center" class="bordereporte"><%model.codigo_discrepanciaService.serchCodigo_discrepancia(dis.getCod_discrepancia());
													Codigo_discrepancia cd = model.codigo_discrepanciaService.getCodigo_discrepancia(); %><%=cd.getDescripcion()%></td>
												<td width="14%" align="center" class="bordereporte">	<%Vector Vecc2 = model.causadiscreService.listarCausas();
													for(int j = 0; j<Vecc2.size(); j++){	
														CausaDiscrepancia cd2 = (CausaDiscrepancia) Vecc2.elementAt(j);	
														if(dis.getCausa_dev().equals(cd2.getCodigo())){%>
															<%=cd2.getDescripcion()%><%
														}
													}%></td>
												<td width="11%" align="center" class="bordereporte">	<% model.responsableService.serchResponsable(dis.getResponsable());%>
																										<%=model.responsableService.getResponsable()!=null?model.responsableService.getResponsable().getDescripcion():""%></td>
											</tr>
										<%}
									}%>
									<tr class="pie">
										<td td height="20" colspan="18" nowrap align="center">
										</td>
									</tr>
								</table>
							<%}%>
						</td>
					</tr>
				</table>
				<% }%></td>
		</tr>
	</table>
<% } 
if(mensaje!=null){%>
	<%--Mensaje de informacion--%>
	<br>
	<table border="2" align="center">
		<tr>
			<td>
				<table width="100%"  border="1" align="center"  bordercolor="#F7F5F4" bgcolor="#FFFFFF">
					<tr>
						<td width="229" align="center" class="mensajes"><%=mensaje%></td>
						<td width="29" background="<%=BASEURL%>/images/cuadronaranja.JPG">&nbsp;</td>
						<td width="58">&nbsp;</td>
					</tr>
				</table>
			</td>
		</tr>
	</table>
<%}%>
<br>
<%--tabla para el boton de volver--%>
<table width="1000" border="0" cellspacing="0" cellpadding="0" align="center">
	<tr>
		<td><img src="<%=BASEURL%>/images/botones/regresar.gif" style="cursor:hand" title="Volver" name="buscar"  onClick="window.location='<%=CONTROLLER%>?estado=Menu&accion=Cargar&carpeta=/jsp/cumplidos/discrepancia&pagina=asignarDiscrepanciaPlanilla.jsp?msg=&marco=no'" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);"> </td>
	</tr>
</table>
</div>
<%=datos[1]%>
<iframe width=188 height=166 name="gToday:normal:agenda.js:gfPop:plugins.js" id="gToday:normal:agenda.js:gfPop:plugins.js" src="<%=BASEURL%>/js/Calendario/ipopeng.htm" scrolling="no" frameborder="0" style="visibility:visible; z-index:999; position:absolute; left:-500px; top:0px;"> </iframe>
</body>
</html>
<%--Funciones Javascript --%>
<script>
function cargarDiscrepancia ( BASEURL ){
	if(forma3.c_cod_discrepancia.value=='RE'){
		grupo_tr.style.display="block";
	}
	else{
		grupo_tr.style.display="none";
	}	
}
function cargarResponsable ( BASEURL ){
	if(forma3.c_responsable.value=='OT'){
		window.open(BASEURL+"?estado=Menu&accion=Cargar&carpeta=/jsp/masivo/tblotros_conceptos&titulo=Definicion Otros Conceptos&pagina=otrconceptDefinir.jsp?tabla=responsable-_-campo=Responsables",'Definicion_Otros_Conceptos','status=yes,scrollbars=yes,width=720,height=400,resizable=yes');
	}	
}	
function AdjuntarImagenes(BASEURL,acti,tipo) {
	var pag="";	
	pag = "/imagen/Manejo.jsp?documento="+forma.c_numpla.value+"-_-actividad="+acti+"-_-tipoDocumento="+tipo;
	window.open(BASEURL+"/Marcostsp.jsp?encabezado=Agregar Imagen&dir="+pag,'Trafico','width=680,height=700,scrollbars=yes,resizable=yes,top=10,left=65');
}
function cargarGruposDiscrepancia ( grupo ){
	window.open(forma3.controller.value+"?estado=Cargar&accion=Varios&carpeta=/jsp/cumplidos/discrepancia&marco=no&pagina=ListaGrupo.jsp&sw=9&grupo="+grupo,'','status=yes,scrollbars=no,width=500,height=500,resizable=no');
}
</script>
<%--Fin de funciones Javascript--%>
