<!--
- Autor : Ing. Jose de la rosa
- Date  : 18 de Noviembre de 2005
- Copyrigth Notice : Fintravalores S.A. S.A
-->
<%--
-@(#)
--Descripcion : Pagina JSP, que maneja el ingreso de las discrepancias generales
--%>

<%-- Declaracion de librerias--%>
<%@page contentType="text/html"%>
<%@page session="true"%>
<%@page import="java.util.*"%>
<%@page import="com.tsp.operation.model.*"%>
<%@page import="com.tsp.util.*"%>
<%@page import="com.tsp.operation.model.beans.*, java.util.*"%>
<%@include file="/WEB-INF/InitModel.jsp"%>
<%@taglib uri="http://jsptags.com/tags/navigation/pager" prefix="pg" %>
<html>
<head>
<title>Modificar Discrepancia General</title>
<link href="<%= BASEURL %>/css/estilostsp.css" rel="stylesheet" type="text/css">
<script type='text/javascript' src="<%= BASEURL %>/js/boton.js"></script>
</head>
<script src='<%=BASEURL%>/js/date-picker.js'></script>
<script type='text/javascript' src="<%= BASEURL %>/js/validar.js"></script>
<script type='text/javascript' src="<%= BASEURL %>/js/reporte.js"></script>
<%
	String path = application.getRealPath(model.menuService.getRealUrl(request.getRequestURI()));
	String datos[] = model.menuService.getContenidoMenu(BASEURL,request.getRequestURI(),path);
	String style = "simple";
    String position =  "bottom";
    String index =  "center";
    int maxPageItems = 10;
    int maxIndexPages = 10;
    String numpla = request.getParameter("c_numpla");
    InfoPlanilla info = model.planillaService.getInfoPlanilla();
    info.setPropietario(model.planillaService.getNomPropie());
    String conductor = info.getCedulaConductor() + " " + info.getNombreConductor();
    String ruta = info.getOrigen() + " - " + info.getDestino();
    String fecha = info.getFecha();
    String placa = info.getVehiculo();
	String propietario = info.getPropietario();
	Discrepancia d = (Discrepancia) model.discrepanciaService.getDiscrepancias().elementAt(0);
%>
<%-- Inicio Body --%>
<body <%if(request.getParameter("reload")!=null){%>onLoad="parent.opener.location.reload();"<%}%>>
<div id="capaSuperior" style="position:absolute; width:100%; height:100px; z-index:0; left: 0px; top: 0px;">
 <jsp:include page="/WEB-INF/toptsp.jsp?encabezado=Modificar Discrepancia General"/>
</div>

 <div id="capaCentral" style="position:absolute; width:100%; height:85%; z-index:0; left: 0px; top: 100px; overflow: scroll;"> 
 
<%-- Tabla del detalle de la planilla --%>
<table width="900" border="2" align="center">          
	<tr>
		<td>
			<table width="100%" class="tablaInferior">
				<tr>
					<td width="50%" colspan="2" class="subtitulo1">&nbsp;Detalle de la Planilla</td>
					<td width="50%" colspan="2" class="barratitulo"><img src="<%=BASEURL%>/images/titulo.gif" width="32" height="20" align="left"><%=datos[0]%></td>
				</tr>
				<tr>
					<td width="14%" class="fila">No.:</td>
					<td width="36%" class="letra"><%=numpla %></td>
					<td width="14%" class="fila">Fecha:</td>
					<td width="36%" class="letra"><%=fecha %></td>
				</tr>
				<tr>
					<td class="fila">Conductor:</td>
					<td class="letra"><%=conductor %></td>
					<td class="fila">Placa:</td>
					<td class="letra"><%=placa %></td>
				</tr>
				<tr>
					<td class="fila">Ruta:</td>
					<td class="letra"><%=ruta %></td>
					<td class="fila">Propietario:</td>
					<td class="letra"><%= propietario %></td>
				</tr>
			</table>
		</td>
	</tr>
</table>
<FORM name='forma3' method='POST' action="<%=CONTROLLER%>?estado=Discrepancia_general&accion=Update" >
	<%-- Tabla de las discrepancias --%>
	<table width="900" border="2" align="center">          
		<tr>
			<td>
				<table width="100%" class="tablaInferior">
					<%-- Formulario de ingreso y modificacion de discrepancia --%>
					<tr class="fila">
						<td colspan="2" class="subtitulo1">&nbsp;Discrepancia</td>
						<td colspan="2" class="barratitulo"><img src="<%=BASEURL%>/images/titulo.gif" width="32" height="20"></td>
					</tr>								
					<tr class="fila">
						<td width="14%">Retenci&oacute;n Pago </td>
						<td width="36%"><select name="c_retencion" class="textbox" id="c_retencion">
								<option value="S" <% if(d.getRetencion().equals("S")){%> selected <%}%>>Si</option>
								<option value="N" <% if(d.getRetencion().equals("N")){%> selected <%}%>>No</option>
							</select></td>
						<td width="14%">Tipo Discrepancia </td>
						<td width="36%"><select name="c_cod_discrepancia" class="textbox"  id="c_cod_discrepancia">
						  <%Vector vecd = model.codigo_discrepanciaService.listarCodigo_discrepancias(); 
							for(int i = 0; i<vecd.size(); i++){	
								Codigo_discrepancia c = (Codigo_discrepancia) vecd.elementAt(i);%>
								<option value="<%=c.getCodigo()%>" <%if(c.getCodigo().equals(d.getCod_discrepancia())){%> selected <%}%> ><%=c.getDescripcion()%></option>
						  <%}%>
						</select>
						  <img src="<%=BASEURL%>/images/botones/iconos/obligatorio.gif"> </td>
					</tr>
					<tr class="fila">
						<td align="left"> Cantidad </td>
					  <td><input name="c_cantidad" type="text" class="textbox" id="c_cantidad" value="<%=d.getCantidad()%>" size="10" maxlength="10" onKeyPress="soloDigitos(event,'decOK')">
						  <select name="c_unidad" id="c_unidad" class="textbox" style='width:70%;'>
							<%  Vector vecu2 = model.unidadService.listarUnidades(); 
							for(int i = 0; i<vecu2.size(); i++){	
								Unidad u2 = (Unidad) vecu2.elementAt(i);%>
								<option value="<%=u2.getCodigo()%>" <%if(u2.getCodigo().equals(d.getUnidad())){%> selected <%}%> ><%=u2.getDescripcion()%> </option>
								<%
							}%>
						  </select><img src="<%=BASEURL%>/images/botones/iconos/obligatorio.gif"></td>
						<td valign="middle">Valor</td>
						<td valign="middle"><input name="c_valor" type="text" value="<%=d.getValor()%>" class="textbox" id="c_valor" size="10" maxlength="10" onKeyPress="soloDigitos(event,'decOK')">
						</td>
					</tr>
					<tr class="fila">
						<td> Observaci&oacute;n </td>
						<td colspan="3"><textarea name="c_observacion" cols="150" rows="3" class="textbox" id="c_observacion"><%=d.getObservacion()%></textarea></td>
					</tr>					
				</table>
			</td>
		</tr>
	</table>
	<input name="c_numpla" id="c_numpla" value="<%=numpla%>" type="hidden">
	<input name="c_num_discre" id="c_num_discre" value="<%=d.getNro_discrepancia()%>" type="hidden">
	<input name="c_fecha" id="c_fecha" value="<%=d.getFecha_creacion()%>" type="hidden">
	<p align="center">
		<img src="<%=BASEURL%>/images/botones/modificar.gif" title="Agregar una discrepancia" style="cursor:hand" name="modificar"  onclick="return validarDiscrepanciaProducto();" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);">&nbsp;
		<img src="<%=BASEURL%>/images/botones/anular.gif" style="cursor:hand" title="Anular una sanci�n" name="anular"  onClick="window.location='<%=CONTROLLER%>?estado=Discrepancia_general&accion=Anular&c_numpla=<%=numpla%>&c_num_discre=<%=d.getNro_discrepancia()%>&c_fecha=<%=d.getFecha_creacion()%>'" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);">&nbsp;
		<img src="<%=BASEURL%>/images/botones/salir.gif" style="cursor:hand" name="salir" title="Salir al Menu Principal" onMouseOver="botonOver(this);" onClick="parent.close();" onMouseOut="botonOut(this);" >&nbsp;
		<img src="<%=BASEURL%>/images/botones/regresar.gif" style="cursor:hand" title="Volver" name="buscar"  onClick="window.location='<%=CONTROLLER%>?estado=Informacion&accion=Planilla_discrepancia&tipo=G&numpla=<%=numpla%>'" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);">
	</p>
</form>	
<%
if(request.getAttribute("msg")!=null){%>
	<%--Mensaje de informacion--%>
	<br>
	<table border="2" align="center">
		<tr>
			<td>
				<table width="100%"  border="1" align="center"  bordercolor="#F7F5F4" bgcolor="#FFFFFF">
					<tr>
						<td width="229" align="center" class="mensajes"><%=request.getAttribute("msg")%></td>
						<td width="29" background="<%=BASEURL%>/images/cuadronaranja.JPG">&nbsp;</td>
						<td width="58">&nbsp;</td>
					</tr>
				</table>
			</td>
		</tr>
	</table>
<%}%>
</div>
<%=datos[1]%>
</body>
</html>
