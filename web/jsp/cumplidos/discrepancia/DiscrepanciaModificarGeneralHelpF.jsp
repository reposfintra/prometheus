<!--
	 - Author(s)       :      Jose de la Rosa
	 - Description     :      AYUDA FUNCIONAL - Buscar Acuerdos especiales
	 - Date            :      08/06/2006 
	 - Copyright Notice:      TRANSPORTES SANCHEZ POLO S.A.
-->
<%@ include file="/WEB-INF/InitModel.jsp"%>
<HTML>
<HEAD>
<TITLE>AYUDA FUNCIONAL - Discrepancia</TITLE>
<META http-equiv=Content-Type content="text/html; charset=windows-1252">
<link href="../css/estilostsp.css" rel="stylesheet" type="text/css"> 
<link href="<%=BASEURL%>/css/estilostsp.css" rel="stylesheet" type="text/css">
<script type='text/javascript' src="<%= BASEURL %>/js/boton.js"></script> 
</HEAD>
<BODY> 
<% String BASEIMG_ACU = BASEURL +"/images/ayuda/equipos/acuerdo_especial/"; %>
<% String BASEIMG = BASEURL +"/images/ayuda/cumplido/discrepancia/"; %>
  <table width="95%"  border="2" align="center">
    <tr>
      <td height="117" >
        <table width="100%" border="0" align="center">
          <tr  class="subtitulo">
            <td height="20"><div align="center">MANUAL DE DISCREPANCIA GENERAL </div></td>
          </tr>
          <tr class="subtitulo1">
            <td>Descripci&oacute;n del funcionamiento del programa para realizar Cumplidos </td>
          </tr>
          <tr>
            <td  class="ayudaHtmlTexto"><div align="center">
              <p>&nbsp;</p>
              </div></td>
          </tr>
            <tr>
            <td  class="ayudaHtmlTexto"><p class="ayudaHtmlTexto">En la siguiente pantalla  como se ilustra en la Imagen1  modificar o anular una discrepancia.</p>
          </td>
          </tr>
            <tr>
            <td  class="ayudaHtmlTexto"><div align="center">
              <p><IMG src="<%=BASEIMG%>DiscrepanciaGeneralModificar.JPG" border=0 ></p>
              <p><strong>Imagen1</strong></p>
            </div></td>
          </tr>
		  
		<tr>
		<td  class="ayudaHtmlTexto"><p class="ayudaHtmlTexto">&nbsp;</p>
		<p class="ayudaHtmlTexto">El sistema verifica que el campo de la cantidad este lleno, si no lo esta aparecer&aacute; como se ilustra en la Imagen2. </p>
		</td>
		</tr>
		<tr>
		<td  class="ayudaHtmlTexto"><div align="center">
		  <p><IMG src="<%=BASEIMG%>MensajeCantidad.JPG" border=0 ></p>
		  <p><strong>Imagen2</strong></p>
		</div></td>
		</tr>		  
		<tr>
		<td  class="ayudaHtmlTexto"><p class="ayudaHtmlTexto">&nbsp;</p>
		<p class="ayudaHtmlTexto">Si modifica una discrepancia  exitosamente aparecer&aacute;  un mensaje como se ilustra en la Imagen3. </p>
		</td>
		</tr>
		<tr>
		<td  class="ayudaHtmlTexto"><div align="center">
		  <p><IMG src="<%=BASEIMG%>MensajeDiscrepanciaModificada.JPG" border=0 ></p>
		  <p><strong>Imagen3</strong></p>
		  </div></td>
		</tr>				  
		  
		  
		<tr>
		<td  class="ayudaHtmlTexto"><p class="ayudaHtmlTexto">&nbsp;</p>
		<p class="ayudaHtmlTexto">Si se anula el registro de una discrepancia aparecer&aacute; un mensaje como se ilustra en la Imagen4 y vuelve a la pagina de creaci&oacute;n de discrepancia. </p>
		</td>
		</tr>
		<tr>
		<td  class="ayudaHtmlTexto"><div align="center">
		  <p><IMG src="<%=BASEIMG_ACU%>MensajeAnular.JPG" border=0 ></p>
		  <p><strong>Imagen4</strong></p>
		  </div></td>
		</tr>
				
      </table></td>
    </tr>
  </table>
  <p></p>
<center>
<img src = "<%=BASEURL%>/images/botones/salir.gif" style = "cursor:hand" name = "imgsalir" onClick = "parent.close();" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);">
</center>
</BODY>
</HTML>
