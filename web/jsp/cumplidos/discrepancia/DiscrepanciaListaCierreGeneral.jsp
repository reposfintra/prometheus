<!--
- Autor : Ing. Jose de la rosa
- Date  : 9 de Nobiembre de 2005
- Copyrigth Notice : Fintravalores S.A. S.A
-->
<%--
-@(#)
--Descripcion : Pagina JSP, que muestra el listado de las discrepancias que se pueden cerrar
--%>

<%-- Declaracion de librerias--%>
<%@page contentType="text/html"%>
<%@page session="true"%>
<%@page errorPage="../error/ErrorPage.jsp"%>
<%@page import="java.util.*"%>
<%@page import="com.tsp.operation.model.*"%>
<%@page import="com.tsp.util.*"%>
<%@page import="com.tsp.operation.model.beans.*, java.util.*"%>
<%@include file="/WEB-INF/InitModel.jsp"%>
<%@taglib uri="http://jsptags.com/tags/navigation/pager" prefix="pg" %>
 
<html>
<head>
<title>Listado de Discrepancias Generales</title>
<link href="<%= BASEURL %>/css/estilostsp.css" rel="stylesheet" type="text/css">
<script type='text/javascript' src="<%= BASEURL %>/js/boton.js"></script>
<script type='text/javascript' src="<%= BASEURL %>/js/reporte.js"></script>
</head>
<%-- Inicio Body --%>
<body>
      <div id="capaSuperior" style="position:absolute; width:100%; height:100px; z-index:0; left: 0px; top: 0px;">
      <jsp:include page="/WEB-INF/toptsp.jsp?encabezado=Listado Discrepancia Generales"/>
      </div>
      <div id="capaCentral" style="position:absolute; width:100%; height:85%; z-index:0; left: 0px; top: 100px; overflow: scroll;">
<%  String style = "simple";
    String position =  "bottom";
    String index =  "center";
    int maxPageItems = 10;
    int maxIndexPages = 10;
	Discrepancia d;
	Vector vec = model.discrepanciaService.getDiscrepancias();
	if ( vec.size() >0 ){  %>
	<%-- Tabla de datos de discrepancia --%>
	<table width="1100" border="2" align="center">
		<tr>
			<td>
				<table width="100%" align="center">
					<tr>
						<td width="373" class="subtitulo1">&nbsp;Datos Discrepancia </td>
						<td width="427" class="barratitulo"><img src="<%=BASEURL%>/images/titulo.gif" width="32" height="20"></td>
					</tr>
				</table>
				<table width="100%" border="1" bordercolor="#999999" bgcolor="#F7F5F4" align="center">
					<tr class="tblTitulo" align="center">
						<td width="7%" align="center">Estado</td>
						<td width="6%" align="center">Numero Planilla</td>
						<td width="7%" align="center">Retenci&oacute;n Pago</td>
						<td width="10%"  align="center">Tipo Discrepancia</td>
						<td width="14%" align="center">Cantidad</td>
						<td width="10%" align="center">Valor</td>
						<td width="8%" align="center">Fecha Cierre</td>
						<td width="12%" align="center">Usuario Cierre </td>
						<td width="10%" align="center">Valor Aprobado</td>
						<td width="8%" align="center">Referencia</td>
					    <td width="8%" align="center">Nota Contable </td>
				    </tr>
					<pg:pager
					items="<%=vec.size()%>"
					index="<%= index %>"
					maxPageItems="<%= maxPageItems %>"
					maxIndexPages="<%= maxIndexPages %>"
					isOffset="<%= true %>"
					export="offset,currentPageNumber=pageNumber"
					scope="request"><%
						for (int i = offset.intValue(), l = Math.min(i + maxPageItems, vec.size()); i < l; i++){
							d = (Discrepancia) vec.elementAt(i);%>
							<pg:item>
								<tr class="<%=(i % 2 == 0 )?"filagris":"filaazul"%>" onMouseOver='cambiarColorMouse(this)'   style="cursor:hand"
								onClick="window.open('<%=CONTROLLER%>?estado=Informacion&accion=Planilla_discrepancia&tipo=C&c_num_discre=<%=d.getNro_discrepancia()%>&numpla=<%=d.getNro_planilla()%>','','status=yes,scrollbars=no,width=900,height=600,resizable=yes');">
									<td width="7%" align="center" class="bordereporte"><%if(d.getFecha_cierre().equals("0099-01-01 00:00:00")){%>&nbsp;<%}else{%><img src="<%=BASEURL%>/images/botones/iconos/eliminar.gif"><%}%></td>
									<td width="6%" align="center" class="bordereporte"><%=d.getNro_planilla()%></td>
									<td width="7%" align="center" class="bordereporte"><% if(d.getRetencion().equals("S")){%>SI<%}else{%>NO<%}%></td>
									<td width="10%" align="center" class="bordereporte"><%model.codigo_discrepanciaService.serchCodigo_discrepancia(d.getCod_discrepancia());Codigo_discrepancia cd = model.codigo_discrepanciaService.getCodigo_discrepancia(); %><%=cd.getDescripcion()%></td>
									<td width="14%" align="center" class="bordereporte"><%=Util.PuntoDeMil(String.valueOf ( Math.round ( d.getCantidad() ) ) )%> <%=d.getUnidad()%></td>
									<td width="10%" align="center" class="bordereporte"><%=Util.PuntoDeMil(String.valueOf ( Math.round ( d.getValor() ) ) )%></td>
									<td width="8%" align="center" class="bordereporte"><%if(d.getFecha_cierre().equals("0099-01-01 00:00:00")){%>&nbsp;<%}else{%><%=d.getFecha_cierre().substring(0,10)%><%}%> </td>
									<td width="12%" align="center" class="bordereporte"><%=d.getUsuario_cierre()%>&nbsp;</td>
									<td width="10%" align="center" class="bordereporte"><%=Util.PuntoDeMil(String.valueOf ( Math.round ( d.getValor_autorizado() ) ) )%></td>
									<td width="8%" align="center" class="bordereporte"><%=d.getReferencia()%>&nbsp;</td>
								    <td width="8%" align="center" class="bordereporte"><%=d.getNota_contable()%>&nbsp;</td>
							    </tr>
							</pg:item>
						<%}%>
						<tr class="pie">
							<td td height="20" colspan="11" nowrap align="center">
								<pg:index>
									<jsp:include page="/WEB-INF/jsp/googlesinimg.jsp" flush="true"/>      
								</pg:index> 
							</td>
						</tr>
					</pg:pager>
		  		</table>
			</td>
		</tr>
	</table>
	<%}
 	else { %>
		<br>
		<%--Mensaje de informacion--%>
		<table border="2" align="center">
			<tr>
				<td>
					<table width="410" height="73" border="1" align="center"  bordercolor="#F7F5F4" bgcolor="#FFFFFF">
						<tr>
							<td width="282" align="center" class="mensajes">La planilla no tiene discrepancias asociadas</td>
							<td width="28" background="<%=BASEURL%>/images/cuadronaranja.JPG">&nbsp;</td>
							<td width="78">&nbsp;</td>
						</tr>
					</table>
				</td>
			</tr>
		</table>
  	<%}%>
	<br>
	<%--tabla para el boton de volver--%>
	<table width="1000" border="0" align="center" cellpadding="0" cellspacing="0">
		<tr style="cursor:hand">
			<td align="left"><img src="<%=BASEURL%>/images/botones/regresar.gif" style="cursor:hand" title="Volver" name="buscar"  onClick="window.location='<%=CONTROLLER%>?estado=Menu&accion=Cargar&carpeta=/jsp/cumplidos/discrepancia&titulo=Buscar Discrepancias&marco=no&pagina=buscarDiscrepanciaPlanilla.jsp?msg='" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);"></td>
		</tr>
	</table>
	</div>
</body>
</html>
