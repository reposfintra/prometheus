<!--
- Autor : Ing. Jose de la rosa
- Date  : 5 de Octubre de 2005
- Copyrigth Notice : Fintravalores S.A. S.A
-->
<%--
-@(#)
--Descripcion : Pagina JSP, que envia una planilla para buscar las discrepancias asociadas a esta
--%>

<%-- Declaracion de librerias--%>
<%@page contentType="text/html"%>
<%@page session="true"%> 
<%@page import="java.util.*" %>
<%@page errorPage="/error/ErrorPage.jsp"%>
<%@page import="com.tsp.operation.model.*"%>
<%@include file="/WEB-INF/InitModel.jsp"%>
<%@page import="com.tsp.operation.model.beans.*"%>
<%@ taglib uri="http://jakarta.apache.org/taglibs/input-0.90" prefix="input" %>
<html>
<head>
<title>Asignaci&oacute;n de Discrepancia</title>
<link href="<%= BASEURL %>/css/estilostsp.css" rel="stylesheet" type="text/css">
<script type='text/javascript' src="<%= BASEURL %>/js/boton.js"></script>
<script type='text/javascript' src="<%= BASEURL %>/js/validar.js"></script>
</head>
<%-- Inicio Body --%>
<body onLoad="redimensionar();" onResize="redimensionar();">
<div id="capaSuperior" style="position:absolute; width:100%; height:100px; z-index:0; left: 0px; top: 0px;">
<jsp:include page="/WEB-INF/toptsp.jsp?encabezado=Asignar Discrepancia"/>
</div>

<div id="capaCentral" style="position:absolute; width:100%; height:85%; z-index:0; left: 0px; top: 100px; overflow: scroll;"> 
<%-- Inicio Formulario --%>
<%
	String path = application.getRealPath(model.menuService.getRealUrl(request.getRequestURI()));
	String datos[] = model.menuService.getContenidoMenu(BASEURL,request.getRequestURI(),path);
	String pla = (request.getParameter("c_numpla") != null)?request.getParameter("c_numpla"):"";  %>
<form name="forma" id="forma" method="post" >
	<%-- Tabla de los numeros de planilla --%>
	<table width="420" border="2" align="center">
		<tr>
			<td>
				<table width="100%" class="tablaInferior">
					<tr class="fila">
					<td colspan="2" >
						<table width="100%"  border="0" cellspacing="1" cellpadding="0">
							<tr>
								<td width="50%" class="subtitulo1">&nbsp;Asignar discrepancia planilla</td>
								<td width="50%" class="barratitulo"><img src="<%=BASEURL%>/images/titulo.gif" width="32" height="20" align="left"><%=datos[0]%></td>
							</tr>
						</table>
					</td>
					</tr>
					<tr class="fila">
						<td width="50%">N&uacute;mero Planilla:</td>
						<td width="50%"><input name="numpla" type="text" class="textbox" value="<%= pla %>" maxlength="10" <% if (request.getParameter("c_numpla") != null) {%> readonly <% } %>></td>
					</tr>
					<tr class="fila">
						<td width="50%">Seleccione el Tipo:</td>
						<td width="50%">Documentos: <input name="tipo" type="radio" id="tipo" value="D" checked>
							General: <input name="tipo" type="radio" id="tipo" value="G">
						</td>
					</tr>					
				</table>
			</td>
		</tr>
	</table>
	<p>
		<div align="center"><img src="<%=BASEURL%>/images/botones/buscar.gif" style="cursor:hand" title="Buscar una planilla" name="buscar"  onClick="irDiscre('<%=CONTROLLER%>');" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);">&nbsp;
		<img src="<%=BASEURL%>/images/botones/salir.gif"  name="salir" style="cursor:hand" title="Salir al Menu Principal" onMouseOver="botonOver(this);" onClick="parent.close();" onMouseOut="botonOut(this);" ></div>
	</p>  
</form>
<% if( !request.getParameter("msg").equals("") ){%>
	<br>
	<%--Mensaje de informacion--%>
	<table border="2" align="center">
		<tr>
			<td>
				<table width="100%"  border="1" align="center"  bordercolor="#F7F5F4" bgcolor="#FFFFFF">
					<tr>
						<td width="229" align="center" class="mensajes"><%=request.getParameter("msg").toString()%></td>
						<td width="29" background="<%=BASEURL%>/images/cuadronaranja.JPG">&nbsp;</td>
						<td width="58">&nbsp;</td>
					</tr>
				</table>
			</td>
		</tr>
	</table>
<%} %>
</div>
<%=datos[1]%>
</body>
</html>
<script>
function irDiscre(controller){
  if(forma.tipo[1].checked)
     tip = 'G';
  else
     tip = 'D';
  window.location.href=controller+'?estado=Informacion&accion=Planilla_discrepancia&numpla='+forma.numpla.value+'&tipo='+tip;
}
</script>