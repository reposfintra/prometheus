<!--  
	 - Author(s)       :      Jose de la rosa
	 - Description     :      AYUDA DESCRIPTIVA - discrepancia
	 - Date            :      11/06/2006 
	 - Copyright Notice:      TRANSPORTES SANCHEZ POLO S.A.
-->
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN""http://www.w3.org/TR/html4/loose.dtd">
<%@page session="true"%> 
<%@page errorPage="/error/ErrorPage.jsp"%>
<%@page import="com.tsp.operation.model.beans.*, java.util.*"%>
<%@ include file="/WEB-INF/InitModel.jsp"%>
<%@ taglib uri="http://jakarta.apache.org/taglibs/input-0.90" prefix="input" %>
<html>
<head>
<title>AYUDA DESCRIPTIVA - Soporte Incompletos</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link href="<%= BASEURL %>/css/estilostsp.css" rel="stylesheet" type="text/css">
<script type='text/javascript' src="<%= BASEURL %>/js/boton.js"></script>
</head>

<body>
<br>
<table width="750"  border="2" align="center">
  <tr>
    <td width="100%" >
      <table width="100%" border="0" align="center">
        <tr  class="subtitulo">
          <td height="24" colspan="2"><div align="center">Soporte Incompleto</div></td>
        </tr>
        <tr class="subtitulo1">
          <td colspan="2">Soportes Incompletos. </td>
        </tr>
		<tr class="subtitulo1">
          <td colspan="2">INFORMACION DE LOS SOPORTES INCOMPLETOS </td>
        </tr>
        <tr>
          <td  class="fila">Entregado Por Conductor </td>
          <td  class="ayudaHtmlTexto">Campo para chequear si los documentos fueron entregados por el conductor.</td>
        </tr>		
		<tr>
          <td  class="fila">No Aplica </td>
          <td  class="ayudaHtmlTexto">Campo para chequear que documentos no se le van a generar registros. </td>
        </tr>

        <tr>		
          <td class="fila">Bot&oacute;n Aceptar.</td>
          <td  class="ayudaHtmlTexto">Bot&oacute;n para grabar los cambios de los documentos de discrepancia. </td>
        </tr>		
		<tr>
          <td width="25%" class="fila">Bot&oacute;n Salir.</td>
          <td width="75%"  class="ayudaHtmlTexto">Bot&oacute;n para salir de la vista 'Soporte Incompletos de Discrepancia' y volver a la vista del men&uacute;.</td>
        </tr>		
		<tr>
          <td class="fila">Bot&oacute;n Regresar.</td>
          <td class="ayudaHtmlTexto">Bot&oacute;n para salir de la vista 'Soporte Incompleto de Discrepancia' y volver a la vista de creaci&oacute;n de discrepancia.</td>
        </tr>		
      </table>
    </td>
  </tr>
</table>
<p></p>
<center>
<img src = "<%=BASEURL%>/images/botones/salir.gif" style = "cursor:hand" name = "imgsalir" onClick = "parent.close();" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);">
</center>
</body>
</html>
