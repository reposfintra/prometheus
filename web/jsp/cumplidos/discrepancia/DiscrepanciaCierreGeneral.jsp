<!--
- Autor : Ing. Jose de la rosa
- Date  : 18 de Noviembre de 2005
- Copyrigth Notice : Fintravalores S.A. S.A
-->
<%--
-@(#)
--Descripcion : Pagina JSP, que maneja el ingreso de las discrepancias generales
--%>

<%-- Declaracion de librerias--%>
<%@page contentType="text/html"%>
<%@page session="true"%>
<%@page import="java.util.*"%>
<%@page import="com.tsp.operation.model.*"%>
<%@page import="com.tsp.util.*"%>
<%@page import="com.tsp.operation.model.beans.*, java.util.*"%>
<%@include file="/WEB-INF/InitModel.jsp"%>
<%@taglib uri="http://jsptags.com/tags/navigation/pager" prefix="pg" %>
<html>
<head>
<title>Discrepancia Cierre General</title>
<link href="<%= BASEURL %>/css/estilostsp.css" rel="stylesheet" type="text/css">
<script type='text/javascript' src="<%= BASEURL %>/js/boton.js"></script>
</head>
<script src='<%=BASEURL%>/js/date-picker.js'></script>
<script type='text/javascript' src="<%= BASEURL %>/js/validar.js"></script>
<script type='text/javascript' src="<%= BASEURL %>/js/reporte.js"></script>
<% 	String style = "simple";
    String position =  "bottom";
    String index =  "center";
    int maxPageItems = 10;
    int maxIndexPages = 10;
    String numpla = request.getParameter("c_numpla");
    InfoPlanilla info = model.planillaService.getInfoPlanilla();
    info.setPropietario(model.planillaService.getNomPropie());
    String conductor = info.getCedulaConductor() + " " + info.getNombreConductor();
    String ruta = info.getOrigen() + " - " + info.getDestino();
    String fecha = info.getFecha();
    String placa = info.getVehiculo();
    String propietario = info.getPropietario();
    Discrepancia d = (Discrepancia) model.discrepanciaService.getDiscrepancias().elementAt(0);
    //nuevo
    String fecha_cierre = "";
    String observacion_cierre = "";
    String referencia = "";
    String nota_debito = "";
    String nota_credito = "";
    String valor = "";
    
    fecha_cierre = (d.getFecha_cierre ()!=null)?d.getFecha_cierre ():"";
    observacion_cierre = (d.getObservacion_cierre ()!=null)?d.getObservacion_cierre ():"";
    referencia = (d.getReferencia ()!=null)?d.getReferencia ():"";
    nota_debito = (d.getNota_debito ()!=null)?d.getNota_debito ():"";
    nota_credito = (d.getNota_credito ()!=null)?d.getNota_credito ():"";
    valor = String.valueOf(d.getValor_autorizado ());

%>
<%-- Inicio Body --%>
<body <%if(request.getParameter("reload")!=null){%>onLoad="parent.opener.location.reload();"<%}%> onResize="redimensionar();">
<div id="capaSuperior" style="position:absolute; width:100%; height:100px; z-index:0; left: 0px; top: 0px;">
 <jsp:include page="/WEB-INF/toptsp.jsp?encabezado=Cierre Discrepancia General"/>
</div>

 <div id="capaCentral" style="position:absolute; width:100%; height:85%; z-index:0; left: 0px; top: 100px; overflow: scroll;"> 
 
<%-- Tabla del detalle de la planilla --%>
<table width="900" border="2" align="center">          
	<tr>
		<td>
			<table width="100%" class="tablaInferior">
				<tr>
					<td width="50%" colspan="2" class="subtitulo1">&nbsp;Detalle de la Planilla</td>
					<td width="50%" colspan="2" class="barratitulo"><img src="<%=BASEURL%>/images/titulo.gif" width="32" height="20"></td>
				</tr>
				<tr class="fila">
					<td width="14%">No.:</td>
					<td width="36%"><%=numpla %></td>
					<td width="14%">Fecha:</td>
					<td width="36%"><%=fecha %></td>
				</tr>
				<tr class="fila">
					<td >Conductor:</td>
					<td ><%=conductor %></td>
					<td >Placa:</td>
					<td ><%=placa %></td>
				</tr>
				<tr class="fila">
					<td >Ruta:</td>
					<td ><%=ruta %></td>
					<td >Propietario:</td>
					<td ><%= propietario %></td>
				</tr>
			</table>
		</td>
	</tr>
</table>
<FORM name='forma' method='POST' action="<%=CONTROLLER%>?estado=Discrepancia_general&accion=Cierre">	
	<%-- Tabla de las discrepancias --%>
	<table width="900" border="2" align="center">          
		<tr>
			<td>
				<table width="100%" class="tablaInferior">
					<%-- Formulario de ingreso y modificacion de discrepancia --%>
					<tr class="fila">
						<td colspan="2" class="subtitulo1">&nbsp;Discrepancia</td>
						<td colspan="2" class="barratitulo"><img src="<%=BASEURL%>/images/titulo.gif" width="32" height="20"></td>
					</tr>								
					<tr class="fila">
						<td width="14%">Retenci&oacute;n Pago </td>
						<td width="36%"><% if(d.getRetencion().equals("S")){%>Si<%}else{%>No<%}%></td>
						<td width="14%">Tipo Discrepancia </td>
						<td width="36%">
						  <%Vector vecd = model.codigo_discrepanciaService.listarCodigo_discrepancias(); 
							for(int i = 0; i<vecd.size(); i++){	
								Codigo_discrepancia c = (Codigo_discrepancia) vecd.elementAt(i);%>
								<%if(c.getCodigo().equals(d.getCod_discrepancia())){%><%=c.getDescripcion()%><%}%>
						  <%}%></tr>
					<tr class="fila">
						<td align="left"> Cantidad </td>
					  <td><%=Util.PuntoDeMil(String.valueOf ( Math.round ( d.getCantidad() ) ) )%>
							<%  model.unidadService.buscarUnidad(d.getUnidad());
                                                            Unidad unid = model.unidadService.getUnidad();%>
                                                            <%=unid!=null?unid.getDescripcion():""%>
								</td>
						<td valign="middle">Valor</td>
						<td valign="middle"><%=Util.PuntoDeMil(String.valueOf ( Math.round (d.getValor()) ) )%>
						</td>
					</tr>
					<tr class="fila">
					  <td width="13%" >Nota Debito </td>
					  <td width="38%" ><input name="c_nota_debito" type="text" class="textbox" <%=(nota_debito.equals(""))?"":"readonly"%> id="c_nota_debito" value="<%=nota_debito%>" size="10" maxlength="10"></td>
					  <td width="10%" >Nota Credito </td>
					  <td width="39%" ><input name="c_nota_credito" type="text" class="textbox" <%=(nota_credito.equals(""))?"":"readonly"%> id="c_nota_credito" value="<%=nota_credito%>" size="10" maxlength="10"></td>
					</tr>
					<tr class="fila">
					  <td> Valor Aprobado</td>
					  <td><input name="c_cantidad" type="text" class="textbox" id="c_cantidad" size="10" value="<%=valor%>" maxlength="10" onKeyPress="soloDigitos(event,'decOK')"></td>
					  <td>Referencia</td>
					  <td><input name="c_referencia" type="text" class="textbox" id="c_referencia" size="10" value="<%=referencia%>" maxlength="10"></td>
					</tr>
					<tr class="fila">
					  <td>Observaci&oacute;n</td>
					  <td colspan="3"><textarea name="c_observacion" cols="150" rows="5" class="textbox" id="c_observacion"><%=observacion_cierre%></textarea></td>
					</tr>
				</table>
			</td>
		</tr>
	</table>
	<input name="c_numpla" id="c_numpla" value="<%=numpla%>" type="hidden">
	<input name="c_num_discre" id="c_num_discre" value="<%=d.getNro_discrepancia()%>" type="hidden">
	<p align="center">
		<img src="<%=BASEURL%>/images/botones/aceptar.gif" title="Cerrar Discrepancia" onClick="validar_campos();" style="cursor:hand" name="Aceptar_" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);">&nbsp;
		<img src="<%=BASEURL%>/images/botones/salir.gif" style="cursor:hand" name="salir" title="Salir al Menu Principal" onMouseOver="botonOver(this);" onClick="parent.close();" onMouseOut="botonOut(this);" >
	</p>
</form>	
<%
if(request.getAttribute("msg")!=null){%>
	<%--Mensaje de informacion--%>
	<br>
	<table border="2" align="center">
		<tr>
			<td>
				<table width="100%"  border="1" align="center"  bordercolor="#F7F5F4" bgcolor="#FFFFFF">
					<tr>
						<td width="229" align="center" class="mensajes"><%=request.getAttribute("msg")%></td>
						<td width="29" background="<%=BASEURL%>/images/cuadronaranja.JPG">&nbsp;</td>
						<td width="58">&nbsp;</td>
					</tr>
				</table>
			</td>
		</tr>
	</table>
<%}%>
</div>
</body>
</html>
<script>
function validar_campos(){
	if(forma.c_nota_debito.value==""&&forma.c_nota_credito.value!=""){
		alert("Debe llenar el campo de la nota debito!");
	}
	else{
		forma.submit();
	}
}
</script>
