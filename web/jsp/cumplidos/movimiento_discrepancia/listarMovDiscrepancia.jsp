<%@page contentType="text/html"%>
<%@page session="true"%>
<%@page errorPage="../error/ErrorPage.jsp"%>
<%@page import="java.util.*"%>
<%@page import="com.tsp.operation.model.*"%>
<%@page import="com.tsp.util.*"%>
<%@page import="com.tsp.operation.model.beans.*, java.util.*"%>
<%@include file="/WEB-INF/InitModel.jsp"%>
<%@taglib uri="http://jsptags.com/tags/navigation/pager" prefix="pg" %>

<html>
<head>
<title>Listado de Paises</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link href="<%= BASEURL %>/css/estilostsp.css" rel="stylesheet" type="text/css"> 
<link href="../../../css/estilostsp.css" rel="stylesheet" type="text/css">
</head>
 
<script type='text/javascript' src="<%= BASEURL %>/js/reporte.js"></script>
<script type='text/javascript' src="<%= BASEURL %>/js/boton.js"></script>
<body onLoad="redimensionar();" onResize="redimensionar();">
<div id="capaSuperior" style="position:absolute; width:100%; height:100px; z-index:0; left: 0px; top: 0px;">
<jsp:include page="/WEB-INF/toptsp.jsp?encabezado=Movimientos de Discrepancia"/>
</div>

<div id="capaCentral" style="position:absolute; width:100%; height:85%; z-index:0; left: 0px; top: 100px; overflow: scroll;"> 

  <%String style = "simple";
    String position =  "bottom";
    String index =  "center";
    int maxPageItems = 7;
    int maxIndexPages = 10;
    
	Vector vec = model.discrepanciaService.getDiscrepancias();
	if ( vec.size() >0 ){
  %>
<form name="forma" method="post" action="">
<table width="545" border="2" align="center">
     <tr>
      <td>
	  	  <table width="100%" align="center">
              <tr>
                <td colspan="3" class="subtitulo1">&nbsp;Movimiento de Discrepancia                </td>
                <td width="265" class="barratitulo"><img src="<%=BASEURL%>/images/titulo.gif" width="32" height="20"></td>
              </tr>
      </table>	  
       <table width="100%" border="1" bordercolor="#999999" bgcolor="#F7F5F4" align="center">
		<tr class="tblTitulo" align="center">
			<td width="109">Codigo Producto</td>
		    <td width="213">Descripci&oacute;n</td>
		    <td width="106">Valor</td>
		    <td width="110">Cantidad</td>
		</tr>
          <pg:pager
    items="<%=vec.size()%>"
    index="<%= index %>"
    maxPageItems="<%= maxPageItems %>"
    maxIndexPages="<%= maxIndexPages %>"
    isOffset="<%= true %>"
    export="offset,currentPageNumber=pageNumber"
    scope="request">
          <%-- keep track of preference --%>
          <%
        for (int i = offset.intValue(), l = Math.min(i + maxPageItems, vec.size()); i < l; i++) {
            Discrepancia d = (Discrepancia) vec.elementAt(i);%>
          <pg:item>
		<tr class="<%=(i % 2 == 0 )?"filagris":"filaazul"%>"  >
					<td class="bordereporte"><%=d.getCod_producto()%></td>
				    <td class="bordereporte"><%=d.getDescripcion()%></td>
				    <td class="bordereporte"><%=d.getValor()%></td>
				    <td class="bordereporte"><%=d.getCantidad()%></td>

				</tr>
          </pg:item>
          <%}%>
          <tr class="bordereporte">
            <td colspan="4" align="center" valign="middle" >
              <div align="center"><pg:index>
                <jsp:include page="../../../WEB-INF/jsp/googlesinimg.jsp" flush="true"/>        
    </pg:index></div></td>
          </tr>
          </pg:pager>
        </table></td>
      </tr>
  </table>
</form>
  <%}
 else { %>
  <table border="2" align="center">
    <tr>
      <td><table width="410" height="40" border="1" align="center"  bordercolor="#F7F5F4" bgcolor="#FFFFFF">
          <tr>
            <td width="282" align="center" class="mensajes">Su b&uacute;squeda no arroj&oacute; resultados!</td>
            <td width="28" background="<%=BASEURL%>/images/cuadronaranja.JPG">&nbsp;</td>
            <td width="78">&nbsp;</td>
          </tr>
      </table></td>
    </tr>
  </table>
  <p>
      <%}%>
</p>
  <table width="545" border="0" align="center">
    <tr>
      <td><img src="<%=BASEURL%>/images/botones/regresar.gif"  name="imgaceptar" onclick="window.location='<%=CONTROLLER%>?estado=Menu&accion=Cargar&pagina=BuscarMovDiscrepancia.jsp&marco=no&carpeta=/jsp/cumplidos/movimiento_discrepancia/'" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" style="cursor:hand"></td>
    </tr>
  </table>
  </div>
</body>
</html>
