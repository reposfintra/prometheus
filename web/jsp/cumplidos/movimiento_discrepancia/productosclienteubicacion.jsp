<!--
- Autor : Ing. Diogenes Bastidas
- Date  : 27 de Diciembre de 2005
- Copyrigth Notice : Fintravalores S.A. S.A
-->
<%--
-@(#)
--Descripcion : Pagina JSP, que lista las ubicacion
--%>

<%-- Declaracion de librerias--%>
<%@page contentType="text/html"%>
<%@page session="true"%>
<%@page errorPage="/error/ErrorPage.jsp"%>
<%@page import="java.util.*"%>
<%@page import="com.tsp.operation.model.*"%>
<%@page import="com.tsp.operation.model.beans.*"%>
<%@page import="com.tsp.util.*"%>
<%@include file="/WEB-INF/InitModel.jsp"%>
<%@taglib uri="http://jsptags.com/tags/navigation/pager" prefix="pg" %>
<html>
<head><title>Productos Cliente</title>
<link href="<%= BASEURL %>/css/estilostsp.css" rel="stylesheet" type="text/css">
<link href="../../../css/estilostsp.css" rel="stylesheet" type="text/css">
<script type='text/javascript' src="<%= BASEURL %>/js/boton.js"></script>
<script type='text/javascript' src="<%= BASEURL %>/js/reporte.js"></script>
<script type='text/javascript' src="<%= BASEURL %>/js/funciones.js"></script>
</head>
<body onLoad="redimensionar();" onResize="redimensionar();">
<div id="capaSuperior" style="position:absolute; width:100%; height:100px; z-index:0; left: 0px; top: 0px;">
<jsp:include page="/WEB-INF/toptsp.jsp?encabezado=Lista de Ubicaciones"/>
</div>

<div id="capaCentral" style="position:absolute; width:100%; height:85%; z-index:0; left: 0px; top: 100px; overflow: scroll;"> 
<%  
    
    String cod = request.getParameter("cliente");
	String ubi = request.getParameter("ubicacion");
	String nom = request.getParameter("nombre");
	String mensaje = (request.getParameter("mensaje")!=null)?request.getParameter("mensaje"):"";
	String candes= "candes";
	
    Vector vec = model.discrepanciaService.getDiscrepancias();
	if ( vec.size() >0 ){%>
<form name="forma" method="post" action="<%=CONTROLLER%>?estado=MovDiscrepancia&accion=Insertar" >	
<table width="749" border="2" align="center">
    <tr>
      <td>
	  	  <table width="100%" align="center">
              <tr>
                <td colspan="3" class="subtitulo1">&nbsp;Ubicaciones 
                <input name="codcli" type="hidden" id="codcli" value="<%=cod%>">
                <input name="ubi" type="hidden" id="ubi" value="<%=ubi%>"></td>
                <td colspan="2" class="barratitulo"><img src="<%=BASEURL%>/images/titulo.gif" width="32" height="20"></td>
              </tr>
              <tr >
                <td width="113" class="fila" >Nro. Planilla </td>
                <td colspan="2" class="fila"><input name="numpla" type="text" class="textbox" id="numpla"></td>
                <td width="79"class="fila">Cliente<input name="nombre" type="hidden" value="<%=nom%>"></td>
                <td width="295" class="letra"><%=nom%></td>
              </tr>
        </table>	  
       <table width="100%" border="1"  bordercolor="#999999" bgcolor="#F7F5F4" align="center">
		<tr class="tblTitulo" align="center">
			<td width="109">Codigo Producto</td>
		    <td width="213">Descripci&oacute;n</td>
		    <td width="106">Valor</td>
		    <td width="110">Cantidad</td>
		    <td width="165">Cantidad despachada</td>
		   
		</tr>
		
	<%
		for (int i = 0;  i < vec.size() ; i++){
			Discrepancia d = (Discrepancia) vec.elementAt(i);%>
				<tr class="<%=(i % 2 == 0 )?"filagris":"filaazul"%>"  >
					<td class="bordereporte"><%=d.getCod_producto()%></td>
				    <td class="bordereporte"><%=d.getDescripcion()%></td>
				    <td class="bordereporte"><%=d.getValor()%></td>
			      <td class="bordereporte"><%=d.getCantidad()%>
			        <input name="cant<%=i%>" type="hidden" id="cant<%=i%>" value="<%=d.getCantidad()%>"></td>
			      <td class="bordereporte">
					
			        <input name="<%=candes+i%>" type="text" class="texbox" id="<%=candes+i%>"  size="10" value="0">			        </td>
				</tr>
			<%}%>
			<input type="hidden" name="numItems"	id="numItems" value="<%=(vec.size())%>">
	 </table>	</td>
  </tr>
</table>
<br>
    <div align="center">
	   <img src="<%=BASEURL%>/images/botones/aceptar.gif"  name="imgaceptar" onClick="validarCantidad();" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" style="cursor:hand">&nbsp;
	   <img src="<%=BASEURL%>/images/botones/cancelar.gif"  name="imgcancelar"  onMouseOver="botonOver(this);" onClick="forma.reset();" onMouseOut="botonOut(this);" style="cursor:hand"> &nbsp;
       <img src="<%=BASEURL%>/images/botones/salir.gif"  name="imgsalir"  onMouseOver="botonOver(this);" onClick="parent.close();" onMouseOut="botonOut(this);" style="cursor:hand">
   </div>
<br>
<%if(!mensaje.equals("")){%>
  </p>
  <p><table border="2" align="center">
  <tr>
    <td><table width="100%"  border="1" align="center"  bordercolor="#F7F5F4" bgcolor="#FFFFFF">
      <tr>
        <td width="229" align="center" class="mensajes">Informacion Ingresada</td>
        <td width="29" background="<%=BASEURL%>/images/cuadronaranja.JPG">&nbsp;</td>
        <td width="58">&nbsp;</td>
      </tr>
    </table></td>
  </tr>
</table>
</p>
 <%}%>
<%}else { %>
</p>
  <table border="2" align="center">
    <tr>
      <td><table width="410" height="73" border="1" align="center"  bordercolor="#F7F5F4" bgcolor="#FFFFFF">
          <tr>
            <td width="282" align="center" class="mensajes">Su b&uacute;squeda no arroj&oacute; resultados!</td>
            <td width="28" background="<%=BASEURL%>/images/cuadronaranja.JPG">&nbsp;</td>
            <td width="78">&nbsp;</td>
          </tr>
      </table></td>
    </tr>
  </table>
  <p>&nbsp; </p>
  <%}%>
 </form>
</div>
</body>
</html>
