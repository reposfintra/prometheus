<%@page contentType="text/html"%>
<%@page session="true"%>
<%@page import="java.util.*"%>
<%@page import="com.tsp.operation.model.*"%>
<%@page import="com.tsp.operation.model.beans.*"%>
<%@page import="com.tsp.util.*"%>
<%@include file="/WEB-INF/InitModel.jsp"%>
<html>
<head>
<title>Asignar Actividad  Cliente</title>
<link href="<%= BASEURL %>/css/estilostsp.css" rel='stylesheet'>
<link href="../../../css/estilostsp.css" rel="stylesheet" type="text/css">
</head>
<script type='text/javascript' src="<%= BASEURL %>/js/validar.js"></script>
<script type='text/javascript' src="<%= BASEURL %>/js/boton.js"></script>
<script>
function campos(){
	if (forma.fecini.value=="" || forma.fecfin.value==""  ) {
    	alert("Seleccione las Fechas");
    }
	else{
		forma.submit();
	}
}
</script>
<body onResize="redimensionar()" onLoad="redimensionar()">
<div id="capaSuperior" style="position:absolute; width:100%; height:100px; z-index:0; left: 0px; top: 0px;">
 <jsp:include page="/toptsp.jsp?encabezado=Buscar Movimiento Discrepancia"/>
</div>
<div id="capaCentral" style="position:absolute; width:100%; height:87%; z-index:0; left: 0px; top: 100px; overflow: scroll;">
<% String codcli ="",codact="",res="";
   Vector Vecclient = model.clienteService.getClientes();
%>
<form name="forma" method="post" action="<%=CONTROLLER%>?estado=MovDiscrepancia&accion=Busqueda" onSubmit="return validarTCamposLlenos();">
    <table width="420" border="2" align="center">
      <tr>
        <td><table width="100%"  border="0">
          <tr>
            <td width="48%" class="subtitulo1"><span class="subtitulos">Informaci&oacute;n</span></td>
            <td width="52%" class="barratitulo"><img src="<%=BASEURL%>/images/titulo.gif" width="32" height="20"></td>
          </tr>
        </table>
          <table width="100%" class="tablaInferior">
          <tr class="fila">
            <td width="113">Cliente</td>
            <td width="283"><select name="cliente" id="cliente" class="textbox">
                <%for (int i = 0; i < Vecclient.size(); i++){
			 Cliente clien = (Cliente) Vecclient.elementAt(i);				
		    if (clien.getCodcli().equals(codcli)){%>
                <option value="<%=clien.getCodcli()%>" selected><%=clien.getNomcli()%></option>
                <%}else{%>
                <option value="<%=clien.getCodcli()%>"><%=clien.getNomcli()%></option>
                <%}
	}%>
              </select>
            </td>
          </tr>
          <tr class="fila">
            <td>Ubicacion</td>
            <td><input name="ubicacion" type="text" class="textbox" id="ubicacion"></td>
          </tr>
          <tr class="fila">
            <td>Fecha Inical </td>
            <td><input   name="fecini"  type="text"  class="textbox" id="fecini" value="" size="15" readonly >
              <a href="javascript:void(0)" onclick="if(self.gfPop)gfPop.fPopCalendar(document.forma.fecini);return false;" HIDEFOCUS><img src="<%=BASEURL%>\js\Calendario\cal.gif" width="16" height="16" border="0" alt="De click aqui para escoger la fecha"></a> <img src="<%=BASEURL%>/images/botones/iconos/obligatorio.gif" width="10" height="10"></td>
          </tr>
          <tr class="fila">
            <td>Fecha Final </td>
            <td><input   name="fecfin"  type="text"  class="textbox" id="fecfin" value="" size="15" readonly >
              <a href="javascript:void(0)" onclick="if(self.gfPop)gfPop.fPopCalendar(document.forma.fecfin);return false;" HIDEFOCUS><img src="<%=BASEURL%>\js\Calendario\cal.gif" width="16" height="16" border="0" alt="De click aqui para escoger la fecha"></a> <img src="<%=BASEURL%>/images/botones/iconos/obligatorio.gif" width="10" height="10"></td>
          </tr>
        </table></td>
      </tr>
    </table>
	<br>
	   <div align="center">
			 <img src="<%=BASEURL%>/images/botones/aceptar.gif"  name="imgaceptar" onClick="campos()" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" style="cursor:hand">&nbsp;
			 <img src="<%=BASEURL%>/images/botones/cancelar.gif"  name="imgcancelar"  onMouseOver="botonOver(this);" onClick="window.location='<%=CONTROLLER%>?estado=Menu&accion=Cargar&pagina=clienteactividad.jsp&carpeta=/jsp/trafico/clienteactividad&marco=no'" onMouseOut="botonOut(this);" style="cursor:hand"> &nbsp;
             <img src="<%=BASEURL%>/images/botones/salir.gif"  name="imgsalir"  onMouseOver="botonOver(this);" onClick="parent.close();" onMouseOut="botonOut(this);" style="cursor:hand">
       </div>
 </form>
</div>
 <iframe width=188 height=166 name="gToday:normal:agenda.js:gfPop:plugins.js" id="gToday:normal:agenda.js:gfPop:plugins.js" src="<%=BASEURL%>/js/Calendario/ipopeng.htm" scrolling="no" frameborder="0" style="visibility:visible; z-index:999; position:absolute; left:-500px; top:0px;"> </iframe>
</body>
</html>
