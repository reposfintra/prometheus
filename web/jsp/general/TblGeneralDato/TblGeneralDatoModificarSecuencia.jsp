<!--
- Autor : Ing. Armando Oviedo C
- Date  : 21 de Diciembre de 2005
- Copyrigth Notice : Fintravalores S.A. S.A
-->
<%--
-@(#)
--Descripcion : Pagina JSP, que modifica o anula un item de codigo tabla dato
--%>

<%-- Declaracion de librerias--%>
<%@page contentType="text/html"%>
<%@page session="true"%>
<%@page errorPage="../error/ErrorPage.jsp"%>
<%@page import="com.tsp.operation.model.*"%>
<%@page import="com.tsp.util.*"%>
<%@page import="com.tsp.operation.model.beans.*, java.util.*"%>
<%@include file="/WEB-INF/InitModel.jsp"%>
<%@ taglib uri="http://jakarta.apache.org/taglibs/input-0.90" prefix="input" %>
<%
	Vector tmp = model.tblgendatosvc.getTodosTablaGeneralDato();
	String codtabla = request.getParameter("codtabla");
	String secuencia = request.getParameter("secuenciasel");
%>
<html>
<head>
<title>Modificar Secuencia Tabla General Dato</title>
<link href="<%= BASEURL %>/css/estilostsp.css" rel="stylesheet" type="text/css">
<link href="../../../css/estilostsp.css" rel="stylesheet" type="text/css"> 
<script type='text/javascript' src="<%= BASEURL %>/js/boton.js"></script>
<script type='text/javascript' src="<%= BASEURL %>/js/TblGeneralDato.js"></script>
</head>
<body onload = " <% if(request.getParameter("reload") != null ){ %> window.opener.location.reload(); <%}%>">
<form name="forma" action="<%=CONTROLLER%>?estado=TblGeneralDato&accion=Modificar&modsec=ok&codtabla=<%=codtabla%>&secuencia=<%=secuencia%>" method="post">
<table width="100%" align="center" border="2">
	<tr>
		<td class="subtitulo1">C�digo Tabla - <%=codtabla%></td>
 	</tr>
	<tr>
		<td class="fila">
			<table>
				<tr>
					<td class="bordereporte">
						<select name="secuencia" size="10" id="secuencia" multiple>
							<%
								for(int i=0;i<tmp.size();i++){
									TblGeneralDato tgd = (TblGeneralDato)(tmp.elementAt(i));%>
									<option value="<%=tgd.getSecuencia()%>" <%=(tgd.getSecuencia().equalsIgnoreCase(secuencia))? "selected" : "" %>><%=tgd.getLeyenda()%></option>
								<%}
							%>
						</select>			  
					</td>
					<td>
						<img title='Subir' src="<%= BASEURL %>/images/botones/envArriba.gif" onmouseover="botonOver(this);" onmouseout="botonOut(this);" onClick="subir()"></img><br>
						<img title='Bajar' src="<%= BASEURL %>/images/botones/envAbajo.gif" onmouseover="botonOver(this);" onmouseout="botonOut(this);" onClick="bajar()"></img>
					</td>
			  </tr>		
				<tr class="fila" >
					<td>
					</td>
			  </tr>
	  </table>	  </td>
	</tr>
</table>
<br>
<table align="center">
	<tr>		
		<td>
			<img title='Aceptar' src="<%= BASEURL %>/images/botones/aceptar.gif" onmouseover="botonOver(this);" onmouseout="botonOut(this);" onClick="selectAll();"></img>
			<img title='Salir' src="<%= BASEURL %>/images/botones/salir.gif" onmouseover="botonOver(this);" onmouseout="botonOut(this);" onClick="window.close()"></img>			
		</td>
	</tr>
</table>
</form>
</body>
</html>