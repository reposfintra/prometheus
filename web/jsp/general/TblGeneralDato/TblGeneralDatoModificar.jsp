<!--
- Autor : Ing. Armando Oviedo C
- Date  : 21 de Diciembre de 2005
- Copyrigth Notice : Fintravalores S.A. S.A
-->
<%--
-@(#)
--Descripcion : Pagina JSP, que modifica o anula un item de codigo tabla dato
--%>

<%-- Declaracion de librerias--%>
<%@page contentType="text/html"%>
<%@page session="true"%>
<%@page errorPage="../error/ErrorPage.jsp"%>
<%@page import="com.tsp.operation.model.*"%>
<%@page import="com.tsp.util.*"%>
<%@page import="com.tsp.operation.model.beans.*, java.util.*"%>
<%@include file="/WEB-INF/InitModel.jsp"%>
<%@ taglib uri="http://jakarta.apache.org/taglibs/input-0.90" prefix="input" %>
<html>
<head>
<title>Modificar Item Tabla General Dato</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link href="<%= BASEURL %>/css/estilostsp.css" rel="stylesheet" type="text/css">
<link href="../../../css/estilostsp.css" rel="stylesheet" type="text/css"> 
<script type='text/javascript' src="<%= BASEURL %>/js/boton.js"></script>
<script type='text/javascript' src="<%= BASEURL %>/js/validar.js"></script>
</head>
<body onload="window.opener.location.reload();redimensionar();" onresize="redimensionar()">
<div id="capaSuperior" style="position:absolute; width:100%; height:100px; z-index:0; left: 0px; top: 0px;">
	<jsp:include page="/WEB-INF/toptsp.jsp?encabezado=Modificar Item Tabla General Dato"/>
</div>
<div id="capaCentral" style="position:absolute; width:100%; height:87%; z-index:0; left: 0px; top: 100px; overflow: scroll;">
<%
	TblGeneralDato tmp = model.tblgendatosvc.getTablaGeneralDato();	
	String mensajemod = request.getParameter("mensajemod");
	if(mensajemod!=null){
		out.print("<table width=450 border=2 align=center><tr><td><table width=100%  border=1 align=center  bordercolor=#F7F5F4 bgcolor=#FFFFFF><tr><td width=350 align=center class=mensajes>"+mensajemod+"</td><td width=100><img src="+BASEURL+"/images/cuadronaranja.JPG></td></tr></table></td></tr></table>");%>		
		<br>
		<table width='450' align=center>
			<tr>
				<td align=center><img title='Regresar' src="<%= BASEURL %>/images/botones/salir.gif" onmouseover="botonOver(this);" onmouseout="botonOut(this);" onclick="window.close();"></img></td>
			</tr>
		</table>
	<%} 
	else if(tmp==null){
		out.print("<table width=450 border=2 align=center><tr><td><table width=100%  border=1 align=center  bordercolor=#F7F5F4 bgcolor=#FFFFFF><tr><td width=350 align=center class=mensajes>Descuento No Existe</td><td width=100><img src="+BASEURL+"/images/cuadronaranja.JPG></td></tr></table></td></tr></table>");%>
		<br>
		<table width='450' align=center>
			<tr>
				<td align=center><img title='Regresar' src="<%= BASEURL %>/images/botones/salir.gif" onmouseover="botonOver(this);" onmouseout="botonOut(this);" onclick="window.close();"></img></td>
			</tr>
		</table>
	<%}
	else{			
%>
<form name="forma" action="<%=CONTROLLER%>?estado=TblGeneralDato&accion=Modificar&mensaje=modificar" method="post">
<input type="hidden" name="codtabla" value="<%=tmp.getCodTabla()%>"></input>
<input type="hidden" name="secuencia" value="<%=tmp.getSecuencia()%>"></input>
  <table width="45%"  border="2" align="center">
  	<tr>
    	<td>
			<table width="100%" align="center">
				<tr>
					<td width="40%" class="subtitulo1" nowrap>Datos</td>
					<td width="60%" class="barratitulo"><img src="<%=BASEURL%>/images/titulo.gif" width="32" height="20"></td>
				</tr>
				<tr class="fila">
				  <td>C&oacute;digo Tabla </td>
				  <td><%=tmp.getCodTabla()%></td>
			  </tr>
				<tr class="fila">
				  <td>Secuencia</td>
				  <td nowrap>
				  		<%=tmp.getSecuencia()%>&nbsp &nbsp;</img>
  				  </td>
				</tr>
				<tr class="fila">
				  <td>Leyenda</td>
				  <td>
						<input type="text" name="leyenda" value="<%=tmp.getLeyenda()%>">
						<img src="<%=BASEURL%>/images/botones/iconos/obligatorio.gif" width="10" height="10"></img>
				  </td>
				</tr>
				<tr class="fila">
				  <td>Tipo</td>
				  <td>
				  	<input:select name="tipo" options="<%=model.tblgendatosvc.getTiposDatos()%>" default="<%=(tmp.getTipo()!=null ? tmp.getTipo(): "")%>"/>				  	
				   	<img src="<%=BASEURL%>/images/botones/iconos/obligatorio.gif" width="10" height="10"></img>
				  </td>
				</tr>						
				<tr class="fila">
				  <td>Longitud</td>
				  <td>
				  		<input type="text" name="longitud" value="<%=tmp.getLongitud()%>">
						<img src="<%=BASEURL%>/images/botones/iconos/obligatorio.gif" width="10" height="10"></img>
				</td>
				</tr>		
		  </table>
  		</td>
  	</tr>
  </table><br>
  <table align=center width='450'>
    <tr>
        <td align="center" colspan="2">
            <img title='Modificar' src="<%= BASEURL %>/images/botones/modificar.gif" onmouseover="botonOver(this);" onmouseout="botonOut(this);" onclick="TCamposLlenos();"></img>                        
			<img title='Eliminar' src="<%= BASEURL %>/images/botones/eliminar.gif" onmouseover="botonOver(this);" onmouseout="botonOut(this);" onclick="document.forma.action='<%=CONTROLLER%>?estado=TblGeneralDato&accion=Modificar&mensaje=eliminar';TCamposLlenos();"></img>
            <img title='Salir' src="<%= BASEURL %>/images/botones/salir.gif" onmouseover="botonOver(this);" onmouseout="botonOut(this);" onClick="window.close()"></img>
        </td>
    </tr>
  </table>
  
  </form>
  <br>
  
  <%}%>  
</div>
</body>
</html>
