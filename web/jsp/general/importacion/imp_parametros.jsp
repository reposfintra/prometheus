<!--
     - Author(s)       :      MARIO FONTALVO
     - Date            :      10/10/2005  
     - Copyright Notice:      TRANSPORTES SANCHEZ POLO S.A.
-->
<%--   
     - @(#)  
     - Description: Definicion general de los parametros de Importacion
--%> 

<%@page session="true"%> 
<%@page errorPage="/error/ErrorPage.jsp"%>
<%@page import="com.tsp.operation.model.beans.*" %>
<%@page import="java.util.*" %>
<%@include file="/WEB-INF/InitModel.jsp"%>
<%
   ImportacionParametros dt = (ImportacionParametros)  model.ImportacionSvc.getDatos();
   String Opcion = ( dt!=null ? "Parametros-Modificar" : "Parametros-Grabar" );
   String Mensaje = (request.getParameter("Mensaje")!=null?request.getParameter("Mensaje"):"");
%>


<html>
    <head>
        <title>Importaciones</title>
        <link href="<%= BASEURL %>/css/estilostsp.css" rel="stylesheet">
        <script src="<%= BASEURL %>/js/boton.js"></script>	  
        <script src="<%= BASEURL %>/js/validacionesImportacion.js"></script>
        <script>
          <!--
           // Maximizar Ventana por Nick Lowe (nicklowe@ukonline.co.uk)

           window.moveTo(0,0);
           if (document.all) {
                 top.window.resizeTo(screen.availWidth,screen.availHeight);
           }
           else if (document.layers||document.getElementById) {
                   if(top.window.outerHeight<screen.availHeight||top.window.outerWidth<screen.availWidth){
                   top.window.outerHeight = screen.availHeight;
                   top.window.outerWidth = screen.availWidth;
                   }
           }
        //-->        
        </script>
    </head>

    <body>
        
        <div id="capaSuperior" style="position:absolute; width:100%; height:100px; z-index:0; left: 0px; top: 0px;">
            <jsp:include page="/toptsp.jsp?encabezado=Importacion - Definicion de Parametros Generales"/>
        </div>
        <div id="capaCentral" style="position:absolute; width:100%; height:87%; z-index:0; left: 0px; top: 100px; overflow: scroll;">
		<center>
            <br>
            <!-- Marco de la tabla principal del formulario -->
            <form action="<%= CONTROLLER %>?estado=Opciones&accion=Importacion" method="post" name="fpar">
                <table width="500" border="2" align="center">
                    <tr><td align="center">
    
                        <!-- cabecera de la seccion -->
                        <table  width="100%" align="center" class="tablaInferior" >
                            <tr>
                                <td class="subtitulo1"  width="70%">Tabla Destinos</td>
                                <td class="barratitulo" width="30%"><img src="<%= BASEURL %>/images/titulo.gif" width="32" height="20" align='left'></td>
                            </tr>
                        </table>		
                        <!-- fin cabecera -->	
			
	
                        <table  width="100%" align="center" class="tablaInferior" >
							<tr class="fila">
                                <td width="30%" >Formato</td>
                                <td width="70%" ><input name="formato" class="textbox" style="width:49%" size="40" maxlength="40" readonly></td>
                            </tr>						
                            <tr class="fila">
                                <td width="30%" >Tabla</td>
                                <td width="70%" ><input name="ntabla" class="textbox" style="width:49%" size="40" maxlength="40" readonly ></td>
                            </tr>
                            <tr class="fila">
                                <td width="30%" >Descripcion</td>
                                <td width="70%" ><input name="dtabla" class="textbox" style="width:98% " maxlength="40"></td>
                            </tr>  
                        </table>
                        
                    </td>
                    </tr>
                </table>
                <span class='informacion'>Definicion del nombre de la tabla de Importacion y descripcion de la misma.</span>
                <br>
                <br>        
                <table width="500" border="2" align="center" style='visibility:hidden; position:absolute;'>
                    <tr><td align="center">

                        <table  width="100%" align="center" class="tablaInferior"   >
                        <tr>
                        <td class="subtitulo1"  width="70%">Atributos del Nombre de la Tabla Destino</td>
                        <td class="barratitulo" width="30%"><img src="<%= BASEURL %>/images/titulo.gif" width="32" height="20"></td>
                    </tr>
                    </table>		
                    <!-- fin cabecera -->	

                    <table width="100%" align="center" class="tablaInferior"  >
                        <tr class="fila">
                            <td width="30%">Intercambiar Nombre</td>
                            <td width="70%">
                                <select name="ctab" class="listmenu" style="width:99%">
                                <option value="A">Por el nombre del archivo Origen</option>
                                <option value="T" selected>Mantener nombre de la Tabla</option>				   
                                </select>
                            </td>
                        </tr>
                        <tr class="fila">
                            <td width="30%">Adicionar al Nombre</td>
                            <td width="70%">
                                <select name="fcon" class="listmenu" style="width:99%">
                                <option value="A" >Fecha del Archivo</option>
                                <option value="S" >Fecha Actual</option>
                                <option value="N" selected>Ninguno</option>				   
                                </select>
                            </td>
                        </tr>				
                    </table>
	
                    </tr>
                </table><br>
                <table width="500" border="2" align="center">
                    <tr><td align="center">
	
                        <!-- cabecera de la seccion -->
                        <table  width="100%" align="center" class="tablaInferior"  >
                            <tr>
                                <td class="subtitulo1"  width="70%">Actualizaci&oacute;n de Datos de la Tabla Destino</td>
                                <td class="barratitulo" width="30%"><img src="<%= BASEURL %>/images/titulo.gif" width="32" height="20"></td>
                            </tr>
                        </table>		
                        <!-- fin cabecera -->	
		

                        <table  width="100%" align="center" class="tablaInferior"  >		
                            <tr class="fila">
                                <td width="30%">Operaciones</td>
                                <td width="70%">
                                    <select name="adat" class="listmenu" style="width:99%">
                                    <option value="A" selected>Solo Adicionar datos en la tabla Destino</option>
                                    <option value="E">Eliminar y Adicionar Datos en la tabla Destino</option>
                                    <option value="U">Eliminar y Adicionar Datos segun el Usuario</option>
                                    </select>
                                </td>
                            </tr>
                            <tr class="fila">
                                <td width="30%">Insercion</td>
                                <td width="70%">
                                    <select name="insert" class="listmenu" style="width:99%">
                                    <option value="S" selected>Si, permite insercion sobre la tabla</option>
                                    <option value="N" >No, permite insercion sobre la tabla</option>
                                    </select>
                                </td>
                            </tr>
                            <tr class="fila">
                                <td width="30%">Actualizacion</td>
                                <td width="70%">
                                    <select name="update" class="listmenu" style="width:99%">
                                    <option value="S" selected>Si, permite actualizaciones sobre la tabla</option>
                                    <option value="N" >No, permite actualizaciones sobre la tabla</option>
                                    </select>
                                </td>
                            </tr>												
                        </table>

                    </tr>
                </table>
                <span class='informacion'>Operaciones sobre los datos ya existentes en la tabla</span>
                <br><br><br>
                <table width="500" border="2" align="center">
                    <tr><td align="center">

	
                        <!-- cabecera de la seccion -->
                        <table  width="100%" align="center" class="tablaInferior"  >
                            <tr>
                                <td class="subtitulo1"  width="70%">Localizacion del Archivo Origen</td>
                                <td class="barratitulo" width="30%"><img src="<%= BASEURL %>/images/titulo.gif" width="32" height="20"></td>
                            </tr>
                        </table>		
                        <!-- fin cabecera -->	

                        <table  width="100%" align="center" class="tablaInferior"  >		
                            <tr class="fila" >
                                <td width="30%" title='Indique la manera de asociar su archivo de importacion con el nombre de la tabla en la base de datos'>Por Nombre </td>
                                <td width="70%">
                                    <select name="bpna" class="listmenu" style="width:99%">
                                    <option value="C">Segun nombre de exacto del FORMATO</option>
                                    <option value="I" selected>Aquellos que inicien por el nombre del FORMATO</option>
                                    </select>
                                </td>
                            </tr>	
                            <tr class="fila" >
                                <td width="30%" title='Indique si el archivo que usted importará contiene o no titulos en la cabecera, recuerde que estos deben ser coherentes con los nombre de del los campos de la tablas en la base de datos'>Contiene titulos</td>
                                <td width="70%">
                                    <select style="width:70" name="titulo" >
                                        <option value='S' selected>Si </option>
                                        <option value='N'>No </option>
                                    </select>
                                </td>
                            </tr>	
		
                            <tr class="fila">
                                <td width="30%" title='Nombre completo del thread que se ejecutará una vez termine el proceso de importacion' >Evento Especial</td>
                                <td width="70%" title='Nombre completo del thread que se ejecutará una vez termine el proceso de importacion'>
                                    <input type='text' name='evento' style='width:100%' >
                                </td>
                            </tr>

                        </table>
                    </td></tr>
                </table>
                <span class='informacion'>Manera de asociar el archivo de importacion a la tabla de la base de datos</span>
                <br>
                <br>
                <img name="imgGrabar" src="<%=BASEURL%>/images/botones/aceptar.gif" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" style="cursor:hand" onClick=" fpar.submit(); " accion="Grabar">
&nbsp;
<img name="imgSalir" src="<%=BASEURL%>/images/botones/salir.gif" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" style="cursor:hand" onClick=" window.close(); " ><input type="hidden" name="Opcion" value="<%= Opcion %>"> 
            <input name="ctab" type="hidden" value="A">
			<input name="fcon" type="hidden" value="I">
			</form>
            <!-- fin Marco general -->
            
			<% if (dt!=null ) {%>
			<br>
			<a style="cursor:hand; color:red" href="<%= CONTROLLER %>?estado=Opciones&accion=Importacion&Opcion=Estructura-Buscar&formato=<%= dt.getFormato() %>" >Estructura Importacion</a>   | <a style="cursor:hand; color:red" href="<%= CONTROLLER %>?estado=TablaGen&accion=Manager&cmd=mng&type=TFORMATOS"	>Usuarios de Importacion</a>			
			<% } %>
			
			
 <% if (!Mensaje.equals("")) { %>
      <br>
    
        <table border="2" align="center">
           <tr>
           <td><table width="100%"  border="1" align="center"  bordercolor="#F7F5F4" bgcolor="#FFFFFF">
                <tr>
                 <td width="320" align="center" class="mensajes"><%=Mensaje%></td>
                 <td width="29" background="<%=BASEURL%>/images/cuadronaranja.JPG">&nbsp;</td>
                 <td width="58">&nbsp;</td>
                </tr>
              </table>
           </td>
           </tr>
        </table>  
        <% } %>
        </center>
        </div>
		
    </body>
</html>
<script>
<% if (dt!=null) { %>

    fpar.formato.value = '<%= dt.getFormato() %>';
    fpar.ntabla.value = '<%= dt.getTabla() %>';
    fpar.formato.readOnly = true;
    fpar.ntabla.readOnly = true;
    fpar.dtabla.value = '<%= dt.getDescripcion() %>';
      
    fpar.ctab.value    = '<%= dt.getCTAB() %>';
    fpar.fcon.value    = '<%= dt.getFCON() %>';
    fpar.adat.value    = '<%= dt.getADAT() %>';
    fpar.bpna.value    = '<%= dt.getBPNA() %>';
    fpar.titulo.value  = '<%= dt.getTitulo() %>';
    fpar.evento.value  = '<%= dt.getEvento() %>';
	fpar.insert.value  = '<%= dt.getInsert() %>';
	fpar.update.value  = '<%= dt.getUpdate() %>';
      
<% } %>
</script>