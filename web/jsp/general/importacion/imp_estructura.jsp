<!--
     - Author(s)       :      MARIO FONTALVO
     - Date            :      10/10/2005  
     - Copyright Notice:      TRANSPORTES SANCHEZ POLO S.A.
-->
<%--   
     - @(#)  
     - Description: Listado de campos de una tabla de importacion.
--%> 
<%@page session="true"%> 
<%@page errorPage="/error/ErrorPage.jsp"%>
<%@page import="com.tsp.operation.model.beans.*" %>
<%@page import="java.util.*" %>
<%@include file="/WEB-INF/InitModel.jsp"%>
<%
   ImportacionParametros dt = (ImportacionParametros)  model.ImportacionSvc.getDatos();
   String Opcion = ( dt!=null ? "Estructura-Modificar" : "Estructura-Grabar" );
   String Mensaje = (request.getParameter("Mensaje")!=null?request.getParameter("Mensaje"):"");
   String Archivo = (request.getParameter("Archivo")!=null?request.getParameter("Archivo"):"");
   String Op = request.getParameter("Op");
   String ntabla  = (request.getParameter("ntabla")!=null?request.getParameter("ntabla"):"");
   String formato = (request.getParameter("formato")!=null?request.getParameter("formato"):"");
   Usuario user   = (Usuario) session.getAttribute("Usuario");
%>

<html>
<head>
    
    <title>Estructura Importacion</title></head>
    <link  href="<%= BASEURL %>/css/estilostsp.css" rel="stylesheet">
    <script src="<%= BASEURL %>/js/boton.js"></script>	  	
    <script src="<%= BASEURL %>/js/validacionesImportacion.js"></script>
    <script> 
        BASEURL    = '<%= BASEURL    %>'; 
        CONTROLLER = '<%= CONTROLLER %>'; 
        <!--
           // Maximizar Ventana por Nick Lowe (nicklowe@ukonline.co.uk)

           window.moveTo(0,0);
           if (document.all) {
                 top.window.resizeTo(screen.availWidth,screen.availHeight);
           }
           else if (document.layers||document.getElementById) {
                   if(top.window.outerHeight<screen.availHeight||top.window.outerWidth<screen.availWidth){
                   top.window.outerHeight = screen.availHeight;
                   top.window.outerWidth = screen.availWidth;
                   }
           }
        //-->
    </script>
    
</head>
<body onresize="redimensionar()" onload = 'redimensionar()'>
<div id="capaSuperior" style="position:absolute; width:100%; height:100px; z-index:0; left: 0px; top: 0px;">
 <jsp:include page="/toptsp.jsp?encabezado=Importacion - Estructura"/>
</div>
<div id="capaCentral" style="position:absolute; width:100%; height:87%; z-index:0; left: 0px; top: 100px; overflow: scroll;">
    <center>
	<br>
	
	<form action="<%= CONTROLLER %>?estado=Opciones&accion=Importacion" method="post" name="fest">
	<table width="<%= (dt==null?380:1200) %>" border="2" align="center">
	<tr>
		<td align="center">
		
		<!-- cabecera de la seccion -->
		<table width="100%" align="center" class="tablaInferior" >
			<tr>
			<td class="subtitulo1"  width="50%">Datos Basicos</td>
			<td class="barratitulo" width="50%">
			  <img src="<%= BASEURL %>/images/titulo.gif" width="32" height="20" align="left">
			</td>
			</tr>
		</table>
		<!-- fin cabecera -->
		
		

		<table width="100%" align="center" >
			<tr class="filaresaltada">
			    <td width="10%" >&nbsp;Formato</td>
			    <td width="40%" >
                   <input name='formato' type='text' class='textbox'  style="width:180" size="40" maxlength="40" >
                   <% if (dt==null){ %>
			       <img name="imgBuscar" src="<%=BASEURL%>/images/botones/iconos/lupa.gif"  width='18' style="cursor:hand" onClick=" if (fest.formato.value!='') { goEdicion2(fest.formato.value,'Estructura-Buscar'); } " title="Consulta un formato ya definido en el sistema">
			       <% } %>
			    </td>
			</tr>
			
					
			<tr class="filaresaltada">
			    <td width="10%" >&nbsp;Tabla</td>
			    <td width="40%" >
				   <input name='ntabla' type='text' class='textbox'  style="width:180" size="40" maxlength="40" >
				   <% if (dt==null){ %>			       
			       <img name="imgListar" src="<%=BASEURL%>/images/botones/iconos/clasificar.gif"  width='22' style="cursor:hand" onClick=" encontrarTabla(); " title="Muestra las tablas del sistema.">
			       <% } %>
			    </td>
			</tr>
			<% if (dt!=null){ %>
			<tr class="filaresaltada">
			    <td >&nbsp;Descripcion</td>
		            <td ><input name='dtabla' type='text' class='textbox' style="width:40%" size="40" maxlength="40"></td>
			</tr>		
			<% } %>
		 </table>

		<% if (dt!=null){ %>
		 
		<!-- cabecera de la seccion -->
		<table width="100%"  >
			<tr>
			<td class="subtitulo1"  width="50%">Campos Tabla</td>
			<td class="barratitulo" width="50%">
			  <img src="<%= BASEURL %>/images/titulo.gif" width="32" height="20">
			</td>
			</tr>
		</table>		
		<!-- fin cabecera -->		

		<table width="100%" border="1" bordercolor="#999999" bgcolor="#F7F5F4" id='ListaCampos'>
		
			<tr class="tblTitulo">
		        <th colspan='3'>Tablas</th>
			    <th colspan='7'>Parametros Importacion</th>
			</tr>		
			<tr class="tblTitulo">
  			    <th width="2%"  >pk</th>
		        <th width="16%" >Campo</th>
			    <th width="18%" >Tipo</th>
			    <th width="5%"  >Default</th>
			    <th width="12%" >Tipo Default</th>
			    <th width="14%" >Valor Default</th>
				<th width="16%" >Alias</th>
				<th width="7%"  >Valid.</th>
				<th width="7%"  >Inser.</th>
				<th width="4%"  >Update</th>
			</tr>
		 </table>
		<% } %>
			
		</td>
	</tr>
	</table>
	<br>
   <% if (dt==null){ %>
   <img name="imgAceptar"   src="<%=BASEURL%>/images/botones/aceptar.gif" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" style="cursor:hand" onClick=" if(fest.formato.value!='' && fest.ntabla.value!=''){ fest.Opcion.value='Estructura-Incluir'; fest.submit(); } else {  alert('Indique el nombre del formato y la tabla para poder continuar'); }">	   
      &nbsp;   
   <% } else { %>
   <img name="imgGrabar"   src="<%=BASEURL%>/images/botones/modificar.gif" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" style="cursor:hand" onClick=" enviar(fest);">	   
      &nbsp;
   <img name="imgEliminar" src="<%=BASEURL%>/images/botones/eliminar.gif" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" style="cursor:hand" onClick= "goEdicion2('<%= formato %>','Estructura-Eliminar'); ">	   
      &nbsp;      
   <img name="imgExportar" src="<%=BASEURL%>/images/botones/exportarExcel.gif" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" style="cursor:hand" onClick= "goEdicion2('','Estructura-FormatoArchivo'); ">	   
      &nbsp;             
   <img name="imgValidar"  src="<%=BASEURL%>/images/botones/validar.gif" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" style="cursor:hand" onClick= "goEdicion2('<%= formato %>','Estructura-Recargar'); ">	   
      &nbsp;       
      
   <% } %>      
   <% if (dt!=null || Op!=null){ %>
   <img name="imgRestablecer" src="<%=BASEURL%>/images/botones/restablecer.gif" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" style="cursor:hand" onClick=" goEdicion2('','Estructura-Reset');">	   
      &nbsp;      
   <% } %>   
   <img name="imgSalir" src="<%=BASEURL%>/images/botones/salir.gif" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" style="cursor:hand" onClick=" window.close(); ">
   
   <input type="hidden" name="Opcion" value="<%= Opcion %>"> 
 </form>
 
 
   <% if (dt!=null) {%>
   <br>
   <span class='informacion'>
       <b>Nota</b>: para validar la tabla actual contra modificaciones de la base de datos por favor presione el boton validar. <br>
       Para descargar el formato del archivo de importacion para esta tabla utilice el boton exportar.
   </span><br><br>
	<a style="cursor:hand; color:red" href="<%= CONTROLLER %>?estado=Opciones&accion=Importacion&Opcion=Parametros-Buscar&formato=<%= dt.getFormato() %>" >Parametros de Importacion</a>   | <a style="cursor:hand; color:red" href="<%= CONTROLLER %>?estado=TablaGen&accion=Manager&cmd=mng&type=TFORMATOS"	>Usuarios de Importacion</a>
   <% } else { %>
   <br>
   <span class='informacion'>
       <b>Nota</b>: para buscar el formato haga click en el icono 
           <img src="<%=BASEURL%>/images/botones/iconos/lupa.gif"        width='18' >,
           Si lo que desea es agregar un nuevo formato Defina el nombre del formato y la tabla <br>
           luego haga click en el boton ACEPTAR.
		   Para ver las tablas del sistema haga click en el icono
           <img src="<%=BASEURL%>/images/botones/iconos/clasificar.gif"  width='22' > .
    </span>   
   <% } %>
 
 
 <% if (!Mensaje.equals("")) { %>
      <br><br>
    
        <table border="2" align="center">
           <tr>
           <td><table width="100%"  border="1" align="center"  bordercolor="#F7F5F4" bgcolor="#FFFFFF">
                <tr>
                 <td width="320" align="center" class="mensajes"><%=Mensaje%></td>
                 <td width="29" background="<%=BASEURL%>/images/cuadronaranja.JPG">&nbsp;</td>
                 <td width="58">&nbsp;</td>
                </tr>
              </table>
           </td>
           </tr>
      </table>  
<% } %>

<% if (Archivo.equals("ok")) { %>
        <br><br>
        
        <img style="cursor:hand " src="<%= BASEURL %>/images/file.gif" width="48" height="48" align="absmiddle" onClick="window.open('<%= CONTROLLER %>?estado=Ver&accion=Directorio&usuario=<%= user.getLogin() %>&evento=LOAD','','scroll=no,resizable=yes');">
        <br>
        <span class='informacion'>Haga sobre la imagen para poder consultar sus archivos y busque el siguiente archivo <%= dt.getFormato() + "_FORMATO.xls" %></span>

<% } %>
 
</center>
</div>
</body>
</html>


<script>
<% if (dt!=null) { %>
      fest.formato.value = '<%= dt.getFormato() %>';	  
      fest.ntabla.value  = '<%= dt.getTabla  () %>';
      fest.formato.readOnly = true;
      fest.ntabla.readOnly = true;
      fest.dtabla.value = '<%= dt.getDescripcion() %>';

      <% Iterator it = dt.getListaCampos().iterator(); 
         while (it.hasNext()){
             ImportacionEstructura campo = (ImportacionEstructura) it.next();
      %>
            nuevoCampo ("<%= campo.isPrimaryKey()  %>",
						"<%= campo.getCampo()      %>",
                        "<%= campo.getTipo()       %>",
                        "<%= campo.getDefault()    %>", 
                        "<%= campo.getVariable()   %>", 
                        "<%= campo.getConstante()  %>",
                        "<%= campo.getAlias()      %>",
                        "<%= campo.getValidacion() %>",
                        "<%= campo.getInsercion()  %>",
						"<%= campo.isAplicaUpdate()%>",
						"<%= campo.getObs_validacion()%>",
						"<%= campo.getObs_insercion ()%>"
                        );
      <% } %>
<% } else { %>
      fest.formato.value = '<%= formato %>';
      fest.ntabla.value =  '<%= ntabla  %>';	  
<%} %>      

</script>
