<!--  
     - Author(s)       :      MARIO FONTALVO
     - Date            :      25/01/2006
     - Copyright Notice:      TRANSPORTES SANCHEZ POLO S.A.
-->
<%--   
     - @(#)  
     - Description: Importacion Directa de un archivo
--%> 
<%@include file="/WEB-INF/InitModel.jsp"%>
<html>
<head>
<title>Descripcion de Campos de Fromualrio de Importacion de Archivos</title>
<link href="../../../css/estilostsp.css" rel="stylesheet" type="text/css">
<link href="<%= BASEURL %>/css/estilostsp.css" rel="stylesheet" type="text/css">
</head>
<body>
<BR>
<table width="696"  border="2" align="center">
  <tr>
    <td width="811" >
      <table width="100%" border="0" align="center">
        <tr  class="subtitulo">
          <td width="674" height="24"><div align="center">IMPORTACION DE ARCHIVOS </div></td>
        </tr>
		<tr class="subtitulo1">
          <td> DATOS DEL ARCHIVO</td>
        </tr>
		<tr class="ayudaHtmlTexto">
		<td>Formulario de Importacion del archivos.<br>
		    <div align="center">
            <img src="<%= BASEURL %>/images/ayuda/importacion/dibujo01.JPG" align="absmiddle">	
			</div><br>	
		    
			<div align="center">
		    <img src="<%= BASEURL %>/images/ayuda/importacion/dibujo02.JPG" align="absmiddle" >	
			</div><br>
		      Tipo de proceso a realizar indicar� si los datos de importaci&oacute;n ser�n tomados para realizar 
		    grabaciones o actualizaciones sobre las tablas de la base de datos. <br>
		    <br>
		    Recuerde que el resultado del proceso depender� en gran parte de la consistencia del archivo
	      de importacion , el nombre de la hoja a procesar deber� ser 'BASE', en caso contrario este no reconocer&aacute; el origen de los datos y lo mas probable es que no se ejecute ningun proceso.<br>
	      <br>
	      Una vez ejecutado el proceso este le mostrar&aacute; un mensaje de 'inicio de proceso' y el archivo a importar se subir&aacute; en el directorio WEB del usuario para ser procesado.<br>
	      <br>
			<div align="center">
		    <img src="<%= BASEURL %>/images/ayuda/importacion/dibujo03.JPG" align="absmiddle" >	
			</div><br>
	      Internamente este genera un archivo de Importacion llamado 'logImportacion' + la fecha del sistema en el siguiente formato 'YYYYMMDD_HHMISS', en este se almacena el estado del proceso, para efectos de verificacion del proceso. Adicional a esto en caso de tener errores este generar&aacute; un archivo adicional que contendra los registros no montados para el caso de grabaci&oacute;n o un archivo que contenga el estado de los registros procesados en caso de ser una actualizacion. <br>	      <br>
			<div align="center">
		    <img src="<%= BASEURL %>/images/ayuda/importacion/dibujo04.JPG" align="absmiddle" >	
			</div><br>
			<br>
			Si desconoce el formato del archivo de Importacion que usted desea procesar utilice la opcion de exportar de la vista de desfinicon de estructura de la tabla de Importacion. Para llegar a ella siga el siguiente proceso.<br>			<br>
			<div align="center">
		    <img src="<%= BASEURL %>/images/ayuda/importacion/dibujo05.JPG" align="absmiddle" >	
			</div><br>				
			<div align="center">
		    <img src="<%= BASEURL %>/images/ayuda/importacion/dibujo06.JPG" align="absmiddle" >	
			</div><br>	
			<div align="center">
		    <img src="<%= BASEURL %>/images/ayuda/importacion/dibujo07.JPG" align="absmiddle" >	
			</div><br>												
		  </td>
		</tr>
      </table>
    </td>
  </tr>
</table>
</body>
</html>
