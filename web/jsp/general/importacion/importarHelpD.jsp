<!--  
     - Author(s)       :      MARIO FONTALVO
     - Date            :      25/01/2006
     - Copyright Notice:      TRANSPORTES SANCHEZ POLO S.A.
-->
<%--   
     - @(#)  
     - Description: Importacion Directa de un archivo
--%> 
<%@include file="/WEB-INF/InitModel.jsp"%>
<html>
<head>
<title>Descripcion de Campos de Fromualrio de Importacion de Archivos</title>
<link href="../../../css/estilostsp.css" rel="stylesheet" type="text/css">
<link href="<%= BASEURL %>/css/estilostsp.css" rel="stylesheet" type="text/css">
</head>
<body>
<BR>
<table width="696"  border="2" align="center">
  <tr>
    <td width="811" >
      <table width="100%" border="0" align="center">
        <tr  class="subtitulo">
          <td height="24" colspan="2"><div align="center">IMPORTACION DE ARCHIVOS </div></td>
        </tr>
		<tr class="subtitulo1">
          <td colspan="2"> DATOS DEL ARCHIVO</td>
        </tr>
        <tr>
          <td width="172" class="fila"> TIPO DE PROCESO </td>
          <td width="502"  class="ayudaHtmlTexto">Seleccion del proceso de Importacion a ejecutar, esta tiene dos posibilidades : Insercion y Actualizacion de Datos.</td>
        </tr>
        <tr>
          <td  class="fila">SELECCION DE ARCHIVO</td>
          <td  class="ayudaHtmlTexto">Campo que indica el origen del Archivo a Importar.</td>
        </tr>
        <tr>
          <td class="fila">Boton Aceptar</td>
          <td  class="ayudaHtmlTexto">Boton para indicar el inicio del proceso.</td>
        </tr>
      </table>
    </td>
  </tr>
</table>
</body>
</html>
