<%@page session="true"%> 
<%@page errorPage="/error/ErrorPage.jsp"%>
<%@page import="com.tsp.operation.model.beans.*" %>
<%@page import="java.util.*" %>
<%@include file="/WEB-INF/InitModel.jsp"%>
<%
   ImportacionParametros dt = (ImportacionParametros)  model.ImportacionSvc.getDatos();
   String Opcion = ( dt!=null ? "Parametros-Modificar" : "Parametros-Grabar" );
%>


<html>
<head>
  <title>Importaciones</title>
  <link href="<%= BASEURL %>/css/estilostsp.css" rel="stylesheet">
  <script src="<%= BASEURL %>/js/boton.js"></script>	  
  <script src="<%= BASEURL %>/js/validacionesImportacion.js"></script>
</head>

<body>
<center>
<br>
<!-- Marco de la tabla principal del formulario -->
<form action="<%= CONTROLLER %>?estado=Opciones&accion=Importacion" method="post" name="fpar">
<table width="500" border="2" align="center">
  <tr><td align="center">
    
    <!-- cabecera de la seccion -->
	<table  width="100%" align="center" class="tablaInferior" >
		<tr>
		<td class="subtitulo1"  width="70%">Tabla Destino</td>
		<td class="barratitulo" width="30%"><img src="<%= BASEURL %>/images/titulo.gif" width="32" height="20"></td>
		</tr>
    </table>		
    <!-- fin cabecera -->	
			
	
	<table  width="100%" align="center" class="tablaInferior" >
		<tr class="fila">
			<td width="30%" >Tabla</td>
			<td width="70%" ><input name="ntabla" class="textbox" style="width:49% "></td>
		</tr>
		<tr class="fila">
			<td width="30%" >Descripcion</td>
			<td width="70%" ><input name="dtabla" class="textbox" style="width:98% "></td>
		</tr>		
<!--		<tr class="filagris">
			<td  colspan="2">&nbsp;</td>
		</tr>			-->
     </table>
	 
	 </tr>
	 </table><br>
     <table width="500" border="2" align="center">
     <tr><td align="center">

	<table  width="100%" align="center" class="tablaInferior"   >
		<tr>
		<td class="subtitulo1"  width="70%">Atributos del Nombre de la Tabla Destino</td>
		<td class="barratitulo" width="30%"><img src="<%= BASEURL %>/images/titulo.gif" width="32" height="20"></td>
		</tr>
    </table>		
    <!-- fin cabecera -->	

    <table width="100%" align="center" class="tablaInferior"  >
		<tr class="fila">
			<td width="30%">Intercambiar Nombre</td>
			<td width="70%">
			     <select name="ctab" class="listmenu" style="width:99%">
				   <option value="A">Por el nombre del archivo Origen</option>
				   <option value="T" selected>Mantener nombre de la Tabla</option>				   
				 </select>
		    </td>
		</tr>
		<tr class="fila">
			<td width="30%">Adicionar al Nombre</td>
			<td width="70%">
			     <select name="fcon" class="listmenu" style="width:99%">
				   <option value="A" >Fecha del Archivo</option>
				   <option value="S" >Fecha Actual</option>
				   <option value="N" selected>Ninguno</option>				   
				 </select>
		    </td>
		</tr>			
<!--		<tr class="filagris">
			<td  colspan="2">&nbsp;</td>
		</tr>			-->
		
    </table>
	
	 </tr>
	 </table><br>
     <table width="500" border="2" align="center">
     <tr><td align="center">
	
	<!-- cabecera de la seccion -->
	<table  width="100%" align="center" class="tablaInferior"  >
		<tr>
		<td class="subtitulo1"  width="70%">Actualizaci&oacute;n de Datos de la Tabla Destino</td>
		<td class="barratitulo" width="30%"><img src="<%= BASEURL %>/images/titulo.gif" width="32" height="20"></td>
		</tr>
    </table>		
    <!-- fin cabecera -->	
		

	<table  width="100%" align="center" class="tablaInferior"  >		
		<tr class="fila">
			<td width="30%">Operaciones</td>
			<td width="70%">
			     <select name="adat" class="listmenu" style="width:99%">
				   <option value="A">Solo Adicionar datos en la tabla Destino</option>
				   <option value="E" selected>Eliminar y Adicionar Datos en la tabla Destino</option>				   
				 </select>
		    </td>
		</tr>	
	</table>

	 </tr>
	 </table><br>
     <table width="500" border="2" align="center">
     <tr><td align="center">

	
	<!-- cabecera de la seccion -->
	<table  width="100%" align="center" class="tablaInferior"  >
		<tr>
		<td class="subtitulo1"  width="70%">Localizacion del Archivo Origen</td>
		<td class="barratitulo" width="30%"><img src="<%= BASEURL %>/images/titulo.gif" width="32" height="20"></td>
		</tr>
    </table>		
    <!-- fin cabecera -->	

	<table  width="100%" align="center" class="tablaInferior"  >		
		<tr class="fila">
			<td width="30%">Por Nombre </td>
			<td width="70%">
			     <select name="bpna" class="listmenu" style="width:99%">
				   <option value="C">Segun nombre de la tabla Destino</option>
				   <option value="I" selected>Aquellos que inicien por el nombre de la Tabla Destino</option>				   
				 </select>
		    </td>
		</tr>		
	</table>
	
    

  </td></tr>
</table>



<br >
<img name="imgGrabar" src="<%=BASEURL%>/images/botones/aceptar.gif" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" style="cursor:hand" onClick=" enviar(fpar); " accion="Grabar">
<input type="hidden" name="Opcion" value="<%= Opcion %>"> 
</form>
<!-- fin Marco general -->
</center>
</body>
</html>



<script>
<% if (dt!=null) { %>
      fpar.ntabla.value = '<%= dt.getTabla() %>';
      fpar.ntabla.readOnly = true;
      fpar.dtabla.value = '<%= dt.getDescripcion() %>';
      
      fpar.ctab.value = '<%= dt.getCTAB() %>';
      fpar.fcon.value = '<%= dt.getFCON() %>';
      fpar.adat.value = '<%= dt.getADAT() %>';
      fpar.bpna.value = '<%= dt.getBPNA() %>';
      
      
      
<% } %>
</script>