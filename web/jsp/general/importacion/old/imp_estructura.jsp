<%@page session="true"%> 
<%@page errorPage="/error/ErrorPage.jsp"%>
<%@page import="com.tsp.operation.model.beans.*" %>
<%@page import="java.util.*" %>
<%@include file="/WEB-INF/InitModel.jsp"%>
<%
   ImportacionParametros dt = (ImportacionParametros)  model.ImportacionSvc.getDatos();
   String Opcion = ( dt!=null ? "Estructura-Modificar" : "Estructura-Grabar" );
%>

<html>
<head>
    
    <title>Estructura Importacion</title></head>
    <link  href="<%= BASEURL %>/css/estilostsp.css" rel="stylesheet">
    <script src="<%= BASEURL %>/js/boton.js"></script>	  	
    <script src="<%= BASEURL %>/js/validacionesImportacion.js"></script>
    <script> BASEURL = '<%= BASEURL %>'; </script>
    
</head>
<body onresize="redimensionar()" onload = 'redimensionar()'>
<div id="capaSuperior" style="position:absolute; width:100%; height:100px; z-index:0; left: 0px; top: 0px;">
 <jsp:include page="/toptsp.jsp?encabezado=Importacion - Estructura"/>
</div>
<div id="capaCentral" style="position:absolute; width:100%; height:87%; z-index:0; left: 0px; top: 100px; overflow: scroll;">
    <center>
	<br>
	<form action="<%= CONTROLLER %>?estado=Opciones&accion=Importacion" method="post" name="fest">
	<table width="700" border="2" align="center">
	<tr>
		<td align="center">
		
		<!-- cabecera de la seccion -->
		<table width="100%" align="center" class="tablaInferior" >
			<tr>
			<td class="subtitulo1"  width="50%">Datos Basicos</td>
			<td class="barratitulo" width="50%">
			  <img src="<%= BASEURL %>/images/titulo.gif" width="32" height="20">
			</td>
			</tr>
		</table>
		<!-- fin cabecera -->

		<table width="100%" align="center" >
			<tr class="filaresaltada">
			    <td width="20%" >Tabla</td>
				<td width="80%" ><input type='text' name='ntabla' class='textbox'  style="width:45%"></td>
			</tr>
			<tr class="filaresaltada">
			    <td >Descripcion</td>
				<td ><input type='text' name='dtabla' class='textbox' style="width:98%"></td>
			</tr>
		 </table>

		 
		<!-- cabecera de la seccion -->
		<table width="100%"  >
			<tr>
			<td class="subtitulo1"  width="50%">Campos Nueva Tabla</td>
			<td class="barratitulo" width="50%">
			  <img src="<%= BASEURL %>/images/titulo.gif" width="32" height="20">
			</td>
			</tr>
		</table>		
		<!-- fin cabecera -->		

		<table width="100%" border="1" bordercolor="#999999" bgcolor="#F7F5F4" id='ListaCampos'>
			<tr class="tblTitulo">
			    <th width="6%" >&nbsp;</th>
				<th width="30%" >Campo</th>
				<th width="30%" >Tipo</th>
				<th width="30%" >Extra</th>
				<th width="8%"  >Opcion</th>
			</tr>
		 </table>		
			
		</td>
	</tr>
	</table>
	<br>
   <img name="imgAgregar" src="<%=BASEURL%>/images/botones/agregar.gif" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" style="cursor:hand" onClick=" nuevoCampo('','',''); ">	
      &nbsp;
   <img name="imgGrabar" src="<%=BASEURL%>/images/botones/aceptar.gif" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" style="cursor:hand" onClick=" enviar(fest);">	   
   &nbsp;
   <img name="imgQuitar" src="<%=BASEURL%>/images/botones/eliminar.gif" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" style="cursor:hand" onClick=" deleteRow(); ">	
   <input type="hidden" name="Opcion" value="<%= Opcion %>"> 
 </form>
</center>
</div>
</body>
</html>


<script>
<% if (dt!=null) { %>
      fest.ntabla.value = '<%= dt.getTabla() %>';
      fest.ntabla.readOnly = true;
      fest.dtabla.value = '<%= dt.getDescripcion() %>';
      
      <% Iterator it = dt.getListaCampos().iterator(); 
         while (it.hasNext()){
             ImportacionEstructura campo = (ImportacionEstructura) it.next();
      %>
        nuevoCampo ("<%= campo.getCampo() %>","<%= campo.getTipo() %>","<%= campo.getExtra() %>");
      <% } %>
<% } %>
</script>
