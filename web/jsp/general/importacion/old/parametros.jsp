<%@page session="true"%>
<%@page errorPage="/error/ErrorPage.jsp"%>
<%@page import="java.util.*" %>
<%@include file="/WEB-INF/InitModel.jsp"%>
<link href="<%= BASEURL %>/css/estilotsp.css" rel="stylesheet">
<table border="1" align="center" bordercolor="#F7F5F4" bgcolor="#FFFFFF" width="500">
<tr>
<td class="subtitulo1"  width="50%" colspan='2'>Parametros</td>
<td class="barratitulo" width="50%">
  <img src="<%= BASEURL %>/images/cuadrosverde.JPG" width="32" height="20">
</td>
</tr>

<tr class="filaresaltada">
    <th width="30%" >Campo</th>
    <th width="10%" >Cant </th>
    <th width="60%" >Valor</th>
</tr>
<%
    Enumeration en = request.getParameterNames();
    while (en.hasMoreElements()){
       String ele = en.nextElement().toString();
       String [] valores = request.getParameterValues(ele);
       out.print("<tr class='fila'><td rowspan='"+ valores.length +"'>" + ele + "</td><td rowspan='"+ valores.length +"'>" + valores.length + "</td>");
       for (int i = 0 ; i < valores.length ; i++)
           out.print(  (i!=0?"<tr class='fila'> ":"") +  "<td>" + (!valores[i].equals("")?valores[i]:"vacio") + "</td></tr>");
    }
%>
</table>
