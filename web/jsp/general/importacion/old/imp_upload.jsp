<%@page session="true"%>
<%@page errorPage="/error/ErrorPage.jsp"%>
<%@page import="java.util.*" %>
<%@include file="/WEB-INF/InitModel.jsp"%>
<html>
<head>
    <title>Importar Archivos</title>
    <link   href="<%= BASEURL %>/css/estilostsp.css" rel="stylesheet">
    <script src ="<%= BASEURL %>/js/boton.js"></script>	
</head>
<body onresize="redimensionar()" onload = 'redimensionar()'>
<div id="capaSuperior" style="position:absolute; width:100%; height:100px; z-index:0; left: 0px; top: 0px;">
 <jsp:include page="/toptsp.jsp?encabezado=Importacion - Proceso Automatico"/>
</div>
<div id="capaCentral" style="position:absolute; width:100%; height:87%; z-index:0; left: 0px; top: 100px; overflow: scroll;">


        <center>
	<br>
	<form action="<%= CONTROLLER %>?estado=Opciones&accion=Importacion" method="post" name="fest">
	<table width="350" border="2" align="center">
	<tr>
		<td align="center">
		
		<!-- cabecera de la seccion -->
		<table width="100%" >
			<tr>
			<td class="subtitulo1"  width="50%">Importar Archivo</td>
			<td class="barratitulo" width="50%">
			  <img src="<%= BASEURL %>/images/titulo.gif" width="32" height="20">
			</td>
			</tr>
		</table>
		<!-- fin cabecera -->

		<table  width="100%" >
			<tr class="fila">
			    <th ><br>Correr proceso Automatico de Importacion<br><br>
			    </th>
			</tr>
		 </table>
        </td>         
      </tr>
      </table>
      <br>
      <input type="hidden" value="Run-Importacion-Auto" name="Opcion" >
<!--      <input type="submit" value="Procesar" class="boton" style=" width:120"> -->
	  <img name="imgProcesar" src="<%=BASEURL%>/images/botones/aceptar.gif" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" style="cursor:hand" onClick=" fest.submit(); ">
</form>
        

</center
</div></body>
</html>