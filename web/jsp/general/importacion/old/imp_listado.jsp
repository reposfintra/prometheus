<%@page session="true"%> 
<%@page errorPage="/error/ErrorPage.jsp"%>
<%@page import="com.tsp.operation.model.beans.*" %>
<%@page import="java.util.*" %>
<%@include file="/WEB-INF/InitModel.jsp"%>
<%
    List lista = model.ImportacionSvc.getListado();
%>
<html>
<head>

    <title>Listado General</title></head>
    <link  href="<%= BASEURL %>/css/estilostsp.css" rel="stylesheet">
    <script src="<%= BASEURL %>/js/validacionesImportacion.js"></script>
    <script src="<%= BASEURL %>/js/boton.js"></script>	
    <script src="<%= BASEURL %>/js/reporte.js"></script>
    <script> 
         BASEURL    = '<%= BASEURL %>'; 
         CONTROLLER = '<%= CONTROLLER %>';
    </script>
    
</head>    
<body onresize="redimensionar()" onload = 'redimensionar()'>
<div id="capaSuperior" style="position:absolute; width:100%; height:100px; z-index:0; left: 0px; top: 0px;">
 <jsp:include page="/toptsp.jsp?encabezado=Importacion - Listado"/>
</div>
<div id="capaCentral" style="position:absolute; width:100%; height:87%; z-index:0; left: 0px; top: 100px; overflow: scroll;">
<br>
<% if (lista!=null && lista.size()>0) { %>

<form action="<%= CONTROLLER %>?estado=Opciones&accion=Importacion" method="post" name="flist">
<table width="500" border="2" align="center">
    <tr><td>
	<table width="100%" align="center" class="tablaInferior">
           <tr>
             <td width="373" class="subtitulo1">&nbsp;Listado General de Tablas</td>
             <td width="427" class="barratitulo">
                <table width='100%' border='0' cellpadding='0' cellspacing='0'>
                <tr>
                    <td align="left"><img src="<%=BASEURL%>/images/titulo.gif" width="32" height="20"></td>    
                    <td align="right" width="70%">
                      <select name="Destino" class="listmenu" style='width:98%'>
                        <optgroup label="Opciones de Edicion">
                            <option value="Estructura-Buscar">Estructura de Archivos</option>
                            <option value="Parametros-Buscar">Parametros Importacion</option>
                        </optgroup>
                      </select>
                    </td>
                </table>
             
             </td>
           </tr>
        </table>
        <table width="100%"  border="1" bordercolor="#999999" bgcolor="#F7F5F4">
        <tr class="tblTitulo">
        <th width="1%">-</th>
        <th width="39%">Campo</th>
        <th width="60%">Descripcion</th>
        </tr>
        <%
           Iterator it = lista.iterator();
           int i = 0;
           while(it.hasNext()){
               ImportacionParametros dt = (ImportacionParametros) it.next(); %>
           <tr class="<%= (i++ % 2 == 0 )?"filagris":"filaazul"%>" 
               onMouseOver= "cambiarColorMouse(this)"
               style      = "cursor:hand"
               ondblclick = "goEdicion ('<%= dt.getTabla() %>',Destino.value);">
              <td class="bordereporte" ><input type="checkbox" name="tablas" value="<%= dt.getTabla() %>"></td>
              <td class="bordereporte" ><%= dt.getTabla()        %></td>
              <td class="bordereporte" ><%= dt.getDescripcion () %></td>
           </tr>
        <%  } %>
        </table>
    </td></tr>
</table>      

 <div align="center"><br>
    <img name="imgEliminar" src="<%=BASEURL%>/images/botones/eliminar.gif" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" style="cursor:hand" onClick=" enviar(flist); ">
    <input type="hidden" name="Opcion" value="Eliminar"> 
   
   <% }  else { %>
  </div>
 <table border="2" align="center">
    <tr>
      <td><table width="100%"  border="1" align="center"  bordercolor="#F7F5F4" bgcolor="#FFFFFF">
          <tr>
            <td width="282" align="center" class="mensajes">No se encontraron registros.</td>
            <td width="28" background="<%=BASEURL%>/images/cuadronaranja.JPG">&nbsp;</td>
            <td width="78">&nbsp;</td>
          </tr>
      </table></td>
    </tr>
  </table>
<% } %>
</center>
</div>
</body>
</html>
