<!--  
     - Author(s)       :      MARIO FONTALVO
     - Date            :      25/01/2006
     - Copyright Notice:      TRANSPORTES SANCHEZ POLO S.A.
-->
<%--   
     - @(#)  
     - Description: Importacion Directa de un archivo
--%> 

<%@page session="true"%>
<%@page errorPage="/error/ErrorPage.jsp"%>
<%@page import   ="com.tsp.operation.model.beans.Usuario;"%>
<%@include file="/WEB-INF/InitModel.jsp"%>
<%  
    String Mensaje = (request.getParameter("Mensaje")!=null?request.getParameter("Mensaje"):""); 
    Usuario user = (Usuario) session.getAttribute("Usuario");
    String path = application.getRealPath(model.menuService.getRealUrl(request.getRequestURI()));
    String datos[] = model.menuService.getContenidoMenu(BASEURL,request.getRequestURI(),path);
%>    
%>
<html>
<head>
    <title>Importar Archivos</title>
    <link   href="<%= BASEURL %>/css/estilostsp.css" rel="stylesheet">
    <script src ="<%= BASEURL %>/js/boton.js"></script>	
    <script>

      function validarExtension ( archivo ){
         var ext = /.XLS$/gi;
         if (!ext.test(archivo.toUpperCase()) ){
             alert ('Extension no valida, para el archivo de importacion');
             return false;
         } return true;
      }    
    </script>
    
</head>
<body onresize="redimensionar()" onload = 'redimensionar()'>
<div id="capaSuperior" style="position:absolute; width:100%; height:100px; z-index:0; left: 0px; top: 0px;">
 <jsp:include page="/toptsp.jsp?encabezado=Importacion - Proceso Automatico"/>
</div>
<div id="capaCentral" style="position:absolute; width:100%; height:87%; z-index:0; left: 0px; top: 100px; overflow: scroll;">


        <center>
	<br>
	<form action="<%= CONTROLLER %>?estado=Opciones&accion=ImportacionDirecta" method="post" name="fimp" enctype='multipart/form-data'>
	<table width="500" border="2" align="center">
	<tr>
		<td align="center">
		
		<!-- cabecera de la seccion -->
		<table width="100%" >
			<tr>
			<td class="subtitulo1"  width="50%">Importar Archivo</td>
			<td class="barratitulo" width="50%">
			  <img src="<%=BASEURL%>/images/titulo.gif" width="32" height="20" align="left"><%=datos[0]%>
			</td>
			</tr>
		</table>
		<!-- fin cabecera -->

		<table  width="100%" >
			<tr class="fila">
			    <td width='40%'>&nbsp;Tipo de proceso a ejecutar</td>
                            <td width='60%'>
                                <span style='width:49%'><input type='radio' name='tProceso' value='Actualizacion'>Actualización</span>
                                <span style='width:49%'><input type='radio' name='tProceso' value='Insercion'     checked>Inserción</span>
			    </td>
			</tr>		
		
			<tr class="fila">
			    <td >&nbsp;Seleccione su archivo</td>
			    <td ><input type='file' name='archivo' style='width:100%'></td>
			</tr>
		 </table>
        </td>         
      </tr>
      </table>
      <br>
      <input type="hidden" value="Importar" name="Opcion" >
      <img name="imgProcesar" src="<%=BASEURL%>/images/botones/aceptar.gif" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" style="cursor:hand" onClick=" if (fimp.archivo.value!='') { if ( validarExtension(fimp.archivo.value) ) {fimp.submit();} } else { alert ('Indique el archivo, para poder continuar'); } ">
      &nbsp;
      <img name="imgSalir" src="<%=BASEURL%>/images/botones/salir.gif" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" style="cursor:hand" onClick=" window.close(); ">
      </form>
        


<% if (!Mensaje.equals("")) { %>
    
        <table border="2" align="center">
           <tr>
           <td><table width="100%"  border="1" align="center"  bordercolor="#F7F5F4" bgcolor="#FFFFFF">
                <tr>
                 <td width="320" align="center" class="mensajes"><%=Mensaje%></td>
                 <td width="29" background="<%=BASEURL%>/images/cuadronaranja.JPG">&nbsp;</td>
                 <td width="58">&nbsp;</td>
                </tr>
              </table>
           </td>
           </tr>
        </table>  
        
        <br><br>
        
        <img style="cursor:hand " src="<%= BASEURL %>/images/file.gif" width="48" height="48" align="absmiddle" onClick="window.open('<%= CONTROLLER %>?estado=Ver&accion=Directorio&usuario=<%= user.getLogin() %>&evento=LOAD','','scroll=no,resizable=yes');">
        <br>
        <span class='informacion'>Haga sobre la imagen para poder consultar el log <br>de importacion, y el registro de errores en caso de tenerlos.</span>
        
        
        
<% } %>      
      
</center>
</div>
<%=datos[1]%>
</body>
</html>
