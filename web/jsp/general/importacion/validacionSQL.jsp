<%@page session="true"%> 
<%@page errorPage="/error/ErrorPage.jsp"%>
<%@page import="com.tsp.operation.model.beans.*, com.tsp.util.*" %>
<%@page import="java.util.*" %>
<%@include file="/WEB-INF/InitModel.jsp"%>

<%
	String campo       = Util.coalesce( (String) request.getParameter("campo"     ), "" );
	String validacion  = Util.coalesce( (String) request.getParameter("validacion"), "" );	
	String fila        = Util.coalesce( (String) request.getParameter("fila"      ), "" );
	String col         = Util.coalesce( (String) request.getParameter("col"       ), "" );
	String observacion = Util.coalesce( (String) request.getParameter("obs"       ), "" );
	String label       = (col.equals("8")?"INSERCION":"VALIDACION");
%>
<html>
<head>
    <link  href="<%= BASEURL %>/css/estilostsp.css" rel="stylesheet">
	<link  href="../../../css/estilostsp.css" rel="stylesheet">
	<script src="<%= BASEURL %>/js/boton.js"></script>
	<script src="<%= BASEURL %>/js/validacionesImportacion.js"></script>	
	<title>.:: Validacion SQL</title>
</head>
<body>
<center>
<br>


	<table width="560"  border="2" >

		<tr>
		<td>		
			<table width="100%" class="tablaInferior">
				<tr>
					<td class="subtitulo1"  width="50%">&nbsp;<%= label %> SQL</td>
					<td class="barratitulo" width="50%">
					  <img src="<%= BASEURL %>/images/titulo.gif" width="32" height="20">
					</td>
				</tr>			
			
				<tr class="fila" >
					<td colspan="2">&nbsp;Ingrese aqui la consulta de <%= label %> SQL para el campo <i style="color:#FF0000 "><%= campo %></i></td>
				</tr>
				<tr class="fila">
					<td  colspan="2">
                      <textarea name="sql" rows="15" title="Consulta de Vlidacion SQL" style="width:100% "><%= validacion %></textarea>
					</td>				
				</tr>			
				<tr class="fila" >
					<td colspan="2">&nbsp;Observacion (Este mensaje sera mostrado en caso de que la importacion <br>
									&nbsp;no cumpla con este criterio).</td>
				</tr>
				<tr class="fila">
					<td  colspan="2">
                      <textarea name="obs" rows="3"  title="Mensaje" style="width:100% " ><%= observacion %></textarea>
					</td>
				</tr>					
			</table>
		
		
		</td>
		</tr>
	</table>
   <br>

   <img name="imgGrabar"   src="<%=BASEURL%>/images/botones/aceptar.gif" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" style="cursor:hand" onClick=" asignar(sql.value, obs.value, <%= fila %>, <%= col%>);">
   <img name="imgRestablecer" src="<%=BASEURL%>/images/botones/cancelar.gif" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" style="cursor:hand" onClick=" asignar('','',<%= fila %>, <%= col%>);">	   
   <img name="imgSalir" src="<%=BASEURL%>/images/botones/salir.gif" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" style="cursor:hand" onClick=" window.close(); ">
   

</center>
</body>
</html>
