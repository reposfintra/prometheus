<!--
     - Author(s)       :      MARIO FONTALVO
     - Date            :      25/01/2006  
     - Copyright Notice:      TRANSPORTES SANCHEZ POLO S.A.
-->
<%--
     - @(#)  
     - Description: Listado de campos de una tabla de importacion.
--%> 
<%@page session="true"%>
<%@page errorPage="/error/ErrorPage.jsp"%>
<%@include file="/WEB-INF/InitModel.jsp"%>
<%@taglib uri="http://jakarta.apache.org/taglibs/input-0.90" prefix="input" %>
<html>
<head>

<title>Listado General de Tablas</title>
<script src="<%= BASEURL %>/js/boton.js"></script>
<script src="<%= BASEURL %>/js/validacionesImportacion.js"></script>
</head>
<body>
<div id="capaSuperior" style="position:absolute; width:100%; height:100px; z-index:0; left: 0px; top: 0px;">
 <jsp:include page="/toptsp.jsp?encabezado=LISTADO DE TABLAS"/>
</div>
<div id="capaCentral" style="position:absolute; width:100%; height:85%; z-index:0; left: 0px; top: 100px; overflow: scroll;">
<center>
<br>

<table align="center"  border="2" bgcolor="#F7F5F4" width='450'>
<tr>
  <td align='center' >
        <table class='tablaInferior' width='100%'>
        <tr>
            <td >
             <table border='0' width='100%' cellspacing='0'>
                  <tr>
                     <td class='subtitulo1'  width="50%">&nbsp; Listado de Tablas Generales</td>
                     <td class='barratitulo' width="50%"><img src="<%= BASEURL %>/images/titulo.gif" width="32" height="20"></td>
                  </tr>
             </table>             
            </td> 
        </tr>  
       <tr class='fila'>
            <td>Indique la tabla de la base de datos que desea consultar</td>
        </tr>        
        <tr class='fila'>
            <td><input type='text' name='tabla' style="width:100%" onkeyup="filtrarCombo (this.value, listaTablas)"></td>
        </tr>
        <tr class='fila'>
            <td><input:select name="listaTablas" attributesText="<%= "id='listaTablas'  style='width:100%;' class='textbox'  size='15' onchange='tabla.value=this.value' ondblclick='retornarTabla(this.value);' " %>"  default="" options="<%= model.ImportacionSvc.getTablas() %>" /></td>
        </tr>
        
        </table>
   </td>
</tr>   
</table>


<br >
<img name="imgGrabar" src="<%=BASEURL%>/images/botones/aceptar.gif" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" style="cursor:hand" onClick=" retornarTabla(listaTablas.value); " accion="Grabar">
&nbsp;
<img name="imgSalir" src="<%=BASEURL%>/images/botones/salir.gif" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" style="cursor:hand" onClick=" window.close(); " >
<br>

</center>
</div>




</body>
</html>
