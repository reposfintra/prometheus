<!--
     - Author(s)       :      MARIO FONTALVO
     - Date            :      10/10/2005  
     - Copyright Notice:      TRANSPORTES SANCHEZ POLO S.A.
-->
<%--   
     - @(#)  
     - Description: Listado general de la tablas de importacion.
--%> 
<%@page session="true"%> 
<%@page errorPage="/error/ErrorPage.jsp"%>
<%@page import="com.tsp.operation.model.beans.ImportacionParametros" %>
<%@page import="java.util.*" %>
<%@include file="/WEB-INF/InitModel.jsp"%>
<%
    List lista = model.ImportacionSvc.getListado();
    String Mensaje = (request.getParameter("Mensaje")!=null?request.getParameter("Mensaje"):"");
%>
<html>
<head>

    <title>Listado General</title></head>
    <link  href="<%= BASEURL %>/css/estilostsp.css" rel="stylesheet">
    <script src="<%= BASEURL %>/js/validacionesImportacion.js"></script>
    <script src="<%= BASEURL %>/js/boton.js"></script>	
    <script src="<%= BASEURL %>/js/reporte.js"></script>
    <script> 
         BASEURL    = '<%= BASEURL %>'; 
         CONTROLLER = '<%= CONTROLLER %>';
         function validarListaSeleccion (form){
            for (var i=0;i<form.length;i++){
               if (form.elements[i].type=='checkbox' && form.elements[i].checked)
                  return true;
            }
            return false;
         }
         
        <!--
           // Maximizar Ventana por Nick Lowe (nicklowe@ukonline.co.uk)

           window.moveTo(0,0);
           if (document.all) {
                 top.window.resizeTo(screen.availWidth,screen.availHeight);
           }
           else if (document.layers||document.getElementById) {
                   if(top.window.outerHeight<screen.availHeight||top.window.outerWidth<screen.availWidth){
                   top.window.outerHeight = screen.availHeight;
                   top.window.outerWidth = screen.availWidth;
                   }
           }
        //-->         
    </script>
    
</head>    
<body onresize="redimensionar()" onload = 'redimensionar()'>
<div id="capaSuperior" style="position:absolute; width:100%; height:100px; z-index:0; left: 0px; top: 0px;">
 <jsp:include page="/toptsp.jsp?encabezado=Importacion - Listado"/>
</div>
<div id="capaCentral" style="position:absolute; width:100%; height:87%; z-index:0; left: 0px; top: 100px; overflow: scroll;">
<br>
<% if (lista!=null && lista.size()>0) { %>

<form action="<%= CONTROLLER %>?estado=Opciones&accion=Importacion" method="post" name="flist">
<table width="900" border="2" align="center">
    <tr><td>
	<table width="100%" align="center" class="tablaInferior">
           <tr>
             <td width="373" class="subtitulo1">&nbsp;Listado General de Tablas</td>
             <td  class="barratitulo">
                <img src="<%=BASEURL%>/images/titulo.gif" width="32" height="20">
             </td>
           </tr>
        </table>
        <table width="100%"  border="1" bordercolor="#999999" bgcolor="#F7F5F4">
        <tr class="tblTitulo">
        <th width="1%">&nbsp;</th>
		<th width="20%">Formato</th>
        <th width="20%">Tabla</th>
        <th width="41%">Descripcion</th>
        <th width="18%" colspan="2">Opciones de Edicion</th>
        </tr>
        <%
           Iterator it = lista.iterator();
           int i = 0;
           while(it.hasNext()){
               ImportacionParametros dt = (ImportacionParametros) it.next(); 
               String style = (i++ % 2 == 0 )?"filagris":"filaazul";
           %>
           <tr class="<%= style %>" onMouseOver='cambiarColorMouse(this)'> 
              <td class="bordereporte" ><input type="checkbox" name="formatos" value="<%= dt.getFormato() %>" onclick=" this.parentNode.parentNode.className = (this.checked?'filaseleccion':'<%= style %>') ;   "></td>
              <td class="bordereporte" ><%= dt.getFormato()      %></td>			  			  
              <td class="bordereporte" ><%= dt.getTabla()        %></td>
              <td class="bordereporte" ><%= dt.getDescripcion () %></td>
              <td class="bordereporte" align='center'><a href='javascript: void (0);' onclick="goEdicion ('<%= dt.getFormato() %>','Parametros-Buscar');" style="cursor:hand; color:red">Parametros</a></td>
              <td class="bordereporte" align='center'><a href='javascript: void (0);' onclick="goEdicion ('<%= dt.getFormato() %>','Estructura-Buscar');" style="cursor:hand; color:red">Estructura</a></td>
           </tr>
        <%  } %>
        </table>
    </td></tr>
</table>      



 <div align="center"><br>
    <img name="imgEliminar" src="<%=BASEURL%>/images/botones/eliminar.gif" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" style="cursor:hand" onClick=" if (validarListaSeleccion(flist)){  flist.submit(); } else { alert ('Por favor seleccione por lo menos una tabla para poder continuar.'); } ">&nbsp;
    <img name="imgSalir" src="<%=BASEURL%>/images/botones/salir.gif" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" style="cursor:hand" onClick=" window.close(); ">
    <input type="hidden" name="Opcion" value="Eliminar"> 
   
   <% }  else { %>
  </div>
  <table border="2" align="center">
    <tr>
      <td><table width="100%"  border="1" align="center"  bordercolor="#F7F5F4" bgcolor="#FFFFFF">
          <tr>
            <td width="282" align="center" class="mensajes">No se encontraron registros.</td>
            <td width="28" background="<%=BASEURL%>/images/cuadronaranja.JPG">&nbsp;</td>
            <td width="78">&nbsp;</td>
          </tr>
      </table></td>
    </tr>
  </table>
  <br>
  <img name="imgSalir" src="<%=BASEURL%>/images/botones/salir.gif" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" style="cursor:hand" onClick=" window.close(); ">
<% } %>
<br><br>
<a style='color:red' href='<%= BASEURL %>/jsp/general/importacion/importar.jsp'>Importacion Directa</a>
<br><br>
<span class="informacion">
  <b>Nota: </b> Para retirar una o mas tablas de la lista de importacion primero seleccione la tabla y haga click sobre el boton eliminar.<br>
  Si lo que desea es consultar o editar los parametros de importacion utilice su respectivo enlace. 
</span>



 <% if (!Mensaje.equals("")) { %>
      <br><br>
    
        <table border="2" align="center">
           <tr>
           <td><table width="100%"  border="1" align="center"  bordercolor="#F7F5F4" bgcolor="#FFFFFF">
                <tr>
                 <td width="320" align="center" class="mensajes"><%=Mensaje%></td>
                 <td width="29" background="<%=BASEURL%>/images/cuadronaranja.JPG">&nbsp;</td>
                 <td width="58">&nbsp;</td>
                </tr>
              </table>
           </td>
           </tr>
        </table>  
        
<% } %>
</center>
</div>
</body>
</html>
