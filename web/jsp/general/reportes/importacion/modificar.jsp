<!--
- Autor : Ing. Osvaldo Pérez Ferrer
- Date : 19 de Octubre de 2006
- Copyrigth Notice : Fintravalores S.A. S.A
-->
<%--
-@(#)
--Descripcion : Pagina JSP, que sirve para consultar importaciones
--%>

<%@ page session="true"%>
<%@ page errorPage="../error/ErrorPage.jsp"%>
<%@page import="com.tsp.util.*"%>
<%@ include file="/WEB-INF/InitModel.jsp"%>
<%@ taglib uri="http://jakarta.apache.org/taglibs/input-0.90" prefix="input" %>
<%@page import="com.tsp.operation.model.*, com.tsp.operation.model.beans.*, com.tsp.util.*,java.util.*, java.text.*"%>

<html>
<head>
    <title>Modificar Importación</title>
    <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">    
    <script src="<%=BASEURL%>/js/utilidades.js"></script>
    <link href="../css/estilostsp.css" rel="stylesheet" type="text/css">
    <script type='text/javascript' src="<%= BASEURL %>/js/boton.js"></script>
    <link href="<%=BASEURL%>/css/estilostsp.css" rel="stylesheet" type="text/css">
<%
    String mensaje  = ( request.getParameter("mensaje")!=null )? (String)request.getParameter("mensaje"):"";
    String llegada  = ( request.getParameter("llegada")!=null )? (String)request.getParameter("llegada"):"";
    String salida   = ( request.getParameter("salida")!=null )? (String)request.getParameter("salida"):"";
    String anterior = ( request.getParameter("anterior")!=null )? (String)request.getParameter("anterior"):"";
    String path     = application.getRealPath(model.menuService.getRealUrl(request.getRequestURI()));
    String datos[]  = model.menuService.getContenidoMenu(BASEURL,request.getRequestURI(),path);    
    String codigo   = "";
    Vector v        = null;
%>
</head>

<body>
    <div id="capaSuperior" style="position:absolute; width:100%; height:100px; z-index:0; left: 1px; top: 0px;">
        <jsp:include page="/toptsp.jsp?encabezado=Modificar Importación"/>
    </div>
    <div id="capaCentral" style="position:absolute; width:100%; height:87%; z-index:0; left: 0px; top:100px; overflow: scroll;">        
    <form id="form1" method="post" action="">
    <table width="400"  border="2" align="center">
        <tr>
            <td><table width="100%"  border="1" align="center" class="tablaInferior">
            <tr>
            <td><table width="100%" border="0" cellpadding="0" cellspacing="0">
            <tr>
            <td height="22" colspan=2 class="subtitulo1"><div align="center" class="subtitulo1">
                <strong>Ingresar Llegada y Salida CDR</strong>                     
            </div></td>
            <td width="212" class="barratitulo">
            <img src="<%=BASEURL%>/images/titulo.gif" width="32" height="20" align="left">
                    <%=datos[0]%>
        </tr>
            </table>
             <%
                   
                        v = model.impoExpoService.getVector();
                        if( v !=null && v.size() > 0 ){
                        
                        ImpoExpo impoexpo = (ImpoExpo) v.elementAt(0);
                        codigo = impoexpo.getDocumento();
                %>
                
            <table width="100%"  border="0" align="center" cellpadding="2" cellspacing="3" onclick="document.getElementById('noexiste').style.visibility='hidden';">
                                        
                <tr >
                    <td class="fila">Importación : </td>
                    <td class="Letra"><%=codigo%></td>
                </tr>                
                
                <tr >
                    <td class="fila">Fecha SIA : </td>
                    <td class="Letra"><%=impoexpo.getFecha_sia()%></td>
                </tr>
                
                <tr >
                    <td class="fila">Fecha ETA : </td>
                    <td class="Letra"><%=impoexpo.getFecha_eta()%></td>
                </tr>
                
                <tr class="fila">                
                    <td class="fila">Fecha Llegada CDR : </td>
                    <td><input type="text" name="llegada" id="llegada" size="10" readonly value="<%=llegada%>" >
                        <a href="javascript:void(0)" onclick="if(self.gfPop)gfPop.fPopCalendar(llegada);return false;" HIDEFOCUS><img src="<%=BASEURL%>\js\Calendario\cal.gif" width="16" height="16" border="0" alt="Click aqui para escoger la fecha"></a>
                    </input></td>
		</tr>          
		
		<tr class="fila">                
                    <td class="fila">Fecha Salida CDR : </td>
                    <td><input type="text" name="salida" id="salida" size="10" readonly value="<%=salida%>">
                        <a href="javascript:void(0)" onclick="if(self.gfPop)gfPop.fPopCalendar(salida);return false;" HIDEFOCUS><img src="<%=BASEURL%>\js\Calendario\cal.gif" width="16" height="16" border="0" alt="Click aqui para escoger la fecha"></a>
                    </input></td>
		</tr>
            </table>
            <%}%>
            </td>
            </tr>
        </table></td>
        </tr>
    </table>
    <br>
    <div align="center">
        
        <img src="<%=BASEURL%>/images/botones/modificar.gif"  height="21" onMouseOver="botonOver(this);" onClick="sub('<%=CONTROLLER%>','<%=codigo%>');" onMouseOut="botonOut(this);" style="cursor:hand">
        <% if( anterior.equals("impo") ){%>
            <img src="<%=BASEURL%>/images/botones/regresar.gif"  height="21" onMouseOver="botonOver(this);" onClick="location.replace( '<%=CONTROLLER%>?estado=Impo&accion=Expo&opcion=<%=anterior%>&impo=<%=codigo%>');" onMouseOut="botonOut(this);" style="cursor:hand">
        <%}else{%>
            <img src="<%=BASEURL%>/images/botones/regresar.gif"  height="21" onMouseOver="botonOver(this);" onClick="location.replace( '<%=CONTROLLER%>?estado=Impo&accion=Expo&opcion=<%=anterior%>');" onMouseOut="botonOut(this);" style="cursor:hand">
        <%}%>
        <img src="<%=BASEURL%>/images/botones/salir.gif"  height="21" name="imgsalir"  onMouseOver="botonOver(this);" onClick="window.close();" onMouseOut="botonOut(this);" style="cursor:hand">            
    </div>
    </form>
    <br/>
	
        
    <div id="noexiste">
       <%if(mensaje != null && !mensaje.equals("") ){ %>
        
        <table border="2" align="center">
            <tr>                
                <td height="45"><table width="410" height="41" border="1" align="center"  bordercolor="#F7F5F4" bgcolor="#FFFFFF">
                    <tr>
                        <td width="282" height="35" align="center" class="mensajes"><%=mensaje%></td>
                        <td width="28" background="<%=BASEURL%>/images/cuadronaranja.JPG">&nbsp;</td>
                        <td width="78">&nbsp;</td>
                    </tr>
                </table></td>
            </tr>
        </table>        
               
       <%}%>     
    </div>
    	      
    </div>

<%=datos[1]%>
<iframe width=188 height=166 name="gToday:normal:agenda.js:gfPop:plugins.js" id="gToday:normal:agenda.js:gfPop:plugins.js" src="<%=BASEURL%>/js/Calendario/ipopeng.htm" scrolling="no" frameborder="0" style="visibility:visible; z-index:999; position:absolute; left:-500px; top:0px;"> </iframe>        
</body>

</html>
<script>
function sub( con , cod ){
    if( form1.llegada.value == "" || form1.salida.value == "" ){
        alert("Debe ingresar la fecha de Llegada y Salida CDR");
    }else{
        location.replace( con+"?estado=Impo&accion=Expo&opcion=guardar&codigo="+cod+"&llegada="+form1.llegada.value+"&salida="+form1.salida.value);
    }
}


</script>
