<!--
- Autor : Ing. Osvaldo P�rez Ferrer
- Date : 28 de Octubre de 2006
- Copyrigth Notice : Fintravalores S.A. S.A
-->
<%--
-@(#)
--Descripcion : Pagina JSP, que sirve para mostrar un reporte cuadro azul
--%>

<%@ page session="true"%>
<%@ page errorPage="../error/ErrorPage.jsp"%>
<%@page import="com.tsp.util.*"%>
<%@ include file="/WEB-INF/InitModel.jsp"%>
<%@ taglib uri="http://jakarta.apache.org/taglibs/input-0.90" prefix="input" %>
<%@page import="com.tsp.operation.model.*, com.tsp.operation.model.beans.*, com.tsp.util.*,java.util.*, java.text.*"%>

<html>
<head>
    <title>Cuadro Azul</title>
    <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">

    <script src="<%=BASEURL%>/js/utilidades.js"></script>
    <link href="../css/estilostsp.css" rel="stylesheet" type="text/css">
    <script type='text/javascript' src="<%= BASEURL %>/js/boton.js"></script>
    <link href="<%=BASEURL%>/css/estilostsp.css" rel="stylesheet" type="text/css">
<%
    
    
    String mensaje = "";
    String path = application.getRealPath(model.menuService.getRealUrl(request.getRequestURI()));
    String datos[] = model.menuService.getContenidoMenu(BASEURL,request.getRequestURI(),path);    
    List   tDoc     =  model.FormatoSvc.getTipoDocumento();
    
    String[] titulos;
    
    Vector v;
   
%>
</head>

<body>
    <div id="capaSuperior" style="position:absolute; width:100%; height:100px; z-index:0; left: 1px; top: 0px;">
        <jsp:include page="/toptsp.jsp?encabezado=Cuadro Azul"/>
    </div>
    <div id="capaCentral" style="position:absolute; width:100%; height:87%; z-index:0; left: 0px; top:100px; overflow: scroll;">        
    <form id="form1"  name="form1" method="post" action="">

            <table width="100%"  border="0" align="center" cellpadding="2" cellspacing="3" onclick="document.getElementById('noexiste').style.visibility='hidden';">
                
                        
                <%
                   
                        v = model.FormatoSvc.getVector();
                        if( v !=null && v.size() > 0 ){
                %>
                                    <table width="800"  border="2" align="center">


                            <tr>
                            <td width="800">
                                <table width="800" class="tablaInferior">
                                    <tr>
                                        <td height="22" colspan=2 class="subtitulo1">Reporte Cuadro Azul</td>
                                        <td width="50%" class="barratitulo"><img src="<%=BASEURL%>/images/titulo.gif" width="32" height="20"></td>
                                    </tr>
                                </table>                                   
                                <table width="790" border="1" align="center" bordercolor="#999999" bgcolor="#F7F5F4">    
                                <tr class="tblTitulo" align="center">
                                    <td>Documento</td>
                                    <% String[] titulos = model.FormatoSvc.getTitulos();
                                    for(int i=0; i<titulos.length; i++){%>
                                        <td ><%=titulos[i]%></td>                        
                                    <%}%>
                                </tr>
                                    <%


                                     DatoFormato dt = (DatoFormato) v.elementAt(0);
                                     String tipodoc = "";

                                     for(int i=0;i<tDoc.size();i++){
                                         Hashtable  doc  = (Hashtable)tDoc.get(i);     
                                         if( ( (String)doc.get("codigo") ).equals(dt.getTipo_doc()) ){
                                            tipodoc = (String)doc.get("descripcion");
                                            break;
                                         }
                                     }


                                     //Creaando el reporte
                                    String[] f;
                                    String doc = "";
                                    String[] cols;

                                     for(int i = 0; i<v.size(); i++){

                                        f = (String[])datos.get(i);

                                        doc = f[0];
                                        cols = f[1].split(",");
                                    %>
                                        <tr class="<%=i%2==0?"filagris":"filaazul"%>" onMouseOver='cambiarColorMouse(this)' 
                                        style="cursor:hand" title="Click para Modificar" align="center">    
                                        <td><%=doc%></td>

                                    <%
                                        for( int j=0; j<cols.length; j++ ){%>
                                            <td><%=cols[j]%></td>
                                        <%    
                                        }

                                    %>
                                    </tr>
                               <%     
                                }
                                        %>


                                </tr>            

                            </td>
                        </tr>            

                                      </table>                        
                            </table>   

                <%          }//fin si
                %>
                                                  
            </table>
    <br>
    <div align="center">                
        <img src="<%=BASEURL%>/images/botones/imprimir.gif"  height="21" onMouseOver="botonOver(this);" onClick="reporte();" onMouseOut="botonOut(this);" style="cursor:hand">
        <img src="<%=BASEURL%>/images/botones/regresar.gif"  height="21" onMouseOver="botonOver(this);" onClick="location.replace('<%=BASEURL%>/jsp/masivo/formato/filtro.jsp');" onMouseOut="botonOut(this);" style="cursor:hand">
        <img src="<%=BASEURL%>/images/botones/salir.gif"  height="21" name="imgsalir"  onMouseOver="botonOver(this);" onClick="window.close();" onMouseOut="botonOut(this);" style="cursor:hand">            
    </div>
    </form>
    <br/>
	
        
    <div id="noexiste">
       <%if(mensaje != null && !mensaje.equals("") ){ %>
        
        <table border="2" align="center">
            <tr>                
                <td height="45"><table width="410" height="41" border="1" align="center"  bordercolor="#F7F5F4" bgcolor="#FFFFFF">
                    <tr>
                        <td width="282" height="35" align="center" class="mensajes"><%=mensaje%></td>
                        <td width="28" background="<%=BASEURL%>/images/cuadronaranja.JPG">&nbsp;</td>
                        <td width="78">&nbsp;</td>
                    </tr>
                </table></td>
            </tr>
        </table>        
               
       <%}%>     
    </div>
    	      
    </div>
<%=datos[1]%>
</body>
<script>
function validar(){

    if(form1.impo.value == ""){
        alert("Debe ingresar el c�digo de la Importacion");
        return false;
    }    
    
    return true;
}

function reporte(){
    var num ="";
    
    if( num == "" ){
        alert("Debe seleccionar por lo menos una Importacion para el reporte");
    }else{
        alert(num);
    }    
}
</script>
</html>