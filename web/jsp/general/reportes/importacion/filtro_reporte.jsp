<!--
- Autor : Ing. Osvaldo P�rez Ferrer
- Date : 19 de Octubre de 2006
- Copyrigth Notice : Fintravalores S.A. S.A
-->
<%--
-@(#)
--Descripcion : Pagina JSP, que sirve para consultar importaciones
--%>

<%@ page session="true"%>
<%@ page errorPage="../error/ErrorPage.jsp"%>
<%@page import="com.tsp.util.*"%>
<%@ include file="/WEB-INF/InitModel.jsp"%>
<%@ taglib uri="http://jakarta.apache.org/taglibs/input-0.90" prefix="input" %>
<%@page import="com.tsp.operation.model.*, com.tsp.operation.model.beans.*, com.tsp.util.*,java.util.*, java.text.*"%>

<html>
<head>
    <title>Consultar Importaciones</title>
    <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">

    <script src="<%=BASEURL%>/js/utilidades.js"></script>
    <link href="../css/estilostsp.css" rel="stylesheet" type="text/css">
    <script type='text/javascript' src="<%= BASEURL %>/js/boton.js"></script>
    <link href="<%=BASEURL%>/css/estilostsp.css" rel="stylesheet" type="text/css">
<%
    String mensaje = "";
    String path = application.getRealPath(model.menuService.getRealUrl(request.getRequestURI()));
    String datos[] = model.menuService.getContenidoMenu(BASEURL,request.getRequestURI(),path);    
    
%>
</head>

<body>
    <div id="capaSuperior" style="position:absolute; width:100%; height:100px; z-index:0; left: 1px; top: 0px;">
        <jsp:include page="/toptsp.jsp?encabezado=Consultar Importaciones"/>
    </div>
    <div id="capaCentral" style="position:absolute; width:100%; height:87%; z-index:0; left: 0px; top:100px; overflow: scroll;">        
    <form id="form1" method="post" action="<%=CONTROLLER%>?estado=Impo&accion=Expo&opcion=impo">
    <table width="400"  border="2" align="center">
        <tr>
            <td><table width="100%"  border="1" align="center" class="tablaInferior">
            <tr>
            <td><table width="100%" border="0" cellpadding="0" cellspacing="0">
            <tr>
            <td height="22" colspan=2 class="subtitulo1"><div align="center" class="subtitulo1">
                <strong>Importaciones</strong>                     
            </div></td>
            <td width="212" class="barratitulo">
            <img src="<%=BASEURL%>/images/titulo.gif" width="32" height="20" align="left">
                    <%=datos[0]%> </td>
        </tr>
            </table>
            <table width="100%"  border="0" align="center" cellpadding="2" cellspacing="3" onclick="document.getElementById('noexiste').style.visibility='hidden';">
                                        
                <tr class="fila">
                    <td width="31%">Importaci�n : </td>
                    <td><input name="impo" id="impo" type="text" class="textbox" size="12">
                    <img src="<%=BASEURL%>/images/botones/buscar.gif" style="cursor:hand"  onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" onClick="if(validar()){form1.submit()};">            
                    </td>
                </tr>
                <tr class="fila">
                    <td height="20" colspan="2" class="Simulacion_Hiper" style="cursor:hand" onclick="listar('<%=CONTROLLER%>', '1');">&nbsp;Ver Importaciones sin fecha CDR</td>
                    
                </tr>
                <tr class="fila">
                    <td height="20" colspan="2" class="Simulacion_Hiper" style="cursor:hand" onclick="listar('<%=CONTROLLER%>', '2');">&nbsp;Ver todas las Importaciones</td>                    
                </tr>                              
                  
            </table></td>
            </tr>
        </table></td>
        </tr>
    </table>
    <br>
    <div align="center">                
        <img src="<%=BASEURL%>/images/botones/salir.gif"  height="21" name="imgsalir"  onMouseOver="botonOver(this);" onClick="window.close();" onMouseOut="botonOut(this);" style="cursor:hand">            
    </div>
    </form>
    <br/>
	
        
    <div id="noexiste">
       <%if(mensaje != null && !mensaje.equals("") ){ %>
        
        <table border="2" align="center">
            <tr>                
                <td height="45"><table width="410" height="41" border="1" align="center"  bordercolor="#F7F5F4" bgcolor="#FFFFFF">
                    <tr>
                        <td width="282" height="35" align="center" class="mensajes"><%=mensaje%></td>
                        <td width="28" background="<%=BASEURL%>/images/cuadronaranja.JPG">&nbsp;</td>
                        <td width="78">&nbsp;</td>
                    </tr>
                </table></td>
            </tr>
        </table>        
               
       <%}%>     
    </div>
    	      
    </div>
<%=datos[1]%>
</body>
<script>
function validar(){
    if(form1.impo.value == ""){
        alert("Debe ingresar el c�digo de la Importacion");
        return false;
    }    
    
    return true;
}

function listar( controller, opc ){
    
    if( opc == "1" ){   
        location.replace( controller+"?estado=Impo&accion=Expo&opcion=sinCDR");
    }
    else if( opc == "2" ){   
        location.replace( controller+"?estado=Impo&accion=Expo&opcion=todasImpo");
    }

}
</script>
</html>