<!--
- Autor : Ing. Mario Fontalvo Solano
- Date  : 12 Marzo del 2007
- Copyrigth Notice : Fintravalores S.A. S.A
-->
<%--
-@(#)
--Descripcion : Pagina JSP, para la generacion del reporte de ppp
--%>
<%@page contentType="text/html"%>
<%@ page session   ="true"%> 
<%@ page errorPage ="/error/ErrorPage.jsp"%>
<%@page import="java.util.*,com.tsp.util.*"%>
<%@include file="/WEB-INF/InitModel.jsp"%>
<%
  String path = application.getRealPath(model.menuService.getRealUrl(request.getRequestURI()));
  String datos[] = model.menuService.getContenidoMenu(BASEURL,request.getRequestURI(),path);
  String msg      = (String) request.getAttribute("msg");  
%>
<html>
<head>
	<title>.: Reporte Periodo Promedio de Pago</title>
	<link href="../../../../css/estilostsp.css" rel="stylesheet">
	<script type='text/javascript' src="<%= BASEURL %>/js/validar.js"></script>
	<script type='text/javascript' src="<%= BASEURL %>/js/boton.js"></script>
</head>
<body onresize="redimensionar()" onload = 'redimensionar()'>
<div id="capaSuperior" style="position:absolute; width:100%; height:110px; z-index:0; left: 0px; top: 0px;">
 <jsp:include page="/toptsp.jsp?encabezado=Periodo Promedio de Pago"/>
</div>
<div id="capaCentral" style="position:absolute; width:100%; height:87%; z-index:0; left: 0px; top: 109px; overflow: scroll;">
<center>
<!--<form name="Formulario" action="<%=BASEURL%>/jsp/general/importacion/parametros.jsp" method="post">-->
<form name="Formulario" action="<%= CONTROLLER %>?estado=Reporte&accion=PPP&opcion=Generar" method="post">
	<table border="2" width="500">
		<tr>
			<td>			
				<table width="100%"  border="0" cellpadding="0" cellspacing="1">
					<tr>
						<td width="50%" class="subtitulo1">&nbsp;Generacion Reporte PPP </td>
						<td class="barratitulo"><img align="left" src="<%=BASEURL%>/images/titulo.gif" width="32" height="20"></td>
					</tr>
				</table>
				<table width="100%"  border="0">
				
					<tr class="fila"  >
						<td colspan="2" height="25" >&nbsp;Indique los periodos para cada categoria de Remesas.</td>                        
					</tr>			

					<tr class="fila">
						<td > <li>No Facturadas</li></td>
                        <td >
							<input type="text" name="fecIniRNF" readonly style="text-align:center "  >
			                <a href='javascript:void(0)' onclick='if(self.gfPop)gfPop.fPopCalendar(document.Formulario.fecIniRNF);return false;' HIDEFOCUS><img src='<%= BASEURL %>/js/Calendario/cal.gif' width='16' height='16' border='0' alt='De click aqui para escoger la fecha'></a>
							<input type="text" name="fecFinRNF"  readonly style="text-align:center " >
			                <a href='javascript:void(0)' onclick='if(self.gfPop)gfPop.fPopCalendar(document.Formulario.fecFinRNF);return false;' HIDEFOCUS><img src='<%= BASEURL %>/js/Calendario/cal.gif' width='16' height='16' border='0' alt='De click aqui para escoger la fecha'></a>
						</td>
					</tr>			
					
					<tr class="fila" >
						<td ><li >Facturadas No Pagadas</li> </td>
                        <td >
							<input type="text" name="fecIniRFNP" readonly style="text-align:center " >
			                <a href='javascript:void(0)' onclick='if(self.gfPop)gfPop.fPopCalendar(document.Formulario.fecIniRFNP);return false;' HIDEFOCUS><img src='<%= BASEURL %>/js/Calendario/cal.gif' width='16' height='16' border='0' alt='De click aqui para escoger la fecha'></a>
							<input type="text" name="fecFinRFNP"  readonly style="text-align:center ">
			                <a href='javascript:void(0)' onclick='if(self.gfPop)gfPop.fPopCalendar(document.Formulario.fecFinRFNP);return false;' HIDEFOCUS><img src='<%= BASEURL %>/js/Calendario/cal.gif' width='16' height='16' border='0' alt='De click aqui para escoger la fecha'></a>
						</td>
					</tr>								
			
														
					<tr class="fila">
						<td ><li >Pagadas</li></td>
                        <td >
							<input type="text" name="fecIniRFP" readonly style="text-align:center ">
			                <a href='javascript:void(0)' onclick='if(self.gfPop)gfPop.fPopCalendar(document.Formulario.fecIniRFP);return false;' HIDEFOCUS><img src='<%= BASEURL %>/js/Calendario/cal.gif' width='16' height='16' border='0' alt='De click aqui para escoger la fecha'></a>
							<input type="text" name="fecFinRFP"  readonly style="text-align:center ">
			                <a href='javascript:void(0)' onclick='if(self.gfPop)gfPop.fPopCalendar(document.Formulario.fecFinRFP);return false;' HIDEFOCUS><img src='<%= BASEURL %>/js/Calendario/cal.gif' width='16' height='16' border='0' alt='De click aqui para escoger la fecha'></a>
						</td>
					</tr>			
													
				</table>
				
			</td>
		</tr>
	</table>	
	
</form>	


<img src='<%=BASEURL%>/images/botones/aceptar.gif'      style='cursor:hand' onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" onclick='validarForma();'>
<img src='<%=BASEURL%>/images/botones/restablecer.gif'  style='cursor:hand' onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" onclick='Formulario.reset();'>
<img src='<%=BASEURL%>/images/botones/salir.gif'        style='cursor:hand' onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" onclick='window.close();'>



 
   <% if(msg!=null && !msg.trim().equals("")) { %>
        <p>
            <table border="2" align="center">
                <tr>
                    <td>
                        <table width="100%"  border="1" align="center"  bordercolor="#F7F5F4" bgcolor="#FFFFFF">
                            <tr>
                                <td width="257" align="center" class="mensajes"><%=msg%></td>
                                <td width="8" background="<%=BASEURL%>/images/cuadronaranja.JPG">&nbsp;</td>
                                <td width="58">&nbsp;</td>
                            </tr>
                        </table>                     
                    </td>
                </tr>
            </table>
        </p>
  <%}%>	
 
</center>
</div>
<iframe width=188 height=166 name="gToday:normal:<%=BASEURL%>/js/calendartsp/agenda.js:gfPop:<%=BASEURL%>/js/calendartsp/plugins.js" id="gToday:normal:<%=BASEURL%>/js/calendartsp/agenda.js:gfPop:<%=BASEURL%>/js/calendartsp/plugins_24.js" src="<%=BASEURL%>/js/calendartsp/ipopeng.htm" scrolling="no" frameborder="0" style="visibility:visible; z-index:999; position:absolute; left:-500px; top:0px;">
</iframe>
</body>
</html>
<script>
	
	function validarForma(){
	 	with (Formulario) {
		
			// remesas no facturadas
			if (fecIniRNF.value == "" ){
				alert ('Indique la fecha inicial para poder continuar. (Remesas No Facturadas).');			
				return false;
			}
			if (fecFinRNF.value == "" ){
				alert ('Indique la fecha final para poder continuar. (Remesas No Facturadas).');			
				return false;
			}			
			if (fecIniRNF.value > fecFinRNF.value ){
				alert ('La fecha final debe ser mayor que la fecha inicial. (Remesas No Facturadas).');
				return false;
			}
			
			// remesas facturadas no pagadas
			if (fecIniRFNP.value == "" ){
				alert ('Indique la fecha inicial para poder continuar (Remesas Facturadas No Pagadas).');			
				return false;
			}
			if (fecFinRFNP.value == "" ){
				alert ('Indique la fecha final para poder continuar (Remesas Facturadas No Pagadas).');			
				return false;
			}			
			if (fecIniRFNP.value > fecFinRFNP.value ){
				alert ('La fecha final debe ser mayor que la fecha inicial. (Remesas Facturadas No Pagadas).');
				return false;
			}
			
			// remesas pagadas
			if (fecIniRFP.value == "" ){
				alert ('Indique la fecha inicial para poder continuar. (Remesas Pagadas)');			
				return false;
			}
			if (fecFinRFP.value == "" ){
				alert ('Indique la fecha final para poder continuar.  (Remesas Pagadas)');			
				return false;
			}			
			if (fecIniRFP.value > fecFinRFP.value ){
				alert ('La fecha final debe ser mayor que la fecha inicial.  (Remesas Pagadas)');
				return false;
			}						
			submit();
		}
		
	}
	
	
	window.focus();
</script>