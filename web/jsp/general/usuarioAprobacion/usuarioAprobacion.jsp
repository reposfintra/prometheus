<%@page contentType="text/html"%>
<%@page session="true"%> 
<%@page errorPage="/error/ErrorPage.jsp"%>
<%@page import="java.util.*"%>
<%@page import="com.tsp.operation.model.beans.*"%>
<%@page import="com.tsp.util.*"%>
<%@page import="com.tsp.operation.model.*"%>
<%@ include file="/WEB-INF/InitModel.jsp"%>
<%@ taglib uri="http://jakarta.apache.org/taglibs/input-0.90" prefix="input" %>
<% TreeMap agencias = model.agenciaService.getAgencia();

   Vector vec = model.usuarioService.listarUsuarios();
    %>

<html>
<head>
<title>Asignar Usuario Aprobaci&oacute;n</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link href="../../../css/estilostsp.css" rel="stylesheet" type="text/css">
<link href="<%=BASEURL%>/css/estilostsp.css" rel="stylesheet" type="text/css">
<script src='<%=BASEURL%>/js/date-picker.js'></script>
<script src='<%=BASEURL%>/js/validar.js'></script>
<script language="JavaScript" type="text/javascript" src="<%=BASEURL%>/js/boton.js"></script>
<script language="JavaScript" type="text/javascript" src="<%=BASEURL%>/js/funciones.js"></script>
</head>
<body <%= ( request.getParameter("sw")!=null && request.getParameter("mensaje")!=null ) ?"onLoad='window.opener.location.reload();'":""%> >
<div id="capaSuperior" style="position:absolute; width:100%; height:100px; z-index:0; left: 1px; top: 0px;">
 <jsp:include page="/toptsp.jsp?encabezado=Asignar Usuario Aprobación"/>
</div>
<div id="capaCentral" style="position:absolute; width:100%; height:87%; z-index:0; left: 0px; top: 100px; overflow: scroll;">
<%
String sw = (request.getParameter("sw")!=null)? request.getParameter("sw"):"" ;
String msg = (request.getParameter("mensaje")!=null)? request.getParameter("mensaje"):"" ;
String agencia="",tabla="",usur="";
String accion = "Insertar";
if (!sw.equals("")){
  UsuarioAprobacion usuapro =  model.usuaprobacionService.getUsuarioAprobacion();
  agencia = usuapro.getId_agencia();
  tabla = usuapro.getTabla();
  usur = usuapro.getUsuario_aprobacion();
  accion = "Modificar";
}

%>
<form name="forma" method="post" action="<%=CONTROLLER%>?estado=UsuarioAprobacion&accion=<%=accion%>" >
  <table width="450" border="2" align="center">
    <tr>
      <td><table width="100%"  border="0">
        <tr>
          <td width="48%" class="subtitulo1">Informacion            </td>
          <td width="52%" class="barratitulo"><img src="<%=BASEURL%>/images/titulo.gif" width="32" height="20"></td>
        </tr>
      </table>        <table width="100%"  class="tablaInferior">
        <tr class="fila">
          <td width="37%" align="left" class="letra_resaltada">Agencia</td>
          <td width="63%" align="left"><input:select name="agencia" attributesText="class=textbox"   options="<%= agencias %>" />
              <img src="<%=BASEURL%>/images/botones/iconos/obligatorio.gif">
              <input type="hidden" name="agenciaAct" value="<%=agencia%>">              </tr>
		<tr class="fila">
			<td class="letra_resaltada" align="left">Programa</td>
			<td align="left"><select name="tabla" class="textbox" id="tabla" style="width:90% ">
              <option value="">Seleccione Un Item</option>
              <% LinkedList tblres = model.tablaGenService.obtenerTablas();
               for(int i = 0; i<tblres.size(); i++){
                       TablaGen respa = (TablaGen) tblres.get(i); %>
              <option value="<%=respa.getTable_code()%>" <%=(respa.getTable_code().equals(tabla))?"selected":""%> ><%=respa.getDescripcion()%></option>
              <%}%>
            </select>
<img src="<%=BASEURL%>/images/botones/iconos/obligatorio.gif">
			  <input type="hidden" name="tablaAct" value="<%=tabla%>"></td>
		<tr class="fila">
		  <td class="letra_resaltada" align="left">Usuario</td>
		  <td align="left"><select name='usuario' class="textbox" id="usuario" >
            <%
                       if(vec.size()>0) {
                           for (int i = 0; i < vec.size(); i++ ){ 
                               Usuario  usu =  (Usuario) vec.elementAt(i); 
							   if ( usu.getIdusuario().equals(usur) ) {                            
                               		out.print("<option value='"+usu.getIdusuario()+"' selected >"+usu.getNombre()+"</option> \n");
							   }
							   else{
							   		out.print("<option value='"+usu.getIdusuario()+"' >"+usu.getNombre()+"</option> \n");
							   }
                           }
                       }
                       %>
          </select>		    <img src="<%=BASEURL%>/images/botones/iconos/obligatorio.gif">
          <input type="hidden" name="usuarioAct" value="<%=usur%>"></td>
		</table></td>
    </tr>
  </table>
  <br>
  <table align="center">
<tr>
        <td width="300" colspan="2" align="center" nowrap>		
		<%if(sw.equals("")){%>
		<img src="<%=BASEURL%>/images/botones/aceptar.gif"  name="imgaceptar" onClick="validarUsuario();" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" style="cursor:hand">&nbsp;
		<%}else{%>
		<img src="<%=BASEURL%>/images/botones/modificar.gif"  name="imgmodificar" onClick="validarUsuario();" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" style="cursor:hand">&nbsp;
		<img src="<%=BASEURL%>/images/botones/eliminar.gif"  name="imgeliminar" onClick="window.location='<%=CONTROLLER%>?estado=UsuarioAprobacion&accion=Eliminar&agencia=<%=agencia%>&tabla=<%=tabla%>&usuario=<%=usur%>'" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" style="cursor:hand">
		<%}%>
	    <img src="<%=BASEURL%>/images/botones/salir.gif" name="imgcerrar"  onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" onclick="window.close();" style="cursor:hand "></td>
      </tr>
</table>
<br>
<%if(!msg.equals("")){%>
  </p>
  <p><table border="2" align="center">
  <tr>
    <td><table width="100%"  border="1" align="center"  bordercolor="#F7F5F4" bgcolor="#FFFFFF">
      <tr>
        <td width="229" align="center" class="mensajes"><%=msg%></td>
        <td width="29" background="<%=BASEURL%>/images/cuadronaranja.JPG">&nbsp;</td>
        <td width="58">&nbsp;</td>
      </tr>
    </table></td>
  </tr>
</table>
</p>
 <%}%>
</form>
</div>

</body>
</html>
