<!-- 
- Autor : Ing. Diogenes Bastidas Morales
- Date  : 18 de enero de 2006
- Copyrigth Notice : Fintravalores S.A. S.A
-->
<%--
-@(#)
--Descripcion : Pagina JSP, que lista las actividades
--%>

<%-- Declaracion de librerias--%>
<%@page contentType="text/html"%>
<%@page session="true"%> 
<%@page errorPage="/error/ErrorPage.jsp"%>
<%@page import="java.util.*"%>
<%@page import="com.tsp.operation.model.beans.*"%>
<%@page import="com.tsp.util.*"%>
<%@page import="com.tsp.operation.model.*"%>
<%@ include file="/WEB-INF/InitModel.jsp"%>
<%@ taglib uri="http://jakarta.apache.org/taglibs/input-0.90" prefix="input" %>
<% TreeMap agencias = model.agenciaService.getAgencia();

   Vector vec = model.usuarioService.listarUsuarios();
    %>

<html>
<head>
<title>Relacion de Egreso</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link href="../../../css/estilostsp.css" rel="stylesheet" type="text/css">
<link href="<%=BASEURL%>/css/estilostsp.css" rel="stylesheet" type="text/css">
<script src='<%=BASEURL%>/js/date-picker.js'></script>
<script src='<%=BASEURL%>/js/validar.js'></script>
<script language="JavaScript" type="text/javascript" src="<%=BASEURL%>/js/boton.js"></script>
</head>
<body>
<div id="capaSuperior" style="position:absolute; width:100%; height:100px; z-index:0; left: 1px; top: 0px;">
 <jsp:include page="/toptsp.jsp?encabezado=Buscar Usuario Aprobación"/>
</div>
<div id="capaCentral" style="position:absolute; width:100%; height:87%; z-index:0; left: 0px; top: 100px; overflow: scroll;">

<form name="forma" method="post" action="<%=CONTROLLER%>?estado=UsuarioAprobacion&accion=Busqueda" >
  <table width="50%" border="2" align="center">
    <tr>
      <td><table width="100%"  border="0">
        <tr>
          <td width="48%" class="subtitulo1">Informacion            </td>
          <td width="52%" class="barratitulo"><img src="<%=BASEURL%>/images/titulo.gif" width="32" height="20"></td>
        </tr>
      </table>        <table width="100%"  class="tablaInferior">
        <tr class="fila">
          <td width="37%" align="left" class="letra_resaltada">Agencia</td>
          <td width="63%" align="left"><input:select name="agencia" attributesText="class=textbox"   options="<%= agencias %>" />
        </tr>
		<tr class="fila">
			<td class="letra_resaltada" align="left">Programas</td>
			<td align="left"><select name="tabla" class="textbox" id="tabla" style="width:90% ">
              <option value=""></option>
              <% LinkedList tblres = model.tablaGenService.obtenerTablas();
               for(int i = 0; i<tblres.size(); i++){
                       TablaGen respa = (TablaGen) tblres.get(i); %>
              <option value="<%=respa.getTable_code()%>"  ><%=respa.getDescripcion()%></option>
              <%}%>
            </select></td>
		<tr class="fila">
		  <td class="letra_resaltada" align="left">Usuario</td>
		  <td align="left"><select name='usuario' class="textbox" id="usuario" >
		   <option value=""> </option>
            <%
                       if(vec.size()>0) {
                           for (int i = 0; i < vec.size(); i++ ){ 
                               Usuario  usu =  (Usuario) vec.elementAt(i);                             
                               out.print("<option value='"+usu.getIdusuario()+"'>"+usu.getNombre()+"</option> \n");
                           }
                       }
                       %>
          </select></td>
		</table></td>
    </tr>
  </table>
  <br>
  <table align="center">
<tr>
        <td colspan="2" nowrap align="center">		
		<img src="<%=BASEURL%>/images/botones/buscar.gif"  name="imgaceptar" onClick="forma.submit();" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" style="cursor:hand">&nbsp;
	    <img src="<%=BASEURL%>/images/botones/salir.gif" name="imgcerrar"  onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" onclick="window.close();" style="cursor:hand "></td>
      </tr>
</table>


</form>
</div>

</body>
</html>
