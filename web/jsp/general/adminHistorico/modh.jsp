<%@page contentType="text/html"%>
<%@page errorPage="/error/ErrorPage.jsp"%>
<%@page import="com.tsp.operation.model.beans.*, java.util.*"%>
<%@include file="/WEB-INF/InitModel.jsp" %>
<%@ taglib uri="http://jakarta.apache.org/taglibs/input-0.90" prefix="input" %>
<%
        String tablaO= ""+request.getParameter("id");
		//out.print(""+request.getParameter("id"));
        Usuario usuarioLogin  = (Usuario) session.getAttribute("Usuario");
        String us  = usuarioLogin.getLogin();
      	model.adminH.listarHistoricoPorId(tablaO);
        AdminHistorico adminH = model.adminH.getAdmin();
%>

<html>
<head>
<title>Modificar</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link href="<%=BASEURL%>/css/estilostsp.css" rel="stylesheet" type="text/css"> 
<script type='text/javascript' src="<%=BASEURL%>/js/boton.js"></script> 
<script type='text/javascript' src="<%=BASEURL%>/js/Validacion.js"></script> 
<script>

function validar(){
	
	if(forma1.tablaO.value==""  && forma1.rutina.value=="" ){
		alert ("Debe Digitar un Nombre de Tabla ");
		forma1.tablaO.focus();
		return false;
	}
	
	if(forma1.fechaInicio.value==""  ){
		alert ("Debe seleccionar una Fecha ");
		forma1.fechaInicio.focus();
		return false;
	}
	
	if(isNaN (forma1.duracion.value) || forma1.duracion.value=="" ){
		alert ("La duracion debe ser un Numero ");
		forma1.duracion.focus();
		return false;
	}
	
}
</script>
<link href="../../../css/estilostsp.css" rel="stylesheet" type="text/css">
</head>

<body>
<form name="forma1" action="<%=CONTROLLER%>?estado=Historico&accion=Actualizar" method="post" onSubmit="return validar();">      
  <table width="379" border="2" align="center">
  <tr>
    <td >
<table width="100%"  border="0" align="center" cellpadding="0" cellspacing="0" bordercolor="#F7F5F4" bgcolor="#FFFFFF"> 
  <tr>
    <td width="226" height="24"  class="subtitulo1">Administracion de Historiales</td>
    <td width="141"  class="barratitulo"><img src="<%=BASEURL%>/images/titulo.gif" width="32" height="20"></td>
  </tr>
</table>
<table width="361" align="center" class="tablaInferior">
  <tr class="fila">
    <td width="144" height="30" >Tabla Origen : </td>
    <td width="201"><input name="tablaO" type="text" class="textbox" value="<%=adminH.getTablaO()%>" id="tablaO" size="20" >      </td>
    </tr>
  <tr class="fila">
    <td width="144" height="30" >Fecha de Inicio : </td>
    <td width="201"><input name="fechaInicio" type="text" class="textbox" id="fechaInicio" size="20" readonly value="<%=adminH.getFechaInicio()%>">      
    <span class="comentario"><a href="javascript:void(0)" onClick="if(self.gfPop)gfPop.fPopCalendar(document.forma1.fechaInicio);return false;" hidefocus><img name="popcal" align="absmiddle" src="<%=BASEURL%>/js/calendartsp/calbtn.gif" width="16" height="16" border="0" alt=""></a></span></span>  </td>
    </tr>
  <tr class="fila">
    <td width="144" height="30" >Duraci&oacute;n:</td>
    <td width="201"><input name="duracion" type="text" class="textbox" id="duracion" value="<%=adminH.getDuracion()%>"size="5" maxlength="3" > 
    valor en dias   </td>
    </tr>
  <tr class="fila">
    <td width="144" height="30" >Rutina</td>
    <td width="201"><!--input name="rutina" type="text" class="textbox" id="rutina" value="<%//=adminH.getRutina()%>" size="20"-->
				<input name="rutina" type="file" class="textbox"  id="rutina">	
	       </td>
    </tr>
	 <tr class="fila">
    <td width="144" height="30" >Estado</td>
    <td width="201">Activo &nbsp;<input type="radio" name="estadoo"  value="A"  checked>  Inactivo &nbsp; <input type="radio" name="estadoo"  value="I"   >     </td>
    </tr>
	<tr class="fila">
    <td colspan="2"  align="center"></td>
    </tr>
</table>
<input type="hidden" name='tablaOA' value='<%=adminH.getTablaO()%>'>
<input type="hidden" name='id' value='<%=adminH.getId()%>'>
<!--input type="hidden" name='tablaD' value='<%//=adminH.getTablaD()%>'-->
</td>
</tr>
</table>
<br><center>
<input type="image" name="Submit" value="Ingresar" src="<%= BASEURL %>/images/botones/aceptar.gif" align="absmiddle" onClick="return validar();" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);">
<img src="<%=BASEURL%>/images/botones/salir.gif" name="c_salir" onClick="parent.close();" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" align="absmiddle">
</center>
<input type="hidden" name='usuario' value="<%=us%>">
</form>
<iframe width=188 height=166 name="gToday:normal:<%=BASEURL%>/js/calendartsp/agenda.js:gfPop:<%=BASEURL%>/js/calendartsp/plugins_24.js" id="gToday:normal:<%=BASEURL%>/js/calendartsp/agenda.js:gfPop:<%=BASEURL%>/js/calendartsp/plugins_24.js" src="<%=BASEURL%>/js/calendartsp/ipopeng.htm" scrolling="no" frameborder="0" style="visibility:visible; z-index:999; position:absolute; left:-500px; top:0px;">
</iframe>
</html>

