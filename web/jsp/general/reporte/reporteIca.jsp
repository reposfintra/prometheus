<%@ page session="true"%>
<%@ include file="/WEB-INF/InitModel.jsp"%>
<%@ taglib uri="http://jakarta.apache.org/taglibs/input-0.90" prefix="input" %>
<%@page import="com.tsp.operation.model.*, com.tsp.operation.model.beans.*, com.tsp.util.*,java.util.*, java.text.*"%>

<html>
<head>
<title>Editor para la generación del reporte de ICA</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link href="../css/estilostsp.css" rel="stylesheet" type="text/css" >
<script type='text/javascript' src="<%= BASEURL %>/js/boton.js"></script>
<link href="<%=BASEURL%>/css/estilostsp.css" rel="stylesheet" type="text/css">
<%
    
    String path = application.getRealPath(model.menuService.getRealUrl(request.getRequestURI()));
    String datos[] = model.menuService.getContenidoMenu(BASEURL,request.getRequestURI(),path);
%>
<%
  String msg = (request.getParameter("msg")!=null)?request.getParameter("msg"):"";
 
%>
</head>
<script>
    function validaFechas(){
        if(form1.fechai.value == ""){
            alert("Debe seleccionar la fecha inicial");
            if(self.gfPop)gfPop.fPopCalendar(form1.fechai);
            return false;
        }
        if(form1.fechaf.value == ""){
            alert("Debe seleccionar la fecha final");
            if(self.gfPop)gfPop.fPopCalendar(form1.fechaf);
            return false;
        }
        if( form1.fechai.value > form1.fechaf.value ){
            alert( "La fecha fin no puede ser menor que la fecha inicio." );
            return false;
        }
        form1.submit();
        
    }
</script>
<body>
<div id="capaSuperior" style="position:absolute; width:100%; height:100px; z-index:0; left: 1px; top: 0px;">
 <jsp:include page="/toptsp.jsp?encabezado=Reporte ICA"/>
</div>
<div id="capaCentral" style="position:absolute; width:100%; height:87%; z-index:0; left: 0px; top: 100px; overflow: scroll;">        
	<form id="form1" method="post" action="<%=CONTROLLER%>?estado=Reporte&accion=Ica">
    <table width="50%"  border="2" align="center" >
      <tr>
        <td><table width="100%"  border="1" align="center" class="tablaInferior">
          <tr>
            <td>
                <table width="100%" border="0" cellpadding="0" cellspacing="0">
                    <tr>
                        <td height="22" colspan=2 class="subtitulo1">
                            <div align="left" class="subtitulo1">
                                <strong>Generar Proceso</strong>
                            </div>
                        </td>
                        <td width="212" class="barratitulo">
                            <img src="<%=BASEURL%>/images/titulo.gif" width="32" height="20" align="left">
                            <%=datos[0]%>
                        </td>
                    </tr>
                </table>
                <table width="100%"  border="0" align="center" cellpadding="2" cellspacing="3">                  
                  <tr class="fila">
                        <td width="31%"><strong>Fecha Inicial: </strong></td>
                        <td>
                            <input name="fechai" type="text" id="fechai" style='width:120'  readonly >
                            <a href="javascript:void(0)" onclick="if(self.gfPop)gfPop.fPopCalendar(form1.fechai);return false;" HIDEFOCUS><img src="<%=BASEURL%>\js\Calendario\cal.gif" width="16" height="16" border="0" alt="De click aqui para escoger la fecha"></a>
                        </td>
                  </tr>
                  <tr class="fila">
                        <td><strong>Fecha Final: </strong></td>
                        <td>
                            <input name="fechaf" type='text' id="fechaf" style='width:120' readonly  >
                            <a href="javascript:void(0)" onclick="if(self.gfPop)gfPop.fPopCalendar(form1.fechaf);return false;" HIDEFOCUS><img  src="<%=BASEURL%>\js\Calendario\cal.gif" width="16" height="16" border="0" alt="De click aqui para escoger la fecha"></a>                
                        </td>
                  </tr>                  
              </table>
            </td>
          </tr>
        </table>
        </td>
      </tr>
    </table>
    <br>
    <div align="center">      
        <img id="baceptar" name="baceptar"  src="<%=BASEURL%>/images/botones/aceptar.gif" style="cursor:hand"  onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" onClick="validaFechas();">
        <img src="<%=BASEURL%>/images/botones/salir.gif"  height="21" name="imgsalir"  onMouseOver="botonOver(this);" onClick="window.close();" onMouseOut="botonOut(this);" style="cursor:hand">            
    </div>
	</form>
    <br/>	        
  <%
if(!msg.equals("")){%>
	<p>
  <table width="407" border="2" align="center">
     <tr>
    <td><table width="100%"   align="center"  >
      <tr>
        <td width="229" align="center" class="mensajes"><%=msg%></td>
        <td width="29" background="<%=BASEURL%>/images/cuadronaranja.JPG">&nbsp;</td>
        <td width="58">&nbsp;</td>
      </tr>
    </table></td>
  </tr>
</table>
  </p>
   <%}%>
</div>	
<%=datos[1]%>
<iframe width=188 height=166 name="gToday:normal:agenda.js:gfPop:plugins.js" id="gToday:normal:agenda.js:gfPop:plugins.js" src="<%=BASEURL%>/js/Calendario/ipopeng.htm" scrolling="no" frameborder="0" style="visibility:visible; z-index:999; position:absolute; left:-500px; top:0px;"> </iframe>

</body>
</html>
