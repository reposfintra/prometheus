<%@page session="true"%>
<%@page import="com.tsp.util.*"%>
<%@page import="com.tsp.operation.model.beans.*"%>
<%@page import="java.util.*" %>
<%@include file="/WEB-INF/InitModel.jsp"%>
<%
  response.setHeader("Cache-Control","");
  String contentType = request.getParameter("tipoPage");
  if (!contentType.equals("file")){
      String tipoPage =  (contentType.equals("EXCEL"))?"application/vnd.ms-excel":"text/html"; 
      response.setContentType(tipoPage);
  }
  Vector  reporte = model.discrepanciaService.getDiscrepancias();
  Discrepanica discrepancia = null;
  String fechaIni = request.getParameter("fecini");
  String fechaFin = request.getParameter("fecfin");
  String op = request.getParameter("op");
%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
   "http://www.w3.org/TR/html4/loose.dtd">

<html>
    <head>
        <title>REPORTE CONTROL MIGRACION DISCREPANICA</title>
        <link href="css/estilostsp.css" rel="stylesheet" type="text/css">
    </head>
    <body>
        <%if (op == null){%>
        <table >
              <tr class="fila">
                 <td colspan="3">FINTRAVALORES S.A.</td>
              </tr>
              <tr class="fila">
                 <td colspan="3">REPORTE CONTROL MIGRACION DISCREPANICA</td>
              </tr>
              <tr class="fila">
                 <td colspan="3">FECHA DE EJECUCION: <%=(fechaIni + " - "+fechaFin)%></td>
              </tr>
              <tr class="tblTitulo">
                 <th nowrap>PLANILLA</th>
                 <th nowrap>DESCRIPCIÓN DE DISCREPANCIA</th>
                 <th nowrap>FECHA DISCREPANICA</th>
                 <th nowrap>TIPO DISCREPANCIA</th>
				 <th nowrap>SUBIDO MIMS</th>
              </tr>
            <%for(int i = 0; i < reporte.size(); i++){
                 discrepanicia = (Discrepanica)reporte.get(i);
             %>
              <tr class="<%=(i%2!=0)?"filaazul":"filagris"%>" >
                 <td nowrap align="center"><%=discrepanicia.getNro_planilla()%></td>
                 <td nowrap align="center"><%=discrepanicia.getDescripcion()%></td>
                 <td nowrap align="center"><%=discrepanicia.getFecha_creacion()%></td>
                 <td nowrap align="center"><%=discrepanicia.getCod_discrepancia()%></td>
                 <td nowrap align="center"><%=discrepanicia.getFecha_migracion()%></td>
              </tr>
            <%}
              model.repInfoOtMimPosSvc.setReporte(null);%>  
        </table>
        <%}else if (op.equals("file")){%>
        <table class="fondotabla">
              <tr>
                 <td class='normal'> 
                     SU ARCHIVO SE ESTA GENERANDO VAYA AL SERVICIO DE DESCARGAS  
                     <img src="images/file.gif"> PARA OBTENERNO...
                 </td>
              </tr>
        </table>
        <%}%>
    </body>
</html>
