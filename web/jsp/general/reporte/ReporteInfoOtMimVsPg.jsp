<%@page session="true"%>
<%@page import="com.tsp.util.*"%>
<%@page import="com.tsp.operation.model.beans.*"%>
<%@page import="java.util.*" %>
<%@include file="/WEB-INF/InitModel.jsp"%>
<%
  response.setHeader("Cache-Control","");
  String contentType = request.getParameter("tipoPage");
  if (!contentType.equals("file")){
      String tipoPage =  (contentType.equals("EXCEL"))?"application/vnd.ms-excel":"text/html"; 
      response.setContentType(tipoPage);
  }
  Vector  reporte = model.repInfoOtMimPosSvc.getReporte();
  RepInfoOtMimVsPos repInfoOtMimVsPos = null;
  String fechaIni = request.getParameter("fecini");
  String fechaFin = request.getParameter("fecfin");
  String op = request.getParameter("op");
%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
   "http://www.w3.org/TR/html4/loose.dtd">

<html>
    <head>
        <title>REPORTE INFORMACION DE OTS MIMS VS POSTGRES</title>
        <link href="css/estilostsp.css" rel="stylesheet" type="text/css">
    </head>
    <body>
        <%if (op == null){%>
        <table >
              <tr class="fila">
                 <td colspan="3">FINTRAVALORES S.A.</td>
              </tr>
              <tr class="fila">
                 <td colspan="3">INFORMACION DE OTS MIMS VS POSTGRES</td>
              </tr>
              <tr class="fila">
                 <td colspan="3">FECHA DE EJECUCION: <%=(fechaIni + " - "+fechaFin)%></td>
              </tr>
              <tr class="tblTitulo">
                 <th nowrap>FECHA</th>
                 <th nowrap>OT</th>
                 <th nowrap>DOCUMENTO INTERNO</th>
                 <th nowrap>DOCUMENTO INTERNO PG</th>
                 <th nowrap>DIF S/N </th>
                 <th nowrap>FACTURA COMERCIAL</th>
                 <th nowrap>FACTURA COMERCIAL PG</th>
                 <th nowrap>DIF S/N </th>
                 <th nowrap>CANTIDADES EN KG</th>
                 <th nowrap>CANTIDADES EN KG PG</th>
                 <th nowrap>DIF S/N </th>
                 <th nowrap>CONTENEDOR</th>
                 <th nowrap>CONTENEDOR PG</th>
                 <th nowrap>DIF S/N </th>
                 <th nowrap>PRECINTO</th>
                 <th nowrap>PRECINTO PG</th>
                 <th nowrap>DIF S/N </th>
                 <th nowrap>UNIDADES</th>
                 <th nowrap>UNIDADES PG</th>
                 <th nowrap>DIF S/N </th>
                 
              </tr>
            <%for(int i = 0; i < reporte.size(); i++){
                 repInfoOtMimVsPos = (RepInfoOtMimVsPos)reporte.get(i);
             %>
              <tr class="<%=(i%2!=0)?"filaazul":"filagris"%>" >
                 <td nowrap align="center"><%=repInfoOtMimVsPos.getFecha()%></td>
                 <td nowrap align="center"><%=repInfoOtMimVsPos.getOt()%></td>
                 <td nowrap align="center"><%=repInfoOtMimVsPos.getDocint()%></td>
                 <td nowrap align="center"><%=repInfoOtMimVsPos.getDocintpg()%></td>
                 <td nowrap align="center"><%=repInfoOtMimVsPos.getDifdocint()%></td>
                 <td nowrap align="center"><%=repInfoOtMimVsPos.getFactura()%></td>
                 <td nowrap align="center"><%=repInfoOtMimVsPos.getFacturapg()%></td>
                 <td nowrap align="center"><%=repInfoOtMimVsPos.getDiffactura()%></td>
                 <td nowrap align="center"><%=repInfoOtMimVsPos.getCantkg()%></td>
                 <td nowrap align="center"><%=repInfoOtMimVsPos.getCantkgpg()%></td>
                 <td nowrap align="center"><%=repInfoOtMimVsPos.getDifcantkg()%></td>
                 <td nowrap align="center"><%=repInfoOtMimVsPos.getContenedor()%></td>
                 <td nowrap align="center"><%=repInfoOtMimVsPos.getContenedorpg()%></td>
                 <td nowrap align="center"><%=repInfoOtMimVsPos.getDifcontenedor()%></td>
                 <td nowrap align="center"><%=repInfoOtMimVsPos.getPrecinto()%></td>
                 <td nowrap align="center"><%=repInfoOtMimVsPos.getPrecintopg()%></td>
                 <td nowrap align="center"><%=repInfoOtMimVsPos.getDifprecinto()%></td>
                 <td nowrap align="center"><%=repInfoOtMimVsPos.getUnidades()%></td>
                 <td nowrap align="center"><%=repInfoOtMimVsPos.getUnidadespg()%></td>
                 <td nowrap align="center"><%=repInfoOtMimVsPos.getDifunidades()%></td>
                 
              </tr>
            <%}
              model.repInfoOtMimPosSvc.setReporte(null);%>  
        </table>
        <%}else if (op.equals("file")){%>
        <table class="fondotabla">
              <tr>
                 <td class='normal'> 
                     SU ARCHIVO SE ESTA GENERANDO VAYA AL SERVICIO DE DESCARGAS  
                     <img src="images/file.gif"> PARA OBTENERNO...
                 </td>
              </tr>
        </table>
        <%}%>
        
        <%// response.addHeader("Cache-Control","no-cache"); %>
    </body>
</html>
