<%@ page session="true"%>
<%@ page errorPage="../error/ErrorPage.jsp"%>
<%@ include file="/WEB-INF/InitModel.jsp"%>
<%@ taglib uri="http://jakarta.apache.org/taglibs/input-0.90" prefix="input" %>
<%@page import="com.tsp.operation.model.*, com.tsp.operation.model.beans.*, com.tsp.util.*,java.util.*, java.text.*"%>

<html>
<head>
<title>Interface Migracion Facturas</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link href="../css/estilostsp.css" rel="stylesheet" type="text/css">
<script type='text/javascript' src="<%= BASEURL %>/js/boton.js"></script>
<link href="<%=BASEURL%>/css/estilostsp.css" rel="stylesheet" type="text/css">
<%
    String Mensaje = (request.getParameter("Mensaje")!=null)?request.getParameter("Mensaje"):"";
    String path = application.getRealPath(model.menuService.getRealUrl(request.getRequestURI()));
    String datos[] = model.menuService.getContenidoMenu(BASEURL,request.getRequestURI(),path);    
    System.out.println("mensaje:"+Mensaje);
%>
<script>
    function verificarArchivo(){
        if(form1.archivo.value == "" ){
            alert( "Debe seleccionar un archivo" );
            form1.archivo.focus();
            return;
        }
        form1.submit();
    }
	
</script>
</head>

<body>
<div id="capaSuperior" style="position:absolute; width:100%; height:100px; z-index:0; left: 1px; top: 0px;">
 <jsp:include page="/toptsp.jsp?encabezado=Migracion Facturas Clientes"/>
</div>
<div id="capaCentral" style="position:absolute; width:100%; height:87%; z-index:0; left: 0px; top: 100px; overflow: scroll;">        
<form id="form1" method="post" action="<%=CONTROLLER%>?estado=HImportaciones&accion=CF" enctype="multipart/form-data">
    
      <table width="500" border="2" align="center">
        <tr>
          <td><table width="100%"  border="0" align="center" class="tablaInferior">
            <tr>
              <td><table width="100%" border="0" cellpadding="0" cellspacing="0">
                  <tr>
                    <td height="22" colspan=2 class="subtitulo1" align="left">Cargar Archivo Remesa_Docto</td>
                    <td width="212" class="barratitulo"> <img src="<%=BASEURL%>/images/titulo.gif" width="32" height="20" align="left"> <%=datos[0]%>           
                  </tr>
                </table>
                  <table width="100%"  border="0" align="center" cellpadding="2" cellspacing="3">
                    <tr class="fila">
                      <td width="31%">Seleccione Archivo: </td>
                      <td><input type="file" name="archivo" id="archivo" style="width:100%"></td>
                    </tr> 
                </table></td>
            </tr>
          </table></td>
        </tr>
      </table>
      <br>
    <div align="center">      
        <p><img id="baceptar" name="baceptar" src="<%=BASEURL%>/images/botones/aceptar.gif" style="cursor:hand"  onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" onClick="verificarArchivo();">
         
            <img  src="<%=BASEURL%>/images/botones/cancelar.gif" style="cursor:hand" onMouseOver="botonOver(this);" onClick="window.reload();" onMouseOut="botonOut(this);">         
      
          <img src="<%=BASEURL%>/images/botones/salir.gif"  height="21" name="imgsalir"  onMouseOver="botonOver(this);" onClick="window.close();" onMouseOut="botonOut(this);" style="cursor:hand"> </p>

		<p>
  <%if(!Mensaje.equals("")){
  %>
</p>
		<p><table border="2" align="center">
          <tr>
            <td><table width="100%"  border="1" align="center"  bordercolor="#F7F5F4" bgcolor="#FFFFFF">
                <tr>
                  <td width="229" align="center" class="mensajes"><%= Mensaje %></td>
                  <td width="29" background="<%=BASEURL%>/images/cuadronaranja.JPG">&nbsp;</td>
                  <td width="58">&nbsp;</td>
                </tr>
            </table></td>
          </tr>
        </table>
        </div>
</form>
    </p>
 <%}%>
  
</div>	
<%=datos[1]%>
</body>
</html>
