<!--
- Autor : FERNEL VILLACOB DIAZ
- Date  : 10/01/2006  
- Copyrigth Notice : Fintravalores S.A. S.A
-->
<%--
-@(#)
--Descripcion : Vista que permite administrar software
--%>

<%@page    session   ="true"%> 
<%@page    errorPage ="/error/ErrorPage.jsp"%>
<%@page    import    ="com.tsp.operation.model.beans.*"%>
<%@include file      ="/WEB-INF/InitModel.jsp"%>
<%@page    import    ="com.tsp.util.*"%>
<%@page    import    ="java.util.*"%>


<% 
   Programa programa     = model.AdminSoft.getPrograma();
   List     filesProgram = programa.getListFile();  
   List     listExt      = model.AdminSoft.getExt();
   List     filesSitio   = model.AdminSoft.getFilesExt();   
   String   msj          = (request.getParameter("msj")==null)?"":request.getParameter("msj");%>


<html>
<head>
     <title>Administarcion Software</title>
     <link href="<%= BASEURL %>/css/estilostsp.css" rel="stylesheet" type="text/css"> 
     <script language="JavaScript" type="text/javascript" src="<%=BASEURL%>/js/script.js"></script>  
     <script language="JavaScript" type="text/javascript" src="<%=BASEURL%>/js/boton.js"></script>
       
     <script>
         <%= model.AdminSoft.getEquiposJS()%>
         <%= model.AdminSoft.getSitiosJS() %>
         
         var separadorRow     = '<%=model.AdminSoft.SEPERADOR_ROW%>';
         var separadorColumn  = '<%=model.AdminSoft.SEPERADOR_COLUMN%>';
             
        function loadEquipos(){
            var vec = equipos.split(separadorRow);
            formPage.equipo.length = vec.length;
            for(var i=0;i<(vec.length);i++){            
                 formPage.equipo.options[i].value = vec[i];
                 formPage.equipo.options[i].text  = vec[i];                 
                 if( vec[i]=='<%= programa.getEquipo() %>')
                      formPage.equipo.options[i].selected='selected';
            }            
         }             
             
         function loadSitios(){
                var vec   = sitios .split(separadorRow);
                var selec = formPage.equipo.value;
                cont=0;
                for(var i=0;i<(vec.length);i++){ 
                   var column = vec[i].split(separadorColumn);
                   if( column[0]==selec )
                      cont++;
                }
                formPage.sitio.length = cont;
                cont=0;
                for(var i=0;i<(vec.length);i++){ 
                   var column = vec[i].split(separadorColumn);
                   if( column[0]==selec ){               
                       formPage.sitio.options[cont].value = column[1];
                       formPage.sitio.options[cont].text  = column[1];
                       if( column[1]=='<%= programa.getSitio() %>')
                          formPage.sitio.options[cont].selected='selected';
                       cont++;
                   }                 
                }
         }      
   
         
                  
       function move(cmbO, cmbD){
           for (i=cmbO.length-1; i>=0 ;i--)
            if (cmbO[i].selected){
               addOption(cmbD, cmbO[i].value, cmbO[i].text)
               cmbO.remove(i);
            }
           //order(cmbD);
           deleteRepeat(cmbD);
        }
        
        function addOption(Comb,valor,texto){
           if(valor!='' && texto!=''){
                var Ele = document.createElement("OPTION");
                Ele.value=valor;
                Ele.text=texto;
                Comb.add(Ele);
          }
        }  
        
        function order(cmb){
           for (i=0; i<cmb.length;i++)
             for (j=i+1; j<cmb.length;j++)
                if (cmb[i].text > cmb[j].text){
                    var temp = document.createElement("OPTION");
                    temp.value = cmb[i].value;
                    temp.text  = cmb[i].text;
                    cmb[i].value = cmb[j].value;
                    cmb[i].text  = cmb[j].text;
                    cmb[j].value = temp.value;
                    cmb[j].text  = temp.text;
                }
        }
        
        function deleteRepeat(cmb){
          var ant='';
          for (i=0;i<cmb.length;i++){
             if(ant== cmb[i].value)
                cmb.remove(i);
             ant = cmb[i].value;
          }
        }
        
        
        function selectAll(cmb){
            for (i=0;i<cmb.length;i++)
              cmb[i].selected = true;
        }
         
       function sendAdmin(opcion){
            formPage.action += "&evento=" + opcion;
            selectAll( formPage.programFile );
            var sw = 0;            
            if(opcion=='save'){
               if( formPage.equipo.value==''  ||  formPage.sitio.value=='' || formPage.programa.value=='' || formPage.descripcion.value=='' || formPage.programador.value==''  || formPage.programFile.value==''){
                   formPage.programa.focus();
                   alert('Deberá digitar todos los campos para la transacción....')
                   sw=1;
               }
            }            
            if(sw==0)
               formPage.submit();
         }
         
         
         
       function searchCombo(data, cmb){
           data = data.toLowerCase();
           spaceRutaFile.innerHTML='';
           for (i=0;i<cmb.length;i++){
              cmb[i].selected = false; 
           }           
           for (i=0;i<cmb.length;i++){                
                var txt = cmb[i].text.toLowerCase( );
                if( txt.indexOf(data)==0 ){
                    cmb[i].selected = true;
                    spaceRutaFile.innerHTML=  cmb[i].label; 
                    break;
               }
               else
                  cmb[i].selected = false;
            }
        }
         
         
        
         
    </script>
    
    
</head>

<body onLoad="redimensionar();" onResize="redimensionar();">
<div id="capaSuperior" style="position:absolute; width:100%; height:100px; z-index:0; left: 0px; top: 0px;">
<jsp:include page="/WEB-INF/toptsp.jsp?encabezado=Administración de Software"/>
</div>

<div id="capaCentral" style="position:absolute; width:100%; height:85%; z-index:0; left: 0px; top: 100px; overflow: scroll;"> 
<center>



<form  action="<%= CONTROLLER %>?estado=Admin&accion=Software"   method='post' name='formPage'  id='formPage' >
<table width="750" border="2"    align="center">
   <tr>
      <td>  
      
           <table width='100%' align='center' class='tablaInferior'>           
                 <tr class="barratitulo">
                    <td colspan='2' >
                       <table cellpadding='0' cellspacing='0' width='100%'>
                         <tr>
                          <td align="left" width='55%' class="subtitulo1">&nbsp;Formación de Programas</td>
                          <td align="left" width='*'  ><img src="<%= BASEURL %>/images/titulo.gif" width="32" height="20"></td>
                        </tr>
                       </table>
                    </td>
                 </tr> 
                 <tr class='fila'>
                    <td width='20%'>Distrito</td>
                    <td width='*'  >
                              <select name='distrito'>
                                 <option value='FINV'>FINV</option>
                              </select>
                    </td>
                 </tr> 
                 <tr class='fila'>
                    <td width='20%'>Equipo:</td>
                    <td width='*'  ><select name='equipo' style='width:50%' onchange='loadSitios()'></select></td>
                 </tr>                 
                 <tr class='fila'>
                    <td>Sitio:</td>
                    <td>           <select name='sitio' style='width:50%'></select></td>
                 </tr>                 
                 <tr class='fila'>
                    <td>Programa:</td>
                    <td><input type='text' size='10' maxlength='10' name='programa'    title='Codigo del programa'     value='<%=programa.getPrograma()%>'></td>
                 </tr>                 
                  <tr class='fila'>
                    <td>Descripción:</td>
                    <td><input type='text' style='width:95%' maxlength='100'        name='descripcion'  title='Nombre de la Opcion en el Menu' value='<%= programa.getDescripcion()%>' ></td>
                 </tr>                 
                  <tr class='fila'>
                    <td>Programador:</td>
                    <td><input type='text'  style='width:95%' maxlength='100'       name='programador'  title='Persona que creo el programa'  value='<%= programa.getProgramador()%>' ></td>
                 </tr>   
              
                 <tr class='fila'>
                    <td colspan='2' align='center'>
                        <table cellpadding='0'  cellspacing='0' width='98%'>
                        
                           <tr  class='fila'>
                              <td width='47%' align='center' >
                              
                                  <table cellpadding='0'  cellspacing='0' width='70%' >                                    
                                    <tr  class='fila'>
                                       <td width='60%'> ARCHIVOS DEL SITIO</td>
                                       <td width='*' align='center'><img src='<%=BASEURL%>/images/botones/iconos/lupa.gif'   style=" cursor:hand'"  title='Load File....'   name='i_load'      onclick="sendAdmin('load')"    > </td>                                       
                                    </tr>
                                  </table>                                
                                  <table cellpadding='0'  cellspacing='0' width='100%'>   
                                    <tr class='fila'>
                                        <td width='25%'>Ext:</td>
                                        <td width='*'  >
                                            <select name='ext' style="width:40%" title='Buscar por Extensión de archivos' onchange="sendAdmin('extension')">
                                            <% for(int i=0; i<listExt.size();i++){
                                                  String ext  = ( String)listExt.get(i);  
                                                  String sel  = ( ext.equals( model.AdminSoft.getExtension() )) ? " selected='selected' ":"";%>
                                                  <option  value='<%=ext%>'  <%=sel%> > <%=ext%></option>
                                            <%}%>
                                            </select>                                        
                                        </td> 
                                    </tr>
                                    <tr class='fila' >
                                       <td width='25%'>Buscar Por:</td>
                                       <td width='*'  ><input type='text' name='busqueda' style="width:100%" title='Buscar por palabras' onkeyup='searchCombo(this.value, this.form.programSitio)' onkeypress='searchCombo(this.value, this.form.programSitio)'> </td>
                                    </tr>
                                  </table>
                                  
                                      <select multiple size='10' class='select' style='width:100%' name='programSitio' onchange=" if (this.selectedIndex!=-1) { window.status=this[this.selectedIndex].text;  spaceRutaFile.innerHTML= this[this.selectedIndex].label;  }">
                                           <% for(int i=0; i<filesSitio.size();i++){
                                                  InventarioSoftware  file = ( InventarioSoftware)filesSitio.get(i);%>
                                                  <option value='<%=file.getId()%>' label='<%= file.getRuta() %>'>  <%=file.getNombre()+"."+ file.getExtension()%> </option>
                                           <%}%>
                                      </select>
                                   
                              </td>
                              <td width='6%' align='center'>
                                   <br>
                                   <img src='<%=BASEURL%>/images/botones/envDer.gif' style='cursor:hand' name='imgEnvDer'  onclick="move   (programSitio,  programFile); " onMouseOver='botonOver(this);' onMouseOut='botonOut(this);'><br>             
                                   <img src='<%=BASEURL%>/images/botones/envIzq.gif' style='cursor:hand' name='imgEnvIzq'  onclick="move   (programFile, programSitio ); " onMouseOver='botonOver(this);' onMouseOut='botonOut(this);'><br>            
                              </td>
                              <td width='47%' align='center' >
                                   <table cellpadding='0'  cellspacing='0'>
                                      <tr  class='fila'> <td height='75' align='center' > ARCHIVOS PARA EL PROGRAMA </td> </tr>
                                    </table>
                                  
                                   <select multiple size='10' class='select' style='width:100%' name='programFile' onchange=" if (this.selectedIndex!=-1) { window.status=this[this.selectedIndex].text;}">
                                    <% for(int i=0; i<filesProgram.size();i++){
                                          InventarioSoftware  file = ( InventarioSoftware)filesProgram.get(i); %>
                                          <option value='<%=file.getId()%>'><%=file.getNombre()+"."+ file.getExtension()%></option>
                                   <%}%>
                                   </select>
                              </td>                           
                           </tr>    
                    
                           <tr  class='fila'>
                               <td colspan='3' width='100%' height='40'>
                                  RUTA :<br><font id='spaceRutaFile'  style=" width:90%; font size:11"></font<BR>
                               </td>
                           </tr>
                           
                        </table>    
                        <br>
                    </td>
                 </tr> 
                 
                 
                 
                 
                 
            </table>
            
        </td>
    </tr>
 </table>
 
  <br>
  <img src='<%=BASEURL%>/images/botones/cancelar.gif'   style=" cursor:hand'"  title='Limpiar....'   name='i_cancel'      onclick="sendAdmin('reset')"     onMouseOver='botonOver(this);' onMouseOut='botonOut(this);'> 
  <img src='<%=BASEURL%>/images/botones/aceptar.gif'    style=" cursor:hand'"  title='Crear....'     name='i_crear'       onclick="sendAdmin('save')"      onMouseOver='botonOver(this);' onMouseOut='botonOut(this);'> 
  <img src='<%=BASEURL%>/images/botones/salir.gif'      style='cursor:hand'    title='Salir...'      name='i_salir'       onclick='parent.close();'        onMouseOver='botonOver(this);' onMouseOut='botonOut(this);'>
	 
 </form>

 
 
 <!--  Load combos -->
 <script>
     loadEquipos();
     loadSitios();
 </script>
 

 
 <!-- Mensaje -->
 <% if(!msj.equals("")){%>
      <table border="2" align="center" width='600'>
          <tr>
              <td>
                <table width="100%"  border="1" align="center"  bordercolor="#F7F5F4" bgcolor="#FFFFFF">
                        <tr>
                        <td width="500" align="center" class="mensajes"><%=msj%></td>
                        <td width="29" background="<%=BASEURL%>/images/cuadronaranja.JPG">&nbsp;</td>
                        <td width="58">&nbsp;</td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
   <%}%>
 
 
 
</div>

</body>
</html>
