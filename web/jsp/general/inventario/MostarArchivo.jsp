<!--
- Autor : FERNEL VILLACOB DIAZ
- Date  : 10/01/2006  
- Copyrigth Notice : Fintravalores S.A. S.A
-->
<%--
-@(#)
--Descripcion : Vista que permite mostrar los archivos de un programa
--%>

<%@page    session   ="true"%> 
<%@page    errorPage ="/error/ErrorPage.jsp"%>
<%@page    import    ="com.tsp.operation.model.beans.*"%>
<%@include file      ="/WEB-INF/InitModel.jsp"%>
<%@page    import    ="com.tsp.util.*"%>
<%@page    import    ="java.util.*"%>


<html>
<head>
     <title>Archivos del Programa</title>
     <link href="<%= BASEURL %>/css/estilostsp.css" rel="stylesheet" type="text/css"> 
     <script language="JavaScript" type="text/javascript" src="<%=BASEURL%>/js/script.js"></script>  
     <script language="JavaScript" type="text/javascript" src="<%=BASEURL%>/js/boton.js"></script>
     <script language="JavaScript" type="text/javascript" src="<%=BASEURL%>/js/finanzas/presupuesto/move.js"></script>
   
</head>
<body  >

<div id="capaSuperior" style="position:absolute; width:100%; height:100px; z-index:0; left: 0px; top: 0px;">
<jsp:include page="/WEB-INF/toptsp.jsp?encabezado=Revisión de Archivos"/>
</div>


<div id="capaCentral" style="position:absolute; width:100%; height:85%; z-index:0; left: 0px; top: 100px; overflow: scroll;"> 
<center>

<% InventarioSoftware archivo     =  (request.getAttribute("archivo")==null)? new InventarioSoftware() : (InventarioSoftware )request.getAttribute("archivo") ;
   List       listRevision        =  archivo.getRevisiones(); %>
   
   <table width="900" border="2"    align="center">
   <tr>
      <td> 
           <table width='100%' align='center' class='tablaInferior'>           
                 <tr class="barratitulo">
                    <td colspan='2' >
                       <table cellpadding='0' cellspacing='0' width='100%'>
                         <tr>
                          <td align="left" width='55%' class="subtitulo1">&nbsp;Archivo</td>
                          <td align="left" width='*'  ><img src="<%= BASEURL %>/images/titulo.gif" width="32" height="20"></td>
                        </tr>
                       </table>
                    </td>
                 </tr>                  
                 <tr class='fila'>
                    <td width='10%'>Archivo:</td>
                    <td width='*'  ><%= archivo.getNombre() +"."+ archivo.getExtension()%></td>              
                 </tr>
                 <tr class='fila'>
                    <td width='10%'>Equipo:</td>
                    <td width='*'  ><%= archivo.getEquipo() %></td>              
                 </tr>
                 <tr class='fila'>
                    <td width='10%'>Sitio:</td>
                    <td width='*'  ><%= archivo.getSitio() %></td>              
                 </tr>
                 <tr class='fila'>
                    <td width='10%'>Ruta:</td>
                    <td width='*'  ><%= archivo.getRuta() %></td>              
                 </tr>                 
                 <tr class="barratitulo">
                    <td colspan='2' >
                       <table cellpadding='0' cellspacing='0' width='100%'>
                         <tr>
                          <td align="left" width='55%' class="subtitulo1">&nbsp;Revisiones</td>
                          <td align="left" width='*'  ><img src="<%= BASEURL %>/images/titulo.gif" width="32" height="20"></td>
                        </tr>
                       </table>
                    </td>
                 </tr>
                 <tr>
                    <td colspan='2' align='center' >
                        <table width='100%' border="1" bordercolor="#999999" bgcolor="#F7F5F4" align="center">
                          <tr class="tblTitulo">
                              <TH  width='10%'>Usuario         </TH>
                              <TH  width='15%'>Tipo Revisión   </TH>
                              <TH  width='50%'>Observacion     </TH>               
                              <TH  width='10%'>Aprobado        </TH>               
                              <TH  width='*'  >Fecha           </TH>               
                         </tr>
                            <% for(int i=0; i<listRevision.size();i++){
                              Revision revision = (Revision)listRevision.get(i);%>
                              <tr class='<%= (i%2==0?"filagris":"filaazul") %>' onMouseOver='cambiarColorMouse(this)'   >
                                <td class='bordereporte' width='10%'               ><%= revision.getUser()          %></td>
                                <td class='bordereporte' width='15%' align='center'><%= revision.getTipoRevision()  %></td>
                                <td class='bordereporte' width='50%'               ><%= revision.getObservacion()   %></td>
                                <td class='bordereporte' width='10%' align='center'><%= revision.getAprovado()      %></td>
                                <td class='bordereporte' width='*'   align='right' ><%= revision.getFecha()         %></td>
                              </tr>
                          <%}%>
                       </table>
                       
                    </td>                            
                 </tr>
             </table>
          </td>
       </tr>
    </table>
    <br>
     
    
    
    
   <!--  Tabla de Revision  -->
   <div  id='rev'   style=" position:absolute; left:250; top:100; visibility:hidden "  >  
      <script language="JavaScript" type="text/javascript" src="<%=BASEURL%>/js/script.js"></script> 
         <table width="550" border="1" cellpadding='0' cellspacing='0'    align="center" bordercolor="#123456">
         <tr class='fila'>
            <td align='center'>
                     <form  action="<%= CONTROLLER %>?estado=Revision&accion=Software&evento=revisionfile"   method='post' name='formRev'  id='formRev' > 
                      <table  width='100%'  class='tablaInferior'>
                        <tr class="barratitulo">
                            <td colspan='2' >
                               <table cellpadding='0' cellspacing='0' width='100%'>
                                 <tr  style='cursor: move;' ondragstart='' onMouseDown="_initMove('rev');">
                                  <td align="left" width='55%' class="subtitulo1">&nbsp;Nueva Revisión</td>
                                  <td align="left" width='*'  ><img src="<%= BASEURL %>/images/titulo.gif" width="32" height="20"></td>
                                </tr>
                               </table>
                            </td>
                         </tr> 
                         <tr class='fila'>
                            <td width='20%' > &nbsp Tipo Revisión</td>
                            <td width='*'> 
                               <select name='tipoRevision' style='width:40%' >
                                  <option value='Funcional'>Funcional</option>
                                  <option value='Tecnica'>Tecnica</option>
                                </select>
                            </td>
                         </tr>
                         <tr class='fila' valign='top'>
                            <td> <br>&nbsp Observación</td>
                            <td> <textarea style="width:100%" rows='10' name='observacion'  class='textbox'></textarea></td>
                         </tr>
                         <tr class='fila'>
                            <td > &nbsp Aprobado</td>
                            <td> 
                                <select name='aprobacion'>
                                  <option value='N'>No</option>
                                  <option value='S'>Si</option>
                                </select>
                            </td>
                         </tr>
                      </table>
                      </form >
                      <br>
                      <img src='<%=BASEURL%>/images/botones/aceptar.gif'    style=" cursor:hand'"  title='Guardar Revisión'  name='i_aceptar'     onclick="javascript:if( formRev.observacion.value!='' ){formRev.submit();}else{ alert('Deberá digitar una observación'); formRev.observacion.focus();}"   onMouseOver='botonOver(this);' onMouseOut='botonOut(this);'> 
                      <img src='<%=BASEURL%>/images/botones/cancelar.gif'   style='cursor:hand'    title='Cancelar...'       name='i_cancel'      onclick="rev.style.visibility='hidden'"  onMouseOver='botonOver(this);' onMouseOut='botonOut(this);'>
                      <br>
                      <br>   
              </td>
          </tr>
       </table>  
   </div>
  
   
   
   <br>
   <img src='<%=BASEURL%>/images/botones/agregar.gif'    style=" cursor:hand'"  title='Adjuntar  Revision al Archivo '   name="imgmod"        onClick="rev.style.visibility='';  formRev.observacion.focus();"  onMouseOver='botonOver(this);' onMouseOut='botonOut(this);'>     
   <img src='<%=BASEURL%>/images/botones/salir.gif'      style='cursor:hand'    title='Salir...'                         name='i_salir'       onclick='parent.close();'                                         onMouseOver='botonOver(this);' onMouseOut='botonOut(this);'>


</div>

</body>
</html>
