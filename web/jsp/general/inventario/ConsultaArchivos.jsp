
<!--
- Autor : FERNEL VILLACOB DIAZ
- Date  : 10/01/2006  
- Copyrigth Notice : Fintravalores S.A. S.A
-->
<%--
-@(#)
--Descripcion : Vista que permite visualizar informacion de un file
--%>

<%@page    session   ="true"%> 
<%@page    errorPage ="/error/ErrorPage.jsp"%>
<%@page    import    ="com.tsp.operation.model.beans.*"%>
<%@include file      ="/WEB-INF/InitModel.jsp"%>
<%@page    import    ="com.tsp.util.*"%>
<%@page    import    ="java.util.*"%>

<html>
<head>
    <title>Revision Archivos</title>
    <link href="<%= BASEURL %>/css/estilostsp.css" rel="stylesheet" type="text/css"> 
    <script language="JavaScript" type="text/javascript" src="<%=BASEURL%>/js/script.js"></script>  
    <script language="JavaScript" type="text/javascript" src="<%=BASEURL%>/js/boton.js"></script>
 
</head>
<body>


<div id="capaSuperior" style="position:absolute; width:100%; height:100px; z-index:0; left: 0px; top: 0px;">
<jsp:include page="/WEB-INF/toptsp.jsp?encabezado=Consulta de Archivos"/>
</div>


<div id="capaCentral" style="position:absolute; width:100%; height:85%; z-index:0; left: 0px; top: 100px; overflow: scroll;"> 
<center>

<%  InventarioSoftware  file = (InventarioSoftware) request.getAttribute("fileInventario");
    List        listRevFile  = file.getRevisiones();%> 

            <table   cellpadding='0' cellspacing='0' width='90%'    align="center" >  
                 <tr class="barratitulo">
                    <td colspan='2' >
                       <table cellpadding='0' cellspacing='0' width='100%'>
                         <tr>
                          <td align="left" width='55%' class="subtitulo1">&nbsp;Revisi�n Archivo : <%= file.getNombre() +"."+ file.getExtension() %> </td>
                          <td align="left" width='*'  ><img src="<%= BASEURL %>/images/titulo.gif" width="32" height="20"></td>
                        </tr>
                       </table>
                    </td>
                  </tr> 
                  <tr class="barratitulo">
                        <td >
                           <table width='100%' border="1" bordercolor="#999999" bgcolor="#F7F5F4" align="center">
                              <tr class="tblTitulo">
                                  <TH  nowrap width='15%'>Usuario         </TH>
                                  <TH  nowrap width='15%'>Tipo Rev.       </TH>
                                  <TH  nowrap width='35%'>Observacion     </TH>               
                                  <TH  nowrap width='10%'>Apro.           </TH>               
                                  <TH  nowrap width='*'  >Fecha           </TH>               
                             </tr>                                                                          
                            <% for(int j=0; j<listRevFile.size();j++){
                                  Revision revision = (Revision)listRevFile.get(j);%>
                                  <tr class='<%= (j%2==0?"filagris":"filaazul") %>'style="font size:11" onMouseOver='cambiarColorMouse(this)'>
                                    <td class="bordereporte" width='15%'               ><%= revision.getUser()          %></td>
                                    <td class="bordereporte" width='15%'               ><%= revision.getTipoRevision()  %></td>
                                    <td class="bordereporte" width='35%'               ><%= revision.getObservacion()   %></td>
                                    <td class="bordereporte" width='10%' align='center'><%= revision.getAprovado()      %></td>
                                    <td class="bordereporte" width='*'   align='right' ><%= revision.getFecha()         %></td>
                                  </tr>
                              <%}%>
                           </table>
                          </td>                            
                  </tr>
                  <tr>
                       <td height='40' align='center'>
                           <br>
                           <img src='<%=BASEURL%>/images/botones/salir.gif'      style='cursor:hand'    title='Salir...'              name='i_salir'       onclick='parent.close();'  onMouseOver='botonOver(this);' onMouseOut='botonOut(this);'>
                           <br><br>
                       </td>
                 </tr>
             </table>


</body>
</html>
