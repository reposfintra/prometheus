<!--
- Autor : Ing. Armando Oviedo C
- Date  : 27 de Diciembre de 2005
- Copyrigth Notice : Fintravalores S.A. S.A
-->
<%--
-@(#)
--Descripcion : Pagina JSP, que genera un reporte de inventario de software en excel
--%>

<%-- Declaracion de librerias--%>
<%@page contentType="text/html"%>
<%@page session="true"%>
<%@page errorPage="../error/ErrorPage.jsp"%>
<%@page import="java.util.*"%>
<%@page import="com.tsp.operation.model.*"%>
<%@page import="com.tsp.util.*"%>
<%@page import="com.tsp.operation.model.beans.*, java.util.*"%>
<%@include file="/WEB-INF/InitModel.jsp"%>
<%@ taglib uri="http://jsptags.com/tags/navigation/pager" prefix="pg" %>

<html>
<head>
    <title>Generar Reporte Inventario Software</title>
    <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
    <link href="<%= BASEURL %>/css/estilostsp.css" rel="stylesheet" type="text/css">
    <script type='text/javascript' src="<%= BASEURL %>/js/boton.js"></script>
    <script type='text/javascript' src="<%= BASEURL %>/js/validar.js"></script>
    <link href="../../../css/estilostsp.css" rel="stylesheet" type="text/css">
</head>

<body onresize="redimensionar()" onload = 'redimensionar()'>

<div id="capaSuperior" style="position:absolute; width:100%; height:100px; z-index:0; left: 0px; top: 0px;">
	<jsp:include page="/WEB-INF/toptsp.jsp?encabezado=Generar Reporte Inventario Software"/>
</div>
<div id="capaCentral" style="position:absolute; width:100%; height:87%; z-index:0; left: 0px; top: 100px; overflow: scroll;">


    <% ResourceBundle  rb       = ResourceBundle.getBundle("com/tsp/util/connectionpool/db");
       String          equipo   = rb.getString("equipo");
       String          sitio    =  request.getContextPath().replaceAll("/","");
       String          mensaje  = request.getParameter("mensaje");%>
       
            <form action="<%=CONTROLLER%>?estado=InventarioSoftware&accion=Generar&mensaje=listar" name="forma" method="post">
              <table width="400"  border="2" align="center">
                  <tr>
                      <td colspan="2">
                            <table width="100%">
                                 <tr>
                                    <td colspan='2'>
                                         <table width="100%">
                                           <tr>
                                              <td width="40%" class="subtitulo1" nowrap>Generar Reporte Inventario</td>
                                              <td width="60%" class="barratitulo"><img src="<%=BASEURL%>/images/titulo.gif" width="32" height="20"></td>
                                           </tr>
                                         </table>
                                    </td>  
                                 </tr>

                                 <tr class="fila">
                                      <td width='20%'>Equipo</td> 
                                      <td width='*' >  
                                           <select name='equipo' style='width:90%'>
                                              <option value='<%= equipo %>'><%= equipo %></option>
                                           </select>
                                      </td>
                                 </tr> 
                                 <tr class="fila">
                                      <td>Sitio</td> 
                                      <td >  
                                           <select name='sitio' style='width:90%'>
                                              <option value='<%= sitio %>'><%= sitio %></option>
                                           </select> 
                                      </td>
                                 </tr> 
                                 <tr>
                                      <td class="fila" colspan="2"><input type="checkbox" name="grabarBD">Actualizar base de datos</input></td>
                                 </tr>    
                           </table>
                      </td>
                  </tr>
              </table>                  
              <br>
              <table align=center width='450'>
                <tr>
                    <td align="center" colspan="2">
                        <img title='Generar' src="<%= BASEURL %>/images/botones/exportarExcel.gif" onmouseover="botonOver(this);" onmouseout="botonOut(this);" onclick="forma.submit();"></img>                        
                        <img title='Salir'   src="<%= BASEURL %>/images/botones/salir.gif" onmouseover="botonOver(this);" onmouseout="botonOut(this);" onClick="window.close()"></img>
                    </td>
                </tr>
              </table>
            </form>
                
                
                
       <%if(mensaje!=null){%>
		<table width=450 border=2 align=center>
                    <tr>
                       <td>
                         <table width='100%'  border=1 align='center'  bordercolor='#F7F5F4' bgcolor='#FFFFFF'>
                              <tr><td width=350 align=center class=mensajes><%=mensaje%></td><td width=100><img src="<%=BASEURL%>/images/cuadronaranja.JPG"></td></tr>
                         </table>
                        </td>
                    </tr>
		</table>  
       <%}%>
       
</div>
</body>
</html>
