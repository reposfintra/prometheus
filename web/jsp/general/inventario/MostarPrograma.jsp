<!--
- Autor : FERNEL VILLACOB DIAZ
- Date  : 10/01/2006  
- Copyrigth Notice : Fintravalores S.A. S.A
-->
<%--
-@(#)
--Descripcion : Vista que permite mostrar un programa
--%>

<%@page    session   ="true"%> 
<%@page    errorPage ="/error/ErrorPage.jsp"%>
<%@page    import    ="com.tsp.operation.model.beans.*"%>
<%@include file      ="/WEB-INF/InitModel.jsp"%>
<%@page    import    ="com.tsp.util.*"%>
<%@page    import    ="java.util.*"%>

<html>
<head>
    <title>Mostrar Programa</title>
    <link href="<%= BASEURL %>/css/estilostsp.css" rel="stylesheet" type="text/css"> 
    <script language="JavaScript" type="text/javascript" src="<%=BASEURL%>/js/script.js"></script>  
    <script language="JavaScript" type="text/javascript" src="<%=BASEURL%>/js/boton.js"></script>
    
    <script language="JavaScript" type="text/javascript" src="<%=BASEURL%>/js/finanzas/presupuesto/move.js"></script>
      
</head>
<body  >

<div id="capaSuperior" style="position:absolute; width:100%; height:100px; z-index:0; left: 0px; top: 0px;">
<jsp:include page="/WEB-INF/toptsp.jsp?encabezado=Revisi�n de Software"/>
</div>


<div id="capaCentral" style="position:absolute; width:100%; height:85%; z-index:0; left: 0px; top: 100px; overflow: scroll;"> 
<center>
    
    <% Programa programa     =  (request.getAttribute("programa")==null)? new Programa() : (Programa )request.getAttribute("programa") ;
       List     listRevision =  programa.getListRevisiones();
       List     listFiles    =  programa.getListFile();   
       String   swFile       =  (request.getParameter("swFile")==null)?"":request.getParameter("swFile");%>
    
  <table width="950" border="2"    align="center">
   <tr>
      <td> 
           <table width='100%' align='center' class='tablaInferior'>           
                 <tr class="barratitulo">
                    <td  >
                       <table cellpadding='0' cellspacing='0' width='100%'>
                         <tr>
                          <td align="left" width='55%' class="subtitulo1">&nbsp;Programa</td>
                          <td align="left" width='*'  ><img src="<%= BASEURL %>/images/titulo.gif" width="32" height="20"></td>
                        </tr>
                       </table>
                    </td>
                 </tr> 
                 <tr class='fila'>
                    <td width='100%'>
                       <table cellpadding='0' cellspacing='0' width='100%'>
                          <tr class='fila'>
                                <td width='12%'>CODIGO:                         </td>
                                <td width='15%'  ><%= programa.getPrograma() %> </td>              
                                <td width='12%'>DESCRIPCI�N:                    </td>
                                <td width='*'  ><%= programa.getDescripcion()%> </td>  
                          </tr>
                       </table>
                    </td>
                 </tr>
                 
                 <tr class='fila'>
                    <td width='100%'>
                        <table cellpadding='0' cellspacing='0' width='100%'>
                          <tr class='fila'>
                                <td width='12%' >EQUIPO:                        </td>
                                <td width='15%' ><%=  programa.getEquipo()%>    </td>                          
                                <td width='12%' >SITIO:                         </td>
                                <td width='15%' ><%= programa.getSitio()%>      </td> 
                                <td width='12%' >PROGRAMADOR:                   </td>
                                <td width='*'  ><%= programa.getProgramador()%> </td>
                           </tr>
                       </table>
                    </td>
                 </tr>
                 
                 
                 <tr class='fila'>
                    <td width='100%'>
                        <table cellpadding='0' cellspacing='0' width='100%'>
                          <tr class='fila'>
                                <td width='12%'>USUARIO                    </td>
                                <td width='15%'><%= programa.getUser()%>   </td>                            
                                <td width='12%'>FECHA:                     </td>
                                <td width='*'  ><%= programa.getFecha()%>  </td>  
                            </tr>
                       </table>
                    </td>
                 </tr>
                 
                 
                 <tr class="barratitulo">
                    <td colspan='4' >
                       <table cellpadding='0' cellspacing='0' width='100%'>
                         <tr>
                          <td align="left" width='55%' class="subtitulo1">&nbsp;Revisiones</td>
                          <td align="left" width='*'  ><img src="<%= BASEURL %>/images/titulo.gif" width="32" height="20"></td>
                        </tr>
                       </table>
                    </td>
                 </tr> 
                 <tr>
                    <td colspan='4' align='center' >
                       <table width='100%' border="1" bordercolor="#999999" bgcolor="#F7F5F4" align="center">
                          <tr class="tblTitulo">
                              <TH  width='10%'>Usuario         </TH>
                              <TH  width='13%'>Tipo Revisi�n   </TH>
                              <TH  width='55%'>Observacion     </TH>               
                              <TH  width='8%' >Aprobado        </TH>               
                              <TH  width='*'  >Fecha           </TH>               
                         </tr>
                            <% for(int i=0; i<listRevision.size();i++){
                              Revision revision = (Revision)listRevision.get(i);%>
                              <tr class='<%= (i%2==0?"filagris":"filaazul") %>' onMouseOver='cambiarColorMouse(this)'  >
                                <td class='bordereporte' width='10%'               ><%= revision.getUser()          %></td>
                                <td class='bordereporte' width='13%' align='center'><%= revision.getTipoRevision()  %></td>
                                <td class='bordereporte' width='55%'               ><%= revision.getObservacion()   %></td>
                                <td class='bordereporte' width='8%'  align='center'><%= revision.getAprovado()      %></td>
                                <td class='bordereporte' width='*'   align='right' ><%= revision.getFecha()         %></td>
                              </tr>
                          <%}%>
                       </table>
                       
                    </td>                            
                 </tr>
            </table> 
         </td>
      </tr>
   </table>
   <br>
       
   
   <!--  Tabla de Revision  -->
   <div  id='rev'   style=" position:absolute;  visibility:hidden "  >  
      <script language="JavaScript" type="text/javascript" src="<%=BASEURL%>/js/script.js"></script> 
         <table width="550" border="1" cellpadding='0' cellspacing='0'    align="center" bordercolor="#123456">
         <tr class='fila'>
            <td align='center'>
                     <form  action="<%= CONTROLLER %>?estado=Revision&accion=Software&evento=revisionprogram"   method='post' name='formRev'  id='formRev' > 
                      <table  width='100%'  class='tablaInferior'>
                        <tr class="barratitulo">
                            <td colspan='2' >
                               <table cellpadding='0' cellspacing='0' width='100%'>
                                 <tr  >
                                  <td align="left" width='55%' class="subtitulo1">&nbsp;Nueva Revisi�n</td>
                                  <td align="left" width='*'  ><img src="<%= BASEURL %>/images/titulo.gif" width="32" height="20"></td>
                                </tr>
                               </table>
                            </td>
                         </tr> 
                         <tr class='fila'>
                            <td width='20%' > &nbsp Tipo Revisi�n</td>
                            <td width='*'> 
                               <select name='tipoRevision' style='width:40%' >
                                  <option value='Funcional'>Funcional</option>
                                  <option value='Tecnica'>Tecnica</option>
                                </select>
                            </td>
                         </tr>
                         <tr class='fila' valign='top'>
                            <td> <br>&nbsp Observaci�n</td>
                            <td> <textarea style="width:100%" rows='10' name='observacion'  class='textbox'></textarea></td>
                         </tr>
                         <tr class='fila'>
                            <td > &nbsp Aprobado</td>
                            <td> 
                                <select name='aprobacion'>
                                  <option value='N'>No</option>
                                  <option value='S'>Si</option>
                                </select>
                            </td>
                         </tr>
                      </table>
                      </form >
                      <img src='<%=BASEURL%>/images/botones/aceptar.gif'    style=" cursor:hand'"  title='Guardar Revisi�n'  name='i_aceptar'     onclick="javascript:if( formRev.observacion.value!='' ){formRev.submit();}else{ alert('Deber� digitar una observaci�n'); formRev.observacion.focus();}"   onMouseOver='botonOver(this);' onMouseOut='botonOut(this);'> 
                      <img src='<%=BASEURL%>/images/botones/cancelar.gif'   style='cursor:hand'    title='Cancelar...'       name='i_cancel'      onclick="rev.style.visibility='hidden'"  onMouseOver='botonOver(this);' onMouseOut='botonOut(this);'>
                      <br>
                      <br>
                              
              </td>
          </tr>
       </table>  
   </div>
   
   
   
   
   
   
   <!--  Tabla de Archivos  -->
   <div  id='files'   style=" position:absolute;  visibility:hidden "  >  
   
     <table width="900" border="2" cellpadding='0' cellspacing='0'    align="center" bordercolor="#123456" border='1'>
       <tr class='fila'>
          <td align='center'> 
             <table cellpadding='0' cellspacing='0' width='98%'>
                <tr>
                  <td width='100%' align='center'>
                       <br>
                       <table width='98%' align='center' class='tablaInferior' cellpadding='0' cellspacing='0' >           
                             <tr class="barratitulo" >
                                <td colspan='2' >
                                   <table cellpadding='0' cellspacing='0' width='100%'>
                                     <tr >
                                      <td align="left" width='55%' class="subtitulo1" >&nbsp;Lista de Archivos </td>
                                      <td align="left" width='*'  ><img src="<%= BASEURL %>/images/titulo.gif" width="32" height="20"></td>
                                    </tr>
                                   </table>
                                </td>
                             </tr> 

                             <tr class="barratitulo">
                                <td colspan='2' >
                                   <table width='100%' border="1" bordercolor="#999999" bgcolor="#F7F5F4" align="center">
                                      <tr class="tblTitulo">
                                          <TH  width='30%' title='Nombre del archivo'     >ARCHIVO      </TH>
                                          <TH  width='10%' title='Tama�o en bytes'        >TAMA�O       </TH>               
                                          <TH  width='10%' title='Fecha de Subida '       >FECHA        </TH>               
                                          <TH  width='15%' title='Fecha de Modificaci�n'  >FECHA  MOD.  </TH>               
                                          <TH  width='10%' title='Tama�o de Modificaci�n' >TAM.   MOD.  </TH> 
                                          <TH  width='15%' title='Fecha de Eliminacion'   >FECHA  ELIMI.</TH>               
                                          <TH  width='*' TITLE='revisiones'               >REVISIONES   </TH>               
                                     </tr>

                                    <% for (int i=0; i<listFiles.size();i++){
                                            InventarioSoftware  inv = (InventarioSoftware) listFiles.get(i);
                                            List        listRevFile = inv.getRevisiones();%>                        
                                            <tr class='<%= (i%2==0?"filagris":"filaazul") %>'   onMouseOver='cambiarColorMouse(this)' style="cursor:hand" onclick="javascript:NuevaVentana('<%= CONTROLLER %>?estado=Revision&accion=Software&evento=searchfile&archivo=<%= inv.getId()%>','archivos',700,550,100,200);">
                                                  <td class="bordereporte" width='30%'                ><%= inv.getNombre() +"."+ inv.getExtension() %>   </td>
                                                  <td class="bordereporte" width='10%' align='right'  ><%= inv.getTamano()    %>   </td>               
                                                  <td class="bordereporte" width='10%' align='center' ><%= inv.getFecha()     %>   </td>               
                                                  <td class="bordereporte" width='15%' align='right'  ><%= inv.getFechaMod()  %>   </td>               
                                                  <td class="bordereporte" width='10%' align='right'  ><%= inv.getTamanoMod() %>   </td> 
                                                  <td class="bordereporte" width='15%' align='right'  ><%= inv.getFechaDelete()%>  </td>               
                                                  <td class="bordereporte" width='*'   align='center' ><%= listRevFile.size()  %>  </td>               
                                            </tr>                         
                                     <% }%>
                                 
                               </table> 
                            </td>
                          </tr>
                   </table>
                        
                    <br>
                      <img src='<%=BASEURL%>/images/botones/restablecer.gif'   style='cursor:hand'    title='Refrescar...'      name='i_refresh'     onclick="window.location.href='<%= CONTROLLER %>?estado=Revision&accion=Software&evento=refrescar';"  onMouseOver='botonOver(this);' onMouseOut='botonOut(this);'>    
                      <img src='<%=BASEURL%>/images/botones/cancelar.gif'      style='cursor:hand'    title='Cancelar...'       name='i_cancel'      onclick="files.style.visibility='hidden'"  onMouseOver='botonOver(this);'     onMouseOut='botonOut(this);'>    
                   <br>
                   <br>
               </td>
            </tr>
        </table>
       </td>
     </tr>
   </table>
   <br>
      
   </div>
      
   
   <!-- Botones -->   
   
   <img src='<%=BASEURL%>/images/botones/agregar.gif'    style=" cursor:hand'"  title='Adjuntar  Revision al programa'   name="imgmod"        onClick="rev.style.visibility  =''; rev.style.left=300;   rev.style.top=200;     files.style.visibility='hidden'; formRev.observacion.focus();"   onMouseOver='botonOver(this);' onMouseOut='botonOut(this);'>   
   <img src='<%=BASEURL%>/images/botones/detalles.gif'   style=" cursor:hand'"  title='Detalle Archivos....'             name='i_detail'      onclick="files.style.visibility=''; files.style.left=110; files.style.top=200;  rev.style.visibility='hidden'"                                   onMouseOver='botonOver(this);' onMouseOut='botonOut(this);'> 
   <img src='<%=BASEURL%>/images/botones/salir.gif'      style='cursor:hand'    title='Salir...'                         name='i_salir'       onclick='parent.close();'                                                                                                                       onMouseOver='botonOver(this);' onMouseOut='botonOut(this);'>

   
   
   
   <!-- val. script -->
   <%if(!swFile.equals("")){%>
       <script> files.style.visibility=''; rev.style.visibility='hidden';
                files.style.left=110; files.style.top=200;
       </script>   
   <%}%>
    
   
    
</div>

</body>
</html>
