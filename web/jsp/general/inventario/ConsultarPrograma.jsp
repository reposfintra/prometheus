<!--
- Autor : FERNEL VILLACOB DIAZ
- Date  : 10/01/2006  
- Copyrigth Notice : Fintravalores S.A. S.A
-->
<%--
-@(#)
--Descripcion : Vista que permite consultar un programa
--%>

<%@page    session   ="true"%> 
<%@page    errorPage ="/error/ErrorPage.jsp"%>
<%@page    import    ="com.tsp.operation.model.beans.*"%>
<%@include file      ="/WEB-INF/InitModel.jsp"%>
<%@page    import    ="com.tsp.util.*"%>
<%@page    import    ="java.util.*"%>


<html>
<head>
    <title>Consultar Programa</title>
     <link href="<%= BASEURL %>/css/estilostsp.css" rel="stylesheet" type="text/css"> 
    <script language="JavaScript" type="text/javascript" src="<%=BASEURL%>/js/script.js"></script>  
    <script language="JavaScript" type="text/javascript" src="<%=BASEURL%>/js/boton.js"></script>
    
    
</head>


<body >

<div id="capaSuperior" style="position:absolute; width:100%; height:100px; z-index:0; left: 0px; top: 0px;">
<jsp:include page="/WEB-INF/toptsp.jsp?encabezado=Consulta de Software"/>
</div>


<div id="capaCentral" style="position:absolute; width:100%; height:85%; z-index:0; left: 0px; top: 100px; overflow: scroll;"> 
<center>
    
<% Programa programa     =  (request.getAttribute("programa")==null)? new Programa() : (Programa )request.getAttribute("programa") ;
   List     listRevision =  programa.getListRevisiones();
   List     listFiles    =  programa.getListFile();    %>
       
       
      
  <table width="920" border="2"    align="center">
   <tr>
      <td> 
      
      
           <table width='100%' align='center' class='tablaInferior'>           
                 <tr class="barratitulo">
                    <td  >
                       <table cellpadding='0' cellspacing='0' width='100%'>
                         <tr>
                          <td align="left" width='55%' class="subtitulo1">&nbsp;Consulta Programa</td>
                          <td align="left" width='*'  ><img src="<%= BASEURL %>/images/titulo.gif" width="32" height="20"></td>
                        </tr>
                       </table>
                    </td>
                 </tr> 
                 
                 
                  <tr class='fila'>
                    <td width='100%'>
                       <table cellpadding='0' cellspacing='0' width='100%'>
                          <tr class='fila'>
                                <td width='12%'>CODIGO:                         </td>
                                <td width='15%'  ><%= programa.getPrograma() %> </td>              
                                <td width='12%'>DESCRIPCI�N:                    </td>
                                <td width='*'  ><%= programa.getDescripcion()%> </td>  
                          </tr>
                       </table>
                    </td>
                 </tr>
                 
                 <tr class='fila'>
                    <td width='100%'>
                        <table cellpadding='0' cellspacing='0' width='100%'>
                          <tr class='fila'>
                                <td width='12%' >EQUIPO:                        </td>
                                <td width='15%' ><%=  programa.getEquipo()%>    </td>                          
                                <td width='12%' >SITIO:                         </td>
                                <td width='15%' ><%= programa.getSitio()%>      </td> 
                                <td width='12%' >PROGRAMADOR:                   </td>
                                <td width='*'  ><%= programa.getProgramador()%> </td>
                           </tr>
                       </table>
                    </td>
                 </tr>
                 
                 
                 <tr class='fila'>
                    <td width='100%'>
                        <table cellpadding='0' cellspacing='0' width='100%'>
                          <tr class='fila'>
                                <td width='12%'>USUARIO                    </td>
                                <td width='15%'><%= programa.getUser()%>   </td>                            
                                <td width='12%'>FECHA:                     </td>
                                <td width='*'  ><%= programa.getFecha()%>  </td>  
                            </tr>
                       </table>
                    </td>
                 </tr>
                 
                
                 
                 <tr class="barratitulo">
                    <td >
                       <table cellpadding='0' cellspacing='0' width='100%'>
                         <tr>
                          <td align="left" width='55%' class="subtitulo1">&nbsp;Revisiones</td>
                          <td align="left" width='*'  ><img src="<%= BASEURL %>/images/titulo.gif" width="32" height="20"></td>
                        </tr>
                       </table>
                    </td>
                 </tr> 
                 
                 <tr>
                    <td  >
                       <table width='100%' border="1" bordercolor="#999999" bgcolor="#F7F5F4" align="center">
                          <tr class="tblTitulo" style="font size:11">
                              <TH  nowrap width='10%'>Usuario         </TH>
                              <TH  nowrap width='13%'>Tipo Revision   </TH>
                              <TH  nowrap width='55%'>Observaci�n     </TH>               
                              <TH  nowrap width='10%'>Aprobado        </TH>               
                              <TH  nowrap width='*'  >Fecha           </TH>               
                         </tr>
                      
                        <% for(int i=0; i<listRevision.size();i++){
                              Revision revision = (Revision)listRevision.get(i);%>
                              <tr class='<%= (i%2==0?"filagris":"filaazul") %>' style="font size:11" onMouseOver='cambiarColorMouse(this)'  >
                                  <td class='bordereporte' width='10%'               ><%= revision.getUser()          %></td>
                                  <td class='bordereporte' width='13%' align='center'><%= revision.getTipoRevision()  %></td>
                                  <td class='bordereporte' width='55%'               ><%= revision.getObservacion()   %></td>
                                  <td class='bordereporte' width='10%' align='center'><%= revision.getAprovado()      %></td>
                                  <td class='bordereporte' width='*'   align='right' ><%= revision.getFecha()         %></td>
                              </tr>
                          <%}%>
                       </table>
                    </td>                            
                 </tr>
                 
                 
                 <tr class="barratitulo">
                    <td >
                       <table cellpadding='0' cellspacing='0' width='100%'>
                         <tr>
                          <td align="left" width='55%' class="subtitulo1">&nbsp;Archivos del Programa</td>
                          <td align="left" width='*'  ><img src="<%= BASEURL %>/images/titulo.gif" width="32" height="20"></td>
                        </tr>
                       </table>
                    </td>
                 </tr> 
                 
                  <tr class="barratitulo">
                        <td  >
                           <table width='100%' border="1" bordercolor="#999999" bgcolor="#F7F5F4" align="center">
                              <tr class="tblTitulo" style="font size:11">
                                  <TH  width='30%'                                >ARCHIVO      </TH>
                                  <TH  width='10%'                                >TAMA�O       </TH>               
                                  <TH  width='10%' title='Fecha de Subida '       >FECHA        </TH>               
                                  <TH  width='15%' title='Fecha de Modificaci�n'  >FECHA  MOD.  </TH>               
                                  <TH  width='10%' title='Tama�o de Modificaci�n' >TAM.   MOD.  </TH> 
                                  <TH  width='15%' title='Fecha de Eliminacion'   >FECHA  ELIMI.</TH>               
                                  <TH  width='*'   title='revisiones'             >REVISIONES   </TH>               
                             </tr>
                             
                             <% for (int i=0; i<listFiles.size();i++){
                                    InventarioSoftware  inv = (InventarioSoftware) listFiles.get(i);
                                    List        listRevFile = inv.getRevisiones();%>                        
                                    <tr class='<%= (i%2==0?"filagris":"filaazul") %>' style="font size:11" onMouseOver='cambiarColorMouse(this)' onclick=" javascript: NuevaVentana('<%= CONTROLLER %>?estado=Consultar&accion=Software&evento=searchfile&file=<%= inv.getId() %>','revFile',400,200,100,100); " style="cursor:hand">
                                          <td class="bordereporte" width='30%'                ><%= inv.getNombre() +"."+ inv.getExtension() %>   </td>
                                          <td class="bordereporte" width='10%' align='right'  ><%= inv.getTamano()     %>   </td>               
                                          <td class="bordereporte" width='10%' align='center' ><%= inv.getFecha()      %>   </td>               
                                          <td class="bordereporte" width='15%' align='right'  ><%= inv.getFechaMod()   %>   </td>               
                                          <td class="bordereporte" width='10%' align='right'  ><%= inv.getTamanoMod()  %>   </td> 
                                          <td class="bordereporte" width='15%' align='right'  ><%= inv.getFechaDelete()%>   </td>               
                                          <td class="bordereporte" width='*'   align='center' ><%= listRevFile.size()  %>   </td>
                                    </tr>                                                           
                              <% }%>
                          </table>
                        </td>
                  </tr>
                 
                 
                 
            </table> 
         </td>
      </tr>
   </table>
   
   <!-- Botones -->
   <br>
   <img src='<%=BASEURL%>/images/botones/salir.gif'      style='cursor:hand'    title='Salir...'              name='i_salir'       onclick='parent.close();'    onMouseOver='botonOver(this);' onMouseOut='botonOut(this);'>
    
</div>
</body>
</html>
