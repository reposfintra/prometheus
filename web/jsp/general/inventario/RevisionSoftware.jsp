<!--
- Autor : FERNEL VILLACOB DIAZ
- Date  : 10/01/2006  
- Copyrigth Notice : Fintravalores S.A. S.A
-->
<%--
-@(#)
--Descripcion : Vista que permite revisar software
--%>

<%@page    session   ="true"%> 
<%@page    errorPage ="/error/ErrorPage.jsp"%>
<%@page    import    ="com.tsp.operation.model.beans.*"%>
<%@include file      ="/WEB-INF/InitModel.jsp"%>
<%@page    import    ="com.tsp.util.*"%>
<%@page    import    ="java.util.*"%>


<html>
<head>
    <title>Revision Software</title>
    <link href="<%= BASEURL %>/css/estilostsp.css" rel="stylesheet" type="text/css"> 
    <script language="JavaScript" type="text/javascript" src="<%=BASEURL%>/js/script.js"></script>  
    <script language="JavaScript" type="text/javascript" src="<%=BASEURL%>/js/boton.js"></script>
  
    <script>
    
      <%= model.RevisionSoft.getEquiposJS()%>
      <%= model.RevisionSoft.getSitiosJS() %>
       
      var separadorRow     = '<%=model.AdminSoft.SEPERADOR_ROW%>';
      var separadorColumn  = '<%=model.AdminSoft.SEPERADOR_COLUMN%>';
             
     function loadEquipos(){
        var vec = equipos.split(separadorRow);
        formPage.equipo.length = vec.length;
        for(var i=0;i<(vec.length);i++){            
             formPage.equipo.options[i].value = vec[i];
             formPage.equipo.options[i].text  = vec[i];
             if( vec[i]=='<%= model.RevisionSoft.getEquipo() %>')
                      formPage.equipo.options[i].selected='selected';
        }            
     }             
             
     function loadSitios(){
            var vec   = sitios .split(separadorRow);
            var selec = formPage.equipo.value;
            cont=0;
            for(var i=0;i<(vec.length);i++){ 
               var column = vec[i].split(separadorColumn);
               if( column[0]==selec )
                  cont++;
            }
            formPage.sitio.length = cont;
            cont=0;
            for(var i=0;i<(vec.length);i++){ 
               var column = vec[i].split(separadorColumn);
               if( column[0]==selec ){               
                   formPage.sitio.options[cont].value = column[1];
                   formPage.sitio.options[cont].text  = column[1];
                   if( column[1]=='<%=model.RevisionSoft.getSitios() %>')
                          formPage.sitio.options[cont].selected='selected';
                   cont++;
               }                 
            }
     }         


    function sendRevision(opcion){
        formPage.action += "&evento=" + opcion;
        formPage.submit();
    }
       
    </script>
    
</head>
<body>

<div id="capaSuperior" style="position:absolute; width:100%; height:100px; z-index:0; left: 0px; top: 0px;">
<jsp:include page="/WEB-INF/toptsp.jsp?encabezado=Revisi�n de Software"/>
</div>

<div id="capaCentral" style="position:absolute; width:100%; height:85%; z-index:0; left: 0px; top: 100px; overflow: scroll;"> 
<center>

   <script language="JavaScript" type="text/javascript" src="<%=BASEURL%>/js/script.js"></script>  
    

 <% String   msj          = (request.getParameter("msj")==null)?"":request.getParameter("msj");
    List     listProgram  = model.RevisionSoft.getPrograms();                                %>
    

  <form  action="<%= CONTROLLER %>?estado=Revision&accion=Software"   method='post' name='formPage'  id='formPage' >
  <table width="450" border="2"    align="center">
   <tr>
      <td>  
      
           <table width='100%' align='center' class='tablaInferior'>           
                 <tr class="barratitulo">
                    <td colspan='2' >
                       <table cellpadding='0' cellspacing='0' width='100%'>
                         <tr>
                          <td align="left" width='55%' class="subtitulo1">&nbsp;Revisi�n</td>
                          <td align="left" width='*'  ><img src="<%= BASEURL %>/images/titulo.gif" width="32" height="20"></td>
                        </tr>
                       </table>
                    </td>
                 </tr>                 
                 <tr class='fila'>
                    <td width='20%'>Equipo:</td>
                    <td width='*'  ><select name='equipo' style='width:50%' onchange='loadSitios()'></select></td>
                 </tr>                 
                 <tr class='fila'>
                    <td>Sitio:</td>
                    <td>           <select name='sitio' style='width:50%'></select></td>
                 </tr>                 
            </table>
            
         </td>
      </tr>
   </table>
   </form> 
   <img src='<%=BASEURL%>/images/botones/aceptar.gif'    style=" cursor:hand'"  title='Crear....'     name='i_crear'       onclick="sendRevision('load')"   onMouseOver='botonOver(this);' onMouseOut='botonOut(this);'>
   <img src='<%=BASEURL%>/images/botones/cancelar.gif'   style=" cursor:hand'"  title='Limpiar....'   name='i_cancel'      onclick="sendRevision('reset')"  onMouseOver='botonOver(this);' onMouseOut='botonOut(this);'>
   <img src='<%=BASEURL%>/images/botones/salir.gif'      style='cursor:hand'    title='Salir...'      name='i_salir'       onclick='parent.close();'        onMouseOver='botonOver(this);' onMouseOut='botonOut(this);'>

       
 
 <!--  Load combos -->
 <script>
     loadEquipos();
     loadSitios();
 </script>
 
 
 <!--  Lista de programas -->
 <% if( listProgram.size()>0){%>
 
      <table width="700" border="2" align="center">
      <tr>
        <td ALIGN='center'> 
        
               <table class='tablaInferior' width='100%' cellpadding='0' cellspacing='0' >
                    <tr>
                        <td >
                             <table border='0' width='100%' cellspacing='0'>
                                  <tr>
                                     <td class='subtitulo1'  width="40%">LISTADO PROGRAMAS </td>
                                     <td class='barratitulo' width="60%"><img src="<%= BASEURL %>/images/titulo.gif" width="32" height="20"></td>
                                  </tr>
                             </table>
                        </td> 
                    </tr>
                    <tr >
                       <td >
                             <table width='100%' border="1" bordercolor="#999999" bgcolor="#F7F5F4" align="center">   
                               <tr class="tblTitulo">
                                  <TH  nowrap width='15%'>CODIGO        </TH>
                                  <TH  nowrap width='50%'>DESCRIPCION   </TH>
                                  <TH  nowrap width='*'  >PROGRAMADOR   </TH>  
                               </tr> 
                    
                                <% for(int i=0;i<listProgram.size();i++){
                                      Programa  programa  = (Programa)listProgram.get(i);%>
                                        <tr class='<%= (i%2==0?"filagris":"filaazul") %>' onMouseOver='cambiarColorMouse(this)' style="cursor:hand"  onclick="javascript:NuevaVentana('<%= CONTROLLER %>?estado=Revision&accion=Software&evento=view&programa=<%= programa.getPrograma() %>','Programa',700,500,100,200)">
                                             <TD  class="bordereporte" width='15%'> <%=  programa.getPrograma()     %> </TD>
                                             <TD  class="bordereporte" width='50%'> <%=  programa.getDescripcion()  %> </TD>
                                             <TD  class="bordereporte" width='*'  > <%=  programa.getProgramador()  %> </TD>
                                        </tr>
                                <% } %>
                    
               </table>
            </td>
         </tr>
       </table>
        
 <%}%>
 
 
 
 
 
 
 <!-- Mensaje -->
 <% if(!msj.equals("")){%>
      <table border="2" align="center" width='400'>
          <tr>
              <td>
                <table width="100%"  border="1" align="center"  bordercolor="#F7F5F4" bgcolor="#FFFFFF">
                        <tr>
                        <td width="229" align="center" class="mensajes"><%=msj%></td>
                        <td width="29" background="<%=BASEURL%>/images/cuadronaranja.JPG">&nbsp;</td>
                        <td width="58">&nbsp;</td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
  <%}%>
   
   

</div>

</body>
</html>
