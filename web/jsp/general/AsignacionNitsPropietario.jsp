<!--
- Autor : Ing. Ivan Dario Gomez Vanegas
- Date  : 17 Febrero 2006
- Copyrigth Notice : Fintravalores S.A. S.A
-->
<%--
-@(#)
--Descripcion : Pagina JSP, para la asignacion de nits a propietarios
--%>

<%-- Declaracion de librerias--%>
<%@page contentType="text/html"%>
<%@page session="true"%>

<%@page import="java.util.*"%>
<%@page import="com.tsp.operation.model.*"%>
<%@page import="com.tsp.util.*"%>
<%@page import="com.tsp.operation.model.beans.*, java.util.*"%>
<%@include file="/WEB-INF/InitModel.jsp"%>
<%@ taglib uri="http://jakarta.apache.org/taglibs/input-0.90" prefix="input" %>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<title>Consulta Reporte Planilla</title>
<link href="<%= BASEURL %>/css/estilostsp.css" rel="stylesheet" type="text/css">
<script type='text/javascript' src="<%= BASEURL %>/js/boton.js"></script>

<script>
function enviar(frm){
    var parameter  = frm.action + '&Opcion=BUSCAR'; 
    for(i=0;i<frm.length;i++){ 
        parameter += '&'+ frm.elements[i].name + '=' +  frm.elements[i].value;
    }   
    OtraVentana(parameter,700,350,80,180);
}

function OtraVentana(url,largo,ancho,x,y){
    var option ="top="+ x +",left="+ y +",toolbar=no,location=no,status=yes, menubar=no,scrollbars=no, resizable=yes,width=" + largo + ",height=" + ancho;
    ventana=window.open(url,"ventana",option);
    return ventana;
}
</script>
</head>
<% List ListPropietario =  model.UbicaVehicuPorpSvc.getListaPropietario();
   String Asignar = request.getParameter("Asignar");
    String Msg = request.getParameter("Msg");%>
<%-- Inicio Body --%>
<body onLoad="redimensionar();" onResize="redimensionar();">
	<div id="capaSuperior" style="position:absolute; width:100%; height:100px; z-index:0; left: 0px; top: 0px;">
	<jsp:include page="/WEB-INF/toptsp.jsp?encabezado=Asignacion de Nits a Propietarios"/>
	</div>
<%if(Msg == null){
  if(Asignar == null){%>	
	<div id="capaCentral" style="position:absolute; width:100%; height:85%; z-index:0; left: 0px; top: 100px; overflow: scroll;">
	<form name="forma" method="post" action="<%=CONTROLLER%>?estado=AsignacionNits&accion=Propietario">
		<table width="500" border="2" align="center">
			<tr>
				<td>
					<table width="100%" align="center"  class="tablaInferior">
						<tr class="fila">
							<td width="50%" align="left" class="subtitulo1">&nbsp;Seleccion del Propietario  </td>
							<td width="50%"  align="left" class="barratitulo"><img src="<%=BASEURL%>/images/titulo.gif" width="32" height="20"></td>
						</tr>
					</table>
					 <table width="100%" border="0" align="center" class="tablaInferior">
                                              <tr class='fila'>
                                                <td><strong>Propietario</strong></td>
                                                <td>
                                                   <select name="Usuario" class="textbox">
                                                   <% Iterator It = ListPropietario.iterator();
                                                      while(It.hasNext()){
                                                      Usuario  usuario   = (Usuario)It.next();    
                                                      %>
                                                       <option  value="<%=usuario.getLogin()%>"><%=usuario.getNombre()%>     </option>
                                                     <%}%>
                                                   </select><img src="<%= BASEURL%>/images/botones/iconos/obligatorio.gif" width="10" height="10"></td>
                                              </tr>
                                              
                                            </table>
				</td>
			</tr>
		</table>
		<p align="center">
			<img src="<%=BASEURL%>/images/botones/aceptar.gif"  name="aceptar" style="cursor:hand" title="buscar nit" onMouseOver="botonOver(this);" onClick="javascript:enviar(forma);" onMouseOut="botonOut(this);" >&nbsp;
			<img src="<%=BASEURL%>/images/botones/salir.gif"  name="regresar" style="cursor:hand" title="Salir al Menu Principal" onMouseOver="botonOver(this);" onClick="parent.close();" onMouseOut="botonOut(this);" >
		</p>
	</form>
	</div>
<%}else{%>
	<div id="capaCentral" style="position:absolute; width:100%; height:85%; z-index:0; left: 0px; top: 100px; overflow: scroll;">
	<form name="form" method="post" action="<%=CONTROLLER%>?estado=AsignacionNits&accion=Propietario&Opcion=UPDATE">
		<table width="500" border="2" align="center">
			<tr>
				<td>
					<table width="100%" align="center"  class="tablaInferior">
						<tr class="fila">
							<td width="50%" align="left" class="subtitulo1">&nbsp;Agregar Nits  </td>
							<td width="50%"  align="left" class="barratitulo"><img src="<%=BASEURL%>/images/titulo.gif" width="32" height="20"></td>
						</tr>
					</table>
					 <table width="100%" border="0" align="center" class="tablaInferior">
                                        <% Iterator It = ListPropietario.iterator();
                                           if(It.hasNext()){
                                              Usuario  usuario   = (Usuario)It.next();    
                                           %>
                                              <tr class='fila'>
                                                <td><strong>Propietario:</strong></td>
                                                <td><%=usuario.getNombre()%><input type="hidden" name="Usuario" value='<%=usuario.getLogin()%>'></td>
                                              </tr> 
                                              <tr class='fila'>
                                                <td ><strong>Nits</strong></td>
                                                <td><input type="text" name="Nits" value="<%=usuario.getNitPropietario()%>" style='width:200;'></td>
                                              
                                              </tr>
                                          <%}%>
                                                  
                                              
                                              
                                            </table>
				</td>
			</tr>
		</table>
		<p align="center">
			<img src="<%=BASEURL%>/images/botones/aceptar.gif"  name="aceptar" style="cursor:hand" title="buscar nit" onMouseOver="botonOver(this);" onClick="form.submit();" onMouseOut="botonOut(this);" >&nbsp;
			<img src="<%=BASEURL%>/images/botones/salir.gif"  name="regresar" style="cursor:hand" title="Salir al Menu Principal" onMouseOver="botonOver(this);" onClick="parent.close();" onMouseOut="botonOut(this);" >
		</p>
	</form>
	</div>
<%}
}else{%>
            <div id="capaCentral" style="position:absolute; width:100%; height:85%; z-index:0; left: 0px; top: 100px; overflow: scroll;">
              <table border="2" align="center">
                  <tr>
                    <td><table width="100%"  border="1" align="center"  bordercolor="#F7F5F4" bgcolor="#FFFFFF">
                       <tr>
                        <td width="229" align="center" class="mensajes"><%=Msg%> 
                       </td>
                       <td width="29" background="<%=BASEURL%>/images/cuadronaranja.JPG">&nbsp;</td>
                        <td width="58">&nbsp;</td>
                      </tr>
                    </table>
                    </td>
                  </tr>
              </table>
              <p align="center">
			<img src="<%=BASEURL%>/images/botones/salir.gif"  name="regresar" style="cursor:hand" title="Salir al Menu Principal" onMouseOver="botonOver(this);" onClick="parent.close();" onMouseOut="botonOut(this);" >
		</p>
   </div>
<%}%>
</body>
</html>

