<!--  
	 - Author(s)       :      LREALES
	 - Description     :      AYUDA FUNCIONAL - Reporte de Demoras
	 - Date            :      30/11/2006 
	 - Copyright Notice:      TRANSPORTES SANCHEZ POLO S.A.
-->
<%@ include file="/WEB-INF/InitModel.jsp"%>
<HTML>
<HEAD>
<TITLE>Funcionalidad del Reporte de Carbon</TITLE>
<META http-equiv=Content-Type content="text/html; charset=windows-1252">
<link href="/../../../css/estilostsp.css" rel="stylesheet" type="text/css"> 
<link href="<%=BASEURL%>/css/estilostsp.css" rel="stylesheet" type="text/css">
<script type='text/javascript' src="<%= BASEURL %>/js/boton.js"></script> 
</HEAD>
<BODY> 
<% String BASEIMG = BASEURL +"/images/ayuda/reporte/carbon/"; %>
  <table width="95%"  border="2" align="center">
    <tr>
      <td height="117" >
        <table width="100%" border="0" align="center">
          <tr  class="subtitulo">
            <td height="20"><div align="center">MANUAL DE REPORTE WEB</div></td>
          </tr>
          <tr class="subtitulo1">
            <td>Descripci&oacute;n del funcionamiento del programa Reporte de Carb&oacute;n</td>
          </tr>
          <tr>
            <td  class="ayudaHtmlTexto"><div align="center"></div></td>
          </tr>
<tr>
            <td  class="ayudaHtmlTexto"><p class="ayudaHtmlTexto">&nbsp;</p>
            <p class="ayudaHtmlTexto">En la siguiente pantalla  se escoge el rango de las fechas en que desea generar el reporte.</p></td>
          </tr>
<tr>
            <td  class="ayudaHtmlTexto"><div align="center"><IMG height=240 src="<%=BASEIMG%>image_001.JPG" width=1010 border=0 v:shapes="_x0000_i1143"></div></td>
          </tr>
<tr>
  <td  class="ayudaHtmlTexto"><p>&nbsp;</p>
    <p>El sistema verifica que usted ingrese la fecha inicial.</p></td>
</tr>
<tr>
  <td  class="ayudaHtmlTexto"><div align="center"><IMG height=124 src="<%=BASEIMG%>image_error001.JPG" width=368 border=0 v:shapes="_x0000_i1052"></div></td>
</tr>
<tr>
  <td  class="ayudaHtmlTexto"><p>&nbsp;</p>
    <p>En la pantalla le saldra el siguiente mensaje si usted no ha definido la fecha final</p></td>
</tr>
<tr>
  <td  class="ayudaHtmlTexto"><p align="center"><IMG height=125 src="<%=BASEIMG%>image_error002.JPG" width=359 border=0 v:shapes="_x0000_i1052"></p>    </td>
</tr>
<tr>
  <td  class="ayudaHtmlTexto"><p>&nbsp;</p>
    <p>El sistema verifica que la fecha final no sea menor que la fecha inicial</p></td>
</tr>
<tr>
  <td  class="ayudaHtmlTexto"><div align="center"><IMG height=125 src="<%=BASEIMG%>image_error003.JPG" width=311 border=0 v:shapes="_x0000_i1052"></div></td>
</tr>
<tr>
  <td  class="ayudaHtmlTexto"><p>&nbsp;</p>
    <p>Si en la pantalla le sale el siguiente mensaje de error, es por que usted no ha seleccionado un parametro de busqueda. </p></td>
</tr>
<tr>
  <td  class="ayudaHtmlTexto"><div align="center"><IMG height=124 src="<%=BASEIMG%>image_error004.JPG" width=355 border=0 v:shapes="_x0000_i1052"></div></td>
</tr>
<tr>
  <td  class="ayudaHtmlTexto"><p>&nbsp;</p>
    <p>Si usted selecciona la opcion de busqueda por el nit del proveedor y le sale el siguiente mensaje, es por que usted no ha digitado el nit ha buscar. </p></td>
</tr>
<tr>
  <td  class="ayudaHtmlTexto"><div align="center"><IMG height=124S src="<%=BASEIMG%>image_error005.JPG" width=399 border=0 v:shapes="_x0000_i1052"></div></td>
</tr>
<tr>
  <td  class="ayudaHtmlTexto"><p>&nbsp;</p>
    <p>Si usted no sabe cual es el nit del proveedor, al presionar click en la lupa o bot&oacute;n de busqueda especifica, podr&aacute; buscar el proveedor con el cual desea realizar el reporte. </p></td>
</tr>
<tr>
  <td  class="ayudaHtmlTexto"><div align="center"><IMG height=132 src="<%=BASEIMG%>image_002.JPG" width=713 border=0 v:shapes="_x0000_i1052"></div></td>
</tr>
<tr>
  <td  class="ayudaHtmlTexto"><p>&nbsp;</p>
    <p>Si le sale el siguiente mensaje, es por que el proveedor con el nombre que usted especifico no existe en nuestra base de datos. </p></td>
</tr>
<tr>
  <td  class="ayudaHtmlTexto"><div align="center"><IMG height=222 src="<%=BASEIMG%>image_error007.JPG" width=713 border=0 v:shapes="_x0000_i1052"></div></td>
</tr>
<tr>
  <td  class="ayudaHtmlTexto"><p>&nbsp;</p>
    <p>Luego de escribir el nombre del proveedor que desea buscar, puede presionar enter &oacute; click sobre el boton de busqueda, e inmediatamente le aperecera una lista con los nombres de proveedores que usted esta buscando, alli escoje el proveedor que desea presionando click sobre &eacute;l. </p></td>
</tr>
<tr>
  <td  class="ayudaHtmlTexto"><div align="center"><IMG height=325 src="<%=BASEIMG%>image_003.JPG" width=830 border=0 v:shapes="_x0000_i1052"></div></td>
</tr>
<tr>
  <td  class="ayudaHtmlTexto"><p>&nbsp;</p>
    <p>Si el sistema le arroja el siguiente mensaje, es por que no existe informacion entre ese rango de fechas para su reporte en nuestra base de datos.</p></td>
</tr>
<tr>
  <td  class="ayudaHtmlTexto"><div align="center"><IMG height=93 src="<%=BASEIMG%>image_error006.JPG" width=737 border=0 v:shapes="_x0000_i1052"></div></td>
</tr>
<tr>
            <td  class="ayudaHtmlTexto"><p class="ayudaHtmlTexto">&nbsp;</p>
            <p class="ayudaHtmlTexto">Despues de haber realizado la busqueda satisfactoriamente, se mostrara un reporte previo para que pueda confirmar los datos y proceder a generar el excel.</p></td>
          </tr>
<tr>
            <td  class="ayudaHtmlTexto"><div align="center"><IMG height=342 src="<%=BASEIMG%>image_004.JPG" width=928 border=0 v:shapes="_x0000_i1052"></div></td>
          </tr>
<tr>
  <td  class="ayudaHtmlTexto"><p>&nbsp;</p>
    <p>Al escoger 'EXPORTAR' el sistema le mostrara el siguiente mensaje, informandole donde puede observar el archivo excel generado.</p></td>
</tr>
<tr>
  <td  class="ayudaHtmlTexto"><div align="center"><IMG height=224 src="<%=BASEIMG%>image_005.JPG" width=425 border=0 v:shapes="_x0000_i1052"></div></td>
</tr>
<tr>
            <td  class="ayudaHtmlTexto"><p class="ayudaHtmlTexto">&nbsp;</p>
            <p class="ayudaHtmlTexto">El sistema confirma que el reporte fue exportado a excel y que fue enviado un e-mail a los proveedores correspondientes con este archivo adjunto.</p></td>
          </tr>
<tr>
  <td  class="ayudaHtmlTexto"><div align="center"><IMG height=155 src="<%=BASEIMG%>image_006.JPG" width=753 border=0 v:shapes="_x0000_i1054"></div></td>
</tr>
<tr>
      </table></td>
    </tr>
  </table>
  <p></p>
<center>
<img src = "<%=BASEURL%>/images/botones/salir.gif" style = "cursor:hand" name = "imgsalir" onClick = "parent.close();" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);">
</center>
</BODY>
</HTML>
