<!--
- Autor : LREALES
- Date  : 31 de octubre de 2006
- Copyrigth Notice : Fintravalores S.A. S.A
-->
<%--
-@(#)
--Descripcion : Pagina JSP, que se encarga de realizar la busqueda para el reporte de carbon
--%>
<%--
The taglib directive below imports the JSTL library. If you uncomment it,
you must also add the JSTL library to the project. The Add Library... action
on Libraries node in Projects view can be used to add the JSTL 1.1 library.
--%>
<%-- Declaracion de librerias--%>
<%@page contentType="text/html"%>
<%@page errorPage="/error/ErrorPage.jsp"%>
<%@page import="com.tsp.operation.model.beans.*, java.util.*"%>
<%@taglib uri="/WEB-INF/tlds/tagsAgencia.tld" prefix="ag"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://jakarta.apache.org/taglibs/input-0.90" prefix="input" %>
<%@ include file="/WEB-INF/InitModel.jsp"%>
<%boolean bandera = false; %>
<%
  String path = application.getRealPath(model.menuService.getRealUrl(request.getRequestURI()));
  String datos[] = model.menuService.getContenidoMenu(BASEURL,request.getRequestURI(),path);
%>
<html>
    <head>
        <title>Consultar Reporte de Carbon</title>        
		<script src='<%=BASEURL%>/js/date-picker.js'></script>
		<script language="JavaScript" type="text/javascript" src="<%=BASEURL%>/js/boton.js"></script>
		<script type='text/javascript' src="<%=BASEURL%>/js/validar.js"></script> 
        <link href="../css/estilostsp.css" rel="stylesheet" type="text/css">
		<script>	
			
			var valorRadio = "";
		
			function setRadio( value ){
			
				valorRadio = value;	
				
			}
			
			function validar( form ){
			
				if( valorRadio == "" ){
					alert( 'Defina un parametro de busqueda para poder continuar...' );
					return false;
				}
				
				if( valorRadio == "2" && form.nit_prov.value == "" ){
					alert( 'Debe escribir o escoger el nit del proveedor para poder continuar...' );
					return false;
				}
				
				if ( form.fecini.value == '' ){
					alert( 'Defina la fecha de inicio de busqueda para poder continuar...' );
					return false;
				}
				
				if ( form.fecfin.value == '' ){
					alert( 'Defina la fecha de fin de busqueda para poder continuar...' );
					return false;
				}
				
   				var fecha1 = form.fecini.value.replace( /-|:| /gi, '' );
				var fecha2 = form.fecfin.value.replace( /-|:| /gi, '' );
				 
				var fech1 = parseFloat( fecha1 );
				var fech2 = parseFloat( fecha2 );
				 
				if( fecha2 < fecha1 ) { 				   
					 alert( 'La fecha final debe ser mayor que la fecha inicial!' );
					 return false;					 
				}
				
				return true;
				
			}	
			
			function abrirVentanaBusq( an, al, url, pag ) {
			
        		parent.open( url, 'Conductor', 'width=' + an + ', height=' + al + ',scrollbars=no,resizable=no,top=10,left=65,status=yes' );
				
    		}
							
		</script>
    </head>
   <body onResize="redimensionar()" onLoad="redimensionar()">
<div id="capaSuperior" style="position:absolute; width:100%; height:100px; z-index:0; left: 1px; top: 0px;">
 <jsp:include page="/toptsp.jsp?encabezado=Reporte de Carbon"/>
</div>
<div id="capaCentral" style="position:absolute; width:100%; height:87%; z-index:0; left: 0px; top: 100px; overflow: scroll;">

        <form name='formulario' method='POST' id="formulario" action="<%=CONTROLLER%>?estado=Reporte&accion=Carbon&hacer=1" onSubmit="return validar(this);">
            
		<!-- <form name='formulario' method='POST' id="formulario" > -->
			<table width="700"  border="2" align="center">
              <tr>
                <td><table width="100%"  border="0" class="tablaInferior">
                  
				  <tr>
                   <td class="subtitulo1" colspan="2">Reporte de Carb&oacute;n</td>
          			<td class="barratitulo" colspan="2"><img src="<%=BASEURL%>/images/titulo.gif" width="32" height="20" align="left"><%=datos[0]%></td>
                  </tr>
                  
                  <tr class="fila">
                     <td width="17%" align="left">&nbsp;Fecha Inicial</td>
          		     <td width="33%" align="left">
				  		<input name="fecini" type="text" class="textbox" id="fecini" size="17" readonly>
              			<span class="Letras"> <img src="<%=BASEURL%>/images/botones/iconos/obligatorio.gif" name="obligatorio"> 
						<img src="<%=BASEURL%>/images/cal.gif" width="16" height="16" align="absmiddle" style="cursor:hand " onclick="if(self.gfPop)gfPop.fPopCalendar(document.formulario.fecini);return false;" HIDEFOCUS></span></td>
                     <td width="17%" align="left">&nbsp;Fecha Final</td>
          		     <td width="33%" align="left">
              			<input name="fecfin" type="text" class="textbox" id="fecfin" size="17" readonly>
              			<span class="Letras"><a href="javascript:void(0)" onclick="jscript: show_calendar('fecfin');" HIDEFOCUS> </a>
						<img src="<%=BASEURL%>/images/botones/iconos/obligatorio.gif" name="obligatorio"> 
						<img src="<%=BASEURL%>/images/cal.gif" width="16" height="16" align="absmiddle" style="cursor:hand " onclick="if(self.gfPop)gfPop.fPopCalendar(document.formulario.fecfin);return false;" HIDEFOCUS></span></td>
                  </tr>
				           
                  <tr class="fila">
                    <td colspan="2" align="left">
                      <input name="opcion" id="opcion" type="radio" value="1" onClick="setRadio(this.value);">
                      Todos los Proveedores
					</td>
                    <td colspan="2" align="left">
                      <input name="opcion" id="opcion" type="radio" value="2" onClick="setRadio(this.value);">
                      Nit del Proveedor
                      <input name="nit_prov" id="nit_prov" type="text" class="textbox" size="15" maxlength="15">
                      <img src='<%=BASEURL%>/images/botones/iconos/lupa.gif' width="15" height="15" onClick="javascript:abrirVentanaBusq(850,550,'<%=BASEURL%>/jsp/general/exportar/proveedores_fintra/BuscarNitProveedor.jsp')" title="Buscar" style="cursor:hand" >
					</td>
                  </tr>
				        
                </table></td>
              </tr>
            </table>
			<br>
            <table align="center">
              <tr>
                <td colspan="2" nowrap align="center">
				
				<input name="Guardar" src="<%=BASEURL%>/images/botones/aceptar.gif"  style = "cursor:hand" align="middle" type="image" id="Guardar" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);">      
				 
				<!--           
				<img name="Guardar" src="< %=BASEURL%>/images/botones/aceptar.gif"  style = "cursor:hand" align="middle" type="image" id="Guardar" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" onClick="Buscar (); this.disabled=true;">      
				-->
		&nbsp;
                  <img src="<%=BASEURL%>/images/botones/salir.gif"  name="imgsalir" align="absmiddle" style="cursor:hand" onClick="parent.close();" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);"></td>
              </tr>
            </table>
        </form > 
	</div>  
	<iframe width=188 height=166 name="gToday:normal:agenda.js:gfPop:plugins_24.js" id="gToday:normal:agenda.js:gfPop:plugins_24.js" src="<%=BASEURL%>/js/Calendario/ipopeng.htm" scrolling="no" frameborder="0" style="visibility:visible; z-index:999; position:absolute; left:-500px; top:0px;"> </iframe>    
    <%=datos[1]%>
	</body>
</html>