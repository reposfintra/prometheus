<!--
- Autor : LREALES
- Date  : 9 de noviembre de 2006
- Copyrigth Notice : Fintravalores S.A. S.A
-->
<%--
-@(#)
--Descripcion : Pagina JSP, que se encarga de mostrar el reporte de carbon
--%>
<%--
The taglib directive below imports the JSTL library. If you uncomment it,
you must also add the JSTL library to the project. The Add Library... action
on Libraries node in Projects view can be used to add the JSTL 1.1 library.
--%>
<%-- Declaracion de librerias--%>
<%@page session="true"%>
<%@page errorPage="../error/ErrorPage.jsp"%>
<%@page import="com.tsp.operation.model.beans.*, java.util.*, java.text.*"%>
<%@ include file="/WEB-INF/InitModel.jsp"%>
<%@ taglib uri="http://jakarta.apache.org/taglibs/input-0.90" prefix="input" %>
<%@page import="com.tsp.util.*"%>
<% Util u = new Util(); %>
<html>
    <head>        
        <title>Reporte de Carbon</title>
		<script src='<%=BASEURL%>/js/date-picker.js'></script>
		<script language="JavaScript" type="text/javascript" src="<%=BASEURL%>/js/boton.js"></script>
		<script type='text/javascript' src="<%=BASEURL%>/js/validar.js"></script> 
		<link href="../css/estilostsp.css" rel="stylesheet" type="text/css">
		<link href="<%=BASEURL%>/css/estilostsp.css" rel="stylesheet" type="text/css">
		 <script>
		 	function Exportar (){
				
			    alert( 
				
					'Puede observar el seguimiento del proceso en la siguiente ruta del menu:' + '\n' + '\n' +
					'SLT -> PROCESOS -> Seguimiento de Procesos' + '\n' + '\n' + '\n' +
					'Y puede observar el archivo excel generado, en la siguiente parte:' + '\n' + '\n' +
					'En la pantalla de bienvenida de la Pagina WEB,' + '\n' +
					'vaya a la parte superior-derecha,' + '\n' +
					'y presione un click en el icono que le aparece.'
					
				);
				
			}
		</script>
    </head>
   <body onResize="redimensionar()" onLoad="redimensionar()">
	<div id="capaSuperior" style="position:absolute; width:100%; height:100px; z-index:0; left: 1px; top: 0px;">
	 <jsp:include page="/toptsp.jsp?encabezado=Reporte de Carbon"/>
	</div>
	<div id="capaCentral" style="position:absolute; width:100%; height:87%; z-index:0; left: 0px; top: 100px; overflow: scroll;">
	<%String msg = request.getParameter("msg");
		if ( msg!=null && !msg.equals("") ){%>							
		  <table border="2" align="center">
			<tr>
				<td>
					<table width="100%"  border="1" align="center"  bordercolor="#F7F5F4" bgcolor="#FFFFFF">
						<tr>
							<td width="229" align="center" class="mensajes"><%=msg%></td>
							<td width="29" background="<%=BASEURL%>/images/cuadronaranja.JPG">&nbsp;</td>
							<td width="58">&nbsp;</td>
						</tr>
					</table>
				</td>
			</tr>
		  </table>		  
		        <br>
		        <img src="<%=BASEURL%>/images/botones/regresar.gif" name="c_regresar" onclick="location.href='<%=BASEURL%>/jsp/general/exportar/carbon/ConsultarReporteCarbon.jsp'" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" style="cursor:hand">
 	  <input name="salir" src="<%=BASEURL%>/images/botones/salir.gif" type="image" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" style="cursor:hand" class="boton" onclick="window.close();">
	  <%}
		else { 
		    Vector datos = model.reporteCarbonService.getVectorReporte ();
			
		    if( datos != null && datos.size() > 0 ){
			    BeanGeneral info_fechas = (BeanGeneral) datos.elementAt(0);
		%>		
		    <form name="form1" method="post" action="<%=CONTROLLER%>?estado=Reporte&accion=Carbon&hacer=2">
      <input name="fecini" type="hidden" id="fecini" value="<%=info_fechas.getValor_11()%>">
	  <input name="fecfin" type="hidden" id="fecfin" value="<%=info_fechas.getValor_12()%>">
	  
	  <input name="opcion" type="hidden" id="opcion" value="<%=info_fechas.getValor_16()%>">
	  <input name="nit_prov" type="hidden" id="nit_prov" value="<%=info_fechas.getValor_17()%>">
	  
      <input name="Guardar2" src="<%=BASEURL%>/images/botones/exportarExcel.gif" type="image" class="boton" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" style="cursor:hand" onclick="Exportar();">
      <img src="<%=BASEURL%>/images/botones/regresar.gif" name="c_regresar" onclick="location.href='<%=BASEURL%>/jsp/general/exportar/carbon/ConsultarReporteCarbon.jsp'" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" style="cursor:hand">
 	  <input name="salir" src="<%=BASEURL%>/images/botones/salir.gif" type="image" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" style="cursor:hand" class="boton" onclick="window.close();">
    </form>
		<table width="100%"  border="2">  
          <tr>
        	<td class="subtitulo1"><strong>Reporte de Carb&oacute;n Desde: <%=info_fechas.getValor_11()%> - Hasta: <%=info_fechas.getValor_12()%></strong></td>
          </tr>
          <tr>
            <td>
              <table width="100%"  border="1" bordercolor="#999999" bgcolor="#F7F5F4" align="center">
                <tr class="tblTitulo" align="center">
					<td nowrap align="center">NIT PROVEEDOR</td>
					<td nowrap align="center">NOMBRE PROVEEDOR</td>
					<td nowrap align="center">FECHA DESPACHO</td>
                    <td nowrap align="center">FECHA CUMPLIDO</td>
                    <td nowrap align="center">PROYECTO</td>
                    <td nowrap align="center">N� REMESA</td>
                    <td nowrap align="center">N� ESTANDAR</td>
                    <td nowrap align="center">N� PLANILLA</td>
                    <td nowrap align="center">AGENCIA PLANILLA</td>
                    <td nowrap align="center">ORIGEN PLANILLA</td>
                    <td nowrap align="center">DESTINO PLANILLA</td>
                    <td nowrap align="center">PESO REAL</td>
                    <td nowrap align="center">VALOR ANTICIPO</td>         
                </tr>						
		<%
			for( int i = 0; i<datos.size(); i++ ){
				BeanGeneral info = (BeanGeneral) datos.elementAt(i);
	  %>  	  
                  <tr class="<%=(i % 2 == 0 )?"filagris":"filaazul"%>">
				  <td nowrap align="left" class="bordereporte"><%=(info.getValor_14()!=null)?info.getValor_14():""%>&nbsp;</td>
				  <td nowrap align="left" class="bordereporte"><%=(info.getValor_15()!=null)?info.getValor_15():""%>&nbsp;</td>
				  <td nowrap align="left" class="bordereporte"><%=(info.getValor_13()!=null)?info.getValor_13():""%>&nbsp;</td>
				  <td nowrap align="left" class="bordereporte"><%=(info.getValor_07()!=null)?info.getValor_07():""%>&nbsp;</td>
                  <td nowrap align="left" class="bordereporte"><%=(info.getValor_01()!=null)?info.getValor_01():""%>&nbsp;</td>
                  <td nowrap align="left" class="bordereporte"><%=(info.getValor_09()!=null)?info.getValor_09():""%>&nbsp;</td>
				  <td nowrap align="left" class="bordereporte"><%=(info.getValor_08()!=null)?info.getValor_08():""%>&nbsp;</td>
				  <td nowrap align="left" class="bordereporte"><%=(info.getValor_02()!=null)?info.getValor_02():""%>&nbsp;</td>
				  <td nowrap align="left" class="bordereporte"><%=(info.getValor_03()!=null)?info.getValor_03():""%>&nbsp;</td>
				  <td nowrap align="left" class="bordereporte"><%=(info.getValor_04()!=null)?info.getValor_04():""%>&nbsp;</td>
				  <td nowrap align="left" class="bordereporte"><%=(info.getValor_05()!=null)?info.getValor_05():""%>&nbsp;</td>
				  <td nowrap align="right" class="bordereporte"><%=(info.getValor_06()!=null)?info.getValor_06():""%>&nbsp;</td>
                  <td nowrap align="right" class="bordereporte">$&nbsp;<%=(info.getValor_10()!=null)?info.getValor_10():""%>&nbsp;</td>     
                </tr>
				         <%
	  	}%>
            </table>
      </table></td>
      </tr>
		    </table>			
	  <form name="form1" method="post" action="<%=CONTROLLER%>?estado=Reporte&accion=Carbon&hacer=2">
		  <input name="fecini" type="hidden" id="fecini" value="<%=info_fechas.getValor_11()%>">
		  <input name="fecfin" type="hidden" id="fecfin" value="<%=info_fechas.getValor_12()%>">
		  
	  <input name="opcion" type="hidden" id="opcion" value="<%=info_fechas.getValor_16()%>">
	  <input name="nit_prov" type="hidden" id="nit_prov" value="<%=info_fechas.getValor_17()%>">
	  
		  <input name="Guardar3" src="<%=BASEURL%>/images/botones/exportarExcel.gif" type="image" class="boton" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" style="cursor:hand" onclick="Exportar();">
		  <img src="<%=BASEURL%>/images/botones/regresar.gif" name="c_regresar" onclick="location.href='<%=BASEURL%>/jsp/general/exportar/carbon/ConsultarReporteCarbon.jsp'" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" style="cursor:hand">
		  <input name="salir2" src="<%=BASEURL%>/images/botones/salir.gif" type="image" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" style="cursor:hand" class="boton" onclick="window.close();">
      </form>	
	  <%}	  
	  }
	  %>	  
	</body>
  </div>
</html>