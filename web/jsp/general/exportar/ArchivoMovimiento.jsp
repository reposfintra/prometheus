<!--
     - Author(s)       :      FERNEL VILLACOB DIAZ
     - Date            :      14/13/2006  
     - Copyright Notice:      TRANSPORTES SANCHEZ POLO S.A.
  -->
 <%--   - @(#)  
     - Description: Muestra  los filtros  de seleccion para buscar los cheques de la corrida
 --%>

<%@ page session   ="true"%> 
<%@ page errorPage ="/error/ErrorPage.jsp"%>
<%@ page import    ="java.util.*" %>
<%@ page import    ="com.tsp.util.*" %>
<%@ page import    ="com.tsp.operation.model.beans.*" %>
<%@ include file   ="/WEB-INF/InitModel.jsp"%>



<html>
<head>
        <title>Archivo de Movimiento</title>
        <link href="<%= BASEURL %>/css/estilostsp.css"     rel="stylesheet" type="text/css"> 
        <script type='text/javascript' src="<%= BASEURL %>/js/boton.js"></script>
        <script type='text/javascript' src="<%= BASEURL %>/js/Validaciones.js"></script>
        
        <script>
            function sendForm(theForm){ 
               var sw = 0;
               for(i=0;i<theForm.length;i++) {
                  var elemento = theForm.elements[i];
                  if ( elemento.value==''){
                      alert('Deber� digitar el valor del campo '+ elemento.name );
                      elemento.focus();
                      sw = 1; 
                      break;
                  }
               }
               
               if( sw==0 ){
                   if ( !isNumerico(theForm.corrida.value)  ){
                       alert('El valor del campo corrida debe ser solo num�rico');
                       theForm.corrida.value = '';
                       theForm.corrida.focus();
                   }
                   else
                       theForm.submit();
               }
            }
        </script>
   
</head>
<body onload='formulario.banco.focus();'>

<div id="capaSuperior" style="position:absolute; width:100%; height:100px; z-index:0; left: 0px; top: 0px;">
<jsp:include page="/WEB-INF/toptsp.jsp?encabezado=Archivo de Movimiento de Transferencia"/>
</div>

<div id="capaCentral" style="position:absolute; width:100%; height:85%; z-index:0; left: 0px; top: 100px; overflow: scroll;"> 
<center>
   
   
<%  String path    = application.getRealPath(model.menuService.getRealUrl(request.getRequestURI()));
    String datos[] = model.menuService.getContenidoMenu(BASEURL,request.getRequestURI(),path);    %>



  <% Usuario  usuario   = (Usuario) session.getAttribute("Usuario");
     String   msj       = request.getParameter("msj"); %>
  
  <form action="<%=CONTROLLER %>?estado=Archivo&accion=Movimiento&evento=BUSCAR" method="post" name="formulario">  
             <table   border="2" align="center" width='400'>
                 <tr>
                    <td colspan='2'>             

                                <table width="100%"   align="center">
                                          <tr>
                                             <td colspan='2' >
                                                  <table width='100%'  class="barratitulo">
                                                        <tr class="fila">
                                                                <td align="left" width='70%' class="subtitulo1" nowrap title='Busqueda en mims'> TRANSFERENCIAS A BANCOS [MIMS]</td>
                                                                <td align="left" width='*' ><img src="<%=BASEURL%>/images/titulo.gif" width="32" height="20"  align="left"><%=datos[0]%></td>
                                                        </tr>
                                                  </table>
                                             </td>
                                          </tr>
                                          
                                          <tr class="fila">
                                                   <td width='30%' >&nbsp Distrito</td>
                                                   <td width='*'>
                                                       <select name='distrito' style="width:40%">
                                                          <option value='<%= usuario.getDstrct()%>'><%= usuario.getDstrct()%></option>
                                                       </select>                                            
                                                   </td>
                                          </tr>
                                          
                                          <tr class="fila">
                                                   <td >&nbsp Banco</td>
                                                   <td width='*'>
                                                       <input type='text' name='banco'     title='El banco de la corrida'  style="width:100%" maxlength='15'>                                             
                                                   </td>
                                           </tr>
                                           
                                            <tr class="fila">
                                                   <td >&nbsp Sucursal</td>
                                                   <td width='*'>
                                                       <input type='text' name='sucursal'  title='La sucursal del banco'  style="width:100%" maxlength='30'>                                             
                                                   </td>
                                           </tr>
                                           
                                           <tr class="fila">
                                                   <td >&nbsp Corrida</td>
                                                   <td width='*'>
                                                       <input type='text' name='corrida'   title='N�mero de la corrida'  style="width:40%" maxlength='6'>                                             
                                                   </td>
                                           </tr>
                                          
                                 </table>
                     </td>
                 </tr>
              </table>
     </form>

      <p>       
           <img src='<%=BASEURL%>/images/botones/aceptar.gif'    style=" cursor:hand"  title='Aceptar....'     name='i_crear'       onclick="sendForm(formulario);"   onMouseOver='botonOver(this);' onMouseOut='botonOut(this);'>        
           <img src='<%=BASEURL%>/images/botones/salir.gif'      style=" cursor:hand"  title='Salir...'        name='i_salir'       onclick='parent.close();'         onMouseOver='botonOver(this);' onMouseOut='botonOut(this);'>
      </p> 
        

      
     
     
     <!-- Mensaje -->
     <%  if ( msj!=null && !msj.equals("")){%>
             <table border="2" align="center">
                   <tr>
                        <td>
                               <table width="100%"  border="1" align="center"  bordercolor="#F7F5F4" bgcolor="#FFFFFF">
                                  <tr>
                                        <td width="500" align="center" class="mensajes"><%=msj%></td>
                                        <td width="29" background="<%=BASEURL%>/images/cuadronaranja.JPG">&nbsp;</td>
                                        <td width="58">&nbsp; </td>
                                  </tr>
                                </table>
                       </td>
                   </tr>
            </table>
     <%}%>
     
     
</div>
<%=datos[1]%> 


</body>
</html>
