<!--  
	 - Author(s)       :      LREALES
	 - Description     :      AYUDA FUNCIONAL - Actualizar Documentos de las Remesas
	 - Date            :      25/07/2006 
	 - Copyright Notice:      TRANSPORTES SANCHEZ POLO S.A.
-->
<%@ include file="/WEB-INF/InitModel.jsp"%>
<HTML>
<HEAD>
<TITLE>AYUDA FUNCIONAL - Actualizar Documentos de las Remesas</TITLE>
<META http-equiv=Content-Type content="text/html; charset=windows-1252">
<link href="../css/estilostsp.css" rel="stylesheet" type="text/css"> 
<link href="<%=BASEURL%>/css/estilostsp.css" rel="stylesheet" type="text/css">
<script type='text/javascript' src="<%= BASEURL %>/js/boton.js"></script> 
</HEAD>
<BODY> 
<% String BASEIMG = BASEURL +"/images/ayuda/reporte/ActualizarDocumentoRemesa/"; %>
  <table width="95%"  border="2" align="center">
    <tr>
      <td height="117" >
        <table width="100%" border="0" align="center">
          <tr  class="subtitulo">
            <td height="20"><div align="center">MANUAL DE ACTUALIZACION WEB</div></td>
          </tr>
          <tr class="subtitulo1">
            <td>Descripci&oacute;n del funcionamiento del programa para  Actualizar los Documentos de las Remesas.</td>
          </tr>
          <tr>
            <td  class="ayudaHtmlTexto"><div align="center">
              <p>&nbsp;</p>
              </div></td>
          </tr>
          <tr>
            <td  class="ayudaHtmlTexto"><p>En la siguiente pantalla se procede a presionar un click sobre el bot&oacute;n 'ACEPTAR' para comenzar el proceso de actualizaci&oacute;n de los documentos de las remesas.</p>            </td>
          </tr>
          <tr>
            <td  class="ayudaHtmlTexto"><div align="center"><IMG height=183 src="<%=BASEIMG%>image_001.JPG" width=789 border=0 v:shapes="_x0000_i1143"></div></td>
          </tr>
          <tr>
            <td  class="ayudaHtmlTexto"><p>&nbsp;</p>
            <p>Luego en la pantalla le saldr&aacute; el siguiente mensaje, confirmando que el proceso de actualizaci&oacute;n ha sido iniciado y esta ejecutandose. </p></td>
          </tr>
          <tr>
            <td  class="ayudaHtmlTexto"><div align="center"><IMG height=263 src="<%=BASEIMG%>image_002.JPG" width=789 border=0 v:shapes="_x0000_i1054"></div></td>
          </tr>
      </table></td>
    </tr>
  </table>
  <p></p>
<center>
<img src = "<%=BASEURL%>/images/botones/salir.gif" style = "cursor:hand" name = "imgsalir" onClick = "parent.close();" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);">
</center>
</BODY>
</HTML>
