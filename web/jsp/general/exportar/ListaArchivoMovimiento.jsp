<!--  
     - Author(s)       :      FERNEL VILLACOB DIAZ
     - Date            :      14/13/2006  
     - Copyright Notice:      TRANSPORTES SANCHEZ POLO S.A.
  -->
 <%--   - @(#)  
     - Description: Muestra  los cheques de la corridas establecida
 --%>

<%@ page session   ="true"%> 
<%@ page errorPage ="/error/ErrorPage.jsp"%>
<%@ page import    ="java.util.*" %>
<%@ page import    ="com.tsp.util.*" %>
<%@ page import    ="com.tsp.operation.model.beans.*" %>
<%@ include file   ="/WEB-INF/InitModel.jsp"%>



<html>
<head>

       <title>Archivo de Movimiento</title>
       <link href="<%= BASEURL %>/css/estilostsp.css"     rel="stylesheet" type="text/css"> 
       <script type='text/javascript' src="<%= BASEURL %>/js/boton.js"></script>

</head>
<body>


<div id="capaSuperior" style="position:absolute; width:100%; height:100px; z-index:0; left: 0px; top: 0px;">
<jsp:include page="/WEB-INF/toptsp.jsp?encabezado=Archivo de Movimiento de Transferencia"/>
</div>

<div id="capaCentral" style="position:absolute; width:100%; height:85%; z-index:0; left: 0px; top: 100px; overflow: scroll;"> 
<center>
   

<%  String path    = application.getRealPath(model.menuService.getRealUrl(request.getRequestURI()));
    String datos[] = model.menuService.getContenidoMenu(BASEURL,request.getRequestURI(),path);    %>

    
 <% String msj = request.getParameter("msj");  %>

  <form action="<%=CONTROLLER %>?estado=Archivo&accion=Movimiento&evento=MIGRAR" method="post" name="formulario">  
             <table   border="2" align="center" width='800'>
                 <tr>
                    <td colspan='2'>             

                                <table width="100%"   align="center">
                                          <tr>
                                             <td >
                                                  <table width='100%'  class="barratitulo">
                                                        <tr class="fila">
                                                                <td align="left" width='40%' class="subtitulo1" nowrap> ARCHIVO DE MOVIMIENTO</td>
                                                                <td align="left" width='*' ><img src="<%=BASEURL%>/images/titulo.gif" width="32"  height="20"  align="left"><%=datos[0]%></td>
                                                        </tr>
                                                  </table>
                                             </td>
                                          </tr>
                 
                                          <tr class="fila">
                                             <td  width='100%' >
                                                  <table width='40%'>
                                                     <tr class="fila">
                                                          <td width='10%'><%= model.ArchivoMovimientoSvc.getDistrito() %></td>
                                                          <td width='20%'><%= model.ArchivoMovimientoSvc.getBanco()    %></td>
                                                          <td width='40%'><%= model.ArchivoMovimientoSvc.getSucursal() %></td>
                                                          <td width='*'  ><%= model.ArchivoMovimientoSvc.getCorrida()  %></td>
                                                     </tr>
                                                  </table>
                                                 
                                                 
                                                 
                                                 
                                             </td>
                                          </tr>
                                          
                                           <tr class="fila">
                                                <td width='100%'>
                                                      <table width='100%' border="1" bordercolor="#999999" bgcolor="#F7F5F4" align="center">
                                                      
                                                         <tr class="tblTitulo" >
                                                              <TH   colspan='2' title='No aplica para la generación del archivo de migración' >DATOS PROPIETARIO    </TH>
                                                              <TH   colspan='3' >DATOS REGISTRO       </TH>
                                                              <TH   colspan='9' >DATOS TRANSFERENCIA  </TH>
                                                              <TH   colspan='4' >CLIENTE              </TH>     
                                                         </tr>
                                                         
                                                         
                                                          <tr class="tblTitulo" >
                                                              <TH style="font size:11">NIT               </TH>
                                                              <TH style="font size:11">ID MIMS           </TH>
                                                              
                                                              <TH style="font size:11">SECUENCIA REGISTRO</TH>
                                                              <TH style="font size:11">SECUENCIA CUENTA  </TH>
                                                              <TH style="font size:11">FECHA             </TH>
                                                              
                                                              <TH style="font size:11">NIT CUENTA        </TH>
                                                              <TH style="font size:11">NOMBRE CUENTA     </TH>
                                                              <TH style="font size:11">COD. BANCO        </TH>                                                              
                                                              <TH style="font size:11">TIPO CUENTA       </TH>
                                                              <TH style="font size:11">NO. CUENTA        </TH>
                                                              <TH style="font size:11">TIPO TRANS.       </TH>
                                                              <TH style="font size:11">VALOR             </TH>
                                                              <TH style="font size:11">OBSERVACION       </TH>
                                                              <TH style="font size:11">NOTA ADICIONAL    </TH>
                                                              
                                                              <TH style="font size:11">NIT CLIENTE       </TH> 
                                                              <TH style="font size:11">NO CUENTA CLI.    </TH>
                                                              <TH style="font size:11">TIPO CUENTA CLI.  </TH>
                                                              <TH style="font size:11">TIPO OPERACION    </TH>
            
                                                         </tr>
                                                         
                                                         <% List lista = model.ArchivoMovimientoSvc.getListCheques();
                                                            for(int i=0;i<lista.size();i++){
                                                               ArchivoMovimiento  am =(ArchivoMovimiento)lista.get(i);  %>
                                                               <tr  class='<%= (i%2==0?"filagris":"filaazul") %>'>
                                                                   
                                                                         <td nowrap class="bordereporte" style="font size:11"> <%= am.getNitProveedor()             %> </td>
                                                                         <td nowrap class="bordereporte" style="font size:11"> <%= am.getIdMims()                   %> </td>
                                                                         
                                                                         
                                                                         <td nowrap class="bordereporte" style="font size:11"> <%= am.getSecuencia()                %> </td>
                                                                         <td nowrap class="bordereporte" style="font size:11"> <%= am.getSecuenciaProveedor()       %> </td>
                                                                         <td nowrap class="bordereporte" style="font size:11"> <%= am.getFecha()                    %> </td>
                                                                         <td nowrap class="bordereporte" style="font size:11"> <%= am.getCedulaCuenta()             %> </td>
                                                                         <td nowrap class="bordereporte" style="font size:11"> <%= am.getNombreCuenta()             %> </td>
                                                                        
                                                                         <td nowrap class="bordereporte" style="font size:11"> <%= am.getCodigoBancoProveedor()     %> </td>
                                                                         <td nowrap class="bordereporte" style="font size:11"> <%= am.getTipoCuentaProveedor()      %> </td>
                                                                         <td nowrap class="bordereporte" style="font size:11"> <%= am.getNoCuentaProveedor()        %> </td>
                                                                         <td nowrap class="bordereporte" style="font size:11"> <%= am.getTipoTransaccionProveedor() %> </td>
                                                                         <td nowrap class="bordereporte" style="font size:11"> <%= am.getValor()                    %> </td>
                                                                         <td nowrap class="bordereporte" style="font size:11"> <%= am.getObservacion()              %> </td>
                                                                         <td nowrap class="bordereporte" style="font size:11"> <%= am.getSecuenciaNotaAdicional()   %> </td>

                                                                         <td nowrap class="bordereporte" style="font size:11"> <%= am.getNitCliente()               %> </td>
                                                                         <td nowrap class="bordereporte" style="font size:11"> <%= am.getNoCuentaCliente()          %> </td>
                                                                         <td nowrap class="bordereporte" style="font size:11"> <%= am.getTipoCuentaCliente()        %> </td>
                                                                         <td nowrap class="bordereporte" style="font size:11"> <%= am.getTipoOperacionCliente()     %> </td>
                                                                  
                                                                                                                   
                                                               </tr>
                                                                
                                                         <%}%>
                                                     </table>
                                                </td>
                                           </tr>
                                          
                              </table>
                      </td>
                  </tr>
              </table>
     </form>

      <p>       
          <% if ( model.ArchivoMovimientoSvc.isActivoBtn() ){%>
           <img src='<%=BASEURL%>/images/botones/aceptar.gif'    style=" cursor:hand"   title='Aceptar....'     name='i_crear'      onclick="formulario.submit()"                                                           onMouseOver='botonOver(this);' onMouseOut='botonOut(this);'>        
          <%}%>
          <img src='<%=BASEURL%>/images/botones/regresar.gif'    style=' cursor:hand'   title='Regresar...'     name='i_regre'      onclick="location.href='<%=BASEURL%>/jsp/general/exportar/ArchivoMovimiento.jsp'"       onMouseOver='botonOver(this);' onMouseOut='botonOut(this);'>
          <img src='<%=BASEURL%>/images/botones/salir.gif'       style=' cursor:hand'   title='Salir...'        name='i_salir'      onclick='parent.close();'                                                               onMouseOver='botonOver(this);' onMouseOut='botonOut(this);'>
      </p> 
      
      
      
      
      
      
     <!-- Mensaje -->
     <%  if ( msj!=null && !msj.equals("")){%>
             <table border="2" align="center">
                   <tr>
                        <td>
                               <table width="100%"  border="1" align="center"  bordercolor="#F7F5F4" bgcolor="#FFFFFF">
                                  <tr>
                                        <td width="500" align="center" class="mensajes"><%=msj%></td>
                                        <td width="29" background="<%=BASEURL%>/images/cuadronaranja.JPG">&nbsp;</td>
                                        <td width="58">&nbsp; </td>
                                  </tr>
                                </table>
                       </td>
                   </tr>
            </table>
     <%}%>

</div>
<%=datos[1]%> 


</body>
</html>
