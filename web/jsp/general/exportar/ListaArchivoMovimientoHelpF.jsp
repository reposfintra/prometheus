<%@ include file="/WEB-INF/InitModel.jsp"%>
<html>
<head>
  <title>Transferencia a Bancos- Ayuda</title>
  <META http-equiv=Content-Type content="text/html; charset=windows-1252">
  <link href="<%=BASEURL%>/css/estilostsp.css" rel="stylesheet" type="text/css"> 
  <script type='text/javascript' src="<%= BASEURL %>/js/boton.js"></script>
</head>
<body>
<center>




<% String BASEIMG = BASEURL +"/images/ayuda/cxpagar/transferenciaMims/"; %>

<table width="100%"  border="2" align="center">
    <tr>
      <td >
 
            <table width='99%' align="center" cellpadding='0' cellspacing='0'>
                 <tr class="subtitulo" ><td height="20"><div align="center">MANUAL DE TRANSFERENCIA A BANCOS </div></td></tr>
                 <tr class="subtitulo1"> <td>Descripci&oacute;n del funcionamiento </td> </tr>

                 
                  <tr>
                      <td  class="ayudaHtmlTexto" height='40'>
                         Presenta una vista previa del archivo a migrar. Este archivo de migraci�n deber� tener la estructura descrita en el formato enviado por el banco en el numeral 2.
                         Para ver el archivo deber� dar click 
                         <a href='#' onclick=" var win = window.open('<%= BASEIMG%>BancocreditoTEF.doc','planViaje',' top=100,left=100, width=700, height=600, scrollbars=yes, status=yes, resizable=yes  ');">aqui</a>
                  
                         <br><br>
                         La vista previa consta de una cabecera donde se describe los datos  del banco, sucursal y n�mero de la corrida a migrar, 
                         tal como lo indica la figura 1.
                      </td>
                 </tr>
                 
                 
                 <tr><td  align="center" > 
                     <br><br>
                     <img  src="<%= BASEIMG%>Encabezado.JPG" >                  
                     <br>
                     <strong>Figura 1</strong>
                     <br><br>
                     <br><br>
                 </td></tr>
                 
                 <tr>
                      <td  class="ayudaHtmlTexto" height='40'>
                         Posteriormente se presenta el cuerpo de la migraci�n, la cual consta de los siguientes grupos:
                         <ul>
                             <li>Datos del propietario: No interviene en el archivo de migraci�n, es tan solo datos de refenencia para el usuario.</li>
                             <li>Datos de Registros</li>
                             <li>Datos de Transferencia</li>
                             <li>Datos del Cliente</li>
                         </ul>
                      </td>
                 </tr>
                 
                 
                 
                 
                 <tr>
                     <td  class="ayudaHtmlTexto" >
                        <b>Datos del propietario:</b><br>
                         Se muestra el nit y el c�digo en mims del propietario en el registro de la corrida, tal como lo indica la figura 2.
                         No interviene en el archivo de migraci�n, es tan solo datos de refenencia para el usuario.
                     </td>
                </tr>   
                <tr><td  align="center" > 
                     <br><br>
                     <img  src="<%= BASEIMG%>DatosPropietario.JPG" >                  
                     <br>
                     <strong>Figura 2</strong>
                     <br><br>
                     <br><br>
                 </td></tr>
                
                 
                 
                 
                 <tr>
                     <td  class="ayudaHtmlTexto" >
                        <b>Datos de Registros:</b><br>
                         Se muestra la secuencia del registro, la secuencia del nit de la cuenta y la fecha del registro, tal como lo indica la figura 3.
                     </td>
                </tr>   
                <tr><td  align="center" > 
                     <br><br>
                     <img  src="<%= BASEIMG%>DatosRegistro.JPG" >                  
                     <br>
                     <strong>Figura 3</strong>
                     <br><br>
                     <br><br>
                 </td></tr>
                 
                 
                 
                 
                 <tr>
                     <td  class="ayudaHtmlTexto" >
                        <b>Datos de Transferencia:</b><br>
                         Se muestra el nit de la cuenta, nombre de la cuenta, c�digo del banco(c�digos establecidos en tabla general),tipo de cuenta,
                         n�mero de la cuenta,tipo de transacci�n, valor, observaci�n, y nota adicional
                         , tal como lo indica la figura 4.
                     </td>
                </tr> 
                <tr><td  align="center" > 
                     <br><br>
                     <img  src="<%= BASEIMG%>DatosTransferencia.JPG" >                  
                     <br>
                     <strong>Figura 4</strong>
                     <br><br>
                     <br><br>
                 </td></tr>
                 
                 
                 
                  <tr>
                     <td  class="ayudaHtmlTexto" >
                        <b>Datos del Cliente:</b><br>
                         Se muestra datos del cliente, en este caso datos de TSP.
                         Se muestra nit del cliente, n�mero de la cuenta, tipo de cuenta y tipo de operaci�n
                         , tal como lo indica la figura 5.
                     </td>
                </tr>
                <tr><td  align="center" > 
                     <br><br>
                     <img  src="<%= BASEIMG%>DatosCliente.JPG" >                  
                     <br>
                     <strong>Figura 5</strong>
                     <br><br>
                     <br><br>
                 </td></tr>
                
                 
                 
                 
                 
                <tr>
                     <td  class="ayudaHtmlTexto" >
                        Para realizar la generaci�n del archivo a migrar, deber� hacer click en el bot�n "Aceptar", el cual estar� activo
                        solamente para las corridas que no se han migrado para transferencia.
                        <br>
                        El proceso de generaci�n del archivo lo llevar� a cabo una clase tipo Threads, su estado podr� visualizarlo en el
                        log de procesos del sitio.
                        El archivo generado, se guardar� en la carpeta del usuario en sessi�n. Para acceder a el, deber� dirigirse a la vista de 
                        archivos del usuario realizando click en el icono ubicado en la parte superior de la p�gina principal del sitio, tal
                        como lo indica la figura 6.
                        
                     </td>
                </tr>
                
                <tr><td  align="center" > 
                     <br><br>
                     <img  src="<%= BASEIMG%>dir.JPG" >                  
                     <br>
                     <strong>Figura 6</strong>
                     <br><br>
                     <br><br>
                 </td></tr>
                
                
            </table>
            
      </td>
  </tr>
</table>
<br><br>
<img src="<%=BASEURL%>/images/botones/salir.gif"      name="exit"    height="21"  title='Salir'                onClick="parent.close();"                 onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" style="cursor:hand">          



</body>
</html>
