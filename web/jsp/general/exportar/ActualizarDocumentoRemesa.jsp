<!--
- Nombre P�gina :                  ActualizarDocumentoRemesa.jsp                 
- Descripci�n :                    Pagina JSP, que permite actualizar los documentos de las remesas.
- Autor :                          LREALES                        
- Fecha Creado :                   25 de Julio de 2006, 9:44 A.M.                        
- Versi�n :                        1.0                                       
- Copyright :                      Fintravalores S.A.                   
-->

<%-- Declaracion de librerias--%>
<%@page contentType="text/html"%>
<%@ page session="true"%>
<%//@ page errorPage="/error/error.jsp"%>
<%@page import="com.tsp.operation.model.beans.*, java.util.*"%>
<%@ include file="/WEB-INF/InitModel.jsp"%>
<%@page import="com.tsp.operation.model.*"%>
<%@page import="com.tsp.util.*"%>
<%@ taglib uri="http://jakarta.apache.org/taglibs/input-0.90" prefix="input" %>
<%
  String path = application.getRealPath(model.menuService.getRealUrl(request.getRequestURI()));
  String datos[] = model.menuService.getContenidoMenu(BASEURL,request.getRequestURI(),path);
%>
<html>
<head><title>Actualizar Documentos de las Remesas</title>
<link href="<%= BASEURL %>/css/estilostsp.css" rel="stylesheet" type="text/css">
<script src='<%=BASEURL%>/js/date-picker.js'></script>
<script type='text/javascript' src="<%= BASEURL %>/js/boton.js"></script>
<script type='text/javascript' src="<%= BASEURL %>/js/validar.js"></script>
</head>
<body onLoad="redimensionar();" onResize="redimensionar();">
<div id="capaSuperior" style="position:absolute; width:100%; height:100px; z-index:0; left: 0px; top: 0px;">
<jsp:include page="/WEB-INF/toptsp.jsp?encabezado=Actualizar Remesas"/>
</div>

<div id="capaCentral" style="position:absolute; width:100%; height:85%; z-index:0; left: 0px; top: 100px; overflow: scroll;"> 
<%
    Usuario usuario = (Usuario) session.getAttribute("Usuario");  	
%>
<script>
	var controlador ="<%=CONTROLLER%>";
	
	function Actualizar (){
		
		document.form2.action = controlador + "?estado=Actualizar&accion=DocumentoRemesa";
		document.form2.submit();
					
	}
</script>

<form name="form2" method="post">
    <table width="80%" border="2" align="center">
      <tr>
        <td><table width="100%" align="center"  class="tablaInferior">
            <tr>
				<td width="30%" class="subtitulo1">Actualizar Documentos de las Remesas</td>
				<td width="50%" class="barratitulo"><img src="<%=BASEURL%>/images/titulo.gif" width="32" height="20" align="left"><%=datos[0]%></td>
            </tr>
            <tr class="fila">
              <td colspan="2" align="left" valign="middle"><p>&nbsp;</p>
              <p>Si desea comenzar la actualizaci&oacute;n de los documentos de las remesas, puede dar click en el bot&oacute;n 'ACEPTAR'..</p>
              <p>&nbsp;</p></td>
            </tr>		
        </table></td>
      </tr>
    </table>
  <p>
	<div align="center">
	  <p>
		<img src="<%=BASEURL%>/images/botones/aceptar.gif" style="cursor:hand" title="Actualizar Remesas.." name="exportar" onClick="Actualizar (); this.disabled=true;" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);">&nbsp;   
		<img src="<%=BASEURL%>/images/botones/salir.gif"  name="salir" style="cursor:hand" title="Salir al Menu Principal" onMouseOver="botonOver(this);" onClick="parent.close();" onMouseOut="botonOut(this);" >
	  </p>
	  <%String msg = request.getParameter("msg");
		if ( msg!=null && !msg.equals("") ){%>
							
		  <table border="2" align="center">
			<tr>
				<td>
					<table width="100%"  border="1" align="center"  bordercolor="#F7F5F4" bgcolor="#FFFFFF">
						<tr>
							<td width="229" align="center" class="mensajes"><%=msg%></td>
							<td width="29" background="<%=BASEURL%>/images/cuadronaranja.JPG">&nbsp;</td>
							<td width="58">&nbsp;</td>
						</tr>
					</table>
				</td>
			</tr>
		  </table>
		<%}%>
	  </p>
	</div>
  </p>
</form>
</div>
<%=datos[1]%>
</body>
</html>