<%@ include file="/WEB-INF/InitModel.jsp"%>
<html>
<head>
      <title>Transferencia a Bancos- Ayuda</title>
      <META http-equiv=Content-Type content="text/html; charset=windows-1252">
      <link href="<%=BASEURL%>/css/estilostsp.css" rel="stylesheet" type="text/css"> 
      <script type='text/javascript' src="<%= BASEURL %>/js/boton.js"></script>
</head>
<body>
<center>


<table width="696"  border="2" align="center">
  <tr>
    <td width="811" >
    
        <table width="100%" border="0" align="center">
            <tr  class="subtitulo">
              <tr class="subtitulo" ><td height="20" colspan='2'><div align="center">MANUAL DE TRANSFERENCIA A BANCOS</div></td></tr>
            </tr>
         
            
             <tr>
                 <td width="172" class="fila"> DISTRITO</td>
                 <td width="502"  class="ayudaHtmlTexto">Se establece el distrito del usuario.</td>
            </tr>

            <tr>
                 <td width="172" class="fila"> BANCO</td>
                 <td width="502"  class="ayudaHtmlTexto">Deber� digitar el banco de la corrida. Campo alfanum�rico de  15 caracteres m�ximos.</td>
            </tr>
            
            <tr>
                 <td width="172" class="fila"> SUCURSAL</td>
                 <td width="502"  class="ayudaHtmlTexto">Deber� digitar la sucursal del banco de la corrida. Campo alfanum�rico de  30 caracteres m�ximos.</td>
            </tr>
            
            <tr>
                 <td width="172" class="fila"> CORRIDA</td>
                 <td width="502"  class="ayudaHtmlTexto">Deber� digitar el n�mero de la corrida. Campo num�rico de  6 caracteres.</td>
            </tr>

            
            <tr>
                 <td width="172" class="fila"> BOT�N ACEPTAR</td>
                 <td width="502"  class="ayudaHtmlTexto">Realiza el proceso de busqueda y muestra una vista previa de los datos a migrar.</td>
            </tr>
            
            
            <tr>
                 <td width="172" class="fila"> BOT�N SALIR</td>
                 <td width="502"  class="ayudaHtmlTexto">Cierra la ventana</td>
            </tr>
            
            
            
                 
        </table>
        
    </td>
  </tr>
</table>


<P>
  <img src="<%=BASEURL%>/images/botones/salir.gif"      name="exit"    height="21"  title='Salir'                onClick="parent.close();"                 onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" style="cursor:hand">          
</P>

</body>
</html>
