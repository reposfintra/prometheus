<!--
- Autor            : LREALES
- Date             : 27 de septiembre de 2006
- Modificado       : 28 de noviembre de 2006
- Copyrigth Notice : Fintravalores S.A. S.A
-->
<%--
-@(#)
--Descripcion : Pagina JSP, que se encarga de realizar y mostrar busqueda de los codigos de los clientes
--%>
<%--
The taglib directive below imports the JSTL library. If you uncomment it,
you must also add the JSTL library to the project. The Add Library... action
on Libraries node in Projects view can be used to add the JSTL 1.1 library.
--%>
<%-- Declaracion de librerias--%>
<%@page contentType="text/html"%>
<%@page import="com.tsp.operation.model.*"%>
<%@page import="com.tsp.util.*"%>
<%@page import="com.tsp.operation.model.beans.*, java.util.*"%>
<%@include file="/WEB-INF/InitModel.jsp"%>
<%@taglib uri="http://jakarta.apache.org/taglibs/input-0.90" prefix="input" %>
<html>
<head>
<script type='text/javascript' src="<%= BASEURL %>/js/boton.js"></script>
<script type='text/javascript' src="<%= BASEURL %>/js/reporte.js"></script>
<script language="javascript">    
	function asignar ( cod ) {
	
		var campo = parent.opener.document.formulario.cod_cli;
		campo.value = "" + cod;
		window.self.close();
		
	}
	
    function procesar ( element ) {
	
        if ( window.event.keyCode == 13 ) { 
		
		    lista ();
	   
	    }
	  
    }
	
	function lista () {
	
		document.forma1.action = "<%=CONTROLLER%>?estado=Buscar&accion=Cliente"; 
        document.forma1.submit(); 
		
    }
</script>
<title>Buscar el Codigo del Cliente</title>
<link href="<%= BASEURL %>/css/estilostsp.css" rel="stylesheet" type="text/css">
<link href="../../../css/estilostsp.css" rel="stylesheet" type="text/css">
</head>
<body onresize="redimensionar()" onload = 'forma1.nom_cli.focus(); redimensionar()'>
<div id="capaSuperior" style="position:absolute; width:100%; height:100px; z-index:0; left: 0px; top: 0px;">
 <jsp:include page="/toptsp.jsp?encabezado=Buscar Cliente"/>
</div>
<div id="capaCentral" style="position:absolute; width:100%; height:87%; z-index:0; left: 0px; top: 100px; overflow: scroll;">
<%
  String accion = (request.getParameter("accion")!=null)?request.getParameter("accion"):"";
%>
<form name="forma1"  method="post" action="<%=CONTROLLER%>?estado=Buscar&accion=Cliente">      
    <table border="2" align="center" width="700">
        <tr>
            <td>
                <table width="100%" class="tablaInferior">
                    <tr>
                        <td width="30%" class="subtitulo1" align="left">Buscar el Codigo del Cliente</td>
                        <td width="70%" class="barratitulo"><img src="<%=BASEURL%>/images/titulo.gif" width="32" height="20"></td>
                    </tr>
                </table>        
                <table width="700" border="0" align="center" class="tablaInferior">
                    <tr class="fila">
                        
            <td width="30%">Nombre del Cliente</td>
                      <td width="70%" > 
                        <input name="nom_cli" id="nom_cli" type="text" class="textbox" size="50" maxlength="50">
                        <img src='<%=BASEURL%>/images/botones/buscar.gif' name='Buscar' align="absmiddle" style='cursor:hand' title='Buscar Nombre del Cliente..'  onClick="forma1.submit();"  onMouseOver='botonOver(this);' onMouseOut='botonOut(this);'> 
					  </td>
                    </tr>
              </table>
            </td>
        </tr>
    </table>
<%
if ( accion.equals("1") ) {

    Vector datos = model.reporteDemorasService.getVector_clientes ();
	if( datos != null && datos.size() > 0 ){
%>
<br>  <table width="700" border="2" align="center">
    <tr>
    <td width="700">
		<table width="100%" border="1" borderColor="#999999" bgcolor="#F7F5F4">
          <tr class="tblTitulo">
    		<td>Lista de Clientes</td>    
  		  </tr>
		</table>
        <table width="100%" border="1" borderColor="#999999" align="center" >
<%
        for ( int i=0; i < datos.size(); i++ ) {
            Vector info = ( Vector )datos.elementAt( i );
%>  
  <tr class="<%=(i % 2 == 0 )?"filagris":"filaazul"%>" onMouseOver='cambiarColorMouse(this)' >
    <td width="100" height="20" class="bordereporte" > 
		<a onClick="asignar('<%=info.elementAt(0)%>')" style="cursor:hand" ><%=info.elementAt(0)%></a>
	</td>
	<td width="600" height="20" class="bordereporte" > 
		<a onClick="asignar('<%=info.elementAt(0)%>')" style="cursor:hand" ><%=info.elementAt(1)%></a>
	</td>
  </tr>
<%
        }
	} else { %> 
	  <br>               
	  <%out.print("<table width=379 border=2 align=center><tr><td><table width=100%  border=1 align=center  bordercolor=#F7F5F4 bgcolor=#FFFFFF><tr><td width=350 align=center class=mensajes>No Existen Clientes con ese nombre!</td><td width=100><img src="+BASEURL+"/images/cuadronaranja.JPG></td></tr></table></td></tr></table>");%>
	  <br>
  <%}		
}%>  
</table>
</td>
</tr>
</table>
<br>
</form>
<br>
<table width="700" border="0" cellspacing="0" cellpadding="0" align="center">
  <tr>
    <td><img src="<%=BASEURL%>/images/botones/salir.gif"  name="salir" title="Salir a la vista anterior.." style="cursor:hand" onClick="window.close();" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);"></td>
  </tr>
</table>
</div>
</bodY>
</html>