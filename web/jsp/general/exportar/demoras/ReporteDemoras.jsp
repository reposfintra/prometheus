<!--
- Autor : LREALES
- Date  : 27 de septiembre de 2006
- Copyrigth Notice : Fintravalores S.A. S.A
-->
<%--
-@(#)
--Descripcion : Pagina JSP, que se encarga de mostrar el reporte de demoras
--%>
<%--
The taglib directive below imports the JSTL library. If you uncomment it,
you must also add the JSTL library to the project. The Add Library... action
on Libraries node in Projects view can be used to add the JSTL 1.1 library.
--%>
<%-- Declaracion de librerias--%>
<%@page session="true"%>
<%@page errorPage="../error/ErrorPage.jsp"%>
<%@page import="com.tsp.operation.model.beans.*, java.util.*, java.text.*"%>
<%@ include file="/WEB-INF/InitModel.jsp"%>
<%@ taglib uri="http://jakarta.apache.org/taglibs/input-0.90" prefix="input" %>
<%@page import="com.tsp.util.*"%>
<% Util u = new Util(); %>
<html>
    <head>        
        <title>Reporte de Demoras</title>
		<script src='<%=BASEURL%>/js/date-picker.js'></script>
		<script language="JavaScript" type="text/javascript" src="<%=BASEURL%>/js/boton.js"></script>
		<script type='text/javascript' src="<%=BASEURL%>/js/validar.js"></script> 
		<link href="../css/estilostsp.css" rel="stylesheet" type="text/css">
		<link href="<%=BASEURL%>/css/estilostsp.css" rel="stylesheet" type="text/css">
		 <script>
		 	function Exportar (){
				
			    alert( 
				
					'Puede observar el seguimiento del proceso en la siguiente ruta del menu:' + '\n' + '\n' +
					'SLT -> PROCESOS -> Seguimiento de Procesos' + '\n' + '\n' + '\n' +
					'Y puede observar el archivo excel generado, en la siguiente parte:' + '\n' + '\n' +
					'En la pantalla de bienvenida de la Pagina WEB,' + '\n' +
					'vaya a la parte superior-derecha,' + '\n' +
					'y presione un click en el icono que le aparece.'
					
				);
				
			}
		</script>
    </head>
   <body onResize="redimensionar()" onLoad="redimensionar()">
	<div id="capaSuperior" style="position:absolute; width:100%; height:100px; z-index:0; left: 1px; top: 0px;">
	 <jsp:include page="/toptsp.jsp?encabezado=Reporte de Demoras"/>
	</div>
	<div id="capaCentral" style="position:absolute; width:100%; height:87%; z-index:0; left: 0px; top: 100px; overflow: scroll;">
	<%String msg = request.getParameter("msg");
		if ( msg!=null && !msg.equals("") ){%>							
		  <table border="2" align="center">
			<tr>
				<td>
					<table width="100%"  border="1" align="center"  bordercolor="#F7F5F4" bgcolor="#FFFFFF">
						<tr>
							<td width="229" align="center" class="mensajes"><%=msg%></td>
							<td width="29" background="<%=BASEURL%>/images/cuadronaranja.JPG">&nbsp;</td>
							<td width="58">&nbsp;</td>
						</tr>
					</table>
				</td>
			</tr>
		  </table>		  
		        <br>
		        <img src="<%=BASEURL%>/images/botones/regresar.gif" name="c_regresar" onclick="location.href='<%=BASEURL%>/jsp/general/exportar/demoras/ConsultarReporteDemoras.jsp'" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" style="cursor:hand">
 	  <input name="salir" src="<%=BASEURL%>/images/botones/salir.gif" type="image" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" style="cursor:hand" class="boton" onclick="window.close();">
	  <%}
		else {
          Vector datos = model.reporteDemorasService.getVectorReporte ();  
					
		if( datos != null && datos.size() > 0 ){
			BeanGeneral info_fechas = (BeanGeneral) datos.elementAt(0);
		%>		
		    <form name="form1" method="post" action="<%=CONTROLLER%>?estado=Reporte&accion=Demoras&hacer=2">
      <input name="fecini" type="hidden" id="fecini" value="<%=info_fechas.getValor_16()%>">
	  <input name="fecfin" type="hidden" id="fecfin" value="<%=info_fechas.getValor_17()%>">
      <input name="Guardar2" src="<%=BASEURL%>/images/botones/exportarExcel.gif" type="image" class="boton" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" style="cursor:hand" onclick="Exportar();">
      <img src="<%=BASEURL%>/images/botones/regresar.gif" name="c_regresar" onclick="location.href='<%=BASEURL%>/jsp/general/exportar/demoras/ConsultarReporteDemoras.jsp'" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" style="cursor:hand">
 	  <input name="salir" src="<%=BASEURL%>/images/botones/salir.gif" type="image" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" style="cursor:hand" class="boton" onclick="window.close();">
    </form>
		<table width="100%"  border="2">  
          <tr>
        	<td class="subtitulo1"><strong>Reporte de Demoras Desde: <%=info_fechas.getValor_16()%> - Hasta: <%=info_fechas.getValor_17()%></strong></td>
          </tr>
          <tr>
            <td>
              <table width="100%"  border="1" bordercolor="#999999" bgcolor="#F7F5F4" align="center">
                <tr class="tblTitulo" align="center">
                    <td nowrap align="center">PLANILLA</td>
                    <td nowrap align="center">FECHA DESPACHO</td>
                    <td nowrap align="center">PLACA</td>
                    <td nowrap align="center">REMESA</td>
                    <td nowrap align="center">DESCRIPCION</td>
                    <td nowrap align="center">CAUSA VARADO</td>
                    <td nowrap align="center">FECHA INICIO</td>
                    <td nowrap align="center">FECHA FIN</td>
                    <td nowrap align="center">DIF.DIAS</td>
                    <td nowrap align="center">ORIGEN PLANILLA</td>
                    <td nowrap align="center">DESTINO PLANILLA</td>
                    <td nowrap align="center">ORIGEN REMESA</td>
                    <td nowrap align="center">DESTINO REMESA</td>
                    <td nowrap align="center">AGENCIA</td>
                    <td nowrap align="center">CLIENTE</td>
                    <td nowrap align="center">NOMBRE CLIENTE</td>
                    <td nowrap align="center">NOMBRE CONDUCTOR</td>
                    <td nowrap align="center">NOMBRE DESTINATARIO</td> 
                    <td nowrap align="center">FACTURA</td>
                    <td nowrap align="center">USUARIO CREACION</td>          
                </tr>						
		<%
			for( int i = 0; i<datos.size(); i++ ){
				BeanGeneral info = (BeanGeneral) datos.elementAt(i);
	  %>  	  
                  <tr class="<%=(i % 2 == 0 )?"filagris":"filaazul"%>">
				  <td nowrap align="left" class="bordereporte"><%=(info.getValor_01()!=null)?info.getValor_01():""%>&nbsp;</td>
                  <td nowrap align="left" class="bordereporte"><%=(info.getValor_09()!=null)?info.getValor_09():""%>&nbsp;</td>
                  <td nowrap align="left" class="bordereporte"><%=(info.getValor_07()!=null)?info.getValor_07():""%>&nbsp;</td>
				  <td nowrap align="left" class="bordereporte"><%=(info.getValor_10()!=null)?info.getValor_10():""%>&nbsp;</td>
				  <td nowrap align="left" class="bordereporte"><%=(info.getValor_19()!=null)?info.getValor_19():""%>&nbsp;</td>
				  <td nowrap align="left" class="bordereporte"><%=(info.getValor_19()!=null && info.getValor_19().trim().equalsIgnoreCase("VARADO"))? info.getValor_22() :""%>&nbsp;</td>
				  <td nowrap align="left" class="bordereporte"><%=(info.getValor_02()!=null)?info.getValor_02():""%>&nbsp;</td>
				  <td nowrap align="left" class="bordereporte"><%=(info.getValor_03()!=null)?info.getValor_03():""%>&nbsp;</td>
				  <td nowrap align="right" class="bordereporte"><%=(info.getValor_21()!=null)?info.getValor_21():""%>&nbsp;</td>
                  <td nowrap align="left" class="bordereporte"><%=(info.getValor_05()!=null)?info.getValor_05():""%>&nbsp;</td>
                  <td nowrap align="left" class="bordereporte"><%=(info.getValor_06()!=null)?info.getValor_06():""%>&nbsp;</td>
				  <td nowrap align="left" class="bordereporte"><%=(info.getValor_11()!=null)?info.getValor_11():""%>&nbsp;</td>
                  <td nowrap align="left" class="bordereporte"><%=(info.getValor_12()!=null)?info.getValor_12():""%>&nbsp;</td>
                  <td nowrap align="left" class="bordereporte"><%=(info.getValor_04()!=null)?info.getValor_04():""%>&nbsp;</td>
                  <td nowrap align="left" class="bordereporte"><%=(info.getValor_13()!=null)?info.getValor_13():""%>&nbsp;</td>
                  <td nowrap align="left" class="bordereporte"><%=(info.getValor_14()!=null)?info.getValor_14():""%>&nbsp;</td>   
				  <td nowrap align="left" class="bordereporte"><%=(info.getValor_08()!=null)?info.getValor_08():""%>&nbsp;</td> 
				  <td nowrap align="left" class="bordereporte"><%=(info.getValor_15()!=null)?info.getValor_15():""%>&nbsp;</td> 
				  <td nowrap align="left" class="bordereporte"><%=(info.getValor_20()!=null)?info.getValor_20():""%>&nbsp;</td> 
				  <td nowrap align="left" class="bordereporte"><%=(info.getValor_18()!=null)?info.getValor_18():""%>&nbsp;</td>         
                </tr>
				         <%
	  	}%>
            </table>
      </table></td>
      </tr>
		    </table>			
		    <form name="form1" method="post" action="<%=CONTROLLER%>?estado=Reporte&accion=Demoras&hacer=2">
      <input name="fecini" type="hidden" id="fecini" value="<%=info_fechas.getValor_16()%>">
	  <input name="fecfin" type="hidden" id="fecfin" value="<%=info_fechas.getValor_17()%>">
      <input name="Guardar3" src="<%=BASEURL%>/images/botones/exportarExcel.gif" type="image" class="boton" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" style="cursor:hand" onclick="Exportar();">
      <img src="<%=BASEURL%>/images/botones/regresar.gif" name="c_regresar" onclick="location.href='<%=BASEURL%>/jsp/general/exportar/demoras/ConsultarReporteDemoras.jsp'" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" style="cursor:hand">
	  <input name="salir2" src="<%=BASEURL%>/images/botones/salir.gif" type="image" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" style="cursor:hand" class="boton" onclick="window.close();">
    </form>	
	  <%}	  
	  }
	  %>	  
	      </body>
		  </div>
</html>