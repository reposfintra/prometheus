<!--
- Autor : Ing. Jose de la rosa
- Date  : 15 de agosto de 2006, 08:24 AM
- Copyrigth Notice : Fintravalores S.A. S.A
-->
<%--
-@(#)
--Descripcion : Pagina JSP, que maneja el reporte del control de vacios
--%>

<%-- Declaracion de librerias--%>
<%@page contentType="text/html"%>
<%@page session="true"%>
<%@page import="java.util.*"%>
<%@page import="com.tsp.operation.model.*"%>
<%@page import="com.tsp.util.*"%>
<%@page import="com.tsp.operation.model.beans.*"%>
<%@include file="/WEB-INF/InitModel.jsp"%>
<%@ page errorPage="/error/ErrorPage.jsp"%>
<%@ taglib uri="http://jakarta.apache.org/taglibs/input-0.90" prefix="input" %>

<html>
<head>
<title>Reporte Formato de Cumplimiento</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link href="<%= BASEURL %>/css/estilostsp.css" rel="stylesheet" type="text/css"> 
<link href="../../../css/estilostsp.css" rel="stylesheet" type="text/css"> 
<script src='<%=BASEURL%>/js/date-picker.js'></script>
<script type='text/javascript' src="<%= BASEURL %>/js/validar.js"></script>
<script type='text/javascript' src="<%= BASEURL %>/js/boton.js"></script>
<script type="text/javascript" src="<%=BASEURL%>/js/script.js"></script>  
<script src='<%=BASEURL%>/js/tools.js' language='javascript'></script>
</head>
<%	String  hoy       = Utility.getHoy("-");  
	String msg = (String) request.getParameter("msg")!=null?request.getParameter("msg"):"";
	String path = application.getRealPath(model.menuService.getRealUrl(request.getRequestURI()));
	String datos[] = model.menuService.getContenidoMenu(BASEURL,request.getRequestURI(),path);
	TreeMap cli = (TreeMap) request.getAttribute("TClientes");         
	//cli.put(" TODOS","%");
%>
<%-- Inicio Body --%>
<body onLoad="redimensionar();" onResize="redimensionar();">
<div id="capaSuperior" style="position:absolute; width:100%; height:100px; z-index:0; left: 0px; top: 0px;">
	<jsp:include page="/WEB-INF/toptsp.jsp?encabezado=Reporte Formato de Cumplimiento"/>
</div>	
<div id="capaCentral" style="position:absolute; width:100%; height:85%; z-index:0; left: 0px; top: 100px; overflow: scroll;">
<form name="formulario" method="post" action="<%=CONTROLLER%>?estado=ReporteFormato&accion=Cumplimiento">
<table width="509" border="2" align="center">
	<tr>
		<td>  
			<table width='100%' align='center' class='tablaInferior'>           
				<tr >
					<td colspan='2' >
						<table cellpadding='0' cellspacing='0' width='100%' class="barratitulo">
                     		<tr>
                      			<td width='60%' class="subtitulo1">Reporte Formato de Cumplimiento</td>
                      			<td width='40%'  ><img src="<%=BASEURL%>/images/titulo.gif" width="32" height="20" align="left"><%=datos[0]%></td>
                    		</tr>
                   		</table>
                	</td>
             	</tr>              
              	<tr class="fila">
					<td class='letrafila' width='30%'> Fecha Inicial</td>
				  <td width='70%'>
						<!-- Fecha Inicial -->
			      <input  name='fechaInicio' class="textbox"  value='<%=hoy%>' size="12" maxlength="12" readonly="true">
						&nbsp; 
						<a href="javascript:void(0)" onClick="if(self.gfPop)gfPop.fPopCalendar(fechaInicio);return false;" hidefocus>
							<img name="popcal" align="absmiddle" src="<%=BASEURL%>/js/calendartsp/calbtn.gif" width="16" height="16" border="0" alt="" title="Haga clic aqui para seleccionar la Fecha Inicial del proceso">
						</a>
						<input name="distrito" type="hidden" id="distrito" value="FINV">
					</td>
			  </tr>
				 <tr class="fila">
					<td class='letrafila'>Fecha Final </td>
					<td> 
                    	<!-- Fecha Final -->                   
                     	<input  name='fechaFinal' class="textbox"  value='<%=hoy%>' size="12" maxlength="12" readonly="true">
                     	&nbsp;
                     	<a href="javascript:void(0)" onClick="if(self.gfPop)gfPop.fPopCalendar(fechaFinal);return false;" hidefocus>
                      		<img name="popcal" align="absmiddle" src="<%=BASEURL%>/js/calendartsp/calbtn.gif" width="16" height="16" border="0" alt="" title="Haga clic aqui para seleccionar la Fecha Final del proceso">
                     	</a>     
                	</td>
             	</tr>
				<tr class="fila">
				   <td class='letrafila'>Agencia (S) </td>
				   <td><a href="JavaScript:void(0);" class="Simulacion_Hiper" onClick="window.open('<%=BASEURL%>/agencia/agenciasDistrito.jsp?distrito=' + formulario.distrito.value,'','status=no,scrollbars=yes,resizable=yes')">Seleccionar Agencias</a>
				   <input name='agencias' type='hidden' id="agencias" value=""></td>
		      </tr>
				 <tr class="fila">
				   <td class='letrafila'>Cliente</td>
				   <td><input name="cliente" type="text" class="textbox" id="campo" style="width:200;" onKeyUp="buscar(document.formulario.cliente,this)" size="15" >
			       <span class="Simulacion_Hiper" style="cursor:hand " onClick="window.open('<%=BASEURL%>/jsp/general/exportar/consultasClientes.jsp','','HEIGHT=200,WIDTH=600,SCROLLBARS=YES,RESIZABLE=YES, STATUS=YES')">Consultar clientes...</span></td>
		      </tr>

              	<tr class="letra">
                	<td colspan="2" class='informacion'>* Haga click en el boton EXPORTAR para generar el reporte</td>
           	  </tr>
       	  </table>
      	</td>
   	</tr>
</table>
<br>
<center>	
	<img src="<%=BASEURL%>/images/botones/exportarExcel.gif" name="mod"  height="21" onClick=" if ( validarFechaIF(formulario) ) {formulario.submit();}" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" style="cursor:hand" title="Haga clic aqui para iniciar el proceso de generacion del reporte">
	<img src="<%=BASEURL%>/images/botones/salir.gif"  name="imgsalir"  height="21" onMouseOver="botonOver(this);" onClick="parent.close();" onMouseOut="botonOut(this);" style="cursor:hand" title="Haga clic aqui para salir de la pagina">
</center>
</form>
<% if(request.getAttribute ("mensaje")!=null){%>
<br>
<table width="550" border="2" align="center">
<tr>
	<td width="707">
		<table width="100%"  border="1" align="center"  bordercolor="#F7F5F4" bgcolor="#FFFFFF">
			<tr>
				<td width="50%" nowrap  class="mensajes">
					<%=request.getAttribute ("mensaje")%>
					<br>
					&nbsp;&nbsp;<a href="#" class="Simulacion_Hiper" onClick="window.open('<%= CONTROLLER %>?estado=Log&accion=Proceso&Estado=TODO','log','scroll=no, resizable=yes, width=800, height=600')" style="cursor:hand "><%=request.getParameter("ruta")%></a>
				</td>
				<td width="16%" background="<%=BASEURL%>/images/cuadronaranja.JPG">&nbsp;</td>
				<td width="34%">&nbsp;</td>
			</tr>
		</table>
	</td>
</tr>
</table>
<%}%>
</div>
<%=datos[1]%>
<!-- calendario-->
<iframe width="188" height="166" name="gToday:normal:<%=BASEURL%>/js/calendartsp/agenda.js:gfPop:<%=BASEURL%>/js/calendartsp/plugins.js" id="gToday:normal:<%=BASEURL%>/js/calendartsp/agenda.js:gfPop:<%=BASEURL%>/js/calendartsp/plugins.js" src="<%=BASEURL%>/js/calendartsp/ipopeng.htm" scrolling="no" frameborder="0" style="visibility:visible; z-index:999; position:absolute; left:-500px; top:0px;"></iframe>
</body>
</html>
