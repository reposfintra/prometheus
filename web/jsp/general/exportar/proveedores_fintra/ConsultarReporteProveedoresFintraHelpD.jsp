<!--  
	 - Author(s)       :      LREALES
	 - Description     :      AYUDA DESCRIPTIVA - Reporte Proveedores Fintra
	 - Date            :      14/09/2006  
	 - Copyright Notice:      TRANSPORTES SANCHEZ POLO S.A.
-->
<%@page session="true"%> 
<%@page errorPage="/error/ErrorPage.jsp"%>
<%@page import="com.tsp.operation.model.beans.*, java.util.*"%>
<%@ include file="/WEB-INF/InitModel.jsp"%>
<%@ taglib uri="http://jakarta.apache.org/taglibs/input-0.90" prefix="input" %>
<html>
<head>
    <title>Descripcion de los campos a ingresar en el Reporte de Proveedores de Fintra</title>
    <link href="<%= BASEURL %>/css/estilostsp.css" rel="stylesheet" type="text/css"> 
    <link href="/../../../css/estilostsp.css" rel="stylesheet" type="text/css">  
    
</head>
<script type='text/javascript' src="<%= BASEURL %>/js/boton.js"></script> 
<body> 
<div id="capaSuperior" style="position:absolute; width:100%; height:100px; z-index:0; left: 0px; top: 0px;">
<jsp:include page="/WEB-INF/toptsp.jsp?encabezado=Ayuda Descriptiva - Reporte de Proveedores de Fintra"/>
</div>
<div id="capaCentral" style="position:absolute; width:100%; height:90%; z-index:0; left: 0px; top: 100px;"> 
   <p>&nbsp;</p>
   <table width="65%" border="2" align="center" id="tabla1" >
        <td>
         <table width="100%" height="50%" align="center">
              <tr>
                <td class="subtitulo1"><strong>Reporte</strong></td>
                <td width="50%" class="barratitulo"><img src="<%=BASEURL%>/images/titulo.gif" width="32" height="20"></td>
              </tr>
         </table>
         <table width="100%" borderColor="#999999" bgcolor="#F7F5F4">
           <tr>
             <td colspan="2" class="tblTitulo"><strong>Reporte de Proveedores de Fintra - Pantalla Preliminar</strong></td>
           </tr>
           <tr>
             <td colspan="2" class="subtitulo"><strong>INFORMACION</strong></td>
           </tr>
           <tr>
             <td width="40%" class="fila">Fecha Inicial</td>
             <td width="60%"><span class="ayudaHtmlTexto">Campo para escoger la fecha inicial de la busqueda del  reporte que se desea ver.</span></td>
           </tr>
           <tr>
             <td class="fila">Fecha Final</td>
             <td><span class="ayudaHtmlTexto">Campo para escoger la fecha final de la busqueda del reporte que se desea ver.</span></td>
           </tr>
           <tr>
             <td class="fila">'Todos los Proveedores' </td>
             <td>Campo de seleccion donde defino que el parametro de busqueda es por todos los proveedores. </td>
           </tr>
           <tr>
             <td class="fila">'Nit del Proveedor' </td>
             <td>Campo de seleccion donde defino que el parametro de busqueda es por un nit de proveedor en especifico, el cual puedo digitar o buscar.</td>
           </tr>
           <tr>
             <td class="fila">Lupa &oacute; Bot&oacute;n Buscar </td>
             <td>Bot&oacute;n que permite realizar la busqueda especifica del Nit del Proveedor a partir del nombre. </td>
           </tr>
           <tr>
             <td class="fila">Bot&oacute;n Aceptar</td>
             <td><span class="ayudaHtmlTexto">Bot&oacute;n para realizar el procedimiento de b&uacute;squeda del Reporte de Proveedores de Fintra. </span></td>
           </tr>
           <tr>
             <td class="fila">Bot&oacute;n Salir</td>
             <td><span class="ayudaHtmlTexto">Bot&oacute;n para salir de la vista 'Reporte de Proveedores de Fintra' y volver a la vista del men&uacute;.</span></td>
           </tr>
         </table></td>
  </table>
		<p></p>
	<center>
	<img src = "<%=BASEURL%>/images/botones/salir.gif" style = "cursor:hand" name = "imgsalir" onClick = "parent.close();" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);">
	</center>
</div>  
</body>
</html>