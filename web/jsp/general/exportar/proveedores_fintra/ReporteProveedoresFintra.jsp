<!--
- Autor : LREALES
- Date  : 19 de septiembre de 2006
- Copyrigth Notice : Fintravalores S.A. S.A
-->
<%--
-@(#)
--Descripcion : Pagina JSP, que se encarga de mostrar el reporte de proveedores de fintra
--%>
<%--
The taglib directive below imports the JSTL library. If you uncomment it,
you must also add the JSTL library to the project. The Add Library... action
on Libraries node in Projects view can be used to add the JSTL 1.1 library.
--%>
<%-- Declaracion de librerias--%>
<%@page session="true"%>
<%@page errorPage="../error/ErrorPage.jsp"%>
<%@page import="com.tsp.operation.model.beans.*, java.util.*, java.text.*"%>
<%@ include file="/WEB-INF/InitModel.jsp"%>
<%@ taglib uri="http://jakarta.apache.org/taglibs/input-0.90" prefix="input" %>
<%@page import="com.tsp.util.*"%>
<% Util u = new Util(); %>
<html>
    <head>        
        <title>Reporte Proveedores Fintra</title>
		<script src='<%=BASEURL%>/js/date-picker.js'></script>
		<script language="JavaScript" type="text/javascript" src="<%=BASEURL%>/js/boton.js"></script>
		<script type='text/javascript' src="<%=BASEURL%>/js/validar.js"></script> 
		<link href="../css/estilostsp.css" rel="stylesheet" type="text/css">
		<link href="<%=BASEURL%>/css/estilostsp.css" rel="stylesheet" type="text/css">
		 <script>
		 	function Exportar (){
				
			    alert( 
				
					'Puede observar el seguimiento del proceso en la siguiente ruta del menu:' + '\n' + '\n' +
					'FINTRAVALORES -> PROCESOS -> Seguimiento de Procesos' + '\n' + '\n' + '\n' +
					'Y puede observar el archivo excel generado, en la siguiente parte:' + '\n' + '\n' +
					'En la pantalla de bienvenida de la Pagina WEB,' + '\n' +
					'vaya a la parte superior-derecha,' + '\n' +
					'y presione un click en el icono que le aparece.'
					
				);
				
			}
		</script>
    </head>
   <body onResize="redimensionar()" onLoad="redimensionar()">
	<div id="capaSuperior" style="position:absolute; width:100%; height:100px; z-index:0; left: 1px; top: 0px;">
	 <jsp:include page="/toptsp.jsp?encabezado=Reporte Proveedores Fintra"/>
	</div>
	<div id="capaCentral" style="position:absolute; width:100%; height:87%; z-index:0; left: 0px; top: 100px; overflow: scroll;">
	<%String msg = request.getParameter("msg");
		if ( msg!=null && !msg.equals("") ){%>							
		  <table border="2" align="center">
			<tr>
				<td>
					<table width="100%"  border="1" align="center"  bordercolor="#F7F5F4" bgcolor="#FFFFFF">
						<tr>
							<td width="229" align="center" class="mensajes"><%=msg%></td>
							<td width="29" background="<%=BASEURL%>/images/cuadronaranja.JPG">&nbsp;</td>
							<td width="58">&nbsp;</td>
						</tr>
					</table>
				</td>
			</tr>
		  </table>		  
		        <br>
		        <img src="<%=BASEURL%>/images/botones/regresar.gif" name="c_regresar" onclick="location.href='<%=BASEURL%>/jsp/general/exportar/proveedores_fintra/ConsultarReporteProveedoresFintra.jsp'" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" style="cursor:hand">
 	  <input name="salir" src="<%=BASEURL%>/images/botones/salir.gif" type="image" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" style="cursor:hand" class="boton" onclick="window.close();">
	  <%}
		else {
          Vector datos = model.reporteProveedoresFintraService.getVectorReporte ();  
					
		if( datos != null && datos.size() > 0 ){
			Movpla info_fechas = (Movpla) datos.elementAt(0);
		%>		
		    <form name="form1" method="post" action="<%=CONTROLLER%>?estado=Reporte&accion=ProveedoresFintra&hacer=2">
      <input name="fecini" type="hidden" id="fecini" value="<%=info_fechas.getApplicated_ind()%>">
	  <input name="fecfin" type="hidden" id="fecfin" value="<%=info_fechas.getApplication_ind()%>">
      <input name="Guardar2" src="<%=BASEURL%>/images/botones/exportarExcel.gif" type="image" class="boton" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" style="cursor:hand" onclick="Exportar();">
      <img src="<%=BASEURL%>/images/botones/regresar.gif" name="c_regresar" onclick="location.href='<%=BASEURL%>/jsp/general/exportar/proveedores_fintra/ConsultarReporteProveedoresFintra.jsp'" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" style="cursor:hand">
 	  <input name="salir" src="<%=BASEURL%>/images/botones/salir.gif" type="image" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" style="cursor:hand" class="boton" onclick="window.close();">
    </form>
		<table width="100%"  border="2">  
          <tr>
        	<td class="subtitulo1"><strong>Reporte de Proveedores de Fintra Desde: <%=info_fechas.getApplicated_ind()%> - Hasta: <%=info_fechas.getApplication_ind()%></strong></td>
          </tr>
          <tr>
            <td>
              <table width="100%"  border="1" bordercolor="#999999" bgcolor="#F7F5F4" align="center">
                <tr class="tblTitulo" align="center">
				  <td nowrap width="5%" align="center">NIT</td>
				  <td nowrap width="5%" align="center">PROVEEDOR</td>
                  <td nowrap width="5%" align="center">FECHA DOCUMENTO</td>
                  <td nowrap width="5%" align="center">PLANILLA</td>
                  <td nowrap width="5%" align="center">AGENCIA</td>
				  <td nowrap width="5%" align="center">ORIGEN</td>
				  <td nowrap width="5%" align="center">DESTINO</td>
                  <td nowrap width="5%" align="center">PLACA</td>
                  <td nowrap width="10%" align="center">PROPIETARIO</td>
                  <td nowrap width="5%" align="center">DESCUENTO</td>
				  <td nowrap width="5%" align="center">VALOR</td>
                  <td nowrap width="5%" align="center">VALOR ME</td>
				  <td nowrap width="5%" align="center">TASA</td>
                  <td nowrap width="5%" align="center">MONEDA</td>
                  <td nowrap width="5%" align="center">CHEQUE</td>
                  <td nowrap width="5%" align="center">BANCO</td>
                  <td nowrap width="5%" align="center">SUCURSAL</td>    
				  <td nowrap width="5%" align="center">CREATION_DATE</td>  
				  <td nowrap width="5%" align="center">CREATION_USER</td>             
                </tr>						
		<%
			for( int i = 0; i<datos.size(); i++ ){
				Movpla info = (Movpla) datos.elementAt(i);
	  %>  	  
                  <tr class="<%=(i % 2 == 0 )?"filagris":"filaazul"%>">
				  <td nowrap align="left" class="bordereporte"><%=info.getProveedor_anticipo()%>&nbsp;</td>
				  <td nowrap align="left" class="bordereporte"><%=info.getProveedor()%>&nbsp;</td>
                  <td nowrap align="center" class="bordereporte"><%=info.getDate_doc()%>&nbsp;</td>
                  <td nowrap align="center" class="bordereporte"><%=(info.getPlanilla()!=null)?info.getPlanilla():""%>&nbsp;</td>
                  <td nowrap align="left" class="bordereporte"><%=(info.getAgency_id()!=null)?info.getAgency_id():""%>&nbsp;</td>
				  <td nowrap align="left" class="bordereporte"><%=(info.getObservacion()!=null)?info.getObservacion():""%>&nbsp;</td>
				  <td nowrap align="left" class="bordereporte"><%=(info.getDocument_type()!=null)?info.getDocument_type():""%>&nbsp;</td>
                  <td nowrap align="center" class="bordereporte"><%=(info.getSupplier()!=null)?info.getSupplier():""%>&nbsp;</td>
                  <td nowrap align="left" class="bordereporte"><%=(info.getPla_owner()!=null)?info.getPla_owner():""%>&nbsp;</td>
                  <td nowrap align="right" class="bordereporte"><%=UtilFinanzas.customFormat("#,###.#########",info.getVlr_disc(),9)%>&nbsp;</td>
				  <td nowrap align="right" class="bordereporte"><%=UtilFinanzas.customFormat("#,###.#########",info.getVlr(),9)%>&nbsp;</td>
                  <td nowrap align="right" class="bordereporte"><%=UtilFinanzas.customFormat("#,###.#########",info.getVlr_for(),9)%>&nbsp;</td>
				  <td nowrap align="right" class="bordereporte"><%=(info.getTipo_rec()!=null)?info.getTipo_rec():""%>&nbsp;</td>
                  <td nowrap align="center" class="bordereporte"><%=(info.getCurrency()!=null)?info.getCurrency():""%>&nbsp;</td>
                  <td nowrap align="left" class="bordereporte"><%=(info.getDocument()!=null)?info.getDocument():""%>&nbsp;</td>
                  <td nowrap align="left" class="bordereporte"><%=(info.getBranch_code()!=null)?info.getBranch_code():""%>&nbsp;</td>
                  <td nowrap align="left" class="bordereporte"><%=(info.getBank_account_no()!=null)?info.getBank_account_no():""%>&nbsp;</td>   
				  <td nowrap align="left" class="bordereporte"><%=(info.getCreation_date()!=null)?info.getCreation_date():""%>&nbsp;</td> 
				  <td nowrap align="left" class="bordereporte"><%=(info.getCreation_user()!=null)?info.getCreation_user():""%>&nbsp;</td>         
                </tr>
				         <%
	  	}%>
            </table>
      </table></td>
      </tr>
		    </table>			
		    <form name="form1" method="post" action="<%=CONTROLLER%>?estado=Reporte&accion=ProveedoresFintra&hacer=2">
      <input name="fecini" type="hidden" id="fecini" value="<%=info_fechas.getApplicated_ind()%>">
	  <input name="fecfin" type="hidden" id="fecfin" value="<%=info_fechas.getApplication_ind()%>">
      <input name="Guardar3" src="<%=BASEURL%>/images/botones/exportarExcel.gif" type="image" class="boton" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" style="cursor:hand" onclick="Exportar();">
      <img src="<%=BASEURL%>/images/botones/regresar.gif" name="c_regresar" onclick="location.href='<%=BASEURL%>/jsp/general/exportar/proveedores_fintra/ConsultarReporteProveedoresFintra.jsp'" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" style="cursor:hand">
	  <input name="salir2" src="<%=BASEURL%>/images/botones/salir.gif" type="image" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" style="cursor:hand" class="boton" onclick="window.close();">
    </form>	
	  <%}	  
	  }
	  %>	  
	      </body>
		  </div>
</html>