<!--
	 - Author(s)       :      Jose de la Rosa
	 - Description     :      AYUDA FUNCIONAL - Formato Cumplimiento
	 - Date            :      01/11/2006
	 - Copyright Notice:      TRANSPORTES SANCHEZ POLO S.A.
-->
<%@ include file="/WEB-INF/InitModel.jsp"%>
<HTML>
<HEAD>
<TITLE>AYUDA FUNCIONAL - Reporte Formato Cumplimiento</TITLE>
<META http-equiv=Content-Type content="text/html; charset=windows-1252">
<link href="../css/estilostsp.css" rel="stylesheet" type="text/css"> 
<link href="<%=BASEURL%>/css/estilostsp.css" rel="stylesheet" type="text/css">
<script type='text/javascript' src="<%= BASEURL %>/js/boton.js"></script> 
</HEAD>
<BODY> 
<% String BASEIMG = BASEURL +"/images/ayuda/cxpagar/facturas/"; %>
  <table width="95%"  border="2" align="center">
    <tr>
      <td height="117" >
        <table width="100%" border="0" align="center">
          <tr  class="subtitulo">
            <td height="20"><div align="center">Reporte Formato Cumplimiento</div></td>
          </tr>
          <tr class="subtitulo1">
            <td>Descripci&oacute;n del funcionamiento del reporte de formato de cumplimiento.</td>
          </tr>
          <tr>
            <td  class="ayudaHtmlTexto"><div align="center">
              <p>&nbsp;</p>
              </div></td>
          </tr>
<tr>
            <td  class="ayudaHtmlTexto"><p class="ayudaHtmlTexto">En la siguiente pantalla se permitirá generar un reporte a exel del formato de cumplimiento entre un rango de fechas como se muestra en la siguiente imagen.</p>
            </td>
          </tr>
<tr>
            <td  class="ayudaHtmlTexto"><div align="center"><IMG src="<%=BASEIMG%>ImagenPrincipalFormatoCumplimiento.JPG" border=0 ></div></td>
          </tr>
		<tr>
		<td  class="ayudaHtmlTexto"><p class="ayudaHtmlTexto">&nbsp;</p>
		<p class="ayudaHtmlTexto">El sistema comienza a generar el reporte a exel y le saldr&aacute; en la pantalla el siguiente mensaje con un link para saber como va el proceso de generaci&oacute;n del reporte. </p>
		</td>
		</tr>
		<tr>
		<td  class="ayudaHtmlTexto"><div align="center"><IMG src="<%=BASEIMG%>MensajeInicioProceso.JPG" border=0 ></div></td>
		</tr>
		
		<tr>
		<td  class="ayudaHtmlTexto"><p class="ayudaHtmlTexto">&nbsp;</p>
		<p class="ayudaHtmlTexto">Para descargar el reporte, se debe posicionar en la pantalla principal y presiona el boton que indica la siguiente imagen. </p>
		</td>
		</tr>
		<tr>
		<td  class="ayudaHtmlTexto"><div align="center"><IMG src="<%=BASEIMG%>PantallaPrincipal.JPG" border=0 ></div></td>
		</tr>
		<tr>
		<td  class="ayudaHtmlTexto"><p class="ayudaHtmlTexto">&nbsp;</p>
		<p class="ayudaHtmlTexto">Cuando ha descargado el archivo aparecera el reporte como se muestra en la siguiente imagen donde obtendremos información detallada del reporte entre el rango de fechas escogido. </p>
		</td>
		</tr>
		<tr>
		<td  class="ayudaHtmlTexto"><div align="center"><IMG src="<%=BASEIMG%>ImagenExelFormatoCumplimiento.JPG" border=0 ></div></td>
		</tr>		

      </table></td>
    </tr>
  </table>
  <p></p>
<center>
<img src = "<%=BASEURL%>/images/botones/salir.gif" style = "cursor:hand" name = "imgsalir" onClick = "parent.close();" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);">
</center>
</BODY>
</HTML>
