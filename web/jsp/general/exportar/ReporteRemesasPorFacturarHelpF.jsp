<!--  
	 - Author(s)       :      LREALES
	 - Description     :      AYUDA FUNCIONAL - Reporte de Remesas Por Facturar
	 - Date            :      18/07/2006 
	 - Copyright Notice:      TRANSPORTES SANCHEZ POLO S.A.
-->
<%@ include file="/WEB-INF/InitModel.jsp"%>
<HTML>
<HEAD>
<TITLE>AYUDA FUNCIONAL - Reporte de Remesas Por Facturar</TITLE>
<META http-equiv=Content-Type content="text/html; charset=windows-1252">
<link href="../css/estilostsp.css" rel="stylesheet" type="text/css"> 
<link href="<%=BASEURL%>/css/estilostsp.css" rel="stylesheet" type="text/css">
<script type='text/javascript' src="<%= BASEURL %>/js/boton.js"></script> 
</HEAD>
<BODY> 
<% String BASEIMG = BASEURL +"/images/ayuda/reporte/RemesasPorFacturar/"; %>
  <table width="95%"  border="2" align="center">
    <tr>
      <td height="117" >
        <table width="100%" border="0" align="center">
          <tr  class="subtitulo">
            <td height="20"><div align="center">MANUAL DE REPORTE WEB</div></td>
          </tr>
          <tr class="subtitulo1">
            <td>Descripci&oacute;n del funcionamiento del programa para exportar un Reporte de Remesas Por Facturar.</td>
          </tr>
          <tr>
            <td  class="ayudaHtmlTexto"><div align="center">
              <p>&nbsp;</p>
              </div></td>
          </tr>
          <tr>
            <td  class="ayudaHtmlTexto"><p>En la siguiente pantalla escojo los filtros por los cuales deseo realizar el reporte. </p>            </td>
          </tr>
          <tr>
            <td  class="ayudaHtmlTexto"><div align="center"><IMG src="<%=BASEIMG%>Dibujo1.PNG" border=0 v:shapes="_x0000_i1143"></div></td>
          </tr>
          <tr>
            <td  class="ayudaHtmlTexto"><p>&nbsp;</p>
            <p>Si le sale el siguiente mensaje de error, es por que no ha definido las fechas con las cuales desea generar el reporte  por per&iacute;odo.</p></td>
          </tr>
          <tr>
            <td  class="ayudaHtmlTexto"><div align="center"><IMG src="<%=BASEIMG%>Dibujo2.PNG" border=0 v:shapes="_x0000_i1143"></div></td>
          </tr>
          <tr>
            <td  class="ayudaHtmlTexto"><p>&nbsp;</p>
            <p>Si en la pantalla, le sale el siguiente mensaje, es por que no ha definido la fecha final para generar el reporte  por per&iacute;odo. </p></td>
          </tr>
          <tr>
            <td  class="ayudaHtmlTexto"><div align="center"><IMG src="<%=BASEIMG%>Dibujo3.PNG" border=0 v:shapes="_x0000_i1143"></div></td>
          </tr>
          <tr>
            <td  class="ayudaHtmlTexto"><p>&nbsp;</p>
            <p>Si  le sale el siguiente mensaje de error, es por que no ha definido la fecha inicial para generar el reporte  por per&iacute;odo. </p></td>
          </tr>
          <tr>
            <td  class="ayudaHtmlTexto"><div align="center"><IMG src="<%=BASEIMG%>Dibujo4.PNG" border=0 v:shapes="_x0000_i1143"></div></td>
          </tr>
          <tr>
            <td  class="ayudaHtmlTexto"><p>&nbsp;</p>
            <p>Si el programa le arroja el siguiente mensaje, es por que la fecha final que se definio para el reporte   por per&iacute;odo es menor que la fecha inicial. </p></td>
          </tr>
          <tr>
            <td  class="ayudaHtmlTexto"><div align="center"><IMG src="<%=BASEIMG%>Dibujo5.PNG" border=0 v:shapes="_x0000_i1143"></div></td>
          </tr>
          <tr>
            <td  class="ayudaHtmlTexto"><p>&nbsp;</p>
            <p>Luego de realizar el procedimiento correctamente, le saldr&aacute; el siguiente mensaje confirmando que el proceso de exportaci&oacute;n ha sido iniciado exitosamente. </p></td>
          </tr>
          <tr>
            <td  class="ayudaHtmlTexto"><div align="center"><IMG height=78 src="<%=BASEIMG%>image_002.JPG" width=418 border=0 v:shapes="_x0000_i1054"></div></td>
          </tr>
		  <tr>
  <td  class="ayudaHtmlTexto"><p>&nbsp;</p>
    <p>Luego si desea observar, el archivo excel generado.. debe ir a la pantalla inicial del sitio, y escoger en la parte superior derecha, el icono que se se&ntilde;ala acontinuaci&oacute;n. </p></td>
</tr>
<tr>
            <td  class="ayudaHtmlTexto"><div align="center"><IMG height=555 src="<%=BASEIMG%>image_003.JPG" width=811 border=0 v:shapes="_x0000_i1054"></div></td>
          </tr>
		  <tr>
  <td  class="ayudaHtmlTexto"><p>&nbsp;</p>
    <p>Despues en la pantalla le debe aparecer una lista con los archivos excel que usted posee, alli podra buscar y escoger el anterior reporte generado. </p></td>
</tr>
<tr>
            <td  class="ayudaHtmlTexto"><div align="center"><IMG height=434 src="<%=BASEIMG%>image_004.JPG" width=700 border=0 v:shapes="_x0000_i1054"></div></td>
          </tr>
		  <tr>
  <td  class="ayudaHtmlTexto"><p>&nbsp;</p>
    <p>Luego de haber escogido el reporte, ya podra observarlo como se muestra acontinuaci&oacute;n.</p></td>
</tr>
<tr>
            <td  class="ayudaHtmlTexto"><div align="center"><IMG height=718 src="<%=BASEIMG%>image_005.JPG" width=1024 border=0 v:shapes="_x0000_i1054"></div></td>
          </tr>
      </table></td>
    </tr>
  </table>
  <p></p>
<center>
<img src = "<%=BASEURL%>/images/botones/salir.gif" style = "cursor:hand" name = "imgsalir" onClick = "parent.close();" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);">
</center>
</BODY>
</HTML>
