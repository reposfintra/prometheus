<%@ include file="/WEB-INF/InitModel.jsp"%>
<html>
<head>
  <title>Transferencia a Bancos- Ayuda</title>
  <META http-equiv=Content-Type content="text/html; charset=windows-1252">
  <link href="<%=BASEURL%>/css/estilostsp.css" rel="stylesheet" type="text/css"> 
  <script type='text/javascript' src="<%= BASEURL %>/js/boton.js"></script>
</head>
<body>
<center>



<% String BASEIMG = BASEURL +"/images/ayuda/cxpagar/transferenciaMims/"; %>

<table width="100%"  border="2" align="center">
    <tr>
      <td >
 
            <table width='99%' align="center" cellpadding='0' cellspacing='0'>
                 <tr class="subtitulo" ><td height="20"><div align="center">MANUAL DE TRANSFERENCIA A BANCOS </div></td></tr>
                 <tr class="subtitulo1"> <td>Descripci&oacute;n del funcionamiento </td> </tr>

                 
                 <tr>
                      <td  class="ayudaHtmlTexto" height='40'>
                         Este programa permite generar un archivo de migraci�n para bancos. Presenta un formulario como lo indica la
                         figura 1, en dode se establece el banco, la sucursal y el n�mero de la corrida a la cual desea generarle el
                         archivo.
                         La busqueda de los registros perteneciente a dicha corrida se realiza en MIMS, y solo muestra aquellos cuyos
                         propietarios tengan datos de  transferencia.
                      </td>
                 </tr>
                 
                 <tr><td  align="center" > 
                     <br><br>
                     <img  src="<%= BASEIMG%>Busqueda.JPG" >                  
                     <br>
                     <strong>Figura 1</strong>
                     <br><br>
                     <br><br>
                 </td></tr>
                 
                 <tr>
                      <td  class="ayudaHtmlTexto" height='40'>
                         Una vez digitados los datos requeridos en el formulario, deber� realizar click en el bot�n "Aceptar", el programa
                         si encuentra registro, mostrar� una vista previa de los datos a migrar, en caso contrario, aparecer� un mensaje 
                         indicando que no se encontraron registros.
                         <br><br><br><br>
                      </td>
                 </tr>
                 
                 
                  <tr>
                      <td  class="ayudaHtmlTexto" height='40'>
                         El archivo de migraci�n, deber� tener la estructura descrita en el formato enviado por el banco en el numeral 2.
                         Para ver el archivo deber� dar click 
                         <a href='#' onclick=" var win = window.open('<%= BASEIMG%>BancocreditoTEF.doc','planViaje',' top=100,left=100, width=700, height=600, scrollbars=yes, status=yes, resizable=yes  ');">aqui</a>
                         <br><br><br><br>
                      </td>
                 </tr>
                 
            </table>
            
      </td>
  </tr>
</table>
<br><br>
<img src="<%=BASEURL%>/images/botones/salir.gif"      name="exit"    height="21"  title='Salir'                onClick="parent.close();"                 onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" style="cursor:hand">          




</body>
</html>
