<!--
- Nombre P�gina :                  ReporteRemesasPorFacturar.jsp                 
- Descripci�n :                    Pagina JSP, que realiza permite generar el 
                                   reporte a excel de las remesas por facturar.
- Autor :                          LREALES                        
- Fecha Creado :                   17 de Julio de 2006, 8:41 P.M.                        
- Versi�n :                        1.0                                       
- Copyright :                      Fintravalores S.A.                   
-->

<%-- Declaracion de librerias--%>
<%@page contentType="text/html"%>
<%@ page session="true"%>
<%//@ page errorPage="/error/error.jsp"%>
<%@page import="com.tsp.operation.model.beans.*, java.util.*"%>
<%@ include file="/WEB-INF/InitModel.jsp"%>
<%@page import="com.tsp.operation.model.*"%>
<%@page import="com.tsp.util.*"%>
<%@ taglib uri="http://jakarta.apache.org/taglibs/input-0.90" prefix="input" %>
<%
  String path = application.getRealPath(model.menuService.getRealUrl(request.getRequestURI()));
  String datos[] = model.menuService.getContenidoMenu(BASEURL,request.getRequestURI(),path);
  
  String msg = request.getParameter("msg") != null?request.getParameter ("msg"):"";
  
  String fini = request.getParameter ("fecini") != null?request.getParameter ("fecini").toUpperCase():"";
  String ffin = request.getParameter ("fecfin") != null?request.getParameter ("fecfin").toUpperCase():"";
  String aged = request.getParameter ("agencia_duenia") != null?request.getParameter ("agencia_duenia").toUpperCase():"NADA";        
  String agef = request.getParameter ("agencia_facturadora") != null?request.getParameter ("agencia_facturadora").toUpperCase():"NADA";
		
  if ( msg.equals("Proceso iniciado exitosamente!") ) {
  	fini = "";
	ffin = "";
	aged = "NADA";
	agef = "NADA";
  }
%>
<html>
<head><title>Reporte De Remesas Por Facturar</title>
<link href="<%= BASEURL %>/css/estilostsp.css" rel="stylesheet" type="text/css">
<link href="/css/estilostsp.css" rel="stylesheet" type="text/css">
<script src='<%=BASEURL%>/js/date-picker.js'></script>
<script type='text/javascript' src="<%= BASEURL %>/js/boton.js"></script>
<script type='text/javascript' src="<%= BASEURL %>/js/validar.js"></script>
</head>
<body onLoad="redimensionar();" onResize="redimensionar();">
<div id="capaSuperior" style="position:absolute; width:100%; height:100px; z-index:0; left: 0px; top: 0px;">
<jsp:include page="/WEB-INF/toptsp.jsp?encabezado=Remesas Por Facturar"/>
</div>

<div id="capaCentral" style="position:absolute; width:100%; height:85%; z-index:0; left: 0px; top: 100px; overflow: scroll;"> 
<%
    Usuario usuario = (Usuario) session.getAttribute("Usuario");  	
%>
<script>
	var controlador ="<%=CONTROLLER%>";
	
	function Exportar (){
		
		document.formulario.action = controlador + "?estado=ReporteRemesas&accion=PorFacturar";
		document.formulario.submit();
					
	}
</script>

<form name="formulario" method="post">
    <table width="60%" border="2" align="center">
      <tr>
        <td><table width="100%" align="center"  class="tablaInferior">
            <tr>
				<td colspan="3" class="subtitulo1">Reporte De Remesas Por Facturar</td>
				<td width="45%" colspan="2" class="barratitulo"><img src="<%=BASEURL%>/images/titulo.gif" width="32" height="20" align="left"><%=datos[0]%></td>
            </tr>
            <tr class="fila">
              <td width="30%" align="left" valign="middle"><input name="anual" type="radio" value="NO" checked>
                Per&iacute;odo</td>
              <td width="30%" align="left" valign="middle">&nbsp;Fecha Inicial</td>
              <td colspan="3" align="left" valign="middle">
			  			<input name="fecini" type="text" class="textbox" id="fecini" size="10" value="<%=fini%>" readonly>
              			<span class="Letras"> 
						<img src="<%=BASEURL%>/images/cal.gif" width="16" height="16" align="absmiddle" style="cursor:hand " onclick="if(self.gfPop)gfPop.fPopCalendar(document.formulario.fecini);return false;" HIDEFOCUS>
						</span>
			  </td>
            </tr>
            <tr class="fila">
              <td align="left" valign="middle">&nbsp;</td>
              <td align="left" valign="middle">&nbsp;Fecha Final</td>
              <td colspan="3" align="left" valign="middle">
			  			<input name="fecfin" type="text" class="textbox" id="fecfin" size="10" value="<%=ffin%>" readonly>
              			<span class="Letras">
						<a href="javascript:void(0)" onclick="jscript: show_calendar('fecfin');" HIDEFOCUS> </a>
						<img src="<%=BASEURL%>/images/cal.gif" width="16" height="16" align="absmiddle" style="cursor:hand " onclick="if(self.gfPop)gfPop.fPopCalendar(document.formulario.fecfin);return false;" HIDEFOCUS>
						</span>
			  </td>
            </tr>
			<% TreeMap agencia = model.agenciaService.getCbxAgencia(); %>
            <tr class="fila">
              <td align="left" valign="middle"><input name="anual" type="radio" value="OK">
                Anual</td>
              <td align="left" valign="middle">A&ntilde;o</td>
              <td colspan="3" align="left" valign="middle"><select name="anio" style='width:80%;' class='textbox' ><%= model.remesasPorFacturarService.getComboAnual() %></select></td>
            </tr>
            <tr class="fila">
              <td align="left" valign="middle">Agencia Due&ntilde;a</td>
              <td colspan="4" align="left" valign="middle">
			    <input:select name="agencia_duenia" attributesText="style='width:80%;' class='textbox'" options="<%=agencia%>" default="<%=aged%>"/>
			  	<script>formulario.agencia_duenia.value = '<%=aged%>'</script>			  </td>
            </tr>
            <tr class="fila">
              <td align="left" valign="middle">Agencia Facturadora</td>
			  
              <td colspan="4" align="left" valign="middle">
			    <input:select name="agencia_facturadora" attributesText="style='width:80%;' class='textbox'" options="<%=agencia%>" default="<%=agef%>"/>
			    <script>formulario.agencia_facturadora.value = '<%=agef%>'</script>			  </td>
            </tr>		
        </table></td>
      </tr>
    </table>
  <p>
	<div align="center">
	  <p>
		<img src="<%=BASEURL%>/images/botones/exportarExcel.gif" style="cursor:hand" title="Generar Reporte en Excel" name="exportar" onClick="Exportar (); this.disabled=true;" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);">&nbsp;   
		<img src="<%=BASEURL%>/images/botones/salir.gif"  name="salir" style="cursor:hand" title="Salir al Menu Principal" onMouseOver="botonOver(this);" onClick="parent.close();" onMouseOut="botonOut(this);" >
	  </p>
	  <%if ( msg!=null && !msg.equals("") ){%>
							
		  <table border="2" align="center">
			<tr>
				<td>
					<table width="100%"  border="1" align="center"  bordercolor="#F7F5F4" bgcolor="#FFFFFF">
						<tr>
							<td width="229" align="center" class="mensajes"><%=msg%></td>
							<td width="29" background="<%=BASEURL%>/images/cuadronaranja.JPG">&nbsp;</td>
							<td width="58">&nbsp;</td>
						</tr>
					</table>
				</td>
			</tr>
		  </table>
		<%}%>
	  </p>
	</div>
  </p>
</form>
</div>
<iframe width=188 height=166 name="gToday:normal:agenda.js:gfPop:plugins.js" id="gToday:normal:agenda.js:gfPop:plugins.js" src="<%=BASEURL%>/js/Calendario/ipopeng.htm" scrolling="no" frameborder="0" style="visibility:visible; z-index:999; position:absolute; left:-500px; top:0px;"> </iframe>  
<%=datos[1]%>
</body>
</html>