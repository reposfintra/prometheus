<!--
- Autor : Ing. Jose de la rosa
- Date  : 15 de agosto de 2006, 08:24 AM
- Copyrigth Notice : Fintravalores S.A. S.A
-->
<%--
-@(#)
--Descripcion : Pagina JSP, que maneja el reporte del control de vacios
--%>

<%-- Declaracion de librerias--%>
<%@page contentType="text/html"%>
<%@page session="true"%>
<%@page import="java.util.*"%>
<%@page import="com.tsp.operation.model.*"%>
<%@page import="com.tsp.util.*"%>
<%@page import="com.tsp.operation.model.beans.*"%>
<%@include file="/WEB-INF/InitModel.jsp"%>
<%@taglib uri="/WEB-INF/tlds/tagsAgencia.tld" prefix="ag"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://jakarta.apache.org/taglibs/input-0.90" prefix="input" %>
<html>
<head>
<title>Reporte Analisis de Flota</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link href="<%= BASEURL %>/css/estilostsp.css" rel="stylesheet" type="text/css"> 
<link href="../../../css/estilostsp.css" rel="stylesheet" type="text/css"> 
<script src='<%=BASEURL%>/js/date-picker.js'></script>
<script type='text/javascript' src="<%= BASEURL %>/js/validar.js"></script>
<script type='text/javascript' src="<%= BASEURL %>/js/boton.js"></script>
<script type="text/javascript" src="<%=BASEURL%>/js/script.js"></script>  
<script type='text/javascript' src="<%= BASEURL %>/js/ingreso.js"></script>
</head>
<%	String  hoy       = Utility.getHoy("-");  
	String msg = (String) request.getParameter("msg")!=null?request.getParameter("msg"):"";
	String path = application.getRealPath(model.menuService.getRealUrl(request.getRequestURI()));
	String datos[] = model.menuService.getContenidoMenu(BASEURL,request.getRequestURI(),path);
%>
<%-- Inicio Body --%>
<body onLoad="redimensionar();" onResize="redimensionar();">
<div id="capaSuperior" style="position:absolute; width:100%; height:100px; z-index:0; left: 0px; top: 0px;">
	<jsp:include page="/WEB-INF/toptsp.jsp?encabezado=Reporte de Analisis de Flota"/>
</div>	
<div id="capaCentral" style="position:absolute; width:100%; height:85%; z-index:0; left: 0px; top: 100px; overflow: scroll;">
<form name="form1" method="post" action="<%=CONTROLLER%>?estado=ReporteUtilizacion&accion=Placas">
<table width="400" border="2" align="center">
	<tr>
		<td>  
			<table width='100%' align='center' class='tablaInferior'>           
				<tr >
					<td colspan='2' >
						<table cellpadding='0' cellspacing='0' width='100%' class="barratitulo">
                     		<tr>
                      			<td width='55%' class="subtitulo1">Reporte de Analisis de Flota </td>
                      			<td width='45%'  ><img src="<%=BASEURL%>/images/titulo.gif" width="32" height="20" align="left"><%=datos[0]%></td>
                    		</tr>
                   		</table>
                	</td>
             	</tr>              
              	<tr class="fila">
					<td class='letrafila' width='123'> Fecha Inicial</td>
					<td width='253'>
						<!-- Fecha Inicial -->
						<input  name='fechaInicio' class="textbox"  value='<%=hoy%>' size="12" maxlength="12" readonly="true">
						&nbsp; 
						<a href="javascript:void(0)" onClick="if(self.gfPop)gfPop.fPopCalendar(fechaInicio);return false;" hidefocus>
							<img name="popcal" align="absmiddle" src="<%=BASEURL%>/js/calendartsp/calbtn.gif" width="16" height="16" border="0" alt="" title="Haga clic aqui para seleccionar la Fecha Inicial del proceso">
						</a>
					</td>
			  </tr>
				 <tr class="fila">
					<td class='letrafila'>Fecha Final </td>
					<td> 
                    	<!-- Fecha Final -->                   
                     	<input  name='fechaFinal' class="textbox"  value='<%=hoy%>' size="12" maxlength="12" readonly="true">
                     	&nbsp;
                     	<a href="javascript:void(0)" onClick="if(self.gfPop)gfPop.fPopCalendar(fechaFinal);return false;" hidefocus>
                      		<img name="popcal" align="absmiddle" src="<%=BASEURL%>/js/calendartsp/calbtn.gif" width="16" height="16" border="0" alt="" title="Haga clic aqui para seleccionar la Fecha Final del proceso">
                     	</a>     
                	</td>
             	</tr>
              	<tr class="fila">
              	  <td>Agencia</td>
           	      <td><% TreeMap agencia = model.agenciaService.listar(); 
			     agencia.put("TODAS", "");%>
                    <input:select  name="agencia" attributesText="class=textbox" options="<%=agencia%>" default=""/><img  src="<%=BASEURL%>/images/botones/iconos/obligatorio.gif" width="10" height="10"></td>
           	  </tr>				
              	<tr class="letra">
                	<td colspan="2" class='informacion'>* Haga click en el boton EXPORTAR para generar el reporte</td>
           	  </tr>
       	  </table>
      	</td>
   	</tr>
</table>
<br>
<center>	
	<img src="<%=BASEURL%>/images/botones/exportarExcel.gif" name="mod"  height="21" onClick="validarBusqueda();" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" style="cursor:hand" title="Haga clic aqui para iniciar el proceso de generacion del reporte">
	<img src="<%=BASEURL%>/images/botones/salir.gif"  name="imgsalir"  height="21" onMouseOver="botonOver(this);" onClick="parent.close();" onMouseOut="botonOut(this);" style="cursor:hand" title="Haga clic aqui para salir de la pagina">
</center>
</form>
<% if(!msg.equals("")){%>
<br>
<table width="550" border="2" align="center">
<tr>
	<td width="707">
		<table width="100%"  border="1" align="center"  bordercolor="#F7F5F4" bgcolor="#FFFFFF">
			<tr>
				<td width="50%" nowrap  class="mensajes">
					<%=msg%>
					<%if(request.getParameter("ruta")!=null){%><br>
					&nbsp;&nbsp;<a href="#" class="Simulacion_Hiper" onClick="window.open('<%= CONTROLLER %>?estado=Log&accion=Proceso&Estado=TODO','log','scroll=no, resizable=yes, width=800, height=600')" style="cursor:hand "><%=request.getParameter("ruta")%><%}%></a>
				</td>
				<td width="16%" background="<%=BASEURL%>/images/cuadronaranja.JPG">&nbsp;</td>
				<td width="34%">&nbsp;</td>
			</tr>
		</table>
	</td>
</tr>
</table>
<%}%>
</div>
<%=datos[1]%>
<!-- calendario-->
<iframe width="188" height="166" name="gToday:normal:<%=BASEURL%>/js/calendartsp/agenda.js:gfPop:<%=BASEURL%>/js/calendartsp/plugins.js" id="gToday:normal:<%=BASEURL%>/js/calendartsp/agenda.js:gfPop:<%=BASEURL%>/js/calendartsp/plugins.js" src="<%=BASEURL%>/js/calendartsp/ipopeng.htm" scrolling="no" frameborder="0" style="visibility:visible; z-index:999; position:absolute; left:-500px; top:0px;"></iframe>
</body>
</html>
<script>
	function validarBusqueda(){
		if( form1.fechaInicio.value > form1.fechaFinal.value ){
				alert( "La fecha inicial debe ser menor a la fecha final" );                            
				return false;
		}
		else if( form1.fechaInicio.value == "" ){
				alert( "Debe seleccionar la fecha inicial" );                          
				return false;
		}		
		else if( form1.fechaFinal.value == "" ){
				alert( "Debe seleccionar la fecha final" );                          
				return false;
		}			
		else{
			form1.submit();
		}
	}
</script>