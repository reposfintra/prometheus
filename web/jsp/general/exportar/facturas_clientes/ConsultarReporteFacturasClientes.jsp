<!--
- Autor : LREALES
- Date  : 29 de noviembre de 2006
- Copyrigth Notice : Fintravalores S.A. S.A
-->
<%--
-@(#)
--Descripcion : Pagina JSP, que se encarga de realizar la busqueda para el reporte de facturas de un cliente
--%>
<%--
The taglib directive below imports the JSTL library. If you uncomment it,
you must also add the JSTL library to the project. The Add Library... action
on Libraries node in Projects view can be used to add the JSTL 1.1 library.
--%>
<%-- Declaracion de librerias--%>
<%@page contentType="text/html"%>
<%@page errorPage="/error/ErrorPage.jsp"%>
<%@page import="com.tsp.operation.model.beans.*, java.util.*"%>
<%@taglib uri="/WEB-INF/tlds/tagsAgencia.tld" prefix="ag"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://jakarta.apache.org/taglibs/input-0.90" prefix="input" %>
<%@ include file="/WEB-INF/InitModel.jsp"%>
<%boolean bandera = false; %>
<%
  String path = application.getRealPath(model.menuService.getRealUrl(request.getRequestURI()));
  String datos[] = model.menuService.getContenidoMenu(BASEURL,request.getRequestURI(),path);
 // TreeMap pro= model.Negociossvc.getProv();
  //pro.put(" Seleccione","..."); 
%>
<html>
    <head>

        <title>Consultar Reporte de Facturas de un Cliente</title>        
		<script language="JavaScript" type="text/javascript" src="<%=BASEURL%>/js/boton.js"></script>
		<script src='<%=BASEURL%>/js/tools.js' language='javascript'></script>
		<script type='text/javascript' src="<%=BASEURL%>/js/validar.js"></script> 
                
                <script type="text/javascript" src="<%=BASEURL%>/js/jCal/jscal2.js"></script>
                <script type="text/javascript" src="<%=BASEURL%>/js/jCal/lang/es.js"></script>
                <link rel="stylesheet" type="text/css" href="<%=BASEURL%>/css/jCal/jscal2.css" />
                <link href="../css/estilostsp.css" rel="stylesheet" type="text/css">
		<script>
			var valorRadio = "";
		
			function setRadio( value ){
			
				valorRadio = value;	
				
			}
			
			function validar( form ){
			
				if( valorRadio == "" ){
					alert( 'Defina un parametro de busqueda para poder continuar...' );
					return false;
				}
				
				if ( valorRadio == "1" ) {
				
					if( form.cod_cli.value == "" ){
						alert( 'Debe escribir o escoger el codigo del cliente para poder continuar...' );
						return false;
					}
					
					if ( form.fecini.value == "" ) {
						form.fecini.value = "2000-01-01";
						/*alert( 'Defina la fecha de inicio de busqueda para poder continuar...' );
						return false;*/
					}
					
					if ( form.fecfin.value == "" ) {
                                                var f = new Date();
						form.fecfin.value = f.getFullYear() + "-"  + (f.getMonth() +1) + "-" + f.getDate();
						/*alert( 'Defina la fecha de fin de busqueda para poder continuar...' );
						return false;*/
					}
					
					if ( form.fecini.value != "" && form.fecfin.value != "" ) {
						var fecha1 = form.fecini.value.replace( /-|:| /gi, '' );
						var fecha2 = form.fecfin.value.replace( /-|:| /gi, '' );
						 
						var fech1 = parseFloat( fecha1 );
						var fech2 = parseFloat( fecha2 );
						 
						if( fecha2 < fecha1 ) { 				   
							 alert( 'La fecha final debe ser mayor que la fecha inicial!' );
							 return false;					 
						}
					}
				
				}
				
				if ( valorRadio == "2" ) {
				
					if ( form.documento.value == "" ) {
						alert( 'Defina el numero de la factura para poder continuar...' );
						return false;
					}
					
				}
				
				if ( valorRadio == "3" ) {
				
					if ( form.remesa.value == "" ) {
						alert( 'Defina el numero de la remesa para poder continuar...' );
						return false;
					}
					
				}
				
				if ( valorRadio == "4" ) {
					if ( form.fec_creacion.value == "" && form.fec_creacion.value == "" ) {
						alert( 'Defina el usuario de creacion o la fecha de creacion para poder continuar...' );
						return false;
					}					
					
				}
				
				if ( valorRadio == "5" ) {
					if ( form.text.value == "" && form.clientes.value == "..." ) {
						alert( 'Seleccione el Afiliado para poder continuar..' );
						return false;
					}					
					
				}
				if ( valorRadio == "6" ) {
					if ( form.fec_venc.value == "" ) {
						alert( 'Defina la fecha de vencimiento para poder continuar...' );
						return false;
					}					
					
				}
				
				return true;
				
			}				
			
			function abrirVentanaBusq( an, al, url, pag ) {
			
        		parent.open( url, 'Conductor', 'width=' + an + ', height=' + al + ',scrollbars=no,resizable=no,top=10,left=65,status=yes' );
				
    		}
	
		</script>
    </head>
   <body onResize="redimensionar()" onLoad="redimensionar()">
<div id="capaSuperior" style="position:absolute; width:100%; height:100px; z-index:0; left: 1px; top: 0px;">
 <jsp:include page="/toptsp.jsp?encabezado=Reporte Facturas Cliente"/>
</div>
<div id="capaCentral" style="position:absolute; width:100%; height:87%; z-index:0; left: 0px; top: 100px; overflow: scroll;">

        <form name='formulario' method='POST' id="formulario" action="<%=CONTROLLER%>?estado=Reporte&accion=FacturasClientes&hacer=1" onSubmit="return validar(this);">
            <table width="80%"  border="2" align="center">
              <tr>
                <td><table width="100%"  border="0" class="tablaInferior">
                  
				  <tr>
                   <td class="subtitulo1" colspan="4">Reporte de Facturas de Clientes</td>
          			<td colspan="3" class="barratitulo"><img src="<%=BASEURL%>/images/titulo.gif" width="32" height="20" align="left"><%=datos[0]%></td>
                  </tr>
                  
                  <tr class="fila">
                    <td width="5%" align="left">&nbsp;<input name="opcion" id="opcion" type="radio" value="1" onClick="setRadio(this.value);"></td>
                    <td width="20%" align="left">&nbsp;C&oacute;digo del Cliente</td>
                    <td width="15%" align="left">
					
                      <input name="cod_cli" id="cod_cli" type="text" class="textbox" size="10" maxlength="10">
					  <img src='<%=BASEURL%>/images/botones/iconos/lupa.gif' width="15" height="15" onClick="javascript:abrirVentanaBusq(850,550,'<%=BASEURL%>/jsp/general/exportar/demoras/BuscarCliente.jsp')" title="Buscar" style="cursor:hand" >					
					  
					</td>
                    <td width="15%" align="left">&nbsp;Fecha Inicial</td>
                    <td width="15%" align="left">
					
				  		<input name="fecini" type="text" class="textbox" id="fecini" size="10" readonly>
                                                <span class="Letras">
						<img src="<%=BASEURL%>/images/cal.gif" id="iFecini" alt="fecha" title="fecha"/>
                                                <script type="text/javascript">
                                                         Calendar.setup({
                                                             inputField: "fecini",
                                                             trigger: "iFecini",
                                                             align: "top",
                                                             onSelect: function() {
                                                                 this.hide();
                                                             }
                                                         });
                                                </script>   </span>
					</td>
                    <td width="15%" align="left">&nbsp;Fecha Final</td>
                    <td width="15%" align="left">
					
              			<input name="fecfin" type="text" class="textbox" id="fecfin" size="10" readonly>
              			<span class="Letras">
						<img src="<%=BASEURL%>/images/cal.gif" id="iFecfin" alt="fecha" title="fecha"/>
                                                <script type="text/javascript">
                                                         Calendar.setup({
                                                             inputField: "fecfin",
                                                             trigger: "iFecfin",
                                                             align: "top",
                                                             onSelect: function() {
                                                                 this.hide();
                                                             }
                                                         });
                                                </script>   </span>
						
					</td>
                  </tr>
				  

				  <tr class="fila">
                    <td align="left">&nbsp;<input name="opcion" id="opcion" type="radio" value="2" onClick="setRadio(this.value);"></td>
                    <td colspan="2" align="left">&nbsp;N&uacute;mero de la Factura</td>
                    <td colspan="4" align="left">
					
                      <input name="documento" id="documento" type="text" class="textbox" size="10" maxlength="10">  					
					  
					</td>
                  </tr>  
				  
		<tr class="fila">
                    <td align="left">&nbsp;<input name="opcion" id="opcion" type="radio" value="3" onClick="setRadio(this.value);"></td>
                    <td colspan="2" align="left">&nbsp;N&uacute;mero de la Remesa</td>
                    <td colspan="4" align="left"><input name="remesa" id="remesa" type="text" class="textbox" size="10" maxlength="10"></td>
                  </tr> 
                  
                  <tr class="fila">
                    <td align="left">&nbsp;<input name="opcion" id="opcion" type="radio" value="4" onClick="setRadio(this.value);"></td>
                    <td colspan="2" align="left">&nbsp;Usuario Creacion</td>
                    <td colspan="2" align="left"><input name="usu_creacion" id="usu_creacion" type="text" class="textbox" size="10" maxlength="10"></td>
                    <td colspan="1" align="left">&nbsp;Fecha Creacion</td>
                    <td colspan="1" align="left">
                                <input name="fec_creacion" type="text" class="textbox" id="fec_creacion" size="10" readonly>
              			<span class="Letras">
                                <img src="<%=BASEURL%>/images/cal.gif" id="iFeccrea" alt="fecha" title="fecha"/>
                                <script type="text/javascript">
                                         Calendar.setup({
                                             inputField: "fec_creacion",
                                             trigger: "iFeccrea",
                                             align: "top",
                                             onSelect: function() {
                                                 this.hide();
                                             }
                                         });
                                </script>  </span>
                    </td>
                  </tr> 
				  	
				  <tr class="fila">
                    <td align="left">&nbsp;<input name="opcion" id="opcion" type="radio" value="6" onClick="setRadio(this.value);"></td>
                    <td colspan="2" align="left">&nbsp;Fecha de Vencimiento</td>
                    <td colspan="2" align="left">Fecha
                    
                                <input name="fec_venc" type="text" class="textbox" id="fec_venc" size="10" readonly>
              			<span class="Letras">
                                <img src="<%=BASEURL%>/images/cal.gif" id="iFecven" alt="fecha" title="fecha"/>
                                <script type="text/javascript">
                                         Calendar.setup({
                                             inputField: "fec_venc",
                                             trigger: "iFecven",
                                             align: "top",
                                             onSelect: function() {
                                                 this.hide();
                                             }
                                         });
                                </script>  </span>
                    </td>
					<td colspan="2" align="left">Estado
					<select name="estate1" id="estate">
						  	<option value="T" selected>Todas</option>
							<option value="P">Pendientes</option>
							<option value="PG">Pagadas</option>
							<option value="A">Anuladas</option>
                          </select></td>
                  </tr>  
				  
				  <tr class="fila">
                    <td align="left">&nbsp;<input name="opcion" id="opcion" type="radio" value="8" onClick="setRadio(this.value);"></td>
                    <td colspan="2" align="left">&nbsp;Multiservicio</td>
                    <td colspan="2" align="left"><input name="multi" id="multi" type="text" class="textbox" size="15" maxlength="10"></td>
                    <td colspan="1" align="left">&nbsp;</td>
                    <td colspan="1" align="left">
                                
              			
                    </td>
                  </tr> 
					                 
                </table></td>
              </tr>
            </table>
			<br>
            <table align="center">
              <tr>
                <td colspan="2" nowrap align="center">
<input name="Guardar" src="<%=BASEURL%>/images/botones/aceptar.gif"  style = "cursor:hand" align="middle" type="image" id="Guardar"  onMouseOver="botonOver(this);" onMouseOut="botonOut(this);">                  
&nbsp;
                  <img src="<%=BASEURL%>/images/botones/salir.gif"  name="imgsalir" align="absmiddle" style="cursor:hand" onClick="parent.close();" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);"></td>
              </tr>
            </table>
        </form > 
	</div>  
	
    <%=datos[1]%>
	</body>
</html>