<%@page session="true"%>
<%@page import="java.util.*" %>
<%@page errorPage="/error/ErrorPage.jsp"%>
<%@page import="com.tsp.operation.model.*"%>
<%@include file="/WEB-INF/InitModel.jsp"%>
<%@page import="com.tsp.operation.model.beans.*"%>
<%@page import="com.tsp.util.*"%>
<%@ taglib uri="http://jakarta.apache.org/taglibs/input-0.90" prefix="input" %>
<html>
<head>
<title>Factura Detalle</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link href="<%= BASEURL %>/css/estilostsp.css" rel="stylesheet" type="text/css">
<link href="../css/estilostsp.css" rel="stylesheet" type="text/css">
 <script type='text/javascript' src="<%=BASEURL%>/js/boton.js"></script> 
 <script type='text/javascript' src="<%=BASEURL%>/js/consultadespacho.js"></script> 
 <script type='text/javascript' src="<%=BASEURL%>/js/general.js"></script> 
 <script type='text/javascript' src="<%=BASEURL%>/js/validar.js"></script> 
 <style type="text/css">
<!--
.Estilo1 {color: #D0E8E8}
-->
 </style>
</head> 
<%
	String path = application.getRealPath(model.menuService.getRealUrl(request.getRequestURI()));
    String datos[] = model.menuService.getContenidoMenu(BASEURL,request.getRequestURI(),path);
%>

<body onResize="redimensionar() onLoad="redimensionar()"">
<div id="capaSuperior" style="position:absolute; width:100%; height:100px; z-index:0; left: 0px; top: 0px;">
<jsp:include page="/WEB-INF/toptsp.jsp?encabezado=Factura Detalle"/>
</div> 

<div id="capaCentral" style="position:absolute; width:100%; height:85%; z-index:0; left: 0px; top: 100px; overflow: scroll;"> 
	<%String msg = request.getParameter("msg");
		if ( msg!=null && !msg.equals("") ){%>							
		  <table border="2" align="center">
			<tr>
				<td>
					<table width="100%"  border="1" align="center"  bordercolor="#F7F5F4" bgcolor="#FFFFFF">
						<tr>
							<td width="229" align="center" class="mensajes"><%=msg%></td>
							<td width="29" background="<%=BASEURL%>/images/cuadronaranja.JPG">&nbsp;</td>
							<td width="58">&nbsp;</td>
						</tr>
					</table>
				</td>
			</tr>
		  </table>		  
		        <br>
		        
 	  <input name="salir" src="<%=BASEURL%>/images/botones/salir.gif" type="image" onmouseover="botonOver(this);" onmouseout="botonOut(this);" style="cursor:hand" class="boton" onclick="window.close();">
	  <%}
		else {
          Vector otros_datos = model.reporteFacturasClientesService.getVectorFactura (); //Obtengo el vector con la info de la factura
					
			if ( otros_datos != null && otros_datos.size() > 0 ) {
				BeanGeneral info_especifica = ( BeanGeneral ) otros_datos.elementAt( 0 );
		%>		
<form name="form1" method="post" >
  <table width="1000" border="2" align="center">
    <tr>
      <td width="100%" height="60">
	  
        <table width="100%">
		
          <tr>
            <td width="50%" height="22" align="left" class="subtitulo1">&nbsp;Informaci&oacute;n de la Factura N&deg; <%=info_especifica.getValor_26()%>.</td>
            <td width="50%" align="left" class="barratitulo"><img src="<%=BASEURL%>/images/titulo.gif" width="32" height="20"align="left"><%=datos[0]%></td>
          </tr>
		  
        </table>
		
        <table width="100%">
		
          <tr>
            <td width="11%" class="fila">Cliente:</td>
            <td width="25%" class="letra"><%=info_especifica.getValor_02()%> ( <%=info_especifica.getValor_01()%> ). </td>
            <td width="14%" class="fila">Agencia Facturacion:</td>
            <td width="19%" class="letra"><%=info_especifica.getValor_03()%></td>
            <td width="12%" class="fila">Moneda:</td>
			<td width="19%" class="letra"><%=info_especifica.getValor_04()%></td>			 
          </tr>
		  
          <tr>
            <td class="fila">Nit:</td>
            <td class="letra"><%=info_especifica.getValor_05()%></td>
            <td class="fila">Fecha Factura:</td>
			<td class="letra"><%=info_especifica.getValor_06()%></td>
            <td class="fila">Tasa:</td>           
			<td class="letra"><%=Util.customFormat( Double.parseDouble( info_especifica.getValor_07() ) )%></td>
          </tr>
		 
          <tr>
            <td class="fila">Forma de Pago:</td>
            <td class="letra"><%=info_especifica.getValor_08()%></td>
            <td class="fila">Plazo:</td>
            <td class="letra"><%=info_especifica.getValor_09()%></td>
            <td class="fila">Valor Total:</td>
            <td class="letra"><%=Util.customFormat( Double.parseDouble( info_especifica.getValor_10() ) )%></td>
          </tr>
		  
      </table>
      </td>
    </tr>
  </table>
  
  <table width="1000" border="2" align="center">
    <tr>
      <td height="26"><table width="100%">
          <tr class="fila" id="cantidad">
            <td width="20%">Descripcion:</td>
            <td width="80%"  align="left" >
              <textarea name="desc" cols="100%" rows="2" onkeypress="soloAlfa(event)" readonly><%=info_especifica.getValor_11()%></textarea>
            </td>
          </tr>
          <tr class="fila" id="cantidad">
            <td height="3"  >Observacion:</td>
            <td  align="left">
              <textarea name="obse" cols="100%" rows="2" onkeypress="soloAlfa(event)" readonly><%=info_especifica.getValor_12()%></textarea>
            </td>
          </tr>
      </table></td>
    </tr>
  </table>
  

  
  <table width="1000" border="2" align="center">
      <tr>
        <td  font-size:"10px">
          <table width="100%" align="center">
            <tr>
              <td width="50%" height="22" align="center" class="subtitulo1">&nbsp;Detalle del Documento</td>
              <td width="50%" align="center" class="barratitulo"><div align="left"><img src="<%=BASEURL%>/images/titulo.gif" width="32" height="20"></div></td>
            </tr>
          </table>
          <div align="center">
            <table id="detalle" width="100%" align="center" >
              <tr  id="fila1" class="tblTituloFactura">
                <td align="center">
				<table width="79%" height="37" border="1" align="center"  bordercolor="#999999" bgcolor="#F7F5F4">
				<tr class="tblTitulo">
				 <td width="50" nowrap style="font-size:13px "><div align="center">Item</div></td>
				</tr>
				</table>
				</td>
                <td align="center"> 
				  <table width="100%" border="1"  bordercolor="#999999" bgcolor="#F7F5F4" align="center">
				  <tr class="tblTitulo">
               
                  <td width="57" nowrap style="font-size:11px "><div align="center">Remesa</div></td>
                  <td width="300" align="center" style="font-size:11px ">Descripcion</td>
                  <td width="58" align="center" style="font-size:11px "><div align="center">Cantidad</div></td>
                  <td width="129" align="center" style="font-size:11px "><div align="center">Valor Unitario </div></td>
                  <td width="126" align="center" style="font-size:11px "><div align="center">Valor</div></td>
                  <td width="190" align="center" style="font-size:9px "> Codigo Cuenta<br>
                    Tipo &nbsp &nbsp &nbsp &nbsp &nbsp &nbsp &nbsp &nbsp &nbsp Auxiliar</td>
				  </tr>
				  </table>
				</td>
              </tr>
			  
            <%
						
			for ( int i = 0; i < otros_datos.size(); i++ ) {
				BeanGeneral info = ( BeanGeneral ) otros_datos.elementAt( i );
	  		%>  	
	  		<tr class="<%=(i % 2 != 0 )?"filaazul":"filagris"%>" nowrap  bordercolor="#D1DCEB">
                <td class="bordereporte" align="center">&nbsp;<%=info.getValor_13()%></td>  
				<td>
					<table width='100%' class='tablaInferior'>
					  <tr class='<%=(i%2!=0)?"filaazul":"filagris"%>'  >
						<td width='7%' align="center">&nbsp;<%=info.getValor_14()%></td>
						<td width='35%' align="left">&nbsp;<%=info.getValor_16()%></td>
						<td width="5%" align="center">&nbsp;<%=Util.customFormat( Double.parseDouble( info.getValor_17() ) )%></td>
						<td width='15%' align="right">&nbsp;<%=Util.customFormat( Double.parseDouble( info.getValor_18() ) )%></td>
						<td width='15%' align="right">&nbsp;<%=Util.customFormat( Double.parseDouble( info.getValor_20() ) )%></td>
						<td width='23%' align='center'>&nbsp;<%=info.getValor_21()%></td>
					  </tr>
					</table>
				</td>
			
              </tr>
			  
				<%}%>
            </table>
        </div></td>
      </tr>
    </table>
	<table border="2" width="100%" align="center" class='filagris'>
		<tr><td width="50%" height="22" align="center" class="subtitulo1">Historial del Observaciones de Factura</td>
		<td width="50%" align="center" class="barratitulo"><div align="left"><img src="<%=BASEURL%>/images/titulo.gif" width="32" height="20"></div></td></tr>
		</table>
		
	<table border="0" width="100%" align="center" class='filagris'>
		
		</tr>

		<%
		BeanGeneral info = ( BeanGeneral ) otros_datos.elementAt( 0 );
		String[] observaciones = info.getV5();
		
		if(observaciones!=null){
		
			for(int i=0;i<observaciones.length;i++){
				int num= i+1;
				String[] temp = observaciones[i].split(";");
				out.print("<tr><td colspan=2><b>Observaci&oacute;n No. "+num+":</b> "+temp[0]+"</td></tr>");
				out.print("<tr><td colspan=2><b>Usuario:</b> "+temp[1]+"</td></tr>");
				out.print("<tr><td colspan=2><b>Fecha de creaci&oacute;n:</b> "+temp[2]+"</td></tr>");
				
				if(observaciones.length>1){
				%>
				<tr><td colspan=2 ><img src="<%=BASEURL%>/images/titulo.gif" width="32" height="20"></td></tr>
				<%
				}
			}
			
		}else{
			out.print("<tr><td>no hay observaciones</td></tr>");
		}
		%>
	</table>
	<p align="center">
	<img src="<%=BASEURL%>/images/botones/salir.gif"  name="imgsalir"  height="21" onmouseover="botonOver(this);" onclick="parent.close();" onMouseOut="botonOut(this);" style="cursor:hand">
	 </p>

</form >
<%}
	}%>
</div>
<%=datos[1]%>
</body>
</html>