<!--  
	 - Author(s)       :      LREALES
	 - Description     :      AYUDA FUNCIONAL - Reporte de Facturas de un Cliente
	 - Date            :      29/11/2006 
	 - Copyright Notice:      TRANSPORTES SANCHEZ POLO S.A.
-->
<%@ include file="/WEB-INF/InitModel.jsp"%>
<HTML>
<HEAD>
<TITLE>Funcionalidad del Reporte de Facturas de Clientes</TITLE>
<META http-equiv=Content-Type content="text/html; charset=windows-1252">
<link href="/../../../css/estilostsp.css" rel="stylesheet" type="text/css"> 
<link href="<%=BASEURL%>/css/estilostsp.css" rel="stylesheet" type="text/css">
<script type='text/javascript' src="<%= BASEURL %>/js/boton.js"></script> 
</HEAD>
<BODY> 
<% String BASEIMG = BASEURL +"/images/ayuda/reporte/facturas_clientes/"; %>
  <table width="95%"  border="2" align="center">
    <tr>
      <td height="117" >
        <table width="100%" border="0" align="center">
          <tr  class="subtitulo">
            <td height="20"><div align="center">MANUAL DE REPORTE WEB</div></td>
          </tr>
          <tr class="subtitulo1">
            <td>Descripci&oacute;n del funcionamiento del programa Reporte de Facturas de Clientes</td>
          </tr>
          <tr>
            <td  class="ayudaHtmlTexto"><div align="center"></div></td>
          </tr>
<tr>
            <td  class="ayudaHtmlTexto"><p class="ayudaHtmlTexto">&nbsp;</p>
            <p class="ayudaHtmlTexto">En la siguiente pantalla  se escoge el c&oacute;digo del cliente y el rango de las fechas en que desea generar el reporte.</p>
            <p class="ayudaHtmlTexto">&nbsp;</p></td>
          </tr>
<tr>
            <td  class="ayudaHtmlTexto"><div align="center"><IMG height=278 src="<%=BASEIMG%>001.JPG" width=1027 border=0 v:shapes="_x0000_i1143"></div></td>
          </tr>
<tr>
  <td  class="ayudaHtmlTexto"><p>&nbsp;</p>
    <p>El sistema verifica que usted haya escogido alg&uacute;n parametro de b&uacute;squeda. </p></td>
</tr>
<tr>
  <td  class="ayudaHtmlTexto"><div align="center"><IMG height=126 src="<%=BASEIMG%>error_000.JPG" width=356 border=0 v:shapes="_x0000_i1052"></div></td>
</tr>
<tr>
  <td  class="ayudaHtmlTexto"><p>&nbsp;</p>
    <p>Si en la pantalla le sale el siguiente mensaje de error, es por que usted no ha digitado el codigo del cliente. </p></td>
</tr>
<tr>
  <td  class="ayudaHtmlTexto"><div align="center"><IMG height=126 src="<%=BASEIMG%>error_001.JPG" width=399 border=0 v:shapes="_x0000_i1052"></div></td>
</tr>
<tr>
  <td  class="ayudaHtmlTexto"><p>&nbsp;</p>
    <p>El sistema verifica que usted ingrese la fecha inicial.</p></td>
</tr>
<tr>
  <td  class="ayudaHtmlTexto"><div align="center"><IMG height=126 src="<%=BASEIMG%>error_002.JPG" width=369 border=0 v:shapes="_x0000_i1052"></div></td>
</tr>
<tr>
  <td  class="ayudaHtmlTexto"><p>&nbsp;</p>
    <p>En la pantalla le saldra el siguiente mensaje si usted no ha definido la fecha final</p></td>
</tr>
<tr>
  <td  class="ayudaHtmlTexto"><p align="center"><IMG height=126 src="<%=BASEIMG%>error_003.JPG" width=359 border=0 v:shapes="_x0000_i1052"></p>    </td>
</tr>
<tr>
  <td  class="ayudaHtmlTexto"><p>&nbsp;</p>
    <p>El sistema verifica que la fecha final no sea menor que la fecha inicial</p></td>
</tr>
<tr>
  <td  class="ayudaHtmlTexto"><div align="center"><IMG height=126 src="<%=BASEIMG%>error_004.JPG" width=311 border=0 v:shapes="_x0000_i1052"></div></td>
</tr>
<tr>
  <td  class="ayudaHtmlTexto"><p>&nbsp;</p>
    <p>Si en la pantalla le sale el siguiente mensaje de error, es por que usted no ha digitado el n&uacute;mero de la factura. </p></td>
</tr>
<tr>
  <td  class="ayudaHtmlTexto"><div align="center"><IMG height=126 src="<%=BASEIMG%>error_007.JPG" width=336 border=0 v:shapes="_x0000_i1052"></div></td>
</tr>
<tr>
  <td  class="ayudaHtmlTexto"><p>&nbsp;</p>
    <p>El sistema verifica que usted haya ingresado el n&uacute;mero de la remesa.</p></td>
</tr>
<tr>
  <td  class="ayudaHtmlTexto"><div align="center"><IMG height=126 src="<%=BASEIMG%>error_008.JPG" width=336 border=0 v:shapes="_x0000_i1052"></div></td>
</tr>
<tr>
  <td  class="ayudaHtmlTexto"><p>&nbsp;</p>
    <p>Si le sale el siguiente mensaje de error, es por que el cliente digitado por usted, no existe en nuestra Base de Datos </p></td>
</tr>
<tr>
  <td  class="ayudaHtmlTexto"><div align="center"><IMG height=119 src="<%=BASEIMG%>error_006.JPG" width=888 border=0 v:shapes="_x0000_i1052"></div></td>
</tr>
<tr>
  <td  class="ayudaHtmlTexto"><p>&nbsp;</p>
    <p>Si usted no sabe cual es el c&oacute;digo del cliente, al presionar click en la lupa o bot&oacute;n de busqueda especifica, podr&aacute; buscar el cliente con el cual desea realizar el reporte. </p></td>
</tr>
<tr>
  <td  class="ayudaHtmlTexto"><div align="center"><IMG height=176 src="<%=BASEIMG%>010.JPG" width=775 border=0 v:shapes="_x0000_i1052"></div></td>
</tr>
<tr>
  <td  class="ayudaHtmlTexto"><p>&nbsp;</p>
    <p>Si le sale el siguiente mensaje, es por que el cliente con el nombre que usted especifico no existe en nuestra base de datos. </p></td>
</tr>
<tr>
  <td  class="ayudaHtmlTexto"><div align="center"><IMG height=221 src="<%=BASEIMG%>error_010.JPG" width=712 border=0 v:shapes="_x0000_i1052"></div></td>
</tr>
<tr>
  <td  class="ayudaHtmlTexto"><p>&nbsp;</p>
    <p>Luego de escribir el nombre del cliente que desea buscar, puede presionar enter &oacute; click sobre el boton de busqueda, e inmediatamente le aperecera una lista con los nombres de clientes que usted esta buscando, alli escoje el cliente que desea presionando click sobre &eacute;l. </p></td>
</tr>
<tr>
  <td  class="ayudaHtmlTexto"><div align="center"><IMG height=245 src="<%=BASEIMG%>020.JPG" width=712 border=0 v:shapes="_x0000_i1052"></div></td>
</tr>
<tr>
  <td  class="ayudaHtmlTexto"><p>&nbsp;</p>
    <p>Si el sistema le arroja el siguiente mensaje, es por que no existe informacion  con esos parametros definidos para su reporte en nuestra base de datos.</p></td>
</tr>
<tr>
  <td  class="ayudaHtmlTexto"><div align="center"><IMG height=101 src="<%=BASEIMG%>error_005.JPG" width=888 border=0 v:shapes="_x0000_i1052"></div></td>
</tr>
<tr>
            <td  class="ayudaHtmlTexto"><p class="ayudaHtmlTexto">&nbsp;</p>
            <p class="ayudaHtmlTexto">Despues de haber realizado la busqueda satisfactoriamente, se mostrara un reporte previo para que pueda confirmar los datos y proceder a generar el excel.</p>
            <p class="ayudaHtmlTexto">Y si desea observar la informaci&oacute;n detallada de alguna factura solo debe preseionar click sobre una de ellas. </p></td>
          </tr>
<tr>
            <td  class="ayudaHtmlTexto"><div align="center"><IMG height=376 src="<%=BASEIMG%>002.JPG" width=1084 border=0 v:shapes="_x0000_i1052"></div></td>
          </tr>
<tr>
  <td  class="ayudaHtmlTexto"><p>&nbsp;</p>
    <p>La Factura detallada sera mostrada en la pantalla de la siguiente forma. </p></td>
</tr>
<tr>
  <td  class="ayudaHtmlTexto"><div align="center"><IMG height=415 src="<%=BASEIMG%>005.JPG" width=1002 border=0 v:shapes="_x0000_i1052"></div></td>
</tr>
<tr>
  <td  class="ayudaHtmlTexto"><p>&nbsp;</p>
    <p>Al escoger 'EXPORTAR' el sistema le mostrara el siguiente mensaje, informandole donde puede observar el archivo excel generado.</p></td>
</tr>
<tr>
  <td  class="ayudaHtmlTexto"><div align="center"><IMG height=224 src="<%=BASEIMG%>003.JPG" width=425 border=0 v:shapes="_x0000_i1052"></div></td>
</tr>
<tr>
            <td  class="ayudaHtmlTexto"><p class="ayudaHtmlTexto">&nbsp;</p>
            <p class="ayudaHtmlTexto">El sistema confirma que el reporte fue exportado a excel.</p></td>
          </tr>
<tr>
  <td  class="ayudaHtmlTexto"><div align="center"><IMG height=112 src="<%=BASEIMG%>004.JPG" width=895 border=0 v:shapes="_x0000_i1054"></div></td>
</tr>
        </table></td>
    </tr>
  </table>
  <p></p>
<center>
<img src = "<%=BASEURL%>/images/botones/salir.gif" style = "cursor:hand" name = "imgsalir" onClick = "parent.close();" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);">
</center>
</BODY>
</HTML>