<!--
- Autor : LREALES
- Date  : 29 de noviembre de 2006
- Copyrigth Notice : Fintravalores S.A. S.A
-->
<%--
-@(#)
--Descripcion : Pagina JSP, que se encarga de mostrar el reporte de facturas de un cliente
--%>
<%--
The taglib directive below imports the JSTL library. If you uncomment it,
you must also add the JSTL library to the project. The Add Library... action
on Libraries node in Projects view can be used to add the JSTL 1.1 library.
--%>
<%-- Declaracion de librerias--%>
<%@page session="true"%>
<%@page errorPage="../error/ErrorPage.jsp"%>
<%@page import="com.tsp.operation.model.beans.*, java.util.*, java.text.*"%>
<%@ include file="/WEB-INF/InitModel.jsp"%>
<%@ taglib uri="http://jakarta.apache.org/taglibs/input-0.90" prefix="input" %>
<%@page import="com.tsp.util.*"%>
<% Util u = new Util(); %>
<html>
    <head>        
        <title>Reporte de Facturas de Clientes</title>
		<script src='<%=BASEURL%>/js/date-picker.js'></script>
		<script language="JavaScript" type="text/javascript" src="<%=BASEURL%>/js/boton.js"></script>
		<script type='text/javascript' src="<%=BASEURL%>/js/validar.js"></script> 
		<link href="../css/estilostsp.css" rel="stylesheet" type="text/css">
		<link href="<%=BASEURL%>/css/estilostsp.css" rel="stylesheet" type="text/css">
		 <script>
		 	function Exportar (){
				
			    alert( 
				
					'Puede observar el seguimiento del proceso en la siguiente ruta del menu:' + '\n' + '\n' +
					'FINTRA -> PROCESOS -> Seguimiento de Procesos' + '\n' + '\n' + '\n' +
					'Y puede observar el archivo excel generado, en la siguiente parte:' + '\n' + '\n' +
					'En la pantalla de bienvenida de la Pagina WEB,' + '\n' +
					'vaya a la parte superior-derecha,' + '\n' +
					'y presione un click en el icono que le aparece.'
					
				);
				
			}
		</script>
    </head>
   <body onResize="redimensionar()" onLoad="redimensionar()">
	<div id="capaSuperior" style="position:absolute; width:100%; height:100px; z-index:0; left: 1px; top: 0px;">
	 <jsp:include page="/toptsp.jsp?encabezado=Reporte Facturas Cliente"/>
	</div>
	<div id="capaCentral" style="position:absolute; width:100%; height:87%; z-index:0; left: 0px; top: 100px; overflow: scroll;">
	<%String msg = request.getParameter("msg");
		if ( msg!=null && !msg.equals("") ){%>							
		  <table border="2" align="center">
			<tr>
				<td>
					<table width="100%"  border="1" align="center"  bordercolor="#F7F5F4" bgcolor="#FFFFFF">
						<tr>
							<td width="229" align="center" class="mensajes"><%=msg%></td>
							<td width="29" background="<%=BASEURL%>/images/cuadronaranja.JPG">&nbsp;</td>
							<td width="58">&nbsp;</td>
						</tr>
					</table>
				</td>
			</tr>
		  </table>		  
		        <br>
		        <img src="<%=BASEURL%>/images/botones/regresar.gif" name="c_regresar" onclick="location.href='<%=BASEURL%>/jsp/general/exportar/facturas_clientes/ConsultarReporteFacturasClientes.jsp'" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" style="cursor:hand">
 	  <input name="salir" src="<%=BASEURL%>/images/botones/salir.gif" type="image" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" style="cursor:hand" class="boton" onclick="window.close();">
	  <%}
		else {
          Vector datos = model.reporteFacturasClientesService.getVectorReporte ();  
					
		if( datos != null && datos.size() > 0 ){
			BeanGeneral info_fechas = (BeanGeneral) datos.elementAt(0);
		%>		
		    <form name="form1" method="post" action="<%=CONTROLLER%>?estado=Reporte&accion=FacturasClientes&hacer=2">
	  <input name="cod_cli" type="hidden" id="cod_cli" value="<%=info_fechas.getValor_01()%>">
      <input name="fecini" type="hidden" id="fecini" value="<%=info_fechas.getValor_02()%>">
	  <input name="fecfin" type="hidden" id="fecfin" value="<%=info_fechas.getValor_03()%>">
	  
      <input name="documento" type="hidden" id="documento" value="<%=info_fechas.getValor_16()%>">
	  <input name="remesa" type="hidden" id="remesa" value="<%=info_fechas.getValor_17()%>">
	  <input name="opcion" type="hidden" id="opcion" value="<%=info_fechas.getValor_18()%>">
	  
	  <input name="clientes" type="hidden" id="clientes" value="<%=info_fechas.getValor_25()%>"> 
	  <input name="estate" type="hidden" id="estate" value="<%=info_fechas.getValor_26()%>">
	  
      <input name="Guardar2" src="<%=BASEURL%>/images/botones/exportarExcel.gif" type="image" class="boton" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" style="cursor:hand" onclick="Exportar();">
      <img src="<%=BASEURL%>/images/botones/regresar.gif" name="c_regresar" onclick="location.href='<%=BASEURL%>/jsp/general/exportar/facturas_clientes/ConsultarReporteFacturasClientes.jsp'" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" style="cursor:hand">
 	  <input name="salir" src="<%=BASEURL%>/images/botones/salir.gif" type="image" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" style="cursor:hand" class="boton" onclick="window.close();">
    </form>
		<table width="100%"  border="2">  
		
          <tr>
        	  <td class="subtitulo1"><strong>Reporte de Facturas de Clientes.</strong></td>
          </tr>
		  
		<%
			if ( info_fechas.getValor_18().equals("1") ) {
		%>
		
		  <tr>
		  	  <td class="letra">
			        Desde&nbsp;:&nbsp;<strong><%=info_fechas.getValor_02()%></strong>&nbsp;&nbsp;-&nbsp;&nbsp;Hasta&nbsp;:&nbsp;<strong><%=info_fechas.getValor_03()%></strong>&nbsp;.
			  </td>
		  </tr>
	      
		<%
			} else if ( info_fechas.getValor_18().equals("2") ) {
		%>
		  
		  <tr>
		  	  <td class="letra">
			        N&uacute;mero de la Factura&nbsp;:&nbsp;<strong><%=info_fechas.getValor_16()%></strong>&nbsp;.
			  </td>
		  </tr>
	      
		<%
			} else if ( info_fechas.getValor_18().equals("3") ) {
		%>
		
		  <tr>
		  	  <td class="letra">
			        N&uacute;mero de la Remesa&nbsp;:&nbsp;<strong><%=info_fechas.getValor_17()%></strong>&nbsp;.
			  </td>
		  </tr>
	      
		<%
			}
		%>
		
          <tr>
            <td>
              <table width="100%"  border="1" bordercolor="#999999" bgcolor="#F7F5F4" align="center">
                <tr class="tblTitulo" align="center">
					<td nowrap align="center">C&Oacute;DIGO CLIENTE</td>
		                    <td nowrap align="center">NEGOCIO</td>
                    <td nowrap align="center">NOMBRE CLIENTE</td>
                    <td nowrap align="center">IDENTIFICACI&Oacute;N DEL CLIENTE</td>
	                <td nowrap align="center">DIRECCI&Oacute;N</td>
					
                    <td nowrap align="center">TEL&Eacute;FONO</td>
					<td nowrap align="center">ESTADO</td>
                    <td nowrap align="center">N&deg; FACTURA</td>
                    <td nowrap align="center">VALOR FACTURA</td>
                    <td nowrap align="center">VALOR ABONO</td>
                    <td nowrap align="center">VALOR SALDO</td>
                    <td nowrap align="center">MONEDA</td>
                    <td nowrap align="center">FECHA FACTURA</td>
                    <td nowrap align="center">FECHA VENCIMIENTO</td>
                    <td nowrap align="center">FECHA CONTABILIZACI&oacute;N &Uacute;LTIMO PAGO</td>
					<td nowrap align="center">FECHA &Uacute;LTIMO PAGO</td>
                    <td nowrap align="center">DESCRIPCI&Oacute;N</td>         
					
					<td nowrap align="center">FECHA DE IMPRESIÓN</td>         
					<td nowrap align="center">FECHA DE ANULACIÓN</td>          
					<td nowrap align="center">USUARIO ANULO</td>        
					<td nowrap align="center">FECHA DE CONTABILIZACIÓN</td>   
					       
					<td nowrap align="center">TRANSACCIÓN</td>
					<td nowrap align="center">DOCUMENTO RELACIONADO</td>      
					<td nowrap align="center">USUARIO CREACION</td>
                </tr>						
		<%
			for( int i = 0; i<datos.size(); i++ ){
				BeanGeneral info = (BeanGeneral) datos.elementAt(i);
	  %>  	  
	  
	  <tr class="<%=(i % 2 == 0 )?"filagris":"filaazul"%>" onmouseover='cambiarColorMouse(this)' style="cursor:hand" >
	  
	  			  <!--
                  <tr class="< %=(i % 2 == 0 )?"filagris":"filaazul"%>">
				  -->
				  <td nowrap align="left" class="bordereporte" title="Ver Detalle Factura.." onClick="window.open('<%=CONTROLLER%>?estado=Reporte&accion=FacturasClientes&hacer=3&dist=<%=info.getValor_19()%>&docu=<%=info.getValor_07()%>&tdoc=<%=info.getValor_20()%>' ,'M','status=yes,scrollbars=no,width=850,height=650,resizable=yes');"><%=(info.getValor_01()!=null)?info.getValor_01():""%>&nbsp;</td>

				  <td nowrap align="left" class="bordereporte" title="Ver Detalle Factura.." onClick="window.open('<%=CONTROLLER%>?estado=Reporte&accion=FacturasClientes&hacer=3&dist=<%=info.getValor_19()%>&docu=<%=info.getValor_07()%>&tdoc=<%=info.getValor_20()%>' ,'M','status=yes,scrollbars=no,width=850,height=650,resizable=yes');"><%=(info.getValor_30()!=null)?info.getValor_30():""%>&nbsp;</td>

				  <td nowrap align="left" class="bordereporte" title="Ver Detalle Factura.." onClick="window.open('<%=CONTROLLER%>?estado=Reporte&accion=FacturasClientes&hacer=3&dist=<%=info.getValor_19()%>&docu=<%=info.getValor_07()%>&tdoc=<%=info.getValor_20()%>' ,'M','status=yes,scrollbars=no,width=850,height=650,resizable=yes');"><%=(info.getValor_05()!=null)?info.getValor_05():""%>&nbsp;</td>
				  <td nowrap align="left" class="bordereporte" title="Ver Detalle Factura.." onClick="window.open('<%=CONTROLLER%>?estado=Reporte&accion=FacturasClientes&hacer=3&dist=<%=info.getValor_19()%>&docu=<%=info.getValor_07()%>&tdoc=<%=info.getValor_20()%>' ,'M','status=yes,scrollbars=no,width=850,height=650,resizable=yes');"><%=(info.getValor_04()!=null)?info.getValor_04():""%>&nbsp;</td>
				  
				  <td nowrap align="left" class="bordereporte" title="Ver Detalle Factura.." onClick="window.open('<%=CONTROLLER%>?estado=Reporte&accion=FacturasClientes&hacer=3&dist=<%=info.getValor_19()%>&docu=<%=info.getValor_07()%>&tdoc=<%=info.getValor_20()%>' ,'M','status=yes,scrollbars=no,width=850,height=650,resizable=yes');"><%=(info.getValor_28()!=null)?info.getValor_28():""%>&nbsp;</td>	
				  <td nowrap align="left" class="bordereporte" title="Ver Detalle Factura.." onClick="window.open('<%=CONTROLLER%>?estado=Reporte&accion=FacturasClientes&hacer=3&dist=<%=info.getValor_19()%>&docu=<%=info.getValor_07()%>&tdoc=<%=info.getValor_20()%>' ,'M','status=yes,scrollbars=no,width=850,height=650,resizable=yes');"><%=(info.getValor_04()!=null)?info.getValor_27():""%>&nbsp;</td>	
				  			  
				  <td nowrap align="left" class="bordereporte" title="Ver Detalle Factura.." onClick="window.open('<%=CONTROLLER%>?estado=Reporte&accion=FacturasClientes&hacer=3&dist=<%=info.getValor_19()%>&docu=<%=info.getValor_07()%>&tdoc=<%=info.getValor_20()%>' ,'M','status=yes,scrollbars=no,width=850,height=650,resizable=yes');"><%=(info.getValor_06()!=null)?info.getValor_06():""%>&nbsp;</td>
                  <td nowrap align="left" class="bordereporte" title="Ver Detalle Factura.." onClick="window.open('<%=CONTROLLER%>?estado=Reporte&accion=FacturasClientes&hacer=3&dist=<%=info.getValor_19()%>&docu=<%=info.getValor_07()%>&tdoc=<%=info.getValor_20()%>' ,'M','status=yes,scrollbars=no,width=850,height=650,resizable=yes');"><%=(info.getValor_07()!=null)?info.getValor_07():""%>&nbsp;</td>
                  <td nowrap align="right" class="bordereporte" title="Ver Detalle Factura.." onClick="window.open('<%=CONTROLLER%>?estado=Reporte&accion=FacturasClientes&hacer=3&dist=<%=info.getValor_19()%>&docu=<%=info.getValor_07()%>&tdoc=<%=info.getValor_20()%>' ,'M','status=yes,scrollbars=no,width=850,height=650,resizable=yes');">$&nbsp;<%=(info.getValor_08()!=null)?Util.customFormat( Double.parseDouble( info.getValor_08() ) ):""%>&nbsp;</td>
				  <td nowrap align="right" class="bordereporte" title="Ver Detalle Factura.." onClick="window.open('<%=CONTROLLER%>?estado=Reporte&accion=FacturasClientes&hacer=3&dist=<%=info.getValor_19()%>&docu=<%=info.getValor_07()%>&tdoc=<%=info.getValor_20()%>' ,'M','status=yes,scrollbars=no,width=850,height=650,resizable=yes');">$&nbsp;<%=(info.getValor_09()!=null)?Util.customFormat( Double.parseDouble( info.getValor_09() ) ):""%>&nbsp;</td>
				  <td nowrap align="right" class="bordereporte" title="Ver Detalle Factura.." onClick="window.open('<%=CONTROLLER%>?estado=Reporte&accion=FacturasClientes&hacer=3&dist=<%=info.getValor_19()%>&docu=<%=info.getValor_07()%>&tdoc=<%=info.getValor_20()%>' ,'M','status=yes,scrollbars=no,width=850,height=650,resizable=yes');">$&nbsp;<%=(info.getValor_10()!=null)?Util.customFormat( Double.parseDouble( info.getValor_10() ) ):""%>&nbsp;</td>
				  <td nowrap align="center" class="bordereporte" title="Ver Detalle Factura.." onClick="window.open('<%=CONTROLLER%>?estado=Reporte&accion=FacturasClientes&hacer=3&dist=<%=info.getValor_19()%>&docu=<%=info.getValor_07()%>&tdoc=<%=info.getValor_20()%>' ,'M','status=yes,scrollbars=no,width=850,height=650,resizable=yes');"><%=(info.getValor_11()!=null)?info.getValor_11() :""%>&nbsp;</td>
				  <td nowrap align="center" class="bordereporte" title="Ver Detalle Factura.." onClick="window.open('<%=CONTROLLER%>?estado=Reporte&accion=FacturasClientes&hacer=3&dist=<%=info.getValor_19()%>&docu=<%=info.getValor_07()%>&tdoc=<%=info.getValor_20()%>' ,'M','status=yes,scrollbars=no,width=850,height=650,resizable=yes');"><%=(info.getValor_12()!=null)?info.getValor_12():""%>&nbsp;</td>
				  <td nowrap align="center" class="bordereporte" title="Ver Detalle Factura.." onClick="window.open('<%=CONTROLLER%>?estado=Reporte&accion=FacturasClientes&hacer=3&dist=<%=info.getValor_19()%>&docu=<%=info.getValor_07()%>&tdoc=<%=info.getValor_20()%>' ,'M','status=yes,scrollbars=no,width=850,height=650,resizable=yes');"><%=(info.getValor_13()!=null)?info.getValor_13():""%>&nbsp;</td>
				  <td nowrap align="center" class="bordereporte" title="Ver Detalle Factura.." onClick="window.open('<%=CONTROLLER%>?estado=Reporte&accion=FacturasClientes&hacer=3&dist=<%=info.getValor_19()%>&docu=<%=info.getValor_07()%>&tdoc=<%=info.getValor_20()%>' ,'M','status=yes,scrollbars=no,width=850,height=650,resizable=yes');"><%=(info.getValor_14()!=null)?info.getValor_14():""%>&nbsp;</td>

				  <td nowrap align="center" class="bordereporte" title="Ver Detalle Factura.." onClick="window.open('<%=CONTROLLER%>?estado=Reporte&accion=FacturasClientes&hacer=3&dist=<%=info.getValor_19()%>&docu=<%=info.getValor_07()%>&tdoc=<%=info.getValor_20()%>' ,'M','status=yes,scrollbars=no,width=850,height=650,resizable=yes');"><%=(info.getValor_26()!=null)?info.getValor_29():""%>&nbsp;</td>       
				  
                  <td nowrap align="left" class="bordereporte" title="Ver Detalle Factura.." onClick="window.open('<%=CONTROLLER%>?estado=Reporte&accion=FacturasClientes&hacer=3&dist=<%=info.getValor_19()%>&docu=<%=info.getValor_07()%>&tdoc=<%=info.getValor_20()%>' ,'M','status=yes,scrollbars=no,width=850,height=650,resizable=yes');"><%=(info.getValor_15()!=null)?info.getValor_15():""%>&nbsp;</td>       
				  
				  <td nowrap align="left" class="bordereporte" title="Ver Detalle Factura.." onClick="window.open('<%=CONTROLLER%>?estado=Reporte&accion=FacturasClientes&hacer=3&dist=<%=info.getValor_19()%>&docu=<%=info.getValor_07()%>&tdoc=<%=info.getValor_20()%>' ,'M','status=yes,scrollbars=no,width=850,height=650,resizable=yes');"><%=(info.getValor_21()!=null)?info.getValor_21():""%>&nbsp;</td> 
                  <td nowrap align="center" class="bordereporte" title="Ver Detalle Factura.." onClick="window.open('<%=CONTROLLER%>?estado=Reporte&accion=FacturasClientes&hacer=3&dist=<%=info.getValor_19()%>&docu=<%=info.getValor_07()%>&tdoc=<%=info.getValor_20()%>' ,'M','status=yes,scrollbars=no,width=850,height=650,resizable=yes');"><%=(info.getFecha_anulacion()!=null)?info.getFecha_anulacion():""%>&nbsp;</td>
                  <td nowrap align="center" class="bordereporte" title="Ver Detalle Factura.." onClick="window.open('<%=CONTROLLER%>?estado=Reporte&accion=FacturasClientes&hacer=3&dist=<%=info.getValor_19()%>&docu=<%=info.getValor_07()%>&tdoc=<%=info.getValor_20()%>' ,'M','status=yes,scrollbars=no,width=850,height=650,resizable=yes');"><%=(info.getUsuario_anulo()!=null)?info.getUsuario_anulo():""%>&nbsp;</td>       
				  <td nowrap align="left" class="bordereporte" title="Ver Detalle Factura.." onClick="window.open('<%=CONTROLLER%>?estado=Reporte&accion=FacturasClientes&hacer=3&dist=<%=info.getValor_19()%>&docu=<%=info.getValor_07()%>&tdoc=<%=info.getValor_20()%>' ,'M','status=yes,scrollbars=no,width=850,height=650,resizable=yes');"><%=(info.getValor_23()!=null)?info.getValor_23():""%>&nbsp;</td>       
     
				
                   <%  String f2     = "FAC"; 
                       String fecha2 = "0099-01-01";%> 
				  <%if(info.getValor_24().equals("0")){ %>                  
				     <td nowrap align="left" class="bordereporte" title="Ver Detalle Factura.." onClick="window.open('<%=CONTROLLER%>?estado=Reporte&accion=FacturasClientes&hacer=3&dist=<%=info.getValor_19()%>&docu=<%=info.getValor_07()%>&tdoc=<%=info.getValor_20()%>' ,'M','status=yes,scrollbars=no,width=850,height=650,resizable=yes');"></td>       
				  <%}else{%>	
				  	<td nowrap align="left" class="bordereporte" onClick="window.open('<%=CONTROLLERCONTAB%>?estado=AdminComprobantes&accion=Manager&opc=ver&dstrct=<%=info.getValor_19()%>&tipodoc=<%=f2%>&numdoc=<%=info.getValor_07()%>&grupo=<%=info.getValor_24()%>&fechaapply=<%=fecha2%>','','HEIGHT=600,WIDTH=940,STATUS=YES,SCROLLBARS=YES,RESIZABLE=YES,top=0,left=50')"><%=(info.getValor_24()!=null)?info.getValor_24():""%>&nbsp;</td>       
				  <%}%>	       
				  <td nowrap align="left" class="bordereporte" title="Ver Detalle Factura.." onClick="window.open('<%=CONTROLLER%>?estado=Reporte&accion=FacturasClientes&hacer=3&dist=<%=info.getValor_19()%>&docu=<%=info.getValor_07()%>&tdoc=<%=info.getValor_20()%>' ,'M','status=yes,scrollbars=no,width=850,height=650,resizable=yes');"><%=(info.getOts()!=null)?info.getOts():""%>&nbsp;</td> 
				  <td nowrap align="center" class="bordereporte" title="Ver Detalle Factura.." onClick="window.open('<%=CONTROLLER%>?estado=Reporte&accion=FacturasClientes&hacer=3&dist=<%=info.getValor_19()%>&docu=<%=info.getValor_07()%>&tdoc=<%=info.getValor_20()%>' ,'M','status=yes,scrollbars=no,width=850,height=650,resizable=yes');"><%=(info.getCreation_user()!=null)?info.getCreation_user():""%>&nbsp;</td>
	  </tr>
		<%}%>
            </table>
      </table></td>
      </tr>
		    </table>			
		    <form name="form1" method="post" action="<%=CONTROLLER%>?estado=Reporte&accion=FacturasClientes&hacer=2">
      <input name="cod_cli" type="hidden" id="cod_cli" value="<%=info_fechas.getValor_01()%>">
      <input name="fecini" type="hidden" id="fecini" value="<%=info_fechas.getValor_02()%>">
	  <input name="fecfin" type="hidden" id="fecfin" value="<%=info_fechas.getValor_03()%>">
	  
      <input name="documento" type="hidden" id="documento" value="<%=info_fechas.getValor_16()%>">
	  <input name="remesa" type="hidden" id="remesa" value="<%=info_fechas.getValor_17()%>">
	  <input name="opcion" type="hidden" id="opcion" value="<%=info_fechas.getValor_18()%>">
	  
	  <input name="clientes" type="hidden" id="clientes" value="<%=info_fechas.getValor_25()%>"> 
	  <input name="estate" type="hidden" id="estate" value="<%=info_fechas.getValor_26()%>">
	  
      <input name="Guardar3" src="<%=BASEURL%>/images/botones/exportarExcel.gif" type="image" class="boton" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" style="cursor:hand" onclick="Exportar();">
      <img src="<%=BASEURL%>/images/botones/regresar.gif" name="c_regresar" onclick="location.href='<%=BASEURL%>/jsp/general/exportar/facturas_clientes/ConsultarReporteFacturasClientes.jsp'" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" style="cursor:hand">
	  <input name="salir2" src="<%=BASEURL%>/images/botones/salir.gif" type="image" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" style="cursor:hand" class="boton" onclick="window.close();">
    </form>	
	  <%}	  
	  }
	  %>	  
	      </body>
		  </div>
		 <%session.removeAttribute("prov");
		 session.removeAttribute("est");%>
</html>