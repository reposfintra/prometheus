<!--
- Autor : Ing. Jose De la rosa
- Date  : 23 Agosto 2006
- Copyrigth Notice : Fintravalores S.A. S.A
-->
<%--
-@(#)
--Descripcion : Vista que permite capturar el rango de fecha para realizar el reporte
--%>

<%-- Declaracion de librerias--%>
<%@page contentType="text/html"%>
<%@page session="true"%>
<%@page errorPage="../error/ErrorPage.jsp"%>
<%@page import="java.util.*"%>
<%@page import="com.tsp.operation.model.*"%>
<%@page import="com.tsp.util.*"%>
<%@page import="com.tsp.operation.model.beans.*"%>
<%@include file="/WEB-INF/InitModel.jsp"%>

<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <title>.: Reporte Control Discrepanciad</title>
    <link href="<%= BASEURL %>/css/estilostsp.css" rel="stylesheet" type="text/css"> 
    <link href="../../../css/estilostsp.css" rel="stylesheet" type="text/css"> 
    <script language="JavaScript" type="text/javascript" src="<%=BASEURL%>/js/boton.js"></script>
	<script type='text/javascript' src="<%= BASEURL %>/js/general.js"></script>     
</head>
<body onLoad="redimensionar();" onResize="redimensionar();">
<%  String path    = application.getRealPath(model.menuService.getRealUrl(request.getRequestURI()));
    String datos[] = model.menuService.getContenidoMenu(BASEURL,request.getRequestURI(),path);    %>
    <div id="capaSuperior" style="position:absolute; width:100%; height:100px; z-index:0; left: 0px; top: 0px;">
        <jsp:include page="/WEB-INF/toptsp.jsp?encabezado=Reporte Control Discrepancia"/>
    </div>
    <div id="capaCentral" style="position:absolute; width:100%; height:85%; z-index:0; left: 0px; top: 100px; overflow: scroll;"> 
   <% 
   	String  hoy       = Utility.getHoy("-"); 
    String  msj       = request.getAttribute("mensaje")!=null?(String)request.getAttribute("mensaje"):"";
   %>
        <form action="<%=CONTROLLER%>?estado=Reporte_control&accion=Discrepancia" method='post' name='formulario' id="formulario" >
 
        <table width="400" border="2" align="center">
            <tr>
            <td>  
                <table width='100%' align='center' class='tablaInferior'>
           
                    <tr class="barratitulo">
                        <td colspan='2' >
                            <table cellpadding='0' cellspacing='0' width='100%'>
                                <tr>
                                    <td align="left" width='50%' class="subtitulo1">&nbsp;Rango de Fecha </td>
                                    <td align="left" width='*'  ><img src="<%=BASEURL%>/images/titulo.gif" width="32" height="20" align="left"><%=datos[0]%></td>
                                </tr>
                            </table>
                        </td>
                    </tr>
              
            
                    <tr class="fila">
                    <td class='letrafila' width='35%'> Fecha Inicial</td>
                    <td width='*'>
                        <!-- Fecha Inicial -->
                        <input  name='fechaInicio' size="20" readonly="true" class="textbox" value='<%=hoy%>' style='width:50%'> 
                        <a href="javascript:void(0)" onClick="if(self.gfPop)gfPop.fPopCalendar(fechaInicio);return false;" hidefocus>
                        <img name="popcal" align="absmiddle" src="<%=BASEURL%>/js/calendartsp/calbtn.gif" width="16" height="16" border="0" alt="">
                        </a>
                    </td>
                    </tr>
              
                    <tr class="fila">
                    <td class='letrafila'>Fecha Final </td>
                    <td>
                     
                        <!-- Fecha Final -->                   
                        <input  name='fechaFinal' size="20" readonly="true" class="textbox" value='<%=hoy%>' style='width:50%'> 
                        <a href="javascript:void(0)" onClick="if(self.gfPop)gfPop.fPopCalendar(fechaFinal);return false;" hidefocus>
                        <img name="popcal" align="absmiddle" src="<%=BASEURL%>/js/calendartsp/calbtn.gif" width="16" height="16" border="0" alt="">
                        </a>
                     
                    </td>
                    </tr>
                    <tr class='fila'>
                        <td class="letrafila">Tipo de Discrepancia:</td>
                        <td>
                            <select name="tipo" class="select">
								<option value="T">Todas</option>
								<option value="AB">Abierta</option>
								<option value="CE">Cerrada</option>
							</select>
                        </td>
                    </tr>
            
                </table>
            </td>
            </tr>
        </table>   
        <br>
        <center>                  
            <img src="<%=BASEURL%>/images/botones/exportarExcel.gif" name="a"  height="21" title='Generar Reporte' onclick="if ( validarFecha() ){ formulario.submit(); } " onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" style="cursor:hand">
            <img src="<%=BASEURL%>/images/botones/salir.gif"   name="s"  height="21" title='Salir'            onClick="window.close();"                                                                                                         onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" style="cursor:hand">
        </center>
        </form>
        <!-- mensajes -->
    <% if( !msj.equals("") ){%>        
        <table border="2" align="center" width="600">
            <tr>
                <td><table width="100%"  border="1" align="center"  bordercolor="#F7F5F4" bgcolor="#FFFFFF">
                    <tr>
                        <td width="420" class="mensajes"><%=msj%>
                        <%if(request.getParameter("ruta")!=null){%><br>
					&nbsp;&nbsp;<a href="#" class="Simulacion_Hiper" onClick="window.open('<%= CONTROLLER %>?estado=Log&accion=Proceso&Estado=TODO','log','scroll=no, resizable=yes, width=800, height=600')" style="cursor:hand "><%=request.getParameter("ruta")%><%}%></a></td>
                        <td width="29" background="<%=BASEURL%>/images/cuadronaranja.JPG">&nbsp;</td>
                        <td width="58">&nbsp;</td>
                    </tr>
                </table></td>
            </tr>
        </table>
    <%  } %>
    </div> 

<%=datos[1]%>  
	<!-- Necesario para los calendarios-->
    <iframe width="188" height="166" name="gToday:normal:<%=BASEURL%>/js/calendartsp/agenda.js:gfPop:<%=BASEURL%>/js/calendartsp/plugins.js" id="gToday:normal:<%=BASEURL%>/js/calendartsp/agenda.js:gfPop:<%=BASEURL%>/js/calendartsp/plugins.js" src="<%=BASEURL%>/js/calendartsp/ipopeng.htm" scrolling="no" frameborder="0" style="visibility:visible; z-index:999; position:absolute; left:-500px; top:0px;"></iframe>

</body>
</html>
<script>
/*script que valida que una fecha inicial sea menor que la final*/
function validarFecha(){
	if(formulario.fechaInicio.value > formulario.fechaFinal.value ) {     
		alert('La FECHA INICIAL debe ser igual o menor que la FECHA FINAL');
		return (false);
	}else {
		return (true);
	}
}
</script>