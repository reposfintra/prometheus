<!--
- Autor : Ing. David Lamadrid
- Date  : 5 de Octubre de 2005
- Copyrigth Notice : Fintravalores S.A. S.A
-->
<%--
-@(#)
--Descripcion : Pagina JSP, que se encarga de gestionar 
                            el formulario para la insercion de tablas a diferrentes usuarios
--%>

<%-- Declaracion de librerias--%>
<%@page contentType="text/html"%>
<%@page errorPage="/error/ErrorPage.jsp"%>
<%@page import="com.tsp.operation.model.beans.*, java.util.*"%>
<%@include file="/WEB-INF/InitModel.jsp" %>
<%@ taglib uri="http://jakarta.apache.org/taglibs/input-0.90" prefix="input" %>
<%
    Usuario usuarioLogin  = (Usuario) session.getAttribute("Usuario");
    String us  = usuarioLogin.getLogin();
%>

<html>
<head>
<title>Asignar Turno a Usuario</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link href="../../trafico/css/estilostsp.css" rel="stylesheet" type="text/css"> 
<link href="<%=BASEURL%>/css/estilostsp.css" rel="stylesheet" type="text/css"> 
<script type='text/javascript' src="<%=BASEURL%>/js/Validacion.js"></script> 
<script type='text/javascript' src="<%= BASEURL %>/js/boton.js"></script>
<script>

function abrirVentanaBusq(an,al,url,pag) {
	parent.open(url,'Conductor','width='+an+',height='+al+',scrollbars=no,resizable=no,top=10,left=65,status=yes');
}


function validar(){	
	if(forma1.c_nombre.value==""   ){
		alert ("Digite un nombre de Tabla ");
		forma1.c_nombre.focus();
		return false;
	}
	else if(forma1.usuario.value==""  ){
		alert ("Digite un Login de Usuario");
		forma1.c_usuario.focus();
		return false;
	}
	else {
		insertar();
	}	
}

function salir(){
	document.forma1.action = "<%= CONTROLLER %>?estado=TablasU&accion=Salir"; 
    document.forma1.submit(); 
}

function insertar(){
		document.forma1.action = "<%= CONTROLLER %>?estado=TablasU&accion=Insertar"; 
        document.forma1.submit(); 
}

</script>

<link href="/css/estilostsp.css" rel="stylesheet" type="text/css">
</head>

<body onresize="redimensionar()" onload = 'redimensionar()'>
<div id="capaSuperior" style="position:absolute; width:100%; height:100px; z-index:0; left: 0px; top: 0px;">
 <jsp:include page="/toptsp.jsp?encabezado=Asignar Tabla a Usuario"/>
</div>
<div id="capaCentral" style="position:absolute; width:100%; height:87%; z-index:0; left: 0px; top: 100px; overflow: scroll;">
<%

     TablasUsuario tablas = model.tablasUsuarioService.getTabla();
     String nombre ="";
     String usuario="";
     String descripcion="";
     /*if( tablas!=null){
		nombre= tablas.getNombre();
		usuario=tablas.getUsuario();
		descripcion=tablas.getDescripcion();
    }*/
	
%>
<form name="forma1" action="<%=CONTROLLER%>?estado=Historico&accion=Insertar" method="post" onSubmit="return validar();">      
    <table width="396" border="2" align="center">
      <tr>
    <td width="696">
<table width="99%" align="center"> 
  <tr>
            <td width="145"  class="subtitulo1"> Tablas por Usuario
</td>
            <td width="223"  class="barratitulo"><img src="<%=BASEURL%>/images/titulo.gif"></td>
  </tr>
</table>
        <table width="99%" align="center">
          <tr class="fila"> 
            <td width="144" height="30" >Nombre de la Tabla  : </td>
            <td width="201" >
			  <input name="c_nombre" type="text" class="textbox" id="c_nombre" value="<%=nombre%>" size="20" maxlength="60" > 
			  <img src='<%=BASEURL%>/images/botones/iconos/lupa.gif'  width="15" height="15"  onClick="javascript:abrirVentanaBusq(850,550,'<%=BASEURL%>/jsp/general/consultas/buscartablas.jsp')" title="Buscar" style="cursor:hand" >
              </td>
          </tr>
          <tr class="fila"> 
            <td width="144" height="30" >Usuario(Login):</td>
            <td width="201" ><input name="c_usuario" type="text" class="textbox" id="c_usuario" value="<%=usuario%>"size="30" maxlength="60" > 
			<img src='<%=BASEURL%>/images/botones/iconos/lupa.gif'  width="15" height="15"  onClick="javascript:abrirVentanaBusq(850,550,'<%=BASEURL%>/jsp/general/consultas/usuarios.jsp')" title="Buscar" style="cursor:hand" >           
		  	 <div align="left"  id="td_nombre" name="td_nombre"></div>
		    </td>
          </tr>
          <tr class="fila"> 
              <td  align="center"><div align="left">Descripci&oacute;n:</div></td>
			   <td  align="center">
                  <textarea name="c_descripcion" cols="32" class="textbox" id="c_descripcion"><%=descripcion%></textarea>
                </td>
          </tr>
        </table>
</td>
</tr>
</table>

  <br>
  <div align="center">
  <input type="hidden" name='usuario' value="<%=us%>">
      <img src='<%=BASEURL%>/images/botones/aceptar.gif' name='Buscar' align="absmiddle" style='cursor:hand' title='Aceptar...'  onclick="validar();" onMouseOver='botonOver(this);' onMouseOut='botonOut(this);'> 
      &nbsp;&nbsp;<img src='<%=BASEURL%>/images/botones/salir.gif' name='Buscar' align="absmiddle" style='cursor:hand' title='Salir...'  onclick="salir();" onMouseOver='botonOver(this);' onMouseOut='botonOut(this);'> 
    </div>
  <p>&nbsp;</p> 
</form>

<%
  String mensaje=""+request.getParameter("ms");
    if (! (mensaje.equals("")|| mensaje.equals("null"))){
%>
<table border="2" align="center">
  <tr>
    <td><table width="410" height="20" border="1" align="center"  bordercolor="#F7F5F4" bgcolor="#FFFFFF">
        <tr>
          <td width="282" align="center" class="mensajes"><%=mensaje%></td>
          <td width="28" background="<%=BASEURL%>/images/cuadronaranja.JPG">&nbsp;</td>
          <td width="78">&nbsp;</td>
        </tr>
    </table></td>
  </tr>
</table>
<%
    }   %>
</div>
</body>
</html>
