<%@page session="true"%>
<%@page import="java.util.*" %>
<%@page import="com.tsp.operation.model.*"%>
<%@include file="/WEB-INF/InitModel.jsp"%>
<%@page import="com.tsp.operation.model.beans.*"%>
<%@page import="com.tsp.util.*"%>
<%@ taglib uri="http://jakarta.apache.org/taglibs/input-0.90" prefix="input" %>
<html>
<head>  
<title>Consulta Trailer</title> 
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link href="<%=BASEURL%>/css/estilostsp.css" rel="stylesheet" type="text/css">  
<link href="../../../../css/estilostsp.css" rel="stylesheet" type="text/css"> 
<script language="JavaScript" type="text/javascript" src="<%=BASEURL%>/js/validar.js"></script>
<script type='text/javascript' src="<%= BASEURL %>/js/boton.js"></script>
<script type='text/javascript' src="<%= BASEURL %>/js/ingreso.js"></script>
<script>
	function validarBusqueda(){
		if( form1.fechaInicio.value > form1.fechaFinal.value ){
				alert( "La fecha inicial debe ser menor a la fecha final" );                            
				return false;
		}
		else if( form1.fechaInicio.value == "" ){
				alert( "Debe seleccionar la fecha inicial" );                          
				return false;
		}		
		else if( form1.fechaFinal.value == "" ){
				alert( "Debe seleccionar la fecha final" );                          
				return false;
		}
		form1.mod.src = "<%=BASEURL%>/images/botones/buscarDisable.gif";
	        form1.mod.disabled = true;
                form1.submit();
	}
</script>
</head>
<body onResize="redimensionar();" onLoad='redimensionar();' > 
<div id="capaSuperior" style="position:absolute; width:100%; height:100px; z-index:0; left: 0px; top: 0px;">
 <jsp:include page="/WEB-INF/toptsp.jsp?encabezado=Consulta Trailer"/>
</div>
<div id="capaCentral" style="position:absolute; width:100%; height:87%; z-index:0; left: 0px; top: 100px; overflow: scroll;">
<FORM ACTION="<%=CONTROLLER%>?estado=Cargar&accion=Varios&carpeta=jsp/general/consultas&pagina=ListadoConsultaCabezote.jsp&sw=13" METHOD='post' id='form1' name='form1'>
  <table width="500" border="2" align="center">
    <tr>
      <td><table width="100%"  border="0">
        <tr>
          <td width="50%" class="subtitulo1">&nbsp;Buscar Datos</td>
          <td width="50%" class="barratitulo"><img src="<%=BASEURL%>/images/titulo.gif" width="32" height="20"></td>
          </tr>
      </table>      <table width="100%" class="tablaInferior">
        
        <tr class="fila"> 
          <td width="107">Placa Trailer</td>
          <td width="129"><input name='trailer' type='text'  class='textbox' id="trailer" size='13' maxlength='12'>
            </td>
          <td width="101">Placa Cabezote</td>
          <td width="131"><input name='cabezote' type='text'  class='textbox' id="cabezote" size='13' maxlength='12'></td>
        </tr>
        <tr class="fila">
          <td>Rango de Fecha</td>
          <td><input  name='fechaInicio' size="11" readonly="true" class="textbox" >
            <a href="javascript:void(0)" onClick="if(self.gfPop)gfPop.fPopCalendar(fechaInicio);return false;" hidefocus><img name="popcal" align="absmiddle" src="<%=BASEURL%>/js/calendartsp/calbtn.gif" width="16" height="16" border="0" alt=""></a> <img src="<%=BASEURL%>/images/botones/iconos/obligatorio.gif" width="10" height="10"></td>
          <td colspan="2"><input  name='fechaFinal' size="11" readonly="true" class="textbox" >
            <a href="javascript:void(0)" onClick="if(self.gfPop)gfPop.fPopCalendar(fechaFinal);return false;" hidefocus><img name="popcal2" align="absmiddle" src="<%=BASEURL%>/js/calendartsp/calbtn.gif" width="16" height="16" border="0" alt=""></a> <img src="<%=BASEURL%>/images/botones/iconos/obligatorio.gif" width="10" height="10"></td>
          </tr>
      </table></td>
    </tr>
  </table>
 <br>
  <div align="center"><img src="<%=BASEURL%>/images/botones/buscar.gif"  name="mod" id="mod" onClick="validarBusqueda();" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" style="cursor:hand">&nbsp;<img src="<%=BASEURL%>/images/botones/salir.gif"  name="imgsalir"  onMouseOver="botonOver(this);" onClick="parent.close();" onMouseOut="botonOut(this);" style="cursor:hand"></div>
</form>
</div>
<iframe width=188 height=166 name="gToday:normal:agenda.js:gfPop:plugins.js" id="gToday:normal:agenda.js:gfPop:plugins.js" src="<%=BASEURL%>/js/Calendario/ipopeng.htm" scrolling="no" frameborder="0" style="visibility:visible; z-index:999; position:absolute; left:-500px; top:0px;"> </iframe>
</body>
</html>
