<!--
- Autor : Ing. David Lamadrid
- Date  : 5 de Octubre de 2005
- Copyrigth Notice : Fintravalores S.A. S.A
-->
<%--
-@(#)
--Descripcion : Pagina JSP, que se encarga de realizar la busqueda de tablas en el sistema
--%>

<%-- Declaracion de librerias--%>
<%@page contentType="text/html"%>
<%@page session="true"%>
<%@page errorPage="../error/ErrorPage.jsp"%>
<%@page import="java.util.*"%>
<%@page import="java.sql.*"%>
<%@page import="com.tsp.operation.model.*"%>
<%@page import="com.tsp.util.*"%>
<%@page import="com.tsp.operation.model.beans.*, java.util.*"%>
<%@include file="/WEB-INF/InitModel.jsp"%>
<html>
<head>
<link href="<%= BASEURL %>/css/estilostsp.css" rel="stylesheet" type="text/css">
<link href="../../../css/estilostsp.css" rel="stylesheet" type="text/css">

<title>Tablas de Usuario</title>
    
<script>
    function abrirVentanaBusq(an,al,url,pag) {
        window.open(url,'Trafico1','width='+an+',height='+al+',scrollbars=no,resizable=no,top=10,left=65');
    }
	
	function modificar(d,p,f, CONTROLLER){
		var url= CONTROLLER+"?estado=Despachom&accion=Modificar&sw=2&d="+d+"&p="+p+"&f="+f;
		abrirVentanaBusq(850,550,url);
	}
	
	function eliminar(codigo,CONTROLLER){
		var url= CONTROLLER+"?estado=TablasU&accion=Eliminar&sw=2&codigo="+codigo;
		location.href=url; 
	}
</script>
<script type='text/javascript' src="<%= BASEURL %>/js/boton.js"></script>
<script type='text/javascript' src="<%= BASEURL %>/js/reporte.js"></script>

</head>

<body onResize="redimensionar()" <%if(request.getParameter("reload")!=null){%>
        onLoad = "redimensionar();parent.close();parent.opener.location.href='<%=BASEURL%>/jsp/general/consultas/tablas.jsp?';" 
  <%} else {%>
       onLoad = "redimensionar();";
  <%}%>
>
<div id="capaSuperior" style="position:absolute; width:100%; height:100px; z-index:0; left: 0px; top: 0px;">
 <jsp:include page="/toptsp.jsp?encabezado=Tablas Asignadas a Usuarios"/>
</div>
<div id="capaCentral" style="position:absolute; width:100%; height:87%; z-index:0; left: 0px; top: 100px; overflow: scroll;">
<p>
<%  
	//model.tablasUsuarioService.listarTodo ();
    Vector vTablas = model.tablasUsuarioService.getVTablas ();
	TablasUsuario tablas;
%>

<table width="100%" border="0" bgcolor="#4D71B0">
  <tr > 
      <td>
	    <img src='<%=BASEURL%>/images/cuadrito.gif' width='9' height='9'>&nbsp;&nbsp;<a href="javascript:abrirVentanaBusq(850,550,'<%=BASEURL%>/jsp/general/consultas/insertar.jsp?sw=1')" class="encabezado">Insertar</a>
		<img src='<%=BASEURL%>/images/cuadrito.gif' width='9' height='9'>&nbsp;&nbsp;<a href="javascript:abrirVentanaBusq(850,550,'<%=BASEURL%>/jsp/general/consultas/tablas.jsp?sw=1&reload=1')"  class="encabezado">Restablecer</a>
	  </td>
  </tr>
</table>


</p>
</form>
<table width="556" border="2" align="center" id="tabla1" >
    <tr>
        <td width="544" >
           <table width="100%"  align="center">
              <tr>
                <td class="subtitulo1">Tablas Asignadas a Usuarios </td>
                <td class="barratitulo"><img src="<%=BASEURL%>/images/titulo.gif" width="32" height="20"></td>
              </tr>
          </table>
        
         <table id="tabla3" width="100%" border="1" borderColor="#999999" bgcolor="#F7F5F4">
                <tr class="tblTitulo" id="titulos">
                    <td width="57" nowrap align="center">Nombre</td>
                    <td width="130" nowrap align="center">Descripcion</td>
                    <td width="100" nowrap>Usuario</td>
                    <td width="224" align="center" nowrap>Fecha de Creaci�n</td>
                </tr>
<%     
            for (int i =0 ;i < vTablas.size();i++){
                tablas =(TablasUsuario)vTablas.elementAt(i);
%>     
            <tr class="<%=(i % 2 == 0 )?"filagris":"filaazul"%>" onMouseOver='cambiarColorMouse(this)'>
            <td  align="center" abbr="" nowrap class="bordereporte"><%=tablas.getNombre ()%></td>
            <td  align="center" abbr="" nowrap class="bordereporte"><%=tablas.getDescripcion()%></td>
            <td  abbr="" nowrap class="bordereporte"><%=tablas.getUsuario()%></td>
            <td  align="center" abbr="" nowrap class="bordereporte"><%=tablas.getCreation_date()%></td>
            <td width="181" align="center" class="bordereporte" style="cursor:hand"><img src='<%=BASEURL%>/images/delete.gif' name='Buscar' align="absmiddle" style='cursor:hand' title='Aplicar configuracion'  onclick="eliminar('<%=tablas.getCodigo()%>','<%=CONTROLLER%>');" ></td>
           </tr>
        <%}%>           
          </table> 
        </td>
    </tr>
</table>
<br>
<center><img src="<%=BASEURL%>/images/botones/salir.gif"  name="salir" title="Salir..." onClick="window.close();" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);"></center></div>
</body>
</html>
