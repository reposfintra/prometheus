<!--
- Autor : Ing. David Lamadrid
- Date  : 5 de Octubre de 2005
- Copyrigth Notice : Fintravalores S.A. S.A
-->
<%--
-@(#)
--Descripcion : Pagina JSP, que se encarga de gestionar 
                            el formulario de insercion y generacion de consultas genereados  por los usuarios
--%>

<%-- Declaracion de librerias--%>
<%@page contentType="text/html"%>
<%@page errorPage="/error/ErrorPage.jsp"%>
<%@page import="com.tsp.operation.model.beans.*, java.util.*"%>
<%@include file="/WEB-INF/InitModel.jsp" %>
<%@ taglib uri="http://jakarta.apache.org/taglibs/input-0.90" prefix="input" %>
<%
    Usuario usuarioLogin  = (Usuario) session.getAttribute("Usuario");
    String us  = usuarioLogin.getLogin();
%>

<html>
<head>
<title>Consultas Generales</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link href="../../trafico/css/estilostsp.css" rel="stylesheet" type="text/css"> 
<link href="<%=BASEURL%>/css/estilostsp.css" rel="stylesheet" type="text/css"> 
<script type='text/javascript' src="<%=BASEURL%>/js/Validacion.js"></script> 
<script type='text/javascript' src="<%= BASEURL %>/js/boton.js"></script>
<script>
function cargarConsulta(){
		document.forma1.action = "<%= CONTROLLER %>?estado=ConsultaU&accion=CargarConsulta"; 
        document.forma1.submit(); 
}

function eliminarConsulta(){
		document.forma1.action = "<%= CONTROLLER %>?estado=ConsultaU&accion=Eliminar"; 
        document.forma1.submit(); 
}

function insertarConsulta(){
	if(document.forma1.descripcion.value != ""){
		document.forma1.action = "<%= CONTROLLER %>?estado=ConsultaU&accion=InsertarConsulta"; 
		document.forma1.submit();
	}
	else{
		alert("Deve escribir una Descripcion para la Consulta Actual");
		document.forma1.descripcion.value.focus();
		return false;
	} 
}

function listarCampos(){
		document.forma1.action = "<%= CONTROLLER %>?estado=ConsultaU&accion=Campos"; 
        document.forma1.submit();
}

function validar2(f){	
	var option = document.getElementById("opcion");
	option.value="2";
	generarConsulta(f);
}
function generarConsulta(BASEURL){
		document.forma1.action = "<%= CONTROLLER %>?estado=ConsultaU&accion=Consulta"; 
        document.forma1.submit(); 
}

function restablecer(BASEURL){
		document.forma1.action = "<%= CONTROLLER %>?estado=ConsultaU&accion=Ingresar"; 
        document.forma1.submit(); 
}
function agregarCampo(objeto){
	if(existeEnSelect(objeto.value)==false){	
		var tablaSelect = document.getElementById("tablaSelect");
		var clon = objeto.cloneNode();
		var fila = tablaSelect.insertRow(tablaSelect.rows.length);
		fila.id= objeto.value;
		fila.className ="fila";
		var celda2=	fila.insertCell();
		celda2.appendChild(clon);
		clon.id="cselect"
		clon.name="cselect"
		clon.checked=true;
		clon.onclick = new Function("eliminarUnSelect('"+objeto.value+"')") 
		var celda =	fila.insertCell();
		celda.innerHTML=objeto.value;
		agregarCadenaSelect(objeto.value)
	}
	else{
		eliminarUnSelect(objeto.value);
	}		
}

function existeEnSelect(campo){
	var tablaSelect = document.getElementById("tablaSelect");
	var existe=false;
	for(var i=0;i<tablaSelect.rows.length;i++){
		if(tablaSelect.rows[i].id==campo){
			existe =true;
		}
	}
	return existe;
}

function agregarCadenaSelect(campo){
	var from = document.getElementById("select");
	if(from.value!= ""){
		from.value=from.value+","+campo
	}
	else{
		from.value ="select "+campo;
	}
}

function eliminarUnSelect(campo){
	var tablaSelect = document.getElementById("tablaSelect");
	var tablaCampos = document.getElementById("tablaCampos");
	
	for(var ii=0;ii<tablaCampos.rows.length;ii++){
		var campoS=document.getElementById("campoS"+ii);
		if(campoS.value==campo){
			campoS.checked=false;
		}
	}

	eleminarCadenaSelect(campo);
	
	for(var i=0;i<tablaSelect.rows.length;i++){
		if(tablaSelect.rows[i].id==campo){
			tablaSelect.deleteRow(i);
		}
	}
}

function eleminarCadenaSelect(cadena){
	var from = document.getElementById("select");
	var pos=from.value.indexOf(cadena);
	var tablas=from.value;
	var temp2
	var temp1
	if (pos ==7){
		temp1=tablas.substring(0,pos-1);
		temp2=tablas.substring((pos+cadena.length+1),tablas.length);
		from.value=temp1+" "+temp2;
	}
	else{
		temp1=tablas.substring(0,pos-1);
		temp2=tablas.substring((pos+cadena.length),tablas.length);
		from.value=temp1+temp2;
	}
	var tabla = document.getElementById("tablaSelect");
	if(tabla.rows.length==1){
		from.value="";
	}
}

function eliminarUnFrom(campo){
	var tablaFrom = document.getElementById("tablaFrom");
	var tablaSelect = document.getElementById("tablaSelect");
	eliminarCadenaFrom(campo)
	for(var i=0;i<tablaFrom.rows.length;i++){
	    var id =tablaFrom.rows[i].id.substring(2,tablaFrom.rows[i].id.length);
		//alert(id) 
		if(id==campo){
			var letra=tablaFrom.rows[i].id.substring(0,1);
			
			for(var ii=0;ii<tablaSelect.rows.length;ii++){
				//alert(tablaSelect.rows[ii].id);
				var id2=tablaSelect.rows[ii].id.substring(0,1);
				alert (letra)
				alert(id2);
				if(letra==id2){
					alert("va a eliminar "+letra+"."+tablaSelect.rows[ii].id);
					eliminarUnSelect(tablaSelect.rows[ii].id);
					ii=ii-1;				
				}	
			}
			tablaFrom.deleteRow(i);
		}
	} 
}

function eliminarCadenaFrom(cadena){
	cadena;
	var from = document.getElementById("from");
	//alert(from.value);
	var pos=from.value.indexOf(cadena);
	
	//alert(cadena);
	//alert(pos);
	var tablas=from.value;
	var temp2
	var temp1
	if (pos ==5){
		temp1=tablas.substring(0,pos-1);
		temp2=tablas.substring((pos+cadena.length+1+5),tablas.length);
		from.value=temp1+" "+temp2;
	}
	else{
		temp1=tablas.substring(0,pos-1);
		temp2=tablas.substring((pos+cadena.length+5),tablas.length);
		from.value=temp1+temp2;
	}
	
	var tabla = document.getElementById("tablaFrom");
	if(tabla.rows.length==1){
		from.value="";
	}
}

</script>

<link href="/css/estilostsp.css" rel="stylesheet" type="text/css">
</head>

<body onresize="redimensionar()" onload = 'redimensionar()'>
<div id="capaSuperior" style="position:absolute; width:100%; height:100px; z-index:0; left: 0px; top: 0px;">
 <jsp:include page="/toptsp.jsp?encabezado=Consultas Generales"/>
</div>
<div id="capaCentral" style="position:absolute; width:100%; height:87%; z-index:0; left: 0px; top: 100px; overflow: scroll;">
<%	


TreeMap tTablas;
tTablas = model.tablasUsuarioService.getTTabla();

TreeMap tConsultas;
tConsultas = model.consultaUsuarioService.getTConsultas ();

Vector vFrom = new Vector();
vFrom=model.consultaUsuarioService.getVFrom();

Vector vSelect =new Vector();
vSelect=model.consultaUsuarioService.getVSelect();
 
Vector vCampos=model.consultaUsuarioService.getVCampos ();

String select = (request.getParameter("select")==null?"":request.getParameter("select"));
String from   = (request.getParameter("from")==null?"":request.getParameter("from"));
String where  = (request.getParameter("where")==null?"":request.getParameter("where"));
String otros  = (request.getParameter("otros")==null?"":request.getParameter("otros"));
String nombre_tabla=(request.getParameter("nombre_tabla")==null?"":request.getParameter("nombre_tabla"));
String letra = model.consultaUsuarioService.obtenerLetra(nombre_tabla);

/*
 
//String sw =""+request.getParameter("sw");

/*

Vector vSelect = new Vector();
vCampos = new Vector();*/
//out.println("vFrom  "+vFro*m.size());
//System.out.println("bandera 1 en pagina");
/*  if(!sw.equals("1")){


}*/
//System.out.println("bandera 2 en pagina ");
//out.println("<br>vForm"+vFrom.size());

/*
String accion = ""+request.getParameter("accion");
if(accion.equals("null") || accion.equals("")){
	 
}*/
%>
<form name="forma1" id="forma1" action="<%=CONTROLLER%>?estado=Historico&accion=Insertar" method="post" onSubmit="return validar();">      
<input type="hidden" id="opcion" name="opcion" value="">
<table width="90%" border="2" align="center">
<tr>
	<td >
        <table width="100%" align="center">
		<tr>
		<td  class="subtitulo1" colspan="2"> Tablas por Usuario
		</td>
		<td  class="barratitulo" ><img src="<%=BASEURL%>/images/titulo.gif"></td>
		</tr>
          <tr class="fila"> 
            <td  >Listas de Tablas   : </td>
            <td  >Lista de campos de <%=nombre_tabla%> <input type="hidden" name="nombre_tabla" id="nombre_tabla" value="<%=nombre_tabla%>"></td>
			<td >Seleccion </td>
          </tr>
          <tr class="fila"> 
          <td >
			    <input:select name="c_tabla"      attributesText="<%= "multiple size='12' id='c_tabla'  onChange='listarCampos();' style='width:150px;' class='textbox'  " %>"  default="010" options="<%=tTablas%>" /> 
			</td>
          <td  >
		     <div class="scroll" style=" overflow:auto ; WIDTH: 100%; HEIGHT:160px; " >
	      <table width="263" id="tablaCampos" >
			<% 
				if ((vCampos !=null) && (!letra.equals(""))){
				for (int i=0;i < vCampos.size();i++){
				 Vector fila=(Vector)vCampos.elementAt(i);
				 String checked="";
				 if(model.consultaUsuarioService.existeSelectI(letra+"."+fila.elementAt(0))==true){
				 	checked="checked";
				 }
				 String nombre=""+fila.elementAt(0);
			%>
			<tr class="fila" > 
			<td width="251"  id="<%=fila.elementAt(0)%>" nowrap ><input type="checkbox" onClick="agregarCampo(this);"  value="<%=letra+"."+nombre%>" name="campos<%=i%>" id="campos<%=i%>" <%=checked%>><%=letra+"."+nombre%>
			</td>
			</tr>
			<%}
			}%>
			</table>
			</div>
			 </td>
			<td>			   <div class="scroll" style=" overflow:auto ; WIDTH: 100%; HEIGHT:100px; " >
			   <table id="tablaSelect" > 
			    <%
			   if(vSelect!=null){
			     for(int i=0;i<vSelect.size();i++){%>
			   <tr class="fila" id="<%=vSelect.elementAt(i)%>" ><td><input type="checkbox" onClick="eliminarUnSelect('<%=vSelect.elementAt(i)%>');" name="cselect" id="cselect" value="<%=vSelect.elementAt(i)%>" checked></td><td><%=vSelect.elementAt(i)%></td></tr>
			   <%}}
			   %>
			   </table>
</div>	
			   <textarea name="select" cols="50" rows="6"  class="text" id="select"><%=select%></textarea></td>
          </tr>
			<tr class="fila"> 
            <td  >Listas de Consultas : </td>
              <td >tablas:</td>
              <td>From</td>
			</tr>
			<tr class="fila"> 
            <td nowrap>
			<input:select name="tConsultas"      attributesText="<%= "multiple size='5' id='cConfiguraciones' style='width:100%;'  class='textbox'  " %>"  default="010" options="<%=tConsultas%>" /> 
			 <div align="center"><img src='<%=BASEURL%>/images/botones/eliminar.gif' name='Buscar' align="absmiddle" style='cursor:hand' title='Aceptar...'  onclick="eliminarConsulta();" onMouseOver='botonOver(this);' onMouseOut='botonOut(this);'>
                 <img src='<%=BASEURL%>/images/botones/aceptar.gif' name='Buscar' align="absmiddle" style='cursor:hand' title='Aceptar...'  onclick="cargarConsulta();" onMouseOver='botonOver(this);' onMouseOut='botonOut(this);'> </div></td>
              <td width="285" >
			      <div class="scroll" style=" overflow:auto ; WIDTH: 100%; HEIGHT:100px; " >
			   <table id="tablaFrom" > 
			   <%
			     if(vFrom != null){
			   	 for(int i=0;i<vFrom.size();i++){
			   		Vector fila =(Vector)vFrom.elementAt(i);
			   %>
			   <tr class="fila" id="<%=fila.elementAt(1)+"."+fila.elementAt(0)%>"><td><input type="checkbox" onClick="eliminarUnFrom(this.value);" name="cfrom" id="cfrom" value="<%=fila.elementAt(0)%>" checked><%=fila.elementAt(0)%></td></tr>
			   <%}}%>
			   </table>
			   </div>
			  </td>
			<td><textarea name="from" cols="50" rows="3" id="from"><%=from%></textarea></td>
			</tr>
			<tr class="fila"> 
            <td  >&nbsp;</td>
            <td  >Otros(Order by):</td>
			  <td  >where:</td>
			</tr>
			<tr class="fila"> 
            <td  ><p>Guardar Consulta Actual:</p>
              <p>
                <input type="text" name="descripcion" id="descripcion"> 
              </p>
              <p align="center"><img src='<%=BASEURL%>/images/botones/aceptar.gif' name='Buscar' align="absmiddle" style='cursor:hand' title='Aceptar...'  onclick="insertarConsulta();" onMouseOver='botonOver(this);' onMouseOut='botonOut(this);'> </p></td>
            <td ><textarea name="otros" cols="40" rows="5" id="otros" ><%=otros%></textarea></td>
			<td><textarea name="where" cols="50" rows="5" id="where"><%=where%></textarea></td>
			</tr>
        </table>
</td>
</tr>
</table>
 
 <br>
  <div align="center">
      <img src='<%=BASEURL%>/images/botones/aceptar.gif' name='Buscar' align="absmiddle" style='cursor:hand' title='Aceptar...'  onclick="generarConsulta('<%=BASEURL%>');" onMouseOver='botonOver(this);' onMouseOut='botonOut(this);'> &nbsp;&nbsp;
    <img src='<%=BASEURL%>/images/botones/exportarExcel.gif' name='Buscar' align="absmiddle" style='cursor:hand' title='Aceptar...'  onclick="validar2('<%=BASEURL%>');" onMouseOver='botonOver(this);' onMouseOut='botonOut(this);'>&nbsp; <img src='<%=BASEURL%>/images/botones/restablecer.gif' name='Buscar' align="absmiddle" style='cursor:hand' title='Aceptar...'  onclick="restablecer('<%=BASEURL%>');" onMouseOver='botonOver(this);' onMouseOut='botonOut(this);'>&nbsp;<img src='<%=BASEURL%>/images/botones/salir.gif' name='Buscar' align="absmiddle" style='cursor:hand' title='Salir...'  onclick="parent.close();" onMouseOver='botonOver(this);' onMouseOut='botonOut(this);'>       </div>
  <p>&nbsp;</p> 
</form>

<%
  String mensaje=""+request.getParameter("ms");
    if (! (mensaje.equals("")|| mensaje.equals("null"))){
%>
<table border="2" align="center">
  <tr>
    <td><table width="410" height="20" border="1" align="center"  bordercolor="#F7F5F4" bgcolor="#FFFFFF">
        <tr>
          <td width="282" align="center" class="mensajes"><%=mensaje%></td>
          <td width="28" background="<%=BASEURL%>/images/cuadronaranja.JPG">&nbsp;</td>
          <td width="78">&nbsp;</td>
        </tr>
    </table></td>
  </tr>
</table>
<%
    }   %>
</div>
</body>
</html>

