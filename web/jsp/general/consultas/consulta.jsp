<!--
- Autor : Ing. David Lamadrid
- Date  : 5 de Octubre de 2005
- Copyrigth Notice : Fintravalores S.A. S.A
-->
<%--
-@(#)
--Descripcion : Pagina JSP, que se encarga de realizar el reporte de consultas generados por los usuarios
--%>

<%-- Declaracion de librerias--%>
<%@page contentType="text/html"%>
<%@page session="true"%>
<%@page errorPage="../error/ErrorPage.jsp"%>
<%@page import="java.util.*"%>
<%@page import="java.sql.*"%>
<%@page import="com.tsp.operation.model.*"%>
<%@page import="com.tsp.util.*"%>
<%@page import="com.tsp.operation.model.beans.*, java.util.*"%>
<%@include file="/WEB-INF/InitModel.jsp"%>
<html>
<head>
<link href="<%= BASEURL %>/css/estilostsp.css" rel="stylesheet" type="text/css">
<link href="../../../css/estilostsp.css" rel="stylesheet" type="text/css">

<title>Tablas de Usuario</title>
    
<script>
	function regresar(BASEURL){
		var url= BASEURL+"/jsp/general/consultas/insertarconsultas.jsp?accion=1";
		location.href=url; 
	}
</script>
<script type='text/javascript' src="<%= BASEURL %>/js/boton.js"></script>
<script type='text/javascript' src="<%= BASEURL %>/js/reporte.js"></script>

</head>

<body onResize="redimensionar()" <%if(request.getParameter("reload")!=null){%>
        onLoad = "redimensionar();parent.close();parent.opener.location.href='<%=BASEURL%>/jsp/general/consultas/tablas.jsp?';" 
  <%} else {%>
       onLoad = "redimensionar();";
  <%}%>
>
<div id="capaSuperior" style="position:absolute; width:100%; height:100px; z-index:0; left: 0px; top: 0px;">
 <jsp:include page="/toptsp.jsp?encabezado=Consulta"/>
</div>
<div id="capaCentral" style="position:absolute; width:100%; height:87%; z-index:0; left: 0px; top: 100px; overflow: scroll;">
<p>
<% 
    Vector vConsulta= model.consultaUsuarioService.getVConuslta ();
//	Vector vCampos = model.consultaUsuarioService.getVCampos ();
	Vector vCampos =model.consultaUsuarioService.getVTitulos ();
	// vCampos = model.consultaUsuarioService.encabesado (vCampos);
%>

<table width="100%" border="0" bgcolor="#4D71B0">
  <tr > 
      <td>
	    <img src='<%=BASEURL%>/images/cuadrito.gif' width='9' height='9'>&nbsp;&nbsp;<a href="javascript:abrirVentanaBusq(850,550,'<%=BASEURL%>/jsp/general/consultas/insertar.jsp?sw=1')" class="encabezado">Insertar</a>
	  </td>
  </tr>
</table>


</p>
</form>
<table  border="2" align="center" id="tabla1" >
    <tr>
        <td width="" >
           <table width="100%"  align="center">
              <tr>
                <td class="subtitulo1">Consulta</td>
                <td width="54%" class="barratitulo"><img src="<%=BASEURL%>/images/titulo.gif" width="32" height="20"></td>
              </tr>
          </table>
        
         <table id="tabla3" width="100%" border="1" borderColor="#999999" bgcolor="#F7F5F4">
                <tr class="tblTitulo" id="titulos">
                    <% for (int i=0;i< vCampos.size();i++){
					%>
					<td  nowrap align="center"><%=vCampos.elementAt(i)%></td>
                    <%}%>
                </tr>
<%     
            for (int i =0 ;i < vConsulta.size();i++){
                Vector fila =(Vector)vConsulta.elementAt(i);
%>     
            <tr class="<%=(i % 2 == 0 )?"filagris":"filaazul"%>" onMouseOver='cambiarColorMouse(this)'>
<%
				for ( int x=0;x<fila.size();x++){
%>
            <td  align="center" abbr="" nowrap class="bordereporte"><div align="left"><%=fila.elementAt(x)%></div></td>
           
		<%}%>   
		   </tr>
        <%}%>           
          </table> 
        </td>
    </tr>
</table>
<br>
<center>
<img src="<%=BASEURL%>/images/botones/salir.gif"  name="salir" title="Salir..." onClick="window.close();" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);">
<img src="<%=BASEURL%>/images/botones/regresar.gif"  name="salir" id="salir" title="Regresar..." onClick="regresar('<%=BASEURL%>');" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);">
</center></div>
</body>
</html>
