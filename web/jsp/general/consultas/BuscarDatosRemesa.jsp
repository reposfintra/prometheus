<%@page session="true"%>
<%@page import="java.util.*" %>
<%@page import="com.tsp.operation.model.*"%>
<%@include file="/WEB-INF/InitModel.jsp"%>
<%@page import="com.tsp.operation.model.beans.*"%>
<%@page import="com.tsp.util.*"%>
<%@ taglib uri="http://jakarta.apache.org/taglibs/input-0.90" prefix="input" %>
<html>
<head>  
<title>Buscar Remesas</title> 
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link href="<%=BASEURL%>/css/estilostsp.css" rel="stylesheet" type="text/css">
<link href="../../../../css/estilostsp.css" rel="stylesheet" type="text/css"> 
<script language="JavaScript" type="text/javascript" src="<%=BASEURL%>/js/validar.js"></script>
<script type='text/javascript' src="<%= BASEURL %>/js/boton.js"></script>
<script type='text/javascript' src="<%= BASEURL %>/js/ingreso.js"></script>
</head>
<body onResize="redimensionar();" onLoad='redimensionar();' > 

<%
	String mensaje = (request.getParameter("msg")!= null)? request.getParameter("msg") : "";
	Vector vec = model.remesaService.getRemesas();
	java.util.Date date = new java.util.Date();
    java.text.SimpleDateFormat s = new java.text.SimpleDateFormat("yyyy-MM-dd");
	String fechareporte =s.format(date); 
%>
<script>
	function cambiacolor(element){
		var tr = element.parentNode.parentNode;
		tr.className = (element.checked?'filaazul':'filagris');
	}
	function seleccionartodo(size,check){
		if(check == true){
			for (i=0;i<size;i++){
				var checkbox="checkbox"+i;
				var textfield="textfield"+i
				document.getElementById(checkbox).checked=true;
				cambiacolor(document.getElementById(checkbox));
			}    
		}else{
			for (i=0;i<size;i++){
				var checkbox="checkbox"+i;
				var textfield="textfield"+i
				document.getElementById(checkbox).checked=false;
				cambiacolor(document.getElementById(checkbox));
			}  
		}
	}
	function validFechas(){
        var fecha1 = form1.fechaInicio.value.replace('-','').replace('-','');
        var fecha2 = form1.fechaFinal.value.replace('-','').replace('-','');
        var fech1 = parseFloat(fecha1);
        var fech2 = parseFloat(fecha2);
        if( form1.fechaFinal.value == '' || form1.fechaInicio.value == ''){
			alert('No debe dejar las fechas vacias');
        	return (false);
		}
		if( form1.cliente.value == ''){
			alert('El campo del cliente no puede ser vacio');
        	return (false);
		}
		if( fech1 > fech2 ) {     
        	alert('La fecha final debe ser mayor que la fecha inicial');
        	return (false);
        }  
        form1.submit();
	}     
</script>

<div id="capaSuperior" style="position:absolute; width:100%; height:100px; z-index:0; left: 0px; top: 0px;">
 <jsp:include page="/WEB-INF/toptsp.jsp?encabezado=Aplicar Pagador Remesas"/>
</div>
<div id="capaCentral" style="position:absolute; width:100%; height:87%; z-index:0; left: 0px; top: 100px; overflow: scroll;">
<FORM ACTION="<%=CONTROLLER%>?estado=ConsultaRemesa&accion=Pagador&listar=True" METHOD='post' id='form1' name='form1'>
<%
if( vec == null ){
%>
  <table width="400" border="2" align="center">
    <tr>
      <td><table width="100%"  border="0">
        <tr>
          <td width="50%" class="subtitulo1">&nbsp;Filtro Cliente </td>
          <td width="50%" class="barratitulo"><img src="<%=BASEURL%>/images/titulo.gif" width="32" height="20"></td>
          </tr>
      </table>      <table width="100%" class="tablaInferior">
		  <tr class="fila">
		  <td width="149">Cliente:</td>
		  <td colspan="3"><input name="cliente" type="text" class="textbox" id="cliente" onKeyPress="soloAlfa(event)" onBlur="if(this.value!=''){completarCodigo();}"  size="10" maxlength="6" >
		    <span class="Simulacion_Hiper" style="cursor:hand " onClick="window.open('<%=BASEURL%>/consultas/consultasClientes.jsp','','HEIGHT=200,WIDTH=600,SCROLLBARS=YES,RESIZABLE=YES,STATUS=NO')">Buscar Clientes</span></td>
		  </tr>
        <tr class="fila">
          <td>Fecha Remesa:</td>
          <td width="104">&nbsp;<input  name='fechaInicio' size="11" readonly="true" class="textbox" value='<%=fechareporte%>'>
            <a href="javascript:void(0)" onClick="if(self.gfPop)gfPop.fPopCalendar(fechaInicio);return false;" hidefocus><img name="popcal" align="absmiddle" src="<%=BASEURL%>/js/calendartsp/calbtn.gif" width="16" height="16" border="0" alt=""></a></td>
          <td width="119" colspan="2">&nbsp;<input  name='fechaFinal' size="11" readonly="true" class="textbox" value='<%=fechareporte%>'>
            <a href="javascript:void(0)" onClick="if(self.gfPop)gfPop.fPopCalendar(fechaFinal);return false;" hidefocus><img name="popcal2" align="absmiddle" src="<%=BASEURL%>/js/calendartsp/calbtn.gif" width="16" height="16" border="0" alt=""></a></td>
          </tr>
		  <tr class="fila">
		  <td>Remesas no Cumplidas:</td>
		  <td colspan="3"><input type="checkbox" name="cumplida" value="checkbox"></td>
		  </tr>
      </table></td>
    </tr>
  </table>
 <br>
  <div align="center">
  		<img src="<%=BASEURL%>/images/botones/buscar.gif"  name="imgbuscar" onClick="validFechas();" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" style="cursor:hand">&nbsp;
  		<img src="<%=BASEURL%>/images/botones/salir.gif"  name="imgsalir"  onMouseOver="botonOver(this);" onClick="parent.close();" onMouseOut="botonOut(this);" style="cursor:hand"></div>
<%}else{%>


<table width="350" border="2" align="center">
    <tr>
      <td><table width="100%"  border="0">
        <tr>
          <td width="50%" class="subtitulo1">&nbsp;Buscar Pagador</td>
          <td width="50%" class="barratitulo"><img src="<%=BASEURL%>/images/titulo.gif" width="32" height="20"></td>
          </tr>
      </table>      <table width="100%" class="tablaInferior">
		  <tr class="fila">
		  <td width="109">Codigo Pagador:</td>
		  <td width="217">
		    <input name="pagador" type="text" class="textbox" id="cliente" onKeyPress="soloAlfa(event)" onBlur="if(this.value!=''){completarCodigo();}"  size="10" maxlength="6" >
            <span class="Simulacion_Hiper" style="cursor:hand " onClick="window.open('<%=BASEURL%>/consultas/consultasClientes.jsp','','HEIGHT=200,WIDTH=600,SCROLLBARS=YES,RESIZABLE=YES,STATUS=NO')">Buscar Pagador</span></td>
		  </tr>
      </table></td>
    </tr>
  </table>
 <br>
<table width="98%" border="1" align="center">
	<tr>
		<td>  
			<table width="100%" align="center">
				  <tr>
					<td width="373" class="subtitulo1">&nbsp;Listado de Remesas </td>
					<td width="427" class="barratitulo"><img src="<%=BASEURL%>/images/titulo.gif" width="32" height="20"></td>
				  </tr>
			</table>                      
			<table width="100%" border="1" bordercolor="#999999" bgcolor="#F7F5F4">
				<tr class="tblTitulo">
					<td width="5%"  align="center"><input type="checkbox" name="checkboxall" id ="checkboxall" value="checkbox" onClick="seleccionartodo('<%=vec.size()%>',this.checked);"></td>                        
					<td width="8%" align="center"> Remesa</td>
					<td width="21%" align="center">Cliente</td>
					<td width="10%" align="center">Origen</td>
					<td width="10%" align="center">Destino</td>
					<td width="8%" align="center">Estandar</td>
					<td width="12%" align="center">Estado</td>
					<td width="26%" align="center">Pagador</td>			
				</tr> 
				<%
				  for (int i = 0; i < vec.size(); i++)
				  {
					 Remesa rem = (Remesa) vec.get(i);%>
					
						<tr class="filagris" >
							<td align="center" class="bordereporte"><input type="checkbox" id="checkbox<%=i%>" name="checkbox" value="<%=i%>" onClick="cambiacolor(this);"></td>
							<td align="center" class="bordereporte"><%=rem.getNumrem()%></td>
							<td align="center" class="bordereporte"><%=rem.getNombre_cli()%></td>
							<td align="center" class="bordereporte"><%=rem.getOrirem()%></td>						
							<td align="center" class="bordereporte"><%=rem.getDesrem()%></td>	
							<td align="center" class="bordereporte"><%=rem.getStdJobNo()%></td>	
							<td align="center" class="bordereporte"><%=rem.getEstado()%></td>
							<td align="center" class="bordereporte"><%=rem.getPagador().equals("")?"&nbsp;":rem.getPagador()%></td>	
						</tr>				
				<%}%>
		  </table>
		</td>
	</tr>
</table> 




 <br>
  <div align="center"><img src="<%=BASEURL%>/images/botones/aceptar.gif"  name="imgbuscar" onClick=" if( cliente.value != '' ){form1.action = '<%=CONTROLLER%>?estado=ConsultaRemesa&accion=Pagador&listar=False';form1.submit();}else{alert('Debe llenar el pagador');cliente.focus();}" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" style="cursor:hand">&nbsp;
  <img src="<%=BASEURL%>/images/botones/salir.gif"  name="imgsalir"  onMouseOver="botonOver(this);" onClick="parent.close();" onMouseOut="botonOut(this);" style="cursor:hand">&nbsp;
  <img src="<%=BASEURL%>/images/botones/regresar.gif"  name="imgregresar"  onMouseOver="botonOver(this);" onClick="window.location = '<%=CONTROLLER%>?estado=ConsultaRemesa&accion=Pagador&listar=Back';" onMouseOut="botonOut(this);" style="cursor:hand"></div>
<%} if( !mensaje.equals("") ){ %> 
	<br>
	<table border="2" align="center">
		<tr>
			<td><table width="100%"  border="1" align="center"  bordercolor="#F7F5F4" bgcolor="#FFFFFF">
				<tr>
					<td width="229" align="center" class="mensajes"><%=mensaje%></td>
					<td width="29" background="<%=BASEURL%>/images/cuadronaranja.JPG">&nbsp;</td>
					<td width="58">&nbsp;</td>
				</tr>
			</table></td>
		</tr>
	</table>
 <%}%>
</form>
</div>
<iframe width=188 height=166 name="gToday:normal:agenda.js:gfPop:plugins.js" id="gToday:normal:agenda.js:gfPop:plugins.js" src="<%=BASEURL%>/js/Calendario/ipopeng.htm" scrolling="no" frameborder="0" style="visibility:visible; z-index:999; position:absolute; left:-500px; top:0px;"> </iframe>
</body>
</html>
