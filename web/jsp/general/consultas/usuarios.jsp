<!--
- Autor : Ing. David Lamadrid
- Date  : 5 de Octubre de 2005
- Copyrigth Notice : Fintravalores S.A. S.A
-->
<%--
-@(#)
--Descripcion : Pagina JSP, que se encarga de realizar y mostrar la busqueda de usuarios del sistema
--%>

<%-- Declaracion de librerias--%>
<%@page contentType="text/html"%>
<%@page session="true"%>
<%@page import="java.util.*"%>
<%@page import="com.tsp.operation.model.*"%>
<%@page import="com.tsp.util.*"%>
<%@page import="com.tsp.operation.model.beans.*, java.util.*"%>
<%@include file="/WEB-INF/InitModel.jsp"%>
<%@taglib uri="http://jakarta.apache.org/taglibs/input-0.90" prefix="input" %>
<html>
<%//System.out.println("BANDERA");%>
<head>
<script type='text/javascript' src="<%= BASEURL %>/js/boton.js"></script>
<script type='text/javascript' src="<%= BASEURL %>/js/reporte.js"></script>
<script language="javascript">
    
	function asignar(id,nombre){
		var campo = parent.opener.document.forma1.c_usuario;
		campo.value=""+id;
		var celda = parent.opener.document.getElementById("td_nombre");
		celda.innerHTML= ""+nombre;
		parent.close() ;
	}
	
    function procesar (element){
      if (window.event.keyCode==13) 
        listaCliente();
    }
	
	function listaUsuarios(){
		document.forma1.action = "<%=CONTROLLER%>?estado=TablasU&accion=BUsuario&sw=1"; 
        document.forma1.submit(); 
    }
</script>

<title>Buscar Cliente</title>
<link href="<%= BASEURL %>/css/estilostsp.css" rel="stylesheet" type="text/css">
<link href="../../../css/estilostsp.css" rel="stylesheet" type="text/css">
</head>

<body onresize="redimensionar()" onload = 'forma1.c_nombre.focus(); redimensionar()'>
<div id="capaSuperior" style="position:absolute; width:100%; height:100px; z-index:0; left: 0px; top: 0px;">
 <jsp:include page="/toptsp.jsp?encabezado=Buscar Usuario"/>
</div>
<div id="capaCentral" style="position:absolute; width:100%; height:87%; z-index:0; left: 0px; top: 100px; overflow: scroll;">
<%

 //System.out.println("BANDERA");
  String accion = "" +request.getParameter("accion");
  //out.println(accion);
 
%>
<form name="forma1" action="" method="post">      
    <table border="2" align="center" width="555">
        <tr>
            <td>
                <table width="100%" class="tablaInferior">
                    <tr>
                        <td width="130" class="subtitulo1"><p align="left">Usuario</p></td>
                        <td width="405"  class="barratitulo"><img src="<%=BASEURL%>/images/titulo.gif" width="32" height="20"></td>
                    </tr>
                </table>        
                <table width="547" border="0" align="center" class="tablaInferior">
                    <tr class="fila">
                        
            <td width="131" height="30"> Nombre del Usuario: </td>
                      <td width="406" > 
                            <input name="c_nombre" type="text" class="textbox" id="c_nombre" size="18" maxlength="15"   onKeyUp="procesar(this);">
                            <img src='<%=BASEURL%>/images/botones/buscar.gif' name='Buscar' align="absmiddle" style='cursor:hand' title='Buscar'  onClick="listaUsuarios();"  onMouseOver='botonOver(this);' onMouseOut='botonOut(this);'> </td>
                    </tr>
              </table>
            </td>
        </tr>
    </table>

<%


if (accion.equals("1")){
  //  out.println("hola");
    Vector vUsuarios= model.usuarioService.getUsuarios ();
	if(vUsuarios!= null){
%><br>
  <table width="555" border="2" align="center">
    <tr>
    <td width="556">
<table width="100%" class="tablaInferior">
          <tr>
    <td width="130" height="24"  class="subtitulo1"><p align="left">Lista de Usuarios </p></td>
    <td width="408"  class="barratitulo"><img src="<%=BASEURL%>/images/titulo.gif" width="32" height="20"></td>
  </tr>
</table>
        <table width="551" border="1" borderColor="#999999" bgcolor="#F7F5F4" align="center">
          <tr class="tblTitulo" id="titulos">
            <td width="79" height="22">Login</td>
            <td width="456" >nombre</td>
    </tr>
<%
        for(int i=0; i< vUsuarios.size();i++){
            Usuario usuario=(Usuario)vUsuarios.elementAt(i);
%>  
  <tr class="<%=(i % 2 == 0 )?"filagris":"filaazul"%>" onMouseOver='cambiarColorMouse(this)' style="cursor:hand">
    <td width="79" class="bordereporte"><%=usuario.getLogin()%></td>
    <td width="456" class="bordereporte"><a  onClick="asignar('<%=usuario.getLogin()%>','<%=usuario.getNombre()%>')" style="cursor:hand" ><%=usuario.getNombre()%></a></td>
    </tr>
<%
        }
		}
    }
%>  
</table>
</td>
</tr>
</table>
<br>
<table width="555" border="0" cellspacing="0" cellpadding="0" align="center">
  <tr>
    <td><img src="<%=BASEURL%>/images/botones/salir.gif"  name="salir" title="Salir..." onClick="window.close();" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);"></td>
  </tr>
</table>

</form>
</div>
</bodY>
</html>
