<!--
- Autor : Ing. ricardo Rosero
- Date  : 23 de Diciembre de 2005
- Copyrigth Notice : Fintravalores S.A. S.A
-->

<%--
-@(#)
--Descripcion : Pagina JSP, que retorna un los datos de un registro de la tabla tbl_general_prog
--%>

<%@page contentType="text/html"%>
<%@page session="true"%>
<%@page errorPage="/error/ErrorPage.jsp"%>
<%@page import="java.util.*"%>
<%@page import="com.tsp.operation.model.*"%>
<%@page import="com.tsp.operation.model.services.*"%>
<%@page import="com.tsp.util.*"%>
<%@page import="com.tsp.operation.model.beans.*, java.util.*"%>
<%@include file="/WEB-INF/InitModel.jsp"%>
<%@ taglib uri="http://jakarta.apache.org/taglibs/input-0.90" prefix="input" %>
<html>
<head>
<title>Midificar TablaGen Programa</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link href="<%= BASEURL %>/css/estilostsp.css" rel="stylesheet" type="text/css">
<script type='text/javascript' src="<%= BASEURL %>/js/boton.js"></script>
<link href="../../../css/estilostsp.css" rel="stylesheet" type="text/css"> 
<script type='text/javascript' src="<%= BASEURL %>/js/reporte.js"></script> 
</head>
<% String Mensaje       = (request.getParameter("Mensaje")!=null)?request.getParameter("Mensaje"):""; %>
<body>
<div id="capaSuperior" style="position:absolute; width:100%; height:100px; z-index:0; left: 0px; top: 0px;">
	<jsp:include page="/WEB-INF/toptsp.jsp?encabezado=Anular TablaGen Programa"/>
</div>
<div id="capaCentral" style="position:absolute; width:100%; height:87%; z-index:0; left: 0px; top: 100px; overflow: scroll;">
<%       	   
    TblGeneralProg tg = model.tbl_GeneralProgService.getTblGeneralProg(); 
%>
    <form name="forma" method="post" action="" >
      <table width="350" border="2" align="center">
        <tr>
          <td><table width="100%"  border="0">
            <tr>
              <td width="48%" class="subtitulo1">TablaGen Programa</td>
              <td width="52%" class="barratitulo"><img src="<%=BASEURL%>/images/titulo.gif"></td>
            </tr>
          </table>
          <table width="100%" class="tablaInferior">
           
         <tr >
            <td width="48%" class="fila">Codigo Tabla</td>
		<td width="52%" class="letra"><%= tg.gettable_type() %></td>
		</tr>
		<tr>
            <td class="fila">Codigo</td>
			<td class="letra"><%= tg.gettable_code() %>
			</td>
		</tr>
		<tr >
            <td class="fila">Programa</td>
			<td class="letra"><%=tg.getprogram()%></td></tr>
         </table></td>
       </tr>
     </table>    
            
          </table></td>
        </tr>
      </table>
     <br>
      <div align="center">
			   <img src="<%=BASEURL%>/images/botones/anular.gif"  name="imgaceptar" onMouseOver="botonOver(this);" onClick="window.location = '<%=CONTROLLER%>?estado=TblGeneralProg&accion=Modificar&mensaje=anular&table_type=<%=tg.gettable_type()%>&table_code=<%=tg.gettable_code()%>&program=<%=tg.getprogram()%>';window.opener.location.reload()" onMouseOut="botonOut(this);" style="cursor:hand">&nbsp;	
			   <img src="<%=BASEURL%>/images/botones/salir.gif"  name="imgsalir"  onMouseOver="botonOver(this);" onClick="parent.close();" onMouseOut="botonOut(this);" style="cursor:hand">
      </div>
    </form>

      <%if(!Mensaje.equals("")){%>
<br>
    <table border="2" align="center">
      <tr>
        <td><table width="100%"  border="1" align="center"  bordercolor="#F7F5F4" bgcolor="#FFFFFF">
            <tr>
              <td width="229" align="center" class="mensajes"><%=Mensaje%></td>
              <td width="29" background="<%=BASEURL%>/images/cuadronaranja.JPG">&nbsp;</td>
              <td width="58">&nbsp;</td>
            </tr>
        </table></td>
      </tr>
    </table>

    <%}%>  
</div>
</body>

</html>