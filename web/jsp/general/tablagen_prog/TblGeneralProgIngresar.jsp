<!--
- Autor : ricardo Rosero
- Date  : 23 de Diciembre de 2005
- Copyrigth Notice : Fintravalores S.A. S.A
-->
<%--
-@(#)
--Descripcion : Pagina JSP, presenta el formulario de ingreso de registro a la tabla tbl_general_program
--%>
<%-- Declaracion de librerias--%>
<%@page contentType="text/html"%>
<%@page session="true"%>
<%@page errorPage="../error/ErrorPage.jsp"%>
<%@page import="com.tsp.operation.model.*"%>
<%@page import="com.tsp.util.*"%>
<%@page import="com.tsp.operation.model.beans.*, java.util.*"%>
<%@include file="/WEB-INF/InitModel.jsp"%>
<%@ taglib uri="http://jakarta.apache.org/taglibs/input-0.90" prefix="input" %>
<%@page import="com.tsp.operation.model.DAOS.*"%>
<% String Mensaje       = (request.getParameter("Mensaje")!=null)?request.getParameter("Mensaje"):""; %>
<html>
<head>
<title>Ingresar Elemento tablagen_prog</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link href="<%= BASEURL %>/css/estilostsp.css" rel="stylesheet" type="text/css">
<link href="<%= BASEURL %>/css/estilostsp.css" rel="stylesheet" type="text/css"> 
<script type='text/javascript' src="<%= BASEURL %>/js/boton.js"></script>
<script type='text/javascript' src="<%= BASEURL %>/js/validar.js"></script>
</head>
<body ONLOAD="redimensionar();" onresize="redimensionar()">
<div id="capaSuperior" style="position:absolute; width:100%; height:100px; z-index:0; left: 0px; top: 0px;">
	<jsp:include page="/WEB-INF/toptsp.jsp?encabezado=Insertar Datos en Tabla tablagen_prog"/></div>
<div id="capaCentral" style="position:absolute; width:100%; height:87%; z-index:0; left: 0px; top: 100px; overflow: scroll;">
<form name="forma" id="forma" action="<%=CONTROLLER%>?estado=TblGeneralProg&accion=Insert" method="post">
  <table width="45%"  border="2" align="center">
  	<tr>
    	<td>
			<table width="100%" align="center">
				<tr>
					<td width="40%" class="subtitulo1" nowrap>Datos</td>
					<td width="60%" class="barratitulo"><img src="<%= BASEURL %>/images/titulo.gif" width="32" height="20"></td>
				</tr>
				<tr class="fila">
				  <td>C&oacute;digo Tabla </td>
				  <td><input name="table_type" type="text" size="10" maxlength="10">
				  <img src="<%= BASEURL %>/images/botones/iconos/obligatorio.gif" width="10" height="10"></img></td>
				</tr>
				
			  <tr class="fila">
				  <td>C�digo</td>
				  <td><input name="table_code" type="text" size="10" maxlength="10">
				  <img src="<%= BASEURL %>/images/botones/iconos/obligatorio.gif" width="10" height="10"></img></td>
		      </tr>
				<tr class="fila">
				  <td>Programa</td>
				  <td><input name="program" type="text" size="20" maxlength="20">
				  <img src="<%= BASEURL %>/images/botones/iconos/obligatorio.gif" width="10" height="10"></img></td>
			  </tr>
		  </table>
  		</td>
  	</tr>
  </table><br>
  <table align=center width='450'>
    <tr>
        <td align="center" colspan="2">
            <img src="<%= BASEURL %>/images/botones/aceptar.gif"  name="imgaceptar" onclick="return TCamposLlenos();" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" style="cursor:hand ">&nbsp; 
			<img src="<%= BASEURL %>/images/botones/cancelar.gif"  name="imgcancelar"  onMouseOver="botonOver(this);" onClick="forma.reset();" onMouseOut="botonOut(this);"  style="cursor:hand ">&nbsp; 
			<img src="<%= BASEURL %>/images/botones/salir.gif"  name="imgsalir"  onMouseOver="botonOver(this);" onClick="parent.close();" onMouseOut="botonOut(this);"  style="cursor:hand ">
			
        </td>
    </tr>
  </table>  
  <p>
    <%if(!Mensaje.equals("")){%>
</p>
  <p>  
  <table border="2" align="center">
    <tr>
      <td><table width="100%"  border="1" align="center"  bordercolor="#F7F5F4" bgcolor="#FFFFFF">
          <tr>
            <td width="229" align="center" class="mensajes"><%=Mensaje%></td>
            <td width="29" background="<%=BASEURL%>/images/cuadronaranja.JPG">&nbsp;</td>
            <td width="58">&nbsp;</td>
          </tr>
      </table></td>
    </tr>
  </table>
  <p></p>
  <%}%>
</form>
  <br>
</div>
</body>
</html>
