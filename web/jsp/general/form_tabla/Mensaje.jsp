<!--
- Autor : Ing. Diogenes Bastidas Morales
- Date  : 20 de julio de 2005
- Copyrigth Notice : Fintravalores S.A. S.A
-->
<%--
-@(#)
--Descripcion : Pagina JSP, que maneja la busqueda de identidades

--%>

<%-- Declaracion de librerias--%>
<%@page contentType="text/html"%>
<%@page session="true"%>
<%@page import="java.util.*"%>
<%@page import="com.tsp.operation.model.*"%>
<%@page import="com.tsp.util.*"%>
<%@page import="com.tsp.operation.model.beans.*, java.util.*"%>
<%@include file="/WEB-INF/InitModel.jsp"%>

<html>
<head>
<title>Error Formulario</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link href="<%= BASEURL %>/css/estilostsp.css" rel="stylesheet" type="text/css"> 
<link href="../../../css/estilostsp.css" rel="stylesheet" type="text/css"> 
<script type='text/javascript' src="<%= BASEURL %>/js/boton.js"></script>
<script type='text/javascript' src="<%= BASEURL %>/js/general.js"></script>
<script>
       function enviar(url){
                forma.action = url;
                forma.submit();
        }
</script>
</head>

<body onLoad="redimensionar();" onresize="redimensionar()">
<div id="capaSuperior" style="position:absolute; width:100%; height:100px; z-index:0; left: 0px; top: 0px;">
	<jsp:include page="/WEB-INF/toptsp.jsp?encabezado=Buscar Formulario"/>
</div>
<div id="capaCentral" style="position:absolute; width:100%; height:87%; z-index:0; left: 0px; top: 100px; overflow: scroll;">
<% String formato = request.getParameter("formato") != null ? request.getParameter("formato"): "";
   String tabla =  request.getParameter("tabla") != null ? request.getParameter("tabla"): "";
   String msg = (request.getParameter("mensaje")!=null)? request.getParameter("mensaje"):"" ;
   String sw = (request.getParameter("sw")!=null)?request.getParameter("sw"):"";
%>

<%if(!msg.equals("")){%>
  </p>
  <p>
<form action="" id="forma" name="forma">
<table border="2" align="center">
  <tr>
    <td><table width="100%"  border="1" align="center"  bordercolor="#F7F5F4" bgcolor="#FFFFFF">
      <tr>
        <td width="229" align="center" class="mensajes"><%=msg%></td>
        <td width="29" background="<%=BASEURL%>/images/cuadronaranja.JPG">&nbsp;</td>
        <td width="58">&nbsp;</td>
      </tr>
    </table></td>
  </tr>
</table>
</p>
</form>
 <%}
if(sw.equals("")){%>
 <div align="center" class="Simulacion_Hiper" style="cursor:hand" onClick="window.location='<%=CONTROLLER%>?estado=Formato&accion=Tabla&opcion=3&c_formato=<%=formato%>&c_tabla=<%=tabla%>)'">
  Validar formato
</div>
<%}%>
  <br>
   <div align="center">
  <img src="<%=BASEURL%>/images/botones/salir.gif"  name="imgsalir"  onMouseOver="botonOver(this);" onClick="parent.close();" onMouseOut="botonOut(this);" style="cursor:hand">  </div>
</div>
</body>
</html>
