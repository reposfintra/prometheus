<!--
- Autor : Ing. Diogenes Bastidas Morales
- Date  : 20 de julio de 2005
- Copyrigth Notice : Fintravalores S.A. S.A
-->
<%--
-@(#)
--Descripcion : Pagina JSP, que maneja la busqueda de identidades

--%>

<%-- Declaracion de librerias--%>
<%@page contentType="text/html"%>
<%@page session="true"%>
<%@page import="java.util.*"%>
<%@page import="com.tsp.operation.model.*"%>
<%@page import="com.tsp.util.*"%>
<%@page import="com.tsp.operation.model.beans.*, java.util.*"%>
<%@include file="/WEB-INF/InitModel.jsp"%>

<html>
<head>
<title>Buscar Informaci&oacute;n</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link href="<%= BASEURL %>/css/estilostsp.css" rel="stylesheet" type="text/css"> 
<link href="../../../css/estilostsp.css" rel="stylesheet" type="text/css"> 
<script type='text/javascript' src="<%= BASEURL %>/js/boton.js"></script>
<script type='text/javascript' src="<%= BASEURL %>/js/general.js"></script>
<script>
   function enviar(url){
			forma.action = url;
			forma.submit();
	}
	function redondear(cantidad, decimales) {
		if(cantidad != ''){
			var cantidad = parseFloat(cantidad);
			var decimales = parseFloat(decimales);
			decimales = (!decimales ? 0 : decimales);
			return Math.round(cantidad * Math.pow(10, decimales)) / Math.pow(10, decimales);
		}else{
			return cantidad;
		}
	}
</script>
</head>

<body onLoad="redimensionar();" onresize="redimensionar()">
<div id="capaSuperior" style="position:absolute; width:100%; height:100px; z-index:0; left: 0px; top: 0px;">
	<jsp:include page="/WEB-INF/toptsp.jsp?encabezado=Buscar Información"/>
</div>
<div id="capaCentral" style="position:absolute; width:100%; height:87%; z-index:0; left: 0px; top: 100px; overflow: scroll;">
<% String forma = request.getParameter("formato") != null ? request.getParameter("formato"): "";
    String msg = (request.getParameter("mensaje")!=null)? request.getParameter("mensaje"):"" ;
	Vector campos = model.formato_tablaService.getVecbusqueda(); 
   System.out.println("JSP "+ campos.size() );
%>
<form name="forma" method="post" action="<%=CONTROLLER%>?estado=Formulario&accion=Buscar&pagina=FormularioMod.jsp&carpeta=/jsp/general/form_tabla">
  <table width="500" border="2" align="center">
    <tr>
      <td><table width="100%">
        <tr>
          <td width="173" class="subtitulo1">&nbsp;Datos del formato</td>
          <td width="205" class="barratitulo"><img src="<%=BASEURL%>/images/titulo.gif" width="32" height="20"></td>
        </tr>
      </table>
      <table width="100%" align="center" class="tablaInferior">
      <tr class="fila">
        <td width="119" align="left" valign="middle" >Formato</td>
        <td width="250" valign="middle"><select name="formato" class="textbox" id="formato" style="width:90% " onChange="enviar('<%=CONTROLLER%>?estado=Formulario&accion=CargarCampos&carpeta=/jsp/general/form_tabla&pagina=BuscarDatos.jsp&opcion=CBUSQUEDA');">
                  <option value="">Seleccione Un Item</option>
               <%Vector tblres =  model.formato_tablaService.getFordefinidos();
               for(int i = 0; i<tblres.size(); i++){
                       Formato_tabla respa = (Formato_tabla) tblres.get(i);%>
                       <option value="<%=respa.getFormato()%>" <%=(respa.getFormato().equals(forma))?"selected":""%> ><%=respa.getNombre_formato()%></option>
             <%}%>
     </select>
          <input type="hidden" name="cant" value="<%=campos.size()%>" ></td>
      </tr>
      <% 
         if(campos.size() > 0){
        for(int i=0 ; i < campos.size() ; i++){
          Formato_tabla formato = (Formato_tabla) campos.get(i);
          String m = "";
          m = m.valueOf ( formato.getLongitud() );
          String []def = m.split("\\.");  
          int longitud = Integer.parseInt(def[0]) > 500 ? 50:Integer.parseInt(def[0]);
          int decimal  = Integer.parseInt(def[1]);
%>
          <tr class="fila">
                    <td align="left" valign="middle" nowrap><%=formato.getCampo_jsp()%></td>
         <% if(formato.getTipo_campo().equals("DO") || formato.getTipo_campo().equals("INT")){
                String dec = (formato.getTipo_campo().equals("DO"))?"OK":"NO";%>
        			<td valign="middle"><input name="<%=formato.getCampo_tabla()%>" type="text" onKeyPress="soloDigitos(event,'dec<%=dec%>')" size="<%=longitud%>" maxlength="<%=longitud%>" value="<%=formato.getValor_campo()%>" <%if(decimal>0){%>onBlur="this.value = redondear(this.value,<%=decimal%>);"<%}%> title="<%=formato.getTitulo()%>"></td>
         <%}else if(formato.getTipo_campo().equals("TEXTBOX")){%>
                    <td valign="middle"><textarea name="<%=formato.getCampo_tabla()%>" cols="60%" class="textbox" ><%=formato.getValor_campo()%></textarea></td>
        <%} else if(formato.getTipo_campo().equals("DATE") || formato.getTipo_campo().equals("DATEHOUR")){%>
                  <td valign="middle"><input  type="text"   name="<%=formato.getCampo_tabla()%>"  class="textbox" value="<%=formato.getValor_campo()%>" readonly size="<%=formato.getTipo_campo().equals("DATE")?"10":"15"%>" title="<%=formato.getTitulo()%>" >
                 <a href="javascript:void(0)" onclick="if(self.gfPop)gfPop.fPopCalendar(document.forma.<%=formato.getCampo_tabla()%>);return false;" HIDEFOCUS><img src="<%=BASEURL%>\js\Calendario\cal.gif" width="16" height="16" border="0" alt="De click aqui para escoger la fecha"></a> </td>
        <%}else if(formato.getTipo_campo().equals("TXT")){%> 
                <td valign="middle"><input type="text" name="<%=formato.getCampo_tabla()%>" size="<%=longitud%>"  maxlength="<%=longitud%>" value="<%=formato.getValor_campo()%>" title="<%=formato.getTitulo()%>"></td>
        <%}%>
      </tr> 
   <% 
     }
   }%>
  </table></td>
    </tr>
  </table>
  <br>
   <div align="center">
	   <img src="<%=BASEURL%>/images/botones/aceptar.gif"  name="imgaceptar" onClick="if(forma.cant.value==0){alert('El formato no posee filtros de busqueda')}else{return TCamposLlenos(forma);}" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" style="cursor:hand">&nbsp;       &nbsp;
       <img src="<%=BASEURL%>/images/botones/salir.gif"  name="imgsalir"  onMouseOver="botonOver(this);" onClick="parent.close();" onMouseOut="botonOut(this);" style="cursor:hand">
   </div>
<%if(!msg.equals("")){%>
  </p>
  <p><table border="2" align="center">
  <tr>
    <td><table width="100%"  border="1" align="center"  bordercolor="#F7F5F4" bgcolor="#FFFFFF">
      <tr>
        <td width="229" align="center" class="mensajes"><%=msg%></td>
        <td width="29" background="<%=BASEURL%>/images/cuadronaranja.JPG">&nbsp;</td>
        <td width="58">&nbsp;</td>
      </tr>
    </table></td>
  </tr>
</table>
</p>
 <%}%>
</form>
</div>
<iframe width=188 height=166 name="gToday:normal:agenda.js:gfPop:plugins.js" id="gToday:normal:agenda.js:gfPop:plugins.js" src="<%=BASEURL%>/js/Calendario/ipopeng.htm" scrolling="no" frameborder="0" style="visibility:visible; z-index:999; position:absolute; left:-500px; top:0px;"> </iframe>
</body>
</html>
