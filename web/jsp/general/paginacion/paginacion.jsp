<!--  
     - Author(s)       :      MARIO FONTALVO
     - Date            :      10/12/2005  
     - Copyright Notice:      TRANSPORTES SANCHEZ POLO S.A.
  -->
 <%--   
     - @(#)  
     - Description: Permite realizar paginacion
 --%> 
<style>  
   a:link , a:active ,  a:visited { 
        color: #003399;
	font-family: Tahoma, Arial;
	font-size: small;
	font-size: 12px;
   }

   a:hover { 
        color: red;
	font-family: Tahoma, Arial;
	font-size: small;
	font-size: 12px;
   }
   
</style>
<center>
  <%
  if(paginacion.getListado().size()>0){ //Si el no entra a este IF es porque tu (lista / resultset) esta vacia
      if(paginacion.getCantidad_Vista()>paginacion.getCantidad_Indices()) out.print("<a href='"+ CONTROLLER + paginacion.getAccion() + "&Opcion=Paginar&Mov=Inicio'>[Ini]</a>&nbsp;&nbsp;&nbsp;");
      if(((paginacion.getInicio()*paginacion.getCantidad_Indices())-(paginacion.getCantidad_Indices()-1))!=1) out.print("<a href='"+ CONTROLLER + paginacion.getAccion() + "&Opcion=Paginar&Mov=Ant'>Ant.</a>&nbsp;");
      int i;
      for(i=(paginacion.getInicio()*paginacion.getCantidad_Indices())-(paginacion.getCantidad_Indices()-1);i<=((paginacion.getInicio()*paginacion.getCantidad_Indices())-(paginacion.getCantidad_Indices()-1)+paginacion.getCantidad_Indices()-1);i++) {
          out.print((paginacion.getVista_Actual()==i)?("<span class='informacion'><b>["+(i<10?"0":"")+i+"]</b></span>&nbsp;"):("<a href='"+ CONTROLLER + paginacion.getAccion() + "&Opcion=Paginacion&Vista="+i+"'>"+(i<10?"0":"")+i+"</a> "));
          if(i==paginacion.getCantidad_Vista()){ i++; break; }
      }
      if((i-1)!=paginacion.getCantidad_Vista()) out.print("<a href='"+ CONTROLLER + paginacion.getAccion() + "&Opcion=Paginar&Mov=Sig'   > Sig.</a>");
      if(paginacion.getCantidad_Vista()>paginacion.getCantidad_Indices()) out.print("&nbsp;&nbsp;&nbsp;<a href='"+ CONTROLLER + paginacion.getAccion() + "&Opcion=Paginar&Mov=Fin'>[Fin]</a>");
  }
  else
     out.print("<font color='red'><b>!</b></font>");
  %>
  </center>
