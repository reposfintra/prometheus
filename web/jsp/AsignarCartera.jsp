<%-- 
    Document   : AsignarCartera
    Created on : 19/09/2019, 03:50:36 PM
    Author     : mcamargo
--%>

<%@page contentType="text/html" pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
        <link href="./css/popup.css" rel="stylesheet" type="text/css">
        <link type="text/css" rel="stylesheet" href="./css/jquery/jquery-ui/jquery-ui.css" />
        <script type="text/javascript" src="./js/jquery/jquery.jqGrid/js/jquery.min.js"></script>
        <script type="text/javascript" src="./js/jquery-ui-1.8.5.custom.min.js"></script>   
        <script type="text/javascript" src="./js/jquery/jquery.jqGrid-4.7.0/js/grid.locale-es.js"></script>
        <script type="text/javascript" src="./js/jquery/jquery.jqGrid-4.7.0/js/jquery.jqGrid.min.js"></script>
        <script type="text/javascript" src="./js/jquery/jquery.jqGrid-4.7.0/js/jquery.tabletojson.js"></script> 
        <script src="./js/AsignarCartera.js" type="text/javascript"></script>
        <title>Reporte Asignacion Cartera</title>
        <style>
            .filtro{
                width: 650px;
            }
            .filtro2{
                width: 300px;
                margin-left: 10px;
            }

            .header_filtro{
                text-align: center;   
                border: 1px solid #234684;
                background: #2A88C8;
                color: #ffffff;
                font-weight: bold;
                border-radius: 8px 8px 0px 0px;
            }

            .body_filtro{                
                background: white; 
                border:1px solid #2A88C8;
                height: 140px;
                border-radius: 0px 0px 8px 8px;
            }

           

            label{
                display: block;
                text-align: center;
                font-family: 'Lucida Grande','Lucida Sans',Arial,sans-serif;
                font-size: 15px;
            }

            .caja{
                float:left;
                margin-left:10px;
                margin-right:10px;
                margin-top: 8px;
            }
            input[type=text],select{
                padding: 5px 5px;
                margin: 8px auto;
                border: 1px solid #ccc;
                border-radius: 4px;
                font-size: 15px;
                color: black;
            }
            .select {
                font-size: 12px;
                background: #2A88C8;
                color: white;
                font-weight: bold;
            }
            .button {
                padding: 8px 8px;
                font-size: 12px;
                cursor: pointer;
                text-align: center;
                color: #fff;
                background-color: #2a88c8;
                border: none;
                border-radius:8px;
                margin: 22px auto;
                font-weight: bold;
                width: 80px;
            }
            
        </style>
    </head>
    <body>
        <div>
            <jsp:include page="/WEB-INF/toptsp.jsp?encabezado=Reporte Cartera"/>
        </div>

        <div style="display: flex; justify-content: center; margin-top: 10px;">
        <div class="filtro">
            <div class="header_filtro">
                <label>FILTRO DE BUSQUEDA</label>
            </div>
            <div class="body_filtro">
                <div class="caja">
                       <label> Unidad Negocio:</label>
                       <select id="id_unidad_negocio" name="id_unidad_negocio" class="select">
                       </select>
                </div> 
                <div class="caja">
                       <label> Agencia:</label>
                       <select id="id_agencia" name="id_agencia" class="select" onchange="cargarCboResponsable(this.value);cargarCboCiudad(this.value);">
                           <option value="">...</option>
                           <option value="ATL">ATLANTICO</option>
                           <option value="COR">CORDOBA</option>
                           <option value="SUC">SUCRE</option>
                       </select>
                </div>
                <div class="caja">
                       <label> Altura mora:</label>
                       <select id="id_altura_mora" name="id_altura_mora" class="select">
                            <option value="">...</option>
                            <!--<option value="1- CORRIENTE">1- CORRIENTE</option>-->
                            <option value="02- 1 A 30">02- 1 A 30</option>
                            <option value="03- ENTRE 31 Y 60">03- ENTRE 31 Y 60</option>
                            <option value="04- ENTRE 61 Y 90">04- ENTRE 61 Y 90</option>
                            <option value="05- ENTRE 91 Y 120">05- ENTRE 91 Y 120</option>
                            <option value="06- ENTRE 121 Y 180">06- ENTRE 121 Y 180</option>
                            <option value="07- ENTRE 180 Y 210">07- ENTRE 180 Y 210</option>
                            <option value="08- ENTRE 210 Y 240">08- ENTRE 210 Y 240</option>
                            <option value="09- ENTRE 240 Y 270">09- ENTRE 240 Y 270</option>
                            <option value="10- ENTRE 270 Y 300">10- ENTRE 270 Y 300</option>
                            <option value="11- ENTRE 300 Y 330">11- ENTRE 300 Y 330</option>
                            <option value="12- ENTRE 330 Y 365">12- ENTRE 330 Y 365</option>
                            <option value="13- MAYOR A 1 ANIO">13- MAYOR A 1 ANIO</option>
                       </select>
                </div> 
                 <div class="caja">
                       <label> Responsable cuenta:</label>
                       <select id="id_responsable_cuenta" name="id_responsable_cuenta" style="width: 138px;" class="select">
                           <option value="">...</option>
                       </select>
                </div> 
                <div class="caja">
                       <label> Ciudad:</label>
                       <select id="id_ciudad" name="id_ciudad" style="width: 220px;" class="select" onchange="cargarCboBarrio(this.value);">
                           <option value="">...</option>
                       </select>
                </div> 
                <div class="caja">
                       <label> Barrio:</label>
                       <select id="id_barrio" name="id_barrio" class="select"  style="width: 220px;">
                           <option value="">...</option>
                       </select>
                </div> 
                <div class="caja">
                    <button class="button" onclick="cargarNegociosPorAsignar();">Buscar</button>
                </div>
            </div>

        </div>
         <div class="filtro2">
                    <div class="header_filtro">
                        <label>Asignación</label>
                    </div>
                    <div class="body_filtro">
<!--                        <div class="caja">
                            <div style="display: inline-flex; width: 25px; height: 50px;">
                                <hr style="margin: 0px 0px 0px 13px;">
                            </div>
                        </div>-->
                        <div class="caja">
                            <label> Asignar a:</label>
                            <select id="id_recaudador_tercero" name="id_recaudador_tercero" class="select">
                            </select>
                        </div>
                        <div class="caja">
                            <button class="button" onclick="asignarCarteraTercero();">Asignar</button>
                        </div>
                    </div>
                </div> 
       </div> 
        
        <center>
        <div  style="margin-top: 20px; width: 90%;"  >
                <table id="tabla_negocios" ></table>
                <div id="pager"></div>
        </div>
        <div id="dialogMsj" title="Mensaje" style="display:none; .hide-close .ui-dialog-titlebar-close { display: none }">
                <p style="font-size: 12px;text-align:justify;" id="msj" > Texto </p>
        </div>  
        <div id="info"  class="ventana" >
                <p id="notific"></p>
            </div>
            
            <div id="dialogLoading" style="display:none;">
                <p  style="font-size: 12px;text-align:justify;" id="msj2">Texto </p> <br/>
                <center>
                    <img src="./images/cargandoCM.gif"/>
                </center>
            </div>
            
            <div id="divSalidaEx" title="Exportacion" style=" display: block" >
                <p  id="msjEx" style=" display:  none"> Espere un momento por favor...</p>
                <center>
                    <img id="imgloadEx" style="position: relative;  top: 7px; display: none " src="./images/cargandoCM.gif"/>
                </center>
                <div id="respEx" style=" display: none"></div>
            </div> 
            </center>
    </body>
</html>
