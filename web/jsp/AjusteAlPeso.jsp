<%-- 
    Document   : AjusteAlPeso
    Created on : 15/03/2016, 06:31:46 PM
    Author     : egonzalez
--%>

<%@page contentType="text/html" pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
        <link href="./css/popup.css" rel="stylesheet" type="text/css">
        <link type="text/css" rel="stylesheet" href="./css/jquery/jquery-ui/jquery-ui.css" />
        <link type="text/css" rel="stylesheet" href="./css/TransportadorasApi.css" />
        <script type="text/javascript" src="./js/jquery/jquery.jqGrid/js/jquery.min.js"></script>
        <script type="text/javascript" src="./js/jquery-ui-1.8.5.custom.min.js"></script>   
        <script type="text/javascript" src="./js/jquery/jquery.jqGrid-4.7.0/js/grid.locale-es.js"></script>
        <script type="text/javascript" src="./js/jquery/jquery.jqGrid-4.7.0/js/jquery.jqGrid.min.js"></script>
        <script type="text/javascript" src="./js/jquery/jquery.jqGrid-4.7.0/js/jquery.tabletojson.js"></script>    
        <script type="text/javascript" src="./js/AjusteALPeso.js"></script> 

        <title>AJUSTE AL PESO</title>
    </head>
    <body>

        <div id="capaSuperior" style="position:absolute; width:100%; height:100px; z-index:0; left: 0px; top: 0px;">
            <jsp:include page="/WEB-INF/toptsp.jsp?encabezado=AJUSTE AL PESO"/>
        </div>

        <div id="capaCentral" style="position:absolute; width:100%; height:80%; z-index:0; left: 0px; top: 100px; ">
            <center>

                <div style="background-color: #FFF;
                     width: 500px;
                     height: 95px;
                     border-radius: 4px;
                     padding: 1px;
                     border: 1px solid #2A88C8;
                     margin: 15px auto 0px; " >

                    <div id="encabezadotablita" style="width: 495px">
                        <label class="titulotablita"><b>AJUSTE AL PESO</b></label>
                    </div> 
                    <table border="0" style="width:500px">
                        <tr>
                            <td style="font-size: 12px;width:117px;padding-top:  5px">
                                Seleccione archivo
                            </td>
                            <td style="padding-top:16px">
                                <form id="formulario" name="formulario">
                                    <input type="file" id="examinar" name="examinar" style="width: 180px;" >
                                </form>
                            </td>
                            <td style="padding-top:  5px">
                                <div id ='botones'>
                                    <button id="subir" class="ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only" 
                                            role="button" aria-disabled="false" style="top: 10px;" >
                                        <span class="ui-button-text">Subir Archivo</span>
                                    </button> 
                                </div>
                            </td>
                        </tr>
                    </table>
                </div>

                <br><br>
                <div id="contenido" style="width: 1172px;height: 409px;">
                    <table id="tabla_informacionfac" ></table>
                    <div id="pager1"></div>
                    <br>

                    <table style="float: left;margin-left: 10px;">  
                        <tr>
                            <td>
                                <div style="background-color: #F32541;width: 17px;height: 17px;margin-bottom: 5px;margin-top: 3px;"></div>
                            </td>
                            <td>
                                <label style="font-size: 14px;">Falta informacion </label>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <div style="background-color: #90CC00;width: 17px;height: 17px;margin-bottom: 5px;margin-top: 3px;"></div>
                            </td>
                            <td>
                                <label style="font-size: 14px;">Datos correctos</label>
                            </td>
                        </tr>
                    </table>
                </div>
                <div id="info"  class="ventana" >
                    <p id="notific">EXITO AL GUARDAR</p>
                </div>
        </div>
    </body>
</html>
