<%-- 
    Document   : reasignar_asesores_ventas
    Created on : 21/01/2019, 02:34:03 PM
    Author     : mcamargo
--%>

<%@page contentType="text/html" pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
        <link href="./css/popup.css" rel="stylesheet" type="text/css">
        <link type="text/css" rel="stylesheet" href="./css/jquery/jquery-ui/jquery-ui.css" />
        <script type="text/javascript" src="./js/jquery/jquery.jqGrid/js/jquery.min.js"></script>
        <script type="text/javascript" src="./js/jquery-ui-1.8.5.custom.min.js"></script>   
        <script type="text/javascript" src="./js/jquery/jquery.jqGrid-4.7.0/js/grid.locale-es.js"></script>
        <script type="text/javascript" src="./js/jquery/jquery.jqGrid-4.7.0/js/jquery.jqGrid.min.js"></script>
        <script type="text/javascript" src="./js/jquery/jquery.jqGrid-4.7.0/js/jquery.tabletojson.js"></script>   
        <style type="text/css">             
             .hide-close .ui-dialog-titlebar-close { display: none }
         </style>
                    

        <script type="text/javascript" src="./js/reasignar_asesores_ventas.js"></script> 
        <title>Reasignar asesores de ventas</title>
    </head>
    <body>
        <div id="capaSuperior" style="position:absolute; width:100%; height:100px; z-index:0; left: 0px; top: 0px;">
            <jsp:include page="/WEB-INF/toptsp.jsp?encabezado=Reasignar asesores de ventas"/>
        </div>
        
        <center>
            
            <div id="tablita" style="top: 170px;width: 280px;height: 95px;  margin-top: 100px;" >
            <div class="ui-dialog-titlebar ui-widget-header ui-helper-clearfix">
                <span class="ui-dialog-title text-center" id="ui-dialog-title-listaGlobal">
                   <label><b>ASESORES</b></label>
                </span>
            </div>
            
            <div  style="padding: 5px; background: white; border:1px solid #2A88C8">
                    <div class="col-md-12 ">
                        <table id="tablainterna" style="height: 53px; width: 614px"  >

                            <tr style = "text-align: middle;vertical-align: middle">

                                <td style = "text-align: middle;vertical-align: middle">
                                    <div style = "float: left;valign:middle">
                                    <label>Asignar a:</label>
                                   <select id="asesor" name="asesor" style="height: 23px;width: 200px;color: #070708;">
                                                                 
                                   </select>
                                    </div>  
                                </td> 
                            </tr>
                        </table>
                    </div>
            </div>
            </div>
            
            <div  style="margin-top: 20px; " >
                <table id="tabla_solicitudes_x_asignar" ></table>
                <div id="pager"></div>
            </div>

            <div id="info"  class="ventana" >
                <p id="notific"></p>
            </div>
            
            <div id="dialogLoading" style="display:none;">
                <p  style="font-size: 12px;text-align:justify;" id="msj2">Texto </p> <br/>
                <center>
                    <img src="./images/cargandoCM.gif"/>
                </center>
            </div>
            <div id="dialogMsj" title="Mensaje" style="display:none; .hide-close .ui-dialog-titlebar-close { display: none }">
                <p style="font-size: 12px;text-align:justify;" id="msj" > Texto </p>
            </div>  
            
        </center>
        
    </body>
</html>
