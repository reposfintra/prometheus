<%-- 
    Document   : pazySalvo
    Created on : 22/11/2016, 04:17:55 PM
    Author     : mariana
--%>

<%@page contentType="text/html" pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
        <link href="./css/popup.css" rel="stylesheet" type="text/css">
        <link type="text/css" rel="stylesheet" href="./css/jquery/jquery-ui/jquery-ui.css" />
        <link type="text/css" rel="stylesheet" href="./css/TransportadorasApi.css " />
        <script type="text/javascript" src="./js/jquery/jquery.jqGrid/js/jquery.min.js"></script>
        <script type="text/javascript" src="./js/jquery-ui-1.8.5.custom.min.js"></script>   
        <script type="text/javascript" src="./js/jquery/jquery.jqGrid-4.7.0/js/grid.locale-es.js"></script>
        <script type="text/javascript" src="./js/jquery/jquery.jqGrid-4.7.0/js/jquery.jqGrid.min.js"></script>
        <script type="text/javascript" src="./js/jquery/jquery.jqGrid-4.7.0/js/jquery.tabletojson.js"></script>    
        <script type="text/javascript" src="./js/jquery/jquery.jqGrid-4.7.0/plugins/jquery.contextmenu.js"></script>
        <link type="text/css" rel="stylesheet" href="./css/loaderApi.css">
        <script type="text/javascript" src="./js/utilidadInformacion.js"></script> 
        <script type="text/javascript" src="./js/estadoCuenta.js"></script> 
        <title>ESTADO CUENTA</title>
    </head>
    <body>
        <div id="capaSuperior" style="position:absolute; width:100%; height:100px; z-index:0; left: 0px; top: 0px;">
            <jsp:include page="/WEB-INF/toptsp.jsp?encabezado=ESTADO CUENTA"/>
        </div>
    <center>
        <div id="tablita" class="ui-jqgrid ui-widget ui-widget-content center-block" style="width: 478px; margin-left: 67px;padding: 8px;">
         <div class="ui-dialog-titlebar ui-widget-header ui-helper-clearfix">
                <label class="titulotablita"><b>FILTRO DE BUSQUEDA</b></label>
            </div>
            <table id="tablainterna" style="height: 53px; width: 476px"  >
                <tr>
                    <td>
                        <label>No Cedula</label>
                    </td>
                    <td>
                        <input id="cedula" type="text"  style="width: 110px;" onkeyup="funcionFormatos.soloNumero(this)">
                    </td>
                    <td>
                        <label>Negocio</label>
                    </td>
                    <td>
                        <input id="negocio" type="text"  style="width: 110px;" >
                    </td>
                    <td>
                        <hr style="width: 2px;height: 37px;top: -25px;margin-left: 13px;color: rgba(128, 128, 128, 0.39);" /> 
                    </td>
                    <td>
                        <div style="padding-top: 10px">
                            <center>
                                <button type="button" id="buscar" class="ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only" style="margin-left: 15px;top: -7px;" > 
                                    <span class="ui-button-text">Buscar</span>
                                </button>  
                            </center>
                        </div>
                    </td>
                </tr>
            </table>
        </div>
        <div id="tablita_" class="ui-jqgrid ui-widget ui-widget-content center-block" style="width: 252px; margin-top: 85px;margin-right: 1138px;margin-left: 35px !important;padding: 8px;border: 1px rgba(32, 66, 126, 0.84);">
        <table id="tablainterna" style="height: 53px; width: 243px"  >
                <tr>
                    <td>
                        <label id="labelFechaMax">Fecha Maxima Pago</label>
                    </td>
                    <td>
                        <input id="fecha_max" type="text"  style="width: 110px;">
                    </td>
                </tr>
            </table>
        </div>
        <div style="position: relative;">
            <table id="tabla_estadoCuenta" ></table>
            <div id="pager"></div>
        </div>

        <div id="info"  class="ventana" >
            <p id="notific">EXITO AL GUARDAR</p>
        </div>

        <div id="dialogMsjliquidacionDetalle" style="position: relative;" hidden="true">
            <!--1001590327-->

            <div id="tablita" class="ui-jqgrid ui-widget ui-widget-content center-block" style="width: 361px; margin-left: 10px;padding: 2px;margin-top: -99px">
                <table id="tablainterna" style="height: 53px; width: 296px"  >
                    <tr>
                        <td>
                            <label>Cliente</label>
                        </td>
                        <td>
                            <input id="cliente" type="text"  style="width: 300px;border: 0px;" readonly >
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <label>Negocio</label>
                        </td>
                        <td>
                            <input id="negocio_" type="text"  style="width: 110px;border: 0px;" readonly>
                        </td>
                    </tr>
                </table>
                <div class="ui-dialog-titlebar ui-widget-header ui-helper-clearfix">
                    <label class="titulotablita"><b>Informacion del Negocio</b></label>
                </div>
                <table id="tablainterna" style="height: 53px; width: 361px"  >
                    <tr>
                        <td>
                            <label>Valor Negocio</label>
                        </td>
                        <td>
                            <input id="vlr_negocio" type="text"  style="width: 110px;" readonly>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <label>Tasa de Interes</label>
                        </td>
                        <td>
                            <input id="tasa" type="text"  style="width: 110px;" readonly>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <label>Numero Cuotas</label>
                        </td>
                        <td>
                            <input id="ncuotas" type="text"  style="width: 110px;" readonly>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <label>Valor Aval</label>
                        </td>
                        <td>
                            <input id="vlr_aval" type="text"  style="width: 110px;" readonly>
                        </td>
                    </tr>
                </table>
            </div>
            <div style="position: relative;top: 135px;">
                <table id="tabla_liquidacionDetalle" ></table>
                <div id="pager1"></div>
            </div>

            <div id="tablita" class="ui-jqgrid ui-widget ui-widget-content center-block" style="width: 361px; margin-left: 10px;padding: 2px;top: 150px;">
                <div class="ui-dialog-titlebar ui-widget-header ui-helper-clearfix">
                    <label class="titulotablita"><b>Informacion General</b></label>
                </div>
                <table id="tablainterna" style="height: 53px; width: 361px"  >
                    <tr>
                        <td>
                            <label>Cuota Vencidas</label>
                        </td>
                        <td>
                            <input id="cuotavencidas" type="text"  style="width: 110px;" readonly>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <label id="labeldeuda_">Cuota Actual</label>
                        </td>
                        <td>
                            <input id="deuda" type="text"  style="width: 110px;" readonly>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <label>Capital Futuro</label>
                        </td>
                        <td>
                            <input id="capitalfuturo" type="text"  style="width: 110px;" readonly>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <label>Intereses Capital Futuro</label>
                        </td>
                        <td>
                            <input id="interesescapitalf" type="text"  style="width: 110px;" readonly>
                        </td>
                    </tr>
                    <tr id="mipyme_">
                        <td>
                            <label>Comisión Ley Mipyme</label>
                        </td>
                        <td>
                            <input id="mipyme" type="text"  style="width: 110px;" readonly>
                        </td>
                    <tr>
                        <td>
                            <label>Seguro</label>
                        </td>
                        <td>
                            <input id="seguro" type="text"  style="width: 110px;" readonly>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <label>Gasto de Cobranza</label>
                        </td>
                        <td>
                            <input id="gac" type="text"  style="width: 110px;" readonly>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <label>Intereses Mora</label>
                        </td>
                        <td>
                            <input id="intereses" type="text"  style="width: 110px;" readonly>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <label>Cuota Administracion Futura</label>
                        </td>
                        <td>
                            <input id="cuotaadminfutura" type="text"  style="width: 110px;" readonly>
                        </td>
                    </tr>                   
                    <tr>
                        <td>
                            <label>Tota a Pagar</label>
                        </td>
                        <td>
                            <input id="total" type="text"  style="width: 110px;" readonly>
                        </td>
                    </tr>
                </table>
            </div>
        </div>

        <div id="dialogMsjliquidacion" style="position: relative;" hidden="true">
            <!--1001590327-->
            <div id="infoNegocio">
                <div id="tablita" class="ui-jqgrid ui-widget ui-widget-content center-block" style="width: 361px; margin-left: 10px;padding: 2px;margin-top: -99px">
                    <table id="tablainterna" style="height: 53px; width: 296px"  >
                        <tr>
                            <td>
                                <label>Cliente</label>
                            </td>
                            <td>
                                <input id="cliente_" type="text"  style="width: 300px;border: 0px;" readonly >
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <label>Negocio</label>
                            </td>
                            <td>
                                <input id="negocioC_" type="text"  style="width: 110px;border: 0px;" readonly>
                            </td>
                        </tr>
                    </table>
                    <div class="ui-dialog-titlebar ui-widget-header ui-helper-clearfix">
                        <label class="titulotablita"><b>Informacion del Negocio</b></label>
                    </div>
                    <table id="tablainterna" style="height: 53px; width: 361px"  >
                        <tr>
                            <td>
                                <label>Valor Negocio</label>
                            </td>
                            <td>
                                <input id="vlr_negocio_" type="text"  style="width: 110px;" readonly>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <label>Tasa de Interes</label>
                            </td>
                            <td>
                                <input id="tasa_" type="text"  style="width: 110px;" readonly>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <label>Numero Cuotas</label>
                            </td>
                            <td>
                                <input id="ncuotas_" type="text"  style="width: 110px;" readonly>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <label>Valor Aval</label>
                            </td>
                            <td>
                                <input id="vlr_aval_" type="text"  style="width: 110px;" readonly>
                            </td>
                        </tr>
                    </table>
                </div>
            </div>
            <div style="position: relative;top: 135px;">
                <table id="tabla_liquidacion" ></table>
                <div id="pager2"></div>
            </div>
            <div id="infoGeneral">
                <div id="tablita" class="ui-jqgrid ui-widget ui-widget-content center-block" style="width: 361px; margin-left: 10px;padding: 2px;top: 150px;">
                    <div class="ui-dialog-titlebar ui-widget-header ui-helper-clearfix">
                        <label class="titulotablita"><b>Informacion General</b></label>
                    </div>
                    <table id="tablainterna" style="height: 53px; width: 361px"  >
                    
                    <tr>
                        <td>
                            <label id="labeldeuda_">Capital</label>
                        </td>
                        <td>
                            <input id="capital_" type="text"  style="width: 110px;" readonly>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <label>Intereses</label>
                        </td>
                        <td>
                            <input id="interes_" type="text"  style="width: 110px;" readonly>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <label>Comisión Ley Mipyme</label>
                        </td>
                        <td>
                            <input id="miPyme_" type="text"  style="width: 110px;" readonly>
                        </td>
                    </tr>
                    <tr id="mipyme_">
                        <td>
                            <label>Intermediación Financiera</label>
                        </td>
                        <td>
                            <input id="cuotaAdmin_" type="text"  style="width: 110px;" readonly>
                        </td>
                    <tr>
                        <td>
                            <label>Seguro</label>
                        </td>
                        <td>
                            <input id="seguro_" type="text"  style="width: 110px;" readonly>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <label>Intereses Mora</label>
                        </td>
                        <td>
                            <input id="interesMora_" type="text"  style="width: 110px;" readonly>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <label>Gasto de Cobranza</label>
                        </td>
                        <td>
                            <input id="gac_" type="text"  style="width: 110px;" readonly>
                        </td>
                    </tr>                   
                    <tr>
                        <td>
                            <label>Tota a Pagar</label>
                        </td>
                        <td>
                            <input id="total_" type="text"  style="width: 110px;" readonly>
                        </td>
                    </tr>
                </table>
                </div>
            </div>
            <div id="tablita" class="ui-jqgrid ui-widget ui-widget-content center-block" style="width: 316px; margin-left: 10px;padding: 2px;top:-26px;float: right;border: 1px rgba(32, 66, 126, 0.84);">
                <table>
                    <tr>
                        <td>
                            <label>Elaborado Por</label>
                        </td>
                        <td>
                            <hr style="width: 210px;top: -25px;margin-left: 13px;color: rgba(128, 128, 128, 0.39);" /> 
                        </td>
                    </tr>
                    <tr><td></td></tr>
                    <tr><td></td></tr>
                    <tr><td></td></tr>
                    <tr><td></td></tr>
                    <tr><td></td></tr>
                    <tr><td></td></tr>
                    <tr><td></td></tr>
                    <tr>
                        <td>
                            <label>Revisado Por</label>
                        </td>
                        <td>
                            <hr style="width: 210px;top: -25px;margin-left: 13px;color: rgba(128, 128, 128, 0.39);" /> 
                        </td>
                    </tr>
                    <tr><td></td></tr>
                    <tr><td></td></tr>
                    <tr><td></td></tr>
                    <tr><td></td></tr>
                    <tr><td></td></tr>
                    <tr><td></td></tr>
                    <tr><td></td></tr>
                    <tr>
                        <td>
                            <label>Recibido Por</label>
                        </td>
                        <td>
                            <hr style="width: 210px;top: -25px;margin-left: 13px;color: rgba(128, 128, 128, 0.39);" /> 
                        </td>
                    </tr>
                </table>
            </div>
        </div>
        <div id="loader-wrapper" style="display: none">
            <img src="http://www.fintra.co/wp-content/uploads/2016/04/fintrafooter.png" alt="" class='loaderimg' />
            <div id="loader">          </div>
            <div class="loader-section section-left"></div>
            <div class="loader-section section-right"></div>
        </div>
        <div id="divSalidaEx" title="Exportacion" style=" display: block" >
            <p  id="msjEx" style=" display:  none"> Espere un momento por favor...</p>
            <center>
                <img id="imgloadEx" style="position: relative;  top: 7px; display: none " src="./images/cargandoCM.gif"/>
            </center>
            <div id="respEx" style=" display: none"></div>
        </div> 
    </center>
</body>
</html>
