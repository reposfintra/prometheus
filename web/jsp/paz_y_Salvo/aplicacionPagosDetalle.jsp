<%-- 
    Document   : aplicacinoPagos
    Created on : 26/03/2018, 10:11:03 AM
    Author     : dvalencia
--%>


<%@page contentType="text/html" pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
        <link href="./css/popup.css" rel="stylesheet" type="text/css">
        <link type="text/css" rel="stylesheet" href="./css/jquery/jquery-ui/jquery-ui.css" />
        <link type="text/css" rel="stylesheet" href="./css/TransportadorasApi.css " />
        <script type="text/javascript" src="./js/jquery/jquery.jqGrid/js/jquery.min.js"></script>
        <script type="text/javascript" src="./js/jquery-ui-1.8.5.custom.min.js"></script>   
        <script type="text/javascript" src="./js/jquery/jquery.jqGrid-4.7.0/js/grid.locale-es.js"></script>
        <script type="text/javascript" src="./js/jquery/jquery.jqGrid-4.7.0/js/jquery.jqGrid.min.js"></script>
        <script type="text/javascript" src="./js/jquery/jquery.jqGrid-4.7.0/js/jquery.tabletojson.js"></script>    
        <script type="text/javascript" src="./js/jquery/jquery.jqGrid-4.7.0/plugins/jquery.contextmenu.js"></script>
        <link type="text/css" rel="stylesheet" href="./css/loaderApi.css">
        <script type="text/javascript" src="./js/utilidadInformacion.js"></script> 
        <script type="text/javascript" src="./js/aplicacionPagosDetalle.js"></script> 
        <title>ESTADO CUENTA</title>
    </head>
    <body>
        <div id="capaSuperior" style="position:absolute; width:100%; height:100px; z-index:0; left: 0px; top: 0px;">
            <jsp:include page="/WEB-INF/toptsp.jsp?encabezado=ESTADO CUENTA"/>
        </div>
    <center>
        <div id="tablita" class="ui-jqgrid ui-widget ui-widget-content center-block" style="width: 478px; margin-top: 80px;margin-left: 67px;padding: 8px;">
            <div class="ui-dialog-titlebar ui-widget-header ui-helper-clearfix">
                <label class="titulotablita"><b>FILTRO DE BUSQUEDA</b></label>
            </div>
            <table id="tablainterna" style="height: 53px; width: 476px"  >
                <tr>
                    <td>
                        <label>No Cedula</label>
                    </td>
                    <td>
                        <input id="cedula" type="text"  style="width: 110px;" onkeyup="funcionFormatos.soloNumero(this)">
                    </td>
                    <td>
                        <label>Negocio</label>
                    </td>
                    <td>
                        <input id="negocio" type="text"  style="width: 110px;" >
                    </td>
                    <td>
                        <hr style="width: 2px;height: 37px;top: -25px;margin-left: 13px;color: rgba(128, 128, 128, 0.39);" /> 
                    </td>
                    <td>
                        <div style="padding-top: 10px">
                            <center>
                                <button type="button" id="buscar" class="ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only" style="margin-left: 15px;top: -7px;" > 
                                    <span class="ui-button-text">Buscar</span>
                                </button>  
                            </center>
                        </div>
                    </td>
                </tr>
            </table>
        </div>

        <div style="position: relative;">
            <table id="tabla_negociosCliente" ></table>
            <div id="pager"></div>
        </div>

        <div id="info"  class="ventana" >
            <p id="notific">EXITO AL GUARDAR</p>
        </div>

        
        <div id="loader-wrapper" style="display: none">
            <img src="http://www.fintra.co/wp-content/uploads/2016/04/fintrafooter.png" alt="" class='loaderimg' />
            <div id="loader">          </div>
            <div class="loader-section section-left"></div>
            <div class="loader-section section-right"></div>
        </div>
        <div id="divSalidaEx" title="Exportacion" style=" display: block" >
            <p  id="msjEx" style=" display:  none"> Espere un momento por favor...</p>
            <center>
                <img id="imgloadEx" style="position: relative;  top: 7px; display: none " src="./images/cargandoCM.gif"/>
            </center>
            <div id="respEx" style=" display: none"></div>
        </div> 
    </center>
</body>
</html>
