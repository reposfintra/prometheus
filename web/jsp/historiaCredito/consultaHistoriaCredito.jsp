<%-- 
    Document   : consultaHistoriaCredito
    Created on : 13/12/2011, 05:48:28 PM
    Author     : darrieta
--%>
<%@page contentType="text/html" pageEncoding="ISO-8859-1"%>
<%@ include file="/WEB-INF/InitModel.jsp"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <%
                    String vista = request.getParameter("vista") != null ? request.getParameter("vista") : "1";
        %>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
        <%if (vista.equals("2")) {%>
        <title>Consulta Historia de credito Fenalco Atlantico</title>
        <%} else {%>
        <title>Consulta Historia de credito</title>
        <%}%>
        <link href="<%= BASEURL%>/css/estilostsp.css" rel='stylesheet'/>
        <script type='text/javascript' src="<%= BASEURL%>/js/boton.js"></script>
        <script type='text/javascript' src="<%= BASEURL%>/js/validar.js"></script>
        <script type='text/javascript' src="<%= BASEURL%>/js/prototype.js"></script>
    </head>    
    <body id="main_body" >
        <div id="capaSuperior" style="position:absolute; width:100%; height:100px; z-index:0; left: 0px; top: 0px;">
            <%if (vista.equals("2")) {%>
            <jsp:include page="/WEB-INF/toptsp.jsp?encabezado=Consulta Historia de credito Fenalco Atlantico"/>
            <%} else {%>
            <jsp:include page="/WEB-INF/toptsp.jsp?encabezado=Consulta Historia de credito"/>
            <%}%>

        </div>
        <div id="capaCentral" style="position:absolute; width:100%; height:100%; z-index:0; left: 0px; top: 100px;">
            <form id="form1" name="form1" method="post" action="<%=CONTROLLER%>?estado=Historia&accion=Credito&opcion=GENERAR_PDF">
                <table align="center" style="border: green solid 1px;" width="700px">
                    <tr class="barratitulo">                  
                        <td class="subtitulo1">Consulta Historia de credito</td>
                        <td><img src="<%=BASEURL%>/images/titulo.gif" width="32" height="20" align="left"/></td>
                    </tr>
                    <tr class="fila">
                        <td>Tipo identificaci&oacute;n</td>
                        <td>
                            <select class="element select medium" id="tipoIdentificacion" name="tipoIdentificacion">
                                <option value="1" selected="selected">Natural nacional</option>
                                <option value="2" >Juridica nacional</option>
                                <option value="3" >Juridica extranjera</option>
                                <option value="4" >Natural extranjera</option>
                            </select>
                        </td>
                    </tr>
                    <tr class="fila">
                        <td>Identificación</td>
                        <td>
                            <input id="identificacion" name="identificacion" type="text" maxlength="255" value=""/>
                            <input id="vista" name="vista" type="hidden"  value="<%=vista%>"/>
                        </td>
                    </tr>
                </table>
                <br/>
                <div align="center">
                    <img style="cursor:hand" src="<%= BASEURL%>/images/botones/aceptar.gif" align="absmiddle" onmouseover="botonOver(this);" onmouseout="botonOut(this);" onClick="form1.submit();"/>
                    <img style="cursor:hand" src="<%= BASEURL%>/images/botones/salir.gif" align="absmiddle" onmouseover="botonOver(this);" onmouseout="botonOut(this);" onClick="parent.close();"/>
                </div>
            </form>
            <%if (request.getParameter("mensaje") != null && !request.getParameter("mensaje").equals("")) {%>
            <br/>
            <table border="2" align="center">
                <tr>
                    <td>
                        <table width="100%"  border="0" align="center"  bordercolor="#f7f5f4" bgcolor="#ffffff">
                            <tr>
                                <td width="229" align="center" class="mensajes"><%=request.getParameter("mensaje")%></td>
                                <td width="29" background="<%=BASEURL%>/images/cuadronaranja.JPG">&nbsp;&nbsp;</td>
                            </tr>
                        </table>
                    </td>
                </tr>
            </table>
            <%}%>
        </div>
    </body>
</html>

