  <!--
     - Author(s)       :      MARIO FONTALVO
     - Date            :      23/04/2007  
     - Copyright Notice:      TRANSPORTES SANCHEZ POLO S.A.
  -->
 <%--   - @(#)  
     - Description:  Vista para solicitar el numero de la remesa que se va acambiar de destino
 --%>
<%@page session    ="true"%> 
<%@page errorPage  ="/error/ErrorPage.jsp"%>
<%@include file    ="/WEB-INF/InitModel.jsp"%>
<%@page import    ="java.util.*" %>
<%@page import    ="com.tsp.util.*" %>
<%@page import    ="com.tsp.operation.model.beans.Usuario" %>
<%@page import    ="com.tsp.operation.model.beans.RemiDest" %>
<%@taglib uri="http://jakarta.apache.org/taglibs/input-0.90" prefix="input" %>


<!-- Para los botones -->
<%@ taglib uri="http://www.sanchezpolo.com/sot" prefix="tsp"%>
<%  String path    = application.getRealPath(model.menuService.getRealUrl(request.getRequestURI()));
    String datos[] = model.menuService.getContenidoMenu(BASEURL,request.getRequestURI(),path);   	
    Usuario usuario   = (Usuario) session.getAttribute("Usuario");
	String  msg       = Util.coalesce((String) request.getAttribute("msg"),"");

	
	String dstrct    = Util.coalesce((String) request.getParameter("dstrct"),"");
	String codcli    = Util.coalesce((String) request.getParameter("codcli"),"");
	String nomcli    = Util.coalesce((String) request.getParameter("nomcli"),"");	
	String ciudad    = Util.coalesce((String) request.getParameter("ciudad"),"NINGUNO");	
	String nomciu    = Util.coalesce((String) request.getParameter("nomciu"),"NINGUNO");	
	String tipo      = Util.coalesce((String) request.getParameter("tipo")  ,"");		
	String modo      = Util.coalesce((String) request.getParameter("modo")  ,"");		
	String numrem    = Util.coalesce((String) request.getParameter("numrem"),"");			
	TreeMap ciudades   = model.CambioDestinoSvc.getCiudades();
%>
<html>
<head>
	<title>SELECCION DE <%= (tipo.equals("RE")?"REMITENTES":"DESTINATARIOS") %></title>
	<link href="<%= BASEURL %>/css/estilostsp.css" rel="stylesheet" type="text/css"> 
	<link href="../../../css/estilostsp.css" rel="stylesheet" type="text/css"> 
    <script type='text/javascript' src="<%= BASEURL %>/js/boton.js"></script>
	<script language="JavaScript1.2">
<!--
   // Maximizar Ventana por Nick Lowe (nicklowe@ukonline.co.uk)

   window.moveTo(0,0);
   if (document.all) {
  	 top.window.resizeTo(screen.availWidth,screen.availHeight);
   }
   else if (document.layers||document.getElementById) {
	   if(top.window.outerHeight<screen.availHeight||top.window.outerWidth<screen.availWidth){
	   top.window.outerHeight = screen.availHeight;
	   top.window.outerWidth = screen.availWidth;
	   }
   }
//-->
</script>
</head>

<body>
<div id="capaSuperior" style="position:absolute; width:100%; height:100px; z-index:0; left: 0px; top: 0px;">
<jsp:include page="/WEB-INF/toptsp.jsp?encabezado=REMITENTES Y DESTINATARIOS"/>
</div>
<div id="capaCentral" style="position:absolute; width:100%; height:85%; z-index:0; left: 0px; top: 100px; overflow: scroll;"> 
<center>


<!-- formulario -->
<form action="<%=CONTROLLER%>?estado=Cambio&accion=Destino" method="post" name="formulario">
<table border="2" width="650">
	<tr>
	<td >
		<!-- encabezado -->
		<table class="tablaInferior" border="0" width="100%">
			<tr >
				<td width="51%" align="left" class="subtitulo1">&nbsp;SELECCION DE <%= (tipo.equals("RE")?"REMITENTES":"DESTINATARIOS") %></td>
				<td width="49%" align="left" class="barratitulo"><img src="<%= BASEURL %>/images/titulo.gif" width="32" height="20"  align="left"><%=datos[0]%></td>		
			</tr>
		</table>	
		<!-- datos del formulario -->
		<table class="tablaInferior" border="0" width="100%">

			<tr class="fila">
				<td width="22%" >&nbsp;REMESA</td>
				<td width="78%">&nbsp;<input type="text" name="numrem" value="<%= numrem %>">
				</td>
			</tr>		
			
			<tr class="fila">
				<td width="22%" >&nbsp;CLIENTE</td>
				<td width="78%">&nbsp;<input type="text" name="nomcli" value="<%= nomcli %>" style="width:99% ">
				</td>
			</tr>					
			
			<tr class="fila" >
				<td width="22%" >&nbsp;CIUDADES</td>
				<td width="78%" >&nbsp;<% if ( !tipo.equals("RE")  ) { %><input:select name="ciudad" options="<%= ciudades %>" attributesText="class='textbox' id='ciudad' style='width:98%' onchange='cargarREMIDEST(false); '" default="<%= ciudad %>" /><% } else { %><input type="text" name="nomciu" value="<%= nomciu %>" style="width:99% " readonly><% } %>
				</td>
			</tr>							
		</table>	
		
		
		<% Vector v = ( tipo.equals("RE")? model.CambioDestinoSvc.getRemitentes() : model.CambioDestinoSvc.getDestinatarios() ) ;
		   if ( v!=null && !v.isEmpty() ) {
		%>
		<table class="tablaInferior" border="0" width="100%">

			<tr class="tblTitulo">
				<td width="5%" class="bordereporte" >&nbsp;</td>
				<td width="44%" class="bordereporte"  >&nbsp;NOMBRE</td>
				<td width="51%" class="bordereporte" >&nbsp;DIRECCION</td>
			</tr>		
			
			<% for (int i = 0; i < v.size(); i++ ) { 
				RemiDest r = (RemiDest) v.get(i);		
			%>
			<tr class="<%= (i%2==0?"filagris":"filaazul") %>" >
				<th class="bordereporte" ><input type="<%= (tipo.equals("RE")?"radio":"checkbox" ) %>" name="LOV" value="<%= i  %>"  <%= r.getTipo() %>></th>
				<td class="bordereporte" nowrap >&nbsp;<%= r.getNombre()    %></td>
				<td class="bordereporte" nowrap >&nbsp;<%= r.getDireccion() %></td>
			</tr>							
			<% } %>
		</table>			
		<% } %>
	</td>
	</tr>
</table>
<p>
	<img src = "<%=BASEURL%>/images/botones/aceptar.gif" style = "cursor:hand" name = "imgaceptar" id="imgaceptar" onClick = "cargarREMIDEST(true);" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);">
	<img src = "<%=BASEURL%>/images/botones/salir.gif" style = "cursor:hand" name = "imgsalir" onClick = "window.close();" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);">
</p>
<input type="hidden" name="dstrct"    value="<%= dstrct %>" readonly>
<input type="hidden" name="codcli"    value="<%= codcli %>" readonly>
<input type="hidden" name="tipo"      value="<%= tipo %>" readonly>
<input type="hidden" name="modo"      value="<%= modo %>" readonly>
<input type="hidden" name="ciudadA"   value="<%= ciudad %>" readonly>
<input type="hidden" name="opcion"    value="">
<input type="hidden" name="operacion" value="ASIGNAR">
<input type="hidden" name="ciudad"    value="<%= ciudad %>">
</form>

<% if( !msg.equals("") ){ %>    
<table border="2" align="center">
	<tr>
		<td><table width="100%"  border="1" align="center"  bordercolor="#F7F5F4" bgcolor="#FFFFFF">
		<tr> 
		<td width="350" align="center" class="mensajes"><%= msg %></td>
		<td width="29" background="<%=BASEURL%>/images/cuadronaranja.JPG">&nbsp;</td>
		<td width="58">&nbsp;</td>
	</tr>
	</table></td>
	</tr>
</table>                   
<%} %>
			
			
</center>
</div>
<%=datos[1]%>
<iframe width=188 height=166 name="gToday:normal:<%=BASEURL%>/js/calendartsp/agenda.js:gfPop:<%=BASEURL%>/js/calendartsp/plugins.js" id="gToday:normal:<%=BASEURL%>/js/calendartsp/agenda.js:gfPop:<%=BASEURL%>/js/calendartsp/plugins_24.js" src="<%=BASEURL%>/js/calendartsp/ipopeng.htm" scrolling="no" frameborder="0" style="visibility:visible; z-index:999; position:absolute; left:-500px; top:0px;">
</iframe>
</body>
</html>
<script>
	function validar(){
		with (formulario){
			// validacion de cuentas
			if (numrem.value==''){
				alert('Ingrese la remesa a buscar.');
				return false;
			}
			
			submit();
		}
	}
	
	formulario.numrem.focus();
	

	function cargarREMIDEST( exit ) {
		with (formulario){			
			opcion.value = tipo.value=='RE'?'LOAD_REMITENTES':'LOAD_DESTINATARIOS';
			submit();
		}
		
		if (exit){
			window.close()
		}
	}	
</script>