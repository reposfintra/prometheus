  <!--
     - Author(s)       :      MARIO FONTALVO
     - Date            :      23/04/2007  
     - Copyright Notice:      TRANSPORTES SANCHEZ POLO S.A.
  -->
 <%--   - @(#)  
     - Description:  Vista para solicitar el numero de la remesa que se va acambiar de destino
 --%>
<%@page session    ="true"%> 
<%@page errorPage  ="/error/ErrorPage.jsp"%>
<%@include file    ="/WEB-INF/InitModel.jsp"%>
<%@page import    ="java.util.*" %>
<%@page import    ="com.tsp.util.*" %>
<%@page import    ="com.tsp.operation.model.beans.Usuario" %>

<!-- Para los botones -->
<%@ taglib uri="http://www.sanchezpolo.com/sot" prefix="tsp"%>
<%  String path    = application.getRealPath(model.menuService.getRealUrl(request.getRequestURI()));
    String datos[] = model.menuService.getContenidoMenu(BASEURL,request.getRequestURI(),path);   	
    Usuario usuario   = (Usuario) session.getAttribute("Usuario");
	String  msg       = Util.coalesce((String) request.getAttribute("msg"),"");
	String  numrem    = Util.coalesce((String) request.getAttribute("numrem"),"");
	String  dstrct    = usuario.getDstrct();
%>
<html>
<head>
	<title>CAMBIO DESTINO DE LA REMESA</title>
	<link href="<%= BASEURL %>/css/estilostsp.css" rel="stylesheet" type="text/css"> 
	<link href="../../../css/estilostsp.css" rel="stylesheet" type="text/css"> 
    <script type='text/javascript' src="<%= BASEURL %>/js/boton.js"></script>
	<script language="JavaScript1.2">
<!--
   // Maximizar Ventana por Nick Lowe (nicklowe@ukonline.co.uk)

   window.moveTo(0,0);
   if (document.all) {
  	 top.window.resizeTo(screen.availWidth,screen.availHeight);
   }
   else if (document.layers||document.getElementById) {
	   if(top.window.outerHeight<screen.availHeight||top.window.outerWidth<screen.availWidth){
	   top.window.outerHeight = screen.availHeight;
	   top.window.outerWidth = screen.availWidth;
	   }
   }
//-->
</script>
</head>

<body>
<div id="capaSuperior" style="position:absolute; width:100%; height:100px; z-index:0; left: 0px; top: 0px;">
<jsp:include page="/WEB-INF/toptsp.jsp?encabezado=CAMBIO DESTINO DE LA REMESA"/>
</div>
<div id="capaCentral" style="position:absolute; width:100%; height:85%; z-index:0; left: 0px; top: 100px; overflow: scroll;"> 
<center>


<!-- formulario -->
<form action="<%=CONTROLLER%>?estado=Cambio&accion=Destino" method="post" name="formulario">
<table border="2" width="379">
	<tr>
	<td >
		<!-- encabezado -->
		<table class="tablaInferior" border="0" width="100%">
			<tr >
				<td width="51%" align="left" class="subtitulo1">&nbsp;CAMBIO DESTINO</td>
				<td width="49%" align="left" class="barratitulo"><img src="<%= BASEURL %>/images/titulo.gif" width="32" height="20"  align="left"><%=datos[0]%></td>		
			</tr>
		</table>	
		<!-- datos del formulario -->
		<table class="tablaInferior" border="0" width="100%">

			<tr class="fila">
				<td width="38%" >&nbsp;Remesa</td>
				<td width="62%">&nbsp;<input type="text" name="numrem" value="<%= numrem %>">
				</td>
			</tr>						
		</table>	
	</td>
	</tr>
</table>
<p>
	<img src = "<%=BASEURL%>/images/botones/aceptar.gif" style = "cursor:hand" name = "imgaceptar" id="imgaceptar" onClick = "validar();" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);">
	<img src = "<%=BASEURL%>/images/botones/salir.gif" style = "cursor:hand" name = "imgsalir" onClick = "window.close();" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);">
</p>
<input type="hidden" name="dstrct" value="<%= dstrct %>" readonly>
<input type="hidden" name="opcion"    value="BUSCAR">
</form>

<% if( !msg.equals("") ){ %>    
<table border="2" align="center">
	<tr>
		<td><table width="100%"  border="1" align="center"  bordercolor="#F7F5F4" bgcolor="#FFFFFF">
		<tr> 
		<td width="350" align="center" class="mensajes"><%= msg %></td>
		<td width="29" background="<%=BASEURL%>/images/cuadronaranja.JPG">&nbsp;</td>
		<td width="58">&nbsp;</td>
	</tr>
	</table></td>
	</tr>
</table>                   
<%} %>
			
			
</center>
</div>
<%=datos[1]%>
<iframe width=188 height=166 name="gToday:normal:<%=BASEURL%>/js/calendartsp/agenda.js:gfPop:<%=BASEURL%>/js/calendartsp/plugins.js" id="gToday:normal:<%=BASEURL%>/js/calendartsp/agenda.js:gfPop:<%=BASEURL%>/js/calendartsp/plugins_24.js" src="<%=BASEURL%>/js/calendartsp/ipopeng.htm" scrolling="no" frameborder="0" style="visibility:visible; z-index:999; position:absolute; left:-500px; top:0px;">
</iframe>
</body>
</html>
<script>
	function validar(){
		with (formulario){
			// validacion de cuentas
			if (numrem.value==''){
				alert('Ingrese la remesa a buscar.');
				return false;
			}
			
			submit();
		}
	}
	
	formulario.numrem.focus();
</script>