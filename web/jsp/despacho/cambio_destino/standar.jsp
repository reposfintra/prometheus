  <!--
     - Author(s)       :      MARIO FONTALVO
     - Date            :      23/04/2007  
     - Copyright Notice:      TRANSPORTES SANCHEZ POLO S.A.
  -->
 <%--   - @(#)  
     - Description:  Vista para solicitar el numero de la remesa que se va acambiar de destino
 --%>
<%@page session   ="true"%> 
<%@page errorPage ="/error/ErrorPage.jsp"%>
<%@include file   ="/WEB-INF/InitModel.jsp"%>
<%@page import    ="java.util.*" %>
<%@page import    ="com.tsp.util.*" %>
<%@page import    ="com.tsp.operation.model.beans.Usuario" %>
<%@taglib uri="http://jakarta.apache.org/taglibs/input-0.90" prefix="input" %>

<!-- Para los botones -->
<%@ taglib uri="http://www.sanchezpolo.com/sot" prefix="tsp"%>
<%  String path      = application.getRealPath(model.menuService.getRealUrl(request.getRequestURI()));
    String datos[]   = model.menuService.getContenidoMenu(BASEURL,request.getRequestURI(),path);   	
	String msg       = Util.coalesce((String) request.getAttribute("msg"),"");
	
	String dstrct    = Util.coalesce((String) request.getParameter("dstrct"),"");
	String codcli    = Util.coalesce((String) request.getParameter("codcli"),"");
	String nomcli    = Util.coalesce((String) request.getParameter("nomcli"),"");
	
	String origen    = Util.coalesce((String) request.getParameter("origen"),"NINGUNO");
	String destino   = Util.coalesce((String) request.getParameter("destino"),"NINGUNO");
	String estandar  = Util.coalesce((String) request.getParameter("estandar"),"NINGUNO");
	
	TreeMap origenes   = model.CambioDestinoSvc.getOrigenes();
	TreeMap destinos   = model.CambioDestinoSvc.getDestinos();
	TreeMap estandares = model.CambioDestinoSvc.getEstandares();	
%>


<html>
<head>
	<title>SELECCION ESTANDAR</title>
	<link href="<%= BASEURL %>/css/estilostsp.css" rel="stylesheet" type="text/css"> 
	<link href="../../../css/estilostsp.css" rel="stylesheet" type="text/css"> 
    <script type='text/javascript' src="<%= BASEURL %>/js/boton.js"></script>
	<script language="JavaScript1.2">
<!--
   // Maximizar Ventana por Nick Lowe (nicklowe@ukonline.co.uk)

   window.moveTo(0,0);
   if (document.all) {
  	 top.window.resizeTo(screen.availWidth,screen.availHeight);
   }
   else if (document.layers||document.getElementById) {
	   if(top.window.outerHeight<screen.availHeight||top.window.outerWidth<screen.availWidth){
	   top.window.outerHeight = screen.availHeight;
	   top.window.outerWidth = screen.availWidth;
	   }
   }
//-->
</script>
</head>

<body>
<div id="capaSuperior" style="position:absolute; width:100%; height:100px; z-index:0; left: 0px; top: 0px;">
<jsp:include page="/WEB-INF/toptsp.jsp?encabezado=SELECCION ESTANDAR"/>
</div>
<div id="capaCentral" style="position:absolute; width:100%; height:85%; z-index:0; left: 0px; top: 100px; overflow: scroll;"> 
<center>

<!-- formulario -->
<form action="<%=CONTROLLER%>?estado=Cambio&accion=Destino" method="post" name="formulario">
<table border="2" width="540">
	<tr>
	<td >
		<!-- encabezado -->
		<table class="tablaInferior" border="0" width="100%">
			<tr >
				<td width="57%" align="left" class="subtitulo1">&nbsp;SELECCION NUEVO ESTANDAR</td>
				<td width="43%" align="left" class="barratitulo"><img src="<%= BASEURL %>/images/titulo.gif" width="32" height="20"  align="left"><%=datos[0]%></td>		
			</tr>
		</table>	
		<!-- datos del formulario -->
		<table class="tablaInferior" border="0" width="100%">

			<tr >
				<td width="25%" class="fila">&nbsp;CLIENTE</td>
				<td width="75%" class="letra">&nbsp;<%= nomcli %>
				</td>
			</tr>		
			
			
			<% if (origenes!=null) { %>			
			<tr >
				<td width="25%" class="fila">&nbsp;ORIGENES</td>
				<td width="75%" class="letra">&nbsp;<input:select name="origen" options="<%= origenes %>" attributesText="class='textbox' id='origen' style='width:98%' onchange='loadDestinos();'" default="<%= origen %>" />
				</td>
			</tr>
			<% } %>
			
			
			<% if (destinos!=null && !destinos.isEmpty()) { %>
			<tr >
				<td width="25%" class="fila">&nbsp;DESTINOS</td>
				<td width="75%" class="letra">&nbsp;<input:select name="destino" options="<%= destinos %>" attributesText="class='textbox' id='destino' style='width:98%'  onchange='loadEstandares();'" default="<%= destino %>" />
				</td>
			</tr>						
			<% } %>
			
			<% if (estandares!=null && !estandares.isEmpty()) { %>
			<tr >
				<td width="25%" class="fila">&nbsp;ESTANDARES</td>
				<td width="75%" class="letra">&nbsp;<input:select name="estandar" options="<%= estandares %>" attributesText="class='textbox' id='estandar' style='width:98%'" default="<%= estandar %>" />
				</td>
			</tr>											
			<% } %>
		</table>	
	</td>
	</tr>
</table>
<p>
	<img src = "<%=BASEURL%>/images/botones/aceptar.gif" style = "cursor:hand" name = "imgaceptar" id="imgaceptar" onClick = "validar();" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);">
	<img src = "<%=BASEURL%>/images/botones/salir.gif" style = "cursor:hand" name = "imgsalir" onClick = "window.close();" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);">
</p>
<input type="hidden" name="dstrct"  value="<%= dstrct %>" readonly>
<input type="hidden" name="codcli"  value="<%= codcli %>" readonly>
<input type="hidden" name="nomcli"  value="<%= nomcli %>" readonly>
<input type="hidden" name="opcion"  value="LOAD_ESTANDAR">
</form>

<% if( !msg.equals("") ){ %>    
<table border="2" align="center">
	<tr>
		<td><table width="100%"  border="1" align="center"  bordercolor="#F7F5F4" bgcolor="#FFFFFF">
		<tr> 
		<td width="350" align="center" class="mensajes"><%= msg %></td>
		<td width="29" background="<%=BASEURL%>/images/cuadronaranja.JPG">&nbsp;</td>
		<td width="58">&nbsp;</td>
	</tr>
	</table></td>
	</tr>
</table>                   
<%} %>
			
			
</center>
</div>
<%=datos[1]%>
<iframe width=188 height=166 name="gToday:normal:<%=BASEURL%>/js/calendartsp/agenda.js:gfPop:<%=BASEURL%>/js/calendartsp/plugins.js" id="gToday:normal:<%=BASEURL%>/js/calendartsp/agenda.js:gfPop:<%=BASEURL%>/js/calendartsp/plugins_24.js" src="<%=BASEURL%>/js/calendartsp/ipopeng.htm" scrolling="no" frameborder="0" style="visibility:visible; z-index:999; position:absolute; left:-500px; top:0px;">
</iframe>
</body>
</html>
<script>

	function loadDestinos() {
		with (formulario){
		  	var url = '<%= CONTROLLER %>?estado=Cambio&accion=Destino&opcion=LOAD_DESTINOS&dstrct='+ dstrct.value +'&codcli='+codcli.value+'&nomcli='+nomcli.value+'&origen='+origen.value+'&destino=NINGUNO';
			window.location = url;
		}
	}

	function loadEstandares() {
		with (formulario){
		  	var url = '<%= CONTROLLER %>?estado=Cambio&accion=Destino&opcion=LOAD_ESTANDARES&dstrct='+ dstrct.value +'&codcli='+codcli.value+'&nomcli='+nomcli.value+'&origen='+origen.value+'&destino='+destino.value;
			window.location = url;
		}
	}
	
	function validar(){
		with (formulario){
		    var est = document.getElementById('estandar');
			if (est!=null && est.value!='NINGUNO'){		
				var url = '<%= CONTROLLER %>?estado=Cambio&accion=Destino&opcion=REMPLAZO&dstrct='+ dstrct.value +'&codcli='+codcli.value+'&estandar='+estandar.value;
				window.opener.location = url;
				window.close();
			} else {
				alert ('Debe seleccionar un estandar para poder continuar');
			}
		}
	}
	
</script>