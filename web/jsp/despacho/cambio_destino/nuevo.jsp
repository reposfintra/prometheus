  <!--
     - Author(s)       :      MARIO FONTALVO
     - Date            :      23/04/2007  
     - Copyright Notice:      TRANSPORTES SANCHEZ POLO S.A.
  -->
 <%--   - @(#)  
     - Description:  Vista para solicitar el numero de la remesa que se va acambiar de destino
 --%>
<%@page session   ="true"%> 
<%@page errorPage ="/error/ErrorPage.jsp"%>
<%@include file   ="/WEB-INF/InitModel.jsp"%>
<%@page import    ="java.util.*" %>
<%@page import    ="com.tsp.util.*" %>
<%@page import    ="com.tsp.operation.model.beans.Remesa2" %>
<%@page import    ="com.tsp.operation.model.beans.Usuario" %>

<!-- Para los botones -->
<%@ taglib uri="http://www.sanchezpolo.com/sot" prefix="tsp"%>
<%  String path    = application.getRealPath(model.menuService.getRealUrl(request.getRequestURI()));
    String datos[] = model.menuService.getContenidoMenu(BASEURL,request.getRequestURI(),path);   	
    Usuario usuario   = (Usuario) session.getAttribute("Usuario");
	String  msg       = Util.coalesce((String) request.getAttribute("msg"),"");
	
	
	Remesa2 rem  = model.CambioDestinoSvc.getRemesa();	
	Remesa2 rem2 = model.CambioDestinoSvc.getRemesaRemplazo();	
	
%>
<html>
<head>
	<title>CAMBIO DESTINO DE LA REMESA</title>
	<link href="<%= BASEURL %>/css/estilostsp.css" rel="stylesheet" type="text/css"> 
	<link href="../../../css/estilostsp.css" rel="stylesheet" type="text/css"> 
    <script type='text/javascript' src="<%= BASEURL %>/js/boton.js"></script>
    <script type='text/javascript' src="<%= BASEURL %>/js/validar.js"></script> 
	<script type='text/javascript' src="<%= BASEURL %>/js/finanzas/contab/grabacionComprobante.js"></script>
	<script language="JavaScript1.2">
//alert(0);
<!--
   // Maximizar Ventana por Nick Lowe (nicklowe@ukonline.co.uk)
   window.moveTo(0,0);
   if (document.all) {
  	 top.window.resizeTo(screen.availWidth,screen.availHeight);
   }
   else if (document.layers||document.getElementById) {
	   if(top.window.outerHeight<screen.availHeight||top.window.outerWidth<screen.availWidth){
	   top.window.outerHeight = screen.availHeight;
	   top.window.outerWidth = screen.availWidth;
	   }
   }
//-->
</script>
</head>

<body>
<div id="capaSuperior" style="position:absolute; width:100%; height:100px; z-index:0; left: 0px; top: 0px;">
<jsp:include page="/WEB-INF/toptsp.jsp?encabezado=CAMBIO DESTINO DE LA REMESA"/>
</div>
<div id="capaCentral" style="position:absolute; width:100%; height:85%; z-index:0; left: 0px; top: 100px; overflow: scroll;"> 
<center>


<!-- formulario -->
<% if (rem != null) { %>
<form action="<%=CONTROLLER%>?estado=Cambio&accion=Destino" method="post" name="formulario">
<table border="2" width="790">
	<tr>
	<td >
		<!-- encabezado -->
		<table class="tablaInferior" border="0" width="100%">
			<tr >
				<td width="51%" align="left" class="subtitulo1">&nbsp;CAMBIO DESTINO DE LA REMESA</td>
				<td width="49%" align="left" class="barratitulo"><img src="<%= BASEURL %>/images/titulo.gif" width="32" height="20"  align="left"><%=datos[0]%></td>		
			</tr>
		</table>	
		<!-- datos del formulario -->
		<table class="tablaInferior" border="0" width="100%">
			
			<tr >
				<td class="fila" width="13%">&nbsp;REMESA</td>
				<td class="letra" width="14%">&nbsp;<%= rem.getNumrem() %></td>
				<td class="fila" width="10%" >&nbsp;ESTADO</td>				
				<td class="letra" colspan="2">&nbsp;<%= ( (rem.getReg_status().equals("A")?"ANULADA":rem.getReg_status().equals("C")?"CUMPLIDA":"ACTIVA")) + (rem.getDoc_fac().equals("")?"":" FACTURADA")  %></td>
				<td class="fila" width="16%">&nbsp;FECHA CREACION</td>
				<td class="letra" width="24%">&nbsp;<%= rem.getFecrem() %></td>
			</tr>		
			<tr>
				<td class="fila" width="13%" >&nbsp;CLIENTE</td>
				<td class="letra" colspan="4">&nbsp;<%=  rem.getCliente() + " " + rem.getNomcli() %></td>
				<td class="fila" width="16%" >&nbsp;AGENCIA</td>
				<td class="letra">&nbsp;<%= rem.getNomagc() %></td>
			</tr>													
		</table>	
		
		
		<table class="tablaInferior" border="0" width="100%">

			<tr class="tblTitulo2">
				<td colspan="7">&nbsp;DATOS ACTUALES DE LA REMESA
				</td>
			</tr>			
	
			<tr>
				<td class="fila" >&nbsp;ESTANDAR</td>
				<td class="letra" >&nbsp;<%= rem.getStd_job_no() %></td>
				<td class="fila" width="20%" >&nbsp;RUTA ESTANDAR </td>
				<td class="letra" colspan="3" >&nbsp;<%= rem.getNomori() + " - " + rem.getNomdes() %></td>
			</tr>		
			<tr>
				<td class="fila" >&nbsp;DESCRIPCION</td>
				<td class="letra" colspan="5" >&nbsp;<%= rem.getDescripcion() %></td>
			</tr>									
			<tr >				
				<td width="18%" class="fila" >&nbsp;CANT. A FACTURAR</td>
				<td width="19%" align="right" class="letra">&nbsp;<%= Util.customFormat(rem.getPesoreal()) %>&nbsp;&nbsp;&nbsp;</td>
				<td class="fila" >&nbsp;TARIFA</td>
				<td class="letra" align="right">&nbsp;<%= Util.customFormat(rem.getQty_value()) %>&nbsp;&nbsp;&nbsp;</td>
				<td width="10%"   class="fila">&nbsp;Unidad</td>
				<td width="13%"   class="letra">&nbsp;<%= rem.getDesc_unidad()  %></td>
			</tr>								
			<tr>
				<td class="fila"  >&nbsp;VALOR REMESA</td>
				<td class="letra" align="right">&nbsp;<%= Util.customFormat(rem.getVlrrem()) %>&nbsp;&nbsp;&nbsp;</td>	
				<td class="fila"  >&nbsp;VALOR REMESA LOCAL </td>
				<td class="letra" align="right" colspan="1">&nbsp;<%= Util.customFormat(rem.getVlrrem2()) %>&nbsp;&nbsp;&nbsp;</td>	
				<td width="10%"  class="fila">&nbsp;Moneda</td>
				<td width="13%"  class="letra">&nbsp;<%= rem.getCurrency() %></td>				
			</tr>				
			<tr class="fila">
				<td colspan="7" align="center" >
				<span style="width:25% "><input type="checkbox" disabled <%= (rem.getCrossdocking().equals("S")?"checked":"" ) %>>&nbsp;CROSSDOCKING</span>
				<span style="width:25% "><input type="checkbox" disabled <%= (rem.getCadena().equals("S")?"checked":"" ) %>>&nbsp;CADENA</span>
				<span style="width:25% "><input type="checkbox" disabled <%= (rem.getN_facturable().equals("S")?"checked":"" ) %>>&nbsp;NO FACTURABLE</span>
				</td>
			</tr>	
			<tr class="fila">
				<td colspan="7" align="center" >
					<a href="javascript: cargarREMIDEST('RE','CONSULTA','<%= rem.getOrirem() %>','<%= rem.getNomori() %>');" class="Simulacion_Hiper">Remitentes</a>&nbsp;&nbsp;
					<a href="javascript: cargarREMIDEST('DE','CONSULTA','<%= rem.getDesrem() %>','<%= rem.getNomdes() %>');" class="Simulacion_Hiper">Destinatarios</a>
				</td>
			</tr>										
		</table>			
		

		<table class="tablaInferior" border="0" width="100%">

			<tr class="tblTitulo2">
				<td colspan="7">&nbsp;NUEVOS DATOS DE LA REMESA
				</td>
			</tr>			
	
			<tr>
				<td class="fila" >&nbsp;ESTANDAR</td>
				<td class="letra" align="left" >
					<input name="std_job_no" type="text" style="text-align:center; width:70% " value="<%= (rem2!=null?rem2.getStd_job_no():"") %>" maxlength="6" onChange="loadEstandar('<%= rem.getReg_status() %>','<%= rem.getDoc_fac() %>');">				
					<img src="<%= BASEURL %>/images/botones/iconos/lupa.gif" style="cursor:hand; " width="16" onClick="nuevoEstandar('<%= rem.getReg_status() %>','<%= rem.getDoc_fac() %>');"></td>
				<td class="fila" width="20%" >&nbsp;RUTA ESTANDAR </td>
				<td class="letra" colspan="3" align="left" ><input type="text" name="ruta" value="<%= (rem2!=null?rem2.getNomori() + " - " + rem2.getNomdes():"") %>" style="text-align:left; width:99% " readonly></td>
			</tr>		
			<tr>
				<td class="fila" >&nbsp;DESCRIPCION</td>
				<td class="letra" colspan="5" align="left" ><input type="text" name="descripcion" value="<%= (rem2!=null?rem2.getDescripcion():"") %>" style="text-align:left; width:99% " readonly></td>
			</tr>
			
			<tr >				
				<td width="18%" class="fila" >&nbsp;CANT. A FACTURAR</td>
				<td width="19%" align="right" class="letra"><input name="pesoreal" type="text" style="text-align:right; width:99% " onFocus="this.select();" onChange="formatear(this); actualizarValores();" onKeyPress="soloDigitos(event, 'decOK')" value="<%=(rem2!=null?Util.customFormat(rem2.getPesoreal()):"") %>" maxlength="7" ></td>
				<td class="fila" >&nbsp;TARIFA</td>
				<td class="letra"  align="right"><input type="text" name="tarifa" value="<%= (rem2!=null? Util.customFormat(rem2.getTarifa()) :"" ) %>" style="text-align:right; width:99% " readonly></td>
				<td width="10%"   class="fila">&nbsp;Unidad</td>
				<td width="13%"  class="letra"><input type="text" name="unidad" value="<%= (rem2!=null?rem2.getDesc_unidad() :"") %>" style="text-align:left; width:99% " readonly></td>
			</tr>								
			<tr>
				<td class="fila">&nbsp;VALOR REMESA</td>
				<td class="letra"  align="right"><input type="text" name="vlrrem" value="<%= (rem2!=null?Util.customFormat(rem2.getVlrrem()) : "" )  %>" style="text-align:right; width:99% " readonly></td>	
				<td class="fila">&nbsp;VALOR REMESA LOCAL </td>
				<td class="letra" align="right" colspan="1"><input type="text" name="vlrrem2" value="<%= (rem2!=null?  Util.customFormat(rem2.getVlrrem2()) : "" )  %>" style="text-align:right; width:99% " readonly></td>	
				<td width="10%"  class="fila">&nbsp;Moneda</td>
				<td width="13%"  class="letra"><input type="text" name="moneda" value="<%= (rem2!=null?rem2.getCurrency():"") %>" style="text-align:left; width:99% " readonly></td>				
			</tr>		
															
					
			<tr class="fila">
				<td colspan="7" align="center" >
				<span style="width:25% "><input type="checkbox" name="crossdocking"  <%= (rem2!=null && rem2.getCrossdocking().equals("S")?"checked":"" ) %>>&nbsp;CROSSDOCKING</span>
				<span style="width:25% "><input type="checkbox" name="cadena"        <%= (rem2!=null && rem2.getCadena().equals("S")?"checked":"" ) %>>&nbsp;CADENA</span>
				<span style="width:25% "><input type="checkbox" name="n_facturable"  <%= (rem2!=null && rem2.getN_facturable().equals("S")?"checked":"" ) %>>&nbsp;NO FACTURABLE</span>
				</td>
			</tr>						
			<tr class="fila">
				<td colspan="7" align="center" >
					<a href="javascript: cargarREMIDEST('RE','MODIFICACION','<%= (rem2!=null?rem2.getOrirem():"") %>','<%= (rem2!=null?rem2.getNomori():"") %>');" class="Simulacion_Hiper">Remitentes</a>&nbsp;&nbsp;
					<a href="javascript: cargarREMIDEST('DE','MODIFICACION','<%= (rem2!=null?rem2.getDesrem():"") %>','<%= (rem2!=null?rem2.getNomdes():"") %>');" class="Simulacion_Hiper">Destinatarios</a>
				</td>
			</tr>			
		</table>			
		
				
		
	</td>
	</tr>
</table>
<p>
	<img src = "<%=BASEURL%>/images/botones/aceptar.gif" style = "cursor:hand" name = "imgaceptar" id="imgaceptar" onClick = "validar('<%= rem.getReg_status() %>','<%= rem.getDoc_fac() %>');" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);">
	<img src = "<%=BASEURL%>/images/botones/regresar.gif" style = "cursor:hand" name = "imgregresar" onClick = "window.location = '<%= BASEURL%>/jsp/despacho/cambio_destino/consulta.jsp' ;" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);">	
	<img src = "<%=BASEURL%>/images/botones/salir.gif" style = "cursor:hand" name = "imgsalir" onClick = "window.close();" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);">
</p>
<input type="hidden" name="dstrct"       value="<%= rem.getCia() %>">
<input type="hidden" name="codcli"       value="<%= rem.getCliente() %>">
<input type="hidden" name="nomcli"       value="<%= rem.getNomcli() %>">
<input type="hidden" name="numrem"       value="<%= rem.getNumrem() %>">
<input type="hidden" name="tasa1"        value="<%= rem.getTasa() %>">
<input type="hidden" name="opcion"       value="CAMBIO_DESTINO">
<input type="hidden" name="tasa2"      value="<%= (rem2!=null?rem2.getTasa():0) %>">
</form>
<% } %>

<% if( !msg.equals("") ){ %>    
<!--<table border="2" align="center">
	<tr>
		<td><table width="100%"  border="1" align="center"  bordercolor="#F7F5F4" bgcolor="#FFFFFF">
		<tr> 
		<td width="350" align="center" class="mensajes"><%= msg %></td>
		<td width="29" background="<%=BASEURL%>/images/cuadronaranja.JPG">&nbsp;</td>
		<td width="58">&nbsp;</td>
	</tr>
	</table></td>
	</tr>
</table>                   -->
<script>alert('<%= msg %>');</script>
<%} %>
			
			
</center>
</div>
<%=datos[1]%>
<iframe width=188 height=166 name="gToday:normal:<%=BASEURL%>/js/calendartsp/agenda.js:gfPop:<%=BASEURL%>/js/calendartsp/plugins.js" id="gToday:normal:<%=BASEURL%>/js/calendartsp/agenda.js:gfPop:<%=BASEURL%>/js/calendartsp/plugins_24.js" src="<%=BASEURL%>/js/calendartsp/ipopeng.htm" scrolling="no" frameborder="0" style="visibility:visible; z-index:999; position:absolute; left:-500px; top:0px;">
</iframe>
</body>
</html>
<script>

	function validar(estado, factura){
		with (formulario){
			// validacion de cuentas
			if (std_job_no.value==''){
				alert('Ingrese el nuevo estandar.');				
				return false;
			}

			if( sePuedeCambiar (estado, factura) )
				submit();
		}
	}
	
	function nuevoEstandar(estado, factura) {
		with (formulario){
			if (sePuedeCambiar(estado, factura)){
				var url = '<%= CONTROLLER %>?estado=Cambio&accion=Destino&opcion=LOAD_ORIGENES&dstrct='+ dstrct.value +'&codcli='+codcli.value+'&nomcli='+nomcli.value+'&origen=NINGUNO';
				var win = window.open (url,'Estandar','resizable, menubar=no, status=yes ');
				win.focus();
			}
		}
	}

	function cargarREMIDEST(tipo, modo, ciudad, nomciu) {
		with (formulario){			
			var op = (tipo=='RE'?'LOAD_REMITENTES':'LOAD_DESTINATARIOS')		
		  	var url = '<%= CONTROLLER %>?estado=Cambio&accion=Destino&opcion='+ op +'&dstrct='+ dstrct.value +'&codcli='+codcli.value+'&nomcli='+nomcli.value+'&ciudad=' + ciudad+'&nomciu=' + nomciu +'&numrem=' + numrem.value + '&tipo=' + tipo + '&modo=' + modo+'&operacion=LOAD_CIUDAD';
			var win = window.open (url,'Estandar','resizable, menubar=no, status=yes ');
			win.focus();
		}
	}
	
	function loadEstandar(estado, factura){
		with (formulario){
			if (std_job_no.value!='' && std_job_no.value!='NINGUNO'){		
				if (sePuedeCambiar(estado, factura)){
					var url = '<%= CONTROLLER %>?estado=Cambio&accion=Destino&opcion=REMPLAZO&dstrct='+ dstrct.value +'&codcli='+codcli.value+'&estandar='+std_job_no.value;
					window.location = url;
				}
			} else {
				alert ('Debe indicar un estandar para poder continuar');
				formulario.reset();
			}
		}
	}
	function actualizarValores(){
		with (formulario){
			var cant  = (pesoreal.value    !=''? parseFloat( sinformato(pesoreal.value    )) : 0 );
			var tari  = (tarifa.value      !=''? parseFloat( sinformato(tarifa.value      )) : 0 );
			var tasa  = (tasa2.value       !=''? parseFloat( sinformato(tasa2.value  )) : 0 );
			vlrrem.value  = redondear( (cant * tari ), moneda.value);
			vlrrem2.value = redondear( (cant * tari * tasa ), 'PES');
			formatear(vlrrem);
			formatear(vlrrem2);
		}	
	}
	function redondear(valor, moneda){
		if (moneda == 'DOL'){
			valor = valor*100;
			valor = Math.round(valor);
			valor = valor/100;
		} else {
			valor = Math.round(valor);
		}
		return valor;
	}
	
	
	function sePuedeCambiar(estado, factura){
		if (estado=='A'){
			alert('La remesa esta anulada, no se puede cambiar el estandar');
			return false;
			formulario.reset();
		}
		if (factura!=''){
			alert('La remesa esta facturada, no se puede cambiar el estandar');
			return false;
			formulario.reset();			
		}
		return true;		
	}
	
</script>