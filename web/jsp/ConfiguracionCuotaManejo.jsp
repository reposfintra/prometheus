<%-- 
    Document   : ConfiguracionCuotaManejo
    Created on : 7/07/2016, 09:22:06 AM
    Author     : egonzalez
--%>

<%@page contentType="text/html" pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
        <link href="./css/popup.css" rel="stylesheet" type="text/css">
        <link type="text/css" rel="stylesheet" href="./css/jquery/jquery-ui/jquery-ui.css" />
        <link type="text/css" rel="stylesheet" href="./css/TransportadorasApi.css " />
        <script type="text/javascript" src="./js/jquery/jquery.jqGrid/js/jquery.min.js"></script>
        <script type="text/javascript" src="./js/jquery-ui-1.8.5.custom.min.js"></script>   
        <script type="text/javascript" src="./js/jquery/jquery.jqGrid-4.7.0/js/grid.locale-es.js"></script>
        <script type="text/javascript" src="./js/jquery/jquery.jqGrid-4.7.0/js/jquery.jqGrid.min.js"></script>
        <script type="text/javascript" src="./js/jquery/jquery.jqGrid-4.7.0/js/jquery.tabletojson.js"></script>    
        <script type="text/javascript" src="./js/ConfiguracionCuotaManejo.js"></script> 
        <title>CUOTAS DE MANEJO</title>
    </head>
    <body>
        <div id="capaSuperior" style="position:absolute; width:100%; height:100px; z-index:0; left: 0px; top: 0px;">
            <jsp:include page="/WEB-INF/toptsp.jsp?encabezado=CUOTAS DE MANEJO"/>
        </div>
    <center>
        <div style="position: relative;top: 231px;">
            <table id="tabla_cuota_manejo" ></table>
            <div id="pager"></div>
        </div>

        <div id="dialogsalarioCuota"  class="ventana" >
            <div id="tablainterna" style="width: 1300px;" >
                <table id="tablainterna"  >
                    <tr>
                        <td>
                            <input type="text" id="id" style="width: 137px;color: black;" hidden>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <label>Convenio</label>
                            <input type="text" id="convenio" style="width: 335px;color: black;">
                            <label>Tipo Calculo</label>
                            <input type="text" id="tipocalculo_" style="width: 110px;color: black;"  >
                            <label>Descripcion</label>
                            <input type="text" id="descripcion" style="width: 410px;color: black;">
                            <label>Valor</label>
                            <input type="text" id="valor" style="width: 90px;color: black;" class="solo-numeric" onkeypress="return onKeyDecimal(event, this)">
                        </td>
                    </tr>
                </table>
            </div>
        </div>
        <div id="info"  class="ventana" >
            <p id="notific">EXITO AL GUARDAR</p>
        </div>
    </center>
</body>
<script>
    $(document).ready(function () {
        $('.solo-numero').keyup(function () {
            this.value = (this.value + '').replace(/[^0-9]/g, '');
        });

        $('.solo-numeric').live('blur', function (event) {
            this.value = numberConComas(this.value);
        });

        function numberConComas(x) {
            return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
        }
    });
</script>
</html>
