<%--
    Document   : Modulo de Seguimiento Cartera Fintra
    Created on : 19/05/2014, 11:00:00 AM
    Author     : hcuello
--%>

<%@page import="com.tsp.operation.model.beans.Usuario"%>
<%@page import="java.text.DecimalFormat"%>
<%@page import="java.text.DecimalFormatSymbols"%>
<%@ page import="com.tsp.operation.model.beans.SeguimientoCarteraBeans"%>
<%@ page import="com.tsp.operation.model.services.SeguimientoCarteraService"%>

<%@ page contentType="text/html; charset=UTF-8" language="java" %>
<%@ page session   ="true"%>
<%@ page errorPage ="/error/ErrorPage.jsp"%>
<%@ page import    ="java.util.*" %>
<%@ page import    ="java.text.SimpleDateFormat" %>
<%@ page import    ="com.tsp.util.*" %>
<%@ page import    ="javax.servlet.*" %>
<%@ page import    ="javax.servlet.http.*" %>
<%@ include file="/WEB-INF/InitModel.jsp"%>


<%
    Usuario usuario = (Usuario) session.getAttribute("Usuario");
    int periodo_foto = Integer.parseInt(request.getParameter("periodo_foto"));
    String Negocio = request.getParameter("negocio") != "" ? request.getParameter("negocio") : "";
    String tipoBusqueda = request.getParameter("tipo_busqueda") != "" ? request.getParameter("tipo_busqueda") : "";
    String item = "";

    SeguimientoCarteraService rqservice = new SeguimientoCarteraService(usuario.getBd());
    ArrayList listaConsolidados = null;
    if (tipoBusqueda.equalsIgnoreCase("1")) {
        listaConsolidados = rqservice.SCarteraDetallePagosBeans("SQL_DETALLE_PAGOS", periodo_foto, Negocio);
    } else if (tipoBusqueda.equalsIgnoreCase("2")) {
        listaConsolidados = rqservice.SCarteraDetallePagosBeans("SQL_DETALLE_PAGOS_TODO", periodo_foto, Negocio);
    }

    DecimalFormatSymbols simbolo = new DecimalFormatSymbols();
    simbolo.setDecimalSeparator('.');
    simbolo.setGroupingSeparator(',');
    DecimalFormat formateador = new DecimalFormat("###,###.##", simbolo);

//    SeguimientoCarteraBeans titulo = (SeguimientoCarteraBeans) listaConsolidados.get(0);

%>

<div style="width :auto ; max-height:700px;overflow: auto;" >
    <table style="margin-bottom: 1%" width="850" height="8" border='0' align='center' cellpadding="0" cellspacing="0" class="tablas" id="tbl_proceso">

        <tr>
            <td class="titulo_ventana" id="drag_detalle_pagos" colspan="9">
                <div style="float:left">DETALLE PAGOS  - <% if(!listaConsolidados.isEmpty()){
    SeguimientoCarteraBeans titulo = (SeguimientoCarteraBeans) listaConsolidados.get(0);                                                      
%>  <%= titulo.getNombreCliente()%> <%}%> -  <%= Negocio%><input type="hidden" name="id_usuario" id="id_usuario" value="coco"/></div> 
                <!-- <div style="float:right" onClick="$('#div_detalle_cartera').fadeOut('slow');"><a class="ui-widget-header ui-corner-all"><span class="ui-icon ui-icon-closethick"></span></a></div> -->
                <div style="float:right" onClick="$('#div_detalle_pagos').fadeOut('slow');"><a class="ui-widget-header ui-corner-all"><span>X</span></a></div>
            </td>
        </tr>    

        <tr><td colspan="15" style="text-align: right"><a href="#" onclick="VisualizarPagos('VISUALIZAR', '<%=Negocio%>', 2, event);">ver detalles</a></td></tr>

        <tr>
            <th width='15' align='center'>#</th>
            <!--        <th width='60' align='center'>NEGOCIO</th>
                        <th width='60' align='center'>CEDULA</th>
                        <th width='400' align='center'>NOMBRE CLIENTE</th>
                        <th width='100' align='center'>FACTURA</th>-->
            <th width='100' align='center'>No. INGRESO</th>
            <th width='50' align='center'>CUOTA</th>
            <!--            <th width='60' align='center'>FECHA VENCIMIENTO</th>
                        <th width='70' align='center'>DIAS VENCIDOS</th>
                        
                        <th width='100' align='center'>VALOR FACTURA</th>
                        <th width='100' align='center'>SALDO FACTURA</th>-->

            <th width='200' align='center'>BRANCH CODE</th>
            <th width='200' align='center'>BANC ACCOUNT NO</th>
            <th width='100' align='center'>FECHA INGRESO</th>
            <th width='100' align='center'>FECHA CONSIGNACION</th>
            <th width='100' align='center'>DESCRIPCION</th>

            <th width='100' align='center'>VALOR INGRESO</th>

        </tr><%

            float SumaValorFactura = 0;
            float SumaValorSaldo = 0;
            float SumaValorIngreso = 0;
            int CuentaRows = 1;

            if (listaConsolidados.size() > 0) {

                for (int i = 0; i < listaConsolidados.size(); i++) {

                    SeguimientoCarteraBeans ListadoContenido = (SeguimientoCarteraBeans) listaConsolidados.get(i);%>

        <tr  ondblclick="<% if (tipoBusqueda.equalsIgnoreCase("1")) {%>
                        verDetallesPagos('<%=periodo_foto%>', '<%=Negocio%>', '<%=ListadoContenido.getIngreso()%>')
             <%} else {%>verDetallesPagosTodos('<%=periodo_foto%>', '<%=Negocio%>', '<%=ListadoContenido.getIngreso()%>')<%}%>">

            <td align="justify"><%=CuentaRows%></td>
            <%--                        <td align="justify"><%=ListadoContenido.getNegocio()%></td>
                                        <td align="justify"><%=ListadoContenido.getCedula()%></td>
                                        <td align="justify"><%=ListadoContenido.getNombreCliente()%></td>
                                        <td align="justify"><%=ListadoContenido.getDocumento()%></td>--%>
            <td align="justify"><%=ListadoContenido.getIngreso()%></td>
            <td align="justify"><%=ListadoContenido.getCuota()%></td>
            <%--                            <td align="justify"><%=ListadoContenido.getFechaVencimiento()%></td>
                                        <td align="justify"><%=ListadoContenido.getDiasVencidos()%></td>
                                        
                                        <td align="right">$<%=formateador.format(Float.valueOf(ListadoContenido.getValorFactura()))%></td>
                                        <td align="right">$<%=formateador.format(Float.valueOf(ListadoContenido.getValorSaldo()))%></td>--%>

            <td align="justify"><%=ListadoContenido.getBranchCode()%></td>
            <td align="justify"><%=ListadoContenido.getBankAccountNo()%></td>
            <td align="justify"><%=ListadoContenido.getFechaIngreso()%></td>
            <td align="justify"><%=ListadoContenido.getFechaConsignacion()%></td>
            <td align="justify"><%=ListadoContenido.getDescripcionIngreso()%></td>
            <td align="right">$<%=formateador.format(Float.valueOf(ListadoContenido.getValorIngreso()))%></td>

        </tr><%

//                    SumaValorFactura  += Float.valueOf(ListadoContenido.getValorFactura());
//                    SumaValorSaldo  += Float.valueOf(ListadoContenido.getValorSaldo());
                SumaValorIngreso += Float.valueOf(ListadoContenido.getValorIngreso());

                CuentaRows = CuentaRows + 1;
                        }%>

        <tr>
            <!--            <th align="right" colspan="5"><strong>TOTAL CARTERA</strong></td>
                        <th align="left"><strong>$<%=formateador.format(SumaValorFactura)%></strong></td>
                        <th align="left"><strong>$<%=formateador.format(SumaValorSaldo)%></strong></td>-->
            <th align="right" colspan="8">TOTAL PAGOS</th>    
            <th align="left" style="text-align: right"><strong>$<%=formateador.format(SumaValorIngreso)%></strong></th>

        </tr><%


        } else {%>

        <tr>
            <td colspan="18">SIN RESULTADOS</td>
        </tr><%            }%>
        </tbody>
    </table>
</div>
<script>
                 $("#tbl_proceso tr:even").addClass("even");
                 $("#tbl_proceso tr:odd").addClass("odd");

                 $(
                         function()
                         {
                             $("#tbl_proceso tr").hover(
                                     function()
                                     {
                                         $(this).addClass("highlight");
                                     },
                                     function()
                                     {
                                         $(this).removeClass("highlight");
                                     }
                             )
                         }
                 )

                 function verDetallesPagos(periodo, negocio, num_ingreso) {


                     $('#popup_det_pagos').fadeIn('slow');
                     $('.popup-overlay_pagos').fadeIn('slow');
                     $('.popup-overlay_pagos').height($(window).height());

                     verDetallesPagosGrid(periodo, negocio, num_ingreso);

                 }

                 function verDetallesPagosTodos(periodo, negocio, num_ingreso) {


                     $('#popup_det_pagos_todos').fadeIn('slow');
                     $('.popup-overlay_pagos_todo').fadeIn('slow');
                     $('.popup-overlay_pagos_todo').height($(window).height());

                     verDetallesPagosGridTodo(periodo, negocio, num_ingreso);

                 }

                 $(document).ready(function() {

                     $('#close1').click(function() {
                         $('#popup_det_pagos').fadeOut('slow');
                         $('.popup-overlay_pagos').fadeOut('slow');
                         return false;
                     });
                     $('#close2').click(function() {
                         $('#popup_det_pagos_todos').fadeOut('slow');
                         $('.popup-overlay_pagos_todo').fadeOut('slow');
                         return false;
                     });


                 });
</script>