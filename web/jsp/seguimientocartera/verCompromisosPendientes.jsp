<%-- 
    Document   : verCompromisosPendientes
    Created on : 18/11/2015, 08:58:59 AM
    Author     : mcastillo
--%>

<%@page import="java.text.DecimalFormatSymbols"%>
<%@page import="java.text.DecimalFormat"%>
<%@page import="com.tsp.operation.model.beans.Usuario"%>
<%@page import="com.tsp.operation.model.beans.BeanGeneral"%>
<%@page import="java.util.ArrayList"%>
<%@page import="com.tsp.operation.model.services.GestionCarteraService"%>
<%@page contentType="text/html" pageEncoding="ISO-8859-1"%>
<%
    String BASEURL = request.getContextPath();
    String CONTROLLER = BASEURL + "/controller";
    GestionCarteraService gserv = null;
    
    Usuario usuario = (Usuario) session.getAttribute("Usuario");   
    gserv = new GestionCarteraService(usuario.getBd());

    String negocio = request.getParameter("negocio");
    String cedula = request.getParameter("cedula");
    ArrayList<BeanGeneral> listaCompromisos = gserv.getCompromisosPorNegocio(negocio);  
    
    DecimalFormatSymbols simbolo = new DecimalFormatSymbols();
    simbolo.setDecimalSeparator('.');
    simbolo.setGroupingSeparator(',');
    DecimalFormat formateador = new DecimalFormat("###,###.##", simbolo);
%>
<!DOCTYPE html>
<html>
     <div style="width :750px ;max-height:400px">

        <table width="100%" height="20" border='0' align='center' cellpadding="0" cellspacing="0" class="tablas" id="tbl_proceso1">

            <tr>
                <td class="titulo_ventana" id="drag_ver_compromisos_pago" colspan="18">
                    <div style="float:left">VER COMPROMISOS DE PAGO<input type="hidden" name="id_usuario" id="id_usuario" value="coco"/></div> 
                    <div style="float:right" onClick="$('#div_ver_compromisos_pago').fadeOut('slow');"><a class="ui-widget-header ui-corner-all"><span>X</span></a></div>
                </td>
            </tr>    

        </table> 

        <% if (!listaCompromisos.isEmpty()) {%>



        <div style="width :745px ; max-height:300px;overflow: auto;margin-top: 10px;margin-left: 8px" >

            <table border="1" width="728px" cellspacing="0" cellpadding="0" class="tablas" id="tbl_compromisos">
                <thead>



                    <tr>
                        <th>Observacion</th>
                        <th>Valor a pagar</th>                       
                        <th>Fecha a pagar</th>
                        <th>Direcci�n</th>
                        <th>Barrio</th>
                        <th>Ciudad</th>
                        <th>Fecha Creacion</th>
                        <th>Usuario Creador</th>
                    </tr>
                </thead>
                <tbody>
                    <%
                        for (int i = 0; i < listaCompromisos.size(); i++) {
                            BeanGeneral bg = listaCompromisos.get(i);
                    %>
                    <tr>
                        <td><%=bg.getValor_01()%></td>
                        <td align="center">$ <%=formateador.format(Float.valueOf(bg.getValor_02()))%></td>
                        <td align="center"><%=bg.getValor_03()%></td>
                        <td align="center"><%=bg.getValor_04()%></td>
                        <td align="center"><%=bg.getValor_05()%></td>
                        <td align="center"><%=bg.getValor_06()%></td> 
                        <td align="center"><%=bg.getValor_07()%></td>
                        <td align="center"><%=bg.getValor_08()%></td>
                          
                    </tr>

                    <%}%>

                </tbody>
            </table>
            <br/>    
        </div>         
        <%} else {%>

        <div>
            <h3 align='center'>No se encontraron compromisos de pago para este negocio </h3><br/>
        </div>
        <%}%>    

    </div>

    <script>
         $("#tbl_compromisos tr:even").addClass("even");
         $("#tbl_compromisos tr:odd").addClass("odd");
		 
		 $(
		   function()
		   {
			  $("#tbl_compromisos tr").hover(
			   function()
			   {
				$(this).addClass("highlight");
			   },
			   function()
			   {
				$(this).removeClass("highlight");
			   }
			  )
		   }
		  )
                           
     </script>
</html>
