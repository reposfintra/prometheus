<%-- 
    Document   : recaudo_consolidado_tendencia
    Created on : 5/07/2014, 11:34:38 AM
    Author     : egonzalez
--%>

<%@page import="com.tsp.operation.model.beans.Usuario"%>
<%@page import="java.text.DecimalFormat"%>
<%@page import="java.text.DecimalFormatSymbols"%>
<%@ page import="com.tsp.operation.model.beans.SeguimientoCarteraBeans"%>
<%@ page import="com.tsp.operation.model.services.SeguimientoCarteraService"%>

<%@ page contentType="text/html; charset=UTF-8" language="java" %>
<%@ page session   ="true"%>
<%@ page errorPage ="/error/ErrorPage.jsp"%>
<%@ page import    ="java.util.*" %>
<%@ page import    ="java.text.SimpleDateFormat" %>
<%@ page import    ="com.tsp.util.*" %>
<%@ page import    ="javax.servlet.*" %>
<%@ page import    ="javax.servlet.http.*" %>
<%@ include file="/WEB-INF/InitModel.jsp"%>


<%
    String unidad_negocio = request.getParameter("unidad_negocio") != "" ? request.getParameter("unidad_negocio") : "";
    int periodo_foto = Integer.parseInt(request.getParameter("periodo_foto"));
    String UserLogin = request.getParameter("UserLogin");

    String item = "";
    Usuario usuario = (Usuario) session.getAttribute("Usuario");
    SeguimientoCarteraService rqservice= new SeguimientoCarteraService(usuario.getBd());
    ArrayList listaConsolidados =  rqservice.SCarteraRecaudoConsolidadoBeans("SQL_OBTENER_CONSOLIDADO_RECAUDO_TENDENCIA",unidad_negocio,periodo_foto,UserLogin,item);
    
    DecimalFormatSymbols simbolo = new DecimalFormatSymbols();
    simbolo.setDecimalSeparator('.');
    simbolo.setGroupingSeparator(',');
    DecimalFormat formateador = new DecimalFormat("###,###.##", simbolo);
    
    
%>

<table width="650" height="18" border='0' align='center' cellpadding="0" cellspacing="0" class="tablas" id="tbl_proceso">
    
    <tr>
        <th width='150' align='center'>VENCIMIENTO MAYOR</th>
        <th width='100' align='center'>ESTADO CARTERA</th>
        <th width='100' align='center'>VALOR SALDO</th>
<!--        <th width='100' align='center'>% DEBIDO COBRAR</th>-->
        <th width='100' align='center'>RECAUDO</th>
        <th width='100' align='center'>% CUMPLIMIENTO</th>
    </tr><%

    float SumaValorSaldo = 0;
    float SumaDebidoCobrar = 0;
    float SumaRecaudo = 0;
    float SumCumplimiento = 0;
    
    float SumaValorSaldoParcial = 0;
    float SumaDebidoCobrarParcial = 0;
    float SumaRecaudoParcial = 0;
    float SumCumplimientoParcial = 0;
    
    String Comparador = "";
    String Swaping = "";
    
    if ( listaConsolidados.size() > 0 ) {
        
        //aqui se hace una empana para que esta vaina quede como el excell
        int rowspan = 0;
        int rowspan1 = 0;
        int rowspan2 = 0;
        int rowspan3 = 0;
        int rowspan4 = 0;
        int rowspan5 = 0;
        int rowspan6 = 0;
        int rowspan7 = 0;
        int prueba = 0;
        String vencimiento="";
        float totalSaldo1=0;
        float totalSaldo2=0;
        float totalSaldo3=0;
        float totalSaldo4=0;
        float totalSaldo5=0;
        float totalSaldo6=0;
        float totalSaldo7=0;
        float totalSaldo8=0;
        float ponderadoItem=0;
        float TotalPonderado=0;
        for(int e = 0; e < listaConsolidados.size(); e++){
              SeguimientoCarteraBeans Listado = (SeguimientoCarteraBeans) listaConsolidados.get(e);
                      if (Listado.getVencimientoMayor().equals("1- CORRIENTE")) {
                          rowspan++;
                          totalSaldo1+=Float.valueOf(Listado.getValorSaldo());
                      }
                      if (Listado.getVencimientoMayor().equals("2- 1 A 30")) {
                          rowspan1++;
                          totalSaldo2+=Float.valueOf(Listado.getValorSaldo());
                      }
                      if (Listado.getVencimientoMayor().equals("3- ENTRE 31 Y 60")) {
                          rowspan2++;
                          totalSaldo3+=Float.valueOf(Listado.getValorSaldo());
                      }
                      if (Listado.getVencimientoMayor().equals("4- ENTRE 61 Y 90")) {
                          rowspan3++;
                          totalSaldo4+=Float.valueOf(Listado.getValorSaldo());
                      }
                      if (Listado.getVencimientoMayor().equals("5- ENTRE 91 Y 120")) {
                          rowspan4++;
                          totalSaldo5+=Float.valueOf(Listado.getValorSaldo());
                      }
                      if (Listado.getVencimientoMayor().equals("6- ENTRE 121 Y 180")) {
                          rowspan5++;
                          totalSaldo6+=Float.valueOf(Listado.getValorSaldo());
                      }
                      if (Listado.getVencimientoMayor().equals("7- ENTRE 180 Y 360")) {
                          rowspan6++;
                          totalSaldo7+=Float.valueOf(Listado.getValorSaldo());
                      }
                      if (Listado.getVencimientoMayor().equals("8- MAYOR A 1 AÑO")) {
                          rowspan7++;
                          totalSaldo8+=Float.valueOf(Listado.getValorSaldo());
                      }
                  }

       for (int i = 0; i < listaConsolidados.size(); i++) {

                        SeguimientoCarteraBeans ListadoContenido = (SeguimientoCarteraBeans) listaConsolidados.get(i);
                        %>
                 <tr  ondblclick="Cargar_detalle_requisicion('',event);" >
                        <% if (ListadoContenido.getVencimientoMayor().equals("1- CORRIENTE")) {
                              if (prueba == 0) {%>
                          <td  style="text-align: center" rowspan="<%=rowspan%>"><%=ListadoContenido.getVencimientoMayor()%></td>    
                        <%
                                  vencimiento="1- CORRIENTE";
                               } else if (prueba == rowspan) {
                                //prueba = 0;
                            }%>

                        <td align="center"><%=ListadoContenido.getStatus()%></td>
                        <td align="right">$<%=formateador.format(Float.valueOf(ListadoContenido.getValorSaldo()))%></td>
        <%--                <td align="right">$<%=formateador.format(Float.valueOf(ListadoContenido.getDebidoCobrar()))%></td>--%>
                        <td align="right">$<%=formateador.format(Float.valueOf(ListadoContenido.getRecaudoxCuota()))%></td>
                        <td align="right"><%=ListadoContenido.getCumplimiento()%>%</td>

                        <%
                        //aqui va los totales parciales...
                        SumaValorSaldoParcial += Float.valueOf(ListadoContenido.getValorSaldo());
                        //SumaDebidoCobrarParcial += Float.valueOf(ListadoContenido.getDebidoCobrar());
                        SumaRecaudoParcial += Float.valueOf(ListadoContenido.getRecaudoxCuota());
                        SumCumplimientoParcial += Float.valueOf(ListadoContenido.getCumplimiento());
                        
                        //ponderado...
                        ponderadoItem = (((Float.valueOf(ListadoContenido.getValorSaldo()) / totalSaldo1) * 100)*Float.valueOf(ListadoContenido.getCumplimiento()))/100;
                        System.out.print("CORRIENTE ponderado "+i+": "+ponderadoItem);
                        TotalPonderado = TotalPonderado + ponderadoItem;
                        prueba++;
                        }
                        %>
                        
                        <% if (ListadoContenido.getVencimientoMayor().equals("2- 1 A 30")) {
                                  if (prueba == 0) {%>
                          <td style="text-align: center" rowspan="<%=rowspan1%>"><%=ListadoContenido.getVencimientoMayor()%></td>    
                        <%
                                      vencimiento="2- 1 A 30";
                            } else if (prueba == rowspan1) {
                               // prueba = 0;
                            }%>

                        <td align="center"><%=ListadoContenido.getStatus()%></td>
                        <td align="right">$<%=formateador.format(Float.valueOf(ListadoContenido.getValorSaldo()))%></td>
        <%--                <td align="right">$<%=formateador.format(Float.valueOf(ListadoContenido.getDebidoCobrar()))%></td>--%>
                        <td align="right">$<%=formateador.format(Float.valueOf(ListadoContenido.getRecaudoxCuota()))%></td>
                        <td align="right"><%=ListadoContenido.getCumplimiento()%>%</td>

                        <%
                        //aqui va los totales parciales...
                        SumaValorSaldoParcial += Float.valueOf(ListadoContenido.getValorSaldo());
                        //SumaDebidoCobrarParcial += Float.valueOf(ListadoContenido.getDebidoCobrar());
                        SumaRecaudoParcial += Float.valueOf(ListadoContenido.getRecaudoxCuota());
                        SumCumplimientoParcial += Float.valueOf(ListadoContenido.getCumplimiento());
                        
                         //ponderado...
                        ponderadoItem = (((Float.valueOf(ListadoContenido.getValorSaldo()) / totalSaldo2) * 100)*Float.valueOf(ListadoContenido.getCumplimiento()))/100;
                        System.out.print("1 A 30 ponderado "+i+": "+ponderadoItem);
                        TotalPonderado = TotalPonderado + ponderadoItem;
                        prueba++;
                        }
                        %>
                        
                        <% if (ListadoContenido.getVencimientoMayor().equals("3- ENTRE 31 Y 60")) {
                                  if (prueba == 0) {%>
                          <td style="text-align: center" rowspan="<%=rowspan2%>"><%=ListadoContenido.getVencimientoMayor()%></td>    
                        <%
                                      vencimiento="3- ENTRE 31 Y 60";
                            } else if (prueba == rowspan2) {
                               // prueba = 0;
                            }%>

                        <td align="center"><%=ListadoContenido.getStatus()%></td>
                        <td align="right">$<%=formateador.format(Float.valueOf(ListadoContenido.getValorSaldo()))%></td>
        <%--                <td align="right">$<%=formateador.format(Float.valueOf(ListadoContenido.getDebidoCobrar()))%></td>--%>
                        <td align="right">$<%=formateador.format(Float.valueOf(ListadoContenido.getRecaudoxCuota()))%></td>
                        <td align="right"><%=ListadoContenido.getCumplimiento()%>%</td>

                        <%
                        //aqui va los totales parciales...
                        SumaValorSaldoParcial += Float.valueOf(ListadoContenido.getValorSaldo());
                        //SumaDebidoCobrarParcial += Float.valueOf(ListadoContenido.getDebidoCobrar());
                        SumaRecaudoParcial += Float.valueOf(ListadoContenido.getRecaudoxCuota());
                        SumCumplimientoParcial += Float.valueOf(ListadoContenido.getCumplimiento());
                        
                        //ponderado...
                        ponderadoItem = (((Float.valueOf(ListadoContenido.getValorSaldo()) / totalSaldo3) * 100)*Float.valueOf(ListadoContenido.getCumplimiento()))/100;
                        System.out.print("ENTRE 31 Y 60 ponderado "+i+": "+ponderadoItem);
                        TotalPonderado = TotalPonderado + ponderadoItem;
                        prueba++;
                        }
                        %>
                        
                        <% if (ListadoContenido.getVencimientoMayor().equals("4- ENTRE 61 Y 90")) {
                                  if (prueba == 0) {%>
                          <td style="text-align: center" rowspan="<%=rowspan3%>"><%=ListadoContenido.getVencimientoMayor()%></td>    
                        <%
                                      vencimiento="4- ENTRE 61 Y 90";
                            } else if (prueba == rowspan3) {
                               // prueba = 0;
                            }%>

                        <td align="center"><%=ListadoContenido.getStatus()%></td>
                        <td align="right">$<%=formateador.format(Float.valueOf(ListadoContenido.getValorSaldo()))%></td>
        <%--                <td align="right">$<%=formateador.format(Float.valueOf(ListadoContenido.getDebidoCobrar()))%></td>--%>
                        <td align="right">$<%=formateador.format(Float.valueOf(ListadoContenido.getRecaudoxCuota()))%></td>
                        <td align="right"><%=ListadoContenido.getCumplimiento()%>%</td>

                        <%
                        //aqui va los totales parciales...
                        SumaValorSaldoParcial += Float.valueOf(ListadoContenido.getValorSaldo());
                        //SumaDebidoCobrarParcial += Float.valueOf(ListadoContenido.getDebidoCobrar());
                        SumaRecaudoParcial += Float.valueOf(ListadoContenido.getRecaudoxCuota());
                        SumCumplimientoParcial += Float.valueOf(ListadoContenido.getCumplimiento());
                        
                         //ponderado...
                        ponderadoItem = (((Float.valueOf(ListadoContenido.getValorSaldo()) / totalSaldo4) * 100)*Float.valueOf(ListadoContenido.getCumplimiento()))/100;
                        System.out.print("ENTRE 61 Y 90 ponderado "+i+": "+ponderadoItem);
                        TotalPonderado = TotalPonderado + ponderadoItem;
                        prueba++;
                        }
                        %>
                        
                          <% if (ListadoContenido.getVencimientoMayor().equals("5- ENTRE 91 Y 120")) {
                                  if (prueba == 0) {%>
                          <td style="text-align: center" rowspan="<%=rowspan4%>"><%=ListadoContenido.getVencimientoMayor()%></td>    
                        <%
                                      vencimiento="5- ENTRE 91 Y 120";
                            } else if (prueba == rowspan4) {
                               // prueba = 0;
                            }%>

                        <td align="center"><%=ListadoContenido.getStatus()%></td>
                        <td align="right">$<%=formateador.format(Float.valueOf(ListadoContenido.getValorSaldo()))%></td>
        <%--                <td align="right">$<%=formateador.format(Float.valueOf(ListadoContenido.getDebidoCobrar()))%></td>--%>
                        <td align="right">$<%=formateador.format(Float.valueOf(ListadoContenido.getRecaudoxCuota()))%></td>
                        <td align="right"><%=ListadoContenido.getCumplimiento()%>%</td>

                        <%
                        //aqui va los totales parciales...
                        SumaValorSaldoParcial += Float.valueOf(ListadoContenido.getValorSaldo());
                        //SumaDebidoCobrarParcial += Float.valueOf(ListadoContenido.getDebidoCobrar());
                        SumaRecaudoParcial += Float.valueOf(ListadoContenido.getRecaudoxCuota());
                        SumCumplimientoParcial += Float.valueOf(ListadoContenido.getCumplimiento());
                        
                         //ponderado...
                        ponderadoItem = (((Float.valueOf(ListadoContenido.getValorSaldo()) / totalSaldo5) * 100)*Float.valueOf(ListadoContenido.getCumplimiento()))/100;
                        System.out.print("ENTRE 91 Y 120 ponderado "+i+": "+ponderadoItem);
                        TotalPonderado = TotalPonderado + ponderadoItem;
                        prueba++;
                        }
                        %>
                        
                          <% if (ListadoContenido.getVencimientoMayor().equals("6- ENTRE 121 Y 180")) {
                                  if (prueba == 0) {%>
                          <td style="text-align: center" rowspan="<%=rowspan5%>"><%=ListadoContenido.getVencimientoMayor()%></td>    
                        <%
                                      vencimiento="6- ENTRE 121 Y 180";
                            } else if (prueba == rowspan5) {
                               // prueba = 0;
                            }%>

                        <td align="center"><%=ListadoContenido.getStatus()%></td>
                        <td align="right">$<%=formateador.format(Float.valueOf(ListadoContenido.getValorSaldo()))%></td>
        <%--                <td align="right">$<%=formateador.format(Float.valueOf(ListadoContenido.getDebidoCobrar()))%></td>--%>
                        <td align="right">$<%=formateador.format(Float.valueOf(ListadoContenido.getRecaudoxCuota()))%></td>
                        <td align="right"><%=ListadoContenido.getCumplimiento()%>%</td>

                        <%
                        //aqui va los totales parciales...
                        SumaValorSaldoParcial += Float.valueOf(ListadoContenido.getValorSaldo());
                        //SumaDebidoCobrarParcial += Float.valueOf(ListadoContenido.getDebidoCobrar());
                        SumaRecaudoParcial += Float.valueOf(ListadoContenido.getRecaudoxCuota());
                        SumCumplimientoParcial += Float.valueOf(ListadoContenido.getCumplimiento());
                        
                         //ponderado...
                        ponderadoItem = (((Float.valueOf(ListadoContenido.getValorSaldo()) / totalSaldo6) * 100)*Float.valueOf(ListadoContenido.getCumplimiento()))/100;
                        System.out.print("ENTRE 121 Y 180 ponderado "+i+": "+ponderadoItem);
                        TotalPonderado = TotalPonderado + ponderadoItem;
                        prueba++;
                        }
                        %>
                        
                           <% if (ListadoContenido.getVencimientoMayor().equals("7- ENTRE 180 Y 360")) {
                                  if (prueba == 0) {%>
                          <td style="text-align: center" rowspan="<%=rowspan6%>"><%=ListadoContenido.getVencimientoMayor()%></td>    
                        <%
                                      vencimiento="7- ENTRE 180 Y 360";
                            } else if (prueba == rowspan6) {
                               // prueba = 0;
                            }%>

                        <td align="center"><%=ListadoContenido.getStatus()%></td>
                        <td align="right">$<%=formateador.format(Float.valueOf(ListadoContenido.getValorSaldo()))%></td>
        <%--                <td align="right">$<%=formateador.format(Float.valueOf(ListadoContenido.getDebidoCobrar()))%></td>--%>
                        <td align="right">$<%=formateador.format(Float.valueOf(ListadoContenido.getRecaudoxCuota()))%></td>
                        <td align="right"><%=ListadoContenido.getCumplimiento()%>%</td>

                        <%
                        //aqui va los totales parciales...
                        SumaValorSaldoParcial += Float.valueOf(ListadoContenido.getValorSaldo());
                        //SumaDebidoCobrarParcial += Float.valueOf(ListadoContenido.getDebidoCobrar());
                        SumaRecaudoParcial += Float.valueOf(ListadoContenido.getRecaudoxCuota());
                        SumCumplimientoParcial += Float.valueOf(ListadoContenido.getCumplimiento());
                        
                         //ponderado...
                        ponderadoItem = (((Float.valueOf(ListadoContenido.getValorSaldo()) / totalSaldo7) * 100)*Float.valueOf(ListadoContenido.getCumplimiento()))/100;
                        System.out.print("ENTRE 180 Y 360 ponderado "+i+": "+ponderadoItem);
                        TotalPonderado = TotalPonderado + ponderadoItem;
                        prueba++;
                        }
                        %>
                        
                                <% if (ListadoContenido.getVencimientoMayor().equals("8- MAYOR A 1 AÑO")) {
                                  if (prueba == 0) {%>
                          <td style="text-align: center" rowspan="<%=rowspan7%>"><%=ListadoContenido.getVencimientoMayor()%></td>    
                        <%
                                      vencimiento="8- MAYOR A 1 AÑO";
                            } else if (prueba == rowspan7) {
                               // prueba = 0;
                            }%>

                        <td align="center"><%=ListadoContenido.getStatus()%></td>
                        <td align="right">$<%=formateador.format(Float.valueOf(ListadoContenido.getValorSaldo()))%></td>
        <%--                <td align="right">$<%=formateador.format(Float.valueOf(ListadoContenido.getDebidoCobrar()))%></td>--%>
                        <td align="right">$<%=formateador.format(Float.valueOf(ListadoContenido.getRecaudoxCuota()))%></td>
                        <td align="right"><%=ListadoContenido.getCumplimiento()%>%</td>

                        <%
                        //aqui va los totales parciales...
                        SumaValorSaldoParcial += Float.valueOf(ListadoContenido.getValorSaldo());
                        //SumaDebidoCobrarParcial += Float.valueOf(ListadoContenido.getDebidoCobrar());
                        SumaRecaudoParcial += Float.valueOf(ListadoContenido.getRecaudoxCuota());
                        SumCumplimientoParcial += Float.valueOf(ListadoContenido.getCumplimiento());
                        
                         //ponderado...
                        ponderadoItem = (((Float.valueOf(ListadoContenido.getValorSaldo()) / totalSaldo8) * 100)*Float.valueOf(ListadoContenido.getCumplimiento()))/100;
                        System.out.print(" MAYOR A 1 AÑO ponderado "+i+": "+ponderadoItem);
                        TotalPonderado = TotalPonderado + ponderadoItem;
                        prueba++;
                        }
                        %>
                        
                 </tr>
           
                 <%--Inicio filas para los sub totales de las alturas de cartera--%>

                 
                 <% if (prueba == rowspan && vencimiento.equals("1- CORRIENTE")) {
                         prueba = 0;
                        
                 %>
                 <tr>

                     <th colspan="2" ><strong>TOTALES CORRIENTE:</strong></td>
                     <th style="text-align: right" ><strong>$<%=formateador.format(SumaValorSaldoParcial)%></strong></td>
                     <th style="text-align: right"><strong>$<%=formateador.format(SumaRecaudoParcial)%></strong></td>
                     <th style="text-align: right"><strong><%=formateador.format(TotalPonderado)%>%</strong></td>

                 </tr>
                 <%                
                     SumaValorSaldoParcial = 0;
                     SumaDebidoCobrarParcial = 0;
                     SumaRecaudoParcial = 0;
                     SumCumplimientoParcial = 0;
                     TotalPonderado=0;
                     ponderadoItem=0; 
                
                 }%>
                 
                 <% if (prueba == rowspan1 && vencimiento.equals("2- 1 A 30")) {
                         prueba = 0;
                 %>
                 <tr >

                     <th colspan="2" ><strong>TOTALES 1 A 30:</strong></td>
                     <th style="text-align: right" ><strong>$<%=formateador.format(SumaValorSaldoParcial)%></strong></td>
                     <th style="text-align: right"><strong>$<%=formateador.format(SumaRecaudoParcial)%></strong></td>
                     <th style="text-align: right"><strong><%=formateador.format(TotalPonderado)%>%</strong></td>

                 </tr>
                 <%                
                     SumaValorSaldoParcial = 0;
                     SumaDebidoCobrarParcial = 0;
                     SumaRecaudoParcial = 0;
                     SumCumplimientoParcial = 0;
                     TotalPonderado=0;
                     ponderadoItem=0;
                 }%>
                 
                 <% if (prueba == rowspan2 && vencimiento.equals("3- ENTRE 31 Y 60")) {
                         prueba = 0;
                 %>
                 <tr>

                     <th colspan="2"><strong>TOTALES ENTRE 31 Y 60:</strong></td>
                     <th style="text-align: right" ><strong>$<%=formateador.format(SumaValorSaldoParcial)%></strong></td>
                     <th style="text-align: right"><strong>$<%=formateador.format(SumaRecaudoParcial)%></strong></td>
                     <th style="text-align: right"><strong><%=formateador.format(TotalPonderado)%>%</strong></td>

                 </tr>
                 <%                
                     SumaValorSaldoParcial = 0;
                     SumaDebidoCobrarParcial = 0;
                     SumaRecaudoParcial = 0;
                     SumCumplimientoParcial = 0;
                     TotalPonderado=0;
                     ponderadoItem=0;
                 }%>
                 
                  <% if (prueba == rowspan3 && vencimiento.equals("4- ENTRE 61 Y 90")) {
                         prueba = 0;
                 %>
                 <tr>

                     <th colspan="2"><strong>TOTALES ENTRE 61 Y 90:</strong></td>
                     <th style="text-align: right" ><strong>$<%=formateador.format(SumaValorSaldoParcial)%></strong></td>
                     <th style="text-align: right"><strong>$<%=formateador.format(SumaRecaudoParcial)%></strong></td>
                     <th style="text-align: right"><strong><%=formateador.format(TotalPonderado)%>%</strong></td>

                 </tr>
                 <%                
                     SumaValorSaldoParcial = 0;
                     SumaDebidoCobrarParcial = 0;
                     SumaRecaudoParcial = 0;
                     SumCumplimientoParcial = 0;
                     TotalPonderado=0;
                     ponderadoItem=0;        
                 }%>
                 
                     <% if (prueba == rowspan4 && vencimiento.equals("5- ENTRE 91 Y 120")) {
                         prueba = 0;
                 %>
                 <tr>

                     <th colspan="2"><strong>TOTALES ENTRE 91 Y 120:</strong></td>
                     <th style="text-align: right" ><strong>$<%=formateador.format(SumaValorSaldoParcial)%></strong></td>
                     <th style="text-align: right"><strong>$<%=formateador.format(SumaRecaudoParcial)%></strong></td>
                     <th style="text-align: right"><strong><%=formateador.format(TotalPonderado)%>%</strong></td>

                 </tr>
                 <%                
                     SumaValorSaldoParcial = 0;
                     SumaDebidoCobrarParcial = 0;
                     SumaRecaudoParcial = 0;
                     SumCumplimientoParcial = 0;
                     TotalPonderado=0;
                     ponderadoItem=0;
                 }%>
                 
                       <% if (prueba == rowspan5 && vencimiento.equals("6- ENTRE 121 Y 180")) {
                         prueba = 0;
                 %>
                 <tr >

                     <th colspan="2"><strong>TOTALES ENTRE 121 Y 180:</strong></td>
                     <th style="text-align: right" ><strong>$<%=formateador.format(SumaValorSaldoParcial)%></strong></td>
                     <th style="text-align: right"><strong>$<%=formateador.format(SumaRecaudoParcial)%></strong></td>
                     <th style="text-align: right"><strong><%=formateador.format(TotalPonderado)%>%</strong></td>

                 </tr>
                 <%                
                     SumaValorSaldoParcial = 0;
                     SumaDebidoCobrarParcial = 0;
                     SumaRecaudoParcial = 0;
                     SumCumplimientoParcial = 0;
                     TotalPonderado=0;
                     ponderadoItem=0;
                 }%>
                 
                 <% if (prueba == rowspan6 && vencimiento.equals("7- ENTRE 180 Y 360")) {
                         prueba = 0;
                 %>
                 <tr >

                     <th colspan="2" ><strong>TOTALES ENTRE 180 Y 360:</strong></td>
                     <th style="text-align: right" ><strong>$<%=formateador.format(SumaValorSaldoParcial)%></strong></td>
                     <th style="text-align: right"><strong>$<%=formateador.format(SumaRecaudoParcial)%></strong></td>
                     <th style="text-align: right"><strong><%=formateador.format(TotalPonderado)%>%</strong></td>

                 </tr>
                 <%                
                     SumaValorSaldoParcial = 0;
                     SumaDebidoCobrarParcial = 0;
                     SumaRecaudoParcial = 0;
                     SumCumplimientoParcial = 0;
                     TotalPonderado=0;
                     ponderadoItem=0;
                 }%>
                 
                   <% if (prueba == rowspan7 && vencimiento.equals("8- MAYOR A 1 AÑO")) {
                         prueba = 0;
                 %>
                 <tr >

                     <th colspan="2"><strong>TOTALES MAYOR A 1 AÑO:</strong></td>
                     <th style="text-align: right" ><strong>$<%=formateador.format(SumaValorSaldoParcial)%></strong></td>
                     <th style="text-align: right"><strong>$<%=formateador.format(SumaRecaudoParcial)%></strong></td>
                     <th style="text-align: right"><strong><%=formateador.format(TotalPonderado)%>%</strong></td>

                 </tr>
                 <%                
                     SumaValorSaldoParcial = 0;
                     SumaDebidoCobrarParcial = 0;
                     SumaRecaudoParcial = 0;
                     SumCumplimientoParcial = 0;
                     TotalPonderado=0;
                     ponderadoItem=0;
                 }%>
               <%--fin filas para los sub totales de las alturas de cartera--%>
            
             <%
                    SumaValorSaldo += Float.valueOf(ListadoContenido.getValorSaldo());
                  //  SumaDebidoCobrar += Float.valueOf(ListadoContenido.getDebidoCobrar());
                    SumaRecaudo += Float.valueOf(ListadoContenido.getRecaudoxCuota());
                    SumCumplimiento += Float.valueOf(ListadoContenido.getCumplimiento());

                }//fin for dos
             
             %>


            <tr>
                
                <th align="right" colspan="2"><strong>TOTALES:</strong></td>
                <th style="text-align: right;"><strong>$<%=formateador.format(SumaValorSaldo)%></strong></td>
<%--                <th align="left"><strong>$<%=formateador.format(SumaDebidoCobrar)%>%</strong></td>--%>
                <th style="text-align: right;"><strong>$<%=formateador.format(SumaRecaudo)%></strong></td>
                <th style="text-align: right;"><strong><%=formateador.format((SumaRecaudo/SumaValorSaldo)*100)%>%</strong></td>
                
            </tr><%

                       
    }else{%>
    
        <tr>
            <td colspan="10">SIN RESULTADOS</td>
        </tr><%
    
    }%>
</table>


    <script>
         $("#tbl_proceso tr:even").addClass("even");
         $("#tbl_proceso tr:odd").addClass("odd");
		 
		 $(
		   function()
		   {
			  $("#tbl_proceso tr").hover(
			   function()
			   {
				$(this).addClass("highlight");
			   },
			   function()
			   {
				$(this).removeClass("highlight");
			   }
			  )
		   }
		  )
     </script>
