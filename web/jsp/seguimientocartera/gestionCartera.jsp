<%-- 
    Document   : gestionCartera
    Created on : 10/06/2014, 04:23:46 PM
    Author     : hcuello
--%>

<%@page import="com.tsp.operation.model.beans.Usuario"%>
<%@page import="com.tsp.operation.model.beans.Cliente"%>
<%@page import="java.util.ArrayList"%>
<%@page import="com.tsp.operation.model.services.GestionCarteraService"%>
<%@page contentType="text/html" pageEncoding="ISO-8859-1"%>
<%
    String BASEURL = request.getContextPath();
    String CONTROLLER = BASEURL + "/controller";
    GestionCarteraService gserv = null;
    Usuario usuario = (Usuario) session.getAttribute("Usuario");
    gserv = new GestionCarteraService(usuario.getBd());

    String negocio = request.getParameter("negocio");
    String cedula = request.getParameter("cedula");
    String departamento = request.getParameter("departamento");
    String ciudad = request.getParameter("ciudad");
    String barrio = request.getParameter("barrio");
    String direccion = request.getParameter("direccion");

    //Buscamos todas las factura con saldo para realizar una gestion masiva.
    String[] facturasGestion = gserv.getFacturasAGestionar(negocio);
    String documentos = facturasGestion[0];
    String facts = facturasGestion[1];
    String valor = facturasGestion[2];
    //aqui voy a consultar los estados.
    Cliente cliente = gserv.estadoClienteCartera(cedula);
    
%>

<!DOCTYPE html>
<html>
   <head>
        <script type="text/javascript" src="<%=BASEURL%>/js/seguimiento_cartera.js"></script>
   </head>
    <div>

        <div id="formgestion" class="curved" >

            <table width="100%" height="20" border='0' align='center' cellpadding="0" cellspacing="0" class="tablas" id="tbl_proceso">

                <tr>
                    <td class="titulo_ventana" id="drag_gestiones_pendientes" colspan="18">
                        <div style="float:left">DATOS DE GESTION<input type="hidden" name="id_usuario" id="id_usuario" value="coco"/></div> 
                        <div style="float:right" onClick="$('#div_gestiones_pendientes').fadeOut('slow');"><a class="ui-widget-header ui-corner-all"><span>X</span></a></div>
                    </td>
                </tr>    

            </table> 

            <br>
            <% if (!documentos.equals("") && !facts.equals("") && !valor.equals("")) {%>

            <form id="formulario" action="<%= CONTROLLER%>?estado=Gestion&accion=Cartera&opcion=insert2" method="post">
                <table border="0" class="labels" style="border-collapse: collapse;margin-left: 2% ;margin-top: 1% ; text-align: left;" width="28%">                    
                    <tbody>
                        <tr>
                            <td class="labels">Cliente</td>
                            <td colspan="2"  >
                                <input style="color: black;" type="text" id="cliente" name="cliente" value="<%=cliente.getNomcli()%>" size="35" readonly="readonly" >
                                <input type="hidden" id="codcli" name="codcli" value="<%=cliente.getCodcli()%>">         
                                <input type="hidden" id="factura" name="factura" value="<%= documentos%>">     
                                <input type="hidden" id="facts" name="facts" value="<%=facts%>">     
                                <input type="hidden" id="valor" name="valor" value="<%=valor%>">     
                            </td>
                        </tr>
                        <tr >
                            <td>Negocio</td>
                            <td>
                                <input style="color: black;" type="text" name="negocio" id="negocio" value="<%=negocio%>" readonly="readonly">
                            </td>

                        </tr>


                        <tr >
                            <td>Gestion</td>
                            <td colspan="2">
                                <select id="tipo_gestion" name="tipo_gestion" style=" width: 23.5em;">
                                    <%
                                        ArrayList<String> listagestion = null;
                                        String[] temp = null;
                                        try {
                                            listagestion = gserv.listaGestion("TIPOGEST", "");
                                        } catch (Exception e) {
                                            System.out.println("Error obtenieindo la lista de gestiones en gestionCartera.jsp : " + e.toString());
                                            e.printStackTrace();
                                        }
                                        if (listagestion != null && listagestion.size() > 0) {
                                            for (int i = 0; i < listagestion.size(); i++) {
                                                temp = (listagestion.get(i)).split(";_;");
                                                out.print("<option value='" + temp[0] + "'>" + temp[1] + "</option>");
                                            }
                                        }
                                    %>
                                </select>    
                                <label id="lbl_domicilio" style="display:none;"><input type='checkbox' id="domicilio" name="domicilios" value="" style="display:none;">Domicilio</label>
                            </td>                       
                        </tr>
                        <tr >
                            <td >Observaciones</td>
                            <td >
                                <textarea name="observacion" id="observacion" rows="6" cols="49" style="resize:none;"></textarea>
                            </td>
                        </tr>
                     </tbody>
                </table>
               <div id="div_addGestion" style="border-collapse: collapse;margin-left: 1.5% ;margin-top: 1% ; text-align: left;display:none">                
                   <table border="0" class="labels"  width="98%">                    
                    <tbody>
                        <tr >
                            <td style="width:67px">Estado cliente</td>
                            <td colspan="2">
                                <select id="estado_cliente" name="estado_cliente" style="width: 23.5em;">
                                    <option value="">...</option>
                                    <%
                                        try {
                                            listagestion = gserv.listaGestion("ESTCLIENT", "");
                                        } catch (Exception e) {
                                            System.out.println("Error obteniendo la lista de gestiones en gestionCartera.jsp : " + e.toString());
                                            e.printStackTrace();
                                        }
                                        if (listagestion != null && listagestion.size() > 0) {
                                            for (int i = 0; i < listagestion.size(); i++) {
                                                temp = (listagestion.get(i)).split(";_;");
                                                String sel = "";
                                                if (cliente.getEstado().equals(temp[0])) {
                                                    sel = " selected='selected'";
                                                }
                                                out.print("<option value='" + temp[0] + "'" + sel + ">" + temp[1] + "</option>");
                                            }
                                        }
                                    %>
                                </select>
                            </td>
                        </tr>
                        <tr >
                            <td>Prox. accion</td>
                            <td colspan="2">
                                <select id="prox_accion" name="prox_accion" style=" width: 23.5em;">
                                    <%
                                        try {
                                            listagestion = gserv.listaGestion("PRXACCION", "");
                                        } catch (Exception e) {
                                            System.out.println("Error obtenieindo la lista de gestiones en gestionCartera.jsp : " + e.toString());
                                            e.printStackTrace();
                                        }
                                        if (listagestion != null && listagestion.size() > 0) {
                                            for (int i = 0; i < listagestion.size(); i++) {
                                                temp = (listagestion.get(i)).split(";_;");
                                                out.print("<option value='" + temp[0] + "'>" + temp[1] + "</option>");
                                            }
                                        }
                                    %>
                                </select>
                            </td>
                        </tr>
                        <tr >
                            <td>Fecha</td>
                            <td colspan="2">

                                <input style="color: black;" type="text" id="fecha_prox_gestion" name="fecha_prox_gestion" value="" readonly="readonly" size="10">
                                <img src="<%=BASEURL%>/images/cal.gif" id="imgFechaI" />
                                <script type="text/javascript">
                            Calendar.setup({
                                inputField: "fecha_prox_gestion",
                                trigger: "imgFechaI",
                                align: "top",
                                onSelect: function() {
                                    this.hide();
                                }
                            });
                                </script>
                                &nbsp;
                                Hora:
                                &nbsp;
                                <select id="hora" name="hora" style="width:50px;">
                                    <%
                                        for (int i = 0; i < 24; i++) {
                                            String cad = i < 10 ? "0" + i : "" + i;
                                    %>
                                    <option value="<%= cad%>"><%= cad%></option>
                                    <%
                                        }
                                    %>
                                </select>
                                &nbsp;
                                Minutos:
                                &nbsp;
                                <select id="minutos" name="minutos" style="width:50px;">
                                    <%
                                        for (int i = 0; i < 60; i++) {
                                            String cad = i < 10 ? "0" + i : "" + i;
                                    %>
                                    <option value="<%= cad%>"><%= cad%></option>
                                    <%
                                        }
                                    %>
                                </select>
                                <!-- <input id="hora" name="hora" type="text" style="color: black;" value="00:00" size="8"> -->
                            </td>
                        </tr>
                    </tbody>
                </table>
        </div>
        <div id="div_addCompromiso" style="border-collapse: collapse;margin-left: 1.5% ;margin-top: 1% ; text-align: left;display:none">  
            <table border="0" class="labels"  width="98%">                    
                <tbody>
                    <tr>
                        <td class="labels" style="width:67px">Valor a Pagar<span style="float:right;">$</span></td>
                        <td class="labels">
                             <input type="text" name="valor_pagar" id="valor_pagar" class="solo-numero"/>
                        </td>
                    </tr>
                    <tr>
                        <td class="labels" colspan="2">
                            <label for="fecha">Fecha de Pago </label>
                            <input type="text" id="fecha_pagar" />                     
                        </td>
                    </tr>                    
                </tbody>
            </table>
        </div>   
        <div id="div_addCompromisoDir" style="border-collapse: collapse;margin-left: 1.5% ;margin-top: 1% ; text-align: left;display:none">  
            <table border="0" class="labels"  width="98%">                    
                <tbody>
                    <tr>
                        <td class="labels" style="width:67px">Valor a Pagar<span style="float:right;">$</span></td>
                        <td class="labels">
                             <input type="text" name="valor_pagar" id="valor_pagar_dir" class="solo-numero"/>
                        </td>
                        <td class="labels" colspan="2">
                            <label for="fecha">Fecha de Pago </label>
                            <input type="text" id="fecha_pagar_dir" />                     
                        </td>
                    </tr>               
                    <tr>                         
                        <td><span>Departamento:</span></td>
                        <td> <select name="departamento" style="font-size: 14px;width:160px" id="departamento" value="<%=departamento%>">
                            </select></td>
                        <input type="hidden" name="departamento" id="coddpto" value="<%=departamento%>"/>
                        <td class="labels">Barrio</td>                      
                        <td class="labels" style="width:68px">
                             <input type="text" name="barrio" id="barrio" value="<%=barrio%>"/>
                        </td>
                    </tr>
                    <tr>                        
                        <td><span>Ciudad</span></td>
                        <td> <select name="ciudad" style="font-size: 14px;width:160px" id="ciudad" value="<%=ciudad%>"> 
                            </select></td>  
                        <input type="hidden" name="ciudad" id="codciu" value="<%=ciudad%>"/>
                        <td class="labels" style="width:68px">Direcci�n</td>
                        <td class="labels">
                             <input type="text" name="direccion" id="direccion" value="<%=direccion%>"/>
                        </td>                      
                    </tr>                   
                </tbody>
            </table>
        </div>                         
           
                <div style="margin-left: 27%;margin-top: 4%">


                    <img alt="aceptar" src="<%= BASEURL%>/images/botones/aceptar.gif" id="imgaceptar" name="imgaceptar" onMouseOver="botonOver(this);" onClick="(document.getElementById('tipo_gestion').value==19) ? guardarCompromisoPago(): guardarGestiones();" onMouseOut="botonOut(this);" style="cursor:pointer">


                    <img alt="salir" src="<%= BASEURL%>/images/botones/salir.gif" name="imgsalir" onMouseOver="botonOver(this);" onClick="$('#div_gestiones_pendientes').fadeOut('slow')" onMouseOut="botonOut(this);" style="cursor:pointer">
                </div>
            </form>
            <% } else {%>
            
            <div>
               
                <h3> NO HAY FACTURAS PENDIENTES PARA REALIZAR UNA GESTION</h3>
                <br>
             
            </div>

            <% }%>
        </div>
    </div>

</html>