<%-- 
    Document   : ReporteSanciones
    Created on : 6/10/2014, 02:40:56 PM
    Author     : lcanchila
--%>

<%@page import="com.tsp.operation.model.beans.CmbGeneralScBeans"%>
<%@page import="com.tsp.operation.model.services.SeguimientoCarteraService"%>
<%@page contentType="text/html" pageEncoding="ISO-8859-1"%>
<%@ page errorPage ="/error/ErrorPage.jsp"%>
<%@ page import    ="java.util.*" %>
<%@ page import    ="com.tsp.util.*" %>
<%@ page import    ="javax.servlet.*" %>
<%@ include file="/WEB-INF/InitModel.jsp"%>
<!DOCTYPE html>
<%
    SeguimientoCarteraService rqservice= new SeguimientoCarteraService();
    ArrayList listaCombo =  rqservice.GetComboGenerico("SQL_OBTENER_UNIDAD_NEGOCIO","id","descripcion","");
    ArrayList listaPeridos =  rqservice.GetComboGenerico("SQL_OBTENER_PERIODOS_FOTO","id","descripcion","");
    %>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
        <title>Reporte de Sanciones</title>
        
        <link href="<%=BASEURL%>/css/estilostsp.css" rel="stylesheet" type="text/css">
        <link href="<%=BASEURL%>/css/default.css" rel="stylesheet" type="text/css">
        <link type="text/css" rel="stylesheet" href="<%=BASEURL%>/css/jquery/jquery-ui/jquery-ui.css"/>

        <link href="<%=BASEURL%>/css/popup.css" rel="stylesheet" type="text/css">
        <link type="text/css" rel="stylesheet" href="<%=BASEURL%>/css/buttons/botonVerde.css"/>
        <link href="<%=BASEURL%>/css/style_azul.css" rel="stylesheet" type="text/css">
        <script type="text/javascript" src="<%=BASEURL%>/js/jCal/jscal2.js"></script>
        <script type="text/javascript" src="<%=BASEURL%>/js/jCal/lang/es.js"></script>

        <script type="text/javascript" src="<%=BASEURL%>/js/jquery-1.4.2.min.js"></script>
        <script language="JavaScript" type="text/javascript" src="<%=BASEURL%>/js/jquery-ui-1.8.5.custom.min.js"></script>
        <script type="text/javascript" src="<%=BASEURL%>/js/jquery/jquery.jqGrid/js/grid.locale-es.js"></script>
        <script type="text/javascript" src="<%=BASEURL%>/js/jquery/jquery.jqGrid/js/jquery.jqGrid.min.js"></script>
        <script type="text/javascript" src="<%=BASEURL%>/js/jquery.contextmenu.js"></script>
        <script type="text/javascript" src="<%=BASEURL%>/js/seguimiento_cartera.js"></script>
        
        <script>
            
            $(document).ready(function(){
                $("#msj").html("Espere un momento por favor...");
                $("#divSalida").dialog({
                    autoOpen: false,
                    height: "auto",
                    width: "300px",
                    modal: true,
                    autoResize: true,
                    resizable: false,
                    position: "center",
                    closeOnEscape: false
                    });
                 $("#botonAceptar").hide();
        

            });
        </script>    
    </head>
    <body>
        <div id="capaSuperior" style="position:absolute; width:100%; height:100px; z-index:0; left: 0px; top: 0px;">
           <jsp:include page="/WEB-INF/toptsp.jsp?encabezado=Reporte de Sanciones"/>
        </div>
        <div id="capaCentral" style="position:absolute; width:100%; height:85%; z-index:0; left: 0px; top: 100px; overflow: scroll;">
        <br>    
        <table width="600" align="center" class="labels">
            <tr>
                <td width="190">
                    <fieldset style=" width: 230px; height: 100px">
                        <legend>UNIDADES DE NEGOCIO</legend>
                        <table align="center" class="labels">
                            <tr>
                                <td>
                                    <select name="unidad_negocio" class="combo_180px" id="unidad_negocio" multiple="select-multiple" style=" width: 180px; height: 80px">
                                        <option value="" selected>< -Escoger- ></option><%
                                                                for (int i = 0; i < listaCombo.size(); i++) {
                                                                    CmbGeneralScBeans CmbContenido = (CmbGeneralScBeans) listaCombo.get(i);%>
                                        <option value="<%=CmbContenido.getIdCmb()%>"><%=CmbContenido.getDescripcionCmb()%></option><%
                                                                     }%>
                                    </select>
                                </td>
                            </tr>
                        </table>
                    </fieldset>
                </td>
                <td width="190">
                    <fieldset  style=" width: 230px; height: 100px">
                        <legend>PERIODO FOTO</legend>
                        <table align="center" class="labels">
                            <tr>
                                <td>
                                    <select name="periodo_foto" class="combo_180px" id="periodo_foto" multiple="select-multiple" style=" width: 180px; height: 80px"><%
                                        for (int i = 0; i < listaPeridos.size(); i++) {
                                            CmbGeneralScBeans CmbPeriodoFoto = (CmbGeneralScBeans) listaPeridos.get(i);%>
                                        <option value="<%=CmbPeriodoFoto.getIdCmb()%>" selected><%=CmbPeriodoFoto.getDescripcionCmb()%></option><%
                                                                    }%>
                                    </select>
                                </td>
                            </tr>
                        </table>
                    </fieldset>
                </td>
            </tr>
            </table> 
            <br>                        
            <table width="250" align="center" class="labels">                         
            <tr>
                <td>
                    <span  class="form-submit-button form-submit-button-simple_green_apple" id="New" onclick="cargarSanciones();" />Buscar </span>
                </td>
                <td>
                    <span  class="form-submit-button form-submit-button-simple_green_apple" id="New" onclick="exportarSanciones();" />Exportar </span>
                </td>
            </tr> 
            </table> 
            </br>  <br>  
            <div id="tb_fenalco" style=" display: none"> <table id="ReporteSancionesFenalco"></table> </div>
             <div id="pageFen"></div>
            <div id="tb_micro" style=" display: none"> <table id="ReporteSancionesMicro"></table> </div>
            <div id="pageMc"></div>
            <div id="divSalida" title="Exportacion" style=" display: block" >
                <p  id="msj" style=" display:  inline; ">texto </p> <br/>
                    <center><img id="imgload1" style="position:relative;  top: 2px; display: none " src="<%=BASEURL%>/images/cargandoCM.gif"/></center>
                    <div id="resp1" style=" display: none"></div>
            </div>
            </br>
       </div>                             
    </body>
</html>
