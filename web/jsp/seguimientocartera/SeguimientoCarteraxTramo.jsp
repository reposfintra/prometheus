<%--
    Document   : Modulo de Seguimiento Cartera Fintra
    Created on : 19/05/2014, 11:00:00 AM
    Author     : hcuello
--%>
<%@page session="true" %>
<%@ page import="com.tsp.operation.model.beans.Usuario"%>
<%@ page import="com.tsp.operation.model.beans.CmbGeneralScBeans"%>
<%@ page import="com.tsp.operation.model.services.SeguimientoCarteraService"%>

<%@ page contentType="text/html; charset=UTF-8" language="java" %>
<%@ page session   ="true"%>
<%@ page errorPage ="/error/ErrorPage.jsp"%>
<%@ page import    ="java.util.*" %>
<%@ page import    ="java.text.SimpleDateFormat" %>
<%@ page import    ="com.tsp.util.*" %>
<%@ page import    ="javax.servlet.*" %>
<%@ page import    ="javax.servlet.http.*" %>
<%@ include file="/WEB-INF/InitModel.jsp"%>


<%
    Usuario usuario = (Usuario) session.getAttribute("Usuario");
    SeguimientoCarteraService rqservice= new SeguimientoCarteraService(usuario.getBd());
    ArrayList listaCombo =  rqservice.GetComboGenerico("SQL_OBTENER_UNIDAD_NEGOCIO","id","descripcion",usuario.getLogin());
    ArrayList listaPeridos =  rqservice.GetComboGenerico("SQL_OBTENER_PERIODOS_FOTO","id","descripcion","");
    ArrayList listaTramos =  rqservice.GetComboGenericoStr("SQL_OBTENER_TRAMOS","id","descripcion","");
    ArrayList listaAgentes =  rqservice.GetComboGenericoStr("SQL_AGENTES_CARTERA","id","descripcion","");
    
    Date dNow = new Date( );
    /*
    SimpleDateFormat ft = new SimpleDateFormat ("E yyyy.MM.dd 'at' hh:mm:ss a zzz");
    out.print( "<h2 align=\"center\">" + ft.format(dNow) + "</h2>");   
    */
    SimpleDateFormat nd = new SimpleDateFormat ("yyyy");
    String aniocte = nd.format(dNow);
    
    SimpleDateFormat md = new SimpleDateFormat ("M");
    String mescte = md.format(dNow);
    
    int acorriente = Integer.parseInt(aniocte);
    int mescorriente = Integer.parseInt(mescte);
    
    int DiasMes = 0;
    
    //out.print( "<h2 align=\"center\">" + acorriente + "</h2>");
    //out.print( "<h2 align=\"center\">" + mescorriente + "</h2>");
    
    String LoginUsuario = usuario.getLogin();

%>

<html>
    <head>
        <title>Seguimiento Cartera Gerencia</title>

        <link rel="stylesheet" type="text/css" href="<%=BASEURL%>/css/jCal/jscal2.css" />
        <link rel="stylesheet" type="text/css" href="<%=BASEURL%>/css/jCal/border-radius.css" />
        <link href="<%=BASEURL%>/css/estilostsp.css" rel="stylesheet" type="text/css">
        <link href="<%=BASEURL%>/css/default.css" rel="stylesheet" type="text/css">
        <link href="<%=BASEURL%>/css/alert.css" rel="stylesheet" type="text/css"/>
        <link href="<%=BASEURL%>/css/alphacube.css" rel="stylesheet" type="text/css"/>
        <link href="<%=BASEURL%>/css/mac_os_x.css" rel="stylesheet" type="text/css">
        <link href="<%=BASEURL%>/css/estilotabla.css" rel="stylesheet" type="text/css">
        <link type="text/css" rel="stylesheet" href="<%=BASEURL%>/css/jquery/jquery-ui/jquery-ui.css"/>

      
        <link href="<%=BASEURL%>/css/popup.css" rel="stylesheet" type="text/css">
        <link href="<%=BASEURL%>/css/style_azul.css" rel="stylesheet" type="text/css">

        <script type="text/javascript" src="<%=BASEURL%>/js/jCal/jscal2.js"></script>
        <script type="text/javascript" src="<%=BASEURL%>/js/jCal/lang/es.js"></script>
        <script src="<%=BASEURL%>/js/boton.js" type="text/javascript"></script>
        <script src="<%=BASEURL%>/js/prototype.js" type="text/javascript"></script>
        <script src="<%=BASEURL%>/js/datacredito.js" type="text/javascript"></script>
        <script src="<%=BASEURL%>/js/effects.js" type="text/javascript"></script>
        <script src="<%=BASEURL%>/js/window.js" type="text/javascript"></script>
        <script src="<%=BASEURL%>/js/window_effects.js" type="text/javascript"></script>
        
        <!-- EDGAR -->
        <link type="text/css" rel="stylesheet" href="<%=BASEURL%>/css/form.css"/>
        <link type="text/css" rel="stylesheet" href="<%=BASEURL%>/css/nova.css"/>
        <link type="text/css" rel="stylesheet" href="<%=BASEURL%>/css/buttons/botonRose.css"/>
        <link type="text/css" rel="stylesheet" href="<%=BASEURL%>/css/buttons/botonVerde.css"/>
           <!-- EDGAR -->
        
        <script type="text/javascript" src="<%=BASEURL%>/js/jquery-1.4.2.min.js"></script>
        <script type="text/javascript" src="<%=BASEURL%>/js/jquery.battatech.excelexport.js"></script>
        <script language="JavaScript" type="text/javascript" src="<%=BASEURL%>/js/jquery-ui-1.8.5.custom.min.js"></script>
        <script type="text/javascript" src="<%=BASEURL%>/js/jquery/jquery.jqGrid/js/grid.locale-es.js"></script>
        <script type="text/javascript" src="<%=BASEURL%>/js/jquery/jquery.jqGrid/js/jquery.jqGrid.min.js"></script>
        <script type="text/javascript" src="<%=BASEURL%>/js/seguimiento_cartera.js"></script>

        <script src="<%=BASEURL%>/js/ex/tsorter.js" type="text/javascript"></script>
        
        <script language="JavaScript1.2">
            
            $(document).ready(function(){
                $("#div_detalle_cartera").draggable({ handle: "#drag_detalle_cartera"});
                $("#div_detalle_pagos").draggable({ handle: "#drag_detalle_pagos"});
                $("#div_actualizar_datos").draggable({ handle: "#drag_actualizar_datos"});
                $("#div_gestiones_pendientes").draggable({ handle: "#drag_gestiones_pendientes"});
                $("#div_ver_gestiones_pendientes").draggable({ handle: "#drag_ver_gestiones_pendientes"}); 
                $("#div_ver_compromisos_pago").draggable({ handle: "#drag_ver_compromisos_pago"}); 
                $("#popup_det_cartera").draggable({ handle: "#drag_popup_det_cartera"});  
                $("#popup_det_pagos").draggable({ handle: "#drag_popup_det_pagos"}); 
                $("#popup_det_pagos_todos").draggable({ handle: "#drag_popup_det_pagos_todos"});
                $("#div_estado_cuenta").draggable({ handle: "#drag_estado_cuenta"});
                $("table[id^=tabla_detalles_cartera]").styleTable();
                $("table[id^=tabla_detalles_pagos]").styleTable();
                
                $("#divSalida").dialog({
                    autoOpen: false,
                    height: "auto",
                    width: "300px",
                    modal: true,
                    autoResize: true,
                    resizable: false,
                    position: "center",
                    closeOnEscape: false


                });
                
                 $("#divSalidaTabla").dialog({
                    autoOpen: false,
                    height: "auto",
                    width: "300px",
                    modal: true,
                    autoResize: true,
                    resizable: false,
                    position: "center",
                    closeOnEscape: false


                });
                
            });     
            
            var ns4 = (document.layers);
            var ie4 = (document.all && !document.getElementById);
            var ie5 = (document.all && document.getElementById);
            var ns6 = (!document.all && document.getElementById);
            var msg = new Array();

            function Posicionar_div(id_objeto,e){
              //alert(e);
              //alert(window.event);
              obj = document.getElementById(id_objeto);
              var posx = 0;
              var posy = 0;
              if (!e) var e = window.event;
              if (e.pageX || e.pageY) {
               //alert('page');
               posx = e.pageX;
               posy = e.pageY;
              }
              else if (e.clientX || e.clientY) {
                    //alert('client'); 
                    posx = e.clientX + document.body.scrollLeft + document.documentElement.scrollLeft;
                    posy = e.clientY + document.body.scrollTop + document.documentElement.scrollTop; 
                  }
                  else
                   alert('ninguna de las anteriores');
             //alert('posx='+posx + ' posy=' + posy);
             obj.style.left = posx;
             obj.style.top = posy;
             //alert('objleft='+posx + ' objtop=' + posy);
             //document.getElementById('posicion').innerHTML = 'scrollLeft='+document.body.scrollLeft+' scrollTop='+document.body.scrollTop+' cientX'+e.clientX +' clientY'+e.clientY;
            }            
            
            function cargar_consolidado(){
                
                var TipoStatus1 = "";
                var TipoStatus2 = "";
                var TipoStatus3 = "";
                var SuperHaving = "";
                var StatusVecctos = "";
                var DiaMes = "";

                if($("#TipoStatus1").is(':checked')) {  
                    TipoStatus1 = $('#TipoStatus1').val();
                } else {  
                    TipoStatus1 = "";
                }              

                if($("#TipoStatus2").is(':checked')) {  
                    TipoStatus2 = $('#TipoStatus2').val();
                } else {  
                    TipoStatus2 = "";
                }                  

                if($("#TipoStatus3").is(':checked')) {  
                    TipoStatus3 = $('#TipoStatus3').val();
                } else {  
                    TipoStatus3 = "";
                }  
                
                
                
                if ( $('#tramosg').val() == "" &&  $('#pagosabonos').val() == "Na" && $('#agentes').val() == "" ) {
                    SuperHaving = "";
                    
                }else if ( $('#tramosg').val() != "" &&  $('#pagosabonos').val() == "Na" && $('#agentes').val() == "" ) {
                    SuperHaving = "having vencimiento_mayor = '"+$('#tramosg').val()+"'";
                    
                }else if ( $('#tramosg').val() == "" &&  $('#pagosabonos').val() != "Na" && $('#agentes').val() == "" ) {
                    SuperHaving = "having "+$('#pagosabonos').val();
                    
                }else if ( $('#tramosg').val() == "" &&  $('#pagosabonos').val() == "Na" && $('#agentes').val() != "" ) {
                    SuperHaving = "having agente = '"+$('#agentes').val()+"'";

                }else if ( $('#tramosg').val() != "" &&  $('#pagosabonos').val() != "Na" && $('#agentes').val() == "" ) {
                    SuperHaving = "having vencimiento_mayor = '"+$('#tramosg').val()+"' and "+$('#pagosabonos').val();

                }else if ( $('#tramosg').val() == "" &&  $('#pagosabonos').val() != "Na" && $('#agentes').val() != "" ) {
                    SuperHaving = "having "+$('#pagosabonos').val()+" and agente = '"+$('#agentes').val()+"'";

                }else if ( $('#tramosg').val() != "" &&  $('#pagosabonos').val() == "Na" && $('#agentes').val() != "" ) {
                    SuperHaving = "having vencimiento_mayor = '"+$('#tramosg').val()+"' and agente = '"+$('#agentes').val()+"'";

                }else if ( $('#tramosg').val() != "" &&  $('#pagosabonos').val() != "Na" && $('#agentes').val() != "" ) {
                    SuperHaving = "having vencimiento_mayor = '"+$('#tramosg').val()+"' and "+$('#pagosabonos').val()+" and agente = '"+$('#agentes').val()+"'";
                }



                if( $('#status_vcto').val() != "" ) {  
                    StatusVecctos = $('#status_vcto').val();
                } else {  
                    StatusVecctos = "";
                }  
                
                if( $('#DiasMes').val() != "" ) {  
                    DiaMes = "AND dia_pago = "+$('#DiasMes').val();
                } else {  
                    DiaMes = "";
                }  

                $('#div_espera').fadeIn('slow');
                $.ajax({
                    type: "POST",
                    url : "/fintra/jsp/seguimientocartera/cargar_seguimiento.jsp",
                    async:true,
                    dataType: "html",
                    data:{
                        unidad_negocio:$('#unidad_negocio').val(),
                        periodo_foto:$('#periodo_foto').val(),
                        aldia:TipoStatus1,
                        avencer:TipoStatus2,
                        vencido:TipoStatus3,
                        SuperHaving:SuperHaving,
                        StatusVcto:StatusVecctos,
                        DayMonth:DiaMes,
                        UserLogin:$('#UserLogin').val()
                    },
                    success:function (data){
                        $('#div_detalle_cartera').fadeOut('slow');
                        if (data!=""){
                            $('#misreq').html(data);
                            $('#div_espera').fadeOut('slow');
                            //alert($("#tbl_proceso").attr("border"));
                            document.getElementById('TotalPerc').value = $("#PercCump").val()+"%";

                            tsorter.create("tbl_consolidado");
                        }
                        }
                });
            }
            
            function Cargar_click_derecho(id,cc,dep,ciu,bar,dir,und_neg, e){
                
              Posicionar_div('div_menu_click_derecho',e);
              $.ajax(
              {
              type: "POST",
              url : "/fintra/jsp/seguimientocartera/menu_click_derecho_sc.jsp",
              async:true,
              dataType: "html",
              data:{
                    id:id,
                    cc:cc,
                    dep:dep,
                    ciu:ciu,
                    bar:bar,
                    dir:dir,
                    und_neg:und_neg
                   },
              success:function (data){
                        if (data!=""){
                          $('#div_menu_click_derecho').html(data);
                          $('#div_menu_click_derecho').fadeIn('slow');
                          $('#div_menu_click_derecho').show().delay(3000).fadeOut();
                        }
                      }
               }
              );

            }

            function VisualizarCartera(accion,item,und_neg,e){
                
                var cont_articulos = 0;
                var cont_item = 0;
                var edit_text = 0;
                
                $('#div_espera').fadeIn('slow');
                
                Posicionar_div('div_detalle_cartera',e);
                $.ajax({
                    type: "POST",
                    url : "/fintra/jsp/seguimientocartera/detalle_cartera.jsp",
                    async:true,
                    dataType: "html",
                    data:{
                        unidad_negocio:$('#unidad_negocio').val(),
                        periodo_foto:$('#periodo_foto').val(),
                        ACCIONE:accion,
                        negocio:item
                    },
                    
                    success:function (data){
                        if (data!=""){
                            $('#div_detalle_cartera').html(data);
                            $('#div_detalle_cartera').fadeIn('slow');
                            $('#div_espera').fadeOut('slow');
                            $('#div_menu_click_derecho').html('');
                        }
                    }
                });
        
            }
            
            function EstadoCuentaApoteosys(accion,item,und_neg,e){
                
                var cont_articulos = 0;
                var cont_item = 0;
                var edit_text = 0;
                
                $('#div_espera').fadeIn('slow');
                
                Posicionar_div('div_estado_cuenta',e);
                $.ajax({
                    type: "POST",
                    url : "/fintra/jsp/seguimientocartera/estado_cuenta_apoteosys.jsp",
                    async:true,
                    dataType: "html",
                    data:{
                        ACCIONE:accion,
                        unidad_negocio:und_neg,
                        negocio:item                        
                    },
                    
                    success:function (data){
                        if (data!=""){
                            $('#div_estado_cuenta').html(data);
                            $('#div_estado_cuenta').fadeIn('slow');
                            //$('#div_espera').fadeOut('slow');
                            $('#div_menu_click_derecho').html('');
                        }
                    }
                });
        
            }            
            
            function ConsultarECApoteosys(accion,item,und_neg) {

                console.log("accion: "+accion+" item: "+ item+" und_neg: "+ und_neg);
                
                $.ajax({
                    url: '/fintra/controller?estado=Estado&accion=Cuenta',
                    datatype: 'json',
                    type: 'get',
                    
                    data: {
                        opcion: 1,
                        ACCIONE:accion,
                        unidad_negocio:und_neg,
                        negocio:item
                    },
                    async: false,
                    success: function (json) {
                        try {

                            if (json.error) {

                                mensajesDelSistema("Guardar información", "Error Guardando informacion", "270");
                                console.log(json.error);

                            } else {
                                
                                console.log("HDC respuesta: " + json.hdc.success);
                                
                                if ( json.hdc.success ) {
                                    
                                    console.log("Son: "+json.hdc.data.length+" Registros");
                                    //console.log(json.hdc.data);
                                    //console.log(Object.keys(json.hdc.data));
                                    
                                    var TablaCalo = "";
                                    var SumDebito = 0;
                                    var SumCredito = 0;
                                    var SumSaldo = 0;
                                    var j = 0;
                                    var FromRows = 0;
                                    
                                    FromRows = json.hdc.data.length - 50;
                                    
                                    TablaCalo = 
                                        "<table width='720' height='18' border='0' align='center' cellpadding='0' cellspacing='0' class='tablas' id='tbl_proceso'>"
                                            +"<tr>"
                                            +    "<td class='titulo_ventana' id='drag_detalle_cartera' colspan='13'>"
                                            +        "<div style='float:left'>ESTADO DE CUENTA APOTEOSYS<input type='hidden' name='id_usuario' id='id_usuario' value='coco'/></div>"
                                            +        "<div style='float:right' onClick=\"$('#div_estado_cuenta').fadeOut('slow');\"><a class='ui-widget-header ui-corner-all'><span>X</span></a></div>"
                                            +    "</td>"
                                            +"</tr>"
                                            +"<tr><td colspan='13'>&nbsp;</td></tr>"
                                            +"<tr>"
                                                + "<th width='15' align='center'>#</th>"
                                                + "<th width='60' align='center'>TIPO DOCUMENTO</th>"
                                                + "<th width='60' align='center'>DOCUMENTO</th>"
                                                + "<th width='50' align='center'>PERIODO</th>"
                                                + "<th width='50' align='center'>VALOR_FACTURA</th>"
                                                + "<th width='50' align='center'>PERIODO_RECAUDO</th>"
                                                + "<th width='70' align='center'>VALOR_RECAUDO</th>"
                                                + "<th width='70' align='center'>DCTO_RECAUDO</th>"
                                                + "<th width='100' align='center'>FECHA_RECAUDO</th>"
                                                + "<th width='100' align='center'>RECAUDO_NO</th>"
                                                + "<th width='100' align='center'>MVTO_DB</th>"
                                                + "<th width='100' align='center'>MVTO_CR</th>"
                                                + "<th width='100' align='center'>SALDO</th>"
                                            +"</tr>";
                                    
                                            
                                            for(var i = 0; i < json.hdc.data.length; i++) {
                                                
                                                //console.log("i: "+i);
                                                //console.log(json.hdc.data[i].DOCUMENTO);
                                                
                                                j = i + 1;
                                                
                                                SumDebito = SumDebito + parseInt(Math.round(json.hdc.data[i].MVTO_DB));
                                                SumCredito = SumCredito + parseInt(Math.round(json.hdc.data[i].MVTO_CR));
                                                SumSaldo = SumSaldo + parseInt(Math.round(json.hdc.data[i].SALDO));
                                                
                                                if ( FromRows <= 0 ) {
                                                
                                                    TablaCalo += "<tr>";
                                                    TablaCalo += "<td align='justify'>"+j+"</td>";                        
                                                    TablaCalo += "<td align='justify'>"+json.hdc.data[i].DS+"</td>";
                                                    TablaCalo += "<td align='justify'>"+json.hdc.data[i].DOCUMENTO+"</td>";
                                                    TablaCalo += "<td align='justify'>"+json.hdc.data[i].PERIODO_FACTURA+"</td>";
                                                    TablaCalo += "<td align='right'>"+formato(json.hdc.data[i].VALOR_FACTURA).moneda+"</td>";
                                                    TablaCalo += "<td align='justify'>"+json.hdc.data[i].PERIODO_RECAUDO+"</td>";
                                                    TablaCalo += "<td align='right' >"+formato(json.hdc.data[i].VALOR_RECAUDO).moneda+"</td>";
                                                    TablaCalo += "<td align='justify'>"+json.hdc.data[i].DOCTO_RECAUDO+"</td>";
                                                    TablaCalo += "<td align='justify'>"+json.hdc.data[i].FECHA_RECAUDO+"</td>";
                                                    TablaCalo += "<td align='justify'>"+json.hdc.data[i].NUMERO_RECAUDO+"</td>";
                                                    TablaCalo += "<td align='right'>"+formato(json.hdc.data[i].MVTO_DB).moneda+"</td>";
                                                    TablaCalo += "<td align='right'>"+formato(json.hdc.data[i].MVTO_CR).moneda+"</td>";
                                                    TablaCalo += "<td align='right'>"+formato(json.hdc.data[i].SALDO).moneda+"</td>";
                                                    TablaCalo += "</tr>";
                                                    
                                                }else if ( FromRows > 0 && j > FromRows ) {
                                                
                                                    TablaCalo += "<tr>";
                                                    TablaCalo += "<td align='justify'>"+j+"</td>";                        
                                                    TablaCalo += "<td align='justify'>"+json.hdc.data[i].DS+"</td>";
                                                    TablaCalo += "<td align='justify'>"+json.hdc.data[i].DOCUMENTO+"</td>";
                                                    TablaCalo += "<td align='justify'>"+json.hdc.data[i].PERIODO_FACTURA+"</td>";
                                                    TablaCalo += "<td align='right'>"+formato(json.hdc.data[i].VALOR_FACTURA).moneda+"</td>";
                                                    TablaCalo += "<td align='justify'>"+json.hdc.data[i].PERIODO_RECAUDO+"</td>";
                                                    TablaCalo += "<td align='right' >"+formato(json.hdc.data[i].VALOR_RECAUDO).moneda+"</td>";
                                                    TablaCalo += "<td align='justify'>"+json.hdc.data[i].DOCTO_RECAUDO+"</td>";
                                                    TablaCalo += "<td align='justify'>"+json.hdc.data[i].FECHA_RECAUDO+"</td>";
                                                    TablaCalo += "<td align='justify'>"+json.hdc.data[i].NUMERO_RECAUDO+"</td>";
                                                    TablaCalo += "<td align='right'>"+formato(json.hdc.data[i].MVTO_DB).moneda+"</td>";
                                                    TablaCalo += "<td align='right'>"+formato(json.hdc.data[i].MVTO_CR).moneda+"</td>";
                                                    TablaCalo += "<td align='right'>"+formato(json.hdc.data[i].SALDO).moneda+"</td>";
                                                    TablaCalo += "</tr>";
                                                
                                                
                                                }
                                            }
                                            
                                            TablaCalo += "<tr>";
                                            TablaCalo += "<th align='justify' colspan='10'>TOTALES</th>";
                                            TablaCalo += "<th align='justify'>"+formato(SumDebito).moneda+"</th>";
                                            TablaCalo += "<th align='justify'>"+formato(SumCredito).moneda+"</th>";
                                            TablaCalo += "<th align='justify'>"+formato(SumDebito-SumCredito).moneda+"</th>";
                                            TablaCalo += "</tr>";
                                            
                                            TablaCalo += "<tr>";
                                            TablaCalo += "<th align='justify' colspan='10'>SALDO PENDIENTE APOTEOSYS</th>";
                                            TablaCalo += "<th align='justify' colspan='3'>"+formato(SumSaldo).moneda+"</th>";
                                            TablaCalo += "</tr>";                                            
                    
                                            TablaCalo += "</table>";

                                            document.getElementById("Canasta").innerHTML = TablaCalo;
                                    
                                }
                                
                            }
                            
                            $('#div_espera').fadeOut('slow');

                        } catch (exc) {
                            console.error(exc);
                        }
                    },
                    error: function () {
                    }
                });
            }
            
            function VisualizarPagos(accion,item,todo,e){
            
                var cont_articulos = 0;
                var cont_item = 0;
                var edit_text = 0;
                
                $('#div_espera').fadeIn('slow');
                
                Posicionar_div('div_detalle_pagos',e);
                $.ajax({
                    type: "POST",
                    url : "/fintra/jsp/seguimientocartera/detalle_pagos.jsp",
                    async:true,
                    dataType: "html",
                    data:{
                        periodo_foto:$('#periodo_foto').val(),
                        ACCIONE:accion,
                        negocio:item,
                        tipo_busqueda:todo
                    },
                    
                    success:function (data){
                        if (data!=""){
                            $('#div_detalle_pagos').html(data);
                            $('#div_detalle_pagos').fadeIn('slow');
                            $('#div_espera').fadeOut('slow');
                            $('#div_menu_click_derecho').html('');
                        }
                    }
                });
        
            }
            
            function EditarCliente(accion,cedula,e){
                
                $('#div_espera').fadeIn('slow');
                
                Posicionar_div('div_actualizar_datos',e);
                $.ajax({
                    type: "POST",
                    url : "/fintra/jsp/fenalco/clientes/EditarClientes.jsp",
                    async:true,
                    dataType: "html",
                    data:{
                        cedula:cedula
                    },
                    
                    success:function (data){
                        if (data!=""){
                            $('#div_actualizar_datos').html(data);
                            $('#div_actualizar_datos').fadeIn('slow');
                            buscarCliente();
                            $('#div_espera').fadeOut('slow');
                            $('#div_menu_click_derecho').html('');
                        }
                    }
                });
                

            }   
            
            
            
     
          
//------------------------------------EDGAR------------------------------------      

            function AgregarGestiones(accion,negocio,cedula,departamento,ciudad,barrio,direccion,e){
       
                $('#div_espera').fadeIn('slow');
                
               Posicionar_div('div_gestiones_pendientes',e);
                $.ajax({
                    type: "POST",
                    url : "/fintra/jsp/seguimientocartera/gestionCartera.jsp",
                    async:true,
                    dataType: "html",
                    data:{
                      negocio:negocio,  
                      cedula:cedula,
                      departamento:departamento,
                      ciudad:ciudad,
                      barrio:barrio,
                      direccion:direccion
                    },
                    
                    success:function (data){
                        if (data!=""){
                            $('#div_gestiones_pendientes').html(data);
                            $('#div_gestiones_pendientes').fadeIn('slow');
          
                            $('#div_espera').fadeOut('slow');
                            $('#div_menu_click_derecho').html('');
                        }
                    }
                });
            }   
            
           function guardarGestiones(cad_facts,valor,documentos){
               
            if($("#observacion").val()!="" && $("#fecha_prox_gestion").val()!=""){
                    
                  var url = "<%= CONTROLLER%>?estado=Gestion&accion=Cartera&opcion=insert2";
           
                   $.ajax({
                    type: "POST",
                    url :url,
                    async:true,
                    dataType:"html",
                    data:{
                      codcli:$("#codcli").val(),  
                      factura:$("#factura").val(),
                      facts:$("#facts").val(),
                      valor:$("#valor").val(),
                      negocio:$("#negocio").val(),
                      observacion:$("#observacion").val(),
                      tipo_gestion:$("#tipo_gestion").val(),
                      estado_cliente:$("#estado_cliente").val(),
                      prox_accion:$("#prox_accion").val(),
                      fecha_prox_gestion:$("#fecha_prox_gestion").val(),
                      hora:$("#hora").val(),
                      minutos:$("#minutos").val()
                    },
                    
                    success:function (data){
                    
                        var resp=trim(data);
                        if (resp =="OK"){
                            
                             $('#div_gestiones_pendientes').fadeOut('slow');
                             
                        }else{
                            alert("No se pudo guardar la gestion.")
                        }
                    }
                });

              }else{
                alert("Debe llenar todos los campos")
              
              }
           }           
           
           function VisualizarGestiones(accion,negocio,cedula,e){
       
                $('#div_espera').fadeIn('slow');
                
               Posicionar_div('div_ver_gestiones_pendientes',e);
                $.ajax({
                    type: "POST",
                    url : "/fintra/jsp/seguimientocartera/verGestionesPendientes.jsp",
                    async:true,
                    dataType: "html",
                    data:{
                      negocio:negocio,  
                      cedula:cedula
                    },
                    
                    success:function (data){
                        if (data!=""){
                            $('#div_ver_gestiones_pendientes').html(data);
                            $('#div_ver_gestiones_pendientes').fadeIn('slow');
          
                            $('#div_espera').fadeOut('slow');
                            $('#div_menu_click_derecho').html('');
                        }
                    }
                });
            }   
            
            function VisualizarCompromisosPago(accion,negocio,cedula,e){
              $('#div_espera').fadeIn('slow');
                
               Posicionar_div('div_ver_compromisos_pago',e);
                $.ajax({
                    type: "POST",
                    url : "/fintra/jsp/seguimientocartera/verCompromisosPendientes.jsp",
                    async:true,
                    dataType: "html",
                    data:{
                      negocio:negocio,  
                      cedula:cedula
                    },
                    
                    success:function (data){
                        if (data!=""){
                            $('#div_ver_compromisos_pago').html(data);
                            $('#div_ver_compromisos_pago').fadeIn('slow');
          
                            $('#div_espera').fadeOut('slow');
                            $('#div_menu_click_derecho').html('');
                        }
                    }
                });
            }   
           
           function guardarCompromisoPago(){
                var jsonParam = "{}", negocio, tipo_gestion, observacion, valor_pagar, fecha_pagar, ciudad, barrio, direccion;
                if ($('#domicilio').is(":checked")){
                    if($("#observacion").val()=="" || $("#valor_pagar_dir").val()=="" || $("#fecha_pagar_dir").val()=="" || $("#ciudad").val()=="" || $("#barrio").val()=="" || $("#direccion").val()=="") {
                        alert("Debe llenar todos los campos");
                        return;
                    }
                }else{
                    if($("#observacion").val()=="" || $("#valor_pagar").val()=="" || $("#fecha_pagar").val()==""){
                        alert("Debe llenar todos los campos");
                        return;
                    }                  
                }
               
                if(parseFloat($("#valor_pagar").val())<=0){
                   alert("El valor a pagar debe ser mayor a cero");
                }else{
                    negocio = $("#negocio").val();
                    tipo_gestion = $("#tipo_gestion").val();
                    observacion = $("#observacion").val();
                    if ($('#domicilio').is(":checked")) {                      
                        valor_pagar = numberSinComas($("#valor_pagar_dir").val());
                        fecha_pagar = $("#fecha_pagar_dir").val();
                        ciudad = $("#ciudad").val();
                        barrio = $("#barrio").val();
                        direccion = $("#direccion").val();
                    } else {
                        valor_pagar = numberSinComas($("#valor_pagar").val());
                        fecha_pagar = $("#fecha_pagar").val();   
                        ciudad = "";
                        barrio = "";
                        direccion = "";
                    }
                    jsonParam = {negocio: negocio, tipo_gestion: tipo_gestion, observacion: observacion, valor_pagar: valor_pagar, fecha_pagar: fecha_pagar, ciudad: ciudad, barrio: barrio, direccion:direccion};
                    
                    var url = "<%= CONTROLLER%>?estado=Gestion&accion=Cartera&opcion=insert2";

                    $.ajax({
                     type: "POST",
                     url :url,
                     async:true,
                     dataType:"html",
                     data: jsonParam,
                     success:function (data){

                         var resp=trim(data);
                         if (resp =="OK"){

                              $('#div_gestiones_pendientes').fadeOut('slow');

                         }else{
                             alert("No se pudo guardar la gestion.");
                         }
                     }
                    });
               }
             
           }
           
            function trim(cadena){
              cadena=cadena.replace(/^\s+/,'').replace(/\s+$/,'');
              return(cadena);
            }      
           
            function numberSinComas(x) {
                 return x.toString().replace(/,/g, "");
            }

            function formato(valor){
                var vaux;
                try {
                    valor = valor.toString().replace(new RegExp(',','g'),'');
                    var pattern =/\S+/;
                    if (pattern.test( valor )) {
                        pattern = /^-?(\d+\.?\d*)$|(\d*\.?\d+)$/;
                        if(pattern.test( valor )) {
                            vaux = parseFloat(valor);
                        } else {
                            vaux = 0;
                        }
                    } else {
                        vaux = 0;
                    }
                } catch(exc) {
                    vaux = 0;
                    console.log('error '+exc);
                }

                return {
                    moneda:vaux.toString().replace(/(\d)(?=(\d{3})+(\.|$))/g, '$1,')
                  , numero:vaux
                  , porcentaje:vaux/100
                }
            }           
                                 
        </script>
        
        <div id="div_espera" style="display:none;z-index:1000; position:absolute"></div>
        <div id="div_nueva_requisicion" style="display:none;z-index:101; position:absolute; border: 1px solid #165FB6; background:#FFFFFF; padding: 3px 3px 3px 3px;"></div>
        <div id="div_menu_click_derecho" style="display:none;z-index:510;position:absolute;border:1px solid #165FB6; background-color:#FFFFFF; padding: 2px 2px 2px 2px;"></div>
        <div id="div_detalle_cartera" style="display:none;z-index:102; position:absolute;border: 1px solid #165FB6; background:#FFFFFF; padding: 3px 3px 3px 3px;"></div>
        <div id="div_detalle_pagos" style="display:none;z-index:102; position:absolute;border: 1px solid #165FB6; background:#FFFFFF; padding: 3px 3px 3px 3px;"></div>
        <div id="div_gestiones_cartera" style="display:none;z-index:100; position:absolute;border: 1px solid #165FB6; background:#FFFFFF; padding: 3px 3px 3px 3px;"></div>
        <div id="div_actualizar_datos" style="display:none;z-index:100; position:absolute;border: 1px solid #165FB6; background:#FFFFFF; padding: 3px 3px 3px 3px;"></div>
        <div id="div_gestiones_pendientes" style="display:none;z-index:100; position:absolute;border: 1px solid #165FB6; background:#FFFFFF; padding: 3px 3px 3px 3px;"></div>
        <div id="div_ver_gestiones_pendientes" style="display:none;z-index:100; position:absolute;border: 1px solid #165FB6; background:#FFFFFF; padding: 3px 3px 3px 3px;"></div>
        <div id="div_ver_compromisos_pago" style="display:none;z-index:100; position:absolute;border: 1px solid #165FB6; background:#FFFFFF; padding: 3px 3px 3px 3px;"></div>
        <div id="div_estado_cuenta" style="display:none;z-index:102; position:absolute;border: 1px solid #165FB6; background:#FFFFFF; padding: 3px 3px 3px 3px;"></div>
    </head>
    
    <style type="text/css">
        .form-label{
            width:80px !important;
        }
        .form-label-left{
            width:80px !important;
        }
        .form-line{
            padding-top:12px;
            padding-bottom:12px;
        }
        .form-label-right{
            width:80px !important;
        }
        .form-all{
            width:600px;
            background:#e5e7e1;
            color:#555 !important;
            font-family:'Lucida Grande';
            font-size:14px;
        }
        .form-radio-item label, .form-checkbox-item label, .form-grading-label, .form-header{
            color:#555;
        }
        .titulo_ventana {
            font-family: "Trebuchet MS", Verdana, Arial, Helvetica, sans-serif;
            font-size: 10px;
            border: 1px solid #2A88C8;
            background: #2A88C8;
            color: #ffffff; 
            font-weight: bold;
            -moz-border-radius: 5px; -webkit-border-radius: 5px; border-radius: 5px; 
            padding: .5em 1em .3em; 
            position: relative;
            cursor: move; 	
        }        
        .inpt {
            text-align: left  !important;         
            text-transform: uppercase !important;
        }

    </style>    
    
    <body onload="">
        
        <div id="capaSuperior" style="position:absolute; width:100%; height:100px; z-index:0; left: 0px; top: 0px;">
           <jsp:include page="/WEB-INF/toptsp.jsp?encabezado=Seguimiento Cartera Gerencia"/>
        </div>
        
        <div id="capaCentral" style="position:absolute; width:100%; height:85%; z-index:0; left: 0px; top: 100px; overflow: scroll;">
            
            <br>
            
            <center>
                
                <input type="hidden" id="baseurl" name="baseurl" value="<%=BASEURL%>">
                <input type="hidden" id="controller" name="controller" value ="<%=CONTROLLER%>"/>
                <input type="hidden" id="UserLogin" name="UserLogin" value ="<%=LoginUsuario%>"/>
                
                <table width="100%" border="0" cellpadding="0" cellspacing="1" class="labels" id="tbl_hys">
                    
                    <tr>
                        <td colspan="2">
                            <fieldset>
                                
                                <legend class="labels">FILTROS</legend>
                                
                                <table width="1850" border="0" align="left" cellpadding="1" cellspacing="1" class="labels">
                                  <tr>
                                      
                                    <td width="190">
                                        <fieldset>
                                            <legend>UNIDADES DE NEGOCIO</legend>
                                            <table border="0" align="center" cellpadding="1" cellspacing="1" class="labels">
                                                <tr>
                                                    <td>
                                                        <select name="unidad_negocio" class="combo_180px" id="unidad_negocio">
                                                            <option value="" selected>< -Escoger- ></option><%
                                                            for (int i = 0; i < listaCombo.size(); i++) {
                                                                CmbGeneralScBeans CmbContenido = (CmbGeneralScBeans) listaCombo.get(i);%>
                                                                 <option value="<%=CmbContenido.getIdCmb() %>"><%=CmbContenido.getDescripcionCmb() %></option><%
                                                            }%>
                                                        </select>
                                                    </td>
                                                </tr>
                                            </table>
                                        </fieldset>
                                    </td>
                                      
                                    <td width="190">
                                        <fieldset>
                                            <legend>PERIODOS FOTO</legend>
                                            <table border="0" align="center" cellpadding="1" cellspacing="1" class="labels">
                                                <tr>
                                                    <td>
                                                        <select name="periodo_foto" class="combo_180px" id="periodo_foto"><%
                                                            for (int i = 0; i < listaPeridos.size(); i++) {
                                                                CmbGeneralScBeans CmbPeriodoFoto = (CmbGeneralScBeans) listaPeridos.get(i);%>
                                                                <option value="<%=CmbPeriodoFoto.getIdCmb() %>" selected><%=CmbPeriodoFoto.getDescripcionCmb() %></option><%
                                                            }%>
                                                        </select>
                                                    </td>
                                                </tr>
                                            </table>
                                        </fieldset>
                                    </td>     
                                    
                                    <td width="190">
                                        <fieldset>
                                            <legend>VENCIMIENTO MAYOR</legend>
                                            <table border="0" align="center" cellpadding="1" cellspacing="1" class="labels">
                                                <tr>
                                                    <td>
                                                        <select name="tramosg" class="combo_180px" id="tramosg">
                                                            <option value="" selected>< -Escoger- ></option><%
                                                            for (int i = 0; i < listaTramos.size(); i++) {
                                                                CmbGeneralScBeans CmbTramo = (CmbGeneralScBeans) listaTramos.get(i);%>
                                                                <option value="<%=CmbTramo.getIdCmbStr() %>"><%=CmbTramo.getDescripcionCmb() %></option><%
                                                                DiasMes = CmbTramo.getDiasMesCmb();
                                                            }%>
                                                        </select>
                                                    </td>
                                                </tr>
                                            </table>
                                        </fieldset>
                                    </td>      

                                    <td width="190">
                                        <fieldset>
                                            <legend>PAGOS</legend>
                                            <table border="0" align="center" cellpadding="1" cellspacing="1" class="labels">
                                                <tr>
                                                    <td>
                                                        <select name="pagosabonos" class="combo_180px" id="pagosabonos">
                                                            <option value="Na" selected>< -Todos- ></option>
                                                            <option value="(case when sum(valor_asignado) != 0 then ((sum(recaudosxcuota)/sum(valor_asignado))*100)::numeric(9,2) else 0 end) = 0">Sin Pagos</option>
                                                            <option value="(case when sum(valor_asignado) != 0 then ((sum(recaudosxcuota)/sum(valor_asignado))*100)::numeric(9,2) else 0 end) between 1 and 90">Abonó</option>
                                                            <option value="(case when sum(valor_asignado) != 0 then ((sum(recaudosxcuota)/sum(valor_asignado))*100)::numeric(9,2) else 0 end) < 100">Abonos Grande</option>
                                                            <option value="(case when sum(valor_asignado) != 0 then ((sum(recaudosxcuota)/sum(valor_asignado))*100)::numeric(9,2) else 0 end) >= 100">Recaudo Pleno</option>
                                                        </select>
                                                    </td>
                                                </tr>
                                            </table>
                                        </fieldset>
                                    </td>      
                                    
                                    <td width="190">
                                        <fieldset>
                                            <legend>ESTADO VENCIMIENTO</legend>
                                            <table border="0" align="center" cellpadding="1" cellspacing="1" class="labels">
                                                <tr>
                                                    <td>
                                                        <select name="status_vcto" class="combo_180px" id="status_vcto">
                                                            <option value="" selected>< -Todos- ></option>
                                                            <option value="and status_vencimiento = 'VENCIO'">VENCIÓ</option>
                                                            <option value="and status_vencimiento = 'AL DIA'">AL DIA</option>
                                                        </select>
                                                    </td>
                                                </tr>
                                            </table>
                                        </fieldset>
                                    </td>                                        
                                    
                                    <td width="60">
                                        <fieldset>
                                            <legend>D PAGO</legend>
                                            <table border="0" align="center" cellpadding="1" cellspacing="1" class="labels">
                                                <tr>
                                                    <td>
                                                        <select name="DiasMes" class="combo_40px" id="DiasMes">
                                                            <option value="" selected>- -</option><%
                                                            for (int i = 1; i <= DiasMes; i++) {%>
                                                                <option value="<%=i %>"><%=i %></option><%
                                                            }%>
                                                        </select>
                                                    </td>
                                                </tr>
                                            </table>
                                        </fieldset>
                                    </td>                                      
                                    
                                    
                                    <td width="190">
                                        <fieldset>
                                            <legend>AGENTES</legend>
                                            <table border="0" align="center" cellpadding="1" cellspacing="1" class="labels">
                                                <tr>
                                                    <td>
                                                        <select name="agentes" class="combo_180px" id="agentes">
                                                            <option value="" selected>< -Todos- ></option><%
                                                            for (int i = 0; i < listaAgentes.size(); i++) {
                                                                CmbGeneralScBeans CmbAgente = (CmbGeneralScBeans) listaAgentes.get(i);%>
                                                                <option value="<%=CmbAgente.getIdCmbStr() %>"><%=CmbAgente.getDescripcionCmb() %></option><%
                                                            }%>
                                                        </select>
                                                    </td>
                                                </tr>
                                            </table>
                                        </fieldset>
                                    </td>                                     
                                    
                                    
                                    <td width="300">
                                        <fieldset>
                                            <legend>ESTADO CARTERA</legend>
                                            <table width="100%" border="0" align="center" cellpadding="0" cellspacing="0" class="tablas_WoBorders">
                                                <tr>
                                                        <td width="29"><div align="center"><input id="TipoStatus1" type="checkbox" name="TipoStatus1" value="Al Dia" /></div></td>
                                                        <td width="200">AL DIA</td>
                                                        <td width="29" ><div align="center"><input id="TipoStatus2" type="checkbox" name="TipoStatus2" value="A Vencer" checked="checked" /></div></td>
                                                        <td width="200">A VENCER</td>
                                                        <td width="29"><div align="center"><input id="TipoStatus3" type="checkbox" name="TipoStatus3" value="Vencido" checked="checked" /></div></td>
                                                        <td width="200">VENCIDO</td>
                                                </tr>
                                            </table>
                                        </fieldset>
                                    </td>                                    
                                    
                                    <td width="150">
                                        <fieldset>
                                            <legend>CUMPLIMIENTO</legend>
                                            <table width="100%" border="0" align="center" cellpadding="0" cellspacing="0" class="tablas_WoBorders">
                                                <tr>
                                                    <td width="29"><div align="center"><input id="TotalPerc" type="text" name="TotalPerc" value="" height="50" class="textbox_65px" readonly="readonly" /></div></td>
                                                </tr>
                                            </table>
                                        </fieldset>
                                    </td>
                                    <td>
                                        <span  class="form-submit-button form-submit-button-simple_green_apple" id="New" onclick="cargar_consolidado();" />Buscar </span>
                                    </td>
                                    </tr>
                                </table>
                            </fieldset>
                        </td>
                    </tr>  
  
                    <tr><td colspan="2" id="mensajes_sistema">&nbsp;</td></tr>
                    
                    <tr>
                        <td width="1800" align="center" >
                            <fieldset>
                            <legend>RECAUDO GENERAL</legend> 
                              <div id="fondo4" style="position:relative; width:1800px; height:600px; visibility: visible; overflow:auto;" align="center">
                                   <div id="misreq" align="center" style="position:absolute; z-index:58; left:0px; top:0px; visibility:visible;"></div>
                            </div>
                            </fieldset>
                        </td>
                        
                    </tr>
                    
                    <div id="box" style="top:281px; left:417px; position:absolute; z-index:1000; width: 43px; height: 29px;"></div>

                </table>                
               <div id="divSalidaTabla" title="Exportacion" style=" display: block" >
                    <center><img id="imgload1" style="position:relative;  top: 20px; display: none" src="<%=BASEURL%>/images/cargandoCM.gif"/></center>
                    <div id="resp1" style=" display: none"></div>
               </div>
               <div id="divSalida" title="Exportacion" style=" display: block" >
                    <center><img id="imgload" style="position:relative;  top: 20px; display: none" src="<%=BASEURL%>/images/cargandoCM.gif"/></center>
                    <div id="resp" style=" display: none"></div>
               </div>     
                <br>
                <table border="0" style=" width: 300px" align="center">
                        <tr>
                            <td>
                                <span  class="form-submit-button form-submit-button-simple_green_apple" id="btnExport" onClick="exportarTabla();" />Exportar Reporte </span>
                            </td>
                            <td>
                                <span  class="form-submit-button form-submit-button-simple_green_apple" id="btnExportAll" onClick="exportarTodo();" />Exportar Detalle </span>
                            </td>
                        <!--    <td>
                                <span  class="form-submit-button form-submit-button-simple_rose" onClick="javascript:window.close();" />Salir </span>
                            </td> -->
                        </tr>
                    </table>
                <br><br>
               
                <center class='comentario'>
                    <div id="comentario" style="visibility: hidden" >
                        <table border="2" align="center">
                            <tr>
                                <td>
                                    <table width="100%"  border="1" align="center"  bordercolor="#F7F5F4" bgcolor="#FFFFFF">
                                        <tr>
                                            <td width="229" align="center" class="mensajes"><span class="normal"><div id="mensaje"></div></span></td>
                                            <td width="29" background="<%=BASEURL%>/images/cuadronaranja.JPG">&nbsp;</td>
                                            <td width="58">&nbsp;</td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                        </table>
                    </div>
                </center> 
                                            
            </center>
                    
             
        </div>
        
            <div id="popup_det_cartera" style="display: none;">
                <div class="content-popup">
                    <table width="920" id="drag_popup_det_cartera" >
                        <tr>
                            <td  class="titulo_ventana" height="35">

                                <div style="float:left; font-size: 15px">DETALLE CARTERA<input type="hidden" name="id_usuario" id="id_usuario" value="coco"/></div> 
                                <div style="float:right"><a id="close" class="ui-widget-header ui-corner-all"><span>X</span></a></div>

                            </td>
                        </tr>

                    </table>
                    <div>
                    <br/>
                    
                    <table id="tabla_detalles_cartera"  ></table>
                    <div id="page_detalles_cartera"></div>
                    
                </div>
            </div>
        </div>
        <div class="popup-overlay"></div>
        
        
        <div id="popup_det_pagos" style="display: none;">
                <div class="content-popup2">
                    <table width="731" id="drag_popup_det_pagos">
                        <tr>
                            <td  class="titulo_ventana" height="35">

                                <div style="float:left; font-size: 15px">DETALLE PAGOS<input type="hidden" name="id_usuario" id="id_usuario" value="coco"/></div> 
                                <div style="float:right"><a id="close1" class="ui-widget-header ui-corner-all"><span>X</span></a></div>

                            </td>
                        </tr>

                    </table>
                    <div>
                    <br/>
                    
                    <table id="tabla_detalles_pagos"  ></table>
                    <div id="page_detalles_pagos"></div>
                    
                </div>
            </div>
        </div>
        <div class="popup-overlay_pagos"></div>
        
        <div id="popup_det_pagos_todos" style="display: none;">
                <div class="content-popup3">
                    <table width="820" id="drag_popup_det_pagos_todos">
                        <tr>
                            <td  class="titulo_ventana" height="35">

                                <div style="float:left; font-size: 15px">DETALLE PAGOS NEGOCIO<input type="hidden" name="id_usuario" id="id_usuario" value="coco"/></div> 
                                <div style="float:right"><a id="close2" class="ui-widget-header ui-corner-all"><span>X</span></a></div>

                            </td>
                        </tr>

                    </table>
                    <div>
                    <br/>
                    
                    <table id="tabla_detalles_pagos_pagos"  ></table>
                    <div id="page_detalles_pagos_pagos"></div>
                    
                </div>
            </div>
        </div>
        <div class="popup-overlay_pagos_todo"></div>
                                     
                                            
                                            
    </body>
</html>
