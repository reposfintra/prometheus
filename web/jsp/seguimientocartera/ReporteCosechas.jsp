<%-- 
    Document   : ReporteCosechas
    Created on : 12/12/2014, 02:19:51 PM
    Author     : lcanchila
--%>

<%@page import="com.tsp.operation.model.beans.Usuario"%>
<%@page import="com.tsp.operation.model.DAOS.impl.AuditoriaIngresosImpl"%>
<%@page import="com.tsp.operation.model.beans.CmbGeneralScBeans"%>
<%@page import="java.util.ArrayList"%>
<%@page import="com.tsp.operation.model.services.SeguimientoCarteraService"%>
<%@page contentType="text/html" pageEncoding="ISO-8859-1"%>
<%@ include file="/WEB-INF/InitModel.jsp"%>
<!DOCTYPE html>
<%
    Usuario usuario = (Usuario) session.getAttribute("Usuario");
    SeguimientoCarteraService rqservice= new SeguimientoCarteraService(usuario.getBd());
    ArrayList listaCombo =  rqservice.GetComboGenerico("SQL_OBTENER_UNIDAD_NEGOCIO_BASE","id","descripcion","");
    AuditoriaIngresosImpl dao = new  AuditoriaIngresosImpl(usuario.getBd());
    ArrayList listaPeridos = dao.cargarPeriodo();
    %>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
        <title>Reporte de Cosechas</title>
        <link type="text/css" rel="stylesheet" href="<%=BASEURL%>/css/jquery/jquery-ui/jquery-ui.css"/>
        <link href="<%=BASEURL%>/css/estilostsp.css" rel="stylesheet" type="text/css">
        <link href="<%=BASEURL%>/css/popup.css" rel="stylesheet" type="text/css">
        <link type="text/css" rel="stylesheet" href="<%=BASEURL%>/css/buttons/botonVerde.css"/>
        
        <script type="text/javascript" src="<%=BASEURL%>/js/jquery/jquery.jqGrid/js/jquery.min.js"></script>
        <script type="text/javascript" src="<%=BASEURL%>/js/jquery-ui-1.8.5.custom.min.js"></script>
        <script type="text/javascript" src="<%=BASEURL%>/js/jquery/jquery.jqGrid/js/grid.locale-es.js"></script>
        <script type="text/javascript" src="<%=BASEURL%>/js/jquery/jquery.jqGrid/js/jquery.jqGrid.min.js"></script>
        <script type="text/javascript" src="<%=BASEURL%>/js/auditoriaIngresos.js"></script>
    </head>
    <body>
        <div id="capaSuperior" style="position:absolute; width:100%; height:100px; z-index:0; left: 0px; top: 0px;">
           <jsp:include page="/WEB-INF/toptsp.jsp?encabezado=Reporte de Cosechas"/>
        </div>
        <div id="capaCentral" style="position:absolute; width:100%; height:85%; z-index:0; left: 0px; top: 100px; overflow: scroll;">
        <br>  </br>  
        <div align="center">
            <div id="filtros" style="display:table-cell; " class="ui-jqgrid ui-widget ui-widget-content ui-corner-all">
                    </br>
                    <table style=" width: 600px">
                        <tr>
                            <td style="padding-left: 7px"><span> Unidad de Negocio:</span></td>
                            <td>
                                <select name="unidad_negocio" class="combo_180px" id="unidad_negocio" multiple="select-multiple" style=" width: 150px; height: 70px" >
                                    <%
                                        for (int i = 0; i < listaCombo.size(); i++) {
                                            CmbGeneralScBeans CmbContenido = (CmbGeneralScBeans) listaCombo.get(i);%>
                                    <option value="<%=CmbContenido.getIdCmb()%>"><%=CmbContenido.getDescripcionCmb()%></option><%
                                        }%>
                                </select>
                                </td>
                           <td><span> Periodo Inicial:</span></td>
                            <td><select name="periodoIni" class="combo_180px" id="periodoIni" ><%
                                        for (int i = 0; i < listaPeridos.size(); i++) {
                                            CmbGeneralScBeans CmbPeriodoFoto = (CmbGeneralScBeans) listaPeridos.get(i);%>
                                        <option value="<%=CmbPeriodoFoto.getIdCmb()%>" selected><%=CmbPeriodoFoto.getDescripcionCmb()%></option><%
                                                                    }%>
                                </select>
                            </td>
                            <td><span> Periodo Final</span></td>
                            <td><select name="periodoFin" class="combo_180px" id="periodoFin" ><%
                                        for (int i = 0; i < listaPeridos.size(); i++) {
                                            CmbGeneralScBeans CmbPeriodoFoto = (CmbGeneralScBeans) listaPeridos.get(i);%>
                                        <option value="<%=CmbPeriodoFoto.getIdCmb()%>" selected><%=CmbPeriodoFoto.getDescripcionCmb()%></option><%
                                                                    }%>
                                </select>
                            </td>
                        </tr> 
                    </table>
                    </br>
                     <table style=" width: 180px">        
                            <tr>
                                <td width="500px"></td>
                                <td width="500px"><button id="buscar" class="ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only" role="button" aria-disabled="false" onclick="cargarReporteCosecha();">
                                                    <span class="ui-button-text">Buscar</span>
                                                </button></td>
                            </tr>
                    </table> 
            </div> 
               </br> </br>                
               <table id="Cosechas"></table>
              <div id="dialogo" class="ventana" title="Mensaje">
                    <p  id="msj1" > </p>
               </div>
               <div id="divSalida" title="Exportacion" style=" display: block" >
                <p  id="msj" style=" display:  none; ">Espere un momento por favor... </p> 
                    <center><img id="imgload1" style="position:relative;  top: 2px; display: none " src="<%=BASEURL%>/images/cargandoCM.gif"/></center>
                    <div id="resp1" style=" display: none"></div>
               </div>    
            </div>
       </div>  
    </body>
</html>
