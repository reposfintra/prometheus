<%-- 
    Document   : verGestionesPendientes
    Created on : 12/06/2014, 11:14:17 AM
    Author     : hcuello
--%>

<%@page import="com.tsp.operation.model.beans.Usuario"%>
<%@page import="com.tsp.operation.model.beans.BeanGeneral"%>
<%@page import="java.util.ArrayList"%>
<%@page import="com.tsp.operation.model.services.GestionCarteraService"%>
<%@page contentType="text/html" pageEncoding="ISO-8859-1"%>
<%
    String BASEURL = request.getContextPath();
    String CONTROLLER = BASEURL + "/controller";
    GestionCarteraService gserv = null;
    
    Usuario usuario = (Usuario) session.getAttribute("Usuario");
    gserv = new GestionCarteraService(usuario.getBd());

    String negocio = request.getParameter("negocio");
    String cedula = request.getParameter("cedula");
    ArrayList<BeanGeneral> listaGestiones = gserv.getGestionesPorNegocio(negocio);
%>
<!DOCTYPE html>
<html>

    <div style="width :750px ;max-height:400px">

        <table width="100%" height="20" border='0' align='center' cellpadding="0" cellspacing="0" class="tablas" id="tbl_proceso1">

            <tr>
                <td class="titulo_ventana" id="drag_ver_gestiones_pendientes" colspan="18">
                    <div style="float:left">VER GESTIONES<input type="hidden" name="id_usuario" id="id_usuario" value="coco"/></div> 
                    <div style="float:right" onClick="$('#div_ver_gestiones_pendientes').fadeOut('slow');"><a class="ui-widget-header ui-corner-all"><span>X</span></a></div>
                </td>
            </tr>    

        </table> 

        <% if (!listaGestiones.isEmpty()) {%>



        <div style="width :745px ; max-height:300px;overflow: auto;margin-top: 10px;margin-left: 8px" >

            <table border="1" width="728px" cellspacing="0" cellpadding="0" class="tablas" id="tbl_proceso">
                <thead>



                    <tr>
                        <th>Observacion</th>
                        <th>Tipo de Gestion</th>
                        <th>Proxima Accion</th>
                        <th>Fecha Proxima Accion</th>
                        <th>Fecha Creacion</th>
                        <th>Usuario Creador</th>
                    </tr>
                </thead>
                <tbody>
                    <%
                        for (int i = 0; i < listaGestiones.size(); i++) {
                            BeanGeneral bg = listaGestiones.get(i);
                    %>
                    <tr>
                        <td><%=bg.getValor_01()%></td>
                        <td><%=bg.getValor_02()%></td>
                        <td><%=bg.getValor_03()%></td>
                        <td><%=bg.getValor_04()%></td>
                        <td><%=bg.getValor_05()%></td>
                        <td><%=bg.getValor_06()%></td>
                    </tr>

                    <%}%>

                </tbody>
            </table>
            <br/>    
        </div>         
        <%} else {%>

        <div>
            <h3 align='center'>No se encontraron gestiones para este negocio </h3><br/>
        </div>
        <%}%>    

    </div>
    <script>
         $("#tbl_proceso tr:even").addClass("even");
         $("#tbl_proceso tr:odd").addClass("odd");
		 
		 $(
		   function()
		   {
			  $("#tbl_proceso tr").hover(
			   function()
			   {
				$(this).addClass("highlight");
			   },
			   function()
			   {
				$(this).removeClass("highlight");
			   }
			  )
		   }
		  )
     </script>

</html>
