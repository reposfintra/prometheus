<%--
    Document   : Modulo de Seguimiento Cartera Fintra
    Created on : 19/05/2014, 11:00:00 AM
    Author     : hcuello
--%>

<%@page import="com.tsp.operation.model.beans.Usuario"%>
<%@page import="java.text.DecimalFormat"%>
<%@page import="java.text.DecimalFormatSymbols"%>
<%@ page import="com.tsp.operation.model.beans.SeguimientoCarteraBeans"%>
<%@ page import="com.tsp.operation.model.services.SeguimientoCarteraService"%>

<%@ page contentType="text/html; charset=UTF-8" language="java" %>
<%@ page session   ="true"%>
<%@ page errorPage ="/error/ErrorPage.jsp"%>
<%@ page import    ="java.util.*" %>
<%@ page import    ="java.text.SimpleDateFormat" %>
<%@ page import    ="com.tsp.util.*" %>
<%@ page import    ="javax.servlet.*" %>
<%@ page import    ="javax.servlet.http.*" %>
<%@ include file="/WEB-INF/InitModel.jsp"%>


<%
    String unidad_negocio = request.getParameter("unidad_negocio") != "" ? request.getParameter("unidad_negocio") : "";
    int periodo_foto = Integer.parseInt(request.getParameter("periodo_foto"));
    Usuario usuario = (Usuario) session.getAttribute("Usuario");
    String item = "";

    SeguimientoCarteraService rqservice= new SeguimientoCarteraService(usuario.getBd());
    ArrayList listaConsolidados =  rqservice.SCarteraTramoAnteriorBeans("SQL_OBTENER_CONSOLIDADO_TRAMO_ANTERIOR",unidad_negocio,periodo_foto,item);
    
    DecimalFormatSymbols simbolo = new DecimalFormatSymbols();
    simbolo.setDecimalSeparator('.');
    simbolo.setGroupingSeparator(',');
    DecimalFormat formateador = new DecimalFormat("###,###.##", simbolo);
    
    
%>

<table width="900" height="18" border='0' align='center' cellpadding="0" cellspacing="0" class="tablas" id="tbl_proceso">
    
    <tr>
        <th width='150' align='center'>VENCIMIENTO MAYOR</th>
        <th width='100' align='center'>VALOR SALDO</th>
        <th width='100' align='center'>% VALOR ASIGNADO</th>
        <th width='100' align='center'>CANTIDAD ASIGNADA</th>
        <th width='100' align='center'>% CANTIDAD ASIGNADA</th>
        
        <th width='150' align='center'>TRAMO ANTERIOR</th>
        <th width='100' align='center'>SUMA TRAMO ANTERIOR</th>
        <th width='100' align='center'>CANTIDAD TRAMO ANTERIOR</th>
    </tr><%

    float SumaShortValorAsignado = 0;
    float SumaShortPercValorAsignado = 0;
    float SumaShortCantAsignado = 0;
    float SumaShortPercCantAsignado = 0;
    float SumaShortTramoAnterior = 0;
    float CantShortTramoAnterior = 0;    
    
    float SumaValorAsignado = 0;
    float SumaPercValorAsignado = 0;
    float SumaCantAsignado = 0;
    float SumaPercCantAsignado = 0;

    float SumaTramoAnterior = 0;
    float CantTramoAnterior = 0;

    if ( listaConsolidados.size() > 0 ) {
        
        for (int i = 0; i < listaConsolidados.size(); i++) { 
        
            SeguimientoCarteraBeans ListadoContenido = (SeguimientoCarteraBeans) listaConsolidados.get(i); %>

            <tr><%
            
                if ( i != 0 && !ListadoContenido.getVencimientoMayor().isEmpty() ) {%>
                    <tr>
                        <th align="right"><strong>TOTALES:</strong></td>
                        <th align="left"><strong>$<%=formateador.format(SumaShortValorAsignado)%></strong></td>
                        <th align="left"><strong><%=formateador.format(SumaShortPercValorAsignado)%>%</strong></td>
                        <th align="left"><strong><%=formateador.format(SumaShortCantAsignado)%></strong></td>
                        <th align="left"><strong><%=formateador.format(SumaShortPercCantAsignado)%>%</strong></td>
                        <th align="left">-</td>
                        <th align="left"><strong>$<%=formateador.format(SumaShortTramoAnterior)%></strong></td>
                        <th align="left"><strong><%=formateador.format(CantShortTramoAnterior)%></strong></td>

                    </tr><%
                    
                    SumaShortValorAsignado = 0;
                    SumaShortPercValorAsignado = 0;
                    SumaShortCantAsignado = 0;
                    SumaShortPercCantAsignado = 0;
                    SumaShortTramoAnterior = 0;
                    CantShortTramoAnterior = 0;    
                               
                }            
            
                if ( !ListadoContenido.getVencimientoMayor().isEmpty() ) { %>
                
                    <td align="justify" rowspan="<%=ListadoContenido.getMaxRegTramo()%>"><%=ListadoContenido.getVencimientoMayor()%></td>
                    <td align="right" rowspan="<%=ListadoContenido.getMaxRegTramo()%>">$<%=formateador.format(Float.valueOf(ListadoContenido.getValorAsignado()))%></td>
                    <td align="right" rowspan="<%=ListadoContenido.getMaxRegTramo()%>"><%=ListadoContenido.getPercValorAsignado()%>%</td>
                    <td align="right" rowspan="<%=ListadoContenido.getMaxRegTramo()%>">$<%=formateador.format(Float.valueOf(ListadoContenido.getCantidadAsignada()))%></td>
                    <td align="right" rowspan="<%=ListadoContenido.getMaxRegTramo()%>"><%=ListadoContenido.getPercCantAsignada()%>%</td><%
                
                } %>
                
                <td align="justify"><%=ListadoContenido.getTramoAnterior()%></td>
                <td align="right">$<%=formateador.format(Float.valueOf(ListadoContenido.getSumaTramoAnterior()))%></td>
                <td align="right"><%=formateador.format(Float.valueOf(ListadoContenido.getCantTramoAnterior()))%></td>

                
            </tr><%
            
            if ( !ListadoContenido.getVencimientoMayor().isEmpty() ) {
             
                SumaValorAsignado += Float.valueOf(ListadoContenido.getValorAsignado());
                SumaPercValorAsignado += Float.valueOf(ListadoContenido.getPercValorAsignado());
                SumaCantAsignado += Float.valueOf(ListadoContenido.getCantidadAsignada());
                SumaPercCantAsignado += Float.valueOf(ListadoContenido.getPercCantAsignada());
                
                SumaShortValorAsignado += Float.valueOf(ListadoContenido.getValorAsignado());
                SumaShortPercValorAsignado += Float.valueOf(ListadoContenido.getPercValorAsignado());
                SumaShortCantAsignado += Float.valueOf(ListadoContenido.getCantidadAsignada());
                SumaShortPercCantAsignado += Float.valueOf(ListadoContenido.getPercCantAsignada());                

            }
            
            SumaTramoAnterior += Float.valueOf(ListadoContenido.getSumaTramoAnterior());
            CantTramoAnterior += Float.valueOf(ListadoContenido.getCantTramoAnterior());

            SumaShortTramoAnterior += Float.valueOf(ListadoContenido.getSumaTramoAnterior());
            CantShortTramoAnterior += Float.valueOf(ListadoContenido.getCantTramoAnterior());
        }%>

            <tr>
                
                <th align="right"><strong>TOTALES:</strong></td>
                <th align="left"><strong>$<%=formateador.format(SumaValorAsignado)%></strong></td>
                <th align="left"><strong><%=formateador.format(SumaPercValorAsignado)%>%</strong></td>
                <th align="left"><strong><%=formateador.format(SumaCantAsignado)%></strong></td>
                <th align="left"><strong><%=formateador.format(SumaPercCantAsignado)%>%</strong></td>
                <th align="left">-</td>
                <th align="left"><strong>$<%=formateador.format(SumaTramoAnterior)%></strong></td>
                <th align="left"><strong><%=formateador.format(CantTramoAnterior)%></strong></td>
                
            </tr><%
  
    }else{%>
    
        <tr>
            <td colspan="10">SIN RESULTADOS</td>
        </tr><%
    
    }%>
</table>


    <script>
         $("#tbl_proceso tr:even").addClass("even");
         $("#tbl_proceso tr:odd").addClass("odd");
		 
		 $(
		   function()
		   {
			  $("#tbl_proceso tr").hover(
			   function()
			   {
				$(this).addClass("highlight");
			   },
			   function()
			   {
				$(this).removeClass("highlight");
			   }
			  )
		   }
		  )
     </script>