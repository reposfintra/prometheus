<%-- 
    Document   : resumen_recaudo
    Created on : 7/07/2014, 02:45:32 PM
    Author     : egonzalez
--%>

<%@page import="com.tsp.operation.model.beans.Usuario"%>
<%@page import="java.text.DecimalFormat"%>
<%@page import="java.text.DecimalFormatSymbols"%>
<%@ page import="com.tsp.operation.model.beans.SeguimientoCarteraBeans"%>
<%@ page import="com.tsp.operation.model.services.SeguimientoCarteraService"%>

<%@ page contentType="text/html; charset=UTF-8" language="java" %>
<%@ page session   ="true"%>
<%@ page errorPage ="/error/ErrorPage.jsp"%>
<%@ page import    ="java.util.*" %>
<%@ page import    ="java.text.SimpleDateFormat" %>
<%@ page import    ="com.tsp.util.*" %>
<%@ page import    ="javax.servlet.*" %>
<%@ page import    ="javax.servlet.http.*" %>
<%@ include file="/WEB-INF/InitModel.jsp"%>


<%
    String unidad_negocio = request.getParameter("unidad_negocio") != "" ? request.getParameter("unidad_negocio") : "";
    int periodo_foto = Integer.parseInt(request.getParameter("periodo_foto"));
    String UserLogin = request.getParameter("UserLogin");

    String item = "";
    Usuario usuario = (Usuario) session.getAttribute("Usuario");
    SeguimientoCarteraService rqservice= new SeguimientoCarteraService(usuario.getBd());
    ArrayList listaConsolidados =  rqservice.GetResumenConsolidado(periodo_foto, unidad_negocio, UserLogin);
    DecimalFormatSymbols simbolo = new DecimalFormatSymbols();
    simbolo.setDecimalSeparator('.');
    simbolo.setGroupingSeparator(',');
    DecimalFormat formateador = new DecimalFormat("###,###.##", simbolo);
    
    
%>

 <style type="text/css">
        .td{
          text-align:center;
        }
        
    </style> 
<html>
    <table  width="570" height="18" border='0' align='center' cellpadding="0" cellspacing="0" class="tablas" id="tbl_proceso">
       
            <tr>
                <th>RANGO</th>
                <th>DEBIDO A COBRAR</th>
                <th>RECAUDO 100%</th>
                <th>ABONO GRANDE</th>
                <th>ABONO</th>
                <th>SIN PAGOS</th>
            </tr>
       
       
  
                <%
               
                int debidoCobrar=0;
                int recaudocien=0;
                int abonoGrande=0;
                int abono=0;
                int sinPagos=0;
                int consolidado=0;
                
                int totalDebidoCobrar=0;
                int totalRecaudocien=0;
                int totalAbonoGrande=0;
                int totalAbono=0;
                int totalSinPagos=0;
                int totalConsolidado=0;
                
                
                int rowspan = 0;
                int rowspan1 = 0;
                int rowspan2 = 0;
                int rowspan3 = 0;
                int rowspan4 = 0;
                int rowspan5 = 0;
                int rowspan6 = 0;
                int rowspan7 = 0;

                
              for(int e = 0; e < listaConsolidados.size(); e++){
              SeguimientoCarteraBeans Listado = (SeguimientoCarteraBeans) listaConsolidados.get(e);
                      if (Listado.getVencimientoMayor().equals("01- CORRIENTE")) {
                          rowspan++;
                       
                      }
                      if (Listado.getVencimientoMayor().equals("02- 1 A 30")) {
                          rowspan1++;
                        
                      }
                      if (Listado.getVencimientoMayor().equals("03- ENTRE 31 Y 60")) {
                          rowspan2++;
                      
                      }
                      if (Listado.getVencimientoMayor().equals("04- ENTRE 61 Y 90")) {
                          rowspan3++;
                        
                      }
                      if (Listado.getVencimientoMayor().equals("05- ENTRE 91 Y 120")) {
                          rowspan4++;
                  
                      }
                      if (Listado.getVencimientoMayor().equals("06- ENTRE 121 Y 180")) {
                          rowspan5++;
                         
                      }
                      if (Listado.getVencimientoMayor().equals("07- ENTRE 180 Y 210") ||
                          Listado.getVencimientoMayor().equals("08- ENTRE 210 Y 240")||
                          Listado.getVencimientoMayor().equals("09- ENTRE 240 Y 270")||
                          Listado.getVencimientoMayor().equals("10- ENTRE 270 Y 300")||
                          Listado.getVencimientoMayor().equals("11- ENTRE 300 Y 330")||
                          Listado.getVencimientoMayor().equals("12- ENTRE 330 Y 365") ) {
                          rowspan6++;
                          
                      }
                      if (Listado.getVencimientoMayor().equals("13- MAYOR A 1 ANIO")) {
                          rowspan7++;
                        
                      }
                  }
             
                    for (int i = 0; i < listaConsolidados.size(); i++) {
                        SeguimientoCarteraBeans Listado = (SeguimientoCarteraBeans) listaConsolidados.get(i);

                        if (Listado.getVencimientoMayor().equals("01- CORRIENTE")) {
                            debidoCobrar++;
                            if (Float.valueOf(Listado.getCumplimiento()) >= 100) {
                                recaudocien++;
                                consolidado++;
                            }
                            if (Float.valueOf(Listado.getCumplimiento()) >= 91 && Float.valueOf(Listado.getCumplimiento()) <100) {
                                abonoGrande++;
                                consolidado++;
                            }
                            if (Float.valueOf(Listado.getCumplimiento()) > 0 && Float.valueOf(Listado.getCumplimiento()) < 91) {
                                abono++;
                                consolidado++;
                            }

                        }

                        if (Listado.getVencimientoMayor().equals("02- 1 A 30")) {
                            debidoCobrar++;
                            if (Float.valueOf(Listado.getCumplimiento()) >= 100) {
                                recaudocien++;
                                consolidado++;
                            }
                            if (Float.valueOf(Listado.getCumplimiento()) >= 91 && Float.valueOf(Listado.getCumplimiento()) < 100) {
                                abonoGrande++;
                                consolidado++;
                            }
                            if (Float.valueOf(Listado.getCumplimiento()) > 0 && Float.valueOf(Listado.getCumplimiento()) < 91) {
                                abono++;
                                consolidado++;
                            }
                        }
                        if (Listado.getVencimientoMayor().equals("03- ENTRE 31 Y 60")) {

                            debidoCobrar++;
                            if (Float.valueOf(Listado.getCumplimiento()) >= 100) {
                                recaudocien++;
                                consolidado++;
                            }
                            if (Float.valueOf(Listado.getCumplimiento()) >= 91 && Float.valueOf(Listado.getCumplimiento()) < 100 ) {
                                abonoGrande++;
                                consolidado++;
                            }
                            if (Float.valueOf(Listado.getCumplimiento()) > 0 && Float.valueOf(Listado.getCumplimiento()) < 91) {
                                abono++;
                                consolidado++;
                            }
                        }
                        if (Listado.getVencimientoMayor().equals("04- ENTRE 61 Y 90")) {

                            debidoCobrar++;
                            if (Float.valueOf(Listado.getCumplimiento()) >= 100) {
                                recaudocien++;
                                consolidado++;
                            }
                            if (Float.valueOf(Listado.getCumplimiento()) >= 91 && Float.valueOf(Listado.getCumplimiento()) < 100 ) {
                                abonoGrande++;
                                consolidado++;
                            }
                            if (Float.valueOf(Listado.getCumplimiento()) > 0 && Float.valueOf(Listado.getCumplimiento()) < 91) {
                                abono++;
                                consolidado++;
                            }
                        }
                        if (Listado.getVencimientoMayor().equals("05- ENTRE 91 Y 120")) {

                            debidoCobrar++;
                            if (Float.valueOf(Listado.getCumplimiento()) >= 100) {
                                recaudocien++;
                                consolidado++;
                            }
                            if (Float.valueOf(Listado.getCumplimiento()) >= 91 && Float.valueOf(Listado.getCumplimiento()) < 100 ) {
                                abonoGrande++;
                                consolidado++;
                            }
                            if (Float.valueOf(Listado.getCumplimiento()) > 0 && Float.valueOf(Listado.getCumplimiento()) < 91) {
                                abono++;
                                consolidado++;
                            }
                        }
                        if (Listado.getVencimientoMayor().equals("06- ENTRE 121 Y 180")) {

                            debidoCobrar++;
                            if (Float.valueOf(Listado.getCumplimiento()) >= 100) {
                                recaudocien++;
                                consolidado++;
                            }
                            if (Float.valueOf(Listado.getCumplimiento()) >= 90 && Float.valueOf(Listado.getCumplimiento()) < 100 ) {
                                abonoGrande++;
                                consolidado++;
                            }
                            if (Float.valueOf(Listado.getCumplimiento()) > 0 && Float.valueOf(Listado.getCumplimiento()) < 91) {
                                abono++;
                                consolidado++;
                            }
                        }
                        if (Listado.getVencimientoMayor().equals("07- ENTRE 180 Y 210") ||
                              Listado.getVencimientoMayor().equals("08- ENTRE 210 Y 240")||
                              Listado.getVencimientoMayor().equals("09- ENTRE 240 Y 270")||
                              Listado.getVencimientoMayor().equals("10- ENTRE 270 Y 300")||
                              Listado.getVencimientoMayor().equals("11- ENTRE 300 Y 330")||
                              Listado.getVencimientoMayor().equals("12- ENTRE 330 Y 365") ) {

                            debidoCobrar++;
                            if (Float.valueOf(Listado.getCumplimiento()) >= 100) {
                                recaudocien++;
                                consolidado++;
                            }
                            if (Float.valueOf(Listado.getCumplimiento()) >= 91 && Float.valueOf(Listado.getCumplimiento()) < 100) {
                                abonoGrande++;
                                consolidado++;
                            }
                            if (Float.valueOf(Listado.getCumplimiento()) > 0 && Float.valueOf(Listado.getCumplimiento()) < 91) {
                                abono++;
                                consolidado++;
                            }
                        }
                        if (Listado.getVencimientoMayor().equals("13- MAYOR A 1 ANIO")) {

                            debidoCobrar++;
                            if (Float.valueOf(Listado.getCumplimiento()) >= 100) {
                                recaudocien++;
                                consolidado++;
                            }
                            if (Float.valueOf(Listado.getCumplimiento()) >= 91 && Float.valueOf(Listado.getCumplimiento()) < 100 ) {
                                abonoGrande++;
                                consolidado++;
                            }
                            if (Float.valueOf(Listado.getCumplimiento()) > 0 && Float.valueOf(Listado.getCumplimiento()) < 91) {
                                abono++;
                                consolidado++;
                            }
                        }
                      
                      //aqui van las trs segun los vencimientos..
                      if (rowspan == debidoCobrar && Listado.getVencimientoMayor().equals("01- CORRIENTE")) {
                         out.println("<tr>");
                             out.println("<td width='30%' style='text-align: center;'>" + Listado.getVencimientoMayor() + "</td>");
                             out.println("<td style='text-align: center;' >" + debidoCobrar + "</td>");
                             out.println("<td style='text-align: center;' >" + recaudocien + "</td>");
                             out.println("<td style='text-align: center;' >" + abonoGrande + "</td>");
                             out.println("<td style='text-align: center;' >" + abono + "</td>");
                             out.println("<td style='text-align: center;' >" + (debidoCobrar - consolidado) + "</td>");
                         out.println("<tr>");
                      
                         totalDebidoCobrar = debidoCobrar;
                         totalRecaudocien = recaudocien;
                         totalAbonoGrande = abonoGrande;
                         totalAbono = abono;
                         totalSinPagos=(debidoCobrar - consolidado);
                         
                         debidoCobrar=0;
                         recaudocien=0;
                         abonoGrande=0;
                         abono=0;
                         consolidado=0;
                      }
                      
                      
                      if (rowspan1 == debidoCobrar && Listado.getVencimientoMayor().equals("02- 1 A 30")) {
                         out.println("<tr>");
                             out.println("<td width='30%' style='text-align: center;' >" + Listado.getVencimientoMayor() + "</td>");
                             out.println("<td style='text-align: center;' >" + debidoCobrar + "</td>");
                             out.println("<td style='text-align: center;' >" + recaudocien + "</td>");
                             out.println("<td style='text-align: center;' >" + abonoGrande + "</td>");
                             out.println("<td style='text-align: center;' >" + abono + "</td>");
                             out.println("<td style='text-align: center;' >" + (debidoCobrar - consolidado) + "</td>");
                         out.println("<tr>");
                         
                         totalDebidoCobrar+= debidoCobrar;
                         totalRecaudocien+= recaudocien;
                         totalAbonoGrande += abonoGrande;
                         totalAbono += abono;
                         totalSinPagos+=(debidoCobrar - consolidado);
                         
                         debidoCobrar=0;
                         recaudocien=0;
                         abonoGrande=0;
                         abono=0;
                         consolidado=0;
                      } 
                      
                       if (rowspan2 == debidoCobrar && Listado.getVencimientoMayor().equals("03- ENTRE 31 Y 60")) {
                         out.println("<tr>");
                             out.println("<td width='30%' style='text-align: center;' >" + Listado.getVencimientoMayor() + "</td>");
                             out.println("<td style='text-align: center;' >" + debidoCobrar + "</td>");
                             out.println("<td style='text-align: center;' >" + recaudocien + "</td>");
                             out.println("<td style='text-align: center;' >" + abonoGrande + "</td>");
                             out.println("<td style='text-align: center;' >" + abono + "</td>");
                             out.println("<td style='text-align: center;' >" + (debidoCobrar - consolidado) + "</td>");
                         out.println("<tr>");
                         
                         totalDebidoCobrar+= debidoCobrar;
                         totalRecaudocien+= recaudocien;
                         totalAbonoGrande+= abonoGrande;
                         totalAbono += abono;
                         totalSinPagos+=(debidoCobrar - consolidado);
                        
                         debidoCobrar=0;
                         recaudocien=0;
                         abonoGrande=0;
                         abono=0;
                         consolidado=0;
                      }
                       
                      if (rowspan3 == debidoCobrar && Listado.getVencimientoMayor().equals("04- ENTRE 61 Y 90")) {
                         out.println("<tr>");
                             out.println("<td width='30%' style='text-align: center;' >" + Listado.getVencimientoMayor() + "</td>");
                             out.println("<td style='text-align: center;' >" + debidoCobrar + "</td>");
                             out.println("<td style='text-align: center;' >" + recaudocien + "</td>");
                             out.println("<td style='text-align: center;' >" + abonoGrande + "</td>");
                             out.println("<td style='text-align: center;' >" + abono + "</td>");
                             out.println("<td style='text-align: center;' >" + (debidoCobrar - consolidado) + "</td>");
                         out.println("<tr>");
                         
                         totalDebidoCobrar+= debidoCobrar;
                         totalRecaudocien+= recaudocien;
                         totalAbonoGrande+= abonoGrande;
                         totalAbono += abono;
                         totalSinPagos+=(debidoCobrar - consolidado);
                         
                         debidoCobrar=0;
                         recaudocien=0;
                         abonoGrande=0;
                         abono=0;
                         consolidado=0;
                      }   
                      
                      
                      if (rowspan4 == debidoCobrar && Listado.getVencimientoMayor().equals("05- ENTRE 91 Y 120")) {
                         out.println("<tr>");
                             out.println("<td width='30%' style='text-align: center;' >" + Listado.getVencimientoMayor() + "</td>");
                             out.println("<td style='text-align: center;' >" + debidoCobrar + "</td>");
                             out.println("<td style='text-align: center;'>" + recaudocien + "</td>");
                             out.println("<td style='text-align: center;' >" + abonoGrande + "</td>");
                             out.println("<td style='text-align: center;' >" + abono + "</td>");
                             out.println("<td style='text-align: center;' >" + (debidoCobrar - consolidado) + "</td>");
                         out.println("<tr>");
                         
                         totalDebidoCobrar+= debidoCobrar;
                         totalRecaudocien+= recaudocien;
                         totalAbonoGrande+= abonoGrande;
                         totalAbono += abono;
                         totalSinPagos+=(debidoCobrar - consolidado);
                         
                         debidoCobrar=0;
                         recaudocien=0;
                         abonoGrande=0;
                         abono=0;
                         consolidado=0;
                      }  
                      
                      if (rowspan5 == debidoCobrar && Listado.getVencimientoMayor().equals("06- ENTRE 121 Y 180")) {
                         out.println("<tr>");
                             out.println("<td width='30%' style='text-align: center;' >" + Listado.getVencimientoMayor() + "</td>");
                             out.println("<td style='text-align: center;'>" + debidoCobrar + "</td>");
                             out.println("<td style='text-align: center;' >" + recaudocien + "</td>");
                             out.println("<td style='text-align: center;' >" + abonoGrande + "</td>");
                             out.println("<td style='text-align: center;' >" + abono + "</td>");
                             out.println("<td style='text-align: center;' >" + (debidoCobrar - consolidado) + "</td>");
                         out.println("<tr>");
                         
                         totalDebidoCobrar+= debidoCobrar;
                         totalRecaudocien+= recaudocien;
                         totalAbonoGrande+= abonoGrande;
                         totalAbono += abono;
                         totalSinPagos+=(debidoCobrar - consolidado);
                         
                         debidoCobrar=0;
                         recaudocien=0;
                         abonoGrande=0;
                         abono=0;
                         consolidado=0;
                      } 
                      
                      
                      if (rowspan6 == debidoCobrar && (Listado.getVencimientoMayor().equals("07- ENTRE 180 Y 210") ||
                                                        Listado.getVencimientoMayor().equals("08- ENTRE 210 Y 240")||
                                                        Listado.getVencimientoMayor().equals("09- ENTRE 240 Y 270")||
                                                        Listado.getVencimientoMayor().equals("10- ENTRE 270 Y 300")||
                                                        Listado.getVencimientoMayor().equals("11- ENTRE 300 Y 330")||
                                                        Listado.getVencimientoMayor().equals("12- ENTRE 330 Y 365") )) {
                         out.println("<tr>");
                             out.println("<td width='30%' style='text-align: center;' >07- ENTRE 180 Y 365</td>");
                             out.println("<td style='text-align: center;' >" + debidoCobrar + "</td>");
                             out.println("<td style='text-align: center;' >" + recaudocien + "</td>");
                             out.println("<td style='text-align: center;' >" + abonoGrande + "</td>");
                             out.println("<td style='text-align: center;' >" + abono + "</td>");
                             out.println("<td style='text-align: center;' >" + (debidoCobrar - consolidado) + "</td>");
                         out.println("<tr>");
                         
                         totalDebidoCobrar+= debidoCobrar;
                         totalRecaudocien+= recaudocien;
                         totalAbonoGrande+= abonoGrande;
                         totalAbono += abono;
                         totalSinPagos+=(debidoCobrar - consolidado);
                        
                         debidoCobrar=0;
                         recaudocien=0;
                         abonoGrande=0;
                         abono=0;
                         consolidado=0;
                      } 
                      
                     if (rowspan7 == debidoCobrar && Listado.getVencimientoMayor().equals("13- MAYOR A 1 ANIO")) {
                         out.println("<tr>");
                             out.println("<td width='30%' style='text-align: center;'  > 8- MAYOR A 1 AÑO </td>");
                             out.println("<td style='text-align: center;' >" + debidoCobrar + "</td>");
                             out.println("<td style='text-align: center;' >" + recaudocien + "</td>");
                             out.println("<td style='text-align: center;' >" + abonoGrande + "</td>");
                             out.println("<td style='text-align: center;' >" + abono + "</td>");
                             out.println("<td style='text-align: center;' >" + (debidoCobrar - consolidado) + "</td>");
                         out.println("<tr>");
                         
                         totalDebidoCobrar+= debidoCobrar;
                         totalRecaudocien+= recaudocien;
                         totalAbonoGrande+= abonoGrande;
                         totalAbono += abono;
                         totalSinPagos+=(debidoCobrar - consolidado);
                          
                         debidoCobrar=0;
                         recaudocien=0;
                         abonoGrande=0;
                         abono=0;
                         consolidado=0;
                         
                      }
                }
                    
                     out.println("<tr>");
                             out.println("<th width='30%' style='text-align: center;' >TOTALES</th>");
                             out.println("<th style='text-align: center;' >" + totalDebidoCobrar + "</th>");
                             out.println("<th style='text-align: center;' >" + totalRecaudocien + "</th>");
                             out.println("<th style='text-align: center;' >" + totalAbonoGrande + "</th>");
                             out.println("<th style='text-align: center;' >" + totalAbono + "</th>");
                             out.println("<th style='text-align: center;' >" + totalSinPagos + "</th>");
                      out.println("<tr>");
                %>
            
    
    </table>

     <script>
         $("#tbl_proceso tr:even").addClass("even");
         $("#tbl_proceso tr:odd").addClass("odd");
		 
		 $(
		   function()
		   {
			  $("#tbl_proceso tr").hover(
			   function()
			   {
				$(this).addClass("highlight");
			   },
			   function()
			   {
				$(this).removeClass("highlight");
			   }
			  )
		   }
		  )
     </script>
</html>
