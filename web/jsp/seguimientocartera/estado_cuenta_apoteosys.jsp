<%--
    Document   : Modulo de Seguimiento Cartera Fintra
    Created on : 20/05/2019, 11:00:00 AM
    Author     : hcuello
--%>

<%@page import="com.tsp.operation.model.beans.Usuario"%>
<%@page import="java.text.DecimalFormat"%>
<%@page import="java.text.DecimalFormatSymbols"%>
<%@ page import="com.tsp.operation.model.beans.SeguimientoCarteraBeans"%>
<%@ page import="com.tsp.operation.model.services.SeguimientoCarteraService"%>

<%@ page contentType="text/html; charset=UTF-8" language="java" %>
<%@ page session   ="true"%>
<%@ page errorPage ="/error/ErrorPage.jsp"%>
<%@ page import    ="java.util.*" %>
<%@ page import    ="java.text.SimpleDateFormat" %>
<%@ page import    ="com.tsp.util.*" %>
<%@ page import    ="javax.servlet.*" %>
<%@ page import    ="javax.servlet.http.*" %>
<%@ include file="/WEB-INF/InitModel.jsp"%>

<%
    String unidad_negocio = request.getParameter("unidad_negocio") != "" ? request.getParameter("unidad_negocio") : "";
    String Negocio = request.getParameter("negocio") != "" ? request.getParameter("negocio") : "";
    String Accion = request.getParameter("ACCIONE") != "" ? request.getParameter("ACCIONE") : "";
%>


<script language="JavaScript1.2">

$(document).ready(function(){
    
    console.log("ECA_unidad_negocio: "+$('#_unidad_negocio').val());
    console.log("ECA_negocio: "+$('#_negocio').val());
    console.log("ECA_accion "+$('#_accion').val());
    
    ConsultarECApoteosys($('#_accion').val(), $('#_negocio').val(), $('#_unidad_negocio').val());
    
});
</script>

<input type="hidden" name="_unidad_negocio" id="_unidad_negocio" value="<%=unidad_negocio%>"/>
<input type="hidden" name="_negocio" id="_negocio" value="<%=Negocio%>"/>
<input type="hidden" name="_accion" id="_accion" value="<%=Accion%>"/>

<div id="Canasta" style="width :auto; max-height:700px;overflow: auto;" >

</div>
<script type="text/javascript">
                $("#tbl_proceso tr:even").addClass("even");
                $("#tbl_proceso tr:odd").addClass("odd");

                $(
                        function()
                        {
                            $("#tbl_proceso tr").hover(
                                    function()
                                    {
                                        $(this).addClass("highlight");
                                    },
                                    function()
                                    {
                                        $(this).removeClass("highlight");
                                    }
                            )
                        }
                )

</script>
