<%--
    Document   : Modulo de Seguimiento Cartera Fintra
    Created on : 19/05/2014, 11:00:00 AM
    Author     : hcuello
--%>

<%@page import="com.tsp.operation.model.beans.Usuario"%>
<%@page import="java.text.DecimalFormat"%>
<%@page import="java.text.DecimalFormatSymbols"%>
<%@ page import="com.tsp.operation.model.beans.SeguimientoCarteraBeans"%>
<%@ page import="com.tsp.operation.model.services.SeguimientoCarteraService"%>

<%@ page contentType="text/html; charset=UTF-8" language="java" %>
<%@ page session   ="true"%>
<%@ page errorPage ="/error/ErrorPage.jsp"%>
<%@ page import    ="java.util.*" %>
<%@ page import    ="java.text.SimpleDateFormat" %>
<%@ page import    ="com.tsp.util.*" %>
<%@ page import    ="javax.servlet.*" %>
<%@ page import    ="javax.servlet.http.*" %>
<%@ include file="/WEB-INF/InitModel.jsp"%>


<%
    String unidad_negocio = request.getParameter("unidad_negocio") != "" ? request.getParameter("unidad_negocio") : "";
    int periodo_foto = Integer.parseInt(request.getParameter("periodo_foto"));
    
    String aldia = request.getParameter("aldia");
    String avencer = request.getParameter("avencer");
    String vencido = request.getParameter("vencido");
    String SuperHaving = request.getParameter("SuperHaving");
    String StatusVcto = request.getParameter("StatusVcto");
    String UserLogin = request.getParameter("UserLogin");   
    String DiaMes = request.getParameter("DayMonth");    

    String item = "";
    
    Usuario usuario = (Usuario) session.getAttribute("Usuario");
    SeguimientoCarteraService rqservice= new SeguimientoCarteraService(usuario.getBd());
    ArrayList listaConsolidados =  rqservice.SCarteraCargarSeguimientoBeans("SQL_CARGAR_SEGUIMIENTO_CARTERA",unidad_negocio,periodo_foto,aldia,avencer,vencido,SuperHaving,StatusVcto,item,UserLogin,DiaMes);

    DecimalFormatSymbols simbolo = new DecimalFormatSymbols();
    simbolo.setDecimalSeparator('.');
    simbolo.setGroupingSeparator(',');
    DecimalFormat formateador = new DecimalFormat("###,###.##", simbolo);
    
    
%>

<table width="1650" height="18" border='0' align='center' cellpadding="0" cellspacing="0" class="tablas" id="tbl_proceso">
    
    <tr>
        <th width='15' align='center'>#</th>
        <th width='100' align='center'>CEDULA</th>
        <th width='300' align='center'>NOMBRE CLIENTE</th>
        <th width='400' align='center'>DIRECCION</th>
        <th width='150' align='center'>BARRIO</th>
        <th width='50' align='center'>CIUDAD</th>
        <th width='80' align='center'>TELEFONO</th>
        <th width='100' align='center'>CELULAR</th>
        <th width='80' align='center'>NEGOCIO</th>
        <th width='100' align='center'>U. NEGOCIO</th>
        <th width='150' align='center'>VENCIMIENTO MAYOR</th>
        <th width='15' align='center'>DIA PAGO</th>
        <th width='100' align='center'>VALOR SALDO</th>
        <th width='100' align='center'>DEBIDO COBRAR</th>
        <th width='100' align='center'>RECAUDO</th>
        <th width='50' align='center'>% CUMPL</th>
        <th width='100' align='center' data-tsorter="numeric">VALOR A PAGAR</th>
        <th width='50' align='center'> FECHA ULT. COMPROMISO</th>
        <th width='40' align='center'> REEST.</th>
        <th width='40' align='center'> JURID.</th>
        
    </tr><%

    float SumaValorSaldo = 0;
    float SumaDebidoCobrar = 0;
    float SumaRecaudo = 0;
    float PercCumplimiento = 0;
    float SumaValoraPagar = 0;
    int CuentaRows = 1;

    if ( listaConsolidados.size() > 0 ) {
        
        for (int i = 0; i < listaConsolidados.size(); i++) { 
        
            SeguimientoCarteraBeans ListadoContenido = (SeguimientoCarteraBeans) listaConsolidados.get(i);%>
            
                <tr  oncontextmenu="Cargar_click_derecho('<%=ListadoContenido.getNegocio()%>','<%=ListadoContenido.getCedula()%>','<%=ListadoContenido.getCodDpto()%>','<%=ListadoContenido.getCodCiudad()%>','<%=ListadoContenido.getBarrio()%>','<%=ListadoContenido.getDireccion()%>','<%=ListadoContenido.getConvenio()%>',event);return false;" >
                        <td align="justify"><%=CuentaRows%></td>
                        <td align="justify"><%=ListadoContenido.getCedula()%></td>
                        <td align="justify"><%=ListadoContenido.getNombreCliente()%></td>
                        <td align="justify"><%=ListadoContenido.getDireccion()%></td>
                        <td align="justify"><%=ListadoContenido.getBarrio()%></td>
                        <td align="justify"><%=ListadoContenido.getCiudad()%></td>
                        <td align="justify"><%=ListadoContenido.getTelefono()%></td>
                        <td align="justify"><%=ListadoContenido.getTelContacto()%></td>
                        <td align="justify"><%=ListadoContenido.getNegocio()%></td>
                        <td align="justify"><%=ListadoContenido.getUndNegocio()%></td>
                        <td align="justify"><%=ListadoContenido.getVencimientoMayor()%></td>
                        <td align="justify"><%=ListadoContenido.getDiaPago()%></td>
                        <td align="right">$<%=formateador.format(Float.valueOf(ListadoContenido.getValorAsignado()))%></td>
                        <td align="right">$<%=formateador.format(Float.valueOf(ListadoContenido.getDebidoCobrar()))%></td>
                        <td align="right">$<%=formateador.format(Float.valueOf(ListadoContenido.getRecaudoxCuota()))%></td>
                        <td align="right"><%=ListadoContenido.getCumplimiento()%>%</td>
                        <td align="right">$<%=formateador.format(Float.valueOf(ListadoContenido.getValoraPagar()))%></td>
                        <td align="right"><%=ListadoContenido.getFechaUltimoCompromiso()%></td>
                        <td align="center"><%=ListadoContenido.getReestructuracion()%></td>
                        <td align="center"><%=ListadoContenido.getJuridica()%></td>
                </tr><%
                
                SumaValorSaldo  += Float.valueOf(ListadoContenido.getValorAsignado());
                SumaDebidoCobrar  += Float.valueOf(ListadoContenido.getDebidoCobrar());
                SumaRecaudo  += Float.valueOf(ListadoContenido.getRecaudoxCuota());
                SumaValoraPagar  += Float.valueOf(ListadoContenido.getValoraPagar());
                
                CuentaRows = CuentaRows + 1;
        }
                
        PercCumplimiento = (SumaRecaudo/SumaDebidoCobrar)*100;
        PercCumplimiento = (SumaRecaudo/SumaValorSaldo)*100;%>

        <tr>
            <th align="right" colspan="12"><strong>TOTALES:</strong></td>
            <th align="left"><strong>$<%=formateador.format(SumaValorSaldo)%></strong></td>
            <th align="left"><strong>$<%=formateador.format(SumaDebidoCobrar)%></strong></td>
            <th align="left"><strong>$<%=formateador.format(SumaRecaudo)%></strong></td>
            <th align="left"><strong><%=formateador.format(PercCumplimiento)%>%</strong></td>
            <th align="left"><strong>$<%=formateador.format(SumaValoraPagar)%></strong></td>  
            <th align="left" colspan="3"></th>   
        </tr><%


    }else{%>
    
        <tr>
            <td colspan="33">SIN RESULTADOS</td>
        </tr><%
    
    }%>
    </tbody>
</table>
<input type="hidden" name="PercCump" id="PercCump" align="center" value="<%=formateador.format(CuentaRows-1)%> / <%=formateador.format(PercCumplimiento)%>" />

    <script>
         $("#tbl_proceso tr:even").addClass("even");
         $("#tbl_proceso tr:odd").addClass("odd");
		 
		 $(
		   function()
		   {
			  $("#tbl_proceso tr").hover(
			   function()
			   {
				$(this).addClass("highlight");
			   },
			   function()
			   {
				$(this).removeClass("highlight");
			   }
			  )
		   }
		  )
     </script>