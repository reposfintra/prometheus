<%--
    Document   : Modulo de Seguimiento Cartera Fintra
    Created on : 19/05/2014, 11:00:00 AM
    Author     : hcuello
--%>

<%@ page import="com.tsp.operation.model.beans.Usuario"%>
<%@ page import="com.tsp.operation.model.beans.CmbGeneralScBeans"%>
<%@ page import="com.tsp.operation.model.services.SeguimientoCarteraService"%>

<%@ page contentType="text/html; charset=UTF-8" language="java" %>
<%@ page session   ="true"%>
<%@ page errorPage ="/error/ErrorPage.jsp"%>
<%@ page import    ="java.util.*" %>
<%@ page import    ="java.text.SimpleDateFormat" %>
<%@ page import    ="com.tsp.util.*" %>
<%@ page import    ="javax.servlet.*" %>
<%@ page import    ="javax.servlet.http.*" %>
<%@ include file="/WEB-INF/InitModel.jsp"%>


<%
    Usuario usuario = (Usuario) session.getAttribute("Usuario");
    SeguimientoCarteraService rqservice= new SeguimientoCarteraService(usuario.getBd());
    ArrayList listaCombo =  rqservice.GetComboGenerico("SQL_OBTENER_UNIDAD_NEGOCIO","id","descripcion", usuario.getLogin());
    ArrayList listaPeridos =  rqservice.GetComboGenericoStr("SQL_OBTENER_PERIODOS_FOTO","id","descripcion","");

    String LoginUsuario = usuario.getLogin();

          /* <jsp:include page="/WEB-INF/toptsp.jsp?encabezado=REQUISICIONES A MI PROCESO"/> */

    Date dNow = new Date( );
    /*
    SimpleDateFormat ft = new SimpleDateFormat ("E yyyy.MM.dd 'at' hh:mm:ss a zzz");
    out.print( "<h2 align=\"center\">" + ft.format(dNow) + "</h2>");   
    */
    SimpleDateFormat nd = new SimpleDateFormat ("yyyy");
    String aniocte = nd.format(dNow);
    
    SimpleDateFormat md = new SimpleDateFormat ("M");
    String mescte = md.format(dNow);
    
    int acorriente = Integer.parseInt(aniocte);
    int mescorriente = Integer.parseInt(mescte);
    
    //out.print( "<h2 align=\"center\">" + acorriente + "</h2>");
    //out.print( "<h2 align=\"center\">" + mescorriente + "</h2>");
%>

<html>
    <head>
        <title>Recaudos de Cartera Consolidados</title>

        <link rel="stylesheet" type="text/css" href="<%=BASEURL%>/css/jCal/jscal2.css" />
        <link rel="stylesheet" type="text/css" href="<%=BASEURL%>/css/jCal/border-radius.css" />
        <link href="<%=BASEURL%>/css/estilostsp.css" rel="stylesheet" type="text/css">
        <link href="<%=BASEURL%>/css/default.css" rel="stylesheet" type="text/css">
        <link href="<%=BASEURL%>/css/alert.css" rel="stylesheet" type="text/css"/>
        <link href="<%=BASEURL%>/css/alphacube.css" rel="stylesheet" type="text/css"/>
        <link href="<%=BASEURL%>/css/mac_os_x.css" rel="stylesheet" type="text/css">
        <link href="<%=BASEURL%>/css/estilotabla.css" rel="stylesheet" type="text/css">
        
        <link href="<%=BASEURL%>/css/style_azul.css" rel="stylesheet" type="text/css">

        <script type="text/javascript" src="<%=BASEURL%>/js/jCal/jscal2.js"></script>
        <script type="text/javascript" src="<%=BASEURL%>/js/jCal/lang/es.js"></script>
        <script src="<%=BASEURL%>/js/boton.js" type="text/javascript"></script>
        <script src="<%=BASEURL%>/js/prototype.js" type="text/javascript"></script>
        <script src="<%=BASEURL%>/js/datacredito.js" type="text/javascript"></script>
        <script src="<%=BASEURL%>/js/effects.js" type="text/javascript"></script>
        <script src="<%=BASEURL%>/js/window.js" type="text/javascript"></script>
        <script src="<%=BASEURL%>/js/window_effects.js" type="text/javascript"></script>
        
        <script type="text/javascript" src="<%=BASEURL%>/js/jquery-1.4.2.min.js"></script>
        <script language="JavaScript" type="text/javascript" src="<%=BASEURL%>/js/jquery-ui-1.8.5.custom.min.js"></script>
        <script language="JavaScript1.2">
            
            $(document).ready(function(){
              //cargar_requisiciones();
              //cargar_mis_pendientes();
              //$("#cantidad").numeric();
              //$("#valor").numeric();
                $("#div_nueva_requisicion").draggable({ handle: "#drag_nueva_requisicion"});
                //$("#div_detalle_requisicion").draggable({ handle: "#drag_detalle_requisicion"});
                //$("#div_editar_requisicion").draggable({ handle: "#drag_editar_requisicion"});

            });            

            var ns4 = (document.layers);
            var ie4 = (document.all && !document.getElementById);
            var ie5 = (document.all && document.getElementById);
            var ns6 = (!document.all && document.getElementById);
            var msg = new Array();

            function Posicionar_div(id_objeto,e){
              //alert(e);
              //alert(window.event);
              obj = document.getElementById(id_objeto);
              var posx = 0;
              var posy = 0;
              if (!e) var e = window.event;
              if (e.pageX || e.pageY) {
               //alert('page');
               posx = e.pageX;
               posy = e.pageY;
              }
              else if (e.clientX || e.clientY) {
                    //alert('client'); 
                    posx = e.clientX + document.body.scrollLeft + document.documentElement.scrollLeft;
                    posy = e.clientY + document.body.scrollTop + document.documentElement.scrollTop; 
                  }
                  else
                   alert('ninguna de las anteriores');
             //alert('posx='+posx + ' posy=' + posy);
             obj.style.left = posx;
             obj.style.top = posy;
             //alert('objleft='+posx + ' objtop=' + posy);
             //document.getElementById('posicion').innerHTML = 'scrollLeft='+document.body.scrollLeft+' scrollTop='+document.body.scrollTop+' cientX'+e.clientX +' clientY'+e.clientY;
            }            
            
            function cargar_consolidado(){
       
                if($('#unidad_negocio').val()!=""){
                    
                $('#div_espera').fadeIn('slow');
                $.ajax({
                    type: "POST",
                    url : "/fintra/jsp/seguimientocartera/recaudo_consolidado.jsp",
                    async:true,
                    dataType: "html",
                    data:{
                        unidad_negocio:$('#unidad_negocio').val(),
                        periodo_foto:$('#periodo_foto').val(),
                        UserLogin:$('#UserLogin').val()
                    },
                    success:function (data){
                        if (data!=""){
                            $("#misreq").html(data);
                            $('#div_espera').fadeOut('slow');
                            cargar_consolidado_tendecia();
                        }
                    }
                });
                
                }else {
                
                 alert('Debe seleccionar la unidad de negocio');
                }
            }
            

            function cargar_consolidado_tendecia(){
                
                $('#div_espera').fadeIn('slow');
                $.ajax({
                    type: "POST",
                    url : "/fintra/jsp/seguimientocartera/recaudo_consolidado_tendencia.jsp",
                    async:true,
                    dataType: "html",
                    data:{
                        unidad_negocio:$('#unidad_negocio').val(),
                        periodo_foto:$('#periodo_foto').val(),
                        UserLogin:$('#UserLogin').val()
                    },
                    success:function (data){
                        if (data!=""){
                            $("#tendencia").html(data);
                            $('#div_espera').fadeOut('slow');
                            cargar_consolidado_resumen();
                        }
                    }
                });
            }
            
            function cargar_consolidado_resumen(){
                
                $('#div_espera').fadeIn('slow');
                $.ajax({
                    type: "POST",
                    url : "/fintra/jsp/seguimientocartera/resumen_recaudo.jsp",
                    async:true,
                    dataType: "html",
                    data:{
                        unidad_negocio:$('#unidad_negocio').val(),
                        periodo_foto:$('#periodo_foto').val(),
                        UserLogin:$('#UserLogin').val()
                    },
                    success:function (data){
                        if (data!=""){
                            $("#resumen").html(data);
                            $('#div_espera').fadeOut('slow');
                        }
                    }
                });
            }

        </script>
        <div id="div_espera" style="display:none;z-index:1000; position:absolute"></div>
        <div id="div_nueva_requisicion" style="display:none;z-index:101; position:absolute; border: 1px solid #165FB6; background:#FFFFFF; padding: 3px 3px 3px 3px;"></div>
        <div id="div_editar_requisicion" style="display:none;z-index:100; position:absolute;border: 1px solid #165FB6; background:#FFFFFF; padding: 3px 3px 3px 3px;"></div>
        <div id="div_detalle_requisicion" style="display:none;z-index:102; position:absolute;border: 1px solid #165FB6; background:#FFFFFF; padding: 3px 3px 3px 3px;"></div>
        <div id="div_menu_click_derecho_requisicion" style="display:none;z-index:510;position:absolute;border:1px solid #165FB6; background-color:#FFFFFF; padding: 2px 2px 2px 2px;"></div>
        
    </head>
    
    <body onload="">
        
        <div id="capaSuperior" style="position:absolute; width:100%; height:100px; z-index:0; left: 0px; top: 0px;">
           <jsp:include page="/WEB-INF/toptsp.jsp?encabezado=Recaudos de Cartera Consolidados"/>
        </div>
        
        <div id="capaCentral" style="position:absolute; width:100%; height:85%; z-index:0; left: 0px; top: 100px; overflow: scroll;">
            
            <br>
            
            <center>
                
                <input type="hidden" id="baseurl" name="baseurl" value="<%=BASEURL%>">
                <input type="hidden" id="controller" name="controller" value ="<%=CONTROLLER%>"/>
                <input type="hidden" id="UserLogin" name="UserLogin" value ="<%=LoginUsuario%>"/>
                
                <table width="100%" border="0" cellpadding="0" cellspacing="1" class="labels">
                    
                    
                    <tr>
                        <td colspan="2">
                            <fieldset>
                                
                                <legend class="labels">FILTROS</legend>
                                
                                <table width="435" border="0" align="left" cellpadding="1" cellspacing="1" class="labels">
                                  <tr>
                                      
                                    <td width="190">
                                        <fieldset>
                                            <legend>UNIDADES DE NEGOCIO</legend>
                                            <table border="0" align="center" cellpadding="1" cellspacing="1" class="labels">
                                                <tr>
                                                    <td>
                                                        <select name="unidad_negocio" class="combo_180px" id="unidad_negocio" >
                                                            <option value="" selected>< -Escoger- ></option><%
                                                            for (int i = 0; i < listaCombo.size(); i++) {
                                                                CmbGeneralScBeans CmbContenido = (CmbGeneralScBeans) listaCombo.get(i);%>
                                                                 <option value="<%=CmbContenido.getIdCmb() %>"><%=CmbContenido.getDescripcionCmb() %></option><%
                                                            }%>
                                                        </select>
                                                    </td>
                                                </tr>
                                            </table>
                                        </fieldset>
                                    </td>
                                      
                                    <td width="190">
                                        <fieldset>
                                            <legend>PERIODOS FOTO</legend>
                                            <table border="0" align="center" cellpadding="1" cellspacing="1" class="labels">
                                                <tr>
                                                    <td>
                                                        <select name="periodo_foto" class="combo_180px" id="periodo_foto">
                                                            <option value="" selected="true" >Seleccionar</option>
                                                            <%
                                                            for (int i = 0; i < listaPeridos.size(); i++) {
                                                                CmbGeneralScBeans CmbPeriodoFoto = (CmbGeneralScBeans) listaPeridos.get(i);%>
                                                                <option value="<%=CmbPeriodoFoto.getIdCmbStr()%>" selected><%=CmbPeriodoFoto.getDescripcionCmb() %></option><%
                                                            }%>
                                                        </select>
                                                    </td>
                                                </tr>
                                            </table>
                                        </fieldset>
                                    </td>                                      
                                    <td><input type="button" name="New" id="New" value="BUSCAR" onclick="cargar_consolidado()" /></td>  
                                  </tr>
                                </table>
                            </fieldset>
                        </td>
                    </tr>  
  
                    <tr><td colspan="2" id="mensajes_sistema">&nbsp;</td></tr>
                    
                    <tr>
                        <td width="35%" align="center" >
                            <fieldset>
                            <legend>RECAUDO GENERAL</legend> 
                              <div id="fondo4" style="position:relative; width:660px; height:700px; visibility: visible; overflow:auto;" align="center">
                                   <div id="misreq" align="center" style="position:absolute; z-index:58; left:0px; top:0px; visibility:visible;"></div>
                            </div>
                            </fieldset>
                        </td>
                        
                        <td width="33%" align="center">
                            <fieldset>
                                <legend>TENDENCIA DEL RECAUDO</legend> 
                                    <div id="fondo5" style="position:relative; width:660px; height:700px; visibility: visible; overflow:auto;" align="center">
                                        <div id="tendencia" align="center" style="position:absolute; z-index:58; left:0px; top:0px; visibility:visible;"></div>
                                    </div>
                              </fieldset>
                        </td>                        
                        
                        <td width="33%" align="center">
                            <fieldset>
                                <legend>RESUMEN DEL RECAUDO</legend> 
                                    <div id="fondo5" style="position:relative; width:600px; height:700px; visibility: visible; overflow:auto;" align="center">
                                        <div id="resumen" align="center" style="position:absolute; z-index:58; left:0px; top:0px; visibility:visible;"></div>
                                    </div>
                              </fieldset>
                        </td>                        
                        
                        
                    </tr>
                    
                    <div id="box" style="top:281px; left:417px; position:absolute; z-index:1000; width: 43px; height: 29px;"></div>

                </table>                
                
                <br>
                <tsp:boton value="salir" onclick="parent.close();"/>
                <br><br>
               
                <center class='comentario'>
                    <div id="comentario" style="visibility: hidden" >
                        <table border="2" align="center">
                            <tr>
                                <td>
                                    <table width="100%"  border="1" align="center"  bordercolor="#F7F5F4" bgcolor="#FFFFFF">
                                        <tr>
                                            <td width="229" align="center" class="mensajes"><span class="normal"><div id="mensaje"></div></span></td>
                                            <td width="29" background="<%=BASEURL%>/images/cuadronaranja.JPG">&nbsp;</td>
                                            <td width="58">&nbsp;</td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                        </table>
                    </div>
                </center> 
                                            
            </center>
                    
             
        </div>
    </body>
</html>