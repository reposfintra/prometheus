<%-- 
    Document   : reporte_recibos_caja
    Created on : 10/03/2016, 05:07:22 PM
    Author     : user
--%>

<%@page contentType="text/html" pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
        <title>Consulta recibos de caja</title>
        <link type="text/css" rel="stylesheet" href="./css/jquery/jquery-ui/jquery-ui-2.css"/>
        <link type="text/css" rel="stylesheet" href="./css/jquery/jquery.jqGrid/ui.jqgrid.css"/>        
        <script type="text/javascript" src="./js/jquery/jquery-ui/jquery.min.js"></script>
        <script type="text/javascript" src="./js/jquery/jquery-ui/jquery.ui.min.js"></script>            
        <script type="text/javascript" src="./js/jquery/jquery.jqGrid/js/grid.locale-es.js"></script>
        <script type="text/javascript" src="./js/jquery/jquery.jqGrid/js/jquery.jqGrid.min.js"></script>
        <link type="text/css" rel="stylesheet" href="./css/buttons/botonVerde.css"/>
        <link type="text/css" rel="stylesheet" href="./css/popup.css"/>
        <script type="text/javascript" src="./js/recibos_caja.js"></script>  
        
         <!--css logica de negocio-->
        <link href="./css/recibos_caja.css" rel="stylesheet" type="text/css">

        <!--jqgrid--> 
        <script type="text/javascript" src="./js/jquery/jquery.jqGrid-4.7.0/js/grid.locale-es.js"></script>
        <script type="text/javascript" src="./js/jquery/jquery.jqGrid-4.7.0/js/jquery.jqGrid.min.js"></script>
        <script type="text/javascript" src="./js/jquery/jquery.jqGrid-4.7.0/plugins/jquery.contextmenu.js"></script>
        <script type="text/javascript" src="./js/jquery/jquery.jqGrid-4.7.0/js/jquery.tabletojson.js"></script>  
    </head>
    <body>
       <div id="capaSuperior" style="position:absolute; width:100%; height:100px; z-index:0; left: 0px; top: 0px;">
             <jsp:include page="/toptsp.jsp?encabezado=Reporte Recibos de caja"/>
        </div>
        <div id="capaCentral" style="position:absolute; width:100%; height:100%; z-index:0; left: 0px; top: 100px; ">
            <center>
                <div id="div_recibos_caja" class="frm_search">  
                    <table>
                         <tr>
                            <td colspan="2">
                                <label for="cobrador">Asesor:<b id="req_asesor" style="color:red">*</b></label>
                                <select name="cobrador" id="asesor" class="combo_180px">
                                </select> 
                            </td>
                        </tr>
                        <tr>
                            <td colspan="2">
                                <label for="fecha">Fecha Ingreso:&nbsp&nbsp&nbsp<b id="req_fecha" style="color:red">*</b></label>
                                <input type="text" id="fecha_ini" readonly style="width:82px"/>
                                <input type="text" id="fecha_fin" readonly style="width:82px"/>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <label for="estado"> Estado Recibo:&nbsp&nbsp<b id="req_estado" style="color:red">*</b></label>
                                <select name="estado" id="estado_recibo" class="combo_180px">
                                </select>                     
                            </td> 
                        </tr>
                        <tr>
                            <td>
                                <label for="recibo">No Recibo:</label>
                                <input id="num_recibo" name="recibo" size="20" type="text" maxlength="20"/>                                   
                            </td>                           
                        </tr> 
                    </table>
                    <br>
                    <div style="text-align: center;">
                        <button id="listarRecibos" class="ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only" 
                                role="button" aria-disabled="false">
                            <span class="ui-button-text">Buscar</span>
                        </button>
                        <button id="clearRecibos" class="ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only" 
                                role="button" aria-disabled="false">
                            <span class="ui-button-text">Limpiar</span>
                        </button>
                    </div>
                </div>
                <br><br>
                <table id="tabla_recibos_caja"></table>
                <div id="page_tabla_recibos_caja"></div>
            </center>
            <div id="divObservaciones" style="text-align: left;display:none">
                <h4> <span> <img id="square_red" src="/fintra/images/cuadroRojo.png" style="margin-left: 5px; height: 16px; width:16px; vertical-align: middle;"> 
                    </span> Recibos hurtados con informacion de recaudo faltante. Dichos recibos no podran ser aplicados.
                </h4>
            </div>
                <!-- Dialogo ventana reasignacion de asesor -->
            <div id="dialogReasignarAsesor"  style="display:none;">       
                <br>
                <fieldset>                   
                    <table border="0" align="left" cellpadding="1" cellspacing="1" class="labels">
                        <tr>
                            <td style="width: 25%"><span>Asesor <b style="color:red">*</b></span></td>        
                            <td style="width: 75%">
                                <select name="asesorEdit" class="combo_180px" id="asesorEdit" style="width: 270px"></select>
                            </td>
                        </tr>                       
                    </table>
                </fieldset>
            </div>
            <!-- Dialogo ventana agregar comentario> -->
            <div id="dialogAddComent" style="display:none;">       
                <br>                          
                <table border="0" align="center" cellpadding="1" cellspacing="1" class="labels">                      
                    <tr>
                        <td style="width: 20%"><span>Comentario:</span></td>   
                        <td style="width: 80%" colspan="4"><textarea id ="observ" rows="4" maxlength="300" style="width: 475px;text-transform:uppercase;resize:none;"></textarea></td>                        
                    </tr>
                </table> 
            </div>
            
            <div id="div_info_recaudo" style="display:none;">
                <table border="0" class="tabla">
                    <tr>
                        <td class="td">                          
                            <label for="consec">Consecutivo:<b id="req_consec" style="color:red">*</b></label>
                            <input id="consecutivo" name="consec" size="9" type="text" maxlength="20" readonly/>                                  
                        </td>   
                        <td>
                            <label for="estado"> Estado Recibo:&nbsp&nbsp<b id="req_estado" style="color:red">*</b></label>
                            <select name="estado" id="estado_recibo_edit" class="combo_180px">
                            </select>                     
                        </td> 
                    </tr>
                    <tr>
                        <td class="td">
                            <label for="id_cliente">Identificacion:<b id="req_ident" style="color:red">*</b></label>
                            <input id="id_cliente" name="idCliente" type="text" class="solo-numero" maxlength="12"/>                                   
                        </td>
                        <td class="td">
                            <label for="fecha_recib">Fecha Recibido:<b id="req_fec_recib" style="color:red">*</b></label>
                            <input type="text" id="fecha_recibido" name="fecha_recib" readonly/>
                        </td>   
                    </tr>
                    <tr>
                        <td class="td" colspan="3">
                            <label for="nomcli">Nombre<b id="req_nombre" style="color:red">*</b></label>
                            <input id="nombre" name="nomcli" type="text" style="width:535px" maxlength="160"/>   
                        </td>                     
                    </tr>
                    <tr>
                        <td class="td">
                            <label for="tipoRecaudo">Tipo Recaudo:<b id="req_tipo_recaudo" style="color:red">*</b></label>
                            <select id="tipo_recaudo" name="tipoRecaudo" />
                        </td>      
                        <td class="td">
                            <label for="valor">&nbsp&nbsp&nbspValor:<b id="req_vlr_recaudo" style="color:red">*</b></label>
                            <input id="valor_recaudo" name="valor" size="10" type="text" class="solo-numero"/>                                   
                        </td>
                    </tr>
                    <tr>
                        <td class="td">
                            <label for="listaNegocios">Negocios:</label>
                            <select id="listaNegocios" name="listaNegocios">
                                <option>...</option>
                            </select>
                        </td>
                    </tr>
                </table>
            </div>

            <div id="divSalidaEx" title="Exportacion" style=" display: block" >
                <p  id="msjEx" style=" display:  none"> Espere un momento por favor...</p>
                <center>
                    <img id="imgloadEx" style="position: relative;  top: 7px; display: none " src="./images/cargandoCM.gif"/>
                </center>
                <div id="respEx" style=" display: none"></div>
            </div> 
            <div id="dialogMsj" title="Mensaje" style="display:none;">
                <p style="font-size: 12px;text-align:justify;" id="msj" > Texto </p>
            </div> 
        </div>
    </body>
</html>
