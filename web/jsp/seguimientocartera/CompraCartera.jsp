<%-- 
    Document   : CompraCartera
    Created on : 3/11/2015, 10:52:18 AM
    Author     : egonzalez
--%>

<%@page contentType="text/html" pageEncoding="ISO-8859-1"%>
<%@ include file="/WEB-INF/InitModel.jsp"%>
<%@page errorPage="/error/ErrorPage.jsp" %>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
        <title>Compra Cartera</title>

        <link type="text/css" rel="stylesheet" href="./css/jquery/jquery-ui/jquery-ui.css" />
        <link href="./css/popup.css" rel="stylesheet" type="text/css">
        <link href="./css/fasecolda.css" rel="stylesheet" type="text/css">
        <script type="text/javascript" src="./js/jquery/jquery.jqGrid/js/jquery.min.js"></script>
        <script type="text/javascript" src="./js/jquery-ui-1.8.5.custom.min.js"></script>
        <script type="text/javascript" src="./js/jquery/jquery.jqGrid-4.7.0/js/grid.locale-es.js"></script>
        <script type="text/javascript" src="./js/jquery/jquery.jqGrid-4.7.0/js/jquery.jqGrid.min.js"></script>
        <script type="text/javascript" src="./js/jquery/jquery.jqGrid-4.7.0/js/jquery.tabletojson.js"></script>

        <script type="text/javascript" src="./js/CompraCartera.js"></script>


    </head>
    <body>
        <div id="capaSuperior" style="position:absolute; width:100%; height:100px; z-index:0; left: 0px; top: 0px;">
            <jsp:include page="/WEB-INF/toptsp.jsp?encabezado=COMPRA CARTERA"/>
        </div>
        <div id="capaCentral" style="position:absolute; width:100%; height:80%; z-index:0; left: 0px; top: 100px; ">
            <div id="loadfile_cc" >
                <h3>Esta ventana le ayudara en la importaci�n de la informaci�n para la creaci�n la nueva cartera</h3>
                <hr>
                <form id="formulario" name="formulario">
                    <table>
                        <tr>
                            <td class="letra_resaltada clasTd" align="left">Solicitud Credito <span style="color: red"> *</span> &nbsp;</td>
                            <td align="left">
                                <input type="file" id="archivo_credito"  name="archivo_credito">
                            </td>
                        </tr>
                        <tr>
                            <td class="letra_resaltada clasTd" align="left" >Solicitud Persona <span style="color: red"> *</span> &nbsp;</td>
                            <td align="left">
                                <input type="file" id="archivo_persona"  name="archivo_persona">
                            </td>

                        </tr>
                        <tr>
                            <td class="letra_resaltada clasTd" align="left">Solicitud Estudiante &nbsp;</td>
                            <td align="left">                           
                                <input type="file" id="archivo_estudiante"  name="archivo_estudiante">                            
                            </td>
                        </tr>
                    </table>
                </form>
                <hr>
                <button id="cargarArchivo" class="ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only" style="float: right;"
                        role="button" aria-disabled="false">
                    <span class="ui-button-text">Subir Archivo</span>
                </button>&nbsp;
                <input type="hidden" value=""  id="ctrl_carga" name="ctrl_carga"/>
                 <a href="#" class="boton">bit�cora carga</a>
                 <div id="desplegable">
                     <center class="boton"><h3 style="height:20px;padding: 10px;">Notificaciones</h3></center>
                     <hr>
                     <div id="internoNoty" >                           
                     </div>
                 </div>
           
                                         
            </div>
            <br>
            
            <div id="contenedor">
                <center>
                    <table id="tbl_cartera_nueva"></table>
                    <div id="page_cartera_nueva"></div>
                </center>     
            </div>
            
            <div id="contenedor_1">
                <center>
                    <table id="tbl_negocios_cartera_nueva"></table>
                    <div id="page_negocios_cartera_nueva"></div>
                </center>     
            </div>
          
            <div id="dialogo" title="Mensaje">
                <p id="msj"></p>

            </div>
            <div id="dialogLoading"  class="ventana" >
                <p  id="msj2">texto </p> <br/>
                <center>
                    <img src="<%=BASEURL%>/images/cargandoCM.gif"/>
                </center>
            </div>
        </div>

    </body>
</html>
