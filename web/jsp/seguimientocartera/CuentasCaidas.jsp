<%-- 
    Document   : CuentasCaidas
    Created on : 11/02/2015, 09:52:17 AM
    Author     : lcanchila
--%>

<%@page import="com.tsp.operation.model.beans.CmbGeneralScBeans"%>
<%@page import="java.util.ArrayList"%>
<%@page import="com.tsp.operation.model.services.SeguimientoCarteraService"%>
<%@page contentType="text/html" pageEncoding="ISO-8859-1"%>
<%@ include file="/WEB-INF/InitModel.jsp"%>
<!DOCTYPE html>
<%
    SeguimientoCarteraService rqservice= new SeguimientoCarteraService();
    ArrayList listaCombo =  rqservice.GetComboGenerico("SQL_OBTENER_UNIDAD_NEGOCIO_BASE","id","descripcion","");
    ArrayList listaPeridos =  rqservice.GetComboGenerico("SQL_OBTENER_PERIODOS_FOTO","id","descripcion","");
%>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
        <link type="text/css" rel="stylesheet" href="<%=BASEURL%>/css/jquery/jquery-ui/jquery-ui.css"/>
        <link href="<%=BASEURL%>/css/estilostsp.css" rel="stylesheet" type="text/css">
        <link href="<%=BASEURL%>/css/popup.css" rel="stylesheet" type="text/css">
               
        <script type="text/javascript" src="<%=BASEURL%>/js/jquery/jquery.jqGrid/js/jquery.min.js"></script>
        <script type="text/javascript" src="<%=BASEURL%>/js/jquery-ui-1.8.5.custom.min.js"></script>
        <script type="text/javascript" src="<%=BASEURL%>/js/jquery/jquery.jqGrid/js/grid.locale-es.js"></script>
        <script type="text/javascript" src="<%=BASEURL%>/js/jquery/jquery.jqGrid/js/jquery.jqGrid.min.js"></script>
        <script type="text/javascript" src="<%=BASEURL%>/js/auditoriaIngresos.js"></script>
    </head>
    <body>
        <div id="capaSuperior" style="position:absolute; width:100%; height:100px; z-index:0; left: 0px; top: 0px;">
           <jsp:include page="/WEB-INF/toptsp.jsp?encabezado=Reporte Cuentas Ca�das"/>
        </div>
        <div id="capaCentral" style="position:absolute; width:100%; height:85%; z-index:0; left: 0px; top: 100px; overflow: scroll;">
        <br>  </br>  
        <div align="center">
            <div id="filtros" style="display:table-cell; " class="ui-jqgrid ui-widget ui-widget-content ui-corner-all">
                    </br>
                    <table style=" width: 600px">
                        <tr>
                            <td style="padding-left: 8px"><span> Unidad de Negocio:</span></td>
                            <td>
                                <select name="unidad_negocio" class="combo_180px" id="unidad_negocio"  >
                                    <%
                                        for (int i = 0; i < listaCombo.size(); i++) {
                                            CmbGeneralScBeans CmbContenido = (CmbGeneralScBeans) listaCombo.get(i);%>
                                    <option value="<%=CmbContenido.getIdCmb()%>"><%=CmbContenido.getDescripcionCmb()%></option><%
                                        }%>
                                </select>
                                </td>
                           <td><span> Periodo Foto:</span></td>
                            <td><select name="periodo" class="combo_180px" id="periodo" ><%
                                        for (int i = 0; i < listaPeridos.size(); i++) {
                                            CmbGeneralScBeans CmbPeriodoFoto = (CmbGeneralScBeans) listaPeridos.get(i);%>
                                        <option value="<%=CmbPeriodoFoto.getIdCmb()%>" selected><%=CmbPeriodoFoto.getDescripcionCmb()%></option><%
                                                                    }%>
                                </select>
                            </td>
                            <td><button id="buscar" class="ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only" role="button" aria-disabled="false" onclick="cargarCuentasCaidas();">
                                                    <span class="ui-button-text">Buscar</span>
                                                </button></td>
                        </tr> 
                    </table>
                    </br>
            </div> 
               </br> </br>                
               <table id="CuentasCaidas"></table>
              <div id="dialogo" class="ventana" title="Mensaje">
                    <p  id="msj1" > </p>
               </div>
               <div id="divSalida" title="Exportacion" style=" display: block" >
                <p  id="msj" style=" display:  none; ">Espere un momento por favor... </p> 
                    <center><img id="imgload1" style="position:relative;  top: 2px; display: none " src="<%=BASEURL%>/images/cargandoCM.gif"/></center>
                    <div id="resp1" style=" display: none"></div>
               </div>
               
                    <div  id="div_deta_cuentas" style=" width: 2000px" class="ventana" title="Detalle Cuentas Caidas" > 
                    <br>
                    <table id="deta_cuentasCaidas" ></table>
                </div>      
            </div>
            </br>
                 
       </div>  
    </body>
</html>
