<%-- 
    Document   : ReporteCompromisosPago
    Created on : 27/11/2015, 11:44:51 AM
    Author     : mcastillo
--%>

<%@page contentType="text/html" pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
        <title>Compromisos de pago cartera</title>
        <link type="text/css" rel="stylesheet" href="./css/jquery/jquery-ui/jquery-ui-2.css"/>
        <link type="text/css" rel="stylesheet" href="./css/jquery/jquery.jqGrid/ui.jqgrid.css"/>        
        <script type="text/javascript" src="./js/jquery/jquery-ui/jquery.min.js"></script>
        <script type="text/javascript" src="./js/jquery/jquery-ui/jquery.ui.min.js"></script>            
        <script type="text/javascript" src="./js/jquery/jquery.jqGrid/js/grid.locale-es.js"></script>
        <script type="text/javascript" src="./js/jquery/jquery.jqGrid/js/jquery.jqGrid.min.js"></script>
        <link type="text/css" rel="stylesheet" href="./css/buttons/botonVerde.css"/>
        <link type="text/css" rel="stylesheet" href="./css/popup.css"/>
        <link href="./css/style_azul.css" rel="stylesheet" type="text/css">
        <script type="text/javascript" src="./js/seguimiento_cartera.js"></script>  
        
         <!--css logica de negocio-->
        <link href="./css/asobancaria.css" rel="stylesheet" type="text/css">

        <!--jqgrid--> 
        <script type="text/javascript" src="./js/jquery/jquery.jqGrid-4.7.0/js/grid.locale-es.js"></script>
        <script type="text/javascript" src="./js/jquery/jquery.jqGrid-4.7.0/js/jquery.jqGrid.min.js"></script>
        <script type="text/javascript" src="./js/jquery/jquery.jqGrid-4.7.0/plugins/jquery.contextmenu.js"></script>
        <script type="text/javascript" src="./js/jquery/jquery.jqGrid-4.7.0/js/jquery.tabletojson.js"></script>  
    </head>
    <body>
        <div id="capaSuperior" style="position:absolute; width:100%; height:100px; z-index:0; left: 0px; top: 0px;">
             <jsp:include page="/toptsp.jsp?encabezado=Reporte de Compromisos de pago"/>
        </div>
        <div id="capaCentral" style="position:absolute; width:100%; height:100%; z-index:0; left: 0px; top: 100px; ">
            <center>
                <div id="div_compromisos" class="frm_search" style="width:550px">  
                    <table>
                        <tr>
                            <td colspan="2">
                                <label for="fecha">Fecha Compromiso:</label>
                                <input type="text" id="fecha_compromiso_ini" />
                                <input type="text" id="fecha_compromiso_fin" />
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <label for="agente"> Agentes de campo:&nbsp&nbsp</label>
                                <select name="agente" id="agente_campo" class="combo_180px">
                                </select>                     
                            </td>     
                            <td>
                                <label for="domicilio"> Domicilio:</label>
                                <select name="domicilio" id="cbo_domicilio" class="combo_60px">
                                      <option value="S">Si</option>
                                      <option value="N">No</option> 
                                </select>                     
                            </td>                           
                            <td style="padding-left: 10px">
                                <button id="listarCompromisos" class="ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only" 
                                        role="button" aria-disabled="false">
                                    <span class="ui-button-text">Buscar</span>
                                </button>
                            </td>
                        </tr> 
                    </table>
                </div>
                <br><br>
                <table id="tabla_compromisos_pago"></table>
                <div id="page_tabla_compromisos_pago"></div>
            </center>

            <div id="divSalidaEx" title="Exportacion" style=" display: block" >
                <p  id="msjEx" style=" display:  none"> Espere un momento por favor...</p>
                <center>
                    <img id="imgloadEx" style="position: relative;  top: 7px; display: none " src="./images/cargandoCM.gif"/>
                </center>
                <div id="respEx" style=" display: none"></div>
            </div> 
        <!--    <div id="dialogMsj" title="Mensaje" style="display:none; visibility: hidden;">
                <p style="font-size: 12px;text-align:justify;" id="msj" > Texto </p>
            </div>  -->

        </div>
    </body>
</html>
