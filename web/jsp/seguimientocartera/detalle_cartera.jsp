<%--
    Document   : Modulo de Seguimiento Cartera Fintra
    Created on : 19/05/2014, 11:00:00 AM
    Author     : hcuello
--%>

<%@page import="com.tsp.operation.model.beans.Usuario"%>
<%@page import="java.text.DecimalFormat"%>
<%@page import="java.text.DecimalFormatSymbols"%>
<%@ page import="com.tsp.operation.model.beans.SeguimientoCarteraBeans"%>
<%@ page import="com.tsp.operation.model.services.SeguimientoCarteraService"%>

<%@ page contentType="text/html; charset=UTF-8" language="java" %>
<%@ page session   ="true"%>
<%@ page errorPage ="/error/ErrorPage.jsp"%>
<%@ page import    ="java.util.*" %>
<%@ page import    ="java.text.SimpleDateFormat" %>
<%@ page import    ="com.tsp.util.*" %>
<%@ page import    ="javax.servlet.*" %>
<%@ page import    ="javax.servlet.http.*" %>
<%@ include file="/WEB-INF/InitModel.jsp"%>


<%
    String unidad_negocio = request.getParameter("unidad_negocio") != "" ? request.getParameter("unidad_negocio") : "";
    int periodo_foto = Integer.parseInt(request.getParameter("periodo_foto"));
    String Negocio = request.getParameter("negocio") != "" ? request.getParameter("negocio") : "";

    String item = "";
    Usuario usuario = (Usuario) session.getAttribute("Usuario");
    SeguimientoCarteraService rqservice = new SeguimientoCarteraService(usuario.getBd());
    ArrayList listaConsolidados = rqservice.SCarteraDetalleCuentaBeans("SQL_DETALLE_CLIENTE", unidad_negocio, periodo_foto, Negocio);

    DecimalFormatSymbols simbolo = new DecimalFormatSymbols();
    simbolo.setDecimalSeparator('.');
    simbolo.setGroupingSeparator(',');
    DecimalFormat formateador = new DecimalFormat("###,###.##", simbolo);


%>
 
<div style="width :auto ; max-height:700px;overflow: auto;" >
<table width="750" height="18" border='0' align='center' cellpadding="0" cellspacing="0" class="tablas" id="tbl_proceso">

    <tr>
        <td class="titulo_ventana" id="drag_detalle_cartera" colspan="13">
            <div style="float:left">DETALLE CARTERA<input type="hidden" name="id_usuario" id="id_usuario" value="coco"/></div> 
            <!-- <div style="float:right" onClick="$('#div_detalle_cartera').fadeOut('slow');"><a class="ui-widget-header ui-corner-all"><span class="ui-icon ui-icon-closethick"></span></a></div> -->
            <div style="float:right" onClick="$('#div_detalle_cartera').fadeOut('slow');"><a class="ui-widget-header ui-corner-all"><span>X</span></a></div>
        </td>
    </tr>    

    <tr><td colspan="13">&nbsp;</td></tr>

    <tr>
        <th width='15' align='center'>#</th>
        <th width='60' align='center'>NEGOCIO</th>
        <th width='60' align='center'>CEDULA</th>
        <th width='200' align='center'>NOMBRE CLIENTE</th>
        <th width='50' align='center'>CUOTA</th>
        <th width='60' align='center'>FECHA VENCIMIENTO</th>
        <th width='70' align='center'>DIAS VENCIDOS </th>
        <th width='70' align='center'>DIAS VENCIDOS FOTO</th>
        <th width='100' align='center'>SALDO FOTO</th>
        <th width='100' align='center'>SALDO ACTUAL</th>
        <th width='100' align='center'>INTERES X MORA</th>
        <th width='100' align='center'>GASTO DE COBRANZA</th>
        <th width='100' align='center'>TOTALES PARCIALES</th>

    </tr><%

        float SumaValorSaldo = 0;
        float SumaDebidoCobrar = 0;
        float interesXmora=0;
        float gastoCobranza=0;
        float totales=0;
        int CuentaRows = 1;

        if (listaConsolidados.size() > 0) {

            for (int i = 0; i < listaConsolidados.size(); i++) {

                    SeguimientoCarteraBeans ListadoContenido = (SeguimientoCarteraBeans) listaConsolidados.get(i);%>

     <tr ondblclick="verDetalles('<%=periodo_foto%>','<%=unidad_negocio%>','<%=Negocio%>','<%=ListadoContenido.getCuota()%>')">
        <td align="justify"><%=CuentaRows%></td>
        <td align="justify"><%=ListadoContenido.getNegocio()%></td>
        <td align="justify"><%=ListadoContenido.getCedula()%></td>
        <td align="justify"><%=ListadoContenido.getNombreCliente()%></td>
        <td align="justify"><%=ListadoContenido.getCuota()%></td>
        <td align="justify"><%=ListadoContenido.getFechaVencimiento()%></td>
        <td align="justify"><%=ListadoContenido.getDias_ven_hoy()%></td>
        <td align="justify"><%=ListadoContenido.getDiasVencidos()%></td>
        <td align="right">$<%=formateador.format(Float.valueOf(ListadoContenido.getValorAsignado()))%></td>
        <td align="right">$<%=formateador.format(Float.valueOf(ListadoContenido.getDebidoCobrar()))%></td>
        <td align="right">$<%=formateador.format(ListadoContenido.getInteres_mora())%></td>
        <td align="right">$<%=formateador.format(ListadoContenido.getGasto_cobranza())%></td>
        <td align="right">$<%=formateador.format(ListadoContenido.getGasto_cobranza()+ListadoContenido.getInteres_mora()+Float.valueOf(ListadoContenido.getDebidoCobrar()))%></td>

    </tr><%

            SumaValorSaldo += Float.valueOf(ListadoContenido.getValorAsignado());
            SumaDebidoCobrar += Float.valueOf(ListadoContenido.getDebidoCobrar());
            interesXmora+=ListadoContenido.getInteres_mora();
            gastoCobranza+=ListadoContenido.getGasto_cobranza();
            CuentaRows = CuentaRows + 1;
            totales +=ListadoContenido.getGasto_cobranza()+ListadoContenido.getInteres_mora()+Float.valueOf(ListadoContenido.getDebidoCobrar());
            
                        }%>

    <tr>
        <th align="right" colspan="8"><strong>TOTALES</strong></td>
        <th align="left"><strong>$<%=formateador.format(SumaValorSaldo)%></strong></td>
        <th align="left"><strong>$<%=formateador.format(SumaDebidoCobrar)%></strong></td>
         <th align="left"><strong>$<%=formateador.format(interesXmora)%></strong></td>
        <th align="left"><strong>$<%=formateador.format(gastoCobranza)%></strong></td>
        <th align="left"><strong>$<%=formateador.format(totales)%></strong></td>   
    </tr><%


        } else {%>

    <tr>
        <td colspan="13">SIN RESULTADOS</td>
    </tr><%            }%>
</tbody>
</table>

</div>
<script type="text/javascript">
                $("#tbl_proceso tr:even").addClass("even");
                $("#tbl_proceso tr:odd").addClass("odd");

                $(
                        function()
                        {
                            $("#tbl_proceso tr").hover(
                                    function()
                                    {
                                        $(this).addClass("highlight");
                                    },
                                    function()
                                    {
                                        $(this).removeClass("highlight");
                                    }
                            )
                        }
                )
                
               function verDetalles(periodo,unid_negocio,negocio,cuota){
             
             
                    $('#popup_det_cartera').fadeIn('slow');
                    $('.popup-overlay').fadeIn('slow');
                    $('.popup-overlay').height($(window).height());
                    
                   verDetalles1(periodo,unid_negocio,negocio,cuota);
                  
              
                  
              
               } 

                $(document).ready(function(){
                  
                  $('#close').click(function(){
                             $('#popup_det_cartera').fadeOut('slow');
                             $('.popup-overlay').fadeOut('slow');
                             return false;
                     });



                 });      
                 
</script>
