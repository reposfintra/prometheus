<%-- 
    Document   : recibos_caja
    Created on : 10/03/2016, 12:17:10 PM
    Author     : user
--%>

<%@page contentType="text/html" pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
      <head>
   
        <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
        <title>Recibos de Caja</title>
        <link type="text/css" rel="stylesheet" href="./css/jquery/jquery-ui/jquery-ui-2.css"/>
        <link type="text/css" rel="stylesheet" href="./css/recibos_caja.css"/>
        <link type="text/css" rel="stylesheet" href="./css/jquery/jquery.jqGrid/ui.jqgrid.css"/>        
        <script type="text/javascript" src="./js/jquery/jquery-ui/jquery.min.js"></script>
        <script type="text/javascript" src="./js/jquery/jquery-ui/jquery.ui.min.js"></script>            
        <script type="text/javascript" src="./js/jquery/jquery.jqGrid/js/grid.locale-es.js"></script>
        <script type="text/javascript" src="./js/jquery/jquery.jqGrid/js/jquery.jqGrid.min.js"></script>
        <link type="text/css" rel="stylesheet" href="./css/buttons/botonVerde.css"/>
        <link type="text/css" rel="stylesheet" href="./css/popup.css"/>
        <script type="text/javascript" src="./js/recibos_caja.js"></script>  
      
    </head>
    <body>
        <div id="capaSuperior" style="position:absolute; width:100%; height:100px; z-index:0; left: 0px; top: 0px;">
             <jsp:include page="/toptsp.jsp?encabezado=Recibos de Caja"/>
        </div>
        <div id="capaCentral" style="position:absolute; width:100%; height:100%; z-index:0; left: 0px; top: 100px; ">
         <center>
                <div id="div_recibo_caja" class="form_recibo_caja">
                    <div class="header_rc"><span class="titulo">REGISTRO DE RECIBOS</span> </div>
                    <div id="contenido">
                     <br>
                     <table border="0" class="tabla">
                            <tr>                        
                                <td class="td">
                                    <label for="consec">Consecutivo:<b id="req_consec" style="color:red">*</b></label>
                                    <input id="consec_ini" name="consec" size="9" type="text" maxlength="20" readonly/>
                                    <input id="consec_fin" name="consec" size="9" type="text" maxlength="20"/>
<!--                                    <img src="./images/botones/iconos/buscar.gif" onclick="cargarInfoRecibo(true)"/>
                                    <img src="./images/botones/iconos/clear2.png" width='20' height='20' onclick="clearInfoRecibo()"/>-->
                                </td>
                                <td class="td">
                                    <label for="fechaEntrega">Fecha Entrega:<b id="req_fec_entrega" style="color:red">*</b></label>
                                    <input type="text" id="fecha_entrega" name="fechaEntrega" readonly/>
                                </td>                                        
                            </tr>
                            <tr>
                                <td class="td">
                                    <label for="cobrador">Asesor:<b id="req_asesor" style="color:red">*</b></label>
                                    <select name="cobrador" id="asesor" class="combo_180px">
                                    </select>                                   
                                </td>    
                                <td class="td">  
                                    <label for="area_">Area:<b id="req_area" style="color:red">*</b></label>
                                    <select id="area" name="area_">
                                        <option value="">Seleccione</option>
                                        <option value="CARTERA">Cartera</option>
                                        <option value="COMERCIAL">Comercial</option>
                                        <option value="TESORERIA">Tesorer�a</option>
                                    </select>
                                </td>
                            </tr>                  
                         </table>
<!--                     <div id="div_info_recaudo" style="display:none;width:728px">
                        <table border="0" class="tabla">
                            <tr>
                                <td class="td">
                                    <label for="idCliente">Identificacion:<b id="req_ident" style="color:red">*</b></label>
                                    <input id="id_cliente" name="idCliente" type="text" class="solo-numero" maxlength="12" style="margin-right:33px"/>                                   
                                </td>
                                <td class="td">
                                    <label for="fecha_recib">&nbspFecha Recibido:<b id="req_fec_recib" style="color:red">*</b></label>
                                    <input type="text" id="fecha_recibido" name="fecha_recib" readonly/>
                                </td>   
                            </tr>
                            <tr>
                                <td class="td" colspan="3">
                                    <label for="nomcli">Nombre<b id="req_nombre" style="color:red">*</b></label>
                                    <input id="nombre" name="nomcli" type="text" style="width:555px" maxlength="160"/>   
                                </td>                     
                            </tr>
                            <tr>
                                <td class="td">
                                    <label for="tipoRecaudo">Tipo Recaudo:<b id="req_tipo_recaudo" style="color:red">*</b></label>
                                    <select id="tipo_recaudo" name="tipoRecaudo" />
                                </td>      
                                <td class="td">
                                    <label for="valor">&nbsp&nbsp&nbspValor:<b id="req_vlr_recaudo" style="color:red">*</b></label>
                                    <input id="valor_recaudo" name="valor" size="10" type="text" class="solo-numero"/>                                   
                                </td>
                            </tr>
                        </table>
                    </div>-->
                        <br>
                        <hr>
                        <br>
                        <div style="text-align: right;margin-right: 28px">
                            <button id="guardar" class="ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only" role="button" aria-disabled="false">
                                <span class="ui-button-text">Guardar</span>
                            </button>  
                            <button id="salir" class="ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only" role="button" aria-disabled="false">
                                <span class="ui-button-text">Salir</span>
                            </button>
                        </div>
                    </div>
                    <br>
                  </div>
            </center>
            <div id="dialogLoading" style="display:none;">
                <p  style="font-size: 12px;text-align:justify;" id="msj2">Texto </p> <br/>
                <center>
                    <img src="./images/cargandoCM.gif"/>
                </center>
            </div>
            <div id="dialogMsj" title="Mensaje" style="display:none;">
                <p style="font-size: 12px;text-align:justify;" id="msj" > Texto </p>
            </div>  
        </div>
    </body>
</html>
