<%-- 
    Document   : reporteCostos
    Created on : 1/09/2016, 12:57:01 PM
    Author     : dvalencia
--%>

<%@page contentType="text/html" pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
        <title>REPORTE DE COSTOS</title>
        <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
        <link href="./css/popup.css" rel="stylesheet" type="text/css">
        <link type="text/css" rel="stylesheet" href="./css/jquery/jquery-ui/jquery-ui.css" />
        <link type="text/css" rel="stylesheet" href="./css/TransportadorasApi.css" />
        <script type="text/javascript" src="./js/jquery/jquery.jqGrid/js/jquery.min.js"></script>
        <script type="text/javascript" src="./js/jquery-ui-1.8.5.custom.min.js"></script>   
        <script type="text/javascript" src="./js/jquery/jquery.jqGrid-4.7.0/js/grid.locale-es.js"></script>
        <script type="text/javascript" src="./js/jquery/jquery.jqGrid-4.7.0/js/jquery.jqGrid.min.js"></script>
        <script type="text/javascript" src="./js/jquery/jquery.jqGrid-4.7.0/js/jquery.tabletojson.js"></script>   
        <script type="text/javascript" src="./js/reporteCostos.js"></script> 
    </head>
    <body>
       <div id="capaSuperior" style="position:absolute; width:100%; height:100px; z-index:0; left: 0px; top: 0px;">
            <jsp:include page="/WEB-INF/toptsp.jsp?encabezado=REPORTE DE COSTOS"/>
        </div>
        <style>
            .onoffswitch-inner:before {
                content: "A";
                padding-left: 14px;
                background-color: #34A7C1; color: #FFFFFF;
            }
            .onoffswitch-inner:after {
                content: "I";
                padding-right: 11px;
                background-color: #C20F0F; color: #F5F0F0;
                text-align: right;
            }
        </style>
        <div id="capaCentral" style="position:absolute; width:100%; height:100%; z-index:0; left: 0px; top: 120px; ">
        <center>
        <%--<div id="tablita" style="top: 170px;width: 1362px;height: 86px;" >
            <div id="encabezadotablita" style="width: 1349px">
                <label class="titulotablita"><b>FILTRO DE BUSQUEDA</b></label>
            </div> 
            <table id="tablainterna"style="height: 53px;"  >
                <tr>
                    <td>
                        <label>Multi-servicio</label>
                    </td>
                    <td>
                        <input type="text" id="multiservicio"  style="width: 100px" ><!--hidden ="false"-->
                    </td>
                    <td>
                        <label>Nombre cliente</label>
                    </td>
                    <td>
                        <input type="text" id="cliente"  style="width: 100px" ><!--hidden ="false"-->
                    </td>
                    <td>
                        <div id ='botones'>
                            <button id="limpiar" class="ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only" 
                                    role="button" aria-disabled="false" style="margin-top: 6px;margin-left: 6px;width: 107px;">
                                <span class="ui-button-text">Limpiar fechas</span>
                            </button> 
                        </div>
                    </td>
                    <td>
                        <div id ='botones'>
                            <button id="buscar" class="ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only" 
                                    role="button" aria-disabled="false" style="margin-top: 6px;margin-left: 6px;">
                                <span class="ui-button-text">Buscar</span>
                            </button> 
                        </div>
                    </td>
                </tr>
            </table>
        </div>--%>
            <div style="position: relative;top: 10px;">
            <table id="tabla_Costos" ></table>
            <div id="pager"></div>
            </div>
        

        <div id="dialogDetalleCostos"  class="ventana" > 
              <%--<table id="tabla_Resumen" ></table>
             <div id="pagerResumen"></div>--%>
              <fieldset  style="width:1630px; height:50px;" >
		<legend>
                    <b>  RESUMEN DE COSTOS </b>
                </legend>
		<table id="tabla_Resumen" style="width:1630px;">
			<tbody>
				<tr>
					<td> Multiservicio:</td>
					<td><input type="text" name="multiservicio" readonly=true id="multiservicio"  style="width:110px"/></td>

                                        <td> Cliente:</td>
					<td><input type="text" name="nombre_cliente" readonly=true id="nombre_cliente"  style="width:200px"/></td>
                                        
                                        <td> Proyecto:</td>
					<td><input type="text" name="nombre_proyecto" readonly=true id="nombre_proyecto" onchange="conMayusculas(this)" style="width:350px"/></td>
                                        
					<td> Costo contratista:</td>				
					<td><input type="text" name="valor_contratista_antes_iva" readonly=true id="valor_contratista_antes_iva"  style="width:110px;text-align:right"  /></td>

					<td> Utilizado:</td>				
					<td><input type="text" name="utilizado" readonly=true id="utilizado"  style="width:110px;text-align:right"/></td>
                                        
                                        <td> Rentabilidad</td>				
					<td><input type="text" name="rentabilidad" readonly=true id="rentabilidad"  style="width:100px;text-align:right"/></td>
				</tr>
                        <tbody>
		</table>
              </fieldset>
              <br>
            <table id="tabla_Detalle_Costos" ></table>
             <div id="pagerDetalles"></div>
        </div>
        <div id="divSalidaEx" title="Exportacion" style=" display: block" >
            <p  id="msjEx" style=" display:  none"> Espere un momento por favor...</p>
            <center>
                <img id="imgloadEx" style="position: relative;  top: 7px; display: none " src="./images/cargandoCM.gif"/>
            </center>
            <div id="respEx" style=" display: none"></div>
        </div> 
        </center>
        </div>
    <script>
        reporteCostos();
    </script>
    </body>
</html>
