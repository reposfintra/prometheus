<%-- 
    Document   : generarArchivoAsobancaria
    Created on : 24/09/2019, 09:12:43 AM
    Author     : mcamargo
--%>

<%@page contentType="text/html" pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
        <link href="./css/popup.css" rel="stylesheet" type="text/css">
        <link type="text/css" rel="stylesheet" href="./css/jquery/jquery-ui/jquery-ui.css" />
        <script type="text/javascript" src="./js/jquery/jquery.jqGrid/js/jquery.min.js"></script>
        <script type="text/javascript" src="./js/jquery-ui-1.8.5.custom.min.js"></script>   
        <script type="text/javascript" src="./js/jquery/jquery.jqGrid-4.7.0/js/grid.locale-es.js"></script>
        <script type="text/javascript" src="./js/jquery/jquery.jqGrid-4.7.0/js/jquery.jqGrid.min.js"></script>
        <script type="text/javascript" src="./js/jquery/jquery.jqGrid-4.7.0/js/jquery.tabletojson.js"></script> 
        <script src="./js/generarArchivoAsobancaria.js" type="text/javascript"></script>
        <title>Generar Archivo Asobancaria</title>
        <style>
            .filtro{
                width: 560px;
                margin: 10px auto; 

            }

            .header_filtro{
                text-align: center;   
                border: 1px solid #234684;
                background: #2A88C8;
                color: #ffffff;
                font-weight: bold;
                border-radius: 8px 8px 0px 0px;
            }

            .body_filtro{                
                background: white; 
                border:1px solid #2A88C8;
                height: 80px;
                border-radius: 0px 0px 8px 8px;
            }

            .contenido{
                height: 83px; 
                width: auto;
                margin-top: 5px;
            }

            label{
                display: block;
                text-align: center;
                font-family: 'Lucida Grande','Lucida Sans',Arial,sans-serif;
                font-size: 15px;
            }

            .caja{
                float:left;
                margin-left:10px;
                margin-right:10px;
                margin-top: 8px;
            }
            input[type=text],select{
                padding: 5px 5px;
                margin: 8px auto;
                border: 1px solid #ccc;
                border-radius: 4px;
                font-size: 15px;
                color: black;
            }
            .select,.select_fecha {
                font-size: 12px;
                background: #2A88C8;
                color: white;
                font-weight: bold;
            }
            .button {
                padding: 8px 8px;
                font-size: 12px;
                cursor: pointer;
                text-align: center;
                color: #fff;
                background-color: #2a88c8;
                border: none;
                border-radius:8px;
                margin: 22px auto;
                font-weight: bold;
                width: 80px;
            }
            
        </style>
    </head>
    <body>
        <div>
            <jsp:include page="/WEB-INF/toptsp.jsp?encabezado=Generar Archivo Asobancaria"/>
        </div>

        <div class="filtro">
            <div class="header_filtro">
                <label>FILTRO DE BUSQUEDA</label>
            </div>
            <div class="body_filtro">
                <div class="caja">
                       <label> Entidad recaudo:</label>
                       <select id="id_entidad_recaudo" name="id_entidad_recaudo" class="select">
                       </select>
                </div> 
                <div class="caja">
                    <label> Fecha recaudo:</label>
                  <input type="date" id="select_fecha" style="height: 25px;width: 160px;color: #070708;font-weight:  bold;font-size: 14px; margin: 8px auto;">
                </div>
                <div class="caja">
                    <button class="button" onclick="cargarRecaudo();">Buscar</button>
                </div>
            </div>

        </div>
    <center>
        <div  style="margin-top: 20px; " >
            <table id="tabla_recaudo" ></table>
            <div id="pager"></div>
        </div>
        
        <div id="div_detalle_recaudo"  style="display:none; margin-top: 20px; ">
            <input type="text" id="id_archivo" name="id_archivo" readonly="true" style="display:none;">               
            <table id="tabla_detalle_recaudo"></table> <br><br>
            <button style="font-weight: bold; font-size: 14px;" id="generarAsobancaria" class="ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only" onclick="generarArchivoAsobancaria();"> 
            <span class="ui-button-text">Generar Archivo Asobancaria</span>
            </button>  
        </div>
        
        <div id="dialogMsj" title="Mensaje" style="display:none; .hide-close .ui-dialog-titlebar-close { display: none }">
            <p style="font-size: 12px;text-align:justify;" id="msj" > Texto </p>
        </div>  
        <div id="info"  class="ventana" >
            <p id="notific"></p>
        </div>

        <div id="dialogLoading" style="display:none;">
            <p  style="font-size: 12px;text-align:justify;" id="msj2">Texto </p> <br/>
            <center>
                <img src="./images/cargandoCM.gif"/>
            </center>
        </div>

    </center>
    </body>
</html>
