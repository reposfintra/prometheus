<!--  
	 - Author(s)       :      LREALES
	 - Description     :      AYUDA FUNCIONAL - Reporte Factura Destinatario
	 - Date            :      08/04/2005  
	 - Copyright Notice:      TRANSPORTES SANCHEZ POLO S.A.
-->
<%@ include file="/WEB-INF/InitModel.jsp"%>
<HTML>
<HEAD>
<TITLE></TITLE>
<META http-equiv=Content-Type content="text/html; charset=windows-1252">
<link href="/../../../css/estilostsp.css" rel="stylesheet" type="text/css"> 
<link href="<%=BASEURL%>/css/estilostsp.css" rel="stylesheet" type="text/css">
<script type='text/javascript' src="<%= BASEURL %>/js/boton.js"></script> 
</HEAD>
<BODY> 
<% String BASEIMG = BASEURL +"/images/ayuda/trafico/convertir_planilla/"; %>
  <table width="95%"  border="2" align="center">
    <tr>
      <td height="117" >
        <table width="100%" border="0" align="center">
          <tr  class="subtitulo">
            <td height="20"><div align="center">MANUAL DE TRAFICO WEB</div></td>
          </tr>
          <tr class="subtitulo1">
            <td>Descripci&oacute;n del funcionamiento del programa Convertir Planilla Manual a Planilla Original</td>
          </tr>
          <tr>
            <td  class="ayudaHtmlTexto"><div align="center"></div></td>
          </tr>
<tr>
            <td  class="ayudaHtmlTexto"><p class="ayudaHtmlTexto">En la siguiente pantalla  se digitan los n&uacute;meros que corespondan a dichas opciones.</p>            </td>
          </tr>
<tr>
            <td  class="ayudaHtmlTexto"><div align="center"><IMG height=187 src="<%=BASEIMG%>image001.JPG" width=722 border=0 v:shapes="_x0000_i1143"></div></td>
          </tr>
<tr>
  <td  class="ayudaHtmlTexto"><p>&nbsp;</p>
    <p>El sistema verifica que no hayan campos vacios.</p></td>
</tr>
<tr>
  <td  class="ayudaHtmlTexto"><div align="center"><IMG height=123 src="<%=BASEIMG%>image_error001.JPG" width=299 border=0 v:shapes="_x0000_i1052"></div></td>
</tr>
<tr>
  <td  class="ayudaHtmlTexto"><p>&nbsp;</p>
    <p>El sistema verifica que la planilla manual ingresada est&eacute; correcta y s&iacute; exista en el sistema, de lo contrario nos muestra el siguiente mensaje, y se debe corregir y volver a presionar ACEPTAR.</p></td>
</tr>
<tr>
  <td  class="ayudaHtmlTexto"><div align="center"><IMG height=241 src="<%=BASEIMG%>image_error003.JPG" width=467 border=0 v:shapes="_x0000_i1052"></div></td>
</tr>
<tr>
            <td  class="ayudaHtmlTexto"><p class="ayudaHtmlTexto">&nbsp;</p>
            <p class="ayudaHtmlTexto">Si desea estar m&aacute;s seguro, de las planillas manuales que existen en el sistema, puede seleccionar la Lupa de Buscar y  en una nueva pantalla aparecer&aacute;n los datos que coinciden con la b&uacute;squeda realizada.</p></td>
          </tr>
<tr>
            <td  class="ayudaHtmlTexto"><div align="center"><IMG height=156 src="<%=BASEIMG%>image002.JPG" width=653 border=0 v:shapes="_x0000_i1052"></div></td>
          </tr>
<tr>
  <td  class="ayudaHtmlTexto"><p>&nbsp;</p>
    <p>Si no existen planillas manuales, en la pantalla le saldr&aacute; lo siguiente. </p></td>
</tr>
<tr>
  <td  class="ayudaHtmlTexto"><div align="center"><IMG height=107 src="<%=BASEIMG%>image_error002.JPG" width=288 border=0 v:shapes="_x0000_i1052"></div></td>
</tr>
<tr>
            <td  class="ayudaHtmlTexto"><p class="ayudaHtmlTexto">&nbsp;</p>
            <p class="ayudaHtmlTexto">Al escoger con un click la planilla manual deseada para convertir, puede proceder a ingresar el n&uacute;mero de la planilla original; Pero si el n&uacute;mero de esa planilla original ingresada ya existe en el sistema le saldr&aacute; la siguiente pantalla.</p></td>
          </tr>
<tr>
  <td  class="ayudaHtmlTexto"><div align="center"><IMG height=254 src="<%=BASEIMG%>image_error004.JPG" width=467 border=0 v:shapes="_x0000_i1054"></div></td>
</tr>
<tr>
  <td  class="ayudaHtmlTexto"><p>&nbsp;</p>
    <p>Finalmente si todos los datos estan ingresados correctamente, el proceso se realizar&aacute; satisfactoriamente, y le saldr&aacute; la siguinete pantalla. </p></td>
</tr>
<tr>
  <td  class="ayudaHtmlTexto"><div align="center"><IMG height=242 src="<%=BASEIMG%>image003.JPG" width=466 border=0 v:shapes="_x0000_i1054"></div></td>
</tr>
      </table></td>
    </tr>
  </table>
  <p></p>
<center>
<img src = "<%=BASEURL%>/images/botones/salir.gif" style = "cursor:hand" name = "imgsalir" onClick = "parent.close();" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);">
</center>
</BODY>
</HTML>
