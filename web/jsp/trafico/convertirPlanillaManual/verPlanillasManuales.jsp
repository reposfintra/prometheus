<%@page contentType="text/html"%>
<%@page session="true"%>
<%@page errorPage="../error/ErrorPage.jsp"%>
<%@page import="java.util.*"%>
<%@page import="com.tsp.operation.model.*"%>
<%@page import="com.tsp.util.*"%>
<%@page import="com.tsp.operation.model.beans.*, java.util.*"%>
<%@include file="/WEB-INF/InitModel.jsp"%>
<%@taglib uri="http://jsptags.com/tags/navigation/pager" prefix="pg" %>

<html>
<head>
<title>Ver Planillas Manuales</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link href="<%= BASEURL %>/css/estilostsp.css" rel="stylesheet" type="text/css"> 
<link href="../../../css/estilostsp.css" rel="stylesheet" type="text/css"> 
<script type='text/javascript' src="<%= BASEURL %>/js/reporte.js"></script> 
<script type='text/javascript' src="<%= BASEURL %>/js/boton.js"></script>
</head>
<body onresize="redimensionar()" onload = 'redimensionar()'>
<div id="capaSuperior" style="position:absolute; width:100%; height:100px; z-index:0; left: 0px; top: 0px;">
	<jsp:include page="/toptsp.jsp?encabezado=Planillas Manuales"/>
</div>
<div id="capaCentral" style="position:absolute; width:100%; height:87%; z-index:0; left: 0px; top: 100px; overflow: scroll;">
	  <% Vector planillas = model.convertirPlaManService.getVectorPlanilla();
	  
	  if( planillas != null  && planillas.size() > 0 ){ %>
 <table width="95%" border="2" align=center>
    <tr>
        <td>
            <table width="100%" align="center" class="tablaInferior"> 
                <tr>
                    <td colspan='2'>                                                
                        <table width="100%"  border="0" cellpadding="0" cellspacing="0" class="barratitulo">
                            <tr>
                                <td width="50%" class="subtitulo1" colspan='3'>Informaci&oacute;n</td>
                                <td width="50%" class="barratitulo" colspan='2'><img src="<%=BASEURL%>/images/titulo.gif"></td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td>
					<div class="scroll" style=" overflow:auto ; WIDTH: 100%; HEIGHT:100%; " >
                        <table width="100%" border="1" align="center" bordercolor="#999999" bgcolor="#F7F5F4">
                            <tr class="tblTitulo">
                                <td nowrap width="6%"><div align="center">Planilla</div></td>
                                <td nowrap width="6%"><div align="center">Placa</div></td>
                                <td nowrap width="30%"><div align="center">Conductor</div></td>
                                <td nowrap width="24%"><div align="center">Origen</div></td>
                                <td nowrap width="24%"><div align="center">Destino</div></td>
                                <td nowrap width="10%"><div align="center">Creation User</div></td>
                            </tr>
							  <%
								for( int i = 0; i < planillas.size(); i++ ){
									Ingreso_Trafico planilla = ( Ingreso_Trafico ) planillas.elementAt( i );
							  %>
							<tr class="<%=(i % 2 == 0 )?"filagris":"filaazul"%>" onMouseOver='cambiarColorMouse(this)' style="cursor:hand" onClick="window.opener.document.forma.numplaman.value='<%=planilla.getPlanilla()%>'; window.close();">
							<td  nowrap  align="center" abbr="" class="bordereporte"><%=planilla.getPlanilla ()%></td>
							<td  align="center" abbr="" nowrap class="bordereporte"><%=planilla.getPlaca ()%></td>
							<td  align="center" abbr="" nowrap class="bordereporte"><div align="left"><%=planilla.getNomcond ()%></div></td>
							<td  abbr="" nowrap class="bordereporte"><div align="left"><%=planilla.getNomorigen ()%></div></td>
							<td  align="center" abbr="" nowrap class="bordereporte"><div align="left"><%=planilla.getNomdestino ()%></div></td>
							<td align="center" nowrap class="bordereporte"  abbr=""><div align="left"><%=planilla.getCreation_user ()%></div></td>
							</tr>
						  
								<%}%>
                        </table>
					  </div>
                    </td>
                </tr>
            </table>
        </td>
    </tr>
 </table>
 <br>
	<%}
	 else { %>                    
	  <%out.print("<table width=379 border=2 align=center><tr><td><table width=100%  border=1 align=center  bordercolor=#F7F5F4 bgcolor=#FFFFFF><tr><td width=350 align=center class=mensajes>No Existen Planillas Manuales!</td><td width=100><img src="+BASEURL+"/images/cuadronaranja.JPG></td></tr></table></td></tr></table>");%>
	  <br>
	<%}%>

 <table width='95%' align=center>
  <tr class="titulo">
    <td align=right colspan='2'>
        <div align="center">
		<img src="<%=BASEURL%>/images/botones/salir.gif"  name="imgsalir"  onMouseOver="botonOver(this);" onClick="parent.close();" onMouseOut="botonOut(this);" style="cursor:hand">
        </div></td>
  </tr>
</table>
</div>
</body>
</html>