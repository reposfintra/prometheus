<!--  
	 - Author(s)       :      LREALES
	 - Description     :      AYUDA DESCRICTIVA - Convertir Planilla Manual a Planilla Original
	 - Date            :      08/04/2005  
	 - Copyright Notice:      TRANSPORTES SANCHEZ POLO S.A.
-->
<%@page session="true"%> 
<%@page errorPage="/error/ErrorPage.jsp"%>
<%@page import="com.tsp.operation.model.beans.*, java.util.*"%>
<%@ include file="/WEB-INF/InitModel.jsp"%>
<%@ taglib uri="http://jakarta.apache.org/taglibs/input-0.90" prefix="input" %>
<html>
<head>
    <title>Descripcion de los campos a ingresar en Convertir Planilla Manual a Planilla Original</title>
    <link href="<%= BASEURL %>/css/estilostsp.css" rel="stylesheet" type="text/css"> 
    <link href="/../../../css/estilostsp.css" rel="stylesheet" type="text/css">  
</head>
<script type='text/javascript' src="<%= BASEURL %>/js/boton.js"></script> 
<body> 
<div id="capaSuperior" style="position:absolute; width:100%; height:100px; z-index:0; left: 0px; top: 0px;">
<jsp:include page="/WEB-INF/toptsp.jsp?encabezado=Ayuda - Convertir Planilla Manual a Planilla Original"/>
</div>
<div id="capaCentral" style="position:absolute; width:100%; height:90%; z-index:0; left: 0px; top: 100px;"> 
   <p>&nbsp;</p>
   <table width="65%" border="2" align="center" id="tabla1" >
        <td>
         <table width="100%" height="50%" align="center">
              <tr>
                <td class="subtitulo1"><strong>Trafico </strong></td>
                <td width="50%" class="barratitulo"><img src="<%=BASEURL%>/images/titulo.gif" width="32" height="20"></td>
              </tr>
         </table>
         <table width="100%" borderColor="#999999" bgcolor="#F7F5F4">
           <tr>
             <td colspan="2" class="tblTitulo"><strong>Convertir Planilla Manual a Planilla Original - Pantalla Preliminar</strong></td>
           </tr>
           <tr>
             <td colspan="2" class="bordereporte"><strong>INFORMACION</strong></td>
           </tr>
           <tr>
             <td width="40%"><span class="fila"><strong>N&uacute;mero Planilla Manual </strong></span></td>
             <td width="60%"><p class="ayudaHtmlTexto">Campo para ingresar el c&oacute;digo de la planilla manual a convertir. Este campo es de m&aacute;ximo 10 caracteres.</p>
             </td>
           </tr>
           <tr>
             <td><span class="fila"><strong>Boton Buscar</strong></span></td>
             <td><span class="ayudaHtmlTexto">Boton para ver cuales son las planillas manuales que existen. </span></td>
           </tr>
           <tr>
             <td><span class="fila"><strong>N&uacute;mero Planilla Original </strong></span></td>
             <td><span class="ayudaHtmlTexto">Campo para ingresar el c&oacute;digo de la planilla original; el cual es de m&aacute;ximo 10 caracteres.</span></td>
           </tr>
           <tr>
             <td><span class="fila"><strong>Boton Aceptar </strong></span></td>
             <td><span class="ayudaHtmlTexto">Boton para realizar el procedimiento de convertir la planilla manual  a la planilla original ingresada. </span></td>
           </tr>
           <tr>
             <td><span class="fila"><strong>Boton Cancelar</strong></span></td>
             <td><span class="ayudaHtmlTexto">Boton que borra toda la informaci&oacute;n que se encuentre en los campos. </span></td>
           </tr>
           <tr>
             <td><span class="fila"><strong>Boton Salir</strong></span></td>
             <td><span class="ayudaHtmlTexto">Boton para salir de la vista 'Convertir Planilla Manual a Planilla Original' y volver a la vista del men&uacute;.</span></td>
           </tr>
         </table></td>
  </table>
		<p></p>
	<center>
	<img src = "<%=BASEURL%>/images/botones/salir.gif" style = "cursor:hand" name = "imgsalir" onClick = "parent.close();" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);">
	</center>
</div>  
</body>
</html>