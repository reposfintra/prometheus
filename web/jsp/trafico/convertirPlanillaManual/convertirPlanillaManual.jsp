<%@ page session="true"%>
<%@ page errorPage="../error/ErrorPage.jsp"%>
<%@page import="com.tsp.operation.model.beans.*, java.util.*"%>
<%@ include file="/WEB-INF/InitModel.jsp"%>
<%
  String path = application.getRealPath(model.menuService.getRealUrl(request.getRequestURI()));
  String datos[] = model.menuService.getContenidoMenu(BASEURL,request.getRequestURI(),path);
%>
<html>
<head>
<title>Convertir Planilla Manual - Control De Tr�fico - TSP</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link href="../../../../../../../../Documents%20and%20Settings/ALVARO/Mis%20documentos/css/estilostsp.css" rel="stylesheet" type="text/css">
<link href="<%=BASEURL%>/css/estilostsp.css" rel="stylesheet" type="text/css">
  <script type='text/javascript' src="<%=BASEURL%>/js/boton.js"></script> 
  <script type='text/javascript' src="<%=BASEURL%>/js/validar.js"></script> 
  <script>
	var controlador ="<%=CONTROLLER%>";
	
	function ConvertirPlanilla( numplaman, numplaori ){
	
		if( numplaman != "" && numplaori != "" ){
		
			document.forma.action = controlador+"?estado=ConvertirPlanillaManual&accion=Search";
			document.forma.submit();
			
		} else{
		
			alert( "Los Campos Estan Vacios! Por Favor Verificar!!" );
			
		}
	}
	
	function Buscar_Info_Facil () {
		if ( window.event.keyCode == 13 ){ 
	
			if( document.forma.numplaman.value != "" && document.forma.numplaori.value != ""  ) {
			
				document.forma.action = controlador+"?estado=ConvertirPlanillaManual&accion=Search";				
				document.forma.submit();
				
			} else {
			
				alert( "Los Campos Estan Vacios! Por Favor Verificar!!" );
				
			}
			
		}
	}
  </script>
</head>
<body>
<div id="capaSuperior" style="position:absolute; width:100%; height:100px; z-index:0; left: 0px; top: 0px;">
  <jsp:include page="/WEB-INF/toptsp.jsp?encabezado=Convertir Planilla Manual a Planilla Original"/>
</div>
<div id="capaCentral" style="position:absolute; width:100%; height:85%; z-index:0; left: 0px; top: 100px; overflow: scroll;"> 
  

  <%
  Usuario usuario = ( Usuario ) session.getAttribute( "Usuario" );
  String Mensaje = ( request.getParameter( "Mensaje" ) != null )?request.getParameter( "Mensaje" ):"";%>
  <br>
  <form name="forma" method="post" action="<%=CONTROLLER%>?estado=ConvertirPlanillaManual&accion=Search">
   
   <table width="50%"  border="2" align="center">
    <tr>
      <td>

    <table width="100%"  border="0" align="center" cellpadding="0" cellspacing="0" class="barratitulo">
      <tr>
        <td width="50%" class="subtitulo1" colspan='3'>Informaci&oacute;n De Planilla</td>
        <td width="50%" class="barratitulo" colspan='2'><img src="<%=BASEURL%>/images/titulo.gif" align="left"><%=datos[0]%></td>
      </tr>
    </table>
    <table width="100%" align="center">
      <tr>
        <td><table width="100%" border="0" cellpadding="0" cellspacing="0">
            <tr class="fila">
              <td width="50%">N&uacute;mero Planilla Manual</td>
              <td width="50%"><input name="numplaman" type="text" class="textbox" id="numplaman" size="10" maxlength="10" onKeyUp="Buscar_Info_Facil()">
                <img src="<%=BASEURL%>/images/botones/iconos/obligatorio.gif" width="10" height="10"> 
				<img src="<%=BASEURL%>/images/botones/iconos/lupa.gif" alt="Buscar # de Planilla Manual.." name="imglupa" width="20" height="20" style="cursor:hand" onClick="window.open('<%=CONTROLLER%>?estado=VerPlanillas&accion=Manuales','','status=no,scrollbars=no,width=700,height=470,resizable=yes');" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);">
				</td>
            </tr>
            <tr class="fila">
              <td width="50%">N&uacute;mero Planilla Original </td>
              <td width="50%"><input name="numplaori" type="text" class="textbox" id="numplaori" size="10" maxlength="10" onKeyUp="Buscar_Info_Facil()">
			  <img src="<%=BASEURL%>/images/botones/iconos/obligatorio.gif" width="10" height="10"></td>
            </tr>
        </table></td>
      </tr>
    </table>	</td>
      </tr>
    </table>
	<div align="center">
	  <p>		
	    <!-- <input type="image" src="< %=BASEURL%>/images/botones/aceptar.gif" name="imgaceptar" onClick="ConvertirPlanilla(numplaman.value, numplaori.value);" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" style="cursor:hand">&nbsp; -->
		<img src="<%=BASEURL%>/images/botones/aceptar.gif" name="imgaceptar" onClick="ConvertirPlanilla(numplaman.value, numplaori.value);" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" style="cursor:hand">&nbsp;
		<img src="<%=BASEURL%>/images/botones/cancelar.gif" name="imgcancelar" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" onClick='forma.reset();' style="cursor:hand">&nbsp;
        <img src="<%=BASEURL%>/images/botones/salir.gif" name="imgsalir"  onMouseOver="botonOver(this);" onClick="parent.close();" onMouseOut="botonOut(this);" style="cursor:hand">
	  </p>
	  <p>&nbsp;</p>
	  <p><%if( (!Mensaje.equals("")) ){%></p>
	  <p><table border="2" align="center">
	  <tr>
		<td><table width="100%"  border="1" align="center"  bordercolor="#F7F5F4" bgcolor="#FFFFFF">
		  <tr>
			<td width="200" align="center" class="mensajes"><%=Mensaje%></td>
			<td width="29" background="<%=BASEURL%>/images/cuadronaranja.JPG">&nbsp;</td>
			<td width="58">&nbsp;</td>
		  </tr>
		</table></td>
	  </tr>
	</table>
	</p>
<%}%>
</div>      
	  </form> 
</div>
<%=datos[1]%>
</body>
</html>