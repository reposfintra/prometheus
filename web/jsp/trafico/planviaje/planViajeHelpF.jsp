  <!--
     - Author(s)       :      FERNEL VILLACOB DIAZ
     - Date            :      07/10/2005  
     - Copyright Notice:      TRANSPORTES SANCHEZ POLO S.A.
  -->
 <%--   - @(#)  
     - Description:  Vista que maneja plan de viajes
 --%>
 <%@ include file="/WEB-INF/InitModel.jsp"%>
<html>
<head>
        <title>Ayuda Plan de Viaje</title>
        <META http-equiv=Content-Type content="text/html; charset=windows-1252">
        <link href="<%=BASEURL%>/css/estilostsp.css" rel="stylesheet" type="text/css"> 
        <script type='text/javascript' src="<%= BASEURL %>/js/boton.js"></script>  
</head>
<body>
<center>    
 <% String BASEIMG = BASEURL +"/images/ayuda/trafico/planviaje/"; %>
 
 
 
<table width="100%"  border="2" align="center">
    <tr>
      <td >
 
            <table width='99%' align="center" cellpadding='0' cellspacing='0'>
                 <tr class="subtitulo" ><td height="20"><div align="center">MANUAL DE PLAN DE VIAJE</div></td></tr>
                 <tr class="subtitulo1"> <td>Descripci&oacute;n del funcionamiento </td> </tr>
        
                 
                 <tr>
                      <td  class="ayudaHtmlTexto" height='40'>
                         El programa permitir� generar plan de viaje a la planilla. Para esto, deber� especificar el distrito, el n�mero de la
                         planilla y la opci�n que desee para el plan de viaje. Se presenta un formulario como lo indica la figura 1.
                      </td>
                 </tr>
                 
                 
                 <tr>
                    <td  align="center" > 
                         <br><br>
                         <img  src="<%= BASEIMG%>Formulario.JPG" >                  
                         <br>
                         <strong>Figura 1</strong>
                         <br><br>
                         <br><br>
                     </td>
                </tr>
                
                 <tr>
                      <td  class="ayudaHtmlTexto" height='40'>
                         Las opciones que puede seleccionar asociadas al plan de viaje son:
                         
                         <ul>
                             <li> Crear: Cuando la planilla no presenta plan de viaje                                                      </li>
                             <li> Consultar: Cuando la planilla ya tiene asociado un plan de viaje y desea consultarlo                         </li>
                             <li> Modificar: Cuando la planilla ya tiene asociado un plan de viaje y desea modificar datos del plan de viaje   </li>
                             <li> Eliminar: Cuando la planilla ya tiene asociado un plan de viaje y desea eliminarlo                          </li>
                          </ul>  
                         
                      </td>
                 </tr>
                
                 <tr>
                      <td  class="ayudaHtmlTexto" height='40'>
                        Si la planilla ya est� cumplida no podr� crear su plan de viaje. Para la creaci�n del plan de viaje deber� existir la planilla
                        en el distrito seleccionado.
                        Si la planilla no tiene plan de viaje, en el formulario de creaci�n de plan de viaje, siempre la aparecer� unicamente habilitado el
                        bot�n  "Crear" independientemente de la opci�n seleccionada, de lo contrario, se habilitar� la opci�n seleccionada.
                        <br><br><br><br>
                      </td>
                 </tr>
                 
 
            </table>
            
      </td>
  </tr>
</table>


<P>
  <img src="<%=BASEURL%>/images/botones/salir.gif"      name="exit"    height="21"  title='Salir'                onClick="parent.close();"                 onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" style="cursor:hand">          
</P>
 
 

</body>
</html>
