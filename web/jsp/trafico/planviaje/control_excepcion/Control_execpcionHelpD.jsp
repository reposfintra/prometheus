<!--  
	 - Author(s)       :      FFERNADEZ	
	 - Description     :      AYUDA DESCRIPTIVA - Adicionar Cliente
	 - Date            :      28/10/2006  
	 - Copyright Notice:      TRANSPORTES SANCHEZ POLO S.A.
-->
<%@page session="true"%> 
<%@page errorPage="/error/ErrorPage.jsp"%>
<%@page import="com.tsp.operation.model.beans.*, java.util.*"%>
<%@ include file="/WEB-INF/InitModel.jsp"%>
<%@ taglib uri="http://jakarta.apache.org/taglibs/input-0.90" prefix="input" %>
<html>
<head>
    <title>Descripcion de los campos para ingresar los Datos de Control Excepcion</title>
    <link href="<%= BASEURL %>/css/estilostsp.css" rel="stylesheet" type="text/css"> 
    <link href="/../../../css/estilostsp.css" rel="stylesheet" type="text/css">  
    
</head>
<script type='text/javascript' src="<%= BASEURL %>/js/boton.js"></script> 
<body> 
<div id="capaSuperior" style="position:absolute; width:100%; height:100px; z-index:0; left: 0px; top: 0px;">
<jsp:include page="/WEB-INF/toptsp.jsp?encabezado=Ayuda Descriptiva - Adicionar Control Excepcion"/>
</div>
<div id="capaCentral" style="position:absolute; width:100%; height:90%; z-index:0; left: 0px; top: 100px;"> 
   <p>&nbsp;</p>
   <table width="65%" border="2" align="center" id="tabla1" >
        <td>
         <table width="100%" height="50%" align="center">
              <tr>
                <td class="subtitulo1"><strong>Reporte</strong></td>
                <td width="50%" class="barratitulo"><img src="<%=BASEURL%>/images/titulo.gif" width="32" height="20"></td>
              </tr>
         </table>
         <table width="100%" borderColor="#999999" bgcolor="#F7F5F4">
           <tr>
             <td colspan="2" class="tblTitulo"><strong>Adicionar Control Excepcion- Pantalla Preliminar</strong></td>
           </tr>
           <tr>
             <td colspan="2" class="subtitulo"><strong>INFORMACION</strong></td>
           </tr>
           <tr>
             <td width="41%" class="fila">Codigo</td>
             <td width="59%"><span class="ayudaHtmlTexto">Un campo donde digita el codigo del cliente </span></td>
           </tr>
           <tr>
             <td class="fila">Origen</td>
             <td>Campo donde se le ingresa la origen del cliente</td>
           </tr>
           <tr>
             <td class="fila">Destino</td>
             <td>Campo donde usted puede digitar el destino del Cliente </td>
           </tr>
          
          
           <tr>
             <td class="fila">Agencia </td>
             <td><span class="ayudaHtmlTexto">Campo cerrado donde usted puede escojer la agencia que le sale en el listado </span></td>
           </tr>
           <tr>
             <td class="fila">plan de viaje </td>
             <td>checkbox donde asigana la excepcion si la hay o no </td>
           </tr>
		    <tr>
             <td class="fila">Hoja de Reporte </td>
             <td>checkbox donde asigana la excepcion si la hay o no</td>
           </tr>
		    <tr>
             <td class="fila">Pernoctacion</td>
             <td>checkbox donde asigana la excepcion si la hay o no</td>
           </tr>
		   
		   
         </table></td>
  </table>
		<p></p>
	<center>
	<img src = "<%=BASEURL%>/images/botones/salir.gif" style = "cursor:hand" name = "imgsalir" onClick = "parent.close();" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);">
	</center>
</div>  
</body>
</html>