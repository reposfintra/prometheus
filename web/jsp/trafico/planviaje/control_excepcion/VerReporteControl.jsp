<!--
- Autor : LREALES
- Date  : 19 de septiembre de 2006
- Copyrigth Notice : Fintravalores S.A. S.A
-->
<%--
-@(#)
--Descripcion : Pagina JSP, que se encarga de mostrar el reporte de proveedores de fintra
--%>
<%--
The taglib directive below imports the JSTL library. If you uncomment it,
you must also add the JSTL library to the project. The Add Library... action
on Libraries node in Projects view can be used to add the JSTL 1.1 library.
--%>
<%-- Declaracion de librerias--%>
<%@page session="true"%>
<%@page errorPage="../error/ErrorPage.jsp"%>
<%@page import="com.tsp.operation.model.beans.*, java.util.*, java.text.*"%>
<%@ include file="/WEB-INF/InitModel.jsp"%>
<%@ taglib uri="http://jakarta.apache.org/taglibs/input-0.90" prefix="input" %>
<%@page import="com.tsp.util.*"%>
<% Util u = new Util(); %>
<%@page contentType="text/html"%>
<%@page import="java.util.*"%> 
<%@page import="com.tsp.operation.model.*"%>
<%@taglib uri="http://jsptags.com/tags/navigation/pager" prefix="pg" %>
<html>
    <head>        
        <title>Reporte Control Excepcion </title>
		<script src='<%=BASEURL%>/js/date-picker.js'></script>
		<script language="JavaScript" type="text/javascript" src="<%=BASEURL%>/js/boton.js"></script>
		<script type='text/javascript' src="<%=BASEURL%>/js/validar.js"></script> 
		<link href="../css/estilostsp.css" rel="stylesheet" type="text/css">
		<link href="<%=BASEURL%>/css/estilostsp.css" rel="stylesheet" type="text/css">
		 <script>
		 	function Exportar (){
				
			    alert( 
				
					'Puede observar el seguimiento del proceso en la siguiente ruta del menu:' + '\n' + '\n' +
					'FINTRTAVALORES -> PROCESOS -> Seguimiento de Procesos' + '\n' + '\n' + '\n' +
					'Y puede observar el archivo excel generado, en la siguiente parte:' + '\n' + '\n' +
					'En la pantalla de bienvenida de la Pagina WEB,' + '\n' +
					'vaya a la parte superior-derecha,' + '\n' +
					'y presione un click en el icono que le aparece.'
					
				);
				
			}
		   function abrirVentanaBusq( an, al, url, pag ) {
			
        		parent.open( url, 'Conductor', 'width=' + an + ', height=' + al + ',scrollbars=no,resizable=no,top=10,left=65,status=yes' );
				
    		}
	
		</script>
    </head>
   <body onResize="redimensionar()" onLoad="redimensionar()">
   <%  String path    = application.getRealPath(model.menuService.getRealUrl(request.getRequestURI()));
    String datos[] = model.menuService.getContenidoMenu(BASEURL,request.getRequestURI(),path);    
	
	String codi=(request.getParameter ("codigo")!=null)?request.getParameter ("codigo"):"";
	String orig =(request.getParameter ("origen")!=null)?request.getParameter ("origen"):"";
	String dest =(request.getParameter ("destino")!=null)?request.getParameter ("destino"):"";
	String agen =(request.getParameter ("agencia")!=null)?request.getParameter ("agencia"):"";
	%>
	


	<div id="capaSuperior" style="position:absolute; width:100%; height:100px; z-index:0; left: 1px; top: 0px;">
	 <jsp:include page="/toptsp.jsp?encabezado=Ver Reporte Control Excepcion"/>
	</div>
	<div id="capaCentral" style="position:absolute; width:100%; height:87%; z-index:0; left: 0px; top: 100px; overflow: scroll;">
	<%String msg = request.getParameter("msg");
		if ( msg!=null){%>	
 <br>
  <table border="2" align="center">
  <tr>
    <td><table width="100%"  border="1" align="center"  bordercolor="#F7F5F4" bgcolor="#FFFFFF">
      <tr>
        <td width="300" align="center" class="mensajes">La busqueda no arrojo resultados</td>
        <td width="29" background="<%=BASEURL%>/images/cuadronaranja.JPG">&nbsp;</td>
        <td width="58">&nbsp;</td>
      </tr>
	  
    </table></td>
  </tr>
</table>
      <img src="<%=BASEURL%>/images/botones/regresar.gif" name="c_regresar" onclick="location.href='<%=BASEURL%>/jsp/trafico/planviaje/control_excepcion/BuscarControl.jsp'"  onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" style="cursor:hand">
	  <input name="salir2" src="<%=BASEURL%>/images/botones/salir.gif" type="image" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" style="cursor:hand" class="boton" onclick="window.close();">
   
 <%}else{
        Vector datosC = model.control_excepcionService.getVector();
		Vector datoss = model.control_excepcionService.getVector();
		String style = "simple";
		String position =  "bottom";
		String index =  "center";
		int maxPageItems = 20;
		int maxIndexPages = 10;
		if( datos != null && datoss.size() > 0 ){
			BeanGeneral beanGeneral = (BeanGeneral) datoss.elementAt(0);
		%>		
   <form name="form1" method="post" action="<%=CONTROLLER%>?estado=Buscar&accion=Control&opcion=2">
              <p>
	    <input name="codigo" type="hidden" id="codigo" value="<%=codi%>">
	    </p>
              <p>
	      <input name="origen" type="hidden" id="origen" value="<%=orig%>">
              </p>
              <p><input name="destino" type="hidden" id="destino" value="<%=dest%>">
	        </p>
              <p>
            <input name="agencia" type="hidden" id="agencia" value="<%=agen%>">
          </p>
              <p>
                <img src="<%=BASEURL%>/images/botones/regresar.gif" name="c_regresar" onclick="location.href='<%=BASEURL%>/jsp/trafico/planviaje/control_excepcion/BuscarControl.jsp'"  onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" style="cursor:hand">
 	            <input name="salir" src="<%=BASEURL%>/images/botones/salir.gif" type="image" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" style="cursor:hand" class="boton" onclick="window.close();">
          </p>
   </form>
   
		<table width="150%"  border="2" align="center">  
           <tr>
				<td class="barratitulo" colspan='2' >
					   <table cellpadding='0' cellspacing='0' width='100%'>
							 <tr class="fila">
								 <td width="76%" align="left" class="subtitulo1">&nbsp;<strong>Ver Reporte Control Excepcion</strong></td>
								 <td width="24%"   align="left"><img src="<%= BASEURL %>/images/titulo.gif" width="32" height="20"  align="left"><%=datos[0]%></td>
							</tr>
					   </table>   
				</td>
              </tr>
                     
			  <tr   class="letra">
				 <td colspan='12'>
					  <table cellpadding='0' cellspacing='0' width='100%' align="center">
						  <%if (!codi.equals("")){%>
					
						  <tr  class="letra">
								   <td width='3%' class="fila" ><div align="left"><strong> Codigo   : </strong></div></td>
							   <td width='97%' class="fila" ><div align="left"><%=codi%></div></td>
						  </tr>
						<%}%>                
						<%if (!orig.equals("")){%>              
							   <tr>
								 <td width='3%' class="fila" ><div align="left"><strong> Origen       : </strong></div></td>
								 <td width='97%' class="fila"><div align="left"><%=orig%></div></td>
							   </tr>
						<%}%>
						<%if (!dest.equals("")){%> 
							   <tr>
								 <td width='3%' class="fila" ><div align="left"><strong> Destino    : </strong></div></td>
								 <td width='97%' class="fila"><div align="left"><%=dest%></div></td>
							   </tr>
						<%}%>                
						<%if (!agen.equals("")){%>
							   <tr>
								 <td width='3%' class="fila" ><div align="left"><strong> Agencia  : </strong></div></td>
								 <td width='97%' class="fila"><div align="left"><%=(beanGeneral.getValor_10()!=null)?beanGeneral.getValor_10():""%></div></td>
							   </tr>
						<%}%>
					   
              		</table>                         
                         </td>
					 </tr>
              </table>
         </td>
        </tr>
          <tr>
                            <td class="barratitulo" colspan='20' align="center" >
                               <table width='150%' height="20" cellpadding='0' cellspacing='0' align="center">
                                         <tr class="fila">
                                             <td width="10%" align="left" class="subtitulo1"><strong>Clientes</strong></td>
                                             <td width="90%"   align="left"><img src="<%= BASEURL %>/images/titulo.gif" width="32" height="20"  align="left"></td>
                                        </tr>
                              </table>   
                            </td>
        </tr>
          <tr>
            <td>
              <table width="150%"  border="1" bordercolor="#999999" bgcolor="#F7F5F4" >
                <tr class="tblTitulo" align="center">
				  <td nowrap width="5%" align="center">ITEM</td>
				  <td nowrap width="5%" align="center">CODIGO</td>
				  <td nowrap width="5%" align="center">ORIGEN</td>
                  <td nowrap width="5%" align="center">DESTINO</td>
				  <td nowrap width="5%" align="center">AGENCIA</td>
				  <td nowrap width="5%" align="center">PLAN DE VIAJE</td>
				  <td nowrap width="5%" align="center">HOJA DE REPORTE</td> 
				  <td nowrap width="5%" align="center">PERNOTACION</td>
				      
                                  
                </tr>	
				<pg:pager
				 items="<%=datosC.size()%>"
				 index="<%= index %>"
				 maxPageItems="<%= maxPageItems %>"
				 maxIndexPages="<%= maxIndexPages %>"
				 isOffset="<%= true %>"
				 export="offset,currentPageNumber=pageNumber"
				 scope="request">					
		<%
			
			for (int i = offset.intValue(), l = Math.min(i + maxPageItems, datosC.size()); i < l; i++){
				BeanGeneral info = (BeanGeneral) datosC.elementAt(i);
	  %>  	  
	  			
	  			<pg:item>
                  <tr class="<%=i%2==0?"filagris":"filaazul"%>" onMouseOver='cambiarColorMouse(this)' style="cursor:hand" title="Click" align="center" onClick="window.open('<%=CONTROLLER%>?estado=ControlBusqueda&accion=Modificar&codigo=<%=info.getValor_01()%>&origen=<%=info.getValor_02()%>&destino=<%=info.getValor_03()%>&agencia=<%=info.getValor_07()%>' ,'M','status=yes,scrollbars=no,width=780,height=650,resizable=yes');"> 
				  <td nowrap align="left" class="bordereporte"><%=(info.getValor_11()!=null)?info.getValor_11():""%>&nbsp;</td>
				  <td nowrap align="left" class="bordereporte"><%=(info.getValor_01()!=null)?info.getValor_01():""%>&nbsp;</td>
                  <td nowrap align="left" class="bordereporte"><%=(info.getValor_08()!=null)?info.getValor_08():""%>&nbsp;</td>
                  <td nowrap align="left" class="bordereporte"><%=(info.getValor_09()!=null)?info.getValor_09():""%>&nbsp;</td>
                  <td nowrap align="left" class="bordereporte"><%=(info.getValor_10()!=null)?info.getValor_10():""%>&nbsp;</td>
		  <td nowrap align="left" class="bordereporte"><%=(info.getValor_04()!=null)?info.getValor_04():""%>&nbsp;</td>		  
                  <td nowrap align="left" class="bordereporte"><%=(info.getValor_05()!=null)?info.getValor_05():""%>&nbsp;</td>
                  <td nowrap align="left" class="bordereporte"><%=(info.getValor_06()!=null)?info.getValor_06():""%>&nbsp;</td>
                  
                  
				   	
                </tr>
			  </pg:item>	
		<%}%>
		<tr class="bordereporte">
          <td td height="20" colspan="10" nowrap align="center">
		   <pg:index>
            <jsp:include page="/WEB-INF/jsp/googlesinimg.jsp" flush="true"/>      
           </pg:index> 
	      </td>
        </tr>
        </pg:pager>
            </table>
      </table></td>
      </tr>
		    </table>
			
		   <form name="form1" method="post" action="<%=CONTROLLER%>?estado=Cliente&accion=Buscar&opcion=2">
		           <p>
	     <input name="codigo" type="hidden" id="codigo" value="<%=codi%>">
	    </p>
              <p>
	      <input name="origen" type="hidden" id="origen" value="<%=orig%>">
              </p>
              <p><input name="destino" type="hidden" id="destino" value="<%=dest%>">
	        </p>
              <p>
            <input name="agencia" type="hidden" id="agencia" value="<%=agen%>">
          </p>
            
	  
      <img src="<%=BASEURL%>/images/botones/regresar.gif" name="c_regresar" onclick="location.href='<%=BASEURL%>/jsp/trafico/planviaje/control_excepcion/BuscarControl.jsp'"  onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" style="cursor:hand">
	  <input name="salir2" src="<%=BASEURL%>/images/botones/salir.gif" type="image" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" style="cursor:hand" class="boton" onclick="window.close();">
    </form>	
	  <%}%>	  
	  
 <%}%>
	  
</body>
</div>
<%=datos[1]%>  
</html>
