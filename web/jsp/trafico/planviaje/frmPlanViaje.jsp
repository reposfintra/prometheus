  <!--
     - Author(s)       :      FERNEL VILLACOB DIAZ
     - Date            :      07/10/2005  
     - Copyright Notice:      TRANSPORTES SANCHEZ POLO S.A.
  -->
 <%--   - @(#)  
     - Description: Plan de viaje de una planilla
 --%>

<%@ page session   ="true"%> 
<%@ page errorPage ="/error/ErrorPage.jsp"%>
<%@ page import    ="java.util.*" %>
<%@ page import    ="com.tsp.util.*" %>
<%@ page import    ="com.tsp.operation.model.beans.*" %>
<%@ include file   ="/WEB-INF/InitModel.jsp"%>


<html>
<head>

    <title>Formulario Plan de Viaje</title>
    <link href="<%= BASEURL %>/css/estilostsp.css" rel="stylesheet" type="text/css"> 
    <script type='text/javascript' src="<%= BASEURL %>/js/boton.js"></script>
  	<script language="JavaScript" type="text/javascript" src="<%=BASEURL%>/js/script.js"></script> 
  	<script language="JavaScript" type="text/javascript" src="<%=BASEURL%>/js/general.js"></script> 
  	<script>
		function buscarProveedores(CONTROLLER, validar,maxfila){
			var url = "";
			url = CONTROLLER+"?estado=Factura&accion=AProveedores&validar="+validar+"&maxfila="+maxfila;
			for (var x=0;x<document.formulario.length;x++)
				url =  url +"&"+ document.formulario.elements[x].name + "=" + document.forma1.elements[x].value;
			
			var hijo=window.open(url,'Trafico','width=700,height=520,scrollbars=no,resizable=yes,top=10,left=65,status=yes');
			hijo.opener=document.window;
		}
		
		function cargarUbicaciones(CONTROLLER, via) {
			formulario.action = CONTROLLER + "?estado=PlanViaje&accion=BuscarUbicaciones";
			formulario.viaSelec.value = via;
			formulario.submit();
		}
	</script>
</head>
<body onresize="redimensionar()" onload="redimensionar()">

<div id="capaSuperior" style="position:absolute; width:100%; height:100px; z-index:0; left: 0px; top: 0px;">
<jsp:include page="/WEB-INF/toptsp.jsp?encabezado=Plan de Viaje"/>
</div>


<%  String path    = application.getRealPath(model.menuService.getRealUrl(request.getRequestURI()));
    String datos[] = model.menuService.getContenidoMenu(BASEURL,request.getRequestURI(),path);    %>


<div id="capaCentral" style="position:relative; width:100%; height:'200%'; z-index:0; left: 0px; top: 100px; overflow: scroll;">
    
<%    PlanDeViaje planViaje        = (PlanDeViaje)request.getAttribute("planViajeObject");
      List  listAlimentacion       = planViaje.getListAlimentacion();
      List  listPernotacion        = planViaje.getListPernotacion() ;
      List  listParqueadero        = planViaje.getListParqueadero() ;
      List  listTanqueo            = planViaje.getListTanqueo()     ;
      List  listAlimentacionPlan   = planViaje.getListAlimentacionPlan();
      List  listPernotacionPlan    = planViaje.getListPernotacionPlan() ;
      List  listParqueaderoPlan    = planViaje.getListParqueaderoPlan() ;
      List  listTanqueoPlan        = planViaje.getListTanqueoPlan()     ;
      Usuario     Usuario          = (Usuario) session.getAttribute("Usuario");
      String      msj              = request.getParameter("msj"); 
      String      usuario          = Usuario.getLogin(); 
      String      hoy              = Utility.getDate(6).replaceAll("/","-");%>

  
 
<form action="<%=CONTROLLER%>?estado=PlanViaje&accion=Manager" method="post" name="formulario">
 <table width="90%"  border="2" align="center">
  <tr>
    <td>
		
        <table  width='100%' class='tablaInferior'>      
	   <tr>
             <td>
                 <table width='100%'  class="barratitulo">
                    <tr class="fila">
                            <td align="left" width='30%' class="subtitulo1" nowrap> INFORMACI�N PLAN DE VIAJE</td>
                            <td align="left" width='70%' ><img src="<%=BASEURL%>/images/titulo.gif"  width="32" height="20"   align="left"><%=datos[0]%></td>
                    </tr>
                </table>
              </td>
           </tr>         
           <tr>
           	<td>
           	
           	             <!-- DATOS DEL VIAJE-->            
				<table width="100%"  border="1" align="center">
  				    <tr>
    			             <td>
                                        <table width='100%' align="center"  class="tablaInferior">
                                        
                                        <tr class="subtitulo1"><td colspan="6">Datos del Viaje</td></tr>
                                        
                                        <tr >                         
                                                <td width='15%' class="fila" >   Planilla   </td><td  width='10%' class="letra">  <input type='hidden' class="textbox"  name='planilla' readonly value="<%= planViaje.getPlanilla() %>" maxlength='10' ><%= planViaje.getPlanilla() %>           </td>
                                                <td width='12%' class="fila" >   Distrito   </td><td  width='25%' class="letra">  <input type='hidden' class="textbox"  name='distrito' readonly value="<%= planViaje.getDistrito() %>" maxlength='4'  size='5'><%= planViaje.getDistrito() %>   </td>
                                                <td width='14%' class="fila" >   Placa      </td><td  width='*'   class="letra">  <input type='hidden' class="textbox"  name='placa'    readonly value="<%= planViaje.getPlaca()    %>" maxlength='10' ><%= planViaje.getPlaca()    %>           </td>
                                        </tr>
                                        
                                        <tr >
                                                <td class="fila">  Fecha Despacho    </td><td class="letra">  <input type='hidden' class="textbox"  name='fechaDespacho' readonly value="<%= planViaje.getFechaDespacho() %>"   style='width:100%'><%= planViaje.getFechaDespacho() %> </td>                                                
                                                <td class="fila">  Origen            </td><td class="letra">  <input type='hidden' class="textbox"  name='origen'        readonly value="<%= planViaje.getOrigen()        %>"   style='width:100%'><%= planViaje.getOrigen()        %> </td>
                                                <td class="fila">  Destino           </td><td class="letra">  <input type='hidden' class="textbox"  name='destino'       readonly value="<%= planViaje.getDestino()       %>"   style='width:100%'><%= planViaje.getDestino()       %> </td>
                                        </tr>
                                        
                                        <tr >   
                                             <td class="fila">  Destinatario     </td><td colspan='3' class="letra">  <input type='hidden' style="width='100%'"  class="textbox"  name='destinatario'  readonly value="<%= planViaje.getDestinatario()  %>"><%= planViaje.getDestinatario()  %> </td>                                        
                                             <td class="fila">  Tipo Carga       </td><td class="letra">              <input type='hidden' style="width='100%'"  class="textbox"  name='tipoCarga'     readonly value="<%= planViaje.getTipoCarga()     %>"><%= planViaje.getTipoCarga()     %> </td>
                                        </tr>
                                        
                                        <tr >
                                                <td class="fila">Productos                 </td><td colspan='3' class="fila"> <input type='text' class="textbox"   name='productos'            value="<%= planViaje.getProducto()    %>" maxlength='50' style='width:100%' > </td>
                                                <td class="fila">  Placa Un. Carga         </td><td class="letra"><input type='hidden' class="textbox"   name='placaUnidad' readonly value="<%= planViaje.getPlacaUnidad() %>"><%= planViaje.getPlacaUnidad() %> <input type="hidden" name="viaSelec" id="viaSelec"></td>
                                        </tr>
                                        
                                        <tr >
                                                <td class="fila">Contenedores              </td><td colspan='3' class="letra"> <input type='hidden' class="textbox"   name='contenedores' readonly value="<%= planViaje.getContenedores() %>" style='width:100%' ><%= planViaje.getContenedores() %> </td>
                                                <td class="fila">  Trailer                 </td><td class="letra">             <input type='hidden' class="textbox"   name='trailer'      readonly value="<%= planViaje.getTrailer()      %>" maxlength='10'> <%= planViaje.getTrailer()      %></td>                     
                                        </tr>
                                        
                                        <tr class="fila">
                                                <td >Ruta - <%= planViaje.getRuta()%></td>
                                                <td colspan='3'>  
                                                 <select name='ruta' class="textbox" style='width:90%' onchange="cargarUbicaciones('<%= CONTROLLER%>', this.value);/*NuevaVentana('<%=BASEURL%>/jsp/trafico/planviaje/ListaPC.jsp?ruta='+this.value,'yy',500,500,100,100);*/" >
                                                 <option  value="" > Ninguna </option>
                                                 <% List lisVia = planViaje.getVias();
                                                    if(lisVia!=null && lisVia.size()>0){
                                                          Iterator it = lisVia.iterator();
                                                          while(it.hasNext()){
                                                              Via  via = (Via) it.next();
                                                              String select = (via.getCodigo().equals(planViaje.getRuta()) )?"selected='selected'":"";%>
                                                              <option  value="<%=via.getCodigo()%>"  <%=select%>> [<%=via.getCodigo()%>] &nbsp - <%=via.getDescripcion()%> </option>
                                                          <%}
                                                    }%>
                                                 </select>                                                 
                                                 <img src='<%=BASEURL%>/images/botones/iconos/buscar.gif'   style=" cursor:hand'"  title='Escojer Pr�ximo PC....'   name='i_load'      onclick=" javascript: NuevaVentana('<%=BASEURL%>/jsp/trafico/planviaje/ListaPC.jsp?ruta='+formulario.ruta.value,'xx',500,500,100,100); "    > 
                                                 
                                                 
                                                </td>
                                                
                                                <td > Fecha Plan Viaje</td>
                                                <td  width='24%'> 
                                                     <input type='text' class="textbox"  name='fechaPlan'     readonly       value="<%= (planViaje.getFechaPlanViaje().equals(""))? hoy :planViaje.getFechaPlanViaje() %>">                                                        
                                                     <a href="javascript:void(0)" onclick="if(self.gfPop)gfPop.fPopCalendar(document.formulario.fechaPlan);return false;" HIDEFOCUS>
                                                        <img src="<%=BASEURL%>\js\Calendario\cal.gif" width="16" height="16" border="0" alt="De click aqui para escoger la fecha">
                                                     </a>
                                                </td>
                                                
                                       </tr>         
                   
                                        <tr class="fila">
                                                <td>Prox. P.C.   </td>
                                                <td colspan='2'> <input type='text' class="textbox"   name='proximopc'     style="width='100%'"   readonly value="<%= planViaje.getPCTrafico() + ":" + planViaje.getNombrePCTrafico() %>" > </td>
                                                
                                                <td>&nbsp&nbsp&nbsp&nbsp Zona P.C.  </td>
                                                <td colspan='2'> <input type='text' class="textbox"   name='zona'                                 readonly value="<%= planViaje.getZona() + ":" + planViaje.getNombreZona() %>" > </td>
                                        </tr>
                                        
                                        
                                        <tr >
                                                <td class="fila">  Salida      </td>
                                                <td title='Caravanas' colspan='2' class="fila">           
                                                 <a href='#' onclick="javascript: NuevaVentana('<%=BASEURL%>/jsp/trafico/caravana/modificarCaravana.jsp?first=1&caravana=<%= planViaje.getCaravana() %>','Caravana',900,600,100,50);">
                                                     <%= planViaje.getCaravana() %> 
                                                 </a>
                                                </td>
                                                <td class="fila"> &nbsp&nbsp&nbsp&nbsp Escolta     </td><td colspan='2' class="letra"> <input type='hidden' class="textbox" TITLE='Placas asociadas a la misma caravana'   name='escolta'  readonly value="<%= planViaje.getEscolta() %>" maxlength='100' style='width:100%'><%= planViaje.getEscolta() %> </td>                     
                                        </tr>
                                        
                                        
                                        <tr >      
                                                <td class="fila">  Usu. Creaci�n   </td><td class="letra"> <input type='hidden' class="textbox"  name='usuarioCreacion'  readonly value="<%= (planViaje.getUsuario().equals(""))?usuario: planViaje.getUsuario() %>" style='width:100%' ><%= (planViaje.getUsuario().equals(""))?usuario: planViaje.getUsuario() %> </td>
                                                <td class="fila">  Fecha  Creaci�n </td><td class="letra"> <input type='hidden' class="textbox"  name='fechaCreacion'    readonly value="<%= planViaje.getFechaCreacion() %>"  ><%= planViaje.getFechaCreacion() %> </td>                              
                                                <td colspan='2' class="fila"> 
                                                    <input type='checkbox'  name='cbRetorno' <%= planViaje.getRetorno().equals("N")?"":"checked='checked'"%>   >Compromiso Retorno
                                                </td>                                                  
                                        </tr>
                                        
                                        
                                        <tr class="fila">      
                                                <td colspan='6' align='center' >  
                                                     <a href='#' onclick="NuevaVentana('<%=BASEURL%>/jsp/trafico/caravana/caravana.jsp?frt=1','Caravana',900,250,100,50);">Caravana</a>
                                                     &nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp
                                                     <a href="#" onclick="NuevaVentana('<%=BASEURL%>/jsp/trafico/caravana/asignarEscolta.jsp','Escolta',900,250,100,50);">Escolta</a>
                                                </td>                       
                                        </tr>
                                        
                                        
                                       </table>
                                       
				    </td>
		  		</tr>
		        </table>
           	           
           	
           	
           	
           	
                  <!-- DATOS CONDUCTOR -->
				<table width="100%"  border="1" align="center">
				   <tr>
    					<td>
					  <table  width='100%' class="tablaInferior">
						   <tr class="subtitulo1">
							<td colspan="6">Datos Conductor</td>
						   </tr>
                                                    <tr >                         
                                                            <td width="13%" class="fila">C�dula   </td><td width="30%" class="letra">              <input type='hidden' class="textbox" name='cedulaCond' readonly value="<%= planViaje.getCedulaConductor()  %>" maxlength='15'><%= planViaje.getCedulaConductor()  %> </td>
                                                            <td width="15%" class="fila">Nombre   </td><td width="*" colspan='3' class="letra">    <input type='hidden' class="textbox" name='nameCond'   readonly value="<%= planViaje.getNameConductor()    %>" style='width:100%' ><%= planViaje.getNameConductor()    %> </td>
                                                    </tr>
                                                    <tr >                         
                                                         <td class="fila">Ciudad    </td><td class="letra">               <input type='hidden' class="textbox" name='ciudadCond' readonly value="<%= planViaje.getCiudadConductor() %>" style='width:100%'><%= planViaje.getCiudadConductor() %> </td>
                                                         <td class="fila">Direccion </td><td COLSPAN='3' class="letra">   <input type='hidden' class="textbox" name='dirCond'    readonly value="<%= planViaje.getDirConductor()    %>" style='width:100%'><%= planViaje.getDirConductor()    %> </td>                                
                                                    </tr>
                                                    <tr >
                                                        <td class="fila">Telefono  </td><td colspan='5' class="letra">       <input type='hidden' class="textbox" name='telCond'   readonly value="<%= planViaje.getTelConductor()    %>"><%= planViaje.getTelConductor()    %> </td>
                                                    </tr> 
                          </table>
				    </td>
	  			</tr>
			  </table>
     	        <!--fin DATOS CONDUCTOR--> 
     	        
     	        
     	        <!-- EQUIPOS DE COMUNICACION-->
			    <table width="100%"  border="1">
  			        <tr>
    				   <td>
					  <table  width='100%' class="letrafila">
                                                <tr class="subtitulo1"> <td colspan="6">Equipos de Comunicaci�n</td></tr>
                                                <tr class="fila">                         
                                                       <td width='13%'>  Radio      </td><td width='20%'> <input type='text' class="textbox"  name='radio'   value="<%= planViaje.getRadio()   %>"   maxlength='20' style='width:90%'> </td>
                                                       <td width='10%'>  Avantel    </td><td width='19%'> <input type='text' class="textbox"  name='avantel' value="<%= planViaje.getAvantel() %>"   maxlength='20' style='width:90%'> </td>
                                                       <td width='13%'>  Cazador    </td><td width='*'>   <input type='text' class="textbox"  name='cazador' value="<%= planViaje.getCazador() %>"   maxlength='20' style='width:90%'> </td>
                                                </tr>
                                                <tr class="fila">                         
                                                        <td>  Celular               </td><td>             <input type='text' class="textbox"  name='celular'  value="<%= planViaje.getCelular()  %>" maxlength='20' style='width:90%' onkeypress="soloDigitos(event,'decNO')"> </td>
                                                        <td>  Telefono              </td><td>             <input type='text' class="textbox"  name='telefono' value="<%= planViaje.getTelefono() %>" maxlength='20' style='width:90%' onkeypress="soloDigitos(event,'decNO')"> </td>
                                                        <td>  Movil                 </td><td>             <input type='text' class="textbox"  name='movil'    value="<%= planViaje.getMovil()    %>" maxlength='20' style='width:90%'> </td>
                                                </tr>
                                                <tr class="fila">                         
                                                        <td> Otro                   </td><td colspan='5'> <input type='text' class="textbox"  name='otros'    value="<%= planViaje.getOtros()    %>" maxlength='40' style='width:20%'> </td>
                                                </tr>                   
                         </table>
				   </td>
  			       </tr>
			  </table>
		<!--fin EQ DE COMUNICACION--> 
		
		
		 <!-- DATOS FAMILIAR-->
				<table width="100%"  border="1">
  				    <tr><td colspan="6" class="subtitulo1">Datos de un Familiar</td></tr>
  				    <tr>
    					<td>
					     <table  width='100%' class="letrafila">
                                                <tr class="fila">                         
                                                        <td width='13%'>  Nombre     </td><td width='49%'> <input type='text' class="textbox" name='nameFamiliar' value="<%= planViaje.getNameFamiliar() %>" maxlength='50' style='width:100%'> </td>
                                                        <td width='15%'>  Telefono   </td><td width='*'  > <input type='text' class="textbox" name='telFamiliar'  value="<%= planViaje.getTelFamiliar()  %>" maxlength='20'  onkeypress="soloDigitos(event,'decNO')"> </td>
                                                </tr>
                          </table>
				       </td>
  				   </tr>
		        </table>
                <!-- fin DATOS FAMILIAR-->
                
                
                <!-- DATOS PROPIETARIO -->
				<table width="100%"  border="1">
  				     <tr><td colspan="6" class="subtitulo1">Datos Propietario</td></tr>
  			             <tr>
    					<td>
                                            <table  width='100%' class="letrafila">
                                                <tr >                         
                                                      <td width='13%' class="fila">    C�dula   </td><td width='30%' class="letra">              <input type='hidden' class="textbox" name='cedulaProp' readonly value="<%= planViaje.getCedulaPropietario() %>" maxlength='15'><%= planViaje.getCedulaPropietario() %> </td>
                                                      <td width='15%' class="fila">    Nombre   </td><td width="*" colspan='3' class="letra">    <input type='hidden' class="textbox" name='nameProp'   readonly value="<%= planViaje.getNamePropietario()   %>" style='width:100%' ><%= planViaje.getNamePropietario()   %> </td>
                                                </tr>
                                                <tr >                         
                                                     <td class="fila">  Ciudad               </td><td class="letra">                <input type='hidden' class="textbox"  name='ciudadProp'readonly value="<%= planViaje.getCiudadPropietario() %>" style='width:100%'><%= planViaje.getCiudadPropietario() %> </td>
                                                     <td class="fila">  Direccion            </td><td colspan='3'class="letra">    <input type='hidden' class="textbox"  name='dirProp'   readonly  value="<%= planViaje.getDirPropietario()    %>" style='width:100%'><%= planViaje.getDirPropietario()    %> </td>
                                                </tr>
                                                <tr >
                                                     <td class="fila">  Telefono             </td><td colspan='5' class="letra">    <input type='hidden' class="textbox"  name='telProp'   readonly  value="<%= planViaje.getTelPropietario()    %>"><%= planViaje.getTelPropietario()    %> </td>
                                                </tr>
                          </table>
				    </td>
  				</tr>
			  </table>
		<!-- fin DATOS PROPIETARIO -->
		
     	        
                <!-- ALIMENTACION-->
                         <table width="100%"  border="1">
  			        <tr>
    				       <td>
				             <table  width='100%' class="tablaInferior" >
                                                     <tr class="subtitulo1"><td colspan="3">Alimentaci�n</td></tr>
                                                     <tr class="fila">                         
                                                        <td width="45%" align='center' >                                                               
                                                               RESTAURANTES EN LA VIA
                                                               <select multiple size='5' class='select' style='width:90%' name='alimentacion' onchange=" if (this.selectedIndex!=-1) { window.status=this[this.selectedIndex].text; }">
                                                                   <% for(int i=0; i<listAlimentacion.size();i++){
                                                                          Ubicacion  ub = ( Ubicacion)listAlimentacion.get(i); %>
                                                                          <option value='<%=ub.getCod_ubicacion()%>' >  <%= ub.getDescripcion()%> </option>
                                                                   <%}%>
                                                              </select>
                                                        </td>
                                                        <td width="10%" align='center'>
                                                               <br>
                                                               <img src='<%=BASEURL%>/images/botones/envDer.gif' style='cursor:hand' name='imgEnvDer'  onclick="move   (alimentacion,  alimentacionPlan); " onMouseOver='botonOver(this);' onMouseOut='botonOut(this);'><br>             
                                                               <img src='<%=BASEURL%>/images/botones/envIzq.gif' style='cursor:hand' name='imgEnvIzq'  onclick="move   (alimentacionPlan, alimentacion ); " onMouseOver='botonOver(this);' onMouseOut='botonOut(this);'><br>            
                                                        </td>
                                                        <td width="*" align='center'>
                                                           
                                                               RESTAURANTES PLAN DE VIAJE
                                                               <select multiple size='5' class='select' style='width:90%' name='alimentacionPlan'     onchange=" if (this.selectedIndex!=-1) { window.status=this[this.selectedIndex].text; }">
                                                                   <% for(int i=0; i<listAlimentacionPlan.size();i++){
                                                                          Ubicacion  ub = ( Ubicacion)listAlimentacionPlan.get(i); %>
                                                                          <option value='<%=ub.getCod_ubicacion()%>' >  <%= ub.getDescripcion()%> </option>
                                                                   <%}%>
                                                              </select>
                                                        
                                                        </td>
                                                    </tr>
                             </table>
                                            
				   </td>
	  		       </tr>
			  </table>
	       <!--fin ALIMENTACION-->
	       
	       
	       
                <!-- PERNOTACION-->
			    <table width="100%"  border="1">
  			        <tr>
    				       <td>
				             <table  width='100%' class="tablaInferior">
                                                     <tr class="subtitulo1"><td colspan="3">Pernotaci�n</td></tr>
                                                     <tr class="fila">                         
                                                        <td width="45%" align='center' >
                                                               PERNOTADEROS EN LA VIA
                                                               <select multiple size='5' class='select' style='width:90%' name='pernotacion' onchange=" if (this.selectedIndex!=-1) { window.status=this[this.selectedIndex].text; }">
                                                                   <% for(int i=0; i<listPernotacion.size();i++){
                                                                          Ubicacion  ub = ( Ubicacion)listPernotacion.get(i); %>
                                                                          <option value='<%=ub.getCod_ubicacion()%>' >  <%= ub.getDescripcion()%> </option>
                                                                   <%}%>
                                                              </select>
                                                        </td>
                                                        <td width="10%"  align='center'>
                                                               <br>
                                                               <img src='<%=BASEURL%>/images/botones/envDer.gif' style='cursor:hand' name='imgEnvDer'  onclick="move   (pernotacion,  pernotacionPlan); " onMouseOver='botonOver(this);' onMouseOut='botonOut(this);'><br>             
                                                               <img src='<%=BASEURL%>/images/botones/envIzq.gif' style='cursor:hand' name='imgEnvIzq'  onclick="move   (pernotacionPlan, pernotacion ); " onMouseOver='botonOver(this);' onMouseOut='botonOut(this);'><br>            
                                                        </td>
                                                        <td width="*"  align='center'>
                                                             PERNOTADEROS PLAN DE VIAJE
                                                             <select multiple size='5' class='select' style='width:90%' name='pernotacionPlan' onchange=" if (this.selectedIndex!=-1) { window.status=this[this.selectedIndex].text; }">
                                                                   <% for(int i=0; i<listPernotacionPlan.size();i++){
                                                                          Ubicacion  ub = ( Ubicacion)listPernotacionPlan.get(i); %>
                                                                          <option value='<%=ub.getCod_ubicacion()%>' >  <%= ub.getDescripcion()%> </option>
                                                                   <%}%>
                                                          </select>
                                                        </td>
                                                    </tr>
                             </table>
				   </td>
	  		       </tr>
			  </table>
		<!--fin PERNOCTACION--> 
		
		
		
                
		
		
                <!-- PARQUEADERO-->
			 <table width="100%"  border="1">
  			        <tr>
    				       <td>
				             <table  width='100%' class="tablaInferior">
                                                     <tr class="subtitulo1"><td colspan="3">Parqueaderos</td></tr>
                                                     <tr class="fila">                         
                                                        <td width="45%" align='center' >
                                                               PARQUEADEROS EN LA VIA
                                                               <select multiple size='5' class='select' style='width:90%' name='parqueadero' onchange=" if (this.selectedIndex!=-1) { window.status=this[this.selectedIndex].text; }">                                                                   
                                                                   <% for(int i=0; i<listParqueadero.size();i++){
                                                                          Ubicacion  ub = ( Ubicacion)listParqueadero.get(i); %>
                                                                          <option value='<%=ub.getCod_ubicacion()%>' >  <%= ub.getDescripcion()%> </option>
                                                                   <%}%>
                                                              </select>
                                                        </td>
                                                        <td width="10%"  align='center'>
                                                               <br>
                                                               <img src='<%=BASEURL%>/images/botones/envDer.gif' style='cursor:hand' name='imgEnvDer'  onclick="move   (parqueadero,  parqueaderoPlan); " onMouseOver='botonOver(this);' onMouseOut='botonOut(this);'><br>             
                                                               <img src='<%=BASEURL%>/images/botones/envIzq.gif' style='cursor:hand' name='imgEnvIzq'  onclick="move   (parqueaderoPlan, parqueadero ); " onMouseOver='botonOver(this);' onMouseOut='botonOut(this);'><br>            
                                                        </td>
                                                        <td width="*"  align='center'>
                                                             PARQUEADEROS PLAN DE VIAJE
                                                             <select multiple size='5' class='select' style='width:90%' name='parqueaderoPlan' onchange=" if (this.selectedIndex!=-1) { window.status=this[this.selectedIndex].text; }">                                                                   
                                                                   <% for(int i=0; i<listParqueaderoPlan.size();i++){
                                                                          Ubicacion  ub = ( Ubicacion)listParqueaderoPlan.get(i); %>
                                                                          <option value='<%=ub.getCod_ubicacion()%>' >  <%= ub.getDescripcion()%> </option>
                                                                   <%}%>
                                                             </select>
                                                        </td>
                                                    </tr>
                             </table>
				   </td>
	  		       </tr>
			  </table>
                <!-- fin PARQUEADERO-->  
                
                
                <!-- TANQUEO -->
		       <table width="100%"  border="1">
  			        <tr>
    				       <td>
				             <table  width='100%' class="tablaInferior">
                                                     <tr class="subtitulo1"><td colspan="3">Tanqueos</td></tr>
                                                     <tr class="fila">                         
                                                        <td width="45%" align='center'>
                                                               ESTACIONES DE COMBUSTIBLES EN LA VIA
                                                               <select multiple size='5' class='select' style='width:90%' name='tanqueo' onchange=" if (this.selectedIndex!=-1) { window.status=this[this.selectedIndex].text; }">
                                                                  <% for(int i=0; i<listTanqueo.size();i++){
                                                                          Ubicacion  ub = ( Ubicacion)listTanqueo.get(i); %>
                                                                          <option value='<%=ub.getCod_ubicacion()%>' >  <%= ub.getDescripcion()%> </option>
                                                                   <%}%>
                                                              </select>
                                                        </td>
                                                       <td width="10%"  align='center'>
                                                               <br>
                                                               <img src='<%=BASEURL%>/images/botones/envDer.gif' style='cursor:hand' name='imgEnvDer'  onclick="move   (tanqueo,  tanqueoPlan); " onMouseOver='botonOver(this);' onMouseOut='botonOut(this);'><br>             
                                                               <img src='<%=BASEURL%>/images/botones/envIzq.gif' style='cursor:hand' name='imgEnvIzq'  onclick="move   (tanqueoPlan, tanqueo ); " onMouseOver='botonOver(this);' onMouseOut='botonOut(this);'><br>            
                                                       </td>
                                                        <td width="*"  align='center'>
                                                             ESTACIONES DE COMBUSTIBLES  PLAN DE VIAJE
                                                             <select multiple size='5' class='select' style='width:90%' name='tanqueoPlan' onchange=" if (this.selectedIndex!=-1) { window.status=this[this.selectedIndex].text; }">
                                                                  <% for(int i=0; i<listTanqueoPlan.size();i++){
                                                                          Ubicacion  ub = ( Ubicacion)listTanqueoPlan.get(i); %>
                                                                          <option value='<%=ub.getCod_ubicacion()%>' >  <%= ub.getDescripcion()%> </option>
                                                                   <%}%>
                                                             </select>
                                                        </td>
                                                    </tr>
                             </table>
				   </td>
	  		       </tr>
			  </table>
		<!-- TANQUEO-->
		
		
               
		
                <!-- COMENTARIOS -->
				<table width="100%"  border="1">
  					<tr><td colspan="6" class="subtitulo1">Comentarios</td></tr>
  					<tr>
    					  <td style='width:100%'>    					        
    					    <textarea rows='5' class='textbox' style='width:100%' name='comentario1' > <%= planViaje.getComentario1() %> </textarea>
    					  </td>
			      </tr>
		        </table>
   		<!-- fin COMENTARIOS -->
   		
   		
			</td>
		</tr>         
     </table>
	</td>
  </tr>         
  
</table>


    <!-- OCULTOS  -->
    
     <input type='hidden' name='Opcion'   >
     <input type='hidden' name='rdOpcion'  value = '<%= request.getParameter("rdOpcion")%>' >
     <input type='hidden' name='hRetorno' >
     <input type='hidden' name='statusPlanilla'    value='<%=planViaje.getEstado()%>'>  
     <input type='hidden' name='oriOriginal'       value='<%=planViaje.getOrigenCode()%>'>
     <input type='hidden' name='viaOriginal'       value='<%=planViaje.getRuta()%>'>  
     <input type='hidden' name='pcOriginal'        value='<%=planViaje.getPCTrafico()%>'>
     <input type='hidden' name='zonaOriginal'      value='<%=planViaje.getZona()%>'>
     <input type='hidden' name='zonaNameOriginal'  value='<%=planViaje.getNombreZona()%>'>
     
     
     
     <!-- BOTONES -->
     
	 <br>
	 <center>
<% if( model.PlanViajeSvc.getBtnSave().equals(""))  { %>   
<img src='<%=BASEURL%>/images/botones/aceptar.gif'    style=" cursor:'hand'"  title='Crear....'     name='i_crear'       onclick="validarFormPlanViaje(formulario, 'Crear')"                               onMouseOver='botonOver(this);' onMouseOut='botonOut(this);'> 
              <%}%>	  
              <% if( model.PlanViajeSvc.getBtnModify().equals("")){ %>  
                   <img src='<%=BASEURL%>/images/botones/modificar.gif'  style=" cursor:'hand'"  title='Modificar....' name='i_modificar'   onclick="validarFormPlanViaje(formulario, 'Modificar')"                           onMouseOver='botonOver(this);' onMouseOut='botonOut(this);'> 
              <%}%>
              <% if( model.PlanViajeSvc.getBtndelete().equals("")){ %>  
                 <img src='<%=BASEURL%>/images/botones/eliminar.gif'     style=" cursor:'hand'"  title='Eliminar....'  name='i_eliminar'    onclick="validarFormPlanViaje(formulario, 'Eliminar')"                            onMouseOver='botonOver(this);' onMouseOut='botonOut(this);'> 
              <%}%>

              <img src='<%=BASEURL%>/images/botones/imprimir.gif'        style="cursor:'hand'"    title='Impripir....'  name='i_regresar'    onclick="NuevaVentana('<%=CONTROLLER%>?estado=PdfPlan&accion=Viaje','PlanViaje',900,250,100,50);"  onMouseOver='botonOver(this);' onMouseOut='botonOut(this);'>
              <img src='<%=BASEURL%>/images/botones/regresar.gif'        style="cursor:'hand'"    title='Regresar....'  name='i_regresar'    onclick="window.location.href='<%=BASEURL%>/jsp/trafico/planviaje/planViaje.jsp'" onMouseOver='botonOver(this);' onMouseOut='botonOut(this);'>
              <img src='<%=BASEURL%>/images/botones/salir.gif'           style="cursor:'hand'"    title='Salir...'      name='i_salir'       onclick='parent.close();'                                                         onMouseOver='botonOver(this);' onMouseOut='botonOut(this);'>
	 
	 </center>
</form>  
  


<script>  formulario.productos.focus();  </script>



   <!-- mensajes -->
    <% if(msj!=null &&  !msj.equals("") ){%>        
         <table border="2" align="center">
          <tr>
            <td><table width="100%"  border="1" align="center"  bordercolor="#F7F5F4" bgcolor="#FFFFFF">
              <tr>
                <td width="500" align="center" class="mensajes"><%=msj%></td>
                <td width="29" background="<%=BASEURL%>/images/cuadronaranja.JPG">&nbsp;</td>
                <td width="58">&nbsp; </td>
              </tr>
            </table></td>
          </tr>
        </table>
        <br><br>
    <%  } %>
    
</div> 
<%=datos[1]%> 
   
<iframe width=188 height=166 name="gToday:normal:agenda.js:gfPop:plugins_24.js" id="gToday:normal:agenda.js:gfPop:plugins_24.js" src="<%=BASEURL%>/js/Calendario/ipopeng.htm" scrolling="no" frameborder="0" style="visibility:visible; z-index:999; position:absolute; left:-500px; top:0px;"> </iframe>
   
</body>
</html>
