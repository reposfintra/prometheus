  <!--
     - Author(s)       :      FERNEL VILLACOB DIAZ
     - Date            :      07/10/2005  
     - Copyright Notice:      TRANSPORTES SANCHEZ POLO S.A.
  -->
 <%--   - @(#)  
     - Description:  Vista que maneja plan de viajes
 --%>

<%@page session="true"%> 
<%@page errorPage="/error/ErrorPage.jsp"%>
<%@include file="/WEB-INF/InitModel.jsp"%>

<html>
<head>
    <title>Plan de Viaje</title>
    <link href="<%= BASEURL %>/css/estilostsp.css" rel="stylesheet" type="text/css">
    <script language="JavaScript" type="text/javascript" src="<%=BASEURL%>/js/script.js"></script>
    <script type='text/javascript' src="<%= BASEURL %>/js/boton.js"></script>
    <link href="/css/estilostsp.css" rel="stylesheet" type="text/css">
</head>
<body>



<%  String path    = application.getRealPath(model.menuService.getRealUrl(request.getRequestURI()));
    String datos[] = model.menuService.getContenidoMenu(BASEURL,request.getRequestURI(),path);    %>

<%String numpla=(request.getParameter("numpla")!=null?request.getParameter("numpla"):""); %>
 
<div id="capaSuperior" style="position:absolute; width:100%; height:100px; z-index:0; left: 0px; top: 0px;">
<jsp:include page="/WEB-INF/toptsp.jsp?encabezado=Plan de Viaje"/>
</div>

<div id="capaCentral" style="position:absolute; width:100%; height:85%; z-index:0; left: 0px; top: 100px; overflow: scroll;"> 
 <form action='<%=CONTROLLER%>?estado=PlanViaje&accion=Buscar' method='post' name='formulario' onsubmit='return false;'>
 
   <table width="370" border="2" align="center">
   <tr>
      <td>         

           <table width='100%' align='center' class='tablaInferior'>
           
              <tr>
                <td class="barratitulo" >
                   <table cellpadding='0' cellspacing='0' width='100%'>
                     <tr class="fila">
                      <td width="32%" align="left" class="subtitulo1">&nbsp;Plan de Viajes</td>
                      <td width="68%" align="left"><img src="<%= BASEURL %>/images/titulo.gif" width="32" height="20"  align="left"><%=datos[0]%></td>
                    </tr>
                   </table>   
				</td>
              </tr>

              <tr class="letrafila">
                   <td width='40%' class="fila" >
                        &nbsp Distrito: &nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp
                        <select name='txtDistrito' class="textbox">
                           <option value='FINV'>FINV</option>
                </select>                </td>
                   
              </tr>

              <tr class="letrafila">
                   <td width='100%' class="fila" >
                        &nbsp No Planilla: &nbsp&nbsp&nbsp
                        <input type='text' name='txtPlanilla' value="<%=numpla%>" maxlength="6" size='15' class="textbox" title='Digite el n�mero de planilla'>
                        <a class="Simulacion_Hiper" href="<%=CONTROLLER%>?estado=Planillas&accion=SinPlanViaje">Planillas sin Plan de Viaje</a>                </td>
              </tr>
              
           
               <tr> 
                 <TD class="fila" >
                         <table cellpadding='0' cellspacing='0' width='100%'>
                                <tr class="fila"><td ><input type='radio' name='rdOpcion' value='SAVE' checked='checked'>   </td><td >Crear     <td></td>
                                    <td ><input type='radio' name='rdOpcion' value='SEARCH'>                   </td><td >Consultar <td></td>
                                    <td ><input type='radio' name='rdOpcion' value='MODIFY'>                   </td><td >Modificar <td></td>
                                    <td ><input type='radio' name='rdOpcion' value='DELETE'>                   </td><td >Eliminar  <td></td>
                                </tr>
                         </table>                 
                 </td>
              </tr>
              
        </table>
      </td>
   </tr>
   </table>   
   <br>
   
   
   <!--botones -->
   
   <center>
   <img src='<%=BASEURL%>/images/botones/buscar.gif'   style='cursor:hand' title='Busqueda...' name='i_buscar'     onclick='planViaje_validarPlanilla(formulario)' onMouseOver='botonOver(this);' onMouseOut='botonOut(this);'>
   <img src='<%=BASEURL%>/images/botones/cancelar.gif' style='cursor:hand' title='Cancelar'    name='i_cancelar'   onclick="formulario.reset();formulario.txtPlanilla.value=''"                              onMouseOver='botonOver(this);' onMouseOut='botonOut(this);'>
   <img src='<%=BASEURL%>/images/botones/salir.gif'    style='cursor:hand' title='Salir...'    name='i_salir'      onclick='parent.close();'                                  onMouseOver='botonOver(this);' onMouseOut='botonOut(this);'>
   </center>
   
   
   
 </form>
 
 <!-- habilitamos el focus -->
  <script>formulario.txtPlanilla.focus();</script>
   
 
   
   <!-- mensajes -->
    <% String   msj  = request.getParameter("msj");
       if(msj!=null &&  !msj.equals("") ){%>        
         <table border="2" align="center">
          <tr>
            <td><table width="100%"  border="1" align="center"  bordercolor="#F7F5F4" bgcolor="#FFFFFF">
              <tr>
                <td width="260" align="center" class="mensajes"><%=msj%></td>
                <td width="29" background="<%=BASEURL%>/images/cuadronaranja.JPG">&nbsp;</td>
                <td width="58">&nbsp;</td>
              </tr>
            </table></td>
          </tr>
        </table>
    <%  } %>
    
    
    
</div>  
<%=datos[1]%> 


</body>
</html>
