  <!--
     - Author(s)       :      FERNEL VILLACOB DIAZ
     - Date            :      07/10/2005  
     - Copyright Notice:      TRANSPORTES SANCHEZ POLO S.A.
  -->
 <%--   - @(#)  
     - Description:  Vista que maneja plan de viajes
 --%>
 <%@ include file="/WEB-INF/InitModel.jsp"%>
<html>
<head>
        <title>Ayuda Plan de Viaje</title>
        <META http-equiv=Content-Type content="text/html; charset=windows-1252">
        <link href="<%=BASEURL%>/css/estilostsp.css" rel="stylesheet" type="text/css"> 
        <script type='text/javascript' src="<%= BASEURL %>/js/boton.js"></script>  
</head>
<body>
<center>

<table width="696"  border="2" align="center">
  <tr>
    <td width="811" >
    
      <table width="100%" border="0" align="center">
            <tr  class="subtitulo">
              <tr class="subtitulo" ><td height="20" colspan='2'><div align="center">MANUAL DE PLAN DE VIAJE</div></td></tr>
            </tr>
           
            <tr>
                 <td width="172" class="fila"> DISTRITO</td>
                 <td width="502"  class="ayudaHtmlTexto">Determina el distrito de la planilla, se carga por default el distrito del usuario.</td>
            </tr>
            
            <tr>
                  <td width="172" class="fila"> PLANILLA</td>
                 <td width="502"  class="ayudaHtmlTexto">Deber� digitar el n�mero de la planilla. Valor alfanum�rico.</td>
            </tr>

            <tr>
                  <td width="172" class="fila"> OPCI�N</td>
                  <td width="502"  class="ayudaHtmlTexto">Opci�n deseada para el plan de viaje: Crear, Modificar, Eliminar, Consultar.</td>
            </tr>

       </table>
    </td>
  </tr>
</table>


<P>
  <img src="<%=BASEURL%>/images/botones/salir.gif"      name="exit"    height="21"  title='Salir'                onClick="parent.close();"                 onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" style="cursor:hand">          
</P>


</body>
</html>
