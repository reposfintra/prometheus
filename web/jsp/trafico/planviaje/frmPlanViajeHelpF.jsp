  <!--
     - Author(s)       :      FERNEL VILLACOB DIAZ
     - Date            :      07/10/2005  
     - Copyright Notice:      TRANSPORTES SANCHEZ POLO S.A.
  -->
 <%--   - @(#)  
     - Description:  Vista que maneja plan de viajes
 --%>
 <%@ include file="/WEB-INF/InitModel.jsp"%>
<html>
<head>
     <title>Ayuda Plan de Viaje</title>
     <META http-equiv=Content-Type content="text/html; charset=windows-1252">
     <link href="<%=BASEURL%>/css/estilostsp.css" rel="stylesheet" type="text/css"> 
     <script type='text/javascript' src="<%= BASEURL %>/js/boton.js"></script>  
</head>
<body>

<% String BASEIMG = BASEURL +"/images/ayuda/trafico/planviaje/"; %>
<center>    
    
<table width="100%"  border="2" align="center">
    <tr>
      <td >
 
            <table width='99%' align="center" cellpadding='0' cellspacing='0'>
                 <tr class="subtitulo" ><td height="20"><div align="center">MANUAL DE PLAN DE VIAJE</div></td></tr>
                 <tr class="subtitulo1"> <td>Descripci&oacute;n del funcionamiento </td> </tr>
                 
                 
                 <tr>
                     <td  class="ayudaHtmlTexto" >
                       Este formulario le permite crear, consultar, eliminar y/o actualizar el plan de viaje. El formulario se divide en
                       sesiones tales como:
                       <ul>
                             <li> Datos del viaje.         </li>
                             <li> Datos del conductor.     </li>
                             <li> Equipos de comunicaci�n. </li>
                             <li> Datos de un familiar.    </li>
                             <li> Datos del propietario.   </li>
                             <li> Ubicaciones del plan de viaje. </li>   
                             <li> Comentario.                    </li>
                       </ul>    
                       
                       
                     </td>
                </tr>   
                    
   
                <tr>
                     <td  class="ayudaHtmlTexto" >
                        <b>Datos del viaje:</b><br>
                        Se muestra informaci�n relacionada a la planilla tal como lo indica la figura 1.
                     </td>
                </tr>   
                
                <tr>
                    <td  align="center" > 
                         <br><br>
                         <img  src="<%= BASEIMG%>Datosviaje.JPG" >                  
                         <br>
                         <strong>Figura 1</strong>
                         <br><br><br><br>
                     </td>
                </tr>
                 
                
                <tr>
                     <td  class="ayudaHtmlTexto" >
                          <ul>
                             <li> <b>A:</b> El n�mero de la planilla a la cual desea crear el plan de viaje. </li>
                             <li> <b>B:</b> Fecha de despacho de la planilla. </li>
                             <li> <b>C:</b> Destinatario de la planilla.      </li>
                             <li> <b>D:</b> Producto  que se transporta, debe ser digitado.           </li>
                             <li> <b>E:</b> Placa de los contenedores que tiene asociada la  planilla.</li>
                             <li> <b>F:</b> Ruta de la planilla.              </li>
                             <li> <b>G:</b> Pr�ximo p.c. de la planilla.      </li>
                             <li> <b>H:</b> N�mero de la caravana a la cual pertenece la planilla. </li>
                             <li> <b>I:</b> Usuario de creaci�n del plan de viaje.                 </li>
                             <li> <b>J:</b> Distrito de la planilla.          </li>
                             <li> <b>K:</b> Origen de la planilla.            </li>
                             <li> <b>L:</b> Placa de la planilla.             </li>
                             <li> <b>M:</b> Destino de la planilla.           </li>
                             <li> <b>N:</b> Tipo de carga de la remesa asociada a la planilla.     </li>
                             <li> <b>O:</b> Placa que transporta la carga (trailer).     </li>
                             <li> <b>P:</b> Placa del trailer.                           </li>
                             <li> <b>Q:</b> Fecha en la cual se crea el plan de viaje.   </li>
                             <li> <b>R:</b> Zona a la cual pertenece el p.c.             </li>
                             <li> <b>S:</b> Placa de escolta  de la planilla.            </li>
                             <li> <b>T:</b> Determina si la planilla tiene retorno o no. </li>
                             <li> <b>U:</b> Fecha de creaci�n del plan de viaje.         </li>
                             <li> <b>V:</b> Link al programa de Caravana.                </li>
                             <li> <b>W:</b> Link al programa de Escolta.                 </li>
                             <li> <b>X:</b> Link para cambiar ruta y pr�ximo p.c. a la planilla. </li>
                          </ul>  
                    </td>
                </tr>
                
                
                <tr>
                     <td  class="ayudaHtmlTexto" >
                         Para cambiar y/o escojer un nuevo pr�ximo  p.c. de la ruta cargada en el lista, deber� realizar click en el link de ruta ( X ), le aparecer�
                         un listado de los p.c. que conforman esa ruta, tal como lo indica la figura 2.
                         En el cual deber� realizar click en el nuevo p.c. deseado, le aparecer� una ventana de confirmaci�n como lo indica la figura 3.
                     </td>
                </tr>  
                
                 <tr>
                    <td  align="center" > 
                         <br><br>
                         <img  src="<%= BASEIMG%>pc.JPG" >                  
                         <br>
                         <strong>Figura 2</strong>
                         <br><br>
                     </td>
                </tr>
                
                <tr>
                    <td  align="center" > 
                         <img  src="<%= BASEIMG%>ConfirmacionPc.JPG" >                  
                         <br>
                         <strong>Figura 3</strong>
                         <br><br>
                     </td>
                </tr>
                
                
                <tr>
                     <td  class="ayudaHtmlTexto" >
                        Una vez seleccionado el pr�ximo p.c. se actualizar� el formulario en los campos  de pr�ximo p.c.(G) y zona del p.c.(R).
                     </td>
                </tr>  
                
                
                
                
                <tr>
                     <td  class="ayudaHtmlTexto" >
                        <br><br><br><br>
                        <b>Datos del conductor:</b><br>
                        Se muestra informaci�n relacionada al conductor del viaje, tal como lo indica la figura 4.
                     </td>
                </tr> 
                <tr>
                    <td  align="center" > 
                         <br><br>
                         <img  src="<%= BASEIMG%>DatosConductor.JPG" >                  
                         <br>
                         <strong>Figura 4</strong>
                         <br><br><br><br>
                     </td>
                </tr>
                
                
                
                
                
                
                <tr>
                     <td  class="ayudaHtmlTexto" >
                        <br><br><br><br>
                        <b>Equipos de Comunicaci�n:</b><br>
                        Se muestra un formulario donde se deber� digitar n�meros de equipos de comunicaci�n 
                        para contactar al conductor del viaje,
                        tal como lo indica la figura 5.
                     </td>
                </tr>   
                <tr>
                    <td  align="center" > 
                         <br><br>
                         <img  src="<%= BASEIMG%>EquiposComunicacion.JPG" >                  
                         <br>
                         <strong>Figura 5</strong>
                         <br><br><br><br>
                     </td>
                </tr>
                
                
                
                
                
                <tr>
                     <td  class="ayudaHtmlTexto" >
                        <br><br><br><br>
                        <b>Datos de un familiar:</b><br>
                        Se muestra un formulario donde se deber� digitar nombre y n�mero de tel�fono de un familiar del conductor,
                        tal como lo indica la figura 6.
                     </td>
                </tr>   
                <tr>
                    <td  align="center" > 
                         <br><br>
                         <img  src="<%= BASEIMG%>DatosFamiliar.JPG" >                  
                         <br>
                         <strong>Figura 6</strong>
                         <br><br><br><br>
                     </td>
                </tr>
                
                
                
                <tr>
                     <td  class="ayudaHtmlTexto" >
                        <br><br><br><br>
                        <b>Datos del propietario:</b><br>
                        Se muestra informaci�n relacionada al propietario del vehiculo del viaje, 
                        tal como lo indica la figura 7.
                     </td>
                </tr>   
                <tr>
                    <td  align="center" > 
                         <br><br>
                         <img  src="<%= BASEIMG%>DatosPropietario.JPG" >                  
                         <br>
                         <strong>Figura 7</strong>
                         <br><br><br><br>
                     </td>
                </tr>
                
                
                
                
                
                <tr>
                     <td  class="ayudaHtmlTexto" >
                        <br><br><br><br>
                        <b>Ubicaciones del plan de viaje:</b><br>
                        Se establecen las ubicaciones de la ruta y las del plan de viaje, tal como lo indica la figura 8.
                        Se manejan cuatro(4) tipos de ubicaciones tales como:
                        
                          <ul>
                             <li> Restaurantes  </li>
                             <li> Pernoctaderos </li>
                             <li> Parqueaderos  </li>
                             <li> Estaciones de combustibles </li>
                          </ul> 
                        
                     </td>
                </tr> 
                <tr>
                    <td  align="center" > 
                         <br><br>
                         <img  src="<%= BASEIMG%>Ubicacion.JPG" >                  
                         <br>
                         <strong>Figura 8</strong>
                         <br><br><br><br>
                     </td>
                </tr>
                
                <tr>
                     <td  class="ayudaHtmlTexto" >
                        Para agregar y/o quitar ubicaciones de la ruta al plan de viaje, deber� seleccionarla y hacer uso de los botones de agregar y quitar ubicaciones respectivamente,
                        tal como lo indica la figura 9.
                     </td>
                </tr>  
                
                
                 <tr>
                    <td  align="center" > 
                         <br><br>
                         <img  src="<%= BASEIMG%>BotonesSeleccion.JPG" >                  
                         <br>
                         <strong>Figura 9</strong>
                         <br><br><br><br>
                     </td>
                </tr> 
                
                
                
                
                <tr>
                     <td  class="ayudaHtmlTexto" >
                        <br><br><br><br>
                        <b>Comentario:</b><br>
                        Se agrega un comentario adicional al plan de viaje, tal como lo indica la figura 10. 
                     </td>
                </tr> 
                <tr>
                    <td  align="center" > 
                         <br><br>
                         <img  src="<%= BASEIMG%>Comentario.JPG" >                  
                         <br>
                         <strong>Figura 10</strong>
                         <br><br><br><br>
                     </td>
                </tr>
                
                
                
                
                <tr>
                     <td  class="ayudaHtmlTexto" >
                        Dependiendo la opci�n seleccionada, asi se le habilitar� el bot�n de la opcion tales como:
                          <ul>
                             <li> Opci�n Crear: Habilitar� el bot�n crear                                    </li>
                             <li> Opci�n Consultar: Habilitar� el bot�n imprimir, este siempre estar� activo </li>
                             <li> Opci�n Modificar: Habilitar� el bot�n modificar                            </li>
                             <li> Opci�n Eliminar: Habilitar� el bot�n eliminar                              </li>
                          </ul>  
                     </td>
                </tr> 
                
                
         </table>
            
      </td>
  </tr>
</table>

<P>
  <img src="<%=BASEURL%>/images/botones/salir.gif"      name="exit"    height="21"  title='Salir'                onClick="parent.close();"                 onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" style="cursor:hand">          
</P>

</body>
</html>
