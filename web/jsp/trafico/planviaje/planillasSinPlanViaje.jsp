  <!--
     - Author(s)       :      Osvaldo P�rez Ferrer
     - Date            :      13 de Junio de 2006
     - Copyright Notice:      TRANSPORTES SANCHEZ POLO S.A.
  -->
 <%--   - @(#)  
     - Description:  Listado de Planillas sin plan de viaje, por agencia
 --%>


<%@page session="true"%>
<%@page errorPage="../error/ErrorPage.jsp"%>
<%@page import="com.tsp.operation.model.beans.*, java.util.*, java.text.*"%>
<%@ include file="/WEB-INF/InitModel.jsp"%>
<%@ taglib uri="http://java.sun.com/jstl/core" prefix="c" %>
<%@ taglib uri="http://jakarta.apache.org/taglibs/input-0.90" prefix="input" %>
<html>
<head>
    <title>Planillas sin Plan de Viaje</title>
    <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">    
    <script language="javascript" src="<%=BASEURL%>/js/general.js"></script>
    <script type='text/javascript' src="<%=BASEURL%>/js/boton.js"></script>
    <script type='text/javascript' src="<%= BASEURL %>/js/reporte.js"></script>
    <link href="<%=BASEURL%>/css/estilostsp.css" rel="stylesheet" type="text/css">    

</head>
<%
    String path = application.getRealPath(model.menuService.getRealUrl(request.getRequestURI()));
    String datos[] = model.menuService.getContenidoMenu(BASEURL,request.getRequestURI(),path);
    String accion = CONTROLLER+"?estado=Consulta&accion=Inventario";        
    String codCliente = request.getParameter( "cliente" );
    Vector planillas = (Vector)request.getAttribute( "planillas" );           
    String mensaje = (String) request.getAttribute("mensaje");        
    String agencia = (String) request.getAttribute("nagencia");        
    
%>
<body>
    <div id="capaSuperior" style="position:absolute; width:100%; height:100px; z-index:0; left: 0px; top: 0px;">
        <jsp:include page="/WEB-INF/toptsp.jsp?encabezado=Planillas sin Plan de Viaje"/>
    </div>    
    <div id="capaCentral" style="position:absolute; width:100%; height:85%; z-index:0; left: 0px; top: 100px; overflow: scroll;">                
        <br>
        
        <div id="working" class="letrasFrame" align="center" ><br></div>
        <br>
       
        <div id="listaPlanillas" align="center">
           
             <%                              
                if(planillas != null && planillas.size()>0){%>
            <table width="700"  border="2" align="center">
            <tr>
            <td width="690">
                <table width="690" class="tablaInferior">
                    <tr>
                        <td height="22" colspan=2 class="subtitulo1">Planillas sin Plan de Viaje de agencia <%=agencia%></td>
                        <td width="232" class="barratitulo"><img src="<%=BASEURL%>/images/titulo.gif" width="32" height="20"></td>
                    </tr>
                </table>                                   
                <table width="690" border="1" align="center" bordercolor="#999999" bgcolor="#F7F5F4">    
                <tr class="tblTitulo">
                    <td >Fecha</td>
                    <td >N&uacute;mero</td>
                    <td >Placa</td>
                    <td >Origen</td>
                    <td width="123" >Destino</td>				
                    <td width="90" >Conductor</td>										
                    <td width="91" >Despachador</td>							
                </tr>
                    <%                                    
                    for(int i=0; i<planillas.size(); i++){
                        Planilla p = (Planilla) planillas.elementAt(i);                                                                                                                                                                       
                        %>
                <tr class="<%=i%2==0?"filagris":"filaazul"%>" onMouseOver='cambiarColorMouse(this)' 
                    style="cursor:hand" 
                    onclick="window.open('<%=BASEURL%>/jsp/trafico/planviaje/planViaje.jsp?numpla=<%=p.getNumpla()%>','','MENUBAR=NO,resizable=yes,width=700,heigth=550');
                    window.close();" title="Click para asignar Plan de Viaje">    
                <td width="75" class="bordereporte"><%=p.getFecdsp()%>&nbsp;</td>
                <td width="65" class="bordereporte">
                <span class="Letras"><%=p.getNumpla()%>&nbsp;<strong>

                </strong></span></td>
                <td width="55" class="bordereporte"><%=p.getPlaveh()%>&nbsp;</td>

                <td width="139" class="bordereporte"><%=p.getOripla()%>&nbsp;</td>
						
                <td width="123" class="bordereporte"><%=p.getDespla()%>&nbsp;</td>
						
                <td width="90" class="bordereporte"><%=p.getCedcon()%>&nbsp;</td>
						
                <td width="91" class="bordereporte"><%=p.getDespachador()%>&nbsp;</td>
                </tr>            
                
              
            </td>
            </tr>            
                <%}%>
                      </table>                        
            </table>                        
            <%
              }      
                    else{
                %>                                           
            
            <table border="2" align="center">
                <tr>
                    <td><table width="100%"  border="1" align="center"  bordercolor="#F7F5F4" bgcolor="#FFFFFF">
                    <tr>
                    <td width="229" align="center" class="mensajes">No existen registros</td>
                    <td width="29" background="<%=BASEURL%>/images/cuadronaranja.JPG">&nbsp;</td>
                    <td width="58">&nbsp;</td>
                </tr>
                </table></td>
                </tr>
            </table>                   
          <%}%>
            <br>
            <img src="<%=BASEURL%>/images/botones/salir.gif" name="c_salir" style="cursor:pointer" onClick="window.close();" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" align="absmiddle">   </td>
            </tr>
            
        </div>
    </div>
    
</body>
</html>
