<!--  
     - Author(s)       :      FERNEL VILLACOB DIAZ
     - Date            :      07/10/2005  
     - Copyright Notice:      TRANSPORTES SANCHEZ POLO S.A.
  -->
 <%--   - @(#)  
     - Description: Listado de Puestos de Control de Una Via
 --%>

<%@ page session   ="true"%> 
<%@ page errorPage ="/error/ErrorPage.jsp"%>
<%@ page import    ="java.util.*" %>
<%@ page import    ="com.tsp.util.*" %>
<%@ page import    ="com.tsp.operation.model.beans.*" %>
<%@ include file   ="/WEB-INF/InitModel.jsp"%>


<html>
<head>

  <title>Lista PC para una Via</title>
  <link href="<%= BASEURL %>/css/estilostsp.css" rel="stylesheet" type="text/css"> 
  <link href="/css/estilostsp.css" rel="stylesheet" type="text/css">
  <script language="JavaScript" type="text/javascript" src="<%=BASEURL%>/js/script.js"></script> 
  <script type='text/javascript' src="<%= BASEURL %>/js/boton.js"></script>

  <script>   
     function loadPC(pc,zona, sw){
       parent.opener.document.formulario.proximopc.value = pc;         
       parent.opener.document.formulario.zona.value      = zona; 
       if(sw==1){
         alert('Selecci�n de Proximo PC \n Proximo puesto de Control :' + pc + '\n Zona : ' + zona);
       }
     }   
  </script>  
  
</head>
<body>



<div id="capaSuperior" style="position:absolute; width:100%; height:100px; z-index:0; left: 0px; top: 0px;">
<jsp:include page="/WEB-INF/toptsp.jsp?encabezado=Plan de Viaje"/>
</div>

<div id="capaCentral" style="position:absolute; width:100%; height:85%; z-index:0; left: 0px; top: 110px; overflow: scroll;"> 
<% PlanDeViaje planViaje    = model.PlanViajeSvc.getPlan(); 
   String      viaEscojida  = request.getParameter("ruta"); 
   String      SEPERADOR    = ":";                        %>
              
<center>
<form  method='post' name='formPC'>
  <table width="90%"  border="2" align="center">
    <tr>
      <td><table  width='100%' border='0' cellpadding='0' cellspacing='0' class="tablaInferior"> 
              <tr>
                 <td colspan='2' >
                     <table width='100%'  class="tablaInferior">
                        <tr>
                                <td align="left" width='70%' class="subtitulo1" nowrap> VIA <%=planViaje.getOrigen()%> &nbsp <%=planViaje.getDestino()%> &nbsp <%=viaEscojida%></td>
                                <td align="left" width='*'  class="barratitulo"><img src="<%=BASEURL%>/images/titulo.gif"></td>
                        </tr>
                        <tr>
                          <td colspan="2" align="left" nowrap class="letra">**ESCOJA PROX. PUESTO CONTROL</td>
                        </tr>
                   </table>
                </td>
              </tr>  
               <%List lisVia = planViaje.getVias();
                 for(int j=0; j<lisVia.size();j++){
                     Via  via = (Via) lisVia.get(j);
                     if( viaEscojida.equals( via.getCodigo() ) ){
                         List puestos  = via.getPuestos(); 
                         for(int i=0; i<puestos.size();i++){
                            String   dato   = (String) puestos.get(i);
                            String[] vec    = dato.split(SEPERADOR);      
                            String   pc     = vec[0]  + SEPERADOR + vec[1];
                            String   zona   = vec[2]  + SEPERADOR + vec[3];
                            if(i==1){%>                            
                               <script> loadPC('<%=pc%>','<%=zona%>','0');</script>                                
                           <%}%>
                            <TR class="fila" >
                                  <TD><%=i+1%></TD>
                                  <TD> <a href='#'  onclick="javascript: loadPC('<%=pc%>','<%=zona%>','1'); "> <%=pc%>  </a> </TD>
                            </TR>  
                        <%}
                     }
                 }
                %>
          </table></td>
    </tr>
  </table>  
  <br>
  <img src='<%=BASEURL%>/images/botones/salir.gif'      style='cursor:hand'   title='Salir...'      name='i_salir'      onclick='parent.close();'                                            onMouseOver='botonOver(this);' onMouseOut='botonOut(this);'>
	 
</form>
</center>
</div>              
</body>
</html>
