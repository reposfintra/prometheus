  <!--
     - Author(s)       :      FERNEL VILLACOB DIAZ
     - Date            :      07/10/2005  
     - Copyright Notice:      TRANSPORTES SANCHEZ POLO S.A.
  -->
 <%--   - @(#)  
     - Description:  Vista que maneja plan de viajes
 --%>
 <%@ include file="/WEB-INF/InitModel.jsp"%>
<html>
<head>
        <title>Ayuda Plan de Viaje</title>
        <META http-equiv=Content-Type content="text/html; charset=windows-1252">
        <link href="<%=BASEURL%>/css/estilostsp.css" rel="stylesheet" type="text/css"> 
        <script type='text/javascript' src="<%= BASEURL %>/js/boton.js"></script>  
</head>
<body>
<center>


<table width="696"  border="2" align="center">
  <tr>
    <td width="811" >
    
        <table width="100%" border="0" align="center">
            <tr  class="subtitulo">
              <tr class="subtitulo" ><td height="20" colspan='2'><div align="center">MANUAL DE PLAN DE VIAJE</div></td></tr>
            </tr>
            
            
            <tr>
                 <td width="172" class="fila"> PRODUCTO</td>
                 <td width="502"  class="ayudaHtmlTexto">Determina el producto que se transporta. Campo alfanum�rico</td>
            </tr>
            
            <tr>
                 <td width="172" class="fila"> RUTA</td>
                 <td width="502"  class="ayudaHtmlTexto">Se establece la ruta de la planilla. Deber� seleccionar de la lista</td>
            </tr>
            
            <tr>
                 <td width="172" class="fila"> FECHA PLAN DE VIAJE</td>
                 <td width="502"  class="ayudaHtmlTexto">Se establece la fecha del plan de viaje. En formato  YYYY-MM-DD HH:MM.SS</td>
            </tr>
            
            
            <tr>
                 <td width="172" class="fila">RADIO</td>
                 <td width="502"  class="ayudaHtmlTexto">Se  establece el n�mero del radio.</td>
            </tr>
             <tr>
                 <td width="172" class="fila">CELULAR</td>
                 <td width="502"  class="ayudaHtmlTexto">Se  establece el n�mero del celular.</td>
            </tr>
             <tr>
                 <td width="172" class="fila">AVANTEL</td>
                 <td width="502"  class="ayudaHtmlTexto">Se  establece el n�mero del avantel.</td>
            </tr>
             <tr>
                 <td width="172" class="fila">TELEFONO</td>
                 <td width="502"  class="ayudaHtmlTexto">Se  establece el n�mero del tel�fono fijo.</td>
            </tr> <tr>
                 <td width="172" class="fila">CAZADOR</td>
                 <td width="502"  class="ayudaHtmlTexto">Se  establece el n�mero del cazador.</td>
            </tr>
             <tr>
                 <td width="172" class="fila">MOVIL</td>
                 <td width="502"  class="ayudaHtmlTexto">Se  establece el n�mero del movil.</td>
            </tr>
             <tr>
                 <td width="172" class="fila">OTRO</td>
                 <td width="502"  class="ayudaHtmlTexto">Se  establece otro n�mero donde se pueda contactar.</td>
            </tr>
            
             <tr>
                 <td width="172" class="fila">NOMBRE</td>
                 <td width="502"  class="ayudaHtmlTexto"> Se  establece el nombre del familiar. Campo alfanum�rico.</td>
            </tr>
             <tr>
                 <td width="172" class="fila">TELEFONO</td>
                 <td width="502"  class="ayudaHtmlTexto">Se  establece el n�mero del tel�fono del familiar.</td>
            </tr>
             <tr>
                 <td width="172" class="fila">COMENTARIO</td>
                 <td width="502"  class="ayudaHtmlTexto">Se  establece un comentario adicional al plan de viaje.</td>
            </tr>
            
            
            
            
            
            
            
            
            
            
            
            
        </table>
        
    </td>
  </tr>
</table>


<P>
  <img src="<%=BASEURL%>/images/botones/salir.gif"      name="exit"    height="21"  title='Salir'                onClick="parent.close();"                 onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" style="cursor:hand">          
</P>

            

</body>
</html>
