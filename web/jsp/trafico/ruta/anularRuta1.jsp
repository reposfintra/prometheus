<%@page contentType="text/html"%>
<%@page session="true"%>
<%@page import="java.util.*"%>
<%@page import="com.tsp.operation.model.*"%>
<%@page import="com.tsp.operation.model.beans.*"%>
<%@page import="com.tsp.util.*"%>
<%@include file="/WEB-INF/InitModel.jsp"%>
<%@ taglib uri="http://jakarta.apache.org/taglibs/input-0.90" prefix="input" %>

<html>
<head>
<title>Rutas</title>
<link href="<%= BASEURL %>/css/estilo.css" rel='stylesheet'>
<link href="../css/estilo.css" rel="stylesheet" type="text/css">
</head>
<script type='text/javascript' src="<%= BASEURL %>/js/validar.js"></script>
<body <%if(request.getParameter("reload")!=null){%>onLoad="window.opener.location.reload();"<%}%>>
<%	String ncia = "", norigen = "", origen = "", ndestino = "", destino = "", ccia="",secuencia="",via="", vian = "", viat = "", duracion="", tiempo= ""; 
	
	
	if(request.getParameter("lista")!=null){
		Ruta ruta = model.rutaService.getRuta();
		ccia = ruta.getCia();		
		origen = ruta.getOrigen();
		norigen = ruta.getPais_o()+"-"+ruta.getNorigen();
		destino = ruta.getDestino();
		ndestino = ruta.getPais_d()+"-"+ruta.getNdestino();
		secuencia = ruta.getSecuencia();
		via = ruta.getVia();
		vian = ruta.getNvia(); 
		viat = ruta.getTvia();
		tiempo = ""+ruta.getTiempo();
		duracion = ""+ruta.getDuracion();
	}
	else{
		ccia = request.getParameter("c_cia");
		origen = request.getParameter("c_origen");
		norigen = request.getParameter("c_norigen");
		destino = request.getParameter("c_destino");
		ndestino = request.getParameter("c_ndestino");
		secuencia = request.getParameter("c_secuencia");
		via = request.getParameter("c_via");
		vian = request.getParameter("c_vian");
		viat = request.getParameter("c_viat");
		tiempo = request.getParameter("c_tiempo");
		duracion = request.getParameter("c_duracion");
		
	}
%>

<FORM name='forma' id='forma' method='POST' action="<%=CONTROLLER%>?estado=Ruta&accion=Anular&pagina=anularRuta.jsp&modif=ok" onSubmit="return validarRuta();">
  <table width="416" border="1" align="center" cellpadding="3" cellspacing="2" bordercolor="#CCCCCC" class="letra">
    <tr class="titulo" align="center"> 
      <td colspan="2">Modificar/Anular Ruta
    </tr>
    <tr class="subtitulos"> 
      <td height="20" colspan="2" >Informacion
	  </td>
    </tr>
    <tr class="fila" > 
      <td width="274">Distrito
      </td>
      <td width="274" valign="middle"><%=ccia%>
      <input name="c_cia" type="hidden" id="c_cia" value="<%=ccia%>">
</td>
    </tr>
    <tr class="fila" > 
      <td width="274">Origen</td>
      <td width="274" valign="middle"><%=norigen%>
        <input name="c_origen" type="hidden" id="c_origen2" value="<%=origen%>">
      <input name="c_norigen" type="hidden" id="c_norigen" value="<%=norigen%>"></td>
    </tr>
    <tr class="fila" > 
      <td width="274">Destino</td>
      <td width="274" valign="middle"><%=ndestino%>
      <input name="c_destino" type="hidden" id="c_destino" value="<%=destino%>">
      <input name="c_ndestino" type="hidden" id="c_ndestino" value="<%=ndestino%>">
</td>
    </tr>
    <tr class="fila" >
      <td>Secuencia </td>
      <td><%=secuencia%>
      <input name="c_secuencia" type="hidden" id="c_secuencia" value="<%=secuencia%>"></td>
    </tr>
	<tr class="fila" >
      <td>Tiempo</td>
      <td>
      <%=tiempo%>
      <input name="c_tiempo" type="hidden" id="c_tiempo" value="<%=tiempo%>"></td>
    </tr>
	<tr class="fila" >
      <td>Duracion </td>
      <td><%=duracion%>
      <input name="c_duracion" type="hidden" id="c_duracion" value="<%=duracion%>"> </td>
    </tr>
    <tr class="fila">
      <td colspan="2" align="center" ><table width="100%"  border="0" cellspacing="0" cellpadding="0">
          <tr>
            <td width="31%"><textarea name="c_viat" cols="20" rows="5" readonly class="textbox" id="c_viat"><%=viat%></textarea>			</td>
            <td width="2%">&nbsp;</td>
            <td width="67%"><textarea name="c_vian" cols="49" rows="5" readonly class="textbox" id="c_vian"><%=vian%></textarea></td>
          </tr>
        </table>
        <br>
        <input name="c_via" id="c_via" type="hidden" value="<%=via%>">
      <input name="Submit" type="button" class="boton" value="Borrar Tramo" onClick="borrarTramoAnular();"></td>
    </tr>
    <tr class="fila" >
      <td colspan="2" >Tramo&nbsp;&nbsp;
        <%TreeMap tramos= model.tramoService.getLtramos();%>
        <input name="pagina" type="hidden" id="pagina" value="<%=CONTROLLER%>?estado=Cargar&accion=Variosruta&carpeta=jsp/trafico/ruta&pagina=anularRuta.jsp&anular=ok">
        <input:select name="tramo" options="<%=tramos%>" attributesText="id='c_tramo' style='width:100%'; onChange='escogeTramo(this.value)';" /></td>
    </tr>
    <tr class="fila">
		<td colspan="2" align="center">&nbsp;</td>
    </tr>
    <tr class="pie">
      <td colspan="2" align="center"><input  type="submit" class="boton" id= "modificar" name="modificar" value="Modificar">                <input type="button" class="boton" id="anular2" name="anular" value="Anular" onClick="window.location='<%=CONTROLLER%>?estado=Ruta&accion=Anular&pagina=anularRuta.jsp&c_cia=<%=ccia%>&c_origen=<%=origen%>&c_secuencia=<%=secuencia%>&c_destino=<%=destino%>'">
      <input type="button" class="boton" name="regresar" value="Cancelar" onClick='window.close()'></td></tr>
  </table>
</form>
<%if(request.getAttribute("mensaje")!=null){%> 
<span class="mensajes"><%=request.getAttribute("mensaje")%></span>
<%}%>
</body>
</html>
