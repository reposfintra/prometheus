<!--
- Date  : 2005
- Copyrigth Notice : Fintravalores S.A. S.A
-->
<%--
-@(#)
--Descripcion : Pagina JSP, que permite mostrar el listado de las rutas
--%>   
<%@page contentType="text/html"%>
<%@page session="true"%>
<%@page import="java.util.*"%>
<%@page import="com.tsp.operation.model.*"%>
<%@page import="com.tsp.operation.model.beans.*"%>
<%@page import="com.tsp.util.*"%>
<%@include file="/WEB-INF/InitModel.jsp"%>
<%@ taglib uri="http://jsptags.com/tags/navigation/pager" prefix="pg" %>
<html> 
<head>
<title>Rutas</title>
<link href="<%= BASEURL %>/css/estilostsp.css" rel='stylesheet'> 
<link href="../css/estilostsp.css" rel="stylesheet" type="text/css">
<link href="../../../css/estilostsp.css" rel="stylesheet" type="text/css">
<script type='text/javascript' src="<%= BASEURL %>/js/reporte.js"></script>
<script type='text/javascript' src="<%= BASEURL %>/js/boton.js"></script>
</head>
<body onResize="redimensionar()" onLoad="redimensionar();">
<div id="capaSuperior" style="position:absolute; width:100%; height:100px; z-index:0; left: 0px; top: 0px;">
<jsp:include page="/WEB-INF/toptsp.jsp?encabezado=Listado General"/>
</div>

<div id="capaCentral" style="position:absolute; width:100%; height:85%; z-index:0; left: 0px; top: 100px; overflow: scroll;"> 
<%
String style = "simple";
String position = "bottom";
String index = "center";
int maxPageItems = 10;
int maxIndexPages = 10;

Vector rutas = model.rutaService.getRutas();
%>
<table width="919" border="2" align="center">
  <tr>
    <td>
       <table width="100%" height="34" border="0" align="center" class="tablaInferior">
			<tr>
   				<td width="69"  height="24"  class="subtitulo1" colspan="3"><p align="left">Tramos</p></td>
            	<td width="296"  class="barratitulo"><img src="<%=BASEURL%>/images/titulo.gif" width="32" height="20"></td>						
            </tr>
		</table>			
      <table width="99%" border="1" cellspacing="2" cellpadding="3" align="center" bordercolor="#B7D0DC">
	  <tr class="tblTitulo" align="center" height="20">
        <td nowrap width="32%"><div align="center">Origen</div></td>
        <td nowrap width="32%"><div align="center">Destino</div></td>
		<td nowrap width="18%"><div align="center">Descripción</div></td>
        <td nowrap width="10%">Secuencia</td>
        <td nowrap width="54%">Ruta</td>
        <td nowrap width="10%"><div align="center">Distrito</div></td>
      </tr>
      <pg:pager
    items="<%=rutas.size()%>"
    index="<%= index %>"
    maxPageItems="<%= maxPageItems %>"
    maxIndexPages="<%= maxIndexPages %>"
    isOffset="<%= true %>"
    export="offset,currentPageNumber=pageNumber"
    scope="request">
      <% 
     for (int i = offset.intValue(), l = Math.min(i + maxPageItems, rutas.size()); i < l; i++){
        Ruta ruta = (Ruta) rutas.elementAt(i);
		String descrip = ruta.getNcia();
     %>
      <pg:item>
      <tr class="<%=(i % 2 == 0 )?"filaazul":"filagris"%>"  onMouseOver='cambiarColorMouse(this)' bgcolor="#ECE0D8" style="cursor:hand" title="Mostrar informacion del tramo" onClick="window.open('<%=CONTROLLER%>?estado=Buscar&accion=Rutas&carpeta=jsp/trafico/ruta&pagina=anularRuta.jsp&origen=<%=ruta.getOrigen()%>&destino=<%=ruta.getDestino()%>&cia=<%=ruta.getCia()%>&secuencia=<%=ruta.getSecuencia()%>&sw=3&lista=ok&marco=no','','status=yes,scrollbars=no,width=650,height=600,resizable=yes')">
        <td nowrap align="center" class="bordereporte"><%=ruta.getNorigen()%> (<%=ruta.getPais_o().substring(0,3)%>)</td>
        <td align="center" nowrap class="bordereporte"><%=ruta.getNdestino()%> (<%=ruta.getPais_d().substring(0,3)%>)</td>
		<td align="center" nowrap class="bordereporte"><%=descrip%></td>
        <td align="center" nowrap class="bordereporte"><%=ruta.getSecuencia()%></td>
        <td nowrap class="bordereporte" title="<%=ruta.getVia()%>"><%=ruta.getNvia()%></td>
        <td nowrap align="center" class="bordereporte"><%=ruta.getCia()%></td>
      </tr>
      </pg:item>
      <%}%>
      <tr class="pie" align="center">
        <td height="20" colspan="10" nowrap> <pg:index>
          <jsp:include page="../../../WEB-INF/jsp/googlesinimg.jsp" flush="true"/>    
    </pg:index> </td>
      </tr>
      </pg:pager>
    </table></td>
  </tr>
</table>
<br>
<%if(rutas.size()==0) {%>
<table width="411" border="2" align="center">
    <tr>
      <td width="399"><table width="100%"  border="1" align="center"  bordercolor="#F7F5F4" bgcolor="#FFFFFF">
          <tr>
            <td width="278" align="center" class="mensajes">Su busqueda no arrojo resultados. </td>
            <td width="23" background="<%=BASEURL%>/images/cuadronaranja.JPG">&nbsp;</td>
            <td width="70">&nbsp;</td>
          </tr>
      </table></td>
    </tr>
  </table>
<%}%>
<br>
<table width="919" border="0" align="center">
	<tr>		
		<td align="left" width="50%"><img src="<%=BASEURL%>/images/botones/regresar.gif"  name="imgaceptar" onclick="window.location='<%=CONTROLLER%>?estado=Menu&accion=Cargar&carpeta=/jsp/trafico/ruta&pagina=buscarRuta.jsp&marco=no'" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" style="cursor:hand"></td>	  
	</tr>

</table>
</div>
<form name="formReload" id="formReload" method="post" action='<%=CONTROLLER%>?estado=Buscar&accion=Rutas&carpeta=jsp/trafico/ruta&pagina=rutas.jsp&sw=2&marco=no'>
    <input name="c_origen" type="hidden" class="textbox" id="c_origen" value="<%=request.getParameter("c_origen")%>" >
    <input name="c_destino" type="hidden" class="textbox" id="c_destino" value="<%=request.getParameter("c_destino")%>" >
</form>
</body>
</html>
