<!--
- Autor : Ing. Enrique De Lavalle.
- Date  : 27 de Noviembre 2006
- Copyrigth Notice : Fintravalores S.A. S.A
-->
<%--
-@(#)
--Descripcion : Jsp que muestra la ayuda para reporte de Oc de Viajes Vacios con anticipo sin reporte 
--%>
<%@page session="true"%> 
<%@page errorPage="/error/ErrorPage.jsp"%>
<%@page import="com.tsp.operation.model.beans.*, java.util.*"%>
<%@ include file="/WEB-INF/InitModel.jsp"%>
<%@ taglib uri="http://jakarta.apache.org/taglibs/input-0.90" prefix="input" %>
<html>
<head>
    <title>Descripcion Mantenimiento de Vias - Anular Ruta</title>
    <link href="<%= BASEURL %>/css/estilostsp.css" rel="stylesheet" type="text/css"> 
    <link href="/../../../css/estilostsp.css" rel="stylesheet" type="text/css">  
    <style type="text/css">
<!--
.Estilo3 {font-size: 12px}
-->
    </style>
</head>
<script type='text/javascript' src="<%= BASEURL %>/js/boton.js"></script> 
<body> 
<div id="capaSuperior" style="position:absolute; width:100%; height:100px; z-index:0; left: 0px; top: 0px;">
<jsp:include page="/WEB-INF/toptsp.jsp?encabezado=Ayuda Descriptiva - Reporte Oc Vacios con anticipo sin reporte"/>
</div>
<div id="capaCentral" style="position:absolute; width:100%; height:90%; z-index:0; left: 0px; top: 100px;"> 
   <p>&nbsp;</p>
   <table width="66%" border="2" align="center" id="tabla1" >
        <td>
         <table width="100%" height="50%" align="center">
              <tr>
                <td width="58%" class="subtitulo1"><strong>Mantenimiento de Vias - Anular Ruta</strong></td>
                <td width="42%" class="barratitulo"><img src="<%=BASEURL%>/images/titulo.gif" width="32" height="20"></td>
              </tr>
         </table>
         <table width="100%" bordercolor="#999999" bgcolor="#F7F5F4">
		  <tr>
             <td colspan="2" class="subtitulo"><span class="subtitulo"><strong>INFORMACION: Campos. </strong></span></td>
           </tr>
           <tr>
             <td width="26%" class="fila">Descripcion Adicional:</td>
             <td width="53%">Campo para digitar alguna descripcion adicional que el usuario requiera para diferenciar vias similares. </td>
           </tr>
           <tr>
             <td class="fila">Eliminar:</td>
             <td>Bot&oacute;n que elimina el ultimo tramo adicionado a la lista.</td>
           </tr>
           <tr>
             <td class="fila">Tramo:</td>
             <td>Campo para escoger(modificar) los tramos segun el origen hasta llegar al destino. </td>
           </tr>
           <tr>
             <td colspan="2" class="subtitulo"><span class="subtitulo"><strong>INFORMACION: Botones. </strong></span></td>
           </tr>
           <tr>
             <td class="fila">Modificar:</td>
             <td>Bot&oacute;n para realizar la accion de  modificar en el sistemas la ruta con todos los datos obtenidos en los otros campos . </td>
           </tr>
		   <tr>
             <td class="fila">Anular:</td>
             <td>Bot&oacute;n para realizar la accion anular de la ruta del sistemas. </td>
           </tr>
           <tr>
             <td class="fila">Salir:</td>
             <td>Bot&oacute;n para salir de la ventana sin realizar alguna acci&oacute;n.</td>
           </tr>	
		     
		    
         </table></td>
        </table>
		<p></p>
	<center>
	<img src = "<%=BASEURL%>/images/botones/salir.gif" style = "cursor:hand" name = "imgsalir" onClick = "parent.close();" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);">
	</center>
</div>  
</body>
</html>