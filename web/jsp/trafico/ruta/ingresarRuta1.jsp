<%@page contentType="text/html"%>
<%@page session="true"%>
<%@page import="java.util.*"%>
<%@page import="com.tsp.operation.model.*"%>
<%@page import="com.tsp.operation.model.beans.*"%>
<%@page import="com.tsp.util.*"%>
<%@include file="/WEB-INF/InitModel.jsp"%>
<%@ taglib uri="http://jakarta.apache.org/taglibs/input-0.90" prefix="input" %>
 
<html>
<head>
<title>Compa&ntilde;ia</title>
<link href="<%= BASEURL %>/css/estilo.css" rel='stylesheet'>
<link href="../css/estilo.css" rel="stylesheet" type="text/css">
<link href="../../../css/estilo.css" rel="stylesheet" type="text/css">
</head>
<script type='text/javascript' src="<%= BASEURL %>/js/validar.js"></script>
<body>
<%	String cpaiso = "", cestadoo = "", origen = "", cpaisd = "", cestadod = "", destino = "", combustible = "", distancia = "", duracion = "", ccombustible = "", cdistancia = "", cduracion = "", ccia="", via="", vian = "", viat = ""; 
	if(request.getParameter("sw")!=null){
		cpaiso = request.getParameter("c_paiso");
		origen = request.getParameter("c_origen");
		cpaisd = request.getParameter("c_paisd");
		//destino = request.getParameter("direccion");
		
		via = request.getParameter("c_via");
		vian = request.getParameter("c_vian");
		viat = request.getParameter("c_viat");
	}
	
%>

<FORM name='forma' id='forma' method='POST' action="<%=CONTROLLER%>?estado=Ruta&accion=Ingresar" onSubmit="return validarRuta();">
  <table width="416" border="1" cellspacing="2" cellpadding="3" align="center" bordercolor="#B7D0DC">
    <tr class="titulo" align="center"> 
      <td colspan="2">Ingresar Ruta</td>
    </tr>
    <tr class="subtitulos"> 
      <td height="20" colspan="2">Informacion
	  </td>
    </tr>
	<tr class="fila"> 
      <td width="142">Distito</td>
      <td width="274" valign="middle"><input name="c_cia" type="text" id="c_cia" value="<%=(String) session.getAttribute("Distrito")%>" readonly></td>
	</tr>
    <tr>
      <td colspan="2" class="subtitulos">Origen</td>
    </tr>
    <tr class="fila"> 
      <td width="142">Pais</td>
      <td width="274" valign="middle"><select name="c_paiso" id="select" class="texbox" onChange="cargarSelects('<%=CONTROLLER%>?estado=Cargar&accion=Variosruta&carpeta=jsp/trafico/ruta&pagina=ingresarRuta.jsp&sw=4');">
        <%if (model.paisservice.existePaises()){
			model.paisservice.listarpaises();
			Pais pais; 
	        List paises;
    	    paises = model.paisservice.obtenerpaises();		
			Iterator it = paises.iterator();%>
        <option value="">Seleccione</option>
        <%
			while (it.hasNext()){  
				pais = (Pais) it.next();	
				if(cpaiso.equals(pais.getCountry_code())){
    	    	%>
        <option value="<%=pais.getCountry_code()%>" selected><%=pais.getCountry_name()%></option>
        <%}else{%>
        <option value="<%=pais.getCountry_code()%>"><%=pais.getCountry_name()%></option>
        <%}
	   }
	   }%>
      </select></td>
    </tr>
    <tr class="fila"> 
      <td width="142">Ciudad</td>
      <td width="274" valign="middle">
	  <select id="select4" name="c_origen"  class="texbox" onChange="forma.c_via.value='';if(forma.c_cia.value==''){alert('Seleccione una compania')}else{forma.c_via.value=this.value;forma.c_viat.value=this.value;forma.c_vian.value=forma.c_origen.options(forma.c_origen.selectedIndex).text;cargarSelects('<%=CONTROLLER%>?estado=Cargar&accion=Variosruta&carpeta=jsp/trafico/ruta&pagina=ingresarRuta.jsp&sw=2')};">
          <%
				List ciudades = (List) request.getAttribute("ciudadO");%>
				<option value="">Seleccione</option>
				<%if(ciudades!=null){
            	Iterator it = ciudades.iterator();
			    while (it.hasNext()){  
			      	Ciudad ciu = (Ciudad) it.next();
					String c_ciu = request.getParameter("c_origen")!=null?request.getParameter("c_origen"):"";
					if(c_ciu.equals(ciu.getcodciu())){%>
					<option value="<%=ciu.getcodciu()%>" selected><%=ciu.getnomciu()%></option>
					<%}else{%>
					<option value="<%=ciu.getcodciu()%>"><%=ciu.getnomciu()%></option>
					<%}%>
          	  <%}
			  }%>
        </select>        
</td>
    </tr>
    <tr>
      <td colspan="2" class="subtitulos">Destino </td>
    </tr>
    <tr class="fila"> 
      <td width="142">Pais</td>
      <td width="274" valign="middle"><select name="c_paisd" id="select2" class="texbox" onChange="cargarSelects('<%=CONTROLLER%>?estado=Cargar&accion=Variosruta&carpeta=jsp/trafico/ruta&pagina=ingresarRuta.jsp');">
        <%if (model.paisservice.existePaises()){
			model.paisservice.listarpaises();
			Pais pais; 
	        List paises;
    	    paises = model.paisservice.obtenerpaises();		
			Iterator it = paises.iterator();%>
        <option value="">Seleccione</option>
        <%
			while (it.hasNext()){  
				pais = (Pais) it.next();	
				if(cpaisd.equals(pais.getCountry_code())){
    	    	%>
        <option value="<%=pais.getCountry_code()%>" selected><%=pais.getCountry_name()%></option>
        <%}else{%>
        <option value="<%=pais.getCountry_code()%>"><%=pais.getCountry_name()%></option>
        <%}
	   }
	   }%>
      </select></td>
    </tr>
    <tr class="fila"> 
      <td width="142">Ciudad</td>
      <td width="274" valign="middle">
	  
        <select id="select6" name="c_destino"  class="texbox">
          <%
				ciudades = (List) request.getAttribute("ciudadD");%>
				<option value="">Seleccione</option>
				<%if(ciudades!=null){
            	Iterator it = ciudades.iterator();
				
			    while (it.hasNext()){  
			      	Ciudad ciu = (Ciudad) it.next();
					String c_ciu = request.getParameter("c_destino")!=null?request.getParameter("c_destino"):"";
					if(c_ciu.equals(ciu.getcodciu())){%>
					<option value="<%=ciu.getcodciu()%>" selected><%=ciu.getnomciu()%></option>
					<%}else{%>
					<option value="<%=ciu.getcodciu()%>"><%=ciu.getnomciu()%></option>
					<%}%>
          <%}
		  }%>
        </select>
      </td>
    </tr>
    <tr  class="subtitulos">
      <td colspan="2">Via</td>
    </tr>
    <tr class="fila" align="center">
      <td colspan="2">
        
        <table width="100%"  border="0" cellspacing="0" cellpadding="0">
          <tr>
            <td width="31%"><textarea name="c_viat" cols="20" rows="5" readonly class="textbox" id="c_viat"><%=viat%></textarea>			</td>
            <td width="2%">&nbsp;</td>
            <td width="67%"><textarea name="c_vian" cols="49" rows="5" readonly class="textbox" id="c_vian"><%=vian%></textarea></td>
          </tr>
        </table>
        <br>
        <input name="c_via" id="c_via" type="hidden" value="<%=via%>">
        <input name="Submit" type="button" class="boton" value="Borrar Tramo" onClick="borrarTramo();">
      </td>
    </tr>
    <tr class="fila">
      <td width="142">Tramo</td>
      <td valign="middle"> <%TreeMap tramos= model.tramoService.getLtramos();%>
        <input name="pagina" type="hidden" id="pagina" value="<%=CONTROLLER%>?estado=Cargar&accion=Variosruta&carpeta=jsp/trafico/ruta&pagina=ingresarRuta.jsp">	  
      <input:select name="tramo" attributesText=" id='c_tramo' style='width:100% ' onChange='escogeTramo(this.value)';" options="<%=tramos%>"/></td>
    </tr>
    <tr class="pie">
		<td colspan="2" align="center">
			<input type="submit" class="boton" id="c_ingresar" name="c_ingresar" value="Ingresar"> 		
			<input name="c_regresar" type="button" class="boton" id="c_regresar" onClick="window.location='<%=BASEURL%>/inicio.htm'" value="Cancelar">
		</td>
    </tr>
  </table>
  <p></p>
</form>
<div align="center">
  <%if(request.getAttribute("mensaje")!=null){%>
    <span class="mensajes"><%=request.getAttribute("mensaje")%></span>
  <%}%>
</div>
</body>
</html>
