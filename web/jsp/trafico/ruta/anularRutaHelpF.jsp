<!--
- Autor : Ing. Enrique De Lavalle.
- Date  : 27 de Noviembre 2006
- Copyrigth Notice : Fintravalores S.A. S.A
-->
<%--
-@(#)
--Descripcion : Jsp que muestra la ayuda para reporte de Oc de Viajes Vacios con anticipo sin reporte 
--%>
<%@ include file="/WEB-INF/InitModel.jsp"%>
<HTML>
<HEAD>
<TITLE>Funcionalidad del Reporte OC de vacios con anticipos sin reporte</TITLE>
<META http-equiv=Content-Type content="text/html; charset=windows-1252">
<link href="/../../../css/estilostsp.css" rel="stylesheet" type="text/css"> 
<link href="<%=BASEURL%>/css/estilostsp.css" rel="stylesheet" type="text/css">
<script type='text/javascript' src="<%= BASEURL %>/js/boton.js"></script> 
</HEAD>
<BODY> 
<% String BASEIMG = BASEURL +"/images/ayuda/reporte/"; %>
  <table width="95%"  border="2" align="center">
    <tr>
      <td height="117" >
        <table width="100%" border="0" align="center">
          <tr  class="subtitulo">
            <td height="20"><div align="center">MANUAL DE  ANULAR VIA</div></td>
          </tr>
          <tr class="subtitulo1">
            <td><p>Descripci&oacute;n del funcionamiento del programa de Anular de Via: </p>
              <p>&nbsp;</p></td>
          </tr>
          <tr>
            <td  class="ayudaHtmlTexto"><div align="center"><img src="../../../images/ayuda/trafico/mantenimientoVias/anularVia.JPG" width="665" height="663"></div></td>
          </tr>
<tr>
  <td  class="ayudaHtmlTexto"><p>&nbsp;</p>    </td>
</tr>
<tr>
            <td  class="ayudaHtmlTexto"><p class="ayudaHtmlTexto">El &uacute;ltimo destino del &uacute;ltimo tramo escojido debe ser igual a la ciudad de destino de lo contrario sale el siguiente error </p>            </td>
          </tr>
<tr>
  <td  class="ayudaHtmlTexto"><div align="center"><img src="../../../images/ayuda/trafico/mantenimientoVias/error3.JPG" width="357" height="128"></div></td>
</tr>
<tr>
            <td  class="ayudaHtmlTexto"><div align="left">
              <p>&nbsp;</p>
              <p>Finalmente si los datos fueron igresados correctamente se recargan los datos en la venta de busqueda de ruta en caso de escojer la modificaci&oacute;n o anula la ruta seleccionada en caso que se escoja anular. </p>
            </div></td>
          </tr>
<tr>
  <td  class="ayudaHtmlTexto"><p align="center">. </p>
    <p>&nbsp;</p></td>
</tr>
      </table></td>
    </tr>
  </table>
  <p></p>
<center>
<img src = "<%=BASEURL%>/images/botones/salir.gif" style = "cursor:hand" name = "imgsalir" onClick = "parent.close();" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);">
</center>
</BODY>
</HTML>

