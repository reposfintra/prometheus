<!--
- Date  : 2005
- Copyrigth Notice : Fintravalores S.A. S.A
-->
<%--
-@(#)
--Descripcion : Pagina JSP, que muestra lo datos de entrada para ingresar una ruta
--%>   
<%@page contentType="text/html"%>
<%@page session="true"%>
<%@page import="java.util.*"%>
<%@page import="com.tsp.operation.model.*"%>
<%@page import="com.tsp.operation.model.beans.*"%>
<%@page import="com.tsp.util.*"%>
<%@include file="/WEB-INF/InitModel.jsp"%>
<%@ taglib uri="http://jakarta.apache.org/taglibs/input-0.90" prefix="input" %>
 
<html>
<head>
<title>Ingresar Ruta</title>
<script src='<%=BASEURL%>/js/validar.js'></script>
<script type='text/javascript' src="<%= BASEURL %>/js/boton.js"></script>
<link href="<%= BASEURL %>/css/estilostsp.css" rel='stylesheet'> 
<link href="../css/estilostsp.css" rel="stylesheet" type="text/css">
<link href="../../../css/estilostsp.css" rel="stylesheet" type="text/css">
</head>
<body onResize="redimensionar()" onLoad="redimensionar();" >
<div id="capaSuperior" style="position:absolute; width:100%; height:100px; z-index:0; left: 0px; top: 0px;">
<jsp:include page="/WEB-INF/toptsp.jsp?encabezado=Parametros de Busqueda"/>
</div>

<div id="capaCentral" style="position:absolute; width:100%; height:85%; z-index:0; left: 0px; top: 100px; overflow: scroll;"> 
<%	
	//variables para las ayudas 
	String path = application.getRealPath(model.menuService.getRealUrl(request.getRequestURI()));
	String datos[] = model.menuService.getContenidoMenu(BASEURL,request.getRequestURI(),path);

	String cpaiso = "", cestadoo = "", origen = "", cpaisd = "", cestadod = "", destino = "", combustible = "", distancia = "", duracion = "", ccombustible = "", cdistancia = "", cduracion = "", ccia="", via="", vian = "", viat = "", descAdd=""; 
	if(request.getParameter("sw")!=null){
		cpaiso = request.getParameter("c_paiso");
		origen = request.getParameter("c_origen");
		cpaisd = request.getParameter("c_paisd");
		//destino = request.getParameter("direccion");
		
		via = request.getParameter("c_via");
		vian = request.getParameter("c_vian");
		viat = request.getParameter("c_viat");
		descAdd = request.getParameter("desc2");
	}
	String Mensaje       = (request.getParameter("Mensaje")!=null)?request.getParameter("Mensaje"):"";
        String valorTramo       = (request.getParameter("tramo")!=null)?request.getParameter("tramo"):"";
%>
<script>

    function validacionCopiar(){
        if(forma.c_cia.value==""){
            alert("El campo compa�ia no puede ser vacio!");
            return (false);
        }
	if(forma.c_origen.value==""){
            alert("El campo origen no puede ser vacio!");
            return (false);
        }
	if(forma.c_destino.value==""){
            alert("El campo destino no puede ser vacio!");
            return (false);
	}
	window.open('<%=CONTROLLER%>?estado=Manager&accion=CopiarRutas&opc=1&origen='+forma.c_origen.value+'&marco=no','','width=480,height=455,scrollbars=yes,resizable=yes,top=0,left=50,status=yes');
    }
    function validacionSeleccion( valor ){
        if( valor != "" ){
            escogeTramo( valor );
        }else{
            document.getElementById( "c_tramo" ).value = "<%= valorTramo %>";
        }
        
    }
</script>
<FORM name='forma' id='forma' method='POST' action="<%=CONTROLLER%>?estado=Ruta&accion=Ingresar">
  <table width="416" border="2" align="center">
    <tr>
      <td>
	  <table width="100%" height="34" border="0" align="center" class="tablaInferior">
        <tr>
            <td width="157" height="24"  class="subtitulo1"><p align="left">Ingresar Ruta</p></td>
            <td width="404" class="barratitulo"><%=datos[0]%><img src="<%=BASEURL%>/images/titulo.gif" width="32" height="20"></td>
          </tr>
          <tr class="fila">
          	<td width="142">Distito</td>
          	<td width="274" valign="middle"><input name="c_cia" type="text" id="c_cia" value="<%=(String) session.getAttribute("Distrito")%>" readonly class="textbox"></td>
       	  </tr>
		</table>
	  </td>
	</tr>
 </table>		
 <table width="416" border="2" align="center">
 	<tr>
		<td>
		 <table width="100%" height="34" border="0" align="center" class="tablaInferior">
     	 	<tr>
            	<td width="157" height="24"  class="subtitulo1"><p align="left">Origen</p></td>
            	<td width="404" class="barratitulo"><img src="<%=BASEURL%>/images/titulo.gif" width="32" height="20"></td>
          	</tr>
       		 <tr class="fila">
          		<td width="142">Pais</td>
          		<td width="274" valign="middle"><select name="c_paiso" id="select" class="textbox" onChange="cargarSelects('<%=CONTROLLER%>?estado=Cargar&accion=Variosruta&carpeta=jsp/trafico/ruta&pagina=ingresarRuta.jsp&sw=4');">
              		<%if (model.paisservice.existePaises()){
						model.paisservice.listarpaises();
						Pais pais; 
	        			List paises;
    	    			paises = model.paisservice.obtenerpaises();		
						Iterator it = paises.iterator();%>
              			<option value="">Seleccione</option>
              		<%
					while (it.hasNext()){  
						pais = (Pais) it.next();
						if(cpaiso.equals(pais.getCountry_code())){
    	    		%>
              			<option value="<%=pais.getCountry_code()%>" selected><%=pais.getCountry_name()%></option>
              		<%}else{%>
              			<option value="<%=pais.getCountry_code()%>"><%=pais.getCountry_name()%></option>
              		<%}
	   				}
	   			}%>
          		</select></td>
        	</tr>
        <tr class="fila">
          <td width="142">Ciudad</td>
          <td width="274" valign="middle">
            <select id="select4" name="c_origen"  class="textbox" onChange="forma.c_via.value='';if(forma.c_cia.value==''){alert('Seleccione una compania')}else{forma.c_via.value=this.value;forma.c_viat.value=this.value;forma.c_vian.value=forma.c_origen.options(forma.c_origen.selectedIndex).text;cargarSelects('<%=CONTROLLER%>?estado=Cargar&accion=Variosruta&carpeta=jsp/trafico/ruta&pagina=ingresarRuta.jsp&sw=2')};">
              <%
				List ciudades = (List) request.getAttribute("ciudadO");%>
              <option value="">Seleccione</option>
              <%if(ciudades!=null){
            	Iterator it = ciudades.iterator();
			    while (it.hasNext()){  
			      	Ciudad ciu = (Ciudad) it.next();
					String c_ciu = request.getParameter("c_origen")!=null?request.getParameter("c_origen"):"";
					if(c_ciu.equals(ciu.getcodciu())){%>
              <option value="<%=ciu.getcodciu()%>" selected><%=ciu.getnomciu()%></option>
              <%}else{%>
              <option value="<%=ciu.getcodciu()%>"><%=ciu.getnomciu()%></option>
              <%}%>
              <%}
			  }%>
            </select>
          </td>
        </tr>
        <tr>
		</table>
		</td>
	</tr>
 </table>		
 <table width="416" border="2" align="center">
 	<tr>
		<td>
		 <table width="100%" height="34" border="0" align="center" class="tablaInferior">
	 		<tr>
            	<td width="157" height="24"  class="subtitulo1"><p align="left">Destino</p></td>
            	<td width="404" class="barratitulo"><img src="<%=BASEURL%>/images/titulo.gif" width="32" height="20"></td>
          	</tr>
        <tr class="fila">
          <td width="142">Pais</td>
          <td width="274" valign="middle"><select name="c_paisd" id="c_paisd" class="textbox" onChange="cargarSelects('<%=CONTROLLER%>?estado=Cargar&accion=Variosruta&carpeta=jsp/trafico/ruta&pagina=ingresarRuta.jsp');">
              <%if (model.paisservice.existePaises()){
			model.paisservice.listarpaises();
			Pais pais; 
	        List paises;
    	    paises = model.paisservice.obtenerpaises();		
			Iterator it = paises.iterator();%>
              <option value="">Seleccione</option>
              <%
			while (it.hasNext()){  
				pais = (Pais) it.next();	
				if(cpaisd.equals(pais.getCountry_code())){
    	    	%>
              <option value="<%=pais.getCountry_code()%>" selected><%=pais.getCountry_name()%></option>
              <%}else{%>
              <option value="<%=pais.getCountry_code()%>"><%=pais.getCountry_name()%></option>
              <%}
	   }
	   }%>
          </select></td>
        </tr>
        <tr class="fila">
          <td width="142">Ciudad</td>
          <td width="274" valign="middle">
            <select id="select6" name="c_destino"  class="textbox">
              <%
				ciudades = (List) request.getAttribute("ciudadD");%>
              <option value="">Seleccione</option>
              <%if(ciudades!=null){
            	Iterator it = ciudades.iterator();
				
			    while (it.hasNext()){  
			      	Ciudad ciu = (Ciudad) it.next();
					String c_ciu = request.getParameter("c_destino")!=null?request.getParameter("c_destino"):"";
					if(c_ciu.equals(ciu.getcodciu())){%>
              <option value="<%=ciu.getcodciu()%>" selected><%=ciu.getnomciu()%></option>
              <%}else{%>
              <option value="<%=ciu.getcodciu()%>"><%=ciu.getnomciu()%></option>
              <%}%>
              <%}
		  }%>
            </select>
          </td>
        </tr>
		</table>
		</td>
	</tr>
 </table>		
 
 
 <table width="416" border="2" align="center">
 	<tr>
		<td>
		 <table width="100%" height="34" border="0" align="center" class="tablaInferior">
         <tr class="fila">
          <td width="157">Descripcion Adicional </td>
          <td width="404" valign="middle">
			    <input type="text" name="desc2" id="desc2" class="textbox" value="<%=descAdd%>">
		  </td>
        </tr>
		</table>
		</td>
	</tr>
 </table>
 
 
 
 
  <table width="416" border="2" align="center">
 	<tr>
		<td>
		 <table width="100%" height="34" border="0" align="center" class="tablaInferior">
		   <tr>
            	<td width="157" height="24"  class="subtitulo1"><p align="left">Via</p></td>
            	<td width="404" class="barratitulo"><img src="<%=BASEURL%>/images/titulo.gif" align=left width="32" height="20"><img src="<%=BASEURL%>/images/botones/iconos/new.gif" align="right" alt="Copiar Rutas en V�a" style="cursor:pointer" onclick="validacionCopiar();"></td>
       	   </tr>
        </tr>
        <tr class="fila" align="center">
          <td colspan="2">
            <table width="100%"  border="0" cellspacing="0" cellpadding="0">
              <tr>
                <td width="31%"><textarea name="c_viat" cols="20" rows="5" readonly class="textbox" id="c_viat"><%=viat%></textarea>
                </td>
                <td width="2%">&nbsp;</td>
                <td width="67%"><textarea name="c_vian" cols="49" rows="5" readonly class="textbox" id="c_vian"><%=vian%></textarea></td>
              </tr>
            </table>
            <br>
            <input name="c_via" id="c_via" type="hidden" value="<%=via%>">           
			<img src="<%=BASEURL%>/images/botones/eliminar.gif"  name="imgeliminar" onClick="borrarTramo();" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" style="cursor:hand">        
          </td>
        </tr>
		</table>
		</td>
	</tr>
 </table>
 <table width="416" border="2" align="center">
 	<tr>
		<td>
		 <table width="100%" height="34" border="0" align="center" class="tablaInferior">
        <tr class="fila">
          <td width="142">Tramo</td>
          <td valign="middle">
            <%TreeMap tramos= model.tramoService.getLtramos();%>
            <input name="pagina" type="hidden" id="pagina" value="<%=CONTROLLER%>?estado=Cargar&accion=Variosruta&carpeta=jsp/trafico/ruta&pagina=ingresarRuta.jsp">
            <input:select name="tramo" attributesText=" id='c_tramo' style='width:100% ' onChange='validacionSeleccion( this.value );' class='listmenu' " options='<%=tramos%>' /></td>
        </tr>
        </table>
        </td>
	</tr>
 </table>
 <br>
    <table align="center">
    <td colspan="2" align="center">
        <img src="<%=BASEURL%>/images/botones/aceptar.gif"  name="imgaceptar" onClick="if( validarRuta() ){forma.submit();}" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" style="cursor:hand">
        <img src="<%=BASEURL%>/images/botones/salir.gif"  name="imgsalir" onClick="parent.close();" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" style="cursor:hand">
    </td>
    </table>
        </tr>
      </table></td>
    </tr>
  </table>
  <p></p>
</form>
<div align="center">
  <%
  
  if(!Mensaje.equals("")){%>
  <p>
  <table border="2" align="center">
    <tr>
      <td><table width="100%"  border="1" align="center"  bordercolor="#F7F5F4" bgcolor="#FFFFFF">
          <tr>
            <td width="229" align="center" class="mensajes"><%=Mensaje%></td>
            <td width="29" background="<%=BASEURL%>/images/cuadronaranja.JPG">&nbsp;</td>
            <td width="58">&nbsp;</td>
          </tr>
      </table></td>
    </tr>
  </table>
  <p></p>
    <%}%>
  </div>
  </div>
   <%=datos[1]%>
</body>
</html>