<!--
- Date  : 2006
- Autor : David Pi�a lopez
- Copyrigth Notice : Fintravalores S.A. S.A
-->
<%@page contentType="text/html"%>
<%@page session="true"%>
<%@page import="java.util.*"%>
<%@page import="com.tsp.operation.model.*"%>
<%@page import="com.tsp.operation.model.beans.*"%>
<%@page import="com.tsp.util.*"%>
<%@include file="/WEB-INF/InitModel.jsp"%>
<%@ taglib uri="http://jakarta.apache.org/taglibs/input-0.90" prefix="input" %>
 
<html>
    <head>
        <title>Copiar Rutas en V�a</title>
        <script src='<%=BASEURL%>/js/validar.js'></script>
        <script src='<%=BASEURL%>/js/utilidades.js'></script>
        <script type='text/javascript' src="<%= BASEURL %>/js/boton.js"></script>
        <link href="<%= BASEURL %>/css/estilostsp.css" rel='stylesheet'> 
        <link href="../css/estilostsp.css" rel="stylesheet" type="text/css">
        <link href="../../../css/estilostsp.css" rel="stylesheet" type="text/css">
        <script>
            function validarBusqueda( valor ){
                if( valor != "" ){
                    mostrarVias( "contenido", "<%=CONTROLLER%>?estado=Manager&accion=CopiarRutas&opc=2", "forma" );
                }else{                
                    document.getElementById( "contenido" ).innerHTML = "";
                }
            }
            function enviarFormulario(){                
                if( document.getElementById( "rutas" ).value == "" ){
                    alert( "Debe seleccionar una ruta" );
                    return false;
                }
                validandoEnvio( "contenido", "<%=CONTROLLER%>?estado=Manager&accion=CopiarRutas&opc=3", "forma" );
            }
        </script>
    </head>
    <body>
<%	
	String Mensaje       = (request.getParameter("Mensaje")!=null)?request.getParameter("Mensaje"):"";
        Vector rutas = model.rutaService.getRutas();
%>
        <FORM name='forma' id='forma' method='POST' > 	
        <table width="450" border="2" align="center">
            <tr>
                <td>
                    <table width="100%" height="34" border="0" align="center" class="tablaInferior">
                        <tr>
                            <td width="140" height="24"  class="subtitulo1">Copiar Rutas en V�as</td>
                            <td class="barratitulo"><img src="<%=BASEURL%>/images/titulo.gif" width="32" height="20"></td>
                        </tr>
                        <tr class="fila">
                            <td width="142" valign=top>Rutas en V�as</td>
                            <td width="280" valign="middle">
                                <select name="rutas" id="rutas" class="textbox" style="width:260" onChange="validarBusqueda( this.value );">
                                <option value="">Seleccione ruta</option>
                                <%
                                    for( int i = 0; i < rutas.size(); i++ ){
                                        Ruta r = (Ruta)rutas.get( i );
                                        out.print("<option value='"+r.getVia()+"'>"+r.getDescripcion()+"</option>");
                                    }
                                %>                                
                                </select>
                                <div id="working" align="center" style="visibility:hidden" class="informacion"><img src="<%=BASEURL%>/images/cargando.gif" align="middle">&nbsp;&nbsp;Cargando V�as...</div>
                            </td>                            
                        </tr>
                    </table>
                    <table width="100%" height="34" border="0" align="center" class="tablaInferior">                        
                        <tr class="fila">
                            <td >
                                <div id = "contenido" ></div>
                            </td>                    
                        </tr>
                    </table>
                </td>
            </tr>
        </table>        
        <br>
        <table align="center">
            <td colspan="2" align="center">
                <img src="<%=BASEURL%>/images/botones/aceptar.gif"  name="imgaceptar" onClick="enviarFormulario();" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" style="cursor:hand">        
                <img src="<%=BASEURL%>/images/botones/salir.gif"  name="imgsalir" onClick="parent.close();" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" style="cursor:hand">
            </td>
        </table>
        </tr>
        </table></td>
        </tr>
        </table>  
        </form>        
  <%  
  if(!Mensaje.equals("")){%>
        <div align="center">
            <p>
            <table border="2" align="center">
                <tr>
                    <td><table width="100%"  border="1" align="center"  bordercolor="#F7F5F4" bgcolor="#FFFFFF">
                        <tr>
                            <td width="229" align="center" class="mensajes"><%=Mensaje%></td>
                            <td width="29" background="<%=BASEURL%>/images/cuadronaranja.JPG">&nbsp;</td>
                            <td width="58">&nbsp;</td>
                        </tr>
                    </table></td>
                </tr>
            </table>
            </p>
            <p></p>
        </div>  
    <%}%>        
    </body>
</html>
