<%@ include file="/WEB-INF/InitModel.jsp"%>

<html>
<head>
<title>Descripcion de campos Ingresar Ruta</title>


<link href="<%=BASEURL%>/css/estilostsp.css" rel="stylesheet" type="text/css"> 
</head>

<body>
<% String BASEIMG = BASEURL +"/images/botones/"; %> 
<br>
<table width="696"  border="2" align="center">
  <tr>
    <td width="811" >
      <table width="100%" border="0" align="center">
        <tr  class="subtitulo">
          <td height="24" colspan="2"><div align="center">Ingresar Ruta</div></td>
        </tr>
        <tr class="subtitulo1">
          <td colspan="2">PROCESOS</td>
        </tr>
        <tr>
          <td  class="fila">Distrito </td>
          <td  class="ayudaHtmlTexto">Ingresar el Distrito</td>
        </tr>
        <tr>
          <td  class="fila">Pais de Origen</td>
          <td  class="ayudaHtmlTexto">Seleccionar el Pais de origen</td>
        </tr>
        <tr>
          <td  class="fila">Ciudad de Origen</td>
          <td  class="ayudaHtmlTexto">Seleccionar la Cuidad de origen</td>
        </tr>
        <tr>
          <td  class="fila">Pais de Destino</td>
          <td  class="ayudaHtmlTexto">Seleccionar el Pais de Destino</td>
        </tr>
        <tr>
          <td  class="fila">Ciudad de Destino</td>
          <td  class="ayudaHtmlTexto">Seleccionar la Ciudad de Destino</td>
        </tr>
        <tr>
         <tr>
          <td  class="fila">Via</td>
          <td  class="ayudaHtmlTexto">Captura el Codigo y Descripcion de la Via</td>
        </tr>
        <tr>
          <td class="fila"> Boton Copiar Rutas <img src="<%=BASEURL%>/images/botones/iconos/new.gif" ></td>
          <td  class="ayudaHtmlTexto">Boton para Copiar Rutas en V�a</td>
        </tr>
        <tr>
          <td class="fila"> Boton Eliminar<img src="<%=BASEURL%>/images/botones/eliminar.gif"></td>        
          <td  class="ayudaHtmlTexto">Boton para Eliminar Rutas en V�a</td>
        </tr>
        <tr>
          <td class="fila"> Tramo</td>        
          <td  class="ayudaHtmlTexto">Seleccionar el � los Tramos de la Via escogida</td>
        </tr>
        
        
        <tr>
          <td class="fila"> Boton Aceptar <img src="<%=BASEURL%>/images/botones/aceptar.gif"></td>
          <td  class="ayudaHtmlTexto">Boton para Ingresar la nueva Ruta</td>
        </tr>
        <tr>
          <td width="556" class="fila">Boton Salir <img src="<%=BASEIMG%>salir.gif"></td>
          <td width="556"  class="ayudaHtmlTexto">Boton para salir de la vista, regresando a control trafico.</td>
        </tr>
      </table>
      <br>
      <center>
	<img src="<%=BASEURL%>/images/botones/salir.gif" name="c_salir" onClick="window.close();" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" align="absmiddle">
      </center>
      <br>
        <p align="center" class="fuenteAyuda"> Fintravalores S. A. </p>
      <br>
    </td>
  </tr>
</table>
<p>&nbsp;</p>

<p>&nbsp;</p>
<p>&nbsp; </p>
</body>
</html>
