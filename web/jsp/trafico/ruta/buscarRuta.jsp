<!--
- Date  : 2005
- Copyrigth Notice : Fintravalores S.A. S.A
-->
<%--
-@(#)
--Descripcion : Pagina JSP, que muestra los parametros de busqueda de ruta
--%>   
<%@page contentType="text/html"%>
<%@page session="true"%>
<%@page import="java.util.*"%>
<%@page import="com.tsp.operation.model.*"%>
<%@page import="com.tsp.operation.model.beans.*"%>
<%@page import="com.tsp.util.*"%>
<%@include file="/WEB-INF/InitModel.jsp"%>
<html>
<head> 
<title>Buscar Ruta</title>
<link href="<%= BASEURL %>/css/estilostsp.css" rel='stylesheet'> 
<link href="../css/estilostsp.css" rel="stylesheet" type="text/css">
<link href="../../../css/estilostsp.css" rel="stylesheet" type="text/css">
<style type="text/css">
<script src='<%=BASEURL%>/js/validar.js'></script>
<script type='text/javascript' src="<%= BASEURL %>/js/boton.js"></script>
<link href="<%= BASEURL %>/css/estilomenuemergente.css" rel='stylesheet'> 
<link href="../../trafico/css/estilostsp.css" rel="stylesheet" type="text/css">
<script type='text/javascript' src="<%= BASEURL %>/js/reporte.js"></script>
<script language="JavaScript" type="text/javascript" src="<%=BASEURL%>/js/validarDocumentos.js"></script>




<!--
.Estilo2 {
	font-size: 9px;
	font-family: Verdana, Arial, Helvetica, sans-serif;
	color: #FF0000;
	font-weight: bold;
}
-->
</style>
</head>
<script type='text/javascript' src="<%= BASEURL %>/js/validar.js"></script>
<script type='text/javascript' src="<%= BASEURL %>/js/boton.js"></script>
<body onResize="redimensionar()" onLoad="redimensionar();">
<div id="capaSuperior" style="position:absolute; width:100%; height:100px; z-index:0; left: 0px; top: 0px;">
<jsp:include page="/WEB-INF/toptsp.jsp?encabezado=Buscar Ruta"/>
</div>
<%
String path = application.getRealPath(model.menuService.getRealUrl(request.getRequestURI()));
String datos[] = model.menuService.getContenidoMenu(BASEURL,request.getRequestURI(),path);
%>
<div id="capaCentral" style="position:absolute; width:100%; height:85%; z-index:0; left: 0px; top: 100px; overflow: scroll;">
<form name="forma" id="forma" method="post"   action='<%=CONTROLLER%>?estado=Buscar&accion=Rutas&carpeta=jsp/trafico/ruta&pagina=rutas.jsp&sw=2&marco=no'>
  <table width="416" border="2" align="center">
    <tr>
      <td><table width="100%" height="34" border="0" align="center" class="tablaInferior">
        <tr>
            <td width="157" height="24"  class="subtitulo1"><p align="left">Buscar Ruta</p></td>
            <td width="450" class="barratitulo"><%=datos[0]%><img src="<%=BASEURL%>/images/titulo.gif" width="32" height="20"align="left"></td>
          </tr>
        
        <tr class="fila">
          <td width="119">Origen</td>
          <td width="273" colspan="2" valign="middle">
            <input name="c_origen" type="text" class="textbox" id="c_origen" maxlength="45" onkeypress='soloAlfa(event)'>
          </td>
        </tr>
        <tr class="fila">
          <td width="119">Destino</td>
          <td width="273" colspan="2" valign="middle">
            <input name="c_destino" type="text" class="textbox" id="c_destino" maxlength="15" onkeypress='soloAlfa(event)'>
          </td>
        </tr>       
      </table></td>
    </tr>
  </table>
  <br>
  <table align="center">
  <td colspan="2" align="center">
  	<img  name="imgbuscar" style="cursor:hand" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" src="<%=BASEURL%>/images/botones/buscar.gif" align="middle" onclick="validarBuscar()">
  	<img src="<%=BASEURL%>/images/botones/salir.gif"  name="imgsalir" align="middle" style="cursor:hand" onClick="parent.close();" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);">
  	 </td>
  </table>
  <p>&nbsp;</p>
</form>
</div>
<iframe width=188 height=166 name="gToday:normal:agenda.js:gfPop:plugins.js" id="gToday:normal:agenda.js:gfPop:plugins.js" src="<%=BASEURL%>/js/Calendario/ipopeng.htm" scrolling="no" frameborder="0" style="visibility:visible; z-index:999; position:absolute; left:-500px; top:0px;"> </iframe>
  <%=datos[1]%>
</body>
</html>
<script>
function validarBuscar() {
    if(forma.c_origen.value=='' && forma.c_destino.value=='') {
        alert('Digite por lo menos un valor de busqueda');
        forma.c_origen.focus();
        return false;
    } else {
        forma.submit();
    }
}
</script>
