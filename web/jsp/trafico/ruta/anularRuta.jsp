<!--
- Date  : 2005
- Copyrigth Notice : Fintravalores S.A. S.A
-->
<%--
-@(#) 
--Descripcion : Pagina JSP, que permite anular una ruta
--%>   
<%@page contentType="text/html"%>
<%@page session="true"%>
<%@page import="java.util.*"%>
<%@page import="com.tsp.operation.model.*"%>
<%@page import="com.tsp.operation.model.beans.*"%>
<%@page import="com.tsp.util.*"%>
<%@include file="/WEB-INF/InitModel.jsp"%>
<%@ taglib uri="http://jakarta.apache.org/taglibs/input-0.90" prefix="input" %>

<html>
<head>
<title>Anulacion Ruta</title>
<link href="<%= BASEURL %>/css/estilostsp.css" rel='stylesheet'> 
<link href="../css/estilostsp.css" rel="stylesheet" type="text/css">
<link href="../../../css/estilostsp.css" rel="stylesheet" type="text/css">
</head>
<script type='text/javascript' src="<%= BASEURL %>/js/validar.js"></script>
<script type='text/javascript' src="<%= BASEURL %>/js/boton.js"></script>
<body onResize="redimensionar()" <%if(request.getParameter("reload")!=null){%>onLoad="window.opener.location.reload(); redimensionar();"<%}%>>
<div id="capaSuperior" style="position:absolute; width:100%; height:100px; z-index:0; left: 0px; top: 0px;">
<jsp:include page="/WEB-INF/toptsp.jsp?encabezado=Anular Ruta"/>
</div>

<div id="capaCentral" style="position:absolute; width:100%; height:85%; z-index:0; left: 0px; top: 100px; overflow: scroll;"> 

<%	String ncia = "", norigen = "", ciuOrig="", ciuDest="", origen = "", ndestino = "", destino = "", ccia="",secuencia="",via="", vian = "", viat = "", duracion="", tiempo= "", descripcion="",descAdd="";
	
	
	if(request.getParameter("lista")!=null){
		Ruta ruta = model.rutaService.getRuta();
		ccia = ruta.getCia();
		descripcion = ruta.getNcia();		
		origen = ruta.getOrigen();
		ciuOrig = ruta.getNorigen();
		ciuDest = ruta.getNdestino();
		norigen = ruta.getPais_o()+"-"+ruta.getNorigen();
		destino = ruta.getDestino();
		ndestino = ruta.getPais_d()+"-"+ruta.getNdestino();
		secuencia = ruta.getSecuencia();
		via = ruta.getVia();
		vian = ruta.getNvia(); 
		viat = ruta.getTvia();
		tiempo = ""+ruta.getTiempo();
		duracion = ""+ruta.getDuracion();
	}
	else{
		ccia = request.getParameter("c_cia");
		origen = request.getParameter("c_origen");
		norigen = request.getParameter("c_norigen");
		destino = request.getParameter("c_destino");
		ndestino = request.getParameter("c_ndestino");
		secuencia = request.getParameter("c_secuencia");
		via = request.getParameter("c_via");
		vian = request.getParameter("c_vian");
		viat = request.getParameter("c_viat");
		tiempo = request.getParameter("c_tiempo");
		duracion = request.getParameter("c_duracion");
		descripcion  = request.getParameter("descripcion");
		descAdd = request.getParameter("descAdd");
		ciuOrig = request.getParameter("ciudad_orig");
		ciuDest = request.getParameter("ciudad_dest");
	}
	
	//variables para las ayudas 
	String path = application.getRealPath(model.menuService.getRealUrl(request.getRequestURI()));
	String datos[] = model.menuService.getContenidoMenu(BASEURL,request.getRequestURI(),path);
%>

<FORM name='forma' id='forma' method='POST' action="<%=CONTROLLER%>?estado=Ruta&accion=Anular&pagina=anularRuta.jsp&modif=ok">
  <table width="539" border="2" align="center">
    <tr>
      <td width="527"><table width="100%" height="34" border="0" align="center" class="tablaInferior">
        <tr>
            <td height="24" colspan="2"  class="subtitulo1"><p align="left">Modificar/Anular Ruta</p></td>
            <td width="365" class="barratitulo"><%=datos[0]%><img src="<%=BASEURL%>/images/titulo.gif" width="32" height="20"></td>
          </tr>
      
        <tr class="fila" >
          <td colspan="2">Distrito </td>
          <td width="365" valign="middle"><%=ccia%>
              <input name="c_cia" type="hidden" id="c_cia" value="<%=ccia%>" class="textbox">
          </td>
        </tr>
        <tr class="fila" >
          <td colspan="2">Origen</td>
          <td width="365" valign="middle"><%=norigen%>
              <input name="c_origen" type="hidden" id="c_origen2" value="<%=origen%>">
              <input name="c_norigen" type="hidden" id="c_norigen" value="<%=norigen%>"></td>
        </tr>
        <tr class="fila" >
          <td colspan="2">Destino</td>
          <td width="365" valign="middle"><%=ndestino%>
              <input name="c_destino" type="hidden" id="c_destino" value="<%=destino%>">
              <input name="c_ndestino" type="hidden" id="c_ndestino" value="<%=ndestino%>">
          </td>
        </tr>
        <tr class="fila" >
          <td colspan="2">Secuencia </td>
          <td><%=secuencia%>
              <input name="c_secuencia" type="hidden" id="c_secuencia" value="<%=secuencia%>"></td>
        </tr>
        <tr class="fila" >
          <td colspan="2">Tiempo</td>
          <td> <%=tiempo%>
              <input name="c_tiempo" type="hidden" id="c_tiempo" value="<%=tiempo%>"></td>
        </tr>
        <tr class="fila" >
          <td colspan="2">Duracion </td>
          <td><%=duracion%>
              <input name="c_duracion" type="hidden" id="c_duracion" value="<%=duracion%>">
          </td>
        </tr>
		<tr class="fila" >
		  <td colspan="2">Descripcion Actual</td>
          <td><%=descripcion%>
              <input name="descripcion" type="hidden" id="descripcion" value="<%=descripcion%>">
          </td>
        </tr>
		
		<tr class="fila" >
		  <td colspan="2">Nueva Descripcion Adicional </td>
          <td>
		      <input name="descAdd" type="text" id="descAdd" value="<%=descAdd%>">
		      <input name="ciudad_orig" type="hidden" id="ciudad_orig" value="<%=ciuOrig%>">
              <input name="ciudad_dest" type="hidden" id="ciudad_dest" value="<%=ciuDest%>">
</td>
        </tr>
				
        <tr class="fila">
          <td colspan="3" align="center" ><table width="100%"  border="0" cellspacing="0" cellpadding="0">
              <tr>
                <td width="31%"><textarea name="c_viat" cols="20" rows="5" readonly class="textbox" id="c_viat"><%=viat%></textarea>
                </td>
                <td width="2%">&nbsp;</td>
                <td width="67%"><textarea name="c_vian" cols="49" rows="5" readonly class="textbox" id="c_vian"><%=vian%></textarea></td>
              </tr>
            </table>
              <br>
              <input name="c_via" id="c_via" type="hidden" value="<%=via%>">              
			  <img src="<%=BASEURL%>/images/botones/eliminar.gif"  name="imgeliminar" onClick="borrarTramoAnular();" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" style="cursor:hand"> 
			  </td>
        </tr>
        <tr class="fila" >
          <td colspan="3" >Tramo&nbsp;&nbsp;
              <%TreeMap tramos= model.tramoService.getLtramos();%>
              <input name="pagina" type="hidden" id="pagina" value="<%=CONTROLLER%>?estado=Cargar&accion=Variosruta&carpeta=jsp/trafico/ruta&pagina=anularRuta.jsp&anular=ok">
              <input:select name="tramo" options="<%=tramos%>" attributesText="id='c_tramo' style='width:80%'; onChange='escogeTramo(this.value)'; class='textbox';" /></td>
        </tr>
        <tr class="fila">
          <td colspan="3" align="center">&nbsp;</td>
        </tr>        
      </table></td>
    </tr>
  </table>
</form>
<center class='comentario'>
  <table align="center">
  <td colspan="2" align="center"> 
  <img src="<%=BASEURL%>/images/botones/modificar.gif"  name="imgmodificar" onClick="if(validarRuta()){forma.submit();}" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" style="cursor:hand"> 
  <img src="<%=BASEURL%>/images/botones/anular.gif"  name="imganular" onClick="window.location='<%=CONTROLLER%>?estado=Ruta&accion=Anular&pagina=anularRuta.jsp&c_cia=<%=ccia%>&c_origen=<%=origen%>&c_secuencia=<%=secuencia%>&c_destino=<%=destino%>'" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" style="cursor:hand"> 
  <img src="<%=BASEURL%>/images/botones/salir.gif"  name="imgsalir" onClick="parent.close();" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" style="cursor:hand"> 
  </td>
  </table>
  <%
  String Mensaje = ((String)request.getAttribute("mensaje")!=null)?(String)request.getAttribute("mensaje"):"";
  if(!Mensaje.equals("")){%>
  <p>
  <table border="2" align="center">
    <tr>
      <td><table width="100%"  border="1" align="center"  bordercolor="#F7F5F4" bgcolor="#FFFFFF">
          <tr>
            <td width="229" align="center" class="mensajes"><%=Mensaje%></td>
            <td width="29" background="<%=BASEURL%>/images/cuadronaranja.JPG">&nbsp;</td>
            <td width="58">&nbsp;</td>
          </tr>
      </table></td>
    </tr>
  </table>
  <p></p>
  <p>
    <%}%>
  </center>
  </div>
   <%=datos[1]%>
</body>
</html>
