<%@page contentType="text/html"%>
<%@page session="true"%>
<%@page import="java.util.*"%>
<%@page import="com.tsp.operation.model.*"%>
<%@page import="com.tsp.operation.model.beans.*"%>
<%@page import="com.tsp.util.*"%>
<%@include file="/WEB-INF/InitModel.jsp"%>
<%
    String opc = request.getParameter( "opc" );    
    if(opc.equals( "2" ) ){
        %>
        <div id="validate" align="center" style="visibility:hidden" class="informacion"><img src="<%=BASEURL%>/images/cargando.gif" align="middle">&nbsp;&nbsp;Validando Tramos...</div>
<%
        Vector vec = model.rutaService.getVRutas();
        for( int i = 0; i < vec.size(); i++ ){
            Ruta rut = (Ruta)vec.get( i );
%>            
            <input type=checkbox name="pcontrol" name="pcontrol<%= i %>" value="<%= rut.getOrigen() %>" > <%= rut.getDescripcion() %><br>
<%      }
    }
    if(opc.equals( "3" ) ){
        Vector errores = (Vector)request.getAttribute( "errores" );
        out.print( "var errores='No existen los siguientes tramos: ';" );
        if( errores.size() > 0 ){
            for( int i = 0; i < errores.size(); i++ ){
                String error = (String)errores.get( i );
%>
                errores = errores + "\n <%= error %>";
<%
            }
            out.print( "alert( errores );" );
        }else{
            Vector selects = (Vector)request.getAttribute( "selects" );
            if( selects.size() == 0 ){
                out.print( "alert( 'No se seleccionó ninguna ruta' );" );
            }else{
                out.print( "opener.forma.c_via.value = '';" );
                out.print( "opener.forma.c_viat.value = '';" );
                out.print( "opener.forma.c_vian.value = '';" );
                for( int i = 0; i < selects.size(); i++ ){
                    Ciudad ciudad = (Ciudad)selects.get( i );
%>
                    opener.forma.c_via.value = opener.forma.c_via.value + "<%= ciudad.getCodCiu() %>";
                    opener.forma.c_viat.value= opener.forma.c_viat.value + "\n<%= ciudad.getCodCiu() %>";
                    opener.forma.c_vian.value= opener.forma.c_vian.value + "\n<%= ciudad.getNombre() %>";                    
<%
                }
                out.print( "opener.forma.action =  '" + CONTROLLER + "?estado=Cargar&accion=Variosruta&carpeta=jsp/trafico/ruta&pagina=ingresarRuta.jsp';" );
                out.print( "opener.forma.submit();" );
                out.print( "window.self.close();" );
            }
        }
    }%>