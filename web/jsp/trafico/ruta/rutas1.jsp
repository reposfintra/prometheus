<%@page contentType="text/html"%>
<%@page session="true"%>
<%@page import="java.util.*"%>
<%@page import="com.tsp.operation.model.*"%>
<%@page import="com.tsp.operation.model.beans.*"%>
<%@page import="com.tsp.util.*"%>
<%@include file="/WEB-INF/InitModel.jsp"%>
<%@ taglib uri="http://jsptags.com/tags/navigation/pager" prefix="pg" %>
<html>
<head>
<title>COMPA&Ntilde;IAS</title>
<link href="<%= BASEURL %>/css/estilo.css" rel='stylesheet'>
<link href="../css/estilo.css" rel="stylesheet" type="text/css">
</head>
<body>
<%
String style = "simple";
String position = "bottom";
String index = "center";
int maxPageItems = 10;
int maxIndexPages = 10;

Vector rutas = model.rutaService.getRutas();
%>
<table width="100%" border="1" cellspacing="1" cellpadding="5" align="center" bordercolor="#B7D0DC">
  <tr class="titulo" align="center" height="20">
	<td colspan="5">Rutas
	</td>
  </tr>
  <tr class="subtitulos" align="center" height="20">
    <td nowrap width="32%"><div align="center">Origen</div></td>
    <td nowrap width="32%"><div align="center">Destino</div></td>
    <td nowrap width="10%">Secuencia</td>
    <td nowrap width="54%">Ruta</td>
    <td nowrap width="10%"><div align="center">Distrito</div></td>
  </tr>
  <pg:pager
    items="<%=rutas.size()%>"
    index="<%= index %>"
    maxPageItems="<%= maxPageItems %>"
    maxIndexPages="<%= maxIndexPages %>"
    isOffset="<%= true %>"
    export="offset,currentPageNumber=pageNumber"
    scope="request">
  <% 
     for (int i = offset.intValue(), l = Math.min(i + maxPageItems, rutas.size()); i < l; i++){
        Ruta ruta = (Ruta) rutas.elementAt(i);
  %>
  <pg:item>
  <tr class="letra"  onMouseOver="bgColor= '#99cc99'" onMouseOut="bgColor='#ECE0D8'" bgcolor="#ECE0D8" style="cursor:hand" title="Mostrar informacion del tramo" onClick="window.open('<%=CONTROLLER%>?estado=Buscar&accion=Rutas&carpeta=jsp/trafico/ruta&pagina=anularRuta.jsp&origen=<%=ruta.getOrigen()%>&destino=<%=ruta.getDestino()%>&cia=<%=ruta.getCia()%>&secuencia=<%=ruta.getSecuencia()%>&sw=3&lista=ok','','status=yes,scrollbars=yes,width=500,height=500,resizable=yes')">
    <td nowrap align="center"><%=ruta.getNorigen()%> (<%=ruta.getPais_o().substring(0,3)%>)</td>
	<td align="center" nowrap><%=ruta.getNdestino()%> (<%=ruta.getPais_d().substring(0,3)%>)</td>
    <td align="center" nowrap><%=ruta.getSecuencia()%></td>
    <td nowrap title="<%=ruta.getVia()%>"><%=ruta.getNvia()%></td>
    <td nowrap align="center"><%=ruta.getCia()%></td>
	
  </tr>
  </pg:item>
  <%}%>
  <tr class="pie" align="center">
 	<td height="20" colspan="10" nowrap>
	<pg:index>
	<jsp:include page="../../../WEB-INF/jsp/googlesinimg.jsp" flush="true"/>
	</pg:index>
	</td>
  </tr>
  </pg:pager>
</table>
<br>
<a href="<%=CONTROLLER%>?estado=Menu&accion=Cargar&carpeta=/jsp/trafico/ruta&pagina=buscarRuta.jsp" class="letra_resaltada"  target="_self">Atras</a>
</body>
</html>