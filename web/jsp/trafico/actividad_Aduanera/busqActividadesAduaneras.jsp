<%@ page session="true"%>
<%@ page errorPage="../error/error.jsp"%>
<%@page import="com.tsp.operation.model.beans.*, java.util.*,java.text.DecimalFormat"%>
<%@ include file="/WEB-INF/InitModel.jsp"%>
<%@ taglib uri="http://jakarta.apache.org/taglibs/input-0.90" prefix="input" %>

<html>
<head>
    <title>Agencia</title>
    <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
    <script src="<%=BASEURL%>/js/validar.js"></script>
	<script src='<%=BASEURL%>/js/date-picker.js'></script>
    <script language="JavaScript" type="text/javascript" src="<%=BASEURL%>/js/boton.js"></script>
	<link  href='<%=BASEURL%>/css/estilostsp.css' rel='stylesheet'>
	<link  href='../../../css/estilostsp.css' rel='stylesheet'>
</head>

<body onResize="redimensionar()" onLoad="redimensionar()">
<div id="capaSuperior" style="position:absolute; width:100%; height:100px; z-index:0; left: 1px; top: 0px;">
 <jsp:include page="/toptsp.jsp?encabezado=Control de Actividades Aduanales"/>
</div>
<div id="capaCentral" style="position:absolute; width:100%; height:87%; z-index:0; left: 0px; top: 100px; overflow: scroll;">

    <form name="form2" method="post" action="<%=CONTROLLER%>?estado=Actividadesaduaneras&accion=Reporte" onSubmit="return validarFechas(this);">
      <table width="500" border="2" align="center">
        <tr>
          <td><table width="100%"  border="0">
            <tr>
              <td width="48%" class="subtitulo1">Datos de Busqueda </td>
              <td width="52%" class="barratitulo"><img src="<%=BASEURL%>/images/titulo.gif" width="32" height="20"></td>
            </tr>
          </table>
            <table width="100%" class="tablaInferior">
            
            <tr class="fila">
              <td width="177" nowrap>Fecha Inicio </td>
              <td width="287" nowrap><input name='fechai' type='text' class="textbox" id="fechai" style='width:120' value='' readonly>                <img src="<%=BASEURL%>/images/cal.gif" width="16" height="16" align="absmiddle" style="cursor:hand " onclick="if(self.gfPop)gfPop.fPopCalendar(document.form2.fechai);return false;" HIDEFOCUS></td>
            </tr>
            <tr class="fila">
              <td nowrap>Fecha Fin </td>
              <td nowrap>
                <input name='fechaf' type='text' class="textbox" id="fechaf" style='width:120' value='' readonly>                <img src="<%=BASEURL%>/images/cal.gif" width="16" height="16" align="absmiddle" style="cursor:hand " onclick="if(self.gfPop)gfPop.fPopCalendar(document.form2.fechaf);return false;" HIDEFOCUS></td>
            </tr>
          </table></td>
        </tr>
      </table>
      <div align="center"><br>
      <input type="image" name="Submit" src="<%=BASEURL%>/images/botones/aceptar.gif" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);">
     <img src="<%=BASEURL%>/images/botones/salir.gif"  name="salir" title="Salir..." onClick="window.close();" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);"></div>
    </form>

   
</div>
<iframe width=188 height=166 name="gToday:normal:agenda.js:gfPop:plugins.js" id="gToday:normal:agenda.js:gfPop:plugins.js" src="<%=BASEURL%>/js/Calendario/ipopeng.htm" scrolling="no" frameborder="0" style="visibility:visible; z-index:999; position:absolute; left:-500px; top:0px;"> </iframe>    

</body>
</html>
