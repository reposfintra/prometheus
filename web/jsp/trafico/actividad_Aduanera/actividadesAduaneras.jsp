<%@page contentType="text/html"%>
<%@page session="true"%>
<%@page errorPage="/error/ErrorPage.jsp"%>
<%@page import="java.util.*"%>
<%@page import="com.tsp.operation.model.*"%>
<%@page import="com.tsp.util.*"%>
<%@page import="com.tsp.operation.model.beans.*, java.util.*"%>
<%@include file="/WEB-INF/InitModel.jsp"%>
<%@taglib uri="http://jsptags.com/tags/navigation/pager" prefix="pg" %>

<html> 
<head>
<title></title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link href="<%= BASEURL %>/css/estilostsp.css" rel="stylesheet" type="text/css"> 
<link href="../../../css/estilostsp.css" rel="stylesheet" type="text/css">
</head>
 
<script type='text/javascript' src="<%= BASEURL %>/js/reporte.js"></script>
<script type='text/javascript' src="<%= BASEURL %>/js/boton.js"></script>
<body onResize="redimensionar()" onLoad="redimensionar()">
<div id="capaSuperior" style="position:absolute; width:100%; height:100px; z-index:0; left: 1px; top: 0px;">
 <jsp:include page="/toptsp.jsp?encabezado=Control de Actividades Aduanales"/>
</div>
<div id="capaCentral" style="position:absolute; width:100%; height:87%; z-index:0; left: 0px; top: 100px; overflow: scroll;">

  <%String style = "simple";
    String position =  "bottom";
    String index =  "center";
    int maxPageItems = 10;
    int maxIndexPages = 10;

	List Listado = model.controlactAduaneraService.getActividadAduanera();
	if ( Listado.size() >0 ){
  %>
<form name="form1" method="post" action="">
<table width="1100" border="2" align="center">
      <tr>
        <td><table width="100%" align="center">
          <tr>
            <td width="48%" class="subtitulo1">&nbsp;Repoorte de Remesas </td>
            <td width="52%" class="barratitulo"><img src="<%=BASEURL%>/images/titulo.gif" width="32" height="20">              </td>
          </tr>
        </table>
          <table width="99%" border="1" align="center" bordercolor="#999999" bgcolor="#F7F5F4">
          <tr class="tblTitulo" align="center">
            <td width="7%">Remesa</td>
            <td width="8%">Fecha</td>
            <td width="25%" >Cliente</td>
            <td width="5%">Factura comercial </td>
			<td width="25%">Ruta</td>
			<td width="35%">Standart</td>
			
          </tr>
          <pg:pager
			items="<%=Listado.size()%>"
			index="<%= index %>"
			maxPageItems="<%= maxPageItems %>"
			maxIndexPages="<%= maxIndexPages %>"
			isOffset="<%= true %>"
			export="offset,currentPageNumber=pageNumber"
			scope="request">
          <%-- keep track of preference --%>
          <%
        for (int i = offset.intValue(), l = Math.min(i + maxPageItems, Listado.size()); i < l; i++) {
            ControlactAduanera actaduana = (ControlactAduanera) Listado.get(i);
         %>
          <pg:item>
          <tr class="<%=(i % 2 == 0 )?"filagris":"filaazul"%>" onMouseOver='cambiarColorMouse(this)'   style="cursor:hand"  
		   onClick="window.open('<%=CONTROLLER%>?estado=Actaduanera&accion=Buscarplan&numrem=<%=actaduana.getRemesa()%>' ,'','status=yes,scrollbars=no,width=650,height=650,resizable=yes');">
            <td height="20" class="bordereporte"  ><%=actaduana.getRemesa()%></td>
            <td height="20" class="bordereporte" ><%=actaduana.getFecrem()%></td>
            <td height="20" class="bordereporte" ><%=actaduana.getCliente()%></td>
            <td height="20" class="bordereporte" ><%=actaduana.getFacComercial()%></td>
  		    <td height="20" class="bordereporte" ><%=actaduana.getOrirem()%> - <%=actaduana.getDesrem()%></td>
			<td height="20" class="bordereporte" ><%=actaduana.getDesstdjob()%></td>
          </tr>
          </pg:item>
          <%}%>
          <tr class="bordereporte">
            <td colspan="6" align="center" valign="middle" >
              <div align="center"><pg:index>
                <jsp:include page="../../../WEB-INF/jsp/googlesinimg.jsp" flush="true"/>        
    </pg:index></div></td>
          </tr>
          </pg:pager>
        </table></td>
      </tr>
  </table>
</form>
  <%}
 else { %>
  <table border="2" align="center">
    <tr>
      <td><table width="410" height="40" border="1" align="center"  bordercolor="#F7F5F4" bgcolor="#FFFFFF">
          <tr>
            <td width="282" align="center" class="mensajes">Su b&uacute;squeda no arroj&oacute; resultados!</td>
            <td width="28" background="<%=BASEURL%>/images/cuadronaranja.JPG">&nbsp;</td>
            <td width="78">&nbsp;</td>
          </tr>
      </table></td>
    </tr>
  </table>
  <p>
      <%}%>
</p>
  <table width="900" border="0" align="center">
    <tr>
      <td><img src="<%=BASEURL%>/images/botones/regresar.gif"  name="imgaceptar" onclick="window.location='<%=CONTROLLER%>?estado=Menu&accion=Cargar&pagina=busqActividadesAduaneras.jsp&carpeta=/jsp/trafico/actividad_Aduanera&mensaje=&marco=no'" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" style="cursor:hand"></td>
    </tr>
  </table>
 </div>
</body>
</html>
