<%@page session="true"%>
<%@page import="java.util.*" %>
<%@page import="com.tsp.operation.model.*"%>
<%@include file="/WEB-INF/InitModel.jsp"%>
<%@page import="com.tsp.operation.model.beans.*"%>
<%@page import="com.tsp.util.*"%>
<%@ taglib uri="http://jakarta.apache.org/taglibs/input-0.90" prefix="input" %>
<html>
<head>  
<title>Untitled Document</title> 
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link href="css/estilostsp.css" rel="stylesheet" type="text/css">  
<link href="../../../css/estilostsp.css" rel="stylesheet" type="text/css"> 
<script language="JavaScript" type="text/javascript" src="<%=BASEURL%>/js/validar.js"></script>
<script type='text/javascript' src="<%= BASEURL %>/js/boton.js"></script>
<script type='text/javascript' src="<%= BASEURL %>/js/hvida.js"></script>
<script type='text/javascript' src="<%= BASEURL %>/js/referencia.js"></script>


</head>
<body onResize="redimensionar();" onLoad='redimensionar();' > 


<%   
   //--- variables de bloqueos
  	    String hoy  = Utility.getDate(8).substring(0,10);
        String estado = (request.getParameter("estado")!=null)?request.getParameter("estado"):"";
        String comentario = (request.getParameter("comentario")!=null)?request.getParameter("comentario"):"";
		String reg = (request.getParameter("reg")!=null)?request.getParameter("reg"):"";
		String tit = (estado.equals("true"))?"Modificar ":"Registrar";
		String titulo =  "/toptsp.jsp?encabezado="+tit+"Conductor";
		String huella1= "";
		String huella2= "";
        //nuevo   
       
		String act="003";
        String tipo = "";
        String rh = "";
		//*****
		
     
   //--- objetos
      Conductor conductor = model.conductorService.obtConductor();
      Identidad iden = model.identidadService.obtIdentidad();
	  Ciudad ciu;
    %>
<div id="capaSuperior" style="position:absolute; width:100%; height:100px; z-index:0; left: 0px; top: 0px;">
 <jsp:include page="<%=titulo%>"/>
</div>
<div id="capaCentral" style="position:absolute; width:100%; height:87%; z-index:0; left: 0px; top: 100px; overflow: scroll;">
<FORM ACTION="<%=CONTROLLER%>?estado=Conductor&accion=Evento<%= request.getParameter("proveedor")!=null ? "&proveedor=OK" : "" %>" METHOD='post' id='formulario' name='formulario'>
 <%if(iden==null){%>
  <table width="332" border="2" align="center">
    <tr>
      <td><table width="100%" class="tablaInferior">
        <tr>
          <td width="155" class="subtitulo1">&nbsp;Buscar Persona</td>
          <td width="184" class="barratitulo"><img src="<%=BASEURL%>/images/titulo.gif" width="32" height="20"></td>
        </tr>
        <tr class="fila">
          <td>Cedula</td>
          <td><input  class='textbox' type='text' name='identificacion' size='15' maxlength='15' onKeyPress="soloDigitos(event,'decNO');"  onKeyUp="if(event.keyCode == 13 && formulario.identificacion.value!='' ){formulario.submit();}" >
            <input type="hidden" name="evento" value="Verificar">
            <img src="<%=BASEURL%>/images/botones/iconos/lupa.gif" width="20" height="20" onClick="if(formulario.identificacion.value!=''){ formulario.submit();}else{alert('Digite la cedula');}" style="cursor:hand "></td>
        </tr>
      </table></td>
    </tr>
  </table>
 <br>
  <div align="center"><img src="<%=BASEURL%>/images/botones/salir.gif"  name="imgsalir"  onMouseOver="botonOver(this);" onClick="parent.close();" onMouseOut="botonOut(this);" style="cursor:hand"></div>
  <%}else{%>
  
  <table width="700" border="2" align="center">
    <tr>
      <td>          <table width="100%" class="tablaInferior">
            <tr>
              <td colspan="3" class="subtitulo1"> Datos Personales</td>
              <td colspan="2" class="barratitulo"><img src="<%=BASEURL%>/images/titulo.gif"></td>
            </tr>
            <tr class="fila">
              <td width="127">Estado del Conductores</td>
              <td width="114"><%=(conductor!=null && conductor.getEstado().equals("A"))?"Activo":"Inactivo"%>
                <input name="estadoConductor" type="hidden" id="estadoConductor" value="<%=(conductor!=null)?conductor.getEstado():""%>">
                <input name="hoy" type="hidden" id="hoy" value="<%=hoy%>">
</td>
              <td width="88">Estado del registro Nit</td>
                <td width="151"><%=(iden.getEstado().equals("A"))?"Activo":"Inactivo"%>
                  <input name="estadoNit" type="hidden" id="estadoNit" value="<%=(iden.getEstado()!=null)?iden.getEstado():""%>"></td>
              <td width="184" align="center" class="letraresaltada" ><img src="<%=BASEURL%>/images/botones/iconos/Foto.jpg" alt="Adjuntar Foto" width="30" height="37" style="cursor:hand" onClick="AdjuntarDoc('<%=CONTROLLER%>','','<%=act%>','032');" ></td>
            </tr>
            <tr>
              <td width="127" class="fila">Cedula</td>
              <td colspan="2" class="letra"><% String ced=iden.getCedula();%>
                  <input name='identificacion' type='hidden'  class='textbox' id="identificacion" value='<%=ced%>' size='15' maxlength='15' ><%=ced%>
                  <img src="<%=BASEURL%>/images/botones/iconos/documento.gif" alt="Adjuntar Imagen" width="20" height="15" style="cursor:hand" 
						  onClick="AdjuntarDoc('<%=CONTROLLER%>',1,'<%=act%>','011');">  </td>
              <td width="151" class="fila">Expedicion</td>
              <td width="184" class="letra"><%String expced =(iden.getExpced()!=null)?iden.getExpced():"";%>
                  <%=model.ciudadService.obtenerNombreCiudad(expced)%>
            </tr>
            <tr >
              <td align="left" class="fila">Libreta Militar</td>
              <td colspan="2" valign="middle" class="letra">
                <%String lib =(iden.getLibmilitar()!=null)?iden.getLibmilitar():"";%>
                <%=lib%>
                <img src="<%=BASEURL%>/images/botones/iconos/documento.gif" alt="Adjuntar Imagen" width="20" height="15" 
						  onClick="AdjuntarDoc('<%=CONTROLLER%>',2,'<%=act%>','024');" style="cursor:hand"></td>
              <td colspan="2" valign="middle" class="fila">&nbsp;</td>
            </tr>
            <tr>
              <td class="fila">Nombres Primero</td>
              <td colspan="2" class="letra">
                <%String nom1 =(iden.getNom1()!=null)?iden.getNom1():"";%>
                <%=nom1%>
              </td>
              <td class="fila">Segundo </td>
              <td class="letra">
                <%String nom2 =(iden.getNom2()!=null)?iden.getNom2():"";%>
                <%=nom2%>
              </td>
            </tr>
            <tr>
              <td width="127" align="left"  class="fila" >Apellidos Primero </td>
              <td colspan="2" valign="middle" class="letra">
                <%String ape1 =(iden.getApe1()!=null)?iden.getApe1():"";%>
                <%=ape1%>
              </td>
              <td valign="middle" class="fila">Segundo</td>
              <td valign="middle" class="letra">
                <%String ape2 =(iden.getApe2()!=null)?iden.getApe2():"";%>
                <%=ape2%>
              </td>
            </tr>
            <tr>
              <td width="127"  class="fila">Genero</td>
              <td colspan="2" valign="middle" class="letra">
                <%String genero =(iden.getSexo()!=null)?iden.getSexo():"";%>
                <%if (genero.equals("M")){
				           genero ="Masculino";
				    }
					else if (genero.equals("F")){ 
                           genero="Femenino";
                    }
                  %>
                <%=genero%></td>
              <td valign="middle"  class="fila">Estado Civil</td>
              <td valign="middle" class="letra">
                <%String estcivil =(iden.getEst_civil()!=null)?iden.getEst_civil():"";
                     if (estcivil.equals("S")){
					 	 estcivil ="Soltero";
				     }
                     else if (estcivil.equals("C")){ 
					     estcivil="Casado";
				     }
                     else if (estcivil.equals("V")){
					     estcivil = "Viudo";
					 }
			         else if (estcivil.equals("E")){
					      estcivil = "Separado";
					 }
                     else if (estcivil.equals("U")){
					      estcivil = "Union Libre";
					 }%>
                <%=estcivil%>
              </td>
            </tr>
            <tr class="fila" >
              <td width="127">Grupo Sangu&iacute;neo</td>
              <td colspan="2" valign="middle">
                <% if (conductor!=null && !conductor.getRh().equals("")){
                      if(conductor.getRh().replaceAll(" ", "").length() == 2){
                           tipo = conductor.getRh().replaceAll(" ", "").substring(0,1);
                           rh = conductor.getRh().replaceAll(" ", "").substring(1,2); 
                      }
					  else if(conductor.getRh().replaceAll(" ", "").length() == 3){
                           tipo = conductor.getRh().replaceAll(" ", "").substring(0,2);
                           rh = conductor.getRh().replaceAll(" ", "").substring(2,3);               
                      }
                  }%>
                <select name='grupoSanguineo' class="textbox">
                  <option value='A ' <%= (tipo.equals("A"))?"selected":"" %>>A</option>
                  <option value='B ' <%= (tipo.equals("B"))?"selected":""  %> >B</option>
                  <option value='O ' <%= (tipo.equals("O"))?"selected":""  %> >O</option>
                  <option value='AB' <%= (tipo.equals("AB"))?"selected":"" %> >AB</option>
              </select>
                <img src="<%= BASEURL%>/images/botones/iconos/obligatorio.gif" width="10" height="10"></td>
              <td valign="middle">RH</td>
              <td valign="middle"><select name='rh' class="textbox">
                  <option value='+' <%= (rh.equals("+"))?"selected":"" %> >+</option>
                  <option value='-' <%= (rh.equals("-"))?"selected":"" %> >-</option>
              </select>
                <img src="<%= BASEURL%>/images/botones/iconos/obligatorio.gif" width="10" height="10"></td>
            </tr>
            <tr>
              <td width="127"  class="fila" >Fecha de Nacimiento</td>
              <td colspan="2" valign="middle" class="letra"><span class="comentario">
                <% String est4=(iden.getFechanac()!=null )?iden.getFechanac():""; %>
                <%= est4%>
                <a href="javascript:void(0)" onClick="if(self.gfPop)gfPop.fPopCalendar(fechaNacimiento);return false;" hidefocus>  </a> </span></td>
              <td valign="middle" class="fila">Lugar Nacimiento</td>
              <td valign="middle" class="letra"><%String lug =(iden.getLugarnac()!=null)?iden.getLugarnac():"";%>
                  <%=model.ciudadService.obtenerNombreCiudad(lug)%>
              </td>
            </tr>
            <tr>
              <td align="left" class="fila">Direcci&oacute;n</td>
              <td colspan="4" valign="middle" class="letra"><%String dir =(iden.getDireccion()!=null)?iden.getDireccion():"";%>
                  <%=dir%></td>
            </tr>
            <tr>
              <td align="left" class="fila">Barrio</td>
              <td colspan="2" valign="middle" class="letra"><%String barrio =(iden.getBarrio()!=null)?iden.getBarrio():"";%>
                  <%=barrio%></td>
              <td valign="middle" class="fila">Celular</td>
              <td valign="middle" class="letra"><%String cel =(iden!=null && iden.getCelular()!=null)?iden.getCelular():"";%>
                  <%=cel%></td>
            </tr>
            <tr>
              <td class="fila" >Telefonos</td>
              <td colspan="2" valign="middle" class="fila">
                <table width="100%" border="0" cellpadding="0" cellspacing="0" >
                    <tr class="fila">
                      <td width="20%">Pais</td>
                      <td width="20%">Area</td>
                    <td width="60%">Numero</td></tr>
                    <tr class="letra">
                      <td><%String t11 =(iden.getPais1()!=null)?iden.getPais1():"";%>
                      <%=t11%></td>
                      <td width="20%"><%String t12 =(iden.getArea1()!=null)?iden.getArea1():"";%>
                      <%=t12%></td>
                      <td width="60%"><%String t13 =(iden!=null && iden.getnum1()!=null)?iden.getnum1():"";%> <%=t13%></td>
                    </tr>
                </table></td>
              <td colspan="2" valign="middle" class="fila">
                <% if ( !iden.getPais2().trim().equals("") ) {%>
               <table width="100%" border="0" cellpadding="0" cellspacing="0" >
                <tr class="fila">
                      <td width="20%">Pais</td>
                      <td width="20%">Area</td>
                    <td width="60%">Numero</td></tr> 
                <tr class="letra">
                    <td width="20%"><%String t21 =(iden!=null && iden.getPais2()!=null)?iden.getPais2():"";%>
                      <%=t21%></td>
                    <td width="20%"><%String t22 =(iden!=null && iden.getArea2()!=null)?iden.getArea2():"";%>
                      <%=t22%></td>
                    <td width="60%"><%String t23 =(iden!=null && iden.getnum2()!=null)?iden.getnum2():"";%>
                      <%=t23%></td>
                  </tr>
                </table><%}%></td>
            </tr>
            <tr>
              <td width="127" align="left" class="fila">Email</td>
              <td colspan="2" valign="middle" class="letra"><%String em =(iden.getE_mail()!=null)?iden.getE_mail():"";%>
                  <%=em%>
              </td>
              <td valign="middle" class="fila">Pais</td>
              <td valign="middle" class="letra"><%ciu =  model.ciudadService.obtenerCiudad();%>             
								    <%=(ciu !=null && ciu.getpais_name()!=null)?ciu.getpais_name():""%></td>
            </tr>
            <tr>
              <td align="left"  class="fila">Departamento</td>
              <td colspan="2" valign="middle" class="letra"><%=(ciu !=null && ciu.getdepartament_name() !=null)?ciu.getdepartament_name():""%>
              </td>
              <td valign="middle" class="fila">Ciudad</td>
              <td valign="middle" class="letra"><%=(ciu !=null && ciu.getNomCiu()!=null)?ciu.getNomCiu():""%></td>
            </tr>
            <tr>
              <td align="left"  class="fila" >Se&ntilde;al Particular </td>
              <td colspan="4" valign="middle" class="letra"><% String est12=(iden.getSenalParticular()!=null)?iden.getSenalParticular():""; %>
                  <%= est12%></td>
            </tr>
      </table></td></tr>
  </table>
  <table width="700" border="2" align="center">
    <tr>
      <td><table width="100%" class="tablaInferior">
          <tr>
            <td colspan="2" class="subtitulo1">Documentaci&oacute;n </td>
            <td colspan="2" class="barratitulo"><img src="<%=BASEURL%>/images/titulo.gif" width="32" height="20"></td>
          </tr>
          <tr class="fila">
            <td width="143">N&uacute;mero Pase</td>
            <td width="177"><% String est5=(conductor!=null && !conductor.getPassNo().equals(""))?conductor.getPassNo():""; %>
                <input class='textbox' value='<%= est5%>' type='text' name='numeroPase' size='20' maxlength='20' onKeyPress="soloDigitos(event,'decNO')">
                <img src="<%=BASEURL%>/images/botones/iconos/documento.gif" alt="Adjuntar Imagen" width="20" height="15" 
						  onClick="AdjuntarDoc('<%=CONTROLLER%>',6,'<%=act%>','023');" style="cursor:hand"> <img src="<%= BASEURL%>/images/botones/iconos/obligatorio.gif" width="10" height="10"></td>
            <td width="139">Categoria</td>
            <td width="189"><% int est6=(conductor!=null && !conductor.getPassCat().equals(""))?Integer.parseInt(conductor.getPassCat()):0; %>
                <select name='categoriaPase' class="textbox">
                  <%
                                                    for(int i=1;i<=6;i++){
                                                     String select=(i==est6)?"selected='selected'" :"";%>
                  <option  value='<%=i%>' <%= select %>><%= i%></option>
                  <%}%>
              </select> <img src="<%= BASEURL%>/images/botones/iconos/obligatorio.gif" width="10" height="10"></td>
          </tr>
          <tr class="fila">
            <td>Fecha Vencimiento</td>
            <td><span class="comentario">
              <% String vpase=(conductor!=null && !conductor.getPassExpiryDate().equals("0099-01-01"))?conductor.getPassExpiryDate():""; %>
              <input name='VencePase' type='text' class='textbox' id="VencePase" value="<%=vpase%>"  size='10' maxlength='10'  readonly='readonly'>
              <a href="javascript:void(0)" onClick="if(self.gfPop)gfPop.fPopCalendar(VencePase);return false;" hidefocus> <img name="popcal" align="absmiddle" src="<%=BASEURL%>\js\Calendario\cal.gif" width="16" height="16" border="0" alt=""> </a><img src="<%= BASEURL%>/images/botones/iconos/obligatorio.gif" width="10" height="10"> </span> </td>
            <td> Restricciones del pase </td>
            <td><% String resp=(conductor!=null && !conductor.getRes_pase().equals("") && conductor.getRes_pase()!=null)?conductor.getRes_pase():""; %>
			  <select name="respase" class="textbox" id="respase" style="width:90% ">
                  <option value="">Seleccione Un Item</option>
               <% LinkedList tblres = model.tablaGenService.obtenerTablas();
               for(int i = 0; i<tblres.size(); i++){
                       TablaGen respa = (TablaGen) tblres.get(i); %>
                       <option value="<%=respa.getTable_code()%>" <%=(respa.getTable_code().equals(resp))?"selected":""%> ><%=respa.getDescripcion()%></option>
             <%}%>
     </select> <img src="<%= BASEURL%>/images/botones/iconos/obligatorio.gif" width="10" height="10">
            </td>
          </tr>
          <tr class="fila">
            <td>Pasaporte</td>
            <td><% String pasa=(conductor!=null && !conductor.getPassport().equals(""))?conductor.getPassport():""; %>
                <input name='pasaporte' type='text' class='textbox' id="pasaporte" onKeyPress="soloDigitos(event,'decNO')" value="<%=pasa%>" size='20' maxlength='15'>
                <img src="<%=BASEURL%>/images/botones/iconos/documento.gif" alt="Adjuntar Imagen" width="20" height="15" 
						  onClick="AdjuntarDoc('<%=CONTROLLER%>',8,'<%=act%>','030');" style="cursor:hand"></td>
            <td>Fecha Vencimiento</td>
            <td><% String vpasa=(conductor!=null && !conductor.getPassportExpiryDate().equals("0099-01-01"))?conductor.getPassportExpiryDate():""; %>
                <input  name='VencePasaporte' type='text' class='textbox' id="VencePasaporte" value='<%= vpasa%>' size='10' maxlength='10'   readonly='readonly'>
                <a href="javascript:void(0)" onClick="if(self.gfPop)gfPop.fPopCalendar(VencePasaporte);return false;" hidefocus> <img name="popcal" align="absmiddle" src="<%=BASEURL%>\js\Calendario\cal.gif" width="16" height="16" border="0" alt=""> </a> </td>
          </tr>
          <tr class="fila">
            <td height="31">Certificado Judicial </td>
            <td><% String jud=(conductor!=null && !conductor.getNrojudicial().equals(""))?conductor.getNrojudicial():""; %>
                <input name="Judicial" type="text" class="textbox" id="Judicial" onKeyPress="soloDigitos(event,'decNO')" value="<%=jud%>" size="20" maxlength="15">
                <img src="<%=BASEURL%>/images/botones/iconos/documento.gif" alt="Adjuntar Imagen" width="20" height="15" 
						  onClick="AdjuntarDoc('<%=CONTROLLER%>',3,'<%=act%>','025');" style="cursor:hand"> <img src="<%= BASEURL%>/images/botones/iconos/obligatorio.gif" width="10" height="10"></td>
            <td>Fecha Vencimiento</td>
            <td><% String vjud=(conductor!=null && !conductor.getVencejudicial().equals("0099-01-01"))?conductor.getVencejudicial():""; %>
                <input  name='VenceJudicial' type='text' class='textbox' id="VenceJudicial" value="<%=vjud%>" size='10' maxlength='10'   readonly='readonly'>
                <a href="javascript:void(0)" onClick="if(self.gfPop)gfPop.fPopCalendar(VenceJudicial);return false;" hidefocus> <img name="popcal" align="absmiddle" src="<%=BASEURL%>\js\Calendario\cal.gif" width="16" height="16" border="0" alt=""> </a><img src="<%= BASEURL%>/images/botones/iconos/obligatorio.gif" width="10" height="10"> </td>
          </tr>
          <tr class="fila">
            <td height="31">Visa</td>
            <td><% String vi=(conductor!=null && !conductor.getNrovisa().equals(""))?conductor.getNrovisa():""; %>
                <input name="visa" type="text" class="textbox" id="visa" onKeyPress="soloDigitos(event,'decNO')" value="<%=vi%>" size="20" maxlength="15">
                <img src="<%=BASEURL%>/images/botones/iconos/documento.gif" alt="Adjuntar Imagen" width="20" height="15" 
						  onClick="AdjuntarDoc('<%=CONTROLLER%>',9,'<%=act%>','026');" style="cursor:hand"></td>
            <td>Fecha Vencimiento</td>
            <td><% String vvi=(conductor!=null && !conductor.getVencevisa().equals("0099-01-01"))?conductor.getVencevisa():""; %>
                <input  name='VenceVisa' type='text' class='textbox' id="VenceVisa" value="<%=vvi%>" size='10' maxlength='10'   readonly='readonly'>
                <a href="javascript:void(0)" onClick="if(self.gfPop)gfPop.fPopCalendar(VenceVisa);return false;" hidefocus> <img name="popcal" align="absmiddle" src="<%=BASEURL%>\js\Calendario\cal.gif" width="16" height="16" border="0" alt=""> </a> </td>
          </tr>
          <tr class="fila">
            <td height="31">Libreta Tripulante </td>
            <td><% String libr = (conductor!=null && !conductor.getNrolibtripulante().equals(""))?conductor.getNrolibtripulante():""; %>
                <input name="libTripulante" type="text" class="textbox" id="libTripulante" onKeyPress="soloDigitos(event,'decNO')" value="<%=libr%>" size="20" maxlength="15">
                <img src="<%=BASEURL%>/images/botones/iconos/documento.gif" alt="Adjuntar Imagen" width="20" height="15" 
						  onClick="AdjuntarDoc('<%=CONTROLLER%>',7,'<%=act%>','029');" style="cursor:hand"></td>
            <td>Fecha Vencimiento</td>
            <td><% String vlib=(conductor!=null && !conductor.getVencelibtripulante().equals("0099-01-01"))?conductor.getVencelibtripulante():""; %>
                <input  name='VenceLibTripulante' type='text' class='textbox' id="VenceLibTripulante" value="<%=vlib%>" size='10' maxlength='10'   readonly='readonly'>
                <a href="javascript:void(0)" onClick="if(self.gfPop)gfPop.fPopCalendar(VenceLibTripulante);return false;" hidefocus> <img name="popcal" align="absmiddle" src="<%=BASEURL%>\js\Calendario\cal.gif" width="16" height="16" border="0" alt=""> </a> </td>
          </tr>
          <tr class="fila">
            <td height="31">Numero Afiliaci&oacute;n Eps </td>
            <td><% String neps=(conductor!=null && !conductor.getNroeps().equals(""))?conductor.getNroeps():""; %>
                <input name="nroeps" type="text" class="textbox" id="nroeps" onKeyPress="soloDigitos(event,'decNO')" value="<%=neps%>" size="20" maxlength="15">
                <img src="<%=BASEURL%>/images/botones/iconos/documento.gif" alt="Adjuntar Imagen" width="20" height="15" 
						  onClick="AdjuntarDoc('<%=CONTROLLER%>',4,'<%=act%>','027');" style="cursor:hand"> </td>
            <td>Fecha Afiliaci&oacute;n </td>
            <td><% String aeps=(conductor!=null && !conductor.getFecafieps().equals("0099-01-01"))?conductor.getFecafieps():""; %>
                <input  name='Afiliaeps' type='text' class='textbox' id="Afiliaeps" value="<%=aeps%>" size='10' maxlength='10'   readonly='readonly'>
                <a href="javascript:void(0)" onClick="if(self.gfPop)gfPop.fPopCalendar(Afiliaeps);return false;" hidefocus> <img name="popcal" align="absmiddle" src="<%=BASEURL%>\js\Calendario\cal.gif" width="16" height="16" border="0" alt=""> </a> </td>
          </tr>
          <tr >
            <td height="31" class="fila">Nombre Eps</td>
            <td class="fila">
              <% String nomeps=(conductor!=null && !conductor.getNomeps().equals(""))?conductor.getNomeps():""; %>
              <input name="eps" type="text" class="textbox" id="eps" value="<%=nomeps%>" size="25" maxlength="25">              </td>
            <td class="fila">Grado Riesgo </td>
            <td class="letra"><% int grie = Integer.parseInt((conductor!=null)?conductor.getGradoriesgo():"0"); %></td>
          </tr>
          <tr class="fila">
            <td height="31">Arp</td>
            <td><% String arp=(conductor!=null && !conductor.getNomarp().equals(""))?conductor.getNomarp():""; %>
                <input name="arp" type="text" class="textbox" id="arp" value="<%=arp%>" size="25" maxlength="25">
                <img src="<%=BASEURL%>/images/botones/iconos/documento.gif" alt="Adjuntar Imagen" width="20" height="15" 
						  onClick="AdjuntarDoc('<%=CONTROLLER%>',5,'<%=act%>','028');" style="cursor:hand"> </td>
            <td>Fecha Afiliaci&oacute;n</td>
            <td><% String aarp=(conductor!=null && !conductor.getFecafiarp().equals("0099-01-01"))?conductor.getFecafiarp():""; %>
                <input  name='Vencearp' type='text' class='textbox' id="Vencearp" value="<%=aarp%>" size='10' maxlength='10'   readonly='readonly'>
                <a href="javascript:void(0)" onClick="if(self.gfPop)gfPop.fPopCalendar(Vencearp);return false;" hidefocus> </a><a href="javascript:void(0)" onClick="if(self.gfPop)gfPop.fPopCalendar(Vencearp);return false;" hidefocus><img name="popcal" align="absmiddle" src="<%=BASEURL%>\js\Calendario\cal.gif" width="16" height="16" border="0" alt=""></a> </td>
          </tr>
		  <tr class="fila">
            <td height="31"><p>Pension</p>
            </td>
            <td><% String pension=(conductor!=null && !conductor.getPension().equals(""))?conductor.getPension():""; %>
                <input name="pension" type="text" class="textbox" id="pension" value="<%=pension%>" size="20" maxlength="15">                </td>
            <td>Fecha Afiliaci&oacute;n</td>
            <td><% String apension=(conductor!=null && !conductor.getFecafipension().equals("0099-01-01"))?conductor.getFecafipension():""; %>
                <input  name='fecpension' type='text' class='textbox' id="fecpension" value="<%=apension%>" size='10' maxlength='10'   readonly='readonly'>
                <a href="javascript:void(0)" onClick="if(self.gfPop)gfPop.fPopCalendar(Vencearp);return false;" hidefocus> </a><a href="javascript:void(0)" onClick="if(self.gfPop)gfPop.fPopCalendar(fecpension);return false;" hidefocus><img name="popcal" align="absmiddle" src="<%=BASEURL%>\js\Calendario\cal.gif" width="16" height="16" border="0" alt=""></a> </td>
          </tr>
		   <tr>
            <td align="left" class="fila">Fecha de Ingreso</td>
            <td valign="middle" class="letra"><% String fini=(conductor!=null && conductor.getInitDate()!=null)?conductor.getInitDate():""; %>
			<%=(fini.equals("0099-01-01"))?"":fini%></td>
            <td valign="middle" class="fila">Total viajes</td>
            <td valign="middle" class="letra"><% String tvi=(conductor!=null && conductor.getTripTotal()!=null)?conductor.getTripTotal():""; %> <%=tvi%>
           </td>
          </tr>
          <tr class="fila">
            <td height="31">Observaci&oacute;n</td>
            <td colspan="3"><% String doc=(conductor!=null && !conductor.getDocument().equals(""))?conductor.getDocument():""; %>
                <input name="documento" type="text" class="textbox" style="width=100%" value="<%=doc%>" size="70" maxlength="70">
                <input type="hidden" name="evento">
                <input name="consulta" type="hidden" id="consulta" value="ok"></td>
          </tr>
         
      </table></td> 
    </tr>
  </table>
  <table width="700" border="2" align="center">
    <tr>
      <td>
        <table width="100%" class="tablaInferior">
          <tr>
            <td width="392" height="24"  class="subtitulo1">Referencias y Huellas Dactilares </td>
            <td width="392"  class="barratitulo"><img src="<%=BASEURL%>/images/titulo.gif" width="32" height="20"></td>
          </tr>
        </table>
        <table width="100%" class="tablaInferior">
          <tr class="filaresaltada">
            <td width="39%" height="22" ><a class="Simulacion_Hiper" style="cursor:hand " onClick="<%if(conductor!=null){%>ReferenciaConductor('<%=CONTROLLER%>','2')<%}else{%>ReferenciaConductor('<%=CONTROLLER%>','1')<%}%>">Referencias a este conductor</a></td>
            <td width="24%" align="center" >Huella 1   <img src="<%=BASEURL%>/images/botones/iconos/documento.gif" alt="Adjuntar Imagen" width="20" height="15" 
						  onClick="AdjuntarDoc('<%=CONTROLLER%>','','015','<%=(conductor!=null)?conductor.getHuella_der():""%>');" style="cursor:hand"></td>
            <td width="19%" align="center" >Huella 2  <img src="<%=BASEURL%>/images/botones/iconos/documento.gif" alt="Adjuntar Imagen" width="20" height="15" 
						  onClick="AdjuntarDoc('<%=CONTROLLER%>','','016','<%=(conductor!=null)?conductor.getHuella_izq():""%>');" style="cursor:hand"></td>
            <td width="18%" align="center" >Firma <img src="<%=BASEURL%>/images/botones/iconos/documento.gif" alt="Adjuntar Imagen" width="20" height="15" 
						  onClick="AdjuntarDoc('<%=CONTROLLER%>','','<%=act%>','039');" style="cursor:hand"></td>
          </tr>
      </table></td>
    </tr>
  </table>
  <p align="center">
	   <%if(estado.equals("false")){ %><img src="<%=BASEURL%>/images/botones/aceptar.gif"  name="imgaceptar" onclick="Conductor('INSERT');" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" style="cursor:hand"><%}%>&nbsp;
	   <%if(estado.equals("true")){ %><img src="<%=BASEURL%>/images/botones/modificar.gif"  name="imgmodificar" onclick="Conductor('UPDATE');" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" style="cursor:hand"><%}%>&nbsp;
       <img src="<%=BASEURL%>/images/botones/cancelar.gif"  name="imgcancelar"  onMouseOver="botonOver(this);" onclick="Conductor('NEW');" onMouseOut="botonOut(this);" style="cursor:hand"> &nbsp;
       <img src="<%=BASEURL%>/images/botones/salir.gif"  name="imgsalir"  onMouseOver="botonOver(this);" onClick="parent.close();" onMouseOut="botonOut(this);" style="cursor:hand">
    <%}%>
  </p>
  <p>
  <%if(!comentario.equals("")){%>
  <table border="2" align="center">
    <tr>
      <td><table width="100%"  border="1" align="center"  bordercolor="#F7F5F4" bgcolor="#FFFFFF">
          <tr>
            <td width="229" align="center" class="mensajes"><%=comentario%></td>
            <td width="29" background="<%=BASEURL%>/images/cuadronaranja.JPG">&nbsp;</td>
            <td width="58">&nbsp;</td>
          </tr>
      </table></td>
    </tr>
  </table>
  <%}%>
</p>
<%if(reg.equals("no")){%>
<p align="center"><a style="cursor:hand" class="Simulacion_Hiper" onClick="window.location='<%=CONTROLLER%>?estado=Menu&accion=Cargar&opcion=24&carpeta=/jsp/hvida/identidad&pagina=identidad.jsp?clas=0E000&marco=no'" >Registrar Nit</a>
<%}%>
</form>
</div>
<iframe width=188 height=166 name="gToday:normal:agenda.js:gfPop:plugins.js" id="gToday:normal:agenda.js:gfPop:plugins.js" src="<%=BASEURL%>/js/Calendario/ipopeng.htm" scrolling="no" frameborder="0" style="visibility:visible; z-index:999; position:absolute; left:-500px; top:0px;"> </iframe>
</body>
</html>
