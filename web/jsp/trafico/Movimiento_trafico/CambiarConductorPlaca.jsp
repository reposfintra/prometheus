<%@page contentType="text/html"%>
<%@page session="true"%>
<%@page errorPage="../error/ErrorPage.jsp"%>
<%@page import="com.tsp.operation.model.*"%>
<%@page import="com.tsp.util.*"%>
<%@page import="com.tsp.operation.model.beans.*, java.util.*"%>
<%@include file="/WEB-INF/InitModel.jsp"%>
<%@ taglib uri="http://jakarta.apache.org/taglibs/input-0.90" prefix="input" %>
<%  String path    = application.getRealPath(model.menuService.getRealUrl(request.getRequestURI()));
    String datos[] = model.menuService.getContenidoMenu(BASEURL,request.getRequestURI(),path);
%>
<html>
<head>
<title>Cambiar Placa Conductor</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link href="<%= BASEURL %>/css/estilostsp.css" rel="stylesheet" type="text/css">
<link href="../../../css/estilostsp.css" rel="stylesheet" type="text/css">
<script src="<%=BASEURL%>/js/validar.js"></script>
<script src="<%=BASEURL%>/js/general.js"></script>
<script type='text/javascript' src="<%= BASEURL %>/js/boton.js"></script>
<script type='text/javascript' src="<%= BASEURL %>/js/RepMovTraficoSelect.js"></script>
</head>
<body onresize="redimensionar()" ONLOAD="establecerIndiceInicial(forma);redimensionar();forma.c_hora.focus();forma.c_hora.select();llenarObservacion();">  
<div id="capaSuperior" style="position:absolute; width:100%; height:100px; z-index:0; left: 0px; top: 0px;">
	<jsp:include page="/WEB-INF/toptsp.jsp?encabezado=Cambiar Conductor Placa"/>
</div>
<div id="capaCentral" style="position:absolute; width:100%; height:87%; z-index:0; left: 0px; top: 100px; overflow: scroll;">
  <form method="post" name="forma1" action="<%=CONTROLLER%>?estado=CambioPlaca&accion=Conductor&pagina=CambiarConductorPlaca.jsp&carpeta=jsp/trafico/Movimiento_trafico" id='forma1'>
 <% 	DatosPlanillaRMT dp = model.rmtService.getDatosPlanillaRMT(); %>
		<table align="center" width="900" border="2">
                    <tr>
                        <td>
                            <table width="100%" >
                                <tr>
                                  <td class="subtitulo1" colspan="4">Informaci�n De Planilla</td>
                                  <td class="barratitulo" colspan="4"><img src="<%=BASEURL%>/images/titulo.gif" width="32" height="20" align="left"><%=datos[0]%></td>
                                </tr>         
                                <tr class="fila">
                                    <td width="8%" rowspan="2" align="center"><strong>Planilla</strong></td>
                                    <td colspan="2" align="center"><strong>Conductor</strong></td>
                                    <td width="10%" rowspan="2" align="center"><strong>Placa Veh&iacute;culo </strong></td>
                                    <td colspan="2" align="center">Propietario</td>
                                    <td colspan="2" align="center">Ruta Planilla </td>
                                </tr>
                                <tr class="fila">
                                    <td width="10%" align="center">C&eacute;dula</td>
                                    <td width="19%" align="center">Nombre</td>
                                    <td width="10%" align="center">C&eacute;dula</td>
                                    <td width="19%" align="center">Nombre</td>
                                    <td width="12%" align="center"><strong>Origen</strong></td>
                                    <td width="12%" align="center"><strong>Destino</strong></td>
                                </tr>
                                <tr class="letra">
                                  <td align="center"><%=dp.getNumpla()%></td>
                                    <td align="center"><%=dp.getCedcon()%></td>
                                    <td align="center"><%=dp.getNombrecond()%></td>
                                    <td align="center"><%=dp.getPlaveh()%></td>
                                    <td align="center"><%=dp.getCed_propietario()%></td>
                                    <td align="center"><%=dp.getNom_propietario()%></td>
                                    <td align="center"><%=dp.getNomciuori()%></td>
                                    <td align="center"><%=dp.getNomciudest()%></td>
                              </tr>  
								<%
                                    RepMovTrafico ultimoreporte = model.rmtService.getLastCreatedReport();                                    
                                    String nombrepc = "";
									String nextpc=request.getParameter("ubicacion")!=null?request.getParameter("ubicacion"):"";
                                    String SalidaSeleccionable = "onchange = seleccionarOrigen(forma);";
                                    if(ultimoreporte!=null){
										String fecha = ultimoreporte.getFechaReporte();                                        
                                        if(ultimoreporte.getTipo_procedencia().equalsIgnoreCase("PC")){
                                            nombrepc = ultimoreporte.getUbicacion_procedencia();
                                        }
                                        /*Este else aun no se utiliza, no esta definido...*/
                                        else{
                                            nombrepc = model.rmtService.getDescripcionUbicacion(ultimoreporte.getUbicacion_procedencia());                                
                                        }
                                        if(!nombrepc.equalsIgnoreCase("")){
                                            SalidaSeleccionable = "onchange = salidaSeleccionable(forma);";
                                        }                                        
                                        nextpc = (request.getParameter("ubicacion")!=null&&request.getParameter("causa")!=null)?request.getParameter("ubicacion"):dp.getPto_control_proxreporte();%>
										  <tr class="letra">										
											<td colspan=4 height="18"><strong>Ubicaci�n Ultimo Reporte:</strong> <%=model.rmtService.getNombreCiudad(dp.getPto_control_ultreporte().equals("")?dp.getOripla():dp.getPto_control_ultreporte())%></td>
											<td colspan=4 height="18">&nbsp;<strong>Fecha Ultimo Reporte de Tr�fico:</strong> <% if(fecha.equals("0099-01-01 00:00:00")){%>Ninguno<%}else{%><%=fecha.substring (0,11)%>&nbsp;&nbsp;&nbsp;<%=fecha.substring (11,16)%><%}%></td>
										  </tr>                                                                        
                                    <%}%>
                          </table>
                        </td>
                    </tr>
    </table> 
  <br> 
  <table width="650" border="2" align="center">
    <tr>
      <td>
        <table width="100%" class="tablaInferior">
          <tr class="fila">
            <td align="left" colspan="3" class="subtitulo1">&nbsp;Cambio de placa y conductor</td>
            <td align="left" colspan="1" class="barratitulo"><img src="<%=BASEURL%>/images/titulo.gif" width="32" height="20"></td>
          </tr>
          <tr>
			<td width="9%" class="fila">Placa</td>
			<td width="20%" class="letra">&nbsp;<input name="placa" type="text" class="textbox" value="<%=request.getParameter ("placa")!=null?request.getParameter ("placa"):dp.getPlaveh()%>" size="12" maxlength="12"><img src="<%=BASEURL%>/images/botones/iconos/obligatorio.gif">
			</td>
			<td width="16%" class="fila">Conductor &nbsp; <img src='<%=BASEURL%>/images/botones/iconos/lupa.gif'  width="15" height="15" onClick="javascript:abrirVentanaBusq(850,550,'<%=BASEURL%>/jsp/trafico/Movimiento_trafico/ListaConductor.jsp')" title="Buscar" style="cursor:hand" ></td>
			<td width="55%" class="letra">&nbsp;<input onKeyPress="soloDigitos(event,'')" name="c_conductor" id="c_conductor" type="text" class="textbox" value="<%=request.getParameter ("c_conductor")!=null?request.getParameter ("c_conductor"):dp.getCedcon()%>" size="12" maxlength="12"><img src="<%=BASEURL%>/images/botones/iconos/obligatorio.gif">
			&nbsp;<input type="text" id="n_co" name="n_co" value="<%=dp.getNombrecond()%>" style="border:0;width:65%" class="filaresaltada"></td>
		  </tr>		
          <tr class="fila">
            <td colspan="6" align="center" >Descripci&oacute;n</td>
          </tr>		    
          <tr class="fila">
            <td colspan="6"  align="center"><textarea name="c_descripcion" cols="122" rows="6" class="textbox" id="c_descripcion"><%=request.getParameter ("c_descripcion")!=null?request.getParameter ("c_descripcion"):""%></textarea></td>
          </tr>
      </table></td>
    </tr>
  </table>
  <br>
  
  <table align=center width='450'>
    <tr>
        <td align="center" colspan="2">
            <img title='Agregar reporte' name="mod" id="mod" style="visibility:visible;cursor:hand" src="<%= BASEURL %>/images/botones/modificar.gif" onmouseover="botonOver(this);" onmouseout="botonOut(this);" onClick="return validarConductorPlaca(forma1);"></img>            
            <img title='Salir' src="<%= BASEURL %>/images/botones/salir.gif" onmouseover="botonOver(this);" onmouseout="botonOut(this);" onClick="parent.close()"></img>
 	</td>
    </tr>
  </table>
  </form>
	<br>
    <%        
		String mensaje = request.getParameter("mensaje");
        if(mensaje!=null) {
            out.print("<table width=450 border=2 align=center><tr><td><table width=100%  border=1 align=center  bordercolor=#F7F5F4 bgcolor=#FFFFFF><tr><td width=350 align=center class=mensajes>"+mensaje+"</td><td width=100><img src="+BASEURL+"/images/cuadronaranja.JPG></td></tr></table></td></tr></table>");            
        }
    %>    
</div>
<%=datos[1]%> 
</body>
</html>
<script>
   function abrirVentanaBusq(an,al,url,pag) {
        parent.open(url,'Conductor','width='+an+',height='+al+',scrollbars=no,resizable=no,top=10,left=65,status=yes');
    }
	function validarConductorPlaca(forma){
		if(forma.placa.value==""){
			alert("Debe llenar el campo de la placa!");
			forma.placa.focus();
		}
		else if(forma.c_conductor.value==""){
			alert("Debe llenar el campo del conductor!");
			forma.c_conductor.focus();
		}	
		else
			forma.submit();
	}
</script>