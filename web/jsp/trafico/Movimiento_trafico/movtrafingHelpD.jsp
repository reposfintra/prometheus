<%@ include file="/WEB-INF/InitModel.jsp"%>
<html>
<head>
<title>Descripcion Campos Reporte Movimiento Trafico</title>
<link href="<%= BASEURL %>/css/estilostsp.css" rel="stylesheet" type="text/css">
</head>

<body>
<br>


<table width="696"  border="2" align="center">
  <tr>
    <td width="811" >
      <table width="100%" border="0" align="center">
        <tr  class="subtitulo">
          <td height="24" colspan="2"><div align="center"> REPORTE MOVIMIENTO TRAFICO </div></td>
        </tr>
        <tr class="subtitulo1">
          <td colspan="2"> Ingresar Movimiento </td>
        </tr>
        <tr>
          <td width="149" class="fila"> Fecha del Reporte </td>
          <td width="525"  class="ayudaHtmlTexto"> Campo para digitar la fecha del reporte, representado por n&uacute;meros y en el siguiente formato(AA-MM-DD) (HH:MM).</td>
        </tr>
        <tr>
          <td class="fila">Tipo del Reporte </td>
          <td  class="ayudaHtmlTexto">Campo de selecci&oacute;n donde se encuentran los distintos reportes a realizar. </td>
        </tr>
        <tr>
          <td class="fila">Tipo Ubicaci&oacute;n</td>
          <td  class="ayudaHtmlTexto">Campo de selecci&oacute;n donde se encuentran las distintas tipo de ubicaciones del reporte. </td>
        </tr>
        <tr>
          <td class="fila">Ubicaci&oacute;n</td>
          <td  class="ayudaHtmlTexto">Campo de selecci&oacute;n donde se encuentran las distintas ciudades o puestos de control del reporte. </td>
        </tr>
        <tr>
          <td class="fila">Clasificaci&oacute;n</td>
          <td  class="ayudaHtmlTexto">Campo de selecci&oacute;n donde se encuentran las distintas clasificaciones del reporte. </td>
        </tr>
        <tr>
          <td class="fila">Observaci&oacute;n</td>
          <td  class="ayudaHtmlTexto">Campo de texto donde se digitan las observaciones pertinentes al nuevo reporte creado. </td>
        </tr>
        <tr>
          <td class="fila">Ingresar Retraso </td>
          <td  class="ayudaHtmlTexto">Link que direcciona a una pantalla que permite ingresar  un reporte de retraso a una planilla.</td>
        </tr>
        <tr>
          <td class="fila">Escolta</td>
          <td  class="ayudaHtmlTexto">Link que direcciona a una pantalla que permite asociar los escoltas a las planillas.</td>
        </tr>
        <tr>
          <td class="fila">Copiar Reportes a Otras Planillas </td>
          <td  class="ayudaHtmlTexto">Link que direcciona a una pantalla donde permite escoger varias planillas que se les van a aplicar el mismo reporte.</td>
        </tr>
        <tr>
          <td class="fila">Cambiar Via </td>
          <td  class="ayudaHtmlTexto">Link que direcciona a una pantalla donde le permiten cambiar de via a la planilla.</td>
        </tr>
        <tr>
          <td class="fila">Tramos</td>
          <td  class="ayudaHtmlTexto">Link que direcciona a una pantalla donde se ingresan los tramos.</td>
        </tr>
        
      </table>
    </td>
  </tr>
</table>
<p>&nbsp;</p>
<p>&nbsp;</p>

<p>&nbsp;</p>
<p>&nbsp; </p>
</body>
</html>
