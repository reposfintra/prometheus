<!-- 
- Autor : Ing. Jose de la rosa
- Date  : 9 de Noviembre de 2005
- Copyrigth Notice : Fintravalores S.A. S.A
-->
<%--
-@(#)
--Descripcion : Pagina JSP, que busca las discrepancias por fecha de cierre o sin fecha de cierre
--%>

<%-- Declaracion de librerias--%>
<%@page contentType="text/html"%>
<%@page session="true"%>
<%@page errorPage="../error/ErrorPage.jsp"%>
<%@page import="java.util.*"%>
<%@page import="com.tsp.operation.model.*"%>
<%@page import="com.tsp.util.*"%>
<%@page import="com.tsp.operation.model.beans.*, java.util.*"%>
<%@include file="/WEB-INF/InitModel.jsp"%>
<%@ taglib uri="http://jakarta.apache.org/taglibs/input-0.90" prefix="input" %>
<%  String path    = application.getRealPath(model.menuService.getRealUrl(request.getRequestURI()));
    String datos[] = model.menuService.getContenidoMenu(BASEURL,request.getRequestURI(),path);
%>
<html>
<head>
<title>Reporte Movimiento Trafico</title>
<link href="<%= BASEURL %>/css/estilostsp.css" rel="stylesheet" type="text/css">
<script src="<%=BASEURL%>/js/validar.js"></script>
<script src="<%=BASEURL%>/js/general.js"></script>
<script type='text/javascript' src="<%= BASEURL %>/js/boton.js"></script>
<script type='text/javascript' src="<%= BASEURL %>/js/RepMovTraficoSelect.js"></script>
</head>
<%-- Inicio Body --%>
<body>
<div id="capaSuperior" style="position:absolute; width:100%; height:100px; z-index:0; left: 0px; top: 0px;">
<jsp:include page="/WEB-INF/toptsp.jsp?encabezado=Cambio de Ruta"/>
</div>
<div id="capaCentral" style="position:absolute; width:100%; height:85%; z-index:0; left: 0px; top: 100px; overflow: scroll;"> 
<%-- Inicio Formulario --%>
<form name="forma" id="forma" method="post" action="<%=CONTROLLER%>?estado=Cambio_via&accion=RepMovTrafico&opcion=2&pagina=JustificacionInsertar.jsp&carpeta=jsp/trafico/justificacion&esCambioRuta=si">
	<%-- Inicio Tabla Principal --%>
 <% String Mruta = (request.getParameter("Mruta")!=null)?request.getParameter("Mruta"):"";
 	DatosPlanillaRMT dp = model.rmtService.getDatosPlanillaRMT();
	java.util.Date date = new java.util.Date();
    java.text.SimpleDateFormat s = new java.text.SimpleDateFormat("yyyy-MM-dd HH:mm");
	String fechareporte =s.format(date);
	if(dp==null){
                out.print("<table width=400 border=2 align=center><tr><td><table width=100%  border=1 align=center  bordercolor=#F7F5F4 bgcolor=#FFFFFF><tr><td width=229 align=center class=mensajes>La planilla no existe o est� anulada</td><td width=29><img src="+BASEURL+"/images/cuadronaranja.JPG></td></tr></table></td></tr></table>");%>
                <br>
                <table align=center width='400'>
                <tr align=center>
                        <td><img title='Salir' src="<%= BASEURL %>/images/botones/salir.gif" onmouseover="botonOver(this);" onmouseout="botonOut(this);" onClick="parent.close()"></img></td>   
                  </tr>                    
                </table><%
	}
	else{%>
		<table align="center" width="850" border="2">
                    <tr>
                        <td>
                            <table width="100%" align="center">
                                <tr>
                                  <td width="50%" class="subtitulo1" colspan='4'>Informaci�n De Planilla 
                                  </td>
                                  <td width="50%" class="barratitulo" colspan='4'><img src="<%=BASEURL%>/images/titulo.gif" width="32" height="20" align="left"><%=datos[0]%></td>
                                </tr>         
                                <tr class="fila">
                                    <td align="center" rowspan="2"><strong>Planilla</strong></td>
                                    <td colspan="2" align="center"><strong>Conductor</strong></td>
                                    <td align="center" rowspan="2"><strong>Placa Veh&iacute;culo </strong></td>
                                    <td colspan="2" align="center"><strong>Origen</strong></td>
                                    <td colspan="2" align="center"><strong>Destino</strong></td>      
                                </tr>
                                <tr class="fila">
                                    <td align="center">C&eacute;dula</td>
                                    <td align="center">Nombre</td>
                                    <td align="center">C&oacute;digo</td>
                                    <td align="center">Descripci&oacute;n</td>
                                    <td align="center">Codigo</td>
                                    <td align="center">Descripci&oacute;n</td>
                                </tr>
                                <tr class="letra">
                                  <td align="center"><%=dp.getNumpla()%></td>
                                    <td align="center"><%=dp.getCedcon()%></td>
                                    <td align="center"><%=dp.getNombrecond()%></td>
                                    <td align="center"><%=dp.getPlaveh()%></td>
                                    <td align="center"><%=dp.getOripla()%></td>
                                    <td align="center"><%=dp.getNomciuori()%></td>
                                    <td align="center"><%=dp.getDespla()%></td>
                                    <td align="center"><%=(dp.getNomciudest()!= null)?dp.getNomciudest():""%></td>
                              </tr>  
                                <%
                                    RepMovTrafico ultimoreporte = model.rmtService.getLastCreatedReport();                                    
                                    String nombrepc = "";
                                    String nextpc=request.getParameter("ubicacion")!=null?request.getParameter("ubicacion"):"";
                                    String SalidaSeleccionable = "onchange = seleccionarOrigen(forma);";
                                    if(ultimoreporte!=null){
                                        String fecha = ultimoreporte.getFechaReporte();                                        
                                        if(ultimoreporte.getTipo_procedencia().equalsIgnoreCase("PC")){
                                            nombrepc = ultimoreporte.getUbicacion_procedencia();
                                        }
                                        /*Este else aun no se utiliza, no esta definido...*/
                                        else{
                                            nombrepc = model.rmtService.getDescripcionUbicacion(ultimoreporte.getUbicacion_procedencia());                                
                                        }
                                        if(!nombrepc.equalsIgnoreCase("")){
                                            SalidaSeleccionable = "onchange = salidaSeleccionable(forma);";
                                        }                                        
                                        nextpc = (request.getParameter("ubicacion")!=null&&request.getParameter("causa")!=null)?request.getParameter("ubicacion"):dp.getPto_control_proxreporte();%>
										  <tr class="letra">										
											<td colspan=4 height="18">&nbsp;<strong>Fecha Ultimo Reporte de Tr�fico:</strong> <%=fecha.substring (0,11)%>&nbsp;&nbsp;&nbsp;<%=fecha.substring (11,16)%></td>
											<td colspan=4 height="18">&nbsp;<strong>Fecha Proximo Reporte Planeado:</strong> <%=dp.getFecha_prox_rep().substring (0,11)%>&nbsp;&nbsp;&nbsp;<%=dp.getFecha_prox_rep().substring (11,16)%></td>
										  </tr>
										  <tr class="letra">
										    <td height="18" colspan=4 >&nbsp;<strong>Ubicaci�n Ultimo Reporte:</strong> <%=model.rmtService.getNombreCiudad(dp.getPto_control_ultreporte().equals("")?dp.getOripla():dp.getPto_control_ultreporte())%></td>
						                    <td height="18" colspan=4 >&nbsp;<strong>Ubicaci�n Proximo Reporte:</strong> <%=(model.rmtService.getNombreCiudad(dp.getPto_control_proxreporte())!= null)?model.rmtService.getNombreCiudad(dp.getPto_control_proxreporte()):""%></td>
							  			</tr>                                                                       
                                    <%}%>
                          </table>
                        </td>
                    </tr>
    </table>
 	<br>
        <table width="750" border="2" align="center">
		<tr>
			<td>
                                <%-- Inicio Tabla Secundaria --%>
				<table width="100%" align="center"  class="tablaInferior">
					<tr class="fila">
						<td colspan="2" class="subtitulo1">&nbsp;Cambio Ruta </td>
						<td colspan="2" class="barratitulo"><img src="<%=BASEURL%>/images/titulo.gif" width="32" height="20"></td>
					</tr>
					<tr class="fila">
						<td colspan="4" align="center">Fecha del cambio&nbsp;&nbsp;<input name="c_dia" type="text" class="textbox" onBlur="llenarObservacion();" id="c_dia" onClick="" onKeyPress="soloDigitos(event,'decNo');FormatNumber(this,'####-##-##');" value="<%=request.getParameter("c_dia")!=null?request.getParameter("c_dia"):fechareporte.substring (0,10)%>" size="13" maxlength="10">
            			<input name="c_hora" type="text" class="textbox" onBlur="llenarObservacion();" id="c_hora" onClick="" onKeyPress="soloDigitos(event,'decNo');FormatNumber(this,'##:##');" value="<%=request.getParameter("c_hora")!=null?request.getParameter("c_hora"):fechareporte.substring (11,16)%>" size="6" maxlength="5">
						&nbsp;<img src="<%=BASEURL%>/images/botones/iconos/obligatorio.gif" width="10" height="10"></img>

						</td>
					</tr>
					<tr class="fila">
					<td>Origen
					</td>
					<td>
						<input:select name="origen" options="<%=model.ciudadService.getTreMapCiudades ()%>" default="<%=dp.getOripla()%>" attributesText=" style='width:90%;' onchange =cargarCambioRuta(forma.controller.value,forma.numpla.value,forma.ruta.value,this.value,forma.destino.value,forma.c_dia.value,forma.c_hora.value);"/> <img src="<%=BASEURL%>/images/botones/iconos/obligatorio.gif" width="10" height="10"></img>
					</td>
					<td>Destino
					</td>
					<td>
						<input:select name="destino" options="<%=model.ciudadService.getTreMapCiudades ()%>" default="<%=dp.getDespla()%>" attributesText=" style='width:90%;' onchange =cargarCambioRuta(forma.controller.value,forma.numpla.value,forma.ruta.value,forma.origen.value,this.value,forma.c_dia.value,forma.c_hora.value);"/> <img src="<%=BASEURL%>/images/botones/iconos/obligatorio.gif" width="10" height="10"></img>
					</td>
					<script>
					  forma.origen.value ='<%=dp.getOripla()%>';
                                          forma.destino.value ='<%=dp.getDespla()%>';
					</script>
					</tr>
					<%%>
					<tr class="fila" id="fechas">
						<td width="9%" align="left" > Vias </td>
						<td width="42%" valign="middle" >     
     									<input:select name="ruta" options="<%=model.viaService.getCbxVias()%>" default="<%=Mruta%>" attributesText=" style='width:90%;' onchange =cargarCambioRuta(forma.controller.value,forma.numpla.value,this.value,forma.origen.value,forma.destino.value,forma.c_dia.value,forma.c_hora.value);"/> <img src="<%=BASEURL%>/images/botones/iconos/obligatorio.gif" width="10" height="10"></img></td>
						<td width="11%" valign="middle">Ubicacion</td>
						<td width="38%" valign="middle"><select name="ubicacion" id="ubicacion" class='textbox' style="width:90%;">
                            <%
                                Vector ubicaciones = model.rmtService.getUbicaciones();
				nextpc = (request.getParameter("ubicacion")!=null)?request.getParameter("ubicacion"):nextpc;
                                if (ubicaciones!=null){ 
                                    for(int i=0;i<ubicaciones.size();i++){
                                        Vector tmp = (Vector)(ubicaciones.get(i));%>
                                    <option value="<%=(String)tmp.get(0)%>" <%=nextpc.equals((String)tmp.get(0))?"selected":""%>><%=(String)(tmp.get(1))%></option>
                                    <%}
                                }
                             %>
                        </select>
                        <img src="<%=BASEURL%>/images/botones/iconos/obligatorio.gif" width="10" height="10"></img></td>
			</tr>
				</table>
			</td>
		</tr>
	</table>
	<input type="hidden" name="controller" id="controller" value="<%=CONTROLLER%>">
	<input type="hidden" name="numpla" id="numpla" value="<%=dp.getNumpla()%>">
	<input type="hidden" name="nextpc" id="nextpc" value="<%=nextpc%>">
	<%}%>
	<p align="center">
	<img src="<%=BASEURL%>/images/botones/aceptar.gif" style="cursor:hand" title="Buscar Ruta" name="buscar"  onClick=" if(validarFormaCambioRuta()) ValidarFormularioHoras(); " onMouseOver="botonOver(this);" onMouseOut="botonOut(this);">&nbsp;
        <img src="<%=BASEURL%>/images/botones/salir.gif"  name="salir" style="cursor:hand" title="Salir al Menu Principal" onMouseOver="botonOver(this);" onClick="parent.close();" onMouseOut="botonOut(this);" >&nbsp;
        <img src="<%=BASEURL%>/images/botones/regresar.gif" style="cursor:hand" title="Volver" name="buscar"  onClick="window.location='<%=CONTROLLER%>?estado=Buscar&accion=NumPlanilla&mensaje=default&pagina=movtrafing.jsp&carpeta=jsp/trafico/Movimiento_trafico&mensaje=default&numpla=<%=dp.getNumpla()%>'" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);">
	</p>	  
</form>
<% if( request.getAttribute("msg")!= null){%>
	<br>
	<%--Mensaje de informacion--%>
	<table border="2" align="center">
		<tr>
			<td>
				<table width="100%"  border="1" align="center"  bordercolor="#F7F5F4" bgcolor="#FFFFFF">
					<tr>
						<td width="500" align="center" class="mensajes"><%=request.getAttribute("msg").toString()%></td>
						<td width="29" background="<%=BASEURL%>/images/cuadronaranja.JPG">&nbsp;</td>
						<td width="58">&nbsp;</td>
					</tr>
				</table>
			</td>
		</tr>
	</table>
<%} %>
</div>
<%=datos[1]%> 
</body>
</html>
<script>
    var index = forma.ubicacion.selectedIndex;
    function validarFormaCambioRuta(){
       if(forma.ruta.selectedIndex == -1){
            alert ("Seleccione una ruta disponible para poder continuar ");
            return false;
       }
       if(forma.ubicacion.selectedIndex == -1){
            alert ("Seleccione una ubicacion disponible para poder continuar ");
            return false;
       }
       if(index!=-1 && forma.ubicacion.selectedIndex < index){
            alert ("No puede indicar una ciudad o puesto de control anterior al proximo pc.");
            return false;
       }  
       return true;
    }
</script>
