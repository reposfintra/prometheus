<!--
- Autor : Ing. David Lamadrid
- Date  : 5 de Octubre de 2005
- Copyrigth Notice : Fintravalores S.A. S.A
-->
<%--
-@(#)
--Descripcion : Pagina JSP, que se encarga de realizar y mostrar busqueda de los nits de proveedores
--%>

<%-- Declaracion de librerias--%>
<%@page contentType="text/html"%>
<%@page session="true"%>
<%@page import="java.util.*"%>
<%@page import="com.tsp.operation.model.*"%>
<%@page import="com.tsp.util.*"%>
<%@page import="com.tsp.operation.model.beans.*, java.util.*"%>
<%@include file="/WEB-INF/InitModel.jsp"%>
<%@taglib uri="http://jakarta.apache.org/taglibs/input-0.90" prefix="input" %>
<html>
<%System.out.println("BANDERA");%>
<head>
<script type='text/javascript' src="<%= BASEURL %>/js/boton.js"></script>
<script type='text/javascript' src="<%= BASEURL %>/js/reporte.js"></script>
<script language="javascript">
    
	function asignar(cedula,nombre){
		var campo = parent.opener.document.forma1.c_conductor;
		campo.value=""+cedula;
		var campo1 = parent.opener.document.forma1.n_co;
		campo1.value=""+nombre;
		window.close() ;
	}
	


    function procesar (element){
      if (window.event.keyCode==13) 
        listaConductores();
    }
	
	function listaConductores(){
	 	//alert("hola");
		var nit= document.forma1.c_conductor.value; 
		document.forma1.action = "<%= CONTROLLER %>?estado=Despachom&accion=Lconductores&carpeta=jsp/trafico/Movimiento_trafico&pagina=ListaConductor.jsp&opcion=ok&conductor="+nit; 
        document.forma1.submit(); 
    }
</script>

<title>Buscar Conductor</title>
<link href="<%= BASEURL %>/css/estilostsp.css" rel="stylesheet" type="text/css">
<link href="../../../css/estilostsp.css" rel="stylesheet" type="text/css">
</head>

<body onresize="redimensionar()" onload = 'forma1.c_conductor.focus(); redimensionar()'>
<div id="capaSuperior" style="position:absolute; width:100%; height:100px; z-index:0; left: 0px; top: 0px;">
 <jsp:include page="/toptsp.jsp?encabezado=Buscar Conductor"/>
</div>
<div id="capaCentral" style="position:absolute; width:100%; height:87%; z-index:0; left: 0px; top: 100px; overflow: scroll;">
<%
  String accion = "" +request.getParameter("accion");
 
%>
<form name="forma1" action="" method="post">      
    <table border="2" align="center" width="555">
        <tr>
            <td width="610">
                <table width="100%" class="tablaInferior">
                    <tr>
                        <td width="129" height="24"  class="subtitulo1"><p align="left">Conductor</p></td>
                        <td width="406"  class="barratitulo"><img src="<%=BASEURL%>/images/titulo.gif" width="32" height="20"></td>
                    </tr>
                </table>        
                <table width="547" border="0" align="center" class="tablaInferior">
                    <tr class="fila">
                        
            <td width="126" height="30">Cedula o Nombre: </td>
                      <td width="405" > 
                            <input name="c_conductor" type="text" class="textbox" id="c_conductor" size="18" maxlength="15" onKeyPress="procesar(this);">
                            <input type="image" src='<%=BASEURL%>/images/botones/buscar.gif' name='Buscar' align="absmiddle" style='cursor:hand' title='Buscar'  onClick="listaConductores();"  onMouseOver='botonOver(this);' onMouseOut='botonOut(this);'> </td>
                    </tr>
              </table>
            </td>
        </tr>
    </table>

<%


if (accion.equals("1")){
    Vector vConductores=   model.conductorService.getConductores();
	if(vConductores!= null){
%>
<br>  <table width="555" border="2" align="center">
    <tr>
    <td width="556">
<table width="100%" class="tablaInferior">
          <tr>
    <td width="133" height="24"  class="subtitulo1"><p align="left">Conductor</p></td>
    <td width="405"  class="barratitulo"><img src="<%=BASEURL%>/images/titulo.gif" width="32" height="20"></td>
  </tr>
</table>
        <table width="551" align="center" border="1" borderColor="#999999" bgcolor="#F7F5F4">
          <tr class="tblTitulo" id="titulos">
            <td width="79" align="center">Cedula</td>
    		<td width="456" align="center">Nombre</td>
    	  </tr>
<%
        for(int i=0; i< vConductores.size();i++){
            Vector conductor=(Vector)vConductores.elementAt(i);
%>  
  <tr class="<%=(i % 2 == 0 )?"filagris":"filaazul"%>" onMouseOver='cambiarColorMouse(this)' style="cursor:hand" onClick="asignar('<%=conductor.elementAt(0)%>','<%=conductor.elementAt(1)%>');">
    <td width="79" align="center" class="bordereporte"><%=conductor.elementAt(0)%></td>
    <td width="456" class="bordereporte"><%=conductor.elementAt(1)%></td>
    </tr>
<%
        }}
    }
%>  
</table>
</td>
</tr>
</table>
<br>
<table width="555" border="0" cellspacing="0" cellpadding="0" align="center">
  <tr>
    <td><img src="<%=BASEURL%>/images/botones/salir.gif"  name="salir" title="Salir..." onClick="window.close();" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);"></td>
  </tr>
</table></form>
</div>
</bodY>
</html>