<%@ include file="/WEB-INF/InitModel.jsp"%>
<% String BASEIMG = BASEURL +"/images/ayuda/trafico/movtrafico/"; %>
<HTML>
<HEAD>
<TITLE></TITLE>
<META http-equiv=Content-Type content="text/html; charset=windows-1252">
<link href="../../css/estilostsp.css" rel="stylesheet" type="text/css"> 
<link href="<%=BASEURL%>/css/estilostsp.css" rel="stylesheet" type="text/css"> 
</HEAD>
<BODY> 
<table width="100%"  border="2" align="center">
    <tr>
      <td >
        <table width="100%" border="0" align="center">
          <tr  class="subtitulo">
            <td height="20"><div align="center">MANUAL DEL REPORTE DEMORA </div></td>
          </tr>
          <tr class="subtitulo1">
            <td>Como registrar los reportes de movimiento de trafico.</td>
          </tr>
          <tr>
            <td  height="18"  class="ayudaHtmlTexto"><p> El reporte de demora  consta de dos partes, un encabezado y un cuerpo donde se agregan los datos para registrar el reporte. Como se <span class="MsoNormal"> presenta en la  Figura 1 .</span></p>
            </td>
          </tr>
          <tr align="center">
            <td height="18"  class="ayudaHtmlTexto"><div align="center">
              <img src="<%= BASEIMG%>DetencionMovtraf1.JPG"><br>
              <strong>Figura 1</strong>
            </div></td>
          </tr>
          <tr>
            <td  class="ayudaHtmlTexto"><p>Para grabar un reporte de <strong>demora </strong> debemos seguir los siguientes pasos: 
              <br>
              -Modificamos o dejamos fecha que viene por default de la fecha del reporte.
              <br>-Seleccionar el tipo en el tipo de reporte ya sea <strong>Pernoctado </strong> o <strong>Varado </strong>. 
              <br>-Seleccionamos la ciudad la ciudad o PC del reporte. 
              <br>-Colocamos si es necesario una observaci&oacute;n. 
              <br>
              Luego presionamos un click en el bot&oacute;n <strong>Aceptar </strong> para crear el reporte. 
              Existen 2 botones adicionales en el reporte uno es <strong>Regresar</strong><strong> </strong> para volver a la pantalla de reporte principal y <strong>Salir </strong> para salir de la pantalla. 
              Si usted no selecciona ning&uacute;n tipo de reporte presentara un mensaje como se muestra en la Figura 2.
            </td>
          </tr>
          <tr  align="center">
            <td  class="ayudaHtmlTexto"><div align="center"><img src="<%= BASEIMG%>movtrafing4.JPG"><strong><br>Figura 2</strong> </div></td>
          </tr>
          <tr  align="center">
            <td  class="ayudaHtmlTexto"><div align="left"> Si usted digita mal la fecha o no digita nada en el campo de la fecha presentar&aacute; un mensaje como se muestra en la Figura 3. </div></td>
          </tr>
          <tr  align="center">
            <td  class="ayudaHtmlTexto"><div align="center"><img src="<%= BASEIMG%>movtrafing5.JPG"><br><strong>Figura 3</strong> </div></td>
          </tr>
          <tr  align="center">
            <td  class="ayudaHtmlTexto"><div align="left"> Si usted digita una fecha mayor a la fecha actual presentar&aacute; un mensaje como se muestra en la Figura 4. </div>            </td>
          </tr>
          <tr>
            <td  class="ayudaHtmlTexto"><div align="center"><img src="<%= BASEIMG%>movtrafing6.JPG"><br><strong>Figura 4</strong></div></td>
          </tr>
          <tr>
            <td  class="ayudaHtmlTexto"> Si usted presion&oacute; el bot&oacute;n <strong>Aceptar </strong> y no le presento ning&uacute;n cuadro anterior y la fecha del pr&oacute;ximo reporte planeado tiene una hora de diferencia presentar&aacute; un listado como se muestra en la Figura 5.  </td>
          </tr>
          <tr>
            <td  class="ayudaHtmlTexto"><div align="center"><img src="<%= BASEIMG%>movtrafing7.JPG"><br><strong>Figura 5</strong></div></td>
          </tr>
          <tr>
            <td  class="ayudaHtmlTexto"><p> Si usted no selecciona una causa de la lista presentar&aacute; un mensaje como se muestra en la Figura 6. </p></td>
          </tr>
          <tr>
            <td  class="ayudaHtmlTexto"><div align="center"><img src="<%= BASEIMG%>movtrafing8.JPG"><br><strong>Figura 6</strong></div></td>
          </tr>
      </table>
      </td>
  </tr>
</table>	  
  <br>
</BODY>
</HTML>
