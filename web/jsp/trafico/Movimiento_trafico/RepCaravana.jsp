<%@page contentType="text/html"%>
<%@page session="true"%>
<%@page errorPage="../error/ErrorPage.jsp"%>
<%@page import="com.tsp.operation.model.*"%>
<%@page import="com.tsp.util.*"%>
<%@page import="com.tsp.operation.model.beans.*, java.util.*"%>
<%@include file="/WEB-INF/InitModel.jsp"%>
<%@ taglib uri="http://jakarta.apache.org/taglibs/input-0.90" prefix="input" %>
<html>
<head>
<title>Ingresar Movimiento - Movimiento De Tr�fico - TSP</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link href="<%= BASEURL %>/css/estilostsp.css" rel="stylesheet" type="text/css">
<link href="../../../css/estilostsp.css" rel="stylesheet" type="text/css"> 
<script type='text/javascript' src="<%= BASEURL %>/js/boton.js"></script>
<script type='text/javascript' src="<%= BASEURL %>/js/RepMovTraficoSelect.js"></script>
</head>
<body>  

  <form method="post" name="forma" action="<%=CONTROLLER%>?estado=Aplicar&accion=Caravana&pagina=movtrafing.jsp&carpeta=jsp/trafico/Movimiento_trafico&cmd=caravana" id='forma'>
    <br>
    <table align=center width='450'>
      <tr>
        <td align="center" colspan="2"> <div align="left" class="informacion">*La planilla pertenece a una caravana, para ingresar este reporte a mas planillas seleccione las planillas y haga click en Aceptar. </div></td>
      </tr>
    </table>
    <br>
    <table width="450" border="2" align="center">
    <tr>
        <td>
            <table width="100%" align="center"  >
                <tr>            
                    <td width="54%" class="subtitulo1"><div align="center">Ingresar Movimiento A Caravana</div></td>
                    <td width="46%" class="barratitulo"><img src="<%=BASEURL%>/images/titulo.gif" width="32" height="20"></td>                    
                </tr>
        </table>        
            <table width="100%" border="1" bordercolor="#999999">
			
              <tr class="tblTitulo">
                <td width="6%">Seleccionar</td>
                <td width="94%">Planilla</td>
              </tr>
			<%Vector caravana = model.rmtService.getCaravana();
			for(int i = 0; i<caravana.size();i++){
			String numpla = (String) caravana.elementAt(i);
			%>
              <tr class="<%=i%2==0?"filagris":"filaazul"%>">
                <td class="bordereporte"><input type="checkbox" name="<%=numpla%>" value="checkbox"></td>
                <td class="bordereporte"><%=numpla%></td>
              </tr>
			  <%}%>
          </table></td>
    </tr>
  </table>
  <br>
  <table align=center width='450'>
    <tr>
        <td align="center" colspan="2">
            <img title='Agregar reporte a estas planillas' src="<%= BASEURL %>/images/botones/aceptar.gif" onmouseover="botonOver(this);" onmouseout="botonOut(this);" onclick="forma.submit();window.close()"></img>            
            <img title='Salir' src="<%= BASEURL %>/images/botones/salir.gif" onmouseover="botonOver(this);" onmouseout="botonOut(this);" onClick="parent.close()"></img>
</td>
    </tr>
  </table>
  </form>
  <div align=center>  </div>      
  <table width=450 border="0" align="center">
  	<tr class="titulo">
                
	</tr>
  </table>
<iframe width=188 height=166 name="gToday:normal:agenda.js:gfPop:plugins_24.js" id="gToday:normal:agenda.js:gfPop:plugins_24.js" src="<%=BASEURL%>/js/Calendario/ipopeng.htm" scrolling="no" frameborder="0" style="visibility:visible; z-index:999; position:absolute; left:-500px; top:0px;"> </iframe>
</body>
</html>