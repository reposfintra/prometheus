<!--
- Autor : Ing. Andr�s Maturana De La Cruz
- Date : 
- Copyrigth Notice : Fintravalores S.A. S.A
-->
<%--
-@(#)
--Descripcion : Pagina JSP, que sirve consultar el movimiento en trafico de una planilla
--%>
<%@page contentType="text/html"%>
<%@page session="true"%>
<%@page errorPage="../error/ErrorPage.jsp"%>
<%@page import="java.util.*"%>
<%@page import="com.tsp.operation.model.*"%>
<%@page import="com.tsp.util.*"%>
<%@page import="com.tsp.operation.model.beans.*, java.util.*"%>
<%@include file="/WEB-INF/InitModel.jsp"%>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<title>Buscar Planilla - Consulta Movimiento De Tr�fico</title>
<link href="<%= BASEURL %>/css/estilostsp.css" rel="stylesheet" type="text/css">
<script SRC='<%=BASEURL%>/js/boton.js'></script>
<link href="../../../css/estilostsp.css" rel="stylesheet" type="text/css">
<script type='text/javascript' src="<%= BASEURL %>/js/validar.js"></script>
</head>
<body onLoad="redimensionar();" onResize="redimensionar();">
<div id="capaSuperior" style="position:absolute; width:100%; height:100px; z-index:0; left: 0px; top: 0px;">
<jsp:include page="/WEB-INF/toptsp.jsp?encabezado=Movimientos Trafico"/>
</div>

<div id="capaCentral" style="position:absolute; width:100%; height:85%; z-index:0; left: 0px; top: 100px; overflow: scroll;"> 
<%
        Vector reporte = model.rmtService.getReportesPlanilla();
        Ingreso_Trafico trafico = model.traficoService.getIngreso();
%>
<form name="forma" id="forma" method="post" action="<%=CONTROLLER%>?estado=MovimientoTrafico&accion=BuscarPlanilla&cmd=show">
  <table width="100%" border="2" align="center">
    <tr>
      <td><table width="100%" align="center" class="tablaInferior">
        <tr>
          <td colspan="2" class="subtitulo1"><div align="left" >Informaci&oacute;n de la Planilla </div></td>
          <td colspan="2" class="barratitulo"><img src="<%=BASEURL%>/images/titulo.gif"></td>
        </tr>
        <tr valign="top">
          <td class="fila">N&uacute;mero Planilla</td>
          <td width="25%" class="letra"><%= trafico.getPlanilla() %></td>
          <td width="23%" class="fila">Placa</td>
          <td width="34%" class="letra"><%= trafico.getPlaca() %></td>
        </tr>
        <tr valign="top">
          <td class="fila">C&eacute;dula Conductor</td>
          <td class="letra"><%= trafico.getCedcon() %></td>
          <td class="fila">Nombre Conductor </td>
          <td class="letra"><%= trafico.getNomcond() %></td>
        </tr>
        <tr valign="top">
          <td class="fila">C&eacute;dula Propietario </td>
          <td class="letra"><%= trafico.getCedprop() %></td>
          <td class="fila">Nombre Propietario </td>
          <td class="letra"><%= trafico.getNomprop() %></td>
        </tr>
        <tr valign="top">
          <td class="fila">Origen</td>
          <td class="letra"><%= trafico.getNomorigen() %></td>
          <td class="fila">Destino</td>
          <td class="letra"><%= trafico.getNomdestino() %></td>
        </tr>
        <tr valign="top">
          <td class="fila">Fecha de Salida </td>
		  <% String fec_salida = trafico.getFecha_salida().substring(0,16); %>
          <td class="letra"><%= ( fec_salida.compareTo("0099-01-01 00:00")==0 ) ?  "NO REGISTRA PLAN DE VIAJE" : fec_salida %></td>
          <td class="fila">Zona</td>
          <td class="letra"><%= trafico.getNomzona() %></td>
        </tr>
        <tr valign="top">
          <td class="fila">Fecha de Despacho </td>
          <td class="letra"><%= trafico.getFecha_despacho().substring(0,16) %></td>
          <td colspan="2" class="fila"><a href="JavaScript:void(0);" class="Simulacion_Hiper" onClick="window.open('<%=CONTROLLER%>?estado=Sancion&accion=BuscarPlanilla&numpla=<%= trafico.getPlanilla()%>', 'VERSANC','status=yes,scrollbars=yes,width=750,height=500,resizable=yes');">Ver Sanciones</a></td>
          </tr>
        <tr valign="top">
          <td class="fila">Escolta <img src="<%= BASEURL%>/images/botones/iconos/buscar.gif" alt="Consultar escolta."  name="imgsalir" align="absmiddle" style="cursor:hand " onClick="window.open('<%= BASEURL %>/jsp/trafico/caravana/buscarEscolta.jsp?titulo=Buscar Escoltas','Escolta','status=yes,scrollbars=no,width=780,height=450,resizable=yes');"></td>
          <td class="letra"><%= trafico.getEscolta() %></td>
          <td class="fila">Caravana <img src="<%= BASEURL%>/images/botones/iconos/buscar.gif" alt="Consultar Caravana."  name="imgsalir" align="absmiddle" style="cursor:hand " onClick="window.open('<%= BASEURL %>/jsp/trafico/caravana/buscarCaravana.jsp?titulo=Buscar Caravanas','Caravana','status=yes,scrollbars=no,width=780,height=450,resizable=yes');"></td>
          <td class="letra"><%= trafico.getCaravana() %></td>
        </tr>
        <tr valign="top">
          <td class="fila">PC Ultimo Reporte </td>
          <td class="letra">
		  <%= trafico.getNompto_control_ultreporte() %></td>
          <td class="fila">Fecha Ultimo Reporte </td>
          <td class="letra">
		  <% if( trafico.getFecha_ult_reporte()!=null && trafico.getFecha_ult_reporte().compareTo("0099-01-01 00:00:00")==0 ){
				out.print("NINGUNA");
			} else {		
				out.print(trafico.getFecha_ult_reporte().substring(0,16));
			}%></td>
        </tr>
        <tr valign="top">
          <td class="fila">PC Pr&oacute;ximo Reporte </td>
          <td class="letra"><%= trafico.getNompto_control_proxreporte() %></td>
          <td class="fila">Fecha Pr&oacute;ximo Reporte </td>
          <td class="letra">
		  <% if( trafico.getFecha_prox_reporte()!=null && trafico.getFecha_prox_reporte().compareTo("0099-01-01 00:00:00")==0 ){
				out.print("NINGUNA");
			} else {		
				out.print(trafico.getFecha_prox_reporte().substring(0,16));
			}%></td>
        </tr>
        <tr valign="top">
          <td width="18%" class="fila">Ultima Observaci&oacute;n</td>
          <td colspan="3" class="letra"><%= trafico.getUlt_observacion()%></td>
          </tr>
        <tr class="fila">
          <td colspan="2" class="subtitulo1">Movimiento de Tr&aacute;fico</td>
          <td colspan="2"><span class="subtitulos"><span class="titulo"><span class="barratitulo"><img src="<%=BASEURL%>/images/titulo.gif"></span></span></span></td>
        </tr>
        <tr >
          <td colspan="4"><table width="100%" border="1" bordercolor="#999999" bgcolor="#F7F5F4">
            <tr class="tblTitulo">
              <td width="10%" align="center" nowrap style="font-size:11px ">Fecha</td>
              <td width="10%" align="center" nowrap style="font-size:11px ">Tipo de Reporte </td>
              <td width="10%" align="center" nowrap style="font-size:11px ">Tipo de Ubicaci&oacute;n </td>
              <td width="15%" align="center" nowrap style="font-size:11px ">Ubicaci&oacute;n</td>
              <td width="31%" align="center" nowrap style="font-size:11px ">Observaciones</td>
			  <td width="8%" align="center" nowrap style="font-size:11px ">Creado Por </td>
			  <td width="8%" align="center" nowrap style="font-size:11px ">Fecha Creaci�n </td>
			  <td width="8%" align="center" nowrap style="font-size:11px ">Modificado Por </td>
              </tr>
<%
      for (int i = 0; i < reporte.size(); i++)
	  {	
                  RepMovTrafico rmt = (RepMovTrafico) reporte.elementAt(i);
%>
            <tr valign="top" class="<%=(i % 2 == 0 )?"filagris":"filaazul"%>">
              <td class="bordereporte" align="center" nowrap style="font-size:11px "><%= rmt.getFechaReporte().substring(0,16) %></td>
              <td class="bordereporte" nowrap style="font-size:11px "><%= rmt.getDesctipo().length()!=0 ? rmt.getDesctipo() : "&nbsp;" %></td>
              <td class="bordereporte" nowrap style="font-size:11px "><%= rmt.getDescubicacion().length()!=0 ? rmt.getDescubicacion() : "&nbsp;" %></td>
              <td class="bordereporte" nowrap style="font-size:11px "><%= rmt.getNombre_procedencia() %></td>
              <td class="bordereporte" wrap style="font-size:11px "><%= (rmt.getObservacion().length()==0 || rmt.getObservacion()==null )? "&nbsp;" : rmt.getObservacion()%></td>  
              <td class="bordereporte" nowrap style="font-size:11px "><%= (rmt.getCreation_user().length()==0 || rmt.getCreation_user()==null )? "&nbsp;" : rmt.getCreation_user()%></td>
              <td class="bordereporte" nowrap style="font-size:11px "><%= (rmt.getCreation_date().length()>16 )? rmt.getCreation_date().substring(0,16) : rmt.getCreation_date()%></td>
			  <td class="bordereporte" nowrap style="font-size:11px "><%= (rmt.getUpdate_user().length()==0 || rmt.getUpdate_user()==null )? "&nbsp;" : rmt.getUpdate_user()%></td>  
			</tr>
<%}%>
          </table></td>
          </tr>
      </table></td>
    </tr>
  </table>
  <br>
  <br>
  <table width="100%" border="0" align="center" cellpadding="0" cellspacing="0">
    <tr align="left">
      <td><img src="<%=BASEURL%>/images/botones/regresar.gif" name="c_regresar" id="c_regresar" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" onClick="parent.history.back();" style='cursor:hand'> <img src="<%=BASEURL%>/images/botones/salir.gif" name="c_regresar" id="c_regresar" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" onClick="parent.close();" style='cursor:hand'>    </tr>
  </table>
</form>
</div>
</body>
</html>

