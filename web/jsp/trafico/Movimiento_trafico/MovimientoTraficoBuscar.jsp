<!--
- Autor : Ing. Andr�s Maturana De La Cruz
- Date : 23.08.2006
- Copyrigth Notice : Fintravalores S.A. S.A
-->
<%--
-@(#)
--Descripcion : Pagina JSP, que sirve consultar el movimiento en trafico de una planilla
--%>
<%@page contentType="text/html"%>
<%@page session="true"%>
<%@page errorPage="../error/ErrorPage.jsp"%>
<%@page import="java.util.*"%>
<%@page import="com.tsp.operation.model.*"%>
<%@page import="com.tsp.util.*"%>
<%@page import="com.tsp.operation.model.beans.*, java.util.*"%>
<%@include file="/WEB-INF/InitModel.jsp"%>
<%String path = application.getRealPath(model.menuService.getRealUrl(request.getRequestURI()));
  String datos[] = model.menuService.getContenidoMenu(BASEURL,request.getRequestURI(),path);
%>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<title>Buscar Planilla - Consulta Movimiento De Tr�fico</title>
<script type='text/javascript' src="<%= BASEURL %>/js/validar.js"></script>
<link href="<%= BASEURL %>/css/estilostsp.css" rel="stylesheet" type="text/css">
<script SRC='<%=BASEURL%>/js/boton.js'></script>
<script>

	function Buscar_Info_Facil () {
	
		if ( window.event.keyCode == 13 ) { 
		
			if( document.forma.numpla.value != "" ) {
			
				document.forma.action = "<%= CONTROLLER %>?estado=MovimientoTrafico&accion=BuscarPlanilla&cmd=show";
				
				document.forma.submit();
				
			} else {
			
				alert( 'No se puede procesar la informaci�n. Verifique que todos los campos esten llenos.' );
				
			}
			
		}
		
	}

</script>
<link href="../../../css/estilostsp.css" rel="stylesheet" type="text/css">
</head>
<body onLoad="redimensionar();" onResize="redimensionar();">
<div id="capaSuperior" style="position:absolute; width:100%; height:100px; z-index:0; left: 0px; top: 0px;">
<jsp:include page="/WEB-INF/toptsp.jsp?encabezado=Buscar Planilla"/>
</div>

<div id="capaCentral" style="position:absolute; width:100%; height:85%; z-index:0; left: 0px; top: 100px; overflow: scroll;"> 
<br>
<form name="forma" id="forma" method="post" action="<%=CONTROLLER%>?estado=MovimientoTrafico&accion=BuscarPlanilla&cmd=show" onSubmit="return validarTCamposLlenos();">
  <table width="365" border="2" align="center">
    <tr>
      <td><table width="100%" class="tablaInferior">
        <tr>
          <td width="50%" class="subtitulo1">Informaci&oacute;n</td>
          <td width="50%" class="barratitulo"><img align="left" src="<%=BASEURL%>/images/titulo.gif" width="32" height="20"> <%=datos[0]%></td>
        </tr>
      </table>
        <table width="100%" align="center" class="tablaInferior">
          <tr class="fila">
            <td width="18%">N&uacute;mero Planilla: </td>
            <%
        String numpla = "";
        numpla = request.getParameter("numpla");
        if(numpla==null) numpla = "";

       %>
            <td width="31%"><input name="numpla" type="text" class="textbox" id="numpla2" value="<%=numpla %>" maxlength="10"  onKeyPress="soloDigitos(event,'noDec')" onKeyUp="Buscar_Info_Facil()">
            <img src="<%= BASEURL%>/images/botones/iconos/obligatorio.gif" width="10" height="10"></td>
          </tr>
        </table>      </td>
    </tr>
  </table>
  <div align="center"><br>
      <input:form name="forma" bean="app" attributesText="id='forma' onSubmit='return validarTCamposLlenos();'" method="post" action="<%=CONTROLLER + "?estado=Aplicacion&accion=Insert&cmd=show"%>" >
	  <img src="<%= BASEURL%>/images/botones/aceptar.gif"  name="imgaceptar" onClick="if ( validarTCamposLlenos() ) forma.submit();" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" style='cursor:hand'>&nbsp; 
	  <img src="<%= BASEURL%>/images/botones/cancelar.gif"  name="imgcancelar"  onMouseOver="botonOver(this);" onClick="forma.reset();" onMouseOut="botonOut(this);" style='cursor:hand'>&nbsp; 
	  <img src="<%= BASEURL%>/images/botones/salir.gif"  name="imgsalir"  onMouseOver="botonOver(this);" onClick="parent.close();" onMouseOut="botonOut(this);" style='cursor:hand'></input:form></div>
</form>

<%if(request.getParameter("msg")!=null){%>
  <table border="2" align="center">
    <tr>
      <td><table width="410" height="73" border="1" align="center"  bordercolor="#F7F5F4" bgcolor="#FFFFFF">
          <tr>
            <td width="282" align="center" class="mensajes"><%=request.getParameter("msg").toString() %></td>
            <td width="28" background="<%=BASEURL%>/images/cuadronaranja.JPG">&nbsp;</td>
            <td width="78">&nbsp;</td>
          </tr>
      </table></td>
    </tr>
  </table>
<%}%>
</div>
<%=datos[1]%>
</body>
</html>

