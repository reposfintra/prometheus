<!--
- Autor : Ing. Andr�s Maturana De La Cruz
- Date : 
- Copyrigth Notice : Fintravalores S.A. S.A
-->
<%--
-@(#)
--Descripcion : Pagina JSP, que sirve consultar el movimiento en trafico de una planilla
--%>
<%@page contentType="text/html"%>
<%@page session="true"%>
<%@page errorPage="../error/ErrorPage.jsp"%>
<%@page import="java.util.*"%>
<%@page import="com.tsp.operation.model.*"%>
<%@page import="com.tsp.util.*"%>
<%@page import="com.tsp.operation.model.beans.*, java.util.*"%>
<%@include file="/WEB-INF/InitModel.jsp"%>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<title>Buscar Planilla - Consulta Movimiento De Tr�fico</title>
<link href="<%= BASEURL %>/css/estilostsp.css" rel="stylesheet" type="text/css">
<script SRC='<%=BASEURL%>/js/boton.js'></script>
<link href="../../../css/estilostsp.css" rel="stylesheet" type="text/css">
<script type='text/javascript' src="<%= BASEURL %>/js/validar.js"></script>
</head>
<body onLoad="redimensionar();" onResize="redimensionar();">
<div id="capaSuperior" style="position:absolute; width:100%; height:100px; z-index:0; left: 0px; top: 0px;">
<jsp:include page="/WEB-INF/toptsp.jsp?encabezado=Movimientos Trafico"/>
</div>

<div id="capaCentral" style="position:absolute; width:100%; height:85%; z-index:0; left: 0px; top: 100px; overflow: scroll;"> 
<%
	String msg = request.getParameter("msg");
	if( msg == null ){
        Vector reporte = (Vector) request.getAttribute("datos");
        //Ingreso_Trafico trafico = model.traficoService.getIngreso();
%>
<form name="forma" id="forma" method="post" action="<%=CONTROLLER%>?estado=MovimientoTrafico&accion=BuscarPlanilla&cmd=show">
  <table width="100%" border="2" align="center">
  
        <tr>
          <td width="50%" class="subtitulo1"><div align="left" >Planillas registradas en Ingeso a Tr&aacute;fico con la placa digitada. </div></td>
          <td width="50%" class="barratitulo"><img src="<%=BASEURL%>/images/titulo.gif"></td>
        </tr>
    <tr>
      <td colspan="2">
<table width="100%" border="1" cellpadding="4" cellspacing="1" bordercolor="#999999" bgcolor="#F7F5F4" align="center">
  <tr class="tblTitulo" align="center">
    <td >Planilla</td>
    <td>Placa</td>
    <td>Conductor</td>
    <td>Fecha Salida </td>
    <td>Origen</td>
    <td>Destino</td>
  </tr>
<%
		for( int i=0; i<reporte.size(); i++ ){
			Ingreso_Trafico trafico = (Ingreso_Trafico) reporte.elementAt(i);
			String fec_salida = trafico.getFecha_salida().substring(0,16); 
%>
  <tr class="<%=(i % 2 == 0 )?"filagris":"filaazul"%>" onMouseOver='cambiarColorMouse(this)' style="cursor:hand"  title="Ver Movimiento en Tr�fico"
  	onClick="window.open('<%=CONTROLLER%>?estado=MovimientoTrafico&accion=BuscarPlaca&cmd=show&numpla=<%= trafico.getPlanilla() %>', 'MOVPLANILLA','status=yes,scrollbars=yes,width=750,height=500,resizable=yes');"
  >
    <td class="bordereporte"><%= trafico.getPlanilla() %></td>
    <td class="bordereporte"><%= trafico.getPlaca() %></td>
    <td class="bordereporte"><%= trafico.getNomcond() %></td>
    <td class="bordereporte"><%= ( fec_salida.compareTo("0099-01-01 00:00")==0 ) ?  "NO REGISTRA PLAN DE VIAJE" : fec_salida %></span></td>
    <td class="bordereporte"><%= trafico.getNomorigen() %></td>
    <td class="bordereporte"><%= trafico.getNomdestino() %></td>
  </tr>  
<%
		}
%>	  
  </table>
  <br>
  <br>
  <table width="100%" border="0" align="center" cellpadding="0" cellspacing="0">
    <tr align="left">
      <td> <img src="<%=BASEURL%>/images/botones/salir.gif" name="c_regresar" id="c_regresar" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" onClick="parent.close();">    </tr>
  </table>
 <%
 	} else {
 %>
 <table border="2" align="center">
   <tr>
     <td><table width="100%"  border="1" align="center"  bordercolor="#F7F5F4" bgcolor="#FFFFFF">
         <tr>
           <td width="229" align="center" class="mensajes"><%=request.getParameter("msg").toString()%></td>
           <td width="29" background="<%=BASEURL%>/images/cuadronaranja.JPG">&nbsp;</td>
           <td width="58">&nbsp;</td>
         </tr>
     </table></td>
   </tr>
 </table> 
 <br>
 <table width="350" border="0" align="center" cellpadding="0" cellspacing="0">
   <tr align="left">
     <td> <img src="<%=BASEURL%>/images/botones/salir.gif" name="c_regresar" id="c_regresar" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" onClick="parent.close();">  
   </tr>
 </table>
 <%
 	}
 %>
</form>
</div>
</body>
</html>

