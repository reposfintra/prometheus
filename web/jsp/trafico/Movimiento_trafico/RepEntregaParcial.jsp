<%@page contentType="text/html"%>
<%@page session="true"%>
<%@page errorPage="../error/ErrorPage.jsp"%>
<%@page import="com.tsp.operation.model.*"%>
<%@page import="com.tsp.util.*"%>
<%@page import="com.tsp.operation.model.beans.*, java.util.*"%>
<%@include file="/WEB-INF/InitModel.jsp"%>
<%@ taglib uri="http://jakarta.apache.org/taglibs/input-0.90" prefix="input" %>
<html>
<head>
<title>Ingresar Movimiento - Movimiento De Tr�fico - TSP</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link href="<%= BASEURL %>/css/estilostsp.css" rel="stylesheet" type="text/css">
<link href="../../../css/estilostsp.css" rel="stylesheet" type="text/css"> 
<script type='text/javascript' src="<%= BASEURL %>/js/boton.js"></script>
<script type='text/javascript' src="<%= BASEURL %>/js/RepMovTraficoSelect.js"></script>
</head>
<body  <%if(request.getParameter("cerrar")!=null){%>onLoad="window.close();"<%}%>>  

  <form method="post" name="forma" action="<%=CONTROLLER%>?estado=Aplicar&accion=Caravana&pagina=movtrafing.jsp&carpeta=jsp/trafico/Movimiento_trafico&cmd=destinos" id='forma'>
    <br>
    <table align=center width='450'>
      <tr>
        <td align="center" colspan="2"> <div align="left" class="informacion">*Seleccione los destinatarios para registrar la entrega.</div></td>
      </tr>
    </table>
    <br>
    <table width="100%" border="2" align="center">
    <tr>
        <td >
            <table width="100%"  align="center"  >
                <tr>            
                    <td width="42%"  class="subtitulo1"><div align="left">Ingresar Movimiento A Caravana</div></td>
                    <td width="58%"  class="barratitulo"><img src="<%=BASEURL%>/images/titulo.gif" width="32" height="20"></td>                    
                </tr>
          </table>        
            <table width="100%"  border="1" bordercolor="#999999">
	  <tr class="tblTitulo">
                <td width="82" >Seleccionar</td>
                <td width="213" >Cliente</td>
                <td width="160" >Destinatarios</td>
                <td width="200" >Ciudad</td>
                <td width="152" >Nombre Contacto</td>
                <td width="150" >Cargo</td>
              </tr>
			<%Vector clientes = model.rmtService.getClientes();
			for(int i = 0; i<clientes.size();i++){
			Remesa rem = (Remesa) clientes.elementAt(i);
			%>
              <tr class="<%=i%2==0?"filagris":"filaazul"%>">
                <td class="bordereporte"><input type="checkbox" name="check<%=i%>" value="<%="Cliente: "+rem.getCodcli()+" Destinatario:  "+rem.getDestinatario()+" Ciudad:  "+rem.getCiuDestinatario()%>"></td>
                <td class="bordereporte"><%=rem.getNombre_cli()%></td>
                <td class="bordereporte"><%=rem.getNom_destinatario()%></td>
                <td class="bordereporte"><%=rem.getNombre_ciu()%></td>
                <td class="bordereporte"><input type="text" name="nombre<%=i%>" style="width:100%"></td>
                <td class="bordereporte"><input type="text" name="cargo<%=i%>" style="width:100%"></td>
              </tr>
			  <%}%>
       	  </table>
		  
	  </td>
    </tr>
  </table>
  <br>
  <table align=center width='450'>
    <tr>
        <td align="center" colspan="2">
            <img title='Destinatarios entrega parcial' src="<%= BASEURL %>/images/botones/aceptar.gif" onmouseover="botonOver(this);" onmouseout="botonOut(this);" onclick="forma.submit();"></img>            
            <img title='Salir' src="<%= BASEURL %>/images/botones/salir.gif" onmouseover="botonOver(this);" onmouseout="botonOut(this);" onClick="parent.close()"></img>
</td>
    </tr>
  </table>
  </form>
  <div align=center>  </div>      
  <table width=450 border="0" align="center">
  	<tr class="titulo">
                
	</tr>
  </table>
<iframe width=188 height=166 name="gToday:normal:agenda.js:gfPop:plugins_24.js" id="gToday:normal:agenda.js:gfPop:plugins_24.js" src="<%=BASEURL%>/js/Calendario/ipopeng.htm" scrolling="no" frameborder="0" style="visibility:visible; z-index:999; position:absolute; left:-500px; top:0px;"> </iframe>
</body>
</html>