<!--  
	 - Author(s)       :      Jose de la Rosa
	 - Description     :      AYUDA FUNCIONAL - Cambio Placa Conductor
	 - Date            :      08/06/2006 
	 - Copyright Notice:      TRANSPORTES SANCHEZ POLO S.A.
-->
<%@ include file="/WEB-INF/InitModel.jsp"%>
<HTML>
<HEAD>
<TITLE>AYUDA FUNCIONAL - Cambio Placa Conductor</TITLE>
<META http-equiv=Content-Type content="text/html; charset=windows-1252">
<link href="../css/estilostsp.css" rel="stylesheet" type="text/css"> 
<link href="<%=BASEURL%>/css/estilostsp.css" rel="stylesheet" type="text/css">
<script type='text/javascript' src="<%= BASEURL %>/js/boton.js"></script> 
</HEAD>
<BODY> 
<% String BASEIMG = BASEURL +"/images/ayuda/trafico/movtrafico/"; %>
  <table width="95%"  border="2" align="center">
    <tr>
      <td height="117" >
        <table width="100%" border="0" align="center">
          <tr  class="subtitulo">
            <td height="20"><div align="center">MANUAL DE CAMBIO DE PLACA Y CONDUCTOR</div></td>
          </tr>
          <tr class="subtitulo1">
            <td>Descripci&oacute;n del funcionamiento del programa para el Cambio Placa y Conductor.</td>
          </tr>
          <tr>
            <td  class="ayudaHtmlTexto"><div align="center">
              <p>&nbsp;</p>
              </div></td>
          </tr>
            <tr>
            <td  class="ayudaHtmlTexto"><p class="ayudaHtmlTexto">En la siguiente pantalla permite la modificación de los datos.</p>
            </td>
          </tr>
            <tr>
            <td  class="ayudaHtmlTexto"><div align="center"><IMG src="<%=BASEIMG%>CambioPlacaConductor.JPG" border=0 ></div></td>
          </tr>
                <tr>
                  <td  class="ayudaHtmlTexto"><p>&nbsp;</p>
                    <p><span class="ayudaHtmlTexto">Cuando se presiona sobre el boton de la lupa aparecera un listado como el siguiente, donde se debe presionar sobre uno de ellos para colocar la cedula y el nombre el la pantalla de cambio de plac conductor. </span></p></td>
                </tr>
                <tr>
              <td  class="ayudaHtmlTexto"><div align="center"><IMG src="<%=BASEIMG%>ListadoConductor.JPG" border=0 ></div></td>
            </tr>
            <tr>
            <td  class="ayudaHtmlTexto"><p class="ayudaHtmlTexto">&nbsp;</p>
            <p class="ayudaHtmlTexto">El sistema verifica si la placa esta llena, si no lo estan le saldr&aacute; en la pantalla el siguiente mensaje. </p>
            </td>
            </tr>
            <tr>
            <td  class="ayudaHtmlTexto"><div align="center"><IMG  src="<%=BASEIMG%>MensajeLLenarPlaca.JPG" border=0 ></div></td>
            </tr>
                <tr>
                  <td  class="ayudaHtmlTexto"><p>&nbsp;</p>
                    <p><span class="ayudaHtmlTexto">El sistema verifica si la placa existe en la Base de datos, si no lo estan le saldr&aacute; en la pantalla el siguiente mensaje. </span></p></td>
                </tr>
                <tr>
              <td  class="ayudaHtmlTexto"><div align="center"><IMG src="<%=BASEIMG%>MensajeNoExistePlaca.JPG" border=0 ></div></td>
            </tr>			
            <tr>
            <td  class="ayudaHtmlTexto"><p class="ayudaHtmlTexto">&nbsp;</p>
            <p class="ayudaHtmlTexto">El sistema verifica si la cedula del conductor esta llena, si no lo estan le saldr&aacute; en la pantalla el siguiente mensaje. </p>
            </td>
            </tr>
            <tr>
            <td  class="ayudaHtmlTexto"><div align="center"><IMG  src="<%=BASEIMG%>MensajeLLenarConductor.JPG" border=0 ></div></td>
            </tr>		
                <tr>
                  <td  class="ayudaHtmlTexto"><p>&nbsp;</p>
                    <p><span class="ayudaHtmlTexto">El sistema verifica si la cedula del conductor existe en la Base de datos, si no lo estan le saldr&aacute; en la pantalla el siguiente mensaje. </span></p></td>
                </tr>
                <tr>
              <td  class="ayudaHtmlTexto"><div align="center"><IMG src="<%=BASEIMG%>MensajeNoExisteConductor.JPG" border=0 ></div></td>
            </tr>
            </tr>		
                <tr>
                  <td  class="ayudaHtmlTexto"><p>&nbsp;</p>
                    <p><span class="ayudaHtmlTexto">Cuando todos los datos estan correctos saldr&aacute; en la pantalla el siguiente mensaje. </span></p></td>
                </tr>
                <tr>
              <td  class="ayudaHtmlTexto"><div align="center"><IMG src="<%=BASEIMG%>MensajeCambioExitoso.JPG" border=0 ></div></td>
            </tr>			
      </table></td>
    </tr>
  </table>
  <p></p>
<center>
<img src = "<%=BASEURL%>/images/botones/salir.gif" style = "cursor:hand" name = "imgsalir" onClick = "parent.close();" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);">
</center>
</BODY>
</HTML>
