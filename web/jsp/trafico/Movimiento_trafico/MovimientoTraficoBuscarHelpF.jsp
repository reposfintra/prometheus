<%@ include file="/WEB-INF/InitModel.jsp"%>

<HTML>
<HEAD>
<TITLE></TITLE>
<META http-equiv=Content-Type content="text/html; charset=windows-1252">
<link href="../css/estilostsp.css" rel="stylesheet" type="text/css"> 
<link href="<%=BASEURL%>/css/estilostsp.css" rel="stylesheet" type="text/css"> 
</HEAD>
<BODY>  

  <table width="95%"  border="2" align="center">
    <tr>
      <td>
        <table width="100%" border="0" align="center">
          <tr  class="subtitulo">
            <td height="20"><div align="center">CONSULTA MOVIMIENTO TRAFICO </div></td>
          </tr>
          <tr class="subtitulo1">
            <td>Descripci&oacute;n del funcionamiento del programa de consulta de movimiento en tr&aacute;fico.</td>
          </tr>
          <tr>
            <td  height="18"  class="ayudaHtmlTexto">Formulario de consulta de movimiento en tr&aacute;fico.</td>
          </tr>
          <tr>
            <td height="18"  class="ayudaHtmlTexto"><div align="center"><img src="<%= BASEURL%>/images/ayuda/trafico/movtrafico/Dibujo1.JPG"></div></td>
          </tr>
          <tr>
            <td  class="ayudaHtmlTexto">&nbsp;</td>
          </tr>
          <tr>
            <td  class="ayudaHtmlTexto"><p>El sistema verifica que no hayan campos vacios. </p></td>
          </tr>
          <tr>
            <td  class="ayudaHtmlTexto"><div align="center"><img src="<%= BASEURL%>/images/ayuda/trafico/demora/Dibujo2.JPG"></div></td>
          </tr>
          <tr>
            <td  class="ayudaHtmlTexto">&nbsp;</td>
          </tr>
          <tr>
            <td  class="ayudaHtmlTexto"><p>El sistema verifica que toda la informaci&oacute;n ingresada est&eacute; correcta y existan registros de movimiento en tr&aacute;fico con el n&uacute;mero de la planilla en el sistema, de lo contrario nos muestra el siguiente mensaje. </p></td>
          </tr>
          <tr>
            <td  class="ayudaHtmlTexto"><div align="center"><img src="<%= BASEURL %>/images/ayuda/trafico/movtrafico/Dibujo2.JPG"></div></td>
          </tr>
          <tr>
            <td  class="ayudaHtmlTexto">&nbsp;</td>
          </tr>
          <tr>
            <td  class="ayudaHtmlTexto">Si existen registros del movimiento en tr&aacute;fico de la planilla se desplegar&aacute; la vista con informaci&oacute;n detallada de la planilla y de los distintos reportes de movimiento en tr&aacute;fico. </td>
          </tr>
          <tr>
            <td  class="ayudaHtmlTexto"><div align="center"><img src="<%= BASEURL %>/images/ayuda/trafico/movtrafico/Dibujo3.JPG"></div></td>
          </tr>
          <tr>
            <td  class="ayudaHtmlTexto"><p>&nbsp;</p></td>
          </tr>
          
          
      </table></td>
    </tr>
  </table>
</BODY>
</HTML>
