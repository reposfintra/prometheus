<%@page contentType="text/html"%>
<%@page session="true"%>
<%@page errorPage="../error/ErrorPage.jsp"%>
<%@page import="java.util.*"%>
<%@page import="com.tsp.operation.model.*"%>
<%@page import="com.tsp.util.*"%>
<%@page import="com.tsp.operation.model.beans.*, java.util.*"%>
<%@include file="/WEB-INF/InitModel.jsp"%>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<title>Buscar Planilla - Movimiento De Tr�fico - TSP</title>
<link href="<%= BASEURL %>/css/estilostsp.css" rel="stylesheet" type="text/css">
<script type='text/javascript' src="<%= BASEURL %>/js/boton.js"></script>
<script type='text/javascript' src="<%= BASEURL %>/js/validar.js"></script>
</head>
<body onresize="redimensionar()" onload = "redimensionar()">
<div id="capaSuperior" style="position:absolute; width:100%; height:100px; z-index:0; left: 0px; top: 0px;">
	<jsp:include page="/WEB-INF/toptsp.jsp?encabezado=Buscar Planilla"/>
</div>
<div id="capaCentral" style="position:absolute; width:100%; height:87%; z-index:0; left: 0px; top: 100px; overflow: scroll;">
<form name="form" method="post" action="">
<table width="400" border="2" align="center">
    <tr>
        <td>
            <table width="100%" align="center">
                <tr>
                    <td width="173" class="subtitulo1">Informaci�n De Planilla</td>
                    <td width="205" class="barratitulo"><img src="<%=BASEURL%>/images/titulo.gif" width="32" height="20"></td>
                </tr>            
                <tr class="fila">
                    <td>N&uacute;mero Planilla </td>
                    <td>
                        <input type="text" name="numpla" class="textbox" id="numpla"  onkeypress="openIt(event,'<%=CONTROLLER%>');"><img src="<%=BASEURL%>/images/botones/iconos/obligatorio.gif" width="10" height="10"></img>                         
                    </td>
                </tr>
            </table>            
        </td>
    </tr>
</table>
<br>
<table align=center>
    <tr>
        <td>
            <img title='Buscar planilla' src="<%= BASEURL %>/images/botones/buscar.gif" onmouseover="botonOver(this);" onmouseout="botonOut(this);" onclick="window.open('<%=CONTROLLER%>?estado=Buscar&accion=NumPlanilla&mensaje=default&pagina=movtrafing.jsp&carpeta=jsp/trafico/Movimiento_trafico&mensaje=default&numpla=' + form.numpla.value,'','status=yes,scrollbars=no,resizable=yes,width=875,height=600')"></img>
            <img title='Cancelar' src="<%= BASEURL %>/images/botones/cancelar.gif" onmouseover="botonOver(this);" onmouseout="botonOut(this);" onclick='form.reset();'></img>
            <img title='Cerrar ventana' src="<%= BASEURL %>/images/botones/salir.gif" onmouseover="botonOver(this);" onmouseout="botonOut(this);" onclick='parent.close();'></img>
        </td>
    </tr>
</table>
</form>
<p align="center"><STRONG>
    <%
        String msg = (String)(request.getAttribute("mensaje"));
        if(msg!=null){
            out.print(msg);
        }
    %>
</STRONG></p>
</div>
</body>
</html>
<script>
    var isIE = document.all?true:false;
    function openIt(e,CONTROLLER) {		
	var key = (isIE) ? window.event.keyCode : e.which;	
	if(key==13){
            window.open(CONTROLLER+"?estado=Buscar&accion=NumPlanilla&pagina=movtrafing.jsp&carpeta=jsp/trafico/Movimiento_trafico&numpla="+form.numpla.value,'','status=yes,scrollbars=no,resizable=yes,width=875,height=600');				
	}
    }
</script>
