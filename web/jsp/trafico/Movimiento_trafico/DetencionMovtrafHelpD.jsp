<%@ include file="/WEB-INF/InitModel.jsp"%>
<html>
<head>
<title>Descripcion Campos Reporte Movimiento Trafico</title>
<link href="<%= BASEURL %>/css/estilostsp.css" rel="stylesheet" type="text/css">
</head>

<body>
<br>


<table width="696"  border="2" align="center">
  <tr>
    <td width="811" >
      <table width="100%" border="0" align="center">
        <tr  class="subtitulo">
          <td height="24" colspan="2"><div align="center"> REPORTE DEMORA </div></td>
        </tr>
        <tr class="subtitulo1">
          <td colspan="2"> Ingresar Reporte Demora </td>
        </tr>
        <tr>
          <td width="149" class="fila"> Fecha del Reporte </td>
          <td width="525"  class="ayudaHtmlTexto"> Campo para digitar la fecha del reporte, representado por n&uacute;meros y en el siguiente formato(AA-MM-DD) (HH:MM).</td>
        </tr>
        <tr>
          <td class="fila">Tipo Demora </td>
          <td  class="ayudaHtmlTexto">Campo de selecci&oacute;n donde se encuentran los distintos reportes a realizar. </td>
        </tr>
        <tr>
          <td class="fila">Ubicaci&oacute;n Procedencia </td>
          <td  class="ayudaHtmlTexto">Campo de selecci&oacute;n donde se encuentran las distintas ciudades o puestos de control del reporte. </td>
        </tr>
        <tr>
          <td class="fila">Observaci&oacute;n</td>
          <td  class="ayudaHtmlTexto">Campo de texto donde se digitan las observaciones pertinentes al nuevo reporte creado. </td>
        </tr>
        
      </table>
    </td>
  </tr>
</table>
<p>&nbsp;</p>
<p>&nbsp;</p>

<p>&nbsp;</p>
<p>&nbsp; </p>
</body>
</html>
