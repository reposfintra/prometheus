<!--  
	 - Author(s)       :      Jose de la Rosa
	 - Description     :      AYUDA FUNCIONAL - Cambio Placa Conductor
	 - Date            :      08/06/2006 
	 - Copyright Notice:      TRANSPORTES SANCHEZ POLO S.A.
-->
<%@ include file="/WEB-INF/InitModel.jsp"%>
<HTML>
<HEAD>
<TITLE>AYUDA FUNCIONAL - Cambio Ruta</TITLE>
<META http-equiv=Content-Type content="text/html; charset=windows-1252">
<link href="../css/estilostsp.css" rel="stylesheet" type="text/css"> 
<link href="<%=BASEURL%>/css/estilostsp.css" rel="stylesheet" type="text/css">
<script type='text/javascript' src="<%= BASEURL %>/js/boton.js"></script> 
</HEAD>
<BODY> 
<% String BASEIMG = BASEURL +"/images/ayuda/trafico/movtrafico/"; %>
<% String BASEIMG_ACT = BASEURL +"/images/ayuda/equipos/acuerdo_especial/"; %>
  <table width="95%"  border="2" align="center">
    <tr>
      <td height="117" >
        <table width="100%" border="0" align="center">
          <tr  class="subtitulo">
            <td height="20"><div align="center">MANUAL DE CAMBIO RUTA </div></td>
          </tr>
          <tr class="subtitulo1">
            <td>Descripci&oacute;n del funcionamiento del programa para el Cambio Ruta.</td>
          </tr>
          <tr>
            <td  class="ayudaHtmlTexto"><div align="center">
              <p>&nbsp;</p>
              </div></td>
          </tr>
            <tr>
            <td  class="ayudaHtmlTexto"><p class="ayudaHtmlTexto">En la siguiente pantalla permite escoger la via dependiendo a un origen y a un destino.</p>
            </td>
          </tr>
            <tr>
            <td  class="ayudaHtmlTexto"><div align="center"><IMG src="<%=BASEIMG%>CambioRuta.JPG" border=0 ></div></td>
          </tr>
            <tr>
            <td  class="ayudaHtmlTexto"><p class="ayudaHtmlTexto">&nbsp;</p>
            <p class="ayudaHtmlTexto">El sistema verifica si todos los datos estan llenos, si no lo estan le saldr&aacute; en la pantalla el siguiente mensaje. </p>
            </td>
            </tr>
            <tr>
            <td  class="ayudaHtmlTexto"><div align="center"><IMG  src="<%=BASEIMG_ACT%>MensajeErrorCamposLLenos.JPG" border=0 ></div></td>
            </tr>		  
                <tr>
                  <td  class="ayudaHtmlTexto"><p>&nbsp;</p>
                    <p><span class="ayudaHtmlTexto">Cuando se ha seleccionado una via y se ha escogido una ubicaci&oacute;n y no ha presentado ningun error, pasara a la pantalla de justificaci&oacute;n como se muestra en la siguiente ilustraci&oacute;n. </span></p></td>
                </tr>
                <tr>
              <td  class="ayudaHtmlTexto"><div align="center"><IMG src="<%=BASEIMG%>JustificacionVia.JPG" border=0 ></div></td>
            </tr>

                <tr>
                  <td  class="ayudaHtmlTexto"><p>&nbsp;</p>
                    <p>El sistema verifica si todos los datos estan llenos, si no lo estan le saldr&aacute; en la pantalla el siguiente mensaje. </p></td>
                </tr>
                <tr>
              <td  class="ayudaHtmlTexto"><div align="center"><IMG src="<%=BASEIMG_ACT%>MensajeErrorCamposLLenos.JPG" border=0 ></div></td>
            </tr>	
                <tr>
                  <td  class="ayudaHtmlTexto"><p>&nbsp;</p>
                    <p><span class="ayudaHtmlTexto">Cuando todos los datos estan correctos saldr&aacute; en la pantalla el siguiente mensaje. </span></p></td>
                </tr>
                <tr>
              <td  class="ayudaHtmlTexto"><div align="center"><IMG src="<%=BASEIMG%>MensajeViaModificada.JPG" border=0 ></div></td>
            </tr>			
      </table></td>
    </tr>
  </table>
  <p></p>
<center>
<img src = "<%=BASEURL%>/images/botones/salir.gif" style = "cursor:hand" name = "imgsalir" onClick = "parent.close();" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);">
</center>
</BODY>
</HTML>
