<%@page contentType="text/html"%>
<%@page session="true"%>
<%@page errorPage="../error/ErrorPage.jsp"%>
<%@page import="com.tsp.operation.model.*"%>
<%@page import="com.tsp.util.*"%>
<%@page import="com.tsp.operation.model.beans.*, java.util.*"%>
<%@include file="/WEB-INF/InitModel.jsp"%>
<%@ taglib uri="http://jakarta.apache.org/taglibs/input-0.90" prefix="input" %>
<%  String path    = application.getRealPath(model.menuService.getRealUrl(request.getRequestURI()));
    String datos[] = model.menuService.getContenidoMenu(BASEURL,request.getRequestURI(),path);
%>
<html>
<head>
<title>Ingresar Movimiento - Movimiento De Tr�fico - TSP</title>
<link href="<%= BASEURL %>/css/estilostsp.css" rel="stylesheet" type="text/css">
<link href="../../../css/estilostsp.css" rel="stylesheet" type="text/css"> 
<script type='text/javascript' src="<%= BASEURL %>/js/boton.js"></script>
<script src="<%=BASEURL%>/js/validar.js"></script>
<script src="<%=BASEURL%>/js/general.js"></script>
<script type='text/javascript' src="<%= BASEURL %>/js/RepMovTraficoSelect.js"></script>
</head>
<body onresize="redimensionar()" ONLOAD="redimensionar();forma.c_hora.focus();forma.c_hora.select();llenarObservacion();<%if(request.getParameter("alert")!=null){%>alert('<%=request.getParameter("alert")%>');<%}%><%if(request.getParameter("cerrar")!=null){%>window.close();<%}%>">  
<div id="capaSuperior" style="position:absolute; width:100%; height:100px; z-index:0; left: 0px; top: 0px;">
	<jsp:include page="/WEB-INF/toptsp.jsp?encabezado=Reporte Detenci�n"/>
</div>
<div id="capaCentral" style="position:absolute; width:100%; height:87%; z-index:0; left: 0px; top: 100px; overflow: scroll;">
  <form method="post" name="forma" action="<%=CONTROLLER%>?estado=Insert&accion=RepMovTrafico&pagina=DetencionMovtraf.jsp&carpeta=jsp/trafico/Movimiento_trafico" id='forma'>

 <%     String open_tramo = request.getAttribute("open_tramo")!=null?(String)request.getAttribute("open_tramo"):""; 
 	DatosPlanillaRMT dp = model.rmtService.getDatosPlanillaRMT();
	if(dp==null){
                out.print("<table width=400 border=2 align=center><tr><td><table width=100%  border=1 align=center  bordercolor=#F7F5F4 bgcolor=#FFFFFF><tr><td width=239 align=center class=mensajes>Error buscando los datos</td><td width=35><img src="+BASEURL+"/images/cuadronaranja.JPG></td></tr></table></td></tr></table>");%>
                <br>
                <table align=center width='400'>
                <tr align=center>
                        <td><img title='Salir' src="<%= BASEURL %>/images/botones/salir.gif" onmouseover="botonOver(this);" onmouseout="botonOut(this);" onClick="parent.close()"></img>&nbsp;
						<img src="<%=BASEURL%>/images/botones/regresar.gif" style="cursor:hand" title="Volver" name="buscar"  onClick="window.location='<%=CONTROLLER%>?estado=Buscar&accion=NumPlanilla&pagina=movtrafing.jsp&carpeta=jsp/trafico/Movimiento_trafico&numpla=<%=dp.getNumpla()%>'" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);"></td>   
                  </tr>                    
                </table><%
	}
	else{%>
		<table align="center" width="850" border="2">
                    <tr>
                        <td>
                            <table width="100%" align="center">
                                <tr>
                                  <td width="50%" class="subtitulo1" colspan='4'>Informaci�n De Planilla
                                  </td>
                                  <td width="50%" class="barratitulo" colspan='4'><img src="<%=BASEURL%>/images/titulo.gif" width="32" height="20" align='left'><%=datos[0]%> </td>
                                </tr>         
                                <tr class="fila">
                                    <td align="center" rowspan="2"><strong>Planilla</strong></td>
                                    <td colspan="2" align="center"><strong>Conductor</strong></td>
                                    <td align="center" rowspan="2"><strong>Placa Veh&iacute;culo </strong></td>
                                    <td colspan="2" align="center"><strong>Origen</strong></td>
                                    <td colspan="2" align="center"><strong>Destino</strong></td>      
                                </tr>
                                <tr class="fila">
                                    <td align="center">C&eacute;dula</td>
                                    <td align="center">Nombre</td>
                                    <td align="center">C&oacute;digo</td>
                                    <td align="center">Descripci&oacute;n</td>
                                    <td align="center">Codigo</td>
                                    <td align="center">Descripci&oacute;n</td>
                                </tr>
                                <tr class="letra">
                                  <td align="center"><%=dp.getNumpla()%></td>
                                    <td align="center"><%=dp.getCedcon()%></td>
                                    <td align="center"><%=dp.getNombrecond()%></td>
                                    <td align="center"><%=dp.getPlaveh()%></td>
                                    <td align="center"><%=dp.getOripla()%></td>
                                    <td align="center"><%=dp.getNomciuori()%></td>
                                    <td align="center"><%=dp.getDespla()%></td>
                                    <td align="center"><%=dp.getNomciudest()%></td>
                              </tr>  
                                <%
                                    RepMovTrafico ultimoreporte = model.rmtService.getLastCreatedReport();                                    
                                    String nombrepc = "";
                                    String nextpc=request.getParameter("ubicacion")!=null?request.getParameter("ubicacion"):model.rmtService.getCodNextPC();
                                    String SalidaSeleccionable = "onchange = seleccionarOrigen(forma);";
                                    if(ultimoreporte!=null){
                                        String fecha = ultimoreporte.getFechaReporte();                                        
                                        if(ultimoreporte.getTipo_procedencia().equalsIgnoreCase("PC")){
                                            nombrepc = ultimoreporte.getUbicacion_procedencia();
                                        }
                                        /*Este else aun no se utiliza, no esta definido...*/
                                        else{
                                            nombrepc = model.rmtService.getDescripcionUbicacion(ultimoreporte.getUbicacion_procedencia());                                
                                        }
                                        if(!nombrepc.equalsIgnoreCase("")){
                                            SalidaSeleccionable = "onchange = salidaSeleccionable(forma);";
                                        }                                        
                                        //nextpc = (request.getParameter("ubicacion")!=null&&request.getParameter("causa")!=null)?request.getParameter("ubicacion"):model.rmtService.getCodNextPC();%>
										  <tr class="letra">										
											<td colspan=4 height="18">&nbsp;<strong>Fecha Ultimo Reporte de Tr�fico:</strong> <%=fecha.substring (0,11)%>&nbsp;&nbsp;&nbsp;<%=fecha.substring (11,16)%></td>
											<td colspan=4 height="18">&nbsp;<strong>Fecha Proximo Reporte Planeado:</strong> <%=dp.getFecha_prox_rep().substring (0,11)%>&nbsp;&nbsp;&nbsp;<%=dp.getFecha_prox_rep().substring (11,16)%></td>
										  </tr>
										  <tr class="letra">
										    <td height="18" colspan=4 >&nbsp;<strong>Ubicaci�n Ultimo Reporte:</strong> <%=model.rmtService.getNombreCiudad(dp.getPto_control_ultreporte().equals("")?dp.getOripla():dp.getPto_control_ultreporte())%></td>
						                    <td height="18" colspan=4 >&nbsp;<strong>Ubicaci�n Proximo Reporte:</strong> <%=model.rmtService.getNombreCiudad(dp.getPto_control_proxreporte())%></td>
							  			</tr>                                                                        
                                    <%}%>
                          </table>
                        </td>
                    </tr>
    </table>
 <% 	       
	java.util.Date date = new java.util.Date();
    java.text.SimpleDateFormat s = new java.text.SimpleDateFormat("yyyy-MM-dd HH:mm");
	String fechareporte =s.format(date);  
	 
	String mensaje = request.getParameter("mensaje");	
	String tiporeporte = request.getParameter("tiporeporte");        
	String tipoubicacion = request.getParameter("tubicacion");        	
	String observaciones = request.getParameter("observacion");
	
 %>  
  <br>    
  <input type="hidden" value="<%=dp.getNumpla()%>" name="numplanilla">
  <input type="hidden" name="mensaje"> 
  <input name="reinicio" type="hidden" value="ok">
  <input name="numpla" type="hidden" id="numpla" value="<%=dp.getNumpla()%>">
  <input type="hidden" name="fecha_actu" id="fecha_actu" value="<%=fechareporte%>"> 
  <input type="hidden" name="controller" value="<%=CONTROLLER%>">
  <input type="hidden" name="rutas" value="<%=dp.getRutapla()%>">
  <input name="frontera" type="hidden" id="frontera" value="<%=dp.getFrontera()%>">
  <input type="hidden" name="destino" id="destino" value="<%=dp.getDespla()%>"> 
  <input name="origen" type="hidden" id="origen" value="<%=dp.getOripla()%>">
  <table width="850" border="2" align="center">
    <tr>
        <td>
            <table width="100%" align="center"  >
                <tr>            
                    <td width="50%" class="subtitulo1"><div align="center">Ingresar Reporte Detenci�n</div></td>
                    <td width="50%" class="barratitulo"><img src="<%=BASEURL%>/images/titulo.gif" width="32" height="20" align='left'><div align="right" class="letra"><strong>Usuario Modificaci�n :</strong> <%=dp.getUser_update()%> &nbsp;</div></td>
                </tr>			
                <tr class="fila">
                    <td align="center"><strong>Fecha del Reporte </strong></td>
                    <td align="center"><strong>Tipo De Detenci&oacute;n</strong></td>
                </tr>
                <tr class="fila">
                    <td align="center">
                        <input name="c_dia" type="text" class="textbox" onBlur="llenarObservacion();" id="c_dia" onClick="" onKeyPress="soloDigitos(event,'decNo');FormatNumber(this,'####-##-##');" value="<%=request.getParameter("c_dia")!=null?request.getParameter("c_dia"):fechareporte.substring (0,10)%>" size="13" maxlength="10">
            			<input name="c_hora" type="text" class="textbox" onBlur="llenarObservacion();" id="c_hora" onClick="" onKeyPress="soloDigitos(event,'decNo');FormatNumber(this,'##:##');" value="<%=request.getParameter("c_hora")!=null?request.getParameter("c_hora"):fechareporte.substring (11,16)%>" size="6" maxlength="5">
						&nbsp;<img src="<%=BASEURL%>/images/botones/iconos/obligatorio.gif" width="10" height="10"></img>
                    </td>
                    <td align="center">                        
                            <input:select name="tiporep" options="<%=model.tblgensvc.getLista_des()%>" default="<%=tiporeporte%>" attributesText="style='width:38%;' onchange =isSancion(this.value);llenarObservacion();"/>
                        <img src="<%=BASEURL%>/images/botones/iconos/obligatorio.gif" width="10" height="10"></img>
                    </td>
                </tr>    
                <tr class="fila">
                    <td align="center" colspan="2"><strong>Ubicaci&oacute;n de Procedencia </strong></td>
                </tr>
                <tr class="fila">
                  <td align="center" colspan="2">
				  <input name="campo" type="text" class="textbox" id="campo" style="width:90%" onKeyUp="mostrarCiudadDinamica(document.forma.ubicacion,this);" ><br>
                      <select name="ubicacion" id="ubicacion" class='listmenu' size='5' style='width:90%' onclick='document.forma.campo.value = this[this.selectedIndex].innerText;llenarObservacion();' >
                            <%  
                                Vector ubicaciones = model.rmtService.getUbicaciones();
                                if (ubicaciones!=null){ 
                                    for(int i=0;i<ubicaciones.size();i++){
                                        Vector tmp = (Vector)(ubicaciones.get(i));%>
                                    <option value="<%=(String)tmp.get(0)%>" <%=nextpc.equals((String)tmp.get(0))?"selected":""%>><%=(String)(tmp.get(1))%></option>
                                    <%}
                                }
                             %>
                    </select>                    </img></td> 
                </tr>
                <tr class="fila">
                    <td colspan="2" align="center"><strong>Observaciones</strong></td>
                </tr>
                <tr class="fila">
                    <td colspan='2' align="center">
					  <input name="obaut" class="textbox" type="text" id="obaut" size="145" readonly>
					<textarea class="textbox" name='observacion' cols=145 rows=5><%if(observaciones!=null)out.print(observaciones);%></textarea></td>      
                </tr>
				<tr class="fila">
					<td align='center'  class='letraResaltada'><a href="javascript: void(0);" onclick="window.open('<%=BASEURL%>/jsp/trafico/Movimiento_trafico/RepPlanillas.jsp?fechar='+ getDate (new Date(), 'yyyy-mm-dd hh:mi') ,'','status=no,scrollbars=yes,width=720,height=400,resizable=yes')">Copiar Reporte a otras planillas</a></td>                    
					<td align='center' class='letraResaltada'><a href="#" onclick="window.open('<%=CONTROLLER%>?estado=Menu&accion=Cargar&pagina=ingresarTramo.jsp&carpeta=/jsp/trafico/tramo&marco=no&opcion=26','','status=no,scrollbars=no,width=800,height=450,resizable=yes')">Tramos</a></td>
				</tr>				
            </table>                         
        </td>
    </tr>
  </table>
  <br>
  <%if(request.getParameter("causa")!=null){%>
  <table width="550" border="2" align="center">
    <tr>
      <td>
        <table width="100%" align="center"  >
          <tr>
            <td width="225" class="subtitulo1"><div align="center">Lista de causas </div></td>
            <td width="225" class="barratitulo"><img src="<%=BASEURL%>/images/titulo.gif" width="32" height="20"></td>
          </tr>
          <tr class="fila">
            <td align="center">Seleccione una causa</td>
            <td align="center"> <div align="left"></div>
			<input:select name="causas" options="<%=model.rmtService.getCausas()%>" default=""  attributesText="onchange =isSancion(this.value);"/></td>
          </tr>
      </table></td>
    </tr>
  </table>
  <br>
    <%}%>
  <table align=center width='450'>
    <tr>
        <td align="center" colspan="2">
            <img style="visibility:visible;cursor:hand" title='Agregar reporte' name="mod" id="mod" src="<%= BASEURL %>/images/botones/aceptar.gif" onmouseover="botonOver(this);" onmouseout="botonOut(this);" onclick="if(tiporep.value=='VAR/'){ forma.action=forma.action+'&causas='; } if( ( forma.ubicacion.value == '<%=dp.getPto_control_ultreporte()%>' ) || ( forma.ubicacion.value == '<%=dp.getPto_control_proxreporte()%>' ) ){ forma.tubicacion.value='PC'; }else{ forma.tubicacion.value='CIU'; } mensaje.value='insert';enviaFormulario('<%=BASEURL%>');"></img> &nbsp;           
            <img title='Salir' src="<%= BASEURL %>/images/botones/salir.gif" onmouseover="botonOver(this);" onmouseout="botonOut(this);" onClick="parent.close()"></img>&nbsp;
			<img src="<%=BASEURL%>/images/botones/regresar.gif" style="cursor:hand" title="Volver" name="buscar"  onClick="window.location='<%=CONTROLLER%>?estado=Buscar&accion=NumPlanilla&pagina=movtrafing.jsp&carpeta=jsp/trafico/Movimiento_trafico&numpla=<%=dp.getNumpla()%>'" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);">
			<input name="tubicacion" type="hidden" value="">
		</td>
    </tr>
  </table>
  </form>
  <div align=center>
    <%        
        if(mensaje!=null && !mensaje.equalsIgnoreCase("default") && !mensaje.equalsIgnoreCase("listarub")) {
            out.print("<table width=450 border=2 align=center><tr><td><table width=100%  border=1 align=center  bordercolor=#F7F5F4 bgcolor=#FFFFFF><tr><td width=350 align=center class=mensajes>"+mensaje+"</td><td width=100><img src="+BASEURL+"/images/cuadronaranja.JPG></td></tr></table></td></tr></table>");            
        }
    %>
  </div>      
  <table width=450 border="0" align="center">
  	<tr class="titulo">
                
	</tr>
  </table>
  <%out.print(open_tramo);%>
  
<%}%>
</div>
<%=datos[1]%> 
</body>
</html>
<script>
document.forma.campo.value = document.forma.ubicacion[document.forma.ubicacion.selectedIndex].innerText;
</script>