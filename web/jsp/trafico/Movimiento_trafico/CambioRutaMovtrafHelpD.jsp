<!--  
	 - Author(s)       :      Jose de la rosa
	 - Description     :      AYUDA DESCRIPTIVA - Cambio Ruta
	 - Date            :      21/06/2006 
	 - Copyright Notice:      TRANSPORTES SANCHEZ POLO S.A.
-->
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN""http://www.w3.org/TR/html4/loose.dtd">
<%@page session="true"%> 
<%@page errorPage="/error/ErrorPage.jsp"%>
<%@page import="com.tsp.operation.model.beans.*, java.util.*"%>
<%@ include file="/WEB-INF/InitModel.jsp"%>
<%@ taglib uri="http://jakarta.apache.org/taglibs/input-0.90" prefix="input" %>
<html>
<head>
<title>AYUDA DESCRIPTIVA - Cambio Ruta</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link href="<%= BASEURL %>/css/estilostsp.css" rel="stylesheet" type="text/css">
<script type='text/javascript' src="<%= BASEURL %>/js/boton.js"></script>
</head>

<body>
<br>
<table width="696"  border="2" align="center">
  <tr>
    <td width="811" >
      <table width="100%" border="0" align="center">
        <tr  class="subtitulo">
          <td height="24" colspan="2"><div align="center">Cambiar Ruta </div></td>
        </tr>
        <tr class="subtitulo1">
          <td colspan="2">Cambio Ruta</td>
        </tr>
		<tr class="subtitulo1">
          <td colspan="2">INFORMACION DEL CAMBIO DE RUTA </td>
        </tr>
           <td  class="fila">Origen</td>
          <td  class="ayudaHtmlTexto">Campo de selecci&oacute;n donde se encuentra el origen de la via a cambiar.</td>
        </tr>
        <tr>
          <td  class="fila">Conductor</td>
          <td  class="ayudaHtmlTexto">Campo de selecci&oacute;n donde se encuentra el  destino de la via a cambiar.</td>
        </tr>
        <tr>
          <td  class="fila">Via</td>
          <td  class="ayudaHtmlTexto">Campo de selecci&oacute;n donde se encuentra la via a cambiar.</td>
        </tr>
        <tr>
          <td  class="fila">Ubicación</td>
          <td  class="ayudaHtmlTexto">Campo de selecci&oacute;n donde se encuentra la ubicación de la via a cambiar.</td>
        </tr>				
        <tr>
          <td class="fila">Bot&oacute;n Aceptar </td>
          <td  class="ayudaHtmlTexto">Bot&oacute;n para confirmar y pasar a la pantalla de justificación del cambio.</td>
        </tr>
		<tr>
          <td width="149" class="fila">Bot&oacute;n Salir</td>
          <td width="525"  class="ayudaHtmlTexto">Bot&oacute;n para salir de la vista 'Cambiar Ruta' y volver a la vista de la busqueda.</td>
        </tr>
		<tr>
          <td width="149" class="fila">Bot&oacute;n Regresar</td>
          <td width="525"  class="ayudaHtmlTexto">Bot&oacute;n para salir de la vista 'Cambiar Ruta' y volver a la vista del reporte.</td>
        </tr>		
      </table>
    </td>
  </tr>
</table>
<p></p>
<center>
<img src = "<%=BASEURL%>/images/botones/salir.gif" style = "cursor:hand" name = "imgsalir" onClick = "parent.close();" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);">
</center>
</body>
</html>
