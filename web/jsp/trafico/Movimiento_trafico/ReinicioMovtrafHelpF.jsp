<%@ include file="/WEB-INF/InitModel.jsp"%>
<% String BASEIMG = BASEURL +"/images/ayuda/trafico/movtrafico/"; %>
<HTML>
<HEAD>
<TITLE></TITLE>
<META http-equiv=Content-Type content="text/html; charset=windows-1252">
<link href="../../css/estilostsp.css" rel="stylesheet" type="text/css"> 
<link href="<%=BASEURL%>/css/estilostsp.css" rel="stylesheet" type="text/css"> 
</HEAD>
<BODY> 
<table width="100%"  border="2" align="center">
    <tr>
      <td >
        <table width="100%" border="0" align="center">
          <tr  class="subtitulo">
            <td height="20"><div align="center">MANUAL DEL REPORTE DE REINICIO </div></td>
          </tr>
          <tr class="subtitulo1">
            <td>Como registrar los reportes de reinicio.</td>
          </tr>
          <tr>
            <td  height="18"  class="ayudaHtmlTexto"><p> El reporte de reinicio  consta de dos partes, un encabezado y un cuerpo donde se agregan los datos para registrar el reporte. Como se <span class="MsoNormal"> presenta en la  Figura 1 .</span></p>
            </td>
          </tr>
          <tr align="center">
            <td height="18"  class="ayudaHtmlTexto"><div align="center">
              <img src="<%= BASEIMG%>ReinicioMovtraf1.JPG"><br>
              <strong>Figura 1</strong>
            </div></td>
          </tr>
          <tr>
            <td  class="ayudaHtmlTexto"><p>Para grabar un reporte de <strong>reinicio</strong> debemos seguir los siguientes pasos: 
              <br>
              -Modificamos o dejamos fecha que viene por default de la fecha del reporte.
              <br>-Colocamos si es necesario una observaci&oacute;n. 
              <br>
              Luego presionamos un click en el bot&oacute;n <strong>Aceptar </strong> para crear el reporte. Adem&aacute;s existen 2 botones adicionales en el reporte uno es <strong>Salir </strong> para salir de la pantalla y otro es <strong>Regresar </strong> para volver a la pantalla de reporte principal.             
            <p>Si usted digita mal la fecha o no digita nada en el campo de la fecha presentar&aacute; un mensaje como se muestra en la Figura 2.              </td>
          </tr>
          <tr  align="center">
            <td  class="ayudaHtmlTexto"><div align="center"><img src="<%= BASEIMG%>movtrafing5.JPG"><br>
              <strong>Figura 2 </strong></div></td>
          </tr>
      </table>
      </td>
  </tr>
</table>	  
  <br>
</BODY>
</HTML>
