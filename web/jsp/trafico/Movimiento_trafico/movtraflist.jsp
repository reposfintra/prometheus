<%@page contentType="text/html"%>
<%@page session="true"%>
<%@page errorPage="../error/ErrorPage.jsp"%>
<%@page import="java.util.*"%>
<%@page import="com.tsp.operation.model.*"%>
<%@page import="com.tsp.util.*"%>
<%@page import="com.tsp.operation.model.beans.*, java.util.*"%>
<%@include file="/WEB-INF/InitModel.jsp"%>
<%@ taglib uri="http://jsptags.com/tags/navigation/pager" prefix="pg" %>
<html>
<head>
    <title>Listado De Reportes - Movimiento Tr�fico - TSP</title>
    <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
    <link href="<%= BASEURL %>/css/estilostsp.css" rel="stylesheet" type="text/css">
    <script type='text/javascript' src="<%= BASEURL %>/js/boton.js"></script>
    <link href="../../../css/estilostsp.css" rel="stylesheet" type="text/css"> 
    <script type='text/javascript' src="<%= BASEURL %>/js/reporte.js"></script> 
    <script type='text/javascript' src="<%= BASEURL %>/js/RepMovTraficoSelect.js"></script>
</head>
<body <%if(request.getParameter("reload")!=null){%>onLoad="parent.opener.location.reload();"<%}%> onresize="redimensionar()" onload = 'redimensionar()'>
    <div id="capaSuperior" style="position:absolute; width:100%; height:100px; z-index:0; left: 0px; top: 0px;">
        <jsp:include page="/WEB-INF/toptsp.jsp?encabezado=Listado De Reportes Movimiento De Tr�fico"/>
    </div>
    <div id="capaCentral" style="position:absolute; width:100%; height:87%; z-index:0; left: 0px; top: 100px; overflow: scroll;">
<%
    String numpla = model.rmtService.getPlanilla();
    Vector reportes = model.rmtService.getReportesPlanilla();
    String style      = "simple";
    String position   = "bottom";
    String index      = "center";
%>

        <table width="100%" border="2" align="center">
            <tr>
            <td>
                <table width="100%" align="center" class="tablaInferior"> 
                    <tr>
                        <td colspan='5'>                
                            <table width="100%"  border="0" cellpadding="0" cellspacing="0" class="barratitulo">
                                <tr>
                                    <td width="50%" class="subtitulo1" colspan='3'>Movimiento de planilla <%=numpla%></td>
                                    <td width="50%" class="barratitulo" colspan='2'><img src="<%=BASEURL%>/images/titulo.gif"></td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                    <td colspan='5'>
                        <table width="100%" border="1" align="center" bordercolor="#999999" bgcolor="#F7F5F4">
                            <tr class="tblTitulo">
                            <td><div align="center"><strong>Fecha</strong></div></td>    
                            <td><div align="center"><strong>Tipo Reporte </strong></div></td>
                            <td><div align="center"><strong>Tipo Ubicaci&oacute;n </strong></div></td>
                            <td><div align="center"><strong>Ubicaci&oacute;n</strong></div></td>    
                            <td><div align="center"><strong>Observaciones</strong></div></td>
                            </tr>
                            
                            
                                <%  
                                      for (int i = 0; i < reportes.size(); i++){
                                        RepMovTrafico rmt = (RepMovTrafico)(reportes.elementAt(i));%>
                                    <tr  class="<%=(i % 2 == 0 )?"filagris":"filaazul"%>" onMouseOver='cambiarColorMouse(this)'   style="cursor:hand"
                                    onClick="window.open('<%=CONTROLLER%>?estado=Detalles&accion=ReporteMovTraf&pagina=movtrafacteli.jsp&carpeta=Movimiento_trafico&mensaje=view&numpla=<%=rmt.getNumpla()%>&fecha=<%=rmt.getFechaReporte()%>&ultimo=<%=rmt.IsUltimo()%>' ,'','status=yes,scrollbars=no,width=700,height=470,resizable=yes')">
                                    <td align=center class="bordereporte"><%=rmt.getFechaReporte().substring(0,16)%></td>
                                    <td align=center class="bordereporte"><%=rmt.getDesctipo().length()!=0 ? rmt.getDesctipo() : "&nbsp;"%></td>
                                    <td align=center class="bordereporte"><%=rmt.getDescubicacion().length()!=0 ? rmt.getDescubicacion() : "&nbsp;"%></td>
                                    <td align=center class="bordereporte"><%=rmt.getNombre_procedencia() %> </td>
                                    <td align=center class="bordereporte"><%=rmt.getObservacion()%></td>
                                    </tr>
                                <% } %>
                               
                        </table>
                    </td>
                    </tr>
                </table>
            </td>
            </tr>
        </table>
        <br>
        <table width='100%' align=center border=0>
            <tr class="titulo">
                <td align=left>
<!-- http://localhost:8084/fintravalores/controller?estado=Buscar&accion=NumPlanilla&mensaje=default&pagina=movtrafing.jsp&carpeta=jsp/trafico/Movimiento_trafico&numpla=596687                
                    <img title='Regresar' src="<%= BASEURL %>/images/botones/regresar.gif" onmouseover="botonOver(this);" onmouseout="botonOut(this);" onclick="parent.close();"></img>-->
                    <img title='Regresar' src="<%= BASEURL %>/images/botones/regresar.gif" onmouseover="botonOver(this);" onmouseout="botonOut(this);" onclick="
                        parent.opener.location = '<%= CONTROLLER %>?estado=Buscar&accion=NumPlanilla&mensaje=default&pagina=movtrafing.jsp&carpeta=jsp/trafico/Movimiento_trafico&numpla=<%= numpla%>'
                        parent.close();">
                    </img>
                </td>
            </tr>
        </table>
    </div>
</body>
</html>
