<%@page contentType="text/html"%>
<%@page session="true"%>
<%@page errorPage="../error/ErrorPage.jsp"%>
<%@page import="com.tsp.operation.model.*"%>
<%@page import="com.tsp.util.*"%>
<%@page import="com.tsp.operation.model.beans.*, java.util.*"%>
<%@include file="/WEB-INF/InitModel.jsp"%>
<%@ taglib uri="http://jakarta.apache.org/taglibs/input-0.90" prefix="input" %>
<%  String path    = application.getRealPath(model.menuService.getRealUrl(request.getRequestURI()));
    String datos[] = model.menuService.getContenidoMenu(BASEURL,request.getRequestURI(),path);
%>
<html>
<head>
<title>Ingresar Movimiento - Movimiento De Tr�fico - TSP</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link href="<%= BASEURL %>/css/estilostsp.css" rel="stylesheet" type="text/css">
<link href="../../../css/estilostsp.css" rel="stylesheet" type="text/css"> 
<script type='text/javascript' src="<%= BASEURL %>/js/boton.js"></script>
<script src="<%=BASEURL%>/js/validar.js"></script>
<script src="<%=BASEURL%>/js/general.js"></script>
<script type='text/javascript' src="<%= BASEURL %>/js/RepMovTraficoSelect.js"></script>
</head>
<body onresize="redimensionar()" ONLOAD="redimensionar();forma.c_hora.focus();forma.c_hora.select();llenarObservacionReinicio();<%if(request.getParameter("alert")!=null){%>alert('<%=request.getParameter("alert")%>');<%}%><%if(request.getParameter("cerrar")!=null){%>window.close();<%}%>">
<div id="capaSuperior" style="position:absolute; width:100%; height:100px; z-index:0; left: 0px; top: 0px;">
	<jsp:include page="/WEB-INF/toptsp.jsp?encabezado=Reporte Movimiento De Tr�fico"/>
</div>
<div id="capaCentral" style="position:absolute; width:100%; height:87%; z-index:0; left: 0px; top: 100px; overflow: scroll;">
  <form method="post" name="forma" action="<%=CONTROLLER%>?estado=Cambio_via&accion=RepMovTrafico&pagina=movtrafing.jsp&carpeta=jsp/trafico/Movimiento_trafico&opcion=3" id='forma'>
 <%
 	DatosPlanillaRMT dp = model.rmtService.getDatosPlanillaRMT();
	if(!dp.getReg_status().equals("D")){
                out.print("<table width=400 border=2 align=center><tr><td><table width=100%  border=1 align=center  bordercolor=#F7F5F4 bgcolor=#FFFFFF><tr><td width=239 align=center class=mensajes>No se ha registrado ninguna detenci�n para registrar un reinicio</td><td width=35><img src="+BASEURL+"/images/cuadronaranja.JPG></td></tr></table></td></tr></table>");%>
                <br>
                <table align=center width='400'>
                <tr align=center>
                        <td><img title='Salir' src="<%= BASEURL %>/images/botones/salir.gif" onmouseover="botonOver(this);" onmouseout="botonOut(this);" onClick="parent.close()"></img>&nbsp;
						<img src="<%=BASEURL%>/images/botones/regresar.gif" style="cursor:hand" title="Volver" name="buscar"  onClick="window.location='<%=CONTROLLER%>?estado=Buscar&accion=NumPlanilla&pagina=movtrafing.jsp&carpeta=jsp/trafico/Movimiento_trafico&numpla=<%=dp.getNumpla()%>'" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);"></td>   
                  </tr>                    
                </table><%
	}
	else{%>
		<table align="center" width="850" border="2">
		<tr>
			<td>
				<table width="100%" align="center">
					<tr>
					  <td width="50%" class="subtitulo1" colspan='4'>Informaci�n De Planilla 
					  </td>
					  <td width="50%" class="barratitulo" colspan='4'><img src="<%=BASEURL%>/images/titulo.gif" width="32" height="20" align='left'><%=datos[0]%></td>
					</tr>         
					<tr class="fila">
						<td align="center" rowspan="2"><strong>Planilla</strong></td>
						<td colspan="2" align="center"><strong>Conductor</strong></td>
						<td align="center" rowspan="2"><strong>Placa Veh&iacute;culo </strong></td>
						<td colspan="2" align="center"><strong>Origen</strong></td>
						<td colspan="2" align="center"><strong>Destino</strong></td>      
					</tr>
					<tr class="fila">
						<td align="center">C&eacute;dula</td>
						<td align="center">Nombre</td>
						<td align="center">C&oacute;digo</td>
						<td align="center">Descripci&oacute;n</td>
						<td align="center">Codigo</td>
						<td align="center">Descripci&oacute;n</td>
					</tr>
					<tr class="letra">
					  <td align="center"><%=dp.getNumpla()%></td>
						<td align="center"><%=dp.getCedcon()%></td>
						<td align="center"><%=dp.getNombrecond()%></td>
						<td align="center"><%=dp.getPlaveh()%></td>
						<td align="center"><%=dp.getOripla()%></td>
						<td align="center"><%=dp.getNomciuori()%></td>
						<td align="center"><%=dp.getDespla()%></td>
						<td align="center"><%=dp.getNomciudest()%></td>
				  </tr>  
					<%
						RepMovTrafico ultimoreporte = model.rmtService.getLastCreatedReport();                                    
						String nombrepc = "";
						String fecha = "";
						String nextpc=request.getParameter("ubicacion")!=null?request.getParameter("ubicacion"):"";
						String SalidaSeleccionable = "onchange = seleccionarOrigen(forma);";
						String nom_ult_rep = "";
						if(ultimoreporte!=null){
							nom_ult_rep = model.rmtService.getNombreCiudad(dp.getPto_control_ultreporte().equals("")?dp.getOripla():dp.getPto_control_ultreporte());
							fecha = ultimoreporte.getFechaReporte();                                        
							if(ultimoreporte.getTipo_procedencia().equalsIgnoreCase("PC")){
								nombrepc = ultimoreporte.getUbicacion_procedencia();
							}
							/*Este else aun no se utiliza, no esta definido...*/
							else{
								nombrepc = model.rmtService.getDescripcionUbicacion(ultimoreporte.getUbicacion_procedencia());                                
							}
							if(!nombrepc.equalsIgnoreCase("")){
								SalidaSeleccionable = "onchange = salidaSeleccionable(forma);";
							}                                        
							nextpc = (request.getParameter("ubicacion")!=null&&request.getParameter("causa")!=null)?request.getParameter("ubicacion"):model.rmtService.getCodNextPC();%>
							  <tr class="letra">										
								<td colspan=4 height="18">&nbsp;<strong>Fecha Ultimo Reporte de Tr�fico:</strong> <%=fecha.substring (0,11)%>&nbsp;&nbsp;&nbsp;<%=fecha.substring (11,16)%></td>
								<td colspan=4 height="18">&nbsp;<strong>Fecha Proximo Reporte Planeado:</strong> <%=dp.getFecha_prox_rep().substring (0,11)%>&nbsp;&nbsp;&nbsp;<%=dp.getFecha_prox_rep().substring (11,16)%></td>
							  </tr>
							  <tr class="letra">
								<td height="18" colspan=4 >&nbsp;<strong>Ubicaci�n Ultimo Reporte:</strong> <%=nom_ult_rep%></td>
								<td height="18" colspan=4 >&nbsp;<strong>Ubicaci�n Proximo Reporte:</strong> <%=model.rmtService.getNombreCiudad(dp.getPto_control_proxreporte())%></td>
							</tr>                                                                        
						<%}%>
			  </table>
			</td>
		</tr>
    </table>
	
 <% 	       
	java.util.Date date = new java.util.Date();
    java.text.SimpleDateFormat s = new java.text.SimpleDateFormat("yyyy-MM-dd HH:mm");
	String fechareporte =s.format(date);   
	String mensaje = request.getParameter("mensaje");	
	String tiporeporte = request.getParameter("tiporeporte");        
	String tipoubicacion = request.getParameter("tubicacion");        	
	String observaciones = request.getParameter("observacion");
	
 %>  
	<input name="tubicacion" type="hidden" value="CIU"> 
	<input type="hidden" name="mensaje" value="insert">
  <br>    
  <table width="850" border="2" align="center">
    <tr>
        <td>
            <table width="100%" align="center"  >
                <tr>            
                    <td class="subtitulo1" width="50%"><div align="center">Ingresar Reporte de Reinicio </div></td>
                    <td class="barratitulo" width="50%"><img src="<%=BASEURL%>/images/titulo.gif" width="32" height="20" align='left'><div align="right" class="letra"><strong>Usuario Modificaci�n :</strong> <%=dp.getUser_update()%> &nbsp;</div></td>
                </tr>							
                <tr class="fila">
                    <td align="center"><strong>Fecha de Detenci&oacute;n</strong></td>
                    <td align="center"><strong>Ciudad de Detenci&oacute;n </strong></td>
                </tr>
                <tr class="letra">
                    <td align="center"><%=!fecha.equals("")?fecha.substring (0,16):""%>
                        
                    </td>
                    <td align="center"><%=dp.getNom_ciudad().equals("")?nom_ult_rep:dp.getNom_ciudad()%>                    
					</td>
                </tr>    
                <tr class="fila">
                    <td colspan="2" align="center"><strong>Fecha de reinicio </strong></td> 
                </tr>
                <tr class="fila">
                    <td colspan="2" align="center">	
  				      <input name="c_dia" type="text" class="textbox" id="c_dia" onBlur="llenarObservacionReinicio();" onKeyPress="soloDigitos(event,'decNo');FormatNumber(this,'####-##-##');" value="<%=request.getParameter("c_dia")!=null?request.getParameter("c_dia"):fechareporte.substring (0,10)%>" size="13" maxlength="10">
            			<input name="c_hora" type="text" class="textbox" id="c_hora" onBlur="llenarObservacionReinicio();" onKeyPress="soloDigitos(event,'decNo');FormatNumber(this,'##:##');" value="<%=request.getParameter("c_hora")!=null?request.getParameter("c_hora"):fechareporte.substring (11,16)%>" size="6" maxlength="5">
						&nbsp;<img src="<%=BASEURL%>/images/botones/iconos/obligatorio.gif" width="10" height="10"></img></td>      	
                </tr>
                <tr class="fila">
                    <td colspan="2" align="center"><strong>Observaciones</strong></td>
                </tr>
              <tr class="fila">
                    <td colspan='2' align=center>
					<input name="obaut" class="textbox" type="text" id="obaut" size="145" readonly>
                  <textarea class="textbox" onBlur="llenarObservacionReinicio();" name='observacion' cols=145 rows=5 onKeyPress="soloAlfa(this)"><%if(observaciones!=null)out.print(observaciones);%></textarea></td>      
                </tr>  
				<tr class="fila">
				<td width="37%" colspan="2" align="right"><a href="javascript: void(0);" onclick="window.open('<%=CONTROLLER%>?estado=Cargar&accion=Varios&carpeta=jsp/trafico/Movimiento_trafico&pagina=RepReinicioPlanillas.jsp&sw=10&fechar='+ getDate (new Date(), 'yyyy-mm-dd hh:mi') ,'','status=no,scrollbars=yes,width=720,height=400,resizable=yes')">Copiar Reporte a otras planillas</a></td>
				</tr>				  				
            </table>                         
        </td>
    </tr>
  </table>
  <br>
  <table align=center width='450'>
    <tr>
        <td align="center" colspan="2">
            <img title='Agregar reporte' name="mod" id="mod" style="visibility:visible;cursor:hand" src="<%= BASEURL %>/images/botones/aceptar.gif" onmouseover="botonOver(this);" onmouseout="botonOut(this);" onclick="<%if(dp.getNom_ciudad().equals("")){%>forma.tubicacion.value='PC';<%}%>mensaje.value='insert';enviaFormularioReinicio('<%=BASEURL%>');"></img>&nbsp;           
            <img title='Salir' src="<%= BASEURL %>/images/botones/salir.gif" onmouseover="botonOver(this);" onmouseout="botonOut(this);" onClick="parent.close()"></img>&nbsp;
			<img src="<%=BASEURL%>/images/botones/regresar.gif" style="cursor:hand" title="Volver" name="buscar"  onClick="window.location='<%=CONTROLLER%>?estado=Buscar&accion=NumPlanilla&pagina=movtrafing.jsp&carpeta=jsp/trafico/Movimiento_trafico&numpla=<%=dp.getNumpla()%>'" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);">
     
</td>
    </tr>
  </table>
  
  <input type="hidden" name="fecha_actu" id="fecha_actu" value="<%=fechareporte%>"> 
  <input name="numpla" type="hidden" id="numpla" value="<%=dp.getNumpla()%>">
  <input name="tiporep" type="hidden" value="REI">
  <input type="hidden" name="rutas" value="<%=dp.getRutapla()%>">
  <input name="ubicacion" type="hidden" value="<%=dp.getCod_ciudad().equals("")?dp.getPto_control_ultreporte().equals("")?dp.getOripla():dp.getPto_control_ultreporte():dp.getCod_ciudad()%>">
  <input name="nomubic" id="nomubic" type="hidden" value="<%=dp.getNom_ciudad().equals("")?nom_ult_rep:dp.getNom_ciudad()%>">
  </form> 
  </span>
    <%        
        if(mensaje!=null && !mensaje.equalsIgnoreCase("default") && !mensaje.equalsIgnoreCase("listarub")) {
            out.print("<table width=450 border=2 align=center><tr><td><table width=100%  border=1 align=center  bordercolor=#F7F5F4 bgcolor=#FFFFFF><tr><td width=350 align=center class=mensajes>"+mensaje+"</td><td width=100><img src="+BASEURL+"/images/cuadronaranja.JPG></td></tr></table></td></tr></table>");            
        }
    %>
  </div>      
  <table width=450 border="0" align="center">
  	<tr class="titulo">
                
	</tr>
  </table>
<%}%>
</div>
<%=datos[1]%> 
</body>
</html>
