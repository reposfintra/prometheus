<%@page contentType="text/html"%>
<%@page session="true"%>
<%@page errorPage="../error/ErrorPage.jsp"%>
<%@page import="com.tsp.operation.model.*"%>
<%@page import="com.tsp.util.*"%>
<%@page import="com.tsp.operation.model.beans.*, java.util.*"%>
<%@include file="/WEB-INF/InitModel.jsp"%>
<%@ taglib uri="http://jakarta.apache.org/taglibs/input-0.90" prefix="input" %>
<html>
<head>
<title>Ingresar Movimiento - Movimiento De Tr�fico - TSP</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link href="<%= BASEURL %>/css/estilostsp.css" rel="stylesheet" type="text/css">
<link href="../../../css/estilostsp.css" rel="stylesheet" type="text/css">
<script src="<%=BASEURL%>/js/validar.js"></script>
<script src="<%=BASEURL%>/js/general.js"></script>
<script type='text/javascript' src="<%= BASEURL %>/js/boton.js"></script>
<script type='text/javascript' src="<%= BASEURL %>/js/RepMovTraficoSelect.js"></script>
</head>
<body onLoad="<%if(request.getParameter("cerrar")!=null){%>window.close();<%}%>">  

  <form method="post" name="forma" action="<%=CONTROLLER%>?estado=Aplicar&accion=Caravana&pagina=movtrafing.jsp&carpeta=jsp/trafico/Movimiento_trafico&cmd=planillas" id='forma'>
    <br>
    <table align=center width='450'>
      <tr class="informacion">
        <td align="center" colspan="2">*Seleccione las planillas y escoja la fecha del reporte para cada una de ellas.</td>
      </tr>
    </table>
    <br>
    <table width="85%" border="2" align="center">
    <tr>
        <td>
            <table width="100%" align="center"  >
                <tr>            
                    <td width="54%" class="subtitulo1"><div align="center">Ingresar Movimiento a otras planillas </div></td>
                    <td width="46%" class="barratitulo"><img src="<%=BASEURL%>/images/titulo.gif" width="32" height="20"></td>                    
                </tr>
        </table>        
            <table width="100%" border="1" bordercolor="#999999">
			
              <tr class="tblTitulo">
                <td width="10%">Seleccionar</td>
                <td width="19%">Planilla</td>
				<td width="19%">Placa</td>
                <td width="35%">Fecha Reporte </td>
                <td width="36%">CAUSAS</td>
              </tr>
			<%Vector caravana = model.rmtService.getPlanillas();
			for(int i = 0; i<caravana.size();i++){
			String numpla = (String) caravana.elementAt(i);
			String vecNumpla[] = numpla.split (",");
			String causas = "causas"+vecNumpla[0];
			%>
              <tr class="<%=i%2==0?"filagris":"filaazul"%>">
                <td class="bordereporte"><input type="checkbox"  name="<%=vecNumpla[0]%>" id="<%=vecNumpla[0]%>" value="checkbox" <%=request.getParameter(vecNumpla[0])!=null?"checked":""%>></td>
                <td class="bordereporte"><%=vecNumpla[0]%></td>
				<td class="bordereporte"><%=vecNumpla[1]%></td>
                <td class="bordereporte">
						<input name="c_dia<%=vecNumpla[0]%>" type="text" class="textbox" id="c_dia<%=vecNumpla[0]%>" onBlur="llenarObservacion();" onKeyPress="soloDigitos(event,'decNo');FormatNumber(this,'####-##-##');" value="<%=request.getParameter("c_dia"+vecNumpla[0])!=null?request.getParameter("c_dia"+vecNumpla[0]):request.getParameter("fechar").substring (0,10)%>" size="13" maxlength="10">
            			<input name="c_hora<%=vecNumpla[0]%>" type="text" class="textbox" onBlur="llenarObservacion();" id="c_hora<%=vecNumpla[0]%>" onKeyPress="soloDigitos(event,'decNo');FormatNumber(this,'##:##');" value="<%=request.getParameter("c_hora"+vecNumpla[0])!=null?request.getParameter("c_hora"+vecNumpla[0]):request.getParameter("fechar").substring (11,16)%>" size="6" maxlength="5">
						&nbsp;<img src="<%=BASEURL%>/images/botones/iconos/obligatorio.gif" width="10" height="10"></img></td>
                <td class="bordereporte"><%if(request.getParameter("causa"+vecNumpla[0])!=null){%>
				<input:select name="<%=causas%>" options="<%=model.rmtService.getCausas()%>" default=""/>
				<%}%></td>
              </tr>
			  <%}%>
          </table></td>
    </tr>
  </table>
  <br>
  <table align=center width='450'>
    <tr>
        <td align="center" colspan="2">
            <img title='Agregar reporte a estas planillas' style="visibility:visible;cursor:hand" name="mod" id="mod" src="<%= BASEURL %>/images/botones/aceptar.gif" onmouseover="botonOver(this);" onmouseout="botonOut(this);" onclick="ValidarFormularioCopia('<%=BASEURL%>');"></img>
            <img title='Salir' src="<%= BASEURL %>/images/botones/salir.gif" onmouseover="botonOver(this);" onmouseout="botonOut(this);" onClick="parent.close()"></img>
</td>
    </tr>
  </table>
  </form>
</body>
</html>
