<%@page contentType="text/html"%>
<%@page session="true"%>
<%@page errorPage="../error/ErrorPage.jsp"%>
<%@page import="com.tsp.operation.model.*"%>
<%@page import="com.tsp.util.*"%>
<%@page import="com.tsp.operation.model.beans.*, java.util.*"%>
<%@include file="/WEB-INF/InitModel.jsp"%>
<%@ taglib uri="http://jakarta.apache.org/taglibs/input-0.90" prefix="input" %>
<%  String path    = application.getRealPath(model.menuService.getRealUrl(request.getRequestURI()));
    String datos[] = model.menuService.getContenidoMenu(BASEURL,request.getRequestURI(),path);
%>
<html>
<head>
<title>Ingresar Movimiento - Movimiento De Tr�fico - TSP</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link href="<%= BASEURL %>/css/estilostsp.css" rel="stylesheet" type="text/css">
<link href="../../../css/estilostsp.css" rel="stylesheet" type="text/css">
<script src="<%=BASEURL%>/js/validar.js"></script>
<script src="<%=BASEURL%>/js/general.js"></script>
<script type='text/javascript' src="<%= BASEURL %>/js/boton.js"></script>
<script type='text/javascript' src="<%= BASEURL %>/js/RepMovTraficoSelect.js"></script>
<style type="text/css">
<!--
.Estilo1 {
	color: #FF0000;
	font-weight: bold;
}
-->
</style>
</head>
<body onresize="redimensionar()" ONLOAD="establecerIndiceInicial(forma);redimensionar();forma.c_hora.focus();forma.c_hora.select();llenarObservacion();">  
<div id="capaSuperior" style="position:absolute; width:100%; height:100px; z-index:0; left: 0px; top: 0px;">
	<jsp:include page="/WEB-INF/toptsp.jsp?encabezado=Reporte Movimiento De Tr�fico"/>
</div>
<div id="capaCentral" style="position:absolute; width:100%; height:87%; z-index:0; left: 0px; top: 100px; overflow: scroll;">
  <form method="post" name="forma" action="<%=CONTROLLER%>?estado=Insert&accion=RepMovTrafico&pagina=movtrafing.jsp&carpeta=jsp/trafico/Movimiento_trafico" id='forma'>
 <% 	DatosPlanillaRMT dp = model.rmtService.getDatosPlanillaRMT(); %>
		<table align="center" width="890" border="2">
                    <tr>
                        <td>
                            <table width="100%" >
                                <tr>
                                  <td width="50%" class="subtitulo1" colspan="4">Informaci�n De Planilla</td>
                                  <td width="50%" class="barratitulo" colspan="4"><img src="<%=BASEURL%>/images/titulo.gif" width="32" height="20" align="left"><%=datos[0]%></td>
                                </tr>         
                                <tr class="fila">
                                    <td align="center" rowspan="2"><strong>Planilla</strong></td>
                                    <td colspan="2" align="center"><strong>Conductor</strong></td>
                                    <td align="center" rowspan="2"><strong>Placa Veh&iacute;culo </strong></td>
                                    <td colspan="2" align="center"><strong>Origen</strong></td>
                                    <td colspan="2" align="center"><strong>Destino</strong></td>
                                </tr>
                                <tr class="fila">
                                    <td align="center">C&eacute;dula</td>
                                    <td align="center">Nombre</td>
                                    <td align="center">C&oacute;digo</td>
                                    <td align="center">Descripci&oacute;n</td>
                                    <td align="center">Codigo</td>
                                    <td align="center">Descripci&oacute;n</td>
                                </tr>
                                <tr class="letra">
                                  <td align="center"><%=dp.getNumpla()%></td>
                                    <td align="center"><%=dp.getCedcon()%></td>
                                    <td align="center"><%=dp.getNombrecond()%></td>
                                    <td align="center"><%=dp.getPlaveh()%></td>
                                    <td align="center"><%=dp.getOripla()%></td>
                                    <td align="center"><%=dp.getNomciuori()%></td>
                                    <td align="center"><%=dp.getDespla()%></td>
                                    <td align="center"><%=dp.getNomciudest()%></td>
                              </tr>  
								<%
                                    RepMovTrafico ultimoreporte = model.rmtService.getLastCreatedReport();                                    
                                    String nombrepc = "";
									String nextpc=request.getParameter("ubicacion")!=null?request.getParameter("ubicacion"):"";
                                    String SalidaSeleccionable = "onchange = seleccionarOrigen(forma);";
                                    String fecha = "";
									if(ultimoreporte!=null){
										fecha = ultimoreporte.getFechaReporte();                                        
                                        if(ultimoreporte.getTipo_procedencia().equalsIgnoreCase("PC")){
                                            nombrepc = ultimoreporte.getUbicacion_procedencia();
                                        }
                                        /*Este else aun no se utiliza, no esta definido...*/
                                        else{
                                            nombrepc = model.rmtService.getDescripcionUbicacion(ultimoreporte.getUbicacion_procedencia());                                
                                        }
                                        if(!nombrepc.equalsIgnoreCase("")){
                                            SalidaSeleccionable = "onchange = salidaSeleccionable(forma);";
                                        }                                        
                                        nextpc = (request.getParameter("ubicacion")!=null&&request.getParameter("causa")!=null)?request.getParameter("ubicacion").split ("/")[0]:dp.getPto_control_proxreporte();%>
										  <tr class="letra">										
											<td colspan=4 height="18">&nbsp;<strong>Fecha Ultimo Reporte de Tr�fico:</strong> <% if(fecha.equals("0099-01-01 00:00:00")){%>Ninguno<%}else{%><%=fecha.substring (0,11)%>&nbsp;&nbsp;&nbsp;<%=fecha.substring (11,16)%><%}%></td>
											<td colspan=4 height="18">&nbsp;<strong>Fecha Proximo Reporte Planeado:</strong> <%=dp.getFecha_prox_rep().substring (0,11)%>&nbsp;&nbsp;&nbsp;<%=dp.getFecha_prox_rep().substring (11,16)%></td>
										  </tr>
										  <tr class="letra">
										    <td height="18" colspan=4 >&nbsp;<strong>Ubicaci�n Ultimo Reporte:</strong> <%=model.rmtService.getNombreCiudad(dp.getPto_control_ultreporte().equals("")?dp.getOripla():dp.getPto_control_ultreporte())%>
									        <input name="eintermed" type="HIDDEN" id="eintermed" value="<%=dp.getEintermedias()%>"></td>
						                    <td height="18" colspan=4 >&nbsp;<strong>Ubicaci�n Proximo Reporte:</strong> <%=!dp.getPto_control_proxreporte().equals("")?model.rmtService.getNombreCiudad(dp.getPto_control_proxreporte()):"Ninguno"%>
					                        <input name="prox_rep" type="HIDDEN" id="prox_rep" value="<%=dp.getPto_control_proxreporte().equals("")?"N/A":dp.getPto_control_proxreporte()%>"></td>
							  			</tr><%if(dp.getEintermedias()!=null){
											if(!dp.getEintermedias().equals("")){%>
										  <tr class="letra">
										    <td height="18" >&nbsp; <strong>Entregas Intermedias:</strong></td>
						                    <td height="18" colspan=7 >&nbsp;<span class="Estilo1"><%=dp.getNeintermedias()%></span></td>
		                      </tr>                                                                        
										  <%}
										  }%>
                                    <%}%>
                          </table>
                        </td>
                    </tr>
    </table>
 <% 	       
	java.util.Date date = new java.util.Date();
    java.text.SimpleDateFormat s = new java.text.SimpleDateFormat("yyyy-MM-dd HH:mm");
	String fechareporte =s.format(date);   
	String mensaje = request.getParameter("mensaje");	
	String tiporeporte = request.getParameter("tiporeporte").equals("EVIA")?"EVIA/":request.getParameter("tiporeporte");        
	String tipoubicacion = request.getParameter("tubicacion");        	
	String observaciones = request.getParameter("observacion");
 %>  
  <br> 
  <input type="hidden" value="<%=dp.getNumpla()%>" name="numplanilla">
  <input type="hidden" name="mensaje"> 
  <input name="numpla" type="hidden" id="numpla" value="<%=dp.getNumpla()%>">
  <input type="hidden" name="controller" value="<%=CONTROLLER%>">
  <input type="hidden" name="rutas" value="<%=dp.getRutapla()%>">
  <input name="frontera" type="hidden" id="frontera" value="<%=dp.getFrontera()%>">
  <input type="hidden" name="destino" id="destino" value="<%=dp.getDespla()%>"> 
  <input name="origen" type="hidden" id="origen" value="<%=dp.getOripla()%>">
  <input name="ultpto" type="hidden" id="ultpto" value="<%=dp.getPto_control_ultreporte()%>">
  <table width="890" border="2" align="center">
    <tr>
        <td>
            <table width="100%" align="center"  >
                <tr>            
                    <td colspan="2" class="subtitulo1"><div align="center">Ingresar Movimiento</div></td>
                    <td colspan="2" class="barratitulo"><img src="<%=BASEURL%>/images/titulo.gif" width="32" height="20" align="left"><div align="right" class="letra"><strong>Usuario Modificaci�n :</strong> <%=dp.getUser_update()%> &nbsp;</div></td>
                </tr>				
                <tr class="fila">
                    <td colspan="2" align="center"><strong>Fecha del Reporte </strong></td>
                    <td colspan="2" align="center"><strong>Tipo De Reporte </strong></td>
                </tr>
                <tr class="fila">
                    <td colspan="2" align="center">
						<input name="c_dia" type="text" class="textbox" id="c_dia" onBlur="llenarObservacion();" onKeyPress="soloDigitos(event,'decNo');FormatNumber(this,'####-##-##');" value="<%=request.getParameter("c_dia")!=null?request.getParameter("c_dia"):fechareporte.substring (0,10)%>" size="13" maxlength="10">
            			<input name="c_hora" type="text" class="textbox" onBlur="llenarObservacion();" id="c_hora" onKeyPress="soloDigitos(event,'decNo');FormatNumber(this,'##:##');" value="<%=request.getParameter("c_hora")!=null?request.getParameter("c_hora"):fechareporte.substring (11,16)%>" size="6" maxlength="5">
						&nbsp;<img src="<%=BASEURL%>/images/botones/iconos/obligatorio.gif" width="10" height="10"></img>
                    </td>
                    <td colspan="2" align="center">
                            <input:select name="tiporep" options="<%=model.tblgensvc.getLista_des()%>" default="<%=tiporeporte%>" attributesText=" style='width:38%;' onchange =isSancion(this.value);llenarObservacion();"/>
                        <img src="<%=BASEURL%>/images/botones/iconos/obligatorio.gif" width="10" height="10"></img>
                    </td>
                </tr>    
                <tr class="fila">
                    <td align="center" colspan="4"><strong>Procedencia </strong></td>
                </tr>
                <tr class="fila">
                    <td colspan="2" align="center">Tipo De Ubicaci�n</td> 
                    <td colspan="2" align="center">Ubicaci&oacute;n</td>
                </tr>
                <tr class="fila">
                    <td colspan="2" align="center">	
                          
                        <select name='tubicacion' class='textbox' onchange="mensaje.value='listarub';this.form.submit();">
                          <%
                                Vector tu = model.rmtService.getTiposubicacion();
                                for(int i=0;i<tu.size();i++){
                                    Vector tmp = (Vector)(tu.get(i));
                                    String codtu = (String)(tmp.get(0));
                                   %>
                          <option value="<%=codtu%>" <%=codtu.equalsIgnoreCase(tipoubicacion)?"selected":""%>><%=(String)(tmp.get(1))%></option>
                          <% }
                            %>
                        </select>
                        <img src="<%=BASEURL%>/images/botones/iconos/obligatorio.gif" width="10" height="10"></img>
                    </td>      	
                    <td colspan="2" align="center">
                        <select name="ubicacion" id="ubicacion" class='textbox' onchange="llenarObservacion();soloOrigen(document.forma);<%if(tipoubicacion.equals("PC")){%>ubicacionSelect(document.forma,'<%=CONTROLLER%>','<%=dp.getNumpla()%>');<%}%>">
                            <%
                                Vector ubicaciones = model.rmtService.getUbicaciones();
								String pcs = "";
								boolean ssw=false;
                                if (ubicaciones!=null){
                                    for(int i=0;i<ubicaciones.size();i++){
                                        Vector tmp = (Vector)(ubicaciones.get(i));
										pcs += (String)tmp.get(0)+",";
										%>
                                    <option value="<%=(String)tmp.get(0)+"/"+(String)(tmp.get(3))%>" <%=nextpc.equals((String)tmp.get(0))?"selected":""%>><%=(String)(tmp.get(1))%></option>
                                    <%}
									if(pcs.length()>0){
										pcs = pcs.substring(0,pcs.length() - 1 );
									}
                                }
                             %>
                        </select>
                        <img src="<%=BASEURL%>/images/botones/iconos/obligatorio.gif" width="10" height="10"></img>
                    </td>
                </tr>
                <tr class="fila">
                    <td colspan="4" align="center"><strong>Observaciones</strong></td>
                </tr>
                <tr class="fila">
                  <td colspan="2" align="center">Clasificacion</td>
                  <td colspan="2" align="center"><input:select name="clasificacion" options="<%=model.rmtService.getListaClasificacion()%>" /></td>
                </tr>	
                <tr class="fila">
                    <td colspan='4' align=center><input name="obaut" class="textbox" type="text" id="obaut" size="145" readonly>
                  <textarea class="textbox" name='observacion' cols=145 rows=5><%if(observaciones!=null)out.print(observaciones);%></textarea></td>      
                </tr>
				<tr class="fila">
				<td width="35%" align="center"><a href="javascript: void(0);" onclick="window.open('<%=CONTROLLER%>?estado=Informacion&accion=Planilla&cmd=show&numpla=<%=dp.getNumpla()%>' ,'','status=no,scrollbars=no,width=720,height=400,resizable=yes')">Ingresar Retraso </a>
				</td>
				<td colspan="2" align="center"><a href="#" onclick="window.open('<%=CONTROLLER%>?estado=Menu&accion=Cargar&pagina=asignarEscolta.jsp&carpeta=/jsp/trafico/caravana&titulo= Nuevo Vehiculo Escolta','','status=no,scrollbars=yes,width=720,height=400,resizable=yes')">Escolta</a></td>
				<td width="37%" align="center"><a href="javascript: void(0);" onclick="window.open('<%=BASEURL%>/jsp/trafico/Movimiento_trafico/RepPlanillas.jsp?fechar='+ getDate (new Date(), 'yyyy-mm-dd hh:mi') ,'','status=no,scrollbars=yes,width=720,height=400,resizable=yes')">Copiar Reporte a otras planillas</a></td>
				</tr>    
  	        	<tr class="fila">
                    <td align="center">
						<p style="cursor:hand" class='Simulacion_Hiper' onclick="window.location='<%=CONTROLLER%>?estado=Cambio_via&accion=RepMovTrafico&pagina=CambioViaMovtraf.jsp&carpeta=jsp/trafico/Movimiento_trafico&numpla=<%=dp.getNumpla()%>&opcion=0&rutas=<%=dp.getRutapla()%>&origen=<%=dp.getOripla()%>&destino=<%=dp.getDespla()%>';">Cambiar Via</p>
					</td>  
					<td colspan="2" align="center"><a href="#" onclick="window.open('<%=CONTROLLER%>?estado=Menu&accion=Cargar&pagina=ingresarTramo.jsp&carpeta=/jsp/trafico/tramo&marco=no&opcion=26','','status=no,scrollbars=no,width=800,height=450,resizable=yes')">Tramos</a>
					</td>         
					<td align="center"><p style="cursor:hand" class='Simulacion_Hiper' onclick="window.location='<%=CONTROLLER%>?estado=Cambio_via&accion=RepMovTrafico&pagina=CambioRutaMovtraf.jsp&carpeta=jsp/trafico/Movimiento_trafico&numpla=<%=dp.getNumpla()%>&opcion=0&rutas=<%=dp.getRutapla()%>&origen=<%=dp.getOripla()%>&destino=<%=dp.getDespla()%>';">Cambiar Ruta</p></td>         
  	        	</tr>
            </table>                         
        </td>
    </tr>
  </table>
  <br>
  <%if(request.getParameter("causa")!=null){%>
  <table width="550" border="2" align="center">
    <tr>
      <td>
        <table width="100%" align="center"  >
          <tr>
            <td width="225" class="subtitulo1"><div align="center">Lista de causas </div></td>
            <td width="225" class="barratitulo"><img src="<%=BASEURL%>/images/titulo.gif" width="32" height="20"></td>
          </tr>
          <tr class="fila">
            <td align="center">Seleccione una causa</td>
            <td align="center"> <div align="left"></div>
			<input:select name="causas" options="<%=model.rmtService.getCausas()%>" default=""  attributesText=" id='causas' onchange =isSancion(this.value);"/></td>
          </tr>
      </table></td>
    </tr>
  </table>
  <br>
    <%}%>
  <table align=center width='450'>
    <tr>
        <td align="center" colspan="2">
            <img title='Agregar reporte' name="mod" id="mod" style="visibility:visible;cursor:hand" src="<%= BASEURL %>/images/botones/aceptar.gif" onmouseover="botonOver(this);" onmouseout="botonOut(this);" onclick="<% if( ( (!pcs.equals("NR") ||!pcs.equals("") ) && tipoubicacion.equals("CIU") && !fecha.equals("0099-01-01 00:00:00") ) || ( (!pcs.equals("NR") ||!pcs.equals("") ) && !tipoubicacion.equals("CIU") ) ){ %> if ( validacionUlt('<%=pcs%>') ){ <%if( model.rmtService.getCaravana().size()<=0){%>mensaje.value='insert';enviaFormulario('<%=BASEURL%>');<%}else{%>this.disabled=true;enviarReporte('<%=BASEURL%>/jsp/trafico/Movimiento_trafico/RepCaravana.jsp');<%}%> } <%}else{%>alert('Debe crear primero un reporte en un puesto de control')<%}%>"></img>            
            <img title='Ver todos los reportes de esta planilla' src="<%= BASEURL %>/images/botones/detalles.gif" onmouseover="botonOver(this);" onmouseout="botonOut(this);" onClick="window.open('<%=CONTROLLER%>?estado=Listar&accion=ReportesMovTraf&pagina=movtraflist.jsp&carpeta=jsp/trafico/Movimiento_trafico&numpla=<%=dp.getNumpla()%>','','status=yes,scrollbars=no,resizable=yes,width=875,height=575')"></img>
            <img title='Salir' src="<%= BASEURL %>/images/botones/salir.gif" onmouseover="botonOver(this);" onmouseout="botonOut(this);" onClick="parent.close()"></img>
            <input name="enviar" type="hidden" id="enviar">
</td>
    </tr>
  </table>
  </form>
  <div align=center><span class="fila">
  </span>
    <%        
        if(mensaje!=null && !mensaje.equalsIgnoreCase("default") && !mensaje.equalsIgnoreCase("listarub")) {
            out.print("<table width=450 border=2 align=center><tr><td><table width=100%  border=1 align=center  bordercolor=#F7F5F4 bgcolor=#FFFFFF><tr><td width=350 align=center class=mensajes>"+mensaje+"</td><td width=100><img src="+BASEURL+"/images/cuadronaranja.JPG></td></tr></table></td></tr></table>");            
        }
    %>
  </div>      
  <table width=450 border="0" align="center">
  	<tr class="titulo">
                
	</tr>
  </table>
</div>
<%=datos[1]%>
</body>
</html>