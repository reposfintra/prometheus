<%@page contentType="text/html"%>
<%@page session="true"%>
<%@page errorPage="../error/ErrorPage.jsp"%>
<%@page import="java.util.*"%>
<%@page import="com.tsp.operation.model.*"%>
<%@page import="com.tsp.util.*"%>
<%@page import="com.tsp.operation.model.beans.*, java.util.*"%>
<%@include file="/WEB-INF/InitModel.jsp"%>
<%@ taglib uri="http://jakarta.apache.org/taglibs/input-0.90" prefix="input" %>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<title>Movimientro Tr�fico - TSP</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link href="<%= BASEURL %>/css/estilostsp.css" rel="stylesheet" type="text/css">
<script type='text/javascript' src="<%= BASEURL %>/js/boton.js"></script>
<link href="../../../css/estilostsp.css" rel="stylesheet" type="text/css"> 
<script src='<%=BASEURL%>/js/date-picker.js'></script>
<script src='<%=BASEURL%>/js/script.js'></script>


   <script>
       function valFachaUltimo(fechaAnterior, fechaActual){
             
            var frm = document.forma;
            var fechaUltimo = replaceALL(replaceALL(replaceALL(replaceALL(frm.fecha.value,' ',''),'-',''),'/',''),':','');
            fechaUltimo = parseInt(fechaUltimo);
            fechaAnterior = parseInt(fechaAnterior);
            fechaActual = parseInt(fechaActual);
            
            if(frm.unregistro.value=='false'){
                if(frm.ultimo.value == 'true'){
                    if((fechaUltimo >= fechaAnterior) &&(fechaUltimo<= fechaActual) ){
                         return true;
                    }else{
                        alert('La fecha debe ser mayor a la fecha anterior y menor que la actual...');
                        return false;
                    }                
                }else{
                    if(fechaUltimo <= fechaActual){
                        return true;
                    }else{
                         alert('La fecha no debe ser mayor a la actual...');
                    }  

                }
            }else{
                 if(fechaUltimo <= fechaActual){
                     return true;
                 }else{
                     alert('La fecha no debe ser mayor a la actual...');
                 } 
            }
            
       
       }
       
function llenarObservacion(){
    var list =forma.tiporep;
    var tiporeporte = list.options[list.selectedIndex].text;

    if(list.options[list.selectedIndex].value!=""){
    //alert (list.options[list.selectedIndex].value ) ;
            if(list.options[list.selectedIndex].value == "ECL/" ||  list.options[list.selectedIndex].value == "EIN/" )	{
                    tiporeporte="FIN DE VIAJE";
            }
            var list2 =forma.ubicacion;
            var pc = list2.options[list2.selectedIndex].text;

    forma.obaut.value = tiporeporte+" "+pc+" "+forma.fecha.value;
    }
    else{
            forma.obaut.value = "";
            }
}

      
   </script>




</head>
<body <%if(request.getParameter("reload")!=null){%> onLoad="window.opener.location.reload();"<%}%> onresize="redimensionar()" onload = "redimensionar();llenarObservacion();">
<div id="capaSuperior" style="position:absolute; width:100%; height:100px; z-index:0; left: 0px; top: 0px;">
	<jsp:include page="/WEB-INF/toptsp.jsp?encabezado=Detalle De Reporte"/>
</div>
<div id="capaCentral" style="position:absolute; width:100%; height:87%; z-index:0; left: 0px; top: 100px; overflow: scroll;">
<% 	
        DatosPlanillaRMT dp = model.rmtService.getDatosPlanillaRMT();
        RepMovTrafico rmt = model.rmtService.getReporteMovTraf();        
        String mensaje = request.getParameter("mensaje");
        String IsUltimo = (request.getParameter("ultimo")!=null)?request.getParameter("ultimo"):"";
        String FecUltimo = model.rmtService.getFechaUltimo();
        String FecAnterior = model.rmtService.getFechaAnterior();
        String UnsoloRegistro = (model.rmtService.isUnSoloRegistro())?"true":"false";
        String FecActual = Util.getFechaActual_String(6).replaceAll("-|:| |/","");
        FecActual = FecActual.substring(0,12);
        String TipReporte = rmt.getTipo_reporte();
        String Tipubicacion = (request.getParameter("tubicacion") != null)?request.getParameter("tubicacion"):rmt.getTipo_procedencia();
        
        // model.rmtService.BuscarUbicaciones(Tipubicacion);
 
        
        if (mensaje==null) mensaje="";
        if(rmt==null){
            out.print("<table width=450 border=2 align=center><tr><td><table width=100%  border=1 align=center  bordercolor=#F7F5F4 bgcolor=#FFFFFF><tr><td width=350 align=center class=mensajes>"+mensaje+"</td><td width=100><img src="+BASEURL+"/images/cuadronaranja.JPG></td></tr></table></td></tr></table>");%>
            <br>
            <table width='450' align=center>
                <tr>
                    <td align=center><img title='Regresar' src="<%= BASEURL %>/images/botones/regresar.gif" onmouseover="botonOver(this);" onmouseout="botonOut(this);" onclick="parent.close();"></img></td>
                </tr>
            </table>
        <%}else if(mensaje.equalsIgnoreCase("Reporte modificado correctamente") || mensaje.equalsIgnoreCase("Reporte eliminado correctamente")){
            out.print("<table width=450 border=2 align=center><tr><td><table width=100%  border=1 align=center  bordercolor=#F7F5F4 bgcolor=#FFFFFF><tr><td width=350 align=center class=mensajes>"+mensaje+"</td><td width=100><img src="+BASEURL+"/images/cuadronaranja.JPG></td></tr></table></td></tr></table>");%>
            <br>
            <table width='450' align=center>
                <tr>
                    <td align=center><img title='Regresar' src="<%= BASEURL %>/images/botones/regresar.gif" onmouseover="botonOver(this);" onmouseout="botonOut(this);" onclick="parent.close();"></img></td>
                </tr>
            </table>
        <%}else{
%>        
  <form method="post" name="forma" action="<%=CONTROLLER%>?estado=Detalles&accion=ReporteMovTraf&pagina=movtrafacteli.jsp&carpeta=jsp/trafico/Movimiento_trafico">  
  <input type="hidden" name="mensaje">  
  <input type="hidden" name="numpla" value="<%=rmt.getNumpla()%>">  
  <input type="hidden" name="olddate" value="<%=rmt.getFechaReporte()%>">  
  <input type="hidden" name="creation_user" value="<%=rmt.getCreation_user()%>">  
  <table width="600" border="2" align="center">
    <tr>
        <td>
            <table width="100%" align="center"> 
                <tr>
                    <td width="250" class="subtitulo1">Datos Del Reporte</td>
                    <td width="200" class="barratitulo"><img src="<%=BASEURL%>/images/titulo.gif" width="32" height="20"></td>
                </tr>                
                <tr class="fila">
                    <td align="center" width="225"><strong>Fecha (YYYY-MM-DD) / Hora (24)</strong></td>
                    <td align="center" width="225"><strong>Tipo De Reporte </strong></td>
                </tr>
                <tr class="fila">
                    <!-- Ivan Dario Gomez 2006-04-11 --> 
                    <td align="center">
                        <input type="hidden" value="<%=IsUltimo%>" name="ultimo">
                        <input type="hidden" value="<%=UnsoloRegistro%>" name="unregistro">
                        <input name="fecha" id="fecha" class=textbox value="<%=rmt.getFechaReporte().substring(0,16)%>"  readonly   size=20>	  	
                         <a href="javascript:void(0)"   onclick="if(self.gfPop)gfPop.fPopCalendar(document.forma.fecha); return false; " HIDEFOCUS><img src="<%=BASEURL%>\js\Calendario\cal.gif" width="16" height="16" border="0" alt="De click aqui para escoger la fecha">
                    </td>
                    <td align="center">
                    <!--Ivan DArio Gomez 2006-04-10 -->
                        <%  TreeMap lista    = model.tblgensvc.getLista_des();
                            TreeMap newLista = new TreeMap();;
                            Iterator it =  lista.keySet().iterator();
                            while( it.hasNext() ){
                                String key = (String) it.next();
                                String valor = (String) lista.get(key);
                                if(!TipReporte.equals("DET") && !TipReporte.equals("DEM") && !TipReporte.equals("REI")){
                                    if ("|DET|DEM|REI|".indexOf("|"+valor+"|")==-1){
                                        newLista.put(key,valor);
                                         
                                    }
                                }else if(valor.equals(TipReporte)){
                                    newLista.put(key,valor);
                                    
                                }
                            }%>
                        
                     
                        
                        
                            <input:select name="tiporep" options="<%=newLista%>"   default="<%=TipReporte%>"  attributesText=" style='width:90%; ' onchange='llenarObservacion();' "/>
                            <img src="<%=BASEURL%>/images/botones/iconos/obligatorio.gif" width="10" height="10"></img>
                            
                            <script>forma.tiporep.value = '<%=TipReporte%>'</script>
                        
                    </td>
                </tr>    
                <tr class="fila">
                    <td align="center" colspan="2"><strong>Procedencia</strong></td>
                </tr>
                <tr class="fila">
                    <td align="center">Tipo De Ubicaci�n</td> 
                    <td align="center">Ubicaci&oacute;n</td>
                </tr>
                <tr class="fila">
                    <td align="center">
                        <select name="tubicacion"  class="textbox" id="tubicacion"  <%if(!TipReporte.equals("PER")){%>onChange="mensaje.value='listarubicaciones';this.form.submit();"<%}%>>
                            <%
                                Vector tu = model.rmtService.getTiposubicacion();
                                for(int i=0;i<tu.size();i++){               
                                    Vector tmp = (Vector)(tu.get(i));
                                    String codtu = (String)(tmp.get(0));
                                        if(codtu.equalsIgnoreCase(Tipubicacion)) out.print("<option value='"+codtu+"' selected>"+(String)(tmp.get(1))+"</option>");
                                    else out.print("<option value='"+codtu+"'>"+(String)(tmp.get(1))+"</option>");
                                }
                            %>  
                        </select>
                        <img src="<%=BASEURL%>/images/botones/iconos/obligatorio.gif" width="10" height="10"></img>
                    </td>      	
	  	<td align="center">
        	<select name="ubicacion" id="ubicacion" class='textbox'  <%if(!TipReporte.equals("PER")){%>onchange='llenarObservacion();'<%}%>>
                    <%
                        Vector ubicaciones = model.rmtService.getUbicaciones();
                        for(int i=0;i<ubicaciones.size();i++){
                            Vector tmp = (Vector)(ubicaciones.get(i));
                            if(rmt.getUbicacion_procedencia().equalsIgnoreCase(String.valueOf(tmp.get(0)))) out.print("<option value='"+(String)(tmp.get(0))+"' selected>"+(String)(tmp.get(1))+"</option>");
                            else out.print("<option value='"+(String)(tmp.get(0))+"'>"+(String)(tmp.get(1))+"</option>");
                        }
                    %>
        	</select>
        	<img src="<%=BASEURL%>/images/botones/iconos/obligatorio.gif" width="10" height="10"></img>
	   </td>
    </tr>
      
	<tr class="fila">
      <td colspan="2"><strong>Observaciones</strong></td>
	</tr>
	 <tr class="fila">
      <td colspan="2" ><input type="text" name="obaut" style='width:620;' readonly ></td>
	</tr>
	<%
            String observaciones = rmt.getObservacion();
            String observacion   = rmt.getObservacion(); 
            int pos = observacion.indexOf(":");
            if(pos!=-1){
              pos+=3; 
              observacion = observacion.substring(pos,observacion.length());  
            }
               
        %>
	<tr class="fila">
		<td colspan='2' >
                <textarea name='observacion' cols='120' rows=3   class='textbox'><%=observacion%></textarea>
            </td>
      
    </tr>      		
	</table>
   </td>
</tr>
  </table>
  <br>
  <table align=center width='100%'>
    <tr>
	<td align="center" colspan="2">
             <%if(!TipReporte.equals("PER")){%>
            <img title='Modificar reporte' src="<%= BASEURL %>/images/botones/modificar.gif" onmouseover="botonOver(this);" onmouseout="botonOut(this);" onclick="llenarObservacion();mensaje.value='update';forma.submit();"></img>
            <%}%>
            
            <%System.out.println("ES ULTIMO REPORTE-->"+rmt.isUltimoRep());
             if(IsUltimo.equals("true")){%><img title='Eliminar reporte' src="<%= BASEURL %>/images/botones/anular.gif" onmouseover="botonOver(this);" onmouseout="botonOut(this);" onclick="mensaje.value='delete';forma.submit();"></img><%}%>
            <img title='Salir' src="<%= BASEURL %>/images/botones/salir.gif" onmouseover="botonOver(this);" onmouseout="botonOut(this);" onclick="parent.close();"></img>
	</td>		
    </tr>
  </table>    
  </form>  
 <iframe width=188 height=166 name="gToday:normal:agenda.js:gfPop:plugins_24.js" id="gToday:normal:agenda.js:gfPop:plugins_24.js" src="<%=BASEURL%>/js/Calendario/ipopeng.htm" scrolling="no" frameborder="0" style="visibility:visible; z-index:999; position:absolute; left:-500px; top:0px;"> 
   
 </iframe>

  <%}%>
</div>
</body>
</html>
