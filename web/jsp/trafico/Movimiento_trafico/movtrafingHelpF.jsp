<%@ include file="/WEB-INF/InitModel.jsp"%>
<html>
<head>
  <title>reporte Movimiento Trafico - Ayuda</title>
  <META http-equiv=Content-Type content="text/html; charset=windows-1252">
  <link href="<%=BASEURL%>/css/estilostsp.css" rel="stylesheet" type="text/css"> 
</head>
<body>
<% String BASEIMG = BASEURL +"/images/ayuda/trafico/movtrafico/"; %>
<table width="100%"  border="2" align="center">
    <tr>
      <td >
		  <table width='99%' align="center" cellpadding='0' cellspacing='0'>
		 <tr class="subtitulo" ><td height="20"><div align="center">MANUAL DEL REPORTE MOVIMIENTO TRAFICO </div></td></tr>
		 <tr class="subtitulo1"> <td>Descripci&oacute;n del funcionamiento </td> </tr>
		 <tr>
		 	<td class="ayudaHtmlTexto"><div class=Section1> 
  <p>El reporte de Movimiento trafico consta de dos partes, un encabezado y un cuerpo donde se agregan los datos para registrar el reporte de movimiento de trafico. En la Figura 1 se mostrar� la vista general.</p> 
  <p align=center style='text-align:center'><img  src="<%= BASEIMG%>movtrafing1.JPG" > </p> 
  <p align="center"><strong>Figura 1</strong></p> 
  <p>En la Figura 2 se muestran los tipos de reportes que se explicaran a continuaci�n de forma detallada.</p> 
  <p align=center style='text-align:center'><img
src="<%= BASEIMG%>movtrafing2.JPG" v:shapes="_x0000_i1032"></p> 
  <p align="center"><strong>Figura 2</strong></p> 
  <p><b>1</b> Si el tipo de reporte que selecciones es <b>Demora</b> se presentara la siguiente vista como lo indica la Figura 3.</p> 
  <p align=center style='text-align:center'><img
src="<%= BASEIMG%>movtrafing3.JPG" v:shapes="_x0000_i1026"></p> 
  <p align="center"><strong>Figura 3</strong></p> 
  <p>Para grabar un reporte de <b>demora</b> debemos seguir los siguientes pasos:</p> 
  <p>-<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </span>Modificamos o dejamos fecha que viene por default de la fecha del reporte.</p> 
  <p>-<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </span>Seleccionar el tipo en el tipo de reporte ya sea <b>Pernoctado</b> o <b>Varado</b>.</p> 
  <p>-<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </span>Seleccionamos la ciudad la ciudad o PC del reporte.</p> 
  <p>-<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </span>Colocamos si es necesario una observaci�n. </p> 
  <p>Luego presionamos un click en el bot�n <b>Aceptar</b> para crear el reporte.</p> 
  <p>Existen 2 botones adicionales en el reporte uno es <b>Detalles</b> para modificar reportes existentes y <b>Salir</b> para salir de la pantalla. </p> 
  <p>Si usted no selecciona ning�n tipo de reporte presentara un mensaje como se muestra en la Figura 4.</p> 
  <p align=center style='text-align:center'><img
src="<%= BASEIMG%>movtrafing4.JPG" v:shapes="_x0000_i1027"></p> 
  <p align="center"><strong>Figura 4</strong></p> 
  <p>Si usted digita mal la fecha o no digita nada en el campo de la fecha presentar� un mensaje como se muestra en la Figura 5.</p> 
  <p align=center style='text-align:center'><img
src="<%= BASEIMG%>movtrafing5.JPG" v:shapes="_x0000_i1028"></p> 
  <p align="center"><strong>Figura 5</strong></p> 
  <p>Si usted digita una fecha mayor a la fecha actual presentar� un mensaje como se muestra en la Figura 6.</p> 
  <p align=center style='text-align:center'><img
src="<%= BASEIMG%>movtrafing6.JPG" v:shapes="_x0000_i1029"></p> 
  <p align="center"><strong>Figura 6</strong></p> 
  <p>Si usted presion� el bot�n <b>Aceptar</b> y no le presento ning�n cuadro anterior y la fecha del pr�ximo reporte planeado tiene una hora de diferencia presentar� un listado como se muestra en la Figura 7.</p> 
  <p align=center style='text-align:center'><img
src="<%= BASEIMG%>movtrafing7.JPG" v:shapes="_x0000_i1030"></p> 
  <p align="center"><strong>Figura 7</strong></p> 
  <p>Si usted no selecciona una causa de la lista presentar� un mensaje como se muestra en la Figura 8.</p> 
  <p align=center style='text-align:center'><img
src="<%= BASEIMG%>movtrafing8.JPG" v:shapes="_x0000_i1031"></p> 
  <p align="center"><strong>Figura 8</strong></p> 
  <p><b>2</b> Si el tipo de reporte que selecciones es <b>En Via</b> se presentar� la vista como lo indica la Figura 1.</p> 
  <p>Para grabar un reporte de <b>En Via</b> debemos seguir los siguientes pasos:</p> 
  <p>-<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </span>Modificamos o dejamos fecha que viene por default de la fecha del reporte.</p> 
  <p>-<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </span>Seleccionar el tipo en el tipo de reporte <b>En Via</b>.</p> 
  <p>-<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </span>Seleccionamos el tipo de ubicaci�n:</p> 
  <p>1.<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </span>Si es <b>Ciudad</b>, aparecer�n en las ubicaciones de las ciudades o los puestos de control.</p> 
  <p>2.<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </span>Si es <b>Puesto de Control</b>, aparecer�n los puestos de control donde se van a registrar los reportes.</p> 
  <p>-<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </span>Seleccionamos la ubicaci�n de procedencia del reporte. </p> 
  <p>-<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </span>Seleccionamos una clasificaci�n si es necesario.</p> 
  <p>-<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </span>Colocamos si es necesario una observaci�n. </p> 
  <p>Luego presionamos un click en el bot�n <b>Aceptar</b> para crear el reporte.</p> 
  <p>Adem�s existen 2 botones adicionales en el reporte uno es <b>Detalles</b> para modificar reportes existentes y <b>Salir</b> para salir de la pantalla. </p> 
  <p>Si usted no selecciona ning�n tipo de reporte presentara un mensaje como se muestra en la Figura 4.</p> 
  <p>Si usted digita mal la fecha o no digita nada en el campo de la fecha presentar� un mensaje como se muestra en la Figura 5.</p> 
  <p>Si usted digita una fecha mayor a la fecha actual presentar� un mensaje como se muestra en la Figura 6.</p> 
  <p>Si usted presion� el bot�n <b>Aceptar</b> y no le presento ning�n cuadro anterior y la fecha del pr�ximo reporte planeado tiene una hora de diferencia presentar� un listado como se muestra en la Figura 7.</p> 
  <p>Si usted no selecciona una causa de la lista presentar� un mensaje como se muestra en la Figura 8.</p> 
  <p><b>3</b> Si el tipo de reporte que selecciones es <b>Entrega Final</b> se presentar� la vista como lo indica la Figura 1 y se abrir� una ventana adicional como aparece en la Figura 9.</p> 
  <p align=center style='text-align:center'><img
src="<%= BASEIMG%>movtrafing9.JPG" v:shapes="_x0000_i1033"></p> 
  <p align="center"><strong>Figura 9</strong></p> 
  <p>Para grabar un reporte de <b>Entrega Final</b> primero debemos seleccionar los destinatarios que les vamos registrar la entrega, en los campos de nombre de contacto y cargo llenamos la informaci�n. Luego damos un click en el bot�n <b>Aceptar</b>. En la pantalla del reporte debemos seguir los siguientes pasos:</p> 
  <p>-<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </span>Modificamos o dejamos fecha que viene por default de la fecha del reporte.</p> 
  <p>-<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </span>Seleccionar el tipo en el tipo de reporte <b>Entrega Final</b>.</p> 
  <p>-<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </span>Seleccionamos el tipo de ubicaci�n:</p> 
  <p>3.<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </span>Si es <b>Ciudad</b>, aparecer�n en las ubicaciones de las ciudades o los puestos de control.</p> 
  <p>4.<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </span>Si es <b>Puesto de Control</b>, aparecer�n los puestos de control donde se van a registrar los reportes.</p> 
  <p>-<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </span>Seleccionamos la ubicaci�n de procedencia del reporte. </p> 
  <p>-<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </span>Seleccionamos una clasificaci�n si es necesario.</p> 
  <p>-<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </span>Colocamos si es necesario una observaci�n. </p> 
  <p>Luego presionamos un click en el bot�n <b>Aceptar</b> para crear el reporte.</p> 
  <p>Adem�s existen 2 botones adicionales en el reporte uno es <b>Detalles</b> para modificar reportes existentes y <b>Salir</b> para salir de la pantalla. </p> 
  <p>Si usted no selecciona ning�n tipo de reporte presentara un mensaje como se muestra en la Figura 4.</p> 
  <p>Si usted digita mal la fecha o no digita nada en el campo de la fecha presentar� un mensaje como se muestra en la Figura 5.</p> 
  <p>Si usted digita una fecha mayor a la fecha actual presentar� un mensaje como se muestra en la Figura 6.</p> 
  <p>Si usted presion� el bot�n <b>Aceptar</b> y no le presento ning�n cuadro anterior y la fecha del pr�ximo reporte planeado tiene una hora de diferencia presentar� un listado como se muestra en la Figura 7.</p> 
  <p>Si usted no selecciona una causa de la lista presentar� un mensaje como se muestra en la Figura 8.</p> 
  <p><b>4</b> Si el tipo de reporte que selecciones es <b>Entrega Frontera</b> se presentar� la vista como lo indica la Figura 1 y se abrir� una ventana adicional como aparece en la Figura 9.</p> 
  <p>Para grabar un reporte de <b>Entrega Frontera</b> primero debemos seleccionar los destinatarios que les vamos registrar la entrega, en los campos de nombre de contacto y cargo llenamos la informaci�n. Luego damos un click en el bot�n <b>Aceptar</b>. En la pantalla del reporte debemos seguir los siguientes pasos:</p> 
  <p>-<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </span>Modificamos o dejamos fecha que viene por default de la fecha del reporte.</p> 
  <p>-<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </span>Seleccionar el tipo en el tipo de reporte <b>Entrega Frontera</b>.</p> 
  <p>-<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </span>Seleccionamos el tipo de ubicaci�n:</p> 
  <p>5.<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </span>Si es <b>Ciudad</b>, aparecer�n en las ubicaciones de las ciudades o los puestos de control.</p> 
  <p>6.<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </span>Si es <b>Puesto de Control</b>, aparecer�n los puestos de control donde se van a registrar los reportes.</p> 
  <p>-<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </span>Seleccionamos la ubicaci�n de procedencia del reporte. </p> 
  <p>-<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </span>Seleccionamos una clasificaci�n si es necesario.</p> 
  <p>-<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </span>Colocamos si es necesario una observaci�n. </p> 
  <p>Luego presionamos un click en el bot�n <b>Aceptar</b> para crear el reporte.</p> 
  <p>Adem�s existen 2 botones adicionales en el reporte uno es <b>Detalles</b> para modificar reportes existentes y <b>Salir</b> para salir de la pantalla. </p> 
  <p>Si usted no selecciona ning�n tipo de reporte presentara un mensaje como se muestra en la Figura 4.</p> 
  <p>Si usted digita mal la fecha o no digita nada en el campo de la fecha presentar� un mensaje como se muestra en la Figura 5.</p> 
  <p>Si usted digita una fecha mayor a la fecha actual presentar� un mensaje como se muestra en la Figura 6.</p> 
  <p>Si usted presion� el bot�n <b>Aceptar</b> y no le presento ning�n cuadro anterior y la fecha del pr�ximo reporte planeado tiene una hora de diferencia presentar� un listado como se muestra en la Figura 7.</p> 
  <p>Si usted no selecciona una causa de la lista presentar� un mensaje como se muestra en la Figura 8. </p> 
  <p><b>5</b> Si el tipo de reporte que selecciones es <b>Entrega Intermedia</b> se presentar� la vista como lo indica la Figura 1.</p> 
  <p>Para grabar un reporte de <b>Entrega Intermedia</b> primero debemos seleccionar los destinatarios que les vamos registrar la entrega, en los campos de nombre de contacto y cargo llenamos la informaci�n. Luego damos un click en el bot�n <b>Aceptar</b>. En la pantalla del reporte debemos seguir los siguientes pasos:</p> 
  <p>-<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </span>Modificamos o dejamos fecha que viene por default de la fecha del reporte.</p> 
  <p>-<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </span>Seleccionar el tipo en el tipo de reporte <b>Entrega Intermedia</b>.</p> 
  <p>-<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </span>Seleccionamos el tipo de ubicaci�n:</p> 
  <p>7.<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </span>Si es <b>Ciudad</b>, aparecer�n en las ubicaciones de las ciudades o los puestos de control.</p> 
  <p>8.<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </span>Si es <b>Puesto de Control</b>, aparecer�n los puestos de control donde se van a registrar los reportes.</p> 
  <p>-<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </span>Seleccionamos la ubicaci�n de procedencia del reporte. </p> 
  <p>-<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </span>Seleccionamos una clasificaci�n si es necesario.</p> 
  <p>-<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </span>Colocamos si es necesario una observaci�n. </p> 
  <p>Luego presionamos un click en el bot�n <b>Aceptar</b> para crear el reporte.</p> 
  <p>Adem�s existen 2 botones adicionales en el reporte uno es <b>Detalles</b> para modificar reportes existentes y <b>Salir</b> para salir de la pantalla. </p> 
  <p>Si usted no selecciona ning�n tipo de reporte presentara un mensaje como se muestra en la Figura 4.</p> 
  <p>Si usted digita mal la fecha o no digita nada en el campo de la fecha presentar� un mensaje como se muestra en la Figura 5.</p> 
  <p>Si usted digita una fecha mayor a la fecha actual presentar� un mensaje como se muestra en la Figura 6.</p> 
  <p>Si usted presion� el bot�n <b>Aceptar</b> y no le presento ning�n cuadro anterior y la fecha del pr�ximo reporte planeado tiene una hora de diferencia presentar� un listado como se muestra en la Figura 7.</p> 
  <p>Si usted no selecciona una causa de la lista presentar� un mensaje como se muestra en la Figura 8.</p> 
  <p><b>6</b> Si el tipo de reporte que selecciones es <b>Entrega Parcial</b> se presentar� la vista como lo indica la Figura 1 y se abrir� una ventana adicional como aparece en la Figura 9.</p> 
  <p>Para grabar un reporte de <b>Entrega Parcial</b> primero debemos seleccionar los destinatarios que les vamos registrar la entrega, en los campos de nombre de contacto y cargo llenamos la informaci�n. Luego damos un click en el bot�n <b>Aceptar</b>. En la pantalla del reporte debemos seguir los siguientes pasos:</p> 
  <p>-<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </span>Modificamos o dejamos fecha que viene por default de la fecha del reporte.</p> 
  <p>-<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </span>Seleccionar el tipo en el tipo de reporte <b>Entrega Parcial</b>.</p> 
  <p>-<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </span>Seleccionamos el tipo de ubicaci�n:</p> 
  <p>9.<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </span>Si es <b>Ciudad</b>, aparecer�n en las ubicaciones de las ciudades o los puestos de control.</p> 
  <p>10.<span style='font:7.0pt "Times New Roman"'>&nbsp; </span>Si es <b>Puesto de Control</b>, aparecer�n los puestos de control donde se van a registrar los reportes.</p> 
  <p>-<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </span>Seleccionamos la ubicaci�n de procedencia del reporte. </p> 
  <p>-<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </span>Seleccionamos una clasificaci�n si es necesario.</p> 
  <p>-<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </span>Colocamos si es necesario una observaci�n. </p> 
  <p>Luego presionamos un click en el bot�n <b>Aceptar</b> para crear el reporte.</p> 
  <p>Adem�s existen 2 botones adicionales en el reporte uno es <b>Detalles</b> para modificar reportes existentes y <b>Salir</b> para salir de la pantalla. </p> 
  <p>Si usted no selecciona ning�n tipo de reporte presentara un mensaje como se muestra en la Figura 4.</p> 
  <p>Si usted digita mal la fecha o no digita nada en el campo de la fecha presentar� un mensaje como se muestra en la Figura 5.</p> 
  <p>Si usted digita una fecha mayor a la fecha actual presentar� un mensaje como se muestra en la Figura 6.</p> 
  <p>Si usted presion� el bot�n <b>Aceptar</b> y no le presento ning�n cuadro anterior y la fecha del pr�ximo reporte planeado tiene una hora de diferencia presentar� un listado como se muestra en la Figura 7.</p> 
  <p>Si usted no selecciona una causa de la lista presentar� un mensaje como se muestra en la Figura 8.</p> 
  <p><b>7</b> Si el tipo de reporte que selecciones es <b>Observaci�n</b> se presentara la siguiente vista como lo indica la Figura 10.</p> 
  <p align=center style='text-align:center'><img
src="<%= BASEIMG%>movtrafing10.JPG" v:shapes="_x0000_i1036"></p> 
  <p align="center"><strong>Figura 10</strong></p> 
  <p>Para grabar un reporte de <b>Observaci�n</b> debemos seguir los siguientes pasos:</p> 
  <p>-<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </span>Modificamos o dejamos fecha que viene por default de la fecha del reporte.</p> 
  <p>-<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </span>Seleccionar el tipo en el tipo de reporte <b>Observaci�n</b>.</p> 
  <p>-<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </span>Seleccionamos el tipo de ubicaci�n:</p> 
  <p>11.<span style='font:7.0pt "Times New Roman"'>&nbsp; </span>Si es <b>Ciudad</b>, aparecer�n en las ubicaciones de las ciudades o los puestos de control.</p> 
  <p>12.<span style='font:7.0pt "Times New Roman"'>&nbsp; </span>Si es <b>Puesto de Control</b>, aparecer�n los puestos de control donde se van a registrar los reportes.</p> 
  <p>-<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </span>Seleccionamos la ubicaci�n de procedencia del reporte. </p> 
  <p>-<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </span>Seleccionamos una clasificaci�n si es necesario.</p> 
  <p>-<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </span>Colocamos si es necesario una observaci�n. </p> 
  <p>Luego presionamos un click en el bot�n <b>Aceptar</b> para crear el reporte.</p> 
  <p>Adem�s existen 2 botones adicionales en el reporte uno es <b>Detalles</b> para modificar reportes existentes, <b>Salir</b> para salir de la pantalla y <b>Regresar</b> para volver a la pantalla de reporte principal. </p> 
  <p>Si usted no selecciona ning�n tipo de reporte presentara un mensaje como se muestra en la Figura 4.</p> 
  <p>Si usted digita mal la fecha o no digita nada en el campo de la fecha presentar� un mensaje como se muestra en la Figura 5.</p> 
  <p>Si usted digita una fecha mayor a la fecha actual presentar� un mensaje como se muestra en la Figura 6.</p> 
  <p><b>8</b> Si el tipo de reporte que selecciones es <b>Reinicio</b> se presentara la siguiente vista como lo indica la Figura 11.</p> 
  <p align=center style='text-align:center'><img
src="<%= BASEIMG%>movtrafing11.JPG" v:shapes="_x0000_i1037"></p> 
  <p align="center"><strong>Figura 11</strong></p> 
  <p>Para grabar un reporte de <b>Reinicio</b> debemos seguir los siguientes pasos:</p> 
  <p>-<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </span>Modificamos o dejamos fecha que viene por default de la fecha del reporte.</p> 
  <p>-<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </span>Seleccionar el tipo en el tipo de reporte <b>Reinicio</b>.</p> 
  <p>-<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </span>Seleccionamos el tipo de ubicaci�n:</p> 
  <p>13.<span style='font:7.0pt "Times New Roman"'>&nbsp; </span>Si es <b>Ciudad</b>, aparecer�n en las ubicaciones de las ciudades o los puestos de control.</p> 
  <p>14.<span style='font:7.0pt "Times New Roman"'>&nbsp; </span>Si es <b>Puesto de Control</b>, aparecer�n los puestos de control donde se van a registrar los reportes.</p> 
  <p>-<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </span>Seleccionamos la ubicaci�n de procedencia del reporte. </p> 
  <p>-<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </span>Seleccionamos una clasificaci�n si es necesario.</p> 
  <p>-<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </span>Colocamos si es necesario una observaci�n. </p> 
  <p>Luego presionamos un click en el bot�n <b>Aceptar</b> para crear el reporte.</p> 
  <p>Adem�s existen 2 botones adicionales en el reporte uno es <b>Detalles</b> para modificar reportes existentes, <b>Salir</b> para salir de la pantalla y <b>Regresar</b> para volver a la pantalla de reporte principal. </p> 
  <p>Si usted no selecciona ning�n tipo de reporte presentara un mensaje como se muestra en la Figura 4.</p> 
  <p>Si usted digita mal la fecha o no digita nada en el campo de la fecha presentar� un mensaje como se muestra en la Figura 5.</p> 
  <p>Si usted digita una fecha mayor a la fecha actual presentar� un mensaje como se muestra en la Figura 6.</p> 
  <p><b>9</b> Si el tipo de reporte que selecciones es <b>Retraso</b> se presentara la siguiente vista como lo indica la Figura 12.</p> 
  <p align=center style='text-align:center'><img
src="<%= BASEIMG%>Dibujo5.JPG" v:shapes="_x0000_i1038"></p> 
  <p align="center"><strong>Figura 12</strong></p> 
  <p><b>10</b> Si el tipo de reporte que selecciones es <b>Salida</b> se presentar� la vista como lo indica la Figura 1.</p> 
  <p>Para grabar un reporte de <b>Salida</b> debemos seguir los siguientes pasos:</p> 
  <p>-<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </span>Modificamos o dejamos fecha que viene por default de la fecha del reporte.</p> 
  <p>-<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </span>Seleccionar el tipo en el tipo de reporte <b>Salida</b>.</p> 
  <p>-<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </span>Seleccionamos el tipo de ubicaci�n:</p> 
  <p>15.<span style='font:7.0pt "Times New Roman"'>&nbsp; </span>Si es <b>Ciudad</b>, aparecer�n en las ubicaciones de las ciudades o los puestos de control.</p> 
  <p>16.<span style='font:7.0pt "Times New Roman"'>&nbsp; </span>Si es <b>Puesto de Control</b>, aparecer�n los puestos de control donde se van a registrar los reportes.</p> 
  <p>-<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </span>Seleccionamos la ubicaci�n de procedencia del reporte. </p> 
  <p>-<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </span>Seleccionamos una clasificaci�n si es necesario.</p> 
  <p>-<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </span>Colocamos si es necesario una observaci�n. </p> 
  <p>Luego presionamos un click en el bot�n <b>Aceptar</b> para crear el reporte.</p> 
  <p>Adem�s existen 2 botones adicionales en el reporte uno es <b>Detalles</b> para modificar reportes existentes y <b>Salir</b> para salir de la pantalla. </p> 
  <p>Si usted no selecciona ning�n tipo de reporte presentara un mensaje como se muestra en la Figura 4.</p> 
  <p>Si usted digita mal la fecha o no digita nada en el campo de la fecha presentar� un mensaje como se muestra en la Figura 5.</p> 
  <p>Si usted digita una fecha mayor a la fecha actual presentar� un mensaje como se muestra en la Figura 6.</p> 
  <p>Si usted presion� el bot�n <b>Aceptar</b> y no le presento ning�n cuadro anterior y la fecha del pr�ximo reporte planeado tiene una hora de diferencia presentar� un listado como se muestra en la Figura 7.</p> 
  <p>Si usted no selecciona una causa de la lista presentar� un mensaje como se muestra en la Figura 8.</p> 
</div> 
</td>
</tr>
</table>
</td>
</tr>
</table>
</body>
</html>
