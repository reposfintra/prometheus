<%@page contentType="text/html"%>
<%@page session="true"%>
<%@page errorPage="../error/ErrorPage.jsp"%>
<%@page import="com.tsp.operation.model.*"%>
<%@page import="com.tsp.util.*"%>
<%@page import="com.tsp.operation.model.beans.*, java.util.*"%>
<%@include file="/WEB-INF/InitModel.jsp"%>
<%@ taglib uri="http://jakarta.apache.org/taglibs/input-0.90" prefix="input" %>
<html>
<head>
<title>Ingresar Movimiento de Observaci&oacute;n</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link href="<%= BASEURL %>/css/estilostsp.css" rel="stylesheet" type="text/css">
<link href="../../../css/estilostsp.css" rel="stylesheet" type="text/css"> 
<script type='text/javascript' src="<%= BASEURL %>/js/boton.js"></script>
<script src="<%=BASEURL%>/js/validar.js"></script>
<script src="<%=BASEURL%>/js/general.js"></script>
<script type='text/javascript' src="<%= BASEURL %>/js/RepMovTraficoSelect.js"></script>
</head>
<body onresize="redimensionar()" ONLOAD="redimensionar();llenarObservacion2();forma.c_hora.focus();forma.c_hora.select();" >  
<div id="capaSuperior" style="position:absolute; width:100%; height:100px; z-index:0; left: 0px; top: 0px;">
	<jsp:include page="/WEB-INF/toptsp.jsp?encabezado=Reporte Movimiento De Tr�fico"/>
</div>
<div id="capaCentral" style="position:absolute; width:100%; height:87%; z-index:0; left: 0px; top: 100px; overflow: scroll;">
  <form method="post" name="forma" action="<%=CONTROLLER%>?estado=Insert&accion=RepMovTrafico&pagina=ingresarObservacion.jsp&carpeta=jsp/trafico/Movimiento_trafico" id='forma'>

 <%
 	DatosPlanillaRMT dp = model.rmtService.getDatosPlanillaRMT();
	if(dp==null){
                out.print("<table width=400 border=2 align=center><tr><td><table width=100%  border=1 align=center  bordercolor=#F7F5F4 bgcolor=#FFFFFF><tr><td width=229 align=center class=mensajes>La planilla no existe o est� anulada</td><td width=29><img src="+BASEURL+"/images/cuadronaranja.JPG></td></tr></table></td></tr></table>");%>
                <br>
                <table align=center width='400'>
                <tr align=center>
                        <td><img title='Salir' src="<%= BASEURL %>/images/botones/salir.gif" onmouseover="botonOver(this);" onmouseout="botonOut(this);" onClick="parent.close()"></img></td>   
                  </tr>                    
                </table><%
	}
	else{%>
		<table align="center" width="850" border="2">
                    <tr>
                        <td>
                            <table width="100%" align="center">
                                <tr>
                                  <td width="173" class="subtitulo1" colspan='4'>Informaci�n De Planilla 
                                  </td>
                                  <td width="205" class="barratitulo" colspan='4'><img src="<%=BASEURL%>/images/titulo.gif" width="32" height="20"></td>
                                </tr>         
                                <tr class="fila">
                                    <td align="center" rowspan="2"><strong>Planilla</strong></td>
                                    <td colspan="2" align="center"><strong>Conductor</strong></td>
                                    <td align="center" rowspan="2"><strong>Placa Veh&iacute;culo </strong></td>
                                    <td colspan="2" align="center"><strong>Origen</strong></td>
                                    <td colspan="2" align="center"><strong>Destino</strong></td>      
                                </tr>
                                <tr class="fila">
                                    <td align="center">C&eacute;dula</td>
                                    <td align="center">Nombre</td>
                                    <td align="center">C&oacute;digo</td>
                                    <td align="center">Descripci&oacute;n</td>
                                    <td align="center">Codigo</td>
                                    <td align="center">Descripci&oacute;n</td>
                                </tr>
                                <tr class="letra">
                                  <td align="center"><%=dp.getNumpla()%></td>
                                    <td align="center"><%=dp.getCedcon()%></td>
                                    <td align="center"><%=dp.getNombrecond()%></td>
                                    <td align="center"><%=dp.getPlaveh()%></td>
                                    <td align="center"><%=dp.getOripla()%></td>
                                    <td align="center"><%=dp.getNomciuori()%></td>
                                    <td align="center"><%=dp.getDespla()%></td>
                                    <td align="center"><%=dp.getNomciudest()%></td>
                              </tr>  
                                <%
                                    RepMovTrafico ultimoreporte = model.rmtService.getLastCreatedReport();                                    
                                    String nombrepc = "";
                                    String nextpc=request.getParameter("ubicacion")!=null?request.getParameter("ubicacion"):"";
                                    String SalidaSeleccionable = "onchange = seleccionarOrigen(forma);";
                                    if(ultimoreporte!=null){
                                        String fecha = ultimoreporte.getFechaReporte();                                        
                                        if(ultimoreporte.getTipo_procedencia().equalsIgnoreCase("PC")){
                                            nombrepc = ultimoreporte.getUbicacion_procedencia();
                                        }
                                        /*Este else aun no se utiliza, no esta definido...*/
                                        else{
                                            nombrepc = model.rmtService.getDescripcionUbicacion(ultimoreporte.getUbicacion_procedencia());                                
                                        }
                                        if(!nombrepc.equalsIgnoreCase("")){
                                            SalidaSeleccionable = "onchange = salidaSeleccionable(forma);";
                                        }                                        
                                        nextpc = (request.getParameter("ubicacion")!=null&&request.getParameter("causa")!=null)?request.getParameter("ubicacion"):dp.getPto_control_proxreporte();%>
  										  <tr class="letra">										
											<td colspan=4 height="18">&nbsp;<strong>Fecha Ultimo Reporte de Tr�fico:</strong> <%=fecha.substring (0,11)%>&nbsp;&nbsp;&nbsp;<%=fecha.substring (11,16)%></td>
											<td colspan=4 height="18">&nbsp;<strong>Fecha Proximo Reporte Planeado:</strong> <%=dp.getFecha_prox_rep().substring (0,11)%>&nbsp;&nbsp;&nbsp;<%=dp.getFecha_prox_rep().substring (11,16)%></td>
										  </tr>
										  <tr class="letra">
										    <td height="18" colspan=4 >&nbsp;<strong>Ubicaci�n Ultimo Reporte:</strong> <%=model.rmtService.getNombreCiudad(dp.getPto_control_ultreporte().equals("")?dp.getOripla():dp.getPto_control_ultreporte())%></td>
						                    <td height="18" colspan=4 >&nbsp;<strong>Ubicaci�n Proximo Reporte:</strong> <%=model.rmtService.getNombreCiudad(dp.getPto_control_proxreporte())%></td>
							  			</tr>                                                                       
                                    <%}%>
                          </table>
                        </td>
                    </tr>
    </table>
	
 <% 	        
	java.util.Date date = new java.util.Date();
    java.text.SimpleDateFormat s = new java.text.SimpleDateFormat("yyyy-MM-dd HH:mm");
	String fechareporte =s.format(date);	        
	String mensaje = request.getParameter("mensaje");	
	String tiporeporte = request.getParameter("tiporeporte");        
	String tipoubicacion = request.getParameter("tubicacion");        	
	String observaciones = request.getParameter("observacion");
	
 %>  
  <br>    
  <input type="hidden" value="<%=dp.getNumpla()%>" name="numplanilla">
  <input type="hidden" name="mensaje"> 
  <input type="hidden" name="fecha_actu" id="fecha_actu" value="<%=fechareporte%>">
  <input name="numpla" type="hidden" id="numpla4" value="<%=dp.getNumpla()%>">
  <input type="hidden" name="controller" value="<%=CONTROLLER%>">
  <input name="frontera" type="hidden" id="frontera" value="<%=dp.getFrontera()%>"> 
  <input name="origen" type="hidden" id="origen" value="<%=dp.getOripla()%>">
  <table width="850" border="2" align="center">
    <tr>
        <td>
            <table width="100%" align="center"  >
                <tr>            
                    <td width="50%" class="subtitulo1"><div align="center">Ingresar Reporte de Observaci&oacute;n</div></td>
                    <td width="50%" class="barratitulo"><img src="<%=BASEURL%>/images/titulo.gif" width="32" height="20"></td>                    
                </tr>	
                <tr class="fila">
                    <td colspan="2" align="center"><strong>Fecha del Reporte </strong></td>
                </tr>
                <tr class="fila">
                    <td colspan="2" align="center">
                        <input name="c_dia" type="text" class="textbox" id="c_dia" onBlur="llenarObservacion2();" onKeyPress="soloDigitos(event,'decNo');FormatNumber(this,'####-##-##');" value="<%=request.getParameter("c_dia")!=null?request.getParameter("c_dia"):fechareporte.substring (0,10)%>" size="13" maxlength="10">
            			<input name="c_hora" type="text" class="textbox" onBlur="llenarObservacion2();" id="c_hora" onKeyPress="soloDigitos(event,'decNo');FormatNumber(this,'##:##');" value="<%=request.getParameter("c_hora")!=null?request.getParameter("c_hora"):fechareporte.substring (11,16)%>" size="6" maxlength="5">
						&nbsp;<img src="<%=BASEURL%>/images/botones/iconos/obligatorio.gif" width="10" height="10"></img>
                        <input name="tiporep" type="hidden" id="tiporep" value="OBS">
					</td>
                </tr>    
                <tr class="fila">
                    <td align="center" colspan="2"><strong>Procedencia </strong></td>
                </tr>
                <tr class="fila">
                    <td align="center">Tipo De Ubicaci�n</td> 
                    <td align="center">Ubicaci&oacute;n</td>
                </tr>
                <tr class="fila">
                    <td align="center">	
                        <select name='tubicacion' class='textbox' onchange="mensaje.value='listarub';this.form.submit();llenarObservacion2();">
                            <%
                                Vector tu = model.rmtService.getTiposubicacion();
                                for(int i=0;i<tu.size();i++){
                                    Vector tmp = (Vector)(tu.get(i));
                                    String codtu = (String)(tmp.get(0));
                                   %>
									<option value="<%=codtu%>" <%=codtu.equalsIgnoreCase(tipoubicacion)?"selected":""%>><%=(String)(tmp.get(1))%></option>
                               <% }
                            %>  	
                        </select>
                        <img src="<%=BASEURL%>/images/botones/iconos/obligatorio.gif" width="10" height="10"></img>
                    </td>      	
                    <td align="center">
                        <select name="ubicacion" id="ubicacion" class='textbox' onChange="llenarObservacion2();">
                            <%
                                Vector ubicaciones = model.rmtService.getUbicaciones();
                                if (ubicaciones!=null){ 
                                    for(int i=0;i<ubicaciones.size();i++){
                                        Vector tmp = (Vector)(ubicaciones.get(i));%>
                                    <option value="<%=(String)tmp.get(0)%>" <%=nextpc.equals((String)tmp.get(0))?"selected":""%>><%=(String)(tmp.get(1))%></option>
                                    <%}
                                }
                             %>
                        </select>
                        <img src="<%=BASEURL%>/images/botones/iconos/obligatorio.gif" width="10" height="10"></img>
                    </td>
                </tr>
                <tr class="fila">
                    <td colspan="2" align="center">Observaciones</td>
                </tr>
                <tr class="fila">
                  <td align="center">Clasificacion</td>
                  <td align="center"><input:select name="clasificacion" options="<%=model.rmtService.getListaClasificacion()%>" /></td>
                </tr>	
                <tr class="fila">
                    <td colspan='2' align=center>
						<input name="obaut" class="textbox" type="text" id="obaut" size="145" readonly>
                        <textarea class="textbox" name='observacion' cols=145 rows=5  onKeyPress="soloAlfa(this)" ><%if(observaciones!=null)out.print(observaciones);%></textarea>
                    </td>      
                </tr>    
  	        <tr class="fila">
                    <td align=right colspan='2' class='letraResaltada'><a href="javascript: void(0);" onclick="window.open('<%=BASEURL%>/jsp/trafico/Movimiento_trafico/RepVariasPlanillas.jsp?fechar='+ getDate (new Date(), 'yyyy-mm-dd hh:mi') ,'','status=no,scrollbars=yes,width=720,height=400,resizable=yes')">Copiar Reporte a otras planillas</a></td>                    
  	        </tr>
            </table>                         
        </td>
    </tr>
  </table>
  <br>
  <table align=center width='450'>
    <tr>
        <td align="center" colspan="2">
            <img title='Agregar reporte' name="mod" id="mod" src="<%= BASEURL %>/images/botones/aceptar.gif" onmouseover="botonOver(this);" onmouseout="botonOut(this);" style="visibility:visible;cursor:hand" onclick="mensaje.value='observacion';enviaFormulario('<%=BASEURL%>');"></img>&nbsp;            
            <img title='Ver todos los reportes de esta planilla' src="<%= BASEURL %>/images/botones/detalles.gif" onmouseover="botonOver(this);" onmouseout="botonOut(this);" onClick="window.open('<%=CONTROLLER%>?estado=Listar&accion=ReportesMovTraf&pagina=movtraflist.jsp&carpeta=jsp/trafico/Movimiento_trafico&numpla=<%=dp.getNumpla()%>','','status=yes,scrollbars=no,resizable=yes,width=875,height=575')"></img>&nbsp;
            <img title='Salir' src="<%= BASEURL %>/images/botones/salir.gif" onmouseover="botonOver(this);" onmouseout="botonOut(this);" onClick="parent.close()"></img>&nbsp;
			<img src="<%=BASEURL%>/images/botones/regresar.gif" style="cursor:hand" title="Volver" name="buscar"  onClick="window.location='<%=CONTROLLER%>?estado=Buscar&accion=NumPlanilla&pagina=movtrafing.jsp&carpeta=jsp/trafico/Movimiento_trafico&numpla=<%=dp.getNumpla()%>'" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);">
</td>
    </tr>
  </table>
  </form>
  </span>
    <%        
        if(mensaje!=null && !mensaje.equalsIgnoreCase("default") && !mensaje.equalsIgnoreCase("listarub")) {
            out.print("<table width=450 border=2 align=center><tr><td><table width=100%  border=1 align=center  bordercolor=#F7F5F4 bgcolor=#FFFFFF><tr><td width=350 align=center class=mensajes>"+mensaje+"</td><td width=100><img src="+BASEURL+"/images/cuadronaranja.JPG></td></tr></table></td></tr></table>");            
        }
    %>
  </div>      
<%}%>
</div>
<iframe width=188 height=166 name="gToday:normal:agenda.js:gfPop:plugins_24.js" id="gToday:normal:agenda.js:gfPop:plugins_24.js" src="<%=BASEURL%>/js/Calendario/ipopeng.htm" scrolling="no" frameborder="0" style="visibility:visible; z-index:999; position:absolute; left:-500px; top:0px;"> </iframe>
</body>
</html>