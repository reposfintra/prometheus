<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">

<%@page isErrorPage = "true" %>
<html>
<head>
<title>Tratamiento de errores</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
</head>
<body bgcolor="#FFFFFF">
<%if (exception != null) { 
%>
<blockquote>
  <p><strong>La excepci&oacute;n causante del error ha sido:</strong></p>
  <p><% exception.printStackTrace(new java.io.PrintWriter(out)); %></p>
</blockquote>
<% } // if (exception != null) %>
<ul>
  <li><strong>Atributos del objeto Request:</strong> </li>
    <blockquote> 
      <p>
	  	<%
        java.util.Enumeration enum1;
        String atributo;
		enum1 = request.getAttributeNames(); // Leemos todos los atributos del request
        while (enum1.hasMoreElements()) {
	        atributo = (String) enum1.nextElement();
		%>
	        <%=atributo%> = <%=request.getAttribute(atributo)%><br>
		<%
        } // del while
		%>
	  </p>
    </blockquote>
  </li>
  <li><strong>Par&aacute;metros recibidos:</strong> </li>
    <blockquote> 
      <p>
	  	<%
        String parametro;
		enum1 = request.getParameterNames(); // Leemos todos los atributos del request
        while (enum1.hasMoreElements()) {
	        parametro = (String) enum1.nextElement();
		%>
	        <%=parametro%> = <%=request.getParameter(parametro)%><br>
		<%
        } // del while
		%>
	  </p>
    </blockquote>
  </li>
  <li><strong>Sesi&oacute;n:</strong> </li>
    <blockquote> 
      <p>
		
	  </p>
    </blockquote>
  </li>
 </ul>
</body>
</html>

<!-- END OF CONTENTS OF THIS PAGE -->
