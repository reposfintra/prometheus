<%@page session="true"%>
<%@page import="java.util.*" %>
<%@page import="com.tsp.operation.model.*"%>
<%@include file="/WEB-INF/InitModel.jsp"%>
<%@page import="com.tsp.operation.model.beans.*"%>
<%@page import="com.tsp.util.*"%>
<%@ taglib uri="http://jakarta.apache.org/taglibs/input-0.90" prefix="input" %>
<html>
<head>  
<title>Listado Escolta Vehiculos</title> 
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link href="<%=BASEURL%>/css/estilostsp.css" rel="stylesheet" type="text/css">
<link href="../../../../css/estilostsp.css" rel="stylesheet" type="text/css"> 
<script type='text/javascript' src="<%= BASEURL %>/js/boton.js"></script>
 <script type='text/javascript' src="<%=BASEURL%>/js/general.js"></script> 
</head>
<body onResize="redimensionar();" onLoad='redimensionar();' > 

<%	
	String mensaje = (request.getParameter("msg")!= null)? request.getParameter("msg") : "";
	Vector vec = model.escoltaVehiculoSVC.getVectorEscoltaCaravanaActual();
	int n = 0;
	int m = 0;
	int cont = 0;
	boolean sw = false;
	String fac = "_";
	for (int i = 0; i < vec.size(); i++){
		EscoltaVehiculo es = (EscoltaVehiculo) vec.get(i);
		n = 1;
		for (int j = i+1; j < vec.size(); j++){
			EscoltaVehiculo esv = (EscoltaVehiculo) vec.get(j);
			if( !es.getFactura().equals(fac) ){
				if( esv.getFactura().equals(es.getFactura()) ){
					n++;
					sw = true;
				}
			}
		}
		if( !es.getFactura().equals(fac) ){
			sw = true;
			m = n;
		}
		else
			n = m;
		fac = es.getFactura();
		if (sw) {
			sw = false; 
			cont++; 
		}
	}
%>
<div id="capaSuperior" style="position:absolute; width:100%; height:100px; z-index:0; left: 0px; top: 0px;">
 <jsp:include page="/WEB-INF/toptsp.jsp?encabezado=Generación Factura a Escolta"/>
</div>
<div id="capaCentral" style="position:absolute; width:100%; height:87%; z-index:0; left: 0px; top: 100px; overflow: scroll;">
<FORM ACTION="<%=CONTROLLER%>?estado=EscoltaVehiculo&accion=Operacion&opcion=2" METHOD='post' id='form1' name='form1'>
<% if ( request.getAttribute("aprobacion")!=null ){
		if( vec != null && vec.size() > 0 ){
		%>
		<table width="98%" border="1" align="center">
			<tr>
				<td>  
					<table width="100%" align="center">
						  <tr>
							<td width="373" class="subtitulo1">&nbsp;Listado de Escolta Vehiculos </td>
							<td width="427" class="barratitulo"><img src="<%=BASEURL%>/images/titulo.gif" width="32" height="20"></td>
						  </tr>
					</table>                      
					<table width="100%" border="1" bordercolor="#999999" bgcolor="#F7F5F4">
						<tr class="tblTitulo">
							<td width="3%"  align="center"><input type="checkbox" name="checkboxall" id ="checkboxall" value="checkbox" onClick="seleccionartodo('<%=cont%>',this.checked);"></td>                        
							<td width="16%" align="center">Factura</td>
							<td width="8%" align="center">DO</td>
							<td width="13%" align="center">Origen</td>
							<td width="13%" align="center">Destino</td>
							<td width="6%" align="center">Planilla</td>
							<td width="6%" align="center">Placa</td>
							<td width="18%" align="center">Escolta</td>
							<td width="7%" align="center">Tarifa</td>
							<td width="10%" align="center">Fecha</td>
						</tr> 
						<%
						  n = 0;
						  m = 0;
						  cont = 0;
						  sw = false;
						  fac = "_";
						  for (int i = 0; i < vec.size(); i++)
						  {
							 EscoltaVehiculo es = (EscoltaVehiculo) vec.get(i);
							 n = 1;
							 for (int j = i+1; j < vec.size(); j++){
								EscoltaVehiculo esv = (EscoltaVehiculo) vec.get(j);
								if( !es.getFactura().equals(fac) ){
									if( esv.getFactura().equals(es.getFactura()) ){
										n++;
										sw = true;
									}
								}
							 }
							 if( !es.getFactura().equals(fac) ){
								sw = true;
								m = n;
							}
							else
								n = m;
							 fac = es.getFactura();%>
								
								<tr class="filagris" >
									<% if (sw) {%>
										<td align="center" rowspan="<%=n%>" nowrap class="bordereporte"><input type="checkbox" id="checkbox<%=cont%>" name="checkbox" value="<%=es.getFactura()%>"></td>
										<td align="center" rowspan="<%=n%>" nowrap class="bordereporte"><%=es.getFactura()%> </td>
										<% sw = false; cont++; 
									}%>
									<td align="center" nowrap class="bordereporte"><%=es.getD_o()%></td>
									<td align="center" nowrap class="bordereporte"><%=es.getOrigen()%></td>	
									<td align="center" nowrap class="bordereporte"><%=es.getDestino()%></td>
									<td align="center" nowrap class="bordereporte"><%=es.getNumpla()%></td>
									<td align="center" nowrap class="bordereporte"><%=es.getPlaca()%></td>
									<td align="center" nowrap class="bordereporte"><%=es.getEscolta()%></td>
									<td align="center" nowrap class="bordereporte"><%=UtilFinanzas.customFormat(es.getTarifa())%></td>
									<td align="center" nowrap class="bordereporte"><%=es.getCreation_date()%></td>	
								</tr>				
						<%}%>
				  </table>
				</td>
			</tr>
		</table> 
		 <br>
		  <div align="center"><img src="<%=BASEURL%>/images/botones/aceptar.gif" id="imgaceptar" name="imgaceptar" onClick="validarCampos(<%=cont%>); " onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" style="cursor:hand">&nbsp;
		  <img src="<%=BASEURL%>/images/botones/salir.gif"  name="imgsalir"  onMouseOver="botonOver(this);" onClick="parent.close();" onMouseOut="botonOut(this);" style="cursor:hand">
		<% if( !mensaje.equals("") ){ %> 
			</div>
			<br>
			<table border="2" align="center">
				<tr>
					<td><table width="100%"  border="1" align="center"  bordercolor="#F7F5F4" bgcolor="#FFFFFF">
						<tr>
							<td width="229" align="center" class="mensajes"><%=mensaje%></td>
							<td width="29" background="<%=BASEURL%>/images/cuadronaranja.JPG">&nbsp;</td>
							<td width="58">&nbsp;</td>
						</tr>
					</table></td>
				</tr>
			</table>
		 <%}
		 }else{%>
				<br>   
				<table border="2" align="center">
					<tr>
						<td>
							<table width="100%"  border="1" align="center"  bordercolor="#F7F5F4" bgcolor="#FFFFFF">
								<tr> 
									<td width="350" align="center" class="mensajes">Su b&uacute;squeda no arroj&oacute; resultados!</td>
									<td width="29" background="<%=BASEURL%>/images/cuadronaranja.JPG">&nbsp;</td>
									<td width="58">&nbsp;</td>
								</tr>
							</table>
						</td>
					</tr>
				</table>
				<br>
				<img src="<%=BASEURL%>/images/botones/salir.gif"  name="imgsalir"  onMouseOver="botonOver(this);" onClick="parent.close();" onMouseOut="botonOut(this);" style="cursor:hand">                   
		 <%}
	  }else{%>
  <br>
	<table border="2" align="center">
		<tr>
			<td><table width="100%"  border="1" align="center"  bordercolor="#F7F5F4" bgcolor="#FFFFFF">
				<tr>
					<td width="229" align="center" class="mensajes">El usuario no esta permitido para generar este proceso</td>
					<td width="29" background="<%=BASEURL%>/images/cuadronaranja.JPG">&nbsp;</td>
					<td width="58">&nbsp;</td>
				</tr>
			</table></td>
		</tr>
	</table>
	<br>
	<img src="<%=BASEURL%>/images/botones/salir.gif"  name="imgsalir"  onMouseOver="botonOver(this);" onClick="parent.close();" onMouseOut="botonOut(this);" style="cursor:hand">                   
  <%}%>
</form>
</div>
<iframe width=188 height=166 name="gToday:normal:agenda.js:gfPop:plugins.js" id="gToday:normal:agenda.js:gfPop:plugins.js" src="<%=BASEURL%>/js/Calendario/ipopeng.htm" scrolling="no" frameborder="0" style="visibility:visible; z-index:999; position:absolute; left:-500px; top:0px;"> </iframe>
</body>
</html>
<script>
	function seleccionartodo(size,check){
		if(check == true){
			for (i=0;i<size;i++){
				var checkbox="checkbox"+i;
				var textfield="textfield"+i
				document.getElementById(checkbox).checked=true;
				
			}    
		}else{
			for (i=0;i<size;i++){
				var checkbox="checkbox"+i;
				var textfield="textfield"+i
				document.getElementById(checkbox).checked=false;
				
			}  
		}
	}
	function validarCampos( tam ){
		var num = "";
		var estaDisabled = false;
		for( var i=0; i < tam ; i++ ){
			var checkbox="checkbox"+i;
			if(document.getElementById(checkbox).checked){
				num+= document.getElementById(checkbox).value;
			}
		}
		if( num == "" ){
			alert("Debe seleccionar por lo menos una factura");
		}else{
			form1.imgaceptar.disabled=true;
			form1.imgaceptar.src = "<%=BASEURL%>/images/botones/aceptarDisable.gif";
			form1.submit();
		}    
	}     
</script>