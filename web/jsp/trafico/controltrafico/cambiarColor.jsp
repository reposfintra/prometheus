<%@page contentType="text/html"%>
<%@page session="true"%>
<%@page errorPage="../error/ErrorPage.jsp"%>
<%@page import="java.util.*"%>
<%@page import="com.tsp.operation.model.*"%>
<%@page import="com.tsp.util.*"%>
<%@page import="com.tsp.operation.model.beans.*, java.util.*"%>
<%@include file="/WEB-INF/InitModel.jsp"%>

<html>
<head>
<title>Cambiar Color</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link href="<%= BASEURL %>/css/estilostsp.css" rel="stylesheet" type="text/css"> 
<link href="../../../css/estilostsp.css" rel="stylesheet" type="text/css"> 
<script type='text/javascript' src="<%= BASEURL %>/js/boton.js"></script>
<script language=JavaScript src="<%= BASEURL %>/js/picker.js"></script>

</head>

<body>
<div id="capaSuperior" style="position:absolute; width:100%; height:100px; z-index:0; left: 0px; top: 0px;">
<jsp:include page="/WEB-INF/toptsp.jsp?encabezado=Cambiar De Color Planilla"/>
</div>
<div id="capaCentral" style="position:absolute; width:100%; height:85%; z-index:0; left: 0px; top: 100px; overflow: scroll;"> 
<% 	Planilla pla = model.planillaService.getPlanilla(); 
	Usuario usuario = (Usuario) session.getAttribute("Usuario");
	if(pla!=null){
%>

<form name="forma" method="post" action="<%=CONTROLLER%>?estado=CambioColor&accion=Trafico&tipo=insert">
    <table width="50%"  border="2" align="center">
      <tr>
        <td>
          <table width="100%" class="tablaInferior">
            <tr>
              <td colspan="2"><table width="100%"  border="0" cellspacing="0" cellpadding="0">
                  <tr>
                    <td width="46%" class="subtitulo1">PLANILLA Nro. <%= pla.getNumpla() %>
                        <input type="hidden" name="planilla" value="<%= pla.getNumpla() %>"></td>
                    <td width="54%" class="barratitulo"><img src="<%=BASEURL%>/images/titulo.gif"></td>
                  </tr>
              </table></td>
            </tr>
            <tr class="fila">
              <td width="27%">Origen:</td>
              <td width="73%"><%= pla.getNomori() %></td>
            </tr>
            <tr class="fila">
              <td >Destino:</td>
              <td><%= pla.getNomdest() %></td>
            </tr>
            <tr class="fila">
              <td >Placa:</td>
              <td><%= pla.getPlaveh() %></td>
            </tr>
            <tr class="fila">
              <td >Conductor:</td>
              <td><%= pla.getNomCond() %></td>
            </tr>
            <tr class="fila">
              <td >Zona:</td>
              <td><%= pla.getNomzona() %>
                  <input name="azona" type="hidden" id="azona" value="<%= pla.getZona() %>"></td>
            </tr>
            <tr>
              <td colspan="2" class="subtitulo1">Cambio de Color en la letra </td>
            </tr>
            <tr>
              <td colspan="2" class="informacion">Seleccione el color que desea colocarle a la planilla </td>
            </tr>
            <tr class="fila">
              <td >Color</td>
              <td><input name="color" type="Text" id="color" value="#003399" readonly>
                <a href="javascript:TCP.popup(document.forms['forma'].elements['color'],0,'<%=BASEURL%>')"><img width="15" height="13" border="0" alt="Clic para seleccionar un color" src="<%=BASEURL%>/images/sel.gif"></a> </td>
            </tr>
        </table></td>
      </tr>
    </table>
    <br>
    <table align=center>
        <tr>
            <td colspan="2" valign="middle" ><div align="center">
                <img title='Buscar' src="<%= BASEURL %>/images/botones/aceptar.gif" onmouseover="botonOver(this);" onmouseout="botonOut(this);" onclick='forma.submit();'></img>                </img>
                <img title='Salir' src="<%= BASEURL %>/images/botones/salir.gif" onmouseover="botonOver(this);" onmouseout="botonOut(this);" onclick='parent.close();'></img>
            </div></td>
        </tr>        
    </table>
</form>
<%}
String msg = "";//out.println(request.getParameter("mensaje"));
if (request.getParameter("mensaje")!=null){
	msg=request.getParameter("mensaje"); 
	//out.println(msg);%>
	<br>
<table border="2" align="center">
  <tr>
    <td>
	<table width="100%"  border="1" align="center"  bordercolor="#F7F5F4" bgcolor="#FFFFFF">
      <tr>
        <td width="229" align="center" class="mensajes"><%= msg %></td>
        <td width="29"><img src="<%=BASEURL%>/images/cuadronaranja.JPG"></td>
      </tr>
    </table></td>
  </tr>
</table>
<br>
<% } %>


</div>
</body>
</html>
