<!--  
     - Author(s)       :      Karen Reales
     - Date            :      06/05/2006
     - Copyright Notice:      TRANSPORTES SANCHEZ POLO S.A.
-->
<%--   
     - @(#)  
     - Description: Ayuda
--%> 
<%@include file="/WEB-INF/InitModel.jsp"%>


<html>
    <head>
        <title>.: Descripción del programa de trafico :.</title>
        <link href="<%=BASEURL%>/css/estilostsp.css" rel="stylesheet" type="text/css">
    </head>
    <body>
        <BR>
        <table width="98%"  border="2" align="center">
            <tr>
            <td width="100%" valign="top" >
                <table width="100%" border="0" align="center">
                    <tr  class="subtitulo">
                        <td height="24" align="center">
                        TRAFICO </td>
                    </tr>
                    <tr class="subtitulo1">
                        <td> PANTALLA TAMA&Ntilde;OS</td>
                    </tr>
                    <tr class="ayudaHtmlTexto">
                      <td><div align="left">
                        <p align="center"><strong><img src="../../../images/ayuda/trafico/tamano/1.png"></strong></p>
                        <p>A continuaci&oacute;n aparece la siguiente pantalla: </p>
                        <p>Nota: Al cerrar el programa e iniciarlo otra vez queda guardada la &uacute;ltima configuraci&oacute;n. </p>
                        <p align="center"><img src="../../../images/ayuda/trafico/tamano/2.png"></p>
                        <p>Luego de modificar el ancho de las columnas: </p>
                        <p align="center"><img src="../../../images/ayuda/trafico/tamano/3.png"></p>
                        <p>Finalmente, el reporte que da as&iacute;: </p>
                        <p align="center"><img src="../../../images/ayuda/trafico/tamano/4.png"></p>
                      </div>                        
                      </td>
                    </tr>
                </table>
            </td>
            </tr>
    </table>
        <p align="center" class="fuenteAyuda"> Fintravalores S. A. </p>
    </body>

</html>