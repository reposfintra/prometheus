<!--  
     - Author(s)       :      Karen Reales
     - Date            :      06/05/2006
     - Copyright Notice:      TRANSPORTES SANCHEZ POLO S.A.
-->
<%--   
     - @(#)  
     - Description: Ayuda
--%> 
<%@include file="/WEB-INF/InitModel.jsp"%>


<html>
    <head>
        <title>.: Descripción del programa de trafico :.</title>
        <link href="<%=BASEURL%>/css/estilostsp.css" rel="stylesheet" type="text/css">
    </head>
    <body>
        <BR>
        <table width="98%"  border="2" align="center">
            <tr>
            <td width="100%" valign="top" >
                <table width="100%" border="0" align="center">
                    <tr  class="subtitulo">
                        <td height="24" align="center">
                        TRAFICO </td>
                    </tr>
                    <tr class="subtitulo1">
                        <td> PANTALLA CONFIGURACION</td>
                    </tr>
                    <tr class="ayudaHtmlTexto">
                      <td><div align="left">
                        <p align="center">&nbsp;</p>
                        <p align="center"><img src="../../../images/ayuda/trafico/configuracion/1.png"></p>
                        <p>A continuaci&oacute;n aparece la siguiente pantalla: <br>
                        Nota: Al cerrar el programa e iniciarlo otra vez queda guardada la &uacute;ltima configuraci&oacute;n. </p>
                        <p align="center"><img src="../../../images/ayuda/trafico/configuracion/2.png"></p>
                        <p>&nbsp;</p>
                        <p>Para seleccionar las columnas que se quieren ver en el informe: </p>
                        <p align="center"><img src="../../../images/ayuda/trafico/configuracion/3.png"></p>
                        <p>&nbsp;</p>
                        <p>Luego de pasar los campos que se desean a columnas visibles: </p>
                        <p>&nbsp;</p>
                        <p align="center"><img src="../../../images/ayuda/trafico/configuracion/4.png"></p>
                        <p>&nbsp;</p>
                        <p>Si lo que se desea es guardar la configuraci&oacute;n: </p>
                        <p align="center"><img src="../../../images/ayuda/trafico/configuracion/5.png"></p>
                        <br>
                        <p>&nbsp;</p>
                        <p>Seguidamente: </p>
                        <p align="center"><img src="../../../images/ayuda/trafico/configuracion/6.png"></p>
                        <p>El reporte aparece de la siguiente manera: </p>
                        <p align="center"><img src="../../../images/ayuda/trafico/configuracion/7.png"></p>
                        <br>
                        <p>Para ver las configuraciones aplicadas: </p>
                        <p>&nbsp;</p>
                        <p align="center"><img src="../../../images/ayuda/trafico/configuracion/7b.png"></p>
                        <br>
                        <p>Finalmente, aparece un mensaje as&iacute;: </p>
                        <p align="center"><img src="../../../images/ayuda/trafico/configuracion/8.png"></p>
</div>                        
                      </td>
                    </tr>
                </table>
            </td>
            </tr>
    </table>
        <p align="center" class="fuenteAyuda"> Fintravalores S. A. </p>
    </body>

</html>