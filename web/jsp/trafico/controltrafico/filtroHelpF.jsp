<!--  
     - Author(s)       :      Karen Reales
     - Date            :      06/05/2006
     - Copyright Notice:      TRANSPORTES SANCHEZ POLO S.A.
-->
<%--   
     - @(#)  
     - Description: Ayuda
--%> 
<%@include file="/WEB-INF/InitModel.jsp"%>


<html>
    <head>
        <title>.: Descripción del programa de trafico :.</title>
        <link href="<%=BASEURL%>/css/estilostsp.css" rel="stylesheet" type="text/css">
    </head>
    <body>
        <BR>
        <table width="98%"  border="2" align="center">
            <tr>
            <td width="100%" valign="top" >
                <table width="100%" border="0" align="center">
                    <tr  class="subtitulo">
                        <td height="24" align="center">
                        TRAFICO </td>
                    </tr>
                    <tr class="subtitulo1">
                        <td> PANTALLA FILTROS </td>
                    </tr>
                    <tr class="ayudaHtmlTexto">
                      <td><div align="left">
                        <p align="center">&nbsp;</p>
                        <p><img src="../../../images/ayuda/trafico/filtro/1.PNG"></p>
                        <p>A continuaci&oacute;n aparece la siguiente pantalla: </p>
                        <p>Nota: Al cerrar el programa e iniciarlo otra vez queda guardada la &uacute;ltima configuraci&oacute;n. </p>
                        <p><img src="../../../images/ayuda/trafico/filtro/2.PNG"></p>
                        <p>Luego, al hacer clic en cualquiera de las opciones del cuadro Campos, el cuadro Datos muestra las opciones que se tienen. </p>
                        <p>&nbsp;</p>
                        <p><img src="../../../images/ayuda/trafico/filtro/3.PNG"></p>
                        <p>&nbsp;</p>
                        <p>Luego de agregar el filtro deseado aparece la siguiente pantalla. Para ver el informe con el filtro aplicado: </p>
                        <p>&nbsp;</p>
                        <img src="../../../images/ayuda/trafico/filtro/4.PNG"><br>
                        <p>&nbsp;</p>
                        <p>Si lo que se desea es guardar el filtro: </p>
                        <p><img src="../../../images/ayuda/trafico/filtro/6.png"></p>
                        <p>Guarado el filtro, aparece en la ficha consultas: </p>
                        <p><img src="../../../images/ayuda/trafico/filtro/7.png"></p>
                        <p>&nbsp;</p>
                        <p>Finalmente aparecer&aacute; la pantalla con las columnas deseadas: </p>
                        <p><img src="../../../images/ayuda/trafico/filtro/8.png"></p>
                        <p>&nbsp;</p>
                        <p>Finalmente, aparece la siguiente pantalla: </p>
                        <p><img src="../../../images/ayuda/trafico/filtro/9.PNG" width="970" height="572"></p>
                        <p align="center">&nbsp;</p>
                        </div>                        
                      </td>
                    </tr>
                </table>
            </td>
            </tr>
    </table>
        <p align="center" class="fuenteAyuda"> Fintravalores S. A. </p>
    </body>

</html>