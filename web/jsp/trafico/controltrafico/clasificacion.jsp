<%@page contentType="text/html"%>
<%@page session="true"%>
<%@page import="java.util.*"%>
<%@page import="com.tsp.operation.model.*"%>
<%@page import="com.tsp.operation.model.beans.*"%>
<%@page import="com.tsp.util.*"%>
<%@include file="/WEB-INF/InitModel.jsp"%>
<%String path = application.getRealPath(model.menuService.getRealUrl(request.getRequestURI()));
  String datos[] = model.menuService.getContenidoMenu(BASEURL,request.getRequestURI(),path);
%>

<html>
<head>
   <title>Clasificaci�n</title>
   <!--link href="<%= BASEURL %>/css/estilo.css" rel='stylesheet'>
<link href="../css/estilo.css" rel="stylesheet" type="text/css"-->
<link href="<%= BASEURL %>/css/estilostsp.css" rel="stylesheet" type="text/css">
<link href="../../../css/estilostsp.css" rel="stylesheet" type="text/css">
   <%
     String linea        = request.getParameter ("linea");
     //out.println("<br> Linea: " +linea);
     String conf        = request.getParameter ("conf");
    // out.println("<br> Configuracion " +conf);
     String filtro        = request.getParameter ("filtro");
   //  out.println("<br> Filtro: " +filtro);
     String filtroP        = request.getParameter ("filtroP");
   //  out.println("<br> FiltroP: " +filtroP);
      String var1        = request.getParameter ("var1");
   //  out.println("<br> Filtro: " +filtro);
       String var2        = request.getParameter ("var2");
       String var3        = request.getParameter ("var3");
   //  out.println("<br> Filtro: " +filtro);
     
     Usuario usuarioLogin  = (Usuario) session.getAttribute("Usuario");
     String usuLogin  = usuarioLogin.getLogin();
     
     List campos = model.traficoService.searchCampos();
   //  out.println("<br>"+model.traficoService.getVarJSCampo()); 
   %>
    <title>Asignavion de Usuarios a Perfiles</title>
    <style>
        tr.select   { background-color: teal;  color:white; font-size:10px; font-family:Verdana}
        tr.unselect { background-color: white; color:black; font-size:10px; font-family:Verdana}
    </style>
    <script src='<%= BASEURL %>/js/Validaciones.js'></script>
	<script type='text/javascript' src="<%= BASEURL %>/js/boton.js"></script>
    <script>
        <%-- Added by Andr�s Maturana --%>
        <%= model.traficoService.jsCampos(var1+var2) %>
        <%= model.traficoService.jsClasificPrevia(request.getParameter("clas")) %>
        var SeparadorJS = '~';
        
        var viewByPerfil = '';
        var viewByUser   = '';
    
        function _onmousemove(item) { item.className='select';   }
        function _onmouseout (item) { item.className='unselect'; }
        
        
        function loadCombo(datos, cmb){
            cmb.length = 0;
			for (i=0;i<datos.length;i++){
                var dat = datos[i].split(SeparadorJS);
				addOption(cmb, dat[0], dat[1]);
            }
        }
        
        function loadCombo2(datos, datosA ,cmbA, cmbNA){
            cmbA.length = 0;
            cmbNA.length = 0;
            for (i=0;i<datos.length;i++){
                var dat = datos[i].split(SeparadorJS);                
                if (datosA.indexOf(dat[0])==-1)
                    addOption(cmbNA, dat[0], dat[1]);
                else
                    addOption(cmbA , dat[0], dat[1]);
            }
        }   

        function selectAll(cmb){
            for (i=0;i<cmb.length;i++)
              cmb[i].selected = true;
        }
          
        function validarFormulario (){
            if( formularioNuevo.camposS.length==0 ){
               alert('Debe Seleccionar seleccionar un Campo');               
            }
			else {
	            selectAll (formularioNuevo.camposS );
				formularioNuevo.submit();
			}
        }
        
        function SelAll(){
            for(i=0;i<FormularioListado.length;i++)
               FormularioListado.elements[i].checked=FormularioListado.All.checked;
        }
        
        function ActAll(){
            FormularioListado.All.checked = true;
            for(i=0;i<FormularioListado.length;i++)	
              if (FormularioListado.elements[i].type=='checkbox' && !FormularioListado.elements[i].checked && !FormularioListado.elements[i].name!='All'){
                  FormularioListado.All.checked = false;
                  break;
              }
        } 
        
        function validarListado(form){
            for(i=0;i<FormularioListado.length;i++)	
                if (FormularioListado.elements[i].type=='checkbox' && FormularioListado.elements[i].checked && !FormularioListado.elements[i].name!='All')
                    return true;
            alert('Por favor seleccione un item para poder continuar');
            return false;
        }   
   
        function objetoOpcion(id, nombre, parametros, tipo, ayuda, usuarios, grupos){
            this.id         = id;
            this.nombre     = nombre;
            this.parametros = parametros;
            this.tipo       = tipo;
            this.ayuda      = ayuda;
            this.usuarios   = usuarios;
            this.grupos     = grupos;
            return this;
        }
       

        <%= model.traficoService.getVarJSSeparador() %>
        <%= model.traficoService.getVarJSCampo()    %>
     
        
        
       function clear(combo){
           for (k=combo.length-1; k>=0 ;k--)
               combo.remove(k);
        }
		
		
        function move(cmbO, cmbD){
           for (i=cmbO.length-1; i>=0 ;i--)
            if (cmbO[i].selected){
               addOption(cmbD, cmbO[i].value, cmbO[i].text)
               cmbO.remove(i);
            }
           //order(cmbD);
           deleteRepeat(cmbD);
        }
        
        function moveAll(cmbO, cmbD){
           for (i=cmbO.length-1; i>=0 ;i--){
             if(cmbO[i].value!='' && cmbO[i].text!=''){
               addOption(cmbD, cmbO[i].value, cmbO[i].text)
               cmbO.remove(i);
             }
           }
           order(cmbD);
           deleteRepeat(cmbD);
        }
        
        function addOption(Comb,valor,texto){
           if(valor!='' && texto!=''){
                var Ele = document.createElement("OPTION");
                Ele.value=valor;
                Ele.text=texto;
                Comb.add(Ele);
          }
        }    
            
        function order(cmb){
           for (i=0; i<cmb.length;i++)
             for (j=i+1; j<cmb.length;j++)
                if (cmb[i].text > cmb[j].text){
                    var temp = document.createElement("OPTION");
                    temp.value = cmb[i].value;
                    temp.text  = cmb[i].text;
                    cmb[i].value = cmb[j].value;
                    cmb[i].text  = cmb[j].text;
                    cmb[j].value = temp.value;
                    cmb[j].text  = temp.text;
                }
        }
        
        function deleteRepeat(cmb){
          var ant='';
          for (i=0;i<cmb.length;i++){
             if(ant== cmb[i].value)
                cmb.remove(i);
             ant = cmb[i].value;
          }
        }
        
        <%-- Added by Andr�s Maturana --%>
        function cargarUltimaConfiguracion(){
            loadCombo(jsClasif, document.formularioNuevo.camposS);
            loadCombo(jsCampos, document.formularioNuevo.camposE);
            unloadCombo(jsClasif, document.formularioNuevo.camposE);
        }
        
        function unloadCombo(datos, cmb){
            for(j=0; j<cmb.length; j++){
                for (i=0;i<datos.length;i++){
                        var dat = datos[i].split(SeparadorJS);
                        if( dat[0] == cmb[j].value ){
                                cmb.remove(j);
                                j--;
                        }
                        //addOption(cmb, dat[0], dat[1]);
                }
            }
            order(cmb);
        }
        
    </script>
</head>
<body onresize="redimensionar()" onload = "redimensionar(); loadCombo(CamposJS, formularioNuevo.camposE);<%if (request.getParameter("clas").length()!=0 ) {%>cargarUltimaConfiguracion();<%}%>">
<div id="capaSuperior" style="position:absolute; width:100%; height:100px; z-index:0; left: 0px; top: 0px;">
 <jsp:include page="/toptsp.jsp?encabezado=Clasificaci�n"/>
</div>
<div id="capaCentral" style="position:absolute; width:100%; height:87%; z-index:0; left: 0px; top: 100px; overflow: scroll;">

<center>
<FORM METHOD='POST' NAME='formularioNuevo'>
    <table width="61%" border="2" align="center">
    <tr>
        <td>
          <table width="100%">
            <tr>
              <td width="40%" colspan='8' class="subtitulo1">Clasificaciones</td>
              <td class="barratitulo" align="left"><img src="<%=BASEURL%>/images/titulo.gif" align="left"><%=datos[0]%></td>
            </tr>
          </table>
          <TABLE width='100%' order='4'  align='center' bordercolor='#F7F5F4' bgcolor="#FFFFFF">
          <TR class='fila'><TD align='center'>
              <!-- Grupos Asignados   -->
			  <br>
			  <TABLE width='95%' height="318" border='0' cellpadding='0' cellspacing='0'>
              <TR >
                  <TH width='45%' class="subtitulo1">Campos</TH>
                  <TH width='10%' >&nbsp;                </TH>
                  <TH width='45%' class="subtitulo1">Campos a Ordenar </TH>      
              </TR>              
              <TR>
              <TD height="249" valign="top"><select multiple size='15' class='textbox' style='width:100%' name='camposE' ></select></TD>
              <TD>
			    <img src='<%=BASEURL%>/images/botones/envIzq.gif' style='cursor:hand' title='Pasar a Columnas Disponibles' name='imgEnvIzq'  onclick='move   (camposS, camposE ); ' onMouseOver='botonOver(this);' onMouseOut='botonOut(this);'><br>
                <img src='<%=BASEURL%>/images/botones/enviarIzq.gif' style='cursor:hand' title='Pasar TODO a Columnas Disponibles' name='imgTEnvIzq'  onclick='moveAll(camposS, camposE ) ;' onMouseOver='botonOver(this);' onMouseOut='botonOut(this);'><br>
				<img src='<%=BASEURL%>/images/botones/envDer.gif' style='cursor:hand' title='Pasar a Columnas Visibles' name='imgEnvDer'  onclick='move   (camposE,  camposS );' onMouseOver='botonOver(this);' onMouseOut='botonOut(this);'><br>
                <img src='<%=BASEURL%>/images/botones/enviarDerecha.gif' style='cursor:hand' title='Pasar TODO a Columnas Visibles' name='imgTEnvDer'  onclick='moveAll(camposE,  camposS );' onMouseOver='botonOver(this);' onMouseOut='botonOut(this);'><br>
                
              </TD>
             
              <TD valign="top"><select multiple size='15' class='textbox' style='width:100%' name='camposS' ></select></TD>
              </TR>
			  <tr class="fila">
			    <td colspan = "3"><div align="center">ORDENACION 
			          <TABLE width="54%" align="center" class="fila">
                        <TR>
                          <TD width="8%"><input type="radio" name="orden" value="ASC" <%= model.traficoService.isClasifCAtual_Asc()? "checked" : ""%>></Td>
                          <TD width="37%">Ascendente</TD>
                          <TD width="7%"><input type="radio" name="orden" value="DESC" <%= !model.traficoService.isClasifCAtual_Asc()? "checked" : ""%>></Td>
                          <TD width="48%">Descendente</TD>
                      </table>
			      </div></td>
			  </tr>
              </TABLE>
              <!-- Fin Grupos no Asignados   -->
   
             <input type='hidden' name='Id' value=''>
             <input type="hidden" name ="linea" value="<%=linea%>">
             <input type="hidden" name ="conf" value="<%=conf%>">
             <input type="hidden" name ="var1" value="<%=var1%>">
             <input type="hidden" name ="var2" value="<%=var2%>">
             <input type="hidden" name ="var3" value="<%=var3%>">
             <input type="hidden" name ="filtro" value="<%=filtro%>">
             <input type="hidden" name ="filtroP" value="<%=filtroP%>">
             <input type='hidden' name='usuario' value='<%=usuLogin%>'><br>
			 <img src='<%=BASEURL%>/images/botones/aceptar.gif' style='cursor:hand' title='Aplicar configuracion' name='imgAceptar'  onclick="formularioNuevo.action='<%= CONTROLLER %>?estado=Trafico&accion=Clasificacion'; validarFormulario();" onMouseOver='botonOver(this);' onMouseOut='botonOut(this);'>
              <img src="<%=BASEURL%>/images/botones/salir.gif"  name="salir" title="Salir..." onClick="window.close();" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);"><br>&nbsp;
          </TD></TR>        
      </TABLE>      </td>
  </tr>
</table> 

    </FORM>
    <br><br>
	</div>
	<%=datos[1]%>
</body>
</html>

