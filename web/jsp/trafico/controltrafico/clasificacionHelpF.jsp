<!--  
     - Author(s)       :      Karen Reales
     - Date            :      06/05/2006
     - Copyright Notice:      TRANSPORTES SANCHEZ POLO S.A.
-->
<%--   
     - @(#)  
     - Description: Ayuda
--%> 
<%@include file="/WEB-INF/InitModel.jsp"%>


<html>
    <head>
        <title>.: Descripción del programa de trafico :.</title>
        <link href="<%=BASEURL%>/css/estilostsp.css" rel="stylesheet" type="text/css">
    </head>
    <body>
        <BR>
        <table width="98%"  border="2" align="center">
            <tr>
            <td width="100%" valign="top" >
                <table width="100%" border="0" align="center">
                    <tr  class="subtitulo">
                        <td height="24" align="center">
                        TRAFICO </td>
                    </tr>
                    <tr class="subtitulo1">
                        <td> PANTALLA CLASIFICACION </td>
                    </tr>
                    <tr class="ayudaHtmlTexto">
                      <td><div align="left">A continuaci&oacute;n aparece la siguiente pantalla: </div>                        
                        <p align="center"><br>
                        <img src="../../../images/ayuda/trafico/clasificacion/1.png"><strong></strong></p>
                        <p >&nbsp;</p>
                        <p>Nota: Al cerrar el programa e iniciarlo otra vez queda guardada la &uacute;ltima configuraci&oacute;n. </p>
                        <p align="center"><img src="../../../images/ayuda/trafico/clasificacion/2.png"></p>
                        <p>&nbsp;</p>
                        <p>Seleccionados los campos a ordenar, se decide si se quiere ordenar de manera ascendente o descendente: </p>
                        <p align="center"><img src="../../../images/ayuda/trafico/clasificacion/3.png"></p>
                        <p>&nbsp;</p>
                        <p>Finalmente, </p>
                        <p align="center"><img src="../../../images/ayuda/trafico/clasificacion/5.png"></p>
                        <p align="center">&nbsp;  </p></td>
                    </tr>
                </table>
            </td>
            </tr>
    </table>
        <p align="center" class="fuenteAyuda"> Fintravalores S. A. </p>
    </body>

</html>