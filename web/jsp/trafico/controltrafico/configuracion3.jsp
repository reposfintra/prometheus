<%@page contentType="text/html"%>
<%@page session="true"%>
<%@page import="java.util.*"%>
<%@page import="com.tsp.operation.model.*"%>
<%@page import="com.tsp.operation.model.beans.*"%>
<%@page import="com.tsp.util.*"%>
<%@include file="/WEB-INF/InitModel.jsp"%>
<%@taglib uri="http://jakarta.apache.org/taglibs/input-0.90" prefix="input" %>
<%String path = application.getRealPath(model.menuService.getRealUrl(request.getRequestURI()));
  String datos[] = model.menuService.getContenidoMenu(BASEURL,request.getRequestURI(),path);
%>

<html>
<head>
   <title>Configuración de Pantalla</title>

<style type="text/css">

body {
	background-color: #FFFFFF;
}
</style>
<link href="<%= BASEURL %>/css/estilostsp.css" rel="stylesheet" type="text/css">
<link href="../../../css/estilostsp.css" rel="stylesheet" type="text/css">
   <%
    String filtro       = request.getParameter ("filtro");
    String filtroP       = request.getParameter ("filtroP");
    //System.out.println("................. FiltroP Config ="+filtroP);
    String linea = request.getParameter("linea");
    
     System.out.println("filtro--->  "+filtro);
     System.out.println("filtroP--->  "+filtroP);
     System.out.println("linea--->  "+linea);
    
    linea=linea.replaceAll("-tipo_despacho","");
    linea=linea.replaceAll("-planilla","");
    linea=linea.replaceAll("-placa","");
    linea=linea.replaceAll("-ult_observacion","");//AMATURANA 11.08.2006
	
    Usuario usuarioLogin  = (Usuario) session.getAttribute("Usuario");
    
	String usuLogin  = usuarioLogin.getLogin();
    TreeMap tConf ;
	model.traficoService.tConfiguracionesPorUsuario (usuLogin);
    tConf = model.traficoService.getTConf ();
		
	String clas       = request.getParameter ("clas");
    //out.println("Clasificacion ="+clas+"<br>");
    String var1        = request.getParameter ("var1");
    //out.println("<br> var1: " +var1);
    String var2        = request.getParameter ("var2");
    //out.println("<br> var2: " +var2);
    String var3        = request.getParameter ("var3");
    //out.println("<br>var3"+var3);
    String []c1     = request.getParameterValues ("camposE");
    String []c2     = request.getParameterValues ("camposS");
    String campo1      = ""+request.getParameter ("camposE");
    //out.println("<br>campo1"+campo1+"<br>");
    String campo2      = ""+request.getParameter ("camposS");
    //out.println("<br>campo2"+campo2+"<br>");
    String combo1="";
    String combo2="";
    String operacion   = ""+ request.getParameter ("operacion");
    //out.println("<br>operacion"+operacion+"<br>");
    Vector v1 =new Vector();
    Vector v2 =new Vector();
    String select="";
    if(operacion.equals("null")){
        var1=var1.replaceAll("-tipo_despacho","");
         var1=var1.replaceAll("-planilla","");
		 var1=var1.replaceAll("-placa","");
	    var1=var1.replaceAll("-ult_observacion","");//AMATURANA 11.08.2006
        v1 = model.traficoService.obtenerCampos(var1);
        for (int i=0;i<v1.size();i++){
            if (i==0){
                select="SELECTED";
            }
            String nombre= model.traficoService.nomAtributo(""+v1.elementAt(i));
            combo1=combo1+"<option value='"+v1.elementAt(i)+"' "+select+">"+nombre+"</option>";
            select="";
        }
        var2=var2.replaceAll("-tipo_despacho","");
        var2=var2.replaceAll("-planilla","");
        var2=var2.replaceAll("-placa","");	
	    var2=var2.replaceAll("-ult_observacion","");//AMATURANA 11.08.2006
        /*v2 = model.traficoService.obtenerCampos(var2);*/        
        v2 = model.traficoService.obtenerCampos(linea);
        for (int i=0;i<v2.size();i++){
            if (i==0){
                select="SELECTED";
            }
            String nombre= model.traficoService.nomAtributo(""+v2.elementAt(i));
            combo2=combo2+"<option value='"+v2.elementAt(i)+"' "+select+">"+nombre+"</option>";
            select="";
        }
    } 
    
    if (operacion.equals("enviar")){
        v1 = model.traficoService.obtenerCampos(var1);
        var1="";
        for (int i=0;i<v1.size();i++){
                if (i==0){
                    select="SELECTED";
                }
                if(!(campo1.equals(""+v1.elementAt(i)))){
                    String nombre= model.traficoService.nomAtributo(""+v1.elementAt(i));
                    combo1=combo1+"<option value='"+v1.elementAt(i)+"' "+select+">"+nombre+"</option>";
                    var1=var1+"-"+v1.elementAt(i);
                }
                select="";
        }
        
        var2=var2+"-"+campo1;
        v2 = model.traficoService.obtenerCampos(var2);
        for (int i=0;i<v2.size();i++){
            if (i==0){
                select="SELECTED";
            }
            String nombre= model.traficoService.nomAtributo(""+v2.elementAt(i));
            combo2=combo2+"<option value='"+v2.elementAt(i)+"' "+select+">"+nombre+"</option>";
            select="";
        }
   }
   
   if (operacion.equals("enviarTodo")){
        var2=var1+var2;
        var1="";
        v2 = model.traficoService.obtenerCampos(var2);
        for (int i=0;i<v2.size();i++){
            if (i==0){
                select="SELECTED";
            }
            String nombre= model.traficoService.nomAtributo(""+v2.elementAt(i));
            combo2=combo2+"<option value='"+v2.elementAt(i)+"' "+select+">"+nombre+"</option>";
            select="";
        }
   }
   
   if (operacion.equals("regresarTodo")){
        var1=var2+var1;
        var2="";
        v1 = model.traficoService.obtenerCampos(var1);
        for (int i=0;i<v1.size();i++){
            if (i==0){
                select="SELECTED";
            }
            String nombre= model.traficoService.nomAtributo(""+v1.elementAt(i));
            combo1=combo1+"<option value='"+v1.elementAt(i)+"' "+select+">"+nombre+"</option>";
            select="";
        }
   }
    
   if(operacion.equals("regresar")){
        var1=var1+"-"+campo2;
        v1 = model.traficoService.obtenerCampos(var1);
       
        for (int i=0;i<v1.size();i++){
            if (i==0){
                select="SELECTED";
            }
            String nombre= model.traficoService.nomAtributo(""+v1.elementAt(i));
            combo1=combo1+"<option value='"+v1.elementAt(i)+"' "+select+">"+nombre+"</option>";
            select="";
        }
        
        v2 = model.traficoService.obtenerCampos(var2);
        var2="";
        for (int i=0;i<v2.size();i++){
            if (i==0){
                select="SELECTED";
            }   
            if(!(campo2.equals(""+v2.elementAt(i)))){
                String nombre= model.traficoService.nomAtributo(""+v2.elementAt(i));
                combo2=combo2+"<option value='"+v2.elementAt(i)+"' "+select+">"+nombre+"</option>";
                var2=var2+"-"+v2.elementAt(i);
            }
            select="";
        }
   }
   //System.out.println("..................... OPERACION : " + operacion);
   //System.out.println("..................... COMBO 1 : " + combo1);
   //System.out.println("..................... COMBO 2 : " + combo2);
     List campos = model.traficoService.searchCampos();
     //out.println("<br>"+model.traficoService.getVarJSCampo()); 
   %>
    <title>Asignavion de Usuarios a Perfiles</title>
    <style>
        tr.select   { background-color: teal;  color:white; font-size:10px; font-family:Verdana}
        tr.unselect { background-color: white; color:black; font-size:10px; font-family:Verdana}
    </style>
    <script src='<%= BASEURL %>/js/Validaciones.js'></script>
	<script type='text/javascript' src="<%= BASEURL %>/js/boton.js"></script>
    <script>
        
        <%-- Added by Andrés Maturana --%>
        <%= model.traficoService.jsCamposConfig(usuarioLogin.getLogin()) %>
        <%= model.traficoService.jsCampos(var1+var2) %>
        <%= model.traficoService.jsConfigActual(linea) %>
        var SeparadorJS = '~';
        
        
        var viewByPerfil = '';
        var viewByUser   = '';
    
        function _onmousemove(item) { item.className='select';   }
        function _onmouseout (item) { item.className='unselect'; }
        
        
        function loadCombo(datos, cmb){
            cmb.length = 0;
            for (i=0;i<datos.length;i++){
                var dat = datos[i].split(SeparadorJS);
                addOption(cmb, dat[0], dat[1]);
            }
        }
        
        function loadCombo2(datos, datosA ,cmbA, cmbNA){
            cmbA.length = 0;
            cmbNA.length = 0;
            for (i=0;i<datos.length;i++){
                var dat = datos[i].split(SeparadorJS);                
                if (datosA.indexOf(dat[0])==-1)
                    addOption(cmbNA, dat[0], dat[1]);
                else
                    addOption(cmbA , dat[0], dat[1]);
            }
        }   

        function selectAll(cmb){
            for (i=0;i<cmb.length;i++)
              cmb[i].selected = true;
        }
          
        function validarFormulario (){
            if( formularioNuevo.camposS.length==0 ){
              
			   alert('Debe Seleccionar seleccionar un Campo');               
            }
			else{
            	selectAll (formularioNuevo.camposS );
            	formularioNuevo.submit();
			}
        }
        
        function SelAll(){
            for(i=0;i<FormularioListado.length;i++)
               FormularioListado.elements[i].checked=FormularioListado.All.checked;
        }
        
        function ActAll(){
            FormularioListado.All.checked = true;
            for(i=0;i<FormularioListado.length;i++)	
              if (FormularioListado.elements[i].type=='checkbox' && !FormularioListado.elements[i].checked && !FormularioListado.elements[i].name!='All'){
                  FormularioListado.All.checked = false;
                  break;
              }
        } 
        
        function validarListado(form){
            for(i=0;i<FormularioListado.length;i++)	
                if (FormularioListado.elements[i].type=='checkbox' && FormularioListado.elements[i].checked && !FormularioListado.elements[i].name!='All')
                    return true;
            alert('Por favor seleccione un item para poder continuar');
            return false;
        }   
   
        function objetoOpcion(id, nombre, parametros, tipo, ayuda, usuarios, grupos){
            this.id         = id;
            this.nombre     = nombre;
            this.parametros = parametros;
            this.tipo       = tipo;
            this.ayuda      = ayuda;
            this.usuarios   = usuarios;
            this.grupos     = grupos;
            return this;
        }
        
        

        <%//= model.traficoService.getVarJSSeparador() %>
        <%//= model.traficoService.getVarJSCampo()    %>
        
      
        <%//=var1%>;
        <%//=var2%>;
        
       function clear(combo){
           for (k=combo.length-1; k>=0 ;k--)
               combo.remove(k);
        }
		
	<%-- Added by Andrés Maturana --%>	
        function moveOrder(cmbO, cmbD){
           for (i=cmbO.length-1; i>=0 ;i--)
            if (cmbO[i].selected){
               addOption(cmbD, cmbO[i].value, cmbO[i].text)
               cmbO.remove(i);
            }
           order(cmbD);
           deleteRepeat(cmbD);
        }
	
        function move(cmbO, cmbD){
           for (i=cmbO.length-1; i>=0 ;i--)
            if (cmbO[i].selected){
               addOption(cmbD, cmbO[i].value, cmbO[i].text)
               cmbO.remove(i);
            }
           deleteRepeat(cmbD);
        }
        
        function moveAll(cmbO, cmbD){
         // for (i=cmbO.length-1; i>=0 ;i--){
           var  numero=cmbO.length;
            for (i=0; i< cmbO.length ;i++){
             if(cmbO[i].value!='' && cmbO[i].text!=''){
               addOption(cmbD, cmbO[i].value, cmbO[i].text)
               cmbO.remove(i);
               i=i-1;
             }
           };
           deleteRepeat(cmbD);
        }
        
        <%-- Added by Andrés Maturana --%>
        function moveAllOrder(cmbO, cmbD){
         // for (i=cmbO.length-1; i>=0 ;i--){
           var  numero=cmbO.length;
            for (i=0; i< cmbO.length ;i++){
             if(cmbO[i].value!='' && cmbO[i].text!=''){
               addOption(cmbD, cmbO[i].value, cmbO[i].text)
               cmbO.remove(i);
               i=i-1;
             }
           }
           order(cmbD);
           deleteRepeat(cmbD);
        }
        
        function addOption(Comb,valor,texto){
           if(valor!='' && texto!=''){
                var Ele = document.createElement("OPTION");
                Ele.value=valor;
                Ele.text=texto;
                Comb.add(Ele);
          }
        }    
           
        function order(cmb){
           for (i=0; i<cmb.length;i++)
             for (j=i+1; j<cmb.length;j++)
                if (cmb[i].text > cmb[j].text){
                    var temp = document.createElement("OPTION");
                    temp.value = cmb[i].value;
                    temp.text  = cmb[i].text;
                    cmb[i].value = cmb[j].value;
                    cmb[i].text  = cmb[j].text;
                    cmb[j].value = temp.value;
                    cmb[j].text  = temp.text;
                }
        }
        
		function bajar(){
			var cmb=document.getElementById("camposS");
           var sw=1;
		   for (i=0; i<cmb.length;i++){
					if (cmb[i].selected==true && i < cmb.length && sw==1){
						//alert("hola"+cmb[i].selected);
						var temp = document.createElement("OPTION");
						temp.value = cmb[i+1].value;
                    	temp.text  = cmb[i+1].text;
						cmb[i+1].value = cmb[i].value;
                    	cmb[i+1].text  = cmb[i].text;
						cmb[i].value = temp.value;
                    	cmb[i].text  = temp.text;
						cmb[i].selected=false;
						cmb[i+1].selected=true;
						sw=2;
				 }
           }
        }
		
		function guardar(){
			var cmb=document.getElementById("camposS");
           
		   for (i=0; i<cmb.length;i++){
					cmb[i].selected=true; 	
           }
		  // alert("hola");
		   document.formularioNuevo.action = "<%= CONTROLLER %>?estado=Trafico&accion=GConf"; 
            document.formularioNuevo.submit();
		}
		
		function eliminar(){
			document.formularioNuevo.action = "<%= CONTROLLER %>?estado=Trafico&accion=EConf"; 
           	document.formularioNuevo.submit();
		}
		
		function aplicar(){
		   document.formularioNuevo.action = "<%= CONTROLLER %>?estado=Trafico&accion=AConf"; 
            document.formularioNuevo.submit();
		}
		
		function subir(){
			var cmb=document.getElementById("camposS");
           var sw=1;
		   for (i=0; i<cmb.length;i++){
					if (cmb[i].selected==true && i > 0  && sw==1){
						//alert("hola"+cmb[i].selected);
						var temp = document.createElement("OPTION");
						temp.value = cmb[i-1].value;
                    	temp.text  = cmb[i-1].text;
						cmb[i-1].value = cmb[i].value;
                    	cmb[i-1].text  = cmb[i].text;
						cmb[i].value = temp.value;
                    	cmb[i].text  = temp.text;
						cmb[i].selected=false;
						cmb[i-1].selected=true;
						sw=2;
				 }
           }
        }
		
        function deleteRepeat(cmb){
          var ant='';
          for (i=0;i<cmb.length;i++){
             if(ant== cmb[i].value)
                cmb.remove(i);
             ant = cmb[i].value;
          }
        }

      
        function enviar(){  
            document.formularioNuevo.operacion.value="enviar";
            document.formularioNuevo.action = "configuracion3.jsp"; 
            document.formularioNuevo.submit();
           
        }
        
        function enviarTodo(){  
            document.formularioNuevo.operacion.value="enviarTodo";
            document.formularioNuevo.action = "configuracion3.jsp"; 
            document.formularioNuevo.submit();
           
        }
        
        function regresar(){  
            document.formularioNuevo.operacion.value="regresar";
            document.formularioNuevo.action = "configuracion3.jsp"; 
            document.formularioNuevo.submit();  
        }
        
        function regresarTodo(){  
            document.formularioNuevo.operacion.value="regresarTodo";
            document.formularioNuevo.action = "configuracion3.jsp"; 
            document.formularioNuevo.submit();  
        }
        
        <%-- Added by Andrés Maturana --%>
        function loadComboVar(varn, cmb){
            var varname = 'var' + varn;
            //alert('... SE RECIBIO: ' + varname);
            var datos = eval(varname);
            cmb.length = 0;
            for (i=0;i<datos.length;i++){
                var dat = datos[i].split(SeparadorJS);
                addOption(cmb, dat[0], dat[1]);
            }
        }
        
        function verConfiguracion(obj){
       
            var i = obj.selectedIndex == -1 ? 0 : obj.selectedIndex;
            var value = obj[i].value;
            
            loadComboVar(value, document.formularioNuevo.camposS);
            
            loadCombo(jsCampos, document.formularioNuevo.camposE);
           
            unloadComboVar(value, document.formularioNuevo.camposE);
            
            
        }
        
        function unloadComboVar(varn, cmb){
            var varname = 'var' + varn;
            var datos = eval(varname);
            for(j=0; j<cmb.length; j++){
                for (i=0;i<datos.length && j<cmb.length;i++){                
                    var dat = datos[i].split(SeparadorJS);
                    if( dat[0] == cmb[j].value ){
                        cmb.remove(j);                        
                        if(j>0) j--;
                    }
                }
            }
            order(cmb)
        }
        
        function cargarUltimaConfiguracion(){
            loadCombo(jsConfig, document.formularioNuevo.camposS);
            loadCombo(jsCampos, document.formularioNuevo.camposE);
            unloadCombo(jsConfig, document.formularioNuevo.camposE);
        }
        
        function unloadCombo(datos, cmb){
            for (i=0;i<datos.length;i++){
                var dat = datos[i].split(SeparadorJS);
				for(j=0; j<cmb.length; j++){
					if( dat[0] == cmb[j].value ){
						cmb.remove(j);
						//if(j>0) j--;
						break;
					}
                }
            }
            order(cmb);
        }
		
		function reestablecer(){
			loadCombo(jsCampos, document.formularioNuevo.camposS);
            //unloadCombo(jsCampos, document.formularioNuevo.camposE);
			document.formularioNuevo.camposE.length = 0;
			formularioNuevo.action='<%= CONTROLLER %>?estado=Trafico&accion=Configuracion'; 
			validarFormulario();
		}
    </script>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1"></head>
<body background-color=" #FFFFFF" <% if ( linea.length()!=0 ){ %>onload='cargarUltimaConfiguracion();'<% } %>>
<div id="capaSuperior" style="position:absolute; width:100%; height:100px; z-index:0; left: 0px; top: 0px;">
 <jsp:include page="/toptsp.jsp?encabezado=Configuración"/>
</div>
<div id="capaCentral" style="position:absolute; width:100%; height:87%; z-index:0; left: 0px; top: 100px; overflow: scroll;">

<center>
<%
//out.println("Filtro ="+filtro+"<br>");
%>
<FORM METHOD='POST' NAME='formularioNuevo' id='formularioNuevo'>
<table width="76%" height="417"  border="2" align="center">
    <tr>
        <td height="409" align="left" valign="top">
      
          <table width="100%">
            <tr>
              <td width="40%" colspan='8' class="subtitulo1">Configuraciones</td>
              <td class="barratitulo" align="left"><img src="<%=BASEURL%>/images/titulo.gif" align="left"><%=datos[0]%></td>
            </tr>
          </table>
          <table width="100%" height="417"  border="0" align="center">
    <tr>
        <td height="409" align="left" valign="top">
          <TABLE width='100%' height='100%' class='tablaInferior' valign="top">
          <TR class='fila'><TD height="397" align='center' valign="middle">
              <!-- Grupos Asignados   -->
              <TABLE border='0' width='98%' cellpadding='0' cellspacing='0'>
              <TR class='subtitulos'>
                  <TH width='40%' class='subtitulo1' >Columnas Disponibles</TH>
                  <TH width='10%' >&nbsp;                </TH>
                  <TH width='40%' class='subtitulo1' nowrap>Columnas Visibles </TH>      
				  <TH width ='10%' >&nbsp; </TH>          
              </TR>              
              <TR>
              <TD height="257" valign="top"><select multiple size='15' class='textbox' style='width:100%' name='camposE' id='camposE'>
              <%
                        out.println(combo1);
              %>
              </select></TD>
              <TD align='center'>
			    <img src='<%=BASEURL%>/images/botones/envIzq.gif' style='cursor:hand' title='Pasar a Columnas Disponibles' name='imgEnvIzq'  onclick='moveOrder(camposS, camposE);' onMouseOver='botonOver(this);' onMouseOut='botonOut(this);'><br>
                <img src='<%=BASEURL%>/images/botones/enviarIzq.gif' style='cursor:hand' title='Pasar TODO a Columnas Disponibles' name='imgTEnvIzq'  onclick='moveAllOrder(camposS,camposE);' onMouseOver='botonOver(this);' onMouseOut='botonOut(this);'><br>
				<img src='<%=BASEURL%>/images/botones/envDer.gif' style='cursor:hand' title='Pasar a Columnas Visibles' name='imgEnvDer'  onclick='move(camposE, camposS)' onMouseOver='botonOver(this);' onMouseOut='botonOut(this);'><br>
                <img src='<%=BASEURL%>/images/botones/enviarDerecha.gif' style='cursor:hand' title='Pasar TODO a Columnas Visibles' name='imgTEnvDer'  onclick='moveAll(camposE, camposS);' onMouseOver='botonOver(this);' onMouseOut='botonOut(this);'><br>
              </TD>
              <TD valign="top"><select multiple size='15' class='textbox' style='width:100%' name='camposS' id='camposS' >
                <%out.println(combo2);%>
              </select> </TD>
			  <TD align='center'>
			    <img src='<%=BASEURL%>/images/botones/envArriba.gif' style='cursor:hand' title='Pasar a Columnas Visibles' name='imgEnvArr'  onclick='subir(); ' onMouseOver='botonOver(this);' onMouseOut='botonOut(this);'><br>
			    <img src='<%=BASEURL%>/images/botones/envAbajo.gif' style='cursor:hand' title='Pasar a Columnas Disponibles' name='imgEnvAba'  onclick='bajar();' onMouseOver='botonOver(this);' onMouseOut='botonOut(this);'><br>
				</TD>
              </TR>
              </TABLE>
              <!-- Fin Grupos no Asignados   -->
               <input type='hidden' name='Id' value=''>
               <input type='hidden' name="var1" value="<%=var1%>">
                <input type='hidden' name="operacion" value="<%=operacion%>">
                <input type='hidden' name="var2" value="<%=var2%>"> 
                <input type='hidden' name="var3" value="<%=var3%>"> 
                <input type='hidden' name='clas' value='<%=clas%>'>
                <input type='hidden' name='filtro' value="<%=filtro%>">
                <input type='hidden' name='usuario' value='<%=usuLogin%>'>
                <input type='hidden' name='linea' value='<%=linea%>'>
                <input type='hidden' name='filtroP' value='<%=filtroP%>'>				
                <table width="100%">
				<tr>
				<Td valign="top" align="center">				
				<img src='<%=BASEURL%>/images/botones/aceptar.gif' style='cursor:hand' title='Aplicar configuracion' name='imgAceptar'  onclick="formularioNuevo.action='<%= CONTROLLER %>?estado=Trafico&accion=Configuracion'; validarFormulario();" onMouseOver='botonOver(this);' onMouseOut='botonOut(this);'> <img src='<%=BASEURL%>/images/botones/restablecer.gif' style='cursor:hand' title='Restalecer Configuración' name='ResetConfig'  onclick='reestablecer();' onMouseOver='botonOver(this);' onMouseOut='botonOut(this);'> <img src="<%=BASEURL%>/images/botones/salir.gif"  name="salir" title="Salir..." onClick="window.close();" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);">             
       			    </Td>
			  </tr>
			  <tr>
			  <td valign="top">
			  <hr>
			  <table width="100%"  border="0" cellpadding="0" cellspacing="0" class="tablaInferior">
  				<tr class="fila">
				    <td width="25%">Configuracion: </td>
			    	<td width="75%" nowrap><input type="text" name="nombreC" id="nombreC" >
			  <img src='<%=BASEURL%>/images/botones/agregar.gif' name='guardarFiltro' align="absmiddle" style='cursor:hand' title='Guardar filtro'  onclick="if( nombreC.value.replace(/ /g, '').length != 0 ) { guardar(); } else { alert('Debe digitar el nombre de la configuración.'); }" onMouseOver='botonOver(this);' onMouseOut='botonOut(this);'></td>
			  	</tr>
			  </table>
			</td>
			  </tr>
			  </table>
			  </TD></TR>        
  </TABLE>      </td><td valign="top">
  <TABLE width='100%' height='100%' class='tablaInferior'>
          <TR class='fila'><TD height="400" align='center' valign="top">
              <!-- Grupos Asignados   --><br><br>
              <TABLE border='0' width='90%' cellpadding='0' cellspacing='0'>
              <TR class='subtitulo1'>
                  <TH width='30%' height="19" align="center"  nowrap>Configuraciones</TH>      
              </TR>              
              <TR height="257">
              <TD valign="top" align="center" >
			  <input:select name="cConfiguraciones"      attributesText="<%= "size='15' id='cConfiguraciones' style='width:100%;' class='textbox' onChange='verConfiguracion(this)' "  %>"  default="010" options="<%=tConf%>" /> 
			  </TD>
              </TR>
			  <Tr><td colspan="2" align="center" nowrap><br>
			  <img src='<%=BASEURL%>/images/botones/aceptar.gif' style='cursor:hand' title='Aplicar un filtro guardado' name='imgagregar'  onclick="if( cConfiguraciones.selectedIndex != -1) { aplicar(); } else { alert('Debe seleccionar alguna configuración.'); }" onMouseOver='botonOver(this);' onMouseOut='botonOut(this);'> <img src='<%=BASEURL%>/images/botones/eliminar.gif' style='cursor:hand' title='Eliminar filtro' name='imgeliminar'  onclick="if( cConfiguraciones.selectedIndex != -1) { eliminar(); } else { alert('Debe seleccionar alguna configuración.'); }" onMouseOver='botonOver(this);' onMouseOut='botonOut(this);'>              
			  </td></Tr>
              </TABLE>   <br>  
				
              </TD>
          </TR>        
  </TABLE>
  </td>
        </tr>
  </table>   </td>
      </tr>
  </table>
</FORM>
    
    <br><br>
	
	</div>
	<%=datos[1]%>
</body>
</html>

