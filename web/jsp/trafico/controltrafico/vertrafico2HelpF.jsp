<!--
     - Author(s)       :      Karen Reales
     - Date            :      06/05/2006
     - Copyright Notice:      TRANSPORTES SANCHEZ POLO S.A.
-->
<%--   
     - @(#)  
     - Description: Ayuda
--%> 
<%@include file="/WEB-INF/InitModel.jsp"%>


<html>
    <head>
        <title>.: Descripción del programa de Control Trafico :.</title>
        <link href="<%=BASEURL%>/css/estilostsp.css" rel="stylesheet" type="text/css">
    </head>
    <body>
        <BR>
        <table width="98%"  border="2" align="center">
            <tr>
            <td width="811" valign="top" >
                <table width="100%" border="0" align="center">
                    <tr  class="subtitulo">
                        <td height="24" align="center">
                        TRAFICO </td>
                    </tr>
                    <tr class="subtitulo1">
                        <td> PANTALLA PRINCIPAL CONTROL TRAFICO </td>
                    </tr>
                    <tr class="ayudaHtmlTexto">
                      <td> <div align="left">
					  </div>                        
                        <br>
                        <p>Al dar inicio al programa aparece una pantalla as&iacute;: </p>
                        <p align="center"><img width="990" height="461" src="../../../images/ayuda/trafico/control_trafico/1.PNG"></p>
                        <br>
                        <p>Pueden haber filas amarillas, naranjas, rojas, verdes. <br>
                        Las filas de color azul claro no presentan ninguna novedad. <br>
                        Las filas color amarillo indican retraso desde 1 minuto hasta media hora. <br>
                        Las filas color naranja, indican retraso desde &frac12; hora hasta 50 horas. <br>
                        Las filas color rojo, indican retraso de mas de 50 horas. <br>
                        Las filas color verde, indican que el veh&iacute;culo est&aacute; detenido, ya sea por estar varado, accidentado o pernoctando. En estos casos el tiempo no sigue corriendo. </p>
                        <p align="center"><img width="903" height="286" src="../../../images/ayuda/trafico/control_trafico/12.PNG"><br>
                          <br>
                          <img width="929" height="141" src="../../../images/ayuda/trafico/control_trafico/13.PNG"><br>
                          <br>
                        <img src="../../../images/ayuda/trafico/control_trafico/FilasRojas.PNG" width="940" height="371">                        </p>
                        <p>Cada campo muestra lo siguiente:</p>
                        <p align="center"><img src="../../../images/ayuda/trafico/control_trafico/14.PNG" width="938" height="440"><br>
                          En la pantalla aparecen distintos campos para buscar o filtrar los datos. </p>                        <p align="center"><img width="937" height="322" src="../../../images/ayuda/trafico/control_trafico/2.PNG"><br>
                        </p>
                        <p>Por ejemplo: </p>
                        <p align="center"><img width="942" height="316" src="../../../images/ayuda/trafico/control_trafico/3.PNG" ><br>
  De manera an&aacute;loga ocurre al digitar los campos: Placa y Conductor. </p>
                        <p>Al digitar una placa que no aparece en el reporte, el programa genera un mensaje as&iacute;: </p>
                        <p align="center"><img width="209" height="126" src="../../../images/ayuda/trafico/control_trafico/4.PNG"><br>
                        </p>
                        <p>Lo mismo ocurre si se digita una planilla o un conductor inexistente en el reporte. <br>
                        Cuando se ingresa un valor entre 5 y 60 en la casilla tiempo, y se da clic en aceptar, el programa arroja un mensaje as&iacute;: </p>
                        <p align="center"><img width="329" height="126" src="../../../images/ayuda/trafico/control_trafico/5.PNG"><br>
                        </p>
                        <p>Para realizar cambios sobre la planilla, se hace clic derecho sobre la fila que contiene la planilla a modificar. <br>
                        Seguidamente aparece la siguiente pantalla: </p>
                        <p align="center"><img width="944" height="532" src="../../../images/ayuda/trafico/control_trafico/6.PNG"><br>
                        </p>
                        <br>
                        <p>Para saber lo que se debe hacer en cada uno de los links de modificaci&oacute;n: </p>
                        <p align="center"><img width="720" height="474" src="../../../images/ayuda/trafico/control_trafico/8.PNG"><br>
                        </p>
                        <br>
                        <p>Y en los links de Consultas: </p>
                        <p align="center"><img width="702" height="421" src="../../../images/ayuda/trafico/control_trafico/9b.PNG"><br>
                        </p>
                        <br>
                        <p>Para las planillas creadas manualmente, se presenta lo siguiente: </p>
                        <p align="center"><img width="848" height="540" src="../../../images/ayuda/trafico/control_trafico/10.PNG"><br>
                        </p>
                        Los links presentan las mismas opciones de las planillas creadas a trav&eacute;s de la web; excepto: <br>
                        <p align="center"><img width="501" height="168" src="../../../images/ayuda/trafico/control_trafico/11b.PNG"><br>
  </p></td>
                    </tr>
                </table>
            </td>
            </tr>
    </table>
        <p align="center" class="fuenteAyuda"> Fintravalores S. A. </p>
    </body>

</html>
