<!--
- Autor : Ing. David Lamadrid
- Date  : 5 de Octubre de 2005
- Copyrigth Notice : Fintravalores S.A. S.A
-->
<%--
-@(#)
--Descripcion : Pagina JSP, que se encarga de realizar todas las operaciones referentes al control trafico
--%>

<%-- Declaracion de librerias--%>
<%@page contentType="text/html"%>
<%@page session="true"%>
<%@page errorPage="../error/ErrorPage.jsp"%>
<%@page import="java.util.*"%>
<%@page import="java.sql.*"%>
<%@page import="com.tsp.operation.model.*"%>
<%@page import="com.tsp.util.*"%>
<%@page import="com.tsp.operation.model.beans.*, java.util.*"%>
<%@include file="/WEB-INF/InitModel.jsp"%>
<%@taglib uri="http://jakarta.apache.org/taglibs/input-0.90" prefix="input" %>
<%String path = application.getRealPath(model.menuService.getRealUrl(request.getRequestURI()));
  String datos[] = model.menuService.getContenidoMenu(BASEURL,request.getRequestURI(),path);
%>
<html>
<head>
    <title>Control trafico</title>
   
    <link href="<%= BASEURL %>/css/estilostsp.css" rel="stylesheet" type="text/css">
    <link href="/css/estilostsp.css" rel="stylesheet" type="text/css">
</head>
<script type='text/javascript' src="<%= BASEURL %>/js/reporte.js"></script>
<script type='text/javascript' src="<%= BASEURL %>/js/boton.js"></script>


<script>

var horaDeEntrada;

function horaEntrada(){
	var hora = new Date();
	alert (hora);
	horaDeEntrada=hora.getTime();
	alert (horaDeEntrada);
}

function abrirVentanaBusq(an,al,url,pag) {
    window.open(url,'Trafico','width='+an+',height='+al+',scrollbars=no,resizable=yes,top=10,left=65, status=yes');
}

function resetearAcciones(){
//alert("Refrescar");
	var zona  = document.getElementById("zonasUsuario");
	document.forma1.action = "<%= CONTROLLER %>?estado=Trafico&accion=R&zonasUsuario="+zona.value; 
	document.forma1.submit();
}
		
function reiniciar(){
	document.forma1.action = "<%= CONTROLLER %>?estado=Trafico&accion=Resetear"; 
	document.forma1.submit();
}

function actualizarAlarma(){
	//alert("Entre a actualizar alarma");
	var hora= new Date();
	var horaActual= hora.getTime();
	var tabla = document.getElementById("tabla3");
	for(var i=1;i< tabla.rows.length;i++){	   		 
		var fila=tabla.rows[i];
		var idFila=fila.id
		var pos = idFila.indexOf("~");
		idFila=idFila.substr(0,pos);
		var duracion = document.getElementById("duracion"+idFila);
		//alert(duracion);
		var tipo_despacho=document.getElementById("tipo_despacho"+idFila);
		var detenido = document.getElementById("estadoDetencion"+idFila);
		//alert(tipo_despacho);
		var celda=fila.cells[(fila.cells.length-1)];	
		var fechaPReporte=celda.abbr;
		
		if(tipo_despacho.value!="v" || tipo_despacho.value!="V" ){
			if((fechaPReporte != 59043146400000)&& (fechaPReporte != 59043150000000)&&(fechaPReporte != -59043146400000)&& (fechaPReporte != -59043150000000)){
				var minutosAtrazo=(horaActual - fechaPReporte)-(duracion.value*60*1000);
				
				if( minutosAtrazo >=0){			
					minutosAtrazo=((minutosAtrazo/1000)/60);
					var mi = parseInt(""+minutosAtrazo);			
					if (mi <= 30 && mi > 0){
						fila.bgColor="#FFFFBB";
					}
					if((mi > 30) &&( mi <= 60)){
						fila.bgColor="#FFBF80";
					}
					if (mi >3000){
						fila.bgColor="#FF9F9F";
					}
					
					if(detenido.value=="D"){
						fila.bgColor="#BDCE10";
					}
					if(i==(tabla.rows.length-1)){
					}
					var horas=0;
					var minutos = mi%60;
					var horas =mi-minutos; 
					horas=horas/60;
					celda.innerHTML= horas + " : "+minutos;
					celda.appendChild(duracion);		
					celda.appendChild(tipo_despacho);
					celda.appendChild(detenido);	
				}
			}
		}
		else{
			if((fechaPReporte != 59043146400000)&& (fechaPReporte != 59043150000000)&&(fechaPReporte != -59043146400000)&& (fechaPReporte != -59043150000000)){
				var retraso=(horaActual-(((parseInt(""+duracion.value)/1000)/60)+fechaPReporte));
				var mi = parseInt(""+retraso)		
				if(retraso > 6000000){
					fila.bgColor="#0066CC";
					var horas=0;
					var minutos = mi%60;
					var horas =mi-minutos; 
					horas=horas/60;
					celda.innerHTML= horas + " : "+minutos;
					celda.appendChild(duracion);		
					celda.appendChild(tipo_despacho);	
				}
			}
		}	
	}
}

function alarma(){
	//alert("entre en alarma");
	setInterval("actualizarAlarma()",60000);
}


function actualizarT(){	
	var campoe = document.forma1.tiempor;
	var tiempo =parseInt(campoe.value)*60000;
	setInterval("resetearAcciones()",tiempo);
}

var timpoRefersca=1800000;

function asignarTiempo(){
	 var campo = document.getElementById("t_r");
	  var campoe = document.forma1.tiempor;
	if(isNaN (campo.value) || campo.value=="" ){
		alert ("El tiempo debe ser un Numero ");
		campo.focus();
		campo.value="";
		return false;
	}
	else{
		if(Number(campo.value)<5 || Number(campo.value)>60 ){
			alert('El tiempo no puede ser menor a 5 minutos o mayor a 60 minutos');
			return false;
		}
		else{
			timpoRefersca=campo.value*60000;
			campoe.value=campo.value*60000;
			alert('Tiempo de refrescamiento actualizado a '+ campo.value + ' minuto(s)');
			campo.value="";
		}
	}
	resetearAcciones();
}

var isIE = document.all?true:false;
var isNS = document.layers?true:false;
function soloDigitos(e,decReq) {
	//alert(window.event.keyCode);
	var key = (isIE) ? window.event.keyCode : e.which;
    var obj = (isIE) ? event.srcElement : e.target;
    var isNum = (key > 47 && key < 58) ? true:false;
    var dotOK =  (decReq=='decOK' && key ==46) ? true:false;
    window.event.keyCode = (!isNum && !dotOK && isIE) ? 0:key;
    e.which = (!isNum && !dotOK && isNS) ? 0:key;
    return (isNum || dotOK );
}
</script>
<%
    java.util.TreeMap zonasU;
	zonasU=  model.traficoService.getZonas ();
   
    String []camposS = request.getParameterValues ("camposS");
    String reload = request.getParameter ("reload");
    session.setAttribute ("salida", "");
    String conf = request.getParameter ("conf");
    String linea = request.getParameter ("linea");
    String clas = request.getParameter ("clas");
    String filtro = request.getParameter ("filtro");
	String filtroP = request.getParameter("filtroP");
	if(filtroP!=null){
		if(filtroP.equalsIgnoreCase("No se han aplicado filtros")){
			filtroP="";
		}
		filtroP = filtroP.replaceAll("null","");
		filtroP = filtroP.replaceAll("'","");
		filtroP = filtroP.replaceAll(" and ","SEPARADOR");
		filtroP = filtroP.replaceAll(" or ","SEPARADOR");
	}
	String tiempo =""+ request.getParameter ("tiempo");
	if(tiempo.equals("") || tiempo.equals("null")){
		tiempo="60000";
	}
    String var1 = request.getParameter ("var1");
    String var2 = request.getParameter ("var2");
    String var3 = request.getParameter ("var3");
    Usuario usuarioLogin  = (Usuario) session.getAttribute("Usuario");
    String us  = usuarioLogin.getLogin();
    java.util.Date utilDate = new java.util.Date(); //fecha actual
    long lnMilisegundos = utilDate.getTime();
    java.sql.Timestamp sqlTimestamp = new java.sql.Timestamp(lnMilisegundos);
   
    Vector campos = new Vector();
    campos=model.traficoService.obtCamposTrafico();
    Vector co2= model.traficoService.obtColTrafico();
    Vector tabla = model.traficoService.generarColumnas(campos,co2);
    Vector resultado = new Vector();
    resultado=model.traficoService.obtVecTrafico();
    
    String para = "/jsp/trafico/controltrafico/filtro.jsp?linea="+linea+"&conf="+conf+"&clas="+clas+"&var2="+var2+"&var1="+var1+"&var3="+var3+"&filtro=null&filtroP=" + ( filtroP!=null ? filtroP : "" )+"&zonasUsuario="+request.getParameter("zonasUsuario");
    para = para.replaceAll("'","-_-");
    System.out.println("PARA---->"+ para); 
    
    
    String para1 = "/jsp/trafico/controltrafico/configuracion3.jsp?clas="+clas+"&filtro="+filtro+"&var1="+var1+"&var2="+var2+"&var3="+var3+"&linea="+linea+"&filtro=null&filtroP=" + ( filtroP!=null ? filtroP : "" )+"&zonasUsuario="+request.getParameter("zonasUsuario");
    System.out.println("PARA 1 ---->"+ para1); 
    
    
    
    String para2 = "/jsp/trafico/controltrafico/clasificacion.jsp?linea="+linea+"&conf="+conf+"&filtro="+filtro+"&var2="+var2+"&var1="+var1+"&var3="+var3+"&clas="+clas+"&filtro=null&filtroP=" + ( filtroP!=null ? filtroP : "" )+"&zonasUsuario="+request.getParameter("zonasUsuario");
    para2 = para2.replaceAll("'","-_-");
    System.out.println("PARA 2---->"+ para2);
 
    
    String para3 ="/jsp/trafico/controltrafico/columnas.jsp?linea="+linea+"&conf="+conf+"&filtro="+filtro+"&var2="+var2+"&var1="+var1+"&var3="+var3+"&filtro=null&filtroP=" + ( filtroP!=null ? filtroP : "" )+"&zonasUsuario="+request.getParameter("zonasUsuario");
    para3 = para3.replaceAll("'","-_-");
    System.out.println("PARA 3---->"+ para3); 
    
	String para4 ="/jsp/trafico/despacho_manual/despacho.jsp";
	String para5 = "/jsp/trafico/zona/planillaxnro.jsp?marco=no";
	String para6 = "/consultas/consultasPlanillaNormal.jsp";
	
	String configuracion = linea.replaceAll("-","  ");
	
%>
<body  <%if(request.getParameter("reload")!=null){%>
	onLoad="parent.close();alarma();actualizarT();parent.opener.location.href='<%=BASEURL%>/jsp/trafico/controltrafico/vertrafico2.jsp?conf=<%=request.getParameter ("conf").replaceAll("'","-_-")%>&linea=<%=request.getParameter ("linea")%>&clas=<%=request.getParameter ("clas")%>&filtro=<%=request.getParameter ("filtro")%>&filtroP=<%=filtroP%>&var1=<%=request.getParameter ("var1")%>&var2=<%=request.getParameter ("var2")%>&var3=<%=request.getParameter ("var3")%>&zonasUsuario=<%=request.getParameter("zonasUsuario")%>';redimensionar();" 
 <%}else{%>
	onLoad="alarma();redimensionar();actualizarT();"
 <%}%>
onResize="redimensionar();" >

<div id="capaSuperior" style="position:absolute; width:100%; height:100px; z-index:0; left: 0px; top: 0px;">
 <jsp:include page="/toptsp.jsp?encabezado=Control de Trafico"/>
</div>
<div id="capaCentral" style="position:absolute; width:100%; height:85%; z-index:0; left: 0px; top: 100px; overflow: scroll;"> 
<table width="100%" border="0" bgcolor="#4D71B0">
  <tr>
    <td>
    &nbsp;<img src='<%=BASEURL%>/images/cuadrito.gif' width='9' height='9'>&nbsp; <a href="javascript:abrirVentanaBusq(850,550,'<%=BASEURL%><%=para%>')" class="encabezado">Filtro</a>
	&nbsp;<img src='<%=BASEURL%>/images/cuadrito.gif' width='9' height='9'>&nbsp; <a href="javascript:abrirVentanaBusq(750,500,'<%=BASEURL%><%=para1%>')" class="encabezado">Configuracion</a>
	&nbsp;<img src='<%=BASEURL%>/images/cuadrito.gif' width='9' height='9'>&nbsp; <a href="javascript:abrirVentanaBusq(750,550,'<%=BASEURL%><%=para2%>')" class="encabezado">Clasificacion</a>
	&nbsp;<img src='<%=BASEURL%>/images/cuadrito.gif' width='9' height='9'> &nbsp; <a href="javascript:abrirVentanaBusq(750,550,'<%=BASEURL%><%=para3%>')" class="encabezado">Tama�o</a>
	&nbsp;<img src='<%=BASEURL%>/images/cuadrito.gif' width='9' height='9'> &nbsp; <a href="javascript:abrirVentanaBusq(750,550,'<%=BASEURL%><%=para5%>')" class="encabezado">Cambiar Zona</a>
	&nbsp;<img src='<%=BASEURL%>/images/cuadrito.gif' width='9' height='9'> &nbsp; <a class="encabezado" onClick="resetearAcciones();" style="cursor:hand; text-decoration:underline ">Refrescar</a>
	&nbsp;<img src='<%=BASEURL%>/images/cuadrito.gif' width='9' height='9'> &nbsp; <a class="encabezado" onClick="reiniciar();" style="cursor:hand; text-decoration:underline ">Reiniciar</a>
	&nbsp;<img src='<%=BASEURL%>/images/cuadrito.gif' width='9' height='9'> &nbsp; <a href="javascript:abrirVentanaBusq(850,550,'<%=BASEURL%><%=para4%>')" class="encabezado">Despacho Manual</a>
	&nbsp;<img src='<%=BASEURL%>/images/cuadrito.gif' width='9' height='9'>	  </td>
    <td ><%=datos[0]%></td>
  </tr>
</table>
<br>
<%
/*for(int i=0;i<campos.size();i++)
  	out.println("<br>configuracion "+campos.elementAt(i));
   	for(int i=0;i<co2.size();i++)
    	out.println("<br> campo col:"+co2.elementAt(i));
   		for(int i=0;i<tabla.size();i++)
   			out.println("<br> tabla col:"+tabla.elementAt(i));*/
//out.println("filtro :"+filtro);
//out.println("<br>linea :"+linea);
//out.println("<br>var1 :"+var1);
//out.println("<br>var2 :"+var2);
//out.println("<br>var2 :"+zonasU.size());
%>

<table width="100%" border="2">
  <tr>
    <td>
		<table width="100%" border="0" align="left" class="tablaInferior">
      	<tr class="fila">
        	<td width="60"> Planilla: </td>
        	<td width="87" nowrap>
				<input name="nPlanilla" type="text"  class="textbox" id="nPlanilla" onKeyUp="procesar(this.value);" size="6" maxlength="6" > 
          		<img src='<%=BASEURL%>/images/botones/iconos/lupa.gif' width="16" height="15" name='Buscar' align="absmiddle" style='cursor:hand' title='Aplicar configuracion'  onclick="recorrido(nPlanilla.value);" >          </td>
      	 	<td width="46"> Placa: </td>
         	<td width="167" nowrap>
			 	<input name="nPlaca" id="nPlaca" type="text"  class="textbox" onKeyUp="procesar2(this.value);" size="6" maxlength="6" > 
    	      	<img src='<%=BASEURL%>/images/botones/iconos/lupa.gif' width="16" height="15" name='Buscar' align="absmiddle" style='cursor:hand' title='Aplicar configuracion'  onclick="recorridoPlaca(nPlaca.value);" > <a onClick="window.open('<%=CONTROLLER%>?estado=MovimientoTrafico&accion=BuscarPlaca&cmd=show&placa='+nPlaca.value)" class="Simulacion_Hiper" style="cursor:hand ">Ver Ubicacion</a></td>
			<td width="81"> Conductor: </td>
         	<td width="159" nowrap>
		 		<input name="nConductor" id="nConductor" type="text"  class="textbox" onKeyUp="procesar3(this.value);" > 
          		<img src='<%=BASEURL%>/images/botones/iconos/lupa.gif' width="16" height="15" name='Buscar' align="absmiddle" style='cursor:hand' title='Aplicar configuracion'  onclick="recorridoConductor(nConductor.value);" >          </td>
			<td width="45" nowrap> Zona: </td>	
   			<td width="110">				<input:select name="zonasUsuario"      attributesText="<%= " id='zonasUsuario' class='listmenu'  onChange='filtroPorZona();' style='width:100px;'" %>" options="<%=zonasU%>" default='<%=request.getParameter("zonasUsuario")!=null?request.getParameter("zonasUsuario"):""%>'/></td>
	  		<td width="62" >Tiempo: </td>
        	<td width="122"  nowrap>
				<input name="t_r" type="text"  class="textbox" id="t_r" size="5" maxlength="3" onKeyPress="soloDigitos(event, 'decNO')" > Min 
          		<img src='<%=BASEURL%>/images/botones/aceptar.gif' name='Buscar' align="absmiddle" style='cursor:hand' title='Aplicar configuracion'  onclick="asignarTiempo();" onMouseOver='botonOver(this);' onMouseOut='botonOut(this);'>          </td>
	  	</tr>
      	<tr class="fila">
      	  <td colspan="3">Ver filtros actuales <img src='<%=BASEURL%>/images/botones/iconos/lupa.gif' width="16" height="15" name='Buscar' align="absmiddle" style='cursor:hand' title='Ver Filtros'  onclick="alert('Filtros aplicados\n\n <%=filtroP!=null?filtroP:"No se han aplicado filtros"%> ');" ><br>
</td>
      	  <td colspan="3" nowrap>Ver configuraciones actuales <img src='<%=BASEURL%>/images/botones/iconos/lupa.gif' width="16" height="15" name='Buscar' align="absmiddle" style='cursor:hand' title='Ver Configuracion'  onclick="alert('Configuracion Aplicada \n\n <%=configuracion%>');" ></td>
      	  <td nowrap>&nbsp;</td>
      	  <td><input type="hidden" name="textfield" value="<%=request.getParameter("zonasUsuario")%>"></td>
      	  <td >&nbsp;</td>
      	  <td  nowrap>&nbsp;</td>
    	  </tr>
    	</table>
	</td>
  </tr>
</table>


<form name="forma1" id="forma1" method="post">
	<input type="hidden" value="<%=conf%>" name="conf">
	<input type="hidden" value="<%=var1%>" name="var1">
	<input type="hidden" value="<%=var2%>" name="var2">
	<input type="hidden" value="<%=var3%>" name="var3">
	<input type="hidden" name="tiempoR" id="tiempoR" value="30">
	<input type="hidden" value="<%=linea%>" name="linea">
	<input type="hidden" value="<%=filtro%>" name="filtro">
	<input type="hidden" value="<%=filtroP%>" name="filtroP">
	<input type="hidden" value="<%=clas%>" name="clas">
	<input type="hidden" value="<%=us%>" name="usuario">
	<input type="hidden"  value="<%=tiempo%>" name="tiempor" id="tiempor">
    <input type="hidden" id="cedula" name="cedula" value="0">
    <input type="hidden" id="cz" name="cz" value="">
    <input type="hidden" id="fechaS" name="fechaS" value="">
    <input type="hidden" id="c_titulo" name="c_titulo" value="I">
    <input type="hidden" id="celular" name="celular" value="">
    <input type="hidden" id="tipoDespacho" name="tipoDespacho" value="">
	    <input type="hidden" id="clas" name="clas" value="<%=clas%>">
	<table width="" border="2" align="center" id="tabla1"  onClick="hidemenuie5();hidemenuie5Manual();">
    <tr>
		<td>
    	<table width="100%">
        	<tr>
            	<td width="40%" colspan='8' class="subtitulo1">Planillas </td>
                <td class="barratitulo" align="left"><img src="<%=BASEURL%>/images/titulo.gif"></td>
            </tr>
        </table>

		<table width="100%" border="1"    align="left" borderColor="#999999" bgcolor="#F7F5F4" id="tabla3" >
      		<tr class="tblTitulo" id="titulos" nowrap>
        		<%for(int i=0;i< tabla.size();i++){
                  	Vector fila = (Vector)tabla.elementAt(i);
                    int ancho = Integer.parseInt(""+fila.elementAt(2));
                    ancho =ancho*5;%>
            	<td id="titulo" abbr="<%=fila.elementAt(1)%>" nowrap><%=fila.elementAt(0)%></td>
        		<%}%>
        		<td nowrap align="center"> T (M)</td>
      		</tr>   
 			<%String idPlanilla="";
        	String numeroPlanilla="";
 		    String colorAlarma="";
        	long atrazo=0;
			int duracion=0;
			long horas=0;
			long minutos=0;
			String c_duracion="";
	        String f2="";
			String placa="";
			String cedula="";
			String fechaSalida="";
			String celular="";
			String tipoDespacho="";
			String nombreConductor="";
			String estado ="";
			String observacion="";
			String color="";
			String reg_status ="";
			String eintermed="";
                        String obs=""; 
    	    for ( int i = 0; i < resultado.size(); i++ ) {
				Vector fila = (Vector)resultado.elementAt(i);
				java.sql.Timestamp fecha1 =  java.sql.Timestamp.valueOf(""+fila.elementAt((campos.size())));
				java.sql.Timestamp fecha2 = java.sql.Timestamp.valueOf(""+fila.elementAt((campos.size()+1)));
				numeroPlanilla=""+fila.elementAt(campos.size()+2);
				c_duracion=(String)fila.elementAt(campos.size()+3);
				duracion = Integer.parseInt(c_duracion);
				placa=""+fila.elementAt(campos.size()+4);
				cedula=""+fila.elementAt(campos.size()+5);
				fechaSalida=""+fila.elementAt(campos.size()+6);
				celular=""+fila.elementAt(campos.size()+7);
				tipoDespacho=""+fila.elementAt(campos.size()+8);
				nombreConductor=""+fila.elementAt(campos.size()+9);
			 	//c_duracion= c_duracion.substring(0,c_duracion.indexOf("0"));
				atrazo=model.traficoService.getDiferencia(fecha2,sqlTimestamp);
				atrazo      = (atrazo*-1)-duracion;
				estado      = ""+fila.elementAt(campos.size()+10);
				observacion = ""+fila.elementAt(campos.size()+11);
				color       = ""+fila.elementAt(campos.size()+12);
                obs         = ""+fila.elementAt(campos.size()+13);
                reg_status  = ""+fila.elementAt(campos.size()+14);
				eintermed  = ""+fila.elementAt(campos.size()+15);
                                
                                
				
    	        colorAlarma="";		
			 	f2=""+fila.elementAt((campos.size()+1));
				
				if(f2.equals("0099-01-01 00:00:00")){
					atrazo=0;		
				 	if((i%2)!=0){
    	            	colorAlarma="#F7F5F4";
					}
        	    	
					else {
            	    	colorAlarma="#EEEEF2";
					}
					
						
				}
        		else{		
					colorAlarma=model.traficoService.generarAlarma(atrazo,i);
					//atrazo=atrazo*-1;
    	    	}
//				atrazo=atrazo - duracion;
				minutos=atrazo%60;
				horas=atrazo-minutos;
				horas=horas/60;
         	    long minuto2=minutos<0?minutos*-1:minutos;
				if (estado.equalsIgnoreCase("D")){
					colorAlarma="#BDCE10";
				}
			%>
			
			<tr  id ='<%=numeroPlanilla+"~"+placa+"~"+nombreConductor%>'  onContextMenu="mostrarMenu(this.id,'<%=cedula%>','<%=fechaSalida%>','<%=celular%>','<%=tipoDespacho%>');return false;" abbr='<%=colorAlarma%>' border="1" bordercolor="#E6E6E6"  class="letraTitulo"   onMouseOver="cambiarColorMouse(this);" onMouseOut="bgColor=this.bgcolor" bgcolor='<%=colorAlarma%>'  style="cursor:hand;<%if(!color.equals("#003399")){%>color:<%=color%>;<%}%>">
        		<%for (int ii=0;ii< campos.size();ii++){
					Vector fila1 = (Vector)tabla.elementAt(ii);
					String numero= ""+fila1.elementAt(2);
					String alineacion=""+fila1.elementAt(3);
					int n= Integer.parseInt(numero);
					String campo1= (reg_status.equals("D")&& fila.elementAt(ii).equals(observacion) )?""+obs:""+fila.elementAt(ii);
                                        //String campo1= ""+fila.elementAt(ii);
                                       
	             	if (n <= campo1.length() && n > 0)
    	            	campo1=campo1.substring(0,n);
        		%>
            	<td  nowrap  align="<%=alineacion%>" abbr="<%=campo1%>" title="<%=observacion%>" class="bordereporte" ><%=campo1%></td>
        		<%}%>
       		  <td  width=""  align="center" nowrap bordercolor="#E6E6E6"  abbr="<%=fecha2.getTime()%>" >
			  
					<input type="hidden" name="duracion<%=numeroPlanilla%>" id="duracion<%=numeroPlanilla%>"  value="<%=c_duracion%>"><%=horas+" : "+minuto2%>
					 <input name="tipo_despacho<%=numeroPlanilla%>" type="hidden" id="tipo_despacho<%=numeroPlanilla%>" value="<%=tipoDespacho%>">
              <input type="hidden" name="estadoDetencion<%=numeroPlanilla%>" id="estadoDetencion<%=numeroPlanilla%>" value="<%=estado%>"></td>
			</tr>
  			<%}//cierre del for de resultado%>
        </table>    
		</td>
	</tr>			
	</table>

  </form>
</div>
<%=datos[1]%>
</body>
<script  language="javascript1.4" type="text/javascript">
var tabla = document.getElementById('tabla3');
var color = "";
	
function color1(){this.style.backgroundColor="#F7F5F4";}

function color2(){this.style.backgroundColor=""+color;}

function actualizar(){	parent.location= "<%= CONTROLLER %>?estado=Trafico&accion=Mostrar"; }

function filtroPorZona(){
	var cmb=document.getElementById("zonasUsuario");
	<%linea="\""+linea+"\"";%>
	<%filtro="\""+filtro+"\"";%>
	<%conf="\""+conf+"\"";%>
	<%clas="\""+clas+"\"";%>
	<%var1="\""+var1+"\"";%>
	<%var2="\""+var2+"\"";%>
	<%var3="\""+var3+"\"";%>
	var var3=<%=var3%>;
	var var2=<%=var2%>;
	var var1=<%=var1%>;
	var conf=<%=conf%>;
	var clas=<%=clas%>;
	var linea=<%=linea%>;
	var filtro=<%=filtro%>;
	var zona=cmb.value;
    parent.location= "<%= CONTROLLER %>?estado=Trafico&accion=FiltroZona&linea="+linea+"&conf="+conf+"&clas="+clas+"&var2="+var2+"&var1="+var1+"&var3="+var3+"&filtro="+filtro+"&zonasUsuario="+zona; 
}

function resetCombo(){
	/*var cmb=document.getElementById("zonasUsuario");
	for (i=0; i<cmb.length;i++){	
		cmb[i].selected=false;
	}*/
}

function procesar (valor){	
	if (window.event.keyCode==13){
		recorrido(valor);			
	}
}
	
function procesar2 (valor){
	if (window.event.keyCode==13){
		recorridoPlaca(valor);			
	}
}

function procesar3 (valor){		
	if (window.event.keyCode==13){
		recorridoConductor(valor);			
	}
}
	
function recorrido(planilla){
	planilla=planilla.toUpperCase();  
	var filaO;
	var idFila;
	var sw=1; 
	var i=1;
	while( (i < tabla.rows.length) && (sw==1)){
		filaO= tabla.rows[i];
		idFila=filaO.id;
		var pos = idFila.indexOf("~");
		idFila=idFila.substr(0,pos);
		if (idFila == planilla){
			sw=2;
		}
		i++;
	}
	if(sw==2){	
		var fila = filaO.cloneNode(true);
		fila = tabla.insertRow(1); 
		
		for(var x=0;x< filaO.cells.length;x++){
			var celda1 = filaO.cells[x];			
			var celda =celda1.cloneNode(true); 
			//alert(celda.onContextMenu);
			celda =fila.insertCell();
			celda.abbr = celda1.abbr;
			celda.align = celda1.align ;
			celda.innerHTML= celda1.innerHTML;
			
			//celda.onContextMenu=celda1.onContextMenu;
			
		}
		//alert(filaO.onmouseover);
		var duracion = document.getElementById("duracion"+idFila);
		fila.onmouseover=filaO.onmouseover;
		fila.name = filaO.name;
		var funcion =filaO.oncontextmenu;
		//alert(funcion);
		fila.oncontextmenu=funcion;
//		var bSuccess =fila.attachEvent('oncontextmenu',funcion);
		//fila.onContextMenu=;
		fila.bgColor=filaO.bgColor;
		fila.border="1";
		fila.borderColor="#E6E6E6"; 
		fila.abbr=filaO.abbr;
		fila.id=filaO.id;		
		fila.style.cursor="hand";
		fila.className ="letraTitulo";
		//alert(fila.oncontextmenu);
		tabla.deleteRow(i);
		var duracion = document.getElementById("duracion"+idFila);
		
	}
	else{
		alert("No existe esa Planilla");
	}       
	var nPlanilla=document.getElementById("nPlanilla");
	nPlanilla.value="";
}//Fin de la funcion Recorrido
   
   function ola(){
   	alert('HOLA');
	return false;
   }
function recorridoPlaca(placa){

	placa=placa.toUpperCase();  
	var filaO;
	var idFila;
	var sw=1; 		
    var i=1;
	while( (i < tabla.rows.length)){
		filaO= tabla.rows[i];
		idFila=filaO.id;
		var pos = idFila.indexOf("~");
		var pos2=idFila.lastIndexOf("~");
		idFila=idFila.substr((pos+1),pos2-pos-1);
		if (idFila.indexOf(placa)==0){
			var fila = filaO.cloneNode(true);
			fila = tabla.insertRow(1); 
			for(var x=0;x< filaO.cells.length;x++){
				var celda1 = filaO.cells[x];
				var celda = fila.insertCell();
				celda.abbr = celda1.abbr;
				celda.align = celda1.align ;
				celda.innerHTML= celda1.innerHTML;
				if ( x == (filaO.cells.length-1)){}		
			}
			fila.onmouseover=filaO.onmouseover;
			fila.bgColor=filaO.bgColor;
			fila.border="1";
			fila.borderColor="#E6E6E6"; 
			fila.abbr=filaO.abbr;
			fila.id=filaO.id;
			fila.style.cursor="hand";
			fila.className ="letraTitulo";
			var funcion =filaO.oncontextmenu;
			//alert(funcion);
			fila.oncontextmenu=funcion;
			//tabla.deleteRow(i);
			var filaBorrar = i+1;
			//alert("Se va a borrar la fila "+filaBorrar);
			tabla.deleteRow(filaBorrar);

			
		}
		i++;
	}
	     
	 var nPlaca=document.getElementById("nPlaca");
	 nPlaca.value="";
}//Fin de la funcion Recorrido
   
function cadenaSinEspacios(cadena){
	var cad="";
	for(var i=0; i<cadena.length; i++ ){
		var c = cadena.charAt(i);
		if ( c != ' ' ){
			cad += c;
		}
	}
	return cad;
}

function recorridoConductor(placa){
	placa=placa.toUpperCase();  
	placa=cadenaSinEspacios(placa);
	var filaO;
	var idFila;
	var sw=1; 
    var i=1;
	while( (i < tabla.rows.length) ){
	    filaO= tabla.rows[i];
	    idFila=filaO.id;
		idFila=cadenaSinEspacios(idFila);
		var pos = idFila.lastIndexOf("~");
		idFila=idFila.substr((pos+1),idFila.length);

		if (idFila.indexOf(placa)>=0){
			//alert("Se encontro en la fila "+i);
			sw=2;
			var fila = filaO.cloneNode(true);
			fila = tabla.insertRow(1); 
			for(var x=0;x< filaO.cells.length;x++){
				var celda1 = filaO.cells[x];
				var celda = fila.insertCell();
				celda.abbr = celda1.abbr;
				celda.align = celda1.align ;
				celda.innerHTML= celda1.innerHTML;
			}
			fila.onmouseover=filaO.onmouseover;
			fila.bgColor=filaO.bgColor;
			fila.border="1";
			fila.borderColor="#E6E6E6"; 
			fila.abbr=filaO.abbr;
			fila.id=filaO.id;
			fila.style.cursor="hand";
			fila.className ="letraTitulo";
			var funcion =filaO.oncontextmenu;
			fila.oncontextmenu=funcion;
			var filaBorrar = i+1;
			//alert("Se va a borrar la fila "+filaBorrar);
			tabla.deleteRow(filaBorrar);

		}
		i++;
    }
	/*if(sw==2){        
		
	}*/
	if(sw!=2){
		alert("No se encontro el conductor");
	} 
	var nPlaca=document.getElementById("nPlaca");
	nPlaca.value="";
}//Fin de la funcion Recorrido
</script>

<STYLE>
#ie5menu {
	BORDER-RIGHT: #003399 1px solid; BORDER-TOP: #003399 1px solid; VISIBILITY: hidden; FONT: 11px Verdana; BORDER-LEFT: #003399 1px solid; WIDTH: 250px; CURSOR: hand; BORDER-BOTTOM: #003399 1px solid; POSITION: absolute; BACKGROUND-COLOR: #D1DCEB
}
#iemenuManual {
	BORDER-RIGHT: #003399 1px solid; BORDER-TOP: #003399 1px solid; VISIBILITY: hidden; FONT: 11px Verdana; BORDER-LEFT: #003399 1px solid; WIDTH: 250px; CURSOR: hand; BORDER-BOTTOM: #003399 1px solid; POSITION: absolute; BACKGROUND-COLOR: #D1DCEB
}
#ie4menu {
	BORDER-RIGHT: #003399 1px solid; BORDER-TOP: #003399 1px solid; VISIBILITY: hidden; FONT: 11px Verdana; BORDER-LEFT: #003399 1px solid; WIDTH: 250px; CURSOR: hand; BORDER-BOTTOM: #003399 1px solid; POSITION: absolute; BACKGROUND-COLOR: #D1DCEB
}
#ie3menu {
	BORDER-RIGHT: #003399 1px solid; BORDER-TOP: #003399 1px solid; VISIBILITY: hidden; FONT: 11px Verdana; BORDER-LEFT: #003399 1px solid; WIDTH: 250px; CURSOR: hand; BORDER-BOTTOM: #003399 1px solid; POSITION: absolute; BACKGROUND-COLOR: #D1DCEB
}
#ie2menu {
	BORDER-RIGHT: #003399 1px solid; BORDER-TOP: #003399 1px solid; VISIBILITY: hidden; FONT: 11px Verdana; BORDER-LEFT: #003399 1px solid; WIDTH: 250px; CURSOR: hand; BORDER-BOTTOM: #003399 1px solid; POSITION: absolute; BACKGROUND-COLOR: #D1DCEB
}
.menuitems {
	PADDING-RIGHT: 15px; PADDING-LEFT: 15px; CURSOR: hand; FONT: 11px Arial; font-weight:bold; 
}
</STYLE>

<SCRIPT language=JavaScript1.2>
var display_url=0
function mostrarMenu (v,cedula,fechaSalida,celular,tipoDespacho){
	//alert(tipoDespacho);
	//alert(v);
	hidemenuie5();
	hidemenuie5Manual();
	var ctipoDespacho = tipoDespacho;
	//alert(ctipoDespacho.value );
	if(ctipoDespacho !="M"){
		showmenuie5();
	}
	else if(ctipoDespacho =="M"){
		showmenuie5Manual();
	}
	valor(v,cedula,fechaSalida,celular,tipoDespacho);
	return false
}
function showmenuie5(){ 
	var ctipoDespacho = document.getElementById('tipoDespacho');
	//alert(ctipoDespacho.value );
	
	//var c_titulo = document.getElementById("c_titulo");
	//alert("tipo de Despacho "+ctipoDespacho.value+" titulo "+c_titulo.value);
	//if(ctipoDespacho.value !="M"){//(c_titulo.value=="I") &&(
		//ie3menu.style.visibility="hidden"
		//ie4menu.style.visibility="hidden"
		//ie2menu.style.visibility="hidden"
		var rightedge=document.body.clientWidth-event.clientX
		var bottomedge=document.body.clientHeight-event.clientY
				
		if (rightedge<ie5menu.offsetWidth)
			ie5menu.style.left=document.body.scrollLeft+event.clientX-ie5menu.offsetWidth
		else
			ie5menu.style.left=document.body.scrollLeft+event.clientX
		
		if (bottomedge<ie5menu.offsetHeight)
			ie5menu.style.top=document.body.scrollTop+event.clientY-ie5menu.offsetHeight
		else
			ie5menu.style.top=document.body.scrollTop+event.clientY
		
		ie5menu.style.visibility="visible"
	//}
	return false
}


function hidemenuie5(){
	ie5menu.style.visibility="hidden"
}

function highlightie5(){
	if (event.srcElement.className=="menuitems"){
		event.srcElement.style.backgroundColor="#FFFFFF"
		event.srcElement.style.color="white"
		if (display_url==1)
			window.status=event.srcElement.url
	}
}

function lowlightie5(){
	if (event.srcElement.className=="menuitems"){
		event.srcElement.style.backgroundColor=""
		event.srcElement.style.color="black"
		window.status=''
	}
}

function jumptoie5(){
	if (event.srcElement.className=="menuitems")
		window.location=event.srcElement.url
}
//MENU MANUAL
//ING. KAREN REALES

function showmenuie5Manual(){ 
 	//alert ("entre a mostrar menu manual"); 
	var rightedge=document.body.clientWidth-event.clientX;
	var bottomedge=document.body.clientHeight-event.clientY;

		if (rightedge<iemenuManual.offsetWidth){
			iemenuManual.style.left=document.body.scrollLeft+event.clientX-iemenuManual.offsetWidth;
			//alert ("entre en el primer if");
		}
		else{
			iemenuManual.style.left=document.body.scrollLeft+event.clientX;
			//alert ("entre en el primer else");
		}
		
		if (bottomedge<iemenuManual.offsetHeight){
			//alert ("entre en el segundo if");
			iemenuManual.style.top=document.body.scrollTop+event.clientY-iemenuManual.offsetHeight
		}
		else{
			//alert ("entre en el segundo else");
			iemenuManual.style.top=document.body.scrollTop+event.clientY
		}
		
		iemenuManual.style.visibility="visible"
		//alert("Menu: "+iemenuManual.style.visibility);
	return false
}


function hidemenuie5Manual(){
	iemenuManual.style.visibility="hidden"
}

function highlightie5Manual(){
	if (event.srcElement.className=="menuitems"){
		event.srcElement.style.backgroundColor="#FFFFFF"
		event.srcElement.style.color="white"
		if (display_url==1)
			window.status=event.srcElement.url
	}
}

function lowlightie5Manual(){
	if (event.srcElement.className=="menuitems"){
		event.srcElement.style.backgroundColor=""
		event.srcElement.style.color="black"
		window.status=''
	}
}

function jumptoie5Manual(){
	if (event.srcElement.className=="menuitems")
		window.location=event.srcElement.url
}
/*function valor1(valor){
	var ctipoDespacho = document.getElementById('tipoDespacho');
 	ctipoDespacho.value="I";
	//alert(ctipoDespacho.value);
	var c_titulo = document.getElementById("c_titulo");
	//var c_titulo = document.getElementById('c_titulo');
 	//alert(c_titulo);
	c_titulo.value=valor;
	//alert(c_titulo.value);
	/*c_titulo.value=valor;
	alert(c_titulo.value);*/	
//}

function valor(v,cedula,fechaSalida,celular,tipoDespacho){
	//alert(cedula);/*
	var ccedula = document.getElementById('cedula');
 	ccedula.value=cedula;
	//alert(ccedula.value);
	var fechaS = document.getElementById('fechaS');
 	fechaS.value=fechaSalida;
	
	var vcelular = document.getElementById('celular');
 	vcelular.value=celular;
	
	var ctipoDespacho = document.getElementById('tipoDespacho');
 	ctipoDespacho.value=tipoDespacho;
	
	var c_titulo = document.getElementById("c_titulo");
 	c_titulo.value="I";
	
	//alert(vcelular.value);
  	var pos = v.indexOf("~");
	v=v.substr(0,pos);
	
	var cz = document.getElementById("cz");
 	cz.value= "" + v;
  	//alert(cz.value);
}

/*function tipoAccionOrdenar(){
	var c_titulo = document.getElementById("c_titulo");
	document.forma1.action = "<%=CONTROLLER%>?estado=Trafico&accion=Clasificacion&var4="+c_titulo.value+"&orden=ASC"; 
    document.forma1.submit(); 
}

function tipoAccionOcultar(){
	var c_titulo = document.getElementById("c_titulo");
	//alert(c_titulo.value);
	document.forma1.action = "<%=CONTROLLER%>?estado=Trafico&accion=Configuracion&nombre="+c_titulo.value+""; 
    document.forma1.submit(); 
} */

function tipoAccion(valor){

	var cedula  = document.getElementById('cedula');
	var vcedula = ""+cedula.value;
	
	var fechaS  = document.getElementById('fechaS');
 	var vfechaS  =fechaS.value;
	
	var celular = document.getElementById('celular');
 	vcelular=celular.value;
	
	var cz = document.getElementById("cz");
	var dir = "<%=BASEURL%>/jsp/trafico/controltrafico/red1.jsp?cz="+cz.value+"&op="+valor+"&cedula="+vcedula+"&fechaS="+vfechaS+"&celular="+vcelular;
	var wdth = screen.width ;
	var hght = screen.height;
	var lf = 0;
	var tp = 0;
	window.open(dir,'','scrollbars=no,status=yes,resizable=yes,top=' + tp + ',left=' + lf + ',width=' +
          wdth + ',height=' + hght);
}
</script>



<DIV id=ie5menu onmouseover=highlightie5() onclick=jumptoie5() onmouseout=lowlightie5()>
<table width="250" border="0" cellpadding="0" cellspacing="1">
  <tr onClick="tipoAccion('2');" class="menuitems">
    <td><a  class="Simulacion_Hiper" >Registar Movimiento Trafico</a></td>
  </tr>
  <tr onClick="tipoAccion('9');" class="menuitems">
    <td><a   class="Simulacion_Hiper">Registar Demora Planilla</a></td>
  </tr>
  <tr onClick="tipoAccion('12');" class="menuitems">
    <td><a  class="Simulacion_Hiper" >Registar Observaci�n</a></td>
  </tr>
  <tr onClick="tipoAccion('13');" class="menuitems">
    <td><a  class="Simulacion_Hiper">Servicios de Escolta</a></td>
  </tr>
  <tr onClick="tipoAccion('14');" class="menuitems">
    <td><a  class="Simulacion_Hiper">Conformacion de Caravanas</a></td>
  </tr>
  <tr onClick="tipoAccion('15');" class="menuitems">
    <td><a class="Simulacion_Hiper">Registrar sanciones</a></td>
  </tr>
  <tr onClick="tipoAccion('6');" class="menuitems">
    <td><a  class="Simulacion_Hiper" >Cambiar Zona</a></td>
  </tr>
  <tr onClick="tipoAccion('6');" class="menuitems">
    <td><a  class="Simulacion_Hiper" >Cambiar Ruta</a></td>
  </tr>
  <tr onClick="tipoAccion('10');" class="menuitems">
    <td><a  class="Simulacion_Hiper">Cambiar Celular Conductor</a></td>
  </tr>
  <tr onClick="tipoAccion('11');" class="menuitems">
    <td><a  class="Simulacion_Hiper">Cambiar Hora de Salida</a></td>
  </tr>
  <tr onClick="tipoAccion('18');" class="menuitems">
    <td><a  class="Simulacion_Hiper">Consultar / Modificar Tramos</a></td>
  </tr>
  <tr onClick="tipoAccion('19');" class="menuitems">
    <td><a  class="Simulacion_Hiper">Modificar Vias</a></td>
  </tr>
  <tr onClick="tipoAccion('22');" class="menuitems">
    <td><a  class="Simulacion_Hiper">Cambiar Placa o Conductor</a></td>
  
  <tr>
    <td><HR></td>
  </tr>
  <tr onClick="tipoAccion('1');" class="menuitems">
    <td><a  class="Simulacion_Hiper">Movimiento Trafico</a></td>
  </tr>
  <tr onClick="tipoAccion('3');" class="menuitems">
    <td><a  class="Simulacion_Hiper">Planilla</a></td>
  </tr>
  <tr onClick="tipoAccion('4');" class="menuitems">
    <td><a  class="Simulacion_Hiper">Plan de Viaje</a></td>
  </tr>
  <tr onClick="tipoAccion('5');" class="menuitems">
    <td><a class="Simulacion_Hiper">Hoja de Vida</a></td>
  </tr>
  <tr onClick="tipoAccion('7');" class="menuitems">
    <td><a  class="Simulacion_Hiper">Caravana</a></td>
  </tr>
  <tr onClick="tipoAccion('8');" class="menuitems">
    <td><a  class="Simulacion_Hiper">Escolta</a></td>
  </tr>
  <tr onClick="tipoAccion('16');" class="menuitems">
    <td><a  class="Simulacion_Hiper">Consultas</a></td>
  </tr>
  <tr onClick="tipoAccion('23');" class="menuitems">
    <td><a  class="Simulacion_Hiper">Trafico de Planillas</a></td>
  </tr>
   <tr onClick="tipoAccion('24');" class="menuitems">
    <td><a  class="Simulacion_Hiper">Cambiar de Color la Fila</a></td>
  </tr>
</table>
</div>

<DIV id=iemenuManual onmouseover=highlightie5Manual() onclick=jumptoie5Manual() onmouseout=lowlightie5Manual()>
<table width="250" border="0" cellpadding="0" cellspacing="1">
  <tr class="menuitems">
    <td>&nbsp;</td>
  </tr>
  <tr onClick="tipoAccion('2');" class="menuitems">
    <td><a  class="Simulacion_Hiper" >Registar Movimiento Trafico</a></td>
  </tr>
    <tr onClick="tipoAccion('9');" class="menuitems">
    <td><a   class="Simulacion_Hiper">Registar Demora Planilla</a></td>
  </tr>

   <tr onClick="tipoAccion('12');" class="menuitems">
    <td><a  class="Simulacion_Hiper" >Registar Observaci�n</a></td>
  </tr>
  <tr>
  <tr onClick="tipoAccion('20');" class="menuitems">
    <td><a  class="Simulacion_Hiper">Servicios de Escolta</a></td>
  </tr>
  <tr onClick="tipoAccion('21');" class="menuitems">
    <td><a class="Simulacion_Hiper">Conformacion de Caravanas</a></td>
  </tr>
   <tr onClick="tipoAccion('6');" class="menuitems">
    <td><a class="Simulacion_Hiper" >Cambiar Zona</a></td>
  </tr>
  <tr onClick="tipoAccion('10');" class="menuitems">
    <td><a class="Simulacion_Hiper">Cambiar Celular Conductor</a></td>
  </tr>
  <tr onClick="tipoAccion('11');" class="menuitems">
    <td><a  class="Simulacion_Hiper">Cambiar Hora de Salida</a></td>
  </tr>
  <tr onClick="tipoAccion('17');" class="menuitems">
    <td><a  class="Simulacion_Hiper">Cambiar Despacho Manual a Original</a></td>
  </tr>
   <tr onClick="tipoAccion('22');" class="menuitems">
    <td><a  class="Simulacion_Hiper">Cambiar Placa o Conductor</a></td>
  </tr>
  <tr>
    <td><HR></td>
  </tr>
  <tr onClick="tipoAccion('1');" class="menuitems">
    <td><a  class="Simulacion_Hiper">Movimiento Trafico</a></td>
  </tr>
  <tr onClick="tipoAccion('23');" class="menuitems">
    <td><a  class="Simulacion_Hiper">Trafico de Planillas</a></td>
  </tr>
   <tr onClick="tipoAccion('24');" class="menuitems">
    <td><a  class="Simulacion_Hiper">Cambiar de Color la Fila</a></td>
  </tr>
  <tr class="menuitems">
    <td>&nbsp;</td>
  </tr>
 
</table>
</div>
</html>

