<%@page errorPage="/error/ErrorPage.jsp"%>
<%@page import="java.*" %>
<%@page import="java.util.*" %>
<%@include file="/WEB-INF/InitModel.jsp"%>
<%@page import="com.tsp.operation.model.beans.*, java.util.*"%>
<%@page import="com.tsp.util.*"%>
<%@taglib uri="http://jsptags.com/tags/navigation/pager" prefix="pg" %>
<%
  Vector datos = model.rptNumeroMovSVC.getVectorRpts();
  String style = "simple";
  String position =  "bottom";
  String index =  "center";
  int maxPageItems = 10;
  int maxIndexPages = 10;  
 String dst =  (request.getParameter("distrito")!=null)?request.getParameter("distrito"):""; 
 String fecI =  (request.getParameter("fecini")!=null)?request.getParameter("fecini"):""; 
 String fecF =  (request.getParameter("fecfin")!=null)?request.getParameter("fecfin"):""; 
 String men = "";
 String arc =  (request.getParameter("arc")!=null)?request.getParameter("arc"):""; 
  if(!arc.equals(""))
      men = "Archivo Generado";
%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<title>REPORTE DE NUMERO DE MOVIMIENTOS DE UNA PLANILLA</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<script type='text/javascript' src="<%= BASEURL %>/js/reporte.js"></script>
<link href="../../../css/estilostsp.css" rel="stylesheet" type="text/css">
<link href="<%=BASEURL%>/css/estilostsp.css" rel="stylesheet" type="text/css">
<script type='text/javascript' src="<%=BASEURL%>/js/reporte.js"></script>
</head>

<body>

<table border="2" align="center" width="997">
  <tr>
    <td>
	<table width="100%" align="center">
  <tr>
    <td width="50%"  class="subtitulo1"><p align="left">REPORTE DE CONTROL DE CHEQUES ANTICIPOS</p></td>
    <td width="50%"  class="barratitulo"><img src="<%=BASEURL%>/images/titulo.gif" width="32" height="20"></td>
  </tr>
</table>
  <table width="99%" border="1" align="center" bordercolor="#999999" bgcolor="#F7F5F4" class="fondotabla">
    <!--DWLayoutTable-->
    <tr class="tblTitulo">
      <td width="62" height="21" > <span class="letra_resaltada">BANCO</span></td>
      <td width="86" >SUCURSAL</td>
      <td width="110" ><span class="letra_resaltada">PROVEEDOR A PAGAR </span></td>
      <td width="70" > <span class="letra_resaltada">FACTURA ORIGINAL </span></td>
      <td width="69" class="letra_resaltada" >CORRIDA CHEQUE </td>
      <td width="64" class="letra_resaltada" >CHEQUE No.</td>
      <td width="74" class="letra_resaltada" >VLR. NETO </td>
      <td width="52" class="letra_resaltada" >PLACA</td>
      <td width="140" class="letra_resaltada" >PROVEEDOR</td>
      <td width="70" class="letra_resaltada" >FACTURA</td>
      <td width="108" class="letra_resaltada" >MOVIMIENTOS </td>
    </tr>
    <pg:pager
    items="<%=datos.size()%>"
    index="<%= index %>"
    maxPageItems="<%= maxPageItems %>"
    maxIndexPages="<%= maxIndexPages %>"
    isOffset="<%= true %>"
    export="offset,currentPageNumber=pageNumber"
    scope="request">
    <%
	  for (int j = offset.intValue(), l = Math.min(j + maxPageItems, datos.size()); j < l; j++){
              NumeroRptControlOracle rpt = (NumeroRptControlOracle) datos.elementAt(j);
    %>
    <pg:item>
    <tr class="<%=(j % 2 == 0 )?"filagris":"filaazul"%>" onMouseOver='cambiarColorMouse(this)' style="cursor:hand">
      <td width="62" height="17" class="bordereporte"> <%=rpt.getBRANCH_CODE()%></td>
      <td width="86" class="bordereporte"><%=rpt.getBANCK_ACCT_NO()%></td>
      <td width="110" class="bordereporte"><%=rpt.getSUPP_TO_PAY()%></td>
      <td width="70" class="bordereporte"><%=rpt.getEXT_INV_NO()%> </td>
      <td width="69" class="bordereporte"><%=rpt.getCHEQUE_RUN_NO()%></td>
      <td width="64" class="bordereporte"><%=rpt.getCHEQUE_NO()%></td>
      <td width="74" class="bordereporte"><%=Util.customFormat(Double.parseDouble(rpt.getLOC_INV_ORIG()))%></td>
      <td width="52" class="bordereporte"><%=rpt.getSUPPLIER_NO()%></td>
      <td width="140" class="bordereporte"><%=rpt.getSUPPLIER_NAME()%></td>
      <td width="70" class="bordereporte"><div align="center"><%=rpt.getINV_NO()%></div></td>
	  <%String mov = (rpt.getNumMov()!=null)?rpt.getNumMov():"Ninguno";%>
      <td width="108" class="bordereporte"><div align="center"><%=mov%></div></td>
    </pg:item>
    <%}%>
    <tr class="filagris">
      <td height="20" colspan="12" align="center" nowraptd><a href="javascript:window.location.href='<%=CONTROLLER%>?estado=Rptnumero&accion=Control&opc=generar&distrito=<%=dst%>&fecfin=<%=fecF%>&fecini=<%=fecI%>'">
	  <img src="<%=BASEURL%>/images/botones/iconos/excel.gif" width="18" height="18" border="0"></a></td>
    </tr>
	<%if(!men.equals("")){%><tr class="filagris">
      <td height="20" colspan="12" align="center" nowrap><%=men%></td>
    </tr><%}%>
    <tr class="filagris">
      <td td height="20" colspan="12" nowrap align="center"> <pg:index>
      <jsp:include page="/WEB-INF/jsp/googlesinimg.jsp" flush="true"/>      
</pg:index> </td>
    </tr>
    </pg:pager>
  </table>
  </td>
  </tr>
</table>
</body>
</html>
