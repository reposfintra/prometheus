<%@ page contentType="text/html; charset=iso-8859-1" language="java" errorPage="" %>
<%@page import="com.tsp.operation.model.beans.*, java.util.*"%>
<%@include file="/WEB-INF/InitModel.jsp" %>
<%
  //String noCaravana =  (request.getParameter("var")!=null)?request.getParameter("var"):""; 
%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<title>REPORTE DE CONTROL DE CHEQUES</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<script type='text/javascript' src="<%=BASEURL%>/js/validar.js"></script>
<script type='text/javascript' src="<%=BASEURL%>/js/boton.js"></script>

<script type='text/javascript' src="<%=BASEURL%>/js/date-picker.js"></script>
<link href="../../../css/estilostsp.css" rel="stylesheet" type="text/css">
<link href="<%=BASEURL%>/css/estilostsp.css" rel="stylesheet" type="text/css">
</head>

<body>
<form name="buscar" method="post" id="busq"  action="<%= CONTROLLER %>?estado=Rptnumero&accion=Control">
<table border="2" align="center" width="504">
  <tr>
    <td>
	<table width="100%" align="center">
  <tr>
    <td width="58%"  class="subtitulo1"><p align="left">Reporte deControl de Cheques Anticipos </p></td>
    <td width="42%"  class="barratitulo"><img src="<%=BASEURL%>/images/titulo.gif" width="32" height="20"></td>
  </tr>
</table>
  <table width="99%" align="center">
    <tr class="fila">
      <td colspan="2"><div align="center" class="letra_resaltada">DISTRITO</div></td>
      <td colspan="2"><div align="left"><span class="letras"><span class="comentario">
          <select name="distrito" id="distrito" class="listmenu">
            <option value="FINV" selected>FINV</option>
            <option value="VEN">VEN</option>
          </select>
          </span></span></div></td>
    </tr>
    <tr class="fila">
      <td width="129" height="28"><div align="center" class="letra_resaltada">FECHA INICIAL DE SUBIDA.</div></td>
      <td width="120"><span class="comentario">
        <input name='fecini' type='text' class="textbox" id="fecini" size="13" readonly>
        <a href="javascript:void(0)" onclick="jscript: show_calendar('fecini');" HIDEFOCUS><img src="<%=BASEURL%>/images/cal.gif" alt="" name="popcal" width="16" height="16" border="0" align="top"></a></span></td>
      <td width="116"><div align="center" class="letra_resaltada">FECHA FIN AL DE SUBIDA.</div></td>
      <td width="131"><span class="comentario">
        <input name='fecfin' type='text' class="textbox" id="fecfin" size="13" readonly>
        <a href="javascript:void(0)" onclick="jscript: show_calendar('fecfin');" HIDEFOCUS><img src="<%=BASEURL%>/images/cal.gif" alt="" name="popcal" width="16" height="16" border="0" align="top"></a></span></td>
    </tr>
  </table>
  </td>
  </tr>
  </table>
  <div align="center"><br>
    <img src="<%=BASEURL%>/images/botones/aceptar.gif" name="mod"  height="21" onClick="validarRptMov(buscar);" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" style="cursor:hand">&nbsp;
	<img src="<%=BASEURL%>/images/botones/salir.gif"  name="imgsalir"  height="21" onMouseOver="botonOver(this);" onClick="parent.close();" onMouseOut="botonOut(this);" style="cursor:hand">
  </div>
</form>
</body>
</html>

