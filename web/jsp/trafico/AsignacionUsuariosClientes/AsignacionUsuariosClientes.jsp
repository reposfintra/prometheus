<%@page session="true"%>
<%@page import="java.util.*" %>
<%@page import="com.tsp.operation.model.beans.*" %>
<%@page errorPage="../error/error.jsp"%>
<%@include file="/WEB-INF/InitModel.jsp"%>
<%@include file="/jsp/trafico/AsignacionUsuariosClientes/datosProyectosUsuarios.jsp"%>
<%
   String Perfil = model.AsigUsuCliSvc.getPeril();
   String Tipo   = model.AsigUsuCliSvc.getTipoRelacion();
%>
<html>
<head>
<title>Asigancion de Usuarios Clientes</title>
<link href="<%= BASEURL %>/css/estilostsp.css" rel="stylesheet">
<script>

        
        <% showDatosJs (model.AsigUsuCliSvc.getListaPerfiles(), out, "datosP"); %>
        <% showDatosJs (model.AsigUsuCliSvc.getListaTipos()   , out, "datosT"); %>
        <% showDatosJs (model.AsigUsuCliSvc.getListaUsuarios(), out, "datosU"); %>
        <% showDatosJs (model.AsigUsuCliSvc.getListaClientes(), out, "datosC"); %>
        
        <% showDatosJs (model.AsigUsuCliSvc.getListaPerfilClientes(), out, "datosPC"); %>
        <% showDatosJs (model.AsigUsuCliSvc.getListaPerfilUsuarios(), out, "datosPU"); %>
        
        var separador = '~';
        
        function addOption(Comb,valor,texto){
            var Ele = document.createElement("OPTION");
            Ele.value=valor;
            Ele.text=texto;
            Comb.add(Ele);
        }   
 
        function loadCombo(datos, cmb){
            cmb.length = 0;
            for (i=0;i<datos.length;i++){
                var dat = (new String(datos[i])).split(separador);
                addOption(cmb, dat[0], dat[1]);
            }
        }
        
        function loadCombo2(datosG, datosA , cmbG, cmbA){
            var i = 0;
            cmbG.length = 0;
            cmbA.length = 0;
            for (i=0;i<datosG.length;i++){
                var dat = (new String(datosG[i])).split(separador);
                if ( !buscar(datosA, dat[0])  )
                   addOption(cmbG, dat[0], dat[1]);
                else
                   addOption(cmbA, dat[0], dat[1]);
            }
        } 
        
        function buscar(datos, item){
            var i = 0;
            for (i=0;i<datos.length;i++){
                var dat = (new String(datos[i])).split(separador);
                if (dat[1]==item)
                   return true;
            }
            return false;
        }
       
        
        function move(cmbO, cmbD){
           for (i=cmbO.length-1; i>=0 ;i--)
            if (cmbO[i].selected){
               addOption(cmbD, cmbO[i].value, cmbO[i].text)
               cmbO.remove(i);
            }
           //order(cmbD);
           //deleteRepeat(cmbD);
        }
        function moveAll(cmbO, cmbD){
           for (i=cmbO.length-1; i>=0 ;i--){
             if(cmbO[i].value!='' && cmbO[i].text!=''){
               addOption(cmbD, cmbO[i].value, cmbO[i].text)
               cmbO.remove(i);
             }
           }
           //order(cmbD);
           //deleteRepeat(cmbD);
        }
        
        
        function order(cmb){
           for (i=0; i<cmb.length;i++)
             for (j=i+1; j<cmb.length;j++)
                if (cmb[i].text > cmb[j].text){
                    var temp = document.createElement("OPTION");
                    temp.value = cmb[i].value;
                    temp.text  = cmb[i].text;
                    cmb[i].value = cmb[j].value;
                    cmb[i].text  = cmb[j].text;
                    cmb[j].value = temp.value;
                    cmb[j].text  = temp.text;
                }
        }
        
        function selectAll(cmb){
            for (i=0;i<cmb.length;i++)
              cmb[i].selected = true;
        }
        
        function deleteRepeat(cmb){
          var ant='';
          for (i=0;i<cmb.length;i++){
             if(ant== cmb[i].value)
                cmb.remove(i);
             ant = cmb[i].value;
          }
        }
        
        function validar(){
           for (i=0;i<form1.UsuariosA.length;i++)
              form1.UsuariosA[i].selected = true;
           for (i=0;i<form1.ClientesA.length;i++)
              form1.ClientesA[i].selected = true;
        }
        
        function goPerfil(perfil){
          var url = "<%= CONTROLLER %>?estado=Asig&accion=UsuariosClientes&Opcion=SeleccionarPerfil&Perfil=" + perfil;
          document.location.href = url;
        }
        
  
</script>
<script type='text/javascript' src="<%= BASEURL %>/js/boton.js"></script>
<link href="../../../css/estilostsp.css" rel="stylesheet" type="text/css">
</head>
<body onresize="redimensionar()" onload="redimensionar();">
<div id="capaSuperior" style="position:absolute; width:100%; height:100px; z-index:0; left: 0px; top: 0px;">
 <jsp:include page="/toptsp.jsp?encabezado=Asignacion Usuario - Cliente"/>
</div>
<div id="capaCentral" style="position:absolute; width:100%; height:87%; z-index:0; left: 0px; top: 100px; overflow: scroll;">
<center>
<form action="<%= CONTROLLER %>?estado=Asig&accion=UsuariosClientes" method="post" name="form1" onsubmit="return validar();">
<table width="600" border="2">
  <tr>
    <td><table width="600" class="tablaInferior">
  <tr>
    <th class="barratitulo" scope="col"><table width="100%"  border="0" class="barratitulo">
      <tr>
        <td width="51%" class="subtitulo1">ASIGNACION USUARIOS CLIENTES </td>
        <td width="49%"><img src="<%=BASEURL%>/images/titulo.gif" width="32" height="20"></td>
      </tr>
    </table>      </th>
  </tr>
  <tr>
    <td align="center">

	   
   <table width="100%" border="0" align="center" cellpadding="1" cellspacing="0" class="tablaInferior">
     <tr class="fila">
        <td width="21%" class="letra_resaltada" >Perfil</td>
        <td colspan="3"><select name="Perfil" class="textbox" id="Perfil" style="width:100%" onchange=" goPerfil(this.value); ">
        </select></td>
        <td width="14%" colspan="3" align="left">&nbsp; </td>
      </tr>	  
	  
      <tr class="fila">
        <td class="letra_resaltada" >Tipo de Relacion</td>
        <td colspan="3"><select name="Tipo" class="textbox" style="width:100%" onchange="">
        </select></td>
	<td colspan="3" align="left">&nbsp; </td>
      </tr>
      <tr class="fila">
        <td colspan="7" class="letra_resaltada" >&nbsp;</td>
        </tr>
      <tr  class='fila'><td colspan="7">
	  
	  <table width="100%"  border="1" bordercolor="#999999">
	  <tr bgcolor="#F7F5F4">
		<th class='tblTitulo' width="20%">Usuarios<br>
          Generales</th>
        <th width="8%" class="bordereporte fila">&nbsp;</th>
        <th class='tblTitulo' width="20%">Usuarios<br>a Procesar</th>
        <th width="4%" class="bordereporte fila">&nbsp;</th>
        <th class='tblTitulo' width="20%">Clientes<br>a Procesar</th>
        <th width="8%" class="bordereporte fila">&nbsp;</th>
        <th class='tblTitulo' width="20%">Clientes<br>Generales</th>
  </tr>
  <tr>
    <th class="bordereporte"><select name="UsuariosG" size="8" multiple class="textbox" style="width:100%">
        </select></th>
        <th class="bordereporte fila">
				<img src='<%=BASEURL%>/images/botones/envIzq.gif' style='cursor:hand' name='imgEnvIzq'  onclick="move   (UsuariosA, UsuariosG ); " onMouseOver='botonOver(this);' onMouseOut='botonOut(this);'><br>
                <img src='<%=BASEURL%>/images/botones/enviarIzq.gif' style='cursor:hand' name='imgTEnvIzq'  onclick="moveAll(UsuariosA, UsuariosG ); " onMouseOver='botonOver(this);' onMouseOut='botonOut(this);'><br>
				<img src='<%=BASEURL%>/images/botones/envDer.gif' style='cursor:hand'  name='imgEnvDer'  onclick="move   (UsuariosG, UsuariosA ); " onMouseOver='botonOver(this);' onMouseOut='botonOut(this);'><br>
                <img src='<%=BASEURL%>/images/botones/enviarDerecha.gif' style='cursor:hand' name='imgTEnvDer'  onclick="moveAll(UsuariosG, UsuariosA ); " onMouseOver='botonOver(this);' onMouseOut='botonOut(this);'><br>
		</th>
        <th class="bordereporte"><select name="UsuariosA" size="8" multiple class="textbox" style="width:100%">
        </select></th>
        <td class="bordereporte fila">&nbsp;</td>
        <th class="bordereporte"><select name="ClientesA"  size="8" multiple class="textbox" style="width:100%">
        </select></th>
        <th class="bordereporte fila">
				<img src='<%=BASEURL%>/images/botones/envIzq.gif' style='cursor:hand' name='imgEnvIzq'  onclick="move   (ClientesG, ClientesA );" onMouseOver='botonOver(this);' onMouseOut='botonOut(this);'><br>
                <img src='<%=BASEURL%>/images/botones/enviarIzq.gif' style='cursor:hand' name='imgTEnvIzq'  onclick="moveAll(ClientesG, ClientesA );" onMouseOver='botonOver(this);' onMouseOut='botonOut(this);'><br>
				<img src='<%=BASEURL%>/images/botones/envDer.gif' style='cursor:hand'  name='imgEnvDer'  onclick="move   (ClientesA, ClientesG );" onMouseOver='botonOver(this);' onMouseOut='botonOut(this);'><br>
                <img src='<%=BASEURL%>/images/botones/enviarDerecha.gif' style='cursor:hand' name='imgTEnvDer'  onclick="moveAll(ClientesA, ClientesG );" onMouseOver='botonOver(this);' onMouseOut='botonOut(this);'><br>
		</th>
        <th class="bordereporte"><select name="ClientesG" size="8" multiple class="textbox" style="width:100%">
        </select></th>
  </tr>
</table><br>
</td>
</tr>     
    </table>
  </td>
  </tr>
</table></td>
  </tr>
</table>
<br><input type="image" name="Submit" value="Ingresar" src="<%= BASEURL %>/images/botones/aceptar.gif" align="absmiddle" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);">
<input name="Opcion" type="hidden" class="boton" value="Grabar" style=" width:120;">
</form>

<script>
  loadCombo(datosT, form1.Tipo);
  loadCombo(datosP, form1.Perfil);
  loadCombo2(datosU, datosPU ,form1.UsuariosG, form1.UsuariosA);
  loadCombo2(datosC, datosPC ,form1.ClientesG, form1.ClientesA);
  form1.Perfil.value = '<%= Perfil %>';  
  form1.Tipo.value   = '<%= Tipo   %>'; 
</script>
</center>
</div>
</body>
</html>
