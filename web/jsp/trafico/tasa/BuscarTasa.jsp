<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
"http://www.w3.org/TR/html4/loose.dtd">
<%@page contentType="text/html"%>
<%@page session="true"%>
<%@page errorPage="../error/ErrorPage.jsp"%>
<%@page import="java.util.*"%>
<%@page import="com.tsp.operation.model.*"%>
<%@page import="com.tsp.util.*"%>
<%@page import="com.tsp.operation.model.beans.*, java.util.*"%>
<%@include file="/WEB-INF/InitModel.jsp"%>

<html>
<head>
<title>Buscar Icoterm</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link href="<%= BASEURL %>/css/estilostsp.css" rel="stylesheet" type="text/css">
<link href="../../../css/estilostsp.css" rel="stylesheet" type="text/css">  
<script type='text/javascript' src="<%= BASEURL %>/js/validar.js"></script>
<script type='text/javascript' src="<%= BASEURL %>/js/boton.js"></script>
</head>

<body>
<% String Titulo="Buscar Tasa";//BUSCAR PAIS
   String Subtitulo ="Informacion";
   String M = "Moneda";
   String fec = "Fecha";
   %>
<form name="forma" method="post" action="<%=CONTROLLER%>?estado=Tasa&accion=Buscarx&pagina=VerTasaX.jsp&carpeta=jsp/trafico/tasa">
  <table width="380" border="2" align="center">
    <tr>
      <td><table width="100%"  border="0">
        <tr>
          <td width="48%" class="subtitulo1"><%=Subtitulo%></td>
          <td width="52%" class="barratitulo"><img src="<%=BASEURL%>/images/titulo.gif" width="32" height="20"></td>
        </tr>
      </table>
      <table width="100%" class="tablaInferior">
        <tr class="fila">
          <td width="127"><%=M%> 1</td>
          <td width="210" ><input name="c_mon1" class="textbox" type="text" maxlength="40" >
&nbsp; </td>
        </tr>
        <tr class="fila">
          <td width="127" ><%=M%> 2 </td>
          <td width="210" ><input name="c_mon2" class="textbox" type="text" maxlength="40" >
&nbsp; </td>
        </tr>
        <tr class="fila">
          <td width="127" align="left"><%=fec%></td>
          <td width="210" class="comentario">
            <input  type="text"   name="c_fecha"  class="textbox" value="" READONLY>
           <span class="Letras"><a href="javascript:void(0)" onclick="jscript: show_calendar('c_fecha');" HIDEFOCUS> </a><img src="<%=BASEURL%>/images/botones/iconos/obligatorio.gif" name="obligatorio"> <img src="<%=BASEURL%>/images/cal.gif" width="16" height="16" align="absmiddle" style="cursor:hand " onclick="if(self.gfPop)gfPop.fPopCalendar(document.forma.c_fecha);return false;" HIDEFOCUS></span></td>
        </tr>
       
      </table></td>
    </tr>
  </table>
   <br>
         <div align="center">
			   <img src="<%=BASEURL%>/images/botones/buscar.gif"  name="imgbuscar" onClick="forma.submit()" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" style="cursor:hand">&nbsp;
			   <img src="<%=BASEURL%>/images/botones/detalles.gif"  name="imgdetalles"  onMouseOver="botonOver(this);" onClick="window.location='<%=CONTROLLER%>?estado=Todos&accion=VerTodos&carpeta=jsp/trafico/tasa&pagina=VerTasa.jsp'" onMouseOut="botonOut(this);" style="cursor:hand"> &nbsp;
               <img src="<%=BASEURL%>/images/botones/salir.gif"  name="imgsalir"  onMouseOver="botonOver(this);" onClick="parent.close();" onMouseOut="botonOut(this);" style="cursor:hand">
           </div>
</form>
<iframe width=188 height=166 name="gToday:normal:agenda.js:gfPop:plugins.js" id="gToday:normal:agenda.js:gfPop:plugins.js" src="<%=BASEURL%>/js/Calendario/ipopeng.htm" scrolling="no" frameborder="0" style="visibility:visible; z-index:999; position:absolute; left:-500px; top:0px;"> </iframe>    
</body>
</html>
