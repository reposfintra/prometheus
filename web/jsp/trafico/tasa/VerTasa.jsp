<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
"http://www.w3.org/TR/html4/loose.dtd">
<%@page contentType="text/html"%>
<%@page session="true"%>
<%@page errorPage="../error/ErrorPage.jsp"%>
<%@page import="java.util.*"%>
<%@page import="com.tsp.operation.model.*"%>
<%@page import="com.tsp.util.*"%>
<%@page import="com.tsp.operation.model.beans.*, java.util.*"%>
<%@include file="/WEB-INF/InitModel.jsp"%>
<%@taglib uri="http://jsptags.com/tags/navigation/pager" prefix="pg" %>

<html>
<head>
<title>Listado de Tasa</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link href="<%= BASEURL %>/css/estilostsp.css" rel="stylesheet" type="text/css"> 
<link href="../../../css/estilostsp.css" rel="stylesheet" type="text/css"> 
<script type='text/javascript' src="<%= BASEURL %>/js/reporte.js"></script>
<script type='text/javascript' src="<%= BASEURL %>/js/boton.js"></script>
</head>

<body>
  <% 
   String style = "simple";
   String position =  "bottom";
   String index =  "center";
   int maxPageItems = 10;
   int maxIndexPages = 10;
   
   model.tasaService.listarTasa();
   Vector Vectasa = model.tasaService.obtVecTasa();
   Tasa tasa;  
   Compania cia;
   Moneda m1,m2;

   System.out.println("Tama�o vec " + Vectasa.size());
   if ( Vectasa.size() > 0 ) { 
   %>
  <table width="800" border="2" align="center">
    <tr>
      <td><table width="99%" align="center" cellspacing="0">
        <tr>
          <td>
            <table width="100%" bgcolor="#FFFFFF" align="center">
              <tr>
                <td width="48%" class="subtitulo1">&nbsp;Datos Tasa </td>
                <td width="52%" class="barratitulo"><img src="<%=BASEURL%>/images/titulo.gif" width="32" height="20"> </td>
              </tr>
            </table>
            <table width="100%" border="1" align="center" bordercolor="#999999" bgcolor="#F7F5F4">
              <tr class="tblTitulo" align="center">
                <td width="16%">Compania</td>
                <td width="14%">Fecha</td>
                <td width="14%">Moneda 1</td>
                <td width="14%">Moneda 2</td>
                <td width="14%">Conversion</td>
                <td width="14%">Compra</td>
                <td width="14%">Venta</td>
              </tr>
          </table></td>
        </tr>
        <tr>
          <td> <pg:pager
    			items="<%=Vectasa.size()%>"
  				index="<%= index %>"
    			maxPageItems="<%= maxPageItems %>"
                maxIndexPages="<%= maxIndexPages %>"
                isOffset="<%= true %>"
                export="offset,currentPageNumber=pageNumber"
                scope="request">
              <div class="scroll" style=" overflow:auto ; WIDTH: 100%; HEIGHT:350px; " >
                <table width="100%"  border="1" align="center" bordercolor="#999999" bgcolor="#F7F5F4">
                  <% for (int i = offset.intValue(), l = Math.min(i + maxPageItems, Vectasa.size()); i < l; i++){
                     tasa = (Tasa) Vectasa.elementAt(i);
                     if( model.monedaService.existeMoneda( tasa.getMoneda1() ) && model.monedaService.existeMoneda( tasa.getMoneda2() ) ){ %>
                  <pg:item>
                  <tr class="<%=(i % 2 == 0 )?"filagris":"filaazul"%>" onMouseOver='cambiarColorMouse(this)'   style="cursor:hand"
			      onClick="window.open('<%=CONTROLLER%>?estado=Tasa&accion=Buscar&carpeta=jsp/trafico/tasa&pagina=Tasa.jsp&c_combom1=<%=tasa.getMoneda1()%>&c_combom2=<%=tasa.getMoneda2()%>&c_fecha=<%=tasa.getFecha()%>&mensaje=','','status=no,scrollbars=no,width=620,height=450,resizable=yes')">
                    <td width="16%" height="22" class="bordereporte"><%=tasa.getCia()%></td>
                    <td width="14%" class="bordereporte"><%=tasa.getFecha()%></td>
                    <%model.monedaService.buscarMonedaxCodigo(tasa.getMoneda1());
                                  m1 = model.monedaService.getMoneda();%>
                    <td width="14%" class="bordereporte"><%=m1.getNomMoneda()%></td>
                    <%model.monedaService.buscarMonedaxCodigo(tasa.getMoneda2());
                                  m2 = model.monedaService.getMoneda();%>
                    <td width="14%" class="bordereporte"><%=m2.getNomMoneda()%></td>
                    <td width="14%" class="bordereporte"><%=tasa.getVlr_conver()%></td>
                    <td width="14%" class="bordereporte"><%=tasa.getCompra()%></td>
                    <td width="14%" class="bordereporte"><%=tasa.getVenta()%></td>
                  </tr>
                  </pg:item>
                  <%}
			    }%>
                </table>
            </div></td>
        </tr>
        <tr class="letra" align="center">
          <td height="20" colspan="10" nowrap> <pg:index>
            <jsp:include page="../../../WEB-INF/jsp/googlesinimg.jsp" flush="true"/>      
    </pg:index> </td>
        </tr>
  </pg:pager>
  
      </table></td>
    </tr>
  </table>
  <p>&nbsp;</p>
  <%}
 else { %>
  <table border="2" align="center">
    <tr>
      <td><table width="410" height="40" border="1" align="center"  bordercolor="#F7F5F4" bgcolor="#FFFFFF">
          <tr>
            <td width="282" align="center" class="mensajes">Su b&uacute;squeda no arroj&oacute; resultados!</td>
            <td width="28" background="<%=BASEURL%>/images/cuadronaranja.JPG">&nbsp;</td>
            <td width="78">&nbsp;</td>
          </tr>
      </table></td>
    </tr>
  </table>
  <p>
      <%}%>
</p>
  <table width="800" border="0" align="center">
    <tr>
      <td><img src="<%=BASEURL%>/images/botones/regresar.gif"  name="imgaceptar" onclick="window.location='<%=CONTROLLER%>?estado=Menu&accion=Cargar&carpeta=/jsp/trafico/tasa&pagina=BuscarTasa.jsp&titulo=Buscar Tasa'" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" style="cursor:hand"></td>
    </tr>
  </table>
</body>
</html>
