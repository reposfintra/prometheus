<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
"http://www.w3.org/TR/html4/loose.dtd">
<%@page contentType="text/html"%>
<%@page session="true"%>
<%@page errorPage="../error/ErrorPage.jsp"%>
<%@page import="java.util.*"%>
<%@page import="com.tsp.operation.model.*"%>
<%@page import="com.tsp.util.*"%>
<%@page import="com.tsp.operation.model.beans.*, java.util.*"%>
<%@include file="/WEB-INF/InitModel.jsp"%>

<html>
<head>
<title>TASA</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link href="<%= BASEURL %>/css/estilostsp.css" rel="stylesheet" type="text/css">
<link href="../../../css/estilostsp.css" rel="stylesheet" type="text/css"> 
<script type='text/javascript' src="<%= BASEURL %>/js/validar.js"></script>
<script type='text/javascript' src="<%= BASEURL %>/js/boton.js"></script>
</head>

<body <%if(request.getParameter("reload")!=null){%>onLoad="parent.opener.location.reload();"<%}%>>
<% 
   Vector VecMon = model.monedaSvc.listarMonedas();
   Moneda moneda,mone;
   Tasa tasa;
   Vector VecM = null;
 
   int i=0,swcon=-1;
  
   String mensaje =(request.getParameter("mensaje")!=null)?request.getParameter("mensaje"):"";
   String codmon1 = (request.getParameter("codmon1")!=null)?request.getParameter("codmon1"):"";
   
   String cod="", nom="",codmon2="", men="", des2="",des1="", fec="",com="",ven="",conv="" ;
   
   String Titulo="Ingresar Tasa";//AGREGAR ESTADO
   String Subtitulo ="Informacion";//
   String Moneda = "Moneda";
   String Compra = "Compra";
   String Venta = "Venta";
   String Fecha = "Fecha";
   String Conversion = "Conversion";
   String action= CONTROLLER + "?estado=Tasa&accion=Ingresar";
   String a = (request.getParameter("sw")!=null)?request.getParameter("sw"):"-1";
   String tipo = "text";
   if ( a != null ){
        swcon = Integer.parseInt( a );
   }	
   
   
   if( mensaje.equals("cargado") ) {
       model.monedaSvc.buscarMonedaxCodigo(codmon1);
       moneda = model.monedaSvc.getMoneda();
       codmon1 = moneda.getCodMoneda();
       des1 = moneda.getNomMoneda();
       VecM = model.monedaSvc.listarMonedas();
	   
   }
   if ( swcon == 0 ){ //swcon = 0 capturo la informacion de la pag anterior
        VecM = model.monedaSvc.listarMonedas();
        codmon1= request.getParameter("c_combom1");
	    model.monedaSvc.buscarMonedaxCodigo(codmon1);
        moneda = model.monedaSvc.getMoneda();
        des1 = moneda.getNomMoneda();	
        codmon2= request.getParameter("c_combom2");
        model.monedaSvc.buscarMonedaxCodigo(codmon2);
        mone = model.monedaSvc.getMoneda();
        des2 = mone.getNomMoneda();	
        fec = request.getParameter("c_fecha");
        conv = request.getParameter("c_conver");  
        com = request.getParameter("c_compra");   
        ven = request.getParameter("c_venta");      
   } 
   else if ( swcon == 1) { //swcon = 1 capturo el objeto con la información
        tipo = "hidden";
        tasa = model.tasaService.obtenerTasa();
        model.monedaSvc.buscarMonedaxCodigo(tasa.getMoneda1());
        moneda = model.monedaSvc.getMoneda();
        codmon1 = moneda.getCodMoneda();
        des1 = moneda.getNomMoneda();
	    model.monedaSvc.buscarMonedaxCodigo(tasa.getMoneda2());
        moneda = model.monedaSvc.getMoneda();
        codmon2 = moneda.getCodMoneda();
        des2 = moneda.getNomMoneda();	   
        fec = tasa.getFecha();
        conv = ""+tasa.getVlr_conver();
        com = ""+tasa.getCompra();  
        ven = ""+tasa.getVenta(); 
 		Titulo="Modificar Tasa";//MODIFICAR ICOTERM 
		action = CONTROLLER + "?estado=Tasa&accion=Modificar";
    }  
	if(mensaje.equalsIgnoreCase("MsgModificado")){mensaje="Modificacion exitosa!";}
	if(mensaje.equalsIgnoreCase("MsgErrorIng")){mensaje="Modificacion exitosa!";} 
	if(mensaje.equalsIgnoreCase("MsgErrorT")){mensaje="No puede Ingresar valores muy extensos en Conversion, Compra y Venta!";} 
%>
<form name="forma" method="post" action="<%=action%>" >
  <table width="380" border="2" align="center">
    <tr>
      <td><table width="100%"  border="0">
        <tr>
          <td width="50%" class="subtitulo1"><%=Subtitulo%></td>
          <td width="50%" class="barratitulo"><img src="<%=BASEURL%>/images/titulo.gif" width="32" height="20"></td>
        </tr>
      </table>
      <table width="100%" class="tablaInferior">
        <tr class="fila">
          <td width="119" valign="middle" align="left"><%=Moneda%> 1</td>
          <td width="235" valign="middle">
            <%if (swcon==1) {%>
            <input type="hidden" name="c_combom1" value="<%=codmon1%>">
            <%=des1%>
            <%}
	    else{%>
            <select name="c_combom1"  class="textbox" id="c_combom1" onChange="cargarestado('<%=CONTROLLER%>?estado=Tasa&accion=Cargar&codmon1=',this);">
              <%if ( VecMon.size() > 0 ) {  
                for ( i=0;  i < VecMon.size(); i++ ){ 
                     moneda = (Moneda) VecMon.elementAt(i); 
                     if (moneda.getCodMoneda().equals(codmon1)){ %>
              <option value="<%=codmon1%>" selected><%=des1%></option>
              <%}
                     else{%>
              <option value="<%=moneda.getCodMoneda()%>"><%=moneda.getNomMoneda()%></option>
              <%}
                }
		     }%>
            </select>
			<img src="<%=BASEURL%>/images/botones/iconos/obligatorio.gif" width="10" height="10">
            <%}%>
            </td>
        </tr>
        <tr class="fila">
          <td valign="middle" align="left" ><%=Moneda%> 2</td>
          <td valign="middle">
            <%if (swcon==1) {%>
            <input type="hidden" name="c_combom2" value="<%=codmon2%>">
            <%=des2%>
            <%}
	    else{%>
            <select name="c_combom2"  class="textbox" >
              <% if( mensaje.equals("cargado") || mensaje.equals("MsgErrorIng") ) {     
                              if ( VecM.size() > 0 ) {  
                		  for ( i=0;  i < VecM.size(); i++ ){ 
                                       mone = (Moneda) VecM.elementAt(i); 
                                       if (mone.getCodMoneda().equals(codmon2)){ %>
              <option value="<%=codmon2%>" selected><%=des2%></option>
              <%}
                                       else if ( ! mone.getCodMoneda().equals(codmon1) ){%>
              <option value="<%=mone.getCodMoneda()%>"><%=mone.getNomMoneda()%></option>
              <%}
                	         }
		     	      }
                           }
			   else{%>
              <option value="" selected>Seleccione</option>
              <%}%>
            </select>
			<img src="<%=BASEURL%>/images/botones/iconos/obligatorio.gif" width="10" height="10">
            <%}%>
          </td>
        </tr>
        <tr class="fila">
          <td valign="middle"><%=Fecha%></td>
          <td valign="middle">
            <%=fec%>
            <input  type="<%=tipo%>"   name="c_fecha"  class="textbox" value="<%=fec%>" READONLY>
			<%if (swcon!=1) {%>
            <span class="Letras"><a href="javascript:void(0)" onclick="jscript: show_calendar('c_fecha');" HIDEFOCUS> </a> <img src="<%=BASEURL%>/images/cal.gif" width="16" height="16" align="absmiddle" style="cursor:hand " onclick="if(self.gfPop)gfPop.fPopCalendar(document.forma.c_fecha);return false;" HIDEFOCUS></span>
			<img src="<%=BASEURL%>/images/botones/iconos/obligatorio.gif" width="10" height="10"></td>
			
          <%}%>
        </tr>
        <tr class="fila">
          <td valign="middle"><%=Conversion%></td>
          <td valign="middle"><input name="c_conver" type="text" class="textbox"  maxlength="15" value="<%=conv%>" onKeyPress="soloDigitos(event,'decOK')" >
		  <img src="<%=BASEURL%>/images/botones/iconos/obligatorio.gif" width="10" height="10"></td>
        </tr>
        <tr class="fila">
          <td valign="middle" ><%=Compra%></td>
          <td valign="middle"><input name="c_compra" type="text" class="textbox"  maxlength="15" value="<%=com%>" onKeyPress="soloDigitos(event,'decOK')">
		  <img src="<%=BASEURL%>/images/botones/iconos/obligatorio.gif" width="10" height="10"></td>
        </tr>
        <tr class="fila">
          <td valign="middle"><%=Venta%></td>
          <td valign="middle"><input name="c_venta" type="text" class="textbox"  maxlength="15" value="<%=ven%>" onKeyPress="soloDigitos(event,'decOK')">
		  <img src="<%=BASEURL%>/images/botones/iconos/obligatorio.gif" width="10" height="10"></td>
        </tr>
      </table></td>
    </tr>
  </table>
  <br>
        <% if ((swcon==0) || (swcon==-1) ){%>
	       <div align="center">
			   <img src="<%=BASEURL%>/images/botones/aceptar.gif"  name="imgaceptar" onClick="return validarTCamposCiudad();" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" style="cursor:hand">&nbsp;
			   <img src="<%=BASEURL%>/images/botones/cancelar.gif"  name="imgcancelar"  onMouseOver="botonOver(this);" onClick="forma.reset();" onMouseOut="botonOut(this);" style="cursor:hand"> &nbsp;
               <img src="<%=BASEURL%>/images/botones/salir.gif"  name="imgsalir"  onMouseOver="botonOver(this);" onClick="parent.close();" onMouseOut="botonOut(this);" style="cursor:hand">
           </div>
		  <%} else {%>
		  	<div align="center">
			  <img src="<%=BASEURL%>/images/botones/modificar.gif"  name="imgmodificar" onClick="return validarTCamposCiudad();" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" style="cursor:hand">&nbsp;
              <img src="<%=BASEURL%>/images/botones/anular.gif"  name="imganular" onClick="window.location='<%=CONTROLLER%>?estado=Tasa&accion=Anular&c_combom1=<%=codmon1%>&c_combom2=<%=codmon2%>&c_fecha=<%=fec%>'" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" style="cursor:hand">&nbsp;
              <img src="<%=BASEURL%>/images/botones/salir.gif"  name="imgsalir"  onMouseOver="botonOver(this);" onClick="parent.close();" onMouseOut="botonOut(this);" style="cursor:hand">
          </div>
		  <%}%>

  <br>
   <% if( !mensaje.equalsIgnoreCase("") && !mensaje.equalsIgnoreCase("cargado")){ %>
   <table border="2" align="center">
  <tr>
    <td><table width="99%"  border="1" align="center" bordercolor="#F7F5F4" bgcolor="#FFFFFF">
      <tr>
        <td width="229" align="center" class="mensajes"><%=mensaje%></td>
        <td width="29" background="<%=BASEURL%>/images/cuadronaranja.JPG">&nbsp;</td>
        <td width="58">&nbsp;</td>
      </tr>
    </table></td>
  </tr>
</table>
    <% }%>
</form>
<iframe width=188 height=166 name="gToday:normal:agenda.js:gfPop:plugins.js" id="gToday:normal:agenda.js:gfPop:plugins.js" src="<%=BASEURL%>/js/Calendario/ipopeng.htm" scrolling="no" frameborder="0" style="visibility:visible; z-index:999; position:absolute; left:-500px; top:0px;"> </iframe>    
</body>
</html>
