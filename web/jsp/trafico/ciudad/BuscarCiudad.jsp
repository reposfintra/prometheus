<!--
- Autor : Ing. Diogenes Bastidas
- Date  : 25 Abril 2005
- Copyrigth Notice : Fintravalores S.A. S.A
-->
<%--
-@(#)
--Descripcion : Pagina JSP, que se encarga de gestionar 
				el formulario para la busqueda de ciudades
--%>

<%-- Declaracion de librerias--%>
<%@page contentType="text/html"%>
<%@page session="true"%>
<%@page errorPage="../error/ErrorPage.jsp"%>
<%@page import="java.util.*"%>
<%@page import="com.tsp.operation.model.*"%>
<%@page import="com.tsp.util.*"%>
<%@page import="com.tsp.operation.model.beans.*, java.util.*"%>
<%@include file="/WEB-INF/InitModel.jsp"%>

<html>
<head>
<title>.: Buscar Ciudad</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link href="<%= BASEURL %>/css/estilostsp.css" rel="stylesheet" type="text/css"> 
<link href="../../../css/estilostsp.css" rel="stylesheet" type="text/css"> 
<script type='text/javascript' src="<%= BASEURL %>/js/validar.js"></script>
<script type='text/javascript' src="<%= BASEURL %>/js/boton.js"></script>
</head>

<body onresize="redimensionar()" <%if(request.getParameter("reload")!=null){%>onLoad="parent.opener.location.reload();redimensionar();"<%}else {%>onload = 'redimensionar()'<%}%>>
<div id="capaSuperior" style="position:absolute; width:100%; height:100px; z-index:0; left: 0px; top: 0px;">
 <jsp:include page="/toptsp.jsp?encabezado=Buscar Ciudad"/>
</div>
<div id="capaCentral" style="position:absolute; width:100%; height:87%; z-index:0; left: 0px; top: 100px; overflow: scroll;">
<%
   String mensaje =(request.getParameter("mensaje")!=null)?request.getParameter("mensaje"):"";
   String men="",nompa = "",nomes="",nomci="";
   
   String Nompais = ("Pais");//Pais
   String Nomestado = ("Estado");//Estado
   String Nomciudad = ("Ciudad");//Ciudad
   if( mensaje.equals("Error_E") ) {
	   nompa = request.getParameter("nompais");
	   nomes = request.getParameter("nomestado");
	   nomci = request.getParameter("nomciudad");
	   men = "MsgErrorBusq";
   }%>

<form name="forma" method="post" action="<%=CONTROLLER%>?estado=Buscar&accion=Ciudadnom&pagina=VerCiudadXBusq.jsp&carpeta=jsp/trafico/ciudad">
  <table width="370" border="2" align="center">
    <tr>
      <td><table width="100%"  border="0">
        <tr>
          <td width="48%" class="subtitulo1">Buscar Ciudad</td>
          <td width="52%" class="barratitulo"><img src="<%=BASEURL%>/images/titulo.gif" width="32" height="20"></td>
        </tr>
      </table>
      <table width="100%" class="tablaInferior">
       
        <tr class="fila">
          <td width="157" valign="middle" align="left" >Pais</td>
          <td width="214" valign="middle"><input name="nompais" class="textbox" type="text" id="nompais" maxlength="40" value="<%=nompa%>" >
          </td>
        </tr>
        <tr class="fila">
          <td width="157" valign="middle" >Estado</td>
          <td valign="middle"><input name="nomestado" type="text" class="textbox" id="nomestado" maxlength="40" value="<%=nomes%>" ></td>
        </tr>
        <tr class="fila">
          <td width="157" valign="middle" >Ciudad</td>
          <td valign="middle"><input name="nomciudad" type="text" class="textbox" id="nomciudad" maxlength="40" value="<%=nomci%>"></td>
        </tr>
      </table></td>
    </tr>
  </table>
   <br>
   <div class="informacion" align="center">Digite el C�digo o Nombre de los Par�metros de b�squeda para realizar la consulta.</div>
   <br>
         <div align="center">
			   <img src="<%=BASEURL%>/images/botones/buscar.gif"  name="imgbuscar" onClick="forma.submit()" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" style="cursor:hand">&nbsp;
			   <img src="<%=BASEURL%>/images/botones/detalles.gif"  name="imgdetalles"  onMouseOver="botonOver(this);" onClick="window.location='<%=CONTROLLER%>?estado=Buscar&accion=Ciudadnom&pagina=VerCiudadXBusq.jsp&carpeta=jsp/trafico/ciudad&nompais=&nomestado=&nomciudad='" onMouseOut="botonOut(this);" style="cursor:hand"> &nbsp;
               <img src="<%=BASEURL%>/images/botones/salir.gif"  name="imgsalir"  onMouseOver="botonOver(this);" onClick="parent.close();" onMouseOut="botonOut(this);" style="cursor:hand">
           </div>
</form>
</div>
</body>
</html>
