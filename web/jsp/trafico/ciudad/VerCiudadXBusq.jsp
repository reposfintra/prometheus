<!--
- Autor : Ing. Diogenes Bastidas
- Date  : 25 Abril 2005
- Copyrigth Notice : Fintravalores S.A. S.A
-->
<%--
-@(#)
--Descripcion : Pagina JSP, que se encarga de gestionar 
				el formulario para la muestra de la lista de las ciudades
--%>

<%-- Declaracion de librerias--%>
<%@page contentType="text/html"%>
<%@page session="true"%>
<%@page errorPage="../error/ErrorPage.jsp"%>
<%@page import="java.util.*"%>
<%@page import="com.tsp.operation.model.*"%>
<%@page import="com.tsp.util.*"%>
<%@page import="com.tsp.operation.model.beans.*, java.util.*"%>
<%@include file="/WEB-INF/InitModel.jsp"%>
<%@taglib uri="http://jsptags.com/tags/navigation/pager" prefix="pg" %>

<html>
<head>
<title>.: Ciudades</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link href="<%= BASEURL %>/css/estilostsp.css" rel="stylesheet" type="text/css"> 
<link href="../../../css/estilostsp.css" rel="stylesheet" type="text/css">
</head>
 
<script type='text/javascript' src="<%= BASEURL %>/js/reporte.js"></script>
<script type='text/javascript' src="<%= BASEURL %>/js/boton.js"></script>
<body onresize="redimensionar()" <%if(request.getParameter("reload")!=null){%>onLoad="parent.opener.location.reload();redimensionar();"<%}else {%>onload = 'redimensionar()'<%}%>>
<div id="capaSuperior" style="position:absolute; width:100%; height:100px; z-index:0; left: 0px; top: 0px;">
 <jsp:include page="/toptsp.jsp?encabezado=Ciudades"/>
</div>
<div id="capaCentral" style="position:absolute; width:100%; height:87%; z-index:0; left: 0px; top: 100px; overflow: scroll;">

  <%String style = "simple";
    String position =  "bottom";
    String index =  "center";
    int maxPageItems = 30;
    int maxIndexPages = 10;
    String nompais = (String) session.getAttribute("nompais");
	String nomestado = (String) session.getAttribute("nomestado");
    String nomciudad = (String) session.getAttribute("nomciudad");

	
    Vector VecCiudades = model.ciudadservice.obtCiudades();
    Ciudad ciudad;  
	if ( VecCiudades.size() >0 ){
  %>
<form name="form1" method="post" action="">
<table width="545" border="2" align="center">
      <tr>
        <td><table width="100%" align="center">
          <tr>
            <td width="48%" class="subtitulo1">&nbsp;Datos Ciudades </td>
            <td width="52%" class="barratitulo"><img src="<%=BASEURL%>/images/titulo.gif" width="32" height="20">
                <input name="nompais" type="hidden" id="nompais" value="<%=nompais%>">
                <input name="nomestado" type="hidden" id="nomestado" value="<%=nomestado%>">
                <input name="nomciudad" type="hidden" id="nomciudad" value="<%=nomciudad%>"></td>
          </tr>
        </table>
          <table width="99%" border="1" align="center" bordercolor="#999999" bgcolor="#F7F5F4">
          <tr class="tblTitulo" align="center">
            <td width="133">Pais </td>
            <td width="124">Estado</td>
            <td width="69" >Codigo</td>
            <td width="218">Nombre</td>
          </tr>
          <pg:pager
    items="<%=VecCiudades.size()%>"
    index="<%= index %>"
    maxPageItems="<%= maxPageItems %>"
    maxIndexPages="<%= maxIndexPages %>"
    isOffset="<%= true %>"
    export="offset,currentPageNumber=pageNumber"
    scope="request">
          <%-- keep track of preference --%>
          <%
        for (int i = offset.intValue(), l = Math.min(i + maxPageItems, VecCiudades.size()); i < l; i++) {
            ciudad = (Ciudad) VecCiudades.elementAt(i);%>
          <pg:item>
          <tr class="<%=(i % 2 == 0 )?"filagris":"filaazul"%>" onMouseOver='cambiarColorMouse(this)'   style="cursor:hand" 
        onClick="window.open('<%=CONTROLLER%>?estado=Buscar&accion=Ciudad&pagina=ciudad.jsp&carpeta=jsp/trafico/ciudad&codpais=<%=ciudad.getpais_code()%>&codestado=<%=ciudad.getdepartament_code()%>&codigo=<%=ciudad.getcodciu()%>' ,'','status=yes,scrollbars=no,width=620,height=520,resizable=yes');">
            <td height="20" class="bordereporte"  ><%=ciudad.getpais_name()%></td>
            <td height="20" class="bordereporte" ><%=ciudad.getdepartament_name()%></td>
            <td height="20" class="bordereporte" ><%=ciudad.getcodciu()%></td>
            <td height="20" class="bordereporte" ><%=ciudad.getnomciu()%></td>
          </tr>
          </pg:item>
          <%}%>
          <tr class="bordereporte">
            <td colspan="4" align="center" valign="middle" >
              <div align="center"><pg:index>
                <jsp:include page="../../../WEB-INF/jsp/googlesinimg.jsp" flush="true"/>        
    </pg:index></div></td>
          </tr>
          </pg:pager>
        </table></td>
      </tr>
  </table>
</form>
  <%}
 else { %>
  <table border="2" align="center">
    <tr>
      <td><table width="410" height="40" border="1" align="center"  bordercolor="#F7F5F4" bgcolor="#FFFFFF">
          <tr>
            <td width="282" align="center" class="mensajes">Su b&uacute;squeda no arroj&oacute; resultados!</td>
            <td width="28" background="<%=BASEURL%>/images/cuadronaranja.JPG">&nbsp;</td>
            <td width="78">&nbsp;</td>
          </tr>
      </table></td>
    </tr>
  </table>
  <p>
      <%}%>
</p>
  <table width="545" border="0" align="center">
    <tr>
      <td><img src="<%=BASEURL%>/images/botones/regresar.gif"  name="imgaceptar" onclick="window.location='<%=CONTROLLER%>?estado=Menu&accion=Cargar&pagina=BuscarCiudad.jsp&carpeta=/jsp/trafico/ciudad&mensaje=&marco=no'" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" style="cursor:hand">
	  <img src="<%=BASEURL%>/images/botones/salir.gif"  name="imgsalir"  onMouseOver="botonOver(this);" onClick="parent.close();" onMouseOut="botonOut(this);" style="cursor:hand"></td>
    </tr>
  </table>
  </div>
</body>
</html>
