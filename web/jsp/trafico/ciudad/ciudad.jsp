<!--
- Autor : Ing. Diogenes Bastidas
- Date  : 25 Abril 2005
- Copyrigth Notice : Fintravalores S.A. S.A
-->
<%--
-@(#)
--Descripcion : Pagina JSP, que se encarga de gestionar 
				el formulario para insertar/modificar/anular ciudad
--%>

<%-- Declaracion de librerias--%>
<%@page contentType="text/html"%>
<%@page session="true"%>
<%@page errorPage="../error/ErrorPage.jsp"%>
<%@page import="java.util.*"%>
<%@page import="com.tsp.operation.model.*"%>
<%@page import="com.tsp.util.*"%>
<%@page import="com.tsp.operation.model.beans.*, java.util.*"%>
<%@include file="/WEB-INF/InitModel.jsp"%>
<%@ taglib uri="http://jakarta.apache.org/taglibs/input-0.90" prefix="input" %>
<html>
<head>
<%	Iterator it;   
	List Paises, Estados, Zonas, Fronaso;
	Pais pais; 
	Estado estado; 
	Ciudad ciudad = new Ciudad();   
	
	Zona zona, zonano;
	int i=0;
    Vector codigoRICA = model.TimpuestoSvc.getImpuestos();
   String mensaje =(request.getParameter("mensaje")!=null)?request.getParameter("mensaje"):"";
   String cpais = (request.getParameter("pais")!=null)?request.getParameter("pais"):"";
   String cod="", nom="", men="", codpais="",nompais="",coddpto="",nomdpto="", nomzona="" ;
   String codica="", aplica="N", czona="", frontera="N", agasoc="", tipociu="N",ind="",naplica="No", nfrontera="No";
   String cagencia="", nomtipoc="NORMAL", nomagasoc="";
   String cagasoc="",treporte="",zonaurb="",fronaso=""; 

   String a = (request.getParameter("sw")!=null)?request.getParameter("sw"):"-1";
   int swcon=0;//0:ingresar; 1:modificar-anular

   if ( a != null ){
        swcon = Integer.parseInt( a );
   }	
   
   if ( swcon == 1 ){  
		ciudad = model.ciudadservice.obtenerCiudad(); 
	}
   
   String tipo = ( swcon == 1  )?"hidden":"text";   
   String Titulo = ( (swcon == 0) || (swcon == -1) )?"Ingresar Ciudad":"Modificar/Anular Ciudad";
   String action= ( (swcon == 0) || (swcon == -1) )?CONTROLLER + "?estado=Insertar&accion=Ciudad":CONTROLLER + "?estado=Modificar&accion=Ciudad";
   
   String []vntipo = {"AGENCIA","NORMAL","PC CONTROL"};
   String []vctipo = {"A","N","PC"};
   
   if ( swcon == 1 ){
		ciudad = model.ciudadservice.obtenerCiudad(); }
   if( mensaje.equals("cargado") ) {           
       pais = model.paisservice.obtenerpais(); 
       codpais = pais.getCountry_code();
       nompais = pais.getCountry_name();
   }
   
   if ( swcon == 0 ){
		estado = model.estadoservice.obtenerestado(); 
		coddpto=(mensaje.equals(""))?estado.getdepartament_code():request.getParameter("c_comboestado");
		nomdpto=estado.getdepartament_name();
		codpais = estado.getpais_code();
        nompais = estado.getpais_name();        
        cod = request.getParameter("c_codigo");
        nom = request.getParameter("c_nombre");
		codica = request.getParameter("c_codica");
		ind = request.getParameter("c_ind");
		aplica = request.getParameter("c_aplica");
		naplica = (aplica.equals("S"))?"Si":"No";
		frontera = request.getParameter("c_frontera");
		nfrontera = (frontera.equals("S"))?"Si":"No";
		tipociu = request.getParameter("c_tipoc");
		czona = request.getParameter("c_combozona");
		treporte=request.getParameter("treporte");
        zonaurb=request.getParameter("urbana");
        fronaso=request.getParameter("fron_asoc");
	}
   else if ( swcon == 1 ){ 
        cod = ciudad.getcodciu();
        nom = ciudad.getnomciu();
        nomagasoc = ciudad.getNomagasoc();
        agasoc  = ciudad.getAgAsoc();
        nomzona = ciudad.getNomzona();
        czona   = ciudad.getZona();
        codica  = ciudad.getCodIca();
        aplica  = ciudad.getAplIca();
		naplica = (aplica.equals("S"))?"Si":"No";
        frontera = ciudad.getFrontera();
        tipociu = ciudad.getTipociu();
		ind = ""+ciudad.getIndicativo();
		treporte=ciudad.getTiene_rep_urbano();
        zonaurb=ciudad.getZona_urb();
        fronaso=ciudad.getFrontera_asoc();        
		     	   
        estado = model.estadoservice.obtenerestado(); 
        coddpto = ciudad.getdepartament_code();
        nomdpto = ciudad.getdepartament_name();
        codpais = ciudad.getpais_code();
        nompais = ciudad.getpais_name();
   }
 %>
    <title>.: <%=Titulo%></title>
    <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
    <link href="<%= BASEURL %>/css/estilostsp.css" rel="stylesheet" type="text/css"> 
    <link href="../../../css/estilostsp.css" rel="stylesheet" type="text/css"> 
    <script type='text/javascript' src="<%= BASEURL %>/js/validar.js"></script>
    <script type='text/javascript' src="<%= BASEURL %>/js/boton.js"></script>
    <script type='text/javascript' src="<%= BASEURL %>/js/general.js"></script>
</head>
<body onresize="redimensionar()" <%if(request.getParameter("reload")!=null){%>onLoad="parent.opener.location.reload();redimensionar();forma.c_nombre.focus();"<%}else {%>onload = 'redimensionar();'<%}%>>
<div id="capaSuperior" style="position:absolute; width:100%; height:100px; z-index:0; left: 0px; top: 0px;">
 <jsp:include page="/toptsp.jsp?encabezado=Ciudad"/>
</div>
<div id="capaCentral" style="position:absolute; width:100%; height:87%; z-index:0; left: 0px; top: 100px; overflow: scroll;">
    <form name="forma" method="post" action="<%=action%>" >
      <table width="465" border="2" align="center">
        <tr>
          <td><table width="100%"  border="0">
            <tr>
              <td width="48%" class="subtitulo1"><%=Titulo%><input name="titulo" type="hidden" value="<%=Titulo%>"></td>
              <td width="52%" class="barratitulo"><img src="<%=BASEURL%>/images/titulo.gif" width="32" height="20"></td>
            </tr>
          </table>
          <table width="100%" class="tablaInferior">
            <tr class="fila">
              <td width="155" valign="middle" align="left">Pais</td>
              <td valign="middle" colspan="3">
                <%if (swcon==1) {%>
                <input type="hidden" name="c_combopais" value="<%=codpais%>">
                 <input type="text" name="nomp" value="<%=nompais%>" readonly> 
                 &nbsp;<a href="#" class="Simulacion_Hiper" onClick="window.open('<%=CONTROLLER%>?estado=Cargar&accion=Varios&sw=16','','menubar=no, status=yes, resizable=yes, width=800, height=500, top=100')" title="Haga clic aqui para seleccionar un Pais.">Seleccione...</a>
                <%}
	           else{%>
                <select name="c_combopais"   class="textbox" id="c_combopais" onChange="cargarestado('<%=CONTROLLER%>?estado=Cargar&accion=Estado&pais=',this);"  >
                  <%Paises = model.paisservice.obtenerpaises();		
			   		it = Paises.iterator();%>
                  <%if ( ( swcon == 0 ) || (mensaje.equals("")) ){%>
                  	<option value="" selected>Seleccione</option>
                  <%}
			   		while (it.hasNext()){  
			       		pais = (Pais) it.next();	
    	    	        if (pais.getCountry_code().equals(codpais)){%>
                  			<option value="<%=codpais%>" selected><%=nompais%></option>
                  		<%}else{%>
                  			<option value="<%=pais.getCountry_code()%>"><%=pais.getCountry_name()%></option>
                  		<%}
                  }%>
              </select> 
			  <img src="<%=BASEURL%>/images/botones/iconos/obligatorio.gif" width="10" height="10"></td>
              <%}%>
            </tr>
            <tr class="fila">
              <td width="155" valign="middle">Estado</td>
              <td valign="middle" colspan="3">
           		<%if(swcon==1){%>
                	<input name="c_comboestado" type="text" value="<%=coddpto%>" size="15" maxlength="10" onKeyPress="soloAlfa(event);" readonly>
                	<img src="<%=BASEURL%>/images/botones/iconos/obligatorio.gif" width="10" height="10">                	<input name="antest" type="hidden" value="<%=coddpto%>">
					&nbsp;<a href="#" class="Simulacion_Hiper" onClick="window.open('<%=CONTROLLER%>?estado=Cargar&accion=Varios&sw=7&pais=<%=codpais%>','','menubar=no, status=yes, resizable=yes, width=800, height=500, top=100')" title="Haga clic aqui para seleccionar un Estado.">Seleccione...</a>
                <%}else {%>
                	<select name="c_comboestado"  class="textbox" >
						<option value="" selected>Seleccione</option>
                	<%if ( mensaje.equals("cargado") || ( mensaje.equals("Error al Ingresar Ciudad") ) ){
                    	Estados = model.estadoservice.obtenerEstados();			   			
                    	it=Estados.iterator();
                       	while (it.hasNext()){  
                        	estado = (Estado) it.next();	
							if ( estado.getdepartament_code().equals(coddpto) ){%>
        	          			<option value="<%=coddpto%>" selected><%=estado.getdepartament_name()%></option>
            	      		<%}else{%>
                	  			<option value="<%=estado.getdepartament_code()%>"><%=estado.getdepartament_name()%></option>
                  			<%}  
			        	}                   		
		     		}%>
              		</select>
			  		<img src="<%=BASEURL%>/images/botones/iconos/obligatorio.gif" width="10" height="10"> </td>
              <%}//fin if swcon estado%>
            </tr>
            <tr class="fila">
              <td >Codigo</td>
              <td colspan="3">
			  	<input name="c_codigo" type="<%=tipo%>" id="codigo" class="textbox" maxlength="2" value="<%=cod%>" onKeyPress="soloAlfa(event);"><%if (swcon==1)%><%=cod%>
                <%if(swcon==0){%>
              	  <img src="<%=BASEURL%>/images/botones/iconos/obligatorio.gif" width="10" height="10">
				<%}%>
              </td>
            </tr>
            <tr class="fila">
              <td valign="middle">Nombre</td>
              <td valign="middle" colspan="3"><input name="c_nombre" type="text" class="textbox" id="nombre" size="45" maxlength="40" value="<%=nom%>" onKeyPress="soloAlfa(event);" >
			  <img src="<%=BASEURL%>/images/botones/iconos/obligatorio.gif" width="10" height="10"></td>
            </tr>
            <tr class="fila">
              <td valign="middle">Aplicar codigo de ICA</td>
              <td width="88" valign="middle">	 
<select name="c_aplica" class="textbox" onChange="buscarIca('?estado=Cargar&accion=Varios&sw=18&c_codigo=');">
  <option value="S" <%=(aplica.equals("S"))?"selected":""%>  >Si</option>
  <option value="N" <%=(!aplica.equals("S"))?"selected":""%> >No</option>
</select>              
<img src="<%=BASEURL%>/images/botones/iconos/obligatorio.gif" width="10" height="10"> </td>
			  
			  
              <td width="96" valign="middle">Codigo de ICA</td>
              <td width="94" valign="middle" id="ica"><%if(codigoRICA!=null){%>
                <select   class="textbox"  name="c_codica"  >
                <option value="">  </option>
                <%for(int k=0; k < codigoRICA.size() ;k++){  
                   		Tipo_impuesto dato = (Tipo_impuesto) codigoRICA.get(k);%>
                <option value="<%=dato.getCodigo_impuesto()%>" <%=(dato.getCodigo_impuesto().equals(codica))?"selected":""%> ><%=dato.getCodigo_impuesto()%></option>
                <%}%>
              </select><%}%></td>
                     
            </tr>
            <tr class="fila">
              <td valign="middle">Ciudad de Frontera</td>
              <td valign="middle" colspan="3">
              <select name="c_frontera" class="textbox">
				  	<option value="S" <%=(frontera.equals("S"))?"selected":""%>>Si</option>
				  	<option value="N" <%=(!frontera.equals("S"))?"selected":""%>>No</option>
                </select>
				<img src="<%=BASEURL%>/images/botones/iconos/obligatorio.gif" width="10" height="10">
               
              </td>
            </tr>
            <tr class="fila">
              <td valign="middle">Tipo de Ciudad</td>
              <td valign="middle" colspan="3">
                <select name="c_tipoc" class="textbox">
					<% for (int z = 0; z < vctipo.length; z++){ 
							if (tipociu.equals(vctipo[z])){%>
						    	<option value="<%=vctipo[z]%>" selected><%=vntipo[z]%>
						    <%}else{%>
                  				<option value="<%=vctipo[z]%>"><%=vntipo[z]%>
					        <%}
						}%>                  				
                </select>
              <img src="<%=BASEURL%>/images/botones/iconos/obligatorio.gif" width="10" height="10">              </td>
            </tr>
            <tr class="fila">
              <td width="155" valign="middle">Zona</td>
              <td valign="middle" colspan="3">
                	<select   class="textbox"  name="c_combozona"  >
                  	<%Zonas = model.zonaService.getZonasNoFronterisas();
			  	   	it = Zonas.iterator();
		           	while (it.hasNext()){  
                   		zona = (Zona) it.next();
						if (zona.getCodZona().equals(czona)){%>
							<option value="<%=zona.getCodZona()%>" selected><%=zona.getNomZona()%></option>
						<%}else{%>
                  			<option value="<%=zona.getCodZona()%>"><%=zona.getNomZona()%></option>
                  		<%}
					}%>
              		</select>
			  		<img src="<%=BASEURL%>/images/botones/iconos/obligatorio.gif" width="10" height="10"></td>
             
            </tr>
            <tr class="fila">
              <td width="155" valign="middle">Agencia Asociada</td>
              <td valign="middle" colspan="3">
                	<%TreeMap agencia = model.agenciaService.getCbxAgencia(); %>
                	<input:select name="agencia"  attributesText="style='width:80%;' class='textbox'"  options="<%=agencia%>" default="<%=agasoc%>"/>
					<img src="<%=BASEURL%>/images/botones/iconos/obligatorio.gif" width="10" height="10">
              </td>
            </tr>
            <tr class="fila">
              <td valign="middle">Indicativo</td>
              <td valign="middle" colspan="3"><input name="c_ind" type="text" class="textbox" id="c_ind" maxlength="3" value="<%=ind%>" onKeyPress="soloDigitos(event, 'decNO');" >
              <img src="<%=BASEURL%>/images/botones/iconos/obligatorio.gif" width="10" height="10"></td>
            </tr>
			 <tr class="fila">
              <td valign="middle">Trafico Urbano</td>
              <td valign="middle" colspan="3"><select name="treporte" class="textbox">
                <option value="S" <%=(treporte.equals("S"))?"selected":""%>>Si</option>
                <option value="N" <%=(!treporte.equals("S"))?"selected":""%>>No</option>
              </select>
               <img src="<%=BASEURL%>/images/botones/iconos/obligatorio.gif" width="10" height="10"></td>
            </tr>
            <tr class="fila">
              <td valign="middle">Zona Urbana </td>
              <td valign="middle" colspan="3"><select name="urbana" class="textbox">
                <option value="U" <%=(zonaurb.equals("U"))?"selected":""%>>Si</option>
                <option value="" <%=(zonaurb.equals(""))?"selected":""%>>No</option>
			</select>
                <img src="<%=BASEURL%>/images/botones/iconos/obligatorio.gif" width="10" height="10"> </td>
            </tr>
            <tr class="fila">
              <td valign="middle">Frontera Asociada </td>
              <td valign="middle" colspan="3"><select   class="textbox"  name="fron_asoc"  >
                <option value="">Ninguna</option>
                <%Fronaso = model.zonaService.getZonasFronterisas();
			  	    for(int j=0; j < Fronaso.size() ;j++){  
                   		zonano = (Zona) Fronaso.get(j);%>
                <option value="<%=zonano.getCodZona()%>" <%=(zonano.getCodZona().equals(cagasoc))?"selected":""%> ><%=zonano.getNomZona()%></option>
               <%}%>
              </select>
                <img src="<%=BASEURL%>/images/botones/iconos/obligatorio.gif" width="10" height="10"></td>
            </tr>
           

          </table></td>
        </tr>
      </table>
     <br>
      <% if ((swcon==0) || (swcon==-1) ){%>
	       <div align="center">
			   <img src="<%=BASEURL%>/images/botones/aceptar.gif"  name="imgaceptar" onClick="validarFormCiudad(forma)" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" style="cursor:hand">&nbsp;
			   <img src="<%=BASEURL%>/images/botones/cancelar.gif"  name="imgcancelar"  onMouseOver="botonOver(this);" onClick="forma.reset();" onMouseOut="botonOut(this);" style="cursor:hand"> &nbsp;
               <img src="<%=BASEURL%>/images/botones/salir.gif"  name="imgsalir"  onMouseOver="botonOver(this);" onClick="parent.close();" onMouseOut="botonOut(this);" style="cursor:hand">
           </div>
		  <%} else {%>
		  	<div align="center">
			  <img src="<%=BASEURL%>/images/botones/modificar.gif"  name="imgmodificar" onClick="validarFormCiudad(forma)" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" style="cursor:hand">&nbsp;
              <img src="<%=BASEURL%>/images/botones/anular.gif"  name="imganular" onClick="window.location='<%=CONTROLLER%>?estado=Anular&accion=Ciudad&codigo=<%=cod%>&pais=<%=codpais%>&dpto=<%=coddpto%>&codigo=<%=cod%>&nombre=<%=nom%>'" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" style="cursor:hand">&nbsp;
              <img src="<%=BASEURL%>/images/botones/salir.gif"  name="imgsalir"  onMouseOver="botonOver(this);" onClick="parent.close();" onMouseOut="botonOut(this);" style="cursor:hand">
          </div>
		  <%}%>

  <%if( (!mensaje.equalsIgnoreCase("")) && (!mensaje.equalsIgnoreCase("cargado"))){%>
  <p><table border="2" align="center">
  <tr>
    <td><table width="100%"  border="1" align="center"  bordercolor="#F7F5F4" bgcolor="#FFFFFF">
      <tr>
        <td width="229" align="center" class="mensajes"><%=mensaje%></td>
        <td width="29" background="<%=BASEURL%>/images/cuadronaranja.JPG">&nbsp;</td>
        <td width="58">&nbsp;</td>
      </tr>
    </table></td>
  </tr>
</table>
</p>
 <%}%>
    </form>
</div>
</body>
<script>
    function enviar(url){
         var a = "<iframe name='ejecutor' style='visibility:hidden'  src='<%= CONTROLLER %>" + url + "'> ";
         aa.innerHTML = a;
    }

</script>
<font id='aa'></font>

</html>
