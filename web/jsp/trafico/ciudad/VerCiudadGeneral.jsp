<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
"http://www.w3.org/TR/html4/loose.dtd">
<%@page contentType="text/html"%>
<%@page session="true"%>
<%@page errorPage="../error/ErrorPage.jsp"%>
<%@page import="java.util.*"%>
<%@page import="com.tsp.operation.model.*"%>
<%@page import="com.tsp.util.*"%>
<%@page import="com.tsp.operation.model.beans.*, java.util.*"%>
<%@include file="/WEB-INF/InitModel.jsp"%>
<%@taglib uri="http://jsptags.com/tags/navigation/pager" prefix="pg" %>

<html>
<head>
<title>Listado de Paises</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link href="<%= BASEURL %>/css/estilo.css" rel="stylesheet" type="text/css"> 
</head>

<body>
  <div align="center">
  <%String style = "simple";
    String position =  "bottom";
    String index =  "center";
    int maxPageItems = 10;
    int maxIndexPages = 10;

    model.ciudadservice.listarCiudadGeneral();
    Vector VecCiudades = model.ciudadservice.obtCiudades();
    Ciudad ciudad; 
	if ( VecCiudades.size() >0 ){ 
  %>
  </div>
  <table width="541" height="91" align="center"  border="1"  cellpadding="4" cellspacing="1" bordercolor="#B7D0DC">
    <tr class="titulo">
      <td colspan="10" valign="middle" align="center">Ciudades</td>
    </tr>
    <tr class="subtitulos" align="center">
      <td width="133" >Pais</td>
      <td width="124" >Estado</td>
      <td width="69"  >Codigo</td>
      <td width="231" >Nombre</td>
    </tr>
	<pg:pager
    items="<%=VecCiudades.size()%>"
    index="<%= index %>"
    maxPageItems="<%= maxPageItems %>"
    maxIndexPages="<%= maxIndexPages %>"
    isOffset="<%= true %>"
    export="offset,currentPageNumber=pageNumber"
    scope="request">
<%-- keep track of preference --%>

  <%
        for (int i = offset.intValue(), l = Math.min(i + maxPageItems, VecCiudades.size()); i < l; i++) {
            ciudad = (Ciudad) VecCiudades.elementAt(i);%>

		<pg:item>

    <tr class="letra" onMouseOver="bgColor= '#99cc99'" onMouseOut="bgColor='#ECE0D8'" bgcolor="#ECE0D8" style="cursor:hand" 
        onClick="window.open('<%=CONTROLLER%>?estado=Buscar&accion=Ciudad&pagina=ciudad.jsp&carpeta=jsp/trafico/ciudad&codpais=<%=ciudad.getpais_code()%>&codestado=<%=ciudad.getdepartament_code()%>&codigo=<%=ciudad.getcodciu()%>&mensaje=' ,'','status=no,scrollbars=no,width=400,height=500,resizable=yes');">
      <td height="20" ><%=ciudad.getpais_name()%></td>
      <td height="20" ><%=ciudad.getdepartament_name()%></td>
      <td height="20" ><%=ciudad.getcodciu()%></td>
      <td height="20"><%=ciudad.getnomciu()%></td>
     
    </tr>
	</pg:item>
  <%}%>

    <tr class="fila">
      <td colspan="4" align="center" valign="middle" >
	    <div align="center"><pg:index>
	      <jsp:include page="../../../WEB-INF/jsp/googlesinimg.jsp" flush="true"/>        
      </pg:index></div></td>
    </tr>
  </pg:pager>
</table>

  <%}
 else { %>
  <table width="100%">
    <tr>
      <td height="50" class="titulo"> </td>
    </tr>
    <tr>
      <td height="60" class="fila"align="center"> <span class="mensajes">Error en la Busqueda</span> </td>
    </tr>
    <tr>
      <td height="50" class="titulo">&nbsp; </td>
    </tr>
  </table>
  <%}%>
  <p><a style="cursor:hand" class="letra_resaltada" onclick="window.location='<%=CONTROLLER%>?estado=Idioma&accion=Cargar&pagina=BuscarCiudad.jsp&ruta=/jsp/trafico/ciudad/&mensaje='" >Atras</a></p>
</body>
</html>
