<!--
- Autor: Ing. Sandra M Escalante G
- Date  : 02 Febrero 2006
- Copyrigth Notice : Fintravalores S.A. S.A
-->
<%--
-@(#)
--Descripcion : Pagina JSP, que se encarga de gestionar 
				el formulario para la busqueda
				y seleleccion de companias
--%>

<%-- Declaracion de librerias--%>
<%@page contentType="text/html"%>
<%@page session="true"%>
<%@page import="java.util.*"%>
<%@page import="com.tsp.operation.model.*"%>
<%@page import="com.tsp.util.*"%>
<%@page import="com.tsp.operation.model.beans.*, java.util.*"%>
<%@include file="/WEB-INF/InitModel.jsp"%>
<%@taglib uri="http://jsptags.com/tags/navigation/pager" prefix="pg" %>
<html>
<head>
<title>.: Buscar Estados</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<script type='text/javascript' src="<%= BASEURL %>/js/validar.js"></script>
<script type='text/javascript' src="<%= BASEURL %>/js/boton.js"></script>
<script type='text/javascript' src="<%= BASEURL %>/js/reporte.js"></script>
<link href="../../../css/estilostsp.css" rel="stylesheet" type="text/css">
<link href="<%=BASEURL%>/css/estilostsp.css" rel="stylesheet" type="text/css">
<style type="text/css">
<!--
.Estilo1 {font-size: 10px}
-->
</style>
</head>

<body onResize="redimensionar()" onLoad="redimensionar()">
<div id="capaSuperior" style="position:absolute; width:100%; height:100px; z-index:0; left: 0px; top: 0px;">
 <jsp:include page="/toptsp.jsp?encabezado=Buscar Estados"/>
</div>
<div id="capaCentral" style="position:absolute; width:100%; height:87%; z-index:0; left: 0px; top: 100px; overflow: scroll;">

<%String style = "simple";
   	String position =  "bottom";
   	String index =  "center";
    String comp = "";
   	int maxPageItems = 10;
   	int maxIndexPages = 10;
	
%>
<form name="forma" method="post" action="<%=CONTROLLER%>?estado=Cargar&accion=Varios&sw=8" onSubmit="return validarFormaContacto(this);">
<table width="501" border="2" align="center">
  <tr>
    <td>
<table width="99%" align="center" cellpadding="0">
  <tr>
    <td width="392"  class="subtitulo1"><p align="left">Busqueda de Estados </p></td>
    <td width="392"  class="barratitulo"><img src="<%=BASEURL%>/images/titulo.gif" width="32" height="20"></td>
  </tr>
</table>
<table width="99%" align="center" cellpadding="2" cellspacing="0">
  <tr class="subtitulos">
    <td class="fila" >**Digite la frase sobre la cual desea realizar la busqueda.</td>
    </tr>
  <tr class="fila">
    <td ><table width="92%"  border="0" cellpadding="0" cellspacing="0">
      <tr>
        <td width="61%"><input name="frase" id="frase" type="text" class="textbox" size="50" onKeyPress="soloTextoPorcentaje(event);">
          <input type="hidden" name="pais" id="pais" value="<%=request.getParameter("pais")%>"></td>
        <td width="39%">          <img src="<%=BASEURL%>/images/botones/buscar.gif" name="mod"  height="21" onClick="forma.submit()" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" style="cursor:hand">
          </td>
      </tr>
    </table></td>
  </tr>
  <tr class="fila">
    <td>
	  <span class="Simulacion_Hiper" style="cursor:hand " onClick='verNota();'>Ver formato de b�squeda...</span>
	</td>
  </tr>
</table>
</td>
</tr>
</table>

<table id="nota" width="59%"  align="center" style="display:none">
	  	<tr><td class="letra">
			<FIELDSET><legend><span class="fila Estilo1">Formato de B�squeda</span></legend>
			<table width="100%"  border="0" cellpadding="0" cellspacing="0" class="informacion">
  				<tr>
    			  <td colspan="2">* La  b&uacute;squeda es realizada por  NOMBRE del Estado<br>
				  * Formato de  la frase para ejecutar la b&uacute;squeda. </td>
    			</tr>
  				<tr align="right"> <td width="12%" nowrap>Ejemplo</td>
  				  <td width="88%">&nbsp;</td></tr>
  				<tr>
    		      <td>&nbsp;</td>
    		      <td>** Para realizar una b&uacute;squeda donde el nombre contega el conjunto de letras '<i>RE</i>', en el campo de texto debe digitar:<i> %RE% </i></td>
  				</tr>
  				<tr>
    			  <td>&nbsp;</td>
    			  <td>** Para ver todos los registros, debe digitar: <i> % </i></td>
				</tr>
			</table>
  		</FIELDSET>
		</td></tr>
  </table>
</form>
<br>
<%if ( request.getParameter("mostrar")==null || request.getParameter("mostrar").equals("OK")){
	Vector estados = model.estadoservice.obtEstados();
	%>
	<font class="informacion"><center>Resultados de la Busqueda: <%=session.getAttribute("frase")%></center></font>
  
<br>
<table width="400" border="2" align="center">
  <tr>
    <td>
	<table width="100%" border="1" align="center" cellpadding="0" bordercolor="#F7F5F4">
  <tr>
    <td width="78"  class="subtitulo1"><p align="left">Estados</p></td>
    <td width="298"  class="barratitulo"><img src="<%=BASEURL%>/images/titulo.gif" width="32" height="20"></td>
  </tr>
</table>
<table width="100%" border="1" align="center"  bordercolor="#999999" bgcolor="#F7F5F4">
  <tr class="tblTitulo">
    <td width="27%">Codigo</td>
    <td width="60%">Nombre</td>	
  </tr>
  <pg:pager    items="<%=estados.size()%>"
    index="<%= index %>"
    maxPageItems="<%= maxPageItems %>"
    maxIndexPages="<%= maxIndexPages %>"
    isOffset="<%= true %>"
    export="offset,currentPageNumber=pageNumber"
    scope="request">
	<%Estado estado;
	for (int i = offset.intValue(), l = Math.min(i + maxPageItems, estados.size()); i < l; i++){
          estado = (Estado) estados.elementAt(i);%>
  <pg:item>
  <tr class="<%=(i % 2 == 0 )?"filagris":"filaazul"%>" onMouseOver='cambiarColorMouse(this)' style="cursor:hand" >
    <td class="bordereporte"   
        onClick="copiarCampo('<%=estado.getdepartament_code ()%>',window.opener.document.forma.c_comboestado);" title="Haga clic aqui para seleccionar el Estado"><%=estado.getdepartament_code ()%></td>
    <td nowrap class="bordereporte"
		onClick="copiarCampo('<%=estado.getdepartament_code ()%>',window.opener.document.forma.c_comboestado);" title="Haga clic aqui para seleccionar el Estado"><%=estado.getdepartament_name ()%> </td>
  </tr>
  </pg:item>
  <%}%>
   <tr>
        <td td height="20" colspan="10" nowrap align="center">          <pg:index>
                <jsp:include page="/WEB-INF/jsp/googlesinimg.jsp" flush="true"/>
        </pg:index>
        </td>
  </tr>
  </pg:pager>  
</table>
</td>
</tr>
</table>
<%}%>
</form>
</div>
</body>
</html>
