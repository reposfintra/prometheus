<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
"http://www.w3.org/TR/html4/loose.dtd">
<%@page contentType="text/html"%>
<%@page session="true"%>
<%@page errorPage="../error/ErrorPage.jsp"%>
<%@page import="java.util.*"%>
<%@page import="com.vcc.operation.model.*"%>
<%@page import="com.vcc.util.*"%>
<%@page import="com.vcc.operation.model.beans.*, java.util.*"%>
<%@include file="/WEB-INF/InitModel.jsp"%>
<%@taglib uri="http://jsptags.com/tags/navigation/pager" prefix="pg" %>

<html>
<head>
<title>Listado de Paises</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link href="<%= BASEURL %>/css/estilo.css" rel="stylesheet" type="text/css"> 
</head>

<body>
  <div align="center">
    <% 
    String lenguaje = (String) session.getAttribute("idioma");
    model.idiomaService.cargarIdioma(lenguaje, "VerCiudad.jsp");
    Properties lista = model.idiomaService.getIdioma();
   String style = "simple";
   String position =  "bottom";
   String index =  "center";
   int maxPageItems = 10;
   int maxIndexPages = 10;

  	String nomdpto="", nompais="";
    Vector VecCiudades = model.ciudadservice.obtCiudades();
    Ciudad ciudad; 
	Estado estado;
	estado = model.estadoservice.obtenerestado(); 
    nomdpto = estado.getdepartament_name();
    nompais = estado.getpais_name(); 
	if ( VecCiudades.size() >0 ){
  %>
  </div>
  <table width="470" height="91" align="center"  border="1" align="center" cellpadding="4" cellspacing="1" bordercolor="#B7D0DC">
    <tr class="titulo">
      <td colspan="8" valign="middle" align="center"><%=lista.getProperty("CiudadesDe")%> <%=nomdpto%> (<%=nompais%>)</td>
    </tr>
    <tr class="subtitulos">
      <td width="101" align="center"><%=lista.getProperty("Codigo")%><%//Codigo%></td>
      <td width="279" align="center"><%=lista.getProperty("Nombre")%><%//Nombre%></td>

    </tr>
	<pg:pager
    items="<%=VecCiudades.size()%>"
    index="<%= index %>"
    maxPageItems="<%= maxPageItems %>"
    maxIndexPages="<%= maxIndexPages %>"
    isOffset="<%= true %>"
    export="offset,currentPageNumber=pageNumber"
    scope="request">
<%-- keep track of preference --%>

  <%
        for (int i = offset.intValue(), l = Math.min(i + maxPageItems, VecCiudades.size()); i < l; i++) {
            ciudad = (Ciudad) VecCiudades.elementAt(i);%>

		<pg:item>

    <tr class="letra" bgcolor="#B7D0DC" onMouseOver="bgColor= '#0092A6'" onMouseOut="bgColor='#B7D0DC'" style="cursor:hand" 
        onClick="window.open('<%=CONTROLLER%>?estado=Buscar&accion=Ciudad&pagina=ciudad.jsp&carpeta=ciudad&codpais=<%=ciudad.getpais_code()%>&codestado=<%=ciudad.getdepartament_code()%>&codigo=<%=ciudad.getcodciu()%>&mensaje=','','status=no,scrollbars=no,width=400,height=300,resizable=yes');">
      <td height="20" ><%=ciudad.getcodciu()%></td>
      <td height="20" ><%=ciudad.getnomciu()%></td>
     
    </tr>
	</pg:item>
  <%}%>
  <tr class="fila">
 	<td colspan="2" align="center" valign="middle">
	<pg:index>
	<jsp:include page="../WEB-INF/jsp/google.jsp" flush="true"/>
	</pg:index>	</td>
  </tr>
  </pg:pager>
</table>

  <%}
 else { %>
  <table width="100%">
    <tr>
      <td height="50" class='titulo'> </td>
    </tr>
    <tr>
      <td class='fila' height="60"  align="center"> <span class="mensajes"><%=lista.getProperty("MsgErrorBusq")%></span> </td>
    </tr>
    <tr>
      <td height="50"  class='titulo'>&nbsp; </td>
    </tr>
  </table>
  <%}%>
  <p><a style="cursor:hand" class="letra_resaltada" onclick="window.location='<%=CONTROLLER%>?estado=Menu&accion=Cargar&pagina=SeleccionarEstado.jsp&carpeta=ciudad&mensaje='" ><%=lista.getProperty("Atras")%></a></p>
</body>
</html>
