<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
"http://www.w3.org/TR/html4/loose.dtd">
<%@page contentType="text/html"%>
<%@page session="true"%>
<%@page errorPage="../error/ErrorPage.jsp"%>
<%@page import="java.util.*"%>
<%@page import="com.vcc.operation.model.*"%>
<%@page import="com.vcc.util.*"%>
<%@page import="com.vcc.operation.model.beans.*, java.util.*"%>
<%@include file="/WEB-INF/InitModel.jsp"%>

<html>
<head>
<title>SELECCIONAR ESTADO</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link href="<%= BASEURL %>/css/estilo.css" rel="stylesheet" type="text/css"> 
<script type='text/javascript' src="<%= BASEURL %>/js/validar.js"></script>
</head>

<body>
<% Properties lista = model.idiomaService.getIdioma();
   Iterator it;   
   List Paises, Estados;
   Pais pais; 
   Estado estado; 
   int i=0;

   String mensaje =request.getParameter("mensaje");
   String cpais = request.getParameter("pais");
   String cod="", nom="", men="", codpais="",nompais="" ;
   String Titulo=lista.getProperty("Ciudades");
   String Subtitulo =lista.getProperty("Informacion");//Seleccione el pais y el estado
   String Nompais = lista.getProperty("Pais");//Pais
   String Nomestado = lista.getProperty("Estado");//Estado
   String action= CONTROLLER + "?estado=Ver&accion=Ciudad&carpeta=ciudad&pagina=VerCiudad.jsp";
   int sw=0, sw1=0;
   if( mensaje.equals("cargado") ) {
       sw1=1;
	   model.paisservice.buscarpais(cpais);      
	   pais = model.paisservice.obtenerpais(); 
       codpais = pais.getPais_code();
       nompais = pais.getPais_name();
	   model.estadoservice.listarEstado(cpais);
   }
 
  		
 %>

<form name="forma" method="post" action="<%=action%>" onSubmit="return validarTCamposLlenos();">
  <table width="379" align="center"  border="1"  cellpadding="4" cellspacing="1" bordercolor="#B7D0DC">
    <tr class="titulo">
      <td colspan="2" align="center"><%=Titulo%></td>
    </tr>
    <tr class="subtitulos">
      <td colspan="2" ><%=Subtitulo%> </td>
    </tr>
    <tr class="fila">
      <td width="115" align="left" ><%=Nompais%></div></td>
      <td  >
        <select   class="texbox" onchange="cargarestado('<%=CONTROLLER%>?estado=Cargar&accion=Estadobusq&pais=',this);" name="combopais"  >
	   	 <%if (model.paisservice.existePaises()){
		       model.paisservice.listarpaises();
    	       Paises = model.paisservice.obtenerpaises();		
			   it = Paises.iterator();%>
			 <%if ( sw == 0 ){%>
			       <option value="" selected><%=lista.getProperty("Seleccione")%></option>
			 <%}
			   while (it.hasNext()){  
			       pais = (Pais) it.next();	
    	    	   if (pais.getPais_code().equals(codpais)){%>
					  <option value="<%=codpais%>" selected><%=nompais%></option>
			     <%}
			       else{%>
				       <option value="<%=pais.getPais_code()%>"><%=pais.getPais_name()%></option>
			     <%}
			   }%>
		 <%}
		   else {%>
		       <option value="" selected><%=lista.getProperty("SelectVacio")%></option>
		 <%}%>
      </select></td>
    </tr>
    <tr class="fila">
      <td width="264" ><%=Nomestado%></td>

      <td width="264" valign="middle"><select name="comboestado"  class="texbox" >
        <%   
		   if (sw1==1){
               Estados = model.estadoservice.obtenerEstados();
			   if ( model.estadoservice.existeEstadosxPais(codpais) ){ 
                   it=Estados.iterator();
		           while (it.hasNext()){  
		               estado = (Estado) it.next();	%>
                       <option value="<%=estado.getdepartament_code()%>"><%=estado.getdepartament_name()%></option>
        <%         }
		       }
			   else {%>
			       <option value="" selected><%=lista.getProperty("SelectVacio")%></option>
			  <% }
		   }
		   else {%>
		       <option value="" selected><%=lista.getProperty("Seleccione")%></option>
	    <% }%>
	  </select></td>

      
    </tr>
    
    <tr class="pie">
      <td colspan="2" align="center" ><input name="btnVer" type="submit" class="boton" id="btnVer3" value="<%=lista.getProperty("Buscar")%>" ></td>
    </tr>    
  </table>
</form>
<p><a style="cursor:hand" class="letra_resaltada" onclick="window.location='<%=CONTROLLER%>?estado=Idioma&accion=Cargar&idioma=<%=session.getAttribute("idioma")%>&pagina=inicio.htm&ruta=/'" ><%=lista.getProperty("Atras")%></a></p>
</body>
</html>
