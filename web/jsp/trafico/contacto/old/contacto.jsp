<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
"http://www.w3.org/TR/html4/loose.dtd">
<%@page contentType="text/html"%>
<%@page session="true"%>
<%@page errorPage="../error/ErrorPage.jsp"%>
<%@page import="java.util.*"%>
<%@page import="com.tsp.operation.model.*"%>
<%@page import="com.tsp.util.*"%>
<%@page import="com.tsp.operation.model.beans.*, java.util.*"%>
<%@include file="/WEB-INF/InitModel.jsp"%>
<html>
<head>
<title>CONTACTO</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link href="<%= BASEURL %>/css/estilo.css" rel="stylesheet" type="text/css"> 
<script type='text/javascript' src="<%= BASEURL %>/js/validar.js"></script>
<script type='text/javascript' src="<%= BASEURL %>/js/boton.js"></script>
<link href="../../../css/estilostsp.css" rel="stylesheet" type="text/css">
<link href="<%=BASEURL%>/css/estilostsp.css" rel="stylesheet" type="text/css">
</head>

<body <%if(request.getParameter("mensaje")!=null) { if(request.getParameter("mensaje").equalsIgnoreCase("MsgModificado") ){ %>onLoad="window.opener.location.reload();"<%} }%>>
<% 	Contacto contacto;
   	String mensaje =request.getParameter("mensaje")+"";
   	String men="", cod = "", tel1 = "", tel2 = "", tipo=" ", comp=" ", nomcia="", nomcont="";
   	String Titulo= "Ingresar Contacto";//AGREGAR CONTACTO
   	String Subtitulo ="Informacion";
   	String Ide = "Id";//Codigo
   	String Comp = "Compania";//Codigo
   	String action = CONTROLLER + "?estado=Insertar&accion=Contacto";

	int sw=0,swcon=-1; 
   	if (mensaje==null){
   		mensaje=" ";
	}
    if( mensaje.equals("Error") ) {
       	swcon=0;
	   	men ="Contacto ya existe!";
		if(request.getParameter("mens")!=null){
			men = request.getParameter("mens");
		}
    }		
    else if ( mensaje.equals("Agregado") ) {
     	men ="Contacto ingresado exitosamente!";	   
    }
   	else if ( mensaje.equals("Modificar") ) {
    	swcon=1;
	    sw = 1;
   	}
	else if ( mensaje.equals("MsgModificado") ) {
	    swcon=1;
	    sw = 1;	   
	    men = "Modificaci�n exitosa!";	   
   	}
	else if ( mensaje.equals("Anulado") ) {
        swcon = 0;
	    sw = 1;
	    men = "Anulacion Exitosa!";
   	}
	else if ( mensaje.equals("Error_Anular") ) {
	    swcon=0;
		sw = 1;
		men = "Error durante la anulacion!";
   	}
	
	if ( swcon == 0 ){ //swcon = 0 capturo la informacion de la pag anterior
        cod = request.getParameter("c_codigo");
	    comp = request.getParameter("c_cia");
		tipo = request.getParameter("c_tipo_contacto");
	} 
	else if ( swcon == 1) { //swcon = 1 capturo el objeto con la informaci�n
	    contacto = model.contactoService.obtenerContacto(); 
        cod = contacto.getCod_contacto();
		nomcont = contacto.getNomcontacto();
		comp = contacto.getCod_cia();
		nomcia = contacto.getNomcopania();
		tipo = contacto.getTipo();	
	}
	
    if ( sw==1 ){
        Titulo="Modificar Contacto";//MODIFICAR CONTACTO 
	    
		action = CONTROLLER + "?estado=Modificar&accion=Contacto&nomcont=" + nomcont + "&nomcia=" + nomcia;
    }
%>
<form name="forma" id="forma" method="post" action="<%=action%>" onSubmit="return validarTCamposLlenos();">
<table width="524" border="2" align="center">
  <tr>
    <td>
	<table width="99%" align="center" cellpadding="0">
  <tr>
    <td width="392"  class="subtitulo1"><p align="left"><%=Titulo%></p></td>
    <td width="392"  class="barratitulo"><img src="<%=BASEURL%>/images/titulo.gif" width="32" height="20"></td>
  </tr>
</table>
  <table width="99%" align="center" >
    
    <tr class="fila">
      <td width="28%">Contacto Id</td>
      <td width="72%" nowrap>
      <%if(sw!=1){%>
	  <input name="c_codigo" type="text" class="textbox" maxlength="10" value="<%=cod%>">
	  &nbsp;<a href="<%=BASEURL%>/jsp/trafico/contacto/seleccionarCto.jsp?mostrar=NO" target="_blank" class="Simulacion_Hiper" title="Haga clic aqui para seleccionar una Contacto." >Seleccionar...</a>
	  <%}else{%>
	  [ <%=cod%> ] <%=nomcont%><input name="c_codigo" type="hidden" value="<%=cod%>">
	  <%}%>
	  </td>
    </tr>
    <tr class="fila">
      <td>Compa�ia Id </td>
      <td nowrap>
      <%if(sw!=1){%>
        <input name="c_cia" type="text" class="textbox" id="c_cia" value="<%=comp%>">
        &nbsp;<a href="<%=BASEURL%>/jsp/trafico/contacto/seleccionarCia.jsp?mostrar=NO" target="_blank" class="Simulacion_Hiper" title="Haga clic aqui para seleccionar una Compa�ia." >Seleccionar...</a>
	  <%}else {%>
	  [ <%=comp%> ] <%=nomcia%><input name="c_cia" type="hidden" value="<%=comp%>">
	  <%}%>
</td>
    </tr>
	<tr class="fila">
      <td>Tipo de Contacto</td>
      <td> 
	  <select id="c_tipo" name="c_tipo" class="listmenu">
	  	<option value="">Seleccione</option>
		
	  	<% Vector tcont = (Vector) session.getAttribute("tipocont");//model.tipo_contactoService.listarTContactos();
		
		for (int i = 0; i < tcont.size(); i++){
			TipoContacto tcontac = (TipoContacto) tcont.elementAt(i);				
		    if (tcontac.getCodigo().equals(tipo)){%>
				<option value="<%=tcontac.getCodigo()%>" selected><%=tcontac.getDescripcion()%></option>
			<%}else{%>
				<option value="<%=tcontac.getCodigo()%>"><%=tcontac.getDescripcion()%></option>
			<%}
		}%>
      </select>	  </td>
    </tr>
  </table>
	
	</td>
	</tr>
  </table>
  <br>
  <table width="307" border="0" align="center">
     <% if (sw==0){%>
   <tr>
      <td colspan="2" align="center">
      <img src="<%=BASEURL%>/images/botones/aceptar.gif" name="mod"  height="21" onClick="forma.submit()" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" style="cursor:hand"> &nbsp;
	  <img src="<%=BASEURL%>/images/botones/cancelar.gif"  name="imgsalir"  height="21" onMouseOver="botonOver(this);" onClick="forma.reset();" onMouseOut="botonOut(this);" style="cursor:hand">&nbsp;
	  <img src="<%=BASEURL%>/images/botones/salir.gif"  name="imgsalir"  height="21" onMouseOver="botonOver(this);" onClick="parent.close();" onMouseOut="botonOut(this);" style="cursor:hand"></td>
    </tr>
    <%} else {%>

    <tr>
     <td colspan="2" align="center">
	 <img src="<%=BASEURL%>/images/botones/modificar.gif" name="mod"  height="21" onClick="forma.submit()" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" style="cursor:hand"> &nbsp;
	  <img src="<%=BASEURL%>/images/botones/anular.gif"  name="imgsalir" height="21" onMouseOver="botonOver(this);" onClick="location.href='<%=CONTROLLER%>?estado=Anular&accion=Contacto&codigo=<%=cod%>&ccia=<%=comp%>'" onMouseOut="botonOut(this);" style="cursor:hand">&nbsp;
	  <img src="<%=BASEURL%>/images/botones/salir.gif"  name="imgsalir" height="21" onMouseOver="botonOver(this);" onClick="parent.close();" onMouseOut="botonOut(this);" style="cursor:hand">
      </td>
    </tr>
  <%}%></table>
<%if(!men.equals("")){%>
	<p>
   <table border="2" align="center">
     <tr>
    <td><table width="100%"  border="1" align="center"  bordercolor="#F7F5F4" bgcolor="#FFFFFF">
      <tr>
        <td width="229" align="center" class="mensajes"><%=men%></td>
        <td width="29" background="<%=BASEURL%>/images/cuadronaranja.JPG">&nbsp;</td>
        <td width="58">&nbsp;</td>
      </tr>
    </table></td>
  </tr>
</table>
  </p>
  <%}%>

</form>
</body>
</html>