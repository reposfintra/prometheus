<%@page contentType="text/html"%>
<%@page session="true"%>
<%@page errorPage="../error/ErrorPage.jsp"%>
<%@page import="java.util.*"%>
<%@page import="com.tsp.operation.model.*"%>
<%@page import="com.tsp.util.*"%>
<%@page import="com.tsp.operation.model.beans.*, java.util.*"%>
<%@include file="/WEB-INF/InitModel.jsp"%>
<%@taglib uri="http://jsptags.com/tags/navigation/pager" prefix="pg" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<title>Busqueda...</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link href="<%= BASEURL %>/css/estilo.css" rel="stylesheet" type="text/css"> 
<script type='text/javascript' src="<%= BASEURL %>/js/validar.js"></script>
<script type='text/javascript' src="<%= BASEURL %>/js/boton.js"></script>
<script type='text/javascript' src="<%= BASEURL %>/js/reporte.js"></script>
<link href="../../../css/estilostsp.css" rel="stylesheet" type="text/css">
<link href="<%=BASEURL%>/css/estilostsp.css" rel="stylesheet" type="text/css">
<style type="text/css">
<!--
.Estilo1 {font-size: 10px}
-->
</style>
</head>

<body>
<%String style = "simple";
   	String position =  "bottom";
   	String index =  "center";
    String comp = "";
   	int maxPageItems = 7;
   	int maxIndexPages = 10;
	
%>
<form name="forma" method="post" action="<%=CONTROLLER%>?estado=Contacto&accion=SelectCia&pagina=seleccionarCia.jsp" onSubmit="return validarFormaContacto(this);">
<table width="501" border="2" align="center">
  <tr>
    <td>
<table width="99%" align="center" cellpadding="0">
  <tr>
    <td width="392"  class="subtitulo1"><p align="left">Busqueda de Compa&ntilde;ias</p></td>
    <td width="392"  class="barratitulo"><img src="<%=BASEURL%>/images/titulo.gif" width="32" height="20"></td>
  </tr>
</table>
<table width="99%" align="center" cellpadding="2" cellspacing="0">
  <tr class="subtitulos">
    <td class="fila" >**Digite la frase sobre la cual desea realizar la busqueda.</td>
    </tr>
  <tr class="fila">
    <td ><table width="92%"  border="0" cellpadding="0" cellspacing="0">
      <tr>
        <td width="61%"><input name="frase" id="frase" type="text" class="textbox" size="50" onKeyPress="soloTextoPorcentaje(event);"></td>
        <td width="39%">          <img src="<%=BASEURL%>/images/botones/buscar.gif" name="mod"  height="21" onClick="forma.submit()" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" style="cursor:hand"></td>
      </tr>
    </table></td>
  </tr>
  <tr class="fila">
    <td>
	  <span class="Simulacion_Hiper" style="cursor:hand " onClick='verNota();'>Ver formato de b�squeda...</span>
	</td>
  </tr>
</table>
</td>
</tr>
</table>

<table id="nota" width="59%"  align="center" style="display:none">
	  	<tr><td class="letra">
			<FIELDSET><legend><span class="subtitulo1 Estilo1">Formato de B�squeda</span></legend>
			<table width="100%"  border="0" cellpadding="0" cellspacing="0" class="letra Estilo1">
  				<tr>
    			  <td colspan="2">* La  b&uacute;squeda es realizada por  NOMBRE del Contacto.<br>
				  * Formato de  la frase para ejecutar la b&uacute;squeda. </td>
    			</tr>
  				<tr align="right"> <td width="12%" nowrap>Ejemplo</td>
  				  <td width="88%">&nbsp;</td></tr>
  				<tr>
    		      <td>&nbsp;</td>
    		      <td>** Para realizar una b&uacute;squeda donde el nombre contega el conjunto de letras '<i>RE</i>', en el campo de texto debe digitar:<i> %RE% </i></td>
  				</tr>
  				<tr>
    			  <td>&nbsp;</td>
    			  <td>** Para ver todos los registros, debe digitar: <i> % </i></td>
				</tr>
			</table>
  		</FIELDSET>
		</td></tr>
  </table>
</form>
<br>
<%if ( request.getParameter("mostrar")==null || request.getParameter("mostrar").equals("OK")){
	Vector nits = model.identidadService.obtVecIdentidad();
	%>
	<p>
   <table width="473" border="2" align="center">
     <tr>
    <td><table width="100%"  border="1" align="center"  bordercolor="#F7F5F4" bgcolor="#FFFFFF">
      <tr>
        <td width="229" align="center" class="mensajes">Resultados de la Busqueda: <%=session.getAttribute("frase")%></td>
        <td width="29" background="<%=BASEURL%>/images/cuadronaranja.JPG">&nbsp;</td>
        <td width="58">&nbsp;</td>
      </tr>
    </table></td>
  </tr>
</table>
  </p>
  
<br>
<table width="621" border="2" align="center">
  <tr>
    <td>
	<table width="100%" border="1" align="center" cellpadding="0" bordercolor="#F7F5F4">
  <tr>
    <td width="392"  class="subtitulo1"><p align="left">Compa&ntilde;ias</p></td>
    <td width="392"  class="barratitulo"><img src="<%=BASEURL%>/images/titulo.gif" width="32" height="20"></td>
  </tr>
</table>
<table width="99%" border="1" align="center"  bordercolor="#999999" bgcolor="#F7F5F4">
  <tr class="tblTitulo">
    <td width="27%">Codigo</td>
    <td width="60%">Nombre</td>
	<td width="13%">Consultar</td>
  </tr>
  <pg:pager    items="<%=nits.size()%>"
    index="<%= index %>"
    maxPageItems="<%= maxPageItems %>"
    maxIndexPages="<%= maxIndexPages %>"
    isOffset="<%= true %>"
    export="offset,currentPageNumber=pageNumber"
    scope="request">
	<%Identidad identidad;
	for (int i = offset.intValue(), l = Math.min(i + maxPageItems, nits.size()); i < l; i++){
          identidad = (Identidad) nits.elementAt(i);%>
  <pg:item>
  <tr class="<%=(i % 2 == 0 )?"filagris":"filaazul"%>" onMouseOver='cambiarColorMouse(this)' style="cursor:hand" >
    <td class="bordereporte"   
        onClick="copiarCampo('<%=identidad.getCedula()%>',window.opener.document.forma.c_cia);" title="Haga clic aqui para seleccionar la Compa�ia"><%=identidad.getCedula()%></td>
    <td nowrap class="bordereporte"><%=identidad.getNombre()%> </td>
	<td nowrap class="bordereporte"><div align="center"><img src="<%=BASEURL%>/images/botones/iconos/detalles.gif" name="mod"  height="21" onClick="window.open('<%=CONTROLLER%>?estado=Identidad&accion=Buscar&pagina=identidadMod.jsp&carpeta=jsp/hvida/identidad&ced=<%=identidad.getCedula()%>&soloinfo=<%=identidad.getNombre()%>&mensaje=','','HEIGHT=700,WIDTH=700,SCROLLBARS=YES,RESIZABLE=YES,MENUBAR=NO')"  style="cursor:hand"></div></td>
  </tr>
  </pg:item>
  <%}%>
   <tr>
        <td td height="20" colspan="10" nowrap align="center">          <pg:index>
                <jsp:include page="/WEB-INF/jsp/googlesinimg.jsp" flush="true"/>
        </pg:index>
        </td>
  </tr>
  </pg:pager>  
</table>
</td>
</tr>
</table>
<%}%>
</form>
</body>
</html>
