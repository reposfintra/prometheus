<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
"http://www.w3.org/TR/html4/loose.dtd">
<%@page contentType="text/html"%>
<%@page session="true"%>
<%@page errorPage="../error/ErrorPage.jsp"%>
<%@page import="java.util.*"%>
<%@page import="com.tsp.operation.model.*"%>
<%@page import="com.tsp.util.*"%>
<%@page import="com.tsp.operation.model.beans.*, java.util.*"%>
<%@include file="/WEB-INF/InitModel.jsp"%>
<%@taglib uri="http://jsptags.com/tags/navigation/pager" prefix="pg" %>

<html>
<head>
<title>Listado de Contactos</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link href="<%= BASEURL %>/css/estilostsp.css" rel="stylesheet" type="text/css"> 
<link href="../../../css/estilostsp.css" rel="stylesheet" type="text/css"> 
</head>
<script type='text/javascript' src="<%= BASEURL %>/js/validar.js"></script>
<script type='text/javascript' src="<%= BASEURL %>/js/boton.js"></script>
<script type='text/javascript' src="<%= BASEURL %>/js/reporte.js"></script>

<body>
  <%String style = "simple";
   	String position =  "bottom";
   	String index =  "center";
    String comp = "";
   	int maxPageItems = 10;
   	int maxIndexPages = 10;
   	%>
	<table width="804" border="2" align="center">
  <tr>
    <td>
<table width="100%" border="1" align="center" cellpadding="0" bordercolor="#F7F5F4">
  <tr>
    <td width="392"  class="subtitulo1"><p align="left">Contactos </p></td>
    <td width="392"  class="barratitulo"><img src="<%=BASEURL%>/images/titulo.gif" width="32" height="20"></td>
  </tr>
</table>
   	<table width="99%" border="1" align="center"  bordercolor="#999999" bgcolor="#F7F5F4">
	<%Vector VecContacto = model.contactoService.obtcontactos();
   	Contacto contacto;  
   	if ( VecContacto.size() > 0 ) { 
   	%>
  <tr class="tblTitulo" align="center" height="20">
    <td nowrap width="40%">Contacto</td>
    <td nowrap width="40%">Compa�ia</td>
	<td nowrap width="20%">Tipo de Contacto</td>
  </tr>
  <pg:pager    items="<%=VecContacto.size()%>"
    index="<%= index %>"
    maxPageItems="<%= maxPageItems %>"
    maxIndexPages="<%= maxIndexPages %>"
    isOffset="<%= true %>"
    export="offset,currentPageNumber=pageNumber"
    scope="request">
	<%for (int i = offset.intValue(), l = Math.min(i + maxPageItems, VecContacto.size()); i < l; i++){
          contacto = (Contacto) VecContacto.elementAt(i);%>
  <pg:item>
  <tr class="<%=(i % 2 == 0 )?"filagris":"filaazul"%>" onMouseOver='cambiarColorMouse(this)' style="cursor:hand"  onClick="window.open('<%=CONTROLLER%>?estado=Buscar&accion=Contacto&pagina=contacto.jsp&carpeta=jsp/trafico/contacto&codigo=<%=contacto.getCod_contacto()%>&compania=<%=contacto.getCod_cia()%>&mensaje=Modificar','','status=no,scrollbars=no,width=525,height=300,resizable=yes');">
    <td height="22" nowrap class="bordereporte">[ <%=contacto.getCod_contacto()%> ] <%=contacto.getNomcontacto()%></td>
    <td nowrap class="bordereporte">[ <%=contacto.getCod_cia()%> ] <%=contacto.getNomcopania()%></td>
	<td nowrap class="bordereporte"><%=contacto.getNomtipo()%></td>
  </tr>
  </pg:item>
  <%}%>
   <tr>
        <td td height="20" colspan="10" nowrap align="center">          <pg:index>
                <jsp:include page="/WEB-INF/jsp/googlesinimg.jsp" flush="true"/>
        </pg:index>
        </td>
    </tr>
    </pg:pager>    
</table>
</td>
</tr>
</table>
<%}
 else { %>
  	<p>
   <table width="425" border="2" align="center">
     <tr>
    <td><table width="100%"  border="1" align="center"  bordercolor="#F7F5F4" bgcolor="#FFFFFF">
      <tr>
        <td width="229" align="center" class="mensajes">No se encontraron resultados</td>
        <td width="29" background="<%=BASEURL%>/images/cuadronaranja.JPG">&nbsp;</td>
        <td width="58">&nbsp;</td>
      </tr>
    </table></td>
  </tr>
</table>
  </p>
<%}%>

<br>
<table width="804"  border="0" align="center">
  <tr>
    <td height="23"><img src="<%=BASEURL%>/images/botones/regresar.gif" name="mod" height="21" onclick="parent[1].location.href='<%=BASEURL%>/jsp/trafico/contacto/BuscarContacto.jsp'" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" style="cursor:hand"></td>
  </tr>
</table>

</body>
</html>
