<!--
- Autor : Ing. Rodrigo Salazar
- Modificado por: Ing. Sandra M Escalante G
- Date  : 02 Febrero 2006
- Copyrigth Notice : Fintravalores S.A. S.A
-->
<%--
-@(#)
--Descripcion : Pagina JSP, que se encarga de gestionar 
				el formulario para la insercion, modificacion y
				y eliminacion de datos Contactos en tablagen
--%>

<%-- Declaracion de librerias--%>
<%@page contentType="text/html"%>
<%@page session="true"%>
<%@page errorPage="../error/ErrorPage.jsp"%>
<%@page import="java.util.*"%>
<%@page import="com.tsp.operation.model.*"%>
<%@page import="com.tsp.util.*"%>
<%@page import="com.tsp.operation.model.beans.*, java.util.*"%>
<%@include file="/WEB-INF/InitModel.jsp"%>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link href="../../../css/estilostsp.css" rel="stylesheet" type="text/css">
<link href="<%=BASEURL%>/css/estilostsp.css" rel="stylesheet" type="text/css">
<% 	Contacto contacto= new Contacto();
   	String mensaje =(request.getParameter("mensaje")!= null)?request.getParameter("mensaje"):"";
   	String cod = "", tipo=" ", comp=" ", nomcia="", nomcont="";
   	int sw=0; 
       		
    if( mensaje.equals("Error...Contacto ya existe!") ) {
        cod = request.getParameter("c_codigo");
	    comp = request.getParameter("c_cia");
		tipo = request.getParameter("c_tipo_contacto");
    }		
    else if ( mensaje.equals("Modificar") ||  mensaje.equals("MsgModificado")) {
    	contacto = model.contactoService.obtenerContacto(); 
        cod = contacto.getCod_contacto();
		nomcont = contacto.getNomcontacto();
		comp = contacto.getCod_cia();
		nomcia = contacto.getNomcopania();
		tipo = contacto.getTipo();	
	    sw = 1;
		mensaje = (mensaje.equals("MsgModificado"))?"Modificacion exitosa!":"";
   	}
	else if ( mensaje.equals("Eliminar") ) {    
	    sw = 1;
	    mensaje = "Registro eliminado!";
   	}

	String action = (sw == 0)?CONTROLLER + "?estado=Insertar&accion=Contacto":
					CONTROLLER + "?estado=Modificar&accion=Contacto&nomcont=" + nomcont + "&nomcia=" + nomcia;
	String Titulo=(sw==0)?".: Ingresar Contacto":".: Modificar/Eliminar Contacto";%>
<title><%=Titulo%></title>
<script type='text/javascript' src="<%= BASEURL %>/js/validar.js"></script>
<script type='text/javascript' src="<%= BASEURL %>/js/boton.js"></script>
<script type='text/javascript' src="<%= BASEURL %>/js/funciones.js"></script>
</head>
<%String load = ( ( request.getParameter("mensaje") != null ) && 
				  ( ( request.getParameter("mensaje").equalsIgnoreCase("MsgModificado") ) ||
				  	( request.getParameter("mensaje").equalsIgnoreCase("Eliminar") ) ) ) ?
				"onLoad=window.opener.location.reload();redimensionar()":
				"onLoad=redimensionar()";%>
<body onResize="redimensionar()" <%=load%>>
<div id="capaSuperior" style="position:absolute; width:100%; height:100px; z-index:0; left: 0px; top: 0px;">
 <jsp:include page="/toptsp.jsp?encabezado=Ingresar Contacto"/>
</div>
<div id="capaCentral" style="position:absolute; width:100%; height:87%; z-index:0; left: 0px; top: 100px; overflow: scroll;">

<form name="forma" id="forma" method="post" action="<%=action%>" onSubmit="return validarTCamposLlenos();">
<table width="524" border="2" align="center">
  <tr>
    <td>
	<table width="99%" align="center" cellpadding="0">
  <tr>
    <td width="392"  class="subtitulo1"><p align="left"><%=Titulo%></p></td>
    <td width="392"  class="barratitulo"><img src="<%=BASEURL%>/images/titulo.gif" width="32" height="20"></td>
  </tr>
</table>
  <table width="99%" align="center" >
    
    <tr class="fila">
      <td width="28%">Contacto Id</td>
      <td width="72%" nowrap>
      <%if(sw!=1){%>
	  <input name="c_codigo" type="text" class="textbox" maxlength="10" value="<%=cod%>">
	  &nbsp;<a href="#" onClick="window.open('<%=BASEURL%>/jsp/trafico/contacto/seleccionarCto.jsp?mostrar=NO','','resizable=yes, scroll=no, menubar=no, status=no')" class="Simulacion_Hiper" title="Haga clic aqui para seleccionar una Contacto." >Seleccionar...</a>
	  <%}else{%>
	  [ <%=cod%> ] <%=nomcont%><input name="c_codigo" type="hidden" value="<%=cod%>">
	  <%}%>
	  </td>
    </tr>
    <tr class="fila">
      <td>Compa�ia Id </td>
      <td nowrap>
      <%if(sw!=1){%>
        <input name="c_cia" type="text" class="textbox" id="c_cia" value="<%=comp%>">
        &nbsp;<a href="#" onClick="window.open('<%=BASEURL%>/jsp/trafico/contacto/seleccionarCia.jsp?mostrar=NO','','resizable=yes, scroll=no, menubar=no, status=no')" class="Simulacion_Hiper" title="Haga clic aqui para seleccionar una Compa�ia." >Seleccionar...</a>
	  <%}else {%>
	  [ <%=comp%> ] <%=nomcia%><input name="c_cia" type="hidden" value="<%=comp%>">
	  <%}%>
</td>
    </tr>
	<tr class="fila">
      <td>Tipo de Contacto</td>
      <td> 
	  <select id="c_tipo" name="c_tipo" class="listmenu">
	  	<option value="">Seleccione</option>
		
	  	<% Vector tcont = (Vector) session.getAttribute("tipocont");//model.tipo_contactoService.listarTContactos();
		
		for (int i = 0; i < tcont.size(); i++){
			TipoContacto tcontac = (TipoContacto) tcont.elementAt(i);				
		    if (tcontac.getCodigo().equals(tipo)){%>
				<option value="<%=tcontac.getCodigo()%>" selected><%=tcontac.getDescripcion()%></option>
			<%}else{%>
				<option value="<%=tcontac.getCodigo()%>"><%=tcontac.getDescripcion()%></option>
			<%}
		}%>
      </select>	  </td>
    </tr>
  </table>
	
	</td>
	</tr>
  </table>
  <br>
  <table width="307" border="0" align="center">
     <% if (sw==0){%>
   <tr>
      <td colspan="2" align="center">
      <img src="<%=BASEURL%>/images/botones/aceptar.gif" name="mod"  height="21" onClick="validarFormContacto('<%=Titulo%>')" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" style="cursor:hand"> &nbsp;
	  <img src="<%=BASEURL%>/images/botones/cancelar.gif"  name="imgsalir"  height="21" onMouseOver="botonOver(this);" onClick="forma.reset();" onMouseOut="botonOut(this);" style="cursor:hand">&nbsp;
	  <img src="<%=BASEURL%>/images/botones/salir.gif"  name="imgsalir"  height="21" onMouseOver="botonOver(this);" onClick="parent.close();" onMouseOut="botonOut(this);" style="cursor:hand"></td>
    </tr>
    <%} else {%>

    <tr>
     <td colspan="2" align="center">
	 <img src="<%=BASEURL%>/images/botones/modificar.gif" name="mod"  height="21" onClick="validarFormContacto('<%=Titulo%>')" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" style="cursor:hand"> &nbsp;
	  <img src="<%=BASEURL%>/images/botones/eliminar.gif"  name="imgsalir" height="21" style="cursor:hand" onClick="location.href='<%=CONTROLLER%>?estado=Anular&accion=Contacto&codigo=<%=cod%>&ccia=<%=comp%>'" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);">&nbsp;
	  <img src="<%=BASEURL%>/images/botones/salir.gif"  name="imgsalir" height="21" onMouseOver="botonOver(this);" onClick="parent.close();" onMouseOut="botonOut(this);" style="cursor:hand">
      </td>
    </tr>
  <%}%></table>
<%if(!mensaje.equals("")){%>
	<p>
   <table border="2" align="center">
     <tr>
    <td><table width="100%"  border="1" align="center"  bordercolor="#F7F5F4" bgcolor="#FFFFFF">
      <tr>
        <td width="229" align="center" class="mensajes"><%=mensaje%></td>
        <td width="29" background="<%=BASEURL%>/images/cuadronaranja.JPG">&nbsp;</td>
        <td width="58">&nbsp;</td>
      </tr>
    </table></td>
  </tr>
</table>
  </p>
  <%}%>
</form>
</div>
</body>
</html>
