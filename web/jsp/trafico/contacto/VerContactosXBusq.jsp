<!--
- Autor : Ing. Rodrigo Salazar
- Modificado por: Ing. Sandra M Escalante G
- Date  : 02 Febrero 2006
- Copyrigth Notice : Fintravalores S.A. S.A
-->
<%--
-@(#)
--Descripcion : Pagina JSP, que se encarga de gestionar 
				el formulario que muestra los contactos
--%>

<%-- Declaracion de librerias--%>
<%@page contentType="text/html"%>
<%@page session="true"%>
<%@page errorPage="../error/ErrorPage.jsp"%>
<%@page import="java.util.*"%>
<%@page import="com.tsp.operation.model.*"%>
<%@page import="com.tsp.util.*"%>
<%@page import="com.tsp.operation.model.beans.*, java.util.*"%>
<%@include file="/WEB-INF/InitModel.jsp"%>
<%@taglib uri="http://jsptags.com/tags/navigation/pager" prefix="pg" %>

<html>
<head>
<title>.: Contactos por busqueda</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link href="../../../css/estilostsp.css" rel="stylesheet" type="text/css">
<link href="<%=BASEURL%>/css/estilostsp.css" rel="stylesheet" type="text/css">
<script type='text/javascript' src="<%=BASEURL%>/js/reporte.js"></script>
<script type='text/javascript' src="<%=BASEURL%>/js/boton.js"></script>
</head>

<body onResize="redimensionar()" onLoad="redimensionar()">
<div id="capaSuperior" style="position:absolute; width:100%; height:100px; z-index:0; left: 0px; top: 0px;">
 <jsp:include page="/toptsp.jsp?encabezado=Buscar Contactos"/>
</div>
<div id="capaCentral" style="position:absolute; width:100%; height:87%; z-index:0; left: 0px; top: 100px; overflow: scroll;">

<% 
   String style = "simple";
   String position =  "bottom";
   String index =  "center";
   int maxPageItems = 10;
   int maxIndexPages = 10;
   Vector VecContacto = model.contactoService.obtcontactos();
   Contacto contacto;  
   if (VecContacto.size() >0 ){%>
   <table width="417" border="2" align="center">
  <tr>
    <td>
<table width="99%" border="1" align="center" cellpadding="0" bordercolor="#F7F5F4">
  <tr>
    <td width="392"  class="subtitulo1"><p align="left">Contactos</p></td>
    <td width="392"  class="barratitulo"><img src="<%=BASEURL%>/images/titulo.gif" width="32" height="20"></td>
  </tr>
</table>
	<table width="99%" border="1" align="center"  bordercolor="#999999" bgcolor="#F7F5F4">
    <tr class="tblTitulo" align="center" height="20">
    <td nowrap width="40%">Contacto</td>
    <td nowrap width="40%">Compa�ia</td>
	<td nowrap width="20%">Tipo de Contacto</td>
  </tr>
  <pg:pager    items="<%=VecContacto.size()%>"
    index="<%= index %>"
    maxPageItems="<%= maxPageItems %>"
    maxIndexPages="<%= maxIndexPages %>"
    isOffset="<%= true %>"
    export="offset,currentPageNumber=pageNumber"
    scope="request">
	<%for (int i = offset.intValue(), l = Math.min(i + maxPageItems, VecContacto.size()); i < l; i++){
          contacto = (Contacto) VecContacto.elementAt(i);%>
  <pg:item>
  <tr class="<%=(i % 2 == 0 )?"filagris":"filaazul"%>" onMouseOver='cambiarColorMouse(this)' style="cursor:hand"  onClick="window.open('<%=CONTROLLER%>?estado=Buscar&accion=Contacto&pagina=contacto.jsp&carpeta=jsp/trafico/contacto&codigo=<%=contacto.getCod_contacto()%>&compania=<%=contacto.getCod_cia()%>&mensaje=Modificar','','status=no,resizable=yes');">
    <td class="bordereporte" height="22" nowrap>[ <%=contacto.getCod_contacto()%> ] <%=contacto.getNomcontacto()%></td>
    <td class="bordereporte" nowrap>[ <%=contacto.getCod_cia()%> ] <%=contacto.getNomcopania()%></td>
	<td class="bordereporte" nowrap><%=contacto.getNomtipo()%></td>
  </tr>
	</pg:item>
  <%}%>
   <tr>
        <td td height="20" colspan="10" nowrap align="center">          <pg:index>
                <jsp:include page="/WEB-INF/jsp/googlesinimg.jsp" flush="true"/>
        </pg:index>
        </td>
    </tr>
    </pg:pager>    
</table>
</td>
</tr>
</table>
<%}
 else { %>
<br>
   <table width="425" border="2" align="center">
     <tr>
    <td><table width="100%"  border="1" align="center"  bordercolor="#F7F5F4" bgcolor="#FFFFFF">
      <tr>
        <td width="229" align="center" class="mensajes">No se encontraron resultados</td>
        <td width="29" background="<%=BASEURL%>/images/cuadronaranja.JPG">&nbsp;</td>
        <td width="58">&nbsp;</td>
      </tr>
    </table></td>
  </tr>
</table>
<%}%>
<center>
	<br>
	<img src="<%=BASEURL%>/images/botones/regresar.gif" name="mod"  height="21" onClick="window.history.back();" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" style="cursor:hand">
	<img src="<%=BASEURL%>/images/botones/salir.gif" name="mod"  height="21" onClick="window.close();" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" style="cursor:hand">
	</center>
</div>
</body>
</html>
