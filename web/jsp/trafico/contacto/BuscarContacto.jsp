<!--
- Autor : Ing. Rodrigo Salazar
- Modificado por: Ing. Sandra M Escalante G
- Date  : 02 Febrero 2006
- Copyrigth Notice : Fintravalores S.A. S.A
-->
<%--
-@(#)
--Descripcion : Pagina JSP, que se encarga de gestionar 
				el formulario para realizar la busqueda de los contactos
--%>

<%-- Declaracion de librerias--%>
<%@page contentType="text/html"%>
<%@page session="true"%>
<%@page errorPage="../error/ErrorPage.jsp"%>
<%@page import="java.util.*"%>
<%@page import="com.tsp.operation.model.*"%>
<%@page import="com.tsp.util.*"%>
<%@page import="com.tsp.operation.model.beans.*, java.util.*"%>
<%@include file="/WEB-INF/InitModel.jsp"%>

<html>
<head>
<title>.: Buscar Contactos</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link href="<%= BASEURL %>/css/estilostsp.css" rel="stylesheet" type="text/css"> 
<link href="../../../css/estilostsp.css" rel="stylesheet" type="text/css"> 
<script type='text/javascript' src="<%= BASEURL %>/js/validar.js"></script>
<script type='text/javascript' src="<%= BASEURL %>/js/boton.js"></script>
</head>

<body onResize="redimensionar()" onLoad="redimensionar()">
<div id="capaSuperior" style="position:absolute; width:100%; height:100px; z-index:0; left: 0px; top: 0px;">
 <jsp:include page="/toptsp.jsp?encabezado=Buscar Contactos"/>
</div>
<div id="capaCentral" style="position:absolute; width:100%; height:87%; z-index:0; left: 0px; top: 100px; overflow: scroll;">

<form name="forma" method="post" action="<%=CONTROLLER%>?estado=Buscar&accion=Contacto&cmd=busqueda">
<table width="400" border="2" align="center">
  <tr>
    <td>
<table width="99%" align="center">
  <tr>
    <td width="392"  class="subtitulo1"><p align="left">Buscar Contactos </p></td>
    <td width="392"  class="barratitulo"><img src="<%=BASEURL%>/images/titulo.gif" width="32" height="20"></td>
  </tr>
</table>
  <table width="99%" align="center">
    <tr class="fila">
      <td width="30%" >Contacto Id</td>
      <td width="70%">
	  	<input name="c_codigo" type="text" class="textbox" id="nombre" size="20" maxlength="40" >
		&nbsp;<a href="#" onClick="window.open('<%=BASEURL%>/jsp/trafico/contacto/seleccionarCto.jsp?mostrar=NO&busqueda=ok','','resizable=yes, scroll=no, menubar=no, status=no')" class="Simulacion_Hiper" title="Haga clic aqui para seleccionar una Contacto." >Seleccionar...</a>
	  </td>
    </tr>
	<tr class="fila">
      <td >Compa�ia Id</td>
      <td >
	    <input name="c_cia" type="text" class="textbox" id="nombre" size="20" maxlength="40" >
		&nbsp;<a href="#" onClick="window.open('<%=BASEURL%>/jsp/trafico/contacto/seleccionarCia.jsp?mostrar=NO&busqueda=ok','','resizable=yes, scroll=no, menubar=no, status=no')" class="Simulacion_Hiper" title="Haga clic aqui para seleccionar una Compa�ia." >Seleccionar...</a>
	  </td>
    </tr>
	<tr class="fila">
      <td >Tipo de Contacto</td>
      <td ><input name="c_tipo" class="textbox" type="text" id="nombre" maxlength="40" >
	  </td>
    </tr>
    <tr class="fila">
      <td colspan="2">	    
	  </td>
    </tr>    
  </table>
  </td>
  </tr>
  </table>
<p align="center"><img src="<%=BASEURL%>/images/botones/buscar.gif" name="mod"  height="21" onClick="forma.submit()" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" style="cursor:hand">&nbsp;
<img src="<%=BASEURL%>/images/botones/detalles.gif" name="mod"  height="21" onClick="window.location.href='<%=CONTROLLER%>?estado=Menu&accion=Cargar&carpeta=/jsp/trafico/contacto&pagina=VerContactos.jsp&opcion=26&marco=no'" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" style="cursor:hand">&nbsp; 
<img src="<%=BASEURL%>/images/botones/salir.gif"  height="21" name="imgsalir"  onMouseOver="botonOver(this);" onClick="parent.close();" onMouseOut="botonOut(this);" style="cursor:hand"> </p>
<br>
<%if (request.getParameter("mensaje") != null){%>
<table width="473" border="2" align="center">
  <tr>
    <td><table width="100%"  border="1" align="center"  bordercolor="#F7F5F4" bgcolor="#FFFFFF">
        <tr>
          <td width="229" align="center" class="mensajes"><%=request.getParameter("mensaje")%></td>
          <td width="29" background="<%=BASEURL%>/images/cuadronaranja.JPG">&nbsp;</td>
          <td width="58">&nbsp;</td>
        </tr>
    </table></td>
  </tr>
</table>
<%}%>
</form>
</div>
</body>
</html>