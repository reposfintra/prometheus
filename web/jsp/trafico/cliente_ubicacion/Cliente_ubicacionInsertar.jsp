<!--
- Autor : Ing. Jose de la rosa
- Date  : 20 de enero del 2006
- Copyrigth Notice : Fintravalores S.A. S.A
-->
<%--
-@(#)
--Descripcion : Pagina JSP, que maneja el ingreso de las ubicaciones del cliente
--%>

<%-- Declaracion de librerias--%>
<%@page contentType="text/html"%>
<%@ page session="true"%>
<%@ page errorPage="../error/error.jsp"%>
<%@page import="com.tsp.operation.model.beans.*, java.util.*"%>
<%@ include file="/WEB-INF/InitModel.jsp"%>
<%@page import="com.tsp.operation.model.*"%>
<%@page import="com.tsp.util.*"%>
<%@ taglib uri="http://jakarta.apache.org/taglibs/input-0.90" prefix="input" %>
<html>
<head>
<title>Insertar Ubicaciones del Cliente</title>
<link href="<%= BASEURL %>/css/estilostsp.css" rel="stylesheet" type="text/css">
<script type='text/javascript' src="<%= BASEURL %>/js/boton.js"></script>
</head>
<script type='text/javascript' src="<%= BASEURL %>/js/funciones.js"></script>
<body onLoad="redimensionar();" onResize="redimensionar();">
<div id="capaSuperior" style="position:absolute; width:100%; height:100px; z-index:0; left: 0px; top: 0px;">
<jsp:include page="/WEB-INF/toptsp.jsp?encabezado=Ingresar Ubicación del Cliente"/>
</div>

<div id="capaCentral" style="position:absolute; width:100%; height:85%; z-index:0; left: 0px; top: 100px; overflow: scroll;">
<FORM name='form1' id='form1' method='POST' action='<%=CONTROLLER%>?estado=Cliente_ubicacion&accion=Insert'>
  <table width="480" border="2" align="center">
    <tr>
      <td>
        <table width="100%" class="tablaInferior">
          <tr class="fila">
            <td align="left" class="subtitulo1">Ubicación del Cliente</td>
            <td width="275" align="left" class="barratitulo"><img src="<%=BASEURL%>/images/titulo.gif" width="32" height="20"></td>
          </tr>
          <tr class="fila">
            <td align="left" >Cliente</td>
            <td valign="middle"><input name="codcli" type="text" class="textbox" id="codcli" size="15" maxlength="15"> <img src="<%=BASEURL%>/images/botones/iconos/obligatorio.gif"> <a onClick="window.open('<%=CONTROLLER%>?estado=Menu&accion=Cargar&pagina=consultasCliente.jsp&marco=no&carpeta=/consultas','','status=yes,scrollbars=no,width=550,height=150,resizable=yes')" style="cursor:hand"><img height="20" width="25" src="<%=BASEURL%>/images/botones/iconos/user.jpg"></a></td>
          </tr>
          <tr class="fila">
            <td width="201" align="left" >Codigo Ubicación</td>
            <td valign="middle">
				<select name="c_ubicacion" class="textbox"  id="c_ubicacion" >
					<%model.ubService.listarUbicaciones ();
					Vector vec = model.ubService.getUbicaciones (); 
					for(int i = 0; i<vec.size(); i++){	
						Ubicacion u = (Ubicacion) vec.elementAt(i);	%>
							<option value="<%=u.getCod_ubicacion()%>"><%=u.getDescripcion()%></option>
					<%}%>
				</select><img src="<%=BASEURL%>/images/botones/iconos/obligatorio.gif"></td>
          </tr>
      </table></td>
    </tr>
  </table>
<p>
<div align="center"><img src="<%=BASEURL%>/images/botones/aceptar.gif" style="cursor:hand" title="Agregar una ubicación del cliente" name="imgaceptar"  onclick="form1.submit();" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);">&nbsp;
<img src="<%=BASEURL%>/images/botones/cancelar.gif" style="cursor:hand" name="regresar" title="Limpiar todos los registros" onMouseOver="botonOver(this);" onClick="form1.reset();" onMouseOut="botonOut(this);" >&nbsp;
<img src="<%=BASEURL%>/images/botones/salir.gif" style="cursor:hand" name="salir" title="Salir al Menu Principal" onClick="parent.close();" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);"></div>
</p>
  <%String mensaje = (String) request.getAttribute("mensaje"); 
 %>
    <%  if(mensaje!=null){%>
            <br>
            <table border="2" align="center">
              <tr>
                <td><table width="100%"  border="1" align="center"  bordercolor="#F7F5F4" bgcolor="#FFFFFF">
                    <tr>
                      <td width="229" align="center" class="mensajes"><%=mensaje%></td>
                      <td width="29" background="<%=BASEURL%>/images/cuadronaranja.JPG">&nbsp;</td>
                      <td width="58">&nbsp;</td>
                    </tr>
                </table></td>
              </tr>
            </table>
    <%  }%>	
</FORM>
</div>
</body>
</html>
