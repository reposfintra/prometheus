<!--
- Autor : Ing. Jose de la rosa
- Date  : 20 de enero del 2006
- Copyrigth Notice : Fintravalores S.A. S.A
-->
<%--
-@(#)
--Descripcion : Pagina JSP, que maneja el listado de las ubicaciones de los clientes
--%>

<%-- Declaracion de librerias--%>
<%@page contentType="text/html"%>
<%@page session="true"%>
<%@page errorPage="/error/error.jsp"%>
<%@page import="java.util.*"%>
<%@page import="com.tsp.operation.model.*"%>
<%@page import="com.tsp.operation.model.beans.*"%>
<%@page import="com.tsp.util.*"%>
<%@include file="/WEB-INF/InitModel.jsp"%>
<%@taglib uri="http://jsptags.com/tags/navigation/pager" prefix="pg" %>
<html>
<head><title>Listar Ubicación del Cliente</title>
<link href="<%= BASEURL %>/css/estilostsp.css" rel="stylesheet" type="text/css">
<script type='text/javascript' src="<%= BASEURL %>/js/boton.js"></script>
<script type='text/javascript' src="<%= BASEURL %>/js/reporte.js"></script>
<link href="/css/estilostsp.css" rel="stylesheet" type="text/css">
</head>
<body onLoad="redimensionar();" onResize="redimensionar();">
<div id="capaSuperior" style="position:absolute; width:100%; height:100px; z-index:0; left: 0px; top: 0px;">
<jsp:include page="/WEB-INF/toptsp.jsp?encabezado=Listar Ubicación del Cliente"/>
</div>

<div id="capaCentral" style="position:absolute; width:100%; height:85%; z-index:0; left: 0px; top: 100px; overflow: scroll;"> 
<%  String c = (String)session.getAttribute("cliente");
    String u = (String)session.getAttribute("ubicacion");
	String distrito = (String) session.getAttribute ("Distrito");
    model.cliente_ubicacionService.searchDetalleCliente_ubicacion(c,u);
    Vector vec = model.cliente_ubicacionService.getCliente_ubicaciones();
    String style = "simple";
    String position =  "bottom";
    String index =  "center";
    int maxPageItems = 10;
    int maxIndexPages = 10;
if ( vec.size() >0 ){ 
%>
<table width="600" border="2" align="center">
    <tr>
      <td>
	  	  <table width="100%" align="center">
              <tr>
                <td width="373" class="subtitulo1">&nbsp;Ubicación del Cliente</td>
                <td width="427" class="barratitulo"><img src="<%=BASEURL%>/images/titulo.gif" width="32" height="20"></td>
              </tr>
        </table>
<table width="100%" border="1" bordercolor="#999999" bgcolor="#F7F5F4" align="center">
    <tr class="tblTitulo" align="center">
        <td width="274" nowrap >Cliente</td>
        <td width="298" nowrap >Codigo Ubicación</td>
    </tr>
    <pg:pager
        items="<%=vec.size()%>"
        index="<%= index %>"
        maxPageItems="<%= maxPageItems %>"
        maxIndexPages="<%= maxIndexPages %>"
        isOffset="<%= true %>"
        export="offset,currentPageNumber=pageNumber"
        scope="request">
<%
    for (int i = offset.intValue(), l = Math.min(i + maxPageItems, vec.size()); i < l; i++){
        Cliente_ubicacion cl = (Cliente_ubicacion) vec.elementAt(i);%>
        <pg:item>
            <tr class="<%=(i % 2 == 0 )?"filagris":"filaazul"%>" onMouseOver='cambiarColorMouse(this)'   style="cursor:hand" onClick="window.open('<%=CONTROLLER%>?estado=Cliente_ubicacion&accion=Search&c_cliente=<%=cl.getCliente()%>&c_ubicacion=<%=cl.getUbicacion()%>&listar=False','','status=no,scrollbars=no,width=650,height=350,resizable=yes');" >
                <td align="center" class="bordereporte"><%model.clienteService.datosCliente (cl.getCliente());
            											Cliente cli = model.clienteService.getCliente ();%><%=cli.getNomcli()%></td>
                <td align="center" class="bordereporte"><%model.ubService.buscarUbicacion (cl.getUbicacion(), distrito);
            											Ubicacion ub = model.ubService.getUb ();%><%=ub.getDescripcion()%></td>
            </tr>
      </pg:item>
<%  }%>
    <tr class="pie">
        <td td height="20" colspan="10" nowrap align="center">          <pg:index>
                <jsp:include page="/WEB-INF/jsp/googlesinimg.jsp" flush="true"/>
        </pg:index>
        </td>
    </tr>
    </pg:pager>        
</table>
</td>
</tr>
</table>
<br>
<%}else { %>
</p>
  <table border="2" align="center">
    <tr>
      <td><table width="410" height="73" border="1" align="center"  bordercolor="#F7F5F4" bgcolor="#FFFFFF">
          <tr>
            <td width="282" align="center" class="mensajes">Su b&uacute;squeda no arroj&oacute; resultados!</td>
            <td width="28" background="<%=BASEURL%>/images/cuadronaranja.JPG">&nbsp;</td>
            <td width="78">&nbsp;</td>
          </tr>
      </table></td>
    </tr>
  </table>
  <%}%>
   <table width="416" border="0" align="center">
    <tr>
      <td>
	  <img src="<%=BASEURL%>/images/botones/regresar.gif" style="cursor:hand" title="Buscar todos las ubicaciones de los clientes" name="buscar"  onClick="window.location='<%=BASEURL%>/jsp/trafico/cliente_ubicacion/Cliente_ubicacionBuscar.jsp'" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);">
	  </td>
	</tr>
</table>   
</div> 
</body>
</html>
