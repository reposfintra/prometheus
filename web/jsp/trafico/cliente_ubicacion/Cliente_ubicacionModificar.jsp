<!--
- Autor : Ing. Jose de la rosa
- Date  : 20 de enero del 2006
- Copyrigth Notice : Fintravalores S.A. S.A
-->
<%--
-@(#)
--Descripcion : Pagina JSP, que maneja la anulación de las ubicaciones del cliente
--%>

<%-- Declaracion de librerias--%>
<%@page contentType="text/html"%>
<%@page session="true"%>
<%@page import="java.util.*"%>
<%@page import="com.tsp.operation.model.*"%>
<%@page import="com.tsp.operation.model.beans.*"%>
<%@page import="com.tsp.util.*"%>
<%@include file="/WEB-INF/InitModel.jsp"%>
<html>
<head>
<title>Modificar Ubicación del Cliente</title>
<link href="<%= BASEURL %>/css/estilostsp.css" rel="stylesheet" type="text/css">
<script type='text/javascript' src="<%= BASEURL %>/js/boton.js"></script>
</head>
<script type='text/javascript' src="<%= BASEURL %>/js/validar.js"></script>
<body <%if(request.getParameter("reload")!=null){%>onLoad="parent.opener.location.reload();redimensionar();"<%}%> onResize="redimensionar();">
<div id="capaSuperior" style="position:absolute; width:100%; height:100px; z-index:0; left: 0px; top: 0px;">
<jsp:include page="/WEB-INF/toptsp.jsp?encabezado=Modificar Ubicación del Cliente"/>
</div>
        
<div id="capaCentral" style="position:absolute; width:100%; height:85%; z-index:0; left: 0px; top: 100px; overflow: scroll;"> 
<%
    Cliente_ubicacion c = model.cliente_ubicacionService.getCliente_ubicacion();    
    String mensaje = (String) request.getAttribute("mensaje");
%>
<FORM name='forma' id='forma' method='POST' action='<%=CONTROLLER%>?estado=Cliente_ubicacion&accion=Anular'>
    
	<table width="420" border="2" align="center">
      <tr>
        <td>
          <table width="100%" class="tablaInferior">
            <tr class="fila">
              <td align="left" class="subtitulo1">&nbsp;Ubicación del Cliente</td>
              <td align="left" class="barratitulo"><img src="<%=BASEURL%>/images/titulo.gif" width="32" height="20"></td>
            </tr>
            <tr class="fila">
              <td align="left" >Cliente</td>
              <td width="60%" valign="middle"><%Cliente cli = model.clienteService.getCliente ();%><%=cli.getNomcli()%><input name="c_cliente" type="hidden" id="c_cliente" value="<%=c.getCliente()%>"></td>
            </tr>
            <tr class="fila">
              <td width="40%" align="left" >Codigo Ubicación</td>
              <td valign="middle"><%Ubicacion ub = model.ubService.getUb ();%><%=ub.getDescripcion()%>
              <input name="c_ubicacion" type="hidden" id="c_ubicacion" value="<%=c.getUbicacion()%>"></td>
            </tr>
        </table></td>
      </tr>
    </table>
	<p>
	<div align="center"><img src="<%=BASEURL%>/images/botones/anular.gif" style="cursor:hand" title="Anular un codigo de discrepancia" name="anular"  onClick="forma.submit();" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);">&nbsp;
	<img src="<%=BASEURL%>/images/botones/salir.gif" style="cursor:hand" name="salir" title="Salir al Menu Principal" onMouseOver="botonOver(this);" onClick="parent.close();" onMouseOut="botonOut(this);" ></div>
	</p>
	<%  if(mensaje!=null){%>
		<table border="2" align="center">
		  <tr>
			<td><table width="100%"  border="1" align="center"  bordercolor="#F7F5F4" bgcolor="#FFFFFF">
			  <tr>
				<td width="229" align="center" class="mensajes"><%=mensaje%></td>
				<td width="29" background="<%=BASEURL%>/images/cuadronaranja.JPG">&nbsp;</td>
				<td width="58">&nbsp;</td>
			  </tr>
			</table></td>
		  </tr>
		</table>
	<%  }%>

</FORM>
</div>
</body>
</html>
