<!--
- Autor : Ing. Jose de la rosa
- Date  : 20 de enero del 2006
- Copyrigth Notice : Fintravalores S.A. S.A
-->
<%--
-@(#)
--Descripcion : Pagina JSP, que maneja la busqueda de las ubicaciones del cliente
--%>

<%-- Declaracion de librerias--%>
<%@page contentType="text/html"%>
<%@ page session="true"%>
<%@ page errorPage="../error/error.jsp"%>
<%@page import="com.tsp.operation.model.beans.*, java.util.*"%>
<%@ include file="/WEB-INF/InitModel.jsp"%>
<%@page import="com.tsp.operation.model.*"%>
<%@page import="com.tsp.util.*"%>
<%@ taglib uri="http://jakarta.apache.org/taglibs/input-0.90" prefix="input" %>
<html>
<head><title>Insertar Ubicación del Cliente</title>
<link href="<%= BASEURL %>/css/estilostsp.css" rel="stylesheet" type="text/css">
<script type='text/javascript' src="<%= BASEURL %>/js/boton.js"></script>
</head>
<body onLoad="redimensionar();" onResize="redimensionar();">
<div id="capaSuperior" style="position:absolute; width:100%; height:100px; z-index:0; left: 0px; top: 0px;">
<jsp:include page="/WEB-INF/toptsp.jsp?encabezado=Buscar Ubicación del Cliente"/>
</div>

<div id="capaCentral" style="position:absolute; width:100%; height:85%; z-index:0; left: 0px; top: 100px; overflow: scroll;"> 

<form name="form2" method="post" action="<%=CONTROLLER%>?estado=Cliente_ubicacion&accion=Search&listar=True&sw=True">
    <table width="380" border="2" align="center">
      <tr>
        <td><table width="100%" align="center"  class="tablaInferior">
            <tr>
				<td width="173" class="subtitulo1">Ubicación del Cliente </td>
				<td width="205" class="barratitulo"><img src="<%=BASEURL%>/images/titulo.gif" width="32" height="20"></td>
            </tr>
            <tr class="fila">
              <td width="173" align="left" valign="middle" >Cedula Cliente</td>
              <td width="205" valign="middle"><input name="c_cliente" type="text" class="textbox" id="c_cliente" size="15" maxlength="15"></td>
            </tr>
            <tr class="fila">
              <td width="173" align="left" valign="middle" >Codigo Ubicación</td>
              <td width="205" valign="middle"><input name="c_ubicacion" type="text" class="textbox" id="c_ubicacion" size="15" maxlength="6"></td>
            </tr>		
        </table></td>
      </tr>
    </table>
<p>
<div align="center"><img src="<%=BASEURL%>/images/botones/buscar.gif" style="cursor:hand" title="Buscar una ubicación del cliente" name="buscar"  onClick="form2.submit();" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);">&nbsp;
<img src="<%=BASEURL%>/images/botones/detalles.gif" style="cursor:hand" title="Buscar todos las ubicaciones del cliente" name="buscar"  onClick="window.location='<%=CONTROLLER%>?estado=Cliente_ubicacion&accion=Search&listar=True&sw=False'" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);">&nbsp;
<img src="<%=BASEURL%>/images/botones/salir.gif"  name="salir" style="cursor:hand" title="Salir al Menu Principal" onMouseOver="botonOver(this);" onClick="parent.close();" onMouseOut="botonOut(this);" ></div>
</p>
</form>
</div>
</body>
</html>
