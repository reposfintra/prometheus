<%@page session="true"%> 
<%@page errorPage="/error/ErrorPage.jsp"%>
<%@page import="com.tsp.util.Utility"%>
<%@page import="java.util.*" %>
<%@page import="com.tsp.operation.model.beans.*"%>
<%@include file="/WEB-INF/InitModel.jsp"%>
<%@ taglib uri="http://jakarta.apache.org/taglibs/input-0.90" prefix="input" %>

<!-- KAREN REALES -->
<html>
<head>
  <title>TIEMPO DE VIAJES</title>
  <link href="<%= BASEURL %>/css/estilostsp.css" rel='stylesheet'>
  <link href="../css/estilostsp.css" rel='stylesheet'>
  <script language="JavaScript" type="text/javascript" src="<%=BASEURL%>/js/Validaciones.js"></script>
    <script type='text/javascript' src="<%=BASEURL%>/js/boton.js"></script> 
</script>
  <script>

    function validarTV(){
	//alert("aki toy");
       var sw=0;
       if(theForm.agency.value=="NADA"){
		if(!confirm("Usted no selecciono ninguna agencia. \n Se va a generar el reporte para TODAS las agencias, esta seguro?")){
			sw=1;
		}
	   }
	   if(theForm.feci.value==""){
	   	sw=1;
		alert("Seleccione la  fecha inicial");
	   }
	   if(theForm.fecf.value==""){
	   	sw=1;
		alert("Seleccione la fecha final");
	   }
       
	   if(sw==0) {
	     theForm.imageField.disabled = true;
		 theForm.submit();
		}
	   
	   		
    }
  </script>
  
  <link href="../../../css/estilostsp.css" rel="stylesheet" type="text/css">
</head>
<body>
<div id="capaSuperior" style="position:absolute; width:100%; height:100px; z-index:0; left: 0px; top: 0px;">
 <jsp:include page="/WEB-INF/toptsp.jsp?encabezado=Despacho"/>
</div>

 <div id="capaCentral" style="position:absolute; width:100%; height:85%; z-index:0; left: 0px; top: 100px; overflow: scroll;"> 
  

  <FORM action='<%=CONTROLLER%>?estado=TiemposDeViaje&accion=Search' method='post' name='theForm'>
    <table width="53%"  border="2" align="center" class="tablaInferior">
      <tr>
        <td>          <table width="100%"  border="2">
              <tr>
                <td><table width="100%" border="0" cellpadding="0" cellspacing="0">
                  <tr>
                    <td height="22" colspan=2 class="subtitulo1">TIEMPO DE VIAJES </td>
                    <td width="264" class="barratitulo"><img src="<%=BASEURL%>/images/titulo.gif" width="32" height="20" align="left"></td>
                  </tr>
                </table>
                  
                  <TABLE width='100%' border='0' class='tablaInferior'>
                    <TR>
                      <TD height='20' colspan="4" class="subtitulo1">GENERAR REPORTE POR: </TD>
                    </TR>
                    <TR class="letra">
                      <TD width="18%" class="letraresaltada">Tipo de viaje</TD>
                      <TD><table cellpadding='0' cellspacing='0' width='100%' class='letra'>
                        <tr id='op1'>
                          <td colspan='2' class="letraresaltada"><input type='radio' name='op' value='ALL'  onclick='cambiarColorMouse(this); this.form.tipo.value=this.value'  checked='checked' class="textbox">
      Todos 
        <input  type='hidden' name='tipo' value='ALL'>
        <input  type='hidden' name='dias'></td>
                        </tr>
                        <tr id='op2' class="letraresaltada">
                          <td colspan="2">
                            <input type='radio' name='op' value='NA' onclick='cambiarColorMouse(this);  this.form.tipo.value=this.value' class="textbox">
      Nacionales      </td>
                        </tr>
                        <tr id='op2' class="letraresaltada">
                          <td colspan="2"><input type='radio' name='op' value='IN'  onClick='cambiarColorMouse(this);  this.form.tipo.value=this.value' class="textbox" >
Internacionales </td>
                        </tr>
                      </table></TD>
                      <TD width="15%"><span class="letraresaltada">Agencia</span></TD>
                      <TD width="29%"><%TreeMap agencia = model.agenciaService.listar(); 
				  String agen = request.getParameter("agency")!=null?request.getParameter("agency"):"NADA";
							 %>
                        <input:select name="agency" options="<%=agencia%>"   default='<%=agen%>' /></TD>
                    </TR>
                    <TR class="letra">
                      <TD height='14' colspan="4" class="letraresaltada">RANGO DE FECHAS DE SALIDA DE LA REMESA </TD>
                    </TR>
                    <TR class="letra">
                      <TD height='14' class="letraresaltada">Fecha inicio                      </TD>
                      <TD height='14' class="letraresaltada"><input name="feci" type="text" class="textbox" id="feci" onChange="formatoFecha(this);" onKeyPress="soloDigitos(event,'decNo');" readonly size="13" maxlength="16">
                        <a href="javascript:void(0)" onClick="if(self.gfPop)gfPop.fPopCalendar(document.theForm.feci);return false;" hidefocus> <img src="<%=BASEURL%>\js\Calendario\cal.gif" width="16" height="16" border="0" alt="De click aqui para escoger la fecha"></a></TD>
                      <TD height='14' class="letraresaltada">Fecha fin</TD>
                      <TD height='14' class="letraresaltada"><input name="fecf" type="text" class="textbox" id="fecf" onChange="formatoFecha(this);" onKeyPress="soloDigitos(event,'decNo');" readonly size="13" maxlength="16">
                        <a href="javascript:void(0)" onClick="if(self.gfPop)gfPop.fPopCalendar(document.theForm.fecf);return false;" hidefocus> <img src="<%=BASEURL%>\js\Calendario\cal.gif" width="16" height="16" border="0" alt="De click aqui para escoger la fecha"></a></TD>
                    </TR>
                  </TABLE></td>
              </tr>
            </table>
        </td>
      </tr>
    </table>
    <br>
    <div align="center">
      <img name="imageField" type="image" style="cursor:hand " src="<%=BASEURL%>/images/botones/buscar.gif" align="middle" width="87" height="21" border="0" onMouseOver="botonOver(this);"  onMouseOut="botonOut(this);" onClick="validarTV();">
      
      <br>
	  <%if(request.getParameter("mensaje")!=null){%>
      <table border="2" align="center">
        <tr>
          <td><table width="100%"  border="1" align="center"  bordercolor="#F7F5F4" bgcolor="#FFFFFF">
              <tr>
                <td width="229" align="center" class="mensajes"><span class="normal"><%=request.getParameter("mensaje")%></span></td>
                <td width="29" background="<%=BASEURL%>/images/cuadronaranja.JPG">&nbsp;</td>
                <td width="58">&nbsp;</td>
              </tr>
          </table></td>
        </tr>
      </table>
	  <%}%>
    </div>
  </FORM>
</div>
<iframe width=188 height=166 name="gToday:normal:agenda.js:gfPop:plugins.js" id="gToday:normal:agenda.js:gfPop:plugins_24.js" src="js/Calendario/ipopeng.htm" scrolling="no" frameborder="0" style="visibility:visible; z-index:999; position:absolute; left:-500px; top:0px;"> </iframe>

</body>
</html>
