<%@ include file="/WEB-INF/InitModel.jsp"%>

<HTML>
<HEAD>
<TITLE>Funcionamiento de la ventana de Modificación de Unidades</TITLE>
<META http-equiv=Content-Type content="text/html; charset=windows-1252">
<link href="../../../css/estilostsp.css" rel="stylesheet" type="text/css"> 
<link href="<%=BASEURL%>/css/estilostsp.css" rel="stylesheet" type="text/css"> 
<script type='text/javascript' src="<%=BASEURL%>/js/boton.js"></script>
</HEAD>
<BODY>  

  <table width="95%"  border="2" align="center">
    <tr>
      <td>
        <table width="100%" border="0" align="center">
          <tr  class="subtitulo">
            <td height="20"><div align="center">MODIFICACION DE UNIDADES </div></td>
          </tr>
          <tr class="subtitulo1">
            <td>Descripci&oacute;n del funcionamiento de la p&aacute;gina para Modificación de Unidades.</td>
          </tr>
          <tr>
            <td  height="18"  class="ayudaHtmlTexto"><p>&nbsp;</p>
            <p>Esta ventana permite modificar una unidad. 
            <br>Puede realizar modificaciones en la descripción y/o tipo de unidad.</p>
            
          </tr>
          
          <tr>
            <td  class="ayudaHtmlTexto"><div align="center"><img src="<%=BASEURL%>/images/ayuda/trafico/unidad/img4.JPG" ></div></td>
          </tr>   
          
          <tr>
            <td  class="ayudaHtmlTexto"><p>Si la unidad se modificó satisfactoriamente, se mostrará un mensaje de confirmación,
                    de lo contrario un mensaje de error indicará el motivo por el cual la unidad no se modificó.
                </p></td>
          </tr>
                              
         
      </table></td>
    </tr>
  </table>
  <table width="416" align="center">
	<tr>
	<td align="center">
		<img src="<%=BASEURL%>/images/botones/salir.gif" style="cursor:pointer" name="c_salir" onClick="window.close();" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" align="absmiddle">
	</td>
	</tr>
</table>
</BODY>
</HTML>