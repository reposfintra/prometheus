<!--
- Date  : 2005
- Autor: Ing Jose de la Rosa
- Modificado Ing Sandra Escalante
- Copyrigth Notice : Fintravalores S.A. S.A
-->   
<%--
-@(#)
--Descripcion : Pagina JSP, formulario de busqueda de unidades
--%>
<%@page contentType="text/html"%>
<%@page session="true"%>
<%@page errorPage="/error/ErrorPage.jsp"%>
<%@page import="java.util.*"%>
<%@page import="com.tsp.operation.model.*"%>
<%@page import="com.tsp.operation.model.beans.*"%>
<%@page import="com.tsp.util.*"%>
<%@include file="/WEB-INF/InitModel.jsp"%>
<%@ taglib uri="http://jakarta.apache.org/taglibs/input-0.90" prefix="input" %>
<html>
<head><title>.: Buscar Unidad</title>
<link href="<%= BASEURL %>/css/estilostsp.css" rel="stylesheet" type="text/css"> 
<script type='text/javascript' src="<%= BASEURL %>/js/boton.js"></script>
<script type='text/javascript' src="<%= BASEURL %>/js/validar.js"></script>
<link href="../../../css/estilostsp.css" rel="stylesheet" type="text/css">
</head>
<body onresize="redimensionar();" onload="redimensionar();form2.c_codigo.focus();">
<%
    Usuario usuario = (Usuario) session.getAttribute("Usuario");    
    String path = application.getRealPath(model.menuService.getRealUrl(request.getRequestURI()));
    String datos[] = model.menuService.getContenidoMenu(BASEURL,request.getRequestURI(),path);        
%>
<div id="capaSuperior" style="position:absolute; width:100%; height:100px; z-index:0; left: 0px; top: 0px;">
	<jsp:include page="/toptsp.jsp?encabezado=Buscar Unidad"/>
</div>
<div id="capaCentral" style="position:absolute; width:100%; height:87%; z-index:0; left: 0px; top: 100px; overflow: scroll;">
<form name="form2" method="post" action="<%=CONTROLLER%>?estado=Unidad&accion=Serch&listar=True"> 
    <table align="center" width="450" border="2"> 
        <tr>
            <td>
                <table width="100%" align="center">                    
                    <tr>
                        <td colspan="2"><table width="100%"  border="0" cellpadding="0" cellspacing="0" class="barratitulo">
                          <tr>
                            <td width="35%" class="subtitulo1">Buscar Unidad </td>
                            <td width="65%"><img align="left" src="<%=BASEURL%>/images/titulo.gif" width="32" height="20"><%=datos[0]%> </td>
                          </tr>
                        </table>                        </td>
                    </tr>
                    <tr class="fila">
                        <td width="108">Codigo</td>
                        <td width="205" nowrap><input name="c_codigo" type="text" class="textbox" id="c_codigo" onKeyPress="soloAlfa(event)" size="32" maxlength="30">
                        <img src="<%=BASEURL%>/images/botones/iconos/obligatorio.gif" width="10" height="10"></td>
                    </tr>        
                    <tr class="fila">
                        <td>Descripcion</td>
                        <td><input name="c_descripcion" type="text" class="textbox" id="c_descripcion" size="45">
                        <img src="<%=BASEURL%>/images/botones/iconos/obligatorio.gif" width="10" height="10"></td>
                    </tr> 
                    <tr class="fila">
                        <td>Tipo</td>
                        <td>
						<% TreeMap c_tipo = model.unidadTrfService.getTipos(); c_tipo.put("","");%>
						<input:select name="c_tipo"  attributesText="style='width:50%;' class='textbox'"  options="<%=c_tipo%>" default=""/>
                            <!--<select name="c_tipo" id="c_tipo" class="textbox">
                              <option value="V">VOLUMEN</option>
                              <option value="D">DISTANCIA</option>
                              <option value="P">PESO</option>
                              <option value="T">TIEMPO</option> 
                            </select>-->
                            <img src="<%=BASEURL%>/images/botones/iconos/obligatorio.gif" width="10" height="10">
                        </td>
                    </tr>                    
                </table>
            </td>
      </tr>
    </table>        
    <br>
   <table align=center>
    <tr class="pie">
            <td align='center' colspan="2">                
                <img title='Buscar' src="<%= BASEURL %>/images/botones/buscar.gif" onmouseover="botonOver(this);" onmouseout="botonOut(this);" name="c_Submit" value="Buscar" onclick='form2.submit();'></img>
                <img title='Ver detalles' src="<%= BASEURL %>/images/botones/detalles.gif" onmouseover="botonOver(this);" onmouseout="botonOut(this);" onclick="location.href='<%=CONTROLLER%>?estado=Unidad&accion=Serch&listar=True&todo=ok&c_codigo=&c_tipo=';"></img>
                <img title='Salir' src="<%= BASEURL %>/images/botones/salir.gif" onmouseover="botonOver(this);" onmouseout="botonOut(this);" name="c_regresar" value="Cancelar" onClick="parent.close();"></img>
            </td>            
      </tr>          
   </table>
</form>
</div>
<%=datos[1]%>
</body>
</html>
