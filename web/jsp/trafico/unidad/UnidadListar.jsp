<!--
- Date  : 2005
- Autor: Ing Jose de la Rosa
- Modificado Ing Sandra Escalante
- Copyrigth Notice : Fintravalores S.A. S.A
-->   
<%--
-@(#)
--Descripcion : Pagina JSP, tabla donde se muestran las unidades, acceso a modificar/anular unidad
--%>
<%@page contentType="text/html"%>
<%@page session="true"%>
<%@page errorPage="/error/ErrorPage.jsp"%>
<%@page import="java.util.*"%>
<%@page import="com.tsp.operation.model.*"%>
<%@page import="com.tsp.operation.model.beans.*"%>
<%@page import="com.tsp.util.*"%>
<%@include file="/WEB-INF/InitModel.jsp"%>
<%@taglib uri="http://jsptags.com/tags/navigation/pager" prefix="pg" %>
<html>
<head><title>.: Unidades</title>
<link href="<%= BASEURL %>/css/estilostsp.css" rel="stylesheet" type="text/css"> 
<link href="../../../css/estilostsp.css" rel="stylesheet" type="text/css"> 
<script type='text/javascript' src="<%= BASEURL %>/js/boton.js"></script>
<script type='text/javascript' src="<%= BASEURL %>/js/reporte.js"></script> 
</head>
<body onresize="redimensionar()" onload = 'redimensionar()'>
<%     
    String style = "simple";
    String position =  "bottom";
    String index =  "center";
    int maxPageItems = 20;
    int maxIndexPages = 10;   

    Vector unidades = model.unidadTrfService.getDetalleUnidades();%>
<br>
<div id="capaSuperior" style="position:absolute; width:100%; height:100px; z-index:0; left: 0px; top: 0px;">
	<jsp:include page="/toptsp.jsp?encabezado=Listar Unidad"/>
</div>
<div id="capaCentral" style="position:absolute; width:100%; height:87%; z-index:0; left: 0px; top: 100px; overflow: scroll;">
<% if( unidades.size() > 0 ){ %>
<table align="center" width="450" border="2">
    <tr>
        <td>
            <table width='100%' class="tablaInferior">
                <tr>
                    <td>
                        <table width="100%"  border="0" cellpadding="0" cellspacing="0" class="barratitulo">                
                            <tr>
                                <td width="87" class="subtitulo1" >Unidades</td>
                                <td width="423" class="barratitulo" colspan='2'><img src="<%=BASEURL%>/images/titulo.gif" width="32" height="20"></td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                	<td>
		                <table width="100%" border="1" align="center" bordercolor="#999999" bgcolor="#F7F5F4">
                    		<tr class="tblTitulo">
                        		<td width="60" align=center> Codigo</td>
								<td width="340" align=center> Descripcion</td>
								<td width="88" align=center> Tipo</td>
                    		</tr>
							<pg:pager
								items="<%=unidades.size()%>"
								index="<%= index %>"
								maxPageItems="<%= maxPageItems %>"
								maxIndexPages="<%= maxIndexPages %>"
								isOffset="<%= true %>"
								export="offset,currentPageNumber=pageNumber"
								scope="request">
                            <%for (int i = offset.intValue(), l = Math.min(i + maxPageItems, unidades.size()); i < l; i++){
                            	Unidad u = (Unidad) unidades.elementAt(i);%>
                         	<pg:item>
                    		<tr class="<%=(i % 2 == 0 )?"filagris":"filaazul"%>" onMouseOver='cambiarColorMouse(this)'   style="cursor:hand" title="Modificar Unidad..." onClick="window.open('<%=CONTROLLER%>?estado=Unidad&accion=Serch&c_codigo=<%=u.getCodigo()%>&c_tipo=<%=u.getTipo()%>&listar=False','myWindow','status=yes,scrollbars=no,width=700,height=400,resizable=yes');">
                        		<td class='bordereporte' align=center nowrap><span class="style7"><%=u.getCodigo()%></span></td>
		                        <td class='bordereporte' align=center nowrap><span class="style7"><%=u.getDescripcion()%></span></td>
        		                <td class='bordereporte' align=center nowrap><span class="style7"><%=u.getNtipo()%></span></td>
                    		</tr>
                            </pg:item>
                            <%}%>
							<tr class="pie">
								<td td height="20" colspan="3" nowrap align="center">          
									<pg:index>
										<jsp:include page="/WEB-INF/jsp/googlesinimg.jsp" flush="true"/>
									</pg:index>
								</td>
							</tr>
                        	</pg:pager>        
                		</table>
					</td>
				</tr>
			</table>
		</td>
	</tr>
</table>
<% }else {%>
<br>
<table width=450 border=2 align=center>
		<tr>
			<td>
				<table width=100%  border=1 align=center  bordercolor=#F7F5F4 bgcolor=#FFFFFF>
					<tr>
						<td width=350 align=center class=mensajes>Su busqueda no arrojo resultados!</td>
						<td width=100><img src="<%= BASEURL %>/images/cuadronaranja.JPG"></td>
					</tr>
				</table>
			</td>
		</tr>
	</table>
<%}%>
<br>
<table align=center width='530'>
    <tr>    
        <td align="center">
			<img title='Regresar' src="<%= BASEURL %>/images/botones/regresar.gif" onmouseover="botonOver(this);" onmouseout="botonOut(this);" onclick="location.href=('<%=BASEURL%>/jsp/trafico/unidad/UnidadBuscar.jsp');">
			<img title='Salir' src="<%= BASEURL %>/images/botones/salir.gif" onmouseover="botonOver(this);" onmouseout="botonOut(this);" onclick="window.close();">
		</td>
    </tr>
</table>
</div>
</body>
</html>
