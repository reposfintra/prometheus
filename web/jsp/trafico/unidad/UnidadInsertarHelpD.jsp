<%@ include file="/WEB-INF/InitModel.jsp"%>

<html>
<head>
<title>Descripci&oacute;n de campos Ingreso  de Unidad</title>

<script type='text/javascript' src="<%=BASEURL%>/js/boton.js"></script>
<link href="<%=BASEURL%>/css/estilostsp.css" rel="stylesheet" type="text/css"> 
</head>

<body>
<% String BASEIMG = BASEURL +"/images/botones/"; %> 
<br>
<table width="594"  border="2" align="center">
  <tr>
    <td width="635" >
      <table width="100%" border="0" align="center">
        <tr  class="subtitulo">
          <td height="24" colspan="2"><div align="center">INGRESAR UNIDAD </div></td>
        </tr>
        <tr class="subtitulo1">
          <td colspan="2">&nbsp;</td>
        </tr>
        <tr>
          <td width="123" class="fila">C�digo:</td>
          <td width="551"  class="ayudaHtmlTexto"> El codigo es utilizado como un identificador
          en el sistema, utilice un c�digo que haga referencia a la unidad, P. Ej.<br> Descripci�n: CAJAS<br>C�digo: CAJ</td>
        </tr>
        <tr>
          <td  class="fila"> Descripci�n:</td>
          <td  class="ayudaHtmlTexto"> Nombre de la Unidad. </td>
        </tr>
        <tr>
          <td width="123"  class="fila"> Tipo:</td>
          <td width="551"  class="ayudaHtmlTexto">Tipo de Unidad, puede ser DISTANCIA, PESO, TIEMPO, VOLUMEN. </td>
        </tr>
        
	<tr>
          <td width="123"  class="fila"> Bot&oacute;n Aceptar </td>
          <td width="551"  class="ayudaHtmlTexto">Ingresa la unidad. </td>
        </tr>
        <tr>
          <td width="123"  class="fila"> Bot&oacute;n Cancelar </td>
          <td width="551"  class="ayudaHtmlTexto">Limpia el formulario. </td>
        </tr>
		<tr>
		  <td  class="fila">Bot&oacute;n Salir </td>
	      <td  class="ayudaHtmlTexto">Cierra la ventana. </td>
		</tr>
      </table>
    </td>
  </tr>
</table>
<br>
<table width="416" align="center">
	<tr>
	<td align="center">
		<img src="<%=BASEURL%>/images/botones/salir.gif" style="cursor:pointer" name="c_salir" onClick="window.close();" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" align="absmiddle">
	</td>
	</tr>
</table>
<p>&nbsp;</p>

<p>&nbsp;</p>
<p>&nbsp; </p>
</body>
</html>