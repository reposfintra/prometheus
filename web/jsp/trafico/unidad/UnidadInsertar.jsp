<!--
- Date  : 2005
- Autor: Ing Jose de la Rosa
- Modificado Ing Sandra Escalante
- Copyrigth Notice : Fintravalores S.A. S.A
-->   
<%--
-@(#)
--Descripcion : Pagina JSP, formulario de registro de unidades (redireccionado a tablagen)
--%>
<%@page contentType="text/html"%>
<%@page session="true"%>
<%@page import="java.util.*"%>
<%@page import="com.tsp.operation.model.*"%>
<%@page import="com.tsp.operation.model.beans.*"%>
<%@page import="com.tsp.util.*"%>
<%@include file="/WEB-INF/InitModel.jsp"%>
<%@ taglib uri="http://jakarta.apache.org/taglibs/input-0.90" prefix="input" %>
<html>
    <head>
        <title>.: Ingresar Unidad</title>
        <link href="<%= BASEURL %>/css/estilostsp.css" rel="stylesheet" type="text/css"> 
        <link href="../../../css/estilostsp.css" rel="stylesheet" type="text/css"> 
        <script type='text/javascript' src="<%= BASEURL %>/js/boton.js"></script>
    </head>
    <script type='text/javascript' src="<%= BASEURL %>/js/validar.js"></script>
    <script type='text/javascript' src="<%= BASEURL %>/js/funciones.js"></script>
    <body onresize="redimensionar();" onload = 'redimensionar(); forma.c_codigo.focus();'>
<%
    String mensaje = (String) request.getAttribute("mensaje");
	String cod = "";
	String desc = "";
	if ( (mensaje != null) && ( mensaje.equals ("El codigo de Unidad digitado ya existe!") ) ){
		cod = request.getParameter("c_codigo");
		desc = request.getParameter("c_descripcion");	
	}
        
    String path = application.getRealPath(model.menuService.getRealUrl(request.getRequestURI()));
    String datos[] = model.menuService.getContenidoMenu(BASEURL,request.getRequestURI(),path);        
%>
        <div id="capaSuperior" style="position:absolute; width:100%; height:100px; z-index:0; left: 0px; top: 0px;">
            <jsp:include page="/toptsp.jsp?encabezado=Ingresar Unidad"/>
        </div>
        <div id="capaCentral" style="position:absolute; width:100%; height:87%; z-index:0; left: 0px; top: 100px; overflow: scroll;">
            <form name='forma' id='forma' method='POST' action=''>
            <table align="center" width="450" border="2"> 
                <tr>
                    <td>
                    <table width="100%" align="center">
                    <tr>
                        <td colspan="2" align="center"><div align="left">
                            <table width="100%"  border="0" cellpadding="0" cellspacing="0" class="barratitulo">
                                <tr>
                                    <td width="34%" class="subtitulo1">Unidad</td>
                                    <td width="66%"><img align="left" src="<%=BASEURL%>/images/titulo.gif" width="32" height="20"><%=datos[0]%>    </td>                                    
                                </tr>
                            </table>
                        </div></td>
                    </tr>                        
                    <tr class="fila">
                        <td width="119">Codigo</td>
                        <td width="227" nowrap><input name="c_codigo" type="text" class="textbox" id="c_codigo" onKeyPress="soloAlfa(event)" value="<%= cod %>" size="35" maxlength="30">
                        <img src="<%=BASEURL%>/images/botones/iconos/obligatorio.gif" width="10" height="10"></td>
                    </tr>
                    <tr class="fila">
                        <td nowrap>Descripcion</td>
                        <td><input name="c_descripcion" type="text" class="textbox" id="c_descripcion" value="<%= desc %>" size="45" >
                        <img src="<%=BASEURL%>/images/botones/iconos/obligatorio.gif" width="10" height="10"></td>
                    </tr>
                    <tr class="fila">
                    <td>Tipo</td>
                    <td>
						<% TreeMap c_tipo = model.unidadTrfService.getTipos(); %>
                        <input:select name="c_tipo"  attributesText="style='width:50%;' class='textbox'"  options="<%=c_tipo%>" default="<%= (request.getParameter("c_tipo")!=null)?request.getParameter("c_tipo"):"D" %>"/>
                        <img src="<%=BASEURL%>/images/botones/iconos/obligatorio.gif" width="10" height="10">
                    </td>
                </tr>                        
                    </table>
                </td>
                </tr>
            </table>
            <br>
            <table align=center>
                <tr>
                    <td align='center' colspan="2" >
                        <img title='Ingresar' src="<%= BASEURL %>/images/botones/aceptar.gif" onmouseover="botonOver(this);" onmouseout="botonOut(this);" onclick="forma.action='<%=CONTROLLER%>?estado=Unidad&accion=Insert';TCamposLlenos();" name="c_Submit" value="Ingresar">
                        <img title='Cancelar' src="<%= BASEURL %>/images/botones/cancelar.gif" onmouseover="botonOver(this);" onmouseout="botonOut(this);" name="c_regresar" onClick="borrarTexto(forma);">
                        <img title='Salir' src="<%= BASEURL %>/images/botones/salir.gif" onmouseover="botonOver(this);" onmouseout="botonOut(this);" name="c_regresar" onClick="window.close();">
                    </td>
                </tr>
            </table>
    <%  if(mensaje!=null){%>
            <br>
            <table width=450 border=2 align=center>
                <tr>
                    <td>
                        <table width="100%  border=1 align=center  bordercolor=#F7F5F4 bgcolor=#FFFFFF>"
                            <tr>
                                <td width=350 align=center class=mensajes><%= mensaje %></td>
                                <td width=100><img src="<%= BASEURL %>/images/cuadronaranja.JPG"></td>
                            </tr>
                        </table>
                    </td>
                </tr>
            </table>                        
    <%}%>	
            </form>
        </div>
<%=datos[1]%>        
    </body>
</html>
