<!--
- Date  : 2005
- Autor: Ing Jose de la Rosa
- Modificado Ing Sandra Escalante
- Copyrigth Notice : Fintravalores S.A. S.A
-->   
<%--
-@(#)
--Descripcion : Pagina JSP, formulario donde se permite modificar/eliminar unidad
--%>
<%@page contentType="text/html"%>
<%@page session="true"%>
<%@page import="java.util.*"%>
<%@page import="com.tsp.operation.model.*"%>
<%@page import="com.tsp.operation.model.beans.*"%>
<%@page import="com.tsp.util.*"%>
<%@include file="/WEB-INF/InitModel.jsp"%>
<%@ taglib uri="http://jakarta.apache.org/taglibs/input-0.90" prefix="input" %>
<html>
<head>
<title>.: Modificar/Eliminar Unidad</title>
<link href="<%= BASEURL %>/css/estilostsp.css" rel="stylesheet" type="text/css"> 
<link href="../../../css/estilostsp.css" rel="stylesheet" type="text/css"> 
<script type='text/javascript' src="<%= BASEURL %>/js/boton.js"></script>
</head>
<script type='text/javascript' src="<%= BASEURL %>/js/validar.js"></script>
<body <%if(request.getParameter("reload")!=null){%>onLoad="parent.opener.location.reload();redimensionar();"<%}else {%>onload = "redimensionar();"<%}%> onresize="redimensionar()" >
<div id="capaSuperior" style="position:absolute; width:100%; height:100px; z-index:0; left: 0px; top: 0px;">
	<jsp:include page="/toptsp.jsp?encabezado=Modificar/Eliminar Unidad"/>
</div>
<div id="capaCentral" style="position:absolute; width:100%; height:87%; z-index:0; left: 0px; top: 100px; overflow: scroll;">
<%
//    Usuario usuario = (Usuario) session.getAttribute("Usuario");    
    String mensaje = (String) request.getAttribute("mensaje");
    Vector vec = new Vector(); 
    Unidad u = model.unidadTrfService.getUnidad();    
    
    String path = application.getRealPath(model.menuService.getRealUrl(request.getRequestURI()));
    String datos[] = model.menuService.getContenidoMenu(BASEURL,request.getRequestURI(),path);        
%>
<FORM name='forma' id='forma' method='POST' onSubmit="" action=''>    
    <table align="center" width="416" border="2">
        <tr>
            <td>
                <table width="100%" align="center">
                    <tr>
                        <td width="104" class="subtitulo1">Información</td>
                        <td width="288" class="barratitulo"><img align="left" src="<%=BASEURL%>/images/titulo.gif" width="32" height="20"><%=datos[0]%></td>
                    </tr>                                                         
					<tr class="fila">
						<td width="104">Codigo</td>
						<td width="288" nowrap><%=u.getCodigo()%><input type="hidden" name="c_codigo" value="<%=u.getCodigo()%>"></td>
					</tr>
					<tr class="fila">
						<td nowrap>Descripcion</td>
						<td><input name="c_descripcion" type="text" class="textbox" id="c_descripcion" value="<%=u.getDescripcion()%>" size="50">
						<img src="<%=BASEURL%>/images/botones/iconos/obligatorio.gif" width="10" height="10"></td>
					</tr>    
					<tr class="fila">
						<td>Tipo</td>
						<td>
							<input name="c_atipo" type="hidden" value="<%=u.getTipo()%>">
							<% TreeMap c_tipo = model.unidadTrfService.getTipos(); 
								String tipo = u.getTipo();%>
							<input:select name="c_tipo"  attributesText="style='width:50%;' class='textbox'"  options="<%=c_tipo%>" default="<%= tipo %>"/>
							<img src="<%=BASEURL%>/images/botones/iconos/obligatorio.gif" width="10" height="10">
						</td>
					</tr>             
    			</table>
            </td>
        </tr>
    </table>
    <br>
    <table align=center>
        <tr >
            <td align='center' colspan="3" >
					<img title='Modificar' src="<%= BASEURL %>/images/botones/modificar.gif" onmouseover="botonOver(this);" onmouseout="botonOut(this);" onclick="forma.action='<%=CONTROLLER%>?estado=Unidad&accion=Update&sw=';validarUnidad();" name="c_Submit1" value="Modificar"></img>
					<img title='Anular' src="<%= BASEURL %>/images/botones/eliminar.gif" onmouseover="botonOver(this);" onmouseout="botonOut(this);" onclick="forma.action='<%=CONTROLLER%>?estado=Unidad&accion=Anular';forma.submit();" name="c_Submit2" value="Anular"></img>
					<img title='Salir' src="<%= BASEURL %>/images/botones/salir.gif" onmouseover="botonOver(this);" onmouseout="botonOut(this);" onclick="window.close()"></img>                
            </td>
        </tr>        
    </table>
	<%  if(mensaje!=null){ %>
	<br>
		<table width=450 border=2 align=center>
		<tr>
			<td>
				<table width=100%  border=1 align=center  bordercolor=#F7F5F4 bgcolor=#FFFFFF>
					<tr>
						<td width=350 align=center class=mensajes><%= mensaje %></td>
						<td width=100><img src="<%= BASEURL %>/images/cuadronaranja.JPG"></td>
					</tr>
				</table>
			</td>
		</tr>
	</table>
        <% } %>
</FORM>
</div>
<%=datos[1]%>
</body>
<script>
function validarUnidad(){
	if ((forma.c_descripcion.value == "") || (forma.c_tipo.value == '(vacio)')){
		alert("Debe ingresar los datos necesarios para diligenciar el formulario");		
		return false;
	}
	forma.submit();
}
</script>
</html>
