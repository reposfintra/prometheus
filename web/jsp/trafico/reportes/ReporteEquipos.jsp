<!--
- Autor : Ing. Tito Andrés Maturana
- Date  : 04 de enero de 2006
- Copyrigth Notice : Fintravalores S.A. S.A
-->
<%--
-@(#)
--Descripcion : Pagina JSP, que maneja la generación del reporte
				de stickers utilizados.
--%>
<%@page contentType="text/html"%>
<%@page session="true"%>
<%@page import="java.util.*"%>
<%@page import="com.tsp.operation.model.*"%>
<%@page import="com.tsp.operation.model.beans.*"%>
<%@page import="com.tsp.operation.controller.*"%>
<%@page import="com.tsp.util.*"%>
<%@include file="/WEB-INF/InitModel.jsp"%>
<%@ taglib uri="http://jakarta.apache.org/taglibs/input-0.90" prefix="input" %>
<%String path = application.getRealPath(model.menuService.getRealUrl(request.getRequestURI()));
  String datos[] = model.menuService.getContenidoMenu(BASEURL,request.getRequestURI(),path);
%>
<html>
<head>
 <script>
		 	function Exportar (){
				
			    alert( 
				
					'Puede observar el seguimiento del proceso en la siguiente ruta del menu:' + '\n' + '\n' +
					'SLT -> PROCESOS -> Seguimiento de Procesos' + '\n' + '\n' + '\n' +
					'Y puede observar el archivo excel generado, en la siguiente parte:' + '\n' + '\n' +
					'En la pantalla de bienvenida de la Pagina WEB,' + '\n' +
					'vaya a la parte superior-derecha,' + '\n' +
					'y presione un click en el icono que le aparece.'
					
				);
				
			}
		   function abrirVentanaBusq( an, al, url, pag ) {
			
        		parent.open( url, 'Conductor', 'width=' + an + ', height=' + al + ',scrollbars=no,resizable=no,top=10,left=65,status=yes' );
				
    		}
	
		</script>
<title>Reporte Equipo Trailers</title>
<link href="<%= BASEURL %>/css/estilostsp.css" rel='stylesheet'>
<link href="../../../../css/estilostsp.css" rel="stylesheet" type="text/css">
<link href="/css/estilostsp.css" rel="stylesheet" type="text/css">
</head>
<script type='text/javascript' src="<%= BASEURL %>/js/validar.js"></script>
<script src='<%= BASEURL %>/js/date-picker.js'></script>
<script type='text/javascript' src="<%= BASEURL %>/js/boton.js"></script>
<body onresize="redimensionar()" onload = 'redimensionar()'>
<div id="capaSuperior" style="position:absolute; width:100%; height:100px; z-index:0; left: 0px; top: 0px;">
 <jsp:include page="/toptsp.jsp?encabezado=Reporte Equipo Trailers"/>
</div>
<div id="capaCentral" style="position:absolute; width:100%; height:87%; z-index:0; left: 0px; top: 100px; overflow: scroll;">
<table width="350"  border="2" align="center">
  <tr>
    <td><table width="100%" align="center" class="tablaInferior">
  <tr>
    <td><table width="100%"  border="0" class="barratitulo">
      <tr >
        <td width="77%" class="subtitulo1">Ubicación de equipos (Trailers) </td>
        <td width="23%"><img src="<%=BASEURL%>/images/titulo.gif"></td>
      </tr>
    </table></td>
  </tr>
  <tr>
    <td align="center" class="fila">Generaci&oacute;n del Archivo Equipo trailers<br>
    Presione Exportar para iniciar el proceso.</td>
  </tr>
</table></td>
  </tr>
</table>
<form name="forma" action='<%=CONTROLLER%>?estado=Reporte&accion=Equipo' id="forma" method="post">
  
 
  <center>
   <input name="Guardar2" src="<%=BASEURL%>/images/botones/exportarExcel.gif"  type="image" class="boton" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" style="cursor:hand" onclick="Exportar();">
   <input name="salir2" src="<%=BASEURL%>/images/botones/salir.gif" type="image" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" style="cursor:hand" class="boton" onclick="window.close();">
  </center>
</form>
<% if( request.getParameter("msg") != null ){%>
  <p><table border="2" align="center">
  <tr>
    <td><table width="100%"  border="1" align="center"  bordercolor="#F7F5F4" bgcolor="#FFFFFF">
      <tr>
        <td width="229" align="center" class="mensajes"><%=request.getParameter("msg").toString()%></td>
        <td width="29" background="<%=BASEURL%>/images/cuadronaranja.JPG">&nbsp;</td>
        <td width="58">&nbsp;</td>
      </tr>
    </table></td>
  </tr>
</table>
</p>
 <%} %>
</div>
<iframe width=188 height=166 name="gToday:normal:agenda.js:gfPop:plugins.js" id="gToday:normal:agenda.js:gfPop:plugins_24.js" src="js/Calendario/ipopeng.htm" scrolling="no" frameborder="0" style="visibility:visible; z-index:999; position:absolute; left:-500px; top:0px;"> </iframe>
<%= datos[1]%>
</body>
</html>