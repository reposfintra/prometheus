<!--  
     - Author(s)       :      FERNEL VILLACOB DIAZ
     - Date            :      0908/02/2006  
     - Copyright Notice:      TRANSPORTES SANCHEZ POLO S.A.
  -->
 <%--   - @(#)  
     - Description:  Vista que permite capturar datos para la generacion del reporte
 --%>

<%@page    session   ="true"%> 
<%@page    errorPage ="/error/ErrorPage.jsp"%>
<%@page    import    ="com.tsp.operation.model.beans.*"%>
<%@include file      ="/WEB-INF/InitModel.jsp"%>
<%@page    import    ="com.tsp.util.*"%>
<%@page    import    ="java.util.*" %>




<html>
<head>
    <title>Reporte Planillas por Via</title>
    <link href="<%= BASEURL %>/css/estilostsp.css" rel="stylesheet" type="text/css"> 
    <script type='text/javascript' src="<%= BASEURL %>/js/boton.js"></script>
    <script language="JavaScript" type="text/javascript" src="<%=BASEURL%>/js/script.js"></script>
    
    <script>
       function send(theForm){
          var sw = 0;
          selectAll( theForm.cliente );
          for(var i=0;i<theForm.length;i++){
              if(  ( theForm.elements[i].name=='origen' || theForm.elements[i].name=='destino'  ||  theForm.elements[i].name=='via'  )  
                    && theForm.elements[i].value=='' 
              ){
                    alert('Deber� llenar los campos de busqueda :' + theForm.elements[i].title );
                    theForm.elements[i].focus();
                    sw = 1;
                    break;
              }
              
              if(  theForm.elements[i].name=='tipo' && theForm.elements[i].checked   && theForm.elements[i].value=='ONE'  && theForm.cliente.value=='' ){
                    alert('Deber� seleccionar los clientes para el reporte' );
                    theForm.elements[i].focus();
                    sw = 1;
                    break;
              }
              
          }
          
          
          if( sw==0 ){              
              theForm.submit();
          }
       }
    </script>
    
</head>
<body>

<%  String path    = application.getRealPath(model.menuService.getRealUrl(request.getRequestURI()));
    String datos[] = model.menuService.getContenidoMenu(BASEURL,request.getRequestURI(),path);    %>

<div id="capaSuperior" style="position:absolute; width:100%; height:100px; z-index:0; left: 0px; top: 0px;">
<jsp:include page="/WEB-INF/toptsp.jsp?encabezado=Reporte Planillas por Via"/>
</div>

<div id="capaCentral" style="position:absolute; width:100%; height:85%; z-index:0; left: 0px; top: 100px; overflow: scroll;"> 
<center>
    

 <% String     msj       =   request.getParameter("msj");  
    List       origenes  =   model.ReportePlanillasViaSvc.getOrigenes();
    List       clientes  =   model.ReportePlanillasViaSvc.getClientes(); 
    String     hoy       =   Utility.getHoy("-");%>
    
    

<form action='<%=CONTROLLER%>?estado=Reporte&accion=PlanillasVia&evento=GENERAR' method='post' name='formulario'>
         <table  width="600" border="2" align="center">
             <tr>
                  <td>  
                       <table width='100%' align='center' class='tablaInferior'>

                              <tr class="barratitulo">
                                    <td colspan='2' >
                                           <table cellpadding='0' cellspacing='0' width='100%'>
                                                 <tr>
                                                      <td align="left" width='55%' class="subtitulo1">&nbsp;Datos de Generaci�n</td>
                                                      <td align="left" width='*'  ><img src="<%=BASEURL%>/images/titulo.gif" width="32"  height="20" align="left"><%=datos[0]%></td>
                                                </tr>
                                           </table>
                                    </td>
                             </tr>
                             
                             
                            <tr  class="fila">
                                   
                                    <td width='15%'> FECHA    </td>
                                    <td>          
                                        
                                         <input type='text' class="textbox"  name='fechaIni'     readonly title='FECHA INICIAL'       value="<%= hoy %>">                                                        
                                         <a href="javascript:void(0)" onclick="if(self.gfPop)gfPop.fPopCalendar(document.formulario.fechaIni);return false;" HIDEFOCUS>
                                            <img src="<%=BASEURL%>\js\Calendario\cal.gif" width="16" height="16" border="0" alt="De click aqui para escoger la fecha">
                                         </a>
                                         &nbsp&nbsp&nbsp  
                                         <input type='text' class="textbox"  name='fechaFin'     readonly   title='FECHA FINAL'     value="<%= hoy %>">                                                        
                                         <a href="javascript:void(0)" onclick="if(self.gfPop)gfPop.fPopCalendar(document.formulario.fechaFin);return false;" HIDEFOCUS>
                                            <img src="<%=BASEURL%>\js\Calendario\cal.gif" width="16" height="16" border="0" alt="De click aqui para escoger la fecha">
                                         </a>        
                                    
                                    </td>
                            </tr>
                            
                            <tr  class="fila">        
                                    <td> RUTA     </td>
                                    <td> 
                                         <table cellpadding='0' cellspacing='0' width='100%'>
                                             <tr  class="fila"><td width='20%'>Origen  </td>
                                                               <td width='*'> 
                                                                   <select  name='origen'   style="width:90%" title='ORIGEN'  >
                                                                      <% for(int i=0;i<origenes.size();i++){
                                                                             Hashtable pc = (Hashtable)origenes.get(i); 
                                                                             String    cod = (String) pc.get("codigo");
                                                                             String    sel = (cod.equals("MB"))?"selected":"";%>
                                                                             <option value='<%= pc.get("codigo")%>'  <%=sel%> > <%= pc.get("nombre")%>
                                                                      <%}%>
                                                                   </select>                                                               
                                                               </td>
                                             </tr>
                                             <tr  class="fila"><td>Destino </td>
                                                              <td> 
                                                                    <font id='opDestinos'>
                                                                        <select  name='destino'  style="width:90%" title='DESTINO'>
                                                                          <option value=''>.....
                                                                        </select> 
                                                                    </font>
                                                                    <img src='<%=BASEURL%>/images/botones/iconos/buscar.gif' style="cursor:hand" onclick="load('<%=CONTROLLER%>?estado=Reporte&accion=PlanillasVia&evento=BUSCAR_DESTINOS&origen=' + formulario.origen.value )">
                                                              </td>
                                             </tr>
                                             <tr  class="fila"><td>Via     </td>
                                                               <td>  
                                                                    <font id='opVias'>
                                                                        <select  name='via'      style="width:90%" title='VIA'>
                                                                          <option value=''>.....
                                                                        </select> 
                                                                    </font>
                                                                    <img src='<%=BASEURL%>/images/botones/iconos/buscar.gif' style="cursor:hand" onclick="load('<%=CONTROLLER%>?estado=Reporte&accion=PlanillasVia&evento=BUSCAR_VIAS&origen=' + formulario.origen.value  +'&destino=' +  formulario.destino.value )">
                                                          
                                                               </td>
                                             </tr>
                                         </table>
                                    </td>
                            </tr>
                            
                            <tr  class="fila"> 
                                    <td   width='15%' > 
                                          CLIENTE  
                                    </td>
                                    <td class='letrafila'    style="font size:12">
                                          <input type='radio' name='tipo' value='ALL' checked > Seleccionar Todos
                                          &nbsp&nbsp&nbsp&nbsp
                                          <input type='radio' name='tipo' value='ONE'         > Seleccionar Algunos
                                    </td>
                            </tr>
                            
                            
                            <tr>   
                                  <td colspan='2'> 
                                   
                                       
                                           <table cellpadding='0' cellspacing='0' width='100%'>
                                               <tr   class="fila">                            
                                                    <td width='45%' align='center' style="font size:12">  

                                                        Listado General
                                                        <input type='text' name='busqueda' style="width:100%" title='Buscar por palabras' onkeyup='searchCombo(this.value, this.form.clientes)' onkeypress='searchCombo(this.value, this.form.clientes)'>
                                                        
                                                        <select  multiple size='10' name='clientes'  style='width:100%' title='CLIENTES'   onchange=" if (this.selectedIndex!=-1) { window.status=this[this.selectedIndex].text; }" >
                                                           <% for(int i=0;i<clientes.size();i++){
                                                                 Hashtable pc = (Hashtable)clientes.get(i); 
                                                                 String    cod = (String) pc.get("codigo");
                                                                 String    sel = (cod.equals("000031"))?"selected":"";%>
                                                                 <option value='<%= pc.get("codigo")%>'  <%=sel%> > <%= pc.get("nombre")%>
                                                            <%}%>
                                                        </select>
                                                        
                                                    </td>

                                                    <td width='10%' align='center'>
                                                         <img src='<%=BASEURL%>/images/botones/envDer.gif' style='cursor:hand' name='imgEnvDer'  onclick="move(clientes,  cliente); " onMouseOver='botonOver(this);' onMouseOut='botonOut(this);'><br>             
                                                         <img src='<%=BASEURL%>/images/botones/envIzq.gif' style='cursor:hand' name='imgEnvIzq'  onclick="move(cliente, clientes ); " onMouseOver='botonOver(this);' onMouseOut='botonOut(this);'><br>                                                             
                                                    </td>

                                                    <td width='45%'  align='center' style="font size:12"> 
                                                        Listado Reporte
                                                        <select multiple size='12' class='select' style='width:100%'  name='cliente'   title='CLIENTES'   onchange=" if (this.selectedIndex!=-1) { window.status=this[this.selectedIndex].text; }"></select>                                              
                                                    </td>

                                               <tr>
                                            </table>
                                            
                                        
                                  </td>   
                             </tr>                              
                     </table>
                     
                  </td>
               </tr>       
         </table>
</form>


<p>
   <img src="<%=BASEURL%>/images/botones/aceptar.gif"    name="acept"   height="21"  title='Generar reporte'      onClick="send(formulario);"   onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" style="cursor:hand">
   <img src="<%=BASEURL%>/images/botones/salir.gif"      name="exit"    height="21"  title='Salir'                onClick="parent.close();"     onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" style="cursor:hand">
</p>






 <!-- mensajes -->
 <% if(msj!=null && !msj.equals("")){%>        
         <table border="2" align="center">
          <tr>
            <td><table width="100%"  border="1" align="center"  bordercolor="#F7F5F4" bgcolor="#FFFFFF">
              <tr>
                <td width="470" align="center" class="mensajes"><%=msj%></td>
                <td width="29" background="<%=BASEURL%>/images/cuadronaranja.JPG">&nbsp;</td>
                <td width="58">&nbsp;</td>
              </tr>
            </table></td>
          </tr>
        </table>
  <%}%>
    
    
  
 <script>
     function load(url){ 
         var a = "<iframe name='ejecutor'  style='visibility:hidden' src='" + url + "'> ";
         aa.innerHTML = a;
     }
     
     
 </script>
 <font id='aa'></font>

 
 
</div>
<%=datos[1]%> 
<iframe width=188 height=166 name="gToday:normal:agenda.js:gfPop:plugins.js"  style='cursor: move;' ondragstart='' onMouseDown="_initMove(this);"    id="gToday:normal:agenda.js:gfPop:plugins_24.js" src="<%=BASEURL%>/js/Calendario/ipopeng.htm" scrolling="no" frameborder="0" style="visibility:visible; z-index:999; position:absolute; left:-500px; top:0px;"> </iframe>
 


</body>
</html>
