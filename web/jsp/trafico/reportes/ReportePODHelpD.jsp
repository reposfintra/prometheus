<%@include file="/WEB-INF/InitModel.jsp"%>
<html>
<head>
<title>Reporte POD</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link href="/css/estilostsp.css" rel="stylesheet" type="text/css">
<link href="<%= BASEURL %>/css/estilostsp.css" rel='stylesheet'>
</head>

<body>
<br>


<table width="696"  border="2" align="center">
  <tr>
    <td width="811" >
      <table width="100%" border="0" align="center">
        <tr  class="subtitulo">
          <td height="24" colspan="2"><div align="center">Reporte POD </div></td>
        </tr>
        <tr class="subtitulo1">
          <td colspan="2">Datos del  reporte POD </td>
        </tr>
        <tr>
          <td width="149" class="fila">Cliente</td>
          <td width="525"  class="ayudaHtmlTexto">Campo donde se encuentra el nombre del cliente </td>
        </tr>
        <tr>
          <td class="fila">Planilla</td>
          <td  class="ayudaHtmlTexto">Campo donde se encuentra la planilla.</td>
        </tr>
        <tr>
          <td class="fila">Remesa</td>
          <td  class="ayudaHtmlTexto">Campo donde se encuentra la remesa. </td>
        </tr>
        <tr>
          <td class="fila">Fecha de remesa </td>
          <td  class="ayudaHtmlTexto">Campo donde se encuentra la fecha de la remesa </td>
        </tr>
        <tr>
          <td class="fila">Placa</td>
          <td  class="ayudaHtmlTexto">Campo donde se encuentra la placa del vehiculo </td>
        </tr>
        <tr>
          <td class="fila">Ruta</td>
          <td  class="ayudaHtmlTexto">Campo donde se muestra el origen y el destino de la remesa </td>
        </tr>
        <tr>
          <td class="fila">Destinatario</td>
          <td  class="ayudaHtmlTexto">Campo donde se muestra el destinatario de la remesa </td>
        </tr>
        <tr>
          <td class="fila">Fac. Comercial </td>
          <td  class="ayudaHtmlTexto">Campo donde se muestra la factura comercial </td>
        </tr>
        <tr>
          <td class="fila">Fecha de llegada </td>
          <td  class="ayudaHtmlTexto">Campo donde se muestra la fecha de llegada </td>
        </tr>
        <tr>
          <td class="fila">No. Viaje </td>
          <td  class="ayudaHtmlTexto">Campo donde se muestra el numero del viaje </td>
        </tr>
        <tr>
          <td class="fila">No. Entrega </td>
          <td  class="ayudaHtmlTexto">Campo donde se muesta el numero de entrega </td>
        </tr>
        <tr>
          <td class="fila">&nbsp;</td>
          <td  class="ayudaHtmlTexto">&nbsp;</td>
        </tr>

      </table>
    </td>
  </tr>
</table>
<p>&nbsp;</p>
<p>&nbsp;</p>

<p>&nbsp;</p>
<p>&nbsp; </p>
</body>
</html>
