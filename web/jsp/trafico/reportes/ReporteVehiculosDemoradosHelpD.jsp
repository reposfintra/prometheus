<%@include file="/WEB-INF/InitModel.jsp"%>
<html>
<head>
<title>Reporte de Vehiculos Demorados y Varados</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link href="/css/estilostsp.css" rel="stylesheet" type="text/css">
<link href="<%= BASEURL %>/css/estilostsp.css" rel='stylesheet'>
</head>

<body>
<br>


<table width="696"  border="2" align="center">
  <tr>
    <td width="811" >
      <table width="100%" border="0" align="center">
        <tr  class="subtitulo">
          <td height="24" colspan="2"><div align="center">Reporte de Vehiculos Demorados y Varados</div></td>
        </tr>
        <tr class="subtitulo1">
          <td colspan="2">Reporte de Vehiculos Demorados y Varados</td>
        </tr>
        <tr>
          <td width="149" class="fila">Placa</td>
          <td width="525"  class="ayudaHtmlTexto">Campo donde se digita la placa del veh&iacute;culo. </td>
        </tr>
        <tr>
          <td class="fila">Cliente</td>
          <td  class="ayudaHtmlTexto">Campo donde se selecciona el cliente. </td>
        </tr>
        <tr>
          <td class="fila">Origen</td>
          <td  class="ayudaHtmlTexto">Campo donde se selecciona el origen de una ruta.</td>
        </tr>
        <tr>
          <td class="fila">Destino</td>
          <td  class="ayudaHtmlTexto">Campo donde se selecciona el destino de una ruta. </td>
        </tr>
        <tr>
          <td class="fila">Agencia</td>
          <td  class="ayudaHtmlTexto">Campo donde se selecciona la agencia. </td>
        </tr>
        <tr>
          <td class="fila"><div align="center">
              <input type="image" src = "<%=BASEURL%>/images/botones/aceptar.gif" style="cursor:default ">
          </div></td>
          <td  class="ayudaHtmlTexto">Bot&oacute;n que valida el la informaci&oacute;n digitada en los campos para iniciar la generaci&oacute;n del reporte. </td>
        </tr>
        <tr>
          <td class="fila"><div align="center">
              <input type="image" src = "<%=BASEURL%>/images/botones/cancelar.gif" style="cursor:default ">
          </div></td>
          <td  class="ayudaHtmlTexto"> Bot&oacute;n que cancela todas los operaciones realizadas, y resetea la pagina llev&aacute;ndola a su estado inicial. </td>
        </tr>
        <tr>
          <td class="fila"><div align="center">
              <input type="image" src = "<%=BASEURL%>/images/botones/salir.gif" style="cursor:default ">
          </div></td>
          <td  class="ayudaHtmlTexto">Bot&oacute;n que cierra la ventana. </td>
        </tr>

      </table>
    </td>
  </tr>
</table>
<p>&nbsp;</p>
<p>&nbsp;</p>

<p>&nbsp;</p>
<p>&nbsp; </p>
</body>
</html>
