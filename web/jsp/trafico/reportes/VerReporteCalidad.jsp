<!--
- Autor : LREALES
- Date  : 19 de septiembre de 2006
- Copyrigth Notice : Fintravalores S.A. S.A
-->
<%--
-@(#)
--Descripcion : Pagina JSP, que se encarga de mostrar el reporte de proveedores de fintra
--%>
<%--
The taglib directive below imports the JSTL library. If you uncomment it,
you must also add the JSTL library to the project. The Add Library... action
on Libraries node in Projects view can be used to add the JSTL 1.1 library.
--%>
<%-- Declaracion de librerias--%>
<%@page session="true"%>
<%@page errorPage="../error/ErrorPage.jsp"%>
<%@page import="com.tsp.operation.model.beans.*, java.util.*, java.text.*"%>
<%@ include file="/WEB-INF/InitModel.jsp"%>
<%@ taglib uri="http://jakarta.apache.org/taglibs/input-0.90" prefix="input" %>
<%@page import="com.tsp.util.*"%>
<% Util u = new Util(); %>
<%@page contentType="text/html"%>
<%@page import="java.util.*"%> 
<%@page import="com.tsp.operation.model.*"%>
<%@taglib uri="http://jsptags.com/tags/navigation/pager" prefix="pg" %>
<html>
    <head>        
        <title>Reporte De Viajes</title>
		<script src='<%=BASEURL%>/js/date-picker.js'></script>
		<script language="JavaScript" type="text/javascript" src="<%=BASEURL%>/js/boton.js"></script>
		<script type='text/javascript' src="<%=BASEURL%>/js/validar.js"></script> 
		<link href="../css/estilostsp.css" rel="stylesheet" type="text/css">
		<link href="<%=BASEURL%>/css/estilostsp.css" rel="stylesheet" type="text/css">
		 <script>
		 	function Exportar (){
				
			    alert( 
				
					'Puede observar el seguimiento del proceso en la siguiente ruta del menu:' + '\n' + '\n' +
					'SLT -> PROCESOS -> Seguimiento de Procesos' + '\n' + '\n' + '\n' +
					'Y puede observar el archivo excel generado, en la siguiente parte:' + '\n' + '\n' +
					'En la pantalla de bienvenida de la Pagina WEB,' + '\n' +
					'vaya a la parte superior-derecha,' + '\n' +
					'y presione un click en el icono que le aparece.'
					
				);
				
			}
		   function abrirVentanaBusq( an, al, url, pag ) {
			
        		parent.open( url, 'Conductor', 'width=' + an + ', height=' + al + ',scrollbars=no,resizable=no,top=10,left=65,status=yes' );
				
    		}
	
		</script>
    </head>
   <body onResize="redimensionar()" onLoad="redimensionar()">
   <%  String path    = application.getRealPath(model.menuService.getRealUrl(request.getRequestURI()));
    String datos[] = model.menuService.getContenidoMenu(BASEURL,request.getRequestURI(),path);    
	
	String FechaI=(request.getParameter ("FechaI")!=null)?request.getParameter ("FechaI"):"";
	String FechaF =(request.getParameter ("FechaF")!=null)?request.getParameter ("FechaF"):"";
	
	%>
	


	<div id="capaSuperior" style="position:absolute; width:100%; height:100px; z-index:0; left: 11px; top: 1px;">
	 <jsp:include page="/toptsp.jsp?encabezado=Reporte de Viajes por Descargues" />
   </div>
	<div id="capaCentral" style="position:absolute; width:100%; height:87%; z-index:0; left: 0px; top: 100px; overflow: scroll;">
	<%String msg = request.getParameter("msg");
		if ( msg!=null){%>	
 <br>
  <table border="2" align="center">
  <tr>
    <td><table width="100%"  border="1" align="center"  bordercolor="#F7F5F4" bgcolor="#FFFFFF">
      <tr>
        <td width="300" align="center" class="mensajes">La busqueda no arrojo resultados</td>
        <td width="29" background="<%=BASEURL%>/images/cuadronaranja.JPG">&nbsp;</td>
        <td width="58">&nbsp;</td>
      </tr>
	  
    </table></td>
  </tr>
</table>
      <img src="<%=BASEURL%>/images/botones/regresar.gif" name="c_regresar1" onclick="location.href='<%=CONTROLLER%>?estado=Menu&accion=Cargar&carpeta=/jsp/trafico/reportes&pagina=ReporteCalidad.jsp&marco=no';//'<%=BASEURL%>/jsp/trafico/reportes/ReporteCalidad.jsp'" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" style="cursor:hand">
	  <input name="salir2" src="<%=BASEURL%>/images/botones/salir.gif" type="image" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" style="cursor:hand" class="boton" onclick="window.close();">
   
 <%}else{
        Vector datosC =model.clienteService.getVector();
		String style = "simple";
		String position =  "bottom";
		String index =  "center";
		int maxPageItems = 20;
		int maxIndexPages = 10;
		
		%>		
   <form name="form1" method="post" action="<%=CONTROLLER%>?estado=Reporte&accion=Viajes&opcion=2">
              <p>
	    <input name="FechaF" type="hidden" id="FechaF" value="<%=FechaF%>">
	    </p>
              <p>
	      <input name="FechaI" type="hidden" id="FechaI" value="<%=FechaI%>">
              </p>
            
              <p>
                <input name="Guardar2" src="<%=BASEURL%>/images/botones/exportarExcel.gif" type="image" class="boton" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" style="cursor:hand" onclick="Exportar();">
                <img src="<%=BASEURL%>/images/botones/regresar.gif" name="c_regresar1" onclick="location.href='<%=CONTROLLER%>?estado=Menu&accion=Cargar&carpeta=/jsp/trafico/reportes&pagina=ReporteCalidad.jsp&marco=no';//'<%=BASEURL%>/jsp/trafico/reportes/ReporteCalidad.jsp'" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" style="cursor:hand">
 	            <input name="salir" src="<%=BASEURL%>/images/botones/salir.gif" type="image" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" style="cursor:hand" class="boton" onclick="window.close();">
          </p>
   </form>
        <p>        </p>
        <tr>
            <td>
              <table width="100%"  border="1" bordercolor="#999999" bgcolor="#F7F5F4" >
			  	<tr class="fila" >
			  		<td colspan="7" align="left" class="subtitulo1">Reporte de viajes del <%=FechaI%> al <%=FechaF%></td>
				  	<td colspan="7" align="left"><img src="<%= BASEURL %>/images/titulo.gif" width="32" height="20"  align="left"></td>
				</tr>
				
                <tr class="tblTitulo" align="center">
				  <td nowrap align="center">ITEM</td>
				  <td nowrap  align="center">REMISION</td>
				   	<td nowrap  align="center">PLACA</td>
				    <td nowrap align="center">FECHA DE CUMPLIDO </td>
                    <td nowrap  align="center"> PESO REAL </td>
				    <td nowrap  align="center">NIT DEL PROPIETARIO </td>
				    <td nowrap  align="center">NOMBRE DEL PROPIETARIO </td>
				    <td nowrap  align="center">CEDULA DEL CONDUCTOR</td>
				    <td nowrap  align="center">NOMBRE DEL CONDUCTOR </td>
				    <td nowrap  align="center">PESO MINA LLENO </td>
				    <td nowrap align="center">PESO MINA VACIO </td>
		            <td nowrap  align="center">PESO MINA NETO</td>
				    <td nowrap  align="center">RUTA</td>
                    <td nowrap  align="center"><div align="center">PUERTO DE DESCARGUE </div></td>
                    
				  
				         
                </tr>	
				<%
				double lleno = 0;
				double vacio =0;
				double totalNeto = 0;
				Vector datosValor = model.planllegadacargue.getvectorValores();
				for( int z = 0; z<datosValor.size(); z++ ){
					BeanGeneral BeanEspecifico = (BeanGeneral) datosValor.elementAt(z);								
					if (BeanEspecifico.getValor_09().equals("")){
						lleno = 0;
					}else{
					   lleno = Double.parseDouble(BeanEspecifico.getValor_09());
					} 	
					if (BeanEspecifico.getValor_10().equals("")){
						vacio = 0;
					}else{
					   vacio = Double.parseDouble(BeanEspecifico.getValor_10());
					} 
					totalNeto = lleno - vacio ;    
				 %>
				  <tr class="<%=(z % 2 == 0 )?"filagris":"filaazul"%>" onMouseOver='cambiarColorMouse(this)'   style="cursor:hand"
				  onClick="">
		
	  			   
				  <td nowrap align="left" class="bordereporte"><%=(BeanEspecifico.getValor_01()!=null)?BeanEspecifico.getValor_01():""%>&nbsp;</td>
                  <td nowrap align="left" class="bordereporte"><%=(BeanEspecifico.getValor_02()!=null)?BeanEspecifico.getValor_02():""%>&nbsp;</td>
				  <td nowrap align="left" class="bordereporte"><%=(BeanEspecifico.getValor_14()!=null)?BeanEspecifico.getValor_14():""%>&nbsp;</td>
				  <td nowrap align="left" class="bordereporte"><%=(BeanEspecifico.getValor_03()!=null)?BeanEspecifico.getValor_03():""%>&nbsp;</td>
				  <td nowrap align="left" class="bordereporte"><%=(BeanEspecifico.getValor_04()!=null)?BeanEspecifico.getValor_04():""%>&nbsp;</td>
				  <td nowrap align="left" class="bordereporte"><%=(BeanEspecifico.getValor_05()!=null)?BeanEspecifico.getValor_05():""%>&nbsp;</td>
				  <td nowrap align="left" class="bordereporte"><%=(BeanEspecifico.getValor_06()!=null)?BeanEspecifico.getValor_06():""%>&nbsp;</td>
				  <td nowrap align="left" class="bordereporte"><%=(BeanEspecifico.getValor_07()!=null)?BeanEspecifico.getValor_07():""%>&nbsp;</td>
				  <td nowrap align="left" class="bordereporte"><%=(BeanEspecifico.getValor_08()!=null)?BeanEspecifico.getValor_08():""%>&nbsp;</td>
				  <td nowrap align="right" class="bordereporte"><%=Util.customFormat( Float.parseFloat( (BeanEspecifico.getValor_09()!=null && !BeanEspecifico.getValor_09().equals("") )? (String)BeanEspecifico.getValor_09() :"0")  )%>&nbsp;</td>
				  <td nowrap align="right" class="bordereporte"><%=Util.customFormat( Float.parseFloat( (BeanEspecifico.getValor_10()!=null && !BeanEspecifico.getValor_10().equals("") )? (String)BeanEspecifico.getValor_10() :"0")  )%>&nbsp;</td>
				  <td nowrap align="rigth" class="bordereporte"><%=Util.customFormat(totalNeto)%></td>
				  <td nowrap align="left" class="bordereporte"><%=(BeanEspecifico.getValor_12()!=null)?BeanEspecifico.getValor_12():""%>    --    <%=(BeanEspecifico.getValor_13()!=null)?BeanEspecifico.getValor_13():""%> </td>
				  <td nowrap align="left" class="bordereporte"><%=(BeanEspecifico.getValor_15()!=null)?BeanEspecifico.getValor_15():""%>&nbsp;</td>
				  
				  
				  
				  
				   	
            </tr>
		<% lleno = 0;
		   vacio = 0;
		   totalNeto = 0;
		}%>
            </table>
      </table></td>
      </tr>
		    </table>
		   <form name="form1" method="post" action="<%=CONTROLLER%>?estado=Reporte&accion=Viajes&opcion=2">
		           <p>
	     <input name="FechaF" type="hidden" id="FechaF" value="<%=FechaF%>">
	    </p>
              <p>
	      <input name="FechaI" type="hidden" id="FechaI" value="<%=FechaI%>">
              </p>
             
            
      <input name="Guardar2" src="<%=BASEURL%>/images/botones/exportarExcel.gif" type="image" class="boton" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" style="cursor:hand" onclick="Exportar();">
      <img src="<%=BASEURL%>/images/botones/regresar.gif" name="c_regresar1" onclick="location.href='<%=CONTROLLER%>?estado=Menu&accion=Cargar&carpeta=/jsp/trafico/reportes&pagina=ReporteCalidad.jsp&marco=no';//'<%=BASEURL%>/jsp/trafico/reportes/ReporteCalidad.jsp'" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" style="cursor:hand">
	  <input name="salir2" src="<%=BASEURL%>/images/botones/salir.gif" type="image" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" style="cursor:hand" class="boton" onclick="window.close();">
    </form>	
  
	  
 <%}%>
	  
</body>
</div>

<%=datos[1]%>  
</html>
