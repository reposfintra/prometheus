<!--
- Autor : LREALES
- Date  : 19 de septiembre de 2006
- Copyrigth Notice : Fintravalores S.A. S.A
-->
<%--
-@(#)
--Descripcion : Pagina JSP, que se encarga de mostrar el reporte de proveedores de fintra
--%>
<%--
The taglib directive below imports the JSTL library. If you uncomment it,
you must also add the JSTL library to the project. The Add Library... action
on Libraries node in Projects view can be used to add the JSTL 1.1 library.
--%>
<%-- Declaracion de librerias--%>
<%@page session="true"%>
<%@page errorPage="../error/ErrorPage.jsp"%>
<%@page import="com.tsp.operation.model.beans.*, java.util.*, java.text.*"%>
<%@ include file="/WEB-INF/InitModel.jsp"%>
<%@ taglib uri="http://jakarta.apache.org/taglibs/input-0.90" prefix="input" %>
<%@page import="com.tsp.util.*"%>
<% Util u = new Util(); %>
<%@page contentType="text/html"%>
<%@page import="java.util.*"%> 
<%@page import="com.tsp.operation.model.*"%>
<%@taglib uri="http://jsptags.com/tags/navigation/pager" prefix="pg" %>
<html>
    <head>
        <title>Reporte Equipos Trailers </title>
		<script src='<%=BASEURL%>/js/date-picker.js'></script>
		<script language="JavaScript" type="text/javascript" src="<%=BASEURL%>/js/boton.js"></script>
		<script type='text/javascript' src="<%=BASEURL%>/js/validar.js"></script> 
		<link href="file:///C|/sltequipo%20viejo/slt1/jsp/sot/css/estilostsp.css" rel="stylesheet" type="text/css">
		<link href="<%=BASEURL%>/css/estilostsp.css" rel="stylesheet" type="text/css">
		 <script>
		 	function Exportar (){
				
			    alert( 
				
					'Puede observar el seguimiento del proceso en la siguiente ruta del menu:' + '\n' + '\n' +
					'SLT -> PROCESOS -> Seguimiento de Procesos' + '\n' + '\n' + '\n' +
					'Y puede observar el archivo excel generado, en la siguiente parte:' + '\n' + '\n' +
					'En la pantalla de bienvenida de la Pagina WEB,' + '\n' +
					'vaya a la parte superior-derecha,' + '\n' +
					'y presione un click en el icono que le aparece.'
					
				);
				
			}
		   function abrirVentanaBusq( an, al, url, pag ) {
			
        		parent.open( url, 'Conductor', 'width=' + an + ', height=' + al + ',scrollbars=no,resizable=no,top=10,left=65,status=yes' );
				
    		}
	
		</script>
    </head>
   <body onResize="redimensionar()" onLoad="redimensionar()">
   <%  String path    = application.getRealPath(model.menuService.getRealUrl(request.getRequestURI()));
    String datos[] = model.menuService.getContenidoMenu(BASEURL,request.getRequestURI(),path);    
	
	String fechaI=(request.getParameter ("FechaI")!=null)?request.getParameter ("FechaI"):"";
	String fechaF =(request.getParameter ("FechaF")!=null)?request.getParameter ("FechaF"):"";
	%>
	


	<div id="capaSuperior" style="position:absolute; width:100%; height:100px; z-index:0; left: 1px; top: 0px;">
	 <jsp:include page="/toptsp.jsp?encabezado=Reporte Equipo - Trailer"/>
	</div>
	<div id="capaCentral" style="position:absolute; width:100%; height:87%; z-index:0; left: 0px; top: 100px; overflow: scroll;">
	<%String msg = request.getParameter("msg");
		if ( msg!=null){%>	
 <br>
  <table border="2" align="center">
  <tr>
    <td><table width="100%"  border="1" align="center"  bordercolor="#F7F5F4" bgcolor="#FFFFFF">
      <tr>
        <td width="300" align="center" class="mensajes">La busqueda no arrojo resultados</td>
        <td width="29" background="<%=BASEURL%>/images/cuadronaranja.JPG">&nbsp;</td>
        <td width="58">&nbsp;</td>
      </tr>
	  
    </table></td>
  </tr>
</table>
      <img src="<%=BASEURL%>/images/botones/regresar.gif" name="c_regresar" onclick="location.href='<%=BASEURL%>/jsp/trafico/reportes/ReporteEquipos.jsp'"  onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" style="cursor:hand">
	  <input name="salir2" src="<%=BASEURL%>/images/botones/salir.gif" type="image" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" style="cursor:hand" class="boton" onclick="window.close();">
   
 <%}else{
        Vector datosC = model.reporteGeneralService.getVector();
		String style = "simple";
		String position =  "bottom";
		String index =  "center";
		int maxPageItems = 40;
		int maxIndexPages = 10;
		
		%>		
   <form name="form1" method="post" action="<%=CONTROLLER%>?estado=Reporte&accion=Equipo&opcion=2">
              <p>
	    <input name="FechaI" type="hidden" id="FechaI" value="<%=fechaI%>">
		<input name="FechaF" type="hidden" id="FechaF" value="<%=fechaF%>">

                <input name="Guardar2" src="<%=BASEURL%>/images/botones/exportarExcel.gif" type="image" class="boton" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" style="cursor:hand" onclick="Exportar();">
                <img src="<%=BASEURL%>/images/botones/regresar.gif" name="c_regresar" onclick="location.href='<%=BASEURL%>/jsp/trafico/reportes/ReporteEquipos.jsp'"  onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" style="cursor:hand">
 	            <input name="salir" src="<%=BASEURL%>/images/botones/salir.gif" type="image" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" style="cursor:hand" class="boton" onclick="window.close();">
          </p>
   </form>
  <table  width="100%">  
           <tr>
				<td class="barratitulo" colspan='2' >
					   <table cellpadding='0' cellspacing='0' width='100%'>
							 <tr class="fila">
							  <td width="15%" align="left" class="subtitulo1">&nbsp;<strong>Equipos trailers     </strong></td>
								 <td width="20%" class="subtitulo1"  align="left">&nbsp;</td>
								
							     <td width="65%" class="fila"  align="left"><img src="<%= BASEURL %>/images/titulo.gif" width="32" height="20"  align="left"><%=datos[0]%></td>
						    </tr>
							
						 
			  </table>   
			</td>
        </tr>
          <tr>
            <td>
              <table width="100%"  border="1" bordercolor="#999999" bgcolor="#F7F5F4" >
                <tr class="tblTitulo" align="center">
				  <td nowrap width="5%" align="center">ITEM</td>
				  <td nowrap width="5%" align="center">No INTERNO</td>
                  <td nowrap width="5%" align="center">EQUIPO</td>
				  <td nowrap width="5%" align="center">CABEZOTE</td>
				  <td nowrap width="5%" align="center">OT</td>
				  <td nowrap width="5%" align="center">OC</td>
				  <td nowrap width="5%" align="center">FECHA DE DESPACHO</td>
				  <td nowrap width="5%" align="center">ORIGEN OC</td>
				  <td nowrap width="5%" align="center">DESTINO OC</td>
				  <td nowrap width="5%" align="center">FECHA DE CUMPLIDO</td>
				  <td nowrap width="5%" align="center">FECHA ULTIMO REPORTE</td>
		          <td nowrap width="5%" align="center">CLIENTE </td>
				  <td nowrap width="5%" align="center">GRUPO EQUIPO</td>
                  <td nowrap width="5%" align="center">OBSERVACIONES</td>
                  <td nowrap width="5%" align="center">ORIGEN OT</td>
				  <td nowrap width="5%" align="center">DESTINO OT</td>
				  <td nowrap width="5%" align="center">ZONA DE DISPONIBILIDAD</td>
		          <td nowrap width="5%" align="center">F_D</td>
				   
                </tr>	
				<pg:pager
				 items="<%=datosC.size()%>"
				 index="<%= index %>"
				 maxPageItems="<%= maxPageItems %>"
				 maxIndexPages="<%= maxIndexPages %>"
				 isOffset="<%= true %>"
				 export="offset,currentPageNumber=pageNumber"
				 scope="request">					
		<%
			int cont = 1;
			for (int i = offset.intValue(), l = Math.min(i + maxPageItems, datosC.size()); i < l; i++){
				ReporteEquipos info = (ReporteEquipos) datosC.elementAt(i);
	  %>  	  
	  			
	  			<pg:item>
                  <tr class="<%=i%2==0?"filagris":"filaazul"%>" onMouseOver='cambiarColorMouse(this)' style="cursor:hand" title="Click" align="center" > 
				  <td nowrap align="left" class="bordereporte"><%=cont%>&nbsp;</td>
				  <td nowrap align="left" class="bordereporte"><%=(info.getNoInterno()!=null)?info.getNoInterno():""%>&nbsp;</td>
                  <td nowrap align="left" class="bordereporte"><%=(info.getEquipo()!=null)?info.getEquipo():""%>&nbsp;</td>
                  <td nowrap align="left" class="bordereporte"><%=(info.getCabezote()!=null)?info.getCabezote():""%>&nbsp;</td>
                  <td nowrap align="left" class="bordereporte"><%=(info.getOT()!=null)?info.getOT():""%>&nbsp;</td>
				  <td nowrap align="left" class="bordereporte"><%=(info.getOc()!=null)?info.getOc():""%>&nbsp;</td>
				  <td nowrap align="left" class="bordereporte"><%=(info.getFecha_despacho()!=null)?info.getFecha_despacho():""%>&nbsp;</td>
				  <td nowrap align="left" class="bordereporte"><%=(info.getOrigen_oc()!=null)?info.getOrigen_oc():""%>&nbsp;</td>
                  <td nowrap align="left" class="bordereporte"><%=(info.getDestino_oc()!=null)?info.getDestino_oc():""%>&nbsp;</td>
				  <td nowrap align="left" class="bordereporte"><%=(info.getFecha_cumplido()!=null)?info.getFecha_cumplido():""%>&nbsp;</td>   
				  <td nowrap align="left" class="bordereporte"><%=(info.getFecha_ult_reporte()!=null)?info.getFecha_ult_reporte():""%>&nbsp;</td>
				  <td nowrap align="left" class="bordereporte"><%=(info.getCliente()!=null)?info.getCliente():""%>&nbsp;</td>
                  <td nowrap align="left" class="bordereporte"><%=(info.getWgEquipo()!=null)?info.getWgEquipo():""%>&nbsp;</td>
				  <td nowrap align="left" class="bordereporte"><%=(info.getObservaciones_trafico()!=null)?info.getObservaciones_trafico():""%>&nbsp;</td>
                  <td nowrap align="left" class="bordereporte"><%=(info.getOrigen_ot()!=null)?info.getOrigen_ot():""%>&nbsp;</td>
                  <td nowrap align="left" class="bordereporte"><%=(info.getDestino_ot()!=null)?info.getDestino_ot():""%>&nbsp;</td>
                  <td nowrap align="left" class="bordereporte"><%=(info.getZona_disponibilidad()!=null)?info.getZona_disponibilidad():""%>&nbsp;</td>
                  <td nowrap align="left" class="bordereporte"><%=(info.getF_D()!=null)?info.getF_D():""%>&nbsp;</td>
                   
                </tr>
			  </pg:item>	
		<%cont = cont + 1; 
		}%>
		<tr class="bordereporte">
          <td td height="20" colspan="10" nowrap align="center">
		   <pg:index>
            <jsp:include page="/WEB-INF/jsp/googlesinimg.jsp" flush="true"/>      
           </pg:index> 
	      </td>
        </tr>
        </pg:pager>
            </table>
      </table>
			</table>
		   <form name="form1" method="post" action="<%=CONTROLLER%>?estado=Reporte&accion=Equipo&opcion=2">
		           <p>
	     <input name="FechaI" type="hidden" id="FechaI" value="<%=fechaI%>">
		<input name="FechaF" type="hidden" id="FechaF" value="<%=fechaF%>">
	    </p>
         
            
	  <input name="Guardar2" src="<%=BASEURL%>/images/botones/exportarExcel.gif" type="image" class="boton" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" style="cursor:hand" onclick="Exportar();">
      <img src="<%=BASEURL%>/images/botones/regresar.gif" name="c_regresar" onclick="location.href='<%=BASEURL%>/jsp/trafico/reportes/ReporteEquipos.jsp'"  onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" style="cursor:hand">
	  <input name="salir2" src="<%=BASEURL%>/images/botones/salir.gif" type="image" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" style="cursor:hand" class="boton" onclick="window.close();">
    </form>	
<%}%>	  
	  

	  
</body>
</div>
<%=datos[1]%>  
</html>
