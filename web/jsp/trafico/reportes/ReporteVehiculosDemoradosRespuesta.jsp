<!--
- Autor : Ing. Tito Andr�s Maturana
- Date  : 4 enero del 2006
- Copyrigth Notice : Fintravalores S.A. S.A
-->
<%--
-@(#)
--Descripcion : Pagina JSP, que maneja la generaci�n del reporte
				de coherecia de reportes en puestos de control.
--%>
<%@page contentType="text/html"%>
<%@page session="true"%>
<%@page errorPage="/error/ErrorPage.jsp"%>
<%@page import="java.util.*"%>
<%@page import="com.tsp.operation.model.*"%>
<%@page import="com.tsp.operation.model.beans.*"%>
<%@page import="com.tsp.util.*"%>
<%@include file="/WEB-INF/InitModel.jsp"%>
<%@taglib uri="http://jsptags.com/tags/navigation/pager" prefix="pg" %>
<html>
<head><title>Reporte de Vehiculos Demorados y Varados</title> 
 <link href="<%= BASEURL %>/css/estilostsp.css" rel='stylesheet'>
<link href="/css/estilostsp.css" rel="stylesheet" type="text/css">
<script type='text/javascript' src="<%= BASEURL %>/js/reporte.js"></script>
<script type='text/javascript' src="<%= BASEURL %>/js/boton.js"></script>
<body onresize="redimensionar()" onload = 'redimensionar()'>
<div id="capaSuperior" style="position:absolute; width:100%; height:100px; z-index:0; left: 0px; top: 0px;">
 <jsp:include page="/toptsp.jsp?encabezado=Reporte de Vehiculos Demorados y Varados"/>
</div>
<div id="capaCentral" style="position:absolute; width:100%; height:87%; z-index:0; left: 0px; top: 100px; overflow: scroll;">
<form name="forma" method="post" action="<%=CONTROLLER%>?estado=ReporteVehiculosVarados&accion=Excel">
<% 
    Vector vec = model.rmtService.getReportesPlanilla();
    String style = "simple";
	String position =  "bottom";
	String index =  "center";
	int maxPageItems = 15;
	int maxIndexPages = 10;
	Hashtable ht;

	if (vec.size() > 0) {
%>
<br>
<table width="100%" border="2" align="center">
  <tr>
    <td>
	  <table width="100%">
        <tr>
          <td width="50%" class="subtitulo1">Reporte Movimiento Tr&aacute;fico </td>
          <td width="50%" class="barratitulo"><img src="<%=BASEURL%>/images/titulo.gif" width="32" height="20"></td>
        </tr>
      </table>
	  <table border="1" cellpadding="4" cellspacing="1" bordercolor="#999999" bgcolor="#F7F5F4" align="center">
        <tr class="tblTitulo" align="center">
          <td >Planilla</td>
          <td>Placa</td>
          <td>Cliente</td>
          <td>Agencia<br>
          Despacho</td>
          <td>Agencia<br>
          Due&ntilde;a</td>
          <td>Origen</td>
          <td>Destino</td>
          <td>Fecha</td>
          <td>Tipo Reporte </td>
          <td nowrap width="300">Observacion</td>
          <td nowrap>Tiempo de la <br>
            Detenci&oacute;n</td>
          <td>Tipo Procedencia </td>
          <td>Ubicaci&oacute;n</td>
          <td>Zona</td>
          <td>OT's</td>
		</tr>
    <pg:pager
         items="<%= vec.size()%>"
         index="<%= index %>"
         maxPageItems="<%= maxPageItems %>"
         maxIndexPages="<%= maxIndexPages %>"
         isOffset="<%= true %>"
         export="offset,currentPageNumber=pageNumber"
         scope="request">
        <%-- keep track of preference --%>
<%
    for (int i = offset.intValue(), l = Math.min(i + maxPageItems, vec.size()); i < l; i++){
        ht = (Hashtable) vec.elementAt(i);%>
        <pg:item>
        <tr valign="top" class="<%=(i % 2 == 0 )?"filagris":"filaazul"%>" style="cursor:hand"  title="Lista de Paginas" onMouseOver='cambiarColorMouse(this)'>
          <td nowrap class="bordereporte"><%= ht.get("numpla") %></td>
          <td nowrap class="bordereporte"><%= ht.get("plaveh") %></td>
          <td nowrap class="bordereporte"><%= ht.get("nomcliente") %></td>
          <td nowrap class="bordereporte"><%= ht.get("agencia") %></td>
          <td nowrap class="bordereporte"><%= ht.get("agenciad") %></td>
          <td nowrap class="bordereporte"><%= ht.get("origen") %></td>
          <td nowrap class="bordereporte"><%= ht.get("destino") %></td>
          <td nowrap class="bordereporte"><%= ht.get("fechareporte") %></td>
          <td nowrap class="bordereporte"><%= ht.get("tipo_reporte") %></td>
          <td class="bordereporte"  style="font-size:11px "><%= ht.get("observacion") %></td>
          <td nowrap class="bordereporte"><div align="center"><%= ht.get("tiempo").toString().replaceAll("days", "dias").replaceAll("day", "dia") %></div></td>
          <td nowrap class="bordereporte"><%= ht.get("tipo_procedencia").toString().toUpperCase() %></td>
          <td nowrap class="bordereporte"><%= ht.get("ubicacion") %></td>
          <td nowrap class="bordereporte"><%= ht.get("zona") %></td>
          <td nowrap class="bordereporte"><%= ht.get("OTS") %></td>
		</tr>
      </pg:item>
<%  }%>
      <tr   class="bordereporte">
          <td td height="20" colspan="40" nowrap align="center">
		   <pg:index>
            <jsp:include page="/WEB-INF/jsp/googlesinimg.jsp" flush="true"/>      
           </pg:index> 
	      </td>
        </tr>
        </pg:pager>  
    </table>
<%} else {%>
    <table border="2" align="center">
      <tr>
        <td>
	      <table width="100%"  border="1" align="center"  bordercolor="#F7F5F4" bgcolor="#FFFFFF">
            <tr>
              <td width="229" align="center" class="mensajes">Su b&uacute;squeda no arroj&oacute; resultados!</td>
              <td width="29"><img src="<%=BASEURL%>/images/cuadronaranja.JPG"></td>
            </tr>
          </table>
	    </td>
      </tr>
    </table>
<%}%>
<p>
<table width="100%" align="center">
  <tr>
  	<td>
      <img src="<%=BASEURL%>/images/botones/regresar.gif" name="c_regresar" id="c_regresar" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" onClick="window.location.href = '<%= CONTROLLER%>?estado=Menu&accion=Cargar&carpeta=/jsp/trafico/reportes&pagina=ReporteVehiculosDemorados.jsp&marco=no&opcion=28'; ">
    </td>
    <td align="left" width="50%">
    <img src="<%=BASEURL%>/images/botones/iconos/excel.jpg"  name="imgaceptar" onClick="forma.submit();"  style="cursor:hand" height="30"></td>
  </tr>
</table>
</form>
</div>
</body>
</html>
