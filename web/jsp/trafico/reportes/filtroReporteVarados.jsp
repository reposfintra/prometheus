<%@ page session="true"%>
<%@ page errorPage="../error/ErrorPage.jsp"%>
<%@page import="com.tsp.util.*"%>
<%@ include file="/WEB-INF/InitModel.jsp"%>
<%@ taglib uri="http://jakarta.apache.org/taglibs/input-0.90" prefix="input" %>
<%@page import="com.tsp.operation.model.*, com.tsp.operation.model.beans.*, com.tsp.util.*,java.util.*, java.text.*"%>

<html>
<head>
    <title>Reporte Varados Cliente</title>
    <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">    
    <script src="<%=BASEURL%>/js/utilidades.js"></script>
    <link href="../css/estilostsp.css" rel="stylesheet" type="text/css">
    <script type='text/javascript' src="<%= BASEURL %>/js/boton.js"></script>
    <script type='text/javascript' src="/fintravalores/js/validar.js"></script> 
    <script src='/fintravalores/js/date-picker.js'></script>
    <link href="<%=BASEURL%>/css/estilostsp.css" rel="stylesheet" type="text/css">
<%
    String mensaje = "Ha iniciado la generación de su reporte, puede tardar algunos minutos...";
    String path = application.getRealPath(model.menuService.getRealUrl(request.getRequestURI()));
    String datos[] = model.menuService.getContenidoMenu(BASEURL,request.getRequestURI(),path);    
    
    String hoy = Util.getFechaActual_String(4);
%>
</head>

<body>
    <div id="capaSuperior" style="position:absolute; width:100%; height:100px; z-index:0; left: 1px; top: 0px;">
        <jsp:include page="/toptsp.jsp?encabezado=Reporte Varados Cliente"/>
    </div>
    <div id="capaCentral" style="position:absolute; width:100%; height:87%; z-index:0; left: 0px; top:100px; overflow: scroll;">        
    
    <form id="formulario" name="formulario"  method="post" action="<%=CONTROLLER%>?estado=Reporte&accion=Varados&opcion=report">
    <table width="350"  border="2" align="center">
        <tr>
            <td><table width="100%"  border="1" align="center" class="tablaInferior">
            <tr>
            <td><table width="100%" border="0" cellpadding="0" cellspacing="0">
            <tr>
            <td height="22" colspan=2 class="subtitulo1"><div align="center" class="subtitulo1">
                <strong>&nbsp;Cliente y Fecha</strong>                     
            </div></td>
            <td width="212" class="barratitulo">
            <img src="<%=BASEURL%>/images/titulo.gif" width="32" height="20" align="left">
                    <%=datos[0]%>
        </tr>
            </table>
  
                
            <table width="100%"  border="0" align="center" cellpadding="2" cellspacing="3" onclick="document.getElementById('noexiste').style.visibility='hidden';">
                 
                       
                <tr  class="fila">
                    <td >Cliente </td>
                    <td width='*' > 
                        <input type="text" name="cod_cli" id="cod_cli"  >
                        <img src='/fintravalores/images/botones/iconos/lupa.gif' width="15" height="15" onClick="javascript:abrirVentanaBusq(850,550,'/fintravalores/jsp/general/exportar/demoras/BuscarCliente.jsp')" title="Buscar" style="cursor:hand" >

                    </td>
               </tr>
               
               <tr class="fila">                
                    <td class="fila">Fecha Inicial : </td>
                    <td><input type="text" name="inicial" id="inicial" size="10" readonly value="<%=hoy%>" >
                        <a href="javascript:void(0)" onclick="if(self.gfPop)gfPop.fPopCalendar(inicial);return false;" HIDEFOCUS><img src="<%=BASEURL%>\js\Calendario\cal.gif" width="16" height="16" border="0" alt="Click aqui para escoger la fecha"></a>
                    </input></td>
		</tr>
               
		
		<tr class="fila">                
                    <td class="fila">Fecha Final : </td>
                    <td><input type="text" name="final" id="final" size="10" readonly value="<%=hoy%>" >
                        <a href="javascript:void(0)" onclick="if(self.gfPop)gfPop.fPopCalendar(final);return false;" HIDEFOCUS><img src="<%=BASEURL%>\js\Calendario\cal.gif" width="16" height="16" border="0" alt="Click aqui para escoger la fecha"></a>
                    </input></td>
		</tr>
                
               
               
            </table>
           
            </td>
            </tr>
        </table></td>
        </tr>
    </table>
    <br>
    <div align="center">
        
        <img src="<%=BASEURL%>/images/botones/aceptar.gif"  height="21" onMouseOver="botonOver(this);" onClick="sub('<%=CONTROLLER%>');" onMouseOut="botonOut(this);" style="cursor:hand">        
        <img src="<%=BASEURL%>/images/botones/salir.gif"  height="21" name="imgsalir"  onMouseOver="botonOver(this);" onClick="window.close();" onMouseOut="botonOut(this);" style="cursor:hand">            
        
    </div>
    </form>
    <br/>
	
        
    <div id="noexiste" style="visibility:hidden">
       <%if(mensaje != null && !mensaje.equals("") ){ %>
        
        <table border="2" align="center">
            <tr>                
                <td height="45"><table width="410" height="41" border="1" align="center"  bordercolor="#F7F5F4" bgcolor="#FFFFFF">
                    <tr>
                        <td width="282" height="35" align="center" class="mensajes"><%=mensaje%></td>
                        <td width="28" background="<%=BASEURL%>/images/cuadronaranja.JPG">&nbsp;</td>
                        <td width="78">&nbsp;</td>
                    </tr>
                </table></td>
            </tr>
        </table>        
               
       <%}%>     
    </div>
    	      
    </div>

<%=datos[1]%>
<iframe width=188 height=166 name="gToday:normal:agenda.js:gfPop:plugins.js" id="gToday:normal:agenda.js:gfPop:plugins.js" src="<%=BASEURL%>/js/Calendario/ipopeng.htm" scrolling="no" frameborder="0" style="visibility:visible; z-index:999; position:absolute; left:-500px; top:0px;"> </iframe>        
</body>

</html>
<script>

function sub( con ){
    
    if( formulario.cod_cli.value == "" ){
        alert("Debe ingresar el cliente");
        return false;
    }
    formulario.submit();
}

function abrirVentanaBusq( an, al, url, pag ) {
			
    parent.open( url, 'Conductor', 'width=' + an + ', height=' + al + ',scrollbars=no,resizable=no,top=10,left=65,status=yes' );
				
}


</script>