<!--  
     - Author(s)       :      FERNEL VILLACOB DIAZ
     - Date            :      14/08/2006  
     - Copyright Notice:      TRANSPORTES SANCHEZ POLO S.A.
  -->
 <%--   - @(#)  
     - Description:  Vista que permite setear valores en los combos
 --%>

<%@page    session   ="true"%> 
<%@page    errorPage ="/error/ErrorPage.jsp"%>
<%@page    import    ="com.tsp.operation.model.beans.*"%>
<%@include file      ="/WEB-INF/InitModel.jsp"%>
<%@page    import    ="com.tsp.util.*"%>
<%@page    import    ="java.util.*" %>



<script>var  option = '';</script>

  
<% String op = request.getParameter("op");
   if(op!=null){
       
      if( op.equals("DESTINO") ){
          List       lista  = model.ReportePlanillasViaSvc.getDestino();
          for(int i=0;i<lista.size();i++){
              Hashtable pc  = (Hashtable)lista.get(i);
              String    cod = (String) pc.get("codigo");
              String    sel = (cod.equals("CG"))?"selected":"";%>
              <script>option += "<option  value='<%= pc.get("codigo")%>'  <%= sel %> > <%= pc.get("nombre")%> ";</script>
         <%}%>     
         <SCRIPT>parent.opDestinos.innerHTML  =  " <select  name='destino'  style='width:90%' title='DESTINO'> " + option +"</select>";</SCRIPT>
     <%}
       
      if( op.equals("VIAS") ){
          List       lista  = model.ReportePlanillasViaSvc.getVias();
          for(int i=0;i<lista.size();i++){
              Hashtable pc = (Hashtable)lista.get(i);%>
              <script>  option += "<option value='<%= pc.get("codigo")%>'> <%= pc.get("nombre")%> ";</script>
        <%}%> 
         <SCRIPT> parent.opVias.innerHTML     = " <select  name='via'      style='width:90%' title='VIA'> " + option +"</select>"; </SCRIPT>
    <% }
  }%>

  
  
<script>
   if(option=='')
      alert('No se encontraron registros...');
</script>




