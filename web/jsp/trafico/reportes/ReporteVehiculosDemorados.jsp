<!--
- Autor : Ing. Tito Andr�s Maturana
- Date  : 4 enero del 2006
- Copyrigth Notice : Fintravalores S.A. S.A
-->
<%--
-@(#)
--Descripcion : Pagina JSP, que maneja la generaci�n del reporte
				de coherecia de reportes en puestos de control.
--%>
<%@page contentType="text/html"%>
<%@page import="java.util.*"%>
<%@page import="com.tsp.util.*"%>
<%@include file="/WEB-INF/InitModel.jsp"%>
<%@ taglib uri="http://jakarta.apache.org/taglibs/input-0.90" prefix="input" %>
<%String path = application.getRealPath(model.menuService.getRealUrl(request.getRequestURI()));
  String datos[] = model.menuService.getContenidoMenu(BASEURL,request.getRequestURI(),path);
%>
<html>
<head>
<script>
function validar(){
	validaFechas();
    if ( forma.origen.options(forma.origen.selectedIndex).value != '' && forma.destino.options(forma.destino.selectedIndex).value == '' ){
        alert('Debe seleccionar un destino.');
        return false;
    } else if (!( forma.plaveh.value != '' || forma.agencias.options(forma.agencias.selectedIndex).value != ''
            || forma.clientes.options(forma.clientes.selectedIndex).value != '' || 
            ( forma.origen.options(forma.origen.selectedIndex).value != '' && forma.destino.options(forma.destino.selectedIndex).value != '' )
			|| ( forma.FechaI.value != '' && forma.FechaF.value != '' ) || forma.treporte.options(forma.treporte.selectedIndex).value != '' || forma.zona.options(forma.zona.selectedIndex).value != '')) {
            
        alert('Debe al menos seleccionar un filtro de b�squeda.');
        return false;
    } else {
        return true;
    }
}
</script>
<title>Reporte de Vehiculos Demorados y Varados</title>
<link href="<%= BASEURL %>/css/estilostsp.css" rel='stylesheet'>
<link href="<%= BASEURL %>/webapps/css/estilostsp.css" rel="stylesheet" type="text/css">
<link href="/css/estilostsp.css" rel="stylesheet" type="text/css">
</head>
<script type='text/javascript' src="<%= BASEURL %>/js/validar.js"></script>
<script src='<%= BASEURL %>/js/date-picker.js'></script>
<script type='text/javascript' src="<%= BASEURL %>/js/boton.js"></script>
<script>    
    function actualizarInfo() {
		forma.origenselec.value = forma.origen.options(forma.origen.selectedIndex).value;
	    forma.action = "<%=CONTROLLER%>?estado=Reporte&accion=VehiculosVaradosRefresh&cmd=show&agencia=" + 
				forma.agencias.options(forma.agencias.selectedIndex).value + "&cliente=" + 
				forma.clientes.options(forma.clientes.selectedIndex).value + "&plaveh=" + forma.plaveh.value;        
        forma.submit();
    }
	function validaFechas(){
		var fecha1 = document.forma.FechaI.value.replace('-','').replace('-','');
		var fecha2 = document.forma.FechaF.value.replace('-','').replace('-','');
		var fech1 = parseFloat(fecha1);
		var fech2 = parseFloat(fecha2);
		if(fech1>fech2) {     
			alert('La fecha final debe ser mayor que la fecha inicial');
			return (false);
		}
		return true;
	}
	function parametros(){
            forma.action = forma.action + "&rstatus=D";
            forma.submit();

        }
</script>
<body onresize="redimensionar()" onload = 'redimensionar()'>
<div id="capaSuperior" style="position:absolute; width:100%; height:100px; z-index:0; left: 0px; top: 0px;">
 <jsp:include page="/toptsp.jsp?encabezado=Reporte de Veh�culos Varados y Demorados"/>
</div>
<div id="capaCentral" style="position:absolute; width:100%; height:87%; z-index:0; left: 0px; top: 100px; overflow: scroll;">
<%
    String      hoy       = Utility.getDate(4).replaceAll("/","-");
    //TreeMap agencias = model.ciudadService.listadoAgencias();
	TreeMap agencias = model.agenciaService.getAgencia();
    //TreeMap origins  = model.tramoService.listarOrigenes();
	TreeMap origins  = model.tramoService.getOrigenes();
    //TreeMap clientes = model.clienteService.listarClientes();    
	TreeMap clientes = model.clienteService.getTreemap();
    //TreeMap destinos = new TreeMap();
    TreeMap destinos = model.viaService.getCbxVias();
	TreeMap reportes = new TreeMap();
	TreeMap zonas = model.zonaService.getZonas();
	
	agencias.remove("");
    destinos.put(" Seleccione", "");
    clientes.put(" Seleccione", "");
    agencias.put(" Seleccione", "");
	destinos.put(" Seleccione", "");
	reportes.put(" TODOS", "");
	reportes.put("DEMORADO", "DEM");
	reportes.put("VARADO", "VAR");
	zonas.put(" Seleccione", "");

    /*if ( request.getParameter("origenselec")!=null){
	 destinos = model.tramoService.listarDestinos(request.getParameter("origenselec").toString());
    }*/

    String plavehsel = ( request.getParameter("plaveh")!=null ) ? request.getParameter("plaveh") : "";
    String codcli = ( request.getAttribute("codcli")!=null ) ? (String) request.getAttribute("codcli") : "";
    String codagc = ( request.getAttribute("codagc")!=null ) ? (String)  request.getAttribute("codagc") : "";
	String origensel = ( request.getAttribute("origensel")!=null ) ? (String)  request.getAttribute("origensel") : "";
    //( request.getParameter("")!=null ) ? request.getParameter("") : "";
%>
<form name="forma" action='<%=CONTROLLER%>?estado=Reporte&accion=VehiculosVarados' id="forma" method="post">
  <table width="451"  border="2" align="center" cellpadding="0" cellspacing="0">
    <tr>
      <td><table width="100%" class="tablaInferior">
        <tr>
          <td colspan="2" ><table width="100%"  border="0" cellpadding="0" cellspacing="1" class="barratitulo">
              <tr>
                <td width="56%" class="subtitulo1">&nbsp;Filtros de Reporte </td>
                <td width="44%" class="barratitulo"><img src="<%=BASEURL%>/images/titulo.gif" align="left" width="32" height="20"><%=datos[0]%></td>
              </tr>
          </table></td>
        </tr>
        <tr class="fila">
          <td width="27%">Placa</td>
          <td width="73%" nowrap><input name="plaveh" type="text" class="textbox" id="plaveh" value="<%= request.getAttribute("plaveh")!=null ? (String) request.getAttribute("plaveh") : "" %>">
            <span class="subtitulo1"></span></td>
        </tr>
        <tr class="fila">
          <td>Cliente</td>
          <td nowrap><input:select name="clientes" default="<%= codcli %>" attributesText="class=textbox" options="<%=clientes %>"/></td>
        </tr>
        <tr class="fila">
          <td>Origen</td>
          <td nowrap><input:select name="origen" default="<%= origensel %>" attributesText="id='origen' class=textbox onChange='actualizarInfo();'" options="<%=origins %>"/></td>
        </tr>
        <tr class="fila">
          <td>Destino</td>
          <td nowrap><input type="hidden" name="origenselec" id="origenselec" value="<%=request.getParameter("origenselec") %>"/>            <input:select name="destino" attributesText="class=textbox" options="<%=destinos %>"/></td>
        </tr>
        <tr class="fila">
          <td>Agencia Despacho</td>
          <td nowrap><input:select name="agencias" default="<%= codagc %>" attributesText="class=textbox" options="<%= agencias %>" /></td>
        </tr>
        <tr class="fila">
          <td>Fecha Inicial </td>
          <td nowrap>
            <input name="FechaI" type='text' class="textbox" id="FechaI" style='width:120' value='<%= request.getAttribute("fechai")!=null ? (String) request.getAttribute("fechai") : "" %>' readonly>
            <a href="javascript:void(0)" onclick="if(self.gfPop)gfPop.fPopCalendar(document.forma.FechaI);return false;" HIDEFOCUS><img src="<%=BASEURL%>\js\Calendario\cal.gif" width="16" height="16"
               border="0" alt="De click aqu&iacute; para escoger la fecha inicial"></a></td>
        </tr>
        <tr class="fila">
          <td>Fecha Final </td>
          <td nowrap>
            <input name="FechaF" type='text' class="textbox" id="FechaF" style='width:120' value='<%= request.getAttribute("fechaf")!=null ? (String) request.getAttribute("fechaf") : "" %>' readonly>
            <a href="javascript:void(0)" onclick="if(self.gfPop)gfPop.fPopCalendar(document.forma.FechaF);return false;" HIDEFOCUS><img src="<%=BASEURL%>\js\Calendario\cal.gif" width="16" height="16"
               border="0" alt="De click aqu&iacute; para escoger la fecha inicial"></a> </td>
        </tr>
        <tr class="fila">
          <td>Tipo de Reporte</td>
          <td nowrap><input:select name="treporte" default="<%= codagc %>" attributesText="class=textbox" options="<%= reportes %>" /></td>
        </tr>
        <tr class="fila">
          <td>Zona</td>
          <td nowrap><input:select name="zona" default="<%= codagc %>" attributesText="class=textbox" options="<%= zonas %>" /></td>
        </tr>
        <tr class="fila">
          <td colspan="2" class="Simulacion_Hiper" onclick="parametros();" style='cursor:hand'>Consultar vehiculos detenidos a la fecha actual</td>
          </tr>
      </table></td>
    </tr>
  </table>
  <br>
  <center>
    <img src="<%=BASEURL%>/images/botones/aceptar.gif" name="c_aceptar" onClick="if (validar()) forma.submit();" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);"> 
    <img src="<%=BASEURL%>/images/botones/cancelar.gif" name="c_cancelar" onClick="forma.reset();" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);"> 
    <img src="<%=BASEURL%>/images/botones/salir.gif" name="c_salir" onClick="parent.close();" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);">
  </center>
</form>
<%
    if( request.getParameter("msg") != null ){
%>
  <p><table border="2" align="center">
  <tr>
    <td><table width="100%"  border="1" align="center"  bordercolor="#F7F5F4" bgcolor="#FFFFFF">
      <tr>
        <td width="229" align="center" class="mensajes"><%=request.getParameter("msg").toString()%></td>
        <td width="29" background="<%=BASEURL%>/images/cuadronaranja.JPG">&nbsp;</td>
        <td width="58">&nbsp;</td>
      </tr>
    </table></td>
  </tr>
</table>
</p>
<%
    }
%>
</div>
<iframe width=188 height=166 name="gToday:normal:agenda.js:gfPop:plugins.js" id="gToday:normal:agenda.js:gfPop:plugins_24.js" src="js/Calendario/ipopeng.htm" scrolling="no" frameborder="0" style="visibility:visible; z-index:999; position:absolute; left:-500px; top:0px;"> </iframe>
<%=datos[1]%>
</body>
</html>
