<%@ page session="true"%>
<%//@ page errorPage="../error/ErrorPage.jsp"%>
<%@page import="com.tsp.util.*"%>
<%@ include file="/WEB-INF/InitModel.jsp"%>
<%@ taglib uri="http://jakarta.apache.org/taglibs/input-0.90" prefix="input" %>
<%@page import="com.tsp.operation.model.*, com.tsp.operation.model.beans.*, com.tsp.util.*,java.util.*, java.text.*"%>

<html>
<head>
    <title>Reporte Varados</title>
    <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">

    <script src="<%=BASEURL%>/js/utilidades.js"></script>
    <link href="../css/estilostsp.css" rel="stylesheet" type="text/css">
    <script type='text/javascript' src="<%= BASEURL %>/js/boton.js"></script>
    <link href="<%=BASEURL%>/css/estilostsp.css" rel="stylesheet" type="text/css">
<%
    
    
    String mensaje = (String)request.getAttribute("mens");
    String path = application.getRealPath(model.menuService.getRealUrl(request.getRequestURI()));
    String datos[] = model.menuService.getContenidoMenu(BASEURL,request.getRequestURI(),path);
    
    String ini = request.getParameter("ini")!=null? request.getParameter("ini") : "";
    String fin = request.getParameter("fin")!=null? request.getParameter("fin") : "";
    
    Vector v = (Vector) request.getAttribute("varados");
   
%>
</head>

<body>
    <div id="capaSuperior" style="position:absolute; width:100%; height:100px; z-index:0; left: 1px; top: 0px;">
        <jsp:include page="/toptsp.jsp?encabezado=Reporte Varados"/>
    </div>
    <div id="capaCentral" style="position:absolute; width:100%; height:85%; z-index:0; left: 0px; top:100px; overflow: scroll;">        
    <form id="form1"  name="form1" method="post" action="">

            <table width="100%"  border="0" align="center" cellpadding="2" cellspacing="3" onclick="document.getElementById('noexiste').style.visibility='hidden';">
                
                        
                <%
                   
                        
                        if( v !=null && v.size() > 0 ){
                %>
                
                <div align="center">                
                    <img src="<%=BASEURL%>/images/botones/exportarExcel.gif"  height="21" onMouseOver="botonOver(this);" onClick="reporte('<%=CONTROLLER%>');" onMouseOut="botonOut(this);" style="cursor:hand">
                    <img src="<%=BASEURL%>/images/botones/regresar.gif"  height="21" onMouseOver="botonOver(this);" onClick="location.replace('<%=BASEURL%>/jsp/trafico/reportes/filtroReporteVarados.jsp');" onMouseOut="botonOut(this);" style="cursor:hand">
                    <img src="<%=BASEURL%>/images/botones/salir.gif"  height="21" name="imgsalir"  onMouseOver="botonOver(this);" onClick="window.close();" onMouseOut="botonOut(this);" style="cursor:hand">            
                </div>
                <br>
                                    <table width="100%"  border="2" align="center">


                            <tr>
                            <td width="100%">
                                <table width="100%" class="tablaInferior">
                                    <tr>
                                        <td height="22" colspan=2 class="subtitulo1">Reporte Varados</td>
                                        <td width="50%" class="barratitulo"><img src="<%=BASEURL%>/images/titulo.gif" width="32" height="20"></td>
                                    </tr>
                                    </table> 

                                
                                <table width="100%" border="1" align="center" bordercolor="#999999" bgcolor="#F7F5F4">    
                                <tr class="tblTitulo" align="left">
                                    <td colspan="24"> &nbsp;<%=(v.size())%>&nbsp;Registros Encontrados</td>
                                </tr>                                                                                              
                                
                                <tr class="tblTitulo" align="center">
                                        <td nowrap>Cliente</td>
                                        <td nowrap>OT'S</td>
                                        <td nowrap>Descripcion OT</td>
                                        <td nowrap>Ruta OT</td>
                                        <td nowrap>Agencia Desp</td>
                                        <td nowrap>OC</td>
                                        <td nowrap>Ruta OC</td>
                                        <td nowrap>Placa</td>
                                        <td nowrap>Fecha Reporte</td>
                                        <td nowrap>Hora Reporte</td>                                    
                                        <td nowrap>Puesto Control</td>
                                        <td nowrap>Sig Fec</td>
                                        <td nowrap>Sig Hora</td>
                                        <td nowrap>Sig Pto</td>
                                        <td nowrap>Observacion</td>
                                        <td nowrap>Tiempo Varado</td>
                                        <td nowrap>Fecha Despacho</td>
                                        <td nowrap>Hora Despacho</td>
                                        <td nowrap>Fecha Entrega</td>
                                        <td nowrap>Hora Entrega</td>
                                        
                                    </tr>
                                                                                                                                        
                                
                                    <%
                                     
                                     for(int i = 0; i<v.size(); i++){
                                         Hashtable h = (Hashtable)v.get(i);
                                    %>
                                        <tr class="<%=i%2==0?"filagris":"filaazul"%>" onMouseOver='cambiarColorMouse(this)' 
                                        align="center">    
                                        
                                        <td class="bordeReporte" nowrap>&nbsp;<%=h.get("cliente")%></td>                                        
                                        <td class="bordeReporte" nowrap>&nbsp;<%=h.get("ot")%></td>
                                        <td class="bordeReporte" nowrap>&nbsp;<%=h.get("desc_ot")%></td>
                                        <td class="bordeReporte" nowrap>&nbsp;<%=h.get("ruta_ot")%></td>
                                        <td class="bordeReporte" nowrap>&nbsp;<%=h.get("ag_desp")%></td>
                                        <td class="bordeReporte" nowrap>&nbsp;<%=h.get("oc")%></td>
                                        <td class="bordeReporte" nowrap>&nbsp;<%=h.get("ruta_oc")%></td>
                                        <td class="bordeReporte" nowrap>&nbsp;<%=h.get("placa")%></td>
                                        <td class="bordeReporte" nowrap>&nbsp;<%=h.get("fecha_reporte")%></td>
                                        <td class="bordeReporte" nowrap>&nbsp;<%=h.get("hora_reporte")%></td>
                                        <td class="bordeReporte" nowrap>&nbsp;<%=h.get("puesto_control")%></td>
                                        <td class="bordeReporte" nowrap>&nbsp;<%=h.get("sig_fec")%></td>
                                        <td class="bordeReporte" nowrap>&nbsp;<%=h.get("sig_hor")%></td>
                                        <td class="bordeReporte" nowrap>&nbsp;<%=h.get("sig_pto")%></td>
                                        <td class="bordeReporte" nowrap>&nbsp;<%=h.get("observacion")%></td>
                                        <% String tiempo = (String)h.get("tiempo");%>
                                        <% String horas = !(tiempo).equals("")? UtilFinanzas.customFormat( "#,##0.00", Util.toHours(tiempo), 2 ) : "";  %>
                                        <td class="bordeReporte" nowrap>&nbsp;<%=horas%></td>
                                        <td class="bordeReporte" nowrap>&nbsp;<%=h.get("fecha_despacho")%></td>
                                        <td class="bordeReporte" nowrap>&nbsp;<%=h.get("hora_despacho")%></td>
                                        <td class="bordeReporte" nowrap>&nbsp;<%=h.get("fecha_entrega")%></td>
                                        <td class="bordeReporte" nowrap>&nbsp;<%=h.get("hora_entrega")%></td>
                                        
                                        <%}%>
                                </tr> 
                            

                            </td>
                        </tr>            

                                      </table>                        
                            </table>   
                            <br>
                            <div align="center">                
                                <img src="<%=BASEURL%>/images/botones/exportarExcel.gif"  height="21" onMouseOver="botonOver(this);" onClick="reporte('<%=CONTROLLER%>');" onMouseOut="botonOut(this);" style="cursor:hand">
                                <img src="<%=BASEURL%>/images/botones/regresar.gif"  height="21" onMouseOver="botonOver(this);" onClick="location.replace('<%=BASEURL%>/jsp/trafico/reportes/filtroReporteVarados.jsp');" onMouseOut="botonOut(this);" style="cursor:hand">
                                <img src="<%=BASEURL%>/images/botones/salir.gif"  height="21" name="imgsalir"  onMouseOver="botonOver(this);" onClick="window.close();" onMouseOut="botonOut(this);" style="cursor:hand">            
                            </div>
                <%          }//fin si
                            else{
                                mensaje = "No se encontraron datos para el reporte en este periodo";%>
                                <br>
                                <div align="center">                                                
                                    <img src="<%=BASEURL%>/images/botones/regresar.gif"  height="21" onMouseOver="botonOver(this);" onClick="location.replace('<%=BASEURL%>/jsp/trafico/reportes/filtroReporteVarados.jsp');" onMouseOut="botonOut(this);" style="cursor:hand">
                                    <img src="<%=BASEURL%>/images/botones/salir.gif"  height="21" name="imgsalir"  onMouseOver="botonOver(this);" onClick="window.close();" onMouseOut="botonOut(this);" style="cursor:hand">            
                                </div>
                            <%}
                %>
                                                  
            </table>
    <br>
    
    </form>
    <br/>
	
        
    <div id="noexiste">
       <%if(mensaje != null && !mensaje.equals("") ){ %>
        
        <table border="2" align="center">
            <tr>                
                <td height="45"><table width="410" height="41" border="1" align="center"  bordercolor="#F7F5F4" bgcolor="#FFFFFF">
                    <tr>
                        <td width="282" height="35" align="center" class="mensajes"><%=mensaje%></td>
                        <td width="28" background="<%=BASEURL%>/images/cuadronaranja.JPG">&nbsp;</td>
                        <td width="78">&nbsp;</td>
                    </tr>
                </table></td>
            </tr>
        </table>        
               
       <%}%>     
    </div>
    	      
    </div>
<%=datos[1]%>
</body>
<script>


function reporte( con ){    
    alert("Su reporte se est� generando, consulte sus archivos en unos instantes");
    location.replace(con+"?estado=Reporte&accion=Varados&opcion=excel&inicial=<%=ini%>&final=<%=fin%>");    
}
</script>
</html>
