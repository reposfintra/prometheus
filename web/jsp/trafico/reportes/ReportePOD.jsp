<!--
- Autor : Ing. Ivan Dario Gomez
- Date  : 2 de Septiembre de 2006
- Copyrigth Notice : Fintravalores S.A. S.A
-->
<%--
-@(#)
--Descripcion : Pagina JSP, Reporte POD Trafico

--%>

<%-- Declaracion de librerias--%>
<%@page contentType="text/html"%>
<%@page session="true"%>
<%@page errorPage="/error/ErrorPage.jsp"%>
<%@page import="java.util.*"%> 
<%@page import="com.tsp.operation.model.*"%>
<%@page import="com.tsp.util.*"%>
<%@page import="com.tsp.operation.model.beans.*, java.util.*"%>
<%@include file="/WEB-INF/InitModel.jsp"%>
<%@taglib uri="http://jsptags.com/tags/navigation/pager" prefix="pg" %>
 
<html>
<head>
<title>Reporte POD Trafico</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link href="<%= BASEURL %>/css/estilostsp.css" rel="stylesheet" type="text/css">
<link href="../../../../css/estilostsp.css" rel="stylesheet" type="text/css"> 
<script type='text/javascript' src="<%= BASEURL %>/js/reporte.js"></script>
<script type='text/javascript' src="<%= BASEURL %>/js/boton.js"></script>

</head>
<body onLoad="redimensionar();" onresize="redimensionar()">
<%String path    = application.getRealPath(model.menuService.getRealUrl(request.getRequestURI()));
  String datos[] = model.menuService.getContenidoMenu(BASEURL,request.getRequestURI(),path); %>
<div id="capaSuperior" style="position:absolute; width:100%; height:100px; z-index:0; left: 0px; top: 0px;">
	<jsp:include page="/WEB-INF/toptsp.jsp?encabezado=Reporte POD Trafico"/>
</div>
<div id="capaCentral" style="position:absolute; width:100%; height:87%; z-index:0; left: 0px; top: 100px; overflow: scroll;">

  <%String style = "simple";
    String position =  "bottom";
    String index =  "center";
    int maxPageItems = 10;
    int maxIndexPages = 10;
	
    List lista = model.ReportePODSvc.getReporte();
    ReportePOD reporte; 

	if ( lista.size() >0 ){  
%>
</p>
<table width="700" border="2" align="center">
    <tr>
      <td width="100%">
	  <table width="100%" align="center">
              <tr>
                <td width="50%" class="subtitulo1">&nbsp;Datos del Reporte</td>
                <td width="50%" class="barratitulo"><img src="<%=BASEURL%>/images/titulo.gif" width="32" height="20"  height="20" align="left"><%=datos[0]%></td>
              </tr>
        </table>
		  <table width="99%" border="1" align="center" bordercolor="#999999" bgcolor="#F7F5F4">
          <tr class="tblTitulo">
          <td nowrap align="center">Cliente</td>
          <td nowrap align="center">Planilla</td>
          <td nowrap align="center">Remesa</td>
          <td nowrap align="center">Fecha Remesa</td>
          <td nowrap align="center">Placa </td>
          <td nowrap align="center">Ruta </td>
          <td nowrap align="center">Destinatario </td>
          <td nowrap align="center">Fac. Comercial </td>
          <td nowrap align="center">Fecha de Llegada </td>
          <td nowrap align="center">No. Viaje</td>
          <td nowrap align="center">No. Nota entrega </td>  
          
          
        </tr>
        <pg:pager
         items="<%=lista.size()%>"
         index="<%= index %>"
         maxPageItems="<%= maxPageItems %>"
         maxIndexPages="<%= maxIndexPages %>"
         isOffset="<%= true %>"
         export="offset,currentPageNumber=pageNumber"
         scope="request">
        <%-- keep track of preference --%>
        <%
      for (int i = offset.intValue(), l = Math.min(i + maxPageItems, lista.size()); i < l; i++)
	  {
          reporte = (ReportePOD) lista.get(i);%>
        <pg:item>
        <tr class="<%=(i % 2 == 0 )?"filagris":"filaazul"%>" onMouseOver='cambiarColorMouse(this)'   style="cursor:hand">
          <td nowrap class="bordereporte"><%=reporte.getCliente()%></td>
          <td nowrap class="bordereporte"><%=reporte.getPlanilla()%></td>
          <td nowrap class="bordereporte"><%=reporte.getRemesa()%> </td>
          <td nowrap class="bordereporte"><%=reporte.getFechaRemesa()%></td>
          <td nowrap class="bordereporte"><%=reporte.getPlaca()%></td>
          <td nowrap class="bordereporte"><%=reporte.getRuta()%></td>
          <td nowrap class="bordereporte"><%=reporte.getDestinatario()%></td>
          <td nowrap class="bordereporte"><%=reporte.getFaccial()%></td>
          <td nowrap class="bordereporte"><%=reporte.getFechaLlegada()%></td>
          <td nowrap class="bordereporte"><%=reporte.getNum_viaje()%></td>
          <td nowrap class="bordereporte"><%=reporte.getNota_entrega()%></td>
        </tr>
        </pg:item>
        <%}%>
        <tr class="bordereporte">
          <td td height="20" colspan="11" nowrap align="center">
		   <pg:index>
            <jsp:include page="/WEB-INF/jsp/googlesinimg.jsp" flush="true"/>      
           </pg:index> 
	      </td>
        </tr>
        </pg:pager>
      </table></td>
    </tr>
</table>
  <p>
      <%}
 else { %>
</p>
  <table border="2" align="center">
    <tr>
      <td><table width="410" height="40" border="1" align="center"  bordercolor="#F7F5F4" bgcolor="#FFFFFF">
          <tr>
            <td width="282" align="center" class="mensajes">Su b&uacute;squeda no arroj&oacute; resultados!</td>
            <td width="28" background="<%=BASEURL%>/images/cuadronaranja.JPG">&nbsp;</td>
            <td width="78">&nbsp;</td>
          </tr>
      </table></td>
    </tr>
  </table>
  <p>&nbsp; </p>
  <%}%>
<table width="700" border="0" align="center">
   <tr>
     <td width="1515"><img src="<%=BASEURL%>/images/botones/regresar.gif"  name="imgaceptar" onClick="window.location='<%=CONTROLLER%>?estado=Menu&accion=Cargar&pagina=FiltroReportePOD.jsp&carpeta=/jsp/trafico/reportes&marco=no'" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" style="cursor:hand"></td>
   </tr>
 </table>
</div> 
<%=datos[1]%>  
</body>

</html>
