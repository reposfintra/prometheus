<%@page contentType="text/html"%>
<%@page session="true"%>
<%@page import="java.util.*"%>
<%@page import="com.tsp.operation.model.*"%>
<%@page import="com.tsp.operation.model.beans.*"%>
<%@page import="com.tsp.util.*"%>
<%@include file="/WEB-INF/InitModel.jsp"%>
<html>
<head>
<title>Asignar Actividad  Cliente</title>
<link href="<%= BASEURL %>/css/estilostsp.css" rel='stylesheet'>
<link href="../../../css/estilostsp.css" rel="stylesheet" type="text/css">
</head>
<script type='text/javascript' src="<%= BASEURL %>/js/validar.js"></script>
<script type='text/javascript' src="<%= BASEURL %>/js/boton.js"></script>
<body onResize="redimensionar()" <%= ( request.getParameter("reload")!=null ) ?"onLoad='window.opener.location.reload();redimensionar();window.close();'" : "onLoad='redimensionar();'"%>>
<div id="capaSuperior" style="position:absolute; width:100%; height:100px; z-index:0; left: 0px; top: 0px;">
 <jsp:include page="/toptsp.jsp?encabezado=Modificar Actividad Cliente"/>
</div>
<div id="capaCentral" style="position:absolute; width:100%; height:87%; z-index:0; left: 0px; top: 100px; overflow: scroll;">
<% String res="";
   String msg = (request.getParameter("mensaje")!=null)?request.getParameter("mensaje"):"";
   ClienteActividad ca = model.clientactSvc.obtClienteActividad();
   if (msg.equals("Modificado")){
       res = "Actividad Cliente modificada exitosamente!";
   }
  if (msg.equals("Existe")){
       res = "La Secuencia Ya existe";
   }
  
   
   
%>
<form name="forma" method="post" action="<%=CONTROLLER%>?estado=Clientact&accion=Modificar" onSubmit="return validarTCamposLlenos();">
    <table width="395" border="2" align="center">
      <tr>
        <td><table width="100%"  border="0">
          <tr>
            <td width="48%" class="subtitulo1"><span class="subtitulos">Informaci&oacute;n</span></td>
            <td width="52%" class="barratitulo"><img src="<%=BASEURL%>/images/titulo.gif" width="32" height="20"></td>
          </tr>
        </table>
          <table width="395" class="tablaInferior">
          <tr >
            <td width="99" class="fila">Cliente</td>
            <td width="271" class="letra"><input name="cliente" type="hidden" id="cliente" value="<%=ca.getCodCliente()%>"  ><%=ca.getNomCliente()%></td> </tr>
          <tr>
            <td class="fila">Tipo Viaje </td>
            <td class="letra"><input name="tipo" type="hidden" id="tipo" size="5" maxlength="4"  value="<%=ca.getTipoViaje()%>"><%=ca.getDestipo_viaje()%>
            </td>
          </tr>
          <tr>
            <td class="fila">Actividad</td>
            <td class="letra"><input name="actividad" type="hidden" id="actividad" size="11" maxlength="10" value="<%=ca.getCodActividad()%>">
                <%=ca.getLargaAct()%></td>
          </tr>
          <tr class="fila">
            <td>Secuencia</td>
            <td><input name="secuencia" type="text" class="textbox" id="secuencia" onKeyPress="soloDigitos(event,'decNO')" value="<%=ca.getSecuencia()%>" size="10" maxlength="6" >
              <img src="<%=BASEURL%>/images/botones/iconos/obligatorio.gif" width="10" height="10">
              <input name="sec" type="hidden" id="sec" value="<%=ca.getSecuencia()%>"></td>
          </tr>
        </table></td>
      </tr>
    </table>
	<br>
	<div align="center">
			  <img src="<%=BASEURL%>/images/botones/modificar.gif"  name="imgmodificar" onClick="return TCamposLlenos();" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" style="cursor:hand">&nbsp;
              <img src="<%=BASEURL%>/images/botones/anular.gif"  name="imganular" onClick="window.location='<%=CONTROLLER%>?estado=Clientact&accion=Anular&codact=<%=ca.getCodActividad()%>&codcli=<%=ca.getCodCliente()%>&tipo=<%=ca.getTipoViaje()%>'" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" style="cursor:hand">&nbsp;
              <img src="<%=BASEURL%>/images/botones/salir.gif"  name="imgsalir"  onMouseOver="botonOver(this);" onClick="parent.close();" onMouseOut="botonOut(this);" style="cursor:hand">
          </div>
	<%if( !res.equalsIgnoreCase("")){%>
  <p><table border="2" align="center">
  <tr>
    <td><table width="100%" border="1" align="center"  bordercolor="#F7F5F4" bgcolor="#FFFFFF">
      <tr>
        <td width="262" align="center" class="mensajes"><%=res%></td>
        <td width="32" background="<%=BASEURL%>/images/cuadronaranja.JPG">&nbsp;</td>
        <td width="44">&nbsp;</td>
      </tr>
    </table></td>
  </tr>
</table>
<%}%>

</form>
</div>
</body>
</html>
