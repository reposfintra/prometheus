<%@page contentType="text/html"%>
<%@page session="true"%>
<%@page import="java.util.*"%>
<%@page errorPage="/error/ErrorPage.jsp"%>
<%@page import="com.tsp.operation.model.*"%>
<%@page import="com.tsp.operation.model.beans.*"%>
<%@page import="com.tsp.util.*"%>
<%@include file="/WEB-INF/InitModel.jsp"%>
<html>
<head>
<title>Buscar Actividad</title>
</head>
<link href="<%= BASEURL %>/css/estilostsp.css" rel='stylesheet'>
<link href="../../../css/estilostsp.css" rel="stylesheet" type="text/css">
<script type='text/javascript' src="<%= BASEURL %>/js/boton.js"></script>
<body onResize="redimensionar()" onLoad="redimensionar()">
<div id="capaSuperior" style="position:absolute; width:100%; height:100px; z-index:0; left: 0px; top: 0px;">
 <jsp:include page="/toptsp.jsp?encabezado=Buscar Actividad Asignada "/>
</div>
<script>
function BuscarCliente(codcli,BASEURL){
   if(codcli == 'LUPA'){
       var dir = BASEURL+'/jsp/trafico/clienteactividad/buscarCliente.jsp';
	   var hijo=window.open(dir,'Impuesto','width=700,height=520,scrollbars=no,resizable=yes,top=10,left=65,status=yes');
	   hijo.opener=document.window;
   }else if(codcli!=''){
      var url = "?estado=Clientact&accion=Busqueda&cod_cli="+codcli+"&op=BUSCAR_CLIENTE";
	   enviar(url);
   }
}
</script>
<div id="capaCentral" style="position:absolute; width:100%; height:87%; z-index:0; left: 0px; top: 100px; overflow: scroll;">
<form name="forma" method="post" action="<%=CONTROLLER%>?estado=Clientact&accion=Busqueda" >
  <table width="66%" border="2" align="center">
    <tr>
      <td><table width="100%"  border="0">
        <tr>
          <td width="48%" class="subtitulo1"><span class="subtitulos">Informaci&oacute;n</span></td>
          <td width="52%" class="barratitulo"><img src="<%=BASEURL%>/images/titulo.gif" width="32" height="20"></td>
        </tr>
      </table>
        <table width="100%"  class="tablaInferior">
        <tr class="fila">
          <td width="22%">Cliente</td>
          <td width="78%"><input name="cod_cli" type="text" class="textbox" size="8" maxlength="6" onBlur="BuscarCliente(this.value,'<%=BASEURL%>')">
            <img src="<%= BASEURL %>/images/botones/iconos/lupa.gif" width="15" height="16" title="Buscar" style="cursor:hand" onClick="BuscarCliente('LUPA','<%=BASEURL%>')"> &nbsp; <input name="cliente" type="text" class="fila" style=" border:0" size="40" readonly></td>
        </tr>
        <tr class="fila">
          <td>Actividad</td>
          <td><input name="actividad" type="text" class="textbox" id="actividad" size="40" maxlength="30"></td>
        </tr>
        <tr class="fila">
          <td>Tipo Viaje </td>
          <td><select name="tipo" class="textbox" id="tipo" style="width:90% ">
            <option value=""></option>
            <% LinkedList tipo = model.tablaGenService.obtenerTablas();
               for(int j = 0; j<tipo.size(); j++){
                       TablaGen reg = (TablaGen) tipo.get(j); %>
            <option value="<%=reg.getTable_code()%>" ><%=reg.getDescripcion()%></option>
            <%}%>
          </select></td>
        </tr>
       
      </table></td>
    </tr>
  </table>
  <br>
    <div align="center">
	    <img src="<%=BASEURL%>/images/botones/buscar.gif"  name="imgbuscar" onClick="forma.submit()" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" style="cursor:hand">&nbsp;
	    <img src="<%=BASEURL%>/images/botones/detalles.gif"  name="imgdetalles"  onMouseOver="botonOver(this);" onClick="window.location='<%=CONTROLLER%>?estado=Clientact&accion=Busqueda&cliente=&actividad=&tipo='" onMouseOut="botonOut(this);" style="cursor:hand"> &nbsp;
        <img src="<%=BASEURL%>/images/botones/salir.gif"  name="imgsalir"  onMouseOver="botonOver(this);" onClick="parent.close();" onMouseOut="botonOut(this);" style="cursor:hand">
    </div>
</form>
</div>
</body>
<script>
    function enviar(url){
         var a = "<iframe name='ejecutor' style='visibility:hidden'  src='<%= CONTROLLER %>" + url + "'> ";
         aa.innerHTML = a;
    }
</script>
<font id='aa'></font>
</html>
