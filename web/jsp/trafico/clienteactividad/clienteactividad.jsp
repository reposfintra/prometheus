<%@page contentType="text/html"%>
<%@page session="true"%>
<%@page errorPage="/error/ErrorPage.jsp"%>
<%@page import="java.util.*"%>
<%@page import="com.tsp.operation.model.*"%>
<%@page import="com.tsp.operation.model.beans.*"%>
<%@page import="com.tsp.util.*"%>
<%@include file="/WEB-INF/InitModel.jsp"%>
<html>
<head>
<title>Asignar Actividad  Cliente</title>
<link href="<%= BASEURL %>/css/estilostsp.css" rel='stylesheet'>
<link href="../../../css/estilostsp.css" rel="stylesheet" type="text/css">
</head>
<script type='text/javascript' src="<%= BASEURL %>/js/validar.js"></script>
<script type='text/javascript' src="<%= BASEURL %>/js/boton.js"></script>
<body onResize="redimensionar()" onLoad="redimensionar()">
<div id="capaSuperior" style="position:absolute; width:100%; height:100px; z-index:0; left: 0px; top: 0px;">
 <jsp:include page="/toptsp.jsp?encabezado=Asignar Actividad Cliente"/>
</div>
<div id="capaCentral" style="position:absolute; width:100%; height:87%; z-index:0; left: 0px; top: 100px; overflow: scroll;">
<% String codcli ="",codact="",res="",sec="",tip="";
   String msg = (request.getParameter("mensaje")!=null)?request.getParameter("mensaje"):"";
   LinkedList clientes = model.clienteService.getClientesSot();
   Vector Vecact = model.actividadSvc.ObtVecActividad ();
   codcli = request.getParameter("cliente")  != null ? request.getParameter("cliente") : ""  ;
   codact =  request.getParameter("actividad") != null ? request.getParameter("actividad") : "" ;
   sec =  request.getParameter("secuencia") != null ? request.getParameter("secuencia") : "";
   tip = request.getParameter("tipo") != null ? request.getParameter("tipo") : "";
   if ( msg.equals("MsgAgregado")){//exito en el ingreso
	  res = "Actividad Asignada exitosamente!";
   } 
   else if ( msg.equals("MsgErrorIng")){
	   res = "Error...Actividad ya Asignada!";
   }
  else if ( msg.equals("Existe")){
	   res = "Error...La Secuencia ya Existe!";
   }
%>
<form name="forma" method="post" action="<%=CONTROLLER%>?estado=Clientact&accion=Ingresar" onSubmit="return validarTCamposLlenos();">
    <table width="420" border="2" align="center">
      <tr>
        <td><table width="100%"  border="0">
          <tr>
            <td width="48%" class="subtitulo1"><span class="subtitulos">Informaci&oacute;n</span></td>
            <td width="52%" class="barratitulo"><img src="<%=BASEURL%>/images/titulo.gif" width="32" height="20"></td>
          </tr>
        </table>
          <table width="100%" class="tablaInferior">
          <tr class="fila">
            <td width="113">Cliente</td>
            <td width="283"><select name="cliente" id="cliente" class="textbox" style="width:90% ">
                <%for (int i = 0; i < clientes.size(); i++){
			 Cliente clien = (Cliente) clientes.get(i);				
		    if (clien.getCodcli().equals(codcli)){%>
                <option value="<%=clien.getCodcli()%>" selected><%=clien.getNomcli()%></option>
                <%}else{%>
                <option value="<%=clien.getCodcli()%>"><%=clien.getNomcli()%></option>
                <%}
	}%>
              </select><img src="<%=BASEURL%>/images/botones/iconos/obligatorio.gif" width="10" height="10">
            </td>
          </tr>
          <tr class="fila">
            <td>Tipo Viaje </td>
            <td><select name="tipo" class="textbox" id="tipo" style="width:90% ">
              <option value="">Seleccione Un Item</option>
              <% LinkedList tipo = model.tablaGenService.obtenerTablas();
               for(int j = 0; j<tipo.size(); j++){
                       TablaGen reg = (TablaGen) tipo.get(j); %>
              <option value="<%=reg.getTable_code()%>" <%=(tip.equals(reg.getTable_code()))?"selected":""%>  ><%=reg.getDescripcion()%></option>
              <%}%>
            </select><img src="<%=BASEURL%>/images/botones/iconos/obligatorio.gif" width="10" height="10"></td>
          </tr>
          <tr class="fila">
            <td>Actividad</td>
            <td><select name="actividad" id="actividad" class="textbox">
                <%for (int i = 0; i < Vecact.size(); i++){
			 Actividad act = (Actividad) Vecact.elementAt(i);				
		    if (act.getCodActividad().equals(codact)){%>
                <option value="<%=act.getCodActividad()%>" selected><%=act.getDescorta()%></option>
                <%}else{%>
                <option value="<%=act.getCodActividad()%>"><%=act.getDescorta()%></option>
                <%}
	     }%>
              </select><img src="<%=BASEURL%>/images/botones/iconos/obligatorio.gif" width="10" height="10">
            </td>
          </tr>
          <tr class="fila">
            <td>Secuencia</td>
            <td><input name="secuencia" type="text" class="textbox" id="secuencia" onKeyPress="soloDigitos(event,'decNO')" value="<%=sec%>" size="10" maxlength="6" ><img src="<%=BASEURL%>/images/botones/iconos/obligatorio.gif" width="10" height="10"></td>
          </tr>
        </table></td>
      </tr>
    </table>
	<br>
	   <div align="center">
			 <img src="<%=BASEURL%>/images/botones/aceptar.gif"  name="imgaceptar" onClick="return TCamposLlenos();" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" style="cursor:hand">&nbsp;
			 <img src="<%=BASEURL%>/images/botones/cancelar.gif"  name="imgcancelar"  onMouseOver="botonOver(this);" onClick="window.location='<%=CONTROLLER%>?estado=Menu&accion=Cargar&pagina=clienteactividad.jsp&carpeta=/jsp/trafico/clienteactividad&marco=no'" onMouseOut="botonOut(this);" style="cursor:hand"> &nbsp;
             <img src="<%=BASEURL%>/images/botones/salir.gif"  name="imgsalir"  onMouseOver="botonOver(this);" onClick="parent.close();" onMouseOut="botonOut(this);" style="cursor:hand">
       </div>

	 <%if( !res.equalsIgnoreCase("")){%>
  <p><table border="2" align="center">
  <tr>
    <td><table width="100%" border="1" align="center"  bordercolor="#F7F5F4" bgcolor="#FFFFFF">
      <tr>
        <td width="262" align="center" class="mensajes"><%=res%></td>
        <td width="32" background="<%=BASEURL%>/images/cuadronaranja.JPG">&nbsp;</td>
        <td width="44">&nbsp;</td>
      </tr>
    </table></td>
  </tr>
</table>
<%}%>
 </form>

<%if ( msg.equals("MsgAgregado") ){
     Vector Vecca = model.clientactSvc.obtVecClientAct();%>
<table width="545" border="2" align="center">
  <tr>
    <td><table width="100%"  border="0">
      <tr>
        <td width="48%" class="subtitulo1"><span class="subtitulos">Actividades</span></td>
        <td width="52%" class="barratitulo"><img src="<%=BASEURL%>/images/titulo.gif" width="32" height="20"></td>
      </tr>
    </table>
      <table width="574" border="0" align="center" cellpadding="0" cellspacing="0">
      <tr>
        <td width="574"><table width="574"  border="1" align="center" bordercolor="#999999" bgcolor="#F7F5F4">
            <tr class="tblTitulo" align="center">
              <td width="80">Secuencia</td>
              <td width="86">Nombres</td>
              <td width="240">Descripci&oacute;n</td>
              <td width="140">Tipo viaje </td>
            </tr>
        </table></td>
      </tr>
      <tr>
        <td>
            <table width="100%" border="1" align="center" bordercolor="#999999" bgcolor="#F7F5F4">
              <%for (int i = 0; i < Vecca.size(); i++){
			 ClienteActividad ca = (ClienteActividad) Vecca.elementAt(i);%>
              <tr class="<%=(i % 2 == 0 )?"filagris":"filaazul"%>">
                <td width="15%" class="bordereporte"><%=ca.getSecuencia()%></td>
                <td width="17%" class="bordereporte"><%=ca.getDescortaAct()%></td>
                <td width="43%" class="bordereporte"><%=ca.getLargaAct()%></td>
                <td width="25%" class="bordereporte"><%=ca.getTipoViaje()%></td>
              </tr>
              <%}%>
            </table>
        </td>
      </tr>
    </table></td>
  </tr>
</table>
<%}%>
</div>
</body>
</html>
