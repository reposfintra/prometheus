

<!--
- Autor : Ing. David Lamadrid
- Date  : 5 de Octubre de 2005
- Copyrigth Notice : Fintravalores S.A. S.A
-->
<%--
-@(#)
--Descripcion : Pagina JSP, que se encarga de realizar y mostrar busqueda de los nits de proveedores
--%>

<%-- Declaracion de librerias--%>
<%@page contentType="text/html"%>
<%@page session="true"%>
<%@page import="java.util.*"%>
<%@page import="com.tsp.operation.model.*"%>
<%@page import="com.tsp.util.*"%>
<%@page import="com.tsp.operation.model.beans.*, java.util.*"%>
<%@include file="/WEB-INF/InitModel.jsp"%>
<%@taglib uri="http://jakarta.apache.org/taglibs/input-0.90" prefix="input" %>
<html>
<%System.out.println("BANDERA");%>
<head>
<script type='text/javascript' src="<%= BASEURL %>/js/boton.js"></script>
<script language="javascript">
    
	
	
	function accion(id){
		document.forma1.action = "<%= CONTROLLER %>?estado=Despachom&accion=BXPlaca&sw=2&placa="+id; 
        document.forma1.submit(); 
    }

    function procesar (element){
      if (window.event.keyCode==13) 
        listaPlacas();
    }
	
	function listaPlacas(){
	 	//alert("hola");
		var id= document.forma1.c_placa.value; 
      // alert("hola1");
		document.forma1.action = "<%= CONTROLLER %>?estado=Despachom&accion=Lplaca&sw=2&conductor="+id; 
        document.forma1.submit(); 
    }
</script>
<script type='text/javascript' src="<%= BASEURL %>/js/reporte.js"></script>

<title>Despacho Placa</title>
<link href="<%= BASEURL %>/css/estilostsp.css" rel="stylesheet" type="text/css">
<link href="../../../css/estilostsp.css" rel="stylesheet" type="text/css">
</head>

<body onresize="redimensionar()" onload = 'forma1.c_placa.focus(); redimensionar()'>
<div id="capaSuperior" style="position:absolute; width:100%; height:100px; z-index:0; left: 0px; top: 0px;">
 <jsp:include page="/toptsp.jsp?encabezado=Despacho Placa"/>
</div>
<div id="capaCentral" style="position:absolute; width:100%; height:87%; z-index:0; left: 0px; top: 100px; overflow: scroll;">
<%

 System.out.println("BANDERA");
  String accion = "" +request.getParameter("accion");
 
%>
<form name="forma1" action="" method="post">      
    <table border="2" align="center" width="380">
        <tr>
            <td>
                <table width="100%" class="tablaInferior">
                    <tr>
                        <td width="125" class="subtitulo1"><p align="left">Placa</p></td>
                        <td width="410"  class="barratitulo"><img src="<%=BASEURL%>/images/titulo.gif" width="32" height="20"></td>
                    </tr>
                </table>        
                <table width="547" border="0" align="center" class="tablaInferior">
                    <tr class="fila">
                        
            <td width="126">Numero: </td>
                      <td width="405" > 
                            <input name="c_placa" type="text" class="textbox" id="c_placa" size="18" maxlength="15"   onKeyUp="procesar(this);">
                            <img src='<%=BASEURL%>/images/botones/buscar.gif' name='Buscar' align="absmiddle" style='cursor:hand' title='Aplicar configuracion'  onClick="listaPlacas();"  onMouseOver='botonOver(this);' onMouseOut='botonOut(this);'> </td>
                    </tr>
              </table>
            </td>
        </tr>
    </table>

<%


if (accion.equals("1")){
    Vector vPlacas=  model.placaService.getVPlacas ();
	if(vPlacas != null){
%>
<br>  <table width="350" border="2" align="center">
    <tr>
    <td>
	  	<table width="100%" border="1" borderColor="#999999" bgcolor="#F7F5F4">
          <tr class="tblTitulo" align="center"><td>Lista de Placas</td></tr>
		</table>
        <table width="100%" border="1" borderColor="#999999" align="center" >
<%
          for(int i=0; i< vPlacas.size();i++){
            Vector placa=(Vector)vPlacas.elementAt(i);
%>  
  <tr class="<%=(i % 2 == 0 )?"filagris":"filaazul"%>" onMouseOver='cambiarColorMouse(this)' style="cursor:hand">
    <td class="bordereporte" align="center"><a  onClick="accion('<%=placa.elementAt(0)%>')" style="cursor:hand" ><%=placa.elementAt(0)%></a></td>
    </tr>
<%
        }}
    }
%>  
</table>
</td>
</tr>
</table>
<br>
<table width="547" border="0" cellspacing="0" cellpadding="0" align="center">
  <tr>
    <td><img src="<%=BASEURL%>/images/botones/salir.gif"  name="salir" title="Salir..." onClick="window.close();" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);"></td>
  </tr>
</table>-</form>
</div>
</bodY>
</html>