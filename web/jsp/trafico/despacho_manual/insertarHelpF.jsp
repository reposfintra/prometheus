<%@ include file="/WEB-INF/InitModel.jsp"%>

<HTML>
<HEAD>
<TITLE></TITLE>
<META http-equiv=Content-Type content="text/html; charset=windows-1252">
<link href="../css/estilostsp.css" rel="stylesheet" type="text/css"> 
<link href="<%=BASEURL%>/css/estilostsp.css" rel="stylesheet" type="text/css"> 
</HEAD>
<BODY> 
<% String BASEIMG = BASEURL +"/images/ayuda/trafico/despacho_manual/"; %> 

  <table width="95%"  border="2" align="center">
    <tr>
      <td height="" >
        <table width="100%" border="0" align="center">
          <tr  class="subtitulo">
            <td height="20"><div align="center">MANUAL DE DESPACHOS MANUAL WEB </div></td>
          </tr>
          <tr class="subtitulo1">
            <td>Descripci&oacute;n del funcionamiento del programa de Insertar nuevo Despacho Manual               <div align="center"></div>              <div align="center"></div>              <div align="center"></div></td>
          </tr>
<tr>
            <td  class="ayudaHtmlTexto"><span class="ayudaHtmlTexto">En la siguiente pantalla 


 se puede colocar el codigo de la placa si se conoce o utilizar la opci&oacute;n Consultar placas representado por la lupa. </span></td>
          </tr>
<tr>
            <td  class="ayudaHtmlTexto"><div align="center"><img src="<%=BASEIMG%>Dibujo3.JPG"></div></td>
          </tr>
<tr>
            <td  class="ayudaHtmlTexto"><table cellspacing="0" cellpadding="0">
              <tr>
                <td class="ayudaHtmlTexto">Si se ingresa por el link de Consultar Placas, aparece la siguiente pantalla, en donde se coloca el codigo de la placa. Se puede utilizar el s&iacute;mbolo % como comod&iacute;n. Es decir que donde se coloque puede existir cualquier informaci&oacute;n adicional que lo reemplace. Y se presiona BUSCAR </td>
              </tr>
            </table></td>
          </tr>
<tr>
            <td  class="ayudaHtmlTexto"><div align="center"><img src="<%=BASEIMG%>Dibujo4.JPG"></div></td>
          </tr>
<tr>
            <td  class="ayudaHtmlTexto"> En la pantalla aparecen las placas que coincidan con la b&uacute;squeda realizada. A la placa que se requiera, se le presiona CLICK </td>
          </tr>
<tr>
            <td  class="ayudaHtmlTexto"><div align="center"><img src="<%=BASEIMG%>Dibujo5.JPG" ></div></td>
          </tr>
<tr>
            <td  class="ayudaHtmlTexto"><span class="ayudaHtmlTexto">En la siguiente pantalla se presiona el boton del calendario para escojer la fecha de despacho, de la misma manera se realiza para escojer la fecha de salida </span></td>
          </tr>
<tr>
            <td  class="ayudaHtmlTexto"><div align="center"><img src="<%=BASEIMG%>Dibujo6.JPG" ></div></td>
          </tr>
<tr>
            <td  class="ayudaHtmlTexto">En la siguiente pantalla se puede colocar la cedula del conductor si se conoce o utilizar la opci&oacute;n Consultar cedulas de conductores representado por la lupa. </td>
          </tr>
<tr>
            <td  class="ayudaHtmlTexto"><div align="center"><img src="<%=BASEIMG%>Dibujo7.JPG" ></div></td>
          </tr>
<tr>
            <td  class="ayudaHtmlTexto">Si se ingresa por el link de cedulas Conductor, aparece la siguiente pantalla, en donde se coloca la cedula. Se puede utilizar el s&iacute;mbolo % como comod&iacute;n. Es decir que donde se coloque puede existir cualquier informaci&oacute;n adicional que lo reemplace. Y se presiona BUSCAR </td>
          </tr>
<tr>
            <td  class="ayudaHtmlTexto"><div align="center"><img src="<%=BASEIMG%>Dibujo8.JPG" ></div></td>
          </tr>
<tr>
            <td  class="ayudaHtmlTexto">En la pantalla aparecen los nombres y cedulas de los conductores que coincidan con la b&uacute;squeda realizada. Al nombre que se requiera, se le presiona CLICK.</td>
          </tr>
<tr>
            <td  class="ayudaHtmlTexto"><div align="center"><img src="<%=BASEIMG%>Dibujo9.JPG" ></div></td>
          </tr>
<tr>
            <td  class="ayudaHtmlTexto">En la siguiente pantalla se utilizar la opci&oacute;n Consultar rutas  representado por la lupa. </td>
          </tr>
<tr>
            <td  class="ayudaHtmlTexto"><div align="center"><img src="<%=BASEIMG%>Dibujo10.JPG" ></div></td>
          </tr>
<tr>
            <td  class="ayudaHtmlTexto">Si se ingresa por el link de rutas, aparece la siguiente pantalla, en donde se coloca la ruta. Se puede utilizar el s&iacute;mbolo % como comod&iacute;n. Es decir que donde se coloque puede existir cualquier informaci&oacute;n adicional que lo reemplace. Y se presiona BUSCAR </td>
          </tr>
<tr>
            <td  class="ayudaHtmlTexto"><div align="center"><img src="<%=BASEIMG%>Dibujo11.JPG" ></div></td>
          </tr>
<tr>
            <td  class="ayudaHtmlTexto"><div align="center"></div>              
            En la pantalla aparecen las rutas que coincidan con la b&uacute;squeda realizada. A la ruta que se requiera, se le presiona CLICK.</td>
          </tr>
<tr>
            <td  class="ayudaHtmlTexto"><div align="center"><img src="<%=BASEIMG%>Dibujo12.JPG" ></div></td>
          </tr>
<tr>
            <td  class="ayudaHtmlTexto"> En la siguiente pantalla encontramos un campo de seleccion, escojemos un puesto de control . </td>
          </tr>
<tr>
            <td  class="ayudaHtmlTexto"><img src="<%=BASEIMG%>Dibujo13.JPG" ></td>
          </tr>
<tr>
            <td  class="ayudaHtmlTexto">En la siguiente pantalla se puede colocar el cliente del codigo si se conoce o utilizar la opci&oacute;n Consultar clientes representado por la lupa. </td>
          </tr>
<tr>
            <td  class="ayudaHtmlTexto"><img src="<%=BASEIMG%>Dibujo14.JPG" ></td>
          </tr>
<tr>
            <td  class="ayudaHtmlTexto"><div align="left">Si se ingresa por el link de clientes, aparece la siguiente pantalla, en donde se coloca en nombre del cliente. Se puede utilizar el s&iacute;mbolo % como comod&iacute;n. Es decir que donde se coloque puede existir cualquier informaci&oacute;n adicional que lo reemplace. Y se presiona BUSCAR </div></td>
          </tr>
<tr>
  <td  class="ayudaHtmlTexto"><img src="<%=BASEIMG%>Dibujo15.JPG" ></td>
</tr>
<tr>
  <td  class="ayudaHtmlTexto"><div align="left">En la pantalla aparecen los clientes que coincidan con la b&uacute;squeda realizada. A la ruta que se requiera, se le presiona CLICK.</div></td>
</tr>
<tr>
  <td  class="ayudaHtmlTexto"><img src="<%=BASEIMG%>Dibujo16.JPG" ></td>
</tr>
<tr>
  <td  class="ayudaHtmlTexto">En la siguiente pantalla se coloca la descripcion de la carga a transportar </td>
</tr>
<tr>
  <td  class="ayudaHtmlTexto"><img src="<%=BASEIMG%>Dibujo17.JPG" ></td>
</tr>
<tr>
  <td  class="ayudaHtmlTexto">Cualquier campo que no se escoja, provora un mensaje de alerta , avisandole al usuario que debe llenar el campo respectivo </td>
</tr>
<tr>
  <td  class="ayudaHtmlTexto"><div align="center"><img src="<%=BASEIMG%>Dibujo18.JPG" ></div></td>
</tr>
<tr>
  <td  class="ayudaHtmlTexto"><P class=ayudaHtmlTexto>El sistema verifica que toda la informaci&oacute;n ingresada est&eacute; correcta, de lo contrario muestra el mensaje. Se debe corregir y volver a presionar ACEPTAR.</P>    </td>
</tr>
<tr>
  <td  class="ayudaHtmlTexto"><div align="center"><img src="<%=BASEIMG%>Dibujo19.JPG" ></div></td>
</tr>
<tr>
  <td  class="ayudaHtmlTexto">El sistema luego de verificar que todos los datos fueron ingresados correctamente y que es valido los datos, se mostrara el mensaje que la planilla fue ingresada correctamente. </td>
</tr>
<tr>
  <td  class="ayudaHtmlTexto"><div align="center"><img src="<%=BASEIMG%>Dibujo20.JPG" ></div></td>
</tr>
<tr>
  <td  class="ayudaHtmlTexto">Al ingresar en la pantalla de Despachos Manuales vemos que se encuentra un link para buscar despachos Manuales por ruta.</td>
</tr>
<tr>
  <td  class="ayudaHtmlTexto"><div align="center"><img src="<%=BASEIMG%>Dibujo21.JPG" ></div></td>
</tr>
<tr>
  <td  class="ayudaHtmlTexto">Si se ingresa por el link buscar por ruta, aparece la siguiente pantalla, en donde se coloca la ruta. Se puede utilizar el s&iacute;mbolo % como comod&iacute;n. Es decir que donde se coloque puede existir cualquier informaci&oacute;n adicional que lo reemplace. Y se presiona BUSCAR </td>
</tr>
<tr>
  <td  class="ayudaHtmlTexto"><img src="<%=BASEIMG%>Dibujo22.JPG" >    <div align="center"></div></td>
</tr>
<tr>
  <td  class="ayudaHtmlTexto">En la pantalla aparecen las rutas que coincidan con la b&uacute;squeda realizada. A la ruta que se requiera, se le presiona CLICK.</td>
</tr>
<tr>
  <td  class="ayudaHtmlTexto"><img src="<%=BASEIMG%>Dibujo23.JPG" ></td>
</tr>
<tr>
  <td  class="ayudaHtmlTexto">En la siguiente pantalla muestra solos los despachos manuales filtrados por la ruta escjida, de la misma manera se realiza para los links de buscar por placa y por cliente.</td>
</tr>
<tr>
  <td  class="ayudaHtmlTexto"><img src="<%=BASEIMG%>Dibujo24.JPG" ></td>
</tr>
<tr>
  <td  class="ayudaHtmlTexto">En el caso de el link reestablecer, al undirlo reestablece la vista de los despachos manuales sin aplicar ningun filtro, es decir los muestra todos. </td>
</tr>
<tr>
  <td  class="ayudaHtmlTexto"><img src="<%=BASEIMG%>Dibujo25.JPG" ></td>
</tr>
      </table></td>
    </tr>
  </table>
</BODY>
</HTML>
