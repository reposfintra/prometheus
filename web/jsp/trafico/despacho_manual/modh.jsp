<!--
- Autor : Ing. David Lamadrid
- Date  : 5 de Octubre de 2005
- Copyrigth Notice : Fintravalores S.A. S.A
-->
<%--
-@(#)
--Descripcion : Pagina JSP, que se encarga de gestionar 
                            el formulario para la modificacion de datos en la 
                            tabala admin_historicos
--%>

<%-- Declaracion de librerias--%>
<%@page contentType="text/html"%>
<%@page pageEncoding="UTF-8"%>
<%@page errorPage="/error/ErrorPage.jsp"%>
<%@page import="com.tsp.operation.model.beans.*, java.util.*"%>
<%@include file="/WEB-INF/InitModel.jsp" %>
<%@ taglib uri="http://jakarta.apache.org/taglibs/input-0.90" prefix="input" %>
<%
        String tablaO= ""+request.getParameter("id");
        Usuario usuarioLogin  = (Usuario) session.getAttribute("Usuario");
        String us  = usuarioLogin.getLogin();
      	model.adminH.listarHistoricoPorId(tablaO);
        AdminHistorico adminH = model.adminH.getAdmin();
%>

<html>
<head>
<title>Untitled Document</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link href="../css/estilotsp.css" rel="stylesheet" type="text/css"> 
<link href="<%=BASEURL%>/css/estilotsp.css" rel="stylesheet" type="text/css"> 
<script type='text/javascript' src="<%=BASEURL%>/js/Validacion.js"></script> 
<script>

function validar(){
	
	if(forma1.tablaO.value==""  && forma1.rutina.value=="" ){
		alert ("Deve Digitar un Nombre de Tabla ");
		forma1.tablaO.focus();
		return false;
	}
	
	if(forma1.fechaInicio.value==""  ){
		alert ("Deve seleccionar una Fecha ");
		forma1.fechaInicio.focus();
		return false;
	}
	
	if(isNaN (forma1.duracion.value) || forma1.duracion.value=="" ){
		alert ("La duracion deve ser un Numero ");
		forma1.duracion.focus();
		return false;
	}
	
}
</script>
</head>

<body>
<form name="forma1" action="<%=CONTROLLER%>?estado=Historico&accion=Actualizar" method="post" onSubmit="return validar();">      
  <table width="379" border="2" align="center">
  <tr>
    <td width="696">
<table width="364"  border="1" align="center" bgcolor="#FFFFFF" bordercolor="#F7F5F4"> 
  <tr>
    <td width="378" height="24"  class="subtitulo1"><p align="left">Administracion de Historiales </p></td>
    <td width="214"  class="barratitulo"><img src="<%=BASEURL%>/images/cuadrosverde.JPG" width="32" height="20"></td>
  </tr>
</table>
<table width="361" border="1" align="center"   bordercolor="#F7F5F4" bgcolor="#FFFFFF">
  <tr class="fila">
    <td width="144" height="30" >Tabla Origen : </td>
    <td width="201"><input name="tablaO" type="text" class="textbox" value="<%=adminH.getTablaO()%>" id="tablaO" size="20" >      </td>
    </tr>
  <tr class="fila">
    <td width="144" height="30" >Fecha de Inicio : </td>
    <td width="201"><input name="fechaInicio" type="text" class="textbox" id="fechaInicio" size="20" readonly value="<%=adminH.getFechaInicio()%>">      
    <span class="comentario"><a href="javascript:void(0)" onClick="if(self.gfPop)gfPop.fPopCalendar(document.forma1.fechaInicio);return false;" hidefocus><img name="popcal" align="absmiddle" src="<%=BASEURL%>/js/calendartsp/calbtn.gif" width="16" height="16" border="0" alt=""></a></span></span>  </td>
    </tr>
  <tr class="fila">
    <td width="144" height="30" >Duraci&oacute;n:</td>
    <td width="201"><input name="duracion" type="text" class="textbox" id="duracion" value="<%=adminH.getDuracion()%>"size="5" maxlength="3" > 
    valor en dias   </td>
    </tr>
  <tr class="fila">
    <td width="144" height="30" >Rutina</td>
    <td width="201"><!--input name="rutina" type="text" class="textbox" id="rutina" value="<%//=adminH.getRutina()%>" size="20"-->
				<input type="file" name="rutina"  id="rutina">	
	       </td>
    </tr>
	 <tr class="fila">
    <td width="144" height="30" >Estado</td>
    <td width="201">Activo &nbsp;<input type="radio" name="estadoo"  value="A"  checked>  Inactivo &nbsp; <input type="radio" name="estadoo"  value="I"   >     </td>
    </tr>
	<tr class="fila">
    <td colspan="2"  align="center"><input type="submit" value="Aplicar" onClick="return validar();"></td>
    </tr>
</table>
<input type="hidden" name='tablaOA' value='<%=adminH.getTablaO()%>'>
<input type="hidden" name='id' value='<%=adminH.getId()%>'>
<!--input type="hidden" name='tablaD' value='<%//=adminH.getTablaD()%>'-->
</td>
</tr>
</table>

<input type="hidden" name='usuario' value="<%=us%>">
  <p>&nbsp;</p>
</form>
<iframe width=188 height=166 name="gToday:datetime:<%=BASEURL%>/js/calendartsp/agenda.js:gfPop:<%=BASEURL%>/js/calendartsp/plugins_24.js" id="gToday:datetime:<%=BASEURL%>/js/calendartsp/agenda.js:gfPop:<%=BASEURL%>/js/calendartsp/plugins_24.js" src="<%=BASEURL%>/js/calendartsp/ipopeng.htm" scrolling="no" frameborder="0" style="visibility:visible; z-index:999; position:absolute; left:-500px; top:0px;">
</iframe>
</html>

