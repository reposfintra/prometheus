<%@ include file="/WEB-INF/InitModel.jsp"%>

<html>
<head>
<title>Descripcion de campos ingresar despacho</title>


<link href="<%=BASEURL%>/css/estilostsp.css" rel="stylesheet" type="text/css"> 
</head>
<body>
<% String BASEIMG = BASEURL +"/images/botones/iconos/"; %> 
<br>
<table width="696"  border="2" align="center">
  <tr>
    <td width="811" >
      <table width="100%" border="0" align="center">
        <tr  class="subtitulo">
          <td height="24" colspan="2"><div align="center">TRAFICO</div></td>
        </tr>
        <tr class="subtitulo1">
          <td colspan="2"> Nuevo despacho manual </td>
        </tr>
		<tr class="subtitulo1">
          <td colspan="2"> Despacho manual </td>
        </tr>
        <tr>
          <td width="131" class="fila"> Placa</td>
          <td width="543"  class="ayudaHtmlTexto">Campo para ingresar un c&oacute;digo de seis digitos que representa una placa.</td>
        </tr>
        <tr>
          <td  class="fila"> Boton <img src='<%=BASEURL%>/images/botones/iconos/lupa.gif'  width="15" height="15"> (Placa) </td>
          <td  class="ayudaHtmlTexto">Link que muestra una pantalla para la busqueda de placas. </td>
        </tr>
        <tr>
          <td  class="fila">Fecha Despacho</td>
          <td  class="ayudaHtmlTexto"> Campo donde se ingresa la fecha de despacho escojida.</td>
        </tr>
        <tr>
          <td  class="fila">Boton <img src='<%=BASEURL%>/images/botones/iconos/lupa.gif'  width="15" height="15"> (despacho)</td>
          <td  class="ayudaHtmlTexto">Boton que despliega un calendario para poder escojer la fecha de despacho.</td>
        </tr>
        <tr>
          <td class="fila"> Fecha Salida</td>
          <td  class="ayudaHtmlTexto">Campo donde se ingresa la fecha de salida escojida.</td>
        </tr>
        <tr>
          <td class="fila">Boton <img src='<%=BASEURL%>/images/botones/iconos/lupa.gif'  width="15" height="15"> (Salida) </td>
          <td  class="ayudaHtmlTexto">Boton que despliega un calendario para poder escojer la fecha de salida.</td>
        </tr>
		<tr>
          <td class="fila"> Conductor(Cedula)</td>
          <td  class="ayudaHtmlTexto">Campo para ingresar un numero de seis digitos que representa una cedula. </td>
        </tr>
		<tr>
		  <td class="fila">Boton <img src='<%=BASEURL%>/images/botones/iconos/lupa.gif'  width="15" height="15"> (Cedula) </td>
	      <td  class="ayudaHtmlTexto">Link que muestra una pantalla para la busqueda de cedulas de conductor</td>
		</tr>
		<tr>
          <td width="131" class="fila"> Ruta</td>
          <td width="543"  class="ayudaHtmlTexto">Campo donde se ingresa la ruta escojida. </td>
        </tr>
        <tr>
          <td width="131" class="fila"> Boton <img src='<%=BASEURL%>/images/botones/iconos/lupa.gif'  width="15" height="15">(Ruta)</td>
          <td width="543"  class="ayudaHtmlTexto">Link que muestra una pantalla para la busqueda de rutas </td>
        </tr>
        <tr>
          <td class="fila">Puesto de Control</td>
          <td width="543"  class="ayudaHtmlTexto">Campo de seleccion donde se escoje los puestos de control de la ruta escojida en el campo anterior.</td>
        </tr>
        <tr>
          <td class="fila"> Cliente(Codigo)</td>
          <td width="543"  class="ayudaHtmlTexto">Campo para ingresar codigo de un cliente.</td>
        </tr>
        <tr>
          <td class="fila">Boton <img src='<%=BASEURL%>/images/botones/iconos/lupa.gif'  width="15" height="15"> (Cliente) </td>
          <td width="543"  class="ayudaHtmlTexto">Link que muestra una pantalla para la busqueda de clientes. </td>
        </tr>
        <tr>
          <td class="fila"> Carga </td>
          <td width="543"  class="ayudaHtmlTexto">Campo donde se ingresa la descripcion de la carga a transportar. </td>
        </tr>
      </table>
    </td>
  </tr>
</table>
<p>&nbsp;</p>

<p>&nbsp;</p>
<p>&nbsp; </p>
</body>
</html>
