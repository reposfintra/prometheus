<!--
- Autor : Ing. David Lamadrid
- Date  : 5 de Octubre de 2005
- Copyrigth Notice : Fintravalores S.A. S.A
-->
<%--
-@(#)
--Descripcion : Pagina JSP, que se encarga de realizar y mostrar busqueda de los nits de proveedores
--%>

<%-- Declaracion de librerias--%>
<%@page contentType="text/html"%>
<%@page session="true"%>
<%@page import="java.util.*"%>
<%@page import="com.tsp.operation.model.*"%>
<%@page import="com.tsp.util.*"%>
<%@page import="com.tsp.operation.model.beans.*, java.util.*"%>
<%@include file="/WEB-INF/InitModel.jsp"%>
<%@taglib uri="http://jakarta.apache.org/taglibs/input-0.90" prefix="input" %>
<html>
<%System.out.println("BANDERA");%>
<head>
<script type='text/javascript' src="<%= BASEURL %>/js/boton.js"></script>
<script type='text/javascript' src="<%= BASEURL %>/js/reporte.js"></script>
<script language="javascript">
    
	function asignar(id,nombre){
		//alert ("hola");
		var campo = parent.opener.document.forma1.c_cliente;
		campo.value=""+id;
		
		var campo2 = parent.opener.document.getElementById("n_cliente");
		//alert ("hola12"+nombre);
		campo2.value= nombre;
		//alert ("hola2");
		parent.close() ;
	}
	
	

    function procesar (element){
      if (window.event.keyCode==13) 
        listaCliente();
    }
	
	function listaCliente(){
	 	//alert("hola");
		var cliente= document.forma1.c_cliente.value; 
     // alert("hola1");
		document.forma1.action = "<%= CONTROLLER %>?estado=Despachom&accion=Lcliente&sw=1&c_cliente="+cliente; 
        document.forma1.submit(); 
    }
</script>

<title>Buscar Cliente</title>
<link href="<%= BASEURL %>/css/estilostsp.css" rel="stylesheet" type="text/css">
<link href="../../../css/estilostsp.css" rel="stylesheet" type="text/css">
</head>

<body onresize="redimensionar()" onload = 'forma1.c_cliente.focus(); redimensionar()'>
<div id="capaSuperior" style="position:absolute; width:100%; height:100px; z-index:0; left: 0px; top: 0px;">
 <jsp:include page="/toptsp.jsp?encabezado=Buscar Cliente"/>
</div>
<div id="capaCentral" style="position:absolute; width:100%; height:87%; z-index:0; left: 0px; top: 100px; overflow: scroll;">
<%

 System.out.println("BANDERA");
  String accion = "" +request.getParameter("accion");
  //out.println(accion);
 
%>
<form name="forma1" action="" method="post">      
    <table border="2" align="center" width="555">
        <tr>
            <td>
                <table width="100%" class="tablaInferior">
                    <tr>
                        <td width="130" class="subtitulo1"><p align="left">Cliente</p></td>
                        <td width="405"  class="barratitulo"><img src="<%=BASEURL%>/images/titulo.gif" width="32" height="20"></td>
                    </tr>
                </table>        
                <table width="547" border="0" align="center" class="tablaInferior">
                    <tr class="fila">
                        
            <td width="131" height="30"> Nombre del Cliente: </td>
                      <td width="406" > 
                            <input name="c_cliente" type="text" class="textbox" id="c_cliente" size="18" maxlength="15" onKeyPress="procesar(this);">
                            <input type="image" src='<%=BASEURL%>/images/botones/buscar.gif' name='Buscar' align="absmiddle" style='cursor:hand' title='Buscar'  onClick="listaCliente();"  onMouseOver='botonOver(this);' onMouseOut='botonOut(this);'> </td>
                    </tr>
              </table>
            </td>
        </tr>
    </table>

<%


if (accion.equals("1")){
  //  out.println("hola");
    Vector vClientes=   model.clienteService.getClientesVec ();
	if(vClientes!= null){
%><br>
  <table width="555" border="2" align="center">
    <tr>
    <td width="556">
<table width="100%" class="tablaInferior">
          <tr>
    <td width="130" height="24"  class="subtitulo1"><p align="left">Lista de Clientes </p></td>
    <td width="408"  class="barratitulo"><img src="<%=BASEURL%>/images/titulo.gif" width="32" height="20"></td>
  </tr>
</table>
        <table width="551" border="1" borderColor="#999999" bgcolor="#F7F5F4" align="center">
          <tr class="tblTitulo" id="titulos">
            <td width="79" height="22">Codigo</td>
            <td width="456" >nombre</td>
    </tr>
<%
        for(int i=0; i< vClientes.size();i++){
            Vector cliente=(Vector)vClientes.elementAt(i);
%>  
  <tr class="<%=(i % 2 == 0 )?"filagris":"filaazul"%>" onMouseOver='cambiarColorMouse(this)' style="cursor:hand">
    <td width="79" class="bordereporte"><%=cliente.elementAt(0)%></td>
    <td width="456" class="bordereporte"><a  onClick="asignar('<%=cliente.elementAt(0)%>','<%=cliente.elementAt(1)%>')" style="cursor:hand" ><%=cliente.elementAt(1)%></a></td>
    </tr>
<%
        }}
    }
%>  
</table>
</td>
</tr>
</table>
<br>
<table width="555" border="0" cellspacing="0" cellpadding="0" align="center">
  <tr>
    <td><img src="<%=BASEURL%>/images/botones/salir.gif"  name="salir" title="Salir..." onClick="window.close();" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);"></td>
  </tr>
</table>

</form>
</div>
</bodY>
</html>