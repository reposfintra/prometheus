<!--
- Autor : Ing. David Lamadrid
- Date  : 5 de Octubre de 2005
- Copyrigth Notice : Fintravalores S.A. S.A
-->
<%--
-@(#)
--Descripcion : Pagina JSP, que se encarga de realizar y mostrar busqueda de los nits de proveedores
--%>

<%-- Declaracion de librerias--%>
<%@page contentType="text/html"%>
<%@page session="true"%>
<%@page import="java.util.*"%>
<%@page import="com.tsp.operation.model.*"%>
<%@page import="com.tsp.util.*"%>
<%@page import="com.tsp.operation.model.beans.*, java.util.*"%>
<%@include file="/WEB-INF/InitModel.jsp"%>
<%@taglib uri="http://jakarta.apache.org/taglibs/input-0.90" prefix="input" %>
<html>
<%System.out.println("BANDERA");%>
<head>
<script type='text/javascript' src="<%= BASEURL %>/js/boton.js"></script>
<script language="javascript">
    
    function procesar (element){
      if (window.event.keyCode==13) 
        listaCliente();
    }
	
	function listaCliente(){
		var cliente= document.forma1.c_cliente.value; 
		document.forma1.action = "<%= CONTROLLER %>?estado=Despachom&accion=Lcliente&sw=2&c_cliente="+cliente; 
        document.forma1.submit(); 
    }
	
	function accion(id){
		document.forma1.action = "<%= CONTROLLER %>?estado=Despachom&accion=BXCliente&sw=2&cliente="+id; 
        document.forma1.submit(); 
    }
</script>

<title>Despacho Cliente</title>
<link href="<%= BASEURL %>/css/estilostsp.css" rel="stylesheet" type="text/css">
<link href="../../../css/estilostsp.css" rel="stylesheet" type="text/css">
</head>

<body onresize="redimensionar()" onload = 'forma1.c_cliente.focus(); redimensionar()'>
<div id="capaSuperior" style="position:absolute; width:100%; height:100px; z-index:0; left: 0px; top: 0px;">
 <jsp:include page="/toptsp.jsp?encabezado=Despacho Clientes"/>
</div>
<div id="capaCentral" style="position:absolute; width:100%; height:87%; z-index:0; left: 0px; top: 100px; overflow: scroll;">
<%

 System.out.println("BANDERA");
  String accion = "" +request.getParameter("accion");
  //out.println(accion);
 
%>
<form name="forma1" action="" method="post">      
    <table border="2" align="center" width="555">
        <tr>
            <td>
                <table width="100%" class="tablaInferior">
                    <tr>
                        <td width="132"  class="subtitulo1"><p align="left">Cliente</p></td>
                        <td width="403"  class="barratitulo"><img src="<%=BASEURL%>/images/titulo.gif" width="32" height="20"></td>
                    </tr>
                </table>        
                <table width="547" border="0" align="center" class="tablaInferior">
                    <tr class="fila">
                        
            <td width="126"> Nombre del Cliente: </td>
                      <td width="405" > 
                            <input name="c_cliente" type="text" class="textbox" id="c_cliente" size="18" maxlength="15"   onKeyUp="procesar(this);">
                            <img src='<%=BASEURL%>/images/botones/buscar.gif' name='Buscar' align="absmiddle" style='cursor:hand' title='Aplicar configuracion'  onClick="listaCliente();"  onMouseOver='botonOver(this);' onMouseOut='botonOut(this);'> </td>
                    </tr>
              </table>
            </td>
        </tr>
    </table>

<%


if (accion.equals("1")){
  //  out.println("hola");
    Vector vClientes=   model.clienteService.getClientesVec ();
	if(vClientes!= null){
%>
<br>  <table width="555" border="2" align="center">
    <tr>
    <td width="556">
<table width="100%" class="tablaInferior">
          <tr>
    <td width="346" height="24"  class="subtitulo1"><p align="left">Lista de Clientes </p></td>
    <td width="188"  class="barratitulo"><img src="<%=BASEURL%>/images/titulo.gif" width="32" height="20"></td>
  </tr>
</table>
        <table width="551" align="center" border="1" borderColor="#999999" bgcolor="#F7F5F4">
  <tr class="tblTitulo" id="titulos">
            <td width="79" height="22">Codigo</td>
            <td width="456" >nombre</td>
    </tr>

<%
        for(int i=0; i< vClientes.size();i++){
            Vector cliente=(Vector)vClientes.elementAt(i);
%>  
  <tr class="<%=(i % 2 == 0 )?"filagris":"filaazul"%>" onMouseOver='cambiarColorMouse(this)' style="cursor:hand">
    <td width="79" class="bordereporte"><%=cliente.elementAt(0)%></td>
    <td width="456" class="bordereporte"><a  onClick="accion('<%=cliente.elementAt(0)%>')" style="cursor:hand" ><%=cliente.elementAt(1)%></a></td>
    </tr>
<%
        }}
    }
%>  
</table>
</td>
</tr>
</table>
<br>
<table width="555" border="0" cellspacing="0" cellpadding="0" align="center">
  <tr>
    <td><img src="<%=BASEURL%>/images/botones/salir.gif"  name="salir" title="Salir..." onClick="window.close();" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);"></td>
  </tr>
</table></form>
</div>
</bodY>
</html>
