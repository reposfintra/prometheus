
<!--
- Autor : Ing. David Lamadrid
- Date  : 5 de Octubre de 2005
- Copyrigth Notice : Fintravalores S.A. S.A
-->
<%--
-@(#)
--Descripcion : Pagina JSP, que se encarga de realizar y mostrar busqueda de los nits de proveedores
--%>

<%-- Declaracion de librerias--%>
<%@page contentType="text/html"%>
<%@page session="true"%>
<%@page import="java.util.*"%>
<%@page import="com.tsp.operation.model.*"%>
<%@page import="com.tsp.util.*"%>
<%@page import="com.tsp.operation.model.beans.*, java.util.*"%>
<%@include file="/WEB-INF/InitModel.jsp"%>
<%@taglib uri="http://jakarta.apache.org/taglibs/input-0.90" prefix="input" %>
<html>
<%System.out.println("BANDERA");%>
<head>
<script type='text/javascript' src="<%= BASEURL %>/js/boton.js"></script>
<script type='text/javascript' src="<%= BASEURL %>/js/reporte.js"></script>
<script language="javascript">
    
	function asignar(origen,destino,secuencia,corigen,cdestino){
		
		var codigo = "";
		codigo=""+origen+destino+secuencia;
		document.forma1.action = "<%= CONTROLLER %>?estado=Despachom&accion=BXRuta&sw=2&ruta="+codigo; 
        document.forma1.submit(); 

	}
	
    function procesar (element){
	
        if (window.event.keyCode==13) {
		
            listaRuta();
			
		}
		
    }
	
	function listaRuta(){
	 	
		document.forma1.action = "<%= CONTROLLER %>?estado=Despachom&accion=Lruta&sw=2"; 
        document.forma1.submit(); 
		
    }
</script>
<title>Despacho Ruta</title>
<link href="<%= BASEURL %>/css/estilostsp.css" rel="stylesheet" type="text/css">
<link href="../../../css/estilostsp.css" rel="stylesheet" type="text/css">
</head>
<body onresize="redimensionar()" onload = 'forma1.c_ciudad.focus(); redimensionar()'>
<div id="capaSuperior" style="position:absolute; width:100%; height:100px; z-index:0; left: 0px; top: 0px;">
 <jsp:include page="/toptsp.jsp?encabezado=Buscar Despacho Ruta"/>
</div>
<div id="capaCentral" style="position:absolute; width:100%; height:87%; z-index:0; left: 0px; top: 100px; overflow: scroll;">
<%
  String accion = "" +request.getParameter("accion"); 
%>
<form name="forma1" action="" method="post">      
    <table border="2" align="center" width="555">
        <tr>
            <td width="610">
                <table width="100%" class="tablaInferior">
                    <tr>
                        <td width="128" class="subtitulo1">Ruta</td>
                        <td width="407" class="barratitulo"><img src="<%=BASEURL%>/images/titulo.gif" width="32" height="20"></td>
                    </tr>
                </table>        
                <table width="100%" border="0" align="center" class="tablaInferior">
                    <tr class="fila">
	            	  <td width="126">Ciudad Origen: </td>
                      <td width="405" > 
                            <input name="c_ciudad" type="text" class="textbox" id="c_ciudad" size="18" maxlength="15"   onKeyUp="procesar(this);">
                            <img src='<%=BASEURL%>/images/botones/buscar.gif' name='Buscar' align="absmiddle" style='cursor:hand' title='Buscar...'  onClick="listaRuta();"  onMouseOver='botonOver(this);' onMouseOut='botonOut(this);'> </td>
                    </tr>
              </table>
            </td>
        </tr>
    </table>
<%
if (accion.equals("1")){
    Vector vRutas=  model.rutaService.getVRutas ();
	if( vRutas != null && vRutas.size() > 0 ){
%>
  <table width="555" border="2" align="center">
    <tr>
    <td width="556">
		<table width="100%" border="1" borderColor="#999999" bgcolor="#F7F5F4">
          <tr class="tblTitulo">
    		<td>Lista de Rutas</td>
    		</tr>
		</table>
        <table width="100%" border="1" borderColor="#999999" align="center" > 
<%
        for(int i=0; i< vRutas.size();i++){
            Vector ruta=(Vector)vRutas.elementAt(i);
%>  
  <tr class="<%=(i % 2 == 0 )?"filagris":"filaazul"%>" onMouseOver='cambiarColorMouse(this)' style="cursor:hand">
    <td width="541" class="bordereporte"><a  onClick="asignar('<%=ruta.elementAt(0)%>','<%=ruta.elementAt(1)%>','<%=ruta.elementAt(2)%>','<%=ruta.elementAt(3)%>','<%=ruta.elementAt(4)%>')" style="cursor:hand" ><%=ruta.elementAt(5)%></a></td>
    </tr>
<%
        }
	}
		
}
%>  
</table>
</td>
</tr>
</table>
<br>
<table width="555" border="0" cellspacing="0" cellpadding="0" align="center">
  <tr>
    <td><img src="<%=BASEURL%>/images/botones/salir.gif"  name="salir" title="Salir..." onClick="window.close();" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);"></td>
  </tr>
</table></form>
</div>
</bodY>
</html>