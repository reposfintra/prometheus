<!--
- Autor : Ing. David Lamadrid
- Date  : 5 de Octubre de 2005
- Copyrigth Notice : Fintravalores S.A. S.A
-->
<%--
-@(#)
--Descripcion : Pagina JSP, que se encarga de realizar el reporte de despacho_manual de tablas del sistema
--%>

<%-- Declaracion de librerias--%>
<%@page contentType="text/html"%>
<%@page session="true"%>
<%@page errorPage="../error/ErrorPage.jsp"%>
<%@page import="java.util.*"%>
<%@page import="java.sql.*"%>
<%@page import="com.tsp.operation.model.*"%>
<%@page import="com.tsp.util.*"%>
<%@page import="com.tsp.operation.model.beans.*, java.util.*"%>
<%@include file="/WEB-INF/InitModel.jsp"%>
<%String path = application.getRealPath(model.menuService.getRealUrl(request.getRequestURI()));
  String datos[] = model.menuService.getContenidoMenu(BASEURL,request.getRequestURI(),path);
%>
<html>
<head>
<link href="<%= BASEURL %>/css/estilostsp.css" rel="stylesheet" type="text/css">
<link href="../../../css/estilostsp.css" rel="stylesheet" type="text/css">

<title>Planillas de Despacho Manual</title>
    
<script>
    function abrirVentanaBusq(an,al,url,pag) {
        window.open(url,'Trafico1','width='+an+',height='+al+',scrollbars=no,resizable=yes,top=10,left=65,status=yes');
    }
	
	function modificar(d,p,f, CONTROLLER,planilla){
		//alert(d+p+f);
		var url= CONTROLLER+"?estado=Despachom&accion=Modificar&sw=2&d="+d+"&p="+p+"&f="+f+"&planilla="+planilla;
		abrirVentanaBusq(850,550,url);
		//window.open (url,'height=400,width=800,dependent=yes,resizable=yes,scrollbars=yes,status=yes'); 
	}
	
	function eliminar(d,p,f,np,CONTROLLER){
		//alert(d+p+f);
		var url= CONTROLLER+"?estado=Despachom&accion=Eliminar&sw=2&d="+d+"&p="+p+"&f="+f+"&np="+np;
		location.href=url; 
	}
</script>
<script type='text/javascript' src="<%= BASEURL %>/js/boton.js"></script>
<script type='text/javascript' src="<%= BASEURL %>/js/reporte.js"></script>

</head>

<body onResize="redimensionar()" <%if(request.getParameter("reload")!=null){%>
        onLoad = "redimensionar();parent.close();parent.opener.location.href='<%=BASEURL%>/jsp/trafico/despacho_manual/despacho.jsp?';" 
  <%} else {%>
       onLoad = "redimensionar();";
  <%}%>
>
<div id="capaSuperior" style="position:absolute; width:100%; height:100px; z-index:0; left: 0px; top: 0px;">
 <jsp:include page="/toptsp.jsp?encabezado=Despachos Manuales"/>
</div>
<div id="capaCentral" style="position:absolute; width:100%; height:87%; z-index:0; left: 0px; top: 100px; overflow: scroll;">
<p>
<%  

    Vector vDespacho = model.despachoManualService.getVDespacho();
	
	//System.out.println("vDespacho:"+vDespacho.size());
	if(vDespacho == null || vDespacho.size() == 0){
	
		model.despachoManualService.listaDespacho();
		
		vDespacho = model.despachoManualService.getVDespacho();
		
	}
	
	DespachoManual despacho;
    String para = "/adminHistorico/insertar.jsp";
    para = Util.convertidorParametros(para);
%>


<input type="hidden" name ="linea" value="<%//=linea%>">
<input type="hidden" name ="conf" value="<%//=conf%>">

<table width="100%" border="0" bgcolor="#4D71B0">
  <tr > 
      <td>
	    <img src='<%=BASEURL%>/images/cuadrito.gif' width='9' height='9'>&nbsp;&nbsp;<a href="javascript:abrirVentanaBusq(850,550,'<%=BASEURL%>/jsp/trafico/despacho_manual/insertar.jsp?sw=1')" class="encabezado">Insertar</a>
		<img src='<%=BASEURL%>/images/cuadrito.gif' width='9' height='9'>&nbsp;&nbsp;<a href="javascript:abrirVentanaBusq(850,550,'<%=BASEURL%>/jsp/trafico/despacho_manual/dxruta.jsp')" class="encabezado">Buscar por Ruta</a>
		<img src='<%=BASEURL%>/images/cuadrito.gif' width='9' height='9'>&nbsp;&nbsp;<a href="javascript:abrirVentanaBusq(850,550,'<%=BASEURL%>/jsp/trafico/despacho_manual/dxplaca.jsp')" class="encabezado">Buscar por Placa</a>
		<img src='<%=BASEURL%>/images/cuadrito.gif' width='9' height='9'>&nbsp;&nbsp;<a href="javascript:abrirVentanaBusq(850,550,'<%=BASEURL%>/jsp/trafico/despacho_manual/dxcliente.jsp')" class="encabezado">Buscar por Cliente</a> 		
		<img src='<%=BASEURL%>/images/cuadrito.gif' width='9' height='9'>&nbsp;&nbsp;<a href="<%=CONTROLLER%>?estado=Despachom&accion=Reset" class="encabezado">Restablecer</a>
	  </td>
  </tr>
</table>


</p>
</form>
<table width="1041" border="2" align="center" id="tabla1" >
    <tr>
        <td width="" >
           <table width="100%"  align="center">
              <tr>
                <td class="subtitulo1">Despachos Manuales </td>
                <td width="54%" class="barratitulo"><img src="<%=BASEURL%>/images/titulo.gif" width="32" height="20" align="left"><%=datos[0]%></td>
              </tr>
          </table>
        
         <table id="tabla3" width="100%" border="1" borderColor="#999999" bgcolor="#F7F5F4">
                <tr class="tblTitulo" id="titulos">
                    <td width="36" nowrap >&nbsp;&nbsp;&nbsp;</td>
                    <td width="70" nowrap align="center">Despacho</td>
                    <td width="58" nowrap align="center">Placa</td>
                    <td width="127" nowrap align="center"> Fecha Despacho</td>
                    <td width="103" nowrap> Conductor </td>
                    <td width="274" align="center" nowrap>Ruta</td>
                    <td width="91" nowrap align="center"> Cod Cliente </td>
					<td width="76" nowrap> Cliente </td>
                    <td width="62" nowrap align="center"> Carga</td>
                    <td width="68" nowrap align="center">Eliminar</td>
                </tr>
<%     
            for (int i =0 ;i < vDespacho.size();i++){
                despacho =(DespachoManual)vDespacho.elementAt(i);
                //despacho.
                String para1 = "/adminHistorico/modh.jsp?id=";
%>     
            <tr class="<%=(i % 2 == 0 )?"filagris":"filaazul"%>" onMouseOver='cambiarColorMouse(this)'>
            <td  nowrap  align="center" abbr="" class="bordereporte" style="cursor:hand"> <img src='<%=BASEURL%>/images/botones/iconos/modificar.gif' name='Buscar' align="absmiddle" style='cursor:hand' title='Aplicar configuracion'  onclick="modificar('<%= despacho.getDstrct ()%>','<%=despacho.getPlaca ()%>','<%=despacho.getFecha_despacho ()%>','<%=CONTROLLER%>','<%=despacho.getNPlanilla()%>');"></td>
			<%
			String planilla = ""+despacho.getNPlanilla();
			%>
			<td  align="center" nowrap class="bordereporte" abbr=""><%=planilla%></td>
            <td  align="center" nowrap class="bordereporte" abbr=""><%=despacho.getPlaca()%></td>
            <td  align="center" abbr="" nowrap class="bordereporte"><%=despacho.getFecha_despacho ()%></td>
			<%
			String nomcon = "";
			if ( despacho.getNombreConductor () == null || despacho.getNombreConductor ().equals("") )
				nomcon = model.despachoManualService.buscarNombreConductor( planilla );
			else
				nomcon = despacho.getNombreConductor ();
			%>
            <td  abbr="" nowrap class="bordereporte"><%=nomcon%></td>
            <td  align="center" abbr="" nowrap class="bordereporte"><%=despacho.getDescripcionRuta ()%></td>
            <td  abbr="" nowrap class="bordereporte" align="center"><%=despacho.getCliente ()%></td>
			<%
			String nomcli = "";
			if ( despacho.getNombreCliente () == null || despacho.getNombreCliente ().equals("") )
				nomcli = model.despachoManualService.buscarNombreCliente( planilla );
			else
				nomcli = despacho.getNombreCliente ();
			%>
			<td  abbr="" nowrap class="bordereporte"><%=nomcli%></td>
            <td  align="center" abbr="" nowrap class="bordereporte"><%=despacho.getCarga ()%></td>
			<td class="bordereporte" align="center" style="cursor:hand"><img src='<%=BASEURL%>/images/delete.gif' name='Buscar' align="absmiddle" style='cursor:hand' title='Aplicar configuracion'  onclick="eliminar('<%= despacho.getDstrct ()%>','<%=despacho.getPlaca ()%>','<%=despacho.getFecha_despacho ()%>','<%=despacho.getNPlanilla ()%>','<%=CONTROLLER%>');" ></td>
           </tr>
        <%}%>           
          </table> 
        </td>
    </tr>
</table>
<br>
<img src="<%=BASEURL%>/images/botones/salir.gif"  name="salir" title="Salir..." onClick="window.close();" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);">
</div>
<%=datos[1]%>
</body>
</html>