<%@ include file="/WEB-INF/InitModel.jsp"%>

<html>
<head>
<title>Descripcion de campos ingresar despacho</title>


<link href="<%=BASEURL%>/css/estilostsp.css" rel="stylesheet" type="text/css"> 
</head>

<body>
<% String BASEIMG = BASEURL +"/images/botones/"; %> 
<br>
<table width="696"  border="2" align="center">
  <tr>
    <td width="811" >
      <table width="100%" border="0" align="center">
        <tr  class="subtitulo">
          <td height="24" colspan="2"><div align="center">TRAFICO</div></td>
        </tr>
        <tr class="subtitulo1">
          <td colspan="2">Despachos manuales </td>
        </tr>
        <tr>
          <td width="118" class="fila"> Link Insertar </td>
          <td width="556"  class="ayudaHtmlTexto">Campo para ingresar un c&oacute;digo de seis digitos que representa una placa.</td>
        </tr>
        <tr>
          <td  class="fila"> Link Buscar por Ruta </td>
          <td  class="ayudaHtmlTexto">Link que muestra una pantalla para la busqueda de despachos manuales dependiendo de una ruta, de esta manera aplica el filtro correspondiente. </td>
        </tr>
        <tr>
          <td  class="fila">Link Buscar por Placa </td>
          <td  class="ayudaHtmlTexto"> Link que muestra una pantalla para la busqueda de despachos manuales dependiendo de una placa, de esta manera aplica el filtro correspondiente. </td>
        </tr>
        <tr>
          <td  class="fila">Link Buscar por Cliente </td>
          <td  class="ayudaHtmlTexto">Link que muestra una pantalla para la busqueda de despachos manuales dependiendo de un cliente, de esta manera aplica el filtro correspondiente. </td>
        </tr>
        <tr>
          <td class="fila"> Restablecer </td>
          <td  class="ayudaHtmlTexto">Link para reestablecer la vista, de esta manera quitando cualquier filtro aplicado. </td>
        </tr>
        <tr>
          <td width="118" class="fila">Boton Salir <img src="<%=BASEIMG%>salir.gif"></td>
          <td width="556"  class="ayudaHtmlTexto">Boton para salir de la vista , regresando a control trafico.</td>
        </tr>
      </table>
    </td>
  </tr>
</table>
<p>&nbsp;</p>

<p>&nbsp;</p>
<p>&nbsp; </p>
</body>
</html>
