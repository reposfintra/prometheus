
<!--
- Autor : Ing. David Lamadrid
- Date  : 5 de Octubre de 2005
- Copyrigth Notice : Fintravalores S.A. S.A
-->
<%--
-@(#)
--Descripcion : Pagina JSP, que se encarga de realizar y mostrar busqueda de los nits de proveedores
--%>

<%-- Declaracion de librerias--%>
<%@page contentType="text/html"%>
<%@page session="true"%>
<%@page import="java.util.*"%>
<%@page import="com.tsp.operation.model.*"%>
<%@page import="com.tsp.util.*"%>
<%@page import="com.tsp.operation.model.beans.*, java.util.*"%>
<%@include file="/WEB-INF/InitModel.jsp"%>
<%@taglib uri="http://jakarta.apache.org/taglibs/input-0.90" prefix="input" %>
<html>
<%System.out.println("BANDERA");%>
<head>
<script type='text/javascript' src="<%= BASEURL %>/js/boton.js"></script>
<script type='text/javascript' src="<%= BASEURL %>/js/reporte.js"></script>
<script language="javascript">
    
	function asignar(id,conduc){
		//alert ("hola");
		var campo = parent.opener.document.forma1.c_placa;
		var condu = parent.opener.document.forma1.c_conductor
		campo.value=""+id;
		condu.value=""+conduc;
		//alert ("hola2");
		parent.close() ;
	}
	
	

    function procesar (element){
      if (window.event.keyCode==13) 
        listaPlacas();
    }
	
	function listaPlacas(){
	 	//alert("hola");
		var id= document.forma1.c_placa.value; 
      // alert("hola1");
		document.forma1.action = "<%= CONTROLLER %>?estado=Despachom&accion=Lplaca&sw=1&conductor="+id; 
        document.forma1.submit(); 
    }
</script>

<title>Buscar Placa</title>
<link href="<%= BASEURL %>/css/estilostsp.css" rel="stylesheet" type="text/css">
<link href="../../../css/estilostsp.css" rel="stylesheet" type="text/css">
</head>

<body onresize="redimensionar()" onload = 'forma1.c_placa.focus(); redimensionar()'>
<div id="capaSuperior" style="position:absolute; width:100%; height:100px; z-index:0; left: 0px; top: 0px;">
 <jsp:include page="/toptsp.jsp?encabezado=Despachos Manuales"/>
</div>
<div id="capaCentral" style="position:absolute; width:100%; height:87%; z-index:0; left: 0px; top: 100px; overflow: scroll;">
<%

 System.out.println("BANDERA");
  String accion = "" +request.getParameter("accion");
  //out.println(accion);
 
%>
<form name="forma1" action="" method="post">      
    <table border="2" align="center" width="555">
        <tr>
            <td width="100%">
			<table width="100%">
  <tr>
    <td>
	<table width="100%" border="0" cellpadding="0" cellspacing="0">
                    <tr>
                        <td width="126" class="subtitulo1">Placa</td>
                        <td width="409"  class="barratitulo"><img src="<%=BASEURL%>/images/titulo.gif" width="32" height="20"></td>
                    </tr>
                </table>
	</td>
  </tr>
  <tr>
    <td>
	 <table width="100%" border="0" align="center" cellpadding="0" cellspacing="0" class="tablaInferior">
                    <tr class="fila">                        
			            <td width="126">Numero: </td>
                      	<td width="405" > 
                            <input name="c_placa" type="text" class="textbox" id="c_placa" size="18" maxlength="15" onKeyPress="procesar(this);">
                            <input type="image" src='<%=BASEURL%>/images/botones/buscar.gif' name='Buscar' align="absmiddle" style='cursor:hand' title='Buscar...'  onClick="listaPlacas();"  onMouseOver='botonOver(this);' onMouseOut='botonOut(this);'> </td>
                    </tr>
              </table>
	</td>
  </tr>
</table>

                        
               
            </td>
        </tr>
    </table>

<%


if (accion.equals("1")){
    Vector vPlacas=  model.placaService.getVPlacas ();
	if(vPlacas != null){
%><br>
  <table width="350" border="2" align="center">
    <tr>
    <td>
	<table width="100%">
  <tr>
    <td>
	<table width="100%" border="0" cellpadding="0" cellspacing="0" class="barratitulo">
          <tr>
    	<td width="207" class="subtitulo1">Lista de Placas</td>
    	<td width="324"><img src="<%=BASEURL%>/images/titulo.gif" width="32" height="20"></td>
  	</tr>
	</table>
	</td>
  </tr>
  <tr>
    <td>
	<table width="100%" border="1" borderColor="#999999" bgcolor="#F7F5F4" align="center">
          <tr  align="center" class="tblTitulo">
            <td width="538">Numero de Placa</td>
            </tr>
<%
        for(int i=0; i< vPlacas.size();i++){
            Vector placa=(Vector)vPlacas.elementAt(i);
%>  
  <tr class="<%=(i % 2 == 0 )?"filagris":"filaazul"%>" onMouseOver='cambiarColorMouse(this)' style="cursor:hand">
    <td width="538" align="center" class="bordereporte"><a onClick="asignar('<%=placa.elementAt(0)%>','<%=placa.elementAt(1)%>')" style="cursor:hand" ><%=placa.elementAt(0)%></a></td>
    </tr>
<%
        }}
    }
%>  
</table>
	</td>
  </tr>
</table>

	
        
</td>
</tr>
</table>
<br>
<table width="555" border="0" cellspacing="0" cellpadding="0" align="center">
  <tr>
    <td><img src="<%=BASEURL%>/images/botones/salir.gif"  name="salir" title="Salir..." onClick="window.close();" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);"></td>
  </tr>
</table></form>
</div>
</bodY>
</html>