
<!--
- Autor : Ing. David Lamadrid
- Date  : 5 de Octubre de 2005
- Copyrigth Notice : Fintravalores S.A. S.A
-->
<%--
-@(#)
--Descripcion : Pagina JSP, que se encarga de realizar y mostrar busqueda de los nits de proveedores
--%>

<%-- Declaracion de librerias--%>
<%@page contentType="text/html"%>
<%@page import="com.tsp.operation.model.*"%>
<%@page import="com.tsp.util.*"%>
<%@page import="com.tsp.operation.model.beans.*, java.util.*"%>
<%@include file="/WEB-INF/InitModel.jsp"%>
<%@taglib uri="http://jakarta.apache.org/taglibs/input-0.90" prefix="input" %>
<html>
<%
String cerrar = (request.getParameter("cerrar")!=null)?request.getParameter("cerrar"):"";
%>
<head>
<script type='text/javascript' src="<%= BASEURL %>/js/boton.js"></script>
<script type='text/javascript' src="<%= BASEURL %>/js/reporte.js"></script>
<script language="javascript">
    
	function asignar(origen,destino,secuencia,descripcion){
	
		var campo = parent.opener.document.forma1.c_ruta;
		campo.value=""+descripcion;
		var campo1 = parent.opener.document.forma1.c_cod_ruta;
		campo1.value=""+origen+destino+secuencia;
		buscarpuestos(campo1);
		
	}
	
	function buscarpuestos(campo1){
	
        var url = "controller?estado=Despachom&accion=Lpuestos&ruta="+ campo1.value; 
	    window.location.href = url;
		
	}
	
    function procesar (element){
	
        if (window.event.keyCode==13){ 
		
		    listaRuta();
	   
	    }
	  
    }
	
	function listaRuta(){
	
		document.forma1.action = "<%= CONTROLLER %>?estado=Despachom&accion=Lruta&sw=1"; 
        document.forma1.submit(); 
		
    }
	
	function cargarCombo (lista){
	
   	   var comb = parent.opener.document.forma1.ubicacion;
	   comb.length = lista.length;
	   for (var i=0; i<lista.length; i++ ){ 
	      var datos = lista[i]; 
		  comb[i].value =  datos[0];
		  comb[i].text  =  datos[1];
	   }
	   window.close();		 
	    	   
	}
	
	<% if (cerrar.equals("ok")) {
	     out.print("\n var lista = [");
	     Vector ubicaciones = model.despachoManualService.listaPuestos();
		  for(int i=0;i<ubicaciones.size();i++){
		     Vector tmp = (Vector)(ubicaciones.get(i));
			 out.print("\n\t ['"+ (String)tmp.get(2) +"','"+ (String)(tmp.get(1)) +"']" +
			            (i<ubicaciones.size()-1?",":""));
		  }
	     out.print("\n ];");		  
		 out.print("\n\t cargarCombo (lista); ");
	  }	%>		
</script>
<title>Buscar Ruta</title>
<link href="<%= BASEURL %>/css/estilostsp.css" rel="stylesheet" type="text/css">
<link href="../../../css/estilostsp.css" rel="stylesheet" type="text/css">
</head>
<body onresize="redimensionar()" onload = 'forma1.c_ciudad.focus(); redimensionar()'>
<div id="capaSuperior" style="position:absolute; width:100%; height:100px; z-index:0; left: 0px; top: 0px;">
 <jsp:include page="/toptsp.jsp?encabezado=Buscar Ruta"/>
</div>
<div id="capaCentral" style="position:absolute; width:100%; height:87%; z-index:0; left: 0px; top: 100px; overflow: scroll;">
<%
  String accion = (request.getParameter("accion")!=null)?request.getParameter("accion"):"";
%>
<form name="forma1"  method="post" action="<%= CONTROLLER %>?estado=Despachom&accion=Lruta&sw=1">      
    <table border="2" align="center" width="555">
        <tr>
            <td>
                <table width="100%" class="tablaInferior">
                    <tr>
                        <td width="130" height="24"  class="subtitulo1"><p align="left">Ruta</p></td>
                        <td width="405"  class="barratitulo"><img src="<%=BASEURL%>/images/titulo.gif" width="32" height="20"></td>
                    </tr>
                </table>        
                <table width="547" border="0" align="center" class="tablaInferior">
                    <tr class="fila">
                        
            <td width="131">Ciudad Origen : </td>
                      <td width="406" > 
                            <input name="c_ciudad" type="text" class="textbox" id="c_ciudad" size="18" maxlength="15">
                            <img src='<%=BASEURL%>/images/botones/buscar.gif' name='Buscar' align="absmiddle" style='cursor:hand' title='Buscar'  onClick="forma1.submit();"  onMouseOver='botonOver(this);' onMouseOut='botonOut(this);'> </td>
                    </tr>
              </table>
            </td>
        </tr>
    </table>
<%
if (accion.equals("1")){
    Vector vRutas=  model.rutaService.getVRutas ();
	if( vRutas != null && vRutas.size() > 0 ){
%>
<br>  <table width="555" border="2" align="center">
    <tr>
    <td width="556">
		<table width="100%" border="1" borderColor="#999999" bgcolor="#F7F5F4">
          <tr class="tblTitulo">
    		<td>Lista de Rutas</td>    
  		  </tr>
		</table>
        <table width="100%" border="1" borderColor="#999999" align="center" >
<%
        for(int i=0; i< vRutas.size();i++){
            Vector ruta=(Vector)vRutas.elementAt(i);
%>  
  <tr class="<%=(i % 2 == 0 )?"filagris":"filaazul"%>" onMouseOver='cambiarColorMouse(this)' >
    <td width="541" height="20" class="bordereporte" > 
		<a onClick="asignar('<%=ruta.elementAt(0)%>','<%=ruta.elementAt(1)%>','<%=ruta.elementAt(2)%>','<%=ruta.elementAt(5)%>')" style="cursor:hand" ><%=ruta.elementAt(5)%></a>
	</td>
  </tr>
<%
        }
	}		
}
%>  
</table>
</td>
</tr>
</table>
<br>
<!-- <input name="oculto"  type="submit" value="5" style="visibility:hidden" > -->
</form>
<br>
<table width="555" border="0" cellspacing="0" cellpadding="0" align="center">
  <tr>
    <td><img src="<%=BASEURL%>/images/botones/salir.gif"  name="salir" title="Salir..." onClick="window.close();" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);"></td>
  </tr>
</table>
</div>
</bodY>
</html>