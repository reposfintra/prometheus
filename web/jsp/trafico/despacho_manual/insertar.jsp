<!--
- Autor : Ing. David Lamadrid
- Date  : 5 de Octubre de 2005
- Copyrigth Notice : Fintravalores S.A. S.A
-->
<%--
-@(#)
--Descripcion : Pagina JSP, que se encarga de gestionar 
                            el formulario para la insercion de datos en la 
                            tabala admin_historicos
--%>

<%-- Declaracion de librerias--%>
<%@page contentType="text/html"%>
<%@page errorPage="/error/ErrorPage.jsp"%>
<%@page import="com.tsp.operation.model.beans.*, java.util.*"%>
<%@include file="/WEB-INF/InitModel.jsp" %>
<%@ taglib uri="http://jakarta.apache.org/taglibs/input-0.90" prefix="input" %>
<%
    Usuario usuarioLogin  = (Usuario) session.getAttribute("Usuario");
	
    String us  = usuarioLogin.getLogin();
	String path = application.getRealPath(model.menuService.getRealUrl(request.getRequestURI()));
    String datos[] = model.menuService.getContenidoMenu(BASEURL,request.getRequestURI(),path);

%>

<html>
<head>
<title>Nuevo Despacho Manual</title>
<link href="../css/estilostsp.css" rel="stylesheet" type="text/css">
<link href="<%=BASEURL%>/css/estilostsp.css" rel="stylesheet" type="text/css"> 

<script type='text/javascript' src="<%=BASEURL%>/js/Validacion.js"></script> 
<script type='text/javascript' src="<%=BASEURL%>/js/general.js"></script> 
<script type='text/javascript' src="<%= BASEURL %>/js/boton.js"></script>
<script>

   function abrirVentanaBusq(an,al,url,pag) {
        parent.open(url,'Conductor','width='+an+',height='+al+',scrollbars=no,resizable=no,top=10,left=65,status=yes');
    }

function validaFechas () {

	 var fecha1 = document.forma1.c_fecha.value.replace( /-|:| /gi, '' );
	 var fecha2 = document.forma1.c_fecha2.value.replace( /-|:| /gi, '' );
	 
	 var fech1 = parseFloat( fecha1 );
	 var fech2 = parseFloat( fecha2 );
	 
	 if( fecha2 < fecha1 ) {   
	   
		 alert( 'La fecha de salida debe ser mayor que la fecha de despacho!' );
	     return ( false );
		 
	 } else {
	 
	     return ( true );
		 
	 }
	 
}

function validar(i,j){

    
	var nultimopto;
	var nproxipto;
	var justifi=forma1.justificacion.options[j].text;
	if(i!=-1){
		nultimopto=forma1.ubicacion.options[i].text;
		
	    if((i+1)<forma1.ubicacion.length){
			
			nproxipto=forma1.ubicacion.options[i+1].text;
			
		}	
		else{
			
		    nproxipto=forma1.ubicacion.options[i].text;		
		}		
	}
	
	if ( !validaFechas() ){
		return false;
	}
	
	if(forma1.c_placa.value==""   ){
		alert ("Digite una Placa");
		forma1.c_placa.focus();
		return false;
	}
	else if(forma1.c_fecha.value==""  ){
		alert ("Debe seleccionar la Fecha de Despacho");
		forma1.c_fecha.focus();
		return false;
	}
	else if(forma1.c_fecha2.value==""  ){
		alert ("Debe seleccionar la Fecha de Salida");
		forma1.c_fecha2.focus();
		return false;
	}
		/*
	else if(forma1.c_conductor.value==""  ){
		alert ("Debe seleccionar un conductor ");
		forma1.c_conductor.focus();
		return false;
	}*/
	else if(forma1.c_ruta.value==""  ){
		alert ("Debe seleccionar una ruta ");
		forma1.c_ruta.focus();
		return false;
	}/*
	else if(forma1.c_cliente.value==""  ){
		alert ("Debe seleccionar un cliente ");
		forma1.c_cliente.focus();
		return false;
	}*/
	else if(forma1.c_carga.value==""  ){
		alert ("Digite el tipo de carga");
		forma1.c_carga.focus();
		return false;
	}
	else if(forma1.c_observacion.value==""  ){
		alert ("Digite una observacion");
		forma1.c_carga.focus();
		return false;
	}
	else {
		insertar(nultimopto,nproxipto,justifi);
	}
}
function salir(){
		document.forma1.action = "<%= CONTROLLER %>?estado=Despachom&accion=Salir"; 
        document.forma1.submit(); 

}
function cerrar(){
	parent.close();
}
function insertar(nultimopto,nproxipto,justifi){
     
		document.forma1.action = "<%= CONTROLLER %>?estado=Despachom&accion=Insertar&nultimopto="+nultimopto+"&nproxipto="+nproxipto+"&justifi="+justifi; 
        document.forma1.submit(); 
}
function infoPlaca(){
		if(document.forma1.c_placa.value!="")
		    window.open("<%= CONTROLLER %>?estado=PlacaTipo&accion=Search&idplaca="+document.forma1.c_placa.value,'NONE','status=yes,resizable=yes,width=700,height=590,statusbar=no');
        else
		    alert("Favor digitar placa");
}
function infoConductor(){
        if(document.forma1.c_conductor.value!="")
		    window.open("<%= CONTROLLER %>?estado=Conductor&accion=Evento&evento=SEARCH&identificacion="+document.forma1.c_conductor.value,'NONE','status=yes,resizable=yes,width=700,height=590,statusbar=no');
        else
		   alert("Favor Digitar cedula conductor")
}
function CedulaXplaca(url){
	 	if (window.event.keyCode==13){ 
			var a = "<iframe name='ejecutor'  style='visibility:hidden' src='" + url + "'> ";
             aa.innerHTML = a;
			
		}
}

function Buscar_Info ( numpla ) {
	
	if( numpla != "" ) {
		
		document.forma1.action = "<%= CONTROLLER %>?estado=PlanillaManual&accion=PlanViaje&numpla=" + numpla + "&ruta=" + document.forma1.codruta.value; 
		document.forma1.submit();
		
	} else {
	
		alert( 'Por favor ingrese un numero de planilla!' );
		
	}
	
}

function Buscar_Info_Facil () {
	if ( window.event.keyCode == 13 ){ 
	
		if( document.forma1.numpla.value != "" ) {
		
			document.forma1.action = "<%= CONTROLLER %>?estado=PlanillaManual&accion=PlanViaje&numpla=" + document.forma1.numpla.value + "&ruta=" + document.forma1.codruta.value;
			
			document.forma1.submit();
			
		} else {
		
			alert( 'Por favor ingrese un numero de planilla!' );
			
		}
		
	}
}


		
</script>
<link href="/css/estilostsp.css" rel="stylesheet" type="text/css">
</head>

<body onresize="redimensionar()" onload = 'redimensionar()'>
<div id="capaSuperior" style="position:absolute; width:100%; height:100px; z-index:0; left: 0px; top: 0px;">
 <jsp:include page="/toptsp.jsp?encabezado=Nuevo Despacho Manual"/>
</div>
<div id="capaCentral" style="position:absolute; width:100%; height:87%; z-index:0; left: 2px; top: 100px; overflow: scroll;">
<%

     DespachoManual despacho = model.despachoManualService.getDespacho();
     String placa ="";
     String fecha="";
	 String fecha2="";
     String conductor="";
     String cliente="";
     String carga="";
     String ruta="";
	 String zona="";
	 String rutacod="";
	 String observacion="";
	 String justificacion="";
	 String sw=""+request.getParameter("sw");
	 if(!sw.equals("1")){
		 if( despacho!=null){
			placa= despacho.getPlaca();
			fecha=despacho.getFecha_despacho();
			fecha2=despacho.getFecha_salida();
			conductor=despacho.getConductor();
			cliente=despacho.getCliente();
			carga=despacho.getCarga();
			ruta=despacho.getDescripcionRuta();
			rutacod=despacho.getRuta();
			zona=despacho.getZona();
			observacion=despacho.getObservacion();
			justificacion=despacho.getJustificacion();
		 }
	}	     

	placa = (String)session.getAttribute("placa") != null?(String)session.getAttribute("placa"):"";
	fecha = (String)session.getAttribute("fecha_despacho") != null?(String)session.getAttribute("fecha_despacho"):"";
	fecha2 = (String)session.getAttribute("fecha_salida") != null?(String)session.getAttribute("fecha_salida"):"";
	conductor = (String)session.getAttribute("cedcon") != null?(String)session.getAttribute("cedcon"):"";
	String nomcon = (String)session.getAttribute("nomcon") != null?(String)session.getAttribute("nomcon"):"";	
	String codruta = (String)session.getAttribute("codruta") != null?(String)session.getAttribute("codruta"):"";	
	rutacod = (String)session.getAttribute("codruta") != null?(String)session.getAttribute("codruta"):"";	
	ruta = (String)session.getAttribute("ruta") != null?(String)session.getAttribute("ruta"):"";
	cliente = (String)session.getAttribute("codcli") != null?(String)session.getAttribute("codcli"):"";
	String nomcli = (String)session.getAttribute("nomcli") != null?(String)session.getAttribute("nomcli"):"";
	carga = (String)session.getAttribute("carga") != null?(String)session.getAttribute("carga"):"";
	observacion = (String)session.getAttribute("observacion") != null?(String)session.getAttribute("observacion"):"";	
	String numpla = (String)session.getAttribute("numpla") != null?(String)session.getAttribute("numpla"):"";
	
	session.removeAttribute("placa");
	session.removeAttribute("fecha_despacho");
	session.removeAttribute("fecha_salida");
	session.removeAttribute("cedcon");
	session.removeAttribute("nomcon");
	session.removeAttribute("codruta");
	session.removeAttribute("ruta");
	session.removeAttribute("codcli");
	session.removeAttribute("nomcli");
	session.removeAttribute("carga");
	session.removeAttribute("observacion");
	session.removeAttribute("numpla");
	
%>

<!-- *************************************************************************************** -->
<form name="forma1" action="<%=CONTROLLER%>?estado=Historico&accion=Insertar" method="post" onSubmit="return validar();">
  <table width="70%" border="2" align="center">
    <tr>
      <td>
	  
	  <table width="99%" align="center"> 
		  <tr>
					<td width="60%"  class="subtitulo1">
		Busqueda de planilla manual de plan de viaje..</td>
					<td width="40%"  class="barratitulo"><img src="<%=BASEURL%>/images/titulo.gif" width="32" height="20" align="left"><%=datos[0]%></td>
		  </tr>
		</table>

	  <table width="99%" align="center">
	  
  <tr class="fila">
    <td>
	  Planilla #..
	  <input type="hidden" name="codruta" id="codruta" value="<%=codruta%>">
      <input name="numpla" type="text" class="textbox" id="numpla" size="10" maxlength="10" value="<%=numpla%>" onKeyUp="Buscar_Info_Facil()"> 
	  <img src='<%=BASEURL%>/images/botones/buscar.gif' onClick="Buscar_Info( forma1.numpla.value )" title="Cargar informacion.." style="cursor:hand">
	</td>
  </tr>
</table></td>
    </tr>
  </table>  <br> 
  
  <%
  String mensaje=""+request.getParameter("ms");
    if (! (mensaje.equals("")|| mensaje.equals("null"))){
%>
<table border="2" align="center">
  <tr>
    <td><table width="410" height="20" border="1" align="center"  bordercolor="#F7F5F4" bgcolor="#FFFFFF">
        <tr>
          <td width="282" align="center" class="mensajes"><%=mensaje%></td>
          <td width="28" background="<%=BASEURL%>/images/cuadronaranja.JPG">&nbsp;</td>
          <td width="78">&nbsp;</td>
        </tr>
    </table></td>
  </tr>
</table>
<%
    } %>
	   <br> 
    <table width="70%" border="2" align="center">
      <tr>
    <td>
<table width="99%" align="center"> 
  <tr>
            <td width="60%"  class="subtitulo1">
Despacho Manual</td>
            <td width="40%"  class="barratitulo"><img src="<%=BASEURL%>/images/titulo.gif" width="32" height="20" align="left"></td>
  </tr>
</table>


        <table width="99%" align="center">
          <tr class="fila"> 
            <td width="261" height="21" >Placa : </td>
            <td colspan="2" >
			 
			   
			      <input name="c_placa" type="text" class="textbox" id="c_placa" value="<%=placa%>" size="6" maxlength="6" onKeyUp="CedulaXplaca('<%= CONTROLLER %>?estado=Despachom&accion=Lplaca&cargarDocumento=ok&c_placa='+this.value)" > 
			      <img src='<%=BASEURL%>/images/botones/iconos/lupa.gif'  width="15" height="15"  onClick="javascript:abrirVentanaBusq(850,550,'<%=BASEURL%>/jsp/trafico/despacho_manual/placa.jsp')" title="Buscar" style="cursor:hand" >
			      <a  href="javascript:infoPlaca()"HIDEFOCUS> <input type="hidden" name="tipo" id="tipo" >			      <br>Informacion Placa
		      </td>
          </tr>
          <tr class="fila"> 
            <td width="261" height="14" >Fecha Despacho : </td>
            <td width="181"><input name="c_fecha" type="text" class="textbox" id="c_fecha" size="20" readonly>
			<a href="javascript:void(0)" onclick="if(self.gfPop)gfPop.fPopCalendar(document.forma1.c_fecha);return false;" HIDEFOCUS>
              <img src="<%=BASEURL%>\js\Calendario\cal.gif" width="16" height="16" border="0" alt="De click aqui para escoger la fecha">
</td>
<td width="238">
<%
if ( !fecha.equals("") ) {
%><div align="left"  id="td_fech" name="td_fech"><%=fecha%></div>
<%
}
%>
</td>
			
          </tr>
          <tr class="fila">
            <td height="14" >Fecha Salida : </td>
            <td colspan="2"><input name="c_fecha2" type="text" class="textbox" id="c_fecha2" size="20" readonly value="<%=fecha2%>">
			 			<a href="javascript:void(0)" onclick="if(self.gfPop)gfPop.fPopCalendar(document.forma1.c_fecha2);return false;" HIDEFOCUS>
              <img src="<%=BASEURL%>\js\Calendario\cal.gif" width="16" height="16" border="0" alt="De click aqui para escoger la fecha">
			</td>
          </tr>
          <tr class="fila"> 
            <td width="261" height="30" >C&eacute;dula del Conductor :</td>
            <td colspan="2" >
		        <div align="left" id="comb">
			        <input name="c_conductor" type="text" class="textbox" id="c_conductor" value="<%=conductor%>"size="10" maxlength="10"  onChange="" onKeyPress="soloDigitos(event,'')"> 
			        <img src='<%=BASEURL%>/images/botones/iconos/lupa.gif'  width="15" height="15"  onClick="javascript:abrirVentanaBusq(850,550,'<%=BASEURL%>/jsp/trafico/despacho_manual/conductor.jsp')" title="Buscar" style="cursor:hand" >
	
		          <a h href="javascript:infoConductor()"HIDEFOCUS> <br>
		          Informacion Conductor</a>
			    </div>
		      </td></tr>
			  
			  <tr class="fila"> 			  
           		 <td width="261" height="30" >Nombre del Conductor :</td>
				 <td colspan="2" ><input name="n_conductor" type="text" class="textbox" id="n_conductor" value="<%=nomcon%>"size="50"></td>				 
			 </tr>
			
          <tr class="fila"> 
            <td height="14" >Ruta :</td>
            <td colspan="2"><input name="c_ruta" type="text" class="textbox" id="c_ruta" value="<%=ruta%>"  readonly size="50" >
              <img src='<%=BASEURL%>/images/botones/iconos/lupa.gif'  width="15" height="15"  onClick="javascript:abrirVentanaBusq(850,550,'<%=BASEURL%>/jsp/trafico/despacho_manual/ruta.jsp')" title="Buscar" style="cursor:hand" >	
			  <input name="c_cod_ruta" type="hidden" class="textbox" id="c_cod_ruta" value="<%=rutacod%>"></td>
          </tr>
          <tr class="fila">
            <td height="14" >Puesto de Control : </td>
            <td colspan="2">
			<select name="ubicacion" id="ubicacion" class='textbox' onchange="" >
			
			<% Vector ubicaciones =new Vector();
				ubicaciones= model.despachoManualService.listaPuestos();
	 			
				if(ubicaciones!=null&&ruta!=""){
				for(int i=0;i<ubicaciones.size();i++){
					Vector tmp = (Vector)(ubicaciones.get(i));%>
					<option value="<%=tmp.get(2)%>"><%=tmp.get(1)%></option>
				<%}
				}
				%>
            </select>
			
			</td>
          </tr>
          <tr class="fila"> 
            <td height="30" >C&oacute;digo del Cliente :</td>
            <td colspan="2"><input name="c_cliente" type="text" class="textbox" id="c_cliente" value="<%=cliente%>"size="6" maxlength="6" >              
            <img src='<%=BASEURL%>/images/botones/iconos/lupa.gif'  width="15" height="15"  onClick="javascript:abrirVentanaBusq(850,550,'<%=BASEURL%>/jsp/trafico/despacho_manual/cliente.jsp')" title="Buscar" style="cursor:hand" >
</td>
          </tr>

			<tr class="fila"> 			  
           		 <td width="261" height="30" >Nombre del Cliente :</td>
				 <td colspan="2" ><input name="n_cliente" type="text" class="textbox" id="n_cliente" value="<%=nomcli%>"size="50"></td>				 
			 </tr>
			 
          <tr class="fila"> 
              <td  align="center"><div align="left">Carga : </div></td>
			   <td colspan="2"  align="center">
                  <div align="left">
                    <textarea name="c_carga" cols="60" class="textbox" id="c_carga" rows="2"><%=carga%></textarea>
                  </div></td>
          </tr>
          <tr class="fila">
            <td  align="center"><div align="left">Justificaci&oacute;n : </div></td>
            <td colspan="2"  align="center"><div align="left">
              <select name="justificacion" id="justificacion" class='textbox' onchange="" >
                  <% Vector justific =new Vector();
				justific= model.despachoManualService.listaJustificaciones();
	 			System.out.println(":"+justific.size());  
				
				for(int i=0;i<justific.size();i++){
					%>
                  <option value="<%=justific.get(i)%>"><%=justific.get(i)%></option>
                  
				  <%}
				%>
              </select>
            </div></td>
          </tr>
          <tr class="fila">
            <td  align="center"><div align="left">Observaci&oacute;n : </div></td>
            <td colspan="2"  align="left"><textarea name="c_observacion" cols="60" class="textbox" id="c_observacion" rows="2"><%=observacion%></textarea></td>
          </tr>
        </table>
</td>
</tr>
</table>

  <br>
  <div align="center">
  <input type="hidden" name='usuario' value="<%=us%>">
      <img src='<%=BASEURL%>/images/botones/aceptar.gif' name='Buscar' align="absmiddle" style='cursor:hand' title='Aceptar...'  onclick="validar(forma1.ubicacion.selectedIndex,forma1.justificacion.selectedIndex);" onMouseOver='botonOver(this);' onMouseOut='botonOut(this);'> 
      &nbsp;&nbsp;<img src='<%=BASEURL%>/images/botones/salir.gif' name='Buscar' align="absmiddle" style='cursor:hand' title='Salir...'  onclick="salir();" onMouseOver='botonOver(this);' onMouseOut='botonOut(this);'> 
    </div>
  <p>&nbsp;</p> 
</form>
</div>
<%=datos[1]%>
</body>
<font id='aa'></font>
<iframe width=188 height=166 name="gToday:normal:agenda.js:gfPop:plugins_24.js" id="gToday:normal:agenda.js:gfPop:plugins_24.js" src="<%=BASEURL%>/js/Calendario/ipopeng.htm" scrolling="no" frameborder="0" style="visibility:visible; z-index:999; position:absolute; left:-500px; top:0px;"> </iframe>
</html>