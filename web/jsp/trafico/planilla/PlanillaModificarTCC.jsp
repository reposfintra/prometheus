<%@page contentType="text/html"%>
<%@page session="true"%>
<%@page errorPage="../error/ErrorPage.jsp"%>
<%@page import="java.util.*"%>
<%@page import="com.tsp.operation.model.*"%>
<%@page import="com.tsp.operation.model.beans.*"%>
<%@include file="/WEB-INF/InitModel.jsp"%>
<html>
<head>
<title>Planilla</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link href="<%= BASEURL %>/css/estilostsp.css" rel="stylesheet" type="text/css"> 
<script type='text/javascript' src="<%= BASEURL %>/js/boton.js"></script>
<script type='text/javascript' src="<%= BASEURL %>/js/validar.js"></script>
<link href="../../../css/estilostsp.css" rel="stylesheet" type="text/css">
</head>
<% 	Planilla pla = model.planillaService.getPlanilla(); 
String nomboton = (request.getParameter("tipo").equalsIgnoreCase("validar"))?"Validar":"Modificar";
String imagen = (request.getParameter("tipo").equalsIgnoreCase("validar"))?BASEURL+"/images/botones/validar.gif":BASEURL+"/images/botones/modificar.gif";
String trailer = (!(request.getParameter("tr")!=null))?pla.getPlatlr():"NA";
String []contenedores = (pla.getContenedores().equals(""))?"NA,NA".split(","):pla.getContenedores().split(","); 
String datos = (!(request.getParameter("tr")!=null))?"display:block":"display:none";
String t = ((request.getParameter("c_pltrailer")!=null))?request.getParameter("c_pltrailer"):trailer;
String pt = ((request.getParameter("proptr")!=null))?request.getParameter("proptr"):pla.getTipotrailer();
String c1 = ((request.getParameter("cont1")!=null))?request.getParameter("cont1"):contenedores[0];
String c2 = ((request.getParameter("cont1")!=null))?request.getParameter("cont2"):contenedores[1];
String pc = ((request.getParameter("propcont")!=null))?request.getParameter("propcont"):pla.getTipocont();
String estado = (nomboton.equalsIgnoreCase("Modificar"))?"readonly":"";
String estadoR = (nomboton.equalsIgnoreCase("Modificar"))?"disabled":"";%>
<body onresize="redimensionar()" onload = 'redimensionar()'>
<div id="capaSuperior" style="position:absolute; width:100%; height:100px; z-index:0; left: 0px; top: 0px;">
 <jsp:include page="/toptsp.jsp?encabezado=Confirmacion Trailer"/>
</div>
<div id="capaCentral" style="position:absolute; width:100%; height:87%; z-index:0; left: 0px; top: 100px; overflow: scroll;">
<form action="<%=CONTROLLER%>?estado=Planilla&accion=ModificarTCC&tipo=<%= nomboton %>" method="post" name="forma" >
<table width="50%"  border="2" align="center">
  <tr>
    <td>
	<!--Inicio Formulario-->
	<table width="100%" align="center" class="tablaInferior">
  <tr align="center" class="subtitulo1">
    <td colspan="2" class="titulo">PLANILLA Nro. <%= pla.getNumpla() %>
      <input type="hidden" name="planilla" value="<%= pla.getNumpla() %>"></td>
  </tr>
  <tr>
    <td width="27%" class="fila">Origen:</td>
    <td class="letra"><%= pla.getNomori() %></td>
  </tr>
  <tr>
    <td class="fila">Destino:</td>
    <td class="letra"><%= pla.getNomdest() %></td>
  </tr>
    <tr>
    <td nowrap class="fila">Fecha de Despacho:</td>
    <td class="letra"><%= pla.getFecdsp().substring(0,10) %></td>
  </tr>
  <tr class="subtitulo1">
    <td colspan="2">Informacion del Conductor</td>
  </tr>
  <tr>
    <td class="fila">Placa:</td>
    <td class="letra"><%= pla.getPlaveh() %>
      <input name="placa" type="hidden" id="placa" value="<%= pla.getPlaveh() %>"></td>
  </tr>
  <tr>
    <td class="fila">Conductor:</td>
    <td class="letra">[ <%= pla.getCedcon() %> ] <%= pla.getNomCond() %></td>
  </tr>
    <tr>
    <td class="fila">Trailer:</td>
    <td>
      <table width="100%"  border="0" cellpadding="0" cellspacing="0" class="fila">
        <tr>
          <td width="35%" class="letra"><%= trailer %></td>
          <td width="22%">Propiedad:</td>
          <td width="43%" class="letra"><%= pla.getTipotrailer() %></td>
        </tr>
      </table></td>
    </tr>
  
    <tr>
      <td class="fila">Contenedores</td>
      <td><%  %><table width="100%"  border="1" cellpadding="0" cellspacing="0" class="fila">
        <tr>
          <td width="32%" nowrap class="fila">Cont. Nro 1</td>
          <td width="68%" class="letra"><%= contenedores[0] %></td>
        </tr>
        <tr>
          <td nowrap class="fila">Cont. Nro 2 </td>
          <td class="letra"><%= contenedores[1]  %></td>
        </tr>
        <tr>
          <td nowrap class="fila">Propiedad</td>
          <td class="letra"><%= pla.getTipocont() %></td>
        </tr>
      </table></td>
    </tr>
  <tr style="<%= datos %>">
    <td colspan="2"><table width="100%" class="tablaInferior" id="datos">
  <tr class="subtitulos">
    <td colspan="2" class="informacion">**Digite la(s) placa(s) del trailer y/o los contenedores a modificar.</td>
  </tr>
  <tr class="subtitulo1">
    <td colspan="2">Trailer </td>
  </tr>
  <tr class="fila">
    <td class="fila">Placa</td>
    <td><input name="c_pltrailer" type="text" class="textbox" id="c_pltrailer" value="<%= t %>" <%= estado %>>      </td>
  </tr>
  <tr class="fila">
    <td class="fila">Propiedad</td>
    <td class="fila"><input name="proptr" type="radio" value="FINV" <% if (pt.equals("FINV")){%>checked<% } %>>
    TSP
      <input name="proptr" type="radio" value="NAV" <% if (!pt.equals("FINV")){%>checked<% } %>>
    Otro    </td>
  </tr>
  <tr class="subtitulo1">
    <td colspan="2">Contenedores</td>
  </tr>
  <tr class="fila">
    <td class="fila">Contenedor 1 </td>
    <td><input name="cont1" type="text" class="textbox" id="cont1" value="<%= c1 %>" <%= estado %>>      </td>
  </tr>
  <tr class="fila">
    <td class="fila">Contenedor 2 </td>
    <td><input name="cont2" type="text" class="textbox" id="cont2" value="<%= c2 %>" <%= estado %>>      </td>
  </tr>
  <tr class="fila">
    <td class="fila">Propiedad</td>
    <td class="fila"><input name="propcont" type="radio" value="FINV" <% if (pc.equals("FINV")){%>checked<% } %>>
    TSP
      <input name="propcont" type="radio" value="NAV" <% if (!pc.equals("FINV")){%>checked<% } %>>
    Naviera    </td>
  </tr>
</table>
</td>
  </tr>
</table>
	<!--fin Formulario-->
	</td>
  </tr>
</table>
<br>
<center>
<input name="cambiar" type="hidden" value="<%= nomboton %>">
<input type="image" value="Ingresar" src="<%= imagen %>" align="absmiddle" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);">
<img src="<%=BASEURL%>/images/botones/regresar.gif" name="c_salir" onClick="window.history.back();" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" align="absmiddle">
<img src="<%=BASEURL%>/images/botones/salir.gif" name="c_salir" onClick="window.close();" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" align="absmiddle">
</center>
</form>
<%if (request.getParameter("msg")!=null ){
String msg = request.getParameter("msg");%>
<script>var c = '<%= msg %>';alert(c.replace(/\-/g,"\n"));</script>
<%}%>
<br>
</div>
</body>
</html>
