<%@page contentType="text/html"%>
<%@page session="true"%>
<%@page errorPage="../error/ErrorPage.jsp"%>
<%@page import="java.util.*"%>
<%@page import="com.tsp.operation.model.*"%>
<%@page import="com.tsp.util.*"%>
<%@page import="com.tsp.operation.model.beans.*, java.util.*"%>
<%@include file="/WEB-INF/InitModel.jsp"%>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<title>Buscar Planilla</title>
<link href="<%= BASEURL %>/css/estilostsp.css" rel="stylesheet" type="text/css"> 
<script type='text/javascript' src="<%= BASEURL %>/js/validar.js"></script>
<script type='text/javascript' src="<%= BASEURL %>/js/boton.js"></script>
<link href="../../../css/estilostsp.css" rel="stylesheet" type="text/css">
</head>
<body onresize="redimensionar()" onload = 'redimensionar()'>
<div id="capaSuperior" style="position:absolute; width:100%; height:100px; z-index:0; left: 0px; top: 0px;">
 <jsp:include page="/toptsp.jsp?encabezado=Buscar Planilla"/>
</div>
<div id="capaCentral" style="position:absolute; width:100%; height:87%; z-index:0; left: 0px; top: 100px; overflow: scroll;">
<form action="<%=CONTROLLER%>?estado=Planilla&accion=ModificarTCC&tipo=buscar" method="post" name="forma" onSubmit="return validarTCamposLlenos();">
<table width="436" border="2" align="center">
  <tr>
    <td>
	<table width="100%" class="tablaInferior">
	  <tr>
        <td colspan="2">
		<table width="100%"  border="0" cellspacing="0" cellpadding="0">
		  <tr>
		    <td width="31%" class="subtitulo1">Buscar Planilla</td>
		    <td width="69%" class="barratitulo"><img src="<%=BASEURL%>/images/titulo.gif" width="32" height="20"></td>
		  </tr>
		</table>
		</td>
      </tr>
      <tr class="letra">
        <td colspan="2" class="subtitulos">**Digite el codigo de la planilla</td>
      </tr>
      <tr class="fila">
          <td>Planilla Nro.</td>
          <td><input type="text" name="planilla" class="textbox" id="planilla"></td>
      </tr>
    </table></td>
  </tr>
</table>
<br><center>
	<input type="image" name="Submit" value="buscar" src="<%= BASEURL %>/images/botones/buscar.gif" align="absmiddle" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);">	
    <img src="<%=BASEURL%>/images/botones/salir.gif" name="c_salir" onClick="window.close();" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" align="absmiddle">
</center>
<%String msg = (request.getParameter("mensaje")!=null)?request.getParameter("mensaje"):"";
if (!msg.equals("")){%>
<br>
<table border="2" align="center">
   		<tr>
        	<td>
            	<table width="100%"  border="1" align="center"  bordercolor="#F7F5F4" bgcolor="#FFFFFF">
                	<tr>
                    	<td width="229" align="center" class="mensajes"><%=msg%></td>
                        <td width="29" background="<%=BASEURL%>/images/cuadronaranja.JPG">&nbsp;</td>
                        <td width="58">&nbsp;</td>
                    </tr>
                </table>
            </td>
		</tr>
	</table>
<%}%>
</form>
</div>
</body>
</html>