<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
"http://www.w3.org/TR/html4/loose.dtd">
<%@page contentType="text/html"%>
<%@page session="true"%>

<%@page import="java.util.*"%>
<%@page import="com.tsp.operation.model.*"%>
<%@page import="com.tsp.util.*"%>
<%@page import="com.tsp.operation.model.beans.*, java.util.*"%>
<%@include file="/WEB-INF/InitModel.jsp"%>
<html>
<head>
<title>NOVEDAD</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link href="../../../css/estilostsp.css" rel="stylesheet" type="text/css"> 
<link href="<%=BASEURL%>/css/estilostsp.css" rel="stylesheet" type="text/css">
<script type='text/javascript' src="<%= BASEURL %>/js/boton.js"></script>
<script type='text/javascript' src="<%= BASEURL %>/js/validar.js"></script>
</head>

<body>
<% Novedad novedad;
   String mensaje = (request.getParameter("mensaje")!=null)?request.getParameter("mensaje"):"";
   String men="",cod = "",nom = "";   
   String action = CONTROLLER + "?estado=Insertar&accion=Novedad";
   String Titulo = "Ingresar Novedad";
   
   int sw=0,swcon=-1; 

   if( mensaje.equals("ErrorA") ) {
       swcon=0;
	   men = "Ya existe esta novedad";
   }		
   else if ( mensaje.equals("Agregado") ) {
       men ="La información ha sido agregada satisfactoriamente";
   }
   else if ( mensaje.equals("Modificar") ) {
       swcon=1;
	   sw = 1;
   	}
	else if ( mensaje.equals("Modificado") ) {
	    swcon=1;
	    sw = 1;	   
        men ="La información ha sido modificada satisfactoriamente";
   	}
	else if ( mensaje.equals("Anulado") ) {
        swcon = 0;
	    sw = 1;
	    men ="La novedad ha sido eliminada";
   	}
	else if ( mensaje.equals("Error_Anular") ) {
	    swcon=0;
		sw = 1;
		men = "No puede eliminar esta novedad";
   	}
	
	if ( swcon == 0 ){ //swcon = 0 capturo la informacion de la pag anterior
           cod = request.getParameter("codigo");
	    nom = request.getParameter("nombre");
	} 
	else if ( swcon == 1) { //swcon = 1 capturo el objeto con la información
	    novedad = model.novedadService.obtenerNovedad(); 
            cod = novedad.getCodNovedad();
            nom = novedad.getNomNovedad();
	}
	
    if ( sw==1 ){
        Titulo="Modificar Novedad";
	action = CONTROLLER + "?estado=Modificar&accion=Novedad";
    }
	//out.println(mensaje);
	//out.println(swcon);
%>

<form name="forma" method="post" action="<%=action%>" >
<table width="346"  border="2" align="center">
  <tr>
    <td>
<table width="99%" border="1" align="center">
  <tr>
    <td width="48%"  class="subtitulo1"><p align="left">Ingresar Novedad </p></td>
    <td width="52%"  class="barratitulo"><img src="<%=BASEURL%>/images/titulo.gif" width="32" height="20"></td>
  </tr>
</table>
  <table width="99%" align="center">
    
    <tr class="fila">
      <td width="123" valign="middle" ><div align="left" >Codigo</div></td>
      <td width="231" valign="middle">
      <input name="codigo" type="text" class="textbox" maxlength="3" value="<%=cod%>" <%if(sw==1){%> readonly <%}%>></td>
    </tr>
    <tr class="fila">
      <td>Nombre</td>
      <td >
      <input name="nombre" type="text" class="textbox" maxlength="40" value="<%=nom%>"></td>
    </tr>
    
   <% if (sw==0){%>
    <%} else {%>
	<%}%>
  </table>
  </td>
  </tr>
  </table>
  <br>
  <table width="346" align="center">
   <% if (sw==0){%>
  <tr>
    <td align="center">      <img src="<%=BASEURL%>/images/botones/aceptar.gif" name="mod"  height="21" onClick="validarTCamposLlenos()" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" style="cursor:hand"> &nbsp; 
	  <img src="<%=BASEURL%>/images/botones/cancelar.gif"  name="imgsalir"  height="21" onMouseOver="botonOver(this);" onClick="forma.reset();" onMouseOut="botonOut(this);" style="cursor:hand">&nbsp; 
	  <img src="<%=BASEURL%>/images/botones/salir.gif"  name="imgsalir"  height="21" onMouseOver="botonOver(this);" onClick="parent.close();" onMouseOut="botonOut(this);" style="cursor:hand"></td>
  </tr>
  <%} else {%>
  <tr>
    <td align="center">     
	 <img src="<%=BASEURL%>/images/botones/modificar.gif" name="mod"  height="21" onClick="forma.submit()" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" style="cursor:hand"> &nbsp; 
	 <img src="<%=BASEURL%>/images/botones/anular.gif"  name="imgsalir" height="21" onMouseOver="botonOver(this);" onClick="window.location='<%=CONTROLLER%>?estado=Anular&accion=Novedad&codigo=<%=cod%>&nombre=<%=nom%>'" onMouseOut="botonOut(this);" style="cursor:hand">&nbsp; 
	 <img src="<%=BASEURL%>/images/botones/salir.gif"  name="imgsalir" height="21" onMouseOver="botonOver(this);" onClick="parent.close();" onMouseOut="botonOut(this);" style="cursor:hand"> </td>
  </tr>
  <%}%>
</table>

  <%if(!men.equals("")){%>
	<p>
   <table width="420" border="2" align="center">
     <tr>
    <td><table width="100%"  border="1" align="center"  bordercolor="#F7F5F4" bgcolor="#FFFFFF">
      <tr>
        <td width="229" align="center" class="mensajes"><%=men%></td>
        <td width="29" background="<%=BASEURL%>/images/cuadronaranja.JPG">&nbsp;</td>
        <td width="58">&nbsp;</td>
      </tr>
    </table></td>
  </tr>
</table>
  </p>
  <%}%>

</form>
</body>
</html>
