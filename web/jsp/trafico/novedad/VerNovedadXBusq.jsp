<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
"http://www.w3.org/TR/html4/loose.dtd">
<%@page contentType="text/html"%>
<%@page session="true"%>
<%@page errorPage="../error/ErrorPage.jsp"%>
<%@page import="java.util.*"%>
<%@page import="com.tsp.operation.model.*"%>
<%@page import="com.tsp.util.*"%>
<%@page import="com.tsp.operation.model.beans.*, java.util.*"%>
<%@include file="/WEB-INF/InitModel.jsp"%>
<%@taglib uri="http://jsptags.com/tags/navigation/pager" prefix="pg" %>

<html>
<head>
<title>Listado de Novedades</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link href="<%= BASEURL %>/css/estilostsp.css" rel="stylesheet" type="text/css">
<link href="../../../css/estilostsp.css" rel="stylesheet" type="text/css"> 
<script type='text/javascript' src="<%= BASEURL %>/js/reporte.js"></script>
<script type='text/javascript' src="<%= BASEURL %>/js/boton.js"></script>
</head>

<body <%if(request.getParameter("reload")!=null){%>onLoad="window.close();window.opener.location.reload();"<%}%>>
<% 
   String style = "simple";
   String position =  "bottom";
   String index =  "center";
   int maxPageItems = 10;
   int maxIndexPages = 10;
   Vector VecPais = model.novedadService.obtNovedades();
   Novedad zona;  
   if (VecPais.size() >0 ){%>
      <table border="2" align="center" width="435">
  <tr>
    <td>
      <table width="99%" border="1" align="center"   bordercolor="#F7F5F4" bgcolor="#FFFFFF">
        <tr>
          <td width="392" height="24"  class="subtitulo1"><p align="left">Novedades</p></td>
          <td width="392"  class="barratitulo"><img src="<%=BASEURL%>/images/titulo.gif" width="32" height="20"></td>
        </tr>
      </table>
  <table width="99%" border="1" align="center" bordercolor="#999999" bgcolor="#F7F5F4">
    <tr class="tblTitulo">
      <td width="117" ><div align="center">Codigo
          <%//Codigo%></div></td>
      <td width="286" ><div align="center">Nombre<%//Nombre%></div></td>
    </tr>
	<pg:pager
    items="<%=VecPais.size()%>"
    index="<%= index %>"
    maxPageItems="<%= maxPageItems %>"
    maxIndexPages="<%= maxIndexPages %>"
    isOffset="<%= true %>"
    export="offset,currentPageNumber=pageNumber"
    scope="request">
<%-- keep track of preference --%>

  <%
      for (int i = offset.intValue(), l = Math.min(i + maxPageItems, VecPais.size()); i < l; i++)
	  {
          zona = (Novedad) VecPais.elementAt(i);%>
          
    <pg:item>
    <tr class="<%=(i % 2 == 0 )?"filagris":"filaazul"%>" onMouseOver='cambiarColorMouse(this)' style="cursor:hand" 
        onClick="window.open('<%=CONTROLLER%>?estado=Buscar&accion=Novedad&pagina=novedad.jsp&carpeta=jsp/trafico/novedad&codigo=<%=zona.getCodNovedad()%>&mensaje=Modificar' ,'','status=no,scrollbars=no,width=400,height=300,resizable=yes')">
      <td height="20" class="bordereporte"><%=zona.getCodNovedad()%></td>
      <td class="bordereporte"><%=zona.getNomNovedad()%></td>
    </tr>
    </pg:item>
  <%}%>

    <tr class="fila" align="center">
 	<td colspan="10" nowrap>
	<pg:index>
	<jsp:include page="../../../WEB-INF/jsp/googlesinimg.jsp" flush="true"/>
	</pg:index></td>
  </tr>
  </pg:pager>
</table>
</td>
</tr>
</table>
<br>
<%}
  else {%>      
        <table width="429" border="2" align="center">
     <tr>
    <td><table width="100%"  border="1" align="center"  bordercolor="#F7F5F4" bgcolor="#FFFFFF">
      <tr>
        <td width="229" align="center" class="mensajes">No se encontraron resultados </td>
        <td width="29" background="<%=BASEURL%>/images/cuadronaranja.JPG">&nbsp;</td>
        <td width="58">&nbsp;</td>
      </tr>
    </table></td>
  </tr>
</table>
        <%}%> 
<table width="435" align="center">
  <tr>
<td align="center"> 
<p align="left"><img src="<%=BASEURL%>/images/botones/regresar.gif" name="mod" height="21" onclick="window.location='<%=CONTROLLER%>?estado=Menu&accion=Cargar&pagina=BuscarNovedad.jsp&carpeta=/jsp/trafico/novedad'" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" style="cursor:hand"></td></tr></table>
</p>
</body>
</html>
