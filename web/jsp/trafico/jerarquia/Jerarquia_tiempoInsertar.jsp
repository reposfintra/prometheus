<%@page contentType="text/html"%>
<%@page session="true"%>
<%@page import="java.util.*"%>
<%@page import="com.tsp.operation.model.*"%>
<%@page import="com.tsp.operation.model.beans.*"%>
<%@page import="com.tsp.util.*"%>
<%@include file="/WEB-INF/InitModel.jsp"%>
<html>
<head>
<title>Insertar Jerarquia Tiempo</title>
<link href="<%= BASEURL %>/css/estilostsp.css" rel='stylesheet'>
<link href="../../../css/estilostsp.css" rel="stylesheet" type="text/css">
</head>
<script type='text/javascript' src="<%= BASEURL %>/js/validar.js"></script>
<script type='text/javascript' src="<%= BASEURL %>/js/boton.js"></script>
<body onResize="redimensionar()" onLoad="redimensionar()"> 
<div id="capaSuperior" style="position:absolute; width:100%; height:100px; z-index:0; left: 0px; top: 0px;">
 <jsp:include page="/toptsp.jsp?encabezado=Ingresar Jerarquia"/>
</div>
<div id="capaCentral" style="position:absolute; width:100%; height:87%; z-index:0; left: 0px; top: 100px; overflow: scroll;">
<%String mensaje = (String) request.getAttribute("mensaje");%>
<FORM name='forma' id='forma' method='POST' action="<%=CONTROLLER%>?estado=Jerarquia_tiempo&accion=Insert" >
<table width="450" border="2" align="center">
  <tr>
    <td>
	<table width="99%" align="center" class="tablaInferior">
  <tr>
    <td width="392"  class="subtitulo1"><p align="left">Ingresar Jerarquia Tiempo</p></td>
    <td width="392"  class="barratitulo"><img src="<%=BASEURL%>/images/titulo.gif" width="32" height="20"></td>
  </tr>
</table>
  <table width="99%" align="center" class="tablaInferior">
        <tr class="fila">
            <td width="144">Actividad</td>
            <td width="214">              
			<select name="c_actividad" class="listmenu" id="c_actividad" style="width:80%">
				  <%Vector vec = model.actividadSvc.ObtVecActividad (); 
				  	for(int i = 0; i<vec.size(); i++){	
					Actividad a = (Actividad) vec.elementAt(i);	%>
						<option value="<%=a.getCodActividad()%>"><%=a.getCodActividad()%> - <%=a.getDesLarga()%></option>
				<%}%>
		  </select>
		  </td>
        </tr>
		<tr class="fila">
            <td width="144">Responsable de la Actividad</td>
            <td width="214">              
			<select name="c_responsable" class="listmenu" id="c_responsable" style="width:80%">
				  <%LinkedList vec2 = model.tablaGenService.getResact(); 
				  	for(int i = 0; i<vec2.size(); i++){	
					TablaGen res = (TablaGen)  vec2.get(i);	%>
						<option value="<%=res.getTable_code()%>"><%=res.getDescripcion()%></option>
				<%}%>
		  </select>
		  </td>
        </tr>
		<tr class="fila">
            <td width="144">Codigo de Demora</td>
            <td width="214">              
			<select name="c_demora" class="listmenu" id="c_demora" style="width:80%">
				  <%LinkedList vec3 = model.tablaGenService.getCausa(); 
				  	for(int i = 0; i<vec3.size(); i++){	
					TablaGen cau = (TablaGen) vec3.get(i);	%>
						<option value="<%=cau.getTable_code()%>"><%=cau.getDescripcion()%></option>
				<%}%>
		  </select>
		  </td>
        </tr>
  </table>
  </td>
  </tr>
  </table>
  <br>
  <div align="center">
      <img src="<%=BASEURL%>/images/botones/aceptar.gif"  name="imgaceptar" width="90" height="21" onclick="return validarTCamposLlenos();" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" style="cursor:hand"> &nbsp; <img src="<%=BASEURL%>/images/botones/salir.gif"  name="imgsalir"  onMouseOver="botonOver(this);" onClick="parent.close();" onMouseOut="botonOut(this);" style="cursor:hand"></div>
  <%if(mensaje!=null){%>
	<p>
   <table border="2" align="center">
     <tr>
    <td><table width="100%"  border="1" align="center"  bordercolor="#F7F5F4" bgcolor="#FFFFFF">
      <tr>
        <td width="229" align="center" class="mensajes"><%=mensaje%></td>
        <td width="29" background="<%=BASEURL%>/images/cuadronaranja.JPG">&nbsp;</td>
        <td width="58">&nbsp;</td>
      </tr>
    </table></td>
  </tr>
</table>
  </p>
  <%}%>
   
</FORM>
</div>
</body>
</html>
