<%@page contentType="text/html"%>
<%@page session="true"%>
<%@page errorPage="/error/ErrorPage.jsp"%>
<%@page import="java.util.*"%>
<%@page import="com.tsp.operation.model.*"%>
<%@page import="com.tsp.operation.model.beans.*"%>
<%@page import="com.tsp.util.*"%>
<%@include file="/WEB-INF/InitModel.jsp"%>
<html>
<head><title>Buscar Jerarquia Tiempo</title>
<link href="<%= BASEURL %>/css/estilostsp.css" rel='stylesheet'>
<link href="../../../css/estilostsp.css" rel="stylesheet" type="text/css">
<script src="<%=BASEURL%>/js/boton.js"></script>
</head>
<div id="capaSuperior" style="position:absolute; width:100%; height:100px; z-index:0; left: 0px; top: 0px;">
	<jsp:include page="/WEB-INF/toptsp.jsp?encabezado=Buscar Jerarquia Tiempo"/> 
</div>
<div id="capaCentral" style="position:absolute; width:100%; height:87%; z-index:0; left: 0px; top: 100px; overflow: scroll;">

<%Usuario usuario = (Usuario) session.getAttribute("Usuario");%>
<form name="form2" method="post" action="<%=CONTROLLER%>?estado=Jerarquia_tiempo&accion=Serch&listar=True">
 <table width="416" border="2" align="center">
  <tr>
    <td>
	<table width="99%" align="center" class="tablaInferior">
  <tr>
    <td width="392"  class="subtitulo1"><p align="left">Buscar Jerarquia Tiempo</p></td>
    <td width="392"  class="barratitulo"><img src="<%=BASEURL%>/images/titulo.gif" width="32" height="20"></td>
  </tr>
</table>
    <table width="99%" align="center" class="tablaInferior">
        <tr class="fila">
            <td width="108">Codigo Actividad</td>
            <td width="159">
                <select name="c_actividad" class="listmenu" id="c_actividad">
                  <option value=""></option>  
                  <%Vector vec = model.actividadSvc.ObtVecActividad (); 
				  	for(int i = 0; i<vec.size(); i++){	
					Actividad a = (Actividad) vec.elementAt(i);	%>
                  <option value="<%=a.getCodActividad()%>"><%=a.getCodActividad()%> - <%=a.getDesLarga()%></option>
                  <%}%>
                </select></td>
        </tr>        
        <tr class="fila">
            <td>Codigo Responsable Actividad</td>
            <td><select name="c_responsable" class="listmenu" id="select">
             <option value=""></option>
              <%LinkedList vec2 = model.tablaGenService.getResact(); 
				  	for(int i = 0; i<vec2.size(); i++){	
					TablaGen res = (TablaGen)  vec2.get(i);	%>
              <option value="<%=res.getTable_code()%>"><%=res.getDescripcion()%></option>
              <%}%>
            </select></td>
        </tr>    
        <tr class="fila">
            <td>Codigo Demora</td>
            <td><select name="c_demora" class="listmenu" id="c_demora">
               <option value=""></option>
              <%LinkedList vec3 = model.tablaGenService.getCausa(); 
				  	for(int i = 0; i<vec3.size(); i++){	
					TablaGen cau = (TablaGen) vec3.get(i);	%>
              <option value="<%=cau.getTable_code()%>"><%=cau.getDescripcion()%></option>
              <%}%>
            </select></td>
        </tr> 		    
        <tr class="pie">
            
        </tr>        
    </table>
	</td>
	</tr>
  </table>
  <br>
  <table width="416" align="center">
 <td align='center'>                
                <img src="<%=BASEURL%>/images/botones/buscar.gif"  name="buscar" width="87" height="21" style="cursor:hand" onClick="form2.submit()" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);">&nbsp;                &nbsp;
                <img src="<%=BASEURL%>/images/botones/salir.gif" name="imgsalir"  height="21" style="cursor:hand" onClick="parent.close();"  onMouseOver="botonOver(this);" onMouseOut="botonOut(this);"> </td>            
        </tr>
  </table>
</form>
</div>
</body>
</html>
