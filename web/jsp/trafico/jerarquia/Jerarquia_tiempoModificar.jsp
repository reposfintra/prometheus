<%@page contentType="text/html"%>
<%@page session="true"%>
<%@page import="java.util.*"%>
<%@page import="com.tsp.operation.model.*"%>
<%@page import="com.tsp.operation.model.beans.*"%>
<%@page import="com.tsp.util.*"%>
<%@include file="/WEB-INF/InitModel.jsp"%>
<html>
<head>
<title>Modificar Jerarquia Tiempo</title>
<link href="<%= BASEURL %>/css/estilostsp.css" rel='stylesheet'>
<link href="../../../css/estilostsp.css" rel="stylesheet" type="text/css">
</head>
<script type='text/javascript' src="<%= BASEURL %>/js/validar.js"></script>
<script type='text/javascript' src="<%= BASEURL %>/js/boton.js"></script>
<body <%if(request.getParameter("reload")!=null){%>onLoad="window.opener.location.reload();redimensionar();window.close();"<%}%>  onResize="redimensionar();">
<div id="capaSuperior" style="position:absolute; width:100%; height:100px; z-index:0; left: 0px; top: 0px;">
	<jsp:include page="/WEB-INF/toptsp.jsp?encabezado=Modificar Jerarquia Tiempo"/> 
</div>
<div id="capaCentral" style="position:absolute; width:100%; height:87%; z-index:0; left: 0px; top: 100px; overflow: scroll;">
<%
    Usuario usuario = (Usuario) session.getAttribute("Usuario");
    Jerarquia_tiempo j = (Jerarquia_tiempo) request.getAttribute("jerarquia_tiempo");    
    String mensaje = (String) request.getAttribute("mensaje");  

	String act = j.getActividad();
	String ract = j.getResponsable();
	String dem = j.getDemora();
%>
<FORM name='forma' id='forma' method='POST'  action="<%=CONTROLLER%>?estado=Jerarquia_tiempo&accion=Update&sw=">
    <input type="hidden" name="c_nactividad" value="<%=j.getActividad()%>" id="c_nactividad">
	<input type="hidden" name="c_nresponsable" value="<%=j.getResponsable()%>" id="c_nresponsable">
	<input type="hidden" name="c_ndemora" value="<%=j.getDemora()%>" id="c_ndemora">
	<table width="450" border="2" align="center">
  <tr>
    <td>
	<table width="99%" align="center" border="0" class="tablaInferior">
  <tr>
    <td width="392" height="22"  class="subtitulo1"><p align="left">Modificar Jerarquia Tiempo</p></td>
    <td width="392"  class="barratitulo"><img src="<%=BASEURL%>/images/titulo.gif" width="32" height="20"></td>
  </tr>
</table>
	<table width="99%" align="center">
        <tr class="fila">
            <td width="191">Codigo Actividad</td>
            <td width="227"><select name="c_actividad" class="listmenu" id="c_actividad" style="width:90%">
              <%Vector vec = model.actividadSvc.ObtVecActividad (); 
				  	for(int i = 0; i<vec.size(); i++){	
					Actividad a = (Actividad) vec.elementAt(i);	%>
              <option value="<%=a.getCodActividad()%>" <%=(a.getCodActividad().equals(act))?"selected":""%>><%=a.getCodActividad()%> - <%=a.getDesLarga()%></option>
              <%}%>
            </select>
            </td>
        </tr>
        <tr class="fila">
            <td>Responsable de la Actividad</td>
            <td><select name="c_responsable" class="listmenu" id="c_responsable" style="width:80%">
              <%LinkedList vec2 = model.tablaGenService.getResact(); 
				  	for(int i = 0; i<vec2.size(); i++){	
					TablaGen res = (TablaGen)  vec2.get(i);	%>
              <option value="<%=res.getTable_code()%>" <%=(res.getTable_code().equals(ract))?"selected":""%> ><%=res.getDescripcion()%></option>
              <%}%>
            </select></td>
        </tr>   
        <tr class="fila">
            <td>Codigo de Demora</td>
            <td><select name="c_demora" class="listmenu" id="c_demora" style="width:80%">
              <%LinkedList vec3 = model.tablaGenService.getCausa(); 
				  	for(int i = 0; i<vec3.size(); i++){	
					TablaGen cau = (TablaGen) vec3.get(i);	%>
              <option value="<%=cau.getTable_code()%>" <%=(cau.getTable_code().equals(dem))?"selected":""%>><%=cau.getDescripcion()%></option>
              <%}%>
            </select></td>
        </tr> 		     
    </table>
	</td>
	</tr>
  </table>
  <br>
  <table width="446" align="center">
   <tr class="pie">
            <td align='center' colspan="3" >
<%              if (mensaje!="MsgAnulado"){%>					
					<img src="<%=BASEURL%>/images/botones/modificar.gif" name="mod"  height="21" onClick="forma.submit()" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" style="cursor:hand">&nbsp;
					<img src="<%=BASEURL%>/images/botones/anular.gif" name="mod"  height="21" onClick="forma.action='<%=CONTROLLER%>?estado=Jerarquia_tiempo&accion=Anular';forma.submit()" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" style="cursor:hand">&nbsp;
<%              }%>
					<img src="<%=BASEURL%>/images/botones/salir.gif"  name="imgsalir"  onMouseOver="botonOver(this);" onClick="parent.close();" onMouseOut="botonOut(this);" style="cursor:hand">&nbsp;
            </td>
    </tr> 
  </table>
    <%  if(mensaje!=null){
			mensaje = (mensaje.equals("MsgModificado"))?"Modificación exitosa!":mensaje;%>
	<p>
   <table width="400" border="2" align="center">
     <tr>
    <td><table width="100%"  border="1" align="center"  bordercolor="#F7F5F4" bgcolor="#FFFFFF">
      <tr>
        <td width="281" align="center" class="mensajes"><%=mensaje%></td>
        <td width="31" background="<%=BASEURL%>/images/cuadronaranja.JPG">&nbsp;</td>
        <td width="79">&nbsp;</td>
      </tr>
    </table></td>
  </tr>
</table>
  </p>
  <%}%>
	
</FORM>
</div>
</body>
</html>
