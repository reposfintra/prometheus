<%@page contentType="text/html"%>
<%@page session="true"%>
<%@page errorPage="/error/ErrorPage.jsp"%>
<%@page import="java.util.*"%>
<%@page import="com.tsp.operation.model.*"%>
<%@page import="com.tsp.operation.model.beans.*"%>
<%@page import="com.tsp.util.*"%>
<%@include file="/WEB-INF/InitModel.jsp"%>
<%@taglib uri="http://jsptags.com/tags/navigation/pager" prefix="pg" %>
<html>
<head><title>Listar Jerarquia Tiempo</title>
<link href="<%= BASEURL %>/css/estilostsp.css" rel='stylesheet'>
<link href="../../../css/estilostsp.css" rel="stylesheet" type="text/css">
<script type='text/javascript' src="<%= BASEURL %>/js/reporte.js"></script>
<script type='text/javascript' src="<%= BASEURL %>/js/boton.js"></script>
</head> 
<div id="capaSuperior" style="position:absolute; width:100%; height:100px; z-index:0; left: 0px; top: 0px;">
	<jsp:include page="/WEB-INF/toptsp.jsp?encabezado=Listado Jerarquia Tiempo"/>
</div>
<div id="capaCentral" style="position:absolute; width:100%; height:87%; z-index:0; left: 0px; top: 100px; overflow: scroll;">
<% Vector vec = (Vector) request.getAttribute("jerarquia_tiempos");
String distrito = (String) session.getAttribute("Distrito");
    String style = "simple";
    String position =  "bottom";
    String index =  "center";
    int maxPageItems = 10;
    int maxIndexPages = 10;
	
	if (vec!=null){
        
    }else{
        model.jerarquia_tiempoService.listJerarquia_tiempo();
        vec = model.jerarquia_tiempoService.getJerarquia_tiempos();    
    }

%>
<br>
<table width="567" border="2" align="center">
  <tr>
    <td>
	<table width="100%" border="1" align="center" cellpadding="0" bordercolor="#F7F5F4">
  <tr>
    <td width="392"  class="subtitulo1"><p align="left">Jerarquia Tiempo</p></td>
    <td width="392"  class="barratitulo"><img src="<%=BASEURL%>/images/titulo.gif" width="32" height="20"></td>
  </tr>
</table>
<table width="99%" border="1" align="center" bordercolor="#999999" bgcolor="#F7F5F4">
    <tr class="tblTitulo" align="center">
        <td >ActividadTrafico</td>
        <td >Responsable de la Actividad</td>
		<td >Codigo de Demora</td>
    </tr>
    <pg:pager
        items="<%=vec.size()%>"
        index="<%= index %>"
        maxPageItems="<%= maxPageItems %>"
        maxIndexPages="<%= maxIndexPages %>"
        isOffset="<%= true %>"
        export="offset,currentPageNumber=pageNumber"
        scope="request">
<%
    for (int i = offset.intValue(), l = Math.min(i + maxPageItems, vec.size()); i < l; i++){
        Jerarquia_tiempo j = (Jerarquia_tiempo) vec.elementAt(i);%>
        <pg:item>
            <tr class="<%=(i % 2 == 0 )?"filagris":"filaazul"%>" onMouseOver='cambiarColorMouse(this)' style="cursor:hand" title="Modificar Jerarquia Tiempo..." onClick="window.open('<%=CONTROLLER%>?estado=Jerarquia_tiempo&accion=Serch&c_actividad=<%=j.getActividad()%>&c_responsable=<%=j.getResponsable()%>&c_demora=<%=j.getDemora()%>&listar=False','myWindow','status=no,scrollbars=no,width=650,height=650,resizable=yes');">
                <td class="bordereporte"> <%=j.getDesactividad()%></td>
                <td class="bordereporte"> <%=j.getDesResponsable()%></td>
				<td class="bordereporte"> <%=j.getDesCausa()%></td>
            </tr>
      </pg:item>
<%  }%>
    <tr>
        <td td height="20" colspan="10" nowrap align="center">          <pg:index>
                <jsp:include page="/WEB-INF/jsp/googlesinimg.jsp" flush="true"/>
        </pg:index>
        </td>
    </tr>
    </pg:pager>        
</table>
</td>
</tr>
</table>
<br>
<table width="567" align="center">
  <tr>
<td height="25" align="left" valign="middle">
  <img src="<%=BASEURL%>/images/botones/regresar.gif"  onClick="location.href('<%=CONTROLLER%>?estado=Menu&accion=Cargar&marco=no&carpeta=/jsp/trafico/jerarquia&pagina=Jerarquia_tiempoBuscar.jsp')" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" width="101" height="21"></td>
  </tr></table>
</div>
</body>
</html>
