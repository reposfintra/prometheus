<!--
- Autor : Ing. Leonardo Parody Ponce
- Date : 27 diciembre de 2005
- Copyrigth Notice : Fintravalores S.A. S.A
-->
<%--
-@(#)
--Descripcion : Pagina JSP, que sirve para ingresar precintos.
--%>

<%@page contentType="text/html"%>
<%@page session="true"%>
<%@page import="java.util.*"%>
<%@page import="com.tsp.operation.model.*"%>
<%@page import="com.tsp.operation.model.beans.*"%>
<%@page import="com.tsp.operation.controller.*"%>
<%@page import="com.tsp.util.*"%>
<%@page errorPage="/error/ErrorPage.jsp"%>
<%@include file="/WEB-INF/InitModel.jsp"%>
<%@ taglib uri="http://jakarta.apache.org/taglibs/input-0.90" prefix="input" %>
<%String path = application.getRealPath(model.menuService.getRealUrl(request.getRequestURI()));
  String datos[] = model.menuService.getContenidoMenu(BASEURL,request.getRequestURI(),path);
%>
<html>
<head>
<title>Ingresar Precintos</title>
<link href="<%= BASEURL %>/css/estilostsp.css" rel='stylesheet'>
<link href="../../../../css/estilostsp.css" rel="stylesheet" type="text/css">
<link href="/css/estilostsp.css" rel="stylesheet" type="text/css">
</head>
<script type='text/javascript' src="<%= BASEURL %>/js/validar.js"></script>
<script src='<%= BASEURL %>/js/date-picker.js'></script>
<script type='text/javascript' src="<%= BASEURL %>/js/boton.js"></script>
<body onresize="redimensionar()" onload = 'redimensionar()'>
<div id="capaSuperior" style="position:absolute; width:100%; height:100px; z-index:0; left: 0px; top: 0px;">
 <jsp:include page="/toptsp.jsp?encabezado=Precintos"/>
</div>
<div id="capaCentral" style="position:absolute; width:100%; height:87%; z-index:0; left: 0px; top: 100px; overflow: scroll;">
<%
	TreeMap agencias = model.agenciaService.listar();//model.agenciaService.listar();
	agencias.remove("");
	agencias.put(" Seleccione Alguna","NADA");
%>
<form name="forma" id="forma" method="post">
  <table width="300"  border="2" align="center" cellpadding="0" cellspacing="0">
    <tr>
      <td><table width="100%" class="tablaInferior">
        <tr>
          <td colspan="2" ><table width="100%"  border="0" cellpadding="0" cellspacing="1" class="barratitulo">
              <tr>
                <td width="56%" class="subtitulo1">&nbsp;Ingresar precintos </td>
                <td width="44%" class="barratitulo"><img align="left" src="<%=BASEURL%>/images/titulo.gif" width="32" height="20"><%=datos[0]%></td>
              </tr>
          </table></td>
        </tr>
        <tr class="fila">
          <td>Inicio de Serie </td>
          <td nowrap>
            <input name="inicio" type='text' class="textbox" id="inicio" style='width:120' value='' onKeyPress="soloDigitos(event,'decNo')" maxlength="10">

		  </td>
        </tr>
		 <tr class="fila">
          <td>Final de Serie</td>
          <td nowrap>
            <input name="fin" type='text' class="textbox" id="fin" style='width:120' value='' onKeyPress="soloDigitos(event,'decNo')" maxlength="10">
          </td>
        </tr>
		<tr class="fila">
          <td>Tipo de Documento</td>
          <td nowrap>
            <select name="tipo_documento" class="textbox" id="tipo_documento" >
			<option selected value='Seleccione Alguna'>Seleccione Alguna</option>
			<option value='Precinto'>Precinto</option>
			<option value='Sticker'>Sticker</option>
			</select>
          </td>
        </tr>
		<tr class="fila">
          <td>Agencia </td>      
          <td ><input:select name="agencia" attributesText="class=textbox" options="<%= agencias %>" default="NADA"/></td>
		  </tr>
      </table></td>
    </tr>
  </table>
  <br>
  <center>
    <img src="<%=BASEURL%>/images/botones/aceptar.gif" style= "cursor:hand" name="c_aceptar" onClick="return validarprecintos();" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);"> 
	<img src="<%=BASEURL%>/images/botones/cancelar.gif" style= "cursor:hand" name="c_cancelar" onClick="forma.reset();" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);">
	<img src="<%=BASEURL%>/images/botones/salir.gif" style= "cursor:hand" name="c_salir" onClick="parent.close();" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);">
  </center>
</form>
  <% if( !request.getParameter("msg").equals("") ){%>
  <p><table border="2" align="center">
  <tr>
    <td><table width="100%"  border="1" align="center"  bordercolor="#F7F5F4" bgcolor="#FFFFFF">
      <tr>
        <td width="229" align="center" class="mensajes"><%=request.getParameter("msg").toString()%></td>
        <td width="29" background="<%=BASEURL%>/images/cuadronaranja.JPG">&nbsp;</td>
        <td width="58">&nbsp;</td>
      </tr>
    </table></td>
  </tr>
</table>
</p>
 <%} %>
</div>
<%=datos[1]%>
</body>
</html>
<script>
function validarprecintos(){
	if (document.forma.inicio.value==''){
		alert('Especifique el inicio de la serie');
		return false;
	}else if (document.forma.fin.value==''){
		alert('Especifique el final de la serie');
		return false;	
	}else if (isNaN(document.forma.inicio.value)){
		alert('El valor del inicio de la serie debe ser num�rico.');
		return false;
	}else if (isNaN(document.forma.fin.value)){
		alert('El valor del final de la serie debe ser num�rico');
		return false;	
	}
	else{
	 var n1=document.getElementById("inicio");
	 var n2=document.getElementById("fin");
	  // alert (n1.value + "  "+ n2.value);
	   var v1=parseInt(n1.value);
	   var v2=parseInt(n2.value);
	   if(v1> v2){
	   		alert('El valor inicial no puede ser mayor que el valor final');
			return false;
		}else if (document.forma.tipo_documento.value=='Seleccione Alguna'){
			alert('Especifique un tipo de documento');
			return false;
		}else if (document.forma.agencia.value=='NADA') {
			alert('Seleccione una agencia');
			return false;
		}else{
			forma.action = '<%=CONTROLLER%>?estado=Precintos&accion=Insert';
			forma.submit();
		}	
	}
}
</script>
