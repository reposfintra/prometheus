<%@ include file="/WEB-INF/InitModel.jsp"%>

<HTML>
<HEAD>
<TITLE></TITLE>
<META http-equiv=Content-Type content="text/html; charset=windows-1252">
<link href="../css/estilostsp.css" rel="stylesheet" type="text/css"> 
<link href="<%=BASEURL%>/css/estilostsp.css" rel="stylesheet" type="text/css"> 
</HEAD>
<BODY> 
<% String BASEIMG = BASEURL +"/images/ayuda/trafico/despacho_manual/"; %> 

  <table width="95%"  border="2" align="center">
    <tr>
      <td>
        <table width="100%" border="0" align="center">
          <tr  class="subtitulo">
            <td height="20"><div align="center">INGRESO DE SERIE DE PRECINTOS/STICKERS </div></td>
          </tr>
          <tr class="subtitulo1">
            <td>Descripci&oacute;n del funcionamiento del programa de inserci&oacute;n de series de precintos o stickers.</td>
          </tr>
          <tr>
            <td  height="18"  class="ayudaHtmlTexto">Formulario de ingreso de series de precintos y/o stickers.</td>
          </tr>
          <tr>
            <td height="18"  class="ayudaHtmlTexto"><div align="center"><img border="0" src="<%= BASEURL %>/images/ayuda/trafico/precinto/Dibujo1.JPG" ></div></td>
          </tr>
          <tr>
            <td  class="ayudaHtmlTexto">&nbsp;</td>
          </tr>
          <tr>
            <td  class="ayudaHtmlTexto">Digitamos el inicio y el final de la serie. </td>
          </tr>
          <tr>
            <td  class="ayudaHtmlTexto"><div align="center"><img src="<%= BASEURL %>/images/ayuda/trafico/precinto/Dibujo2.JPG" ></div></td>
          </tr>
          <tr>
            <td  class="ayudaHtmlTexto">&nbsp;</td>
          </tr>
          <tr>
            <td  class="ayudaHtmlTexto">Seleccionamos el tipo de documento al que pertenece la serie. </td>
          </tr>
          <tr>
            <td  class="ayudaHtmlTexto"><div align="center"><img src="<%= BASEURL %>/images/ayuda/trafico/precinto/Dibujo3.JPG" ></div></td>
          </tr>
          <tr>
            <td  class="ayudaHtmlTexto">&nbsp;</td>
          </tr>
          <tr>
            <td  class="ayudaHtmlTexto">Seleccionamos la agencia a la cual se le asociar&aacute; la serie de precintos o stickers.</td>
          </tr>
          <tr>
            <td  class="ayudaHtmlTexto"><div align="center"><img src="<%= BASEURL %>/images/ayuda/trafico/precinto/Dibujo4.JPG" width="650" height="505"></div></td>
          </tr>
          <tr>
            <td  class="ayudaHtmlTexto">&nbsp;</td>
          </tr>
          <tr>
            <td  class="ayudaHtmlTexto">A continuaci&oacute;n presionamos el bot&oacute;n aceptar para enviar el formulario. </td>
          </tr>
          <tr>
            <td  class="ayudaHtmlTexto"><div align="center"><img src="<%= BASEURL%>/images/ayuda/trafico/precinto/Dibujo5.JPG" width="747" height="441"></div></td>
          </tr>
          <tr>
            <td  class="ayudaHtmlTexto">&nbsp;</td>
          </tr>
          <tr>
            <td  class="ayudaHtmlTexto">Al ingresar la serie digitada, nos aparecer&aacute; un mensaje confirmando el proceso exitoso de la informaci&oacute;n. </td>
          </tr>
          <tr>
            <td  class="ayudaHtmlTexto"><div align="center"><img src="<%= BASEURL%>/images/ayuda/trafico/precinto/Dibujo6.JPG" width="682" height="316"></div></td>
          </tr>
          <tr>
            <td  class="ayudaHtmlTexto"><div align="center"></div></td>
          </tr>
          
      </table></td>
    </tr>
  </table>
</BODY>
</HTML>
