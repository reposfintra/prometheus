<!--
- Autor : Ing. Andr�s Maturana De La Cruz
- Date : 6 abril del 2006
- Copyrigth Notice : Fintravalores S.A. S.A
-->
<%--
-@(#)
--Descripcion : Pagina JSP, que sirve para consultar series de precintos/stickers.
--%>

<%@page contentType="text/html"%>
<%@page session="true"%>
<%@page import="java.util.*"%>
<%@page import="com.tsp.operation.model.*"%>
<%@page import="com.tsp.operation.model.beans.*"%>
<%@page import="com.tsp.operation.controller.*"%>
<%@page import="com.tsp.util.*"%>
<%@page errorPage="/error/ErrorPage.jsp"%>
<%@include file="/WEB-INF/InitModel.jsp"%>
<%@ taglib uri="http://jakarta.apache.org/taglibs/input-0.90" prefix="input" %>
<%String path = application.getRealPath(model.menuService.getRealUrl(request.getRequestURI()));
  String datos[] = model.menuService.getContenidoMenu(BASEURL,request.getRequestURI(),path);
%>

<html>
<head>
<title>Consultar Precintos/Stickers</title>
<link href="<%= BASEURL %>/css/estilostsp.css" rel='stylesheet'>
<link href="../../../../css/estilostsp.css" rel="stylesheet" type="text/css">
<link href="/css/estilostsp.css" rel="stylesheet" type="text/css">
</head>
<script type='text/javascript' src="<%= BASEURL %>/js/validar.js"></script>
<script src='<%= BASEURL %>/js/date-picker.js'></script>
<script type='text/javascript' src="<%= BASEURL %>/js/boton.js"></script>
<body onresize="redimensionar()" onload = 'redimensionar()'>
<div id="capaSuperior" style="position:absolute; width:100%; height:100px; z-index:0; left: 0px; top: 0px;">
 <jsp:include page="/toptsp.jsp?encabezado=Consultar Precintos/Stickers"/>
</div>
<div id="capaCentral" style="position:absolute; width:100%; height:87%; z-index:0; left: 0px; top: 100px; overflow: scroll;">
<%
	TreeMap agencias = model.agenciaService.listar();
	agencias.remove("");
	agencias.put(" Seleccione","");
%>
<form name="forma" id="forma" method="post" action="<%=CONTROLLER%>?estado=Precintos&accion=Consultar">
  <table width="300"  border="2" align="center" cellpadding="0" cellspacing="0">
    <tr>
      <td><table width="100%" class="tablaInferior">
        <tr>
          <td colspan="2" ><table width="100%"  border="0" cellpadding="0" cellspacing="1" class="barratitulo">
              <tr>
                <td width="56%" class="subtitulo1">&nbsp;Filtros</td>
                <td width="44%" class="barratitulo"><img align="left" src="<%=BASEURL%>/images/titulo.gif" width="32" height="20"><%=datos[0]%></td>
              </tr>
          </table></td>
        </tr>
		<tr class="fila">
          <td>Tipo de Documento</td>
          <td nowrap>
            <select name="tipo_documento" class="textbox" id="tipo_documento" style='width:120'>
			<option selected value=''>Seleccione</option>
			<option value='Precinto'>Precinto</option>
			<option value='Sticker'>Sticker</option>
			</select>
          </td>
        </tr>
		<tr class="fila">
          <td>Agencia </td>      
          <td ><input:select name="agencia" attributesText="class=textbox" options="<%= agencias %>" default="NADA"/></td>
		  </tr>
      </table></td>
    </tr>
  </table>
  <br>
  <center>
    <img src="<%=BASEURL%>/images/botones/aceptar.gif" name="c_aceptar" onClick="if ( TCamposLlenos() ) { forma.submit(); }" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" style="cursor:hand"> 
	<img src="<%=BASEURL%>/images/botones/cancelar.gif" name="c_cancelar" onClick="forma.reset();" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" style="cursor:hand"> 
	<img src="<%=BASEURL%>/images/botones/salir.gif" name="c_salir" onClick="parent.close();" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" style="cursor:hand">
  </center>
</form>
  <% if( request.getParameter("msg")!=null ){%>
  <p><table border="2" align="center">
  <tr>
    <td><table width="100%"  border="1" align="center"  bordercolor="#F7F5F4" bgcolor="#FFFFFF">
      <tr>
        <td width="229" align="center" class="mensajes"><%=request.getParameter("msg").toString()%></td>
        <td width="29" background="<%=BASEURL%>/images/cuadronaranja.JPG">&nbsp;</td>
        <td width="58">&nbsp;</td>
      </tr>
    </table></td>
  </tr>
</table>
</p>
 <%} %>
</div>
<%=datos[1]%>
</body>
</html>
<script>
function validarprecintos(){
	if (document.forma.inicio.value==''){
		alert('Especifique el inicio de la serie');
		return false;
	}else if (document.forma.fin.value==''){
		alert('Especifique el final de la serie');
		return false;	
	} else{
	 var n1=document.getElementById("inicio");
	 var n2=document.getElementById("fin");
	  // alert (n1.value + "  "+ n2.value);
	   var v1=parseInt(n1.value);
	   var v2=parseInt(n2.value);
	   if(v1> v2){
	   		alert('El valor inicial no puede ser mayor que el valor final');
			return false;
		}else if (isNaN(n1.value) || isNaN(n2.value)) {
			alert('Los valores de la serie deben ser num�ricos.');
			return false;
		}else{
			forma.action = '<%=CONTROLLER%>?estado=Precintos&accion=Consultar';
			forma.submit();
		}	
	}
}
</script>
