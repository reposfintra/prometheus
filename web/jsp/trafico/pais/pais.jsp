<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
"http://www.w3.org/TR/html4/loose.dtd">
<%@page contentType="text/html"%>
<%@page session="true"%>
<%@page errorPage="../error/ErrorPage.jsp"%>
<%@page import="java.util.*"%>
<%@page import="com.tsp.operation.model.*"%>
<%@page import="com.tsp.util.*"%>
<%@page import="com.tsp.operation.model.beans.*, java.util.*"%>
<%@include file="/WEB-INF/InitModel.jsp"%>
<html>
<head>
<title>PAIS</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">

<link href="<%= BASEURL %>/css/estilostsp.css" rel="stylesheet" type="text/css"> 
<link href="../../../css/estilostsp.css" rel="stylesheet" type="text/css"> 
<script type='text/javascript' src="<%= BASEURL %>/js/validar.js"></script>
<script type='text/javascript' src="<%= BASEURL %>/js/boton.js"></script>
</head>

<body <%if(request.getParameter("reload")!=null){%>onLoad="parent.opener.location.reload();"<%}%>>
<% 
   Pais pais;
   String mensaje =(request.getParameter("mensaje")!=null)?request.getParameter("mensaje"):"";
   String men="",cod = "",nom = "";
   String Titulo= "Ingresar Paises "; 
   String Subtitulo = "Informacion";
   String Codigo = "Codigo";
   String Nombre = "Nombre";
   String action = CONTROLLER + "?estado=Insertar&accion=Pais";  
   String a = (request.getParameter("sw")!=null)?request.getParameter("sw"):"-1";
   int swcon=-1;
   String tipo="text";
   if ( a != null ){
        swcon = Integer.parseInt( a );
   }	
   if ( swcon == 0 ){ //swcon = 0 capturo la informacion de la pag anterior
        cod = request.getParameter("c_codigo");
	    nom = request.getParameter("c_nombre");
   } 
   else if ( swcon == 1) { //swcon = 1 capturo el objeto con la información
        pais = model.paisservice.obtenerpais(); 
        cod = pais.getCountry_code();
        nom = pais.getCountry_name();
		tipo="hidden";
        if ( nom == null ){
            nom = request.getParameter("c_nombre");
        } 
        Titulo="Modificar Pais";
	action = CONTROLLER + "?estado=Modificar&accion=Pais";
   }
%>

<form name="forma" method="post" action="<%=action%>">
  <table width="380"  border="2" align="center">
    <tr>
      <td>
	  <table width="100%"  border="0">
              <tr>
                <td width="48%" class="subtitulo1"><%=Subtitulo%></td>
                <td width="52%" class="barratitulo"><img src="<%=BASEURL%>/images/titulo.gif" width="32" height="20"></td>
              </tr>
        </table>
		  <table width="100%" class="tablaInferior" >
        <tr class="fila">
          <td><%=Codigo%></td>
          <td width="235" ><input name="c_codigo" type="<%=tipo%>" class="textbox" maxlength="3" value="<%=cod%>"  ><%if(swcon==1){%><%=cod%><%}else{%>
            <img src="<%=BASEURL%>/images/botones/iconos/obligatorio.gif" width="10" height="10"><%}%>          </td>
        </tr>
        <tr class="fila">
          <td><%=Nombre%></td>
          <td><input name="c_nombre" type="text" class="textbox" maxlength="40" value="<%=nom%>">
            <img src="<%=BASEURL%>/images/botones/iconos/obligatorio.gif" width="10" height="10"> </td>
        </tr>
      </table></td>
    </tr>
  </table>
   <p>
      <% if ((swcon==0) || (swcon==-1) ){%>
	       <div align="center">
			   <img src="<%=BASEURL%>/images/botones/aceptar.gif"  name="imgaceptar" onClick="return TCamposLlenos();" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" style="cursor:hand">&nbsp;
			   <img src="<%=BASEURL%>/images/botones/cancelar.gif"  name="imgcancelar"  onMouseOver="botonOver(this);" onClick="forma.reset();" onMouseOut="botonOut(this);" style="cursor:hand"> &nbsp;
               <img src="<%=BASEURL%>/images/botones/salir.gif"  name="imgsalir"  onMouseOver="botonOver(this);" onClick="parent.close();" onMouseOut="botonOut(this);" style="cursor:hand">
           </div>
		  <%} else {%>
		  	<div align="center">
			  <img src="<%=BASEURL%>/images/botones/modificar.gif"  name="imgmodificar" onClick="return TCamposLlenos();" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" style="cursor:hand">&nbsp;
              <img src="<%=BASEURL%>/images/botones/anular.gif"  name="imganular" onClick="window.location='<%=CONTROLLER%>?estado=Anular&accion=Pais&c_codigo=<%=cod%>&c_nombre=<%=nom%>'" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" style="cursor:hand">&nbsp;
              <img src="<%=BASEURL%>/images/botones/salir.gif"  name="imgsalir"  onMouseOver="botonOver(this);" onClick="parent.close();" onMouseOut="botonOut(this);" style="cursor:hand">
          </div>
		  <%}%>
</p>
   <%if( !mensaje.equalsIgnoreCase("")){%>
  <p><table border="2" align="center">
  <tr>
    <td><table width="100%">
      <tr>
        <td width="229" align="center" class="mensajes"><%=mensaje%></td>
        <td width="29" background="<%=BASEURL%>/images/cuadronaranja.JPG">&nbsp;</td>
        <td width="58">&nbsp;</td>
      </tr>
    </table></td>
  </tr>
</table>
</p>
 <%}%>
 
</form>
</body>
</html>
