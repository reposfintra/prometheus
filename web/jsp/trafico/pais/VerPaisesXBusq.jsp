<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
"http://www.w3.org/TR/html4/loose.dtd">
<%@page contentType="text/html"%>
<%@page session="true"%>
<%@page errorPage="../error/ErrorPage.jsp"%>
<%@page import="java.util.*"%>
<%@page import="com.tsp.operation.model.*"%>
<%@page import="com.tsp.util.*"%>
<%@page import="com.tsp.operation.model.beans.*, java.util.*"%>
<%@include file="/WEB-INF/InitModel.jsp"%>
<%@taglib uri="http://jsptags.com/tags/navigation/pager" prefix="pg" %>


<html>
<head>
<title>Listado de Paises</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link href="<%= BASEURL %>/css/estilostsp.css" rel="stylesheet" type="text/css"> 
<link href="../../../css/estilostsp.css" rel="stylesheet" type="text/css"> 
<script type='text/javascript' src="<%= BASEURL %>/js/reporte.js"></script>
<script type='text/javascript' src="<%= BASEURL %>/js/boton.js"></script>
</head>

<body>
<% 
   String style = "simple";
   String position =  "bottom";
   String index =  "center";
   int maxPageItems = 10;
   int maxIndexPages =10;
   String nombre = (String) session.getAttribute("nom");
   model.paisservice.buscarpaisnombre(nombre+"%");
   Vector VecPais = model.paisservice.obtpaises();
   Pais pais;  
   if (VecPais.size() >0 ){%>
  <form name="form1" method="post" action="">
    <table width="480" border="2" align="center" >
      <tr>
        <td><table width="100%" class="tablaInferior">
            <tr>
              <td width="40%" class="subtitulo1">&nbsp;Datos Paises </td>
              <td width="50%" class="barratitulo"><img src="<%=BASEURL%>/images/titulo.gif" width="32" height="20">
              <input name="nom" type="hidden" id="nom" value="<%=nombre%>"></td>
            </tr>
          </table>
            <table width="99%" border="1" align="center" bordercolor="#999999" bgcolor="#F7F5F4">
              <tr class="tblTitulo">
                <td width="101" align="center">Codigo</td>
                <td width="279" align="center">Nombre</td>
              </tr>
              <pg:pager
    items="<%=VecPais.size()%>"
    index="<%= index %>"
    maxPageItems="<%= maxPageItems %>"
    maxIndexPages="<%= maxIndexPages %>"
    isOffset="<%= true %>"
    export="offset,currentPageNumber=pageNumber"
    scope="request">
              <%-- keep track of preference --%>
              <%
      for (int i = offset.intValue(), l = Math.min(i + maxPageItems, VecPais.size()); i < l; i++)
	  {
          pais = (Pais) VecPais.elementAt(i);%>
              <pg:item>
              <tr class="<%=(i % 2 == 0 )?"filagris":"filaazul"%>" onMouseOver='cambiarColorMouse(this)'   style="cursor:hand"  
        onClick="window.open('<%=CONTROLLER%>?estado=Buscar&accion=Pais&pagina=pais.jsp&carpeta=jsp/trafico/pais&codigo=<%=pais.getCountry_code()%>&mensaje=' ,'','status=no,scrollbars=no,width=620,height=300,resizable=yes')">
                <td height="20" class="bordereporte" ><%=pais.getCountry_code()%></td>
                <td class="bordereporte" ><%=pais.getCountry_name()%></td>
              </tr>
              </pg:item>
              <%}%>
              <tr class="bordereporte">
      	 	<td colspan="2" align="center">
	    		<pg:index>
	      			<jsp:include page="../../../WEB-INF/jsp/googlesinimg.jsp" flush="true"/>        
      			</pg:index>
			</td>
	    </tr>
  		</pg:pager>
          </table></td>
      </tr>
    </table>
  </form>
  <%}
 else { %>

  <table border="2" align="center">
    <tr>
      <td><table width="410" height="40" border="1" align="center"  bordercolor="#F7F5F4" bgcolor="#FFFFFF">
          <tr>
            <td width="282" align="center" class="mensajes">Su b&uacute;squeda no arroj&oacute; resultados!</td>
            <td width="28" background="<%=BASEURL%>/images/cuadronaranja.JPG">&nbsp;</td>
            <td width="78">&nbsp;</td>
          </tr>
      </table></td>
    </tr>
  </table>

<%}%>
<br>
<table width="480" border="0" align="center">
   <tr>
     <td><img src="<%=BASEURL%>/images/botones/regresar.gif"  name="imgaceptar" onclick="window.location='<%=CONTROLLER%>?estado=Menu&accion=Cargar&pagina=BuscarPais.jsp&carpeta=/jsp/trafico/pais&titulo=Buscar Pais'" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" style="cursor:hand"></td>
   </tr>
 </table>
 
</body>
</html>
