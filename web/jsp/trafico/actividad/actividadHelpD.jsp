<%@ include file="/WEB-INF/InitModel.jsp"%>
<html>
<head>
<title>Descripcion Campos agregar planilla</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link href="../../../css/estilostsp.css" rel="stylesheet" type="text/css"> 
<link href="<%=BASEURL%>/css/estilostsp.css" rel="stylesheet" type="text/css"> 
</head>

<body>
<br>


<table width="696"  border="2" align="center">
  <tr>
    <td width="811" >
      <table width="100%" border="0" align="center">
        <tr  class="subtitulo">
          <td height="24" colspan="2"><div align="center">  REGISTRO DE ACTIVIDADES</div></td>
        </tr>
        <tr class="subtitulo1">
          <td colspan="2">Registro de Actividades y definici&oacute;n de campos</td>
        </tr>
        <tr>
          <td width="149" class="fila">C&oacute;digo Actividad</td>
          <td width="525"  class="ayudaHtmlTexto">Campo para digitar el c&oacute;digo de la actividad.</td>
        </tr>
        <tr>
          <td class="fila">Descripci&oacute;n corta</td>
          <td  class="ayudaHtmlTexto">Campo para digitar el nombre de la Actividad, pero una descripci&oacute;n corta.</td>
        </tr>
        <tr>
          <td class="fila">Descripci&oacute;n Larga</td>
          <td  class="ayudaHtmlTexto">Campo para digitar el nombre de la Actividad.</td>
        </tr>
        <tr>
          <td class="fila">Grupo email</td>
          <td  class="ayudaHtmlTexto">Campo de selecci&oacute;n para escoger el grupo al cual se les enviaran mail.</td>
        </tr>
        <tr>
          <td class="fila">Nombre</td>
          <td  class="ayudaHtmlTexto">Campo para digitar el nombre corto del campo.</td>
        </tr>
        <tr>
          <td class="fila">Descripci&oacute;n</td>
          <td  class="ayudaHtmlTexto">Campo para digitar el nombre del campo.</td>
        </tr>
        
      </table>
    </td>
  </tr>
</table>

</body>
</html>
