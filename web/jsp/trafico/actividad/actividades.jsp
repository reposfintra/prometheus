<!-- 
- Autor : Ing. Diogenes Bastidas Morales
- Date  : 25 de agosto de 2005
- Copyrigth Notice : Fintravalores S.A. S.A
-->
<%--
-@(#)
--Descripcion : Pagina JSP, que lista las actividades
--%>

<%-- Declaracion de librerias--%>
<%@page contentType="text/html"%>
<%@page session="true"%>
<%@page errorPage="../error/ErrorPage.jsp"%>
<%@page import="java.util.*"%>
<%@page import="com.tsp.operation.model.*"%>
<%@page import="com.tsp.util.*"%>
<%@page import="com.tsp.operation.model.beans.*, java.util.*"%>
<%@include file="/WEB-INF/InitModel.jsp"%>
<%@taglib uri="http://jsptags.com/tags/navigation/pager" prefix="pg" %>

<html>
<head>
<title>Listado Actividades</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link href="<%= BASEURL %>/css/estilostsp.css" rel="stylesheet" type="text/css">
<link href="../../../css/estilostsp.css" rel="stylesheet" type="text/css"> 
<script type='text/javascript' src="<%= BASEURL %>/js/reporte.js"></script>
<script type='text/javascript' src="<%= BASEURL %>/js/boton.js"></script>
</head>

<body>
<div id="capaSuperior" style="position:absolute; width:100%; height:100px; z-index:0; left: 0px; top: 0px;">
 <jsp:include page="/toptsp.jsp?encabezado=Actividades"/>
</div>
<div id="capaCentral" style="position:absolute; width:100%; height:87%; z-index:0; left: 0px; top: 100px; overflow: scroll;">
  <%String style = "simple";
    String position =  "bottom";
    String index =  "center";
    int maxPageItems = 10;
    int maxIndexPages = 10;

    Vector VecAct = model.actividadSvc.ObtVecActividad();
    Actividad act; 
	if ( VecAct.size() >0 ){ 
  %>
  <table width="545" border="2" align="center">
    <tr>
      <td><table width="100%"  border="0">
        <tr>
          <td width="48%" class="subtitulo1"><span class="subtitulos">Informaci&oacute;n</span></td>
          <td width="52%" class="barratitulo"><img src="<%=BASEURL%>/images/titulo.gif" width="32" height="20"></td>
        </tr>
      </table>
        <table width="99%" border="1" align="center" bordercolor="#999999" bgcolor="#F7F5F4">
        <tr class="tblTitulo" align="center">
          <td width="133" >Codigo</td>
          <td width="124" >Nombre </td>
          <td  >Descripci&oacute;n</td>
        </tr>
        <pg:pager
    items="<%=VecAct.size()%>"
    index="<%= index %>"
    maxPageItems="<%= maxPageItems %>"
    maxIndexPages="<%= maxIndexPages %>"
    isOffset="<%= true %>"
    export="offset,currentPageNumber=pageNumber"
    scope="request">
        <%-- keep track of preference --%>
        <%
        for (int i = offset.intValue(), l = Math.min(i + maxPageItems, VecAct.size()); i < l; i++) {
            act = (Actividad) VecAct.elementAt(i);%>
        <pg:item>
        <tr class="<%=(i % 2 == 0 )?"filagris":"filaazul"%>" onMouseOver='cambiarColorMouse(this)'   style="cursor:hand"  
        onClick="window.open('<%=CONTROLLER%>?estado=Actividad&accion=Buscar&pagina=actividad.jsp&carpeta=jsp/trafico/actividad&cod=<%=act.getCodActividad()%>&mensaje=' ,'','status=yes,scrollbars=no,width=650,height=650,resizable=yes');">
          <td height="20" class="bordereporte" ><%=act.getCodActividad()%></td>
          <td height="20" class="bordereporte"><%=act.getDescorta()%></td>
          <td height="20" class="bordereporte"><%=act.getDesLarga()%></td>
        </tr>
        </pg:item>
        <%}%>
        <tr class="bordereporte">
          <td colspan="3" align="center" valign="middle" >
            <div align="center"><pg:index>
              <jsp:include page="../../../WEB-INF/jsp/googlesinimg.jsp" flush="true"/>      
    </pg:index></div></td>
        </tr>
        </pg:pager>
      </table></td>
    </tr>
  </table>
  <%}
 else { %>
 <table border="2" align="center">
    <tr>
      <td><table width="410" height="40" border="1" align="center"  bordercolor="#F7F5F4" bgcolor="#FFFFFF">
          <tr>
            <td width="282" align="center" class="mensajes">Su b&uacute;squeda no arroj&oacute; resultados!</td>
            <td width="28" background="<%=BASEURL%>/images/cuadronaranja.JPG">&nbsp;</td>
            <td width="78">&nbsp;</td>
          </tr>
      </table></td>
    </tr>
  </table>
  <%}%>
  <br>
  <table width="545" border="0" align="center">
   <tr>
     <td><img src="<%=BASEURL%>/images/botones/regresar.gif"  name="imgaceptar" onclick="window.location='<%=CONTROLLER%>?estado=Idioma&accion=Cargar&pagina=buscarActividad.jsp&ruta=/jsp/trafico/actividad/'" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" style="cursor:hand"></td>
   </tr>
 </table>

</div>
</body>
</html>
