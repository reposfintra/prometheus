<%@ include file="/WEB-INF/InitModel.jsp"%>

<HTML>
<HEAD>
<TITLE></TITLE>
<META http-equiv=Content-Type content="text/html; charset=windows-1252">
<link href="../../../css/estilostsp.css" rel="stylesheet" type="text/css"> 
<link href="<%=BASEURL%>/css/estilostsp.css" rel="stylesheet" type="text/css"> 
<style type="text/css">
<!--
.Estilo2 {
	font-size: 16pt;
	color: #FF0000;
}
-->
</style>
</HEAD>
<BODY> 

        <table width="90%" border="0" align="center">
          <tr  class="subtitulo">
            <td height="20" align="center">MANUAL  BUSQUEDA DE ACTIVIDAD</td>
          </tr>
          <tr class="subtitulo1">
            <td>Busqueda de Actividades </td>
          </tr>
          <tr>
            <td  height="8"  class="ayudaHtmlTexto">Para realizar la buscar una actividad, le digita los par&aacute;metros solicitados por el filtro de b&uacute;squeda</td>
          </tr>
          <tr>
            <td height="18"  class="ayudaHtmlTexto" align="center"><div align="center"><img src="<%=BASEURL%>/images/ayuda/actividad/img4.JPG"></div></td>
          </tr>
          <tr>
            <td height="18"  class="ayudaHtmlTexto" align="center"><p>El sistema le muestra los resultados de la b&uacute;squedas, mostr&aacute;ndole el documento y el nombre corto y el nombre largo de la actividad.<br>
              para obtener detalles de alguna actividad presione clic sobre la actividad.              <br>
            </p>
            </td>
          </tr>
          <tr>
            <td height="18"  class="ayudaHtmlTexto" align="center"><div align="center"><img src="<%=BASEURL%>/images/ayuda/actividad/img5.JPG">
            </div></td>
          </tr>
          <tr>
            <td height="18"  class="ayudaHtmlTexto" align="center">El sistema muestra la informaci&oacute;n detallada de la actividad seleccionada, permitiendo la opci&oacute;n de modificarlo y / o anularlo.</td>
          </tr>
          <tr>
            <td height="18"  class="ayudaHtmlTexto" align="center"><div align="center"><img src="<%=BASEURL%>/images/ayuda/actividad/img6.JPG"></div></td>
          </tr>
          <tr>
            <td height="18"  class="ayudaHtmlTexto" align="center">Si presiona Modificar el sistema realiza las modificaciones correspondientes y muestra el siguiente mensaje</td>
          </tr>
          <tr>
            <td height="18"  class="ayudaHtmlTexto" align="center"><div align="center"><img src="<%=BASEURL%>/images/ayuda/actividad/img7.JPG" ></div></td>
          </tr>
          <tr>
            <td height="18"  class="ayudaHtmlTexto" align="center">Si presiona Anular el sistema anula el registro y muestra el siguiente mensaje:</td>
          </tr>
          <tr>
            <td height="18"  class="ayudaHtmlTexto" align="center"><div align="center"><img src="<%=BASEURL%>/images/ayuda/proveedor/imagen8.JPG"></div></td>
          </tr>
          <tr>
            <td height="18"  class="ayudaHtmlTexto" align="center">&nbsp;</td>
          </tr>
      </table>
</BODY>
</HTML>
