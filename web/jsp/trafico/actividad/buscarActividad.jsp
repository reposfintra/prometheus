<!-- 
- Autor : Ing. Diogenes Bastidas Morales
- Date  : 25 de agosto de 2005
- Copyrigth Notice : Fintravalores S.A. S.A
-->
<%--
-@(#)
--Descripcion : Pagina JSP, que permite captura los parametros de busqueda
--%>

<%-- Declaracion de librerias--%>
<%@page contentType="text/html"%>
<%@page session="true"%>
<%@page import="java.util.*"%>
<%@page import="com.tsp.operation.model.*"%>
<%@page import="com.tsp.operation.model.beans.*"%>
<%@page import="com.tsp.util.*"%>
<%@include file="/WEB-INF/InitModel.jsp"%>
<html>
<head>
<title>Buscar Actividad</title>
</head>
<link href="<%= BASEURL %>/css/estilostsp.css" rel='stylesheet'>
<link href="../../../css/estilostsp.css" rel="stylesheet" type="text/css">
<%String path = application.getRealPath(model.menuService.getRealUrl(request.getRequestURI()));
String datos[] = model.menuService.getContenidoMenu(BASEURL,request.getRequestURI(),path);%>
<script type='text/javascript' src="<%= BASEURL %>/js/boton.js"></script>
<body onResize="redimensionar()" onLoad="redimensionar()">
<div id="capaSuperior" style="position:absolute; width:100%; height:100px; z-index:0; left: 0px; top: 0px;">
 <jsp:include page="/toptsp.jsp?encabezado=Buscar Actividad"/>
</div>
<div id="capaCentral" style="position:absolute; width:100%; height:87%; z-index:0; left: 0px; top: 100px; overflow: scroll;">
<form name="forma" method="post" action="<%=CONTROLLER%>?estado=Actividad&accion=Busqueda" >
  <table width="51%" border="2" align="center">
    <tr>
      <td><table width="100%"  border="0">
        <tr>
          <td width="48%" class="subtitulo1">Datos de Busqueda </td>
          <td width="52%" class="barratitulo"><img src="<%=BASEURL%>/images/titulo.gif" width="32" height="20" align="left"><%=datos[0]%></td>
        </tr>
      </table>
        <table width="100%" class="tablaInferior">
        <tr class="fila">
          <td width="48%">Codigo Actividad</td>
          <td width="52%"><input name="codactividad" type="text" class="textbox" id="codactividad" onKeyPress="soloAlfa(event)" size="10" maxlength="6"></td>
        </tr>
        <tr class="fila">
          <td>Descripci&oacute;n </td>
          <td><input name="descorta" type="text" class="textbox" id="descorta" size="40" maxlength="30"></td>
        </tr>
        
      </table></td>
    </tr>
  </table>
  <br>
    <div align="center">
	    <img src="<%=BASEURL%>/images/botones/buscar.gif"  name="imgbuscar" onClick="forma.submit()" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" style="cursor:hand">&nbsp;
	    <img src="<%=BASEURL%>/images/botones/detalles.gif"  name="imgdetalles"  onMouseOver="botonOver(this);" onClick="window.location='<%=CONTROLLER%>?estado=Actividad&accion=Busqueda&codactividad=&descorta='" onMouseOut="botonOut(this);" style="cursor:hand"> &nbsp;
        <img src="<%=BASEURL%>/images/botones/salir.gif"  name="imgsalir"  onMouseOver="botonOver(this);" onClick="parent.close();" onMouseOut="botonOut(this);" style="cursor:hand">
    </div>
</form>
</div>
<%=datos[1]%>
</body>
</html>
