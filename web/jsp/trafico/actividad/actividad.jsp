<!-- 
- Autor : Ing. Diogenes Bastidas Morales
- Date  : 25 de agosto de 2005
- Copyrigth Notice : Fintravalores S.A. S.A
-->
<%--
-@(#)
--Descripcion : Pagina JSP, que maneja el ingreso estructuras de actividades
--%>         

<%-- Declaracion de librerias--%>
<%@page contentType="text/html"%>
<%@page session="true"%>
<%@page import="java.util.*"%>
<%@page import="com.tsp.operation.model.*"%>
<%@page import="com.tsp.operation.model.beans.*"%>
<%@page import="com.tsp.util.*"%>
<%@include file="/WEB-INF/InitModel.jsp"%>
<html>
<head>
<title>Actividad</title>
<link href="<%= BASEURL %>/css/estilostsp.css" rel='stylesheet'>
<link href="../../../css/estilostsp.css" rel="stylesheet" type="text/css">
</head>
<script type='text/javascript' src="<%= BASEURL %>/js/actividad.js"></script>
<script type='text/javascript' src="<%= BASEURL %>/js/general.js"></script>
<script type='text/javascript' src="<%= BASEURL %>/js/boton.js"></script>
<body onResize="redimensionar()" <%= ( request.getParameter("reload")!=null ) ?"onLoad='window.opener.location.reload();redimensionar();imgObligatorios();window.focus();'" : "onLoad='redimensionar();imgObligatorios();'"%>>
<%
String path = application.getRealPath(model.menuService.getRealUrl(request.getRequestURI()));
String datos[] = model.menuService.getContenidoMenu(BASEURL,request.getRequestURI(),path);
String msg = (request.getParameter("mensaje")!=null)?request.getParameter("mensaje"):"";
String res="";
String accion=CONTROLLER+"?estado=Actividad&accion=Ingresar";
String tit="Registrar";
System.out.println("Mensaje "+msg);
System.out.println("Mensaje "+accion);
Actividad act = null;
int sw=0;
if (msg.equals("MsgAgregado")){
	res = "Actividad ingresada exitosamente!";
}
else if (msg.equals("MsgErrorIng")){
	res = "Error...Actividad ya existe!";
}
else if (msg.equals("Modificar") || msg.equals("Modificado") ){
	sw=1;
    accion=CONTROLLER+"?estado=Actividad&accion=Modificar";
    act = model.actividadSvc.ObtActividad();
    tit = "Modificar";
    if (msg.equals("Modificado"))
          res = "Actividad modificada exitosamente!";
}
%>
<% String pagina = "/toptsp.jsp?encabezado="+tit+" Campos Actividad"; %>
<div id="capaSuperior" style="position:absolute; width:100%; height:100px; z-index:0; left: 0px; top: 0px;">
 <jsp:include page="<%=pagina%>"/>
</div>
<div id="capaCentral" style="position:absolute; width:100%; height:87%; z-index:0; left: 0px; top: 100px; overflow: scroll;">
<form name="forma" method="post" action="<%=accion%>" >
  <div align="center"></div>
  <table width="600" border="2" align="center">
    <tr>
      <td><table width="100%"  border="0">
        <tr>
          <td width="48%" class="subtitulo1"><span class="subtitulos">Informaci&oacute;n</span></td>
          <td width="52%" class="barratitulo"><img src="<%=BASEURL%>/images/titulo.gif" width="32" height="20" align="left" ><%=datos[0]%></td>
        </tr>
      </table>
        <table width="100%" class="tablaInferior">
        <tr>
          <td width="31%" class="fila"> Codigo Actividad </td>
          <td colspan="4" class="fila"><input name="codactividad"  type="text" class="textbox" id="codactividad" onKeyPress="soloAlfa(event)" value="<%=(sw==1)?act.getCodActividad():""%>" size="10" maxlength="6" <%if(sw==1){%>readonly <%}%>>
            <img src="<%=BASEURL%>/images/botones/iconos/obligatorio.gif" width="10" height="10"></td>
        </tr>
        <tr>
          <td class="fila"> Descripci&oacute;n </td>
          <td class="fila"align="center">Corta</td>
          <td class="fila"><input name="descorta" type="text" class="textbox" id="descorta" value="<%=(sw==1)?act.getDescorta():""%>" size="11" maxlength="10" onKeyPress="soloAlfa(event)"><img src="<%=BASEURL%>/images/botones/iconos/obligatorio.gif" width="10" height="10"></td>
          <td width="7%" class="fila"> Larga </td>
          <td width="40%" class="fila"><input name="deslarga" type="text" class="textbox" id="deslarga" value="<%=(sw==1)?act.getDesLarga():""%>" size="30" maxlength="30" onKeyPress="soloAlfa(event)"><img src="<%=BASEURL%>/images/botones/iconos/obligatorio.gif" width="10" height="10"></td>
        </tr>
        <tr class="subtitulo1">
          <td align="center">Campos</td>
          <td width="8%" ><div align="center">M</div></td>
          <td width="14%" >Nombre</td>
          <td colspan="2"><div align="center">Descripci&oacute;n</div></td>
        </tr>
        <tr class="fila">
          <td><table width="100%"  border="0" cellspacing="0" cellpadding="0">
              <tr class="fila">
                <td width="23%"><input type="checkbox" id="fecini" name="fecini" <%if(sw==1 && act.getFecinicio().equals("S")){%> checked <%}%> onClick="imgFecinicio();" ></td>
                <td width="77%">Fecha Inicio </td>
              </tr>
            </table>
        </td>
          <td align="center" ><input name="m_fecini" type="checkbox" id="m_fecini" <%if(sw==1 && act.getM_Fecinicio().equals("S")){%> checked <%}%>></td>
          <td ><input name="cortafecini" type="text" class="textbox" id="cortafecini" value="<%=(sw==1)?act.getCortaFecini():""%>" size="11" maxlength="10" onKeyPress="soloAlfa(event)"><img id="imgfecini" name="imgfecini" src="<%=BASEURL%>/images/botones/iconos/obligatorio.gif" width="10" height="10" style="visibility:hidden"></td>
          <td colspan="2"><input name="largafecini" type="text" class="textbox" id="largafecini" value="<%=(sw==1)?act.getLargaFecini():""%>" size="40" maxlength="30" onKeyPress="soloAlfa(event)">
            <img id="imgfecini1" name="imgfecini1" src="<%=BASEURL%>/images/botones/iconos/obligatorio.gif" width="10" height="10" style="visibility:hidden"></td>
        </tr>
        <tr class="fila">
          <td><table width="100%"  border="0" cellspacing="0" cellpadding="0">
              <tr class="fila">
                <td width="23%"><input name="fecfin" type="checkbox" id="fecfin" <%if(sw==1 && act.getFecfinal().equals("S")){%> checked <%}%> onClick="imgFecfin();" ></td>
                <td width="77%">Fecha Final </td>
              </tr>
            </table>
          </td>
          <td align="center">
            <input name="m_fecfin" type="checkbox" id="m_fecfin2" <%if(sw==1 && act.getM_Fecfin().equals("S")){%> checked <%}%>>
          </td>
          <td ><input name="cortafecfin" type="text" class="textbox" id="cortafecfin" value="<%=(sw==1)?act.getCortaFecfin():""%>" size="11" maxlength="10" onKeyPress="soloAlfa(event)"><img id="imgfecfin" name="imgfecfin" src="<%=BASEURL%>/images/botones/iconos/obligatorio.gif" width="10" height="10" style="visibility:hidden"></td>
          <td colspan="2"><input name="largafecfin" type="text" class="textbox" id="largafecfin" value="<%=(sw==1)?act.getLargaFecfin():""%>" size="40" maxlength="30" onKeyPress="soloAlfa(event)">
            <img id="imgfecfin1" name="imgfecfin1" src="<%=BASEURL%>/images/botones/iconos/obligatorio.gif" width="10" height="10" style="visibility:hidden"></td>
        </tr>
        <tr class="fila">
          <td ><table width="100%"  border="0" cellspacing="0" cellpadding="0">
              <tr class="fila">
                <td width="23%"><input name="duracion" type="checkbox" id="duracion" <%if(sw==1 && act.getDuracion().equals("S")){%> checked <%}%> onClick="imgDur();"></td>
                <td width="77%"> Duraci&oacute;n </td>
              </tr>
            </table>
              </td>
          <td align="center" ><input name="m_duracion" type="checkbox" id="m_duracion" <%if(sw==1 && act.getM_Duracion().equals("S")){%> checked <%}%>></td>
          <td ><input name="cortaduracion" type="text" class="textbox" id="cortaduracion" value="<%=(sw==1)?act.getCortaDuracion():""%>" size="11" maxlength="10" onKeyPress="soloAlfa(event)"><img id="imgdur" name="imgfecfin" src="<%=BASEURL%>/images/botones/iconos/obligatorio.gif" width="10" height="10" style="visibility:hidden"></td>
          <td colspan="2"><input name="largaduracion" type="text" class="textbox" id="largaduracion" value="<%=(sw==1)?act.getLargaDuracion():""%>" size="40" maxlength="30" onKeyPress="soloAlfa(event)">
            <img id="imgdur1" name="imgdur1" src="<%=BASEURL%>/images/botones/iconos/obligatorio.gif" width="10" height="10" style="visibility:hidden"></td>
        </tr>
        <tr class="fila">
          <td><table width="100%"  border="0" cellspacing="0" cellpadding="0">
              <tr class="fila">
                <td width="23%" ><input name="documento" type="checkbox" id="documento" <%if(sw==1 && act.getDocumento().equals("S")){%> checked <%}%> onClick="imgDoc();"></td>
                <td width="77%"> Documento</td>
              </tr>
            </table>
              <div align="center"> </div></td>
          <td align="center"><input name="m_documento" type="checkbox" id="m_documento2" <%if(sw==1 && act.getM_Documento().equals("S")){%> checked <%}%>></td>
          <td ><input name="cortadocumento" type="text" class="textbox" id="cortadocumento" value="<%=(sw==1)?act.getCortaDocumento():""%>" size="11" maxlength="10" onKeyPress="soloAlfa(event)"><img id="imgdoc" name="imgfecfin" src="<%=BASEURL%>/images/botones/iconos/obligatorio.gif" width="10" height="10" style="visibility:hidden"></td>
          <td colspan="2"><input name="largadocumento" type="text" class="textbox" id="largadocumento" value="<%=(sw==1)?act.getLargaDocumento():""%>" size="40" maxlength="30" onKeyPress="soloAlfa(event)">
            <img id="imgdoc1" name="imgdoc1" src="<%=BASEURL%>/images/botones/iconos/obligatorio.gif" width="10" height="10" style="visibility:hidden"></td>
        </tr>
        <tr class="fila">
          <td><table width="100%"  border="0" cellspacing="0" cellpadding="0">
              <tr class="fila">
                <td width="19%" ><input name="tiempodemora" type="checkbox" id="tiempodemora" <%if(sw==1 && act.getTiempodemora().equals("S")){%> checked <%}%> onClick="imgTiempo();"></td>
                <td width="81%">Tiempo Demora</td>
              </tr>
            </table>
              </td>
          <td align="center"><input name="m_tiempodemora" type="checkbox" id="m_tiempodemora" <%if(sw==1 && act.getM_Tiempodemora().equals("S")){%> checked <%}%>></td>
          <td ><input name="cortatiempodemora" type="text" class="textbox" id="cortatiempodemora" value="<%=(sw==1)?act.getCortaTiempodemora():""%>" size="11" maxlength="10" onKeyPress="soloAlfa(event)"><img id="imgtiempo" name="imgtiempo" src="<%=BASEURL%>/images/botones/iconos/obligatorio.gif" width="10" height="10" style="visibility:hidden"></td>
          <td colspan="2"><input name="largatiempodemora" type="text" class="textbox" id="largatiempodemora" value="<%=(sw==1)?act.getLargaTiempodemora():""%>" size="40" maxlength="30" onKeyPress="soloAlfa(event)">
            <img id="imgtiempo1" name="imgtiempo1" src="<%=BASEURL%>/images/botones/iconos/obligatorio.gif" width="10" height="10" style="visibility:hidden"></td>
        </tr>
        <tr class="fila">
          <td ><table width="100%"  border="0" cellspacing="0" cellpadding="0">
              <tr class="fila">
                <td width="20%" height="19"><input name="causademora" type="checkbox" id="causademora" <%if(sw==1 && act.getCausademora().equals("S")){%> checked <%}%> onClick="imgCausa();"></td>
                <td width="80%">Causa Demora</td>
              </tr>
            </table>
              <div align="center"> </div></td>
          <td align="center"><input name="m_causademora" type="checkbox" id="m_causademora" <%if(sw==1 && act.getM_Causademora().equals("S")){%> checked <%}%>></td>
          <td ><input name="cortacausademora" type="text" class="textbox" id="cortacausademora" value="<%=(sw==1)?act.getCortaCausademora():""%>" size="11" maxlength="10" onKeyPress="soloAlfa(event)"><img id="imgcausa" name="imgcausa" src="<%=BASEURL%>/images/botones/iconos/obligatorio.gif" width="10" height="10" style="visibility:hidden"></td>
          <td colspan="2"><input name="largacausademora" type="text" class="textbox" id="largacausademora" value="<%=(sw==1)?act.getLargaCausademora():""%>" size="40" maxlength="30" onKeyPress="soloAlfa(event)">
            <img id="imgcausa1" name="imgcausa1" src="<%=BASEURL%>/images/botones/iconos/obligatorio.gif" width="10" height="10" style="visibility:hidden"></td>
        </tr>
        <tr class="fila">
          <td><table width="100%"  border="0" cellspacing="0" cellpadding="0">
              <tr class="fila">
                <td width="19%" ><input name="resdemora" type="checkbox" id="resdemora" <%if(sw==1 && act.getResdemora().equals("S")){%> checked <%}%> onClick="imgResp();"></td>
                <td width="81%">Responsable Demora</td>
              </tr>
            </table>
              <div align="center"> </div></td>
          <td align="center" ><input name="m_resdemora" type="checkbox" id="m_resdemora" <%if(sw==1 && act.getM_Resdemora().equals("S")){%> checked <%}%>></td>
          <td ><input name="cortaresdemora" type="text" class="textbox" id="cortaresdemora" value="<%=(sw==1)?act.getCortaResdemora():""%>" size="11" maxlength="10" onKeyPress="soloAlfa(event)"><img id="imgres" name="imgres" src="<%=BASEURL%>/images/botones/iconos/obligatorio.gif" width="10" height="10" style="visibility:hidden"></td>
          <td colspan="2"><input name="largaresdemora" type="text" class="textbox" id="largaresdemora" value="<%=(sw==1)?act.getLargaResdemora():""%>" size="40" maxlength="30" onKeyPress="soloAlfa(event)">
            <img id="imgres1" name="imgres1" src="<%=BASEURL%>/images/botones/iconos/obligatorio.gif" width="10" height="10" style="visibility:hidden"></td>
        </tr>
        <tr class="fila">
          <td><table width="100%"  border="0" cellspacing="0" cellpadding="0">
              <tr class="fila">
                <td width="19%" ><input name="feccierre" type="checkbox" id="feccierre" checked ></td>
                <td width="81%">Fecha Cierre</td>
              </tr>
            </table>
              <div align="center"> </div></td>
          <td align="center" ><input name="m_feccierre" type="checkbox" id="m_feccierre" <%if(sw==1 && act.getM_Feccierre().equals("S")){%> checked <%}%>></td>
          <td ><input name="cortafeccierre" type="text" class="textbox" id="cortafeccierre" value="<%=(sw==1)?act.getCortaFeccierre():""%>" size="11" maxlength="10" onKeyPress="soloAlfa(event)"><img src="<%=BASEURL%>/images/botones/iconos/obligatorio.gif" width="10" height="10"></td>
          <td colspan="2"><input name="largafeccierre" type="text" class="textbox" id="largafeccierre" value="<%=(sw==1)?act.getLargaFeccierre():""%>" size="40" maxlength="30" onKeyPress="soloAlfa(event)">
            <img src="<%=BASEURL%>/images/botones/iconos/obligatorio.gif" width="10" height="10"></td>
        </tr>
		<tr class="fila">
          <td ><table width="100%"  border="0" cellspacing="0" cellpadding="0">
              <tr class="fila">
                <td width="19%"><input name="cantrealizada" type="checkbox" id="cantrealizada" <%if(sw==1 && act.getCantrealizada().equals("S")){%> checked <%}%> onClick="imgReal();"></td>
                <td width="81%">Cant. Realizada</td>
              </tr>
            </table>
              </td>
          <td align="center" ><input name="m_cantrealizada" type="checkbox" id="m_cantrealizada" <%if(sw==1 && act.getM_cantrealizada().equals("S")){%> checked <%}%>></td>
          <td ><input name="cortacantrealizada" type="text" class="textbox" id="cortacantrealizada" value="<%=(sw==1)?act.getCorta_cantrealizada():""%>" size="11" maxlength="10" onKeyPress="soloAlfa(event)"><img id="imgrea" name="imgrea" src="<%=BASEURL%>/images/botones/iconos/obligatorio.gif" width="10" height="10" style="visibility:hidden"></td>
          <td colspan="2"><input name="largacantrealizada" type="text" class="textbox" id="largacantrealizada" value="<%=(sw==1)?act.getLarga_cantrealizada():""%>" size="40" maxlength="30" onKeyPress="soloAlfa(event)">
            <img id="imgrea1" name="imgrea1" src="<%=BASEURL%>/images/botones/iconos/obligatorio.gif" width="10" height="10" style="visibility:hidden"> </td>
        </tr>
		<tr class="fila">
          <td><table width="100%"  border="0" cellspacing="0" cellpadding="0">
              <tr class="fila">
                <td width="19%" ><input name="cantplaneada" type="checkbox" id="cantplaneada" <%if(sw==1 && act.getCantplaneada().equals("S")){%> checked <%}%> onClick="imgPlan();" ></td>
                <td width="81%">Cant. Planeada</td>
              </tr>
            </table>
              <div align="center"> </div></td>
          <td align="center" ><input name="m_cantplaneada" type="checkbox" id="m_cantplaneada" <%if(sw==1 && act.getM_cantplaneada().equals("S")){%> checked <%}%>></td>
          <td ><input name="cortacantplaneada" type="text" class="textbox" id="cortacantplaneada" value="<%=(sw==1)?act.getCorta_cantplaneada():""%>" size="11" maxlength="10" onKeyPress="soloAlfa(event)"><img id="imgplan" name="imgplan" src="<%=BASEURL%>/images/botones/iconos/obligatorio.gif" width="10" height="10" style="visibility:hidden"></td>
          <td colspan="2"><input name="largacantplaneada" type="text" class="textbox" id="largacantplaneada" value="<%=(sw==1)?act.getLarga_cantplaneada():""%>" size="40" maxlength="30" onKeyPress="soloAlfa(event)">
            <img id="imgplan1" name="imgplan1" src="<%=BASEURL%>/images/botones/iconos/obligatorio.gif" width="10" height="10" style="visibility:hidden"> </td>
        </tr>
		
		
        <tr class="fila">
          <td ><table width="100%"  border="0" cellspacing="0" cellpadding="0">
              <tr class="fila">
                <td width="18%" ><input name="observacion" type="checkbox" id="observacion" <%if(sw==1 && act.getObservacion().equals("S")){%> checked <%}%> onClick="imgObs();"></td>
                <td width="82%">Observaci&oacute;n</td>
              </tr>
            </table>
          </td>
          <td align="center"><input name="m_observacion" type="checkbox" id="m_observacion" <%if(sw==1 && act.getM_Observacion().equals("S")){%> checked <%}%>></td>
          <td ><input name="cortaobservacion" type="text" class="textbox" id="cortaobservacion" value="<%=(sw==1)?act.getCortaObservacion():""%>" size="11" maxlength="10" onKeyPress="soloAlfa(event)"><img id="imgobs" name="imgobs" src="<%=BASEURL%>/images/botones/iconos/obligatorio.gif" width="10" height="10" style="visibility:hidden"></td>
          <td colspan="2"><input name="largaobservacion" type="text" class="textbox" id="largaobservacion" value="<%=(sw==1)?act.getLargaObservacion():""%>" size="40" maxlength="30" onKeyPress="soloAlfa(event)">
            <img id="imgobs1" name="imgobs1" src="<%=BASEURL%>/images/botones/iconos/obligatorio.gif" width="10" height="10" style="visibility:hidden"></td>
        </tr>
		<tr class="fila">
          <td ><table width="100%"  border="0" cellspacing="0" cellpadding="0">
              <tr class="fila">
                <td width="18%" ><input name="referencia1" type="checkbox" id="referencia1" <%if(sw==1 && act.getReferencia1().equals("S")){%> checked <%}%> onClick="imgRef1();"></td>
                <td width="82%">Referencia 1 </td>
              </tr>
            </table>
          </td>
          <td align="center"><input name="m_referencia1" type="checkbox" id="m_referencia1" <%if(sw==1 && act.getM_referencia1().equals("S")){%> checked <%}%>></td>
          <td ><input name="cortareferencia1" type="text" class="textbox" id="cortareferencia1" value="<%=(sw==1)?act.getCorta_referencia1():""%>" size="11" maxlength="10" onKeyPress="soloAlfa(event)"><img id="imgref1" name="imgref1" src="<%=BASEURL%>/images/botones/iconos/obligatorio.gif" width="10" height="10" style="visibility:hidden"></td>
          <td colspan="2"><input name="largareferencia1" type="text" class="textbox" id="largareferencia1" value="<%=(sw==1)?act.getLarga_referencia1():""%>" size="40" maxlength="30" onKeyPress="soloAlfa(event)">
            <img id="imgref11" name="imgref11" src="<%=BASEURL%>/images/botones/iconos/obligatorio.gif" width="10" height="10" style="visibility:hidden"></td>
        </tr>
		<tr class="fila">
          <td ><table width="100%"  border="0" cellspacing="0" cellpadding="0">
              <tr class="fila">
                <td width="18%" ><input name="referencia2" type="checkbox" id="referencia2" <%if(sw==1 && act.getReferencia2().equals("S")){%> checked <%}%> onClick="imgRef2();"></td>
                <td width="82%">Referencia 2 </td>
              </tr>
            </table>
          </td>
          <td align="center"><input name="m_referencia2" type="checkbox" id="m_referencia2" <%if(sw==1 && act.getM_referencia2().equals("S")){%> checked <%}%>></td>
          <td ><input name="cortareferencia2" type="text" class="textbox" id="cortareferencia2" value="<%=(sw==1)?act.getCorta_referencia2():""%>" size="11" maxlength="10" ><img id="imgref2" name="imgref2" src="<%=BASEURL%>/images/botones/iconos/obligatorio.gif" width="10" height="10" style="visibility:hidden"></td>
          <td colspan="2"><input name="largareferencia2" type="text" class="textbox" id="largareferencia2" value="<%=(sw==1)?act.getLarga_referencia2():""%>" size="40" maxlength="30" onKeyPress="soloAlfa(event)">
            <img id="imgref21" name="imgref21" src="<%=BASEURL%>/images/botones/iconos/obligatorio.gif" width="10" height="10" style="visibility:hidden"></td>
        </tr>
		<tr class="fila">
          <td ><table width="100%"  border="0" cellspacing="0" cellpadding="0">
              <tr class="fila">
                <td width="18%" ><input name="refnumerica1" type="checkbox" id="refnumerica1" <%if(sw==1 && act.getRefnumerica1().equals("S")){%> checked <%}%> onClick="imgRefnum1();"></td>
                <td width="82%">Referencia Numerica 1 </td>
              </tr>
            </table>
          </td>
          <td align="center"><input name="m_refnumerica1" type="checkbox" id="m_refnumerica1" <%if(sw==1 && act.getM_refnumerica1().equals("S")){%> checked <%}%>></td>
          <td ><input name="cortarefnumerica1" type="text" class="textbox" id="cortarefnumerica1" value="<%=(sw==1)?act.getCorta_refnumerica1():""%>" size="11" maxlength="10" onKeyPress="soloAlfa(event)"><img id="imgrefnum1" name="imgrefnum1" src="<%=BASEURL%>/images/botones/iconos/obligatorio.gif" width="10" height="10" style="visibility:hidden"></td>
          <td colspan="2"><input name="largarefnumerica1" type="text" class="textbox" id="largarefnumerica1" value="<%=(sw==1)?act.getLarga_refnumerica1():""%>" size="40" maxlength="30" onKeyPress="soloAlfa(event)">
            <img id="imgrefnum11" name="imgrefnum11" src="<%=BASEURL%>/images/botones/iconos/obligatorio.gif" width="10" height="10" style="visibility:hidden"></td>
        </tr>
		<tr class="fila">
          <td ><table width="100%"  border="0" cellspacing="0" cellpadding="0">
              <tr class="fila">
                <td width="18%" ><input name="refnumerica2" type="checkbox" id="refnumerica2" <%if(sw==1 && act.getRefnumerica2().equals("S")){%> checked <%}%> onClick="imgRefnum2();"></td>
                <td width="82%">Referencia Numerica 2 </td>
              </tr>
            </table>
          </td>
          <td align="center"><input name="m_refnumerica2" type="checkbox" id="m_refnumerica2" <%if(sw==1 && act.getM_refnumerica2().equals("S")){%> checked <%}%>></td>
          <td ><input name="cortarefnumerica2" type="text" class="textbox" id="cortarefnumerica2" value="<%=(sw==1)?act.getCorta_refnumerica2():""%>" size="11" maxlength="10" onKeyPress="soloAlfa(event)"><img id="imgrefnum2" name="imgrefnum2" src="<%=BASEURL%>/images/botones/iconos/obligatorio.gif" width="10" height="10" style="visibility:hidden"></td>
          <td colspan="2"><input name="largarefnumerica2" type="text" class="textbox" id="largarefnumerica2" value="<%=(sw==1)?act.getLarga_refnumerica2():""%>" size="40" maxlength="30" onKeyPress="soloAlfa(event)">
            <img id="imgrefnum21" name="imgrefnum21" src="<%=BASEURL%>/images/botones/iconos/obligatorio.gif" width="10" height="10" style="visibility:hidden"></td>
        </tr>
        <tr class="fila">
          <td colspan="2" > Manejador de Eventos </td>
          <td colspan="3" ><input name="m_evento" type="text" class="textbox" id="m_evento2" value="<%=(sw==1)?act.getMane_evento():""%>" size="60" maxlength="50" onKeyPress="soloAlfa(event)"></td>
        </tr>
		<tr class="fila">
          <td colspan="2" > Grupo Email </td>
          <td colspan="3" ><% LinkedList list = model.tablaGenService.obtenerTablas();%>
					 <select name="perfil" class="textbox" id="perfil" style="width:90%">
                       <option value=''>Seleccione</option>
                       <%for (int i = 0; i < list.size(); i++){
                       		TablaGen info = (TablaGen) list.get(i);
                            if (info.getTable_code().equals((sw==1)?act.getPerfil():"") ){%>
                              <option value="<%=info.getTable_code()%>" selected><%=info.getTable_code()%></option>
                            <%}else{%>                                                    
                              <option value="<%=info.getTable_code()%>"><%=info.getTable_code()%></option>
                            <%}%>
                        <%}%>
                </select></td>
		</tr>
		<tr class="fila">
		  <td colspan="5" ><input name="linkimg" type="checkbox" id="linkimg" <%if(sw==1 && act.getLink().equals("S")){%> checked <%}%>>
		    Link Imagen </td>
		  </tr>
        
      </table></td>
    </tr>
  </table>
     <p>
      <% if(sw==0){%>
	       <div align="center">
			   <img src="<%=BASEURL%>/images/botones/aceptar.gif"  name="imgaceptar" onClick="validarActividad();" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" style="cursor:hand">&nbsp;
			   <img src="<%=BASEURL%>/images/botones/cancelar.gif"  name="imgcancelar"  onMouseOver="botonOver(this);" onClick="forma.reset();" onMouseOut="botonOut(this);" style="cursor:hand"> &nbsp;
               <img src="<%=BASEURL%>/images/botones/salir.gif"  name="imgsalir"  onMouseOver="botonOver(this);" onClick="parent.close();" onMouseOut="botonOut(this);" style="cursor:hand">
           </div>
		  <%} else {%>
		  	<div align="center">
			  <img src="<%=BASEURL%>/images/botones/modificar.gif"  name="imgmodificar" onClick="validarActividad();" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" style="cursor:hand">&nbsp;
              <img src="<%=BASEURL%>/images/botones/anular.gif"  name="imganular" onClick="window.location='<%=CONTROLLER%>?estado=Actividad&accion=Anular&codigo=<%=act.getCodActividad()%>'" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" style="cursor:hand">&nbsp;
              <img src="<%=BASEURL%>/images/botones/salir.gif"  name="imgsalir"  onMouseOver="botonOver(this);" onClick="parent.close();" onMouseOut="botonOut(this);" style="cursor:hand">
          </div>
		  <%}%>
    </p>

  <%if( !res.equalsIgnoreCase("")){%>
  <p><table border="2" align="center">
  <tr>
    <td><table width="100%" border="1" align="center"  bordercolor="#F7F5F4" bgcolor="#FFFFFF">
      <tr>
        <td width="262" align="center" class="mensajes"><%=res%></td>
        <td width="32" background="<%=BASEURL%>/images/cuadronaranja.JPG">&nbsp;</td>
        <td width="44">&nbsp;</td>
      </tr>
    </table></td>
  </tr>
</table>
<%}%>
</form>
</div>
<%=datos[1]%>
</body>
</html>
