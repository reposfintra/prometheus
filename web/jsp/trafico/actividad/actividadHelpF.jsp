<%@ include file="/WEB-INF/InitModel.jsp"%>

<HTML>
<HEAD>
<TITLE></TITLE>
<META http-equiv=Content-Type content="text/html; charset=windows-1252">
<link href="../../../css/estilostsp.css" rel="stylesheet" type="text/css"> 
<link href="<%=BASEURL%>/css/estilostsp.css" rel="stylesheet" type="text/css"> 
<style type="text/css">
<!--
.Estilo2 {
	font-size: 16pt;
	color: #FF0000;
}
-->
</style>
</HEAD>
<BODY> 

        <table width="90%" border="0" align="center">
          <tr  class="subtitulo">
            <td height="20" align="center">MANUAL DE REGISTRO DE ACTIVIDADES</td>
          </tr>
          <tr class="subtitulo1">
            <td> Registro de Actividades y definici&oacute;n de campos</td>
          </tr>
          <tr>
            <td  height="8"  class="ayudaHtmlTexto">El registro de campos actividad, le permite personalizar el formulario de captura de los datos correspondientes a la actividad, que esta creando.<br>Para crear el registro el formulario le muestra una columna campos, donde encuentra los campos posibles que puede tener una actividad, al lado de cada campo encontramos unos Checkbox, que le permiten decidir si es necesario ese campo para la actividad.</td>
          </tr>
          <tr>
            <td height="18"  class="ayudaHtmlTexto" align="center"><div align="center"><img src="<%=BASEURL%>/images/ayuda/actividad/img1.JPG" ></div></td>
          </tr>
          <tr>
            <td height="18"  class="ayudaHtmlTexto" align="center"><p>Seleccione el campo que desea registrar, escriba un nombre corto y un nombre largo para ese campo como lo muestra el ejemplo<br>
            </p>
            </td>
          </tr>
          <tr>
            <td height="18"  class="ayudaHtmlTexto" align="center"><div align="center"><img src="<%=BASEURL%>/images/ayuda/actividad/img2.JPG">
            </div></td>
          </tr>
          <tr>
            <td height="18"  class="ayudaHtmlTexto" align="center">Luego de haber seleccionado los campos que requiere la actividad, con sus respectivos nombres cortos y largos, y seleccionado los campos Mandatarios, procese a presionar el bot&oacute;n Aceptar.<br>
            Nota: la columna M que existe en el formulario, es para identificar los datos obligatorios que deben digitar al momento de ingresar los datos de la actividad</td>
          </tr>
          <tr>
            <td height="18"  class="ayudaHtmlTexto" align="center"><div align="center"><img src="<%=BASEURL%>/images/ayuda/actividad/img3.JPG" ></div></td>
          </tr>
          <tr>
            <td height="18"  class="ayudaHtmlTexto" align="center">&nbsp;</td>
          </tr>
      </table>
</BODY>
</HTML>
