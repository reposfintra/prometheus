<!--  
	 - Author(s)       :      LREALES
	 - Description     :      AYUDA FUNCIONAL - Información de la Actividad
	 - Date            :      05/08/2006 
	 - Copyright Notice:      TRANSPORTES SANCHEZ POLO S.A.
-->
<%@ include file="/WEB-INF/InitModel.jsp"%>
<HTML>
<HEAD>
<TITLE>AYUDA FUNCIONAL - Informacion de la Actividad</TITLE>
<META http-equiv=Content-Type content="text/html; charset=windows-1252">
<link href="../css/estilostsp.css" rel="stylesheet" type="text/css"> 
<link href="<%=BASEURL%>//css/estilostsp.css" rel="stylesheet" type="text/css">
<script type='text/javascript' src="<%= BASEURL %>/js/boton.js"></script> 
</HEAD>
<BODY> 


        <table width="100%" border="0" align="center">
          <tr  class="subtitulo">
            <td height="20"><div align="center">MANUAL DE INGRESO DE INFORMACIÓN A LA ACTIVIDAD </div></td>
          </tr>
		  
          <tr class="subtitulo1">
            <td>Para llenar la información de la actividad, el sistema le muestra el siguiente formulario.</td>
          </tr>
          <tr>
            <td  class="ayudaHtmlTexto"><div align="center">
              <p><img src="<%=BASEURL%>/images/ayuda/actividad/infoActividad/imag1.jpg"></p>
              </div></td>
          </tr>
		  
          <tr>
            <td  class="ayudaHtmlTexto"><p>Digite la información que solicita el formulario, luego presione aceptar y el sistema le muestra el siguiente mensaje </p>            </td>
          </tr>
          <tr>
            <td  class="ayudaHtmlTexto"><div align="center"><img src="<%=BASEURL%>/images/ayuda/actividad/infoActividad/imag2.jpg"></div></td>
          </tr>
          <tr>
            <td  class="ayudaHtmlTexto">Para Realizar cualquier modificación y/o anulación. </td>
          </tr>
          <tr>
            <td  class="ayudaHtmlTexto"><div align="center"><img src="<%=BASEURL%>/images/ayuda/actividad/infoActividad/imag3.jpg" ></div></td>
          </tr>
          <tr>
            <td  class="ayudaHtmlTexto">Si realiza alguna modificación, el sistema le muestra el siguiente mensaje  </td>
          </tr>
          <tr>
            <td  class="ayudaHtmlTexto"><div align="center"><img src="<%=BASEURL%>/images/ayuda/actividad/infoActividad/imag4.jpg" ></div></td>
          </tr>
      </table>
  <p></p>
<center>
<img src = "<%=BASEURL%>//images/botones/salir.gif" style = "cursor:hand" name = "imgsalir" onClick = "parent.close();" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);">
</center>
</BODY>
</HTML>
