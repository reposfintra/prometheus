<!--  
- Autor : Ing. Diogenes Bastidas Morales
- Date  : 2 de septiembre de 2005
- Copyrigth Notice : Fintravalores S.A. S.A
-->
<%--
-@(#)
--Descripcion : Pagina JSP, captura los los datos para insertar, modificar la informacion de la actividad 
--%>

<%-- Declaracion de librerias--%>
<%@page contentType="text/html"%>
<%@page session="true"%>
<%@page import="java.util.*"%>
<%@page errorPage="/error/ErrorPage.jsp"%>
<%@page import="com.tsp.operation.model.*"%>
<%@page import="com.tsp.operation.model.beans.*"%>
<%@page import="com.tsp.util.*"%>
<%@include file="/WEB-INF/InitModel.jsp"%>
<html>
<head>
<title>Actividad</title>
</head>
<link href="<%= BASEURL %>/css/estilostsp.css" rel='stylesheet'>
<link href="../../../../css/estilostsp.css" rel="stylesheet" type="text/css">
<script type='text/javascript' src="<%= BASEURL %>/js/validar.js"></script>
<script type='text/javascript' src="<%= BASEURL %>/js/boton.js"></script>
 
<body>
<div id="capaSuperior" style="position:absolute; width:100%; height:100px; z-index:0; left: 0px; top: 0px;">
  <jsp:include page="/toptsp.jsp?encabezado=Llenar Actividad"/>
</div>
<div id="capaCentral" style="position:absolute; width:100%; height:87%; z-index:0; left: 0px; top: 100px; overflow: scroll;">
<%
String path = application.getRealPath(model.menuService.getRealUrl(request.getRequestURI()));
String datos[] = model.menuService.getContenidoMenu(BASEURL,request.getRequestURI(),path);

//capturo la informacion planilla y el cliente

Planilla pla = model.clientactSvc.getPla();
Actividad act = model.actividadSvc.ObtActividad();
String msg = (request.getParameter("men")!=null)?request.getParameter("men"):"";
String existeact = (request.getParameter("act")!=null)?request.getParameter("act"):"";
String ult_fec = (request.getParameter("ult_fec")!=null)?request.getParameter("ult_fec"):"";
String nom="",res="",cod="",fecini="",fecfin="",doc="",cau="",resde="",obs="",tiempo="",dur="",ac="",cantr="",cantp="",ref1="",ref2="";
int refnum1=0,refnum2=0;
String accion  = "Ingresar";
String fecc=Util.fechaActualTIMESTAMP(), tienefec = "N";
if(existeact.equals("true")){
     accion  = "Modificar";
	 InfoActividad infact = model.infoactService.obtInfoActividad();
	 fecini=infact.getFecini();
     fecfin= infact.getFecfin();
     dur = ""+infact.getDuracion();
     doc = infact.getDocumento();
     tiempo= ""+infact.getTiempoDemora();
     cau=infact.getCausaDemora();
     resde=infact.getResdemora();
     fecc=infact.getFeccierre();
	 cantr = infact.getCantrealizada();
	 cantp = infact.getCantplaneada();
     obs=infact.getObservacion();
	 ref1 = infact.getReferencia1();
	 ref2 = infact.getReferencia2();
	 refnum1 =   infact.getRefnumerica1();
	 refnum2 =   infact.getRefnumerica2();
	 tienefec = infact.getFeccierre().trim().equals("")?"N":"S"; 
}

if (msg.equals("agregado")){
	res = "Informacion ingresada exitosamente!";
}
else if (msg.equals("Error")){
	res = "Error...Actividad ya existe!";
}
if (msg.equals("Modificado")){
          res = "Información modificada exitosamente!";
}
%>

<form name="forma" method="post" action="<%=CONTROLLER%>?estado=Infoact&accion=<%=accion%>&carpeta=jsp/trafico/actividad/Infoactividad&pagina=DatosActividad.jsp" >
  <table width="580" border="2" align="center">
    <tr>
      <td><table width="100%" border="0" align="center">
        <tr>
          <td><table width="100%"  border="0">
              <tr>
                <td width="48%" class="subtitulo1">Informaci&oacute;n de la Planilla</td>
                <td width="52%" class="barratitulo"><img src="<%=BASEURL%>/images/titulo.gif" width="32" height="20" align="left"><%=datos[0]%></td>
              </tr>
            </table>
              <table width="100%" class="tablaInferior">
                <tr >
                  <td width="114" class="fila" nowrap >Planilla Nro.</td>
                  <td width="121" class="letra"><%=pla.getNumpla()%>
                    <input name="numpla" type="hidden" id="numpla" value="<%=pla.getNumpla()%>"></td>
                  <td width="106" class="fila">Fecha </td>
                  <td width="121" class="letra" ><%=pla.getFecpla()%></td>
                </tr>
                <tr>
                  <td  class="fila">Origen</td>
                  <td class="letra"><%=pla.getOrinom()%><span class="barratitulo"><span class="subtitulos">
                    <input name="numrem" type="hidden" id="numrem" value="<%=pla.getNumrem()%>">
                  </span></span></td>
                  <td class="fila">Destino</td>
                  <td class="letra"><%=pla.getDespla()%></td>
                </tr>
                <tr>
                  <td class="fila">Vehiculo</td>
                  <td class="letra"><%=pla.getPlaveh()%>
                    </td>
                  <td class="fila" nowrap>Actividad Actual</td>
                  <td class="letra" nowrap><%=act.getDesLarga()%>

                    <input type="hidden" name="acti" value="<%=act.getCodActividad()%>" >
                    <input name="desact" type="hidden" id="desact" value="<%=act.getDesLarga()%>"></td>
                </tr>
                <tr>
                  <td class="fila">Cliente</td>
                  <td colspan="3" class="letra"><%=pla.getNomcliente()%>
                    <input name="cliente" type="hidden" id="cliente" value="<%=pla.getClientes()%>">
                    <input type="hidden" name="ult_fec" value="<%=ult_fec%>" id="ult_fec"></td>
                  </tr>

                <tr >
                  <td class="fila">Actividades</td>
                  <td class="letra"><%Vector Vecca = model.clientactSvc.obtVecClientAct();%>
                    <select name="codact" id="select" class="textbox" onChange="cargarSelects('<%=CONTROLLER%>?estado=Infoact&accion=Busact&carpeta=jsp/trafico/actividad/Infoactividad&pagina=DatosActividad.jsp');">
                    <option value="">Seleccione</option>
                    <%for (int i = 0; i < Vecca.size(); i++){
			 				ClienteActividad ca = (ClienteActividad) Vecca.elementAt(i);%>
                    <option value="<%=ca.getCodActividad()%>"><%=ca.getLargaAct()%></option>
                    <%}%>
                  </select></td>
                  <td class="fila">Remesa</td>
                  <td class="letra"><%=pla.getNumrem()%></td>
                </tr>
              </table>
              <table width="100%"  border="0">
                <tr>
                  <td width="48%" class="subtitulo1"><span class="subtitulos">Informaci&oacute;n de la Actividad</span></td>
                  <td width="52%" class="barratitulo"><img src="<%=BASEURL%>/images/titulo.gif" width="32" height="20"><span class="subtitulos">
                    <input name="mfecini" type="hidden" value="<%=act.getM_Fecinicio()%>">
                    <input type="hidden" name="mfecfin" value="<%=act.getM_Fecfin()%>">
                    <input type="hidden" name="mduracion" value="<%=act.getM_Duracion()%>">
                    <input type="hidden" name="mdocumento" value="<%=act.getM_Documento()%>">
                    <input type="hidden" name="mtiempo" value="<%=act.getM_Tiempodemora()%>">
                    <input type="hidden" name="mcausa" value="<%=act.getM_Causademora()%>">
                    <input type="hidden" name="mresponsable" value="<%=act.getM_Resdemora()%>">
                    <input type="hidden" name="mfeccierre" value="<%=act.getM_Feccierre()%>">
                    <input type="hidden" name="mobs" value="<%=act.getM_Observacion()%>">
					<input type="hidden" name="mcreal" value="<%=act.getM_cantrealizada()%>">
					<input type="hidden" name="mcplan" value="<%=act.getM_cantplaneada()%>">
					<input type="hidden" name="mref1" value="<%=act.getM_referencia1()%>">
					<input type="hidden" name="mref2" value="<%=act.getM_referencia2()%>">
					<input type="hidden" name="mrefnum1" value="<%=act.getM_refnumerica1()%>">
					<input type="hidden" name="mrefnum2" value="<%=act.getM_refnumerica2()%>">
                  </span></td>
                </tr>
              </table>
              <table width="100%" class="tablaInferior">
			     <%if (act.getLink().equals("S")){%>
                <tr class="barratitulo">
                  <td colspan="2"><a  style="cursor:hand" class="Simulacion_Hiper" onclick="window.open('<%=CONTROLLER%>?estado=Menu&accion=Cargar&pagina=Manejo.jsp?&carpeta=/imagen&titulo=Imagenes' ,'','status=no,scrollbars=no,width=620,height=520,resizable=yes');">Adjuntar Imagen</a> </td>
                  </tr>
                <%}if (act.getFecinicio().equals("S")){%>
                <tr class="fila">
                  <td width="34%"><%=act.getLargaFecini()%> </td>
                  <td width="66%"><input  type="text"   name="fecini"  class="textbox" value="<%=fecini%>" readonly >
					<img src="<%=BASEURL%>/images/cal.gif" width="16" height="16" align="absmiddle" style="cursor:hand " onclick="if(self.gfPop)gfPop.fPopCalendar(document.forma.fecini);return false;" HIDEFOCUS>
                    <img name="logo" id="logo" src="<%=BASEURL%>/images/botones/iconos/obligatorio.gif" width="10" height="10" style="visibility:<%=(act.getM_Fecinicio().equals("S"))?"visibility":"hidden"%>"></td>
                </tr>
                <%} if (act.getFecfinal().equals("S")){%>
                <tr class="fila">
                  <td><%=act.getLargaFecfin()%> </td>
                  <td><span class="comentario">
                    <input  type="text"   name="fecfin"  class="textbox" value="<%=fecfin%>" readonly >
					<img src="<%=BASEURL%>/images/cal.gif" width="16" height="16" align="absmiddle" style="cursor:hand " onclick="if(self.gfPop)gfPop.fPopCalendar(document.forma.fecfin);return false;" HIDEFOCUS>
                    <img name="logo" id="logo" src="<%=BASEURL%>/images/botones/iconos/obligatorio.gif" width="10" height="10" style="visibility:<%=(act.getM_Fecfin().equals("S"))?"visibility":"hidden"%>"> </span></td>
                </tr>
                <%} if(act.getDuracion().equals("S")){%>
                <tr class="fila">
                  <td><%=act.getLargaDuracion()%> </td>
                  <td><table width="179" border="0" cellspacing="0" cellpadding="0">
                      <tr>
                        <td width="94"><span class="comentario">
                          <input name="duracion" type="text" class="textbox" id="duracion" onKeyPress="soloDigitos(event,'decOK')" value="<%=dur%>" size="8" maxlength="6">
                        </span></td>
                        <td width="179" class="fila">valor en horas <img name="logo" id="logo" src="<%=BASEURL%>/images/botones/iconos/obligatorio.gif" width="10" height="10" style="visibility:<%=(act.getM_Duracion().equals("S"))?"visibility":"hidden"%>"></td>
                      </tr>
                  </table>                    </td>
                </tr>
                <%} if(act.getDocumento().equals("S")){%>
                <tr class="fila">
                  <td><%=act.getLargaDocumento()%> </td>
                  <td><input name="documento" type="text" class="textbox" id="documento" value="<%=doc%>" size="12" maxlength="10">
                    <img name="logo" id="logo" src="<%=BASEURL%>/images/botones/iconos/obligatorio.gif" width="10" height="10" style="visibility:<%=(act.getM_Documento().equals("S"))?"visibility":"hidden"%>">                  </td>
                </tr>
                <%} if(act.getTiempodemora().equals("S")){%>
                <tr class="fila">
                  <td><%=act.getLargaTiempodemora()%> </td>
                  <td><table width="182" border="0" cellspacing="0" cellpadding="0">
                      <tr>
                        <td width="72"><span class="comentario">
                          <input name="tiempo" type="text" class="textbox" id="tiempo" onKeyPress="soloDigitos(event,'decOK')" value="<%=tiempo%>" size="8" maxlength="6">
                        </span></td>
                        <td width="110" class="fila">valor en horas <img name="logo" id="logo" src="<%=BASEURL%>/images/botones/iconos/obligatorio.gif" width="10" height="10" style="visibility:<%=(act.getM_Tiempodemora().equals("S"))?"visibility":"hidden"%>"></td>
                      </tr>
                  </table></td>
                </tr>
                <%} if(act.getCausademora().equals("S")){%>
                <tr class="fila">
                  <td><%=act.getLargaCausademora()%> </td>
                  <td><%Vector vcau = model.jerarquia_tiempoService.getVeccausa();%>
                    <select name="cdemora" class="textbox" id="cdemora" style="width:80%">
                      <option value=''>Seleccione</option>
                      <%for (int i = 0; i < vcau.size(); i++){
                       		Jerarquia_tiempo jt = (Jerarquia_tiempo) vcau.elementAt(i);%>
                      <option  value="<%=jt.getDemora()%>" <%=(jt.getDemora().equals(cau))? "selected":"" %>><%=jt.getDesCausa()%></option>
                      <%}%>
                    </select>
                    <img name="logo" id="logo" src="<%=BASEURL%>/images/botones/iconos/obligatorio.gif" width="10" height="10" style="visibility:<%=(act.getM_Causademora().equals("S"))?"visibility":"hidden"%>"></td>
                </tr>
                <%} if( act.getResdemora().equals("S") ){%>
                <tr class="fila">
                  <td><%=act.getLargaResdemora()%> </td>
                  <td><%Vector vres = model.jerarquia_tiempoService.getVecres(); %>
                    <select name="resdem" class="textbox" id="resdem" style="width:80%">
                    <option value=''>Seleccione</option>
                    <%for (int j = 0; j < vres.size(); j++){
                       		Jerarquia_tiempo jtc = (Jerarquia_tiempo) vres.elementAt(j);%>
                    <option  value="<%=jtc.getResponsable()%>" <%=jtc.getResponsable().equals(resde) ? "selected" : "" %> ><%=jtc.getDesResponsable()%></option>
                    <%}%>
                  </select>  <img name="logo" id="logo" src="<%=BASEURL%>/images/botones/iconos/obligatorio.gif" width="10" height="10" style="visibility:<%=(act.getM_Causademora().equals("S"))?"visibility":"hidden"%>">                </tr>
				<%} if( act.getCantrealizada().equals("S") ){%>
                <tr class="fila">
                  <td><%=act.getLarga_cantrealizada()%> </td>
                  <td><input name="cantrealizada" type="text" class="textbox" id="cantrealizada" value="<%=cantr%>" size="15" maxlength="10">
                    <img name="logo" id="logo" src="<%=BASEURL%>/images/botones/iconos/obligatorio.gif" width="10" height="10" style="visibility:<%=(act.getM_cantrealizada().equals("S"))?"visibility":"hidden"%>"></td>
                </tr>
				<%} if( act.getCantplaneada().equals("S") ){%>
                <tr class="fila">
                  <td><%=act.getLarga_cantplaneada()%> </td>
                  <td><input name="cantplaneada" type="text" class="textbox" id="cantplaneada" value="<%=cantp%>" size="15" maxlength="10">
                    <img name="logo" id="logo" src="<%=BASEURL%>/images/botones/iconos/obligatorio.gif" width="10" height="10" style="visibility:<%=(act.getM_cantplaneada().equals("S"))?"visibility":"hidden"%>"></td>
                </tr>
				<%} if(act.getFeccierre().equals("S")){%>
                <tr class="fila">
                  <td><%=act.getLargaFeccierre()%> </td>
                  <td>
                    <input  type="text"   name="feccierre" id="feccierre"  class="textbox" readonly value="<%=fecc%>" >
					<%if(tienefec.equals("N")){%>
					<img src="<%=BASEURL%>/images/cal.gif" width="16" height="16" align="absmiddle" style="cursor:hand " onclick="if(self.gfPop)gfPop.fPopCalendar(document.forma.feccierre);return false;" HIDEFOCUS>
					<%}%>
                    <img name="logo" id="logo" src="<%=BASEURL%>/images/botones/iconos/obligatorio.gif" width="10" height="10" style="visibility:<%=(act.getM_Feccierre().equals("S"))?"visibility":"hidden"%>"> </td>
                </tr>
				<%} if(act.getReferencia1().equals("S")){%>
                <tr class="fila">
                  <td><%=act.getLarga_referencia1()%> </td>
                  <td>
                    <input   name="referencia1"  type="text"  class="textbox" value="<%=ref1%>" size="15" maxlength="10" >
                    <img name="logo" id="logo" src="<%=BASEURL%>/images/botones/iconos/obligatorio.gif" width="10" height="10" style="visibility:<%=(act.getM_referencia1().equals("S"))?"visibility":"hidden"%>">					</td>
                </tr>
				<%} if(act.getReferencia2().equals("S")){%>
                <tr class="fila">
                  <td><%=act.getLarga_referencia2()%> </td>
                  <td>
                    <input   name="referencia2"  type="text"  class="textbox" value="<%=ref2%>" size="15" maxlength="10" >
                    <img name="logo" id="logo" src="<%=BASEURL%>/images/botones/iconos/obligatorio.gif" width="10" height="10" style="visibility:<%=(act.getM_referencia2().equals("S"))?"visibility":"hidden"%>">
					</td>
                </tr>
				<%} if(act.getRefnumerica1().equals("S")){%>
                <tr class="fila">
                  <td><%=act.getLarga_refnumerica1()%> </td>
                  <td>
                    <input   name="refnumerica1"  type="text"  class="textbox" onKeyPress="soloDigitos(event,'NO')"  value="<%=refnum1%>" size="15" maxlength="10" >
                    <img name="logo" id="logo" src="<%=BASEURL%>/images/botones/iconos/obligatorio.gif" width="10" height="10" style="visibility:<%=(act.getM_refnumerica1().equals("S"))?"visibility":"hidden"%>">					</td>
                </tr>
				<%} if(act.getRefnumerica2().equals("S")){%>
                <tr class="fila">
                  <td><%=act.getLarga_refnumerica2()%> </td>
                  <td>
                    <input   name="refnumerica2"  type="text"  class="textbox" onKeyPress="soloDigitos(event,'NO')" value="<%=refnum2%>" size="15" maxlength="10">
                    <img name="logo" id="logo" src="<%=BASEURL%>/images/botones/iconos/obligatorio.gif" width="10" height="10" style="visibility:<%=(act.getM_refnumerica2().equals("S"))?"visibility":"hidden"%>">					</td>
                </tr>
                <%} if(act.getObservacion().equals("S")){%>
                <tr class="fila">
                  <td><%=act.getLargaObservacion()%> </td>
                  <td><input type="hidden" name="obs" value="<%=obs%>"><%=obs%><%=!obs.equals("")?"<br>":""%><textarea name="c_obse" cols="60" class="textbox" id="textarea"></textarea></td>
                </tr>
                <%}%>
            </table></td>
        </tr>
      </table></td>
    </tr>
  </table>
       <br>
      <%if(existeact.equals("true")){%>
	       <div align="center">
   			  <img src="<%=BASEURL%>/images/botones/modificar.gif"  name="imgmodificar" onClick="validarInfoActividad();" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" style="cursor:hand">&nbsp;
              <img src="<%=BASEURL%>/images/botones/anular.gif"  name="imganular" onClick="window.location='<%=CONTROLLER%>?estado=Infoact&accion=Anular&pagina=DatosActividad.jsp&carpeta=jsp/trafico/actividad/Infoactividad&cliente=<%=pla.getClientes()%>&acti=<%=act.getCodActividad()%>&numpla=<%=pla.getNumpla()%>&numrem=<%=pla.getNumrem()%>'" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" style="cursor:hand">&nbsp;
              <img src="<%=BASEURL%>/images/botones/salir.gif"  name="imgsalir"  onMouseOver="botonOver(this);" onClick="parent.close();" onMouseOut="botonOut(this);" style="cursor:hand">&nbsp;
           <img src="<%=BASEURL%>/images/botones/regresar.gif"  name="imgaceptar" onclick="window.location='<%=CONTROLLER%>?estado=Infoact&accion=Buscarclin&pagina=BuscarClientexPlan.jsp&ruta=/jsp/trafico/actividad/Infoactividad/&numpla=<%=pla.getNumpla()%>'" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" style="cursor:hand"></div>
		  <%} else {%>
		  	<div align="center">
  			   <img src="<%=BASEURL%>/images/botones/aceptar.gif"  name="imgaceptar" onClick="validarInfoActividad();" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" style="cursor:hand">&nbsp;
			   <img src="<%=BASEURL%>/images/botones/cancelar.gif"  name="imgcancelar"  onMouseOver="botonOver(this);" onClick="forma.reset();" onMouseOut="botonOut(this);" style="cursor:hand"> &nbsp;
               <img src="<%=BASEURL%>/images/botones/salir.gif"  name="imgsalir"  onMouseOver="botonOver(this);" onClick="parent.close();" onMouseOut="botonOut(this);" style="cursor:hand">&nbsp;
          <img src="<%=BASEURL%>/images/botones/regresar.gif"  name="imgaceptar" onclick="window.location='<%=CONTROLLER%>?estado=Infoact&accion=Buscarclin&pagina=BuscarClientexPlan.jsp&ruta=/jsp/trafico/actividad/Infoactividad/&numpla=<%=pla.getNumpla()%>'" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" style="cursor:hand"> </div>
		  <%}%>

    <%if( !res.equalsIgnoreCase("")){%>
  <p><table border="2" align="center">
  <tr>
    <td><table width="100%" border="1" align="center"  bordercolor="#F7F5F4" bgcolor="#FFFFFF">
      <tr>
        <td width="262" align="center" class="mensajes"><%=res%></td>
        <td width="32" background="<%=BASEURL%>/images/cuadronaranja.JPG">&nbsp;</td>
        <td width="44">&nbsp;</td>
      </tr>
    </table></td>
  </tr>
</table>
<%}%>
<br>
  </form>
</div>
<%=datos[1]%>
<iframe width=188 height=166 name="gToday:normal:agenda.js:gfPop:plugins_24.js" id="gToday:normal:agenda.js:gfPop:plugins_24.js" src="<%=BASEURL%>/js/Calendario/ipopeng.htm" scrolling="no" frameborder="0" style="visibility:visible; z-index:999; position:absolute; left:-500px; top:0px;"> </iframe>    
</body>
</html>
