<!-- 
- Autor : Ing. Diogenes Bastidas Morales
- Date  : 2 de septiembre de 2005
- Copyrigth Notice : Fintravalores S.A. S.A
-->
<%--
-@(#)
--Descripcion : Pagina JSP, muestra la lista los clientes de X planilla
--%>

<%-- Declaracion de librerias--%>
<%@page contentType="text/html"%>
<%@page session="true"%>
<%@page errorPage=".../InfoActividad/error/ErrorPage.jsp"%>
<%@page import="java.util.*"%>
<%@page import="com.tsp.operation.model.*"%>
<%@page import="com.tsp.util.*"%>
<%@page import="com.tsp.operation.model.beans.*, java.util.*"%>
<%@include file="/WEB-INF/InitModel.jsp"%>
<%@taglib uri="http://jsptags.com/tags/navigation/pager" prefix="pg" %>

<html>
<head>
<title>Listado Actividades</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link href="<%= BASEURL %>/css/estilostsp.css" rel="stylesheet" type="text/css">
<link href="../../../../css/estilostsp.css" rel="stylesheet" type="text/css"> 
<script type='text/javascript' src="<%= BASEURL %>/js/reporte.js"></script>
<script type='text/javascript' src="<%= BASEURL %>/js/boton.js"></script>


</head>
<body onResize="redimensionar()" onLoad="redimensionar()">
<div id="capaSuperior" style="position:absolute; width:100%; height:100px; z-index:0; left: 0px; top: 0px;">
 <jsp:include page="/toptsp.jsp?encabezado=Llenar Actividad"/>
</div>
<div id="capaCentral" style="position:absolute; width:100%; height:87%; z-index:0; left: 0px; top: 100px; overflow: scroll;">
<%  String msg = (request.getParameter("men")!=null)?request.getParameter("men"):"";
	String res ="";
    ActividadPlan actpla ;
	String style = "simple";
	String position =  "bottom";
    String index =  "center";
    int maxPageItems = 10;
    int maxIndexPages = 10;
    Planilla pla = model.clientactSvc.getPla();

    Vector VecActPlan = model.actplanService.ObtActividadPlan();

    if(msg.equals("Noact")){
	     res = " No Tiene Actividades Asignadas!";
	}
  %>
  <table width="500" border="2" align="center">
    <tr>
      <td><table width="100%"  border="0">
        <tr>
          <td width="48%" height="22" class="subtitulo1">Infromaci&oacute;n de la Planilla </td>
          <td width="52%" class="barratitulo"><img src="<%=BASEURL%>/images/titulo.gif" width="32" height="20"></td>
        </tr>
      </table>
        <table width="100%" class="tablaInferior">
        <tr>
          <td class="fila" >Planilla Nro.</td>
          <td width="92" class="letra"><%=pla.getNumpla()%></td>
          <td width="84" class="fila">Fecha</td>
          <td width="132" class="letra" ><%=pla.getFecpla()%></td>
        </tr>
        <tr>
          <td class="fila">Origen</td>
          <td class="letra"><%=pla.getOrinom()%></td>
          <td class="fila">Destino</td>
          <td class="letra"><%=pla.getDespla()%></td>
        </tr>
        <tr>
          <td class="fila" >Vehiculo</td>
          <td class="letra"><%=pla.getPlaveh()%></td>
          <td colspan="2" class="fila">&nbsp;</td>
          </tr>
		</table>
		<table width="100%"  border="0">
          <tr>
            <td width="48%" class="subtitulo1">Clientes</td>
            <td width="52%" class="barratitulo"><img src="<%=BASEURL%>/images/titulo.gif" width="32" height="20"></td>
          </tr>
        </table>		<table width="99%" border="1" align="center" bordercolor="#999999" bgcolor="#F7F5F4">
        <tr class="tblTitulo" align="center">
          <td width="66" >Remesa</td>
          <td width="66" >Codigo</td>
          <td width="313" colspan="3" >Nombre</td>
        </tr>
        <pg:pager
    items="<%=VecActPlan.size()%>"
    index="<%= index %>"
    maxPageItems="<%= maxPageItems %>"
    maxIndexPages="<%= maxIndexPages %>"
    isOffset="<%= true %>"
    export="offset,currentPageNumber=pageNumber"
    scope="request">
        <%-- keep track of preference --%>
        <%
        for (int i = offset.intValue(), l = Math.min(i + maxPageItems, VecActPlan.size()); i < l; i++) {
             actpla = (ActividadPlan) VecActPlan.elementAt(i); %>
        <pg:item>
        <tr class="<%=(i % 2 == 0 )?"filagris":"filaazul"%>" onMouseOver='cambiarColorMouse(this)'   style="cursor:hand" 
        onClick="window.location='<%=CONTROLLER%>?estado=Infoact&accion=Buscaractclin&carpeta=jsp/trafico/actividad/Infoactividad&pagina=DatosActividad.jsp&cliente=<%=actpla.getCodcliente()%>&tipo=<%=actpla.getTipoViaje()%>&numpla=<%=pla.getNumpla()%>&remesa=<%=actpla.getNumrem()%>&nomcliente=<%=actpla.getCliente()%>&est='">
          <td height="20" class="bordereporte" ><%=actpla.getNumrem()%></td>
          <td height="20" class="bordereporte" ><%=actpla.getCodcliente()%></td>
          <td height="20" colspan="3" class="bordereporte"><%=actpla.getCliente()%> </td>
        </tr>
        </pg:item>
        <%}%>
        <tr class="bordereporte">
          <td colspan="8" align="center" valign="middle" >
            <div align="center"> <pg:index>
              <jsp:include page="../../../../WEB-INF/jsp/googlesinimg.jsp" flush="true"/>      
    </pg:index></div></td>
        </tr>
        </pg:pager>
      </table></td>
    </tr>
	</table>
  </table>
    <%if( !res.equalsIgnoreCase("")){%>
  <p><table border="2" align="center">
  <tr>
    <td><table width="100%" border="1" align="center"  bordercolor="#F7F5F4" bgcolor="#FFFFFF">
      <tr>
        <td width="262" align="center" class="mensajes"><%=res%></td>
        <td width="32" background="<%=BASEURL%>/images/cuadronaranja.JPG">&nbsp;</td>
        <td width="44">&nbsp;</td>
      </tr>
    </table></td>
  </tr>
</table>
<%}%>
<br>
  <table width="500" border="0" align="center">
    <tr>
      <td><img src="<%=BASEURL%>/images/botones/regresar.gif"  name="imgaceptar" onclick="window.location='<%=CONTROLLER%>?estado=Idioma&accion=Cargar&pagina=BuscarClientexPlan.jsp&ruta=/jsp/trafico/actividad/Infoactividad/'" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" style="cursor:hand"></td>
    </tr>
  </table>

</div>
</body>
</html>
