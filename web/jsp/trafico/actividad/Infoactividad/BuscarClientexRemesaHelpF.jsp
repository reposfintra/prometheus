<!--  
	 - Author(s)       :      LREALES
	 - Description     :      AYUDA FUNCIONAL - Buscar Cliente Por Remesa
	 - Date            :      05/08/2006 
	 - Copyright Notice:      TRANSPORTES SANCHEZ POLO S.A.
-->
<%@ include file="/WEB-INF/InitModel.jsp"%>
<HTML>
<HEAD>
<TITLE>AYUDA FUNCIONAL - Buscar Cliente Por Remesa</TITLE>
<META http-equiv=Content-Type content="text/html; charset=windows-1252">
<link href="../css/estilostsp.css" rel="stylesheet" type="text/css"> 
<link href="<%=BASEURL%>/css/estilostsp.css" rel="stylesheet" type="text/css">
<script type='text/javascript' src="<%= BASEURL %>/js/boton.js"></script> 
</HEAD>
<BODY> 
<% String BASEIMG = BASEURL +"/images/ayuda/trafico/consultar_actividades/"; %>
  <table width="95%"  border="2" align="center">
    <tr>
      <td height="117" >
        <table width="100%" border="0" align="center">
          <tr  class="subtitulo">
            <td height="20"><div align="center">MANUAL DE CONSULTAS DE ACTIVIDADES WEB</div></td>
          </tr>
		  
          <tr class="subtitulo1">
            <td>Descripci&oacute;n del funcionamiento del programa para realizar la Consulta de Actividades.</td>
          </tr>
          <tr>
            <td  class="ayudaHtmlTexto"><div align="center">
              <p>&nbsp;</p>
              </div></td>
          </tr>
		  
          <tr>
            <td  class="ayudaHtmlTexto"><p>En la siguiente pantalla se procede a presionar un click sobre la lupa &oacute; bot&oacute;n de b&uacute;squeda, &oacute; simplemente presionar enter para comenzar el proceso de b&uacute;squeda de las actividades de esa remesa.</p>            </td>
          </tr>
          <tr>
            <td  class="ayudaHtmlTexto"><div align="center"><IMG height=125 src="<%=BASEIMG%>image_001.JPG" width=412 border=0 v:shapes="_x0000_i1143"></div></td>
          </tr>
		  
          <tr>
            <td  class="ayudaHtmlTexto"><p>&nbsp;</p>
            <p>Si en la pantalla le sale el siguiente mensaje, es por que esa remesa no tiene actividades relacionadas. </p></td>
          </tr>
          <tr>
            <td  class="ayudaHtmlTexto"><div align="center"><IMG height=183 src="<%=BASEIMG%>image_error_001.JPG" width=396 border=0 v:shapes="_x0000_i1054"></div></td>
          </tr>
		  
		  <tr>
            <td  class="ayudaHtmlTexto"><p>&nbsp;</p>
            <p>Si la remesa tiene m&aacute;s de una actividad, le mostrar&aacute; la siguiente pantalla donde usted podr&aacute; escoger una actividad especifica para acceder a su informaci&oacute;n. </p></td>
          </tr>
          <tr>
            <td  class="ayudaHtmlTexto"><div align="center"><IMG height=183 src="<%=BASEIMG%>image_002.JPG" width=586 border=0 v:shapes="_x0000_i1054"></div></td>
          </tr>
		  
		  <tr>
            <td  class="ayudaHtmlTexto"><p>&nbsp;</p>
            <p>Luego se mostrar&aacute; la informaci&oacute;n especifica de esa actividad.</p></td>
          </tr>
          <tr>
            <td  class="ayudaHtmlTexto"><div align="center"><IMG height=298 src="<%=BASEIMG%>image_003.JPG" width=520 border=0 v:shapes="_x0000_i1054"></div></td>
          </tr>
      </table></td>
    </tr>
  </table>
  <p></p>
<center>
<img src = "<%=BASEURL%>/images/botones/salir.gif" style = "cursor:hand" name = "imgsalir" onClick = "parent.close();" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);">
</center>
</BODY>
</HTML>
