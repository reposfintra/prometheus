<!--  
- Autor : LREALES
- Date  : 4 de agosto de 2006
- Copyrigth Notice : Fintravalores S.A. S.A
-->

<%--
-@(#)
--Descripcion : Pagina JSP, muestra la informacion de la actividad 
--%>

<%-- Declaracion de librerias--%>
<%@page contentType="text/html"%>
<%@page session="true"%>
<%@page import="java.util.*"%>
<%@page errorPage="/error/ErrorPage.jsp"%>
<%@page import="com.tsp.operation.model.*"%>
<%@page import="com.tsp.operation.model.beans.*"%>
<%@page import="com.tsp.util.*"%>
<%@include file="/WEB-INF/InitModel.jsp"%>
<%@taglib uri="http://jsptags.com/tags/navigation/pager" prefix="pg" %>
<%
  String path = application.getRealPath(model.menuService.getRealUrl(request.getRequestURI()));
  String datos[] = model.menuService.getContenidoMenu(BASEURL,request.getRequestURI(),path);
%>
<html>
<head>
<title>Informacion de la Actividad</title>
</head>
<link href="<%= BASEURL %>/css/estilostsp.css" rel='stylesheet'>
<link href="../../../../css/estilostsp.css" rel="stylesheet" type="text/css">
<script type='text/javascript' src="<%= BASEURL %>/js/validar.js"></script>
<script type='text/javascript' src="<%= BASEURL %>/js/boton.js"></script> 
<body>
<div id="capaSuperior" style="position:absolute; width:100%; height:100px; z-index:0; left: 0px; top: 0px;">
  <jsp:include page="/toptsp.jsp?encabezado=Informacion Actividad"/>
</div>
<div id="capaCentral" style="position:absolute; width:100%; height:87%; z-index:0; left: 0px; top: 100px; overflow: scroll;">
<%
Planilla pla = model.clientactSvc.getPla();
Actividad act = model.actividadSvc.ObtActividad();
String msg = (request.getParameter("men")!=null)?request.getParameter("men"):"";
String existeact = (request.getParameter("act")!=null)?request.getParameter("act"):"";
String nom="",res="",cod="",fecini="",fecfin="",doc="",cau="",resde="",fecc="",obs="",tiempo="",dur="",ac="",cantr="",cantp="",ref1="",ref2="";
int refnum1=0,refnum2=0;
String accion  = "Ingresar";
if(existeact.equals("true")){
     accion  = "Modificar";
	 InfoActividad infact = model.infoactService.obtInfoActividad();
	 fecini=infact.getFecini();
     fecfin= infact.getFecfin();
     dur = ""+infact.getDuracion();
     doc = infact.getDocumento();
     tiempo= ""+infact.getTiempoDemora();
     cau=infact.getCausaDemora();
     resde=infact.getResdemora();
     fecc=infact.getFeccierre();
	 cantr = infact.getCantrealizada();
	 cantp = infact.getCantplaneada();
     obs=infact.getObservacion();
	 ref1 = infact.getReferencia1();
	 ref2 = infact.getReferencia2();
	 refnum1 =   infact.getRefnumerica1();
	 refnum2 =   infact.getRefnumerica2();
}
%>

<form name="forma" method="post" action="<%=CONTROLLER%>?estado=Infoact&accion=<%=accion%>&carpeta=jsp/trafico/actividad/Infoactividad&pagina=DatosActividad.jsp" >
  <table width="500" border="2" align="center">
    <tr>
      <td><table width="100%" border="0" align="center" cellpadding="0" cellspacing="0">
        <tr>
          <td><table width="100%"  border="0">
              <tr>
                <td width="48%" class="subtitulo1">Informaci&oacute;n de la Planilla</td>
                <td width="52%" class="barratitulo"><img src="<%=BASEURL%>/images/titulo.gif" width="32" height="20" align="left"><%=datos[0]%></td>
              </tr>
            </table>
              <table width="100%" class="tablaInferior">
                <tr >
                  <td width="114" class="fila" >Planilla N&deg;</td>
                  <td width="119" class="letra"><%=pla.getNumpla()%>
                    <input name="numpla" type="hidden" id="numpla" value="<%=pla.getNumpla()%>"></td>
                  <td width="108" class="fila">Fecha </td>
                  <td width="121" class="letra" ><%=pla.getFecpla()%></td>
                </tr>
                <tr>
                  <td  class="fila">Origen</td>
                  <td class="letra"><%=pla.getOrinom()%><span class="barratitulo"><span class="subtitulos">
                    <input name="numrem" type="hidden" id="numrem" value="<%=pla.getNumrem()%>">
                  </span></span></td>
                  <td class="fila">Destino</td>
                  <td class="letra"><%=pla.getDespla()%></td>
                </tr>
                <tr>
                  <td class="fila">Vehiculo</td>
                  <td class="letra"><%=pla.getPlaveh()%>
                    </td>
                  <td class="fila">Remesa</td>
                  <td class="letra"><%=pla.getNumrem()%></td>
                </tr>
                <tr>
                  <td class="fila">Cliente</td>
                  <td colspan="3" class="letra"><%=pla.getNomcliente()%>
                    <input name="cliente" type="hidden" id="cliente" value="<%=pla.getClientes()%>"></td>
                  </tr>

                <tr >
                  <td class="fila">Actividad</td>
                  <td colspan="3" class="letra"><%=act.getDesLarga()%> <input type="hidden" name="acti" value="<%=act.getCodActividad()%>" >
                    <input name="desact" type="hidden" id="desact2" value="<%=act.getDesLarga()%>"></td>
                  </tr>
              </table>
              <table width="100%"  border="0">
                <tr>
                  <td width="48%" class="subtitulo1"><span class="subtitulos">Informaci&oacute;n de la Actividad</span></td>
                  <td width="52%" class="barratitulo"><img src="<%=BASEURL%>/images/titulo.gif" width="32" height="20"><span class="subtitulos">
                    <input name="mfecini" type="hidden" value="<%=act.getM_Fecinicio()%>">
                    <input type="hidden" name="mfecfin" value="<%=act.getM_Fecfin()%>">
                    <input type="hidden" name="mduracion" value="<%=act.getM_Duracion()%>">
                    <input type="hidden" name="mdocumento" value="<%=act.getM_Documento()%>">
                    <input type="hidden" name="mtiempo" value="<%=act.getM_Tiempodemora()%>">
                    <input type="hidden" name="mcausa" value="<%=act.getM_Causademora()%>">
                    <input type="hidden" name="mresponsable" value="<%=act.getM_Resdemora()%>">
                    <input type="hidden" name="mfeccierre" value="<%=act.getM_Feccierre()%>">
                    <input type="hidden" name="mobs" value="<%=act.getM_Observacion()%>">
					<input type="hidden" name="mcreal" value="<%=act.getM_cantrealizada()%>">
					<input type="hidden" name="mcplan" value="<%=act.getM_cantplaneada()%>">
					<input type="hidden" name="mref1" value="<%=act.getM_referencia1()%>">
					<input type="hidden" name="mref2" value="<%=act.getM_referencia2()%>">
					<input type="hidden" name="mrefnum1" value="<%=act.getM_refnumerica1()%>">
					<input type="hidden" name="mrefnum2" value="<%=act.getM_refnumerica2()%>">
                  </span></td>
                </tr>
              </table>
              <table width="100%" class="tablaInferior">
			     <%if (act.getLink().equals("S")){%>
                <%}if (act.getFecinicio().equals("S")){%>
                <tr>
                  <td class="fila" width="43%"><%=act.getLargaFecini()%> </td>
                  <td class="letra" width="57%"><%=fecini%>                    </td>
                </tr>
                <%} if (act.getFecfinal().equals("S")){%>
                <tr>
                  <td class="fila"><%=act.getLargaFecfin()%> </td>
                  <td class="letra"><%=fecfin%>                     </span></td>
                </tr>
                <%} if(act.getDuracion().equals("S")){%>
                <tr>
                  <td class="fila"><%=act.getLargaDuracion()%> </td>
                  <td><table width="179" border="0" cellspacing="0" cellpadding="0">
                      <tr>
                        <td class="letra" width="94"><%=dur%></td>
                        <td width="179" class="letra"> horas </td>
                      </tr>
                  </table>                    </td>
                </tr>
                <%} if(act.getDocumento().equals("S")){%>
                <tr>
                  <td class="fila"><%=act.getLargaDocumento()%> </td>
                  <td class="letra"><%=doc%>                    </td>
                </tr>
                <%} if(act.getTiempodemora().equals("S")){%>
                <tr>
                  <td class="fila"><%=act.getLargaTiempodemora()%> </td>
                  <td><table width="182" border="0" cellspacing="0" cellpadding="0">
                      <tr class="letra">
                        <td width="72"><%=tiempo%></td>
                        <td width="110">horas </td>
                      </tr>
                  </table></td>
                </tr>
                <%} if(act.getCausademora().equals("S")){%>
                <tr>
                  <td class="fila"><%=act.getLargaCausademora()%> </td>
                  <td><%Vector vcau = model.jerarquia_tiempoService.getVeccausa();
						for (int i = 0; i < vcau.size(); i++){
                       		Jerarquia_tiempo jt = (Jerarquia_tiempo) vcau.elementAt(i);
							if(jt.getDemora().equals(cau)){
								 cau = jt.getDesCausa();
					  			 break;
						    }
						}%><%=cau%> </td>
                </tr>
                <%} if( act.getResdemora().equals("S") ){%>
                <tr>
                  <td><%=act.getLargaResdemora()%> </td>
                  <td><%Vector vres = model.jerarquia_tiempoService.getVecres();
                        for (int j = 0; j < vres.size(); j++){
                       		Jerarquia_tiempo jtc = (Jerarquia_tiempo) vres.elementAt(j);
						    if(jtc.getResponsable().equals(resde)){
							    resde = jtc.getDesResponsable();
								break;
							}
						}%>
                  </select>                </tr>
				<%} if( act.getCantrealizada().equals("S") ){%>
                <tr>
                  <td class="fila"><%=act.getLarga_cantrealizada()%> </td>
                  <td class="letra"><%=cantr%>                    </td>
                </tr>
				<%} if( act.getCantplaneada().equals("S") ){%>
                <tr>
                  <td class="fila"><%=act.getLarga_cantplaneada()%> </td>
                  <td class="letra"><%=cantp%>                    </td>
                </tr>
				<%} if(act.getFeccierre().equals("S")){%>
                <tr>
                  <td class="fila"><%=act.getLargaFeccierre()%> </td>
                  <td class="letra"><%=fecc%>                    </td>
                </tr>
				<%} if(act.getReferencia1().equals("S")){%>
                <tr>
                  <td class="fila"><%=act.getLarga_referencia1()%> </td>
                  <td class="letra"><%=ref1%>                    </td>
                </tr>
				<%} if(act.getReferencia2().equals("S")){%>
                <tr>
                  <td class="fila"><%=act.getLarga_referencia2()%> </td>
                  <td class="letra"><%=ref2%>
                    </td>
                </tr>
				<%} if(act.getRefnumerica1().equals("S")){%>
                <tr>
                  <td class="fila"><%=act.getLarga_refnumerica1()%> </td>
                  <td class="letra"><%=refnum1%>                    </td>
                </tr>
				<%} if(act.getRefnumerica2().equals("S")){%>
                <tr>
                  <td class="fila"><%=act.getLarga_refnumerica2()%> </td>
                  <td class="letra"><%=refnum2%>                    </td>
                </tr>
                <%} if(act.getObservacion().equals("S")){%>
                <tr>
                  <td class="fila"><%=act.getLargaObservacion()%> </td>
                  <td class="letra"><%=obs%></td>
                </tr>
                <%}%>
            </table></td>
        </tr>
      </table></td>
    </tr>
  </table>
    <br>
  <table width="500" border="0" align="center">
    <tr>
      <td>
		<img src="<%=BASEURL%>/images/botones/regresar.gif"  name="imgregresar" alt="Regresar a la vista anterior.." onclick="window.location='<%=CONTROLLER%>?estado=Idioma&accion=Cargar&pagina=ClientesxRemesa.jsp&ruta=/jsp/trafico/actividad/Infoactividad/'" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" style="cursor:hand"> 
  	  
		<img src="<%=BASEURL%>/images/botones/salir.gif"  name="imgsalir" alt="Salir al Menu" onclick="parent.close();" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" style="cursor:hand">
	  </td>
    </tr>
  </table>
  </form>
</div>
<%=datos[1]%>
</body>
</html>