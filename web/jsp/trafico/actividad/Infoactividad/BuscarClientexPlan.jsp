<!-- 
- Autor : Ing. Diogenes Bastidas Morales
- Date  : 2 de septiembre de 2005
- Copyrigth Notice : Fintravalores S.A. S.A
-->
<%--
-@(#)
--Descripcion : Pagina JSP, captura los parametros de busqueda de los clientes con x planilla
--%>

<%-- Declaracion de librerias--%>
<%@page contentType="text/html"%>
<%@page session="true"%>
<%@page import="java.util.*"%>
<%@page import="com.tsp.operation.model.*"%>
<%@page import="com.tsp.operation.model.beans.*"%>
<%@page import="com.tsp.util.*"%>
<%@include file="/WEB-INF/InitModel.jsp"%>
<html>
<head>
<title>Buscar Actividad</title>
</head>
<link href="<%= BASEURL %>/css/estilostsp.css" rel='stylesheet'>
<link href="../../../../css/estilostsp.css" rel="stylesheet" type="text/css">
<script type='text/javascript' src="<%= BASEURL %>/js/boton.js"></script>

<body onResize="redimensionar()" onLoad="redimensionar()">
<div id="capaSuperior" style="position:absolute; width:100%; height:100px; z-index:0; left: 0px; top: 0px;">
 <jsp:include page="/toptsp.jsp?encabezado=Buscar Cliente"/>
</div>
<div id="capaCentral" style="position:absolute; width:100%; height:87%; z-index:0; left: 0px; top: 100px; overflow: scroll;">
<%String msg = (request.getParameter("men")!=null)?request.getParameter("men"):"";
%>
<form name="forma" method="post" action="<%=CONTROLLER%>?estado=Infoact&accion=Buscarclin" >
  <table width="39%" border="2" align="center">
    <tr>
      <td><table width="100%"  border="0">
        <tr>
          <td width="48%" class="subtitulo1">Datos de Busqueda </td>
          <td width="52%" class="barratitulo"><img src="<%=BASEURL%>/images/titulo.gif" width="32" height="20"></td>
        </tr>
      </table>
        <table width="100%" class="tablaInferior">
        <tr class="fila">
          <td width="31%">Nro. Planilla </td>
          <td width="69%"><input name="numpla" type="text" class="textbox" id="numpla" >
            <img src="<%=BASEURL%>/images/botones/iconos/lupa.gif" width="20" height="20" onClick="forma.submit()" style="cursor:hand "></td>
        </tr>
      </table></td>
    </tr>
  </table>
<br>
<div align="center"><img src="<%=BASEURL%>/images/botones/salir.gif"  name="imgsalir"  onMouseOver="botonOver(this);" onClick="parent.close();" onMouseOut="botonOut(this);" style="cursor:hand"></div>

<%if(msg.equals("false")){%>
    <p><table width="387" border="2" align="center">
  <tr>
    <td><table width="100%"  border="1" align="center"  bordercolor="#F7F5F4" bgcolor="#FFFFFF">
      <tr>
        <td width="262" align="center" class="mensajes">Su b�squeda no arroj� resultados!</td>
        <td width="25" background="<%=BASEURL%>/images/cuadronaranja.JPG">&nbsp;</td>
        <td width="66">&nbsp;</td>
      </tr>
    </table></td>
  </tr>
</table>
<%}%></p>
</form>
</div>
</body>
</html>
