<!-- 
- Autor : LREALES
- Date  : 4 de agosto de 2006
- Copyrigth Notice : Fintravalores S.A. S.A
-->

<%--
-@(#)
--Descripcion : Pagina JSP, muestra la lista los clientes de X remesa
--%>

<%-- Declaracion de librerias--%>
<%@page contentType="text/html"%>
<%@page session="true"%>
<%@page errorPage=".../InfoActividad/error/ErrorPage.jsp"%>
<%@page import="java.util.*"%>
<%@page import="com.tsp.operation.model.*"%>
<%@page import="com.tsp.util.*"%>
<%@page import="com.tsp.operation.model.beans.*, java.util.*"%>
<%@include file="/WEB-INF/InitModel.jsp"%>
<%@taglib uri="http://jsptags.com/tags/navigation/pager" prefix="pg" %>
<%
  String path = application.getRealPath(model.menuService.getRealUrl(request.getRequestURI()));
  String datos[] = model.menuService.getContenidoMenu(BASEURL,request.getRequestURI(),path);
%>
<html>
<head>
<title>Listado de Actividades</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link href="<%= BASEURL %>/css/estilostsp.css" rel="stylesheet" type="text/css">
<link href="../../../../css/estilostsp.css" rel="stylesheet" type="text/css"> 
<script type='text/javascript' src="<%= BASEURL %>/js/reporte.js"></script>
<script type='text/javascript' src="<%= BASEURL %>/js/boton.js"></script>
</head>
<body onResize="redimensionar()" onLoad="redimensionar()">
<div id="capaSuperior" style="position:absolute; width:100%; height:100px; z-index:0; left: 0px; top: 0px;">
 <jsp:include page="/toptsp.jsp?encabezado=Consultar Actividad"/>
</div>
<div id="capaCentral" style="position:absolute; width:100%; height:87%; z-index:0; left: 0px; top: 100px; overflow: scroll;">
<%  
    ClienteActividad actrem;
	String style = "simple";
	String position =  "bottom";
    String index =  "center";
    int maxPageItems = 10;
    int maxIndexPages = 10;

    Vector Vec = model.clientactSvc.obtVecClientAct();

  %>
  <table width="500" border="2" align="center">
    <tr>
      <td><table width="100%"  border="0">
          <tr>
            <td width="48%" class="subtitulo1">Actividades</td>
            <td width="52%" class="barratitulo"><img src="<%=BASEURL%>/images/titulo.gif" width="32" height="20" align="left"><%=datos[0]%></td>
          </tr>
        </table>		<table width="99%" border="1" align="center" bordercolor="#999999" bgcolor="#F7F5F4">
        <tr class="tblTitulo" align="center">
          <td width="10%" >Remesa</td>
		  <td width="10%" >Planilla</td>
		  <td width="35%" >Cliente</td>
          <td width="10%" >Codigo</td>
          <td width="35%">Nombre</td>
        </tr>
        <pg:pager
    items="<%=Vec.size()%>"
    index="<%= index %>"
    maxPageItems="<%= maxPageItems %>"
    maxIndexPages="<%= maxIndexPages %>"
    isOffset="<%= true %>"
    export="offset,currentPageNumber=pageNumber"
    scope="request">
        <%-- keep track of preference --%>
        <%
        for (int i = offset.intValue(), l = Math.min(i + maxPageItems, Vec.size()); i < l; i++) {
             ClienteActividad cli_act = (ClienteActividad) Vec.elementAt(i); %>
        <pg:item>
        <tr class="<%=(i % 2 == 0 )?"filagris":"filaazul"%>" title="Escoger Actividad Especifica.."  onMouseOver='cambiarColorMouse(this)'   style="cursor:hand" 
        onClick="window.location='<%=CONTROLLER%>?estado=Infoact&accion=Busact&carpeta=jsp/trafico/actividad/Infoactividad&pagina=VerDatosActividad.jsp&numpla=<%=cli_act.getPlanilla()%>&numrem=<%=cli_act.getRemesa()%>&nomcliente=<%=cli_act.getNomCliente()%>&cliente=<%=cli_act.getCodCliente()%>&tipo=<%=cli_act.getTipoViaje()%>&codact=<%=cli_act.getCodActividad()%>&est=ver'">
          <td height="20" class="bordereporte" ><%=cli_act.getRemesa()%></td>
		  <td height="20" class="bordereporte" ><%=cli_act.getPlanilla()%></td>
		  <td height="20" class="bordereporte" ><%=cli_act.getNomCliente()%></td>
          <td height="20" class="bordereporte" ><%=cli_act.getCodActividad()%></td>
          <td height="20" class="bordereporte"><%=cli_act.getLargaAct()%> </td>
        </tr>
        </pg:item>
        <%}%>
        <tr class="bordereporte">
          <td colspan="8" align="center" valign="middle" >
            <div align="center"> <pg:index>
              <jsp:include page="../../../../WEB-INF/jsp/googlesinimg.jsp" flush="true"/>      
    </pg:index></div></td>
        </tr>
        </pg:pager>
      </table></td>
    </tr>
	</table>
  </table>
<br>
  <table width="500" border="0" align="center">
    <tr>
      <td>
	  <img src="<%=BASEURL%>/images/botones/regresar.gif"  name="imgaceptar" alt="Regresar a la vista anterior.." onclick="window.location='<%=CONTROLLER%>?estado=Idioma&accion=Cargar&pagina=BuscarClientexRemesa.jsp&ruta=/jsp/trafico/actividad/Infoactividad/'" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" style="cursor:hand"> 
	  <img src="<%=BASEURL%>/images/botones/salir.gif"  name="imgsalir" alt="Salir al Menu"  onMouseOver="botonOver(this);" onClick="parent.close();" onMouseOut="botonOut(this);" style="cursor:hand">
	  </td>
    </tr>
  </table>
</div>
<%=datos[1]%>
</body>
</html>