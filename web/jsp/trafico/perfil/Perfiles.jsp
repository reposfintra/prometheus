<!--
- Autor : Ing. Sandra M Escalante G
- Date  : 25 Abril 2005
- Copyrigth Notice : Fintravalores S.A. S.A
-->
<%--
-@(#)
--Descripcion : Pagina JSP, que se encarga de gestionar 
				el formulario para mostrar todos los perfiles
--%>

<%-- Declaracion de librerias--%>
<%@ page session="true"%>
<%@page import="com.tsp.operation.model.beans.*, java.util.*"%>
<%@ include file="/WEB-INF/InitModel.jsp"%>
<%@ taglib uri="http://jsptags.com/tags/navigation/pager" prefix="pg" %>
<%@page import="com.tsp.operation.model.*"%>
<%@page import="com.tsp.util.*"%>
<%
    String path = application.getRealPath(model.menuService.getRealUrl(request.getRequestURI()));
    String datos[] = model.menuService.getContenidoMenu(BASEURL,request.getRequestURI(),path);
%>
<html>
<head>
<title>.: Perfiles</title>
<link href="<%= BASEURL %>/css/estilostsp.css" rel='stylesheet'>
<link href="../../../css/estilostsp.css" rel="stylesheet" type="text/css">
</head>
<script type='text/javascript' src="<%= BASEURL %>/js/boton.js"></script>
<script type='text/javascript' src="<%= BASEURL %>/js/reporte.js"></script>
</head>
<body onresize="redimensionar()" onload = 'redimensionar()'>
<div id="capaSuperior" style="position:absolute; width:100%; height:100px; z-index:0; left: 0px; top: 0px;">
 <jsp:include page="/toptsp.jsp?encabezado=Perfiles"/>
</div>
<div id="capaCentral" style="position:absolute; width:100%; height:87%; z-index:0; left: 0px; top: 100px; overflow: scroll;">
<%Vector vperfiles = model.perfilService.getVPerfil();

if (vperfiles!=null){
        
 }else{
	model.perfilService.listarPerfilesxPropietario();
    vperfiles = model.perfilService.getVPerfil();     
}

String style = "simple";
String position =  "bottom";
String index =  "center";
int maxPageItems = 10;
int maxIndexPages = 10;%>
<table width="400" border="2" align="center">
  <tr>
    <td><table width="100%" border="1" bordercolor="#999999" bgcolor="#F7F5F4">
  <tr  align="center" class="barratitulo">
    <td colspan="120"><table width="100%" >
  <tr>
    <td width="23%" class="subtitulo1">Perfiles</td>
    <td width="77%"><img src="<%=BASEURL%>/images/titulo.gif" width="32" height="20" align="left"><%=datos[0]%></td>
  </tr>
</table>
</td>
  </tr>
  <tr  align="center" class="tblTitulo" height="20">
    <td nowrap>Id</td>
    <td nowrap width="280">Nombre</td>
  </tr>
  <pg:pager
    items="<%=vperfiles.size()%>"
    index="<%= index %>"
    maxPageItems="<%= maxPageItems %>"
    maxIndexPages="<%= maxIndexPages %>"
    isOffset="<%= true %>"
    export="offset,currentPageNumber=pageNumber"
    scope="request">
<%-- keep track of preference --%>

  <%for (int i = offset.intValue(), l = Math.min(i + maxPageItems, vperfiles.size()); i < l; i++){
        Perfil prf = (Perfil) vperfiles.elementAt(i);%>
  <pg:item>
  <tr class="<%=(i % 2 == 0 )?"filagris":"filaazul"%>" onMouseOver='cambiarColorMouse(this)' align="center" title="Mostrar informacion del Perfil" style="cursor:hand" onClick="window.open('/fintra/jsp/trafico/perfil/PerfilAdmon.jsp?idp=<%=prf.getId()%>&tipo=objeto&cmd=cgaperfil','','status=no,scrollbars=yes,resizable=yes')">
    <td nowrap class="bordereporte"><%=prf.getId()%></td>
    <td nowrap class="bordereporte"><%=prf.getNombre()%></td>
  </tr>
  </pg:item>
  <%}%>
  <tr class="fila">
        <td height="20" colspan="10" align="center" nowrap class="filagris" td>          <pg:index>
                <jsp:include page="/WEB-INF/jsp/googlesinimg.jsp" flush="true"/>
        </pg:index>        </td>
  </tr>
  </pg:pager>
</table></td>
  </tr>
</table>
<br>
<table width="416" align="center">
  <tr>
    <td align="center"><img src="<%=BASEURL%>/images/botones/regresar.gif" name="c_salir" onClick="window.history.back();" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" align="absmiddle" style="cursor:hand ">
	<img src="<%=BASEURL%>/images/botones/salir.gif" name="c_salir" onClick="window.close();" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" align="absmiddle" style="cursor:hand ">
	</td>
  </tr>
</table>
</div>
<%=datos[1]%>
</body>
</html>