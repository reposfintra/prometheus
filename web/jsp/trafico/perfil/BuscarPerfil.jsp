<!--
- Autor : Ing. Sandra M Escalante G
- Date  : 25 Abril 2005
- Copyrigth Notice : Fintravalores S.A. S.A
-->
<%--
-@(#)
--Descripcion : Pagina JSP, que se encarga de gestionar 
				el formulario para la busqueda de perfiles
--%>

<%-- Declaracion de librerias--%>
<%@page contentType="text/html"%>
<%@ page session="true"%>
<%@ page errorPage="../error/ErrorPage.jsp"%>
<%@page import="com.tsp.operation.model.beans.*, java.util.*"%>
<%@page import="com.tsp.operation.model.*"%>
<%@include file="/WEB-INF/InitModel.jsp"%>
<%@ taglib uri="http://jakarta.apache.org/taglibs/input-0.90" prefix="input" %>
<%
    String path = application.getRealPath(model.menuService.getRealUrl(request.getRequestURI()));
    String datos[] = model.menuService.getContenidoMenu(BASEURL,request.getRequestURI(),path);
%>
<html>
<head>
<title>.: Buscar Perfil</title>
<link href="<%= BASEURL %>/css/estilostsp.css" rel='stylesheet'>
<link href="../css/estilostsp.css" rel="stylesheet" type="text/css">
<link href="../../../css/estilostsp.css" rel="stylesheet" type="text/css">
</head>
<script type='text/javascript' src="<%= BASEURL %>/js/validar.js"></script>
<script type='text/javascript' src="<%= BASEURL %>/js/boton.js"></script>
<body onresize="redimensionar()" onload = 'redimensionar()'>
<div id="capaSuperior" style="position:absolute; width:100%; height:100px; z-index:0; left: 0px; top: 0px;">
 <jsp:include page="/toptsp.jsp?encabezado=Perfil - Buscar"/>
</div>
<div id="capaCentral" style="position:absolute; width:100%; height:87%; z-index:0; left: 0px; top: 100px; overflow: scroll;">
<form name="forma" id="forma" method="post" action="<%=CONTROLLER%>?estado=Perfil&accion=Buscar&carpeta=/jsp/trafico/perfil&pagina=Perfiles.jsp&tipo=Busqueda">
<br>
<table width="416" border="1" align="center">
  <tr>
    <td><table width="100%"  class="tablaInferior">
  <tr align="center" class="barratitulo"> 
    <td colspan="2"><table width="100%"  border="0">
      <tr>
        <td width="39%" class="subtitulo1">Buscar Perfil</td>
        <td width="61%"><img src="<%=BASEURL%>/images/titulo.gif" width="32" height="20" align="left"><%=datos[0]%></td>
      </tr>
    </table>      </td>
  </tr>
  <tr class="fila"> 
    <td width="125">Perfil</td>
    <td width="291">
		<input name="idp" type="text" class="textbox" maxlength="12"></td>
  </tr>
  <tr class="fila">
    <td width="125">Nombre</td>
    <td width="291">
	  <input name="nom" type="text" class="textbox" maxlength="15"></td>  
  <tr class="fila">
    <td colspan="2"><a href="<%=CONTROLLER%>?estado=Perfil&accion=Buscar&carpeta=/jsp/trafico/perfil&pagina=Perfiles.jsp&tipo=todo" class="Simulacion_Hiper" target="_self">Ver Todos</a>
	</td>
  </tr>
</table></td>
  </tr>
</table><br>
<center>
<input type="image" name="Submit" value="Ingresar" src="<%= BASEURL %>/images/botones/buscar.gif" align="absmiddle" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" style="cursor:hand ">
<img src="<%=BASEURL%>/images/botones/salir.gif" name="c_salir" onClick="window.close();" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" align="absmiddle" style="cursor:hand ">
<br>
<%if( (request.getParameter("msg")!=null) && (request.getParameter("msg").equals("ok"))){%>
<br>
<table border="2" align="center">
   		<tr>
        	<td>
            	<table width="100%"  border="1" align="center"  bordercolor="#F7F5F4" bgcolor="#FFFFFF">
                	<tr>
                    	<td width="229" align="center" class="mensajes">Su busqueda no arrojo resultados!!!</td>
                        <td width="29" background="<%//=BASEURL%>/images/cuadronaranja.JPG">&nbsp;</td>
                        <td width="58">&nbsp;</td>
                    </tr>
                </table>
            </td>
		</tr>
	</table>
<%}%>
</center>
</form>
</div>
<%=datos[1]%>
</body>
</html>