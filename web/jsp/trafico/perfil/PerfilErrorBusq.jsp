<!--
- Autor : Ing. Sandra M Escalante G
- Date  : 25 Abril 2005
- Copyrigth Notice : Fintravalores S.A. S.A
-->
<%--
-@(#)
--Descripcion : Pagina JSP, muestra error cuando la busqueda es fallida
--%>

<%-- Declaracion de librerias--%>
<%@page contentType="text/html"%>
<%@ page session="true"%>
<%@ page errorPage="../error/ErrorPage.jsp"%>
<%@page import="com.tsp.operation.model.beans.*, java.util.*"%>
<%@page import="com.tsp.operation.model.*"%>
<%@page import="com.tsp.util.*"%>
<%@include file="/WEB-INF/InitModel.jsp"%>
<%@ taglib uri="http://jakarta.apache.org/taglibs/input-0.90" prefix="input" %>
<html>
<head>
<title>.: Error Busqueda</title>
<link href="<%= BASEURL %>/css/estilo.css" rel='stylesheet'>
<link href="../css/estilo.css" rel="stylesheet" type="text/css">
</head>
<body>
<table width="100%">
  <tr>
    <td height="50" class="titulo">
	</td>
  </tr>
  <tr>
    <td height="60" class="fila" align="center">
	  Su busqueda no arroj� resultados.
	</td>
  </tr>
  <tr>
    <td height="50" class="titulo">&nbsp;
	</td>
  </tr>
</table>
<br>
<a href="<%=BASEURL%>/jsp/trafico/perfil/BuscarPerfil.jsp?tipo=Busqueda" class="letra_resaltada">Atras</a>
</body>
</html>