<!--
- Autor : Ing. Sandra M Escalante G
- Date  : 25 Abril 2005
- Copyrigth Notice : Fintravalores S.A. S.A
-->
<%--
-@(#)
--Descripcion : Pagina JSP, que se encarga de gestionar 
				el formulario paraingresar un perfil con sus opciones
--%>

<%-- Declaracion de librerias--%>
<%@page contentType="text/html"%>
<%@ page session="true"%>
<%@ page errorPage="../error/ErrorPage.jsp"%>
<%@page import="com.tsp.operation.model.beans.*, java.util.*"%>
<%@page import="com.tsp.operation.model.*"%>
<%@page import="com.tsp.util.*"%>
<%@include file="/WEB-INF/InitModel.jsp"%>
<%@ taglib uri="http://jakarta.apache.org/taglibs/input-0.90" prefix="input" %>
<%
    String path = application.getRealPath(model.menuService.getRealUrl(request.getRequestURI()));
    String datos[] = model.menuService.getContenidoMenu(BASEURL,request.getRequestURI(),path);
%>
<html>
<head>
<%Perfil perfil = (Perfil)request.getAttribute("perfil");
String idp = (perfil != null )?perfil.getId():"";
String nom = (perfil != null )?perfil.getNombre():""; 

String tipo = request.getParameter("tipo");//tipo de accion se va a realizar
int sw = ( tipo.equals("Insertar") )?0:1;//controla que arbol mostrar Ingreso o Modificacion 
String encabezado = ( tipo.equals("Insertar") )?".: Nuevo Perfil":".: Modificar/Anular Perfil";//encabezado de la pagina
String titulo = ( tipo.equals("Insertar") )?"Ingresar":"Modificar/Anular";//Titulo del formulario
String action = ( tipo.equals("Insertar") )?
					CONTROLLER + "?estado=Perfil&accion=Agregar&carpeta=/jsp/trafico/perfil&pagina=Perfil.jsp":
					CONTROLLER + "?estado=Perfil&accion=Modificar&carpeta=/jsp/trafico/perfil&pagina=Perfil.jsp";//controla la accion q se va a realizar
					
int sw1 = 0, swe = 0;//sw1 controla la vista de los botones, swe recuperacion de info por error

String msg = request.getParameter("msg");	
//out.println(" TIPO " + tipo + " ACTION " + action);
if ( tipo.equals("Insertar")){
	if (msg.equals("Error al insertar perfil " + request.getParameter("idp"))){
		idp = request.getParameter("idp");
		nom = request.getParameter("nom");
		swe = 1;
	}
}
%>
<title><%=encabezado%></title>
<link href="<%= BASEURL %>/css/estilostsp.css" rel='stylesheet'>
<link href="../../../css/estilostsp.css" rel="stylesheet" type="text/css">
</head>
<script type='text/javascript' src="<%= BASEURL %>/js/validar.js"></script>
<script type='text/javascript' src="../js/validar.js"></script>
<script type='text/javascript' src="<%= BASEURL %>/js/boton.js"></script>

  <script>
     function  seleccionar(theForm, elemento){
         for(var i=0;i< theForm.length;i++){
            var ele = theForm.elements[i];
            if( ele.type=='checkbox'  && ele.src == elemento.value  )
                ele.checked = elemento.checked;
         
         }
     
     }
  </script>




<body id="body" onresize="redimensionar();" onLoad="redimensionar();">
<div id="capaSuperior" style="position:absolute; width:100%; height:100px; z-index:0; left: 0px; top: 0px;">
 <jsp:include page="/toptsp.jsp?encabezado=Perfil - Nuevo"/>
</div>
<div id="capaCentral" style="position:absolute; width:100%; height:87%; z-index:0; left: 0px; top: 100px; overflow: scroll;">

<FORM name="forma" id="forma" method='POST' action="<%=action%>" onSubmit="return validarCamposPerfil();">
<table width="430" border="2" align="center">
  <tr>
    <td><table width="100%"  class="tablaInferior">
    <tr align="center" class="barratitulo"> 
      <td colspan="2"><table width="100%"  border="0">
        <tr>
          <td width="52%" class="subtitulo1"><%=titulo%> Perfil</td>
          <td width="48%"><img src="<%=BASEURL%>/images/titulo.gif" width="32" height="20" align="left"><%=datos[0]%></td>
        </tr>
      </table>        </td>
    </tr>
    
    <tr class="fila"> 
      <td width="119">Id</td>
      <td width="227">
	  <input name="idp" id="idp" type="text" class="textbox" value="<%=idp%>" maxlength="12" <%if ( sw == 1 ){%>readonly="true"<%}%> onKeyPress="soloAlfa(event);">
	  <img src="<%=BASEURL%>/images/botones/iconos/obligatorio.gif" width="10" height="10">
    </tr>
    <tr class="fila"> 
      <td width="119">Nombre</td>
      <td width="227"><input name="nom" id="nom" type="text" class="textbox" value="<%=nom%>" maxlength="60" onKeyPress="soloAlfa(event);">
	  <img src="<%=BASEURL%>/images/botones/iconos/obligatorio.gif" width="10" height="10"></td>
    </tr>

 <tr class="fila">
      <td colspan="2"><a href="<%=CONTROLLER%>?estado=Perfil&accion=Crear" style="cursor:hand">Crear el perfil en base a perfiles existentes.<a></td>
    </tr>	
	<% if ( sw == 0 ){%>        
    <tr class="fila">
      <td colspan="2" height="40"><font size="-2">Opciones del Menu</font><font size="-3"> Selecciones las opciones a las cuales tendr&aacute; acceso el Usuario que ingrese con este Perfil.</font></td>
    </tr>
	<tr class="fila" align="center"> 
     <td colspan="2">
	 
	 <!--INGRESAR PERFIL-->
       <table width="100%" border="0" cellpadding="0" cellspacing="0">
         <tr class="Titulos"> 
             <td width="8%"><img src="<%= BASEURL %>/images/menu-images/home.gif" border="0"></td>
             <td width="92%">&nbsp;[ FINTRAVALORES  S. A. ]</td>
         </tr>
		 <tr>
		   <td colspan="2">
		   		<%	//obtengo el vector de opciones
					Vector v = model.menuService.getOpciones();
					//si viene de error obtengo el vector de checbox para actualizar con conf anterior
					String []vop = (request.getParameter("opciones") != null)?request.getParameterValues("opciones"):new String[0];	
		   			//inicio de la tabla
					out.println ( "<table width = '100%' border='0' cellspacing=0>");
					//recorro opciones para establecer conficuracion de cada una	
		                        int padre = 0;
					for (int i = 0; i < v.size(); i++){				
						Menu m = (Menu) v.elementAt(i);	
                                                
                                                //checkbox
						String checkbox = "";
						
                                                //resaltar raiz
						String res = (m.getIdpadre()==0)?"subtitulo1":(m.getSubmenu()==1)?"filaazul letraresaltada":"filagris";
						//imagen de la opcion (carpate/hoja)
						String imagen = (m.getSubmenu()==1)?"<img src=" + BASEURL + "/images/menu-images/menu_folder_open.gif>":"<img src=" + BASEURL + "/images/menu-images/menu_link_default.gif>";
                                                        
                                                
						//espacion en el arbol
						String esp = "";
						for (int j = 1; j < m.getNivel(); j++){				
							esp += "&nbsp&nbsp";
                                                        
						}
                                                
                                                
                                                
                                                
                                                
                                                if (swe == 0 && m.getSubmenu() == 1){
                                                    if( m.getIdpadre()>0   ){
                                                        checkbox = "<input name=opciones id=opciones type=checkbox value=" + m.getIdopcion() + " onclick='seleccionar(this.form,this)' >";    
                                                        padre = m.getIdopcion();
                                                    }
                                                }else if ( swe != 1 && m.getSubmenu() != 1 ){ //error al ingresar perfil, recupero check seleccionados anteriormente
							checkbox = "<input name=opciones id=opciones type=checkbox value=" + m.getIdopcion() + ">";
							for ( int z = 0; z < vop.length; z++ ){
	  							if( vop[z].equals( ""+m.getIdopcion() ) ){
                                                                    checkbox = "<input name=opciones type=checkbox value=" + m.getIdopcion() + " checked>";
								}
							}	
						}
                                                
                                                
                                                
						if ( swe == 0 && m.getSubmenu() == 2 ){//ingresar perfil 
                                                    
                                                    checkbox = "<input name=opciones  title='hoja' src='"+ padre +"' id=opciones type=checkbox value=" + m.getIdopcion() + " >";
                                                    
						}else if ( swe == 1 && m.getSubmenu() == 2 ){ //error al ingresar perfil, recupero check seleccionados anteriormente
							checkbox = "<input name=opciones   src='"+ padre +"' id=opciones type=checkbox value=" + m.getIdopcion() + ">";
							for ( int z = 0; z < vop.length; z++ ){
	  							if( vop[z].equals( ""+m.getIdopcion() ) ){
                                                                    checkbox = "<input name=opciones   type=checkbox value=" + m.getIdopcion() + " checked>";
								}
							}	
						}
                                                
						//escribo opcion final 
						out.println("<tr class='" + res + "' ><td>" + esp + imagen + "</td><td>" + m.getNombre() + " [ " +  m.getIdopcion() + " ]</td>" +
							"<td>" + checkbox +  "</td></tr>");
					}
                                        out.println ( "</table>");%> 
			   </td>
		   </tr>			
  	  </table>
	  <!--FIN INGRESO PERFIL-->
	
	</td>
  </tr>
  <%}//fin if sw == 0
  else {//mostrar opciones del perfil en la pantalla Anular|Modificar}%>
  <tr class="fila">
      <td colspan="2" height="40"><font size="-2">Opciones del Menu</font><font size="-3"> Habilite\deshabilite las opciones que controlar&aacute;n el acceso del usuario al sistema.</font></td>
    </tr>
	<tr class="fila" align="center"> 
     <td colspan="2">

	 <!--TABLA DE MODIFICACION/ANULACION PERFIL-->
       <table width="100%" border="0" cellpadding="0" cellspacing="0">
         <tr class="Titulos"> 
           <td width="8%"><img src="<%= BASEURL %>/images/menu-images/home.gif" border="0"></td>
           <td width="92%">[ FINTRAVALORES  S. A. ]</td>
		 </tr>
		 <tr align="center">
		   <td colspan="2">
		   <%	//obtengo el vector de opciones
				Vector v = model.menuService.getOpciones();
				//obtengo opciones activas del perfil
				Vector opact = model.perfilService.getOpsactxprf();					
		   		//inicio de la tabla
				out.println ( "<table width = '100%' border='0' cellspacing=0>");
				//recorro opciones para establecer conficuracion de cada una			
				for (int i = 0; i < v.size(); i++){				
					Menu m = (Menu) v.elementAt(i);	
					//resaltar raiz
					String res = (m.getIdpadre()==0)?"subtitulo1":(m.getSubmenu()==1)?"filaazul letraresaltada":"filagris";
					//imagen de la opcion (carpate/hoja)
					String imagen = (m.getSubmenu()==1)?"<img src=" + BASEURL + "/images/menu-images/menu_folder_open.gif>":"<img src=" + BASEURL + "/images/menu-images/menu_link_default.gif>";						
					//espacio en el arbol
					String esp = "";
					for (int j = 1; j < m.getNivel(); j++){				
					esp += "&nbsp&nbsp";				
					}
					//checkbox
					String checkbox = "";
					//verifico si la opcion esta activa y chekeo 
					if (m.getSubmenu() == 2 ){
						checkbox = "<input name=opciones id=opciones type=checkbox value=" + m.getIdopcion() + ">";
						if (!request.getParameter("tipo").equals("Anular")){
							for ( int z = 0; z < opact.size(); z++ ){
								PerfilOpcion o = (PerfilOpcion) opact.elementAt(z);
								if (( m.getIdopcion() == o.getId_opcion() ) && ( o.getEstado().equals("A"))){							
									checkbox = "<input name=opciones id=opciones type=checkbox value=" + m.getIdopcion() + " checked>";
									break;
								}
							}
						}
					}
					//escribo opcion final 
					out.println("<tr class='" + res + "' ><td>" + esp + imagen + "</td><td>" + m.getNombre() + " [ " +  m.getIdopcion() + " ]</td>" +
							"<td>" + checkbox +  "</td></tr>");
				}
				out.println ( "</table>");%> 
				<br>		   
		   </td>
		 </tr>			
  	  </table>
  	 <!--FIN TABLA DE MODIFICACION/ANULACION PERFIL-->
	 
	</td>
  </tr>
  <%}%>
</table></td>
  </tr>
</table>
<br>
<center>
        <%if ( sw == 0 ){%>
			<%if (sw1 == 0){%>
				<input type="image" name="Submit" value="Ingresar" src="<%= BASEURL %>/images/botones/aceptar.gif" align="absmiddle" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" onclick ="controlaceptar(this);">
			<%}else if (sw1 == 1){%>
				<img name="Submit" value="Ingresar" src="<%= BASEURL %>/images/botones/aceptarDisabled.gif" align="absmiddle" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);">
			<%}%> 
  	<%} else {%>
		<input type="image" name="BtnMod" value="Modificar" src="<%= BASEURL %>/images/botones/modificar.gif" align="absmiddle" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" style="cursor:hand; " onClick="anularBtn(this);">
		<img src="<%=BASEURL%>/images/botones/anular.gif" name="anular" onClick="anularBtn(this);window.location='<%=CONTROLLER%>?estado=Perfil&accion=Anular&carpeta=/jsp/trafico/perfil&pagina=Perfil.jsp&idp=<%=idp%>';" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" align="absmiddle" style="cursor:hand " >
  	<%}%>
	        <img src="<%=BASEURL%>/images/botones/salir.gif" name="c_salir" onClick="window.close();" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" align="absmiddle" style="cursor:hand ">
</center>			
<% if (!msg.equals("") ){ %>
<br>
<script>alert('<%=msg%>');</script>
<!--<table border="2" align="center">
   		<tr>
        	<td>
            	<table width="100%"  border="1" align="center"  bordercolor="#F7F5F4" bgcolor="#FFFFFF">
                	<tr>
                    	<td width="229" align="center" class="mensajes"><%//=msg%></td>
                        <td width="29" background="<%//=BASEURL%>/images/cuadronaranja.JPG">&nbsp;</td>
                        <td width="58">&nbsp;</td>
                    </tr>
                </table>
            </td>
		</tr>
	</table>-->
<% } %>
</form>
</div>
<%=datos[1]%>
<script>
function anularBtn(btn){ 
	btn.style.visibility='hidden';
	alert ('Este procedo puede tardar unos minutos, por favor durante la ejecucion no seleccione ninguna otra opcion de este programa, esto puede generar error en la información. Cuando este finalice se mostrará un mensaje inidicando el estado del mismo...');
	btn.style.visibility='visible';
}

function controlaceptar(btn){
	btn.style.visibility='hidden';
	alert ('Este proceso puede tardar unos minutos, por favor espere...');
	btn.style.visibility='visible';
}

</script>
</body>
</html>
