<!--
- Autor : Ing. Sandra M Escalante G
- Date  : 18 de Mayo
- Copyrigth Notice : Fintravalores S.A. S.A
-->
<%--
-@(#)
--Descripcion : Ayuda
--%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
"http://www.w3.org/TR/html4/loose.dtd">
<%@include file="/WEB-INF/InitModel.jsp"%>
<html>
<head>
<title>.: Buscar Perfil - Ayuda - Descripcion Campos</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link href="../../../css/estilostsp.css" rel="stylesheet" type="text/css">
<link href="<%= BASEURL %>/css/estilostsp.css" rel="stylesheet" type="text/css">
<script type='text/javascript' src="<%= BASEURL %>/js/boton.js"></script>
</head>

<body>
<br>
<table width="696"  border="2" align="center">
  <tr>
    <td width="811" height="" >
      <table width="100%" border="0" align="center">
        <tr  class="subtitulo">
          <td height="24" colspan="2" align="center">BUSCAR PERFIL</td>
        </tr>
		<tr class="subtitulo1">
          <td colspan="2">INFORMACION</td>
        </tr>
        <tr>
          <td width="149" class="fila"> Codigo </td>
          <td width="525"  class="ayudaHtmlTexto">Digite aqui la informacion correpondiente para realizar la busqueda por codigo.</td>
        </tr>
        <tr>
          <td width="149" class="fila"> Nombre </td>
          <td width="525"  class="ayudaHtmlTexto">Digite aqui la informacion correpondiente para realizar la busqueda por nombre.</td>
        </tr>
    </table>    </td>
  </tr>
</table>
<br><br>
<br>
<center>
	<img src="<%=BASEURL%>/images/botones/salir.gif" name="c_salir" onClick="window.close();" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" align="absmiddle">
</center>
<br>
<p align="center" class="fuenteAyuda"> Fintravalores S. A. </p>
<br>
</body>
</html>