<!--
- Autor : Ing. Sandra M Escalante G
- Date  : 18 de Mayo
- Copyrigth Notice : Fintravalores S.A. S.A
-->
<%--
-@(#)
--Descripcion : Ayuda
--%>
<%@include file="/WEB-INF/InitModel.jsp"%>
<html>
    <head>
        <title>.: Perfil - Ayuda - Funcionalidad</title>
        <link href="../../../css/estilostsp.css" rel="stylesheet" type="text/css">
        <link href="<%= BASEURL %>/css/estilostsp.css" rel="stylesheet" type="text/css">
		<script type='text/javascript' src="<%= BASEURL %>/js/boton.js"></script>
    </head>
    <body>
        <BR>
        <table width="550"  border="2" align="center">
            <tr>
            <td width="550" >
                <table width="100%" border="0" align="center">
                    <tr  class="subtitulo">
                        <td height="24" align="center">PERFIL</td>
                    </tr>
                    <tr class="subtitulo1">
                        <td> FORMULARIO PARA EL REGISTRO DE UN PERFIL </td>
                    </tr>
                    <tr class="ayudaHtmlTexto">
                        <td align="center">
                            <br>
                            <img src="<%= BASEURL %>/images/ayuda/trafico/perfil/NuevoPerfil1.JPG" align="absmiddle">	
                            <br>	
						</td>
					</tr>	
					<tr class="subtitulo1">
                        <td> FORMULARIO PARA MODIFICAR/ANULAR PERFIL </td>
                    </tr>
                    <tr class="ayudaHtmlTexto">
                        <td align="center">
                            <br>
                            <img src="<%= BASEURL %>/images/ayuda/trafico/perfil/ModificarPerfil1.JPG" align="absmiddle">	
                            <br>	
						</td>
					</tr>	
					<tr class="subtitulo1">
                        <td> MENSAJE DE CONFIRMACION </td>
                    </tr>
					<tr class="ayudaHtmlTexto">
                        <td>    
							<br>
							Luego de registrar el perfil en el sistema, le debe salir el siguiente mensaje confirmando la accion.                       
							<br>
							<center>
                        		<img src="<%= BASEURL %>/images/ayuda/trafico/perfil/MsgNuevoPerfil.JPG" align="absmiddle" >
							</center>	
							<br>
							Luego de modificar el perfil en el sistema, le debe salir el siguiente mensaje confirmando la accion.                       
							<br>
							<center>
                        		<img src="<%= BASEURL %>/images/ayuda/trafico/perfil/MsgModificarPerfil.JPG" align="absmiddle" >
							</center>	
							<br>
							Luego de anular un perfil en el sistema, le debe salir el siguiente mensaje confirmando la accion.                       
							<br>
							<center>
                        		<img src="<%= BASEURL %>/images/ayuda/trafico/perfil/MsgAnularPerfil.JPG" align="absmiddle" >
							</center>							
						</td>
                    </tr>
					<tr class="subtitulo1">
                        <td> ADVERTENCIAS </td>
                    </tr>
					<tr class="ayudaHtmlTexto">
                        <td>                           
							<br>	
							Si Usted no ingresa los datos minimos necesarios para el registro o modificacion de la opcion, en la pantalla se mostraran los siguientes mensajes.
							<br>
							<br>
                            <center>
								<img src="<%= BASEURL %>/images/ayuda/trafico/perfil/Adv1.JPG" align="absmiddle" >	
                            </center>
							<br>
							<center>
                                <img src="<%= BASEURL %>/images/ayuda/trafico/perfil/Adv2.JPG" align="absmiddle" >	
							</center>
                            <br>												
                        </td>
                    </tr>                    
                </table>
            </td>
            </tr>
        </table>
		<br>
		<center>
			<img src="<%=BASEURL%>/images/botones/salir.gif" name="c_salir" onClick="window.close();" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" align="absmiddle">
		</center>
		<br>
        <p align="center" class="fuenteAyuda"> Fintravalores S. A. </p>
		<br>
    </body>
</html>