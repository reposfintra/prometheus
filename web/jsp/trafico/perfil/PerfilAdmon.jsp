<%-- 
    Document   : PerfilAdmon
    Created on : 25/06/2015, 08:28:15 AM
    Author     : egonzalez
--%>

<%@page import="com.google.gson.JsonArray"%>
<%@page import="com.google.gson.JsonObject"%>
<%@page import="com.tsp.operation.controller.PerfilAdmonAction"%>
<%@page contentType="text/html" pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
        <link href="/fintra/css/estilostsp.css" rel="stylesheet" type="text/css">
        <link type="text/css" rel="stylesheet" href="/fintra/css/jquery/jquery-ui/jquery-ui.css" />

        <title>Perfil Administracion</title>
        <script type="text/javascript" src="/fintra/js/jquery/jquery-ui/jquery.min.js"></script>
        <script type="text/javascript" src="/fintra/js/jquery/jquery-ui/jquery.ui.min.js"></script>
        <script src="/fintra/js/PerfilAdmon.js" type="text/javascript"></script>

        <style>
            .menu{
                color : #F0F0F0;
                background-color: #008000;
            }
            .menu label {
                color : #F0F0F0;
                padding-left: 20px;
                background: #008000 url("/fintra/images/menu-images/menu_folder_open.gif") no-repeat scroll 0% 50% / 15px 15px;
            }
            .opcion{
                background-color: #F0F0F0;
            }
            .opcion label {
                padding-left: 18px;
                background: #F0F0F0 url("/fintra/images/menu-images/menu_bar.gif") no-repeat scroll 0% 50% / 15px 15px;
            }
        </style>
    </head>
    <%
        boolean cmd = (request.getParameter("cmd") != null && request.getParameter("cmd").equalsIgnoreCase("cgaperfil"));
        String idperfil = (request.getParameter("idp") != null)
                           ? request.getParameter("idp") : "";
                
        PerfilAdmonAction pa = new PerfilAdmonAction();
        JsonObject opciones = pa.buscarOpciones(idperfil);
        JsonArray vector;
        JsonObject elemento;
    %>
    <body>
        <jsp:include page="/toptsp.jsp?encabezado=Administración de Usuarios"/>
        <div id="capaCentral" style="text-align: center;">
            <center>
                <div class="ui-jqgrid ui-widget ui-widget-content ui-corner-all"
                     style="width: 460px;  margin: 1.2em; padding: 0.2em;">
                    <table>
                        <tr><td colspan="4" class="ui-dialog-titlebar ui-widget-header ui-corner-all ui-helper-clearfix" style="text-align: center;">INFORMACION DEL PERFIL</td></tr>
                        <% elemento = opciones.getAsJsonObject("perfil");%>
                        <tr>
                            <td>ID de Perfil <span style="color:red;">*</span></td>
                            <td><input type="text" id="codigo" maxlength="12" 
                                       value="<%=(elemento != null && elemento.get("id_perfil") != null)?elemento.get("id_perfil").getAsString() : ""%>" <%=(cmd)?"disabled":""%>></td>
                            <td>Estado</td>
                            <td>
                                <select id="estado" style="width: 96%;">
                                    <%if(!cmd) {%>
                                    <option value="N">ACTIVO</option>
                                    <%} else {%>
                                    <option value="" <%=(elemento != null && elemento.get("estado").getAsString().equalsIgnoreCase(""))?"selected" : ""%>>ACTIVO</option>
                                    <option value="A" <%=(elemento != null && elemento.get("estado").getAsString().equalsIgnoreCase("A"))?"selected" : ""%>>INACTIVO</option>
                                    <%}%>
                                </select>
                            </td>
                        </tr>
                        <tr>
                            <td>Nombre Perfil <span style="color:red;">*</span></td>
                            <td colspan="3">
                                <input type="text" id="nombre" style="width: 97%;" maxlength="60" 
                                       value="<%=(elemento != null && elemento.get("nombre") != null)?elemento.get("nombre").getAsString() : ""%>"></td>
                        </tr>
                        <tr>
                            <td colspan="3" style="border-top: 1px solid rgb(0, 128, 0); padding: 0.5em;">
                                <span id="mensaje" style="color:red;"></span>
                            </td>
                            <td style="border-top: 1px solid rgb(0, 128, 0); padding: 0.5em; text-align: right;">
                                <%if(!cmd) {%>
                                <button id="aceptar_btn" class="ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only" role="button" aria-disabled="false"
                                        onclick="crearPerfil('1')">
                                    <span class="ui-button-text">Crear</span>
                                </button>
                                <%} else {%>
                                <button id="actualizar_btn" class="ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only" role="button" aria-disabled="false"
                                        onclick="crearPerfil('2')">
                                    <span class="ui-button-text">Actualizar</span>
                                </button>
                                <%}%>
                            </td>
                        </tr>
                    </table>
                </div>
                <div>
                    <div class="ui-jqgrid ui-widget ui-widget-content ui-corner-all"
                         style="max-width: 800px;">
                        <div class="ui-dialog-titlebar ui-widget-header ui-corner-all ui-helper-clearfix">
                            <span class="ui-dialog-title" id="ui-dialog-title-listaGlobal">
                                OPCIONES
                            </span>
                        </div>
                        <table id="tabla_opciones">
                            <thead>
                                <tr id="titulos" class="menu">
                                    <td></td>
                                </tr>
                            </thead>
                            <tbody id="opciones">
                            <script>
                                cargarOpciones(<%=opciones%>);
                            </script>
                            </tbody>
                        </table>
                    </div>
                    <br>
                </div>
            </center>
        </div>

        <div id="dialogLoading" style="display:none;">
            <p style="font-size: 12px;text-align:justify;" id="msj">Texto </p> <br/>
            <center>
                <img src="/fintra/images/cargandoCM.gif"/>
            </center>
        </div>
    </body>
</html>
