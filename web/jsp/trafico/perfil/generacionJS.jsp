<!--  
     - Author(s)       :      Luis Frieri
     - Date            :      22/01/2007  
     - Copyright Notice:      TRANSPORTES SANCHEZ POLO S.A.
  -->
 <%--   
     - @(#)  
     - Description: Funcion de generacion de variable de Javascript
 --%> 
<%!
    public String SEPARADOR = "~";
    public String convertir(String cadena){
        return cadena.replaceAll("'", "");
    }
    public void showDatosJs (List Listado, JspWriter out, String Variable)throws Exception{
        out.print("var "+ Variable +" = [ ");
        if (Listado!=null){
            for (int i=0; i<Listado.size(); i++){
                General dt = (General) Listado.get(i);
                String item = "\n['"+  dt.getCodigo() + SEPARADOR + convertir(dt.getDescripcion()) +  "']";
                out.print (item);
                if ((i+1)!=Listado.size()) out.print(",");
            }
        }
        out.print("];");
    }
%>