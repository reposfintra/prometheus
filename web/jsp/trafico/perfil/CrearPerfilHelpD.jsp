<%@ include file="/WEB-INF/InitModel.jsp"%>

<html>
<head>
<title>Descripci&oacute;n del programa de creacion de perfiles</title>

<script type='text/javascript' src="<%=BASEURL%>/js/boton.js"></script>
<link href="<%=BASEURL%>/css/estilostsp.css" rel="stylesheet" type="text/css">
<link href="../../../../css/estilostsp.css" rel="stylesheet" type="text/css">
</head>

<body>
<% String BASEIMG = BASEURL +"/images/botones/"; %> 
<br>
<table width="696"  border="2" align="center">
  <tr>
    <td width="811" >
      <table width="100%" border="0" align="center">
        <tr  class="subtitulo">
          <td height="24" colspan="2"><div align="center">DESCRIPCI&OACUTE;N DEL PROGRAMA DE CREACI&OACUTE;N DE PERFILES</div></td>
        <tr>
          <td  width="123" class="fila"> Id </td>
          <td  width="551" class="ayudaHtmlTexto"> Campo en el cual se debera digitar el Id del perfil a crear </td>
        </tr>
        <tr>
          <td  width="123" class="fila"> Nombre </td>
          <td  width="551" class="ayudaHtmlTexto"> Campo en el cual se debera digitar el Nombre del perfil a crear </td>
        </tr>
        <tr>
          <td width="123"  class="fila"> Perfiles Existentes </td>
          <td width="551"  class="ayudaHtmlTexto"> Lista de la cual se deben seleccionar los perfiles base en la creaci&oacute;n del nuevo perfil </td>
        </tr>
        <tr>
          <td width="123"  class="fila"> Perfiles Seleccionados </td>
          <td width="551"  class="ayudaHtmlTexto"> Lista en la cual se encuentran los perfiles seleccionados </td>
        </tr>
        <tr>
          <td width="123"  class="fila"> Flecha Derecha </td>
          <td width="551"  class="ayudaHtmlTexto"> A�ade los perfiles seleccionados </td>
        </tr>
        <tr>
          <td width="123"  class="fila"> Flecha Izquierda </td>
          <td width="551"  class="ayudaHtmlTexto"> Quita los perfiles seleccionados </td>
        </tr>
	<tr>
          <td width="123"  class="fila"> Bot&oacute;n Aceptar </td>
          <td width="551"  class="ayudaHtmlTexto"> Inicia el proceso de creaci&oacute;n del perfil </td>
        </tr>
        <tr>
          <td width="123"  class="fila"> Bot&oacute;n Salir </td>
          <td width="551"  class="ayudaHtmlTexto"> Cierra el formulario </td>
        </tr>		
      </table>
    </td>
  </tr>
</table>
<br>
<table width="416" align="center">
	<tr>
	<td align="center">
		<img src="<%=BASEURL%>/images/botones/salir.gif" style="cursor:pointer" name="c_salir" onClick="window.close();" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" align="absmiddle">
	</td>
	</tr>
</table>
<p>&nbsp;</p>

<p>&nbsp;</p>
<p>&nbsp; </p>
</body>
</html>
