<%@page contentType="text/html"%>
<%@page session="true"%>
<%@page errorPage="../error/ErrorPage.jsp"%>
<%@page import="java.util.*"%>
<%@page import="com.tsp.operation.model.*"%>
<%@page import="com.tsp.util.*"%>
<%@page import="com.tsp.operation.model.beans.*, java.util.*"%>
<%@include file="/WEB-INF/InitModel.jsp"%>

<html>
<head>
<title>Buscar Zona Usuario</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link href="<%= BASEURL %>/css/estilostsp.css" rel="stylesheet" type="text/css"> 
<link href="../../../css/estilostsp.css" rel="stylesheet" type="text/css"> 
<script type='text/javascript' src="<%= BASEURL %>/js/validar.js"></script>
<script type='text/javascript' src="<%= BASEURL %>/js/boton.js"></script>
</head>

<body onresize="redimensionar()" onload = 'redimensionar()'>
<% 
   String men="",nom = "";
   String Titulo="Información";   
   String Nombre = "Nombre de usuario";
  
   %>
<div id="capaSuperior" style="position:absolute; width:100%; height:100px; z-index:0; left: 0px; top: 0px;">
	<jsp:include page="/toptsp.jsp?encabezado=Buscar Zona De Usuario"/>
</div>
<div id="capaCentral" style="position:absolute; width:100%; height:87%; z-index:0; left: 0px; top: 100px; overflow: scroll;">
<form name="forma" method="post" action="<%=CONTROLLER%>?estado=Buscar&accion=ZonaUsuarioNom&pagina=VerZonasUsuarioXBusq.jsp&carpeta=jsp/trafico/zona&mensaje=Modificar">
<table align="center" width="379" border="2">
    <tr>
        <td>
            <table width="100%" align="center">                  
                <tr class="subtitulo1">
                    <td><%=Titulo%></td>
                    <td width="205" class="barratitulo"><img src="<%=BASEURL%>/images/titulo.gif" width="32" height="20"></td>
                </tr>    
                <tr class="fila">
                    <td width="127" valign="middle" ><%=Nombre%></td>
                    <td width="210" valign="middle" >
                        <input name="nombre" class="textbox" type="text" id="nombre" maxlength="40" value="<%=nom%>">
                        <img src="<%=BASEURL%>/images/botones/iconos/obligatorio.gif" width="10" height="10"></img>
                    </td>
                </tr>                                
            </table>
        </td>
    </tr>
</table>
<br>
<table align=center>
        <tr>
            <td colspan="2" valign="middle" ><div align="center">
                <img title='Buscar' src="<%= BASEURL %>/images/botones/buscar.gif" onmouseover="botonOver(this);" onmouseout="botonOut(this);" onclick='forma.submit();'></img>
                <img title='Ver todas las zonas...' src="<%= BASEURL %>/images/botones/detalles.gif" onmouseover="botonOver(this);" onmouseout="botonOut(this);" onclick="window.open('<%=CONTROLLER%>?estado=ZonaUsuario&accion=VerTodos&carpeta=jsp/trafico/zona&pagina=VerZonasUsuario.jsp','','status=no,scrollbars=no,width=700,height=470,resizable=yes')"></img>
                <img title='Salir' src="<%= BASEURL %>/images/botones/salir.gif" onmouseover="botonOver(this);" onmouseout="botonOut(this);" onclick='parent.close();'></img>
            </div></td>
        </tr>        
    </table>
</form>
</div>
</body>
</html>
