<%@page contentType="text/html"%>
<%@page session="true"%>
<%@page errorPage="../error/ErrorPage.jsp"%>
<%@page import="java.util.*"%>
<%@page import="com.tsp.operation.model.*"%>
<%@page import="com.tsp.operation.model.beans.*"%>
<%@include file="/WEB-INF/InitModel.jsp"%>
<html>
<head>
<title>Cambio Zona</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link href="<%= BASEURL %>/css/estilostsp.css" rel="stylesheet" type="text/css"> 
<link href="/css/estilostsp.css" rel="stylesheet" type="text/css">
<script type='text/javascript' src="<%= BASEURL %>/js/validar.js"></script>
<script type='text/javascript' src="<%= BASEURL %>/js/boton.js"></script>
</head>
<% 	Planilla pla = model.planillaService.getPlanilla(); 
	Usuario usuario = (Usuario) session.getAttribute("Usuario");
	model.zonaService.listarZonasxUsuario(usuario.getLogin());
	List zonas = model.zonaService.obtenerZonas();%>
<body onLoad="redimensionar();" onResize="redimensionar();">
<div id="capaSuperior" style="position:absolute; width:100%; height:100px; z-index:0; left: 0px; top: 0px;">
<jsp:include page="/WEB-INF/toptsp.jsp?encabezado=Trasladar Planilla de Zona"/>
</div>

<div id="capaCentral" style="position:absolute; width:100%; height:85%; z-index:0; left: 0px; top: 100px; overflow: scroll;"> 
<form action="<%=CONTROLLER%>?estado=Zona&accion=Traslado&tipo=modificar" method="post" name="forma" onSubmit="return validarTCamposLlenos();">
  <table width="50%"  border="2" align="center">
  <tr>
    <td>
		<table width="100%" class="tablaInferior">
          <tr>
            <td colspan="2"><table width="100%"  border="0" cellspacing="0" cellpadding="0">
              <tr>
                <td width="46%" class="subtitulo1">PLANILLA Nro. <%= pla.getNumpla() %>
                  <input type="hidden" name="planilla" value="<%= pla.getNumpla() %>"></td>
                <td width="54%" class="barratitulo"><img src="<%=BASEURL%>/images/titulo.gif"></td>
              </tr>
            </table></td>
          </tr>
          <tr class="fila">
            <td width="27%">Origen:</td>
            <td width="73%"><%= pla.getNomori() %></td>
          </tr>
          <tr class="fila">
            <td >Destino:</td>
            <td><%= pla.getNomdest() %></td>
          </tr>
          <tr class="fila">
            <td >Placa:</td>
            <td><%= pla.getPlaveh() %></td>
          </tr>
          <tr class="fila">
            <td >Conductor:</td>
            <td><%= pla.getNomCond() %></td>
          </tr>
          <tr class="fila">
            <td >Zona:</td>
            <td><%= pla.getNomzona() %>
                <input name="azona" type="hidden" id="azona" value="<%= pla.getZona() %>"></td>
          </tr>
          <tr>
            <td colspan="2" class="subtitulo1">Cambio de Zona.</td>
          </tr>
          <tr>
            <td colspan="2" class="informacion">Seleccione la zona a la cual desea trasladar la planilla.</td>
          </tr>
          <tr class="fila">
            <td >Zona</td>
            <td><select name="nzona" class="textbox" id="select">
                <option value="" selected>Seleccione</option>
                <%	Iterator it = zonas.iterator();
			while (it.hasNext()){
				Zona z =  (Zona) it.next();
		%>
                <option value="<%= z.getCodZona() %>"><%= z.getNomZona() %></option>
                <% 	} %>
              </select>
		    </td>
          </tr>
        </table>
    </td>
  </tr>
</table>
<br>
<center>
  <img src="<%=BASEURL%>/images/botones/aceptar.gif" title="Modificacr Zona" name="c_imgaceptar"  onClick="if(forma.nzona.value != ''){forma.submit()}else{alert('Seleccione la zona')}" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);"> 
  <img src="<%=BASEURL%>/images/botones/salir.gif"  name="salir" title="Salir..." onClick="parent.close();" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);">
  <img src="<%=BASEURL%>/images/botones/regresar.gif" name="c_regresar" id="c_regresar" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" onClick="window.location='<%=CONTROLLER%>?estado=Menu&accion=Cargar&carpeta=/jsp/trafico/zona&pagina=planillaxnro.jsp'">
</center>
</form>
<%String msg = "";//out.println(request.getParameter("mensaje"));
if (request.getParameter("mensaje")!=null){
	msg=request.getParameter("mensaje"); 
	//out.println(msg);%>
	<br>
<table border="2" align="center">
  <tr>
    <td>
	<table width="100%"  border="1" align="center"  bordercolor="#F7F5F4" bgcolor="#FFFFFF">
      <tr>
        <td width="229" align="center" class="mensajes"><%= msg %></td>
        <td width="29"><img src="<%=BASEURL%>/images/cuadronaranja.JPG"></td>
      </tr>
    </table></td>
  </tr>
</table>
<br>
<% } %>
</div>
</body>
</html>
