<%@page contentType="text/html"%>
<%@page session="true"%>
<%@page import="java.util.*"%>
<%@page import="com.tsp.operation.model.*"%>
<%@page import="com.tsp.operation.model.beans.*"%>
<%@page import="com.tsp.util.*"%>
<%@include file="/WEB-INF/InitModel.jsp"%>
<%@ taglib uri="http://jsptags.com/tags/navigation/pager" prefix="pg" %>
<html>
<head>
<title>Error en la Busqueda</title>
<link href="<%= BASEURL %>/css/estilostsp.css" rel="stylesheet" type="text/css"> 
<link href="../../../css/estilostsp.css" rel="stylesheet" type="text/css"> 
<script type='text/javascript' src="<%= BASEURL %>/js/reporte.js"></script> 
<script type='text/javascript' src="<%= BASEURL %>/js/boton.js"></script>
</head>
<body onresize="redimensionar()" onload = 'redimensionar()'>
<div id="capaSuperior" style="position:absolute; width:100%; height:100px; z-index:0; left: 0px; top: 0px;">
	<jsp:include page="/toptsp.jsp?encabezado=Listado De Zonas"/>
</div>
<div id="capaCentral" style="position:absolute; width:100%; height:87%; z-index:0; left: 0px; top: 100px; overflow: scroll;">
<table width=500 border=2 align=center>
    <tr>
        <td>
            <table width="100%" border=1 align=center  bordercolor="#F7F5F4" bgcolor="#FFFFFF">
                <tr>
                    <td width="50%" align=center class=mensajes>No se encontraron resultados</td>
                    <td width="50%"><img src="<%=BASEURL%>/images/cuadronaranja.JPG"></td>
                </tr>
            </table>
        </td>
    </tr>
  </table><br>
<table width="450" align="center" cellpadding="2" cellspacing="1">
        <tr>
            <td>
                <img title='Regresar' src="<%= BASEURL %>/images/botones/regresar.gif" onmouseover="botonOver(this);" onmouseout="botonOut(this);" onclick="parent.close();"></td>
        </tr>
    </table>
</div>
</body>
</html>