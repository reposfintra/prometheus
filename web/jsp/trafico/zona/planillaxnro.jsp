<%@page contentType="text/html"%>
<%@page session="true"%>
<%@page errorPage="../error/ErrorPage.jsp"%>
<%@page import="java.util.*"%>
<%@page import="com.tsp.operation.model.*"%>
<%@page import="com.tsp.util.*"%>
<%@page import="com.tsp.operation.model.beans.*, java.util.*"%>
<%@include file="/WEB-INF/InitModel.jsp"%>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<title>Buscar Planilla</title>
<link href="<%= BASEURL %>/css/estilostsp.css" rel="stylesheet" type="text/css"> 
<link href="../../../css/estilostsp.css" rel="stylesheet" type="text/css">
<script type='text/javascript' src="<%= BASEURL %>/js/validar.js"></script>
<script type='text/javascript' src="<%= BASEURL %>/js/boton.js"></script>
</head>
<body onLoad="redimensionar();" onResize="redimensionar();">
<div id="capaSuperior" style="position:absolute; width:100%; height:100px; z-index:0; left: 0px; top: 0px;">
<jsp:include page="/WEB-INF/toptsp.jsp?encabezado=Buscar Planilla"/>
</div>

<div id="capaCentral" style="position:absolute; width:100%; height:85%; z-index:0; left: 0px; top: 100px; overflow: scroll;"> 
<table width="50%"  border="2" align="center" cellpadding="0" cellspacing="0">
  <tr>
    <td><table width="100%" class="tablaInferior">
      <tr>
        <td colspan="2"><table width="100%"  border="0" cellpadding="0" cellspacing="1">
            <tr>
              <td width="67%" height="23" class="subtitulo1">&nbsp;Digite el codigo de la planilla</td>
              <td width="33%" class="barratitulo"><img src="<%=BASEURL%>/images/titulo.gif" width="32" height="20"></td>
            </tr>
        </table></td>
      </tr>
      <tr class="fila" valign="middle">
        <form action="<%=CONTROLLER%>?estado=Zona&accion=Traslado&tipo=buscar" method="post" name="forma" onSubmit="return validarTCamposLlenos();">
          <td>Planilla Nro.</td>
          <td nowrap><input type="text" name="planilla" class="textbox" id="planilla"></td>
        </form>
      </tr>
    </table></td>
  </tr>
</table><br>
<center><img src="<%=BASEURL%>/images/botones/buscar.gif" title="Buscar Planilla" name="imgbuscar"  onclick="return TCamposLlenos();" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);">
<img src="<%=BASEURL%>/images/botones/salir.gif"  name="salir" title="Cerrar ventana de busqueda" onClick="parent.close();" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);"></center>
<br>
<%String msg = "";
if (request.getParameter("mensaje")!=null){
	msg=request.getParameter("mensaje");%>
<table border="2" align="center">
  <tr>
    <td>
	<table width="100%"  border="1" align="center"  bordercolor="#F7F5F4" bgcolor="#FFFFFF">
      <tr>
        <td width="229" align="center" class="mensajes"><%= msg %></td>
        <td width="29"><img src="<%=BASEURL%>/images/cuadronaranja.JPG"></td>
      </tr>
    </table></td>
  </tr>
</table>
<% } %>
</div>
</body>
</html>
