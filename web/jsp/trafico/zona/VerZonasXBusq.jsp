<%@page contentType="text/html"%>
<%@page session="true"%>
<%@page errorPage="../error/ErrorPage.jsp"%>
<%@page import="java.util.*"%>
<%@page import="com.tsp.operation.model.*"%>
<%@page import="com.tsp.util.*"%>
<%@page import="com.tsp.operation.model.beans.*, java.util.*"%>
<%@include file="/WEB-INF/InitModel.jsp"%>
<%@taglib uri="http://jsptags.com/tags/navigation/pager" prefix="pg" %>

<html>
<head>
<title>Listado de Zonas</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link href="<%= BASEURL %>/css/estilostsp.css" rel="stylesheet" type="text/css"> 
<link href="../../../css/estilostsp.css" rel="stylesheet" type="text/css"> 
<script type='text/javascript' src="<%= BASEURL %>/js/reporte.js"></script> 
<script type='text/javascript' src="<%= BASEURL %>/js/boton.js"></script>
</head>

<body onresize="redimensionar()" onload = 'redimensionar()'>
<div id="capaSuperior" style="position:absolute; width:100%; height:100px; z-index:0; left: 0px; top: 0px;">
	<jsp:include page="/toptsp.jsp?encabezado=Listado De Zonas"/>
</div>
<div id="capaCentral" style="position:absolute; width:100%; height:87%; z-index:0; left: 0px; top: 100px; overflow: scroll;">
<% 
   String style = "simple";
   String position =  "bottom";
   String index =  "center";
   int maxPageItems = 10;
   int maxIndexPages = 10;
   Vector VecPais = model.zonaService.obtZonas();
   Zona zona;  
   if (VecPais.size() >0 ){%>
  <table width="379" border="2" align=center>
    <tr>
        <td>
            <table width="100%" align="center" class="tablaInferior"> 
                <tr>
                    <td colspan='2'>                                                
                        <table width="100%"  border="0" cellpadding="0" cellspacing="0" class="barratitulo">
                            <tr>
                                <td width="50%" class="subtitulo1" colspan='3'>Información</td>
                                <td width="50%" class="barratitulo" colspan='2'><img src="<%=BASEURL%>/images/titulo.gif"></td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td>
                        <table width="100%" border="1" align="center" bordercolor="#999999" bgcolor="#F7F5F4">
                                <tr class="tblTitulo">
                                    <td nowrap width="100"><div align="center">Codigo</div></td>
                                    <td nowrap width="279">&nbsp;&nbsp;Nombre</td>
                                </tr>    
                                    <pg:pager
                                        items="<%=VecPais.size()%>"
                                        index="<%= index %>"
                                        maxPageItems="<%= maxPageItems %>"
                                        maxIndexPages="<%= maxIndexPages %>"
                                        isOffset="<%= true %>"
                                        export="offset,currentPageNumber=pageNumber"
                                        scope="request">
                                    <%-- keep track of preference --%>

                                      <%
                                          for (int i = offset.intValue(), l = Math.min(i + maxPageItems, VecPais.size()); i < l; i++)
                                              {
                                              zona = (Zona) VecPais.elementAt(i);%>

                                    <pg:item>
                                <tr class="<%=(i % 2 == 0 )?"filagris":"filaazul"%>" onMouseOver='cambiarColorMouse(this)' style="cursor:hand" onClick="window.open('<%=CONTROLLER%>?estado=Buscar&accion=Zona&pagina=zona.jsp&carpeta=/jsp/trafico/zona&codigo=<%=zona.getCodZona()%>&mensaje=Modificar' ,'','status=no,scrollbars=no,width=750,height=470,resizable=yes')">
                                    <td class='bordereporte' ><%=zona.getCodZona()%></td>
                                    <td class='bordereporte'><%=zona.getNomZona()%></td>
                                </tr>
                                    </pg:item>
                                        <%}%>
                                <tr align="center">
                                        <td td height="20" colspan="10" nowrap align="center">
                                                <pg:index>
                                          <jsp:include page="/WEB-INF/jsp/googlesinimg.jsp" flush="true"/>
                                        </pg:index>
                                        </td>
                                </tr>
                                    </pg:pager>        
                        </table>
                    </td>
                </tr>
            </table>
        </td>
      </tr>
  </table>                
<%}
 else { %>

  <table width=379 border=2 align=center>
    <tr>
        <td>
            <table width="100%"  border=1 align=center  bordercolor="#F7F5F4" bgcolor="#FFFFFF">
                <tr>
                    <td align=center class=mensajes>No se encontraron resultados</td>
                    <td align="left" ><img src="<%=BASEURL%>/images/cuadronaranja.JPG"></td>
                </tr>
            </table>
        </td>
    </tr>
  </table>

<%}%>
<br>
 <table width='379' align=center>
  <tr class="titulo">
    <td align=left colspan='2'>
        <img title='Regresar' src="<%= BASEURL %>/images/botones/regresar.gif" onmouseover="botonOver(this);" onmouseout="botonOut(this);" onclick="parent.close();"></img>
    </td>
  </tr>
</table>
</div> 
</body>
</html>
