<%@page contentType="text/html"%>
<%@page session="true"%>
<%@page import="java.util.*"%>
<%@page import="com.tsp.operation.model.*"%>
<%@page import="com.tsp.util.*"%>
<%@page import="com.tsp.operation.model.beans.*, java.util.*"%>
<%@include file="/WEB-INF/InitModel.jsp"%>
<%@ taglib uri="http://jakarta.apache.org/taglibs/input-0.90" prefix="input" %>
<html>
<head>
<title>ZONA</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link href="<%= BASEURL %>/css/estilostsp.css" rel="stylesheet" type="text/css"> 
<link href="../../../css/estilostsp.css" rel="stylesheet" type="text/css"> 
<script type='text/javascript' src="<%= BASEURL %>/js/boton.js"></script>
<script type='text/javascript' src="<%= BASEURL %>/js/validar.js"></script>
</head>

<body <%if(request.getParameter("reload")!=null){%> onLoad="parent.opener.location.reload();"<%}%> onresize="redimensionar()" onload = 'redimensionar()'>
<% Zona zona;
   
   String men = "", cod = "", nom = "";   
   String action = CONTROLLER + "?estado=Insertar&accion=Zona";
   String Titulo = "Datos";   
   int sw=0,swcon=-1; 
if (request.getParameter("mensaje")!=null){
   String mensaje = request.getParameter("mensaje");
   if( mensaje.equals("ErrorA") ) {
       swcon=0;
	   men = "Ya existe esta zona";
   }		
   else if ( mensaje.equals("Agregado") ) {
       men ="La información ha sido agregada satisfactoriamente";
   }
   else if ( mensaje.equals("Modificar") ) {
       swcon=1;
	   sw = 1;
   	}
	else if ( mensaje.equals("Modificado") ) {
	    swcon=1;
	    sw = 1;	   
        men ="La información ha sido modificada satisfactoriamente";
   	}
	else if ( mensaje.equals("Anulado") ) {
        swcon = 0;
	    sw = 1;
	    men ="La zona ha sido eliminada";
   	}
	else if ( mensaje.equals("Error_Anular") ) {
	    swcon=0;
		sw = 1;
		men = "No puede eliminar esta zona";
   	}
}
	if ( swcon == 0 ){ //swcon = 0 capturo la informacion de la pag anterior
        cod = request.getParameter("codigo");
	    nom = request.getParameter("nombre");
	} 
	else if ( swcon == 1) { //swcon = 1 capturo el objeto con la información
	    zona = model.zonaService.obtenerZona(); 
            cod = zona.getCodZona();
            nom = zona.getNomZona();
	}
	
    if ( sw==1 ){
        Titulo="Modificar Zona";
	action = CONTROLLER + "?estado=Modificar&accion=Zona";
    }
	//out.println(mensaje);
	//out.println(swcon);
%>
<div id="capaSuperior" style="position:absolute; width:100%; height:100px; z-index:0; left: 0px; top: 0px;">
        <% if(sw==0){%>
               <jsp:include page="/toptsp.jsp?encabezado=Ingresar Zona"/>
	<% }
           else{%>
               <jsp:include page="/toptsp.jsp?encabezado=Modificar Zona"/>
           <%}%>
</div>
<div id="capaCentral" style="position:absolute; width:100%; height:87%; z-index:0; left: 0px; top: 100px; overflow: scroll;">
<form name="forma" method="post" action="<%=action%>">
    <table align="center" width="379" border="2">
        <tr>
            <td>
                <table width="100%" align="center">  
                    <tr>
                        <td class="subtitulo1" align=center>Datos</td>
                        <td width="205" class="barratitulo"><img src="<%=BASEURL%>/images/titulo.gif" width="32" height="20"></td>
                    </tr>    
                    <tr class="fila">
                        <td width="119" valign="middle" ><div align="left" >Codigo</div></td>
                        <td width="235" valign="middle">
                            <%if(sw==1){%>                                
                                <input type=hidden name='codigo' value='<%=cod%>'>
                                <% out.print(cod);
                            }
                            else{%>
                                <input name="codigo" type="text" class="textbox" maxlength="3"><img src="<%=BASEURL%>/images/botones/iconos/obligatorio.gif" width="10" height="10"></img>
                            <%}%>
                        </td>
                    </tr>
                    <tr class="fila">
                        <td valign="middle" >Nombre</td>
                        <td valign="middle">
                            <input name="nombre" type="text" class="textbox" maxlength="40" value="<%=nom%>"><img src="<%=BASEURL%>/images/botones/iconos/obligatorio.gif" width="10" height="10"></img>
                        </td>
                    </tr>    
                     </table>                
            </td>
        </tr>
    </table>
    <br>
                    <% if (sw==0){%>
                    <tr>
                        <td colspan="2"> <div align="center">
                            <img title='Aceptar' src="<%= BASEURL %>/images/botones/aceptar.gif" onmouseover="botonOver(this);" onmouseout="botonOut(this);" onclick='TCamposLlenos();'></img>  
                            <img title='Cancelar' src="<%= BASEURL %>/images/botones/cancelar.gif" onmouseover="botonOver(this);" onmouseout="botonOut(this);" onclick="forma.reset();"></img>
                            <img title='Salir' src="<%= BASEURL %>/images/botones/salir.gif" onmouseover="botonOver(this);" onmouseout="botonOut(this);" onclick="parent.close();"></img>
                        </div></td>
                    </tr>
                    <%} else {%>               
    <table align=center>
        <tr>
            <td colspan="2"><div align="center">
                <img title='Modificar' src="<%= BASEURL %>/images/botones/modificar.gif" onmouseover="botonOver(this);" onmouseout="botonOut(this);" onclick='forma.submit();'></img>
                <img title='Anular' src="<%= BASEURL %>/images/botones/anular.gif" onmouseover="botonOver(this);" onmouseout="botonOut(this);" onClick="window.location='<%=CONTROLLER%>?estado=Anular&accion=Zona&codigo=<%=cod%>&nombre=<%=nom%>'" >		
                <img title='Salir' src="<%= BASEURL %>/images/botones/salir.gif" onmouseover="botonOver(this);" onmouseout="botonOut(this);" onClick="parent.close();">
            </div></td>
        </tr>
        <%}%>
    </table><br>
         <%
            if(men!=null && !men.equalsIgnoreCase("")) {
                out.print("<table width=379 border=2 align=center><tr><td><table width=100%  border=1 align=center  bordercolor=#F7F5F4 bgcolor=#FFFFFF><tr><td width=350 align=center class=mensajes>"+men+"</td><td width=100><img src="+BASEURL+"/images/cuadronaranja.JPG></td></tr></table></td></tr></table>");            
            }%>	
</form>
</div>
</body>
</html>
