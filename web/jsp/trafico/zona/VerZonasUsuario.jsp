<%@page contentType="text/html"%>
<%@page session="true"%>
<%@page errorPage="../error/ErrorPage.jsp"%>
<%@page import="java.util.*"%>
<%@page import="com.tsp.operation.model.*"%>
<%@page import="com.tsp.util.*"%>
<%@page import="com.tsp.operation.model.beans.*, java.util.*"%>
<%@include file="/WEB-INF/InitModel.jsp"%>
<%@taglib uri="http://jsptags.com/tags/navigation/pager" prefix="pg" %>

<html>
<head>
<title>Listado de Zonas</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link href="<%= BASEURL %>/css/estilostsp.css" rel="stylesheet" type="text/css"> 
<link href="../../../css/estilostsp.css" rel="stylesheet" type="text/css"> 
<script type='text/javascript' src="<%= BASEURL %>/js/reporte.js"></script> 
</head>

<body <%if(request.getParameter("reload")!=null){%>onLoad="parent.close();parent.opener.location.reload();"<%}%>>
<div id="capaSuperior" style="position:absolute; width:100%; height:100px; z-index:0; left: 0px; top: 0px;">
	<jsp:include page="/toptsp.jsp?encabezado=Listado De Zonas De Usuarios"/>
</div>
<div id="capaCentral" style="position:absolute; width:100%; height:87%; z-index:0; left: 0px; top: 100px; overflow: scroll;">
  <% 
   String style = "simple";
   String position =  "bottom";
   String index =  "center";
   int maxPageItems = 10;
   int maxIndexPages = 10;

   model.zonaUsuarioService.zonas();
   Vector VecPais = model.zonaUsuarioService.obtZonas();
   ZonaUsuario zona;  
   if ( VecPais.size() > 0 ) { 
   %>
   <table width="450" border="2" align="center">
    <tr>
        <td>
            <table width="100%" align="center" class="tablaInferior"> 
                <tr>
                    <td colspan='2'>                                                
                        <table width="100%"  border="0" cellpadding="0" cellspacing="0" class="barratitulo">
                            <tr>
                                <td width="50%" class="subtitulo1" colspan='3'>Información</td>
                                <td width="50%" class="barratitulo" colspan='2'><img src="<%=BASEURL%>/images/titulo.gif"></td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td>
                        <table width="100%" border="1" align="center" bordercolor="#999999" bgcolor="#F7F5F4">
                            <tr class="tblTitulo">
                                <td nowrap width="100" align=center>Codigo</td>
                                <td nowrap width="279">&nbsp;&nbsp;Nombre</td>
                            </tr>
                                <pg:pager
                                    items="<%=VecPais.size()%>"
                                    index="<%= index %>"
                                    maxPageItems="<%= maxPageItems %>"
                                    maxIndexPages="<%= maxIndexPages %>"
                                    isOffset="<%= true %>"
                                    export="offset,currentPageNumber=pageNumber"
                                    scope="request">
                                    <%for (int i = offset.intValue(), l = Math.min(i + maxPageItems, VecPais.size()); i < l; i++){
                                        zona = (ZonaUsuario) VecPais.elementAt(i);%>
                                <pg:item>
                            <tr class="<%=(i % 2 == 0 )?"filagris":"filaazul"%>" onMouseOver='cambiarColorMouse(this)' style="cursor:hand"onClick="window.open('<%=CONTROLLER%>?estado=Buscar&accion=ZonaUsuario&pagina=zonaUsuario.jsp&carpeta=jsp/trafico/zona&codigo=<%=zona.getCodZona()%>&nombre=<%=zona.getNomUsuario()%>&mensaje=Modificar','','status=no,scrollbars=no,width=700,height=470,resizable=yes');">
                                <td class="bordereporte" nowrap align="center"><%=zona.getCodZona()%></td>
                                <td nowrap class="bordereporte"><%=zona.getNomUsuario()%></td>
                            </tr>
                                </pg:item>
                                    <%}%>
                            <tr>
                                <td td height="20" colspan="10" nowrap align="center">          <pg:index>
                                    <jsp:include page="/WEB-INF/jsp/googlesinimg.jsp" flush="true"/>
                                    </pg:index>
                                </td>
                            </tr>
                                </pg:pager>
                        </table>
                    </td>
                </tr>
            </table>
         </td>
      </tr>
   </table><br>
<table width="450" align="center" cellpadding="2" cellspacing="1">
    <%}
    else {%>      
    <tr class="pie" align="center">
        <td td height="20" colspan="2" nowrap align="center">No se encontraron registros </td>
    </tr>
    <%}%>     
    <tr class="titulo">
        <td align=right colspan='2'>
            <div align="left"><img title='Regresar' src="<%= BASEURL %>/images/botones/regresar.gif" onmouseover="botonOver(this);" onmouseout="botonOut(this);" onclick="parent.close();"></img>
            </div></td>
    </tr>
</table>
</div>
</body>
</html>
