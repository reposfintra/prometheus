<%@page contentType="text/html"%>
<%@page session="true"%>
<%@page import="java.util.*"%>
<%@page import="com.tsp.operation.model.*"%>
<%@page import="com.tsp.operation.model.beans.*"%>
<%@page import="com.tsp.util.*"%>
<%@include file="/WEB-INF/InitModel.jsp"%>
<html>
<head>
<title>Modificar Tipo De Ubicaci&oacute;n</title>
<link href="<%= BASEURL %>/css/estilostsp.css" rel="stylesheet" type="text/css">
<script type='text/javascript' src="<%= BASEURL %>/js/boton.js"></script>
</head>
<script type='text/javascript' src="<%= BASEURL %>/js/validar.js"></script>
<body <%if(request.getParameter("reload")!=null){%>onLoad="parent.opener.location.reload();"<%}%>>
<%
    Usuario usuario = (Usuario) session.getAttribute("Usuario");
    TipoContacto t = (TipoContacto) model.tipo_contactoService.getTipo_contacto();    
    String mensaje = (String) request.getAttribute("mensaje");
    Vector vec = new Vector();
%>
<FORM name='forma' id='forma' method='POST' onSubmit="return TCamposLlenos();" action='<%=CONTROLLER%>?estado=Tipo_contacto&accion=Update'>
    <table width="420" border="2" align="center">
      <tr>
        <td>
			<table width="100%" class="tablaInferior">
				<tr class="fila">
				  <td align="left" class="subtitulo1">&nbsp;Tipo Contacto</td>
				  <td align="left" class="barratitulo"><img src="<%=BASEURL%>/images/titulo.gif" width="32" height="20"></td>
				</tr>
				<tr class="fila">
					<td width="40%">Codigo</td>
					<td width="60%"><%=t.getCodigo()%><input name="c_codigo" type="hidden" id="c_codigo" value="<%=t.getCodigo()%>"></td>
				</tr>
				<tr class="fila">
					<td>Descripcion</td>
					<td><textarea name="c_descripcion" cols="30" class="textbox" id="c_descripcion"><%=t.getDescripcion()%></textarea></td>
				</tr>        
				<tr class="pie">
					<td align='center' colspan="3" >
					</td>
				</tr>        
			</table>
			</td>
		</tr>
	</table>
	<p>
		<div align="center"><img src="<%=BASEURL%>/images/botones/modificar.gif" style="cursor:hand" title="Agregar un codigo de discrepancia" name="modificar"  onclick="return TCamposLlenos();" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);">&nbsp;
		<img src="<%=BASEURL%>/images/botones/anular.gif" style="cursor:hand" title="Modificar un tipo de contacto" name="anular"  onClick="window.location='<%=CONTROLLER%>?estado=Tipo_contacto&accion=Anular&c_codigo=<%=t.getCodigo()%>'" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);">&nbsp;
		<img src="<%=BASEURL%>/images/botones/salir.gif" style="cursor:hand" name="regresar" title="Salir al Menu Principal" onMouseOver="botonOver(this);" onClick="parent.close();" onMouseOut="botonOut(this);" ></div>
	</p>
	<%  if(mensaje!=null){%>
		<table border="2" align="center">
		  <tr>
			<td><table width="100%"  border="1" align="center"  bordercolor="#F7F5F4" bgcolor="#FFFFFF">
			  <tr>
				<td width="229" align="center" class="mensajes"><%=mensaje%></td>
				<td width="29" background="<%=BASEURL%>/images/cuadronaranja.JPG">&nbsp;</td>
				<td width="58">&nbsp;</td>
			  </tr>
			</table></td>
		  </tr>
		</table>
	<%  }%>
</FORM>
</body>
</html>
