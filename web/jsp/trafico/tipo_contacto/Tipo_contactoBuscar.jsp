<%@page contentType="text/html"%>
<%@page session="true"%>
<%@page errorPage="/error/ErrorPage.jsp"%>
<%@page import="java.util.*"%>
<%@page import="com.tsp.operation.model.*"%>
<%@page import="com.tsp.operation.model.beans.*"%>
<%@page import="com.tsp.util.*"%>
<%@include file="/WEB-INF/InitModel.jsp"%>
<html>
<head><title>Buscar Tipo De Contacto</title>
<link href="<%= BASEURL %>/css/estilostsp.css" rel="stylesheet" type="text/css">
<script type='text/javascript' src="<%= BASEURL %>/js/boton.js"></script>
<link href="file:///C|/Documents%20and%20Settings/EQUIPO12/Escritorio/slt2/css/estilostsp.css" rel="stylesheet" type="text/css">
</head>
<body>
<%
    Usuario usuario = (Usuario) session.getAttribute("Usuario");%>

<form name="form2" method="post" action="<%=CONTROLLER%>?estado=Tipo_contacto&accion=Search&listar=True&sw=True">
 
    <table width="400" border="2" align="center">
      <tr>
        <td><table width="100%" align="center"  class="tablaInferior">
            <tr>
              <td width="173" class="subtitulo1">Tipo Contacto</td>
              <td width="205" class="barratitulo"><img src="<%=BASEURL%>/images/titulo.gif" width="32" height="20"></td>
            </tr>
            <tr class="fila">
              <td width="171" align="left" valign="middle" >Codigo</td>
              <td width="197" valign="middle"><input name="c_codigo" type="text" class="textbox" id="c_codigo" onKeyPress="soloAlfa(event)" size="15" maxlength="6"></td>
            </tr>
            <tr class="fila">
              <td width="171" align="left" valign="middle" >Descripci&oacute;n</td>
              <td width="197" valign="middle"><input name="c_descripcion" type="text" class="textbox" id="c_descripcion" size="15" maxlength="45"></td>
            </tr>
        </table></td>
      </tr>
    </table>
	<p>
<div align="center"><img src="<%=BASEURL%>/images/botones/buscar.gif" style="cursor:hand" title="Buscar un tipo de ubicacion" name="imgaceptar"  onClick="form2.submit();" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);">&nbsp;
<img src="<%=BASEURL%>/images/botones/detalles.gif" style="cursor:hand" title="Buscar todos los tipos de contacto" name="buscar"  onClick="window.location='<%=CONTROLLER%>?estado=Tipo_contacto&accion=Search&listar=True&sw=False'" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);">&nbsp;
<img src="<%=BASEURL%>/images/botones/salir.gif"  name="regresar" style="cursor:hand" title="Salir al Menu Principal" onMouseOver="botonOver(this);" onClick="parent.close();" onMouseOut="botonOut(this);" ></div>
</p>
</form>
</body>
</html>
