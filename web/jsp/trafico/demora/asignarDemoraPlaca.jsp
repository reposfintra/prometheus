<%@page contentType="text/html"%>
<%@ page session="true"%>
<%@ page errorPage="../error/ErrorPage.jsp"%>
<%@page import="com.tsp.operation.model.beans.*, java.util.*"%>
<%@page import="com.tsp.operation.model.*"%>
<%@include file="/WEB-INF/InitModel.jsp"%>
<%@ taglib uri="http://jakarta.apache.org/taglibs/input-0.90" prefix="input" %>
<html>
<head>
<title>Asignaci&oacute;n de Demoras</title>
<script src="<%=BASEURL%>/js/validar.js"></script>
<link href="<%= BASEURL %>/css/estilostsp.css" rel="stylesheet" type="text/css">
<script SRC='<%=BASEURL%>/js/boton.js'></script>
<link href="../../../css/estilostsp.css" rel="stylesheet" type="text/css">
</head>
<body onLoad="redimensionar();" onResize="redimensionar();">
<div id="capaSuperior" style="position:absolute; width:100%; height:100px; z-index:0; left: 0px; top: 0px;">
	<jsp:include page="/WEB-INF/toptsp.jsp?encabezado=Asignaci�n de Demora a Placa de Veh�culo"/>
</div>
<div id="capaCentral" style="position:absolute; width:100%; height:85%; z-index:0; left: 0px; top: 100px; overflow: scroll;">  
<form   name="forma" id="forma" method="post" action="<%=CONTROLLER%>?estado=Ultima&accion=Planilla&cmd=show"  onSubmit="return validarTCamposLlenos();">
  <table width="41%"  border="2" align="center">
    <tr>
      <td><table width="100%">
        <tr>
          <td width="373" class="subtitulo1">&nbsp;Informaci&oacute;n del Veh&iacute;culo </td>
          <td width="427" class="barratitulo"><img src="<%=BASEURL%>/images/titulo.gif"></td>
        </tr>
      </table>
        <table width="100%" align="center" class="tablaInferior">
          <tr class="fila">
            <td width="28%">N&uacute;mero Placa: </td>
            <td width="72%"><input name="plaveh" type="text" class="textbox"> <img src="<%= BASEURL%>/images/botones/iconos/obligatorio.gif" width="10" height="10"></td>
          </tr>
        </table>        </td>
    </tr>
  </table>
  <div align="center"><br>
    <img src="<%= BASEURL%>/images/botones/aceptar.gif"  name="imgaceptar" onClick="if ( validarTCamposLlenos() ) forma.submit();" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" style="cursor:hand ">&nbsp; <img src="<%= BASEURL%>/images/botones/cancelar.gif"  name="imgcancelar"  onMouseOver="botonOver(this);" onClick="forma.reset();" onMouseOut="botonOut(this);"  style="cursor:hand ">&nbsp; <img src="<%= BASEURL%>/images/botones/salir.gif"  name="imgsalir"  onMouseOver="botonOver(this);" onClick="parent.close();" onMouseOut="botonOut(this);"  style="cursor:hand "></div>
</form>
<p>
  <%if(request.getParameter("msg")!=null){%>
</p>
<table border="2" align="center">
  <tr>
    <td><table width="410" height="73" border="1" align="center"  bordercolor="#F7F5F4" bgcolor="#FFFFFF">
        <tr>
          <td width="282" align="center" class="mensajes"><%=request.getParameter("msg").toString() %></td>
          <td width="28" background="<%=BASEURL%>/images/cuadronaranja.JPG">&nbsp;</td>
          <td width="78">&nbsp;</td>
        </tr>
    </table></td>
  </tr>
</table>
	<%}%>
</div>	
</body>
</html>