<%@page contentType="text/html"%>
<%@ page session="true"%>
<%@ page errorPage="../error/ErrorPage.jsp"%>
<%@page import="com.tsp.operation.model.beans.*, java.util.*"%>
<%@page import="com.tsp.operation.model.*"%>
<%@include file="/WEB-INF/InitModel.jsp"%>
<%@ taglib uri="http://jakarta.apache.org/taglibs/input-0.90" prefix="input" %>
<html>
<head>
<title>Menu</title>
<script src="<%=BASEURL%>/js/validar.js"></script>
<link href="<%= BASEURL %>/css/estilostsp.css" rel="stylesheet" type="text/css">
<script SRC='<%=BASEURL%>/js/boton.js'></script>
<link href="../../../css/estilostsp.css" rel="stylesheet" type="text/css">
</head>
<body onLoad="redimensionar();" onResize="redimensionar();">
<div id="capaSuperior" style="position:absolute; width:100%; height:100px; z-index:0; left: 0px; top: 0px;">
	<jsp:include page="/WEB-INF/toptsp.jsp?encabezado=Asignación de Demora a Caravana"/>
</div>
<div id="capaCentral" style="position:absolute; width:100%; height:85%; z-index:0; left: 0px; top: 100px; overflow: scroll;">  
    <%
        String numcaravana = "";
        numcaravana = request.getParameter("numcaravana");
        if(numcaravana==null) numcaravana = "";        
    %>
<form  name="forma" id="forma" method="post" action="<%=CONTROLLER%>?estado=Agregar&accion=DemoraCaravana&cmd=show"  onSubmit="return validarTCamposLlenos();">
  <table width="100%"  border="2">
    <tr>
      <td>      <table width="100%" align="center" class="tablaInferior">
        <tr>
          <td colspan="2" class="subtitulo1"><div align="center"><span class="subtitulos">
            <div align="left">Detalles de la Caravana <%=numcaravana %></div>
          </span></div></td>
          <td colspan="2" class="barratitulo"><span class="barratitulo"><img src="<%=BASEURL%>/images/titulo.gif"></span></td>
          </tr>
        <tr>
          <td colspan="4" class="filaresaltada"><div align="left">Detalles de la Caravana <%=numcaravana %></div></td>
        </tr>
        <%
        Vector numplas = model.demorasSvc.planillasCaravana(Integer.valueOf(numcaravana));
    %>
        <tr>
          <td colspan="4" class="fila"><table width="100%"  border="1" cellpadding="4" cellspacing="1" bordercolor="#999999" class="tablaInferior">
              <tr class="tblTitulo">
                <td width="14%"><div align="center">No. Planilla </div></td>
                <td width="20%"><div align="center">Fecha</div></td>
                <td width="27%"><div align="center">Ruta</div></td>
                <td width="39%"><div align="center">Conductor</div></td>
              </tr>
              <%
        for(int i=0; i<numplas.size();i++){
            String numpla = (String) numplas.elementAt(i);
            InfoPlanilla info = model.planillaService.obtenerInformacionPlanilla(numpla);
    %>
              <tr class="fila">
                <td class="bordereporte"><%=info.getPlanilla()%></td>
                <td class="bordereporte"><%=info.getFecha()%></td>
                <td class="bordereporte"><%=info.getOrigen() + "-" + info.getDestino()%></td>
                <td class="bordereporte"><%=info.getCedulaConductor() + " " + info.getNombreConductor()%></td>
              </tr>
              <%
       }
   %>
          </table></td>
        </tr>
        <tr>
          <td colspan="2" class="subtitulo1"><div align="left">Detalles de la Demora</div></td>
          <td colspan="2" class="barratitulo"><span class="barratitulo"><img src="<%=BASEURL%>/images/titulo.gif"></span></td>
          </tr>
        <% Vector dems = (Vector) session.getAttribute("Demoras"); 
            Demora dem = (Demora) dems.elementAt(0);    
    %>
        <tr bordercolor="#b7d0dc" class="fila">
          <td width="70">Descripci&oacute;n:</td>
          <td width="207"><%=model.codigo_demoraService.obtenerCDemora(dem.getCodigo_demora()) %> </td>
          <td width="99">Causa: </td>
          <td width="258"><%=dem.getCodigo_jerarquia() %></td>
        </tr>
        <tr class="fila">
          <td>Fecha:</td>
          <td><%=dem.getFecha_demora() %></td>
          <td>Tiempo restante: </td>
          <td><%=dem.tiempoRestante()%> horas</td>
        </tr>
        <tr class="fila">
          <td>Duraci&oacute;n:</td>
          <td><%=dem.getDuracion() %></td>
          <td>Sitio:</td>
          <td><%=model.tramoService.obtenerTramo(dem.getSitio_demora()) %></td>
        </tr>
        <tr class="fila">
          <td valign="top">Observaci&oacute;n:</td>
          <td colspan="3"><%=dem.getObservacion()%></td>
        </tr>
        <tr class="fila">
          <td>Finalizada:</td>
          <td><%=dem.getFinalizada() %></td>
          <% if (dem.getFinalizada().toLowerCase().matches("no")) {
        %>
          <td><a href="<%=CONTROLLER%>?estado=Finalizar&accion=DemoraCaravana&cmd=show&numcaravana=<%=numcaravana%>">Finalizar</a></td>
          <td>&nbsp;</td>
          <%}
        else{ %>
          <td colspan=2>&nbsp;</td>
          <%}%>
        </tr>
        <tr class="fila">
          <td colspan="4"><div align="center"> </div></td>
        </tr>
      </table>        </td>
    </tr>
  </table>
  <br>  
  <%if(request.getParameter("msg")!=null){%>
  <table border="2" align="center">
    <tr>
      <td><table width="410" height="73" border="1" align="center"  bordercolor="#F7F5F4" bgcolor="#FFFFFF">
          <tr>
            <td width="282" align="center" class="mensajes"><%=request.getParameter("msg").toString() %></td>
            <td width="28" background="<%=BASEURL%>/images/cuadronaranja.JPG">&nbsp;</td>
            <td width="78">&nbsp;</td>
          </tr>
      </table></td>
    </tr>
  </table>
	
	<%}%>
</form>
</div>
</body>
</html>