<%@page contentType="text/html"%>
<%@page session="true"%>
<%@page errorPage="../error/ErrorPage.jsp"%>
<%@page import="com.tsp.operation.model.*"%>
<%@page import="com.tsp.util.*"%>
<%@page import="com.tsp.operation.model.beans.*, java.util.*"%>
<%@include file="/WEB-INF/InitModel.jsp"%>
<%@ taglib uri="http://jakarta.apache.org/taglibs/input-0.90" prefix="input" %>
<%
	Hashtable ht = model.demorasSvc.getPlanillasSelec();
%>
<html>
<head>
<title>Ingresar Movimiento - Movimiento De Tr�fico - TSP</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link href="<%= BASEURL %>/css/estilostsp.css" rel="stylesheet" type="text/css">
<link href="../../../css/estilostsp.css" rel="stylesheet" type="text/css">
<script src="<%=BASEURL%>/js/validar.js"></script>
<script src="<%=BASEURL%>/js/general.js"></script>
<script type='text/javascript' src="<%= BASEURL %>/js/boton.js"></script>
<script type='text/javascript' src="<%= BASEURL %>/js/RepMovTraficoSelect.js"></script>
</head>
<body onLoad="<%if(request.getParameter("cerrar")!=null){%>window.close();<%}%>">  

  <form method="post" name="forma" action="<%=CONTROLLER%>?estado=Agregar&accion=DemoraPlanillas" id='forma'>
    <br>
    <table align=center width='450'>
      <tr class="informacion">
        <td align="center" colspan="2">*Seleccione las planillas y escoja la fecha del reporte para cada una de ellas.</td>
      </tr>
    </table>
    <br>
    <table width="85%" border="2" align="center">
    <tr>
        <td>
            <table width="100%" align="center"  >
                <tr>            
                    <td width="54%" class="subtitulo1"><div align="center">Ingresar Movimiento a otras planillas </div></td>
                    <td width="46%" class="barratitulo"><img src="<%=BASEURL%>/images/titulo.gif" width="32" height="20"></td>                    
                </tr>
        </table>        
            <table width="100%" border="1" bordercolor="#999999">
			
              <tr class="tblTitulo">
                <td width="10%">Seleccionar</td>
                <td width="19%">Planilla</td>
				<td width="19%">Placa</td>
              </tr>
			<%Vector caravana = model.rmtService.getPlanillas()==null ? new Vector() : model.rmtService.getPlanillas();
			for(int i = 0; i<caravana.size();i++){
			String numpla = (String) caravana.elementAt(i);
			String vecNumpla[] = numpla.split (",");
			String causas = "causas"+vecNumpla[0];
			%>
              <tr class="<%=i%2==0?"filagris":"filaazul"%>">
                <td class="bordereporte"><!-- input type="checkbox"  name="planilla<% //=vecNumpla[0]%>" id="<% //=vecNumpla[0]%>" value="checkbox" <% //=request.getParameter(vecNumpla[0])!=null?"checked":""%> -->
				<input type="checkbox"  name="planilla" id="<%=vecNumpla[0]%>" value="<%=vecNumpla[0]%>" <%=ht.get(vecNumpla[0])!=null?"checked":""%>></td>
                <td class="bordereporte"><%=vecNumpla[0]%></td>
				<td class="bordereporte"><%=vecNumpla[1]%></td>
              </tr>
			  <%}%>
          </table></td>
    </tr>
  </table>
  <br>
  <table align=center width='450'>
    <tr>
        <td align="center" colspan="2">
            <img title='Agregar reporte a estas planillas' name="mod" id="mod" src="<%= BASEURL %>/images/botones/aceptar.gif" onmouseover="botonOver(this);" onmouseout="botonOut(this);" onclick="/*ValidarFormularioVariasCopia();*/forma.submit();"></img>&nbsp;            
            <img title='Salir' src="<%= BASEURL %>/images/botones/salir.gif" onmouseover="botonOver(this);" onmouseout="botonOut(this);" onClick="parent.close()"></img>
		</td>
    </tr>
  </table>
  </form>
</body>
</html>
