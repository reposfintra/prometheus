<%@page contentType="text/html"%>
<%@ page session="true"%>
<%@ page errorPage="../error/ErrorPage.jsp"%>
<%@page import="com.tsp.operation.model.beans.*, java.util.*"%>
<%@page import="com.tsp.operation.model.*"%>
<%@include file="/WEB-INF/InitModel.jsp"%>
<%@ taglib uri="http://jakarta.apache.org/taglibs/input-0.90" prefix="input" %>
<html>
<head>
<title>Asignaci&oacute;n de Demora a Tramo</title>
<script src="<%=BASEURL%>/js/validar.js"></script>
<link href="<%= BASEURL %>/css/estilostsp.css" rel="stylesheet" type="text/css">
<script SRC='<%=BASEURL%>/js/boton.js'></script>
<link href="../../../css/estilostsp.css" rel="stylesheet" type="text/css">
</head>
<body onLoad="redimensionar();" onResize="redimensionar();">
<div id="capaSuperior" style="position:absolute; width:100%; height:100px; z-index:0; left: 0px; top: 0px;">
<jsp:include page="/WEB-INF/toptsp.jsp?encabezado=Asignación de Demora a Tramo"/>
</div>

<div id="capaCentral" style="position:absolute; width:100%; height:85%; z-index:0; left: 0px; top: 100px; overflow: scroll;"> 
    <%
        //Se recibe el tramo
    %>
<form  name="forma" id="forma" method="post" action="<%=CONTROLLER%>?estado=Agregar&accion=DemoraTramo&cmd=show"  onSubmit="return validarTCamposLlenos();">
  <table width="703"  border="2" align="center">
    <tr>
      <td><table width="100%" align="center">
        <tr>
          <td colspan="3" class="subtitulo1"><div align="left">Detalles del Tramo </div></td>
          <td colspan="3" class="barratitulo"><span class="subtitulos"><span class="titulo"><img src="<%=BASEURL%>/images/titulo.gif"></span></span></td>
          </tr>
        <%
        //Vector numplas = model.demorasSvc.planillasCaravana();
    %>
        <tr>
          <td colspan="6" class="fila">Origen - Destino : </td>
        </tr>
        <tr>
          <td colspan="6" class="fila"><table width="100%"  border="1" cellpadding="4" cellspacing="1" bordercolor="#999999">
              <tr class="tblTitulo">
                <td width="14%"><div align="center">No. Planilla </div></td>
                <td width="20%"><div align="center">Fecha</div></td>
                <td width="27%"><div align="center">Ruta</div></td>
                <td width="39%"><div align="center">Conductor</div></td>
              </tr>
              <tr class="fila">
                <td class="bordereporte">&nbsp;</td>
                <td class="bordereporte">&nbsp;</td>
                <td class="bordereporte">&nbsp;</td>
                <td class="bordereporte">&nbsp;</td>
              </tr>
              <tr class="fila">
                <td class="bordereporte">&nbsp;</td>
                <td class="bordereporte">&nbsp;</td>
                <td class="bordereporte">&nbsp;</td>
                <td class="bordereporte">&nbsp;</td>
              </tr>
              <tr class="fila">
                <td class="bordereporte">&nbsp;</td>
                <td class="bordereporte">&nbsp;</td>
                <td class="bordereporte">&nbsp;</td>
                <td class="bordereporte">&nbsp;</td>
              </tr>
              <tr class="fila">
                <td class="bordereporte">&nbsp;</td>
                <td class="bordereporte">&nbsp;</td>
                <td class="bordereporte">&nbsp;</td>
                <td class="bordereporte">&nbsp;</td>
              </tr>
          </table></td>
        </tr>
        <tr>
          <td colspan="3" class="subtitulo1"><div align="left">Detalles de la Demora</div></td>
          <td colspan="3" class="barratitulo"><span class="subtitulos"><span class="titulo"><img src="<%=BASEURL%>/images/titulo.gif"></span></span></td>
          </tr>
        <tr bordercolor="#b7d0dc" class="fila">
          <td width="70">Descripci&oacute;n:</td>
          <% TreeMap cdemoras = model.codigo_demoraService.searchCDemoras();%>
          <td width="124"><input:select name="cdemora" attributesText="class=textbox" options="<%=cdemoras %>"/> </td>
          <td width="42">Causa:</td>
          <% TreeMap cjerarquias = model.demorasSvc.codigosJerarquia(); %>
          <td width="127"><input:select name="cjerarquia" attributesText="class=textbox" options="<%=cjerarquias %>"/> </td>
          <td width="41">Fecha:</td>
          <td width="175"><input name="fecha" type="text" class="textbox" id="fecha2" readonly  value="<%=com.tsp.util.Util.getFechaActual_String(6)%>">
              <a href="javascript:void(0)" onclick="if(self.gfPop)gfPop.fPopCalendar(document.forma.fecha);return false;" HIDEFOCUS> <img src="<%=BASEURL%>\js\Calendario\cal.gif" width="16" height="16"
               border="0" alt="De click aqu&iacute; para escoger la fecha"></a></td>
        </tr>
        <tr class="fila">
          <td>Duraci&oacute;n:</td>
          <td><input name="duracion" type="text" class="textbox" id="duracion" onKeyPress="soloDigitos(event,'decNo')"></td>
          <td>Sitio:</td>
          <% TreeMap tramos = model.demorasSvc.tramosPlanilla(); %>
          <td colspan="3"><input:select name="sitio" attributesText="class=textbox" options="<%=tramos %>"/>
              <input name="tramo" type="hidden" id="tramo" value="default"></td>
        </tr>
        <tr class="fila">
          <td valign="top">Observaci&oacute;n:</td>
          <td colspan="5"><textarea name="observacion" cols="105" class="textbox" id="observacion"></textarea></td>
        </tr>
        <tr class="fila">
          <td colspan="6">&nbsp;</td>
        </tr>
        <tr class="fila">
          <td colspan="6"><div align="center">
              <input name="Submit2" type="submit" class="boton" value="Enviar">
          </div></td>
        </tr>
      </table></td>
    </tr>
  </table>
  <p align="center"><br>
    <img src="<%= BASEURL%>/images/botones/aceptar.gif"  name="imgaceptar" onClick=" forma.submit();" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);">&nbsp; <img src="<%= BASEURL%>/images/botones/cancelar.gif"  name="imgcancelar"  onMouseOver="botonOver(this);" onClick="forma.reset();" onMouseOut="botonOut(this);" >&nbsp; <img src="<%= BASEURL%>/images/botones/salir.gif"  name="imgsalir"  onMouseOver="botonOver(this);" onClick="parent.close();" onMouseOut="botonOut(this);" ></p>
</form>
<iframe width=188 height=166 name="gToday:normal:agenda.js:gfPop:plugins_24.js" id="gToday:normal:agenda.js:gfPop:plugins_24.js" src="js/Calendario/ipopeng.htm" scrolling="no" frameborder="0" style="visibility:visible; z-index:999; position:absolute; left:-500px; top:0px;"> </iframe>
</div>
</body>
</html>