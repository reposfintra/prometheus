<%@page contentType="text/html"%>
<%@ page session="true"%>
<%@ page errorPage="../error/ErrorPage.jsp"%>
<%@page import="com.tsp.operation.model.beans.*, java.util.*"%>
<%@page import="com.tsp.operation.model.*"%>
<%@include file="/WEB-INF/InitModel.jsp"%>
<%@ taglib uri="http://jakarta.apache.org/taglibs/input-0.90" prefix="input" %>
<html>
<head>
<title>Asignaci&oacute;n de Demora a Planilla</title>
<script src="<%=BASEURL%>/js/validar.js"></script>
<link href="<%= BASEURL %>/css/estilostsp.css" rel="stylesheet" type="text/css">
<script SRC='<%=BASEURL%>/js/boton.js'></script>
<link href="../../../css/estilostsp.css" rel="stylesheet" type="text/css">
</head>
<body onLoad="redimensionar();" onResize="redimensionar();">
<div id="capaSuperior" style="position:absolute; width:100%; height:100px; z-index:0; left: 0px; top: 0px;">
<jsp:include page="/WEB-INF/toptsp.jsp?encabezado=Listar de Demoras a Planilla"/>
</div>
<div id="capaCentral" style="position:absolute; width:100%; height:85%; z-index:0; left: 0px; top: 100px; overflow: scroll;">
<%
    String numpla = request.getParameter("numpla");
    InfoPlanilla info = model.planillaService.obtenerInformacionPlanilla(numpla);
    String conductor = info.getCedulaConductor() + " " + info.getNombreConductor();
    String ruta = info.getOrigen() + " - " + info.getDestino();
    String cliente = info.getCliente();
    String fecha = info.getFecha();
    String placa = info.getVehiculo();
%>

  <table width="715"  border="2" align="center">
    <tr>
      <td><table width="100%" align="center" class="tablaInferior">
        <tr>
          <td colspan="2" class="subtitulo1"><div align="left">Detalles de la Planilla</div></td>
          <td colspan="2" class="barratitulo"><img src="<%=BASEURL%>/images/titulo.gif"></td>
        </tr>
        <tr>
          <td colspan="4" class="fila"><table width="100%"  border="0">
              <tr class="fila">
                <td width="14%">No.:</td>
                <td width="46%"><%=numpla %></td>
                <td width="9%">Fecha:</td>
                <td width="31%"><%=fecha %></td>
              </tr>
              <tr class="fila">
                <td>Conductor:</td>
                <td><%=conductor %></td>
                <td>Placa:</td>
                <td><%=placa %></td>
              </tr>
              <tr class="fila">
                <td>Ruta:</td>
                <td colspan=3><%=ruta %></td>
              </tr>
              <tr class="fila">
                <td>Cliente:</td>
                <td colspan=3><%=cliente %></td>
              </tr>
          </table></td>
        </tr>
        <tr>
          <td colspan="2" class="subtitulo1"><div align="left">Detalles de la Demora</div></td>
          <td colspan="2" class="barratitulo"><img src="<%=BASEURL%>/images/titulo.gif"></td>
        </tr>
        <% Demora dem = (Demora) request.getAttribute("Demora"); %>
        <tr bordercolor="#b7d0dc" class="fila">
          <td width="70">Descripci&oacute;n:</td>
          <td width="207"><%=model.codigo_demoraService.obtenerCDemora(dem.getCodigo_demora()) %> </td>
          <td width="99">Causa: </td>
          <td width="258"><%=dem.getCodigo_jerarquia()%></td>
        </tr>
        <tr class="fila">
          <td>Fecha:</td>
          <td><%=dem.getFecha_demora() %></td>
          <td>Tiempo restante: </td>
          <td><%=dem.tiempoRestante()%> horas</td>
        </tr>
        <tr class="fila">
          <td>Duraci&oacute;n:</td>
          <td><%=dem.getDuracion() %></td>
          <td>Sitio:</td>
          <td><%=model.tramoService.obtenerTramo(dem.getSitio_demora()) %></td>
        </tr>
        <tr class="fila">
          <td valign="top">Observaci&oacute;n:</td>
          <td colspan="3"><%=dem.getObservacion()%></td>
        </tr>
        <tr class="fila">
          <td>Finalizada:</td>
          <td><%=dem.getFinalizada() %></td>
          <% if (dem.getFinalizada().toLowerCase().matches("no")) {
        %>
          <td><a href="<%=CONTROLLER%>?estado=Finalizar&accion=DemoraPlanilla&cmd=show&numpla=<%=numpla%>" class="Simulacion_Hiper">Finalizar</a></td>
          <td>&nbsp;</td>
          <%}
        else{ %>
          <td colspan=2>&nbsp;</td>
          <%}%>
        </tr>
        <tr class="fila">
          <td colspan="4"><div align="center"> </div></td>
        </tr>
      </table></td>
    </tr>
  </table>
  <%if(request.getParameter("msg")!=null){%>
  <table border="2" align="center">
    <tr>
      <td><table width="410" height="73" border="1" align="center"  bordercolor="#F7F5F4" bgcolor="#FFFFFF">
          <tr>
            <td width="282" align="center" class="mensajes"><%=request.getParameter("msg").toString() %></td>
            <td width="28" background="<%=BASEURL%>/images/cuadronaranja.JPG">&nbsp;</td>
            <td width="78">&nbsp;</td>
          </tr>
      </table></td>
    </tr>
  </table>	
<%}%>
</div>
</body>
</html>