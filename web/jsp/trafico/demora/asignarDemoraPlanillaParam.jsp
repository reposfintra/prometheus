<!--
- Autor : Ing. Andr�s Maturana De La Cruz
- Date : 
- Copyrigth Notice : Fintravalores S.A. S.A
-->
<%--
-@(#)
--Descripcion : Pagina JSP, que sirve registrar una demora a una planilla
--%>
<%@page contentType="text/html"%>
<%@ page session="true"%>
<%@ page errorPage="/error/ErrorPage.jsp"%>
<%@page import="com.tsp.operation.model.beans.*, java.util.*, java.text.*"%>
<%@page import="com.tsp.operation.model.*"%>
<%@include file="/WEB-INF/InitModel.jsp"%>
<%@ taglib uri="http://jakarta.apache.org/taglibs/input-0.90" prefix="input" %>
<%String path = application.getRealPath(model.menuService.getRealUrl(request.getRequestURI()));
  String datos[] = model.menuService.getContenidoMenu(BASEURL,request.getRequestURI(),path);
%>
<html>
<head>
<title>Asignaci&oacute;n de Retraso a Planilla</title>
<link href="<%= BASEURL %>/css/estilotsp.css" rel='stylesheet'>
<script src="<%=BASEURL%>/js/validar.js"></script>
<script src="<%=BASEURL%>/js/demoras.js"></script>
<link href="<%= BASEURL %>/css/estilostsp.css" rel="stylesheet" type="text/css">
<script SRC='<%=BASEURL%>/js/boton.js'></script>
<script src="<%=BASEURL%>/js/general.js"></script>
<link href="../../../css/estilostsp.css" rel="stylesheet" type="text/css">
</head>
<body onLoad="redimensionar();" onResize="redimensionar();">
<div id="capaSuperior" style="position:absolute; width:100%; height:100px; z-index:0; left: 0px; top: 0px;">
	<jsp:include page="/WEB-INF/toptsp.jsp?encabezado=Asignaci�n de Retraso a Planilla"/>
</div>

<div id="capaCentral" style="position:absolute; width:100%; height:85%; z-index:0; left: 0px; top: 100px; overflow: scroll;"> <%
    String numpla = request.getParameter("numpla");
	Ingreso_Trafico trafico = model.traficoService.getIngreso();
	TreeMap cdemoras = model.codigo_demoraService.getCdemoras();
	TreeMap cjerarquias = model.tblgensvc.getCausasDemora();
	String nom_ultubicacion = trafico.getNompto_control_ultreporte().length() == 0 ? "NINGUNO" : trafico.getNompto_control_ultreporte();
	String cod_ultubicacion = trafico.getNompto_control_ultreporte().length() == 0 ? "NINGUNO" : trafico.getPto_control_ultreporte();
	String nom_proxubicacion = trafico.getNompto_control_proxreporte().length() == 0 ? "NINGUNO" : trafico.getNompto_control_proxreporte();
	String cod_proxubicacion = trafico.getNompto_control_proxreporte().length() == 0 ? "NINGUNO" : trafico.getPto_control_proxreporte();
	String nomtramoi = trafico.getNompto_control_ultreporte().length() == 0 ? trafico.getNomorigen() : trafico.getNompto_control_ultreporte();
	String nomtramof = trafico.getNompto_control_proxreporte().length() == 0 ? trafico.getNomdestino() : trafico.getNompto_control_proxreporte();
%>
<form name="forma" id="forma" method="post" action="<%=CONTROLLER%>?estado=Agregar&accion=DemoraPlanilla&cmd=show" onSubmit="">
  <table width="850"  border="2" align="center">
    <tr>
      <td><table width="100%">
        <tr>
          <td width="373" class="subtitulo1">&nbsp;Informaci&oacute;n De Planilla </td>
          <td width="427" class="barratitulo"><img align="left" src="<%=BASEURL%>/images/titulo.gif"><%=datos[0]%></td>
        </tr>
      </table><table width="100%" align="center" class="tablaInferior">
        <tr>
          <td colspan="4"><table width="100%" align="center">
            <tr class="fila">
              <td align="center" rowspan="2"><strong>Planilla</strong></td>
              <td colspan="2" align="center"><strong>Conductor</strong></td>
              <td align="center" rowspan="2"><strong>Placa Veh&iacute;culo </strong></td>
              <td colspan="2" align="center"><strong>Origen</strong></td>
              <td colspan="2" align="center"><strong>Destino</strong></td>
            </tr>
            <tr class="fila">
              <td align="center">C&eacute;dula</td>
              <td align="center">Nombre</td>
              <td align="center">C&oacute;digo</td>
              <td align="center">Descripci&oacute;n</td>
              <td align="center">Codigo</td>
              <td align="center">Descripci&oacute;n</td>
            </tr>
            <tr class="letra">
              <td align="center"><%= trafico.getPlanilla() %></td>
              <td align="center"><%= trafico.getCedcon() %></td>
              <td align="center"><%= trafico.getNomcond() %></td>
              <td align="center"><%= trafico.getPlaca() %></td>
              <td align="center"><%= trafico.getOrigen() %></td>
              <td align="center"><%= trafico.getNomorigen() %></td>
              <td align="center"><%= trafico.getDestino()%></td>
              <td align="center"><%= trafico.getNomdestino() %></td>
            </tr>
            <tr class="letra">
              <td height="30" colspan=4 align='left'><strong>Ultimo reporte de tr&aacute;fico:</strong>
                <% if( trafico.getFecha_ult_reporte()!=null && trafico.getFecha_ult_reporte().compareTo("0099-01-01 00:00:00")==0 ){
				out.print("NINGUNA");
			} else {		
				out.print(trafico.getFecha_ult_reporte().substring(0,16));
			}%> </td>
              <td height="30" colspan=4 align='left'><strong>Proximo Reporte Planeado:</strong> 
			  <% if( trafico.getFecha_prox_reporte()!=null && trafico.getFecha_prox_reporte().compareTo("0099-01-01 00:00:00")==0 ){
				out.print("NINGUNA");
			} else {		
				out.print(trafico.getFecha_prox_reporte().substring(0,16));
			}%></td>
            </tr>
            <tr class="letra">
              <td height="30" colspan=4 align='left'><strong>Ubicaci&oacute;n Ultimo Reporte:</strong> 
			  <%= nom_ultubicacion %></td>
              <td height="30" colspan=4 align='left'><strong>Ubicaci&oacute;n Proximo Reporte:</strong> <%= nom_proxubicacion %></td>
            </tr>
          </table></td>
        </tr>
        <tr>
          <td colspan="3" class="subtitulo1"><div align="left">Detalles del Retraso </div></td>
          <td class="barratitulo"><span class="subtitulos"><span class="titulo"><img src="<%=BASEURL%>/images/titulo.gif"></span></span></td>
        </tr>
        <tr bordercolor="#b7d0dc" class="fila">
          <td width="97" bordercolor="#F7F5F4">Descripci&oacute;n</td>
          <td width="267" bordercolor="#F7F5F4"><input:select default="<%= (String) request.getAttribute("desc") %>" name="cdemora" attributesText="class=textbox" options="<%=cdemoras %>"/> <img src="<%= BASEURL%>/images/botones/iconos/obligatorio.gif" width="10" height="10"> </td>
          <td width="40" bordercolor="#F7F5F4">Causa</td>
          <td width="414" bordercolor="#ECE9D8"><input:select default="<%= (String) request.getAttribute("causa") %>" name="cjerarquia" attributesText="class=textbox" options="<%=cjerarquias %>"/> <img src="<%= BASEURL%>/images/botones/iconos/obligatorio.gif" width="10" height="10"></td>
          </tr>        
        <tr>
          <td class="fila">Duraci&oacute;n</td>
          <td class="letra"><input name="duracion" type="text" class="textbox" id="duracion"  onFocus="" onKeyPress="soloDigitos(event,'decNo');" size="10" maxlength="4"  value="">
&nbsp;Minutos <img src="<%= BASEURL%>/images/botones/iconos/obligatorio.gif" width="10" height="10"></td>
          <td class="fila">Sitio</td>
          <td class="letra"><input name="numpla" type="hidden" id="numpla" value="<%= numpla%>">
            <input name="sitio" type="hidden" id="sitio" value="<%= cod_ultubicacion+cod_proxubicacion %>">
            <input name="sitio_des" type="hidden" id="sitio_des" value="<%= nomtramoi + " - " + nomtramof%>">            <input name="fecha1" type="hidden" id="fecha1" value="<%= trafico.getNompto_control_ultreporte().length() == 0 ? trafico.getFecha_salida() : trafico.getFecha_ult_reporte()%>">            <%= nomtramoi + " - " + nomtramof %></td>
        </tr>
        <tr class="fila">
          <td valign="top">Observaci&oacute;n</td>
          <td colspan="3" valign="top"><textarea name="observacion" cols="105" rows="8" class="textbox" id="observacion"><%= (request.getAttribute("observ")==null) ? "" :(String) request.getAttribute("observ") %></textarea></td>
        </tr>
        <tr class="fila">
          <td colspan="4" valign="top"><div align="right"><a href="javascript: void(0);" onclick="window.open('<%=BASEURL%>/jsp/trafico/demora/RepVariasPlanillas.jsp?fechar='+ getDate (new Date(), 'yyyy-mm-dd hh:mi'),'','status=no,scrollbars=yes,width=720,height=400,resizable=yes')">Copiar Reporte a otras planillas</a></div></td>
          </tr>
      </table></td>
    </tr>
  </table>
  <p align="center"><br>
    <img src="<%= BASEURL%>/images/botones/aceptar.gif"  name="imgaceptar" onClick="if ( validarFormulario() ) forma.submit();" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);"  style="cursor:hand ">&nbsp; <img src="<%= BASEURL%>/images/botones/cancelar.gif"  name="imgcancelar"  onMouseOver="botonOver(this);" onClick="forma.reset();" onMouseOut="botonOut(this);"  style="cursor:hand ">&nbsp; <img src="<%= BASEURL%>/images/botones/salir.gif"  name="imgsalir"  onMouseOver="botonOver(this);" onClick="parent.close();" onMouseOut="botonOut(this);"  style="cursor:hand "></p>
</form>

<%if(request.getParameter("msg")!=null){%>
  <table border="2" align="center">
    <tr>
      <td><table width="410" height="73" border="1" align="center"  bordercolor="#F7F5F4" bgcolor="#FFFFFF">
          <tr>
            <td width="282" align="center" class="mensajes"><%=request.getParameter("msg").toString() %></td>
            <td width="28" background="<%=BASEURL%>/images/cuadronaranja.JPG">&nbsp;</td>
            <td width="78">&nbsp;</td>
          </tr>
      </table></td>
    </tr>
  </table>
<%}%>
</div>
<iframe width=188 height=166 name="gToday:normal:agenda.js:gfPop:plugins_24.js" id="gToday:normal:agenda.js:gfPop:plugins_24.js" src="js/Calendario/ipopeng.htm" scrolling="no" frameborder="0" style="visibility:visible; z-index:999; position:absolute; left:-500px; top:0px;"> </iframe>
<%=datos[1]%>
</body>
</html>
