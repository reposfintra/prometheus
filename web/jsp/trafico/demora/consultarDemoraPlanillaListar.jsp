<%@page contentType="text/html"%>
<%@page session="true"%>
<%@page import="java.util.*"%>
<%@page import="com.tsp.operation.model.*"%>
<%@page import="com.tsp.util.*"%>
<%@page import="com.tsp.operation.model.beans.*, java.util.*"%>
<%@include file="/WEB-INF/InitModel.jsp"%>
<%@taglib uri="http://jsptags.com/tags/navigation/pager" prefix="pg" %>
 
<html>
<head>
<title>Listar de Demoras a Planilla</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link href="<%= BASEURL %>/css/estilostsp.css" rel="stylesheet" type="text/css">
<script SRC='<%=BASEURL%>/js/boton.js'></script>
<link href="../../../css/estilostsp.css" rel="stylesheet" type="text/css"> 
<script type='text/javascript' src="<%= BASEURL %>/js/reporte.js"></script>
</head>

<body onLoad="redimensionar();" onResize="redimensionar();">
<div id="capaSuperior" style="position:absolute; width:100%; height:100px; z-index:0; left: 0px; top: 0px;">
<jsp:include page="/WEB-INF/toptsp.jsp?encabezado=Listar de Demoras a Planilla"/>
</div>
<div id="capaCentral" style="position:absolute; width:100%; height:85%; z-index:0; left: 0px; top: 100px; overflow: scroll;"> 
<p>&nbsp;</p>
<p>
<%  
    String style = "simple";
    String position =  "bottom";
    String index =  "center";
    int maxPageItems = 10;
    int maxIndexPages = 10;
    //String numpla = (String) request.getAttribute("numpla");
    String numpla = (String) session.getAttribute("numplaDemoraListar");
    Vector vec = model.demorasSvc.listarDemorasPlanilla(numpla);
    Demora dem = new Demora();
    if ( vec.size() > 0 ){  
%>
</p>
<table width="90%" border="2" align="center">
    <tr>
      <td>
	  <table width="100%">
              <tr>
                <td width="373" class="subtitulo1">&nbsp;Demoras Planilla <span class="bordereporte"><%= numpla %></span></td>
                <td width="427" class="barratitulo"><img src="<%=BASEURL%>/images/titulo.gif"></td>
              </tr>
        </table>
		  <table width="100%" border="1" bordercolor="#999999" bgcolor="#F7F5F4">
          <tr class="tblTitulo">
          <td width="26%" align="center">Fecha</td>
          <td width="36%"  align="center">Sitio</td>
          <td width="38%"  align="center">Duraci&oacute;n</td>
          </tr>
        <pg:pager
         items="<%= vec.size()%>"
         index="<%= index %>"
         maxPageItems="<%= maxPageItems %>"
        maxIndexPages="<%= maxIndexPages %>"
    isOffset="<%= true %>"
    export="offset,currentPageNumber=pageNumber"
    scope="request">
        <%-- keep track of preference --%>
        <%
      for (int i = offset.intValue(), l = Math.min(i + maxPageItems, vec.size()); i < l; i++)
	  {
          dem = (Demora) vec.elementAt(i);
          String link = CONTROLLER + "?estado=DemoraPlanilla&accion=Obtener&cmd=show&id=" + dem.getId() + "&mensaje="
                  + "&numpla=" + numpla;	
%>
        <pg:item>
        <tr class="<%=(i % 2 == 0 )?"filagris":"filaazul"%>" onMouseOver='cambiarColorMouse(this)'   style="cursor:hand"
        onClick="window.open('<%= link %>','','status=yes,scrollbars=no,width=780,height=400,resizable=yes');" >
          <td width="26%"  class="bordereporte"><%= ( dem.getFecha_demora().length()>18 )? dem.getFecha_demora().substring(0,19) : dem.getFecha_demora() %></td>
          <td width="36%"  class="bordereporte"><%= model.tramoService.obtenerTramo(dem.getSitio_demora()) %></td>
          <td width="38%"  class="bordereporte"><%= dem.getDuracion() + " mins" %></td>
        </tr>
        </pg:item>
        <%}%>
        <tr  class="bordereporte">
          <td td height="20" colspan="3" nowrap align="center">
		   <pg:index>
            <jsp:include page="/WEB-INF/jsp/googlesinimg.jsp" flush="true"/>      
           </pg:index> 
	      </td>
        </tr>
        </pg:pager>
      </table></td>
    </tr>
</table>
<p>
      <%}
 else { %>
</p>
  <table border="2" align="center">
    <tr>
      <td><table width="410" height="73" border="1" align="center"  bordercolor="#F7F5F4" bgcolor="#FFFFFF">
          <tr>
            <td width="282" align="center" class="mensajes">Su b&uacute;squeda no arroj&oacute; resultados!</td>
            <td width="28" background="<%=BASEURL%>/images/cuadronaranja.JPG">&nbsp;</td>
            <td width="78">&nbsp;</td>
          </tr>
      </table></td>
    </tr>
  </table>
  <p>&nbsp; </p>
<%}%>
<br>
<table width="90%" border="0" align="center" cellpadding="0" cellspacing="0">
  <tr align="left">
    <td><img src="<%=BASEURL%>/images/botones/regresar.gif" name="c_regresar" id="c_regresar" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" onClick="parent.history.back();" style="cursor:hand "> 
  </tr>
</table>
</div>
</body>
</html>
