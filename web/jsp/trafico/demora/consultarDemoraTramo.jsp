<%@page contentType="text/html"%>
<%@ page session="true"%>
<%@ page errorPage="../error/ErrorPage.jsp"%>
<%@page import="com.tsp.operation.model.beans.*, java.util.*"%>
<%@page import="com.tsp.operation.model.*"%>
<%@include file="/WEB-INF/InitModel.jsp"%>
<%@ taglib uri="http://jakarta.apache.org/taglibs/input-0.90" prefix="input" %>
<html>
<head>
<title>Menu</title>
<script src="<%=BASEURL%>/js/validar.js"></script>
<script>    
    function actualizarInfo() {
        forma.action = "<%=BASEURL %>/jsp/trafico/demora/consultarDemoraTramo.jsp";
        forma.origenselec.value = forma.origen.options(forma.origen.selectedIndex).value;
        forma.submit();
    }
</script>    
<link href="<%= BASEURL %>/css/estilostsp.css" rel="stylesheet" type="text/css">
<script SRC='<%=BASEURL%>/js/boton.js'></script>
<link href="../../../css/estilostsp.css" rel="stylesheet" type="text/css">
</head>
<body onLoad="redimensionar();" onResize="redimensionar();">
<div id="capaSuperior" style="position:absolute; width:100%; height:100px; z-index:0; left: 0px; top: 0px;">
<jsp:include page="/WEB-INF/toptsp.jsp?encabezado=Listar Demoras a Tramo"/>
</div>
<div id="capaCentral" style="position:absolute; width:100%; height:85%; z-index:0; left: 0px; top: 100px; overflow: scroll;"> 
<form name="forma" id="forma" method="post" action="<%=CONTROLLER%>?estado=Consultar&accion=DemoraTramo&cmd=show"  onSubmit="return validarTCamposLlenos();">
  <table width="56%"  border="2" align="center">
    <tr>
      <td><table width="100%">
        <tr>
          <td width="373" class="subtitulo1">&nbsp;Informaci&oacute;n del Tramo</td>
          <td width="427" class="barratitulo"><img src="<%=BASEURL%>/images/titulo.gif"></td>
        </tr>
      </table>
        <table width="100%" align="center" class="tablaInferior">
          <tr class="fila">
            <td width="32%"><div align="right">Origen:</div></td>
            <td width="68%">
              <% TreeMap origins  = model.tramoService.listarOrigenes();%>
              <input:select name="origen" attributesText="id='origen' class=textbox onChange='actualizarInfo();'" options="<%=origins %>"/> 
		    <img src="<%= BASEURL%>/images/botones/iconos/obligatorio.gif" width="10" height="10"></td>
          </tr>
          <tr class="fila">
            <td><div align="right">Destino:</div></td>
            <td>
              <%
          TreeMap destinos = new TreeMap();
		  destinos.put("Seleccione","");
          if ( request.getParameter("origenselec")!=null){
             destinos = model.tramoService.listarDestinos(request.getParameter("origenselec").toString());
          }
     %>
              <input:select name="destino" attributesText="class=textbox" options="<%=destinos %>"/>
              <input type="hidden" name="origenselec" id="origenselec" value="<%=request.getParameter("origenselec") %>"/>
              <img src="<%= BASEURL%>/images/botones/iconos/obligatorio.gif" width="10" height="10"> </td>
          </tr>
        </table>      </td>
    </tr>
  </table>
  <div align="center"><br>
    <img src="<%= BASEURL%>/images/botones/aceptar.gif"  name="imgaceptar" onClick="if ( validarTCamposLlenos() ) forma.submit();" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" style="cursor:hand ">&nbsp; <img src="<%= BASEURL%>/images/botones/cancelar.gif"  name="imgcancelar"  onMouseOver="botonOver(this);" onClick="forma.reset();" onMouseOut="botonOut(this);"  style="cursor:hand ">&nbsp; <img src="<%= BASEURL%>/images/botones/salir.gif"  name="imgsalir"  onMouseOver="botonOver(this);" onClick="parent.close();" onMouseOut="botonOut(this);"  style="cursor:hand "></div>
</form>
</div>
</body>
</html>