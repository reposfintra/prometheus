<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
"http://www.w3.org/TR/html4/loose.dtd">
<%@page contentType="text/html"%>
<%@page session="true"%>
<%@page errorPage="../error/ErrorPage.jsp"%>
<%@page import="com.tsp.operation.model.*"%>
<%@page import="com.tsp.util.*"%>
<%@page import="com.tsp.operation.model.beans.*, java.util.*"%>
<%@ include file="/WEB-INF/InitModel.jsp"%>
<%@taglib uri="http://jsptags.com/tags/navigation/pager" prefix="pg" %>



<html>
<head>
<title>Listado de Estados</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link href="<%= BASEURL %>/css/estilo.css" rel="stylesheet" type="text/css"> 
</head>

<body>
<%  String style = "simple";
    String position =  "bottom";
    String index =  "center";
    int maxPageItems = 10;
    int maxIndexPages = 10;
    String nompais,codpais;
    Pais pais; 
    pais = model.paisservice.obtenerpais(); 
    nompais = pais.getCountry_name();
    //String lenguaje = (String) session.getAttribute("idioma");
   // model.idiomaService.cargarIdioma(lenguaje, "VerEstado.jsp");
   // Properties lista = model.idiomaService.getIdioma();
    model.estadoservice.Estados( pais.getCountry_code());
    Vector VecEstado = model.estadoservice.obtEstados();
    Estado estado;  
    if ( VecEstado.size() >0 ){ 
   %>
<table width="547" height="66" align="center" border="1" cellpadding="4" cellspacing="1" bordercolor="#B7D0DC">
  <tr class="titulo">
    <td colspan="10" ><div align="center">Estados De <%=nompais%>
           
    </div></td>
  </tr>
  <tr class="subtitulos">
    <td width="108" align="center">Codigo</td>
    <td width="205" align="center">Nombre</td>
    <td width="133" align="center">Zona</td>
  </tr>
  <pg:pager
    items="<%=VecEstado.size()%>"
    index="<%= index %>"
    maxPageItems="<%= maxPageItems %>"
    maxIndexPages="<%= maxIndexPages %>"
    isOffset="<%= true %>"
    export="offset,currentPageNumber=pageNumber"
    scope="request">
  <%-- keep track of preference --%>
  <%
      for (int i = offset.intValue(), l = Math.min(i + maxPageItems, VecEstado.size()); i < l; i++)
	  {
          estado = (Estado) VecEstado.elementAt(i);%>
  <pg:item>
  <tr class="letra" bgcolor="#B7D0DC" onMouseOver="bgColor='#0092A6'" onMouseOut="bgColor='B7D0DC'" style="cursor:hand" 
      onClick="window.open('<%=CONTROLLER%>?estado=Buscar&accion=Estado&pagina=estado.jsp&carpeta=jsp/trafico/estado&codpais=<%=estado.getpais_code()%>&codigo=<%=estado.getdepartament_code()%>&mensaje=' ,'','status=no,scrollbars=no,width=400,height=300,resizable=yes');">
    <td height="21" ><%=estado.getdepartament_code()%></td>
    <td ><%=estado.getdepartament_name()%></td>
    <td ><%=estado.getzona()%></td>
  </tr>
  </pg:item>
  <%}%>
  <tr class='fila'>
    <td height="29" colspan="4" >
      <div align="center">
	     <pg:index>
            <jsp:include page="../WEB-INF/jsp/google.jsp" flush="true"/>
         </pg:index>
	  </div></td>
  </tr>
  </pg:pager>
</table>
<%}
 else { %>
<table width="100%">
  <tr>
    <td height="50" bgcolor="006699"> </td>
  </tr>
  <tr>
    <td height="60" bgcolor="ebebeb" align="center"> <span class="mensajes">Error en Busqqueda></span> </td>
  </tr>
  <tr>
    <td height="50"  bgcolor="006699">&nbsp; </td>
  </tr>
</table>
<%}%>
<p><a style="cursor:hand" class="letra_resaltada" onClick="window.location='<%=CONTROLLER%>?estado=Menu&accion=Cargar&pagina=SeleccionarPais.jsp&mensaje=&carpeta=jsp/trafico/estado'">Atras</a></p>
</body>
</html>
