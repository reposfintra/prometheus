<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
"http://www.w3.org/TR/html4/loose.dtd">
<%@page contentType="text/html"%>
<%@page session="true"%>
<%@page errorPage="../error/ErrorPage.jsp"%>
<%@page import="java.util.*"%>
<%@page import="com.tsp.operation.model.*"%>
<%@page import="com.tsp.util.*"%>
<%@page import="com.tsp.operation.model.beans.*, java.util.*"%>
<%@include file="/WEB-INF/InitModel.jsp"%>

<html>
<head>
<title>ESTADO</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link href="<%= BASEURL %>/css/estilostsp.css" rel="stylesheet" type="text/css"> 
<link href="../../../css/estilostsp.css" rel="stylesheet" type="text/css"> 
<script type='text/javascript' src="<%= BASEURL %>/js/validar.js"></script>
<script type='text/javascript' src="<%= BASEURL %>/js/boton.js"></script>
</head>

<body <%if(request.getParameter("reload")!=null){%>onLoad="parent.opener.location.reload();"<%}%>>
<% 
   model.paisservice.listarpaises();
   List Paises = model.paisservice.obtenerpaises();
   Pais pais; 
   Estado estado; 
   int i=0;
   Iterator it=Paises.iterator();
   String mensaje =(request.getParameter("mensaje")!=null)?request.getParameter("mensaje"):"";
   String cod="", nom="",zona="", men="", codpais="",nompais="" ;
   String Titulo= ("Ingresar Estado");
   String Subtitulo ="Datos del Estado";
   String Nompais = "Pais";
   String Codigo = "Codigo";
   String Nombre = "Nombre";
   String zona1 = "Zona";
   String action= CONTROLLER + "?estado=Insertar&accion=Estado";
   String a = (request.getParameter("sw")!=null)?request.getParameter("sw"):"-1";
   int swcon=-1;
   String tipo="text";
   
   if ( a != null ){
        swcon = Integer.parseInt( a );
   }	
   
   if ( swcon == 0 ){
        pais = model.paisservice.obtenerpais(); 
        codpais = pais.getCountry_code();
        nompais = pais.getCountry_name();
        cod = request.getParameter("c_codigo");
	nom = request.getParameter("c_nombre");
	zona = request.getParameter("c_zona");	    
   }
   else if ( swcon == 1 ){
    tipo="hidden";
	estado = model.estadoservice.obtenerestado();
	cod = estado.getdepartament_code();
        nom = estado.getdepartament_name();
	zona = estado.getzona();
	Titulo="Modificar Estado";                   
	action = CONTROLLER + "?estado=Modificar&accion=Estado";
        pais = model.paisservice.obtenerpais(); 
	codpais = pais.getCountry_code();
	nompais = pais.getCountry_name();
   }
%>

<form name="forma" method="post" action="<%=action%>" >
  <table width="380" border="2" align="center">
    <tr>
      <td><table width="100%"  border="0">
        <tr>
          <td width="48%" class="subtitulo1"><%=Subtitulo%></td>
          <td width="52%" class="barratitulo"><img src="<%=BASEURL%>/images/titulo.gif" width="32" height="20"></td>
        </tr>
      </table>
      <table width="100%" class="tablaInferior">
       
        <tr class="fila">
          <td width="119" valign="middle" align="left" ><%=Nompais%></td>
          <td width="235" valign="middle">
            <%if (swcon==1){%>
            <input type="hidden" class="textbox" name="c_pais" value="<%=codpais%>" >
            <%=nompais%>
            <%} 
		   else{%>
            <select name="c_pais"  class="textbox" id="c_pais">
              <% while (it.hasNext()){  
			    pais = (Pais) it.next();	
                if (pais.getCountry_code().equals(codpais)){%>
              <option value="<%=codpais%>" selected><%=nompais%></option>
              <%}
			    else{%>
              <option value="<%=pais.getCountry_code()%>"><%=pais.getCountry_name()%></option>
              <%}
		    }%>
            </select>
            <img src="<%=BASEURL%>/images/botones/iconos/obligatorio.gif" width="10" height="10">
			<%}%></td>
        </tr>
        <tr class="fila">
          <td valign="middle" align="left" ><%=Codigo%></td>
          <td valign="middle"><input name="c_codigo" type="<%=tipo%>" class="textbox"  maxlength="3" value="<%=cod%>" ><%if(swcon==1){%> <%=cod%> <%}else{%>
            <img src="<%=BASEURL%>/images/botones/iconos/obligatorio.gif" width="10" height="10"> <%}%>         </td>
        </tr>
        <tr class="fila">
          <td valign="middle" ><%=Nombre%></td>
          <td valign="middle"><input name="c_nombre" type="text" class="textbox" maxlength="40" value="<%=nom%>" >
            <img src="<%=BASEURL%>/images/botones/iconos/obligatorio.gif" width="10" height="10"></td>
        </tr>
        <tr class="fila">
          <td valign="middle" ><%=zona1%></td>
          <td valign="middle"><input name="c_zona" type="text" class="textbox"  maxlength="5" value="<%=zona%>" ></td>
        </tr>
        
      </table></td>
    </tr>
  </table>
  <p><%if((swcon==0) || (swcon==-1) ){%>
  <div align="center">
          <img src="<%=BASEURL%>/images/botones/aceptar.gif"  name="imgaceptar" onClick="return TCamposLlenos();" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" style="cursor:hand">&nbsp;
		  <img src="<%=BASEURL%>/images/botones/cancelar.gif"  name="imgcancelar"  onMouseOver="botonOver(this);" onClick="forma.reset();" onMouseOut="botonOut(this);" style="cursor:hand"> &nbsp;
          <img src="<%=BASEURL%>/images/botones/salir.gif"  name="imgsalir"  onMouseOver="botonOver(this);" onClick="parent.close();" onMouseOut="botonOut(this);" style="cursor:hand">
  </div></p>
      <%} 
	  else {%>
            <div align="center">
			  <img src="<%=BASEURL%>/images/botones/modificar.gif"  name="imgmodificar" onClick="return TCamposLlenos();" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" style="cursor:hand">&nbsp;
              <img src="<%=BASEURL%>/images/botones/anular.gif"  name="imganular" onClick="window.location='<%=CONTROLLER%>?estado=Anular&accion=Estado&codigo=<%=cod%>&pais=<%=codpais%>&nombre=<%=nom%>&zona=<%=zona%>'" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" style="cursor:hand">&nbsp;
              <img src="<%=BASEURL%>/images/botones/salir.gif"  name="imgsalir"  onMouseOver="botonOver(this);" onClick="parent.close();" onMouseOut="botonOut(this);" style="cursor:hand">
            </div>
     <%}%>
  
  
<%if( !mensaje.equalsIgnoreCase("")){%>
  <p><table border="2" align="center">
  <tr>
    <td><table width="100%"  border="1" align="center"  bordercolor="#F7F5F4" bgcolor="#FFFFFF">
      <tr>
        <td width="229" align="center" class="mensajes"><%=mensaje%></td>
        <td width="29" background="<%=BASEURL%>/images/cuadronaranja.JPG">&nbsp;</td>
        <td width="58">&nbsp;</td>
      </tr>
    </table></td>
  </tr>
</table>
</p>
 <%}%>

</form>
</body>
</html>
