<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
"http://www.w3.org/TR/html4/loose.dtd">
<%@page contentType="text/html"%>
<%@page session="true"%>
<%@page errorPage="../error/ErrorPage.jsp"%>
<%@page import="java.util.*"%>
<%@page import="com.tsp.operation.model.*"%>
<%@page import="com.tsp.util.*"%>
<%@page import="com.tsp.operation.model.beans.*, java.util.*"%>
<%@include file="/WEB-INF/InitModel.jsp"%>
<%@taglib uri="http://jsptags.com/tags/navigation/pager" prefix="pg" %>

<html>
<head>
<title>Listado de Estados</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link href="<%= BASEURL %>/css/estilostsp.css" rel="stylesheet" type="text/css"> 
<link href="../../../css/estilostsp.css" rel="stylesheet" type="text/css"> 
<script type='text/javascript' src="<%= BASEURL %>/js/reporte.js"></script>
<script type='text/javascript' src="<%= BASEURL %>/js/boton.js"></script>
</head>

<body>
<%  String style = "simple";
    String position =  "bottom";
    String index =  "center";
    int maxPageItems = 10;
    int maxIndexPages = 10;
	String nompais = (String) session.getAttribute("nompais");
	String nomestado = (String) session.getAttribute("nomestado");
    String zona = (String) session.getAttribute("zona");
	model.estadoservice.buscarestadoXnom(nompais+"%", nomestado+"%",zona+"%"); 
    Vector VecEstado = model.estadoservice.obtEstados();
    Estado estado;
	if ( VecEstado.size() >0 ){  
%>
  <form name="form1" method="post" action="">
    <table width="465" border="2" align="center">
      <tr>
        <td><table width="99%" bgcolor="#FFFFFF" align="center">
            <tr>
              <td width="48%" class="subtitulo1">&nbsp;Datos Estado </td>
              <td width="52%" class="barratitulo"><img src="<%=BASEURL%>/images/titulo.gif" width="32" height="20">
              <input name="nomestado" type="hidden" id="nomestado" value="<%=nomestado%>">
              <input name="nompais" type="hidden" id="nompais" value="<%=nompais%>">
              <input name="zona" type="hidden" id="zona" value="<%=zona%>"></td>
            </tr>
          </table>
            <table width="99%" border="1" align="center" bordercolor="#999999" bgcolor="#F7F5F4">
              <tr class="tblTitulo">
                <td width="123" align="center">Pais</td>
                <td width="61"  align="center">Codigo</td>
                <td width="158" align="center">Nombre</td>
                <td width="51"  align="center">Zona</td>
              </tr>
              <pg:pager
    items="<%=VecEstado.size()%>"
    index="<%= index %>"
    maxPageItems="<%= maxPageItems %>"
    maxIndexPages="<%= maxIndexPages %>"
    isOffset="<%= true %>"
    export="offset,currentPageNumber=pageNumber"
    scope="request">
              <%-- keep track of preference --%>
              <%
      for (int i = offset.intValue(), l = Math.min(i + maxPageItems, VecEstado.size()); i < l; i++)
	  {
          estado = (Estado) VecEstado.elementAt(i);%>
              <pg:item>
              <tr class="<%=(i % 2 == 0 )?"filagris":"filaazul"%>" onMouseOver='cambiarColorMouse(this)'   style="cursor:hand" 
        onClick="window.open('<%=CONTROLLER%>?estado=Buscar&accion=Estado&pagina=estado.jsp&carpeta=jsp/trafico/estado&codpais=<%=estado.getpais_code()%>&codigo=<%=estado.getdepartament_code()%>&mensaje=' ,'','status=no,scrollbars=no,width=620,height=300,resizable=yes');">
                <td height="21" class="bordereporte"><%=estado.getpais_name()%></td>
                <td height="21" class="bordereporte"><%=estado.getdepartament_code()%></td>
                <td class="bordereporte"><%=estado.getdepartament_name()%></td>
                <td class="bordereporte"><%=estado.getzona()%></td>
              </tr>
              </pg:item>
              <%}%>
              <tr class="bordereporte">
                <td colspan="4" align="center" valign="middle" > <pg:index>
                  <jsp:include page="../../../WEB-INF/jsp/googlesinimg.jsp" flush="true"/>      
          </pg:index> </td>
              </tr>
              </pg:pager>
          </table></td>
      </tr>
    </table>
  </form>
  <%}
 else { %> 
<table border="2" align="center">
    <tr>
      <td><table width="410" height="40" border="1" align="center"  bordercolor="#F7F5F4" bgcolor="#FFFFFF">
          <tr>
            <td width="282" align="center" class="mensajes">Su b&uacute;squeda no arroj&oacute; resultados!</td>
            <td width="28" background="<%=BASEURL%>/images/cuadronaranja.JPG">&nbsp;</td>
            <td width="78">&nbsp;</td>
          </tr>
      </table></td>
    </tr>
</table>
  

<p>
  <%}%>
</p>
<table width="465" border="0" align="center">
  <tr>
    <td width="475"><img src="<%=BASEURL%>/images/botones/regresar.gif"  name="imgaceptar" onclick="window.location='<%=CONTROLLER%>?estado=Menu&accion=Cargar&pagina=BuscarEstado.jsp&carpeta=/jsp/trafico/estado&titulo=Buscar Estado'" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" style="cursor:hand"></td>  </tr>
</table>
<p>&nbsp; </p>
<p></p>
</body>
</html>
