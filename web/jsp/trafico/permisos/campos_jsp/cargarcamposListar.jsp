<%@page contentType="text/html"%>
<%@page session="true"%>
<%@page errorPage="/error/ErrorPage.jsp"%>
<%@page import="java.util.*"%>
<%@page import="com.tsp.operation.model.*"%>
<%@page import="com.tsp.operation.model.beans.*"%>
<%@page import="com.tsp.util.*"%>
<%@include file="/WEB-INF/InitModel.jsp"%>
<%@taglib uri="http://jsptags.com/tags/navigation/pager" prefix="pg" %>
<html>
<head>
<title>Campos</title>
<script type='text/javascript' src="<%= BASEURL %>/js/validar.js"></script>
<script SRC='<%=BASEURL%>/js/boton.js'></script>
<link href="<%= BASEURL %>/css/estilostsp.css" rel='stylesheet'>
<script type='text/javascript' src="<%= BASEURL %>/js/reporte.js"></script>
<script type='text/javascript' src="<%= BASEURL %>/js/boton.js"></script>
<link href="../../../../css/estilostsp.css" rel="stylesheet" type="text/css">
</head>
<body onresize="redimensionar()" onload = 'redimensionar()'>
<div id="capaSuperior" style="position:absolute; width:100%; height:100px; z-index:0; left: 0px; top: 0px;">
 <jsp:include page="/toptsp.jsp?encabezado=Paginas JSP"/>
</div>
<div id="capaCentral" style="position:absolute; width:100%; height:87%; z-index:0; left: 0px; top: 100px; overflow: scroll;">

<% 	
    Vector vec = (Vector) session.getAttribute("tipo_ubicaciones");
    String pagina = (String) session.getAttribute("pag");
    String cod_jsp = (String) session.getAttribute("cod_jsp");
    String style = "simple";
    String position =  "bottom";
    String index =  "center";
    String sw = request.getParameter("sw");
	int maxPageItems = 7;
    int maxIndexPages = 5;
	if (vec!=null){
       
    }
	else{
		vec = model.camposjsp.getCampos();   
    }
	if (vec.size() > 0) {
%>
<br>
<table width="90%" border="2" align="center">
  <tr>
    <td class="barratitulo">
	  <table width="100%">
        <tr>
          <td width="373" class="subtitulo1">Campos</td>
          <td width="427" ><img src="<%=BASEURL%>/images/titulo.gif" width="32" height="20"></td>
        </tr>
      </table>
	  <table width="100%" border="1" cellpadding="4" cellspacing="1" bordercolor="#999999" bgcolor="#F7F5F4" align="center">
	          <tr class="fila" align="center">
          <td colspan="2" nowrap ><div align="left">P�gina: <%= pagina %> <img src="<%= BASEURL%>/images/botones/iconos/buscar.gif" alt="Ver c&oacute;digo"  name="imgsalir" align="absmiddle" style="cursor:hand " onClick="window.open('<%=BASEURL%>/jsp/trafico/permisos/campos_jsp/Campos_jspVerCodigo.jsp?codjsp=<%= cod_jsp%>','verConsulta','status=no,scrollbars=yes,width=480,height=500,resizable=yes');"></div></td>
        </tr>
        <tr class="tblTitulo" align="center">
          <td width="349" nowrap >Campo</td>
          <td width="320" nowrap >Tipo Campo </td>
		</tr>
    <pg:pager        
		items="<%=vec.size()%>"
        index="<%= index %>"
        maxPageItems="<%= maxPageItems %>"
        maxIndexPages="<%= maxIndexPages %>"
        isOffset="<%= true %>"
        export="offset,currentPageNumber=pageNumber"
        scope="request">
<%
    for (int i = offset.intValue(), l = Math.min(i + maxPageItems, vec.size()); i < l; i++){
        campos_jsp campo = (campos_jsp) vec.elementAt(i);
        String tipo_campo = "";
        if( campo.getTipo_campo().matches("c") ){
            tipo_campo = "Campo texto";
        } else if( campo.getTipo_campo().matches("i") ) {                 
            tipo_campo = "Imagen";
        } else if( campo.getTipo_campo().matches("s") ) {                 
            tipo_campo = "Combo Box";
        } else if( campo.getTipo_campo().matches("l") ) {                 
            tipo_campo = "Hiperv&iacute;nculo";
        } else {                 
            tipo_campo = "Campo texto";
        }
%>
        <pg:item>
        <tr class="<%=(i % 2 == 0 )?"filagris":"filaazul"%>" onMouseOver='cambiarColorMouse(this)' style="cursor:hand" title="Lista de Campos" onClick="window.open('<%=CONTROLLER%>?estado=Campos_jsp&accion=Serch&c_pagina=<%=campo.getPagina()%>&c_campo=<%=campo.getCampo()%>&listar=False','myWindow','status=no,scrollbars=no,width=450,height=170,resizable=yes');">
          <td class="bordereporte"><%=campo.getCampo()%></td>
          <td class="bordereporte"><%=tipo_campo%></td>
		</tr>
      </pg:item>
<%  }%>
        <tr class="bordereporte">
          <td td height="20" colspan="14" nowrap align="center">          <pg:index>
                <jsp:include page="/WEB-INF/jsp/googlesinimg.jsp" flush="true"/>
        </pg:index>
          </td>
        </tr>
    </pg:pager>        
      </table>
</table>
<%}else {%>
    <table border="2" align="center">
      <tr>
        <td>
	      <table width="100%"  border="1" align="center"  bordercolor="#F7F5F4" bgcolor="#FFFFFF">
            <tr>
              <td width="229" align="center" class="mensajes">No se encontraron Campos relacionados</td>
              <td width="29"><img src="<%=BASEURL%>/images/cuadronaranja.JPG"></td>
            </tr>
          </table>
	    </td>
      </tr>
    </table>
<%}%>
<p>
<table width="90%" align="center">
	  <tr></tr>
	  <tr>
        <img src="<%=BASEURL%>/images/botones/regresar.gif" name="c_salir" id="c_salir" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" onClick="parent.history.back();">
	  </tr>
</table>
</div>
</body>
</html>
