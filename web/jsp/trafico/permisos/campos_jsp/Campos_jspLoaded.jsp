<%@page contentType="text/html"%>
<%@page session="true"%>
<%@page errorPage="/error/ErrorPage.jsp"%>
<%@page import="java.util.*"%>
<%@page import="com.tsp.operation.model.*"%>
<%@page import="com.tsp.operation.model.beans.*"%>
<%@page import="com.tsp.util.*"%>
<%@include file="/WEB-INF/InitModel.jsp"%>
<%@taglib uri="http://jsptags.com/tags/navigation/pager" prefix="pg" %>
<%String path = application.getRealPath(model.menuService.getRealUrl(request.getRequestURI()));
  String datos[] = model.menuService.getContenidoMenu(BASEURL,request.getRequestURI(),path);
%>
<html>
<head>
<title>Campos</title>
<script type='text/javascript' src="<%= BASEURL %>/js/validar.js"></script>
<script SRC='<%=BASEURL%>/js/boton.js'></script>
<link href="<%= BASEURL %>/css/estilostsp.css" rel='stylesheet'>
<script type='text/javascript' src="<%= BASEURL %>/js/reporte.js"></script>
<script type='text/javascript' src="<%= BASEURL %>/js/boton.js"></script>
<link href="../../../../css/estilostsp.css" rel="stylesheet" type="text/css">
</head>
<body onresize="redimensionar()" onload = 'redimensionar()'>
<div id="capaSuperior" style="position:absolute; width:100%; height:100px; z-index:0; left: 0px; top: 0px;">
 <jsp:include page="/toptsp.jsp?encabezado=Paginas JSP"/>
</div>
<div id="capaCentral" style="position:absolute; width:100%; height:87%; z-index:0; left: 0px; top: 100px; overflow: scroll;">

<% 	
    Vector vec = (Vector) session.getAttribute("tipo_ubicaciones");
    String pagina = (String) session.getAttribute("pag");
    String cod_jsp = (String) session.getAttribute("cod_jsp");
    String style = "simple";
    String position =  "bottom";
    String index =  "center";
    String sw = request.getParameter("sw");
	int maxPageItems = 12;
    int maxIndexPages = 5;
	if (vec!=null){
       
    }
	else{
		vec = new Vector();   
    }
	if (vec.size() > 0) {
%>
<form name="forma" id="forma" method="post" action="<%=CONTROLLER%>?estado=Campos_jsp&accion=Ingresar&cmd=show">
  <table width="551" border="2" align="center">
    <tr>
      <td class="barratitulo">
        <table width="100%">
          <tr>
            <td width="373" class="subtitulo1">Campos</td>
            <td width="427" ><img src="<%=BASEURL%>/images/titulo.gif" width="32" height="20" align="left"><%=datos[0]%></td>
          </tr>
        </table>
        <table width="100%" border="1" cellpadding="4" cellspacing="1" bordercolor="#999999" bgcolor="#F7F5F4" align="center">
          <tr class="fila" align="center">
            <td colspan="3" nowrap ><div align="left">P&aacute;gina: <%= pagina %> <img src="<%= BASEURL%>/images/botones/iconos/buscar.gif" alt="Ver c&oacute;digo"  name="imgsalir" align="absmiddle" style="cursor:hand " onClick="window.open('<%=BASEURL%>/jsp/trafico/permisos/campos_jsp/Campos_jspVerCodigoResaltado.jsp?codjsp=<%= cod_jsp%>','verConsulta','status=no,scrollbars=yes,width=480,height=500,resizable=yes');">
			
              <input name="cod_jsp" type="hidden" id="cod_jsp" value='<%= cod_jsp%>'>
</div></td>
          </tr>
          <tr class="tblTitulo" align="center">
            <td width="40%" nowrap >Campo</td>
            <td width="38%" nowrap >Tipo Campo </td>
            <td width="22%" nowrap >Incluir</td>
          </tr>
          <%
    for (int i = 0; i < vec.size(); i++){
        campos_jsp campo = (campos_jsp) vec.elementAt(i);
        /*String tipo_campo = "";
        if( campo.getTipo_campo().matches("c") ){
            tipo_campo = "Campo texto";
        } else if( campo.getTipo_campo().matches("i") ) {                 
            tipo_campo = "Imagen";
        } else if( campo.getTipo_campo().matches("s") ) {                 
            tipo_campo = "Combo Box";
        } else if( campo.getTipo_campo().matches("l") ) {                 
            tipo_campo = "Hiperv&iacute;nculo";
        } else {                 
            tipo_campo = "Campo texto";
        }*/
%>
          <tr class="<%=(i % 2 == 0 )?"filagris":"filaazul"%>" title="Lista de Campos">
            <td class="bordereporte"><%=campo.getCampo()%></td>
            <td class="bordereporte"><%=campo.getTipo_campo()%></td>
            <td class="bordereporte"><div align="center">
              <input name="campos" type="checkbox" class="tablaInferior" id="campos" value="<%=campo.getCampo() + "~" + campo.getTipo_campo()%>">
            </div></td>
          </tr>
          <%  }%>
        </table>
  </table>
  <div align="center"><br>
      <img src="<%= BASEURL%>/images/botones/aceptar.gif"  name="imgaceptar" onClick="if ( validarTCamposLlenos() ) forma.submit();" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" style="cursor:hand ">&nbsp; <img src="<%= BASEURL%>/images/botones/cancelar.gif"  name="imgcancelar"  onMouseOver="botonOver(this);" onClick="forma.reset();" onMouseOut="botonOut(this);"  style="cursor:hand ">&nbsp; <img src="<%= BASEURL%>/images/botones/salir.gif"  name="imgsalir"  onMouseOver="botonOver(this);" onClick="parent.close();" onMouseOut="botonOut(this);"  style="cursor:hand "></div>
</form>
<br>
<%}else {%>
    <table border="2" align="center">
      <tr>
        <td>
	      <table width="100%"  border="1" align="center"  bordercolor="#F7F5F4" bgcolor="#FFFFFF">
            <tr>
              <td width="229" align="center" class="mensajes">No se encontraron Campos relacionados</td>
              <td width="29"><img src="<%=BASEURL%>/images/cuadronaranja.JPG"></td>
            </tr>
          </table>
	    </td>
      </tr>
    </table>
<%}%>
</div>
<%=datos[1]%>
</body>
</html>
