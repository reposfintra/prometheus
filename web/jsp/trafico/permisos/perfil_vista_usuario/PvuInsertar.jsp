<%@page contentType="text/html"%>
<%@page session="true"%>
<%@page import="java.util.*"%>
<%@page import="com.tsp.operation.model.*"%>
<%@page import="com.tsp.operation.model.beans.*"%>
<%@page import="com.tsp.util.*"%>
<%@include file="/WEB-INF/InitModel.jsp"%>
<%String path = application.getRealPath(model.menuService.getRealUrl(request.getRequestURI()));
  String datos[] = model.menuService.getContenidoMenu(BASEURL,request.getRequestURI(),path);
%>
<html>
<head>
<title>Insertar un Perfil de Vista para un Usuario</title>
<link href="../../../../css/estilostsp.css" rel="stylesheet" type="text/css">
<link href="<%= BASEURL %>/css/estilostsp.css" rel="stylesheet" type="text/css">
<script SRC='<%=BASEURL%>/js/boton.js'></script>
<script type='text/javascript' src="<%= BASEURL %>/js/validar.js"></script>
</head>

<body onresize="redimensionar()" onload = 'redimensionar()'>
<div id="capaSuperior" style="position:absolute; width:100%; height:100px; z-index:0; left: 0px; top: 0px;">
 <jsp:include page="/toptsp.jsp?encabezado=Perfil Vista Usuario"/>
</div>
<div id="capaCentral" style="position:absolute; width:100%; height:87%; z-index:0; left: 0px; top: 100px; overflow: scroll;">
<%
    String mensaje = (String) request.getParameter("mensaje");
%>
<FORM name='forma' id='forma' method='POST' onSubmit="return validarTCamposLlenos();" action=''>
  <table width="400" border="2" align="center">
    <tr>
      <td><table width="100%" class="tablaInferior">
        <tr>
          <td width="50%" class="subtitulo1">Informaci&oacute;n</td>
          <td width="50%" class="barratitulo"><img src="<%=BASEURL%>/images/titulo.gif" width="32" height="20" align="left"><%=datos[0]%></td>
        </tr>
      </table>
        <table width="100%" align="center" cellpadding="4" cellspacing="1" class="tablaInferior">
          <tr class="fila">
            <td width="142">Usuario</td>
            <td width="274">
              <select id="select" name="c_usuario" class="textobligatorio">
                <option value="">Seleccione</option>
                <% model.usuarioService.obtenerUsuarios();
					Vector users = model.usuarioService.getUsuarios();					
						for (int i = 0; i < users.size(); i++){
							Usuario user = (Usuario) users.elementAt(i);				
		    				if (user.getLogin().equals(" ")){%>
                <option value="<%=" "%>" selected><%=" "%></option>
                <%}else{%>
                <option value="<%=user.getLogin()%>"><%=user.getNombre()%></option>
                <%}
					}%>
              </select>
              <img src="<%= BASEURL%>/images/botones/iconos/obligatorio.gif" width="10" height="10"> </td>
          </tr>
          <tr class="fila">
            <td width="142">Perfil</td>
            <td width="274" >
              <select name="c_perfil" id="c_perfil" class="textobligatorio" >
                <%model.perfilService.listarPerfilesxPropietario();
		Vector vec = model.perfilService.getVPerfil();
				if (vec != null){			
	     	%>
                <option value="">Seleccione</option>
                <%
			for(int i=0; i < vec.size();i++){  
				Perfil pv = (Perfil) vec.elementAt(i);	
			%>
                <option value="<%=pv.getId()%>"><%=pv.getNombre()%></option>
                <%
	   		}
	   }%>
            </select>
            <img src="<%= BASEURL%>/images/botones/iconos/obligatorio.gif" width="10" height="10"></td>
          </tr>
        </table>      </td>
    </tr>
  </table>
  <p align="center"><img src="<%= BASEURL%>/images/botones/aceptar.gif"  name="imgaceptar" onClick="forma.action='<%=CONTROLLER%>?estado=pvu&accion=Ingresar'; if ( validarTCamposLlenos() ) forma.submit();" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);">&nbsp; <img src="<%= BASEURL%>/images/botones/cancelar.gif"  name="imgcancelar"  onMouseOver="botonOver(this);" onClick="forma.reset();" onMouseOut="botonOut(this);" >&nbsp; <img src="<%= BASEURL%>/images/botones/salir.gif"  name="imgsalir"  onMouseOver="botonOver(this);" onClick="parent.close();" onMouseOut="botonOut(this);" ></p>
  <p>
    <%  if(mensaje!=null){%>
  </p>
  <p></p>
<table border="2" align="center">
  <tr>
    <td><table width="410" height="73" border="1" align="center"  bordercolor="#F7F5F4" bgcolor="#FFFFFF">
        <tr>
          <td width="282" align="center" class="mensajes"><%=mensaje %></td>
          <td width="28" background="<%=BASEURL%>/images/cuadronaranja.JPG">&nbsp;</td>
          <td width="78">&nbsp;</td>
        </tr>
    </table></td>
  </tr>
</table>
    <%  }%>	
</FORM>
</div>
<%=datos[1]%>
</body>
</html>
