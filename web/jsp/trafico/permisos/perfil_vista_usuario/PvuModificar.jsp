<%@page contentType="text/html"%>
<%@page session="true"%>
<%@page import="java.util.*"%>
<%@page import="com.tsp.operation.model.*"%>
<%@page import="com.tsp.operation.model.beans.*"%>
<%@page import="com.tsp.util.*"%>
<%@include file="/WEB-INF/InitModel.jsp"%>
<%String path = application.getRealPath(model.menuService.getRealUrl(request.getRequestURI()));
  String datos[] = model.menuService.getContenidoMenu(BASEURL,request.getRequestURI(),path);
%>
<html>
<head>
<title>Modificar Perfil-Vista-Usuario</title>
<link href="../../../../css/estilostsp.css" rel="stylesheet" type="text/css">
<link href="<%= BASEURL %>/css/estilostsp.css" rel="stylesheet" type="text/css">
<script SRC='<%=BASEURL%>/js/boton.js'></script>
<script type='text/javascript' src="<%= BASEURL %>/js/validar.js"></script>
</head>
<body onresize="redimensionar()" <%if(request.getParameter("reload")!=null){%>onLoad="parent.opener.location.reload();"<%}%>>
<div id="capaSuperior" style="position:absolute; width:100%; height:100px; z-index:0; left: 0px; top: 0px;">
 <jsp:include page="/toptsp.jsp?encabezado=Perfil Vista Usuario"/>
</div>
<div id="capaCentral" style="position:absolute; width:100%; height:87%; z-index:0; left: 0px; top: 100px; overflow: scroll;">

<%
    Usuario usuario = (Usuario) session.getAttribute("Usuario");
    PerfilVistaUsuario u = model.perfilvistausService.getPerfilVistaUsuario();//(PerfilVistaUsuario) request.getAttribute("pvu");    
    model.perfilvistausService.setPerfilVistaUsuario(null);
    String mensaje = (String) request.getAttribute("mensaje");
    Vector vec = new Vector();
	//out.println(u.getCodigo());
	
%>
<FORM name='forma' id='forma' method='POST' onSubmit="return validarTCamposLlenos();" action=''>
    <table width="420" border="2" align="center">
      <tr>
        <td><table width="100%" class="tablaInferior">
          <tr>
            <td width="50%" class="subtitulo1">Informaci&oacute;n</td>
            <td width="50%" class="barratitulo"><img src="<%=BASEURL%>/images/titulo.gif" width="32" height="20" align="left"><%=datos[0]%></td>
          </tr>
        </table>
          <table width="100%" align="center" class="tablaInferior">
            <input name="c_codigo" type="hidden"  id="c_codigo3" value="<%= u.getCodigo()%>" >
            <tr class="fila">
              <td width="142">Usuario</td>
              <td width="274"><input name="c_nombre" type="text" class="textbox" id="c_nombre2" onKeyPress="soloAlfa(event)" value="<%=u.getUsuario()%>" maxlength="6" readonly=""></td>
            </tr>
            <tr class="fila">
              <td width="142">Perfil</td>
              <td width="274" >
                <select name="c_perfil" id="select2" class="textbox">
                  <%model.perfilService.listarPerfilesxPropietario();
		 vec = model.perfilService.getVPerfil();
			if (vec != null){			
	     		for(int i=0; i < vec.size();i++){  
				Perfil pv = (Perfil) vec.elementAt(i);	
				if (pv.getId().equals(u.getPerfil())){%>
                  <option value="<%=pv.getId()%>" selected><%= pv.getNombre()%></option>
                  <%}else{%>
                  <option value="<%=pv.getId()%>"><%=pv.getNombre()%></option>
                  <%}
	   		}
	   }%>
                </select>
              </td>
            </tr>
            <tr class="fila">
              <td colspan="2"><div align="right"><a href="javascript:void(0);" class="letraresaltada"  onClick="window.open('<%=CONTROLLER%>?estado=Perfil_vista&accion=Consultar&consulta=S&c_perfil=' + forma.c_perfil.options(forma.c_perfil.selectedIndex).value,'verPerfilVista','status=no,scrollbars=no,width=650,height=430,resizable=yes');" style="visibility:visible ">Ver Perfil-Vista</a> </div></td>
            </tr>
          </table>          </td>
      </tr>
    </table>
    <br>
    <center>
<% if (mensaje!="MsgAnulado"){%>      
<img src="<%=BASEURL%>/images/botones/modificar.gif" name="c_modificar" onClick="forma.action='<%=CONTROLLER%>?estado=Pvu&accion=Update&sw='; if( validarTCamposLlenos() ) forma.submit();" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);"> <img src="<%=BASEURL%>/images/botones/anular.gif" name="c_anular" onClick="forma.action='<%=CONTROLLER%>?estado=Pvu&accion=Anular'; forma.submit();" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);">
      <%}
  if (mensaje!="MsgAgregado"){%>
      <img src="<%=BASEURL%>/images/botones/salir.gif" name="c_buscar" onClick="parent.close();" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);">
      <%}else{%>
      <img src="<%=BASEURL%>/images/botones/salir.gif" name="c_buscar" onClick="parent.close();" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);">
      <%}%>
    </center>
  <p>&nbsp;</p>
    <p><%  if(mensaje!=null){
			mensaje = (mensaje.equals("MsgModificado"))?"Modificacion exitosa!":"";%>
  </p>
    <p></p>
<table border="2" align="center">
  <tr>
    <td><table width="410" height="73" border="1" align="center"  bordercolor="#F7F5F4" bgcolor="#FFFFFF">
        <tr>
          <td width="282" align="center" class="mensajes"><%=mensaje%></td>
          <td width="28" background="<%=BASEURL%>/images/cuadronaranja.JPG">&nbsp;</td>
          <td width="78">&nbsp;</td>
        </tr>
    </table></td>
  </tr>
</table>
	<%  }%>

    <table width="56%"  border="0" align="center">
      <tr>
        <td width="44%"><input type="text" name="c_user_update" value="<%=usuario.getLogin()%>" class="textbox" style="visibility:hidden"></td>
        <td width="56%">&nbsp;</td>
      </tr>
      <tr>
        <td></td>
        <td>&nbsp;</td>
      </tr>
      <tr>
        <td><input type="text" name="c_creation_user" value="<%=usuario.getLogin()%>" class="textbox" style="visibility:hidden"></td>
        <td>&nbsp;</td>
      </tr>
    </table>
</FORM>
</div>
<%=datos[1]%>
</body>
</html>
