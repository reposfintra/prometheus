<%@page contentType="text/html"%>
<%@page session="true"%>
<%@page import="java.util.*"%>
<%@page import="com.tsp.operation.model.*"%>
<%@page import="com.tsp.operation.model.beans.*"%>
<%@page import="com.tsp.util.*"%>
<%@include file="/WEB-INF/InitModel.jsp"%>
<%String path = application.getRealPath(model.menuService.getRealUrl(request.getRequestURI()));
  String datos[] = model.menuService.getContenidoMenu(BASEURL,request.getRequestURI(),path);
%>
<html>
<head>
 <title>Detalle Pagina</title> 
 <script type='text/javascript' src="<%= BASEURL %>/js/validar.js"></script>
<script SRC='<%=BASEURL%>/js/boton.js'></script>
   <link href="<%= BASEURL %>/css/estilostsp.css" rel='stylesheet'>
   <link href="/css/estilostsp.css" rel="stylesheet" type="text/css">
   <link href="../../../css/estilostsp.css" rel="stylesheet" type="text/css">
</head>
<script type='text/javascript' src="<%= BASEURL %>/js/validar.js"></script>
<script SRC='<%=BASEURL%>/js/boton.js'></script>
<body  onresize="redimensionar()" onload = 'redimensionar();<%if(request.getParameter("reload")!=null){%>window.opener.location.reload();<%}%>'>
<div id="capaSuperior" style="position:absolute; width:100%; height:100px; z-index:0; left: 0px; top: 0px;">
 <jsp:include page="/toptsp.jsp?encabezado=Paginas JSP"/>
</div>
<div id="capaCentral" style="position:absolute; width:100%; height:87%; z-index:0; left: 0px; top: 100px; overflow: scroll;">
<%
    Usuario usuario = (Usuario) session.getAttribute("Usuario");
    Jsp u = (Jsp) request.getAttribute("jsp");    
    String mensaje = (String) request.getParameter("mensaje");
    Vector vec = new Vector();
	if (mensaje == null) {
%>
<FORM name='forma' id='forma' method='POST'  action=''>
  <table width="416" border="2" cellpadding="0" cellspacing="1" align="center">
    <tr class="titulo" align="center">
      <td>
	    <table width="444"  align="center" >
          <tr class="fila">
            <td colspan="2" >
			  <table width="100%"  cellspacing="1" cellpadding="0">
                <tr>
                  <td width="50%" class="subtitulo1">Modificar P&aacute;ginas</td>
                  <td width="50%" class="barratitulo"><img src="<%=BASEURL%>/images/titulo.gif" width="32" height="20" align="left"><%=datos[0]%></td>
                </tr>
              </table>
		    </td>
          </tr>
            <input name="c_codigo" type="hidden"  id="c_codigo" value="<%=u.getCodigo()%>" >
		    <tr class="fila">
		      <td>C&oacute;digo</td>
		      <td class="letra"><%=u.getCodigo()%></td>
	        </tr>
		    <tr class="fila">
            <td width="119">Nombre</td>
            <td width="227"><input name="c_nombre" type="text" class="textbox" id="c_nombre" onKeyPress="soloAlfa(event)" value="<%=u.getNombre()%>" ></td>
          </tr>
          <tr class="fila">
            <td width="119">Ruta</td>
            <td width="227"><input name="c_ruta" type="text" class="textbox" id="c_ruta"  value="<%=u.getRuta()%>" size="40" ></td>
          </tr>
          <tr class="fila">
            <td>Descripcion</td>
            <td><input name="c_descripcion" type="text" class="textbox" id="c_descripcion" value="<%=u.getDescripcion()%>" size="40" maxlength="45"></td>
          </tr>        
          <tr class="pie">
          	<td width="100%" align = "right" colspan="2">
			  <span class="Simulacion_Hiper"><a href="javaScript:void();" onClick="window.open('<%=CONTROLLER%>?estado=Campos_jsp&accion=Serch&listar=True&c_pagina=<%=u.getCodigo()%>&sw=jsp','myWin2','status=no,scrollbars=no,width=650,height=430,resizable=yes');"  target="_self" class="letra_resaltada">Ver Campos</a></span><a href="javaScript:void();" onClick="window.open('<%=CONTROLLER%>?estado=Campos_jsp&accion=Serch&listar=True&c_pagina=<%=u.getCodigo()%>&sw=jsp','myWin2','status=no,scrollbars=no,width=650,height=430,resizable=yes');"  target="_self" class="letra_resaltada"> <img src="<%=BASEURL%>/images/botones/iconos/lupa.gif" width="15" height="15"></a>
	        </td>
		  </tr>
        </table>
      </td>
	</tr>
  </table>
</FORM>
<center>
	<img src="<%=BASEURL%>/images/botones/modificar.gif" name="c_buscar" onClick="forma.action = '<%=CONTROLLER%>?estado=Jsp&accion=Update&sw='; forma.submit();" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);"> <img src="<%=BASEURL%>/images/botones/anular.gif" name="c_salir" onClick="forma.action ='<%=CONTROLLER%>?estado=Jsp&accion=Anular'; forma.submit();" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);"> <img src="<%=BASEURL%>/images/botones/salir.gif" name="c_buscar" onClick="parent.close();" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);">
</center>

	<%}else if(mensaje!=null){
			String msg = "";
	msg=request.getParameter("mensaje"); 
	//out.println(msg);%>
<table border="2" align="center">
  <tr>
    <td>
	<table width="100%"  border="1" align="center"  bordercolor="#F7F5F4" bgcolor="#FFFFFF">
      <tr>
        <td width="229" align="center" class="mensajes"><%= msg %></td>
        <td width="29"><img src="<%=BASEURL%>/images/cuadronaranja.JPG"></td>
      </tr>
    </table></td>
  </tr>
</table>
<br>
<center>
	<img src="<%=BASEURL%>/images/botones/aceptar.gif" name="c_buscar" onClick="parent.close();" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);">
</center>
<% } %>
	
    <table width="56%"  border="0" align="center">
      <tr>
        <td width="44%"><input type="text" name="c_user_update" value="<%=usuario.getLogin()%>" class="textbox" style="visibility:hidden"></td>
        <td width="56%">&nbsp;</td>
      </tr>
      <tr>
        <td></td>
        <td>&nbsp;</td>
      </tr>
      <tr>
        <td><input type="text" name="c_creation_user" value="<%=usuario.getLogin()%>" class="textbox" style="visibility:hidden"></td>
        <td>&nbsp;</td>
      </tr>
    </table>

</div>
<%=datos[1]%>
</body>
</html>
