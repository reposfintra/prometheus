<%@page contentType="text/html"%>
<%@page session="true"%>
<%@page errorPage="/error/ErrorPage.jsp"%>
<%@page import="java.util.*"%>
<%@page import="com.tsp.operation.model.*"%>
<%@page import="com.tsp.operation.model.beans.*"%>
<%@page import="com.tsp.util.*"%>
<%@include file="/WEB-INF/InitModel.jsp"%>
<%@taglib uri="http://jsptags.com/tags/navigation/pager" prefix="pg" %>
<%String path = application.getRealPath(model.menuService.getRealUrl(request.getRequestURI()));
  String datos[] = model.menuService.getContenidoMenu(BASEURL,request.getRequestURI(),path);
%>
<html>
<head><title>Listar Paginas Jsp</title> 
 <link href="<%= BASEURL %>/css/estilostsp.css" rel='stylesheet'>
<link href="/css/estilostsp.css" rel="stylesheet" type="text/css">
<script type='text/javascript' src="<%= BASEURL %>/js/reporte.js"></script>
<script type='text/javascript' src="<%= BASEURL %>/js/boton.js"></script>
<body onresize="redimensionar()" onload = 'redimensionar()'>
<div id="capaSuperior" style="position:absolute; width:100%; height:100px; z-index:0; left: 0px; top: 0px;">
 <jsp:include page="/toptsp.jsp?encabezado=Paginas JSP"/>
</div>
<div id="capaCentral" style="position:absolute; width:100%; height:87%; z-index:0; left: 0px; top: 100px; overflow: scroll;">
<% 
    Vector vec = (Vector) request.getAttribute("jsps");
    String style = "simple";
    String position =  "bottom";
    String index =  "center";
    int maxPageItems = 7;
    int maxIndexPages = 5;
	
	if (vec!=null){
       
    }
	else{
		model.jspService.listJsp();
        vec = model.jspService.getJsps();    
    }
	if (vec.size() > 0) {
%>
<br>
<table width="70%" border="2" align="center">
  <tr>
    <td>
	  <table width="100%">
        <tr>
          <td width="373" class="subtitulo1">P&aacute;ginas</td>
          <td width="427" class="barratitulo"><img src="<%=BASEURL%>/images/titulo.gif" width="32" height="20" <%=datos[0]%>></td>
        </tr>
      </table>
	  <table width="100%" border="1" cellpadding="4" cellspacing="1" bordercolor="#999999" bgcolor="#F7F5F4" align="center">
        <tr class="tblTitulo" align="center">
          <td width="162" nowrap >Nombre</td>
          <td width="352" nowrap >Descripcion</td>
		</tr>
    <pg:pager        
		items="<%=vec.size()%>"
        index="<%= index %>"
        maxPageItems="<%= maxPageItems %>"
        maxIndexPages="<%= maxIndexPages %>"
        isOffset="<%= true %>"
        export="offset,currentPageNumber=pageNumber"
        scope="request">
<%
    for (int i = offset.intValue(), l = Math.min(i + maxPageItems, vec.size()); i < l; i++){
        Jsp t = (Jsp) vec.elementAt(i);%>
        <pg:item>
        <tr class="<%=(i % 2 == 0 )?"filagris":"filaazul"%>" onMouseOver='cambiarColorMouse(this)' style="cursor:hand"  title="Lista de Paginas" onClick="window.open('<%=CONTROLLER%>?estado=Jsp&accion=Serch&c_codigo=<%=t.getCodigo()%>&listar=False','Update','status=yes,scrollbars=yes,width=780,height=450,resizable=yes');">
          <td><span class="style7"><%=t.getNombre()%></span></td>
          <td><span class="style7"><%=t.getDescripcion()%></span></td>
		</tr>
      </pg:item>
<%  }%>
        <tr class="pie">
          <td td height="20" colspan="10" nowrap align="center">          <pg:index>
                <jsp:include page="/WEB-INF/jsp/googlesinimg.jsp" flush="true"/>
        </pg:index>
          </td>
        </tr>
    </pg:pager>        
      </table>
    </table>
<%} else {%>
    <table border="2" align="center">
      <tr>
        <td>
	      <table width="100%"  border="1" align="center"  bordercolor="#F7F5F4" bgcolor="#FFFFFF">
            <tr>
              <td width="229" align="center" class="mensajes">No se encontraron p�ginas relacionadas</td>
              <td width="29"><img src="<%=BASEURL%>/images/cuadronaranja.JPG"></td>
            </tr>
          </table>
	    </td>
      </tr>
    </table>
<%}%>
<p>
<table width="70%" align="center">
  <tr>
  	<td>
      <img src="<%=BASEURL%>/images/botones/regresar.gif" name="c_regresar" id="c_regresar" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" onClick="location.href='<%=BASEURL%>/jsp/trafico/permisos/jsp/JspBuscar.jsp'">
    </td>
  </tr>
</table>
</div>
<%=datos[1]%>
</body>
</html>
