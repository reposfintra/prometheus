<%@include file="/WEB-INF/InitModel.jsp"%>
<html>
<head>
<title>Ingresar P&aacute;gina JSP</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link href="/css/estilostsp.css" rel="stylesheet" type="text/css">
<link href="<%= BASEURL %>/css/estilostsp.css" rel='stylesheet'>
</head>

<body>
<br>


<table width="696"  border="2" align="center">
  <tr>
    <td width="811" >
      <table width="100%" border="0" align="center">
        <tr  class="subtitulo">
          <td height="24" colspan="2"><div align="center">Ingresar P&aacute;gina JSP </div></td>
        </tr>
        <tr class="subtitulo1">
          <td colspan="2">Ingresar P&aacute;gina JSP </td>
        </tr>
        <tr>
          <td width="149" class="fila">C&oacute;digo</td>
          <td width="525"  class="ayudaHtmlTexto">Campo donde se digita el c&oacute;digo con el cual se almacenar&aacute; la p&aacute;gina JSP. Este c&oacute;digo puede contener letras y n&uacute;meros. </td>
        </tr>
        <tr>
          <td class="fila">P&aacute;gina</td>
          <td  class="ayudaHtmlTexto">Nombre de la p&aacute;gina almacenar. </td>
        </tr>
        <tr>
          <td class="fila">Ruta</td>
          <td  class="ayudaHtmlTexto">Ruta de la p&aacute;gina almacenar en el sitio. Debe comenzar por el caracter '/'. y debe serciorarse que exista. </td>
        </tr>
        <tr>
          <td class="fila">Descripci&oacute;n</td>
          <td  class="ayudaHtmlTexto">Digite una descripci&oacute;n de la p&aacute;gina JSP.</td>
        </tr>
        <tr>
          <td class="fila"><div align="center">
            <input name="image" type="image" style="cursor:default " src = "<%=BASEURL%>/images/botones/iconos/lupa.gif">
          </div></td>
          <td  class="ayudaHtmlTexto">Bot&oacute;n para abrir la ventana de navegaci&oacute;n del sitio FINTRAVALORES, para buscar la p&aacute;gina en las diferentes carpetas y subcarpetas del sitio. </td>
        </tr>
        <tr>
          <td class="fila"><div align="center">
              <input type="image" src = "<%=BASEURL%>/images/botones/aceptar.gif" style="cursor:default ">
          </div></td>
          <td  class="ayudaHtmlTexto">Bot&oacute;n que valida el la informaci&oacute;n digitada en los campos para ingresar los datos de la p&aacute;gina.</td>
        </tr>
        <tr>
          <td class="fila"><div align="center">
              <input type="image" src = "<%=BASEURL%>/images/botones/cancelar.gif" style="cursor:default ">
          </div></td>
          <td  class="ayudaHtmlTexto"> Bot&oacute;n que cancela todas los operaciones realizadas, y resetea la pagina llev&aacute;ndola a su estado inicial. </td>
        </tr>
        <tr>
          <td class="fila"><div align="center">
              <input type="image" src = "<%=BASEURL%>/images/botones/salir.gif" style="cursor:default ">
          </div></td>
          <td  class="ayudaHtmlTexto">Bot&oacute;n que cierra la ventana. </td>
        </tr>

      </table>
    </td>
  </tr>
</table>
<p>&nbsp;</p>
<p>&nbsp;</p>

<p>&nbsp;</p>
<p>&nbsp; </p>
</body>
</html>
