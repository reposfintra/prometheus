<%@page contentType="text/html"%>
<%@page session="true"%>
<%@page import="java.util.*"%>
<%@page import="com.tsp.operation.model.*"%>
<%@page import="com.tsp.operation.model.beans.*"%>
<%@page import="com.tsp.util.*"%>
<%@include file="/WEB-INF/InitModel.jsp"%>
<%@ taglib uri="http://jakarta.apache.org/taglibs/input-0.90" prefix="input" %>
<%String path = application.getRealPath(model.menuService.getRealUrl(request.getRequestURI()));
  String datos[] = model.menuService.getContenidoMenu(BASEURL,request.getRequestURI(),path);
%>
<html>
<head>
 <title>Ingresar P�gina</title> 
 <script type='text/javascript' src="<%= BASEURL %>/js/validar.js"></script>
<script SRC='<%=BASEURL%>/js/boton.js'></script>
   <link href="<%= BASEURL %>/css/estilostsp.css" rel='stylesheet'>
   <link href="../../css/estilostsp.css" rel="stylesheet" type="text/css">
  <link href="../../../../css/estilostsp.css" rel="stylesheet" type="text/css">
 
<%
	ResourceBundle rb = ResourceBundle.getBundle("com/tsp/util/connectionpool/db");
    String ruta = rb.getString("ruta");	
%>
<script>
function ventanabusqueda(){
  var hija =  window.open('<%=BASEURL%>/jsp/trafico/permisos/jsp/JspBusqueda.jsp?carpeta=slt&ruta=<%=ruta%>&path=<%=""%>','wrptfields','status=yes,scrollbars=yes,width=780,height=650,resizable=yes');
	if ( (document.window != null) && (!hija.opener) )
    hija.opener = document.window;
}
</script>
</head>

<body onresize="redimensionar()" onload = 'redimensionar()'>
<div id="capaSuperior" style="position:absolute; width:100%; height:100px; z-index:0; left: 0px; top: 0px;">
 <jsp:include page="/toptsp.jsp?encabezado=Paginas JSP"/>
</div>
<div id="capaCentral" style="position:absolute; width:100%; height:87%; z-index:0; left: 0px; top: 100px; overflow: scroll;">
<FORM name='forma' id='forma' method='POST' onSubmit="return validarTCamposLlenos();" action='<%=CONTROLLER%>?estado=Jsp&accion=Insert'>
  <table width="448" border="" align="center">
    <tr>
      <td>
	    <table width="100%" border="0"align="center" class="tablaInferior">
          <tr>
            <td colspan='3'>
              <table cellpadding='0' cellspacing='0' width='100%'>
                <tr class="fila">
                  <td width="186" align="left"  class="subtitulo1">Ingresar Pagina</td>
			      <td align="left" width="202" class="barratitulo"><img src="<%=BASEURL%>/images/titulo.gif" width="32" height="20" align="left"><%=datos[0]%></td>
                </tr> 
			  </table>
			</td>
		  </tr>       
          <tr class="fila">
            <td valign="top" width='186'>Codigo</td>
           	<td valign="top" width='226'><input name="c_codigo" type="text" class="textbox" id="c_codigo" onKeyPress="soloAlfa(event)" size="30" maxlength="50"><img src="<%= BASEURL%>/images/botones/iconos/obligatorio.gif" width="10" height="10"></td>
          </tr>
		  <tr class="fila">
            <td valign="top" width='186'>Pagina</td>
	        <td valign="top" width='226'><input name="c_pagina" type="text" class="textbox" id="c_pagina" value="<%if (request.getParameter("nom")!=null)%><%=request.getParameter("nom")%>" size="26" maxlength="50" ><a style="cursor:hand" onClick="ventanabusqueda();"><img src="<%= BASEURL%>/images/botones/iconos/lupa.gif" width="18" height="18"></a> <img src="<%= BASEURL%>/images/botones/iconos/obligatorio.gif" width="10" height="10"></td>
          </tr>
		  <tr class="fila">
            <td valign="top" width='186'>Ruta</td>
	        <td valign="top" width='226'><input name="c_ruta" type="text" class="textbox" id="c_ruta" size="30" maxlength="150"><img src="<%= BASEURL%>/images/botones/iconos/obligatorio.gif" width="10" height="10"></td>
          </tr>
		  <tr class="fila">
            <td valign="top" width='186'>Descripcion</td>
	        <td valign="top" width='226'><input name="c_descripcion" type="text" class="textbox" id="c_descripcion" size="30" maxlength="50" ><img src="<%= BASEURL%>/images/botones/iconos/obligatorio.gif" width="10" height="10"></td>
          </tr> 
	    </table>
	  </td>
    </tr>
  </table>  
</FORM>
<center>
	<img src="<%=BASEURL%>/images/botones/aceptar.gif" name="c_buscar" onClick="return TCamposLlenos();" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);">   <img src="<%=BASEURL%>/images/botones/cancelar.gif" name="c_buscar" onClick="forma.reset();" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);"> <img src="<%=BASEURL%>/images/botones/salir.gif" name="c_salir" onClick="parent.close();" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);"></center>

 <%
    String mensaje = request.getParameter("mensaje");
	if ( mensaje != null && !mensaje.equals("") ) {
%>
<p>
<table border="2" align="center">
  <tr>
    <td>
	<table width="100%"  border="1" align="center"  bordercolor="#F7F5F4" bgcolor="#FFFFFF">
      <tr>
        <td width="229" align="center" class="mensajes"><%= mensaje %></td>
        <td width="29"><img src="<%=BASEURL%>/images/cuadronaranja.JPG"></td>
      </tr>
    </table></td>
  </tr>
</table>
<% } %>
</div>
<%=datos[1]%>
</body>
</html>
