<%@include file="/WEB-INF/InitModel.jsp"%>
<html>
<head>
<title>Buscar P&aacute;gina JSP</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link href="/css/estilostsp.css" rel="stylesheet" type="text/css">
<link href="<%= BASEURL %>/css/estilostsp.css" rel='stylesheet'>
</head>

<body>
<br>


<table width="696"  border="2" align="center">
  <tr>
    <td width="811" >
      <table width="100%" border="0" align="center">
        <tr  class="subtitulo">
          <td height="24" colspan="2"><div align="center">Ingresar P&aacute;gina JSP </div></td>
        </tr>
        <tr class="subtitulo1">
          <td colspan="2">Ingresar P&aacute;gina JSP </td>
        </tr>
        <tr>
          <td width="149" class="fila">C&oacute;digo</td>
          <td width="525"  class="ayudaHtmlTexto">Campo donde se digita el c&oacute;digo con el cual buscar&aacute; la p&aacute;gina JSP. </td>
        </tr>
        <tr>
          <td class="fila">Nombre</td>
          <td  class="ayudaHtmlTexto">Nombre de la p&aacute;gina buscar. </td>
        </tr>
        <tr>
          <td class="fila"><div align="center">
              <input type="image" src = "<%=BASEURL%>/images/botones/buscar.gif" style="cursor:default ">
          </div></td>
          <td  class="ayudaHtmlTexto">Bot&oacute;n que valida el la informaci&oacute;n digitada en los campos para iniciar la b&uacute;squeda de la informaci&oacute;n. </td>
        </tr>
        <tr>
          <td class="fila"><div align="center">
              <input type="image" src = "<%=BASEURL%>/images/botones/detalles.gif" style="cursor:default ">
          </div></td>
          <td  class="ayudaHtmlTexto"> Bot&oacute;n que muestra todos los registros almacenados.. </td>
        </tr>
        <tr>
          <td class="fila"><div align="center">
              <input type="image" src = "<%=BASEURL%>/images/botones/salir.gif" style="cursor:default ">
          </div></td>
          <td  class="ayudaHtmlTexto">Bot&oacute;n que cierra la ventana. </td>
        </tr>

      </table>
    </td>
  </tr>
</table>
<p>&nbsp;</p>
<p>&nbsp;</p>

<p>&nbsp;</p>
<p>&nbsp; </p>
</body>
</html>
