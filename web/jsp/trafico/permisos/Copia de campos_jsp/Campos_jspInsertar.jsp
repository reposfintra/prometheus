<%@page contentType="text/html"%>
<%@page session="true"%>
<%@page import="java.util.*"%>
<%@page import="com.tsp.operation.model.*"%>
<%@page import="com.tsp.operation.model.beans.*"%>
<%@page import="com.tsp.util.*"%>
<%@include file="/WEB-INF/InitModel.jsp"%>
<%
	String campo = (request.getParameter("campo")==null)? "" : request.getParameter("campo");
	String pag = (request.getParameter("pag")==null)? "" : request.getParameter("pag");	
	//out.println("perfil: " + perfil);
        //out.println("p�gina: " + pag);
        model.jspService.serchJsp(pag);
        Jsp p = model.jspService.getJsp();
        String pagina = ( p!= null ) ? p.getNombre() : "";
        String cod_pag = ( p!= null ) ? p.getCodigo() : "";
%>
<html>
<head>
<title>Insertar Campos</title> 
<script type='text/javascript' src="<%= BASEURL %>/js/validar.js"></script>
<script SRC='<%=BASEURL%>/js/boton.js'></script>
   <link href="<%= BASEURL %>/css/estilostsp.css" rel='stylesheet'>
   <link href="../../../../css/estilostsp.css" rel="stylesheet" type="text/css">
</head>
<script type='text/javascript' src="<%= BASEURL %>/js/validar.js"></script>
<body onresize="redimensionar()" onload = 'redimensionar()'>
<%Usuario usuario = (Usuario) session.getAttribute("Usuario");%>
<%String user = usuario.getLogin();%>
<%Vector pvpag = model.perfil_vistaService.consultPerfil_vistas(user,"Campos_jspInsertar.jsp");%>
<div id="capaSuperior" style="position:absolute; width:100%; height:100px; z-index:0; left: 0px; top: 0px;">
 <jsp:include page="/toptsp.jsp?encabezado=Campos JSP"/>
</div>
<div id="capaCentral" style="position:absolute; width:100%; height:87%; z-index:0; left: 0px; top: 100px; overflow: scroll;">
<%
    String mensaje = (String) request.getAttribute("mensaje");
	if (mensaje == null) {
%>
<FORM name='forma' id='forma' method='POST'  action='<%=CONTROLLER%>?estado=Campos_jsp&accion=Insert'>
 <table width="460" border="2" align="center">
      <tr>
        <td width="3350">
			<table width="100%" border="0"align="center" >
            	<tr>
            		<td colspan='3'>
              			<table cellpadding='0' cellspacing='0' width='100%'>
                			<tr class="fila">
                  				<td width="186" align="left"  class="subtitulo1">Ingresar Campo</td>
                  				<td align="left" width="202" class="barratitulo"><img src="<%=BASEURL%>//images/titulo.gif" width="32" height="20">
              				</tr> 
			  			</table>
			  		</td>
				</tr>
        		<tr class="fila">
            		<td width="112">Pagina</td>
   				  <td width="336">
<input value="<%= pagina %>" name="textfield" type="text" class="textbox" size="50" readonly>
<img src="<%= BASEURL%>/images/botones/iconos/buscar.gif"  name="imgsalir" align="absmiddle" style="cursor:hand " onClick="window.open('<%=BASEURL%>/jsp/trafico/permisos/perfil_vista/perfil_vistaBuscarPag.jsp?perfil=&campo=' + forma.c_campo.value + '&estado=CamposJsp&accion=BuscarPagIngresar','verConsulta','status=no,scrollbars=no,width=480,height=430,resizable=yes');"> <img src="<%= BASEURL%>/images/botones/iconos/obligatorio.gif" width="10" height="10">	<input name="c_pagina" type="hidden" id="c_pagina2" value="<%= cod_pag %>"></td>
    			</tr>
				<tr class="fila">    
					<td>Campo</td>
           		  <td><input <%=model.perfil_vistaService.property("","c_campo",pvpag,"")%> name="c_campo" type="text" class="textbox" id="c_campo" value="<%= campo %>" maxlength="20">
       		      <img src="<%= BASEURL%>/images/botones/iconos/obligatorio.gif" width="10" height="10"></td>
        		</tr>        
        		<tr class="pie">
            	  <td align='center' colspan="2" >
            	  </td>
        		</tr>
		  </table>
		</td>
	</tr>
</table>
</FORM>
<center>
	<img  <%=model.perfil_vistaService.property("src=\""+BASEURL+"/images/botones/aceptar.gif\"","c_aceptar",pvpag, "onClick=\"if( TCamposLlenos() ) forma.submit();\" onMouseOut=\"botonOut(this);\" onMouseOver=\"botonOver(this);\"")%>name="c_aceptar">  <img  <%=model.perfil_vistaService.property("src=\""+BASEURL+"/images/botones/cancelar.gif\"","c_buscar",pvpag, "onClick=\"forma.reset();\" onMouseOut=\"botonOut(this);\" onMouseOver=\"botonOver(this);\"")%>name="c_buscar"> <img  <%=model.perfil_vistaService.property("src=\""+BASEURL+"/images/botones/salir.gif\"","c_salir",pvpag, "onClick=\"window.close();\" onMouseOut=\"botonOut(this);\" onMouseOver=\"botonOver(this);\"")%>name="c_salir">    	 
</center>
<%}else  if(mensaje!=null){
           String msg = "";
	msg=mensaje; 
	%>
<table border="2" align="center">
  <tr>
    <td>
	<table width="100%"  border="1" align="center"  bordercolor="#F7F5F4" bgcolor="#FFFFFF">
      <tr>
        <td width="229" align="center" class="mensajes"><%= msg %></td>
        <td width="29"><img src="<%=BASEURL%>/images/cuadronaranja.JPG"></td>
      </tr>
    </table></td>
  </tr>
</table>
<br>
<p>
<table width='284' border="0" align="center" cellpadding="0" cellspacing="0">
  <tr style="cursor:hand">
    <td align="left">
	  <img  <%=model.perfil_vistaService.property("src=\""+BASEURL+"/images/botones/regresar.gif\"","c_aceptar",pvpag, "Onclick = \" window.location = ' " + CONTROLLER + "?estado=Menu&accion=Cargar&carpeta=/jsp/trafico/permisos/campos_jsp&pagina=Campos_jspInsertar.jsp&marco=no';\" onMouseOut=\"botonOut(this);\" onMouseOver=\"botonOver(this);\"")%>name="c_aceptar">
    </td>
  </tr>
</table>
<br>
<% } %>
</div>
</body>
</html>
