<%@page contentType="text/html"%>
<%@page session="true"%>
<%@page import="java.util.*"%>
<%@page import="com.tsp.operation.model.*"%>
<%@page import="com.tsp.operation.model.beans.*"%>
<%@page import="com.tsp.util.*"%>
<%@include file="/WEB-INF/InitModel.jsp"%>
<html>
<head>
<title>Modificar Tipo De Ubicaci&oacute;n</title>
<script type='text/javascript' src="<%= BASEURL %>/js/validar.js"></script>
<script SRC='<%=BASEURL%>/js/boton.js'></script>
   <link href="<%= BASEURL %>/css/estilostsp.css" rel='stylesheet'>
   <link href="../../css/estilostsp.css" rel="stylesheet" type="text/css">
   <link href="../../../css/estilostsp.css" rel="stylesheet" type="text/css">
</head>
<script type='text/javascript' src="<%= BASEURL %>/js/validar.js"></script>
<body <%if(request.getParameter("reload")!=null){%>onLoad="window.opener.location.reload();"<%}%>>

<%

    Usuario usuario = (Usuario) session.getAttribute("Usuario");
    String user = usuario.getLogin();  
    Vector pvpag = model.perfil_vistaService.consultPerfil_vistas(user,"Campos_jspModificar.jsp");
    campos_jsp t = (campos_jsp) request.getAttribute("campos_jsp");    
    String mensaje = (String) request.getParameter("mensaje");
    Vector vec = new Vector();
	if (mensaje == null){
	%>
<FORM name='forma' id='forma' method='POST' onSubmit="return validarTCamposLlenos();" action='<%=CONTROLLER%>?estado=Campos_jsp&accion=Anular'>
<table width="350" border="2" align="center">
  <tr>
    <td width="3350">
 	  <table border="0"align="center">
        <tr>
          <td colspan='3'>
            <table cellpadding='0' cellspacing='0' width='100%'>
              <tr class="fila">
              	<td width="186" align="left"  class="subtitulo1">Anular Campo</td>
              	<td align="left" width="202" class="barratitulo"><img src="<%=BASEURL%>//images/titulo.gif" width="32" height="20">
              </tr> 
			</table>
		  </td>
		</tr>
        <tr class="fila">
          <td width="119">Pagina</td>
          <td width="227"><input name="c_pagina" type="text" class="textbox" id="%c_pagina"  onKeyPress="soloAlfa(event)" value="<%=t.getPagina()%>" maxlength="6" readonly=""></td>
		</tr>
        <tr class="fila">
          <td>Campo</td>
          <td><input name="c_campo" type="text" class="textbox" id="c_campo" value="<%=t.getCampo()%>" maxlength="45"></td>
        </tr>        
   		<tr class="pie">
		  <td align='center' colspan="3" >
		  </td>
        </tr>        
      </table>
	</td>
  </tr>
</table>
</FORM>
<center>
	<img src="<%=BASEURL%>/images/botones/eliminar.gif" onClick="forma.submit();" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" name="%i_eliminar"> <img src="<%=BASEURL%>/images/botones/salir.gif" name="%i_cancelar" onClick="parent.close();" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);">    	 
</center>

	<%} else if(mensaje!=null){
			String msg = mensaje;
	//out.println(msg);%>
<table border="2" align="center">
  <tr>
    <td>
	<table width="100%"  border="1" align="center"  bordercolor="#F7F5F4" bgcolor="#FFFFFF">
      <tr>
        <td width="229" align="center" class="mensajes"><%= msg %></td>
        <td width="29"><img src="<%=BASEURL%>/images/cuadronaranja.JPG"></td>
      </tr>
    </table></td>
  </tr>
</table>
<br>
<p>
<table width='290' border="0" align="center" cellpadding="0" cellspacing="0">
  <tr style="cursor:hand">
    <td align="left">
    	 <img src="<%=BASEURL%>/images/botones/aceptar.gif" name="%i_aceptar" onClick="parent.close();" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);">
	</td>
  </tr>
</table>
<br>
<% } %>

    <table width="56%"  border="0" align="center">
      <tr>
        <td width="44%"><input type="text" name="user_update" value="<%=usuario.getLogin()%>" class="textbox" style="visibility:hidden"></td>
        <td width="56%">&nbsp;</td>
      </tr>
      <tr>
        <td>&nbsp;</td>
        <td>&nbsp;</td>
      </tr>
      <tr>
        <td><input type="text" name="creation_user" value="<%=usuario.getLogin()%>" class="textbox" style="visibility:hidden"></td>
        <td>&nbsp;</td>
      </tr>
    </table>
</body>
</html>
