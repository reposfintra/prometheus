<%@page contentType="text/html"%>
<%@page session="true"%>
<%@page import="java.util.*"%>
<%@page import="com.tsp.operation.model.*"%>
<%@page import="com.tsp.operation.model.beans.*"%>
<%@page import="com.tsp.util.*"%>
<%@include file="/WEB-INF/InitModel.jsp"%>
<%@ taglib uri="http://jakarta.apache.org/taglibs/input-0.90" prefix="input" %>
<%String path = application.getRealPath(model.menuService.getRealUrl(request.getRequestURI()));
  String datos[] = model.menuService.getContenidoMenu(BASEURL,request.getRequestURI(),path);
%>
<%
	String perfil = (request.getParameter("perfil")==null)? "" : request.getParameter("perfil");
	String pag = (request.getParameter("pag")==null)? "" : request.getParameter("pag");	
	//out.println("perfil: " + perfil);
        //out.println("página: " + pag);
        model.jspService.serchJsp(pag);
        Jsp p = model.jspService.getJsp();
        String pagina = ( p!= null ) ? p.getNombre() : "";
        String cod_pag = ( p!= null ) ? p.getCodigo() : "";
   	    
%>
<html>
<head>
<title>Consultar Perfil-Vista</title>
<script SRC='<%=BASEURL%>/js/boton.js'></script>

<%
        String url_1 = CONTROLLER + "?estado=Perfil_vista&accion=Consultar&consulta=S";
        String url_2 = CONTROLLER + "?estado=Pagina&accion=Perfil_vista";
%>
<script>
        function enviarFormulario(){
                var perfil = forma.c_perfil.options(forma.c_perfil.selectedIndex).value;
                var pagina = forma.c_pagina.value;
                if( perfil.length>0 && pagina.length>0 ){
                        forma.action = '<%= url_2%>';
                        forma.submit();
                }
                else if( perfil.length==0 && pagina.length==0 ){
                        alert('No se puede procesar la información. Debe escoger al menos un criterio de consulta.');
                }
                else{
                        forma.action = '<%= url_1%>';
                        forma.consulta.value = 'S';
                        forma.submit();
                }
        }
</script>
<link href="../../../../css/estilostsp.css" rel="stylesheet" type="text/css">
<link href="<%= BASEURL %>/css/estilostsp.css" rel="stylesheet" type="text/css">
<script type='text/javascript' src="<%= BASEURL %>/js/validar.js"></script>
</head>
<body onresize="redimensionar()" onload = 'redimensionar()'>
<div id="capaSuperior" style="position:absolute; width:100%; height:100px; z-index:0; left: 0px; top: 0px;">
 <jsp:include page="/toptsp.jsp?encabezado=Perfil Vista"/>
</div>
<div id="capaCentral" style="position:absolute; width:100%; height:87%; z-index:0; left: 0px; top: 100px; overflow: scroll;">
<FORM name='forma' id='forma' method='POST' action=""  onSubmit="">
  <table width="470" border="2" align="center">
    <tr>
      <td><table width="100%" class="tablaInferior">
        <tr>
          <td width="50%" class="subtitulo1">Informaci&oacute;n</td>
          <td width="50%" class="barratitulo"><img src="<%=BASEURL%>/images/titulo.gif" width="32" height="20" align="left"><%=datos[0]%></td>
        </tr>
      </table>      
        <table width="100%" border="0" align="center" class="tablaInferior">
          <tr class="fila">
            <td width="112">Perfil</td>
            <td width="336" valign="middle"><select name="c_perfil" id="c_perfil" class="textbox" >
                <%model.perfilService.listarPerfilesxPropietario();
		Vector vec = model.perfilService.getVPerfil();
		if (vec != null){%>
                <option value="">Seleccione</option>
                <%for(int i=0; i < vec.size();i++){  
			Perfil pv = (Perfil) vec.elementAt(i);%>
                <option value="<%=pv.getId()%>" <%= (pv.getId().matches(perfil))? "selected" : "" %>><%=pv.getNombre()%></option>
                <%}
	   		}%>
            </select>
              <span class="barratitulo">
              <input name="consulta" type="hidden" id="consulta2">
            </span></td>
          </tr>
          <tr class="fila">
            <td width="112">P&aacute;gina</td>
            <td width="336" valign="middle"><input value="<%= pagina %>" name="textfield" type="text" class="textbox" size="50" readonly>
            <img src="<%= BASEURL%>/images/botones/iconos/buscar.gif"  name="imgsalir" align="absmiddle" style="cursor:hand " onClick="window.open('<%=BASEURL%>/jsp/trafico/permisos/perfil_vista/perfil_vistaBuscarPag.jsp?perfil=' + forma.c_perfil.options[forma.c_perfil.selectedIndex].value + '&estado=Perfil_vista&accion=BuscarPagConsulta','verConsulta','status=no,scrollbars=no,width=480,height=430,resizable=yes');">
            <input name="c_pagina" type="hidden" id="c_pagina" value="<%= cod_pag %>"></td>
          </tr>
        </table></td>
    </tr>
  </table>
  <p align="center"><img src="<%= BASEURL%>/images/botones/aceptar.gif"  name="imgaceptar" onClick="enviarFormulario();" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" style="cursor:hand ">&nbsp; <img src="<%= BASEURL%>/images/botones/cancelar.gif"  name="imgcancelar"  onMouseOver="botonOver(this);" onClick="forma.reset();" onMouseOut="botonOut(this);"  style="cursor:hand ">&nbsp; <img src="<%= BASEURL%>/images/botones/salir.gif"  name="imgsalir"  onMouseOver="botonOver(this);" onClick="parent.close();" onMouseOut="botonOut(this);"  style="cursor:hand "></p>
</form>
<%if(request.getAttribute("mensaje")!=null){%>
<table border="2" align="center">
  <tr>
    <td><table width="410" height="73" border="1" align="center"  bordercolor="#F7F5F4" bgcolor="#FFFFFF">
        <tr>
          <td width="282" align="center" class="mensajes"><%=request.getAttribute("mensaje")%></td>
          <td width="28" background="<%=BASEURL%>/images/cuadronaranja.JPG">&nbsp;</td>
          <td width="78">&nbsp;</td>
        </tr>
    </table></td>
  </tr>
</table>
<%}%>
</div>
<%=datos[1]%>
</body>
</html>
