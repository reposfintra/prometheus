<%@page contentType="text/html"%>
<%@page session="true"%>
<%@page import="java.util.*"%>
<%@page import="com.tsp.operation.model.*"%>
<%@page import="com.tsp.operation.model.beans.*"%>
<%@page import="com.tsp.util.*"%>
<%@include file="/WEB-INF/InitModel.jsp"%>
<html>
    <head>
        <title>Insertar Tipo De Contrato</title>
        <link href="../../../../css/estilostsp.css" rel="stylesheet" type="text/css">
        <link href="<%= BASEURL %>/css/estilostsp.css" rel="stylesheet" type="text/css">
        <script SRC='<%=BASEURL%>/js/boton.js'></script>
        <script type='text/javascript' src="<%= BASEURL %>/js/validar.js"></script>
    </head>

<body onresize="redimensionar()" onload = 'redimensionar()'>
<div id="capaSuperior" style="position:absolute; width:100%; height:100px; z-index:0; left: 0px; top: 0px;">
 <jsp:include page="/toptsp.jsp?encabezado=Perfil Vista"/>
</div>
<div id="capaCentral" style="position:absolute; width:100%; height:87%; z-index:0; left: 0px; top: 100px; overflow: scroll;">

<%
	String pagina = request.getParameter("c_pagina"); 
	String perfil = request.getParameter("c_perfil");
	Vector campos = model.camposjsp.listarCampos_jsp(pagina);
	
	if( campos.size()>0){
%>
        <FORM name='forma' id='forma' method='POST' onSubmit="return validarTCamposLlenos();" action=''>
            <input name="c_pagina" type="hidden" id="c_pagina" value="<%=pagina%>">
            <input name="c_perfil" type="hidden" id="c_perfil" value="<%=perfil%>">
	  
            <table width="694" border="2" align="center">
            <tr>
                <td><table width="100%" class="tablaInferior">
                    <tr>
                        <td width="50%" class="subtitulo1">Informaci&oacute;n</td>
                        <td width="50%" class="barratitulo"><img src="<%=BASEURL%>/images/titulo.gif" width="32" height="20"> </td>
                    </tr>
                </table>
                <table width="100%" border="0" align="center" class="tablaInferior">
<% 
            Perfil_vista pv;
            for (int i = 0; i < campos.size(); i++){
                    campos_jsp campo = (campos_jsp) campos.elementAt(i);
                    pv =(Perfil_vista) model.perfil_vistaService.buscarPerfil_vista(perfil,pagina,campo.getCampo());
                    String tipo_campo = "";
                    String texto_campo = "";
                    if( campo.getTipo_campo().matches("c") ){
                        tipo_campo = "Campo texto";
                        texto_campo = "Solo-lectura";
                    } else if( campo.getTipo_campo().matches("i") ) {                 
                        tipo_campo = "Imagen";
                        texto_campo = "Deshabilitado";
                    } else if( campo.getTipo_campo().matches("s") ) {                 
                        tipo_campo = "Combo Box";
                        texto_campo = "Deshabilitado";
                    } else if( campo.getTipo_campo().matches("l") ) {                 
            			tipo_campo = "Hiperv&iacute;nculo";
        			} else {                 
                        tipo_campo = "Campo texto";
                        texto_campo = "Solo-lectura";
                    }
%>
                <tr class="fila">
                    <td width="170"><%=campo.getCampo()%></td>
                    <td width="136"><%= campo.getTipo_campo() %></td>
                    <td width="144"><% if( !campo.getTipo_campo().matches("imagen") ) { %>Oculto:
                        <input name="<%=(campo.getCampo()+"V")%>" type="checkbox" class="fila" id="<%=(campo.getCampo()+"V")%>" value="checked" <% if(pv.getVisible()!=null){ out.println(pv.getVisible());}%>>
						<% } else { %>
						&nbsp;
						<% } %>
                    </td>
                    <td width="214"><%= texto_campo %>
                        <input name="<%=(campo.getCampo()+"E")%>" type="checkbox" class="fila" id="<%=(campo.getCampo()+"E")%>" value="checked" <% if(pv.getEditable()!=null){ out.println(pv.getEditable());} %>>
                    </td>
                </tr>
          <%}%>
                </table>      </td>
            </tr>
            </table>
            <p align="center"><img src="<%= BASEURL%>/images/botones/aceptar.gif"  name="imgaceptar" onClick="forma.action='<%=CONTROLLER%>?estado=Perfil_vista&accion=Insert';forma.submit();" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);">&nbsp; <img src="<%= BASEURL%>/images/botones/cancelar.gif"  name="imgcancelar"  onMouseOver="botonOver(this);" onClick="forma.reset();" onMouseOut="botonOut(this);" >&nbsp; <img src="<%= BASEURL%>/images/botones/salir.gif"  name="imgsalir"  onMouseOver="botonOver(this);" onClick="parent.close();" onMouseOut="botonOut(this);" ></p>
        </FORM>
<%
	}
	else{
%>
        <table border="2" align="center">
            <tr>
                <td><table width="410" height="73" border="1" align="center"  bordercolor="#F7F5F4" bgcolor="#FFFFFF">
                    <tr>
                        <td width="282" align="center" class="mensajes">Su b&uacute;squeda no arroj&oacute; resultados!</td>
                        <td width="28" background="<%=BASEURL%>/images/cuadronaranja.JPG">&nbsp;</td>
                        <td width="78">&nbsp;</td>
                    </tr>
                </table></td>
            </tr>
        </table>
        <br>
        <table width="420" border="0" cellspacing="0" cellpadding="0" align="center">
            <tr align="left">
                <td><img src="<%=BASEURL%>/images/botones/regresar.gif" name="c_regresar" id="c_regresar" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" onClick="parent.history.back();"> 
            </tr>
        </table>
        <p>
<%
	}
%>
        </p>
	</div>	
    </body>
</html>
