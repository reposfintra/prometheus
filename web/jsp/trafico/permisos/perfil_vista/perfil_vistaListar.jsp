<%@page contentType="text/html"%>
<%@page session="true"%>
<%@page errorPage="/error/ErrorPage.jsp"%>
<%@page import="java.util.*"%>
<%@page import="com.tsp.operation.model.*"%>
<%@page import="com.tsp.operation.model.beans.*"%>
<%@page import="com.tsp.util.*"%>
<%@include file="/WEB-INF/InitModel.jsp"%>
<%@taglib uri="http://jsptags.com/tags/navigation/pager" prefix="pg" %>
<%String path = application.getRealPath(model.menuService.getRealUrl(request.getRequestURI()));
  String datos[] = model.menuService.getContenidoMenu(BASEURL,request.getRequestURI(),path);
%>
<html>
<head><title>Listar Perfil Vista Usuario</title>
<script type='text/javascript' src="<%= BASEURL %>/js/reporte.js"></script>
<script SRC='<%=BASEURL%>/js/boton.js'></script>
<link href="../../../../css/estilostsp.css" rel="stylesheet" type="text/css">
<link href="<%= BASEURL %>/css/estilostsp.css" rel="stylesheet" type="text/css">

</head>
<body onresize="redimensionar()" onload = 'redimensionar()'>
<div id="capaSuperior" style="position:absolute; width:100%; height:100px; z-index:0; left: 0px; top: 0px;">
 <jsp:include page="/toptsp.jsp?encabezado=Perfil Vista"/>
</div>
<div id="capaCentral" style="position:absolute; width:100%; height:87%; z-index:0; left: 0px; top: 100px; overflow: scroll;">
<%
        Usuario usuario = (Usuario) session.getAttribute("Usuario");  
	String login = usuario.getLogin();
        System.out.println("Login"+login);
	
	//En este espacio se hace la consulta del perfil de vista para esta pagina
	Vector pvpag= model.perfil_vistaService.consultPerfil_vistas(login,"PvuListar.jsp");
	System.out.println("vector"+pvpag.size());
	//************************************************************************
%>
<% 
        Vector vec = new Vector();
        
        String perfil = (String) session.getAttribute("perfil");
        String vista = (String) session.getAttribute("vista");        

        if( perfil.length()>0 ) {                
                vec = model.perfil_vistaService.consultarPerfil(perfil);
        }
        else{
                vec = model.perfil_vistaService.consultarVista(vista);
        }
        String style = "simple";
        String position =  "bottom";
        String index =  "center";
        int maxPageItems = 7;
        int maxIndexPages = 5;
        
        if( vec.size()>0 ){
%>
<br>

<table width="540" height="66" border="2" align="center">
  <tr>
    <td><table width="100%" class="tablaInferior">
      <tr>
        <td width="50%" class="subtitulo1">Informaci&oacute;n</td>
        <td width="50%" class="barratitulo"><img src="<%=BASEURL%>/images/titulo.gif" width="32" height="20" align="left"><%=datos[0]%></td>
      </tr>
    </table>
      <table width="100%" border="1" align="center" cellpadding="4" cellspacing="1" bordercolor="#999999" bgcolor="#F7F5F4">
        <tr  class="tblTitulo" align="center">
          <td width="188" nowrap >Perfil</td>
          <td width="317" nowrap >Vista</td>
        </tr>
        <pg:pager        items="<%=vec.size()%>"
        index="<%= index %>"
        maxPageItems="<%= maxPageItems %>"
        maxIndexPages="<%= maxIndexPages %>"
        isOffset="<%= true %>"
        export="offset,currentPageNumber=pageNumber"
        scope="request">
        <%
    for (int i = offset.intValue(), l = Math.min(i + maxPageItems, vec.size()); i < l; i++){
        Perfil_vista t = (Perfil_vista) vec.elementAt(i);%>
        <pg:item>
        <tr class="<%=(i % 2 == 0 )?"filagris":"filaazul"%>" onMouseOver='cambiarColorMouse(this)'   style="cursor:hand" title="Modificar Tipo de Sello..."
         onClick="window.open('<%=CONTROLLER%>?estado=Pagina&accion=Perfil_vista&c_perfil=<%= t.getPerfil()%>&c_pagina=<%= t.getPagina()%>','verConsulta','status=no,scrollbars=no,width=650,height=430,resizable=yes');">
          <td  class="bordereporte"><%=t.getPerfil()%></td>
          <% 
        model.jspService.serchJsp(t.getPagina()); 
        Jsp pag = model.jspService.getJsp();        
%>
          <td  class="bordereporte"><%= pag.getRuta() + "" + pag.getNombre() %></td>
        </tr>
        </pg:item>
        <%  }%>
        <tr class="bordereporte">
          <td td height="20" colspan="10" nowrap align="center"> <pg:index>
            <jsp:include page="/WEB-INF/jsp/googlesinimg.jsp" flush="true"/>      
    </pg:index> </td>
        </tr>
        </pg:pager>
      </table>    </td>
  </tr>
</table>
<%
        }
        else{
%>
</p>
  <table border="2" align="center">
    <tr>
      <td><table width="410" height="73" border="1" align="center"  bordercolor="#F7F5F4" bgcolor="#FFFFFF">
          <tr>
            <td width="282" align="center" class="mensajes">Su b&uacute;squeda no arroj&oacute; resultados!</td>
            <td width="28" background="<%=BASEURL%>/images/cuadronaranja.JPG">&nbsp;</td>
            <td width="78">&nbsp;</td>
          </tr>
      </table></td>
    </tr>
  </table>
  <p>&nbsp; </p>
  <p>
    <%
        }
%>
</p>
  <table width="544" border="0" cellspacing="0" cellpadding="0" align="center">
    <tr align="left">
      <td><img src="<%=BASEURL%>/images/botones/regresar.gif" name="c_regresar" id="c_regresar" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" onClick="parent.history.back();">     </tr>
  </table>
  <p>&nbsp;  </p>
</div>
<%=datos[1]%>
</body>
</html>
