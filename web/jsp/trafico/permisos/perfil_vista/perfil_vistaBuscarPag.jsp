<%@page contentType="text/html"%>
<%@page session="true"%>
<%@page import="java.util.*"%>
<%@page import="com.tsp.operation.model.*"%>
<%@page import="com.tsp.operation.model.beans.*"%>
<%@page import="com.tsp.util.*"%>
<%@include file="/WEB-INF/InitModel.jsp"%>
<%@ taglib uri="http://jakarta.apache.org/taglibs/input-0.90" prefix="input" %>
<%
	String perfil = request.getParameter("perfil");
	String campo = request.getParameter("campo");
	String accion = request.getParameter("accion");
	String estado = request.getParameter("estado");
%>
<html>
<head>
<title></title>
<link href="../../../../css/estilostsp.css" rel="stylesheet" type="text/css">
<link href="<%= BASEURL %>/css/estilostsp.css" rel="stylesheet" type="text/css">
<script SRC='<%=BASEURL%>/js/boton.js'></script>
<script type='text/javascript' src="<%= BASEURL %>/js/validar.js"></script>
<script>
<%= model.jspService.GenerarJSCampos() %>
<%= model.jspService.GenerarJSCamposPaths()%>
var SeparadorJS = '~';

        function loadCombo(datos, cmb){
            cmb.length = 0;
            for (i=0;i<datos.length;i++){
                var dat = datos[i].split(SeparadorJS);
                addOption(cmb, dat[0], dat[1]);
            }
			if(cmb.length > 0)
				cmb[0].selected = true;
        }

        function order(cmb){
           for (i=0; i<cmb.length;i++)
             for (j=i+1; j<cmb.length;j++)
                if (cmb[i].text > cmb[j].text){
                    var temp = document.createElement("OPTION");
                    temp.value = cmb[i].value;
                    temp.text  = cmb[i].text;
                    cmb[i].value = cmb[j].value;
                    cmb[i].text  = cmb[j].text;
                    cmb[j].value = temp.value;
                    cmb[j].text  = temp.text;
                }
        }     
   
        function addOption(Comb,valor,texto){
           if(valor!='' && texto!=''){
                var Ele = document.createElement("OPTION");
                Ele.value=valor;
                Ele.text=texto;
                Comb.add(Ele);
          }
        }        
		
		function seleccionar(){
			var s = forma.s_jsps.options[forma.s_jsps.selectedIndex].text;
			var c = forma.s_jsps.options[forma.s_jsps.selectedIndex].value;
			var dat = s.split(' ');
			forma.s_url[forma.s_jsps.selectedIndex].selected = true;
			s = dat[0];
			forma.c_pagina.value = s;		
			forma.cod_pagina.value = c;
		}
		
		function seleccionar2(){
			forma.s_jsps[forma.s_url.selectedIndex].selected = true;
			var s = forma.s_jsps.options[forma.s_url.selectedIndex].text;
			var c = forma.s_jsps.options[forma.s_url.selectedIndex].value;
			var dat = s.split(' ');			
			s = dat[0];
			forma.c_pagina.value = s;		
			forma.cod_pagina.value = c;
		}
		
		function searchCombo(data, cmb, cmb2){
			data = data.toLowerCase();
            for (i=0;i<cmb.length;i++){
                var cod = cmb[i].value;
				var txt = cmb[i].text.toLowerCase( );
                if( txt.indexOf(data)==0 ){
					cmb[i].selected = true;
					cmb2[i].selected = true;
					break;
				}
            }
        }
		
		function enviar(){
		    var codpag = forma.s_jsps.options[forma.s_jsps.selectedIndex].value;
			parent.opener.location.href = '<%= CONTROLLER %>?estado=<%=estado %>&accion=<%=accion %>&perfil=<%= perfil %>&pag=' + codpag + '&campo=<%= campo %>';
			parent.close();
		}
		
		function paginaSelec(){
			cmb = forma.s_jsps;
			for (i=0;i<cmb.length;i++){
                if( cmb[i].selected == true ){
					return true;
				}
            }
			
			return false;
		}
		function validar(){
			if( !paginaSelec() ){
				alert('Debe seleccionar una p�gina.');
				return false;				
			} else {
				return true;
			}
		}
</script>
</head>
<body onLoad="loadCombo(CamposJSP, forma.s_jsps);loadCombo(CamposJSPURL, forma.s_url);">
<FORM name='forma' id='forma' method='POST' action=""  onSubmit="">
  <table width="484" border="2" align="center">
    <tr>
      <td height="93"><table width="100%" class="tablaInferior">
        <tr>
          <td width="50%" class="subtitulo1">Informaci&oacute;n</td>
          <td width="50%" class="barratitulo"><img src="<%=BASEURL%>/images/titulo.gif" width="32" height="20"> </td>
        </tr>
      </table>
        <table width="100%" border="0" align="center" class="tablaInferior">
          <tr class="fila">
            <td width="74">P&aacute;gina</td>
            <td width="324" valign="middle"><input name="c_pagina" type="text" class="textbox" id="c_pagina"size="50" onKeyUp="searchCombo(this.value, forma.s_jsps, forma.s_url);">              
            <input name="cod_pagina" type="hidden" id="cod_pagina"><!-- "%c_cod_pagina" -->
            </td>
          </tr>
          <tr class="fila">
            <td colspan="2"><div align="center">
              <table width="100%" border="1" cellpadding="4" cellspacing="1" bordercolor="#999999" bgcolor="#F7F5F4">
                <tr class="tblTitulo">
                  <td>Archivo</td>
                  <td>Ruta</td>
                </tr>
                <tr>
                  <td class="bordereporte"><div align="center">
                    <select name="s_jsps" size="15" class="textbox" id="select2" style="width:200px; border:none; font-weight:bold; " onChange="seleccionar();" onDblClick="if(validar()) enviar();">
                    </select>
                  </div></td>
                  <td class="bordereporte"><div align="center">
                    <select name="s_url" size="15" class="textbox" id="select3" style="width:250px; border:none " onChange="seleccionar2();"  onDblClick="seleccionar2(); if(validar()) enviar();">
                    </select>
                  </div></td>
                </tr>
              </table>
              </div></td>
          </tr>
        </table></td>
    </tr>
  </table>
  <p align="center"><img src="<%= BASEURL%>/images/botones/aceptar.gif"  name="imgaceptar" onClick="if(validar()) enviar();" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);">&nbsp; <img src="<%= BASEURL%>/images/botones/cancelar.gif"  name="imgcancelar"  onMouseOver="botonOver(this);" onClick="forma.reset();" onMouseOut="botonOut(this);" >&nbsp; <img src="<%= BASEURL%>/images/botones/salir.gif"  name="imgsalir"  onMouseOver="botonOver(this);" onClick="parent.close();" onMouseOut="botonOut(this);" ></p>
</form>
</body>
</html>
