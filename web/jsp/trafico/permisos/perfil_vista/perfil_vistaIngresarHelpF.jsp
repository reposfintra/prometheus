<%@ include file="/WEB-INF/InitModel.jsp"%>

<HTML>
<HEAD>
<TITLE>Ingresar Perfil Vista</TITLE>
<META http-equiv=Content-Type content="text/html; charset=windows-1252">
<link href="/css/estilostsp.css" rel="stylesheet" type="text/css"> 
<link href="<%=BASEURL%>/css/estilostsp.css" rel="stylesheet" type="text/css"> 
</HEAD>
<BODY>  

  <table width="95%"  border="2" align="center">
    <tr>
      <td>
        <table width="100%" border="0" align="center">
          <tr  class="subtitulo">
            <td height="20"><div align="center">Ingresar Perfil Vista</div></td>
          </tr>
          <tr class="subtitulo1">
            <td>Descripci&oacute;n del funcionamiento del programa de Ingresar Perfil Vista. </td>
          </tr>
          <tr>
            <td  height="18"  class="ayudaHtmlTexto">Formulario de Ingresar Perfil Vista. </td>
          </tr>
          <tr>
            <td height="18"  class="ayudaHtmlTexto"><div align="center"><img src="<%= BASEURL%>/images/ayuda/trafico/permisos/perfil_vista/Dibujo1.PNG"></div></td>
          </tr>
          <tr>
            <td  class="ayudaHtmlTexto">&nbsp;</td>
          </tr>
          <tr>
            <td  class="ayudaHtmlTexto">Explorador del sitio, puede hacer 'Clic' sobre este bot&oacute;n para abrir la ventana para seleccionar la p&aacute;gina JSP. </td>
          </tr>
          <tr>
            <td  class="ayudaHtmlTexto"><div align="center">
              <img src="<%= BASEURL%>/images/botones/iconos/buscar.gif"  name="imgsalir" width="50" height="50" align="absmiddle" style="cursor:hand " onClick="window.open('<%=BASEURL%>/jsp/trafico/permisos/perfil_vista/perfil_vistaBuscarPag.jsp?perfil=&campo=' + forma.c_campo.value + '&estado=CamposJsp&accion=BuscarPagIngresar','verConsulta','status=no,scrollbars=no,width=480,height=430,resizable=yes');">            </div></td>
          </tr>
          <tr>
            <td  class="ayudaHtmlTexto">&nbsp;</td>
          </tr>
          <tr>
            <td  class="ayudaHtmlTexto">Ventana de exploraci&oacute;n de archivos. </td>
          </tr>
          <tr>
            <td  class="ayudaHtmlTexto"><div align="center"><img src="<%= BASEURL%>/images/ayuda/trafico/permisos/campos_jsp/Dibujo2.PNG"></div></td>
          </tr>
          <tr>
            <td  class="ayudaHtmlTexto">&nbsp;</td>
          </tr>
          <tr>
            <td  class="ayudaHtmlTexto">A continuaci&oacute;n debera especificar las restrivviones que tiene este perfil en cada uno de los campos almacenados de la p&aacute;gina JSP. </td>
          </tr>
          <tr>
            <td  class="ayudaHtmlTexto"><div align="center"><img src="<%= BASEURL%>/images/ayuda/trafico/permisos/perfil_vista/Dibujo2.PNG"></div></td>
          </tr>
          <tr>
            <td  class="ayudaHtmlTexto">&nbsp;</td>
          </tr>
          <tr>
            <td  class="ayudaHtmlTexto">Al guardar el Perfil-Vista debidamente, se mostrar&aacute; el siguiente mensaje:</td>
          </tr>
          <tr>
            <td  class="ayudaHtmlTexto"><div align="center"><img src="<%= BASEURL%>/images/ayuda/trafico/permisos/perfil_vista/Dibujo3.PNG"></div></td>
          </tr>
          <tr>
            <td  class="ayudaHtmlTexto">&nbsp;</td>
          </tr>
          
          
          
      </table></td>
    </tr>
  </table>
</BODY>
</HTML>
