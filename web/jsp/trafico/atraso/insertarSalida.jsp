<!--
- Autor : Ing. David Lamadrid
- Date  : 5 de Octubre de 2005
- Copyrigth Notice : Fintravalores S.A. S.A
-->
<%--
-@(#)
--Descripcion : Pagina JSP, que se encarga de gestionar 
                            el formulario para la insercion de datos en la 
                            tabala retraso_salida
--%>

<%-- Declaracion de librerias--%>
<%@page contentType="text/html"%>
<%@page errorPage="/error/ErrorPage.jsp"%>
<%@page import="com.tsp.operation.model.beans.*, java.util.*"%>
<%@include file="/WEB-INF/InitModel.jsp" %>
<%@ taglib uri="http://jakarta.apache.org/taglibs/input-0.90" prefix="input" %>
<%
    Usuario usuarioLogin  = (Usuario) session.getAttribute("Usuario");
    String us  = usuarioLogin.getLogin();
%>

<html>
<head>
<title>Cambiar Hora Salida</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link href="../css/estilostsp.css" rel="stylesheet" type="text/css"> 
<link href="<%=BASEURL%>/css/estilostsp.css" rel="stylesheet" type="text/css"> 
<script type='text/javascript' src="<%=BASEURL%>/js/Validacion.js"></script> 
<script type='text/javascript' src="<%= BASEURL %>/js/boton.js"></script>
<script>

   function abrirVentanaBusq(an,al,url,pag) {
        parent.open(url,'Conductor','width='+an+',height='+al+',scrollbars=no,resizable=no,top=10,left=65,status=yes');
    }


function validar(){

	if(forma1.c_nuevo.value==""  ){
		alert ("Debe seleccionar una Fecha ");
		return false;
	}
	else if(forma1.motivo.value==""  ){
		alert ("Digite un motivo");
		return false;
	}
	else {
		insertar();
	}
	
	
}
function salir(){
	window.close();

}

function insertar(){
		document.forma1.action = "<%= CONTROLLER %>?estado=Retraso&accion=InsertarS"; 
        document.forma1.submit(); 
}

</script>
<link href="/css/estilostsp.css" rel="stylesheet" type="text/css">
</head>

<body onresize="redimensionar()" onload = 'redimensionar()'>
<div id="capaSuperior" style="position:absolute; width:100%; height:100px; z-index:0; left: 0px; top: 0px;">
 <jsp:include page="/toptsp.jsp?encabezado=Cambiar Hora Salida"/>
</div>
<div id="capaCentral" style="position:absolute; width:100%; height:87%; z-index:0; left: 0px; top: 100px; overflow: scroll;">
<%
	 String fechaActual=""+request.getParameter("fechaS");
	 if (fechaActual.equals("0099-01-01 00:00:00")){
	 	fechaActual="";
	 }
	 String planilla=""+request.getParameter("planilla");   
%>
<form name="forma1" action="<%=CONTROLLER%>?estado=Historico&accion=Insertar" method="post" onSubmit="return validar();">      
    <table width="500" border="2" align="center">
      <tr>
    <td width="696">
<table width="99%" align="center"> 
  <tr>
            <td width="145"  class="subtitulo1"> Informaci&oacute;n</td>
            <td width="223"  class="barratitulo"><img src="<%=BASEURL%>/images/titulo.gif"></td>
  </tr>
</table>
        <table width="100%" align="center">
          <tr>
            <td width="152"  class="fila" >Planilla :</td>
            <td class="letra"><input name="planilla" type="hidden" class="textbox" id="planilla" value="<%=planilla%>"size="30" maxlength="50" >
                <div align="left"  ><%=planilla%></div></td>
          </tr>
          <tr >
            <td class="fila">Fecha de Salida Actual :</td>
            <td class="letra"><input name="c_actual" type="hidden" class="textbox" id="c_actual" value="<%=fechaActual%>"size="30" maxlength="50" >
                <div ><%=fechaActual%></div>
            </td>
          </tr>
          <tr class="fila"> 
            <td width="152" >Nueva Fecha de Salida: </td>
            <td width="324"><input name="c_nuevo" type="text" class="textbox" id="c_nuevo" size="20" readonly > 
              				 <a href="javascript:void(0)" onclick="if(self.gfPop)gfPop.fPopCalendar(document.forma1.c_nuevo);return false;" HIDEFOCUS>
			  <img src="<%=BASEURL%>\js\Calendario\cal.gif" width="16" height="16" border="0" alt="De click aqui para escoger la fecha">
            </td>
          </tr>
          <tr class="fila">
            <td >Causa : </td><% TreeMap causa = model.tblgensvc.getCausasDemora(); %>
            <td ><input type="hidden" name="tipo" id="tipo" value="salida">              <input:select default="" name="causa" attributesText="class='textbox' style='width:99%'" options="<%=causa %>"/> </td>
          </tr>
          <tr class="fila"> 
              <td  align="center"><div align="left">Motivo:</div></td>
		      <td  align="center">
                  <textarea name="motivo" cols="60" class="textbox" id="motivo"></textarea>
			  </td>
          </tr>
        </table>
</td>
</tr>
</table>

  <br>
  <div align="center">
  <input type="hidden" name='usuario' value="<%=us%>">
      <img src='<%=BASEURL%>/images/botones/aceptar.gif' name='Buscar' align="absmiddle" style='cursor:hand' title='Aceptar...'  onclick="validar();" onMouseOver='botonOver(this);' onMouseOut='botonOut(this);'> 
      &nbsp;&nbsp;<img src='<%=BASEURL%>/images/botones/salir.gif' name='Buscar' align="absmiddle" style='cursor:hand' title='Salir...'  onclick="salir();" onMouseOver='botonOver(this);' onMouseOut='botonOut(this);'> 
    </div>
  <p>&nbsp;</p> 
</form>

<%
  String mensaje=""+request.getParameter("ms");
    if (! (mensaje.equals("")|| mensaje.equals("null"))){
%>
<table border="2" align="center">
  <tr>
    <td><table width="410" height="20" border="1" align="center"  bordercolor="#F7F5F4" bgcolor="#FFFFFF">
        <tr>
          <td width="282" align="center" class="mensajes"><%=mensaje%></td>
          <td width="28" background="<%=BASEURL%>/images/cuadronaranja.JPG">&nbsp;</td>
          <td width="78">&nbsp;</td>
        </tr>
    </table></td>
  </tr>
</table>
<%
    }   %>
</div>
</body>
 
<iframe width=188 height=166 name="gToday:normal:agenda.js:gfPop:plugins_24.js" id="gToday:normal:agenda.js:gfPop:plugins_24.js" src="<%=BASEURL%>/js/Calendario/ipopeng.htm" scrolling="no" frameborder="0" style="visibility:visible; z-index:999; position:absolute; left:-500px; top:0px;"> </iframe>
</html>
