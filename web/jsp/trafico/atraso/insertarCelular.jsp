<!--
- Autor : Ing. David Lamadrid
- Date  : 5 de Octubre de 2005
- Copyrigth Notice : Fintravalores S.A. S.A
-->
<%--
-@(#)
--Descripcion : Pagina JSP, que se encarga de gestionar 
                            el formulario para la insercion de datos en la 
                            tabala retraso_salida
--%>

<%-- Declaracion de librerias--%>
<%@page contentType="text/html"%>
<%@page errorPage="/error/ErrorPage.jsp"%>
<%@page import="com.tsp.operation.model.beans.*, java.util.*"%>
<%@include file="/WEB-INF/InitModel.jsp" %>
<%@ taglib uri="http://jakarta.apache.org/taglibs/input-0.90" prefix="input" %>
<%
    Usuario usuarioLogin  = (Usuario) session.getAttribute("Usuario");
    String us  = usuarioLogin.getLogin();
%>

<html>
<head>
<title>Cambiar Celular</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link href="../css/estilostsp.css" rel="stylesheet" type="text/css"> 
<link href="<%=BASEURL%>/css/estilostsp.css" rel="stylesheet" type="text/css"> 
<script type='text/javascript' src="<%=BASEURL%>/js/Validacion.js"></script> 
<script type='text/javascript' src="<%= BASEURL %>/js/boton.js"></script>
<script>

   function abrirVentanaBusq(an,al,url,pag) {
        parent.open(url,'Conductor','width='+an+',height='+al+',scrollbars=no,resizable=no,top=10,left=65,status=yes');
    }


function validar(){

	/*if(isNaN(forma1.c_nuevo.value)|| forma1.c_nuevo.value==""  ){
		alert ("El nuevo Celular deve ser un numero");
		forma1.c_fecha.focus();
		return false;
	}
	else*/ if(forma1.motivo.value==""  ){
		alert ("Digite un motivo");
		forma1.motivo.focus();
		return false;
	}
	else {
		insertar();
	}
	
	
}
function salir(){
	window.close();

}

function insertar(){
		document.forma1.action = "<%= CONTROLLER %>?estado=Retraso&accion=InsertarC"; 
        document.forma1.submit(); 
}

</script>
<link href="/css/estilostsp.css" rel="stylesheet" type="text/css">
</head>

<body onresize="redimensionar()" onload = 'redimensionar()'>
<div id="capaSuperior" style="position:absolute; width:100%; height:100px; z-index:0; left: 0px; top: 0px;">
 <jsp:include page="/toptsp.jsp?encabezado=Cambiar Celular Conductor"/>
</div>
<div id="capaCentral" style="position:absolute; width:100%; height:87%; z-index:0; left: 0px; top: 100px; overflow: scroll;">
<%
	 String cedula=""+request.getParameter("cedula");
	 String nombre = model.nitService.obtenerNombre (cedula);
	 String celularActual=""+request.getParameter("celular");
	 String planilla=""+request.getParameter("planilla");  
%>
<form name="forma1" action="<%=CONTROLLER%>?estado=Historico&accion=Insertar" method="post" onSubmit="return validar();">      
    <table width="396" border="2" align="center">
      <tr>
    <td width="696">
<table width="99%" align="center"> 
  <tr>
            <td width="145"  class="subtitulo1">
Despacho Manual</td>
            <td width="223"  class="barratitulo"><img src="<%=BASEURL%>/images/titulo.gif"></td>
  </tr>
</table>
        <table width="99%" align="center">
          <tr class="fila">
            <td height="30" >Planilla :</td>
            <td><input name="planilla" type="hidden" class="textbox" id="planilla" value="<%=planilla%>"size="30" maxlength="50" >
              <input type="hidden" name="cedula"  value="<%=cedula%>">                
              <div align="left"  ><%=planilla%></div></td>
          </tr>
		  <tr class="fila">
            <td height="30" >Conductor :</td>
            <td>             
              <div align="left"  ><%=nombre%></div></td>
          </tr>
          <tr class="fila">
            <td height="30" >Celular :</td>
            <td><input name="c_actual" type="hidden" class="textbox" id="c_actual" value="<%=celularActual%>"size="30" maxlength="50" >
                <div ><%=celularActual%></div>
            </td>
          </tr>
          <tr class="fila"> 
            <td width="144" height="30" >Nuevo Celular : </td>
            <td width="201"><input name="c_nuevo" type="text" class="textbox" id="c_nuevo" size="20"  maxlength="15"> 
               </td>
          </tr>
          <tr class="fila">
            <td height="30" >Causa : </td><% TreeMap causa = model.tblgensvc.getCausasCambioCelCond(); %>
            <td ><input:select default="" name="causa" attributesText="class=textbox" options="<%=causa %>"/> 
            </td>
          </tr>
          <tr class="fila"> 
              <td  align="center"><div align="left">Motivo:</div></td>
			   <td  align="center">
                  <textarea name="motivo" cols="32" class="textbox" id="motivo"></textarea>
                	<input type="hidden" name="tipo" id="tipo" value="celular">
				</td>
          </tr>
        </table>
</td>
</tr>
</table>

  <br>
  <div align="center">
  <input type="hidden" name='usuario' value="<%=us%>">
      <img src='<%=BASEURL%>/images/botones/aceptar.gif' name='Buscar' align="absmiddle" style='cursor:hand' title='Aceptar...'  onclick="validar();" onMouseOver='botonOver(this);' onMouseOut='botonOut(this);'> 
      &nbsp;&nbsp;<img src='<%=BASEURL%>/images/botones/salir.gif' name='Buscar' align="absmiddle" style='cursor:hand' title='Salir...'  onclick="salir();" onMouseOver='botonOver(this);' onMouseOut='botonOut(this);'> 
    </div>
  <p>&nbsp;</p> 
</form>

<%
  String mensaje=""+request.getParameter("ms");
    if (! (mensaje.equals("")|| mensaje.equals("null"))){
%>
<table border="2" align="center">
  <tr>
    <td><table width="410" height="20" border="1" align="center"  bordercolor="#F7F5F4" bgcolor="#FFFFFF">
        <tr>
          <td width="282" align="center" class="mensajes"><%=mensaje%></td>
          <td width="28" background="<%=BASEURL%>/images/cuadronaranja.JPG">&nbsp;</td>
          <td width="78">&nbsp;</td>
        </tr>
    </table></td>
  </tr>
</table>
<%
    }   %>
</div>
</body>
 
<iframe width=188 height=166 name="gToday:normal:agenda.js:gfPop:plugins_24.js" id="gToday:normal:agenda.js:gfPop:plugins_24.js" src="<%=BASEURL%>/js/Calendario/ipopeng.htm" scrolling="no" frameborder="0" style="visibility:visible; z-index:999; position:absolute; left:-500px; top:0px;"> </iframe>
</html>
