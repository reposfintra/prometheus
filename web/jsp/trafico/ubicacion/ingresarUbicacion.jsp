<!--
- Autor : Ing. Jesus Contaco
- Modificado por: Ing. Sandra M Escalante G
- Date  : 02 Febrero 2006
- Copyrigth Notice : Fintravalores S.A. S.A
-->
<%--
-@(#)
--Descripcion : Pagina JSP, que se encarga de gestionar 
				el formulario para realizar el ingreso 
				se ubicaciones
--%>

<%-- Declaracion de librerias--%>
<%@page contentType="text/html"%>
<%@ page session="true"%>
<%@page import="com.tsp.operation.model.beans.*, java.util.*"%>
<%@ include file="/WEB-INF/InitModel.jsp"%>
<%@page import="com.tsp.operation.model.*"%>
<%@page import="com.tsp.util.*"%>
<%@ taglib uri="http://jakarta.apache.org/taglibs/input-0.90" prefix="input" %>
<html>
<head>
<title>.: Ingresar Ubicacion</title>
<link href="<%= BASEURL %>/css/estilostsp.css" rel='stylesheet'>
<link href="../css/estilo.css" rel="stylesheet" type="text/css">
<link href="../../../css/estilostsp.css" rel="stylesheet" type="text/css">
</head>
<script type='text/javascript' src="<%= BASEURL %>/js/validar.js"></script>
<script type='text/javascript' src="<%= BASEURL %>/js/boton.js"></script>
<script type='text/javascript' src="<%= BASEURL %>/js/funciones.js"></script>
<script type="text/javascript">

</script>
<body onResize="redimensionar()" onLoad="redimensionar()">
<div id="capaSuperior" style="position:absolute; width:100%; height:100px; z-index:0; left: 0px; top: 0px;">
 <jsp:include page="/toptsp.jsp?encabezado=Ingresar Ubicacion"/>
</div>
<div id="capaCentral" style="position:absolute; width:100%; height:87%; z-index:0; left: 0px; top: 100px; overflow: scroll;">
<%String codigo = "", desc = "", ccia="", tipo="", cont="", cpais = "", cestado = "", cciudad = "", dir = "", tel1 = "", tel2 = "", tel3 = "", fax = "", ctramo="", ctiempo="", cdistancia=""; 
String vista = ( (request.getParameter("modalidad") != null) && (request.getParameter("m").equals("T") ))?"style='display:block'":"style='display:none'";
String mod = (request.getParameter("modalidad") != null)?request.getParameter("modalidad"):"C";
List estados = null, ciudades =null;
if(request.getParameter("sw")!=null){
	estados = model.estadoservice.obtenerEstados();
	ciudades = model.ciudadservice.obtenerCiudades();
	codigo = request.getParameter("c_codigo");
	desc = request.getParameter("c_descripcion");
	ccia = request.getParameter("c_cia");
	cont=request.getParameter("c_contacto"); 	
	tipo=request.getParameter("c_tipou"); 
	cpais = request.getParameter("c_pais");
	ctramo = request.getParameter("c_tramo");
	dir = request.getParameter("c_direccion");
	tel1 = request.getParameter("c_tel1"); 
	tel2 = request.getParameter("c_tel2"); 
	tel3 = request.getParameter("c_tel3"); 
	fax =request.getParameter("c_fax"); 
	ctiempo =request.getParameter("c_tiempo"); 
	cdistancia =request.getParameter("c_distancia"); 
}%>

<FORM name='forma' id='forma' method='POST' action="<%=CONTROLLER%>?estado=Ubicacion&accion=Ingresar">
  <table width="670" border="2" align="center">
    <tr>
      <td><table width="100%"  border="0">
        <tr>
          <td width="48%" class="subtitulo1"><span class="subtitulos">Datos Ubicacion </span></td>
          <td width="52%" class="barratitulo"><img src="<%=BASEURL%>/images/titulo.gif" width="32" height="20"></td>
        </tr>
      </table>
        <table width="100%" align="center" cellpadding="0" >
        <tr>
          <td width="53%" height="9" ><table align="center" width="100%" height="100%" >
              <tr class="fila">
                <td width="30%" height="25">C&oacute;digo</td>
                <td width="70%"><input name="c_codigo" type="text" class="textbox" id="c_codigo" value="<%=codigo%>" maxlength="6" onKeyPress="soloAlfa(event);">
                  <img src="<%=BASEURL%>/images/botones/iconos/obligatorio.gif" width="10" height="10"></td>
              </tr>
              <tr class="fila">
                <td height="25">Descripci&oacute;n</td>
                <td><input name="c_descripcion" type="text" class="textbox" id="c_descripcion" value="<%=desc%>" maxlength="60" onKeyPress="soloAlfa(event);">
                  <img src="<%=BASEURL%>/images/botones/iconos/obligatorio.gif" width="10" height="10"></td>
              </tr>
              <tr class="fila">
                <td height="28" nowrap>Tipo de Ubicaci&oacute;n</td>
                <td>
				<select name="c_tipou" id="select" class="textbox">
                    <option value="">Seleccione</option>
                    <%Vector tipoub = model.tipo_ubicacionService.getTipo_ubicaciones();
	  	  			for(int i = 0; i<tipoub.size(); i++){
	  					Tipo_ubicacion tu = (Tipo_ubicacion) tipoub.elementAt(i);
						if(tu.getCodigo().equals(tipo)){%>
                    		<option value="<%=tu.getCodigo()%>" selected><%=tu.getDescripcion()%></option>
                    	<%}	else{%>
                    		<option value="<%=tu.getCodigo()%>"><%=tu.getDescripcion()%></option>
                    	<%}
		  			}%>
                </select>
				<img src="<%=BASEURL%>/images/botones/iconos/obligatorio.gif" width="10" height="10">				</td>
              </tr>
              <tr class="fila">
                <td height="28">Compa&ntilde;ia</td>
                <td>
					<input name="c_cia" type="text" class="textbox" value="<%=ccia%>" maxlength="15" onKeyPress="soloAlfa(event);"><!-- onBlur="cargarSelects('<%=CONTROLLER%>?estado=Cargar&accion=Varios&carpeta=jsp/trafico/ubicacion&pagina=ingresarUbicacion.jsp&sw=3');">-->
					<img src="<%=BASEURL%>/images/botones/iconos/obligatorio.gif" width="10" height="10">&nbsp;<a href="#" class="Simulacion_Hiper" onClick="window.open('<%=CONTROLLER%>?estado=Cargar&accion=Varios&sw=6','','menubar=no, status=yes, resizable=yes, width=800, height=500, top=100')" title="Haga clic aqui para seleccionar una Compa&ntilde;ia.">Seleccione...</a>
				</td>
              </tr>
              <tr class="fila">
                <td height="28">Contacto</td>
                <td><select name="c_contacto" id="select3" class="textbox"> 
					<option value="">Ninguno</option>
                    <% if ( ccia != "" ){
						Vector contactos = model.contactoService.getVecContactos();
		        		if(model.contactoService.getVecContactos()!=null){
							for (int i = 0; i < contactos.size(); i++){
        						Contacto contacto = (Contacto) contactos.elementAt(i);
								if (contacto.getCod_contacto().equals(cont)){%>
				                    <option value="<%=contacto.getCod_contacto()%>" selected><%=contacto.getCod_contacto()%></option>
                			    <%}else{%>
				                    <option value="<%=contacto.getCod_contacto()%>" ><%=contacto.getCod_contacto()%></option>
                			    <%}
							}
						}
					}%>
                </select> &nbsp;<img src="<%= BASEURL %>/images/botones/iconos/buscar.gif" onClick="cargarSelects('<%=CONTROLLER%>?estado=Cargar&accion=Varios&carpeta=jsp/trafico/ubicacion&pagina=ingresarUbicacion.jsp&sw=3');"></td>
              </tr>
              <tr class="fila">
                <td height="28">Modalidad</td>
                <td><%TreeMap modalidad  = new TreeMap();
					modalidad.put(" Seleccione","");
					modalidad.put("Ciudad","C");
					modalidad.put("Tramo","T");%>
                  <input:select name="modalidad" attributesText="id='modalidad' class='textbox' onChange='verTramo();'" options="<%=modalidad%>" default="<%=request.getParameter("mod")%>"/>
				  <input name="m" type="hidden" value="<%=mod%>">	
				  <img src="<%=BASEURL%>/images/botones/iconos/obligatorio.gif" width="10" height="10">				</td>
              </tr>
          </table></td>
          <td width="47%" valign="top"><table width="100%" height="100%" class="tablaInferior" >
		  <tr class="fila">
                <td height="15%">Fax</td>
                <td><input name="c_fax" type="text" class="textbox" id="c_fax" value="<%=fax%>" maxlength="15" onKeyPress="soloDigitos(event, 'decNo');"></td>
		  </tr>
              <tr class="fila">
                <td height="15%">Pais</td>
                <td><select name="c_pais" id="select4" class="textbox" onChange="cargarSelects('<%=CONTROLLER%>?estado=Cargar&accion=Varios&carpeta=jsp/trafico/ubicacion&pagina=ingresarUbicacion.jsp&sw=4');">
                    <%List paises = model.paisservice.obtenerpaises();
					if (paises != null){	
						Pais pais; 
	            	    paises = model.paisservice.obtenerpaises();		
						Iterator it = paises.iterator();%>
                    	<option value="">Seleccione</option>
                    	<%while (it.hasNext()){  
							pais = (Pais) it.next();	
							if(cpais.equals(pais.getCountry_code())){%>
                    			<option value="<%=pais.getCountry_code()%>" selected><%=pais.getCountry_name()%></option>
                    		<%}else{%>
                    			<option value="<%=pais.getCountry_code()%>"><%=pais.getCountry_name()%></option>
                    		<%}
	   					}
	   				}%>
                	</select>
                  <img src="<%=BASEURL%>/images/botones/iconos/obligatorio.gif" width="10" height="10"></td>
              </tr>
              <tr class="fila">
                <td height="15%">Estado</td>
                <td><select name="c_estado" class="textbox" id="select5" onChange="cargarSelects('<%=CONTROLLER%>?estado=Cargar&accion=Varios&carpeta=jsp/trafico/ubicacion&pagina=ingresarUbicacion.jsp&sw=5');">
                    <option value="">Seleccione</option>
                    <%if(estados!=null){
						Iterator it = estados.iterator();
						while (it.hasNext()){  
		      				Estado est = (Estado) it.next();
							if(est.getdepartament_code().equals(request.getParameter("c_estado"))){%>	
			                    <option value="<%=est.getdepartament_code()%>" selected><%=est.getdepartament_name()%></option>
            		        <%}else{%>
                    			<option value="<%=est.getdepartament_code()%>"><%=est.getdepartament_name()%></option>
		                    <%}
						}
					}%>
                </select>
                  <img src="<%=BASEURL%>/images/botones/iconos/obligatorio.gif" width="10" height="10"></td>
              </tr>
              <tr class="fila">
                <td height="15%">Ciudad</td>
                <td><select id="c_ciudad" name="c_ciudad" class="textbox">
                    	<option value="">Seleccione</option>
	                    <%if(ciudades!=null){
							Iterator it = ciudades.iterator();
							while (it.hasNext()){  
								Ciudad ciu = (Ciudad) it.next();
								if(ciu.getcodciu().equals(request.getParameter("c_ciudad"))){%>
   					                 <option value="<%=ciu.getcodciu()%>" selected><%=ciu.getnomciu()%></option>
                    			<%}else{%>
				                    <option value="<%=ciu.getcodciu()%>"><%=ciu.getnomciu()%></option>
                    			<%}
							}
						}%>
                </select>
                  <img src="<%=BASEURL%>/images/botones/iconos/obligatorio.gif" width="10" height="10"></td>
              </tr>
              <tr class="fila">
                <td height="13%">Direcci&oacute;n</td>
                <td><input name="c_direccion" type="text" class="textbox" id="c_direccion" value="<%=dir%>" maxlength="45" onKeyPress="soloAlfa(event);">
                  <img src="<%=BASEURL%>/images/botones/iconos/obligatorio.gif" width="10" height="10"></td>
              </tr>
              <tr class="fila">
                <td height="42%">Tel&eacute;fono </td>
                <td><table width="100%"  border="0" cellspacing="0" cellpadding="0">
                    <tr>
                      <td><input name="c_tel1" type="text" class="textbox" id="c_tel1" value="<%=tel1%>" maxlength="15" onKeyPress="soloAlfa(event);">
                        <img src="<%=BASEURL%>/images/botones/iconos/obligatorio.gif" width="10" height="10"></td>
                    </tr>
                    <tr>
                      <td><input name="c_tel2" type="text" class="textbox" id="c_tel2" value="<%=tel2%>" maxlength="15" onKeyPress="soloAlfa(event);"></td>
                    </tr>
                    <tr>
                      <td><input name="c_tel3" type="text" class="textbox" id="c_tel3" value="<%=tel3%>" maxlength="15" onKeyPress="soloAlfa(event);"></td>
                    </tr>
                </table></td>
              </tr>
          </table></td>
        </tr>
        <tr >
          <td colspan="2"><table id="tblTramo" width="100%" class="tablaInferior" <%= vista %>>
              <tr class="fila">
                <td width="8%" rowspan="2">Tramo<input name="vista" type="hidden" value="<%=vista%>"></td>
                <td width="34%" rowspan="2"><input name="c_tramo" type="text" class="textbox" value="<%=ctramo%>" maxlength="10" onKeyPress="soloAlfa(event);">
                  <img src="<%=BASEURL%>/images/botones/iconos/obligatorio.gif" width="10" height="10">                    <a href="#" class="Simulacion_Hiper" title="Haga clic aqui para seleccionar un Tramo." onClick="window.open('<%=CONTROLLER%>?estado=Cargar&accion=Varios&sw=1','','menubar=no, status=yes, resizable=yes, width=800, height=400, top=100')" >Seleccione...</a></td>
                <td colspan="4"><table width="100%"  border="0" cellspacing="0" cellpadding="0">
                    <tr class="fila">
                      <td width="55%">Distancia y Tiempo en relaci&oacute;n a:</td>
                      <td width="6%"><input name="c_relacion" type="radio" value="O" checked></td>
                      <td width="14%">Origen</td>
                      <td width="5%"><input name="c_relacion" type="radio" value="D"></td>
                      <td width="20%">Destino</td>
                    </tr>
                </table></td>
              </tr>
              <tr class="fila">
                <td width="9%">Tiempo</td>
                <td width="17%"><input name="c_tiempo" type="text" value="<%=ctiempo%>" class="textbox" id="c_tiempo" size="15" maxlength="7" onKeyPress="soloDigitos(event, 'decOK');">
                  <img src="<%=BASEURL%>/images/botones/iconos/obligatorio.gif" width="10" height="10"></td>
                <td width="16%">Distancia (Km)</td>
                <td width="16%"><input name="c_distancia" type="text" value="<%=cdistancia%>" class="textbox" id="c_distancia" size="15" maxlength="7" onKeyPress="soloDigitos(event, 'decOK');">
                  <img src="<%=BASEURL%>/images/botones/iconos/obligatorio.gif" width="10" height="10"></td>
              </tr>
          </table></td>
        </tr>
        
      </table></td>
    </tr>
  </table>
  <br>
   <div align="center">
			   <img src="<%=BASEURL%>/images/botones/aceptar.gif"  name="imgaceptar" onClick="validarUbicacion(forma.m.value, 'Ing');" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" style="cursor:hand">&nbsp;
			   <img src="<%=BASEURL%>/images/botones/cancelar.gif"  name="imgcancelar"  onMouseOver="botonOver(this);" onClick="forma.reset();" onMouseOut="botonOut(this);" style="cursor:hand"> &nbsp;
               <img src="<%=BASEURL%>/images/botones/salir.gif"  name="imgsalir"  onMouseOver="botonOver(this);" onClick="window.close();" onMouseOut="botonOut(this);" style="cursor:hand">
           </div>
  
</form>

  <%if(request.getAttribute("mensaje")!=null){%>
	 <table border="2" align="center">
  <tr>
    <td><table width="100%" border="1" align="center"  bordercolor="#F7F5F4" bgcolor="#FFFFFF">
      <tr>
        <td width="262" align="center" class="mensajes"><%=request.getAttribute("mensaje")%></td>
        <td width="32" background="<%=BASEURL%>/images/cuadronaranja.JPG">&nbsp;</td>
        <td width="44">&nbsp;</td>
      </tr>
    </table></td>
  </tr>
</table> 
  <%}%>
</div>
</body>
</html>