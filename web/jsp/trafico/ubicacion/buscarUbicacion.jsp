<!--
- Autor : Ing. Jesus Cuestas
- Modificado por: Ing. Sandra M Escalante G
- Date  : 02 Febrero 2006
- Copyrigth Notice : Fintravalores S.A. S.A
-->
<%--
-@(#)
--Descripcion : Pagina JSP, que se encarga de gestionar 
				el formulario para realizar la busqueda de las ubicaciones
--%>

<%-- Declaracion de librerias--%>
<%@page contentType="text/html"%>
<%@page session="true"%>
<%@page import="java.util.*"%>
<%@page import="com.tsp.operation.model.*"%>
<%@page import="com.tsp.operation.model.beans.*"%>
<%@page import="com.tsp.util.*"%>
<%@include file="/WEB-INF/InitModel.jsp"%>
<html>
<head>
<title>.: Buscar Ubicacion</title>
<link href="<%= BASEURL %>/css/estilostsp.css" rel='stylesheet'>
<link href="../../../css/estilostsp.css" rel="stylesheet" type="text/css">
</head>
<script type='text/javascript' src="<%= BASEURL %>/js/validar.js"></script>
<script type='text/javascript' src="<%= BASEURL %>/js/boton.js"></script>
<body onResize="redimensionar()" onLoad="redimensionar()">
<div id="capaSuperior" style="position:absolute; width:100%; height:100px; z-index:0; left: 0px; top: 0px;">
 <jsp:include page="/toptsp.jsp?encabezado=Buscar Ubicacion"/>
</div>
<div id="capaCentral" style="position:absolute; width:100%; height:87%; z-index:0; left: 0px; top: 100px; overflow: scroll;">
<form name="forma" id="forma" method="post" action='<%=CONTROLLER%>?estado=Buscar&accion=Ubicaciones&carpeta=jsp/trafico/ubicacion&pagina=ubicaciones.jsp&sw=2'>	
  <table width="420" border="2" align="center">
    <tr>
      <td><table width="100%"  border="0">
        <tr>
          <td width="48%" class="subtitulo1">Parametros de Busqueda </td>
          <td width="52%" class="barratitulo"><img src="<%=BASEURL%>/images/titulo.gif" width="32" height="20"></td>
        </tr>
      </table>
        <table width="100%" >
        
        <tr class="fila">
          <td width="119">C&oacute;digo</td>
          <td width="273" colspan="2"  ><input name="c_codigo" type="text" class="textbox" id="c_codigo" maxlength="15"></td>
        </tr>
        <tr class="fila">
          <td width="119">Descripci&oacute;n</td>
          <td width="273" colspan="2" >
            <input name="c_descripcion" type="text" class="textbox" id="c_descripcion" maxlength="45">
          </td>
        </tr>
        <tr class="fila">
          <td width="119">Pais</td>
          <td width="273" colspan="2" >
            <input name="c_pais" type="text" class="textbox" id="c_pais" maxlength="15">
          </td>
        </tr>
        <tr class="fila">
          <td width="119">Estado</td>
          <td width="273" colspan="2" >
            <input name="c_estado" type="text" class="textbox" id="c_estado" maxlength="15">
          </td>
        </tr>
        <tr class="fila">
          <td width="119">Ciudad</td>
          <td width="273" >
            <input name="c_ciudad" type="text" class="textbox" id="c_ciudad">
          </td>
        </tr>
        <tr class="fila">
          <td width="119">Tipo de Ubicaci&oacute;n</td>
          <td width="273" >
            <input name="c_tipou" type="text" class="textbox" id="c_tipou">
          </td>
        </tr>
        <tr class="fila">
          <td width="119">Compa&ntilde;ia</td>
          <td width="273"  >
            <input name="c_cia" type="text" class="textbox" id="c_cia">
          </td>
        </tr>
        <tr class="fila">
          <td >Contacto</td>
          <td colspan="2" ><input name="c_contacto" type="text" class="textbox" id="c_contacto"></td>
        </tr>
        
      </table></td>
    </tr>
  </table>
  <br>
  <div align="center">
   <img src="<%=BASEURL%>/images/botones/buscar.gif"  name="imgbuscar" onClick="forma.submit()" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" style="cursor:hand">&nbsp;
	    <img src="<%=BASEURL%>/images/botones/detalles.gif"  name="imgdetalles"  onMouseOver="botonOver(this);" onClick="window.location='<%=CONTROLLER%>?estado=Buscar&accion=Ubicaciones&carpeta=jsp/trafico/ubicacion&pagina=ubicaciones.jsp&sw=1'" onMouseOut="botonOut(this);" style="cursor:hand"> &nbsp;
        <img src="<%=BASEURL%>/images/botones/salir.gif"  name="imgsalir"  onMouseOver="botonOver(this);" onClick="parent.close();" onMouseOut="botonOut(this);" style="cursor:hand">		
</div>
<%if(request.getParameter("mensaje")!=null){%>
 <br>
 <table border="2" align="center">
  <tr>
    <td><table width="100%" border="1" align="center"  bordercolor="#F7F5F4" bgcolor="#FFFFFF">
      <tr>
        <td width="262" align="center" class="mensajes"><%=request.getParameter("mensaje")%></td>
        <td width="32" background="<%=BASEURL%>/images/cuadronaranja.JPG">&nbsp;</td>
        <td width="44">&nbsp;</td>
      </tr>
    </table></td>
  </tr>
</table> 
  <%}%>
</form>
</div>
</body>
</html>
