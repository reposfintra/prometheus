<!--
- Autor : Ing. Jesus Cuestas
- Date  : 02 Febrero 2006
- Copyrigth Notice : Fintravalores S.A. S.A
-->
<%--
-@(#)
--Descripcion : Pagina JSP, que se encarga de gestionar 
				el formulario para mostrar las ubicaciones
--%>

<%-- Declaracion de librerias--%>
<%@page contentType="text/html"%>
<%@page session="true"%>
<%@page import="java.util.*"%>
<%@page import="com.tsp.operation.model.*"%>
<%@page import="com.tsp.operation.model.beans.*"%>
<%@page import="com.tsp.util.*"%>
<%@include file="/WEB-INF/InitModel.jsp"%>
<%@ taglib uri="http://jsptags.com/tags/navigation/pager" prefix="pg" %>
<html>
<head>
<title>.: Ubicaciones</title>
<link href="<%= BASEURL %>/css/estilostsp.css" rel='stylesheet'>
<link href="../../../css/estilostsp.css" rel="stylesheet" type="text/css">
<script type='text/javascript' src="<%= BASEURL %>/js/reporte.js"></script>
<script type='text/javascript' src="<%= BASEURL %>/js/boton.js"></script>
</head>
<body onResize="redimensionar()" onLoad="redimensionar()">
<div id="capaSuperior" style="position:absolute; width:100%; height:100px; z-index:0; left: 0px; top: 0px;">
 <jsp:include page="/toptsp.jsp?encabezado=Listado de Ubicaciones"/>
</div>
<div id="capaCentral" style="position:absolute; width:100%; height:87%; z-index:0; left: 0px; top: 100px; overflow: scroll;">
<%String style = "simple";
String position = "bottom";
String index = "center";
int maxPageItems = 12;
int maxIndexPages = 10;

Vector ubicaciones = model.ubService.getUbicaciones();
%>
<table width="90%" border="2" align="center">
  <tr>
    <td><table width="100%"  border="0">
      <tr>
        <td width="48%" class="subtitulo1"><span class="subtitulos">Ubicaciones</span></td>
        <td width="52%" class="barratitulo"><img src="<%=BASEURL%>/images/titulo.gif" width="32" height="20"></td>
      </tr>
    </table>
      <table width="99%" border="1" align="center" bordercolor="#999999" bgcolor="#F7F5F4">
      
      <tr class="tblTitulo" align="center" height="20">
        <td width="100%" nowrap><div align="center">Codigo</div></td>
        <td width="100%" nowrap><div align="center">Descripci&oacute;n</div></td>
        <td width="50%" nowrap>Tipo</td>
        <td width="100%" nowrap>Compa&ntilde;ia</td>
        <td width="100%" nowrap>Contacto</td>
        <td width="100%" nowrap><div align="center">Pais</div></td>
        <td width="100%" nowrap><div align="center">Estado</div></td>
        <td width="100%" nowrap><div align="center">Ciudad</div></td>
      </tr>
      <pg:pager    items="<%=ubicaciones.size()%>"
    index="<%= index %>"
    maxPageItems="<%= maxPageItems %>"
    maxIndexPages="<%= maxIndexPages %>"
    isOffset="<%= true %>"
    export="offset,currentPageNumber=pageNumber"
    scope="request">
      <% 
     for (int i = offset.intValue(), l = Math.min(i + maxPageItems, ubicaciones.size()); i < l; i++){
        Ubicacion ubicacion = (Ubicacion) ubicaciones.elementAt(i);
  %>
      <pg:item>
      <tr class="<%=(i % 2 == 0 )?"filagris":"filaazul"%>" onMouseOver='cambiarColorMouse(this)'  style="cursor:hand" 
	  title="Mostrar informacion de la ubicacion" onClick="window.open('<%=CONTROLLER%>?estado=Buscar&accion=Ubicaciones&carpeta=jsp/trafico/ubicacion&pagina=maUbicacion.jsp&codigo=<%=ubicacion.getCod_ubicacion()%>&dstrct=<%=ubicacion.getDstrct()%>&sw=3&lista=ok','ubi','status=yes,scrollbars=no,width=800,height=600,resizable=yes')">
        <td height="22" nowrap align="center" class="bordereporte"><%=ubicacion.getCod_ubicacion()%></td>
        <td nowrap class="bordereporte"><%=ubicacion.getDescripcion()%></td>
        <td nowrap align="center" class="bordereporte"><%=ubicacion.getNomtipo()%></td>
        <td nowrap align="center" class="bordereporte"><%=ubicacion.getNomcia()%></td>
        <td nowrap align="center" class="bordereporte"><%=ubicacion.getNomcontacto()%></td>
        <td nowrap align="center" class="bordereporte"><%=ubicacion.getNompais()%></td>
        <td nowrap align="center" class="bordereporte"><%=ubicacion.getNomestado()%></td>
        <td nowrap align="center" class="bordereporte"><%=ubicacion.getNomciudad()%></td>
        </tr>
      </pg:item>
      <%}%>
      <tr class="bordereporte" align="center">
        <td height="20" colspan="13" nowrap> <pg:index>
          <jsp:include page="/WEB-INF/jsp/googlesinimg.jsp" flush="true"/>    
    </pg:index> </td>
      </tr>
      </pg:pager>
    </table></td>
  </tr>
</table>
<br>
<table width="90%" border="0" align="center">
   <tr>
     <td><img src="<%=BASEURL%>/images/botones/regresar.gif"  name="imgaceptar" onclick="window.location.href('<%=BASEURL%>/jsp/trafico/ubicacion/buscarUbicacion.jsp')" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" style="cursor:hand">&nbsp;<img src="<%=BASEURL%>/images/botones/salir.gif"  name="imgaceptar" onclick="window.close();" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" style="cursor:hand"></td>
   </tr>
 </table>
</div>
</body>
</html>