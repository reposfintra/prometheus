<!--
- Autor : Ing. Sandra Escalante
- Date  : 02 Febrero 2006
- Copyrigth Notice : Fintravalores S.A. S.A
-->
<%--
-@(#)
--Descripcion : Pagina JSP, que se encarga de gestionar 
				el formulario para la busqueda de los tramos
--%>

<%-- Declaracion de librerias--%>
<%@page contentType="text/html"%>
<%@page session="true"%>
<%@page errorPage="../error/ErrorPage.jsp"%>
<%@page import="java.util.*"%>
<%@page import="com.tsp.operation.model.*"%>
<%@page import="com.tsp.util.*"%>
<%@page import="com.tsp.operation.model.beans.*, java.util.*"%>
<%@include file="/WEB-INF/InitModel.jsp"%>
<%@ taglib uri="http://jakarta.apache.org/taglibs/input-0.90" prefix="input" %>
<html>
<head>
<title>.: Buscar Tramo</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link href="<%= BASEURL %>/css/estilostsp.css" rel="stylesheet" type="text/css"> 
<link href="../../../css/estilostsp.css" rel="stylesheet" type="text/css">
<script type='text/javascript' src="<%= BASEURL %>/js/validar.js"></script>
<script type='text/javascript' src="<%= BASEURL %>/js/boton.js"></script>
<script>
    function cargarDestinos() {
        forma.c_origenselec.value = forma.c_origen.options(forma.c_origen.selectedIndex).value;
		window.location='<%=CONTROLLER%>?estado=Cargar&accion=Varios&sw=2&origen=' + forma.c_origenselec.value;
    }
	
	function copiarInfo() {		
       	forma.c_tramo.value = "[" + forma.c_origen.options(forma.c_origen.selectedIndex).value + 
		forma.c_destino.options(forma.c_destino.selectedIndex).value + "] " + 
		forma.c_origen.options(forma.c_origen.selectedIndex).text + " - " + 
		forma.c_destino.options(forma.c_destino.selectedIndex).text; 
		forma.c_tr.value = forma.c_origen.options(forma.c_origen.selectedIndex).value + 
		forma.c_destino.options(forma.c_destino.selectedIndex).value;
    }
</script>
</head>
<body onResize="redimensionar()" onLoad="redimensionar()">
<div id="capaSuperior" style="position:absolute; width:100%; height:100px; z-index:0; left: 0px; top: 0px;">
 <jsp:include page="/toptsp.jsp?encabezado=Buscar Tramo"/>
</div>
<div id="capaCentral" style="position:absolute; width:100%; height:87%; z-index:0; left: 0px; top: 100px; overflow: scroll;">
<br>
<form name="forma" method="post">
  <table width="60%" border="2" align="center">
    <tr>
      <td><table width="100%"  border="0">
        <tr>
          <td width="48%" class="subtitulo1">Parametros de Busqueda </td>
          <td width="52%" class="barratitulo"><img src="<%=BASEURL%>/images/titulo.gif" width="32" height="20"></td>
        </tr>
      </table>
        <table width="100%"  align="center">
        
        <tr class="subtitulos">
          <td class="fila informacion" >**Seleccione un Tramo (Origen y Destino) y haga clic en Aceptar.</td>
        </tr>
        <tr class="fila">
          <td ><table width="92%" align="center"  border="0" cellpadding="1" cellspacing="2">
              <tr class="fila">
                <td width="10%">Origen</td>
                <td width="61%">
                  <% TreeMap origins  = model.tramoService.getOrigenes();%>
                  <input:select name="c_origen" attributesText="id='c_origen' class=textbox onChange='cargarDestinos();'" options="<%=origins %>" default="<%=request.getParameter("c_origenselec")%>"/> </td>
                <td width="29%" rowspan="2"><img src="<%=BASEURL%>/images/botones/aceptar.gif"  name="imgdetalles"  onMouseOver="botonOver(this);" onClick="if (forma.c_tr.value == '' ){alert('Debe seleccionar un Tramo (Origen y Destino)');}else{copiarCampo(forma.c_tr.value,window.opener.document.forma.c_tramo);}" onMouseOut="botonOut(this);" style="cursor:hand">
				</td>
              </tr>
              <tr>
                <td class="fila">Destino</td>
                <td><%TreeMap destinos = model.tramoService.getDestinos();
					 destinos.put(" Seleccione","");%>
                  <input:select name="c_destino" attributesText="class=textbox onChange='copiarInfo();'" options="<%=destinos %>"/>
                  <input type="hidden" name="c_origenselec" id="c_origenselec" value="<%=request.getParameter("c_origenselec") %>"/>
                  <input type="hidden" name="c_tr" id="c_tr"/>
                </td>
              </tr>
              <tr class="fila">
                <td colspan="2"> </td>
              </tr>
          </table></td>
        </tr>
        <tr class="fila">
          <td><table width="100%" border="0" cellspacing="0" cellpadding="0">
              <tr class="fila">
                <td width="85">TRAMO</td>
                <td width="480"><input name="c_tramo" type="text" class="fila" id="c_tramo" value="" size="70" readonly></td>
              </tr>
          </table></td>
        </tr>
      </table></td>
    </tr>
  </table>
</form>
</div>
</body>
</html>
