<!--
- Autor : Ing. Jesus Cuestas
- Date  : 02 Febrero 2006
- Copyrigth Notice : Fintravalores S.A. S.A
-->
<%--
-@(#)
--Descripcion : Pagina JSP, que se encarga de gestionar 
				el formulario para modificar/anular ubicaciones 
--%>

<%-- Declaracion de librerias--%>
<%@page contentType="text/html"%>
<%@ page session="true"%>
<%@page import="com.tsp.operation.model.beans.*, java.util.*"%>
<%@ include file="/WEB-INF/InitModel.jsp"%>
<%@page import="com.tsp.operation.model.*"%>
<%@page import="com.tsp.util.*"%>
<%@ taglib uri="http://jakarta.apache.org/taglibs/input-0.90" prefix="input" %>
<html>
<head>
<title>.: Modificar/Anular Ubicacion</title>
<link href="<%= BASEURL %>/css/estilostsp.css" rel='stylesheet'>
<link href="../../../css/estilostsp.css" rel="stylesheet" type="text/css">
</head>
<body onResize="redimensionar()" <%= ( request.getParameter("reload")!=null ) ?"onLoad='redimensionar();window.opener.location.reload();'" : "onLoad='redimensionar();'"%>>
<div id="capaSuperior" style="position:absolute; width:100%; height:100px; z-index:0; left: 0px; top: 0px;">
 <jsp:include page="/toptsp.jsp?encabezado=Modificar/Anular Ubicacion"/>
</div>
<div id="capaCentral" style="position:absolute; width:100%; height:87%; z-index:0; left: 0px; top: 100px; overflow: scroll;">

<%	String codigo = "", desc = "", cpais = "", cestado = "", cciudad = "";
	String dir = "", tel1 = "", tel2 = "", tel3 = "", fax = "", tipo="", ccia="";
	String cont="", torigen="", tdestino="", relacion = "", ctramo="", mod=""; 
	double ctiempo=0, cdistancia =0;
	int sw = (request.getParameter("lista")!=null)? 1:0;
	String nomcia="", nomcontacto="", nomtipo="", cdistrito ="";
	Ubicacion u = model.ubService.getUb(); 
	codigo = (request.getParameter("lista")!=null)? u.getCod_ubicacion():request.getParameter("c_codigo");
   	desc = (request.getParameter("lista")!=null)? u.getDescripcion():request.getParameter("c_descripcion");
	tipo= (request.getParameter("lista")!=null)? u.getTipo():request.getParameter("c_tipou"); 		
	ccia= (request.getParameter("lista")!=null)? u.getCia():request.getParameter("c_cia"); 
    cont = (request.getParameter("lista")!=null)? u.getContacto():request.getParameter("c_contacto");
	torigen = (request.getParameter("lista")!=null)? u.getTorigen():"";
	tdestino = (request.getParameter("lista")!=null)? u.getTdestino():"";
	
	mod = (request.getParameter("lista")!=null)? u.getModalidad():request.getParameter("modalidad");
	ctramo = (request.getParameter("lista")!=null)? torigen + tdestino:request.getParameter("c_tramo");
	relacion = (request.getParameter("lista")!=null)? u.getRelacion():request.getParameter("c_relacion");
	ctiempo = (request.getParameter("lista")!=null)? u.getTiempo():Double.parseDouble (request.getParameter("c_tiempo")); 
	cdistancia = (request.getParameter("lista")!=null)? u.getDistancia():Double.parseDouble(request.getParameter("c_distancia")); 

	String ro = (relacion.equals("O") && relacion != null )?"checked":"";
	String rd = (relacion.equals("D"))?"checked":"";
    	
	cpais = (request.getParameter("lista")!=null)? u.getNompais():request.getParameter("c_pais");
   	cestado = (request.getParameter("lista")!=null)? u.getNomestado():request.getParameter("c_comboestado");
   	cciudad = (request.getParameter("lista")!=null)? u.getNomciudad():request.getParameter("c_ciudad");
	    
	dir = (request.getParameter("lista")!=null)? u.getDireccion():request.getParameter("c_direccion");
   	tel1 = (request.getParameter("lista")!=null)? u.getTel1():request.getParameter("c_tel1");
    tel2 = (request.getParameter("lista")!=null)? u.getTel2():request.getParameter("c_tel2"); 
	tel3 = (request.getParameter("lista")!=null)? u.getTel3():request.getParameter("c_tel3"); 
    fax = (request.getParameter("lista")!=null)? u.getFax():request.getParameter("c_fax"); 
	
	String codpais = (request.getParameter("lista")!=null)? u.getPais():request.getParameter("codpais");	
	String codest = (request.getParameter("lista")!=null)? u.getEstado():request.getParameter("codest");	
	String codciud = (request.getParameter("lista")!=null)? u.getCiudad():request.getParameter("codciud");	
	
	cdistrito = (request.getParameter("lista")!=null)? u.getDstrct():request.getParameter("c_distrito");
	String vista =  ( mod.equals("T") ) ?"style='display:block'":"style='display:none'";%>
<script type='text/javascript' src="<%= BASEURL %>/js/validar.js"></script>
<script type='text/javascript' src="<%= BASEURL %>/js/boton.js"></script>
<script type='text/javascript' src="<%= BASEURL %>/js/funciones.js"></script>
<FORM name='forma' id='forma' method='POST' action="<%=CONTROLLER%>?estado=Ubicacion&accion=Modificar">
<table width="670" border="2" align="center">
  <tr>
    <td><table width="100%"  border="0">
      <tr>
        <td width="48%" class="subtitulo1"><span class="subtitulos">Informaci&oacute;n</span></td>
        <td width="52%" class="barratitulo"><img src="<%=BASEURL%>/images/titulo.gif" width="32" height="20"></td>
      </tr>
    </table>
      <table width="668"  align="center" >
      
      <tr >
        <td width="53%" height="9" ><table width="100%" height="100%" border="0" align="center" >
            <tr class="fila">
              <td width="30%" height="25">C&oacute;digo</td>
              <td width="70%"><input name="c_codigo" type="text" class="textbox" id="c_codigo" value="<%=codigo%>" maxlength="6" readonly></td>
            </tr>
            <tr class="fila">
              <td height="25">Descripci&oacute;n</td>
              <td><input name="c_descripcion" type="text" class="textbox" id="c_descripcion" value="<%=desc%>" maxlength="60" onKeyPress="soloAlfa(event);">
                <img src="<%=BASEURL%>/images/botones/iconos/obligatorio.gif" width="10" height="10"></td>
            </tr>
            <tr class="fila">
              <td height="28" nowrap>Tipo de Ubicaci&oacute;n</td>
              <td><select name="c_tipou" id="select" class="textbox">
                  <option value="">Seleccione</option>
                  <%model.tipo_ubicacionService.listTipo_ubicacion();
			  Vector tipoub = model.tipo_ubicacionService.getTipo_ubicaciones();
		  	  for(int i = 0; i<tipoub.size(); i++){
	  			Tipo_ubicacion tu = (Tipo_ubicacion) tipoub.elementAt(i);
				if(tu.getCodigo().equals(tipo)){%>
                  <option value="<%=tu.getCodigo()%>" selected><%=tu.getDescripcion()%></option>
                  <%}
				else{%>
                  <option value="<%=tu.getCodigo()%>"><%=tu.getDescripcion()%></option>
                  <%}
		  }%>
              </select>
                <img src="<%=BASEURL%>/images/botones/iconos/obligatorio.gif" width="10" height="10"></td>
            </tr>
            <tr class="fila">
              <td height="28">Compa&ntilde;ia</td>
              <td><input name="c_cia" type="text" class="textbox" value="<%=ccia%>">
                <img src="<%=BASEURL%>/images/botones/iconos/obligatorio.gif" width="10" height="10">&nbsp;<a href="#" class="Simulacion_Hiper" onClick="window.open('<%=CONTROLLER%>?estado=Cargar&accion=Varios&sw=6','','menubar=no, status=yes, resizable=yes, width=800, height=500, top=100')" title="Haga clic aqui para seleccionar una Compa&ntilde;ia.">Seleccione...</a></td>
            </tr>
            <tr class="fila">
              <td height="28">Contacto</td>
              <td><select name="c_contacto" id="select3" class="textbox">
                  <option value="">Ninguno</option>
                  <%Vector contactos = model.contactoService.getVecContactos();
        			if(contactos!=null){
						for (int i = 0; i < contactos.size(); i++){
          					Contacto contacto = (Contacto) contactos.elementAt(i);
							if (contacto.getCod_contacto().equals(cont)){%>
			                  <option value="<%=contacto.getCod_contacto()%>" selected><%=contacto.getCod_contacto()%></option>
            		      <%}else{%>
                  			<option value="<%=contacto.getCod_contacto()%>" ><%=contacto.getCod_contacto()%></option>
                  		  <%}
						}
					}%>
              </select>&nbsp;
                <img src="<%= BASEURL %>/images/botones/iconos/buscar.gif" onClick="cargarSelects('<%=CONTROLLER%>?estado=Cargar&accion=Varios&carpeta=jsp/trafico/ubicacion&pagina=maUbicacion.jsp&sw=3&modificar=ok');"></td>
            </tr>
            <tr class="fila">
              <td height="12%">Distrito</td>
              <td><input name="c_distrito" type="text" class="textbox" id="c_distrito" value="<%=cdistrito%>" maxlength="45" readonly></td>
            </tr>
            <tr class="fila">
              <td height="12%">Modalidad</td>
              <td>
			  <%TreeMap modalidad  = new TreeMap();
					modalidad.put(" Seleccione","");
					modalidad.put("Ciudad","C");
					modalidad.put("Tramo","T");%>
                  <input:select name="modalidad" attributesText="id='modalidad' class='textbox' onChange='verTramo();'" options="<%=modalidad%>" default="<%=mod%>" />
				  <input name="m" type="hidden" value="<%=mod%>">
                  <img src="<%=BASEURL%>/images/botones/iconos/obligatorio.gif" width="10" height="10"> </td>
            </tr>
			
        </table></td>
        <td width="47%" valign="top"><table width="100%" height="100%" border="0" >
            <tr class="fila">
              <td height="14%" >Pais</td>
              <td><input name="codpais" type="hidden" value="<%=codpais%>"><input name="c_pais" id="c_pais" type="text" class="textbox" value="<%=cpais%>" readonly></td>
            </tr>
            <tr class="fila">
              <td height="14%" >Estado</td>
              <td nowrap><input name="codest" type="hidden" value="<%=codest%>"><input name="c_comboestado" id="c_comboestado" type="text" class="textbox" value="<%=cestado%>" readonly>
                <img src="<%=BASEURL%>/images/botones/iconos/obligatorio.gif" width="10" height="10"> 
				&nbsp;<a href="#" class="Simulacion_Hiper" onClick="window.open('<%=BASEURL%>/jsp/trafico/ubicacion/buscarEstado.jsp?mostrar=NO&pais=<%=codpais%>','','menubar=no, status=yes, resizable=yes, width=800, height=500, top=100')"
				title="Haga clic aqui para seleccionar un Estado.">Seleccione...</a></td>
            </tr>
            <tr class="fila">
              <td height="14%">Ciudad</td>
              <td><input name="codciud" type="hidden" value="<%=codciud%>"><input name="c_ciudad" id="c_ciudad" type="text" class="textbox" value="<%=cciudad%>" readonly></td>
            </tr>
            <tr class="fila">
              <td height="12%">Direcci&oacute;n</td>
              <td><input name="c_direccion" type="text" class="textbox" id="c_direccion" value="<%=dir%>">
                <img src="<%=BASEURL%>/images/botones/iconos/obligatorio.gif" width="10" height="10"></td>
            </tr>
            <tr class="fila">
              <td height="33%">Tel&eacute;fono</td>
              <td><table width="100%"  border="0" cellspacing="0" cellpadding="0">
                  <tr>
                    <td><input name="c_tel1" type="text" class="textbox" id="c_tel1" value="<%=tel1%>" maxlength="15" onKeyPress="soloAlfa(event);">
                      <img src="<%=BASEURL%>/images/botones/iconos/obligatorio.gif" width="10" height="10"></td>
                  </tr>
                  <tr>
                    <td><input name="c_tel2" type="text" class="textbox" id="c_tel2" value="<%=tel2%>" maxlength="15" onKeyPress="soloAlfa(event);"></td>
                  </tr>
                  <tr>
                    <td><input name="c_tel3" type="text" class="textbox" id="c_tel3" value="<%=tel3%>" maxlength="15" onKeyPress="soloAlfa(event);"></td>
                  </tr>
              </table></td>
            </tr>
			<tr class="fila">
              <td height="28">Fax</td>
              <td><input name="c_fax" type="text" class="textbox" id="c_fax" value="<%=fax%>" maxlength="15" onKeyPress="soloAlfa(event);">                </td>
            </tr>
        </table></td>
      </tr>
      <tr  >
        <td colspan="2"><table id="tblTramo" width="100%" class="tablaInferior" <%= vista %>>
            <tr class="fila">
              <td width="8%" rowspan="2">Tramo</td>
              <td nowrap rowspan="2"><input name="c_tramo" type="text" class="textbox" value="<%=ctramo%>" maxlength="10" onKeyPress="soloAlfa(event);">
                <img src="<%=BASEURL%>/images/botones/iconos/obligatorio.gif" width="10" height="10">                  <a href="#" class="Simulacion_Hiper" title="Haga clic aqui para seleccionar un Tramo." onClick="window.open('<%=CONTROLLER%>?estado=Cargar&accion=Varios&sw=1','','menubar=no, status=yes, resizable=yes, width=800, height=400, top=100')" >Seleccione...</a></td>
              <td colspan="4"><table width="100%"  border="0" cellspacing="0" cellpadding="0">
                  <tr class="fila">
                    <td width="55%">Distancia y Tiempo en relaci&oacute;n a:</td>
					<td width="6%"><input name="c_relacion" type="radio" value="O" <%= ro %> ></td>
                    <td width="14%">Origen</td>
                    <td width="5%"><input name="c_relacion" type="radio" value="D" <%= rd %>></td>				                    
                    <td width="20%">Destino</td>
                  </tr>
              </table></td>
            </tr>
            <tr class="fila">
              <td width="9%">Tiempo</td>
              <td nowrap><input name="c_tiempo" type="text" value="<%=ctiempo%>" class="textbox" id="c_tiempo" size="15" maxlength="7" onKeyPress="soloDigitos(event, 'decOK');">
                <img src="<%=BASEURL%>/images/botones/iconos/obligatorio.gif" width="10" height="10"></td>
              <td width="16%">Distancia (Km)</td>
              <td nowrap><input name="c_distancia" type="text" value="<%=cdistancia%>" class="textbox" id="c_distancia" size="15" maxlength="7" onKeyPress="soloDigitos(event, 'decOK');">
                <img src="<%=BASEURL%>/images/botones/iconos/obligatorio.gif" width="10" height="10"></td>
            </tr>
        </table></td>
      </tr>
      
    </table></td>
  </tr>
</table> 
<div align="center"><br>
	  <img src="<%=BASEURL%>/images/botones/modificar.gif"  name="imgmodificar" onClick="validarUbicacion(forma.m.value, 'Mod');" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" style="cursor:hand">&nbsp;
      <img src="<%=BASEURL%>/images/botones/anular.gif"  name="imganular" onClick="window.location='<%=CONTROLLER%>?estado=Ubicacion&accion=Anular&c_codigo=<%=codigo%>&c_distrito=<%=cdistrito%>'" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" style="cursor:hand">&nbsp;
      <img src="<%=BASEURL%>/images/botones/salir.gif"  name="imgsalir"  onMouseOver="botonOver(this);" onClick="window.close();" onMouseOut="botonOut(this);" style="cursor:hand">      <br>
</div>
</form>
  <%if(request.getAttribute("mensaje")!=null){%>
	 <table border="2" align="center">
  <tr>
    <td><table width="100%" border="1" align="center"  bordercolor="#F7F5F4" bgcolor="#FFFFFF">
      <tr>
        <td width="262" align="center" class="mensajes"><%=request.getAttribute("mensaje")%></td>
        <td width="32" background="<%=BASEURL%>/images/cuadronaranja.JPG">&nbsp;</td>
        <td width="44">&nbsp;</td>
      </tr>
    </table></td>
  </tr>
</table> 
  <%}%>
</div>
</div>
</body>
</html>
