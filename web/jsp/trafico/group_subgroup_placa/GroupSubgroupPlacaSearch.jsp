<!--
- Autor : Ing. Leonardo Parody Ponce
- Date : 27 diciembre de 2005
- Copyrigth Notice : Fintravalores S.A. S.A
-->
<%--
-@(#)
--Descripcion : Pagina JSP, que sirve para Buscar relaciones entre grupo, subgrupo y placa.
--%>

<%@page contentType="text/html"%>
<%@page session="true"%>
<%@page import="java.util.*"%>
<%@page import="com.tsp.operation.model.Model"%>
<%@page import="com.tsp.operation.controller.*"%>
<%@page import="com.tsp.util.*"%>
<%@include file="/WEB-INF/InitModel.jsp"%>
<%@ taglib uri="http://jakarta.apache.org/taglibs/input-0.90" prefix="input" %>

<html>
<head>
<title>Busqueda de Relacion Group-Subgroup-Placa</title>
<link href="<%= BASEURL %>/css/estilostsp.css" rel='stylesheet'>
<link href="../../../../css/estilostsp.css" rel="stylesheet" type="text/css">
<link href="/css/estilostsp.css" rel="stylesheet" type="text/css">
</head>
<script type='text/javascript' src="<%= BASEURL %>/js/validar.js"></script>
<script src='<%= BASEURL %>/js/date-picker.js'></script>
<script type='text/javascript' src="<%= BASEURL %>/js/boton.js"></script>
<body onresize="redimensionar()" onload = 'redimensionar()'>
<div id="capaSuperior" style="position:absolute; width:100%; height:100px; z-index:0; left: 0px; top: 0px;">
 <jsp:include page="/toptsp.jsp?encabezado=Group Subgroup Placa"/>
</div>
<div id="capaCentral" style="position:absolute; width:100%; height:87%; z-index:0; left: 0px; top: 100px; overflow: scroll;">
<%TreeMap subgroup = model.tblgensvc.getSub_work_group();
  TreeMap group = model.tblgensvc.getGroup();%>
<form name="forma" id="forma" method="post" action="<%=CONTROLLER%>?estado=RelacionGrupoSubgrupoPlaca&accion=Search">
  <table width="318"  border="2" align="center" cellpadding="0" cellspacing="0">
    <tr>
      <td width="312"><table width="100%" class="tablaInferior">
        <tr>
          <td colspan="2" ><table width="100%"  border="0" cellpadding="0" cellspacing="1" class="barratitulo">
              <tr>
                <td width="75%" class="subtitulo1">&nbsp;Buscar Relaciones</td>
                <td width="25%" class="barratitulo"><img src="<%=BASEURL%>/images/titulo.gif" width="32" height="20"></td>
              </tr>
          </table></td>
        </tr>
        <tr class="fila">
          <td><input name="filtro" type="radio" value="" id="filtro" onClick="validarrelaciongrupoplaca();"> Placa</td>
          <td nowrap>
            <input name="placa" type='text' disabled class="textbox" id="fin" style='width:120' value=''>
          </td>
        </tr>
		<tr class="fila">
          <td><input name="filtro" type="radio" value="" id="filtro1"  onClick="validarrelaciongrupoplaca();"> Grupo </td>      
          <td ><input:select name="grupo"  attributesText=" disabled class=textbox " options="<%= group %>" default="NADA"/></td>
		  </tr>
		<tr class="fila">
          <td><input name="filtro" type="radio" value="" id="filtro2"  onClick="validarrelaciongrupoplaca();"> Subgrupo </td>      
          <td ><input:select name="subgrupo"  attributesText="  disabled class=textbox" options="<%= subgroup %>" default="NADA"/></td>
		  </tr>
      </table></td>
    </tr>
  </table>
  <br>
  <center>
    <img src="<%=BASEURL%>/images/botones/buscar.gif" name="c_aceptar" onClick="validarsubmitrelacion();" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);"> 
	<img src="<%=BASEURL%>/images/botones/cancelar.gif" name="c_cancelar" onClick="forma.reset();" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);"> 
	<img src="<%=BASEURL%>/images/botones/salir.gif" name="c_salir" onClick="parent.close();" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);">
  </center>
</form>
  <% if( !request.getParameter("msg").equals("") ){%>
  <p><table border="2" align="center">
  <tr>
    <td><table width="100%"  border="1" align="center"  bordercolor="#F7F5F4" bgcolor="#FFFFFF">
      <tr>
        <td width="229" align="center" class="mensajes"><%=request.getParameter("msg").toString()%></td>
        <td width="29" background="<%=BASEURL%>/images/cuadronaranja.JPG">&nbsp;</td>
        <td width="58">&nbsp;</td>
      </tr>
    </table></td>
  </tr>
</table>
</p>
 <%} %>
</div>
</body>
</html>
<script>
function validarrelaciongrupoplaca(){
	var x = document.getElementById("filtro");
	var y = document.getElementById("filtro1");
	var z = document.getElementById("filtro2");
	if (x.checked==true){
	    document.forma.placa.disabled=false;
		document.forma.grupo.disabled=true;
		document.forma.subgrupo.disabled=true;
		
		return false;
	}else if (y.checked==true){
		document.forma.grupo.disabled=false;
		document.forma.placa.disabled=true;
		document.forma.subgrupo.disabled=true;
		
		return false;
	}
	else if (z.checked==true){
	    document.forma.subgrupo.disabled=false;
		document.forma.placa.disabled=true;
		document.forma.grupo.disabled=true;
		
		return false;
	}
}
function validarsubmitrelacion(){
	var x = document.getElementById("filtro");
	var y = document.getElementById("filtro1");
	var z = document.getElementById("filtro2");
	if((x.checked==false)&&(y.checked==false)&&(z.checked==false)){
		alert("Seleccione un filtro para la busqueda");
		return false;
	} if ((y.checked==true)&&(document.forma.grupo.value=='NADA')){
		alert('Especifique el grupo');
		return false;	
	}else if ((z.checked==true)&&(document.forma.subgrupo.value=='NADA')){
		alert('Especifique el subgrupo');
		return false;	
	}else{
		forma.submit();
	}
}
</script>
