<!--
- Autor : Ing. Leonardo Parody Ponce
- Date : 27 diciembre de 2005
- Copyrigth Notice : Fintravalores S.A. S.A
-->
<%--
-@(#)
--Descripcion : Pagina JSP, que sirve para Modificar y anular relaciones entre grupo, subgrupo y placa.
--%>

<%@page contentType="text/html"%>
<%@page session="true"%>
<%@page import="java.util.*"%>
<%@page import="com.tsp.operation.model.Model"%>
<%@page import="com.tsp.operation.controller.*"%>
<%@page import="com.tsp.util.*"%>
<%@include file="/WEB-INF/InitModel.jsp"%>
<%@ taglib uri="http://jakarta.apache.org/taglibs/input-0.90" prefix="input" %>

<html>
<head>
<title>Modificar Group-Subgroup-Placa</title>
<link href="<%= BASEURL %>/css/estilostsp.css" rel='stylesheet'>
<link href="../../../../css/estilostsp.css" rel="stylesheet" type="text/css">
<link href="/css/estilostsp.css" rel="stylesheet" type="text/css">
</head>
<script type='text/javascript' src="<%= BASEURL %>/js/validar.js"></script>
<script src='<%= BASEURL %>/js/date-picker.js'></script>
<script type='text/javascript' src="<%= BASEURL %>/js/boton.js"></script>
<body onload="<%if(request.getParameter("reload")!=null){%> window.opener.location.reload();<%}%> redimensionar();" onresize="redimensionar()">
<div id="capaSuperior" style="position:absolute; width:100%; height:100px; z-index:0; left: 0px; top: 0px;">
 <jsp:include page="/toptsp.jsp?encabezado=Group Subgroup Placa"/>
</div>
<div id="capaCentral" style="position:absolute; width:100%; height:87%; z-index:0; left: 0px; top: 100px; overflow: scroll;">
<%TreeMap subgroup = model.tblgensvc.getSub_work_group();
  TreeMap group = model.tblgensvc.getGroup();
  String gr = request.getParameter("grupo");
  String subgr = request.getParameter("subgrupo");
  String grupo = gr;
  String subgrupo = subgr;
  System.out.println("pagina update grupo = "+gr+"   subgrupo ="+subgr);
  %>
<form name="forma" id="forma" method="post" action="<%=CONTROLLER%>?estado=RelacionGrupoSubgrupoPlaca&accion=Update&sw=Modificar&grupo=<%=grupo%>&subgrupo=<%=subgrupo%>">
  <table width="318"  border="2" align="center" cellpadding="0" cellspacing="0">
    <tr>
      <td width="312"><table width="100%" class="tablaInferior">
        <tr>
          <td colspan="2" ><table width="100%"  border="0" cellpadding="0" cellspacing="1" class="barratitulo">
              <tr>
                <td width="75%" class="subtitulo1">Modificar Group-Subgroup-Placa </td>
                <td width="25%" class="barratitulo"><img src="<%=BASEURL%>/images/titulo.gif" width="32" height="20"></td>
              </tr>
          </table></td>
        </tr>
        <tr class="fila">
          <td>Placa</td>
          <td nowrap>
            <input name="placa" type='text' class="textbox" id="fin" style='width:120' readonly value='<%=request.getParameter("placa")%>'>
          </td>
        </tr>
		<tr class="fila">
          <td>Grupo </td>      
          <td ><input:select name="grupo1" attributesText="class=textbox" options="<%= group %>" default="<%=gr%>"/></td>
		  </tr>
		<tr class="fila">
          <td>Subgrupo </td>      
          <td ><input:select name="subgrupo1" attributesText="class=textbox" options="<%= subgroup %>" default="<%=subgr%>"/></td>
		  </tr>
      </table></td>
    </tr>
  </table>
  <br>
  <center>
    <img src="<%=BASEURL%>/images/botones/modificar.gif" name="c_aceptar" onClick="return validarrelacion();" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);"> 
	<img src="<%=BASEURL%>/images/botones/anular.gif" name="c_cancelar" onClick="return anularrelacion(); parent.close();" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);"> 
	<img src="<%=BASEURL%>/images/botones/salir.gif" name="c_salir" onClick="parent.close();" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);">
  </center>
</form>
  <% if( !request.getParameter("msg").equals("") ){%>
  <p><table border="2" align="center">
  <tr>
    <td><table width="100%"  border="1" align="center"  bordercolor="#F7F5F4" bgcolor="#FFFFFF">
      <tr>
        <td width="229" align="center" class="mensajes"><%=request.getParameter("msg").toString()%></td>
        <td width="29" background="<%=BASEURL%>/images/cuadronaranja.JPG">&nbsp;</td>
        <td width="58">&nbsp;</td>
      </tr>
    </table></td>
  </tr>
</table>
</p>
 <%} %>
</div>
</body>
</html>
<script>
function anularrelacion(){
	forma.action = '<%=CONTROLLER%>?estado=RelacionGrupoSubgrupoPlaca&accion=Update&sw=anular';
	return forma.submit();
}
function validarrelacion(){
	
	if (document.forma.placa.value==''){
		alert('Especifique el numero de Placa');
		return false;
	}else if (document.forma.grupo1.value=='NADA'){
		alert('Especifique el grupo');
		return false;	
	}else if (document.forma.subgrupo1.value=='NADA'){
		alert('Especifique el subgrupo');
		return false;	
	}else{
		//forma.action = '<%=CONTROLLER%>?estado=RelacionGrupoSubgrupoPlaca&accion=Update';
		return forma.submit();
	}
}
</script>


