<!--
- Date  : 2005
- Autor: Ing Jesus Cuestas
- Modificado Ing Sandra Escalante
- Copyrigth Notice : Fintravalores S.A. S.A
-->
<%--
-@(#)
--Descripcion : Pagina JSP, que muestra los datos de entrada para
--              ingresar un tiquete
--%>    
<%@page contentType="text/html"%>
<%@page session="true"%>
<%@page import="java.util.*"%>
<%@page import="com.tsp.operation.model.*"%>
<%@page import="com.tsp.operation.model.beans.*"%>
<%@page import="com.tsp.util.*"%>
<%@include file="/WEB-INF/InitModel.jsp"%>
<html>
<head>
<title>.: Modificar/Anular Tiquete</title>
<link href="<%= BASEURL %>/css/estilostsp.css" rel='stylesheet'> 
<link href="../css/estilostsp.css" rel="stylesheet" type="text/css">
<link href="../../../css/estilostsp.css" rel="stylesheet" type="text/css">
 <script type='text/javascript' src="<%= BASEURL %>/js/boton.js"></script>
</head>
<script type='text/javascript' src="<%= BASEURL %>/js/validar.js"></script>
<body onResize="redimensionar()" <%if(request.getParameter("reload")!=null){%>onLoad="window.opener.location.reload();redimensionar()"<%}%>>
<div id="capaSuperior" style="position:absolute; width:100%; height:100px; z-index:0; left: 0px; top: 0px;">
<jsp:include page="/WEB-INF/toptsp.jsp?encabezado=Modificar/Anular Tiquete"/>
</div>
<div id="capaCentral" style="position:absolute; width:100%; height:85%; z-index:0; left: 0px; top: 100px; overflow: scroll;"> 
<%String ccia = request.getParameter("c_cia");	
String origen = request.getParameter("c_origen");
String norigen = request.getParameter("c_norigen");
String destino = request.getParameter("c_destino");
String ndestino = request.getParameter("c_ndestino");	
Tramo_Ticket t = model.tiketService.getTicket();
String ntiket_id = t.getDtiquete();
String tiket_id = t.getTicket_id();
float cant = t.getCantidad();

if (request.getParameter("tipo")== null){%>

<FORM name='form' id='forma' method='POST' action="<%=CONTROLLER%>?estado=Tramo&accion=Modificar&modtiket=ok&pagina=maTiket.jsp" onSubmit="return validarTCamposLlenos();"> 
  <table width="200" border="2" align="center">
    <tr>
      <td><table width="416" class="tablaInferior">
       <tr>
       		<td height="24" colspan="2" nowrap  class="barratitulo"><table width="100%"  border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td width="51%" class="subtitulo1" nowrap>Modificar/Anular Tiquete</td>  
    <td width="49%"><img src="<%=BASEURL%>/images/titulo.gif" width="32" height="20"></td>
  </tr>
</table>
</td>
            </tr>
    
        <tr class="fila">
          <td width="274">Distrito </td>
          <td width="275" valign="middle"><input name="c_dcia" type="text" id="c_dcia2" value="<%=ccia%>" readonly class="textbox">
              <input name="c_cia" type="hidden" id="c_cia" value="<%=ccia%>"></td>
        </tr>
        <tr class="fila">
          <td width="274">Origen</td>
          <td width="275" valign="middle"><input name="c_norigen" type="text" id="c_norigen" value="<%=norigen%>" readonly class="textbox">
              <input name="c_origen" type="hidden" id="c_origen" value="<%=origen%>"></td>
        </tr>
        <tr class="fila">
          <td width="274">Destino</td>
          <td width="275" valign="middle"><input name="c_ndestino" type="text" id="c_ndestino" value="<%=ndestino%>" readonly class="textbox">
              <input name="c_destino" type="hidden" id="c_destino" value="<%=destino%>"></td>
        </tr>
        <tr class="fila">
          <td width="274">Tiquete </td>
          <td width="275" valign="middle"><input name="c_ntiket" type="text" id="c_ntiket" value="<%=ntiket_id %>" readonly class="textbox">
		  	<input name="c_tiket" type="hidden" id="c_tiket" value="<%= tiket_id%>">
          </tr>
        <tr class="fila">
          <td width="274">Max. Tiquetes</td>
          <td width="275" valign="middle"><input name="c_cant" type="text" id="c_cant" value="<%=cant%>" maxlength="2" class="textbox" onKeyPress="soloDigitos(event, 'decNO' )"></td>
        </tr>        
      </table></td>
    </tr>
  </table>
  <br>
  <table width="461" align="center">
    <tr>
      <td align="center"> <img src="<%=BASEURL%>/images/botones/modificar.gif"  name="imgmodificar" onClick="if( validarTCamposLlenos() ){forma.submit();}" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" style="cursor:hand"> <img src="<%=BASEURL%>/images/botones/anular.gif"  name="imgsalir" onclick="window.location='<%=CONTROLLER%>?estado=Tramo&accion=Anular&cia=<%=ccia%>&origen=<%=origen%>&destino=<%=destino%>&c_tiket=<%= tiket_id%>&anultiket=ok&pagina=mensajeTramo.jsp&marco=no'" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" style="cursor:hand"> <img src="<%=BASEURL%>/images/botones/salir.gif"  name="imgsalir" onclick="parent.close();" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" style="cursor:hand"> </td>
    </tr>
  </table>
<br>
</form>
<center> 
<%String Mensaje = ((String)request.getAttribute("mensaje")!=null)?(String)request.getAttribute("mensaje"):"";
  if(!Mensaje.equals("")){%>
   <table border="2" align="center">
    <tr>
      <td><table width="100%"  border="1" align="center"  bordercolor="#F7F5F4" bgcolor="#FFFFFF">
          <tr>
            <td width="229" align="center" class="mensajes"><%=Mensaje%></td>
            <td width="29" background="<%=BASEURL%>/images/cuadronaranja.JPG">&nbsp;</td>
            <td width="58">&nbsp;</td>
          </tr>
      </table></td>
    </tr>
  </table>
  </center>
  <%}
}else{//anulado%>
<br>
<center>
  <table border="2" align="center">
    <tr>
      <td><table width="100%"  border="1" align="center"  bordercolor="#F7F5F4" bgcolor="#FFFFFF">
          <tr>
            <td width="229" align="center" class="mensajes">Tiquete Anulado exitosamente!!!</td>
            <td width="29" background="<%=BASEURL%>/images/cuadronaranja.JPG">&nbsp;</td>
            <td width="58">&nbsp;</td>
          </tr>
      </table></td>
    </tr>
  </table>
  <br>
  <img src="<%=BASEURL%>/images/botones/salir.gif"  name="imgsalir" onclick="window.close();" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" style="cursor:hand"></center>
<%}%>
</div>
</body>
</html>
