<%@include file="/WEB-INF/InitModel.jsp"%>
<html>
<head>
<title>Descripcion Campos Depuraci&oacute;n Masiva de Tramos</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link href="../../../../css/estilostsp.css" rel="stylesheet" type="text/css">
<link href="<%= BASEURL %>/css/estilostsp.css" rel='stylesheet'>
</head>

<body>
<br>


<table width="696"  border="2" align="center">
  <tr>
    <td width="811" >
      <table width="100%" border="0" align="center">
        <tr  class="subtitulo">
          <td height="24" colspan="2"><div align="center">Depuraci&oacute;n Masiva de Tramos</div></td>
        </tr>
        <tr class="subtitulo1">
          <td colspan="2">Depuraci&oacute;n Masiva de Tramos</td>
        </tr>
        <tr>
          <td width="149" class="fila"><div align="center">
              <input name="image" type="image" style="cursor:default " src = "<%=BASEURL%>/images/botones/iconos/sig.gif">
          </div></td>
          <td width="525"  class="ayudaHtmlTexto"><p>Bot&oacute;n que permite seleccionar la opci&oacute;n que indica el texto en la misma fila. </p>
          <p>Eliminaci&oacute;n Masiva: Opci&oacute;n que permite eliminar del archivo de v&iacute;as un punto determinado, de todas las v&iacute;as donde registra.</p>
          <p>Inserci&oacute;n Masiva: Opci&oacute;n que permite  ingresar un nuevo punto intermedio en un tramos en todas la v&iacute;as donde &eacute;ste registra.</p>
          <p>Reemplazo Masivo: Opci&oacute;n que permite reemplazar ya sea el origen o el destino de un tramos por un punto determinado, tanto en el archivo de tramos como en el archivo de v&iacute;as. </p>          </td>
        </tr>
        <tr>
          <td class="fila"><div align="center">
            <input type="image" src = "<%=BASEURL%>/images/botones/salir.gif" style="cursor:default ">
          </div></td>
          <td  class="ayudaHtmlTexto">Bot&oacute;n que cierra  la ventana. </td>
        </tr>
        
      </table>
    </td>
  </tr>
</table>
<p>&nbsp;</p>
<p>&nbsp;</p>

<p>&nbsp;</p>
<p>&nbsp; </p>
</body>
</html>
