<%@include file="/WEB-INF/InitModel.jsp"%>
<html>
<head>
<title>Descripcion Campos Eliminación Masiva de Tramos</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link href="../../../css/estilostsp.css" rel="stylesheet" type="text/css">
<link href="<%= BASEURL %>/css/estilostsp.css" rel='stylesheet'>
</head>

<body>
<br>


<table width="696"  border="2" align="center">
  <tr>
    <td width="811" >
      <table width="100%" border="0" align="center">
        <tr  class="subtitulo">
          <td height="24" colspan="2"><div align="center">Eliminaci&oacute;n Masiva de Tramos</div></td>
        </tr>
        <tr class="subtitulo1">
          <td colspan="2">Eliminaci&oacute;n Masiva de Tramos</td>
        </tr>
        <tr>
          <td width="149" class="fila">Pa&iacute;s</td>
          <td width="525"  class="ayudaHtmlTexto"> Campo donde se selecciona el pa&iacute;s donde se encuentra ubicada la ciudad.. </td>
        </tr>
        <tr>
          <td class="fila">Ciudad</td>
          <td  class="ayudaHtmlTexto">Campos para seleccionar la ciudad qu ese desea depurar de los tramos existentes. </td>
        </tr>
        <tr>
          <td class="fila"><div align="center">
            <input type="image" src = "<%=BASEURL%>/images/botones/aceptar.gif" style="cursor:default ">
          </div></td>
          <td  class="ayudaHtmlTexto">Bot&oacute;n que valida el la informaci&oacute;n digitada en los campos para al depuraci&oacute;n masiva de tramos. </td>
        </tr>
        <tr>
          <td class="fila"><div align="center">
            <input type="image" src = "<%=BASEURL%>/images/botones/cancelar.gif" style="cursor:default ">
          </div></td>
          <td  class="ayudaHtmlTexto"> Bot&oacute;n que cancela todas los operaciones realizadas, y resetea la pagina llev&aacute;ndola a su estado inicial. </td>
        </tr>
        <tr>
          <td class="fila"><div align="center">
            <input name="image" type="image" style="cursor:default " src = "<%=BASEURL%>/images/botones/regresar.gif">
          </div></td>
          <td  class="ayudaHtmlTexto">Bot&oacute;n que muestra la pantalla inicial de depuraci&oacute;n de tramos. </td>
        </tr>
        <tr>
          <td class="fila"><div align="center">
            <input type="image" src = "<%=BASEURL%>/images/botones/salir.gif" style="cursor:default ">
          </div></td>
          <td  class="ayudaHtmlTexto">Bot&oacute;n que cierra  la ventana. </td>
        </tr>
        
      </table>
    </td>
  </tr>
</table>
<p>&nbsp;</p>
<p>&nbsp;</p>

<p>&nbsp;</p>
<p>&nbsp; </p>
</body>
</html>
