<!--
- Autor : Ing. Andr�s Maturana De La Cruz
- Date  : 8.05.2006
- Copyrigth Notice : Fintravalores S.A. S.A
-->
<%--
-@(#)
--Descripcion : Pagina JSP, que maneja la depuraci�n masiva de tramos.
--%>
<%@page contentType="text/html"%>
<%@page session="true"%>
<%@page import="java.util.*"%>
<%@page import="com.tsp.operation.model.*"%>
<%@page import="com.tsp.operation.model.beans.*"%>
<%@page import="com.tsp.operation.controller.*"%>
<%@page import="com.tsp.util.*"%>
<%@page errorPage="/error/ErrorPage.jsp"%>
<%@include file="/WEB-INF/InitModel.jsp"%>
<%@ taglib uri="http://jakarta.apache.org/taglibs/input-0.90" prefix="input" %>
<%String path = application.getRealPath(model.menuService.getRealUrl(request.getRequestURI()));
  String datos[] = model.menuService.getContenidoMenu(BASEURL,request.getRequestURI(),path);
%>
<html>
<head>
</script>
<title>Inserci&oacute;n Masiva de Tramos</title>
<link href="<%= BASEURL %>/css/estilostsp.css" rel='stylesheet'>
<link href="../../../../css/estilostsp.css" rel="stylesheet" type="text/css">
<link href="/css/estilostsp.css" rel="stylesheet" type="text/css">
</head>
<script type='text/javascript' src="<%= BASEURL %>/js/validar.js"></script>
<script src='<%= BASEURL %>/js/date-picker.js'></script>
<script type='text/javascript' src="<%= BASEURL %>/js/boton.js"></script>
<script src='<%=BASEURL%>/js/tools.js' language='javascript'></script>
<script>
function validarForma(){
	var pa01 = document.forma.pais01.value;
	var pa02 = document.forma.pais02.value;
	var pa03 = document.forma.pais03.value;
	var pa04 = document.forma.pais03.value;
	var ciud01 = document.forma.ciud01.value;
	var ciud02 = document.forma.ciud02.value;
	var ciud03 = document.forma.ciud03.value;
	var ciud04 = document.forma.ciud04.value;
	
	var eventos = document.getElementsByName("reemp");
	var evento = false;
	var valor = 0;
	for( var i=0; i<eventos.length; i++ ){
	  	if ( eventos[i].checked ){
		   evento = true;
		   valor = eventos[i].value;
		   break;
		}
	}
	
	if( pa01 == pa02 && pa01 != pa03 ){
		alert('No se puede procesar la informaci�n. El punto intermedio debe estar ubicado en el mismo pa�s que el tramo.');
		return false;			
	} else if ( pa01 == pa02 && pa03!='' && pa01!=pa03 && pa04!='' && pa02!=pa04 ){
		alert('No se puede procesar la informaci�n. El punto intermedio debe estar ubicado en el mismo pa�s que el origen o destino del tramo.');
		return false;			
	} else if ( pa01!=pa03 && pa03!=pa02 ){
		alert('No se puede procesar la informaci�n. El punto intermedio debe estar ubicado en el mismo pa�s que el origen o destino del tramo.');
		return false;			
	} else if ( ciud01==ciud03 || ciud02==ciud03 || ciud01==ciud04 || ciud02==ciud04 ){
		alert('No se puede procesar la informaci�n. El punto intermedio no debe ser igual a la ciudad de origen o destino del tramo.');
		return false;			
	}
	
	var radio1 = document.getElementById("PI");
	var radio2 = document.getElementById("TRC");
    for (i = 0; i < forma.elements.length; i++){
				
		if (forma.elements[i].value == "" && forma.elements[i].name!="clientes" 
				&& forma.elements[i].name!="text" && forma.elements[i].name!="tipo"  && forma.elements[i].name!="document"){
			if( radio1.checked && (forma.elements[i].name=="ciud04" || forma.elements[i].name=="pais04") ){
				
			} else { // if  ( radio2.checked ) {
				forma.elements[i].focus();
				alert('No se puede procesar la informaci�n. Campo Vac�o.');
				return false;	
			} 
		}
    }
    return true;
}
function loadCiudades1(){
	//alert('Se escogio ' + document.forma.paises.value);
    forma.action = "<%= CONTROLLER%>?estado=DepTramo&accion=Load&opc=3&pais=" + document.forma.pais01.value;
	forma.submit();
}
function loadCiudades2(){
	//alert('Se escogio ' + document.forma.paises.value);
    forma.action = "<%= CONTROLLER%>?estado=DepTramo&accion=Load&opc=4&pais=" + document.forma.pais02.value;
	forma.submit();
}
function loadCiudades3(){
	//alert('Se escogio ' + document.forma.paises.value);
    forma.action = "<%= CONTROLLER%>?estado=DepTramo&accion=Load&opc=5&pais=" + document.forma.pais03.value;
	forma.submit();
}
function loadCiudades4(){
	//alert('Se escogio ' + document.forma.paises.value);
    forma.action = "<%= CONTROLLER%>?estado=DepTramo&accion=Load&opc=11&pais=" + document.forma.pais03.value;
	forma.submit();
}
</script>
<body onresize="redimensionar()" onload = 'redimensionar()'>
<div id="capaSuperior" style="position:absolute; width:100%; height:110px; z-index:0; left: 0px; top: 0px;">
 <jsp:include page="/toptsp.jsp?encabezado=Inserci�n Masiva de Tramos"/>
</div>
<div id="capaCentral" style="position:absolute; width:100%; height:87%; z-index:0; left: 0px; top: 109px; overflow: scroll;">
<%
	TreeMap paises = model.paisservice.getPaises();            
	TreeMap cit01 = (TreeMap) session.getAttribute("cit01");   
	TreeMap cit02 = (TreeMap) session.getAttribute("cit02");   
	TreeMap cit03 = (TreeMap) session.getAttribute("cit03");   
	TreeMap cit04 = (TreeMap) session.getAttribute("cit04");
	cit01.put(" Seleccione","");
	cit02.put(" Seleccione","");
	cit03.put(" Seleccione","");
	cit04.put(" Seleccione","");
	paises.put(" Seleccione","");
%>
<form name="forma" action='<%=CONTROLLER%>?estado=DepTramo&accion=Insert' id="forma" method="post">
  <table width="500"  border="2" align="center" cellpadding="0" cellspacing="0">
    <tr>
      <td><table width="100%" class="tablaInferior">
        <tr>
          <td colspan="2" ><table width="100%"  border="0" cellpadding="0" cellspacing="1" class="barratitulo">
              <tr>
                <td width="56%" class="subtitulo1">&nbsp;Detalles</td>
                <td width="44%" class="barratitulo"><img align="left" src="<%=BASEURL%>/images/titulo.gif" width="32" height="20"><%=datos[0]%></td>
              </tr>
          </table></td>
        </tr>
        <tr class="fila">
          <td colspan="2" valign="middle" class="subtitulo1">Origen del Tramo </td>
          </tr>
        <tr class="fila">
          <td valign="middle">Pa&iacute;s</td>
          <td nowrap> <input:select name="pais01" attributesText="class=textbox onChange='loadCiudades1();'" options="<%=paises %>"/></td>
        </tr>
        <tr class="fila">
          <td>Ciudad</td>
          <td nowrap><table width="100%"  border="0" class="tablaInferior">
              <tr>
                <td><input name="text" type="text" class="textbox" id="campo" style="width:200;" onKeyUp="buscar(document.forma.ciud01,this)" size="15" ></td>
              </tr>
              <tr>
                <td><input:select name="ciud01" attributesText="class=textbox" options="<%=cit01 %>"/>&nbsp;</td>
              </tr>
          </table></td>
        </tr>
        <tr class="fila">
          <td colspan="2" valign="middle" class="subtitulo1">Destino del Tramo </td>
          </tr>
        <tr class="fila">
          <td valign="middle">Pa&iacute;s</td>
          <td nowrap> <input:select name="pais02" attributesText="class=textbox onChange='loadCiudades2();'" options="<%=paises %>"/></td>
        </tr>
        <tr class="fila">
          <td>Ciudad</td>
          <td nowrap><table width="100%"  border="0" class="tablaInferior">
              <tr>
                <td><input name="text" type="text" class="textbox" id="campo" style="width:200;" onKeyUp="buscar(document.forma.ciud02,this)" size="15" ></td>
              </tr>
              <tr>
                <td><input:select name="ciud02" attributesText="class=textbox" options="<%=cit02 %>"/>&nbsp;</td>
              </tr>
          </table></td>
        </tr>
        <tr class="fila">
          <td colspan="2" valign="middle" class="subtitulo1">Nuevo Punto/Tramo Intermedio </td>
        </tr>
        <tr class="fila">
          <td colspan="2" valign="middle" class="subtitulo1">Nuevo Punto Intermedio /Origen del Tramo Intermedio </td>
          </tr>
        <tr class="fila">
          <td width="27%" valign="middle">Pa&iacute;s</td>
          <td width="73%" nowrap>
            <input:select name="pais03" attributesText="class=textbox onChange='loadCiudades3();'" options="<%=paises %>"/></td>
        </tr>
        <tr class="fila">
          <td>Ciudad</td>
          <td nowrap><table width="100%"  border="0" class="tablaInferior">
            <tr>
              <td><input name="text" type="text" class="textbox" id="campo" style="width:200;" onKeyUp="buscar(document.forma.ciud03,this)" size="15" ></td>
            </tr>
            <tr>
              <td><input:select name="ciud03" attributesText="class=textbox" options="<%=cit03 %>"/>&nbsp;</td>
            </tr>
          </table></td>
        </tr>
        <tr class="fila">
          <td colspan="2" class="subtitulo1">Destino del Tramo Intermedio</td>
          </tr>
        <tr class="fila">
          <td valign="middle">Pa&iacute;s</td>
          <td nowrap> <input:select name="pais04" attributesText="class=textbox onChange='loadCiudades4();'" options="<%=paises %>"/></td>
        </tr>
        <tr class="fila">
          <td>Ciudad</td>
          <td nowrap><table width="100%"  border="0" class="tablaInferior">
              <tr>
                <td><input name="text" type="text" class="textbox" id="campo" style="width:200;" onKeyUp="buscar(document.forma.ciud04,this)" size="15" ></td>
              </tr>
              <tr>
                <td><input:select name="ciud04" attributesText="class=textbox" options="<%=cit04 %>"/>&nbsp;</td>
              </tr>
          </table></td>
        </tr>
        <tr class="fila">
          <td colspan="2" class="subtitulo1">Opciones de Inserci&oacute;n</td>
          </tr>
        <tr class="fila">
          <td>Insertar</td>
          <td nowrap><table width="100%"  border="0">
            <tr class="letra">
              <td width="25%"><input name="insert" type="radio" value="PI" id = "PI" checked>
      Punto Intermedio </td>
              <td width="44%"><input name="insert" type="radio" value="TRC" id = "TRC">
      Tramo Completo</td>
            </tr>
          </table></td>
        </tr>
      </table></td>
    </tr>
  </table>
  <br>
  
  <br>
  <center>
    <img src="<%=BASEURL%>/images/botones/aceptar.gif" name="c_aceptar" onClick="if (validarForma()) forma.submit(); " onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" style="cursor:hand"> 
    <img src="<%=BASEURL%>/images/botones/cancelar.gif" name="c_cancelar" onClick="forma.reset();" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" style="cursor:hand"> 
	<img src="<%=BASEURL%>/images/botones/regresar.gif" name="c_regresar" id="c_regresar" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" onClick="window.location.href='<%=CONTROLLER%>?estado=Menu&accion=Cargar&carpeta=/jsp/trafico/tramo/depuracion&pagina=start.jsp&marco=no&opcion=33&item=7'"> 
    <img src="<%=BASEURL%>/images/botones/salir.gif" name="c_salir" onClick="parent.close();" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" style="cursor:hand">
  </center>
</form>
<% if( request.getParameter("msg") != null ){%>
  <p><table border="2" align="center">
  <tr>
    <td><table width="100%"  border="1" align="center"  bordercolor="#F7F5F4" bgcolor="#FFFFFF">
      <tr>
        <td width="363" align="center" class="mensajes"><%=request.getParameter("msg").toString()%></td>
        <td width="29" background="<%=BASEURL%>/images/cuadronaranja.JPG">&nbsp;</td>
        <td width="58">&nbsp;</td>
      </tr>
    </table></td>
  </tr>
</table>
</p>
 <%} %>
 <% if( request.getParameter("error") != null ){%>
  <p><table border="2" align="center">
  <tr>
    <td><table width="100%"  border="1" align="center"  bordercolor="#F7F5F4" bgcolor="#FFFFFF">
      <tr>
        <td width="363" align="center" class="mensajes"><%=request.getParameter("error").toString()%></td>
        <td width="29" background="<%=BASEURL%>/images/cuadronaranja.JPG">&nbsp;</td>
        <td width="51">&nbsp;</td>
      </tr>
    </table></td>
  </tr>
</table>
<%
if(request.getAttribute("notramo")!=null){
%>	
</p><table border="2" align="center" id="tabla1" >
    <tr>
        <td >
<table width="479"  border="1" align="center" borderColor="#999999" bgcolor="#F7F5F4" id="tabla3">
  <tr class="tblTitulo" id="titulos">
    <td colspan="2" ><div align="center">Origen</div></td>
    <td colspan="2" ><div align="center">Destino</div></td>
    <td >&nbsp;</td>
  </tr>
  <tr class="tblTitulo" id="titulos">
    <td ><div align="center">Ciudad</div></td>
    <td ><div align="center">Pa&iacute;s</div></td>
    <td ><div align="center">Ciudad</div></td>
    <td ><div align="center">Pa&iacute;s</div></td>
    <td >&nbsp;</td>
  </tr>
<%
Vector info = (Vector) request.getAttribute("notramo");
for( int i=0; i<info.size(); i++){
	Hashtable ht = (Hashtable) info.elementAt(i);
	String link = CONTROLLER + "?estado=Cargar&accion=Variostramo&carpeta=tramo&pagina=ingresarTramo.jsp"
			+ "&marco=no&opcion=26"
			+ "&c_paiso=" + ht.get("paiso")
			+ "&c_origen=" + ht.get("ciuo")
			+ "&c_paisd=" + ht.get("paisd")
			+ "&c_destino=" +  ht.get("ciud")
			+ "&c_comb=&c_distancia=&c_duracion=";
%>	
  <tr class="<%=(i % 2 == 0 )?"filagris":"filaazul"%>">
    <td nowrap class="bordereporte"><%= ht.get("nciuo") %></td>
    <td nowrap class="bordereporte"><%= ht.get("npaiso") %></td>
    <td nowrap class="bordereporte"><%= ht.get("nciud") %></td>
    <td nowrap class="bordereporte"><%= ht.get("npaisd") %></td>
    <td nowrap class="bordereporte"><div align="center"><a href="JavaScript:void(0);"><img src="<%=BASEURL%>/images/botones/iconos/mas.gif" alt="Agregar tramo." width="15" height="15" border="0" onClick="window.open('<%= link %>','nuTramo','status=yes,scrollbars=yes,width=780,height=450,resizable=yes');"></a></div></td>
  </tr>

<%
}
%>	  
</table>
        </td>
    </tr>
</table> 
<p>
    <%}%>
</p>
<p>
<p>&nbsp; </p>
<p>
<%}%>
  <%
if(request.getAttribute("vias")!=null){
%>
</p>
<p></p>
<table border="2" align="center" id="tabla1" >
  <tr>
    <td >
      <table width="479"  border="1" align="center" borderColor="#999999" bgcolor="#F7F5F4" id="tabla3">
        <tr class="tblTitulo" id="titulos">
          <td colspan="6" ><div align="center">V&iacute;as modificadas </div></td>
        </tr>
        <tr class="tblTitulo" id="titulos">
          <td colspan="2"><div align="center">Origen</div></td>
          <td colspan="2"  ><div align="center">Destino</div></td>
          <td ><div align="center">Secuencia</div></td>
          <td ><div align="center">Descripci&oacute;n</div></td>
        </tr>
        <%
Vector info = (Vector) request.getAttribute("vias");
for( int i=0; i<info.size(); i++){
	Hashtable ht = (Hashtable) info.elementAt(i);//ht.get("")
%>
        <tr class="<%=(i % 2 == 0 )?"filagris":"filaazul"%>">
          <td width="30" nowrap class="bordereporte"><%= ht.get("orig") %></td>
          <td nowrap class="bordereporte"><%= ht.get("norig") %></td>
          <td width="30" nowrap class="bordereporte"><%= ht.get("dest") %></td>
          <td nowrap class="bordereporte"><%= ht.get("ndest") %></td>
          <td nowrap class="bordereporte"><%= ht.get("sec") %></td>
          <td nowrap class="bordereporte"><%= ht.get("desc") %></td>
        </tr>
        <%
}
%>
    </table></td>
  </tr>
</table>
<%}%>
 </p>
</div>
<%=datos[1]%>
</body>
</html>
