<!--
- Autor : Ing. Andr�s Maturana De La Cruz
- Date  : 8.05.2006
- Copyrigth Notice : Fintravalores S.A. S.A
-->
<%--
-@(#)
--Descripcion : Pagina JSP, que maneja la depuraci�n masiva de tramos.
--%>
<%@page contentType="text/html"%>
<%@page session="true"%>
<%@page import="java.util.*"%>
<%@page import="com.tsp.operation.model.*"%>
<%@page import="com.tsp.operation.model.beans.*"%>
<%@page import="com.tsp.operation.controller.*"%>
<%@page import="com.tsp.util.*"%>
<%@page errorPage="/error/ErrorPage.jsp"%>
<%@include file="/WEB-INF/InitModel.jsp"%>
<%@ taglib uri="http://jakarta.apache.org/taglibs/input-0.90" prefix="input" %>
<%String path = application.getRealPath(model.menuService.getRealUrl(request.getRequestURI()));
  String datos[] = model.menuService.getContenidoMenu(BASEURL,request.getRequestURI(),path);
%>
<html>
<head>
</script>
<title>Eliminaci&oacute;n Masiva de Tramos</title>
<link href="<%= BASEURL %>/css/estilostsp.css" rel='stylesheet'>
<link href="../../../../css/estilostsp.css" rel="stylesheet" type="text/css">
<link href="/css/estilostsp.css" rel="stylesheet" type="text/css">
</head>
<script type='text/javascript' src="<%= BASEURL %>/js/validar.js"></script>
<script src='<%= BASEURL %>/js/date-picker.js'></script>
<script type='text/javascript' src="<%= BASEURL %>/js/boton.js"></script>
<script src='<%=BASEURL%>/js/tools.js' language='javascript'></script>
<script>
function validarForma(){
    for (i = 0; i < forma.elements.length; i++){
            if (forma.elements[i].value == "" && forma.elements[i].name!="clientes" 
					&& forma.elements[i].name!="text" && forma.elements[i].name!="tipo"  && forma.elements[i].name!="document" ){
                    forma.elements[i].focus();
                    alert('No se puede procesar la informaci�n. Campo Vac�o.');
                    return false;	
            }
    }
    return true;
}
function loadCiudades(){
	//alert('Se escogio ' + document.forma.paises.value);
    forma.action = "<%= CONTROLLER%>?estado=DepTramo&accion=LoadCities&pais=" + document.forma.paises.value;
	forma.submit();
}
</script>
<body onresize="redimensionar()" onload = 'redimensionar()'>
<div id="capaSuperior" style="position:absolute; width:100%; height:110px; z-index:0; left: 0px; top: 0px;">
 <jsp:include page="/toptsp.jsp?encabezado=Eliminaci�n Masiva de Tramos"/>
</div>
<div id="capaCentral" style="position:absolute; width:100%; height:87%; z-index:0; left: 0px; top: 109px; overflow: scroll;">
<%
	TreeMap paises = model.paisservice.getPaises();            
	TreeMap ciudades = model.ciudadService.getCiudadTM();
	ciudades.put(" Seleccione","");
	paises.put(" Seleccione","");
%>
<form name="forma" action='<%=CONTROLLER%>?estado=Dep&accion=Tramo' id="forma" method="post">
  <table width="500"  border="2" align="center" cellpadding="0" cellspacing="0">
    <tr>
      <td><table width="100%" class="tablaInferior">
        <tr>
          <td colspan="2" ><table width="100%"  border="0" cellpadding="0" cellspacing="1" class="barratitulo">
              <tr>
                <td width="56%" class="subtitulo1">&nbsp;Detalles</td>
                <td width="44%" class="barratitulo"><img align="left" src="<%=BASEURL%>/images/titulo.gif" width="32" height="20"><%=datos[0]%></td>
              </tr>
          </table></td>
        </tr>
        <tr class="fila">
          <td width="27%" valign="middle">Pa&iacute;s</td>
          <td width="73%" nowrap>
            <input:select name="paises" attributesText="class=textbox onChange='loadCiudades();'" options="<%=paises %>"/></td>
        </tr>
        <tr class="fila">
          <td>Ciudad</td>
          <td nowrap><table width="100%"  border="0" class="tablaInferior">
            <tr>
              <td><input name="text" type="text" class="textbox" id="campo" style="width:200;" onKeyUp="buscar(document.forma.ciudades,this)" size="15" ></td>
            </tr>
            <tr>
              <td><input:select name="ciudades" attributesText="class=textbox" options="<%=ciudades %>"/>&nbsp;</td>
            </tr>
          </table></td>
        </tr>
      </table></td>
    </tr>
  </table>
  <br>
  
  <br>
  <center>
    <img src="<%=BASEURL%>/images/botones/aceptar.gif" name="c_aceptar" onClick="if (validarForma()) forma.submit(); " onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" style="cursor:hand"> 
    <img src="<%=BASEURL%>/images/botones/cancelar.gif" name="c_cancelar" onClick="forma.reset();" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" style="cursor:hand"> 
    <img src="<%=BASEURL%>/images/botones/regresar.gif" name="c_regresar" id="c_regresar" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" onClick="window.location.href='<%=CONTROLLER%>?estado=Menu&accion=Cargar&carpeta=/jsp/trafico/tramo/depuracion&pagina=start.jsp&marco=no&opcion=33&item=7'"> <img src="<%=BASEURL%>/images/botones/salir.gif" name="c_salir" onClick="parent.close();" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" style="cursor:hand"> 
  </center>
</form>
<% if( request.getParameter("msg") != null ){%>
  <p><table border="2" align="center">
  <tr>
    <td><table width="100%"  border="1" align="center"  bordercolor="#F7F5F4" bgcolor="#FFFFFF">
      <tr>
        <td width="363" align="center" class="mensajes"><%=request.getParameter("msg").toString()%></td>
        <td width="29" background="<%=BASEURL%>/images/cuadronaranja.JPG">&nbsp;</td>
        <td width="58">&nbsp;</td>
      </tr>
    </table></td>
  </tr>
</table>
</p>
 <%} %>
 <% if( request.getParameter("error") != null ){%>
  <p><table border="2" align="center">
  <tr>
    <td><table width="100%"  border="1" align="center"  bordercolor="#F7F5F4" bgcolor="#FFFFFF">
      <tr>
        <td width="363" align="center" class="mensajes"><%=request.getParameter("error").toString()%></td>
        <td width="29" background="<%=BASEURL%>/images/cuadronaranja.JPG">&nbsp;</td>
        <td width="51">&nbsp;</td>
      </tr>
    </table></td>
  </tr>
</table>
<%
if(request.getAttribute("notramo")!=null){
%>	
</p><table border="2" align="center" id="tabla1" >
    <tr>
        <td >
<table width="479"  border="1" align="center" borderColor="#999999" bgcolor="#F7F5F4" id="tabla3">
  <tr class="tblTitulo" id="titulos">
    <td colspan="2" ><div align="center">Origen</div></td>
    <td colspan="2" ><div align="center">Destino</div></td>
    <td >&nbsp;</td>
  </tr>
  <tr class="tblTitulo" id="titulos">
    <td ><div align="center">Ciudad</div></td>
    <td ><div align="center">Pa&iacute;s</div></td>
    <td ><div align="center">Ciudad</div></td>
    <td ><div align="center">Pa&iacute;s</div></td>
    <td >&nbsp;</td>
  </tr>
<%
Vector info = (Vector) request.getAttribute("notramo");
for( int i=0; i<info.size(); i++){
	Hashtable ht = (Hashtable) info.elementAt(i);
	String link = CONTROLLER + "?estado=Cargar&accion=Variostramo&carpeta=tramo&pagina=ingresarTramo.jsp"
			+ "&marco=no&opcion=26"
			+ "&c_paiso=" + ht.get("paiso")
			+ "&c_origen=" + ht.get("ciuo")
			+ "&c_paisd=" + ht.get("paisd")
			+ "&c_destino=" +  ht.get("ciud")
			+ "&c_comb=&c_distancia=&c_duracion=";
%>	
  <tr class="<%=(i % 2 == 0 )?"filagris":"filaazul"%>">
    <td nowrap class="bordereporte"><%= ht.get("nciuo") %></td>
    <td nowrap class="bordereporte"><%= ht.get("npaiso") %></td>
    <td nowrap class="bordereporte"><%= ht.get("nciud") %></td>
    <td nowrap class="bordereporte"><%= ht.get("npaisd") %></td>
    <td nowrap class="bordereporte"><div align="center"><a href="JavaScript:void(0);"><img src="<%=BASEURL%>/images/botones/iconos/mas.gif" alt="Agregar tramo." width="15" height="15" border="0" onClick="window.open('<%= link %>','nuTramo','status=yes,scrollbars=yes,width=780,height=450,resizable=yes');"></a></div></td>
  </tr>

<%
}
%>	  
</table>
        </td>
    </tr>
</table> 
<p>
    <%}%>
</p>
<p>
<p>&nbsp; </p>
<p>
<%}%>
  <%
if(request.getAttribute("vias")!=null){
%>
</p>
<p></p>
<table border="2" align="center" id="tabla1" >
  <tr>
    <td >
      <table width="479"  border="1" align="center" borderColor="#999999" bgcolor="#F7F5F4" id="tabla3">
        <tr class="tblTitulo" id="titulos">
          <td colspan="6" ><div align="center">V&iacute;as modificadas </div></td>
        </tr>
        <tr class="tblTitulo" id="titulos">
          <td colspan="2"><div align="center">Origen</div></td>
          <td colspan="2"  ><div align="center">Destino</div></td>
          <td ><div align="center">Secuencia</div></td>
          <td ><div align="center">Descripci&oacute;n</div></td>
        </tr>
        <%
Vector info = (Vector) request.getAttribute("vias");
for( int i=0; i<info.size(); i++){
	Hashtable ht = (Hashtable) info.elementAt(i);//ht.get("")
%>
        <tr class="<%=(i % 2 == 0 )?"filagris":"filaazul"%>">
          <td width="30" nowrap class="bordereporte"><%= ht.get("orig") %></td>
          <td nowrap class="bordereporte"><%= ht.get("norig") %></td>
          <td width="30" nowrap class="bordereporte"><%= ht.get("dest") %></td>
          <td nowrap class="bordereporte"><%= ht.get("ndest") %></td>
          <td nowrap class="bordereporte"><%= ht.get("sec") %></td>
          <td nowrap class="bordereporte"><%= ht.get("desc") %></td>
        </tr>
        <%
}
%>
    </table></td>
  </tr>
</table>
<%}%>
 </p>
</div>
<%=datos[1]%>
</body>
</html>
