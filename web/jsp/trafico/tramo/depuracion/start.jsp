<!--
- Autor : Ing. Andr�s Maturana De La Cruz
- Date  : 8.05.2006
- Copyrigth Notice : Fintravalores S.A. S.A
-->
<%--
-@(#)
--Descripcion : Pagina JSP, que maneja la depuraci�n masiva de tramos.
--%>
<%@page contentType="text/html"%>
<%@page session="true"%>
<%@page import="java.util.*"%>
<%@page import="com.tsp.operation.model.*"%>
<%@page import="com.tsp.operation.model.beans.*"%>
<%@page import="com.tsp.operation.controller.*"%>
<%@page import="com.tsp.util.*"%>
<%@page errorPage="/error/ErrorPage.jsp"%>
<%@include file="/WEB-INF/InitModel.jsp"%>
<%@ taglib uri="http://jakarta.apache.org/taglibs/input-0.90" prefix="input" %>
<%String path = application.getRealPath(model.menuService.getRealUrl(request.getRequestURI()));
  String datos[] = model.menuService.getContenidoMenu(BASEURL,request.getRequestURI(),path);
%>
<html>
<head>
</script>
<title>Depuraci&oacute;n Masiva de Tramos</title>
<link href="<%= BASEURL %>/css/estilostsp.css" rel='stylesheet'>
<link href="../../../../css/estilostsp.css" rel="stylesheet" type="text/css">
<link href="/css/estilostsp.css" rel="stylesheet" type="text/css">
</head>
<script type='text/javascript' src="<%= BASEURL %>/js/validar.js"></script>
<script src='<%= BASEURL %>/js/date-picker.js'></script>
<script type='text/javascript' src="<%= BASEURL %>/js/boton.js"></script>
<script src='<%=BASEURL%>/js/tools.js' language='javascript'></script>
<script>
function validarForma(){
    for (i = 0; i < forma.elements.length; i++){
            if (forma.elements[i].value == "" && forma.elements[i].name!="clientes" 
					&& forma.elements[i].name!="text" && forma.elements[i].name!="tipo"  && forma.elements[i].name!="document" ){
                    forma.elements[i].focus();
                    alert('No se puede procesar la informaci�n. Campo Vac�o.');
                    return false;	
            }
    }
    return true;
}
function loadCiudades(){
	//alert('Se escogio ' + document.forma.paises.value);
    forma.action = "<%= CONTROLLER%>?estado=DepTramo&accion=LoadCities&pais=" + document.forma.paises.value;
	forma.submit();
}
</script>
<body onresize="redimensionar()" onload = 'redimensionar()'>
<div id="capaSuperior" style="position:absolute; width:100%; height:110px; z-index:0; left: 0px; top: 0px;">
 <jsp:include page="/toptsp.jsp?encabezado=Depuraci�n Masiva de Tramos"/>
</div>
<div id="capaCentral" style="position:absolute; width:100%; height:87%; z-index:0; left: 0px; top: 109px; overflow: scroll;">
<%
	TreeMap paises = model.paisservice.getPaises();            
	TreeMap ciudades = model.ciudadService.getCiudadTM();
	ciudades.put(" Seleccione","");
	paises.put(" Seleccione","");
%>
<form name="forma" action='<%=CONTROLLER%>?estado=Dep&accion=Tramo' id="forma" method="post">
  <table width="330"  border="2" align="center" cellpadding="0" cellspacing="0">
    <tr>
      <td><table width="100%" class="tablaInferior">
        <tr>
          <td colspan="2" ><table width="100%"  border="0" cellpadding="0" cellspacing="1" class="barratitulo">
              <tr>
                <td width="56%" class="subtitulo1">&nbsp;Seleccione la opci&oacute;n: </td>
                <td width="44%" class="barratitulo"><img align="left" src="<%=BASEURL%>/images/titulo.gif" width="32" height="20"><%=datos[0]%></td>
              </tr>
          </table></td>
        </tr>
        <tr class="letra">
          <td width="9%" valign="middle"><span class="barratitulo"><a href="<%= CONTROLLER%>?estado=DepTramo&accion=Load&opc=1"><img src="<%=BASEURL%>/images/botones/iconos/sig.gif" alt="Eliminaci&oacute;n masiva de tramos." border="0" align="left"></a></span></td>
          <td width="91%" nowrap >Eliminaci&oacute;n Masiva</td>
        </tr>
        <tr class="letra">
          <td><span class="barratitulo"><img align="left" src="<%=BASEURL%>/images/botones/iconos/sig.gif" border="0" onClick="window.location= '<%= CONTROLLER%>?estado=DepTramo&accion=Load&opc=2';" style="cursor:hand"></span></td>
          <td nowrap class="letra">Inserci&oacute;n Masiva</td>
        </tr>
        <tr class="letra">
          <td><span class="barratitulo"><img align="left" src="<%=BASEURL%>/images/botones/iconos/sig.gif" border="0" onClick="window.location= '<%= CONTROLLER%>?estado=DepTramo&accion=Load&opc=6';" style="cursor:hand"></span></td>
          <td nowrap class="letra">Reemplazo Masivo</td>
        </tr>
      </table></td>
    </tr>
  </table>
  <br>
  
  <br>
  <center> 
    <img src="<%=BASEURL%>/images/botones/salir.gif" name="c_salir" onClick="parent.close();" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" style="cursor:hand">
  </center>
</form>
</div>
<%=datos[1]%>
</body>
</html>
