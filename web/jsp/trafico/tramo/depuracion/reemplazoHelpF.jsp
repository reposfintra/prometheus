<%@ include file="/WEB-INF/InitModel.jsp"%>

<HTML>
<HEAD>
<TITLE>Eliminaci&oacute;n Masiva de Tramos</TITLE>
<META http-equiv=Content-Type content="text/html; charset=windows-1252">
<link href="../css/estilostsp.css" rel="stylesheet" type="text/css"> 
<link href="<%=BASEURL%>/css/estilostsp.css" rel="stylesheet" type="text/css"> 
</HEAD>
<BODY>  

  <table width="95%"  border="2" align="center">
    <tr>
      <td>
        <table width="100%" border="0" align="center">
          <tr  class="subtitulo">
            <td height="20"><div align="center">Reemplazo Masivo de Tramos</div></td>
          </tr>
          <tr class="subtitulo1">
            <td>Descripci&oacute;n del funcionamiento del programa de depuraci&oacute;n masiva de tramos. </td>
          </tr>
          <tr>
            <td  height="18"  class="ayudaHtmlTexto">Formulario de Reemplazo Masivo de Tramos. </td>
          </tr>
          <tr>
            <td height="18"  class="ayudaHtmlTexto"><div align="center"><img src="<%= BASEURL%>/images/ayuda/trafico/tramo/depuracion/Dibujo6.PNG"></div></td>
          </tr>
          <tr>
            <td  class="ayudaHtmlTexto">&nbsp;</td>
          </tr>
          <tr>
            <td  class="ayudaHtmlTexto"><p>El sistema verifica que no hayan campos vacios. </p></td>
          </tr>
          <tr>
            <td  class="ayudaHtmlTexto"><div align="center"><img src="<%= BASEURL%>/images/ayuda/trafico/tramo/depuracion/Dibujo2.PNG"></div></td>
          </tr>
          <tr>
            <td  class="ayudaHtmlTexto">&nbsp;</td>
          </tr>
          <tr>
            <td  class="ayudaHtmlTexto"><p>Al llenar la informaci&oacute;n debidamente y presionar ACEPTAR, el programa iniciar&aacute; la depuraci&oacute;n masiva de tramos. Si el proceso es exitoso presentar&aacute; un mensaje indicando que la depuraci&oacute;n masiva de tramos se realiz&oacute; satisfactoriamente. A continuaci&oacute;n se mostrar&aacute; un listado de todas las v&iacute;as que se modificaron.</p></td>
          </tr>
          <tr>
            <td  class="ayudaHtmlTexto"><div align="center"><img src="<%= BASEURL%>/images/ayuda/trafico/tramo/depuracion/Dibujo8.PNG"></div></td>
          </tr>
          <tr>
            <td  class="ayudaHtmlTexto">&nbsp;</td>
          </tr>
          <tr>
            <td  class="ayudaHtmlTexto">Cuandoel tramo seleccionado no existe el programa mostrar&aacute; un mensaje indicando que la depuraci&oacute;n masiva ha fallado y se necesita crear el tramo:</td>
          </tr>
          <tr>
            <td  class="ayudaHtmlTexto"><div align="center"><img src="<%= BASEURL%>/images/ayuda/trafico/tramo/depuracion/Dibujo12.PNG"></div></td>
          </tr>
          <tr>
            <td  class="ayudaHtmlTexto">&nbsp;</td>
          </tr>
          <tr>
            <td  class="ayudaHtmlTexto">Si el programa encuentra que al reemplazar el origen o destino de un tramo, el nuevo tramo ya existe, se presentar&aacute; un mensaje indicando qu ela depuraci&oacute;n ha sido fallida. </td>
          </tr>
          <tr>
            <td  class="ayudaHtmlTexto"><div align="center"><img src="<%= BASEURL%>/images/ayuda/trafico/tramo/depuracion/Dibujo11.PNG"></div></td>
          </tr>
          <tr>
            <td  class="ayudaHtmlTexto">&nbsp;</td>
          </tr>
          
          
          
      </table></td>
    </tr>
  </table>
</BODY>
</HTML>
