<!--
- Date  : 2005
- Autor: Ing Jesus Cuestas
- Modificado Ing Sandra Escalante
- Copyrigth Notice : Fintravalores S.A. S.A
-->    
<%--
-@(#)
--Descripcion : Pagina JSP, que muestra el listado de los tramos
--%> 
<%@page contentType="text/html"%>
<%@page session="true"%>
<%@page import="java.util.*"%>
<%@page import="com.tsp.operation.model.*"%>
<%@page import="com.tsp.operation.model.beans.*"%>
<%@page import="com.tsp.util.*"%>
<%@include file="/WEB-INF/InitModel.jsp"%>
<%@ taglib uri="http://jsptags.com/tags/navigation/pager" prefix="pg" %>
<html>
<head>
<title>.: Tramos</title>
<link href="<%= BASEURL %>/css/estilostsp.css" rel='stylesheet'> 
<link href="../css/estilostsp.css" rel="stylesheet" type="text/css">
<link href="../../../css/estilostsp.css" rel="stylesheet" type="text/css">
<script type='text/javascript' src="<%= BASEURL %>/js/reporte.js"></script>
<script type='text/javascript' src="<%= BASEURL %>/js/boton.js"></script>
</head>
<body onResize="redimensionar()" onLoad="redimensionar()">
<div id="capaSuperior" style="position:absolute; width:100%; height:100px; z-index:0; left: 0px; top: 0px;">
<jsp:include page="/WEB-INF/toptsp.jsp?encabezado=Tramos"/>
</div>
<div id="capaCentral" style="position:absolute; width:100%; height:85%; z-index:0; left: 0px; top: 100px; overflow: scroll;"> 
<%String style = "simple";
String position = "bottom";
String index = "center";
int maxPageItems = 30;
int maxIndexPages = 10;

Vector tramos = model.tramoService.getTramos();
%>
<table width="1000" border="2" align="center">
  <tr>
    <td>  
      <table width="100%" border="0" align="center">
			<tr>
   				<td width="110"  height="24"  class="subtitulo1"><p align="left">Tramos</p></td>
            	<td width="868"  class="barratitulo"><img src="<%=BASEURL%>/images/titulo.gif" width="32" height="20"></td>						
            </tr>
	  </table>			
	  <table width="99%" border="1" align="center" bordercolor="#999999" bgcolor="#F7F5F4">   
      <tr class="tblTitulo" align="center" height="20">
        <td width="6%">Distrito</td>
        <td width="20%">Origen</td>
        <td width="20%">Destino</td>
        <td width="17%">Combustible</td>
        <td width="20%">Distancia</td>
        <td width="17%">Duracion</td>
      </tr>
      <pg:pager    items="<%=tramos.size()%>"
    index="<%= index %>"
    maxPageItems="<%= maxPageItems %>"
    maxIndexPages="<%= maxIndexPages %>"
    isOffset="<%= true %>"
    export="offset,currentPageNumber=pageNumber"
    scope="request">
      <% 
     for (int i = offset.intValue(), l = Math.min(i + maxPageItems, tramos.size()); i < l; i++){
        Tramo tramo = (Tramo) tramos.elementAt(i);
  %>
      <pg:item>
      <tr class="<%=(i % 2 == 0 )?"filagris":"filaazul"%>" onMouseOver='cambiarColorMouse(this)'   style="cursor:hand" title="Mostrar informacion del tramo" onClick="window.open('<%=CONTROLLER%>?estado=Buscar&accion=Tramos&carpeta=jsp/trafico/tramo&pagina=maTramo.jsp&origen=<%=tramo.getOrigen()%>&destino=<%=tramo.getDestino()%>&cia=<%=tramo.getDstrct()%>&sw=3&lista=ok&marco=no','','status=yes,scrollbars=no,width=650,height=600,resizable=yes')">
        <td height="22" align="center" class="bordereporte"><%=tramo.getDstrct()%></td>
        <td  align="center" class="bordereporte"><%=(tramo.getPaiso()!=null)?tramo.getPaiso():""%> - <%=tramo.getNorigen()%></td>
        <td  align="center" class="bordereporte"><%=(tramo.getPaisd()!=null)?tramo.getPaisd():""%> - <%=tramo.getNdestino()%></td>
        <td  align="center" class="bordereporte"><%=tramo.getConsumo_combustible()%> <%=(tramo.getUnidad_consumo()!=null)?tramo.getUnidad_consumo():""%></td>
        <td  align="center" class="bordereporte"><%=tramo.getDistancia()%> <%=(tramo.getUnidad_distancia()!=null)?tramo.getUnidad_distancia():""%></td>
        <td  align="center" class="bordereporte"><%=tramo.getDuracion()%> <%=(tramo.getUnidad_duracion()!=null)?tramo.getUnidad_duracion():""%></td>
      </tr>
      </pg:item>
      <%}%>
      <tr bordercolor="#E6E6E6" align="center" class="filagris">
        <td height="20" colspan="6" align="center"> <pg:index>
          <jsp:include page="/WEB-INF/jsp/googlesinimg.jsp" flush="true"/>    
    </pg:index> </td>
      </tr>
      </pg:pager>
    </table></td>    
  </tr>
</table>
<br>

<table width="1000" border="0" align="center">
    <tr align="left">
      <td><img src="<%=BASEURL%>/images/botones/regresar.gif"  name="imgaceptar" onclick="window.location='<%=CONTROLLER%>?estado=Menu&accion=Cargar&carpeta=/jsp/trafico/tramo&pagina=buscarTramo.jsp&marco=no'" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" style="cursor:hand"> <img src="<%=BASEURL%>/images/botones/salir.gif"  name="imgaceptar" onclick="window.close();" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" style="cursor:hand"></td>
    </tr>
  </table> 
</div>
</body>
</html>
