<!--
- Date  : 2005
- Autor: Ing Jesus Cuestas
- Modificado Ing Sandra Escalante
- Copyrigth Notice : Fintravalores S.A. S.A
-->
<%--    
-@(#)
--Descripcion : Pagina JSP, que muestra los datos de entrada para 
--              ingresar un tramo
--%>   
<%@page contentType="text/html"%>
<%@page session="true"%>
<%@page import="java.util.*"%>
<%@page import="com.tsp.operation.model.*"%>
<%@page import="com.tsp.operation.model.beans.*"%>
<%@page import="com.tsp.util.*"%>
<%@include file="/WEB-INF/InitModel.jsp"%>
<html>
<head> 
<title>.: Ingresar Tramo</title>
<link href="<%= BASEURL %>/css/estilostsp.css" rel='stylesheet'> 
<link href="../../../css/estilostsp.css" rel="stylesheet" type="text/css">
</head>
<script type='text/javascript' src="<%= BASEURL %>/js/validar.js"></script>
<script type='text/javascript' src="<%= BASEURL %>/js/funciones.js"></script>
<script type='text/javascript' src="<%= BASEURL %>/js/boton.js"></script>

<%	//INICIALIZAMOS VARIABLES...
	String cpaiso = "", cestadoo = "", origen = "", cpaisd = "", cestadod = "", destino = "", combustible = "", distancia = "", duracion = "", ccombustible = "", cdistancia = "", cduracion = "", ccia=""; 
	String dstrct = (String) session.getAttribute("Distrito");
       if(request.getParameter("sw")!=null){
		cpaiso = request.getParameter("c_paiso")        !=null?request.getParameter("c_paiso"):"";
		origen = request.getParameter("c_origen")       !=null?request.getParameter("c_origen"):"";
                cpaisd = request.getParameter("c_paisd")        !=null?request.getParameter("c_paisd"):"";		
		combustible = request.getParameter("c_comb")    !=null?request.getParameter("c_comb"):""; 
		distancia = request.getParameter("c_distancia") !=null?request.getParameter("c_distancia"):""; 
		duracion = request.getParameter("c_duracion")   !=null?request.getParameter("c_duracion"):""; 
		ccombustible = request.getParameter("c_unidadc")!=null?request.getParameter("c_unidadc"):""; 
		cdistancia = request.getParameter("c_unidadd")  !=null?request.getParameter("c_unidadd"):""; 
		cduracion = request.getParameter("c_unidaddu")  !=null?request.getParameter("c_unidaddu"):""; 
		ccia = request.getParameter("c_cia")            !=null?request.getParameter("c_cia"):"";
	}
	String Mensaje       = (request.getParameter("Mensaje")!=null)?request.getParameter("Mensaje"):"";
	
	Pais pais; 
	List paises;
%>
<body onResize="redimensionar()" onLoad="redimensionar();forma.c_paiso.focus();">
<div id="capaSuperior" style="position:absolute; width:100%; height:100px; z-index:0; left: 0px; top: 0px;">
<jsp:include page="/WEB-INF/toptsp.jsp?encabezado=Ingresar Tramo"/>
</div>
<div id="capaCentral" style="position:absolute; width:100%; height:85%; z-index:0; left: 0px; top: 100px; overflow: scroll;"> 
<form name='forma' id='forma' method='POST' action="<%=CONTROLLER%>?estado=Tramo&accion=Ingresar">
<table width="748" border="2" align="center">
  <tr>
    <td>
	<table width="100%" border="0" align="center">
			<tr>
				<td colspan="2">				  	
				  <table width="100%"  border="0" cellpadding="0" cellspacing="0" class="tablaInferior">
                  <tr class="barratitulo">
                    <td width="23%" class="subtitulo1">Ingresar Tramo</td>
                    <td width="77%"><img src="<%=BASEURL%>/images/titulo.gif" width="32" height="20"></td>
                  </tr>
                </table></td>
			</tr>
			<tr>
				<td width="50%" class="fila">
					<table width="100%" border="0" align="center" class="tablaInferior">     
        				<tr class="fila">
          					<td width="188" align="left">Distrito </td>
          					<td width="329"><input name="dstrct" type="text" value="<%=dstrct%>" class="textbox" readonly="true"><img src="<%=BASEURL%>/images/botones/iconos/obligatorio.gif"></td>
        				</tr>
				  </table>
					
					<table width="100%" border="0" align="center" class="tablaInferior">
        				<tr>
							<td  colspan="2"  class="subtitulo1"><p align="left">Origen</p></td>
           				</tr>    
        				<tr class="fila">
          					<td width="157">Pais</td>
          					<td width="404" >
							<select name="c_paiso" id="select"  onChange="cargarSelects('<%=CONTROLLER%>?estado=Cargar&accion=Variostramo&carpeta=tramo&pagina=ingresarTramo.jsp&sw=1');" class="textbox">
              				<%	paises = model.paisservice.obtenerpaises();		
								Iterator it = paises.iterator();%>
		               		 	<option value="">Seleccione</option>
              					<%
									while (it.hasNext()){  
										pais = (Pais) it.next();	
										if(cpaiso.equals(pais.getCountry_code())){
    	    						%>
				        	     <option value="<%=pais.getCountry_code()%>" selected><%=pais.getCountry_name()%></option>
              				  		<%}else{%>
              					<option value="<%=pais.getCountry_code()%>"><%=pais.getCountry_name()%></option>
              				  		<%}
	   								}%>
          					</select><img src="<%=BASEURL%>/images/botones/iconos/obligatorio.gif">
							</td>
        				</tr>
        				<tr class="fila">
          					<td width="157">Origen</td>
							<td width="404" >
          						<select id="select4" name="c_origen" class="textbox">
            						<option value="">Seleccione</option>
              						<%
									List ciudadeso = model.ciudadService.getCiudadO();//(List) request.getAttribute("ciudadO");
    			        			if(ciudadeso!=null){
										Iterator iter = ciudadeso.iterator();
						    			while (iter.hasNext()){  
			      							Ciudad ciu = (Ciudad) iter.next();
											String c_ciu = request.getParameter("c_origen")!=null?request.getParameter("c_origen"):"";
											if(c_ciu.equals(ciu.getcodciu())){%>
              									<option value="<%=ciu.getcodciu()%>" selected><%=ciu.getnomciu()%></option>
              							  <%}else{%>
              									<option value="<%=ciu.getcodciu()%>"><%=ciu.getnomciu()%></option>
              							   <%}
              							}
			  						}%>
            					</select><img src="<%=BASEURL%>/images/botones/iconos/obligatorio.gif">
       					  </td>
        				</tr>
				  </table>
					
					<table width="100%" border="0" align="center" class="tablaInferior">
						<tr>
		        			<td  colspan="2"  class="subtitulo1"><p align="left">Destino</p>		        			  </td>
       	    			</tr>
        				<tr class="fila">
          					<td width="157">Pais</td>
		           			<td width="404" >
		  						<select name="c_paisd" id="select2" onChange="cargarSelects('<%=CONTROLLER%>?estado=Cargar&accion=Variostramo&carpeta=tramo&pagina=ingresarTramo.jsp&sw=3');" class="textbox">
            						<%paises = model.paisservice.obtenerpaises();		
									  Iterator ite = paises.iterator();%>
              						<option value="">Seleccione</option>
              						<%
										while (ite.hasNext()){  
											pais = (Pais) ite.next();	
											if(cpaisd.equals(pais.getCountry_code())){
    	    							%>
              						<option value="<%=pais.getCountry_code()%>" selected><%=pais.getCountry_name()%></option>
              						 	 <%}else{%>
              						<option value="<%=pais.getCountry_code()%>"><%=pais.getCountry_name()%></option>
              							<%}
	   									}%>
          						</select><img src="<%=BASEURL%>/images/botones/iconos/obligatorio.gif">
		 					</td>
        				</tr>
        				<tr class="fila">
         					 <td width="157">Destino</td>
         					 <td width="404" >
           						 <select id="select6" name="c_destino" class="textbox">
            					 	<option value="">Seleccione</option>
             						 <%List ciudades = model.ciudadService.getCiudadD();// (List) request.getAttribute("ciudadD");
									if(ciudades!=null){
            							Iterator itc = ciudades.iterator();
			    						while (itc.hasNext()){  
			      							Ciudad ciu = (Ciudad) itc.next();
											String c_ciu = request.getParameter("c_destino")!=null?request.getParameter("c_destino"):"";
											if(c_ciu.equals(ciu.getcodciu())){%>
              							<option value="<%=ciu.getcodciu()%>" selected><%=ciu.getnomciu()%></option>
              		  						<%}else{%>
              							<option value="<%=ciu.getcodciu()%>"><%=ciu.getnomciu()%></option>
              								<%}
										}
									}%>
            					</select>            						
									<img src="<%=BASEURL%>/images/botones/iconos/obligatorio.gif">
   						  </td>
        				</tr>
				  </table>
				</td>
			    <td width="50%" class="fila"><table width="100%"  border="0" align="center" class="tablaInferior">
        				<tr class="fila">
          					<td width="141" >Combustible</td>
       					  <td width="212" >
		  						<input name="c_comb"  id="c_comb" value="<%=combustible%>" class="textbox" onKeyPress="soloDigitos(event,'decOK')" onBlur="formatValoresTramo (this);" maxlength="5">
		  					</td>
        				</tr>
						<tr class="fila">
						  <td width="141" >Consumo</td>
						  <td width="212" >
							<input name="textfield" type="text" value="GALONES" size="20" maxlength="10" class="te" readonly>
							<input name="c_unidadc" type="hidden" id="c_unidadc" value="GAL">							
						  </td>
        				</tr>
        				<tr class="fila">
         				 <td width="141">Distancia</td>
          				 <td width="212">
		  					<input name="c_distancia"  id="c_distancia" value="<%=distancia%>" class="textbox" onKeyPress="soloDigitos(event,'decOK')" onBlur="formatValoresTramo (this);" maxlength="5">
		  					<img src="<%=BASEURL%>/images/botones/iconos/obligatorio.gif">
		  				</td>
       				 </tr>
       				 <tr class="fila">
          				<td width="141">Distancia (Unidad) </td>
          				<td width="212"  >
		  				<input name="kmm" type="text" value="KILOMETROS" size="20" maxlength="10" class="textbox" readonly>
					   <input name="c_unidadd" type="hidden" id="c_unidadc" value="KM"><img src="<%=BASEURL%>/images/botones/iconos/obligatorio.gif">	 				   </td>
        			</tr>
        			<tr class="fila">
          				<td>Duracion</td>
         				 <td>
           					 <input name="c_duracion"  id="c_duracion" value="<%=duracion%>" class="textbox" onKeyPress="soloDigitos(event,'decOK')" onBlur="formatValoresTramo (this);" maxlength="5">
           					 <img src="<%=BASEURL%>/images/botones/iconos/obligatorio.gif">
          				</td>
        			</tr>
        			<tr class="fila">
          				<td height="42">Duracion (Unidad) </td>
          				<td><input name="HR" type="text" value="HORAS" size="20" maxlength="10" class="textbox" readonly>
					  <input name="c_unidaddu" type="hidden" id="c_unidadc" value="HR"><img src="<%=BASEURL%>/images/botones/iconos/obligatorio.gif">         			 </td>
       			 </tr> 
			</table></td>
			</tr>			
	</table>
	</td>
  </tr>
</table>

		
   	<br>
	<center>        	
		<img src="<%=BASEURL%>/images/botones/aceptar.gif"  name="imgaceptar" onClick="validarFormTramo();" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" style="cursor:hand">           		 
		<img src="<%=BASEURL%>/images/botones/cancelar.gif"  name="imgcancelar" onClick="borrarTexto();" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" style="cursor:hand">		
		<img src="<%=BASEURL%>/images/botones/salir.gif"  name="imgsalir" onClick="window.close();" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" style="cursor:hand">
    </center>       
     <br>
  </form>



<div align="center">
  <center class='comentario'>
    <%if(!Mensaje.equals("")){%>
    <p>  
    <table border="2" align="center">
      <tr>
        <td><table width="100%"  border="1" align="center"  bordercolor="#F7F5F4" bgcolor="#FFFFFF">
            <tr>
              <td width="229" align="center" class="mensajes"><%=Mensaje%></td>
              <td width="29" background="<%=BASEURL%>/images/cuadronaranja.JPG">&nbsp;</td>
              <td width="58">&nbsp;</td>
            </tr>
        </table></td>
      </tr>
    </table>
    <p></p>
    <%}%>
  </center>

</div>
</div>
<script>
function borrarTexto(){
	forma.c_comb.value = "";
	forma.c_distancia.value = "";
	forma.c_duracion.value = "";
}
</script>
</body>
</html>
