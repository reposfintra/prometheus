<!--
- Date  : 2005
- Autor: Ing Jesus Cuestas
- Modificado Ing Sandra Escalante
- Copyrigth Notice : Fintravalores S.A. S.A
-->
<%--
-@(#)
--Descripcion : Pagina JSP, que muestra informacion si un tramo fue anulado o no
--%>     
<%@page contentType="text/html"%>
<%@page session="true"%>
<%@page import="java.util.*"%>
<%@page import="com.tsp.operation.model.*"%>
<%@page import="com.tsp.operation.model.beans.*"%>
<%@page import="com.tsp.util.*"%>
<%@include file="/WEB-INF/InitModel.jsp"%>
<html>
<head>
<title>Tramos</title>
<link href="<%= BASEURL %>/css/estilostsp.css" rel='stylesheet'>
<link href="../css/estilostsp.css" rel="stylesheet" type="text/css">
</head>
<script type='text/javascript' src="<%= BASEURL %>/js/validar.js"></script>
 <script type='text/javascript' src="<%= BASEURL %>/js/boton.js"></script>
<body onResize="redimensionar()" onLoad="redimensionar();window.opener.location.reload()">
<div id="capaSuperior" style="position:absolute; width:100%; height:100px; z-index:0; left: 0px; top: 0px;">
<jsp:include page="/WEB-INF/toptsp.jsp?encabezado=Anular Tramo"/>
</div>
<div id="capaCentral" style="position:absolute; width:100%; height:85%; z-index:0; left: 0px; top: 100px; overflow: scroll;"> 

<div align="center">
  <%String Mensaje = (request.getParameter("tipo")!=null)?request.getParameter("tipo"):"";
  if(!Mensaje.equals("")){%>
  <p>
  <table border="2" align="center">
    <tr>
      <td><table width="100%"  border="1" align="center"  bordercolor="#F7F5F4" bgcolor="#FFFFFF">
          <tr>
            <td width="229" align="center" class="mensajes">El <%=Mensaje%> fue anulado con exito</td>
            <td width="29" background="<%=BASEURL%>/images/cuadronaranja.JPG">&nbsp;</td>
            <td width="58">&nbsp;</td>
          </tr>
      </table></td>
    </tr>
  </table>
  <p>
    <%}%> 
  </p>
  <p align="center"><img src="<%=BASEURL%>/images/botones/salir.gif"  name="imgsalir" onclick="parent.close();" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" style="cursor:hand"> </p>
</div>
</div>
</body>
</html>
