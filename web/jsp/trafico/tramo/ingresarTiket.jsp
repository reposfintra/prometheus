<!--
- Date  : 2005
- Autor: Ing Jesus Cuestas
- Modificado Ing Sandra Escalante
- Copyrigth Notice : Fintravalores S.A. S.A
-->
<%--
-@(#)
--Descripcion : Pagina JSP, que muestra los datos de entrada para 
--              ingresar un tiket
--%>     
<%@page contentType="text/html"%>
<%@page session="true"%>
<%@page import="java.util.*"%>
<%@page import="com.tsp.operation.model.*"%>
<%@page import="com.tsp.operation.model.beans.*"%>
<%@page import="com.tsp.util.*"%>
<%@include file="/WEB-INF/InitModel.jsp"%>
<%@ taglib uri="http://jakarta.apache.org/taglibs/input-0.90" prefix="input" %>
<html>
<head> 
<title>.: Ingresar Tiquete</title>
<link href="<%= BASEURL %>/css/estilostsp.css" rel='stylesheet'> 
<link href="../css/estilostsp.css" rel="stylesheet" type="text/css">
<link href="../../../css/estilostsp.css" rel="stylesheet" type="text/css">
</head>
<script type='text/javascript' src="<%= BASEURL %>/js/validar.js"></script>
 <script type='text/javascript' src="<%= BASEURL %>/js/boton.js"></script>
<body onResize="redimensionar()" <%if(request.getParameter("reload")!=null){%>onLoad="window.opener.location.reload();redimensionar()"<%}%>>
<div id="capaSuperior" style="position:absolute; width:100%; height:100px; z-index:0; left: 0px; top: 0px;">
<jsp:include page="/WEB-INF/toptsp.jsp?encabezado=Ingresar Tiquete"/>
</div>
<div id="capaCentral" style="position:absolute; width:100%; height:85%; z-index:0; left: 0px; top: 100px; overflow: scroll;"> 
<%  String ccia = request.getParameter("c_cia");
		
	String origen = request.getParameter("c_origen");
	String norigen = request.getParameter("c_norigen");
		
	String destino = request.getParameter("c_destino");
	String ndestino = request.getParameter("c_ndestino");
	if(request.getParameter("tiket")!=null){
		String tiket = request.getParameter("tiket");
	}
	String tiq = "";
	String cant = ""; 
	if ( (request.getAttribute("mensaje") != null) && (!request.getAttribute("mensaje").equals("El Tiquete fue ingresado con exito!")) ){
		tiq = request.getParameter("c_tiket");
		cant = request.getParameter("c_cant");
	
	}
	
%>

<FORM name='form' id='forma' method='POST' action="<%=CONTROLLER%>?estado=Tiket&accion=Ingresar" onSubmit="return validarTCamposLlenos();"> 
  <table width="382" border="2" align="center">
    <tr>
      <td width="370">
	  	<table width="100%" border="0" align="center" class="tablaInferior">
       		<tr>
       			<td width="157" height="24"  class="subtitulo1"><p align="left">Ingresar Peaje</p></td>
           		<td width="404"  class="barratitulo"><img src="<%=BASEURL%>/images/titulo.gif" width="32" height="20"></td>
   	       </tr>        
           <tr class="fila">
          		<td width="274">Distrito </td>
          		<td width="274" valign="middle">
					<input name="c_dcia" type="text" id="c_dcia" value="<%=ccia%>" class="textbox" readonly="true">
              		<input name="c_cia" type="hidden" id="c_cia" value="<%=ccia%>">
				</td>
       	  </tr>
        	<tr class="fila">
          		<td width="274">Origen</td>
          		<td width="274" valign="middle">
					<input name="c_norigen" type="text" id="c_norigen" value="<%=norigen%>" class="textbox" readonly>
              		<input name="c_origen" type="hidden" id="c_origen" value="<%=origen%>">
				</td>
        	</tr>
        	<tr class="fila">
          		<td width="274">Destino</td>
          		<td width="274" valign="middle">
					<input name="c_ndestino" type="text" id="c_ndestino" value="<%=ndestino%>" readonly class="textbox">
              		<input name="c_destino" type="hidden" id="c_destino" value="<%=destino%>">
				</td>
        	</tr>
        	<tr class="fila">
         		<td width="274" >Tiquete </td>
       		    <td width="274" valign="middle">
					<% TreeMap c_tiket = model.tiketService.getPeajes(); %>
					<input:select name="c_tiket"  attributesText="style='width:55%;' class='textbox'"  options="<%=c_tiket%>" default="<%= (request.getParameter("c_tiket")!=null)?request.getParameter("c_tiket"):"A" %>"/>
					
			  <img src="<%=BASEURL%>/images/botones/iconos/obligatorio.gif" width="10" height="10">          	</tr>
        	<tr class="fila">
          		<td width="274" >Max. Tiquetes </td>
       		    <td width="274" valign="middle">
					<input name="c_cant" type="text" class="textbox" id="c_cant" onKeyPress="soloDigitos(event,'decNO')" value="<%= cant %>" maxlength="2" onBlur="validarMaxTiq ();">
				<img src="<%=BASEURL%>/images/botones/iconos/obligatorio.gif" width="10" height="10">				</td>
        	</tr>
        	
   	    </table>
	  </td>
    </tr>
  </table >
  <br>
  <table align="center">
 	 <tr>
    	<td colspan="2">
        	<img src="<%=BASEURL%>/images/botones/aceptar.gif"  name="imgaceptar" onClick="if( validarTCamposLlenos() ){forma.submit();}" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" style="cursor:hand">
			<img src="<%=BASEURL%>/images/botones/salir.gif"  name="imgsalir" onClick="parent.close();" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" style="cursor:hand">
       </td>
     </tr>
  </table>
</form>
<div align="center">
  
  <%
  String Mensaje = ((String)request.getAttribute("mensaje")!=null)?(String)request.getAttribute("mensaje"):"";
  if(!Mensaje.equals("")){%>
  <table border="2" align="center">
    <tr>
      <td><table width="100%"  border="1" align="center"  bordercolor="#F7F5F4" bgcolor="#FFFFFF">
          <tr>
            <td width="229" align="center" class="mensajes"><%=Mensaje%></td>
            <td width="29" background="<%=BASEURL%>/images/cuadronaranja.JPG">&nbsp;</td>
            <td width="58">&nbsp;</td>
          </tr>
      </table></td>
    </tr>
  </table>
    <%}%>
</div>
</div>
</body>
<script>
function validarMaxTiq (){
	if (forma.c_cant.value >= 10 ){
		alert('La Cantidad Maxima de Tiquetes no debe exceder los 9');
		forma.c_cant.focus();
	}
}
</script>
</html>
