<!--
- Date  : 2005
- Autor: Ing Jesus Cuestas
- Modificado Ing Sandra Escalante
- Copyrigth Notice : Fintravalores S.A. S.A
-->
<%--
-@(#)
--Descripcion : Pagina JSP, que muestra los datos de entrada para
--              ingresar un tiquete
--%>
<%@page contentType="text/html"%>
<%@page session="true"%>
<%@page import="java.util.*"%>
<%@page import="com.tsp.operation.model.*"%>
<%@page import="com.tsp.operation.model.beans.*"%>
<%@page import="com.tsp.util.*"%>
<%@include file="/WEB-INF/InitModel.jsp"%>
<html>
<head>
<title>.: Modificar/Anular Tramos</title>
<link href="<%= BASEURL %>/css/estilostsp.css" rel='stylesheet'> 
<link href="../css/estilostsp.css" rel="stylesheet" type="text/css">
<link href="../../../css/estilostsp.css" rel="stylesheet" type="text/css">
</head>
<script type='text/javascript' src="<%= BASEURL %>/js/validar.js"></script>
<script type='text/javascript' src="<%= BASEURL %>/js/funciones.js"></script>
<script type='text/javascript' src="<%= BASEURL %>/js/boton.js"></script>
<script type='text/javascript' src="<%= BASEURL %>/js/reporte.js"></script>
<body onResize="redimensionar()" <%if(request.getParameter("reload")!=null){%>onLoad="window.opener.location.reload();redimensionar()"<%}%>>
<div id="capaSuperior" style="position:absolute; width:100%; height:100px; z-index:0; left: 0px; top: 0px;">
<jsp:include page="/WEB-INF/toptsp.jsp?encabezado=Modificar/Anular Tramo"/>
</div>
<div id="capaCentral" style="position:absolute; width:100%; height:85%; z-index:0; left: 0px; top: 100px; overflow: scroll;"> 
<%	String ncia = "", norigen = "", origen = "", ndestino = "", destino = "", combustible = "", distancia = "", duracion = "", ccombustible = "", cdistancia = "", cduracion = "", ccia=""; 
	String paisd="";
	String paiso="";
	Tramo tramo = new Tramo();
	if(request.getParameter("lista")!=null){
		tramo = model.tramoService.getTramo();
	}

	ccia = (request.getParameter("lista")!=null)? tramo.getDstrct():request.getParameter("c_cia");
	origen = (request.getParameter("lista")!=null)? tramo.getOrigen():request.getParameter("c_origen");
	norigen = (request.getParameter("lista")!=null)? tramo.getNorigen():request.getParameter("c_norigen");
	destino = (request.getParameter("lista")!=null)? tramo.getDestino():request.getParameter("c_destino");
	ndestino = (request.getParameter("lista")!=null)? tramo.getNdestino():request.getParameter("c_ndestino");
	combustible = (request.getParameter("lista")!=null)? ""+tramo.getConsumo_combustible():request.getParameter("c_comb"); 
	distancia = (request.getParameter("lista")!=null)? ""+tramo.getDistancia():request.getParameter("c_distancia");
	duracion = (request.getParameter("lista")!=null)? ""+tramo.getDuracion():request.getParameter("c_duracion"); 
	ccombustible = (request.getParameter("lista")!=null)? tramo.getUnidad_consumo():request.getParameter("c_unidadc"); 
	cdistancia = (request.getParameter("lista")!=null)? tramo.getUnidad_distancia():request.getParameter("c_unidadd"); 
	cduracion = (request.getParameter("lista")!=null)? tramo.getUnidad_duracion():request.getParameter("c_unidaddu"); 
	paisd = (request.getParameter("lista")!=null)? tramo.getPaisd():request.getParameter("c_paisd");
	paiso = (request.getParameter("lista")!=null)? tramo.getPaiso():request.getParameter("c_paiso");%>

<FORM name='form' id='forma' method='POST' action="<%=CONTROLLER%>?estado=Tramo&accion=Modificar&pagina=maTramo.jsp">
  <table width="473" border="2" align="center">
    <tr>
      <td width="461">
	  <table width="100%" height="34" border="0" align="center" class="tablaInferior">
       	<tr>
       		<td width="159" height="24"  class="subtitulo1"><p align="left">Modificar/Anular Tramo</p></td>
            <td width="275"  class="barratitulo"><img src="<%=BASEURL%>/images/titulo.gif" width="32" height="20"></td>
         </tr>
   
        <tr class="fila">
          <td width="159">Distrito</td>
          <td width="275" valign="middle"><%=ccia%><input name="c_dcia" type="hidden" id="c_dcia" value="<%=ccia%>" readonly class="textbox">
              <input name="c_cia" type="hidden" id="c_cia" value="<%=ccia%>">              </td>
        </tr>
		
        <tr class="fila">
          <td width="159">Origen</td>
          <td width="275" valign="middle">
		  	[ <%=(paiso!=null)?paiso:"NR"%>
		  	<input name="c_pais" type="hidden" id="c_pais" value="<%=(paiso!=null)?paiso:"NR"%>" readonly class="textbox"> ]
            <%=norigen%> <input name="c_norigen" type="hidden" id="c_norigen2" value="<%=norigen%>" readonly class="textbox">
            <input name="c_origen" type="hidden" id="c_origen2" value="<%=origen%>">              
		  </td>
        </tr>
        <tr class="fila">
          <td width="159">Destino </td>
          <td width="275" valign="middle">[ 
		  	<%=(paisd!=null)?paisd:"NR"%>
		  	<input name="c_pais" type="hidden" id="c_pais" value="<%=(paisd!=null)?paisd:"NR"%>" readonly class="textbox"> ]
			<%=ndestino%>  <input name="c_ndestino" type="hidden" id="c_ndestino" value="<%=ndestino%>" readonly class="textbox"> 
            <input name="c_destino" type="hidden" id="c_destino" value="<%=destino%>"></td>
        </tr>
        <tr class="fila">
          <td width="159" >Combustible</td>
          <td width="275" valign="middle"><input name="c_comb" type="text" id="c_comb" value="<%=combustible%>" class="textbox" onKeyPress="soloDigitos(event,'decOK')" onBlur="formatValoresTramo (this);" maxlength="5">              </tr>
        <tr class="fila">
          <td width="159" >Consumo</td>
          <td width="275" valign="middle"><input name="textfield" type="text" value="GALONES" size="20" maxlength="10" class="te" readonly>
            <input name="c_unidadc" type="hidden" id="c_unidadc" value="GAL">             </td>
        </tr>
        <tr class="fila">
          <td width="159">Distancia</td>
          <td width="275" valign="middle" >
		  	<input name="c_distancia" type="text" id="c_distancia" value="<%=distancia%>" class="textbox" onKeyPress="soloDigitos(event,'decOK')" onBlur="formatValoresTramo (this);" maxlength="5">
		  	<img src="<%=BASEURL%>/images/botones/iconos/obligatorio.gif">		</td>
        </tr>
        <tr class="fila">
          <td width="159">Unidad de Distancia</td>
          <td width="275" valign="middle" ><input name="kmm" type="text" value="KILOMETROS" size="20" maxlength="10" class="textbox" readonly>
            <input name="c_unidadd" type="hidden" id="c_unidadd2" value="KM">
            <img src="<%=BASEURL%>/images/botones/iconos/obligatorio.gif"> </td>
        </tr>
        <tr class="fila">
          <td >Duracion</td>
          <td >
            <input name="c_duracion" type="text" id="c_duracion" value="<%=duracion%>" class="textbox" onKeyPress="soloDigitos(event,'decOK')" onBlur="formatValoresTramo (this);" maxlength="5">
            <img src="<%=BASEURL%>/images/botones/iconos/obligatorio.gif">          </td>
        </tr>
        <tr class="fila">
          <td >Unidad de Duracion</td>
          <td >            <input name="HR" type="text" value="HORAS" size="20" maxlength="10" class="textbox" readonly>
            <input name="c_unidaddu" type="hidden" id="c_unidaddu2" value="HR">
            <img src="<%=BASEURL%>/images/botones/iconos/obligatorio.gif"> </td>
        </tr>
        <tr class="fila">
          <td colspan="2" align="right"> <a href="#" class="Simulacion_Hiper" onClick="window.open('<%=CONTROLLER%>?estado=Buscar&accion=Tramos&origen=<%=origen%>&destino=<%=destino%>&ccia=<%=ccia%>&ncia=<%=ncia%>&norigen=<%=norigen%>&ndestino=<%=ndestino%>&sw=5','','status=no,scrollbars=no,width=700,height=600,resizable=yes')">Ingresar Ticket</a></td>
          </tr>
        <tr class="fila">
          <td colspan="2" align="center">
            <% Vector tikets = null;
				tikets = model.tiketService.getTickets();
				if(tikets != null){
					if(tikets.size()>0){%>
						<!-- TABLA DE TICKETS -->
            			<table width="100%" height="65" border="0" align="center" class="tablaInferior">
              			  <tr>
       						<td height="19" colspan="2"  class="subtitulo1"><p align="left">Tiquetes</p></td>
           				  </tr>
              			  <tr class="fila" align="center" height="20">
						    <td colspan="2" nowrap>	
						  	  <table width="100%" border="1" align="center"  bordercolor="#999999" bgcolor="#F7F5F4">
  							    <tr class="tblTitulo">
    							  <td><div align="center">Tiquete</div></td>
    							  <td><div align="center">Max. Tiquetes </div></td>
  								</tr>
								<%for (int i = 0; i<tikets.size();  i++){
    	    						Tramo_Ticket tiket= (Tramo_Ticket) tikets.elementAt(i);%>
								<tr class="<%=(i % 2 == 0 )?"filagris":"filaazul"%>" onMouseOver='cambiarColorMouse(this)' style="cursor:hand" title="Mostrar informacion del tiquete" onClick="window.open('<%=CONTROLLER%>?estado=Buscar&accion=Tramos&carpeta=jsp/trafico/tramo&pagina=maTiket.jsp&c_origen=<%=origen%>&c_destino=<%=destino%>&c_cia=<%=ccia%>&c_dcia=<%=ccia%>&c_norigen=<%=norigen%>&c_ndestino=<%=ndestino%>&tiket=<%=tiket.getTicket_id()%>&sw=4&lista=ok','','status=no,scrollbars=no,width=700,height=500,resizable=yes')">
                			  	  <td align="center" nowrap class="bordereporte"><%=tiket.getTicket_id()%></td>
                			  	  <td align="center" nowrap class="bordereporte"><%=tiket.getCantidad()%></td>
              					</tr>
								<%}//fin for%>
						  	  </table>
                	       </td>
               			 </tr>
            		  </table>
            	<%}//fin if
			}//fin if%>
          </td>
        </tr>       
      </table>
	  </td>
    </tr>
  </table>
  <br>
  <center>
  		<img src="<%=BASEURL%>/images/botones/modificar.gif"  name="imgmodificar" onClick="validarFormModificarTramo();" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" style="cursor:hand">
		<img src="<%=BASEURL%>/images/botones/anular.gif"  name="imgsalir" onclick="window.location='<%=CONTROLLER%>?estado=Tramo&accion=Anular&cia=<%=ccia%>&origen=<%=origen%>&destino=<%=destino%>&pagina=mensajeTramo.jsp'" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" style="cursor:hand">
		<img src="<%=BASEURL%>/images/botones/salir.gif"  name="imgsalir" onclick="parent.close();" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" style="cursor:hand">
  </center>
</form>
<center>
 <%String Mensaje = ((String)request.getAttribute("mensaje")!=null)?(String)request.getAttribute("mensaje"):"";
  if(!Mensaje.equals("")){%>
  <table border="2" align="center">
    <tr>
      <td><table width="100%"  border="1" align="center"  bordercolor="#F7F5F4" bgcolor="#FFFFFF">
          <tr>
            <td width="229" align="center" class="mensajes"><%=Mensaje%></td>
            <td width="29" background="<%=BASEURL%>/images/cuadronaranja.JPG">&nbsp;</td>
            <td width="58">&nbsp;</td>
          </tr>
      </table></td>
    </tr>
  </table>
<%}%>
</center>
</div>
</body>
</html>
