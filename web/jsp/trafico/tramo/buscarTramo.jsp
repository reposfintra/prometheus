<!--
- Date  : 2005
- Autor: Ing Jesus Cuestas
- Modificado Ing Sandra Escalante
- Copyrigth Notice : Fintravalores S.A. S.A
-->   
<%--
-@(#)
--Descripcion : Pagina JSP, que busca un tramo
--%>
<%@page contentType="text/html"%>
<%@page session="true"%>
<%@page import="java.util.*"%>
<%@page import="com.tsp.operation.model.*"%>
<%@page import="com.tsp.operation.model.beans.*"%>
<%@page import="com.tsp.util.*"%>
<%@include file="/WEB-INF/InitModel.jsp"%>
<html>
<head>
<title>.: Buscar Tramo</title>
<link href="<%= BASEURL %>/css/estilostsp.css" rel='stylesheet'> 
<link href="../css/estilostsp.css" rel="stylesheet" type="text/css">
<link href="../../../css/estilostsp.css" rel="stylesheet" type="text/css">
</head>
<script type='text/javascript' src="<%= BASEURL %>/js/boton.js"></script>
<body onResize="redimensionar();" onLoad="redimensionar();forma.paiso.focus();">
<div id="capaSuperior" style="position:absolute; width:100%; height:100px; z-index:0; left: 0px; top: 0px;">
<jsp:include page="/WEB-INF/toptsp.jsp?encabezado=Buscar Tramo"/>
</div> 
<div id="capaCentral" style="position:absolute; width:100%; height:85%; z-index:0; left: 0px; top: 100px; overflow: scroll;"> 
<form name="forma" id="forma" method="post" action='<%=CONTROLLER%>?estado=Buscar&accion=Tramos&carpeta=jsp/trafico/tramo&pagina=tramos.jsp&sw=2'>
<table width="500" border="2" align="center">
  <tr>
    <td>
      <table width="100%" border="0" align="center" class="tablaInferior">
      	<tr>
          	<td colspan="5" class="barratitulo">         	  
          	  <table width="100%"  border="0">
                <tr>
                  <td width="29%" class="subtitulo1">Buscar Tramo</td>
                  <td width="71%"><img src="<%=BASEURL%>/images/titulo.gif" width="32" height="20"></td>
                </tr>
              </table></td>
            </tr>
			<tr>
          		<td colspan="5" class="barratitulo informacion">* Digite los parametros de busqueda...</td>
            </tr>
			<tr class="fila">
          <td width="23%">Pais Origen </td>
          <td colspan="2" valign="middle"><input name="paiso" type="text" class="textbox" id="paiso"></td>
          <td width="17%" >Distrito</td>
          <td width="33%" valign="middle"><input name="c_cia" type="text" class="textbox" id="c_cia" maxlength="15"></td>
        </tr>
         <tr class="fila">
          	<td >Ciudad Origen</td>
          	<td colspan="2" valign="middle"><input name="c_origen" type="text" class="textbox" id="c_origen2" maxlength="45">
          	</td>
		  	<td >Combustible</td>
          	<td>
            	<input name="c_combustible" id="c_combustible" type="text" class="textbox">
          	</td>
         </tr>
         <tr class="fila">
         	<td >Pais Destino </td>
          	<td colspan="2" valign="middle"><input name="paisd" type="text" class="textbox" id="paisd">
          	</td>
			<td >Distancia</td>
          	<td valign="middle">
            	<input name="c_distancia" type="text" class="textbox" id="c_distancia">
          	</td>
        </tr>
        <tr class="fila">
          <td>Ciudad Destino</td>
          <td colspan="2" valign="middle">
            <input name="c_destino" type="text" class="textbox" id="c_destino" maxlength="15">
          </td>
		  <td >Duracion</td>
          <td valign="middle">
            <input name="c_duracion" type="text" class="textbox" id="c_duracion">
          </td>
        </tr>      
      </table>
   </td>
  </tr>
</table>
<br>
 <table align="center">
 	<tr align="center">
    	<td colspan="2">
	    <img src="<%=BASEURL%>/images/botones/buscar.gif"  name="imgbuscar" onClick="forma.submit();" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" style="cursor:hand">
		<img src="<%=BASEURL%>/images/botones/detalles.gif"  name="imgdetalles" onClick="location.href='<%=CONTROLLER%>?estado=Buscar&accion=Tramos&carpeta=jsp/trafico/tramo&pagina=tramos.jsp&sw=1&titulo=Listado'" style="cursor:hand">
	    <img src="<%=BASEURL%>/images/botones/salir.gif"  name="imgsalir" onClick="parent.close();" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" style="cursor:hand">
        </td>
     </tr>
  </table>
  <br>
<table width="40%"  align="center">
	  	<tr><td>
			<FIELDSET>
			<legend><span class="letraresaltada">Nota</span></legend>
			<table width="100%"  border="0" cellpadding="0" cellspacing="0" class="informacion">
  				<tr>
    			  <td nowrap>&nbsp;La busqueda de los parametros. Pais/Ciudad Origen y Pais/Ciudad Destino es realizada por NOMBRE o CODIGO. </td>
    			</tr>
			</table>
  		</FIELDSET>
		</td></tr>
  </table>
 <%  if (request.getParameter("mensaje") != null ){ %>
 <br>
  <table border="2" align="center">
      <tr>
        <td><table width="100%"  border="1" align="center"  bordercolor="#F7F5F4" bgcolor="#FFFFFF">
            <tr>
              <td width="229" align="center" class="mensajes"><%=request.getParameter("mensaje")%></td>
              <td width="29" background="<%=BASEURL%>/images/cuadronaranja.JPG">&nbsp;</td>
              <td width="58">&nbsp;</td>
            </tr>
        </table></td>
      </tr>
    </table>
 <% } %>
  </form>
  </div>       
</body>
</html>
