<%@page contentType="text/html"%>
<%@page session="true"%>
<%@page errorPage="/error/error.jsp"%>
<%@page import="java.util.*"%>
<%@page import="com.tsp.operation.model.*"%>
<%@page import="com.tsp.operation.model.beans.*"%>
<%@page import="com.tsp.util.*"%>
<%@include file="/WEB-INF/InitModel.jsp"%>
<%@taglib uri="http://jsptags.com/tags/navigation/pager" prefix="pg" %>
<html>
<head><title>Listar Responsable</title>
<link href="<%= BASEURL %>/css/estilostsp.css" rel="stylesheet" type="text/css">
<script type='text/javascript' src="<%= BASEURL %>/js/boton.js"></script>
<script type='text/javascript' src="<%= BASEURL %>/js/reporte.js"></script>
</head>
<body>
<%  String c = (String)session.getAttribute("codigo");
    String d = (String)session.getAttribute("descripcion");
    model.responsableService.searchDetalleResponsables(c,d);
    Vector vec = model.responsableService.getResponsables();
    String style = "simple";
    String position =  "bottom";
    String index =  "center";
    int maxPageItems = 10;
    int maxIndexPages = 10;

if ( vec.size() >0 ){ 
%>
<table width="416" border="2" align="center">
    <tr>
      <td>
	  	  <table width="99%" align="center">
              <tr>
                <td width="373" class="subtitulo1">&nbsp;Responsable </td>
                <td width="427" class="barratitulo"><img src="<%=BASEURL%>/images/titulo.gif" width="32" height="20"></td>
              </tr>
        </table>
	<table width="100%" border="1" bordercolor="#999999" bgcolor="#F7F5F4" align="center">
    <tr class="tblTitulo" align="center">
        <td width="69" nowrap >Codigo</td>
        <td width="250" nowrap >Descripción</td>
    </tr>
    <pg:pager
        items="<%=vec.size()%>"
        index="<%= index %>"
        maxPageItems="<%= maxPageItems %>"
        maxIndexPages="<%= maxIndexPages %>"
        isOffset="<%= true %>"
        export="offset,currentPageNumber=pageNumber"
        scope="request">
<%
    for (int i = offset.intValue(), l = Math.min(i + maxPageItems, vec.size()); i < l; i++){
        Responsable r = (Responsable) vec.elementAt(i);%>
        <pg:item>
            <tr class="<%=(i % 2 == 0 )?"filagris":"filaazul"%>" onMouseOver='cambiarColorMouse(this)'   style="cursor:hand"onClick="window.open('<%=CONTROLLER%>?estado=Responsable&accion=Serch&c_codigo=<%=r.getCodigo()%>&c_descripcion=<%=r.getDescripcion()%>&listar=False','myWindow','status=no,scrollbars=no,width=650,height=350,resizable=yes');" >
                <td align="center" class="bordereporte"><%=r.getCodigo()%></td>
                <td align="center" class="bordereporte"><%=r.getDescripcion()%></td>
            </tr>
      </pg:item>
<%  }%>
    <tr class="pie">
        <td td height="20" colspan="10" nowrap align="center">          <pg:index>
                <jsp:include page="/WEB-INF/jsp/googlesinimg.jsp" flush="true"/>
        </pg:index>
        </td>
    </tr>
    </pg:pager>        
</table>
</td>
</tr>
</table>
<br>
<%}else { %>
  <table border="2" align="center">
    <tr>
      <td><table width="410" height="73" border="1" align="center"  bordercolor="#F7F5F4" bgcolor="#FFFFFF">
          <tr>
            <td width="282" align="center" class="mensajes">Su b&uacute;squeda no arroj&oacute; resultados!</td>
            <td width="28" background="<%=BASEURL%>/images/cuadronaranja.JPG">&nbsp;</td>
            <td width="78">&nbsp;</td>
          </tr>
      </table></td>
    </tr>
  </table>
  <%}%>
 <table width="416" border="0" align="center">
    <tr>
      <td>
	  <img src="<%=BASEURL%>/images/botones/regresar.gif" style="cursor:hand" title="Volver" name="buscar"  onClick="window.location='<%=BASEURL%>/jsp/trafico/responsable/responsableBuscar.jsp'" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);">
	  </td>
	</tr>
</table>
</body>
</html>
