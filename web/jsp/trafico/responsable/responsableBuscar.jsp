<%@page contentType="text/html"%>
<%@ page session="true"%>
<%@ page errorPage="../error/error.jsp"%>
<%@page import="com.tsp.operation.model.beans.*, java.util.*"%>
<%@ include file="/WEB-INF/InitModel.jsp"%>
<%@page import="com.tsp.operation.model.*"%>
<%@page import="com.tsp.util.*"%>
<%@ taglib uri="http://jakarta.apache.org/taglibs/input-0.90" prefix="input" %>
<html>
<head><title>Insertar Codigo Discrepancia</title>
<link href="<%= BASEURL %>/css/estilostsp.css" rel="stylesheet" type="text/css">
<script type='text/javascript' src="<%= BASEURL %>/js/boton.js"></script>
</head>
<body>
<%
    Usuario usuario = (Usuario) session.getAttribute("Usuario");    
%>

<form name="form2" method="post" action="<%=CONTROLLER%>?estado=Responsable&accion=Serch&listar=True&sw=True">
    <table width="380" border="2" align="center">
      <tr>
        <td><table width="100%" align="center"  class="tablaInferior">
			<tr>
				<td width="173" class="subtitulo1">Codigo Discrepancia</td>
				<td width="205" class="barratitulo"><img src="<%=BASEURL%>/images/titulo.gif" width="32" height="20"></td>
			</tr>
            <tr class="fila">
              <td width="171" align="left" >Codigo</td>
              <td width="197"><input name="c_codigo" type="text" class="textbox" id="c_codigo" onKeyPress="soloAlfa(event)" size="15" maxlength="6"></td>
            </tr>
            <tr class="fila">
              <td width="171" align="left">Descripción</td>
              <td width="197"><input name="c_descripcion" type="text" class="textbox" id="c_descripcion" size="15" maxlength="45"></td>
        </table></td>
      </tr>
    </table>
<p>
<div align="center"><img src="<%=BASEURL%>/images/botones/buscar.gif" style="cursor:hand" title="Buscar Responsables" name="buscar"  onClick="form2.submit();" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);">&nbsp;
<img src="<%=BASEURL%>/images/botones/detalles.gif" style="cursor:hand" title="Buscar todos los responsables" name="buscar"  onClick="window.location='<%=CONTROLLER%>?estado=Responsable&accion=Serch&listar=True&sw=False'" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);">&nbsp;
<img src="<%=BASEURL%>/images/botones/salir.gif"  name="regresar" style="cursor:hand" title="Salir al Menu Principal" onMouseOver="botonOver(this);" onClick="parent.close();" onMouseOut="botonOut(this);" ></div>
</p>		
</form>
</body>
</html>
