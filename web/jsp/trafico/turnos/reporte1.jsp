<!--
- Autor : Ing. David Lamadrid
- Date  : 5 de Octubre de 2005
- Copyrigth Notice : Fintravalores S.A. S.A
-->
<%--
-@(#)
--Descripcion : Pagina JSP, que se encarga de realizar el reporte de turnos pertenecientes a un usuario
--%>

<%-- Declaracion de librerias--%>
<%@page contentType="text/html"%>
<%@page session="true"%>
<%@page errorPage="../error/ErrorPage.jsp"%>
<%@page import="java.util.*"%>
<%@page import="java.sql.*"%>
<%@page import="com.tsp.operation.model.*"%>
<%@page import="com.tsp.util.*"%>
<%@page import="com.tsp.operation.model.beans.*, java.util.*"%>
<%@include file="/WEB-INF/InitModel.jsp"%>
<%String path = application.getRealPath(model.menuService.getRealUrl(request.getRequestURI()));
  String datos[] = model.menuService.getContenidoMenu(BASEURL,request.getRequestURI(),path);
%>
<html>
<head>
<link href="<%= BASEURL %>/css/estilostsp.css" rel="stylesheet" type="text/css">
<link href="../../../css/estilostsp.css" rel="stylesheet" type="text/css">

<title>Listado de Turnos</title>
    
<script>
    function abrirVentanaBusq(an,al,url,pag) {
        window.open(url,'Trafico1','width='+an+',height='+al+',scrollbars=no,resizable=no,top=10,left=65');
    }
	
	function modificar(u,f,hE,hS,CONTROLLER){
		var url= CONTROLLER+"?estado=Turnos&accion=Modificar&sw=2&u="+u+"&f="+f+"&hE="+hE+"&hS="+hS;
		abrirVentanaBusq(950,650,url);
	}
	
	function eliminar(u,f,hE,hS,CONTROLLER){
		var url= CONTROLLER+"?estado=Turnos&accion=Eliminar&sw=2&u="+u+"&f="+f+"&hE="+hE+"&hS="+hS+"&fechai=<%= request.getParameter("fechai")%>&fechaf=<%= request.getParameter("fechaf")%>";
		location.href=url; 
	}
	
	function regresar(){
		var url= "<%= CONTROLLER %>?estado=Menu&accion=Cargar&marco=no&opcion=12&carpeta=/jsp/trafico/turnos&pagina=reporte0.jsp";
		location.href = url; 
	}
</script>
<script type='text/javascript' src="<%= BASEURL %>/js/boton.js"></script>
<script type='text/javascript' src="<%= BASEURL %>/js/reporte.js"></script>

</head>

<body onResize="redimensionar()" <%if(request.getParameter("reload")!=null){%>
        onLoad = "redimensionar();parent.close();parent.opener.location.href='<%=BASEURL%>/jsp/trafico/despacho_manual/despacho.jsp?';" 
  <%} else {%>
       onLoad = "redimensionar();";
  <%}%>
>
<div id="capaSuperior" style="position:absolute; width:100%; height:100px; z-index:0; left: 0px; top: 0px;">
 <jsp:include page="/toptsp.jsp?encabezado=Lista de Turnos"/>
</div>
<div id="capaCentral" style="position:absolute; width:100%; height:87%; z-index:0; left: 0px; top: 100px; overflow: scroll;">
<p>
<%    
     Vector vTurnos = new Vector();
	 vTurnos = model.turnosService.getVTurnos ();		 
%>


</p>
</form>
<table width="729" border="2" align="center" id="tabla1" >
    <tr>
        <td width="779" >
           <table width="100%"  align="center">
              <tr>
                <td class="subtitulo1">Lista de Turnos</td>
                <td width="54%" class="barratitulo"><img align="left" src="<%=BASEURL%>/images/titulo.gif" width="32" height="20"><%=datos[0]%></td>
              </tr>
          </table>
        
         <table id="tabla3"  border="1" borderColor="#999999" bgcolor="#F7F5F4">
                <tr class="tblTitulo" id="titulos">
                    <td width="50" nowrap >&nbsp;&nbsp;&nbsp;</td>
                    <td width="55" nowrap align="center">Usuario</td>
                    <td width="130" nowrap align="center"> Fecha de Turno </td>
                    <td width="100" nowrap> H de Entrada </td>
                    <td width="100" align="center" nowrap>H de Salida </td>
                    <td width="150" nowrap align="center"> Zona </td>
					<td width="85" nowrap align="center">Eliminar</td>
                </tr>
<%     
            for (int i =0 ;i < vTurnos.size();i++){
                Turnos turno =(Turnos)vTurnos.elementAt(i);
				Vector zonas=turno.getNombresZonas();
%>     
            <tr class="<%=(i % 2 == 0 )?"filagris":"filaazul"%>" onMouseOver='cambiarColorMouse(this)'>
            <td  nowrap  align="center" abbr="" class="bordereporte" style="cursor:hand"><img src='<%=BASEURL%>/images/botones/iconos/modificar.gif' name='Buscar' align="absmiddle" style='cursor:hand' title='Aplicar configuracion'  onclick="modificar('<%=turno.getUsuario_turno ()%>','<%=turno.getFecha_turno ()%>','<%=turno.getH_entrada ()%>','<%=turno.getH_salida()%>','<%=CONTROLLER%>');"></td>
            <td  align="center" abbr="" nowrap class="bordereporte"><%=turno.getNombreUsuario()%></td>
            <td  align="center" abbr="" nowrap class="bordereporte"><%=Util.fechaPorNombre(turno.getFecha_turno())%></td>
            <td  abbr="" nowrap class="bordereporte"><%=turno.getH_entrada ()%></td>
            <td  align="center" abbr="" nowrap class="bordereporte"><%=turno.getH_salida()%></td>
            <td  abbr="" nowrap class="bordereporte" align="center"><%for(int ii=0;ii<zonas.size();ii++){out.println(zonas.elementAt(ii)+",");}%></td>
			<td class="bordereporte" align="center" style="cursor:hand"><img src='<%=BASEURL%>/images/delete.gif' name='Buscar' align="absmiddle" style='cursor:hand' title='Aplicar configuracion'  onclick="eliminar('<%=turno.getUsuario_turno ()%>','<%=turno.getFecha_turno ()%>','<%=turno.getH_entrada ()%>','<%=turno.getH_salida()%>','<%=CONTROLLER%>');" ></td>
           </tr>
        <%}%>           
          </table> 
        </td>
    </tr>
</table>
<br>
<center><img src="<%=BASEURL%>/images/botones/regresar.gif"  name="salir" title="Salir..." onClick="regresar();" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);">
</center>
</div>
<%=datos[1]%>
</body>
</html>
