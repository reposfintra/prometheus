<%@include file="/WEB-INF/InitModel.jsp"%>
<html>
<head>
<title>Descripcion Campos agregar planilla</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link href="../../../css/estilostsp.css" rel="stylesheet" type="text/css">
<link href="<%= BASEURL %>/css/estilostsp.css" rel='stylesheet'>
</head>

<body>
<br>


<table width="696"  border="2" align="center">
  <tr>
    <td width="811" >
      <table width="100%" border="0" align="center">
        <tr  class="subtitulo">
          <td height="24" colspan="2"><div align="center">Modificaci&oacute;n  de Turnos </div></td>
        </tr>
        <tr class="subtitulo1">
          <td colspan="2">Modificaci&oacute;n de turnos a usuario</td>
        </tr>
        <tr>
          <td class="fila">Usuario</td>
          <td  class="ayudaHtmlTexto">Campo para seleccionar el usuario al cual se le registrara el turno. </td>
        </tr>
        <tr>
          <td class="fila">Fecha del turno </td>
          <td  class="ayudaHtmlTexto">Campo para seleccionar la fecha de inicio del turno. </td>
        </tr>
        <tr>
          <td class="fila">Hora de entrada </td>
          <td  class="ayudaHtmlTexto">Campo para ingresar la hora de entrada del turno. La hora y minutos estas representadas por numeros. La hora no debe estar en formato de 12 horas (AM/PM). </td>
        </tr>
        <tr>
          <td width="149" class="fila">Hora de salida </td>
          <td width="525"  class="ayudaHtmlTexto"> Campo para ingresar la hora de salida del turno. La hora y minutos estas representadas por numeros. La hora no debe estar en formato de 12 horas (AM/PM). Se debe seleccionar si la hora de salida es en el mismo d&iacute;a o al d&iacute;a siguiente. </td>
        </tr>
        <tr>
          <td class="fila">Zonas</td>
          <td  class="ayudaHtmlTexto">Campos para seleccionar las zonas del usuario que aplican para el turno. </td>
        </tr>
        <tr>
          <td class="fila"><div align="center">
            <input type="image" src = "<%=BASEURL%>/images/botones/aceptar.gif" style="cursor:default ">
          </div></td>
          <td  class="ayudaHtmlTexto">Bot&oacute;n que valida el la informaci&oacute;n digitada en los campos para la modificaci&oacute;n del turno. </td>
        </tr>
        <tr>
          <td class="fila"><div align="center">
            <input type="image" src = "<%=BASEURL%>/images/botones/salir.gif" style="cursor:default ">
          </div></td>
          <td  class="ayudaHtmlTexto">Bot&oacute;n que cierra  la ventana. </td>
        </tr>
        
      </table>
    </td>
  </tr>
</table>
<p>&nbsp;</p>
<p>&nbsp;</p>

<p>&nbsp;</p>
<p>&nbsp; </p>
</body>
</html>
