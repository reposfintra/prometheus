<%@include file="/WEB-INF/InitModel.jsp"%>
<html>
<head>
<title>Descripcion Campos agregar planilla</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link href="../../../css/estilostsp.css" rel="stylesheet" type="text/css">
<link href="<%= BASEURL %>/css/estilostsp.css" rel='stylesheet'>
</head>

<body>
<br>


<table width="696"  border="2" align="center">
  <tr>
    <td width="811" >
      <table width="100%" border="0" align="center">
        <tr  class="subtitulo">
          <td height="24" colspan="2"><div align="center">Registro de Turnos </div></td>
        </tr>
        <tr class="subtitulo1">
          <td colspan="2">Reporte de turnos registrados a usuario</td>
        </tr>
        <tr>
          <td width="149" class="fila"><div align="center">
            <input type="image" src = "<%=BASEURL%>/images/botones/iconos/modificar.gif" style="cursor:default ">
          </div></td>
          <td width="525"  class="ayudaHtmlTexto">Bot&oacute;n que modifica la informaci&oacute;n del turno en la misma l&iacute;nea del boton. </td>
        </tr>
        <tr>
          <td class="fila"><div align="center">
            <input type="image" src = "<%=BASEURL%>/images/delete.gif" style="cursor:default ">
          </div></td>
          <td  class="ayudaHtmlTexto"> Bot&oacute;n que elimina el turno en la misma l&iacute;nea del boton. </td>
        </tr>
        <tr>
          <td class="fila"><div align="center">
            <input type="image" src = "<%=BASEURL%>/images/botones/regresar.gif" style="cursor:default ">
          </div></td>
          <td  class="ayudaHtmlTexto">Bot&oacute;n que lleva a la pantalla inicial de b&uacute;squeda de turnos por usuario. </td>
        </tr>
        
      </table>
    </td>
  </tr>
</table>
<p>&nbsp;</p>
<p>&nbsp;</p>

<p>&nbsp;</p>
<p>&nbsp; </p>
</body>
</html>
