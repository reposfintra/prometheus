<!--
- Autor : Ing. David Lamadrid
- Date  : 28 de Diciembre de 2005
- Copyrigth Notice : Fintravalores S.A. S.A
-->
<%--
-@(#)
--Descripcion : Pagina JSP, que se encarga de gestionar 
                            el formulario para la insercion de datos en la 
                            tabala turnos
--%>

<%-- Declaracion de librerias--%>
<%@page contentType="text/html"%>
<%//@page pageEncoding="UTF-8"%>
<%@page errorPage="/error/ErrorPage.jsp"%>
<%@page import="com.tsp.operation.model.beans.*, java.util.*"%>
<%@include file="/WEB-INF/InitModel.jsp" %>
<%@ taglib uri="http://jakarta.apache.org/taglibs/input-0.90" prefix="input" %>
<%String path = application.getRealPath(model.menuService.getRealUrl(request.getRequestURI()));
  String datos[] = model.menuService.getContenidoMenu(BASEURL,request.getRequestURI(),path);
%>
<%
    Usuario usuarioLogin  = (Usuario) session.getAttribute("Usuario");
    String us  = usuarioLogin.getLogin();
%>
<html>
<head>
<title>Modificar Turno</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link href="../css/estilostsp.css" rel="stylesheet" type="text/css"> 
<link href="<%=BASEURL%>/css/estilostsp.css" rel="stylesheet" type="text/css">  
<script type='text/javascript' src="<%= BASEURL %>/js/boton.js"></script>
<script type='text/javascript' src="<%= BASEURL %>/js/turnosModificar.js"></script>

</head>

<body onresize="redimensionar()" onload = 'redimensionar();<% if( request.getParameter("update")!=null ){ %>window.opener.location.reload();<%}%>'>
<div id="capaSuperior" style="position:absolute; width:100%; height:100px; z-index:0; left: 0px; top: 0px;">
 <jsp:include page="/toptsp.jsp?encabezado=Modificación de Turnos"/>
</div>
<div id="capaCentral" style="position:absolute; width:100%; height:87%; z-index:0; left: 0px; top: 100px; overflow: scroll;">
<%
    List  zonas = model.zonaService.obtenerZonas(); 
	Turnos turno= model.turnosService.getTurno ();
	Vector nombresZonas = turno.getNombresZonas();
	 System.out.println("HORA DE E"+turno.getH_entrada());
	 String minutosE= "";
	 String minutosS="";

   minutosE= model.turnosService.obtenerMinutos(""+turno.getH_entrada())<10 ? 
   		"0" + String.valueOf(model.turnosService.obtenerMinutos(""+turno.getH_entrada())) : String.valueOf(model.turnosService.obtenerMinutos(""+turno.getH_entrada()));
   minutosS= model.turnosService.obtenerMinutos(""+turno.getH_salida())<10 ? 
   		"0" + String.valueOf(model.turnosService.obtenerMinutos(""+turno.getH_salida())) : String.valueOf(model.turnosService.obtenerMinutos(""+turno.getH_salida()));
   	 
%>
<form name="forma1" action="" method="post" onSubmit="return validar();">      
    <table width="396" border="2" align="center">
      <tr>
    <td width="696">
<table width="99%" align="center"> 
  <tr>
            <td width="145"  class="subtitulo1"> Turnos
</td>
            <td width="223"  class="barratitulo"><img align="left" src="<%=BASEURL%>/images/titulo.gif"><%=datos[0]%></td>
  </tr>
</table>
        <table width="99%" align="center">
          <tr class="fila"> 
            <td width="144" height="30" >Usuario : </td>
            <td width="201" ><%=turno.getNombreUsuario ()%>
              <input name="usuario_turno" type="hidden" id="usuario_turno" value=<%=turno.getUsuario_turno ()%>></td>
			</tr>
          <tr class="fila"> 
            <td width="144" height="30" >Fecha del Turno: </td>
            <td width="201"><input name="fecha" type="text" class="textbox" value="<%=turno.getFecha_turno ()%>" id="fecha" size="20" readonly> 
              				 <a href="javascript:void(0)" onclick="if(self.gfPop)gfPop.fPopCalendar(document.forma1.fecha);return false;" HIDEFOCUS>
			  <img src="<%=BASEURL%>\js\Calendario\cal.gif" width="16" height="16" border="0" alt="De click aqui para escoger la fecha">
              <input name="fecha_h" type="hidden" id="fecha_h" value="<%=turno.getFecha_turno ()%>">
</td>
          </tr>
          <tr class="fila"> 
            <td width="144" height="30" >Hora de Entrada:</td>
            <td width="201" ><input name="h_entrada" value="<%=model.turnosService.obtenerHora(""+turno.getH_entrada())%>" type="text" class="textbox" id="h_entrada" size="2" maxlength="2" onKeyPress="soloDigitos(event, 'decNO')" > 
            : 
              <input name="m_entrada" type="text" value="<%=minutosE%>" class="textbox" id="m_entrada" size="2" maxlength="2" onKeyPress="soloDigitos(event, 'decNO')" > <select name="h_e" size="1" id="h_e">
                 <%if( model.turnosService.obtenerHorario(""+turno.getH_entrada()).equals("AM")){%>
				<option value="AM" selected>AM</option>
                <option value="PM">PM</option>
              	<%}else{%>
				<option value="PM" selected>PM</option>
			     <option value="AM">AM</option>       
				<%}%>		
              </select> <input name="hora_e" type="hidden" id="hora_e" value="<%=turno.getH_entrada()%>">     
		    </td>
          </tr>
          <tr class="fila"> 
            <td height="30" >Hora de Salida :</td>
            <td><input name="h_salida" type="text" class="textbox" value="<%=model.turnosService.obtenerHora(""+turno.getH_salida())%>" id="h_salida"  size="2" maxlength="2" onKeyPress="soloDigitos(event, 'decNO')" > 
            : 
              <input name="m_salida" type="text" class="textbox"     value="<%=minutosS%>"      id="m_salida" size="2" maxlength="2" onKeyPress="soloDigitos(event, 'decNO')" > <select name="h_s" size="1"  id="h_s">
                <%if( model.turnosService.obtenerHorario(""+turno.getH_salida()).equals("AM")){%>
				<option value="AM" selected>AM</option>
                <option value="PM">PM</option>
              	<%}else{%>
				<option value="PM" selected>PM</option>
			     <option value="AM">AM</option>       
				<%}%>		
			  </select>
              <input name="hora_s" type="hidden" id="hora_s" value="<%=turno.getH_salida()%>"> 
			  </td>
          </tr>
          <tr class="fila"> 
            <td height="30" >Zonas:</td>
            <td>
				 <div class="scroll" style=" overflow:auto ; WIDTH: 100%; HEIGHT:100px; " >
			   <table id="tablaFrom" > 
			   <%String select="";
			   	 System.out.println("bandera 5 "+zonas.size());
			   		for(int i=0;i<zonas.size();i++){
					select="";
			   		Zona zona=(Zona)zonas.get(i);
						for(int x=0;x<nombresZonas.size();x++){
							//out.println(nombresZonas.elementAt(x)+"  "+zona.getNomZona());
							if((nombresZonas.elementAt(x)+"").equals(zona.getNomZona())){
								select="checked";
							}
						}
					 System.out.println("bandera 6 en pagina");
			   %>
			   <tr class="fila"><td><input  type="checkbox" onClick="" name="zonas" id="zonas" value="<%=zona.getCodZona ()%>" <%=select%>><%= zona.getNomZona()%></td></tr>
			   <%}%>
			   </table>
			   </div>
			
			</td>
         	
		  </tr>
        </table>
</td>
</tr>
</table>

  <br>
  <div align="center">
  <input type="hidden" name='usuario' value="<%=us%>">
      <img src='<%=BASEURL%>/images/botones/aceptar.gif' name='Buscar' align="absmiddle" style='cursor:hand' title='Aceptar...'  onclick="validar('<%=CONTROLLER%>');" onMouseOver='botonOver(this);' onMouseOut='botonOut(this);'> 
      &nbsp;&nbsp;<img src='<%=BASEURL%>/images/botones/salir.gif' name='Buscar' align="absmiddle" style='cursor:hand' title='Salir...'  onclick="salir();" onMouseOver='botonOver(this);' onMouseOut='botonOut(this);'> 
    </div>
  <p>&nbsp;</p> 
</form>

<%
  String mensaje=""+request.getParameter("ms");
   System.out.println("bandera 7 en pagina "+mensaje);
    if (! (mensaje.equals("")|| mensaje.equals("null"))){
%>
<table border="2" align="center">
  <tr>
    <td><table width="410" height="20" border="1" align="center"  bordercolor="#F7F5F4" bgcolor="#FFFFFF">
        <tr>
          <td width="282" align="center" class="mensajes"><%=mensaje%></td>
          <td width="28" background="<%=BASEURL%>/images/cuadronaranja.JPG">&nbsp;</td>
          <td width="78">&nbsp;</td>
        </tr>
    </table></td>
  </tr>
</table>
<%
    }   %>
</div>
<%=datos[1]%>
</body>
<iframe width=188 height=166 name="gToday:normal:agenda.js:gfPop:plugins.js" id="gToday:normal:agenda.js:gfPop:plugins.js" src="<%=BASEURL%>/js/Calendario/ipopeng.htm" scrolling="no" frameborder="0" style="visibility:visible; z-index:999; position:absolute; left:-500px; top:0px;"> </iframe>
</html>
