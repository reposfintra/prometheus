<!--
- Autor : Ing. David Lamadrid
- Date  : 28 de Diciembre de 2005
- Copyrigth Notice : Fintravalores S.A. S.A
-->
<%--
-@(#)
--Descripcion : Pagina JSP, que se encarga de gestionar 
                            el formulario para la insercion de datos en la 
                            tabala turnos
--%>

<%-- Declaracion de librerias--%>
<%@page contentType="text/html"%>
<%//@page pageEncoding="UTF-8"%>
<%@page errorPage="/error/ErrorPage.jsp"%>
<%@page import="com.tsp.operation.model.beans.*, java.util.*"%>
<%@include file="/WEB-INF/InitModel.jsp" %>
<%@ taglib uri="http://jakarta.apache.org/taglibs/input-0.90" prefix="input" %>
<%String path = application.getRealPath(model.menuService.getRealUrl(request.getRequestURI()));
  String datos[] = model.menuService.getContenidoMenu(BASEURL,request.getRequestURI(),path);
%>
<%
    Usuario usuarioLogin  = (Usuario) session.getAttribute("Usuario");
    String us  = usuarioLogin.getLogin();
%>
<html>
<head>
    <title>Nuevo Turno</title>
    <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
    <link href="../../css/estilostsp.css" rel="stylesheet" type="text/css"> 
    <link href="<%=BASEURL%>/css/estilostsp.css" rel="stylesheet" type="text/css">  
    <script type='text/javascript' src="<%= BASEURL %>/js/boton.js"></script>
    <script type='text/javascript' src="<%= BASEURL %>/js/turnosInsertar.js"></script>
    <script type="text/javascript">
    var url = '<%= CONTROLLER%>';
    </script>
</head>

<body onresize="redimensionar()" onload = 'redimensionar()'>
    <div id="capaSuperior" style="position:absolute; width:100%; height:100px; z-index:0; left: 0px; top: 0px;">
        <jsp:include page="/toptsp.jsp?encabezado=Registros de Turnos"/>
    </div>
    <div id="capaCentral" style="position:absolute; width:100%; height:87%; z-index:0; left: 0px; top: 100px; overflow: scroll;">
<%
	TreeMap b = model.usuarioService.usuariosPorDptoT ();
    List  zonas = ( model.zonaService.obtenerZonas()==null ) ? new LinkedList() : model.zonaService.obtenerZonas();

	String turno_sel = request.getParameter("turno_sel")==null ? "" : request.getParameter("turno_sel");
	String fecha_turno = request.getParameter("fecha_turno")!= null ?  request.getParameter("fecha_turno") : "" ;
        String fecha_turno2 = request.getParameter("fecha_turno2")!= null ?  request.getParameter("fecha_turno2") : "" ;
	String h_entrada = request.getParameter("h_ent")!= null ?  request.getParameter("h_entrada") : "" ;
	String h_salida = request.getParameter("h_sal")!= null ? request.getParameter("h_salida") : "" ;
	String m_entrada = request.getParameter("m_ent")!= null ?  request.getParameter("m_entrada") : "" ;
	String m_salida = request.getParameter("m_sal")!= null ?  request.getParameter("m_salida") : "" ;
	String h_e = request.getParameter("h_e")!= null ? request.getParameter("h_e")  : "" ;
	String h_s = request.getParameter("h_s")!= null ? request.getParameter("h_s") : "" ;
	String dia = request.getParameter("d")!= null ? request.getParameter("dia")  : "" ;
        String dias = request.getParameter("dias")!= null ? request.getParameter("dias")  : "" ;
        String chk  = request.getParameter("chk") != null ? request.getParameter("chk") : "no" ;
%>
        <form name="forma1" action="" method="post" onSubmit="return validar();">      
        <table width="396" border="2" align="center">
            <tr>
                <td width="696">
                <table width="99%" align="center"> 
                    <tr>
                        <td width="145"  class="subtitulo1"> Turnos
                        </td>
                        <td width="223"  class="barratitulo"><img align="left" src="<%=BASEURL%>/images/titulo.gif"><%=datos[0]%></td>
                    </tr>
                </table>
                <table width="99%" align="center">
                <tr class="fila"> 
                <td width="144" height="30" >Usuario : </td>
                <td width="201" ><input:select name="usuario"  attributesText=" id='usuario' style='width:150px;' class='listmenu' onChange='actualizar(url);' "  default="<%= turno_sel %>" options="<%=b%>" /><script>selec(document.forma1.usuario,'<%= turno_sel %>');</script></td>
            </tr>	
                    <tr class="fila"> 
                    <td width="144" height="30" >Semanal : </td>
                    <td width="201" ><input type="checkbox"  <%=(  chk.equals("si") )? "checked" : ""%> name="semanal" id="semanal" onclick="enableFechaFin();" ></td>
                    </tr>              
	 
                    <tr class="fila"> 
                        <td width="144" height="30" >Fecha del Turno: </td>
                        <td width="201"><input name="fecha" type="text" class="textbox" id="fecha" value="<%= fecha_turno %>" size="20" readonly> 
                            <a href="javascript:void(0)" onclick="if(self.gfPop)gfPop.fPopCalendar(document.forma1.fecha);return false;" HIDEFOCUS>
                            <img src="<%=BASEURL%>\js\Calendario\cal.gif" width="16" height="16" border="0" alt="De click aqui para escoger la fecha">
                        </td>
                    </tr>
         
          
                    <tr class="fila" id="fechafin" <%=(  chk.equals("no") )? "style='display:none'" : ""%>> 
                        <td width="144" height="30" >Fecha Fin del Turno: </td>
                        <td width="201"><input name="fecha2" type="text" class="textbox" id="fecha2" value="<%= fecha_turno2 %>" size="20" readonly> 
                            <a href="javascript:void(0)" onclick="if(self.gfPop)gfPop.fPopCalendar(document.forma1.fecha2);return false;" HIDEFOCUS>
                            <img src="<%=BASEURL%>\js\Calendario\cal.gif" width="16" height="16" border="0" alt="De click aqui para escoger la fecha">
                        </td>
                    </tr>
          
                    <tr class="fila"> 
                    <td width="144" height="30" >Hora de Entrada:</td>
                    <td width="201" ><input name="h_entrada" type="text" class="textbox" id="h_entrada" onKeyPress="soloDigitos(event, 'decNO')" value="<%= h_entrada%>" size="2" maxlength="2" > 
                        : 
                        <input name="m_entrada" type="text" class="textbox" id="m_entrada" onKeyPress="soloDigitos(event, 'decNO')" value="<%= m_entrada%>" size="2" maxlength="2" > 
                        <select name="h_e" size="1"  id="h_e" >
                        <option value="AM" selected>AM</option>
                        <option value="PM">PM</option>
                        </select><script>selec(forma1.h_e,'<%= h_e %>')</script>     
                    </td>
                    </tr>
                    <tr class="fila"> 
                        <td height="30" >Hora de Salida : </td>
                        <td><input name="h_salida" type="text" class="textbox" id="h_salida" onKeyPress="soloDigitos(event, 'decNO')" value="<%= h_salida%>"  size="2" maxlength="2" > 
                            : 
                            <input name="m_salida" type="text" class="textbox"           id="m_salida" onKeyPress="soloDigitos(event, 'decNO')" value="<%= m_salida%>" size="2" maxlength="2" > 
                            <select name="h_s" size="1" id="h_s">
                            <option value="AM" selected>AM</option>
                            <option value="PM">PM</option>
                            </select><script>selec(forma1.h_s,'<%= h_s %>')</script> 
                            <select name="dia" id="dia" class="textbox" >
                            <option value="a" selected>Dia Actual</option>
                            <option value="S">Dia Siguiente</option>                
                            </select><script>selec(forma1.dia,'<%= dia %>')</script>
                        </td>
                    </tr>
                    <tr class="fila"> 
                        <td <%= zonas.size()>0 ? "height='30'" : "" %>>Zonas:</td>
                        <td><% if ( zonas.size()>0 ) {%> 
                            <div class="scroll" style=" overflow:auto ; WIDTH: 100%; HEIGHT:100px; " >				
                                <table id="tablaFrom" > 
			   <%for(int i=0;i<zonas.size();i++){
			   		Zona zona=(Zona)zonas.get(i);
			   %>
                                    <tr class="fila"><td><input type="checkbox" onClick="" name="zonas" id="zonas" value="<%=zona.getCodZona ()%>" ><%= zona.getNomZona()%></td></tr>
			   <%}%>
                                </table>
                            </div><% } else { out.println("<span class=\"informacion\">No hay zonas asignadas al usuario seleccionado.</span><input type=\"checkbox\" name=\"nozonas\" value=\"1\" checked style=\"visibility:hidden\">"); }%>
			
                        </td>
         	
                    </tr>
                </table>
            </td>
            </tr>
        </table>

        <br>
        <div align="center">
            <input type="hidden" name='usuario' value="<%=us%>">
            <img src='<%=BASEURL%>/images/botones/aceptar.gif' name='Buscar' align="absmiddle" style='cursor:hand' title='Aceptar...'  onclick="validar('<%=CONTROLLER%>');" onMouseOver='botonOver(this);' onMouseOut='botonOut(this);'> 
            &nbsp;&nbsp;<img src='<%=BASEURL%>/images/botones/salir.gif' name='Buscar' align="absmiddle" style='cursor:hand' title='Salir...'  onclick="salir();" onMouseOver='botonOver(this);' onMouseOut='botonOut(this);'> 
        </div>
        <p>&nbsp;</p> 
        </form>

<%
  String mensaje=""+request.getParameter("ms");
    if (! (mensaje.equals("")|| mensaje.equals("null"))){
%>
        <table border="2" align="center">
            <tr>
                <td><table width="600" height="20" border="1" align="center"  bordercolor="#F7F5F4" bgcolor="#FFFFFF">
                    <tr>
                        <td width="600" align="center" class="mensajes"><%=mensaje%></td>
                        <td width="28" background="<%=BASEURL%>/images/cuadronaranja.JPG">&nbsp;</td>
                        <td width="78">&nbsp;</td>
                    </tr>
                </table></td>
            </tr>
        </table>
<%
    }   %>
    </div>
<%=datos[1]%>
</body>
<iframe width=188 height=166 name="gToday:normal:agenda.js:gfPop:plugins.js" id="gToday:normal:agenda.js:gfPop:plugins.js" src="<%=BASEURL%>/js/Calendario/ipopeng.htm" scrolling="no" frameborder="0" style="visibility:visible; z-index:999; position:absolute; left:-500px; top:0px;"> </iframe>
</html>