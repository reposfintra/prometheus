<!--
- Autor : Ing. David Lamadrid
- Date  : 28 de Diciembre de 2005
- Copyrigth Notice : Fintravalores S.A. S.A
-->
<%--
-@(#)
--Descripcion : Pagina JSP, que se encarga de gestionar 
                            el formulario para la insercion de datos en la 
                            tabala turnos
--%>

<%-- Declaracion de librerias--%>
<%@page contentType="text/html"%>
<%@page errorPage="/error/ErrorPage.jsp"%>
<%@page import="com.tsp.operation.model.beans.*, java.util.*"%>
<%@include file="/WEB-INF/InitModel.jsp" %>
<%@ taglib uri="http://jakarta.apache.org/taglibs/input-0.90" prefix="input" %>
<%String path = application.getRealPath(model.menuService.getRealUrl(request.getRequestURI()));
  String datos[] = model.menuService.getContenidoMenu(BASEURL,request.getRequestURI(),path);
%>
<%
    Usuario usuarioLogin  = (Usuario) session.getAttribute("Usuario");
    String us  = usuarioLogin.getLogin();
%>
<html>
<head>
<title>Nuevo Despacho Manual</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link href="../css/estilostsp.css" rel="stylesheet" type="text/css"> 
<link href="<%=BASEURL%>/css/estilostsp.css" rel="stylesheet" type="text/css">  
<script type='text/javascript' src="<%= BASEURL %>/js/boton.js"></script>
<script src='<%=BASEURL%>/js/date-picker.js'></script>
<script>
function generarReporte(CONTROLLER,frm){ 
        if((frm.fechai.value != "" )&&(frm.fechaf.value != "" )){
             if(frm.fechai.value <= frm.fechaf.value){
                  /*window.open(CONTROLLER+'?estado=Turnos&accion=GReporte&usuario='+frm.usuario.value+
                                                                   '&fechai= '+frm.fechai.value +
                                                                   '&fechaf= '+frm.fechaf.value ,'',' top=0,left=100, width=900, height=500, scrollbar=no, status=yes, resizable=yes');*/
					window.location.href = CONTROLLER+'?estado=Turnos&accion=GReporte&usuario='+frm.usuario.value+
                                                                   '&fechai= '+frm.fechai.value +
                                                                   '&fechaf= '+frm.fechaf.value;																   
      
             }else{
                alert("La fecha inicial debe ser menor o igual a la final");     
             }
          }else{
              alert("La campos fecha no pueden estar vacios"); 
          }
}
</script>

</head>

<body onresize="redimensionar()" onload = 'redimensionar()'>
<div id="capaSuperior" style="position:absolute; width:100%; height:100px; z-index:0; left: 0px; top: 0px;">
 <jsp:include page="/toptsp.jsp?encabezado=Lista de Turnos por Usuario"/>
</div>
<div id="capaCentral" style="position:absolute; width:100%; height:87%; z-index:0; left: 0px; top: 100px; overflow: scroll;">
<%
	TreeMap b = model.usuarioService.usuariosPorDptoT ();
    List  zonas = model.zonaService.obtenerZonas(); 
%>
<form name="forma1" action="" method="post" onSubmit="return validar();">      
    <table width="396" border="2" align="center">
      <tr>
    <td width="696">
<table width="99%" align="center"> 
  <tr>
            <td width="145"  class="subtitulo1"> Turnos
</td>
            <td width="223"  class="barratitulo"><img align="left" src="<%=BASEURL%>/images/titulo.gif"><%=datos[0]%></td>
  </tr>
</table>
        <table width="99%" align="center">
          <tr class="fila"> 
            <td width="144" height="30" >Usuario : </td>
            <td width="201" ><input:select name="usuario"      attributesText=" id='usuario' style='width:150px;' class='listmenu'  "  default="" options="<%=b%>" /></td>
	  </tr>
	  <tr class="fila">
            <td><strong>Fecha inicial </strong></td>
            <td>
              <input name='fechai' type='text' class="textbox" id="fechai" style='width:120' value='' readonly>
              <a href="javascript:void(0);" class="link" onFocus="if(self.gfPop)gfPop.fPopCalendar(document.forma1.fechai);return false;"  HIDEFOCUS > <img src="<%=BASEURL%>\js\Calendario\cal.gif" width="16" height="16"
                border="0" alt="De click aqu&iacute; para ver el calendario."></a> <img src="<%= BASEURL%>/images/botones/iconos/obligatorio.gif" width="10" height="10"></td>
         </tr>
         <tr class="fila" >
            <td><strong>Fecha final </strong></td>
            <td>
              <input name='fechaf' type='text' class="textbox" id="fechaf" style='width:120' value='' readonly>
              <a href="javascript:void(0);" class="link" onFocus="if(self.gfPop)gfPop.fPopCalendar(document.forma1.fechaf);return false;"  HIDEFOCUS > <img src="<%=BASEURL%>\js\Calendario\cal.gif" width="16" height="16"
              border="0" alt="De click aqu&iacute; para ver el calendario."></a> <img src="<%= BASEURL%>/images/botones/iconos/obligatorio.gif" width="10" height="10"></td>
          </tr>
        </table>
</td>
</tr>
</table>

  <br>
  <div align="center">
      <img src='<%=BASEURL%>/images/botones/aceptar.gif' name='Buscar' align="absmiddle" style='cursor:hand' title='Aceptar...'  onclick="generarReporte('<%=CONTROLLER%>',forma1);" onMouseOver='botonOver(this);' onMouseOut='botonOut(this);'> 
      &nbsp;&nbsp;<img src='<%=BASEURL%>/images/botones/salir.gif' name='Buscar' align="absmiddle" style='cursor:hand' title='Salir...'  onclick="parent.close();" onMouseOver='botonOver(this);' onMouseOut='botonOut(this);'> 
    </div>
  <p>&nbsp;</p> 
</form>

<%
  String mensaje=""+request.getParameter("ms");
    if (! (mensaje.equals("")|| mensaje.equals("null"))){
%>
<table border="2" align="center">
  <tr>
    <td><table width="410" height="20" border="1" align="center"  bordercolor="#F7F5F4" bgcolor="#FFFFFF">
        <tr>
          <td width="282" align="center" class="mensajes"><%=mensaje%></td>
          <td width="28" background="<%=BASEURL%>/images/cuadronaranja.JPG">&nbsp;</td>
          <td width="78">&nbsp;</td>
        </tr>
    </table></td>
  </tr>
</table>
<%
    }   %>
</div>
<%=datos[1]%>
</body>
<iframe width=188 height=166 name="gToday:normal:agenda.js:gfPop:plugins.js" id="gToday:normal:agenda.js:gfPop:plugins.js" src="<%=BASEURL%>/js/Calendario/ipopeng.htm" scrolling="no" frameborder="0" style="visibility:visible; z-index:999; position:absolute; left:-500px; top:0px;"> </iframe>
</html>
