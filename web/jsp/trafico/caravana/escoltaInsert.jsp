<%@page contentType="text/html"%>
<%@page pageEncoding="UTF-8"%>
<%@page errorPage="../../error/ErrorPage.jsp"%>
<%@page import="com.tsp.operation.model.beans.*, java.util.*"%>
<%@include file="/WEB-INF/InitModel.jsp" %>
<%@ taglib uri="http://jakarta.apache.org/taglibs/input-0.90" prefix="input" %>
<%String placa ="", conductor="";
	if(request.getParameter("placa")!=null){
		placa = request.getParameter("placa");
		conductor = request.getParameter("conductor");
	}
	if(request.getParameter("conductor")!=null){
		
	}
  String msg = (request.getParameter("msg")!=null)?request.getParameter("msg"):"";
%>

<html>
<head>
<title>Untitled Document</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link href="../../../css/estilostsp.css" rel="stylesheet" type="text/css"> 
<link href="<%=BASEURL%>/css/estilostsp.css" rel="stylesheet" type="text/css"> 
<script type='text/javascript' src="<%=BASEURL%>/js/Validacion.js"></script> 
<script type='text/javascript' src="<%=BASEURL%>/js/boton.js"></script> 

</head>

<body onResize="redimensionar()" onLoad="redimensionar()">
<div id="capaSuperior" style="position:absolute; width:100%; height:100px; z-index:0; left: 0px; top: 0px;">
<jsp:include page="/WEB-INF/toptsp.jsp?encabezado=Ingresar Vehiculo Escolta"/>
</div>
<div id="capaCentral" style="position:absolute; width:100%; height:85%; z-index:0; left: 0px; top: 100px; overflow: scroll;"> 
<%//nuevo   
        String tipdoc="012"; 
		String act="005";
		//*****
%>

<form name="frmplaca" action="<%=CONTROLLER%>?estado=Placa&accion=Insert&cmd=show&pag=ESC" method="post">   
   
  <table width="702" border="2" align="center">
  <tr>
    <td>

<table width="99%"  align="center"   >
  <tr>
    <td width="50%" height="22"  class="subtitulo1"><p align="left">Caracteristicas del vehiculo </p></td>
    <td width="392"  class="barratitulo"><img src="<%=BASEURL%>/images/titulo.gif" width="32" height="20"></td>
  </tr>
</table>
<table width="99%"  align="center" >
  <tr class="fila">
    <td width="67" height="29">Placa</td>
    <td width="160" ><input name="placa" type="text" class="textbox" id="placa" size="9" maxlength="8">
      <img  src="<%=BASEURL%>/images/botones/iconos/obligatorio.gif" width="10" height="10"></td>
    <td width="87" >Tipo</td>
    <td colspan="3" nowrap><input name="tipo" type="text" class="textbox" id="cedula35" value="ESC" size="10" readonly>
      <img  src="<%=BASEURL%>/images/botones/iconos/obligatorio.gif" width="10" height="10"></td>
    </tr>
  <tr class="fila">
    <td width="67" height="21">Clase</td>
    <td width="160" ><input name="clase" type="text" class="textbox" id="tarhabilitacion4246" size="13" maxlength="5">
      <img  src="<%=BASEURL%>/images/botones/iconos/obligatorio.gif" width="10" height="10"></td>
    <td width="87" >Modelo</td>
    <td width="126" nowrap><input name="modelo" type="text" class="textbox" id="modelo3" size="10" maxlength="4">
      <img  src="<%=BASEURL%>/images/botones/iconos/obligatorio.gif" width="10" height="10"></td>
    <td width="69">Marca</td>
    <td width="146" nowrap><% TreeMap marcas = model.marcaService.listar(); 
         %>
        <input:select name="marca" options="<%=marcas%>" attributesText="style='width:100%;' class='listmenu'"/></td>
    </tr>

  <tr class="fila">
    <td width="67" height="21">Carroceria</td>
    <td width="160"  nowrap><% TreeMap carrs = model.carrService.listar();%>
        <input:select name="carroceria" attributesText="style='width:100%;' class='listmenu'" options="<%=carrs%>" default="AM"/></td>
    <td width="87" >Serial Motor</td>
    <td colspan="3" nowrap><input name="nomotor" type="text" class="textbox" maxlength="15">
      <img  src="<%=BASEURL%>/images/botones/iconos/obligatorio.gif" width="10" height="10"></td>
    </tr>
  <tr class="fila">
    <td width="67" height="30">Color</td>
    <td width="160" ><% TreeMap colores = model.colorService.listar();
         %>
        <input:select name="color" options="<%=colores%>" attributesText="style='width:100%;' class='listmenu'"/></td>
    <td >Numero Rin</td>
    <td colspan="3"><input name="norin" type="text" class="textbox" id="norin3" size="6"></td>
    </tr>
</table>
</td>
</tr>
</table>
<table width="702" border="2" align="center">
  <tr>
    <td>
<table width="99%"   align="center"> 
  <tr>
    <td width="50%" height="22"  class="subtitulo1"><p align="left">Informaci&oacute;n del Propietario </p></td>
    <td width="414"  class="barratitulo"><img src="<%=BASEURL%>/images/titulo.gif" width="32" height="20"></td>
  </tr>
</table>
<table width="99%"  align="center" >
  <tr class="fila">
    <td width="80" height="20">Agencia</td>
    <td width="131" ><% TreeMap agencia = model.agenciaService.listar();%>
            <input:select name="agencia" attributesText="style='width:90%;' class='listmenu'" options="<%=agencia%>" default="SM"/><img  src="<%=BASEURL%>/images/botones/iconos/obligatorio.gif" width="10" height="10"></td>
    <td width="100" >Poliza SOAT</td>
    <td width="122"><input name="soat" type="text" class="textbox" id="soat3" size="16"></td>
    <td width="117">Fecha de Vencimiento</td>
    <td width="105"><input name="venseguroobliga" type="text" class="textbox" id="venseguroobliga" size="11" readonly>
      <span class="comentario"><a href="javascript:void(0)" onClick="if(self.gfPop)gfPop.fPopCalendar(document.frmplaca.venseguroobliga);return false;" hidefocus><img name="popcal" align="absmiddle" src="<%=BASEURL%>/js/calendartsp/calbtn.gif" width="16" height="16" border="0" alt=""></a></span> </td>
    </tr>

  
  
  <tr class="fila">
    <td width="80" height="30">Cedula Propietario </td>
    <td width="131" ><input name="propietario" type="text" class="textbox" id="propietario" size="16">
      <img  src="<%=BASEURL%>/images/botones/iconos/obligatorio.gif" width="10" height="10"></td>
    <td width="100" >Cedula Conductor</td>
    <td colspan="4"><input name="conductor" type="text" class="textbox" id="conductor" size="13">
      <img  src="<%=BASEURL%>/images/botones/iconos/obligatorio.gif" width="10" height="10"></td>
    </tr>
</table>
</td>
</tr>
</table>
 <br>
<table width="699" border="0" align="center">
  <tr>
    <td height="27" align="center">         
      <img src="<%=BASEURL%>/images/botones/aceptar.gif" name="mod"  height="21" onClick="return validarPlacaEscolta();" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" style="cursor:hand"> &nbsp; <img src="<%=BASEURL%>/images/botones/cancelar.gif"  name="imgsalir"  height="21" onMouseOver="botonOver(this);" onClick="frmplaca.reset();" onMouseOut="botonOut(this);" style="cursor:hand">&nbsp; <img src="<%=BASEURL%>/images/botones/salir.gif"  name="imgsalir"  height="21" onMouseOver="botonOver(this);" onClick="parent.close();" onMouseOut="botonOut(this);" style="cursor:hand"> <input type="hidden" name="grupo_equipo" value="O">
      <input type="hidden" name="grupos" value="O"></td>
    </tr>
</table>
<%if(!msg.equals("")){%>
	<p>
   <table width="407" border="2" align="center">
     <tr>
    <td><table width="100%"   align="center"  >
      <tr>
        <td width="229" align="center" class="mensajes"><%=msg%></td>
        <td width="29" background="<%=BASEURL%>/images/cuadronaranja.JPG">&nbsp;</td>
        <td width="58">&nbsp;</td>
      </tr>
    </table></td>
  </tr>
</table>
  </p>
   <%}%>
</form>
</div>
<iframe width=188 height=166 name="gToday:datetime:<%=BASEURL%>/js/calendartsp/agenda.js:gfPop:<%=BASEURL%>/js/calendartsp/plugins.js" id="gToday:datetime:<%=BASEURL%>/js/calendartsp/agenda.js:gfPop:<%=BASEURL%>/js/calendartsp/plugins.js" src="<%=BASEURL%>/js/calendartsp/ipopeng.htm" scrolling="no" frameborder="0" style="visibility:visible; z-index:999; position:absolute; left:-500px; top:0px;">
</iframe>
</body>
</html>


