<%@ page contentType="text/html; charset=iso-8859-1" language="java" errorPage="" %>
<%@page import="com.tsp.operation.model.beans.*, java.util.*"%>
<%@include file="/WEB-INF/InitModel.jsp" %>
<%
  String noCaravana =  (request.getParameter("var")!=null)?request.getParameter("var"):"";
  Vector datos = new Vector();  
  model.caravanaSVC.getVectorCaravanas(noCaravana,true);
  datos = model.caravanaSVC.getVectorListarCaravana();
 
%>

<html>
<head>
<title>INFORMACION CLIENTE CARAVANA</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<script type='text/javascript' src="<%=BASEURL%>/js/caravanas.js"></script>
<script type='text/javascript' src="<%=BASEURL%>/js/boton.js"></script>
<link href="../../../css/caravana.css" rel="stylesheet" type="text/css">
<link href="<%=BASEURL%>/css/caravana.css" rel="stylesheet" type="text/css">
<link href="<%=BASEURL%>/css/boton.css" rel="stylesheet" type="text/css">
</head>

<body onResize="redimensionar()" onLoad="redimensionar()">
<div id="capaSuperior" style="position:absolute; width:100%; height:100px; z-index:0; left: 0px; top: 0px;">
<jsp:include page="/WEB-INF/toptsp.jsp?encabezado=Caravanas finalizadas"/>
</div>
<div id="capaCentral" style="position:absolute; width:100%; height:85%; z-index:0; left: 0px; top: 100px; overflow: scroll;"> 
<form name="buscar" method="post" id="busq" action="javascript:buscarCaravanaFinalizadas('<%=BASEURL%>')">
<table width="498" border="2" align="center">
    <tr>
      <td>
	  <table width="99%" align="center" bgcolor="#FFFFFF">
              <tr>
                <td width="373" class="subtitulo1">Buscar Caravanas Finalizadas </td>
                <td width="427" class="barratitulo"><img src="<%=BASEURL%>/images/titulo.gif" width="32" height="20"></td>
              </tr>
  </table>
<table width="99%" border="1" align="center" cellpadding="1" cellspacing="1" class="fondotabla">
  <!--DWLayoutTable-->
  <tr class="fila">
    <td height="22" class="letra_resaltada" > <div align="center"> <span class="filaresaltada">Caravana No.</span>        
      <input name="noCaravana" type="text" class="listmenu" id="noCaravana" value="<%=noCaravana%>" size="10">&nbsp;
	  <img src="<%=BASEURL%>/images/botones/iconos/chulo.gif"  height="10"  onClick="buscarCaravanaFinalizadas('<%=BASEURL%>','fin')" style="cursor:hand">
</div></td>
  </tr>
  </table>
  </td>
  </tr>
  </table>
  <table width="498" border="2" align="center">
    <tr>
      <td>
	  <table width="99%" align="center" bgcolor="#FFFFFF">
              <tr>
                <td width="373" height="22" class="subtitulo1">Caravanas Finalizadas </td>
                <td width="427" class="barratitulo"><img src="<%=BASEURL%>/images/titulo.gif" width="32" height="20"></td>
              </tr>
  </table>
  <table width="99%" border="1" align="center" bordercolor="#999999" bgcolor="#F7F5F4">
  <tr class="tblTitulo">
    <td width="66" height="21" > <span class="letra_resaltada">Caravana No. </span></td>
    <td width="77" ><span class="letra_resaltada">Inicio</span></td>
	<td width="79" ><span class="letra_resaltada">Finalizaci&oacute;n</span></td>
    <td width="107" > <span class="letra_resaltada">Planillas Asociadas </span></td>    
    <td width="75" class="letra_resaltada" >Caravana Origen </td>
    </tr>
  <%

    String ori = "Ninguna";	
    if(datos.size()>0){  
    for (int i=0; i<datos.size(); i++) {	
        Caravana c =  (Caravana) datos.elementAt(i);		
        String fin = "Sin finalizar";
  	    if (!c.getFecfin().equals("0099-01-01"))
 	        fin = c.getFecfin();
  %>
  <tr class="<%=(i % 2 == 0 )?"filagris":"filaazul"%>"><a href="javascript:modificarCaravana('<%=BASEURL%>','<%=c.getNumcaravana()%>','<%=c.getFecfin()%>')">
    <td width="66" height="25" class="bordereporte"> <%=c.getNumcaravana()%></td>    
    <td width="77" class="bordereporte"><%=c.getFecinicio()%></td>
    <td width="79" class="bordereporte"><%=fin%></td>
    <td width="107" class="bordereporte"><select name="planillas" class="listmenu" id="planillas">
          <%Vector plani = model.caravanaSVC.getVectorPlanillasCaravana(c.getNumcaravana());
           for (int j = 0; j< plani.size(); j++) {
           %>
	  <option><%=plani.elementAt(j)%></option>	  
	  <%}%>
    </select>
	</td>
    <%if(c.getCaravanaorig()!=0) ori=Integer.toString(c.getCaravanaorig());%><td width="75"><%=ori%></td> </a> 
    <%}
  }%>
</table>
</td>
</tr>
</table>
<div align="center">
  <input name="Submit" type="submit" class="boton" value="Buscar" style="visibility:hidden">
</div>
</form>
<br>
<table width="698" border="0" align="center">
  <tr>
    <td height="27" colspan="5" align="center"> &nbsp; <img src="<%=BASEURL%>/images/botones/salir.gif"  height="21" name="imgsalir"  onMouseOver="botonOver(this);" onClick="parent.close();" onMouseOut="botonOut(this);" style="cursor:hand"> </td>
  </tr>
</table>
</div>
</body>
</html>
