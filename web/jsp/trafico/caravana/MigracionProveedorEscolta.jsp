<%@ page contentType="text/html; charset=iso-8859-1" language="java" errorPage="" %>
<%@page import="com.tsp.operation.model.beans.*, java.util.*"%>
<%@include file="/WEB-INF/InitModel.jsp" %>
<%
  //String noCaravana =  (request.getParameter("var")!=null)?request.getParameter("var"):""; 
%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<title>REPORTE DE CONTROL DE CHEQUES</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<script type='text/javascript' src="<%=BASEURL%>/js/validar.js"></script>
<script type='text/javascript' src="<%=BASEURL%>/js/boton.js"></script>

<script type='text/javascript' src="<%=BASEURL%>/js/date-picker.js"></script>
<link href="../css/estilostsp.css" rel="stylesheet" type="text/css">
<link href="<%=BASEURL%>/css/estilostsp.css" rel="stylesheet" type="text/css">
</head>

<body>
<div id="capaSuperior" style="position:absolute; width:100%; height:100px; z-index:0; left: 1px; top: 1px;">
 <jsp:include page="/toptsp.jsp?encabezado=Migracion de Proveedores de Escolta"/>
</div>
<div id="capaCentral" style="position:absolute; width:100%; height:87%; z-index:0; left: 0px; top: 100px; overflow: scroll;">
<form name="buscar" method="post" id="busq"  action="<%= CONTROLLER %>?estado=MigracionProveedor&accion=Escolta">
<table border="2" align="center" width="504">
  <tr>
    <td>
	<table width="100%" align="center">
  <tr>
    <td width="55%"  class="subtitulo1"><p align="left">Migracion de Proveedores de Escoltas </p></td>
    <td width="45%"  class="barratitulo"><img src="<%=BASEURL%>/images/titulo.gif" width="32" height="20"></td>
  </tr>
</table>
  <table width="99%" align="center">
    <tr class="fila">
      <td width="117" height="28"><div align="center" class="letra_resaltada">FECHA INICIAL</div></td>
      <td width="119"><input name="fechai" type="text" class="textbox" id="fecini" size="14" readonly>
        <img src="<%=BASEURL%>/images/cal.gif" width="16" height="16" align="absmiddle" style="cursor:hand " onclick="if(self.gfPop)gfPop.fPopCalendar(document.buscar.fecini);return false;" HIDEFOCUS></td>
      <td width="100"><div align="center" class="letra_resaltada">FECHA FINAL</div></td>
      <td width="131"><input name="fecfin" type="text" class="textbox" id="fecfin" size="14">
        <span class="Letras"><a href="javascript:void(0)" onclick="jscript: show_calendar('fechaf');" HIDEFOCUS> </a><img src="<%=BASEURL%>/images/cal.gif" width="16" height="16" align="absmiddle" style="cursor:hand " onclick="if(self.gfPop)gfPop.fPopCalendar(document.buscar.fecfin);return false;" HIDEFOCUS></span></td>
    </tr>
  </table>
  </td>
  </tr>
  </table>
<br>
<center>
    <img src="<%=BASEURL%>/images/botones/aceptar.gif" name="mod"  height="21" onClick="validarRptMov(buscar);" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" style="cursor:hand">&nbsp;
	<img src="<%=BASEURL%>/images/botones/salir.gif"  name="imgsalir"  height="21" onMouseOver="botonOver(this);" onClick="parent.close();" onMouseOut="botonOut(this);" style="cursor:hand">
</center>
<br>
<%String msg = (request.getParameter("msg")!=null)?request.getParameter("msg"):"";%>
<%if(!msg.equals("OK") && !msg.equals("")){%>
	<p>
   <table width="380" border="2" align="center">
     <tr>
    <td><table width="100%"  border="1" align="center"  bordercolor="#F7F5F4" bgcolor="#FFFFFF">
      <tr>
        <td width="229" align="center" class="mensajes"> <%=msg%></td>
        <td width="29" background="<%=BASEURL%>/images/cuadronaranja.JPG">&nbsp;</td>
        <td width="58">&nbsp;</td>
      </tr>
    </table></td>
  </tr>
</table>
  </p>
   <%}%>

</form>
</div>
<iframe width=188 height=166 name="gToday:normal:agenda.js:gfPop:plugins.js" id="gToday:normal:agenda.js:gfPop:plugins.js" src="<%=BASEURL%>/js/Calendario/ipopeng.htm" scrolling="no" frameborder="0" style="visibility:visible; z-index:999; position:absolute; left:-500px; top:0px;"> </iframe>    
</body>
</html>

