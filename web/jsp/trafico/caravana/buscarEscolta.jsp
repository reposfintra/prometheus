<%@page import="com.tsp.operation.model.beans.*"%>
<%@page import="com.tsp.operation.model.*"%>
<%@page import="java.util.*"%>
<%@include file="/WEB-INF/InitModel.jsp" %>
<%
  String mensaje = request.getParameter("msg")!=null ? request.getParameter("msg"):"";
  String variable = request.getParameter("planilla")!=null ? request.getParameter("planilla"):"";
  String campo =  request.getParameter("campo")!=null   ?  request.getParameter("campo")  : "";
  String opcion   =  request.getParameter("opcion")!=null  ?  request.getParameter("opcion") : "";
                
  Vector vecEscoltas = (Vector) request.getAttribute("info");
  //Vector vecEscoltas = model.escoltaVehiculoSVC.getVecEscoltas();
  Escolta e = new Escolta();
  if (vecEscoltas!=null && vecEscoltas.size()>0)
      e = (Escolta)vecEscoltas.elementAt(0);    
  else //AMATURANA
  		vecEscoltas = new Vector();  
%>
<html>
<head>
<title>Buscar Vehiculo Escolta</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<script type='text/javascript' src="<%=BASEURL%>/js/caravanas.js"></script>
<script type='text/javascript' src="<%=BASEURL%>/js/reporte.js"></script>
<script type='text/javascript' src="<%=BASEURL%>/js/boton.js"></script>
<link href="../../../css/caravana.css" rel="stylesheet" type="text/css">
<link href="<%=BASEURL%>/css/caravana.css" rel="stylesheet" type="text/css">
</head>

<body onResize="redimensionar()" onLoad="redimensionar()">
<div id="capaSuperior" style="position:absolute; width:100%; height:100px; z-index:0; left: 0px; top: 0px;">
<jsp:include page="/WEB-INF/toptsp.jsp?encabezado=Buscar Escoltas"/>
</div>
<div id="capaCentral" style="position:absolute; width:100%; height:85%; z-index:0; left: 0px; top: 100px; overflow: scroll;"> 
<form name="buscar" method="post" id="busq" action="javascript:buscarEscolta('<%=CONTROLLER%>')">
<table width="653" border="2" align="center">
    <tr>
      <td>
<table width="99%" align="center" bgcolor="#FFFFFF">
              <tr>
                <td width="417" height="22" class="subtitulo1">BUSCAR ESCOLTAS </td>
                <td width="411" class="barratitulo"><img src="<%=BASEURL%>/images/titulo.gif" width="32" height="20"></td>
              </tr>
  </table>
<table width="99%" align="center" class="fondotabla">
  <!--DWLayoutTable-->
  <tr class="fila">
    <td height="13" align="center" class="filaresaltada">Seleccione la opcion de busqueda   </td>
    </tr>
  <tr class="fila">
    <td height="29" align="center" class="filaresaltada">PLACA
      <input name="opcion" type="radio" value="placa"> 
      PLANILLA
      <input name="opcion" type="radio" value="planilla">
     
      &nbsp;<input name="placa" type="text" class="textbox" id="placa" value="<%=variable%>" size="10" align="middle">      &nbsp;<img src="<%=BASEURL%>/images/botones/iconos/lupa.gif" width="31" height="27"  style="cursor:hand" onClick="buscarEscolta('<%=CONTROLLER%>')" align="middle"></td>
    </tr> 
  </table>
  </td>
  </tr>
  </table>
  <table width="653" border="2" align="center">
    <tr>
      <td>
	  <table width="100%" align="center" bgcolor="#FFFFFF">
              <tr>
                <td width="489" height="22" class="subtitulo1">Escoltas Asignados Acutualmente </td>
                <td width="505" class="barratitulo"><img src="<%=BASEURL%>/images/titulo.gif" width="32" height="20"></td>
              </tr>
       </table>
  <table width="99%" border="1" align="center" bordercolor="#999999" bgcolor="#F7F5F4">
  <tr class="tblTitulo">
    <td width="66" height="21" > <span class="letra_resaltada">Placa No. </span></td>
    <td width="133" ><span class="letra_resaltada">Conductor</span></td>
	<td width="96" >Fecha Asignaci&oacute;n</td>
    <td width="87" > <span class="letra_resaltada">Asignado a la Caravana No. </span></td>    
    <%if (e!=null && e.getCedconductor()!=null) {%><%if (e.getCedconductor().equals("0")){%>
    <td width="88" class="letra_resaltada" >Planilla Asignada </td>
    <%}%><%}%>
    <td width="57" class="letra_resaltada" >Liberar</td>
    <td width="51" class="letra_resaltada" >Eliminar</td>

  </tr>
  <%if (e!=null && e.getCedconductor()!=null) {
    String car = "Ninguna";
  	if (!e.getCedconductor().equals("0"))
 	    car = e.getCedconductor();
        String fecA = e.getPropietario();        
       
  %>
  <%for (int i=0; i<vecEscoltas.size(); i++) {
        Escolta c = (Escolta) vecEscoltas.elementAt(i);
  %>
  <tr class="filagris" style="cursor:hand" >
    <td width="66" height="17" class="bordereporte"><a href="javascript:abrirInfoPlaca(840,600,'<%=CONTROLLER%>','<%=c.getPlaca()%>')"><%=c.getPlaca()%></a>
</td>    
    <td width="133" class="bordereporte"><%=c.getConductor()%></td>
    <td width="96" class="bordereporte"><%=fecA!=null ? fecA:""%></td>
    <td width="87" class="bordereporte"><%if(!car.equals("Ninguna")){%><a href="javascript:modificarCaravana('<%=BASEURL%>','<%=car%>','0099-01-01')"><%}%><%=car%></a></td>
   <%if(car.equals("Ninguna")){%><td width="88"><a href="javascript:abrirInfoPlanilla(860,600,'<%=CONTROLLER%>','<%=c.getNumpla()%>','<%=c.getRazon_exclusion()%>')"><%=c.getNumpla()%></a></td>

   <%}%>
     <td  class="bordereporte" align="center"><a href="javascript:eliminarAsignacion('<%=CONTROLLER%>','<%=c.getPlaca()%>','<%=campo%>','<%=opcion%>','liberar','<%=car%>')" ><img src="<%=BASEURL%>/images/botones/iconos/exit1.png" width="18" height="20" border="0" alt="Liberar el Escolta"></a></td>
      <td  class="bordereporte" align="center"><a href="javascript:eliminarAsignacion('<%=CONTROLLER%>','<%=c.getPlaca()%>','<%=campo%>','<%=opcion%>','eliminar','<%=car%>')" ><img src="<%=BASEURL%>/images/delete.gif" width="18" height="20" border="0" alt="Elimina el registro creado"></a></td>
 
  <%}%>
<%}%>
</table>
</td>
</tr>
</table>
  <input name="Submit" type="submit" class="boton" value="Buscar" style="visibility:hidden">
</form>
<br>
<%if(!mensaje.equals("")){%>
<table width="382" border="2" align="center">
      <tr>
        <td><table width="100%"  border="1" align="center"  bordercolor="#F7F5F4" bgcolor="#FFFFFF">
            <tr>
              <td width="229" align="center" class="mensajes"><%=mensaje%></td>
              <td width="29" background="<%=BASEURL%>/images/cuadronaranja.JPG">&nbsp;</td>
              <td width="58">&nbsp;</td>
            </tr>
        </table></td>
      </tr>
</table>
<%}%>
<br>
<table width="698" border="0" align="center">
  <tr>
      <td height="27" colspan="5" align="center">	  &nbsp; 
	  <img src="<%=BASEURL%>/images/botones/salir.gif"  height="21" name="imgsalir"  onMouseOver="botonOver(this);" onClick="parent.close();" onMouseOut="botonOut(this);" style="cursor:hand">
	  </td>
  </tr>
</table>
</div>
</body>
</html>
