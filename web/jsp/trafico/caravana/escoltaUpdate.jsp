<%@page contentType="text/html"%>
<%@page pageEncoding="UTF-8"%>
<%@page errorPage="/error/ErrorPage.jsp"%>
<%@page import="com.tsp.operation.model.beans.*, java.util.*"%>
<%@include file="/WEB-INF/InitModel.jsp" %>
<%@ taglib uri="http://jakarta.apache.org/taglibs/input-0.90" prefix="input" %>
<%String placa ="", conductor="";
	if(request.getParameter("placa")!=null){
		placa = request.getParameter("placa");
		conductor = request.getParameter("conductor");
	}
	if(request.getParameter("conductor")!=null){
		
	}
  String msg = (request.getParameter("msg")!=null)?request.getParameter("msg"):"";
  Placa placaEsc = (Placa)request.getAttribute("placa");
  if (placaEsc==null){
      placaEsc = new Placa();
  }
%>
<html>
<head>
<title>Untitled Document</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link href="../../../css/estilostsp.css" rel="stylesheet" type="text/css"> 
<link href="<%=BASEURL%>/css/estilostsp.css" rel="stylesheet" type="text/css"> 
<script type='text/javascript' src="<%=BASEURL%>/js/Validacion.js"></script> 
<script type='text/javascript' src="<%=BASEURL%>/js/boton.js"></script> 

</head>

<body onResize="redimensionar()" onLoad="redimensionar()">
<div id="capaSuperior" style="position:absolute; width:100%; height:100px; z-index:0; left: 0px; top: 0px;">
<jsp:include page="/WEB-INF/toptsp.jsp?encabezado=Modificar Vehiculo Escolta"/>
</div>
<div id="capaCentral" style="position:absolute; width:100%; height:85%; z-index:0; left: 0px; top: 100px; overflow: scroll;"> 
<%//nuevo   
        String tipdoc="012"; 
		String act="005";
		//*****
%>
<form name="frmbuscarplaca" action="<%=CONTROLLER%>?estado=Placa&accion=Search&cmd=show&pag=ESC" method="post">
<table border="2" align="center" width="720">
  <tr>
    <td>
<table width="99%"  align="center"   >
  <tr>
    <td width="392" height="22"  class="subtitulo1"><p align="left">Buscar Placa </p></td>
    <td width="392"  class="barratitulo"><img src="<%=BASEURL%>/images/titulo.gif" width="32" height="20"></td>
  </tr>
</table>
<table width="99%"  align="center" >
<tr class="fila">
    <td width="176" height="27">Digite la placa a Modificar </td>
    <td width="513" ><input name="idplaca" type="text" class="textbox" title="Numero de la placa" maxlength="10"> 
      <img src="<%=BASEURL%>/images/botones/buscar.gif"  name="imgsalir"  height="21" align="absmiddle" style="cursor:hand" onClick="frmbuscarplaca.submit();" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);"></td>
</tr>
</table>
</td>
</tr>
</table>
</form>
<input:form name="frmplaca" action="controller?estado=Placa&accion=Update&cmd=show&pag=ESC" method="post" bean="placa">
   
  <table width="720" border="2" align="center">
  <tr>
    <td>

<table width="99%"  align="center"   >
  <tr>
    <td width="50%" height="22"  class="subtitulo1"><p align="left">Caracteristicas del vehiculo </p></td>
    <td width="392"  class="barratitulo"><img src="<%=BASEURL%>/images/titulo.gif" width="32" height="20"></td>
  </tr>
</table>
<table width="99%"  align="center" >
<tr class="fila">
    <td height="20">Estado</td>
    <td height="20" colspan="9"><% TreeMap estados = new TreeMap();
                estados.put("INACTIVO", ""); 
                estados.put("ACTIVO", "A");
             %> 
            <input:select name="reg_status" options="<%=estados%>" attributesText="style='width:15%;' class='textbox'"/></td>
    </tr>
  <tr class="fila">
    <td width="69" height="29">Placa</td>
    <td width="165" ><input:text name="placa" attributesText="maxlength='10' size='8' readonly class='textbox'"/>
      <img  src="<%=BASEURL%>/images/botones/iconos/obligatorio.gif" width="10" height="10"></td>
    <td width="90" >Tipo</td>
    <td colspan="3" nowrap><input:text name="tipo" attributesText="maxlength='12' size='13' readonly class='textbox'"/>
      <img  src="<%=BASEURL%>/images/botones/iconos/obligatorio.gif" width="10" height="10"></td>
    </tr>
  <tr class="fila">
    <td width="69" height="20">Clase</td>
    <td width="165" ><input:text name="clase" attributesText="maxlength='5' size='15' class='textbox'"/>
      <img  src="<%=BASEURL%>/images/botones/iconos/obligatorio.gif" width="10" height="10"></td>
    <td width="90" >Modelo </td>
    <td width="136" nowrap><input:text name="modelo" attributesText="class='listmenu' size='10' onkeypress='return acceptNum(event)' maxlength='4''"/>
      <img  src="<%=BASEURL%>/images/botones/iconos/obligatorio.gif" width="10" height="10"></td>
    <td width="77">Marca</td>
    <td width="136" nowrap><% TreeMap marcas = model.marcaService.listar(); 
         %>
        <input:select name="marca"  attributesText="style='width:100%;' class='listmenu'" options="<%=marcas%>" default="<%=placaEsc.getMarca()%>"/></td>
    </tr>

  <tr class="fila">
    <td width="69" height="21">Carroceria</td>
    <td width="165"  nowrap><% TreeMap carrs = model.carrService.listar();%>
        <input:select name="carroceria" attributesText="style='width:100%;' class='listmenu'" options="<%=carrs%>" default="<%=placaEsc.getCarroceria()%>"/></td>
    <td width="90" >Serial Motor</td>
    <td colspan="3" nowrap> <input:text name="nomotor" attributesText="maxlength='15' class='textbox'"/>
      <img  src="<%=BASEURL%>/images/botones/iconos/obligatorio.gif" width="10" height="10"></td>
    </tr>
  <tr class="fila">
    <td width="69" height="20">Color</td>
    <td width="165" ><% TreeMap colores = model.colorService.listar();%>
        <input:select name="color" options="<%=colores%>" attributesText="style='width:90%;' class='listmenu'" default="<%=placaEsc.getColor()%>"/></td>
    <td >Numero Rin</td>
    <td colspan="3"><input:text name="numero_rin" attributesText="maxlength='5' size='6' class='textbox'"/></td>
    </tr>
</table>
</td>
</tr>
</table>
<table width="720" border="2" align="center">
  <tr>
    <td>
<table width="99%"   align="center"> 
  <tr>
    <td width="50%" height="22"  class="subtitulo1"><p align="left">Informaci&oacute;n del Propietario </p></td>
    <td width="414"  class="barratitulo"><img src="<%=BASEURL%>/images/titulo.gif" width="32" height="20"></td>
  </tr>
</table>
<table width="99%"  align="center" >
  <tr class="fila">
    <td width="92" height="20">Agencia</td>
    <td width="143" ><% TreeMap agencia = model.agenciaService.listar();%>
            <input:select name="agencia" attributesText="style='width:92%;' class='listmenu'" options="<%=agencia%>" default="<%=placaEsc.getAgencia()%>"/><img  src="<%=BASEURL%>/images/botones/iconos/obligatorio.gif" width="10" height="10"></td>
    <td width="109" >Poliza SOAT</td>
    <td width="125"><input:text name="polizasoat" attributesText="maxlength='15'  size='16' class='textbox'"/></td>
    <td width="94">Fecha de Vencimiento</td>
    <td width="110"><input:text name="venseguroobliga" attributesText="maxlength='15'  size='11' class='textbox' readonly"/>
      <span class="comentario"><a href="javascript:void(0)" onClick="if(self.gfPop)gfPop.fPopCalendar(document.frmplaca.venctarjetaoper);return false;" hidefocus><img name="popcal" align="absmiddle" src="<%=BASEURL%>/js/calendartsp/calbtn.gif" width="16" height="16" border="0" alt=""></a> </span></td>
    </tr>
  <tr class="fila">
    <td width="92" height="30">Cedula Propietario </td>
    <td width="143" ><input:text name="propietario" attributesText="maxlength='15' size='16' class='textbox'"/><img  src="<%=BASEURL%>/images/botones/iconos/obligatorio.gif" width="10" height="10"></td>
    <td width="109" >Cedula Conductor </td>
    <td colspan="4">
	<input:text name="conductor" attributesText="maxlength='15' size='16' class='textbox'"/>
      <img  src="<%=BASEURL%>/images/botones/iconos/obligatorio.gif" width="10" height="10">      </td>
    </tr>
</table>
</td>
</tr>
</table>
 <br>
<table width="611" border="0" align="center">
  <tr>
    <td height="27" align="center">         
      <img src="<%=BASEURL%>/images/botones/aceptar.gif" name="mod"  height="21" onClick="return validarPlacaEscolta();" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" style="cursor:hand"> &nbsp; <img src="<%=BASEURL%>/images/botones/cancelar.gif"  name="imgsalir"  height="21" onMouseOver="botonOver(this);" onClick="frmplaca.reset();" onMouseOut="botonOut(this);" style="cursor:hand">&nbsp; <img src="<%=BASEURL%>/images/botones/salir.gif"  name="imgsalir"  height="21" onMouseOver="botonOver(this);" onClick="parent.close();" onMouseOut="botonOut(this);" style="cursor:hand"> <input type="hidden" name="grupo_equipo" value="O">
      <input type="hidden" name="grupos" value="O"></td>
  </tr>
</table>
<%if(!msg.equals("")){%>
	<p>
   <table width="407" border="2" align="center">
     <tr>
    <td><table width="100%"   align="center"  >
      <tr>
        <td width="229" align="center" class="mensajes"><%=msg%></td>
        <td width="29" background="<%=BASEURL%>/images/cuadronaranja.JPG">&nbsp;</td>
        <td width="58">&nbsp;</td>
      </tr>
    </table></td>
  </tr>
</table>
  </p>
   <%}%>
</input:form>
</div>
<iframe width=188 height=166 name="gToday:datetime:<%=BASEURL%>/js/calendartsp/agenda.js:gfPop:<%=BASEURL%>/js/calendartsp/plugins.js" id="gToday:datetime:<%=BASEURL%>/js/calendartsp/agenda.js:gfPop:<%=BASEURL%>/js/calendartsp/plugins.js" src="<%=BASEURL%>/js/calendartsp/ipopeng.htm" scrolling="no" frameborder="0" style="visibility:visible; z-index:999; position:absolute; left:-500px; top:0px;">
</iframe>
</body>
</html>
<script>
function agregarReferencia(CONTROLLER) {
	if(frmplaca.placa.value==""){
		alert("Debe digitar una placa");
		frmplaca.placa.focus();
	} else {
		window.open(CONTROLLER+"?estado=Referencia&accion=Insert&tiporeferencia=EA&opcion=LANZAR&documento="+frmplaca.placa.value,'Trafico','width=700,height=350,scrollbars=yes,resizable=yes,top=10,left=65');
	}
}
</script>