<%@page contentType="text/html"%>
<%@page session="true"%>
<%@page errorPage="../error/ErrorPage.jsp"%>
<%@page import="java.util.*"%>
<%@page import="com.tsp.operation.model.*"%>
<%@page import="com.tsp.util.*"%>
<%@page import="com.tsp.operation.model.beans.*, java.util.*"%>
<%@include file="/WEB-INF/InitModel.jsp"%>

<%
  VistaCaravana vista = new VistaCaravana();
  String oriI="",desI="";
  String fechaAct = model.caravanaSVC.getFechaActual();  
  int mes = Integer.parseInt(fechaAct.substring(5,7));
  int numC = model.caravanaSVC.getNoCaravana();
  String  agregar = (request.getParameter("agregar")!=null)?request.getParameter("agregar"):"hidden";
  String escolta = (request.getParameter("escolta")!=null)?request.getParameter("escolta"):"hidden";
  String url = (request.getParameter("url")!=null)?request.getParameter("url"):"caravana.jsp";
  String pos = (request.getParameter("pos")!=null)?request.getParameter("pos"):"";
  String posesc = (request.getParameter("posesc")!=null)?request.getParameter("posesc"):"";
  String msg = (request.getParameter("msg")!=null)?request.getParameter("msg"):"";
  String idAgencia = model.usuario.getId_agencia();
  String first = (request.getParameter("frt")!=null)?request.getParameter("frt"):"";
  String fin = "hidden";
  Vector datos =  null;
  if (first.equals("1")) {
	  model.caravanaSVC.limpiarVectorActualPlanillas();
	  model.escoltaVehiculoSVC.limpiarVectorEscoltaActual();
  }
  if(!pos.equals("")){
      /*Posiciones de Select actuales*/
      oriI = request.getParameter("oriI");
      desI = request.getParameter("desI");
      model.caravanaSVC.eliminarPosicionVectorCaravanaActual(Integer.parseInt(pos));	  	  
  }if(!posesc.equals("")){
      oriI = request.getParameter("oriI");
      desI = request.getParameter("desI");
      model.escoltaVehiculoSVC.eliminarPosicionVectorEscoltaCaravanaActual(Integer.parseInt(posesc));	  
  }
  model.agenciaService.cargarAgencias();
  Vector agc = model.agenciaService.getVectorAgencias();%>  
<html>
<head>
<title>Conformaci�n de Caravana</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<script type='text/javascript' src="<%=BASEURL%>/js/date-picker.js"></script>
<script type='text/javascript' src="<%=BASEURL%>/js/caravanas.js"></script>
<script type='text/javascript' src="<%=BASEURL%>/js/boton.js"></script>
<script type='text/javascript' src="<%=BASEURL%>/js/reporte.js"></script>
<link href="<%=BASEURL%>/css/caravana.css" rel="stylesheet" type="text/css">
<link href="../../../css/caravana.css" rel="stylesheet" type="text/css">
</head>
<body  <%if(request.getParameter("reload")!=null){%>onLoad="window.close();window.opener.location.href='<%=BASEURL%>/jsp/trafico/caravana/caravana.jsp?agregar=<%=agregar%>&escolta=<%=escolta%>'"<%}%>>
<div id="capaSuperior" style="position:absolute; width:100%; height:100px; z-index:0; left: 0px; top: 0px;">
<jsp:include page="/WEB-INF/toptsp.jsp?encabezado=Crear Caravana"/>
</div>
<div id="capaCentral" style="position:absolute; width:100%; height:85%; z-index:0; left: 0px; top: 100px; overflow: scroll;"> 
<form id="caravana" name="caravana" method="post" action="<%=CONTROLLER%>?estado=Caravana&accion=Insert">
<table width="886" border="2" align="center">
    <tr>
      <td>
	  <table width="99%" align="center">
              <tr>
                <td width="50%" height="22" class="subtitulo1">CONFORMACION DE CARAVANAS Y ASIGNACI&Oacute;N DE ESCOLTA(S)</td>
                <td width="411" class="barratitulo"><img src="<%=BASEURL%>/images/titulo.gif" width="32" height="20"></td>
              </tr>
  </table>
	  <table width="99%" align="center">
	<tr class="fila">
    <td width="430"><div align="center"><span class="filaresaltada">Proxima Caravana  No.</span>          
        <input name="numC" type="text" class="textbox" id="numC" value="<%=numC%>"  size="8" readonly>
    </div></td>
    <td width="424" rowspan="2" valign="middle" class="filaresaltada"><div align="center"></div>      
      <div align="center">Asignar  Escolta(s)<span class="letras"><a href="javascript:abrirVentanaBusq(700,470,'<%=BASEURL%>/jsp/trafico/caravana/busquedaesc.jsp?pag=cara')"><img src="<%=BASEURL%>/images/buscar.gif" width="17"  border="0" align="top" title="Buscar escoltas disponibles"></a></span></div></td>
  </tr>
  <tr class="fila">
    <td height="20" ><div align="center" class="fila">
      <div align="left">Dia de creaci&oacute;n de la caravana <%=fechaAct.substring(8,10)%> de <%=Util.NombreMes(mes)%> de <%=fechaAct.substring(0,4)%></div>
    </div></td>
    </tr>
</table>
<table width="99%" align="center">
              <tr>
                <td width="50%" height="22" class="subtitulo1">Agregar Vehiculos a la Caravana</td>
                <td width="50%" class="barratitulo"><img src="<%=BASEURL%>/images/titulo.gif" width="32" height="20"></td>
              </tr>
  </table>
<table width="99%" align="center">
  <tr class="fila">
    <td width="199" rowspan="3"> <div align="center" class="filaresaltada">Incluir por No Planilla</div></td>
    <td width="213" rowspan="3">      <table width="100%"  border="0">
        <tr>
          <td width="61%">&nbsp;</td>
          <td width="39%" class="filaresaltada"><div align="center">Manual</div></td>
          </tr>
        <tr>
          <td><input class="textbox" name='planilla' type='text' id="planillax" size="15">            <img src="<%=BASEURL%>/images/botones/iconos/chulo.gif"  height="10" title="Agregar vehiculo por Numero de  planilla" onClick="buscarPlanilla('<%=CONTROLLER%>','caravana')" style="cursor:hand "></td>
          <td><div align="center">
              <input name="plamanual" type="checkbox" id="plamanual" value="OK">
          </div></td>
          </tr>
      </table>      </td>
    <td colspan="4" class="tblTitulo"><div align="center">Seleccionar Vehiculos por Agencia </div></td>
    </tr>
  <tr class="fila">
    <td width="99" no><div align="center" class="filaresaltada"></div>      
      <div align="center"><span class="letras"><span class="comentario"><span class="filaresaltada">FECHA INICIAL DESPACHO.</span>
      </span></span></div></td>
    <td width="107"><span class="comentario">
      <input name='fecini' type='text' class="textbox" id="fecini" value='<%=fechaAct%>' size="13" readonly>
      <img src="<%=BASEURL%>/images/cal.gif" alt="" name="popcal" width="16" height="16" border="0" align="top"  style="cursor:hand" onclick="if(self.gfPop)gfPop.fPopCalendar(document.caravana.fecini);return false;" HIDEFOCUS></span></td>
    <td width="85"><div align="center"><span class="letras"><span class="comentario"><span class="filaresaltada">FECHA FIN DESPACHO. </span>
          </span></span></div></td>
    <td width="109"><span class="comentario">
      <input name='fecfin' type='text' class="textbox" id="fecfin2" value='<%=fechaAct%>' size="13" readonly>
<img src="<%=BASEURL%>/images/cal.gif" alt="" name="popcal" width="16" height="16" border="0" align="top"  style="cursor:hand" onclick="if(self.gfPop)gfPop.fPopCalendar(document.caravana.fecfin);return false;" HIDEFOCUS></span></td>
  </tr>
  <tr class="fila">
    <td height="28" colspan="2" align="center"><div align="center" class="filaresaltada">AGENCIA</div>      </td>
    <td colspan="2"><span class="comentario"><span class="letras">
      <select name="agencia" id="select2" class="textbox">
        <%for (int k=0; k<agc.size(); k++) {
                Agencia ag = (Agencia) agc.elementAt(k);
                
          %>
        <option value="<%=ag.getId_agencia()%>" <% if(ag.getId_agencia().equals(idAgencia)){%>selected<%}%>><%=ag.getNombre()%></option>
        <%}%>
      </select>
      <a href="javascript:abrirVentanaBusq(900,650,'<%=BASEURL%>/jsp/trafico/caravana/busqueda.jsp?pag=cara')"><img src="<%=BASEURL%>/images/buscar.gif" title="Buscar vehiculos despachados" width="17" height="19" border="0" align="top" style="cursor:hand"></a> </span></span></td>
    </tr>
</table></td>
</tr>
</table>
<%if(agregar.equals("visible")){
	Vector vec = model.caravanaSVC.getVectorCaravanaActual ();
	String posicion_menor = "";
	String origenes = "";
	int tama�o_via = 1000;
	for(int j=0; j< vec.size (); j++){
		VistaCaravana car = (VistaCaravana) vec.elementAt ( j );
		if( tama�o_via > car.getVia ().length () ){
			tama�o_via = car.getVia ().length ();
			posicion_menor = car.getRuta_pla ();
			origenes = car.getOripla();
		}
	}
%>
<table width="886" border="2" align="center">
    <tr>
      <td><table width="99%" align="center">
              <tr>
                <td height="22" colspan="2" class="subtitulo1">Vehiculos de la Caravana Actual </td>
                <td class="barratitulo" colspan="2"><img src="<%=BASEURL%>/images/titulo.gif" width="32" height="20">
                </td>
              </tr>
			  <tr class="filaresaltada">
			  	<td width="20%" align="right">
					Origen Caravana
				</td>
			  	<td width="30%">
					<select name="origen_caravana" id="origen_caravana" class='textbox' onchange="validarUbicacionCaravana(document.caravana)">
						<%         
							Vector via1 = model.caravanaSVC.getVectorRutas();
							if (via1!=null) { 
								for(int l=0;l<via1.size();l++) {
									Via via = (Via) via1.get(l);%>
									<option value="<%=via.getCodigo()%>"<%=origenes.equals(via.getCodigo())?"selected":""%>><%=via.getCiudad()%>
								<%}
							}
						 %>
					</select>
				</td>
			  	<td width="20%" align="right">
					Destino Caravana
				</td>								
			  	<td width="30%">
					<select name="destino_caravana" id="destino_caravana" class='textbox' onchange="validarUbicacionCaravana(document.caravana)">
						<% 
							if (via1!=null) { 
								for(int l=0;l<via1.size();l++) {
									Via via = (Via) via1.get(l);%>
									<option value="<%=via.getCodigo()%>"<%=( l == (via1.size()-1) )?"selected":""%> ><%=via.getCiudad()%>
								<%}
							}
						 %>
					</select>
				</td>				
			  </tr>
  </table>
<table width="99%" border="1" align="center"  bordercolor="#999999" bgcolor="#F7F5F4">
  <tr class="tblTitulo">
    <td width="49">Placa</td>
	<td width="72">Planilla</td>
    <td width="141">Cliente</td>
    <td width="139">Origen - Destino </td>
	<td width="127">Conductor</td>
    <td width="136">Stdjob</td>    
    <td width="88">Fecha despacho</td>
    <td width="36">&nbsp;</td>
  </tr>  
  <%
    datos = model.caravanaSVC.getVectorCaravanaActual();
	if (datos.size()>1) {
	    fin = "";
        vista = (VistaCaravana) datos.elementAt(0);
	} else {
   	    fin = "hidden";
    }
	for(int i=0; i<datos.size(); i++) {
	    vista = (VistaCaravana) datos.elementAt(i);
  %>
   <tr class="<%=(i % 2 == 0 )?"filagris":"filaazul"%>">
    <td height="24" class="bordereporte"><a href="javascript:abrirInfoPlaca(860,615,'<%=CONTROLLER%>','<%=vista.getPlaveh()%>')"><%=vista.getPlaveh()%>
        </a>      </td>
    <td class="bordereporte"><a href="<%= ( vista.getNumrem()!=null && vista.getNumrem().length()!=0 ) ? "javascript:javascript:abrirInfoPlanilla(860,700,'" + CONTROLLER + "','" + vista.getNumpla() + "','" + vista.getNumrem()  + "')" : "JavaScript:void(0);alert('No hay informaci�n disponible. La planilla pertenece a un despacho manual.');"%>"><%=vista.getNumpla()%></a>
        <input name="numpla<%=i%>" type="hidden" id="numpla<%=i%>" value="<%=vista.getNumpla()%>">
    </td>
    <td class="bordereporte"><a href="<%= ( vista.getNumrem()!=null && vista.getNumrem().length()!=0 ) ? "javascript:abrirInfoCliente(700,300,'" + BASEURL  + "','" + vista.getCodcli() + "','" + vista.getStd_job_no() + "')" : "JavaScript:void(0);alert('No hay informaci�n disponible. La planilla pertenece a un despacho manual.');" %>"><%=vista.getNomcli()%></a></td>    
    <td class="bordereporte"><%=vista.getOrinom()+"-"+vista.getDesnom()%>
    </td>
    <td class="bordereporte"><% if ( vista.getNomConductor().compareTo("NO REGISTRA") != 0 ) {%><a href="javascript:abrirInfoConductor(860,700,'<%=CONTROLLER%>','<%=vista.getCedConductor()%>')"><%=vista.getNomConductor()%></a><% } else { out.print(vista.getNomConductor()); }%></td>
    <td class="bordereporte"><% if ( vista.getNumrem()!=null && vista.getNumrem().length()!=0 ) { %><a href="javascript:abrirInfoStdJob(860,700,'<%=CONTROLLER%>','<%=vista.getStd_job_no()%>')"><%=vista.getStd_job_desc()%></a><% } else { %><%=vista.getStd_job_desc()%><%}%></td>
    <td class="bordereporte">
      <%=vista.getFecpla()%>
</td>
    <td class="bordereporte" align="center"><a href="javascript:eliminar('<%=i%>','<%=BASEURL%>','<%=escolta%>')"><img src="<%=BASEURL%>/images/delete.gif" width="18" height="20" border="0"></a>
    </td>
  </tr>
    <%}%>
</table>
</td>
</tr>
</table><p><%}%>
    <% Vector esc = model.escoltaVehiculoSVC.getVectorEscoltaCaravanaActual();	
	if(esc.size()>0 && datos.size()>0) {%>
</p>
<table width="886" border="2" align="center">
    <tr>
      <td>
<table width="99%" align="center">
              <tr>
                <td width="50%" height="22" class="subtitulo1">Escoltas Asignados a la Caravana Actual </td>
                <td width="50%" class="barratitulo"><img src="<%=BASEURL%>/images/titulo.gif" width="32" height="20"></td>
              </tr>
  </table>
<table width="99%" border="1" align="center" style="visibility:<%=escolta%> ">
  <tr class="tblTitulo">
    <td width="74" height="15">Placa</td>
    <td width="223">Propietario</td>
    <td width="223">Conductor</td>
    <td width="159">Origen</td>
    <td width="121">Destino</td>
    <td width="25">&nbsp;</td>
  </tr>
  <%
    
    for (int i=0; i<esc.size(); i++) {
        Escolta e = (Escolta) esc.elementAt(i);        
   %>
  <tr class="<%=(i % 2 == 0 )?"filagris":"filaazul"%>" >
    <td height="24" class="bordereporte"><a href="javascript:abrirInfoPlaca(860,700,'<%=CONTROLLER%>','<%=e.getPlaca()%>')"><%=e.getPlaca()%></a>
      <input name="placaesc<%=i%>" type="hidden" id="placaesc<%=i%>" value="<%=e.getPlaca()%>"></td>
    <td class="bordereporte"><%=e.getPropietario()%> </td>
    <td class="bordereporte"><a href="javascript:abrirInfoConductor(860,700,'<%=CONTROLLER%>','<%=e.getCedconductor()%>')"><%=e.getConductor()%></a> </td>
    <td class="bordereporte"> <select name="origen<%=i%>" id="origen<%=i%>" class='textbox' onchange="validarUbicacion(document.caravana,'<%=i%>')">
                    <%          if(!posesc.equals("") || !pos.equals("")){
 									vista.setOripla(oriI);
								}
                                Vector ubicaciones = model.caravanaSVC.getVectorRutas();
                                if (ubicaciones!=null) { 
                                    for(int l=0;l<ubicaciones.size();l++) {
                                        Via via = (Via) ubicaciones.get(l);%>
                    <option value="<%=via.getCodigo()%>"<%=vista.getOripla().equals(via.getCodigo())?"selected":""%>><%=via.getCiudad()%>
                    <%}
                                }
                             %>
              </select></td>
    <td class="bordereporte"> <select name="destino<%=i%>" id="destino<%=i%>" class='textbox' onchange="validarUbicacion(document.caravana,'<%=i%>')">					
                    <%              if(!posesc.equals("") || !pos.equals("")){
 									    vista.setDespla(desI);
								    }
                                    if (ubicaciones!=null) { 
                                    for(int l=0;l<ubicaciones.size();l++) {
                                        Via via = (Via) ubicaciones.get(l);%>
                    <option value="<%=via.getCodigo()%>" <%=( l == (ubicaciones.size()-1) )?"selected":""%>><%=via.getCiudad()%></option>
                    <%}
                                }
                             %>
                  </select></td>
    <td class="bordereporte"><div align="center"><a href="javascript:eliminarEsc('<%=i%>','<%=BASEURL%>')" ><img src="<%=BASEURL%>/images/delete.gif" width="18" height="20" border="0"></a></div></td>
  </tr>
  <%} %>
</table>
</td>
</tr>
</table>
<%} else{%>
    <input name="origen" value="" type="hidden">    <input name="destino" value="" type="hidden">
<%}%>
<%if(!fin.equals("hidden")){%>
<br>
<table width="860" align="center">
  <tr>
      <td height="27"  colspan="5"><div align="center">          
	  <img src="<%=BASEURL%>/images/botones/aceptar.gif" name="mod" height="21" onClick="caravana.submit()" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" style="cursor:hand">&nbsp; 
	  <img src="<%=BASEURL%>/images/botones/salir.gif"  height="21" name="imgsalir"  onMouseOver="botonOver(this);" onClick="parent.close()" onMouseOut="botonOut(this);" style="cursor:hand"></div></td>
  </table>
<%} else {%>
<br>
<table width="698" border="0" align="center">
  <tr>
      <td height="27" colspan="5" align="center">	  &nbsp; <img src="<%=BASEURL%>/images/botones/salir.gif"  height="21" name="imgsalir"  onMouseOver="botonOver(this);" onClick="parent.close()" onMouseOut="botonOut(this);" style="cursor:hand">
	  </td>
    </tr>
</table>
<%}%>


	


<p>&nbsp;  </p>
<p>    
</p>
</form>
<% if (msg.equals("ERRORB")){
%>
<script>alert('El NO. de Planilla no existe')</script>
<%} if (msg.equals("ERRORE")) {%>
<script>alert('La planilla ya fue asignada a una caravana o a un escolta')</script>
<%}%>
<% if (msg.equals("OKK")){
%>
<script>alert('La caravana ha sido creada exitosamente')</script>
<%}%>
<% if (msg.equals("RUTER")){
%>
<script>alert('No se han agregado algunos vehiculos.\nVerifique que la ruta nueva se encuentre contenida en la ruta principal ')</script>
<%}%>
<% if (msg.equals("V1")){
%>
<script>
if (confirm('Una caravana solo se puede conformar por 2 o mas vehiculos\n�Desea solo asignar un escolta a un vehiculo?') )
  	window.open('<%=BASEURL%>/jsp/trafico/caravana/asignarEscolta.jsp?agregar=visible&escolta=visible');
	else 
	    a = "";
</script>
<%}%>
</div>
<iframe width=188 height=166 name="gToday:normal:agenda.js:gfPop:plugins.js" id="gToday:normal:agenda.js:gfPop:plugins.js" src="<%=BASEURL%>/js/Calendario/ipopeng.htm" scrolling="no" frameborder="0" style="visibility:visible; z-index:999; position:absolute; left:-500px; top:0px;"> </iframe>    
</body>
</html>

