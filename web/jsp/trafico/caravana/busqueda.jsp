<%@ page contentType="text/html; charset=iso-8859-1" language="java" errorPage="" %>
<%@page import="com.tsp.operation.model.beans.*, java.util.*"%>
<%@include file="/WEB-INF/InitModel.jsp" %>
<%@taglib uri="http://jsptags.com/tags/navigation/pager" prefix="pg" %>
<%
   String style = "simple";
   String position =  "bottom";
   String index =  "center";
   int maxPageItems = 8;
   int maxIndexPages = 20;
  String pag  =  (request.getParameter("pag")!=null)?request.getParameter("pag"):"";
  String numC  =  (request.getParameter("numC")!=null)?request.getParameter("numC"):"";
  String fecini  =  (request.getParameter("fecI")!=null)?request.getParameter("fecI"):"";
  String fecfin  =  (request.getParameter("fecF")!=null)?request.getParameter("fecF"):"";
  String agencia =  (request.getParameter("ag")!=null)?request.getParameter("ag"):"";
  String first =  (request.getParameter("frt")!=null)?request.getParameter("frt"):"";
  if (first.equals("1")) {
      model.caravanaSVC.getVehiculosDespachados(fecini,fecfin,agencia);
  }
  Vector vc =  model.caravanaSVC.getVectorVehiculos();
  int tam =0;
%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<title>BUSQUEDA DE VEHICULOS POR AGENCIA</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<script type='text/javascript' src="<%=BASEURL%>/js/caravanas.js"></script>
<script type='text/javascript' src="<%=BASEURL%>/js/reporte.js"></script>
<script type='text/javascript' src="<%=BASEURL%>/js/boton.js"></script>
<link href="../../../css/caravana.css" rel="stylesheet" type="text/css">
<link href="<%=BASEURL%>/css/caravana.css" rel="stylesheet" type="text/css">
</head>

<body>
<form name="busq"  method="post" action="<%=CONTROLLER%>?estado=Caravana&accion=Busqueda&pag=<%=pag%>&numC=<%=numC%>">
<table width="860" border="2" align="center">
    <tr>
      <td width="869" height="161">
	  <table width="100%" align="center" class="encabezado">
              <tr>
                <td width="50%" class="subtitulo1">Vehiculos despachados por Agencia </td>
                <td width="427" class="barratitulo"><img src="<%=BASEURL%>/images/titulo.gif" width="32" height="20"></td>
              </tr>
  </table>
      <table width="99%" border="1" align="center" bordercolor="#999999" bgcolor="#F7F5F4" style="visibility:visible ">
        <tr class="tblTitulo">
          <td width="42">PLACA</td>
          <td width="66">PLANILLA</td>
          <td width="168">CLIENTE</td>
          <td width="153">RUTA</td>
          <td width="166">CONDUCTOR</td>
          <td width="117">STDJOB</td>
          <td width="76">FECHA DESPACHO </td>
          <td width="29">&nbsp;</td>
        </tr>
        <pg:pager
    items="<%=vc.size()%>"
    index="<%= index %>"
    maxPageItems="<%= maxPageItems %>"
    maxIndexPages="<%= maxIndexPages %>"
    isOffset="<%= true %>"
    export="offset,currentPageNumber=pageNumber"
    scope="request">
        <%tam = vc.size();
	  for (int j = offset.intValue(), l = Math.min(j + maxPageItems, vc.size()); j < l; j++){
          VistaCaravana vista = (VistaCaravana) vc.elementAt(j);%>
        <pg:item>
        <tr class="<%=(j % 2 == 0 )?"filagris":"filaazul"%>" >
          <td class="bordereporte"><%=vista.getPlaveh()%> </td>
          <td class="bordereporte"><%=vista.getNumpla()%> </td>
          <td class="bordereporte"><%=vista.getNomcli()%> </td>
          <td class="bordereporte"><%=vista.getOrinom()+"-"+vista.getDesnom()%> </td>
          <td class="bordereporte"><%=vista.getNomConductor()%> </td>
          <td class="bordereporte"><%=vista.getStd_job_desc()%> </td>
          <td class="bordereporte"> <%=vista.getFecpla()%> </td>
          <td class="bordereporte"><div align="center">
              <input type="checkbox" name="fila<%=j%>" value="<%=j%>">
          </div></td>
        </tr>
        </pg:item>
        <%}%>
        <tr class="fila">
          <td td height="66" colspan="8" nowrap align="center"> <pg:index>
            <jsp:include page="/WEB-INF/jsp/googlesinimg.jsp" flush="true"/>      
    </pg:index> </td>
        </tr>
        </pg:pager>
        
      </table></td>
    </tr>
</table>
<br>
<table width="860" border="0" align="center">
  <tr>
          <td >
            <div align="center">
              <img src="<%=BASEURL%>/images/botones/aceptar.gif" name="mod" height="21" onClick="busq.submit()" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" style="cursor:hand">&nbsp; <img src="<%=BASEURL%>/images/botones/salir.gif"  height="21" name="imgsalir"  onMouseOver="botonOver(this);" onClick="parent.close();" onMouseOut="botonOut(this);" style="cursor:hand">
              <input type="hidden" name="tam" value="<%=tam%>">
          </div></td>
    </tr>
</table>

</form>
</body>
</html>
