<%@ page contentType="text/html; charset=iso-8859-1" language="java" errorPage="" %>
<%@page import="com.tsp.operation.model.beans.*, java.util.*"%>
<%@include file="/WEB-INF/InitModel.jsp" %>
<%@taglib uri="http://jsptags.com/tags/navigation/pager" prefix="pg" %>
<%
   String style = "simple";
   String position =  "bottom";
   String index =  "center";
   int maxPageItems = 10;
   int maxIndexPages = 10;
   System.out.println("...................... REQUEST BUSCAR ESCOLTAS: " + request.getQueryString());
   String pag  =  (request.getParameter("pag")!=null)?request.getParameter("pag"):"";
   String first =  (request.getParameter("frt")!=null)?request.getParameter("frt"):"";
   String numC = request.getParameter("caravana")!=null ? request.getParameter("caravana") : "";
  if( request.getParameter("search")!=null ){
  	  model.escoltaVehiculoSVC.listarEscoltasSearch(request.getParameter("search"));
  } else if (first.equals("1")) {
      model.escoltaVehiculoSVC.listarEscoltas();
  }  
  Vector datos =  model.escoltaVehiculoSVC.getVectorEscoltas();
%>

<html>
<head>
<title>BUSQUEDA DE VEHICULOS POR AGENCIA</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<script type='text/javascript' src="<%=BASEURL%>/js/reporte.js"></script>
<script type='text/javascript' src="<%=BASEURL%>/js/boton.js"></script>
<link href="../../../css/caravana.css" rel="stylesheet" type="text/css">
<link href="<%=BASEURL%>/css/caravana.css" rel="stylesheet" type="text/css">
</head>

<body>
<form name="busq"  method="post" action="<%=CONTROLLER%>?estado=Escolta&accion=Busqueda&pag=<%=pag%>&caravana=<%=numC%>">
<table width="626" border="2" align="center">
    <tr>
      <td>

<table width="99%" align="center" bgcolor="#FFFFFF">
              <tr>
                <td width="311" class="subtitulo1">Escoger Vehiculo(s) Escoltas para la Caravana </td>
                <td width="285" class="barratitulo"><img src="<%=BASEURL%>/images/titulo.gif" width="32" height="20"></td>
              </tr>
  </table>
<table width="99%" align="center" bgcolor="#FFFFFF">
              <tr class="filaresaltada">
                <td width="131" class="filaresaltada">Buscar conductor: </td>
                <td width="465" class="letra"><input name="conductorSearch" type="text" class="textbox" id="conductorSearch" size="40">
                <span class="comentario"><img src="<%=BASEURL%>/images/buscar.gif"  title="Buscar escoltas disponibles"width="17" height="19" border="0" align="top" onClick="window.location.href = '<%=BASEURL%>/jsp/trafico/caravana/busquedaesc.jsp?<%= request.getQueryString()%>&search=' + document.busq.conductorSearch.value"></span></td>
          </tr>
  </table>
  <table width="99%" border="1" align="center"  bordercolor="#999999" bgcolor="#F7F5F4">
    <tr class="tblTitulo">
      <td width="77">PLACA</td>      
      <td width="239">PROPIETARIO</td>
      <td width="259">CONDUCTOR VEHICULO </td>
      <td width="28">&nbsp;</td>
    </tr>
	  
	  <pg:pager
    items="<%=datos.size()%>"
    index="<%= index %>"
    maxPageItems="<%= maxPageItems %>"
    maxIndexPages="<%= maxIndexPages %>"
    isOffset="<%= true %>"
    export="offset,currentPageNumber=pageNumber"
    scope="request">
	<%
	  for (int i = offset.intValue(), l = Math.min(i + maxPageItems, datos.size()); i < l; i++){
          Escolta e = (Escolta) datos.elementAt(i);%>
  <pg:item>
    <tr class="<%=(i % 2 == 0 )?"filagris":"filaazul"%>" >
      <td height="22" class="bordereporte"><%=e.getPlaca()%> </td>
      
      <td class="bordereporte"><%=e.getPropietario()%>
            </td>
      <td class="bordereporte"><%=e.getConductor()%>
      </a>                </td>
      <td class="bordereporte"><div align="center"><span class="comentario"><span class="letras">
        <input type="checkbox" name="fila<%=i%>" value="<%=i%>">
      </span></span></div></td>
    </tr>
	</pg:item>
    <%}%>
	<tr class="fila">
        <td td height="20" colspan="8" nowrap align="center">          <pg:index>
                <jsp:include page="/WEB-INF/jsp/googlesinimg.jsp" flush="true"/>
        </pg:index>
        </td>
    </tr>
    </pg:pager>
  </table>
  </td>
  </tr>
  </table>
  <br><table width="382" border="0" align="center">
  <tr>
      <td height="28" colspan="5"><div align="center">     
       <img <%if (datos.size()==0) {%>src="<%=BASEURL%>/images/botones/aceptarDisable.gif" <%} else {%>src="<%=BASEURL%>/images/botones/aceptar.gif" <%}%>name="mod" height="21" <%if (datos.size()>0) {%>onClick="busq.submit()" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" style="cursor:hand"<%}%>>&nbsp;
       <img src="<%=BASEURL%>/images/botones/salir.gif"  height="21" name="imgsalir"  onMouseOver="botonOver(this);" onClick="parent.close();" onMouseOut="botonOut(this);" style="cursor:hand"></div></td>
    </tr>
</table>

</form>
</body>
</html>
