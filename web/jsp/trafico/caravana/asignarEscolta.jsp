<%@page contentType="text/html"%>
<%@page session="true"%>
<%@page errorPage="../error/ErrorPage.jsp"%>
<%@page import="java.util.*"%>
<%@page import="com.tsp.operation.model.*"%>
<%@page import="com.tsp.util.*"%>
<%@page import="com.tsp.operation.model.beans.*, java.util.*"%>
<%@include file="/WEB-INF/InitModel.jsp"%>

<%
  VistaCaravana vista = new VistaCaravana();
  String oriI="",desI="";
  String fechaAct = model.caravanaSVC.getFechaActual();
  String error = (request.getParameter("error")!=null)?request.getParameter("error"):"hidden";
  int numC = model.caravanaSVC.getNoCaravana();
  String  agregar = (request.getParameter("agregar")!=null)?request.getParameter("agregar"):"hidden";
  String escolta = (request.getParameter("escolta")!=null)?request.getParameter("escolta"):"hidden";
  String url = (request.getParameter("url")!=null)?request.getParameter("url"):"caravana.jsp";
  String pos = (request.getParameter("pos")!=null)?request.getParameter("pos"):"";
  String posesc = (request.getParameter("posesc")!=null)?request.getParameter("posesc"):"";
  String msg = (request.getParameter("msg")!=null)?request.getParameter("msg"):"";
  String idAgencia = model.usuario.getId_agencia();  
  String cerrar = (request.getParameter("cerrar")!=null)?request.getParameter("cerrar"):"";
  if(cerrar.equals("true")){%>
     <script>
      parent.close();
     </script>
<%}
  if(!pos.equals("")){
      oriI = request.getParameter("oriI");
      desI = request.getParameter("desI");
      model.caravanaSVC.eliminarPosicionVectorCaravanaActual(Integer.parseInt(pos));
  }if(!posesc.equals("")){
      oriI = request.getParameter("oriI");
      desI = request.getParameter("desI");
      model.escoltaVehiculoSVC.eliminarPosicionVectorEscoltaCaravanaActual(Integer.parseInt(posesc));
  }
  model.agenciaService.cargarAgencias();
  Vector datos = model.caravanaSVC.getVectorCaravanaActual();
  Vector agc = model.agenciaService.getVectorAgencias(); 
%>
<html>
<head>
<title>Asignar escolta a un vehiculo</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<script type='text/javascript' src="<%=BASEURL%>/js/date-picker.js"></script>
<script type='text/javascript' src="<%=BASEURL%>/js/caravanas.js"></script>
<script type='text/javascript' src="<%=BASEURL%>/js/boton.js"></script>
<script type='text/javascript' src="<%=BASEURL%>/js/reporte.js"></script>
<link href="<%=BASEURL%>/css/caravana.css" rel="stylesheet" type="text/css">
<link href="../../../css/caravana.css" rel="stylesheet" type="text/css">
</head>

<body <%if(request.getParameter("reload")!=null){%>onLoad="window.opener.location.href='<%=BASEURL%>/jsp/trafico/caravana/asignarEscolta.jsp?agregar=<%=agregar%>&escolta=<%=escolta%>'; window.close();"<%}%>><br>

<div id="capaSuperior" style="position:absolute; width:100%; height:100px; z-index:0; left: 0px; top: 0px;">
<jsp:include page="/WEB-INF/toptsp.jsp?encabezado=Asignaci�n de Escoltas"/>
</div>
<div id="capaCentral" style="position:absolute; width:100%; height:85%; z-index:0; left: 0px; top: 100px; overflow: scroll;"> 
<form id="asig" name="caravana" method="post" action="<%=CONTROLLER%>?estado=Escolta&accion=Asignacion">
<table width="886" border="2" align="center">
    <tr>
      <td>
	  <table width="100%" align="center" class="encabezado">
              <tr>
                <td width="50%" height="22" class="subtitulo1">ASIGNACI&Oacute;N DE ESCOLTAS A UN VEHICULO </td>
                <td width="411" class="barratitulo"><img src="<%=BASEURL%>/images/titulo.gif" width="32" height="20"></td>
              </tr>
  </table>
<table width="100%" align="center" cellpadding="0" cellspacing="0">
  <tr class="fila">
    <td width="210" rowspan="3"><div align="center" class="filaresaltada"> Incluir vehiculo por No. Planilla</div></td>
    <td width="206" rowspan="3"><table width="100%"  border="0">
        <tr>
          <td width="70%">&nbsp;</td>
          <td width="30%" class="filaresaltada"><div align="center">Manual</div></td>
        </tr>
        <tr>
          <td><input class="textbox" name="plani" type='text' id="plani" size="15">
            <img src="<%=BASEURL%>/images/botones/iconos/lupa.gif"  width="17" height="19" title="Agregar vehiculo por Numero de  planilla"  onClick="buscarPlanillaAsig('<%=CONTROLLER%>')" style="cursor:hand "> </td>
          <td><div align="center">
            <input name="plamanual" type="checkbox" id="plamanual" value="OK">
          </div></td>
        </tr>
      </table></td>
    <td colspan="4"><div align="center" class="filaresaltada">INCLUIR VEHICULO POR AGENCIA</div></td>
    </tr>
  <tr class="fila">
    <td width="90"><div align="center" class="filaresaltada">FECHA INICIAL DESPACHO.</div>      </td>
    <td width="110"><span class="comentario">
      <input name='fecini' type='text' class="textbox" id="fecini" value='<%=fechaAct%>' size="13" readonly>
     <img src="<%=BASEURL%>/images/cal.gif" alt="" name="popcal" width="16" height="16" border="0" align="top"  style="cursor:hand" onclick="if(self.gfPop)gfPop.fPopCalendar(document.caravana.fecini);return false;" HIDEFOCUS></a></span></td>
    <td width="90"><div align="center"><span class="letras"><span class="comentario"><span class="filaresaltada">FECHA FIN DESPACHO. </span>
          </span></span></div></td>
    <td width="120"><span class="comentario">
      <input name='fecfin' type='text' class="textbox" id="fecfin2" value='<%=fechaAct%>' size="13" readonly>
      <img src="<%=BASEURL%>/images/cal.gif" alt="" name="popcal" width="16" height="16" border="0" align="top"  style="cursor:hand" onclick="if(self.gfPop)gfPop.fPopCalendar(document.caravana.fecfin);return false;" HIDEFOCUS></span></td>
  </tr>
  <tr class="fila">
    <td height="30" colspan="2" align="center" class="filaresaltada">      
      AGENCIA</td>
    <td colspan="2"><div align="center" class="filaresaltada"></div>      <span class="comentario"><span class="letras">
      <select name="agencia" id="select" class="textbox">
        <%for (int k=0; k<agc.size(); k++) {
                Agencia ag = (Agencia) agc.elementAt(k);
                
          %>
        <option value="<%=ag.getId_agencia()%>" <%if(ag.getId_agencia().equals(idAgencia)){%>selected<%}%>><%=ag.getNombre()%></option>
        <%}%>
      </select>
      <a href="javascript:abrirVentanaBusq(900,600,'<%=BASEURL%>/jsp/trafico/caravana/busqueda.jsp?pag=escolta')"><img src="<%=BASEURL%>/images/buscar.gif" width="17" height="19" border="0" align="top" style="cursor:hand" title="Buscar vehiculos despachados"></a> </span>
      </span></td>
    </tr>
</table>
<table width="100%" align="center" class="encabezado">
              <tr>
                <td width="50%" height="22" class="subtitulo1">Asignar Escolta </td>
                <td width="411" class="barratitulo"><img src="<%=BASEURL%>/images/titulo.gif" width="32" height="20"></td>
              </tr>
  </table>
<table width="100%" align="center" cellpadding="0" cellspacing="0">
	<tr class="fila" align="center">
    <td height="19">     
      <div align="center"></div>      <span class="filaresaltada">Buscar Escoltas Disponibles</span> <a href="javascript:abrirVentanaBusq(700,600,'<%=BASEURL%>/jsp/trafico/caravana/busquedaesc.jsp?pag=escolta')"><img src="<%=BASEURL%>/images/buscar.gif" width="17" height="19" border="0" align="top"></a></td>
    </tr>
</table>
</td>
</tr>
</table>

<%if(datos.size()>0){
vista = (VistaCaravana) datos.elementAt(0);%>
<table width="886" border="2" align="center">
    <tr>
      <td>
<table width="100%" align="center" class="encabezado">
              <tr>
                <td width="80%" height="22" class="subtitulo1">Vehiculo Agregado </td>
                <td width="441%" class="barratitulo"><img src="<%=BASEURL%>/images/titulo.gif" width="32" height="20"></td>
              </tr>			  
  </table>
<table width="99%" border="1" align="center" bordercolor="#999999" bgcolor="#F7F5F4">
  <tr class="tblTitulo">
    <td width="41">PLACA</td>
	<td width="55">PLANILLA</td>
    <td width="163">CLIENTE</td>
    <td width="157">ORIGEN - DESTINO</td>
	<td width="171">CONDUCTOR</td>
    <td width="133">STDJOB</td>    
    <td width="71">FECHA DESPACHO </td>
    <td width="26">&nbsp;</td>
  </tr>  
  <%
	if (datos.size()>0) {
	for(int i=0; i<datos.size(); i++){
	    vista = (VistaCaravana) datos.elementAt(i);
  %>
   <tr class="<%=(i % 2 == 0 )?"filagris":"filaazul"%>">
    <td height="22"  class="bordereporte"><a href="javascript:abrirInfoPlaca(860,700,'<%=CONTROLLER%>','<%=vista.getPlaveh()%>')"><%=vista.getPlaveh()%>
        </a>
      </td>
    <td class="bordereporte"><a href="<%= ( vista.getNumrem()!=null && vista.getNumrem().length()!=0 ) ? "javascript:javascript:abrirInfoPlanilla(860,700,'" + CONTROLLER + "','" + vista.getNumpla() + "','" + vista.getNumrem()  + "')" : "JavaScript:void(0);alert('No hay informaci�n disponible. La planilla pertenece a un despacho manual.');"%>"><%=vista.getNumpla()%></a>
      <input name="numpla<%=i%>" type="hidden" id="numpla<%=i%>" value="<%=vista.getNumpla()%>"></td>
    <td class="bordereporte"><a href="<%= ( vista.getNumrem()!=null && vista.getNumrem().length()!=0 ) ? "javascript:abrirInfoCliente(700,300,'" + BASEURL  + "','" + vista.getCodcli() + "','" + vista.getStd_job_no() + "')" : "JavaScript:void(0);alert('No hay informaci�n disponible. La planilla pertenece a un despacho manual.');" %>"><%=vista.getNomcli()%></a></td>
    <td class="bordereporte"><%=vista.getOrinom()+"-"+vista.getDesnom()%>
    </td>
    <td class="bordereporte"><% if ( vista.getNomConductor().compareTo("NO REGISTRA") != 0 ) {%><a href="javascript:abrirInfoConductor(860,700,'<%=CONTROLLER%>','<%=vista.getCedConductor()%>')"><%=vista.getNomConductor()%></a><% } else { out.print(vista.getNomConductor()); }%></td>
        <td class="bordereporte"><% if ( vista.getNumrem()!=null && vista.getNumrem().length()!=0 ) { %><a href="javascript:abrirInfoStdJob(860,700,'<%=CONTROLLER%>','<%=vista.getStd_job_no()%>')"><%=vista.getStd_job_desc()%></a><% } else { %><%=vista.getStd_job_desc()%><%}%></td>
    <td class="bordereporte">
      <%=vista.getFecpla()%>
</td>
    <td class="bordereporte"><div align="center"><a href="javascript:eliminarAsignar('<%=i%>','<%=BASEURL%>')"><img src="<%=BASEURL%>/images/delete.gif" width="18" height="20" border="0"></a>
    </div></td>
  </tr>
    <%}
	}%>
</table>
</td>
</tr>
</table>
<%}%>
<%Vector esc = model.escoltaVehiculoSVC.getVectorEscoltaCaravanaActual();	
if(esc.size()>0 && datos.size()>0) {%>
   

<table width="886" border="2" align="center">
    <tr>
      <td>
<table width="100%" align="center" class="encabezado">
              <tr>
                <td width="50%"  colspan="2" height="22" class="subtitulo1">Escolta Asignado</td>
                <td width="411" colspan="2" class="barratitulo"><img src="<%=BASEURL%>/images/titulo.gif" width="32" height="20"></td>
              </tr>

  </table>
<table width="99%" border="1" align="center" bordercolor="#999999" bgcolor="#F7F5F4" style="visibility:<%=escolta%> ">
  <tr class="tblTitulo">
    <td width="64" height="17">PLACA</td>
    <td width="215">PROPIETARIO</td>
    <td width="238">CONDUCTOR</td>
    <td width="148">ORIGEN</td>    
    <td width="131">DESTINO</td>
    <td width="29">&nbsp;</td>
  </tr>
  <%
    
    if (esc.size()>0){
	for (int i=0; i<esc.size(); i++) {
        Escolta e = (Escolta) esc.elementAt(i);        
   %>
  <tr class="<%=(i % 2 == 0 )?"filagris":"filaazul"%>">
    <td class="bordereporte"><a href="javascript:abrirInfoPlaca(860,700,'<%=CONTROLLER%>','<%=e.getPlaca()%>')"><%=e.getPlaca()%></a>      <input name="placaesc<%=i%>" type="hidden" id="pla<%=i%>" value="<%=e.getPlaca()%>"></td>
    <td class="bordereporte"><%=e.getPropietario()%>
      </td>
    <td class="bordereporte"><a href="javascript:abrirInfoConductor(860,700,'<%=CONTROLLER%>','<%=e.getCedconductor()%>')"><%=e.getConductor()%></a>
      </td>
    <td class="bordereporte"> <select name="origen<%=i%>" id="origen<%=i%>" class='textbox' onchange="validarUbicacion(document.caravana,'<%=i%>')">
                    <%          if(!posesc.equals("") || !pos.equals("")){
 									vista.setOripla(oriI);
								}
                                Vector ubicaciones = model.caravanaSVC.getVectorRutas();
                                String origen = vista.getOrigen()!=null?vista.getOrigen():"";
								if (ubicaciones!=null) { 
                                    for(int l=0;l<ubicaciones.size();l++) {
                                        Via via = (Via) ubicaciones.get(l);%>
                    <option value="<%=via.getCodigo()%>"<%=origen.equals(via.getCodigo())?"selected":""%>><%=via.getCiudad()%>
                    <%}
                                }
                             %>
                </select></td>
    <td class="bordereporte"><select name="destino<%=i%>" id="destino<%=i%>" class='textbox' onchange="validarUbicacion(document.caravana,'<%=i%>')">					
                    <%              if(!posesc.equals("") || !pos.equals("")){
 									    vista.setDespla(desI);
								    }
									String destino = vista.getDestino()!=null?vista.getDestino():"";
                                    if (ubicaciones!=null) { 
                                    for(int l=0;l<ubicaciones.size();l++) {
                                        Via via = (Via) ubicaciones.get(l);%>
                    <option value="<%=via.getCodigo()%>" <%=destino.equals(via.getCodigo())?"selected":""%>><%=via.getCiudad()%></option>
                    <%}
                                }
                             %>
                  </select></td>
    <td class="bordereporte"><div align="center"><span class="comentario"><span class="letras"><a href="javascript:eliminarEscAsignar('<%=i%>','<%=BASEURL%>')"><img src="<%=BASEURL%>/images/delete.gif" width="18" height="20" border="0"></a></span></span></div></td>
  </tr>    
   <%}
   }%>
</table>
</td>
</tr>
</table>
<br>
<table width="698" border="0" align="center">
  <tr>
      <td height="28" colspan="5" align="center">
	  <img src="<%=BASEURL%>/images/botones/aceptar.gif" name="mod" height="21" onClick="caravana.submit()" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" style="cursor:hand">&nbsp; 
	  <img src="<%=BASEURL%>/images/botones/salir.gif"  height="21" name="imgsalir"  onMouseOver="botonOver(this);" onClick="window.location.href('<%=CONTROLLER%>?estado=Escolta&accion=Asignacion&cerrar=true')" onMouseOut="botonOut(this);" style="cursor:hand">
	  </td>
    </tr>
</table>
<%
} else {%>
<br>
<table width="698" border="0" align="center">
  <tr>
      <td height="27" colspan="5" align="center">	  &nbsp; 
	  <img src="<%=BASEURL%>/images/botones/salir.gif"  height="21" name="imgsalir"  onMouseOver="botonOver(this);" onClick="window.location.href('<%=CONTROLLER%>?estado=Escolta&accion=Asignacion&cerrar=true');" onMouseOut="botonOut(this);" style="cursor:hand">
	  </td>
    </tr>
</table>
<%}%>
</form>
<%if(error.equals("MP1")){%><script>askData('No puede agregar mas de un vehiculo.�Desea crear una caravana?','<%=BASEURL%>')</script><%}%>
<%if(error.equals("ME1")){%><script>alert("No puede agregar mas de un escolta a un vehiculo");</script><%}%>
<%  if (msg.equals("ERRORB")){
%>
<script>alert('El NO. de Planilla no existe')</script>
<%} if (msg.equals("ERRORE")) {%>
<script>alert('La planilla ya fue asignada a un escolta')</script>
<%}%>
<%if (msg.equals("NONE")) {%>
<script>alert('El escolta fue asignado exitosamente')</script>
<%}%>
<iframe width=188 height=166 name="gToday:normal:agenda.js:gfPop:plugins.js" id="gToday:normal:agenda.js:gfPop:plugins.js" src="<%=BASEURL%>/js/Calendario/ipopeng.htm" scrolling="no" frameborder="0" style="visibility:visible; z-index:999; position:absolute; left:-500px; top:0px;"> </iframe>    
</div>
</body>
</html>
