<%@ page contentType="text/html; charset=iso-8859-1" language="java" errorPage="" %>
<%@page import="com.tsp.operation.model.beans.*, java.util.*"%>
<%@include file="/WEB-INF/InitModel.jsp" %>

<html>
<head>
<title>Buscar Caravanas</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<script type='text/javascript' src="<%=BASEURL%>/js/caravanas.js"></script>
<script type='text/javascript' src="<%=BASEURL%>/js/boton.js"></script>
<link href="../../../css/caravana.css" rel="stylesheet" type="text/css">
<link href="<%=BASEURL%>/css/caravana.css" rel="stylesheet" type="text/css">
</head>

<body onResize="redimensionar()" onLoad="redimensionar()">
<div id="capaSuperior" style="position:absolute; width:100%; height:100px; z-index:0; left: 0px; top: 0px;">
<jsp:include page="/WEB-INF/toptsp.jsp?encabezado=Buscar Caravanas"/>
</div>
<div id="capaCentral" style="position:absolute; width:100%; height:85%; z-index:0; left: 0px; top: 100px; overflow: scroll;"> 
<form name="buscar" method="post" id="busq" action="javascript:buscarCaravana('<%=BASEURL%>')">
<table width="498" border="2" align="center">
    <tr>
      <td>
	  <table width="99%" align="center" bgcolor="#FFFFFF">
              <tr>
                <td width="373" class="subtitulo1">Buscar Carvanas</td>
                <td width="427" class="barratitulo"><img src="<%=BASEURL%>/images/titulo.gif" width="32" height="20"></td>
              </tr>
  </table>
<table width="99%" border="1" align="center" cellpadding="0" cellspacing="0" bordercolor="#F7F5F4" bgcolor="#FFFFFF" >  
  <tr class="fila">
    <td height="22"><a href="<%=BASEURL%>/jsp/trafico/caravana/caravanasActuales.jsp">Caravanas en Proceso</a></td>
  </tr>
  <tr class="fila">
    <td height="22"><a href="<%=BASEURL%>/jsp/trafico/caravana/caravanasFinalizadas.jsp">Caravanas Finalizadas </a></td>
  </tr>
  </table>
  </td>
  </tr>
  </table>
  <div align="center">
  <input name="Submit" type="submit" class="boton" value="Buscar" style="visibility:hidden">
</div>
</form>
<br>
<table width="698" border="0" align="center">
  <tr>
    <td height="27" colspan="5" align="center"> &nbsp; <img src="<%=BASEURL%>/images/botones/salir.gif"  height="21" name="imgsalir"  onMouseOver="botonOver(this);" onClick="parent.close();" onMouseOut="botonOut(this);" style="cursor:hand"> </td>
  </tr>
</table>
</div>
</body>
</html>
