<!--
- Autor : Ing. Sandra M Escalante G
- Date  : 18 de Mayo
- Copyrigth Notice : Fintravalores S.A. S.A
-->
<%--
-@(#)
--Descripcion : Ayuda
--%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
"http://www.w3.org/TR/html4/loose.dtd">
<%@include file="/WEB-INF/InitModel.jsp"%>
<html>
<head>
<title>.: Ayuda - Descripcion de Campos. Anular Opcion</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link href="../../../css/estilostsp.css" rel="stylesheet" type="text/css">
<link href="<%= BASEURL %>/css/estilostsp.css" rel="stylesheet" type="text/css">
<script type='text/javascript' src="<%= BASEURL %>/js/boton.js"></script>
</head>

<body>
<br>
<table width="696"  border="2" align="center">
  <tr>
    <td width="811" height="" >
      <table width="100%" border="0" align="center">
        <tr  class="subtitulo">
          <td height="24" colspan="2" align="center">ANULAR OPCION</td>
        </tr>
		<tr class="subtitulo1">
          <td colspan="2"> DATOS DE LA OPCION</td>
        </tr>
        <tr>
          <td width="149" class="fila"> Nombre </td>
          <td width="525"  class="ayudaHtmlTexto">Nombre de la opci�n. S&oacute;lo Lectura. </td>
        </tr>
        <tr>
          <td width="149" class="fila"> Descripci�n </td>
          <td width="525"  class="ayudaHtmlTexto">Informaci�n adicional al nombre de la opci�n. S&oacute;lo Lectura. </td>
        </tr>
        <tr>
          <td class="fila">Ruta (URL) </td>
          <td  class="ayudaHtmlTexto">Informaci&oacute;n complementaria que muestra el contenido de la URL de la opci&oacute;n. </td>
        </tr>
    </table>    </td>
  </tr>
</table>
<br>
<center>
	<img src="<%=BASEURL%>/images/botones/salir.gif" name="c_salir" onClick="window.close();" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" align="absmiddle">
</center>
<br>
<p align="center" class="fuenteAyuda"> Fintravalores S. A. </p>
<br>
</body>
</html>