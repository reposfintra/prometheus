<!--
- Autor : Ing. Sandra M Escalante G
- Date  : 25 Abril 2005
- Copyrigth Notice : Fintravalores S.A. S.A
-->
<%--
-@(#)
--Descripcion : Pagina JSP, que se encarga de gestionar 
				el formulario para modificar una opcion del menu
--%>

<%-- Declaracion de librerias--%>
<%@page contentType="text/html"%>
<%@ page session="true"%>
<%@ page errorPage="../error/ErrorPage.jsp"%>
<%@page import="com.tsp.operation.model.beans.*, java.util.*"%>
<%@page import="com.tsp.operation.model.*"%>
<%@include file="/WEB-INF/InitModel.jsp"%>
<%@ taglib uri="http://jakarta.apache.org/taglibs/input-0.90" prefix="input" %>
<%
    String path = application.getRealPath(model.menuService.getRealUrl(request.getRequestURI()));
    String datos[] = model.menuService.getContenidoMenu(BASEURL,request.getRequestURI(),path);
%>
<html>
<head>
<title>.: Modificar Opcion</title>
<link href="<%= BASEURL %>/css/estilostsp.css" rel='stylesheet'>
<link href="../../../css/estilostsp.css" rel="stylesheet" type="text/css">
<script type='text/javascript' src="<%= BASEURL %>/js/validar.js"></script>
<script type='text/javascript' src="<%= BASEURL %>/js/boton.js"></script>
</head>
<body onresize="redimensionar()" onload = 'redimensionar()'>
<div id="capaSuperior" style="position:absolute; width:100%; height:100px; z-index:0; left: 0px; top: 0px;">
 <jsp:include page="/toptsp.jsp?encabezado=Menu - Modificar Opcion"/>
</div>
<div id="capaCentral" style="position:absolute; width:100%; height:87%; z-index:0; left: 0px; top: 100px; overflow: scroll;">
<%String nom="", desc="", url="", carp = "", pag="", otro="", nota="";
int subm = 0, padre = 0, niv = 0, opcion = 0, morden = 0;%>
<FORM name='forma' id='forma' method='POST' action="<%=CONTROLLER%>?estado=Menu&accion=ModificarOpcion&carpeta=/jsp/trafico/menu&pagina=ModificarOpcionMenu2.jsp" onSubmit="return validarCamposMenu('M');">
<table width="446" border="2" align="center">
  <tr>
    <td><table width="446" class="tablaInferior">
  <tr align="center">
    <td colspan="6"><table width="100%"  border="0" cellpadding="0" cellspacing="0" class="barratitulo">
  <tr>
    <td width="36%" class="subtitulo1">Modificar Menu\Opcion</td>
    <td width="64%"><img src="<%=BASEURL%>/images/titulo.gif" width="32" height="20" align="left"><%=datos[0]%></td>
  </tr>
</table>
</td>
  </tr>
	<%Menu m = model.menuService.getMenu();
	opcion = m.getIdopcion();
	nom = m.getNombre();
	desc = m.getDescripcion();
	url = m.getUrl();
	padre = m.getIdpadre();
	subm = m.getSubmenu();
	niv = m.getNivel();
	morden = m.getOrden();%>
  <tr class="fila">
    <td width="18">Nombre</td>
    <td width="204"><input name="nom" type="text" class="textbox" id="nom" value="<%=nom%>" size="30" maxlength="60" onKeyPress="soloAlfa(event);"></td>
    <td width="22">ID</td>
    <td width="40"><input name="opcion" type="text" class="textbox" id="opcion" value="<%=opcion%>" readonly size="8" maxlength="8"></td>
    <td width="57" nowrap>ID Padre</td>
    <td width="41"><input name="id_padre" type="text" class="textbox" id="id_padre" value="<%=padre%>" size="8" maxlength="8" onKeyPress="soloDigitos(event,'decNO');"></td>
  </tr>
  <tr class="fila">
    <td>Descripcion</td>
    <td colspan="3"><input name="desc" type="text" id="desc" class="textbox" value="<%=desc%>" size="45" maxlength="30" onKeyPress="soloAlfa(event);"></td>
    <td>Orden</td>
    <td><input name="orden" type="text" class="textbox" value="<%=morden%>" size="8" maxlength="4" onKeyPress="soloDigitos(event,'decNO');"></td>
  </tr>
  <%if ( (m.getSubmenu() == 2)){//hoja%>
  <tr class="fila" align="center">
    <td rowspan="2">Ruta</td>
    <td colspan="5">
	  <table width="97%" class="fila" border="1" cellpadding="1" cellspacing="1">
        <tr>
          <td width="28%">Archivo Enrutador</td>
          <td width="72%"><input name="textfield" type="text" class="textbox" value="MenuCargarAction.java" size="45" readonly></td>
        </tr>
        <tr>
          <td>Cabecera de Ruta</td>
          <td><input name="textfield2" type="text" class="textbox" value="../controller?estado=Menu&amp;accion=Cargar" size="45" readonly></td>
        </tr>
      </table>
    </td>
  </tr>
  <tr class="fila">
    <td colspan="5">
	<%url = m.getUrl();
	//verificar si es MenuCargar	
	String tip = url.substring(33, 39);
	if (tip.equals("Cargar")){
		String cola = url.substring(48, url.length());
	    int asp = cola.indexOf("&");
		carp = cola.substring(0, asp);
		cola = cola.substring(asp+1, cola.length());
    	int igual = cola.indexOf("=");
	    asp = cola.indexOf("&");
		if ( asp != -1){
			pag = cola.substring(igual+1, asp); 
			otro = cola.substring(asp, cola.length());
		}
		else {
			pag = cola.substring(igual+1, cola.length());
		}
	}else if (tip.equals("Enviar")){
		carp = "none";
		pag= "none";
		int ultig = url.lastIndexOf("&");
		otro = url.substring(ultig, url.length());
		nota = "Esta Opcion no trabaja con MenuCargarAction.java. La URL es: <br><font face='Courier new'>" + url + "</font>";
	}
	else{
		carp = "none";
		pag= "none";
		otro = "none";
		nota = "Esta Opcion no trabaja con MenuCargarAction.java. La URL es: <br><font face='Courier new'>" + url + "</font>";
	}%>
		
	  <table class="fila" width="95%" border="1" align="center" cellpadding="1" cellspacing="1">
        <tr>
          <td width="28%">Carpeta</td>
          <td width="72%" align="center">
            <input name="carp" value="<%=carp%>" type="text" class="textbox" size="30">
          </td>
        </tr>
        <tr>
          <td>Pagina</td>
          <td align="center">
            <input name="pag" value="<%=pag%>" type="text" class="textbox" size="30">
          </td>
        </tr>
        <tr>
          <td>Otros</td>
          <td align="center">
            <input name="otro" value="<%=otro%>" type="text" class="textbox" size="30">
          </td>
        </tr>
      </table>
	</td>
  </tr>
  <%}//if submenu%>
  <tr class="fila" align="center">
    <td colspan="6">
	<% if (nota.equals("")){ %>
			<input type="image" name="Submit" value="Ingresar" src="<%= BASEURL %>/images/botones/modificar.gif" align="absmiddle" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" style="cursor:hand ">
		<% }else{ %>
			<img src="<%=BASEURL%>/images/botones/modificarDisable.gif" name="c_nada" style="cursor:hand " align="absmiddle">
		<%}%>
            <img src="<%=BASEURL%>/images/botones/salir.gif" name="c_salir" onClick="window.close();" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" align="absmiddle" style="cursor:hand ">
	</td>
  </tr>
  <tr class="fila">
    <td colspan="6">
      <table width="100%" border="0" cellpadding="0" cellspacing="0">
        <tr class="subtitulo1" onMouseOver="bgColor= '#0092A6'" onMouseOut="bgColor='#4D71B0'" style="cursor:hand" 
			onClick="window.location='<%=CONTROLLER%>?estado=Menu&accion=Cargar&carpeta=/jsp/trafico/menu&pagina=ModificarOpcionMenu.jsp?msg=&marco=no&opcion=25'">
          <td width="8%"><img src="<%= BASEURL %>/images/menu-images/home.gif" border="0"></td>
          <td width="92%">[ FINTRAVALORES  S. A. ]</td>
        </tr>
        <tr>
          <td>&nbsp;</td>
          <td>
			<table width="100%"  cellspacing="0" cellpadding="0">
			<%Vector vmenu = model.menuService.getVMenu();
		      for (int i = 0; i < vmenu.size(); i++){
              	   Menu menu = (Menu) vmenu.elementAt(i);
		           int midop = menu.getIdopcion();
    		        String mnom = menu.getNombre();
        		    int midpa = menu.getIdpadre();            		
					String mnompa = menu.getPnombre();
					if ( midpa != 0 ){%>
              <tr class="letraresaltada" onMouseOver="bgColor= '#99cc99'" onMouseOut="bgColor='#EAFFEA'" 
			  	onClick="window.location='<%=CONTROLLER%>?estado=Menu&accion=CargaxPerfil&direccion=/jsp/trafico/menu/ModificarOpcionMenu2.jsp&idop=<%=midpa%>&msg=&cmd=modop'" style="cursor:hand"> 
                 <td width="5%"><img src="<%= BASEURL %>/images/menu-images/menu_folder_open.gif" border="0"></td>
                 <td width="79%"><font size="-2"><%=mnompa%></font></td>
				 <td width="16%"><font size="-2"><%=menu.getPorden()%></font></td>
              </tr>
              <%}%>
              <tr align="center"> 
                <td colspan="3" align="right">
				  <table width="95%" border="0" cellpadding="0" cellspacing="0">
                    <tr class="fila"> 
                      <td width="5%">
					  <%if(menu.getSubmenu() == 2){//hoja%>
						<img src="<%= BASEURL %>/images/menu-images/menu_link_default.gif" border="0">  
					  <%}else {//carpeta%>
					  	<img src="<%= BASEURL %>/images/menu-images/menu_folder_open.gif" border="0">  
					  <%}%>
					  </td>
                      <td width="74%"><font size="-2"><%=mnom%></font></td>
					  <td width="16%"><font size="-2"><%=menu.getOrden()%></font></td>
                    </tr>
                    <tr align="center"> 
                      <td>&nbsp;</td>
                      <td colspan="2"> 
					    <table width="100%" border="0" cellpadding="0" cellspacing="2">
						<%Vector vhijos = model.menuService.getVhijos();
             			  for (int j = 0; j < vhijos.size(); j++){
                				Menu mhijos = (Menu) vhijos.elementAt(j);
								String hnom = mhijos.getNombre();
								int hidop = mhijos.getIdopcion();
								int hsubm = mhijos.getSubmenu();%>
						  <tr class="letraTitulo" onMouseOver=" bgColor='#99cc99'" onMouseOut= " bgColor='#EAFFEA'" 
								onClick="window.location='<%=CONTROLLER%>?estado=Menu&accion=CargaxPerfil&direccion=/jsp/trafico/menu/ModificarOpcionMenu2.jsp&idop=<%=hidop%>&cmd=modop&msg='">  
                            <td width="5%">
								<%if (hsubm == 1){//carpeta
			  						out.println("<img src=" + BASEURL + "/images/menu-images/menu_folder_closed.gif style=cursor:hand>");
									out.println("</td><td width='79%' style=cursor:hand>"); 
								}
								else{
									out.println("<img src=" + BASEURL + "/images/menu-images/menu_link_default.gif  style=cursor:hand>");
									out.println("</td><td width='79%'  style=cursor:hand>"); 
			  					}%>
								<font size="-2"><%out.println(hnom);%></font>
							</td>
							<td width="16%"><font size="-2"><%=mhijos.getOrden()%></font></td>
                          </tr>
                          <%}//for hijos%>
                        </table>
					  </td>
                    </tr>
                  </table>
				</td>
              </tr>
              <%}//for padres%>
            </table>
          </td>
        </tr>
      </table>
	  <input type="hidden" name="sub" value="<%=subm%>">
	  <input name="nivel" type="hidden" value="<%=niv%>">
	</td>
  </tr>
</table></td>
  </tr>
</table>
<% if (!nota.equals("")){%>
<br>
	<table width="40%"  align="center">
	  	<tr><td>
			<FIELDSET>
			<legend><span class="letraresaltada">IMPORTANTE</span></legend>
			<table width="100%"  border="0" cellpadding="0" cellspacing="0" class="informacion">
  				<tr>
    			  <td nowrap align="center">&nbsp;<%= nota %> </td>
    			</tr>
			</table>
  		</FIELDSET>
		</td></tr>
  	</table>
 <%}%>
  
<br>
<%String msg = request.getParameter("msg");
if (!msg.equals("")){%>
<table border="2" align="center">
   		<tr>
        	<td>
            	<table width="100%"  border="1" align="center"  bordercolor="#F7F5F4" bgcolor="#FFFFFF">
                	<tr>
                    	<td width="229" align="center" class="mensajes"><%=msg%></td>
                        <td width="29" background="<%=BASEURL%>/images/cuadronaranja.JPG">&nbsp;</td>
                        <td width="58">&nbsp;</td>
                    </tr>
                </table>
            </td>
		</tr>
	</table>
<%}%>
</form>
</div>
 <%=datos[1]%>
</body>
</html>