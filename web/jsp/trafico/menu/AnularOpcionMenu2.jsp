<!--
- Autor : Ing. Sandra M Escalante G
- Date  : 25 Abril 2005
- Copyrigth Notice : Fintravalores S.A. S.A
-->
<%--
-@(#)
--Descripcion : Pagina JSP, que se encarga de gestionar 
				el formulario para anular opciones del menu
--%>

<%-- Declaracion de librerias--%>
<%@page contentType="text/html"%>
<%@ page session="true"%>
<%@ page errorPage="../error/ErrorPage.jsp"%>
<%@page import="com.tsp.operation.model.beans.*, java.util.*"%>
<%@page import="com.tsp.operation.model.*"%>
<%@include file="/WEB-INF/InitModel.jsp"%>
<%@ taglib uri="http://jakarta.apache.org/taglibs/input-0.90" prefix="input" %>
<%
    String path = application.getRealPath(model.menuService.getRealUrl(request.getRequestURI()));
    String datos[] = model.menuService.getContenidoMenu(BASEURL,request.getRequestURI(),path);
%>
<html>
<head>
<title>.: Anular Opcion</title>
<link href="<%= BASEURL %>/css/estilostsp.css" rel='stylesheet'>
<link href="../../../css/estilostsp.css" rel="stylesheet" type="text/css">
<script type='text/javascript' src="<%= BASEURL %>/js/validar.js"></script>
<script type='text/javascript' src="<%= BASEURL %>/js/boton.js"></script>
</head>
<body onresize="redimensionar()" onload = 'redimensionar()'>
<div id="capaSuperior" style="position:absolute; width:100%; height:100px; z-index:0; left: 0px; top: 0px;">
 <jsp:include page="/toptsp.jsp?encabezado=Menu - Anular Opcion"/>
</div>
<div id="capaCentral" style="position:absolute; width:100%; height:87%; z-index:0; left: 0px; top: 100px; overflow: scroll;">

<body>
<%String nom="", desc="", url="";
int subm = 0, padre = 0, niv = 0, opcion = 0, orden = 0;%>
<FORM name='forma' id='forma' method='POST' action="<%=CONTROLLER%>?estado=Menu&accion=AnularOpcion&carpeta=/jsp/trafico/menu&pagina=AnularOpcionMenu2.jsp">
<table width="446" border="2" align="center">
  <tr>
    <td><table width="100%" align="center" class="tablaInferior">
  <tr>
    <td colspan="3"><table width="100%"  border="0" class="barratitulo">
  <tr>
    <td width="39%" class="subtitulo1">Anular Menu\Opcion</td>
    <td width="61%"><img src="<%=BASEURL%>/images/titulo.gif" width="32" height="20" align="left"><%=datos[0]%></td>
  </tr>
</table>
</td>
  </tr>
  <%Menu m = model.menuService.getMenu();
	opcion = m.getIdopcion();
	nom = m.getNombre();
	desc = m.getDescripcion();
	url = m.getUrl();
	padre = m.getIdpadre();
	subm = m.getSubmenu();
	niv = m.getNivel();%>
  <tr class="fila">
    <td>Nombre:</td>
    <td colspan="2"><input name="nom" type="text" class="textbox" id="nom" value="<%=nom%>" size="30" maxlength="60" readonly="true"></td>
  </tr>
  <tr class="fila">
    <td>Descripcion:</td>
    <td  colspan="2"><input name="desc" type="text" id="desc" value="<%=desc%>" size="30" maxlength="30" readonly="true"></td>
  </tr>
	<%if ( (m.getSubmenu() == 2)){//hoja%>
  <tr class="fila">
    <td>Ruta:</td>
    <td colspan="2" align="center">
	  <textarea name="url" cols="65" rows="3" readonly="true" class="textbox" id="textarea"><%=url%></textarea>  	
	</td>
  </tr>
	<%}//fin if submenu%>
	<%if ( (subm != 1) || ( model.menuService.getVhijos().size() == 0 ) ){//hoja%>
  <tr class="fila" align="center">
    <td colspan="3">
	<input type="image" name="Submit" value="Ingresar" src="<%= BASEURL %>/images/botones/anular.gif" align="absmiddle" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);">
            <img src="<%=BASEURL%>/images/botones/salir.gif" name="c_salir" onClick="window.close();" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" align="absmiddle">
	</td>
  </tr>
  <%}%>
  <tr class="fila">
    <td colspan="3">
      <table width="100%" border="0" cellpadding="0" cellspacing="0">
        <tr class="subtitulo1" onMouseOver="bgColor= '#0092A6'" onMouseOut="bgColor='#4D71B0'" style="cursor:hand" 
		onClick="window.location='<%=CONTROLLER%>?estado=Menu&accion=Cargar&carpeta=/jsp/trafico/menu&pagina=AnularOpcionMenu.jsp?msg=&marco=no&opcion=25'">			
          <td width="8%"><img src="<%= BASEURL %>/images/menu-images/home.gif" border="0"></td>
          <td width="92%">[ FINTRAVALORES  S. A. ]</td>
        </tr>
        <tr>
          <td>&nbsp;</td>
          <td>
			<table width="100%" border="0" cellspacing="0" cellpadding="0">
			  <%//model.menuService.obtenerPadreMenuxIDopcion(Integer.parseInt(request.getParameter("idop")));
			  Vector vmenu = model.menuService.getVMenu();
		      	for (int i = 0; i < vmenu.size(); i++){
                	Menu menu = (Menu) vmenu.elementAt(i);
		            int midop = menu.getIdopcion();
    		        String mnom = menu.getNombre();
        		    int midpa = menu.getIdpadre();
            		//Menu pmenu = model.menuService.buscarOpcionMenuxIdopcion(menu.getIdpadre());
					String mnompa = menu.getPnombre();
					if ( midpa != 0 ){%>
               <tr class="letraresaltada" onMouseOver="bgColor= '#0092A6'" onMouseOut="bgColor='#D1DCEB'" 
			   		onClick="window.location='<%=CONTROLLER%>?estado=Menu&accion=CargaxPerfil&direccion=/jsp/trafico/menu/AnularOpcionMenu2.jsp&idop=<%=midpa%>&msg=&cmd=modop'" style="cursor:hand"> 
                 <td width="5%"><img src="<%= BASEURL %>/images/menu-images/menu_folder_open.gif" border="0"></td>
                 <td width="79%"><font size="-2"><%=mnompa%></font></td>
				 <td width="15%"><font size="-2"><%=menu.getPorden()%></font></td>
               </tr>
                    <%}//fin if%>
               <tr align="right"> 
                 <td colspan="3">
				   <table width="98%" border="0" cellpadding="0" cellspacing="1">
                     <tr class="fila"> 
                       <td width="5%">
						 <%if(subm == 1){//carpeta%>
						   <img src="<%= BASEURL %>/images/menu-images/menu_folder_open.gif" border="0">
						 <%}else if(subm == 2){//hoja%>
						   <img src="<%= BASEURL %>/images/menu-images/menu_link_default.gif" border="0">
						  <%}%> 
					   </td>
                       <td width="77%"><font size="-2"><%=mnom%></font></td>
					   <td width="16%"><font size="-2"><%=menu.getOrden()%></font></td>
                     </tr>
                     <tr align="right"> 
                       <td colspan="3"> 
						 <table width="98%" border="0" cellpadding="0" cellspacing="2">
						   <%Vector vhijos = model.menuService.getVhijos();
             				for (int j = 0; j < vhijos.size(); j++){
                				Menu mhijos = (Menu) vhijos.elementAt(j);
								String hnom = mhijos.getNombre();
								int hidop = mhijos.getIdopcion();
								int hsubm = mhijos.getSubmenu();%>
						   <tr class="letraTitulo" onMouseOver=" bgColor='#99cc99'" onMouseOut= " bgColor='#D1DCEB'" 
								onClick="window.location='<%=CONTROLLER%>?estado=Menu&accion=CargaxPerfil&direccion=/jsp/trafico/menu/AnularOpcionMenu2.jsp&idop=<%=hidop%>&cmd=modop&msg='">  
                             <td width="5%">
								<%if (hsubm == 1){//carpeta
			  						out.println("<img src=" + BASEURL + "/images/menu-images/menu_folder_closed.gif style=cursor:hand>");
									out.println("</td><td width='77%' style=cursor:hand>"); 
			  					}
								else if (hsubm == 2){//hoja
									out.println("<img src=" + BASEURL + "/images/menu-images/menu_link_default.gif style=cursor:hand>");
									out.println("</td><td width='77%' style=cursor:hand>"); 
			  					}%>
								<font size="-2"><%out.println(hnom);%></font>
							 </td>
							 <td width="16%"><font size="-2"><%=mhijos.getOrden()%></font></td>
                           </tr>
                            <%}//for hijos%>
                         </table>
					   </td>
                     </tr>
                   </table>
			     </td>
               </tr>
               <%}//for padres%>
            </table>
          </td>
        </tr>
      </table>
		<input type="hidden" name="id_padre" id="id_padre" value="<%=padre%>">
		<input type="hidden" name="opcion" value="<%=opcion%>">
	</td>
  </tr>
</table></td>
  </tr>
</table>


<br>
<%String msg = request.getParameter("msg");
if (!msg.equals("")){%>
<table border="2" align="center">
   		<tr>
        	<td>
            	<table width="100%"  border="1" align="center"  bordercolor="#F7F5F4" bgcolor="#FFFFFF">
                	<tr>
                    	<td width="229" align="center" class="mensajes"><%=msg%></td>
                        <td width="29" background="<%=BASEURL%>/images/cuadronaranja.JPG">&nbsp;</td>
                        <td width="58">&nbsp;</td>
                    </tr>
                </table>
            </td>
		</tr>
	</table>
<%}%>
</form>
</div>
<%=datos[1]%>
</body>
</html>