<!--
- Autor : Ing. Sandra M Escalante G
- Date  : 25 Abril 2005
- Copyrigth Notice : Fintravalores S.A. S.A
-->
<%--
-@(#)
--Descripcion : Pagina JSP, que se encarga de gestionar 
				el formulario para registrar una opcion en el menu
--%>

<%-- Declaracion de librerias--%>
<%@page contentType="text/html"%>
<%@ page session="true"%>
<%@ page errorPage="../error/ErrorPage.jsp"%>
<%@page import="com.tsp.operation.model.beans.*, java.util.*"%>
<%@page import="com.tsp.operation.model.*"%>
<%@include file="/WEB-INF/InitModel.jsp"%>
<%@ taglib uri="http://jakarta.apache.org/taglibs/input-0.90" prefix="input" %>
<%
    String path = application.getRealPath(model.menuService.getRealUrl(request.getRequestURI()));
    String datos[] = model.menuService.getContenidoMenu(BASEURL,request.getRequestURI(),path);
%>
<html>
<head>
<title>.: Ingresar Opcion</title>
<link href="<%= BASEURL %>/css/estilostsp.css" rel='stylesheet'>
<link href="../css/estilostsp.css" rel="stylesheet" type="text/css">
<link href="../../../css/estilostsp.css" rel="stylesheet" type="text/css">
<script type='text/javascript' src="<%=BASEURL%>/js/validar.js"></script>
<script type="text/javascript" src="../js/validar.js"></script>
<script type='text/javascript' src="<%= BASEURL %>/js/boton.js"></script>
</head>
<body onresize="redimensionar()" onload = 'redimensionar()'>
<div id="capaSuperior" style="position:absolute; width:100%; height:100px; z-index:0; left: 0px; top: 0px;">
 <jsp:include page="/toptsp.jsp?encabezado=Menu - Nueva Opcion"/>
</div>
<div id="capaCentral" style="position:absolute; width:100%; height:87%; z-index:0; left: 0px; top: 100px; overflow: scroll;">
<FORM name="forma" id="forma" method='POST' action="<%=CONTROLLER%>?estado=Menu&accion=CrearOpcion&carpeta=/jsp/trafico/menu&pagina=InsertarOpcionMenu2.jsp&tipo=opcion" onSubmit="return validarCamposMenu('I');">
<table width="446" border="2" align="center">
  <tr>
    <td><table width="100%" align="center" class="tablaInferior">
    <tr class="barratitulo" align="center">
      <td colspan="3"><table width="446" border="0" cellpadding="0" cellspacing="0" class="tablaInferior">
  <tr>
    <td width="172" class="subtitulo1">Ingresar Menu\Opcion</td>
    <td width="274" nowrap><img src="<%=BASEURL%>/images/titulo.gif" width="32" height="20" align="left"><%=datos[0]%></td>
  </tr>
</table>
</td>
    </tr>
    <tr class="fila">
      <td>Nombre</td>
      <td><input name="nom" type="text" class="textbox" id="nom" size="45" maxlength="60" onKeyPress="soloAlfa(event);"></td>
      <td width="109" rowspan="2">
        <table width="100%" align="center" border="1" cellpadding="1" cellspacing="1">
          <tr class="fila">
            <td width="48%">Menu</td>
            <td width="52%" align="center"><input type="radio" name="op" value="1" onClick="ocultarURL();"></td>
          </tr>
          <tr class="fila">
            <td>Opcion</td>
            <td align="center"><input type="radio" name="op" id="subn" value="2" onClick="mostrarURL();" checked></td>
          </tr>
        </table>
        </td>
    </tr>
    <tr class="fila">
      <td>Descripcion</td>
      <td><input name="desc" type="text" id="desc" class="textbox" size="45" maxlength="60" onKeyPress="soloAlfa(event);"></td>
    </tr>
	<tr class="fila">
      <td>Orden</td>
      <td colspan="2">
	    <input name="orden" type="text" maxlength="4" class="textbox" onKeyPress="soloDigitos(event,'decNO');">
	  </td>
    </tr>
    <tr id="urlpanel" class="fila" align="center">
      <td rowspan="2">Ruta</td>
      <td colspan="2">
	    <table width="100%"  border="1" cellpadding="1" cellspacing="1">
          <tr class="fila">
            <td nowrap width="28%">Archivo Enrutador</td>
            <td width="72%"><input name="textfield" type="text" class="textbox" value="MenuCargarAction.java" size="43" readonly></td>
          </tr>
          <tr class="fila">
            <td nowrap>Cabecera de Ruta</td>
            <td><input name="textfield2" type="text" class="textbox" value="../controller?estado=Menu&amp;accion=Cargar" size="43" readonly></td>
          </tr>
        </table>
	  </td>
    </tr>
    <tr id="urlpanel2" class="fila">
      <td colspan="2">
	  <table width="100%" class="fila" border="1" align="center" cellpadding="1" cellspacing="1">
        <tr>
          <td width="28%">Carpeta</td>
          <td width="72%" align="center"> 
            <input name="carp" type="text" class="textbox" size="48">
          </td>
        </tr>
        <tr>
          <td>Pagina</td>
          <td align="center">
            <input name="pag" type="text" class="textbox" size="48">
          </td>
        </tr>
        <tr>
          <td>Otros</td>
          <td align="center">
            <input name="otro" type="text" class="textbox" size="48">
          </td>
        </tr>
      </table>
	  </td>
    </tr>
    <tr class="fila" align="center">
      <td colspan="3"><input type="image" name="Submit" value="Ingresar" src="<%= BASEURL %>/images/botones/aceptar.gif" align="absmiddle" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);">
            <img src="<%=BASEURL%>/images/botones/salir.gif" name="c_salir" onClick="window.close();" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" align="absmiddle">
	  </td>
    </tr>
    <tr class="fila">
      <td colspan="3">
        <table width="100%" border="0" cellpadding="0" cellspacing="0">
          <tr onMouseOver="bgColor= '#003399'" onMouseOut="bgColor='#4D71B0'" style="cursor:hand" 
		  	onClick="window.location='<%=CONTROLLER%>?estado=Menu&accion=Cargar&carpeta=/jsp/trafico/menu&pagina=InsertarOpcionMenu.jsp?msg=&marco=no&opcion=25'">
            <td width="8%" class="subtitulo1"><img src="<%= BASEURL %>/images/menu-images/home.gif" border="0"></td>
            <td width="92%" class="subtitulo1">[ FINTRAVALORES  S. A. ]</td>
          </tr>
          <tr>
            <td>&nbsp;</td>
            <td>
			  <table width="100%" border="0" cellspacing="0" cellpadding="0">
			  <%int niv = 0;
				Vector vmenu = model.menuService.getVMenu();
		      	for (int i = 0; i < vmenu.size(); i++){
                	Menu menu = (Menu) vmenu.elementAt(i);
		           	int midop = menu.getIdopcion();
    		        String mnom = menu.getNombre();
					int mor = menu.getOrden();
        		    int midpa = menu.getIdpadre();
					String mnompa = menu.getPnombre();
					int morpa = Integer.parseInt(menu.getPorden());
	               	niv = menu.getNivel() + 1;
					if ( midpa != 0 ){//padre no esta en la raiz%>
		                <tr class="letraresaltada" bgcolor="#D1DCEB" onMouseOver="bgColor= '#99cc99'" onMouseOut="bgColor='#EAFFEA'" 
							onClick="window.location='<%=CONTROLLER%>?estado=Menu&accion=CargaxPerfil&direccion=/jsp/trafico/menu/InsertarOpcionMenu2.jsp&idop=<%=midpa%>&msg=&cmd=cgaop'" style="cursor:hand"> 
        		           <td width="5%"><img src="<%= BASEURL %>/images/menu-images/menu_folder_open.gif" border="0"></td>
                		   <td width="79%"><font size="-2"><%=mnompa%> </font></td>
		                   <td width="16%"><font size="-2"> <%=morpa%></font></td>
           		        </tr>
        	       <%}//fin if midpa%>
                 		<tr> 
                   		  <td colspan="3" align="right">
						  
				     		<table width="95%" border="0" align="center" cellpadding="0" cellspacing="1">
                       		  <tr class="letraresaltada"> 
                         		<td width="10%">&nbsp;&nbsp;&nbsp;&nbsp;<img src="<%= BASEURL %>/images/menu-images/menu_folder_open.gif" border="0"></td>
                         		<td width="76%"><font size="-2"><%=mnom%></font></td>
                         		<td width="14%"><font size="-2"> <%=mor%></font></td>
                       		  </tr>
                       		  <tr class="fila"> 
                         		<td>&nbsp;</td>
                         		<td colspan="2"> 
						 
						   		  	<table width="100%" border="0" cellpadding="0" cellspacing="2">
						   			<%Vector vhijos = model.menuService.getVhijos();
    	         					for (int j = 0; j < vhijos.size(); j++){
        	        					Menu mhijos = (Menu) vhijos.elementAt(j);
										String hnom = mhijos.getNombre();
										int hidop = mhijos.getIdopcion();
										int hsubm = mhijos.getSubmenu();
										int hor = mhijos.getOrden();%>
							     		<tr align="left" class="letraTitulo" onMouseOver=" bgColor='#99cc99'" onMouseOut= " bgColor='#EAFFEA'"
											<%if (hsubm == 1){ %>onClick="window.location='<%=CONTROLLER%>?estado=Menu&accion=CargaxPerfil&direccion=/jsp/trafico/menu/InsertarOpcionMenu2.jsp&idop=<%=hidop%>&cmd=cgaop&msg='"<%}%>>  
										
   			                              <td width="5%">
											<%if (hsubm == 1){ //carpeta
				  								out.println("<img src=" + BASEURL + "/images/menu-images/menu_folder_closed.gif  style=cursor:hand>");
												out.println("</td><td width='79%' style=cursor:hand>"); 
						  					}
											else{//opcion
												out.println("<img src=" + BASEURL + "/images/menu-images/menu_link_default.gif>");
						  						out.println("</td><td width='79%'>"); 
				  							}%>
			  									<font size="-2"><%out.println(hnom);%></font>
											</td>
                                			<td width="16%"><font size="-2"><%=hor%></font></td>
						     			</tr>
                            		<%}//for hijos%>
                           			</table>
						  
					     			</td>
                       			</tr>
                     		</table>
						
					</td>
                  </tr>
                <%}//for padres%>
                </table>
            </td>
            </tr>
      	  </table>
	    </td>
      </tr>
  </table>
<input name="id_padre" type="hidden" id="id_padre" value="<%=request.getParameter("idop")%>">
<input name="nivel" type="hidden" id="nivel" value="<%=niv%>">  </td>
  </tr>
</table>

  
<br>
<%String msg = request.getParameter("msg");
if (!msg.equals("")){%>
<table border="2" align="center">
   		<tr>
        	<td>
            	<table width="100%"  border="1" align="center"  bordercolor="#F7F5F4" bgcolor="#FFFFFF">
                	<tr>
                    	<td width="229" align="center" class="mensajes"><%=msg%></td>
                        <td width="29" background="<%=BASEURL%>/images/cuadronaranja.JPG">&nbsp;</td>
                        <td width="58">&nbsp;</td>
                    </tr>
                </table>
            </td>
		</tr>
	</table>
<%}%>
</form>
</div>
<%=datos[1]%>
</body>
</html>