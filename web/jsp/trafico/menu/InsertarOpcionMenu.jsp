<!--
- Autor : Ing. Sandra M Escalante G
- Date  : 25 Abril 2005
- Copyrigth Notice : Fintravalores S.A. S.A
-->
<%--
-@(#)
--Descripcion : Pagina JSP, que se encarga de gestionar 
				el formulario para registrar una opcion en el menu
--%>

<%-- Declaracion de librerias--%>
<%@page contentType="text/html"%>
<%@ page session="true"%>
<%@ page errorPage="../error/ErrorPage.jsp"%>
<%@page import="com.tsp.operation.model.beans.*, java.util.*"%>
<%@page import="com.tsp.operation.model.*"%>
<%@include file="/WEB-INF/InitModel.jsp"%>
<%@ taglib uri="http://jakarta.apache.org/taglibs/input-0.90" prefix="input" %>
<%
    String path = application.getRealPath(model.menuService.getRealUrl(request.getRequestURI()));
    String datos[] = model.menuService.getContenidoMenu(BASEURL,request.getRequestURI(),path);
%>
<html>
<head>
<title>.: Ingresar Opcion</title>
<link href="<%= BASEURL %>/css/estilostsp.css" rel='stylesheet'>
<link href="../css/estilostsp.css" rel="stylesheet" type="text/css">
<link href="/css/estilostsp.css" rel="stylesheet" type="text/css">
<script type='text/javascript' src="<%= BASEURL %>/js/validar.js"></script>
<script type='text/javascript' src="<%= BASEURL %>/js/boton.js"></script>
</head>
<body onresize="redimensionar()" onload = 'redimensionar()'>
<div id="capaSuperior" style="position:absolute; width:100%; height:100px; z-index:0; left: 0px; top: 0px;">
 <jsp:include page="/toptsp.jsp?encabezado=Menu - Nueva Opcion"/>
</div>
<div id="capaCentral" style="position:absolute; width:100%; height:87%; z-index:0; left: 0px; top: 100px; overflow: scroll;">
<FORM name='forma' id='forma' method='POST' action="<%=CONTROLLER%>?estado=Menu&accion=CrearOpcion&carpeta=/jsp/trafico/menu&pagina=InsertarOpcionMenu.jsp&tipo=raiz" onSubmit="return validarCamposMenu();">
<table width="500" border="2" align="center">
  <tr>
    <td><table width="100%" align="center" class="tablaInferior">
      <tr class="titulo" align="center">
        <td colspan="3">
			<table width="100%"  border="0" cellpadding="0" cellspacing="0" class="barratitulo">
			  <tr><td width="39%" class="subtitulo1">Ingresar Menu\Opcion</td>
    			  <td width="61%" nowrap><img src="<%=BASEURL%>/images/titulo.gif" width="32" height="20" align="left"><%=datos[0]%></td>
  			  </tr>
		    </table>
		</td>
      </tr>
      <tr class="fila">
        <td>Nombre</td>
        <td><input name="nom" type="text" class="textbox" id="nom" size="48" maxlength="60" onKeyPress="soloAlfa(event);"></td>
        <td width="109" rowspan="2">
          <table width="95%"  border="1" cellpadding="1" cellspacing="1">
            <tr class="fila">
              <td width="48%">Menu</td>
              <td width="52%" align="center"><input type="radio" name="op" value="1" checked></td>
            </tr>
            <tr class="fila">
              <td>Opcion</td>
              <td align="center"><input type="radio" name="op" value="2" disabled ></td>
            </tr>
        </table></td>
      </tr>
      <tr class="fila">
        <td>Descripcion</td>
        <td><input name="desc" type="text" id="desc" class="textbox" size="48" maxlength="60" onKeyPress="soloAlfa(event);"></td>
      </tr>
      <tr class="fila">
        <td>Orden</td>
        <td colspan="2">
          <input name="orden" type="text" class="textbox" id="orden" maxlength="4" onKeyPress="soloDigitos(event,'decNO');"></td>
      </tr>
      <tr class="fila" align="center">	  	  
        <td colspan="3"><input type="image" name="Submit" value="Ingresar" src="<%= BASEURL %>/images/botones/aceptar.gif" align="absmiddle" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);">		
            <img src="<%=BASEURL%>/images/botones/salir.gif" name="c_salir" onClick="window.close();" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" align="absmiddle"></td>
      </tr>
      <tr class="fila">
        <td colspan="3">
          <table width="100%" border="0" cellpadding="0" cellspacing="0">
            <tr class="subtitulo1">
              <td width="8%"><img src="<%= BASEURL %>/images/menu-images/home.gif" border="0"></td>
              <td width="92%">[ SISTEMA LOGOSTICO DE TRANSPORTE ]</td>
            </tr>
            <tr>
              <td>&nbsp;</td>
              <td><%Vector vmenu = model.menuService.getVMenu();
            	for (int i = 0; i < vmenu.size(); i++){
                	Menu m = (Menu) vmenu.elementAt(i);
                    int idop = m.getIdopcion();
                	String mnom = m.getNombre();
					if (m.getNivel() == 1){%>
                  <table width="99%" border="0" align="center" cellpadding="0" cellspacing="0">
                    <tr class="letraresaltada" <%if ( m.getSubmenu() == 1 ){%>onMouseOver="bgColor= '#99cc99'" onMouseOut="bgColor='#EAFFEA'" onClick="window.location='<%=CONTROLLER%>?estado=Menu&accion=CargaxPerfil&direccion=/jsp/trafico/menu/InsertarOpcionMenu2.jsp&idop=<%=idop%>&nop=<%=mnom%>&cmd=cgaop&msg='" style="cursor:hand"<%}%>>
                      <td width="5%">
                        <%if ( m.getSubmenu() == 1 ){
				  	out.println("<img src=" + BASEURL + "/images/menu-images/menu_folder_closed.gif>");
				  } else {
					out.println("<img src=" + BASEURL + "/images/menu-images/menu_link_default.gif>");
				  }%></td>
                      <td width="80%">&nbsp;&nbsp;<font size="-2">
                        <% out.println(mnom);%>
                      </font></td>
                      <td><font size="-2">
                        <% out.println(m.getOrden());%>
                      </font></td>
                    </tr>
                  </table>
                  <%}//if nivel
				}//for%>
                  <input name="id_padre" type="hidden" id="id_padre" value="0">
                  <input name="nivel" type="hidden" id="nivel" value="1">
              </td>
            </tr>
        </table></td>
      </tr>
    </table></td>
  </tr>
</table>

  <br>
<%String msg = request.getParameter("msg");
if (!msg.equals("")){%>
	<table border="2" align="center">
   		<tr>
        	<td>
            	<table width="100%"  border="1" align="center"  bordercolor="#F7F5F4" bgcolor="#FFFFFF">
                	<tr>
                    	<td width="229" align="center" class="mensajes"><%=msg%></td>
                        <td width="29" background="<%=BASEURL%>/images/cuadronaranja.JPG">&nbsp;</td>
                        <td width="58">&nbsp;</td>
                    </tr>
                </table>
            </td>
		</tr>
	</table>	
<%}%>
</form>
</div>
 <%=datos[1]%>
</body>
</html>
