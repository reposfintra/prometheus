<!--
- Autor : Ing. Sandra M Escalante G
- Date  : 18 de Mayo
- Copyrigth Notice : Fintravalores S.A. S.A
-->
<%--
-@(#)
--Descripcion : Ayuda
--%>
<%@include file="/WEB-INF/InitModel.jsp"%>
<html>
    <head>
        <title>.: Ayuda - Funcionalidad - Anular Opcion</title>
        <link href="../../../css/estilostsp.css" rel="stylesheet" type="text/css">
        <link href="<%= BASEURL %>/css/estilostsp.css" rel="stylesheet" type="text/css">
		<script type='text/javascript' src="<%= BASEURL %>/js/boton.js"></script>
    </head>
    <body>
        <BR>
        <table width="550"  border="2" align="center">
            <tr>
            <td width="550" >
                <table width="100%" border="0" align="center">
                    <tr  class="subtitulo">
                        <td height="24" align="center">ANULAR OPCION </td>
                    </tr>
                    <tr class="subtitulo1">
                        <td> FORMULARIO PARA ANULAR UNA OPCION </td>
                    </tr>
                    <tr class="ayudaHtmlTexto">
                        <td align="center">
                            <br>
                            <img src="<%= BASEURL %>/images/ayuda/trafico/menu/AnularOpcion1.JPG" align="absmiddle">	
                            <br>							
						</td>
					</tr>	
					<tr class="subtitulo1">
                        <td> FORMULARIO PARA ANULAR UNA CARPETA </td>
                    </tr>
					<tr class="ayudaHtmlTexto">
                        <td>                           
							<br>
							<center>
                        		<img src="<%= BASEURL %>/images/ayuda/trafico/menu/AnularOpcion2.JPG" align="absmiddle" >
							</center>														
							<br>
						</td>
                    </tr>
					<tr class="subtitulo1">
                        <td> FORMULARIO PARA ANULAR UNA OPCION </td>
                    </tr>
					<tr class="ayudaHtmlTexto">
                        <td>                           
							<br>
							<center>
                        		<img src="<%= BASEURL %>/images/ayuda/trafico/menu/AnularOpcion3.JPG" align="absmiddle" >
							</center>														
							<br>
						</td>
                    </tr>
					<tr class="subtitulo1">
                        <td> MENSAJE DE CONFIRMACION </td>
                    </tr>
					<tr class="ayudaHtmlTexto">
                        <td>                           							
							<br>
							Luego de Anular la opcion en el sistema, le debe salir el siguiente mensaje confirmando la accion.
							<br>
							<br>
							<center>
								<img src="<%= BASEURL %>/images/ayuda/trafico/menu/MsgAnularOpcion.JPG" align="absmiddle" >
							</center>
                            <br>												
                        </td>			
					</tr>			      					
                </table>
            </td>
            </tr>
        </table>
		<br>
		<center>
			<img src="<%=BASEURL%>/images/botones/salir.gif" name="c_salir" onClick="window.close();" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" align="absmiddle">
		</center>
		<br>
        <p align="center" class="fuenteAyuda"> Fintravalores S. A. </p>
		<br>
    </body>
</html>