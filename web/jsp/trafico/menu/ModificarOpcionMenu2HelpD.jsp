<!--
- Autor : Ing. Sandra M Escalante G
- Date  : 18 de Mayo
- Copyrigth Notice : Fintravalores S.A. S.A
-->
<%--
-@(#)
--Descripcion : Ayuda
--%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
"http://www.w3.org/TR/html4/loose.dtd">
<%@include file="/WEB-INF/InitModel.jsp"%>
<html>
<head>
<title>.: Ayuda - Descripcion Campos - Modificar Opcion</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link href="../../../css/estilostsp.css" rel="stylesheet" type="text/css">
<link href="<%= BASEURL %>/css/estilostsp.css" rel="stylesheet" type="text/css">
<script type='text/javascript' src="<%= BASEURL %>/js/boton.js"></script>
</head>

<body>
<br>
<table width="696"  border="2" align="center">
  <tr>
    <td width="811" height="" >
      <table width="100%" border="0" align="center">
        <tr  class="subtitulo">
          <td height="24" colspan="2" align="center">MODIFICAR OPCION</td>
        </tr>
		<tr class="subtitulo1">
          <td colspan="2"> DATOS DE LA OPCION</td>
        </tr>
        <tr>
          <td width="149" class="fila"> Nombre </td>
          <td width="525"  class="ayudaHtmlTexto">Nombre de la opci�n. Esta informaci�n es la que se va a ver en el Menu.</td>
        </tr>
		<tr>
          <td width="149" class="fila"> ID </td>
          <td width="525"  class="ayudaHtmlTexto">N�mero �nico que identifica a la opci�n en el Menu. SOLO LECTURA. </td>
        </tr>
		<tr>
          <td width="149" class="fila"> ID Padre </td>
          <td width="525"  class="ayudaHtmlTexto">N�mero �nico identificador de la carpeta contenedora de la opci�n.</td>
        </tr>
        <tr>
          <td width="149" class="fila"> Descripci�n </td>
          <td width="525"  class="ayudaHtmlTexto">Informaci�n adicional al nombre de la opci�n. </td>
        </tr>
        <tr>
          <td class="fila">Orden</td>
          <td  class="ayudaHtmlTexto">Lugar donde va a salir la opci�n cuando se despliegue en el menu. Este valor es num�rico y en el menu las opciones se organiza de menor a mayor orden.</td>
        </tr>       
		<tr class="subtitulo1">
          <td colspan="2">DATOS DEL ARCHIVO FUENTE</td>
        </tr>
		 <tr>
          <td class="fila">Archivo Enrutador y Cabecera de Ruta</td>
          <td  class="ayudaHtmlTexto">Informaci�n complemetaria. SOLO LECTURA.</td>
        </tr>
		<tr>
          <td class="fila">Carpeta</td>
          <td  class="ayudaHtmlTexto">Direccion/Ruta donde se encuentra el archivo fuente (*.jsp) que se va a mostrar cuando se seleccione la opci�n en el menu.</td>
        </tr>
		<tr>
		  <td class="fila">Pagina</td>
		  <td  class="ayudaHtmlTexto">Nombre del archivo. Este es un archivo *.jsp.</td>
	    </tr>
		<tr>
		  <td class="fila">Otros</td>
		  <td  class="ayudaHtmlTexto">Parametros adicionales que el usuario necesite enviar en el URL para la ejecuci�n del programa. Ejemplo: &amp;marco=no. Si el usuario desea enviar mas de un par�metro estos se separan por &amp;. </td>
	    </tr>
    </table>    </td>
  </tr>
</table>
<br>
<center>
	<img src="<%=BASEURL%>/images/botones/salir.gif" name="c_salir" onClick="window.close();" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" align="absmiddle">
</center>
<br>
<p align="center" class="fuenteAyuda"> Fintravalores S. A. </p>
<br>
</body>
</html>