<!--
- Autor : Ing. Sandra M Escalante G
- Date  : 25 Abril 2005
- Copyrigth Notice : Fintravalores S.A. S.A
-->
<%--
-@(#)
--Descripcion : Pagina JSP, que se encarga de gestionar 
				el formulario para modificar una opcion del menu
--%>

<%-- Declaracion de librerias--%>
<%@page contentType="text/html"%>
<%@ page session="true"%>
<%@ page errorPage="../error/ErrorPage.jsp"%>
<%@page import="com.tsp.operation.model.beans.*, java.util.*"%>
<%@page import="com.tsp.operation.model.*"%>
<%@include file="/WEB-INF/InitModel.jsp"%>
<%@ taglib uri="http://jakarta.apache.org/taglibs/input-0.90" prefix="input" %>

<%
    String path = application.getRealPath(model.menuService.getRealUrl(request.getRequestURI()));
    String datos[] = model.menuService.getContenidoMenu(BASEURL,request.getRequestURI(),path);
%>
<html>
<head>
<title>.: Modificar Opcion</title>
<link href="<%= BASEURL %>/css/estilostsp.css" rel='stylesheet'>
<link href="../css/estilostsp.css" rel="stylesheet" type="text/css">
<link href="../../../css/estilostsp.css" rel="stylesheet" type="text/css">
<script type='text/javascript' src="<%= BASEURL %>/js/validar.js"></script>
<script type='text/javascript' src="<%= BASEURL %>/js/boton.js"></script>
</head>
<body onresize="redimensionar()" onload = 'redimensionar()'>
<div id="capaSuperior" style="position:absolute; width:100%; height:100px; z-index:0; left: 0px; top: 0px;">
 <jsp:include page="/toptsp.jsp?encabezado=Menu - Modificar Opcion"/>
</div>
<div id="capaCentral" style="position:absolute; width:100%; height:87%; z-index:0; left: 0px; top: 100px; overflow: scroll;">
<FORM name='forma' id='forma' method='POST' action="<%=CONTROLLER%>?estado=Menu&accion=ModificarOpcion&carpeta=menu&pagina=ModificarOpcionMenu.jsp">
  <table width="446" border="2" align="center">
  <tr>
    <td><table width="100%" class="tablaInferior">
    <tr class="titulo" align="center">
      <td><table width="100%"  border="0" cellspacing="0" cellpadding="0">
  <tr class="barratitulo">
    <td width="37%" class="subtitulo1">Modificar Menu\Opcion</td>
    <td width="63%"><img src="<%=BASEURL%>/images/titulo.gif" width="32" height="20" align="left"><%=datos[0]%></td>
  </tr>
</table>
</td>
    </tr>
    <tr class="fila">
      <td>
	    <table width="100%" border="0" cellpadding="0" cellspacing="0">
        <tr class="subtitulo1">
          <td width="8%"><img src="<%= BASEURL %>/images/menu-images/home.gif" border="0"></td>
          <td width="92%">[ FINTRAVALORES  S. A. ]</td>
        </tr>
        <tr>
          <td>&nbsp;</td>
          <td><%Vector vmenu = model.menuService.getVMenu();
            	for (int i = 0; i < vmenu.size(); i++){
                	Menu m = (Menu) vmenu.elementAt(i);
                    int idop = m.getIdopcion();
                	String mnom = m.getNombre();
					if (m.getNivel() == 1){%>
              <table class="letraresaltada" width="99%" border="0" cellpadding="0" cellspacing="0">
                <tr onMouseOver="bgColor= '#99cc99'" onMouseOut="bgColor='#EAFFEA'" 
					onClick="window.location='<%=CONTROLLER%>?estado=Menu&accion=CargaxPerfil&direccion=/jsp/trafico/menu/ModificarOpcionMenu2.jsp&idop=<%=idop%>&nop=<%=mnom%>&cmd=modop&msg='" style="cursor:hand">
                  <td width="5%">
				  <%if ( m.getSubmenu() == 1 ){
				  	out.println("<img src=" + BASEURL + "/images/menu-images/menu_folder_closed.gif>");
				  } else {
					out.println("<img src=" + BASEURL + "/images/menu-images/menu_link_default.gif>");
				  }%></td>
                  <td width="80%">&nbsp;&nbsp;<font size="-2">
                    <% out.println(mnom);%>
                  </font></td>
                  <td width="14%"><font size="-2"><%=m.getOrden()%></font></td>
                </tr>
              </table>
              <%	}//if nivel
				}//for%>
          </td>
        </tr>
      </table>
	  <input name="id_padre" type="hidden" id="id_padre" value="0">
	 </td>
    </tr>
	<tr class="fila">	<td ><div align="center"><img src="<%=BASEURL%>/images/botones/salir.gif" name="c_salir" onClick="window.close();" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" align="absmiddle"></div></td></tr>
  </table></td>
  </tr>
</table>
</form>
</div>
 <%=datos[1]%>
</body>
</html>