<!--
- Autor : Ing. Sandra M Escalante G
- Date  : 18 de Mayo
- Copyrigth Notice : Fintravalores S.A. S.A
-->
<%--
-@(#)
--Descripcion : Ayuda
--%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
"http://www.w3.org/TR/html4/loose.dtd">
<%@include file="/WEB-INF/InitModel.jsp"%>
<html>
<head>
<title>.: Ayuda - Descripcion Campos - Insertar Nueva Opci�n</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link href="../../../css/estilostsp.css" rel="stylesheet" type="text/css">
<link href="<%= BASEURL %>/css/estilostsp.css" rel="stylesheet" type="text/css">
<script type='text/javascript' src="<%= BASEURL %>/js/boton.js"></script>
</head>

<body>
<br>
<table width="696"  border="2" align="center">
  <tr>
    <td width="811" height="" >
      <table width="100%" border="0" align="center">
        <tr  class="subtitulo">
          <td height="24" colspan="2" align="center">INSERTAR OPCION</td>
        </tr>
		<tr class="subtitulo1">
          <td colspan="2"> DATOS DE LA OPCION</td>
        </tr>
        <tr>
          <td width="149" class="fila"> Nombre </td>
          <td width="525"  class="ayudaHtmlTexto">Nombre de la opci�n. Esta informacion es la que se va a ver en el Menu. </td>
        </tr>
        <tr>
          <td width="149" class="fila"> Descripci�n </td>
          <td width="525"  class="ayudaHtmlTexto">Informaci�n adicional al nombre de la opci�n. </td>
        </tr>
        <tr>
          <td class="fila">Orden</td>
          <td  class="ayudaHtmlTexto">Lugar donde va a salir la opcion cuando se despliegue en el menu. Este valor es num�rico y en el menu se organiza de menor a mayor orden.</td>
        </tr>
        <tr>
          <td class="fila">Menu/Opci�n</td>
          <td  class="ayudaHtmlTexto">Selecci�n. Si se desea crear es una carpeta selecciona 'Menu', si por el contrario se desea crear un opci�n selecciona 'Opci�n'.</td>
        </tr>
		<tr class="subtitulo1">
          <td colspan="2">DATOS DEL ARCHIVO FUENTE</td>
        </tr>
		<tr>
          <td class="fila">Carpeta</td>
          <td  class="ayudaHtmlTexto">Direccion/Ruta donde se encuentra el archivo fuente (*.jsp) que se va a mostrar cuando se seleccione la opci�n en el menu.</td>
        </tr>
		<tr>
		  <td class="fila">Pagina</td>
		  <td  class="ayudaHtmlTexto">Nombre del archivo. Este es un archivo *.jsp.</td>
	    </tr>
		<tr>
		  <td class="fila">Otros</td>
		  <td  class="ayudaHtmlTexto">Parametros adicionales que el usuario necesite enviar en el URL para la ejecuci�n del programa. Ejemplo: &amp;marco=no. Si el usuario desea enviar mas de un par�metro, separa estos por &amp;. </td>
	    </tr>
    </table>    </td>
  </tr>
</table>
<br>
<center>
	<img src="<%=BASEURL%>/images/botones/salir.gif" name="c_salir" onClick="window.close();" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" align="absmiddle">
</center>
<br>
<p align="center" class="fuenteAyuda"> Fintravalores S. A. </p>
<br>
</body>
</html>