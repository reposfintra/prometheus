<!--
- Autor : Ing. Armando Oviedo C
- Date  : 19 de Diciembre de 2005
- Copyrigth Notice : Fintravalores S.A. S.A
-->
<%--
-@(#)
--Descripcion : Pagina JSP, que modifica o anula un vehiculo no retornado
--%>

<%-- Declaracion de librerias--%>
<%@page contentType="text/html"%>
<%@page session="true"%>
<%@page errorPage="../error/ErrorPage.jsp"%>
<%@page import="com.tsp.operation.model.*"%>
<%@page import="com.tsp.util.*"%>
<%@page import="com.tsp.operation.model.beans.*, java.util.*"%>
<%@include file="/WEB-INF/InitModel.jsp"%>
<%@ taglib uri="http://jakarta.apache.org/taglibs/input-0.90" prefix="input" %>
<html>
<head>
<title>Modificar Vehiculo No Retornado</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link href="<%= BASEURL %>/css/estilostsp.css" rel="stylesheet" type="text/css">
<link href="../../../css/estilostsp.css" rel="stylesheet" type="text/css"> 
<script type='text/javascript' src="<%= BASEURL %>/js/boton.js"></script>
</head>
<body onload="<%if(request.getParameter("reload")!=null){%> window.opener.location.reload();<%}%> redimensionar();" onresize="redimensionar()">
<div id="capaSuperior" style="position:absolute; width:100%; height:100px; z-index:0; left: 0px; top: 0px;">
	<jsp:include page="/WEB-INF/toptsp.jsp?encabezado=Modificar Vehiculo No Retornado"/>
</div>
<div id="capaCentral" style="position:absolute; width:100%; height:87%; z-index:0; left: 0px; top: 100px; overflow: scroll;">
<%
	VehNoRetornado tmp = model.vehnrsvc.getVehiculoNoRetornado();	
	String mensajemod = request.getParameter("mensajemod");
	if(mensajemod!=null){
		out.print("<table width=450 border=2 align=center><tr><td><table width=100%  border=1 align=center  bordercolor=#F7F5F4 bgcolor=#FFFFFF><tr><td width=350 align=center class=mensajes>"+mensajemod+"</td><td width=100><img src="+BASEURL+"/images/cuadronaranja.JPG></td></tr></table></td></tr></table>");%>		
		<br>
		<table width='450' align=center>
			<tr>
				<td align=center><img title='Regresar' src="<%= BASEURL %>/images/botones/salir.gif" onmouseover="botonOver(this);" onmouseout="botonOut(this);" onclick="window.close();"></img></td>
			</tr>
		</table>
	<%} 
	else if(tmp==null){
		out.print("<table width=450 border=2 align=center><tr><td><table width=100%  border=1 align=center  bordercolor=#F7F5F4 bgcolor=#FFFFFF><tr><td width=350 align=center class=mensajes>Descuento No Existe</td><td width=100><img src="+BASEURL+"/images/cuadronaranja.JPG></td></tr></table></td></tr></table>");%>
		<br>
		<table width='450' align=center>
			<tr>
				<td align=center><img title='Regresar' src="<%= BASEURL %>/images/botones/salir.gif" onmouseover="botonOver(this);" onmouseout="botonOut(this);" onclick="window.close();"></img></td>
			</tr>
		</table>
	<%}
	else{			
%>
<form name="forma" action="<%=CONTROLLER%>?estado=VehNoRetornado&accion=Modificar&mensaje=modificar" method="post">
<input type="hidden" name="fecha" value="<%=tmp.getFecha()%>"></input>
<input type="hidden" name="placa" value="<%=tmp.getPlaca()%>"></input>
  <table width="450"  border="2" align="center">
  	<tr>
    	<td>
			<table width="100%" align="center">
				<tr>
					<td width="50%" class="subtitulo1" nowrap align="center">Datos</td>
					<td width="50%" class="barratitulo"><img src="<%=BASEURL%>/images/titulo.gif" width="32" height="20"></td>
				</tr>
				</table>
				<table width="100%" >
				<tr class="fila">
				  <td width="30%">Fecha</td>
				  <td><%=tmp.getFecha().substring(0,10)%></td>
			  </tr>
				<tr class="fila">
				  <td width="30%">Placa</td>
				  <td><%=tmp.getPlaca()%></td>
				</tr>
				<tr class="fila">
				  <td width="30%">Causa</td>
				  <td nowrap>
				  	<input:select name="causa" options="<%=model.vehnrsvc.getCausasTblGeneral()%>" default="<%=(tmp.getCausa()!=null ? tmp.getCausa() : "")%>"/>
				   	<img src="<%=BASEURL%>/images/botones/iconos/obligatorio.gif" width="10" height="10"></img>
				  </td>
				</tr>				
		  </table>
  		</td>
  	</tr>
  </table><br>
  <table align=center width='450'>
    <tr>
        <td align="center" colspan="2">
            <img title='Modificar' src="<%= BASEURL %>/images/botones/modificar.gif" onmouseover="botonOver(this);" onmouseout="botonOut(this);" onclick="forma.submit();"></img>                        
			<img title='Eliminar' src="<%= BASEURL %>/images/botones/eliminar.gif" onmouseover="botonOver(this);" onmouseout="botonOut(this);" onclick="document.forma.action='<%=CONTROLLER%>?estado=VehNoRetornado&accion=Modificar&mensaje=eliminar';document.forma.submit();"></img>
            <img title='Salir' src="<%= BASEURL %>/images/botones/salir.gif" onmouseover="botonOver(this);" onmouseout="botonOut(this);" onClick="window.close()"></img>
        </td>
    </tr>
  </table>
  
  </form>
  <br>
  
  <%}%>  
</div>
</body>
</html>
