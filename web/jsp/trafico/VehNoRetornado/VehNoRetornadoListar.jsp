<!--
- Autor : Ing. Armando Oviedo C
- Date  : 18 de Diciembre de 2005
- Copyrigth Notice : Fintravalores S.A. S.A
-->
<%--
-@(#)
--Descripcion : Pagina JSP, que lista todos los vehÝculos no retornados
--%>            

<%-- Declaracion de librerias--%>
<%@page contentType="text/html"%>
<%@page session="true"%>
<%@page errorPage="../error/ErrorPage.jsp"%>
<%@page import="java.util.*"%>
<%@page import="com.tsp.operation.model.*"%>
<%@page import="com.tsp.util.*"%>
<%@page import="com.tsp.operation.model.beans.*, java.util.*"%>
<%@include file="/WEB-INF/InitModel.jsp"%>
<%@ taglib uri="http://jsptags.com/tags/navigation/pager" prefix="pg" %>
<html>
<head>
<title>Listado De VehÝculos No Retornados</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link href="<%= BASEURL %>/css/estilostsp.css" rel="stylesheet" type="text/css">
<script type='text/javascript' src="<%= BASEURL %>/js/boton.js"></script>
<link href="../../../css/estilostsp.css" rel="stylesheet" type="text/css"> 
<script type='text/javascript' src="<%= BASEURL %>/js/reporte.js"></script> 
</head>
<body onresize="redimensionar()" onload = 'redimensionar()'>
<div id="capaSuperior" style="position:absolute; width:100%; height:100px; z-index:0; left: 0px; top: 0px;">
	<jsp:include page="/WEB-INF/toptsp.jsp?encabezado=Listado De VehÝculos No Retornados"/>
</div>
<div id="capaCentral" style="position:absolute; width:100%; height:87%; z-index:0; left: 0px; top: 100px; overflow: scroll;">
<%    
    
    Vector reportes = model.vehnrsvc.getTodosVehiculosNoRetornados();
    String style = "simple";
    String position =  "bottom";
    String index =  "center";
    int maxPageItems = 10;
    int maxIndexPages = 10;    
%>

<table width="100%" border="2" align="center">
    <tr>
        <td>
            <table width="100%" align="center" class="tablaInferior"> 
                <tr>
                    <td colspan='3'>                
                        <table width="100%"  border="0" cellpadding="0" cellspacing="0" class="barratitulo">
                            <tr>
                                <td width="50%" class="subtitulo1" colspan='3'>Datos</td>
                                <td width="50%" class="barratitulo" colspan='2'><img src="<%=BASEURL%>/images/titulo.gif"></td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td colspan='3'>
                        <table width="100%" border="1" align="center" bordercolor="#999999" bgcolor="#F7F5F4">
                            <tr class="tblTitulo">
                                <td><div align="center"><strong>Fecha</strong></div></td>    
                                <td><div align="center"><strong>Placa</strong></div></td>
                                <td><div align="center"><strong>Causa</strong></div></td>                                
                            </tr>
                                  <pg:pager
                                    items="<%=reportes.size()%>"
                                    index="<%= index %>"
                                    maxPageItems="<%= maxPageItems %>"
                                    maxIndexPages="<%= maxIndexPages %>"
                                    isOffset="<%= true %>"
                                    export="offset,currentPageNumber=pageNumber"
                                    scope="request">
                                  <%-- keep track of preference --%>
                                  <%
                                      for (int i = offset.intValue(),l = Math.min(i + maxPageItems, reportes.size());i < l; i++){
                                        VehNoRetornado tmp = (VehNoRetornado)(reportes.elementAt(i));%>
                                    <pg:item>
                          <tr  class="<%=(i % 2 == 0 )?"filagris":"filaazul"%>" onMouseOver='cambiarColorMouse(this)'   style="cursor:hand"
                            onClick="window.open('<%=CONTROLLER%>?estado=VehNoRetornado&accion=Buscar&mensaje=obtenervnr&fechar=<%=tmp.getFecha()%>&placa=<%=tmp.getPlaca()%>' ,'','status=yes,scrollbars=no,width=700,height=470,resizable=yes')">
                            <td align=center class="bordereporte"><%=tmp.getFecha()%></td>
                            <td align=center class="bordereporte"><%=tmp.getPlaca()%></td>
                            <td align=center class="bordereporte"><%=tmp.getCausa()%></td>
                          </tr>
                                    </pg:item>
                                      <%}
                                      %>
                          <tr class="fila">
                            <td height="30" colspan="5" nowrap>
								<pg:index>
									<jsp:include page="/WEB-INF/jsp/googlesinimg.jsp" flush="true"/>								
								</pg:index>
							</td>
                          </tr>
                          </pg:pager>
                        </table>
                    </td>
                </tr>
            </table>
        </td>
    </tr>
  </table>
<br>
<table width='100%' align=center border=0>
  <tr class="titulo">
    <td align=left>
        <img title='Regresar' src="<%= BASEURL %>/images/botones/regresar.gif" onmouseover="botonOver(this);" onmouseout="botonOut(this);" onclick="window.location = '<%=BASEURL%>/jsp/trafico/VehNoRetornado/VehNoRetornadoBuscar.jsp'"></img>
    </td>
  </tr>
</table>
</div>
 </body>
</html>
