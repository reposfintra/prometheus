<!--
- Autor : Ing. Jose de la rosa
- Date  : 9 de Nobiembre de 2005
- Copyrigth Notice : Fintravalores S.A. S.A
-->
<%--
-@(#)
--Descripcion : Pagina JSP, que busca las discrepancias por fecha de cierre o sin fecha de cierre
--%>

<%-- Declaracion de librerias--%>
<%@page contentType="text/html"%>
<%@page session="true"%>
<%@page errorPage="../error/ErrorPage.jsp"%>
<%@page import="java.util.*"%>
<%@page import="com.tsp.operation.model.*"%>
<%@page import="com.tsp.util.*"%>
<%@page import="com.tsp.operation.model.beans.*, java.util.*"%>
<%@include file="/WEB-INF/InitModel.jsp"%>
<%@ taglib uri="http://jakarta.apache.org/taglibs/input-0.90" prefix="input" %>
<html>
<head>
<title>Insertar Justificacion</title>
<link href="<%= BASEURL %>/css/estilostsp.css" rel="stylesheet" type="text/css">
<script type='text/javascript' src="<%= BASEURL %>/js/boton.js"></script>
</head>
<script type='text/javascript' src="<%= BASEURL %>/js/validar.js"></script>
<body ONLOAD="redimensionar()" onresize="redimensionar()">  
<div id="capaSuperior" style="position:absolute; width:100%; height:100px; z-index:0; left: 0px; top: 0px;">
<jsp:include page="/WEB-INF/toptsp.jsp?encabezado=Justificación"/>
</div>

<div id="capaCentral" style="position:absolute; width:100%; height:85%; z-index:0; left: 0px; top: 100px; overflow: scroll;"> 

<%
        String programa = request.getParameter ("c_programa");
        String tipo = request.getParameter ("c_tipo").toUpperCase ();
        String numpla = request.getParameter ("numpla").toUpperCase ();
		String origen = request.getParameter ("origen");
		String destino = request.getParameter ("destino");
		String ruta = request.getParameter ("ruta");
		String ubicacion = request.getParameter ("ubicacion");
		String fechareporte = request.getParameter ("fechareporte");
		TablaGen t = model.tablaGenService.getTblgen ();
%>
<FORM name='forma' id='forma' method='POST' action='<%=CONTROLLER%>?estado=Cambio_via&accion=RepMovTrafico&opcion=1&pagina=JustificacionInsertar.jsp&carpeta=jsp/trafico/justificacion'>
  <table width="650" border="2" align="center">
    <tr>
      <td>
        <table width="100%" class="tablaInferior">
          <tr class="fila">
            <td colspan="2" align="left" class="subtitulo1">&nbsp;Justificaci&oacute;n</td>
            <td colspan="2" align="left" class="barratitulo"><img src="<%=BASEURL%>/images/titulo.gif" width="32" height="20"></td>
          </tr>
          <tr class="fila">
            <td width="70" align="left" >Programa</td>
            <td width="279" align="left" ><%=programa%></td>
            <td width="82" valign="middle">Documento</td>
            <td width="362" valign="middle"><%=numpla%></td>
          </tr>
          <tr class="fila">
            <td align="left" >Tipo</td>
            <td colspan="3" align="left" ><%=t.getDescripcion ()%></td>
          </tr>		
          <tr class="fila">
            <td colspan="4" align="center" >Descripci&oacute;n</td>
          </tr>		    
          <tr class="fila">
            <td colspan="4"  align="center"><textarea name="c_descripcion" cols="120" rows="6" class="textbox" id="c_descripcion"></textarea></td>
          </tr>
      </table></td>
    </tr>
  </table>
  <input name="c_programa" type="hidden" value="<%=programa%>">
  <input name="c_tipo" type="hidden" value="<%=tipo%>">
  <input name="c_documento" type="hidden" value="<%=numpla%>">
  <input name="tubicacion" type="hidden" value="PC">
	<input type="hidden" name="origen" value="<%=origen%>">
	<input type="hidden" name="destino" value="<%=destino%>">  
	<input type="hidden" name="ubicacion" value="<%=ubicacion%>"> 
	<input type="hidden" name="ruta" value="<%=ruta%>">
	<input type="hidden" name="numpla" value="<%=numpla%>">
	<input type="hidden" name="fechareporte" value="<%=fechareporte%>">
</FORM>
<p align="center">
<img src="<%=BASEURL%>/images/botones/aceptar.gif" style="cursor:hand" title="Agregar una justificación" name="imgaceptar"  onclick="/*this.disabled=true;*/return TCamposLlenos();" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);">&nbsp;
<img src="<%=BASEURL%>/images/botones/cancelar.gif" style="cursor:hand" name="regresar" title="Limpiar todos los registros" onMouseOver="botonOver(this);" onClick="forma.reset();" onMouseOut="botonOut(this);" >&nbsp;
<img src="<%=BASEURL%>/images/botones/salir.gif" style="cursor:hand" name="salir" title="Salir al Menu Principal" onClick="parent.close();" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);">&nbsp;
<img src="<%=BASEURL%>/images/botones/regresar.gif" style="cursor:hand" title="Volver" name="buscar"  onClick="window.location='<%=CONTROLLER%>?estado=Buscar&accion=NumPlanilla&mensaje=default&pagina=movtrafing.jsp&carpeta=jsp/trafico/Movimiento_trafico&mensaje=default&numpla=<%=numpla%>'" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);">
</p>
  <%String mensaje = (String) request.getAttribute("msg"); 
 %>
    <%  if(mensaje!=null){%>
            <br>
            <table border="2" align="center">
              <tr>
                <td><table width="100%"  border="1" align="center"  bordercolor="#F7F5F4" bgcolor="#FFFFFF">
                    <tr>
                      <td width="229" align="center" class="mensajes"><%=mensaje%></td>
                      <td width="29" background="<%=BASEURL%>/images/cuadronaranja.JPG">&nbsp;</td>
                      <td width="58">&nbsp;</td>
                    </tr>
                </table></td>
              </tr>
            </table>
    <%  }%>	
</div>
</body>
</html>
