<%@ page session="true"%>
<%@ page errorPage="../error/ErrorPage.jsp"%>
<%@ include file="/WEB-INF/InitModel.jsp"%>
<%@ taglib uri="http://jakarta.apache.org/taglibs/input-0.90" prefix="input" %>
<%@page import="com.tsp.operation.model.*, com.tsp.operation.model.beans.*, com.tsp.util.*,java.util.*, java.text.*"%>

<html>
<head>
<title>Reversar Viaje</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link href="../css/estilostsp.css" rel="stylesheet" type="text/css">
<script type='text/javascript' src="<%= BASEURL %>/js/boton.js"></script>
<script type='text/javascript' src="<%= BASEURL %>/js/utilidades.js"></script>
<link href="<%=BASEURL%>/css/estilostsp.css" rel="stylesheet" type="text/css">
<%
    String mensaje = (String) request.getAttribute("mensaje");
    String mostrar = (String) request.getAttribute("mostrarInfo");
    String numpla = (request.getParameter("numpla")!=null)?request.getParameter("numpla"):"";
    Planilla pla = model.planillaService.getPlanilla();
    String path = application.getRealPath(model.menuService.getRealUrl(request.getRequestURI()));
    String datos[] = model.menuService.getContenidoMenu(BASEURL,request.getRequestURI(),path);        
%>
<script>
    function enviarPlanilla(){
        if(trim(form1.numpla.value) == "" ){
            alert( "Digite n�mero de planilla" );
            form1.numpla.focus();
            return false;
        }
        form1.paso.value = "2";
        form1.submit();
    }
    function buscarPlanilla(){
        if(trim(form1.numpla.value) == "" ){
            alert( "Digite n�mero de planilla" );
            form1.numpla.focus();
            return false;
        }
        form1.paso.value = "1";        
    }
</script>
</head>

<body>
<div id="capaSuperior" style="position:absolute; width:100%; height:100px; z-index:0; left: 1px; top: 0px;">
 <jsp:include page="/toptsp.jsp?encabezado=Reversar Viaje"/>
</div>
<div id="capaCentral" style="position:absolute; width:100%; height:87%; z-index:0; left: 0px; top: 100px; overflow: scroll;">        
<form id="form1" method="post" action="<%=CONTROLLER%>?estado=Manager&accion=ReversarViaje" onsubmit="return buscarPlanilla();">
    <input type="hidden" id="paso" name="paso">
    <table width="50%"  border="2" align="center">
      <tr>
        <td><table width="100%"  border="1" align="center" class="tablaInferior">
          <tr>
            <td><table width="100%" border="0" cellpadding="0" cellspacing="0">
                <tr>
                  <td height="22" colspan=2 class="subtitulo1"><div align="left" class="subtitulo1" align="left">
                     <strong>Informaci�n Planilla</strong>                     
                  </div></td>
                  <td width="212" class="barratitulo">
                    <img src="<%=BASEURL%>/images/titulo.gif" width="32" height="20" align="left">
                    <%=datos[0]%>
                </tr>
              </table>
                <table width="100%"  border="0" align="center" cellpadding="2" cellspacing="3">                  
                  <tr class="fila">
                    <td width="31%">N�mero: </td>
                    <td><input type="text" name="numpla" id="numpla" value="<%=numpla%>" >
                    <input type="image" alt="Haga click para buscar la planilla" src="<%=BASEURL%>/images/botones/iconos/lupa.gif" width="15" height="15" style="cursor:hand" ></td>
                  </tr>
                  <%if( pla != null && mostrar != null ){%>
                      <tr class="fila">
                        <td width="31%">Placa: </td>
                        <td><%= pla.getPlaveh() %>&nbsp;</td>
                      </tr>
                      <tr class="fila">
                        <td width="31%">Ruta: </td>
                        <td><%= pla.getRuta_pla() %>&nbsp;</td>
                      </tr>
                      <tr class="fila">
                        <td width="31%">Origen: </td>
                        <td><%= pla.getNomori() %>&nbsp;</td>
                      </tr>
                      <tr class="fila">
                        <td width="31%">Destino: </td>
                        <td><%= pla.getNomdest() %>&nbsp;</td>
                      </tr>
                      <tr class="fila">
                        <td width="31%">Fecha Planilla: </td>
                        <td><%= pla.getFecdsp() %>&nbsp;</td>
                      </tr>
                  <%}%>
              </table></td>
          </tr>
        </table></td>
      </tr>
    </table>
    <br>
    <div align="center">      
        <%if( pla != null && mostrar != null ){%>
        <img id="baceptar" name="baceptar" src="<%=BASEURL%>/images/botones/aceptar.gif" style="cursor:hand"  onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" alt="Haga click para reversar el viaje" onclick="enviarPlanilla();" >
        <%}
          if(mensaje!=null){%>            
            <img  src="<%=BASEURL%>/images/botones/cancelar.gif" style="cursor:hand"  onClick="location.replace('<%=CONTROLLER%>?estado=Menu&accion=Cargar&carpeta=/jsp/trafico/ReversarViaje&pagina=ReversarViaje.jsp&marco=no');" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);">
        <%}%>
        <img src="<%=BASEURL%>/images/botones/salir.gif"  height="21" name="imgsalir"  onMouseOver="botonOver(this);" onClick="window.close();" onMouseOut="botonOut(this);" style="cursor:hand">            
    </div>
	</form>
    <br/>
        
        <%if(mensaje!=null){%>
        <table border="2" align="center">
            <tr>                
                <td height="45"><table width="410" height="41" border="1" align="center"  bordercolor="#F7F5F4" bgcolor="#FFFFFF">
                    <tr>
                        <td width="282" height="35" align="center" class="mensajes"><%=mensaje%></td>
                        <td width="28" background="<%=BASEURL%>/images/cuadronaranja.JPG">&nbsp;</td>
                        <td width="78">&nbsp;</td>
                    </tr>
                </table></td>
            </tr>
        </table>
        <br>
        <%}%>
        
	
</div>	
<%=datos[1]%>
</body>
</html>
