<%-- 
    Document   : reliquidarNegociosFintra.jsp
    Created on : 11/06/2018, 02:17:57 PM
    Author     : mcamargo
--%>

<%@page contentType="text/html" pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
        <title>Reliquidar Negocios Fintra</title>
        
        <link type="text/css" rel="stylesheet" href="./css/jquery/jquery-ui/jquery-ui.css" />
        <link href="./css/popup.css" rel="stylesheet" type="text/css">

        <script type="text/javascript" src="./js/jquery/jquery.jqGrid/js/jquery.min.js"></script> 
        <script type="text/javascript" src="./js/jquery-ui-1.8.5.custom.min.js"></script>
        <script src="./js/reliquidarNegociosFintra.js" type="text/javascript"></script>
        
          <link type="text/css" rel="stylesheet" href="./css/contratos.css " />
     
        <!--jqgrid--> 
        <script type="text/javascript" src="./js/jquery/jquery.jqGrid/js/grid.locale-es.js"></script>
        <script type="text/javascript" src="./js/jquery/jquery.jqGrid/js/jquery.jqGrid.min.js"></script>
    </head>
    <body>
        <div id="capaSuperior"  style="position:absolute; width:100%; height:100px; z-index:0; left: 0px; top: 0px;">
            <jsp:include page="/WEB-INF/toptsp.jsp?encabezado=Reliquidar negocios fintra"/>
        </div>
        <div id="capaCentral" style="position:absolute; width:100%; height:100%; z-index:0; left: 0px; top: 100px; ">
            <br>
            <center>
                 <table id="tabla_negocios_reliquidar"></table>
                 <div id="page_tabla_negocios_reliquidar"></div>    
            </center>
            <div id="dialogLoading" style="display:none;">
                <p  style="font-size: 12px;text-align:justify;" id="msj2">Texto </p> <br/>
                <center>
                    <img src="./images/cargandoCM.gif"/>
                </center>
            </div>
            <div id="dialogMsj" title="Mensaje" style="display:none;">
                <p style="font-size: 12px;text-align:justify;" id="msj" > Texto </p>
            </div>      
        </div>
        <div id="div_reliquidar_negocio"  style="width: 530px;display:none;" > 
               <table aling="center" style=" width: 100%" >
                    <tr>                
                        <td>
                            <label> Afiliado <span style="color:red;">*</span></label> 
                        </td>
                        <td>
                            <input id="afiliado" type="text" maxlength="5" readonly="readonly"/>
                            <input  id="tipo_carrera" readonly="readonly"  hidden >
                             <input id="cod_cli" type="text"  readonly="readonly" hidden/>
                        </td>                       
                    </tr>
                    <tr>
                        <td>
                            <label> Numero Solicitud <span style="color:red;">*</span></label> 
                        </td>
                        <td>
                            <input id="numero_solicitud" class="solo-numero" readonly="readonly" />
                            <input id="negocio" readonly="negocio" hidden />
                        </td>                                           
                    </tr>  
                    <tr>
                        <td>
                            <label> Convenio<span style="color:red;">*</span></label> 
                        </td>
                        <td>
                            <input id="convenio" class="solo-numero" readonly="readonly" />
                        </td>                                            
                    </tr>  
                    <tr>               
                        <td>
                            <label> Cuotas <span style="color:red;">*</span></label> 
                        </td>
                        <td>
                             <select name="cuota" id="cuota">  </select>
                        </td>                       
                    </tr>
                    <tr>
                        <td
                            <label> Valor <span style="color:red;">*</span></label> 
                        </td>
                        <td>
                            <input id="valor" class="solo-numero" />
                        </td>
                    </tr>
                    <tr>
                        <td
                            <label> Fecha cuota <span style="color:red;">*</span></label> 
                        </td>
                        <td>
                           <select name="fechacuota" id="fechacuota">  </select>
                        </td>
                    </tr>
                  
                </table> 
        </div> 
        
        <div id="div_documen_neg_aceptados"  style="width: 530px;display:none;">
             <table id="tabla_documen_neg_aceptados"></table>
        </div>
    </body>
</html>
