<!--
- Autor : Ing. David Lamadrid
- Date  : 5 de Octubre de 2005
- Copyrigth Notice : Fintravalores S.A. S.A
-->
<%--
-@(#)
--Descripcion : Pagina JSP, que se encarga de realizar y mostrar busqueda de los nits de proveedores
--%>

<%-- Declaracion de librerias--%>
<%@page contentType="text/html"%>
<%@page session="true"%>
<%@page import="java.util.*"%>
<%@page import="com.tsp.operation.model.*"%>
<%@page import="com.tsp.util.*"%>
<%@page import="com.tsp.operation.model.beans.*, java.util.*"%>
<%@include file="/WEB-INF/InitModel.jsp"%>
<%@taglib uri="http://jakarta.apache.org/taglibs/input-0.90" prefix="input" %>
<html>
<head>
<title>Buscar Carga</title>
<link href="../css/estilostsp.css" rel="stylesheet" type="text/css"> 
<link href="<%=BASEURL%>/css/estilostsp.css" rel="stylesheet" type="text/css"> 
<script type='text/javascript' src="<%=BASEURL%>/js/Validacion.js"></script> 
<script type='text/javascript' src="<%= BASEURL %>/js/boton.js"></script>
</head>
<script>
	/*Ivan Dario 28 Octubre 2006*/
       function asignarCodigoP(codigo,desc,desc_corta){
	      
		   parent.opener.document.forma1.cod_carga.value =codigo;
		   parent.opener.document.forma1.desc_carga.value =desc;
                   parent.opener.document.forma1.desc_carga_corta.value =desc_corta;
                   
                   var des =  parent.opener.document.forma1.descripcion;
		   var nombre = parent.opener.document.forma1.nom_cli.value;
		   var origen = parent.opener.document.forma1.origen.value;
                   var destino = parent.opener.document.forma1.destino.value;
                   
                   des.value = nombre+" "+ desc_corta + " " + origen + " " + destino; 
		   parent.close();
		
		
	}
	
	function procesarProveedores (element,subtotal,CONTROLLER, validar){
		if (window.event.keyCode==13)
		listaP(subtotal, validar);
	}
	
	/*function buscar (subtotal,CONTROLLER, estado,maxfila){
		if (window.event.keyCode==13)
		listaP(subtotal,CONTROLLER,estado, maxfila);
	}*/
	function listaP(CONTROLLER){
		document.forma1.action = CONTROLLER+"?estado=Standar&accion=Grabacion&opcion=BUSCAR_CARGA_LUPA";
		document.forma1.submit();
	}
</script>
<body onLoad="forma1.descripcion.focus();" onresize="redimensionar()">
<div id="capaSuperior" style="position:absolute; width:100%; height:100px; z-index:0; left: 0px; top: 0px;">
 <jsp:include page="/toptsp.jsp?encabezado=Buscar Carga"/>
</div>
<div id="capaCentral" style="position:absolute; width:100%; height:87%; z-index:0; left: 0px; top: 100px; overflow: scroll;">
<%
	 String accion = (request.getParameter("accion")==null?"":request.getParameter("accion"));
	
	 
	
%>
<form name="forma1" action="<%=CONTROLLER%>?estado=Standar&accion=Grabacion&opcion=BUSCAR_CARGA_LUPA" method="post" >
  <table border="2" align="center" width="566">
    <tr>
    <td width="610" >
        <table width="100%" align="center"  >
          <tr>
            <td width="295" height="24"  class="subtitulo1"><p align="left">Buscar Carga </p></td>
            <td width="265"  class="barratitulo"><img src="<%=BASEURL%>/images/titulo.gif"></td>
          </tr>
        </table>        
        <table width="100%" align="center" >
          <tr class="fila">
            <td width="107" >Descripcion : </td>
            <td width="424" > 
              <input name="descripcion" type="text" class="textbox" id="nomciu" size="18" maxlength="15"   >
      
      &nbsp;

<img src='<%=BASEURL%>/images/botones/buscar.gif' name='Buscar' align="absmiddle" style='cursor:hand' title='Aceptar...'  onclick="listaP('<%=CONTROLLER%>');" onMouseOver='botonOver(this);' onMouseOut='botonOut(this);'></td>
          </tr>
        </table></td>
</tr>
</table>




<%
    if (accion.equals("1")){
        List listaCarga =  model.StandarSvc.getListaCargas();
%>
  <table width="566" border="2" align="center">
    <tr>
    <td width="585">
<table width="555"  >
          <tr>
    <td width="346"  class="subtitulo1"><p align="left">Cargas</p></td>
    <td width="193"  class="barratitulo"><img src="<%=BASEURL%>/images/titulo.gif"></td>
  </tr>
</table>
        <table width="556" >
          <tr class="tblTitulo" id="titulos">
    <td width="56" height="22">codigo</td>
    <td width="334" >descripcion</td>
    </tr>
<%
        for(int i=0; i< listaCarga.size();i++){
            EquivalenciaCarga eq =(EquivalenciaCarga)listaCarga.get(i);
%>  
  <tr class="fila">
    <td width="56" height="22"><%=eq.getCodigo_carga()%></td>
    <td width="334" ><a  onClick="asignarCodigoP('<%=eq.getCodigo_carga()%>','<%=eq.getDescripcion_carga()%>','<%=eq.getDescripcion_corta()%>')" style="cursor:hand"><%=eq.getDescripcion_carga()%></a></td>
    </tr>
<%
        }
%>  
</table>
</td>
</tr>
</table>
<%
    }
%>
</form>
<div align="center"><img src="<%=BASEURL%>/images/botones/salir.gif"  name="salir" title="Salir..." onClick="parent.close();" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);">
</div>
</div>
</body>
</html>
    
