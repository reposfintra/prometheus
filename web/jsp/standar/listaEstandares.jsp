<%@ page session="true"%>
<%@ page errorPage="/error/error.jsp"%>
<%@page import="com.tsp.operation.model.beans.*, java.util.*"%>
<%@ include file="/WEB-INF/InitModel.jsp"%>
<%@ taglib uri="http://jsptags.com/tags/navigation/pager" prefix="pg" %>
<%String path = application.getRealPath(model.menuService.getRealUrl(request.getRequestURI()));
  String datos[] = model.menuService.getContenidoMenu(BASEURL,request.getRequestURI(),path);
%>
<html>
<head>

<title>Consulta de Estandares</title>
<script language="javascript" src="../../../js/validar.js"></script>
<link href="<%= BASEURL %>/css/estilostsp.css" rel="stylesheet" type="text/css">
<script SRC='<%=BASEURL%>/js/boton.js'></script>
<link href="../../../css/estilostsp.css" rel="stylesheet" type="text/css">
<script type='text/javascript' src="<%= BASEURL %>/js/reporte.js"></script>

</head>
<body onLoad="redimensionar();" onResize="redimensionar();">
<div class="fila" style="position:absolute; visibility:hidden; width:350px; height:50px; z-index:1; border: 2 outset #003399; " id="msg">
</div>
<div id="capaSuperior" style="position:absolute; width:100%; height:100px; z-index:0; left: 0px; top: 0px;">
<jsp:include page="/WEB-INF/toptsp.jsp?encabezado=Estandares"/>
</div>
<div id="capaCentral" style="position:absolute; width:100%; height:85%; z-index:0; left: 0px; top: 100px; overflow: scroll;"> 

<%    
    List listaEstandares = model.StandarSvc.getListaEstandares();
%>
<table border="2" align="center">
  <tr>
    <td><table width="100%">
      <tr>
        <td width="50%" class="subtitulo1">Lista  de Estandares </td>
        <td width="50%" class="barratitulo"><img align="left" src="<%=BASEURL%>/images/titulo.gif"><%=datos[0]%></td>
      </tr>
    </table>      
      <table border="1" align="center" bordercolor="#999999" bgcolor="#F7F5F4">
      <tr class="tblTitulo"  >
        <td rowspan="2" nowrap>&nbsp;</td>
        <td rowspan="2" nowrap><div align="center">Codigo sj</div></td>
        <td rowspan="2" nowrap><div align="center">Descripcion</div></td>
        <td rowspan="2" nowrap><div align="center">Tipo</div></td>
        <td rowspan="2" nowrap><div align="center">Codigo Cliente</div></td>
        <td rowspan="2" nowrap><div align="center">Nombre Cliente</div></td>
        <td rowspan="2" nowrap><div align="center">Agencia Due�a</div></td>
        <td rowspan="2" nowrap><div align="center">Codigo Pagador</div></td>
        <td rowspan="2" nowrap><div align="center">Origen</div></td>
        <td rowspan="2" nowrap><div align="center">Nombre Origen</div></td>
        <td rowspan="2" nowrap><div align="center">Destino</div></td>
        <td rowspan="2" nowrap><div align="center">Nombre Destino</div></td>
        <td rowspan="2" nowrap><div align="center">Bloq. Despacho</div></td>
        <td rowspan="2" nowrap><div align="center">Autorizador</div></td>
        <td rowspan="2" nowrap><div align="center">Agente Responsable</div></td>
        
        <td rowspan="2" nowrap><div align="center">Viaje Vacio</div></td>
        <td rowspan="2" nowrap><div align="center">Req. Remesa Padre </div></td>
        <td rowspan="2" nowrap><div align="center">Remesa Facturable</div></td>
        
        <td rowspan="2" nowrap><div align="center">Tipo Ruta</div></td>
        <td rowspan="2" nowrap><div align="center">Codigo de Carga</div></td>
        <td rowspan="2" nowrap><div align="center">Tipo Facturacion</div></td>
        <td rowspan="2" nowrap><div align="center">Vlr Tope de Carga</div></td>
        <td colspan="2" nowrap>Periodo de Facturacion<div align="center"></div></td>
      </tr>
      <tr class="tblTitulo"  >
        <td  width='50%' align="center">Dia inicial </td>
        <td  width='50%' align="center">Dia Final </td>
      </tr>
      
      <%for(int i=0; i< listaEstandares.size();i++){
           Standar sj = (Standar) listaEstandares.get(i);  
      %>
      <!-- Inicio de las filas donde estan los datos -->
       <tr class="<%=(i % 2 == 0 )?"filagris":"filaazul"%>" >
        <td class="bordereporte" nowrap><img title="Ver detalles" src="<%= BASEURL %>/images/botones/iconos/detalles.gif" style="cursor:hand"  name="imgsalir"  onClick="window.open('<%= CONTROLLER %>?estado=Standar&accion=Consultar&opcion=DETALLE&distrito=<%=sj.getDstrct_code()%>&codigo_sj=<%= sj.getCodigo_sj()%>&cod_cli=<%= sj.getCod_cli()%>','DETALL','status=yes,scrollbars=no,width=780,height=650,resizable=yes');"></td>
        <td nowrap class="bordereporte"><div align="center"><%=sj.getCodigo_sj()%></div></td>
        <td nowrap class="bordereporte"><div align="center"><%=sj.getDescripcion_sj()%></div></td>
        <td nowrap class="bordereporte"><div align="center"><%=sj.getTipo()%></div></td>
        <td nowrap class="bordereporte"><div align="center"><%=sj.getCod_cli()%></div></td>
        <td nowrap class="bordereporte"><div align="center"><%=sj.getNomcli()%></div></td>
        <td nowrap class="bordereporte"><div align="center"><%=sj.getAg_duenia()%></div></td>
        <td nowrap class="bordereporte"><div align="center"><%=sj.getPagador()%></div></td>
        <td nowrap class="bordereporte"><div align="center"><%=sj.getOrigen()%></div></td>
        <td nowrap class="bordereporte"><div align="center"><%=sj.getNom_origen()%></div></td>
        <td nowrap class="bordereporte"><div align="center"><%=sj.getDestino()%></div></td>
        <td nowrap class="bordereporte"><div align="center"><%=sj.getNom_destino()%></div></td>
        <td nowrap class="bordereporte"><div align="center"><%=(sj.isBloq_despacho())?"S":"N"%></div></td>
        <td nowrap class="bordereporte"><div align="center"><%=sj.getE_mail()%></div></td>
        <td nowrap class="bordereporte"><div align="center"><%=sj.getAg_responsable()%></div></td>
        
        <td nowrap class="bordereporte"><div align="center"><%=(sj.isInd_vacio())?"S":"N"%></div></td>
        <td nowrap class="bordereporte"><div align="center"><%=(sj.isReq_remesa_padre())?"S":"N"%></div></td>
        <td nowrap class="bordereporte"><div align="center"><%=(sj.isRemesaFacturable())?"S":"N"%></div></td>
        
        <td nowrap class="bordereporte"><div align="center"><%=sj.getTipo_ruta()%></div></td>
        <td nowrap class="bordereporte"><div align="center"><%=sj.getCodigo_carga()%></div></td>
        <td nowrap class="bordereporte"><div align="center"><%=sj.getTipo_facturacion()%></div></td>
        <td nowrap class="bordereporte"><div align="center"><%=sj.getVlr_tope_carga()%></div></td>
        <td nowrap class="bordereporte"><div align="center"><%=sj.getPeriodoFac_inicial()%></div></td>
        <td nowrap class="bordereporte"><div align="center"><%=sj.getPeriodoFac_final()%> </div></td>
      </tr>
      
      <%}%>
      
      <!-- fin de los datos-->
  
    </table></td>
  </tr>
</table>
<br>  
  
<% if( request.getParameter("msg") != null ){%>
<p>
<table border="2" align="center">
  <tr>
    <td><table width="100%"  border="1" align="center"  bordercolor="#F7F5F4" bgcolor="#FFFFFF">
        <tr>
          <td width="229" align="center" class="mensajes"><%=request.getParameter("msg").toString()%></td>
          <td width="29" background="<%=BASEURL%>/images/cuadronaranja.JPG">&nbsp;</td>
          <td width="58">&nbsp;</td>
        </tr>
    </table></td>
  </tr>
</table>
<p></p>
<p>
    <%} %>
</p>
<table width="100%" border="0" align="center" cellpadding="0" cellspacing="0">
  <tr align="left">
    <td><img src="<%=BASEURL%>/images/botones/regresar.gif" name="c_regresar" id="c_regresar" style="cursor:hand " onClick="window.location.href = '<%= CONTROLLER%>?estado=Menu&accion=Cargar&carpeta=/jsp/standar&pagina=ReporteEstandares.jsp&marco=no';" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);">&nbsp;  </tr>
</table>
</div>
<%=datos[1]%>
</body>
</html>



