<!--
- Autor : Ing. David Lamadrid
- Date  : 5 de Octubre de 2005
- Copyrigth Notice : Fintravalores S.A. S.A
-->
<%--
-@(#)
--Descripcion : Pagina JSP, que se encarga de realizar y mostrar busqueda de los nits de proveedores
--%>

<%-- Declaracion de librerias--%>
<%@page contentType="text/html"%>
<%@page session="true"%>
<%@page import="java.util.*"%>
<%@page import="com.tsp.operation.model.*"%>
<%@page import="com.tsp.util.*"%>
<%@page import="com.tsp.operation.model.beans.*, java.util.*"%>
<%@include file="/WEB-INF/InitModel.jsp"%>
<%@taglib uri="http://jakarta.apache.org/taglibs/input-0.90" prefix="input" %>
<html>
<head>
<title>Buscar Nit</title>
<link href="../css/estilostsp.css" rel="stylesheet" type="text/css"> 
<link href="<%=BASEURL%>/css/estilostsp.css" rel="stylesheet" type="text/css"> 
<script type='text/javascript' src="<%=BASEURL%>/js/Validacion.js"></script> 
<script type='text/javascript' src="<%= BASEURL %>/js/boton.js"></script>
</head>
<script>
	/*Ivan Dario 28 Octubre 2006*/
    function asignarCiudadItem(codigo,nombre,id){
          
          var campo = parent.opener.document.getElementById(id);
          campo.value = codigo;
          var de = (id.substr(0,1) =="o")?"nomorigen"+id.substr(6,id.length):"nomdestino"+id.substr(7,id.length);
          
          var desc = parent.opener.document.getElementById(de);
	  desc.value = nombre; 
	  parent.close();	
         
    }	
	
    function asignarCodigoP(codigo,nombre,op){
	       if(op=='ORIGEN'){
	           parent.opener.document.forma1.origen.value =codigo;
	           parent.opener.document.forma1.nomorigen.value =nombre;
			    
	           var des =  parent.opener.document.forma1.descripcion;
		   var nombre = parent.opener.document.forma1.nom_cli.value;
		   var desc_corta = parent.opener.document.forma1.desc_carga_corta.value;
                   var destino = parent.opener.document.forma1.destino.value;
                   
                   des.value = nombre+" "+ desc_corta + " " + codigo + " " + destino; 
	           
               }else{
                   parent.opener.document.forma1.destino.value =codigo;
                   parent.opener.document.forma1.nomdestino.value =nombre;
	           
                   var des =  parent.opener.document.forma1.descripcion;
		   var nombre = parent.opener.document.forma1.nom_cli.value;
		   var desc_corta = parent.opener.document.forma1.desc_carga_corta.value;
                   var origen = parent.opener.document.forma1.origen.value;
                   
                   des.value = nombre+" "+ desc_corta + " " + origen + " " + codigo; 
               }
               parent.close();
		
		
	}
	
	function procesarProveedores (element,subtotal,CONTROLLER, validar){
		if (window.event.keyCode==13)
		listaP(subtotal, validar);
	}
	
	/*function buscar (subtotal,CONTROLLER, estado,maxfila){
		if (window.event.keyCode==13)
		listaP(subtotal,CONTROLLER,estado, maxfila);
	}*/
	function listaP(CONTROLLER,op,id){
		document.forma1.action = CONTROLLER+"?estado=Standar&accion=Grabacion&opcion=BUSCAR_CIUDAD_LUPA&op="+op+"&id="+id;
		document.forma1.submit();
	}
</script>
<body onLoad="forma1.nomciu.focus();" onresize="redimensionar()">
<div id="capaSuperior" style="position:absolute; width:100%; height:100px; z-index:0; left: 0px; top: 0px;">
 <jsp:include page="/toptsp.jsp?encabezado=Buscar Ciudad"/>
</div>
<div id="capaCentral" style="position:absolute; width:100%; height:87%; z-index:0; left: 0px; top: 100px; overflow: scroll;">
<%
	 String accion = (request.getParameter("accion")==null?"":request.getParameter("accion"));
	 String op = (request.getParameter("op")==null?"":request.getParameter("op"));
	 String id = (request.getParameter("id")==null?"":request.getParameter("id")); 
	
%>
<form name="forma1" action="<%=CONTROLLER%>?estado=Standar&accion=Grabacion&opcion=BUSCAR_CIUDAD_LUPA&op=<%=op%>&id=<%=id%>" method="post" >
  <table border="2" align="center" width="566">
    <tr>
    <td width="610" >
        <table width="100%" align="center"  >
          <tr>
            <td width="295" height="24"  class="subtitulo1"><p align="left">Buscar Ciudad </p></td>
            <td width="265"  class="barratitulo"><img src="<%=BASEURL%>/images/titulo.gif"></td>
          </tr>
        </table>        
        <table width="100%" align="center" >
          <tr class="fila">
            <td width="107" >Nombre Ciudad </td>
            <td width="424" > 
              <input name="nomciu" type="text" class="textbox" id="nomciu" size="18" maxlength="15"   >
      
      &nbsp;

<img src='<%=BASEURL%>/images/botones/buscar.gif' name='Buscar' align="absmiddle" style='cursor:hand' title='Aceptar...'  onclick="listaP('<%=CONTROLLER%>','<%=op%>','<%=id%>');" onMouseOver='botonOver(this);' onMouseOut='botonOut(this);'></td>
          </tr>
        </table></td>
</tr>
</table>




<%
    if (accion.equals("1")){
        List ciudades =  model.StandarSvc.getListaCiudad();
%>
  <table width="566" border="2" align="center">
    <tr>
    <td width="585">
<table width="555"  >
          <tr>
    <td width="346"  class="subtitulo1"><p align="left">Ciudades</p></td>
    <td width="193"  class="barratitulo"><img src="<%=BASEURL%>/images/titulo.gif"></td>
  </tr>
</table>
        <table width="556" >
          <tr class="tblTitulo" id="titulos">
    <td width="56" height="22">codigo</td>
    <td width="334" >nombre</td>
    </tr>
<%
        for(int i=0; i< ciudades.size();i++){
            Ciudad ciudad =(Ciudad)ciudades.get(i);
%>  
  <tr class="fila">
    <td width="56" height="22"><%=ciudad.getCodCiu()%></td>
    <%if(!op.equals("ITEM")){%>
    <td width="334" ><a  onClick="asignarCodigoP('<%=ciudad.getCodCiu()%>','<%=ciudad.getNomCiu()%>','<%=op%>')" style="cursor:hand" ><%=ciudad.getNomCiu()%></a></td>
    <%}else{%>
     <td width="334" ><a  onClick="asignarCiudadItem('<%=ciudad.getCodCiu()%>','<%=ciudad.getNomCiu()%>','<%=id%>')" style="cursor:hand" ><%=ciudad.getNomCiu()%></a></td>
    <%}%>
    </tr>
<%
        }
%>  
</table>
</td>
</tr>
</table>
<%
    }
%>
</form>
<div align="center"><img src="<%=BASEURL%>/images/botones/salir.gif"  name="salir" title="Salir..." onClick="parent.close();" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);">
</div>
</div>
</body>
</html>
    
