<!--
- Autor : Ing. David Lamadrid
- Date  : 5 de Octubre de 2005
- Copyrigth Notice : Fintravalores S.A. S.A
-->
<%--
-@(#)
--Descripcion : Pagina JSP, que se encarga de realizar y mostrar busqueda de los nits de proveedores
--%>

<%-- Declaracion de librerias--%>
<%@page contentType="text/html"%>
<%@page session="true"%>
<%@page import="java.util.*"%>
<%@page import="com.tsp.operation.model.*"%>
<%@page import="com.tsp.util.*"%>
<%@page import="com.tsp.operation.model.beans.*, java.util.*"%>
<%@include file="/WEB-INF/InitModel.jsp"%>
<%@taglib uri="http://jakarta.apache.org/taglibs/input-0.90" prefix="input" %>
<html>
<head>
<title>Buscar Nit</title>
<link href="../css/estilostsp.css" rel="stylesheet" type="text/css"> 
<link href="<%=BASEURL%>/css/estilostsp.css" rel="stylesheet" type="text/css"> 
<script type='text/javascript' src="<%=BASEURL%>/js/Validacion.js"></script> 
<script type='text/javascript' src="<%= BASEURL %>/js/boton.js"></script>
</head>
<script>
	/*Ivan Dario 28 Octubre 2006*/
    function asignarCodigoP(codigo,agduenia,nombre,sec,op){
          if(op=='CLIENTE'){
               parent.opener.document.forma1.ag_duenia.value =agduenia;
               parent.opener.document.forma1.nom_cli.value =nombre;
               parent.opener.document.forma1.cod_cli.value ='000'+codigo;
               parent.opener.document.forma1.codigo_sj.value =codigo+sec;			   		   			    

               var des =  parent.opener.document.forma1.descripcion;
               var des_corta = parent.opener.document.forma1.desc_carga_corta.value;
               var origen = parent.opener.document.forma1.origen.value;
               var destino = parent.opener.document.forma1.destino.value;

               des.value = nombre+" "+ des_corta + " " + origen + " " + destino; 
           }else{
               parent.opener.document.forma1.pagador.value ='000'+codigo;
           }
           parent.close();
	}
	
	function procesarProveedores (element,subtotal,CONTROLLER, validar){
		if (window.event.keyCode==13)
		listaP(subtotal, validar);
	}
	
	/*function buscar (subtotal,CONTROLLER, estado,maxfila){
		if (window.event.keyCode==13)
		listaP(subtotal,CONTROLLER,estado, maxfila);
	}*/
	function listaP(CONTROLLER,op){
		document.forma1.action = CONTROLLER+"?estado=Standar&accion=Grabacion&opcion=BUSCAR_CLIENTE_LUPA&op="+op;
		document.forma1.submit();
	}
</script>
<body onLoad="forma1.nomcli.focus();" onresize="redimensionar()">
<div id="capaSuperior" style="position:absolute; width:100%; height:100px; z-index:0; left: 0px; top: 0px;">
 <jsp:include page="/toptsp.jsp?encabezado=Buscar Cliente"/>
</div>
<div id="capaCentral" style="position:absolute; width:100%; height:87%; z-index:0; left: 0px; top: 100px; overflow: scroll;">
<%
	 String accion = (request.getParameter("accion")==null?"":request.getParameter("accion"));
	 String op = (request.getParameter("op")==null?"":request.getParameter("op"));
	 
	
%>
<form name="forma1" action="<%=CONTROLLER%>?estado=Standar&accion=Grabacion&opcion=BUSCAR_CLIENTE_LUPA&op=<%=op%>" method="post" >
  <table border="2" align="center" width="566">
    <tr>
    <td width="610" >
        <table width="100%" align="center"  >
          <tr>
            <td width="295" height="24"  class="subtitulo1"><p align="left">Buscar Cliente </p></td>
            <td width="265"  class="barratitulo"><img src="<%=BASEURL%>/images/titulo.gif"></td>
          </tr>
        </table>        
        <table width="100%" align="center" >
          <tr class="fila">
            <td width="107" >Nombre : </td>
            <td width="424" > 
              <input name="nomcli" type="text" class="textbox" id="nomciu" size="18" maxlength="15"   >
      
      &nbsp;

<img src='<%=BASEURL%>/images/botones/buscar.gif' name='Buscar' align="absmiddle" style='cursor:hand' title='Aceptar...'  onclick="listaP('<%=CONTROLLER%>','<%=op%>');" onMouseOver='botonOver(this);' onMouseOut='botonOut(this);'></td>
          </tr>
        </table></td>
</tr>
</table>




<%
    if (accion.equals("1")){
        List clientes =  model.StandarSvc.getListaClientes();
%>
  <table width="566" border="2" align="center">
    <tr>
    <td width="585">
<table width="555"  >
          <tr>
    <td width="346"  class="subtitulo1"><p align="left">Clientes</p></td>
    <td width="193"  class="barratitulo"><img src="<%=BASEURL%>/images/titulo.gif"></td>
  </tr>
</table>
        <table width="556" >
          <tr class="tblTitulo" id="titulos">
    <td width="56" height="22">codigo</td>
    <td width="334" >nombre</td>
    </tr>
<%
        for(int i=0; i< clientes.size();i++){
            Cliente cli =(Cliente)clientes.get(i);
%>  
  <tr class="fila">
    <td width="56" height="22">000<%=cli.getCodcli()%></td>
    <td width="334" ><a  onClick="asignarCodigoP('<%=cli.getCodcli()%>','<%=cli.getAgduenia()%>','<%=cli.getNomcli()%>','<%=cli.getSec_standard()%>','<%=op%>')" style="cursor:hand" ><%=cli.getNomcli()%></a></td>
    </tr>
<%
        }
%>  
</table>
</td>
</tr>
</table>
<%
    }
%>
</form>
<div align="center"><img src="<%=BASEURL%>/images/botones/salir.gif"  name="salir" title="Salir..." onClick="parent.close();" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);">
</div>
</div>
</body>
</html>
    
