<%@page session="true"%> 
<%@ page import    ="java.util.*" %>
<%@page import="com.tsp.util.*"%>
<%@page import="com.tsp.finanzas.contab.model.*"%>
<%@page import="com.tsp.operation.model.beans.*"%>
<%@page errorPage="/error/ErrorPage.jsp"%>
<%@include file="/WEB-INF/InitModel.jsp"%>

<% 
   
   String opcion  = request.getParameter("opcion");
   String mensaje = (request.getParameter("mensaje")!=null)?request.getParameter("mensaje"):"";
   String op = request.getParameter("op");
  
 %>
<script>
  
   var html ="";
    <%if(opcion.equals("CLIENTE")){
	       if(model.StandarSvc.getCliente()!= null){
	       %>
	           var ag_duenia = parent.forma1.ag_duenia;
		   var nomcli  = parent.forma1.nom_cli;
		   var codigo_sj  = parent.forma1.codigo_sj;
		    
		   nomcli.value  ='<%=model.StandarSvc.getCliente().getNomcli()%>';
		   ag_duenia.value  ='<%=model.StandarSvc.getCliente().getAgduenia()%>';	
		   codigo_sj.value  ='<%=model.StandarSvc.getCliente().getCodcli()%>'+'<%=model.StandarSvc.getCliente().getSec_standard()%>';			   	    
		     
		   var des =  parent.forma1.descripcion;
		   var des_corta = parent.forma1.desc_carga_corta.value;
		   var origen = parent.forma1.origen.value;
                   var destino = parent.forma1.destino.value;
                   
                   des.value = nomcli.value+" "+ des_corta + " " + origen + " " + destino; 
		  <%}else{%>
		       alert('El cliente no existe')
			   var cliente  = parent.forma1.cod_cli;
			   var ag_duenia = parent.forma1.ag_duenia;
		       var nomcli  = parent.forma1.nom_cli;
		       var codigo_sj  = parent.forma1.codigo_sj;	
			   ag_duenia.value="";
			   nomcli.value="";
			   codigo_sj.value="";
			   cliente.focus();
		  <%}%>
     <%}else if(opcion.equals("PAGADOR")){
	       if(model.StandarSvc.getCliente()== null){
	       %>
		       alert('El codigo del pagador digitado no existe')
		       var pagador  = parent.forma1.pagador;
		       pagador.focus();
		  <%}%>
	       
    <%}else if(opcion.equals("CIUDAD")){
 	       if(model.StandarSvc.getCiudad()!= null){
	            if(op.equals("ORIGEN")){%>
  	           
                               var origen = parent.forma1.origen;
                               var nomorigen  = parent.forma1.nomorigen;
                               origen.value   = '<%=model.StandarSvc.getCiudad().getCodCiu()%>';
                               nomorigen.value= '<%=model.StandarSvc.getCiudad().getNomCiu()%>';

                               var descripcion =  parent.forma1.descripcion;
                               var nombre      = parent.forma1.nom_cli.value;
                               var des_corta   = parent.forma1.desc_carga_corta.value;
                               var destino     = parent.forma1.destino.value;

                               descripcion.value = nombre+" "+ des_corta + " " + origen.value + " " + destino; 
                  
					   
			   <%}else{%>
			           var destino = parent.forma1.destino;
                                   var nomdestino  = parent.forma1.nomdestino;
                                   destino.value   = '<%=model.StandarSvc.getCiudad().getCodCiu()%>';
                                   nomdestino.value= '<%=model.StandarSvc.getCiudad().getNomCiu()%>';
                                   
                                   var descripcion =  parent.forma1.descripcion;
                                   var nombre      = parent.forma1.nom_cli.value;
                                   var des_corta   = parent.forma1.desc_carga_corta.value;
                                   var origen     = parent.forma1.origen.value;

                                   descripcion.value = nombre+" "+ des_corta + " " + origen + " " + destino.value;
			   <%}
			     
		   }else if(op.equals("ORIGEN")){%>
		      alert('El codigo de ciudad digitado no existe');
			   var origen = parent.forma1.origen;
			   var nomorigen  = parent.forma1.nomorigen;
			   origen.focus();
			   nomorigen.value="";
	      <%}else{%>
		        alert('El codigo de ciudad digitado no existe');
		        var destino = parent.forma1.destino;
 			    var nomdestino  = parent.forma1.nomdestino;
				destino.focus()
				nomdestino.value="";
		  <%}%>
      <%}else if(opcion.equals("CARGA")){
             if(model.StandarSvc.getEqivalenciaCarga()!= null){%>
                  var codigo = parent.forma1.cod_carga;
                  var des    = parent.forma1.desc_carga;
                  var des_corta = parent.forma1.desc_carga_corta;
                  codigo.value   = '<%=model.StandarSvc.getEqivalenciaCarga().getCodigo_carga()%>';
                  des.value      = '<%=model.StandarSvc.getEqivalenciaCarga().getDescripcion_carga()%>';
                  des_corta.value ='<%=model.StandarSvc.getEqivalenciaCarga().getDescripcion_corta()%>';
                  
                  
                   var descripcion =  parent.forma1.descripcion;
		   var nombre = parent.forma1.nom_cli.value;
		   var origen = parent.forma1.origen.value;
                   var destino = parent.forma1.destino.value;
                   
                   descripcion.value = nombre+" "+ des_corta.value + " " + origen + " " + destino; 
                  
                  
             <%}else{%>
                  alert('El codigo de carga digitado no existe');
                  var codigo = parent.forma1.cod_carga;
                  var des    = parent.forma1.desc_carga;
                  var des_corta = parent.forma1.desc_carga_corta;
                  
                  codigo.focus();
                  des.value='';
                  des_corta.value='';
             <%}
        }%>
	 
   <% if (!mensaje.equals("")){%> 
	   alert('<%=mensaje%>');
   <%}%>
</script>

        
                   