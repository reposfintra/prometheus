<!--
- Autor : Ing. Ivan Dario Gomez
- Date  : 9 Enero 2007
- Copyrigth Notice : Fintravalores S.A. S.A
-->
<%--
-@(#)
--Descripcion : Pagina JSP, que se encarga de gestionar la modificacion de Standares
--%>

<%-- Declaracion de librerias--%>
<%@page contentType="text/html"%>
<%@page session="true"%>
<%@page import="java.util.*"%>
<%@page import="com.tsp.operation.model.*"%>
<%@page import="com.tsp.util.*"%>
<%@page import="com.tsp.operation.model.beans.*, java.util.*"%>
<%@include file="/WEB-INF/InitModel.jsp"%>
<%@ taglib uri="http://jakarta.apache.org/taglibs/input-0.90" prefix="input" %>
<%String path = application.getRealPath(model.menuService.getRealUrl(request.getRequestURI()));
  String datos[] = model.menuService.getContenidoMenu(BASEURL,request.getRequestURI(),path);
%>
<html>
    <head><title>Modificacion de Estandar</title>
        <link   href="../../css/estilostsp.css"rel="stylesheet" type="text/css">
        <link href="<%=BASEURL%>/css/estilostsp.css" rel="stylesheet" type="text/css">

        <script type='text/javascript' src="<%=BASEURL%>/js/Validacion.js"></script>
        <script type='text/javascript' src="<%= BASEURL %>/js/boton.js"></script>
        <script type='text/javascript' src="<%= BASEURL %>/js/posbancaria.js"></script>
        <script type='text/javascript' src="<%= BASEURL %>/js/cxp.js"></script>
<%List Unidades      = model.StandarSvc.getLista();
  List Agentes       = model.StandarSvc.getListaAgentes();
  List tipoRuta      = model.StandarSvc.getTiporuta();
  List ListaUnidades = model.StandarSvc.getListaUnidades();
  List monedas       = model.StandarSvc.getListaMonedas(); 
  List tipo_fact    = model.StandarSvc.getTipoFacturacion();
  List front_asoc   = model.StandarSvc.getFronteraAsociada();
  Standar sj = model.StandarSvc.getSj();  
%>

    </head>

    <body   onLoad="maximizar(); cargarDesc();"onresize="redimensionar()" >


        <div id="capaSuperior" style="position:absolute; width:100%; height:100px; z-index:0; left: 0px; top: 0px;">
            <jsp:include page="/toptsp.jsp?encabezado=Modificacion de Estandar job"/>
        </div>
        <div id="capaCentral" style="position:absolute; width:100%; height:87%; z-index:0; left: 0px; top: 91px; overflow: scroll;">
            <script>
                var controlador ="<%=CONTROLLER%>";
                
                function cargarDesc(){
                    forma1.descripcion.value = forma1.nom_cli.value+" "+forma1.desc_carga_corta.value+" "+forma1.origen.value+" "+forma1.destino.value; 
                }
                function agregarcorreo(){
                    obj = document.getElementById("bloqdespacho");
                    var mail_autorizador = document.getElementById("mail_autorizador");
                    if(obj.checked){
                        mail_autorizador.style.visibility ='visible';

                    }else{
                        mail_autorizador.style.visibility ='hidden';
                        mail_autorizador.value='';
                    }
                }

                function mostrarTexto(tipo){
                    var standar_general = document.getElementById("standar_general");
                    if(tipo=='G'){
                        standar_general.style.visibility ='visible';
                    }else{
                        standar_general.style.visibility ='hidden';
                        standar_general.value='';
                    }
                }

                function BuscarCliente(codcli,BASEURL,op){
                    if(codcli == 'LUPA'){
                        var dir = BASEURL+'/jsp/standar/buscarCliente.jsp?op='+op;
                        var hijo=window.open(dir,'Impuesto','width=700,height=520,scrollbars=no,resizable=yes,top=10,left=65,status=yes');
                        hijo.opener=document.window;
                    }else if(codcli!=''){
                        var cod='';
                        for(var i=0; i< (6 - codcli.length);i++ ){
                           cod +='0';
                        }
                        cod += codcli;
                        var id = (op == "PAGADOR")?"pagador":"cod_cli";
                        var codigo = document.getElementById(id);
                        codigo.value = cod;
                        var url = "?estado=Standar&accion=Consultar&cod_cli="+cod+"&opcion=BUSCAR_CLIENTE&tipo="+op;
                        enviar(url);
                    }
                }
                
                function buscarCarga(codcarga,BASEURL){
                    if(codcarga == 'LUPA'){
                        var dir = BASEURL+'/jsp/standar/buscarCarga.jsp';
                        var hijo=window.open(dir,'buscarcarga','width=700,height=520,scrollbars=no,resizable=yes,top=10,left=65,status=yes');
                        hijo.opener=document.window;
                    }else if(codcarga!=''){
                        var url = "?estado=Standar&accion=Consultar&cod_carga="+codcarga+"&opcion=BUSCAR_CARGA";
                        enviar(url);
                    }
                }

                function BuscarCiudad(BASEURL,op,codciu){
                    if(codciu=='LUPA'){
                        var dir = BASEURL+'/jsp/standar/buscarCiudad.jsp?op='+op;
                        var hijo=window.open(dir,'Impuesto','width=700,height=520,scrollbars=no,resizable=yes,top=10,left=65,status=yes');
                        hijo.opener=document.window;
                    }else if(codciu != ''){
                        var url = "?estado=Standar&accion=Consultar&codciu="+codciu+"&opcion=BUSCAR_CIUDAD&op="+op;
                        enviar(url); 
                    }
                }
                
                function BuscarCiudadItem(BASEURL,codciu,id){
                    if(codciu=='LUPA'){
                        var dir = BASEURL+'/jsp/standar/buscarCiudad.jsp?op=ITEM&id='+id;
                        var hijo=window.open(dir,'Impuesto','width=700,height=520,scrollbars=no,resizable=yes,top=10,left=65,status=yes');
                        hijo.opener=document.window;
                    }else if(codciu != ''){
                        var url = "?estado=Standar&accion=Consultar&codciu="+codciu+"&opcion=BUSCAR_CIUDAD&id="+id;
                        enviar(url); 
                    }
                }
                
                function BuscarRecurso(BASEURL,codrecurso,id,size){
                    if(codrecurso=='LUPA'){
                        var dir = BASEURL+'/jsp/standar/buscarRecurso.jsp?id='+id+'&size='+size;
                        var hijo=window.open(dir,'Impuesto','width=700,height=520,scrollbars=no,resizable=yes,top=10,left=65,status=yes');
                        hijo.opener=document.window;
                    }else if(codrecurso != ''){
                        var url = "?estado=Standar&accion=Consultar&codrecurso="+codrecurso+"&opcion=BUSCAR_RECURSO&id="+id+"&size="+size;
                        enviar(url); 
                    }
                }

                function verificarVigente(id){
                    
                    var check_vigente = document.getElementById(id);		
                    if(check_vigente.checked){
                        maxfila="<%=sj.getListaTarifa().size()%>";
                        for (var i = 0;i < maxfila ; i++){
                            var chek = document.getElementById("vigente"+i);		
                            if ( chek != null ){

                                if(id != 'vigente'+i) 
                                chek.checked = false;


                            }
                        }
                    }

                }

                function verificarFleteVigente(id){
                    var check_vigente = document.getElementById(id);		
                    if(check_vigente.checked){   
                        for (var i = 1;i <= mxfilaflete + 1 ; i++){
                        var chek = document.getElementById("flete_vigente"+i);		
                            if ( chek != null ){

                                if(id != 'flete_vigente'+i) 
                                chek.checked = false;


                            }
                        }
                    }

                }

                function soloDiasDelMes(id) {
                    var inicial = document.getElementById("dia_inicial");  
                    var final   = document.getElementById("dia_final"); 
 	           // alert("inicial-->"+inicial.value+"   final-->"+final.value)
                    if(id =='inicial' && inicial.value!=''){
                        if(inicial.value > 31){
                            alert('el dia no puede ser mayor a 31')
                            inicial.value="";
                            inicial.focus();
                            
                        }else if(final.value !='' && inicial.value > final.value){
                            alert('el dia inicial de facturacion no puede ser mayor al dia final');
                            inicial.value="";
                            inicial.focus();
                            
                        }
                    }else if(id =='final' && final.value!=''){
                        if(final.value > 31){
                            alert('el dia no puede ser mayor a 31')
                            final.value="";
                            final.focus();
                            
                        }else if(inicial.value !='' && inicial.value > final.value){
                            alert('el dia final de facturacion no puede ser menor al dia inicial');
                            final.value="";
                            final.focus();
                            
                        }
                    }  
   
                }

                function agregar(id,op){
                    document.forma1.action = controlador+"?estado=Standar&accion=Consultar&opcion=CARGAR&op="+op+"&id="+id;
                    document.forma1.submit();
		}
		
		 function eliminar(id,op){
                    document.forma1.action = controlador+"?estado=Standar&accion=Consultar&opcion=CARGAR&op="+op+"&id="+id;
                    document.forma1.submit();
		}
		
	
	
	
	function validarFrm(){
	   
	     if(forma1.cod_cli.value==''){
	        alert('El cliente no puede estar vacio');
	        forma1.cod_cli.focus();
	        return false;
	        
             }else if(forma1.ag_duenia.value==''){
                alert('la agencia due�a no puede estar vacia');
                forma1.ag_duenia.focus();
	        return false;
	        
             }else if(forma1.pagador.value==''){
                alert('el campo pagador no puede estar vacio');
                forma1.pagador.focus();
	        return false;
	        
             }else if(forma1.codigo_sj.value==''){
                alert('el campo codigo sj no puede estar vacio');
                forma1.codigo_sj.focus();
	        return false;
	        
             }else if(forma1.descripcion.value==''){
                alert('el campo descripcion no puede estar vacio');
                forma1.descripcion.focus();
	        return false;
	        
             }else if(forma1.tipo_standar.value=='G' && forma1.standar_general.value=='' ){
                alert('el campo Estandar general no puede estar vacio');
                forma1.standar_general.focus();
	        return false;
	        
             }else if(forma1.ag_responsable.value==''){
                alert('el campo ag_responsable no puede estar vacio');
                forma1.ag_responsable.focus();
	        return false;
	        
             }else if(forma1.origen.value==''){
                alert('el campo origen de la remesa no puede estar vacio');
                forma1.origen.focus();
	        return false;
	        
             }else if(forma1.destino.value==''){
                alert('el campo destino de la remesa no puede estar vacio');
                forma1.destino.focus();
	        return false;
	        
             }else if(forma1.dia_inicial.value==''){
                alert('el campo dia inicial de facturacion no puede estar vacio');
                forma1.dia_inicial.focus();
	        return false;
	        
             }else if(forma1.dia_final.value==''){
                alert('el campo dia final de facturacion no puede estar vacio');
                forma1.dia_final.focus();
	        return false;
	        
             }else if(forma1.cod_carga.value==''){
                alert('el campo codigo de carga no puede estar vacio');
                forma1.cod_carga.focus();
	        return false;
	        
             }else if(forma1.vlr_tope_carga.value==''|| forma1.vlr_tope_carga.value=='0'){
                alert('el campo vlr tope de carga no puede estar vacio');
                forma1.vlr_tope_carga.focus();
	        return false;
	        
             }else if(forma1.bloqdespacho.checked && forma1.mail_autorizador.value==''){
                alert('el campo Autorizador no puede estar vacio');
                forma1.cod_carga.focus();
	        return false;
	        
             }else if(forma1.desc_carga.value==''){
                alert('el campo Descripcion de la carga no puede estar vacio');
                forma1.desc_carga.focus();
	        return false;
	        
             }
             
             var sizeTarifa="<%=sj.getListaTarifa().size()%>";
             for(i=0; i< sizeTarifa ; i++){
                   var dato     = document.getElementById("vlr_tarifa"+i);
                   if(dato.value=='' || dato.value=='0'){
                      alert('El campo valor tarifa del item '+(i+1)+' no puede estar vacio');
                      dato.focus();
                      return false;
                   }
             }
              
             var sizeTramo="<%=sj.getListaTramos().size()%>";
             <%int tr =0;%>
             for(t=0; t< sizeTramo ; t++){
              
                   var dato     = document.getElementById("origen"+t);
                   var sizeRecurso = dato.parentNode.size_recurso;
                   if(dato.value==''){
                      alert('El campo Origen del item '+(t+1)+' no puede estar vacio');
                      dato.focus();
                      return false;
                   }
                   dato     = document.getElementById("nomorigen"+t);
                   if(dato.value==''){
                      alert('El nombre del Origen del item '+(t+1)+' no puede estar vacio');
                      dato.focus();
                      return false;
                   } 
                   dato     = document.getElementById("destino"+t);
                   if(dato.value==''){
                      alert('El campo Destino del item '+(t+1)+' no puede estar vacio');
                      dato.focus();
                      return false;
                   }
                   dato     = document.getElementById("nomdestino"+t);
                   if(dato.value==''){
                      alert('El Nombre del Destino del item '+(t+1)+' no puede estar vacio');
                      dato.focus();
                      return false;
                   }
                   dato     = document.getElementById("porcentaje_anticipo"+t);
                   if(dato.value==''){
                      alert('El campo porcentaje anticipo del item '+(t+1)+' no puede estar vacio');
                      dato.focus();
                      return false;
                   }
                   dato     = document.getElementById("peso_lleno_max"+t);
                   if(dato.value==''){
                      alert('El campo Peso lleno maximo del item '+(t+1)+' no puede estar vacio');
                      dato.focus();
                      return false;
                   }
                   dato     = document.getElementById("numero_escolta"+t);
                   if(dato.value=='' || dato.value=='0'){
                      alert('El campo numero de escolta del item '+(t+1)+' no puede estar vacio');
                      dato.focus();
                      return false;
                   }
                   <%int re=0;%>
                  
                   for(r=0; r< sizeRecurso ; r++){
                        var dato      = document.getElementById("cod_recurso"+t+r);
                        var sizeFlete = dato.parentNode.fletes;
                       
                        if(dato.value==''){
                           alert('El campo codigo de recurso del tramo '+(t+1)+' recurso '+(r+1)+' no puede estar vacio');
                           dato.focus();
                           return false;
                        }
                        rec = dato.value;
                        var dato     = document.getElementById("tipo_recurso"+t+r);
                        if(dato.value==''){
                           alert('El campo tipo de recurso del tramo '+(t+1)+' recurso '+(r+1)+' no puede estar vacio');
                           dato.focus();
                           return false;
                        }
                        var dato     = document.getElementById("desc_recurso"+t+r);
                        if(dato.value==''){
                           alert('El campo descripcion de recurso del tramo '+(t+1)+' recurso'+(r+1)+' no puede estar vacio');
                           dato.focus();
                           return false;
                        }
                                                
                        for(f=0; f< sizeFlete ; f++){
                         
                            var dato     = document.getElementById("vlr_flete"+t+r+f); 
                            if(dato.value=='' || dato.value=='0'){
                               alert('El campo vlr del flete del tramo '+(t+1)+' recurso '+(r+1)+'  flete '+(f+1)+' no puede estar vacio');
                               dato.focus();
                               return false;
                            }
                            
                            
                        }
                       
                        <%re++;%>
                   }
                 <%tr++;%> 
             }
             
              
              document.forma1.action = controlador+"?estado=Standar&accion=Consultar&opcion=CARGAR&op=GUARDAR";
              document.forma1.submit();
	
        }

            </script>

            <form name="forma1" id="forma1" action="" method="post"> 
<%
  String mensaje=""+request.getParameter("ms");
    if (! (mensaje.equals("")|| mensaje.equals("null"))){
%>

            <table border="2" align="center">
                <tr>
                    <td><table width="410" height="73" border="1" align="center"  bordercolor="#F7F5F4" bgcolor="#FFFFFF">
                        <tr>
                            <td width="282" align="center" class="mensajes"><%=mensaje%></td>
                            <td width="28" background="<%=BASEURL%>/images/cuadronaranja.JPG">&nbsp;</td>
                            <td width="78">&nbsp;</td>
                        </tr>
                    </table></td>
                </tr>
            </table>
            <div align="center"><br>
      <%
    }   
%>
         

                <!-- marco principal -->
                <table width="993" align="center">
                <tr><td width="985">
        
        
        
        
        
                    <!-- marco de los datos del estandar -->
                    <table border="2"  >
                    <tr >
                        <td width="971"> 
                        <table width="100%" align="center"  >
                            <tr >
                                <td width="50%"  class="subtitulo1"><p align="left">Documento</p></td>
                                <td width="50%"  class="barratitulo"><img src="<%=BASEURL%>/images/titulo.gif" class="Estilo1" height="20" align="left"><%=datos[0]%></td>
                            </tr>
                        </table>
                        <table width="100%" align="center" cols="7">
                        <tr class="fila">
                            <td width="135"  >Cliente:</td>
                            <td width="89" ><input name="cod_cli"  value='<%=sj.getCod_cli()%>' type="text" class="letra" style=" border:0" size="8" maxlength="6" readonly>
                            </td>
                            <td width="120" >Nombre:            </td>
                            <td colspan="3" ><input name="nom_cli"  value='<%=sj.getNomcli()%>' type="text" class="letra" style=" border:0" size="40" readonly></td>
                            <td colspan="2"><div align="center">Propiedades</div></td>
                        </tr>
                        <tr class="fila">
                            <td >Agencia due&ntilde;a:</td>
                            <td ><input name="ag_duenia" value='<%=sj.getAg_duenia()%>' type="text" class="letra" style=" border:0" size="10" readonly>
           		        
                            </td>
                            <td >Pagador:			  
         
                            </td>
                            <td width="152" ><input name="pagador"  value='<%=sj.getPagador()%>' type="text" class="textbox" size="8" maxlength="6" onBlur="BuscarCliente(this.value,'<%=BASEURL%>','PAGADOR')">
                                             <img src="<%= BASEURL %>/images/botones/iconos/lupa.gif" width="15" height="16" title="Buscar" style="cursor:hand" onClick="BuscarCliente('LUPA','<%=BASEURL%>','PAGADOR')">
                            </td>
                            <td width="133" >Unidad negocio: </td>
                            <td width="117" ><select name="unidadNegocio">
			        <% for(int i=0; i<Unidades.size();i++){
                   TablaGen t = (TablaGen) Unidades.get(i);%>
                            <option value="<%=t.getTable_code()%>" <%=(sj.getUnidad_negocio().equals(t.getTable_code()))?"selected":""%>><%=t.getTable_code()%></option>
		           <%}%>
                            </select></td>
                            <td width="145" >Planeable</td>
                            <td width="38" ><input type="checkbox" name="planeable" value="S" <%=(sj.isPlaneable())?"checked":""%>>
                            </td>
			        
                        </tr>
                        <tr class="fila">
                            <td><p>Codigo SJ:</p></td>
                            <td ><input name="codigo_sj" type="text" class="letra" style=" border:0" size="10" readonly value='<%=sj.getCodigo_sj()%>'></td>
                            <td >Descripcion:</td>
                            <td colspan="3" ><input name="descripcion" type="text" size="80" value='<%=sj.getDescripcion_sj()%>' readonly ></td>
                            <td >Viaje vacio </td>
                            <td >			<input type="checkbox" name="ind_vacio" value="S" <%=(sj.isInd_vacio())?"checked":""%>>
                            </td>
			        
                        </tr>
                        <tr class="fila">
                        <td>Tipo:</td>
                        <td ><select name="tipo_standar" onChange="mostrarTexto(this.value)" >
                        <option value="E" <%=(sj.getTipo().equals("E"))?"selected":""%>>Especifico</option>
                        <option value="G" <%=(sj.getTipo().equals("G"))?"selected":""%>>General</option>
                        </select></td>
                        <td >Estandar general: </td>
                        <td ><input name="standar_general" type="text" <%=(!sj.getTipo().equals("G"))?"style='visibility:hidden'":""%> maxlength="10" value="<%=sj.getStandar_general()%>"></td>
                        <td  >Ag. responsable:</td>
                        <td  ><select name="ag_responsable">
		             <% for(int i=0; i<Agentes.size();i++){
                   TablaGen ta = (TablaGen) Agentes.get(i);%>
                        <option value="<%=ta.getTable_code()%>"  <%=(sj.getAg_responsable().equals(ta.getTable_code()))?"selected":""%>><%=ta.getTable_code()%></option>
		           <%}%>
			         
                        </select></td>
                        <td >Remesa facturable</td>
                        <td ><input type="checkbox" name="remesa_facturable" value="S"  <%=(sj.isRemesaFacturable())?"checked":""%>></td>
                    </tr>
                    <tr class="fila">
                    <td>Origen de la remesa:</td>
                    <td ><input name="origen" value='<%=sj.getOrigen()%>' type="text" class="textbox" size="6" maxlength="2" style="text-transform:uppercase"  onBlur="BuscarCiudad('<%=BASEURL%>','ORIGEN',this.value)">
                    <img src="<%= BASEURL %>/images/botones/iconos/lupa.gif" width="15" height="16" title="Buscar" style="cursor:hand" onClick="BuscarCiudad('<%=BASEURL%>','ORIGEN','LUPA')"></td>
                    <td >Nombre origen :</td>
                    <td colspan="2"  ><input name="nomorigen" value='<%=sj.getNom_origen()%>' type="text" class="letra" style=" border:0" size="40" readonly></td>
                    <td  ><div align="center">Tipo ruta:</div></td>
                    <td >Req. remesa padre
                    <div align="center"></div></td>
                    <td ><input type="checkbox" name="remesapadre" value="S" <%=(sj.isReq_remesa_padre())?"checked":""%>></td>
                </tr>
                            <tr class="fila">
                            <td>Destino de la remesa:</td>
                            <td ><input name="destino" value='<%=sj.getDestino()%>'type="text" class="textbox" size="6" maxlength="2" style="text-transform:uppercase" onBlur="BuscarCiudad('<%=BASEURL%>','DESTINO',this.value)">
                            <img src="<%= BASEURL %>/images/botones/iconos/lupa.gif" width="15" height="16" title="Buscar" style="cursor:hand" onClick="BuscarCiudad('<%=BASEURL%>','DESTINO','LUPA')"></td>
                            <td   ><div id="mail"  >Nombre destino:</div></td>
                            <td colspan="2" ><input name="nomdestino"  value='<%=sj.getNom_destino()%>' type="text" class="letra" style=" border:0" size="40" readonly></td>
                            <td ><div align="center">
                                <select name="tipo_ruta">
			                <% for(int i=0; i<tipoRuta.size();i++){
                   TablaGen tab = (TablaGen) tipoRuta.get(i);%>
                                <option value="<%=tab.getTable_code()%>"    <%=(sj.getTipo_ruta().equals(tab.getTable_code()))?"selected":""%>><%=tab.getReferencia()%></option>
                            <%}%>
			                
                                </select>
                            </div></td>
                            <td ><div align="left">Bloq. despacho</div></td>
                            <td ><input type="checkbox" name="bloqdespacho" value="S" <%=(sj.isBloq_despacho())?"checked":""%> onClick="agregarcorreo()"></td>
                            </tr>
                            <tr class="fila">
                                <td>Dia inicial de facturacion : </td>
                                <td ><input name="dia_inicial" id='dia_inicial' value='<%=(sj.getPeriodoFac_inicial()!=0)?sj.getPeriodoFac_inicial():""%>' type="text" class="textbox" size="6" maxlength="2"  onKeyPress="soloDigitos(event, 'decNO')"    onblur="soloDiasDelMes('inicial')" ></td>
                                <td >Dia final de facturacion:</td>
                                <td ><input name="dia_final"  id='dia_final' value='<%=(sj.getPeriodoFac_final()!=0)?sj.getPeriodoFac_final():""%>' type="text" class="textbox" size="6" maxlength="2" onKeyPress="soloDigitos(event, 'decNO')"  onblur="soloDiasDelMes('final')"></td>
                                <td >Tipo Facturacion:</td>
                                <td ><select name="tipo_facturacion">
                                     <% for(int ti=0; ti<tipo_fact.size();ti++){
                                   TablaGen tipo = (TablaGen) tipo_fact.get(ti);%>
                                <option value="<%=tipo.getTable_code()%>"    <%=(sj.getTipo_facturacion().equals(tipo.getTable_code()))?"selected":""%>><%=tipo.getReferencia()%></option>
                                <%}%>
                                </select></td>
                                <td >&nbsp;</td>
                                <td >&nbsp;</td>
                            </tr>
                            <tr class="fila">
                                <td>Codigo de carga:</td>
                                <td ><input name="cod_carga"  value='<%=sj.getCodigo_carga()%>' type="text" class="textbox" size="6" maxlength="4" onblur="buscarCarga(this.value,'<%=BASEURL%>');">
                                <img src="<%= BASEURL %>/images/botones/iconos/lupa.gif" width="15" height="16" title="Buscar" style="cursor:hand" onclick="buscarCarga('LUPA','<%=BASEURL%>');" ></td>
                                <td >Vlr. tope carga:</td>
                                <td ><input name="vlr_tope_carga" type="text" style="text-align:right;" onfocus='this.select()' onblur='formatear(this)' onKeyPress="Digitos(event, 'decNO')" value='<%=UtilFinanzas.customFormat(sj.getVlr_tope_carga())%>' maxlength="10"></td>
                               
                                <td >Frontera Asociada:</td>
                                <td colspan="3" ><select name="frontera_asociada">
                                 <% for(int fa=0; fa<front_asoc.size();fa++){
                                   TablaGen fasoc = (TablaGen) front_asoc.get(fa);%>
                                <option value="<%=fasoc.getTable_code()%>"    <%=(sj.getFrontera_asoc().equals(fasoc.getTable_code()))?"selected":""%>><%=fasoc.getTable_code()%></option>
                                <%}%>
                                </select></td>
 
                            </tr>
                            <tr class="fila">
                                <td>Descripcion Carga: </td>
                                <td colspan="3" ><input name="desc_carga" type="text" size="70" value='<%=sj.getDesc_carga()%>' >
                                                 <input name="desc_carga_corta" type="hidden" size="70" value='<%=sj.getDesc_carga_corta()%>' >                   
                                </td>
                                <td > Autorizador:</td>
                                <td colspan="3" ><input name="mail_autorizador" value='<%=sj.getE_mail()%>' type="text" class="textbox" id="mail_autorizador" <%=(!sj.isBloq_despacho())?"style='visibility:hidden'":""%> size="40"></td>
                            </tr>
  		        
		   
		           
                        </table></td>
                        </tr>
                    </table>
                    <table   border="2" >
                        <tr >
                            <td width="971"> 
                            <table width="100%" align="center"  >
                                <tr >
                                    <td width="50%"  class="subtitulo1"><p align="left">Tarifa</p></td>
                                    <td width="50%"  class="barratitulo"><img src="<%=BASEURL%>/images/titulo.gif" class="Estilo1" height="20" align="left"></td>
                                </tr>
                            </table>
                            <table id="detalleTarifa" width="100%" align="center" cols="7">
                            <tr class="tblTitulo2">
                                <td width="93"  ><div align="center">Item</div></td>
                                <td width="201" ><div align="center">Valor</div></td>
                                <td width="202" ><div align="center">Moneda</div></td>
                                <td width="217" ><div align="center">Unidad</div></td>
                                <td width="228"><div align="center">Vigente</div></td>
                            </tr>
			    <%List listaTarifa = sj.getListaTarifa();
                              int x=1;
                               for(int i=0; i<listaTarifa.size();i++ ){
                                   Standar tarifa = (Standar) listaTarifa.get(i);
                              %>
                             <!-- inicio de las filas de las tarifas--> 
                            <tr class="fila">
                                <td ><table width="60" border="0" cellspacing="0" cellpadding="0" height="100%">
                                <tr>
                                <td width="60%" nowrap class="letraresaltada"><a><%=i+1%></a>
                                    <input name="cod_item<%=i%>" id="cod_item<%=i%>" value="<%=i%>" type="hidden" size="4" maxlength="5" border="0">
                                </td>
                                <td width="40%" nowrap><a onClick="agregar('<%=i%>','AGREGAR_TARIFA');"  id="insert_item<%=i%>" style="cursor:hand" ><img src='<%=BASEURL%>/images/botones/iconos/mas.gif'   width="12" height="12"  ></a> <a onClick="eliminar('<%=i%>','ELIMINAR_TARIFA');"  value="1" id="borrarI<%=i%>" style="cursor:hand" ><img id="imgini" name="imgini" src='<%=BASEURL%>/images/botones/iconos/menos1.gif'   width="12" height="12"  > </a></td>
                            </tr>
                            </table></td>
                            <td ><div align="center">
                                <input name="vlr_tarifa<%=i%>" type="text" style="text-align:right;" onfocus='this.select()'  onblur='formatear(this)' onKeyPress="Digitos(event, 'decNO')" value='<%= UtilFinanzas.customFormat( tarifa.getVlr_tarifa()) %>' maxlength="10">
                            </div></td>
                            <td ><div align="center">
                                <select name="moneda_tarifa<%=i%>">
                                     <% for(int m=0; m<monedas.size();m++){
                                           Moneda mo = (Moneda) monedas.get(m);%>
                                       <option value="<%=mo.getCodMoneda()%>" <%=(tarifa.getMoneda_tarifa().equals(mo.getCodMoneda()))?"selected":""%>><%=mo.getNomMoneda()%></option>
                                   <%}%>
                                </select>
                            </div></td>
                            <td ><div align="center">
                                <select name="unidad_tarifa<%=i%>">
                                 <% for(int h=0; h<ListaUnidades.size();h++){
                                            TablaGen tu = (TablaGen) ListaUnidades.get(h);%>
                                  <option value="<%=tu.getTable_code()%>" <%=(tarifa.getUnidad_tarifa().equals(tu.getTable_code()))?"selected":""%>><%=tu.getTable_code()%></option>
		                <%}%>
                                </select>
                            </div></td>
                            <td ><div align="center">
                                <input type="checkbox" name="vigente<%=i%>" value="S" <%=(tarifa.isVigente_tarifa())?"checked":""%> onClick="verificarVigente('vigente<%=i%>')">
                            </div></td>
                        </tr>
  		<%}//fin del for%>
                        </table></td>
                        </tr>
                    </table>
		

                    <!-- marco de tramos -->		
                    <table   border="2" width="100%" >
                        <tr >
                        <td width="100%"> 
              
              
                            <!-- encabezado detalle tramo -->          
                            <table width="100%" align="center"  >
                                <tr >
                                    <td width="50%"  class="subtitulo1"><p align="left">Tramos</p></td>
                                    <td width="50%"  class="barratitulo"><img src="<%=BASEURL%>/images/titulo.gif" class="Estilo1" height="20" align="left"></td>
                                </tr>
                            </table>
                            <!-- fin encabezado detalle tramo -->
                
                
                            <!-- inicio detalle tramo -->            
                            <table id="detalleTramo" width="100%" align="center" cols="7">
                              <%
                                List listaTramo = sj.getListaTramos();
                             
                               for(int t=0; t<listaTramo.size();t++ ){
                                   Standar tramo = (Standar) listaTramo.get(t);%>
                                <tr class="tblTitulo2">
                                    <td width="76"  ><div align="center">Item</div></td>
                                    <td width="264" ><div align="center">Origen</div></td>
                                    <td width="265" ><div align="center">Destino</div></td>
                                    <td width="83" ><div align="center">Porcentaje <br>Anticipo </div></td>
                                    <td width="83" ><div align="center">Peso Lleno <br>Maximo</div></td>
                                    <td width="83"><div align="center">Numero De Escoltas </div></td>
                                    <td width="83"><div align="center">Tipo<br>Trafico </div></td>
                                </tr>
					
		  	        
                                <!-- celdas de tramos -->
                                
                               
                                <tr class="fila" >
                                <td>
                                    <table width="60" border="0" cellspacing="0" cellpadding="0" height="100%">
                                        <tr>
                                        <td width="60%" nowrap class="letraresaltada">
                                            <a  ><%=t+1%></a>
                                            <input name="cod_item<%=t%>" id="cod_item_tramo<%=t%>" value="<%=t%>" type="hidden" size="4" maxlength="5" border="0">
                                        </td>
                                        <td width="40%" nowrap><a onClick="agregar('<%=t%>','AGREGAR_TRAMO');"  id="insert_item<%=t%>" style="cursor:hand" >
                                            <img src='<%=BASEURL%>/images/botones/iconos/mas.gif'   width="12" height="12"  ></a> 
                                            <a onClick="eliminar('<%=t%>','ELIMINAR_TRAMO');"  value="1" id="borrarI<%=x%>" style="cursor:hand" >
                                            <img id="imgini" name="imgini" src='<%=BASEURL%>/images/botones/iconos/menos1.gif'   width="12" height="12"  > </a>
                                        </td>
                                        </tr>
                                    </table>
                                </td>        
                                <td size_recurso="<%= (tramo.getListaRecursos()==null?0:tramo.getListaRecursos().size()) %>">
                                    <input name="origen<%=t%>" value='<%=tramo.getCod_origen_tramo()%>' type="text" class="textbox" size="6" maxlength="2" style="text-transform:uppercase"  onBlur="BuscarCiudadItem('<%=BASEURL%>',this.value,'origen<%=t%>')">
                                    <img src="<%= BASEURL %>/images/botones/iconos/lupa.gif" width="15" height="16" title="Buscar" style="cursor:hand" onClick="BuscarCiudadItem('<%=BASEURL%>','LUPA','origen<%=t%>')">
                                    <input name="nomorigen<%=t%>" value='<%=tramo.getNombre_origen_tramo()%>' type="text" class="letra" style=" border:0"  size="32" readonly>
                                </td>
                                <td >
                                    <input name="destino<%=t%>" value='<%=tramo.getCodigo_destino_tramo()%>' type="text" class="textbox" size="6" maxlength="2" style="text-transform:uppercase"  onBlur="BuscarCiudadItem('<%=BASEURL%>',this.value,'destino<%=t%>')">
                                    <img src="<%= BASEURL %>/images/botones/iconos/lupa.gif" width="15" height="16" title="Buscar" style="cursor:hand" onClick="BuscarCiudadItem('<%=BASEURL%>','LUPA','destino<%=t%>')">
                                    <input name="nomdestino<%=t%>" value='<%=tramo.getNombre_destino_tramo()%>' type="text" class="letra" style=" border:0" size="32" readonly>
                                </td>
                                <td align="center">
                                    <input name="porcentaje_anticipo<%=t%>" value='<%=tramo.getPorcentaje_tramo()%>' type="text" size="10" maxlength="3" onkeypress="soloDigitos(event,'decNO');">
                                </td>
                                <td align="center">
                                    <input name="peso_lleno_max<%=t%>" value='<%=tramo.getPeso_lleno_tramo()%>' type="text" size="10" maxlength="6" onkeypress="soloDigitos(event,'decNO');" >
                                </td>
                                <td align="center">
                                    <input name="numero_escolta<%=t%>"  value='<%=(tramo.getNumero_escoltas()!=0)?tramo.getNumero_escoltas():""%>'type="text" class="textbox" size="6" maxlength="3"  onKeyPress="soloDigitos(event, 'decNO')"  >
                                </td>
                                <td align="center">
                                    <select name="tipo_trafico<%=t%>" style="width:100% ">
                                    <option value="URB" <%=(tramo.getTipo_trafico().equals("URB"))?"selected":""%>>URBANO</option>
                                    <option value="MAS" <%=(tramo.getTipo_trafico().equals("MAS"))?"selected":""%>>MASIVO</option>
                                    </select>
                                </td>
                                </tr>
                                <!-- fin de celda de los tramos -->
        
        
					
                                <!-- celda de los parametros 1 -->								
                                <tr class='fila'>
                                <td></td>
                                <td colspan='6'>
                                    <span style="width:24%">
                                        <input type="checkbox" name="viaje_vacio<%=t%>" <%=(tramo.isInd_vacio_tramo())?"checked":""%> value="S" >
                                        Viaje vacio
                                    </span>
                                    <span style="width:24%">
                                        <input type="checkbox" name="control_cargue<%=t%>" value="S" <%=(tramo.isControl_cargue())?"checked":""%>  >
                                        Control cargue
                                    </span>
                                    <span style="width:24%">
                                        <input type="checkbox" name="control_descargue<%=t%>" value="S" <%=(tramo.isControl_descargue())?"checked":""%> >
                                        Control descargue
                                    </span>
                                    <span style="width:24%">
                                        <input type="checkbox" name="caravana<%=t%>" value="S" <%=(tramo.isCaravana())?"checked":""%>  >
                                        Caravana
                                    </span>
                                </td>
                                </tr>
                                <!-- fin de la celda de los parametros 1 -->				
        
        
        
        
                                <!-- celda de los parametros 2 -->				
                                <tr class='fila'>
                                    <td></td>
                                    <td colspan='6'>
                                        <span style="width:24%">
                                            <input type="checkbox" name="require_planviaje<%=t%>" value="S"  <%=(tramo.isRequiere_planviaje())?"checked":""%>>
                                            Requiere plan viaje
                                        </span>						
                                        <span style="width:24%">
                                            <input type="checkbox" name="preferencia<%=t%>" value="S" <%=(tramo.isPreferencia())?"checked":""%>  >
                                            Preferencia
                                        </span>
                                        <span style="width:24%">
                                        <input type="checkbox" name="requiere_hojareporte<%=t%>" value="S"  <%=(tramo.isRequiere_hojareporte())?"checked":""%>>
                                        Requiere hoja de reporte						</span>
                                        <span style="width:24%">
                                            <input type="checkbox" name="contingente<%=t%>" value="S"  <%=(tramo.isContingente())?"checked":""%>>
                                            Contingente
                                        </span>
                                    </td>
                                </tr>				
                                <!-- fin de la celda de los parametros 2-->
					
					
		
                                <!-- celda de los recursos -->
                                <tr class='fila'>
                                <td></td>
                                <td colspan='6'>		
			
                                    <!-- recursos -->
                                    <table width="100%" id="detalleRecurso" border="0" cellspacing="1" cellpadding="0">
                                    
                                    <%
                                           List listaRecurso = tramo.getListaRecursos();
                             
                                          for(int r=0; r<listaRecurso.size();r++ ){
                                             Standar recurso = (Standar) listaRecurso.get(r);
                                         %>
                                        <tr class="tblTitulo3">
                                            <td width="65" class="bordereporte"><div align="center">Recurso</div></td>
                                            <td width="79" class="bordereporte"><div align="center">Codigo </div></td>
                                            <td width="38" class="bordereporte"><div align="center">Tipo </div></td>
                                            <td width="200" class="bordereporte"><div align="center">Descripcion</div></td>
                                            <td width="47" class="bordereporte"><div align="center">Prioridad</div></td>
                                            <td colspan="5" class="bordereporte"><div align="center">Flete</div></td>
                                        </tr>

                                        <!-- encabezado del recurso -->
                                        

                                        <tr class="filaazul" >
                                            <td  class="bordereporte">
                                            <table width="65" border="0" cellspacing="0" cellpadding="0" height="30%">
                                            <tr class="filaazul" >
                                            <td width="35%" nowrap class="letraresaltada"><a  ><%=r+1%></a>
                                                <input name="cod_item_recurso<%=t+""+r%>" id="cod_item_recurso<%=t+""+r%>" value="<%=t+""+r%>" type="hidden" size="4" maxlength="5" border="0">
                                            </td>
                                            <td width="65%" nowrap><a onClick="agregar('<%=t+"-"+r%>','AGREGAR_RECURSO');"  id="insert_item<%=r%>" style="cursor:hand" >
                                                <img src='<%=BASEURL%>/images/botones/iconos/mas.gif'   width="12" height="12"  ></a> 
                                                <a onClick="eliminar('<%=t+"-"+r%>','ELIMINAR_RECURSO');"  value="1" id="borrarI<%=r%>" style="cursor:hand" >
                                                <img id="imgini" name="imgini" src='<%=BASEURL%>/images/botones/iconos/menos1.gif'   width="12" height="12"  > </a>
                                            </td>
                                           </tr>
                                            </table>
                                        </td>
                                        <td  class="bordereporte" fletes="<%= (recurso.getListaFletes()==null?0:recurso.getListaFletes().size()) %>" ><input name="cod_recurso<%=t+""+r%>" value='<%=recurso.getCodigo_recurso()%>' type="text" class="textbox" size="6" maxlength="10" style="text-transform:uppercase"  onBlur="BuscarRecurso('<%=BASEURL%>',this.value,'<%=t+"-"+r%>','<%=listaRecurso.size()%>')"><img src="<%= BASEURL %>/images/botones/iconos/lupa.gif" width="15" height="16" title="Buscar" style="cursor:hand" onClick="BuscarRecurso('<%=BASEURL%>','LUPA','<%=t+"-"+r%>','<%=listaRecurso.size()%>')"></td>
                                        <td align="center"><input name="tipo_recurso<%=t+""+r%>" value='<%=recurso.getTipo_recurso()%>' type="text" class="textbox" size="1" readonly maxlength="1" style="text-transform:uppercase; width:20;" ></td>
                                        <td align="center"><input name="desc_recurso<%=t+""+r%>" value='<%=recurso.getDescripcion_recurso()%>' type="text"  size="35" readonly></td>
                                        <td align="center"><input name="prioridad<%=t+""+r%>" value='<%=(recurso.getPrioridad()==0)?"":recurso.getPrioridad()%>' type="text" class="textbox" size="1"  onkeypress="soloDigitos(event,'decNO');" maxlength="2" style="width:20;" ></td>
                                        <td width="83" align="center" class="tblTitulo" style="border: 1PX double #E6E6E6; ">�Item</td>
                                        <td width="127" class="tblTitulo" style="border: 1PX double #E6E6E6; "><div align="center">Valor</div></td>
                                        <td width="93" class="tblTitulo" style="border: 1PX double #E6E6E6; "><div align="center">Moneda</div></td>
                                        <td width="63" class="tblTitulo" style="border: 1PX double #E6E6E6; "><div align="center">Unidad</div></td>
                                        <td width="54" class="tblTitulo" style="border: 1PX double #E6E6E6; "><div align="center">Vig.</div></td>
                                        </tr>
                                        <!-- fin encabezado del recurso -->

                                        <%
                                           List listaFletes = recurso.getListaFletes();
                             
                                          for(int f=0; f<listaFletes.size();f++ ){
                                             Standar fletes = (Standar) listaFletes.get(f);
                                        %>
                                        <tr>
                                            <td>&nbsp;</td>
                                            <td>&nbsp;</td>
                                            <td>&nbsp;</td>
                                            <td>&nbsp;</td>
                                            <td>&nbsp;</td>
                                            <td colspan="5">
                                            
                                            
                                            <table   width="100%" border="0" cellspacing="0" cellpadding="0">
                                            <tr class="filaazul"  >
                                                <td width="18%" class="bordereporte">
                                                <table width="85" border="0" cellspacing="0" cellpadding="0" height="30%">
                                                <tr>
                                                <td width="35%" nowrap class="letraresaltada"><a  ><%=f+1%></a>
                                                    <input name="cod_item_flete<%=f%>" id="cod_item_flete<%=f%>" value="<%=f%>" type="hidden" size="4" maxlength="5" border="0">
                                                </td>
                                                <td width="65%" nowrap><a onClick="agregar('<%=t+"-"+r+"-"+f%>','AGREGAR_FLETE');"  id="insert_item<%=f%>" style="cursor:hand" ><img src='<%=BASEURL%>/images/botones/iconos/mas.gif'   width="12" height="12"  ></a> <a onClick="eliminar('<%=t+"-"+r+"-"+f%>','ELIMINAR_FLETE');"  value="1" id="borrarI<%=f%>" style="cursor:hand" ><img id="imgini" name="imgini" src='<%=BASEURL%>/images/botones/iconos/menos1.gif'   width="12" height="12"  > </a></td>
                                            </tr>
                                                </table>
                                            </td>
                                            <td width="30%" align="center"><input type="text" name="vlr_flete<%=t+""+r+""+f%>" maxlength="10" onfocus='this.select()' value='<%=UtilFinanzas.customFormat(fletes.getVlr_flete())%>' style="text-align:right;" onKeyPress="Digitos(event, 'decNO')" onblur='formatear(this)'></td>
                                            <td width="23%" class="bordereporte" align="center">
                                                <select name="flete_moneda<%=t+""+r+""+f%>">
                                                    <% for(int mm=0; mm<monedas.size();mm++){
                                                       Moneda mon = (Moneda) monedas.get(mm);%>
                                                      <option value="<%=mon.getCodMoneda()%>" <%=(fletes.getMoneda_flete().equals(mon.getCodMoneda()))?"selected":""%>><%=mon.getNomMoneda()%></option>
                                                   <%}%>
                                                </select>
                                            </td>
                                            <td width="16%" class="bordereporte" align="center">
                                                <select name="flete_unidad<%=t+""+r+""+f%>" >
                                                    <% for(int w=0; w<ListaUnidades.size();w++){
                                                            TablaGen tuw = (TablaGen) ListaUnidades.get(w);%>
                                                         <option  value="<%=tuw.getTable_code()%>" <%=(fletes.getUnidad_flete().equals(tuw.getTable_code()))?"selected":""%> ><%=tuw.getTable_code()%></option>
                                                    <%}%>
                                                </select>
                                            </td>
                                            <td width="13%" class="bordereporte" align="center" >
                                                <input type="checkbox" name="flete_vigente<%=t+""+r+""+f%>" value="S"  <%=(fletes.isVigente_flete())?"checked":""%> >
                                            </td>
                                        </tr>
                                            </table>
                                        </td>
                                        </tr>
                                         
                                        <%}//fin del for de los fletes
                                      }//fin del for de los recursos
                                     %>
                                        
                                    </table>
                                    <!-- fin recursos -->


                                </td>
                                </tr>
                                <tr class='fila'><td colspan='7'>&nbsp;</td></tr>
                                <!-- fin celda de los recursos -->
                             
                             <%}%>   
 
                            </table>
                            <!-- fin detalle tramo -->
					
				
                        </td>
                        </tr>
                    </table>    
                    <!-- fin marco de tramos -->


                </td></tr>
                <tr>
                <td>
                <center>
                      <img src='<%=BASEURL%>/images/botones/modificar.gif' name='Buscar' align="absmiddle" style='cursor:hand'   onclick="validarFrm();" onMouseOver='botonOver(this);' onMouseOut='botonOut(this);'>
               &nbsp;<img src='<%=BASEURL%>/images/botones/salir.gif' name='Buscar' align="absmiddle" style='cursor:hand'   onClick="window.close();" onMouseOver='botonOver(this);' onMouseOut='botonOut(this);'>
		</center>
		</td>
		</tr>
               </table>
                <!-- fin marco principal -->

            </div>
           
            </form>
        </div>
    </body>
    <iframe width=188 height=166 name="gToday:normal:agenda.js:gfPop:plugins.js" id="gToday:normal:agenda.js:gfPop:plugins.js" src="<%=BASEURL%>/js/Calendario/ipopeng.htm" scrolling="no" frameborder="0" style="visibility:visible; z-index:999; position:absolute; left:-500px; top:0px;"> </iframe>
<%=datos[1]%>
    <script>
        function enviar(url){
        var a = "<iframe name='ejecutor' style='visibility:hidden'  src='<%= CONTROLLER %>" + url + "'> ";
        aa.innerHTML = a;
        }
        function enviar2(url){
        var a = "<iframe name='ejecutor' style='visibility:hidden'  src='" + url + "'> ";
        aa.innerHTML = a;
        }

    </script>
    <font id='aa'></font>
</html>


  
