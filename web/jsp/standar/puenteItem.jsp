<%@page session="true"%> 
<%@ page import    ="java.util.*" %>
<%@page import="com.tsp.util.*"%>
<%@page import="com.tsp.finanzas.contab.model.*"%>
<%@page import="com.tsp.operation.model.beans.*"%>
<%@page errorPage="/error/ErrorPage.jsp"%>
<%@include file="/WEB-INF/InitModel.jsp"%>

<% 
   
   String opcion  = request.getParameter("opcion");
   String mensaje = (request.getParameter("mensaje")!=null)?request.getParameter("mensaje"):"";
   String id           = request.getParameter("id");
   String size_recurso = request.getParameter("size");  
  
 %>
<script>
  
   var html ="";
    <%if(opcion.equals("CIUDAD")){
 	       if(model.StandarSvc.getCiudad()!= null){%>
  	           
					   var codigo = parent.forma1.<%=id%>;
					   var nombre  = parent.forma1.nom<%=id%>;
					   codigo.value   = '<%=model.StandarSvc.getCiudad().getCodCiu()%>';
					   nombre.value   = '<%=model.StandarSvc.getCiudad().getNomCiu()%>';
			   
			  
	      <%}else{%>
		        alert('El codigo de ciudad digitado no existe');
		        var codigo = parent.forma1.<%=id%>;
				var nombre  = parent.forma1.nom<%=id%>;
				codigo.focus()
				nombre.value="";
		  <%}%>
      <%}else if(opcion.equals("RECURSO")){
              if(model.StandarSvc.getRecurso()!= null){
                  %>
                   var sw =0; 
                  <%
                  String vec[] = id.split("-");
                  int t = Integer.parseInt(vec[0]);
                  int r = Integer.parseInt(size_recurso);
                  for(int i=0; i<r;i++){
                %>
                   var codigo = parent.forma1.cod_recurso<%=id.replaceAll("-","")%>;
                   var a = parent.forma1.cod_recurso<%=t+""+i%>;
                   <%if(!(t+""+i).equals(id.replaceAll("-",""))){%>
                       if((codigo.value).toUpperCase() == (a.value).toUpperCase()){
                           alert('el codigo de recurso ya existe para este tramo');
                           sw =1;

                       }
                  <%}
                   }%>
                   var codigo = parent.forma1.cod_recurso<%=id.replaceAll("-","")%>;
                   var tipo  = parent.forma1.tipo_recurso<%=id.replaceAll("-","")%>;
                   var desc  = parent.forma1.desc_recurso<%=id.replaceAll("-","")%>;
                       
                   if(sw == 0){
                       codigo.value  = '<%=model.StandarSvc.getRecurso().getRecurso()%>';
                       tipo.value    = '<%=model.StandarSvc.getRecurso().getTipo()%>';
                       desc.value    = '<%=model.StandarSvc.getRecurso().getDestino()%>';
                   }else{
                      codigo.focus()
                      tipo.value='';
                      desc.value='';
                   }
              <%}else{%>
                  alert('El codigo de recurso digitado no existe..');
                  var codigo = parent.forma1.cod_recurso<%=id.replaceAll("-","")%>;
                  var tipo  = parent.forma1.tipo_recurso<%=id.replaceAll("-","")%>;
                  var desc  = parent.forma1.desc_recurso<%=id.replaceAll("-","")%>;
                  
                  codigo.focus()
                  tipo.value='';
                  desc.value='';
              <%}%>
          
      <%}%>
	 
   <% if (!mensaje.equals("")){%> 
	   alert('<%=mensaje%>');
   <%}%>
</script>

        
                   