<!--
- Autor : Ing. David Lamadrid
- Date  : 5 de Octubre de 2005
- Copyrigth Notice : Fintravalores S.A. S.A
-->
<%--
-@(#)
--Descripcion : Pagina JSP, que se encarga de realizar y mostrar busqueda de los nits de proveedores
--%>

<%-- Declaracion de librerias--%>
<%@page contentType="text/html"%>
<%@page session="true"%>
<%@page import="java.util.*"%>
<%@page import="com.tsp.operation.model.*"%>
<%@page import="com.tsp.util.*"%>
<%@page import="com.tsp.operation.model.beans.*, java.util.*"%>
<%@include file="/WEB-INF/InitModel.jsp"%>
<%@taglib uri="http://jakarta.apache.org/taglibs/input-0.90" prefix="input" %>
<html>
<head>
<title>Buscar recurso</title>
<link href="../css/estilostsp.css" rel="stylesheet" type="text/css"> 
<link href="<%=BASEURL%>/css/estilostsp.css" rel="stylesheet" type="text/css"> 
<script type='text/javascript' src="<%=BASEURL%>/js/Validacion.js"></script> 
<script type='text/javascript' src="<%= BASEURL %>/js/boton.js"></script>
</head>
<script>
	/*Ivan Dario 28 Octubre 2006*/
    function asignarCodigoP(codigo,tipo,desc,id,size){
               var vec = id.split("-"); 
               var t = vec[0];
               var r = vec[1];
               var sw ='0';
               for(var i=0; i< size;i++){
                   var a    = parent.opener.document.getElementById("cod_recurso"+t+""+i);
                   if(id.replace("-","") != (t+""+i)){
                       if(codigo == (a.value).toUpperCase()){
                           alert('el codigo de recurso ya existe para este tramo');
                           sw =1;

                       }
                   }    
               }
               var cod            = parent.opener.document.getElementById("cod_recurso"+id.replace("-",""));
               var tipo_recurso   = parent.opener.document.getElementById("tipo_recurso"+id.replace("-",""));
               var des            = parent.opener.document.getElementById("desc_recurso"+id.replace("-",""));
                   
               if(sw =='0'){
                   cod.value          = codigo;
                   tipo_recurso.value = tipo; 
                   des.value          = desc;
                   parent.close();    
	       }
               
               
		
	}
	
	function procesarProveedores (element,subtotal,CONTROLLER, validar){
		if (window.event.keyCode==13)
		listaP(subtotal, validar);
	}
	
	/*function buscar (subtotal,CONTROLLER, estado,maxfila){
		if (window.event.keyCode==13)
		listaP(subtotal,CONTROLLER,estado, maxfila);
	}*/
	function listaP(CONTROLLER,id,size){
		document.forma1.action = CONTROLLER+"?estado=Standar&accion=Grabacion&opcion=BUSCAR_RECURSO_LUPA&id="+id+"&size="+size;
		document.forma1.submit();
	}
</script>
<body onLoad="forma1.codrecurso.focus();" onresize="redimensionar()">
<div id="capaSuperior" style="position:absolute; width:100%; height:100px; z-index:0; left: 0px; top: 0px;">
 <jsp:include page="/toptsp.jsp?encabezado=Buscar Recurso"/>
</div>
<div id="capaCentral" style="position:absolute; width:100%; height:87%; z-index:0; left: 0px; top: 100px; overflow: scroll;">
<%
	 String accion = (request.getParameter("accion")==null?"":request.getParameter("accion"));
	 String id = (request.getParameter("id")==null?"":request.getParameter("id"));
         String size = (request.getParameter("id")==null?"":request.getParameter("size"));
%>
<form name="forma1" action="<%=CONTROLLER%>?estado=Standar&accion=Grabacion&opcion=BUSCAR_RECURSO_LUPA&id=<%=id%>&size=<%=size%>" method="post" >
  <table border="2" align="center" width="566">
    <tr>
    <td width="610" >
        <table width="100%" align="center"  >
          <tr>
            <td width="295" height="24"  class="subtitulo1"><p align="left">Buscar recurso </p></td>
            <td width="265"  class="barratitulo"><img src="<%=BASEURL%>/images/titulo.gif"></td>
          </tr>
        </table>        
        <table width="100%" align="center" >
          <tr class="fila">
            <td width="107" >Descripcion </td>
            <td width="424" > 
              <input name="codrecurso" type="text" class="textbox" id="nomciu" size="18" maxlength="15"   >
      
      &nbsp;

<img src='<%=BASEURL%>/images/botones/buscar.gif' name='Buscar' align="absmiddle" style='cursor:hand' title='Aceptar...'  onclick="listaP('<%=CONTROLLER%>','<%=id%>','<%=size%>');" onMouseOver='botonOver(this);' onMouseOut='botonOut(this);'></td>
          </tr>
        </table></td>
</tr>
</table>




<%
    if (accion.equals("1")){
        List recursos =  model.StandarSvc.getListaRecursos();
%>
  <table width="566" border="2" align="center">
    <tr>
    <td width="585">
<table width="555"  >
          <tr>
    <td width="346"  class="subtitulo1"><p align="left">Ciudades</p></td>
    <td width="193"  class="barratitulo"><img src="<%=BASEURL%>/images/titulo.gif"></td>
  </tr>
</table>
        <table width="556" >
          <tr class="tblTitulo" id="titulos">
    <td width="56" height="22">codigo</td>
    <td width="334" >nombre</td>
    </tr>
<%
        for(int i=0; i< recursos.size();i++){
            Recursosdisp recur =(Recursosdisp)recursos.get(i);
%>  
  <tr class="fila">
    <td width="56" height="22"><%=recur.getRecurso()%></td>
    <td width="334" ><a  onClick="asignarCodigoP('<%=recur.getRecurso()%>','<%=recur.getTipo()%>','<%=recur.getDestino()%>','<%=id%>','<%=size%>')" style="cursor:hand" ><%=recur.getDestino()%></a></td>
    </tr>
<%
        }
%>  
</table>
</td>
</tr>
</table>
<%
    }
%>
</form>
<div align="center"><img src="<%=BASEURL%>/images/botones/salir.gif"  name="salir" title="Salir..." onClick="parent.close();" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);">
</div>
</div>
</body>
</html>
    
