<!--
- Copyrigth Notice : Fintravalores S.A
-->
<%--
-@(#)
--Descripcion : Pagina JSP, que maneja la generación del reporte
				de los negocios endozados a Corficolombiana.
--%>
<%@page contentType="text/html"%>
<%@page session="true"%>
<%@page errorPage="/error/ErrorPage.jsp"%>
<%@page import="java.util.*"%>
<%@page import="com.tsp.operation.model.*"%>
<%@page import="com.tsp.operation.model.beans.*"%>
<%@page import="com.tsp.util.*"%>
<%@include file="/WEB-INF/InitModel.jsp"%>
<%@taglib uri="http://jsptags.com/tags/navigation/pager" prefix="pg" %>
<html>
	<head>
		<title>Carga a Corficolombiana</title> 
 		<link href="<%= BASEURL %>/css/estilostsp.css" rel='stylesheet'>
		<link href="/css/estilostsp.css" rel="stylesheet" type="text/css">
		<script type='text/javascript' src="<%= BASEURL %>/js/boton.js"></script>
    </head>
	<body>
	<div id="capaSuperior" style="position:absolute; width:100%; height:100px; z-index:0; left: 0px; top: 0px;">
 	<jsp:include page="/toptsp.jsp?encabezado=Cargar a Corficolombiana"/>
	</div>
	<div id="capaCentral" style="position:absolute; width:100%; height:87%; z-index:0; left: 0px; top: 100px; overflow: scroll;">
		<table width="367" border="2" align="center">
			  <tr>
				<td>
				  <table width="100%"  border="1" align="center"  bordercolor="#F7F5F4" bgcolor="#FFFFFF">
					<tr>
					  <td align="center" class="mensajes">Proceso Exitoso.</td>
					  <td width="29"><img src="<%=BASEURL%>/images/cuadronaranja.JPG"></td>
					</tr>
				  </table>
				</td>
			  </tr>
		</table>
		<p>&nbsp;</p>
		<table width="367" align="center">
		  <tr>
			<td>
			  <img src="<%=BASEURL%>/images/botones/salir.gif" name="c_regresar" id="c_regresar" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" onClick="window.close();"> </td>
		  </tr>
		</table>
	</div>
	</body>
</html>
