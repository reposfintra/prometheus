<%@ page session="true"%>
<%@page import="com.tsp.operation.model.beans.*"%>
<%@page import="java.util.*"%>
<%@ include file="/WEB-INF/InitModel.jsp"%>
<%@ taglib uri="http://jakarta.apache.org/taglibs/input-0.90" prefix="input" %>

<html>
<head>
    <title>Generación de Archivos de Corficolombiana</title>
    <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
    <link href="/css/estilostsp.css" rel="stylesheet" type="text/css">
	<link href="<%=BASEURL%>/css/estilostsp.css" rel="stylesheet" type="text/css">
    <script language="JavaScript" type="text/javascript" src="<%=BASEURL%>/js/boton.js"></script>
</head>
<body>
<div id="capaSuperior" style="position:absolute; width:100%; height:100px; z-index:0; left: 1px; top: 0px;">
 <jsp:include page="/toptsp.jsp?encabezado=Generación de Archivos de Corficolombiana"/>
</div>
<div id="capaCentral" style="position:absolute; width:100%; height:87%; z-index:0; left: 0px; top: 100px; overflow: scroll;">

<form name="form2" method="post" action="controller?estado=Archivos&accion=Corficolombiana">

  <center>
    <table width="500" border="2" align="center">
      <tr>
        <td>
          <table width='100%' align='center' class='tablaInferior'>
            <tr>
              <td class="barratitulo" colspan='2' >
                <table cellpadding='0' cellspacing='0' width='100%'>
                  <tr class="fila">
                    <td width="50%" align="left" class="subtitulo1">&nbsp;Generaci&oacute;n de Archivo </td>
                    <td width="*"   align="left"><img src="<%= BASEURL %>/images/titulo.gif" width="32" height="20"  align="left"></td>
                  </tr>
				  <tr class="fila">
				  	<td>&nbsp;</td>
					<td></td>
				</tr>
				  <tr class="fila">
                    <td width="50%" align="left" >&nbsp;Tipo de Archivo </td>
                    <td width="*"   align="left">
						<select name="tipoarchivo">
							<option value="pri">Archivo principal</option>
							<option value="sec">Archivo secundario</option>
						</select>
					</td>
                  </tr>				  
              </table></td>
            </tr>
            
        </table></td>
      </tr>
    </table>
	<br>	<br>	
	<table border="1">
		<tr class="fila">                 
                    <td colspan="2" align="center">Fecha de Aprobacion </td>
                    <td width="15%" align="center">&nbsp;Fecha Inicial</td>
                    <td width="15%" align="center">
					
				  		<input name="fecini2" type="text" class="textbox" id="fecini2" size="10" readonly>
              			<span class="Letras"><img src="<%=BASEURL%>/images/cal.gif" width="16" height="16" align="absmiddle" style="cursor:hand " onclick="if(self.gfPop)gfPop.fPopCalendar(document.form2.fecini2);return false;" HIDEFOCUS></span>
						&nbsp;
					</td>
                    <td width="15%" align="left">&nbsp;Fecha Final</td>
                    <td width="15%" align="left">
					
              			<input name="fecfin2" type="text" class="textbox" id="fecfin2" size="10" readonly>
              			<span class="Letras"><a href="javascript:void(0)" onclick="jscript: show_calendar('fecfin2');" HIDEFOCUS> </a> <img src="<%=BASEURL%>/images/cal.gif" width="16" height="16" align="absmiddle" style="cursor:hand " onclick="if(self.gfPop)gfPop.fPopCalendar(document.form2.fecfin2);return false;" HIDEFOCUS></span>
						&nbsp;
					</td>
				</tr>
	</table>
    <br>
	<table align="center">
		<tr>
        	<td colspan="2" nowrap align="center">
		  		<span class="letraresaltada"></span>&nbsp;&nbsp;&nbsp;&nbsp;<img src="<%=BASEURL%>/images/botones/aceptar.gif" name="aceptar"  height="21" onClick="this.disabled=true;form2.submit()" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" style="cursor:hand">&nbsp;
				&nbsp;				
				<img src="<%=BASEURL%>/images/botones/salir.gif" name="mod"  height="21" onclick="window.close();" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" style="cursor:hand">
	        </td>
    	</tr>
	</table>
	<br>	<br>
	<table align="center">
		<tr>
        	<td colspan="2" nowrap align="center">
		  		
	        </td>
    	</tr>
	</table>
		  
    <br>
    <br>
    <table width="40%"  align="center">
      <tr>
        <td>
          <FIELDSET>
          <legend><span class="letraresaltada">Nota</span></legend>
          <table width="100%"  border="0" cellpadding="0" cellspacing="0" class="informacion">
            <tr>
              <td nowrap align="center">&nbsp; Para iniciar el proceso de generación haga click en aceptar. </td>
            </tr>
          </table>
        </FIELDSET></td>
      </tr>
    </table>
  </center>
<br>
<%if(request.getParameter("msg")!=null){%>
<table border="2" align="center">
  <tr>
    <td><table width="100%" border="1" align="center"  bordercolor="#F7F5F4" bgcolor="#FFFFFF">
        <tr>
          <td width="262" align="center" class="mensajes"><%=request.getParameter("msg")%> !</td>
          <td width="32" background="<%=BASEURL%>/images/cuadronaranja.JPG">&nbsp;</td>
          <td width="44">&nbsp;</td>
        </tr>
    </table></td>
  </tr>
</table>
<%}%>

</form>
</div>
<iframe width=188 height=166 name="gToday:normal:agenda.js:gfPop:plugins.js" id="gToday:normal:agenda.js:gfPop:plugins.js" src="<%=BASEURL%>/js/Calendario/ipopeng.htm" scrolling="no" frameborder="0" style="visibility:visible; z-index:999; position:absolute; left:-500px; top:0px;"> </iframe>    
</body>
</html>