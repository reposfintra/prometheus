<%-- 
    Document   : maestroTipoDescuentos
    Created on : 19/06/2015, 05:37:02 PM
    Author     : mariana
--%>

<%@page contentType="text/html" pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
        <link href="./css/popup.css" rel="stylesheet" type="text/css">
        <link type="text/css" rel="stylesheet" href="./css/jquery/jquery-ui/jquery-ui.css" />
        <link type="text/css" rel="stylesheet" href="./css/TransportadorasApi.css " />
        <script type="text/javascript" src="./js/jquery/jquery.jqGrid/js/jquery.min.js"></script>
        <script type="text/javascript" src="./js/jquery-ui-1.8.5.custom.min.js"></script>   
        <script type="text/javascript" src="./js/jquery/jquery.jqGrid-4.7.0/js/grid.locale-es.js"></script>
        <script type="text/javascript" src="./js/jquery/jquery.jqGrid-4.7.0/js/jquery.jqGrid.min.js"></script>
        <script type="text/javascript" src="./js/jquery/jquery.jqGrid-4.7.0/js/jquery.tabletojson.js"></script>    
        <script type="text/javascript" src="./js/transportadorasLog.js"></script>    


        <title>MAESTRO TIPO DESCUENTOS</title>
    </head>
    <body>
        <div id="capaSuperior" style="position:absolute; width:100%; height:100px; z-index:0; left: 0px; top: 0px;">
            <jsp:include page="/WEB-INF/toptsp.jsp?encabezado=MAESTRO TIPO DESCUENTOS "/>
        </div>
     <center>

        <div style="position: relative;top: 150px;">
            <table id="tabla_tipo_decuento" ></table>
            <div id="pager"></div>

        </div>
        <%-- --------------------------Registro Tipo Descuentos-------------------------width: 1000px;---%>
        <div id="dialogMsjtipodesc"  class="ventana" >
            <div id="tablita" style="top: 0px;width: 350px;" >
                <div id="encabezadotablita">
                    <label class="titulotablita"><b>REGISTRO TIPO DESCUENTOS</b></label>
                </div> 
                <table id="tablainterna"  >
                    <tr>
                        <td>
                            <input type="text" id="idtipdes"  style="width: 50px" hidden >
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2">
                            <label>Nombre Tipo Descuento</label>
                        </td>
                        <td colspan="2">
                            <input type="text" id="nomtipdes" onchange="conMayusculas(this)" style="width: 200px"   >
                        </td>
                    </tr>
                   
                </table>
                <center>
                    <p id="dialogMsjSMS"  ></p>  
                    <p id="sms2"></p> 
                </center>

            </div>
        </div>
        <div id="dialogTransEditarProp"  class="ventana" >
            <div id="tablita" style="top: 0px;width: 300px;" >
                <div id="encabezadotablita">
                    <label class="titulotablita"><b>EDITAR TIPO DESCUENTO </b></label>
                </div> 
                <table id="tablainterna"  >
                    <tr>
                        <td>
                            <input type="text" id="idpro"  style="width: 50px" hidden >
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <label>Nombre del Tipo de De Producto</label>
                        </td>
                        <td>
                            <input type="text" id="nombretipdesc" onchange="conMayusculas(this)"  style="width: 250px">
                        </td>
                    </tr>
                </table>

            </div>
        </div>

        <div id="info"  class="ventana" >
            <p id="notific" >EXITO AL GUARDAR</p>
        </div>
    </center>

</body>
<script>
    inicio4();
</script>
</html>
