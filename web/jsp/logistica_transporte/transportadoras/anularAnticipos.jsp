<%-- 
    Document   : anularAnticipos.jsp
    Created on : 1/03/2016, 05:07:12 PM
    Author     : si-00261
--%>
<script type="text/javascript" src="./js/anticipos_transportadora.js"></script> 
<div id="capaCentral" align="left" style="position:absolute; width:100%; height:100%; z-index:0; top: 16px;" >
    <center>    
        <div id="tablita" style="width: 400px;height: 230px;" >
            <div id="encabezadotablita" style="width: 395px;">
                <label class="titulotablita"><b>FILTRO DE BUSQUEDA </b></label>
            </div> 
            <table id="tablainterna">
                <tr>
                    <td colspan="1">
                        <label>Transportadora</label>
                    </td>
                    <td colspan="3">
                        <select id="transportadora" style="width: 285px;height: 25px;color: #070708;">
                        </select>
                    </td>
                </tr>
                <tr>
                    <td>
                        <label>Fecha de Inicio</label>
                    </td>
                    <td>
                        <input type="datetime" id="fechainicio" name="fechainicio" style="height: 20px;width: 88px;color: #070708;">
                    </td>
                    <td>
                        <label>Fecha Final</label>
                    </td>
                    <td>
                        <input type="datetime" id="fechafinal" name="fechafinal" style="height: 20px;width: 88px;color: #070708;">
                    </td>
                </tr>
                <tr>
                    <td colspan="1">
                        <label>No Planilla</label>
                    </td>
                    <td colspan="1">
                        <input type="text" id="planilla" name="planilla" style="width: 90px;height: 23px;color: #070708;">                      
                    </td>
                    <td colspan="2">                     
                        <input type="checkbox" id="chk_reanticipo" name="chk_reanticipo"> Reanticipo                      
                    </td>
                </tr>
                  <tr>
                    <td colspan="1">
                        <label>No Placa</label>
                    </td>
                    <td colspan="3">
                        <input type="text" id="placa" name="placa" style="width: 90px;height: 23px;color: #070708;">                      
                    </td>
                </tr>
                <tr>
                    <td colspan="4">
                        <hr>
                    </td>
                </tr>
            </table>
            <div id ='botones' >
                <button id="buscaranticipos_anul" class="ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only" 
                        role="button" aria-disabled="false">
                    <span class="ui-button-text">Buscar</span>
                </button> 
                <button id="clearanticipos_anul" class="ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only" 
                        role="button" aria-disabled="false">
                    <span class="ui-button-text">Limpiar</span>
                </button> 
            </div>

        </div>             
        <br><br>
        <div id="grid_anticipos_anul">
            <table id="tabla_anticipos_anul"></table>                
            <div id="page_tabla_anticipos_anul"></div>
        </div>          
    </center>      
   
                            
</div>
