<%-- 
    Document   : cxc_transportadora
    Created on : 28/07/2015, 11:26:34 AM
    Author     : egonzalez
--%>

<%@page contentType="text/html" pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
        <title>GENERACION DE CORRIDAS</title>

        <link href="./css/reporte_produccion.css" rel="stylesheet" type="text/css" >
        <link type="text/css" rel="stylesheet" href="./css/jquery/jquery-ui/jquery-ui.css" />
        <link href="./css/popup.css" rel="stylesheet" type="text/css">
        <script type="text/javascript" src="./js/jquery/jquery.jqGrid/js/jquery.min.js"></script>
        <script type="text/javascript" src="./js/jquery-ui-1.8.5.custom.min.js"></script>
        <script type="text/javascript" src="./js/jquery/jquery.jqGrid-4.7.0/js/grid.locale-es.js"></script>
        <script type="text/javascript" src="./js/jquery/jquery.jqGrid-4.7.0/js/jquery.jqGrid.min.js"></script>
        <script type="text/javascript" src="./js/jquery/jquery.jqGrid-4.7.0/js/jquery.tabletojson.js"></script>
        <script type="text/javascript" src="./js/cxc_transportadora.js"></script>
    </head>
    <body>
        <div id="capaSuperior" style="position:absolute; width:100%; height:100px; z-index:0; left: 0px; top: 0px;">
            <jsp:include page="/WEB-INF/toptsp.jsp?encabezado=GENERACION DE CORRIDAS"/>
        </div>
        <div id="capaCentral" style="position:absolute; width:100%; height:100%; z-index:0; left: 0px; top: 100px; ">

            <div id="filtro_cxc">
                <table border="0" class="tablaBusqueda_cxc"  cellpadding="1">
                    <thead>
                    <th class="ui-state-default titulo" colspan="5">Filtro  Busqueda</th>
                    </thead>
                    <tbody>
                        <tr>
                            <td>Transportadora</td>
                            <td>
                                <select class="selected" style="width: 234px !important;" id="transportadora">
                                    <option ></option>
                                </select>
                            </td>
                            <td>Fecha Corte</td>
                            <td>
                                <input type="text" id="fecha_corrida" class="inpt" readonly>
                            </td> 
                            <td>
                                <button id="buscar" class="ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only" >
                                    <span class="ui-button-text">Buscar</span>
                                </button>
                            </td>
                        </tr>

                    </tbody>
                </table>           
            </div>  
            <br><br>
            <center>
                <div id="contenedor">   
                    <br>
                    <table id="tblcxc"></table>
                    <div id="pagecxc"></div>
                    
                    <hr>
                    <button id="generar" class="ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only" >
                        <span class="ui-button-text">Generar CxC</span>
                    </button>
                </div>
            </center>
        </div>
        <div id="dialogo" class="ventana" title="Mensaje">
            <p  id="msj"></p>
        </div>

        <div id="dialogo2" class="ventana">
            <p id="msj2"> </p> <br/>
            <center>
                <img src="./images/cargandoCM.gif"/>
            </center>
        </div>
        
        <div id="detalle" class="ventana" title="Detalle corrida cxc">
            
            <table id="tbldetallecxc"></table>
            <div id="pagedetallecxc"></div>
            
        </div>
    </body>
</html>
