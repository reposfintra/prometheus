<%-- 
    Document   : maestroAgenciasTrans
    Created on : 11/08/2015, 10:56:17 AM
    Author     : mariana
--%>

<%@page contentType="text/html" pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
        <link href="./css/popup.css" rel="stylesheet" type="text/css">
        <link type="text/css" rel="stylesheet" href="./css/jquery/jquery-ui/jquery-ui.css" />
        <link type="text/css" rel="stylesheet" href="./css/TransportadorasApi.css " />
        <script type="text/javascript" src="./js/jquery/jquery.jqGrid/js/jquery.min.js"></script>
        <script type="text/javascript" src="./js/jquery-ui-1.8.5.custom.min.js"></script>   
        <script type="text/javascript" src="./js/jquery/jquery.jqGrid-4.7.0/js/grid.locale-es.js"></script>
        <script type="text/javascript" src="./js/jquery/jquery.jqGrid-4.7.0/js/jquery.jqGrid.min.js"></script>
        <script type="text/javascript" src="./js/jquery/jquery.jqGrid-4.7.0/js/jquery.tabletojson.js"></script>    
        <script type="text/javascript" src="./js/AgenciasTransportadoras.js"></script>    


        <title>MAESTRO PRODUCTOS TRANSPORTADORAS</title>
    </head>
    <body>
        <div id="capaSuperior" style="position:absolute; width:100%; height:100px; z-index:0; left: 0px; top: 0px;">
            <jsp:include page="/WEB-INF/toptsp.jsp?encabezado=AGENCIAS TRANSPORTADORAS "/>
        </div>
    <center>

        <div style="position: relative;top: 150px;">
            <table id="tabla_agencias_trans" ></table>
            <div id="pager"></div>

        </div>
        <%-- --------------------------Registro Productos----------------------------%>
        <div id="dialogMsjagencias"  class="ventana" >
            <div id="tablita" style="top: 0px;width: 393px;" >
                <!--                <div id="encabezadotablita">
                                    <label class="titulotablita"><b>REGISTRO AGENCIAS TRANSPORTADORAS </b></label>
                                </div> -->
                <table id="tablainterna"  >
                    <tr>
                        <td>
                            <input type="text" id="idedit" hidden>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <label>Nombre de Agencia<span style="color:red;">*</span></label>
                        </td>
                        <td>
                            <input type="text" id="nombreagencia" onchange="conMayusculas(this)" style="width: 200px"   >
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <label>Transportadoras<span style="color:red;">*</span></label>
                        </td>
                        <td>
                            <select id="transportadoras" onchange="conMayusculas(this)" style="width: 206px" >
                            </select>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <label>Correo<span style="color:red;">*</span></label>
                        </td>
                        <td>
                            <input type="text" id="correo" onchange="conMayusculas(this)" placeholder="Formato: algo@dir.dom" style="width: 200px"   >
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <label>Direccion<span style="color:red;">*</span></label>
                        </td>
                        <td>
                            <input type="text" id="direccion" onchange="conMayusculas(this)" style="width: 200px"   >
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <label>Pais<span style="color:red;">*</span></label> 
                        </td>
                        <td>
                            <select id="pais" style="width: 144px" onchange="cargarDepartamento(this.id)">
                            </select>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <label>Departamento<span style="color:red;">*</span></label> 
                        </td>
                        <td>
                            <select id="departamento" style="width: 144px" onchange="cargarCiudad(this.id)">
                            </select>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <label>Ciudad<span style="color:red;">*</span></label> 
                        </td>

                        <td>
                            <select id="ciudad" style="width: 144px" >
                            </select>
                        </td>
                    </tr>
                </table>
                <center>
                    <p id="dialogMsjSMS"  ></p>  
                    <p id="sms2"></p> 
                </center>

            </div>
        </div>

        <div id="info"  class="ventana" >
            <p id="notific" >EXITO AL GUARDAR</p>
        </div>
    </center>

</body>

</html>
