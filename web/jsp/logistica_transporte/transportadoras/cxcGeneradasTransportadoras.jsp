<%-- 
    Document   : cxcGeneradasTransportadoras
    Created on : 31/07/2015, 10:17:20 AM
    Author     : mariana
--%>

<%@page contentType="text/html" pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
    <head>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
        <link href="./css/popup.css" rel="stylesheet" type="text/css">
        <link type="text/css" rel="stylesheet" href="./css/jquery/jquery-ui/jquery-ui.css" />
        <link type="text/css" rel="stylesheet" href="./css/TransportadorasApi.css " />
        <script type="text/javascript" src="./js/jquery/jquery.jqGrid/js/jquery.min.js"></script>
        <script type="text/javascript" src="./js/jquery-ui-1.8.5.custom.min.js"></script>   
        <script type="text/javascript" src="./js/jquery/jquery.jqGrid-4.7.0/js/grid.locale-es.js"></script>
        <script type="text/javascript" src="./js/jquery/jquery.jqGrid-4.7.0/js/jquery.jqGrid.min.js"></script>
        <script type="text/javascript" src="./js/jquery/jquery.jqGrid-4.7.0/js/jquery.tabletojson.js"></script>    
        <script type="text/javascript" src="./js/cxcGeneradasTranspotadoras.js"></script> 
        <script type="text/javascript" href="http://www.ddginc-usa.com/spanish/editcheck.js"></script>
        <title>VISUALIZACION DE CORRIDAS </title>
    </head>
    <body>

        <div id="capaSuperior" style="position:absolute; width:100%; height:100px; z-index:0; left: 0px; top: 0px;">
            <jsp:include page="/WEB-INF/toptsp.jsp?encabezado=VISUALIZACION DE CORRIDAS"/>
        </div>

    <center>
        <div id="tablita" style="top: 170px;width: 758px;height: 86px;" >
            <div id="encabezadotablita" style="width: 749px">
                <label class="titulotablita"><b>FILTRO DE BUSQUEDA</b></label>
            </div> 
            <table id="tablainterna"style="height: 53px;"  >
                <tr>
                    <td>
                        <label>Transportadoras</label>
                    </td>
                    <td>
                        <select id="transportadoras" style="width: 197px;color: #050505;height: 22px;margin-left: 5px;" > <!--onchange="cargarFechasCXC()"-->
                        </select>
                    </td>
                    <td>
                        <label>Fecha Inicial</label>
                    </td>
                    <!-- <td>
                                            <select id="fechacxc" style="width: 125px;color: #050505;height: 22px;margin-left: 5px;">
                                            </select>
                                        </td>-->
                    <td>
                        <input type="datetime" id="fechacxc" name="fecha" style="height: 20px;width: 88px;color: #070708;" readonly>
                    </td>
                    <td>
                        <label>Fecha Final</label>
                    </td>
                   <td>
                        <input type="datetime" id="fechafin" name="fechafin" style="height: 20px;width: 88px;color: #070708;" readonly>
                    </td>
                    
                    <td>
                        <div id ='botones'>
                            <button id="buscar" class="ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only" 
                                    role="button" aria-disabled="false" style="margin-top: 6px;margin-left: 6px;">
                                <span class="ui-button-text">Buscar</span>
                            </button> 
                        </div>
                    </td>
                </tr>
            </table> 

        </div>

        <div style="position: relative;top: 204px;">
            <table id="tabla_cargar_cxcGenerada" ></table>
            <div id="pager"></div>
        </div>

        <div id="dialogCXC"  class="ventana" >
            <div style="position: relative;">
                <table id="tabla_cargar_cxc_detalle" ></table>
                <div id="pager1"></div>
            </div>
        </div>
    </center>
    <div id="info"  class="ventana" >
        <p id="notific">EXITO AL GUARDAR</p>
    </div>

    <div id="divSalidaEx" title="Exportacion" style=" display: block" >
        <p  id="msjEx" style=" display:  none"> Espere un momento por favor...</p>
        <center>
            <img id="imgloadEx" style="position: relative;  top: 7px; display: none " src="./images/cargandoCM.gif"/>
        </center>
        <div id="respEx" style=" display: none"></div>
    </div> 
     <div id="divSalidaExcxp" title="Exportacion" style=" display: block" >
        <p  id="msjExcxp" style=" display:  none"> Espere un momento por favor...</p>
        <center>
            <img id="imgloadExcxp" style="position: relative;  top: 7px; display: none " src="./images/cargandoCM.gif"/>
        </center>
        <div id="respExcxp" style=" display: none"></div>
    </div> 
</body>
</html>
