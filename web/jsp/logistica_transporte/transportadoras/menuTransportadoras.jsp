<%-- 
    Document   : importarAnticipos
    Created on : 29/02/2016, 11:15:19 AM
    Author     : si-00261
--%>

<%@page contentType="text/html" pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
     <head>
        <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
        <!--Bootstrap Core CSS -->
        <link href="./css/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css"/>
        <link href="./css/simple-sidebar.css" rel="stylesheet" type="text/css"/>
        <link href="./css/popup.css" rel="stylesheet" type="text/css">
        <link type="text/css" rel="stylesheet" href="./css/jquery/jquery-ui/jquery-ui.css" />       
<!--    <link type="text/css" rel="stylesheet" href="./css/TransportadorasApi.css" />-->
        <link type="text/css" rel="stylesheet" href="./css/anticipos_transportadora.css" >
        <script type="text/javascript" src="./js/jquery/jquery.jqGrid/js/jquery.min.js"></script>
        <script type="text/javascript" src="./js/jquery-ui-1.8.5.custom.min.js"></script>   
        <script type="text/javascript" src="./js/jquery/jquery.jqGrid-4.7.0/js/grid.locale-es.js"></script>
        <script type="text/javascript" src="./js/jquery/jquery.jqGrid-4.7.0/js/jquery.jqGrid.min.js"></script>
        <script type="text/javascript" src="./js/jquery/jquery.jqGrid-4.7.0/js/jquery.tabletojson.js"></script>    
        <script type="text/javascript" src="./js/MenuTransportadoras.js"></script>       

        <title>SUBIR ANTICIPOS</title>
    </head>
    <body>
       <jsp:include page="/WEB-INF/toptsp.jsp?encabezado=MODULO TRANSPORTADORAS"/>
            <div id="wrapper">

                <!-- Sidebar -->
                <div id="sidebar-wrapper">
                    <ul id="menu-pagina" class="sidebar-nav">
                    </ul>
                </div>
                <!-- /#sidebar-wrapper -->

                <!-- Page Content -->
                <div id="page-content-wrapper" >                 

                    <button id="menu-toggle" type="button" class="btn btn-default btn-lg">
                        <span class="glyphicon glyphicon-menu-hamburger" aria-hidden="true"></span>
                    </button>    
                    <div class="container-fluid">
                        <div class="row">
                            <div class="col-lg-12">
                                <div id="container">
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- /#page-content-wrapper -->

            </div>
            <!-- /#wrapper -->
            <!-- Dialogo ventana Envio de reanticipo> -->
            <div id="dialogReanticipo" title="Envio de Reanticipo" style="display:none;">                  
                  <table aling="center" style=" width: 100%; margin: 10px" >
                         <tr> 
                             <td style="width: 20%"><span>C�d. Empresa<b style="color:red">*</b></span></td>                          
                             <td style="width: 30%"><input type="text" id="codigo_empresa" name="codigo_empresa" class="input" readonly></td>                            
                             <td style="width: 20%"><span>C�d. Agencia<b style="color:red">*</b></span></td>                          
                             <td style="width: 30%"><input type="text" id="codigo_agencia" name="codigo_agencia" class="input" readonly></td>                     
                         </tr> 
                         <tr>
                                <td style="width: 20%"><span>Planilla:<b style="color:red">*</b></span></td>   
                                <td style="width: 30%"><input type="text" id="num_planilla" name="num_planilla" class="input" readonly></td>      
                                <td style="width: 20%"><span>Fecha</span></td>                          
                                <td style="width: 30%"><input type="text" id="fecha_reanticipo" name="fecha_reanticipo" class="input" readonly></td> 
                         </tr>
                         <tr>                           
                             <td style="width: 20%"><span>Origen<b style="color:red">*</b></span></td>                          
                             <td style="width: 30%"><input type="text" id="origen" name="origen" class="input"></td>      
                             <td style="width: 20%"><span>Destino<b style="color:red">*</b></span></td>                          
                             <td style="width: 30%"><input type="text" id="destino" name="destino" class="input"></td>          
                         </tr> 
                         <tr>                           
                             <td style="width: 20%"><span>Vlr Reanticipo<b style="color:red">*</b></span></td>                          
                             <td style="width: 30%"><input type="text" id="vlr_reanticipo" name="vlr_reanticipo" class="input solo-numero" style="text-align:right"></td>  
                             <td style="width: 20%"><span>Comision Inter.(%)</span></td>                          
                             <td style="width: 30%"><input type="text" id="porc_comision" name="porc_comision" class="input" style="text-align:right" readonly></td>        
                         </tr>
                         <tr>                           
                             <td style="width: 20%"><span>Vlr Comision Inter.</span></td>                          
                             <td style="width: 30%"><input type="text" id="vlr_comision" name="vlr_comision" class="input" style="text-align:right" readonly></td>  
                             <td style="width: 20%"><span>Vlr desembolsar</span></td>                          
                             <td style="width: 30%"><input type="text" id="vlr_desembolsar" name="vlr_desembolsar" class="input" style="text-align:right" readonly></td>        
                         </tr>
                         <tr>
                            <td style="width: 20%"><span>Banco<b style="color:red">*</b></span></td>                          
                            <td style="width: 30%"><input type="text" id="banco" name="banco" class="input"></td>                            
                            <td style="width: 20%"><span>Sucursal<b style="color:red">*</b></span></td>                          
                            <td style="width: 30%"><input type="text" id="sucursal" name="sucursal" class="input"></td>                     
                         </tr>
                         <tr>
                             <td style="width: 20%"><span>Tipo Cuenta<b style="color:red">*</b></span></td>                          
                             <td style="width: 30%"><input type="text" id="tipo_cuenta" name="tipo_cuenta" class="input"></td>                            
                             <td style="width: 20%"><span>No Cuenta<b style="color:red">*</b></span></td>                          
                             <td style="width: 30%"><input type="text" id="cuenta" name="cuenta" class="input solo-numero"></td>                    
                         </tr>
                         <tr>
                             <td style="width: 20%"><span>Id Titular Cta<b style="color:red">*</b></span></td>                          
                             <td style="width: 30%"><input type="text" id="cedula_titular_cta" name="cedula_titular_cta" class="input"></td>                            
                             <td style="width: 20%"><span>Nombre<b style="color:red">*</b></span></td>                          
                             <td style="width: 30%"><input type="text" id="nombre_titular_cta" name="nombre_titular_cta" class="input"></td>                    
                         </tr>                         
                   </table> 
                </br> 
            </div>
                 
                <!-- Dialogo ventana anulacion anticipo> -->
                <div id="dialogAddComent" style="display:none;">       
                    <br>                          
                    <table border="0" align="center" cellpadding="1" cellspacing="1" class="labels">                      
                        <tr>
                            <td style="width: 20%"><span>Comentario:</span></td>   
                            <td style="width: 80%" colspan="4"><textarea id ="comment" name="comment" cols="55"  rows="4" maxlength="300"></textarea></td>                        
                        </tr>
                    </table> 
                </div>
                <div id="dialogLoading" style="display:none;">
                    <p  style="font-size: 12px;text-align:justify;" id="msj2">Texto </p> <br/>
                    <center>
                        <img src="./images/cargandoCM.gif"/>
                    </center>
                </div>
                <div id="dialogMsj" title="Mensaje" style="display:none;">
                    <p style="font-size: 12.5px;text-align:justify;" id="msj" > Texto </p>
                </div>      
        
        </div>
    </body>
</html>
