<%-- 
    Document   : relProductoTransportadoras
    Created on : 19/06/2015, 10:44:38 AM
    Author     : mariana
--%>

<%@page contentType="text/html" pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
        <link href="./css/popup.css" rel="stylesheet" type="text/css">
        <link type="text/css" rel="stylesheet" href="./css/jquery/jquery-ui/jquery-ui.css" />
        <link type="text/css" rel="stylesheet" href="./css/TransportadorasApi.css " />
        <script type="text/javascript" src="./js/jquery/jquery.jqGrid/js/jquery.min.js"></script>
        <script type="text/javascript" src="./js/jquery-ui-1.8.5.custom.min.js"></script>   
        <script type="text/javascript" src="./js/jquery/jquery.jqGrid-4.7.0/js/grid.locale-es.js"></script>
        <script type="text/javascript" src="./js/jquery/jquery.jqGrid-4.7.0/js/jquery.jqGrid.min.js"></script>
        <script type="text/javascript" src="./js/jquery/jquery.jqGrid-4.7.0/js/jquery.tabletojson.js"></script>    
        <script type="text/javascript" src="./js/transportadorasLog.js"></script>    
        <script type="text/javascript" src="./js/jquery/jquery.jqGrid-4.7.0/plugins/jquery.contextmenu.js"></script>
        <title>TRANSPORTADORAS</title>

        <!--        <style type="text/css">
                    .ui-jqgrid .ui-jqgrid-htable th div
                    {
                        height: auto;
                        overflow: hidden;
                        padding-right: 4px;
                        padding-top: 2px;
                        position: relative;
                        vertical-align: text-top;
                        white-space: normal !important;
                    }
                </style>-->
    </head>
    <body>
        <div id="capaSuperior" style="position:absolute; width:100%; height:100px; z-index:0; left: 0px; top: 0px;">
            <jsp:include page="/WEB-INF/toptsp.jsp?encabezado=TRANSPORTADORAS "/>
        </div>

    <center>
        <div id="tablita" style="top: 0px;width: 400px; top: 150px;" >
            <div id="encabezadotablita" style="width: 391px;">
                <label class="titulotablita"><b>FILTRO DE BUSQUEDA</b></label>
            </div> 
            <table id="tablainterna"  >
                <tr>
                    <td >
                        <label >Transportadoras</label>
                    </td>
                    <td>
                        <select id ="transportadoras" style="color: #060606;width: 217px;" onchange="tablaRelacionProTran()"></select>
                    </td>
                    <!--                    <td>
                                            <button id="cargar" class="ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only" 
                                                    role="button" aria-disabled="false">
                                                <span class="ui-button-text">Cargar</span>
                                            </button>                         
                                        </td>-->
                </tr>
            </table>

        </div>
        <div style="position: relative;top: 200px;">
            <table id="tabla_relacion" ></table>
            <div id="pager"></div>

        </div>
        <div id="info"  class="ventana" >
            <p id="notific" >EXITO AL GUARDAR</p>
        </div>
        <div id="menu" class="ContextMenu"style="">
            <ul id="listopciones">
                <li id="eliminar" >
                    <span style="font-family:Verdan">Eliminar fila</span>
                </li>
            </ul>

        </div>

    </center>
</body>
<script>
    inicio3();
</script>
</html>
