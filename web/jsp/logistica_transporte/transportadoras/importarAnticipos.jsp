<%-- 
    Document   : importarAnticipos
    Created on : 29/02/2016, 11:15:19 AM
    Author     : si-00261
--%>

 
<script type="text/javascript" src="./js/anticipos_transportadora.js"></script> 
<div id="capaCentral" align="left" style="position:absolute; width:100%; height:100%; z-index:0; top: 16px;" >
    <center>

        <div id="div_importar_anticipo" >

            <div id="encabezadotablita" style="width: 595px;margin-bottom: 10px">
                <label class="titulotablita"><b>SUBIR ANTICIPOS </b></label>
            </div> 
            <table border="0" style="width:550px">
                <tr>
                    <td>
                        <label>Producto</label>
                    </td>
                    <td>
                        <select name="productos" class="combo_180px" id="productos" style="width: 190px;height: 25px;color: #070708;"></select>
                    </td>
                    <td>
                        <label>Formato&nbsp;</label>
                    </td>
                    <td>
                        <select name="format" class="combo_180px" id="format" style="width: 170px;height: 25px;color: #070708;">
                            <option value="f">Completo</option>
                            <option value="s">Simplificado</option>
                        </select>
                    </td>
                </tr>
                <tr>                   
                    <td style="font-size: 12px;width:120px;padding: 5px">
                        <b>Seleccione archivo</b>
                    </td>
                    <td style="padding-top:8px" colspan="2">
                        <form id="formulario" name="formulario">
                            <input type="file" id="examinar" name="examinar"  style="font-size: 11px;width:275px" >
                        </form>
                    </td>  
                    <td style="text-align: right">
                        <button id="subirArchivo" class="ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only" 
                                role="button" aria-disabled="false" >
                            <span class="ui-button-text">Subir</span>
                        </button> 
                    </td>
                </tr>
               
            </table>    
<!--            <div id ='botones'>
                <button id="subirArchivo" class="ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only" 
                        role="button" aria-disabled="false" >
                    <span class="ui-button-text">Subir</span>
                </button> 
            </div>  -->
        </div>
        </br></br>
    </center>                                 
</div>