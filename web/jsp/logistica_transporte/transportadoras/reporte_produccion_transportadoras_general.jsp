<%-- 
    Document   : reporte_produccion_transportadoras
    Created on : 10/07/2015, 10:44:57 AM
    Author     : mariana
--%>

<%@page contentType="text/html" pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
        <link href="./css/popup.css" rel="stylesheet" type="text/css">
        <link type="text/css" rel="stylesheet" href="./css/jquery/jquery-ui/jquery-ui.css" />
        <link type="text/css" rel="stylesheet" href="./css/TransportadorasApi.css " />
        <script type="text/javascript" src="./js/jquery/jquery.jqGrid/js/jquery.min.js"></script>
        <script type="text/javascript" src="./js/jquery-ui-1.8.5.custom.min.js"></script>   
        <script type="text/javascript" src="./js/jquery/jquery.jqGrid-4.7.0/js/grid.locale-es.js"></script>
        <script type="text/javascript" src="./js/jquery/jquery.jqGrid-4.7.0/js/jquery.jqGrid.min.js"></script>
        <script type="text/javascript" src="./js/jquery/jquery.jqGrid-4.7.0/js/jquery.tabletojson.js"></script>    
        <script type="text/javascript" src="./js/transportadorasReporteProduccionGeneral.js"></script>    


        <title>REPORTE PRODUCCION TRANSPORTADORAS</title>
    </head>
    <style>
        .label{
            font-family: "Lucida Grande","Lucida Sans",Arial,sans-serif;
        }
        #encabezadotablita{


        }
    </style>
    <body>
        <div id="capaSuperior" style="position:absolute; width:100%; height:100px; z-index:0; left: 0px; top: 0px;">
            <jsp:include page="/WEB-INF/toptsp.jsp?encabezado=REPORTE PRODUCCION TRANSPORTADORAS"/>
        </div>
    <center>
        <div id="tablita" style="width: 450px;height: 160px;" >
            <div id="encabezadotablita" style="width: 446px;">
                <label class="titulotablita"><b>FILTRO </b></label>
            </div> 
            <table id="tablainterna" style="margin-top: 10px;" >
                <tr>
                    <td colspan="1">
                        <label>Transportadora</label>
                    </td>
                    <td colspan="3">
                        <select id="transportadora" style="width: 311px;height: 25px;color: #070708;">
                        </select>
                    </td>
                </tr>
                <tr>
                    <td>
                        <label>Fecha de Inicio</label>
                    </td>
                    <td>
                        <input type="datetime" id="fechainicio" name="fechainicio" style="height: 20px;width: 88px;color: #070708;">
                    </td>
                    <td>
                        <label>Fecha Final</label>
                    </td>
                    <td>
                        <input type="datetime" id="fechafinal" name="fechafinal" style="height: 20px;width: 88px;color: #070708;">
                    </td>
                </tr>
                <tr>
                    <td colspan="4">
                        <hr>
                    </td>
                </tr>
            </table>
            <div id ='botones' >
                <button id="buscarfiltro" class="ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only" 
                        role="button" aria-disabled="false">
                    <span class="ui-button-text">Buscar</span>
                </button> 
            </div>

        </div>


        <div style="position: relative;top: 150px;">
            <table id="tabla_reporte_transportadoras" ></table>
            <div id="pager"></div>
        </div>

        <div id="divSalidaEx" title="Exportacion" style=" display: block" >
            <p  id="msjEx" style=" display:  none"> Espere un momento por favor...</p>
            <center>
                <img id="imgloadEx" style="position: relative;  top: 7px; display: none " src="./images/cargandoCM.gif"/>
            </center>
            <div id="respEx" style=" display: none"></div>
        </div> 
    </center>
</body>
</html>
