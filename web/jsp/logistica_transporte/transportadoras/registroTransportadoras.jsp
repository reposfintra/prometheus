<%-- 
    Document   : registroTransportadoras
    Created on : 19/06/2015, 10:44:04 AM
    Author     : mariana
--%>

<%@page contentType="text/html" pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
        <link href="./css/popup.css" rel="stylesheet" type="text/css">
        <link type="text/css" rel="stylesheet" href="./css/jquery/jquery-ui/jquery-ui.css" />
        <link type="text/css" rel="stylesheet" href="./css/TransportadorasApi.css " />
        <script type="text/javascript" src="./js/jquery/jquery.jqGrid/js/jquery.min.js"></script>
        <script type="text/javascript" src="./js/jquery-ui-1.8.5.custom.min.js"></script>   
        <script type="text/javascript" src="./js/jquery/jquery.jqGrid-4.7.0/js/grid.locale-es.js"></script>
        <script type="text/javascript" src="./js/jquery/jquery.jqGrid-4.7.0/js/jquery.jqGrid.min.js"></script>
        <script type="text/javascript" src="./js/jquery/jquery.jqGrid-4.7.0/js/jquery.tabletojson.js"></script>    
        <script type="text/javascript" src="./js/transportadorasLog.js"></script>    


        <title>TRANSPORTADORAS</title>
    </head>
    <body>
        <div id="capaSuperior" style="position:absolute; width:100%; height:100px; z-index:0; left: 0px; top: 0px;">
            <jsp:include page="/WEB-INF/toptsp.jsp?encabezado=TRANSPORTADORAS "/>
        </div>
    <center>

        <div style="position: relative;top: 150px;">
            <table id="tabla_transportadoras" ></table>
            <div id="pager"></div>

        </div>

        <%-- --------------------------REGISTRO TRANSPORTADORAS----------------------------%>
        <div id="dialogMsjtransportadora"  class="ventana" >
            <div id="tablita" style="top: 0px;width: 535px;" >
                <div id="encabezadotablita">
                    <label class="titulotablita"><b>REGISTRO TRANSPORTADORAS</b></label>
                </div> 
                <table id="tablainterna"  >
                    <tr>
                        <td>
                            <input type="text" id="idtran"  style="width: 50px"hidden ><!--hidden ="false"-->
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <input type="text" id="codcli"  style="width: 50px" hidden><!--hidden ="false"-->
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <input type="text" id="iditusu"  style="width: 50px" hidden><!--hidden ="false"-->
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2">
                            <label>Nombre de Transportadora<span style="color:red;">*</span></label> 
                        </td>
                        <td colspan="2">
                            <input type="text" id="nombretran" onchange="conMayusculas(this)" style="width: 95%"  />  
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2">
                            <label>Tipo Documento<span style="color:red;">*</span></label> 
                        </td>
                        <td colspan="2">
                            <select id="tipodoc" style="width: 125px;color: black;">
                                <option value="">Seleccione</option>
                                <option value="CED">CEDULA</option>
                                <option value="NIT">NIT</option>
                            </select>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2">
                            <label>Nit de Transportadora<span style="color:red;">*</span></label> 
                        </td>
                        <td colspan="2">
                            <input type="text" id="nittran" class="solo-numero"  maxlength="10" style="width: 95%"  onblur="verificarTrasportadora()"/>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2">
                            <label>Direccion<span style="color:red;">*</span></label> 
                        </td>
                        <td colspan="2">
                            <input type="text" id="direcciontran" onchange="conMayusculas(this)" style="width: 95%"  />
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2">
                            <label>Correo Electronico<span style="color:red;">*</span></label> 
                        </td>
                        <td colspan="2">
                            <input placeholder="Formato: algo@dir.dom" title="correo incorrecto ejemplo : ejemplo@correo.co" type="email" id="correo" onchange="conMayusculas(this)" style="width: 95%"  />
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2">
                            <label>Cupo Rotativo<span style="color:red;">*</span></label> 
                        </td>
                        <td colspan="2">
                            <input type="text" id="cuporot" class="solo-numero"  style="width: 95%"  />
                        </td>

                    </tr>
                    <tr>
                        <td colspan="2">
                            <label>Periodicidad<span style="color:red;">*</span></label> 
                        </td>
                        <td colspan="2">
                            <select id="periodicidad" style="width: 157px" >
                            </select>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2">
                            <label>Pais<span style="color:red;">*</span></label> 
                        </td>
                        <td colspan="2">
                            <select id="pais" style="width: 157px" onchange="cargarDepartamento(this.id)">
                            </select>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2">
                            <label>Departamento<span style="color:red;">*</span></label> 
                        </td>
                        <td colspan="2">
                            <select id="departamento" style="width: 157px" onchange="cargarCiudad(this.id)">
                            </select>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2">
                            <label>Ciudad<span style="color:red;">*</span></label> 
                        </td>

                        <td colspan="2">
                            <select id="ciudad" style="width: 157px" >
                            </select>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2">
                            <label>Nombre Representate Legal<span style="color:red;">*</span></label> 
                        </td>
                        <td colspan="2">
                            <input type="text" id="nombreencargado" onchange="conMayusculas(this)" style="width: 95%"  />
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2">
                            <label>Identificacion <span style="color:red;">*</span></label> 
                        </td>
                        <td colspan="2">
                            <input type="text" id="identificacion" class="solo-numero"  maxlength="10" style="width: 95%"  />
                        </td>
                    </tr>
                    <!--                    <tr>
                                            <td colspan="2">
                                                <label>Fecha Pago</label> 
                                            </td>
                                                                    <td>
                                                                        <div>
                                                                            <button id="buscarlog" class="ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only" 
                                                                                    role="button" aria-disabled="false" style="width: 55px;height: 25px" onclick="fechapago()">
                                                                                <span class="ui-button-text"></span>
                                                                            </button> 
                                                                        </div>
                                                                    </td>
                                            <td>
                                                <button type="button" id="fechpago" onclick="fechapago()" style="width: 55px;height: 25px"></button>
                                            </td>
                    
                                        </tr>-->
                    <tr>
                        <td>
                            <label>Handle Code</label>
                        </td>
                        <td  colspan="3">
                            <select id="hc" style="width: 355px ;color: black;">
                            </select>
                        </td>
                    </tr>
                </table>

                <fieldset class="fieldset" style="border-color: rgb(42, 136, 200) ! important;">
                    <legend>Usuario de la Transpotadora</legend>
                    <div>
                        <table>
                            <tr>
                                <td>
                                    <label>ID del usuario<span style="color:red;">*</span></label> 
                                </td>
                                <td>
                                    <input type="text" maxlength="10" id="idusu" style="width: 105px" onchange="conMayusculas(this)" onblur="verificarUsuario()" />   
                                </td>
                                <td>
                                    <label>Cambiar Clave al Inicio</label> 
                                </td>
                                <td>
                                    <input type="checkbox" id="actdes" value="true" checked="" />   
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <label>Password<span style="color:red;">*</span></label> 
                                </td>
                                <td>
                                    <input type="password" id="pas1" maxlength="15" style="width: 105px" />   
                                </td>
                                <td>
                                    <label>Confirmar Password<span style="color:red;">*</span></label> 
                                </td>
                                <td>
                                    <input type="password" id="pas2" onkeyup="validarpass()"  maxlength="15"   style="width: 105px" />
                                </td>
                            </tr>
                        </table>
                    </div>

                </fieldset>
                <center>
                    <p id="dialogMsjSMS"  style="color: red"></p>  
                    <p id="sms2" style="color: red"></p> 
                </center>
            </div>
        </div>
        <div id="info"  class="ventana" >
            <p id="notific" >EXITO AL GUARDAR</p>
        </div>
    </center>
</body>
<script>
    $(document).ready(function () {
        $('.solo-numero').keyup(function () {
            this.value = (this.value + '').replace(/[^0-9]/g, '');
        });
    });

    inicio5();
</script>
</html>
