<%-- 
    Document   : enviarReanticipos
    Created on : 1/03/2016, 05:06:58 PM
    Author     : si-00261
--%>
<script type="text/javascript" src="./js/anticipos_transportadora.js"></script> 
<div id="capaCentral" align="left" style="position:absolute; width:100%; height:100%; z-index:0; top: 16px;" >
    <center>    
        <div id="tablita" style="width: 400px;height: 235px;" >
            <div id="encabezadotablita" style="width: 395px;">
                <label class="titulotablita"><b>FILTRO DE BUSQUEDA </b></label>
            </div> 
            <table id="tablainterna">
                <tr>
                    <td colspan="1">
                        <label>Transportadora</label>
                    </td>
                    <td colspan="3">
                        <select id="transportadora" style="width: 285px;height: 25px;color: #070708;">
                        </select>
                    </td>
                </tr>
                <tr>
                    <td>
                        <label>Fecha de Inicio</label>
                    </td>
                    <td>
                        <input type="datetime" id="fechainicio" name="fechainicio" style="height: 20px;width: 88px;color: #070708;">
                    </td>
                    <td>
                        <label>Fecha Final</label>
                    </td>
                    <td>
                        <input type="datetime" id="fechafinal" name="fechafinal" style="height: 20px;width: 88px;color: #070708;">
                    </td>
                </tr>
                <tr>
                    <td colspan="1">
                        <label>Producto</label>
                    </td>
                    <td colspan="3">
                        <select name="productos" class="combo_180px" id="productos" style="width: 285px;height: 25px;color: #070708;"></select>
                    </td>        
                </tr>
                <tr>
                    <td colspan="1">
                        <label>No Planilla</label>
                    </td>
                    <td colspan="3">
                        <input type="text" id="planilla" name="planilla" style="width: 150px;height: 25px;color: #070708;">                      
                    </td> 
                </tr>
                <tr>
                    <td colspan="4">
                        <hr>
                    </td>
                </tr>
            </table>
            <div id ='botones' >
                <button id="buscaranticipos" class="ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only" 
                        role="button" aria-disabled="false">
                    <span class="ui-button-text">Buscar</span>
                </button> 
                <button id="clearanticipos" class="ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only" 
                        role="button" aria-disabled="false">
                    <span class="ui-button-text">Limpiar</span>
                </button>
            </div>

        </div>             
        <br><br>
        <div id="grid_anticipos">
            <table id="tabla_anticipos"></table>                
            <div id="page_tabla_anticipos"></div>
        </div>        
    </center>      
   
                            
</div>