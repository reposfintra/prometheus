<%-- 
    Document   : transportadorasLog
    Created on : 17/04/2015, 09:35:15 AM
    Author     : mariana
--%>

<%@page contentType="text/html" pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
        <link href="./css/popup.css" rel="stylesheet" type="text/css">
        <link type="text/css" rel="stylesheet" href="./css/jquery/jquery-ui/jquery-ui.css" />
        <link type="text/css" rel="stylesheet" href="./css/TransportadorasApi.css " />
        <script type="text/javascript" src="./js/jquery/jquery.jqGrid/js/jquery.min.js"></script>
        <script type="text/javascript" src="./js/jquery-ui-1.8.5.custom.min.js"></script>   
        <script type="text/javascript" src="./js/jquery/jquery.jqGrid-4.7.0/js/grid.locale-es.js"></script>
        <script type="text/javascript" src="./js/jquery/jquery.jqGrid-4.7.0/js/jquery.jqGrid.min.js"></script>
        <script type="text/javascript" src="./js/jquery/jquery.jqGrid-4.7.0/js/jquery.tabletojson.js"></script>    
        <script type="text/javascript" src="./js/transportadorasLog.js"></script>    

        <title>Log</title>
    </head>
    <body>
        <div id="capaSuperior" style="position:absolute; width:100%; height:100px; z-index:0; left: 0px; top: 0px;">
            <jsp:include page="/WEB-INF/toptsp.jsp?encabezado=Log Transportadoras"/>
        </div>
    <center>
        <div id="tablita" style="width: 1150px;">
            <div id="encabezadotablita"style="width: 1140px;">
                <label class="titulotablita"><b>CRITERIOS DE BUSQUEDA</b></label>
            </div>
            <table id="tablainterna"  >
                <tr>
                    <td padding="10" >
                        <label class="tip" >Tipo</label>
                        <select  class="stipo " id="tipo" name="tipo" style="height: 25px;color: #000;">                                   
                        </select>

                        <label >Empresas</label>
                        <select  class="empres " id="empresa" name="empresa" style="height: 25px;color: #000;width: 238px">                                   
                        </select>

                        <label > Estado</label>
                        <select class="est" id="estadoT" name="estadoT" style="height: 25px;color: #000;">
                            <option value="">  </option>
                            <option value="true">Exitoso</option>
                            <option value="false">Fallido</option>
                        </select>

                        <label >Fecha de inicio</label>
                        <input type="datetime" id="fechainicio" name="fechainicio" style="height: 20px;color: #000;" >

                        <label >Fecha Final</label>
                        <input type="datetime" id="fechafinal" name="fechafinal" style="height: 20px;color: #000;">
                        
                        <label >Planilla</label>
                        <input  class="" id="planillaP" name="planillaP" style="height: 20px;color: #000;width: 100px">                                   
                        
                    </td>
                </tr>
                <tr >
                    <td>
                        <hr>
                    </td>
                </tr>
            </table>
            <div id ='botones' >
                <button id="buscarlog" class="ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only" 
                        role="button" aria-disabled="false">
                    <span class="ui-button-text">Buscar</span>
                </button> 

                <button id="salirtrans" class="ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only" 
                        role="button" aria-disabled="false" onclick="">
                    <span class="ui-button-text">Salir</span>
                </button> 
            </div>

        </div>
        <div style="position: relative;top: 150px;">
            <table id="tabla_cargar_log" ></table>
            <div id="pager2"></div>
        </div>

        <div id="dialogMsj" title="Trama Json" class="ventana" >
            <textarea id="texto" cols="106" rows="34" >                        
            </textarea>
        </div>
    </center>
</body>
<script>
    inicio1();
</script>
</html>
