<%-- 
    Document   : reversar_lote
    Created on : 12/08/2019, 03:24:22 PM
    Author     : bterraza
--%>

<%@page contentType="text/html" pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
        <title>Reversar Lote</title>

        <link href="./css/Reversar_lote.css" rel="stylesheet" type="text/css">
        <link href="./css/popup_rl.css" rel="stylesheet" type="text/css">
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">


        <script type="text/javascript" src="./js/jquery/jquery.jqGrid/js/jquery.min.js"></script>
        <script type="text/javascript" src="./js/jquery-ui-1.8.5.custom.min.js"></script>
        <script type="text/javascript" src="./js/jquery/jquery.jqGrid-4.7.0/js/grid.locale-es.js"></script>
        <script type="text/javascript" src="./js/jquery/jquery.jqGrid-4.7.0/js/jquery.jqGrid.min.js"></script>
        <!--        <script type="text/javascript" src="./js/jquery/jquery.jqGrid-4.7.0/js/jquery.tabletojson.js"></script>-->
        <script type="text/javascript" src="./js/reversar_lote.js"></script>


    </head>    
    <body>
        <div id="capaSuperior" style="position:absolute; width:100%; height:100px; z-index:0; left: 0px; top: 0px;">
            <jsp:include page="/WEB-INF/toptsp.jsp?encabezado=Reversar Lote"/>
        </div>



        <div id="capaCentral" class="container" style="width:90%; height:90%; z-index:0; left: 0px; margin-top: 150px; ">
            <div class="card" style="-webkit-box-shadow: 6px 10px 16px 0px rgba(0,0,0,0.75); -moz-box-shadow: 6px 10px 16px 0px rgba(0,0,0,0.75); box-shadow: 6px 10px 16px 0px rgba(0,0,0,0.75);">
                <div class="card-header" style="font-family: monospace; background: rgb(57, 126, 173); color: rgb(255, 255, 255); font-weight: bold">
                    Reversar Lote
                </div>
                <div class="card-body">
                    <!--<h5 class="card-title">Special title treatment</h5>-->
                    <!--<p class="card-text">With supporting text below as a natural lead-in to additional content.</p>-->
                    <div class="row">
                        <div class="col-sm">
                            <div class="form-group">
                                <label for="fecha">Fecha</label>
                                <input type="text" class="form-control" id="fecha" readonly="true" style="font-family: monospace; width: 330px; height: 43px;">
                                <small id="emailHelp" class="form-text text-muted">Se reversaran los lotes del dia de hoy!</small>
                            </div>
                        </div>
                        <div class="col-sm">
                            <div class="form-group">
                                <label for="transportadora">Transportadora:</label>
                                <select class="form-control" id="transportadora" id="transportadora" style="font-family: monospace; width: 330px; height: 43px;"></select>
                                <!--<small id="emailHelp" class="form-text text-muted">Se reversaran los lotes del dia de hoy.</small>-->
                            </div>
                        </div>
                        <div class="col-sm">
                            <div class="form-group" style="display: grid;">
                                <label for="buscar">&nbsp;</label>
                                <button type="button" id="buscar" class="btn btn-secondary" style="font-family: monospace; height: 43px; font-weight: bold">Buscar</button>
                            </div>
                        </div>
                    </div>

                    <br>

                    <div class="row" style="display: flex; justify-content: center; align-items: center;">
                        <table id="tbl_reversar_lote" class="table table-striped"></table>
                        <div id="page_reversar_lote"></div>
                    </div>

                    <div class="row" style="display: flex; justify-content: center; align-items: center;"> 
                        <div id='apr' style='padding-top: 30px; display:none'>
                            <button id="reversar" class="btn btn-secondary" style="font-family: monospace;">Reversar</button>                
                        </div>
                    </div>

                </div>
                <div class="card-footer bg-transparent" style="font-family: monospace; display: flex; justify-content: flex-end; color: rgb(108, 117, 125); ">Fintra S.A - Logistica</div>
            </div>

        </div>

        <!--        <div id="capaCentral" style="position:absolute; width:100%; height:80%; z-index:0; left: 0px; top: 100px; ">
        
                    <div id="tabs" style="width: 810px; margin:15px auto 0 auto;">
                        <ul>
                            <li><a href="#tabs-1">Reversar Lote</a></li>                    
                        </ul>
                        <div id="tabs-1">
                            <fieldset>
                                <td class="td"><label for="fecha"> Fecha </label></td>
                                <td class="td"><input type="text" size="15px"  style=" font-size: 12px" required="true" readonly="true" name="fecha" id="fecha"/></td>
                                <label for="transportadora" >Transportadora: </label>
                                <select id="transportadora"></select>
                                <button id="buscar" class="ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only">
                                    <span class="ui-button-text">Buscar</span>
                                </button>
        
                                <button type="button" id="buscar" class="btn btn-secondary">Buscar</button>
        
                            </fieldset>
                        </div>
                    </div>
        
                    <center style='padding-top: 30px'>
                        <table id="tbl_reversar_lote"></table>
                        <div id="page_reversar_lote"></div>
                    </center>
        
                    <center id='apr' style='padding-top: 30px; display:none'>
                        <button id="reversar" class="ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only" >
                            <span class="ui-button-text">Reversar</span>
                        </button>                
                    </center>
                </div>-->
        <div id="dialogo" class="ventana" title="Mensaje">
            <p id="msj">texto </p>
        </div>
        

        <div id="dialogo2" class="ventana">
            <p id="msj2">texto </p> <br/>
            <center>
                <img src="./images/cargandoCM.gif" />
            </center>
        </div>
    </body>
</html>
