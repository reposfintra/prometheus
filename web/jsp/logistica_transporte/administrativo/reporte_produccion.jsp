<%-- 
    Document   : reporte_produccion
    Created on : 9/06/2015, 03:59:30 PM
    Author     : egonzalez
--%>

<%@page contentType="text/html" pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
        <title>Reporte Produccion</title>

        <link href="./css/reporte_produccion.css" rel="stylesheet" type="text/css" >
        <link type="text/css" rel="stylesheet" href="./css/jquery/jquery-ui/jquery-ui.css" />
        <link href="./css/popup.css" rel="stylesheet" type="text/css">
        <script type="text/javascript" src="./js/jquery/jquery.jqGrid/js/jquery.min.js"></script>
        <script type="text/javascript" src="./js/jquery-ui-1.8.5.custom.min.js"></script>
        <script type="text/javascript" src="./js/jquery/jquery.jqGrid-4.7.0/js/grid.locale-es.js"></script>
        <script type="text/javascript" src="./js/jquery/jquery.jqGrid-4.7.0/js/jquery.jqGrid.min.js"></script>
        <script type="text/javascript" src="./js/jquery/jquery.jqGrid-4.7.0/js/jquery.tabletojson.js"></script>
        <script type="text/javascript" src="./js/reporte_produccion.js"></script>

    </head>
    <body>
        <div id="capaSuperior" style="position:absolute; width:100%; height:100px; z-index:0; left: 0px; top: 0px;">
            <jsp:include page="/WEB-INF/toptsp.jsp?encabezado=REPORTE PRODUCCION"/>
        </div>
        <div id="capaCentral" style="position:absolute; width:100%; height:100%; z-index:0; left: 0px; top: 100px; ">
            <div id="filtro">
                <table border="0" class="tablaBusqueda"  cellpadding="1">
                    <thead>
                    <th class="ui-state-default titulo" colspan="5">Filtro  Busqueda</th>
                    </thead>
                    <tbody>
                        <tr>
                            <td>
                                <input type="checkbox" class="selection">
                            </td>
                            <td>Transportadora</td>
                            <td colspan="3">
                                <select class="selected" id="transportadora">
                                    <option></option>
                                </select>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <input type="checkbox" class="selection">
                            </td>
                            <td>C�dula Conductor</td>
                            <td>
                                <input type="text" id="c_conductor"/>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <input type="checkbox" class="selection">
                            </td>
                            <td>C�dula propietario</td>
                            <td>
                                <input type="text" id="c_propietario"/>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <input type="checkbox" class="selection">
                            </td>
                            <td>Planilla</td>
                            <td>
                                <input type="text" id="planilla"/>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <input type="checkbox" class="selection">
                            </td>
                            <td>Placa</td>
                            <td>
                                <input type="text" id="placa"/>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <input type="checkbox" class="selection">
                            </td>
                            <td>Factura</td>
                            <td>
                                <input type="text" id="factura"/>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <input type="checkbox" class="selection">
                            </td>
                            <td>Fecha Inicio</td>
                            <td>
                                <input type="text" id="fecha_inicio" class="inpt" readonly>
                            </td>
                            <td>Fecha Fin</td>
                            <td>

                                <input type="text" id="fecha_fin" class="inpt" readonly>
                            </td>
                        </tr>
                    </tbody>
                </table>
            </div>

            <br>
            <center>                 
                <table id="tbl_reporte_produccion"></table>                
                <div id="page_reporte_produccion"></div>
            </center>
            <div id="buscar-wrapper">
                <button id="buscar_actualizar" class="ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only" >
                    <span class="ui-button-text">Buscar/Actualizar</span>
                </button>
            </div>
        </div>                

        <div id="dialogo" class="ventana" title="Mensaje">
            <p  id="msj"></p>
        </div>

        <form id="form_expr" action="" method="post" name="form_expr">
            <input type="hidden" name="pdfBuffer" id="pdfBuffer" value="" />
            <input type="hidden" name="fileName" id="fileName" value="reporte_produccion" />
            <input type="hidden" name="fileType" id="fileType" value="" />
            <input type="hidden" name="columnas" id="columnas" value="" />
        </form>

    </body>
</html>

