<%-- 
    Document   : transferencias
    Created on : 2/07/2015, 08:54:02 AM
    Author     : egonzalez
--%>

<%@page contentType="text/html" pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
        <title>Transferis Anticipos</title>

        <link href="./css/transferencias.css" rel="stylesheet" type="text/css">
        <link href="./css/popup.css" rel="stylesheet" type="text/css">

        <script type="text/javascript" src="./js/jquery/jquery.jqGrid/js/jquery.min.js"></script>
        <script type="text/javascript" src="./js/jquery-ui-1.8.5.custom.min.js"></script>
        <script type="text/javascript" src="./js/jquery/jquery.jqGrid-4.7.0/js/grid.locale-es.js"></script>
        <script type="text/javascript" src="./js/jquery/jquery.jqGrid-4.7.0/js/jquery.jqGrid.min.js"></script>
<!--        <script type="text/javascript" src="./js/jquery/jquery.jqGrid-4.7.0/js/jquery.tabletojson.js"></script>-->
        <script type="text/javascript" src="./js/transferencias_anticipos.js"></script>
       

    </head>
    <body>
        <div id="capaSuperior" style="position:absolute; width:100%; height:100px; z-index:0; left: 0px; top: 0px;">
            <jsp:include page="/WEB-INF/toptsp.jsp?encabezado=TRANSFERENCIA"/>
        </div>.

        <div id="capaCentral" style="position:absolute; width:100%; height:100%; z-index:0; left: 0px; top: 100px; ">

            <div id="tabs" style="width: 810px; margin:15px auto 0 auto;">
                <ul>
                    <li><a href="#tabs-1">Aprobar Anticipos</a></li>
                    <li><a href="#tabs-2">Transferir Anticipos</a></li>
<!--                    <li><a href="#tabs-3">Reversar Anticipos</a></li>-->
                </ul>
                <div id="tabs-1">
                    <fieldset>
                        <legend>Filtro</legend>
                        <label for="transportadora" >Transportadora</label>
                        <select id="transportadora"></select>
                        <button id="buscar" class="ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only" >
                            <span class="ui-button-text">Buscar</span>
                        </button>
                    </fieldset>
                    <hr>
                </div>
                <div id="tabs-2">
                    <fieldset>
                        <legend>Filtro</legend>
                        <label for="banco_tercero">Banco Transferencia</label>
                        <select id="banco_tercero">
                            <option>Option 1</option>
                            <option>Option 2</option>
                            <option>Option 3</option>
                            <option>Option 4</option>
                        </select>
                        <label for="transportadora2">Transportadora</label>
                        <select id="transportadora2">            
                        </select>

                        <button id="buscar2" class="ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only" >
                            <span class="ui-button-text">Buscar</span>
                        </button>
                    </fieldset>
                     <hr>
                </div>
<!--                <div id="tabs-3">
                     <fieldset>
                        <legend>Filtro</legend>
                        <label for="banco">Banco Transferencia</label>
                        <select id="banco">
                            <option>Option 1</option>
                            <option>Option 2</option>
                            <option>Option 3</option>
                            <option>Option 4</option>
                        </select>
                      <label for="transportadora3">Transportadora</label>
                        <select id="transportadora3">
                            <option>Option 1</option>
                            <option>Option 2</option>
                            <option>Option 3</option>
                            <option>Option 4</option>
                        </select>
                        <button id="buscar3" class="ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only" >
                            <span class="ui-button-text">Buscar</span>
                        </button>
                    </fieldset>
                    <hr>
                </div>-->
            </div>
            <div id="container">                
            </div>
        </div>
        <div id="dialogo" class="ventana" title="Mensaje">
            <p  id="msj">texto </p>
        </div>
        
        <div id="dialogo2" class="ventana">
        <p id="msj2">texto </p> <br/>
        <center>
            <img src="./images/cargandoCM.gif"/>
        </center>
    </div>

</html>
