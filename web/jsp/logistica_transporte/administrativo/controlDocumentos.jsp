<%-- 
    Document   : auditoriaVentas
    Created on : 10/09/2015, 10:10:26 AM
    Author     : mariana
--%>

<%@page contentType="text/html" pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
    <head>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
        <link href="./css/popup.css" rel="stylesheet" type="text/css">
        <link type="text/css" rel="stylesheet" href="./css/jquery/jquery-ui/jquery-ui.css" />
        <link type="text/css" rel="stylesheet" href="./css/TransportadorasApi.css " />
        <script type="text/javascript" src="./js/jquery/jquery.jqGrid/js/jquery.min.js"></script>
        <script type="text/javascript" src="./js/jquery-ui-1.8.5.custom.min.js"></script>   
        <script type="text/javascript" src="./js/jquery/jquery.jqGrid-4.7.0/js/grid.locale-es.js"></script>
        <script type="text/javascript" src="./js/jquery/jquery.jqGrid-4.7.0/js/jquery.jqGrid.min.js"></script>
        <script type="text/javascript" src="./js/jquery/jquery.jqGrid-4.7.0/js/jquery.tabletojson.js"></script>    
        <script type="text/javascript" src="./js/fintralogisticaAdminitrativo.js"></script> 
        <script type="text/javascript" href="http://www.ddginc-usa.com/spanish/editcheck.js"></script>
        <title>CONTROL DE DOCUMENTOS</title>
    </head>
    <body>

        <div id="capaSuperior" style="position:absolute; width:100%; height:100px; z-index:0; left: 0px; top: 0px;">
            <jsp:include page="/WEB-INF/toptsp.jsp?encabezado=CONTROL DE DOCUMENTOS"/>
        </div>

    <center>
        <div id="tablita" style="top: 170px;width: 1083px;height: 86px;" >
            <div id="encabezadotablita" style="width: 1069px">
                <label class="titulotablita"><b>FILTRO DE BUSQUEDA</b></label>
            </div> 
            <table id="tablainterna"style="height: 53px;"  >
                <tr>
                    <td>
                        <label>Empresa</label>
                    </td>
                    <td>
                        <select id="empresa"  style="width: 190px;"></select>
                    </td>
                    <td>
                        <label>Ventas</label>
                    </td>
                    <td>
                        <select id="ventas"  style="width: 65px;">
                            <option value=''>...</option>
                            <option value="true" >Si</option>
                            <option value="false">No</option>
                        </select>
                    </td>
                    <td>
                        <label>Ingreso</label>
                    </td>
                    <td>
                        <select id="ingreso"  style="width: 65px;">
                            <option value=''>...</option>
                            <option value="si" >Si</option>
                            <option value="no">No</option>
                        </select>
                    </td>
                    <td>
                        <label>Planilla</label>
                    </td>
                    <td>
                        <input type="text" id="planilla"  style="width: 100px;height: 20px;" >
                    </td>
                    <td>
                        <label>Corrida</label>
                    </td>
                    <td>
                        <input type="text" id="corrida"  style="width: 100px;height: 20px;" >
                    </td>
                    <td>
                        <label>Fecha</label>
                    </td>
                    <td>
                        <input type="datetime" id="fecha" name="fecha" style="height: 20px;width: 88px;color: #070708;" readonly>
                    </td>

                    <td>
                        <div id ='botones'>
                            <button id="buscar" class="ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only" 
                                    role="button" aria-disabled="false" style="margin-top: 6px;margin-left: 6px;">
                                <span class="ui-button-text">Buscar</span>
                            </button> 
                        </div>
                    </td>
                </tr>
            </table> 

        </div>
        <div style="position: relative;top: 231px;">
            <table id="tabla_control_documentos" ></table>
            <div id="pager1"></div>
        </div>

        <!--        <div id="dialogMdetalle"  class="ventana" >
                    <div style="position: relative;">
                        <table id="tabla_auditoria_ventas_detalle" ></table>
                        <div id="pager2"></div>
                    </div>
                </div>-->

    </center>
    <div id="info"  class="ventana" >
        <p id="notific">EXITO AL GUARDAR</p>
    </div>

    <div id="dialogIA"  class="ventana" >
        <div style="position: relative;">
            <table id="tabla_resultado_IA" ></table>
            <div id="pager2"></div>
        </div>
    </div>

    <div id="divSalidaEx" title="Exportacion" style=" display: block" >
        <p  id="msjEx" style=" display:  none"> Espere un momento por favor...</p>
        <center>
            <img id="imgloadEx" style="position: relative;  top: 7px; display: none " src="./images/cargandoCM.gif"/>
        </center>
        <div id="respEx" style=" display: none"></div>
    </div> 

</body>
</html>
