<%-- 
    Document   : aprobar_anticipos
    Created on : 3/07/2015, 08:50:19 AM
    Author     : egonzalez
--%>

<script type="text/javascript" src="./js/aprobar_anticipos.js"></script>

<div style="margin-top: 20px">
    <center>
        <table id="tbl_aprobar_anticipos"></table>
        <div id="page_aprobar_anticipo"></div>
    </center>
    <hr>
    <center>
        <button id="aceptar" class="ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only" >
            <span class="ui-button-text">Aceptar</span>
        </button>  
        <button id="anular" class="ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only" >
            <span class="ui-button-text">Anular</span>
        </button> 
    </center>
    <br>
</div>

<div id="dialogo4" class="ventana" title="Mensaje">
    <p  id="msj4" > </p>
</div>
