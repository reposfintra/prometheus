<%-- 
    Document   : edsPropietario
    Created on : 16/05/2015, 09:47:50 AM
    Author     : mariana
--%>

<%@page import="com.tsp.operation.model.beans.Usuario"%>
<%@page contentType="text/html" pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
        <link href="./css/popup.css" rel="stylesheet" type="text/css">
        <link type="text/css" rel="stylesheet" href="./css/jquery/jquery-ui/jquery-ui.css" />
        <link type="text/css" rel="stylesheet" href="./css/TransportadorasApi.css " />
        <script type="text/javascript" src="./js/jquery/jquery.jqGrid/js/jquery.min.js"></script>
        <script type="text/javascript" src="./js/jquery-ui-1.8.5.custom.min.js"></script>   
        <script type="text/javascript" src="./js/jquery/jquery.jqGrid-4.7.0/js/grid.locale-es.js"></script>
        <script type="text/javascript" src="./js/jquery/jquery.jqGrid-4.7.0/js/jquery.jqGrid.min.js"></script>
        <script type="text/javascript" src="./js/jquery/jquery.jqGrid-4.7.0/js/jquery.tabletojson.js"></script>    
        <script type="text/javascript" src="./js/EDS.js"></script> 
        <script type="text/javascript" href="http://www.ddginc-usa.com/spanish/editcheck.js"></script>
        <title>PROPIETARIO</title>
    </head>
    <body>

        <div id="capaSuperior" style="position:absolute; width:100%; height:100px; z-index:0; left: 0px; top: 0px;">
            <jsp:include page="/WEB-INF/toptsp.jsp?encabezado=PROPIETARIO "/>
        </div>

    <center>
        <%
            Usuario u = (Usuario) request.getSession().getAttribute("Usuario");
        %>
        <%-- --------------------------mostrar propietario Eds------------------------------%>
        <div style="position: relative;top: 150px;">
            <table id="tabla_cargar_propietario" ></table>
            <div id="pager"></div>
        </div>

        <%-- --------------------------Registro y edicion propietario----------------------------%>  

        <div id="dialogMsjPropietario"  class="ventana" >
            <div id="tablainterna" style="width: 580px" >
                <!--                <div id="encabezadotablita">
                                    <label class="titulotablita"><b>REGISTRO PROPIETARIO EDS</b></label>
                                </div>  -->
                <table id="tablainterna"  >
                    <tr>
                        <td>
                            <input type="text" id="id"  style="width: 50px"hidden ><!--hidden ="false"-->
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <input type="text" id="idusuarioedit"  style="width: 100px"hidden ><!--hidden ="false"-->
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <input type="text" id="idedit"  style="width: 100px" hidden><!--hidden ="false"-->
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <input type="text" id="codcli"  style="width: 50px" hidden><!--hidden ="false"-->
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <input type="text" id="operacion"  style="width: 50px"hidden ><!--hidden ="false"-->
                        </td>
                        <td>
                            <input type="text" id="existe"  style="width: 50px" hidden><!--hidden ="false"-->
                        </td>
                    </tr>
                    <%-- if (u.getTipo().equals("ADMINN")) { --%>
                    <tr>
                        <td colspan="2" >
                            <label> Razon social<span style="color:red;">*</span></label> 
                        </td>
                        <td colspan="3">
                            <input type="text" id="nombreprop" onchange="conMayusculas(this)" style="width: 298px"  />
                        </td>
                    </tr>  
                    <%-- }--%>
                    <tr>
                        <td colspan="2">
                            <label>Tipo Documento<span style="color:red;">*</span></label> 
                        </td>
                        <td colspan="2">
                            <select id="tipodoc" style="width: 125px"onchange="Vercampodv()">
                                <option value="">Seleccione</option>
                                <option value="CED">CEDULA</option>
                                <option value="NIT">NIT</option>
                            </select>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2" >
                            <label> Numero Documento<span style="color:red;">*</span></label> 
                        </td>
                        <td colspan="2" >
                            <input type="text"  id="nitem" style="width: 80px"  maxlength="10" class="solo-numero" onblur ="CalcularDv()" onchange="verificarUsuarioPropietario()" /> <%--onblur="verificarUsuarioPropietario()--%>
                        </td>
                        <td colspan="1" >
                            <label style="margin-left:-245px;color: rgb(167, 155, 155);"id="dv">-DV</label>
                        </td>
                        <td colspan="1" >
                            <input type="text" maxlength="1" id="digitover" style="width: 24px;margin-left: -222px;" class="solo-numero"   /> <%--onblur="verificarUsuarioPropietario()--%>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="4">
                            <hr>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2"  >
                            <label> Representante Legal<span style="color:red;">*</span></label> 
                        </td>
                        <td colspan="3">
                            <input type="text" id="representante" onchange="conMayusculas(this)" style="width: 298px"  />
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2">
                            <label>Numero Documento<span style="color:red;">*</span></label> 
                        </td>
                        <td colspan="3">
                            <input type="text" class="solo-numero" maxlength="10" id="nit" style="width: 298px"  />
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2" >
                            <label> Direccion<span style="color:red;">*</span></label> 
                        </td>
                        <td colspan="3">
                            <input type="text" id="direccion" onchange="conMayusculas(this)" style="width: 298px"  />
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2">
                            <label> Correo<span style="color:red;">*</span></label> 
                        </td>
                        <td colspan="3">
                            <input type="email" placeholder="Formato: algo@dir.dom" title="correo incorrecto ejemplo : ejemplo@correo.co" id="correo" onchange="conMayusculas(this)" style="width: 298px" />
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2">
                            <label> Telefono<span style="color:red;">*</span></label> 
                        </td>
                        <td colspan="3">
                            <input type="tel"  id="telefono" style="width: 298px" class="solo-numero" maxlength="10"   />
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <label>Estado Usuario</label> 
                        </td>
                        <td>
                            <select id="estadousu" style="width: 125px">
                                <option selected="" value="A">Activo </option>
                            </select>
                        </td>
                        <td>
                            <label>Pais<span style="color:red;">*</span></label> 
                        </td>
                        <td>
                            <select id="pais" style="width: 125px" onchange="cargarDepartamento(this.id)">
                            </select>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <label>Departamento<span style="color:red;">*</span></label> 
                        </td>
                        <td>
                            <select id="departamento" style="width: 125px" onchange="cargarCiudad(this.id)">
                            </select>
                        </td>
                        <td>
                            <label>Ciudad<span style="color:red;">*</span></label> 
                        </td>

                        <td>
                            <select id="ciudad" style="width: 125px" >
                            </select>
                        </td>
                    </tr>

                </table>
                <div id="encabezado2tablita">
                    <label class="titulo2tablita"><b>INFORMACION TRIBUTARIA</b></label>
                </div>
                <table id="tablainterna">
                    <tr>
                        <td >
                            <label>Tipo Persona</label> 
                        </td>
                        <td>
                            <select id="tipopersona" style="width: 92px">
                                <option value="PN">Natural</option>
                                <option value="PJ">Juridica</option>
                            </select>
                        </td>
                        <td >
                            <label>Gran Contribuyente</label> 
                        </td>
                        <td >
                            <select id="gcontribuyente" style="width: 50px">
                                <option value="S">SI</option>
                                <option value="N">NO</option>
                            </select>
                        </td>  
                    </tr>

                    <tr>
                        <td >
                            <label>Tipo Regimen</label> 
                        </td>
                        <td >
                            <select id="regimen" style="width: 92px">
                                <option value="C">Comun</option>
                                <option value="S">Simplificado</option>
                            </select>
                        </td>  
                        <td >
                            <label>Aplica Iva</label> 
                        </td>
                        <td  >
                            <select id="iva" style="width: 50px">
                                <option value="S">SI</option>
                                <option value="N">NO</option>
                            </select>
                        </td>  
                    </tr>
                    <tr>
                        <td>
                            <label>Aplica Retefuente</label> 
                        </td>
                        <td >
                            <select id="retefuente" style="width: 92px">
                                <option value="S">SI</option>
                                <option value="N">NO</option>
                            </select>
                        </td>  
                        <td >
                            <label>Aplica Ica</label> 
                        </td>
                        <td >
                            <select id="ica" style="width: 50px">
                                <option value="S">SI</option>
                                <option value="N">NO</option>
                            </select>
                        </td>  
                    </tr>
                    <tr>
                        <td >
                            <label>Autoretenedor</label> 
                        </td>
                        <td  >
                            <select id="autoretenedor" style="width: 92px">
                                <option value="S">SI</option>
                                <option value="N">NO</option>
                            </select>
                        </td>  
                    </tr>
                    <tr>
                        <td colspan="0">
                            <label>Handle Code<span style="color:red;">*</span></label> 
                        </td>
                        <td  colspan="3">
                            <select id="hc" style="width: 355px">
                            </select>
                        </td>  
                    </tr>
                </table>

                <div id="encabezado2tablita">
                    <label class="titulo2tablita"><b>INFORMACION DE PAGO</b></label>
                </div>
                <table id="tablainterna" >
                    <tr>
                        <td colspan="0" style="text-align: left;">
                            <label>Nombre de Cuenta<span style="color:red;">*</span></label> 
                        </td>
                        <td colspan="2">
                            <input type="text" id="nombre_cuenta" style="width: 250px" onchange="conMayusculas(this)"  />
                        </td>
                        <td></td>
                    </tr>
                    <tr>
                        <td colspan="0" style="text-align: left;">
                            <label>Cedula de Cuenta<span style="color:red;">*</span></label> 
                        </td>
                        <td colspan="2">
                            <input type="text" id="cedula_cuenta" style="width: 250px" class="solo-numero" maxlength="20" />
                        </td>
                        <td></td>
                    </tr>
                    <tr>
                        <td colspan="0" style="text-align: left;">
                            <label>No de Cuenta<span style="color:red;">*</span></label> 
                        </td>
                        <td colspan="2">
                            <input type="text" id="cuenta" style="width: 250px" class="solo-numero" maxlength="20" />
                        </td>
                        <td></td>
                    </tr>
                    <tr>
                        <td >
                            <label>Banco</label> 
                        </td>
                        <td >
                            <select id="banco" style="width: 156px" onchange="cargarAgencia(this.id)" >
                            </select>
                        </td>
                        <td >
                            <label>Sede Pago</label> 
                        </td>
                        <td  >
                            <select id="sedepago" style="width: 118px" >
                            </select>
                        </td>
                    </tr>

                    <tr>
                        <td >
                            <label>Agencia</label> 
                        </td>
                        <td  >
                            <select id="banagencia" style="width: 156px" >
                            </select>
                        </td>
                        <td >
                            <label>Tipo de Cuenta</label> 
                        </td>
                        <td  >
                            <select id="tcuenta" style="width: 118px">
                                <option value="CC">Cuenta Corriente</option>
                                <option value="CA">Cuenta de Ahorro</option>
                            </select>
                        </td>
                    </tr>

                    <tr>
                        <td colspan="0">
                            <label>Pago Transferencia</label> 
                        </td>
                        <td colspan="2">
                            <input type="checkbox" id="ptransfer"  onchange="validarcheck()"/>   <!--value="true"--> 
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <label>Banco Transferencia</label> 
                        </td>
                        <td>
                            <select id="bancotransfer" style="width: 156px">
                            </select>
                        </td>
                        <td>
                            <label>Sucursal</label> 
                        </td>
                        <td>
                            <select id="sucursal" style="width: 118px" >
                            </select>
                        </td>
                    </tr>
                    <!--                    <tr>
                                            <td colspan="1">
                                                <button type="button" id="addcuenta" title="Registra nuevas cuentas al propietario"onclick="ventanaAddCuenta()">Gestion Cuentas</button>
                                            </td>
                    
                                        </tr>-->
                </table>

                <div id="encabezado2tablita">
                    <label class="titulo2tablita"><b>ACCESO AL SISTEMA</b></label>
                </div>
                <table id="tablainterna">

                    <tr>
                        <td>
                            <label> ID del usuario<span style="color:red;">*</span></label> 
                        </td>
                        <td>
                            <input type="text" id="idusu" maxlength="10" onchange="conMayusculas(this)" onblur="verificarUsuario()" style="width: 108px" />   
                        </td>
                        <td>
                            <label>Cambiar Clave al Inicio</label> 
                        </td>
                        <td>
                            <input type="checkbox" id="actdes" />   
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <label> Password<span style="color:red;">*</span></label> 
                        </td>
                        <td>
                            <input type="password" id="password" maxlength="15" style="width: 108px" />   
                        </td>
                        <td>
                            <label> Confirmar Password<span style="color:red;">*</span></label> 
                        </td>
                        <td>
                            <input type="password" id="password2" maxlength="10" onkeyup="validarpass()" style="width: 108px" />   
                        </td>
                    </tr>

                </table>
                <center>
                    <p id="dialogMsjSMS"  ></p>  
                    <p id="sms2"  ></p> 
                </center>
                <!--            <div id ='botones'>
                                <button id="guardarprop" class="ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only" 
                                        role="button" aria-disabled="false" >
                                    <span class="ui-button-text">Guardar</span>
                                </button> 
                
                                <button id="salir" class="ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only" 
                                        role="button" aria-disabled="false" onclick="">
                                    <span class="ui-button-text">Salir</span>
                                </button> 
                            </div>-->
                <div id="mensaje"  class="ventana" >
                    <p></p>
                </div>
                <div id="info"  class="ventana" >
                    <p id="notific">EXITO AL GUARDAR</p>
                </div>
            </div>
        </div>

        <!--    <div id="dialogMsjAddcuenta"  class="ventana" >
                <div id="tablainterna" style="width: 330px" >
                    <div id="encabezadotablita">
                        <label class="titulotablita"><b>REGISTRO CUENTAS</b></label>
                    </div> 
                    <table id="tablainterna"  >
                        <tr>
                            <td>
                                <label>Numero de Cuenta</label> 
                            </td>
                            <td>
                                <input type="text" id="addnumcuenta" maxlength="15" onchange="conMayusculas(this)" style="width: 150px"  />
                            </td>
                        </tr>
                        <tr>
                            <td >
                                <label>Tipo de Cuenta</label> 
                            </td>
                            <td  >
                                <select id="addcuenta" style="width: 156px">
                                    <option value="CC">Cuenta Corriente</option>
                                    <option value="CA">Cuenta de Ahorro</option>
                                </select>
                            </td>
                        </tr>
                        <tr>
                            <td >
                                <label>Banco</label> 
                            </td>
                            <td >
                                <select id="addbanco" style="width: 156px" onchange="cargarAgencia(this.id)" >
                                </select>
                            </td>
                        </tr>
                        <tr>
                        <tr>
                            <td >
                                <label>Agencia</label> 
                            </td>
                            <td  >
                                <select id="addbanagencia" style="width: 156px" >
                                </select>
                            </td>
                        </tr>
                        <tr>
                            <td >
                                <label>Sede Pago</label> 
                            </td>
                            <td  >
                                <select id="sedepago" style="width: 156px" >
                                </select>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="0">
                                <label>Pago Transferencia</label> 
                            </td>
                            <td colspan="2">
                                <input type="checkbox" id="ptransfer"  onchange="validarcheck()"/>   value="true" 
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <label>Banco Transferencia</label> 
                            </td>
                            <td>
                                <select id="bancotransfer" style="width: 156px">
                                </select>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <label>Sucursal</label> 
                            </td>
                            <td>
                                <select id="sucursal" style="width: 156px" >
                                </select>
                            </td>
                        </tr>
        
                    </table>
                </div>
            </div>-->
        <div id="dialogo2" class="ventana">
            <p id="msj2">texto </p> <br/>
            <center>
                <img src="./images/cargandoCM.gif"/>
            </center>
        </div>
    </center>

</body>
<script>
    $(document).ready(function () {
        $('.solo-numero').keyup(function () {
            this.value = (this.value + '').replace(/[^0-9]/g, '');
        });
    });

    init1();
</script>
</html>
