<%-- 
    Document   : maestroBanderasEds
    Created on : 12/06/2015, 03:23:51 PM
    Author     : mariana
--%>

<%@page contentType="text/html" pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
        <link href="./css/popup.css" rel="stylesheet" type="text/css">
        <link type="text/css" rel="stylesheet" href="./css/jquery/jquery-ui/jquery-ui.css" />
        <link type="text/css" rel="stylesheet" href="./css/TransportadorasApi.css " />
        <script type="text/javascript" src="./js/jquery/jquery.jqGrid/js/jquery.min.js"></script>
        <script type="text/javascript" src="./js/jquery-ui-1.8.5.custom.min.js"></script>   
        <script type="text/javascript" src="./js/jquery/jquery.jqGrid-4.7.0/js/grid.locale-es.js"></script>
        <script type="text/javascript" src="./js/jquery/jquery.jqGrid-4.7.0/js/jquery.jqGrid.min.js"></script>
        <script type="text/javascript" src="./js/jquery/jquery.jqGrid-4.7.0/js/jquery.tabletojson.js"></script>    
        <script type="text/javascript" src="./js/EDS.js"></script> 
        <script type="text/javascript" href="http://www.ddginc-usa.com/spanish/editcheck.js"></script>
        <title>MAESTRO BANDERAS EDS</title>
    </head>
    <body>
        <div id="capaSuperior" style="position:absolute; width:100%; height:100px; z-index:0; left: 0px; top: 0px;">
            <jsp:include page="/WEB-INF/toptsp.jsp?encabezado=BANDERAS EDS "/>
        </div>
    <center>

        <div style="position: relative;top: 150px;">
            <table id="tabla_banderas" ></table>
            <div id="pager"></div>

        </div>
        <%-- --------------------------Registro Banderas----------------------------%>
        <div id="dialogMsjbandera"  class="ventana" >
            <div id="tablita" style="top: 0px;width: 435px;" >
                <div id="encabezadotablita">
                    <label class="titulotablita"><b>REGISTRO DE BANDERAS</b></label>
                </div> 
                <table id="tablainterna"  >
                    <tr>
                        <td>
                            <input type="text" id="codbandera"  style="width: 100px" hidden><!--hidden ="false"-->
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2">
                            <label id="labelprop">Razon social<span style="color:red;">*</span></label>
                        </td>
                        <td colspan="2">
                            <input type="text" id="razonsocial" onchange="conMayusculas(this)" style="width: 250px"  />  
                        </td>
                    </tr>
                    <tr>

                    <tr>
                        <td colspan="2">
                            <label id="labelprop">Nit<span style="color:red;">*</span></label>
                        </td>
                        <td colspan="2">
                            <input type="text" id="nit"  maxlength="10" class="solo-numero" style="width: 250px"  />  
                        </td>
                    </tr>

                    <tr>
                        <td colspan="2">
                            <label>Tipo Persona </label> 
                        </td>
                        <td  colspan="2">
                            <select id="tipper" style="width: 117px">
                                <option selected="" value="PJ">Persona Juridica </option>
                                <option selected="" value="PN">Persona Natural </option>
                            </select>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2">
                            <label>Direccion<span style="color:red;">*</span></label> 
                        </td>
                        <td colspan="2">
                            <input type="text" id="direccion" onchange="conMayusculas(this)" style="width: 250px"  />  
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2">
                            <label>Correo Electronico</label> 
                        </td>
                        <td colspan="2">
                            <input placeholder="Formato: algo@dir.dom"  type="email" id="correo" onchange="conMayusculas(this)" style="width: 250px"  />
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2">
                            <label>Representante Legal<span style="color:red;">*</span></label> 
                        </td>
                        <td colspan="2">
                            <input type="text" id="replegal" onchange="conMayusculas(this)" style="width: 250px"  />  
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2">
                            <label>Cc Representante Legal<span style="color:red;">*</span></label> 
                        </td>
                        <td colspan="2">
                            <input type="text" id="docreplegal"  maxlength="10" onchange="conMayusculas(this)" style="width: 250px"  />  
                        </td>
                    </tr>
                </table>

                <center>
                    <p id="dialogMsjSMS"  ></p>  
                    <p id="sms2"></p> 
                </center>

            </div>
        </div>

        <div id="info"  class="ventana" >
            <p id="notific" >EXITO AL GUARDAR</p>
        </div>
    </center>

</body>

<script>
    $(document).ready(function () {
        $('.solo-numero').keyup(function () {
            this.value = (this.value + '').replace(/[^0-9]/g, '');
        });
    });
    init5();
</script>

</html>
