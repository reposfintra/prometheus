<%-- 
    Document   : registroVentasLoginEDS
    Created on : 12/08/2015, 05:09:53 PM
    Author     : mcastillo
--%>


<%@page contentType="text/html" pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
        <title>Registro de ventas EDS</title>
        <link href="./css/simple-sidebar.css" rel="stylesheet" type="text/css"/>       
        <link href="./css/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css"/>
        <link type="text/css" rel="stylesheet" href="./css/jquery/jquery-ui/jquery-ui-2.css" />
        <link href="./css/ventas_eds.css" rel="stylesheet" type="text/css"> 
        <link href="./css/popup.css" rel="stylesheet" type="text/css">
        <script type="text/javascript" src="./js/jquery/jquery.jqGrid/js/jquery.min.js"></script>
        <script type="text/javascript" src="./js/jquery-ui-1.8.5.custom.min.js"></script>
        <script src="./js/MenuVentasEds.js" type="text/javascript"></script>
        <script type="text/javascript" src="./js/configPreciosProdEds.js"></script>
        <script type="text/javascript" src="./js/registroVentasLogin.js"></script>
        
        <!--jqgrid--> 
        <script type="text/javascript" src="./js/jquery/jquery.jqGrid/js/grid.locale-es.js"></script>
        <script type="text/javascript" src="./js/jquery/jquery.jqGrid/js/jquery.jqGrid.min.js"></script>
<!--        <script type="text/javascript" src="./js/jquery/jquery.jqGrid-4.7.0/js/grid.locale-es.js"></script>
        <script type="text/javascript" src="./js/jquery/jquery.jqGrid-4.7.0/js/jquery.jqGrid.min.js"></script>
        <script type="text/javascript" src="./js/jquery/jquery.jqGrid-4.7.0/js/jquery.tabletojson.js"></script>   -->
        
    </head>
    <body>

        <div id="capaCentral" style="position:absolute; width:100%; height:100%; z-index:0; left: 0px; top: 0px; ">
            
            <div id="wrapper" style="display: none;">

                <!-- Sidebar -->
                <div id="sidebar-wrapper">
                    <ul id="menu-pagina" class="sidebar-nav">
                    </ul>
                </div>
                <!-- /#sidebar-wrapper -->

                <!-- Page Content -->
                <div id="page-content-wrapper" >                 

                    <button id="menu-toggle" type="button" class="btn btn-default btn-lg">
                        <span class="glyphicon glyphicon-menu-hamburger" aria-hidden="true"></span>
                    </button>    
                    <div class="container-fluid">
                        <div class="row">
                            <div class="col-lg-12">
                                <div id="container">
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- /#page-content-wrapper -->

            </div>
            
           
                <br>  
               <div id="div_inicioSesion" class="login" title="INICIAR SESION" >
                    <center>
                    <h1>Login estaci�n de Servicio</h1>
                    <table id="tbl_eds_login">

                        <tr>
                            <td class="td">
                                <label> Usuario </label> 
                            </td>
                            <td class="td">
                                <input type="text" id="idusuario" maxlength="10" class="inpt" />   
                            </td>                        
                        </tr>
                        <tr>
                            <td class="td">
                                <label> Password</label> 
                            </td>
                            <td class="td">
                                <input type="password" id="password" maxlength="15" class="inpt" />   
                            </td>                       
                        </tr>
                    </table>
                    <br>        
                    <button id="loginEds" class="ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only" 
                            role="button" aria-disabled="false">
                        <span class="ui-button-text">Iniciar sesi�n</span>
                    </button>
                    </center>
                </div>  
     
                        
            <div id="div_configIniPrecioProd" title="PRE-CONFIGURACION DE PRECIOS PRODUCTOS" style="display: none; width: 800px" >                
             
                    </br>
                    <table id="tbl_config_ini_prices" class="tablas" align="center" style=" width: 100%" >
                       
                    </table>   
                </br> 
            </div>  
                
            <!-- Dialogo ventana detalle venta x dia> -->
            <div id="dialogTransxdiaDet" title="Detalle de venta" style="display:none;">       
                <center><table id="tbl_det_trans_xdia" class="tablas" style=" width: 95%"></table></center>
            </div>
            
            <div id="dialogMsj" title="Mensaje" style="display:none;">
                <p style="font-size: 12px;text-align:justify;" id="msj" > Texto </p>
            </div>  
            
        </div>

    </body>
</html>