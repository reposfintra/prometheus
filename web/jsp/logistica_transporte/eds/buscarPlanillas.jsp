<%-- 
    Document   : buscarPlanillas
    Created on : 14/08/2015, 11:34:10 AM
    Author     : mcastillo
--%>
 <script type="text/javascript" src="./js/buscarPlanillasEds.js"></script> 
 <div id="capaCentral" align="left" style="position:absolute; width:100%; height:100%; z-index:0; top: 0px;" >
    <center>
    <h4 style="width:1150px">VENTAS ESTACION DE SERVICIO</h4>
    <div id="search_planilla">
        <table id="tbl_search_planilla" > 
        <tr>
            <td class="td">
                <label>Transportadora</label> 
            </td>
            <td class="td">
                <select id="transportadora" class="select" ></select>
            </td>
            <td class="td">
                <label>No Planilla</label> 
            </td>
            <td class="td">
                <input type="text" id="numplanilla" class="inpt">
            </td>  
            <td class="td">
                <label>C.C. Conductor</label> 
            </td>
            <td class="td">
                <input type="text" id="idconductor" class="solo-numero inpt" maxlength="10" >
            </td>  
            <td class="td">
                <label>Placa</label> 
            </td>
            <td class="td">
                <input type="text" id="numplaca" class="inpt" maxlength="10">
            </td>  
            <td class="td" align="center">
                <button id="btn_search_planillas" class="ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only" >
                    <span class="ui-button-text">Buscar</span>
                </button>
            </td>   
        </tr>          
    </table>
  </div>
    <br>
    <div id="div_enc_planilla" style="display:none;">
        <table id="tbl_enc_planilla" class="tbl_planilla">
            <thead>
               <th class="ui-state-default titulo" colspan="4">PROCESO DE VENTA</th>
            </thead>
            <tr>              
                <input type="hidden" id="ideds" name="ideds" readOnly>     
                <input type="hidden" id="idmanifiesto" name="idmanifiesto" readOnly>     
                <input type="hidden" id="planilla" name="planilla" readOnly>     
                <td width="32%">
                    <label>Estacion de Servicio</label> 
                </td>
                <td>
                   <input type="text" id="nombre_eds" style="border:none; box-shadow: none;width:auto;" class="inpt" readOnly>
                </td>
            </tr> 
                 <tr>
                <td>
                    <label>Anticipo</label> 
                </td>
                <td>
                   <input type="text" id="anticipo" style="border:none; box-shadow: none;" class="inpt" readOnly>
                </td>
            </tr> 
            <tr>
                <td>
                    <label>Disponible</label> 
                </td>
                <td>
                    <input type="text" id="disponible" style="border:none; box-shadow: none;" class="inpt" readOnly>
                </td>
            </tr> 
            <tr>
                <td >
                    <label>Kilometraje</label> 
                </td>
                <td>
                   <input type="text" id="kilometraje" class="inpt solo-numero"><span> Km</span>
                </td>
            </tr> 
        </table>
        <br>
        <table id="tabla_info_venta"></table>
        <div id="page_tabla_info_venta"></div><br> 
        <button id="btn_save_info_venta" class="ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only" 
                role="button" aria-disabled="false">
            <span class="ui-button-text">Guardar</span>
        </button>
    </div>

   
    </center>
</div>