<%-- 
    Document   : ConfigProductosEDS
    Created on : 19/06/2015, 04:11:33 PM
    Author     : egonzalez
--%>

<%@page contentType = "text/html" pageEncoding = "ISO-8859-1" session = "true"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
        <title>Gestion de productos EDS</title>


        <link type="text/css" rel="stylesheet" href="/fintra/css/jquery/jquery-ui/jquery-ui.css"/>

        <link href="./css/popup.css" rel="stylesheet" type="text/css">
        <link rel="stylesheet" type="text/css" href="/fintra/css/jCal/jscal2.css" />
        <link type="text/css" rel="stylesheet" href="./css/TransportadorasApi.css " />
        <script type="text/javascript" src="/fintra/js/jquery/jquery.jqGrid/js/jquery.min.js"></script>
        <script type="text/javascript" src="/fintra/js/jquery-ui-1.8.5.custom.min.js"></script>
        <script type="text/javascript" src="/fintra/js/jquery/jquery.jqGrid/js/grid.locale-es.js"></script>
        <script type="text/javascript" src="/fintra/js/jquery/jquery.jqGrid/js/jquery.jqGrid.min.js"></script>
        <script type="text/javascript" src="./js/jquery/jquery.jqGrid-4.7.0/plugins/jquery.contextmenu.js"></script>

        <script type="text/javascript" src="/fintra/js/jCal/jscal2.js"></script>
        <script type="text/javascript" src="/fintra/js/jCal/lang/es.js"></script>
        <script type="text/javascript" src="/fintra/js/ConfigProductosEDS.js"></script>
        <link type="text/css" rel="stylesheet" href="./css/jquery/jquery-ui/jquery-ui.css" />

        <style type="text/css">
            .ui-jqgrid .ui-jqgrid-htable th div
            {
                height: auto;
                overflow: hidden;
                padding-right: 4px;
                font-size:11px; 
                padding-top: 2px;
                position: relative;
                vertical-align: text-top;
                white-space: normal !important;
            }
        </style>

    </head>
    <body>
        <div id="capaSuperior" style="position:absolute; width:100%; height:100px; z-index:0; left: 0px; top: 0px;">
            <jsp:include page="/WEB-INF/toptsp.jsp?encabezado=Gestion de productos EDS"/>
        </div>
        <div id="capaCentral" style="position:absolute; width:100%; height:85%; z-index:0; left: 0px; top: 100px; overflow: auto;" align="center">
            <center>
                <br>
                <div class="ui-jqgrid ui-widget ui-widget-content ui-corner-all" style="max-width: 407px; height: 130px;">

                    <div class="ui-dialog-titlebar ui-widget-header ui-corner-all ui-helper-clearfix">
                        <span class="ui-dialog-title" id="ui-dialog-title-listaGlobal">
                            &nbsp;
                            <label class="titulotablita"><b>FILTRO DE BUSQUEDA</b></label>
                        </span>
                    </div>
                    <table style=" padding: 0.5em 1em 0.5em 1em" style="width: 95%;">
                        <tr>
                            <td>Propietarios Estaciones</td>
                            <td>
                                <select id="propietarios" onchange="cargarCombo('eds', [this.value])" style="width: 264px;">
                                    <option value=''>...</option>
                                </select>
                            </td>
                        </tr>
                        <tr>
                            <td>Estaciones de Servicio</td>
                            <td>
                                <select id="eds"  style="width: 264px;" >
                                    <option value=''>...</option>
                                </select>
                            </td>
                        </tr>
                    </table>
                    <hr>
                    <div id ='botones'>
                        <button id="buscar" class="ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only" 
                                role="button" aria-disabled="false" onclick="">
                            <span class="ui-button-text">Buscar</span>
                        </button> 
                    </div>
                </div>
                <br>
                <div>
                    <table id="tabla_productos"></table>
                    <div id="page_productos"></div>
                </div>
            </center>
        </div>
        <div id="info" class="ventana"style="display: none">
            <p id="notific">Espere un momento por favor... </p> <br/>
        </div>
        <div id="menu" class="ContextMenu"style="">
            <ul id="listopciones">
                <li id="eliminar" >
                    <span style="font-family:Verdan">Eliminar fila</span>
                </li>
            </ul>
        </div>
        <div id="dialogConfigRangos"  class="ventana" >
            <div id="tablita" style="top: 0px;width: 523px;" >
                <div id="encabezadotablita">
                    <label class="titulotablita"><b>Agregar comisiones por galones vendidos</b></label>
                </div> 
                <input type="text" id="id_config" name="id_config" hidden>
                <table id="tablainterna"  >
                    <!--                    <tr>
                                            <td>
                                                <label>Galones Iniciales</label>
                                            </td>
                                            <td></td>
                                            <td>
                                                <label>Galones Finales</label>
                                            </td>
                                            <td></td>
                                            <td>
                                                <label>Valor Descuento</label>
                                            </td>
                                            <td></td>
                                            <td>
                                                <label style="margin-left: 10px;">% Descuento</label>
                                            </td>
                    
                                        </tr>
                                        <tr>
                                            <td>
                                                <input type="text" id="rangoini_" name="rangoini" style="margin-left: 9px;"class="solo-numero" maxlength="5">
                                            </td>
                                            <td>
                                                <label style="margin-left: -10px;">A</label>
                                            </td>
                                            <td>
                                                <input type="text" id="rangofin_" name="rangofin" style="margin-left: 9px;" class="solo-numero" maxlength="5">
                                            </td>
                                            <td>
                                                <label>$</label>
                                            </td>
                                            <td>
                                                <input type="text" id="vlrdes_" name="vlrdes" style="margin-left: 9px;" maxlength="10">
                                            </td>
                                            <td></td>
                                            <td>
                                                <input type="text" id="desc_" name="desc" style="margin-left: 9px;" maxlength="10">
                                            </td>
                                            <td>
                                                <img id="add_" src="/fintra/images/botones/iconos/adds.png" 
                                                     style="margin-left: 5px; height: 100%; vertical-align: middle;" onclick="addElemento()"> 
                                            </td>
                                            <td>
                                                <img id="delete_" src="/fintra/images/botones/iconos/delete.png" 
                                                     style="margin-left: -4px; height: 100%; vertical-align: middle;"onclick="deleteElemento()"> 
                                            </td>
                                        </tr>-->

                </table>
            </div>
        </div>
   <div id="dialogHistorico"  class="ventana" >
            <div style="position: relative;">
                <table id="tabla_historico" ></table>
                <div id="pager1"></div>
            </div>
        </div>
        <script>
            $(document).ready(function () {
                $('.solo-numero').keyup(function () {
                    this.value = (this.value + '').replace(/[^0-9]/g, '');
                });
            });
            init();</script>
    </body>
</html>
