<%-- 
    Document   : maestroUnidadMedida
    Created on : 24/06/2015, 10:51:51 AM
    Author     : mariana
--%>

<%@page contentType="text/html" pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
        <link href="./css/popup.css" rel="stylesheet" type="text/css">
        <link type="text/css" rel="stylesheet" href="./css/jquery/jquery-ui/jquery-ui.css" />
        <link type="text/css" rel="stylesheet" href="./css/TransportadorasApi.css " />
        <script type="text/javascript" src="./js/jquery/jquery.jqGrid/js/jquery.min.js"></script>
        <script type="text/javascript" src="./js/jquery-ui-1.8.5.custom.min.js"></script>   
        <script type="text/javascript" src="./js/jquery/jquery.jqGrid-4.7.0/js/grid.locale-es.js"></script>
        <script type="text/javascript" src="./js/jquery/jquery.jqGrid-4.7.0/js/jquery.jqGrid.min.js"></script>
        <script type="text/javascript" src="./js/jquery/jquery.jqGrid-4.7.0/js/jquery.tabletojson.js"></script>    
        <script type="text/javascript" src="./js/EDS.js"></script> 
        <script type="text/javascript" href="http://www.ddginc-usa.com/spanish/editcheck.js"></script>
        <title>MAESTRO UNIDADES DE MEDIDA</title>
    </head>
    <body>
        <div id="capaSuperior" style="position:absolute; width:100%; height:100px; z-index:0; left: 0px; top: 0px;">
            <jsp:include page="/WEB-INF/toptsp.jsp?encabezado=PRODUCTOS EDS "/>
        </div>

    <center>

        <div style="position: relative;top: 150px;">
            <table id="tabla_unidadmedida" ></table>
            <div id="pager"></div>

        </div>
        <%-- --------------------------Registro Productos----------------------------%>
        <div id="dialogMsjMedicion"  class="ventana" >
            <div id="tablita" style="top: 0px;width: 435px;" >
                <div id="encabezadotablita">
                    <label class="titulotablita"><b>UNIDADES DE MEDIDA</b></label>
                </div> 
                <table id="tablainterna"  >
                    <tr>
                        <td>
                            <input type="text" id="idum"  style="width: 50px" hidden >
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <label>Nombre Unidad Medida</label>
                        </td>
                        <td>
                            <input type="text" id="nomunmed" onchange="conMayusculas(this)" style="width: 200px"  >
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <label>Nombre Unidad de Medicion</label>
                        </td>
                        <td>
                            <input type="text" id="medicion" class="solo-numero" style="width: 200px"  >
                        </td>
                    </tr>
                </table>

            </div>
        </div>


        <div id="info"  class="ventana" >
            <p id="notific" >EXITO AL GUARDAR</p>
        </div>
    </center>
</body>
<script>
    $(document).ready(function () {
        $('.solo-numero').keyup(function () {

            this.value = (this.value + '').replace(/[^0-9]/g, '');
        });
    });
    init6();
</script>
</html>
