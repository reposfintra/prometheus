<%-- 
    Document   : extracto_propietario
    Created on : 21/06/2015, 09:34:33 AM
    Author     : egonzalez
--%>

<%@page contentType="text/html" pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
        <title>Extracto propietario</title>
        <link href="./css/reporte_produccion.css" rel="stylesheet" type="text/css" >
        <link type="text/css" rel="stylesheet" href="./css/jquery/jquery-ui/jquery-ui.css" />
        <link href="./css/popup.css" rel="stylesheet" type="text/css">
        <script type="text/javascript" src="./js/jquery/jquery.jqGrid/js/jquery.min.js"></script>
        <script type="text/javascript" src="./js/jquery-ui-1.8.5.custom.min.js"></script>
        <script type="text/javascript" src="./js/jquery/jquery.jqGrid-4.7.0/js/grid.locale-es.js"></script>
        <script type="text/javascript" src="./js/jquery/jquery.jqGrid-4.7.0/js/jquery.jqGrid.min.js"></script>
        <script type="text/javascript" src="./js/jquery/jquery.jqGrid-4.7.0/js/jquery.tabletojson.js"></script>      
        <script type="text/javascript" src="./js/extracto_propietario.js"></script>
    </head>
    <body>
        <div id="capaSuperior" style="position:absolute; width:100%; height:100px; z-index:0; left: 0px; top: 0px;">
            <jsp:include page="/WEB-INF/toptsp.jsp?encabezado=EXTRACTO PROPIETARIO"/>
        </div>
        <div id="capaCentral" style="position:absolute; width:100%; height:100%; z-index:0; left: 0px; top: 100px; ">
            <div id="filtro_extracto_propietario">
                <table border="0"cellpadding="1" class="tablaBusquedaExtracto">           
                    <tbody>
                    <thead>
                    <th class="ui-state-default titulo" colspan="8">Filtro  Busqueda</th>
                    </thead>
                    <tr>
                        <td>Propietario</td>
                        <td>  
                            <select class="input" id="propietario" name="propietario">
                            </select>
                        </td>
                        <td>Placa</td>
                        <td> 
                            <select class="input selectplaca" id="placa" name="placa">
                                <option value=""></option>
                            </select>
                        </td>
                    </tr>
                    <tr>

                        <td>Planilla</td>
                        <td> <input type="text" id="planilla" class="inpt widhTd"></td>

                        <td>Fecha Inicial</td>
                        <td> <input type="text" id="fecha_inicio" class="inpt" readonly></td>
                        <td>Fecha Final</td>
                        <td>  <input type="text" id="fecha_fin" class="inpt" readonly> </td>
                    </tr>
                    </tbody>
                </table>
                <hr>
                <button id="buscar" class="ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only" >
                    <span class="ui-button-text">Buscar</span>
                </button>
                <button id="exportar" class="ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only" >
                    <span class="ui-button-text">Exportar</span>
                </button>

            </div>
            <br>
            <center>

                <table  id="tbl_report_owner" class="scroll" >
                    <thead  class="ui-state-default">
                    <th>#</th>    
                    <th>Planilla</th>
                    <th>Placa</th>
                    <th>Nombre conductor</th>
                    <th>Cedula</th>
                    <th>Valor anticipo</th>
                    <th>Descuento</th>
                    <th>Valor neto </th>
                    <th>Consecutivo Venta</th>
                    <th>Fecha Venta</th>
                    <th>Hora</th>
                    <th>Bandera</th>
                    <th>Nombre eds</th>
                    <th>Kilometraje</th>
                    <th>Producto</th>
                    <th>Precio unitario</th>
                    <th>Unidades</th>
                    <th>Valor venta</th>
                    <th>Disponible</th>
                    </thead>
                    <tbody id="rowsdelegate" >
                    </tbody>
                    <tfoot  class="ui-state-default" id="foottbl">
                    <th colspan="19"></th>
                    </tfoot>
                </table>                 

            </center>
        </div>

        <div id="dialogo" class="ventana" title="Mensaje">
            <p  id="msj">texto </p>
        </div>

        <div id="divSalidaEx" title="Exportacion" style=" display: block" >
            <p  id="msjEx" style=" display:  none"> Espere un momento por favor...</p>
            <center><img id="imgloadEx" style="position: relative;  top: 7px; display: none " src="./images/cargandoCM.gif"/></center>
            <div id="respEx" style=" display: none"></div>
        </div> 
    </body>
</html>
