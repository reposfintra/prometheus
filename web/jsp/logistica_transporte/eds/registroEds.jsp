<%-- 
    Document   : registroEds
    Created on : 16/05/2015, 01:30:30 PM
    Author     : mariana
--%>

<%@page contentType="text/html" pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
        <link href="./css/popup.css" rel="stylesheet" type="text/css">
        <link type="text/css" rel="stylesheet" href="./css/jquery/jquery-ui/jquery-ui.css" />
        <link type="text/css" rel="stylesheet" href="./css/TransportadorasApi.css " />
        <script type="text/javascript" src="./js/jquery/jquery.jqGrid/js/jquery.min.js"></script>
        <script type="text/javascript" src="./js/jquery-ui-1.8.5.custom.min.js"></script>   
        <script type="text/javascript" src="./js/jquery/jquery.jqGrid-4.7.0/js/grid.locale-es.js"></script>
        <script type="text/javascript" src="./js/jquery/jquery.jqGrid-4.7.0/js/jquery.jqGrid.min.js"></script>
        <script type="text/javascript" src="./js/jquery/jquery.jqGrid-4.7.0/js/jquery.tabletojson.js"></script>    
        <script type="text/javascript" src="./js/EDS.js"></script> 
        <title>EDS</title>
    </head>
    <body>
        <div id="capaSuperior" style="position:absolute; width:100%; height:100px; z-index:0; left: 0px; top: 0px;">
            <jsp:include page="/WEB-INF/toptsp.jsp?encabezado=GESTION ESTACIONES DE SERVICIOS "/>
        </div>
    <center>

        <div style="position: relative;top: 150px;">
            <table id="tabla_Eds" ></table>
            <div id="pager"></div>

        </div>
        <%-- --------------------------Registro Eds----------------------------%>
        <div id="dialogMsjEds"  class="ventana" >
            <div id="tablita" style="top: 0px;width: 535px;" >
                <div id="encabezadotablita">
                    <label class="titulotablita"><b>REGISTRO ESTACION DE SERVICIO</b></label>
                </div> 
                <table id="tablainterna"  >
                    <tr>
                        <td colspan="2">
                            <label id="labelprop">Propietario</label>
                        </td>
                        <td colspan="2">
                            <%--  <input type="text" id="propietario"  readonly style="width: 250px"  />  --%>
                            <select id ="idpropietario" ></select>
                        </td>
                    </tr>
                    <tr>
                    <tr>
                        <td colspan="2">
                            <label id="labelban">Banderas</label> 
                        </td>
                        <td  colspan="2">
                            <select id="idbandera" style="width: 156px" >
                            </select>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <input type="text" id="ideds"  style="width: 50px" hidden><!--hidden ="false"-->
                        </td>

                    </tr>
                    <tr>
                        <td>
                            <input type="text" id="idusuarioedit"  style="width: 100px"hidden ><!--hidden ="false"-->
                        </td>

                    </tr>
                    <tr>
                        <td colspan="2">
                            <label>Nombre de Estacion de Servicio<span style="color:red;">*</span></label> 
                        </td>
                        <td colspan="2">
                            <input type="text" id="nombreds" onchange="conMayusculas(this)" style="width: 250px"  />  
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2">
                            <label>Nit de Estacion de Servicio<span style="color:red;">*</span></label> 
                        </td>
                        <td colspan="2">
                            <input type="text" id="nitestacion" class="solo-numero"  maxlength="10" style="width: 250px"  />
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2">
                            <label>Direccion<span style="color:red;">*</span></label> 
                        </td>
                        <td colspan="2">
                            <input type="text" id="direccioneds" onchange="conMayusculas(this)" style="width: 250px"  />
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2">
                            <label>Correo Electronico<span style="color:red;">*</span></label> 
                        </td>
                        <td colspan="2">
                            <input placeholder="Formato: algo@dir.dom" title="correo incorrecto ejemplo : ejemplo@correo.co" type="email" id="correo" onchange="conMayusculas(this)" style="width: 250px"  />
                        </td>

                    </tr>
                    <tr>
                        <td colspan="2">
                            <label>Telefono de Contacto<span style="color:red;">*</span></label> 
                        </td>
                        <td colspan="2">
                            <input type="tel" id="telefonoeds" class="solo-numero" style="width: 150px" maxlength="10"  />
                        </td>
                    </tr>
<!--                    <tr>
                        <td colspan="2">
                            <label>Dias pago factura<span style="color:red;">*</span></label> 
                        </td>
                        <td colspan="2">
                            <input type="text" id="dpagof" class="solo-numero" style="width: 150px" maxlength="3"  />
                        </td>
                    </tr>-->
                    <tr>
                        <td colspan="2">
                            <label>Pais<span style="color:red;">*</span></label> 
                        </td>
                        <td colspan="2">
                            <select id="pais" style="width: 157px" onchange="cargarDepartamento(this.id)">
                            </select>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2">
                            <label>Departamento<span style="color:red;">*</span></label> 
                        </td>
                        <td colspan="2">
                            <select id="departamento" style="width: 157px" onchange="cargarCiudad(this.id)">
                            </select>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2">
                            <label>Ciudad<span style="color:red;">*</span></label> 
                        </td>

                        <td colspan="2">
                            <select id="ciudad" style="width: 157px" >
                            </select>
                        </td>
                    </tr>
                </table>

                <div id="encabezado2tablita">
                    <label class="titulo2tablita"><b>INFORMACION CONEXION APPMOVIL</b></label>
                </div> 

                <table id="tablainterna"  >
                    <tr>
                        <td colspan="2">
                            <label>Responsable AppMovil<span style="color:red;">*</span></label> 
                        </td>
                        <td colspan="2">
                            <input type="text" id="nombreencargado" onchange="conMayusculas(this)" style="width: 250px"  />
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2">
                            <label>identificacion <span style="color:red;">*</span></label> 
                        </td>
                        <td colspan="2">
                            <input type="text" id="nit" class="solo-numero"  maxlength="10" style="width: 250px"  onblur="verificarUsuarioeds()"/>
                        </td>
                    </tr>

                    <tr>
                        <td>
                            <label>ID del usuario<span style="color:red;">*</span></label> 
                        </td>
                        <td>
                            <input type="text" maxlength="10" id="idusu" style="width: 105px" onchange="conMayusculas(this)" onblur="verificarUsuario()" />   
                        </td>
                        <td>
                            <label>Cambiar Clave al Inicio</label> 
                        </td>
                        <td>
                            <input type="checkbox" id="actdes" value="true" checked="" />   
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <label>Password<span style="color:red;">*</span></label> 
                        </td>
                        <td>
                            <input type="password" id="pas1" maxlength="15" style="width: 105px" />   
                        </td>
                        <td>
                            <label>Confirmar Password<span style="color:red;">*</span></label> 
                        </td>
                        <td>
                            <input type="password" id="pas2" onkeyup="validarpass()"  maxlength="15"   style="width: 105px" />
                        </td>
                    </tr>

                </table>
                <center>
                    <p id="dialogMsjSMS"  style="color: red"></p>  
                    <p id="sms2" style="color: red"></p> 
                </center>

            </div>
        </div>
        <div id="info"  class="ventana" >
            <p id="notific" >EXITO AL GUARDAR</p>
        </div>
    </center>

</body>

<script>
    $(document).ready(function () {
        $('.solo-numero').keyup(function () {
            this.value = (this.value + '').replace(/[^0-9]/g, '');
        });
    });

    init2();
</script>

</html>
