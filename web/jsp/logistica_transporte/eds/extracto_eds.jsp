<%-- 
    Document   : extracto_eds
    Created on : 19/06/2015, 09:15:30 AM
    Author     : egonzalez
--%>

<%@page contentType="text/html" pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
        <title>Extracto Eds</title>

        <link href="./css/reporte_produccion.css" rel="stylesheet" type="text/css" >
        <link type="text/css" rel="stylesheet" href="./css/jquery/jquery-ui/jquery-ui.css" />
        <link href="./css/popup.css" rel="stylesheet" type="text/css">
        <script type="text/javascript" src="./js/jquery/jquery.jqGrid/js/jquery.min.js"></script>
        <script type="text/javascript" src="./js/jquery-ui-1.8.5.custom.min.js"></script>
        <script type="text/javascript" src="./js/jquery/jquery.jqGrid-4.7.0/js/grid.locale-es.js"></script>
        <script type="text/javascript" src="./js/jquery/jquery.jqGrid-4.7.0/js/jquery.jqGrid.min.js"></script>
        <script type="text/javascript" src="./js/jquery/jquery.jqGrid-4.7.0/js/jquery.tabletojson.js"></script>
        <script type="text/javascript" src="./js/extracto_eds.js"></script> 
    </head>
    <body>
        <div id="capaSuperior" style="position:absolute; width:100%; height:100px; z-index:0; left: 0px; top: 0px;">
            <jsp:include page="/WEB-INF/toptsp.jsp?encabezado=EXTRACTO EDS"/>
        </div>
        <div id="capaCentral" style="position:absolute; width:100%; height:100%; z-index:0; left: 0px; top: 100px; ">
            <div id="filtro_extracto">
                <table border="0"cellpadding="1" class="tablaBusquedaExtracto">           
                    <tbody>
                    <thead>
                    <th class="ui-state-default titulo" colspan="8">Filtro  Busqueda</th>
                    </thead>
                    <tr>
                        <td>Nombre Eds</td>
                        <td>  
                            <select class="input" id="nombre" name="nombre">
                            </select>
                        </td>
                        <td>Nit Estacion</td>
                        <td><input name="nit" id="nit" type="text" class="inpt" readonly></td>
                        <td>Fecha Inicial</td>
                        <td> <input type="text" id="fecha_inicio" class="inpt" readonly></td>
                        <td>Fecha Final</td>
                        <td>  <input type="text" id="fecha_fin" class="inpt" readonly> </td>
                    </tr>
                    </tbody>
                </table>
                <hr>
                <button id="buscar" class="ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only" >
                    <span class="ui-button-text">Buscar</span>
                </button>
            </div>
            <br>
            <center>
                <table id="tbl_extracto_eds"> </table>
                <div id="page_extracto_eds"></div> 
            </center>
        </div>
        <div id="dialogo" class="ventana" title="Mensaje">
            <p  id="msj">texto </p>
        </div>

        <form id="form_expr" action="" method="post" name="form_expr">
            <input type="hidden" name="pdfBuffer" id="pdfBuffer" value="" />
            <input type="hidden" name="fileName" id="fileName" value="reporte_produccion" />
            <input type="hidden" name="fileType" id="fileType" value="" />
           
        </form>
    </body>
</html>
