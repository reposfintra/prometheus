<%-- 
    Document   : cuentasDeCobroEDS
    Created on : 5/08/2015, 03:03:28 PM
    Author     : mariana
--%>

<%@page contentType="text/html" pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
    <head>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
        <link href="./css/popup.css" rel="stylesheet" type="text/css">
        <link type="text/css" rel="stylesheet" href="./css/jquery/jquery-ui/jquery-ui.css" />
        <link type="text/css" rel="stylesheet" href="./css/TransportadorasApi.css " />
        <script type="text/javascript" src="./js/jquery/jquery.jqGrid/js/jquery.min.js"></script>
        <script type="text/javascript" src="./js/jquery-ui-1.8.5.custom.min.js"></script>   
        <script type="text/javascript" src="./js/jquery/jquery.jqGrid-4.7.0/js/grid.locale-es.js"></script>
        <script type="text/javascript" src="./js/jquery/jquery.jqGrid-4.7.0/js/jquery.jqGrid.min.js"></script>
        <script type="text/javascript" src="./js/jquery/jquery.jqGrid-4.7.0/js/jquery.tabletojson.js"></script>    
        <script type="text/javascript" src="./js/CuentasCobroEDS.js"></script> 
        <script type="text/javascript" href="http://www.ddginc-usa.com/spanish/editcheck.js"></script>
        <title>CUENTAS DE COBRO EDS </title>
    </head>
    <body>

        <div id="capaSuperior" style="position:absolute; width:100%; height:100px; z-index:0; left: 0px; top: 0px;">
            <jsp:include page="/WEB-INF/toptsp.jsp?encabezado=CUENTAS DE COBRO EDS"/>
        </div>

    <center>
        <div id="tablita" style="top: 170px;width: 759px;height: 86px;" >
            <div id="encabezadotablita" style="width: 746px">
                <label class="titulotablita"><b>FILTRO DE BUSQUEDA</b></label>
            </div> 
            <table id="tablainterna"style="height: 53px;"  >
                <tr>
                    <td>
                        <label>Propietario</label>
                    </td>
                    <td>
                        <select id="propietario" style="width: 253px;color: #050505;height: 22px;margin-left: 5px;"onchange="cargarComboEds()" >
                        </select>
                    </td>
                    <td>
                        <label>EDS</label>
                    </td>
                    <td>
                        <select id="eds" style="width: 151px;color: #050505;height: 22px;margin-left: 5px;" onchange="limpiar()">
                        </select>
                    </td>
                    <td>
                        <label>Fecha</label>
                    </td>
                    <td>
                        <input type="datetime" id="fecha" name="fecha" style="height: 20px;width: 88px;color: #070708;" readonly>
                    </td>
                    <td>
                        <div id ='botones'>
                            <button id="buscar" class="ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only" 
                                    role="button" aria-disabled="false" style="margin-top: 6px;margin-left: 6px;">
                                <span class="ui-button-text">Buscar</span>
                            </button> 
                        </div>
                    </td>
                </tr>
            </table> 

        </div>

        <div style="position: relative;top: 231px;">
            <div id="contenedor" style="width: 1395px;height: 420px; border-radius: 4px; border: 1px solid #008000; background: #FFF none repeat scroll 0% 0%;">   
                <br>
                <table id="tabla_cuentas_cobro" ></table>
                <div id="pager1"></div>
                <hr>
                <div id ='botones'>
                    <center>
                        <button id="generar" class="ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only" 
                                role="button" aria-disabled="false" style="margin-top: 6px;margin-left: 6px;">
                            <span class="ui-button-text">Generar Cuenta de Cobro</span>
                        </button> 
                    </center>
                </div>  
            </div>
        </div>
    </div>
    <div id="divSalidaEx" title="Exportacion" style=" display: block" >
        <p  id="msjEx" style=" display:  none"> Espere un momento por favor...</p>
        <center>
            <img id="imgloadEx" style="position: relative;  top: 7px; display: none " src="./images/cargandoCM.gif"/>
        </center>
        <div id="respEx" style=" display: none"></div>
    </div> 
</center>
<div id="info"  class="ventana" >
    <p id="notific">EXITO AL GUARDAR</p>
</div>
</body>
</html>
