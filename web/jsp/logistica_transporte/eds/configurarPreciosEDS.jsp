<%-- 
    Document   : configurarPreciosEDS
    Created on : 14/08/2015, 11:33:24 AM
    Author     : mcastillo
--%>
 <script type="text/javascript" src="./js/configPreciosProdEds.js"></script>
 <div id="capaCentral" align="left" style="position:absolute; width:100%; height:100%; z-index:0; top: 0px;" >
           <center>
                <h4 style="width:50%">CONFIGURACION PRECIOS DE PRODUCTOS</h4>
                <div id="div_configPrecioProd" >                         
                         <table id="tbl_config_prices" class="tablas" align="center" style=" width: 98%;" >

                         </table>                
                     </br>                    
                </div>  
                <br>  
                <button id="btn_config_prices" class="ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only" 
                        role="button" aria-disabled="false">
                    <span class="ui-button-text">Actualizar</span>
                </button>
            </center>
 </div>
