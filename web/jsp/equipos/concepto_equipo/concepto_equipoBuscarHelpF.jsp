<!--  
	 - Author(s)       :      LREALES
	 - Description     :      AYUDA FUNCIONAL - Buscar Concepto de Equipo
	 - Date            :      07/06/2006 
	 - Copyright Notice:      TRANSPORTES SANCHEZ POLO S.A.
-->
<%@ include file="/WEB-INF/InitModel.jsp"%>
<HTML>
<HEAD>
<TITLE>AYUDA FUNCIONAL - Buscar Concepto de Equipo</TITLE>
<META http-equiv=Content-Type content="text/html; charset=windows-1252">
<link href="../css/estilostsp.css" rel="stylesheet" type="text/css"> 
<link href="<%=BASEURL%>/css/estilostsp.css" rel="stylesheet" type="text/css">
<script type='text/javascript' src="<%= BASEURL %>/js/boton.js"></script> 
</HEAD>
<BODY> 
<% String BASEIMG = BASEURL +"/images/ayuda/equipos/buscar_concepto/"; %>
  <table width="95%"  border="2" align="center">
    <tr>
      <td height="117" >
        <table width="100%" border="0" align="center">
          <tr  class="subtitulo">
            <td height="20"><div align="center">MANUAL DE CONCEPTO EQUIPO WEB</div></td>
          </tr>
          <tr class="subtitulo1">
            <td>Descripci&oacute;n del funcionamiento del programa para Buscar Concepto de Equipo.</td>
          </tr>
          <tr>
            <td  class="ayudaHtmlTexto"><div align="center">
              <p>&nbsp;</p>
              </div></td>
          </tr>
<tr>
            <td  class="ayudaHtmlTexto"><p class="ayudaHtmlTexto">En la siguiente pantalla se puede realizar la busqueda del concepto de equipo.</p>
            </td>
          </tr>
<tr>
            <td  class="ayudaHtmlTexto"><div align="center"><IMG height=122 src="<%=BASEIMG%>image001.JPG" width=554 border=0 v:shapes="_x0000_i1143"></div></td>
          </tr>
		  <tr>
            <td  class="ayudaHtmlTexto"><p class="ayudaHtmlTexto">&nbsp;</p>
              <p class="ayudaHtmlTexto">Si  se desea realizar una busqueda detallada de los conceptos de equipos..
                <br>
              Usted debe seleccionar el bot&oacute;n 'DETALLES' y en la pantalla se saldr&aacute; lo siguiente.</p>
          </td>
          </tr>
<tr>
  <td  class="ayudaHtmlTexto"><div align="center"><IMG height=231 src="<%=BASEIMG%>image002.JPG" width=463 border=0 v:shapes="_x0000_i1054"></div></td>
</tr>
<tr>
            <td  class="ayudaHtmlTexto"><p class="ayudaHtmlTexto">&nbsp;</p>
              <p class="ayudaHtmlTexto">Si por el contrario se desea realizar una busqueda especifica por c&oacute;digo de concepto de equipo..
                <br>
              Usted debe seleccionar el bot&oacute;n 'BUSCAR'.<br>
              El sistema verificar&aacute; que todos los campos obligatorios esten llenos, si no lo estan le saldr&aacute; en la pantalla el siguiente mensaje. </p>
          </td>
          </tr>
<tr>
  <td  class="ayudaHtmlTexto"><div align="center"><IMG height=125 src="<%=BASEIMG%>image_error001.JPG" width=319 border=0 v:shapes="_x0000_i1054"></div></td>
</tr>
<tr>
  <td  class="ayudaHtmlTexto"><p>&nbsp;</p>
    <p>Si el c&oacute;digo digitado no existe en nuestra base de datos, entonces el sistema le mostrar&aacute; el siguiente mensaje.</p></td>
</tr>
<tr>
  <td  class="ayudaHtmlTexto"><div align="center"><IMG height=68 src="<%=BASEIMG%>image_error002.JPG" width=377 border=0 v:shapes="_x0000_i1054"></div></td>
</tr>
<tr>
  <td  class="ayudaHtmlTexto"><p>&nbsp;</p>
    <p>Cuando alla echo el procedimiento de busqueda especifica correctamente, en la pantalla nos saldr&aacute; la informaci&oacute;n del concepto de equipo a modificar o anular. </p></td>
</tr>
<tr>
  <td  class="ayudaHtmlTexto"><div align="center"><IMG height=155 src="<%=BASEIMG%>image003.JPG" width=702 border=0 v:shapes="_x0000_i1054"></div></td>
</tr>
<tr>
  <td  class="ayudaHtmlTexto"><p>&nbsp;</p>
    <p>El sistema verificar&aacute; que no hayan datos vacios, si no lo estan le saldr&aacute; en la pantalla el siguiente mensaje. </p></td>
</tr>
<tr>
  <td  class="ayudaHtmlTexto"><div align="center"><IMG height=126 src="<%=BASEIMG%>image_error004.JPG" width=464 border=0 v:shapes="_x0000_i1054"></div></td>
</tr>
<tr>
  <td  class="ayudaHtmlTexto"><p>&nbsp;</p>
    <p>Si desea modificar el concepto de equipo, debe seleccionar el bot&oacute;n 'MODIFICAR'. <br>
      Si es asi, luego de haber realizado los cambios deseados en la pantalla el sistema nos mostrar&aacute; lo siguiente. </p></td>
</tr>
<tr>
  <td  class="ayudaHtmlTexto"><div align="center"><IMG height=226 src="<%=BASEIMG%>image004.JPG" width=646 border=0 v:shapes="_x0000_i1054"></div></td>
</tr>

<tr>
  <td  class="ayudaHtmlTexto"><p>&nbsp;</p>
    <p>Si por el contrario se desea realizar la anulaci&oacute;n de un concepto de equipo, se debe seleccionar el bot&oacute;n 'ANULAR'.<br>
      Si es asi, luego de haber verificado que ese es el concepto que se desea anular, y se presione click sobre este bot&oacute;n, el sistema nos mostrar&aacute; lo siguiente. </p></td>
</tr>
<tr>
  <td  class="ayudaHtmlTexto"><div align="center"><IMG height=115 src="<%=BASEIMG%>image005.JPG" width=451 border=0 v:shapes="_x0000_i1054"></div></td>
</tr>

      </table></td>
    </tr>
  </table>
  <p></p>
<center>
<img src = "<%=BASEURL%>/images/botones/salir.gif" style = "cursor:hand" name = "imgsalir" onClick = "parent.close();" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);">
</center>
</BODY>
</HTML>
