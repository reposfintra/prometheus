<%@page contentType="text/html"%>
<%@page session="true"%>
<%@page errorPage="../error/ErrorPage.jsp"%>
<%@page import="java.util.*"%>
<%@page import="com.tsp.operation.model.*"%>
<%@page import="com.tsp.util.*"%>
<%@page import="com.tsp.operation.model.beans.*, java.util.*"%>
<%@include file="/WEB-INF/InitModel.jsp"%>
<%@taglib uri="http://jsptags.com/tags/navigation/pager" prefix="pg" %>

<html>
<head>
<title>Ver Conceptos</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link href="<%= BASEURL %>/css/estilostsp.css" rel="stylesheet" type="text/css"> 
<link href="../../../css/estilostsp.css" rel="stylesheet" type="text/css"> 
<script type='text/javascript' src="<%= BASEURL %>/js/reporte.js"></script> 
<script type='text/javascript' src="<%= BASEURL %>/js/boton.js"></script>
</head>
<body onresize="redimensionar()" onload = 'redimensionar()'>
<div id="capaSuperior" style="position:absolute; width:100%; height:100px; z-index:0; left: 0px; top: 0px;">
	<jsp:include page="/toptsp.jsp?encabezado=Concepto Equipo"/>
</div>
<div id="capaCentral" style="position:absolute; width:100%; height:87%; z-index:0; left: 0px; top: 100px; overflow: scroll;">
	  <% Vector conceptos = model.concepto_equipoService.getOtro_vector();
	  
	  if( conceptos != null  && conceptos.size() > 0 ){ %>
 <table width="95%" border="2" align=center>
    <tr>
        <td>
            <table width="100%" align="center" class="tablaInferior"> 
                <tr>
                    <td colspan='2'>                                                
                        <table width="100%"  border="0" cellpadding="0" cellspacing="0" class="barratitulo">
                            <tr>
                                <td width="50%" class="subtitulo1" colspan='3'>Informaci&oacute;n</td>
                                <td width="50%" class="barratitulo" colspan='2'><img src="<%=BASEURL%>/images/titulo.gif"></td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td>
					<div class="scroll" style=" overflow:auto ; WIDTH: 100%; HEIGHT:100%; " >
                        <table width="100%" border="1" align="center" bordercolor="#999999" bgcolor="#F7F5F4">
                            <tr class="tblTitulo">
                                <td nowrap width="6%"><div align="center">C&oacute;digo</div></td>
                                <td nowrap width="6%"><div align="center">Descripci&oacute;n</div></td>
                            </tr>
							  <%
								for( int i = 0; i < conceptos.size(); i++ ){
									Hashtable ht = ( Hashtable ) conceptos.get( i );
							  %>
							<tr class="<%=(i % 2 == 0 )?"filagris":"filaazul"%>" onMouseOver='cambiarColorMouse(this)' style="cursor:hand" onClick="window.opener.document.forma.c_codigo.value='<%=( String ) ht.get( "codigo" )%>'; window.opener.document.forma.c_descripcion.value='<%=( String ) ht.get( "descripcion" )%>'; window.close();">
							<td  nowrap  align="center" abbr="" class="bordereporte"><%=( String ) ht.get( "codigo" )%></td>
							<td  align="center" abbr="" nowrap class="bordereporte"><%=( String ) ht.get( "descripcion" )%></td>
							</tr>
						  
								<%}%>
                        </table>
					  </div>
                    </td>
                </tr>
            </table>
        </td>
    </tr>
 </table>
 <br>
	<%}
	 else { %>                    
	  <%out.print("<table width=379 border=2 align=center><tr><td><table width=100%  border=1 align=center  bordercolor=#F7F5F4 bgcolor=#FFFFFF><tr><td width=350 align=center class=mensajes>No Existen Conceptos!</td><td width=100><img src="+BASEURL+"/images/cuadronaranja.JPG></td></tr></table></td></tr></table>");%>
	  <br>
	<%}%>

 <table width='95%' align=center>
  <tr class="titulo">
    <td align=right colspan='2'>
        <div align="center">
		<img src="<%=BASEURL%>/images/botones/salir.gif"  name="imgsalir"  onMouseOver="botonOver(this);" onClick="parent.close();" onMouseOut="botonOut(this);" style="cursor:hand">
        </div></td>
  </tr>
</table>
</div>
</body>
</html>