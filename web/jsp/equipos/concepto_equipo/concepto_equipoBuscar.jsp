<!--
- Nombre P�gina :                  concepto_equipoInsertar.jsp                  
- Descripci�n :                    Pagina JSP, que maneja la busqueda de los conceptos de equipos              
- Autor :                          Ing. Jose de la rosa                        
- Fecha Creado :                   2 de Diciembre de 2005                
- Modificado por :                 LREALES                                   
- Fecha Modificado :               7 de Junio de 2006            
- Versi�n :                        1.0                                       
- Copyright :                      Fintravalores S.A.                   
-->

<%-- Declaracion de librerias--%>
<%@page contentType="text/html"%>
<%@ page session="true"%>
<%@ page errorPage="../error/error.jsp"%>
<%@page import="com.tsp.operation.model.beans.*, java.util.*"%>
<%@ include file="/WEB-INF/InitModel.jsp"%>
<%@page import="com.tsp.operation.model.*"%>
<%@page import="com.tsp.util.*"%>
<%@ taglib uri="http://jakarta.apache.org/taglibs/input-0.90" prefix="input" %>
<%
  String path = application.getRealPath(model.menuService.getRealUrl(request.getRequestURI()));
  String datos[] = model.menuService.getContenidoMenu(BASEURL,request.getRequestURI(),path);
%>
<html>
<head><title>Buscar Concepto de Equipo</title>
<link href="<%= BASEURL %>/css/estilostsp.css" rel="stylesheet" type="text/css">
<script type='text/javascript' src="<%= BASEURL %>/js/boton.js"></script>
<script type='text/javascript' src="<%= BASEURL %>/js/validar.js"></script>
</head>
<body onLoad="redimensionar();" onResize="redimensionar();">
<div id="capaSuperior" style="position:absolute; width:100%; height:100px; z-index:0; left: 0px; top: 0px;">
<jsp:include page="/WEB-INF/toptsp.jsp?encabezado=Concepto Equipo"/>
</div>

<div id="capaCentral" style="position:absolute; width:100%; height:85%; z-index:0; left: 0px; top: 100px; overflow: scroll;"> 
<%
    Usuario usuario = (Usuario) session.getAttribute("Usuario");    
%>
<script>
	var controlador ="<%=CONTROLLER%>";
	
	function Buscar ( c_codigo ){
	
		if( c_codigo != "" ){
		
			document.form2.action = controlador+"?estado=Concepto_equipo&accion=Search&listar=True&sw=True&c_codigo="+c_codigo;
			document.form2.submit();
						
		} else{
		
			alert( "Por Favor No Deje Los Campos Obligatorios Vacios!" );
			
		}
	}
</script>

<form name="form2" method="post">
    <table width="60%" border="2" align="center">
      <tr>
        <td><table width="100%" align="center"  class="tablaInferior">
            <tr>
				<td width="40%" class="subtitulo1">Buscar Concepto de Equipo</td>
				<td width="60%" class="barratitulo"><img src="<%=BASEURL%>/images/titulo.gif" width="32" height="20" align="left"><%=datos[0]%></td>
            </tr>
            <tr class="fila">
              <td width="40%" align="left" valign="middle" >C&oacute;digo</td>
              <td width="60%" valign="middle"><input name="c_codigo" type="text" class="textbox" id="c_codigo" onKeyPress="soloAlfa(event)" size="10" maxlength="30">
              <img src="<%=BASEURL%>/images/botones/iconos/obligatorio.gif"></td>
            </tr>		
        </table></td>
      </tr>
    </table>
<p>
<div align="center">
  <p><img src="<%=BASEURL%>/images/botones/buscar.gif" style="cursor:hand" title="Buscar un concepto equipo" name="buscar"  onClick="Buscar ( c_codigo.value );" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);">&nbsp;
      <img src="<%=BASEURL%>/images/botones/detalles.gif" style="cursor:hand" title="Buscar todos los conceptos equipos" name="buscar"  onClick="window.location='<%=CONTROLLER%>?estado=Concepto_equipo&accion=Search&listar=True&sw=False'" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);">&nbsp;
      <img src="<%=BASEURL%>/images/botones/salir.gif"  name="salir" style="cursor:hand" title="Salir al Menu Principal" onMouseOver="botonOver(this);" onClick="parent.close();" onMouseOut="botonOut(this);" ></p>
  <p>
  <%String msg = request.getParameter("msg");
	if ( msg!=null && !msg.equals("") ){%>
                        
	  <table border="2" align="center">
   		<tr>
        	<td>
            	<table width="100%"  border="1" align="center"  bordercolor="#F7F5F4" bgcolor="#FFFFFF">
                	<tr>
                    	<td width="229" align="center" class="mensajes"><%=msg%></td>
                        <td width="29" background="<%=BASEURL%>/images/cuadronaranja.JPG">&nbsp;</td>
                        <td width="58">&nbsp;</td>
                    </tr>
                </table>
            </td>
		</tr>
	  </table>
	<%}%>
  </p>
</div>
</p>
</form>
</div>
<%=datos[1]%>
</body>
</html>