<!--
- Nombre P�gina :                  concepto_equipoInsertar.jsp                  
- Descripci�n :                    Pagina JSP, que maneja los listados de los conceptos de equipos
- Autor :                          Ing. Jose de la rosa                        
- Fecha Creado :                   2 de Diciembre de 2005                
- Modificado por :                 LREALES                                   
- Fecha Modificado :               7 de Junio de 2006            
- Versi�n :                        1.0                                       
- Copyright :                      Fintravalores S.A.                   
-->

<%-- Declaracion de librerias--%>
<%@page contentType="text/html"%>
<%@page session="true"%>
<%@page errorPage="/error/error.jsp"%>
<%@page import="java.util.*"%>
<%@page import="com.tsp.operation.model.*"%>
<%@page import="com.tsp.operation.model.beans.*"%>
<%@page import="com.tsp.util.*"%>
<%@include file="/WEB-INF/InitModel.jsp"%>
<%@taglib uri="http://jsptags.com/tags/navigation/pager" prefix="pg" %>
<%
  String path = application.getRealPath(model.menuService.getRealUrl(request.getRequestURI()));
  String datos[] = model.menuService.getContenidoMenu(BASEURL,request.getRequestURI(),path);
%>
<html>
<head><title>Lista de Conceptos de Equipos</title>
<link href="<%= BASEURL %>/css/estilostsp.css" rel="stylesheet" type="text/css">
<script type='text/javascript' src="<%= BASEURL %>/js/boton.js"></script>
<script type='text/javascript' src="<%= BASEURL %>/js/reporte.js"></script>
<link href="/css/estilostsp.css" rel="stylesheet" type="text/css">
</head>
<body onLoad="redimensionar();" onResize="redimensionar();">
<div id="capaSuperior" style="position:absolute; width:100%; height:100px; z-index:0; left: 0px; top: 0px;">
<jsp:include page="/WEB-INF/toptsp.jsp?encabezado=Concepto Equipo"/>
</div>

<div id="capaCentral" style="position:absolute; width:100%; height:85%; z-index:0; left: 0px; top: 100px; overflow: scroll;"> 
<%  String c = (String)session.getAttribute("codigo");
    String d = (String)session.getAttribute("descripcion");
    model.concepto_equipoService.searchDetalleConcepto_equipo(c,d);
    Vector vec = model.concepto_equipoService.getConcepto_equipos();
    String style = "simple";
    String position =  "bottom";
    String index =  "center";
    int maxPageItems = 10;
    int maxIndexPages = 10;

if ( vec.size() > 0 ){
%>
<table width="50%" border="2" align="center">
    <tr>
      <td>
	  	  <table width="100%" align="center">
              <tr>
                <td width="373" class="subtitulo1">&nbsp;Conceptos de Equipos</td>
                <td width="427" class="barratitulo"><img src="<%=BASEURL%>/images/titulo.gif" width="32" height="20" align="left"><%=datos[0]%></td>
              </tr>
        </table>
<table width="100%" border="1" bordercolor="#999999" bgcolor="#F7F5F4" align="center">
    <tr class="tblTitulo" align="center">
        <td width="20%" nowrap >C&oacute;digo</td>
        <td width="80%" nowrap >Descripci&oacute;n</td>
    </tr>
    <pg:pager
        items="<%=vec.size()%>"
        index="<%= index %>"
        maxPageItems="<%= maxPageItems %>"
        maxIndexPages="<%= maxIndexPages %>"
        isOffset="<%= true %>"
        export="offset,currentPageNumber=pageNumber"
        scope="request">
<%
    for (int i = offset.intValue(), l = Math.min(i + maxPageItems, vec.size()); i < l; i++){
        Concepto_equipo u = (Concepto_equipo) vec.elementAt(i);%>
        <pg:item>
            <tr class="<%=(i % 2 == 0 )?"filagris":"filaazul"%>" onMouseOver='cambiarColorMouse(this)'   style="cursor:hand"onClick="window.open('<%=CONTROLLER%>?estado=Concepto_equipo&accion=Search&c_codigo=<%=u.getCodigo()%>&c_descripcion=<%=u.getDescripcion()%>&c_base=<%=u.getBase()%>&listar=False','myWindow','status=no,scrollbars=no,width=650,height=350,resizable=yes');" >
                <td align="center" class="bordereporte"><%=u.getCodigo()%></td>
                <td align="center" class="bordereporte"><%=u.getDescripcion()%></td>
            </tr>
      </pg:item>
<%  }%>
    <tr class="pie">
        <td td height="20" colspan="10" nowrap align="center">          <pg:index>
                <jsp:include page="/WEB-INF/jsp/googlesinimg.jsp" flush="true"/>
        </pg:index>
        </td>
    </tr>
    </pg:pager>        
</table>
</td>
</tr>
</table>
<br>
<%}else { %>
</p>
  <table border="2" align="center">
    <tr>
      <td><table width="410" height="73" border="1" align="center"  bordercolor="#F7F5F4" bgcolor="#FFFFFF">
          <tr>
            <td width="282" align="center" class="mensajes">Su b&uacute;squeda no arroj&oacute; resultados!</td>
            <td width="28" background="<%=BASEURL%>/images/cuadronaranja.JPG">&nbsp;</td>
            <td width="78">&nbsp;</td>
          </tr>
      </table></td>
    </tr>
  </table>
  <%}%>
   <table width="50%" border="0" align="center">
    <tr>
      <td>
	  <img src="<%=BASEURL%>/images/botones/regresar.gif" style="cursor:hand" title="Buscar un concepto de equipo" name="buscar"  onClick="window.location='<%=BASEURL%>/jsp/equipos/concepto_equipo/concepto_equipoBuscar.jsp'" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);">
	  </td>
	</tr>
</table>   
</div> 
<%=datos[1]%>
</body>
</html>