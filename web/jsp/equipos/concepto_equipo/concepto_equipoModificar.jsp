<!--
- Nombre P�gina :                  concepto_equipoInsertar.jsp                  
- Descripci�n :                    Pagina JSP, que maneja la modificaci�n de los conceptos de equipos
- Autor :                          Ing. Jose de la rosa                        
- Fecha Creado :                   2 de Diciembre de 2005                
- Modificado por :                 LREALES                                   
- Fecha Modificado :               7 de Junio de 2006            
- Versi�n :                        1.0                                       
- Copyright :                      Fintravalores S.A.                   
-->

<%-- Declaracion de librerias--%>
<%@page contentType="text/html"%>
<%@page session="true"%>
<%@page import="java.util.*"%>
<%@page import="com.tsp.operation.model.*"%>
<%@page import="com.tsp.operation.model.beans.*"%>
<%@page import="com.tsp.util.*"%>
<%@include file="/WEB-INF/InitModel.jsp"%>
<%
  String path = application.getRealPath(model.menuService.getRealUrl(request.getRequestURI()));
  String datos[] = model.menuService.getContenidoMenu(BASEURL,request.getRequestURI(),path);
%>
<html>
<head>
<title>Modificar Concepto de Equipo</title>
<link href="<%= BASEURL %>/css/estilostsp.css" rel="stylesheet" type="text/css">
<script type='text/javascript' src="<%= BASEURL %>/js/boton.js"></script>
</head>
<script type='text/javascript' src="<%= BASEURL %>/js/validar.js"></script>
<body onResize="redimensionar();" <%if(request.getParameter("reload")!=null){%>onLoad="parent.opener.location.reload();redimensionar();"<%}%>>
<div id="capaSuperior" style="position:absolute; width:100%; height:100px; z-index:0; left: 0px; top: 0px;">
<jsp:include page="/WEB-INF/toptsp.jsp?encabezado=Concepto Equipo"/>
</div>

<div id="capaCentral" style="position:absolute; width:100%; height:85%; z-index:0; left: 0px; top: 100px; overflow: scroll;"> 
<%
    Usuario usuario = (Usuario) session.getAttribute("Usuario");
    Concepto_equipo u = model.concepto_equipoService.getConcepto_equipo();    
    String mensaje = (String) request.getAttribute("mensaje");
    Vector vec = new Vector();
%>
<FORM name='forma' id='forma' method='POST' action='<%=CONTROLLER%>?estado=Concepto_equipo&accion=Update&sw='>
    
	<table width="70%" border="2" align="center">
      <tr>
        <td>
          <table width="100%" class="tablaInferior">
            <tr class="fila">
              <td align="left" class="subtitulo1">&nbsp;Modificar Concepto de Equipo</td>
              <td align="left" class="barratitulo"><img src="<%=BASEURL%>/images/titulo.gif" width="32" height="20" align="left"><%=datos[0]%></td>
            </tr>
            <tr>
              <td align="left" class="fila">C&oacute;digo</td>
              <td width="50%" valign="middle" class="letra"><%=u.getCodigo()%><input name="c_codigo" type="HIDDEN" id="c_codigo" value="<%=u.getCodigo()%>" ></td>
            </tr>
            <tr class="fila">
              <td width="50%" align="left" >Descripci&oacute;n</td>
              <td valign="middle"><textarea name="c_descripcion" cols="30" class="textbox" id="c_descripcion"><%=u.getDescripcion()%></textarea></td>
            </tr>
        </table></td>
      </tr>
    </table>
	<p>
	<div align="center"><img src="<%=BASEURL%>/images/botones/modificar.gif" style="cursor:hand" title="Modificar un concepto equipo" name="modificar"  onclick="return TCamposLlenos();" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);">&nbsp;
	<img src="<%=BASEURL%>/images/botones/anular.gif" style="cursor:hand" title="Anular un concepto equipo" name="anular"  onClick="window.location='<%=CONTROLLER%>?estado=Concepto_equipo&accion=Anular&c_codigo=<%=u.getCodigo()%>'" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);">&nbsp;
		  <img src="<%=BASEURL%>/images/botones/regresar.gif" style="cursor:hand" title="Buscar un concepto de equipo" name="buscar"  onClick="window.location='<%=BASEURL%>/jsp/equipos/concepto_equipo/concepto_equipoBuscar.jsp'" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);">&nbsp;
	<img src="<%=BASEURL%>/images/botones/salir.gif" style="cursor:hand" name="salir" title="Salir al Menu Principal" onMouseOver="botonOver(this);" onClick="parent.close();" onMouseOut="botonOut(this);" ></div>
	</p>
	<%  if(mensaje!=null){%>
		<table border="2" align="center">
		  <tr>
			<td><table width="100%"  border="1" align="center"  bordercolor="#F7F5F4" bgcolor="#FFFFFF">
			  <tr>
				<td width="229" align="center" class="mensajes"><%=mensaje%></td>
				<td width="29" background="<%=BASEURL%>/images/cuadronaranja.JPG">&nbsp;</td>
				<td width="58">&nbsp;</td>
			  </tr>
			</table></td>
		  </tr>
		</table>
	<%  }%>

</FORM>
</div>
<%=datos[1]%>
</body>
</html>