<!--
- Nombre P�gina :                  concepto_equipoInsertar.jsp                  
- Descripci�n :                    Pagina JSP, que maneja el ingreso de los conceptos de equipos                  
- Autor :                          Ing. Jose de la rosa                        
- Fecha Creado :                   2 de Diciembre de 2005                
- Modificado por :                 LREALES                                   
- Fecha Modificado :               5 de Junio de 2006            
- Versi�n :                        1.0                                       
- Copyright :                      Fintravalores S.A.                   
-->

<%-- Declaracion de librerias--%>
<%@page contentType="text/html"%>
<%@ page session="true"%>
<%@ page errorPage="../error/error.jsp"%>
<%@page import="com.tsp.operation.model.beans.*, java.util.*"%>
<%@ include file="/WEB-INF/InitModel.jsp"%>
<%@page import="com.tsp.operation.model.*"%>
<%@page import="com.tsp.util.*"%>
<%@ taglib uri="http://jakarta.apache.org/taglibs/input-0.90" prefix="input" %>
<%
  String path = application.getRealPath(model.menuService.getRealUrl(request.getRequestURI()));
  String datos[] = model.menuService.getContenidoMenu(BASEURL,request.getRequestURI(),path);
%>
<html>
<head>
<title>Ingresar Concepto de Equipo</title>
<link href="<%= BASEURL %>/css/estilostsp.css" rel="stylesheet" type="text/css">
<script type='text/javascript' src="<%= BASEURL %>/js/boton.js"></script>
</head>
<script type='text/javascript' src="<%= BASEURL %>/js/validar.js"></script>
<body onLoad="redimensionar();" onResize="redimensionar();">
<div id="capaSuperior" style="position:absolute; width:100%; height:100px; z-index:0; left: 0px; top: 0px;">
<jsp:include page="/WEB-INF/toptsp.jsp?encabezado=Concepto Equipo"/>
</div>

<div id="capaCentral" style="position:absolute; width:100%; height:85%; z-index:0; left: 0px; top: 100px; overflow: scroll;"> 
<FORM name='forma' id='forma' method='POST' action='<%=CONTROLLER%>?estado=Concepto_equipo&accion=Insert'>
  <table width="60%" border="2" align="center">
    <tr>
      <td>
        <table width="100%" class="tablaInferior">
          <tr class="fila">
            <td align="left" class="subtitulo1">&nbsp;Ingresar Concepto de Equipo</td>
            <td width="59%" align="left" class="barratitulo"><img src="<%=BASEURL%>/images/titulo.gif" width="32" height="20" align="left"><%=datos[0]%></td>
          </tr>
          <tr class="fila">
            <td align="left" >C&oacute;digo</td>
            <td valign="middle"><input name="c_codigo" type="text" class="textbox" id="c_codigo" onKeyPress="soloAlfa(event)" size="10" maxlength="30" readonly>
            <img src="<%=BASEURL%>/images/botones/iconos/obligatorio.gif">
			<img src="<%=BASEURL%>/images/botones/iconos/lupa.gif" alt="Buscar # Concepto.." name="imglupa" width="20" height="20" style="cursor:hand" onClick="window.open('<%=CONTROLLER%>?estado=Ver&accion=Conceptos','','status=no,scrollbars=no,width=700,height=470,resizable=yes');" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);"></td>
          </tr>
          <tr class="fila">
            <td width="41%" align="left" >Descripci&oacute;n</td>
            <td valign="middle"><textarea name="c_descripcion" cols="60" class="textbox" id="c_descripcion" readonly></textarea></td>
          </tr>
      </table></td>
    </tr>
  </table>
<p>
<div align="center"><img src="<%=BASEURL%>/images/botones/agregar.gif" style="cursor:hand" title="Agregar un concepto equipo" name="agregar"  onclick="return TCamposLlenos();" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);">&nbsp;
<img src="<%=BASEURL%>/images/botones/cancelar.gif" style="cursor:hand" name="cancelar" title="Limpiar todos los registros" onMouseOver="botonOver(this);" onClick="forma.reset();" onMouseOut="botonOut(this);" >&nbsp;
<img src="<%=BASEURL%>/images/botones/salir.gif" style="cursor:hand" name="salir" title="Salir al Menu Principal" onClick="parent.close();" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);"></div>
</p>
  <%String mensaje = (String) request.getAttribute("mensaje"); 
 %>
    <%  if(mensaje!=null){%>
            <br>
            <table border="2" align="center">
              <tr>
                <td><table width="100%"  border="1" align="center"  bordercolor="#F7F5F4" bgcolor="#FFFFFF">
                    <tr>
                      <td width="229" align="center" class="mensajes"><%=mensaje%></td>
                      <td width="29" background="<%=BASEURL%>/images/cuadronaranja.JPG">&nbsp;</td>
                      <td width="58">&nbsp;</td>
                    </tr>
                </table></td>
              </tr>
            </table>
    <%  }%>	
</FORM>
</div>
<%=datos[1]%>
</body>
</html>