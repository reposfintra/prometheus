<!--
- Autor : Ing. Juan Manuel Escandon Perez
- Date  : 7 de Diciembre de 2005
- Copyrigth Notice : Fintravalores S.A. S.A
-->
<%--
-@(#)
--Descripcion : Pagina JSP, que maneja el insertar los registros a  la tabla flota
--%>

<%-- Declaracion de librerias--%>
<%@page session="true"%>
<%@page errorPage="../error/ErrorPage.jsp"%>
<%@page import="com.tsp.operation.model.beans.*, java.util.*, java.text.*"%>
<%@ include file="/WEB-INF/InitModel.jsp"%>
<%@page import="com.tsp.operation.model.*"%>
<%@ taglib uri="http://jakarta.apache.org/taglibs/input-0.90" prefix="input" %>
<%  
    String path = application.getRealPath(model.menuService.getRealUrl(request.getRequestURI()));
    String datos[] = model.menuService.getContenidoMenu(BASEURL,request.getRequestURI(),path);
%>
<%
String Mensaje       = (request.getParameter("Mensaje")!=null)?request.getParameter("Mensaje"):"";
%>
<html>
<head>
<title>Flota</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<script language="javascript" src="<%=BASEURL%>/js/validar.js"></script>
<script language="javaScript" type="text/javascript" src="<%=BASEURL%>/js/boton.js"></script>

<link href="<%=BASEURL%>/css/estilostsp.css" rel="stylesheet" type="text/css">
<link href="../css/estilostsp.css" rel="stylesheet" type="text/css">

</head>

<body onResize="redimensionar()" onLoad="redimensionar();form1.placa.focus();">
<div id="capaSuperior" style="position:absolute; width:100%; height:100px; z-index:0; left: 1px; top: 0px;">
 <jsp:include page="/toptsp.jsp?encabezado=Flota"/>
</div>
<div id="capaCentral" style="position:absolute; width:100%; height:87%; z-index:0; left: 0px; top: 100px; overflow: scroll;">
  <br>
  <form name="form1" method="post" action="<%=CONTROLLER%>?estado=Flota&accion=Manager">
    <input name="Opcion" type="hidden" id="Opcion">
	 <table width="75%"  border="2" align="center">
       <tr>
         <td><table width="100%"  border="0" class="tablaInferior">
            <tr>
          		<td width="48%" class="subtitulo1">Flota
          		<td width="423" class="barratitulo" colspan="2"><img src="<%=BASEURL%>/images/titulo.gif" width="32" height="20" align="left"><%=datos[0]%></td>
        	</tr>            
             <tr class="fila">
               <th width="48%" rowspan="2" scope="row"><div align="left">STANDARD/CLIENTE</div></th>
               <td colspan="2">
                 <p> <span class="Simulacion_Hiper" style="cursor:hand " onClick="window.open('<%=BASEURL%>/consultas/consultasClientes.jsp','','HEIGHT=200,WIDTH=600,SCROLLBARS=YES,RESIZABLE=YES')">Consultar clientes...</span></p></td>
             </tr>
             <tr>
               <td class="fila" colspan="2"><p class="fila">
                   <input name="cliente" type="text" id="cliente" maxlength="6" onKeyPress="return verificarTeclaNF(event,'<%=CONTROLLER%>');" class="textbox">
                   <img src="<%=BASEURL%>/images/botones/buscar.gif" width="87" height="21" align="absmiddle" style="cursor:hand" onClick="if(form1.cliente.value!=''){buscarClientNF('<%=CONTROLLER%>');}else{window.location='<%=BASEURL%>/jsp/equipos/flota/Flota2.jsp';}" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);">                   
                   <input name="clienteR" type="hidden" id="clienteR" value="<%=request.getParameter("cliente")%>">
                   <input name="standard_nom" type="hidden" id="standard_nom" value="<%=(String)request.getAttribute("std") %>">
                   <input name="remitentes" type="hidden" id="remitentes" value=" ">
                   <input name="docinterno" type="hidden" id="docinterno" value=" ">
                   <input name="fechadesp" type="hidden" id="fechadesp" value=" ">
                   <input name="destinatarios" type="hidden" id="destinatarios" value=" ">
                   <input name="trailer" type="hidden" id="trailer" value=" ">
                   <input name="conductor" type="hidden" id="conductor" value=" ">
                   <input name="ruta" type="hidden" id="ruta" value=" ">
                   <input name="toneladas" type="hidden" id="toneladas" value=" ">
                   <input name="valorpla" type="hidden" id="valorpla3" value=" ">
                   <input name="anticipo" type="hidden" id="anticipo" value=" ">
                   <input type="hidden" name="hiddenField7">
                   <input name="precintos" type="hidden" id="precintos" value=" ">
                   <input name="observacion" type="hidden" id="observacion" value=" ">
                   <input name="click_buscar" type="hidden" id="click_buscar">
				   </p>
               </td>
             </tr>
<tr class="fila">
               <td rowspan="2" ><strong>RUTA</strong>
               <td width="86"><strong>ORIGEN</strong></td>
               <td width="371">
                 <%TreeMap ciudades = model.stdjobdetselService.getCiudadesOri(); 
				  String corigen="";
				  if(request.getParameter("origen")!=null){
					corigen = request.getParameter("origen");
				  }
				  String cdest="";
				  if(request.getParameter("destino")!=null){
					cdest = request.getParameter("destino");
				  }
				  %>
                 <input:select name="ciudadOri" options="<%=ciudades%>" attributesText="style='width:100%;' onChange='buscarDestinosF()' ; class=listmenu" default='<%=corigen%>'/> </td>
             </tr>
             <tr class="Estilo6">
               <td class="fila"><strong>DESTINO</strong></td>
               <td class="fila">
                 <%TreeMap ciudadesDest = model.stdjobdetselService.getCiudadesDest(); %>
                 <input:select name="ciudadDest" options="<%=ciudadesDest%>" attributesText="style='width:100%;' class=listmenu"  default='<%=cdest%>'/> </td>
             </tr>
             <%if(ciudadesDest.size()>0){%>
             <tr class="fila">
               <td colspan="3" >                 <div align="center">                   <img src="<%=BASEURL%>/images/botones/buscar.gif" width="87" height="21" onClick="buscarStandardF()" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" style="cursor:hand"></div>
             </tr>
             <% }%>			 
             <tr class="fila">
               <td ><strong>PLACA CABEZOTE </strong></td>
               <td colspan="2"><input name="placa" type="text" id="placa" class="textbox" size="12" maxlength="12">
			   <input name="standard" type="hidden" id="standard" value=" "></td>
             </tr>
             <tr class="fila">
               <td colspan="3" >
                 <div align="center">
                   <img src="<%=BASEURL%>/images/botones/aceptar.gif" width="90" height="21" onClick="if(form1.placa.value!=''){Opcion.value='Guardar';form1.submit();}else{alert('Debe llenar la placa');form1.placa.focus();}" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" style="cursor:hand">               </div></td>
             </tr>
         </table></td>
       </tr>
     </table>
	 <br>
	 <table width="75%"  border="0" align="center">
       <tr align="center">
		 <td><img src="<%=BASEURL%>/images/botones/detalles.gif" style="cursor:hand" title="Buscar todos los productos" name="buscar"  onClick="window.location='<%=BASEURL%>/jsp/equipos/flota/ListaFlota.jsp'" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);">&nbsp;
		 <img src="<%=BASEURL%>/images/botones/salir.gif" height="21" onClick="parent.close();" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" style="cursor:hand">
		 </td>
       </tr>
     </table>
	 <br>
    </td>
        </tr>
    </table>
</form>

  <p>
    <%if(!Mensaje.equals("")){%>
  <table border="2" align="center">
    <tr>
      <td><table width="100%"  border="1" align="center"  bordercolor="#F7F5F4" bgcolor="#FFFFFF">
          <tr>
            <td width="229" align="center" class="mensajes"><%=Mensaje%></td>
            <td width="29" background="<%=BASEURL%>/images/cuadronaranja.JPG">&nbsp;</td>
            <td width="58">&nbsp;</td>
          </tr>
      </table></td>
    </tr>
  </table>
  <%}%>
  </div>
  <%=datos[1]%>  
</body>
</html>
