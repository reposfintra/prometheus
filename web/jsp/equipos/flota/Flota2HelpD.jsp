<!--  
	 - Author(s)       :      Jose de la rosa
	 - Description     :      AYUDA DESCRIPTIVA - flota
	 - Date            :      23/05/2006 
	 - Copyright Notice:      TRANSPORTES SANCHEZ POLO S.A.
-->
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN""http://www.w3.org/TR/html4/loose.dtd">
<%@page session="true"%> 
<%@page errorPage="/error/ErrorPage.jsp"%>
<%@page import="com.tsp.operation.model.beans.*, java.util.*"%>
<%@ include file="/WEB-INF/InitModel.jsp"%>
<%@ taglib uri="http://jakarta.apache.org/taglibs/input-0.90" prefix="input" %>
<html>
<head>
<title>AYUDA DESCRIPTIVA - Flota</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link href="<%= BASEURL %>/css/estilostsp.css" rel="stylesheet" type="text/css">
<script type='text/javascript' src="<%= BASEURL %>/js/boton.js"></script>
</head>

<body>
<br>
<table width="696"  border="2" align="center">
  <tr>
    <td width="811" >
      <table width="100%" border="0" align="center">
        <tr  class="subtitulo">
          <td height="24" colspan="2"><div align="center">EQUIPO - FLOTA</div></td>
        </tr>
        <tr class="subtitulo1">
          <td colspan="2">Flota</td>
        </tr>
		<tr class="subtitulo1">
          <td colspan="2">INFORMACION DE L A FLOTA </td>
        </tr>
		<tr>
           <td  class="fila">Standar/Cliente</td>
          <td  class="ayudaHtmlTexto">Campo para digitar el codigo del cliente o el standar. </td>
        </tr>
		<tr>
           <td  class="fila">Ruta Origen</td>
          <td  class="ayudaHtmlTexto">Campo para escoger el origen de la ruta a buscar el standar. </td>
        </tr>
		<tr>
           <td  class="fila">Ruta Destino</td>
          <td  class="ayudaHtmlTexto">Campo para escoger el destino de la ruta a buscar el standar. </td>
        </tr>
		<tr>
           <td  class="fila">Estandar Job</td>
          <td  class="ayudaHtmlTexto">Campo para escoger el estandar job del cliente con la ruta seleccionada. </td>
        </tr>						
        <tr>
          <td  class="fila">Placa Cabezote</td>
          <td  class="ayudaHtmlTexto">campo para digitar la placa del cabezote. Este campo es de m&aacute;ximo 12 caracteres.</td>
        </tr>
        <tr>
          <td  class="fila">Link Consultar Clientes</td>
          <td  class="ayudaHtmlTexto">Link para buscar una lista de clientes.  </td>
        </tr>		
        <tr>
          <td class="fila">Bot&oacute;n Buscar 1 </td>
          <td  class="ayudaHtmlTexto">Bot&oacute;n para Buscar el cliente y o estandar.</td>
        </tr>
		<tr>
          <td class="fila">Bot&oacute;n Buscar 2 </td>
          <td  class="ayudaHtmlTexto">Bot&oacute;n para buscar el estandar con el origen y destino seleccionado.</td>
        </tr>
		<tr>
          <td class="fila">Bot&oacute;n Aceptar </td>
          <td  class="ayudaHtmlTexto">Bot&oacute;n para guardar los datos.</td>
        </tr>		
		<tr>
          <td class="fila">Bot&oacute;n Detalle </td>
          <td  class="ayudaHtmlTexto">Bot&oacute;n para ir a la busqueda de standar y placas.</td>
        </tr>			
		<tr>
          <td width="149" class="fila">Bot&oacute;n Salir</td>
          <td width="525"  class="ayudaHtmlTexto">Bot&oacute;n para salir de la vista 'Flota' y volver al menu principal.</td>
        </tr>
      </table>
    </td>
  </tr>
</table>
<p></p>
<center>
<img src = "<%=BASEURL%>/images/botones/salir.gif" style = "cursor:hand" name = "imgsalir" onClick = "parent.close();" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);">
</center>
</body>
</html>
