<!--  
	 - Author(s)       :      Jose de la Rosa
	 - Description     :      AYUDA FUNCIONAL - flota
	 - Date            :      08/06/2006 
	 - Copyright Notice:      TRANSPORTES SANCHEZ POLO S.A.
-->
<%@ include file="/WEB-INF/InitModel.jsp"%>
<HTML>
<HEAD>
<TITLE>AYUDA FUNCIONAL - flota</TITLE>
<META http-equiv=Content-Type content="text/html; charset=windows-1252">
<link href="../css/estilostsp.css" rel="stylesheet" type="text/css"> 
<link href="<%=BASEURL%>/css/estilostsp.css" rel="stylesheet" type="text/css">
<script type='text/javascript' src="<%= BASEURL %>/js/boton.js"></script> 
</HEAD>
<BODY> 
<% String BASEIMG = BASEURL +"/images/ayuda/equipos/flota/"; %>
  <table width="95%"  border="2" align="center">
    <tr>
      <td height="117" >
        <table width="100%" border="0" align="center">
          <tr  class="subtitulo">
            <td height="20"><div align="center">AYUDA EQUIPOS � FLOTA </div></td>
          </tr>
            <tr>
            <td  class="ayudaHtmlTexto"><p align="center">&nbsp;</p>
              <p align="center">Al entrar al programa, aparece la siguiente pantalla: </p>
              <p align="center"><img src="<%=BASEIMG%>1.png"><br>
              </p>
              <div align="center">
                <p>Al entrar al link �Consultar clientes� aparece en campo para escribir el nombre del cliente: </p>
                <p><img src="<%=BASEIMG%>2.png"></p>
              </div>
              <p align="center">Luego, el programa muestra lo siguiente, dando la opci&oacute;n de realizar otra consulta: </p>
              <p align="center"><img src="<%=BASEIMG%>3.png"><br>
  Si se conoce el est&aacute;ndar del cliente, no es necesario entrar a la opci&oacute;n de �Consultar clientes� solo se digita el n&uacute;mero en el campo �Standard/ Cliente�. </p>
              <p align="center"><img src="<%=BASEIMG%>4.png"><br>
                <br>
              Luego, se seleccionan el origen y el destino de los viajes que realizar&aacute; la placa. </p>
              <div align="center"><img src="<%=BASEIMG%>5.png"><br>
              </div>
              <p align="center">Seguidamente, se hace clic en buscar, en la casilla est&aacute;ndar job se despliega una lista. De ah&iacute; se escoge el deseado, se digita la placa y se hace clic en aceptar. </p>
              <p align="center">Si se quiere salir del programa, se hace clic en salir; si se quiere ver la flota se hace clic en ver flota: </p>
              <p align="center"><img src="<%=BASEIMG%>7.png"><br>
              </p>
              <div align="center">              Creado el registro, aparece lo siguiente: </div>
              <p align="center"><img src="<%=BASEIMG%>10.png"><br>
              </p>
              <div align="center">Al hacer clic en ver flota, el programa arroja la siguiente pantalla: </div>
              <p align="center"><img src="<%=BASEIMG%>8.png"><br>
              </p>
              <div align="center">              Al hacer clic en buscar aparecen los registros creados. </div>
              <p align="center">  Para anular, restablecer o eliminar, es necesario seleccionar el registro correspondiente. </p>              
  <p align="center" class="ayudaHtmlTexto"><img src="<%=BASEIMG%>9.png"></p>
            </td>
          </tr>
      </table></td>
    </tr>
  </table>
  <p></p>
<center>
<img src = "<%=BASEURL%>/images/botones/salir.gif" style = "cursor:hand" name = "imgsalir" onClick = "parent.close();" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);">
</center>
</BODY>
</HTML>
