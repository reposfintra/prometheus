<!--
- Autor : Ing. Juan Manuel Escandon Perez
- Date  : 7 de Diciembre de 2005
- Copyrigth Notice : Fintravalores S.A. S.A
-->
<%--
-@(#)
--Descripcion : Pagina JSP, que maneja el listado y las funciones basicas sobre las flotas
--%>

<%-- Declaracion de librerias--%>
<%@page contentType="text/html"%>
<%@page session="true"%>
<%@page errorPage="/error/ErrorPage.jsp"%>
<%@page import="java.util.*"%>
<%@page import="com.tsp.operation.model.beans.*"%>
<%@page import="com.tsp.util.*"%>
<%@page import="com.tsp.operation.model.*"%>
<%@ include file="/WEB-INF/InitModel.jsp"%>
<% 

List Listado = model.FlotaSvc.getList();
String Mensaje       = (request.getParameter("Mensaje")!=null)?request.getParameter("Mensaje"):"";
if( Listado==null && Listado.size()==0){
	Mensaje = "No hay Registros";
}	
%>

<html>
<head>
<title>Listado de Flota</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
    <link href="/css/estilostsp.css" rel="stylesheet" type="text/css">
    <link href="<%=BASEURL%>/css/estilostsp.css" rel="stylesheet" type="text/css">      
   <script language="javaScript" type="text/javascript" src="<%=BASEURL%>/js/boton.js"></script>
   <script>
		function SelAll2(){
				for(i=0;i<FormularioListado.length;i++)
						FormularioListado.elements[i].checked=FormularioListado.All.checked;
		}
    	function ActAll2(){
            FormularioListado.All.checked = true;
            for(i=1;i<FormularioListado.length;i++)	
                    if (!FormularioListado.elements[i].checked){
                            FormularioListado.All.checked = false;
                            break;
                    }
    	}   
		
		function validarlistado2(){			
        	for(i=0;i<=FormularioListado.length-1;i++)    
        		if(FormularioListado.elements[i].type=='checkbox' && FormularioListado.elements[i].checked){
         	   		FormularioListado.submit();
					return true;
			    }
   	 			alert('Debe seleccionar por lo menos un item...')                                    
   		 	return false;
				//FormularioListado.submit();
    	}      
</script>   
</head>

<body onResize="redimensionar()" onLoad="redimensionar()">
<div id="capaSuperior" style="position:absolute; width:100%; height:100px; z-index:0; left: 1px; top: 0px;">
 <jsp:include page="/toptsp.jsp?encabezado=Listado de Flota"/>
</div>
<div id="capaCentral" style="position:absolute; width:100%; height:87%; z-index:0; left: 0px; top: 100px; overflow: scroll;">
<form action="<%=CONTROLLER%>?estado=Flota&accion=Manager" method='post' name='FormularioListado' id="FormularioListado">
  <div align="center"><br>
  </div>
  <div align="center">
    <table width="600" border="2">
      <tr>
        <td>
		
		
		<table width="100%"  border="0" class="tablaInferior">
           <tr>
          		<td width="48%" class="subtitulo1">Listado</td>
            	<td width="52%" class="barratitulo"><img src="<%=BASEURL%>/images/titulo.gif" width="32" height="20"></td>
        	</tr>    
		  </table>
		  
		  
		  <table width="100%">
			  <tr class="fila">
			  	<input type='hidden' name='Opcion'/>  
				<td>&nbsp;FILTROS</td>
				<td>&nbsp;Placa</td>
				<td>&nbsp;<input type="text" id="placa" class="textbox"  name="placa" size="10" maxlength="10"></td>
				<td>&nbsp;Std_job_no</td>
				<td>&nbsp;<input type="text" id="standard" class="textbox"  name="standard" size="10" maxlength="10"></td>
				<td>&nbsp;<img src="<%=BASEURL%>/images/botones/buscar.gif" width="90" height="21" onClick="Opcion.value='Listado';FormularioListado.submit();" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" style="cursor:hand"></td>
			  </tr>
		  </table>
		  
		  
		  <% if(Listado!=null && Listado.size()>0) { %>
          <table width="100%"  border="0" class="tablaInferior">
			  <tr class="tblTitulo">
				<th width="41">N&ordm;</th>
				<th width="41"><input type='checkbox' name='All' id="All" onclick='jscript: SelAll2();'>
				</th>
				<th width='400' class="tblTitulo">ESTANDARD JOB</th>
				<th width='108'>PLACA</th>
			  </tr>		  
			  <%
			   int Cont = 1;
				Iterator it2 = Listado.iterator();
				while(it2.hasNext()){
					Flota dat  = (Flota) it2.next();
					String e = (Cont % 2 == 0 )?"filagris":"filaazul";
					String Estilo = (dat.getReg_status().equals("A"))?"filaresaltada":e;
					%>
			  <tr class='<%=Estilo%>'>
				<td align='center' class="bordereporte"><%=Cont++%></td>
				<td align='center' class="bordereporte"><input type='checkbox' name='LOV' id="LOV" value='<%=dat.getPlaca()+"/"+dat.getStd_job_no()%>' onclick='jscript: ActAll2();'></td>
				<td class="bordereporte"><%=dat.getStd_job_no() + " - " + dat.getDescripcion()  %></td>
				<td align='center' class="bordereporte"><%= dat.getPlaca() %> </td>
			  </tr>
			  <%  }   %>
        	</table>
			</td>
		</tr>
    </table>
    <br>           
	  <img src="<%=BASEURL%>/images/botones/anular.gif" width="90" height="21" onClick="Opcion.value='Anular';validarlistado2();" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" style="cursor:hand">
	  <img src="<%=BASEURL%>/images/botones/restablecer.gif" width="90" height="21" onClick="Opcion.value='Activar';validarlistado2();" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" style="cursor:hand">
	  <img src="<%=BASEURL%>/images/botones/eliminar.gif" width="90" height="21" onClick="Opcion.value='Eliminar';validarlistado2();" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" style="cursor:hand">
	  <img src="<%=BASEURL%>/images/botones/salir.gif" width="90" height="21" onClick="parent.close();" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" style="cursor:hand"></p>
  </div>
</form>
<%}else{%>
	</td>
	</tr>
	</table>
	<%}%>
   <%if(!Mensaje.equals("")){%>
   <br>
  <table border="2" align="center">
    <tr>
      <td><table width="100%"  border="1" align="center"  bordercolor="#F7F5F4" bgcolor="#FFFFFF">
          <tr>
            <td width="229" align="center" class="mensajes"><%=Mensaje%></td>
            <td width="29" background="<%=BASEURL%>/images/cuadronaranja.JPG">&nbsp;</td>
            <td width="58">&nbsp;</td>
          </tr>
      </table>
	 </td>
    </tr>
  </table>
	  <%}%>
	  <br>
	      <table width="600" border="0">
      <tr align="left">
        <td><img src="<%=BASEURL%>/images/botones/regresar.gif" style="cursor:hand" title="Volver" name="buscar"  onClick="window.location='<%=BASEURL%>/jsp/equipos/flota/Flota.jsp';" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);"></td>
      </tr>
    </table>
</div>
</body>
</html>
