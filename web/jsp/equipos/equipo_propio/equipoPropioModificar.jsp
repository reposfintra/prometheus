<!--
- Autor : Osvaldp P�rez Ferrer
- Date : 30 de Junio de 2006
- Copyrigth Notice : Fintravalores S.A. S.A
-->
<%--
-@(#)
--Descripcion : Pagina JSP, que sirve para modificar equipo propio
--%>

<%@ page session="true"%>
<%@ page errorPage="../error/ErrorPage.jsp"%>
<%@page import="com.tsp.util.*"%>
<%@ include file="/WEB-INF/InitModel.jsp"%>
<%@ taglib uri="http://jakarta.apache.org/taglibs/input-0.90" prefix="input" %>
<%@page import="com.tsp.operation.model.*, com.tsp.operation.model.beans.*, com.tsp.util.*,java.util.*, java.text.*"%>

<html>
<head>
    <title>Modificaci&oacute;n de Equipos Propios</title>
    <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">

    <script src="<%=BASEURL%>/js/utilidades.js"></script>
    <link href="../css/estilostsp.css" rel="stylesheet" type="text/css">
    <script type='text/javascript' src="<%= BASEURL %>/js/boton.js"></script>
    <link href="<%=BASEURL%>/css/estilostsp.css" rel="stylesheet" type="text/css">
<%
    String mensaje = (String) request.getAttribute("mensaje");
    String modified = (String) request.getAttribute("modified");
    String clase = (String) request.getAttribute("clase");        
    LinkedList clases = model.tablaGenService.obtenerTablas();
    
    TablaGen equipo = model.tablaGenService.getTblgen();
%>
</head>

<body>
    <div id="capaSuperior" style="position:absolute; width:100%; height:100px; z-index:0; left: 1px; top: 0px;">
        <jsp:include page="/toptsp.jsp?encabezado=Modificar Equipo Propio"/>
    </div>
    <div id="capaCentral" style="position:absolute; width:100%; height:87%; z-index:0; left: 0px; top: 100px; overflow: scroll;">        
    
    <form id="form1" method="post" action="<%=CONTROLLER%>?estado=Equipo&accion=Propio&opcion=save&placa=<%=equipo.getTable_code()%>">
    <%if(equipo != null){%>
    <table width="50%"  border="2" align="center">
        <tr>
            <td><table width="100%"  border="1" align="center" class="tablaInferior">
            <tr>
            <td><table width="100%" border="0" cellpadding="0" cellspacing="0">
            <tr>
            <td height="22" colspan=2 class="subtitulo1"><div align="center" class="subtitulo1">
                <strong>Equipo Propio</strong>                     
            </div></td>
            <td width="212" class="barratitulo">
            <img src="<%=BASEURL%>/images/titulo.gif" width="32" height="20" align="left">
                    
        </tr>
            </table>
            <table width="100%"  border="0" align="center" cellpadding="2" cellspacing="3" onclick="document.getElementById('noexiste').style.visibility='hidden';">
                
            
            
                <tr class="fila">
                    <td width="31%">Placa : </td>
                    <td><%=equipo.getTable_code()%></td>
                    
                </tr>
                <tr class="fila">
                    <td>Clase:</td>
                    <td><select name="clase" id="clase">
                    <%for(int i=0;i<clases.size();i++){
                        TablaGen t = new TablaGen ();
                        t = (TablaGen) clases.get(i);
                    %>                        
                        <option value="<%=t.getTable_code()%>"><%=t.getDescripcion()%></option>
                    <%}%>
                    </select></td>
                </tr>
                <tr class="fila">
                    <td>N&uacute;mero:</td>
                    <td><input name="numero" id="numero" value="<%=equipo.getDato()%>" type="text" size="12" maxlength="3"></td>
                </tr>
                  
            </table></td>
            </tr>
        </table></td>
        </tr>
    </table>
    <%}%>
    <br>
    <div align="center" id="botones">      
        <img src="<%=BASEURL%>/images/botones/modificar.gif" id="mod" name="c_aceptar" style="cursor:pointer" onClick="if(validar()){form1.submit()};" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);"> 
        <img src="<%=BASEURL%>/images/botones/anular.gif" id="anu" name="c_cancelar" style="cursor:pointer" onClick="if(validar()){location.replace('<%=CONTROLLER%>?estado=Equipo&accion=Propio&opcion=delete&placa=<%=equipo.getTable_code()%>')};" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);"> 
        <img src="<%=BASEURL%>/images/botones/salir.gif" name="c_salir" style="cursor:pointer" onClick="parent.close();" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);">
    </div>
    </form>
    <br/>
	
        
    <div id="noexiste">
       <%if(mensaje != null){ %>
        
        <table border="2" align="center">
            <tr>                
                <td height="45"><table width="410" height="41" border="1" align="center"  bordercolor="#F7F5F4" bgcolor="#FFFFFF">
                    <tr>
                        <td width="282" height="35" align="center" class="mensajes"><%=mensaje%></td>
                        <td width="28" background="<%=BASEURL%>/images/cuadronaranja.JPG">&nbsp;</td>
                        <td width="78">&nbsp;</td>
                    </tr>
                </table></td>
            </tr>
        </table>        
        
       <%}%>     
    </div>
    	      
    <%if(modified != null){%>
       <script>            
            parent.opener.location.reload("<%=CONTROLLER%>?estado=Equipo&accion=Propio&opcion=filter&clase=<%=clase%>");            
            document.getElementById("anu").src="<%=BASEURL%>/images/botones/anularDisable.gif";
            document.getElementById("anu").onclick = "";
            document.getElementById("anu").onmouseover = "";
            document.getElementById("anu").onmouseout = "";
            document.getElementById("mod").src="<%=BASEURL%>/images/botones/modificarDisable.gif";
            document.getElementById("mod").onclick = "";
            document.getElementById("mod").onmouseover = "";
            document.getElementById("mod").onmouseout = "";
        </script>
        <%}%>

</body>
</html>

<script>
    <%if(equipo != null){%>
    form1.clase.value = "<%=equipo.getReferencia()%>";
    <%}%>
    function validar(){        
        if(form1.numero.value == ""){
            alert("Debe digitar el n�mero de equipo");
            return false;
        }
        if(form1.numero.value.length < 3){
            alert("En n�mero de equipo debe tener tres caracteres");
            return false;
        }
        return true;
    }       
</script>