<%@ include file="/WEB-INF/InitModel.jsp"%>

<HTML>
<HEAD>
<TITLE>Funcionamiento de la ventana de Ingreso de Equipo Propio</TITLE>
<META http-equiv=Content-Type content="text/html; charset=windows-1252">
<link href="../../../css/estilostsp.css" rel="stylesheet" type="text/css"> 
<link href="<%=BASEURL%>/css/estilostsp.css" rel="stylesheet" type="text/css"> 
<script type='text/javascript' src="<%=BASEURL%>/js/boton.js"></script>
</HEAD>
<BODY>  

  <table width="95%"  border="2" align="center">
    <tr>
      <td>
        <table width="100%" border="0" align="center">
          <tr  class="subtitulo">
            <td height="20"><div align="center">INGRESAR EQUIPO PROPIO </div></td>
          </tr>
          <tr class="subtitulo1">
            <td>Descripci&oacute;n del funcionamiento de la p&aacute;gina para ingreso de equipos propios.</td>
          </tr>
          <tr>
            <td  height="18"  class="ayudaHtmlTexto"><p>&nbsp;</p>
            <p>Ingresar placa del equipo, la placa del equipo es obligatoria, y debe encontrarse registrada en el sistema.</p>
            <p>Seleccionar la clase de equipo. </p></td>
          </tr>
          <tr>
            <td height="18"  class="ayudaHtmlTexto"><div align="center"><img src="<%=BASEURL%>/images/ayuda/equipos/equipo_propio/img1.JPG" width="553" height="248" ></div></td>
          </tr>
          <tr>
            <td  class="ayudaHtmlTexto">&nbsp;</td>
          </tr>
          <tr>
            <td  class="ayudaHtmlTexto"><p>Finalmente ingrese el n&uacute;mero de equipo. El n&uacute;mero no debe estar asignado a un equipo que ya existe. </p>
              <p>Se mostrar&aacute; el siguente mensaje de confirmaci&oacute;n: </p></td>
          </tr>
          <tr>
            <td  class="ayudaHtmlTexto">&nbsp;</td>
          </tr>
          <tr>
            <td  class="ayudaHtmlTexto"><div align="center"><img src="<%=BASEURL%>/images/ayuda/equipos/equipo_propio/img2.JPG" width="454" height="88" ></div></td>
          </tr>   
          
      </table></td>
    </tr>
  </table>
  <table width="416" align="center">
	<tr>
	<td align="center">
		<img src="<%=BASEURL%>/images/botones/salir.gif" style="cursor:pointer" name="c_salir" onClick="window.close();" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" align="absmiddle">
	</td>
	</tr>
</table>
</BODY>
</HTML>
