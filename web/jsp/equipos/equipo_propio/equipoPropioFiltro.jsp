<!--
- Autor : Osvaldp P�rez Ferrer
- Date : 11 de Julio de 2006
- Copyrigth Notice : Fintravalores S.A. S.A
-->
<%--
-@(#)
--Descripcion : Pagina JSP, que sirve para seleccionar la clase de equipos a consultar
--%>

<%@ page session="true"%>
<%@ page errorPage="../error/ErrorPage.jsp"%>
<%@page import="com.tsp.util.*"%>
<%@ include file="/WEB-INF/InitModel.jsp"%>
<%@ taglib uri="http://jakarta.apache.org/taglibs/input-0.90" prefix="input" %>
<%@page import="com.tsp.operation.model.*, com.tsp.operation.model.beans.*, com.tsp.util.*,java.util.*, java.text.*"%>

<html>
<head>
    <title>B&uacute;squeda de Equipos Propios</title>
    <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">

    <script src="<%=BASEURL%>/js/utilidades.js"></script>
    <link href="../css/estilostsp.css" rel="stylesheet" type="text/css">
    <script type='text/javascript' src="<%= BASEURL %>/js/boton.js"></script>
    <link href="<%=BASEURL%>/css/estilostsp.css" rel="stylesheet" type="text/css">
<%
    String mensaje = (String) request.getAttribute("mensaje");
   
    LinkedList clases = model.tablaGenService.obtenerTablas();
%>
</head>

<body>
    <div id="capaSuperior" style="position:absolute; width:100%; height:100px; z-index:0; left: 1px; top: 0px;">
        <jsp:include page="/toptsp.jsp?encabezado=Listar Equipos Propios"/>
    </div>
    <div id="capaCentral" style="position:absolute; width:100%; height:87%; z-index:0; left: 0px; top:100px; overflow: scroll;">        
    <form id="form1" method="post" action="<%=CONTROLLER%>?estado=Equipo&accion=Propio&opcion=filter">
    <table width="50%"  border="2" align="center">
        <tr>
            <td><table width="100%"  border="1" align="center" class="tablaInferior">
            <tr>
            <td><table width="100%" border="0" cellpadding="0" cellspacing="0">
            <tr>
            <td height="22" colspan=2 class="subtitulo1"><div align="left" class="subtitulo1">
                <strong>Clase de Equipo</strong>                     
            </div></td>
            <td width="212" class="barratitulo">
            <img src="<%=BASEURL%>/images/titulo.gif" width="32" height="20" align="left">
                    
        </tr>
            </table>
            <table width="100%"  border="0" align="center" cellpadding="2" cellspacing="3" onclick="document.getElementById('noexiste').style.visibility='hidden';">
                                        
                
                <tr class="fila">
                    <td>Clase:</td>
                    <td><select name="clase" id="clase">
                    <%for(int i=0;i<clases.size();i++){
                        TablaGen t = new TablaGen ();
                        t = (TablaGen) clases.get(i);
                    %>                        
                        <option value="<%=t.getTable_code()%>"><%=t.getDescripcion()%></option>
                    <%}%>
                    </select></td>
                </tr>
                
                  
            </table></td>
            </tr>
        </table></td>
        </tr>
    </table>
    <br>
    <div align="center">      
        <img id="baceptar" name="baceptar"  src="<%=BASEURL%>/images/botones/buscar.gif" style="cursor:hand"  onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" onClick="form1.submit();">            
       
        <img src="<%=BASEURL%>/images/botones/salir.gif"  height="21" name="imgsalir"  onMouseOver="botonOver(this);" onClick="window.close();" onMouseOut="botonOut(this);" style="cursor:hand">            
    </div>
    </form>
    <br/>
	
        
    <div id="noexiste">
       <%if(mensaje != null){ %>
        
        <table border="2" align="center">
            <tr>                
                <td height="45"><table width="410" height="41" border="1" align="center"  bordercolor="#F7F5F4" bgcolor="#FFFFFF">
                    <tr>
                        <td width="282" height="35" align="center" class="mensajes"><%=mensaje%></td>
                        <td width="28" background="<%=BASEURL%>/images/cuadronaranja.JPG">&nbsp;</td>
                        <td width="78">&nbsp;</td>
                    </tr>
                </table></td>
            </tr>
        </table>        
               
       <%}%>     
    </div>
    	      
    </div>

</body>

</html>
