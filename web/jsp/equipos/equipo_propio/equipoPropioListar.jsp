<!--
- Autor : Osvaldp P�rez Ferrer
- Date : 30 de Junio de 2006
- Copyrigth Notice : Fintravalores S.A. S.A
-->
<%--
-@(#)
--Descripcion : Pagina JSP, que sirve para ingresar equipos propios
--%>

<%@ page session="true"%>
<%@ page errorPage="../error/ErrorPage.jsp"%>
<%@page import="com.tsp.util.*"%>
<%@ include file="/WEB-INF/InitModel.jsp"%>
<%@ taglib uri="http://jakarta.apache.org/taglibs/input-0.90" prefix="input" %>
<%@page import="com.tsp.operation.model.*, com.tsp.operation.model.beans.*, com.tsp.util.*,java.util.*, java.text.*"%>

<html>
<head>
    <title>Buscar Equipos Propios</title>
    <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">

    <script src="<%=BASEURL%>/js/utilidades.js"></script>
    <link href="../css/estilostsp.css" rel="stylesheet" type="text/css">
    <script type='text/javascript' src="<%= BASEURL %>/js/boton.js"></script>
    <link href="<%=BASEURL%>/css/estilostsp.css" rel="stylesheet" type="text/css">
<%
    LinkedList equipos = model.tablaGenService.obtenerTablas();
%>
</head>

<body>
    <div id="capaSuperior" style="position:absolute; width:100%; height:100px; z-index:0; left: 1px; top: 0px;">
        <jsp:include page="/toptsp.jsp?encabezado=Equipos Propios"/>
    </div>
    <div id="capaCentral" style="position:absolute; width:100%; height:87%; z-index:0; left: 0px; top: 100px; overflow: scroll;">        
    <form id="form1" method="post">
    
    <%if(equipos != null && equipos.size() > 0){%>
    <table width="50%"  border="2" align="center">
        <tr>
            <td><table width="100%"  border="0" align="center" class="tablaInferior">
            <tr>
            <td><table width="100%" border="0" cellpadding="0" cellspacing="0">
            <tr>
            <td height="22" colspan=2 class="subtitulo1"><div align="left" class="subtitulo1">
                <strong>Equipos Propios</strong>                     
            </div></td>
            <td width="212" class="barratitulo">
            <img src="<%=BASEURL%>/images/titulo.gif" width="32" height="20" align="left">
                    
        </tr>
            </table>
            <table width="99%" border="1" align="center" bordercolor="#999999" bgcolor="#F7F5F4">    
                <tr class="tblTitulo" align="center">
                    <td >Placa</td>
                    <td >Clase</td>
                    <td >N�mero</td>   
                   
                </tr>
                    <%                                    
                    for(int i=0; i<equipos.size(); i++){
                        TablaGen t = (TablaGen) equipos.get(i);                        
                        %>
                <tr class="<%=i%2==0?"filagris":"filaazul"%>" onMouseOver='cambiarColorMouse(this)' 
                    style="cursor:hand" 
                    onclick="window.open('<%=CONTROLLER%>?estado=Equipo&accion=Propio&opcion=update&numero=<%=t.getDato()%>&placa=<%=t.getTable_code()%>','myWindow','width=650,height=550,resizable=yes');"
                    title="Click para Modificar" align="center">    
                    
                <td width="30%" class="bordereporte"><%=t.getTable_code()%>&nbsp;</td>    
                <td width="40%" class="bordereporte"><%=t.getDescripcion()%>&nbsp;</td>
                <td width="30%" class="bordereporte"><%=t.getDato()%></td>
                
                </tr>                                          
            </td>
            </tr>            
                <%}%>
                      </table>                        
            </table>               </td>
            </tr>
        </table>
        <%}
          else{%>
         <table border="2" align="center">
            <tr>                
                <td height="45"><table width="410" height="41" border="1" align="center"  bordercolor="#F7F5F4" bgcolor="#FFFFFF">
                    <tr>
                        <td width="282" height="35" align="center" class="mensajes">Su b�squeda no produjo resultados</td>
                        <td width="28" background="<%=BASEURL%>/images/cuadronaranja.JPG">&nbsp;</td>
                        <td width="78">&nbsp;</td>
                    </tr>
                </table></td>
            </tr>
        </table>        
        <%  
          }
        %>
        
            </td>
        </tr>
    </table>
    <br>
    <div align="center">
        <img src="<%=BASEURL%>/images/botones/regresar.gif"  height="21" onMouseOver="botonOver(this);" onclick="location.replace('<%=CONTROLLER%>?estado=Equipo&accion=Propio&opcion=filter_again');" onMouseOut="botonOut(this);" style="cursor:hand">                         
        <img src="<%=BASEURL%>/images/botones/salir.gif"  height="21" name="imgsalir"  onMouseOver="botonOver(this);" onClick="window.close();" onMouseOut="botonOut(this);" style="cursor:hand">                    
    </div>
    </form>
    <br/>
	
   
</body>
</html>