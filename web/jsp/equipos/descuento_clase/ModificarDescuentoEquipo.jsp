<!--
- Nombre P�gina :                  ModificarDescuentoEquipo.jsp                  
- Descripci�n :                    Pagina JSP, que modifica o anula un descuento de equipo                 
- Autor :                          Ing. Armando Oviedo C                         
- Fecha Creado :                   8 de Diciembre de 2005                  
- Modificado por :                 LREALES                                   
- Fecha Modificado :               23 de mayo de 2006, 07:12 AM              
- Versi�n :                        1.0                                       
- Copyright :                      Fintravalores S.A.                   
-->

<%-- Declaracion de librerias--%>
<%@page contentType="text/html"%>
<%@page session="true"%>
<%@page errorPage="../error/ErrorPage.jsp"%>
<%@page import="com.tsp.operation.model.*"%>
<%@page import="com.tsp.util.*"%>
<%@page import="com.tsp.operation.model.beans.*, java.util.*"%>
<%@include file="/WEB-INF/InitModel.jsp"%>
<%@ taglib uri="http://jakarta.apache.org/taglibs/input-0.90" prefix="input" %>
<%
  String path = application.getRealPath(model.menuService.getRealUrl(request.getRequestURI()));
  String datos[] = model.menuService.getContenidoMenu(BASEURL,request.getRequestURI(),path);
%>
<html>
<head>
<title>Descuentos De Equipos</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link href="<%= BASEURL %>/css/estilostsp.css" rel="stylesheet" type="text/css">
<link href="../../css/estilostsp.css" rel="stylesheet" type="text/css"> 
<script type='text/javascript' src="<%= BASEURL %>/js/boton.js"></script>
</head>
<script>
		var controlador ="<%=CONTROLLER%>";
		
		function Modificar ( codigo, descripcion, conc_contable, conc_especial ){
		
			if( ( codigo != "" ) && ( descripcion != "" ) && ( conc_contable != "" ) && ( conc_especial != "" )  ){
			
				document.forma.action = controlador+"?estado=Descuento&accion=EquipoModificar&mensaje=modificar&codigo="+codigo+"&descripcion="+descripcion+"&conc_contable="+conc_contable+"&conc_especial="+conc_especial;
				document.forma.submit();
				
			} else{
			
				alert( "Por Favor No Deje Ningun Campo Vacio!" );
				
			}
		}
</script>
<body onload="<%if(request.getParameter("reload")!=null){%> window.opener.location.reload();<%}%> redimensionar();" onresize="redimensionar()">
<div id="capaSuperior" style="position:absolute; width:100%; height:100px; z-index:0; left: 0px; top: 0px;">
	<jsp:include page="/WEB-INF/toptsp.jsp?encabezado=Modificar Descuento De Equipo"/>
</div>
<div id="capaCentral" style="position:absolute; width:100%; height:87%; z-index:0; left: 0px; top: 100px; overflow: scroll;">
<%
	DescuentoEquipo tmp = model.descuentoequiposvc.getDescuentoEquipo();	
	String mensajemod = request.getParameter("mensajemod");
	if(mensajemod!=null){
		out.print("<table width=450 border=2 align=center><tr><td><table width=100%  border=1 align=center  bordercolor=#F7F5F4 bgcolor=#FFFFFF><tr><td width=350 align=center class=mensajes>"+mensajemod+"</td><td width=100><img src="+BASEURL+"/images/cuadronaranja.JPG></td></tr></table></td></tr></table>");%>		
		<br>
		<table width='450' align=center>
			<tr>
				<td align=center><img title='Regresar' src="<%= BASEURL %>/images/botones/salir.gif" onmouseover="botonOver(this);" onmouseout="botonOut(this);" onclick="window.close();"></img></td>
			</tr>
		</table>
	<%} 
	else if(tmp==null){
		out.print("<table width=450 border=2 align=center><tr><td><table width=100%  border=1 align=center  bordercolor=#F7F5F4 bgcolor=#FFFFFF><tr><td width=350 align=center class=mensajes>Descuento No Existe</td><td width=100><img src="+BASEURL+"/images/cuadronaranja.JPG></td></tr></table></td></tr></table>");%>
		<br>
		<table width='450' align=center>
			<tr>
				<td align=center>
				</img><img title='Regresar' src="<%= BASEURL %>/images/botones/regresar.gif" style = "cursor:hand" onmouseover="botonOver(this);" onmouseout="botonOut(this);" onclick="window.history.back();">
				 <img title='Regresar' style = "cursor:hand" src="<%= BASEURL %>/images/botones/salir.gif" onmouseover="botonOver(this);" onmouseout="botonOut(this);" onclick="window.close();">
				</td>
			</tr>
		</table>
	<%}
	else{			
%>
<form name="forma" action='<%= BASEURL %>/jsp/equipos/descuentos_equipo/ModificarDescuentoEquipo.jsp' method="post">
<input type="hidden" name="codigo" value="<%=tmp.getCodigo()%>"></input>
  <table width="50%"  border="2" align="center">
  	<tr>
    	<td>
			<table width="100%" align="center">
				<tr>
					<td width="50%" class="subtitulo1" nowrap>Informaci�n Del Descuento</td>
					<td width="50%" class="barratitulo"><img src="<%=BASEURL%>/images/titulo.gif" width="32" height="20" align="left"><%=datos[0]%></td>
				</tr>
				<tr class="fila">
				  <td>C�digo</td>
				  <td><%=tmp.getCodigo()%></td>
			  </tr>
				<tr class="fila">
				  <td>Descripci�n</td>
				  <td><textarea name="descripcion" class="textbox" cols="50" rows="3"><%=tmp.getDescripcion()%></textarea></td>
			  </tr>
				<tr class="fila">
				  <td>Contepto contable</td>
				  <td><input name="conc_contable" type="text" value="<%=tmp.getConcContable()%>" size="15" maxlength="15"></td>
			  </tr>
				<tr class="fila">
				  <td>Concepto especial</td>
				  <td><input name="conc_especial" type="text" value="<%=tmp.getConcEspecial()%>" size="15" maxlength="15"></td>
			  </tr>
		  </table>
  		</td>
  	</tr>
  </table><br>
  <table align=center width='450'>
    <tr>
        <td align="center" colspan="2">
            <img title='Modificar' style = "cursor:hand" src="<%= BASEURL %>/images/botones/modificar.gif" onmouseover="botonOver(this);" onmouseout="botonOut(this);" onclick="Modificar( '<%=tmp.getCodigo()%>', descripcion.value, conc_contable.value, conc_especial.value );"></img>                        
			<img title='Eliminar' style = "cursor:hand" src="<%= BASEURL %>/images/botones/eliminar.gif" onmouseover="botonOver(this);" onmouseout="botonOut(this);" onclick="document.forma.action='<%=CONTROLLER%>?estado=Descuento&accion=EquipoModificar&mensaje=eliminar&codigo=<%=tmp.getCodigo()%>';document.forma.submit();"></img>
            <img title='Salir' style = "cursor:hand" src="<%= BASEURL %>/images/botones/salir.gif" onmouseover="botonOver(this);" onmouseout="botonOut(this);" onClick="window.close()"></img>
        </td>
    </tr>
  </table>
  
  </form>
  <br>
  
  <%}%>  
</div>
<%=datos[1]%>
</body>
</html>