<!--
- Autor : Ing. Jose de la rosa
- Date  : 2 de Diciembre de 2005
- Copyrigth Notice : Fintravalores S.A. S.A
-->
<%--
-@(#)
--Descripcion : Pagina JSP, que maneja el ingreso de las flotas directas
--%>

<%-- Declaracion de librerias--%>
<%@page contentType="text/html"%>
<%@ page session="true"%>
<%@page import="com.tsp.operation.model.beans.*, java.util.*"%>
<%@ include file="/WEB-INF/InitModel.jsp"%>
<%@page import="com.tsp.operation.model.*"%>
<%@page import="com.tsp.util.*"%>
<%@ taglib uri="http://jakarta.apache.org/taglibs/input-0.90" prefix="input" %>
<%
	java.util.Date date = new java.util.Date();
	java.text.SimpleDateFormat s = new java.text.SimpleDateFormat("yyyy-MM-dd HH:mm");
	String fechareporte =s.format(date);
	String path = application.getRealPath(model.menuService.getRealUrl(request.getRequestURI()));
	String datos[] = model.menuService.getContenidoMenu(BASEURL,request.getRequestURI(),path);
	model.tablaGenService.buscarRegistros("CLAEQUI");
%>
<html>
<head>
<title>Insertar Descuento por clase equipos</title>
<link href="<%= BASEURL %>/css/estilostsp.css" rel="stylesheet" type="text/css">
<script type='text/javascript' src="<%= BASEURL %>/js/boton.js"></script>
</head>
<script type='text/javascript' src="<%= BASEURL %>/js/validar.js"></script>
<body onLoad="redimensionar();" onResize="redimensionar();">
<div id="capaSuperior" style="position:absolute; width:100%; height:100px; z-index:0; left: 0px; top: 0px;">
<jsp:include page="/WEB-INF/toptsp.jsp?encabezado=Ingresar Descuento Clase Por Equipo"/>
</div>

<div id="capaCentral" style="position:absolute; width:100%; height:85%; z-index:0; left: 0px; top: 100px; overflow: scroll;"> 
<FORM name='forma' id='forma' method='POST' action='<%=CONTROLLER%>?estado=Descuento_clase&accion=Insert'>
  <table width="600" border="2" align="center" >
    <tr>
      <td>
        <table width="100%" class="tablaInferior">
          <tr class="fila">
            <td colspan="2" align="left" class="subtitulo1">&nbsp;Descuento por Clase Equipo </td>
            <td colspan="2" align="left" class="barratitulo"><img src="<%=BASEURL%>/images/titulo.gif" width="32" height="20" align="left"><%=datos[0]%>
            </td>
            </tr>
          <tr class="fila">
            <td width="25%" align="left" >Clase Equipo </td>
            <td width="25%" valign="middle">
			<% TreeMap clases = model.placaService.obtenerTreeMapLista( model.tablaGenService.obtenerTablas()); %>
        	<input:select name="c_clase" options="<%=clases%>" attributesText="style='width:80%;' class='listmenu'"/>
            <img src="<%=BASEURL%>/images/botones/iconos/obligatorio.gif"></td>
            <td width="16%" valign="middle">Concepto</td>
            <td width="34%" valign="middle"><select name="c_codigo" id="select" class="textbox">
              <% model.concepto_equipoService.listConcepto_equipo(); 
                Vector vecd = model.concepto_equipoService.getConcepto_equipos();
                for(int i = 0; i<vecd.size(); i++){	
                        Concepto_equipo c = (Concepto_equipo) vecd.elementAt(i);%>
                        <option value="<%=c.getCodigo()%>"><%=c.getDescripcion()%></option>
                <%}%>
            </select> <img src="<%=BASEURL%>/images/botones/iconos/obligatorio.gif"></td>
          </tr>
          <tr class="fila">
            <td valign="middle">Porcentaje Descuento </td>
            <td valign="middle"><input name="c_porcentaje" type="text" class="textbox" id="c_porcentaje" size="7" maxlength="7"  onblur  =" if( this.value !='' ){ this.value=formato(this.value, 2);}"  onKeyPress="soloDigitos(event,'decOK');" onfocus='this.value=sinformato(this.value);'> 
            <img src="<%=BASEURL%>/images/botones/iconos/obligatorio.gif"></td>
            <td valign="middle">Frecuencia</td>
            <td valign="middle"><select name="c_frecuencia" id="c_frecuencia" class="textbox">
              <option value="VIAJE">Viaje</option>
              <option value="MES">Mes</option>
            </select> <img src="<%=BASEURL%>/images/botones/iconos/obligatorio.gif"></td>
          </tr>
          <tr class="fila">
            <td align="left" >Valor Mes </td>
            <td valign="middle"><input name="c_valor" type="text" class="textbox" id="c_valor" size="15" maxlength="15" onblur  =" if( this.value !='' ){ this.value=formato(this.value, 2);}"  onKeyPress="soloDigitos(event,'decOK');" onfocus='this.value=sinformato(this.value);'> 
            <img src="<%=BASEURL%>/images/botones/iconos/obligatorio.gif"></td>
            <td valign="middle">Mes Proceso </td>
            <td valign="middle"><select name="c_ano" id="c_ano" class="textbox"> 
              <%for(int i = 1970;i<2020;i++){%>
              <option value="<%=i%>" <%if( i == Integer.parseInt( fechareporte.substring (0,4) ) ){%> selected <%}%> ><%=i%></option>
              <%}%>
            </select>
              <select name="c_mes" id="c_mes" class="textbox">
                <option value="01" <%=fechareporte.substring (5,7).equals("01")?"selected":""%> >Enero</option>
                <option value="02" <%=fechareporte.substring (5,7).equals("02")?"selected":""%>>Febrero</option>
                <option value="03" <%=fechareporte.substring (5,7).equals("03")?"selected":""%>>Marzo</option>
                <option value="04" <%=fechareporte.substring (5,7).equals("04")?"selected":""%>>Abril</option>
                <option value="05" <%=fechareporte.substring (5,7).equals("05")?"selected":""%>>Mayo</option>
                <option value="06" <%=fechareporte.substring (5,7).equals("06")?"selected":""%>>Junio</option>
                <option value="07" <%=fechareporte.substring (5,7).equals("07")?"selected":""%>>Julio</option>
                <option value="08" <%=fechareporte.substring (5,7).equals("08")?"selected":""%>>Agosto</option>
                <option value="09" <%=fechareporte.substring (5,7).equals("09")?"selected":""%>>Septiembre</option>
                <option value="10" <%=fechareporte.substring (5,7).equals("10")?"selected":""%>>Octubre</option>
                <option value="11" <%=fechareporte.substring (5,7).equals("11")?"selected":""%>>Noviembre</option>
                <option value="12" <%=fechareporte.substring (5,7).equals("12")?"selected":""%>>Diciembre</option>
              </select> <img src="<%=BASEURL%>/images/botones/iconos/obligatorio.gif"></td>
          </tr>	  
      </table></td>
    </tr>
  </table>
  <p>
<div align="center"><img src="<%=BASEURL%>/images/botones/aceptar.gif" style="cursor:hand" title="Agregar un Descuento por clase equipos" name="ingresar"  onclick=" if( validarCamposDescuentos() ){ forma.submit(); }" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);">&nbsp;
<img src="<%=BASEURL%>/images/botones/cancelar.gif" style="cursor:hand" name="regresar" title="Limpiar todos los registros" onMouseOver="botonOver(this);" onClick="forma.reset();" onMouseOut="botonOut(this);" >&nbsp;
<img src="<%=BASEURL%>/images/botones/salir.gif" style="cursor:hand" name="salir" title="Salir al Menu Principal" onClick="parent.close();" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);"></div>
</p>  

<%  if(request.getAttribute("mensaje")!=null){%>
        <br>
        <table border="2" align="center">
          <tr>
            <td><table width="100%">
                <tr>
                  <td width="229" align="center" class="mensajes"><%=request.getAttribute("mensaje")%></td>
                  <td width="29" background="<%=BASEURL%>/images/cuadronaranja.JPG">&nbsp;</td>
                  <td width="58">&nbsp;</td>
                </tr>
            </table></td>
          </tr>
        </table>
    <% }%>	
</FORM>
</div>
<%=datos[1]%>
</body>
</html>
<script>
function validarCamposDescuentos(){
	if(forma.c_porcentaje.value==""){
		alert("Debe llenar el porcentaje de descuento");
		forma.c_porcentaje.focus();
		return false;
	}
	if(forma.c_valor.value==""){
		alert("Debe llenar el valor del mes");
		forma.c_valor.focus();
		return false;
	}	
	else{		
		if( validarDecimales( forma.c_valor, 12 ) ){
			if( validarDecimales( forma.c_porcentaje, 3 ) )
				return true;
			else
				return false;
		}
		else
			return false;
						
	}
}
function validarDecimales( valor , entero ){
	if( valor.value.indexOf('.') != -1 ){
		var porcentaje = sinformato(valor.value).split(".");
		if( porcentaje[0].length > entero ){
			alert("El numero no puedes ser mayor a "+entero+" decimales");
			valor.focus();
			return false;
		}
		else{
			if(porcentaje[0].length == 0 ){
				valor.value = "0."+ porcentaje[1];
			}
			return true;
		}
	}
	else{	
		if(valor.value.length > entero ){
			alert("El numero no puedes ser mayor a "+entero+" decimales");
			valor.focus();
			return false;
		}
		else{
			return true;
		}
	}
}	
function formato (num, decimales){
	var nums = ( new String (num) ).split('.');
	var salida = new String();
	for (var i=nums[0].length-1, j=0; i>=0; salida = nums[0].charAt(i) + (j%3==0 && j!=0? ',':'') + salida , i--, j++);
	return rellenar(salida + (nums.length > 1 && decimales > 0 ? '.' + (nums[1].substr(0, (nums[1].length>decimales?decimales:nums[1].length))) : ''));
}

function rellenar(val){
   var longitud = 2;
   var cantDEcimal = val.indexOf('.');
   if( cantDEcimal >-1  ){
	   var decimal = val.substr(cantDEcimal+1, val.length );
	   var cntDec  =   decimal.length;
	   if ( cntDec < longitud){
			for( var i= 1;i< longitud;i++)
				val +='0'
	   }
   }else{
	  val +='.00';
   }

   return val;
}

function sinformato(element){
   return element.replace( new RegExp(",","g") ,'');
}
</script>
