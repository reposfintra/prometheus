<!--  
	 - Author(s)       :      Jose de la Rosa
	 - Description     :      AYUDA FUNCIONAL - Ingreso descuento por clase de equipo 
	 - Date            :      23/05/2006 
	 - Copyright Notice:      TRANSPORTES SANCHEZ POLO S.A.
-->
<%@ include file="/WEB-INF/InitModel.jsp"%>
<HTML>
<HEAD>
<TITLE>AYUDA FUNCIONAL - Ingreso Descuento por Clase de Equipo</TITLE>
<META http-equiv=Content-Type content="text/html; charset=windows-1252">
<link href="../css/estilostsp.css" rel="stylesheet" type="text/css"> 
<link href="<%=BASEURL%>/css/estilostsp.css" rel="stylesheet" type="text/css">
<script type='text/javascript' src="<%= BASEURL %>/js/boton.js"></script> 
</HEAD>
<BODY> 
<% String BASEIMG = BASEURL +"/images/ayuda/equipos/ingreso_especial/"; %>
<% String BASEIMG1 = BASEURL +"/images/ayuda/equipos/descuento_clase/"; %>
  <table width="95%"  border="2" align="center">
    <tr>
      <td height="117" >
        <table width="100%" border="0" align="center">
          <tr  class="subtitulo">
            <td height="20"><div align="center">MANUAL DEL DESCUENTO POE CLASE DE EQUIPO</div></td>
          </tr>
          <tr class="subtitulo1">
            <td>Descripci&oacute;n del funcionamiento del programa para agregar descuento por clase de equipo.</td>
          </tr>
          <tr>
            <td  class="ayudaHtmlTexto"><div align="center">
              <p>&nbsp;</p>
              </div></td>
          </tr>
            <tr>
            <td  class="ayudaHtmlTexto"><p class="ayudaHtmlTexto">En la siguiente pantalla se ingresan los datos de los descuentos por clase de equipo.</p>
            </td>
          </tr>
            <tr>
            <td  class="ayudaHtmlTexto"><div align="center"><IMG src="<%=BASEIMG1%>IngresarDescuentoClase.JPG" border=0 ></div></td>
          </tr>
          
            <tr>
            <td  class="ayudaHtmlTexto"><p class="ayudaHtmlTexto">&nbsp;</p>
            <p class="ayudaHtmlTexto">El sistema verifica si alguno de los datos del formulario no esta lleno, si no lo estan le saldr&aacute; en la pantalla el siguiente mensaje. </p>
            </td>
            </tr>
            <tr>
            <td  class="ayudaHtmlTexto"><div align="center"><IMG  src="<%=BASEIMG%>MensajeErrorCamposLLenos.JPG" border=0 ></div></td>
            </tr>
		
            <tr>
              <td  class="ayudaHtmlTexto"><p>&nbsp;</p>
                <p>Si los datos digitados ya esta ingresado en nuestra base de datos, entonces el sistema le mostrar&aacute; el siguiente mensaje. </p></td>
            </tr>
            <tr>
              <td  class="ayudaHtmlTexto"><div align="center"><IMG src="<%=BASEIMG%>MensajeNoExiste.JPG" border=0 ></div></td>
            </tr>
            
            <tr>
              <td  class="ayudaHtmlTexto"><p>&nbsp;</p>
                <p>Si el estandar digitado no se encuentra ingresado en la base de datos, entonces el sistema le mostrar&aacute; el siguiente mensaje. </p></td>
            </tr>
            <tr>
              <td  class="ayudaHtmlTexto"><div align="center"><IMG src="<%=BASEIMG%>MensajeExitoso.JPG" border=0 ></div></td>
            </tr>            
        </td>
      </table></td>
    </tr>
  </table>
  <p></p>
<center>
<img src = "<%=BASEURL%>/images/botones/salir.gif" style = "cursor:hand" name = "imgsalir" onClick = "parent.close();" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);">
</center>
</BODY>
</HTML>
