<!--
- Nombre P�gina :                  BuscarDescuentosEquipos.jsp                  
- Descripci�n :                    Pagina JSP, que busca por c�digo un descuento de equipo                     
- Autor :                          Ing. Armando Oviedo C                         
- Fecha Creado :                   8 de Diciembre de 2005                  
- Modificado por :                 LREALES                                   
- Fecha Modificado :               23 de mayo de 2006, 07:12 AM              
- Versi�n :                        1.0                                       
- Copyright :                      Fintravalores S.A.                   
-->

<%-- Declaracion de librerias--%>
<%@page contentType="text/html"%>
<%@page session="true"%>
<%@page errorPage="../error/ErrorPage.jsp"%>
<%@page import="java.util.*"%>
<%@page import="com.tsp.operation.model.*"%>
<%@page import="com.tsp.util.*"%>
<%@page import="com.tsp.operation.model.beans.*, java.util.*"%>
<%@include file="/WEB-INF/InitModel.jsp"%>
<%@ taglib uri="http://jsptags.com/tags/navigation/pager" prefix="pg" %>
<%
  String path = application.getRealPath(model.menuService.getRealUrl(request.getRequestURI()));
  String datos[] = model.menuService.getContenidoMenu(BASEURL,request.getRequestURI(),path);
%>
<html>
<head>
<title>Descuentos De Equipos</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link href="<%= BASEURL %>/css/estilostsp.css" rel="stylesheet" type="text/css">
<script type='text/javascript' src="<%= BASEURL %>/js/boton.js"></script>
<link href="../../css/estilostsp.css" rel="stylesheet" type="text/css">
</head>
<script>
		var controlador ="<%=CONTROLLER%>";
		
		function Buscar ( codigo ){
		
			if( codigo != "" ){
			
				document.forma.action = controlador+"?estado=Descuento&accion=EquipoBuscar&mensaje=listar";
				document.forma.submit();
				
			} else{
			
				alert( "Por Favor Digite El Codigo Del Descuento!" );
				
			}
		}
</script>
<body onresize="redimensionar()" onload = 'redimensionar()'>
<div id="capaSuperior" style="position:absolute; width:100%; height:100px; z-index:0; left: 0px; top: 0px;">
	<jsp:include page="/WEB-INF/toptsp.jsp?encabezado=Buscar Descuentos De Equipos"/>
</div>
<div id="capaCentral" style="position:absolute; width:100%; height:87%; z-index:0; left: 0px; top: 100px; overflow: scroll;">
<form action='<%= BASEURL %>/jsp/equipos/descuentos_equipo/BuscarDescuentosEquipos.jsp?codigo=""' name="forma" method="post">
  <table width="30%"  border="2" align="center">
  <tr>
  <td colspan="2">
  	<table width="100%">
    <tr>
		<td width="50%" class="subtitulo1" nowrap>Buscar Descuentos</td>
		<td width="50%" class="barratitulo"><img src="<%=BASEURL%>/images/titulo.gif" width="32" height="20" align="left"><%=datos[0]%></td>
	</tr>
    <tr class="fila">
      <td>C�digo</td>
      <td><input name="codigo" type="text" class="textbox" size="12" maxlength="12"></td>
    </tr>
  </table>
  </td>
  </tr>
  </table>
  <br>
  <table align=center width='450'>
    <tr>
        <td align="center" colspan="2">
            <img title='Buscar' style = "cursor:hand" src="<%= BASEURL %>/images/botones/buscar.gif" onmouseover="botonOver(this);" onmouseout="botonOut(this);" onclick="Buscar ( codigo.value );"></img>                        
			<img title='Detalles' style = "cursor:hand" src="<%= BASEURL %>/images/botones/detalles.gif" onmouseover="botonOver(this);" onmouseout="botonOut(this);" onClick="document.location = '<%=CONTROLLER%>?estado=Descuento&accion=EquipoBuscar&mensaje=listartodos'"></img>
            <img title='Salir' style = "cursor:hand" src="<%= BASEURL %>/images/botones/salir.gif" onmouseover="botonOver(this);" onmouseout="botonOut(this);" onClick="window.close()"></img>
        </td>
    </tr>
  </table>
</form>
</div>
<%=datos[1]%>
</body>
</html>