<!--
- Autor : Ing. Jose de la rosa
- Date  : 2 de Diciembre de 2005
- Copyrigth Notice : Fintravalores S.A. S.A
-->
<%--
-@(#)
--Descripcion : Pagina JSP, que maneja el listado de las flotas directas
--%>

<%-- Declaracion de librerias--%>
<%@page contentType="text/html"%>
<%@page session="true"%>
<%@page errorPage="/error/error.jsp"%>
<%@page import="java.util.*"%>
<%@page import="com.tsp.operation.model.*"%>
<%@page import="com.tsp.operation.model.beans.*"%>
<%@page import="com.tsp.util.*"%>
<%@include file="/WEB-INF/InitModel.jsp"%>
<%@taglib uri="http://jsptags.com/tags/navigation/pager" prefix="pg" %>
<html>
<head><title>Listar Descuento por clase equipos</title>
<link href="<%= BASEURL %>/css/estilostsp.css" rel="stylesheet" type="text/css">
<script type='text/javascript' src="<%= BASEURL %>/js/boton.js"></script>
<script type='text/javascript' src="<%= BASEURL %>/js/reporte.js"></script>
</head>
<body onLoad="redimensionar();" onResize="redimensionar();">
<div id="capaSuperior" style="position:absolute; width:100%; height:100px; z-index:0; left: 0px; top: 0px;">
<jsp:include page="/WEB-INF/toptsp.jsp?encabezado=Listar Descuento Clase Por Equipo"/>
</div>

<div id="capaCentral" style="position:absolute; width:100%; height:85%; z-index:0; left: 0px; top: 100px; overflow: scroll;"> 
<%  Vector vec = model.descuento_claseService.getDescuento_clases();
    String style = "simple";
    String position =  "bottom";
    String index =  "center";
    String mes = "";
    String ano = "";
    int maxPageItems = 10;
    int maxIndexPages = 10;
if ( vec.size() >0 ){ 
%>
<table width="750" border="2" align="center">
    <tr>
      <td>
	  	  <table width="100%" align="center">
              <tr>
                <td width="50%" class="subtitulo1">&nbsp;Descuento Por Clase Equipo </td>
                <td width="50%" class="barratitulo"><img src="<%=BASEURL%>/images/titulo.gif" width="32" height="20"></td>
              </tr>
        </table>
	<table width="100%" border="1" bordercolor="#999999" bgcolor="#F7F5F4" align="center">
    <tr class="tblTitulo" align="center">
        <td width="14%" nowrap >Clase Equipo</td>
        <td width="19%" nowrap >Concepto</td>
		<td width="20%" nowrap >Porcentaje Descuento</td>
        <td width="12%" nowrap >Frecuencia</td>
        <td width="12%" nowrap >Valor Mes</td>
        <td width="23%" nowrap >Mes Proceso</td>
    </tr>
    <pg:pager
        items="<%=vec.size()%>"
        index="<%= index %>"
        maxPageItems="<%= maxPageItems %>"
        maxIndexPages="<%= maxIndexPages %>"
        isOffset="<%= true %>"
        export="offset,currentPageNumber=pageNumber"
        scope="request">
<%
    for (int i = offset.intValue(), l = Math.min(i + maxPageItems, vec.size()); i < l; i++){
        Descuento_clase d = (Descuento_clase) vec.elementAt(i);
		ano = d.getMes_proceso().substring (0,4);
		mes = d.getMes_proceso().substring (5,7);	
		%>
        <pg:item>
            <tr class="<%=(i % 2 == 0 )?"filagris":"filaazul"%>" onMouseOver='cambiarColorMouse(this)'   style="cursor:hand"onClick="window.open('<%=CONTROLLER%>?estado=Descuento_clase&accion=Search&c_clase=<%=d.getClase_equipo()%>&c_codigo=<%=d.getCodigo_concepto()%>&listar=False','myWindow','status=no,scrollbars=no,width=750,height=350,resizable=yes');" >
                <td align="center" class="bordereporte"><%TablaGen tg = model.tablaGenService.obtenerInformacionDato("CLAEQUI",d.getClase_equipo());%>
				<%=tg!=null?tg.getDescripcion():""%></td>
                <td align="center" class="bordereporte"><% model.concepto_equipoService.searchConcepto_equipo (d.getCodigo_concepto());
                                                            Concepto_equipo conc = model.concepto_equipoService.getConcepto_equipo ();%>
                                                        <%=conc.getDescripcion ()%></td>
				<td align="center" class="bordereporte"><%=Util.customFormat(d.getPorcentaje_descuento())%></td>
				<td align="center" class="bordereporte"><%=d.getFrecuencia()%></td>
                <td align="center" class="bordereporte"><%=Util.customFormat(d.getValor_mes())%></td>				
				<td align="center" class="bordereporte"><%if(mes.equals("01")){%>Enero<%}%>
                                                                        <%if(mes.equals("02")){%>Febrero<%}%>
                                                                        <%if(mes.equals("03")){%>Marzo<%}%>
                                                                        <%if(mes.equals("04")){%>Abril<%}%>
                                                                        <%if(mes.equals("05")){%>Mayo<%}%>
                                                                        <%if(mes.equals("06")){%>Junio<%}%>
                                                                        <%if(mes.equals("07")){%>Julio<%}%>
                                                                        <%if(mes.equals("08")){%>Agosto<%}%>
                                                                        <%if(mes.equals("09")){%>Septiembre<%}%>
                                                                        <%if(mes.equals("10")){%>Octubre<%}%>
                                                                        <%if(mes.equals("11")){%>Noviembre<%}%>
                                                                        <%if(mes.equals("12")){%>Diciembre<%}%> del <%=ano%></td>
            </tr>
      </pg:item>
<%  }%>
    <tr class="pie">
        <td td height="20" colspan="12" nowrap align="center">          <pg:index>
                <jsp:include page="/WEB-INF/jsp/googlesinimg.jsp" flush="true"/>
        </pg:index>
        </td>
    </tr>
    </pg:pager>        
</table>
</td>
</tr>
</table>
<br>
<%}else { %>
  <table border="2" align="center">
    <tr>
      <td><table width="410" height="73" border="1" align="center"  bordercolor="#F7F5F4" bgcolor="#FFFFFF">
          <tr>
            <td width="282" align="center" class="mensajes">Su b&uacute;squeda no arroj&oacute; resultados!</td>
            <td width="28" background="<%=BASEURL%>/images/cuadronaranja.JPG">&nbsp;</td>
            <td width="78">&nbsp;</td>
          </tr>
      </table></td>
    </tr>
  </table>
  <%}%>
 <table width="750" border="0" align="center">
    <tr>
      <td>
	  <img src="<%=BASEURL%>/images/botones/regresar.gif" style="cursor:hand" title="Volver" name="buscar"  onClick="window.location='<%=BASEURL%>/jsp/equipos/descuento_clase/descuento_claseBuscar.jsp'" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);">
	  </td>
	</tr>
</table>
</div>
</body>
</html>
