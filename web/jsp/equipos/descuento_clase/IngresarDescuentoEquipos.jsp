<!--
- Nombre P�gina :                  IngresarDescuentoEquipos.jsp                  
- Descripci�n :                    Pagina JSP, que ingresa un descuento de equipo                      
- Autor :                          Ing. Armando Oviedo C                         
- Fecha Creado :                   8 de Diciembre de 2005                  
- Modificado por :                 LREALES                                   
- Fecha Modificado :               23 de mayo de 2006, 07:12 AM              
- Versi�n :                        1.0                                       
- Copyright :                      Fintravalores S.A.                   
-->

<%-- Declaracion de librerias--%>
<%@page contentType="text/html"%>
<%@page session="true"%>
<%@page errorPage="../error/ErrorPage.jsp"%>
<%@page import="com.tsp.operation.model.*"%>
<%@page import="com.tsp.util.*"%>
<%@page import="com.tsp.operation.model.beans.*, java.util.*"%>
<%@include file="/WEB-INF/InitModel.jsp"%>
<%@ taglib uri="http://jakarta.apache.org/taglibs/input-0.90" prefix="input" %>
<%
  String path = application.getRealPath(model.menuService.getRealUrl(request.getRequestURI()));
  String datos[] = model.menuService.getContenidoMenu(BASEURL,request.getRequestURI(),path);
%>
<html>
<head>
<title>Descuentos De Equipos</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link href="<%= BASEURL %>/css/estilostsp.css" rel="stylesheet" type="text/css">
<link href="../../css/estilostsp.css" rel="stylesheet" type="text/css"> 
<script type='text/javascript' src="<%= BASEURL %>/js/boton.js"></script>
</head>
<script>
		var controlador ="<%=CONTROLLER%>";
		
		function Agregar ( codigo, descripcion, conc_contable, conc_especial ){
		
			if( ( codigo != "" ) && ( descripcion != "" ) && ( conc_contable != "" ) && ( conc_especial != "" )  ){
			
				document.forma.action = controlador+"?estado=Descuento&accion=EquipoInsert&codigo="+codigo+"&descripcion="+descripcion+"&conc_contable="+conc_contable+"&conc_especial="+conc_especial;
				document.forma.submit();
				
			} else{
			
				alert( "Por Favor No Deje Ningun Campo Vacio!" );
				
			}
		}
</script>

<body ONLOAD="redimensionar();" onresize="redimensionar()">
<div id="capaSuperior" style="position:absolute; width:100%; height:100px; z-index:0; left: 0px; top: 0px;">
	<jsp:include page="/WEB-INF/toptsp.jsp?encabezado=Ingresar Descuento De Equipo"/>
</div>
<div id="capaCentral" style="position:absolute; width:100%; height:87%; z-index:0; left: 0px; top: 100px; overflow: scroll;">
<form name="forma" action='<%= BASEURL %>/jsp/equipos/descuentos_equipo/IngresarDescuentoEquipos.jsp?codigo=""&descripcion=""&conc_contable=""&conc_especial=""' method="post">
  <table width="50%"  border="2" align="center">
  	<tr>
    	<td>
			<table width="100%" align="center">
				<tr>
					<td width="50%" class="subtitulo1" nowrap>Informaci�n Del Descuento</td>
					<td width="50%" class="barratitulo"><img src="<%=BASEURL%>/images/titulo.gif" width="32" height="20" align="left"><%=datos[0]%></td>
				</tr>
				<tr class="fila">
				  <td>C�digo</td>
				  <td><input name="codigo" type="text" size="12" maxlength="12"></td>
			  </tr>
				<tr class="fila">
				  <td>Descripci�n</td>
				  <td><textarea class="textbox" name="descripcion" cols="50" rows="3"></textarea></td>
			  </tr>
				<tr class="fila">
				  <td>Contepto Contable</td>
				  <td><input name="conc_contable" type="text" size="15" maxlength="15"></td>
			  </tr>
				<tr class="fila">
				  <td>Concepto Especial</td>
				  <td><input name="conc_especial" type="text" size="15" maxlength="15"></td>
			  </tr>
		  </table>
  		</td>
  	</tr>
  </table><br>
  <table align=center width='450'>
    <tr>
        <td align="center" colspan="2">
            <img title='Agregar reporte' style = "cursor:hand" src="<%= BASEURL %>/images/botones/aceptar.gif" onmouseover="botonOver(this);" onmouseout="botonOut(this);" onclick="Agregar( codigo.value, descripcion.value, conc_contable.value, conc_especial.value );"></img>                        
			<img title='Reset' style = "cursor:hand" src="<%= BASEURL %>/images/botones/cancelar.gif" onmouseover="botonOver(this);" onmouseout="botonOut(this);" onClick="forma.reset(); forma.submit();"></img>
            <img title='Salir' style = "cursor:hand" src="<%= BASEURL %>/images/botones/salir.gif" onmouseover="botonOver(this);" onmouseout="botonOut(this);" onClick="window.close()"></img>
        </td>
    </tr>
  </table>  
  </form>
  <br>
  <%
  if( ( request.getParameter( "mensaje" ) != null ) && ( request.getParameter( "mensaje" ) != "" ) ) {
  
            out.print("<table width=450 border=2 align=center><tr><td><table width=100%  border=1 align=center  bordercolor=#F7F5F4 bgcolor=#FFFFFF><tr><td width=350 align=center class=mensajes>"+request.getParameter("mensaje")+"</td><td width=100><img src="+BASEURL+"/images/cuadronaranja.JPG></td></tr></table></td></tr></table>");            
  
  }
  %>
</div>
<%=datos[1]%>
</body>
</html>