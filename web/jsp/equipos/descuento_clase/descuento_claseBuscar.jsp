<!--
- Autor : Ing. Jose de la rosa
- Date  : 2 de Diciembre de 2005
- Copyrigth Notice : Fintravalores S.A. S.A
-->
<%--
-@(#)
--Descripcion : Pagina JSP, que maneja el ingreso de las flotas directas
--%>

<%-- Declaracion de librerias--%>
<%@page contentType="text/html"%>
<%@ page session="true"%>
<%@ page errorPage="../error/error.jsp"%>
<%@page import="com.tsp.operation.model.beans.*, java.util.*"%>
<%@ include file="/WEB-INF/InitModel.jsp"%>
<%@page import="com.tsp.operation.model.*"%>
<%@page import="com.tsp.util.*"%>
<%@ taglib uri="http://jakarta.apache.org/taglibs/input-0.90" prefix="input" %>
<%  model.tablaGenService.buscarRegistros("CLAEQUI");
	String path = application.getRealPath(model.menuService.getRealUrl(request.getRequestURI()));
	String datos[] = model.menuService.getContenidoMenu(BASEURL,request.getRequestURI(),path);
%>
<html>
<head><title>Buscar Descuento por clase equipos</title>
<link href="<%= BASEURL %>/css/estilostsp.css" rel="stylesheet" type="text/css">
<script type='text/javascript' src="<%= BASEURL %>/js/boton.js"></script>
</head>
<body onLoad="redimensionar();" onResize="redimensionar();">
<div id="capaSuperior" style="position:absolute; width:100%; height:100px; z-index:0; left: 0px; top: 0px;">
<jsp:include page="/WEB-INF/toptsp.jsp?encabezado=Buscar Descuento Clase Por Equipo"/>
</div>

<div id="capaCentral" style="position:absolute; width:100%; height:85%; z-index:0; left: 0px; top: 100px; overflow: scroll;"> 
<form name="form2" method="post" action="<%=CONTROLLER%>?estado=Descuento_clase&accion=Search&listar=True&sw=True">
    <table width="380" border="2" align="center">
      <tr>
        <td><table width="100%" align="center"  class="tablaInferior">
			<tr>
				<td width="173" class="subtitulo1">&nbsp;Descuento Clase </td>
				<td width="205" class="barratitulo"><img src="<%=BASEURL%>/images/titulo.gif" width="32" height="20" align="left"><%=datos[0]%></td>
			</tr>
			<tr class="fila">
              <td align="left" >Clase Equipo</td>
              <td valign="middle"> <% 	TreeMap clases = model.placaService.obtenerTreeMapLista( model.tablaGenService.obtenerTablas() ); 
			  								clases.put("", ""); %>
        	<input:select name="c_clase" options="<%=clases%>" attributesText="style='width:80%;' class='listmenu'"/></td>
		    </tr>
			<tr class="fila">
              <td align="left" >Concepto</td>
              <td valign="middle"><select name="c_codigo" id="select" class="textbox">
                <option value=""></option>
                <% model.concepto_equipoService.listConcepto_equipo(); 
                Vector vecd = model.concepto_equipoService.getConcepto_equipos();
                for(int i = 0; i<vecd.size(); i++){	
                        Concepto_equipo c = (Concepto_equipo) vecd.elementAt(i);%>
                        <option value="<%=c.getCodigo()%>"><%=c.getDescripcion()%></option>
                <%}%>
              </select></td>
		    </tr>
			<tr class="fila">
			  <td align="left" >Frecuencia</td>
			  <td valign="middle"><select name="c_frecuencia" id="select2" class="textbox">
			    <option value=""></option>
			    <option value="VIAJE">Viaje</option>
			    <option value="MES">Mes</option>
                            </select></td>
		    </tr>
			<tr class="fila">
			  <td align="left" >Mes Proceso </td>
			  <td valign="middle">
			  <select name="c_ano" id="c_ano" class="textbox">
			    <option value=""></option>
			  	<%for(int i = 1970;i<2020;i++){%>
                	<option value="<%=i%>"><%=i%></option>
				<%}%>
              </select>
			  <select name="c_mes" id="c_mes" class="textbox">
			    <option value=""></option>
				<option value="01">Enero</option>
                <option value="02">Febrero</option>
				<option value="03">Marzo</option>
                <option value="04">Abril</option>
				<option value="05">Mayo</option>
                <option value="06">Junio</option>
				<option value="07">Julio</option>
                <option value="08">Agosto</option>
				<option value="09">Septiembre</option>
                <option value="10">Octubre</option>
				<option value="11">Noviembre</option>
                <option value="12">Diciembre</option>
              </select></td>
		    </tr>			  
        </table>
		</td>
      </tr>
    </table>
<p>
<div align="center"><img src="<%=BASEURL%>/images/botones/buscar.gif" style="cursor:hand" title="Buscar Descuento por clase equipos" name="buscar"  onClick="form2.submit();" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);">&nbsp;
<img src="<%=BASEURL%>/images/botones/detalles.gif" style="cursor:hand" title="Buscar todas los Descuentos por clase equipos" name="buscar"  onClick="window.location='<%=CONTROLLER%>?estado=Descuento_clase&accion=Search&listar=True&sw=False'" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);">&nbsp;
<img src="<%=BASEURL%>/images/botones/salir.gif"  name="regresar" style="cursor:hand" title="Salir al Menu Principal" onMouseOver="botonOver(this);" onClick="parent.close();" onMouseOut="botonOut(this);" ></div>
</p>		
</form>
</div>
<%=datos[1]%>
</body>
</html>
