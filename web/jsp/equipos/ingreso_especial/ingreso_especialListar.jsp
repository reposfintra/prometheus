<!--
- Autor : Ing. Jose de la rosa
- Date  : 2 de Diciembre de 2005
- Copyrigth Notice : Fintravalores S.A. S.A
-->
<%--
-@(#)
--Descripcion : Pagina JSP, que maneja el listado de las flotas directas
--%>

<%-- Declaracion de librerias--%>
<%@page contentType="text/html"%>
<%@page session="true"%>
<%@page errorPage="/error/error.jsp"%>
<%@page import="java.util.*"%>
<%@page import="com.tsp.operation.model.*"%>
<%@page import="com.tsp.operation.model.beans.*"%>
<%@page import="com.tsp.util.*"%>
<%@include file="/WEB-INF/InitModel.jsp"%>
<%@taglib uri="http://jsptags.com/tags/navigation/pager" prefix="pg" %>
<html>
<head><title>Listar Ingreso Especial</title>
<link href="<%= BASEURL %>/css/estilostsp.css" rel="stylesheet" type="text/css">
<script type='text/javascript' src="<%= BASEURL %>/js/boton.js"></script>
<script type='text/javascript' src="<%= BASEURL %>/js/reporte.js"></script>
</head>
<body onLoad="redimensionar();" onResize="redimensionar();">
<div id="capaSuperior" style="position:absolute; width:100%; height:100px; z-index:0; left: 0px; top: 0px;">
<jsp:include page="/WEB-INF/toptsp.jsp?encabezado=Listar Acuerdo Especial"/>
</div>

<div id="capaCentral" style="position:absolute; width:100%; height:85%; z-index:0; left: 0px; top: 100px; overflow: scroll;"> 
<%  Vector vec = model.ingreso_especialService.getIngreso_especiales();
    String style = "simple";
    String position =  "bottom";
    String index =  "center";
    int maxPageItems = 10;
    int maxIndexPages = 10;
if ( vec.size() >0 ){ 
%>
<table width="550" border="2" align="center">
    <tr>
      <td>
	  	  <table width="100%" align="center">
              <tr>
                <td width="50%" class="subtitulo1">&nbsp;Ingreso Especial </td>
                <td width="50%" class="barratitulo"><img src="<%=BASEURL%>/images/titulo.gif" width="32" height="20"></td>
              </tr>
        </table>
	<table width="100%" border="1" bordercolor="#999999" bgcolor="#F7F5F4" align="center">
    <tr class="tblTitulo" align="center">
        <td width="25%" nowrap >Tipo Acuerdo</td>
        <td width="25%" nowrap >Concepto</td>
		<td width="25%" nowrap >Clase Equipo</td>
		<td width="25%" nowrap >Porcentaje Descuento</td>
    </tr>
    <pg:pager
        items="<%=vec.size()%>"
        index="<%= index %>"
        maxPageItems="<%= maxPageItems %>"
        maxIndexPages="<%= maxIndexPages %>"
        isOffset="<%= true %>"
        export="offset,currentPageNumber=pageNumber"
        scope="request">
<%
    for (int i = offset.intValue(), l = Math.min(i + maxPageItems, vec.size()); i < l; i++){
        Ingreso_especial a = (Ingreso_especial) vec.elementAt(i);
		model.concepto_equipoService.searchConcepto_equipo (a.getCodigo_concepto());
		Concepto_equipo conc = model.concepto_equipoService.getConcepto_equipo();
		TablaGen tg = model.tablaGenService.obtenerInformacionDato("CLAEQUI",a.getClase_equipo()); %>
        <pg:item>
            <tr class="<%=(i % 2 == 0 )?"filagris":"filaazul"%>" onMouseOver='cambiarColorMouse(this)'   style="cursor:hand"onClick="window.open('<%=CONTROLLER%>?estado=Ingreso_especial&accion=Search&c_tipo_acuerdo=<%=a.getTipo_acuerdo()%>&c_codigo_concepto=<%=a.getCodigo_concepto()%>&c_clase_equipo=<%=a.getClase_equipo()%>&listar=False','myWindow','status=no,scrollbars=no,width=650,height=350,resizable=yes');" >
                <td align="center" class="bordereporte"><%=a.getTipo_acuerdo()%></td>
                <td align="center" class="bordereporte"><%=conc.getDescripcion()%>
				<td align="center" class="bordereporte"><%=tg!=null?tg.getDescripcion():""%></td>
				<td align="center" class="bordereporte"><%=a.getPorcentaje_ingreso()%></td>
            </tr>
      </pg:item>
<%  }%>
    <tr class="pie">
        <td td height="20" colspan="14" nowrap align="center">          <pg:index>
                <jsp:include page="/WEB-INF/jsp/googlesinimg.jsp" flush="true"/>
        </pg:index>
        </td>
    </tr>
    </pg:pager>        
</table>
</td>
</tr>
</table>
<br>
<%}else { %>
  <table border="2" align="center">
    <tr>
      <td><table width="410" height="73" border="1" align="center"  bordercolor="#F7F5F4" bgcolor="#FFFFFF">
          <tr>
            <td width="282" align="center" class="mensajes">Su b&uacute;squeda no arroj&oacute; resultados!</td>
            <td width="28" background="<%=BASEURL%>/images/cuadronaranja.JPG">&nbsp;</td>
            <td width="78">&nbsp;</td>
          </tr>
      </table></td>
    </tr>
  </table>
  <br>
  <%}%>
 <table width="550" border="0" align="center">
    <tr>
      <td>
	  <img src="<%=BASEURL%>/images/botones/regresar.gif" style="cursor:hand" title="Volver" name="buscar"  onClick="window.location='<%=BASEURL%>/jsp/equipos/ingreso_especial/ingreso_especialBuscar.jsp'" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);">
	  </td>
	</tr>
</table>
</div>
</body>
</html>
