<!--
- Autor : Ing. Jose de la rosa
- Date  : 2 de Diciembre de 2005
- Copyrigth Notice : Fintravalores S.A. S.A
-->
<%--
-@(#)
--Descripcion : Pagina JSP, que maneja el ingreso especial
--%>

<%-- Declaracion de librerias--%>
<%@page contentType="text/html"%>
<%@ page session="true"%>
<%@page import="com.tsp.operation.model.beans.*, java.util.*"%>
<%@ include file="/WEB-INF/InitModel.jsp"%>
<%@page import="com.tsp.operation.model.*"%>
<%@page import="com.tsp.util.*"%>
<%@ taglib uri="http://jakarta.apache.org/taglibs/input-0.90" prefix="input"%>
<html>
<head>
<title>Ingreso Especial</title>
<link href="<%= BASEURL %>/css/estilostsp.css" rel="stylesheet" type="text/css">
<script type='text/javascript' src="<%= BASEURL %>/js/boton.js"></script>
</head>
<script type='text/javascript' src="<%= BASEURL %>/js/validar.js"></script>
<%
String tipo_acuerdo = request.getParameter("c_tipo_acuerdo")!=null?request.getParameter("c_tipo_acuerdo"):"";
String codigo_concepto = request.getParameter("c_codigo_concepto")!=null?request.getParameter("c_codigo_concepto"):"";
  String path = application.getRealPath(model.menuService.getRealUrl(request.getRequestURI()));
  model.tablaGenService.buscarRegistros("CLAEQUI");
  String datos[] = model.menuService.getContenidoMenu(BASEURL,request.getRequestURI(),path);
%>
<body onLoad="redimensionar();" onResize="redimensionar();">
<div id="capaSuperior" style="position:absolute; width:100%; height:100px; z-index:0; left: 0px; top: 0px;">
<jsp:include page="/WEB-INF/toptsp.jsp?encabezado=Ingreso Especial"/>
</div>

<div id="capaCentral" style="position:absolute; width:100%; height:85%; z-index:0; left: 0px; top: 100px; overflow: scroll;"> 
<FORM name='forma' id='forma' method='POST' action='<%=CONTROLLER%>?estado=Ingreso_especial&accion=Insert'>
  <table width="550" border="2" align="center" >
    <tr>
      <td>
        <table width="100%" class="tablaInferior">
          <tr class="fila">
            <td colspan="2" align="left" class="subtitulo1">&nbsp;Ingreso Especial </td>
            <td colspan="2" align="left" class="barratitulo"><img src="<%=BASEURL%>/images/titulo.gif" width="32" height="20" align="left"><%=datos[0]%></td>
          </tr>
          <tr class="fila">
            <td align="left" >Tipo Acuerdo </td>
            <td align="left" >
			<select name="c_tipo_acuerdo" id="c_tipo_acuerdo" class="textbox" style='width:90%;'>	
				<option value="CLIENTE" <%=tipo_acuerdo.equals("CLIENTE")?"selected":""%> >Cliente</option> 
				 <option value="CABEZOTE" <%=tipo_acuerdo.equals("CABEZOTE")?"selected":""%>>Cabezote</option>
			 </select><img src="<%=BASEURL%>/images/botones/iconos/obligatorio.gif">
			</td>
            <td width="24%" valign="middle">Concepto             </td>
            <td width="26%" valign="middle">
			<select name="c_codigo_concepto" id="c_codigo_concepto" class="textbox" style='width:90%;'>
			<% model.concepto_equipoService.listConcepto_equipo(); 
				Vector vecd = model.concepto_equipoService.getConcepto_equipos();
				for(int i = 0; i<vecd.size(); i++){	
					Concepto_equipo c = (Concepto_equipo) vecd.elementAt(i);%>
					<option value="<%=c.getCodigo()%>" <%=c.getCodigo().equals(codigo_concepto)?"selected":""%> ><%=c.getDescripcion()%></option>
				<%}%>
			</select><img src="<%=BASEURL%>/images/botones/iconos/obligatorio.gif">
			</td>
          </tr>
          <tr class="fila">
            <td width="21%" align="left" >Clase Equipo </td>
            <td width="29%" align="left" ><% TreeMap clases = model.placaService.obtenerTreeMapLista( model.tablaGenService.obtenerTablas()); %>
        	<input:select name="c_clase_equipo" options="<%=clases%>" attributesText="style='width:90%;' class='listmenu'"/><img src="<%=BASEURL%>/images/botones/iconos/obligatorio.gif">
			</td>
            <td valign="middle">Porcentaje Ingreso</td>
            <td valign="middle"><input name="c_porcentaje" type="text" class="textbox" id="c_porcentaje" size="6" maxlength="6"  onKeyPress="soloDigitos(event,'decOK')"><img src="<%=BASEURL%>/images/botones/iconos/obligatorio.gif"></td>
          </tr>	  
      </table></td>
    </tr>
  </table>
  <p>
<div align="center"><img src="<%=BASEURL%>/images/botones/aceptar.gif" style="cursor:hand" title="Agregar un ingreso especial" name="ingresar"  onclick="if( validarDecimales(forma.c_porcentaje,3) ){ return TCamposLlenos(); }" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);">&nbsp;
<img src="<%=BASEURL%>/images/botones/cancelar.gif" style="cursor:hand" name="regresar" title="Limpiar todos los registros" onMouseOver="botonOver(this);" onClick="forma.reset();" onMouseOut="botonOut(this);" >&nbsp;
<img src="<%=BASEURL%>/images/botones/salir.gif" style="cursor:hand" name="salir" title="Salir al Menu Principal" onClick="parent.close();" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);"></div>
</p>  
    <%  if(request.getAttribute("mensaje")!=null){
        String mensaje = (String) request.getAttribute("mensaje"); %>
            <br>
            <table border="2" align="center">
              <tr>
                <td><table width="100%">
                    <tr>
                      <td width="229" align="center" class="mensajes"><%=mensaje%></td>
                      <td width="29" background="<%=BASEURL%>/images/cuadronaranja.JPG">&nbsp;</td>
                      <td width="58">&nbsp;</td>
                    </tr>
                </table></td>
              </tr>
            </table>
    <%  }%>
</FORM>
</div>
<%=datos[1]%>
</body>
</html>
<script>
function validarDecimales( valor , entero ){
	if( valor.value.indexOf('.') != -1 ){
		var porcentaje = valor.value.split(".");
		if(porcentaje[0].length > entero ){
			alert("El numero no puedes ser mayor a "+entero+" decimales");
			valor.focus();
			return false;
		}
		else{
			if(porcentaje[0].length == 0 ){
				valor.value = "0."+ porcentaje[1];
			}
			return true;
		}
	}
	else{	
		if(valor.value.length > entero ){
			alert("El numero no puedes ser mayor a "+entero+" decimales");
			valor.focus();
			return false;
		}
		else{
			return true;
		}
	}
}	
</script>