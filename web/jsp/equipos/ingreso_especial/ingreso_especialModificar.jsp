<!--
- Autor : Ing. Jose de la rosa
- Date  : 2 de Diciembre de 2005
- Copyrigth Notice : Fintravalores S.A. S.A
-->
<%--
-@(#)
--Descripcion : Pagina JSP, que maneja la actualización de las flotas directas
--%>

<%-- Declaracion de librerias--%>
<%@page contentType="text/html"%>
<%@page session="true"%>
<%@page import="java.util.*"%>
<%@page import="com.tsp.operation.model.*"%>
<%@page import="com.tsp.operation.model.beans.*"%>
<%@page import="com.tsp.util.*"%>
<%@include file="/WEB-INF/InitModel.jsp"%>
<html>
<head>
<title>Modificar Ingreso Especial</title>
<link href="<%= BASEURL %>/css/estilostsp.css" rel="stylesheet" type="text/css">
<script type='text/javascript' src="<%= BASEURL %>/js/boton.js"></script>
</head>
<script type='text/javascript' src="<%= BASEURL %>/js/validar.js"></script>
<body onResize="redimensionar();" <%if(request.getParameter("reload")!=null){%>onLoad="parent.opener.location.reload();redimensionar();"<%}%>>
<div id="capaSuperior" style="position:absolute; width:100%; height:100px; z-index:0; left: 0px; top: 0px;">
<jsp:include page="/WEB-INF/toptsp.jsp?encabezado=Modificar Ingreso Especial"/>
</div>

<div id="capaCentral" style="position:absolute; width:100%; height:85%; z-index:0; left: 0px; top: 100px; overflow: scroll;"> 

<%
    Usuario usuario = (Usuario) session.getAttribute("Usuario");
    Ingreso_especial a = model.ingreso_especialService.getIngreso_especial();    
    String mensaje = (String) request.getAttribute("mensaje");
    Concepto_equipo conc = model.concepto_equipoService.getConcepto_equipo();
	TablaGen tg = model.tablaGenService.obtenerInformacionDato("CLAEQUI",a.getClase_equipo());
	String path = application.getRealPath(model.menuService.getRealUrl(request.getRequestURI()));
    String datos[] = model.menuService.getContenidoMenu(BASEURL,request.getRequestURI(),path);
%>
<FORM name='forma' id='forma' method='POST' action='<%=CONTROLLER%>?estado=Ingreso_especial&accion=Update'>
	<table width="500" border="2" align="center">
      <tr>
        <td>
          <table width="100%" class="tablaInferior">
            <tr class="fila">
              <td align="left" colspan="2" class="subtitulo1">&nbsp;Ingreso Especial </td>
              <td align="left" colspan="2" class="barratitulo"><img src="<%=BASEURL%>/images/titulo.gif" width="32" height="20" align="left"><%=datos[0]%></td>
            </tr>
          <tr class="fila">
            <td align="left" >Tipo Acuerdo </td>
            <td width="24%" align="left" ><%=a.getTipo_acuerdo()%><input name="c_tipo_acuerdo" type="hidden" id="c_tipo_acuerdo" value="<%=a.getTipo_acuerdo()%>"></td>
            <td width="26%" valign="middle">Concepto             </td>
            <td width="24%" valign="middle"><%=conc.getDescripcion()%><input name="c_codigo_concepto" type="hidden" id="c_codigo_concepto" value="<%=a.getCodigo_concepto()%>"></td>
          </tr>
          <tr class="fila">
            <td align="left" >Clase Equipo </td>
            <td align="left" ><%=tg!=null?tg.getDescripcion():""%><input name="c_clase_equipo" type="hidden" class="textbox" id="c_clase_equipo" size="6" maxlength="6" value="<%=a.getClase_equipo()%>"></td>
            <td align="left" >Porcentaje Ingreso</td>
            <td align="left" ><input name="c_porcentaje" type="text" class="textbox" id="c_porcentaje" size="6" maxlength="6" value="<%=a.getPorcentaje_ingreso()%>"  onKeyPress="soloDigitos(event,'decOK')"></td>
          </tr>	
        </table></td>
      </tr>
    </table>	
	<p>
	<div align="center"><img src="<%=BASEURL%>/images/botones/modificar.gif" style="cursor:hand" title="Modificar un ingreso especial" name="modificar"  onclick="if( validarDecimales(forma.c_porcentaje,3) ){ return TCamposLlenos(); }" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);">&nbsp;
	<img src="<%=BASEURL%>/images/botones/anular.gif" style="cursor:hand" title="Anular un ingreso especial" name="anular"  onClick="window.location='<%=CONTROLLER%>?estado=Ingreso_especial&accion=Anular&c_tipo_acuerdo=<%=a.getTipo_acuerdo()%>&c_codigo_concepto=<%=a.getCodigo_concepto()%>&c_clase_equipo=<%=a.getClase_equipo()%>'" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);">&nbsp;
	<img src="<%=BASEURL%>/images/botones/salir.gif" style="cursor:hand" name="salir" title="Salir al Menu Principal" onMouseOver="botonOver(this);" onClick="parent.close();" onMouseOut="botonOut(this);" ></div>
	</p>
	<%  if(mensaje!=null){%>
		<table border="2" align="center">
		  <tr>
			<td><table width="100%"  border="1" align="center"  bordercolor="#F7F5F4" bgcolor="#FFFFFF">
			  <tr>
				<td width="229" align="center" class="mensajes"><%=mensaje%></td>
				<td width="29" background="<%=BASEURL%>/images/cuadronaranja.JPG">&nbsp;</td>
				<td width="58">&nbsp;</td>
			  </tr>
			</table></td>
		  </tr>
		</table>
	<%  }%>

</FORM>
</div>
<%=datos[1]%>
</body>
</html>
<script>
function validarDecimales( valor , entero ){
	if( valor.value.indexOf('.') != -1 ){
		var porcentaje = valor.value.split(".");
		if(porcentaje[0].length > entero ){
			alert("El numero no puedes ser mayor a "+entero+" decimales");
			valor.focus();
			return false;
		}
		else{
			if(porcentaje[0].length == 0 ){
				valor.value = "0."+ porcentaje[1];
			}
			return true;
		}
	}
	else{	
		if(valor.value.length > entero ){
			alert("El numero no puedes ser mayor a "+entero+" decimales");
			valor.focus();
			return false;
		}
		else{
			return true;
		}
	}
}	
</script>