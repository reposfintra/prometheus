<!--  
	 - Author(s)       :      Jose de la rosa
	 - Description     :      AYUDA DESCRIPTIVA - Modificar Ingreso Especial
	 - Date            :      23/05/2006 
	 - Copyright Notice:      TRANSPORTES SANCHEZ POLO S.A.
-->
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN""http://www.w3.org/TR/html4/loose.dtd">
<%@page session="true"%> 
<%@page errorPage="/error/ErrorPage.jsp"%>
<%@page import="com.tsp.operation.model.beans.*, java.util.*"%>
<%@ include file="/WEB-INF/InitModel.jsp"%>
<%@ taglib uri="http://jakarta.apache.org/taglibs/input-0.90" prefix="input" %>
<html>
<head>
<title>AYUDA DESCRIPTIVA - Modificar Ingreso Especial</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link href="<%= BASEURL %>/css/estilostsp.css" rel="stylesheet" type="text/css">
<script type='text/javascript' src="<%= BASEURL %>/js/boton.js"></script>
</head>

<body>
<br>
<table width="696"  border="2" align="center">
  <tr>
    <td width="811" >
      <table width="100%" border="0" align="center">
        <tr  class="subtitulo">
          <td height="24" colspan="2"><div align="center">Ingreso Especial</div></td>
        </tr>
        <tr class="subtitulo1">
          <td colspan="2">Modificar Ingreso Especial </td>
        </tr>
		<tr class="subtitulo1">
          <td colspan="2">INFORMACION DEL INGRESO ESPECIAL </td>
        </tr>
        <tr>
          <td  class="fila">Porcentaje Ingreso</td>
          <td  class="ayudaHtmlTexto">Campo para digitar el porcentaje de ingreso a ingresar. Este campo es de m&aacute;ximo 6 caracteres. </td>
        </tr>
        <tr>
          <td class="fila">Bot&oacute;n Modificar </td>
          <td  class="ayudaHtmlTexto">Bot&oacute;n para confirmar y realizar la modificaci&oacute;n del Ingreso Especial.</td>
        </tr>
		<tr>
          <td class="fila">Bot&oacute;n Anular </td>
          <td  class="ayudaHtmlTexto">Bot&oacute;n que anula el registro del Ingreso Especial.</td>
        </tr>
		<tr>
          <td width="149" class="fila">Bot&oacute;n Salir</td>
          <td width="525"  class="ayudaHtmlTexto">Bot&oacute;n para salir de la vista 'Modificar Ingreso Especial' y volver a la vista del listado.</td>
        </tr>
      </table>
    </td>
  </tr>
</table>
<p></p>
<center>
<img src = "<%=BASEURL%>/images/botones/salir.gif" style = "cursor:hand" name = "imgsalir" onClick = "parent.close();" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);">
</center>
</body>
</html>
