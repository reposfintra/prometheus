<!--  
	 - Author(s)       :      LREALES
	 - Description     :      AYUDA FUNCIONAL - Modificar Descuentos de Equipos
	 - Date            :      23/05/2006 
	 - Copyright Notice:      TRANSPORTES SANCHEZ POLO S.A.
-->
<%@ include file="/WEB-INF/InitModel.jsp"%>
<HTML>
<HEAD>
<TITLE>AYUDA FUNCIONAL - Modificar Descuentos de Equipos</TITLE>
<META http-equiv=Content-Type content="text/html; charset=windows-1252">
<link href="../css/estilostsp.css" rel="stylesheet" type="text/css"> 
<link href="<%=BASEURL%>/css/estilostsp.css" rel="stylesheet" type="text/css">
<script type='text/javascript' src="<%= BASEURL %>/js/boton.js"></script> 
</HEAD>
<BODY> 
<% String BASEIMG = BASEURL +"/images/ayuda/equipos/modificar/"; %>
  <table width="95%"  border="2" align="center">
    <tr>
      <td height="117" >
        <table width="100%" border="0" align="center">
          <tr  class="subtitulo">
            <td height="20"><div align="center">MANUAL DE EQUIPOS WEB</div></td>
          </tr>
          <tr class="subtitulo1">
            <td>Descripci&oacute;n del funcionamiento del programa para Modificar Descuentos de Equipos.</td>
          </tr>
          <tr>
            <td  class="ayudaHtmlTexto"><div align="center">
              <p>&nbsp;</p>
              </div></td>
          </tr>
<tr>
            <td  class="ayudaHtmlTexto"><p class="ayudaHtmlTexto">En la siguiente pantalla se encuentran los datos del descuento de equipo buscado, y se puede realizar la modificaci&oacute;n de ellos.</p>
            </td>
          </tr>
<tr>
            <td  class="ayudaHtmlTexto"><div align="center"><IMG height=219 src="<%=BASEIMG%>image001.JPG" width=473 border=0 v:shapes="_x0000_i1143"></div></td>
          </tr>
<tr>
            <td  class="ayudaHtmlTexto"><p class="ayudaHtmlTexto">&nbsp;</p>
              <p class="ayudaHtmlTexto">El sistema verifica que todos los campos esten llenos, si no lo estan le saldr&aacute; en la pantalla el siguiente mensaje. </p>
              </td>
          </tr>
<tr>
  <td  class="ayudaHtmlTexto"><div align="center"><IMG height=124 src="<%=BASEIMG%>image_error001.JPG" width=266 border=0 v:shapes="_x0000_i1054"></div></td>
</tr>
<tr>
  <td  class="ayudaHtmlTexto"><p>&nbsp;</p>
    <p>Al presionar click en el bot&oacute;n 'MODIFICAR' entonces el sistema le mostrar&aacute; el siguiente mensaje. </p></td>
</tr>
<tr>
  <td  class="ayudaHtmlTexto"><div align="center"><IMG height=96 src="<%=BASEIMG%>image002.JPG" width=449 border=0 v:shapes="_x0000_i1054"></div></td>
</tr>
<tr>
  <td  class="ayudaHtmlTexto"><p>&nbsp;</p>
    <p>Al presionar click en el bot&oacute;n 'ELIMINAR' entonces el sistema le mostrar&aacute; el siguiente mensaje.</p></td>
</tr>
<tr>
            <td  class="ayudaHtmlTexto"><div align="center"><IMG height=98 src="<%=BASEIMG%>image003.JPG" width=450 border=0 v:shapes="_x0000_i1054"></div></td>
          </tr>
      </table></td>
    </tr>
  </table>
  <p></p>
<center>
<img src = "<%=BASEURL%>/images/botones/salir.gif" style = "cursor:hand" name = "imgsalir" onClick = "parent.close();" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);">
</center>
</BODY>
</HTML>
