<!--  
	 - Author(s)       :      LREALES
	 - Description     :      AYUDA DESCRIPTIVA - Ingresar Descuentos de Equipos
	 - Date            :      23/05/2006 
	 - Copyright Notice:      TRANSPORTES SANCHEZ POLO S.A.
-->
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN""http://www.w3.org/TR/html4/loose.dtd">
<%@page session="true"%> 
<%@page errorPage="/error/ErrorPage.jsp"%>
<%@page import="com.tsp.operation.model.beans.*, java.util.*"%>
<%@ include file="/WEB-INF/InitModel.jsp"%>
<%@ taglib uri="http://jakarta.apache.org/taglibs/input-0.90" prefix="input" %>
<html>
<head>
<title>AYUDA DESCRIPTIVA - Ingresar Descuentos de Equipos</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link href="<%= BASEURL %>/css/estilostsp.css" rel="stylesheet" type="text/css">
<script type='text/javascript' src="<%= BASEURL %>/js/boton.js"></script>
</head>

<body>
<br>
<table width="696"  border="2" align="center">
  <tr>
    <td width="811" >
      <table width="100%" border="0" align="center">
        <tr  class="subtitulo">
          <td height="24" colspan="2"><div align="center">Equipos</div></td>
        </tr>
        <tr class="subtitulo1">
          <td colspan="2">Ingresar Descuentos de Equipos   - Pantalla Preliminar</td>
        </tr>
		<tr class="subtitulo1">
          <td colspan="2">INFORMACION DEL DESCUENTO</td>
        </tr>
        <tr>
          <td  class="fila">'C&oacute;digo'</td>
          <td  class="ayudaHtmlTexto">Campo para digitar el c&oacute;digo del descuento de equipo que deseo agregar. Este campo es de m&aacute;ximo 12 caracteres. </td>
        </tr>
        <tr>
          <td  class="fila">'Descripci&oacute;n'</td>
          <td  class="ayudaHtmlTexto">Campo de texto donde ingreso la descripci&oacute;n del descuento de equipo. </td>
        </tr>
        <tr>
          <td  class="fila">'Concepto Contable'</td>
          <td  class="ayudaHtmlTexto">Campo para digitar el concepto contable del descuento de equipo que deseo agregar. Este campo es de m&aacute;ximo 15 caracteres. </td>
        </tr>
        <tr>
          <td  class="fila">'Concepto Especial'</td>
          <td  class="ayudaHtmlTexto">Campo para digitar el concepto especial del descuento de equipo que deseo agregar. Este campo es de m&aacute;ximo 15 caracteres. </td>
        </tr>
        <tr>
          <td class="fila">Bot&oacute;n Aceptar </td>
          <td  class="ayudaHtmlTexto">Bot&oacute;n para confirmar y realizar el ingreso del descuento de equipo.</td>
        </tr>
		<tr>
          <td class="fila">Bot&oacute;n Cancelar </td>
          <td  class="ayudaHtmlTexto">Bot&oacute;n que resetea la pantalla dejandola en su estado inicial de ingreso.</td>
        </tr>
		<tr>
          <td width="149" class="fila">Bot&oacute;n Salir</td>
          <td width="525"  class="ayudaHtmlTexto">Bot&oacute;n para salir de la vista 'Ingresar Descuentos de Equipos' y volver a la vista del men&uacute;.</td>
        </tr>
      </table>
    </td>
  </tr>
</table>
<p></p>
<center>
<img src = "<%=BASEURL%>/images/botones/salir.gif" style = "cursor:hand" name = "imgsalir" onClick = "parent.close();" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);">
</center>
</body>
</html>
