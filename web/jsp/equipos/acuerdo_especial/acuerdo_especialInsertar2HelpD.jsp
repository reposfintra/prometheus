<!--  
	 - Author(s)       :      Jose de la rosa
	 - Description     :      AYUDA DESCRIPTIVA - ingresar acuerdos especiales
	 - Date            :      08/06/2006 
	 - Copyright Notice:      TRANSPORTES SANCHEZ POLO S.A.
-->
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN""http://www.w3.org/TR/html4/loose.dtd">
<%@page session="true"%> 
<%@page errorPage="/error/ErrorPage.jsp"%>
<%@page import="com.tsp.operation.model.beans.*, java.util.*"%>
<%@ include file="/WEB-INF/InitModel.jsp"%>
<%@ taglib uri="http://jakarta.apache.org/taglibs/input-0.90" prefix="input" %>
<html>
<head>
<title>AYUDA DESCRIPTIVA - Equipos</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link href="<%= BASEURL %>/css/estilostsp.css" rel="stylesheet" type="text/css">
<script type='text/javascript' src="<%= BASEURL %>/js/boton.js"></script>
</head>

<body>
<br>
<table width="696"  border="2" align="center">
  <tr>
    <td width="811" >
      <table width="100%" border="0" align="center">
        <tr  class="subtitulo">
          <td height="24" colspan="2"><div align="center">Acuerdos Especiales</div></td>
        </tr>
        <tr class="subtitulo1">
          <td colspan="2">Ingresar Acuerdos Especiales </td>
        </tr>
		<tr class="subtitulo1">
          <td colspan="2">INFORMACION DEL ACUERDO </td>
        </tr>
        <tr>
          <td  class="fila">Standar/Cliente</td>
          <td  class="ayudaHtmlTexto">Campo para digitar el codigo del cliente a buscar. Este campo es de m&aacute;ximo 6 caracteres. </td>
        </tr>		
        <tr>
          <td  class="fila">Consultar Clientes</td>
          <td  class="ayudaHtmlTexto">Link para buscar los clientes </td>
        </tr>
        <tr>
          <td  class="fila">Ruta Origen</td>
          <td  class="ayudaHtmlTexto">Campo para escoger la ruta de origen del estandar. </td>
        </tr>			
        <tr>
          <td  class="fila">Ruta Destino</td>
          <td  class="ayudaHtmlTexto">Campo para escoger la ruta destino del estandar. </td>
        </tr>
        <tr>
          <td  class="fila">Estandar Job</td>
          <td  class="ayudaHtmlTexto">Campo para escoger el estandar job del cliente con la ruta seleccionada. </td>
        </tr>
        <tr>
          <td  class="fila">Concepto</td>
          <td  class="ayudaHtmlTexto">campo para escoger el concepto del acuerdo. </td>
        </tr>
        <tr>
          <td  class="fila">Porcentaje Descuento</td>
          <td  class="ayudaHtmlTexto">Campo para digitar el porcentaje de descuento a ingresar. Este campo es de m&aacute;ximo 6 caracteres. </td>
        </tr>
        <tr>
          <td  class="fila">Tipo</td>
          <td  class="ayudaHtmlTexto">Campo para escoger al tipo que se le va a aplicar el acuerdo. </td>
        </tr>
	<tr>
          <td class="fila">Bot&oacute;n Buscar </td>
          <td  class="ayudaHtmlTexto">El primer Bot&oacute;n buscar permite realizar una busqueda de clientes.</td>
        </tr>
        <tr>
          <td class="fila">Bot&oacute;n Buscar </td>
          <td  class="ayudaHtmlTexto">El segundo Bot&oacute;n de buscar permite realizar una busqueda de standar job.</td>
        </tr>
        <tr>
          <td class="fila">Bot&oacute;n Ingresar </td>
          <td  class="ayudaHtmlTexto">Bot&oacute;n para ingresar datos del acuerdo especial.</td>
        </tr>
        <tr>
          <td width="149" class="fila">Bot&oacute;n Salir</td>
          <td width="525"  class="ayudaHtmlTexto">Bot&oacute;n para salir de la vista 'Ingresar Acuerdo Especial' y volver a la vista del listado.</td>
        </tr>
      </table>
    </td>
  </tr>
</table>
<p></p>
<center>
<img src = "<%=BASEURL%>/images/botones/salir.gif" style = "cursor:hand" name = "imgsalir" onClick = "parent.close();" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);">
</center>
</body>
</html>
