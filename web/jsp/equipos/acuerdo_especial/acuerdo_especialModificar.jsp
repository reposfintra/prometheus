<!--
- Autor : Ing. Jose de la rosa
- Date  : 2 de Diciembre de 2005
- Copyrigth Notice : Fintravalores S.A. S.A
-->
<%--
-@(#)
--Descripcion : Pagina JSP, que maneja la actualización de los acuerdos especiales
--%>

<%-- Declaracion de librerias--%>
<%@page contentType="text/html"%>
<%@page session="true"%>
<%@page import="java.util.*"%>
<%@page import="com.tsp.operation.model.*"%>
<%@page import="com.tsp.operation.model.beans.*"%>
<%@page import="com.tsp.util.*"%>
<%@include file="/WEB-INF/InitModel.jsp"%>
<html>
<head>
<title>Modificar Acuerdo Especial</title>
<link href="<%= BASEURL %>/css/estilostsp.css" rel="stylesheet" type="text/css">
<script type='text/javascript' src="<%= BASEURL %>/js/boton.js"></script>
</head>
<script type='text/javascript' src="<%= BASEURL %>/js/validar.js"></script>
<body onResize="redimensionar();" <%if(request.getParameter("reload")!=null){%>onLoad="parent.opener.location.reload();redimensionar();"<%}%>>
<div id="capaSuperior" style="position:absolute; width:100%; height:100px; z-index:0; left: 0px; top: 0px;">
<jsp:include page="/WEB-INF/toptsp.jsp?encabezado=Listar Acuerdo Especial"/>
</div>

<div id="capaCentral" style="position:absolute; width:100%; height:85%; z-index:0; left: 0px; top: 100px; overflow: scroll;"> 
<%
  String path = application.getRealPath(model.menuService.getRealUrl(request.getRequestURI()));
  String datos[] = model.menuService.getContenidoMenu(BASEURL,request.getRequestURI(),path);
    Usuario usuario = (Usuario) session.getAttribute("Usuario");
    Acuerdo_especial a = model.acuerdo_especialService.getAcuerdo_especial(); 
   Concepto_equipo cm = model.concepto_equipoService.getConcepto_equipo();
    String mensaje = (String) request.getAttribute("mensaje");
%>
<FORM name='form1' id='form1' method='POST' action='<%=CONTROLLER%>?estado=Acuerdo_especial&accion=Update'>
	<table width="500" border="2" align="center">
      <tr>
        <td>
          <table width="100%" class="tablaInferior">
            <tr class="fila">
              <td align="left" colspan="2" class="subtitulo1">&nbsp;Acuerdo Especial </td>
              <td align="left" colspan="2" class="barratitulo"><img src="<%=BASEURL%>/images/titulo.gif" width="32" height="20" align="left"><%=datos[0]%>
              <input type="hidden" name="tipo" id="tipo" value="<%=a.getTipo()%>"></td>
            </tr>
          <tr class="fila">
            <td align="left" ><%=a.getTipo().equals("S")?"Standar":"Placa"%> </td>
            <td width="24%" align="left" ><%=a.getStandar()%><input type="hidden" name="c_standar" value="<%=a.getStandar()%>"></td>
            <td width="15%" valign="middle">Concepto             </td>
            <td width="35%" valign="middle"><%=cm.getDescripcion()%><input type="hidden" name="c_codigo" value="<%=a.getCodigo_concepto()%>"></td>
          </tr>
          <tr class="fila">
            <td width="26%" align="left" >Porcentaje Ingreso</td>
            <td align="left" ><input name="c_porcentaje" type="text" class="textbox" id="c_porcentaje" size="7" maxlength="7" value="<%=a.getPorcentaje_descuento()%>"  onKeyPress="soloDigitos(event,'decOK')">
            <img src="<%=BASEURL%>/images/botones/iconos/obligatorio.gif"></td>
            <td >Tipo</TD>
			<td ><%if(a.getTipo().equals("S")){%>
			<select name="c_tipo" id="c_tipo" class="textbox" >
                <option value="CLIENTE">Cliente</option>
                <option value="CABEZOTE" <%if(a.getTipo_acuerdo().equals("CABEZOTE")){%> selected <%}%> >Cabezote</option>
              </select><img src="<%=BASEURL%>/images/botones/iconos/obligatorio.gif">
			  <%}else{%>Cabezote
			  <input type="hidden" name="c_tipo" value="<%=a.getTipo_acuerdo()%>">
			  <%}%></TD>
            </tr>	
        </table></td>
      </tr>
    </table>
	<p>
	<div align="center"><img src="<%=BASEURL%>/images/botones/modificar.gif" style="cursor:hand" title="Modificar un acuerdo especial" name="modificar"  onclick="if(valido_porcentaje()){ form1.submit();}" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);">&nbsp;
	<img src="<%=BASEURL%>/images/botones/anular.gif" style="cursor:hand" title="Anular un acuerdo especial" name="anular"  onClick="window.location='<%=CONTROLLER%>?estado=Acuerdo_especial&accion=Anular&c_standar=<%=a.getStandar()%>&c_codigo=<%=a.getCodigo_concepto()%>&tipo=<%=a.getTipo()%>'" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);">&nbsp;
	<img src="<%=BASEURL%>/images/botones/salir.gif" style="cursor:hand" name="salir" title="Salir al Menu Principal" onMouseOver="botonOver(this);" onClick="parent.close();" onMouseOut="botonOut(this);" ></div>
	</p>
	<%  if(mensaje!=null){%>
		<table border="2" align="center">
		  <tr>
			<td><table width="100%"  border="1" align="center"  bordercolor="#F7F5F4" bgcolor="#FFFFFF">
			  <tr>
				<td width="229" align="center" class="mensajes"><%=mensaje%></td>
				<td width="29" background="<%=BASEURL%>/images/cuadronaranja.JPG">&nbsp;</td>
				<td width="58">&nbsp;</td>
			  </tr>
			</table></td>
		  </tr>
		</table>
	<%  }%>
	<br>
	<div align="center">
            <span onClick="window.open('<%=BASEURL%>/jsp/equipos/ingreso_especial/ingreso_especialInsertar.jsp?c_tipo_acuerdo=<%=a.getTipo_acuerdo()%>&c_codigo_concepto=<%=a.getCodigo_concepto()%>','','HEIGHT=500,WIDTH=700,SCROLLBARS=NO,RESIZABLE=YES')" class="Simulacion_Hiper" style="cursor:hand">Agregar ingresos especiales</span>
        </div>
</FORM>
</div>
<%=datos[1]%>
</body>
</html>
<script>
function valido_porcentaje(){
	if(form1.c_porcentaje.value==""){
		alert("Debe llenar el porcentaje de descuento");
		form1.c_porcentaje.focus();
		return false;
	}
	else
		if( validarDecimales( form1.c_porcentaje, 3 ) )
			return true;
		else
			return false;
	}
	
	
function validarDecimales( valor , entero ){
	if( valor.value.indexOf('.') != -1 ){
		var porcentaje = valor.value.split(".");
		if(porcentaje[0].length > entero ){
			alert("El numero no puedes ser mayor a "+entero+" decimales");
			valor.focus();
			return false;
		}
		else{
			if(porcentaje[0].length == 0 ){
				valor.value = "0."+ porcentaje[1];
			}
			return true;
		}
	}
	else{	
		if(valor.value.length > entero ){
			alert("El numero no puedes ser mayor a "+entero+" decimales");
			valor.focus();
			return false;
		}
		else{
			return true;
		}
	}
}
	
</script>