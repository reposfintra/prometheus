<!--  
	 - Author(s)       :      Jose de la Rosa
	 - Description     :      AYUDA FUNCIONAL - Modificar Acuerdos especiales
	 - Date            :      08/06/2006 
	 - Copyright Notice:      TRANSPORTES SANCHEZ POLO S.A.
-->
<%@ include file="/WEB-INF/InitModel.jsp"%>
<HTML>
<HEAD>
<TITLE>AYUDA FUNCIONAL - Modificar Acuerdos Especiales</TITLE>
<META http-equiv=Content-Type content="text/html; charset=windows-1252">
<link href="../css/estilostsp.css" rel="stylesheet" type="text/css"> 
<link href="<%=BASEURL%>/css/estilostsp.css" rel="stylesheet" type="text/css">
<script type='text/javascript' src="<%= BASEURL %>/js/boton.js"></script> 
</HEAD>
<BODY> 
<% String BASEIMG = BASEURL +"/images/ayuda/equipos/acuerdo_especial/"; %>
  <table width="95%"  border="2" align="center">
    <tr>
      <td height="117" >
        <table width="100%" border="0" align="center">
          <tr  class="subtitulo">
            <td height="20"><div align="center">MANUAL DE EQUIPOS WEB</div></td>
          </tr>
          <tr class="subtitulo1">
            <td>Descripci&oacute;n del funcionamiento del programa para Modificar Acuerdos Especiales.</td>
          </tr>
          <tr>
            <td  class="ayudaHtmlTexto"><div align="center">
              <p>&nbsp;</p>
              </div></td>
          </tr>
            <tr>
            <td  class="ayudaHtmlTexto"><p class="ayudaHtmlTexto">En la siguiente pantalla se modifican los datos del Acuerdo Especial.</p>
            </td>
          </tr>
            <tr>
            <td  class="ayudaHtmlTexto"><div align="center"><IMG src="<%=BASEIMG%>ModificarAcuerdoEspecial.JPG" border=0 ></div></td>
          </tr>

            <tr>
            <td  class="ayudaHtmlTexto"><p class="ayudaHtmlTexto">&nbsp;</p>
            <p class="ayudaHtmlTexto">El sistema verifica si el porcentaje de descuento esta lleno, si no lo estan le saldr&aacute; en la pantalla el siguiente mensaje. </p>
            </td>
            </tr>
            <tr>
            <td  class="ayudaHtmlTexto"><div align="center"><IMG  src="<%=BASEIMG%>MensajeErrorCamposLLenos.JPG" border=0 ></div></td>
            </tr>
            <tr>
            <td  class="ayudaHtmlTexto"><p class="ayudaHtmlTexto">&nbsp;</p>
            <p class="ayudaHtmlTexto">El sistema verifica si los datos estan digitados correctamente le saldr&aacute; en la pantalla el siguiente mensaje. </p>
            </td>
            </tr>
            <tr>
            <td  class="ayudaHtmlTexto"><div align="center"><IMG  src="<%=BASEIMG%>MensajeModificacionAcuerdo.JPG" border=0 ></div></td>
            </tr>		
                <tr>
                  <td  class="ayudaHtmlTexto"><p>&nbsp;</p>
                    <p>Si el Acuerdo especial se anula, aparecer&aacute; el siguiente mensaje. </p></td>
                </tr>
                <tr>
              <td  class="ayudaHtmlTexto"><div align="center"><IMG src="<%=BASEIMG%>MensajeAnular.JPG" border=0 ></div></td>
            </tr>
      </table></td>
    </tr>
  </table>
  <p></p>
<center>
<img src = "<%=BASEURL%>/images/botones/salir.gif" style = "cursor:hand" name = "imgsalir" onClick = "parent.close();" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);">
</center>
</BODY>
</HTML>
