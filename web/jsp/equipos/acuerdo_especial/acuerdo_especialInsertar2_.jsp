<!--
- Autor : Ing. Jose de la rosa
- Date  : 6 de Diciembre de 2005
- Copyrigth Notice : Fintravalores S.A. S.A
-->
<%--
-@(#)
--Descripcion : Pagina JSP, que maneja el ingreso de los acuerdos especiales
--%>

<%-- Declaracion de librerias--%>
<%@page session="true"%>
<%@page errorPage="../error/ErrorPage.jsp"%>
<%@page import="com.tsp.operation.model.beans.*, java.util.*, java.text.*"%>
<%@ include file="/WEB-INF/InitModel.jsp"%>
<%@page import="com.tsp.operation.model.*"%>
<%@ taglib uri="http://jakarta.apache.org/taglibs/input-0.90" prefix="input" %>
<%
String Mensaje       = ((String)request.getAttribute("mensaje")!=null)?(String)request.getAttribute("mensaje"):"";
  String path = application.getRealPath(model.menuService.getRealUrl(request.getRequestURI()));
  String datos[] = model.menuService.getContenidoMenu(BASEURL,request.getRequestURI(),path);
%>
<html>
<head>
<title>Acuerdo Especial</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<script language="javascript" src="<%=BASEURL%>/js/validar.js"></script>
<script language="javaScript" type="text/javascript" src="<%=BASEURL%>/js/boton.js"></script>

<link href="<%=BASEURL%>/css/estilostsp.css" rel="stylesheet" type="text/css">
<link href="../css/estilostsp.css" rel="stylesheet" type="text/css">

</head>

<body onResize="redimensionar()" onLoad="redimensionar()">
<div id="capaSuperior" style="position:absolute; width:100%; height:100px; z-index:0; left: 1px; top: 0px;">
 <jsp:include page="/toptsp.jsp?encabezado=Acuerdo Especial"/>
</div>
<div id="capaCentral" style="position:absolute; width:100%; height:87%; z-index:0; left: 0px; top: 100px; overflow: scroll;">
  <br>
  <form name="form1" method="post" action="<%=CONTROLLER%>?estado=Acuerdo_especial&accion=Insert">
    <input name="Opcion" type="hidden" id="Opcion">
	 <table width="600"  border="2" align="center">
       <tr>
         <td><table width="100%"  border="0" class="tablaInferior">
            <tr>
          		<td colspan="2" class="subtitulo1">Acuerdo Especial <td class="barratitulo" colspan="2"><img src="<%=BASEURL%>/images/titulo.gif" width="32" height="20" align="left"><%=datos[0]%></td>
        	</tr>            
             <tr class="fila">
               <th width="176" rowspan="2" scope="row"><div align="left">STANDARD/CLIENTE</div></th>
               <td colspan="3">
                 <p> <span class="Simulacion_Hiper" style="cursor:hand " onClick="window.open('<%=BASEURL%>/consultas/consultasClientes.jsp','','HEIGHT=200,WIDTH=600,SCROLLBARS=YES,RESIZABLE=YES')">Consultar clientes...</span></p></td>
             </tr>
             <tr>
               <td class="fila" colspan="3"><p class="fila">
                   <input name="cliente" type="text" id="cliente" maxlength="6" onKeyPress="return verificarTeclaAcuerdo2(event, '<%=CONTROLLER%>');" class="textbox">
                   <img src="<%=BASEURL%>/images/botones/buscar.gif" width="87" height="21" align="absmiddle" style="cursor:hand" onClick="if(form1.cliente.value==''){window.location='<%=BASEURL%>/jsp/equipos/acuerdo_especial/acuerdo_especialInsertar2.jsp';}else{buscarClientAcuerdo2('<%=CONTROLLER%>');}" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);">                   
                   <input name="clienteR" type="hidden" id="clienteR" value="<%=request.getParameter("cliente")%>">
                   <input name="standard_nom" type="hidden" id="standard_nom" value="<%=(String)request.getAttribute("std") %>">
                   <input name="remitentes" type="hidden" id="remitentes" value=" ">
                   <input name="docinterno" type="hidden" id="docinterno" value=" ">
                   <input name="fechadesp" type="hidden" id="fechadesp" value=" ">
                   <input name="destinatarios" type="hidden" id="destinatarios" value=" ">
                   <input name="trailer" type="hidden" id="trailer" value=" ">
                   <input name="conductor" type="hidden" id="conductor" value=" ">
                   <input name="ruta" type="hidden" id="ruta" value=" ">
                   <input name="toneladas" type="hidden" id="toneladas" value=" ">
                   <input name="valorpla" type="hidden" id="valorpla3" value=" ">
                   <input name="anticipo" type="hidden" id="anticipo" value=" ">
                   <input type="hidden" name="hiddenField7">
                   <input name="precintos" type="hidden" id="precintos" value=" ">
                   <input name="observacion" type="hidden" id="observacion" value=" ">
                   <input name="click_buscar" type="hidden" id="click_buscar">
				   <input name="standard" type="hidden" id="standard" value=" ">
				   <input name="c_standar" type="hidden" id="c_standar" value=" ">
				   </p>
               </td>
             </tr>
			<tr class="fila">
               <td rowspan="2" ><strong>RUTA</strong>
               <td width="180"><strong>ORIGEN</strong></td>
               <td colspan="2">
                 <%TreeMap ciudades = model.stdjobdetselService.getCiudadesOri(); 
				  String corigen="";
				  if(request.getParameter("origen")!=null){
					corigen = request.getParameter("origen");
				  }
				  String cdest="";
				  if(request.getParameter("destino")!=null){
					cdest = request.getParameter("destino");
				  }
				  %>
                 <input:select name="ciudadOri" options="<%=ciudades%>" attributesText="style='width:100%;' onChange='buscarDestinosF()' ; class=listmenu" default='<%=corigen%>'/> </td>
             </tr>
             <tr class="Estilo6">
               <td class="fila"><strong>DESTINO</strong></td>
               <td colspan="2" class="fila">
                 <%TreeMap ciudadesDest = model.stdjobdetselService.getCiudadesDest(); %>
                 <input:select name="ciudadDest" options="<%=ciudadesDest%>" attributesText="style='width:100%;' class=listmenu"  default='<%=cdest%>'/> </td>
				 
             </tr>
             <%if(ciudadesDest.size()>0){%>
             <tr class="fila">
               <td colspan="4" >                 <div align="center">                   <img src="<%=BASEURL%>/images/botones/buscar.gif" width="87" height="21" onClick="buscarStandardF()" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" style="cursor:hand"></div>
             </tr>
             <% }%>			 
              <tr class="fila">
                   <td ><strong>Concepto </strong></TD>
                   <td ><select name="c_codigo" id="c_codigo" class="textbox">
						<% model.concepto_equipoService.listConcepto_equipo(); 
						Vector vecd = model.concepto_equipoService.getConcepto_equipos();
						for(int i = 0; i<vecd.size(); i++){	
							Concepto_equipo c = (Concepto_equipo) vecd.elementAt(i);%>
							<option value="<%=c.getCodigo()%>"><%=c.getDescripcion()%></option>
						<%}%>
					</select>		
					<img src="<%=BASEURL%>/images/botones/iconos/obligatorio.gif"></TD>
                   <td width="150"><strong>Porcentaje Descuento </strong> </td>
                   <td width="64"><input name="c_porcentaje" type="text" class="textbox" id="c_porcentaje" size="7" maxlength="7"  onKeyPress="soloDigitos(event,'decOK')">
                   <img src="<%=BASEURL%>/images/botones/iconos/obligatorio.gif"></td>
		    </tr>	
				 <tr class="fila">
				   <td ><strong>Tipo</strong>
				   <td ><select name="c_tipo" id="c_tipo" class="textbox">
						 <option value="CLIENTE">Cliente</option>
						 <option value="CABEZOTE">Cabezote</option>
                     </select><img src="<%=BASEURL%>/images/botones/iconos/obligatorio.gif">
				   <td colspan="2">&nbsp;</td>
				 </tr>		
				 <tr class="fila">
				   <td colspan="4" > 		
             <div align="center">
                   <img src="<%=BASEURL%>/images/botones/aceptar.gif" width="90" height="21" onClick="if(valido_porcentaje()){ Opcion.value='Guardar';form1.submit();}" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" style="cursor:hand">&nbsp;
				   <img src="<%=BASEURL%>/images/botones/salir.gif" style="cursor:hand" name="salir" title="Salir al Menu Principal" onMouseOver="botonOver(this);" onClick="parent.close();" onMouseOut="botonOut(this);" > </div>
			 
			 </td>
			 </tr>
         </table></td>
       </tr>
    </table>
    	</td>
      </tr>
    </table>
</form>

  <p>
    <%if(!Mensaje.equals("")){%>
  <table border="2" align="center">
    <tr>
      <td><table width="100%"  border="1" align="center"  bordercolor="#F7F5F4" bgcolor="#FFFFFF">
          <tr>
            <td width="229" align="center" class="mensajes"><%=Mensaje%></td>
            <td width="29" background="<%=BASEURL%>/images/cuadronaranja.JPG">&nbsp;</td>
            <td width="58">&nbsp;</td>
          </tr>
      </table></td>
    </tr>
  </table>
  <br>
  <%}
  if(Mensaje.equals("La información ha sido ingresada exitosamente!")){%>
  <div align="center"> <span onClick="window.open('<%=BASEURL%>/jsp/equipos/ingreso_especial/ingreso_especialInsertar.jsp?c_tipo_acuerdo=<%=(String)request.getAttribute("tipo")%>&c_codigo_concepto=<%=(String)request.getAttribute("cod")%>','','HEIGHT=500,WIDTH=700,SCROLLBARS=NO,RESIZABLE=YES')" class="Simulacion_Hiper" style="cursor:hand">Agregar ingresos especiales</span></div>
  <%}%>
  </div>
  <%=datos[1]%>
</body>
</html>
<script>
function buscarClientAcuerdo2(controller){
 form1.click_buscar.value="ok";
 if(form1.cliente.value.length<6){
  var tamano = 6-form1.cliente.value.length;
  var ceros='';
  i =1;
  while(i<=tamano){
   ceros = ceros + '0';
   i++;
  }
  form1.cliente.value = ceros +form1.cliente.value;
  window.location=controller+'?estado=Buscar&accion=StandardColpapel&op=1&cliente='+form1.cliente.value+'&acuerdo=ok';
 }
 else if(form1.cliente.value!=''){
  window.location=controller+'?estado=Buscar&accion=StandardColpapel&op=1&cliente='+form1.cliente.value+'&acuerdo=ok';
 }else{
  alert('Escriba el codigo del cliente o el Standard correcto');
 }
}
function valido_porcentaje(){
	if(form1.c_porcentaje.value==""){
		alert("Debe llenar el porcentaje de descuento");
		form1.c_porcentaje.focus();
		return false;
	}
	else
		if( validarDecimales( form1.c_porcentaje, 3 ) )
			return true;
		else
			return false;
	}
function validarDecimales( valor , entero ){
	if( valor.value.indexOf('.') != -1 ){
		var porcentaje = valor.value.split(".");
		if(porcentaje[0].length > entero ){
			alert("El numero no puedes ser mayor a "+entero+" decimales");
			valor.focus();
			return false;
		}
		else{
			if(porcentaje[0].length == 0 ){
				valor.value = "0."+ porcentaje[1];
			}
			return true;
		}
	}
	else{	
		if(valor.value.length > entero ){
			alert("El numero no puedes ser mayor a "+entero+" decimales");
			valor.focus();
			return false;
		}
		else{
			return true;
		}
	}
}	

//jose 09-12-2005
function verificarTeclaAcuerdo2(e, controller) {
 var key = (isIE) ? window.event.keyCode : e.which;
 //alert(key);
 var isNum = (key==13) ? false:true;
 if(!isNum){
  buscarClientAcuerdo2(controller);
 }
 return (isNum);
}

function buscarClientAcuerdo2(controller){
 form1.click_buscar.value="ok";
 if(form1.cliente.value.length<6){
  var tamano = 6-form1.cliente.value.length;
  var ceros='';
  i =1;
  while(i<=tamano){
   ceros = ceros + '0';
   i++;
  }
  form1.cliente.value = ceros +form1.cliente.value;
  window.location=controller+'?estado=Buscar&accion=StandardColpapel&op=1&cliente='+form1.cliente.value+'&acuerdo=ok';
 }
 else if(form1.cliente.value!=''){
  window.location=controller+'?estado=Buscar&accion=StandardColpapel&op=1&cliente='+form1.cliente.value+'&acuerdo=ok';
 }else{
  alert('Escriba el codigo del cliente o el Standard correcto');
 }
}
</script>