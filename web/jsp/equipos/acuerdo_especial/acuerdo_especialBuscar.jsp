<!--
- Autor : Ing. Jose de la rosa
- Date  : 2 de Diciembre de 2005
- Copyrigth Notice : Fintravalores S.A. S.A
-->
<%--
-@(#)
--Descripcion : Pagina JSP, que maneja el ingreso de los acuerdos especiales
--%>

<%-- Declaracion de librerias--%>
<%@page contentType="text/html"%>
<%@ page session="true"%>
<%@ page errorPage="../error/error.jsp"%>
<%@page import="com.tsp.operation.model.beans.*, java.util.*"%>
<%@ include file="/WEB-INF/InitModel.jsp"%>
<%@page import="com.tsp.operation.model.*"%>
<%@page import="com.tsp.util.*"%>
<%@ taglib uri="http://jakarta.apache.org/taglibs/input-0.90" prefix="input" %>
<%  
    String path = application.getRealPath(model.menuService.getRealUrl(request.getRequestURI()));
    String datos[] = model.menuService.getContenidoMenu(BASEURL,request.getRequestURI(),path);
%>
<html>
<head><title>Buscar Acuerdo Especial</title>
<link href="<%= BASEURL %>/css/estilostsp.css" rel="stylesheet" type="text/css">
<script type='text/javascript' src="<%= BASEURL %>/js/boton.js"></script>
</head>
<body onLoad="redimensionar();" onResize="redimensionar();">
<div id="capaSuperior" style="position:absolute; width:100%; height:100px; z-index:0; left: 0px; top: 0px;">
<jsp:include page="/WEB-INF/toptsp.jsp?encabezado=Buscar Acuerdo Especial"/>
</div>

<div id="capaCentral" style="position:absolute; width:100%; height:85%; z-index:0; left: 0px; top: 100px; overflow: scroll;"> 
<form name="form2" id="form2" method="post" action="<%=CONTROLLER%>?estado=Acuerdo_especial&accion=Search&listar=True&sw=True">
    <table width="380" border="2" align="center">
      <tr>
        <td><table width="100%" align="center"  class="tablaInferior">
			<tr>
				<td width="173" class="subtitulo1">&nbsp;Acuerdo Especial</td>
				<td width="205" class="barratitulo"><img src="<%=BASEURL%>/images/titulo.gif" width="32" height="20" align="left"><%=datos[0]%></td>
			</tr>
			<tr class="fila">
				<td width="173" align="left" ><input name="tipo" type="radio" value="S" checked>
				  Standar</td>
				<td valign="middle"><input name="tipo" type="radio" value="P">
			    Placa			  </td>
			</tr>			
			<tr class="fila">
				<td width="173" align="left" >Numero</td>
				<td valign="middle"><input name="c_standar" type="text" class="textbox" id="c_standar" size="15" maxlength="7">
			  </td>
			</tr>  
			<tr class="fila">
				<td align="left" >Concepto</td>
			  	<td valign="middle">
					<select name="c_codigo" id="c_codigo" class="textbox">
						<option value=""></option>
						<% model.concepto_equipoService.listConcepto_equipo(); 
						Vector vecd = model.concepto_equipoService.getConcepto_equipos();
						for(int i = 0; i<vecd.size(); i++){	
							Concepto_equipo c = (Concepto_equipo) vecd.elementAt(i);%>
							<option value="<%=c.getCodigo()%>"><%=c.getDescripcion()%></option>
						<%}%>
					</select></td>
			</tr>	  
			<tr class="fila">
				<td align="left" >Tipo Acuerdo </td>
				<td>
					<select name="c_tipo" id="c_tipo" class="textbox">
                                                <option value=""></option>
						<option value="CLIENTE">Cliente</option>
						<option value="CABEZOTE">Cabezote</option>
					</select></td>
			</tr>
        </table>
		</td>
      </tr>
    </table>
<p>
<div align="center"><img src="<%=BASEURL%>/images/botones/buscar.gif" style="cursor:hand" title="Buscar Flota Directa" name="buscar"  onClick="form2.submit();" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);">&nbsp;
<img src="<%=BASEURL%>/images/botones/detalles.gif" style="cursor:hand" title="Buscar todas las flotas directas" name="buscar"  onClick="window.location='<%=CONTROLLER%>?estado=Acuerdo_especial&accion=Search&listar=True&sw=False&tipo='" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);">&nbsp;
<img src="<%=BASEURL%>/images/botones/salir.gif"  name="regresar" style="cursor:hand" title="Salir al Menu Principal" onMouseOver="botonOver(this);" onClick="parent.close();" onMouseOut="botonOut(this);" ></div>
</p>		
</form>
</div>
<%=datos[1]%>
</body>
</html>
