<!--
- Autor : Ing. Jose de la rosa
- Date  : 6 de Diciembre de 2005
- Copyrigth Notice : Fintravalores S.A. S.A
-->
<%--
-@(#)
--Descripcion : Pagina JSP, que maneja el ingreso de los acuerdos especiales
--%>

<%-- Declaracion de librerias--%>
<%@page session="true"%>
<%@page errorPage="../error/ErrorPage.jsp"%>
<%@page import="com.tsp.operation.model.beans.*, java.util.*, java.text.*"%>
<%@ include file="/WEB-INF/InitModel.jsp"%>
<%@page import="com.tsp.operation.model.*"%>
<%@ taglib uri="http://jakarta.apache.org/taglibs/input-0.90" prefix="input" %>
<%
String Mensaje       = ((String)request.getAttribute("mensaje")!=null)?(String)request.getAttribute("mensaje"):"";
  String path = application.getRealPath(model.menuService.getRealUrl(request.getRequestURI()));
  String datos[] = model.menuService.getContenidoMenu(BASEURL,request.getRequestURI(),path);
%>
<html>
<head>
<title>Acuerdo Especial</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<script language="javascript" src="<%=BASEURL%>/js/validar.js"></script>
<script language="javaScript" type="text/javascript" src="<%=BASEURL%>/js/boton.js"></script>

<link href="<%=BASEURL%>/css/estilostsp.css" rel="stylesheet" type="text/css">
<link href="../css/estilostsp.css" rel="stylesheet" type="text/css">

</head>

<body onResize="redimensionar()" onLoad="redimensionar()">
<div id="capaSuperior" style="position:absolute; width:100%; height:100px; z-index:0; left: 1px; top: 0px;">
 <jsp:include page="/toptsp.jsp?encabezado=Acuerdo Especial"/>
</div>
<div id="capaCentral" style="position:absolute; width:100%; height:87%; z-index:0; left: 0px; top: 100px; overflow: scroll;">
  <br>
  <form name="form1" method="post" action="<%=CONTROLLER%>?estado=Acuerdo_especial&accion=Insert">
    <input name="Opcion" type="hidden" id="Opcion">
	 <table width="600"  border="2" align="center">
       <tr>
         <td><table width="100%"  border="0">
           <tr>
          		<td colspan="2" class="subtitulo1">STANDARD/CLIENTE
                <td width="53%" colspan="2" class="barratitulo"><img src="<%=BASEURL%>/images/titulo.gif" width="32" height="20" align="left"><%=datos[0]%></td>
        	</tr>
         </table>
         <table width="100%"  border="0" class="tablaInferior"> 
			 <tr class="fila">
				   <td ><strong>Tipo Acuerdo </strong><td ><select name="c_tipo" id="c_tipo" class="textbox">
						 <option value="CLIENTE">Cliente</option>
						 <option value="CABEZOTE">Cabezote</option>
                     </select><img src="<%=BASEURL%>/images/botones/iconos/obligatorio.gif">
			   <td colspan="2"><input type="checkbox" name="checkbox" value="checkbox" onClick="window.location='<%=BASEURL%>/jsp/equipos/acuerdo_especial/acuerdo_especialInsertar2.jsp';">
		         Placa 
		           <input type="hidden" name="tipo" value="S"></td>
				 </tr>	           
             <tr class="fila">
               <th colspan="2" rowspan="2" scope="row"><div align="left">STANDARD/CLIENTE</div></th>
               <td colspan="2">
                 <p> <span class="Simulacion_Hiper" style="cursor:hand " onClick="window.open('<%=BASEURL%>/consultas/consultasClientes.jsp','','HEIGHT=200,WIDTH=600,SCROLLBARS=YES,RESIZABLE=YES')">Consultar clientes...</span></p></td>
             </tr>
             <tr>
               <td colspan="2" class="fila"><p class="fila">
                   <input name="cliente" type="text" id="cliente" maxlength="6" onKeyPress="return verificarTeclaAcuerdo(event);" class="textbox">
                   <img src="<%=BASEURL%>/images/botones/buscar.gif" width="87" height="21" align="absmiddle" style="cursor:hand" onClick="if(form1.cliente.value==''){alert('digite el Standar o el Cliente');}else{buscarClientAcuerdo();}" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);">                   
                   <input name="clienteR" type="hidden" id="clienteR" value="<%=request.getParameter("cliente")%>">
                   <input name="standard_nom" type="hidden" id="standard_nom" value="<%=(String)request.getAttribute("std") %>">
                   <input name="remitentes" type="hidden" id="remitentes" value=" ">
                   <input name="docinterno" type="hidden" id="docinterno" value=" ">
                   <input name="fechadesp" type="hidden" id="fechadesp" value=" ">
                   <input name="destinatarios" type="hidden" id="destinatarios" value=" ">
                   <input name="trailer" type="hidden" id="trailer" value=" ">
                   <input name="conductor" type="hidden" id="conductor" value=" ">
                   <input name="ruta" type="hidden" id="ruta" value=" ">
                   <input name="toneladas" type="hidden" id="toneladas" value=" ">
                   <input name="valorpla" type="hidden" id="valorpla3" value=" ">
                   <input name="anticipo" type="hidden" id="anticipo" value=" ">
                   <input type="hidden" name="hiddenField7">
                   <input name="precintos" type="hidden" id="precintos" value=" ">
                   <input name="observacion" type="hidden" id="observacion" value=" ">
                   <input name="click_buscar" type="hidden" id="click_buscar">
                   <br>
                   <%if(request.getAttribute("cliente")!=null){%>
                   <input name="valorpla" type="hidden" id="valorpla">
                 </p>
                   <table width="100%" border="0" cellpadding="3" cellspacing="2" bordercolor="#CCCCCC" class="fila">
                     <tr>
                       <td><strong><%=(String) request.getAttribute("cliente")%></strong></td>
                     </tr>
                     <tr>
                       <td height="27"><strong>Agencia Due&ntilde;a del Cliente : <%=(String) request.getAttribute("agency")%></strong></td>
                     </tr>
                   </table>
                   <%}%>
               </td>
             </tr>
             <%if(request.getAttribute("std")==null){%>
             <tr class="fila">
               <td colspan="2" rowspan="2" ><strong>Ruta</strong>
               <td width="155"><strong>ORIGEN</strong></td>
               <td width="192">
                 <%TreeMap ciudades = model.stdjobdetselService.getCiudadesOri(); 
				  String corigen="";
				  if(request.getParameter("origen")!=null){
					corigen = request.getParameter("origen");
				  }
				  String cdest="";
				  if(request.getParameter("destino")!=null){
					cdest = request.getParameter("destino");
				  }
				  %>
                 <input:select name="ciudadOri" options="<%=ciudades%>" attributesText="style='width:100%;' onChange='buscarDestinosAcuerdo()' ; class=listmenu" default='<%=corigen%>'/> </td>
             </tr>
             <tr class="Estilo6">
               <td class="fila"><strong>DESTINO</strong></td>
               <td class="fila">
                 <%TreeMap ciudadesDest = model.stdjobdetselService.getCiudadesDest(); %>
                 <input:select name="ciudadDest" options="<%=ciudadesDest%>" attributesText="style='width:100%;' class=listmenu"  default='<%=cdest%>'/> </td>
             </tr>
             <%if(ciudadesDest.size()>0){%>
             <tr class="fila">
               <td colspan="4" >                 <div align="center">                   <img src="<%=BASEURL%>/images/botones/buscar.gif" width="87" height="21" onClick="buscarStandardAcuerdo()" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" style="cursor:hand"></div>
             </tr>
             <% }%>
             <%if(request.getAttribute("ok")!=null){%>
				 <tr class="fila">
				   <td colspan="2" ><strong> Estandar Job</strong>
				   <td colspan="2"><%TreeMap stdjob = model.stdjobdetselService.getStdjobTree(); %>				     <input:select name="c_standar" options="<%=stdjob%>" attributesText="style='width:100%;' class=listmenu"/> </td>
				 </tr>
				 <tr class="fila">
				   <td width="67" ><strong>Concepto </strong>
				   <td width="156" >
					<select name="c_codigo" id="c_codigo" class="textbox">
						<% model.concepto_equipoService.listConcepto_equipo(); 
						Vector vecd = model.concepto_equipoService.getConcepto_equipos();
						for(int i = 0; i<vecd.size(); i++){	
							Concepto_equipo c = (Concepto_equipo) vecd.elementAt(i);%>
							<option value="<%=c.getCodigo()%>"><%=c.getDescripcion()%></option>
						<%}%>
					</select>					 
					 <img src="<%=BASEURL%>/images/botones/iconos/obligatorio.gif">                   
				   <td><strong>Porcentaje Descuento </strong> </td>
				   <td><input name="c_porcentaje" type="text" class="textbox" id="c_porcentaje" size="6" maxlength="6"  onKeyPress="soloDigitos(event,'decOK')"><img src="<%=BASEURL%>/images/botones/iconos/obligatorio.gif"></td>
				 </tr>
							 
				 <tr class="fila">
				   <td colspan="4" >                 <div align="center">                   
					   <img src="<%=BASEURL%>/images/botones/aceptar.gif" width="90" height="21" onClick="if(valido_porcentaje()){ Opcion.value='Guardar';form1.submit(); }" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" style="cursor:hand"> &nbsp;
					   <img src="<%=BASEURL%>/images/botones/salir.gif" style="cursor:hand" name="salir" title="Salir al Menu Principal" onMouseOver="botonOver(this);" onClick="parent.close();" onMouseOut="botonOut(this);" ></div></td>
				 </tr>
             <%}
			}else{%>
             <tr class="fila">
               <td colspan="2" ><strong>Estandar Job </strong></td>
               <td colspan="2" ><%=(String) request.getAttribute("std")%>
                   <input name="c_standar" type="HIDDEN" id="c_standar" value="<%=(String)request.getAttribute("sj")%>">
               </td>
             </tr>
				 <tr class="fila">
                   <td ><strong>Concepto </strong></TD>
                   <td ><select name="c_codigo" id="c_codigo" class="textbox">
						<% model.concepto_equipoService.listConcepto_equipo(); 
						Vector vecd = model.concepto_equipoService.getConcepto_equipos();
						for(int i = 0; i<vecd.size(); i++){	
							Concepto_equipo c = (Concepto_equipo) vecd.elementAt(i);%>
							<option value="<%=c.getCodigo()%>"><%=c.getDescripcion()%></option>
						<%}%>
					</select>		
					<img src="<%=BASEURL%>/images/botones/iconos/obligatorio.gif"></TD>
                   <td><strong>Porcentaje Descuento </strong> </td>
                   <td><input name="c_porcentaje" type="text" class="textbox" id="c_porcentaje" size="7" maxlength="7"  onKeyPress="soloDigitos(event,'decOK')">
                   <img src="<%=BASEURL%>/images/botones/iconos/obligatorio.gif"></td>
		    </tr>		
				 <tr class="fila">
				   <td colspan="4" > 		
             <div align="center">
                   <img src="<%=BASEURL%>/images/botones/aceptar.gif" width="90" height="21" onClick="if(valido_porcentaje()){ Opcion.value='Guardar';form1.submit();}" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" style="cursor:hand">&nbsp;
				   <img src="<%=BASEURL%>/images/botones/salir.gif" style="cursor:hand" name="salir" title="Salir al Menu Principal" onMouseOver="botonOver(this);" onClick="parent.close();" onMouseOut="botonOut(this);" > </div>
			 <%}%>
			 </td>
			 </tr>
         </table></td>
       </tr>
    </table>
    	</td>
      </tr>
    </table>
</form>

  <p>
    <%if(!Mensaje.equals("")){%>
  <table border="2" align="center">
    <tr>
      <td><table width="100%"  border="1" align="center"  bordercolor="#F7F5F4" bgcolor="#FFFFFF">
          <tr>
            <td width="229" align="center" class="mensajes"><%=Mensaje%></td>
            <td width="29" background="<%=BASEURL%>/images/cuadronaranja.JPG">&nbsp;</td>
            <td width="58">&nbsp;</td>
          </tr>
      </table></td>
    </tr>
  </table>
  <br>
  <%}
  if(Mensaje.equals("La información ha sido ingresada exitosamente!")){%>
  <div align="center"> <span onClick="window.open('<%=BASEURL%>/jsp/equipos/ingreso_especial/ingreso_especialInsertar.jsp?c_tipo_acuerdo=<%=(String)request.getAttribute("tipo")%>&c_codigo_concepto=<%=(String)request.getAttribute("cod")%>','','HEIGHT=500,WIDTH=700,SCROLLBARS=NO,RESIZABLE=YES')" class="Simulacion_Hiper" style="cursor:hand">Agregar ingresos especiales</span></div>
  <%}%>
  </div>
  <%=datos[1]%>
</body>
</html>
<script>
function valido_porcentaje(){
	if(form1.c_porcentaje.value==""){
		alert("Debe llenar el porcentaje de descuento");
		form1.c_porcentaje.focus();
		return false;
	}
	else
		if( validarDecimales( form1.c_porcentaje, 3 ) )
			return true;
		else
			return false;
	}
function validarDecimales( valor , entero ){
	if( valor.value.indexOf('.') != -1 ){
		var porcentaje = valor.value.split(".");
		if(porcentaje[0].length > entero ){
			alert("El numero no puedes ser mayor a "+entero+" decimales");
			valor.focus();
			return false;
		}
		else{
			if(porcentaje[0].length == 0 ){
				valor.value = "0."+ porcentaje[1];
			}
			return true;
		}
	}
	else{	
		if(valor.value.length > entero ){
			alert("El numero no puedes ser mayor a "+entero+" decimales");
			valor.focus();
			return false;
		}
		else{
			return true;
		}
	}
}	
</script>