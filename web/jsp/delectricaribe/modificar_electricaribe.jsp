<!--
- Autor : Ing. Julio Ernesto Barros Rueda
- Date  : 14 de Mayo de 2007
- Copyrigth Notice : Fintravalores S.A. S.A

Descripcion : Pagina JSP, que maneja el ingreso de identidades
-->

<%-- Declaracion de librerias--%>
<%@page contentType="text/html;"%>
<%@page session="true"%>
<%@page import="java.util.*"%>
<%@page import="com.tsp.operation.model.*"%> 
<%@page import="com.tsp.operation.model.beans.*"%>
<%@page import="com.tsp.util.*"%>
<%@page import="com.tsp.util.Util.*"%>
<%@include file="/WEB-INF/InitModel.jsp"%>
<%@page import="com.tsp.operation.model.*"%>
<%@taglib uri="http://jakarta.apache.org/taglibs/input-0.90" prefix="input" %>

<html>
<%try{%>
    <head>
        <title>Datos Electricaribe</title>
        <link href="../../../css/estilostsp.css" rel="stylesheet" type="text/css"> 
        <link href="<%=BASEURL%>/css/estilostsp.css" rel="stylesheet" type="text/css">
		
    </head>
    
    <script type='text/javascript' src="<%=BASEURL%>/js/Validacion.js"></script>
    <script type='text/javascript' src="<%= BASEURL %>/js/boton.js"></script>
    <script type='text/javascript' src="<%= BASEURL %>/js/general.js"></script>
    <script src='<%=BASEURL%>/js/tools.js' language='javascript'></script>
    <script src='<%=BASEURL%>/js/date-picker.js'></script>
    <script src="<%= BASEURL %>/js/transferencias.js"></script>
    
    <%
    Usuario usuario = (Usuario) session.getAttribute("Usuario");
    String fechainicio=request.getParameter("fechainicio");
    String Retorno=request.getParameter("respuesta");
	
    //System.out.println("Opcionn:"+request.getParameter("opcion"));
    if(request.getParameter("opcion")!=null && request.getParameter("opcion").equals("nuevo"))
    {   model.electricaribeVerSvc.reset();
    }
    

    if (fechainicio==null){
        fechainicio=""+com.tsp.util.Util.AnoActual()+"-"+com.tsp.util.Util.getFechaActual_String(3)+"-"+com.tsp.util.Util.getFechaActual_String(5);
    }
    %>
    
    <script>
	
		function calcularPorcsA(frm){
			var mate=frm.valor_materiales.value;
			var mo=frm.valor_mano_obra.value;
			var vo=frm.valor_otros.value;
			//alert("mira"+frm.aiu_admin_porc.value);
			//alert("mira2"+formulario.aiu_admin_porc.value);
			var	porca=frm.aiu_admin_porc.value;
			porca			=parseFloat(porca);
			//if (porca==""){porca="0";}
			var tot=parseFloat(mate)+parseFloat(mo)+parseFloat(vo);
			//alert(tot+"--"+porca);
			frm.aiu_administracion.value=Math.round(tot*porca)/100;
			if (frm.aiu_administracion.value=="NaN"){frm.aiu_administracion.value="0";}
		}
		function calcularPorcsI(frm){
			var mate=frm.valor_materiales.value;			
			var mo=frm.valor_mano_obra.value;
			var vo=frm.valor_otros.value;
			var	porci=parseFloat(frm.aiu_imprevistos_porc.value);
			var tot=parseFloat(mate)+parseFloat(mo)+parseFloat(vo);			
			frm.aiu_imprevistos.value=Math.round(tot*porci)/100;
			if (frm.aiu_imprevistos.value=="NaN"){frm.aiu_imprevistos.value="0";}
		}
		function calcularPorcsU(frm){
			var mate=frm.valor_materiales.value;			
			var mo=frm.valor_mano_obra.value;
			var vo=frm.valor_otros.value;
			var	porcu=parseFloat(frm.utilidades_porc.value);						
			var tot=parseFloat(mate)+parseFloat(mo)+parseFloat(vo);
			frm.utilidades.value=Math.round(tot*porcu)/100;			
			if (frm.utilidades.value=="NaN"){frm.utilidades.value="0";}
		}
		function calcularPorcsAIU(frm){
			if (frm.valor_materiales.value==""){frm.valor_materiales.value="1";}
			if (frm.valor_mano_obra.value==""){frm.valor_mano_obra.value="0";}			
			if (frm.valor_otros.value==""){frm.valor_otros.value="0";}			
			if (frm.aiu_administracion.value==""){frm.aiu_administracion.value="0";}			
			if (frm.aiu_imprevistos.value==""){frm.aiu_imprevistos.value="0";}			
			if (frm.utilidades.value==""){frm.utilidades.value="0";}		
			
			var mate=frm.valor_materiales.value;			
			var mo=frm.valor_mano_obra.value;
			var vo=frm.valor_otros.value;
			var	porca=parseFloat(frm.aiu_admin_porc.value);			
			var	porci=parseFloat(frm.aiu_imprevistos_porc.value);
			var	porcu=parseFloat(frm.utilidades_porc.value);						
			var tot=parseFloat(mate)+parseFloat(mo)+parseFloat(vo);
			frm.utilidades.value=Math.round(tot*porcu)/100;			
			frm.aiu_imprevistos.value=Math.round(tot*porci)/100;
			frm.aiu_administracion.value=Math.round(tot*porca)/100;
			if (frm.aiu_administracion.value=="NaN"){frm.aiu_administracion.value="0";}
			if (frm.aiu_imprevistos.value=="NaN"){frm.aiu_imprevistos.value="0";}
			if (frm.utilidades.value=="NaN"){frm.utilidades.value="0";}
			
		}
	
        function enviarFormularioParaAgregar(CONTROLLER,frm){	
			if (frm.valor_materiales.value==""){frm.valor_materiales.value="1";}
			if (frm.valor_mano_obra.value==""){frm.valor_mano_obra.value="0";}			
			if (frm.valor_otros.value==""){frm.valor_otros.value="0";}			
			if (frm.aiu_administracion.value==""){frm.aiu_administracion.value="0";}			
			if (frm.aiu_imprevistos.value==""){frm.aiu_imprevistos.value="0";}			
			if (frm.utilidades.value==""){frm.utilidades.value="0";}			
			//alert("a");
			if (validar(frm)==true){
				frm.action='<%=CONTROLLER%>?estado=Electricaribe&accion=Ver&opcion=temporall';
				frm.submit();
			}else{
				alert("Por favor revise los datos.");
			}
			
        }
        
        function enviarFormularioX(CONTROLLER,frm){	
			frm.action='<%=CONTROLLER%>?estado=Electricaribe&accion=Ver&opcion=commitee';
			frm.submit();
        }    

        function eliminarIndex(CONTROLLER,frm){	
			frm.action='<%=CONTROLLER%>?estado=Electricaribe&accion=Ver&opcion=delete';
			frm.submit();
        }    
        
		function consultarIndex(CONTROLLER,frm){	
			frm.action='<%=CONTROLLER%>?estado=Electricaribe&accion=Ver&opcion=consultarx';
			frm.submit();
        } 
		
        function asignar_valores(CONTROLLER,frm){	
			frm.action='<%=CONTROLLER%>?estado=Electricaribe&accion=Ver&opcion=charge';
			frm.submit();
        }    

        function cancelarCompraCartera(frm){
            frm.action='<%=CONTROLLER%>?estado=Electricaribe&accion=Ver&opcion=cancelarr';
            frm.submit();
        }
        
        function consultar(frm){
            frm.action='<%=CONTROLLER%>?estado=creacion&accion=CompraCartera&opcion=consultar';
            frm.submit();
        }
        
		function obtenercheque(ite,frm){
            frm.action='<%=CONTROLLER%>?estado=creacion&accion=CompraCartera&opcion=obtenercheque&itemobtenible='+ite;
            frm.submit();
        }
		
		function obtenerfechaconsig(frm){
			frm.action='<%=CONTROLLER%>?estado=creacion&accion=CompraCartera&opcion=obtenerfechaconsig';
            frm.submit();
        }
		
		function modificar(frm){
            frm.action='<%=CONTROLLER%>?estado=creacion&accion=CompraCartera&opcion=modificar';
            frm.submit();
        }
				
		function validar(frm){
			var respuesta=true;
			if (frm.id_contratista.value=="--"){respuesta=false;}
			return respuesta;
		}		
				
				
    </script>
    
    <body >
        
        <div id="capaSuperior" style="position:absolute; width:100%; height:100px; z-index:0; left: 0px; top: 0px;">
            <jsp:include page="/toptsp.jsp?encabezado=DATOS ELECTRICARIBE"/>
        </div>
        <div id="capaCentral" style="position:absolute; width:100%; height:87%; z-index:0; left: 0px; top: 100px; overflow: scroll;">
            
            <FORM name='formulario' method="post" >
			               <!-- <input  type="text" name ="num_os_old" value="<%//=model.electricaribeVerSvc.getOferta().getNumOs()%>">-->
                <input  type="hidden" name ="num_os_old" value="<%=model.electricaribeVerSvc.getOferta().getNumOs()%>">
                <table width="432" height="167" border="2"align="center">
                    
                    <tr>
                        <td width="420" height="159">
                            <table width="100%" height="72%" class="tablaInferior" >
                                <tr class="fila">
                                    <td colspan="2" align="left" nowrap class="subtitulo">&nbsp;Datos Electricaribe</td>
                                    <td colspan="2" align="left" nowrap class="bordereporte"> </td>
                                </tr>
                                <%  TreeMap prem= model.electricaribeVerSvc.getPreMultiservicios();
                                %> 
                                <tr class="fila">
                                    <td colspan="2" >Escoja el Multiservicio</td>
                                    <td class="fila">
   									    <input name="textbox" type="text" class="textbox" id="campo" style="width:200;" onKeyUp="buscar(document.formulario.multiservicios,this)" size="15" >
                                        <input:select  name="multiservicios" attributesText="class=textbox" options="<%=prem%>"/>
                                        <img src='<%=BASEURL%>/images/botones/iconos/lupa.gif' width="15" height="15" onClick="asignar_valores('<%=CONTROLLER%>',formulario);" title="Buscar" style="cursor:hand" >
                                    </td>
                                </tr>
                                <tr class="fila">
                                    <td colspan="2" >Cliente</td>
                                    <td class="fila">
                                        <%  if (model.electricaribeVerSvc.getOferta().getIdCliente()==null){
                                                TreeMap cli= model.electricaribeVerSvc.getClientes();
                                        %> 
                                                <input name="textbox2" type="text" class="textbox" id="campo" style="width:200;" onKeyUp="buscar(document.formulario.cliente,this)" size="15"  >
                                                <input:select  name="cliente" attributesText="class=textbox" options="<%=cli%>"  />												                                            
                                        <%  }else{%>
                                                <input name="cliente" type="text" class="textbox" id="cliente" style="width:200;" size="15" readonly                             
                                                 value='<%=model.electricaribeVerSvc.getOferta().getNombreCliente()+"_"+model.electricaribeVerSvc.getOferta().getIdCliente()%>' >
                                        <%  }%>
                                        
                                    </td>
                                </tr>
                                
                                <tr class="fila">
                                    <td colspan="2" >Cantidad de Cuotas</td>
                                    <td class="fila">
                                        
                                        <%if (model.electricaribeVerSvc.getOferta().getCuotas()==null){%> 
                                            <input name="cuota" type="text" class="textbox" id="cuota" style="width:200;" size="15" >                            
                                        <%}else{%>
                                            <input name="cuota" type="text" class="textbox" id="cuota" style="width:200;" size="15" readonly                             
                                             value='<%=model.electricaribeVerSvc.getOferta().getCuotas()%>' >
                                        <%}%>
                                        
                                    </td>
                                </tr>
                                
                                <tr class="fila">
                                    <td colspan="2" >Observacion</td>
                                    <td class="fila">
                                        
                                        <%if (model.electricaribeVerSvc.getOferta().getDetalleInconsistencia()==null){%> 
                                            <textarea name="detalle_inconsistencia"  id="detalle_inconsistencia" rows="6" cols="50"></textarea>                        
                                        <%}else{%>
                                            <textarea name="detalle_inconsistencia"  rows="6" cols="50" type="text" class="textbox" id="detalle_inconsistencia" readonly                             
                                             value='<%=model.electricaribeVerSvc.getOferta().getDetalleInconsistencia()%>' ><%=model.electricaribeVerSvc.getOferta().getDetalleInconsistencia()%></textarea>
                                        <%}%>
                                        
                                    </td>
                                </tr>

                                
                                <tr class="fila">
                                    <td colspan="2" >Fecha Oferta</td>
                                    <td valign="middle">
                                            <%if (model.electricaribeVerSvc.getOferta().getFechaOferta()==null){%> 
                                            <input name='fecha_oferta' type='text' class="textbox" id="fecha_oferta" style='width:120' value='<%=fechainicio%>' readonly>
                                            <a href="javascript:void(0);" class="link" onFocus="if(self.gfPop)gfPop.fPopCalendar(document.formulario.fecha_oferta);return false;"  HIDEFOCUS > 
                                                <img src="<%=BASEURL%>\js\Calendario\cal.gif" width="16" height="16"
                                                    border="0" alt="De click aqu&iacute; para ver el calendario.">
                                            </a> 
                                            <%}
                                            else
                                            {%>   
                                            <input name='fecha_oferta' type='text' class="textbox" id="fecha_oferta" style='width:120' value='<%=model.electricaribeVerSvc.getOferta().getFechaOferta().substring(0,10)%>' readonly>
                                            <%}%>
                                    </td> 
                                </tr>   
                             

                                <tr class="fila">
                                    <td colspan="2" >Estudio Economico</td>
                                    <td class="fila">
                                        
                                        <%if (model.electricaribeVerSvc.getOferta().getEstudioEconomico()==null){%> 
                                            <select  name="estudio_economico"  id="estudio_economico"  >
                                            <option value='Consorcio ECA-Applus-Fintravalores'>Consorcio ECA-Applus-Fintravalores</option>
                                            <option value='Applus Norcontrol'>Applus Norcontrol</option>
                                            </select>
                                                
                                        <%}else{%>
                                            <input name="estudio_economico" type="text" class="textbox" id="estudio_economico" style="width:200;" size="15" readonly                             
                                             value='<%=model.electricaribeVerSvc.getOferta().getEstudioEconomico()%>' >
                                        <%}%>
                                        
                                    </td>
                                </tr>                                
                                
                                <tr class="fila">
                                    <td colspan="2" >Esquema Comision</td>
                                    <td class="fila">
                                        
                                        <%if (model.electricaribeVerSvc.getOferta().getEsquemaComision()==null){%> 
                                            <select name="esquema_comision"  id="esquema_economico"  >
                                            <option value='MODELO_NUEVO'>MODELO NUEVO</option>
                                            <option value='MODELO_ANTERIOR'>MODELO ANTERIOR</option>
                                            </select>
                                        <%}else{%>
                                            <input name="esquema_comision" type="text" class="textbox" id="esquema_comision" style="width:200;" size="15" readonly                             
                                             value='<%=model.electricaribeVerSvc.getOferta().getEsquemaComision()%>' >
                                        <%}%>
                                        
                                    </td>
                                </tr>     
								
								<tr class="fila">
                                    <td colspan="2" >Nuevo Num Os</td>
                                    <td class="fila">
                                        
                                        <%if (model.electricaribeVerSvc.getOferta().getEsquemaComision()==null){%> 
                                            <input type="text" readonly="">
                                        <%}else{
												String new_num_os=request.getParameter("new_num_os");
												if (new_num_os==null){new_num_os="";}
										%>
                                            <input name="new_num_os" type="text" class="textbox" id="new_num_os" style="width:200;" size="15" value='<%=new_num_os%>' >
                                        <%}%>
                                        
                                    </td>
                                </tr>     
                           
                                  <!--                              
                                <tr class="fila">
                                    <td colspan="2" >Consecutivo</td>
                                    <td class="fila">
                                        
                                        <%/*if (model.electricaribeVerSvc.getOferta().getConsecutivo()==null){%> 
                                            <input name="consecutivo" type="text" class="textbox" id="consecutivo" style="width:200;" size="15" >                            
                                        <%}else{%>
                                            <input name="consecutivo" type="text" class="textbox" id="consecutivo" style="width:200;" size="15" readonly                             
                                             value='<%=model.electricaribeVerSvc.getOferta().getConsecutivo()%>' >
                                        <%}*/%>
                                        
                                    </td>
                                </tr> -->    
                                                                
<%--------------------------------------------------------------------------------------------------------------------------------------------------------%>
                                
                                <tr class="fila">
                                    <td colspan="3" bgcolor='009900' > </td>
                                </tr>
                                <tr class="fila">
                                    <td colspan="2" > Item</td>
                                    <%  
									int itemobtenible =model.electricaribeVerSvc.getAccords().size()+1;
									if	(itemobtenible==2) { itemobtenible=1;}
									String itemx=request.getParameter("itemx");
									String contratist="",actions="",materials="",hand="",others="",aa="",ii="",uu="";			
									ArrayList accords=model.electricaribeVerSvc.getAccords();
									if (itemx!=null){	
										itemobtenible=Integer.parseInt(itemx)-1;
										contratist=((Accord)accords.get(itemobtenible)).getNombreContratista() +"_"+((Accord)accords.get(itemobtenible)).getIdContratista();
										actions=((Accord)accords.get(itemobtenible)).getAcciones();
										materials=((Accord)accords.get(itemobtenible)).getValorMateriales();
										hand=((Accord)accords.get(itemobtenible)).getValorManoObra();
										others=((Accord)accords.get(itemobtenible)).getValorOtros();
										aa=((Accord)accords.get(itemobtenible)).getAdministracion();
										ii=((Accord)accords.get(itemobtenible)).getImprevistos();
										uu=((Accord)accords.get(itemobtenible)).getUtilidad();
										itemobtenible=itemobtenible+1;
									}
                                    %>			
                                    <td width="49%" valign="middle">
                                        <input value="<%=itemobtenible%>" name="item" type="text" class="textbox" value='' maxlength="6" onKeyPress="soloDigitos(event,'decNO')">
                                        <span class="style2"></span>
										<img src='<%=BASEURL%>/images/botones/iconos/lupa.gif' width="15" height="15" onClick="consultarIndex('<%=CONTROLLER%>',formulario);" title="Buscar" style="cursor:hand" >
										&nbsp;&nbsp;
                                        <a href="javascript: void ;" class="Simulacion_Hiper" onClick= "javascript:eliminarIndex('<%=CONTROLLER%>',formulario); "  title="Eliminar" >Eliminar Item</a> 										
										
                                    </td>
                                </tr>
                                
                                <tr class="fila">
                                    <td colspan="2" > Contratista</td>
                                    <td width="49%" valign="middle">
                                    <select name="id_contratista">
									<option value='--'>--</option>
									<%if (!(contratist.equals(""))){%>
										<option value='<%=contratist%>'><%=contratist%></option>
									<%}%>
                                    <option value='SENTEL_CC002'>SENTEL_CC002</option>
                                    <option value='TRACOL_CC008'>TRACOL_CC008</option>
                                    <option value='NORCONTROL_CC016'>NORCONTROL_CC016</option>
                                    <option value='QIELEC_CC017'>QIELEC_CC017</option>
                                    <option value='ELECTRICARIBE_CC027'>ELECTRICARIBE_CC027</option>
                                    <option value='TRADELCA_CC036'>TRADELCA_CC036</option>
                                    <option value='DELELCO_CC038'>DELELCO_CC038</option>
                                    </select>
                                        <span class="style2"></span>
                                    </td>
                                </tr>
                                
                                <tr class="fila">
                                    <td colspan="2" > Acciones</td>
                                    <td width="49%" valign="middle">
                                        <textarea name="acciones" rows="5" cols="35"><%=actions%></textarea>
                                        <span class="style2"></span>
                                    </td>
                                </tr>                                
                                
                                <tr class="fila">
                                    <td colspan="2" > Valor Materiales</td>
                                    <td width="49%" valign="middle"><input name="valor_materiales" type="text" class="textbox" value='<%=materials%>' maxlength="6" onKeyPress="soloDigitos(event,'decNO')" onChange="calcularPorcsAIU(formulario)">
                                        <span class="style2"></span>&nbsp;
                                    </td>
                                </tr>    
                                
                                <tr class="fila">
                                    <td colspan="2" > Valor Mano de Obra</td>
                                    <td width="49%" valign="middle"><input name="valor_mano_obra" type="text" class="textbox" value='<%=hand%>' maxlength="6" onKeyPress="soloDigitos(event,'decNO')" onChange="calcularPorcsAIU(formulario)">
                                        <span class="style2"></span>
                                    </td>
                                </tr>    

                                <tr class="fila">
                                    <td colspan="2" > Valor Otros</td>
                                    <td width="49%" valign="middle"><input name="valor_otros" type="text" class="textbox" value='<%=others%>' maxlength="6" onKeyPress="soloDigitos(event,'decNO')"  onChange="calcularPorcsAIU(formulario)">
                                        <span class="style2"></span>
                                    </td>
                                </tr>    
                                
                                <!--tr class="fila">
                                    <td colspan="2" > Simbolo Variable</td>
                                    <td width="49%" valign="middle"><input name="simbolo_variable" type="text" class="textbox" value='' maxlength="6" >
                                        <span class="style2"></span>
                                    </td>
                                </tr>  
                                
                                
                                <tr class="fila">
                                    <td colspan="2" > Factura Conformada</td>
                                    <td width="49%" valign="middle"><input name="fact_conformada" type="text" class="textbox" value='' maxlength="6" >
                                        <span class="style2"></span>
                                    </td>
                                </tr>  -->
                                
                                
                                <tr class="fila">
                                    <td colspan="2" > Administracion</td>
                                    <td width="49%" valign="middle">
										<input name="aiu_admin_porc" type="text" class="textbox"  maxlength="6"  onChange="calcularPorcsA(formulario)">-->
										<input name="aiu_administracion" type="text" class="textbox" value='<%=aa%>' maxlength="6" >
                                        <span class="style2"></span>
                                    </td>
                                </tr>  
                                
                                <tr class="fila">
                                    <td colspan="2" > Imprevistos</td>
                                    <td width="49%" valign="middle">
										<input name="aiu_imprevistos_porc" type="text" class="textbox"  maxlength="6"  onChange="calcularPorcsI(formulario)">-->
										<input name="aiu_imprevistos" type="text" class="textbox" value='<%=ii%>' maxlength="6" >
                                        <span class="style2"></span>
                                    </td>
                                </tr>                                  
                                
                                <tr class="fila">
                                    <td colspan="2" > Utilidades</td>
                                    <td width="49%" valign="middle">
										<input name="utilidades_porc" type="text" class="textbox" maxlength="6"  onChange="calcularPorcsU(formulario)">-->
										<input name="utilidades" type="text" class="textbox" value='<%=uu%>' maxlength="6" onKeyPress="soloDigitos(event,'decNO')">
                                        <span class="style2"></span>
                                    </td>
                                </tr>
                                  
                            </table>
                        </td>
                    </tr>
                </table>
                <br>
                <div align="center">
                    <img src="<%=BASEURL%>/images/botones/agregar.gif"  name="imgaceptar" onClick="javascript:enviarFormularioParaAgregar('<%=CONTROLLER%>',formulario);" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" style="cursor:hand">
                </div>
                
                <br>
                <table id="detalle" width="100%" >
                    <tr  id="fila1" class="tblTitulo">
                        <td align="center">Item  </td>
                        <td align="center">Contratista </td>
                        <td align="center">Acciones</td>                                   
                        <td align="center">Valor Materiales</td>
                        <td align="center">Valor Mano de Obra</td>
                        <td align="center">Valor Otros</td>
                        <!--td align="center">Simbolo Variable</td>                        
                        <td align="center">Factura Conformada</td>-->
                        <td align="center">Administracion</td>
                        <td align="center">Imprevistos</td>
                        <td align="center">Utilidades</td>
                    </tr>				
                    <% 
                    
                    if(accords!=null)
                    {   Accord acc ;
                        //System.out.println("El tama�o de el array es :"+accords.size());
                        for(int i = 0; i < accords.size(); i++ ){
                            acc=(Accord)accords.get(i);
                        %>	
                    <tr class="<%= (i%2==0)? "filagris" : "filaazul" %> style="cursor:default" bordercolor="#D1DCEB" align="center"> 					   
                        <td align="center" > <%=i+1%></a></td>
                        <td align="center"   ><%=acc.getNombreContratista()+"_"+acc.getIdContratista()%></td>
                        <td align="center" ><%=acc.getAcciones()%></td>
                        <td><%=acc.getValorMateriales()%></td>
                        <td><%=acc.getValorManoObra()%></td>
                        <td><%=acc.getValorOtros()%></td>
                        <!--td align="center" ><%//=acc.getSimboloVariable()%></td>
                        <td align="center" ><%//=acc.getFactConformada()%></td-->
                        <td align="center" ><%=acc.getAdministracion()%></td>
                        <td align="center" ><%=acc.getImprevistos()%></td>
                        <td align="center" ><%=acc.getUtilidad()%></td>
                    </tr>
                        <%}

                    }%>
                </table>
                <br>
                <div align="center">
                    <img src="<%=BASEURL%>/images/botones/aceptar.gif"  name="imgaceptar" onClick="javascript:enviarFormularioX('<%=CONTROLLER%>',formulario);" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" style="cursor:hand">&nbsp;       &nbsp;
                    <img src="<%=BASEURL%>/images/botones/salir.gif"  name="imgsalir"  onMouseOver="botonOver(this);" onClick="parent.close();" onMouseOut="botonOut(this);" style="cursor:hand" >&nbsp;       &nbsp;
                    <img src="<%=BASEURL%>/images/botones/cancelar.gif"  name="imgcancelar"  onMouseOver="botonOver(this);" onClick="javascript:cancelarCompraCartera(formulario);" onMouseOut="botonOut(this);" style="cursor:hand">&nbsp;       &nbsp;
                    <%//<img src="<%=BASEURL%_>/images/botones/imprimir.gif"  name="imgimprimir"  onMouseOver="botonOver(this);" onClick="javascript:imprimir();" onMouseOut="botonOut(this);" style="cursor:hand">%>
                </div>
                <br>
                <%  String mensaje="";
                if( Retorno != null && !(Retorno.equals(""))){%>
                <table border="2" align="center">
                    <tr>
                        <td>
                            <table width="100%"  border="1" align="center"  bordercolor="#F7F5F4" bgcolor="#FFFFFF">
                                <tr>
                                    <td width="229" align="center" class="mensajes"><%=Retorno%></td>
                                    <td width="29" background="<%=BASEURL%>/images/cuadronaranja.JPG">&nbsp;</td>
                                    <td width="58">&nbsp;</td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
                <%}%>
				
				<br>				<br>
				
				<!--
		  <input name="consultablex" type="text" class="textbox" id="campo" style="width:200;" size="15" maxlength="15"   >
		  
          <img src="<%//=BASEURL%>/images/botones/buscar.gif"  name="imgbuscar"  
            onMouseOver="botonOver(this);" 
            onClick="javascript:consultar(formulario);" 
            onMouseOut="botonOut(this);" style="cursor:hand" >

		  <img src="<%//=BASEURL%>/images/botones/modificar.gif"  name="imgmodificar"  
            onMouseOver="botonOver(this);" 
            onClick="javascript:modificar(formulario);" 
            onMouseOut="botonOut(this);" style="cursor:hand" >
			-->
                            
                <br>
            </form>
            
        </div>
       
        <iframe width="188" height="166" name="gToday:normal:agenda.js:gfPop:plugins.js" id="gToday:normal:agenda.js:gfPop:plugins_24.js" src="<%=BASEURL%>/js/Calendario/ipopeng.htm" scrolling="yes" frameborder="0" style="visibility:visible; z-index:999; position:absolute; left:-500px; top:0px;">
        </iframe>
		
    </body>
<%}catch(Exception e){
	System.out.println("en jsp error:"+e.toString());
}%>	
</html>


