<!--
- Autor : Ing. Julio Ernesto Barros Rueda
- Date  : 14 de Mayo de 2007
- Copyrigth Notice : Fintravalores S.A. S.A

Descripcion : Pagina JSP, que maneja el ingreso de identidades
-->

<%-- Declaracion de librerias--%>
<%@page contentType="text/html;"%>
<%@page session="true"%>
<%@page import="java.util.*"%>
<%@page import="com.tsp.operation.model.*"%>
<%@page import="com.tsp.operation.model.beans.*"%>
<%@page import="com.tsp.operation.model.services.*"%>
<%@page import="com.tsp.util.*"%>
<%@page import="com.tsp.util.Util.*"%>
<%@include file="/WEB-INF/InitModel.jsp"%>
<%@page import="com.tsp.operation.model.*"%>
<%@taglib uri="http://jakarta.apache.org/taglibs/input-0.90" prefix="input" %>

<html>
    <head>
        <title>Datos Cliente Electricaribe</title>
        <link href="../../../css/estilostsp.css" rel="stylesheet" type="text/css">
        <link href="<%=BASEURL%>/css/estilostsp.css" rel="stylesheet" type="text/css">
    </head>

    <script type="text/javascript" src="<%=BASEURL%>/js/Validacion.js"></script>
    <script type="text/javascript" src="<%= BASEURL%>/js/boton.js"></script>
    <script type="text/javascript" src="<%= BASEURL%>/js/general.js"></script>
    <script src="<%=BASEURL%>/js/tools.js" language="javascript"></script>
    <script src="<%=BASEURL%>/js/date-picker.js"></script>
    <script src="<%= BASEURL%>/js/transferencias.js"></script>
    <script type="text/javascript">
        function function1() {
            //alert('si funciona');
            var sw=0;
            for(var i=0;i<document.getElementById("nics").options.length;i++){   
                if(document.getElementById("nic").value==document.getElementById("nics").options[i].value){
                    sw=1;
                }
            }

            if(document.getElementById("nic").value!=""&&sw==0){
                var optn = document.createElement("OPTION");
                optn.text = document.getElementById("nic").value;
                optn.value = document.getElementById("nic").value;
                document.getElementById("nics").options.add(optn);

                document.getElementById("niccs").innerHTML=document.getElementById("niccs").innerHTML+"<input name='nicc' type='hidden' id='nicc' value='"+document.getElementById("nic").value+"'>";
                document.getElementById("nic").value="";
            }
            else{
                if(sw==1 && document.getElementById("nic").value!=""){
                    alert("Ese Nic ya existe en la lista");
                }
                else{
                    alert("Nic invalido");
                }
            }
        }

        function submitt(form){
            var sw=0;
            for(var i=0;i<form.length;i++)
            {   if(((form.elements[i].type=='text' ||form.elements[i].type=='select') && form.elements[i].value=='')&&form.elements[i].id!='nic'&&form.elements[i].id!='nics'&&form.elements[i].id!='nic'&&form.elements[i].id!='client'&&form.elements[i].id!='idsolicitud')
                {   sw=1;
                }
            }
            if(!(document.getElementById('tipo_identificacion').value!=''&&document.getElementById('zona').value!=''&&document.getElementById('ciudad').value!=''&&document.getElementById('sector').value!=''&&document.getElementById('ejecutivo_cta').value!=''))
            {   sw=1;
            }
            if(sw==0)
            {   form.submit();
            }
            else
            {   alert('Porfavor llene todos los campos requeridos para la creacion de un cliente');
            }

        }
        function submitt2(form){
            var sw=0;
            for(var i=0;i<form.length;i++)
            {   if(((form.elements[i].type=='text' ||form.elements[i].type=='select') && form.elements[i].value=='')&&form.elements[i].id!='nic'&&form.elements[i].id!='nics'&&form.elements[i].id!='nic'&&form.elements[i].id!='client'&&form.elements[i].id!='idsolicitud')
                {   sw=1;
                }
            }
            if(sw==0)
            {   form.submit();
            }
            else
            {   alert('Porfavor llene todos los campos requeridos para la modificacion de un cliente');
            }

        }

        function submittt(form){
            var sw2=0;

            if(!(document.getElementById('detalle_inconsistencia').value!=''&&document.getElementById('client').value!=''&&document.getElementById('nicsoff').value!=''&&(document.getElementById('tipo_solicitud').value!='' || document.getElementById('tipo_solicitudd').value!='')))
            {   sw2=1;
            }
            if(sw2==0)
            {  form.submit();
            }
            else
            {   alert('Por favor llene todos los campos requeridos');
            }

        }

        function submitttt(form){
            var sw2=0;

            if(!(document.getElementById('descripcion').value!=''&&document.getElementById('contratista').value!=''))
            {
                sw2=1;
            }

            if(sw2==0)
            {
                form.submit();
            }
            else
            {
                alert('Porfavor llene todos los campos requeridos para la modificacion de una solicitud');
            }

        }

    </script>

    <%
        String[] nics = (String[]) session.getAttribute("nics");
        session.removeAttribute("nics");
        Usuario usuario = (Usuario) session.getAttribute("Usuario");
        String Retorno = request.getParameter("mensaje");
        String cli_creation_user = request.getParameter("creation_user");
        String sol_creation_user = request.getParameter("sol_creation_user");
        ClientesVerService clvsrv = new ClientesVerService();
        String perfil= clvsrv.getPerfil(usuario.getLogin());
        ArrayList zona = clvsrv.getDatos("zona");
        ArrayList ciudad = clvsrv.getDatos("ciudad");
        ArrayList sector = clvsrv.getDatos("sector");
        ArrayList contratistas = clvsrv.getContratistas();
        ArrayList padres = clvsrv.getPadres();
        ArrayList estados = clvsrv.getEstados();
        ArrayList ejecutivo_cta = clvsrv.getEjecutivos();
        ArrayList acciones = (ArrayList) session.getAttribute("acciones");
        session.removeAttribute("acciones");
        String fechainicio = request.getParameter("fechainicio");
        String padre = request.getParameter("padre");
        String idusuario = request.getParameter("idusuario");
        String idsolicitud = request.getParameter("idsolicitud");
        String login_ejecutivo = request.getParameter("login_ejecutivo");
        System.out.println("login_ejecutivo:"+login_ejecutivo+"cli_creation_user"+sol_creation_user);
        System.out.println("perfil!!!:"+perfil+idusuario+idsolicitud);
        
        if (fechainicio == null) {
            fechainicio = "" + com.tsp.util.Util.AnoActual() + "-" + com.tsp.util.Util.getFechaActual_String(3) + "-" + com.tsp.util.Util.getFechaActual_String(5);
        }
    %>
    <body>
        <div id="capaSuperior" style="position:absolute; width:100%; height:100px; z-index:0; left: 0px; top: 0px;">
            <jsp:include page="/toptsp.jsp?encabezado=DATOS CLIENTE/SOLICITUD ELECTRICARIBE"/>
        </div>
        <div id="capaCentral" align="center" style="position:absolute; width:100%; height:87%; z-index:0; left: 0px; top: 100px; overflow: scroll;">

            <FORM name='formulario' method="post" action="<%=CONTROLLER%>?estado=Clientes&accion=Ver">
                <%/* try {*/%>
                <table width="432" height="159" border="2"align="center">
                    <tr><td colspan="2">
                            <table width="100%" height="72%" class="tablaInferior" >
                                <tr class="fila">
                                    <td colspan="2" align="left" nowrap class="subtitulo">&nbsp;Datos Cliente Electricaribe</td>
                                    <td colspan="2" align="left" nowrap class="bordereporte"> </td>
                                </tr>

                                <tr class="fila">
                                    <td colspan="2" >Nombre</td>
                                    <td class="fila">
                                        <input name="nombre" type="text" class="textbox" id="nombre" style="width:170;" size="15" maxlength="60"  value="<%=(request.getParameter("nombre") != null) ? request.getParameter("nombre") : ""%>">
                                    </td>
                                </tr>

                                <tr class="fila">
                                    <td colspan="2" >Nics</td>
                                    <td class="fila">
                                        <select name='nics' multiple id="nics">
                                            <%for (int i = 0; nics != null && i < nics.length; i++) {%>
                                            <option value="<%=nics[i]%>"><%=nics[i]%></option>
                                            <%}%>
                                        </select>
                                        <input name="nic" type="text" class="textbox" id="nic" style="width:140;" size="15" maxlength="15" >
                                        <img src='<%=BASEURL%>/images/botones/iconos/mas.gif' width ="12" height="12" onClick="formulario.target='';function1();" title="Agregar" style="cursor:hand" >
                                        <div id="niccs"></div>
                                    </td>
                                </tr>

                                <tr class="fila">
                                    <td colspan="2" >NIT</td>
                                    <td class="fila">
                                        <input name="nit" type="text" class="textbox" id="nit" style="width:170;" size="15" maxlength="15" value="<%=(request.getParameter("nit") != null) ? request.getParameter("nit") : ""%>">
                                    </td>
                                </tr>

                                <tr class="fila">
                                    <td colspan="2" >Tipo de Cliente</td>
                                    <td class="fila">
                                        <%if (idusuario != null) {%> <input name="tipo_identificacionn" type="text" class="textbox" id="tipo_identificacionn" style="width:100;" size="15" maxlength="15" value="<%=(request.getParameter("tipo_identificacionn") != null) ? request.getParameter("tipo_identificacionn") : ""%>" readonly >
                                        <%}%>
                                        <select name='tipo_identificacion' id="tipo_identificacion">
                                            <option value=''>...</option>
                                            <option value='R'>R</option>
                                            <option value='NR'>NR</option>
                                        </select>
                                    </td>
                                </tr>

                                <tr class="fila">
                                    <td colspan="2" >Distrito</td>
                                    <td class="fila">
                                        <%if (idusuario != null) {%> <input name="zonaa" type="text" class="textbox" id="zonaa" style="width:100;" size="15" maxlength="15"  value="<%=(request.getParameter("zonaa") != null) ? request.getParameter("zonaa") : ""%>" readonly >
                                        <%}%>
                                        <select name='zona' id="zona">
                                            <option value="">...</option>
                                            <%for (int i = 0; i < zona.size(); i++) {%>
                                            <option value="<%=zona.get(i)%>"><%=zona.get(i)%></option>
                                            <%}%>
                                        </select>
                                    </td>
                                </tr>

                                <tr class="fila">
                                    <td colspan="2" >Ciudad</td>
                                    <td class="fila">
                                        <%if (idusuario != null) {%> <input name="ciudadd" type="text" class="textbox" id="ciudadd" style="width:170;" size="15" maxlength="15"  value="<%=(request.getParameter("ciudadd") != null) ? request.getParameter("ciudadd") : ""%>" readonly >
                                        <%}%>
                                        <select name='ciudad' id="ciudad">
                                            <option value="">...</option>
                                            <%for (int i = 0; i < ciudad.size(); i++) {%>
                                            <option value="<%=ciudad.get(i)%>"><%=ciudad.get(i)%></option>
                                            <%}%>
                                        </select>
                                    </td>
                                </tr>

                                <tr class="fila">
                                    <td colspan="2" >Departamento</td>
                                    <td class="fila">
                                        <%if (idusuario != null) {%> <input name="sectorr" type="text" class="textbox" id="sectorr" style="width:100;" size="15" maxlength="15" value="<%=(request.getParameter("sectorr") != null) ? request.getParameter("sectorr") : ""%>" readonly >
                                        <%}%>
                                        <select name='sector' id="sector">
                                            <option value="">...</option>
                                            <%for (int i = 0; i < sector.size(); i++) {%>
                                            <option value="<%=sector.get(i)%>"><%=sector.get(i)%></option>
                                            <%}%>
                                        </select>
                                    </td>
                                </tr>

                                <tr class="fila">
                                    <td colspan="2" >Direccion</td>
                                    <td class="fila">
                                        <input name="direccion" type="text" class="textbox" id="direccion" style="width:170;" size="15" maxlength="60" value="<%=(request.getParameter("direccion") != null) ? request.getParameter("direccion") : ""%>">
                                    </td>
                                </tr>

                                <tr class="fila">
                                    <td colspan="2" >Nombre Contacto</td>
                                    <td class="fila">
                                        <input name="nombre_contacto" type="text" class="textbox" id="nombre_contacto" style="width:170;" size="15" maxlength="60" value="<%=(request.getParameter("nombre_contacto") != null) ? request.getParameter("nombre_contacto") : ""%>" >
                                    </td>
                                </tr>

                                <tr class="fila">
                                    <td colspan="2" >Telefono Contacto</td>
                                    <td class="fila">
                                        <input name="telefono_contacto" type="text" class="textbox" id="telefono_contacto" style="width:170;" size="15" maxlength="15" value="<%=(request.getParameter("telefono_contacto") != null) ? request.getParameter("telefono_contacto") : ""%>">
                                    </td>
                                </tr>

                                <tr class="fila">
                                    <td colspan="2" >Celular Contacto</td>
                                    <td class="fila">
                                        <input name="celular_contacto" type="text" class="textbox" id="celular" style="width:170;" size="15" maxlength="15" value="<%=(request.getParameter("celular_contacto") != null) ? request.getParameter("celular_contacto") : ""%>">
                                    </td>
                                </tr>

                                <tr class="fila">
                                    <td colspan="2" >Cargo Contacto</td>
                                    <td class="fila">
                                        <input name="cargo_contacto" type="text" class="textbox" id="cargo_contacto" style="width:170;" size="15" maxlength="15" value="<%=(request.getParameter("cargo_contacto") != null) ? request.getParameter("cargo_contacto") : ""%>">
                                    </td>
                                </tr>

                                <tr class="fila">
                                    <td colspan="2" >Nombre Representante Legal</td>
                                    <td class="fila">
                                        <input name="nombre_representante" type="text" class="textbox" id="nombre_representante" style="width:170;" size="15" maxlength="60" value="<%=(request.getParameter("nombre_representante") != null) ? request.getParameter("nombre_representante") : ""%>">
                                    </td>
                                </tr>

                                <tr class="fila">
                                    <td colspan="2" >Telefono Representante Legal</td>
                                    <td class="fila">
                                        <input name="telefono_representante" type="text" class="textbox" id="telefono_representante" style="width:170;" size="15" maxlength="15" value="<%=(request.getParameter("telefono_representante") != null) ? request.getParameter("telefono_representante") : ""%>">
                                    </td>
                                </tr>

                                <tr class="fila">
                                    <td colspan="2" >Celular Representante Legal</td>
                                    <td class="fila">
                                        <input name="celular_representante" type="text" class="textbox" id="celular_representante" style="width:170;" size="15" maxlength="60" value="<%=(request.getParameter("celular_representante") != null) ? request.getParameter("celular_representante") : ""%>">
                                    </td>
                                </tr>

                                <tr class="fila">
                                    <td colspan="2" >Ejecutivo de Cuenta</td>
                                    <td class="fila">
                                        <%if (idusuario != null) {%> <input name="ejecutivo_ctaa" type="text" class="textbox" id="ejecutivo_ctaa" style="width:450;" value="<%=(request.getParameter("ejecutivo_ctaa") != null) ? request.getParameter("ejecutivo_ctaa") : ""%>" readonly >
                                        <%}%>
                                        <select name='ejecutivo_cta' id="ejecutivo_cta">
                                            <option value="">...</option>
                                            <%for (int i = 0; i < ejecutivo_cta.size(); i++) {%>
                                            <option value="<%=((String[]) ejecutivo_cta.get(i))[1]%>"><%=((String[]) ejecutivo_cta.get(i))[0]%></option>
                                            <%}%>
                                        </select>
                                    </td>
                                </tr>
								
                                <tr class="fila">
                                    <td colspan="2" >Cliente padre (Opcional)</td>
                                    <td><%if ((padre != null)&&(padre.length() > 0)) {%> <input name="padre_input" type="text" class="textbox" id="padre_input" style="width:450;" value="<%=(request.getParameter("padre") != null) ? request.getParameter("padre") : ""%>" readonly >
                                        <%}%>
                                        <select name='padre' id="padre">
                                            <option value="">...</option>
                                            <%for (int i = 0; i < padres.size(); i++) {%>
                                            <option value="<%=((String[]) padres.get(i))[1]%>"><%=((String[]) padres.get(i))[0]%>-<%=((String[]) padres.get(i))[1]%></option>
                                            <%}%>
                                        </select>
                                    </td>
                                </tr>

                            </table>
                            <table width="100%">
                                <tr class="fila">
                                    <td>
                                        <br>
                                    </td>
                                    <td>
                                        <br>
                                    </td>
                                </tr>
                                <tr class="fila">
                                    <%if ((idusuario == null) && (clvsrv.ispermitted(perfil,"5")||clvsrv.ispermitted(perfil,"6"))) {%>
                                    <td colspan="2" ><div align="center"><img src="<%=BASEURL%>/images/botones/aceptar.gif"  name="imgaceptar" onClick="formulario.target='';formulario.action='<%=CONTROLLER%>?estado=Clientes&accion=Ver'+'&opcion=creacliente';submitt(formulario);" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" style="cursor:hand"></div>
                                    </td>
                                    <%} else {%>
                                        <%if ((idusuario != null)&&((clvsrv.ispermitted(perfil,"5") && (cli_creation_user.equals(usuario.getLogin()) || (login_ejecutivo!=null && login_ejecutivo.equals(usuario.getLogin()))))||clvsrv.ispermitted(perfil,"6"))) {%>
                                        <td colspan="2" ><div align="center"><img src="<%=BASEURL%>/images/botones/modificar.gif"  name="imgsalir"  onMouseOver="botonOver(this);" onClick="formulario.target='';formulario.action='<%=CONTROLLER%>?estado=Clientes&accion=Ver'+'&opcion=modcliente';submitt2(formulario);" onMouseOut="botonOut(this);" style="cursor:hand" ></div>
                                        </td>
                                        <%}
                                    }%>
                                </tr>
                                <tr class="fila">
                                    <td>
                                        <br>
                                    </td>
                                    <td>
                                        <br>
                                    </td>
                                </tr>
                            </table>
                    </tr>
                    <tr>
                    <%  if(idusuario!=null)
                        {%>
                        <td valign="top" >
                           <table>
                                <tr class="fila">
                                    <td colspan="2" align="left" nowrap class="subtitulo">&nbsp;Solicitud</td>
                                    <td colspan="2" align="left" nowrap class="bordereporte"> </td>
                                </tr>
                                 <tr class="fila">
                                    <td colspan="2" >Solicitud</td>
                                    <td class="fila">
                                        <input  readonly name="idsolicitud" type="text" id="idsolicitud" class="textbox" value="<%=(idsolicitud != null) ? idsolicitud : ""%>">
                                    </td>
                                </tr>

                                <tr class="fila">
                                    <td colspan="2" >Cliente</td>
                                    <td class="fila">
                                        <input name="client" type="text" class="textbox" id="client" value="<%=(request.getParameter("nombre") != null) ? request.getParameter("nombre") : ""%>" style="width:170;" size="15"  maxlength="80" readonly>
                                        <input name="cliente" type="hidden" id="cliente" value="<%=(idusuario != null) ? idusuario : ""%>">
                                    </td>
                                </tr>

                                <tr class="fila">
                                    <td colspan="2" >Tipo de Solicitud</td>
                                    <td class="fila">
                                        <%if (idsolicitud != null && !(idsolicitud.equals("")) ) {%><input name="tipo_solicitud" type="hidden" class="textbox" id="tipo_solicitud" size="15" maxlength="15" value=""> <input name="tipo_solicitudd" type="text" class="textbox" id="tipo_solicitudd" style="width:100;" size="15" maxlength="15" value="<%=(request.getParameter("tipo_solicitudd") != null) ? request.getParameter("tipo_solicitudd") : ""%>" readonly >
                                        <%}else{%>
                                        <select name='tipo_solicitud' id="tipo_solicitud">
                                            <option value=''>...</option>
                                            <option value='Programado'>Programado</option>
                                            <option value='Emergencia'>Emergencia</option>
											<option value='ValorAgregado'>ValorAgregado</option>
                                        </select>
                                        <input name="tipo_solicitudd" type="hidden" class="textbox" id="tipo_solicitudd" size="15" maxlength="15" value="">
                                        <%}%>
                                    </td>
                                </tr>


                                <tr class="fila">
                                    <td colspan="2" >Nic</td>
                                    <td class="fila">
                                        <%if (request.getParameter("nicsof") != null) {%>
                                        <input name="nicsoff" type="text" class="textbox" id="nicsoff" value="<%=(request.getParameter("nicsof") != null) ? request.getParameter("nicsof") : ""%>" style="width:170;" size="15"  maxlength="80" readonly>
                                        <%} else {%>
                                        <select name='nicsoff' id="nicsoff">
                                            <%for (int i = 0; nics != null && i < nics.length; i++) {%>
                                            <option value="<%=nics[i]%>"><%=nics[i]%></option>
                                            <%}%>
                                        </select>
                                        <%}%>
                                    </td>
                                </tr>
                                <%  System.out.println("print1");
                                    if (request.getParameter("historico") != null) {%>
                                <tr class="fila">
                                    <td colspan="2" >Historico Situaci�n</td>
                                    <td class="fila">
                                        <textarea name="historico"  id="historico" rows="12" cols="50" readonly><%=(request.getParameter("historico") != null)?request.getParameter("historico"):""%></textarea>
                                    </td>
                                </tr>
                                <%}%>
                                <tr class="fila">
                                    <td colspan="2" >Situaci�n</td>
                                    <td class="fila">
                                        <textarea name="detalle_inconsistencia"  id="detalle_inconsistencia" rows="5" cols="50" ><%=(request.getParameter("detalle_inconsistenciaa") != null) ? request.getParameter("detalle_inconsistenciaa") : ""%></textarea>
                                    </td>
                                </tr>
                                <tr class="fila">
                                    <td colspan="2" >Fecha Solicitud</td>
                                    <td valign="middle">
                                        <input  name='fecha_oferta' size="20" readonly="true" class="textbox" value='<%=fechainicio%>' style='width:35%'>
                                    </td>
                                </tr>
                            </table>

                            <table width="100%">
                                <tr class="fila">
                                    <td>
                                        <br>
                                    </td>
                                    <td>
                                        <br>
                                    </td>
                                </tr>
                                <tr class="fila">
                                    <%  System.out.println("print2");
                                    if ((idsolicitud == null || idsolicitud.equals("")) && (((clvsrv.ispermitted(perfil,"1"))&&(cli_creation_user.equals(usuario.getLogin())||login_ejecutivo.equals(usuario.getLogin())))||clvsrv.ispermitted(perfil,"6"))) {%>
                                    <td colspan="2" ><div align="center"><img src="<%=BASEURL%>/images/botones/aceptar.gif"  name="imgaceptar" onClick="formulario.target='';formulario.action='<%=CONTROLLER%>?estado=Clientes&accion=Ver'+'&opcion=creasolicitud';submittt(formulario);" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" style="cursor:hand"></div>
                                    </td>
                                    <%} else {
                                        if ((idsolicitud!=null) && ((clvsrv.ispermitted(perfil,"2") && (sol_creation_user!=null && sol_creation_user.equals(usuario.getLogin())||login_ejecutivo.equals(usuario.getLogin())))||clvsrv.ispermitted(perfil,"6"))) {%>
                                            <td colspan="2" ><div align="center"><img src="<%=BASEURL%>/images/botones/modificar.gif"  name="imgsalir"  onMouseOver="botonOver(this);" onClick="formulario.target='';formulario.action='<%=CONTROLLER%>?estado=Clientes&accion=Ver'+'&opcion=modsolicitud';submittt(formulario);" onMouseOut="botonOut(this);" style="cursor:hand" ></div>
                                            </td>
                                        <%}
                                    }%>
                                </tr>
                            </table>


                        </td>
                        <%}%>
                        <%System.out.println("print3"+idsolicitud);
                        if (idsolicitud != null && !idsolicitud.equals("")) {%>
                        <td valign="top" >
                            <table>
                                <tr class="fila">
                                    <td colspan="2" align="left" nowrap class="subtitulo">&nbsp;Acciones</td>
                                    <td colspan="2" align="left" nowrap class="bordereporte"> </td>
                                </tr>
                                <tr class="fila">
                                    <td colspan="2" >Contratista</td>
                                    <td class="fila">
                                        <select name='contratista' id="contratista">
                                            <option value="">...</option>
                                            <%
                                            System.out.println("print4");
                                            for (int i = 0; i < contratistas.size(); i++) {%>
                                            <option value="<%=((String[]) contratistas.get(i))[1]%>"><%=((String[]) contratistas.get(i))[0]%></option>
                                            <%}
                                            System.out.println("print5");
                                            %>
                                        </select>
                                    </td>
                                </tr>

                                <tr class="fila">
                                    <td colspan="2">Descripcion</td>
                                    <td class="fila">
                                        <textarea name="descripcion"  id="descripcion" rows="5" cols="50" ><%=(request.getParameter("desc") != null) ? request.getParameter("desc") : ""%></textarea>
                                    </td>
                                </tr>
                            </table>

                            <table width="100%">
                                <tr class="fila">
                                    <td>
                                        <br>
                                    </td>
                                    <td>
                                        <br>
                                    </td>
                                </tr>
                                <%
                                System.out.println("print7");
                                if((clvsrv.ispermitted(perfil,"3")&&(sol_creation_user.equals(usuario.getLogin())||login_ejecutivo.equals(usuario.getLogin())))||clvsrv.ispermitted(perfil,"6")) {
                                    System.out.println("print8");
                                %>
                                <tr class="fila">
                                    <td colspan="2" ><div align="center"><img src="<%=BASEURL%>/images/botones/agregar.gif"  name="imgaceptar" onClick="formulario.target='';formulario.action='<%=CONTROLLER%>?estado=Clientes&accion=Ver'+'&opcion=creaaccion';submitttt(formulario);" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" style="cursor:hand"></div>
                                    </td>
                                </tr>
                                <%}
                                System.out.println("print9");
%>
                            </table>

                            <% if (acciones != null && acciones.size() > 0){%>
                            <table width="100%">
                                <tr class="fila">
                                    <td>Contratista</td><td>Descripcion</td><td>Fecha Creacion</td><td>Observacion</td><td>Estado</td><td>Tipo de Trabajo</td><td>Opciones</td>
                                </tr>
                                <%
                                System.out.println("print11");
                                for (int i = 0; i < acciones.size(); i++) {
                                        System.out.println("print12");
                                        AccionesEca temp = ((AccionesEca) acciones.get(i));
                                        String contratemp = "";
                                        String estadostemp = "";%>
                                <tr class="fila">
                                     <%
                                     System.out.println("print13");
                                      for (int j = 0; j < contratistas.size(); j++) {
                                             if (temp.getContratista().equals(((String[]) contratistas.get(j))[1])) {
                                                 contratemp = ((String[]) contratistas.get(j))[0];
                                             }
                                          }
                                         for (int j = 0; j < estados.size(); j++) {
                                             if (temp.getEstado().equals(((String[]) estados.get(j))[1])) {
                                                 estadostemp = ((String[]) estados.get(j))[0];
                                             }
                                         }
                                    %>
                                    <td title="<%=temp.getContratista()%>"><%=contratemp%></td>
                                    <td><%=temp.getDescripcion()%></td>
                                    <td><%=temp.getCreation_date().substring(0, 16)%></td>
                                    <td>
                                        <a target="_blank" href="<%=CONTROLLER%>?estado=Negocios&accion=Applus&opcion=consultarAccion&id_solici=<%=  temp.getId_solicitud()%>&id_accion=<%=temp.getId_accion()%>">
                                            <%=(temp.getObservaciones().length() > 50) ? temp.getObservaciones().substring(0, 50) + "..." : ((temp.getObservaciones().equals("")) ? "agregar" : temp.getObservaciones())%>
                                        </a>
                                    </td>
                                    <td title="<%=estadostemp%>"><%=temp.getEstado()%></td>
                                    <td><%=temp.getTipo_trabajo()%></td>
                                    <td align="center">
                                        <%
                                        //System.out.println("prin14");										
                                        if( clvsrv.ispermitted(perfil,"4") || sol_creation_user.equals(usuario.getLogin()) || login_ejecutivo.equals(usuario.getLogin()) || clvsrv.ispermitted(perfil,"6") ){%>
                                             <%if( clvsrv.ispermitted(perfil,"3") || clvsrv.ispermitted(perfil,"6") ){%>
											 	<img src='<%=BASEURL%>/images/botones/iconos/menos1.gif' width="20" height="20" onClick="formulario.action='<%=CONTROLLER%>?estado=Clientes&accion=Ver'+'&opcion=eliminaaccion&idaccion=<%=temp.getId_accion()%>';if(confirm('�Seguro quiere elminar esta accion?')){formulario.submit()};" title="Eliminar" style="cursor:hand" >
											 <%}%>
                                             <%if (!(temp.getEstado().equals("020"))){//091029pm%>

												<%if ((temp.getEstado().equals("030"))){%>
													<img src='<%=BASEURL%>/images/botones/iconos/clasificar.gif'        width="20" height="20" onClick="formulario.action='<%=BASEURL%>/jsp/delectricaribe/cotizacion/cotizacion_contratista.jsp?idaccion=<%=temp.getId_accion()%>';formulario.target='_blank';formulario.submit();" title="Generar Cotizacion" style="cursor:hand" >
												<%}
                                                else{%>
                                                    <img src='<%=BASEURL%>/images/botones/iconos/srchclasificar.gif'    width="20" height="20" onClick="formulario.action='<%=CONTROLLER%>?estado=Cotizacion&accion=Buscar&consecutivo=<%=temp.getId_accion()%>';formulario.target='_blank';formulario.submit();" title="Consultar Cotizacion" style="cursor:hand" >
                                                    <img src='<%=BASEURL%>/images/botones/iconos/aiu.gif'               width="20" height="20" onClick="formulario.action='<%=BASEURL%>/jsp/delectricaribe/cotizacion/aiu_cotizacion.jsp?idaccion=<%=temp.getId_accion()%>';formulario.target='_blank';formulario.submit();" title="AIU" style="cursor:hand" >
                                                <%}%>

					     					<%}//091029pm%>
                                        <%}%>
                                    </td>
                                </tr>
                                <%}%>
                            </table>
                            <% }%>
                        </td>
                        <%}%>
                    </tr>
                </table>
                <%/*}catch(Exception e)
                    {   e.printStackTrace();
                        System.out.println(e.getMessage());
                        System.out.println(e.toString());
                    }*/%>
            </FORM>

			<div align="center">
				<img src="<%=BASEURL%>/images/botones/restablecer.gif"  name="imgrefrescar" onClick="formulario.target='';formulario.action='<%=CONTROLLER%>?estado=Clientes&accion=Ver'+'&opcion=refrescarx';formulario.submit();" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" style="cursor:hand">
			</div>


            <%String mensaje = "";
            if (Retorno != null && !(Retorno.equals(""))) {%>
            <br>
            <table align="center" width="458" height="70">
                <tr>
                    <td align="center">
                        <table border="2" width="250" height="70" border="2" align="center">
                            <tr>
                                <td align="center">
                                    <table width="100%" height="100%"  border="1" align="center"  bordercolor="#F7F5F4" bgcolor="#FFFFFF">
                                        <tr>
                                            <td width="229" align="center" class="mensajes"><%=Retorno%></td>
                                            <td width="29" background="<%=BASEURL%>/images/cuadronaranja.JPG">&nbsp;</td>
                                            <td width="58">&nbsp;</td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
            </table>
            <%}%>


        </div>
        <iframe width="188" height="166" name="gToday:normal:agenda.js:gfPop:plugins.js" id="gToday:normal:agenda.js:gfPop:plugins_24.js" src="<%=BASEURL%>/js/Calendario/ipopeng.htm" scrolling="yes" frameborder="0" style="visibility:visible; z-index:999; position:absolute; left:-500px; top:0px;">
        </iframe>


    </body>
</html>