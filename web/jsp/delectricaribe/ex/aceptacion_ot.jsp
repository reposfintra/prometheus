<!--
    Autor:  Ing. Pablo Bassil
    Fecha:  10 de Diciembre de 2009
-->

<%@ page session   ="true"%>
<%@ page errorPage ="/error/ErrorPage.jsp"%>
<%@ page import    ="java.util.*" %>
<%@ page import    ="com.tsp.operation.model.*"%>
<%@ page import    ="com.tsp.operation.model.beans.*"%>
<%@ page import    ="com.tsp.operation.model.services.*"%>
<%@ page import    ="com.tsp.util.*"%>
<%@ page import    ="com.tsp.util.Util.*"%>
<%@ include file   ="/WEB-INF/InitModel.jsp"%>
<%@taglib uri="http://jakarta.apache.org/taglibs/input-0.90" prefix="input" %>

<html>
    <head>
        <title>ACEPTACION DE PAGOS</title>
        <link href="<%=BASEURL%>/css/estilostsp.css" rel="stylesheet" type="text/css">

        <script type='text/javascript' src="<%=BASEURL%>/js/boton.js"></script>
        <script type='text/javascript' src="<%=BASEURL%>/js/prototype.js"></script>
        <script type="text/javascript" src="<%=BASEURL%>/js/buscarproveedor.js"></script>

        <script type='text/javascript'>

            var generar = 0;

            /* Esta funcion me indica si el input tiene
             * numeros.
             */
            function validate(size){
                var ok = true;

                for(var i = 0; i < size; i++){
                    if( $('check'+i).checked == true ){
                        if( isNaN( $('cuotas_c'+i).value ) ){
                            ok = false;
                        }
                        if( isNaN( $('meses'+i).value ) ){
                            ok = false;
                        }
                        if( isNaN( ($('val_cuotas_c'+i).value).replace(/,/g,'') ) ){
                            ok = false;
                        }
                        if( isNaN( $('porc_cuotas_c'+i).value ) ){
                            ok = false;
                        }
                        if( isNaN( $('porc_base_c'+i).value ) ){
                            ok = false;
                        }
                    }
                }

                return ok;
            }

            /* Esta funcion me sirve para calcular
             * la cuota inicial.
             */
            function calcCI(valof, i){
                var r = '';

                r = ( (valof * ($('porc_base_c'+i).value/100)) * ($('porc_cuotas_c'+i).value/100) );

                $('val_cuotas_c'+i).value = formatNumber( r );
            }
            function calcPorcCI(valof, i){
                var r = '';
                var val_c = $('val_cuotas_c'+i).value;
                val_c = val_c.replace(',','');

                if((valof>0)&&($('porc_base_c'+i).value>0)&&(val_c>0)){
                    r = (100 * val_c) / (valof * ($('porc_base_c'+i).value));
                    r = r * 100;
                }
                else{
                    r = 'Faltan datos';
                }

                $('porc_cuotas_c'+i).value = formatNumber( r );
            }

            /* Esta funcion sirve para chequear todos los
             * checkbox para no tener necesidad de ir uno
             * a uno chequenadolos.
             */
            function checkAll(field){

                if($('allcheck').checked == true){
                    for (i = 0; i < field; i++){
                        $('check'+i).checked = true;
                    }
                }
                else{
                    for (i = 0; i < field; i++){
                        $('check'+i).checked = false;
                    }
                }

            }

            /* Esta funcion sirve para calcular los
             * porcentajes base para todos los subclientes.
             */
            function setBaseCalcAll(field){
                var porc_final = 0;
                var clients = 0;

                for (var i = 0; i < field; i++){
                    if( $('check'+i).checked == true ){
                        clients++;
                    }
                }

                porc_final = 100/clients;

                for (var i = 0; i < field; i++){
                    if( $('check'+i).checked == true ){
                        $('porc_base_c'+i).value = porc_final;
                    }
                }

                sumAllPorc(field, 'sum_porc');
            }
            function unsetBaseCalcAll(size){
                for (var i = 0; i < size; i++){
                    $('porc_base_c'+i).value = 0;
                }

                $('sum_porc').value = 0;
            }

            /* Esta funcion es para sumar todos los
             * porcentajes base de cada uno de los inputs
             * en la pagina.
             */
            function sumAllPorc(field, input){
                var porc_final = 0;

                for (var i = 0; i < field; i++){
                    if( $('check'+i).checked == true ){
                        porc_final += parseInt( $('porc_base_c'+i).value, 10 );
                    }
                }

                $(input).value = porc_final;
            }

            /* Esta funcion sirve para poner el mismo valor
             * de las cuotas para todos los elementos
             * chequeados.
             */
            function sameCuota(field, val){
                for (var i = 0; i < field; i++){
                    if( $('check'+i).checked == true ){
                        $('cuotas_c'+i).value = val;
                    }
                }
            }

            /* Esta funcion sirve para poner el mismo valor
             * del porcentaje de cuotas para todos los
             * elementos chequeados.
             */
            function samePorc(field, val){

                for (var i = 0; i < field; i++){
                    if( $('check'+i).checked == true ){
                        $('porc_cuotas_c'+i).value = val;
                    }
                }

            }


            //*****************************
            //*           AJAX            *
            //*****************************

            /* Esta funcion me lleva al action de las OT, y
             * me permite hacer operaciones en el mismo.
             */
            function sendAction(url, size){

                if( $('sum_porc').value == '100' ){

                    if( validate(size) ){

                        var id          = '';
                        var cuotas      = '';
                        var val_ci      = '';
                        var meses       = '';
                        var porc_cuotas = '';
                        var porc_base   = '';
                        var nics        = '';

                        /* Con este If nada mas recojo el primer
                         * checkbox, para luego anteponer con el
                         * For un ';' a cada valor.
                         */

                        for (i = 0; i < size; i++){
                            if( $('check'+i).checked == true ){
                                id          += ';' + $('check'+i).value;
                                cuotas      += ';' + $('cuotas_c'+i).value;
                                val_ci      += ';' + ($('val_cuotas_c'+i).value).replace(/,/g,'')   ;
                                meses       += ';' + $('meses'+i).value;
                                porc_cuotas += ';' + $('porc_cuotas_c'+i).value;
                                porc_base   += ';' + $('porc_base_c'+i).value;
                                nics        += ';' + $('nic_c'+i).value;
                            }
                        }

                        var p = 'id=' + id + '&cuotas=' + cuotas + '&porc_cuotas=' + porc_cuotas + '&porc_base=' + porc_base + '&nics=' + nics + '&meses=' + meses + '&val_ci=' + val_ci;

                        new Ajax.Request(
                            url,
                            {
                                method: 'get',
                                parameters: p,
                                onLoading: loading,
                                onComplete: sendMessage
                            });

                    }
                    else{
                        alert('Hay caracteres no validos');
                    }
                }
                else{
                    alert('El total de porcentajes debe estar en 100%');
                }
            }

            function loading(){
                generar = 1;
                $('loading_div').style.visibility = 'visible';
            }

            /* Esta funcion me devuelve la respuesta del
             * action.
             */
            function sendMessage(response){
                alert(response.responseText);
                $('loading_div').style.visibility = 'hidden';
                generar = 0;
            }
        </script>

    </head>

    <%
        ArrayList hijos = model.ElectricaribeOtSvc.getHijosList( request.getParameter("id_padre") );
        ArrayList pagos = model.ElectricaribeOtSvc.getPayments( request.getParameter("id_sol") );
        ArrayList array;
    %>

    <body>
        <div id="capaSuperior" style="position:absolute; width:100%; height:100px; z-index:0; left: 0px; top: 0px;">
            <jsp:include page="/WEB-INF/toptsp.jsp?encabezado=ACEPTACION DE PAGOS"/>
        </div>

        <div align="center" id="capaCentral" style="position:absolute; width:100%; height:85%; z-index:0; left: -1px; top: 100px; overflow: scroll;">
            <table border="1">
                <tr class="fila">
                    <td align="center" colspan="4">
                            OPCIONES GENERALES
                    </td>
                </tr>
                <tr class="fila">
                    <td align="center">
                        <input id="allcheck" name="allcheck" type="checkbox" value="all" onclick="checkAll(<%=hijos.size()%>);">Todos
                    </td>
                    <td align="center">
                        Cuotas <br>
                        <input id="cuotas" name="cuotas" type="text">
                        <img src="<%=BASEURL%>/images/botones/iconos/abajo.gif" align="center" width="23" height="23" style="cursor:hand" onclick="sameCuota(<%=hijos.size()%>, $('cuotas').value)">
                    </td>
                    <td align="center">
                        Porcentaje cuota inicial <br>
                        <input id="porc_cuotas" name="porc_cuotas" type="text">
                        <img src="<%=BASEURL%>/images/botones/iconos/abajo.gif" align="center" width="23" height="23" style="cursor:hand" onclick="samePorc(<%=hijos.size()%>, $('porc_cuotas').value)">
                    </td>
                    <td align="center">
                        Porcentaje base <br>
                        <img src="<%=BASEURL%>/images/botones/iconos/por_call.gif"  onclick="setBaseCalcAll(<%=hijos.size()%>)"   align="center" width="30" height="30" style="cursor:hand" alt="Calcular para todos">
                        <img src="<%=BASEURL%>/images/botones/iconos/eliminate.gif" onclick="unsetBaseCalcAll(<%=hijos.size()%>)" align="center" width="30" height="30" style="cursor:hand" alt="Vaciar">
                    </td>
                </tr>
            </table>

            <br>

            <table border="0">

                <tr class="subtitulo">
                    <td></td>
                    <td>Id cliente</td>
                    <td>Cliente</td>
                    <td>Cuotas</td>
                    <td>Meses Mora</td>
                    <td>Porcentaje cuota inicial</td>
                    <td>Porcentaje base</td>
                    <td>Valor cuota inicial</td>
                    <td>NIC</td>
                </tr>

                <%if(hijos.size() > 0){
                %>

                <tr class="subtitulo">
                    <td>
                        <input id="check0" name="check0" type="checkbox" value="<%=((ClienteEca)hijos.get(0)).getId_cliente()%>" onclick="sumAllPorc(<%=hijos.size()%>, 'sum_porc')">
                    </td>
                    <td><%=((ClienteEca)hijos.get(0)).getId_cliente()%></td>
                    <td><%=((ClienteEca)hijos.get(0)).getNombre()%></td>
                    <td>
                        <input type="text" value="0" id="cuotas_c0" name="cuotas_c0" onkeyup="isNumber( $('cuotas_c<%=0%>').value )">
                    </td>
                    <td style="text-align: center">
                        <select style=" width: 50px; " id="meses0" name="meses0">
                            <option>0</option>
                            <option>1</option>
                            <option>2</option>
                            <option>3</option>
                        </select>
                    </td>
                    <td>
                        <input type="text" value="0" id="porc_cuotas_c0" name="porc_cuotas_c0" onkeyup="calcCI(<%=model.ElectricaribeOfertaSvc.getOfferValue(request.getParameter("id_sol"))%>, '0')">
                    </td>
                    <td>
                        <input type="text" value="0" id="porc_base_c0" name="porc_base_c0" onkeyup="calcCI(<%=model.ElectricaribeOfertaSvc.getOfferValue(request.getParameter("id_sol"))%>, '0');sumAllPorc(<%=hijos.size()%>, 'sum_porc');">
                    </td>
                    <td>
                        <input type="text" value="0" id="val_cuotas_c0" name="val_cuotas_c0" onkeyup="calcPorcCI(<%=model.ElectricaribeOfertaSvc.getOfferValue(request.getParameter("id_sol"))%>, '0')">
                    </td>
                    <td>
                        <select id="nic_c<%=0%>">
                            <%
                            array = model.ElectricaribeOtSvc.getNics(((ClienteEca)hijos.get(0)).getId_cliente());

                            for (int j=0; j<array.size(); j++){%>

                                <option value="<%=((String)array.get(j))%>"><%=((String)array.get(j))%></option>

                            <%}%>
                        </select>
                    </td>
                </tr>

                <%

                for(int i=1; i<hijos.size(); i++){%>
                    <tr class="fila">
                        <td>
                            <input id="check<%=i%>" name="check<%=i%>" type="checkbox" value="<%=((ClienteEca)hijos.get(i)).getId_cliente()%>" onclick="sumAllPorc(<%=hijos.size()%>, 'sum_porc')">
                        </td>
                        <td><%=((ClienteEca)hijos.get(i)).getId_cliente()%></td>
                        <td><%=((ClienteEca)hijos.get(i)).getNombre()%></td>
                        <td>
                            <input type="text" value="0" id="cuotas_c<%=i%>" name="cuotas_c<%=i%>" onkeyup="isNumber( $('cuotas_c<%=i%>').value )">
                        </td>
                        <td style="text-align: center">
                            <select style=" width: 50px; " id="meses<%=i%>" name="meses<%=i%>">
                                <option>0</option>
                                <option>1</option>
                                <option>2</option>
                                <option>3</option>
                            </select>
                        </td>
                        <td>
                            <input type="text" value="0" id="porc_cuotas_c<%=i%>" name="porc_cuotas_c<%=i%>" onkeyup="calcCI(<%=model.ElectricaribeOfertaSvc.getOfferValue(request.getParameter("id_sol"))%>, '<%=i%>')">
                        </td>
                        <td>
                            <input type="text" value="0" id="porc_base_c<%=i%>" name="porc_base_c<%=i%>" onkeyup="calcCI(<%=model.ElectricaribeOfertaSvc.getOfferValue(request.getParameter("id_sol"))%>, '<%=i%>');sumAllPorc(<%=hijos.size()%>, 'sum_porc');">
                        </td>
                        <td>
                            <input type="text" value="0" id="val_cuotas_c<%=i%>" name="val_cuotas_c<%=i%>" onkeyup="calcPorcCI(<%=model.ElectricaribeOfertaSvc.getOfferValue(request.getParameter("id_sol"))%>, '<%=i%>')">
                        </td>
                        <td>
                            <select id="nic_c<%=i%>">
                                <%
                                array = model.ElectricaribeOtSvc.getNics(((ClienteEca)hijos.get(i)).getId_cliente());

                                for (int j=0; j<array.size(); j++){
                                %>

                                <option value="<%=((String)array.get(j))%>"><%=((String)array.get(j))%></option>

                                <%}%>
                            </select>
                        </td>
                    </tr>
                <%}%>

                <tr class="fila">
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td align="center">Total</td>
                    <td>
                        <input id="sum_porc" name="sum_porc" type="text" value="" readonly>
                    </td>
                    <td></td>
                    <td></td>
                </tr>

                <%}%>

            </table>

            <br>

            <input type="hidden" id="ids">

            <%if(hijos.size() > 0){%>
                <img src="<%=BASEURL%>/images/botones/aceptar.gif" height="21" title='Aceptar' onclick="sendAction('<%=CONTROLLER%>?estado=Electricaribe&accion=Ot&opcion=1&id_solicitud=<%=request.getParameter("id_sol")%>', <%=hijos.size()%>, document.getElementsByName('check'));" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" style="cursor:hand">
            <%}%>
            <img src="<%=BASEURL%>/images/botones/salir.gif"      name="imgsalir"     onMouseOver="botonOver(this);" onClick="parent.close();" onMouseOut="botonOut(this);" style="cursor:hand">

            <br>

            <div align="center" id="loading_div" style="visibility: hidden">
                Cargando...
            </div>

            <br>
            <br>

            <%if(pagos.size() > 0){%>
                <table border="0">

                    <tr style="height:30px" class="subtitulo">
                        <td colspan="9" style="text-align: center">PAGOS ANTERIORES DE LA SOLICITUD <%=request.getParameter("id_sol")%></td>
                    </tr>

                    <tr class="subtitulo">
                        <td>Id cliente</td>
                        <td>Cliente</td>
                        <td>Cuotas</td>
                        <td>Meses Mora</td>
                        <td>Porcentaje cuota inicial</td>
                        <td>Porcentaje base</td>
                        <td>Valor cuota inicial</td>
                        <td>Fecha de financiacion</td>
                        <td>NIC</td>
                    </tr>

                    <%for(int i=0; i<pagos.size(); i++){%>
                        <tr class="fila">
                            <td style="text-align: center"> <%=((SubclienteEca)pagos.get(i)).getId_cliente()           %> </td>
                            <td style="text-align: center"> <%=((SubclienteEca)pagos.get(i)).getNombre_cliente()       %> </td>
                            <td style="text-align: center"> <%=((SubclienteEca)pagos.get(i)).getPeriodo()              %> </td>
                            <td style="text-align: center"> <%=((SubclienteEca)pagos.get(i)).getMeses_mora()           %> </td>
                            <td style="text-align: center"> <%=((SubclienteEca)pagos.get(i)).getPorc_cuota_inicial()   %> </td>
                            <td style="text-align: center"> <%=((SubclienteEca)pagos.get(i)).getPorc_base()            %> </td>
                            <td style="text-align: center">$ <%=((SubclienteEca)pagos.get(i)).getVal_cuota_inicial() %> </td>
                            <td style="text-align: center"> <%=((SubclienteEca)pagos.get(i)).getFecha_financiacion()   %> </td>
                            <td style="text-align: center"> <%=((SubclienteEca)pagos.get(i)).getNic()                  %> </td>
                        </tr>
                    <%}%>

                </table>
            <%}
              else{%>
                <div align="center" id="loading_div" style="visibility: hidden">
                    No hay pagos anteriores para esta solicitud.
                </div>
            <%}%>

        </div>
    </body>
</html>