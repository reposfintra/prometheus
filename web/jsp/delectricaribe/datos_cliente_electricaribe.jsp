<!--
- Autor : Ing. Julio Ernesto Barros Rueda
- Date  : 14 de Mayo de 2007
- Copyrigth Notice : Fintravalores S.A. S.A

Descripcion : Pagina JSP, que maneja el ingreso de identidades
-->

<%-- Declaracion de librerias--%>
<%@page contentType="text/html;"%>
<%@page session="true"%>
<%@page import="java.util.*"%>
<%@page import="com.tsp.operation.model.*"%> 
<%@page import="com.tsp.operation.model.beans.*"%>
<%@page import="com.tsp.operation.model.services.*"%>
<%@page import="com.tsp.util.*"%>
<%@page import="com.tsp.util.Util.*"%>
<%@include file="/WEB-INF/InitModel.jsp"%>
<%@page import="com.tsp.operation.model.*"%>
<%@taglib uri="http://jakarta.apache.org/taglibs/input-0.90" prefix="input" %>

<html>
    <head>
        <title>Datos Cliente Electricaribe</title>
        <link href="../../../css/estilostsp.css" rel="stylesheet" type="text/css"> 
        <link href="<%=BASEURL%>/css/estilostsp.css" rel="stylesheet" type="text/css">
    </head>
    
    <script type='text/javascript' src="<%=BASEURL%>/js/Validacion.js"></script>
    <script type='text/javascript' src="<%= BASEURL %>/js/boton.js"></script>
    <script type='text/javascript' src="<%= BASEURL %>/js/general.js"></script>
    <script src='<%=BASEURL%>/js/tools.js' language='javascript'></script>
    <script src='<%=BASEURL%>/js/date-picker.js'></script>
    <script src="<%= BASEURL %>/js/transferencias.js"></script>
    
    <%
    Usuario usuario = (Usuario) session.getAttribute("Usuario");
    String Retorno=request.getParameter("mensaje");
    ClientesVerService clvsrv=new ClientesVerService();
    ArrayList zona = clvsrv.getDatos("zona");
    ArrayList ciudad = clvsrv.getDatos("ciudad");
    ArrayList sector = clvsrv.getDatos("sector");
    ArrayList ejecutivo_cta = clvsrv.getDatos("ejecutivo_cta");
    

    %>
    
    <script>
        
        function enviarFormularioX(CONTROLLER,frm){	
			frm.action='<%=CONTROLLER%>?estado=Clientes&accion=Ver&opcion=commite';
			frm.submit();
        }    
				
    </script>
    
    <body>
        
        <div id="capaSuperior" style="position:absolute; width:100%; height:100px; z-index:0; left: 0px; top: 0px;">
            <jsp:include page="/toptsp.jsp?encabezado=DATOS CLIENTE ELECTRICARIBE"/>
        </div>
        <div id="capaCentral" align="center" style="position:absolute; width:100%; height:87%; z-index:0; left: 0px; top: 100px; overflow: scroll;">
            
            <FORM name='formulario' method="post" >
                
                <table width="432" height="167" border="2"align="center">
                    
                    <tr>
                      <td width="420" height="159">
                            <table width="100%" height="72%" class="tablaInferior" >
                                <tr class="fila">
                                    <td colspan="2" align="left" nowrap class="subtitulo">&nbsp;Datos Cliente Electricaribe</td>
                                    <td colspan="2" align="left" nowrap class="bordereporte"> </td>
                                </tr>
                                
                                <tr class="fila">
                                    <td colspan="2" >NIC</td>
                                    <td class="fila">
                                            <input name="nic" type="text" class="textbox" id="nic" style="width:170;" size="15" maxlength="7" >                            
                                    </td>
                                </tr>
                                
                                <tr class="fila">
                                    <td colspan="2" >NIT</td>
                                    <td class="fila">
                                            <input name="nit" type="text" class="textbox" id="nit" style="width:170;" size="15" maxlength="15" >                            
                                    </td>
                                </tr>
                                
                                <tr class="fila">
                                    <td colspan="2" >Tipo de Cliente</td>
                                    <td class="fila">
                                            <select name='tipo_identificacion' id="tipo_identificacion">
                                                <option value='R'>R</option>
                                                <option value='NR'>NR</option>
                                            </select>
                                    </td>
                                </tr>

                                <tr class="fila">
                                    <td colspan="2" >Nombre</td>
                                    <td class="fila">
                                            <input name="nombre" type="text" class="textbox" id="nombre" style="width:170;" size="15" maxlength="60" >                            
                                    </td>
                                </tr>

                                <tr class="fila">
                                    <td colspan="2" >Zona</td>
                                    <td class="fila">
                                            <select name='zona' id="zona">
                                            <%for(int i=0;i<zona.size();i++){%>
                                                <option value='<%=zona.get(i)%>'><%=zona.get(i)%></option>
                                            <%}%>
                                            </select>
                                    </td>
                                </tr>
                                
                                <tr class="fila">
                                    <td colspan="2" >Ciudad</td>
                                    <td class="fila">
                                            <select name='ciudad' id="ciudad">
                                            <%for(int i=0;i<ciudad.size();i++){%>
                                                <option value='<%=ciudad.get(i)%>'><%=ciudad.get(i)%></option>
                                            <%}%>
                                            </select>
                                    </td>
                                </tr>

                                <tr class="fila">
                                    <td colspan="2" >Sector</td>
                                    <td class="fila">
                                            <select name='sector' id="sector">
                                            <%for(int i=0;i<sector.size();i++){%>
                                                <option value='<%=sector.get(i)%>'><%=sector.get(i)%></option>
                                            <%}%>
                                            </select> 
                                    </td>
                                </tr>
                                
                                <tr class="fila">
                                    <td colspan="2" >Direccion</td>
                                    <td class="fila">
                                            <input name="direccion" type="text" class="textbox" id="direccion" style="width:170;" size="15" maxlength="60" >                            
                                    </td>
                                </tr>
                                
                                
                                <tr class="fila">
                                    <td colspan="2" >Telefono</td>
                                    <td class="fila">
                                            <input name="telefono" type="text" class="textbox" id="telefono" style="width:170;" size="15" maxlength="15" >                            
                                    </td>
                                </tr>
                                
                                
                                <tr class="fila">
                                    <td colspan="2" >Celular</td>
                                    <td class="fila">
                                            <input name="celular" type="text" class="textbox" id="celular" style="width:170;" size="15" maxlength="15" >                            
                                    </td>
                                </tr>
                                
                                <tr class="fila">
                                    <td colspan="2" >Ejecutivo de Cuenta</td>
                                    <td class="fila">
                                            <select name='ejecutivo_cta' id="sector">
                                            <%for(int i=0;i<ejecutivo_cta.size();i++){%>
                                                <option value='<%=ejecutivo_cta.get(i)%>'><%=ejecutivo_cta.get(i)%></option>
                                            <%}%>
                                            </select>
                                  </td>
                                </tr>
                                
                                <tr class="fila">
                                    <td colspan="2" >Contacto</td>
                                    <td class="fila">
                                            <input name="contacto" type="text" class="textbox" id="contacto" style="width:170;" size="15" maxlength="80" >                            
                                    </td>
                                </tr>
                                
                                <tr class="fila">
                                    <td colspan="2" >Cargo</td>
                                    <td class="fila">
                                            <input name="cargo" type="text" class="textbox" id="cargo" style="width:170;" size="15"  maxlength="80">                            
                                    </td>
                                </tr>
								<tr class="fila">
                                    <td colspan="2" >Descripción</td>
                                    <td class="fila">                                            
											<textarea name="descripcione" rows="4"></textarea>
                                    </td>
                                </tr>

                                <tr>
                                    <td colspan="2" ><br><div align="right"><img src="<%=BASEURL%>/images/botones/aceptar.gif"  name="imgaceptar" onClick="javascript:enviarFormularioX('<%=CONTROLLER%>',formulario);" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" style="cursor:hand"></div></td>
                                    <td colspan="2" ><br><div align="left"><img src="<%=BASEURL%>/images/botones/salir.gif"  name="imgsalir"  onMouseOver="botonOver(this);" onClick="parent.close();" onMouseOut="botonOut(this);" style="cursor:hand" ></div></td>
                                </tr>
                                <tr>
                                </tr>
                            </table>
                            <%String mensaje="";
                            if( Retorno != null && !(Retorno.equals(""))){%>
                            <br>
                            <table align="center" width="458" height="70">
                                <tr>
                                    <td align="center">
                                        <table border="2" width="250" height="70" border="2" align="center">
                                            <tr>
                                                <td align="center">
                                                    <table width="100%" height="100%"  border="1" align="center"  bordercolor="#F7F5F4" bgcolor="#FFFFFF">
                                                        <tr>
                                                            <td width="229" align="center" class="mensajes"><%=Retorno%></td>
                                                            <td width="29" background="<%=BASEURL%>/images/cuadronaranja.JPG">&nbsp;</td>
                                                            <td width="58">&nbsp;</td>
                                                        </tr>
                                                    </table>                      
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                            </table>
                            <%}%>
            </form>
        </div>
        <iframe width="188" height="166" name="gToday:normal:agenda.js:gfPop:plugins.js" id="gToday:normal:agenda.js:gfPop:plugins_24.js" src="<%=BASEURL%>/js/Calendario/ipopeng.htm" scrolling="yes" frameborder="0" style="visibility:visible; z-index:999; position:absolute; left:-500px; top:0px;">
        </iframe>
    </body>
</html>


