<!--
- Autor : Ing. Rhonalf Martinez
- Date  : 25 de julio del 2009
- Copyrigth Notice : Fintravalores S.A.
-->

<%--
-@(#)
--Descripcion : P�gina JSP, que maneja el ingreso de ordenes de compra.
--%>

<%@page contentType="text/html"%>
<%@page session="true"%>
<%@page import="java.util.*"%>
<%@page import="com.tsp.operation.model.*"%>
<%@page import="com.tsp.operation.model.beans.*"%>
<%@page import="com.tsp.operation.model.services.*"%>
<%@page import="com.tsp.util.*"%>
<%@include file="/WEB-INF/InitModel.jsp"%>
<%@ taglib uri="http://jakarta.apache.org/taglibs/input-0.90" prefix="input" %>

<html>

<head>
<title>Cotizaciones - Modificar</title>
<link href="<%= BASEURL %>/css/estilostsp.css" rel="stylesheet" type="text/css">
    <script type='text/javascript' src="<%=BASEURL%>/js/Validacion.js"></script>
    <script type='text/javascript' src="<%= BASEURL %>/js/boton.js"></script>
    <script type='text/javascript' src="<%= BASEURL %>/js/general.js"></script>
    <script src='<%=BASEURL%>/js/tools.js' language='javascript'></script>
    <script src='<%=BASEURL%>/js/date-picker.js'></script>
    <script src="<%= BASEURL %>/js/transferencias.js"></script>
    <script type="text/javascript" src="<%=BASEURL%>/js/buscarproveedor.js"></script>

	<%



			CotizacionService coserv = new CotizacionService();//091001
			MaterialesService prod = new MaterialesService();
			ArrayList ver = (ArrayList)prod.verTodos();
			int tam = ver.size();
			String spl="";

			Material mat = null;
			ArrayList array1 = prod.cargaTipoMats();//091207
			ArrayList array2 = prod.cargaTipoMano();//091207
			ArrayList array3 = prod.cargaTipoOtros();//091207
			String matx = "";//091207


	%>
        <script type="text/javascript">
            var cadenaT='';
            var cadenaT2='';
            var cadenaT3='';
            var texto = '';
            var listadoMat = '';
            var listadoMan = '';
            var listadoOt = '';
            var listadoMat2 = '';
            var listadoMan2 = '';
            var listadoOt2 = '';
            var listadoMat3 = '';
            var listadoMan3 = '';
            var listadoOt3 = '';
            var listadoMat4 = '';
            var listadoMan4 = '';
            var listadoOt4 = '';

            var last_used='M';

        </script>
        <%

		for(int i=0;i<array1.size();i++){
			matx = (String)array1.get(i);
			//System.out.println(".... "+matx);
	%>
        <script type="text/javascript">
        cadenaT = cadenaT + '<%=matx%>' + ';_;';
        </script>
	<%
		}
		for(int i=0;i<array2.size();i++){
			matx = (String)array2.get(i);
			//System.out.println("....2 "+matx);
	%>
	<script type="text/javascript">
        cadenaT2 = cadenaT2 + '<%=matx%>' + ';_;';
        </script>
	<%
		}
		for(int i=0;i<array3.size();i++){
			matx = (String)array3.get(i);
			//System.out.println("....3 "+matx);
	%>
	<script type="text/javascript">
        cadenaT3 = cadenaT3 + '<%=matx%>' + ';_;';
        </script>
	<%
		}
            ArrayList ds = prod.buscarPor(1);
            mat=null;
            for(int d=0;d<ds.size();d++){
		mat = (Material)ds.get(d);
                //System.out.println("linea de catsx: "+mat.getCategoria());

	%>
        <script type="text/javascript">
	listadoMat = listadoMat + '<%=mat.getDescripcion()%>' +' Medida: <%=mat.getMedida()%>' + ';_;';
	listadoMat2 = listadoMat2 + '<%=mat.getCodigo()%>' + ';_;';
	listadoMat3 = listadoMat3 + '<%=mat.getValor()%>' + ';_;';
	listadoMat4 = listadoMat4 + '<%=mat.getCategoria()%>' + ';_;';
        </script>
	<%
            }
            ArrayList ds2 = prod.buscarPor(2);
            for(int d=0;d<ds2.size();d++){
		mat = (Material)ds2.get(d);
        %>
        <script type="text/javascript">
	listadoMan = listadoMan + '<%=mat.getDescripcion()%>' +' Medida: <%=mat.getMedida()%>' + ';_;';
	listadoMan2 = listadoMan2 + '<%=mat.getCodigo()%>' + ';_;';
	listadoMan3 = listadoMan3 + '<%=mat.getValor()%>' + ';_;';
	listadoMan4 = listadoMan4 + '<%=mat.getCategoria()%>' + ';_;';
        </script>
    	<%
            }
            ArrayList ds3 = prod.buscarPor(3);
            for(int d=0;d<ds3.size();d++){
		mat = (Material)ds3.get(d);
	%>
	<script type="text/javascript">
	listadoOt = listadoOt + '<%=mat.getDescripcion()%>' +' Medida: <%=mat.getMedida()%>' + ';_;';
	listadoOt2 = listadoOt2 + '<%=mat.getCodigo()%>' + ';_;';
	listadoOt3 = listadoOt3 + '<%=mat.getValor()%>' + ';_;';
	listadoOt4 = listadoOt4 + '<%=mat.getCategoria()%>' + ';_;';
        </script>
        <%
            }
        %>

	<%int x=1;%>

	<script>

		var n=1;
		function agregarCelda(){
                n++;
                var texto = '<table>';
                texto = texto + '<tr class="fila">';
                texto = texto + '<td>Cantidad<input name="cantidad'+n+'" type="text" id="cantidad'+n+'" value="0" onFocus="instantValue('+n+')" onKeyUp="xperience(this.value,'+n+')"></td>';
                texto = texto + '<td>Valor<input type="text" id="precio'+n+'" name="precio'+n+'" value="0.00" readonly ><input name="valor_2'+n+'" type="hidden" id="valor_2'+n+'"></td>';
                texto = texto + '<td>Base Subtotal<input name="base'+n+'" type="text" id="base'+n+'" readonly value="0.00"><input name="base_2'+n+'" type="hidden" id="base_2'+n+'"></td>';
                texto = texto + '<td>Observacion<input name="nota'+n+'" type="text" id="nota'+n+'"></td>'
                texto = texto + '</tr>';
                texto = texto + '</table>';

                var tabla = document.getElementById("tablaform");

                var numrow = tabla.rows.length;

                var tbody = tabla.insertRow(numrow);

                var td1 = tbody.insertCell(0);

                td1.innerHTML = '<img id="imgmas'+n+'" name="imgmas'+n+'" src="<%=BASEURL%>/images/botones/iconos/mas.gif" onClick="agregarCelda()" width="12" height="12"> <img id="imgini<%=x%>" name="imgini'+n+'" src="<%=BASEURL%>/images/botones/iconos/menos1.gif" onClick="borrarDatosLista(this)"  width="12" height="12">';
                td1.align="left";
                td1.className="barratitulo";

                var td2 = tbody.insertCell(1);
                var x1='';
                x1 = '<table width="700"  border="0" align="left"><tr>';
                x1 = x1 + '<td width="102"><input name="radiobutton'+n+'" type="radio" value="M" onClick="cargarTipos(1,'+n+')"';

                if(last_used=='M') x1 = x1 + ' checked';
                x1 = x1 + '><span style="font-family:Verdana, Arial, Helvetica, sans-serif; font-size:12px ">Material</span></td>';
                x1 = x1 + '<td width="129"><input name="radiobutton'+n+'" type="radio" value="D" onClick="cargarTipos(2,'+n+')"';

                if(last_used=='D') x1 = x1 + ' checked';
                x1 = x1 + '><span style="font-family:Verdana, Arial, Helvetica, sans-serif; font-size:12px">Mano de obra</span></td>';
                x1 = x1 + '<td width="77"><input name="radiobutton'+n+'" type="radio" value="O" onClick="cargarTipos(3,'+n+')"';

                if(last_used=='O') x1 = x1 + ' checked';
                x1 = x1 + '><span style="font-family:Verdana, Arial, Helvetica, sans-serif; font-size:12px ">Otros</span></td>';
                x1 = x1 + '<td width="374"><select name="menutipos'+n+'" id="menutipos'+n+'" onChange="updateList(this.options[this.selectedIndex],'+n+');"><option value="-1" selected>Seleccione...</option></select></td></tr></table>';

                x1 = x1 + '<br><br><input name="textfieldx'+n+'" type="text" id="textfieldx'+n+'" onKeyUp="buscar(document.forma.producto'+n+',this);instantValue('+n+');xperience(document.forma.cantidad'+n+'.value,'+n+');">';
                x1 = x1 + '<br><select name="producto'+n+'" id="producto'+n+'" onChange="actualizarPrecio(this,'+n+');xperience(document.forma.cantidad'+n+'.value,'+n+');">';
                x1 = x1 + '<option value="-1" selected >Seleccione ...</option>';

                x1 = x1 + '</select><input name="codp'+n+'" id="codp'+n+'" type="hidden" value="0">';
                x1 = x1 + '<br><br>' + texto;

                td2.align="left";
                td2.className="barratitulo";
                td2.innerHTML = x1;
                td2.colSpan="2";

                if(last_used=='M') cargarTipos(1,n);
                if(last_used=='D') cargarTipos(2,n);
                if(last_used=='O') cargarTipos(3,n);
            }

	</script>

	<script type="text/javascript">
            function cargarTipos(valor,ind){
                var y=null;
                var arraytexto = null;
                if(valor >0){
                    switch(valor){
                        case 1:
                            arraytexto = cadenaT.split(";_;");
                        break;
                        case 2:
                            arraytexto = cadenaT2.split(";_;");
                        break;
                        case 3:
                            arraytexto = cadenaT3.split(";_;");
                        break;
                    }

                    var selection = document.getElementById("menutipos"+ind);
                    borradorOptions(selection);
                    for(i in arraytexto){
                        insertarOptions(arraytexto[i],selection);
                    }
                }
                else {
                    alert('No se selecciono nada (raro ... mas bien casi imposible ...)');
                }
            }

            function insertarOptions(texto,selection){
                if(texto!='') {
                    y = document.createElement('option');
                    y.text = texto;
                    y.value = texto;
                    try {
                        //Para navegadores buenos
                        selection.add(y,null);
                    }
                    catch(ex){
                        selection.add(y);//Tenia que ser Internet Exploiter ... funcionando a las malas...
                    }
                }
            }

            function borradorOptions(selection){
                var ind = 1;
                var tm = selection.length;
                var elim = tm-1;
                while(ind<tm){
                    selection.remove(elim);
                    elim = elim - 1;
                    ind = ind + 1;
                }
            }

            function updateList(optionDrop,indicef){
                //alert('Se ha presionado la categoria '+optionDrop.value);

                var opcionx = document.getElementsByName("radiobutton"+indicef);
                var arrayMat = null;
                var arrayMan = null;
                var arrayOt = null;
                var arrayMat2 = null;
                var arrayMan2 = null;
                var arrayOt2 = null;
                var arrayMat3 = null;
                var arrayMan3 = null;
                var arrayOt3 = null;
                var arrayMat4 = null;
                var arrayMan4 = null;
                var arrayOt4 = null;
                var texto='';

                for(var ix = 0;ix<opcionx.length;ix++){
                    if(opcionx[ix].checked){
                        if(opcionx[ix].value==null) texto='M';
                        else texto = opcionx[ix].value;
                        //alert('Se escogio tipo '+texto);
                    }
                }
                //alert('Tipo '+texto);

                if(texto=='M'){
                    borradorOptions(document.getElementById('producto'+indicef));
                    //alert('despues de borrar y antes del split');
                    arrayMat = listadoMat.split(';_;');
                    //alert('despues primer split long. '+arrayMat.length);
                    arrayMat2 = listadoMat2.split(';_;');
                    //alert('despues segundo split long. '+arrayMat.length);
                    arrayMat3 = listadoMat3.split(';_;');
                    arrayMat4 = listadoMat4.split(';_;');
                    //alert('Largo lista '+arrayMat4.length);
                    //alert('Option text '+optionDrop.value);
                    for(var indic=0;indic<arrayMat4.length;indic++){
                        if(arrayMat4[indic]==optionDrop.value){
                            insertarOptions2(arrayMat[indic],arrayMat3[indic],arrayMat2[indic],document.getElementById("producto"+indicef));
                            //alert('Hay '+arrayMat[indic]);
                        }

                    }
                    //alert('Final del if');
                }

                if(texto=='D'){
                    borradorOptions(document.getElementById('producto'+indicef));
                    arrayMan = listadoMan.split(';_;');
                    arrayMan2 = listadoMan2.split(';_;');
                    arrayMan3 = listadoMan3.split(';_;');
                    arrayMan4 = listadoMan4.split(';_;');
                    for(var indic2=0;indic2<arrayMan4.length;indic2++){
                        if(arrayMan4[indic2]==optionDrop.value){
                            insertarOptions2(arrayMan[indic2],arrayMan3[indic2],arrayMan2[indic2],document.getElementById("producto"+indicef));
                        }
                    }
                }

                if(texto=='O'){
                    borradorOptions(document.getElementById('producto'+indicef));
                    arrayOt = listadoOt.split(';_;');
                    arrayOt2 = listadoOt2.split(';_;');
                    arrayOt3 = listadoOt3.split(';_;');
                    arrayOt4 = listadoOt4.split(';_;');
                    for(var indic3=0;indic3<arrayOt4.length;indic3++){
                        if(arrayOt4[indic3]==optionDrop.value){
                            insertarOptions2(arrayOt[indic3],arrayOt3[indic3],arrayOt2[indic3],document.getElementById("producto"+indicef));
                        }
                    }
                }
                last_used=texto;
            }

            function insertarOptions2(texto,valor,id,selection){
                if(texto!='') {
                    y = document.createElement('option');
                    y.text = texto;
                    y.value = valor;
                    y.id = id;
                    try {
                        //Para navegadores buenos
                        selection.add(y,null);
                    }
                    catch(ex){
                        //Para IE
                        selection.add(y);//Tenia que ser Internet Exploiter ... funcionando a las malas...
                    }
                }
            }

	</script>
        <script type="text/javascript">
            
            function enviarP(cant){

                var tabla = document.getElementById("tablaform");
                var numrow = tabla.rows.length;
                var i=1;
                while(i<numrow-6){
                        i++;
                }
                //i = cant;

                document.getElementById("filas").value = cant;//filas a insertar?
                document.getElementById("contact").value = i;//filas a actualizar??
                //var contx = n + i;
                //alert("Filas enviadas: "+contx);
                //alert('Datos enviados');

                //forma.submit();//20100224
                var verificador = verificarCheck();//20100224
                var v2 = verificarSelect(cant);//20100224
                if(verificador==true){//20100224
                    //alert('Paso 1');
                    forma.submit();//20100224
                }
                else{
                    if(v2==true){//20100224
                        //alert('Paso 2');//20100224
                        forma.submit();//20100224
                    }
                    else{
                        alert('No puede anular todos los items sin insertar alguno ... ');//20100224
                    }
                }

            }

        </script>

<%

    String idaccion = "";
    idaccion = request.getParameter("idaccion")== null ? "901530":request.getParameter("idaccion");
    String[] datitos = new String[2];
    datitos = coserv.datosAccion(idaccion).split(";");
    if(datitos.length<1) System.out.println("datitos esta vacio");
    else {
       System.out.println("datitos : "+datitos[0]+" "+datitos[1]);
    }

    ArrayList detsCot = coserv.buscarDets(idaccion,"id_accion");
    int filasDetalles = detsCot.size();
    Cotizacion cdets = (Cotizacion)detsCot.get(0);
%>

<link href="../../../css/estilostsp.css" rel="stylesheet" type="text/css">

<script type="text/javascript">
    function verificarCheck(){
        var indicador = <%=filasDetalles%>;
        var ind = 1;
        var contador=0;
        var  verificador = true;
        while(ind <= indicador){
            if(document.getElementById('anular'+ind).checked==true){
                contador = contador + 1;
            }
            ind = ind +1;
        }
        if(contador==indicador){
            verificador = false;
        }
        return verificador;
    }

    function verificarSelect(ultimoInd){
        var verificador = false;
        if(document.getElementById('producto'+ultimoInd).selectedIndex>0){
            verificador = true;
        }
        return verificador;
    }

</script>

</head>

<body onResize="redimensionar()" onload = "/*xp();*/redimensionar();">

<div id="capaSuperior" style="position:absolute; width:100%; height:100px; z-index:0; left: 0px; top: 0px;">

 <jsp:include page="/toptsp.jsp?encabezado=Modificar Cotizacion Contratista"/>

</div>

<div id="capaCentral" style="position:absolute; width:100%; height:87%; z-index:0; left: 0px; top: 100px; overflow: scroll;">

<form id ="forma" name="forma" action="<%= CONTROLLER%>?estado=Cotizacion&accion=Modificar" method="post">

  <table width="100%" border="2"align="center">

    <tr>
      <td>
      <table width="100%" align="center" id="tablaformx" >
        <tr>
          <td colspan="3" align="left" class="subtitulo1">Ingresar datos</td>
          <td colspan="7" align="left" class="barratitulo"><img src="<%=BASEURL%>/images/titulo.gif"></td>
        </tr>
        <tr>

          <td colspan="2" align="left" class="fila" >Datos</td>

          <td colspan="7" valign="middle" class="letra">
              <input name="filas" type="hidden" id="filas" value="1">
              <input name="contact" type="hidden" id="contact" value="1">
              <input type="hidden" id="idaccion" value="<%=idaccion%>">
          </td>
          </tr>

        <tr>
          <td align="left" valign="middle" class="fila">Multiservicio</td>
          <td colspan="8" valign="middle" class="letra"><span class="subtitulos"><span class="fila">
              Id accion
              <input name="multiservicios" type="text" id="multiservicios" value="<%=idaccion%>" size="15" READONLY>
              Id Solicitud
              <input name="textfield" type="text" value="<%=datitos[0]%>" size="15" READONLY>
              Nombre cliente
              <input name="textfield2" type="text" value="<%=datitos[1]%>" size="100" READONLY>
          </span>
          </span>
          </td>
        </tr>

        <tr>

          <td align="left" valign="middle" class="fila" >Fecha</td>
          <td colspan="8" valign="middle" class="letra">
              <input name="fecha" type="text" id="fecha" value="<%=cdets.getFecha().substring(0,10)%>" READONLY>
              <!--
		  <img src="<%/*=BASEURL*/%>/images/cal.gif" width="16" height="16" align="absmiddle" style="cursor:pointer " onClick="if(self.gfPop)gfPop.fPopCalendar(document.forma.fecha);return false;" HIDEFOCUS>
              -->
          </td>
        </tr>

		<%
			if(coserv.existeCotizacion(idaccion)){
		%>

        <tr class="fila">

          <td colspan="3" align="left" class="subtitulo1">Productos</td>

          <td colspan="6" align="left" class="barratitulo"><img src="<%=BASEURL%>/images/titulo.gif"></td>
        </tr>

        <tr class="fila">

          <td width="70" align="left" class="subtitulo1">Item </td>
          <td width="500" align="left" class="subtitulo1">Producto actual </td>
          <td width="200" align="left" class="subtitulo1">Cantidad actual </td>
          <td align="left" class="subtitulo1">Nota </td>

        </tr><%int fils=1;%>

        <%
            Cotizacion cotnew = null;
            Material f = null;
            ArrayList rs = null;
            for(int point=0;point<filasDetalles;point++){
                cotnew = (Cotizacion)detsCot.get(point);
                rs = (ArrayList)prod.buscarPor(1,cotnew.getMaterial());
                if(rs.size()>0) f = (Material)rs.get(0);
                else rs = prod.buscarPorAnul(1, cotnew.getMaterial());//2010-01-30
                if (rs.size()>0) f = (Material)rs.get(0);//2010-01-30

        %>

        <tr class="fila">

          <td align="left" class="barratitulo">
            <input type="checkbox" id="anular<%=x%>" name="anular<%=x%>" value="<%=cotnew.getId()%>">Anular
          </td>

          <td align="left" class="barratitulo">
              <table border="0" align="left">
                  <tr>
                      <td>
                         <span style="font-family:Verdana, Arial, Helvetica, sans-serif; font-size:12px ">
                           Tipo:
                           <%
                           if(rs.size()>0) {
                               if(f.getTipo().equals("M")) out.print("Material");
                               if(f.getTipo().equals("D")) out.print("Mano de obra");
                               if(f.getTipo().equals("O")) out.print("Otros");
                           }
                           else out.print("Material actualizado");
                           %>
                         </span>
                      </td>
                  </tr>
                  <tr>
                      <td>
                         <span style="font-family:Verdana, Arial, Helvetica, sans-serif; font-size:12px ">
                            Categoria:
                            <%
                                if(rs.size()>0) out.print(f.getCategoria());
                                else out.print("Material actualizado");
                            %>
                         </span>
                      </td>
                  </tr>
                  <tr>
                      <td>
                         <span style="font-family:Verdana, Arial, Helvetica, sans-serif; font-size:12px ">
                         <%
                           if(f!=null) {
                               out.print(f.getDescripcion());
                           }
                           else {
                               out.print(cotnew.getMaterial());
                           }
                         %>
                         </span>
                      </td>
                  </tr>
              </table>
          </td>


          <td align="left" class="barratitulo"><script type="text/javascript">
                                                    document.write(formatNumber(<%=cotnew.getCantidad()%>));
                                                </script>
            </td>

          <td align="left" class="barratitulo">
              <span style="font-family:Verdana, Arial, Helvetica, sans-serif; font-size:12px ">
              <%=cotnew.getObservacion()%>
              </span>
              </td>
        </tr>

		<%
                        x++;
                    }
                %>

                <script type="text/javascript">
                    n=1;
                </script>
                <%
		}
		else {
		%>

		<script type="text/javascript">
			alert("No existe una cotizacion para esta accion...");
		</script>

		<%
			}
		%>
      </table>
      <br>
      <br>
      <table width="100%" border="0" id="tablaform">
        <tr class="fila">
          <td colspan="3" align="left" class="subtitulo1">Agregar productos </td>
          </tr>
        <tr class="fila">
          <td align="left" class="subtitulo1">Item(+/-)</td>
          <td align="left" class="subtitulo1" colspan="2">Producto</td>
        </tr>
         <tr class="fila">
            <td align="left" class="barratitulo">
                <img id="imgmas<%=x%>" name="imgmas<%=x%>" src="<%=BASEURL%>/images/botones/iconos/mas.gif" alt="Agregar fila" onClick="agregarCelda()" width="12" height="12">
                <img id="imgini<%=x%>" name="imgini<%=x%>" src="<%=BASEURL%>/images/botones/iconos/menos1.gif" alt="Eliminar fila" onClick="borrarDatosLista(this)"  width="12" height="12"></td>
            <td align="left" class="barratitulo" colspan="2">
                <table width="700"  border="0" align="left">
                    <tr>
                        <td width="102"><input name="radiobutton<%=x%>" type="radio" value="M" onClick="cargarTipos(1,<%=x%>)">
                            <span style="font-family:Verdana, Arial, Helvetica, sans-serif; font-size:12px ">Material</span>
                        </td>
                        <td width="129"><input name="radiobutton<%=x%>" type="radio" value="D" onClick="cargarTipos(2,<%=x%>)">
                            <span style="font-family:Verdana, Arial, Helvetica, sans-serif; font-size:12px ">Mano de obra</span>
                        </td>
                        <td width="77"><input name="radiobutton<%=x%>" type="radio" value="O" onClick="cargarTipos(3,<%=x%>)">
                            <span style="font-family:Verdana, Arial, Helvetica, sans-serif; font-size:12px ">Otros</span>
                        </td>
                        <td width="374">
                            <select name="menutipos<%=x%>" id="menutipos<%=x%>" onChange="updateList(this.options[this.selectedIndex],<%=x%>);">
                                <option value="-1" selected >Seleccione...</option>
                            </select>
                        </td>
                    </tr>
                </table>

                <br>
                <br>

                <input name="textfieldx<%=x%>" type="text" id="textfieldx<%=x%>" onKeyUp="buscar(document.forma.producto<%=x%>,this);instantValue(<%=x%>);xperience(document.forma.cantidad<%=x%>.value,<%=x%>);"><br>
                <select name="producto<%=x%>" id="producto<%=x%>" onChange="actualizarPrecio(this,<%=x%>); xperience(document.forma.cantidad<%=x%>.value,<%=x%>);">
                    <option value="-1" selected>Seleccione ...</option>

                </select>

                <br>
                <br>

                <table>
                    <tr class="fila">
                        <td>
                            Cantidad
                            <input name="cantidad<%=x%>" type="text" id="cantidad<%=x%>" value="0" onFocus="instantValue(<%=x%>)" onKeyUp="xperience(this.value,<%=x%>)">
                        </td>
                        <td>
                            Valor
                            <input type="text" id="precio<%=x%>" name="precio<%=x%>" value="0.00" readonly >
                            <input name="valor_2<%=x%>" type="hidden" id="valor_2<%=x%>">
                        </td>
                        <td>
                            Base Subtotal
                            <input name="base<%=x%>" type="text" id="base<%=x%>" readonly value="0.00">
                            <input name="base_2<%=x%>" type="hidden" id="base_2<%=x%>">
                        </td>
                        <td>
                            Observacion
                            <input name="nota<%=x%>" type="text" id="nota<%=x%>">
                        </td>
                    </tr>
                </table>

                <input name="codp<%=x%>" id="codp<%=x%>" type="hidden" value="0">
            </td>
        </tr>
      </table>
     </td>
    </tr>
  </table>

  <div align="center">
    <br>
	<%
		if(coserv.existeCotizacion(idaccion)){
	%>

    <img src="<%= BASEURL%>/images/botones/aceptar.gif" id="imgaceptar" name="imgaceptar" onClick="enviarP(<%=x%>)" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" style="cursor:pointer ">&nbsp;

	<%
		}
	%>

    <img src="<%= BASEURL%>/images/botones/salir.gif"  name="imgsalir"  onMouseOver="botonOver(this);" onClick="parent.close();" onMouseOut="botonOut(this);"  style="cursor:pointer ">

  </div>
</form>
</div>

<iframe width="188" height="166" name="gToday:normal:agenda.js:gfPop:plugins.js" id="gToday:normal:agenda.js:gfPop:plugins.js" src="<%=BASEURL%>/js/Calendario/ipopeng.htm" scrolling="yes" frameborder="0" style="visibility:visible; z-index:999; position:absolute; left:-500px; top:0px;">
</iframe>

</body>
</html>

<%
	String mens=(String)request.getAttribute("msg");
	if(mens!=null && !mens.equals("")){%>
	<script>
		alert('<%=mens%>');
	</script>

<%}%>