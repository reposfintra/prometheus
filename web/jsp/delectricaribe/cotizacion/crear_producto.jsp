<!--
- Autor : Ing. Rhonalf Martinez
- Date  : 25 de julio del 2009
- Copyrigth Notice : Fintravalores S.A.
-->
<%--
-@(#)
--Descripcion : P�gina JSP, que maneja el ingreso de ordenes de compra.
--%>
<%@page contentType="text/html"%>
<%@page session="true"%>
<%@page import="java.util.*"%>
<%@page import="com.tsp.operation.model.*"%>
<%@page import="com.tsp.operation.model.beans.*"%>
<%@page import="com.tsp.operation.model.services.*"%>
<%@page import="com.tsp.util.*"%>
<%@include file="/WEB-INF/InitModel.jsp"%>
<%@ taglib uri="http://jakarta.apache.org/taglibs/input-0.90" prefix="input" %>
<html>
    <head>
        <title>Productos - Crear</title>
        <link href="<%=BASEURL%>/css/estilostsp.css" rel="stylesheet" type="text/css">
        <script src="<%=BASEURL%>/js/boton.js"              type="text/javascript"></script>
        <script src="<%=BASEURL%>/js/prototype.js"          type="text/javascript"></script>

        <script type="text/javascript">

            function enviarForm(){

                var fields  = document.getElementsByName('categoria');

                if( (!fields[0].checked) && (!fields[1].checked) ){
                    alert('Marque alguna opcion del formulario');
                }
                else{
                    if( fields[1].checked ){
                        if ( ($('nuevaCat').value == '') || ( $('nuevaCat').value == null) ){
                            alert('Chequee los campos del formulario');
                        }
                        else{
                            alert($('nuevaCat').value);
                            document.forms["forma"].submit();
                        }
                    }
                    else{
                        alert($('catSel').options[$('catSel').selectedIndex].value);
                        document.forms["forma"].submit();
                    }
                }
            }

            function cambioTipo(url){
                var p = "opcion=consulta&tipo=" + $('tipo').value ;

                new Ajax.Request(
                    url,
                    {
                        method: 'get',
                        parameters: p,
                        onComplete: sendMessage
                    });
            }

            function sendMessage(response){
                $('selectDiv').innerHTML = response.responseText;
            }

        </script>

        <link href="../../../css/estilostsp.css" rel="stylesheet" type="text/css">
    </head>

    <body>
        <div id="capaSuperior" style="position:absolute; width:100%; height:100px; z-index:0; left: 0px; top: 0px;">
            <jsp:include page="/toptsp.jsp?encabezado=Crear productos"/>
        </div>
        <div id="capaCentral" style="position:absolute; width:100%; height:87%; z-index:0; left: -1px; top: 103px; overflow: scroll;">
            <%
                MaterialesService prod = new MaterialesService();
            %>
            <form id ="forma" name="forma" action="<%= CONTROLLER%>?estado=Materiales&accion=Crear" method="post">

                <table width="100%"  border="1">
                    <tr>
                        <td>
                            <table width="100%"  border="0" align="center">
                                <tr>
                                    <td class="subtitulo1">Ingresar Producto </td>
                                    <td class="barratitulo"><img src="<%=BASEURL%>/images/titulo.gif"></td>
                                </tr>
                                
                                <tr>
                                    <td class="fila">Nombre del producto * </td>
                                    <td class="fila"><input name="descripcion" type="text" id="descripcion" size="160" maxlength="500"></td>
                                </tr>
                                <tr>
                                    <td class="fila">Precio de venta </td>
                                    <td class="fila"><input name="precio" type="text" id="precio" value="0" size="20"></td>
                                </tr>
                                <tr>
                                    <td class="fila">Unidad de medida </td>
                                    <td class="fila">
                                        <select name="medida" id="medida">
                                            <option value="UNIDADES" selected>Unidades</option>
                                            <option value="METROS">Metros</option>
                                            <option value="KILOGRAMOS">Kilogramos</option>
                                            <option value="GALON">Galon</option>
                                            <option value="GLOBAL">Global</option>
                                            <option value="LITRO">Litro</option>
                                        </select>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="fila">Tipo</td>
                                    <td class="fila">
                                        <select name="tipo" id="tipo" onchange="cambioTipo('<%=CONTROLLER%>?estado=Materiales&accion=Crear');">
                                            <option value="M" selected>Material</option>
                                            <option value="D">Mano de obra</option>
                                            <option value="O">Otros</option>
                                        </select>
                                    </td>
                                </tr>
                                <tr>
                                    <td rowspan="3" class="fila">Categoria</td>
                                </tr>
                                <tr>
                                    <td class="fila">
                                        <div id="selectDiv"></div>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="fila">
                                        <input type="radio" name="categoria" value="2"> Nueva     <input type="text" id="nuevaCat" name="nuevaCat">
                                    </td>
                                </tr>
                                <tr>
                                    <td class="fila">* M&aacute;ximo 160 caracteres</td>
                                    <td class="fila">&nbsp;</td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
                <div align="center">
                    <br>
                    <img src="<%= BASEURL%>/images/botones/aceptar.gif" name="imgaceptar"   onClick="enviarForm();"     onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" style="cursor:hand ">
                    <img src="<%= BASEURL%>/images/botones/salir.gif"   name="imgsalir"     onClick="parent.close();"   onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" style="cursor:hand ">
                </div>
            </form>
        </div>
    </body>
    <iframe width="188" height="166" name="gToday:normal:agenda.js:gfPop:plugins.js" id="gToday:normal:agenda.js:gfPop:plugins.js" src="<%=BASEURL%>/js/Calendario/ipopeng.htm" scrolling="yes" frameborder="0" style="visibility:visible; z-index:999; position:absolute; left:-500px; top:0px;">
    </iframe>
</html>
<script type="text/javascript">
    cambioTipo('<%=CONTROLLER%>?estado=Materiales&accion=Crear');
</script>
<%
            String mens = (String) request.getAttribute("msg");
            if (!mens.equals("") && mens!=null) {%>
<script>
    alert('<%=mens%>');
</script>
<%}%>
