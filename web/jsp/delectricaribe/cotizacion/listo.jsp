<!--
- Autor : Ing. Rhonalf Martinez
- Date  : 25 de julio del 2009
- Copyrigth Notice : Fintravalores S.A.
-->
<%--
-@(#)
--Descripcion : P�gina JSP, que maneja el ingreso de ordenes de compra.
--%>
<%@page contentType="text/html"%>
<%@page session="true"%>
<%@page import="java.util.*"%>
<%@page import="com.tsp.operation.model.*"%>
<%@page import="com.tsp.operation.model.beans.*"%>
<%@page import="com.tsp.operation.model.services.*"%>
<%@page import="com.tsp.util.*"%>
<%@include file="/WEB-INF/InitModel.jsp"%>
<%@ taglib uri="http://jakarta.apache.org/taglibs/input-0.90" prefix="input" %>
<html>
<head>
<title>Productos - Ver todos</title>
<link href="<%= BASEURL %>/css/estilostsp.css" rel="stylesheet" type="text/css">
    <script type='text/javascript' src="<%=BASEURL%>/js/Validacion.js"></script>
    <script type='text/javascript' src="<%= BASEURL %>/js/boton.js"></script>
    <script type='text/javascript' src="<%= BASEURL %>/js/general.js"></script>
    <script src='<%=BASEURL%>/js/tools.js' language='javascript'></script>
    <script src='<%=BASEURL%>/js/date-picker.js'></script>
    <script src="<%= BASEURL %>/js/transferencias.js"></script>
	<script type="text/javascript" src="<%=BASEURL%>/js/buscarproveedor.js"></script>
	<script type="text/javascript">
		function enviarForm(){
			document.forma.submit();
		}
	</script>

<link href="../../../css/estilostsp.css" rel="stylesheet" type="text/css">
</head>

<body onresize="redimensionar()" onload = "imgProveedor();redimensionar();">
<div id="capaSuperior" style="position:absolute; width:100%; height:100px; z-index:0; left: 0px; top: 0px;">
 <jsp:include page="/toptsp.jsp?encabezado=Productos-Ver todos"/>
</div>
<div id="capaCentral" style="position:absolute; width:100%; height:87%; z-index:0; left: 0px; top: 100px; overflow: scroll;">


<%      
	   MaterialesService prod = new MaterialesService();
%>
<form id ="forma" name="forma" action="" method="post">

  <table width="785" border="2"align="center">
    <tr>
      <td>
	  <table width="100%" align="center" >
        <tr>
          <td colspan="5" align="left" class="subtitulo1">&nbsp;Ver datos</td>
          <td width="204" align="left" class="barratitulo"><img src="<%=BASEURL%>/images/titulo.gif"></td>
        </tr>
        <tr>
          <td width="82" align="left" class="fila" >Consecutivo</td>
          <td width="329" valign="middle" class="fila" >Descripcion del producto</td>
          <td width="67" valign="middle" class="fila" >Tipo</td>
          <td colspan="2" valign="middle" class="fila" >Medida</td>
		  <td align="left" class="fila" >Precio de venta </td>
          </tr>
		 <%
		 	ArrayList ver = (ArrayList)prod.verTodos();
			int tam = ver.size();
			Material mat = null;
		 	for(int i=0;i<tam;i++){
				mat=(Material)ver.get(i);
				if(mat!=null){
		 %>
        <tr>
          <td align="left" valign="top" class="letra"><%=mat.getCodigo()%></td>
          <td valign="middle" class="letra"><%=mat.getDescripcion()%></td>
          <td valign="middle" class="letra"><%=mat.getTipo()%></td>
          <td colspan="2" valign="middle" class="letra"><%=mat.getMedida()%></td>
		  <td align="left" class="letra" >$ <script type="text/javascript">document.write(formatNumber(<%=mat.getValor()%>));</script></td>
        </tr>
		<%
				}
			}
		%>
      </table>	  </td>
    </tr>
  </table>
  <div align="center">
    <br>
      <img src="<%= BASEURL%>/images/botones/salir.gif"  name="imgsalir"  onMouseOver="botonOver(this);" onClick="parent.close();" onMouseOut="botonOut(this);"  style="cursor:hand "></div>
</form>
</div>
</body>
<iframe width="188" height="166" name="gToday:normal:agenda.js:gfPop:plugins.js" id="gToday:normal:agenda.js:gfPop:plugins.js" src="<%=BASEURL%>/js/Calendario/ipopeng.htm" scrolling="yes" frameborder="0" style="visibility:visible; z-index:999; position:absolute; left:-500px; top:0px;">
</iframe>
</html>
<%
	String mens=(String)request.getAttribute("msg");
	if(!mens.equals("")){%>
	<script>
		alert('<%=mens%>');
	</script>
<%}%>