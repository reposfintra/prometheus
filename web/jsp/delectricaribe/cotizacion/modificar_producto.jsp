<!--
- Autor : Ing. Rhonalf Martinez
- Date  : 25 de julio del 2009
- Copyrigth Notice : Fintravalores S.A.
-->
<%--
-@(#)
--Descripcion : P�gina JSP, que maneja el ingreso de ordenes de compra.
--%>
<%@page contentType="text/html"%>
<%@page session="true"%>
<%@page import="java.util.*"%>
<%@page import="com.tsp.operation.model.*"%>
<%@page import="com.tsp.operation.model.beans.*"%>
<%@page import="com.tsp.operation.model.services.*"%>
<%@page import="com.tsp.util.*"%>
<%@include file="/WEB-INF/InitModel.jsp"%>
<%@ taglib uri="http://jakarta.apache.org/taglibs/input-0.90" prefix="input" %>

<html>
<%try{
	System.out.println("modificandoproduct.jsp");
%>
<head>
<title>Productos - Modificar</title>
<link href="<%= BASEURL %>/css/estilostsp.css" rel="stylesheet" type="text/css">
    <script type='text/javascript' src="<%=BASEURL%>/js/Validacion.js"></script>
    <script type='text/javascript' src="<%= BASEURL %>/js/boton.js"></script>
    <script type='text/javascript' src="<%= BASEURL %>/js/general.js"></script>
    <script src='<%=BASEURL%>/js/tools.js' language='javascript'></script>
    <script src='<%=BASEURL%>/js/date-picker.js'></script>
    <script src="<%= BASEURL %>/js/transferencias.js"></script>
	<script type="text/javascript" src="<%=BASEURL%>/js/buscarproveedor.js"></script>
	<script type="text/javascript">
		function enviarForm(){
			document.forma.submit();
		}

		/*function hacer(){
			var cambio = document.getElementById("precio2");
			document.getElementById("precio").value = cambio.value;
			document.getElementById("precio2").value = formatNumber(cambio.value);
			//alert(''+document.getElementById("precio").value+'');
		}*/
	</script>
        <script type="text/javascript">
            var arrayMats=null;
            var arrayMan=null;
            var arrayOts=null;
            var lista1="";
            var lista2="";
            var lista3="";
            var stest="";
        </script>
        <%
            MaterialesService mserv = new MaterialesService();
            ArrayList array1 = mserv.cargaTipoMats();
            ArrayList array2 = mserv.cargaTipoMano();
            ArrayList array3 = mserv.cargaTipoOtros();
            //System.out.println("Tamanos ... array1: "+array1.size()+" ,array2: "+array2.size()+" ,array3: "+array3.size()+" ");
            for(int i=0;i<array1.size();i++){
        %>
        <script type="text/javascript">
            lista1 = lista1 + "<%=array1.get(i)%>" + ";_;";
        </script>
        <%
            }
            for(int i=0;i<array2.size();i++){
        %>
        <script type="text/javascript">
            lista2 = lista2 + "<%=array2.get(i)%>" + ";_;";
        </script>
        <%
            }
            for(int i=0;i<array3.size();i++){
        %>
        <script type="text/javascript">
            lista3 = lista3 + "<%=array3.get(i)%>" + ";_;";
        </script>
        <%
            }
        %>
        <script type="text/javascript">
            arrayMats = lista1.split(";_;");
            arrayMan = lista2.split(";_;");
            arrayOts = lista3.split(";_;");
        </script>

        <script type="text/javascript">
            function recargarCategorias(textsel){
                var selectx = document.getElementById("categoria");
                var selectx2 = document.getElementById("tipo");
                var index = selectx2.selectedIndex;
                var tipo_mat = selectx2.options[index].value;
                borradorOptions(selectx);
                if(tipo_mat=="M"){
                    for(var i1=0;i1<arrayMats.length;i1++){
                        if(arrayMats[i1]==textsel) {
                            insertarOptions(arrayMats[i1],selectx,true);
                        }
                        else if(arrayMats[i1]==stest){
                            insertarOptions(arrayMats[i1],selectx,true);
                        }
                        else{
                            insertarOptions(arrayMats[i1],selectx,false);
                        }
                    }
                }
                if(tipo_mat=="D"){
                    for(var i2=0;i2<arrayMan.length;i2++){
                        if(arrayMan[i2]==textsel) {
                            insertarOptions(arrayMan[i2],selectx,true);
                        }
                        else if(arrayMan[i2]==stest){
                            insertarOptions(arrayMan[i2],selectx,true);
                        }
                        else{
                            insertarOptions(arrayMan[i2],selectx,false);
                        }
                    }
                }
                if(tipo_mat=="O"){
                    for(var i3=0;i3<arrayOts.length;i3++){
                        if(arrayOts[i3]==textsel) {
                            insertarOptions(arrayOts[i3],selectx,true);
                        }
                        else if(arrayOts[i3]==stest){
                            insertarOptions(arrayOts[i3],selectx,true);
                        }
                        else{
                            insertarOptions(arrayOts[i3],selectx,false);
                        }
                    }
                }
            }

            function insertarOptions(texto,selection,seleccionado){
                if(texto!='') {
                    y = document.createElement('option');
                    y.text = texto;
                    y.value = texto;
                    if(seleccionado==true) y.selected=seleccionado;
                    try {
                        //Para navegadores buenos
                        selection.add(y,null);
                    }
                    catch(ex){
                        selection.add(y);//Tenia que ser Internet Exploiter ... funcionando a las malas...
                    }
                }
            }

            function borradorOptions(selection){
                var ind = 0;
                var tm = selection.length;
                var elim = tm-1;
                while(ind<tm){
                    selection.remove(elim);
                    elim = elim - 1;
                    ind = ind + 1;
                }
            }
        </script>

<link href="../../../css/estilostsp.css" rel="stylesheet" type="text/css">
</head>

<body onResize="redimensionar();" onload = "redimensionar();">

<%

String id_product=request.getParameter("materiale");
if (id_product==null){id_product="";}

Material mat=model.negociosApplusService.getMaterial(id_product);

System.out.println("mat"+mat);

%>

<div id="capaSuperior" style="position:absolute; width:100%; height:100px; z-index:0; left: 0px; top: 0px;">
 <jsp:include page="/toptsp.jsp?encabezado=Modificar productos"/>
</div>
<div id="capaCentral" style="position:absolute; width:100%; height:87%; z-index:0; left: 0px; top: 100px; overflow: scroll;">


<%
	   MaterialesService prod = new MaterialesService();
%>
<form id ="forma" name="forma" action="<%= CONTROLLER%>?estado=Materiales&accion=Crear&opcion=modificar" method="post">

  <table width="100%"  border="1">
    <tr>
      <td>
	  <table width="100%"  border="0" align="center">
            <tr>
              <td class="subtitulo1">Modificar Producto </td>
              <td class="barratitulo"><img src="<%=BASEURL%>/images/titulo.gif">
              <input type="hidden" name="exid" value="<%=mat.getIdMaterial()%>">
              </td>
            </tr>

			<tr>
              <td class="fila">exConsecutivo </td>
              <td class="fila"><input name="exproducto" type="text" id="exproducto" value="<%=mat.getCodigo()%>" READONLY></td>
            </tr>

            <tr>
              <td class="fila">Consecutivo a asignar </td>
              <td class="fila"><input name="producto" type="text" id="producto" value="<%=prod.contarProductos()%>" READONLY></td>
            </tr>
            <tr>
              <td class="fila">Nombre del producto * </td>
              <td class="fila"><input name="descripcion" type="text" id="descripcion" size="160" maxlength="160" value="<%=mat.getDescripcion()%>"></td>
            </tr>
            <tr>
              <td class="fila">Precio de venta </td>
              <td class="fila"><input name="precio" type="text" id="precio" value="<%=mat.getValor()%>" size="20"></td>
            </tr>
            <tr>
              <td class="fila">Unidad de medida </td>
              <td class="fila">
                <select name="medida" id="medida">
                        <option value="UNIDADES" <%if (mat.getMedida().equals("UNIDADES")){out.print(" selected");}%>>Unidades</option>
                        <option value="METROS" <%if (mat.getMedida().equals("METROS")){out.print(" selected");}%>>Metros</option>
                        <option value="KILOGRAMOS" <%if (mat.getMedida().equals("KILOGRAMOS")){out.print(" selected");}%>>Kilogramos</option>
                        <option value="GALON" <%if (mat.getMedida().equals("GALON")){out.print(" selected");}%>Galon</option>
                        <option value="GLOBAL" <%if (mat.getMedida().equals("GLOBAL")){out.print(" selected");}%>Global</option>
                        <option value="LITRO" <%if (mat.getMedida().equals("LITRO")){out.print(" selected");}%>Litro</option>
                </select>
              </td>
            </tr>
            <tr>
              <td class="fila">Tipo</td>
              <td class="fila">
                  <select name="tipo" id="tipo" onchange="recargarCategorias(stest);">
                        <option value="M" <%if (mat.getTipo().equals("M")){out.print(" selected");}%>>Material</option>
                        <option value="D" <%if (mat.getTipo().equals("D")){out.print(" selected");}%>>Mano de obra</option>
                        <option value="O" <%if (mat.getTipo().equals("O")){out.print(" selected");}%>>Otros</option>

          	  </select>
              </td>
            </tr>
            <tr>
              <td class="fila">Categoria</td>
              <td class="fila">
                  <select name="categoria" id="categoria">
                      <option value="<%=mat.getCategoria()%>" selected="selected"><%=mat.getCategoria()%></option>
                      <option value="-1">Ninguna</option>
                  </select>
              </td>
            </tr>
            <tr>
              <td class="fila">* M&aacute;ximo 160 caracteres </td>
              <td class="fila">&nbsp;</td>
            </tr>
          </table>
	  </td>
    </tr>
  </table>
  <div align="center">
    <br>
    <img src="<%= BASEURL%>/images/botones/aceptar.gif"  name="imgaceptar" onClick="enviarForm();" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" style="cursor:hand ">&nbsp;<img src="<%= BASEURL%>/images/botones/salir.gif"  name="imgsalir"  onMouseOver="botonOver(this);" onClick="parent.close();" onMouseOut="botonOut(this);" style="cursor:hand ">
  </div>
</form>
</div>
</body>
<iframe width="188" height="166" name="gToday:normal:agenda.js:gfPop:plugins.js" id="gToday:normal:agenda.js:gfPop:plugins.js" src="<%=BASEURL%>/js/Calendario/ipopeng.htm" scrolling="yes" frameborder="0" style="visibility:visible; z-index:999; position:absolute; left:-500px; top:0px;">
</iframe>
<script type="text/javascript">
  var sind = document.getElementById("categoria").selectedIndex;
  var tsel = document.getElementById("categoria").options[sind].value;
  stest = tsel;
  recargarCategorias(tsel);
</script>
<%
}catch(Exception ee){
	System.out.println("error en jsp"+ee.toString()+"__"+ee.getMessage());

}
%>
</html>
<%
	String mens=(String)request.getAttribute ("msg");
	if(mens!=null && !mens.equals("")){%>
	<script>
		alert('<%=mens%>');
	</script>
<%}%>