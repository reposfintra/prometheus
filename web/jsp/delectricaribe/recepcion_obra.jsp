<html >
<%@ page session   ="true"%>
<%@ page errorPage ="/error/ErrorPage.jsp"%>

<%@ include file   ="/WEB-INF/InitModel.jsp"%>
<%@taglib uri="http://jakarta.apache.org/taglibs/input-0.90" prefix="input" %>
<%@ page import    ="java.util.Vector"%>
<%@ page import    ="com.tsp.operation.model.beans.BeanGeneral"%>

<link href="<%=BASEURL%>/css/estilostsp.css"    rel="stylesheet" type="text/css">

<script src="<%=BASEURL%>/js/boton.js"          type="text/javascript"></script>
<script src="<%=BASEURL%>/js/Validacion.js"     type='text/javascript'></script>
<script src="<%=BASEURL%>/js/tools.js"          type="text/javascript"></script>
<script src="<%=BASEURL%>/js/date-picker.js"    type="text/javascript"></script>
<script src="<%=BASEURL%>/js/transferencias.js" type="text/javascript"></script>
<script src="<%=BASEURL%>/js/prototype.js"      type="text/javascript"></script>

<!-- Las siguientes librerias CSS y JS son para el manejo de    DIVS dinamicos.-->

<link href="<%=BASEURL%>/css/default.css"       rel="stylesheet" type="text/css">
<link href="<%=BASEURL%>/css/mac_os_x.css"      rel="stylesheet" type="text/css">
<script src="<%=BASEURL%>/js/effects.js"        type="text/javascript"></script>
<script src="<%=BASEURL%>/js/window.js"         type="text/javascript"></script>
<script src="<%=BASEURL%>/js/window_effects.js" type="text/javascript"></script>
<!-- Las siguientes librerias CSS y JS son para el manejo del
    calendario(jsCalendar).
-->
<link rel="stylesheet" type="text/css" href="<%=BASEURL%>/css/jCal/jscal2.css" />
<link rel="stylesheet" type="text/css" href="<%=BASEURL%>/css/jCal/border-radius.css" />

<script type="text/javascript" src="<%=BASEURL%>/js/jCal/jscal2.js"></script>
<script type="text/javascript" src="<%=BASEURL%>/js/jCal/lang/es.js"></script>

<head >
    <script>
         var generar=0;
    </script>
</head>

<%
boolean sw_aceptar=true;
String solicitud=request.getParameter("solicitud");

%>

<div align="center">

    <br><br><br><br>
      
    <table align="center" border="1">
        <tr class="fila">
            <td>
                &nbsp;&nbsp;Nombre de Cliente&nbsp;&nbsp;
            </td>
            <td>
                &nbsp;&nbsp;<%=request.getParameter("nomcli")%>
            </td>
        </tr>
        <tr class="fila">
            <td>
                &nbsp;&nbsp;Consecutivo Oferta&nbsp;&nbsp;
            </td>
            <td>
                &nbsp;&nbsp;<%=request.getParameter("consecutivo")%>
            </td>
        </tr>
        <tr class="fila">
            <td>
               &nbsp;&nbsp;Num Os&nbsp;&nbsp;
            </td>
            <td>
                &nbsp;&nbsp;<%=request.getParameter("num_os")%>
            </td>
        </tr>
        <tr class="fila">
            <td>
               &nbsp;&nbsp;Solicitud&nbsp;&nbsp;
            </td>
            <td>
                &nbsp;&nbsp;<%=solicitud%>
            </td>
        </tr>
        </table>
        <table align="center">
        <tr>&nbsp;<br></tr>
        <tr class="fila">
            <td align="center">
                <strong>&nbsp;Id Accion&nbsp;</strong>
            </td>
            <td align="center">
                <strong>&nbsp;Contratista&nbsp;</strong>
            </td>
            <td align="center">
                <strong>&nbsp;Fecha Finalización&nbsp;</strong>
            </td>
            <td align="center">
                <strong>&nbsp;Fecha Interventoría&nbsp;</strong>
            </td>
            <td align="center" colspan="2">
                <strong>&nbsp;Observación&nbsp;</strong>
            </td>
            <td align="center">
                <strong>&nbsp;Acciones&nbsp;</strong>
            </td>
            <td align="center">
                <strong> &nbsp;Fecha Registro&nbsp;</strong>
            </td>
            <td align="center">
                <strong> &nbsp;Usuario&nbsp;</strong>
            </td>
        </tr>
        <%
        BeanGeneral bg=model.negociosApplusService.getDatRecepObra();
        //ystem.out.println("bg"+bg);
        Vector  dat_recep_obra=bg.getVec();
        for (int i=0;i<dat_recep_obra.size();i++){
            BeanGeneral datos=(BeanGeneral)dat_recep_obra.get(i);
            if (!(datos.getValor_06().equals("080"))){
                sw_aceptar=false;
            }
            %>
            
            <tr class="filagris">
                <td>
                    &nbsp;&nbsp;<%=datos.getValor_03()%>&nbsp;
                    <input type="hidden" name="accione" value="<%=datos.getValor_03()%>">
                </td>
                <td>
                    &nbsp;&nbsp;<%=datos.getValor_02()%>&nbsp;
                </td>
                <td>
                    &nbsp;<input id="calendar-field<%=i%>" name="fecFin" size="10" value="<%=datos.getValor_01()%>"  readonly><button id="calendar-trigger<%=i%>">...</button>&nbsp;
                    <br>
                    <%if (i==0){%>
                        <center>
                        <img src="<%=BASEURL%>/images/botones/iconos/abajo.gif" title="Generalizar" align="center" width="20" height="20" style="cursor:pointer;cursor:hand" onclick="sameFecFin()">
                        </center>
                    <%}%>
                </td>
                <td>
                    &nbsp;<input id="calendar-field2<%=i%>" name="fecInterventor"  size="10" value="<%=datos.getValor_05()%>"  readonly><button id="calendar-trigger2<%=i%>">...</button>&nbsp;
                    <br>
                    <%if (i==0){%>                        
                        <center>
                        <img src="<%=BASEURL%>/images/botones/iconos/abajo.gif" title="Generalizar" align="center" width="20" height="20" style="cursor:pointer;cursor:hand" onclick="sameFec()">
                        </center>
                    <%}%>
                </td>
                <td colspan="2">
                    &nbsp;<textarea  name="observ" cols="22" wrap="virtual" rows="6"><%=datos.getValor_08()%></textarea>&nbsp;
                </td>
                <td width="200">
                    &nbsp;<%=datos.getValor_04()%>&nbsp;
                </td>
                <td nowrap>
                    &nbsp;<%=datos.getValor_09()%>&nbsp;
                </td>
                <td>
                    &nbsp;<%=datos.getValor_10()%>&nbsp;
                </td>
            </tr>
            <%
        }

        %>
       
      </table>
       
    <br><br>
    <%if (sw_aceptar){%>
        <img src="<%=BASEURL%>/images/botones/aceptar.gif" height="21" title="Aceptar" onclick="ajaRecepObra('<%=solicitud%>');" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" style="cursor:hand">
    <%}%>
    &nbsp;&nbsp;&nbsp;<img src='<%=BASEURL%>/images/botones/salir.gif'      style='cursor:hand'    title='Salir...'      name='i_salir'       onclick='salirx();'           onMouseOver='botonOver(this);' onMouseOut='botonOut(this);'>
    
</div>

<div align="center" name="loading_div" id="loading_div" style="visibility: hidden">
    Cargando...
</div>

    <br>
<script>
   
    <%
    //ystem.out.println("dat_recep_obra"+dat_recep_obra);
    for (int i=0;i<dat_recep_obra.size();i++){
        //ystem.out.println("i"+i);
    %>
            Calendar.setup({
                inputField : "calendar-field<%=i%>",
                trigger    : "calendar-trigger<%=i%>",
                onSelect   : function() { this.hide() }
            });

            Calendar.setup({
                    inputField : "calendar-field2<%=i%>",
                    trigger    : "calendar-trigger2<%=i%>",
                    onSelect   : function() { this.hide() }
                });


    <%
        //ystem.out.println("ii"+i);
    }%>

    
    function ajaRecepObra(id){

            //alert('Solicitud en proceso..id_solicitud:'+id+"_generar"+generar);

            if(generar==0){
                var cad = 'opcion=aceptarrecibirObra&solicitud='+id;//+'&fecha='+$('calendar-field').value;

                //
                var arrayfecFin=document.getElementsByName("fecFin");
                var arrayfecInterventor=document.getElementsByName("fecInterventor");
                var arrayobserv=document.getElementsByName("observ");
                var arrayaccione=document.getElementsByName("accione");

                for  (j=0;j<arrayobserv.length;j++){
                    cad=cad+"&fecFin="+arrayfecFin[j].value;
                    cad=cad+"&fecInterventor="+arrayfecInterventor[j].value;
                    cad=cad+"&observ="+arrayobserv[j].value;
                    cad=cad+"&accione="+arrayaccione[j].value;
                }

                //alert("fecFin0: "+arrayfecFin[0].value);

                new Ajax.Request(    '<%=CONTROLLER%>?estado=Negocios&accion=Applus',
                {
                    method: 'post',
                    parameters: cad,//parameters: {fecFin:valx,fecInterventor:valy},
                    onLoading: whenLoading,
                    onComplete: ajaxRecepFinish
                });
            }
            else{
                alert('Solicitud en proceso.');
            }

    }

        function whenLoading(){
            switchDiv();
            generar = 1;
        }

        function ajaxRecepFinish(response){
            alert(response.responseText);
            switchDiv();
            generar = 0;
            salirx();
        }

        function switchDiv(){
            if($('loading_div').style.visibility == 'visible'){
                $('loading_div').style.visibility = 'hidden';
            }
            else{
                $('loading_div').style.visibility = 'visible';
            }
        }

        function sameFec(){
            var arrayfecInterventor=document.getElementsByName("fecInterventor");
            var fecInterventor0=arrayfecInterventor[0].value;
            for  (j=1;j<arrayfecInterventor.length;j++){
                arrayfecInterventor[j].value=fecInterventor0;
            }
        }
        function sameFecFin(){
            var arrayfecFin=document.getElementsByName("fecFin");
            var fecFin0=arrayfecFin[0].value;
            for  (j=1;j<arrayfecFin.length;j++){
                arrayfecFin[j].value=fecFin0;
            }
        }

        function salirx(){
            var winx = parent.Windows.getWindow("recepobr");
            //alert("mira:"+winx.getId());
            winx.destroy();
        }        


</script>

</html>
