<%-- 
    Document   : ActualizacionSaldosCartera
    Created on : 13/08/2018, 11:44:51 AM
    Author     : mcamargo
--%>

<%@page import="java.util.ArrayList"%>
<%@page import="com.tsp.operation.model.services.SeguimientoCarteraService"%>
<%@page import="com.tsp.operation.model.beans.Usuario"%>
<%@page contentType="text/html" pageEncoding="ISO-8859-1"%>
<%@ page import="com.tsp.operation.model.beans.CmbGeneralScBeans"%>
<%
    Usuario usuario = (Usuario) session.getAttribute("Usuario");
    
    SeguimientoCarteraService rqservice= new SeguimientoCarteraService(usuario.getBd());
    ArrayList listaCombo =  rqservice.GetComboGenerico("SQL_OBTENER_UNIDAD_NEGOCIO","id","descripcion",usuario.getLogin());
 %>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
        <title>Actualizacion Saldos Cartera</title>
        <link type="text/css" rel="stylesheet" href="./css/jquery/jquery-ui/jquery-ui-2.css"/>
        <link type="text/css" rel="stylesheet" href="./css/jquery/jquery.jqGrid/ui.jqgrid.css"/>        
        <script type="text/javascript" src="./js/jquery/jquery-ui/jquery.min.js"></script>
        <script type="text/javascript" src="./js/jquery/jquery-ui/jquery.ui.min.js"></script>            
        <script type="text/javascript" src="./js/jquery/jquery.jqGrid/js/grid.locale-es.js"></script>
        <script type="text/javascript" src="./js/jquery/jquery.jqGrid/js/jquery.jqGrid.min.js"></script>
        <link type="text/css" rel="stylesheet" href="./css/buttons/botonVerde.css"/>
        <link type="text/css" rel="stylesheet" href="./css/popup.css"/>
        <link href="./css/style_azul.css" rel="stylesheet" type="text/css">
        <script type="text/javascript" src="./js/ReporteGarantias.js"></script>  
        
       
    </head>
    <body>
        <div id="capaSuperior" style="position:absolute; width:100%; height:100px; z-index:0; left: 0px; top: 0px;">
             <jsp:include page="/toptsp.jsp?encabezado=Actualizacion Saldos Cartera"/>
        </div>
        <div id="capaCentral" style="position:absolute; width:100%; height:100%; z-index:0; left: 0px; top: 100px; ">
            <center>
                <div id="div_saldos" class="frm_search" style="width:580px;">  
                    <table cellspacing="10">
                        <tr>
                            <td>
                                 <label for="periodo">Fecha Inicial:</label><br>
                                 <input type="text" id="fechaini" name="fechaini" style="height: 20px;color: #070708;" readonly="" >
                            </td>
                            <td>
                                 <label for="periodo">Fecha Final:</label><br>
                                 <input type="text" id="fechafin" name="fechafin" style="height: 20px;color: #070708;" readonly="" >
                            </td>
                            <td>
                                 <label for="periodo">Unidad Negocio:</label><br>
                            <select name="unidad_negocio"  style="font-size: 12px;" id="unidad_negocio">
                                <option value="" selected>< -Escoger- ></option><%
                                for (int i = 0; i < listaCombo.size(); i++) {
                                    CmbGeneralScBeans CmbContenido = (CmbGeneralScBeans) listaCombo.get(i);%>
                                 <option value="<%=CmbContenido.getIdCmb() %>"><%=CmbContenido.getDescripcionCmb() %></option><%
                                }%>
                            </select>
                           
                            </td>
                            
                            <td> <label for=""></label><br>
                                <button id="bucar_negocios_nuevos" class="ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only" 
                                        role="button" aria-disabled="false">
                                    <span class="ui-button-text">Buscar</span>
                                </button>
                            </td>
                        </tr>
                        
                    </table>
                </div>
                <br><br>
                <table id="tabla_saldos_cartera"></table>
                <div id="page_tabla_saldos_cartera"></div>
            </center>

            <div id="divSalidaEx" title="Exportacion" style=" display: block" >
                <p  id="msjEx" style=" display:  none"> Espere un momento por favor...</p>
                <center>
                    <img id="imgloadEx" style="position: relative;  top: 7px; display: none " src="./images/cargandoCM.gif"/>
                </center>
                <div id="respEx" style=" display: none"></div>
            </div> 
        <div id="info"  class="ventana" >
            <p id="notific"></p>
        </div>

        </div>
    </body>
</html>
