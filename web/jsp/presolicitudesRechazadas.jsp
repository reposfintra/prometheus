<%-- 
    Document   : presolicitudesRechazadas
    Created on : Dec 20, 2018, 1:55:41 PM
    Author     : jdbc
--%>

<%@page contentType="text/html" pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="ISO-8859-1">
        <link href="./css/popup.css" rel="stylesheet" type="text/css">
        <link type="text/css" rel="stylesheet" href="./css/jquery/jquery-ui/jquery-ui.css" />
        <link type="text/css" rel="stylesheet" href="./css/TransportadorasApi.css " />
        <script type="text/javascript" src="./js/jquery/jquery.jqGrid/js/jquery.min.js"></script>
        <script type="text/javascript" src="./js/jquery-ui-1.8.5.custom.min.js"></script>   
        <script type="text/javascript" src="./js/jquery/jquery.jqGrid-4.7.0/js/grid.locale-es.js"></script>
        <script type="text/javascript" src="./js/jquery/jquery.jqGrid-4.7.0/js/jquery.jqGrid.min.js"></script>
        <script type="text/javascript" src="./js/jquery/jquery.jqGrid-4.7.0/js/jquery.tabletojson.js"></script>    
        <script type="text/javascript" src="./js/presolicitudesRechazadas.js"></script> 
        <title>PRESOLICITUDES RECHAZADAS</title>
    </head>
    <body>
        <div id="capaSuperior" style="position:absolute; width:100%; height:100px; z-index:0; left: 0px; top: 0px;">
            <jsp:include page="/WEB-INF/toptsp.jsp?encabezado=PRESOLICITUDES RECHAZADAS"/>
        </div>
    <center>
        <div style="position: relative;top: 120px;">
            <table id="tabla_Presolicitudes" ></table>
            <div id="pager"></div>
        </div>
        <div id="info"  class="ventana" >
            <p id="notific"></p>
        </div>        
    </center>
</body>
</html>
