var anterior;

function buscar(lista,campo){
	var texto = campo.value.toLowerCase();
	for( var i=0; i<lista.length; i++ ){
		var v = lista[i].innerText.toLowerCase();
		if ( v.indexOf(texto) >= 0 ){
			lista.selectedIndex = i;
			break;
		}
	}
}



var seleccionada;
var colorSeleccionada;
var anteriorMouse;
var colorAnteriorMouse;

function cambiarColor(fila){
	if ( fila == seleccionada ){
		return;
	}
	if ( fila == anteriorMouse ){
		colorSeleccionada = 'oldSelection';
	}
	else {
		colorSeleccionada = fila.className;
	}
	//fila.bgColor = "mouseOverColor";
	fila.className = 'currentSelection';
	if ( seleccionada != null ){
		seleccionada.className = colorSeleccionada;
	}
	seleccionada = fila;
	anteriorMouse = null;
}





function cambiarColorMouse(fila){
	if ( fila == anteriorMouse ){
		return;
	}
	if ( fila == seleccionada ){
		fila.className = 'mouse_over';
		if ( anteriorMouse != null ){
			anteriorMouse.className = 'select';
		}
		anteriorMouse = seleccionada;
	}
	else if ( seleccionada != null && anteriorMouse == seleccionada ){
		seleccionada.className = 'currentSelection';
		fila.className = 'mouse_over';
		anteriorMouse = fila;	
	}
	else {
		var aux = fila.className;
		fila.className = 'mouse_over';
		if ( anteriorMouse != null ){
			anteriorMouse.className = colorAnteriorMouse;
		}
		colorAnteriorMouse = aux;
		anteriorMouse = fila;	
	}
}

function llamarFuncion(origen){
	if ( origen == "top" ){
		var frames = window.parent.frames;
		for( var i=0; i<frames.length; i++ ){
			if ( frames[i].name == "centro" ){
				alert(frames[i].document.forma.campo.value);
			}
		}
	}
	else {
		alert(document.forma.campo.value);
	}
}

function setPageBounds(left, top, width, height)
{
  return ("top=" + top + ",left=" + left + ",width=" +
          width + ",height=" + height);
}

function abrirPaginaNueva(url, nombrePagina,ancho, alto)
{
  var wdth = ancho;
  var hght = alto;
  var lf = screen.height * 0.075;
  var tp = screen.height * 0.075;
  var options = "menubar=yes,scrollbars=no,resizable=yes,status=no," +
                setPageBounds(lf, tp, wdth, hght);
  var hWnd = window.open(url, nombrePagina, options);
  if ( (document.window != null) && (!hWnd.opener) ){
    hWnd.opener = document.window;
  }
}

function abrirLink(rootUrl)
{
   abrirPaginaNueva(rootUrl, "wreptra",800, 220);
}

function redimensionar(){
    capaCentral.style.height= document.body.offsetHeight-102;
}


function validarRangoDeFechas(inicio,fin){
    // Validar rango de fechas.
    if ( inicio != "" || fin != "" ){
        if( (inicio != "" && fin == "") ||
                (inicio == "" && fin != "") ){
          alert("El rango de fechas est� incompleto.");
          return false;
        }else if( !isFechaIniLeThanFechaFin(inicio,fin) ){
          alert("La fecha inicial debe ser MENOR O IGUAL que la final.");
          return false;
        }
    }
    return true;
}

function dateToMillisecs(tStamp){
  var dtFlds = tStamp.split("-");
  var Year = dtFlds[0];
  var Month = ((dtFlds[1].length == 1)? "0" + dtFlds[1] : dtFlds[1] );
  var Day = ((dtFlds[2].length == 1)? "0" + dtFlds[2] : dtFlds[2] );
  return Date.parse(Month + "/" + Day + "/" + Year);
}

function isFechaIniLeThanFechaFin(inicio,fin){
  var fecini = dateToMillisecs(inicio);
  var fecfin = dateToMillisecs(fin);
  return (fecini <= fecfin);
}
