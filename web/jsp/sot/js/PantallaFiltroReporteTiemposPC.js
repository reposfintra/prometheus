function validarFormularioRTPC(form,inicio,fin,controller){
    var url = form.action+"&fechaini="+inicio+"&fechafin="+fin+"&formatoSalida="+form.formatoSalida.value;
    if ( validarRangoDeFechas( inicio, fin ) ){
        if ( form.formatoSalida.value == "web" ){
            abrirPaginaNueva(url,"repTPC",800,600);
        }
        else if ( form.formatoSalida.value == "excel" ) {
            form.submit();
        }
    }
}