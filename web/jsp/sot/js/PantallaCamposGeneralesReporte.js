function seleccionarTodos(){
  var check;
  var lstChecks = document.getElementsByName("fields");
  for( var i=0; i<lstChecks.length; i++ ){
    check = lstChecks[i];
    check.checked = document.reportFieldsFrm.selectAll.checked;
  }
}


function llenarCombosOrden(n){
    var lstChecks = document.getElementsByName("fields");
    for( var i=0; i<lstChecks.length; i++ ){
        var check = lstChecks[i];
        var combo = document.getElementById("orden" + check.value);
        llenarCombo(combo,n);
    }
    ejecutarComando("1",document.reportFieldsFrm);
}

function ejecutarComando(valor,frm){
    actualizarAutomaticamente = false;
    if ( valor == "1" ){
        var seleccionados = 0;
        for( var i = 0; i < frm.elements.length; i++ ){
            var combo = frm.elements[i];
            if ( combo.name.substring(0,5) == "orden" && !combo.disabled){
                combo.selectedIndex = seleccionados;
                seleccionados++;
            }
        }
    }
    else if ( valor == "2" ){
        var seleccionados = 0;
        for( var i = frm.elements.length-1; i >= 0  ; i-- ){
            var combo = frm.elements[i];
            if ( combo.name.substring(0,5) == "orden" && !combo.disabled){
                combo.selectedIndex = seleccionados;
                seleccionados++;
            }
        }
    }
    else if ( valor == "3" ){
        actualizarAutomaticamente = true;
    }
}

function agregarCampo(url){
  //alert(url);
  var wdth = 500;
  var hght = 300;
  var lf = screen.height * 0.075;
  var tp = screen.height * 0.075;
  var options = "scrollbars=no,resizable=no,closeable=no,status=no," +
  setPageBounds(lf, tp, wdth, hght);
  var hWnd = window.open(url, 'wAgregarCampo', options);
  if ( (document.window != null) && (!hWnd.opener) )
    hWnd.opener = document.window;
}

function enviarFormNuevoCampo(){
    var frm = document.forma;
    var nombre = frm.nombre.value;
    var titulo = frm.titulo.value;
    var grupo = frm.grupos.value;
    if ( nombre == '' ){
        alert("Debe digitar el nombre del campo");
        return;
    }
    else if ( titulo == '' ){
        alert("Debe digitar el t�tulo del campo");
        return;
    }
    frm.submit();
}

function validarOrden()
{
  var checkBoxFld;
  var idx = 0;
  var lstChecks = document.getElementsByName("fields");
  var numeros = new Array();
  for( idx = 0; idx < lstChecks.length; idx++ ){
    checkBoxFld = lstChecks[idx];
    if( checkBoxFld.checked ){
        var combo = document.getElementById("orden"+checkBoxFld.value);
        if ( combo.value == null || combo.value == '' ){
            alert("Debe escojer un valor para la casilla orden del campo "+checkBoxFld.value);
            return false;
        }
        var res = agregarItem(combo.value,numeros);
        if ( res ){
            alert("No pueden haber 2 o mas campos con el mismo orden!");
            return false;
        }
    } 
  }
  return true;
}

function enviarFormCampos(param){
    var form = document.reportFieldsFrm;
    if ( param == 'deleteFields' ){
        var lstChecks = document.getElementsByName("fields");
        var hayChecked = false;
        for( idx = 0; idx < lstChecks.length; idx++ ){
            checkBoxFld = lstChecks[idx];
            if( checkBoxFld.checked ){
                hayChecked = true;
                break;
            }
        }
        if ( !hayChecked ){
            alert("Para eliminar uno o m�s registros debe seleccionar las casillas de\nverficaci�n ubicadas a la izquierda de cada fila de la tabla");
            return;
        }
        var res = confirm("Confirma que desea eliminar el(los) registro(s) seleccionados?");
        if ( res == 0 ){
            return;
        }
    }
    if ( param == 'saveCamposGral' && !validarOrden() ){
        return;
    }
    form.cmd.value = param;
    form.submit();
}


