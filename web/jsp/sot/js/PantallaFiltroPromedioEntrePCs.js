function validarSeleccion(form,origen,destino){
    if ( origen == '' ){
        alert("Debe seleccionar un puesto de control de origen");
        return false;
    }
    else if ( destino == '' ){
        alert("Debe seleccionar un puesto de control de destino");
        return false;
    }
    form.nombreOrigen.value = form.origen[form.origen.selectedIndex].innerText;
    form.nombreDestino.value = form.destino[form.destino.selectedIndex].innerText;
    form.submit();
}


function enviarFormDetalles( form, origen, destino ){
    var formato = form.formatoSalida.value;
    var url = form.action + "&formatoSalida="+formato+"&nombreOrigen=" + origen + "&nombreDestino=" + destino;
    if ( formato == "excel" ){
        location.href = url;
    }
    else {
        abrirPaginaNueva(url, "repProm", 700, 700);
    }
}