var fechainical;
var fechafincal;

function today()
{
  var dt = new Date();
  var year  = dt.getFullYear();
  var month = dt.getMonth() + 1;
  var day   = dt.getDate();
  return (year + "-" + month + "-" + day);
}

function dateToMillisecs(tStamp)
{
  var dtFlds = tStamp.split("-");
  var Year = dtFlds[0];
  var Month = ((dtFlds[1].length == 1)? "0" + dtFlds[1] : dtFlds[1] );
  var Day = ((dtFlds[2].length == 1)? "0" + dtFlds[2] : dtFlds[2] );
  return Date.parse(Month + "/" + Day + "/" + Year);
}

function isFechaIniLeThanFechaFin()
{
  var fecini = dateToMillisecs(document.returnsFrm.fechaini.value);
  var fecfin = dateToMillisecs(document.returnsFrm.fechafin.value);
  return (fecini <= fecfin);
}

function setNombreCliente()
{
  var clSelect = document.returnsFrm.cliente;
  document.returnsFrm.nombreCliente.value = clSelect[clSelect.selectedIndex].innerText;
}

function setPageBounds(left, top, width, height)
{
  return ("top=" + top + ",left=" + left + ",width=" +
          width + ",height=" + height);
}

function abrirPaginaDevoluciones(url, nombrePagina)
{
  var wdth = screen.width - screen.width * 0.15;
  var hght = screen.height - screen.height * 0.15;
  var lf = screen.height * 0.075;
  var tp = screen.height * 0.075;
  var options = "menubar=yes,scrollbars=no,resizable=yes," +
                setPageBounds(lf, tp, wdth, hght);
  var hWnd = window.open(url, nombrePagina, options);
  if ( (document.window != null) && (!hWnd.opener) )
    hWnd.opener = document.window;
}


function abrirPagina2(url, nombrePagina)
{
  var wdth = screen.width - screen.width * 0.15;
  var hght = screen.height - screen.height * 0.15;
  var lf = screen.height * 0.075;
  var tp = screen.height * 0.075;
  var options = " scrollbars=yes,resizable=yes,  "+
                " width=400, height=200, top= 200, left=300  ";
  var hWnd = window.open(url, nombrePagina, options);
  if ( (document.window != null) && (!hWnd.opener) )
    hWnd.opener = document.window;
}



function displayReturns(baseUrl)
{
  var params = "&cmd=show" +
               "&cliente=" + document.returnsFrm.cliente.value +
               "&nombreCliente=" + document.returnsFrm.nombreCliente.value +
               "&fechaini=" + document.returnsFrm.fechaini.value +
               "&fechafin=" + document.returnsFrm.fechafin.value +
               "&tipoDevolucion=" + document.returnsFrm.tipoDevolucion.value +
               "&nroPedido=" + document.returnsFrm.nroPedido.value +
               "&origen=" + document.returnsFrm.origen.value +
               "&todoOrigen=" + (document.returnsFrm.todoOrigen.checked?"true":"false") +
               "&destino=" + document.returnsFrm.destino.value +
               "&todoDestino=" + (document.returnsFrm.todoDestino.checked?"true":"false") +
               "&displayInExcel=" + document.returnsFrm.displayInExcel.value;

   if(document.returnsFrm.displayInExcel.value!='File')
      abrirPaginaDevoluciones(baseUrl + params, "wrptdevol");
   else
      window.location.href(baseUrl + params);
}





function enviarFormularioDevoluciones(baseUrl)
{
  var frm = document.returnsFrm;
  var isDataRight = false;
  setNombreCliente();
  if( frm.nroPedido.value == "" )
  {
    // Validar rango de fechas de remesas.
    if ( frm.fechaini.value != "" || frm.fechafin.value != "" )
    {
      if( (frm.fechaini.value != "" && frm.fechafin.value == "") ||
          (frm.fechaini.value == "" && frm.fechafin.value != "") )
      {
        alert("El rango de fechas est� incompleto.");
      }else if( !isFechaIniLeThanFechaFin() ){
        alert("La fecha inicial debe ser MENOR O IGUAL que la final.");
      }else{
        isDataRight = true;
      }
    }else{
      isDataRight = true;
    }
    
    if( isDataRight ) displayReturns(baseUrl);
  }else{
    displayReturns(baseUrl);
  }
}