// JavaScript Document
<!-- flooble Color Picker header start --> 

// Color Picker Script from Flooble.com
// For more information, visit 
//	http://www.flooble.com/scripts/colorpicker.php
// Copyright 2003 Animus Pactum Consulting inc.
//---------------------------------------------------------
     var perline = 16;
     var urlPaginaPaleta = "";
     var divSet = false;
     var curId;
     var colorLevels = new Array();
     colorLevels[0] = '0';
     colorLevels[1] = '1';
     colorLevels[2] = '2';
     colorLevels[3] = '3';
     colorLevels[4] = '4';
     colorLevels[5] = '5';
     colorLevels[6] = '6';
     colorLevels[7] = '7';
     colorLevels[8] = '8';
     colorLevels[9] = '9';
     colorLevels[10] = 'A';
     colorLevels[11] = 'B';
     colorLevels[12] = 'C';
     colorLevels[13] = 'D';
     colorLevels[14] = 'E';
     colorLevels[15] = 'F';
     var colorArray = Array();
     var ie = false;
     var nocolor = 'none';
     if (document.all) { 
        ie = true; 
        nocolor = ''; 
     }
     
     function getObj(id) {
	if (ie) { 
           return document.all[id]; 
        }
	else {	
             return document.getElementById(id);	
        }
     }

     function addColor(r, g, b) {
     	var red = colorLevels[r];
     	var green = colorLevels[g];
     	var blue = colorLevels[b];
     	addColorValue(red, green, blue);
     }

     function addColorValue(r, g, b) {
     	colorArray[colorArray.length] = '#' + r + r + g + g + b + b;
     }
     
     function setColor(color) {
     	var link = getObj(curId);
     	var field = getObj(curId + 'field');
     	var picker = getObj('colorpicker');
     	field.value = color;
     	if (color == '') {
	     	link.style.background = nocolor;
	     	link.style.color = nocolor;
	     	color = nocolor;
     	} else {
	     	link.style.background = color;
	     	link.style.color = color;
	    }
     	picker.style.display = 'none';
	eval(getObj(curId + 'field').title);
     }
        
     function setDiv(id,onclickPaleta) {     
     	if (!document.createElement) { return; }
        var elemDiv = document.createElement('div');
        if (typeof(elemDiv.innerHTML) != 'string') { return; }
        genColors();
        elemDiv.id = 'colorpicker';
	elemDiv.style.position = 'absolute';
        elemDiv.style.display = 'none';
        elemDiv.style.border = '#000000 1px solid';
        elemDiv.style.background = '#FFFFFF';
        elemDiv.style.zIndex = 999;
        elemDiv.innerHTML = '<span><br>' + getColorTable() + 
                             '<center><button onclick="abrirPaletaCompleta(\''+id+'\',\''+onclickPaleta+'\')">Mas colores...</button></center></span>';
        document.body.appendChild(elemDiv);
        divSet = true;
     }
     
     function pickColor(id) {
     	if (!divSet) { setDiv(id,urlPaginaPaleta); }
     	var picker = getObj('colorpicker');     	
        if (id == curId && picker.style.display == 'block') {
           picker.style.display = 'none';
           return;
        }
     	curId = id;
     	var thelink = getObj(id);
     	picker.style.top = getAbsoluteOffsetTop(thelink) + 20;
     	picker.style.left = getAbsoluteOffsetLeft(thelink);     
	picker.style.display = 'block';
     }
     
     function genColors() {
        addColorValue('0','0','0');
        addColorValue('1','1','1');
        addColorValue('2','2','2');
        addColorValue('3','3','3');
        addColorValue('4','4','4');
        addColorValue('5','5','5');
        addColorValue('6','6','6');
        addColorValue('7','7','7');
        addColorValue('8','8','8');
        addColorValue('9','9','9');
        addColorValue('A','A','A');
        addColorValue('B','B','B');
        addColorValue('C','C','C');
        addColorValue('D','D','D');
        addColorValue('E','E','E');
        addColorValue('F','F','F');                                
			
        for (a = colorLevels.length-1; a >= 0; a--)
			addColor(a,a,15);
        for (a = 0,b=colorLevels.length-1; a < colorLevels.length; a++,b--)
			addColor(a,a,b);

        for (a = colorLevels.length-1; a >= 0; a--)
			addColor(a,15,0);
        for (a = 0; a < colorLevels.length; a++)
			addColor(a,8,a);
			
        for (a = 0; a < colorLevels.length; a++)
			addColor(a,0,0);
        for (a = 0; a < colorLevels.length; a++)
			addColor(7,a,a);
			
			
        for (a = 0; a < colorLevels.length; a++)
			addColor(15,a,0);
        for (a = 0; a < colorLevels.length; a++)
			addColor(9,9,a);
			
        for (a = 0; a < colorLevels.length; a++)
			addColor(0,a,a);
        for (a = 0; a < colorLevels.length; a++)
			addColor(a,7,7);

        for (a = 0; a < colorLevels.length; a++)
			addColor(a,0,a);			
        for (a = 0; a < colorLevels.length; a++)
			addColor(7,a,7);
			
       	return colorArray;
     }
     function getColorTable() {
         var colors = colorArray;
      	 var tableCode = '';
         tableCode += '<table border="0" cellspacing="1" cellpadding="1">';
         for (i = 0; i < colors.length; i++) {
              if (i % perline == 0) { tableCode += '<tr>'; }
              tableCode += '<td bgcolor="#000000"><a style="outline: 1px solid #000000; color: ' 
              	  + colors[i] + '; background: ' + colors[i] + ';font-size: 10px;" title="' 
              	  + colors[i] + '" href="javascript:setColor(\'' + colors[i] + '\');">&nbsp;&nbsp;&nbsp;</a></td>';
              if (i % perline == perline - 1) { tableCode += '</tr>'; }
         }
         if (i % perline != 0) { tableCode += '</tr>'; }
         tableCode += '</table>';
      	 return tableCode;
     }
     function relateColor(id, color) {
     	var link = getObj(id);
     	if (color == '') {
	     	link.style.background = nocolor;
	     	link.style.color = nocolor;
	     	color = nocolor;
     	} else {
	     	link.style.background = color;
	     	link.style.color = color;
	    }
	
     }
     function getAbsoluteOffsetTop(obj) {
     	var top = obj.offsetTop;
     	var parent = obj.offsetParent;
     	while (parent != document.body) {
     		top += parent.offsetTop;
     		parent = parent.offsetParent;
     	}
     	return top;
     }
     
     function getAbsoluteOffsetLeft(obj) {
     	var left = obj.offsetLeft;
     	var parent = obj.offsetParent;
     	while (parent != document.body) {
     		left += parent.offsetLeft;
     		parent = parent.offsetParent;
     	}
     	return left;
     }


<!-- flooble Color Picker header end -->      

