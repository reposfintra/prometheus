function validarSeleccion(){
    var form = document.forma;
    var lstChecks = document.getElementsByName("select");
    var hayChecked = false;
    for( idx = 0; idx < lstChecks.length; idx++ ){
        checkBoxFld = lstChecks[idx];
        if( checkBoxFld.checked ){
            hayChecked = true;
        }
    }
    if ( !hayChecked ){
        alert("Para eliminar uno o m�s registros debe seleccionar las casillas de\nverficaci�n ubicadas a la izquierda de cada fila de la tabla");
        return;
    }
    var res = confirm("Confirma que desea eliminar el(los) registro(s) seleccionados?");
    if ( res != 0 ){
        form.submit();
    }
}