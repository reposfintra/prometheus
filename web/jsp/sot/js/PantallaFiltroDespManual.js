function validarDespManual(){
      if ( document.despmanual.placa.value == "" )
      {
         alert("DEBE DIGITAR UNA PLACA");
         document.despmanual.placa.focus();
         return false;
      }

      if (document.despmanual.cedcon.value == "" )
      {
         alert("DEBE DIGITAR LA CEDULA DEL CONDUCTOR");
         document.despmanual.cedcon.focus();
         return false;
      }

      if (document.despmanual.nomcon.value == "" )
      {
         alert("DEBE DIGITAR EL NOMBRE DEL CONDUCTOR");
         document.despmanual.nomcon.focus();
         return false;
      }
       
      if (document.despmanual.via.value == "" )
      {
         alert("DEBE SELECCIONAR UNA VIA");
         document.despmanual.via.focus();
         return false;
      }

      if (document.despmanual.cliente.value == "" )
      {
         alert("DEBE SELECCIONAR UN CLIENTE");
         document.despmanual.cliente.focus();
         return false;
      }

      return true;
}

function validarDesp(){
   if ( document.busdespmanual.planilla.value == "" )
   {
         alert("DEBE DIGITAR UNA PLANILLA");
         document.busdespmanual.planilla.focus();
         return false;
   }

   return true;  
}



