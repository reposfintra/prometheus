function buscarCaravana(){
   var frm = document.frmfndcara;
   if (frm.caravana.value == ""){
      alert("DEBE DIGITAR UN NUERO DE CARAVANA");
      frm.caravana.focus();
      return false;
   }
   return true;
}

function addPlanilla(){
   var frm = document.frmaddcara;
   var frm1 = document.frmfndcara;
   if (frm.planilla.value == ""){
      alert("DEBE DIGITAR UNA PLANILLA");
      frm.planilla.focus();
      return false;
   }
   if (frm.caravana.value == ""){
      alert("DEBE BUSCAR UNA CARAVANA PRIMERO");
      frm1.caravana.focus();
      return false;
   }
   return true;
}

function vacio(n){
   var frm = document.forms[n];
   if (frm.motivo.value == ""){
      alert("DEBE DIGITAR EL MOTIVO POR EL QUE VA A ELIMINAR LA PLANILLA");
      frm.motivo.focus(); 
   }
   else{
      frm.submit();
   }
}

function soloDigitosKeyPress()
{
  var esDigito = true;
  if (event.keyCode < 48 || event.keyCode > 57)
    esDigito = false;
  return esDigito;
}
