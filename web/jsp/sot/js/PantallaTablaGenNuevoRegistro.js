function enviarFormNuevoRegistro(){
    var form = document.forma;
    var tipo = form.table_type.value;
    var codigo = form.table_code.value;
    var referencia = form.referencia.value;
    var descripcion = form.descripcion.value;
    if ( tipo == null || tipo == "" ){
        alert("Debe digitar el tipo de la tabla");
        form.type.focus();
        return;
    }
    else if ( codigo == null || codigo == "" ){
        alert("Debe digitar el c�digo del registro");
        form.codigo.focus();
        return;
    }
    else if ( descripcion == null || descripcion == "" ){
        alert("Debe digitar la descripcion del registro");
        form.descripcion.focus();
        return;
    }
    form.submit();     
}