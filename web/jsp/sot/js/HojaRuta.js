function ConfirmCreaHRutaManual(){
   var frm = document.frmplamanual;
   var val = frm.confirm[0].checked;
   if (val){
      window.location = "controller?estado=HRutaManual&accion=GetInfo";
   }
   else{
      window.location = "controller?estado=HReporte&accion=Search";
   }
}

//VALIDA EL FORMULARIO DE PANTALLAHOJARUTAGETINFO.JSP
function validarfrmpla(){
   var frm = document.forms[0];
   var pla = frm.planilla.value;
   if (pla == ""){
      alert("DIGITE UN NUMERO DE PLANILLA");
      return false;
   }
   return true;
}

function validarManual(){
      var sw = true;
      
      if (document.despmanual.planilla.value == "")
      {
         alert("Digite n�mero de la planilla");
         document.despmanual.planilla.focus();
         sw = false;
         return sw;
      }

      if (document.despmanual.placa.value == "")
      {
         alert("Digite placa del veh�culo");
         document.despmanual.placa.focus();
         sw = false;
         return sw;
      }

      if (document.despmanual.cedcon.value == "")
      {
         alert("Digite la cedula del conductor");
         document.despmanual.cedcon.focus();
         sw = false;
         return sw;
      }
    
      return sw;
}

function enviar(){
     var frm = document.forms[0];
     var params;
     params = "&planilla=" + frm.planilla.value +
              "&conductor=" + frm.conductor.value +
              "&usuario=" + frm.usuario.value +
              "&cia=" + frm.cia.value +
              "&placa_vehiculo=" + frm.placa_vehiculo.value +
              "&placa_unidad_carga=" + frm.placa_unidad_carga.value +
              "&contenedores=" + frm.contenedores.value +
              "&precinto=" + frm.precinto.value +
              "&ruta=" + frm.ruta.value +
              "&vacio=" + frm.vacio.value +
              "&comentarios=" + frm.comentarios.value;
     abrirPagina("controller?estado=HReporte&accion=Create&cmd=show"+params, "HOJA_REPORTE_" + frm.planilla.value);
     window.location="controller?estado=HReporte&accion=Search";
}

function setPageBounds(left, top, width, height)
{
  return ("top=" + top + ",left=" + left + ",width=" +
          width + ",height=" + height);
}

function abrirPagina(url, nombrePagina)
{
  var wdth = screen.width - screen.width * 0.4;
  var hght = screen.height - screen.height * 0.5;
  var lf = screen.width * 0.2;
  var tp = screen.height * 0.25;
  var options = "status=yes,menubar=no,scrollbars=yes,resizable=yes," +
                setPageBounds(lf, tp, wdth, hght);
  var hWnd = window.open(url, nombrePagina, options);
  if ( (document.window != null) && (!hWnd.opener) )
    hWnd.opener = document.window;
}

function cargar()
{
  window.opener.document.forms[0].submit(); 
}
