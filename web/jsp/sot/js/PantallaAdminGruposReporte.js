var gr = new GruposReporte();

function cargarFormulario(){
    for( var i=0; i<gr.grupos.length; i++ ){
        var grupo = gr.grupos[i];
        if ( grupo.nombre != "" ){
            document.forma.listaGrupos[i] = new Option(""+grupo.nombre,""+grupo.nombre);
        }
        else {

            var nombresCampos = (new VBArray(grupo.campos.Keys())).toArray();	// Get the keys.
            for (var j in nombresCampos){
                var campo = nombresCampos[j];
                var titulo = grupo.campos(campo);
                document.forma.todos[j] = new Option(titulo,campo);
            }
        }
    }
}

function Grupo(nombre, color){
    this.nombre = nombre;
    this.color = color;
    this.campos = new ActiveXObject("Scripting.Dictionary");
    this.agregarCampo = agregarCampo;
    this.eliminarCampo = eliminarCampo;
}

function GruposReporte(){
    this.grupos = new Array();
    this.agregarGrupo = agregarGrupo;
    this.buscarGrupo = buscarGrupo;
    this.existeCampo = existeCampo;
}

function agregarGrupo(nombre,color){
    var g = new Grupo(nombre,color);
    this.grupos[this.grupos.length] = g;
    return g;
}

function agregarCampo( nombre, titulo ){
    this.campos.Add(nombre,titulo);
}

function eliminarCampo(nombre){
    this.campos.Remove(nombre);
}

function buscarGrupo(nombreGrupo){
    for( var i=0; i<this.grupos.length; i++ ){
        var grupo = this.grupos[i];
        if ( grupo.nombre == nombreGrupo ){
            return grupo;
        }
    }
    return null;
}

function existeCampo(nombreCampo){
    for( var i = 0; i < this.grupos.length; i++ ){
        var grupo = this.grupos[i];
        if ( grupo.campos.Exists(nombreCampo) ){
            return true;
        }
    }
    return false;
}

function cargarDatosGrupo(){
    var form = document.forma;
    var nombreGrupo = form.listaGrupos.value;
    var grupo = gr.buscarGrupo(nombreGrupo);
    if ( grupo != null ){
        form.nombreGrupo.value = grupo.nombre;
        form.colorGrupofield.value = grupo.color;
        relateColor('colorGrupo', grupo.color);
        for( var j=document.forma.camposGrupo.length-1; j>=0; j-- ){
            form.camposGrupo.remove(j);
        }
        
        var nombresCampos = (new VBArray(grupo.campos.Keys())).toArray();	// Get the keys.
        for (var i in nombresCampos){
            var campo = nombresCampos[i];
            var titulo = grupo.campos(campo);
            form.camposGrupo[i] = new Option(titulo,campo);
        }

        for( var i=form.todos.length - 1; i >= 0; i--  ){
            if ( gr.existeCampo(form.todos[i].value) ){
                form.todos.remove(i);
            }
        }     
    }
}

function aplicarCambios(mostrarMensaje){
    var form = document.forma;
    var grupo = gr.buscarGrupo(form.listaGrupos.value);
    if ( grupo != null && form.listaGrupos.selectedIndex != -1 ){
        grupo.nombre = form.nombreGrupo.value;
        var opcion = form.listaGrupos[form.listaGrupos.selectedIndex];
        opcion.value = grupo.nombre;
        opcion.text = grupo.nombre;
        grupo.color = form.colorGrupofield.value;
        var nombresCampos = (new VBArray(grupo.campos.Keys())).toArray();// Get the keys.
        var i;
        
        if ( form.camposGrupo.length == 0 ){
            var res = confirm("Usted ha quitado todos los campos del grupo '"+grupo.nombre+"'. �Desea eliminar este grupo?");
            if ( res != 0 ){
                eliminarGrupo(false);
            }
        }
        
        for (i in nombresCampos){
            var campo = nombresCampos[i];
            grupo.eliminarCampo(campo);
        }
            
        var nuevosCampos = form.camposGrupo.options;
        for( i=0; i<nuevosCampos.length; i++ ){
            var campo = nuevosCampos[i];
            grupo.agregarCampo(campo.value,campo.text);
        }
        if ( mostrarMensaje ){
            alert("Los cambios fueron aplicados correctamente");
        }
    }
    else {
        if ( mostrarMensaje ){
            alert("No se pudieron aplicar los cambios");
        }
    }
}

function eliminarGrupo(mostrarConfirmacion){
    var nombreGrupo = document.forma.listaGrupos.value;
    if ( nombreGrupo == null || nombreGrupo == '' ){
        alert("Para eliminar un grupo debe seleccionar uno de la lista");
        return;
    }
    if ( mostrarConfirmacion ){
        var res = confirm("�Confirma que desea eliminar el grupo '"+nombreGrupo+"'?");
        if ( res == 0 ){
            return;
        }
    }
    for( var i in gr.grupos ){
        if ( gr.grupos[i].nombre == nombreGrupo ){
            var grupo = gr.grupos[i];
            var nombresCampos = (new VBArray(grupo.campos.Keys())).toArray();
            for( var j=0; j<nombresCampos.length; j++ ){
                var campo = nombresCampos[j];
                var titulo = grupo.campos(campo);
                document.forma.todos[j] = new Option(titulo,campo);
            }
            gr.grupos.splice(i,1);
            document.forma.listaGrupos.remove(i);
            document.forma.nombreGrupo.value = "";
            document.forma.colorGrupofield.value = "";
            relateColor('colorGrupo', '');
            for( var j=document.forma.camposGrupo.length-1; j>=0; j-- ){
                document.forma.camposGrupo.remove(j);
            }
            break;
        }
    }    
}

function cargarNuevoGrupo(nombreGrupo){
    document.forma.listaGrupos[document.forma.listaGrupos.length] = new Option(nombreGrupo,nombreGrupo);
    document.forma.listaGrupos.selectedIndex = document.forma.listaGrupos.length -1;
    var form = document.forma;
    var nombreGrupo = form.listaGrupos.value;
    var grupo = gr.buscarGrupo(nombreGrupo);
    if ( grupo != null ){
        form.nombreGrupo.value = grupo.nombre;
        form.colorGrupofield.value = grupo.color;
        relateColor('colorGrupo', grupo.color);
        for( var j=document.forma.camposGrupo.length-1; j>=0; j-- ){
            form.camposGrupo.remove(j);
        }
    }
    //cargarDatosGrupo();
}


function ventanaAgregarGrupo(url){
    abrirVentanaNuevoGrupo(url,'nuevoGrupo',650,330);
}

function abrirVentanaNuevoGrupo(url, nombrePagina,ancho, alto)
{
  var wdth = ancho;
  var hght = alto;
  var lf = screen.height * 0.075;
  var tp = screen.height * 0.075;
  var options = "menubar=no,scrollbars=no,resizable=no,status=yes," +
                setPageBounds(lf, tp, wdth, hght);
  var hWnd = window.open(url, nombrePagina, options);
  if ( (document.window != null) && (!hWnd.opener) ){
    hWnd.opener = document.window;
  }
  
}


function abrirPaletaCompleta(idCampo,url){
      var wdth = 370;
      var hght = 240;
      var lf = screen.height * 0.475;
      var tp = screen.height * 0.475;
      var options = "center:yes ;status:no; help:no; dialogWidth:368px; dialogHeight:273px;";
      var res = window.showModalDialog(url, 'wpaleta', options);
      getObj(idCampo+"field").value = res;
      setColor(res);
      /*if ( (document.window != null) && (!hWnd.opener) ){
        hWnd.opener = document.window;
      }*/
}


function cerrarDialogoPaleta(color){
    window.returnValue = color;
    window.close();    
}




function enviarNuevoGrupo(form){
    var nombre = form.nombreGrupo.value;
    var color = form.colorGrupo2field.value;
    if ( nombre == null || nombre == "" ){
        alert("Debe digitar un nombre para el nuevo grupo");
        return;
    }
    else if ( color == null || color == "" ){
        alert("Debe escojer un color para el nuevo grupo");
        return;
    }
    opener.gr.agregarGrupo(nombre,color);
    opener.cargarNuevoGrupo(nombre);
    window.close();    
}

function enviarFormulario(url){
    aplicarCambios(false);
    if ( document.forma.todos.length > 0 ){
        alert("Todos los campos del reporte deben pertenecer a algun grupo");
        return;
    }
    var parametros = "";
    
    var grupos = gr.grupos;
    //alert("tama�o grupos = "+grupos.length);
    var i;
    for( i in grupos ){
        var grupo = grupos[i];
        parametros += grupo.nombre + "," + grupo.color + ":";
        //alert((i+1)+"� parametros = "+parametros);
        var nombresCampos = (new VBArray(grupo.campos.Keys())).toArray();// Get the keys.
        //alert("numero de campos = "+nombresCampos.length);
        var j;
        for (j in nombresCampos){
            var campo = nombresCampos[j];
            //alert("campo["+j+"] = "+campo);
            parametros += campo +"-";
        }
        parametros += ";";
    }
    while( parametros.indexOf("#") >= 0 ){
        parametros = parametros.replace("#","_");
    }
    //alert("parametros = "+parametros);
    location.href = url + "&codigoReporte="+document.forma.codigoReporte.value+"&grupos=" + parametros;
}

function soloLetrasGrupos(e) {
        var isIE = document.all?true:false;
        var isNS = document.layers?true:false;
        var key = (isIE) ? window.event.keyCode : e.which;
        var obj = (isIE) ? event.srcElement : e.target;
        //alert(key);
	// para permitir las comas (apayares 17-03-2006)
	var isNum = ( (key < 48 && key != 32) || 
                      (key > 57 && key < 65) || (key > 90 &&  key < 97) || (key > 122 && !(key == 225 || key == 233 || key == 237 || key == 243 || key == 250 || key == 193 || key == 201 || key == 205 || key == 211 || key == 218) )) ? true:false;
        window.event.keyCode = (isNum && isIE) ? 0:key;
	e.which = (!isNum && isNS) ? 0:key;
	return (isNum);
}        



