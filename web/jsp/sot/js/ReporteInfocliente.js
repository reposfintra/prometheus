function setPageBounds(left, top, width, height)
{
  return ("top=" + top + ",left=" + left + ",width=" +
          width + ",height=" + height);
}

function abrirPaginaDetalle(url, nombrePagina)
{
  var wdth = 770;
  var hght = screen.height - screen.height * 0.50;
  var lf = screen.height * 0.075;
  var tp = screen.height * 0.075;
  var options = "menubar=yes,scrollbars=yes,resizable=yes,status=yes," +
                setPageBounds(lf, tp, wdth, hght);
  var hWnd = window.open(url, nombrePagina, options);
  if ( (document.window != null) && (!hWnd.opener) ){
    hWnd.opener = document.window;
  }
}

function verDetalleAduana(rootUrl)
{
   window.status = "Generando reporte de aduana. Por favor espere...";
   abrirPaginaDetalle(rootUrl, "wrepaduana");

}
function openWindow(url){
    window.open(url,'NONE','status=yes,resizable=yes,width=700,height=590')
}
