var fechainical;
var fechafincal;
var placaTrailerParser = /[A-Z0-9]{6,}/;
var planillaParser = /([A-Z]|[0-9]){6}/;

function trim(str)
{
  var startIdx = 0, endIdx = str.length - 1;
  while( endIdx > 0 && str.charAt(endIdx) == ' ' )
    endIdx -= 1;
  while( startIdx <= endIdx && str.charAt(startIdx) == ' ' )
    startIdx += 1;
  var trimmedString = "";
  if( startIdx < endIdx + 1 )
    trimmedString = str.substring(startIdx, endIdx + 1);
  return trimmedString;
}

function today()
{
  var dt = new Date();
  var year  = dt.getFullYear();
  var month = dt.getMonth() + 1;
  var day   = dt.getDate();
  return (year + "-" + month + "-" + day);
}

function todayMinus7()
{
  var dt = new Date() - 7;
//  dt = dt - 604800000;
  var year  = dt.getFullYear();
  var month = dt.getMonth() + 1;
  var day   = dt.getDate();
  return (year + "-" + month + "-" + day);
}

function dateToMillisecs(tStamp)
{
  var dtFlds = tStamp.split("-");
  var Year = dtFlds[0];
  var Month = ((dtFlds[1].length == 1)? "0" + dtFlds[1] : dtFlds[1] );
  var Day = ((dtFlds[2].length == 1)? "0" + dtFlds[2] : dtFlds[2] );
  return Date.parse(Month + "/" + Day + "/" + Year);
}

function isFechaIniLeThanFechaFin()
{
  var fecini = dateToMillisecs(document.form1.fechaini.value);
  var fecfin = dateToMillisecs(document.form1.fechafin.value);
  return (fecini <= fecfin);
}

function setPageBounds(left, top, width, height)
{
  return ("top=" + top + ",left=" + left + ",width=" +
          width + ",height=" + height);
}

function abrirPaginaUbica(url, nombrePagina)
{
  var wdth = screen.width - screen.width * 0.15;
  var hght = screen.height - screen.height * 0.15;
  var lf = screen.height * 0.075;
  var tp = screen.height * 0.075;
  var options = "menubar=yes,scrollbars=scrollbars="+ ( document.form1.displayInExcel.value == "false"? "no":"yes") +",resizable=yes,status=yes," +
                setPageBounds(lf, tp, wdth, hght);
  var hWnd = window.open(url, nombrePagina, options);
  if ( (document.window != null) && (!hWnd.opener) ){
    hWnd.opener = document.window;
  }
}



function abrirPaginaUbica2(url, nombrePagina)
{
  var wdth = 300;
  var hght = 300;
  var lf   = 100;
  var tp   = 100;
  var options = " scrollbars=scrollbars="+ ( document.form1.displayInExcel.value == "false"? "no":"yes") +",resizable=yes," +
                " width=400, height=200, top= 200, left=300 ";
  hWnd = window.open(url, nombrePagina, options);
  if ( (document.window != null) && (!hWnd.opener) ){
    hWnd.opener = document.window;
  }
}




function ConsultarBtnClick2(rootUrl)
{
  var frm = document.form1;
  // Validar rango de fechas de remesas.
  if ( frm.fechaini.value != "" || frm.fechafin.value != "" )
  {
    if( (frm.fechaini.value != "" && frm.fechafin.value == "") ||
        (frm.fechaini.value == "" && frm.fechafin.value != "") )
    {
      alert("El rango de fechas est� incompleto.");
      return;
    }else if( !isFechaIniLeThanFechaFin() ){
      alert("La fecha inicial debe ser MENOR O IGUAL que la final.");
      return;
    }
  }
  // apayares 17-03-2006
  if ( frm.PlacasBuscar != null && frm.PlacasBuscar.length == 0 ) {
    alert("Debe seleccionar por lo menos una placa a buscar");
    return;
  }
  frm.submit();
}