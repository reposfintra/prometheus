var fechainical;
var fechafincal;
var placaTrailerParser = /[A-Z0-9]{6,}/;
var planillaParser = /([A-Z]|[0-9]){6}/;

function trim(str)
{
  var startIdx = 0, endIdx = str.length - 1;
  while( endIdx > 0 && str.charAt(endIdx) == ' ' )
    endIdx -= 1;
  while( startIdx <= endIdx && str.charAt(startIdx) == ' ' )
    startIdx += 1;
  var trimmedString = "";
  if( startIdx < endIdx + 1 )
    trimmedString = str.substring(startIdx, endIdx + 1);
  return trimmedString;
}

function today()
{
  var dt = new Date();
  var year  = dt.getFullYear();
  var month = dt.getMonth() + 1;
  var day   = dt.getDate();
  return (year + "-" + month + "-" + day);
}

function todayMinus7()
{
  var dt = new Date() - 7;
//  dt = dt - 604800000;
  var year  = dt.getFullYear();
  var month = dt.getMonth() + 1;
  var day   = dt.getDate();
  return (year + "-" + month + "-" + day);
}

function dateToMillisecs(tStamp)
{
  var dtFlds = tStamp.split("-");
  var Year = dtFlds[0];
  var Month = ((dtFlds[1].length == 1)? "0" + dtFlds[1] : dtFlds[1] );
  var Day = ((dtFlds[2].length == 1)? "0" + dtFlds[2] : dtFlds[2] );
  return Date.parse(Month + "/" + Day + "/" + Year);
}

function isFechaIniLeThanFechaFin()
{
  var fecini = dateToMillisecs(document.form1.fechaini.value);
  var fecfin = dateToMillisecs(document.form1.fechafin.value);
  return (fecini <= fecfin);
}

function setPageBounds(left, top, width, height)
{
  return ("top=" + top + ",left=" + left + ",width=" +
          width + ",height=" + height);
}

function abrirPaginaUbica(url, nombrePagina)
{
  var wdth = screen.width - screen.width * 0.15;
  var hght = screen.height - screen.height * 0.15;
  var lf = screen.height * 0.075;
  var tp = screen.height * 0.075;
  var options = "menubar=yes,scrollbars=scrollbars="+ ( document.form1.displayInExcel.value == "false"? "no":"yes") +",resizable=yes,status=yes," +
                setPageBounds(lf, tp, wdth, hght);
  var hWnd = window.open(url, nombrePagina, options);
  if ( (document.window != null) && (!hWnd.opener) ){
    hWnd.opener = document.window;
  }
}



function abrirPaginaUbica2(url, nombrePagina)
{
  var wdth = 300;
  var hght = 300;
  var lf   = 100;
  var tp   = 100;
  var options = " scrollbars=scrollbars="+ ( document.form1.displayInExcel.value == "false"? "no":"yes") +",resizable=yes," +
                " width=400, height=200, top= 200, left=300 ";
  hWnd = window.open(url, nombrePagina, options);
  if ( (document.window != null) && (!hWnd.opener) ){
    hWnd.opener = document.window;
  }
}




function ConsultarBtnClick(rootUrl)
{
  var frm = document.form1;
  var isDataRight = false;

  // Validar rango de fechas de remesas.
  if ( frm.fechaini.value != "" || frm.fechafin.value != "" )
  {
    if( (frm.fechaini.value != "" && frm.fechafin.value == "") ||
        (frm.fechaini.value == "" && frm.fechafin.value != "") )
    {
      alert("El rango de fechas est� incompleto.");
    }else if( !isFechaIniLeThanFechaFin() ){
      alert("La fecha inicial debe ser MENOR O IGUAL que la final.");
    }else{
      isDataRight = true;
    }
  }else{
    isDataRight = true;
  }

  // Ver cuales de los valores del campo "Tipo de viaje" fueron
  // seleccionados, para armar con ellos una lista separada por comas.
  if( isDataRight )
  {
    var lst = "";
    if( frm.tipoViaje.options[0].selected )
    {
      // Escogerlas todas si fue seleccionada la opci�n que as� lo determina.
      //lst = "NA,RM,RC,RE,DM,DC";
      lst = "TODOS";
    }else{
      for( var idx = 1; idx < 5; idx++ )
      {
        if( frm.tipoViaje.options[idx].selected )
        {
          lst += frm.tipoViaje.options[idx].value + ",";
        }
      }
      lst = ((lst == "")? "" : lst.substring(0, lst.length - 1));
    }
    frm.listaTipoViaje.value = lst;
  }

  // Si los datos est�n correctos, env�elos para mostrar el reporte.
  if( isDataRight )
  {
    var toExcel = frm.displayInExcel.value;
    var soloUltimoReporte = "N";
    var asociadasorigen = "N";
    var asociadasdestino = "N";
    if(frm.asociadasorigen.checked){
       asociadasorigen = "S" 
    }

    if(frm.asociadasdestino.checked){
       asociadasdestino = "S" 
    }

    if( toExcel == "true" )
      window.status = "Generando archivo de reporte en MS-Excel. Por favor espere...";
    else
      window.status = "Generando reporte. Por favor espere...";
    
    var placasTrailersList = trim(frm.placasTrailers.value.toUpperCase());
    if( placasTrailersList != "" )
    {
      var firstChar = placasTrailersList.substring(0, 1);
      var lastChar  = placasTrailersList.substring(
        placasTrailersList.length - 1, placasTrailersList.length
      );
      if( firstChar.search(/[A-Z0-9]/) == -1 ||
           lastChar.search(/[A-Z0-9]/) == -1)
      {
        alert(
          "Lista de placas mal digitada." +
          "\nTenga en cuenta las siguientes observaciones:" +
          "\n1.  La lista no puede empezar / terminar por caracteres distintos de letras y n�meros." +
          "\n2.  El �nico separador permitido es LA COMA (',')."
        );
        return;
      }
      var placasTrailersArray = placasTrailersList.split(",");
      var idx = 0;
      while( idx < placasTrailersArray.length )
      {
        //alert("placasTrailersArray[" + idx + "] = " + trim(placasTrailersArray[idx]));
        if( placaTrailerParser.test(trim(placasTrailersArray[idx])) )
          idx++;
        else{
          alert(
            "Lista de placas / trailers incorrecta." +
            "\nTenga en cuenta las siguientes observaciones:" +
            "\n1.  La lista DEBE separarse por comas." +
            "\n2.  Cada placa / trailer DEBE tener como m�nimo 6 letras y/o n�meros." +
            "\nPor favor, rectifique."
          );
          return;
        }
      }
    }
    var planillasList = trim(frm.planillas.value.toUpperCase());
    if( planillasList != "" )
    {
      var firstChar = planillasList.substring(0, 1);
      var lastChar  = planillasList.substring(
        planillasList.length - 1, planillasList.length
      );
      if( firstChar.search(/[A-Z0-9]/) == -1 ||
           lastChar.search(/[A-Z0-9]/) == -1)
      {
        alert(
          "Lista de planillas mal digitada." +
          "\nTenga en cuenta las siguientes observaciones:" +
          "\n1.  La lista no puede empezar / terminar por caracteres distintos de letras y n�meros." +
          "\n2.  El �nico separador permitido es LA COMA (',')."
        );
        return;
      }
      var planillasArray = planillasList.split(",");
      var idx = 0;
      while( idx < planillasArray.length )
      {
        //alert("planillasArray[" + idx + "] = " + trim(planillasArray[idx]));
        var ocValue = trim(planillasArray[idx]);
        var ocParsingData = planillaParser.exec(ocValue);
        if( ocParsingData != null && ocParsingData[0] == ocValue )
          idx++;
        else{
          alert(
            "Lista de planillas incorrecta." +
            "\nTenga en cuenta las siguientes observaciones:" +
            "\n1.  La lista DEBE separarse por comas." +
            "\n2.  Cada planilla DEBE tener 6 letras y/o n�meros." +
            "\nPor favor, rectifique."
          );
          return;
        }
      }
    }
    var tipoBusquedaRbtn = document.getElementsByName("tipoBusquedaRBtn");
    var tipoBusquedaValue = (tipoBusquedaRbtn[0].checked ? "placas" : "trailers");
    var tipoReporte = "";
    var FITDEF = "";
    if(frm.FITDEF.checked ){
       FITDEF ="SI"; 
    }else{
       FITDEF ="NO"
    }
    
    for( var i=0; i<frm.tipoReporte.length; i++ ){
        if ( frm.tipoReporte[i].checked ){
            tipoReporte = frm.tipoReporte[i].value;
            break;
        }
    }
      var params = "&fechaini=" + frm.fechaini.value +
                   "&fechafin=" + frm.fechafin.value +
                   "&listaTipoViaje=" + frm.listaTipoViaje.value +
                   "&estadoViajes=" + frm.viajes.value +
                   "&displayInExcel=" + frm.displayInExcel.value +
                   "&grupoequipo=" + frm.grupoequipo.value +
                   "&tipoBusqueda=" + tipoBusquedaValue +
                   "&placasTrailers=" + placasTrailersList +
                   "&origen=" + frm.origen.value +
                   "&destino=" + frm.destino.value +
                   "&asociadasorigen=" + asociadasorigen +
                   "&asociadasdestino=" + asociadasdestino +
                   "&planillas=" + trim(frm.planillas.value).toUpperCase()+
		   "&tipoReporte=" + tipoReporte + //*******APAYARES 20060203
                   "&FITDEF=" + FITDEF;
      //alert( params );
      //*******APAYARES 20060203
      if (tipoReporte == "repDetallado") {
          if (planillasList == ""){
             alert("DEBE INGRESAR PLANILLAS");
          }
          else{          
             if(toExcel=='File') window.location.href("../fintravalores/controller?estado=TraficoDetallado&accion=Search" + params);
             else                window.open("../fintravalores/controller?estado=TraficoDetallado&accion=Search" + params, "wubicveh","scrollbars=yes");
          }
      }
      else{
          if(toExcel=='File') window.location.href(rootUrl + params);
          else                abrirPaginaUbica(rootUrl + params, "wubicveh");
      }
  }
}