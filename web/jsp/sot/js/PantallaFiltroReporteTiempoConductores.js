function setPageBounds(left, top, width, height){
  return ("top=" + top + ",left=" + left + ",width=" +
          width + ",height=" + height);
}

function abrirPaginaTiempos(url, nombrePagina){
  var wdth = screen.width - screen.width * 0.15;
  var hght = screen.height - screen.height * 0.15;
  var lf = screen.height * 0.075;
  var tp = screen.height * 0.075;
  var options = "menubar=yes,scrollbars=no,resizable=yes,status=yes," +
                setPageBounds(lf, tp, wdth, hght);
  var hWnd = window.open(url, nombrePagina, options);
  if ( (document.window != null) && (!hWnd.opener) ){
    hWnd.opener = document.window;
  }
}

function validarCamposReporteTiempos(action){
	var form = document.returnsFrm;
	var inicio = document.returnsFrm.fechaini.value;
	var fin = document.returnsFrm.fechafin.value;
	
		// Validar rango de fechas de remesas.
	  if ( inicio != "" || fin != "" ){
		if( (inicio != "" && fin == "") ||
			(inicio == "" && fin != "") ){
		  alert("El rango de fechas est� incompleto.");
		  return;
		}else if( !isFechaIniLeThanFechaFin() ){
		  alert("La fecha inicial debe ser MENOR O IGUAL que la final.");
		  return;
		}
	  }
	  var url = action  + "&inicio="+inicio + "&fin="+fin;
        if ( form.placa.value != "" ){
             url += "&placa="+form.placa.value;
        }
        if ( form.origen.value != "" && !(form.todoOrigen.checked)){
             url += "&origen="+form.origen.value+"&nombreOrigen="+form.origen[form.origen.selectedIndex].innerText;
        }
        if ( form.destino.value != "" && !(form.todoDestino.checked)){
             url += "&destino="+form.destino.value+"&nombreDestino="+form.destino[form.destino.selectedIndex].innerText;
        }
        if ( form.cliente.value != "" ){
             url += "&cliente="+form.cliente.value+"&nombreCliente="+form.cliente[form.cliente.selectedIndex].innerText;
        }
        url += "&formatoSalida="+form.formatoSalida.value;
        if ( form.formatoSalida.value == "web" ) {
            abrirPaginaTiempos(url,"rptTiempos");
        }
        else {
            location.href = url;
        }
}

function dateToMillisecs(tStamp){
  var dtFlds = tStamp.split("-");
  var Year = dtFlds[0];
  var Month = ((dtFlds[1].length == 1)? "0" + dtFlds[1] : dtFlds[1] );
  var Day = ((dtFlds[2].length == 1)? "0" + dtFlds[2] : dtFlds[2] );
  return Date.parse(Month + "/" + Day + "/" + Year);
}

function isFechaIniLeThanFechaFin(){
  var fecini = dateToMillisecs(document.returnsFrm.fechaini.value);
  var fecfin = dateToMillisecs(document.returnsFrm.fechafin.value);
  return (fecini <= fecfin);
}