function enviarFormNuevaTabla(){
        var form = document.forma;
	var tipo = form.table_type.value;
        var descripcion = form.descripcion.value;
        var obs = form.dato.value;
        if ( tipo == null || tipo == "" ){
            alert("Debe digitar el nombre de la tabla");
            form.type.focus();
            return;
        }
        else if ( descripcion == null || descripcion == "" ){
            alert("Debe digitar la descripcion de la tabla");
            form.descripcion.focus();
            return;
        }
        else if ( obs == null || obs == "" ){
            alert("Debe digitar la observacion de la tabla");
            form.dato.focus();
            return;
        }
        form.submit();        
}