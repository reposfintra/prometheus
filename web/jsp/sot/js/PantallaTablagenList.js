function validarSeleccion(){
    var form = document.forma;
    var lstChecks = document.getElementsByName("select");
    var hayChecked = false;
    for( idx = 0; idx < lstChecks.length; idx++ ){
        checkBoxFld = lstChecks[idx];
        if( checkBoxFld.checked ){
            hayChecked = true;
        }
    }
    if ( !hayChecked ){
        alert("Para eliminar uno o m�s tablas debe seleccionar las casillas de\nverficaci�n ubicadas a la izquierda de cada fila de la lista");
        return;
    }
    var res = confirm("Confirma que desea eliminar la(las) tabla(s) seleccionadas?");
    if ( res != 0 ){
        form.submit();
    }
}