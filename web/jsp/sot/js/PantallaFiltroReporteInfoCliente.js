var fechainical;
var fechafincal;
var hWnd;

function today()
{
  var dt = new Date();
  var year  = dt.getFullYear();
  var month = dt.getMonth() + 1;
  var day   = dt.getDate();
  return (year + "-" + month + "-" + day);
}

function dateToMillisecs(tStamp)
{
  var dtFlds = tStamp.split("-");
  var Year = dtFlds[0];
  var Month = ((dtFlds[1].length == 1)? "0" + dtFlds[1] : dtFlds[1] );
  var Day = ((dtFlds[2].length == 1)? "0" + dtFlds[2] : dtFlds[2] );
  return Date.parse(Month + "/" + Day + "/" + Year);
}

function isFechaIniLeThanFechaFin()
{
  var fecini = dateToMillisecs(document.form1.fechaini.value);
  var fecfin = dateToMillisecs(document.form1.fechafin.value);
  return (fecini <= fecfin);
}

function clienteChange(controller){
   var form = document.form1;
   var nombre = form.cliente[form.cliente.selectedIndex].innerText;
   if ( nombre != "" ){
       form.action = controller+"?estado=InfoCliente&accion=Search&cmd=update&nombreCliente="+nombre;
       form.submit();
       return true;
   }
}

function setPageBounds(left, top, width, height)
{
  return ("top=" + top + ",left=" + left + ",width=" +
          width + ",height=" + height);
}

function abrirPaginaSOT(url, nombrePagina)
{
  var wdth = screen.width - screen.width * 0.15;
  var hght = screen.height - screen.height * 0.15;
  var lf = screen.width * 0.075;
  var tp = screen.height * 0.075;
  var options = "menubar=yes,scrollbars="+ ( document.form1.displayInExcel.value == "false"? "no":"yes") +",status=yes,resizable=yes," +
                setPageBounds(lf, tp, wdth, hght);
  hWnd = window.open(url, nombrePagina, options);
  if ( (document.window != null) && (!hWnd.opener) ){
    hWnd.opener = document.window;
  }
}


function abrirPaginaSOT2(url, nombrePagina)
{
  var wdth = 300;
  var hght = 300;
  var lf   = 100;
  var tp   = 100;
  var options = " scrollbars="+ ( document.form1.displayInExcel.value == "false"? "no":"yes") +",resizable=yes," +
                " width=400, height=200, top= 200, left=300 ";
  hWnd = window.open(url, nombrePagina, options);
  if ( (document.window != null) && (!hWnd.opener) ){
    hWnd.opener = document.window;
  }
}

function ConsultarBtnClick(baseUrl)
{
  var frm = document.form1;
  var isDataRight = false;

  
  	// Validar rango de fechas de remesas.
    if ( frm.fechaini.value != "" || frm.fechafin.value != "" ){
      if( (frm.fechaini.value != "" && frm.fechafin.value == "") ||
          (frm.fechaini.value == "" && frm.fechafin.value != "") ){
        alert("El rango de fechas est� incompleto.");
      }else if( !isFechaIniLeThanFechaFin() ){
        alert("La fecha inicial debe ser MENOR O IGUAL que la final.");
      }else{
        isDataRight = true;
      }
    }else{
      isDataRight = true;
    }

	/*var criterios = document.getElementsByName("criteria");
	var hayCriterio = false;
	for( var i=0; i<criterios.length; i++ ){
		if ( criterios[i].checked ){
			hayCriterio = true;
			break;
		}
	}*/
 
	if ( !form1.todosLosClientes.checked && (form1.nombreCliente.value == null || form1.nombreCliente.value == "") ){
		alert("Debe seleccionar un cliente de la lista de clientes");
		isDataRight = false;
	}
    else if ( form1.tipo_carga.value == "" ){
        alert("Debe seleccionar algun tipo de carga!");
        isDataRight = false;
    }
	else if ( form1.tipo_carga.value == "" ){
        alert("Debe seleccionar algun tipo de carga!");
        isDataRight = false;
    }
    //alert("Tipo "+form1.criteria.value+" Nro "+form1.userDefValue.value);
    if ( form1.criteria.value == '' && form1.userDefValue.value != ""){
		alert("Seleccione el tipo de documento");
	    form1.criteria.focus();
		isDataRight = false;
	}

    // Ver cuales de los valores del campo "Tipo de viaje" fueron
    // seleccionados, para armar con ellos una lista separada por comas.
    if( isDataRight ){
      var lst = "";
      if( frm.tipoViaje.options[0].selected ){
        // Escogerlas todas si fue seleccionada la opci�n que as� lo determina.
        lst = "NA,RM,RC,RE,DC,DM,DE";
      }else{
        for( var idx = 1; idx < 7; idx++ )
        {
          if( frm.tipoViaje.options[idx].selected )
          {
            lst += frm.tipoViaje.options[idx].value + ",";
          }
        }
        lst = ((lst == "")? "" : lst.substring(0, lst.length - 1));
      }
      frm.listaTipoViaje.value = lst;
    }

  

  // Si los datos est�n correctos, env�elos para mostrar el reporte.
  if( isDataRight )
  {
    var toExcel = frm.displayInExcel.value;
    if( toExcel == "true" )
      window.status = "Generando Archivo de Reporte en MS-Excel. Por Favor, Espere...";
    else
      window.status = "Generando Reporte. Por Favor, Espere...";
   /* var criteriaValue = "";
   if( frm.criteria[0].checked )
      criteriaValue = "facturacial";
    else if( frm.criteria[1].checked )
      criteriaValue = "docinterno";
    else if( frm.criteria[2].checked )
      criteriaValue = "ot";*/
    
    var actionUrl = baseUrl + "?estado=InfoCliente&accion=Search&cmd=show" +
                   "&fechaini=" + form1.fechaini.value +
                   "&fechafin=" + form1.fechafin.value +
                   "&listaTipoViaje=" + form1.listaTipoViaje.value +
                   "&userDefValue=" + form1.userDefValue.value +
                   "&criteria=" + frm.criteria.value +
                   "&displayInExcel=" + toExcel +
                   "&tipo_carga=" + form1.tipo_carga.value +
                   "&cliente=" + form1.cliente.value +
                   "&todosLosClientes=" + form1.todosLosClientes.checked +
                   "&nombreCliente=" + form1.nombreCliente.value;
                   //"&clientesConsultados=" + form1.clientesConsultados.value;
   
    var ALL = 'N';
    //var ALL = (form1.clientesConsultados.value.split(",").length == 1)?"N":"S";

    if(toExcel=='File'){
       if(ALL=='N')  window.location.href(actionUrl);
       else          abrirPaginaSOT2(actionUrl, "wrptinfocliente");
    }
    else{
       abrirPaginaSOT(actionUrl, "wrptinfocliente");
    }
  }
	
}
  
  var colorAnterior;
  var filaAnterior;
  function marcarFila(fila){
    if ( filaAnterior != null ){
        filaAnterior.bgColor = colorAnterior;
    }
    colorAnterior = fila.bgColor;
    fila.bgColor = "#00FF00";
    filaAnterior = fila;
  }

  function cambiarCombo(casilla){
    document.form1.cliente.disabled = casilla.checked;
    document.form1.campo.disabled = casilla.checked;
  }
