<!--
- Autor : Alejandro Payares
- Date  : Diciembre 24 del 2005
- Copyrigth Notice : Fintravalores S.A. S.A
- Descripcion : Pagina JSP que permite editar la informaci�n de un usuario.
- Ultima modificaci�n: Enero 11 de 2006
-->
<%@ page session="true" %>
<%@ page import="java.util.*" %>
<%@ page import="com.tsp.operation.model.beans.*" %>
<%@ include file="/WEB-INF/InitModel.jsp" %>
<%@ taglib uri="http://jakarta.apache.org/taglibs/input-0.90" prefix="input" %>
<%@ taglib uri="http://www.sanchezpolo.com/sot" prefix="tsp"%><head>
<% try { %>
<%
  Usuario loggedUser = (Usuario) session.getAttribute("Usuario");
  Usuario userData = (Usuario)request.getAttribute("tsp.sot.userData");
  String nombre    = request.getParameter("nombreUsuario");
  String agencia   = request.getParameter("agencia");
  String nit       = request.getParameter("nitUsuario");
  //String nitPro    = request.getParameter("NitPro");
  String base       = request.getParameter("baseUsuario");
  String direccion = request.getParameter("direccionUsuario");
  String telefono  = request.getParameter("telefonoUsuario");
  String email     = request.getParameter("emailUsuario");
  String pais      = request.getParameter("paisUsuario");
  String ciudad    = request.getParameter("ciudadUsuario");
  String tipoUsu   = request.getParameter("tipoUsuario");
  String estado    = request.getParameter("estadoUsuario");
  String id        = request.getParameter("idUser");
  String clave     = request.getParameter("claveUsuario");
  String cliDest   = request.getParameter("clienteDestinat");
  String accesoPlVj   = request.getParameter("accesoPlanViaje");

  System.out.println("EL VALOR CLIDEST ES "+cliDest);
  //AMENDEZ 20050705
  String dpto = request.getParameter("dpto");
  String updatePass = request.getParameter("updatePass");
  TreeMap atribAgen = null;
  nombre     = (nombre == null? userData.getNombre() : nombre);
  agencia    = (agencia == null? userData.getId_agencia() : agencia);
  nit        = (nit == null? userData.getCedula() : nit);
 // nitPro     = (nitPro == null? userData.getNitPropietario(): nitPro);
  direccion  = (direccion == null? userData.getDireccion() : direccion);
  base  = (base == null? userData.getBase() : base);
  telefono   = (telefono == null? userData.getTelefono() : telefono);
  email      = (email == null? userData.getEmail() : email);
  pais       = (pais == null? userData.getCodPais() : pais);
  ciudad     = (ciudad == null? userData.getCiudad() : ciudad);
  tipoUsu    = (tipoUsu == null? userData.getTipo() : tipoUsu);
  estado     = (estado == null? userData.getEstado() : estado);
  id         = (id == null? userData.getIdusuario() : id);
  clave      = (clave == null? userData.getPassword() : clave);
  cliDest    = (cliDest == null? userData.getClienteDestinat() : cliDest);
  accesoPlVj = (accesoPlVj == null? userData.getAccesoplanviaje() : accesoPlVj);
  //AMENDEZ 20050705
  dpto = (dpto == null? userData.getDpto() : dpto);
  updatePass = (updatePass == null? Boolean.toString(userData.isCambiarClaveLogin()).toUpperCase() : updatePass);
  TreeMap cia = userData.getListCia();

  List list = null;
  String textoCliDest = "";
  String textoAgencia = "";
  if( tipoUsu.equals("CLIENTEADM") || tipoUsu.equals("CLIENTETSP") )
  {
    textoCliDest = "Cliente";
    model.clienteService.clienteSearch();
    list = model.clienteService.getClientesSot();
  }else if( tipoUsu.equals("DEST") ){
    textoCliDest = "Destinatario ";
    model.remidestService.destinatarioSearch(loggedUser.getClienteDestinat());
    list = model.remidestService.getDestinatarios();
  }
   if (tipoUsu.equals("TSPUSER")||tipoUsu.equals("ADMIN")){
           textoAgencia = "Agencia ";
    }
  String frmAction = CONTROLLER + "?estado=GestionUsuarios&accion=ManageUsers";
%>
<head>
    <title>.: Edici�n de datos de usuarios :.</title>
    <link href='<%=BASEURL%>/css/estilostsp.css' rel='stylesheet' type='text/css'>
    <link href='../css/estilostsp.css' rel='stylesheet' type='text/css'>
    <script src='<%=BASEURL%>/js/PantallaEditarUsuario.js' language='javascript'></script><br><br>
    <script src='<%=BASEURL%>/js/boton.js' language='javascript'></script><br><br>
    <script>
        function _onmousemove(item) { item.className='select';   }
        function _onmouseout (item) { item.className='unselect'; }
        function addOption(Comb,valor,texto){
        var Ele = document.createElement("OPTION");
        Ele.value=valor;
        Ele.text=texto;
        Comb.add(Ele);
        }
        function loadCombo(datos, cmb){
        cmb.length = 0;
        for (i=0;i<datos.length;i++){
        var dat = datos[i].split(",");
        addOption(cmb, dat[0], dat[1]);
        }
        }
        function loadCombo2(datos, datosA ,cmbA, cmbNA){
        cmbA.length = 0;
        cmbNA.length = 0;
        for (i=0;i<datos.length;i++){
        var dat = datos[i].split(SeparadorJS);
        if (datosA.indexOf(dat[0])==-1)
        addOption(cmbNA, dat[0], dat[1]);
        else
        addOption(cmbA , dat[0], dat[1]);
        }
        }
        function move(cmbO, cmbD){
        for (i=cmbO.length-1; i>=0 ;i--)
        if (cmbO[i].selected){
        addOption(cmbD, cmbO[i].value, cmbO[i].text)
        cmbO.remove(i);
        }
        // order(cmbD);
        }
        function moveAll(cmbO, cmbD){
        for (i=cmbO.length-1; i>=0 ;i--){
        addOption(cmbD, cmbO[i].value, cmbO[i].text)
        cmbO.remove(i);
        }
        }
        function selectAll(cmb){
        for (i=0;i<cmb.length;i++)
        cmb[i].selected = true;
        }
    </script>
</head>
<body onresize="redimensionar()" onload='redimensionar()'>
    <div id="capaSuperior" style="position:absolute; width:100%; z-index:0; left: 0px; top: 0px;">
        <jsp:include page="/WEB-INF/toptsp.jsp?encabezado=EDITAR DATOS DE USUARIO"/>
    </div>
    <div id="capaCentral" style="position:absolute; width:100%; height:87%; z-index:0; left: 0px; top: 100px; overflow: scroll;">
        <form name='userManagerFrm' method='post' action='<%= frmAction %>'>
            <table  border = '2' align = 'center'>
                <tr>
                <td>
                    <table width='700' align='center'>
                        <tr align='center'>
                            <td align="center" class="subtitulo1">DATOS BASICOS DEL USUARIO</td>
                            <td width="45%" align="left" class="barratitulo"><img src="<%=BASEURL%>/images/titulo.gif" width="32" height="20"></td>
                        </tr>
                        <tr class='fila'>
                        <th  align='center'>
						<table>
							<tr class='fila'>
							<td width="100">Compa�ia</td>
							<td>
							<%--AMENDEZ 20050628--%>
								<%
								  // lo que antes se llamaba listar
								  model.ciaService.listarDistritos();
								  TreeMap cias = model.ciaService.getCbxCia();
								%>
								<input:select attributesText="class='listmenu'" name="cia" options="<%=cias%>" defaults="<%=cia%>" multiple="true"/></th>
								</td>
							</tr>
						</table>
                        <td align="center">
                            <table>
							<tr class='fila'>
							<td width="50">Base</td>
							<td>
							<%--APAYARES 20051121--%>
							  <%
								model.ciaService.listarBases();
								TreeMap bases = model.ciaService.getBases();
							  %>
								<input:select name="base" attributesText="class='listmenu'" options="<%=bases%>" default="<%=base%>" size="3"/>
								</td>
							</tr>
						</table>
                        </td>
                        </tr>
                        <tr class='fila'>
                            <th  align='left'>Nombre</th>
                            <td >
                                <input type='text' class='textbox' name='nombreUsuario' maxLength='50'
                                value='<%= nombre %>'><img src="<%=BASEURL%>/images/botones/iconos/obligatorio.gif" width="10" height="10">
                            </td>
                        </tr>
                        <tr class='fila'>
                        <th  align='left'>Agencia</th>
                        <td >
      <% String str = "id='agencia' class='listmenu' style='width:100%'"; %>
                            <input:select default='<%= agencia %>' name="agencia" options="<%=model.agenciaService.getCbxAgencia()%>" attributesText="<%= str %>"/>
                        </td>
                        </tr>
                        <tr class='fila'>
                            <th  align='left'>NIT</th>
                            <td >
                                <input type='text' class='textbox' name='nitUsuario' maxLength='20'
                                value='<%= nit %>'><img src="<%=BASEURL%>/images/botones/iconos/obligatorio.gif" width="10" height="10">
                            </td>
                        </tr>
                        <tr class='fila'>
                            <th  align='left'>Direcci�n</th>
                            <td >
                                <input type='text' class='textbox' name='direccionUsuario' maxLength='50'
                                value='<%= direccion %>'><img src="<%=BASEURL%>/images/botones/iconos/obligatorio.gif" width="10" height="10">
                            </td>
                        </tr>
                        <tr class='fila'>
                            <th  align='left'>Tel�fono</th>
                            <td >
                                <input type='text' class='textbox' maxLength='20' value='<%= telefono %>'
                                name='telefonoUsuario' onkeypress='return justDigitsKeyPress();'>
								<img src="<%=BASEURL%>/images/botones/iconos/obligatorio.gif" width="10" height="10">
                            </td>
                        </tr>
                        <tr class='fila'>
                            <th  align='left'>E-Mail</th>
                            <td >
                                <input type='text' class='textbox' name='emailUsuario' id='emailUsuario' maxLength='50'
                                value='<%= email %>'>
								<img src="<%=BASEURL%>/images/botones/iconos/obligatorio.gif" width="10" height="10">
                            </td>
                        </tr>
                        <tr class='fila'>
                            <th  align='left'>Pa�s</th>
                            <td >
                                <select class='listmenu' name='paisUsuario'>
                                <option value='COL' <%= pais.equals("COL")? "selected" : "" %>>Colombia
                                <option value='VEN' <%= pais.equals("VEN")? "selected" : "" %>>Venezuela
                                <option value='ECU' <%= pais.equals("ECU")? "selected" : "" %>>Ecuador
                                </select>
                            </td>
                        </tr>
                        <tr class='fila'>
                            <th  align='left'>Ciudad</th>
                            <td >
                                <input type='text' class='textbox' name='ciudadUsuario' maxLength='30'
                                value='<%= ciudad %>'>
								<img src="<%=BASEURL%>/images/botones/iconos/obligatorio.gif" width="10" height="10">
                            </td>
                        </tr>
                        <tr class='fila'>
                        <th  align='left'>Tipo de Usuario</th>
                        <td >
                            <select class='listmenu' name='tipoUsuario' id='tipoUsuario'
              <%= (loggedUser.getTipo().equals("ADMIN")? " onchange='tipoUsuarioChange()'" : "") %>><%
      if( loggedUser.getTipo().equals("ADMIN") )
      { %>
                            <option value='ADMIN' <%= (tipoUsu.equals("ADMIN")? "selected" : "") %>>Administrador
                            <option value='CLIENTETSP' <%= (tipoUsu.equals("CLIENTETSP")? "selected" : "") %>>Cliente de Fintra
                            <option value='CLIENTEADM' <%= (tipoUsu.equals("CLIENTEADM")? "selected" : "") %>>Cliente - Admin. de Transporte
                            <option value='TSPSERVCLI' <%= (tipoUsu.equals("TSPSERVCLI")? "selected" : "") %>>Servicio al Cliente de TSP
                            <option value='TSPUSER' <%= (tipoUsu.equals("TSPUSER")? "selected" : "") %>>Usuario de Fintra
                            <%
      } else if( loggedUser.getTipo().equals("CLIENTEADM") ) { %>
                            <option value='DEST' <%= (tipoUsu.equals("DEST")? "selected" : "") %>>Destinatario <%
      } %>
                            </select>
                        </td>
                        </tr>

                        <%--APAYARES 20060224--%>
                        <tr class='fila'>
                            <th align="left">
                                Perfil(es)
                            </th>
                            <td colspan="3" align="left" valign='top'>
                                <select name="perfil" multiple class='listmenu' size='5'>
                                <%  List listaPerfiles = model.menuService.getPerfiles();
                                    Iterator itePerfiles = listaPerfiles.iterator();
                                    Vector perfilesAsociados = userData.getPerfiles();
                                    while( itePerfiles.hasNext() ){
                                        Perfil p = (Perfil) itePerfiles.next();
                                        System.out.println("perfilesAsociados: "+perfilesAsociados+" \ncontains: "+p.getId()+"? "+perfilesAsociados.contains(p.getId()));
                                        out.println("\t\t\t\t<option value='"+p.getId()+"' "+(perfilesAsociados.contains(p.getId())?"selected":"")+">"+p.getNombre()+"</option>");
                                    }
                                 %>
                                </select>
                                <img src="<%=BASEURL%>/images/botones/iconos/obligatorio.gif" width="10" height="10">
                            </td>
                        </tr>


                        <tr class='fila'>
                            <th  align='left'>Estado Actual</th>
                            <td >
                                <select class='listmenu' name='estadoUsuario'>
                                <option value='A' <%= estado.equals("A")? "selected" : "" %>>Activo
                                <option value='I' <%= estado.equals("I")? "selected" : "" %>>Inactivo
                                <option value='R'>Eliminar del Sistema
                                </select>
                            </td>
                        </tr><%
  if( tipoUsu.equals("CLIENTEADM") || tipoUsu.equals("DEST") || tipoUsu.equals("CLIENTETSP") ) { %>
                        <tr class='fila'>
                        <th  align='left' o><%= textoCliDest %></th>
                        <td >
    <%
      // Generar din�micamente
      if (list != null)
      {
            Iterator it = list.iterator();
            List Asig = new LinkedList();
            List NAsig = new LinkedList();
             while (it.hasNext())  {
                Cliente cliente = (Cliente) it.next();
                if (cliDest.indexOf(cliente.getCodcli())==-1 ){
                    NAsig.add(cliente);

                }else{
                    Asig.add(cliente);}
             } %>
                            <table  border='0' width='500' cellpadding='3' cellspacing='0' align='left'>
                                <tr class='fila'><td align='center'>

                                    <table border='1' width='98%' cellpadding='0' cellspacing='0'>
                                        <tr class='subtitulo1'>
                                            <th width='46%' class='bordereporte'>Clientes Asignados</th>
                                              <th width='*' class='bordereporte' >&nbsp;</th>
                                              <th width='46%' class='bordereporte'>Clientes </th>
                                        </tr>
                                        <tr class='fila'>
                                        <td class='bordereporte'><select class='listmenu' multiple size='8' class='select' style='width:100%' name='clienteDestinat' onchange=" if (this.selectedIndex!=-1) { window.status=this[this.selectedIndex].text;}">
                      <% for (int i=0;i<Asig.size() ;i++){
                          Cliente  cliente = (Cliente) Asig.get(i);%>
                                            <option value='<%= cliente.getCodcli() %>' label='<%= cliente.getNomcli() %>'><%= cliente.getNomcli() %></option>
                      <% } %>
                                            </select>
                                        </td>
                                        <td align="center" class='bordereporte'>
                                          <tsp:boton value="envIzq" onclick="move(Clientes, clienteDestinat);"/><br>
                                          <tsp:boton value="enviarIzq" onclick="moveAll(Clientes, clienteDestinat);"/><br>
                                          <tsp:boton value="envDer" onclick="move(clienteDestinat, Clientes );"/><br>
                                          <tsp:boton value="enviarDerecha" onclick="moveAll(clienteDestinat, Clientes );"/><br>
                                      </td>
                                        <td><select class='listmenu' multiple size='8' class='select' style='width:100%' name='Clientes' onchange=" if (this.selectedIndex!=-1) { window.status=this[this.selectedIndex].text;}">
                                        <!-- *******************************************  -->

                 <%  for (int i=0;i<NAsig.size() ;i++){
                     Cliente  cliente = (Cliente) NAsig.get(i);
                     /*String []datos = ((String) NAsig.get(i)).split("~");*/%>
                                        <option value='<%= cliente.getCodcli() %>' label='<%= cliente.getNomcli() %>'><%= cliente.getNomcli() %></option>
                <%  } %>


                                        <!-- *******************************************  -->
                                        </select></td>
                                        </tr>
                                    </table>
                                </td></tr>
                            </table>

     <%} %>
                        </td>
                        </tr> <%
  } %>


                        <tr class='fila'>
                            <th  align='left'>�Tiene Acceso a Plan de Viaje?</th>
                            <td >
                                <select class='listmenu' name='accesoPlanViaje'>
                                <option value='1' <%= accesoPlVj.equals("1")? "selected" : "" %>>SI
                                <option value='0' <%= accesoPlVj.equals("readonly")? "selected" : "" %>>NO
                                </select>
                            </td>
                        </tr>
                        <tr class='fila'>
                        <th  align='left'>�Pertenece a Trafico?</th>
                        <td >
                            <select class='listmenu' name='dpto'>
                            <option value='traf' <%= "traf".equals(dpto)? "selected" : "" %>>SI
                            <option value='' <%= "".equals(dpto)? "selected" : "" %>>NO
                            </select>
                        </td>
                        </tr>
                        <tr>
                            <td align="center" class="subtitulo1">DATOS PARA ACCESO AL SISTEMA</td>
                            <td width="45%" align="left" class="barratitulo"><img src="<%=BASEURL%>/images/titulo.gif" width="32" height="20"></td>
                        </tr>
                        <tr class='fila'>
                            <th  align='left'>ID de Usuario</th>
                            <td >
                                <b><%= id %></b>
                            </td>
                        </tr>
                        <tr class='fila'>
                            <th  align='left'>Clave</th>
                            <td >
                                <input class='textbox' type='password' name='claveUsuario' maxLength='15'
                                value='<%= clave %>'><img src="<%=BASEURL%>/images/botones/iconos/obligatorio.gif" width="10" height="10">
                            </td>
                        </tr>
                        <tr class='fila'>
                            <th  align='left'>�Cambia Clave al Inicio?</th>
                            <td >
                                <select class='listmenu' name='updatePass'>
                                <option value='TRUE' <%= updatePass.equals("TRUE")? "selected" : "" %>>SI
                                <option value='FALSE' <%= updatePass.equals("FALSE")? "selected" : "" %>>NO
                                </select>
                            </td>
                        </tr>
                    </table>
                </td>
                </tr>
            </table>
			<table align="center" width="700">
			<tr>
			<td class="letraresaltada"><img src="<%=BASEURL%>/images/botones/iconos/obligatorio.gif" width="10" height="10"> Campo mandatorio</td>
			</tr>
			</table>
            <p>
                <table align='center' width='700'>
                <tr>
                        <td>
                <table align='right' border='0'>
                    <tr>
                        <td>
                            <% String back = "location.href='"+CONTROLLER+"?estado=GestionUsuarios&accion=ManageUsers';"; %>
                            <tsp:boton name="regresar" value="regresar" onclick="<%=back%>" />
                        </td>
                        <td width='20'></td>
                        <td>
                            <tsp:boton name="saveUserDataBtn" value="modificar" onclick="if(userManagerFrm.clienteDestinat){ selectAll(userManagerFrm.clienteDestinat);}  saveUserDataBtnClick();" />
                        </td>
                    </tr>
                 </table>
                        </td>
                        </tr>
                        </table>
            </p>
            <input type='hidden' name='cmd' value='edit'>
            <input type='hidden' name='claveCambio' value='N'>
            <input type='hidden' name='idUser' value='<%= id %>'>
        </form>
    </div>
<script language='javascript'>
    claveVieja = document.userManagerFrm.claveUsuario.value;
</script>
<% request.removeAttribute("tsp.sot.userData"); %>
<% } catch(Exception a ){
    System.out.println("ERROR: "+a.getMessage() );
a.printStackTrace(); } %>