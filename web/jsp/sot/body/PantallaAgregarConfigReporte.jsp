<%@ page session="true" %>
<%@ page errorPage="/ErrorPage.jsp" %>
<%@ page import="java.util.*" %>
<%@ page import="com.tsp.operation.model.beans.*" %>
<%@ taglib uri="http://jakarta.apache.org/taglibs/input-0.90" prefix="input" %>
<%@ taglib uri="http://www.sanchezpolo.com/sot" prefix="tsp"%>
<%@ include file="/WEB-INF/InitModel.jsp" %>

<%
    Usuario loggedUser = (Usuario) session.getAttribute("Usuario");
    String tipoUsuario = loggedUser.getTipo();
    String reporte     = request.getParameter("reporte");
    reporte            = (reporte == null ? "infocliente" : reporte);
    String listaCampos = request.getParameter("listaCampos");
    String codCliDest  = request.getParameter("codCliDest");
    listaCampos        = listaCampos == null || listaCampos.equals("")? "(ninguno)" : listaCampos;
%>
<html>
    <head>
        <title>.: Agregar configuración de reporte :.</title>
        <script src='<%=BASEURL%>/js/PantallaConfigReportes.js' language='javascript'></script>
        <link href="<%=BASEURL%>/css/estilostsp.css" rel="stylesheet" type="text/css">
        <script type="text/javascript" src="<%=BASEURL%>/js/tools.js"></script>
    </head>
    <body onresize="redimensionar()" onload = 'redimensionar()'>
        <div id="capaSuperior" style="position:absolute; width:100%; z-index:0; left: 0px; top: 0px;">
            <jsp:include page="/WEB-INF/toptsp.jsp?encabezado=NUEVA CONFIGURACIÓN DE REPORTES"/>
        </div>
        <div id="capaCentral" style="position:absolute; width:100%; height:87%; z-index:0; left: 0px; top: 100px; overflow: scroll;"> 
            <form name='forma' action="<%=CONTROLLER%>?estado=GestionReportes&accion=Setup&cmd=save" method="post">
            <table align='center' border = '1'>
                <tr>
                    <td width='600'>
                    <table width='100%'>
                    <tr align='center'>
                        <td colspan="3" align="center" class="subtitulo1">CONFIGURACI&Oacute;N DE CAMPOS PARA UN REPORTE</td>
                        <td colspan="1" width="15%" align="left" class="barratitulo"><img src="<%=BASEURL%>/images/titulo.gif" width="32" height="20"></td>
                    </tr>
                    <tr class="fila">
                        <td width="5%">&nbsp;</td>
                        <td width="28%">Tipo de usuario: </td>
                        <td width="52%">
                            <select name="tipoUsuario" id="tipoUsuario" class="listmenu" onchange="cargarDatos('<%=CONTROLLER%>')">
                            <option value='ninguno'>seleccione tipo
                                    <% if( tipoUsuario.equals("ADMIN") ) { %>
                            <option value='Cliente' <%="Cliente".equals(request.getParameter("tipo_usuario"))?"selected":""%>>Cliente
                            <option value='Usuario' <%="Usuario".equals(request.getParameter("tipo_usuario"))?"selected":""%>>Usuario
                                    <% }else if( tipoUsuario.equals("CLIENTEADM") ){ %>
                            <option value='Destinatario' <%="Destinatario".equals(request.getParameter("tipo_usuario"))?"selected":""%>>Destinatario
                                    <% } %>
                            </select>
                        </td>
                        <td>&nbsp;</td>
                    </tr>
                        <% if ( request.getParameter("tipo_usuario") != null ) { %>
                    <tr class="fila">
                    <td>&nbsp;</td>
                    <td><%=request.getParameter("tipo_usuario")%></td>
                    <td>
                        Campo de busqueda: <br>
                        <input style="width:100%" id="campo" type="text" onKeyUp="buscar(document.forma.usuario,this)" ><br>
                        <input:select name="usuario" options="<%=model.confReportService.getUsuarios()%>" attributesText="class='listmenu' id='usuario' style='width=100%'" />
                    </td>
                    <td>&nbsp;</td>
                </tr>
                        <tr class="fila">
                        <td>&nbsp;</td>
                        <td>Reporte:</td>
                        <td>
                            <input:select name="reporte" default="<%=reporte%>" options="<%=model.confReportService.obtenerReportes()%>" attributesText="class='listmenu'" />
                        </td>
                        <td>&nbsp;</td>
                        </tr>
                        <tr class="fila">
                            <td>&nbsp;</td>
                            <td>Campos del reporte: </td>
                            <% String onclick = "accionSeleccionarCampos(document.forma.usuario.value,document.forma.reporte.value,document.forma.tipoUsuario.value,document.forma.listaCampos.value,'agregar','"+BASEURL+"/jsp/sot/body/PantallaCamposReportes.jsp')"; %>
                            <td><tsp:boton value="selecCampo" onclick="<%=onclick%>"/></td>
                            <td>&nbsp;</td>
                        </tr>
                        <tr class="fila">
                            <td>&nbsp;</td>
                            <td>Campos seleccionados: </td>
                            <td><textarea name="listaCampos"  id='listaCampos' rows='5' cols='50' readonly><%=listaCampos%></textarea></td>
                            <td>&nbsp;</td>
                        </tr>
                        <% } %>
                        
                    </table>
                </td>
                </tr>
            </table>
            <p>
            <center>
                <table>
                <tr>
                <td>
                    <tsp:boton value="agregar" onclick="enviarFormAgregarConf()"/>
                </td>
                <td width='30'>&nbsp;</td>
                <td>
                    <% String onclick = "location.href='"+CONTROLLER+"?estado=GestionReportes&accion=Setup'"; %>
                    <tsp:boton value="regresar" onclick="<%=onclick%>"/>
                </td>
                </tr>
            </center>
            </p>
            </form>
        </div>
    </body>
</html>
