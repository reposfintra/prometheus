<!--  
     - Author(s)       :      Karen Reales
     - Date            :      15/05/2006
     - Copyright Notice:      TRANSPORTES SANCHEZ POLO S.A.
-->
<%--   
     - @(#)  
     - Description: Ayuda del reporte de informacion al cliente
--%> 
<%@include file="/WEB-INF/InitModel.jsp"%>

<html>
<head>
<link href="../../../css/estilostsp.css" rel="stylesheet" type="text/css">
<link href="<%= BASEURL %>/css/estilostsp.css" rel="stylesheet" type="text/css">
<title>Ayuda informacion al cliente</title>
<style type="text/css">
<!--
.Estilo1 {
	font-family: Tahoma;
	font-size: 10.0pt;
}
-->
</style></head>
<body> 
<div>
<table width="90%"  border="2" align="center">
            <tr>
            <td >
                <table width="100%" border="0" align="center">
                    <tr  class="subtitulo">
                        <td height="24" align="center">
                          AYUDA INFORMACI�N AL CLIENTE
                        </td>
                    </tr>
                    <tr class="subtitulo1">
                        <td> 
                           GENERAR REPORTE
                        </td>
                    </tr>
					<TR class="ayudaHtmlTexto">
<TD >
  
  <p class=MsoBodyText style='line-height:normal'>Para generar este reporte INFOCLIENTE se ingresa en el men� �CLIENTES� a las diferentes opciones haciendo click en el signo + a la izquierda. Se escoge la primera opci�n �Informaci�n al cliente�.</p> 
  <p class=MsoBodyText style='line-height:normal' align="center"><img  
src="<%=BASEURL%>/images/ayuda/sot/infocliente/image001.jpg" v:shapes="_x0000_s1089 _x0000_s1088 _x0000_s1028"></span></p> 
  <div align="center"></div>
  <p align="center" class=MsoBodyText style='line-height:normal'>&nbsp; </p>
  <br clear=ALL> 
  <p style='text-align:justify'><span >El sistema le muestra el formulario, con unos par�metros para realizar una b�squeda detallada o especifica dependiendo la selecci�n.
El primer filtro de b�squeda es la un rango de fecha, que por defecto cargan la fecha actual. Para cambiar las fechas presione el icono de calendario y seleccione la fecha
</span></p> 
  <p align="center"><img src="<%=BASEURL%>/images/ayuda/sot/infocliente/imagen004.JPG"></p>
  <span style='font-size:10.0pt;font-family:Tahoma'>&nbsp; </span> 
  <p></p> 
  <br clear=ALL> 
  <p class=MsoBodyText2 style='text-align:justify'>Si desea informaci&oacute;n de un cliente especifico digite el nombre del cliente, y presione doble click sobre el nombre del cliente para que le cargue los tipo de cargue que tiene.</p> 
  <p  align="center"><img src="<%=BASEURL%>/images/ayuda/sot/infocliente/imagen005.JPG" >&nbsp; </p> 
  <p style='text-align:justify'>Cargada la informaci&oacute;n de la clasificaci&oacute;n , procese a escoger el tipo o los tipos de viaje y el tipo de carga.</p> 
 <p align="center"> <img src="<%=BASEURL%>/images/ayuda/sot/infocliente/imagen006.JPG"></p>
  <div align="left"><span style='font-size:10.0pt;font-family:Tahoma'> </span>Luego encontramos los estados de los viajes, permiti&eacute;ndoles filtrar la b&uacute;squeda por cualquiera de estos. </div>
  <p align="center" class=MsoBodyText style='line-height:normal'><img src="<%=BASEURL%>/images/ayuda/sot/infocliente/imagen007.JPG" ></p> 
  <div align="left">Si desea realizar la b&uacute;squeda especifica de acuerdo a un numero de documento debe seleccionar el tipo y digitar el numero del documento, sino escoge un tipo de documento el obtiene todos los documentos. </div>
  <p align="center"><img src="<%=BASEURL%>/images/ayuda/sot/infocliente/imagen008.JPG" ></p>
  <span style='font-size:10.0pt;font-family:Tahoma'> </span> 
  <p></p> 
  Luego de seleccionar el tipo de documento procede a digitar el numero del documento 
  <p style='text-align:justify'>&nbsp;</p> 
 <p align="center"> <img src="<%=BASEURL%>/images/ayuda/sot/infocliente/imagen009.JPG" ></p>
  <p style='text-align:justify'>L<span style='font-size:10.0pt;
font-family:Tahoma'>uego, se selecciona el formato de salida del reporte haciendo click en el men� desplegable de �Formato de Salida�. <BR><span style='font-size:10.0pt;
font-family:Tahoma'>Seguidamente, se hace click en �BUSCAR�.</span></span></p> 
  <p align="center"><img src="<%=BASEURL%>/images/ayuda/sot/infocliente/imagen010.JPG"><span
style='font-size:10.0pt;font-family:Tahoma'>&nbsp; </span></p> 
  <br clear=ALL> 
  <p style='text-align:justify'><span style='font-size:10.0pt;
font-family:Tahoma'>Luego de esperar unos segundos, el programa arroja la siguiente pantalla:</span></p> 
  <table cellpadding=0 cellspacing=0 align=center> 
    <tr> 
      <td width=0 height=8 class="Normal"></td> 
    </tr> 
    <tr> 
      <td class="Normal"></td> 
      <td class="Normal"><img src="<%=BASEURL%>/images/ayuda/sot/infocliente/image020.jpg"
  v:shapes="_x0000_s1122"></td> 
    </tr> 
  </table> 
  <span style='font-size:10.0pt;font-family:Tahoma'>&nbsp; </span> 
  <p></p> 
  <br clear=ALL> 
  <p style='text-align:justify'><span style='font-size:10.0pt;
font-family:Tahoma'>Desplazando la pantalla hacia la derecha se tiene lo siguiente:</span></p> 
  <table cellpadding=0 cellspacing=0 align=center> 
    <tr> 
      <td width=0 height=13 class="Normal"></td> 
    </tr> 
    <tr> 
      <td class="Normal"></td> 
      <td class="Normal"><img  src="<%=BASEURL%>/images/ayuda/sot/infocliente/image022.jpg"
  v:shapes="_x0000_s1125 _x0000_s1124 _x0000_s1042"></td> 
    </tr> 
  </table> 
  <span style='font-size:10.0pt;font-family:Tahoma'>&nbsp; </span> 
  <p></p> 
  <br clear=ALL> 
  <p style='text-align:justify'><span style='font-size:10.0pt;
font-family:Tahoma'>Haciendo un click sobre el link deseado de la columna �ltimo Reporte, se obtiene la informaci�n detallada del respectivo viaje.</span></p> 
  <p align="center"><img 
src="<%=BASEURL%>/images/ayuda/sot/infocliente/image024.jpg" v:shapes="_x0000_s1128 _x0000_s1127 _x0000_s1043"><span
style='font-size:10.0pt;font-family:Tahoma'>&nbsp; </span></p> 
  <br clear=ALL> 
  <p style='text-align:justify'><span style='font-size:10.0pt;
font-family:Tahoma'>A continuaci�n el programa arroja la siguiente pantalla con el detalle del viaje seleccionado previamente.</span></p> 
  <table cellpadding=0 cellspacing=0 align=center> 
    <tr> 
      <td width=0 height=1 class="Normal"></td> 
    </tr> 
    <tr> 
      <td class="Normal"></td> 
      <td class="Normal"><img  src="<%=BASEURL%>/images/ayuda/sot/infocliente/image025.jpg"
  v:shapes="_x0000_s1130"></td> 
    </tr> 
  </table> 
  <span style='font-size:10.0pt;font-family:Tahoma'>&nbsp; </span> 
  <p></p> 
  <br clear=ALL> 
  <p style='text-align:justify'><span style='font-size:10.0pt;
font-family:Tahoma'>Si se desea ver el detalle en Excel se hace click en Archivo Excel en la ficha Formato Salida.</span></p> 
  <table cellpadding=0 cellspacing=0 align=center> 
    <tr> 
      <td width=0 height=8 class="Normal"></td> 
    </tr> 
    <tr> 
      <td class="Normal"></td> 
      <td class="Normal"><img  src="<%=BASEURL%>/images/ayuda/sot/infocliente/image027.jpg"
  v:shapes="_x0000_s1133 _x0000_s1132 _x0000_s1044"></td> 
    </tr> 
  </table> 
  <span style='font-size:10.0pt;font-family:Tahoma'>&nbsp; </span> 
  <p></p> 
  <br clear=ALL> 
  <p style='text-align:justify'><span style='font-size:10.0pt;
font-family:Tahoma'>Luego, se hace click en buscar:</span></p> 
  <table cellpadding=0 cellspacing=0 align=center> 
    <tr> 
      <td width=0 height=5 class="Normal"></td> 
    </tr> 
    <tr> 
      <td class="Normal"></td> 
      <td class="Normal"><img  src="<%=BASEURL%>/images/ayuda/sot/infocliente/image029.jpg"
  v:shapes="_x0000_s1136 _x0000_s1135 _x0000_s1045"></td> 
    </tr> 
  </table> 
  <span style='font-size:10.0pt;font-family:Tahoma'>&nbsp; </span> 
  <p></p> 
  <br clear=ALL> 
  <p style='text-align:justify'><span style='font-size:10.0pt;
font-family:Tahoma'>El programa arroja un mensaje donde remite al usuario a manejo de archivos. Se hace click en aceptar.</span></p> 
  <p align="center"><img 
src="<%=BASEURL%>/images/ayuda/sot/infocliente/image031.jpg" v:shapes="_x0000_s1139 _x0000_s1138 _x0000_s1046"><span
style='font-size:10.0pt;font-family:Tahoma'>&nbsp; </span></p> 
  <br clear=ALL> 
  <p style='text-align:justify'><span style='font-size:10.0pt;
font-family:Tahoma'>Luego, se cierra la pantalla.</span></p> 
  <p align="center"><img 
src="<%=BASEURL%>/images/ayuda/sot/infocliente/image033.jpg" v:shapes="_x0000_s1142 _x0000_s1141 _x0000_s1047"><span
style='font-size:10.0pt;font-family:Tahoma'>&nbsp; </span></p> 
  <br clear=ALL> 
  <p style='text-align:justify'><span style='font-size:10.0pt;
font-family:Tahoma'>Seguidamente, se hace click en manejo de archivos.</span></p> 
  <table cellpadding=0 cellspacing=0 align=center> 
    <tr> 
      <td width=0 height=4 class="Normal"></td> 
    </tr> 
    <tr> 
      <td class="Normal"></td> 
      <td class="Normal"><img  src="<%=BASEURL%>/images/ayuda/sot/infocliente/image035.jpg"
  v:shapes="_x0000_s1145 _x0000_s1144 _x0000_s1048"></td> 
    </tr> 
  </table> 
  <span style='font-size:10.0pt;font-family:Tahoma'>&nbsp; </span> 
  <p></p> 
  <br clear=ALL> 
  <p style='text-align:justify'><span style='font-size:10.0pt;
font-family:Tahoma'>Al salir la siguiente pantalla se hace un click sobre el link Download.</span></p> 
  <table cellpadding=0 cellspacing=0 align=center> 
    <tr> 
      <td width=0 height=5 class="Normal"></td> 
    </tr> 
    <tr> 
      <td class="Normal"></td> 
      <td class="Normal"><img  src="<%=BASEURL%>/images/ayuda/sot/infocliente/image037.jpg"
  v:shapes="_x0000_s1148 _x0000_s1147 _x0000_s1049"></td> 
    </tr> 
  </table> 
  <span style='font-size:10.0pt;font-family:Tahoma'>&nbsp; </span> 
  <p></p> 
  <br clear=ALL> 
  <p style='text-align:justify'><span style='font-size:10.0pt;
font-family:Tahoma'>Luego, se genera el reporte en Excel as�:</span></p> 
  <table cellpadding=0 cellspacing=0 align=center> 
    <tr> 
      <td width=0 height=4 class="Normal"></td> 
    </tr> 
    <tr> 
      <td class="Normal"></td> 
      <td class="Normal"><img  src="<%=BASEURL%>/images/ayuda/sot/infocliente/image038.jpg"
  v:shapes="_x0000_s1150"></td> 
    </tr> 
  </table> 
  <span style='font-size:10.0pt;font-family:Tahoma'>&nbsp; </span> 
  <p></p> 
  <br clear=ALL> 
  <p style='text-align:justify'><span style='font-size:10.0pt;
font-family:Tahoma'>Y finalmente, desplazando la pantalla hacia la derecha se encuentra el resto de la informaci�n.</span></p> 
  <table cellpadding=0 cellspacing=0 align=center> 
    <tr> 
      <td width=0 height=5 class="Normal"></td> 
    </tr> 
    <tr> 
      <td class="Normal"></td> 
      <td class="Normal"><img src="<%=BASEURL%>/images/ayuda/sot/infocliente/image040.jpg"
  v:shapes="_x0000_s1153 _x0000_s1152 _x0000_s1050"></td> 
    </tr> 
  </table> 
  </TD>
</TR>
</TABLE>

  </TD>
</TR>
</TABLE>

</div>
 
</body>
</html>
