<!--
- Autor : Alejandro Payares
- Date  : Diciembre 24 del 2005
- Copyrigth Notice : Fintravalores S.A. S.A
- Descripcion : Pagina JSP que permite ver las tablas dentro de tablagen.
-->
<%@page contentType="text/html"%>
<%@page import="java.util.*"%>
<%@include file="/WEB-INF/InitModel.jsp" %>
<%@ taglib uri="http://www.sanchezpolo.com/sot" prefix="tsp"%>
<%@ taglib uri="http://jsptags.com/tags/navigation/pager" prefix="pg" %>
<%
    String path = application.getRealPath(model.menuService.getRealUrl(request.getRequestURI()));
    String datos[] = model.menuService.getContenidoMenu(BASEURL,request.getRequestURI(),path);
%>

<%
   String style = "simple";
   String position =  "bottom";
   String index =  "center";
   int maxPageItems = 10;
   int maxIndexPages = 20;
   int tam =0;
%>
<html>
    <head> 
        <title>.: ADMINISTRACIÓN DE REPORTES :.</title>
        <script language="javascript" src="<%=BASEURL%>/js/tools.js"></script>
        <script language="javascript" src="<%=BASEURL%>/js/boton.js"></script>
        <link href="<%=BASEURL%>/css/estilostsp.css" rel="stylesheet" type="text/css">
		<link href="../../../css/estilostsp.css" rel="stylesheet" type="text/css">
    </head>
    <body onresize="redimensionar()" onload='redimensionar()'>
    <div id="capaSuperior" style="position:absolute; width:100%; z-index:0; left: 0px; top: 0px;">
        <jsp:include page="/toptsp.jsp?encabezado=ADMINISTRACIÓN DE REPORTES"/>
    </div>
    <div id="capaCentral" style="position:absolute; width:100%; height:87%; z-index:0; left: 0px; top: 100px; overflow: scroll;"> 

        <%-- <jsp:useBean id="beanInstanceName" scope="session" class="beanPackage.BeanClassName" /> --%>
        <%-- <jsp:getProperty name="beanInstanceName"  property="propertyName" /> --%>
        <br>
        <% Vector reportes = model.gruposReporteService.obtenerReportes(); 
           if ( reportes != null && !reportes.isEmpty() ) { 
           String link = CONTROLLER+"?estado=Grupos&accion=Reporte&cmd=load";
        %>
        <table align='center' border = '1'>
            <tr>
            <td width='500px'>
                <table width='100%'>
                <tr align='center'>
                    <td colspan="1" align="center" class="subtitulo1">LISTA DE REPORTES CONFIGURABLES<br></td>
                    <td colspan="1" width="35%" align="left" class="barratitulo"><img src="<%=BASEURL%>/images/titulo.gif" width="32" height="20" align="left"><%=datos[0]%></td>
                </tr>
                <tr align='center' class="tblTitulo">
                    <td>NOMBRE DEL REPORTE</td>
                    <td>OPCIONES</td>
                </tr>
                <pg:pager
                items="<%=reportes.size()%>"
                index="<%= index %>"
                maxPageItems="<%= maxPageItems %>"
                maxIndexPages="<%= maxIndexPages %>"
                isOffset="<%= true %>"
                export="offset,currentPageNumber=pageNumber"
                scope="request"
                >
                            <%  tam = reportes.size();
                                for (int j = offset.intValue(), l = Math.min(j + maxPageItems, reportes.size()); j < l; j++){
                                    Hashtable fila = (Hashtable)reportes.elementAt(j);%>
                                <pg:item>
                                    <tr class="<%=(j % 2 == 0 )?"filagris":"filaazul"%>" onMouseDown='cambiarColor(this)' style="cursor:hand">
                                        <td class='bordereporte' align='center'><%= fila.get("nombre") %></td>
                                        <td class='bordereporte' nowrap align='center'>
                                            <a href="<%= link + "&codrpt=" + fila.get("codigo") + "&nombreReporte="+fila.get("nombre") %>" class="Simulacion_Hiper">EDITAR GRUPOS</a>&nbsp;&nbsp;&nbsp;
                                            <a href="<%= link + "CamposGral&codrpt=" + fila.get("codigo") + "&nombreReporte="+fila.get("nombre")  %>" class="Simulacion_Hiper">EDITAR CAMPOS</a>
                                        </td>
                                    </tr>
                                </pg:item>
                             <% } %>
                <tr class="fila">
                    <table width="100%" border="0">
                            <tr>
                                    <td td height="15" nowrap align="center"> 
                                    <pg:index>
                                            <jsp:include page="/WEB-INF/jsp/googlesinimg.jsp" flush="true"/>      
                                    </pg:index> 
                                    </td>
                            </tr>
                    </table>
                </tr>
                </pg:pager>
            </table>
            </td>
            </tr>
        </table>
        <p>
        <center>
            <table>
                <tr>
                    <td>
                        <% String onclick = "location.href='"+BASEURL+"/jsp/sot/body/PantallaTablaGenNueva.jsp?titulo=Nueva tabla'"; %>
                        <tsp:boton value="agregar" onclick="<%=onclick%>"/>
                    </td>
                    <td width='30'>&nbsp;</td>
                    <td>
                        <tsp:boton value="eliminar" onclick="validarSeleccion()"/>
                    </td>
                    <td width='30'>&nbsp;</td>
                    <td>
                        <tsp:boton value="salir" onclick="window.close()"/>
                    </td>
                </tr>
            </table>
        </center>
        </p>
        <% }
           else {%>
        <table border="2" align="center">
            <tr>
                <td><table width="100%"  border="1" align="center"  bordercolor="#F7F5F4" bgcolor="#FFFFFF">
                <tr>
                <td width="229" align="center" class="mensajes">No hay tablas en el sistema para administrar, haga click en agregar para crear una tabla</td>
                <td width="29" background="<%=BASEURL%>/images/cuadronaranja.JPG">&nbsp;</td>
                <td width="58">&nbsp;</td>
            </tr>
            </table></td>
            </tr>
        </table>
        <br>
        <center>
                <% String onclick = "location.href='"+BASEURL+"/jsp/sot/body/PantallaTablaGenNueva.jsp?titulo=Nueva tabla'"; %>
            <tsp:boton value="agregar" onclick="<%=onclick%>"/>
        </center>
        <% } %>
    </div>
    <% String str = datos[1];
       str = str.replaceAll("HelpD.jsp","HelpD.jsp#adminTablas");
       str = str.replaceAll("HelpF.jsp","HelpF.jsp#adminTablas");
       out.println(str);
    %>
    </body>
</html>
