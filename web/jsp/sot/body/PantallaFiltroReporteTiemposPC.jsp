<!--
- Autor : Alejandro Payares
- Date  : Febrero 13 del 2006  7:23 PM
- Copyrigth Notice : Fintravalores S.A. S.A
- Descripcion : Pagina JSP que permite escojer los parametros para ejecutar el reporte.
-->
<%@page contentType="text/html"%>
<%@page import="com.tsp.operation.model.beans.*, java.util.*"%>
<%@include file="/WEB-INF/InitModel.jsp" %>
<%@ taglib uri="http://jakarta.apache.org/taglibs/input-0.90" prefix="input" %>
<%@ taglib uri="http://www.sanchezpolo.com/sot" prefix="tsp"%>

<html>
    <head> 
        <title>.: REPORTE DE TIEMPOS ENTRE PUESTOS DE CONTROL :.</title>
        <script language="javascript" src="<%=BASEURL%>/jsp/sot/js/PantallaFiltroReporteTiemposPC.js"></script>
        <script language="javascript" src="<%=BASEURL%>/jsp/sot/js/tools.js"></script>
        <link href="<%=BASEURL%>/css/estilostsp.css" rel="stylesheet" type="text/css">
    </head>
    <body onresize="redimensionar()" onload='redimensionar()'>
        <div id="capaSuperior" style="position:absolute; width:100%; z-index:0; left: 0px; top: 0px;">
            <jsp:include page="/toptsp.jsp?encabezado=REPORTE DE TIEMPOS ENTRE PUESTOS DE CONTROL"/>
        </div>
        <div id="capaCentral" style="position:absolute; width:100%; height:87%; z-index:0; left: 0px; top: 100px; overflow: scroll;"> 

            <%-- <jsp:useBean id="beanInstanceName" scope="session" class="beanPackage.BeanClassName" /> --%>
            <%-- <jsp:getProperty name="beanInstanceName"  property="propertyName" /> --%>
            <br>
            <%if(request.getParameter("mensaje") != null){%>
            <table border="2" align="center">
                <tr>
                    <td><table width="100%"  border="1" align="center"  bordercolor="#F7F5F4" bgcolor="#FFFFFF">
                        <tr>
                            <td width="229" align="center" class="mensajes"><%=request.getParameter("mensaje")%></td>
                            <td width="29" background="<%=BASEURL%>/images/cuadronaranja.JPG">&nbsp;</td>
                            <td width="58">&nbsp;</td>
                        </tr>
                    </table></td>
                </tr>
            </table>
            <br>
            <%}
            String action = CONTROLLER+"?estado=ReporteTiempos&accion=PuestosDeControl&cmd=show"; %>
            <form name='forma' method="post"  action="<%=action%>">
                <table align='center' border = '1'>
                <tr>
                    <td width='550px'>
                    <table width='100%'>
                    <tr align='center'>
                        <td colspan="2" align="center" class="subtitulo1">PARAMETROS DEL REPORTE</td>
                        <td colspan="1" width="15%" align="left" class="barratitulo"><img src="<%=BASEURL%>/images/titulo.gif" width="32" height="20"></td>
                    </tr>
                    <tr class='fila'>
                        <td  align='left' colspan="3">
                            <table width="100%">
                                <tr class='fila'>
                                    <td> Fecha Inicial: </td>
                                    <td> <tsp:Date fechaInicial="hoy" formulario="forma" clase="textbox" name="fechaini" otros="size='20' alt='Haga click aqu&iacute; para escoger la fecha'" /> </td>
                                    <td width='50'>&nbsp;</td>
                                    <td align='left'> Fecha Final: </td>
                                    <td> <tsp:Date fechaInicial="hoy" formulario="forma" clase="textbox" name="fechafin" otros="size='20' alt='Haga click aqu&iacute; para escoger la fecha'" /> </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr class="fila">
                            <td colspan='3'>Formato de Salida:&nbsp;&nbsp;
                                <select class='listmenu' name="formatoSalida" >
                                <option value="web" selected="selected">P&aacute;gina WEB
                                <option value="excel">Archivo Excel
                                </select>
                            </td>
                        </tr>
                    </table>
                </td>
                </tr>
                </table>
            </form>
            <p>
            <center>
                <table>
                <tr>
                    <td>
                        <tsp:boton value="buscar" onclick="validarFormularioRTPC(document.forma,document.forma.fechaini.value,document.forma.fechafin.value)"/>
                    </td>
                    <td width='30'>&nbsp;</td>
                    <td>
                        <tsp:boton value="salir" onclick="window.close()"/>
                    </td>
                </tr>
		</table>
            </center>
            </p>
        </div>
    </body>
    <tsp:InitCalendar/>
</html>
