<!--
- Autor : Alejandro Payares
- Date  : Mayo 9 del 2006
- Copyrigth Notice : Fintravalores S.A. S.A
- Descripcion : Pagina JSP que permite agregar un nuevo registro dentro de una subtabla de tablagen.
-->
<%@page contentType="text/html"%>
<%@page import="com.tsp.operation.model.beans.*, java.util.*"%>
<%@include file="/WEB-INF/InitModel.jsp" %>
<%@ taglib uri="http://jakarta.apache.org/taglibs/input-0.90" prefix="input" %>
<%@ taglib uri="http://www.sanchezpolo.com/sot" prefix="tsp"%>
<%@page import="com.tsp.operation.model.beans.*"%>
<%
    String path = application.getRealPath(model.menuService.getRealUrl(request.getRequestURI()));
    String datos[] = model.menuService.getContenidoMenu(BASEURL,request.getRequestURI(),path);
    GruposReporte gr = (GruposReporte) request.getAttribute("gr");
    Vector v = gr.getGrupos(); %>    
%>
<html>
    <head> 
        <title>.: ADMINISTRACIÓN DE GRUPOS DEL REPORTE :.</title>
        <script language="javascript" src="<%=BASEURL%>/js/tools.js"></script>
        <script language="javascript" src="<%=BASEURL%>/js/boton.js"></script>
        <script language="javascript" src="<%=BASEURL%>/jsp/sot/js/PantallaAdminGruposReporte.js"></script>
        <script language="javascript" src="<%=BASEURL%>/jsp/sot/js/colorpickerFlooble.js"></script>
        <link href="<%=BASEURL%>/css/estilostsp.css" rel="stylesheet" type="text/css">
        <link href="../../../css/estilostsp.css" rel="stylesheet" type="text/css">
        <script>
            function initGrupos(){
            urlPaginaPaleta = '<%= BASEURL + "/jsp/sot/PantallaPaletaColores.jsp" %>';
            var grupo = null;
            <%  for( int i=0; i<v.size(); i++ ){
                    Hashtable grupo = (Hashtable) v.elementAt(i);
                    out.println("    grupo = gr.agregarGrupo('"+grupo.get(GruposReporte.LLAVE_NOMBRE_GRUPO)+"','"+grupo.get(GruposReporte.LLAVE_COLOR_GRUPO)+"');");
                    Vector campos = gr.obtenerCamposDeGrupo(grupo.get(GruposReporte.LLAVE_NOMBRE_GRUPO).toString());
                    //System.out.println("campos "+grupo.get("nombre")+" = "+campos.size());
                    for( int j=0; j<campos.size(); j++ ){
                        Hashtable campo = (Hashtable) campos.elementAt(j);
                        out.println("    grupo.agregarCampo('"+ campo.get(GruposReporte.LLAVE_NOMBRE_CAMPO)+ "','"+
                                                                campo.get(GruposReporte.LLAVE_TITULO_CAMPO)+ "');");
                    }					
                } %>
            }
        </script>
    </head>
    <body onresize="redimensionar()" onload='redimensionar();initGrupos();cargarFormulario()'>
    <div id="capaSuperior" style="position:absolute; width:100%; z-index:0; left: 0px; top: 0px;">
        <jsp:include page="/toptsp.jsp?encabezado=ADMINISTRACIÓN DE GRUPOS"/>
    </div>
    <div id="capaCentral" style="position:absolute; width:100%; height:87%; z-index:0; left: 0px; top: 100px; overflow: scroll;"> 
        <br>
            <%if(request.getParameter("mensaje") != null){%>
        <table border="2" align="center">
            <tr>
                <td>
                    <table width="100%"  border="1" align="center"  bordercolor="#F7F5F4" bgcolor="#FFFFFF">
                        <tr>
                            <td width="229" align="center" class="mensajes"><%=request.getParameter("mensaje")%></td>
                            <td width="29" background="<%=BASEURL%>/images/cuadronaranja.JPG">&nbsp;</td>
                            <td width="58">&nbsp;</td>
                        </tr>
                    </table>
                </td>
            </tr>
        </table>
        <br>
            <%	}  %>
        <form name='forma' method="post"  action="">
        <input name='codigoReporte' value='<%= request.getParameter("codigoReporte") %>' type='hidden' />
        <table align='center' border = '1'>
            <tr>
                <td width='600px'>
                <table width='100%'>
                <tr align='center'>
                    <td colspan="4" align="center" class="subtitulo1">CONFIGURANDO: <%= request.getParameter("nombreReporte") %><br></td>
                    <td colspan="1" width="20%" align="left" class="barratitulo"><img src="<%=BASEURL%>/images/titulo.gif" width="32" height="20" align="left"><%=datos[0]%></td>
                </tr>
                <tr class="fila">
                    <td width="164" align='center'>
                    <table align='center' width="100%"  border="0">
                    <tr>
                        <td align='center'>
                            <tsp:boton value="agregarGrupo" onclick="<%="ventanaAgregarGrupo('"+BASEURL+"/jsp/sot/body/PantallaAgregarGrupo.jsp')"%>"/>
                        </td>
                    </tr>
                    <tr>
                        <td><div align="center"></div></td>
                    </tr>
                    <tr>
                    <td align='center'>
                        <tsp:boton value="eliminarGrupo" onclick="eliminarGrupo(true)"/>
                    </td>
                </tr>
                    </table>
                </td>
                <td colspan="4" align="center">
                    Grupos de campos<br>
                    <select name="listaGrupos" size="4" onchange='cargarDatosGrupo()'>
                    </select>
                </td>
            </tr>
                    <tr align='center'>
                        <td colspan="3" align="center" class="subtitulo1">DATOS DEL GRUPO <br></td>
                        <td colspan="2" align="left" class="barratitulo"><img src="<%=BASEURL%>/images/titulo.gif" width="32" height="20" align="left"></td>
                    </tr>
                    <tr class="fila">
                    <td colspan="2" >Nombre:
                    <input name="nombreGrupo" onkeypress="soloLetrasGrupos(event);"/></td>
                    <td width='40' align="right"></td>
                    <td colspan="2" align='right'>
                        Color:&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                        <input name="colorGrupofield" id="colorGrupofield" value="" readOnly/>
                        &nbsp;&nbsp;&nbsp;
                        <a 
                        id="colorGrupo" 
                        style="BORDER-RIGHT: #000000 1px solid; BORDER-TOP: #000000 1px solid; FONT-SIZE: 10px; BORDER-LEFT: #000000 1px solid; BORDER-BOTTOM: #000000 1px solid; FONT-FAMILY: Verdana; TEXT-DECORATION: none" 
                        href="javascript:pickColor('colorGrupo');"
                        >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</a>&nbsp;								
                        <script language=javascript>relateColor('colorGrupo', getObj('colorGrupofield').value);</script>
                    </td>
                    </tr>
                    <tr class="fila">
                    <td colspan="5" align="center">
                        <tsp:boton value="aplicar" onclick="aplicarCambios(true)"/>
                    </td>
                    </tr>
                    <tr class="fila">
                        <td colspan="2" align="center">Campos del grupo</td>
                        <td colspan="1"></td>
                        <td colspan="2" align="center">Todos los campos</td>
                    </tr>
                    <tr class="fila">
                        <td colspan="2" align="center"> 
                        <select name="camposGrupo" size="7" id="camposGrupo">
                        </select></td>
                        <td colspan="1" align='center'>
                            <tsp:boton value="envIzq" onclick="mover(document.forma.todos, document.forma.camposGrupo);"/><br>
                            <tsp:boton value="enviarIzq" onclick="moverTodos(document.forma.todos, document.forma.camposGrupo);"/><br>
                            <tsp:boton value="envDer" onclick="mover(document.forma.camposGrupo, document.forma.todos );"/><br>
                            <tsp:boton value="enviarDerecha" onclick="moverTodos(document.forma.camposGrupo, document.forma.todos );"/><br>
                        </td>
                        <td colspan="2" align="center"><select name="todos" size="7" id="todos" style='z-index=-1'>
                        </select></td>
                    </tr>
                </table>
            </td>
            </tr>
        </table>
        </form>
        <p>
        <center>
            <table>
                <tr>
                    <td>
                        <tsp:boton value="aceptar" onclick="<%= "enviarFormulario('"+CONTROLLER+"?estado=Grupos&accion=Reporte&cmd=save')" %>"/>
                    </td>
                    <td width='20'>&nbsp;</td>
                    <td>
                        <tsp:boton value="regresar" onclick="<%="location.href ='" + CONTROLLER + "?estado=Grupos&accion=Reporte&cmd=list'"%>"/>
                    </td>
                    <td width='20'>&nbsp;</td>
                    <td>
                        <tsp:boton value="salir" onclick="<%="window.close()" %>"/>
                    </td>
                </tr>
            </table>
        </center>
        </p>
    </div>
        <% String str = datos[1];
           str = str.replaceAll("PantallaTablaGenNuevoRegistroHelpD.jsp","PantallaTablagenListHelpD.jsp#Registro");
           str = str.replaceAll("PantallaTablaGenNuevoRegistroHelpF.jsp","PantallaTablagenListHelpF.jsp#Registro");
           out.println(str);
        %>
    </body>
</html>