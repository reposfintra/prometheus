<!--
- Autor : Ing. David Lamadrid
- Date  : 5 de Octubre de 2005
- Copyrigth Notice : Fintravalores S.A. S.A
-->
<%--
-@(#)
--Descripcion : Pagina JSP, que se encarga de realizar y mostrar busqueda de los tipos de impuestos
--%>

<%-- Declaracion de librerias--%>
<%@page contentType="text/html"%>
<%@page session="true"%>
<%@page import="java.util.*"%>
<%@page import="com.tsp.operation.model.*"%>
<%@page import="com.tsp.util.*"%>
<%@page import="com.tsp.operation.model.beans.*, java.util.*"%>
<%@include file="/WEB-INF/InitModel.jsp"%>
<%@taglib uri="http://jakarta.apache.org/taglibs/input-0.90" prefix="input" %>
<html>
<head><title>Tipo de Impuestos</title>
<link href="../css/estilostsp.css" rel="stylesheet" type="text/css"> 
<link href="<%=BASEURL%>/css/estilostsp.css" rel="stylesheet" type="text/css"> 
<script type='text/javascript' src="<%=BASEURL%>/js/Validacion.js"></script> 
<script type='text/javascript' src="<%= BASEURL %>/js/boton.js"></script>
<script type='text/javascript' src="<%=BASEURL%>/js/Ingreso_detalle.js"></script> 
 
<script>
function asignarImpuestoS(  codigo ){
	//asigno codigo a la reteica
	var cod_rica = parent.opener.document.getElementById("CodImpCliente");
	cod_rica.value = codigo;
	window.close();
}
</script>
</head>
<body>
<div id="capaSuperior" style="position:absolute; width:100%; height:100px; z-index:0; left: 0px; top: 0px;">
 <jsp:include page="/toptsp.jsp?encabezado=Impuestos "/>
</div>

<div id="capaCentral" style="position:absolute; width:100%; height:87%; z-index:0; left: 0px; top: 100px; overflow: scroll;">
<%
    String distrito = (String) session.getAttribute ("Distrito");
	//fecha actual
    java.util.Date utilDate = new java.util.Date(); 
    long lnMilisegundos = utilDate.getTime();
    java.sql.Timestamp sqlTimestamp = new java.sql.Timestamp(lnMilisegundos);
	
	//vector de tipos de impuestos
	Vector vImpuestos= model.TimpuestoSvc.buscarImpuestoCliente("RFTECL",""+sqlTimestamp,distrito, "");
%>
<form name="forma1" action="" method="post">      
<table width="450" border="2" align="center">
  <tr>
    <td>
<table width="100%" align="center" > 
  <tr>
    <td width="50%" height="24"  class="subtitulo1"><p align="left">Impuestos Tipo RFTE</p></td>
    <td width="50%"  class="barratitulo"><img src="<%=BASEURL%>/images/titulo.gif"></td>
  </tr>
</table>
<table width="100%"  align="center" >
 <tr class="tblTitulo" id="titulos" >
    <td width="15%" align="center">Codigo </td>
    <td width="20%" align="center">Impuesto</td>
	<td width="65%" align="center">Descripción</td>
 </tr>
<% 
    String fecha =""+sqlTimestamp; 
    for(int i=0; i< vImpuestos.size();i++){
        Tipo_impuesto tipo=(Tipo_impuesto)vImpuestos.elementAt(i);
%>  
        <tr class="<%=(i % 2 == 0 )?"filagris":"filaazul"%>" onMouseOver='cambiarColorMouse(this)'   style="cursor:hand" title="Tipo de impuestos" onClick="asignarImpuestoS('<%=tipo.getCodigo_impuesto()%>');">

        <td align="center"><%=tipo.getCodigo_impuesto()%></td>
        <td align="center"><%=tipo.getPorcentaje1()+ " % "%></td>
		<td align="center"><%=tipo.getDescripcion()%></td>
        </tr>
<%
    }
%>  
</table>
</td>
</tr>
</table>
</form>
<center><img src="<%=BASEURL%>/images/botones/salir.gif"  name="salir" title="Salir..." onClick="window.close();" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);"></center>
</div>
</body>
</html>
