<%-- 
    Document   : PantallaAdmonUsuario
    Created on : 29/05/2015, 04:50:24 PM
    Author     : egonzalez
--%>

<%@page import="com.google.gson.JsonArray"%>
<%@page import="com.google.gson.JsonObject"%>
<%@page import="com.tsp.operation.controller.GestionUsuariosAdmonUserAction"%>
<%@page import="com.tsp.operation.model.beans.Usuario"%>
<%@page import="com.tsp.operation.model.services.UsuarioService"%>
<%@page contentType="text/html" pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
        <link href="/fintra/css/estilostsp.css" rel="stylesheet" type="text/css">
        <link type="text/css" rel="stylesheet" href="/fintra/css/jquery/jquery-ui/jquery-ui.css" />

        <title>Administraci�n de Usuarios</title>
        <script type="text/javascript" src="/fintra/js/jquery/jquery-ui/jquery.min.js"></script>
        <script type="text/javascript" src="/fintra/js/jquery/jquery-ui/jquery.ui.min.js"></script>            
        <script src="/fintra/js/admonUsuarios.js" type="text/javascript"></script>
        <style>
            input[type='text'] .mayuscula {
                text-transform:uppercase;
            }
         </style>
    </head>
    <%
        UsuarioService us = new UsuarioService();
        boolean cmd = (request.getParameter("cmd") != null && request.getParameter("cmd").equalsIgnoreCase("edit"));
        Usuario u = new Usuario();
        boolean activo = true;
        String idusuario = (request.getParameter("idUser") != null)
                           ? request.getParameter("idUser") : "";
        
        if (cmd || !idusuario.equalsIgnoreCase("")) {
            us.buscaUsuario(idusuario);
            if (us.getUsuario() != null) {
                u = us.getUsuario();
                activo = (u.getEstado().equalsIgnoreCase("A"));
            } else {
                cmd = false;
            }
        } else {
            cmd = false;
        }
        
        GestionUsuariosAdmonUserAction gua = new GestionUsuariosAdmonUserAction();
        JsonObject ca = gua.cargaInicial(idusuario);
        JsonArray vector;
        JsonObject elemento;
    %>
    <body>
        <jsp:include page="/toptsp.jsp?encabezado=Administraci�n de Usuarios"/>
        <div id="capaCentral" style="text-align: center;">
            <center>
                <div class="ui-jqgrid ui-widget ui-widget-content ui-corner-all"
                     style="min-width: 550px; max-width: 900px; padding: 2em; margin: 2em;">
                    <table>
                        <tr><td colspan="4" class="ui-dialog-titlebar ui-widget-header ui-corner-all ui-helper-clearfix" style="text-align: center;">DATOS BASICOS DEL USUARIO</td></tr>
                        <tr>
                            <td>NIT o CC <span style="color:red;">*</span></td>
                            <td><input type="text" id="nit" maxlength="20" value="<%=(cmd) ? u.getCedula() : ""%>" <%=(!activo) ? "readonly" : ""%>></td>
                            <td>Estado</td>
                            <td>
                                <select id="estado" style="width: 96%;">c
                                    <option value="A" <%= (activo ? "selected" : "")%>>ACTIVO</option>
                                    <% if (cmd) {%> <option value="I" <%= (activo ? "" : "selected")%>>INACTIVO</option> <%}%>
                                </select>
                            </td>
                        </tr>
                        <tr>
                            <td>Nombre o Razon social <span style="color:red;">*</span></td>
                            <td colspan="3">
                                <input type="text" class="mayuscula" id="nombre" maxlength="150" value="<%=(cmd) ? u.getNombre() : ""%>" pattern=".+" style="width: 96%;" <%=(!activo) ? "readonly" : ""%>>
                            </td>
                        </tr>
                        <tr>
                            <td>Direccion <span style="color:red;">*</span></td>
                            <td colspan="3">
                                <input type="text" class="mayuscula" id="direccion" maxlength="50" value="<%=(cmd) ? u.getDireccion() : ""%>" style="width: 96%;" <%=(!activo) ? "readonly" : ""%>>
                            </td>
                        </tr>
                        <tr>
                            <td>Correo <span style="color:red;">*</span></td>
                            <td colspan="3">
                                <input type="text" class="mayuscula" id="email" maxlength="50" value="<%=(cmd) ? u.getEmail() : ""%>" style="width: 96%;" <%=(!activo) ? "readonly" : ""%>>
                            </td>
                        </tr>
                        <tr>
                            <td>Telefono <span style="color:red;">*</span></td>
                            <td>
                                <input type="text" id="telefono" maxlength="50" value="<%=(cmd) ? u.getTelefono() : ""%>" style="width: 96%;" <%=(!activo) ? "readonly" : ""%>>
                            </td>
                            <td>Nit Propietario</td>
                            <td>
                                <input type="text" id="npropietario" value="<%=(cmd) ? u.getNitPropietario() : ""%>" class="mayuscula" style="width: 96%;" <%=(!activo) ? "readonly" : ""%>>
                            </td>
                        </tr>
                        <tr>
                            <td>Pais <span style="color:red;">*</span></td>
                            <td>
                                <select id="pais" style="width: 96%;" onchange="locacion('dptos',this.value)">
                                    <option value=""></option>
                                    <% 
                                    vector = ca.get("paises").getAsJsonArray();
                                    for (int i = 0; i < vector.size(); i++) {
                                        elemento = (JsonObject) vector.get(i);
                                    %>
                                    <option value=<%=elemento.get("cod")%>
                                            <%=(cmd && u.getCodPais().equals(elemento.get("cod").getAsString()))? "selected":""%>>
                                        <%=elemento.get("nombre").getAsJsonPrimitive().getAsString()%>
                                    </option>
                                    <%}%>
                                </select>
                            </td>
                            <%
                            elemento = (cmd) ? ca.get("locacion").getAsJsonObject() : new JsonObject();
                            %>
                            <td>Departamento <span style="color:red;">*</span></td>
                            <td>
                                <select id="depto" style="width: 96%;" onchange="locacion('ciuds', this.value)">
                                    <%if(elemento.get("cdpto") != null) {%>
                                    <option value=<%=elemento.get("cdpto")%>>
                                        <%=elemento.get("ndpto").getAsJsonPrimitive().getAsString()%>
                                    </option>
                                    <% } %>
                                </select>
                            </td>
                        </tr>
                        <tr>
                            <td>Ciudad <span style="color:red;">*</span></td>
                            <td>
                                <select id="ciudad" style="width: 96%;">
                                    <%if(elemento.get("cciu") != null) {%>
                                    <option value=<%=elemento.get("cciu")%>>
                                        <%=elemento.get("nciu").getAsJsonPrimitive().getAsString()%>
                                    </option>
                                    <% } %>
                                </select>
                            </td>
                        </tr>
                        <tr>
                            <td>Tipo de usuario</td>
                            <td>
                                <select id="tipo" style="width: 96%;">
                                    <option value='ADMIN' <%= (cmd && u.getTipo().equals("ADMIN")? "selected" : "") %>>Administrador</option>
                                    <option value='CLIENTETSP' <%= (cmd && u.getTipo().equals("CLIENTETSP")? "selected" : "") %>>Cliente de Fintra</option>
                                    <option value='TSPUSER' <%= (cmd && u.getTipo().equals("TSPUSER")? "selected" : "") %>>Usuario de Fintra</option>
                                </select>
                            </td>
                            <td>Agencia</td>
                            <td>
                                <select id="agencia" style="width: 96%;">
                                    <option value=""></option>
                                    <% 
                                    vector = ca.get("agencias").getAsJsonArray();
                                    for (int i = 0; i < vector.size(); i++) {
                                        elemento = (JsonObject) vector.get(i);
                                    %>
                                    <option value=<%=elemento.get("cod")%>
                                            <%=(cmd && u.getId_agencia().equals(elemento.get("cod").getAsString()))? "selected":""%>>
                                        <%=elemento.get("nombre").getAsJsonPrimitive().getAsString()%>
                                    </option>
                                    <%}%>
                                </select>
                            </td>
                        </tr>
                        <tr><td colspan="4" class="ui-dialog-titlebar ui-widget-header ui-corner-all ui-helper-clearfix" style="text-align: center;">DATOS PARA ACCESO AL SISTEMA</td></tr>
                        <!--input type="checkbox" onchange="vista_clave(this.checked)"-->
                        <tr>
                            <td>ID Usuario <span style="color:red;">*</span></td>
                            <td>
                                <input type="text" id="idusuario" class="mayuscula" maxlength="10" onblur="verifica_usuario();"
                                       value="<%= (cmd) ? u.getLogin() : ""%>" style="width: 96%;" <%=(cmd) ? "readonly" : ""%>>
                            </td>
                            <td>cambiar clave</td>
                            <td>
                                <input type="checkbox" id="cambio_clave" <%= (!cmd || u.isCambiarClaveLogin()) ? "checked" : ""%> >
                            </td>
                      </tr>
                      <tr>
                            <td style="">Usuario App</td>
                            <td>
                                <input type="checkbox" id="app" onclick="verifica_usuarioApp()">
                            </td>
                            <td style="">Id Usuario App</td>
                            <td>
                                <input type="text" id="idapp"  style="width: 96%;" disabled="true">
                            </td>
                        </tr>
                        <tr disabled>
                            <td>Clave <span style="color:red;">*</span></td>
                            <td><input type="password" id="clave" style="width: 96%;"
                                       value="<%= (cmd) ? u.getPassword() : ""%>"></td>
                            <td>Reescriba Clave</td>
                            <td><input type="password" id="clave_ver" style="width: 96%;"
                                       value="<%= (cmd) ? u.getPassword() : ""%>"
                                       onkeyup="verifica_clave()"></td>
                        </tr>

                        <tr>
                            <td colspan="4" class="ui-dialog-titlebar ui-widget-header ui-corner-all ui-helper-clearfix" style="text-align: center;">
                                PERFILES
                                <% if (activo) {%>
                                <img style="float: right; margin-right: 0.70em;" title="agregar compa�ia" alt="agregar compa�ia"
                                     src="/fintra/images/botones/iconos/mas.gif" width="14" height="14"
                                     onclick="agregarCompania(false,'<%=idusuario%>')"> 
                                <%}%>
                            </td>
                        </tr>
                        <!--tr><td colspan="4"><label onclick="agregarCompania(false,'<%=idusuario%>')"> agregar compa�ia </label></td></tr-->
                        <tr>
                            <td colspan="4">
                                <table>
                                <% if (!activo) {%>
                                <a style="bottom: 0px; left: 0px; position: relative; right: 0px; top: 0px;" onclick="alert('Usuario anulado')"></a>
                                <%}%>
                                    <tr id="tbl_empresas">
                                        <%
                                        vector = (cmd) ? ca.get("perfiles").getAsJsonArray() : new JsonArray();
                                        for (int i = 0; i < vector.size(); i++) {
                                            elemento = (JsonObject) vector.get(i);
                                        %>
                                        <td>
                                            <span onclick='agregarCompania(<%=elemento.get("dstrct")%>,"<%=idusuario%>")'><%=elemento.get("empresa").getAsJsonPrimitive().getAsString()%></span>
                                            <ol name="empresas" id=<%=elemento.get("dstrct")%>>
                                                <% for (int j = 0; j < elemento.get("perfiles").getAsJsonArray().size(); j++) {%>
                                                <li id="<%=((JsonObject)elemento.get("perfiles").getAsJsonArray().get(j)).get("perfil").getAsJsonPrimitive().getAsString()%>">
                                                    <input type="checkbox" <%= (((JsonObject)elemento.get("perfiles").getAsJsonArray().get(j)).get("estado").getAsBoolean()) ? "checked" : ""%>
                                                           id='<%= elemento.get("dstrct").getAsJsonPrimitive().getAsString()+"_"
                                                                   +((JsonObject)elemento.get("perfiles").getAsJsonArray().get(j)).get("perfil").getAsJsonPrimitive().getAsString()%>'>
                                                    <%=((JsonObject)elemento.get("perfiles").getAsJsonArray().get(j)).get("nombre").getAsJsonPrimitive().getAsString()%>
                                                </li>
                                                <%}%>
                                            </ol>
                                        </td>
                                        <%}%>
                                    </tr>
                                </table>                            
                            </td>
                        </tr>
                        <tr>
                            <td colspan="3" style="border-top: 1px solid rgb(0, 128, 0); padding: 0.5em;">
                                <span id="mensaje" style="color:red;"></span>
                            </td>
                            <td style="border-top: 1px solid rgb(0, 128, 0); padding: 0.5em; text-align: right;"> 
                                <% if (!cmd) { %>
                                <button id="aceptar_btn" class="ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only" role="button" aria-disabled="false"
                                        onclick="crearUsuario('1')">
                                    <span class="ui-button-text">Crear</span>
                                </button>
                                <% } else {%>
                                <button id="actualizar_btn" class="ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only" role="button" aria-disabled="false"
                                        onclick="crearUsuario('2')">
                                    <span class="ui-button-text">Actualizar</span>
                                </button>
                                <% }%>
                            </td>
                        </tr>
                    </table>
                </div>
            </center>
        </div>

        <div id="dialogCompania" class="ventana" style="display:none;">
            <table  style="width: 100%; height: 100%;">
                <thead>
                    <tr>
                        <td>
                            <select id="empresa" onchange="agregarCompania(this.value,'<%=idusuario%>', true)" style="width: 100%;">
                                <% 
                                vector = ca.get("empresas").getAsJsonArray();
                                for (int i = 0; i < vector.size(); i++) {
                                    elemento = (JsonObject) vector.get(i);
                                %>
                                <option value=<%=elemento.get("dstrct")%>>
                                    <%=elemento.get("nombre").getAsJsonPrimitive().getAsString()%>
                                </option>
                                <%}%>
                            </select>
                        </td>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td>
                            <select id="perfiles" multiple style="width: 100%; height: 100%;" size="15">
                            </select>
                        </td>
                    </tr>
                </tbody>
            </table>
        </div>

        <div id="dialogLoading" style="display:none;">
            <p style="font-size: 12px;text-align:justify;" id="msj">Texto </p> <br/>
            <center>
                <img src="/fintra/images/cargandoCM.gif"/>
            </center>
        </div>

    </body>
</html>
