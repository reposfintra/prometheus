<!--
     - Author(s)       :      MARIO FONTALVO
     - Date            :      10/12/2005  
     - Copyright Notice:      TRANSPORTES SANCHEZ POLO S.A.
  -->
 <%--   
     - @(#)  
     - Description: Formulario asignar clientes a los diferentes soportes
 --%> 
<%@page session   = "true"%> 
<%@page errorPage = "/error/ErrorPage.jsp"%>
<%@page import    = "java.util.*" %>
<%@page import    = "com.tsp.operation.model.beans.*" %>
<%@include file   = "/WEB-INF/InitModel.jsp"%>
<%@include file="/jsp/masivo/asignaciones/generacionJS.jsp"%>



<%@ taglib uri="http://jakarta.apache.org/taglibs/input-0.90" prefix="input" %>
<%@page import="com.tsp.util.*"%>
<%@taglib uri="http://jsptags.com/tags/navigation/pager" prefix="pg" %>
<html>
<head>
    <title>Soportes Clientes</title>
    <script src ="<%= BASEURL %>/js/boton.js"></script>
    <link   href="<%= BASEURL %>/css/estilostsp.css" rel='stylesheet'>
    <link   href="<%= BASEURL %>/css/EstilosReportes.css" rel='stylesheet'>
    <script>
        <% showDatosJs (model.SoporteClienteSvc.getListaSoportes() , out, "datosS"); %>
        <% showDatosJs (model.SoporteClienteSvc.getListaClientes() , out, "datosC"); %>
        <% showDatosJs (model.SoporteClienteSvc.getListaRelacion() , out, "datosR"); %>
        var separador = '~';
        
        function addOption(Comb,valor,texto){
            var Ele = document.createElement("OPTION");
            Ele.value=valor;
            Ele.text=texto;
            Comb.add(Ele);
        }
 
        function loadCombo(datos, cmb){
            cmb.length = 0;
            for (i=0;i<datos.length;i++){
                var dat = (new String(datos[i])).split(separador);
                addOption(cmb, dat[0], dat[1]);
            }
        }   
     
        function move(cmbO, cmbD){
           for (i=cmbO.length-1; i>=0 ;i--)
            if (cmbO[i].selected){
               addOption(cmbD, cmbO[i].value, cmbO[i].text);
            }
           //order(cmbD);
           deleteRepeat(cmbD);
        }        
        function remove(cmb){
           for (i=cmb.length-1; i>=0 ;i--)
            if (cmb[i].selected){
               cmb.remove(i);
            }
        }
        
        function order(cmb){
           for (i=0; i<cmb.length;i++)
             for (j=i+1; j<cmb.length;j++)
                if (cmb[i].text > cmb[j].text){
                    var temp = document.createElement("OPTION");
                    temp.value = cmb[i].value;
                    temp.text  = cmb[i].text;
                    cmb[i].value = cmb[j].value;
                    cmb[i].text  = cmb[j].text;
                    cmb[j].value = temp.value;
                    cmb[j].text  = temp.text;
                }
        }        
        
        function deleteRepeat(cmb){
          for (i=0;i<cmb.length;i++){
             for (j=i+1;j<cmb.length;j++){
               if(cmb[i].value == cmb[j].value)
                  cmb.remove(j);
             }
          }
        }        
        
        function moveAll(cmbO, cmbD){
           for (i=cmbO.length-1; i>=0 ;i--){
             if(cmbO[i].value!='' && cmbO[i].text!=''){
               addOption(cmbD, cmbO[i].value, cmbO[i].text)
//               cmbO.remove(i);
             }
           }
           //order(cmbD);
           deleteRepeat(cmbD);
        } 
        
        function removeAll(cmb){
          cmb.length = 0;
        }        
       
        function loadCombo2(datos, cmb, ocurrencia){
            cmb.length = 0;
            sw = 0;
            var i;
            for (i=0; i < datos.length;i++){
                var dat = (new String(datos[i])).split(separador);
                if (dat[0]==ocurrencia){
                    addOption(cmb, dat[1], getDescripcion(dat[1], datosS));
                    sw = 1;
                }
                else if (sw == 1) break;
            }
        }       
         
        function getDescripcion (id, datos){
            var i;
            for (i=0;i<datos.length;i++){
                var dat = (new String(datos[i])).split(separador);
                if (dat[0]==id){
                    return dat[1];
                }
            }        
            return "";
        }
        
        function validar(){
            if (form1.Soportes.length==0){
               alert ('Debe indicar un Cliente para poder continuar.....');
               return false;
            }
            var i;
            for (i=0;i<form1.Soportes.length;i++)
              form1.Soportes[i].selected = true;
            for (i=0;i<form1.Clientes.length;i++)
              form1.Clientes[i].selected = true;
              
           return true;   
        }        
    </script>
    <link href="../../../css/estilostsp.css" rel="stylesheet" type="text/css">
</head>
<body>
<div id="capaSuperior" style="position:absolute; width:100%; height:100px; z-index:0; left: 0px; top: 0px;">
 <jsp:include page="/toptsp.jsp?encabezado=Asignacion Soportes Clientes"/>
</div>
<div id="capaCentral" style="position:absolute; width:100%; height:87%; z-index:0; left: 0px; top: 100px; overflow: scroll;">


    <center>


    <form action="<%= CONTROLLER %>?estado=Soporte&accion=Cliente&Opcion=Grabar" method="post" name="form1" >
    <table width="780" border='2' align='center' >
      <tr>
        <td align="center" colspan='2' class='fila' >
       	  <table width="100%" align="center" class="tablaInferior">
            <tr>
				<td colspan="12">
		  			<table width="100%"  border="0">
  						<tr>
    						<td width="37%" class="subtitulo1">ASIGNACION SOPORTES CLIENTES</td>
    						<td width="63%" class="barratitulo"><img src="<%= BASEURL %>/images/titulo.gif" width="32" height="20"></td>
  						</tr>
					</table>
               	</td>
          	</tr>
			<tr class="tblTitulo">
            <th width="22%" >Soportes</th>
            <th width="5%" class="bordereporte" bgcolor='#D1DCEB'></th>
            <th width="22%">Soportes a Procesar</th>
            <th width="0%"  class='barratitulo'></th>
            <th width="22%">Clientes a Procesar</th>
            <th width="5%" class="bordereporte" bgcolor='#D1DCEB'></th>
            <th width="22%">Clientes</th>
          </tr>
			  <tr class="tbltitulo">
				<th><select name="SoportesG" size="12" multiple class="select" style="width:100%" ></select></th>
				<th class="bordereporte" bgcolor='#D1DCEB'>
						<image src='<%= BASEURL %>/images/botones/envDer.gif'        style="cursor:hand" onclick="move     (SoportesG, Soportes );" onMouseOver='botonOver(this);' onMouseOut='botonOut(this);'><br>
						<image src='<%= BASEURL %>/images/botones/enviarDerecha.gif' style="cursor:hand" onclick="moveAll  (SoportesG, Soportes );" onMouseOver='botonOver(this);' onMouseOut='botonOut(this);'><br>
						<image src='<%= BASEURL %>/images/botones/envIzq.gif'        style="cursor:hand" onclick="remove   (Soportes );" onMouseOver='botonOver(this);' onMouseOut='botonOut(this);'><br>
						<image src='<%= BASEURL %>/images/botones/enviarIzq.gif'     style="cursor:hand" onclick="removeAll(Soportes );" onMouseOver='botonOver(this);' onMouseOut='botonOut(this);'><br>
				</th>
				<th><select name="Soportes" size="12" multiple class="select" style="width:100%" onChange="return validar(this);"></select></th>
				<td class="bordereporte" bgcolor='#D1DCEB'>&nbsp;</td>
				<th><select name="Clientes"  size="12" multiple class="select" style="width:100%"></select></th>
				<th class="bordereporte" bgcolor='#D1DCEB'>
						<image src='<%= BASEURL %>/images/botones/envIzq.gif'        style="cursor:hand" onclick="move     (ClientesG, Clientes );" onMouseOver='botonOver(this);' onMouseOut='botonOut(this);'><br>
						<image src='<%= BASEURL %>/images/botones/enviarIzq.gif'     style="cursor:hand" onclick="moveAll  (ClientesG, Clientes );" onMouseOver='botonOver(this);' onMouseOut='botonOut(this);'><br>
						<image src='<%= BASEURL %>/images/botones/envDer.gif'        style="cursor:hand" onclick="remove   (Clientes );" onMouseOver='botonOver(this);' onMouseOut='botonOut(this);'><br>
						<image src='<%= BASEURL %>/images/botones/enviarDerecha.gif' style="cursor:hand" onclick="removeAll(Clientes );" onMouseOver='botonOver(this);' onMouseOut='botonOut(this);'><br>		
				</th>
				<th><select name="ClientesG" size="12" multiple class="select" style="width:100%" onchange='loadCombo2(datosR, form1.Soportes, this.value);'>
			    </select></th>
			  </tr>	        
        </table>
          <br>
      </td>
      </tr>
    </table>
    <br>
    <img src='<%=BASEURL%>/images/botones/aceptar.gif' style='cursor:hand' onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" onclick='if (validar()) { form1.submit(); } '>
    &nbsp;
    <img src='<%=BASEURL%>/images/botones/salir.gif'  style='cursor:hand' onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" onclick='window.close();'>
    
    </form>
    <center>
</div>    
</body>
</html>
<script>
    loadCombo (datosS,form1.SoportesG);
    loadCombo (datosC,form1.ClientesG);
	<% String codcli = request.getParameter("codcli");
	   if (codcli!=null) {	%>  
	    var elementos = form1.ClientesG.options;
		for (var i = 0; i < elementos.length ; i++ ){
			if (elementos[i].value == '<%= codcli  %>') {
				elementos[i].selected = true;
				break;
			}
		}		
		move     (form1.ClientesG, form1.Clientes );
		loadCombo2(datosR, form1.Soportes, '<%= codcli %>');
		
	<% } %>   
</script>
