<!--
- Autor : Ivan Dario Gomez Vanegas
- Date  : Enero 27 del 2006
- Copyrigth Notice : Fintravalores S.A. S.A
- Descripcion : Pagina JSP, que permite filtrar los datos para mostrar el reporte de
-               ubicación de equipos en ruta para el Propietario.
--> 
<%@ page session="true" %>
<%@ page errorPage="/ErrorPage.jsp" %>
<%@ page import="java.util.*" %>
<%@ page import="com.tsp.operation.model.beans.*" %>
<%@taglib uri="http://www.sanchezpolo.com/sot" prefix="tsp"%> 
<%@ include file="/WEB-INF/InitModel.jsp" %>
<% try { %>
<%
Usuario loggedUser    = (Usuario) session.getAttribute("Usuario");
String userType       = loggedUser.getTipo();
String agenciaUsuario = loggedUser.getId_agencia();
String Perfil         = (String)session.getAttribute("Perfil");
List listaPlacas      = model.UbicaVehicuPorpSvc.getListaPlacas();
String condicion      = "";
%>
<head>

<title>.: Reporte de ubicación y llegada de equipos en ruta Para Propietarios :.</title>
<link href="<%=BASEURL%>/css/estilostsp.css" rel="stylesheet" type="text/css">
<link href="../css/estilostsp.css" rel="stylesheet" type="text/css">
<script language="javascript" src="<%=BASEURL%>/js/tools.js"></script>
<script src="<%=BASEURL%>/jsp/sot/js/PantallaFiltroUbicacionVehicularPropietario.js" language="javascript"></script>
<script>
var isIE = document.all?true:false;
var isNS = document.layers?true:false;
        function _onmousemove(item) { item.className='select';   }
        function _onmouseout (item) { item.className='unselect'; }
        function addOption(Comb,valor,texto){
        var Ele = document.createElement("OPTION");
        Ele.value=valor;
        Ele.text=texto;
        Comb.add(Ele);
        }
        function move(cmbO, cmbD){
             for (i=cmbO.length-1; i>=0 ;i--)
                  if (cmbO[i].selected){
                       addOption(cmbD, cmbO[i].value, cmbO[i].text)
            
              cmbO.remove(i);
              }
       
        }
        function moveAll(cmbO, cmbD){
             for (i=cmbO.length-1; i>=0 ;i--){
                  addOption(cmbD, cmbO[i].value, cmbO[i].text)
                  cmbO.remove(i);
              }
        }
        function selectAll(cmb){
        for (i=0;i<cmb.length;i++)
        cmb[i].selected = true;
        }
        

function soloLetras(e) {
	var key = (isIE) ? window.event.keyCode : e.which;
	var obj = (isIE) ? event.srcElement : e.target;
	// para permitir las comas (apayares 17-03-2006)
	var isNum = ((key > 0 && key < 44) || (key > 44 && key < 48) 
                    || (key > 57 && key < 65) || (key > 90 &&  key < 97) || (key > 122)) ? true:false;
	window.event.keyCode = (isNum && isIE) ? 0:key;
	e.which = (!isNum && isNS) ? 0:key;
	return (isNum);
}        
    </script>
</head>
<body onresize="redimensionar()" onload = 'redimensionar()'>
    <div id="capaSuperior" style="position:absolute; width:100%; height:100px; z-index:0; left: 0px; top: 0px;">
        <jsp:include page="/WEB-INF/toptsp.jsp?encabezado=Ubicación Vehicular Propietario"/>
    </div>
    <div id="capaCentral" style="position:absolute; width:100%; height:87%; z-index:0; left: 0px; top: 100px; overflow: scroll;"> 
        <br>
        <table border="0" align="center" cellpadding="0" cellspacing="0">
            <tr>
            <td height="330" align="center" valign="top">
	
            <form name="form1" method="post" onmousemove='window.status = "";'>
 
            <table width="700" border="2" align="center">
                <tr>
                    <td>
                    <table width="100%" border="1" cellpadding="0" cellspacing="0"
                    align="center" bordercolor="#F7F5F4" bgcolor="#FFFFFF" onmousemove='window.status = "";'>
                    <tr>
                        <td width="50%" align="left" class="subtitulo1">OPCIONES DEL REPORTE</td>
                        <td width="50%" align="left" class="barratitulo"><img src="<%=BASEURL%>/images/titulo.gif" width="32" height="20"></td>
                    </tr>
                    </table>
                    <table width='100%'>
                    <tr class="fila">
                        <td nowrap width="50%" height="56">        <div align="left">
                        <font size="2" face="Tahoma">
                        </font>          <a href="javascript:fechainical.popup()" hidefocus>
                        </a>
                     <table width="100%" height="100%"  border="0" cellpadding="0">
                        <tr class="fila">
                            <td height="40"><font size="2" face="Tahoma" class="letraresaltada"><b>Fecha Inicial:</b></font></td>
                            <td height="40"><tsp:Date fechaInicial="hoy" formulario="form1" clase="textbox" name="fechaini" otros="size='20'" /></td>
                        </tr>
                         <tr>
                            <td height="40"><font size="2" face="Tahoma" class="letraresaltada"><b>Fecha Final:</b></font></td>
                            <td height="40"><tsp:Date fechaInicial="hoy" formulario="form1" clase="textbox" name="fechafin" otros="size='20'" /></td>
                         </tr>
                      </table>
                        </div>
                        </td>
                        <td width="50%" align='center'><font size="2" face="Tahoma"><b class="letraresaltada">Tipo de Viaje</b><br>
                        </font>
                        <select name="tipoViaje" size="6" multiple="multiple" id="tipoViaje" class="listmenu">
                        <option value="1" selected="selected">Todas </option>
                        <option value="2">Nacional </option>
                        <option value="3">Reexpe. Maicao</option>
                        <option value="4">Directo Maicao</option>
                        <option value="5">Reexpe. C&uacute;cuta </option>
                        <option value="6">Directo C&uacute;cuta</option>
                        <option value="7">Reexpe. Ecuador </option>
                        </select></td>
                        </tr>
         
                   <tr class="fila">
                     <%if(Perfil.equals("PROPIETARIO")){ 
                       condicion = "selectAll(form1.PlacasBuscar);";%>
                      <td>
                       <table border='1' width='320' cellpadding='0' cellspacing='0'>
                      <tr class='subtitulo1'>
                          <th width='46%' class='bordereporte'>Placas</th>
                          <th width='*' class='bordereporte' >&nbsp;</th>
                          <th width='46%' class='bordereporte'>Placas a Buscar</th>
                      </tr>
                      <tr class='fila'>
                          <td class='bordereporte'>
                            <select multiple size='8' class='listmenu' style='width:100%' name='Placas' onchange=" if (this.selectedIndex!=-1) { window.status=this[this.selectedIndex].text;}">
                                        <% Iterator It = listaPlacas.iterator();
                                    while( It.hasNext() ){
                                      Placa placa = (Placa) It.next();
                                     %>
                                     <option value='<%= placa.getPlaca() %>'><%=placa.getPlaca()%></option><%
                                    } %>
                            </select>
                          </td>
                          <td align="center" class='bordereporte'>
                              <tsp:boton value="envIzq" onclick="move(PlacasBuscar, Placas);"/>
                              <tsp:boton value="enviarIzq" onclick="moveAll(PlacasBuscar, Placas);"/>
                              <tsp:boton value="envDer" onclick="move(Placas, PlacasBuscar );"/>
                              <tsp:boton value="enviarDerecha" onclick="moveAll(Placas, PlacasBuscar );"/>
                          </td>
                          <td class='bordereporte'>
                            <select multiple size='8' class='listmenu' style='width:100%' name='PlacasBuscar' onchange=" if (this.selectedIndex!=-1) { window.status=this[this.selectedIndex].text;}">
                            </select>
                          </td>
                      </tr>
                  </table>
                      </td>
                      <%}else{%>
                        <td>
                           <font size="2" face="Tahoma" class="letraresaltada"><b>Placa:</b></font>
                           <input type="text" name="PlacaAdmin" style="text-transform:uppercase" onkeypress="soloLetras(event);">
                        </td>
                      <%}%>
                      <td><font size="2" face="Tahoma" class="letraresaltada"><b>Estado de Viaje: </b></font>&nbsp;&nbsp;&nbsp;&nbsp;
                        <select name="viajes" class="listmenu">
                        <option value="TODOS" selected >Todos </option>
                        <option value="RUTA" >En Ruta</option>
                        <option value="PORCONF">Por Confirmar Salida</option>
                        <option value="CONENTREGA">Con Entrega</option>
                        </select><br><br>
                        <b><font size="2" face="Tahoma"><b>Formato de Salida: </b></font></b>
                            <select name="displayInExcel" class="listmenu">
                            <option value="false" selected="selected">P&aacute;gina WEB </option>
                            <option value="File">Archivo Excel </option>
                            </select>
                        
                      </td>
                   </tr>
                    </table>
  
  
  
                </td>
                </tr>
            </table>
            <center>
            <p>
            <table>
            <tr>
                <td colspan="2" align="center">
                    <% String evento = condicion+"ConsultarBtnClick2('"+CONTROLLER+"?estado=UbicacionVehicular&accion=Propietario')"; %>
                    <tsp:boton type="button" id="ConsultarBtn" name="ConsultarBtn" value="buscar" onclick="<%=evento%>"/>
                </td>
                <td width="10%"></td>
                <td>
                    <tsp:boton type="reset" id="LimpiarFormularioBtn" name="LimpiarFormularioBtn" value="restablecer"/>
                </td>
            </tr>
        </table>
        </p>
        </center>
        <p>&nbsp;</p>
         </form></td>
        </tr>
        </table>
    </div>
	<tsp:InitCalendar/>
</body>



  <!-- COMENTARIO DEL PROCESO DEL ARCHIVO EN EXCEL -->
 <% String comentario= request.getParameter("comentario");
     if(comentario!=null){%>
      <SCRIPT>
          alert(' <%=comentario%>' );
      </SCRIPT>  
  <%}%>
  
<% }catch( Exception a ){ a.printStackTrace(); } %>

