<!--
- Autor : Ing. Andres Martinez
- Date  : Noviembre de 2005
- Copyrigth Notice : Fintravalores S.A. S.A
-->
<%--
-@(#)
--Descripcion : Pagina JSP, que muestra los datos de entrada para
--              la creacion de la boleta recoge como parametro la OC
--%>

<%@page contentType="text/html"%>
<%@page session="true"%>
<%@page errorPage="/error/ErrorPage.jsp"%>
<%@page import="java.util.*"%>
<%@page import="com.tsp.operation.model.beans.*"%>
<%@page import="com.tsp.util.*"%>
<%@page import="com.tsp.operation.model.*"%>
<%@ include file="/WEB-INF/InitModel.jsp"%>


<%String IMP = (request.getParameter("IMP")!= null)?request.getParameter("IMP"):"";
String path = application.getRealPath(model.menuService.getRealUrl(request.getRequestURI()));
String datos[] = model.menuService.getContenidoMenu(BASEURL,request.getRequestURI(),path);%>
<html>
<head>

    <title>Creacion de boletas</title>
    <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
    <link href="../css/estilostsp.css" rel="stylesheet" type="text/css">
    <link href="<%= BASEURL %>/css/estilostsp.css" rel="stylesheet" type="text/css">
    <script src='<%= BASEURL %>/js/validar.js'></script>
	<script type='text/javascript' src="<%= BASEURL %>/js/boton.js"></script>

</head>
<body onResize="redimensionar()" onLoad="redimensionar();">
<div id="capaSuperior" style="position:absolute; width:100%; height:100px; z-index:0; left: 0px; top: 0px;">
<jsp:include page="/WEB-INF/toptsp.jsp?encabezado=Generacion de Boletas WEB"/>
</div>

             <div id="capaCentral" style="position:absolute; width:100%; height:85%; z-index:0; left: 0px; top: 100px; overflow: scroll;">
    <form name="form1" method="post" action="<%= CONTROLLER %>?estado=Descuento&accion=TercmWeb&Opcion=buscar">
        <table width="60%" border="2" align="center">
			<tr>
                            <td>
                            <table width="99%" height="34" border="0" align="center" class="tablaInferior">
                            <tr>
                               <td width="190" height="24"  class="subtitulo1"><p align="left">Parametros Iniciales</p></td>
                                <td width="404"  class="barratitulo"><img src="<%=BASEURL%>/images/titulo.gif" width="32" height="20" align="left"><%=datos[0]%></td>
			    </tr>
                            <tr class="fila">
                                    <td class="letra_resaltada" align="left">&nbsp;&nbsp;OC</td>
                                    <td align="left">&nbsp;&nbsp;<input type="text"  name="OC" class="textbox" maxlength="6"></td>
                             </tr>
                              </table>
			</td>
		      </tr>
		</table>
        <table align="center">
          <tr align="center">
            <td colspan="2"> <img src="<%=BASEURL%>/images/botones/aceptar.gif"  name="imgaceptar" onClick="if(Validar(form1)){form1.submit();}" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" style="cursor:hand">  <img src="<%=BASEURL%>/images/botones/salir.gif"  name="imgsalir" onClick="parent.close();" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" style="cursor:hand"> </td>
          </tr>
        </table>

    </form>
    <form name="form2" method="post" action="<%= CONTROLLER %>?estado=Descuento&accion=TercmWeb&Opcion=BuscarParaImprimir">
       <input type="hidden" name="OCIMPRIMIR">
    </form>
	</div>
 <%if(IMP.equals("Existe")){%>
     <div id="capaCentral" style="position:absolute; width:100%; height:85%; z-index:0; left: 0px; top: 200px; overflow: scroll;">
         <table border="2" align="center">
                  <tr>
                    <td><table width="100%"  border="1" align="center"  bordercolor="#F7F5F4" bgcolor="#FFFFFF">
                       <tr>
                        <td width="229" align="center" class="mensajes">La planilla ya existe!!
                       </td>
                       <td width="29" background="<%=BASEURL%>/images/cuadronaranja.JPG">&nbsp;</td>
                        <td width="58">&nbsp;</td>
                      </tr>
                    </table>
                    </td>
                  </tr>
                </table>

      </div>

  <%}%>
  <%if(IMP.equals("ExisteImpresa")){%>
     <div id="capaCentral" style="position:absolute; width:100%; height:85%; z-index:0; left: 0px; top: 200px; overflow: scroll;">
         <table border="2" align="center">
                  <tr>
                    <td><table width="100%"  border="1" align="center"  bordercolor="#F7F5F4" bgcolor="#FFFFFF">
                       <tr>
                        <td width="229" align="center" class="mensajes">La planilla ya se imprimio o no existe..
                       </td>
                       <td width="29" background="<%=BASEURL%>/images/cuadronaranja.JPG">&nbsp;</td>
                        <td width="58">&nbsp;</td>
                      </tr>
                    </table>
                    </td>
                  </tr>
                </table>

      </div>

  <%}%>
<%=datos[1]%>
</body>
<script>
function Validar(form){
	if(form.OC.value == '' ){
		alert('Debe ingresar la OC');
		return false;
	}
	return true;
}
function cambiar(form1, form2){
  form2.OCIMPRIMIR.value = form1.OC.value;
  form2.submit();
}
</script>
</html>
