<!--
- Autor : Alejandro Payares
- Date  : Diciembre 24 del 2005
- Copyrigth Notice : Fintravalores S.A. S.A
- Descripcion : Pagina JSP que permite administrar los datos de los usuarios.
-->
<%@ page session="true" %>
<%@ page errorPage="/ErrorPage.jsp" %>
<%@ page import="java.util.*" %>
<%@ page import="com.tsp.operation.model.beans.*" %>
<%@ include file="/WEB-INF/InitModel.jsp" %>
<head>
    <title>.: Administraci�n de usuarios :.</title>
    <link href='<%=BASEURL%>/css/estilostsp.css' rel='stylesheet' type='text/css'>
    <link href='../css/estilostsp.css' rel='stylesheet' type='text/css'>
    <script language="javascript" src="<%=BASEURL%>/js/reporte.js"></script>
    <script language="javascript" src="<%=BASEURL%>/js/boton.js"></script>
    <style type="text/css">
        <!--
        .icon {
        border-top: 1px solid #d6d3ce;
        border-left: 1px solid #d6d3ce;
        border-right: 1px solid #666666;
        border-bottom: 1px solid #666666;
        padding: 2px;
        width: 222px;
        height: 34px;
        }

        .iconOver {
        border-top: 1px solid #d6d3ce;
        border-left: 1px solid #d6d3ce;
        border-right: 1px solid #003399;
        border-bottom: 1px solid #003399;
        padding: 2px;
        width: 222px;
        height: 34px;
        background-color:#D1DCEB;
        color: #003399;
        }

        .iconClick {
        border-top: 1px solid #003399;
        border-left: 1px solid #003399;
        border-right: 1px solid #d6d3ce;
        border-bottom: 1px solid #d6d3ce;
        padding: 2px;
        width: 222px;
        height: 34px;
        background-color:#D1DCEB;
        color: #003399;
        }
        -->
    </style>
</head>
<body onresize="redimensionar()" onload='redimensionar();document.formBusqueda.focus()'>
    <div id="capaSuperior" style="position:absolute; width:100%; z-index:0; left: 0px; top: 0px;">
        <jsp:include page="/WEB-INF/toptsp.jsp?encabezado=ADMINISTRACI�N DE USUARIOS"/>
    </div>
    <div id="capaCentral" style="position:absolute; width:100%; height:87%; z-index:0; left: 0px; top: 100px; overflow: scroll;"> 
        <%  String linkCreateUsersPage = CONTROLLER + "?estado=GestionUsuarios&accion=CreateUser";
            String urlBusqueda = CONTROLLER + "?estado=GestionUsuarios&accion=ManageUsers&cmd=search"; %>
        <table border='0'>
            <tr>
                <td width='40'>&nbsp;</td>
                <td>
                <div style='cursor:hand' class='icon' onmouseover="this.className='iconOver'"
                onmouseout="this.className='icon'"
                onmousedown="this.className='iconClick'"
                onmouseup="this.className='iconOver'" onClick="location.href='<%= linkCreateUsersPage %>'">
                <table width="220">
                <tr>
                    <td>
                    <table width="100%"  border="0">
                    <tr class="letraresaltada">
                    <td><img src="<%=BASEURL%>/images/botones/iconos/users.gif"></td>
                    <td><b>Crear Nuevos Usuarios</b> </td>
                </tr>
                    </table>
                </td>
            </tr>
                    </table>
                </div>
            </td>
            <td width="50"></td>
            <td>
                <form name='formBusqueda' method='post' action='<%= urlBusqueda %>'>
                <div class='icon' >
                    <table width="100%"  border="0">
                        <tr>
                        <td class="letraresaltada"><b>Busqueda por ID de usuario</b></td>
                        <td rowspan="2">
                            <input type=image style='cursor:hand' src="<%=BASEURL%>/images/botones/iconos/lupa.gif" width="31" height="33" />
                        </td>
                        </tr>
                        <tr>
                            <td><input type="text" name="campoBusqueda"></td>
                        </tr>
                    </table>
                </div>
                </form>
            </td>
            <td width='50'>&nbsp;</td>
            <td>
                <div style='cursor:hand' class='icon' onmouseover="this.className='iconOver'"
                    onmouseout="this.className='icon'"
                    onmousedown="this.className='iconClick'"
                    onmouseup="this.className='iconOver'" onClick="location.href='<%= urlBusqueda + "&campoBusqueda=" %>'">
                    <table width="220">
                        <tr>
                            <td>
                            <table width="100%"  border="0">
                            <tr class="letraresaltada">
                            <td><img src="<%=BASEURL%>/images/botones/iconos/users.gif"></td>
                            <td><b>Ver todos los Usuarios</b> </td>
                        </tr>
                            </table>
                        </td>
                        </tr>
                    </table>
                </div>
            </td>
            </tr>
        </table>
        
            <%    List userLst = model.usuarioService.getUsuariosCreados();  
                  if( userLst != null )  { %>
        <table  border = '2' align = 'center' >
            <tr>
            <td>
                <table cellSpacing=1 cellPadding=1 align=center border=0>
                <tr class="filaresaltada">
                    <th class='bordereporte'>Editar datos</th>
                    <th class='bordereporte'>ID de Usuario</th>
                    <th class='bordereporte'>Nombre</th>
                    <th class='bordereporte'>Agencia</th>
                    <th class='bordereporte'>NIT</th>
                    <th class='bordereporte'>Direcci�n</th>
                    <th class='bordereporte'>Tel�fono</th>
                    <th class='bordereporte'>E-Mail</th>
                    <th class='bordereporte'>Pa�s</th>
                    <th class='bordereporte'>Ciudad</th>
                    <th class='bordereporte'>Tipo de Usuario</th>
                    <th class='bordereporte'>Cliente / Destinatario Asociado</th>
                    <th class='bordereporte'>�Puede Modificar Planes de Viaje?</th>
                </tr>
<%
    Usuario user = null;
    Iterator userLstIt = userLst.iterator();
	boolean gris = true;
    while( userLstIt.hasNext() )
    {
      user = (Usuario) userLstIt.next();
      String nomAgencia = user.getNombre_agencia() == null?"-":user.getNombre_agencia();
      String nit        = user.getCedula();
      String direccion  = user.getDireccion();
      String telefono   = user.getTelefono();
      String email      = user.getEmail();
      String nomPais    = user.getNombrePais();
      String ciudad     = user.getCiudad();
      String descTipo   = user.getDescTipo();
      String accesoPlVj = user.getAccesoplanviaje();
      String QUERYSTRING = "?estado=GestionUsuarios&accion=ManageUsers" +
                           "&cmd=edit&idUser=" + user.getLogin(); %>
                <tr class='<%= (gris = !gris)?"filagris":"filaazul" %>' onMouseOver='cambiarColorMouse(this)' style="cursor:hand">
                    <td align="center" class='bordereporte'>
                        <A href='<%= CONTROLLER + QUERYSTRING %>' class="bordereporte"><img src="<%=BASEURL%>/images/botones/iconos/modificar.gif"></A>
                    </td>
                    <td nowrap class='bordereporte'><%= user.getLogin() %></td>
                    <td nowrap class='bordereporte'><%= user.getNombre() %></td>
                    <td nowrap class='bordereporte'>
          <%= "".equals(nomAgencia) ? "&nbsp;" : nomAgencia %>
                    </td>
                    <td nowrap class='bordereporte'>
          <%= "".equals(nit) ? "&nbsp;" : nit %>
                    </td>
                    <td nowrap class='bordereporte'>
          <%= "".equals(direccion) ? "&nbsp;" : direccion %>
                    </td>
                    <td nowrap class='bordereporte'>
          <%= "".equals(telefono) ? "&nbsp;" : telefono %>
                    </td>
                    <td nowrap class='bordereporte'>
          <%= "".equals(email) ? "&nbsp;" : email %>
                    </td>
                    <td nowrap class='bordereporte'>
          <%= "".equals(nomPais) ? "&nbsp;" : nomPais %>
                    </td>
                    <td nowrap class='bordereporte'>
          <%= "".equals(ciudad) ? "&nbsp;" : ciudad %>
                    </td>
                    <td nowrap class='bordereporte'>
          <%= "".equals(descTipo) ? "&nbsp;" : descTipo %>
                    </td>
                    <td nowrap class='bordereporte'><%= user.getNombreClienteDestinat() %></td>
                    <td nowrap class='bordereporte'>
          <%= "".equals(accesoPlVj) || "readonly".equals(accesoPlVj) ? "No" : "S�" %>
                    </td>
                </tr>
    <% } %>
            </td>
                </tr>
            </table>
            </td>
            </tr>
        </table>
    <% }else if ( request.getParameter("primeraVez") == null ) { %>
                    
        <table border="2" align="center">
            <tr>
                <td><table width="100%"  border="1" align="center"  bordercolor="#F7F5F4" bgcolor="#FFFFFF">
                    <tr>
                        <td width="229" align="center" class="mensajes">No se encontraron usuarios</td>
                        <td width="29" background="<%=BASEURL%>/images/cuadronaranja.JPG">&nbsp;</td>
                        <td width="58">&nbsp;</td>
                    </tr>
                </table></td>
            </tr>
        </table>
                    
               
     <% } %>
            
        <br>
    </div>
</body>
