<!--
- Autor : Alejandro Payares
- Date  : Mayo 10 del 2006
- Copyrigth Notice : Fintravalores S.A. S.A
- Descripcion : Pagina JSP que permite agregar un nuevo registro dentro de una subtabla de tablagen.
-->
<%@include file="/WEB-INF/InitModel.jsp" %>
<%@ taglib uri="http://www.sanchezpolo.com/sot" prefix="tsp"%>
<html>
    <head>
        <title>Agregar grupo</title>
        <script language="javascript" src="<%=BASEURL%>/jsp/sot/js/colorpickerFlooble.js"></script>
        <script language="javascript" src="<%=BASEURL%>/jsp/sot/js/PantallaAdminGruposReporte.js"></script>
        <link href="<%=BASEURL%>/css/estilostsp.css" rel="stylesheet" type="text/css">
        <script>
            function initURL(){
                urlPaginaPaleta = '<%= BASEURL + "/jsp/sot/PantallaPaletaColores.jsp" %>';
            }
        </script>
    </head>
    <body onload='document.formGrupo.nombreGrupo.focus();initURL()'>
        <br>
        <form name="formGrupo">
            <table border='0'>
                <tr>
                    <td>
                        <table align='left' border = '1' width='500px'>
                            <tr>
                                <td width='500px' align='left'>
                                    <table width='100%'>
                                        <tr align='center'>
                                            <td colspan="3" align="center" class="subtitulo1">DATOS DEL GRUPO <br></td>
                                            <td colspan="2" align="left" class="barratitulo"><img src="<%=BASEURL%>/images/titulo.gif" width="32" height="20" align="left"></td>
                                        </tr>
                                        <tr class="fila">
                                            <td colspan="2" >
                                                Nombre <input name="nombreGrupo" onkeypress='soloLetrasGrupos(event);'/>
                                            </td>
                                            <td width='95' align="right">Color</td>
                                            <td colspan="2">
                                                <input name="colorGrupo2field" id="colorGrupo2field" value="" readOnly/>
                                                <a 
                                                    id="colorGrupo2" 
                                                    style="BORDER-RIGHT: #000000 1px solid; BORDER-TOP: #000000 1px solid; FONT-SIZE: 10px; BORDER-LEFT: #000000 1px solid; BORDER-BOTTOM: #000000 1px solid; FONT-FAMILY: Verdana; TEXT-DECORATION: none" 
                                                    href="javascript:pickColor('colorGrupo2');">
                                                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                                </a>

                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td>
                        <p align='left'>
                        <center>
                            <table border='0'>
                                <tr>
                                <td>
                                    <tsp:boton value="aceptar" onclick="enviarNuevoGrupo(document.formGrupo)"/>
                                </td>
                                <td width='30'>&nbsp;</td>
                                <td>
                                    <tsp:boton value="cancelar" onclick="window.close()"/>
                                </td>
                                </tr>
                            </table>
                        </center>
                        </p>
                    </td>
                </tr>
            </table>
            
        </form>
        <br>
        
    </body>
</html>
