<!--
- Autor : Alejandro Payares
- Date  : Diciembre 24 del 2005
- Copyrigth Notice : Fintravalores S.A. S.A
- Descripcion : Pagina JSP, que permite filtrar los datos para mostrar el reporte de ubicación de equipos en ruta.
--> 
<%@ page session="true" %>
<%@ page errorPage="/ErrorPage.jsp" %>
<%@ page import="java.util.*" %>
<%@ page import="com.tsp.operation.model.beans.*" %>
<%@taglib uri="http://www.sanchezpolo.com/sot" prefix="tsp"%> 
<%@ include file="/WEB-INF/InitModel.jsp" %>
<% try { %>
<%
Usuario loggedUser    = (Usuario) session.getAttribute("Usuario");
String userType       = loggedUser.getTipo();
String agenciaUsuario = loggedUser.getId_agencia();
%>
<head>
<title>.: Reporte de ubicación y llegada de equipos en ruta :.</title>
<link href="<%=BASEURL%>/css/estilostsp.css" rel="stylesheet" type="text/css">
<link href="../../../css/estilostsp.css" rel="stylesheet" type="text/css">
<script language="javascript" src="<%=BASEURL%>/js/tools.js"></script>

<script src="<%=BASEURL%>/jsp/sot/js/PantallaFiltroUbicacionEquipoRuta.js" language="javascript"></script>
</head>
<body onresize="redimensionar()" onload = 'redimensionar()'>
    <div id="capaSuperior" style="position:absolute; width:100%; height:100px; z-index:0; left: 0px; top: 0px;">
        <jsp:include page="/WEB-INF/toptsp.jsp?encabezado=Reporte de ubicación y llegada de equipos en ruta"/>
    </div>
    <div id="capaCentral" style="position:absolute; width:100%; height:87%; z-index:0; left: 0px; top: 100px; overflow: scroll;"> 
        <br>
        <table border="0" align="center" cellpadding="0" cellspacing="0">
            <tr>
            <td height="330" align="center" valign="top">
	
            <form name="form1" method="post" onmousemove='window.status = "";'>
 
            <table width="700" border="2" align="center">
                <tr>
                    <td>
                    <table width="100%" border="1" cellpadding="0" cellspacing="0"
                    align="center" bordercolor="#F7F5F4" bgcolor="#FFFFFF" onmousemove='window.status = "";'>
                    <tr>
                        <td width="50%" align="left" class="subtitulo1">OPCIONES DEL REPORTE</td>
                        <td width="50%" align="left" class="barratitulo"><img src="<%=BASEURL%>/images/titulo.gif" width="32" height="20"></td>
                    </tr>
                    <tr class="fila">
                        <td nowrap width="50%" height="56">        <div align="left">
                        <font size="2" face="Tahoma">
                        </font>          <a href="javascript:fechainical.popup()" hidefocus>
                        </a>
                        <table width="97%" height="100%"  border="0" cellpadding="0">
                          <tr class="fila">
                            <td height="40"><font size="2" face="Tahoma" class="letraresaltada"><b>Fecha Inicial:</b></font></td>
                            <td height="40"><tsp:Date fechaInicial="hoy" formulario="form1" clase="textbox" name="fechaini" otros="size='20'" /></td>
                          </tr>
                          <tr>
                            <td height="40"><font size="2" face="Tahoma" class="letraresaltada"><b>Fecha Final:</b></font></td>
                            <td height="40"><tsp:Date fechaInicial="hoy" formulario="form1" clase="textbox" name="fechafin" otros="size='20'" /></td>
                          </tr>
                        </table>
                        </div>
                        </td>
                        <td width="50%" align='center'><font size="2" face="Tahoma"><b class="letraresaltada">Tipo de Viaje</b><br>
                        </font>
                        <select name="tipoViaje" size="6" multiple="multiple" id="tipoViaje" class="listmenu">
                        <option value="ALL" selected="selected">Todas </option>
                        <option value="NA">Nacional </option>
                        <option value="RM">Reexpe. Maicao</option>
                        <option value="DM">Directo Maicao</option>
                        <option value="RC">Reexpe. C&uacute;cuta </option>
                        <option value="DC">Directo C&uacute;cuta</option>
                        <option value="RE">Reexpe. Ecuador </option>
                        </select></td>
                        </tr>
                        <tr class="fila">
                        <td ><font size="2" face="Tahoma" class="letraresaltada"><b><b>Estado de Viaje: </b></b></font>
                        <select name="viajes" class="listmenu">
                        <option value="TODOS" selected >Todos </option>
                        <option value="RUTA" >En Ruta</option>
                        <option value="PORCONF">Por Confirmar Salida</option>
                        <option value="CONENTREGA">Con Entrega</option>
                        </select>
                        </td>

                        <td >
                            <font size="2" face="Tahoma"> <b>Grupo Equipo: </b> </font>
                            <select name="grupoequipo" class="listmenu">
                                <option value=''><%
                                    List gruposEquiposList = model.grupoEquipoService.getGruposEquipos();
                                    Iterator gruposEquiposIt = gruposEquiposList.iterator();
                                    while( gruposEquiposIt.hasNext() )
                                    {
                                      GrupoEquipo grupoEq = (GrupoEquipo) gruposEquiposIt.next();
                                      String grupoTrabajo     = grupoEq.getGrupoTrabajo();
                                      String descGrupoTrabajo = grupoEq.getDescGrupoTrabajo().toUpperCase(); %>
                                                        <option value='<%= grupoTrabajo.replaceAll("&","-_-") %>'><%= descGrupoTrabajo %><%
                                    } %>
                            </select>
                        </td>
                        </tr>
                        <tr class="fila">
                        <td>
                            <font size="2" face="Tahoma"> <b>Origen: </b> </font>
                            <input type="checkbox" name="asociadasorigen" id="asociadasorigen">Ciudades Asociadas
                            <select name='origen' class="listmenu">
                            <option value='TODOS' selected>Todos<%
                          Ciudad ciudad = null;
                          String codCiu = null;
                          String nomCiu = null;
                          List ciudadList = model.ciudadService.getCiudadList();
                          Iterator ciudadIt = ciudadList.iterator();
                          while( ciudadIt.hasNext() )
                          {
                            ciudad = (Ciudad)ciudadIt.next();
                            codCiu = ciudad.getCodCiu();
                            nomCiu = ciudad.getNomCiu(); %>
                                            <option value='<%= codCiu %>'><%= nomCiu %><%
                          } %>
                            </select><br>
                        </td>
                        <td>
                            <font size="2" face="Tahoma"> <b>Destino: </b> </font>
                            <input type="checkbox" name="asociadasdestino" id="asociadasdestino">Ciudades Asociadas
                            <select name='destino' class="listmenu">
                            <option value='TODOS'>Todos<%
                              ciudadIt = ciudadList.iterator();
                              while( ciudadIt.hasNext() )
                              {
                                ciudad = (Ciudad)ciudadIt.next(); 
                                codCiu = ciudad.getCodCiu();
                                nomCiu = ciudad.getNomCiu(); %>
                                                <option <%= codCiu.equals(agenciaUsuario) ? "selected" : "" %>
                                                value='<%= codCiu %>'><%= nomCiu %><%
                              } %>
                            </select>
                        </td>
                        </tr>
                        <tr class="subtitulo1">
                          <td><div align="center">BUSQUEDA DE VALORES ESPECIFICOS </div></td>
                          <td><div align="center">TIPOS DE REPORTES </div></td>
                        </tr>

                        <tr class="fila">
                        <td>
                            <table width="100%" bordercolor="#F7F5F4">
                                <tr class="fila">
                                <td width="39%">
                                    <input type=radio name='tipoBusquedaRBtn' id='placas' value='placas'
                                    checked>Placa(s): <br>
                                    <input type=radio name='tipoBusquedaRBtn' id='trailers' value='trailers'
                                    >Trailer(s): 
                                </td>
                                <td width="61%">
                                    <input name="placasTrailers" type="text" class="textbox" value="" size="40"
                                    alt="Digite la(s) placa(s) / trailer(s) para buscarle los despachos">
                                </td>
                                </tr>
								<tr height="1">
								<td colspan="2"><hr></td>
								</tr>
                                <tr class="fila">
								  <td>
                                  OC(s)/Planilla(s):</td>
                                  <td><input name="planillas" type="text" class="textbox" value="" size="40"
                            alt="Digite el (las) OC(s) / Planilla(s)"></td>
                                </tr>
                            </table>
                        </td>

                        <td>
                          <input type="radio" name="tipoReporte" id="tipoReporte4" value="normal" checked/>
                                Ninguno de los siguientes (normal)  
                                <br> 
                          <input type="radio" name="tipoReporte" id="tipoReporte1" value="repDetallado"/>
						  Mostrar solo reporte detallado de trafico
						  <br> 
                          <input type="radio" name="tipoReporte" id="tipoReporte2" value="ultimoRep"/>
                          Mostrar solo ultimos reportes 
						  <br> 
                          <input type="radio" name="tipoReporte" id="tipoReporte3" value="soloVarados"/>
                          Mostrar solo los varados y/o demorados
						  
                          
						 </td>
                        </tr>

                        <tr class="fila">
                        <td ><b><font size="2" face="Tahoma"><b>Formato de Salida: </b></font></b>
                            <select name="displayInExcel" class="listmenu">
                            <option value="false" selected="selected">P&aacute;gina WEB </option>
                            <option value="File">Archivo Excel </option>
                            </select><br>
                        <font size="2" face="Tahoma" class="letraresaltada"><b><b>Con FIT/DEF: </b></b></font>
                        <input type="checkbox" name='FITDEF'>
                        </td>
                        <td >&nbsp;
                          </td>
                        </tr>
                    </table>
  
  
  
                </td>
                </tr>
            </table>
            <center>
            <p>
            <table>
            <tr>
                <td colspan="2" align="center">
                    <% String evento = "ConsultarBtnClick('"+CONTROLLER+"?estado=UbicacionEquipoRuta&accion=Search')"; %>
                    <tsp:boton type="button" id="ConsultarBtn" name="ConsultarBtn" value="buscar" onclick="<%=evento%>"/>
                </td>
                <td width="10%"></td>
                <td>
                    <tsp:boton type="reset" id="LimpiarFormularioBtn" name="LimpiarFormularioBtn" value="restablecer"/>
                </td>
            </tr>
        </table>
        </p>
        </center>
        <p>&nbsp;</p>
        <input type="hidden" name="listaTipoViaje" value="">
        <input type="hidden" name="nombreCliente" value="XX">
        </form></td>
        </tr>
        </table>
    </div>
	<tsp:InitCalendar/>
</body>



  <!-- COMENTARIO DEL PROCESO DEL ARCHIVO EN EXCEL -->
 <% String comentario= request.getParameter("comentario");
     if(comentario!=null){%>
      <SCRIPT>
          alert(' <%=comentario%>' );
      </SCRIPT>  
  <%}%>
  
<% }catch( Exception a ){ a.printStackTrace(); } %>
