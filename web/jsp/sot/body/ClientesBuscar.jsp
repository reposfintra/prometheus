<%@page session="true"%>
<%@page import="java.util.*" %>
<%@page import="com.tsp.operation.model.*"%>
<%@include file="/WEB-INF/InitModel.jsp"%>
<%@page import="com.tsp.operation.model.beans.*"%>
<%@page import="com.tsp.util.*"%>
<%@ taglib uri="http://jakarta.apache.org/taglibs/input-0.90" prefix="input" %>
<%@page contentType="text/html"%>
<%@page import="com.tsp.operation.model.beans.*, java.util.*"%>


<html>
<head>  
<title>Buscar Cliente</title> 
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link href="<%=BASEURL%>/css/estilostsp.css" rel="stylesheet" type="text/css">  
<link href="file:///C|/Tomcat5/webapps/css/estilostsp.css" rel="stylesheet" type="text/css"> 
<script language="JavaScript" type="text/javascript" src="<%=BASEURL%>/js/validar.js"></script>
<script type='text/javascript' src="<%= BASEURL %>/js/boton.js"></script>
<script type='text/javascript' src="<%= BASEURL %>/js/ingreso.js"></script>


</head>
<body onResize="redimensionar();" onLoad='redimensionar();' > 

<%
  String path = application.getRealPath(model.menuService.getRealUrl(request.getRequestURI()));
  String datos[] = model.menuService.getContenidoMenu(BASEURL,request.getRequestURI(),path);
%>
<%
String res = (request.getParameter("msg")!= null)? request.getParameter("msg") : "";%>
<div id="capaSuperior" style="position:absolute; width:100%; height:100px; z-index:0; left: 0px; top: 0px;">
 <jsp:include page="/WEB-INF/toptsp.jsp?encabezado=Buscar Cliente"/>
</div>
<div id="capaCentral" style="position:absolute; width:100%; height:87%; z-index:0; left: 0px; top: 100px; overflow: scroll;">
<FORM action="<%=CONTROLLER%>?estado=Cliente&accion=Buscar&opcion=1" METHOD='post' id='form1' name='form1'  onSubmit="return validar(this);">
  <table width="450" border="2" align="center">
    <tr>
      <td><table width="99%"   align="center"> 
		  <tr>
			<td width="50%" height="22"  class="subtitulo1"><p align="left">Buscar Cliente</p></td>
			<td width="414"  class="barratitulo"><img src="<%=BASEURL%>/images/titulo.gif" width="32" height="20" align="left"><%=datos[0]%></td>
		  </tr>
		</table>     <table width="100%" class="tablaInferior">
        
        <tr class="fila">
          <td width="119">            Codigo : </td>
          <td colspan="2"><input  class='textbox' type='text' name="Codigo" id="Codigo" size='15' maxlength='11'  >
</td>
        </tr>
		<tr class="fila">
          <td width="119">            Nit : </td>
          <td colspan="2"><input  class='textbox' type='text' name="Nit" size='15' id="Nit" maxlength='11' onKeyPress="soloDigitos(event,'decNO');"   >
</td>
        </tr>
        <tr class="fila">
          <td>            Nombre:            </td>
          <td width="132"><input name="Nombre" type="text" class="textbox" id="Nombre"  size="20" maxlength="15" ></td>
          <td width="171">&nbsp;</td>
          </tr>
        <tr class="fila">
          <td>            Agencia : </td>
          <td><% TreeMap Agencia = model.agenciaService.listar(); 
				 Agencia.put("", "");%>
			  <input:select name="agencia" attributesText="style='width:95%;' class='listmenu'" options="<%=Agencia%>" /></td>
          <td>&nbsp;            </td>
          </tr>
		  <tr class="fila">
          <td>            pais : </td>
          <td><% model.paisservice.listarPaises();
            	TreeMap paises =   model.paisservice.getListpais();
			    paises.put( " ", "");%>
			  <input:select name="Pais" attributesText="style='width:95%;' class='listmenu'" options="<%=paises%>" /></td>
          <td>&nbsp;            </td>
          </tr>
      </table></td>
    </tr>
  </table>
 <br>
  <div align="center">
    <div align="center"><img src="<%=BASEURL%>/images/botones/buscar.gif"  name="imgbuscar"  onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" style="cursor:hand" onClick="form1.submit();">&nbsp;
	<img src="<%=BASEURL%>/images/botones/salir.gif"  name="imgsalir"  onMouseOver="botonOver(this);" onClick="parent.close();" onMouseOut="botonOut(this);" style="cursor:hand"></div>
    <br>
</div>
  <%if(!res.equals("")){%>
 <br>
  <table border="2" align="center">
  <tr>
    <td><table width="100%"  border="1" align="center"  bordercolor="#F7F5F4" bgcolor="#FFFFFF">
      <tr>
        <td width="300" align="center" class="mensajes">La busqueda no arrojo resultados</td>
        <td width="29" background="<%=BASEURL%>/images/cuadronaranja.JPG">&nbsp;</td>
        <td width="58">&nbsp;</td>
      </tr>
    </table></td>
  </tr>
</table>

 <%}%>

</form>
</div> 
<%=datos[1]%>
</body>
</html>
<script>
			function validar( form ){
			
				if( form.Codigo == "" && form.Nit=="" && form.Nombre =="" && form.aegencia ==""){
					alert( 'Defina un parametro de busqueda para poder continuar...' );
					return false;
				}
				
				
				
			}				
			
			function abrirVentanaBusq( an, al, url, pag ) {
			
        		parent.open( url, 'Conductor', 'width=' + an + ', height=' + al + ',scrollbars=no,resizable=no,top=10,left=65,status=yes' );
				
    		}
	
		</script>