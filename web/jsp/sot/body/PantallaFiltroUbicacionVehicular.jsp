<!--
- Autor : Ivan Dario Gomez
- Date  : Diciembre 24 del 2005
- Copyrigth Notice : Fintravalores S.A. S.A
- Descripcion : Pagina JSP que permite filtrar los datos para mostrar el reporte de Ubicacion Vehicular.
--> 
<%@ page session="true" %>
<%@ page errorPage="/ErrorPage.jsp" %>
<%@ page import="java.util.*" %>
<%@ page import="com.tsp.operation.model.beans.*" %>
<%@ taglib uri="http://jakarta.apache.org/taglibs/input-0.90" prefix="input" %>
<%@ taglib uri="http://www.sanchezpolo.com/sot" prefix="tsp"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %> 
<%@ include file="/WEB-INF/InitModel.jsp" %>
<%@ page isELIgnored ="false"%> 
<%
try {

    Usuario loggedUser = (Usuario) session.getAttribute("Usuario");
%>
<head>
<title>.: Reporte Ubicacion Vehicular Clientes :.</title>
<script src='<%=BASEURL%>/js/tools.js' language='javascript'></script>
<link href="<%=BASEURL%>/css/estilostsp.css" rel="stylesheet" type="text/css">
<script src="<%=BASEURL%>/js/PantallaFiltroUbicacionVehicular.js" language="javascript"></script>
</head>

<body onresize="redimensionar()" onload = "redimensionar();document.form1.campo.focus()">
<div id="capaSuperior" style="position:absolute; width:100%; height:100px; z-index:0; left: 0px; top: 0px;">
	<jsp:include page="/WEB-INF/toptsp.jsp?encabezado=Reporte Ubicacion Vehicular Clientes"/>
</div>
<div id="capaCentral" style="position:absolute; width:100%; z-index:0; left: 0px; top: 100px; overflow: scroll;"> 
    <form name="form1" method="post" onmousemove='window.status = "";'>
    <table border="0" align="center" cellpadding="0" cellspacing="0">
        <tr>
        <td height="330" align="center" valign="top">
  
        
        <table width="608" border="2" align="center">
            <tr>
            <td>
            <table width="100%">
                <tr>
                    <td width="50%" align="left" class="subtitulo1">CLIENTE</td>
					<td width="50%" align="left" class="barratitulo"><img src="<%=BASEURL%>/images/titulo.gif" width="32" height="20"></td>
                </tr>
            </table>
            <table width="100%"  border="0">
                <tr class="fila">
                    <td width="50%" >Campo de busqueda:<br>
                    <input name="text" type="text" class="textbox" id="campo" style="width:265;" onKeyUp="buscar(document.form1.cliente,this)" ></td>
                    <td width="50%" rowspan="2" >
                    <table width="100%" class="tablaInferior">
                        <tr height="50%" class="fila">
                            <td width="38%">FECHA INICIAL:</td>
                            <td width="62%" align="left">
                                <tsp:Date fechaInicial="hoy" formulario="form1" clase="textbox" name="fechaini" otros="size='20'" />
                            </td>
                        </tr>
                        <tr height="50%" class="fila">
                            <td>FECHA FINAL: </td>
                            <td align="left">
                                <tsp:Date fechaInicial="hoy" clase="textbox" formulario="form1" name="fechafin" otros="size='20'" />
                            </td>
                        </tr>
                    </table></td>
                </tr>
                <tr class="fila">
                <td height="33" rowspan="2">
             
                <input:select default='<%= request.getParameter("clienteSelected") %>' name="cliente" options="<%=model.clienteService.listar(loggedUser)%>"/> <br>
               </td>
                </tr>
            </table>
            <table width="100%" class="tablaInferior">
                <tr class="fila">
                    <td class="subtitulo1" width="50%" ><div align="center">TIPO DE VIAJE </div></td>
                    <td class="barratitulo"><img src="<%=BASEURL%>/images/titulo.gif" width="32" height="20">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
                </tr>
                <tr class="fila">
                <td ><div align="center">
                    <select name="tipoViaje" size="7" multiple="multiple" id="tipoViaje" style="FONT-FAMILY: Tahoma; FONT-SIZE: 11px">
                        <option value="ALL" selected="selected">Todas </option>
                        <option value="NA">Nacional </option>
                        <option value="RM">Reexpe. Maracaibo</option>
                        <option value="DM">Directo Maracaibo</option>
                        <option value="RC">Reexpe. C&uacute;cuta </option>
                        <option value="DC">Directo C&uacute;cuta</option>
                        <option value="RE">Reexpe. Ecuador </option>
                    </select>
                </div></td>
                 <td ><b>Estado de Viaje: </b>&nbsp;&nbsp;&nbsp;
                   <select name="viajes" class="listmenu">
                      <option value="TODOS" selected>Todos </option>
                      <option value="RUTA">En Ruta</option>
                      <option value="PORCONF">Por Confirmar Salida</option>
                      <option value="CONENTREGA">Con Entrega</option>
                    </select><br><br>
                    <b>Formato de Salida: </b></b>
                    <select name="displayInExcel" class="listmenu">
                      <option value="false" selected="selected">P&aacute;gina WEB </option>
                    </select>
      
                </td>
                </tr>
            </table>
          
  
            <!-- empanada nueva -->  </td>
            </tr>
        </table>
		
        <p>
        <table>
        <tr>
        <td>
            <% String evento = "ConsultarBtnClickUbicacion('"+CONTROLLER+"?estado=UbicacionVehicular&accion=Search')"; %>
            <tsp:boton id="botonConsultar" onclick="<%=evento%>" name="ConsultarBtn" value="buscar" />&nbsp;
        </td>
        <td>
            <tsp:boton id="botonLimpiarForma" type="reset" name="LimpiarFormularioBtn"  value="restablecer" />&nbsp;
        </td>
		<td ><img src="<%=BASEURL%>/images/botones/salir.gif"  name="salir" style="cursor:hand" title="Salir al Menu Principal" onMouseOver="botonOver(this);" onClick="parent.close();" onMouseOut="botonOut(this);" >
		</td>
        </tr>
    </table>
	<input type="hidden" name="listaTipoViaje" value="">
	<input type="hidden" name="nombreCliente" value="<%= request.getParameter("clienteSelected") %>">
		</p>
  
    </td>
    </tr>
    </table>
    </form>
	</div>
</body>
<tsp:InitCalendar/>
<% }catch( Exception a ){ a.printStackTrace(); } %>
