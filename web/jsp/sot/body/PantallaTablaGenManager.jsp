<!--
- Autor : Alejandro Payares
- Date  : Diciembre 24 del 2005
- Copyrigth Notice : Fintravalores S.A. S.A
- Descripcion : Pagina JSP que permite ver administrar las tablas dentro de tablagen.
-->
<%@page contentType="text/html"%>
<%@page import="com.tsp.operation.model.beans.TablaGen, java.util.*"%>
<%@include file="/WEB-INF/InitModel.jsp" %>
<%@ taglib uri="http://jakarta.apache.org/taglibs/input-0.90" prefix="input" %>
<%@ taglib uri="http://www.sanchezpolo.com/sot" prefix="tsp"%>
<%@ taglib uri="http://jsptags.com/tags/navigation/pager" prefix="pg" %>
<%
    String path = application.getRealPath(model.menuService.getRealUrl(request.getRequestURI()));
    String datos[] = model.menuService.getContenidoMenu(BASEURL,request.getRequestURI(),path);
%>
<%
   String style = "simple";
   String position =  "bottom";
   String index =  "center";
   int maxPageItems = 20;
   int maxIndexPages = 20;
   int tam =0;
%>
<html>
    <head> 
        <title>.: ADMINISTRACIÓN DE TABLAGEN :.</title>
        <script language="javascript" src="<%=BASEURL%>/js/tools.js"></script>
        <script language="javascript" src="<%=BASEURL%>/js/boton.js"></script>
        <script language="javascript" src="<%=BASEURL%>/jsp/sot/js/PantallaTablaGenManager.js"></script>
        <link href="<%=BASEURL%>/css/estilostsp.css" rel="stylesheet" type="text/css">
    </head>
    <body onresize="redimensionar()" onload='redimensionar()'>
    <div id="capaSuperior" style="position:absolute; width:100%; z-index:0; left: 0px; top: 0px;">
        <jsp:include page="/toptsp.jsp?encabezado=ADMINISTRACIÓN DE TABLAGEN"/>
    </div>
    <div id="capaCentral" style="position:absolute; width:100%; height:87%; z-index:0; left: 0px; top: 100px; overflow: scroll;"> 

        <%-- <jsp:useBean id="beanInstanceName" scope="session" class="beanPackage.BeanClassName" /> --%>
        <%-- <jsp:getProperty name="beanInstanceName"  property="propertyName" /> --%>
        <br>
            <%if(request.getParameter("mensaje") != null){%>
        <table border="2" align="center">
            <tr>
                <td><table width="100%"  border="1" align="center"  bordercolor="#F7F5F4" bgcolor="#FFFFFF">
                    <tr>
                        <td width="229" align="center" class="mensajes"><%=request.getParameter("mensaje")%></td>
                        <td width="29" background="<%=BASEURL%>/images/cuadronaranja.JPG">&nbsp;</td>
                        <td width="58">&nbsp;</td>
                    </tr>
                </table></td>
            </tr>
        </table>
        <br>
            <% } 
               LinkedList tablas = model.tablaGenService.obtenerTablas();  %>
            <% if ( tablas.isEmpty() ) { %>
        <table border="2" align="center">
            <tr>
                <td><table width="100%"  border="1" align="center"  bordercolor="#F7F5F4" bgcolor="#FFFFFF">
                <tr>
                <td width="229" align="center" class="mensajes">La tabla <%=model.tablaGenService.getDato("type")%> no tiene registros. Haga click en el boton Agregar para ingresar nuevos registros</td>
                <td width="29" background="<%=BASEURL%>/images/cuadronaranja.JPG">&nbsp;</td>
                <td width="58">&nbsp;</td>
            </tr>
            </table></td>
            </tr>
        </table>
        <br>
        <br>
        <center>
            <% String onclick = "window.location.href('"+BASEURL+"/jsp/sot/body/PantallaTablaGenNuevoRegistro.jsp?titulo=AGREGAR REGISTRO A "+model.tablaGenService.getDato("type")+"&type="+model.tablaGenService.getDato("type")+"&bean=no');"; %>
            <tsp:boton value="agregar" onclick="<%=onclick%>"/>
            &nbsp;&nbsp;&nbsp;&nbsp;
                <% onclick = "location.href='"+CONTROLLER+"?estado=TablaGen&accion=Manager'"; %>
            <tsp:boton value="regresar" onclick="<%=onclick%>"/>
        </center>
            <% } else { %>
        <form action='<%=CONTROLLER%>?estado=TablaGen&accion=Manager&cmd=eliminar&table_type=<%=model.tablaGenService.getDato("type")%>' method='post' name='forma'>
        <table align='center' border = '1' width="90%">
            <tr>
            <td>
                <table width='100%' border='1' bordercolor="#999999" bgcolor="#F7F5F4">
                <tr align='center'>
                    <td colspan="3" width="70%" align="left" class="subtitulo1">MANTENIMIENTO DE LA TABLA <%=model.tablaGenService.getDato("type")%><br></td>
                    <td colspan="2" width="30%" align="left" class="barratitulo"><img src="<%=BASEURL%>/images/titulo.gif" width="32" height="20" align="left"><%=datos[0]%></td>
                </tr>
                <tr align='center' class="tblTitulo">
                    <td width="5%">&nbsp;</td>
                    <td width="15%">CÓDIGO</td>
                    <td width="20%">REFERENCIA</td>
                    <td width="50%" nowrap>DESCRIPCIÓN</td>
                    <td width="10%">&nbsp;</td>
                </tr>
                        
                <pg:pager
                items="<%=tablas.size()%>"
                index="<%= index %>"
                maxPageItems="<%= maxPageItems %>"
                maxIndexPages="<%= maxIndexPages %>"
                isOffset="<%= true %>"
                export="offset,currentPageNumber=pageNumber"
                scope="request"
                >
                            <%  
                                tam = tablas.size();
                                Vector vec = new Vector(tablas);
                                for (int j = offset.intValue(), l = Math.min(j + maxPageItems, tablas.size()); j < l; j++){
                                    TablaGen tabla = (TablaGen)vec.elementAt(j);
                            %>
                <pg:item>
                    <tr class="<%=(j % 2 == 0 )?"filagris":"filaazul"%>" onMouseOver='cambiarColorMouse(this)' style="cursor:hand">
                        <td class='bordereporte' align='center' nowrap><input type="checkbox" name='select' value='<%=tabla.getOid()%>'></td>
                        <td class='bordereporte'><%=tabla.getTable_code()%></td>
                        <td class='bordereporte' nowrap><%=tabla.getReferencia()%></td>
                        <td class='bordereporte' nowrap><%=tabla.getDescripcion()%></td>
                        <td class='bordereporte' align='center'><a class="Simulacion_Hiper" href="<%=CONTROLLER%>?estado=TablaGen&accion=Manager&cmd=load&oid=<%=tabla.getOid()%>">EDITAR</a></td>
                    </tr>
                </pg:item>
                            <% } %>
                <tr class="tblTitulo">
                    <td td height="15" colspan="5" nowrap align="center"> 
                    <table>
                    <tr>
                    <td>
                        <pg:index>
                            <jsp:include page="/WEB-INF/jsp/googlesinimg.jsp" flush="true"/>      
                        </pg:index> 
                    </td>
                </tr>
                </table>
            </td>
                    </tr>
                </pg:pager>
            </table>
            </td>
            </tr>
        </table>
        </form>
        <p>
        <center>
            <table>
                <tr>
                <td>
                <% String onclick = "window.location.href('"+BASEURL+"/jsp/sot/body/PantallaTablaGenNuevoRegistro.jsp?titulo=AGREGAR REGISTRO A "+model.tablaGenService.getDato("type")+"&type="+model.tablaGenService.getDato("type")+"&bean=no');"; %>
                    <tsp:boton value="agregar" onclick="<%=onclick%>"/>
                </td>
                <td width='5'>&nbsp;</td>
                <td>
                    <tsp:boton value="eliminar" onclick="validarSeleccion()"/>
                </td>
                <td width='5'>&nbsp;</td>
                <td>
                <% onclick = "location.href='"+CONTROLLER+"?estado=TablaGen&accion=Manager'"; %>
                    <tsp:boton value="regresar" onclick="<%=onclick%>"/>
                </td>
                <td width='5'>&nbsp;</td>
                <td>
                    <tsp:boton value="salir" onclick="window.close()"/>
                </td>
                </tr>
            </table>
        </center>
        </p>
        <% } %>
    </div>
    <%= datos[1] %>
    </body>
</html>
