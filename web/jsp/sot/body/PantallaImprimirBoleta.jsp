<!--
- Autor : Ing. Ivan Dario Gomez Vanegas
- Date  : Diciembre 24 del 2005
- Copyrigth Notice : Fintravalores S.A. S.A
-->
<%--
-@(#)
--Descripcion : Pagina JSP, que muestra Las boletas listas para imprimir 
--%>

<%@page contentType="text/html"%>
<%@page session="true"%>
<%@page errorPage="/error/ErrorPage.jsp"%>
<%@page import="java.util.*"%>
<%@page import="com.tsp.operation.model.beans.*"%>
<%@page import="com.tsp.util.*"%>
<%@page import="com.tsp.operation.model.*"%>
<%@ include file="/WEB-INF/InitModel.jsp"%>

<% List	listado = model.DescuentoTercmSvc.getListaImpresion();
   List lisNombre = model.DescuentoTercmSvc.getListado();
   String Foto  = request.getParameter("foto");
   String Nombre = "";
   String OC = "";
%>
<html>
<head>

    <title>Impresion de boletas</title>
    <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
    <script src='<%= BASEURL %>/js/validar.js'></script>
    <script type='text/javascript' src="<%= BASEURL %>/js/boton.js"></script>
<style>
body {
	margin-left: 0px;
	margin-top: 0px;
	background-color: white;
	margin-right: 0px;
	margin-bottom: 0px;

}
.letraresaltada {	
    color: #003399;
	font-family: Tahoma, Arial;
	font-size: small;
	font-weight: bold;
	font-size: 12px;
}
.letraGrande {	
    color: #003399;
	font-family: Tahoma, Arial;
	font-size: small;
	font-weight: bold;
	font-size: 18px;
}
.letra {
    color: #003399;
	font-family: Tahoma, Arial;
	font-size: small;
	font-size: 12px;
	
}
H1.SaltoDePagina{
 PAGE-BREAK-AFTER: always
}

</style>	
</head>
<body  onResize="redimensionar()" onLoad="redimensionar();cargar();">
<center>
<form name="formImp" method="post" action="<%= CONTROLLER %>?estado=Descuento&accion=Tercm&Opcion=Cambiar">

    <%  Iterator Ite = lisNombre.iterator();
      if(Ite.hasNext()){
        DescuentoTercm  desc   = (DescuentoTercm)Ite.next();
        Nombre = desc.getNombre();
        OC     = desc.getOC();
      }%>
      <input type="hidden" name="OC" value='<%=OC%>'>
 <%
     Iterator It = listado.iterator();
     int i = 1;
      while(It.hasNext()){
       DescuentoTercm  tercm   = (DescuentoTercm)It.next(); %>
         <br><br><br>
         <table width='500' border='3'>
           <tr>
            <td>
             <table width='100%' border='0'>
              <tr>
                 <td class='letraresaltada'>BOLETA # : &nbsp;&nbsp;<%=tercm.getBoleta()%> </td><td><img src='<%=BASEURL%>/images/Logo.gif' align="right"></td>
              </tr>
              <tr>
                 <td></td><td><hr style='color:#003399;'></td>
              </tr>
              <tr>
                 <td align="center"><img src='<%=Foto%>' width='100' height='100'></td>
                 <td width='350'  class='letra'><b class='letraresaltada'>Fintravalores S.A. S.A.</b> autoriza a <b class='letraresaltada'><%=Nombre%></b>
                    con C.C: <b class='letraresaltada'><%=tercm.getCedula()%></b>  
                    el suministro de combustible al equipo con placa <b class='letraresaltada'> <%=tercm.getPlaca()%></b> y planilla <b class='letraresaltada'> <%=tercm.getOC()%></b> 
                    <br><br><br>por valor de:&nbsp;&nbsp; <b class='letraGrande'>$<%=Util.customFormat(Double.parseDouble(tercm.getValorPost()))%></b>
                    
                 </td>
             </tr>
              <tr>
                 <td colspan='2' width='400'>&nbsp;&nbsp;<br><br></td>
              </tr>
              <tr>
                 <td colspan='2' width='400'>&nbsp;&nbsp;<br><br></td>
              </tr>
              <tr>
                 <td colspan='2' class='letraresaltada' align="center" valign="top" width='100%'>Firma del funcionario &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Sello<br><br> Presente la boleta al momento de leer el chip � boton</td>
                 
              </tr>
              
             </table>
            </td>
           </tr> 
         </table>
         <% if(i==1){%>
              <br><br><br><br><br><br><br><br><br>
           <%}%>
         
         
         
         <% if((i==2)&&(It.hasNext())){ i=0;%>
               <H1 class="SaltoDePagina"> </H1>
            <%}%>
            
     <%i++;}%>
     
     </form>
     <table align="center">
          <tr align="center">
            <td colspan="2"> <img src="<%=BASEURL%>/images/botones/imprimir.gif"  name="imgaimprimir" onClick="esconder();formImp.submit();" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" style="cursor:hand">  </td>
          </tr>
        </table>
  </center>
  
</body>
<script>
  function esconder(){
         document.imgaimprimir.style.visibility='hidden';
         window.print();
         document.imgaimprimir.style.visibility='';
         
  }
               
</script>
</html>
