<%@page contentType="text/html"%>
<%@page errorPage="/error/ErrorPage.jsp"%>
<%@page import="com.tsp.operation.model.beans.*, java.util.*, com.tsp.util.*"%>
<%@taglib uri="/WEB-INF/tlds/tagsAgencia.tld" prefix="ag"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://jakarta.apache.org/taglibs/input-0.90" prefix="input" %>
<%@ include file="/WEB-INF/InitModel.jsp"%>
<% Util u = new Util(); %>

<%
  String msg = (request.getParameter("msg")!=null)?request.getParameter("msg"):"";
 
%>
<%
  String path = application.getRealPath(model.menuService.getRealUrl(request.getRequestURI()));
  String datos[] = model.menuService.getContenidoMenu(BASEURL,request.getRequestURI(),path);

	
	   
			       
%>
<html>
<head>
<title>Modificar Cliente</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link href="../../../css/estilostsp.css" rel="stylesheet" type="text/css"> 
<link href="<%=BASEURL%>/css/estilostsp.css" rel="stylesheet" type="text/css"> 
</head>
<script type='text/javascript' src="<%=BASEURL%>/js/boton.js"></script> 
<script type='text/javascript' src="<%= BASEURL %>/js/validar.js"></script>
<script type='text/javascript' src="<%= BASEURL %>/js/reporte.js"></script>
<script>
function BuscarCliente(codcli){
          
         var conceros = "";
         if(codcli.length < 6){     
              var tam = 6 - codcli.length;             
              for(var i = 1 ; i <= tam ; i++){
                 conceros = conceros + '0';
              }
              conceros = conceros + codcli;
              document.forma.Codigo.value = conceros;
	}      
}
</script>
<body onResize="redimensionar()" onLoad="redimensionar()">
<div id="capaSuperior" style="position:absolute; width:100%; height:100px; z-index:0; left: 0px; top: 0px;">
<jsp:include page="/WEB-INF/toptsp.jsp?encabezado=Buscar para Modificar Cliente"/>
</div>
<div id="capaCentral" style="position:absolute; width:100%; height:85%; z-index:0; left: -2px; top: 100px; overflow: scroll;"> 

<form name="forma" id="forma" method="post" action="<%=CONTROLLER%>?estado=ClienteBuscar&accion=Modificar" onSubmit="return validar(this);">
  <table width="30%"  border="2" align="center">
    <tr>
      <td><table width="100%"  border="0" align="center">
        <tr class="fila">
          <td colspan="2" ><table width="101%"  border="0" cellspacing="1" cellpadding="0">
              <tr>
                <td width="55%" class="subtitulo1">Buscar Cliente </td>
                <td width="45%" class="barratitulo"><img src="<%=BASEURL%>/images/titulo.gif" width="32" height="20" align="left"><%=datos[0]%></td>
              </tr>
          </table></td>
        </tr>
       <tr class="fila">
          <td width="50%" align="left">Codigo del Cliente </td>
          <td width="50%" nowrap>
            <input name="Codigo" type='text' id="Codigo"  size="10" maxlength="10" onBlur="BuscarCliente(this.value)">
            <img src="<%=BASEURL%>/images/botones/iconos/obligatorio.gif"></td>
	   </tr>
	</table></td>
    </tr>
  </table>
  <br>
  <center>
    <input type="image" src="<%=BASEURL%>/images/botones/buscar.gif" name="c_buscar" title="Buscar Clientes" style="cursor:hand" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);"> 
	<img src="<%=BASEURL%>/images/botones/cancelar.gif" name="c_cancelar" title="Resetear" style="cursor:hand" onClick="forma.reset(); forma.submit();" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" > 
	<img src="<%=BASEURL%>/images/botones/salir.gif" name="c_salir" title="Salir al Menu" style="cursor:hand" onClick="parent.close();" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);">
  </center>
 
</form> 
 	


<%
if(!msg.equals("")){%>
	<p>
  <table width="407" border="2" align="center">
     <tr>
    <td><table width="100%"   align="center"  >
      <tr>
        <td width="229" align="center" class="mensajes"><%=msg%></td>
        <td width="29" background="<%=BASEURL%>/images/cuadronaranja.JPG">&nbsp;</td>
        <td width="58">&nbsp;</td>
      </tr>
    </table></td>
  </tr>
</table>
  </p>
   <%}%>

</div>
<iframe width=188 height=166 name="gToday:normal:agenda.js:gfPop:plugins.js" id="gToday:normal:agenda.js:gfPop:plugins.js" src="<%=BASEURL%>/js/Calendario/ipopeng.htm" scrolling="no" frameborder="0" style="visibility:visible; z-index:999; position:absolute; left:-500px; top:0px;"> </iframe>    
<%=datos[1]%>
</body>
</html>

