<%@ page session="true" %>
<%@ page errorPage="/ErrorPage.jsp" %>
<%@ page import="java.util.*" %>
<%@ page import="com.tsp.operation.model.beans.*" %>
<%@ include file="/WEB-INF/InitModel.jsp" %>
<%@ taglib uri="http://www.sanchezpolo.com/sot" prefix="tsp"%>

<%
Usuario loggedUser = (Usuario) session.getAttribute("Usuario");
String tipoUsuario = loggedUser.getTipo();
String headerCliDest = (tipoUsuario.equals("ADMIN")? "Cliente" : "Destinatario");

Vector datos = model.confReportService.getDatosReporte();

String bodyMessage = (String)request.getAttribute("tsp.sot.bodyMessage");
if (bodyMessage != null) { %>
  <center>
      <h3><%= bodyMessage %></h3>
  </center><%
}
request.removeAttribute("tsp.sot.bodyMessage");
%>
<head>
    <script src='<%=BASEURL%>/js/PantallaConfigReportes.js' language='javascript'></script>
    <link href="<%=BASEURL%>/css/estilostsp.css" rel="stylesheet" type="text/css">
    <script type="text/javascript" src="<%=BASEURL%>/js/tools.js"></script>
</head>
    <body onresize="redimensionar()" onload = 'redimensionar()'>
        <div id="capaSuperior" style="position:absolute; width:100%; z-index:0; left: 0px; top: 0px;">
            <jsp:include page="/WEB-INF/toptsp.jsp?encabezado=CONFIGURACIÓN DE REPORTES"/>
        </div>
        <div id="capaCentral" style="position:absolute; width:100%; height:87%; z-index:0; left: 0px; top: 100px; overflow: scroll;"> 
        
            <form name='reportSetupFrm' 
                action="<%= CONTROLLER %>?estado=GestionReportes&accion=Setup" method='post'>
                <table align='center' border = '1'>
                    <tr>
                        <td width='600'>
                        <table align='center' width='100%' >
                        <tr>
                            <td align="center" class="subtitulo1">CONFIGURACI&Oacute;N GENERAL DE REPORTES</td>
                            <td width="15%" align="left" class="barratitulo"><img src="<%=BASEURL%>/images/titulo.gif" width="32" height="20"></td>
                        </tr>
                        <tr>
                        <td align='middle' height='50' colspan='2' >
                        <table width="100%"  border="1">
                        <tr class="tblTitulo">
                            <td nowrap width="16%"><div align="center">Cliente / Usuario</div></td>
                            <td width="20%"><div align="center">Reportes</div></td>
                            <td width="39%"><div align="center">Columnas</div></td>
                            <td width="14%"><div align="center">Editar</div></td>
                            <td width="11%"><div align="center">Eliminar </div></td>
                        </tr>
        <% for( int i=0; i<datos.size(); i++ ){ 
            ConfigReporte cf = (ConfigReporte) datos.elementAt(i);
            Vector reportes = cf.getReportes();
            ConfigReporte.Reporte rep = (ConfigReporte.Reporte) reportes.firstElement();
            String c = rep.getCampos();			
        %>
                        <tr class='<%= i%2==0?"filaGris":"filaAzul"%>'>
                        <td class='bordereporte' rowspan="<%=reportes.size()%>"><div align="center"><%=cf.getNombreCliente()%></div></td>
                        <td class='bordereporte'><%= rep.getNombre() %></td>
                        <td class='bordereporte'><%= rep.getCampos() %></td>
                                <% String onclick = "accionSeleccionarCampos('"+cf.getCodigoCliente()+"','"+rep.getCodigo()+"','"+headerCliDest.charAt(0)+"','"+rep.getCampos()+"','actualizar','"+BASEURL+"/jsp/sot/body/PantallaCamposReportes.jsp')"; %>
                        <td class='bordereporte' align='center'>
                            <a href='#' class="bordereporte"><img onclick="<%=onclick%>" src="<%=BASEURL%>/images/botones/iconos/modificar.gif"></a>
                        </td>
                        <td class='bordereporte'><div align="center">
                            <input type="checkbox" name="chk" value="<%=cf.getCodigoCliente()+"/"+rep.getCodigo()%>">
                        </div></td>
                    </tr>
        <% for( int j=1; j<reportes.size(); j++ ){
            rep = (ConfigReporte.Reporte) reportes.elementAt(j);  %>
                                <tr <%= j%2==0?"class='fila"+(i%2==0?"filaGris":"filaAzul")+"'":"class='"+(i%2==0?"filaAzul":"filaGris")+"'"%>>
                                <td class='bordereporte'><%= rep.getNombre() %></td>
                                <td class='bordereporte'><%= rep.getCampos() %></td>
                                <td class='bordereporte' align='center'>
                                    <a href='#' class="bordereporte"><img onclick="<%=onclick%>" src="<%=BASEURL%>/images/botones/iconos/modificar.gif"></a>
                                </td>
                                <td class='bordereporte'><div align="center">
                                    <input type="checkbox" name="chk" value="<%=cf.getCodigoCliente()+"/"+rep.getCodigo()%>">
                                </div></td>
                                </tr>
        <% }
        } %>
                            </table></td>
                            </tr>
                        </table>
                    </td>
                    </tr>
                </table>
                <center>
                    <p>
                    <table border="0">
                        <tr>
                            <td>
                                <tsp:boton name="submit2" 
                                onclick="document.reportSetupFrm.cmd.value = 'adicionar';document.reportSetupFrm.submit()" 
                                value='agregar'/>
                            </td>
                            <td width='100'>&nbsp;</td>
                            <td>
                                <tsp:boton name="botonEliminar" 
                                onclick="document.reportSetupFrm.cmd.value = 'eliminar';validarChecksEliminar()" 
                                value='eliminar'/>
                            </td>
                            <td width='100'>&nbsp;</td>
                            <td>
                                <tsp:boton name="botonSalir" 
                                onclick="window.close()" 
                                value='salir'/>
                            </td>
                        </tr>
                    </table>
                    </p>
                </center>
                <input type='hidden' name='cmd' value='edit'>
            </form>
        </div>
    </body>