<!--  
	 - Author(s)       :      LREALES
	 - Description     :      AYUDA FUNCIONAL - Reporte Proveedores Fintra
	 - Date            :      14/09/2006 
	 - Copyright Notice:      TRANSPORTES SANCHEZ POLO S.A.
-->
<%@ include file="/WEB-INF/InitModel.jsp"%>
<HTML>
<HEAD>
<TITLE>Funcionalidad del Modificar Cliente</TITLE>
<META http-equiv=Content-Type content="text/html; charset=windows-1252">
<link href="/../../../css/estilostsp.css" rel="stylesheet" type="text/css"> 
<link href="<%=BASEURL%>/css/estilostsp.css" rel="stylesheet" type="text/css">
<script type='text/javascript' src="<%= BASEURL %>/js/boton.js"></script> 
</HEAD>
<BODY> 
<% String BASEIMG = BASEURL +"/images/ayuda/Cliente/"; %>
  <table width="95%"  border="2" align="center">
    <tr>
      <td height="117" >
        <table width="100%" border="0" align="center">
          <tr  class="subtitulo">
            <td height="20"><div align="center">MANUAL DE REPORTE WEB</div></td>
          </tr>
          <tr class="subtitulo1">
            <td>Descripci&oacute;n del funcionamiento del programa Modificar Cliente </td>
          </tr>
          <tr>
            <td  class="ayudaHtmlTexto"><div align="center"></div></td>
          </tr>
<tr>
            <td  class="ayudaHtmlTexto"><p class="ayudaHtmlTexto">&nbsp;</p>
            <p class="ayudaHtmlTexto">En la siguiente pantalla  muestra todos los campos obligatorios donde dependiendo del campo dejara asignar ya sea caracteres o numeros</p>
            <p class="ayudaHtmlTexto"> un ejemplo podria ser en la Cedula del Cliente solo deja ingrasar numeros y el campo Nombre del Cliente deja ingresar caracteres o numeros . En el codigo del Cliente no se puede Digitar ya que el codigo no se puede cambiar por lo tanto la caja es gris. Hay campos donde solo se puede ingresar un valor ya sea numero o caracteres un ejemplo Fiduciaria solo deja ingresar un parametro</p>
            </td>
          </tr>
<tr>
            <td  class="ayudaHtmlTexto"><div align="center"><IMG src="<%=BASEIMG%>image007.JPG" width="1001" height=872 border=0 ></div></td>
          </tr>
<tr>
  <td  class="ayudaHtmlTexto"><p>&nbsp;</p>
    <p>El sistema le muestra el mensaje con el Nombre del Cliente que fue Modificado a la BD exitosamente.</p></td>
</tr>
<tr>
  <td  class="ayudaHtmlTexto"><div align="center"> <img height=181 src="<%=BASEIMG%>image008.JPG" width=430 border=0 v:shapes="_x0000_i1052"></div></td>
</tr>
<tr>
  <td  class="ayudaHtmlTexto"><p>&nbsp;</p>
    <p>En esta pantalla muestra cuando el Cliente en la Busqueda no existe en la BD</p></td>
</tr>
<tr>
  <td  class="ayudaHtmlTexto"><p align="center"><img src="<%=BASEIMG%>image006.JPG" width=439 height=184 border=0 lowsrc="file:///C|/Tomcat5/webapps/fintravalores/images/ayuda/Cliente/image009.JPG" v:shapes="_x0000_i1052"></p>    </td>
</tr>
<tr>
  <td  class="ayudaHtmlTexto"><p>&nbsp;</p>
    <p>El sistema tambien usa u sistema de caja cerrada donde usted de darle click en la casilla y se desplegara un listado pues como muestra en la pantalla la </p>
    <p>Agencia de Duenia del cliente.</p></td>
</tr>
<tr>
  <td  class="ayudaHtmlTexto"><div align="center"><IMG height=874 src="<%=BASEIMG%>image004.JPG" width=1007 border=0 v:shapes="_x0000_i1052"></div></td>
</tr>


<tr>
  <td  class="ayudaHtmlTexto"><p>&nbsp;</p>
    <p>Luego de escribir en los campos del sistemas y dejo alguno vacio le saldra el siguiente Mensaje. Despendiendo del campo que halla dejado en blanco </p>
    <p>le saldra el mesaje especifico en este caso fue el de cedula... </p></td>
</tr>
<tr>
  <td  class="ayudaHtmlTexto"><div align="center"><IMG height=126 src="<%=BASEIMG%>image_error001.JPG" width=327 border=0 v:shapes="_x0000_i1052"></div></td>
</tr>

<tr>
  <td  class="ayudaHtmlTexto"><p>&nbsp;</p>
    <p>Mensaje que se muestra cuando a sido eliminado el Cliente del Sistema le muestra la caja de busqueda por si quiere buscar otro Cliente. </p>
    </td>
</tr>
<tr>
  <td  class="ayudaHtmlTexto"><div align="center"><IMG height=199 src="<%=BASEIMG%>image009.JPG" width=425 border=0 v:shapes="_x0000_i1052"></div></td>
</tr>
        </table></td>
    </tr>
  </table>
  <p></p>
<center>
<img src = "<%=BASEURL%>/images/botones/salir.gif" style = "cursor:hand" name = "imgsalir" onClick = "parent.close();" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);">
</center>
</BODY>
</HTML>
