<!--
- Autor : Alejandro Payares
- Date  : 20 de Enero de 2006, 09:00:12 a.m.
- Copyrigth Notice : Fintravalores S.A. S.A
--Descripcion : Pagina JSP que permite ver el reporte de movimiento de aduana de una remesa.
-->
<%@page contentType="text/html"%>
<%@page import="com.tsp.operation.model.beans.MovimientoAduana, java.util.LinkedList,java.util.Iterator"%>
<%@include file="/WEB-INF/InitModel.jsp" %>
<%@ taglib uri="http://jakarta.apache.org/taglibs/input-0.90" prefix="input" %>
<%@ taglib uri="http://www.sanchezpolo.com/sot" prefix="tsp"%>

<html>
    <head> 
        <title>.: Movimientos de aduana - remesa: <%=request.getParameter("numrem")%> :.</title>
        <script language="javascript" src="<%=BASEURL%>/js/tools.js"></script>
        <link href="<%=BASEURL%>/css/estilostsp.css" rel="stylesheet" type="text/css">
    </head>
    <body>
    
        <%-- <jsp:useBean id="beanInstanceName" scope="session" class="beanPackage.BeanClassName" /> --%>
        <%-- <jsp:getProperty name="beanInstanceName"  property="propertyName" /> --%>
        <br>
        <% LinkedList datos = model.movAduanaService.obtenerMovimientos();
           if ( datos != null && !datos.isEmpty() ) { %>
        <form name='forma' method='post' action='<%=CONTROLLER%>?estado=TablaGen&accion=Manager&cmd=droptable'>
        <table align='center' border = '1'>
            <tr>
                <td width='750px'>
                    <table width='100%'>
                        <tr align='center'>
                            <td colspan="3" align="center" class="subtitulo1">DETALLE DE <%=request.getParameter("detalle")%> DE ADUANA<br>REMESA N� <%=request.getParameter("numrem")%></td>
                            <td colspan="1" width="15%" align="left" class="barratitulo"><img src="<%=BASEURL%>/images/titulo.gif" width="32" height="20"></td>
                        </tr>
                        <tr align='center' class="tblTitulo">
                            <td width='10%'>ESTADO</td>
                            <td>FECHA DE ESTADO </td>
                            <td>OBSERVACI&Oacute;N</td>
                            <td>FECHA OBSERVACI&Oacute;N </td>
                        </tr>
                            <%
                               Iterator ite = datos.iterator();
                               for(int i=0; ite.hasNext(); i++ ){
                                   MovimientoAduana mova = (MovimientoAduana)ite.next();
                                   
                            %>
                        <tr class="<%=(i % 2 == 0 )?"filagris":"filaazul"%>" onMouseDown='cambiarColor(this)' style="cursor:hand">
                            <td class='bordereporte' align='center'><%=mova.getEstado()%></td>
                            <td class='bordereporte' align='center'><%=mova.getFechaestado() == null || "0099-01-01".equals(mova.getFechaestado())?"-":mova.getFechaestado()%></td>
                            <td class='bordereporte' align='center'><%=mova.getObservacion()%></td>
                            <td class='bordereporte' align='center'><%=mova.getFechaobservacion() == null || "0099-01-01".equals(mova.getFechaobservacion())?"-":mova.getFechaobservacion()%></td>
                        </tr>
                            <% } %>
                    </table>
                </td>
            </tr>
        </table>
        </form>
        <p>
        <center>
            <tsp:boton value="aceptar" onclick="window.close();"/>
        </center>
        </p>
        <% }
           else {%>
        <table width="400"  border="1" align="center"  bordercolor="#F7F5F4" bgcolor="#FFFFFF">
            <tr>
            <td width="85%" align="center" class="mensajes">No se encontraron movimientos de aduana para la remesa <%=request.getParameter("numrem")%></td>
            <td width="29" background="<%=BASEURL%>/images/cuadronaranja.JPG">&nbsp;</td>
            <td width="*">&nbsp;</td>
            </tr>
        </table>
        <br>
        <center>
            <tsp:boton value="aceptar" onclick="window.close();"/>
        </center>
        <% } %>
    </body>
</html>
