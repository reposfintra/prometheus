<!--
- Autor : Ing. Ivan Dario Gomez Vanegas
- Date  : 3 de Enero de 2006
- Copyrigth Notice : Fintravalores S.A. S.A
-->
<%--
-@(#)
--Descripcion : Pagina JSP, que maneja el reporte de las prefacturas para los descuentos 
                por concepto de combustible
--%>

<%-- Declaracion de librerias--%>
<%@page contentType="text/html"%>
<%@page session="true"%>
<%@page errorPage="../error/ErrorPage.jsp"%>
<%@page import="java.util.*"%>
<%@page import="com.tsp.operation.model.*"%>
<%@page import="com.tsp.util.*"%>
<%@page import="com.tsp.operation.model.beans.*, java.util.*"%>
<%@include file="/WEB-INF/InitModel.jsp"%>
<%@ taglib uri="http://jakarta.apache.org/taglibs/input-0.90" prefix="input" %>
<html>
<head>
<title>Consulta De Prefacturas</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link href="<%= BASEURL %>/css/estilostsp.css" rel="stylesheet" type="text/css"> 
<link href="../../../css/estilostsp.css" rel="stylesheet" type="text/css"> 
<script src='<%=BASEURL%>/js/date-picker.js'></script>
<script type='text/javascript' src="<%= BASEURL %>/js/validar.js"></script>
<script type='text/javascript' src="<%= BASEURL %>/js/boton.js"></script>
<script type='text/javascript' src="<%= BASEURL %>/js/reporte.js"></script>
</head>
<%List listado =  model.DescuentoTercmSvc.getPrefactura();
  String OP  =  (request.getParameter("OP") != null)?request.getParameter("OP"):"";
  String fecini = request.getParameter("fechaini");
  String fecfin = request.getParameter("fechafin");
%>
<%-- Inicio Body --%>
<body onLoad="redimensionar();" onResize="redimensionar();">
	<div id="capaSuperior" style="position:absolute; width:100%; height:100px; z-index:0; left: 0px; top: 0px;">
	<jsp:include page="/WEB-INF/toptsp.jsp?encabezado=Prefactura de combustible TERCM"/>
	</div>
	<%if(OP.equals("Listar")){
             Iterator It = listado.iterator();
              if(listado.size()>0){
                  %>
             <div id="capaCentral" style="position:absolute; width:100%; height:85%; z-index:0; left: 0px; top: 100px; overflow: scroll;"> 
                <table width="416" border="2" align="center">
                      <tr>
                      <td>
                          <table width="100%" align="center">
                              <tr>
                                  <td width="373" class="subtitulo1">&nbsp;Listado de boletas sin facturar </td>
                                  <td width="427" class="barratitulo"><img src="<%=BASEURL%>/images/titulo.gif" width="32" height="20"></td>
                              </tr>
                          </table>
                          <table width="100%" border="1" bordercolor="#999999" bgcolor="#F7F5F4" align="center">
                          <tr class="tblTitulo" align="center">
                              <td width="69" nowrap >Boleta #</td>
                              <td width="200" nowrap >Fecha de Impresion</td>
                              <td width="45" nowrap >Placa</td>
                              <td width="45" nowrap >planilla</td>
                              <td width="45" nowrap >Valor</td>
                          </tr>
                         <%int i = 0;
                            while(It.hasNext()){
                           DescuentoTercm  tercm   = (DescuentoTercm)It.next();%>
                              <tr class="<%=(i % 2 == 0 )?"filagris":"filaazul"%>" onMouseOver='cambiarColorMouse(this)'   style="cursor:hand" >
                                  <td align="center" class="bordereporte"><%=tercm.getBoleta()%></td>
                                  <td align="center" class="bordereporte"><%=tercm.getFecha()%></td>
                                  <td align="center" class="bordereporte"><%=tercm.getPlaca()%></td>
                                  <td align="center" class="bordereporte"><%=tercm.getOC()%></td>
                                  <td align="center" class="bordereporte"><%=tercm.getValorPost()%></td>
                              </tr>
                              <%i++;}%>
                      </table>
                      </td>
                      </tr>
                  </table><br>
                   <center>
                     <form name="form" method="post" action="<%=CONTROLLER%>?estado=Descuento&accion=Tercm&Opcion=xls">
                        <input type="hidden" name='fechaini' value="<%=fecini%>">
                        <input type="hidden" name='fechafin' value="<%=fecfin%>">
			<img src="<%=BASEURL%>/images/botones/exportarExcel.gif" name="imgaceptar"  height="21" onClick="form.submit();" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" style="cursor:hand"> &nbsp; 
			<img src="<%=BASEURL%>/images/botones/salir.gif"  name="imgsalir"  height="21" onMouseOver="botonOver(this);" onClick="parent.close();" onMouseOut="botonOut(this);" style="cursor:hand">&nbsp;
		    </form>	
		   </center>	
              </div>
                  
	     <%}else{%>
	            <div id="capaCentral" style="position:absolute; width:100%; height:85%; z-index:0; left: 0px; top: 100px; overflow: scroll;"> 
                        <table border="2" align="center">
                            <tr>
                                <td><table width="100%"  border="1" align="center"  bordercolor="#F7F5F4" bgcolor="#FFFFFF">
                                <tr>
                                <td width="229" align="center" class="mensajes">No se encontraron registros... 
                                </td>
                                <td width="29" background="<%=BASEURL%>/images/cuadronaranja.JPG">&nbsp;</td>
                                <td width="58">&nbsp;</td>
                            </tr>
                            </table>
                            </td>
                            </tr>
                        </table>
                        <center><br><img src="<%=BASEURL%>/images/botones/salir.gif"  name="imgsalir" onClick="parent.close();" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" style="cursor:hand"></center>
	            </div> 
	       <%}%>
	
	<%}else{
             
         if(!OP.equals("mensage")){%>
	<div id="capaCentral" style="position:absolute; width:100%; height:85%; z-index:0; left: 0px; top: 100px; overflow: scroll;">
		<form name="forma" method="post" action="<%=CONTROLLER%>?estado=Descuento&accion=Tercm&Opcion=listar">
		<table width="500" border="2" align="center">
			<tr>
				<td>
					<table width="500" align="center" class="tablaInferior">
						<tr class="fila">
							<td width="242" align="left" class="subtitulo1">&nbsp;Reporte Prefacturas</td>
							<td width="242" align="left" class="barratitulo"><img src="<%=BASEURL%>/images/titulo.gif" width="32" height="20"></td>
						</tr>
					</table>
	                                <table width="100%" border="0" align="center" class="tablaInferior">
						<tr class="fila">
                                                <td><strong>Fecha inicial </strong></td>
                                                <td>
                                                  <input name='fechaini' type='text' class="textbox" id="fechai" style='width:120' value='' readonly>
                                                  <a href="javascript:void(0);" class="link" onFocus="if(self.gfPop)gfPop.fPopCalendar(document.forma.fechai);return false;"  HIDEFOCUS > <img src="<%=BASEURL%>\js\Calendario\cal.gif" width="16" height="16"
                                               border="0" alt="De click aqu&iacute; para ver el calendario."></a> <img src="<%= BASEURL%>/images/botones/iconos/obligatorio.gif" width="10" height="10"></td>
                                              </tr>
                                              <tr class="fila" >
                                                <td><strong>Fecha final </strong></td>
                                                <td>
                                                  <input name='fechafin' type='text' class="textbox" id="fechaf" style='width:120' value='' readonly>
                                                  <a href="javascript:void(0);" class="link" onFocus="if(self.gfPop)gfPop.fPopCalendar(document.forma.fechaf);return false;"  HIDEFOCUS > <img src="<%=BASEURL%>\js\Calendario\cal.gif" width="16" height="16"
                                               border="0" alt="De click aqu&iacute; para ver el calendario."></a> <img src="<%= BASEURL%>/images/botones/iconos/obligatorio.gif" width="10" height="10"></td>
                                              </tr>
					</table>
				</td>
			</tr>
		</table>
		<div align="center"><br>
			<img src="<%=BASEURL%>/images/botones/aceptar.gif" name="imgaceptar"  height="21" onClick="forma.submit();" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" style="cursor:hand"> &nbsp; 
			<img src="<%=BASEURL%>/images/botones/salir.gif"  name="imgsalir"  height="21" onMouseOver="botonOver(this);" onClick="parent.close();" onMouseOut="botonOut(this);" style="cursor:hand">&nbsp;
		</div>
		</form>
            <%}else{%>
                 <div id="capaCentral" style="position:absolute; width:100%; height:85%; z-index:0; left: 0px; top: 100px; overflow: scroll;"> 
                        <table border="2" align="center">
                            <tr>
                                <td><table width="100%"  border="1" align="center"  bordercolor="#F7F5F4" bgcolor="#FFFFFF">
                                <tr>
                                <td width="229" align="center" class="mensajes">El archivo fue enviado al log de procesos... 
                                </td>
                                <td width="29" background="<%=BASEURL%>/images/cuadronaranja.JPG">&nbsp;</td>
                                <td width="58">&nbsp;</td>
                            </tr>
                            </table>
                            </td>
                            </tr>
                        </table>
                        <center><br><img src="<%=BASEURL%>/images/botones/salir.gif"  name="imgsalir" onClick="parent.close();" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" style="cursor:hand"></center>
                        
	            </div> 
            
		<%}%>
 	<%}%>
	<iframe width="188" height="166" name="gToday:normal:agenda.js:gfPop:plugins.js" id="gToday:normal:agenda.js:gfPop:plugins_24.js" src="<%=BASEURL%>/js/Calendario/ipopeng.htm" scrolling="no" frameborder="0" style="visibility:visible; z-index:999; position:absolute; left:-500px; top:0px;">
                        </iframe>	
 </div>
</body>
</html>

