<!--  
     - Author(s)       :      Alejandro Payares
     - Date            :      06/05/2006
     - Copyright Notice:      TRANSPORTES SANCHEZ POLO S.A.
-->
<%--   
     - @(#)  
     - Description: Programa de mantenimiento de tablagen
--%> 
<%@include file="/WEB-INF/InitModel.jsp"%>
<html>
    <head>
        <title>Funcionalidad de la tabla de administración de tablagen</title>
        <link href="../../../css/estilostsp.css" rel="stylesheet" type="text/css">
        <link href="<%= BASEURL %>/css/estilostsp.css" rel="stylesheet" type="text/css">
    </head>
    <body>
        <BR>
        <table width="696"  border="2" align="center">
            <tr>
            <td width="811" >
                <table width="100%" border="0" align="center">
                    <tr  class="subtitulo">
                        <td height="24" align="center">
                            ADMINISTRACI&Oacute;N DE TABLAGEN
                        </td>
                    </tr>
                    <tr class="subtitulo1">
                        <td> 
                            <a name="agregarTabla"><!-- --></a>INGRESO DE UNA NUEVA TABLA
                        </td>
                    </tr>
                    <tr class="ayudaHtmlTexto">
                        <td>
                            Formulario de ingreso de nueva tabla en tablagen.<br>
                            <div align="center">
                                <img src="<%= BASEURL %>/images/ayuda/sot/tablagen/form_nueva_tabla.png" width="747" height="331" align="absmiddle">	
                            </div>
                            En caso de presionar el boton agregar sin haber digitado el nombre de la tabla, aparecer&aacute; el siguiente mensaje de advertencia: <br>	
		    
                            <div align="center">
                                <p>
                                    <img src="<%= BASEURL %>/images/ayuda/sot/tablagen/alert_nombre_nueva_tabla.png" width="242" height="132" align="absmiddle" >	
                                </p>
                            </div>
                            <p>
                                <br>
                                El siguiente mensaje de advertencia aparecer&aacute; al intentar ingresar la nueva tabla sin la descripci&oacute;n:
                            </p>
                            <p align="center">
                                <img src="<%= BASEURL %>/images/ayuda/sot/tablagen/alert_desc_nueva_tabla.png" width="261" height="132" align="absmiddle" >
                            </p>
                            <p align="left">
                                <br>
                                Si al momento de agregar la nueva tabla usted digita un nombre de una tabla que ya existe en el tablagen, aparecer&aacute; un mensaje como el siguiente encima del formulario:
                            </p>
                            <p align="center">
                                <img src="<%= BASEURL %>/images/ayuda/sot/tablagen/mensaje_tabla_existente.png" width="355" height="45" align="absmiddle" >
                            </p>
                            <p align="left"> 
                                Si la nueva tabla a agregar fu&eacute; agregada sin problemas aparecer&aacute; un mensaje como el siguiente en la parte superior del formulario:
                            </p>
                            <p align="center">
                                <img src="<%= BASEURL %>/images/ayuda/sot/tablagen/mensaje_tabla_nueva.png" width="354" height="45" align="absmiddle" >	
                            </p>
                            <br>												
                        </td>
                    </tr>
                    <tr class="subtitulo1">
                        <td><a name="adminTablas"><!-- --></a>ADMINISTRACI&Oacute;N DE TABLAS </td>
                    </tr>
                    <tr class="ayudaHtmlTexto">
                        <td>
                            <p>
                                En esta ventana usted puede ver, editar y eliminar las tablas agregadas en tablagen.
                            </p>
                            <p align="center">
                                <img src="<%= BASEURL %>/images/ayuda/sot/tablagen/tablagen_list.png" width="1114" height="567" align="absmiddle" >
                            </p>
                            <p align="left">
                                Al momento de eliminar una o mas tablas de tablagen aparecer&aacute; un mensaje de confirmaci&oacute;n como el siguiente:
                            </p>
                            <p align="center">
                                <img src="<%= BASEURL %>/images/ayuda/sot/tablagen/confirmacion_eliminacion_tablas.png" width="627" height="219" align="absmiddle" > 
                            </p>
                            <p></p>
                        </td>
                    </tr>
                    <tr class="subtitulo1">
                        <td>
                            <a name="modificarTabla"><!-- --></a>EDITANDO UNA TABLA:
                        </td>
                    </tr>
                    <tr class="ayudaHtmlTexto">
                        <td>
                            <p align="left">Para iniciar el proceso de edición de una tabla, haga click sobre el vonculo editar de la tabla deseada</p>
                            <p align="center"><img src="<%= BASEURL %>/images/ayuda/sot/tablagen/antes_de_editar.png" width="669" height="168" border="1" align="absmiddle" > </p>
                            <p align="left">Al hacer click sobre la tabla indicada se mostrar&aacute; una pantalla como la siguiente:</p>
                            <p align="center"><img src="<%= BASEURL %>/images/ayuda/sot/tablagen/editar_tablagen.png" width="713" height="355" border="0" align="absmiddle" > </p>
                            <p align="left">En caso de presionar el boton modificar sin haber digitado el nombre de la tabla, aparecer&aacute; el siguiente mensaje de advertencia: <br>
                            </p>
                            <div align="center">
                                <p><img src="<%= BASEURL %>/images/ayuda/sot/tablagen/alert_nombre_nueva_tabla.png" width="242" height="132" align="absmiddle" > </p>
                            </div>
                            <p><br>
                            El siguiente mensaje de advertencia aparecer&aacute; al intentar modificar la nueva tabla sin la descripci&oacute;n:</p>
                            <p align="center"><img src="<%= BASEURL %>/images/ayuda/sot/tablagen/alert_desc_nueva_tabla.png" width="261" height="132" align="absmiddle" ></p>
                            <p align="left">Si al momento de modificar el nombre de la tabla resulta que el nuevo nombre es igual al de otra tabla que ya existe, aparecer&aacute; el la parte superior del formulario un mensaje como el siguiente:</p>
                            <p align="center"><img src="<%= BASEURL %>/images/ayuda/sot/tablagen/mensaje_tabla_existente2.png" width="354" height="46" align="absmiddle" > </p>
                            <p align="center">&nbsp;</p>
                        </td>
                    </tr>
                    <tr class="subtitulo1">
                        <td>
                            <a name="adminRegistros"><!-- --></a>ADMINISTRACI&Oacute;N DE REGISTROS DE UNA TABLA 
                        </td>
                    </tr>
                    <tr class="ayudaHtmlTexto">
                        <td>
                            <p align="left">Para iniciar el proceso de administraci&oacute;n una tabla espec&iacute;fica de tablagen, haga click sobre la tabla deseada:</p>
                            <p align="center"><img src="<%= BASEURL %>/images/ayuda/sot/tablagen/antes_de_admin_tabla.png" width="433" height="238" border="1" align="absmiddle" > </p>
                            <p align="left">Hal hacer click sobre la tabla deseada se mostrar&aacute; una ventana donde se muestran los registros de esa tabla, como la siguiente:</p>
                            <p align="center"><img src="<%= BASEURL %>/images/ayuda/sot/tablagen/admin_tabla.png" width="960" height="359" border="0" align="absmiddle" ></p>
                            <p align="left">Al momento de eliminar un registro aparecer&aacute; un mensaje de confirmaci&oacute;n como el siguiente:</p>
                            <p align="center">
                                <img src="<%= BASEURL %>/images/ayuda/sot/tablagen/confirm_elminacion_registro.png" width="373" height="130" align="absmiddle" >
                            </p>
                        </td>
                    </tr>
                    <tr class="subtitulo1">
                        <td>
                            <a name="modificarRegistro"><!-- --></a> EDITANDO UN REGISTRO: 
                         </td>
                    </tr>
                    <tr class="ayudaHtmlTexto">
                        <td>
                            <p align="left">Para iniciar la edici&oacute;n de un registro, haga click sobre el registro</p>
                            <p align="center"><img src="<%= BASEURL %>/images/ayuda/sot/tablagen/antes_de_editar_registro.png" width="571" height="195" border="1" align="absmiddle" > </p>
                            <p align="left">Despues de hacer click sobre el registro que se quiere editar, aparecer&aacute; una pantalla como la siguiente:</p>
                            <p align="center"><img src="<%= BASEURL %>/images/ayuda/sot/tablagen/editar_registro.png" width="596" height="390" border="0" align="absmiddle" > </p>
                            <p align="left">Debe tener en cuenta que el codigo del registro no puede ser vacio, de ser asi aparecer&aacute; el siguiente mensaje:</p>
                            <p align="center"><img src="<%= BASEURL %>/images/ayuda/sot/tablagen/alert_cod_nuevo_registro.png" width="232" height="128" border="0" align="absmiddle" > </p>
                        </td>
                    </tr>
                    <tr class="subtitulo1">
                        <td><a name="agregarRegistro"><!-- --></a>AGREGANDO UN REGISTRO: </td>
                    </tr>
                    <tr class="ayudaHtmlTexto">
                        <td>
                            <p align="left">Sobre la ventana de administraci&oacute;n de la tabla actual haga click en el boton agregar en la parte inferior de la ventana.</p>
                            <p align="center"><img src="<%= BASEURL %>/images/ayuda/sot/tablagen/antes_de_agregar_registro.png" width="646" height="253" border="1" align="absmiddle" > </p>
                            <p align="left">Despues de hacer click sobre el boton aceptar, aparecer&aacute; el siguiente formulario:</p>
                            <p align="center"><img src="<%= BASEURL %>/images/ayuda/sot/tablagen/agregar_registro.png" width="600" height="423" border="0" align="absmiddle" > </p>
                            <p align="left">Debe tener en cuenta que el codigo del registro no puede ser vacio, de ser asi aparecer&aacute; el siguiente mensaje:</p>
                            <p align="center"><img src="<%= BASEURL %>/images/ayuda/sot/tablagen/alert_cod_nuevo_registro.png" width="232" height="128" border="0" align="absmiddle" > </p>          
                            <p>Si al momento de agregar el registro resulta que ya existe un registro con ese mismo codig&oacute; aparecer&aacute; el siguiente mensaje:</p>
                            <p align="center"><img src="<%= BASEURL %>/images/ayuda/sot/tablagen/mensaje_registro_existente.png" width="352" height="53" border="0" align="absmiddle" ></p>          <p>Si el registro fue agregado correctamente  aparecer&aacute; el siguiente mensaje:</p>          
                            <p align="center"><img src="<%= BASEURL %>/images/ayuda/sot/tablagen/mensaje_nuevo_registro.png" width="355" height="55" border="0" align="absmiddle" ></p>
                            <p>&nbsp;</p>          <p align="center">&nbsp;</p>          <p align="left">&nbsp;</p>
                        </td>
                    </tr>
                </table>
            </td>
            </tr>
        </table>
        <p></p>
        <p></p>
        <p></p>
        <p align="center" class="fuenteAyuda"> Fintravalores S. A. </p>
    </body>
</html>