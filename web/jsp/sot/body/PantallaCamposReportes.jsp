<%@ page session="true" %>
<%@ page errorPage="/ErrorPage.jsp" %>
<%@ page import="java.util.*" %>
<%@ page import="com.tsp.operation.model.beans.*" %>
<%@ taglib uri="http://www.sanchezpolo.com/sot" prefix="tsp"%>
<%@ include file="/WEB-INF/InitModel.jsp" %>
<%  String [] params   = new String[3];
    params[0] = request.getParameter("codCliDest");
    params[1] = request.getParameter("codReporte");
    params[2] = request.getParameter("tipoUsuario");
    String tipo = request.getParameter("tipo");
 %>
 <script>
     window.resizeTo(400,650);    
     window.moveTo  (250,50);
 </script>
   

<html>
    <head>
        <title>Escoger campos del reporte </title>
        <script src='<%=BASEURL%>/js/PantallaConfigReportes.js' language='javascript'></script>
        <link href="<%=BASEURL%>/css/estilostsp.css" rel="stylesheet" type="text/css">
    </head>
    <body onload="camposReportesLoad('<%=request.getParameter("campos")%>');">
<%
    String next = tipo.equals("actualizar")?CONTROLLER +"?estado=GestionReportes&accion=Setup&cmd=actualizar":"";
%>
        <form name='reportFieldsFrm' method='post' action="<%= next %>">
            <input type=hidden name='cliente' value="<%=request.getParameter("codCliDest")%>">
            <input type=hidden name='reporte' value="<%=request.getParameter("codReporte")%>">
            <input type=hidden name='tipoUsuario' value="<%=request.getParameter("tipoUsuario")%>">
    <% 
        String [] campos = model.consultasGrlesReportesService.buscarCamposDeReporte(request.getParameter("codReporte"));
        if( campos != null ) { %>
            <input type=hidden id="numeroDeCampos" value="<%= campos.length %>">
             <table cellSpacing=1 cellPadding=1 align=center border=1 class='fondotabla' width='300'>
                <tr align='center'>
                    <td colspan="2" align="center" class="subtitulo1">LISTA DE CAMPOS DEL REPORTE</td>
                    <td colspan="1" width="15%" align="left" class="barratitulo"><img src="<%=BASEURL%>/images/titulo.gif" width="32" height="20"></td>
                </tr>
                <tr class="fila">
                <td class='bordereporte' colspan='3'><input onclick='ejecutarOpcion(this)' checked='true' type='radio' id="opcion"  name="opcion" value="0"/>No hacer nada</td>
                </tr>
                <tr class="fila">
                    <td class='bordereporte' colspan='3'><input onclick='ejecutarOpcion(this)' type='radio' id="opcion"  name="opcion"  value="1"/>Ordenar ascendentemente</td>
                </tr>
                <tr class="fila">
                    <td class='bordereporte' colspan='3'><input onclick='ejecutarOpcion(this)' type='radio' id="opcion"  name="opcion"  value="2"/>Ordenar descendentemente</td>
                </tr>
                <tr class="fila">
                    <td class='bordereporte' colspan='3'><input onclick='ejecutarOpcion(this)' type='radio' id="opcion"  name="opcion"  value="3"/>Actualizar orden automaticamente</td>
                </tr>
                <tr class='tblTitulo'>
                    <td colspan='2' align='left' >
                        <input type='checkbox' name='selectAll'
                        onclick='selectAllClick(document.reportFieldsFrm.numeroDeCampos.value)'>Seleccionar Todos
                    </td>
                    <td>Orden</td>
                </tr>
     <%
      String checked = null;
      Hashtable titulos = model.consultasGrlesReportesService.obtenerTitulosDeReporte(request.getParameter("codReporte"));
   for( int idx = 0; idx < campos.length; idx++ ){ %>
                <tr class="<%=idx%2 == 0?"filaGris":"filaAzul"%>">
                    <td width='5%' class='bordereporte' >
                        <input type='checkbox' name='fields' id='fld<%= idx %>'
                        value='<%= campos[idx] %>' onclick='actualizarComboOrden(this)'>
                    </td>
                    <td class='bordereporte' width='*' ><%= titulos.get(campos[idx]) %></td>
                    <td class='bordereporte' width="15%">
		  
                        <select onchange='verificarOrden(this)' id="orden<%= campos[idx] %>" name="orden<%= campos[idx] %>" disabled >
                        </select>
                    </td>
                </tr>
<%
      }
      out.println("</table>");
    } %>
            
            <p>
            <center>
            <% if ( tipo.equals("actualizar") ){ %>
                <tsp:boton name='submitFieldsBtn' value='aceptar' onclick="actualizarCamposSubmit()"/>
      <% } else { %>
                <tsp:boton name='submitFieldsBtn' value='aceptar' onclick='camposReportesSubmit()'/>
      <% } %>
            </center>
            </p>
        </form>
    </body>
</html>
