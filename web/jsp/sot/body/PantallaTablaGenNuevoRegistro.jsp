<!--
- Autor : Alejandro Payares
- Date  : Diciembre 24 del 2005
- Copyrigth Notice : Fintravalores S.A. S.A
- Descripcion : Pagina JSP que permite agregar un nuevo registro dentro de una subtabla de tablagen.
-->
<%@page contentType="text/html"%>
<%@page import="com.tsp.operation.model.beans.*, java.util.*"%>
<%@include file="/WEB-INF/InitModel.jsp" %>
<%@ taglib uri="http://jakarta.apache.org/taglibs/input-0.90" prefix="input" %>
<%@ taglib uri="http://www.sanchezpolo.com/sot" prefix="tsp"%>
<%@page import="com.tsp.operation.model.beans.*"%>
<%
    String path = application.getRealPath(model.menuService.getRealUrl(request.getRequestURI()));
    String datos[] = model.menuService.getContenidoMenu(BASEURL,request.getRequestURI(),path);
    String type =request.getParameter("type") ;
%>
<html>
    <head> 
        <title>.: ADMINISTRACIÓN DE TABLAGEN :.</title>
        <script language="javascript" src="<%=BASEURL%>/jsp/sot/js/PantallaTablaGenNuevoRegistro.js"></script>
        <script language="javascript" src="<%=BASEURL%>/js/tools.js"></script>
        <script language="javascript" src="<%=BASEURL%>/js/boton.js"></script>
        <link href="<%=BASEURL%>/css/estilostsp.css" rel="stylesheet" type="text/css">
    </head>
    <body onresize="redimensionar()" onload='redimensionar()'>
        <div id="capaSuperior" style="position:absolute; width:100%; z-index:0; left: 0px; top: 0px;">
            <jsp:include page="/toptsp.jsp?encabezado=ADMINISTRACIÓN DE TABLAGEN"/>
        </div>
        <div id="capaCentral" style="position:absolute; width:100%; height:87%; z-index:0; left: 0px; top: 100px; overflow: scroll;"> 

            <%-- <jsp:useBean id="beanInstanceName" scope="session" class="beanPackage.BeanClassName" /> --%>
            <%-- <jsp:getProperty name="beanInstanceName"  property="propertyName" /> --%>
            <br>
            <%if(request.getParameter("mensaje") != null){%>
            <table border="2" align="center">
                <tr>
                    <td><table width="100%"  border="1" align="center"  bordercolor="#F7F5F4" bgcolor="#FFFFFF">
                        <tr>
                            <td width="229" align="center" class="mensajes"><%=request.getParameter("mensaje")%></td>
                            <td width="29" background="<%=BASEURL%>/images/cuadronaranja.JPG">&nbsp;</td>
                            <td width="58">&nbsp;</td>
                        </tr>
                    </table></td>
                </tr>
            </table>
            <br>
            <%}
            String cmd = request.getParameter("titulo") != null && request.getParameter("titulo").toLowerCase().startsWith("editar")?"modificar":"agregar";
            String action = CONTROLLER+"?estado=TablaGen&accion=Manager&cmd="+cmd; 
			String info="";%>
            <input:form bean="bean" name='forma' method="post"  action="<%=action%>">
                <input:hidden name="oid"/>
                <table align='center' border = '1'>
                <tr>
                    <td width='500px'>
                    <table width='100%'>
                    <tr align='center'>
                        <td colspan="4" align="center" class="subtitulo1"><%=request.getParameter("titulo")%><br></td>
                        <td colspan="1" width="45%" align="left" class="barratitulo"><img src="<%=BASEURL%>/images/titulo.gif" width="32" height="20" align="left"><%=datos[0]%></td>
                    </tr>
                    <tr class="fila">
                    <td width='30%'>Tipo</td>
                    <td colspan="4"><input:text name="table_type" default="<%=type%>" attributesText="style='width:100%' readOnly"/></td>
                </tr>
                    <tr class="fila">
                        <td>C&oacute;digo</td>
                        <td colspan="4">
                            <table border='0' width='100%'>
                                <tr>
                                    <td width='99%'>
                                        <input:text name="table_code" attributesText="style='width:100%' maxlength='30'"/>
                                    </td>
                                    <td>
                                        <img src='<%=BASEURL%>/images/botones/iconos/obligatorio.gif'>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr class="fila">
                    <td>Referencia</td>
                    <td colspan="4"><input:text name="referencia" attributesText="style='width:100%' maxlength='50'"/></td>
                    </tr>
                    <tr class="fila">
                    <td>Descripci&oacute;n</td>
                    <td colspan="4"><input:text name="descripcion" attributesText="style='width:100%'"/></td>
                    </tr>
					
					 <% 
					   TablaGen tg = (TablaGen) request.getAttribute("bean");
					  Vector leyenda = model.tblgendatosvc.buscarLeyendaTable_type(request.getParameter("type"));
					  %>
					  <input name="size" type="hidden" id="size" value="<%=leyenda.size()%>">
					  <%if (leyenda.size() > 0){
 					      int ini = 0,fin=0;
					 	  for (int i = 0; i < leyenda.size(); i++){
						       TblGeneralDato tblgen = (TblGeneralDato) leyenda.elementAt(i);
						       if( (cmd.equals("modificar")) && (tg.getDato().length()>0) ){
							    if(i==0){  
                                                                    fin = fin + Integer.parseInt(tblgen.getLongitud());
                                                                    info = tg.getDato().substring(ini,fin);
                                                                    ini=fin;
                                                              }else{
                                                                    fin = fin + Integer.parseInt(tblgen.getLongitud());
                                                                    info = tg.getDato().substring(ini,fin);
                                                                    ini=fin; 
                                                              }
							   }
 				              %>
 					          <tr class="fila">
                                <td><%=tblgen.getLeyenda()%>
                                                                    
                                  </td>
                                 <td colspan="4"><input name="ley<%=i%>" type="text" id="ley<%=i%>" size="<%=tblgen.getLongitud()%>" maxlength="<%=tblgen.getLongitud()%>" value="<%=info.trim()%>">
                                   <input name="lon<%=i%>" type="hidden" id="lon<%=i%>" value="<%=tblgen.getLongitud()%>">                                   
                                   </td>
                              </tr>
					    <%}
					 }%>
                    </table>
                </td>
                </tr>
                </table>
             </input:form>
            <p>
            <center>
                <table>
                <tr>
                    <td>
                      <%  String op=cmd.equals("agregar")?"aceptar":cmd;%>
                        <tsp:boton value="<%=op%>" onclick="enviarFormNuevoRegistro()"/>
                    </td>
                    <td width='5'>&nbsp;</td>
                    <td>
                        <% String onclick = "location.href='"+CONTROLLER+"?estado=TablaGen&accion=Manager&cmd=mng&type="+request.getParameter("type")+"'"; %>
                        <tsp:boton value="detalles" onclick="<%=onclick%>"/>
                    </td>
                    <td width='5'>&nbsp;</td>
                    <td>
                        <tsp:boton value="cancelar" onclick="document.forma.reset()"/>
                    </td>
                    <td width='5'>&nbsp;</td>
                    <td>
                        <tsp:boton value="salir" onclick="window.close()"/>
                    </td>
                </tr>
                </table>
            </center>
            </p>
        </div>
        <% String str = datos[1];
           str = str.replaceAll("PantallaTablaGenNuevoRegistroHelpD.jsp","PantallaTablagenListHelpD.jsp#"+cmd+"Registro");
           str = str.replaceAll("PantallaTablaGenNuevoRegistroHelpF.jsp","PantallaTablagenListHelpF.jsp#"+cmd+"Registro");
           out.println(str);
        %>
    </body>
</html>
