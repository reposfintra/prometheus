<html>
<head>
<title>.: Descripción del programa de mantanimiento de tablagen :.</title>
<link href="../../../css/estilostsp.css" rel="stylesheet" type="text/css"> 
<style type="text/css">
<!--
.Estilo1 {font-size: 14px}
-->
</style>
</head>

<body>
<a name="adminTablas"><!-- --></a>
<br>
<table width="696"  border="2" align="center">
  <tr>
    <td width="811" >
      <table width="100%" border="0" align="center">
        <tr  class="subtitulo">
          <td height="24" colspan="2"><div align="center">MANTENIMIENTO DE TABLAGEN</div></td>
        </tr>
        <tr class="subtitulo1">
          <td colspan="2"> ADMINISTRACI&Oacute;N DE TABLAS </td>
        </tr>
        <tr>
          <td width="149" class="fila"> Descripci&oacute;n General </td>
          <td width="525"  class="ayudaHtmlTexto">Listado que muestra las diferentes tablas para su edici&oacute;n o eliminaci&oacute;n. </td>
        </tr>
        <tr>
          <td class="fila">V&iacute;nculo editar </td>
          <td  class="ayudaHtmlTexto">Permite editar el nombre y/o la descripci&oacute;n de la tabla </td>
        </tr>
        
      </table>
    </td>
  </tr>
</table>
<%  StringBuffer str = new StringBuffer();
    for( int i=0; i<27; i++){
        str.append("\t<p>&nbsp;</p>\n");
    }
    out.println(str);
%>
<a name="agregarTabla"><!-- --></a>
<p>&nbsp;</p>
<table width="696"  border="2" align="center">
  <tr>
    <td width="811" >
      <table width="100%" border="0" align="center">
        <tr  class="subtitulo">
          <td height="24" colspan="2"><div align="center">MANTENIMIENTO DE TABLAGEN</div></td>
        </tr>
        <tr class="subtitulo1">
          <td colspan="2">NUEVA TABLA </td>
        </tr>
        <tr>
          <td width="149" class="fila"> <strong class="Estilo6">Nombre</strong></td>
          <td width="525"  class="ayudaHtmlTexto">Campo mandatorio para digitar el nombre de la nueva tabla. Este nombre no puede ser mayor a 10 caracteres.</td>
        </tr>
		<tr>
          <td width="149" class="fila"> <strong>Descripcion</strong></td>
          <td width="525"  class="ayudaHtmlTexto">Campo de texto mandatorio donde se digita la descripci&oacute;n de la nueva tabla. </td>
        </tr>
      </table>
    </td>
  </tr>
</table>
<%= str %>
<a name="modificarTabla"><!-- --></a>
<p>&nbsp;</p>
<table width="696"  border="2" align="center">
  <tr>
    <td width="811" >
      <table width="100%" border="0" align="center">
        <tr  class="subtitulo">
          <td height="24" colspan="2"><div align="center">MANTENIMIENTO DE TABLAGEN</div></td>
        </tr>
        <tr class="subtitulo1">
          <td colspan="2">EDITAR TABLA </td>
        </tr>
        <tr>
          <td width="149" class="fila"> <strong class="Estilo6">Nombre</strong></td>
          <td width="525"  class="ayudaHtmlTexto">Campo mandatorio para digitar el nombre de la  tabla. Este nombre no puede ser mayor a 10 caracteres.</td>
        </tr>
        <tr>
          <td width="149" class="fila"> <strong>Descripcion</strong></td>
          <td width="525"  class="ayudaHtmlTexto">Campo de texto mandatorio donde se digita la descripci&oacute;n de la  tabla. </td>
        </tr>
    </table></td>
  </tr>
</table>
<%= str %>
<a name="adminRegistros"><!-- --></a>
<p>&nbsp;</p>
<table width="696"  border="2" align="center">
  <tr>
    <td width="811" >
      <table width="100%" border="0" align="center">
        <tr  class="subtitulo">
          <td height="24" colspan="2"><div align="center">MANTENIMIENTO DE TABLAGEN</div></td>
        </tr>
        <tr class="subtitulo1">
          <td colspan="2">ADMINISTRACI&Oacute;N DE REGISTROS DE UNA TABLA </td>
        </tr>
        <tr>
          <td width="149" class="fila"> Descripci&oacute;n General </td>
          <td width="525"  class="ayudaHtmlTexto">Listado que muestra los registros de una tabla para su edici&oacute;n o eliminaci&oacute;n. </td>
        </tr>
        <tr>
          <td width="149" class="fila"> V&iacute;nculo editar </td>
          <td width="525"  class="ayudaHtmlTexto">Permite editar el codigo, referencia y descripci&oacute;n del registro seleccionado. </td>
        </tr>
    </table></td>
  </tr>
</table>
<%= str %>
<a name="agregarRegistro"><!-- --></a>
<p>&nbsp;</p>
<table width="696"  border="2" align="center">
  <tr>
    <td width="811" >
      <table width="100%" border="0" align="center">
        <tr  class="subtitulo">
          <td height="24" colspan="2"><div align="center">MANTENIMIENTO DE TABLAGEN</div></td>
        </tr>
        <tr class="subtitulo1">
          <td colspan="2">AGREGAR REGISTRO</td>
        </tr>
        <tr>
          <td class="fila">Tipo</td>
          <td  class="ayudaHtmlTexto">Campo que muestra el tipo del registro, es decir la tabla a la que pertenece. Este campo  es de solo lectura. </td>
        </tr>
        <tr>
          <td width="149" class="fila"> C&oacute;digo </td>
          <td width="525"  class="ayudaHtmlTexto">Campo mandatorio para digitar el c&oacute;digo  del nuevo registro. Este c&oacute;digo no puede ser mayor a 30 caracteres.</td>
        </tr>
        <tr>
          <td class="fila">Referencia</td>
          <td  class="ayudaHtmlTexto">Campo de texto para digitar la referencia del nuevo registro. Longitud maxima 50 caracteres. </td>
        </tr>
        <tr>
          <td width="149" class="fila"> <strong>Descripcion</strong></td>
          <td width="525"  class="ayudaHtmlTexto">Campo de texto mandatorio donde se digita la descripci&oacute;n del nuevo registro. </td>
        </tr>
    </table></td>
  </tr>
</table>
<%= str %>
<a name="modificarRegistro"><!-- --></a>
<p>&nbsp;</p>
<table width="696"  border="2" align="center">
  <tr>
    <td width="811" >
      <table width="100%" border="0" align="center">
        <tr  class="subtitulo">
          <td height="24" colspan="2"><div align="center">MANTENIMIENTO DE TABLAGEN</div></td>
        </tr>
        <tr class="subtitulo1">
          <td colspan="2">EDITAR REGISTRO</td>
        </tr>
        <tr>
          <td class="fila">Tipo</td>
          <td  class="ayudaHtmlTexto">Campo que muestra el tipo del registro, es decir la tabla a la que pertenece. Este campo es de solo lectura. </td>
        </tr>
        <tr>
          <td width="149" class="fila"> C&oacute;digo </td>
          <td width="525"  class="ayudaHtmlTexto">Campo mandatorio para digitar el c&oacute;digo del registro. Este c&oacute;digo no puede ser mayor a 30 caracteres.</td>
        </tr>
        <tr>
          <td class="fila">Referencia</td>
          <td  class="ayudaHtmlTexto">Campo de texto para digitar la referencia del registro. Longitud maxima 50 caracteres. </td>
        </tr>
        <tr>
          <td width="149" class="fila"> <strong>Descripcion</strong></td>
          <td width="525"  class="ayudaHtmlTexto">Campo de texto mandatorio donde se digita la descripci&oacute;n del registro. </td>
        </tr>
    </table></td>
  </tr>
</table>
<%= str %>
<p align="center" class="fuenteAyuda"> Fintravalores S. A. </p>
<p>&nbsp;</p>
<p>&nbsp;</p>

</body>
</html>