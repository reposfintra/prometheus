<!--  
	 - Author(s)       :      FFERNADEZ	
	 - Description     :      AYUDA DESCRIPTIVA - Adicionar Cliente
	 - Date            :      28/10/2006  
	 - Copyright Notice:      TRANSPORTES SANCHEZ POLO S.A.
-->
<%@page session="true"%> 
<%@page errorPage="/error/ErrorPage.jsp"%>
<%@page import="com.tsp.operation.model.beans.*, java.util.*"%>
<%@ include file="/WEB-INF/InitModel.jsp"%>
<%@ taglib uri="http://jakarta.apache.org/taglibs/input-0.90" prefix="input" %>
<html>
<head>
    <title>Descripcion de los campos para Modificar los Datos del Cliente</title>
    <link href="<%= BASEURL %>/css/estilostsp.css" rel="stylesheet" type="text/css"> 
    <link href="/../../../css/estilostsp.css" rel="stylesheet" type="text/css">  
    
</head>
<script type='text/javascript' src="<%= BASEURL %>/js/boton.js"></script> 
<body> 
<div id="capaSuperior" style="position:absolute; width:100%; height:100px; z-index:0; left: 0px; top: 0px;">
<jsp:include page="/WEB-INF/toptsp.jsp?encabezado=Ayuda Descriptiva - Modificar Cliente"/>
</div>
<div id="capaCentral" style="position:absolute; width:100%; height:90%; z-index:0; left: 0px; top: 100px;"> 
   <p>&nbsp;</p>
   <table width="65%" border="2" align="center" id="tabla1" >
        <td>
         <table width="100%" height="50%" align="center">
              <tr>
                <td class="subtitulo1"><strong>Reporte</strong></td>
                <td width="50%" class="barratitulo"><img src="<%=BASEURL%>/images/titulo.gif" width="32" height="20"></td>
              </tr>
         </table>
         <table width="100%" borderColor="#999999" bgcolor="#F7F5F4">
           <tr>
             <td colspan="2" class="tblTitulo"><strong>Modificar Cliente- Pantalla Preliminar</strong></td>
           </tr>
           <tr>
             <td colspan="2" class="subtitulo"><strong>INFORMACION</strong></td>
           </tr>
		   <tr>
             <td class="fila"> De la caja buscar Codigo del Cliente </td>
             <td>En este campo se le asigna un codigo al cliente para su identificacion para su busqueda de la informacion</td>
           </tr>
           <tr>
             <td width="41%" class="fila">Estado</td>
             <td width="59%"><span class="ayudaHtmlTexto">Un campo cerrado donde solo se puede ingresar el campo si esta Activo o Inactivo</span></td>
           </tr>
           <tr>
             <td class="fila">Cedula del Cliente </td>
             <td>Campo donde se le ingresa la cedula del cliente y no se le puede asignar caracteres sino solamente numericos</td>
           </tr>
           <tr>
             <td class="fila">Nombre del Cliente </td>
             <td>Campo donde usted puede digitar el nombre del Cliente ya sea en mayusculas o en minusculas </td>
           </tr>
          
          
           <tr>
             <td class="fila">Agencia de Duenia del Cliente </td>
             <td><span class="ayudaHtmlTexto">Campo cerrado donde usted puede escojer la agencia que le sale en el listado </span></td>
           </tr>
           <tr>
             <td class="fila">Moneda de Facturacion del Cliente </td>
             <td>campo donde asigna el tipo de moneda que factura con el cliente </td>
           </tr>
		    <tr>
             <td class="fila">Codigo del Cliente </td>
             <td>En este campo se le asigna un codigo al cliente para su identificacion </td>
           </tr>
		    <tr>
             <td class="fila">Fiduciaria</td>
             <td>Campo que solo se le puede ingresar un solo caracter.</td>
           </tr>
		   <tr>
             <td class="fila">Base</td>
             <td>Campo donde solo se le puede ingresar tres caracteres.</td>
           </tr>
           <tr>
             <td class="fila">Rentabilidad</td>
             <td>Campo donde usted puede ingresar el dato de rentabilidad ya sea en texto o en numero </td>
           </tr>
		    <tr>
             <td class="fila">Districto del Cliente </td>
             <td>Este es un campo Cerrado donde usted puede asignar que tipo de Districto es o TSP o VEN </td>
           </tr>
		    <tr>
             <td class="fila">Cedula del Agente </td>
             <td>Campo donde se le ingresa la cedula del Agente y no se le puede asignar caracteres sino solamente numericos</td>
           </tr>
		   
		   <tr>
             <td class="fila">Forma de Pago </td>
             <td>Campo donde se digita que tipo de pago se hara con el cliente </td>
           </tr>
		    <tr>
             <td class="fila"> Plazo</td>
             <td>campo donde asigna el plazo que tiene el cliente para pagar en Nro de dias </td>
           </tr>
		    <tr>
             <td class="fila">Zona</td>
             <td>Campo donde digita la zona </td>
           </tr>
		   <tr>
             <td class="fila">Banco de Consignacion</td>
             <td>En este campo usted digita el banco donde se har&aacute; la consignacion </td>
           </tr>
           <tr>
             <td class="fila">Codigo del Manejo Contable </td>
             <td>El campo Manejo Contable es el codigo del manejo que se le asignara al cliente </td>
           </tr>
		    <tr>
             <td class="fila"> Unidad Contable Asociada </td>
             <td> En este campo se le ingresa la unidad contable Asociada como campo obligatorio </td>
           </tr>
		    <tr>
             <td class="fila">Sucursal del Banco </td>
             <td>Aqui se escribe  donde esta la sucursal del banco actualemente</td>
           </tr>
		    <tr>
             <td class="fila"> Codigo del Impuesto Aplicara al Cliente</td>
             <td>En este campo se le asignara el codigo del impuesto que sera identificado para el cliente </td>
           </tr>
		    <tr>
             <td class="fila">Agencia de Facturacion </td>
             <td><span class="ayudaHtmlTexto">Campo cerrado donde usted puede escojer la agencia que le sale en el listado </span></td>
           </tr>
		   <tr>
             <td class="fila">Notas</td>
             <td>En este campo usted puede escribir anexos ya sea de interes para mas especificacion del cliente </td>
           </tr>
           <tr>
             <td class="fila">Soporte de Facturacion</td>
             <td>El campo Soporte de facturacion donde usted puede ingresar la remesa. </td>
           </tr>
		    <tr>
             <td class="fila"> Texto_oc</td>
             <td> En este campo se le ingresa el texto_oc de una breve descripcion </td>
           </tr>
		   
         </table></td>
  </table>
		<p></p>
	<center>
	<img src = "<%=BASEURL%>/images/botones/salir.gif" style = "cursor:hand" name = "imgsalir" onClick = "parent.close();" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);">
	</center>
</div>  
</body>
</html>