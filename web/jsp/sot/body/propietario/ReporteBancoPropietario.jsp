  <!--
     - Author(s)       :      FILY STEVEN FERNANDEZ
     - Date            :      16/01/2007  
     - Copyright Notice:      TRANSPORTES SANCHEZ POLO S.A.
  -->
 <%--   - @(#)  
     - Description:  Vista  que muestra los  movimientos de cuentas auxiliar.
 --%>
<%@page session="true"%>
<%@page errorPage="../error/ErrorPage.jsp"%>
<%@page import="com.tsp.operation.model.beans.*, java.util.*, java.text.*"%>
<%@ include file="/WEB-INF/InitModel.jsp"%>
<%@ taglib uri="http://jakarta.apache.org/taglibs/input-0.90" prefix="input" %>
<%@page import="com.tsp.util.*"%>
<%@taglib uri="http://jsptags.com/tags/navigation/pager" prefix="pg" %>
<% Util u = new Util(); %>

<html>
<head>
        <title>Reporte Banco Propietario</title>
        <link href="<%= BASEURL %>/css/estilostsp.css" rel="stylesheet" type="text/css"> 
        <script src='<%=BASEURL%>/js/date-picker.js'></script>
		<script language="JavaScript" type="text/javascript" src="<%=BASEURL%>/js/boton.js"></script>
		<script type='text/javascript' src="<%=BASEURL%>/js/validar.js"></script> 
		<link href="../../estilostsp.css" rel="stylesheet" type="text/css">
		<link href="<%=BASEURL%>/css/estilostsp.css" rel="stylesheet" type="text/css">
		 <script>
		 	function Exportar (){
				
			    alert( 
				
					'Puede observar el seguimiento del proceso en la siguiente ruta del menu:' + '\n' + '\n' +
					'SLT -> PROCESOS -> Seguimiento de Procesos' + '\n' + '\n' + '\n' +
					'Y puede observar el archivo excel generado, en la siguiente parte:' + '\n' + '\n' +
					'En la pantalla de bienvenida de la Pagina WEB,' + '\n' +
					'vaya a la parte superior-derecha,' + '\n' +
					'y presione un click en el icono que le aparece.'
					
				);
				
			}
		</script>
        
        
</head>
<body>



<%  String path    = application.getRealPath(model.menuService.getRealUrl(request.getRequestURI()));
    String datos[] = model.menuService.getContenidoMenu(BASEURL,request.getRequestURI(),path);   
	
	
	String propietario= (request.getParameter("Propietario")!= null)?request.getParameter("Propietario"):"" ;
    String hc=(request.getParameter("HC")!= null)?request.getParameter("HC"):"" ;
	String banco= (request.getParameter("Banco")!= null)?request.getParameter("Banco"):"" ; %>
    
     
<div id="capaSuperior" style="position:absolute; width:100%; height:100px; z-index:0; left: 0px; top: 0px;">
<jsp:include page="/WEB-INF/toptsp.jsp?encabezado=Banco Propietario"/>
</div>

<div id="capaCentral" style="position:absolute; width:100%; height:85%; z-index:0; left: 2px; top: 100px; overflow: scroll;"> 
 <center>
 <%
	   String style = "simple";
	   String position =  "bottom";
       String index =  "center";
       int maxPageItems = 20;
       int maxIndexPages = 10;
	   String msg = request.getParameter("msg"); %>
 
 
 <%Vector vectores =  model.banco_PropietarioService.getVector();
    String msj = (request.getParameter("msg")==null)?"":request.getParameter("msg");
    if( vectores != null && vectores.size() > 0 ){
      Banco_Propietario BancoPo  = (Banco_Propietario) vectores.elementAt(0);
	%>		

   
    <form aling= "left" name="form1" method="post" action="<%=CONTROLLER%>?estado=BancoPro&accion=Buscar&opcion=2">
		    <input name="Guardar2" src="<%=BASEURL%>/images/botones/exportarExcel.gif" type="image" class="boton" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" style="cursor:hand" onclick="Exportar();">
			<img src="<%=BASEURL%>/images/botones/regresar.gif" name="c_regresar" onclick="location.href='<%=BASEURL%>/jsp/sot/body/propietario/BuscarBanco_Propietario.jsp'" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" style="cursor:hand">
	  		<input name="salir2" src="<%=BASEURL%>/images/botones/salir.gif" type="image" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" style="cursor:hand" class="boton" onclick="window.close();">
		    
			<input name="Propietario" type="hidden" id="Propietario"  value="<%=propietario%>" readonly >
			<input name="HC" type="hidden" id="HC" value="<%=hc%>" readonly >
			<input name="Banco" type="hidden" id="Banco" value="<%=banco%>" readonly >
			
      <table width="98%" border="2" align="center">
       <tr>
          <td>        
			
               
                <table width='100%' align='center' class='tablaInferior'>
                
                      <tr>
                            <td class="barratitulo" colspan='2' >
                                   <table cellpadding='0' cellspacing='0' width='100%'>
                                         <tr class="fila">
                                             <td width="50%" align="left" class="subtitulo1">&nbsp;<strong>Reporte Banco Propietario</strong></td>
                                             <td width="*"   align="left"><img src="<%= BASEURL %>/images/titulo.gif" width="32" height="20"  align="left"><%=datos[0]%></td>
                                        </tr>
                                   </table>   
                            </td>
                      </tr>
                      
                      
                      <tr   class="letra">
                         <td colspan='2'>
                                                    
                         </td>
              </table>
         </td>
        </tr>
          <tr>
                            <td class="barratitulo" colspan='2' >
                                   <table cellpadding='0' cellspacing='0' width='100%'>
                                         <tr class="fila">
                                             <td width="50%" align="left" class="subtitulo1"><strong>LISTADO DE BANCO PROPIETARIOS</strong></td>
                                             <td width="*"   align="left"><img src="<%= BASEURL %>/images/titulo.gif" width="32" height="20"  align="left"></td>
                                        </tr>
                                   </table>   
                            </td>
        </tr>
                      
                      
                      
                      
                      <tr class="fila">
                              <td width='100%' colspan='2'>
                                      <table width='100%' border="1" bordercolor="#999999" bgcolor="#F7F5F4" align="center">
                                      
                                           
                                            <tr class="letra">
                                                  <td colspan='10' align='right'>&nbsp;</td>
                                        </tr>  
                                      
                                      
                                            <tr class="tblTitulo" >
                                                  <TH  width='7%'  title='click para modificar'        nowrap style="font size:11; font weight: bold" >MODIFICAR ITEM</TH>
												  <TH  width='9%'  title=''      nowrap style="font size:11; font weight: bold" >DISTRITO</TH>
                                                  <TH  width='9%'  title=''      nowrap style="font size:11; font weight: bold" >PROPIETARIO </TH>
                                                  <TH  width='10%' title=''          nowrap style="font size:11; font weight: bold" >HC</TH>
                                                  <TH  width='8%'  title=''       nowrap style="font size:11; font weight: bold" >BANCO </TH>
												  <TH  width='8%'  title=''       nowrap style="font size:11; font weight: bold" >SUCURSAL</TH>                    
                                                  <TH  width='15%' title='' nowrap style="font size:11; font weight: bold" >USUARIO DE CREACION </TH>
                                                  <TH  width='5%'  title=''         nowrap style="font size:11; font weight: bold" >BASE</TH>               
                                                 
												         <!-- Movimientos--> 
                                          
																				  
										</tr>	
													<pg:pager
													 items="<%=vectores.size()%>"
													 index="<%= index %>"
													 maxPageItems="<%= maxPageItems %>"
													 maxIndexPages="<%= maxIndexPages %>"
													 isOffset="<%= true %>"
													 export="offset,currentPageNumber=pageNumber"
													 scope="request">					
											<%
												int cont = 1;
												for (int i = offset.intValue(), l = Math.min(i + maxPageItems, vectores.size()); i < l; i++){
													Banco_Propietario BancoPRO= (Banco_Propietario) vectores.elementAt(i);
										  %>  	  
													
													<pg:item>
													  <tr class="<%=i%2==0?"filagris":"filaazul"%>" onMouseOver='cambiarColorMouse(this)' style="cursor:hand" title="Click" align="center" > 
													  <td nowrap align="center" class="bordereporte" title="click para modificar" onClick="window.open('<%=CONTROLLER%>?estado=BancoPro&accion=Modificar&opcion=1&Distrito=<%=BancoPRO.getDistrito()%>&Propietario=<%=BancoPRO.getPropietario()%>&HC=<%=BancoPRO.getHc()%>' ,'M','status=yes,scrollbars=no,width=780,height=650,resizable=yes');"> <%=cont%>&nbsp;</td>													  
													  <td nowrap align="left" class="bordereporte"><%=(BancoPRO.getDistrito()!=null)?BancoPRO.getDistrito():""%>&nbsp;</td> 
													  <td nowrap align="left" class="bordereporte"><%=(BancoPRO.getPropietario()!=null)?BancoPRO.getPropietario():""%>&nbsp;</td>
													  <td nowrap align="left" class="bordereporte"><%=(BancoPRO.getDescripcionHc()!=null)?BancoPRO.getDescripcionHc():""%>&nbsp;</td>
													  <td nowrap align="left" class="bordereporte"><%=(BancoPRO.getBanco()!=null)?BancoPRO.getBanco():""%>&nbsp;</td>
													  <td nowrap align="right" class="bordereporte"><%=(BancoPRO.getSucursal()!=null)?BancoPRO.getSucursal():""%>&nbsp;</td>
													  <td nowrap align="left" class="bordereporte"><%=(BancoPRO.getCreation_user()!=null)?BancoPRO.getCreation_user():""%>&nbsp;</td>
													  <td nowrap align="left" class="bordereporte"><%=(BancoPRO.getBase()!=null)?BancoPRO.getBase():""%>&nbsp;</td>
													
													    
													  
														
													</tr>
												  </pg:item>	
											<%cont = cont + 1; 
											}%>
											<tr class="bordereporte">
											  <td td height="20" colspan="10" nowrap align="center">
											   <pg:index>
												<jsp:include page="/WEB-INF/jsp/googlesinimg.jsp" flush="true"/>      
											   </pg:index> 
											  </td>
											</tr>
											</pg:pager>
								</table>
								  
                        </td>
                     </tr>
  </table>               
            
          </td>
       </tr>
     </table>  
     
		    <input name="Guardar2" src="<%=BASEURL%>/images/botones/exportarExcel.gif" type="image" class="boton" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" style="cursor:hand" onclick="Exportar();"> 
		    <img src="<%=BASEURL%>/images/botones/regresar.gif" name="c_regresar" onclick="location.href='<%=BASEURL%>/jsp/sot/body/propietario/BuscarBanco_Propietario.jsp'" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" style="cursor:hand">
			<input name="salir2" src="<%=BASEURL%>/images/botones/salir.gif" type="image" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" style="cursor:hand" class="boton" onclick="window.close();">
  </form>	
	<%}%>   
 <br>
    <% if( ! msj.equals("") ){%>
          <table border="2" align="center">
                  <tr>
                    <td>
                        <table width="100%"  border="1" align="center"  bordercolor="#F7F5F4" bgcolor="#FFFFFF">
                              <tr>
                                    <td width="400" align="center" class="mensajes"><%=msj%></td>
                                    <td width="29" background="<%=BASEURL%>/images/cuadronaranja.JPG">&nbsp;</td>
                                    <td width="58">&nbsp; </td>
                              </tr>
                      </table>
                    </td>
                  </tr>
          </table>
          <p align="center"><img src="<%=BASEURL%>/images/botones/regresar.gif" name="c_regresar" onclick="location.href='<%=BASEURL%>/jsp/sot/body/propietario/BuscarBanco_Propietario.jsp'" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" style="cursor:hand">
		  <img src="<%=BASEURL%>/images/botones/salir.gif" name="c_salir" onClick="parent.close();" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);"></p>
          <!--botones -->   
		
      <%}%>
</div>
<%=datos[1]%>  
</body>
</html>
