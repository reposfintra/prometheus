<!--
- Autor : Ing. Tito Andr�s Maturana
- Date  : 04 de enero de 2006
- Copyrigth Notice : Fintravalores S.A. S.A
-->
<%--
-@(#)
--Descripcion : Pagina JSP, que maneja la generaci�n del reporte
				de stickers utilizados.
--%>
<%@page contentType="text/html"%>
<%@page session="true"%>
<%@page import="java.util.*"%>
<%@page import="com.tsp.operation.model.*"%>
<%@page import="com.tsp.operation.model.beans.*"%>
<%@page import="com.tsp.operation.controller.*"%>
<%@page import="com.tsp.util.*"%>
<%@include file="/WEB-INF/InitModel.jsp"%>
<%@ taglib uri="http://jakarta.apache.org/taglibs/input-0.90" prefix="input" %>
<%String path = application.getRealPath(model.menuService.getRealUrl(request.getRequestURI()));
  String datos[] = model.menuService.getContenidoMenu(BASEURL,request.getRequestURI(),path);
  %>
  
 <%
  String propietario =(request.getParameter ("Propietario")!=null)?request.getParameter ("Propietario"):"";  
  String hc =(request.getParameter ("HC")!=null)?request.getParameter ("HC"):""; 
  String banco     = Util.coalesce( ((String) request.getParameter("Banco")), "" );
  String sucursal  = Util.coalesce( ((String) request.getParameter("Sucursal")), "" );
  TreeMap BancoOrigen = model.servicioBanco.getBanco();
  TreeMap SucBanco = model.servicioBanco.getSucursal();
  BancoOrigen.put( "Seleccione", "");
  SucBanco.put( "Seleccione", "");
%>
<html>
<head>
<script>
function validaFechas(){
  	var fecha1 = document.forma.FechaI.value.replace('-','').replace('-','');
   	var fecha2 = document.forma.FechaF.value.replace('-','').replace('-','');
	var fech1 = parseFloat(fecha1);
	var fech2 = parseFloat(fecha2);
	if(fech1>fech2) {     
   		alert('La fecha final debe ser mayor que la fecha inicial');
		return (false);
    }
	return true;
}
function validarTCamposLlenos(){
   return true;
}
</script>
<title>Buscar Banco Propietario</title>
<link href="<%= BASEURL %>/css/estilostsp.css" rel='stylesheet'>
<link href="../../../../css/estilostsp.css" rel="stylesheet" type="text/css">
<link href="/css/estilostsp.css" rel="stylesheet" type="text/css">
</head>
<script type='text/javascript' src="<%= BASEURL %>/js/validar.js"></script>
<script src='<%= BASEURL %>/js/date-picker.js'></script>
<script type='text/javascript' src="<%= BASEURL %>/js/boton.js"></script>
<body onresize="redimensionar()" onload = 'redimensionar()'>
<div id="capaSuperior" style="position:absolute; width:100%; height:100px; z-index:0; left: 0px; top: 0px;">
 <jsp:include page="/toptsp.jsp?encabezado=Buscar Banco Propietario"/>
</div>
<div id="capaCentral" style="position:absolute; width:100%; height:87%; z-index:0; left: 0px; top: 100px; overflow: scroll;">
<form name="formulario" action='<%=CONTROLLER%>?estado=BancoPro&accion=Buscar&opcion=1' id="forma"  method="post">
  <table width="300"  border="2" align="center" cellpadding="0" cellspacing="0">
    <tr>
      <td><table width="100%" class="tablaInferior">
        <tr>
          <td colspan="2" ><table width="100%"  border="0" cellpadding="0" cellspacing="1" class="barratitulo">
              <tr>
                <td width="68%" class="subtitulo1">Buscar Banco Propietario</td>
                <td width="32%" class="barratitulo"><img align="left" src="<%=BASEURL%>/images/titulo.gif" width="32" height="20"><%=datos[0]%></td>
              </tr>
          </table></td>
        </tr>
        <tr class="fila">
          <td>Propietario</td>
          <td nowrap>
            <input name="Propietario" type='text' class="textbox" id="Propietario" style='width:120' value="<%=propietario%>">         
        </tr>
		<tr class="fila">
        <td width="40" >HC <% TreeMap hcs = model.tablaGenService.getThcs(); %></td> 
			<td width="214"><input:select  name="HC" attributesText="style='width:85%;' class='listmenu'" options="<%=hcs%>" default="<%=hc%>"/></td>	
			
        </tr>
		 <tr class="fila">
		  <td >Banco </td>
		  <td height="20"><input:select name="Banco" options="<%=BancoOrigen%>" default="<%= banco %>"/></td>
        </tr>
		
		
      </table></td>
    </tr>
  </table>
  <br>
  <center>
    <img src="<%=BASEURL%>/images/botones/buscar.gif" name="c_aceptar" onClick="forma.submit();" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);"> 
    <img src="<%=BASEURL%>/images/botones/cancelar.gif" name="c_cancelar" onClick="forma.reset();" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);"> 
    <img src="<%=BASEURL%>/images/botones/salir.gif" name="c_salir" onClick="parent.close();" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);">
  </center>
</form>
<% if( request.getParameter("msg") != null ){%>
  <p><table border="2" align="center">
  <tr>
    <td><table width="100%"  border="1" align="center"  bordercolor="#F7F5F4" bgcolor="#FFFFFF">
      <tr>
        <td width="229" align="center" class="mensajes"><%=request.getParameter("msg").toString()%></td>
        <td width="29" background="<%=BASEURL%>/images/cuadronaranja.JPG">&nbsp;</td>
        <td width="58">&nbsp;</td>
      </tr>
    </table></td>
  </tr>
</table>
</p>
 <%} %>
</div>
<iframe width=188 height=166 name="gToday:normal:agenda.js:gfPop:plugins.js" id="gToday:normal:agenda.js:gfPop:plugins_24.js" src="js/Calendario/ipopeng.htm" scrolling="no" frameborder="0" style="visibility:visible; z-index:999; position:absolute; left:-500px; top:0px;"> </iframe>
<%= datos[1]%>
</body>
</html>
<script>

function loadSucursalesOri(){
	formulario.action = '<%= CONTROLLER%>?estado=Cargar&accion=BancosDestino&tipo=ORI&opc=PROPIETARIO';
	formulario.submit();
}
</script>