<!--
- Autor : Ing. Tito Andr�s Maturana
- Date  : 04 de enero de 2006
- Copyrigth Notice : Fintravalores S.A. S.A
-->
<%--
-@(#)
--Descripcion : Pagina JSP, que maneja la generaci�n del reporte
				de stickers utilizados.
--%>
<%@page contentType="text/html"%>
<%@page session="true"%>
<%@page import="java.util.*"%>
<%@page import="com.tsp.operation.model.*"%>
<%@page import="com.tsp.operation.model.beans.*"%>
<%@page import="com.tsp.operation.controller.*"%>
<%@page import="com.tsp.util.*"%>
<%@include file="/WEB-INF/InitModel.jsp"%>
<%@ taglib uri="http://jakarta.apache.org/taglibs/input-0.90" prefix="input" %>
<%String path = application.getRealPath(model.menuService.getRealUrl(request.getRequestURI()));
  String datos[] = model.menuService.getContenidoMenu(BASEURL,request.getRequestURI(),path);
  %>
  
 
<html>
<head>
<script>
function validaFechas(){
  	var fecha1 = document.forma.FechaI.value.replace('-','').replace('-','');
   	var fecha2 = document.forma.FechaF.value.replace('-','').replace('-','');
	var fech1 = parseFloat(fecha1);
	var fech2 = parseFloat(fecha2);
	if(fech1>fech2) {     
   		alert('La fecha final debe ser mayor que la fecha inicial');
		return (false);
    }
	return true;
}
</script>
<title>Modificar Banco Propietario</title>
<link href="<%= BASEURL %>/css/estilostsp.css" rel='stylesheet'>
<link href="../../../../css/estilostsp.css" rel="stylesheet" type="text/css">
<link href="/css/estilostsp.css" rel="stylesheet" type="text/css">
</head>
<script type='text/javascript' src="<%= BASEURL %>/js/validar.js"></script>
<script src='<%= BASEURL %>/js/date-picker.js'></script>
<script type='text/javascript' src="<%= BASEURL %>/js/boton.js"></script>
<body  <%if(request.getParameter("reload")!=null){%>onLoad="parent.opener.location.reload();"<%}%> onResize="redimensionar()" onLoad="redimensionar();" onKeyDown="onKeyDown();">
<div id="capaSuperior" style="position:absolute; width:100%; height:100px; z-index:0; left: 0px; top: 0px;">
 <jsp:include page="/toptsp.jsp?encabezado=Banco Propietario"/>
</div>
<div id="capaCentral" style="position:absolute; width:100%; height:87%; z-index:0; left: 0px; top: 100px; overflow: scroll;">
<% if( request.getParameter("msg") != null ){%>
  <p><table border="2" align="center">
  <tr>
    <td><table width="100%"  border="1" align="center"  bordercolor="#F7F5F4" bgcolor="#FFFFFF">
      <tr>
        <td width="229" align="center" class="mensajes"><%=request.getParameter("msg").toString()%></td>
        <td width="29" background="<%=BASEURL%>/images/cuadronaranja.JPG">&nbsp;</td>
        <td width="58">&nbsp;</td>
      </tr>
    </table></td>
  </tr>
</table>
<p align="center"><img src="<%=BASEURL%>/images/botones/salir.gif" name="c_salir" onClick="parent.close();" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);"></p>
</p>
 <%}else{ %>
 <% Vector datosC = model.banco_PropietarioService.getVector();
   if( datos != null && datosC.size() > 0 ){
     Banco_Propietario BancoPRO= (Banco_Propietario) datosC.elementAt(0); 
	
	 String propietario =(request.getParameter ("Propietario")!=null)?request.getParameter ("Propietario"):BancoPRO.getPropietario();
	  String hc =(request.getParameter ("HC")!=null)?request.getParameter ("HC"):BancoPRO.getHc();
	 String banco =(request.getParameter ("Banco")!=null)?request.getParameter ("Banco"):BancoPRO.getBanco();
	 String sucursal =(request.getParameter ("Sucursal")!=null)?request.getParameter ("Sucursal"):BancoPRO.getSucursal(); 
	 String distrito =(request.getParameter ("Distrito")!=null)?request.getParameter ("Distrito"):BancoPRO.getDistrito(); 
	 TreeMap BancoOrigen = model.servicioBanco.getBanco();
  	 TreeMap SucBanco = model.servicioBanco.getSucursal(); 
  	 BancoOrigen.put( "Seleccione", "");  
  	 SucBanco.put( "Seleccione", "");%>
	        
	 
<form name="formulario" action='<%=CONTROLLER%>?estado=BancoPro&accion=Modificar&opcion=2' id="forma"  method="post" onSubmit="return validar(this);">
  <table width="300"  border="2" align="center" cellpadding="0" cellspacing="0">
    <tr>
      <td><table width="100%" class="tablaInferior">
        <tr>
          <td colspan="2" ><table width="100%"  border="0" cellpadding="0" cellspacing="1" class="barratitulo">
              <tr>
                <td width="74%" class="subtitulo1">Modificar Banco Propietario</td>
                <td width="26%" class="barratitulo"><img align="left" src="<%=BASEURL%>/images/titulo.gif" width="32" height="20"><%=datos[0]%></td>
              </tr>
          </table></td>
        </tr>
        <tr class="fila">
          <td>Propietario</td>
          <td nowrap>
            <input name="CPropietario" type='text' class="textbox" id="CPropietario" style='width:120' value="<%=propietario%>" readonly disabled>
			<input name="Propietario"  class="textbox" id="Propietario"  value="<%=propietario%>" type="hidden">         
			<input name="Distrito"  class="textbox" id="Distrito"  value="<%=distrito%>" type="hidden">         
        </tr>
		<tr class="fila">
        <td width="40" >HC <% TreeMap hcs = model.tablaGenService.getThcs(); %></td> 
			<td width="214"><input:select  name="Combo" attributesText="style='width:85%;' class='listmenu' disabled" options="<%=hcs%>" default="<%= hc %>"/></td>	
			<input name="HC"  class="textbox" id="HC"  value="<%=hc%>" type="hidden">         
        </tr>
		 <tr class="fila">
		  <td >Banco </td>
		  <td height="20"><input:select name="Banco" attributesText="class=textbox onChange=\"Sucursal.value=''; loadSucursalesOri();\"" options="<%=BancoOrigen%>" default="<%= banco %>"/><img  src="<%=BASEURL%>/images/botones/iconos/obligatorio.gif" width="10" height="10"></td>
        </tr>
		 <tr class="fila">
          <td>Sucursal</td>
          <td><input:select name="Sucursal" attributesText="class=textbox" options="<%=SucBanco %>"  default="<%= sucursal %>" /><img  src="<%=BASEURL%>/images/botones/iconos/obligatorio.gif" width="10" height="10"> </td>
        </tr>
		
      </table></td>
    </tr>
  </table>
  <br>
  <center>
    <input  name="Guardar" src="<%=BASEURL%>/images/botones/modificar.gif"  style = "cursor:hand" type="image" id="Guardar"  onMouseOver="botonOver(this);" onMouseOut="botonOut(this);">                  
    <img src="<%=BASEURL%>/images/botones/anular.gif"  name="imgdetalles"   onMouseOver="botonOver(this);" onClick="window.location='<%=CONTROLLER%>?estado=BancoPro&accion=Modificar&opcion=3&HC=<%=hc%>&Distrito=<%=distrito%>&Propietario=<%=propietario%>'" onMouseOut="botonOut(this);" style="cursor:hand"> 
    <img src="<%=BASEURL%>/images/botones/salir.gif" name="c_salir" onClick="parent.close();" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);">
  </center>
</form>
<%}
}%>

</div>
<iframe width=188 height=166 name="gToday:normal:agenda.js:gfPop:plugins.js" id="gToday:normal:agenda.js:gfPop:plugins_24.js" src="js/Calendario/ipopeng.htm" scrolling="no" frameborder="0" style="visibility:visible; z-index:999; position:absolute; left:-500px; top:0px;"> </iframe>
<%= datos[1]%>
</body>
</html>
<script>

function loadSucursalesOri(){
	formulario.action = '<%= CONTROLLER%>?estado=Cargar&accion=BancosDestino&tipo=ORI&opc=PROPIETARIO&tip=2';
	formulario.submit();
}

function validar( forma ){

   		if ( forma.Propietario.value == '' ){
			alert( 'Debe Ingresar El propietario para continuar...' );
			return false;
		}
		if ( forma.HC.value == '' ){
			alert( 'Debe Ingresar el HC para continuar...' );
			return false;
		}
		if ( forma.Banco.value == '' ){
			alert( 'Debe Ingresar el Banco para continuar...' );
			return false;
		}
		if ( forma.Sucursal.value == '' ){
			alert( 'Debe Ingresar la Sucursal del banco para continuar...' );
			return false;
		}
}		
		
</script>