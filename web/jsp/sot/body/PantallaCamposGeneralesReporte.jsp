<%@ page session="true" %>
<%@ page errorPage="/ErrorPage.jsp" %>
<%@ page import="java.util.*" %>
<%@ page import="com.tsp.operation.model.beans.*" %>
<%@ taglib uri="http://www.sanchezpolo.com/sot" prefix="tsp"%>
<%@ include file="/WEB-INF/InitModel.jsp" %>
<%  String tipo = request.getParameter("tipo"); %>
<html>
    <head>
        <title>.: ADMINISTRACIÓN DE CAMPOS DE REPORTES :.</title>
        <script language="javascript" src="<%=BASEURL%>/js/tools.js"></script>
        <script language="javascript" src="<%=BASEURL%>/js/boton.js"></script>
        <script src='<%=BASEURL%>/jsp/sot/js/PantallaCamposGeneralesReporte.js' language='javascript'></script>
        <script src='<%=BASEURL%>/js/PantallaConfigReportes.js' language='javascript'></script>
        <link href="<%=BASEURL%>/css/estilostsp.css" rel="stylesheet" type="text/css">
    </head>
    <body onresize="redimensionar()" onload="redimensionar();llenarCombosOrden(document.reportFieldsFrm.numeroDeCampos.value);">
    <div id="capaSuperior" style="position:absolute; width:100%; z-index:0; left: 0px; top: 0px;">
        <jsp:include page="<%="/toptsp.jsp?encabezado=CAMPOS DE " + request.getParameter("nombreReporte")%>"/>
    </div>
    <div id="capaCentral" style="position:absolute; width:100%; height:87%; z-index:0; left: 0px; top: 100px; overflow: scroll;"> 
        <%if(request.getParameter("mensaje") != null){%>
        <table border="2" align="center">
            <tr>
                <td><table width="100%"  border="1" align="center"  bordercolor="#F7F5F4" bgcolor="#FFFFFF">
                    <tr>
                        <td width="229" align="center" class="mensajes"><%=request.getParameter("mensaje")%></td>
                        <td width="29" background="<%=BASEURL%>/images/cuadronaranja.JPG">&nbsp;</td>
                        <td width="58">&nbsp;</td>
                    </tr>
                </table></td>
            </tr>
        </table>
        <br>
        <%}
		String next = CONTROLLER +"?estado=Grupos&accion=Reporte";
	%>
        <form name='reportFieldsFrm' method='post' action="<%= next %>">
            <input type=hidden name='cmd' value=''>
            <input type=hidden name='reporte' value="<%=request.getParameter("codReporte")%>">
    <% 
        Vector campos = model.consultasGrlesReportesService.obtenerVectorDetalleCampos();
        if( campos != null && !campos.isEmpty() ) { %>
            <input type=hidden id="numeroDeCampos" value="<%= campos.size() %>">
            <table cellSpacing=0 cellPadding=1 align=center border=1 class='fondotabla' width='750'>
            <tr align='center'>
                <td colspan="3" align="center" class="subtitulo1">LISTA DE CAMPOS DEL REPORTE</td>
                <td colspan="3" width="45%" align="left" class="barratitulo"><img src="<%=BASEURL%>/images/titulo.gif" width="32" height="20"></td>
            </tr>
            <tr class="fila">
            <td class='bordereporte' colspan='6'><input onclick='ejecutarComando(this.value,document.reportFieldsFrm)' checked='true' type='radio' id="opcion"  name="opcion" value="0"/>No hacer nada</td>
            </tr>
            <tr class="fila">
                <td class='bordereporte' colspan='6'><input onclick='ejecutarComando(this.value,document.reportFieldsFrm)' type='radio' id="opcion"  name="opcion"  value="1"/>Ordenar ascendentemente</td>
            </tr>
            <tr class="fila">
                <td class='bordereporte' colspan='6'><input onclick='ejecutarComando(this.value,document.reportFieldsFrm)' type='radio' id="opcion"  name="opcion"  value="2"/>Ordenar descendentemente</td>
            </tr>
            <tr class="fila">
                <td class='bordereporte' colspan='6'><input onclick='ejecutarComando(this.value,document.reportFieldsFrm)' type='radio' id="opcion"  name="opcion"  value="3"/>Actualizar orden automaticamente</td>
            </tr>
            <tr class='tblTitulo'>
                <td align='center' alt='Haga click aqui para seleccionar todos los campos'>
                    <input  type='checkbox' 
                    name='selectAll' 
                    alt='Haga click aqui para seleccionar todos los campos'
                    onclick='seleccionarTodos()'
                    >
                </td>
                <td align='center'>Nombre</td>
                <td align='center'>Titulo</td>
                <td align='center'>Grupo</td>
                <td align='center'>Orden</td>
                <td align='center'>Editar</td>
            </tr>
     <%
	 	
   	  	for( int idx = 0; idx < campos.size(); idx++ ){ 
			Hashtable campo = (Hashtable) campos.elementAt(idx);
		%>
		
            <tr class="letraTitulo" bgcolor="<%= campo.get("colorgrupo") %>">
            <td align='center' width='5%' class='bordereporte' >
                <input name='campos' value='<%= campo.get("nomcampo") %>' type='hidden'/>
                <input name='antiguoOrden<%= campo.get("nomcampo") %>' value='<%= campo.get("orden") %>' type='hidden'/>
                <input type='checkbox' name='fields' id='fld<%= idx %>' value='<%= campo.get("nomcampo") %>'>
            </td>
            <td class='bordereporte' nowrap width='*' ><b><%= campo.get("nomcampo")    %></b></td>
            <td class='bordereporte' nowrap width='*' ><b><%= campo.get("titulocampo") %></b></td>
            <td class='bordereporte' nowrap width='*' ><b><%= campo.get("grupo")       %></b></td>
            <td class='bordereporte'  align='center' width="25">
                <select onchange='verificarOrden(this)' id="orden<%= campo.get("nomcampo") %>" name="orden<%= campo.get("nomcampo") %>" >
                </select>
            </td>
            <td class='bordereporte' align='center'>
                <a href='javascript:void(0);' class="bordereporte"><img alt='Haga click aqui para editar el campo <%= campo.get("titulocampo") %>' onclick="agregarCampo('<%=BASEURL%>/jsp/sot/body/PantallaAgregarCampo.jsp?nombre=<%= campo.get("nomcampo") %>&tituloCampo=<%= campo.get("titulocampo") %>&grupo=<%= campo.get("grupo") %>&orden=<%= (idx+1) %>&titulo=Editar campo&codReporte=<%=request.getParameter("codReporte")%>')" src="<%=BASEURL%>/images/botones/iconos/modificar.gif"></a>
            </td>
            </tr>
<%
      }
      out.println("</table>");
%>
            <p>
            <center>
                <table>
                    <tr>
                        <td>
                            <tsp:boton value="eliminar" onclick="<%="enviarFormCampos('deleteFields')" %>"/>
                        </td>
                        <td width='20'>&nbsp;</td>
                        <td>
                            <tsp:boton value="agregar" onclick="<%="agregarCampo('"+BASEURL+"/jsp/sot/body/PantallaAgregarCampo.jsp?titulo=Agregar campo&codReporte="+request.getParameter("codReporte")+"')" %>"/>
                        </td>
                        <td width='20'>&nbsp;</td>
                        <td>
                            <tsp:boton value="aceptar" onclick="<%="enviarFormCampos('saveCamposGral')" %>"/>
                        </td>
                        <td width='20'>&nbsp;</td>
                        <td>
                            <tsp:boton value="regresar" onclick="<%="location.href ='" + CONTROLLER + "?estado=Grupos&accion=Reporte&cmd=list'"%>"/>
                        </td>
                        <td width='20'>&nbsp;</td>
                        <td>
                            <tsp:boton value="salir" onclick="<%="window.close()" %>"/>
                        </td>
                    </tr>
                </table>
            </center>
            </p>
   <% }
	else { %>
            <table border="2" align="center">
                <tr>
                    <td>
                        <table width="100%"  border="1" align="center"  bordercolor="#F7F5F4" bgcolor="#FFFFFF">
                            <tr>
                                <td width="229" align="center" class="mensajes">No se pueden configurar los campos del reporte <%= request.getParameter("nombreReporte")%></td>
                                <td width="29" background="<%=BASEURL%>/images/cuadronaranja.JPG">&nbsp;</td>
                                <td width="58">&nbsp;</td>
                            </tr>
                        </table>
                    </td>
                </tr>
            </table>
	<% } %>
            
            
        </form>
    </div>
    </body>
</html>
