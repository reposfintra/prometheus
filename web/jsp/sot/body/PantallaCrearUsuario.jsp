<!--
- Autor : Alejandro Payares
- Date  : Diciembre 24 del 2005
- Copyrigth Notice : Fintravalores S.A. S.A
- Descripcion : Pagina JSP que crear un usuario.
-->
<%@ page session="true" %>
<%@ page errorPage="/error/ErrorPage.jsp" %>
<%@ page import="java.util.*" %>
<%@ page import="com.tsp.operation.model.beans.*" %>
<%@ include file="/WEB-INF/InitModel.jsp" %>
<%@ taglib uri="http://jakarta.apache.org/taglibs/input-0.90" prefix="input" %>
<%@ taglib uri="http://www.sanchezpolo.com/sot" prefix="tsp"%><head>
    <title>.: Creaci�n de usuarios :.</title>
<%
  int NINGUNO = 0;
  int CLIENTES = 1;
  int DESTINATARIOS = 2;
  boolean resetForm = false;

  Usuario loggedUser = (Usuario) session.getAttribute("Usuario");

  String nomUsu  = request.getParameter("nombre");
  nomUsu  = (nomUsu == null ? "" : nomUsu);
  String dirUsu  = request.getParameter("direccion");
  dirUsu  = (dirUsu == null ? "" : dirUsu);
  String paisUsu = request.getParameter("pais");
  paisUsu  = (paisUsu == null ? "COL" : paisUsu);
  String ciuUsu  = request.getParameter("ciudad");
  ciuUsu  = (ciuUsu == null ? "" : ciuUsu);
  String mailUsu = request.getParameter("email");
  mailUsu  = (mailUsu == null ? "" : mailUsu);
  String telUsu  = request.getParameter("telefono");
  telUsu  = (telUsu == null ? "" : telUsu);
  String tipoUsu = request.getParameter("tipoUsuario");
  if( loggedUser.getTipo().equals("ADMIN") ) {
    tipoUsu  = (tipoUsu == null ? "ADMIN" : tipoUsu);
  }else if( loggedUser.getTipo().equals("CLIENTEADM") ){
    tipoUsu  = "DEST";
  }
  String nitUsu  = request.getParameter("nit");
  nitUsu  = (nitUsu == null ? "" : nitUsu);
  String cliDes  = request.getParameter("clienteDestinat");
  cliDes  = (cliDes == null ? "" : cliDes);
  String estUsu  = request.getParameter("estadoUsuario");
  estUsu  = (estUsu == null ? "A" : estUsu);
  String idUsu   = request.getParameter("idUser");
  idUsu  = (idUsu == null ? "" : idUsu);
  String key1Usu = request.getParameter("pass1");
  key1Usu  = (key1Usu == null ? "" : key1Usu);
  String key2Usu = request.getParameter("pass2");
  key2Usu  = (key2Usu == null ? "" : key2Usu);
  String clienteDestinatDisabled = "";
  List list = null;
  String nomClienteSelected = "";
  String textoCliDest = "";
 // boolean selected  = false;
  String formAction = "";

  //amendez 20050628
  String cia = request.getParameter("cia");
  String agencia = request.getParameter("agencia");
  String savePlanVj = request.getParameter("savePlanVj");
  String updatePass = request.getParameter("updatePass");
  agencia = (agencia == null ? "" : agencia);
  TreeMap agencias = null;
  String textoAgencia = "";
  TreeMap atribAgen = new TreeMap();
  atribAgen.put("class", "listmenu");
  String clienteVisible = "";

  if( loggedUser.getTipo().equals("ADMIN") )
  {
    if( tipoUsu.startsWith("CLIENTE") )
    {
      textoCliDest = "Cliente ";
      model.clienteService.clienteSearch();
      list = model.clienteService.getClientes();
    }else{
      //textoCliDest = "-";
      clienteDestinatDisabled = " disabled ";
      clienteVisible = "NoVisible";
    }

    if (tipoUsu.equals("TSPUSER")||tipoUsu.equals("ADMIN")){
       agencias = model.agenciaService.searchAgencia();
       textoAgencia = "Agencia ";
    }
    // else{ //COMENTARIADO POR IVAN GOMEZ
    //     atribAgen = new TreeMap();
    //    atribAgen.put("class", "NoVisible");
    //    atribAgen.put("disabled", "true");
    //    if(tipoUsu.equals("PROPIETA")){
    //         textoAgencia = "Nits";
    //    }
    //}
  }else if( loggedUser.getTipo().equals("CLIENTEADM") ){ %>
    <br><center>
        <b>Nombre del Usuario Cliente: <%= loggedUser.getNombre() %></b>
    </center><br><%
    textoCliDest = "Destinatario ";
    model.remidestService.destinatarioSearch(loggedUser.getClienteDestinat());
    list = model.remidestService.getDestinatarios();
  }
  if (tipoUsu.equals("CLIENTETSP")){
      atribAgen.put("disabled","true");
      atribAgen.put("style","visibility:hidden");
  }
  formAction = CONTROLLER + "?estado=GestionUsuarios&accion=CreateUser";
%>
    <script>
        var usuarios = new Array();
    <% model.usuarioService.obtenerUsuarios();
       Vector usuarios = model.usuarioService.getUsuarios();
       for( int i=0; i<usuarios.size(); i++ ){
           out.println("usuarios["+i+"] = '"+((Usuario)usuarios.elementAt(i)).getLogin()+"';");
       }
    %>
    </script>
    
 
    


<link href='<%=BASEURL%>/css/estilostsp.css' rel='stylesheet' type='text/css'>
    <script src='<%=BASEURL%>/js/PantallaCrearUsuario.js' language="javascript"></script>
    <script src='<%=BASEURL%>/js/boton.js' language="javascript"></script>
    <script>
        function _onmousemove(item) { item.className='select';   }
        function _onmouseout (item) { item.className='unselect'; }
        function addOption(Comb,valor,texto){
        var Ele = document.createElement("OPTION");
        Ele.value=valor;
        Ele.text=texto;
        Comb.add(Ele);
        }
        function move(cmbO, cmbD){
        for (i=cmbO.length-1; i>=0 ;i--)
        if (cmbO[i].selected){
        addOption(cmbD, cmbO[i].value, cmbO[i].text)
        cmbO.remove(i);
        }
        // order(cmbD);
        }
        function moveAll(cmbO, cmbD){
        for (i=cmbO.length-1; i>=0 ;i--){
        addOption(cmbD, cmbO[i].value, cmbO[i].text)
        cmbO.remove(i);
        }
        }
        function selectAll(cmb){
        for (i=0;i<cmb.length;i++)
        cmb[i].selected = true;
        }
		
    </script>
	
    <style type="text/css">
        <!--
        .icon {
        border-top: 1px solid #d6d3ce;
        border-left: 1px solid #d6d3ce;
        border-right: 1px solid #666666;
        border-bottom: 1px solid #666666;
        background-color:white;
        padding: 2px;
        width: 122px;
        height: 27px;
        }

        .iconOver {
        border-top: 1px solid #d6d3ce;
        border-left: 1px solid #d6d3ce;
        border-right: 1px solid #003399;
        border-bottom: 1px solid #003399;
        padding: 2px;
        width: 122px;
        height: 27px;
        background-color:#D1DCEB;
        color: #003399;
        }

        .iconClick {
        border-top: 1px solid #003399;
        border-left: 1px solid #003399;
        border-right: 1px solid #d6d3ce;
        border-bottom: 1px solid #d6d3ce;
        padding: 2px;
        width: 122px;
        height: 27px;
        background-color:#D1DCEB;
        color: #003399;
        }
        -->
    </style>
</head>
<body onresize="redimensionar()" onload='redimensionar()'>
    <div id="capaSuperior" style="position:absolute; width:100%; z-index:0; left: 0px; top: 0px;">
        <jsp:include page="/WEB-INF/toptsp.jsp?encabezado=CREAR NUEVO USUARIO"/>
    </div>
    <div id="capaCentral" style="position:absolute; width:100%; height:87%; z-index:0; left: 0px; top: 100px; overflow: scroll;"> 
	    <% String saveData  = request.getParameter("salvarDatos");
		  if( saveData != null && saveData.equals("S") ){
			saveData = "N";
			resetForm = true; %>
			<center>
				<table align="center" width="400"  border="0">
					<tr class="letraresaltada">
					<td><img src="<%=BASEURL%>/images/botones/iconos/users.gif"></td>
					<td nowrap>�� EL USUARIO FUE CREADO EXITOSAMENTE !!</td>
					</tr>
				</table>
			</center><br><%
		  } 
		  saveData  = (saveData == null ? "N" : saveData);%>
        <form name="createUserFrm" method="post" action="<%= formAction %>">
        <br>
        <table  border = '2' align = 'center'>
            <tr>
            <td>

                <table width="710">
                    <tr align='center'>
                        <td colspan="3" align="center" class="subtitulo1">DATOS BASICOS DEL USUARIO</td>
                        <td width="25%" align="left" class="barratitulo"><img src="<%=BASEURL%>/images/titulo.gif" width="32" height="20"></td>
                    </tr>

    

                    <%--AMENDEZ 20050628--%>
  <%
    model.ciaService.listarDistritos();
    TreeMap cias = model.ciaService.getCbxCia();
  %>
  
                    <%--APAYARES 20051121--%>
  <%
    model.ciaService.listarBases();
    TreeMap bases = model.ciaService.getBases();
  %>
                    <tr class='fila'>
                    <th align="left">Compa�ia</th>
                    <th align="left"><input:select attributesText="class='listmenu'" name="cia" options="<%=cias%>" default="FINV" multiple="true"/></th>
                    <td>Base</td>
                    <td><input:select name="base" attributesText="class='listmenu'" options="<%=bases%>" default="COL" size="3"/></td>
                    </tr>

                    <tr class='fila'>
                        <th align="left">
                            Nombre o Raz&oacute;n Social
                        </th>
                      <td colspan="3" align="left" >
                            <input name="nombre" class="textbox" type="text" maxlength='50'
                            size="80" value='<%= nomUsu %>'>
                          <img src="<%=BASEURL%>/images/botones/iconos/obligatorio.gif" width="10" height="10"></td>
                    </tr>

                    <tr class='fila'>
                        <th align="left">
                            Direcci&oacute;n
                        </th>
                        <td colspan="3" align="left">
                            <input name="direccion" class="textbox" type="text" maxlength='30'
                            size="80" value='<%= dirUsu %>'>
                            <img src="<%=BASEURL%>/images/botones/iconos/obligatorio.gif" width="10" height="10">
                        </td>
                    </tr>

                    <tr>
                        <th align="left" class='fila'>
                            Pa&iacute;s
                        </th>
                        <th align="left" class='fila'>
                            <select name="pais" class="listmenu">
                            <option value="COL" <%= (paisUsu.equals("COL")? "selected" : "") %>>Colombia
                            <option value="ECU" <%= (paisUsu.equals("ECU")? "selected" : "") %>>Ecuador
                            <option value="VEN" <%= (paisUsu.equals("VEN")? "selected" : "") %>>Venezuela
                          </select>
                            <img src="<%=BASEURL%>/images/botones/iconos/obligatorio.gif" width="10" height="10">
                        </th>
                        <th align="left" class='fila'>
                            Ciudad
                        </th>
                        <th align="left" class='fila'>
                        <input class="textbox" type="text" maxlength='30' name="ciudad" value='<%= ciuUsu %>'>
                        <img src="<%=BASEURL%>/images/botones/iconos/obligatorio.gif" width="10" height="10">
                        </th>
                    </tr>

                    <tr>
                    <th nowrap align="left" class='fila'>
                        Correo Electr&oacute;nico
                    </th>
                    <th align="left" class='fila'>
                    <input class="textbox" name="email" id="email" maxlength='50' type="text"
                    size="35" value='<%= mailUsu %>'>
                    <img src="<%=BASEURL%>/images/botones/iconos/obligatorio.gif" width="10" height="10">
                    </th>
                    <th align="left" class='fila'>
                        Tel&eacute;fono
                    </th>
                    <th align="left" class='fila'>
                    <input class="textbox" name="telefono" maxlength='20' type="text" value='<%= telUsu %>'
                    onkeypress='return soloDigitosKeypress();'>
                    <img src="<%=BASEURL%>/images/botones/iconos/obligatorio.gif" width="10" height="10">
                    </th>
                    </tr>

                    <tr>
                    <th align="left" class='fila'>
                        Tipo de Usuario
                    </th>
                    <th align="left" class='fila'>
                        <select name="tipoUsuario" id="tipoUsuario" class="listmenu" onChange="document.createUserFrm.submit();">
                          <%
        if( loggedUser.getTipo().equals("ADMIN") )
        { %>
                        <option value='ADMIN' <%= (tipoUsu.equals("ADMIN")? "selected" : "") %>>Administrador
                        <option value='CLIENTETSP' <%= (tipoUsu.equals("CLIENTETSP")? "selected" : "") %>>Cliente de Fintra
                        <option value='CLIENTEADM' <%= (tipoUsu.equals("CLIENTEADM")? "selected" : "") %>>Cliente - Admin. de Transporte
                        <option value='TSPSERVCLI' <%= (tipoUsu.equals("TSPSERVCLI")? "selected" : "") %>>Servicio al Cliente de TSP
                        <option value='TSPUSER' <%= (tipoUsu.equals("TSPUSER")? "selected" : "") %>>Usuario de Fintra
                         <%
        } else if( loggedUser.getTipo().equals("CLIENTEADM") ) { %>
                        <option value='DEST' <%= (tipoUsu.equals("DEST")? "selected" : "") %>>Destinatario <%
        } %>
                      </select><img src="<%=BASEURL%>/images/botones/iconos/obligatorio.gif" width="10" height="10">
                    </th>
                    <th align="left" class='fila'>
                        NIT o CC 
                    </th>
                    <th align="left" class='fila'>
                    <input class="textbox" name="nit" maxlength='20' type="text" value='<%= nitUsu %>'>
                    <img src="<%=BASEURL%>/images/botones/iconos/obligatorio.gif" width="10" height="10">
                    </th>
                    </tr>
                    <%--APAYARES 20060224--%>
                    <tr class='fila'>
                        <th align="left">
                            Perfil(es)
                        </th>
                        <td colspan="3" align="left" valign='top'>
                            <select name="perfil" multiple class='listmenu' size='5'>
                            <%  List listaPerfiles = model.menuService.getPerfiles();
                                Iterator itePerfiles = listaPerfiles.iterator();
                                while( itePerfiles.hasNext() ){
                                    Perfil p = (Perfil) itePerfiles.next();
                                    out.println("\t\t\t\t<option value='"+p.getId()+"'>"+p.getNombre()+"</option>");
                                }
                             %>
                            </select>
                            <img src="<%=BASEURL%>/images/botones/iconos/obligatorio.gif" width="10" height="10">
                        </td>
                    </tr>
                    
                    <tr>
                    <th align="left" class='fila'>
                        <div id='TextoClienteDestinat'><%= textoCliDest %></div>
                        <div id='TextoAgencia'><%= textoAgencia %></div>
                    </th>
                    <th align="left" class='fila' colspan='3'>
       <%
      // Generar din�micamente
      String codigo = null;
      String nombre = null;
      Iterator it = null;
      if (list != null)
      {
        Cliente cliente = null;
        it = list.iterator();
		System.out.println("tipo de usuario: "+loggedUser.getTipo());
        if( loggedUser.getTipo().equals("ADMIN") )
        {
          %><table  border='0' width='500' cellpadding='3' cellspacing='0' align='left'>
              <tr><td align='center'>

                  <table border='1' width='98%' cellpadding='0' cellspacing='0'>
                      <tr class='subtitulo1'>
                          <th width='46%' class='bordereporte'>Clientes Asignados</th>
                          <th width='*' class='bordereporte' >&nbsp;</th>
                          <th width='46%' class='bordereporte'>Clientes </th>
                      </tr>
                      <tr class='fila'>
                          <td class='bordereporte'><select multiple size='8' class='listmenu' style='width:100%' name='clienteDestinat' onchange=" if (this.selectedIndex!=-1) { window.status=this[this.selectedIndex].text;}"></select></td>
                          <td align="center" class='bordereporte'>
                              <tsp:boton value="envIzq" onclick="move(Clientes, clienteDestinat);"/><br>
                              <tsp:boton value="enviarIzq" onclick="moveAll(Clientes, clienteDestinat);"/><br>
                              <tsp:boton value="envDer" onclick="move(clienteDestinat, Clientes );"/><br>
                              <tsp:boton value="enviarDerecha" onclick="moveAll(clienteDestinat, Clientes );"/><br>
                          </td>
                          <td class='bordereporte'><select multiple size='8' class='listmenu' style='width:100%' name='Clientes' onchange=" if (this.selectedIndex!=-1) { window.status=this[this.selectedIndex].text;}">
                          <!-- *******************************************  -->

                             <% while (it.hasNext())  {
                                cliente = (Cliente) it.next();
                                codigo = cliente.getCodcli();
                                nombre = cliente.getNomcli();%>
                                <option value='<%= codigo %>' label='<%= nombre %>'><%= nombre %></option>
                              <% } %>


                          <!-- *******************************************  -->
                          </select></td>
                      </tr>
                  </table>
              </td></tr>
          </table>
     <% }
   }%>
                        <%--AMENDEZ 20050628--%>
                        <input:select name="agencia" options="<%=agencias%>" attributes="<%=atribAgen%>"/>
                        <% if ( "listmenu".equals(atribAgen.get("class")) ) { %>
                            <img src="<%=BASEURL%>/images/botones/iconos/obligatorio.gif" width="10" height="10">
                        <% }%>
                    
                    </th>
                    </tr>
                    <tr>
                        <td align="left" class='fila'>
                            Estado del Usuario 
                        </td>
                        <td align="left" class='fila' colspan='3'>
                            <select name="estadoUsuario" class="listmenu">
                            <option value="A" <%= (estUsu.equals("A")? " selected " : "") %>>Activo
                            <option value="I" <%= (estUsu.equals("I")? " selected " : "") %>>Inactivo
                            <option value="R" <%= (estUsu.equals("R")? " selected " : "") %>>Retirado
                          </select>
                        </td>
                    </tr>
                    <%--AMENDEZ 20050628--%>
                    <tr class='fila'>
                    <td align='left'>�Graba Plan de Viaje?</td>
                    <td><input:checkbox name="savePlanVj" value="1"/></td>
                    <td align='left'>�Pertenece a Trafico?</td>
                    <td><input:checkbox name="dpto" value="traf"/></td>
                    </tr>
                    <tr>
                        <td colspan="3" align="center" class="subtitulo1">DATOS PARA ACCESO AL SISTEMA</td>
                        <td width="25%" align="left" class="barratitulo"><img src="<%=BASEURL%>/images/titulo.gif" width="32" height="20"></td>
                    </tr>
    
                    <tr>
                        <th align="left" class='fila'>
                            ID del Usuario
                        </th>
                        <th align="left" class='fila'>
                        <input class="textbox" name="idUser" maxlength='10' type="text" value='<%= idUsu %>'>
                        <img src="<%=BASEURL%>/images/botones/iconos/obligatorio.gif" width="10" height="10">
                        </th>
                        <th align="left" class='fila'>
                            Password
                        </th>
                        <th align="left" class='fila'>
                        <input class="textbox" name="pass1" type="password" maxlength='15' value='<%= key1Usu %>'>
                        <img src="<%=BASEURL%>/images/botones/iconos/obligatorio.gif" width="10" height="10">
                        </th>
                    </tr>

                    <tr>
                    <th align="left" class='fila'>
                        Reescriba Password
                    </th>
                    <th align="left" class='fila'>
                    <input class="textbox" name="pass2" type="password" maxlength='15' value='<%= key2Usu %>'>
                    <img src="<%=BASEURL%>/images/botones/iconos/obligatorio.gif" width="10" height="10">
                    </th>

                    <%--AMENDEZ 20050628--%>
                    <th class='fila'>
                        Cambiar Clave al Inicio
                    </th>
                    <th align="left" class='fila'>
                        <input:checkbox name="updatePass" value="true" attributesText="checked"/>  
                    </th>

                    </tr>
        
                </table>
            </td>
            </tr>
        </table>
        <br>
        <table width="710" align='center' border='0'>
            <tr class="letraresaltada">
            <th align='left'>
                <img src="<%=BASEURL%>/images/botones/iconos/obligatorio.gif" width="10" height="10">
                <b>Datos obligatorios</b>
            </th>
            <td align='right'>
                <table align='right' border='0'>
                    <tr>
                        <td>
                            <% String back = "location.href='"+CONTROLLER+"?estado=GestionUsuarios&accion=ManageUsers';"; %>
                            <tsp:boton name="regresar" value="regresar" onclick="<%=back%>" />
                        </td>
                        <td width='20'></td>
                        <td>
                            <tsp:boton name="saveUserDataBtn" value="agregar" onclick="if(createUserFrm.clienteDestinat){ selectAll(createUserFrm.clienteDestinat);} enviarDatosBtnClick();" />
                        </td>
                    </tr>
                 </table>
            </td>
            </tr>
        </table>
        <input type='hidden' name='salvarDatos' value='<%= saveData %>'>
        </form>
    </div>
</body>
 <%
if( resetForm )
{ %>
  <script language='javascript'>
    createUserFrmReset();
  </script><%
} %>
