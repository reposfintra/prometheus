<!--  
	 - Author(s)       :      LREALES
	 - Description     :      AYUDA FUNCIONAL - Reporte Proveedores Fintra
	 - Date            :      14/09/2006 
	 - Copyright Notice:      TRANSPORTES SANCHEZ POLO S.A.
-->
<%@ include file="/WEB-INF/InitModel.jsp"%>
<HTML>
<HEAD>
<TITLE>Funcionalidad del Adicionar Cliente</TITLE>
<META http-equiv=Content-Type content="text/html; charset=windows-1252">
<link href="/../../../css/estilostsp.css" rel="stylesheet" type="text/css"> 
<link href="<%=BASEURL%>/css/estilostsp.css" rel="stylesheet" type="text/css">
<script type='text/javascript' src="<%= BASEURL %>/js/boton.js"></script> 
</HEAD>
<BODY> 
<% String BASEIMG =  BASEURL +"/images/ayuda/Cliente/"; %>
  <table width="95%"  border="2" align="center">
    <tr>
      <td height="117" >
        <table width="100%" border="0" align="center">
          <tr  class="subtitulo">
            <td height="20"><div align="center">MANUAL DE REPORTE WEB</div></td>
          </tr>
          <tr class="subtitulo1">
            <td>Descripci&oacute;n del funcionamiento del programa Adicionar Cliente </td>
          </tr>
          <tr>
            <td  class="ayudaHtmlTexto"><div align="center"></div></td>
          </tr>
<tr>
            <td  class="ayudaHtmlTexto"><p class="ayudaHtmlTexto">&nbsp;</p>
            <p class="ayudaHtmlTexto">En la siguiente pantalla  muestra todos los campos obligatorios donde dependiendo del campo dejara asignar ya sea caracteres o numeros</p>
            <p class="ayudaHtmlTexto"> un ejemplo podria ser en la Cedula del Cliente solo deja ingresar numeros y el campo Nombre del Cliente deja ingresar caracteres o numeros .Hay</p>
            <p class="ayudaHtmlTexto"> campos donde solo se puede ingresar un valor ya sea numero o caracteres un ejemplo Fiduciaria solo deja ingresar un parametro</p></td>
          </tr>
<tr>
            <td  class="ayudaHtmlTexto"><div align="center"><IMG height=862 src="<%=BASEIMG%>image001.JPG" width=1019 border=0 v:shapes="_x0000_i1143"></div></td>
          </tr>
<tr>
  <td  class="ayudaHtmlTexto"><p>&nbsp;</p>
    <p>El sistema le muestra el mensaje con el Nombre del Cliente que fue agregado a la BD exitosamente.</p></td>
</tr>
<tr>
  <td height="962"  class="ayudaHtmlTexto"><div align="center"> <img height=983 src="<%=BASEIMG%>image002.JPG" width=1019 border=0 v:shapes="_x0000_i1052"></div></td>
</tr>
<tr>
  <td  class="ayudaHtmlTexto"><p>&nbsp;</p>
    <p>En esta pantalla muestra cuando el Cliente existe en la BD </p></td>
</tr>
<tr>
  <td  class="ayudaHtmlTexto"><p align="center"><img height=983 src="<%=BASEIMG%>image003.JPG" width=1019 border=0 v:shapes="_x0000_i1052"></p>    </td>
</tr>
<tr>
  <td  class="ayudaHtmlTexto"><p>&nbsp;</p>
    <p>El sistema tambien usa un sistema de caja cerrada donde usted de darle click en la casilla y se desplegara un listado pues como muestra en la pantalla la </p>
    <p>Agencia de Duenia del cliente.</p></td>
</tr>
<tr>
  <td  class="ayudaHtmlTexto"><div align="center"><IMG height=874 src="<%=BASEIMG%>image004.JPG" width=1007 border=0 v:shapes="_x0000_i1052"></div></td>
</tr>




<tr>
  <td  class="ayudaHtmlTexto"><p>&nbsp;</p>
    <p>Luego de escribir en los campos del sistemas y dejo alguno vacio le saldra el siguiente Mensaje. Despendiendo del campo que halla dejado en blanco </p>
    <p>le saldra el mesaje especifico en este caso fue el de cedula... </p></td>
</tr>
<tr>
  <td  class="ayudaHtmlTexto"><div align="center"><IMG height=126 src="<%=BASEIMG%>image_error001.JPG" width=327 border=0 v:shapes="_x0000_i1052"></div></td>
</tr>
        </table></td>
    </tr>
  </table>
  <p></p>
<center>
<img src = "<%=BASEURL%>/images/botones/salir.gif" style = "cursor:hand" name = "imgsalir" onClick = "parent.close();" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);">
</center>
</BODY>
</HTML>
