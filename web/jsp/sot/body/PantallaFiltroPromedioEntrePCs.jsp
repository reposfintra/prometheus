<!--
- Autor : Alejandro Payares
- Date  : Febrero 16 de 2006 - 11:44 AM
- Copyrigth Notice : Fintravalores S.A. S.A
- Descripcion : Pagina JSP que permite ver el calculo del promedio de tiempo entre 2 puestos de control.
-->
<%@ page errorPage="/ErrorPage.jsp" %>
<%@ page import="java.util.*" %>
<%@ page import="com.tsp.operation.model.beans.*" %>
<%@ include file="/WEB-INF/InitModel.jsp" %>
<%@ taglib uri="http://www.sanchezpolo.com/sot" prefix="tsp"%>
<% String controllerAction = CONTROLLER + "?estado=ReporteTiempos&accion=PuestosDeControl&cmd="; %>
<%! 
        private StringBuffer cola = null;
	private String obtenerComboCiudades(String nombreCombo, String nombreCampo, String selected, com.tsp.operation.model.Model model ){
		StringBuffer sb = new StringBuffer();
		sb.append("<select name='"+nombreCombo+"' id='"+nombreCombo+"' size='4' class='listmenu' style='width:100%' onclick='document.returnsFrm."+nombreCampo+".value = this[this.selectedIndex].innerText;' >");
		if ( cola != null ){
			return sb.append(cola).toString();
		}
		cola = new StringBuffer();
                TreeMap tree = model.ciudadService.getTreMapCiudades();
		Set set = tree.entrySet();
		Iterator ite = set.iterator();
		while( ite.hasNext() ){
			Map.Entry entry = (Map.Entry)ite.next();
                        String select = selected != null && selected.equals(entry.getValue())?"selected":"";
			cola.append("\n\t<option value='"+entry.getValue()+"' "+select+">" + entry.getKey() + " - " + entry.getValue() + "</option>");
		}
		cola.append("\n</select>");
		return sb.append(cola).toString();
	}
%>
<html>
<head>
    <title>.: CALCULAR PROMEDIO ENTRE DOS PUESTOS DE CONTROL :.</title>
    <script language="javascript" src="<%=BASEURL%>/jsp/sot/js/tools.js"></script>
    <script language="javascript" src="<%=BASEURL%>/jsp/sot/js/PantallaFiltroPromedioEntrePCs.js"></script>
    <link href="<%=BASEURL%>/css/estilostsp.css" rel="stylesheet" type="text/css">
</head>
<body onload='redimensionar();document.returnsFrm.campoOrigen.focus();' onresize="redimensionar()">
    <div id="capaSuperior" style="position:absolute; width:100%; z-index:0; left: 0px; top: 0px;">
        <jsp:include page="/toptsp.jsp?encabezado=TIEMPO PROMEDIO ENTRE PUESTOS DE CONTROL"/>
    </div>
    <div id="capaCentral" style="position:absolute; width:100%; height:87%; z-index:0; left: 0px; top: 100px; overflow: scroll;"> 
        <br>
        <br>
        <%if(request.getParameter("mensaje") != null){%>
            <table border="2" align="center">
                <tr>
                    <td><table width="100%"  border="1" align="center"  bordercolor="#F7F5F4" bgcolor="#FFFFFF">
                        <tr>
                            <td width="229" align="center" class="mensajes"><%=request.getParameter("mensaje")%></td>
                            <td width="29" background="<%=BASEURL%>/images/cuadronaranja.JPG" >&nbsp;</td>
                            <td width="58">&nbsp;</td>
                        </tr>
                    </table></td>
                </tr>
            </table>
            <br>
        <%} %>
        
            <table align='center' border = '1'>
                <tr>
                    <td width='500px'>
                        <table border='0' cellpadding="3" cellspacing="3" align="center" class='fondotabla' width='100%'>
                            <tr align='center'>
                                <td colspan="2" width="85%"align="center" class="subtitulo1">SELECCI�N DEL TRAMO</td>
                                <td colspan="1" width="15%" align="left" class="barratitulo"><img src="<%=BASEURL%>/images/titulo.gif" width="32" height="20"></td>
                            </tr>
                            <form name="returnsFrm" method="post" action='<%= controllerAction+"avg" %>'>
                            <tr class='fila'>
                                <th align='left' width='30%'>Puesto de control<br>Origen:</th>
                                <td colspan="2">
                                  <table width="100%"  border="0">
                                    <tr class="fila">
                                        <td>Campo de busqueda: 
                                        <input type="text" class="textbox" name="campoOrigen" id="campoOrigen" style="width:100%" onKeyUp="buscar(document.returnsFrm.origen,this)" ></td>
                                    </tr>
                                    <tr>
                                        <td> 
						<%= obtenerComboCiudades("origen", "campoOrigen", request.getParameter("origen"),model) %>
                                        </td>
                                    </tr>
                                  </table>
                                </td>
                            </tr>
                            <tr class='fila'>
                                <th  align='left'>Puesto de control<br>Destino:</th>
                                <td colspan="2"><table width="100%"  border="0">
                                    <tr class="fila">
                                        <td>Campo de busqueda: 
                                        <input type="text" class="textbox" id="campoDestino" style="width:100%" onKeyUp="buscar(document.returnsFrm.destino,this)" ></td>
                                    </tr>
                                    <tr>
                                        <td> 
						<%= obtenerComboCiudades("destino", "campoDestino", request.getParameter("destino"),model) %>
                                        </td>
                                    </tr>
                                </table></td>
                            </tr>
                                 <input type='hidden' name='nombreOrigen'>
                                 <input type='hidden' name='nombreDestino'>
                            </form>
                            <% if ( model.repTiemposPCService.obtenerPromedio() != null && request.getParameter("mensaje") == null ) { %>
                            <tr class='fila'>
                                <td colspan='3' align='center'>
                                    <%  if ( model.repTiemposPCService.obtenerPromedio().length() == 0 ) {
                                            out.println("No fue encontrado ning�n viaje entre los puestos de control "+
                                            request.getParameter("nombreOrigen")+" y "+
                                            request.getParameter("nombreDestino")+" en el �ltimo mes");
                                        }
                                        else {
                                            out.println("El tiempo promedio en el tramo<br>"+request.getParameter("nombreOrigen")+" y "+
                                            request.getParameter("nombreDestino")+" es de:<br>"+model.repTiemposPCService.obtenerPromedio() +
                                            "<br>");
                                    %>
                                    <form method='post' name='formDetalles' action='<%= controllerAction + "detalles" %>'>
                                        <table border='0'>
                                            <tr class='fila'>
                                                <td>Formato: <select class='listmenu' name="formatoSalida" >
                                                                  <option value="web" selected="selected">P&aacute;gina WEB
                                                                  <option value="excel">Archivo Excel
                                                             </select> &nbsp;
                                                    <tsp:boton value="detalles" name="botonVerDetalles"  onclick='<%="enviarFormDetalles(document.formDetalles,'"+request.getParameter("nombreOrigen")+"','"+request.getParameter("nombreDestino")+"');"%>' />
                                                </td>
                                            </tr>
                                        </table>
                                    </form>
                                    <% } %>
                                </td>
                            </tr>  
                          <% } %>
                        </table>
                    </td>
                </tr>
            </table>
            <center>
            <p>
            <table>
            <tr>
            <td  colspan="3" align="center">
                <tsp:boton name="returnsBtn" value="buscar" type="submit" onclick="validarSeleccion(returnsFrm,returnsFrm.origen.value,returnsFrm.destino.value);"/>
            </td>
            <td width='30'></td>
            <td>
                <tsp:boton id="botonLimpiarForma" type="reset" name="LimpiarFormularioBtn"  value="restablecer" />
            </td>
            </tr>
            </table>
            </p>
            </center>
            </tr>
       
    </div>
</body>
<tsp:InitCalendar/>
</html>
