<!--
- Autor : Alejandro Payares
- Date  : Diciembre 24 del 2005
- Copyrigth Notice : Fintravalores S.A. S.A
- Descripcion : Pagina JSP que permite agregar un nuevo registro dentro de una subtabla de tablagen.
-->
<%@page contentType="text/html"%>
<%@page import="com.tsp.operation.model.beans.*, java.util.*"%>
<%@include file="/WEB-INF/InitModel.jsp" %>
<%@ taglib uri="http://jakarta.apache.org/taglibs/input-0.90" prefix="input" %>
<%@ taglib uri="http://www.sanchezpolo.com/sot" prefix="tsp"%>
<%@page import="com.tsp.operation.model.beans.*"%>
<%! private String notNull(Object x){
        return x == null? "":x.toString();
    }
%>
<%
    String path = application.getRealPath(model.menuService.getRealUrl(request.getRequestURI()));
    String datos[] = model.menuService.getContenidoMenu(BASEURL,request.getRequestURI(),path);
%>
<html>
    <%if(request.getParameter("mensaje") != null){%>
    <body onload='window.opener.location.reload();'>
    <% }
        else {
            out.println("<body>");
        }
    %>
        
    <head> 
        <title>.: ADMINISTRACIÓN DE CAMPOS :.</title>
        <script language="javascript" src="<%=BASEURL%>/jsp/sot/js/PantallaCamposGeneralesReporte.js"></script>
        <script language="javascript" src="<%=BASEURL%>/js/tools.js"></script>
        <script language="javascript" src="<%=BASEURL%>/js/boton.js"></script>
        <link href="<%=BASEURL%>/css/estilostsp.css" rel="stylesheet" type="text/css">
    </head>
    
            <%if(request.getParameter("mensaje") != null){%>
    <table border="2" align="center">
        <tr>
            <td><table width="100%"  border="1" align="center"  bordercolor="#F7F5F4" bgcolor="#FFFFFF">
                <tr>
                    <td width="229" align="center" class="mensajes"><%=request.getParameter("mensaje")%></td>
                    <td width="29" background="<%=BASEURL%>/images/cuadronaranja.JPG">&nbsp;</td>
                    <td width="58">&nbsp;</td>
                </tr>
            </table></td>
        </tr>
    </table>
    <br>
            <%}
            String cmd = request.getParameter("titulo") != null && request.getParameter("titulo").toLowerCase().startsWith("editar")?"modificar":"agregar";
            String action = CONTROLLER+"?estado=Grupos&accion=Reporte&cmd="+cmd+"Campo"; 
			String info="";%>
    <form name='forma' method="post"  action="<%=action%>">
        <input type=hidden name='codrpt' value="<%=request.getParameter("codReporte")%>">
        <input type=hidden name='nombreAntiguo' value="<%=request.getParameter("nombre")%>">
        <input type=hidden name='tituloAntiguo' value="<%=request.getParameter("tituloCampo")%>">
        <input type=hidden name='orden' value="<%=request.getParameter("orden")%>">
        <table align='center' border = '1'>
        <tr>
            <td width='400px'>
            <table width='100%'>
            <tr align='center'>
                <td colspan="4" align="center" class="subtitulo1"><%=notNull(request.getParameter("titulo"))%><br></td>
                <td colspan="1" width="45%" align="left" class="barratitulo"><img src="<%=BASEURL%>/images/titulo.gif" width="32" height="20" align="left"><%=datos[0]%></td>
            </tr>
            <tr class="fila">
                <td width='30%'>Nombre (codigo) </td>
                <td colspan="4">
                    <input name="nombre" type="text" id="nombre" value="<%=notNull(request.getParameter("nombre"))%>" style='width:100%' maxlength='50'/>
                    <img src='<%=BASEURL%>/images/botones/iconos/obligatorio.gif'>
                </td>
            </tr>
            <tr class="fila">
                <td>Titulo</td>
                <td colspan="4">
                    <input name="titulo" type="text"  value="<%=notNull(request.getParameter("tituloCampo"))%>" id="titulo" style='width:100%' maxlength='50'/>
                    <img src='<%=BASEURL%>/images/botones/iconos/obligatorio.gif'>
                </td>
            </tr>
            <tr class="fila">
            <td>Grupo</td>
            <td colspan="4">
                <select name="grupos" id="grupos">
                                <%  Vector grupos = model.gruposReporteService.obtenerGrupos();
                                    for( int i=0; i<grupos.size(); i++ ){
                                        Hashtable grupo = (Hashtable)grupos.elementAt(i);
                                        String selected = grupo.get("grupo").equals(request.getParameter("grupo"))?"selected":"";
                                        out.println("<option style='background-color:"+grupo.get("colorgrupo")+"' value='"+grupo.get("grupo")+","+grupo.get("colorgrupo")+"' "+selected+">"+grupo.get("grupo")+"</option>");
                                    }
                                %>
                </select>
            </td>
        </tr>
            </table>
        </td>
        </tr>
        </table>
    </form>
    <p>
    <center>
        <table>
            <tr>
                <td>
                    <tsp:boton value="<%=cmd%>" onclick="enviarFormNuevoCampo()"/>
                </td>
                <td width='30'>&nbsp;</td>
                <td>
                    <tsp:boton value="salir" onclick="window.close()"/>
                </td>
            </tr>
        </table>
    </center>
    </p>
        <% String str = datos[1];
           str = str.replaceAll("PantallaTablaGenNuevoRegistroHelpD.jsp","PantallaTablagenListHelpD.jsp#"+cmd+"Registro");
           str = str.replaceAll("PantallaTablaGenNuevoRegistroHelpF.jsp","PantallaTablagenListHelpF.jsp#"+cmd+"Registro");
           out.println(str);
        %>
    </body>
</html>
