<!--
- Autor : Alejandro Payares
- Date  : Enero 6 de 2006 - 7:37 PM
- Copyrigth Notice : Fintravalores S.A. S.A
- Descripcion : Pagina JSP que permite ver los datos del reporte de devoluciones.
-->
<%@ page session="true" %>
<%@ page errorPage="/ErrorPage.jsp" %>
<%@ page import="java.util.*" %>
<%@ page import="com.tsp.operation.model.beans.*" %>
<%@ include file="/WEB-INF/InitModel.jsp" %>
<%@ taglib uri="http://jakarta.apache.org/taglibs/input-0.90" prefix="input" %>
<%@ taglib uri="http://www.sanchezpolo.com/sot" prefix="tsp"%>

<% Usuario loggedUser = (Usuario)session.getAttribute("Usuario");
String codCliente = "";
String tipoUsuario = loggedUser.getTipo();
if( tipoUsuario.startsWith("CLIENTE") )
  codCliente = loggedUser.getClienteDestinat();
else if( tipoUsuario.equals("DEST") ) {
  String codDest = loggedUser.getClienteDestinat();
  java.util.TreeMap dest = model.clienteService.listar(loggedUser);
  codCliente = (String)dest.get(dest.firstKey());
}
String controllerAction = CONTROLLER + "?estado=Devoluciones&accion=SearchDiscrep"; %>
<head>
    <title>.: FILTRO DEL REPORTE DE DEVOLUCIONES DISCREPANCIA:.</title>
    <script src='<%=BASEURL%>/jsp/sot/js/PantallaFiltroDevolucionesDiscrep.js' language='javascript'></script>
    <script language="javascript" src="<%=BASEURL%>/js/tools.js"></script>
    <link href="<%=BASEURL%>/css/estilostsp.css" rel="stylesheet" type="text/css">
</head>
<body onload='redimensionar();window.document.returnsFrm.campo.focus()' onresize="redimensionar()">
    <div id="capaSuperior" style="position:absolute; width:100%; z-index:0; left: 0px; top: 0px;">
        <jsp:include page="/toptsp.jsp?encabezado=REPORTE DE DEVOLUCIONES DISCREPANCIA"/>
    </div>
    <div id="capaCentral" style="position:absolute; width:100%; height:87%; z-index:0; left: 0px; top: 100px; overflow: scroll;"> 
        <form name="returnsFrm" method="post">
        <table align='center' border = '1'>
            <tr>
                <td width='500px'>
        
                <table border='0' cellpadding="3" cellspacing="3" align="center" class='fondotabla'>
                <tr align='center'>
                    <td colspan="2" align="center" class="subtitulo1">DEVOLUCIONES DISCREPANCIA<br></td>
                    <td colspan="1" width="15%" align="left" class="barratitulo"><img src="<%=BASEURL%>/images/titulo.gif" width="32" height="20"></td>
                </tr>
                <tr class="fila" >
                    <th  align='left'>Cliente</th>
                    <td colspan="2">
                    <table width="100%"  border="0">
                    <tr class="fila">
                        <td>Campo de busqueda: <br>
                        <input type="text" class="textbox" id="campo" style="width:400" onKeyUp="buscar(document.returnsFrm.cliente,this)" ></td>
                    </tr>
                    <tr>
                    <td>
                        <input:select default='<%= codCliente %>' name="cliente" options="<%=model.clienteService.listar(loggedUser)%>" attributesText="id='cliente' class='listmenu' size='5' style='width:100%' onclick='document.returnsFrm.campo.value = this[this.selectedIndex].innerText;'"/>
                    </td>
                </tr>
                    </table>
                </td>
            </tr>
                    <tr class='fila'>
                    <th  align='left' colspan="3">
                        <table>
                            <tr class='fila'>
                            <td>
                                Fecha Inicial:
                            </td>
                            <td>
                                <tsp:Date fechaInicial="hoy" formulario="returnsFrm" clase="textbox" name="fechaini" otros="size='20' alt='De click aqu� para escoger la fecha'" />
                            </td>
                            <td width='50'>&nbsp;</td>
                            <td  align='left'>
                                Fecha Final:
                            </td>
                            <td>
                                <tsp:Date fechaInicial="hoy" formulario="returnsFrm" clase="textbox" name="fechafin" otros="size='20' alt='De click aqu� para escoger la fecha'" />
                            </td>
                            </tr>
                        </table>
                    </th>
                    </tr>
                    <tr class='fila'>
                    <% model.ciudadService.searchTreMapCiudades(); %>
                    
                        <th  align='left'>Origen:</th>
                        <td colspan="2"><table width="100%"  border="0">
                        <tr class="fila">
                            <td>Campo de busqueda: 
                            <input type="text" class="textbox" id="campoOrigen" style="width:250" onKeyUp="buscar(document.returnsFrm.origen,this)" ></td>
                        </tr>
                        <tr>
                        <td> <input:select default="" name="origen" options="<%=model.ciudadService.getTreMapCiudades()%>" attributesText="id='origen' size='2' class='listmenu' style='width:100%' onclick='document.returnsFrm.campoOrigen.value = this[this.selectedIndex].innerText;'"/> </td>
                    </tr>
                    <tr>
                            <td>
                                <input type="checkbox" name='todoOrigen' id="todoOrigen" onChange="document.returnsFrm.origen.disabled = this.checked"/><label class='fila' for="todoOrigen">Buscar en todas las ciudades</label>
                            </td>
                        </tr>
                    </table></td>
                    </tr>
                    <tr class='fila'>
                        <th  align='left'>Destino:</th>
                        <td colspan="2"><table width="100%"  border="0">
                        <tr class="fila">
                            <td>Campo de busqueda: 
                            <input type="text" class="textbox" id="campoDestino" style="width:250" onKeyUp="buscar(document.returnsFrm.destino,this)" ></td>
                        </tr>
                        <tr>
                        <td> <input:select default="" name="destino" options="<%=model.ciudadService.getTreMapCiudades()%>" attributesText="id='destino' size='2' class='listmenu' style='width:100%' onclick='document.returnsFrm.campoDestino.value = this[this.selectedIndex].innerText;'"/> </td>
                    </tr>
                        <tr>
                            <td>
                                <input type="checkbox" name="todoDestino" id="todoDestino" onChange="document.returnsFrm.destino.disabled = this.checked"/><label class='fila' for="todoDestino">Buscar en todas las ciudades</label>
                            </td>
                        </tr>
                    </table></td>
                    </tr>
                    <tr class='fila'>
                        <th  align='left'>
                            Tipo de Devoluci&oacute;n</b>
                        </th>
                        <td colspan="2">
                            <select name="tipoDevolucion" class='listmenu'>
                            <option value="ALL" selected>Todos
                            <option value="OPEN">Abiertas
                            <option value="CLOSED">Cerradas
                            </select>
                        </td>
                    </tr>
                    <tr align='center'>
                        <td colspan="2" align="center" class="subtitulo1">Consulta de las devoluciones de un pedido espec&iacute;fico<br></td>
                        <td colspan="1" width="15%" align="left" class="barratitulo"><img src="<%=BASEURL%>/images/titulo.gif" width="32" height="20"></td>
                    </tr>
                    <tr class='normal'>
                        <th  align='left' class="fila">
                            No. Pedido
                        </th>
                        <td colspan="2" class="fila">
                        <input type="text" name="nroPedido" size='30' value='' class='textbox'>                  </td>
                    </tr>
                    <tr>
                        <td  colspan='3' class='fila'>
                        <b> Formato de Salida:&nbsp</b>
                        <select name="displayInExcel" class='listmenu'>
                        <option value="false" selected>P&aacute;gina WEB
                        <option value="File">Archivo Excel
                        </select>                </td>
                    </tr>
                </table>
                <input type='hidden' name='nombreCliente' value=''>
            </td>
            </tr>
        </table>
        <center>
        <p>
        <table>
        <tr>
        <td  colspan="3" align="center">
            <% String onclick = "enviarFormularioDevoluciones('"+controllerAction+"')"; %>
            <tsp:boton name="returnsBtn" value="buscar" onclick="<%=onclick%>"/>
        </td>
        <td width='30'></td>
        <td>
            <tsp:boton id="botonLimpiarForma" type="reset" name="LimpiarFormularioBtn"  value="restablecer" />
        </td>
        </tr>
        </table>
        </p>
        </center>
        </tr>
        </form>
    </div>
   <% String mensaje = request.getParameter("comentario");
      if(mensaje!=null && !mensaje.equals("")){%>
    <script>
        alert(' <%=mensaje%>' );
    </script>   
   <%}%>
</body>
<tsp:InitCalendar/>
