<!--
- Autor : Ing. Ivan Dario Gomez Vanegas
- Date  : Diciembre 24 del 2005
- Copyrigth Notice : Fintravalores S.A. S.A
-->
<%--
-@(#)
--Descripcion : Pagina JSP, que muestra los datos las boleta a imprimir por oc. 
--%>

<%@page contentType="text/html"%>
<%@page session="true"%>
<%@page errorPage="/error/ErrorPage.jsp"%>
<%@page import="java.util.*"%>
<%@page import="com.tsp.operation.model.beans.*"%>
<%@page import="com.tsp.util.*"%>
<%@page import="com.tsp.operation.model.*"%>
<%@ include file="/WEB-INF/InitModel.jsp"%>

<% List	listado        = model.DescuentoTercmSvc.getListado();
   List ListValores    = model.DescuentoTercmSvc.getListValores();
   List ListaImpresion = model.DescuentoTercmSvc.getListaImpresion();
   String IMP = (request.getParameter("IMP")!= null)?request.getParameter("IMP"):"Entrar";
%>
<html>
<head>

    <title>Creacion de boletas</title>
    <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
    <link href="../css/estilostsp.css" rel="stylesheet" type="text/css">
    <link href="<%= BASEURL %>/css/estilostsp.css" rel="stylesheet" type="text/css">
    <script src='<%= BASEURL %>/js/validar.js'></script>
    <script language="JavaScript" type="text/javascript" src="<%=BASEURL%>/js/script.js"></script> 
	<script type='text/javascript' src="<%= BASEURL %>/js/boton.js"></script>
	
   <script>
      function sendBoleta(valOC){
         NuevaV('<%= CONTROLLER %>?estado=Descuento&accion=Tercm&Opcion=imprimir&OC='+ valOC,700,600,20,200);
      }
      function NuevaV(url,largo,hancho,x,y){
          ventana = window.open(url,'','width='+largo+',height='+hancho+',top='+x+',left='+y+',scrollbars=yes,resizable=yes');
          return ventana;
      }
   </script>
	
	
	
</head>
<body onResize="redimensionar()" onLoad="redimensionar();cargar();">
<div id="capaSuperior" style="position:absolute; width:100%; height:100px; z-index:0; left: 0px; top: 0px;">
<jsp:include page="/WEB-INF/toptsp.jsp?encabezado=Generacion de Boletas"/>
</div>
<% if(IMP.equals("Entrar")){
     Iterator It = listado.iterator();
      if(It.hasNext()){
       DescuentoTercm  tercm   = (DescuentoTercm)It.next();
       int VolorOc = tercm.getValor();
       

   %>
  <div id="capaCentral" style="position:absolute; width:100%; height:85%; z-index:0; left: 0px; top: 100px; overflow: scroll;"> 
    <form name="form1" method="post" action="<%= CONTROLLER %>?estado=Descuento&accion=Tercm&Opcion=guardar">
        <table width="60%" border="2" align="center">
			<tr>
                            <td>
                            <table width="99%" height="34" border="0" align="center" class="tablaInferior">
                            <tr>
                               <td width="350" height="24"  class="subtitulo1" ><p align="left">Informacion del Valor</p></td>
                                <td width="404"  class="barratitulo" ><img src="<%=BASEURL%>/images/titulo.gif" width="32" height="20"></td>
                                
			    </tr>     						
                           </table>
                           
                           <table width="99%" height="34" border="0" align="center" class="tablaInferior">
                                  <tr class="letra">
                                    <td  align="center" colspan='2'>&nbsp;&nbsp;<b>OC:</b>&nbsp;&nbsp;<%=tercm.getOC()%>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<b>Placa:</b>&nbsp;&nbsp;<%=tercm.getPlaca()%>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<b>Conductor:</b>&nbsp;&nbsp;<%=tercm.getCedula()%>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<b>Valor:</b>&nbsp;&nbsp;<%=tercm.getValor()%></td>
                                    <input type="hidden" value='<%=tercm.getOC()%>' name='OC'>
                                    </tr>
                                   <tr class="fila">
                                    <td class="letra_resaltada"  colspan='2'>&nbsp;&nbsp;</td>
                                             
                                   </tr>
                                 
                      <%for(int i=1;i<=10;i++){
                          Iterator Ite = ListValores.iterator();
                       %>
                                  
                                   <tr class="fila">
                                    <td class="letra_resaltada" width='120'>&nbsp;&nbsp;Valor Boleta <%=i%>:</td>
                                    <td width='300'>&nbsp;&nbsp;&nbsp;&nbsp;
                                       <select name='Boleta<%=i%>' onchange="Validar(form1,<%=tercm.getValor()%>,this)">
                                         <%  int j = 1;
                                             while(Ite.hasNext()){
                                             DescuentoTercm  Desc   = (DescuentoTercm)Ite.next(); 
                                             if( j == 1){ j=2;%>
                                               <option value=""></option>
                                            <%}
                                         %>
                                           <option value="<%=Desc.getValorDisp()%>"><%=Desc.getValorDisp()%> </option>
                                             <%}%>
                                       </select>
                                    </td>            
                                  </tr>
                           
                        <%}%>   
   
                                  <tr class="fila">
                                    <td class="letra_resaltada" align="right" colspan='2'>&nbsp;&nbsp;TOTAL:&nbsp;&nbsp;&nbsp;&nbsp;<input type="text" name='total' readonly></td>
                                  </tr>
                              </table>
			</td>        
		      </tr>
        	      
		</table>
        <table align="center">
          <tr align="center">
            <td colspan="2"> <img src="<%=BASEURL%>/images/botones/aceptar.gif"  name="imgaceptar" onClick="Verificar(form1,<%=VolorOc%>);" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" style="cursor:hand"> <img src="<%=BASEURL%>/images/botones/salir.gif"  name="imgsalir" onClick="parent.close();" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" style="cursor:hand"> </td>
          </tr>
        </table>
    </form>
	</div>  
    
    <%}else{%>
      <div id="capaCentral" style="position:absolute; width:100%; height:85%; z-index:0; left: 0px; top: 100px; overflow: scroll;"> 
         <table border="2" align="center">
                  <tr>
                    <td><table width="100%"  border="1" align="center"  bordercolor="#F7F5F4" bgcolor="#FFFFFF">
                       <tr>
                        <td width="229" align="center" class="mensajes">No se encontro ningun valor... 
                       </td>
                       <td width="29" background="<%=BASEURL%>/images/cuadronaranja.JPG">&nbsp;</td>
                        <td width="58">&nbsp;</td>
                      </tr>
                    </table>
                    </td>
                  </tr>
                </table>
                <center><br><img src="<%=BASEURL%>/images/botones/salir.gif"  name="imgsalir" onClick="parent.close();" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" style="cursor:hand"></center>
      </div>   
        <%}%>
  <%}else if(IMP.equals("OK")){
      Iterator Iter = ListaImpresion.iterator();
      String Total = request.getParameter("Total");
    %>
       
            <div id="capaCentral" style="position:absolute; width:100%; height:85%; z-index:0; left: 0px; top: 100px; overflow: scroll;"> 
    <form name="formularioOC" method="post" action="<%= CONTROLLER %>?estado=Descuento&accion=Tercm&Opcion=imprimir">
        <table width="60%" border="2" align="center">
			<tr>
                            <td>
                            <table width="99%" height="34" border="0" align="center" class="tablaInferior">
                            <tr>
                               <td width="350" height="24"  class="subtitulo1" ><p align="left">Informacion Para la impresion</p></td>
                                <td width="404"  class="barratitulo" ><img src="<%=BASEURL%>/images/titulo.gif" width="32" height="20"></td>
                                
			    </tr>     						
                           </table>
                          
                            <table width="99%" height="34" border="0" align="center" class="tablaInferior">
                         <%    int i =1;
                                Iterator ite = ListaImpresion.iterator();
                                if ( ite.hasNext() ){
                                    DescuentoTercm  DescTer   = (DescuentoTercm)ite.next();
                                     %><input type="hidden"  value="<%=DescTer.getOC()%>" name="OC">
                                 <%}
                              while(Iter.hasNext()){
                                DescuentoTercm  DescTer   = (DescuentoTercm)Iter.next();%>
                                <%if(i==1){ i++;%>
                                 <tr class='letra'>
                                   <td width='100%' colspan='4' align="center"><b>OC:</b>&nbsp;&nbsp;<%=DescTer.getOC()%>&nbsp;&nbsp;&nbsp;&nbsp;<b>Placa:</b>&nbsp;&nbsp;<%=DescTer.getPlaca()%>&nbsp;&nbsp;&nbsp;&nbsp;<b>Conductor:</b>&nbsp;&nbsp;<%=DescTer.getCedula()%></td>
                                   
                		 </tr>
                                <%}%>
                             <tr class='letra'>
                               <td width='100'><b>Boleta Numero:</b></td><td width='100'><%=DescTer.getBoleta()%></td><td><b>Valor:</b></td><td><%=DescTer.getValorPost()%></td>
                                
			    </tr>
     			   <%}%>
			    <tr class='letra'>
                                   <td width='100%' colspan='4' align="right"><b>TOTAL:</b>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<%=Total%>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
                	   </tr>
                           </table>
                          </td>
                        </tr>  
		</table>
        <table align="center">
          <tr align="center">
            <td colspan="2"> <img src="<%=BASEURL%>/images/botones/imprimir.gif"  name="imgaimprimir" onClick="javascript:sendBoleta(formularioOC.OC.value);parent.close();" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" style="cursor:hand"> <img src="<%=BASEURL%>/images/botones/salir.gif"  name="imgsalir" onClick="parent.close();" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" style="cursor:hand"> </td>
          </tr>
        </table>
    </form>
	</div>  
  <%}else if(IMP.equals("Existe")){%>
        <div id="capaCentral" style="position:absolute; width:100%; height:85%; z-index:0; left: 0px; top: 100px; overflow: scroll;"> 
         <table border="2" align="center">
                  <tr>
                    <td><table width="100%"  border="1" align="center"  bordercolor="#F7F5F4" bgcolor="#FFFFFF">
                       <tr>
                        <td width="229" align="center" class="mensajes">La planilla ya Existe..
                       </td>
                       <td width="29" background="<%=BASEURL%>/images/cuadronaranja.JPG">&nbsp;</td>
                        <td width="58">&nbsp;</td>
                      </tr>
                    </table>
                    </td>
                  </tr>
                </table>
                <center><br><img src="<%=BASEURL%>/images/botones/salir.gif"  name="imgsalir" onClick="parent.close();" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" style="cursor:hand"></center>
      </div>   
    <%}%>
</body>
<script>


function Validar(form, valor, cmb){
  if(( parseFloat((form.Boleta1.value == '')?0:form.Boleta1.value) + parseFloat((form.Boleta2.value == '')?0:form.Boleta2.value) +
       parseFloat((form.Boleta3.value == '')?0:form.Boleta3.value) + parseFloat((form.Boleta4.value == '')?0:form.Boleta4.value) +
       parseFloat((form.Boleta5.value == '')?0:form.Boleta5.value) + parseFloat((form.Boleta6.value == '')?0:form.Boleta6.value) +
       parseFloat((form.Boleta7.value == '')?0:form.Boleta7.value) + parseFloat((form.Boleta8.value == '')?0:form.Boleta8.value) +
       parseFloat((form.Boleta9.value == '')?0:form.Boleta9.value) + parseFloat((form.Boleta10.value == '')?0:form.Boleta10.value)
      )> valor ){
     alert('Ha excedido el valor la suma debe ser igual a : ' + valor);
     cmb.value = '';
  }
   if(( parseFloat((form.Boleta1.value == '')?0:form.Boleta1.value) + parseFloat((form.Boleta2.value == '')?0:form.Boleta2.value) +
        parseFloat((form.Boleta3.value == '')?0:form.Boleta3.value) + parseFloat((form.Boleta4.value == '')?0:form.Boleta4.value) +
        parseFloat((form.Boleta5.value == '')?0:form.Boleta5.value) + parseFloat((form.Boleta6.value == '')?0:form.Boleta6.value) +
        parseFloat((form.Boleta7.value == '')?0:form.Boleta7.value) + parseFloat((form.Boleta8.value == '')?0:form.Boleta8.value) +
        parseFloat((form.Boleta9.value == '')?0:form.Boleta9.value) + parseFloat((form.Boleta10.value == '')?0:form.Boleta10.value)
      )== valor ){
     alert('Ha llegado al limite de Boletas para esta OC');
     
   }
   
   form.total.value = (parseFloat((form.Boleta1.value == '')?0:form.Boleta1.value) + parseFloat((form.Boleta2.value == '')?0:form.Boleta2.value) +
                       parseFloat((form.Boleta3.value == '')?0:form.Boleta3.value) + parseFloat((form.Boleta4.value == '')?0:form.Boleta4.value) +
                       parseFloat((form.Boleta5.value == '')?0:form.Boleta5.value) + parseFloat((form.Boleta6.value == '')?0:form.Boleta6.value) +
                       parseFloat((form.Boleta7.value == '')?0:form.Boleta7.value) + parseFloat((form.Boleta8.value == '')?0:form.Boleta8.value) +
                       parseFloat((form.Boleta9.value == '')?0:form.Boleta9.value) + parseFloat((form.Boleta10.value == '')?0:form.Boleta10.value)); 
}



function Verificar(form, valor){
   if(( parseFloat((form.Boleta1.value == '')?0:form.Boleta1.value) + parseFloat((form.Boleta2.value == '')?0:form.Boleta2.value) +
        parseFloat((form.Boleta3.value == '')?0:form.Boleta3.value) + parseFloat((form.Boleta4.value == '')?0:form.Boleta4.value) +
        parseFloat((form.Boleta5.value == '')?0:form.Boleta5.value) + parseFloat((form.Boleta6.value == '')?0:form.Boleta6.value) +
        parseFloat((form.Boleta7.value == '')?0:form.Boleta7.value) + parseFloat((form.Boleta8.value == '')?0:form.Boleta8.value) +
        parseFloat((form.Boleta9.value == '')?0:form.Boleta9.value) + parseFloat((form.Boleta10.value == '')?0:form.Boleta10.value)
      )!= valor ){
     alert('El valor de la suma de las boletas a crear debe ser igual a: ' + valor);
     
   }else{
     form.submit();
   } 
}
</script>
</html>
