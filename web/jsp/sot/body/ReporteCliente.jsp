<!--
- Autor : LREALES
- Date  : 19 de septiembre de 2006
- Copyrigth Notice : Fintravalores S.A. S.A
-->
<%--
-@(#)
--Descripcion : Pagina JSP, que se encarga de mostrar el reporte de proveedores de fintra
--%>
<%--
The taglib directive below imports the JSTL library. If you uncomment it,
you must also add the JSTL library to the project. The Add Library... action
on Libraries node in Projects view can be used to add the JSTL 1.1 library.
--%>
<%-- Declaracion de librerias--%>
<%@page session="true"%>
<%@page errorPage="../error/ErrorPage.jsp"%>
<%@page import="com.tsp.operation.model.beans.*, java.util.*, java.text.*"%>
<%@ include file="/WEB-INF/InitModel.jsp"%>
<%@ taglib uri="http://jakarta.apache.org/taglibs/input-0.90" prefix="input" %>
<%@page import="com.tsp.util.*"%>
<% Util u = new Util(); %>
<%@page contentType="text/html"%>
<%@page import="java.util.*"%> 
<%@page import="com.tsp.operation.model.*"%>
<%@taglib uri="http://jsptags.com/tags/navigation/pager" prefix="pg" %>
<html>
    <head>        
        <title>Reporte Clientes </title>
		<script src='<%=BASEURL%>/js/date-picker.js'></script>
		<script language="JavaScript" type="text/javascript" src="<%=BASEURL%>/js/boton.js"></script>
		<script type='text/javascript' src="<%=BASEURL%>/js/validar.js"></script> 
		<link href="file:///C|/sltequipo%20viejo/slt1/jsp/sot/css/estilostsp.css" rel="stylesheet" type="text/css">
		<link href="<%=BASEURL%>/css/estilostsp.css" rel="stylesheet" type="text/css">
		 <script>
		 	function Exportar (){
				
			    alert( 
				
					'Puede observar el seguimiento del proceso en la siguiente ruta del menu:' + '\n' + '\n' +
					'SLT -> PROCESOS -> Seguimiento de Procesos' + '\n' + '\n' + '\n' +
					'Y puede observar el archivo excel generado, en la siguiente parte:' + '\n' + '\n' +
					'En la pantalla de bienvenida de la Pagina WEB,' + '\n' +
					'vaya a la parte superior-derecha,' + '\n' +
					'y presione un click en el icono que le aparece.'
					
				);
				
			}
		   function abrirVentanaBusq( an, al, url, pag ) {
			
        		parent.open( url, 'Conductor', 'width=' + an + ', height=' + al + ',scrollbars=no,resizable=no,top=10,left=65,status=yes' );
				
    		}
	
		</script>
    </head>
   <body onResize="redimensionar()" onLoad="redimensionar()">
   <%  String path    = application.getRealPath(model.menuService.getRealUrl(request.getRequestURI()));
    String datos[] = model.menuService.getContenidoMenu(BASEURL,request.getRequestURI(),path);    
	
	String Codig=(request.getParameter ("Codigo")!=null)?request.getParameter ("Codigo"):"";
	String Nombr =(request.getParameter ("Nombre")!=null)?request.getParameter ("Nombre"):"";
	String Ni =(request.getParameter ("Nit")!=null)?request.getParameter ("Nit"):"";
	String agenci =(request.getParameter ("agencia")!=null)?request.getParameter ("agencia"):"";
	String Pai =(request.getParameter ("pais")!=null)?request.getParameter ("pais"):"";
	%>
	


	<div id="capaSuperior" style="position:absolute; width:100%; height:100px; z-index:0; left: 1px; top: 0px;">
	 <jsp:include page="/toptsp.jsp?encabezado=Reporte Clientes"/>
	</div>
	<div id="capaCentral" style="position:absolute; width:100%; height:87%; z-index:0; left: 0px; top: 100px; overflow: scroll;">
	<%String msg = request.getParameter("msg");
		if ( msg!=null){%>	
 <br>
  <table border="2" align="center">
  <tr>
    <td><table width="100%"  border="1" align="center"  bordercolor="#F7F5F4" bgcolor="#FFFFFF">
      <tr>
        <td width="300" align="center" class="mensajes">La busqueda no arrojo resultados</td>
        <td width="29" background="<%=BASEURL%>/images/cuadronaranja.JPG">&nbsp;</td>
        <td width="58">&nbsp;</td>
      </tr>
	  
    </table></td>
  </tr>
</table>
      <img src="<%=BASEURL%>/images/botones/regresar.gif" name="c_regresar" onclick="location.href='<%=BASEURL%>/jsp/sot/body/ClientesBuscar.jsp'"  onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" style="cursor:hand">
	  <input name="salir2" src="<%=BASEURL%>/images/botones/salir.gif" type="image" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" style="cursor:hand" class="boton" onclick="window.close();">
   
 <%}else{
        Vector datosC =model.clienteService.getVector();
		String style = "simple";
		String position =  "bottom";
		String index =  "center";
		int maxPageItems = 20;
		int maxIndexPages = 10;
		if( datos != null && datosC.size() > 0 ){
			BeanGeneral beanGeneral = (BeanGeneral) datosC.elementAt(0);
		%>		
   <form name="form1" method="post" action="<%=CONTROLLER%>?estado=Cliente&accion=Buscar&opcion=2">
              <p>
	    <input name="Nombre" type="hidden" id="Nombre" value="<%=Nombr%>">
	    </p>
              <p>
	      <input name="Codigo" type="hidden" id="Codigo" value="<%=Codig%>">
              </p>
              <p><input name="Nit" type="hidden" id="Nit" value="<%=Ni%>">
	        </p>
              <p>
            <input name="agencia" type="hidden" id="agencia" value="<%=agenci%>">
			<input name="Pais" type="hidden" id="Pais" value="<%=Pai%>">
          </p>
              <p>
                <input name="Guardar2" src="<%=BASEURL%>/images/botones/exportarExcel.gif" type="image" class="boton" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" style="cursor:hand" onclick="Exportar();">
                <img src="<%=BASEURL%>/images/botones/regresar.gif" name="c_regresar" onclick="location.href='<%=BASEURL%>/jsp/sot/body/ClientesBuscar.jsp'"  onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" style="cursor:hand">
 	            <input name="salir" src="<%=BASEURL%>/images/botones/salir.gif" type="image" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" style="cursor:hand" class="boton" onclick="window.close();">
          </p>
   </form>
  <table  width="100%">  
           <tr>
				<td class="barratitulo" colspan='2' >
					   <table cellpadding='0' cellspacing='0' width='100%'>
							 <tr class="fila">
							  <td width="4%" align="left" class="subtitulo1">&nbsp;<strong>Consulta de Clientes</strong></td>
								 <td width="31%" class="subtitulo1"  align="left">&nbsp;</td>
								
							     <td width="65%" class="fila"  align="left"><img src="<%= BASEURL %>/images/titulo.gif" width="32" height="20"  align="left"><%=datos[0]%></td>
						    </tr>
						  <%if (!Codig.equals("")){%>
					
						  <tr  class="letra">
								   <td width='4%' class="fila" ><div align="left"><strong> Codigo   : </strong></div></td>
							   <td colspan="2" class="fila" ><div align="left"><%=Codig%></div></td>
						  </tr>
						<%}%>                
						<%if (!Ni.equals("")){%>              
							   <tr>
								 <td width='4%' class="fila" ><div align="left"><strong> Nit       : </strong></div></td>
								 <td colspan="2" class="fila"><div align="left"><%=Ni%></div></td>
							   </tr>
						<%}%>
						<%if (!Nombr.equals("")){%> 
							   <tr>
								 <td width='4%' class="fila" ><div align="left"><strong> Nombre    : </strong></div></td>
								 <td colspan="2" class="fila"><div align="left"><%=Nombr%></div></td>
							   </tr>
						<%}%>                
						<%if (!agenci.equals("")){%>
							   <tr>
								 <td width='4%' class="fila" ><div align="left"><strong> Agencia  : </strong></div></td>
								 <td colspan="2" class="fila"><div align="left"><%=(beanGeneral.getValor_05()!=null)?beanGeneral.getValor_05():""%></div></td>
							   </tr>
						<%}%>
						<%if (!Pai.equals("")){%>
							   <tr>
								 <td width='4%' class="fila" ><div align="left"><strong> Pais  : </strong></div></td>
								 <td colspan="2" class="fila"><div align="left"><%=(beanGeneral.getValor_45()!=null)?beanGeneral.getValor_45():""%></div></td>
							   </tr>
						<%}%>
					   
              			 <tr class="fila">
							 <td width="4%" align="left" class="subtitulo1"><strong>Clientes</strong></td>
							 <td width="31%"   align="left" class="subtitulo1">&nbsp;</td>
						     <td width="65%"   align="left" class="fila"><img src="<%= BASEURL %>/images/titulo.gif" width="32" height="20"  align="left"></td>
              			 </tr>
			  </table>   
			</td>
        </tr>
          <tr>
            <td>
              <table width="100%"  border="1" bordercolor="#999999" bgcolor="#F7F5F4" >
                <tr class="tblTitulo" align="center">
				  <td nowrap width="5%" align="center">ITEM</td>
				  <td nowrap width="5%" align="center">ESTADO</td>
				  <%if (agenci.equals("")){%>
				  	<td nowrap width="5%" align="center">AGENCIA DUE&Ntilde;A </td>
				    <%}%>
                  <td nowrap width="5%" align="center">CODIGO DEL CLIENTE</td>
                  <td nowrap width="5%" align="center">NOMBRE DEL CLIENTE</td>
				  <td nowrap width="5%" align="center">DIRECCION DEL CLIENTE</td>
				  <td nowrap width="5%" align="center">CIUDAD DEL CLIENTE</td>
				  <td nowrap width="5%" align="center">PAIS DEL CLIENTE</td>
				  <td nowrap width="5%" align="center">TELEFONO DEL CLIENTE</td>
				  <td nowrap width="5%" align="center">NOMBRE DEL CONTACTO</td>
				  <td nowrap width="5%" align="center">TELEFONO DEL CONTACTO</td>
				  <td nowrap width="5%" align="center">DIRECCION DEL CONTACTO</td>
				  <td nowrap width="5%" align="center">E-MAIL DEL CONTACTO</td>
		          <td nowrap width="5%" align="center">NOTAS DEL CLIENTE </td>
				  <td nowrap width="5%" align="center">OBSERVACION DE FACTURACION </td>
                  <td nowrap width="5%" align="center">NIT DEL CLIENTE</td>
                  <td nowrap width="5%" align="center">CEDULA DEL AGENTE</td>
				  <td nowrap width="5%" align="center">RENTABILIDAD</td>
				  <td nowrap width="5%" align="center">DIA LIMITE DEL MES PARA RECIBIR FACTURA </td>
		          <td nowrap width="5%" align="center">SOPORTE DE FACTURACION</td>
				  <td nowrap width="5%" align="center">FIDUCIARIA</td>
				  <td nowrap width="5%" align="center">AGENCIA DE COBRO</td>
                  <td nowrap width="5%" align="center">DISTRITO CLIENTE</td>
                  <td nowrap width="5%" align="center">MONEDA FACTURA DEL CLIENTE</td>
                  <td nowrap width="5%" align="center">FORMA DE PAGO</td>
                  <td nowrap width="5%" align="center">PLAZO DE PAGO</td>    
				  <td nowrap width="5%" align="center">BANCO DE CONSIGNACION</td>    
				  <td nowrap width="5%" align="center">UNIDAD CONTABLE ASOCIADA</td>
                  <td nowrap width="5%" align="center">CODIGO DEL IMPUESTO DEL CLIENTE</td>
                  <td nowrap width="5%" align="center">AGENCIA DE FACTURACION</td>
				  <td nowrap width="5%" align="center">DIRECCION DE FACTURACION</td> 
				  <td nowrap width="5%" align="center">CIUDAD DE FACTURACION</td> 
				  
				  <td nowrap width="5%" align="center">TIEMPO DE ELABORACION DE PREFACTURA</td> 
				  <td nowrap width="5%" align="center">TIEMPO DE LEGALIZACION DE FACTURA</td>
				  <td nowrap width="5%" align="center">HC</td> 
				  <td nowrap width="5%" align="center">RIF</td>       
                </tr>	
				<pg:pager
				 items="<%=datosC.size()%>"
				 index="<%= index %>"
				 maxPageItems="<%= maxPageItems %>"
				 maxIndexPages="<%= maxIndexPages %>"
				 isOffset="<%= true %>"
				 export="offset,currentPageNumber=pageNumber"
				 scope="request">					
		<%
			int cont = 1;
			for (int i = offset.intValue(), l = Math.min(i + maxPageItems, datosC.size()); i < l; i++){
				BeanGeneral info = (BeanGeneral) datosC.elementAt(i);
	  %>  	  
	  			
	  			<pg:item>
                  <tr class="<%=i%2==0?"filagris":"filaazul"%>" onMouseOver='cambiarColorMouse(this)' style="cursor:hand" title="Click" align="center" onClick="window.open('<%=CONTROLLER%>?estado=Menu&accion=Cargar&opcion=10&op=1&codcli=<%=info.getValor_02()%>&marco=no' ,'M','status=yes,scrollbars=no,width=780,height=650,resizable=yes');"> 
				  <td nowrap align="left" class="bordereporte"><%=cont%>&nbsp;</td>
				  <td nowrap align="left" class="bordereporte"><%=(info.getValor_01()!=null)?info.getValor_01():""%>&nbsp;</td>
                	<%if (agenci.equals("")){%>
						<td nowrap align="left" class="bordereporte"><%=(info.getValor_05()!=null)?info.getValor_05():""%>&nbsp;</td> 
					<%}%> 
				  <td nowrap align="left" class="bordereporte"><%=(info.getValor_02()!=null)?info.getValor_02():""%>&nbsp;</td>
                  <td nowrap align="left" class="bordereporte"><%=(info.getValor_03()!=null)?info.getValor_03():""%>&nbsp;</td>
                  <td nowrap align="left" class="bordereporte"><%=(info.getValor_26()!=null)?info.getValor_26():""%>&nbsp;</td>
				  <td nowrap align="left" class="bordereporte"><%=(info.getValor_43()!=null)?info.getValor_43():""%>&nbsp;</td>
				  <td nowrap align="left" class="bordereporte"><%=(info.getValor_45()!=null)?info.getValor_45():""%>&nbsp;</td>
				  <td nowrap align="left" class="bordereporte"><%=(info.getValor_27()!=null)?info.getValor_27():""%>&nbsp;</td>
                  <td nowrap align="left" class="bordereporte"><%=(info.getValor_28()!=null)?info.getValor_28():""%>&nbsp;</td>
                  <td nowrap align="left" class="bordereporte"><%=(info.getValor_29()!=null)?info.getValor_29():""%>&nbsp;</td>
				  <td nowrap align="left" class="bordereporte"><%=(info.getValor_39()!=null)?info.getValor_39():""%>&nbsp;</td>
                  <td nowrap align="left" class="bordereporte"><%=(info.getValor_30()!=null)?info.getValor_30():""%>&nbsp;</td>
				  <td nowrap align="left" class="bordereporte"><%=(info.getValor_04()!=null)?info.getValor_04():""%>&nbsp;</td>
                  <td nowrap align="left" class="bordereporte"><%=(info.getValor_07()!=null)?info.getValor_07():""%>&nbsp;</td>
                  <td nowrap align="left" class="bordereporte"><%=(info.getValor_08()!=null)?info.getValor_08():""%>&nbsp;</td>
                  <td nowrap align="left" class="bordereporte"><%=(info.getValor_09()!=null)?info.getValor_09():""%>&nbsp;</td>
                  <td nowrap align="left" class="bordereporte"><%=(info.getValor_10()!=null)?info.getValor_10():""%>&nbsp;</td>
                  <td nowrap align="left" class="bordereporte"><%=(info.getValor_36()!=null)?info.getValor_36():""%>&nbsp;</td>   
				  <td nowrap align="left" class="bordereporte"><%=(info.getValor_11()!=null)?info.getValor_11():""%>&nbsp;</td> 
				  <td nowrap align="left" class="bordereporte"><%=(info.getValor_12()!=null)?info.getValor_12():""%>&nbsp;</td>         
				  <td nowrap align="left" class="bordereporte"><%=(info.getValor_40()!=null)?info.getValor_40():""%>&nbsp;</td>
				  <td nowrap align="left" class="bordereporte"><%=(info.getValor_13()!=null)?info.getValor_13():""%>&nbsp;</td>
                  <td nowrap align="left" class="bordereporte"><%=(info.getValor_14()!=null)?info.getValor_14():""%>&nbsp;</td>
                  <td nowrap align="left" class="bordereporte"><%=(info.getValor_15()!=null)?info.getValor_15():""%>&nbsp;</td>   
				  <td nowrap align="left" class="bordereporte"><%=(info.getValor_16()!=null)?info.getValor_16():""%>&nbsp;</td>
				  
				  <td nowrap align="left" class="bordereporte"><%=(info.getValor_18()!=null)?info.getValor_18():""%>&nbsp;</td>
                  <td nowrap align="left" class="bordereporte"><%=(info.getValor_21()!=null)?info.getValor_21():""%>&nbsp;</td>   
				  <td nowrap align="left" class="bordereporte"><%=(info.getValor_22()!=null)?info.getValor_22():""%>&nbsp;</td> 
				  <td nowrap align="left" class="bordereporte"><%=(info.getValor_23()!=null)?info.getValor_23():""%>&nbsp;</td>         
				  <td nowrap align="left" class="bordereporte"><%=(info.getValor_31()!=null)?info.getValor_31():""%>&nbsp;</td>
				  <td nowrap align="left" class="bordereporte"><%=(info.getValor_44()!=null)?info.getValor_44():""%>&nbsp;</td>   
                  <td nowrap align="left" class="bordereporte"><%=(info.getValor_35()!=null)?info.getValor_35():""%>&nbsp;</td>   
				  <td nowrap align="left" class="bordereporte"><%=(info.getValor_34()!=null)?info.getValor_34():""%>&nbsp;</td>
				  <td nowrap align="center" class="bordereporte"><%=(info.getValor_41()!=null)?info.getValor_41():""%>&nbsp;</td>
				  <td nowrap align="center" class="bordereporte"><%=(info.getValor_42()!=null)?info.getValor_42():""%>&nbsp;</td>	
                </tr>
			  </pg:item>	
		<%cont = cont + 1; 
		}%>
		<tr class="bordereporte">
          <td td height="20" colspan="10" nowrap align="center">
		   <pg:index>
            <jsp:include page="/WEB-INF/jsp/googlesinimg.jsp" flush="true"/>      
           </pg:index> 
	      </td>
        </tr>
        </pg:pager>
            </table>
      </table>
			</table>
		   <form name="form1" method="post" action="<%=CONTROLLER%>?estado=Cliente&accion=Buscar&opcion=2">
		           <p>
	     <input name="Nombre" type="hidden" id="Nombre" value="<%=Nombr%>">
	    </p>
              <p>
	      <input name="Codigo" type="hidden" id="Codigo" value="<%=Codig%>">
              </p>
              <p><input name="Nit" type="hidden" id="Nit" value="<%=Ni%>">
	        </p>
              <p>
            <input name="agencia" type="hidden" id="agencia" value="<%=agenci%>">
			<input name="Pais" type="hidden" id="Pais" value="<%=Pai%>">
          </p>
            
	  <input name="Guardar3" src="<%=BASEURL%>/images/botones/exportarExcel.gif" type="image" class="boton" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" style="cursor:hand" onclick="Exportar();" style="cursor:hand" onClick="form1.submit();">
      <img src="<%=BASEURL%>/images/botones/regresar.gif" name="c_regresar" onclick="location.href='<%=BASEURL%>/jsp/sot/body/ClientesBuscar.jsp'"  onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" style="cursor:hand">
	  <input name="salir2" src="<%=BASEURL%>/images/botones/salir.gif" type="image" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" style="cursor:hand" class="boton" onclick="window.close();">
    </form>	
	  <%}%>	  
	  
 <%}%>
	  
</body>
</div>
<%=datos[1]%>  
</html>
