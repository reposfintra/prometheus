<!--
- Autor : Alejandro Payares
- Date  : Diciembre 24 del 2005
- Copyrigth Notice : Fintravalores S.A. S.A
- Descripcion : Pagina JSP que permite crear una nueva tabla dentro de tablagen.
-->
<%@page import="com.tsp.operation.model.beans.*, java.util.*"%>
<%@include file="/WEB-INF/InitModel.jsp" %>
<%@ taglib uri="http://jakarta.apache.org/taglibs/input-0.90" prefix="input" %>
<%@ taglib uri="http://www.sanchezpolo.com/sot" prefix="tsp"%>
<%
    String path = application.getRealPath(model.menuService.getRealUrl(request.getRequestURI()));
    String datos[] = model.menuService.getContenidoMenu(BASEURL,request.getRequestURI(),path);
%>
<html>
    <head> 
        <title>.: ADMINISTRACIÓN DE TABLAGEN :.</title>
        <script language="javascript" src="<%=BASEURL%>/jsp/sot/js/PantallaTablaGenNueva.js"></script>
        <script language="javascript" src="<%=BASEURL%>/js/tools.js"></script>
        <script language="javascript" src="<%=BASEURL%>/js/boton.js"></script>
        <link href="<%=BASEURL%>/css/estilostsp.css" rel="stylesheet" type="text/css">
    </head>
    <body onresize="redimensionar()" onload='redimensionar()'>
        <div id="capaSuperior" style="position:absolute; width:100%; z-index:0; left: 0px; top: 0px;">
            <jsp:include page="/toptsp.jsp?encabezado=ADMINISTRACIÓN DE TABLAGEN"/>
        </div>
        <div id="capaCentral" style="position:absolute; width:100%; height:87%; z-index:0; left: 0px; top: 100px; overflow: scroll;"> 
            <br>
            <%if(request.getParameter("mensaje") != null){%>
            <table border="2" align="center">
                <tr>
                    <td><table width="100%"  border="1" align="center"  bordercolor="#F7F5F4" bgcolor="#FFFFFF">
                        <tr>
                            <td width="229" align="center" class="mensajes"><%=request.getParameter("mensaje")%></td>
                            <td width="29" background="<%=BASEURL%>/images/cuadronaranja.JPG">&nbsp;</td>
                            <td width="58">&nbsp;</td>
                        </tr>
                    </table></td>
                </tr>
            </table>
            <br>
            <%}
            String cmd = request.getParameter("titulo") != null && request.getParameter("titulo").toLowerCase().startsWith("editar")?"modificar":"agregar";
            String action = CONTROLLER+"?estado=TablaGen&accion=Manager&cmd="+cmd+"Tabla"; %>
            <input:form bean="bean" name='forma' method="post"  action="<%=action%>">
                <input type='hidden' name='old_type' value='<%=request.getParameter("old_type")%>'/>
                <table align='center' border = '1'>
                    <tr>
                        <td width='500px'>
                        <table width='100%'>
                            <tr align='center'>
                                <td colspan="2" align="center" class="subtitulo1"><%=request.getParameter("titulo")%><br></td>
                                <td width="350" align="left" class="barratitulo"><img src="<%=BASEURL%>/images/titulo.gif" width="32" height="20" align="left"><%=datos[0]%></td>
                            </tr>
                            <tr class="fila">
                                <td width='130'>Nombre</td>
                                <td colspan="3">
                                <table border='0' width='100%'>
                                	<tr>
                                	<td colspan='2'>
                                    	<input:text name="table_type" attributesText="maxlength='10' style='width:100%' style='text-transform:uppercase'"/>
									</td>
									<td>
										<img src='<%=BASEURL%>/images/botones/iconos/obligatorio.gif'>
									</td>
									</tr>
                        		</table>
                        		</td>
                    		</tr>
                    		<tr class="fila">
                        		<td>Descripci&oacute;n</td>
								<td colspan="3">
								<table border='0' width='100%'>
									<tr>
									<td width='99%'>
										<input:text name="descripcion" attributesText="style='width:100%'"/>
									</td>
									<td >
										<img src='<%=BASEURL%>/images/botones/iconos/obligatorio.gif'>
									</td>
									</tr>
                        		</table>
                    			</td>
                    		</tr>
							<tr class="fila">
                        		<td>Observaciones Generales</td>
								<td colspan="3">
								<table border='0' width='100%'>
									<tr>
									<td width='99%'>
										<input:textarea name="dato" attributesText="style='width:100%'"/>
									</td>
									<td >
										<img src='<%=BASEURL%>/images/botones/iconos/obligatorio.gif'>
									</td>
									</tr>
                        		</table>
                    			</td>
                    		</tr>
						</table>
                	</td>
                	</tr>
                </table>
            </input:form>
            <p>
            <center>
                <table>
                    <tr>
                    <td>
                        <% String op= cmd.equals("agregar")?"aceptar":cmd ; %>
                        <tsp:boton value="<%=op%>" onclick="enviarFormNuevaTabla()"/>
                    </td>
                    <td width='5'>&nbsp;</td>
                    <td>
                        <% String onclick = "location.href='"+CONTROLLER+"?estado=TablaGen&accion=Manager'"; %>
                        <tsp:boton value="detalles" onclick="<%=onclick%>"/>
                    </td>
                    <td width='5'>&nbsp;</td>
                    <td>
                        <tsp:boton value="cancelar" onclick="document.forma.reset()"/>
                    </td>
                    <td width='5'>&nbsp;</td>
                    <td>
                        <tsp:boton value="salir" onclick="window.close()"/>
                    </td>
                </tr>
                </table>
            </center>
            </p>
        </div>
        <% String str = datos[1];
           str = str.replaceAll("PantallaTablaGenNuevaHelpD.jsp","PantallaTablagenListHelpD.jsp#"+cmd+"Tabla");
           str = str.replaceAll("PantallaTablaGenNuevaHelpF.jsp","PantallaTablagenListHelpF.jsp#"+cmd+"Tabla");
           out.println(str);
        %>
    </body>
    
</html>
