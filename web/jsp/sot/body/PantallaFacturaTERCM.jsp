<!--
- Autor : Ing. Ivan Dario Gomez Vanegas
- Date  : 12 de Enero de 2006
- Copyrigth Notice : Fintravalores S.A. S.A
-->
<%--
-@(#)
--Descripcion : Pagina JSP, que maneja el reporte de las facturas para los descuentos 
                por concepto de combustible
--%>

<%-- Declaracion de librerias--%>
<%@page contentType="text/html"%>
<%@page session="true"%>
<%@page errorPage="../error/ErrorPage.jsp"%>
<%@page import="java.util.*"%>
<%@page import="com.tsp.operation.model.*"%>
<%@page import="com.tsp.util.*"%>
<%@page import="com.tsp.operation.model.beans.*, java.util.*"%>
<%@include file="/WEB-INF/InitModel.jsp"%>
<%@ taglib uri="http://jakarta.apache.org/taglibs/input-0.90" prefix="input" %>
<html>
<head>
<title>Consulta De Prefacturas</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link href="<%= BASEURL %>/css/estilostsp.css" rel="stylesheet" type="text/css"> 
<link href="../../../css/estilostsp.css" rel="stylesheet" type="text/css"> 
<script src='<%=BASEURL%>/js/date-picker.js'></script>
<script type='text/javascript' src="<%= BASEURL %>/js/validar.js"></script>
<script type='text/javascript' src="<%= BASEURL %>/js/boton.js"></script>
<script type='text/javascript' src="<%= BASEURL %>/js/reporte.js"></script>
<script>
  function validar(){
     var sel = document.getElementsByName('selec');
     for (i=0;i<sel.length;i++){
        if (sel[i].checked){
           form.submit();
           return true;
        }
     }
     
     alert ('Seleccione alguno, para poder continuar.')
     return false;        
     
  }
 
  
</script>
</head>
<%List listado =  model.DescuentoTercmSvc.getPrefacturaCreada();
  String OP  =  (request.getParameter("OP") != null)?request.getParameter("OP"):"";
  String Prefac ="";%>
<%-- Inicio Body --%>
<body onLoad="redimensionar();" onResize="redimensionar();">
	<div id="capaSuperior" style="position:absolute; width:100%; height:100px; z-index:0; left: 0px; top: 0px;">
	<jsp:include page="/WEB-INF/toptsp.jsp?encabezado=Modificacion Prefactura combustible TERCM"/>
	</div>
	<%if(OP.equals("Listar")){
             Iterator It = listado.iterator();
              if(listado.size()>0){
                  %>
             <div id="capaCentral" style="position:absolute; width:100%; height:85%; z-index:0; left: 0px; top: 100px; overflow: scroll;"> 
                <form name="form" method="post" action="<%=CONTROLLER%>?estado=Descuento&accion=Tercm&Opcion=marcar">
                  <table width="416" border="2" align="center">
                      <tr>
                      <td><input type="hidden" name="exportar"  value ='NO'>
                          <table width="100%" align="center">
                              <tr>
                                  <%Iterator ite = listado.iterator();
                                    if(ite.hasNext()){
                                         DescuentoTercm  ter   = (DescuentoTercm)ite.next();
                                         Prefac = ter.getPrefactura(); 
                                     }
                                    %>
                                    
                                  <td width="400" class="subtitulo1">&nbsp; Prefactura <%=Prefac%>  </td><input type="hidden" name="NumPrefactura" value="<%=Prefac%>">
                                  <td width="427" class="barratitulo"><img src="<%=BASEURL%>/images/titulo.gif" width="32" height="20"></td>
                              </tr>
                          </table>
                          <table width="100%" border="1" bordercolor="#999999" bgcolor="#F7F5F4" align="center">
                          <tr class="tblTitulo" align="center">
                              <td width="69" nowrap >Boleta #</td>
                              <td width="200" nowrap >Fecha de Impresion</td>
                              <td width="45" nowrap >Placa</td>
                              <td width="45" nowrap >planilla</td>
                              <td width="45" nowrap >Valor</td>
                              <td width="45" nowrap >Excluir</td>
                          </tr>
                         <%int i = 0;
                            while(It.hasNext()){
                           DescuentoTercm  tercm   = (DescuentoTercm)It.next();%>
                              <tr class="<%=(i % 2 == 0 )?"filagris":"filaazul"%>" onMouseOver='cambiarColorMouse(this)'   style="cursor:hand" >
                                  <td align="center" class="bordereporte"><%=tercm.getBoleta()%></td>
                                  <td align="center" class="bordereporte"><%=tercm.getFecha()%></td>
                                  <td align="center" class="bordereporte"><%=tercm.getPlaca()%></td>
                                  <td align="center" class="bordereporte"><%=tercm.getOC()%></td>
                                  <td align="center" class="bordereporte"><%=tercm.getValorPost()%></td>
                                  <td align="center" class="bordereporte"><input type="checkbox" name='selec' value="<%=tercm.getBoleta()%>"></td>
                              </tr>
                              <%i++;}%>
                      </table>
                      </td>
                      </tr>
                  </table><br>
                   <center>
                        <img src="<%=BASEURL%>/images/botones/modificar.gif" name="imgamodificar"  height="21" onClick="validar()" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" style="cursor:hand"> &nbsp; 
			<img src="<%=BASEURL%>/images/botones/exportarExcel.gif" name="imgaceptar"  height="21" onClick="form.exportar.value ='SI';form.submit();" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" style="cursor:hand"> &nbsp; 
			<img src="<%=BASEURL%>/images/botones/salir.gif"  name="imgsalir"  height="21" onMouseOver="botonOver(this);" onClick="parent.close();" onMouseOut="botonOut(this);" style="cursor:hand">&nbsp;
		    </form>	
		   </center>	
              </div>
                  
	     <%}else{%>
	            <div id="capaCentral" style="position:absolute; width:100%; height:85%; z-index:0; left: 0px; top: 100px; overflow: scroll;"> 
                        <table border="2" align="center">
                            <tr>
                                <td><table width="100%"  border="1" align="center"  bordercolor="#F7F5F4" bgcolor="#FFFFFF">
                                <tr>
                                <td width="229" align="center" class="mensajes">No se encontraron registros... 
                                </td>
                                <td width="29" background="<%=BASEURL%>/images/cuadronaranja.JPG">&nbsp;</td>
                                <td width="58">&nbsp;</td>
                            </tr>
                            </table>
                            </td>
                            </tr>
                        </table>
                        <center><br><img src="<%=BASEURL%>/images/botones/salir.gif"  name="imgsalir" onClick="parent.close();" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" style="cursor:hand"></center>
	            </div> 
	       <%}%>
	
	<%}else{
             
         if(!OP.equals("mensage")){%>
	<div id="capaCentral" style="position:absolute; width:100%; height:85%; z-index:0; left: 0px; top: 100px; overflow: scroll;">
		<form name="forma" method="post" action="<%=CONTROLLER%>?estado=Descuento&accion=Tercm&Opcion=BuscarPrefactura">
		<table width="500" border="2" align="center">
			<tr>
				<td>
					<table width="500" align="center" class="tablaInferior">
						<tr class="fila">
							<td width="242" align="left" class="subtitulo1">&nbsp;Creacion de facturas</td>
							<td width="242" align="left" class="barratitulo"><img src="<%=BASEURL%>/images/titulo.gif" width="32" height="20"></td>
						</tr>
					</table>
	                                <table width="100%" border="0" align="center" class="tablaInferior">
						<tr class="fila">
                                                <td width='180'><strong>&nbsp;Numero de Prefactura </strong></td>
                                                <td>
                                                  <input name='NumPrefactura' type='text' class="textbox"  style='width:120' value='' maxlength='8' >
                                              </td>
                                              </tr>
                                             
					</table>
				</td>
			</tr>
		</table>
		<div align="center"><br>
			<img src="<%=BASEURL%>/images/botones/aceptar.gif" name="imgaceptar"  height="21" onClick="forma.submit();" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" style="cursor:hand"> &nbsp; 
			<img src="<%=BASEURL%>/images/botones/salir.gif"  name="imgsalir"  height="21" onMouseOver="botonOver(this);" onClick="parent.close();" onMouseOut="botonOut(this);" style="cursor:hand">&nbsp;
		</div>
		</form>
            <%}else{%>
                 <div id="capaCentral" style="position:absolute; width:100%; height:85%; z-index:0; left: 0px; top: 100px; overflow: scroll;"> 
                        <table border="2" align="center">
                            <tr>
                                <td><table width="100%"  border="1" align="center"  bordercolor="#F7F5F4" bgcolor="#FFFFFF">
                                <tr>
                                <td width="229" align="center" class="mensajes">El archivo fue enviado al log de procesos... 
                                </td>
                                <td width="29" background="<%=BASEURL%>/images/cuadronaranja.JPG">&nbsp;</td>
                                <td width="58">&nbsp;</td>
                            </tr>
                            </table>
                            </td>
                            </tr>
                        </table>
                        <center><br><img src="<%=BASEURL%>/images/botones/salir.gif"  name="imgsalir" onClick="parent.close();" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" style="cursor:hand"></center>
                        
	            </div> 
            
		<%}%>
 	<%}%>
	<iframe width="188" height="166" name="gToday:normal:agenda.js:gfPop:plugins.js" id="gToday:normal:agenda.js:gfPop:plugins_24.js" src="<%=BASEURL%>/js/Calendario/ipopeng.htm" scrolling="no" frameborder="0" style="visibility:visible; z-index:999; position:absolute; left:-500px; top:0px;">
                        </iframe>	
 </div>
</body>
</html>

