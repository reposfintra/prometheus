<!--  
	 - Author(s)       :      FFERNANDEZ
	 - Description     :      AYUDA FUNCIONAL - CLIENTE BUSCAR
	 - Date            :      3/11/2006
	 - Copyright Notice:      TRANSPORTES SANCHEZ POLO S.A.
-->
<%@ include file="/WEB-INF/InitModel.jsp"%>
<HTML>
<HEAD>
<TITLE>Funcionalidad del Adicionar Cliente</TITLE>
<META http-equiv=Content-Type content="text/html; charset=windows-1252">
<link href="/../../../css/estilostsp.css" rel="stylesheet" type="text/css"> 
<link href="<%=BASEURL%>/css/estilostsp.css" rel="stylesheet" type="text/css">
<script type='text/javascript' src="<%= BASEURL %>/js/boton.js"></script> 
</HEAD>
<BODY> 
<% String BASEIMG =  BASEURL +"/images/ayuda/Cliente/"; %>
  <table width="95%"  border="2" align="center">
    <tr>
      <td height="117" >
        <table width="100%" border="0" align="center">
          <tr  class="subtitulo">
            <td height="20"><div align="center">MANUAL DE REPORTE WEB</div></td>
          </tr>
          <tr class="subtitulo1">
            <td>Descripci&oacute;n del funcionamiento del programa Buscar Cliente </td>
          </tr>
          <tr>
            <td  class="ayudaHtmlTexto"><div align="center"></div></td>
          </tr>
<tr>
            <td  class="ayudaHtmlTexto"><p class="ayudaHtmlTexto">&nbsp;</p>
            <p class="ayudaHtmlTexto">En la siguiente pantalla  muestra los campos de filtros para buscar un cliente o los clientes dependiendo:</p>
            <p class="ayudaHtmlTexto">Codigo: ingrese el codigo del cliente para su busqueda de informacion</p>
            <p class="ayudaHtmlTexto">Cedula:  ingrese la Cedula del cliente para su busqueda de informacion </p>
            <p class="ayudaHtmlTexto"> Nombre :ingrese el Nombre del cliente para su busqueda de informacion </p>
            <p class="ayudaHtmlTexto">Agencia :  ingrese la agencia de los clientes para su listado de busqueda de informacion</p></td>
          </tr>
<tr>
            <td  class="ayudaHtmlTexto"><div align="center"><IMG height=197 src="<%=BASEIMG%>image011.JPG" width=463 border=0 v:shapes="_x0000_i1143"></div></td>
          </tr>
<tr>
  <td  class="ayudaHtmlTexto"><p>&nbsp;</p>
    <p>El sistema le muestra un Mensaje Cuando no muestra ningun resultado de la Busqueda </p></td>
</tr>
<tr>
  <td  class="ayudaHtmlTexto"><div align="center"> <img height=150 src="<%=BASEIMG%>image_error002.JPG" width=745 border=0 v:shapes="_x0000_i1052"></div></td>
</tr>
<tr>
  <td  class="ayudaHtmlTexto"><p>&nbsp;</p>
    <p>En esta pantalla muestra Cuando la consulta fue ejecutada y muestra el listado de los Clientes </p></td>
</tr>
<tr>
  <td  class="ayudaHtmlTexto"><p align="center"><IMG height=364 src="<%=BASEIMG%>image010.JPG" width=878 border=0 v:shapes="_x0000_i1052"></p>    </td>
</tr>
       </table></td>
    </tr>
  </table>
  <p></p>
<center>
<img src = "<%=BASEURL%>/images/botones/salir.gif" style = "cursor:hand" name = "imgsalir" onClick = "parent.close();" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);">
</center>
</BODY>
</HTML>
