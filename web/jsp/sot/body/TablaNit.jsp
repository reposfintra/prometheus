    <%@ page session="true" %>
    <%@ page errorPage="../error/ErrorPage.jsp" %>
    <%@ page import="java.util.*" %>
    <%@ page import="com.tsp.operation.model.beans.*" %>
    <%@ include file="/WEB-INF/InitModel.jsp" %><%
      int NINGUNO = 0;
      int CLIENTES = 1;
      int DESTINATARIOS = 2;
      boolean resetForm = false;
      String saveData  = request.getParameter("salvarDatos");
      if( saveData != null && saveData.equals("S") )
      {
        saveData = "N";
        resetForm = true; %>
        <center>
          <b>ˇˇ EL USUARIO HA SIDO CREADO EXITOSAMENTE !!</b>
        </center><br><%
      }
      Usuario loggedUser = (Usuario) session.getAttribute("userLoggedIn");
      saveData  = (saveData == null ? "N" : saveData);
      String nomUsu  = request.getParameter("nombre");
      nomUsu  = (nomUsu == null ? "" : nomUsu);
      String dirUsu  = request.getParameter("direccion");
      dirUsu  = (dirUsu == null ? "" : dirUsu);
      String paisUsu = request.getParameter("pais");
      paisUsu  = (paisUsu == null ? "COL" : paisUsu);
      String ciuUsu  = request.getParameter("ciudad");
      ciuUsu  = (ciuUsu == null ? "" : ciuUsu);
      String mailUsu = request.getParameter("email");
      mailUsu  = (mailUsu == null ? "" : mailUsu);
      String telUsu  = request.getParameter("telefono");
      telUsu  = (telUsu == null ? "" : telUsu);
      String tipoUsu = request.getParameter("tipoUsuario");
      if( loggedUser.getTipo().equals("ADMIN") ) {
        tipoUsu  = (tipoUsu == null ? "ADMIN" : tipoUsu);
      }else if( loggedUser.getTipo().equals("CLIENTEADM") ){
        tipoUsu  = "DEST";
      }
      String nitUsu  = request.getParameter("nit");
      nitUsu  = (nitUsu == null ? "" : nitUsu);
      String cliDes  = request.getParameter("clienteDestinat");
      cliDes  = (cliDes == null ? "" : cliDes);
      String estUsu  = request.getParameter("estadoUsuario");
      estUsu  = (estUsu == null ? "A" : estUsu);
      String idUsu   = request.getParameter("idUser");
      idUsu  = (idUsu == null ? "" : idUsu);
      String key1Usu = request.getParameter("pass1");
      key1Usu  = (key1Usu == null ? "" : key1Usu);
      String key2Usu = request.getParameter("pass2");
      key2Usu  = (key2Usu == null ? "" : key2Usu);
      String clienteDestinatDisabled = "";
      List list = null;
      String nomClienteSelected = "";
      String textoCliDest = "";
      boolean selected  = false;
      String formAction = "";
      if( loggedUser.getTipo().equals("ADMIN") )
      {
        if( tipoUsu.startsWith("CLIENTE") )
        {
          textoCliDest = "Cliente ";
          model.ClienteSearch();
          list = model.getClientes();
        }else{
          textoCliDest = "-";
          clienteDestinatDisabled = " disabled ";
        }
      }else if( loggedUser.getTipo().equals("CLIENTEADM") ){ %>
        <br><center>
          <b>Nombre del Usuario Cliente: <%= loggedUser.getNombre() %></b>
        </center><br><%
        textoCliDest = "Destinatario ";
        model.DestinatarioSearch(loggedUser.getClienteDestinat());
        list = model.getDestinatarios();
      }
      formAction = CONTROLLER + "?estado=GestionUsuarios&accion=CreateUser";
    %>
    <link href='css/EstilosFiltros.css' rel='stylesheet' type='text/css'>
    <script src='js/PantallaCrearUsuario.js' language="javascript"></script>
    <form name="createUserFrm" method="post" action="<%= formAction %>">
    <br><br>
    <table width="710" border="1" align="center" cellpadding="0" cellspacing="0">
      <tr>
        <th class='TableHeaderDecoration' colspan="8" align="center">
          Datos Basicos del Usuario
        </th>
      </tr>
      <tr>
        <td colspan="8" class='TableRowDecoration'>&nbsp;</td>
      </tr>
      <tr>
        <th align="left" class='TableSubHeaderDecoration'>
          Nombre o Raz&oacute;n Social
        </th>
        <td colspan="7" align="left" class='TableRowDecoration'>
          <input name="nombre" type="text" maxlength='50'
                 size="80" value='<%= nomUsu %>'>
          <font color='#FF0000' size='2'><b>*</b></font>
        </td>
      </tr>
      <tr>
        <th align="left" class='TableSubHeaderDecoration'>
          Direcci&oacute;n
        </th>
        <td colspan="7" align="left" class='TableRowDecoration'>
          <input name="direccion" type="text" maxlength='30'
                 size="80" value='<%= dirUsu %>'>
          <font color='#FF0000' size='2'><b>*</b></font>
        </td>
      </tr>
      <tr>
        <th align="left" class='TableSubHeaderDecoration' colspan='4'>
          Pa&iacute;s
          <select name="pais">
            <option value="COL" <%= (paisUsu.equals("COL")? "selected" : "") %>>Colombia
            <option value="ECU" <%= (paisUsu.equals("ECU")? "selected" : "") %>>Ecuador
            <option value="VEN" <%= (paisUsu.equals("VEN")? "selected" : "") %>>Venezuela
          </select>
          <font color='#FF0000' size='2'><b>*</b></font>
        </th>
        <th align="left" class='TableSubHeaderDecoration' colspan='4'>
          Ciudad
          <input type="text" maxlength='30' name="ciudad" value='<%= ciuUsu %>'>
          <font color='#FF0000' size='2'><b>*</b></font>
        </th>
      </tr>
      <tr>
        <th nowrap align="left" class='TableSubHeaderDecoration' colspan='4'>
          Correo Electr&oacute;nico
          <input name="email" maxlength='50' type="text"
                 size="35" value='<%= mailUsu %>'>
          <font color='#FF0000' size='2'><b>*</b></font>
        </th>
        <th align="left" class='TableSubHeaderDecoration' colspan='4'>
          Tel&eacute;fono
          <input name="telefono" maxlength='20' type="text" value='<%= telUsu %>'
                 onkeypress='return soloDigitosKeypress();'>
          <font color='#FF0000' size='2'><b>*</b></font>
        </th>
      </tr>
      <tr>
        <th align="left" class='TableSubHeaderDecoration' colspan='4'>
          Tipo de Usuario
          <select name="tipoUsuario" onChange="document.createUserFrm.submit();"><%
            if( loggedUser.getTipo().equals("ADMIN") )
            { %>
              <option value='ADMIN' <%= (tipoUsu.equals("ADMIN")? "selected" : "") %>>Administrador
              <option value='CLIENTETSP' <%= (tipoUsu.equals("CLIENTETSP")? "selected" : "") %>>Cliente de TSP
              <option value='CLIENTEADM' <%= (tipoUsu.equals("CLIENTEADM")? "selected" : "") %>>Cliente - Admin. de Transporte
              <option value='TSPSERVCLI' <%= (tipoUsu.equals("TSPSERVCLI")? "selected" : "") %>>Servicio al Cliente de TSP
              <option value='TSPUSER' <%= (tipoUsu.equals("TSPUSER")? "selected" : "") %>>Usuario de Consulta TSP <%
            } else if( loggedUser.getTipo().equals("CLIENTEADM") ) { %>
              <option value='DEST' <%= (tipoUsu.equals("DEST")? "selected" : "") %>>Destinatario <%
            } %>
          </select><font color='#FF0000' size='2'><b>*</b></font>
        </th>
        <th align="left" class='TableSubHeaderDecoration' colspan='4'>
          NIT o CC <input name="nit" maxlength='20' type="text" value='<%= nitUsu %>'>
        </th>
      </tr>
      <tr>
        <th nowrap align="left" class='TableSubHeaderDecoration' colspan='4'>
          <div id='TextoClienteDestinat'><%= textoCliDest %></div>
          <select name="clienteDestinat" <%= clienteDestinatDisabled %>><%
          // Generar dinámicamente
          String codigo = null;
          String nombre = null;
          Iterator it = null;
          if (list != null)
          {
            Cliente cliente = null;
            it = list.iterator();
            if( loggedUser.getTipo().equals("ADMIN") )
            {
              while (it.hasNext())
              {
                cliente = (Cliente) it.next();
                codigo = cliente.getCodigo();
                nombre = cliente.getNombre();
                if( !selected )
                { %>
                  <option value='<%= codigo %>' label='<%= nombre %>' selected><%= nombre %><%
                  nomClienteSelected = nombre;
                  selected = true;
                }else{ %>
                  <option value='<%= codigo %>' label='<%= nombre %>'><%= nombre %><%
                }
                out.println("\n");
              }
            } else if( loggedUser.getTipo().equals("CLIENTEADM") ) {
              Remidest destinatario = null;
              while (it.hasNext())
              {
                destinatario = (Remidest) it.next();
                codigo = destinatario.getCodigo();
                nombre = destinatario.getNombre();
                if( !selected )
                { %>
                  <option value='<%= codigo %>' label='<%= nombre %>' selected><%= nombre %><%
                  nomClienteSelected = nombre;
                  selected = true;
                }else{ %>
                  <option value='<%= codigo %>' label='<%= nombre %>'><%= nombre %><%
                }
                out.println("\n");
              }
            }
          } %>
          </select>
        </th>
        <th align="left" class='TableSubHeaderDecoration' colspan='4'>
          Estado del Usuario 
          <select name="estadoUsuario">
            <option value="A" <%= (estUsu.equals("A")? " selected " : "") %>>Activo
            <option value="I" <%= (estUsu.equals("I")? " selected " : "") %>>Inactivo
            <option value="R" <%= (estUsu.equals("R")? " selected " : "") %>>Retirado
          </select>
        </th>
      </tr>
      <tr>
        <td colspan="8" class='TableSubHeaderDecoration'>&nbsp;</td>
      </tr>
      <tr>
        <th colspan="8" align='center' class='TableHeaderDecoration'>
          Datos para acceso al sistema
        </th>
      </tr>
      <tr>
        <td colspan="8" class='TableSubHeaderDecoration'>&nbsp;</td>
      </tr>
      <tr>
        <th align="left" class='TableSubHeaderDecoration' colspan='4'>
          ID del Usuario
          <input name="idUser" maxlength='10' type="text" value='<%= idUsu %>'>
          <font color='#FF0000' size='2'><b>*</b></font>
        </th>
        <th align="left" class='TableSubHeaderDecoration' colspan='4'>
          Password
          <input name="pass1" type="password" maxlength='15' value='<%= key1Usu %>'>
          <font color='#FF0000' size='2'><b>*</b></font>
        </th>
      </tr>
      <tr>
        <th align="left" class='TableSubHeaderDecoration' colspan='8'>
          Reescriba Password
          <input name="pass2" type="password" maxlength='15' value='<%= key2Usu %>'>
          <font color='#FF0000' size='2'><b>*</b></font>
        </th>
      </tr>
      <tr>
        <th class='TableHeaderDecoration' colspan="2">
          <font color='#FF0000' size='2'><b>*</b></font>
          <b>Datos obligatorios</b>
        </th>
        <td align="center" colspan="6" class='TableSubHeaderDecoration'>
          <input type="button" name="enviarDatosBtn" value=' Continuar '
                 onclick="enviarDatosBtnClick()" class='botones'>
        </td>
      </tr>
      <tr>
        <td colspan="8" bgcolor="#CCCCCC" >         
      </tr>
    </table>
    <input type='hidden' name='salvarDatos' value='<%= saveData %>'>
    </form><%
    if( resetForm )
    { %>
      <script language='javascript'>
        createUserFrmReset();
      </script><%
    } %>