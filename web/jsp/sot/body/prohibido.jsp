<!--
- Autor : Alejandro Payares
- Date  : Diciembre 24 del 2005
- Copyrigth Notice : Fintravalores S.A. S.A
--Descripcion : Pagina JSP que muestra un mensaje de restricción.
-->
<%@ include file="/WEB-INF/InitModel.jsp" %>
<html>
<head>
<title>.: Acceso restringido :.</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link href="<%=BASEURL%>/css/estilostsp.css" rel="stylesheet" type="text/css">
<script src='<%=BASEURL%>/js/boton.js' language="javascript"></script>
    </head>

<body onresize="redimensionar()" onload='redimensionar()'>
    <div id="capaSuperior" style="position:absolute; width:100%; z-index:0; left: 0px; top: 0px;">
        <jsp:include page="/WEB-INF/toptsp.jsp?encabezado=ADMINISTRACIÓN DE USUARIOS"/>
    </div>
    <div id="capaCentral" style="position:absolute; width:100%; height:87%; z-index:0; left: 0px; top: 100px; overflow: scroll;"> 
	    <br>
<table align="center" border="2" width="50%"><tr><td class="bordereporte">
<table width="100%"  border="0" align="center" cellpadding="10" cellspacing="0">
  <tr class="tblTitulo">
    <td><img src="<%=BASEURL%>/images/alert_icon.jpg" width="150" height="150"></td>
    <td nowrap><%=request.getParameter("mensaje")%></td>
  </tr>
</table>
</td></tr></table>
</div>
</body>
</html>
