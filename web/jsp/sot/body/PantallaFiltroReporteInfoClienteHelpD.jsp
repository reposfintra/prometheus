<html>
<head>
<title>.: Descripción del programa de mantanimiento de tablagen :.</title>
<link href="../../../css/estilostsp.css" rel="stylesheet" type="text/css"> 
<style type="text/css">
<!--
.Estilo1 {font-size: 14px}
-->
</style>
</head>

<body>
<table width="696"  border="2" align="center">
  <tr>
    <td width="811" >
      <table width="100%" border="0" align="center">
        <tr  class="subtitulo">
          <td height="24" colspan="2"><div align="center">INFORMACION AL CLIENTE </div></td>
        </tr>
        <tr class="subtitulo1">
          <td colspan="2"> REPORTE DE INFORMACION AL CLIENTE </td>
        </tr>
        <tr>
          <td width="194" class="fila"><span>Seleccionar todos los clientes</span></td>
          <td width="480"  class="ayudaHtmlTexto">Permite generar el reporte para todos los clientes</td>
        </tr>
        <tr>
          <td class="fila">Cliente</td>
          <td  class="ayudaHtmlTexto">Permite seleccionar el cliente del cual se requiere el reporte </td>
        </tr>
        <tr>
          <td class="fila">Tipo de viaje</td>
          <td  class="ayudaHtmlTexto">Permite realizar un filtro por tipo de viaje </td>
        </tr>
        <tr>
          <td class="fila">Fecha Inicial</td>
          <td  class="ayudaHtmlTexto">Permite seleccionar la fecha inicial del despacho de la remesa </td>
        </tr>
        <tr>
          <td class="fila">Fecha Final</td>
          <td  class="ayudaHtmlTexto">Permite seleccionar la fecha final del despacho de la remesa </td>
        </tr>
        <tr>
          <td class="fila">Tipo Carga </td>
          <td  class="ayudaHtmlTexto">Permite filtrar por tipo de carga segun el cliente seleccionado </td>
        </tr>
        <tr>
          <td class="fila">Estado del viaje </td>
          <td  class="ayudaHtmlTexto">Permite filtrar por estado del viaje en trafico </td>
        </tr>
        <tr>
          <td class="fila">Consultar por despacho especifico </td>
          <td  class="ayudaHtmlTexto">Permite filtrar por factura comercial, documento interno y numero de remesa (OT)</td>
        </tr>
        <tr>
          <td class="fila">Formato de salida </td>
          <td  class="ayudaHtmlTexto">Se especifica en que formato desea su reporte. </td>
        </tr>
        
      </table>
    </td>
  </tr>
</table>

<p align="center" class="fuenteAyuda"> Fintravalores S. A. </p>
<p>&nbsp;</p>
<p>&nbsp;</p>

</body>
</html>