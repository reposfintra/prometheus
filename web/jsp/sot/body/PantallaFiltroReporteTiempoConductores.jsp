<!--
- Autor : Alejandro Payares
- Date  : Enero 24 de 2006
- Copyrigth Notice : Fintravalores S.A. S.A
- Descripcion : Pagina JSP que permite escojer las opciones para generar el reporte de tiempos de viaje de  los conductores.
-->
<%@ page errorPage="/ErrorPage.jsp" %>
<%@include file="/WEB-INF/InitModel.jsp" %>
<%@ taglib uri="http://jakarta.apache.org/taglibs/input-0.90" prefix="input" %>
<%@ taglib uri="http://www.sanchezpolo.com/sot" prefix="tsp"%>

<html>
    <head> 
        <title>.: TIEMPOS DE VIAJES DE CONDUCTORES :.</title>
        <script language="javascript" src="<%=BASEURL%>/jsp/sot/js/PantallaFiltroReporteTiempoConductores.js"></script>
        <script language="javascript" src="<%=BASEURL%>/js/tools.js"></script>
        <link href="<%=BASEURL%>/css/estilostsp.css" rel="stylesheet" type="text/css">
    </head>
    <body onresize="redimensionar()" onload='redimensionar()'>
        <div id="capaSuperior" style="position:absolute; width:100%; z-index:0; left: 0px; top: 0px;">
            <jsp:include page="/toptsp.jsp?encabezado=REPORTE DE TIEMPOS DE CONDUCTORES"/>
        </div>
        <div id="capaCentral" style="position:absolute; width:100%; height:87%; z-index:0; left: 0px; top: 100px; overflow: scroll;"> 

            <%-- <jsp:useBean id="beanInstanceName" scope="session" class="beanPackage.BeanClassName" /> --%>
            <%-- <jsp:getProperty name="beanInstanceName"  property="propertyName" /> --%>
            <br>
            <%if(request.getParameter("mensaje") != null){%>
            <table border="2" align="center">
                <tr>
                    <td><table width="100%"  border="1" align="center"  bordercolor="#F7F5F4" bgcolor="#FFFFFF">
                        <tr>
                            <td width="229" align="center" class="mensajes"><%=request.getParameter("mensaje")%></td>
                            <td width="29" background="<%=BASEURL%>/images/cuadronaranja.JPG">&nbsp;</td>
                            <td width="58">&nbsp;</td>
                        </tr>
                    </table></td>
                </tr>
            </table>
            <br>
            <%}
            String action = CONTROLLER+"?estado=ReporteTiemposDeViaje&accion=Conductores&cmd=show"; %>
            <form name="returnsFrm" method="post" action='<%=action%>'>
            <table align='center' border = '1'>
                <tr>
                    <td width='500px'>
                    <table width='100%'>
                    <tr align='center'>
                        <td colspan="2" align="center" class="subtitulo1">OPCIONES DEL REPORTE <br></td>
                        <td colspan="1" width="15%" align="left" class="barratitulo"><img src="<%=BASEURL%>/images/titulo.gif" width="32" height="20"></td>
                    </tr>
                    <tr class='fila'>
                        <td  align='left' colspan="3">
                            <table>
                                <tr class='fila'>
                                    <td> Fecha Inicial: </td>
                                    <td> <tsp:Date fechaInicial="hoy" formulario="returnsFrm" clase="textbox" name="fechaini" otros="size='20' alt='Haga click aqu&iacute; para escoger la fecha'" /> </td>
                                    <td width='50'>&nbsp;</td>
                                    <td align='left'> Fecha Final: </td>
                                    <td> <tsp:Date fechaInicial="hoy" formulario="returnsFrm" clase="textbox" name="fechafin" otros="size='20' alt='Haga click aqu&iacute; para escoger la fecha'" /> </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr class="fila">
                        <td width='30%'>Placa:</td>
                        <td colspan="2"><input:text name="placa" default="" attributesText="style='width:50%'"/></td>
                    </tr>
                    <tr class="fila" >
                        <th  align='left'>Cliente</th>
                        <td colspan="2">
                            <table width="100%"  border="0">
                                <tr class="fila">
                                    <td>Campo de busqueda: <br>
                                    <input type="text" class="textbox" id="campo" style="width:400" onKeyUp="buscar(document.returnsFrm.cliente,this)" ></td>
                                </tr>
                                <tr>
                                <td>
                                    <input:select default="" name="cliente" options="<%=model.clienteService.listar((com.tsp.operation.model.beans.Usuario)session.getAttribute("Usuario"))%>" attributesText="id='cliente' class='listmenu' size='5' style='width:100%' onclick='document.returnsFrm.campo.value = this[this.selectedIndex].innerText;'"/>
                                </td>
                                </tr>
                            </table>
                        </td>
                        </tr>
                        <tr class='fila'>
                    
                            <th  align='left'>Origen:</th>
                            <td colspan="2"><table width="100%"  border="0">
                            <tr class="fila">
                                <td>Campo de busqueda: 
                                <input type="text" class="textbox" name='campoOrigen' id="campoOrigen" style="width:250" onKeyUp="buscar(document.returnsFrm.origen,this)" ></td>
                            </tr>
                            <tr>
                            <td> <input:select default="" name="origen" options="<%=model.ciudadService.getTreMapCiudades()%>" attributesText="id='origen' size='2' class='listmenu' style='width:100%' onclick='document.returnsFrm.campoOrigen.value = this[this.selectedIndex].innerText;'"/> </td>
                        </tr>
                            <tr>
                                <td>
                                    <input type="checkbox" name='todoOrigen' id="todoOrigen" onChange="document.returnsFrm.origen.disabled = this.checked"/><label class='fila' for="todoOrigen">Buscar en todas las ciudades</label>
                                </td>
                            </tr>
                        </table></td>
                        </tr>
                        <tr class='fila'>
                            <th  align='left'>Destino:</th>
                            <td colspan="2"><table width="100%"  border="0">
                            <tr class="fila">
                                <td>Campo de busqueda: 
                                <input type="text" class="textbox" name='campoDestino' id="campoDestino" style="width:250" onKeyUp="buscar(document.returnsFrm.destino,this)" ></td>
                            </tr>
                            <tr>
                            <td> <input:select default="" name="destino" options="<%=model.ciudadService.getTreMapCiudades()%>" attributesText="id='destino' size='2' class='listmenu' style='width:100%' onclick='document.returnsFrm.campoDestino.value = this[this.selectedIndex].innerText;'"/> </td>
                        </tr>
                            <tr>
                                <td>
                                    <input type="checkbox" name="todoDestino" id="todoDestino" onChange="document.returnsFrm.destino.disabled = this.checked"/><label class='fila' for="todoDestino">Buscar en todas las ciudades</label>
                                </td>
                            </tr>
                        </table></td>
                        </tr>
                        
                        <tr class="fila">
                            <td >Formato de Salida:</td>
                            <td colspan='2'>
                                <select class='listmenu' name="formatoSalida" >
                                <option value="web" selected="selected">P&aacute;gina WEB
                                <option value="excel">Archivo Excel
                                </select>
                            </td>
                        </tr>
                        
                    </table>
                </td>
                </tr>
            </table>
            </form>
            <p>
            <center>
                <table>
                <tr>
                <td><% String onclick = "validarCamposReporteTiempos('"+action+"')"; %>
                    <tsp:boton value="buscar" onclick="<%=onclick%>"/>
                </td>
                <td width='30'>&nbsp;</td>
                <td>
                    <tsp:boton value="salir" onclick="window.close()"/>
                </td>
                </tr>
            </center>
            </p>
        </div>
        
    </body>
    <tsp:InitCalendar/>
</html>
