<!--
- Autor : Ivan Dario Gomez
- Date  : Diciembre 24 del 2005
- Copyrigth Notice : Fintravalores S.A. S.A
- Descripcion : Pagina JSP que permite ver los datos del reporte Ubicacion Vehicular Clientes.
-->
<%@ page session="true" %>
<%@ page errorPage="/ErrorPage.jsp" %>
<%@ page import="java.util.*" %>
<%@ page import="com.tsp.operation.model.beans.*" %>
<%@ include file="/WEB-INF/InitModel.jsp" %> 

<% 
boolean displayInExcel = new Boolean(request.getParameter("displayInExcel").toLowerCase()).booleanValue();
String claseCelda = "class='bordereporteSot informacion'";
if ( displayInExcel ) {
    response.setContentType("application/vnd.ms-excel");
    claseCelda = "";
} 
boolean hayFechaIni = (request.getParameter("fechaInicial") != null);
String cliente = (request.getParameter("cliente")!= null)?request.getParameter("cliente"):""; 
String devoluciones = null, QUERYSTRING = null; 
 
try {
%>
<html>
<head>
    <title>Reporte de Ubicacion Vehicular Clientes</title>
</head>
<% String path = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+request.getContextPath(); %>
    <link href='<%=path%>/css/estilostsp.css' rel='stylesheet' type='text/css'>
<script src="<%=BASEURL%>/js/tools.js" language="javascript"></script>
<script type='text/javascript' src="<%= BASEURL %>/js/boton.js"></script>
    <script src="/sot/js/ReporteUbicaEquipoRuta.js" language="javascript"></script>
<!-- ESTA CONDICIÓN SE DEBE A QUE PARA EL REPORTE EN VISTA EXCEL NO SE MUESTRA EL ENCABEZADO VERDE
     DE LA NUEVA IMAGEN CORPORATIVA. Alejandro Payares, nov 11 de 2005. -->
<% if ( displayInExcel ) { %>
    <body>
<% } else { %>
    <body onresize="redimensionar()" onload='redimensionar()'>
    <div id="capaSuperior" style="position:absolute; width:100%; height:100px; z-index:0; left: 0px; top: 0px;">
        <jsp:include page="/WEB-INF/toptsp.jsp?encabezado=Reporte de Ubicacion Vehicular Clientes"/>
    </div>
    <div id="capaCentral" style="position:absolute; width:100%; height:85%; z-index:0; left: 0px; top: 100px; overflow: scroll;"> 
    <% } %>
        <table border="0" align="left" cellpadding="0" cellspacing="0">
        <tr>
        <td colspan='2' align="center" valign="top">
            <table align='left' border='1' cellspacing="0" cellpadding="0">
                <tr class="subtitulo1">
                  <th align='center' colspan='2'>Cliente</th><%
                  if( hayFechaIni ) { %>
                    <th align='center' >Fecha Inicial</th>
                    <th align='center' >Fecha Final</th><%
                  } %>
                  <th align='center'  <%= (hayFechaIni? "" : "colspan='2'") %>>
                    Tipos de Viaje
                  </th>
                  <th align='center' <%= (hayFechaIni? "" : "colspan='2'") %>>
                    Ultima Actualizaci&oacute;n
                  </th>
                  <th align='center'  colspan='4'>
                    Convenci&oacute;n de Colores Para el Reporte
                  </th>
                </tr>
                <tr class='fila'>
                  <td align='center' class='TableRowDecoration Estilo10' colspan='2'>
                    <%= cliente %>
                  </td><%
                  if( hayFechaIni ) { %>
                    <td align='center' class='TableRowDecoration Estilo10'>
                      <%= request.getParameter("fechaInicial") %>
                    </td>
                    <td align='center' class='TableRowDecoration Estilo10'>
                      <%= request.getParameter("fechaFinal") %>
                    </td><%
                  } %>
                  <td align='center' class='TableRowDecoration Estilo10'
                      <%= (hayFechaIni? "" : "colspan='2'") %>>
                    <%= request.getParameter("listaTipoViaje") %>
                  </td>
                  <td align='center' class='TableRowDecoration Estilo10'
                      <%= (hayFechaIni? "" : "colspan='2'") %>>
                    <%@include file="/WEB-INF/lastUpdateSotDb.html" %>
                  </td>
                  <th class='letraresaltada' bgcolor="#FFFF8C">En Ruta</th>
                  <th class='letraresaltada' bgcolor="#CAFFCA">Con Entrega</th>
                  <th class='letraresaltada' bgcolor="#FF8888">Con Problemas</th>
                  <th class='letraresaltada' bgcolor="#FFFFFF">Por Confirmar Salida</th>
                </tr>
              </table>
            </td>
           </tr>
           <tr>
             <td colspan='2' align="center" valign="top">
                  <%List datos = model.UbicacionVehicularClientesSvc.obtenerDatosReporte();
                   
                    Hashtable titulos = model.UbicacionVehicularClientesSvc.obtenerTitulosDeReporte();
                    String [] campos = model.UbicacionVehicularClientesSvc.obtenerCamposDeReporte();
                 %>
                  <div class="tblTitulo" align='left'>
                       <h4><%= datos.size() %> Registros encontrados.</h4>
                  </div>
                   <table border="1">
                    <tr class='tblTitulo'>
                      <% for( int i=0; i<campos.length; i++ ){ %>
                        <th align='center' class="subtitulo1"><%= titulos.get(campos[i]) %></th>
                      <% } %>                
                    </tr>
                     
                    <%   Iterator iterator = datos.iterator();
                        while( iterator.hasNext() ) {
                         ReporteUbicacionVehicular Ubicacion = (ReporteUbicacionVehicular)iterator.next(); 
                         String cEstado   = Ubicacion.getEstado();
                         String cTieneDev = Ubicacion.getDevoluciones().toUpperCase().trim();
                          if (cTieneDev.equals("SI")) {%>
                              <tr bgcolor="#FF8888">
                        <%} else {%>
                                <tr bgcolor="<%= (cEstado.trim().equals("En Ruta")?"#FFFF8C":( cEstado.trim().equals("Con Entrega")?"#CAFFCA":"#FFFFFF" ))%>">
                               <%}
                           for(int i=0; i<campos.length; i++ ){
                            if ( campos[i].equals("devoluciones") ){
                                if( Ubicacion.obtenerValor(campos[i]).equals("Si")){ 
                                 QUERYSTRING = "?estado=UbicacionVehicular" +
                                               "&accion=Return&planilla=" +Ubicacion.getNumPlanilla() +
                                               "&cliente="+cliente;%>  
                                    <td nowrap align='center' <%=claseCelda%>><a href="javascript:abrirVent('<%= CONTROLLER + QUERYSTRING %>');"><%= Ubicacion.obtenerValor(campos[i]) %></a></td><%
                                }else{%>
                                    <td nowrap align='center' <%=claseCelda%>> <%= Ubicacion.obtenerValor(campos[i]) %> </td>
                                   <%}
                            }else{%>
                             <td nowrap align='center' <%=claseCelda%>> <%= Ubicacion.obtenerValor(campos[i]) %> </td>  
                            <%}
                            }//fin del for%>
                           </tr>
                       <%}//fin del while %>
                   </table>
             </td>
            </tr>
        </table>

    <% if ( !displayInExcel ) { %>
    </div>
    <% } %>
<% } catch( Exception ex ){ ex.printStackTrace(); } %>
</body>
</html>
<script>
function abrirVent(url){
    window.open(url,'','resizable=yes,status=yes')
}
</script>