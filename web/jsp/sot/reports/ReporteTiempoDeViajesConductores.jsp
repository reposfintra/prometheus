<!--
- Autor : Alejandro Payares
- Date  : Enero 24 de 2006 - 7:37 PM
- Copyrigth Notice : Fintravalores S.A. S.A
- Descripcion : Pagina JSP que permite ver los datos del reporte de tiempos de conductores.
-->
<%@ page errorPage="/error/ErrorPage.jsp" %>
<%@ page import="java.util.Hashtable" %>
<%@ page import="java.util.LinkedList" %>
<%@ page import="java.util.Iterator" %>
<%@ page import="com.tsp.util.Util" %>
<%@ page import="com.tsp.util.Utility" %>
<%@ include file="/WEB-INF/InitModel.jsp" %>

<% /*if ( request.getParameter("displayInExcel").equalsIgnoreCase("true") ) {
        response.setContentType("application/vnd.ms-excel");
   }*/ %>

<html>
    <head>
        <title>.: Reporte de tiempos de viajes de conductores :.</title>  
        <link href="<%=BASEURL%>/css/estilostsp.css" rel="stylesheet" type="text/css">
        <script src='<%=BASEURL%>/js/tools.js' language='javascript'></script>
        <link href="../../../css/estilostsp.css" rel="stylesheet" type="text/css">
    </head>
    <body onresize="redimensionar()" onload='redimensionar()'>
        <div id="capaSuperior" style="position:absolute; width:100%; height:100px; z-index:0; left: 0px; top: 0px;">
            <jsp:include page="/WEB-INF/toptsp.jsp?encabezado=REPORTE DE TIEMPOS DE CONDUCTORES"/>
        </div>
        <div id="capaCentral" style="position:absolute; width:100%; height:85%; z-index:0; left: 0px; top:100px; overflow:scroll;"> 
    
            <table width="100%"  border="0" align="center" cellpadding="0" cellspacing="0">
            <tr>
            <td height="30" align="center" valign="top">          <table width="700" border="1" align="left">
                <tr class="subtitulo1">
                <td><div align="center">Fecha del reporte</div></td>
			  <% if (request.getParameter("inicio") != null ){ %>
                <td><div align="center">Fecha inicial </div></td>
                <td><div align="center">Fecha final </div></td>
			  <% } %>
			  <% if (request.getParameter("placa") != null ){ %>
                <td><div align="center">Placa</div></td>
              <% } %>
			  <% if (request.getParameter("nombreCliente")!= null && !"null".equals(request.getParameter("nombreCliente"))){ %>
                <td><div align="center">Cliente</div></td>
              <% } %>
			  <% if ( request.getParameter("origen") != null && !"null".equals(request.getParameter("origen"))){ %>
                <td><div align="center">Origen</div></td>
              <% } %>
			  <% if (request.getParameter("destino") != null && !"null".equals(request.getParameter("destino"))){ %>
                <td><div align="center">Destino</div></td>
              <% } %>
                <tr class="filagris">
                    <td><div align="center"><%=Utility.getDate(6)%></div></td>
              <% if (request.getParameter("inicio") != null ){ %>
                    <td><div align="center"><%=request.getParameter("inicio")%></div></td>
                    <td><div align="center"><%=request.getParameter("fin")%></div></td>
			  <% } %>
			  <% if (request.getParameter("placa") != null ){ %>
                    <td><div align="center"><%=request.getParameter("placa")%></div></td>
              <% } %>
			  <% if (request.getParameter("nombreCliente") != null && !"null".equals(request.getParameter("nombreCliente"))){ %>
                    <td><div align="center"><%=request.getParameter("nombreCliente")%></div></td>
              <% } %>
			  <% if (request.getParameter("origen") != null  && !"null".equals(request.getParameter("origen"))){ %>
                    <td><div align="center"><%=request.getParameter("origen")%></div></td>
              <% } %>
			  <% if (request.getParameter("destino") != null  && !"null".equals(request.getParameter("destino"))){ %>
                    <td><div align="center"><%=request.getParameter("destino")%></div></td>
              <% } %>
                </tr>
            </table></td>
            </tr>
            <tr height='30'>
                <td>
                </td>
            </tr>
            <tr>
            <td>
      <% String titulos [] = {"Planilla","Origen","Destino","Placa","Conductor","Cliente","Fecha y hora de salida"};
         String campos [] = {"planilla","origen","destino","placa","conductor","nombrecliente","fechaSalida"}; %>
            <table border="1" cellpadding='1' cellspacing='1' bgcolor='white'>
     <% 
        out.println("<tr class='tblTitulo'>");
        for(int i=0; i<titulos.length; i++ ){
            out.println("<td class='bordereporte tblTitulo'>&nbsp;</td>");
        }
        int diasMaximos = model.repTiempoDeViajeConductores.obtenerNumeroMaximoDeDias();
        for( int i=1; i<=diasMaximos; i++ ){
            out.println("<td class='subtitulo1' colspan='6' align='center'>DIA "+i+"</td>");
            if ( i < diasMaximos ){
                out.println("<td class='bordereporte tblTitulo' width='5'>&nbsp;</td>");
            }
        }
        out.println("</tr>");
        out.println("<tr height='25' class='tblTitulo'>");
        for( int i=0; i<campos.length; i++ ){
            out.println("<td align='center' nowrap>"+titulos[i]+"</td>");
        }
        for( int i=1; i<=diasMaximos; i++ ){
            out.println("<td align='center' nowrap>Primer rep</td>");
            out.println("<td align='center' nowrap>Ultimo rep</td>");
            out.println("<td align='center' nowrap>Tiempo trabajo</td>");
            out.println("<td align='center' nowrap>Demora</td>");
            out.println("<td align='center' nowrap>Descripción demora</td>");
            out.println("<td align='center' nowrap>Total</td>");
            if ( i < diasMaximos ){
                out.println("<td class='bordereporte tblTitulo' width='5'>&nbsp;</td>");
            }
        }
        out.println("</tr>");
        LinkedList devolList = model.repTiempoDeViajeConductores.obtenerDatosReporte();
        if( devolList != null ) {
          out.println("<b class='tblTituloSinFondo'>" + devolList.size() + " Registros Encontrados</b>.<br>");
          Iterator devolIt = devolList.iterator();
          int j=0;
          while( devolIt.hasNext() ) {
            Hashtable fila = (Hashtable) devolIt.next(); 
            out.println("    <tr class='fila"+ (j%2==0?"azul":"gris")+"' onMouseDown='cambiarColor(this)' style='cursor:hand'>");
            for( int i = 0; i < campos.length; i++ ) {
                out.print("      <td class='bordereporte' nowrap>");
                String str = ((String)fila.get(campos[i]));
                str = str == null? "":str;
                str = str.equals("0099-01-01")?"":str;
                out.print(str.equals("")?"-":str);
                out.println("      </td>");
            } 
            for( int i=1; i<=diasMaximos; i++){
                Hashtable datosDelDia = (Hashtable)fila.get("dia"+i);
                if ( datosDelDia == null ){
                    out.print("      <td class='bordereporte'></td>");
                    out.print("      <td class='bordereporte'></td>");
                    out.print("      <td class='bordereporte'></td>");
                    out.print("      <td class='bordereporte'></td>");
                    out.print("      <td class='bordereporte'></td>");
                    out.print("      <td class='bordereporte'></td>");
                }
                else {
                    out.print("      <td class='bordereporte' nowrap>"+datosDelDia.get("inicio")+"</td>");
                    out.print("      <td class='bordereporte' nowrap>"+datosDelDia.get("fin")+"</td>");
                    out.print("      <td class='bordereporte' nowrap>"+datosDelDia.get("tiempo")+"</td>");
                    if ( datosDelDia.get("demora").equals("-") ){
                        out.print("      <td class='bordereporte'>-</td>");
                        out.print("      <td class='bordereporte'>-</td>");
                    }
                    else {
                        out.print("      <td class='bordereporte' nowrap><font color='red'>-"+datosDelDia.get("demora")+"</font></td>");
                        out.print("      <td class='bordereporte' nowrap><font color='red'>"+datosDelDia.get("descripciones")+"</font></td>");
                    }
                    out.print("      <td class='bordereporte' nowrap>"+datosDelDia.get("total")+"</td>");
                }
                if ( i < diasMaximos ){
                    out.println("<td class='bordereporte tblTitulo' width='5'>&nbsp;</td>");
                }
            }
            out.println("    </tr>");
            j++;
          }
          model.repTiempoDeViajeConductores.borrarDatos();
        }
        else{ %>
            <td colspan='<%=campos.length%>' class='fondotabla' bgcolor='#ECE9D8'>
                No se encontraron viajes.
            </td>
    <%  } %>
            </table>
            </td>
            </tr>
            </table>
        </div>
    </body>
</html>
