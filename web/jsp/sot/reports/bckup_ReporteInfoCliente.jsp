<!--
- Autor : Alejandro Payares
- Date  : Diciembre 24 del 2005
- Copyrigth Notice : Fintravalores S.A. S.A
- Descripcion : Pagina JSP que permite ver los datos del reporte de infocliente.
-->
<%@ page session="true" %>
<%@ page errorPage="/ErrorPage.jsp" %>
<%@ page import="java.util.*" %>
<%@ page import="com.tsp.operation.model.beans.*" %>
<%@ include file="/WEB-INF/InitModel.jsp" %> 

<% 
boolean displayInExcel = new Boolean(request.getParameter("displayInExcel").toLowerCase()).booleanValue();
String claseCelda = "class='bordereporteSot'";
if ( displayInExcel ) {
    response.setContentType("application/vnd.ms-excel");
    claseCelda = "";
} 
try {
%>

<html>
<head>
    <title>.: Información al cliente :.</title>
    <script src="<%=BASEURL%>/js/ReporteUbicaEquipoRuta.js" language="javascript"></script>
    <script src="<%=BASEURL%>/jsp/sot/js/ReporteInfocliente.js" language="javascript"></script>
    <script src="<%=BASEURL%>/js/tools.js" language="javascript"></script>
    <% String path = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+request.getContextPath(); %>
    <link href='<%=path%>/css/estilostsp.css' rel='stylesheet' type='text/css'>
</head>
<!-- ESTA CONDICIÓN SE DEBE A QUE PARA EL REPORTE EN VISTA EXCEL NO SE MUESTRA EL ENCABEZADO VERDE
     DE LA NUEVA IMAGEN CORPORATIVA. Alejandro Payares, nov 11 de 2005. -->
<% if ( displayInExcel ) { %>
    <body>
<% } else { %>
    <body onresize="redimensionar()" onload='redimensionar()'>
    <div id="capaSuperior" style="position:absolute; width:100%; height:100px; z-index:0; left: 0px; top: 0px;">
	<jsp:include page="/WEB-INF/toptsp.jsp?encabezado=REPORTE%20DE%20INFORMACIÓN%20AL%20CLIENTE"/>
    </div>
    <div id="capaCentral" style="position:absolute; width:100%; height:85%; z-index:0; left: 0px; top:100px; overflow:scroll;"> 
<% } %>
    <table width="100%"  border="0" align="center" cellpadding="0" cellspacing="0">
        <tr>
        <td height="30" valign="top">
            <table width='700' border="1" align="left">
              <%
            boolean isListaTipoViajeNA = false;
            String searchCriteria = request.getParameter("criteria");
			if( searchCriteria.equals("ot") ) {
                searchCriteria = "OT";
            }else if( searchCriteria.equals("facturacial") ){
                searchCriteria = "Factura Comercial";
            }else if( searchCriteria.equals("docinterno") ){
                searchCriteria = "Documento Interno";
            }
            if( searchCriteria != null && searchCriteria.length() > 0 ) { %>
                <tr class="subtitulo1">
                    <th align='center' >Cliente</th>
                    <th align='center' ><%= searchCriteria %></th>
                </tr>
                <tr class="fila">
                    <td align='center' class='Estilo10' >
              <%= request.getParameter("nombreCliente") %>
                    </td>
                    <td align='center' class='Estilo10' >
              <%= request.getParameter("userDefValue") %>
                    </td>
              </tr>
        <%  }else {
                boolean hayFechaIni = (request.getParameter("fechaini") != null); %>
                <tr class="subtitulo1">
                    <th align='center' >Cliente</th>
                    <%
            if( hayFechaIni ) { %>
                    <th align='center'>Fecha Inicial</th>
                    <th align='center'>Fecha Final</th>
                    <%
            } %>
                    <th align='center'
                <%= (hayFechaIni? "" : "") %>>
                        Tipos de Viaje
                    </th>
                    <th align='center'
                <%= (hayFechaIni? "" : "") %>>
                        Ultima Actualizaci&oacute;n
                    </th>
                </tr>
                <tr class="fila">
                <td align='center' >
              <%= request.getParameter("nombreCliente") %>
                </td><%
            if( hayFechaIni ) { %>
                <td align='center' >
                <%= request.getParameter("fechaini") %>
                </td>
                <td align='center' >
                <%= request.getParameter("fechafin") %>
                </td><%
            }
            String listaTipoViaje = request.getParameter("listaTipoViaje");
            if( listaTipoViaje.equals("NA") )
              isListaTipoViajeNA = true;%>
                <td align='center' 
                <%= (hayFechaIni? "" : "") %>>
              <%= listaTipoViaje %>
                </td>
                <td align='center' 
                <%= (hayFechaIni? "" : "") %>>
                    <%@include file="/WEB-INF/lastUpdateSotDb.html" %>
                </td>
              </tr><%
             } 
               %>
          </table>
        </td>
        </tr>
        <tr>
        <td height="330" align="center" valign="top">
      <% Hashtable titulos = model.reporteInfoClienteService.obtenerTitulosDeReporte();
         System.out.println("TITULOS = "+titulos);
         String campos [] = model.reporteInfoClienteService.obtenerCamposDeReporte(); 
         Vector datos = model.reporteInfoClienteService.obtenerDatosReporte();
      %>
        <div class="tblTitulo" align='left'>
            <b><%= datos.size() %> Registros encontrados.</b>        </div>
        <table border="2" align="center" cellpadding="0" cellspacing="0">
        	<tr>
        	<td height="330" align="center" valign="top">
			<table width="100%"  border="1" align="center">
            <%= model.reporteInfoClienteService.obtenerEncabezadoTablaReporte(isListaTipoViajeNA) %>
            <%
            if( datos != null ) {
              int j=0;
              for( int x=0; x<datos.size(); x++ ) {
                Hashtable fila = (Hashtable) datos.elementAt(x);
                String clase = j%2==0?"filagris":"filaazul";
                out.println("    <tr onMouseDown='cambiarColor(this)' class='"+clase+"' style='cursor:hand'>");
                for( int i = 0; i < campos.length; i++ ) {
                    String str = (String)fila.get(campos[i]);
                    str = str == null? "": str.trim();
                    str = str.equals("") || str.equals("0099-01-01")?"-":str;
                    boolean hayPlanilla = ((Boolean)fila.get("hay_planilla")).booleanValue();
                    if ( campos[i].equals("ultreporte") && !displayInExcel && hayPlanilla){
                            String cParam = "?estado=Trafico" +
                                        "&accion=Search&numpla=" + fila.get("planilla") +
                                        "&estador=" + fila.get("estado") +
                                        "&origen=" + fila.get("orirem") +
                                        "&destino=" + fila.get("desrem") +
                                        "&placa=" + fila.get("placa") +
                                        "&displayInExcel=" + displayInExcel ;
                            
                            out.print("<td nowrap align='center' "+claseCelda+"><a href=\"javascript:ConsultarClick('"+ CONTROLLER + cParam  +"')\">");
                            out.print(str);
                            out.println("</a></td>");
                    }
                    else if ( (campos[i].equals("estadoaduana") || campos[i].equals("observacionaduana")) && !("-".equals(str)) && !displayInExcel){
                        String cParam = "?estado=Movimiento&accion=Aduana&cmd=show&remesa="+fila.get("numrem")+"&detalle="+campos[i];
                            out.print("<td nowrap align='center' "+claseCelda+"><a href=\"javascript:verDetalleAduana('"+ CONTROLLER + cParam  +"')\">");
                            out.print(str);
                            out.println("</a></td>");
                    }
                    else {
                        if ( campos[i].equals("docuinterno") || campos[i].equals("facturacial")){
                                str = str.replaceAll("/", "<br>");
                        }
                        out.print("      <td "+claseCelda+" nowrap>");
                        out.print(str);
                        out.println("      </td>");
                    }
                }
                out.println("    </tr>");
                j++;
              }
            }
            else{ %>
              <th colspan='<%=campos.length%>' class='fila'>
                No se encontraron datos.
              </th>
        <%  } %>
        </table>
		</td>
		</tr>
		</table>
        </td>
        </tr>
        <tr>
        <td height="25" align="center" class="subtitulo">
            <span class="style1">
            <%
        Usuario loggedUser = (Usuario) session.getAttribute("Usuario");
        if(loggedUser != null && loggedUser.getTipo().startsWith("CLIENTE"))
        {
          String notas = (String) session.getAttribute("notas");
          if( notas == null )
          {
            notas = model.clienteService.notasClienteSearch(loggedUser.getClienteDestinat());
            session.setAttribute("notas", notas);
          }
          if( !notas.equals("") ) { %>
                <CENTER><H5><%= notas %></H5></CENTER><%
          }
        } 

}
catch( Exception ex ){ ex.printStackTrace(); } 
%>
          Fintravalores S. A. 2007- Best View 1024 x 768 pixels          </span>        </td>
        </tr>
  </table>
<% if ( !displayInExcel ) { %>
    </div>
<% } %>
</body>
</html>
