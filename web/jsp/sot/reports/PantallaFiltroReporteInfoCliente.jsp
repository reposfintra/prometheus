<!--
- Autor : Alejandro Payares
- Date  : Diciembre 24 del 2005
- Copyrigth Notice : Fintravalores S.A. S.A
- Descripcion : Pagina JSP que permite filtrar los datos para mostrar el reporte de infocliente.
--> 
<%@ page session="true" %>
<%@ page errorPage="/ErrorPage.jsp" %>
<%@ page import="java.util.*" %>
<%@ page import="com.tsp.operation.model.beans.*" %>
<%@ taglib uri="http://jakarta.apache.org/taglibs/input-0.90" prefix="input" %>
<%@ taglib uri="http://www.sanchezpolo.com/sot" prefix="tsp"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %> 
<%@ include file="/WEB-INF/InitModel.jsp" %>
<%@ page isELIgnored ="false" %> 
<%String path = application.getRealPath(model.menuService.getRealUrl(request.getRequestURI()));
  String datos[] = model.menuService.getContenidoMenu(BASEURL,request.getRequestURI(),path);
%>
<% try { %>

<%
    Usuario loggedUser = (Usuario) session.getAttribute("Usuario");
    Hashtable bean = (Hashtable)request.getAttribute("bean");
%>
<head>
    <title>.: Reporte de información al cliente :.</title>
    <script src="<%=BASEURL%>/js/PantallaFiltroReporteInfoCliente.js" language="javascript"></script>
    <script language="javascript" src="<%=BASEURL%>/js/tools.js"></script>
	<script type='text/javascript' src="<%= BASEURL %>/js/boton.js"></script>
    <link href="<%=BASEURL%>/css/estilostsp.css" rel="stylesheet" type="text/css">
</head>
<body onresize="redimensionar()" onload = "redimensionar();document.form1.tipo_carga.selectedIndex = 0;document.form1.campo.focus()">
<div id="capaSuperior" style="position:absolute; width:100%; height:100px; z-index:0; left: 0px; top: 0px;">
	<jsp:include page="/WEB-INF/toptsp.jsp?encabezado=REPORTE%20DE%20INFORMACIÓN%20AL%20CLIENTE"/>
</div>
<div id="capaCentral" style="position:absolute; width:100%; z-index:0; left: 0px; top: 100px; overflow: scroll;"> 
    <form name="form1" method="post" >
    <table border="0" align="center" cellpadding="0" cellspacing="0">
        <tr>
        <td height="330" align="center" valign="top">
  		<table width="608" border="2" align="center">
            <tr>
            <td>
            <table width="100%">
                <tr>
                    <td width="50%" align="left" class="subtitulo1">CLIENTE<%=" " + request.getParameter("nombreCliente")%></td>
					<td width="50%" align="left" class="barratitulo"><img src="<%=BASEURL%>/images/titulo.gif" width="32" height="20" align="left"><%=datos[0]%></td>
                </tr>
            </table>
            <table width="100%"  border="0">
                <tr class="fila">
                    <td width="50%" >Campo de busqueda:<br>
                    <input name="text" type="text" class="textbox" id="campo" style="width:100%" onKeyUp="buscar(document.form1.cliente,this)" value='<%=request.getParameter("nombreCliente")%>' ></td>
                    <td width="50%" rowspan="2" >
                    <table width="100%" class="tablaInferior">
                        <tr height="50%" class="fila">
                            <td width="38%">FECHA INICIAL:</td>
                            <td width="62%" align="left">
                                <tsp:Date fechaInicial="<%= bean.get("fechaini").toString() %>" formulario="form1" clase="textbox" name="fechaini" otros="size='20'" />
                            </td>
                        </tr>
                        <tr height="50%" class="fila">
                            <td>FECHA FINAL: </td>
                            <td align="left">
                                <tsp:Date fechaInicial="<%= bean.get("fechafin").toString() %>" clase="textbox" formulario="form1" name="fechafin" otros="size='20'" />
                            </td>
                        </tr>
                    </table></td>
                </tr>
                <tr class="fila">
                <td height="33" rowspan="2">
              <% String str = "id='cliente' class='listmenu' size='5' style='width:100%' onclick=\"clienteChange('"+ CONTROLLER +"')\" "; %>
                <input:select default='<%= request.getParameter("clienteSelected") %>' name="cliente" options="<%=model.clienteService.listar(loggedUser)%>" attributesText="<%= str %>"/> <br>
                <span>
                    <input onclick='cambiarCombo(this)' onchange='cambiarCombo(this)' id="todosLosClientes" type="checkbox" name="todosLosClientes" <%=(loggedUser.getTipo().equals("ADMIN") || loggedUser.getTipo().equals("TSPUSER"))?"":"disabled"%>>
                    <label for="todosLosClientes">Seleccionar todos los clientes</label>
                </span></td>
                </tr>
            </table>
            <table width="100%" class="tablaInferior">
                <tr class="fila">
                    <td class="subtitulo1" width="50%" ><div align="center">TIPO DE VIAJE </div></td>
                    <td class="barratitulo"><img src="<%=BASEURL%>/images/titulo.gif" width="32" height="20">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;TIPO DE CARGA</td>
                </tr>
                <tr class="fila">
                <td ><div align="center">
                    <select name="tipoViaje" size="8" multiple="multiple" id="select" class="listmenu">
                    <% 
                    String valoresCombo [] = {"ALL","NA","RM","DM","RC","DC","RE","DE"};
                    String textoCombo [] = {"Todas","Nacional","Reexpe. Maicao","Directo Maicao","Reexpe. C&uacute;cuta","Directo C&uacute;cuta","Reexpe. Ecuador","Directo Ecuador"};
                    for( int i=0; i<valoresCombo.length; i++ ){
                        String selected = valoresCombo[i].equals(bean.get("tipoViaje"))?"selected":"";
                        out.println("<option value='"+valoresCombo[i]+"' "+selected+">"+textoCombo[i]+"</option>");
                    } 
                    %>                    
                    </select>
                </div></td>
                <td width="*"><div align="center"><input:select name="tipo_carga" default="<%= bean.get("tipo_carga").toString() %>" options="<%=model.reporteInfoClienteService.obtenerTiposDeCarga()%>" attributesText="class='listmenu' size='8' "/></div></td>
                </tr>
            </table>
            <table width="100%" class="tablaInferior" onmousemove='window.status = "";'>
                <tr class="fila">
                <td colspan="2" ><b>Estado de Viaje: </b>
                    <select name="viajes" class='listmenu'>
                    <option value="TODOS" selected>Todos </option>
                    <option value="VIA">En Via</option>
                    <option value="PORCONF">Por confirmar entrega</option>
                    <option value="CONENTR">Con Entrega</option>
                    </select>
                    <input type=hidden value = '<%=request.getParameter("nombreCliente")%>' id='nombreCliente2' name='nombreCliente'/>
                </td>
                </tr>
                <tr>
                    <td align="left" class="subtitulo1">CONSULTA DE UN DESPACHO ESPEC&Iacute;FICO</td>
                    <td align="left" class="barratitulo"><img src="<%=BASEURL%>/images/titulo.gif" width="32" height="20"></td>
                </tr>
                <tr class="fila">
                    <td align='left' width='50%'>
                    <% String id_criterios[] = {"criteriaFactCom","criteriaDocInt","criteriaOt"};
                       String values_criterios[] = {"facturacial","docinterno","ot"};
                       String texto_criterios[] = {"Factura Comercial<br>","Documento Interno<br>","OT"};
                       for( int i=0; i<id_criterios.length; i++ ){
                           String checked = values_criterios[i].equals(bean.get("criteria"))?"checked":"";
                           out.println("<input type='radio' name='criteria' id='"+id_criterios[i]+"' value='"+values_criterios[i]+"' "+checked+">"+texto_criterios[i]);
                       }
                    %>   
                    </td>
                    <td width='50%'><b>
                    Valor a Buscar:
                    <input class="textbox" type="text" id="userDefValue" name="userDefValue" value="<%= bean.get("userDefValue") %>" ></td>
                </tr>
                <tr class="fila">
                <td colspan='2' ><b>Formato de Salida: </b>
        
                    <select class='listmenu' name="displayInExcel" >
                    <option value="false" selected="selected">P&aacute;gina WEB
                    <option value="File"                     >Archivo Excel
                    </select>
                </td>
                </tr>
                <tr class="fila">
                    <td colspan="2" align="center" height='0'>
      
                    </td>
                </tr>
                <input type="hidden" name="listaTipoViaje"      value="">
	
            </table>
  
            </td>
            </tr>
        </table>
		
        <p>
        <table>
        <tr>
        <td>
            <% String evento = "ConsultarBtnClick('"+CONTROLLER+"')"; %>
            <tsp:boton id="botonConsultar" onclick="<%=evento%>" name="ConsultarBtn" value="buscar" />
        </td>
	<td width="10"></td>
        <td>
            <tsp:boton id="botonLimpiarForma" type="reset" name="LimpiarFormularioBtn"  value="restablecer" />
        </td>
        <td width="10"></td>
        <td>
            <tsp:boton value="salir" onclick="window.close()" />
        </td>
        </tr>
    </table>
	
        </p>
  
    </td>
    </tr>
    </table>
    </form>
	
	</div><%=datos[1]%>
</body>
<tsp:InitCalendar/>

  <!-- COMENTARIO DEL PROCESO DEL ARCHIVO EN EXCEL -->
  <% String comentario= request.getParameter("comentario");
     if(comentario!=null){
        out.println("<script>");
        out.println("alert('"+comentario+"');");
        out.println("</script>");
    }%>
<% }catch( Exception a ){ a.printStackTrace(); } %>
