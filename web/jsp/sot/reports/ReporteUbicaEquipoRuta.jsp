<!--
- Autor : Alejandro Payares
- Date  : Diciembre 24 del 2005
- Copyrigth Notice : Fintravalores S.A. S.A
--Descripcion : Pagina JSP que permite ver los datos del reporte de ubicacion de equipos en ruta.
-->
<%@ page errorPage="/ErrorPage.jsp" %>
<%@ page import="java.util.*" %>
<%@ page import="com.tsp.operation.model.beans.*" %>
<%@ include file="/WEB-INF/InitModel.jsp" %>
<%@ page isELIgnored ="false" %> 
<% 
boolean displayInExcel = new Boolean(request.getParameter("displayInExcel").toLowerCase()).booleanValue();
String claseCelda = "class='bordereporteSot informacion'";
if ( displayInExcel ) {
    response.setContentType("application/vnd.ms-excel");
    claseCelda = "";
} 
boolean hayFechaIni   = (request.getParameter("fechaInicial") != null);
String listaTipoViaje = request.getParameter("listaTipoViaje");
String estadoViajes   = request.getParameter("estadoViajes");
String placasTrailers = request.getParameter("placasTrailers");
String tipoBusqueda   = request.getParameter("tipoBusqueda");
String planilla       = request.getParameter("planillas");
String devoluciones = null, QUERYSTRING = null;  
%>
<html>
<head>
    <title>Reporte de Ubicaci&oacute;n y LLegada de Equipos en Ruta</title>
</head>
<link href="../css/estilostsp.css" rel="stylesheet" type="text/css">
<link href="<%=BASEURL%>/css/estilostsp.css" rel="stylesheet" type="text/css">
<script src="<%=BASEURL%>/js/tools.js" language="javascript"></script>
    <script src="<%=BASEURL%>/js/ReporteUbicaEquipoRuta.js" language="javascript"></script>
<!-- ESTA CONDICIÓN SE DEBE A QUE PARA EL REPORTE EN VISTA EXCEL NO SE MUESTRA EL ENCABEZADO VERDE
     DE LA NUEVA IMAGEN CORPORATIVA. Alejandro Payares, nov 11 de 2005. -->
<% if ( displayInExcel ) { %>
    <body>
<% } else { %>
    <body onresize="redimensionar()" onload='redimensionar()'>
    <div id="capaSuperior" style="position:absolute; width:100%; height:100px; z-index:0; left: 0px; top: 0px;">
        <jsp:include page="/WEB-INF/toptsp.jsp?encabezado=Reporte de ubicación y llegada de equipos en ruta"/>
    </div>
    <div id="capaCentral" style="position:absolute; width:100%; height:85%; z-index:0; left: 0px; top: 100px; overflow: scroll;"> 
    <% } %>
        <table border="0" align="left" cellpadding="0" cellspacing="0">
        <tr>
        <td colspan='2' align="center" valign="top">
            <table align='left' border='1' cellspacing="0" cellpadding="0">
                <tr class="subtitulo1">
          <%
          if( hayFechaIni ) { %>
                    <th align='center'>Fecha Inicial</th>
                    <th align='center'>Fecha Final</th>
            <%
          } %>
                    <th align='center'
              <%= (hayFechaIni? "" : "colspan='2'") %>>
                        Tipos de Viaje
                    </th>
                    <th align='center'>Estado de Viaje</th>
          <%
          if( placasTrailers != null ) { %>
                    <th align='center'>Lista de <%= tipoBusqueda %></th><%
          }
          if( planilla != null ) { %>
                    <th align='center'>Lista de OC(s) / Planilla(s)</th>
            <%
          } %>
                    <th align='center'
              <%= (hayFechaIni? "" : "colspan='2'") %>>
                        Ultima Actualizaci&oacute;n
                    </th>
                    <th align='center' colspan='4'>
                        Convenci&oacute;n de Colores Para el Reporte
                    </th>
                </tr>
                <tr class="fila">
          <%
          if( hayFechaIni ) { %>
                <td align='center' class='TableRowDecoration Estilo10'>
              <%= request.getParameter("fechaInicial") %>
                </td>
                <td align='center' class='TableRowDecoration Estilo10'>
              <%= request.getParameter("fechaFinal") %>
                </td><%
          } %>
                <td align='center' class='TableRowDecoration Estilo10' <%= (hayFechaIni? "" : "colspan='2'") %>>
            <%= listaTipoViaje %>
                </td>
                <td align='center' class='TableRowDecoration Estilo10'>
            <%= estadoViajes %>
                </td><%
          if( placasTrailers != null ) { %>
                <td align='center' class='TableRowDecoration Estilo10'>
              <%= placasTrailers %>
                </td><%
          }
          if( planilla != null ) { %>
                <td align='center' class='TableRowDecoration Estilo10'><%= planilla %></td><%
          } %>
                <td align='center' class='TableRowDecoration Estilo10'
              <%= (hayFechaIni? "" : "colspan='2'") %>>
                    <%@include file="/WEB-INF/lastUpdateSotDb.html" %>
                </td>
                <th class='letraresaltada' bgcolor="#FFFF8C">En Ruta</th>
                <th class='letraresaltada' bgcolor="#CAFFCA">Con Entrega</th>
                <th class='letraresaltada' bgcolor="#FF8888">Con Problemas</th>
                <th class='letraresaltada' bgcolor="#FFFFFF">Por Confirmar Salida</th>
                </tr>
            </table>
        </td>  
        </tr>
        <tr>
        <td colspan='2' align="center" valign="top"><%
      List ubicVehList = model.reporteUbicacionVehService.obtenerDatosReporte();
      Iterator ubicVehIt = ubicVehList.iterator();
      Hashtable titulos = model.reporteUbicacionVehService.obtenerTitulosDeReporte();
      String [] campos = model.reporteUbicacionVehService.obtenerCamposDeReporte();
   %>
        <div class="tblTitulo" align='left'>
            <h4><%= ubicVehList.size() %> Registros encontrados.</h4>
        </div>
        <table border="1">
            <tr class='tblTitulo'>
              <% for( int i=0; i<campos.length; i++ ){ %>
                <th align='center' class="subtitulo1"><%= titulos.get(campos[i]) %></th>
              <% } %>                
            </tr>
            <%
              String placaAnt = "";
              while( ubicVehIt.hasNext() ) {
                ReporteUbicacionEquipoRuta rptUbVeh = (ReporteUbicacionEquipoRuta)ubicVehIt.next();
                String cEstado   = rptUbVeh.getEstado();
                String grEqCabezote = rptUbVeh.getGrupoEquipoCabezote();
                String grEqTrailer  = rptUbVeh.getGrupoEquipoTrailer();
                  String cTieneDev = rptUbVeh.getDevoluciones().toUpperCase().trim();
                  if (cTieneDev.equals("SI")) {%>
            <tr bgcolor="#FF8888">
                  <%} else {%>
            <tr bgcolor="<%= (cEstado.trim().equals("En Ruta")?"#FFFF8C":( cEstado.trim().equals("Con Entrega")?"#CAFFCA":"#FFFFFF" ))%>">
                  <%}
                  for(int i=0; i<campos.length; i++ ){ 
                       if ( campos[i].equals("ultimoreporte") ){
                            if (displayInExcel || rptUbVeh.getUltimoReporte().length() <= 1 ) {
                                out.println("<td nowrap align='center' "+claseCelda+">"+rptUbVeh.getUltimoReporte()+"</td>");
                            }
                            else { 
                                String    cParam = "?estado=Trafico" +
                                            "&accion=Search&numpla=" +
                                            rptUbVeh.getNumPlanilla() +
                                            "&estador=" + rptUbVeh.getEstado() +
                                            "&origen=" + rptUbVeh.getOrigen() +
                                            "&destino=" + rptUbVeh.getDestino() +
                                            "&placa=" + rptUbVeh.getEquipo() +
                                            "&displayInExcel=" + displayInExcel ;
                                 out.print("<td nowrap align='center' "+claseCelda+"><a href=\"javascript:ConsultarClick('"+ CONTROLLER + cParam +"')\">");
                                 out.println(rptUbVeh.getUltimoReporte()+"</a></td>");
                           }
                       }
                       else { %>
            <td nowrap align='center' <%=claseCelda%>> <%= rptUbVeh.obtenerValor(campos[i]) %> </td>
                    <% }
                    }%>
            </tr>
            <% }
            // Liberamos el objeto tipo List que contiene los registros del informe
            // y los atributos tipo request utilizados.
            ubicVehList = null;
            ubicVehIt   = null;
            request.removeAttribute("fechafin");
            request.removeAttribute("listaTipoViaje");
            request.removeAttribute("estadoViajes");
            request.removeAttribute("grupoequipo");
            request.removeAttribute("placa");
            request.removeAttribute("planilla");
            request.removeAttribute("origen");
            request.removeAttribute("destino");
            request.removeAttribute("showUltReporte");
            request.removeAttribute("asociadasorigen");
            request.removeAttribute("asociadasdestino"); 
		    %>
        </table>
        </td>
        </tr>
        </table>
    <% if ( !displayInExcel ) { %>
    </div>
    <% } %>
</body>
</html>
