<%@ page errorPage="/ErrorPage.jsp" %>
<%@ page import="java.util.*" %>
<%@ page import="com.tsp.operation.model.beans.*" %>
<%@ include file="/WEB-INF/InitModel.jsp" %>
<%
Usuario loggedUser = (Usuario) session.getAttribute("Usuario");
String cNumPla = request.getParameter("numpla");
String cPlaca  = request.getParameter("placa");
String cEstador= request.getParameter("estador");
String cOrigen = request.getParameter("origen");
String cDestino= request.getParameter("destino");
if (cNumPla == null) cNumPla = "*****";
 %>
<html>
<head>
    <title>Reportes de Trafico</title>
    <link href="<%=BASEURL%>/css/estilostsp.css" rel="stylesheet" type="text/css">
    <script language="javascript" src="<%=BASEURL%>/js/boton.js"></script>
        
</head>
<body onunload='window.opener.status = "";'>
    <table border="0" align="left" cellpadding="0" cellspacing="0">
        <tr>
            <td colspan='2' align="center" valign="top">
                <table border='1' align='left'>
                    <tr>
                        <td>
                            <table border='0' cellspacing="1" cellpadding="1" bordercolor="#999999" bgcolor="#F7F5F4">
                                <tr class="subtitulo1">
                                    <th align='center' class='bordereporte'>
                                        OC/Planilla
                                    </th>
                                    <th align='center' class='bordereporte'>
                                        Equipo
                                    </th>
                                    <th align='center' class='bordereporte'>
                                        Estado
                                    </th>
                        
                                </tr>
                                <tr class='filagris'>
                                    <td align='center' class='bordereporte'>
            <%= cNumPla %>
                                    </td>
                                    <td align='center' class='bordereporte'>
            <%= cPlaca %>
                                    </td>
                                    <td align='center' class='bordereporte'>
            <%= cEstador %>
                                    </td>
                        
                                </tr>

                                <tr  class="subtitulo1">
                                    <th align='center' class='bordereporte'>
                                        Origen
                                    </th>
                                    <th align='center' class='bordereporte'>
                                        Destino
                                    </th>
                                    <th align='center' class='bordereporte'>
                                        Ultima Actualizaci&oacute;n
                                    </th>
                                </tr>
                                <tr class='filagris'>
                                    <td align='center' class='bordereporte'>
            <%= cOrigen %>
                                    </td>
                                    <td align='center' class='bordereporte'>
            <%= cDestino %>
                                    </td>
                                    <td align='center' class='bordereporte'>
                                        <%@include file="/WEB-INF/lastUpdateSotDb.html" %>
                                    </td>
                                </tr>

                            </table>
                        </td>
                    </tr>

                </table>
            </td>  
        </tr>
        <tr>
            <td>&nbsp;</td>
        </tr>
        
        <tr>
        <td colspan='2' align="center" valign="top"><%
      ReporteTrafico rptTra = null;
      List traficoList = model.traficoService.getTraficoList();
      Iterator traficoIt = traficoList.iterator();
//   out.println( model.getSQL() + "<br><br>" ); %>
            <!--      <div align='left'>
            <h3><%= traficoList.size() %> Registros encontrados.</h3>
            </div> -->
            <table border='1'>
                <tr>
                <td>
                    <table border="0" bordercolor="#999999" bgcolor="#F7F5F4">
                        <tr align='center'>
                            <td colspan="3" align="center" class="subtitulo1">Informes de Tr�fico</td>
                            <td colspan="2" align="left" class="barratitulo"><img src="<%=BASEURL%>/images/titulo.gif" width="32" height="20"></td>
                        </tr>
                        <tr class='tblTitulo'>
                            <th><div align="center">Tipo</div></th>
                            <th><div align="center">Fecha</div></th>
                            <th><div align="center">Hora</div></th>
                            <th><div align="center">Ciudad</div></th>
                        </tr>
                <%  int i=0;
                    while( traficoIt.hasNext() ) {
                    rptTra = (ReporteTrafico)traficoIt.next(); %>
                        <tr class="<%=(i % 2 == 0 )?"filagris":"filaazul"%>" onMouseOver='cambiarColorMouse(this)' style="cursor:hand">
                            <td nowrap align='center' class='bordereporte'> <%= rptTra.getTipo() %> </td>
                            <td nowrap align='center' class='bordereporte'> <%= rptTra.getFechaReporte() %> </td>
                            <td nowrap align='center' class='bordereporte'> <%= rptTra.getHoraReporte() %> </td>
                            <td nowrap align='center' class='bordereporte'> <%= rptTra.getNomCiu() %> </td>
                        </tr>
                <% i++;
                }
            // Liberamos el objeto tipo List que contiene los registros del informe
            traficoList = null;
            traficoIt   = null;
             %>
                    </table>
                </td>
                </tr>
            </table>
        </td>
        </tr>

        <tr>
            <td>
                &nbsp;
            </td>
        </tr>

        <tr>
        <td colspan='2' align="center" valign="top"><%
      ReporteTraficoOb rptTraOb = null;
      List traficoObList = model.traficoService.getTraficoObList();
      Iterator traficoObIt = traficoObList.iterator();
//   out.println( model.getSQL() + "<br><br>" ); %>
            <table border='1'>
                <tr>
                <td>
                    <table border="0"  bordercolor="#999999" bgcolor="#F7F5F4">
                        <tr align='center'>
                            <td colspan="3" align="center" class="subtitulo1">Observaciones</td>
                            <td colspan="2" align="left" class="barratitulo"><img src="<%=BASEURL%>/images/titulo.gif" width="32" height="20"></td>
                        </tr>
                        <tr class='tblTitulo'>
                            <th><div align="center">Observacion</div></th>
                            <th><div align="center">Fecha</div></th>
                            <th><div align="center">Hora</div></th>
                            <th><div align="center">Ciudad</div></th>
                        </tr>
                        <%  i=0;
                            while( traficoObIt.hasNext() ){
                            rptTraOb = (ReporteTraficoOb)traficoObIt.next(); %>
                        <tr class="<%=(i % 2 == 0 )?"filagris":"filaazul"%>" onMouseOver='cambiarColorMouse(this)' style="cursor:hand">
                            <td nowrap align='center' class='bordereporte'> <%= rptTraOb.getObservacion() %> </td>
                            <td nowrap align='center' class='bordereporte'> <%= rptTraOb.getFechaReporte() %> </td>
                            <td nowrap align='center' class='bordereporte'> <%= rptTraOb.getHoraReporte() %> </td>
                            <td nowrap align='center' class='bordereporte'> <%= rptTraOb.getNomCiu() %> </td>
                        </tr>
                        <%  i++;
                            }

            // Liberamos el objeto tipo List que contiene los registros del informe
            traficoObList = null;
            traficoObIt   = null;
             %>
                    </table>
                </td>
                </tr>
            </table>
        </td>
        </tr>


    </table>
</body>
</html>