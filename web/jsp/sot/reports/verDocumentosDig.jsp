<!--
- Autor : Ing. Diogenes Bastidas Morales
- Date  : 28 de noviembre de 2005                      
- Copyrigth Notice : Fintravalores S.A. S.A
-->
<%--
-@(#)
--Descripcion : Pagina JSP, lista de Cuentas Tipo Subledger

--%>

<%-- Declaracion de librerias--%>
<%@page contentType="text/html"%>
<%@page session="true"%>
<%@page errorPage="/error/ErrorPage.jsp"%>
<%@page import="java.util.*"%> 
<%@page import="com.tsp.operation.model.*"%>
<%@page import="com.tsp.util.*"%>
<%@page import="com.tsp.operation.model.beans.*, java.util.*"%>
<%@include file="/WEB-INF/InitModel.jsp"%>
<%@taglib uri="http://jsptags.com/tags/navigation/pager" prefix="pg" %>
 
<html>
<head>
<title>Documentos Digitalizados</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link href="<%= BASEURL %>/css/estilostsp.css" rel="stylesheet" type="text/css">
<link href="../../../css/estilostsp.css" rel="stylesheet" type="text/css">
<link href="file:///C|/Tomcat5/webapps/slt%20ING/css/estilostsp.css" rel="stylesheet" type="text/css"> 
<script type='text/javascript' src="<%= BASEURL %>/js/reporte.js"></script>
<script type='text/javascript' src="<%= BASEURL %>/js/boton.js"></script>
<script type='text/javascript' src="<%= BASEURL %>/js/script.js"></script>

</head>
<body onLoad="redimensionar();" onresize="redimensionar()">
<div id="capaSuperior" style="position:absolute; width:100%; height:100px; z-index:0; left: 0px; top: 0px;">
	<jsp:include page="/WEB-INF/toptsp.jsp?encabezado=Documentos Digitalizados"/>
</div>
<div id="capaCentral" style="position:absolute; width:100%; height:87%; z-index:0; left: 0px; top: 100px; overflow: scroll;">

  <% Usuario usuario = (Usuario) session.getAttribute("Usuario");
	List despPlanilla = model.ImagenSvc.getDespPlanilla();
    List despRemesa =  model.ImagenSvc.getDespRemesa();
    List cumpPlanilla =  model.ImagenSvc.getCumpPlanilla();
    List cumpRemesa = model.ImagenSvc.getCumpRemesa();
    String url =  BASEURL + "/documentos/imagenes/" + usuario.getLogin() + "/";
    String planilla =  request.getParameter("planilla");
    String remesa = request.getParameter("remesa");
%>
</p>
<table width="700" border="2" align="center">
    <tr>
      <td width="1009">
	  <table width="100%" align="center">
              <tr>
                <td width="373" class="subtitulo1">&nbsp;Despacho</td>
                <td width="427" class="barratitulo"><img src="<%=BASEURL%>/images/titulo.gif" width="32" height="20"></td>
              </tr>
        </table>
	  <table width="100%">
        <tr>
          <td width="50%"> 
         <%if (despPlanilla !=null ){%>
          <table width="100%"  border="1" align="center" bordercolor="#999999" bgcolor="#F7F5F4">
           <tr>
              <td colspan="2" align="center" class="subtitulo1">Planilla <%=planilla%></td>
              </tr>
            <tr class="tblTitulo">
              <td width="30%" align="center">Digitalizado</td>
              <td width="70%" align="center">Nombre</td>
            </tr>
          <%for (int i = 0; i < despPlanilla.size() ; i++)  {
				 Imagen imgdespPla = (Imagen) despPlanilla.get(i);               %>
            <tr class="<%=(i % 2 == 0 )?"filagris":"filaazul"%>" onClick="javascript: viewImagen('<%=url%><%=imgdespPla.getFileName()%>','','','')" onMouseOver='cambiarColorMouse(this)' style="cursor:hand">
              <td class="bordereporte" nowrap><%=imgdespPla.getFecha_creacion()%></td>
              <td class="bordereporte"><%=imgdespPla.getFileName()%></td>
            </tr>
          <%}%>
          </table>
        <%}else{%>
          <div align="center" class="tblTitulo">              
              No existen documentos digitalizados de la Planilla despachada
              
          </div>
          <%}%>       
        </td>
          <td width="50%">
         <%if (despRemesa !=null ){%>
          <table width="100%"  border="1" align="center" bordercolor="#999999" bgcolor="#F7F5F4">
            <tr>
              <td colspan="2" align="center" class="subtitulo1">Remesa <%=remesa%></td>
            </tr>
            <tr class="tblTitulo">
              <td width="30%" align="center">Digitalizado</td>
              <td width="70%" align="center">Nombre</td>
            </tr>
            <%for (int j = 0; j < despRemesa.size() ; j++)  {
				 Imagen imgdespRem = (Imagen) despRemesa.get(j);%>
            <tr class="<%=(j % 2 == 0 )?"filagris":"filaazul"%>" onClick="javascript: viewImagen('<%=url%><%=imgdespRem.getFileName()%>','','','')" onMouseOver='cambiarColorMouse(this)' style="cursor:hand">
              <td class="bordereporte" nowrap><%=imgdespRem.getFecha_creacion()%></td>
              <td class="bordereporte"><%=imgdespRem.getFileName()%></td>
            </tr>
  			<%}%>
          </table>
         <%}else{%>
          <div align="center" class="tblTitulo">              
              No existen documentos digitalizados de la Remesa despachada</div>
          <%}%>   
       </td>
        </tr>
      </table>
	  </td>
    </tr>
</table>
<br>
<table width="700" border="2" align="center">
    <tr>
      <td width="1009">
	  <table width="100%" align="center">
              <tr>
                <td width="373" class="subtitulo1">&nbsp;Cumplido</td>
                <td width="427" class="barratitulo"><img src="<%=BASEURL%>/images/titulo.gif" width="32" height="20"></td>
              </tr>
        </table>
	  <table width="100%">
        <tr>
          <td width="50%">
         <%if (cumpPlanilla !=null ){%>
         <table width="100%"  border="1" align="center" bordercolor="#999999" bgcolor="#F7F5F4">
            <tr>
              <td colspan="2" align="center" class="subtitulo1">Planilla <%=planilla%></td>
            </tr>
            <tr class="tblTitulo">
              <td width="30%" align="center">Digitalizado</td>
              <td width="70%" align="center">Nombre</td>
            </tr>
            <%for (int k = 0; k < cumpPlanilla.size() ; k++)  {
				 Imagen imgcumpPla = (Imagen) cumpPlanilla.get(k);%>
            <tr class="<%=(k % 2 == 0 )?"filagris":"filaazul"%>" onMouseOver='cambiarColorMouse(this)' onClick="javascript: viewImagen('<%=url%><%=imgcumpPla.getFileName()%>','','','')" style="cursor:hand">
              <td class="bordereporte" nowrap><%=imgcumpPla.getFecha_creacion()%></td>
              <td class="bordereporte"><%=imgcumpPla.getFileName()%></td>
            </tr>
			<%}%>
          </table>
		<%}else{%>
          <div align="center" class="tblTitulo">              
              No existen documentos digitalizados de la Planilla cumplida              
          </div>
          <%}%>   
        </td>
          <td width="50%">
          <%if (cumpRemesa !=null ){%>
          <table width="100%"  border="1" align="center" bordercolor="#999999" bgcolor="#F7F5F4">
            <tr>
              <td colspan="2" align="center" class="subtitulo1">Remesa <%=remesa%></td>
            </tr>
            <tr class="tblTitulo">
              <td width="30%" align="center">Digitalizado</td>
              <td width="70%" align="center">Nombre</td>
            </tr>
            <%for (int l = 0; l < cumpRemesa.size() ; l++)  {
				 Imagen imgcumRem = (Imagen) cumpRemesa.get(l);%>
            <tr class="<%=(l % 2 == 0 )?"filagris":"filaazul"%>" onMouseOver='cambiarColorMouse(this)' onClick="javascript: viewImagen('<%=url%><%=imgcumRem.getFileName()%>','','','')" style="cursor:hand">
              <td class="bordereporte" nowrap><%=imgcumRem.getFecha_creacion()%></td>
              <td class="bordereporte"><%=imgcumRem.getFileName()%></td>
            </tr>
			<%}%>
          </table>
		<%}else{%>
          <div align="center" class="tblTitulo">              
              No existen documentos digitalizados de la Remesa cumplida              
          </div>
          <%}%>   
		</td>
        </tr>
      </table>
	  </td>
    </tr>
</table>
 <br>
<table width="700" border="0" align="center">
   <tr>
     <td width="1515"><img src="<%=BASEURL%>/images/botones/salir.gif"   name="imgsalir"     onClick=" parent.close();"               onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" style="cursor:hand"></td>
   </tr>
 </table>
</div> 
</body>

</html>
