<!--
- Autor : Ivan DArio Gomez
- Date  : Diciembre 24 del 2005
- Copyrigth Notice : Fintravalores S.A. S.A
- Descripcion : Pagina JSP que permite ver los datos de las devoluciones.
-->
<%@ page session="true" %>
<%@ page errorPage="/ErrorPage.jsp" %>
<%@ page import="java.util.*" %>
<%@ page import="com.tsp.operation.model.beans.*" %>
<%@ include file="/WEB-INF/InitModel.jsp" %>
<%

String cliente = request.getParameter("cliente"); %>

<head>
  <title>Reporte de Ubicación Vehicular: Devoluciones.</title>
  <script type='text/javascript' src="<%= BASEURL %>/js/boton.js"></script>
  <% String path = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+request.getContextPath(); %>
    <link href='<%=path%>/css/estilostsp.css' rel='stylesheet' type='text/css'>

<body onresize="redimensionar()" onload='redimensionar()'>
    <div id="capaSuperior" style="position:absolute; width:100%; height:100px; z-index:0; left: 0px; top: 0px;">
        <jsp:include page="/WEB-INF/toptsp.jsp?encabezado=Reporte de Ubicacion Vehicular Clientes"/>
    </div>
    <div id="capaCentral" style="position:absolute; width:100%; height:85%; z-index:0; left: 0px; top: 100px; overflow: scroll;"> 
<table width="100%"  border="0" align="center" cellpadding="0" cellspacing="0">
  <tr>
    <td height="330" align="center" valign="top">
      <table align='left' border='1' cellspacing="0" cellpadding="0">
        <tr class="subtitulo1">
          <th align='center'>Cliente</th>
          <th align='center'>Ultima Actualizaci&oacute;n</th>
         
        </tr>
        <tr class='fila'>
          <td align='center' class='TableRowDecoration Estilo10'><%= cliente %></td>
          <td align='center' class='TableRowDecoration Estilo10'>
            <%@include file="/WEB-INF/lastUpdateSotDb.html" %>
          </td>
         
        </tr>
      </table><br>
      <p>&nbsp;</p>
      <table border="1">
        <tr class='tblTitulo'>
          <th class='subtitulo1'  nowrap>Equipo</th>
          <th class='subtitulo1'  nowrap >Planilla</th>
          <th class='subtitulo1' nowrap>Remesa</th>
          <th class='subtitulo1' nowrap>Origen</th>
          <th class='subtitulo1' nowrap>Destino</th>
          <th class='subtitulo1' nowrap>Fecha de Devolución</th>
          <th class='subtitulo1' nowrap>Cliente</th>
          <th class='subtitulo1' nowrap>Doc Internos</th>
          <th class='subtitulo1' nowrap>Pedidos</th>
          <th class='subtitulo1' nowrap>Cod. prod.</th>
          <th class='subtitulo1' nowrap>Producto</th>
          <th class='subtitulo1' nowrap>Tipo de empaque</th>
          <th class='subtitulo1' nowrap>Cant. devueltas</th>
          <th class='subtitulo1' nowrap>Nombre de la persona que devolvió</th>
          <th class='subtitulo1' nowrap>Motivo de la devolución</th>
          <th class='subtitulo1' nowrap>Responsable de la devoluci&oacute;n</th>
          <th class='subtitulo1' nowrap>N N/C N/D de XOM</th>
          <th class='subtitulo1' nowrap>Bodega Devoluci&oacute;n</th>
          <th class='subtitulo1' nowrap>Fecha Devoluci&oacute;n Bodega</th>
        </tr><%
        LinkedList devolList = model.UbicacionVehicularClientesSvc.getDevoluciones();
        if( devolList != null )
        {
          Iterator devolIt = devolList.iterator();
          out.println(
            "<div class='tblTitulo' align='left'><b>" + devolList.size() + " Registros Encontrados</b></div>.<br>"
          );
          while( devolIt.hasNext() )
          { %>
            <tr class="fila"><%
            Hashtable fila = (Hashtable) devolIt.next();
            for( int i = 1; i <=19; i++ ) { 
                    String valor = (String)fila.get(String.valueOf(i));
                    valor = valor == null? "": valor.trim();
                    valor = valor.equals("")?"-":valor;%>
              <td nowrap class='bordereporteSot informacion'>
                     <%=valor%>
              </td><%
            } %>
            </tr><%
          }
        }else{ %>
          <tr>
            <th align='center' colspan='19' class='TableRowDecoration Estilo10'>
              No se encontraron devoluciones.
            </th>
          </tr><%
        } %>
      </table>
      <p align="left"><img src="<%=BASEURL%>/images/botones/salir.gif"  name="salir" style="cursor:hand" title="Salir al Menu Principal" onMouseOver="botonOver(this);" onClick="parent.close();" onMouseOut="botonOut(this);" ></p></td>
  </tr>
</table>
</div>
</body>
</html>