<!--
- Autor : Alejandro Payares
- Date  : Febrero 18 de 2006 - 10:53 AM
- Copyrigth Notice : Fintravalores S.A. S.A
- Descripcion : Pagina JSP que permite ver el detalle del tiempo promedio entre 2 de control.
-->
<%@ page errorPage="/error/ErrorPage.jsp" %>
<%@ page import="java.util.Hashtable" %>
<%@ page import="java.util.LinkedList" %>
<%@ page import="java.util.Iterator" %>
<%@ page import="com.tsp.util.Util" %>
<%@ page import="com.tsp.util.Utility" %>
<%@ include file="/WEB-INF/InitModel.jsp" %>

<% /*if ( request.getParameter("displayInExcel").equalsIgnoreCase("true") ) {
        response.setContentType("application/vnd.ms-excel");
   }*/ %>

<html>
    <head>
        <title>.: Detalle del tiempo promedio entre 2 puestos de control :.</title>  
        <link href="<%=BASEURL%>/css/estilostsp.css" rel="stylesheet" type="text/css">
        <script src='<%=BASEURL%>/js/tools.js' language='javascript'></script>
        <link href="../../../css/estilostsp.css" rel="stylesheet" type="text/css">
    </head>
    <body onresize="redimensionar()" onload='redimensionar()'>
        <div id="capaSuperior" style="position:absolute; width:100%; height:100px; z-index:0; left: 0px; top: 0px;">
            <jsp:include page="/WEB-INF/toptsp.jsp?encabezado=DETALLE DEL TIEMPO PROMEDIO ENTRE PUESTOS DE CONTROL"/>
        </div>
        <div id="capaCentral" style="position:absolute; width:100%; height:85%; z-index:0; left: 0px; top:100px; overflow:scroll;"> 
    
            <table width="100%"  border="0" align="center" cellpadding="0" cellspacing="0">
            <tr>
            <td height="30" align="center" valign="top">          <table width="400" border="1" align="left">
                <tr class="subtitulo1">
                    <td><div align="center">Fecha del reporte</div></td>
                    <td><div align="center">Puesto de control de origen</div></td>
                    <td><div align="center">Puesto de control de destino</div></td>
                    <td><div align="center">Promedio</div></td>
                </tr>	  
                <tr class="filagris">
                    <td><div align="center"><%=Utility.getDate(6)%></div></td>
                    <td><div align="center"><%=request.getParameter("nombreOrigen")%></div></td>
                    <td><div align="center"><%=request.getParameter("nombreDestino")%></div></td>
                    <td><div align="center"><%=model.repTiemposPCService.obtenerPromedio()%></div></td>
                </tr>
            </table></td>
            </tr>
            <tr height='30'>
                <td>
                </td>
            </tr>
            <tr>
            <td>
      <% String titulos [] = {"PLANILLA","DURACION","DEMORA","TOTAL"};
         String [] campos = {"numpla","duracion","demora","total"}; %>
            <table border="1" cellpadding='1' cellspacing='1' bgcolor='white'>
     <% 
        out.println("<tr height='25' class='tblTitulo'>");
        for( int i=0; i<campos.length; i++ ){
            out.println("<td align='center' nowrap>"+titulos[i]+"</td>");
        }
        out.println("</tr>");
        LinkedList devolList = model.repTiemposPCService.obtenerDatos();
        if( devolList != null ) {
          out.println("<b class='tblTituloSinFondo'>" + devolList.size() + " Registros Encontrados</b>.<br>");
          Iterator devolIt = devolList.iterator();
          int j=0;
          while( devolIt.hasNext() ) {
            Hashtable fila = (Hashtable) devolIt.next(); 
            out.println("    <tr class='fila"+ (j%2==0?"azul":"gris")+"' onMouseDown='cambiarColor(this)' style='cursor:hand'>");
            for( int i = 0; i < campos.length; i++ ) {
                out.print("      <td class='bordereporte' nowrap>");
                String str = ((String)fila.get(campos[i]));
                str = str == null? "":str;
                str = str.equals("0099-01-01")?"":str;
                out.print(str.equals("")?"-":str);
                out.println("      </td>");
            } 
            out.println("    </tr>");
            j++;
          }
          //model.repTiemposPCService.borrarDatos();
        }
        else{ %>
            <td colspan='<%=campos.length%>' class='fondotabla' bgcolor='#ECE9D8'>
                No se encontraron datos.
            </td>
    <%  } %>
            </table>
            </td>
            </tr>
            </table>
            <br><br>
        </div>
    </body>
</html>
