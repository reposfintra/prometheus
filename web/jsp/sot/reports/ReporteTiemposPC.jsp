<!--
- Autor : Alejandro Payares
- Date  : Febrero 14 de 2006 - 8:31 AM
- Copyrigth Notice : Fintravalores S.A. S.A
- Descripcion : Pagina JSP que permite ver los datos del reporte de tiempos entre puestos de control.
-->
<%@ page errorPage="/error/ErrorPage.jsp" %>
<%@ page import="java.util.Hashtable" %>
<%@ page import="java.util.LinkedList" %>
<%@ page import="java.util.Iterator" %>
<%@ page import="com.tsp.util.Util" %>
<%@ page import="com.tsp.util.Utility" %>
<%@ include file="/WEB-INF/InitModel.jsp" %>

<% /*if ( request.getParameter("displayInExcel").equalsIgnoreCase("true") ) {
        response.setContentType("application/vnd.ms-excel");
   }*/ %>

<html>
    <head>
        <title>.: Reporte de tiempos entre puestos de control :.</title>  
        <link href="<%=BASEURL%>/css/estilostsp.css" rel="stylesheet" type="text/css">
        <script src='<%=BASEURL%>/js/tools.js' language='javascript'></script>
        <link href="../../../css/estilostsp.css" rel="stylesheet" type="text/css">
    </head>
    <body onresize="redimensionar()" onload='redimensionar()'>
        <div id="capaSuperior" style="position:absolute; width:100%; height:100px; z-index:0; left: 0px; top: 0px;">
            <jsp:include page="/WEB-INF/toptsp.jsp?encabezado=REPORTE DE TIEMPOS ENTRE PUESTOS DE CONTROL"/>
        </div>
        <div id="capaCentral" style="position:absolute; width:100%; height:85%; z-index:0; left: 0px; top:100px; overflow:scroll;"> 
    
            <table width="100%"  border="0" align="center" cellpadding="0" cellspacing="0">
            <tr>
            <td height="30" align="center" valign="top">          <table width="400" border="1" align="left">
                <tr class="subtitulo1">
                    <td><div align="center">Fecha del reporte</div></td>
                    <% if (request.getParameter("inicio") != null ){ %>
                        <td><div align="center">Fecha inicial </div></td>
                        <td><div align="center">Fecha final </div></td>
                    <% } %>
                </tr>	  
                <tr class="filagris">
                    <td><div align="center"><%=Utility.getDate(6)%></div></td>
                    <% if (request.getParameter("inicio") != null ){ %>
                        <td><div align="center"><%=request.getParameter("inicio")%></div></td>
                        <td><div align="center"><%=request.getParameter("fin")%></div></td>
                    <% } %>			  
                </tr>
            </table></td>
            </tr>
            <tr height='30'>
                <td>
                </td>
            </tr>
            <tr>
            <td>
      <% String titulos [] = {"PLANILLA","RUTA","PUESTO DE CONTROL 1","CODIGO MIMS PC 1","FECHA Y HORA PC 1","PUESTO DE CONTROL 2","CODIGO MIMS PC 2","FECHA Y HORA PC 2","TIEMPO DEL TRAMO"};
         String campos [] = {"planilla","ruta","puesto_de_control","codmims","hora_pc","puesto_de_control2","codmims2","hora_pc2","diferencia"}; %>
            <table border="1" cellpadding='1' cellspacing='1' bgcolor='white'>
     <% 
        out.println("<tr height='25' class='tblTitulo'>");
        for( int i=0; i<campos.length; i++ ){
            out.println("<td align='center' nowrap>"+titulos[i]+"</td>");
        }
        out.println("</tr>");
        LinkedList devolList = model.repTiemposPCService.obtenerDatos();
        if( devolList != null ) {
          out.println("<b class='tblTituloSinFondo'>" + devolList.size() + " Registros Encontrados</b>.<br>");
          Iterator devolIt = devolList.iterator();
          int j=0;
          while( devolIt.hasNext() ) {
            Hashtable fila = (Hashtable) devolIt.next(); 
            out.println("    <tr class='fila"+ (j%2==0?"azul":"gris")+"' onMouseDown='cambiarColor(this)' style='cursor:hand'>");
            for( int i = 0; i < campos.length; i++ ) {
                out.print("      <td class='bordereporte' "+(campos[i].startsWith("codmims")?"align=center":"")+" nowrap>");
                String str = ((String)fila.get(campos[i]));
                str = str == null? "":str;
                str = str.equals("0099-01-01")?"":str;
                out.print(str.equals("")?"-":str);
                out.println("      </td>");
            } 
            out.println("    </tr>");
            j++;
          }
          model.repTiemposPCService.borrarDatos();
        }
        else{ %>
            <td colspan='<%=campos.length%>' class='fondotabla' bgcolor='#ECE9D8'>
                No se encontraron datos.
            </td>
    <%  } %>
            </table>
            </td>
            </tr>
            </table>
        </div>
    </body>
</html>
