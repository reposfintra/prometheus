<!--
- Autor : Ivan Dariio Gomez
- Date  : Febrero 2 del 2005
- Copyrigth Notice : Fintravalores S.A. S.A
- Descripcion : Pagina JSP que permite ver los datos del reporte de ubicacion de equipos en ruta.
-->
<%@ page import="java.util.*" %>
<%@ page import="com.tsp.operation.model.beans.*" %>
<%@ include file="/WEB-INF/InitModel.jsp" %>
<%@ page errorPage="/error/ErrorPage.jsp" %>
<%@ page isELIgnored ="false" %> 
<%! private double parseDouble(String str){
        try {
        return Double.parseDouble(str);
        }
        catch( NumberFormatException ex ){return 0;}
    }
%>
<% 
boolean displayInExcel = new Boolean(request.getParameter("displayInExcel").toLowerCase()).booleanValue();
String claseCelda = "class='bordereporteSot informacion'";
if ( displayInExcel ) {
    response.setContentType("application/vnd.ms-excel");
    claseCelda = "";
} 
boolean hayFechaIni   = (request.getParameter("fechaini") != null);
String listaTipoViaje = request.getParameter("tipoViaje");
listaTipoViaje = (listaTipoViaje.equals("2")?"NA":
                 (listaTipoViaje.equals("3")?"RM":
                 (listaTipoViaje.equals("4")?"DM":
                 (listaTipoViaje.equals("5")?"RC":
                 (listaTipoViaje.equals("6")?"DC":
                 (listaTipoViaje.equals("7")?"RE":"TODOS"))))));
String estadoViajes   = request.getParameter("viajes");
String placasTrailers = request.getParameter("placasTrailers");
String tipoBusqueda   = request.getParameter("tipoBusqueda");
String planilla       = request.getParameter("planillas");

%>
<html>
<head>
    <title>Reporte de Ubicaci&oacute;n Vehicular Propietario</title>
</head>
<link href="../css/estilostsp.css" rel="stylesheet" type="text/css">
<link href="<%=BASEURL%>/css/estilostsp.css" rel="stylesheet" type="text/css">
<script src="<%=BASEURL%>/js/tools.js" language="javascript"></script>
    <script src="<%=BASEURL%>/js/ReporteUbicaEquipoRuta.js" language="javascript"></script>
<!-- ESTA CONDICIÓN SE DEBE A QUE PARA EL REPORTE EN VISTA EXCEL NO SE MUESTRA EL ENCABEZADO VERDE
     DE LA NUEVA IMAGEN CORPORATIVA. Alejandro Payares, nov 11 de 2005. -->
<% if ( displayInExcel ) { %>
    <body>
<% } else { %>
    <body onresize="redimensionar()" onload='redimensionar()'>
    <div id="capaSuperior" style="position:absolute; width:100%; height:100px; z-index:0; left: 0px; top: 0px;">
        <jsp:include page="/WEB-INF/toptsp.jsp?encabezado=Reporte de ubicación y llegada de equipos en ruta"/>
    </div>
    <div id="capaCentral" style="position:absolute; width:100%; height:85%; z-index:0; left: 0px; top: 100px; overflow: scroll;"> 
    <% } %>
        <table border="0" align="left" cellpadding="0" cellspacing="0">
        <tr>
        <td colspan='2' align="center" valign="top">
            <table align='left' border='1' cellspacing="0" cellpadding="0">
                <tr class="subtitulo1">
          <%
          if( hayFechaIni ) { %>
                    <th align='center'>Fecha Inicial</th>
                    <th align='center'>Fecha Final</th>
            <%
          } %>
                    <th align='center'
              <%= (hayFechaIni? "" : "colspan='2'") %>>
                        Tipos de Viaje
                    </th>
                    <th align='center'>Estado de Viaje</th>
          <%
          if( placasTrailers != null ) { %>
                    <th align='center'>Lista de <%= tipoBusqueda %></th><%
          }
          if( planilla != null ) { %>
                    <th align='center'>Lista de OC(s) / Planilla(s)</th>
            <%
          } %>
                    <th align='center'
              <%= (hayFechaIni? "" : "colspan='2'") %>>
                        Ultima Actualizaci&oacute;n
                    </th>
                    <th align='center' colspan='5'>
                        Convenci&oacute;n de Colores Para el Reporte
                    </th>
                </tr>
                <tr class="fila">
          <%
          if( hayFechaIni ) { %>
                <td align='center' class='TableRowDecoration Estilo10'>
              <%= request.getParameter("fechaini") %>
                </td>
                <td align='center' class='TableRowDecoration Estilo10'>
              <%= request.getParameter("fechafin") %>
                </td><%
          } %>
                <td align='center' class='TableRowDecoration Estilo10' <%= (hayFechaIni? "" : "colspan='2'") %>>
            <%= listaTipoViaje %>
                </td>
                <td align='center' class='TableRowDecoration Estilo10'>
            <%= estadoViajes %>
                </td><%
          if( placasTrailers != null ) { %>
                <td align='center' class='TableRowDecoration Estilo10'>
              <%= placasTrailers %>
                </td><%
          }
          if( planilla != null ) { %>
                <td align='center' class='TableRowDecoration Estilo10'><%= planilla %></td><%
          } %>
                <td align='center' class='TableRowDecoration Estilo10'
              <%= (hayFechaIni? "" : "colspan='2'") %>>
                    <%@include file="/WEB-INF/lastUpdateSotDb.html" %>
                </td>
                <th class='letraresaltada' bgcolor="#FFFF8C">En Ruta</th>
                <th class='letraresaltada' bgcolor="#CAFFCA">Con Entrega</th>
                <th class='letraresaltada' bgcolor="#FF8888">Con Problemas</th>
                <th class='letraresaltada' bgcolor="#FFFFFF">Por Confirmar Salida</th>
                <th class='letraresaltada' bgcolor="#FFB775">Planilla Anulada</th>
                </tr>
            </table>
        </td>  
        </tr>
        <tr>
        <td colspan='2' align="center" valign="top"><%
      List ubicVehList = model.UbicaVehicuPorpSvc.obtenerDatosReporte();
      Iterator ubicVehIt = ubicVehList.iterator();
      Hashtable titulos = model.UbicaVehicuPorpSvc.obtenerTitulosDeReporte();
      String [] campos = model.UbicaVehicuPorpSvc.obtenerCamposDeReporte();
   %>
        <div class="tblTitulo" align='left'>
            <h4><%= ubicVehList.size() %> Registros encontrados.</h4>
        </div>
        <table border="1">
            <tr class='tblTitulo'>
              <% for( int i=0; i<campos.length; i++ ){ %>
                <th align='center' class="subtitulo1"><%= titulos.get(campos[i]) %></th>
              <% } %>                
            </tr>
            <%
              double sumaPlanillas = 0;
              double sumaSaldos = 0;
              String placaAnt = "";
              while( ubicVehIt.hasNext() ) {
                UbicacionVehicularPropietario rptUbVeh = (UbicacionVehicularPropietario)ubicVehIt.next();
                String cEstado   = rptUbVeh.obtenerValor("estado").trim();
                 %>
            <tr bgcolor="<%= cEstado.equals("En Ruta")? "#FFFF8C": 
                                cEstado.equals("Con Entrega")? "#CAFFCA": 
                                    cEstado.equals("Anulada") ?"#FFB775":"#FFFFFF" %>">
                  <%
                  for(int i=0; i<campos.length; i++ ){ 
                       if ( campos[i].equals("ultimoreporte") ){
                            if (displayInExcel || rptUbVeh.getUltimoReporte().length() <= 1 ) {
                                out.println("<td nowrap align='center' "+claseCelda+">"+rptUbVeh.getUltimoReporte()+"</td>");
                            }
                            else { 
                                String    cParam = "?estado=Trafico" +
                                            "&accion=Search&numpla=" +
                                            rptUbVeh.getNumPlanilla() +
                                            "&estador=" + rptUbVeh.getEstado() +
                                            "&origen=" + rptUbVeh.getOrigen() +
                                            "&destino=" + rptUbVeh.getDestino() +
                                            "&placa=" + rptUbVeh.getEquipo() +
                                            "&displayInExcel=" + displayInExcel ;
                                 out.print("<td nowrap align='center' "+claseCelda+"><a href=\"javascript:ConsultarClick('"+ CONTROLLER + cParam +"')\">");
                                 out.println(rptUbVeh.getUltimoReporte()+"</a></td>");
                           }
                       }
                       else if ( campos[i].equals("saldo" ) && "COL".equals(rptUbVeh.obtenerValor("base")) && (!cEstado.equals("Anulada")) ) {
                           String link = CONTROLLER + "?estado=Liquidar&accion=OC&c_oc="+rptUbVeh.getNumPlanilla();
                           out.println("<td nowrap align='right' "+claseCelda+"><a href=\"javascript:verReporteFinanciero('"+ link + "');\">"+rptUbVeh.getSaldoPlanilla()+"</a></td>");
                       }
                       else if ( campos[i].equals("valorPlanilla" ) || campos[i].equals("saldo" ) ) {
                           out.println("<td nowrap align='right' "+claseCelda+"> "+ rptUbVeh.obtenerValor(campos[i]) + "</td>");
                       }
                       else { %>
                            <td nowrap align='center' <%=claseCelda%>> <%= rptUbVeh.obtenerValor(campos[i]) %> </td>
                    <% }
                    }%>
            </tr>
            <% 
              sumaPlanillas += parseDouble(rptUbVeh.obtenerValor("valorPlanilla").replaceAll(",",""));
              sumaSaldos += parseDouble(rptUbVeh.obtenerValor("saldo").replaceAll(",",""));
              }
              out.println("<tr class='tblTitulo'><td colspan='3' align='center'>TOTALES</td><td align='right'>"+model.UbicaVehicuPorpSvc.separarPorMiles(sumaPlanillas)+"</td><td align='right'>"+model.UbicaVehicuPorpSvc.separarPorMiles(sumaSaldos)+"</td>");
            // Liberamos el objeto tipo List que contiene los registros del informe
            // y los atributos tipo request utilizados.
            ubicVehList = null;
            ubicVehIt   = null;
                %>
        </table>
        </td>
        </tr>
        </table>
    <% if ( !displayInExcel ) { %>
    </div>
    <% } %>
</body>
</html>
