<!--
- Autor : Alejandro Payares
- Date  : Enero 6 de 2006 - 7:37 PM
- Copyrigth Notice : Fintravalores S.A. S.A
- Descripcion : Pagina JSP que permite ver los datos del reporte de devoluciones.
-->
<%@ page errorPage="/error/ErrorPage.jsp" %>
<%@ page import="java.util.Hashtable" %>
<%@ page import="java.util.LinkedList" %>
<%@ page import="java.util.Iterator" %>
<%@ page import="com.tsp.util.Util" %>
<%@ include file="/WEB-INF/InitModel.jsp" %>

<%  //response.setHeader("Cache-Control","");
   if ( request.getParameter("displayInExcel").equalsIgnoreCase("true") ) {
        response.setContentType("application/vnd.ms-excel");
   } %>

<html>
<head>
    <title>Reporte de Devoluciones Discrepancias.</title>  
    <link href="<%=BASEURL%>/css/estilostsp.css" rel="stylesheet" type="text/css">
    <script src='<%=BASEURL%>/js/tools.js' language='javascript'></script>
</head>
<body onresize="redimensionar()" onload='redimensionar()'>
    <div id="capaSuperior" style="position:absolute; width:100%; height:100px; z-index:0; left: 0px; top: 0px;">
        <jsp:include page="/WEB-INF/toptsp.jsp?encabezado=REPORTE DE DEVOLUCIONES DISCREPANCIA"/>
    </div>
    <div id="capaCentral" style="position:absolute; width:100%; height:85%; z-index:0; left: 0px; top:100px; overflow:scroll;"> 
    
        <table width="100%"  border="0" align="center" cellpadding="0" cellspacing="0">
        <tr>
            <td height="30" align="center" valign="top">
                <table align='left' border='1' cellspacing="0" cellpadding="0" width='665'>
	<%  if ( request.getParameter("nroPedido") != null && request.getParameter("nroPedido").length() > 0 ){ %>
                    <tr class='tblTitulo'>
                        <td colspan="6" align='center' height='30'>N�mero de Pedido</td>
                    </tr>
                    <tr >
                        <td colspan="6" align='center'>
                <%= request.getParameter("nroPedido") %>
                        </td>
                    </tr>
	<% } %>	
                    <tr class='subtitulo1'>
                        <td align='center' height='30'>Cliente</td><%
              if( request.getParameter("fechaini") != null ) { %>
                        <td align='center'>Fecha Inicial</td>
                        <td align='center'>Fecha Final</td><%
              } %>
                        <td align='center'>Tipo de Devoluci�n</td>
                        <td align='center'>Ultima Actualizaci&oacute;n</td>
                        <td align='center'>Fecha de Generaci&oacute;n</td>
                    </tr>
                    <tr class='filaazul'>
                        <td align='center'  class='bordereporte'>
                <%= request.getParameter("cliente") %>
                        </td><%
              if( request.getParameter("fechaini") != null ) { %>
                        <td align='center' class='bordereporte'>
                  <%= request.getParameter("fechaini") %>
                        </td>
                        <td align='center' class='bordereporte'>
                  <%= request.getParameter("fechafin") %>
                        </td><%
              } %>
                        <td align='center' class='bordereporte'>
                <%= request.getParameter("tipoDevolucion") %>
                        </td>
                        <td align='center' class='bordereporte'>
                            <%@include file="/WEB-INF/lastUpdateSotDb.html" %>
                        </td>
                        <td align='center' class='bordereporte'><%= Util.getFullDate() %></td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr height='30'>
            <td>
            </td>
        </tr>
        <tr>
        <td>
      <% Hashtable titulos = model.reporteDevolucionesDiscrepService.obtenerTitulosDeReporteDevoluciones();
         String campos [] = model.reporteDevolucionesDiscrepService.obtenerCamposReporteDevoluciones(); %>
        <table border="1" cellpadding='1' cellspacing='1' bgcolor='white'>
     <% 
        out.println("<tr height='25' class='tblTitulo'>");
        for( int i=0; i<campos.length; i++ ){
            out.println("<td align='center' nowrap>"+titulos.get(campos[i])+"</td>");
        }            
        out.println("</tr>");
        LinkedList devolList = model.reporteDevolucionesDiscrepService.obtenerDatosDevoluciones();
        if( devolList != null ) {
          out.println("<b class='tblTituloSinFondo'>" + devolList.size() + " Registros Encontrados</b>.<br>");
          Iterator devolIt = devolList.iterator();
          int j=0;
          while( devolIt.hasNext() ) {
            Hashtable fila = (Hashtable) devolIt.next(); 
            out.println("    <tr class='fila"+ (j%2==0?"azul":"gris")+"' onMouseOver='cambiarColorMouse(this)' style='cursor:hand'>");
            for( int i = 0; i < campos.length; i++ ) {
                out.print("      <td class='bordereporte' nowrap><font face='Verdana, Arial, Helvetica, sans-serif' size='-2'>");
                String str = ((String)fila.get(campos[i]));
                str = str == null? "":str;
                str = str.equals("0099-01-01")?"":str;
                out.print(str.equals("")?"-":str);
                out.println("      </font></td>");
            } 
            out.println("    </tr>");
            j++;
          }
        }
        else{ %>
        <td colspan='<%=campos.length%>' class='fondotabla' bgcolor='#ECE9D8'>
        No se encontraron devoluciones.
        </td>
    <%  } %>
        </table>
        </td>
        </tr>
        </table>
    </div>
</body>
</html>
