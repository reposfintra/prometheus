<%@ page import="java.util.*" %>
<%@ page import="com.tsp.operation.model.beans.*" %>
<%@ include file="/WEB-INF/InitModel.jsp" %>
<%
Usuario loggedUser = (Usuario) session.getAttribute("userLoggedIn");
TraficoDetallado trafico;
Vector reporte;
String dir = "";

if (model.traficoService.getTraficoDetallado().size() > 0){
   reporte = new Vector(model.traficoService.getTraficoDetallado());
}
else{
    %>
    <p>No hay datos para el reporte</p>
    <%
    reporte = new Vector();
}

String displayInExcel = request.getParameter("displayInExcel");
displayInExcel = displayInExcel != null ? displayInExcel : "";
if (displayInExcel.equals("true")){
    response.setContentType("application/vnd.ms-excel");
}

dir = "http://"+request.getLocalName()+":"+request.getLocalPort()+request.getContextPath();
%>
<html>
<head>
    <title>Reportes Detallado de Trafico</title>
    <link href="<%=dir%>/css/EstilosReportes.css" rel="stylesheet" type="text/css">
    <link href="<%=BASEURL%>/css/estilostsp.css" rel="stylesheet" type="text/css">
    <script language="javascript" src="<%=BASEURL%>/js/boton.js"></script>
</head>
<body onunload='window.opener.status = "";'>
<% for (int i = 0; i < reporte.size(); i++){ 
        trafico = (TraficoDetallado)reporte.get(i);%>
    <table border="0" align="left" cellpadding="0" cellspacing="0">
 
        <tr>
            <td colspan='2' align="center" valign="top">
            <table border='1' align='left'>
            <tr>
            <td>                
            <table border='0' cellspacing="1" cellpadding="1" bordercolor="#999999" bgcolor="#F7F5F4">
            <tr class="subtitulo1">
                <th align='center' class='bordereporte'>
                    OC/Planilla
                </th>
                <th align='center' class='bordereporte'>
                    Equipo
                </th>
                <th align='center' class='bordereporte'>
                    Estado
                </th>
                        
            </tr>
            <tr class='filagris'>
            <td align='center' class='bordereporte'>
            <%= trafico.getNumpla() %>
            </td>
            <td align='center' class='bordereporte'>
            <%= trafico.getPlaca() %>
            </td>
            <td align='center' class='bordereporte'>
            <%= trafico.getEstado() %>
            </td>
                        
        </tr>

                        <tr  class="subtitulo1">
                            <th align='center' class='bordereporte'>
                                Origen
                            </th>
                            <th align='center' class='bordereporte'>
                                Destino
                            </th>
                            <th align='center' class='bordereporte'>
                                Ultima Actualizaci&oacute;n
                            </th>
                        </tr>
                        <tr class='filagris'>
                        <td align='center' class='bordereporte'>
            <%= trafico.getOripla() %>
                        </td>
                        <td align='center' class='bordereporte'>
            <%= trafico.getDespla() %>
                        </td>
                        <td align='center' class='bordereporte'>
                            <%@include file="/WEB-INF/lastUpdateSotDb.html" %>
                        </td>
                        </tr>

                    </table>  
                            
                </td>
                </tr>

            </table>
     
        </td>  
        </tr>
        
         <tr>
            <td>&nbsp;</td>
        </tr>
        
        <tr>
        <td colspan='2' align="left" valign="top"><%
      ReporteTrafico rptTra = null;
      List traficoList = trafico.getTraficoList();
      Iterator traficoIt = traficoList.iterator();
//   out.println( model.getSQL() + "<br><br>" ); %>
            <!--      <div align='left'>
            <h3><%= traficoList.size() %> Registros encontrados.</h3>
            </div> -->
            <table border='1'>
                <tr>
                <td>
                    <table border="0" bordercolor="#999999" bgcolor="#F7F5F4">
                        <tr align='center'>
                            <td colspan="3" align="center" class="subtitulo1">Informes de Tr�fico</td>
                            <td colspan="2" align="left" class="barratitulo"><img src="<%=BASEURL%>/images/titulo.gif" width="32" height="20"></td>
                        </tr>
                        <tr class='tblTitulo'>
                            <th><div align="center">Tipo</div></th>
                            <th><div align="center">Fecha</div></th>
                            <th><div align="center">Hora</div></th>
                            <th><div align="center">Ciudad</div></th>
                        </tr>
                <%  int j=0;
                    while( traficoIt.hasNext() ) {
                    rptTra = (ReporteTrafico)traficoIt.next(); %>
                        <tr class="<%=(j % 2 == 0 )?"filagris":"filaazul"%>" onMouseOver='cambiarColorMouse(this)' style="cursor:hand">
                            <td nowrap align='center' class='bordereporte'> <%= rptTra.getTipo() %> </td>
                            <td nowrap align='center' class='bordereporte'> <%= rptTra.getFechaReporte() %> </td>
                            <td nowrap align='center' class='bordereporte'> <%= rptTra.getHoraReporte() %> </td>
                            <td nowrap align='center' class='bordereporte'> <%= rptTra.getNomCiu() %> </td>
                        </tr>
                <% j++;
                }
            // Liberamos el objeto tipo List que contiene los registros del informe
            traficoList = null;
            traficoIt   = null;
             %>
                    </table>
                </td>
                </tr>
            </table>
        </td>
        </tr>

        <tr><td>    &nbsp;</td></tr>

        <tr>
        <td colspan='2' align="left" valign="top"><%
      ReporteTraficoOb rptTraOb = null;
      List traficoObList = trafico.getTraficoObList();
      Iterator traficoObIt = traficoObList.iterator();
//   out.println( model.getSQL() + "<br><br>" ); %>
            <table border='1'>
                <tr>
                <td>
                    <table border="0"  bordercolor="#999999" bgcolor="#F7F5F4">
                        <tr align='center'>
                            <td colspan="3" align="center" class="subtitulo1">Observaciones</td>
                            <td colspan="2" align="left" class="barratitulo"><img src="<%=BASEURL%>/images/titulo.gif" width="32" height="20"></td>
                        </tr>
                        <tr class='tblTitulo'>
                            <th><div align="center">Observacion</div></th>
                            <th><div align="center">Fecha</div></th>
                            <th><div align="center">Hora</div></th>
                            <th><div align="center">Ciudad</div></th>
                        </tr>
                        <%int k=0;
                            while( traficoObIt.hasNext() ){
                            rptTraOb = (ReporteTraficoOb)traficoObIt.next(); %>
                        <tr class="<%=(k % 2 == 0 )?"filagris":"filaazul"%>" onMouseOver='cambiarColorMouse(this)' style="cursor:hand">
                            <td nowrap align='center' class='bordereporte'> <%= rptTraOb.getObservacion() %> </td>
                            <td nowrap align='center' class='bordereporte'> <%= rptTraOb.getFechaReporte() %> </td>
                            <td nowrap align='center' class='bordereporte'> <%= rptTraOb.getHoraReporte() %> </td>
                            <td nowrap align='center' class='bordereporte'> <%= rptTraOb.getNomCiu() %> </td>
                        </tr>
                        <%  k++;
                            }

            // Liberamos el objeto tipo List que contiene los registros del informe
            traficoObList = null;
            traficoObIt   = null;
             %>
            </table>
  
        </td>
        </tr>
        
    </table>
    <br>
    <br>
    <%}%>
</body>
</html>
