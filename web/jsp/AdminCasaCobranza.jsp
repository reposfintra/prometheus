<%-- 
    Document   : AdminCasaCobranza
    Created on : 28/10/2019, 12:08:34 PM
    Author     : mcamargo
--%>

<%@page contentType="text/html" pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
        <link type="text/css" rel="stylesheet" href="./css/jquery/jquery-ui/jquery-ui.css" />
        <link href="./css/popup.css" rel="stylesheet" type="text/css">

        <script type="text/javascript" src="./js/jquery/jquery.jqGrid/js/jquery.min.js"></script> 
        <script type="text/javascript" src="./js/jquery-ui-1.8.5.custom.min.js"></script>
        
          <link type="text/css" rel="stylesheet" href="./css/contratos.css " />
     
        <!--jqgrid--> 
        <script type="text/javascript" src="./js/jquery/jquery.jqGrid/js/grid.locale-es.js"></script>
        <script type="text/javascript" src="./js/jquery/jquery.jqGrid/js/jquery.jqGrid.min.js"></script>
        <script src="./js/AdminCasaCobranza.js" type="text/javascript"></script>
        <title>Administrador de casa de cobranza</title>
        <style>
            .filtro{
                width: 650px;
            }
            .filtro2{
                width: 300px;
                margin-left: 10px;
            }

            .header_filtro{
                text-align: center;   
                border: 1px solid #234684;
                background: #2A88C8;
                color: #ffffff;
                font-weight: bold;
                border-radius: 8px 8px 0px 0px;
            }

            .body_filtro{                
                background: white; 
                border:1px solid #2A88C8;
                height: 140px;
                border-radius: 0px 0px 8px 8px;
            }

           

            label{
                display: block;
                text-align: center;
                font-family: 'Lucida Grande','Lucida Sans',Arial,sans-serif;
                font-size: 15px;
            }

            .caja{
                float:left;
                margin-left:10px;
                margin-right:10px;
                margin-top: 8px;
            }
            input[type=text],select{
                padding: 5px 5px;
                margin: 8px auto;
                border: 1px solid #ccc;
                border-radius: 4px;
                font-size: 15px;
                color: black;
            }
            .select {
                font-size: 12px;
                background: #2A88C8;
                color: white;
                font-weight: bold;
            }
            .button {
                padding: 8px 8px;
                font-size: 12px;
                cursor: pointer;
                text-align: center;
                color: #fff;
                background-color: #2a88c8;
                border: none;
                border-radius:8px;
                margin: 22px auto;
                font-weight: bold;
                width: 80px;
            }
            
        </style>
    </head>
    <body>
        <div>
            <jsp:include page="/WEB-INF/toptsp.jsp?encabezado=Reporte Cartera"/>
        </div>
    <center>
        <div id="capaCentral" style=" width:90%; height:100%;">
            <br>
            <center>
                <table id="tabla_casa_cobranza"></table>
                <div id="page_tabla_casa_cobranza"></div>    
            </center>
            <div id="dialogLoading" style="display:none;">
                <p  style="font-size: 12px;text-align:justify;" id="msj2">Texto </p> <br/>
                <center>
                    <img src="./images/cargandoCM.gif"/>
                </center>
            </div>
            <div id="dialogMsj" title="Mensaje" style="display:none;">
                <p style="font-size: 12px;text-align:justify;" id="msj" > Texto </p>
            </div>      
        </div>
        
        <div id="div_config_casa_cobranza"  style="width: 530px;display:none;" > 
                <table aling="center" style=" width: 100%" >
                    <tr style="display:none;"><td><label>Id<span style="color:red;">*</span></label></td><td><input type="text"  id="id_casa" /></td></tr>
        <tr><td><label>Nit<span style="color:red;">*</span></label></td><td><input type="text"  id="nit"/></td></tr>
        <tr><td><label>Nombre<span style="color:red;">*</span></label></td><td><input type="text"  id="nombre"/></td></tr>
        <tr><td><label>Direccion<span style="color:red;">*</span></label></td><td><input type="text"  id="direccion"/></td></tr>
        <tr><td><label>Telefono<span style="color:red;">*</span></label></td><td><input type="text"  id="telefono"/></td></tr>
        <tr><td><label>Nombre contacto 1<span style="color:red;">*</span></label></td><td><input type="text"  id="nombre_contacto1"/></td></tr>
        <tr><td><label>Telefono contacto 1</label></td><td><input type="text"  id="telefono_contacto1"/></td></tr>
        <tr><td><label>Cargo contacto 1</label></td><td><input type="text"  id="cargo_contacto1"/></td></tr>
        <tr><td><label>Email contacto 1<span style="color:red;">*</span></label></td><td><input type="text"  id="email_contacto1"/></td></tr>
        <tr><td><label>Nombre contacto 2</label></td><td><input type="text"  id="nombre_contacto2"/></td></tr>
        <tr><td><label>Telefono contacto 2</label></td><td><input type="text"  id="telefono_contacto2"/></td></tr>
        <tr><td><label>Cargo contacto 2</label></td><td><input type="text"  id="cargo_contacto2"/></td></tr>
        <tr><td><label>Email contacto 2</label></td><td><input type="text"  id="email_contacto2"/></td></tr>
        <tr><td><label>Nombre contacto 3</label></td><td><input type="text"  id="nombre_contacto3"/></td></tr>
        <tr><td><label>Telefono contacto 3</label></td><td><input type="text"  id="telefono_contacto3"/></td></tr>
        <tr><td><label>Cargo contacto 3</label></td><td><input type="text"  id="cargo_contacto3"/></td></tr>
        <tr><td><label>Email contacto 3</label></td><td><input type="text"  id="email_contacto3"/></td></tr>
                    <hr>
                </table> 
        </div> 
    </center>
    </body>
</html>
