<%-- 
    Document   : cajasEfectivo
    Created on : 2/02/2012, 11:36:25 AM
    Author     : darrieta
--%>

<%@page import="com.tsp.operation.model.beans.Agencia"%>
<%@page import="com.tsp.operation.model.beans.CajaPago"%>
<%@page contentType="text/html" pageEncoding="ISO-8859-1"%>
<%@page import="com.tsp.util.Util"%>
<%@page import="com.tsp.util.UtilFinanzas"%>
<%@include file="/WEB-INF/InitModel.jsp"%>
<%@page import="com.tsp.operation.model.services.CajasEfectivoService"%>
<%@page import="java.util.ArrayList"%>
<%@page import="java.util.Date"%>
<%@page import="java.util.TreeMap"%>
<%@page import="com.tsp.operation.model.beans.Usuario"%>
<%@taglib uri="http://jakarta.apache.org/taglibs/input-0.90" prefix="input" %>
<!DOCTYPE html>
<%
Usuario usuario = (Usuario)session.getAttribute("Usuario");
String agenciaUsuario = (request.getParameter("agencia")==null)?usuario.getId_agencia():request.getParameter("agencia");

Agencia agencia = model.agenciaService.obtenerAgencia(agenciaUsuario);

//Bancos egreso
String age = (usuario.getId_agencia().equals("OP")?"":usuario.getId_agencia());
model.servicioBanco.loadBancos(age, usuario.getCia());
TreeMap b = model.servicioBanco.getBanco();
session.setAttribute( "bancosDetalles", b );

//Se obtienen las fechas para la consulta
String fechaInicial = Util.getFechaActual_String(4);
String fechaFinal = Util.getFechaActual_String(4);
if(request.getParameter("fechaInicial")!=null){
    fechaInicial = request.getParameter("fechaInicial");
}
if(request.getParameter("fechaFinal")!=null){
    fechaFinal = request.getParameter("fechaFinal");
}

//Se consultan los saldos
CajasEfectivoService service = new CajasEfectivoService();
double saldoInicial = service.obtenerSaldoInicial(Util.ConvertiraDate1(fechaInicial), agenciaUsuario, usuario.getDstrct(), usuario.getLogin());
double saldoCanje = service.consultarSaldoTotalCanje(usuario.getDstrct(), agenciaUsuario);
ArrayList<CajaPago> pagos = service.consultarPagosAgencia(usuario.getDstrct(), agenciaUsuario, fechaInicial, fechaFinal);
ArrayList<CajaPago> anticipos = service.consultarPagosAnticipos(usuario.getDstrct(), agenciaUsuario, fechaInicial, fechaFinal);

%>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1"/>
        <title>Cajas efectivo</title>
        <link href="<%= BASEURL%>/css/estilostsp.css" rel='stylesheet'/>
        <link href="<%= BASEURL%>/css/letras.css" rel='stylesheet'/>
        <link href="<%=BASEURL%>/css/default.css"       rel="stylesheet" type="text/css"/>
        <script type='text/javascript' src="<%= BASEURL%>/js/boton.js"></script>
        <script type='text/javascript' src="<%= BASEURL%>/js/prototype.js"></script>
        <script type='text/javascript' src="<%=BASEURL%>/js/validar.js"></script>
        <script type='text/javascript' src="<%=BASEURL%>/js/cajasEfectivo.js"></script>
        <!-- Para las ventanas modales-->
        <script src="<%=BASEURL%>/js/effects.js" type="text/javascript"></script>
        <script src="<%=BASEURL%>/js/window.js" type="text/javascript"></script>
        <script src="<%=BASEURL%>/js/window_effects.js" type="text/javascript"></script>
    </head>
    <body>
        <div id="capaSuperior" style="position:absolute; width:100%; height:100px; z-index:0; left: 0px; top: 0px;">
            <jsp:include page="/WEB-INF/toptsp.jsp?encabezado=Manejo de creditos bancarios"/>
        </div>
        <div id="capaCentral" style="position:absolute; width:100%; height:85%; z-index:0; left: 0px; top: 100px; overflow: auto;">
            <%if(!agenciaUsuario.equals(usuario.getId_agencia())){%>
                <form id="form1" name="form1" method="post" action="">
                    <input type="hidden" name="CONTROLLER" id="CONTROLLER" value="<%=CONTROLLER%>"/>
                    <input type="hidden" name="agencia" id="agencia" value="<%=agenciaUsuario%>"/>
                    <table style="border: green solid 1px;" width="600px" align="center">
                        <tr class="barratitulo">
                            <td class="subtitulo1">Ingreso a caja</td>
                            <td><img src="<%=BASEURL%>/images/titulo.gif" width="32" height="20" align="left"/></td>
                        </tr>
                        <tr class="fila">
                            <td>Valor</td>
                            <td>
                                <input type="text" name="valor" id="valor" onkeyup="soloNumerosformateado(this.id);" />
                            </td>
                        </tr>
                        <tr class="fila">
                            <td>Fecha</td>
                            <td>
                                <input type="text" name="fecha" id="fecha" readonly />
                                <a href="javascript:void(0);" onFocus="if(self.gfPop)gfPop.fPopCalendar(document.form1.fecha);return false;" hidefocus> <img src="<%=BASEURL%>/js/Calendario/cal.gif" width="16" height="16" border="0" alt="De click aqu&iacute; para ver el calendario."></a>
                            </td>
                        </tr>
                        <tr class="fila">
                            <td>Descripci&oacute;n</td>
                            <td>
                                <textarea name="descripcion" id="descripcion" rows="2" cols="60"></textarea>
                            </td>
                        </tr>
                        <tr class="fila">
                            <td>Banco detalle</td>
                            <td>
                                <input:select name="banco" attributesText="id='banco' style='width:150px;' onChange='cargarSucursalesIngreso()' default=''" options="<%=b%>" >
                                    <option value="" selected="selected">Seleccione</option>
                                </input:select>
                            </td>
                        </tr>
                        <tr class="fila">
                            <td>Sucursal detalle</td>
                            <td>
                                <select class="element select medium" id="sucursal" name="sucursal">
                                </select>
                            </td>
                        </tr>
                    </table>
                    <br/>
                    <div align="center">
                        <img style="cursor:pointer" src="<%= BASEURL%>/images/botones/aceptar.gif" onmouseover="botonOver(this);" onmouseout="botonOut(this);" onClick="validarIngreso()"/>
                        <img style="cursor:pointer" src="<%= BASEURL%>/images/botones/salir.gif" onmouseover="botonOver(this);" onmouseout="botonOut(this);" onClick="parent.close();"/>
                    </div>
                </form>
                <br/>
                <br/>
            <%}%>
                <form id="form2" name="form2" method="post" action="">
                    <input type="hidden" name="agencia" id="agencia" value="<%=agenciaUsuario%>"/>
                    <table style="border: green solid 1px;" width="400px" align="center">
                        <tr class="barratitulo">
                            <td class="subtitulo1">Movimientos de caja</td>
                            <td><img src="<%=BASEURL%>/images/titulo.gif" width="32" height="20" align="left"/></td>
                        </tr>
                        <tr class="fila">
                            <td>Agencia</td>
                            <td>
                                <%=agencia.getNombre()%>
                            </td>
                        </tr>
                        <tr class="fila">
                            <td>Fecha inicial</td>
                            <td>
                                <input type="text" name="fechaInicial" id="fechaInicial" value="<%=fechaInicial%>" readonly />
                                <a href="javascript:void(0);" onFocus="if(self.gfPop)gfPop.fPopCalendar(document.form2.fechaInicial);return false;" hidefocus> <img src="<%=BASEURL%>/js/Calendario/cal.gif" width="16" height="16" border="0" alt="De click aqu&iacute; para ver el calendario."></a>
                            </td>
                        </tr>
                        <tr class="fila">
                            <td>Fecha final</td>
                            <td>
                                <input type="text" name="fechaFinal" id="fechaFinal" value="<%=fechaFinal%>" readonly />
                                <a href="javascript:void(0);" onFocus="if(self.gfPop)gfPop.fPopCalendar(document.form2.fechaFinal);return false;" hidefocus> <img src="<%=BASEURL%>/js/Calendario/cal.gif" width="16" height="16" border="0" alt="De click aqu&iacute; para ver el calendario."></a>
                            </td>
                        </tr>
                        <tr class="fila">
                            <td></td>
                            <td>
                                <img style="cursor:pointer" src="<%= BASEURL%>/images/botones/buscar.gif" onmouseover="botonOver(this);" onmouseout="botonOut(this);" onClick="validarRangoFechas()"/>
                            </td>
                        </tr>
                    </table>
                    <br/>
                    <table style="border: green solid 1px;" width="1100px" align="center">
                        <tr class="barratitulo">
                            <td colspan="6" class="subtitulo1">Saldos</td>
                            <td colspan="6"><img src="<%=BASEURL%>/images/titulo.gif" width="32" height="20" align="left"/></td>
                        </tr>
                        <tr class="fila">
                            <td>Saldo canje</td>
                            <td>
                                <%=UtilFinanzas.customFormat("#,###.##",saldoCanje,4)%>
                                <%if(saldoCanje>0){%>
                                    <a class="Simulacion_Hiper" style="cursor: pointer" onclick="abrirDialogConfirmar('<%=BASEURL%>','<%=agenciaUsuario%>');" >Confirmar ingreso</a>
                                <%}%>
                            </td>
                            <td colspan="9"></td>
                        </tr>
                        <tr class="fila">
                            <td>Saldo inicial</td>
                            <td>
                                <%=UtilFinanzas.customFormat("#,###.##",saldoInicial,4)%>
                            </td>
                            <td colspan="9"></td>
                        </tr>
                    
                    <%if(pagos.size()!=0 || anticipos.size()!=0){
                        double totalEgresos = 0;
                    %>
                            <tr class="Titulos" align="center">
                                <td>Empresa</td>
                                <td>Modalidad</td>
                                <td>No. documento</td>
                                <td>Beneficiario</td>
                                <td>Cedula</td>
                                <td>Banco</td>
                                <td>Valor</td>
                                <td>Comisi&oacute;n</td>
                                <td>Valor comisi&oacute;n</td>
                                <td>comisi&oacute;n bancaria</td>
                                <td>Valor a entregar</td>
                            </tr>
                            <%for(int i=0; i<pagos.size(); i++){
                                totalEgresos += (pagos.get(i).getValor() - pagos.get(i).getValorComision());
                            %>
                                <tr class="fila">
                                    <td><%=pagos.get(i).getTransportadora()%></td>
                                    <td><%=pagos.get(i).getTipoDocumento().equals("V")?"Vale":pagos.get(i).getTipoDocumento().equals("C")?"Cheque":"Transferencia"%></td>
                                    <td><%=pagos.get(i).getNumDocumento()%></td>
                                    <td><%=pagos.get(i).getBeneficiario()%></td>
                                    <td><%=pagos.get(i).getCedulaBeneficiario()%></td>
                                    <td><%=pagos.get(i).getBanco()%></td>
                                    <td><%=pagos.get(i).getValor()%></td>
                                    <td><%=pagos.get(i).getComision()%></td>
                                    <td><%=pagos.get(i).getValorComision()%></td>
                                     <td><%=pagos.get(i).getComisionBancaria()%></td>
                                     <td><%=pagos.get(i).getValor() - pagos.get(i).getValorComision()- pagos.get(i).getComisionBancaria()%></td>
                                </tr>
                            <%}%>
                            <%for(int i=0; i<anticipos.size(); i++){
                                totalEgresos += anticipos.get(i).getValorEntregar();
                            %>
                                <tr class="fila">
                                    <td>TSP</td>
                                    <td>Planilla</td>
                                    <td><%=anticipos.get(i).getNumDocumento()%></td>
                                    <td><%=anticipos.get(i).getNombreBeneficiario()%></td>
                                    <td><%=anticipos.get(i).getCedulaBeneficiario()%></td>
                                    <td></td>
                                    <td><%=anticipos.get(i).getValor()%></td>
                                    <td><%=anticipos.get(i).getComision()%></td>
                                    <td><%=anticipos.get(i).getValorComision()%></td>
                                     <td></td>
                                    <td><%=anticipos.get(i).getValorEntregar()%></td>
                                </tr>
                            <%}%>
                            <tr class="fila">
                                <td colspan="9"></td>
                                <td class="Titulos">TOTAL EGRESOS</td>
                                <td><%=UtilFinanzas.customFormat("#,###.##",totalEgresos,0)%></td>
                            </tr>
                            <tr class="fila">
                                <td colspan="9"></td>
                                <td class="Titulos">SALDO FINAL</td>
                                <td><%=UtilFinanzas.customFormat("#,###.##",saldoInicial-totalEgresos,0)%></td>
                            </tr>
                    <%}else{%>
                            <tr>
                                <td>
                                    <table width="100%"  border="0" align="center"  bordercolor="#f7f5f4" bgcolor="#ffffff">
                                        <tr>
                                            <td width="229" align="center" class="mensajes">No hay pagos en el rango de fechas seleccionado</td>
                                            <td width="29" background="<%=BASEURL%>/images/cuadronaranja.JPG">&nbsp;&nbsp;</td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                    <%}%>
                    </table>
                    <br/>
                    <div align="center">
                        <img style="cursor:pointer" src="<%= BASEURL%>/images/botones/agregar.gif" onmouseover="botonOver(this);" onmouseout="botonOut(this);" onClick="abrirDialogPago('<%=BASEURL%>','<%=agenciaUsuario%>')"/>
                        <img style="cursor:pointer" src="<%= BASEURL%>/images/botones/salir.gif" onmouseover="botonOver(this);" onmouseout="botonOut(this);" onClick="parent.close();"/>
                    </div>
                </form>
                <br/>
                <%if(request.getParameter("mensaje")!=null && !request.getParameter("mensaje").equals("")){%>
                    <div id="divMensaje">
                        <table border="2" align="center">
                            <tr>
                                <td>
                                    <table width="100%"  border="0" align="center"  bordercolor="#f7f5f4" bgcolor="#ffffff">
                                        <tr>
                                            <td width="229" align="center" class="mensajes"><%=request.getParameter("mensaje")%></td>
                                            <td width="29" background="<%=BASEURL%>/images/cuadronaranja.JPG">&nbsp;&nbsp;</td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                        </table>
                    </div>
                <%}%>
        </div>
        <iframe width=188 height=166 name="gToday:normal:agenda.js:gfPop:plugins.js" id="gToday:normal:agenda.js:gfPop:plugins.js" src="<%=BASEURL%>/js/Calendario/ipopeng.htm" scrolling="no" frameborder="0" style="visibility:visible; z-index:999; position:absolute; left:-500px; top:0px;"> </iframe>
    </body>
</html>
