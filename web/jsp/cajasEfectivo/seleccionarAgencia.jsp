<%-- 
    Document   : seleccionarAgencia
    Created on : 4/02/2012, 10:49:04 AM
    Author     : darrieta
--%>

<%@page import="com.tsp.operation.model.beans.TablaGen"%>
<%@page import="java.util.LinkedList"%>
<%@page contentType="text/html" pageEncoding="ISO-8859-1"%>
<%@include file="/WEB-INF/InitModel.jsp"%>
<%
model.tablaGenService.buscarRegistrosNoAnulados("CE_AGENCIA");
LinkedList<TablaGen> agencias = model.tablaGenService.getTablas();
%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
        <title>Seleccionar agencia</title>
    </head>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1"/>
        <title>Cajas efectivo</title>
        <link href="<%= BASEURL%>/css/estilostsp.css" rel='stylesheet'/>
        <script type='text/javascript' src="<%= BASEURL%>/js/boton.js"></script>
        <script type='text/javascript' src="<%=BASEURL%>/js/cajasEfectivo.js"></script>
    </head>
    <body>
        <div id="capaSuperior" style="position:absolute; width:100%; height:100px; z-index:0; left: 0px; top: 0px;">
            <jsp:include page="/WEB-INF/toptsp.jsp?encabezado=Cajas efectivo"/>
        </div>
        <div id="capaCentral" style="position:absolute; width:100%; height:85%; z-index:0; left: 0px; top: 100px; overflow: auto;">
            <form id="form1" name="form1" method="post" action="<%=CONTROLLER%>?estado=Cajas&accion=Efectivo">
                <table align="center" style="border: green solid 1px;" width="400" >
                    <tr class="barratitulo">
                        <td class="subtitulo1">Agencia</td>
                        <td><img src="<%=BASEURL%>/images/titulo.gif" width="32" height="20" align="left"/></td>
                    </tr>
                    <tr class="fila">
                        <td>Agencia</td>
                        <td>
                            <select id="agencia" name="agencia">
                                <%for(int i=0; i<agencias.size(); i++){%>
                                    <option value="<%=agencias.get(i).getTable_code()%>"><%=agencias.get(i).getDescripcion()%></option>
                                <%}%>
                            </select>
                        </td>
                    </tr>
                </table>
                <br/>
                <div align="center">
                    <img style="cursor:pointer" src="<%= BASEURL%>/images/botones/aceptar.gif" onmouseover="botonOver(this);" onmouseout="botonOut(this);" onClick="form1.submit()"/>
                    <img style="cursor:pointer" src="<%= BASEURL%>/images/botones/salir.gif" onmouseover="botonOver(this);" onmouseout="botonOut(this);" onClick="parent.close();"/>
                </div>
            </form>
        </div>
    </body>
</html>
