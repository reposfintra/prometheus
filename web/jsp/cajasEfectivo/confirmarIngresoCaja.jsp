<%-- 
    Document   : confirmarIngresoCaja
    Created on : 7/02/2012, 05:39:45 PM
    Author     : darrieta
--%>

<%@page import="java.util.Map"%>
<%@page import="com.tsp.util.UtilFinanzas"%>
<%@page import="com.tsp.operation.model.services.CajasEfectivoService"%>
<%@page import="java.util.TreeMap"%>
<%@page import="com.tsp.operation.model.beans.Usuario"%>
<%@page contentType="text/html" pageEncoding="ISO-8859-1"%>
<%@include file="/WEB-INF/InitModel.jsp"%>
<%@ taglib uri="http://jakarta.apache.org/taglibs/input-0.90" prefix="input" %>
<%
Usuario usuario = (Usuario)session.getAttribute("Usuario");
String agencia = request.getParameter("agencia");

CajasEfectivoService service = new CajasEfectivoService();
TreeMap<String, Double> saldos = service.consultarSaldosCanje(usuario.getDstrct(), agencia);

%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
        <title>Confirmar ingreso</title>
        <link href="<%= BASEURL%>/css/estilostsp.css" rel='stylesheet'/>
        <link href="<%=BASEURL%>/css/default.css"       rel="stylesheet" type="text/css"/>
        <script type='text/javascript' src="<%= BASEURL%>/js/boton.js"></script>
        <script type='text/javascript' src="<%=BASEURL%>/js/cajasEfectivo.js"></script>
        <script type='text/javascript'>
            window.name = "confirmarIngreso";
        </script>
    </head>
    <body onload="mostrarMensajeDialog('<%=request.getParameter("mensaje")%>');">
        <br/>
        <form id="form" name="form" method="post" enctype="multipart/form-data" action="<%=CONTROLLER%>?estado=Cajas&accion=Efectivo&opcion=CONFIRMAR_INGRESO" target="confirmarIngreso">
            <input type="hidden" id="agencia" name="agencia" value="<%=agencia%>"/>
            <table align="center" style="border: green solid 1px;" width="400px">
                <tr class="barratitulo">
                    <td class="subtitulo1">Confirmar ingreso a caja</td>
                    <td><img src="<%=BASEURL%>/images/titulo.gif" width="32" height="20" align="left"/></td>
                </tr>
                <tr class="fila">
                    <td>Ingreso</td>
                    <td>
                        <select id="ingreso" name="ingreso">
                        <%for (Map.Entry<String, Double> entry : saldos.entrySet()){%>
                                <option value="<%=entry.getKey()%>"><%=entry.getKey()+" - $"+UtilFinanzas.customFormat("#,###.##",entry.getValue().doubleValue(),4)%></option>
                        <%}%>
                        </select>
                    </td>
                </tr>
                <tr class="fila">
                    <td>Planilla</td>
                    <td>                            
                        <input type='file' id="planilla" name='planilla' style='width:100%' class="textbox" />
                    </td>
                </tr>                    
            </table>
            <br/>
            <div align="center">
                <img style="cursor:pointer" src="<%= BASEURL%>/images/botones/aceptar.gif" onmouseover="botonOver(this);" onmouseout="botonOut(this);" onClick="confirmarIngreso()"/>
                <img style="cursor:pointer" src="<%= BASEURL%>/images/botones/salir.gif" onmouseover="botonOver(this);" onmouseout="botonOut(this);" onClick="parent.close();"/>
            </div>
        </form>
            <%if(request.getParameter("mensaje")!=null && !request.getParameter("mensaje").equals("")){%>
            <div id="divMensaje">
                <table border="2" align="center">
                    <tr>
                        <td>
                            <table width="100%"  border="0" align="center"  bordercolor="#f7f5f4" bgcolor="#ffffff">
                                <tr>
                                    <td width="229" align="center" class="mensajes"><%=request.getParameter("mensaje")%></td>
                                    <td width="29" background="<%=BASEURL%>/images/cuadronaranja.JPG">&nbsp;&nbsp;</td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
            </div>
            <%}%>
    </body>
</html>
