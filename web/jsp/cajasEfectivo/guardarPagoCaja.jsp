<%-- 
    Document   : guardarPagoCaja
    Created on : 9/02/2012, 04:30:18 PM
    Author     : darrieta
--%>
<%@page session="true" %>
<%@page import="com.tsp.util.Util"%>
<%@page import="com.tsp.operation.model.beans.Bancos"%>
<%@page import="java.util.ArrayList"%>
<%@page import="com.tsp.operation.model.services.BancosService"%>
<%@page import="java.util.Map"%>
<%@page import="com.tsp.operation.model.services.CajasEfectivoService"%>
<%@page import="java.util.TreeMap"%>
<%@page import="com.tsp.operation.model.beans.Usuario"%>
<%@page contentType="text/html" pageEncoding="ISO-8859-1"%>
<%@include file="/WEB-INF/InitModel.jsp"%>
<%@taglib uri="http://jakarta.apache.org/taglibs/input-0.90" prefix="input" %>
<%
Usuario usuario = (Usuario)session.getAttribute("Usuario");
String agencia = request.getParameter("agencia");

BancosService bservice = new BancosService();
ArrayList<Bancos> bancos = bservice.obtenerBancos();

//bancos detalle egresos....
TreeMap b =(TreeMap) session.getAttribute("bancosDetalles");
%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1"/>
        <title>Guardar pago</title>
        <link href="<%= BASEURL%>/css/estilostsp.css" rel='stylesheet'/>
        <link href="<%=BASEURL%>/css/default.css"       rel="stylesheet" type="text/css"/>
        <script type='text/javascript' src="<%= BASEURL%>/js/boton.js"></script>
        <script type='text/javascript' src="<%= BASEURL%>/js/prototype.js"></script>
        <script type='text/javascript' src="<%=BASEURL%>/js/cajasEfectivo.js"></script>
        <script type='text/javascript' src="<%=BASEURL%>/js/validar.js"></script>
        <script type='text/javascript'>
            window.name = "guardarPago";
            var ventanaCliente;
            function abrirBusquedaCliente(tipo){
                ventanaCliente = window.open('<%=CONTROLLER%>?estado=Menu&accion=Cargar&pagina=consultasCliente.jsp&marco=no&carpeta=/consultas','','status=yes,scrollbars=no,width=650,height=500,resizable=yes');
                $("tipoCliente").value = tipo;
            }
            
            function cargarDatosCliente(){
                if($F("tipoCliente")=="T"){
                    $("transportadora").value = $F("codcli");
                    $("nomTransportadora").value = $F("nomcli");
                }
            }

        </script>
    </head>
    <body onload="mostrarMensajeDialogGuardar('<%=request.getParameter("mensaje")%>');" onfocus="cargarDatosCliente()">
        <br/>
        <form id="form1" name="form1" method="post" action="<%=CONTROLLER%>?estado=Cajas&accion=Efectivo&opcion=GUARDAR_PAGO" target="guardarPago">
            <input type="hidden" name="CONTROLLER" id="CONTROLLER" value="<%=CONTROLLER%>"/>
            <input type="hidden" id="agencia" name="agencia" value="<%=agencia%>"/>
            <input type="hidden" id="codcli" name="codcli"/>
            <input type="hidden" id="nomcli" name="nomcli"/>
            <input type="hidden" id="tipoCliente"/>
            <table align="center" style="border: green solid 1px;" width="500px">
                <tr class="barratitulo">
                    <td class="subtitulo1">Guardar pago</td>
                    <td><img src="<%=BASEURL%>/images/titulo.gif" width="32" height="20" align="left"/></td>
                </tr>
                <tr class="fila">
                    <td>Empresa</td>
                    <td>
                        <input name='transportadora' type='text' id="transportadora" size="10" maxlength="10" readonly/>
			<img src="<%=BASEURL%>/images/botones/iconos/obligatorio.gif"/>
                        <a onClick="abrirBusquedaCliente('T')" style="cursor:pointer">
                            <img height="20" width="25" src="<%=BASEURL%>/images/botones/iconos/user.jpg"/>
                        </a>
                        <br/>
                        <input name='nomTransportadora' type='text' id="nomTransportadora" class="fila" style="border: 0pt none;" size="45" />
                    </td>
                </tr>
                <tr class="fila">
                    <td>Fecha</td>
                    <td>
                        <input type="text" name="fecha" id="fecha" value="<%=Util.getFechaActual_String(4)%>" readonly />
                        <a href="javascript:void(0);" onFocus="if(self.gfPop)gfPop.fPopCalendar(document.form1.fecha);return false;" hidefocus> <img src="<%=BASEURL%>/js/Calendario/cal.gif" width="16" height="16" border="0" alt="De click aqu&iacute; para ver el calendario."></a>
                    </td>
                </tr>
                <tr class="fila">
                    <td>Modalidad</td>
                    <td>
                        <select id="tipoDocumento" name="tipoDocumento" onchange="cambiaComisionBancaria()">
                           <option value=""></option>
                            <option value="C">Cheque</option>
                            <option value="V">Vale</option>
                            <option value="T">Transferencia</option>
                        </select>
                    </td>
                </tr>
                 <tr class="fila">
                    <td>Tipo</td>
                    <td>
                        <select id="tipo" name="tipo" >
                           <option value=""></option>
                            <option value="A">Anticipos</option>
                            <option value="P">Pronto Pago</option>
                            
                        </select>
                    </td>
                </tr>
                <tr class="fila">
                    <td>Documento</td>
                    <td>
                        <input name='documento' type='text' id="documento" />
                    </td>
                </tr>
                <tr class="fila">
                    <td>Beneficiario</td>
                    <td>
                        <input name='provee' type='text' id="provee" size="10" maxlength="10" readonly/>
			<img src="<%=BASEURL%>/images/botones/iconos/obligatorio.gif"/>
                        <a onClick="window.open('<%=BASEURL%>/consultas/consultaProveedor.jsp','','status=yes,scrollbars=no,width=650,height=500,resizable=yes');" style="cursor:pointer">
                            <img height="20" width="25" src="<%=BASEURL%>/images/botones/iconos/user.jpg"/>
                        </a>
                        <br/>
                        <input name='nombre' type='text' id="nombre" class="fila" style="border: 0pt none;" size="45" />
                    </td>
                </tr>
                <tr class="fila">
                    <td>Banco</td>
                    <td>
                        <select id="banco" name="banco">
                            <option value="" selected="selected">Seleccione</option>
                            <%for(int i=0; i<bancos.size(); i++){%>
                                <option value="<%=bancos.get(i).getCodigo()%>" id="<%=bancos.get(i).getCodigo()%>_<%=bancos.get(i).getBaseAno()%>" ><%=bancos.get(i).getNombre()%></option>
                            <%}%>
                        </select>
                    </td>
                </tr>
                 <tr class="fila">
                    <td>Banco Detalle</td>
                    <td>
                        
                        <input:select name="bancoDetalle" attributesText="id='bancoDetalle' style='width:150px;' onChange='cargarSucursalesEngreso()' default=''" options="<%=b%>" >
                            <option value="" selected="selected">Seleccione</option>
                        </input:select>
                       
                    </td>
                </tr>
                 <tr class="fila">
                    <td>Sucursal Detalle</td>
                    <td>
                        <select class="element select medium" id="sucursalDetalle" name="sucursalDetalle">
                        </select>
                    </td>
                </tr>
                <tr class="fila">
                    <td>Valor</td>
                    <td>
                        <input id="valor" name="valor" onkeyup="soloNumerosformateado(this.id);" onblur="calcularComision()" />
                    </td>
                </tr>
                <tr class="fila">
                    <td>Comision (%)</td>
                    <td>
                        <input id="comision" name="comision" onkeyup="soloNumerosformateado(this.id);" onblur="calcularComision()" />
                    </td>
                </tr>
                <tr class="fila">
                    <td>Valor comision</td>
                    <td>
                        <input id="valorComision" name="valorComision" readonly />
                    </td>
                </tr>
                  <tr class="fila">
                    <td>comision Bancaria</td>
                    <td>
                        <input id="comisionBancaria" onkeyup="soloNumerosformateado(this.id);" value="0" name="comisionBancaria" onblur="calcularComision()"  />
                    </td>
                </tr>
                <tr class="fila">
                    <td>Valor a entregar</td>
                    <td>
                        <input id="valorEntregar" name="valorEntregar" readonly />
                    </td>
                </tr>
            </table>
            <br/>
            <div align="center">
                <img style="cursor:pointer" src="<%=BASEURL%>/images/botones/aceptar.gif" onmouseover="botonOver(this);" onmouseout="botonOut(this);" onClick="guardarPago()"/>
                <img style="cursor:pointer" src="<%=BASEURL%>/images/botones/salir.gif" onmouseover="botonOver(this);" onmouseout="botonOut(this);" onClick="parent.close();"/>
            </div>
        </form>
        <%if(request.getParameter("mensaje")!=null && !request.getParameter("mensaje").equals("")){%>
            <div id="divMensaje">
                <table border="2" align="center">
                    <tr>
                        <td>
                            <table width="100%"  border="0" align="center"  bordercolor="#f7f5f4" bgcolor="#ffffff">
                                <tr>
                                    <td width="229" align="center" class="mensajes"><%=request.getParameter("mensaje")%></td>
                                    <td width="29" background="<%=BASEURL%>/images/cuadronaranja.JPG">&nbsp;&nbsp;</td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
            </div>
        <%}%>
        <iframe width=188 height=166 name="gToday:normal:agenda.js:gfPop:plugins.js" id="gToday:normal:agenda.js:gfPop:plugins.js" src="<%=BASEURL%>/js/Calendario/ipopeng.htm" scrolling="no" frameborder="0" style="visibility:visible; z-index:999; position:absolute; left:-500px; top:0px;"> </iframe>
    </body>
</html>
