<%-- 
    Document   : administracionAsesores
    Created on : 29/11/2016, 03:52:36 PM
    Author     : mariana
--%>
<%@page contentType="text/html" pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
        <link href="./css/popup.css" rel="stylesheet" type="text/css">
        <link type="text/css" rel="stylesheet" href="./css/jquery/jquery-ui/jquery-ui.css" />
        <link type="text/css" rel="stylesheet" href="./css/TransportadorasApi.css " />
        <script type="text/javascript" src="./js/jquery/jquery.jqGrid/js/jquery.min.js"></script>
        <script type="text/javascript" src="./js/jquery/jquery-ui/jquery.ui.min.js"></script>
        <script type="text/javascript" src="./js/jquery-ui-1.8.5.custom.min.js"></script>   
        <script type="text/javascript" src="./js/jquery/jquery.jqGrid-4.7.0/js/grid.locale-es.js"></script>
        <script type="text/javascript" src="./js/jquery/jquery.jqGrid-4.7.0/js/jquery.jqGrid.min.js"></script>
        <script type="text/javascript" src="./js/jquery/jquery.jqGrid-4.7.0/js/jquery.tabletojson.js"></script>    
        <script type="text/javascript" src="./js/jquery/jquery.jqGrid-4.7.0/plugins/jquery.contextmenu.js"></script>
        <script type="text/javascript" src="./js/utilidadInformacion.js"></script> 
        <script type="text/javascript" src="./js/administracionAsesores.js"></script> 
        <title>ADMINISTRACION DE ASESORES</title>
    </head>
    <body>
        <div id="capaSuperior" style="position:absolute; width:100%; height:100px; z-index:0; left: 0px; top: 0px;">
            <jsp:include page="/WEB-INF/toptsp.jsp?encabezado=ADMINISTRACION DE ASESORES"/>
        </div>
        <style>
            .cbox{
                width: 10px;
            }


        </style>
    <center>
        <div id="tablita" class="ui-jqgrid ui-widget ui-widget-content center-block" style="width: 945px; margin-top: 80px;margin-left: 60px;padding: 8px;">
            <div class="ui-dialog-titlebar ui-widget-header ui-helper-clearfix">
                <label class="titulotablita"><b>FILTRO DE BUSQUEDA</b></label>
            </div>
            <table id="tablainterna" style="height: 53px; width: 945px"  >
                <tr>
                    <td>
                        <label>Asesor</label>
                    </td>
                    <td>
                        <select id="asesor" type="text"  style="width: 300px;" class="requerido"></select>
                    </td>
                    <td>
                        <label>Estado Cartera</label>
                    </td>
                    <td>
                        <select id="estado_cuenta">
                            <option value="consaldo">Con saldo</option>
                            <option value="sinsaldo">Sin saldo</option>
                            <option value="todos">Todos</option>
                        </select>
                    </td>
                    <td>
                        <label>Fecha</label>
                    </td>
                    <td>
                        <input id="fecha_inicio" type="text"  style="width: 75px;" readonly>
                    </td>
                    <td>
                        <input id="fecha_fin" type="text"  style="width: 75px;" readonly>
                    </td>
                    <td>
                        <div style="padding-top: 10px">
                            <center>
                                <button type="button" id="limpiar" class="ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only" style="margin-left: 15px;top: -7px;" > 
                                    <span class="ui-button-text">limpiar Fecha</span>
                                </button>  
                            </center>
                        </div>
                    </td>
                    <td>
                        <hr style="width: 2px;height: 37px;top: -25px;margin-left: 13px;color: rgba(128, 128, 128, 0.39);" /> 
                    </td>
                    <td>
                        <div style="padding-top: 10px">
                            <center>
                                <button type="button" id="buscar" class="ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only" style="margin-left: 15px;top: -7px;" > 
                                    <span class="ui-button-text">Buscar</span>
                                </button>  
                            </center>
                        </div>
                    </td>
                </tr>
            </table>
        </div>
        <div style="position: relative;top: 165px;margin-left: 75px;">
            <table id="tabla_negocios" ></table>
            <div id="pager"></div>
        </div>

        <div id="dialogMsj" style="display:none;">
            <p style="font-size: 12px;text-align:justify;" id="msj" > Texto </p>
        </div>
        <div id="dialogMsjAddAsesores" class="ventana" style="top: 24px ">
            <label>Asesores</label>
            <select id="asesores" style="width: 300px;"></select>
        </div>
        <div id="divSalidaEx" title="Exportacion" style=" display: block" >
            <p  id="msjEx" style=" display:  none"> Espere un momento por favor...</p>
            <center>
                <img id="imgloadEx" style="position: relative;  top: 7px; display: none " src="./images/cargandoCM.gif"/>
            </center>
            <div id="respEx" style=" display: none"></div>
        </div> 
    </center>
</body>
</html>
