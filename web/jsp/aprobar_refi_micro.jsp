<%-- 
    Document   : aprobar_refi_micro
    Created on : 29/07/2019, 02:11:25 PM
    Author     : mcamargo
--%>

<%@page contentType="text/html" pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
    <head>        
        <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
        <link type="text/css" rel="stylesheet" href="./css/jquery/jquery-ui/jquery-ui.css" />
        <link href="./css/popup.css" rel="stylesheet" type="text/css">
        <script type="text/javascript" src="./js/jquery/jquery.jqGrid/js/jquery.min.js"></script> 
        <script type="text/javascript" src="./js/jquery-ui-1.8.5.custom.min.js"></script>
        <script type="text/javascript" src="./js/jquery/jquery.jqGrid/js/grid.locale-es.js"></script>
        <script type="text/javascript" src="./js/jquery/jquery.jqGrid/js/jquery.jqGrid.min.js"></script>  
        <script src="./js/aprobar_refi_micro.js" type="text/javascript"></script>
        <title>Aprobar refinanciacion micro</title>
        <style type="text/css">
            
            .filtro{
                width: 550px;
                margin: 10px auto; 
                
            }

            .header_filtro{
                text-align: center;   
                border: 1px solid #234684;
                background: #2A88C8;
                color: #ffffff;
                font-weight: bold;
                border-radius: 8px 8px 0px 0px;
            }

            .body_filtro{                
                background: white; 
                border:1px solid #2A88C8;
                height: 80px;
                border-radius: 0px 0px 8px 8px;
            }

            .contenido{
                height: 83px; 
                width: auto;
                margin-top: 5px;
            }

            .label_cabecera{
                display: block;
                text-align: center;
                font-family: 'Lucida Grande','Lucida Sans',Arial,sans-serif;
                font-size: 15px;
            }
            .label_valor{
                height:17px;
                width:100px;
                color:#070708;
                font-weight:bold;
                font-size:12px;
                display: block;
                text-align: center;
                font-family: 'Lucida Grande','Lucida Sans',Arial,sans-serif;
                font-size: 12px; 
            }
            .caja{
                float:left;
                margin-left:10px;
                margin-right:10px;
                margin-top: 8px;
            }
            input[type=text],select{
                padding: 5px 5px;
                margin: 8px auto;
                border: 1px solid #ccc;
                border-radius: 4px;
                font-size: 15px;
                color: black;
            }
            .select {
                font-size: 12px;
                background: #2A88C8;
                color: white;
                font-weight: bold;
            }
            .button {
                padding: 8px 8px;
                font-size: 12px;
                cursor: pointer;
                text-align: center;
                color: #fff;
                background-color: #2a88c8;
                border: none;
                border-radius:8px;
                margin: 22px auto;
                font-weight: bold;
            }
            
            #tablainterna_tb td{
                width: 600px;
                text-align: left;
            }  
            
        </style>
    </head>
    <body>
        <div>
            <jsp:include page="/WEB-INF/toptsp.jsp?encabezado=Aprobar Refinanciacion Microcredito"/>
        </div>

        <div class="filtro">             
            <div class="header_filtro">
                <label>FILTRO DE BUSQUEDA</label>
            </div>
            <div class="body_filtro">
                <di class="contenido"> 
                    <div class="caja">
                        <label class="label_cabecera" >Buscar por:</label>
                                   <select id="tipo_busqueda" name="tipo_busqueda" class="select">
                                       <option value="negocio">Negocio</option>
                                       <option value="ide">Identificacion</option>
                                   </select>
                    </div>
                    <div class="caja">
                        <label class="label_cabecera" >Documento:</label>
                        <input type="text" id="documento" maxlength="15">
                    </div> 
                    <div class="caja">
                        <label class="label_cabecera" >Estado:</label>
                                   <select id="id_estado_refi" name="estado" class="select">
                                       <option value="V">Por Aprobar</option>
                                       <option value="P">Aprobado</option>
                                       <option value="S">Aplicado</option>
                                       <option value="A">Anulado</option>
                                   </select>
                    </div>
                    <div class="caja">
                        <button class="button" onclick="buscar_negocio_aprobar_refi();">Buscar</button>
                    </div>  
                </di>
            </div>
        </div>
        <div class="contenido" style="margin: 10px auto; ">   
            <center>
            <table id="tabla_aprobar_refi" ></table>
             <div id="page_tabla_aprobar_refi" ></div> 
             <div style="display:none;">
                 <input type="text" id="cod_negocio" >
                 <input type="text" id="key_ref" >
                 <input type="text" id="fecha_vencimiento_acuerdo" >
             </div>
             <br>
             <div id="div_detalle_refinanciacion" style="display:none;">
                 
            <label class="label_cabecera"  style="font-weight: bold; font-size: 16px; margin-bottom: 5px;">Negocio:
            <span id="id_cod_negocio" style=" font-size: 14px; margin-bottom: 5px;"></span>
            </label>
            <label class="label_cabecera"  style="font-weight: bold; font-size: 16px; margin-bottom: 5px;">Valor Cuota Actual:
            <span id="id_vlr_cuota_actual" style=" font-size: 14px; margin-bottom: 5px;"></span>
            </label>
                 
            <label class="label_cabecera" style="font-weight: bold; font-size: 16px;">Cliente:
            <span id="id_cliente" style=" font-size: 14px;"></span>
            </label>
                 
            <table id="tablainterna_tb" cellspacing="8px" style="margin-left: 2%"> 
                                <tr>
                                    <td> 
                                        <label>Interes Corriente</label><br>
                                        <label id="id_interes_tb" class="label_valor"></label>                                      
                                    </td> 
                                    <td id="id_cat_vencido"> 
                                        <label>Cat Vencido:</label><br>
                                        <label id="id_cat_vencido_tb" class="label_valor"></label>                                       
                                    </td> 
                                    <td> 
                                        <label >Otros Conceptos</label><br>
                                        <label id="id_cuota_admon_tb" class="label_valor"></label>                                        
                                    </td> 
                                    <td> 
                                        <label >Interes de Mora</label><br>
                                        <label id="id_ixm_tb" class="label_valor"></label>                                       
                                    </td> 

                                    <td> 
                                        <label>Gastos Cobranza</label><br>
                                        <label id="id_gac_tb" class="label_valor"></label>                                      
                                    </td>
                                    <td > 
                                        <label style="color:red;"># Pago mensual: <span id="peridos"></span> </label><br>
                                        <label id="total_pagar" class="label_valor" style="color: #2A88C8"></label>                                       
                                    </td>   
                                </tr>
                                <tr>                                     
                                    <td style="width: 0px;"> 
                                        <label>Capital Refinan.</label><br>
                                        <label id="id_capital_tb" class="label_valor"></label>                                        
                                    </td>
                                    <td> 
                                        <label><span>Ley Mypime</span></label><br>
                                        <label id="id_cat_tb" class="label_valor"></label>                                       
                                    </td> 
                                </tr>
                            </table> 
                 <br>
                 <b><label style="margin-left: 10px;">Comentarios : </label></b>
                 <textarea id="id_observaciones_simulacion" readonly style="margin-left: 10px;margin-bottom:5px; margin-top:5px; height: 40px; width: 804px;"></textarea>
                        
              <center> 
                 <table id="table_detalle_refinanciacion" ></table>
              </center>
             </div>
        <div id="div_obsevaciones" style="display:none;">  
            <textarea id="id_observaciones" style="margin-bottom:5px; height: 140px; width: 320px;">
            </textarea>
        </div>
             <br>
            </center>
            <div id="dialogLoading" style="display:none;">
                <p  style="font-size: 12px;text-align:justify;" id="msj2">Texto </p> <br/>
                <center>
                    <img src="./images/cargandoCM.gif"/>
                </center>
            </div>
            <div id="dialogMsj" title="Mensaje del sistema" style="display:none;">
                <p style="font-size: 12px;text-align:justify;" id="msj" > Texto </p>
            </div>  
        </div>
    </body>
</html>
