<%-- 
    Document   : polizas
    Created on : 10/06/2016, 10:04:08 AM
    Author     : dvalencia
--%>

<%@page contentType="text/html" pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
        <title>MAESTRO POLIZAS</title>
        <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
        <link href="./css/popup.css" rel="stylesheet" type="text/css">
        <link type="text/css" rel="stylesheet" href="./css/jquery/jquery-ui/jquery-ui.css" />
        <link type="text/css" rel="stylesheet" href="./css/TransportadorasApi.css" />
        <script type="text/javascript" src="./js/jquery/jquery.jqGrid/js/jquery.min.js"></script>
        <script type="text/javascript" src="./js/jquery-ui-1.8.5.custom.min.js"></script>   
        <script type="text/javascript" src="./js/jquery/jquery.jqGrid-4.7.0/js/grid.locale-es.js"></script>
        <script type="text/javascript" src="./js/jquery/jquery.jqGrid-4.7.0/js/jquery.jqGrid.min.js"></script>
        <script type="text/javascript" src="./js/jquery/jquery.jqGrid-4.7.0/js/jquery.tabletojson.js"></script>   
        <script type="text/javascript" src="./js/polizas.js"></script> 
    </head>
    <body>
       <div id="capaSuperior" style="position:absolute; width:100%; height:100px; z-index:0; left: 0px; top: 0px;">
            <jsp:include page="/WEB-INF/toptsp.jsp?encabezado=MAESTRO POLIZAS"/>
        </div>
        <style>
            .onoffswitch-inner:before {
                content: "A";
                padding-left: 14px;
                background-color: #34A7C1; color: #FFFFFF;
            }
            .onoffswitch-inner:after {
                content: "I";
                padding-right: 11px;
                background-color: #C20F0F; color: #F5F0F0;
                text-align: right;
            }
        </style>
        <center>
        <div style="position: relative;top: 150px">
            <table id="tabla_Polizas" ></table>
            <div id="pager"></div>
        </div>
        <div id="dialogPolizas"  class="ventana">
            <div id="tablainterna" style="width: 680px;" >
                <table id="tablainterna"  >
                    <tr>
                        <td>
                            <input type="text" id="id" style="width: 137px;color: black;" hidden>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <label>Nombre poliza</label>
                            <input type="text" id="nombre" style="width: 150px;color: black;">
                        </td> 
                        <td>
                            <label>Descripcion</label>
                            <input type="text" id="descripcion" style="width: 315px;color: black;"onchange="conMayusculas(this)">
                        </td>
                    </tr>
                </table>
            </div>
        </div>
        <div id="info"  class="ventana" >
            <p id="notific">EXITO AL GUARDAR</p>
        </div>
    </center>
    </body>
    <script>
  
        inicio1();
    

</script>
</html>
