<%-- 
    Document   : procesosInternos
    Created on : 10/09/2014, 05:55:54 PM
    Author     : desarrollo
--%>

<%@page import="com.tsp.operation.model.beans.Usuario"%>
<%@page import="com.tsp.operation.model.beans.CmbGeneralScBeans"%>
<%@page import="com.tsp.operation.model.DAOS.impl.ProcesosMetaImpl"%>
<%@page import="java.util.ArrayList"%>
<%@page import="com.tsp.operation.model.DAOS.ProcesosMetaDAO"%>
<%@page contentType="text/html" pageEncoding="ISO-8859-1"%>
<%@include file="/WEB-INF/InitModel.jsp"%>
<!DOCTYPE html>

<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
        <title>Procesos Internos</title>
        <link type="text/css" rel="stylesheet" href="./css/jquery/jquery-ui/jquery-ui.css"/>
        <link type="text/css" rel="stylesheet" href="./css/jquery/jquery.jqGrid/ui.jqgrid.css"/>        
        <script type="text/javascript" src="./js/jquery/jquery-ui/jquery.min.js"></script>
        <script type="text/javascript" src="./js/jquery/jquery-ui/jquery.ui.min.js"></script>            
        <script type="text/javascript" src="./js/jquery/jquery.jqGrid/js/grid.locale-es.js"></script>
        <script type="text/javascript" src="./js/jquery/jquery.jqGrid/js/jquery.jqGrid.min.js"></script>
        <link type="text/css" rel="stylesheet" href="<%=BASEURL%>/css/buttons/botonVerde.css"/>
        <link type="text/css" rel="stylesheet" href="<%=BASEURL%>/css/popup.css"/>
        <link href="<%=BASEURL%>/css/style_azul.css" rel="stylesheet" type="text/css">
        <script type="text/javascript" src="<%=BASEURL%>/js/procesosInternos.js"></script>    
        
    </head>
    <body>
        <div id="capaSuperior" style="position:absolute; width:100%; height:100px; z-index:0; left: 0px; top: 0px;">
             <jsp:include page="/toptsp.jsp?encabezado=Maestro de Procesos Internos"/>
        </div>
        
        <div id="capaCentral" style="position:absolute; width:100%; height:83%; z-index:0; left: 0px; top: 100px; overflow:auto;">
             </br></br>
             
<!--            <table border="0"  align="center" style=" width: 300px">
                 <tr>
                     <td width="50%"><span  class="form-submit-button form-submit-button-simple_green_apple" onClick="crearProcesoInterno();"> Nuevo </span></td>                  
                 </tr>
             </table>-->
             </br>
             <table border="0"  align="center">
                 <tr>
                     <td>
                         <table id="procesosInternos" ></table>
                         <div id="page_tabla_procesos_internos"></div>
                     </td>
                 </tr>
             </table>
         </div>
        
        <div id="div_procesoInterno" title="CREAR PROCESO INTERNO" style="display: none; width: 800px" >                
                 
                 <div id="filtros" class="ui-jqgrid ui-widget ui-widget-content ui-corner-all" style="height: 50px;padding: 0px 10px 5px 10px">
                     </br>
                     <table aling="center" style=" width: 100%" >
                         <tr>
                             <td style="width: 15%"><span>Proceso Meta:</span></td>
                             <td style="width: 30%"> <select name="idProMeta" class="combo_180px" style="font-size: 14px;" id="idProMeta">
                              </select></td>
                             <td style="width: 10%"><span>Descripcion:</span></td>
                             <td style="width: 30%"><input type="text" id="nomProceso" name="nomProceso" style=" width: 250px" </td>
                         </tr>
                     </table>
                 </div>   
                 </br>
<!--                 <div id="boton_guardar" style="position:absolute; left: 40%; top: 17%; width: 100%; display: inline"> 
                     <span aling="center"  class="form-submit-button form-submit-button-simple_green_apple" onClick="guardarProcesoInterno();"> Guardar </span>
                 </div>-->
<!--                 <div id="bt_asociar_und" style="position:absolute; left: 55%; top: 17%; width: 100%; display: none"> 
                     <span aling="center"  class="form-submit-button form-submit-button-simple_green_apple" onClick="asociarProcesoInterno();"> Asociar </span>
                 </div> 
                 <div id="div_unidadNegocio" style="display: none; position:absolute; left: 0%; top: 22%; width: 100% ">    
                     <table border="0"  align="center">
                         <tr>
                             <td>
                                 <table id="unidadNegocio" ></table>
                             </td>
                         </tr>
                     </table>   
                 </div>-->   
         </div>
                                                        
         <div id="div_editar_procesoInterno"  style="display: none; width: 800px" >
             <div class="content-popupNegocios"> 
                 <table align="center" style=" width: 100%" >
                     <tr>
                         <td  class="titulo_ventana" height="100%">
                             <div style="float: left; font-size: 15px">EDITAR PROCESO INTERNO</div> 
                             <div style="float:right"><a id="closeEditarProInterno" class="ui-widget-header ui-corner-all"><span>X</span></a></div>
                         </td>
                     </tr>
                 </table>
                 </br>
             <div id="filtros" style="display:table-cell; width:750px; height:50px; z-index:100; left: 8%; top: 7%;position:absolute" class="ui-jqgrid ui-widget ui-widget-content ui-corner-all">

                 </br>
                 <table align="center" style=" width: 100%" >
                     <tr>
                         <td style="width: 0%"><input type="hidden" id="idProinterno" name="idProinterno"</td>
                         <td style="width: 10%"><span>Nombre:</span></td>
                         <td style="width: 30%"><input type="text" id="nomProinterno" style=" width: 200px" name="nomProinterno"</td>
                         <td style="width: 15%"><span>Proceso Meta:</span></td>
                         <td style="width: 30%"> <select name="idProMetaEdit" class="combo_180px" style="font-size: 14px;" id="idProMetaEdit">
                         </select> </td>
                        <td style="width: 15%"><div id="boton_guardar" style="position:absolute; left: 82%; top: 36%; width: 100%"> 
                            <span aling="center"  class="form-submit-button form-submit-button-simple_green_apple" onClick="actualizarProInterno();"> Actualizar </span>
                        </div></td>
<!--                         <td style="width: 20%"><span>Meta Proceso:</span></td>
                         <td style="width: 30%"><input type="text" id="metaProceso" name="metaProceso" readOnly style=" width: 200px" </td>-->
                     </tr>
                 </table>
             </div> 
             </br>        
<!--         <div id="bt_listar_undPro" style="position:absolute; left: 50%; top: 16%; width: 100%; display:  none "> 
                     <span aling="center"  class="form-submit-button form-submit-button-simple_green_apple" onClick="listarUndNegocio();"> Listar Und Negocio </span>
             </div>-->
             <div id="bt_asociar_undPro" title="Asignar und de negocio al proceso interno" style="position:absolute; left: 47%; top: 50%; width: 100%; display:  none "> 
                     <span aling="center"  class="form-submit-button form-submit-button-simple_green_apple" onClick="asociarUndProinterno();"> >> </span>
             </div>
             <div id="bt_desasociar_undPro" title="Desasignar und de negocio del proceso interno" style="position:absolute; left: 47%; top: 58%; width: 100%; display:  none "> 
                     <span aling="center"  class="form-submit-button form-submit-button-simple_green_apple" onClick="desasociarUndProinterno();"> << </span>
             </div>
             <div id="div_und_prointerno" style="display: none; position: absolute; left: 55%; top: 18%; width: 40% ">    
                     <table border="0"  align="center">
                         <tr>
                             <td>
                                 <table id="UndNegocioPro" ></table>
                             </td>
                         </tr>
                     </table>   
             </div>
             <div id="div_undNegocios" style="position:absolute; left: 5%; top: 18%; width: 40% ">    
                 <table border="0"  align="center">
                     <tr>
                         <td>
                             <table id="UndadesNegocios" ></table>
                         </td>
                     </tr>
                 </table>   
             </div>
             </div>
         </div>
        
        <div id="div_asoc_user_prointerno"  style="display: none; width: 800px">
            <div class="content-popupNegocios">
                <table aling="center" style=" width: 100%" >
                    <tr>
                        <td  class="titulo_ventana" height="100%">
                            <div style="float: left; font-size: 15px">ASOCIAR USUARIOS A PROCESOS INTERNOS</div> 
                            <div style="float:right"><a id="closeAsocUserProInterno" class="ui-widget-header ui-corner-all"><span>X</span></a></div>
                        </td>
                    </tr>
                </table>
                </br>
                <div id="filtros" style="display:table-cell; width:820px; height:50px; z-index:100; left: 3%; top: 7%;position:absolute" class="ui-jqgrid ui-widget ui-widget-content ui-corner-all">

                    </br>
                    <table aling="center" style=" width: 100%" >
                        <tr>
                            <td style="width: 0%"><input type="hidden" id="idProinternoAsoc" name="idProinternoAsoc"</td>
                            <td style="width: 10%"><span>Nombre:</span></td>
                            <td style="width: 40%"><input type="text" id="nomProinternoAsoc" name="nomProinternoAsoc" readonly style=" width: 200px" </td>
                            <td style="width: 15%"><span>Proceso Meta:</span></td>
                            <td style="width: 35%"><input type="text" id="nommetaproceso" name="nommetaproceso" readonly style=" width: 200px" </td>
                            </select> </td>  
                        </tr>
                    </table>
                </div> 
                </br>
<!--            <div id="bt_listar_userPro" style="position:absolute; left: 50%; top: 16%; width: 100%; display:  none "> 
                    <span aling="center"  class="form-submit-button form-submit-button-simple_green_apple" onClick="listarUsuariosRelProInterno();"> Listar Usuarios </span>
                </div>-->
                <div id="bt_asociar_userPro" title="Asignar usuarios al proceso interno" style="position:absolute; left: 46%; top: 50%; width: 100%; display:  none "> 
                    <span aling="center"  class="form-submit-button form-submit-button-simple_green_apple" onClick="asociarUsuariosProinterno();"> >> </span>
                </div>  
                <div id="bt_desasociar_userPro" title="Desasignar usuarios del proceso interno" style="position:absolute; left: 46%; top: 58%; width: 100%; display:  none "> 
                    <span aling="center"  class="form-submit-button form-submit-button-simple_green_apple" onClick="desasociarUsuariosProinterno();"> << </span>
                </div>  
                <div id="div_user_prointerno" style="display: none; position: absolute; left: 55%; top: 19%; width: 40% ">    
                    <table border="0"  align="center">
                        <tr>
                            <td>
                                <table id="userProInterno" ></table>
                            </td>
                        </tr>
                    </table>   
                </div>
                <div id="div_usuarios" style="position:absolute; left: 3%; top: 18%; width: 40% ">    
                    <table border="0"  align="center">
                        <tr>
                            <td>
                                <table id="listUsuarios" ></table>
                            </td>
                        </tr>
                    </table> 
                </div>
            </div>
         </div> 
        
         <div id="dialogMsgPini" title="Mensaje" style="display:none; visibility: hidden;">
            <p style="font-size: 12.5px;text-align:justify;" id="msj" > Texto </p>
        </div>      
    </body>
</html>
