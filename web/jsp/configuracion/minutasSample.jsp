<%-- 
    Document   : minutasSample
    Created on : 4/04/2016, 02:35:53 PM
    Author     : user
--%>

<%@page contentType="text/html" pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
     <head>
        <meta charset="utf-8">
        <title>Configurador de Docuemntos</title>
        <!-- Make sure the path to CKEditor is correct. -->
         <script type="text/javascript" src="./js/jquery/jquery.jqGrid/js/jquery.min.js"></script> 
        <script type="text/javascript" src="./js/jquery-ui-1.8.5.custom.min.js"></script>
        <script src="./js/jquery/editor/ckeditor/ckeditor.js" type="text/javascript"></script>
        <script src="./js/minutas.js" type="text/javascript"></script>
    </head>
    <body>
        <div id="capaSuperior"  style="position:absolute; width:100%; height:100px; z-index:0; left: 0px; top: 0px;">
            <jsp:include page="/WEB-INF/toptsp.jsp?encabezado=CONFIGURAR DOCUMENTO"/>
        </div>
        <div id="capaCentral" style="position:absolute; width:100%; height:100%; z-index:0; left: 0px; top: 120px; ">
            <center>
                <div style="width: 780px">
                    <form>
                        <textarea cols="80" id="editor1" name="editor1" rows="20" >&lt;p&gt;This is some &lt;strong&gt;sample text&lt;/strong&gt;. You are using &lt;a href="http://ckeditor.com/"&gt;CKEditor&lt;/a&gt;.&lt;/p&gt;
                        </textarea>   
                    </form>
                </div>
            </center>
        </div>
    </body>
</html>
