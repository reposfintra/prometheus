<%-- 
    Document   : procesoMeta
    Created on : 9/09/2014, 09:47:53 AM
    Author     : desarrollo
--%>

<%@page import="com.tsp.operation.model.beans.Compania"%>
<%@page import="com.tsp.operation.model.CompaniaService"%>
<%@page import="com.tsp.operation.model.beans.Usuario"%>
<%@page import="com.tsp.operation.model.beans.CmbGeneralScBeans"%>
<%@page import="java.util.ArrayList"%>
<%@page import="com.tsp.operation.model.DAOS.impl.ProcesosMetaImpl"%>
<%@page import="com.tsp.operation.model.DAOS.ProcesosMetaDAO"%>
<%@page contentType="text/html" pageEncoding="ISO-8859-1"%>
<%@include file="/WEB-INF/InitModel.jsp"%>
<!DOCTYPE html>
<%
    Usuario usuario = (Usuario) session.getAttribute("Usuario");
    String nomEmpresa = "";
    CompaniaService ciaserv = new CompaniaService();
    ciaserv.buscarCia(usuario.getDstrct());
    Compania cia = ciaserv.getCompania();   
    nomEmpresa = cia.getdescription();   
 %>   
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
        <title>Procesos Meta</title>
        <link type="text/css" rel="stylesheet" href="./css/jquery/jquery-ui/jquery-ui.css"/>
        <link type="text/css" rel="stylesheet" href="./css/jquery/jquery.jqGrid/ui.jqgrid.css"/>        
        <script type="text/javascript" src="./js/jquery/jquery-ui/jquery.min.js"></script>
        <script type="text/javascript" src="./js/jquery/jquery-ui/jquery.ui.min.js"></script>            
        <script type="text/javascript" src="./js/jquery/jquery.jqGrid/js/grid.locale-es.js"></script>
        <script type="text/javascript" src="./js/jquery/jquery.jqGrid/js/jquery.jqGrid.min.js"></script>
        <link type="text/css" rel="stylesheet" href="./css/buttons/botonVerde.css"/>
        <link type="text/css" rel="stylesheet" href="./css/popup.css"/>
        <link href="./css/style_azul.css" rel="stylesheet" type="text/css">
        <script type="text/javascript" src="./js/procesosMeta.js"></script>   
    </head>
    <body>
        <div id="capaSuperior" style="position:absolute; width:100%; height:100px; z-index:0; left: 0px; top: 0px;">
             <jsp:include page="/toptsp.jsp?encabezado=Maestro de Procesos Meta"/>
        </div>
        <div id="capaCentral" style="position:absolute; width:100%; height:83%; z-index:0; left: 0px; top: 100px; overflow:auto;">       
             </br></br>
             </br>
             <table border="0"  align="center">
                 <tr>
                     <td>
                         <table id="procesosMeta" ></table>
                         <div id="page_tabla_procesos_meta"></div>
                     </td>
                 </tr>
             </table>
         </div>
         
         <div id="div_procesosMeta"  style="display: none; width: 800px" >                       
                 <div id="filtros" class="ui-jqgrid ui-widget ui-widget-content ui-corner-all" style="height: 110px;padding: 0px 10px 5px 10px">
                     </br>
                     <table aling="center" style=" width: 100%" >
                         <tr>
                             <input type="hidden" id="idProceso" name="idMeta">     
                             <input type="hidden" id="idempresa" name="idempresa" value="<%=usuario.getDstrct()%>" >       
                             <td style="width: 10%"><span>Empresa:</span></td>    
                             <td style="width: 30%"><input type="text" id="nomempresa" name="nomempresa" style=" width: 210px" value="<%=nomEmpresa%>" readonly ></td>
                             <td style="width: 10%"><span>Nombre</span></td>                          
                             <td style="width: 50%"><input type="text" id="nommeta" name="nommeta" style=" width: 248px" ></td>                            
                         </tr> 
                         <tr>
                                <td style="width: 10%"><span>Descripcion:</span></td>   
                                <td style="width: 90%" colspan="4"><textarea id ="descmeta" name="descmeta" style="width:535px;resize:none" rows="2" maxlength="300"></textarea></td>                        
                         </tr>
                     </table>
                 </div>  
                </br> 
         </div>
         <div id="div_editar_procesosMeta"  style="display: none; width: 800px" >
              <div id="filtros" class="ui-jqgrid ui-widget ui-widget-content ui-corner-all" style="height: 145px;padding: 0px 10px 5px 10px">
                     </br>
                     <table aling="center" style=" width: 100%" >
                         <tr>
                             <input type="hidden" id="idMetaEdit" name="idMetaEdit">    
                             <input type="hidden" id="idempresaEdit" name="idempresaEdit" value="<%=usuario.getDstrct()%>" >      
                             <td style="width: 10%"><span>Empresa:</span></td>    
                             <td style="width: 30%"><input type="text" id="nomempresaEdit" name="nomempresaEdit" style=" width: 210px" value="<%=nomEmpresa%>" readonly ></td>
                             <td style="width: 10%"><span>Nombre</span></td>                          
                             <td style="width: 50%"><input type="text" id="nommetaEdit" name="nommetaEdit" style=" width: 250px" ></td>
                         </tr>
                        <tr>
                                <td style="width: 10%"><span>Descripcion:</span></td>   
                                <td style="width: 90%" colspan="4"><textarea id ="descmetaEdit" name="descmetaEdit" style="width:525px;resize:none"  rows="2" maxlength="300"></textarea></td>                        
                         </tr>
                         <tr>
                             <td style="width: 100%"><div id="boton_guardar" style="position:absolute; left: 83%; top: 81%; "> 
                                     <span aling="center"  class="form-submit-button form-submit-button-simple_green_apple" onClick="actualizarMetaProceso();"> Actualizar </span>
                             </div></td>
                         </tr>
                     </table>
                 </div>  
                 </br>  
                <table border="0"  align="center">
                    <tr>
                        <td>
                            <table id="procesosInternos" ></table>
                            <div id="page_tabla_procesos_internos"></div>
                        </td>
                    </tr>
                </table>
        </div> 
                             
         <div id="div_procesoInterno" title="CREAR PROCESO INTERNO" style="display: none; width: 800px" >                
                 
                 <div id="filtros" class="ui-jqgrid ui-widget ui-widget-content ui-corner-all" style="height: 110px;padding: 0px 10px 5px 10px">
                     </br>
                     <table aling="center" style=" width: 100%" >
                         <tr>
                             <td style="width: 15%"><span>Proceso Meta:</span></td>
                             <td style="width: 30%"> <select name="idProMeta" class="combo_180px" style="font-size: 14px;width:230px" id="idProMeta">
                              </select></td>
                             <td style="width: 10%"><span>Nombre</span></td>
                             <td style="width: 30%"><input type="text" id="nomProceso" name="nomProceso" style=" width: 222px" </td>
                         </tr>
                         <tr>
                             <td style="width: 10%"><span>Descripcion:</span></td>   
                             <td style="width: 90%" colspan="4"><textarea id ="descProceso" name="descProceso" style="width:525px;resize:none"  rows="2" maxlength="300"></textarea></td>                        
                         </tr>
                     </table>
                 </div>   
                 </br> 
        </div>                    
        
        <div id="div_editar_procesoInterno"  style="display: none; width: 800px; z-index:1100" >
             <div id="filtros"  class="ui-jqgrid ui-widget ui-widget-content ui-corner-all" style="height: 145px;z-index:1100;padding: 0px 10px 5px 10px; ">

                 </br>
                 <table aling="center" style=" width: 100%" >
                     <tr>
                         <input type="hidden" id="idProinterno" name="idProinterno">
                         <td style="width: 15%"><span>Proceso Meta:</span></td>
                         <td style="width: 40%"> <select name="idProMetaEdit" class="combo_180px" style="font-size: 14px;width:280px" id="idProMetaEdit">
                         </select> </td> 
                         <td style="width: 10%"><span>Nombre:</span></td>
                         <td style="width: 32%"><input type="text" id="nomProinterno" style=" width: 245px" name="nomProinterno"/></td>                                             
<!--                         <td style="width: 20%"><span>Meta Proceso:</span></td>
                         <td style="width: 30%"><input type="text" id="metaProceso" name="metaProceso" readOnly style=" width: 200px" </td>-->
                     </tr>
                     <tr>
                         <td style="width: 10%"><span>Descripcion:</span></td>   
                         <td style="width: 90%" colspan="4"><textarea id ="descProinterno" name="descProinterno" style="width:572px;resize:none"  rows="2" maxlength="300"></textarea></td>                        
                     </tr>
                     <tr>
                         <td style="width: 100%"><div id="boton_guardar_proInterno" style="position:absolute; left: 86%; top: 81%; "> 
                                 <span aling="center"  class="form-submit-button form-submit-button-simple_green_apple" onClick="actualizarProInterno();"> Actualizar </span>
                             </div></td>
                     </tr>
                 </table>
             </div> 
             </br>        

             <div id="bt_asociar_undPro" title="Asignar und de negocio al proceso interno" style="position:absolute; left: 47%; top: 54%; width: 10%; display:  none "> 
                     <span aling="center"  class="form-submit-button form-submit-button-simple_green_apple" onClick="asociarUndProinterno();"> >> </span>
             </div>
             <div id="bt_desasociar_undPro" title="Desasignar und de negocio del proceso interno" style="position:absolute; left: 47%; top: 62%; width: 10%; display:  none "> 
                     <span aling="center"  class="form-submit-button form-submit-button-simple_green_apple" onClick="desasociarUndProinterno();"> << </span>
             </div>
             <div id="div_und_prointerno" style="display: none; position: absolute; left: 55%; top: 27%; width: 40% ">    
                     <table border="0"  align="center">
                         <tr>
                             <td>
                                 <table id="UndNegocioPro" ></table>
                             </td>
                         </tr>
                     </table>   
             </div>
             <div id="div_undNegocios" style="position:absolute; left: 5%; top: 27%; width: 40% ">    
                 <table border="0"  align="center">
                     <tr>
                         <td>
                             <table id="UndadesNegocios" ></table>
                         </td>
                     </tr>
                 </table>   
             </div>
         </div>
                             
        <div id="div_asoc_user_prointerno"  style="display: none; width: 800px">
                <div id="filtros" class="ui-jqgrid ui-widget ui-widget-content ui-corner-all" style="height:110px; z-index:1100; padding: 0px 10px 0px 10px;">

                    </br>
                    <table aling="center" style=" width: 100%" >
                        <tr>
                            <input type="hidden" id="idProinternoAsoc" name="idProinternoAsoc"/>
                            <td style="width: 10%"><span>Nombre:</span></td>
                            <td style="width: 32%"><input type="text" id="nomProinternoAsoc" name="nomProinternoAsoc" readonly style=" width: 245px" </td>
                            <td style="width: 12%"><span>Proceso Meta:</span></td>
                            <td style="width: 40%"><input type="text" id="nommetaproceso" name="nommetaproceso" readonly style=" width: 278px" </td>
                            </select> </td>  
                        </tr>
                        <tr>
                            <td style="width: 10%"><span>Descripcion:</span></td>   
                            <td style="width: 90%" colspan="4"><textarea id ="descProinternoAsoc" name="descProinternoAsoc" style="width:662px;resize:none"  rows="2" readonly></textarea></td>                        
                        </tr>
                    </table>
                </div> 
                </br>
                <div id="bt_asociar_userPro" title="Asignar usuarios al proceso interno" style="position:absolute; left: 47%; top: 54%; width: 10%; display:  none "> 
                    <span aling="center"  class="form-submit-button form-submit-button-simple_green_apple" onClick="asociarUsuariosProinterno();"> >> </span>
                </div>  
                <div id="bt_desasociar_userPro" title="Desasignar usuarios del proceso interno" style="position:absolute; left: 47%; top: 62%; width: 10%; display:  none "> 
                    <span aling="center"  class="form-submit-button form-submit-button-simple_green_apple" onClick="desasociarUsuariosProinterno();"> << </span>
                </div>  
                <div id="div_user_prointerno" style="display: none; position: absolute; left: 55%; top: 22%; width: 40% ">    
                    <table border="0"  align="center">
                        <tr>
                            <td>
                                <table id="userProInterno" ></table>
                            </td>
                        </tr>
                    </table>   
                </div>
                <div id="div_usuarios" style="position:absolute; left: 3%; top: 21%; width: 40% ">    
                    <table border="0"  align="center">
                        <tr>
                            <td>
                                <table id="listUsuarios" ></table>
                            </td>
                        </tr>
                    </table> 
                </div>
         </div> 
                             
        <div id="div_procesos_internos_anul"  style="display: none; width: 800px" >
            <div id="filtros" class="ui-jqgrid ui-widget ui-widget-content ui-corner-all" style="height: 110px;padding: 0px 10px 5px 10px">
                </br>
                <table aling="center" style=" width: 100%" >
                    <tr>          
                        <input type="hidden" id="idMetaAnul" name="idMetaAnul">    
                        <td style="width: 10%"><span>Empresa:</span></td>    
                        <td style="width: 30%"><input type="text" name="nomempresa" style=" width: 210px" value="<%=nomEmpresa%>" readonly ></td>
                        <td style="width: 10%"><span>Nombre</span></td>                          
                        <td style="width: 50%"><input type="text" id="nommetaprocesoAnul" name="nommetaproceso" style=" width: 275px" readonly></td>
                    </tr>
                    <tr>
                        <td style="width: 10%"><span>Descripcion:</span></td>   
                        <td style="width: 90%" colspan="4"><textarea id ="descmetaprocesoAnul" name="descmetaproceso" style="width:565px;resize:none"  rows="2" readonly></textarea></td>                        
                    </tr>                         
                </table>
            </div>  
            </br>  
            <table border="0"  align="center">
                <tr>
                    <td>
                        <table id="procesos_internos_anul" ></table>
                        <div id="page_tabla_procesos_internos_anul"></div>
                    </td>
                </tr>
            </table>
        </div>                             
                             
        <div id="dialogMsgMeta" title="Mensaje" style="display:none; visibility: hidden">
            <p style="font-size: 12.5px;text-align:justify;" id="msj" > Texto </p>
        </div>               
    </body>
</html>
