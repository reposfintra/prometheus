<%-- 
    Document   : verProcesosAnulados
    Created on : 14/05/2015, 08:55:16 AM
    Author     : mcastillo
--%>
<%@page import="com.tsp.operation.model.beans.Compania"%>
<%@page import="com.tsp.operation.model.CompaniaService"%>
<%@page import="com.tsp.operation.model.beans.Usuario"%>
<%@page import="com.tsp.operation.model.beans.CmbGeneralScBeans"%>
<%@page import="java.util.ArrayList"%>
<%@page import="com.tsp.operation.model.DAOS.impl.ProcesosMetaImpl"%>
<%@page import="com.tsp.operation.model.DAOS.ProcesosMetaDAO"%>
<%@page contentType="text/html" pageEncoding="ISO-8859-1"%>
<%@include file="/WEB-INF/InitModel.jsp"%>
<!DOCTYPE html>
 <%
    Usuario usuario = (Usuario) session.getAttribute("Usuario");
    String nomEmpresa = "";
    CompaniaService ciaserv = new CompaniaService();
    ciaserv.buscarCia(usuario.getDstrct());
    Compania cia = ciaserv.getCompania();   
    nomEmpresa = cia.getdescription();   
 %>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
        <title>Procesos Anulados</title>
        <link type="text/css" rel="stylesheet" href="./css/jquery/jquery-ui/jquery-ui.css"/>
        <link type="text/css" rel="stylesheet" href="./css/jquery/jquery.jqGrid/ui.jqgrid.css"/>        
        <script type="text/javascript" src="./js/jquery/jquery-ui/jquery.min.js"></script>
        <script type="text/javascript" src="./js/jquery/jquery-ui/jquery.ui.min.js"></script>            
        <script type="text/javascript" src="./js/jquery/jquery.jqGrid/js/grid.locale-es.js"></script>
        <script type="text/javascript" src="./js/jquery/jquery.jqGrid/js/jquery.jqGrid.min.js"></script>
        <link type="text/css" rel="stylesheet" href="./css/buttons/botonVerde.css"/>
        <link type="text/css" rel="stylesheet" href="./css/popup.css"/>
        <link href="./css/style_azul.css" rel="stylesheet" type="text/css">
        <script type="text/javascript" src="./js/procesosAnulados.js"></script>   
    </head>
    <body>
        <div id="capaSuperior" style="position:absolute; width:100%; height:100px; z-index:0; left: 0px; top: 0px;">
             <jsp:include page="/toptsp.jsp?encabezado=Procesos Meta Inactivos"/>
        </div>
        <div id="capaCentral" style="position:absolute; width:100%; height:83%; z-index:0; left: 0px; top: 100px; overflow:auto;">       
             </br></br>
             </br>
             <table border="0"  align="center">
                 <tr>
                     <td>
                         <table id="metaAnulados" ></table>
                         <div id="page_tabla_metaAnulados"></div>
                     </td>
                 </tr>
             </table>
         </div>
         <div id="div_procesos_internos_asoc"  style="display: none; width: 800px" >
              <div id="filtros" class="ui-jqgrid ui-widget ui-widget-content ui-corner-all" style="height: 110px;padding: 0px 10px 5px 10px">
                     </br>
                     <table aling="center" style=" width: 100%" >
                         <tr>                          
                             <td style="width: 10%"><span>Empresa:</span></td>    
                             <td style="width: 30%"><input type="text" id="nomempresa" name="nomempresa" style=" width: 210px" value="<%=nomEmpresa%>" readonly ></td>
                             <td style="width: 10%"><span>Nombre</span></td>                          
                             <td style="width: 50%"><input type="text" id="nommetaproceso" name="nommetaproceso" style=" width: 250px" readonly></td>
                         </tr>
                        <tr>
                                <td style="width: 10%"><span>Descripcion:</span></td>   
                                <td style="width: 90%" colspan="4"><textarea id ="descmetaproceso" name="descmetaproceso" cols="71"  rows="2" readonly></textarea></td>                        
                         </tr>                         
                     </table>
                 </div>  
                 </br>  
                <table border="0"  align="center">
                    <tr>
                        <td>
                            <table id="procesos_internos_asoc" ></table>
                            <div id="page_tabla_procesos_internos_asoc"></div>
                        </td>
                    </tr>
                </table>
        </div> 
        
        <div id="dialogMsgAnulados" title="Mensaje" style="display:none; visibility: hidden">
            <p style="font-size: 12.5px;text-align:justify;" id="msj" > Texto </p>
        </div>   
    </body>
</html>
