<%-- 
    Document   : unidadNegocio
    Created on : 4/09/2014, 10:33:24 AM
    Author     : desarrollo
--%>

<%@page contentType="text/html" pageEncoding="ISO-8859-1"%>
<%@include file="/WEB-INF/InitModel.jsp"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
        <title>Unidades de Negocio</title>
        
        <link type="text/css" rel="stylesheet" href="./css/jquery/jquery-ui/jquery-ui.css"/>
        <link type="text/css" rel="stylesheet" href="./css/jquery/jquery.jqGrid/ui.jqgrid.css"/>        
        <script type="text/javascript" src="./js/jquery/jquery-ui/jquery.min.js"></script>
        <script type="text/javascript" src="./js/jquery/jquery-ui/jquery.ui.min.js"></script>            
        <script type="text/javascript" src="./js/jquery/jquery.jqGrid/js/grid.locale-es.js"></script>
        <script type="text/javascript" src="./js/jquery/jquery.jqGrid/js/jquery.jqGrid.min.js"></script>
        <link type="text/css" rel="stylesheet" href="<%=BASEURL%>/css/buttons/botonVerde.css"/>
        <link type="text/css" rel="stylesheet" href="<%=BASEURL%>/css/popup.css"/>
        <link href="<%=BASEURL%>/css/style_azul.css" rel="stylesheet" type="text/css">
        <script type="text/javascript" src="<%=BASEURL%>/js/unidadesNegocio.js"></script>    
        
    </head>
    <script>
        $(document).ready(function(){
            $('#close').click(function(){
                        $('#popupNegocios').fadeOut('slow');
                        $('#div_editar').fadeOut('slow');
                        return false;
                    });
            
            $('#closeEditar').click(function(){
                        $('#div_editar').fadeOut('slow');
                            return false;
                    });
        });
     </script> 
     
     <body>
         <div id="capaSuperior" style="position:absolute; width:100%; height:100px; z-index:0; left: 0px; top: 0px;">
             <jsp:include page="/toptsp.jsp?encabezado=Maestro de Unidades de Negocio"/>
         </div>
         <div id="capaCentral" style="position:absolute; width:100%; height:83%; z-index:0; left: 0px; top: 100px; overflow:auto;">
             </br></br>
             <table border="0"  align="center">
                 <tr>
                     <td width="70px"><span  class="form-submit-button form-submit-button-simple_green_apple" onClick="crearUnidNegocios();"> Nuevo </span></td>
                 </tr>
             </table>
             </br>
             <table border="0"  align="center">
                 <tr>
                     <td>
                         <table id="unidadesNegocio" ></table>
                     </td>
                 </tr>
             </table>
         </div> 
         
         <div id="popupNegocios"  style="display: none; width: 800px" >
             <div class="content-popupNegocios">   
                 <table aling="center" style=" width: 100%" >
                     <tr>
                         <td  class="titulo_ventana" height="100%">
                             <div style="float: left; font-size: 15px">CREAR UNIDAD DE NEGOCIO</div> 
                             <div style="float:right"><a id="close" class="ui-widget-header ui-corner-all"><span>X</span></a></div>
                         </td>
                     </tr>
                 </table>  
                 </br>
                 <div id="filtros" style="display:table-cell; width:750px; height:65px; z-index:100; left: 3%; top: 6%;position:absolute;padding:0px 5px 0px 5px" class="ui-jqgrid ui-widget ui-widget-content ui-corner-all">
                     </br>
                     <table aling="center" style=" width: 100%" >
                         <tr>
                             <td style="width: 10%"><span>Nombre:</span></td>
                             <td style="width: 30%"><input type="text" id="nombre" style=" width: 230px" name="nombre"</td>
                             <td style="width: 8%"><span>Codigo:</span></td>
                             <td style="width: 12%"><input type="text" id="codigo" style=" width: 110px" name="codigo"</td>
                             <td style="width: 15%"><span>Cod Central Riesgo:</span></td>
                             <td style="width: 25%"><input type="text" id="codCentral" name="codCentral"</td>
                         </tr>
                     </table>
                 </div>   
                 </br>
                 <div id="boton_guardar" style="position:absolute; left: 40%; top: 17%; width: 100%"> 
                     <span aling="center"  class="form-submit-button form-submit-button-simple_green_apple" onClick="guardarUnidNegocio();"> Guardar </span>
                 </div> 
                 <div id="boton_asociar" style="position:absolute; left: 40%; top: 17%; width: 100%; display: none"> 
                     <span aling="center"  class="form-submit-button form-submit-button-simple_green_apple" onClick="asociarConvenios();"> Asociar Convenios </span>
                 </div>    
                 <div id="div_convenio" style="display: none; position:absolute; left: 0%; top: 22%; width: 100% ">    
                     <table border="0"  align="center">
                         <tr>
                             <td>
                                 <table id="convenios" ></table>
                             </td>
                         </tr>
                     </table>   
                 </div>
             </div> 
         </div> 
         
         <div id="div_editar" style="display: none; width: 800px"> 
             <div class="content-popupNegocios" > 
             <table aling="center" style=" width: 100%" >
                 <tr>
                     <td  class="titulo_ventana" height="100%">
                         <div style="float: left; font-size: 15px">EDITAR UNIDAD DE NEGOCIO</div> 
                         <div style="float:right"><a id="closeEditar" class="ui-widget-header ui-corner-all"><span>X</span></a></div>
                     </td>
                 </tr>
             </table>  
             </br>
             <div id="filtros" style="display:table-cell; width:750px; height:65px; z-index:100; left: 3%; top: 6%;position:absolute;padding:0px 5px 0px 5px" class="ui-jqgrid ui-widget ui-widget-content ui-corner-all">

                 </br>
                 <table aling="center" style=" width: 100%" >
                     <tr>
                         <input type="hidden" id="idUnidad" name="idUnidad">
                         <td style="width: 10%"><span>Nombre:</span></td>
                         <td style="width: 30%"><input type="text" id="nombreUnd" style=" width: 230px" name="nombre"</td>
                         <td style="width: 8%"><span>Codigo:</span></td>
                         <td style="width: 12%"><input type="text" id="codigoUnd"  style=" width: 110px" name="codigo"</td>
                         <td style="width: 15%"><span>Cod Central Riesgo:</span></td>
                         <td style="width: 25%"><input type="text" id="codCentralR" name="codCentral"</td>
                     </tr>
                 </table>
             </div> 
             </br>
             <div id="boton_guardar" style="position:absolute; left: 35%; top: 17%; width: 100%"> 
                 <span aling="center"  class="form-submit-button form-submit-button-simple_green_apple" onClick="actualizarUnidNegocio();"> Actualizar </span>
             </div>
             <div id="bt_listar_conv" style="position:absolute; left: 50%; top: 17%; width: 100%; display: none "> 
                     <span aling="center"  class="form-submit-button form-submit-button-simple_green_apple" onClick="listarConveniosUnidad();"> Listar Convenios </span>
             </div>
             <div id="bt_asociar_conv" style="position:absolute; left: 50%; top: 17%; width: 100%; display:  none "> 
                     <span aling="center"  class="form-submit-button form-submit-button-simple_green_apple" onClick="asociarConveniosUnd();"> Asociar Convenios </span>
             </div>
             <div id="div_convenio_und" style="display: none; position: absolute; left: 0%; top: 45%; width: 100% ">    
                     <table border="0"  align="center">
                         <tr>
                             <td>
                                 <table id="conveniosUnd" ></table>
                             </td>
                         </tr>
                     </table>   
            </div>
             <div id="div_convenioNegocio" style="position:absolute; left: 0%; top: 22%; width: 100% ">    
                 <table border="0"  align="center">
                     <tr>
                         <td>
                             <table id="conveniosNegocio" ></table>
                         </td>
                     </tr>
                 </table>   
             </div>
         </div>
       </div>      
    </body>
</html>
