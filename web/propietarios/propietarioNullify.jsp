<%@page contentType="text/html"%>
<%@ page session="true"%>
<%@ page errorPage="../error/ErrorPage.jsp"%>
<%@page import="java.util.*"%>
<%@page import="com.tsp.operation.model.*"%>
<%@page import="com.tsp.util.*"%>
<%@page import="com.tsp.operation.model.beans.*"%>
<%@ include file="/WEB-INF/InitModel.jsp"%>
<%@ taglib uri="http://jakarta.apache.org/taglibs/input-0.90" prefix="input" %>

<html>
<head>
<title>Anular Propietario</title>
<script type='text/javascript' src="<%= BASEURL %>/js/validar.js"></script>
<script SRC='<%=BASEURL%>/js/boton.js'></script>
<link href="../css/estilostsp.css" rel='stylesheet' type="text/css">
</head>
<body onresize="redimensionar()" onload = 'redimensionar()'>
<div id="capaSuperior" style="position:absolute; width:100%; height:100px; z-index:0; left: 0px; top: 0px;">
 <jsp:include page="/toptsp.jsp?encabezado=Propietarios"/>
</div>
<div id="capaCentral" style="position:absolute; width:100%; height:87%; z-index:0; left: 0px; top: 100px; overflow: scroll;">
<%String ced="", pnom = "", snom = "", pape = "", sape = "", dist = "";

String msg = "";
String mensaje = request.getParameter("msg");
int sw = 0;//boton
if (request.getParameter("msg").equals("deB")){
	msg = "";
	ced = request.getParameter("c");
	pnom = request.getParameter("pn");
	snom = request.getParameter("sn");
	pape = request.getParameter("pa");
	sape = request.getParameter("sa");
	dist = request.getParameter("d");
}
else if (mensaje.equals("exitoA")){
	String res = (String) request.getAttribute("res");
	msg = "EL propietario con cedula " + res + " ha sido anulado";
	sw = 1;
}
%>
<form name="form1" id="form1" method="post" action="<%=CONTROLLER%>?estado=Propietario&accion=Nullify">
  <table width="650"  border="2" align="center" >
  <tr>
    <td  height="63">
  <table width="100%" border="" align="center"  class="tablaInferior">
    <tr>
      <td width="392" height="24"  class="subtitulo1">Anular Propietario </td>
      <td width="392"  class="barratitulo"><img src="<%=BASEURL%>/images/titulo.gif" width="32" height="20"></td>
    </tr>
  </table>
  <table width="100%"  align="center" >
    <tr class="fila">
      <td width="100" nowrap ><strong>Cedula</strong></td>
      <td colspan="3" nowrap>
	    <input name="ced" type="text" class="textbox" id="ced" maxlength="15" value="<%=ced%>"></td>
      </tr>
    <tr class="fila">
      <td width="100" nowrap ><strong>Primer Nombre </strong></td>
      <td width="192" nowrap ><input name="pnombre" type="text" class="textbox" id="pnombre" value="<%=pnom%>" size="34" maxlength="30"></td>
      <td width="118" nowrap>Segundo Nombre </td>
      <td width="194"nowrap ><input name="snombre" type="text" class="textbox" id="snombre" value="<%=snom%>" size="34" maxlength="30"></td>
    </tr>
    <tr  class="fila">
      <td nowrap ><strong>Primer Apellido </strong></td>
      <td nowrap><input name="papellido" type="text" class="textbox" id="papellido" value="<%=pape%>" size="34" maxlength="30"></td>
      <td nowrap><strong>Segundo Apellido </strong></td>
      <td nowrap ><input name="sapellido" type="text" class="textbox" id="sapellido2" value="<%=sape%>" size="34" maxlength="30"></td>
    </tr>
    <tr class="fila">
      <td ><strong>Distrito</strong></td>
      <td colspan="3"><input name="dstrct" type="text" class="textbox" id="dstrct" value="<%=dist%>" readonly></td>
      </tr>
  </table>  </td>
  </tr>
  </table>
</form>
<%if ( sw == 0 ){%>
<center>
	<img src="<%=BASEURL%>/images/botones/anular.gif" name="%i_buscar" onClick="form1.submit();" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);">  <img src="<%=BASEURL%>/images/botones/regresar.gif" name="%i_regresar" onClick="window.location='<%=BASEURL%>/propietarios/propietarioSearch.jsp?msg=vacio';" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);">
</center>
<%} else {%>
<center>
	<img src="<%=BASEURL%>/images/botones/regresar.gif" name="%i_regresar" onClick="window.location='<%=BASEURL%>/propietarios/propietarioSearch.jsp?msg=vacio';" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);">
</center>
<%}
if (mensaje.equals("exitoA")) { 
%>
<table border="2" align="center">
  <tr>
    <td>
	<table width="100%"  border="1" align="center"  bordercolor="#F7F5F4" bgcolor="#FFFFFF">
      <tr>
        <td width="229" align="center" class="mensajes"><%= msg %></td>
        <td width="29"><img src="<%=BASEURL%>/images/cuadronaranja.JPG"></td>
      </tr>
    </table></td>
  </tr>
</table>
<%}%>
</div>
</body>
</html>