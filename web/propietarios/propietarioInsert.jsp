<%@page contentType="text/html"%>
<%@ page session="true"%>
<%@ page errorPage="../error/ErrorPage.jsp"%>
<%@page import="java.util.*"%>
<%@page import="com.tsp.operation.model.*"%>
<%@page import="com.tsp.util.*"%>
<%@page import="com.tsp.operation.model.beans.*"%>
<%@ include file="/WEB-INF/InitModel.jsp"%>
<%@ taglib uri="http://jakarta.apache.org/taglibs/input-0.90" prefix="input" %>

<html>
<head>
<script>
function CamposPropietario(){
	
	if (form1.ced.value == ""){
		alert("Verifique que haya ingresado la Cedula");
		return (false);
	}
	if (form1.pnombre.value == ""){
		alert("Verifique que haya ingresado el Primer Nombre del Propietario");
		return (false);
	}
	if (form1.papellido.value == ""){
		alert("Verifique que haya ingresado el Primer Apellido del Propietario");
		return (false);
	}
	if (form1.sapellido.value == ""){
		alert("Verifique que haya ingresado el Segundo Apellido del Propietario");
		return (false);
	}
	form1.submit();
}
</script>
<title>Registrar Propietario</title>
<script type='text/javascript' src="<%= BASEURL %>/js/validar.js"></script>
<script SRC='<%=BASEURL%>/js/boton.js'></script>
<link href="../css/estilostsp.css" rel='stylesheet' type="text/css">
</head>
<body onresize="redimensionar()" onload = 'redimensionar()'>
<div id="capaSuperior" style="position:absolute; width:100%; height:100px; z-index:0; left: 0px; top: 0px;">
 <jsp:include page="/toptsp.jsp?encabezado=Propietarios"/>
</div>
<div id="capaCentral" style="position:absolute; width:100%; height:87%; z-index:0; left: 0px; top: 100px; overflow: scroll;">
<form name="form1" id="form1" method="post" action="<%=CONTROLLER%>?estado=Propietario&accion=Insert" onSubmit="return validarCamposLlenos();">
  <table width="650" border="2" align="center" >
    <tr>
      <td  height="63">
        <table width="100%" border="" align="center"  class="tablaInferior">
          <tr>
            <td width="392" height="24"  class="subtitulo1">Registrar Propietario </td>
      		<td width="392"  class="barratitulo"><img src="<%=BASEURL%>/images/titulo.gif" width="32" height="20"></td>
    	  </tr>
  		</table>
  		<table width="100%"  align="center" class="tablaInferior" >
    	  <tr class="fila">
      		<td width="100" nowrap ><strong>Cedula</strong></td>
      		<td colspan="3" nowrap>
	    	  <input name="ced" type="text" class="textbox" id="ced" maxlength="15"><img src="<%=BASEURL%>/images/botones/iconos/obligatorio.gif"></td>
      	  </tr>
    	  <tr class="fila">
      		<td width="100" nowrap ><strong>Primer Nombre </strong></td>
      		<td width="192" nowrap ><input name="pnombre" type="text" class="textbox" id="pnombre" size="30" maxlength="30"><img src="<%=BASEURL%>/images/botones/iconos/obligatorio.gif"></td>
      		<td width="118" nowrap>Segundo Nombre </td>
      		<td width="194"nowrap ><input name="snombre" type="text" class="textbox" id="snombre" size="34" maxlength="30"></td>
    	  </tr>
    	  <tr  class="fila">
      		<td nowrap ><strong>Primer Apellido </strong></td>
      		<td nowrap><input name="papellido" type="text" class="textbox" id="papellido" size="34" maxlength="30"><img src="<%=BASEURL%>/images/botones/iconos/obligatorio.gif"></td>
      		<td nowrap><strong>Segundo Apellido </strong></td>
      		<td nowrap ><input name="sapellido" type="text" class="textbox" id="sapellido2" size="34" maxlength="30"><img src="<%=BASEURL%>/images/botones/iconos/obligatorio.gif"></td>
    	  </tr>
    	  <tr class="fila">
      		<td ><strong>Distrito</strong></td>
      		<td colspan="3">
			  <select name="dstrct" class="listmenu" id="dstrct">
          		<option value="FINV">FINV</option>
        	  </select><img src="<%=BASEURL%>/images/botones/iconos/obligatorio.gif"></td>
      	  </tr>
  		</table>  
	  </td>
  	</tr>
  </table>
  <table width="650" border="2" align="center">
    <tr>
      <td  height="63">
        <table width="100%" align="center" class="tablaInferior">
          <tr>
    		<td width="392" height="24"  class="subtitulo1">Referencias</td>
    		<td width="392"  class="barratitulo"><img src="<%=BASEURL%>/images/titulo.gif" width="32" height="20"></td>
  		  </tr>
		</table>
		<table width="100%" align="center">
  		  <tr>
    		<td height="22" style="cursor:hand " class="Simulacion_Hiper" onClick="agregarReferencia('<%=CONTROLLER%>')">Agregar referencias a este propietario </td>
    	  </tr>
		</table>
      </td>
  	</tr>
  </table>
</form>
<center>
  <img src="<%=BASEURL%>/images/botones/aceptar.gif" name="i_aceptar" onClick="return CamposPropietario();" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);"> <img src="<%=BASEURL%>/images/botones/cancelar.gif" name="i_cancelar" onClick="form1.reset();" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);"> <img src="<%=BASEURL%>/images/botones/salir.gif" name="i_salir" onClick="parent.close();" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);">
</center>
<p>

<% String msg = request.getParameter("msg");
   if (!msg.equals("")) {%>
<table border="2" align="center">
  <tr>
    <td>
	  <table width="100%"  border="1" align="center"  bordercolor="#F7F5F4" bgcolor="#FFFFFF">
        <tr>
          <td width="229" align="center" class="mensajes"><%=msg%></td>
          <td width="29" background="<%=BASEURL%>/images/cuadronaranja.JPG">&nbsp;</td>
          <td width="58">&nbsp;</td>
        </tr>
      </table>
	</td>
  </tr>
</table>
<%}%>
</div>
</body>
</html>
<script>
function agregarReferencia(CONTROLLER) {
	if(form1.ced.value==""){
		alert("Debe digitar una cedula");
		form1.ced.focus();
	} else {
		window.open(CONTROLLER+"?estado=Referencia&accion=Insert&tiporeferencia=PR&opcion=LANZAR&documento="+form1.ced.value,'Trafico','width=700,height=700,scrollbars=yes,resizable=yes,top=10,left=65');
	}
}
</script>