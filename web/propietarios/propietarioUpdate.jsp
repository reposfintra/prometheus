<%@page contentType="text/html"%>
<%@page session="true"%>
<%@page import="java.util.*"%>
<%@page import="com.tsp.operation.model.*"%>
<%@page import="com.tsp.operation.model.beans.*"%>
<%@page import="com.tsp.util.*"%>
<%@include file="/WEB-INF/InitModel.jsp"%>
<html>
<head>
<title>Modificar Propietario</title>
<script type='text/javascript' src="<%= BASEURL %>/js/validar.js"></script>
<script type='text/javascript' src="<%= BASEURL %>/js/validar.js"></script>
<script SRC='<%=BASEURL%>/js/boton.js'></script>
<link href="../css/estilostsp.css" rel='stylesheet' type="text/css">
</head>
<body onresize="redimensionar()" onload = 'redimensionar()'>
<div id="capaSuperior" style="position:absolute; width:100%; height:100px; z-index:0; left: 0px; top: 0px;">
 <jsp:include page="/toptsp.jsp?encabezado=Propietarios"/>
</div>
<div id="capaCentral" style="position:absolute; width:100%; height:87%; z-index:0; left: 0px; top: 100px; overflow: scroll;">
<p>
  <%String ced="", pnom = "", snom = "", pape = "", sape = "", dist = "";

String msg = "";
if (request.getParameter("msg").equals("deB")){
	msg = "";
	ced = request.getParameter("c");
	pnom = request.getParameter("pn");
	snom = request.getParameter("sn");
	pape = request.getParameter("pa");
	sape = request.getParameter("sa");
	dist = request.getParameter("d");
}
else if (request.getParameter("msg").equals("exitoM")){
	msg = "La modificaci&oacute;n fue exitosa!";
	Propietario p = (Propietario) request.getAttribute("prop");
	ced = p.getCedula();
	pnom = p.getP_nombre();
	snom = p.getS_nombre();
	pape = p.getP_apellido();
	sape = p.getS_apellido();
	dist = p.getdstrct();
}
%>
<form name="form1" id="form1" method="post" action="<%=CONTROLLER%>?estado=Propietario&accion=Update" onSubmit="return validarCamposLlenos();">
<table width="650" border="2" align="center" >
  <tr>
    <td  height="63">
  <table width="100%" align="center" >
    <tr>
      <td width="392" height="24"  class="subtitulo1">Registrar Propietario </td>
      <td width="392"  class="barratitulo"><img src="<%=BASEURL%>/images/titulo.gif" width="32" height="20"></td>
    </tr>
  </table>
  <table width="100%" align="center">
    <tr class="fila">
      <td width="100" nowrap ><strong>Cedula</strong></td>
      <td colspan="3" nowrap>
	    <input name="ced" type="text" class="textbox" id="ced" maxlength="15" value="<%=ced%>"></td>
      </tr>
    <tr class="fila">
      <td width="100" nowrap ><strong>Primer Nombre </strong></td>
      <td width="192" nowrap ><input name="pnombre" type="text" class="textbox" id="pnombre" value="<%=pnom%>" size="34" maxlength="30"></td>
      <td width="118" nowrap>Segundo Nombre </td>
      <td width="194"nowrap ><input name="snombre" type="text" class="textbox" id="snombre" value="<%=snom%>" size="34" maxlength="30"></td>
    </tr>
    <tr  class="fila">
      <td nowrap ><strong>Primer Apellido </strong></td>
      <td nowrap><input name="papellido" type="text" class="textbox" id="papellido" value="<%=pape%>" size="34" maxlength="30"></td>
      <td nowrap><strong>Segundo Apellido </strong></td>
      <td nowrap ><input name="sapellido" type="text" class="textbox" id="sapellido2" value="<%=sape%>" size="34" maxlength="30"></td>
    </tr>
    <tr class="fila">
      <td ><strong>Distrito</strong></td>
      <td colspan="3"><input name="dstrct" type="text" class="textbox" id="dstrct" value="<%=dist%>" readonly></td>
      </tr>
  </table>  </td>
  </tr>
  </table>
  <table width="650" border="2" align="center">
  <tr>
    <td  height="63">
<table width="100%"  align="center">
  <tr>
    <td width="392" height="24"  class="subtitulo1">Referencias</td>
    <td width="392"  class="barratitulo"><img src="<%=BASEURL%>/images/titulo.gif" width="32" height="20"></td>
  </tr>
</table>
<table width="100%" align="center">
  <tr>
    <td height="22" style="cursor:hand " class="Simulacion_Hiper" onClick="modReferencia('<%=BASEURL%>')">Modificar  referencias del propietario </td>
    </tr>
</table>
    </td>
  </tr>
</table>
</form>
<center>
	<img src="<%=BASEURL%>/images/botones/aceptar.gif" name="%i_aceptar" onClick="form1.submit();" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);">  <img src="<%=BASEURL%>/images/botones/regresar.gif" name="%i_regresar" onClick="window.location='<%=BASEURL%>/propietarios/propietarioSearch.jsp?msg=vacio';" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);"> <img src="<%=BASEURL%>/images/botones/salir.gif" name="%i_salir" onClick="parent.close();" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);">    	 
</center>
<%if(!msg.equals("")){%>
	<p>
   <table align="center">
     <tr>
    <td><table width="100%"  border="1" align="center"  bordercolor="#F7F5F4" bgcolor="#FFFFFF">
      <tr>
        <td width="229" align="center" class="mensajes"><%=msg%></td>
        <td width="29" background="<%=BASEURL%>/images/cuadronaranja.JPG">&nbsp;</td>
        <td width="58">&nbsp;</td>
      </tr>
    </table></td>
  </tr>
</table>
  </p>
   <%}%>
</div>
</body>
</html>
<script>
function modReferencia(BASEURL) {
    var pag="";
	if(form1.ced.value==""){
		alert("Debe digitar una cedula");
		form1.ced.focus();
	} else {
	    pag = "/jsp/hvida/referencias/referenciaUpdate.jsp?tipo=PR-_-documento="+form1.ced.value;
		window.open(BASEURL+"/Marcostsp.jsp?encabezado=Actualizar&dir="+pag,'Trafico','width=700,height=700,scrollbars=yes,resizable=yes,top=10,left=65');
	}
}
</script>