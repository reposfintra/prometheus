<%@page contentType="text/html"%>
<%@page session="true"%>
<%@page import="java.util.*"%>
<%@page import="com.tsp.operation.model.*"%>
<%@page import="com.tsp.operation.model.beans.*"%>
<%@page import="com.tsp.util.*"%>
<%@include file="/WEB-INF/InitModel.jsp"%>
<html>
<head>
<title>Buscar Propietario</title>
<script type='text/javascript' src="<%= BASEURL %>/js/validar.js"></script>
<script SRC='<%=BASEURL%>/js/boton.js'></script>
<link href="../css/estilostsp.css" rel='stylesheet' type="text/css">
</head>
<body onresize="redimensionar()" onload = 'redimensionar()'>
<div id="capaSuperior" style="position:absolute; width:100%; height:100px; z-index:0; left: 0px; top: 0px;">
 <jsp:include page="/toptsp.jsp?encabezado=Propietarios"/>
</div>
<div id="capaCentral" style="position:absolute; width:100%; height:87%; z-index:0; left: 0px; top: 100px; overflow: scroll;">
<%
String ced="", pnom = "", snom = "", pape = "", sape = "", dist = "";

String msg = "";
Propietario p = null;
int sw = 0;// 0=binactivo 1=bactivo
if (request.getParameter("msg").equals("vacio")){
	msg = "";
}
else if (request.getParameter("msg").equals("errorB")){
	msg = "Su b&uacute;squeda no arroj&oacute; resultados!";
	ced = request.getParameter("c");
}
else if (request.getParameter("msg").equals("exitoB")){
	sw = 1;
	msg = "exito";
	p = (Propietario) request.getAttribute("prop");
	ced = p.getCedula();
	pnom = p.getP_nombre();
	snom = p.getS_nombre();
	pape = p.getP_apellido();
	sape = p.getS_apellido();
	dist = p.getdstrct();
}
%>
<form name="form1" id="form1" method="post" action="<%=CONTROLLER%>?estado=Propietario&accion=Search">
  <table width="650" border="2" align="center" >
    <tr>
      <td  height="63">
        <table width="99%"  align="center"   class="tablaInferior">
          <tr>
            <td width="392" height="24"  class="subtitulo1">Buscar  Propietario </td>
      		<td width="392"  class="barratitulo"><img src="<%=BASEURL%>/images/titulo.gif" width="32" height="20"></td>
          </tr>
  		</table>
  		<table width="99%"  align="center">
    	  <tr class="fila">
      	  	<td width="100" nowrap ><strong>Cedula</strong></td>
      		<td colspan="3" nowrap>
	    	  <input name="ced" type="text" class="textbox" id="ced" maxlength="15" value="<%=ced%>"></td>
      	  </tr>
    	  <tr class="fila">
      		<td width="100" nowrap ><strong>Primer Nombre </strong></td>
      		<td width="192" nowrap ><input name="pnombre" type="text" class="textbox" id="pnombre"  readonly value="<%=pnom%>" size="34" maxlength="30"></td>
      		<td width="118" nowrap>Segundo Nombre </td>
      		<td width="194"nowrap ><input name="snombre" type="text" class="textbox" id="snombre" readonly value="<%=snom%>" size="34" maxlength="30"></td>
    	  </tr>
    	  <tr  class="fila">
      		<td nowrap ><strong>Primer Apellido </strong></td>
      		<td nowrap><input name="papellido" type="text" class="textbox" id="papellido" readonly value="<%=pape%>" size="34" maxlength="30"></td>
      		<td nowrap><strong>Segundo Apellido </strong></td>
      		<td nowrap ><input name="sapellido" type="text" class="textbox" id="sapellido2" readonly value="<%=sape%>" size="34" maxlength="30"></td>
    	  </tr>
    	  <tr class="fila">
      		<td ><strong>Distrito</strong></td>
      		<td colspan="3"><input name="dstrct" type="text" class="textbox" id="dstrct" value="<%=dist%>" readonly></td>
      	  </tr>
	   	</table>
	  </td>
  	</tr>
  </table>
</form>
<%if (sw != 1) {%>
<center>
	<img src="<%=BASEURL%>/images/botones/buscar.gif" name="%i_buscar" onClick="form1.submit();" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);"> <img src="<%=BASEURL%>/images/botones/cancelar.gif" name="%i_cancelar" onClick="form1.reset();" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);">  <img src="<%=BASEURL%>/images/botones/salir.gif" name="%i_salir" onClick="parent.close();" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);">    	 
</center>
<%} else {%>
<center>
	<img src="<%=BASEURL%>/images/botones/modificar.gif" name="%i_modificar" onClick="window.location='<%=BASEURL%>/propietarios/propietarioUpdate.jsp?msg=deB&c=<%=ced%>&pn=<%=pnom%>&sn=<%=snom%>&pa=<%=pape%>&sa=<%=sape%>&d=<%=dist%>';" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);">  <img src="<%=BASEURL%>/images/botones/anular.gif" name="%i_salir" onClick="window.location='<%=BASEURL%>/propietarios/propietarioNullify.jsp?msg=deB&c=<%=ced%>&pn=<%=pnom%>&sn=<%=snom%>&pa=<%=pape%>&sa=<%=sape%>&d=<%=dist%>';" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);"> <img src="<%=BASEURL%>/images/botones/salir.gif" name="%i_salir" onClick="parent.close();" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);">    	 
</center>
<%}%>
<p>
<%
    if ((!msg.equals("exito"))&&(!msg.equals(""))) {  
%>
<p>
<table border="2" align="center">
  <tr>
    <td>
	<table width="100%"  border="1" align="center"  bordercolor="#F7F5F4" bgcolor="#FFFFFF">
      <tr>
        <td width="229" align="center" class="mensajes"><%= msg %></td>
        <td width="29"><img src="<%=BASEURL%>/images/cuadronaranja.JPG"></td>
      </tr>
    </table></td>
  </tr>
</table>
<% } %>
</div>
</body>
</html>