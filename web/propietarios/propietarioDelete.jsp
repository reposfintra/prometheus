<%@ page session="true"%>
<%@ page errorPage="/error/ErrorPage.jsp"%>
<%@page import="com.tsp.operation.model.beans.*, java.util.*"%>
<%@ include file="/WEB-INF/InitModel.jsp"%>
<html>
<head>
<title>Ingresar proveedor ACPM</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<script src="js/validar.js"></script>
<link href="css/letras.css" rel="stylesheet" type="text/css">

</head>

<body>
<%Usuario usuario = (Usuario) session.getAttribute("Usuario");%>
<table width="530" border="1" align="center" cellpadding="3" cellspacing="2" bordercolor="#CCCCCC" bgcolor="ECE0D8" class="Letras">
  <tr bgcolor="#FFA928">
    <td colspan="2" nowrap><div align="center" class="Estilo2"><strong><strong>ESCOJA EL PROVEEDOR ACPM A ANULAR </strong></strong></div></td>
  </tr>
  <tr bgcolor="#99CCFF">
    <td width="162" nowrap bgcolor="#99CCFF"><strong>NIT:</strong></td>
    <td width="352" nowrap bgcolor="#99CCFF"><strong>SUCURSAL:</strong> </td>
  </tr>
  <%if(model.proveedoracpmService.existProveedoresAcpm(usuario.getDstrct())){
			List list = model.proveedoracpmService.getProveedoresACPM(usuario.getDstrct());
			int i=0;
			Iterator it=list.iterator();
				while (it.hasNext()){%>
  <%Proveedor_Acpm pa = (Proveedor_Acpm) it.next();
					
					String  nit= pa.getNit();
					String desc=pa.getNombre();
					String codigo=pa.getCodigo();%>
  <tr style="cursor:hand" title="Anular Proveedor de Acpm..." onClick="window.location='<%=CONTROLLER%>?estado=Acpm&accion=Search&num=2&nit=<%=nit%>&codigo=<%=codigo%>'" onMouseOver="bgColor='#99cc99'" onMouseOut="bgColor='ECE0D8'">
    <td nowrap bgcolor="ECE0D8"><%=desc%></td>
    <td nowrap bgcolor="ECE0D8"><%=codigo%></td>
  </tr>
  <%}
	  }%>
</table>
<br>

<form name="form2" method="post" action="<%=CONTROLLER%>?estado=Acpm&accion=Delete&cmd=show">
  <%String nit="";
  	String distrito="", cciudad="", tipo="", moneda="", nombre="", codigo="";
	float  acpm=0, e_max=0;
 	if(request.getAttribute("pacpm")!=null){
     	Proveedor_Acpm pacpm= (Proveedor_Acpm)request.getAttribute("pacpm");
		nit=pacpm.getNit();
        cciudad=pacpm.getCity_code();
        e_max=pacpm.getMax_e();
        moneda=pacpm.getMoneda();
        tipo=pacpm.getTipo();
        acpm=pacpm.getValor();
		nombre=pacpm.getNombre();
		codigo=pacpm.getCodigo();
		distrito=pacpm.getDstrct();
				
	}%>
 
  <table width="530" border="1" align="center" cellpadding="3" cellspacing="2" bordercolor="#CCCCCC" bgcolor="#EFE3DE" class="Letras">
    <tr bgcolor="#FFA928">
      <td colspan="2" nowrap><div align="center" class="Estilo2"><strong><strong>ANULA PROVEEDOR ACPM </strong></strong></div></td>
    </tr>
    <tr bgcolor="#99CCFF">
      <td width="168" rowspan="2" nowrap bgcolor="#99CCFF"><strong>Nit:</strong></td>
      <td width="346" nowrap bgcolor="ECE0D8">      <%=nit%>
        <input name="codigo" type="hidden" id="codigo" value="<%=codigo%>">
      <input name="nit" type="hidden" id="nit" value="<%=nit%>" ></td>
    </tr>
    <tr bgcolor="#99CCFF">
      <td nowrap bgcolor="ECE0D8"><%=nombre%></td>
    </tr>
    <tr bgcolor="#99CCFF">
      <td nowrap bgcolor="#99CCFF"><strong>Distrito:</strong></td>
      <td nowrap bgcolor="ECE0D8"><%=distrito%></td>
    </tr>
    <tr bgcolor="#99CCFF">
      <td nowrap bgcolor="#99CCFF"><strong>Ciudad:</strong></td>
      <td nowrap bgcolor="ECE0D8"><%=cciudad%> </td>
    </tr>
    <tr bgcolor="#99CCFF">
      <td nowrap bgcolor="#99CCFF"><strong>Tipo de Servicio:</strong></td>
      <td nowrap bgcolor="ECE0D8">
	  <%if(tipo.equals("E")){%>
      Efectivo
	  <%}else if(tipo.equals("G")){%>        
        ACPM
	<%}%>
	</td>
    </tr>
    <tr bgcolor="#99CCFF">
      <td nowrap bgcolor="#99CCFF"><strong> <%if(tipo.equals("G")){%>Valor ACPM:<%}else if(tipo.equals("E")){%>Efectivo M&aacute;ximo:<%}%></strong></td>
      <td nowrap bgcolor="ECE0D8">        <%if(tipo.equals("G")){%>        <%=acpm%>        <%}else if(tipo.equals("E")){%>        <%=e_max%>        <%}%>      </td>
    </tr>
    <tr bgcolor="#99CCFF">
      <td nowrap bgcolor="#99CCFF"><strong>Moneda:</strong></td>
      <td nowrap bgcolor="ECE0D8"><%=moneda%></td>
    </tr>
  </table>
  <%if(request.getAttribute("pacpm")!=null){%>
  <div align="center"><br>
    <input type="submit" name="Submit" value="Anular">
    <input type="button" name="Submit2" value="Regresar">
  </div>
  <%}%>
  <table width="530" border="1" align="center" bgcolor="ECE0D8" class="Letras">
  </table>
</form>
</body>
</html>
