<%@page contentType="text/html"%>
<%@ page session="true"%>
<%@ page errorPage="../error/ErrorPage.jsp"%>
<%@page import="java.util.*"%>
<%@page import="com.tsp.operation.model.*"%>
<%@page import="com.tsp.util.*"%>
<%@page import="com.tsp.operation.model.beans.*"%>
<%@ include file="/WEB-INF/InitModel.jsp"%>
<%@ taglib uri="http://jakarta.apache.org/taglibs/input-0.90" prefix="input" %>

<html>
<head>
<title>Propietario</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<script type='text/javascript' src="<%= BASEURL %>/js/validar.js"></script>
<link href="css/letras.css" rel="stylesheet" type="text/css">
</head>
<body bgcolor="#ebebeb">
<%String action = CONTROLLER+ "?estado=Propietario&accion=Insert";

String ced="", pnom = "", snom = "", pape = "", sape = "", dist = "";

String msg = "";
if (request.getParameter("msg").equals("vacio")){
	msg = "";
}
else if (request.getParameter("msg").equals("errorB")){
	msg = "Su b&uacute;squeda no arroj&oacute; resultados!";
}
else if (request.getParameter("msg").equals("exitoB")){
	msg = "Su b&uacute;squeda arroj&oacute; este resultado!";
	Propietario p = (Propietario) request.getAttribute("prop");
	ced = p.getCedula();
	pnom = p.getP_nombre();
	snom = p.getS_nombre();
	pape = p.getP_apellido();
	sape = p.getS_apellido();
	dist = p.getdstrct();
}
else if (request.getParameter("msg").equals("exitoA")){
	String res = (String) request.getAttribute("res");
	msg = "EL propietario con cedula " + res + " ha sido anulado";
}


int swmodo = 0;//0= insert, 1= modoficar/anular

if (request.getParameter("modo").equals("insert")){
	swmodo = 0;
}
else if (request.getParameter("modo").equals("update")){
	swmodo = 1;
}

%>
<form name="form1" id="form1" method="post" action="<%=action%>" onSubmit="return validarCamposLlenos();">
  <table width="530" border="1" align="center" cellpadding="3" cellspacing="2" bordercolor="#CCCCCC" bgcolor="#EFE3DE" class="Letras">
    <tr bgcolor="#FFA928">
      <td colspan="2" nowrap><div align="center" class="Estilo2"><strong>PROPIETARIOS</strong></div></td>
    </tr>
    <tr bgcolor="#99CCFF">
      <td width="166" nowrap bgcolor="#99CCFF"><strong>CEDULA</strong></td>
      <td width="288" nowrap bgcolor="ECE0D8">
	    <input name="ced" type="text" id="ced" value="<%=ced%>" maxlength="15" <%if (swmodo == 1 ){%>readonly<%}%>></td>
    </tr>
    <tr bgcolor="#99CCFF">
      <td width="166" nowrap bgcolor="#99CCFF"><strong>PRIMER NOMBRE</strong></td>
      <td nowrap bgcolor="ECE0D8"><input name="pnombre" type="text" id="pnombre" value="<%=pnom%>" maxlength="30"></td>
    </tr>
    <tr bgcolor="#99CCFF">
      <td nowrap bgcolor="#99CCFF"><strong>SEGUNDO NOMBRE</strong></td>
      <td nowrap bgcolor="ECE0D8"><input name="snombre" type="text" id="snombre" value="<%=snom%>" maxlength="30"></td>
    </tr>
    <tr bgcolor="#99CCFF">
      <td nowrap bgcolor="#99CCFF"><strong>PRIMER APELLIDO</strong></td>
      <td nowrap bgcolor="ECE0D8"><input name="papellido" type="text" id="papellido" value="<%=pape%>" maxlength="30"></td>
    </tr>
    <tr bgcolor="#99CCFF">
      <td nowrap bgcolor="#99CCFF"><strong>SEGUNDO APELLIDO</strong></td>
      <td nowrap bgcolor="ECE0D8"><input name="sapellido" type="text" id="sapellido" value="<%=sape%>" maxlength="30"></td>
    </tr>
    <tr bgcolor="#99CCFF">
      <td nowrap bgcolor="#99CCFF"><strong>DISTRITO</strong></td>
      <td nowrap bgcolor="ECE0D8">
	    <%if ( swmodo == 0 ) {%>
		<select name="dstrct" id="dstrct">
          <option value="FINV">FINV</option>
        </select>
		<%}else if ( swmodo == 1 ) {%>
	    <input name="dist" type="text" value="<%=dist%>" readonly>
		<%}%>
      </td>
    </tr>
    <tr>
      <td colspan="2" nowrap align="center">
		<%if ( swmodo == 0 ){//modo insert%>
        <input type="button" name="buscar" id="buscar" value="Buscar" title="RECUERDE: la b&uacute;squeda s&oacute;lo se realiza por cedula" onClick="window.location='<%=BASEURL%>/propietarios/prueba.jsp?ced=<%=request.getParameter("ced")%>'"><!---<%//=//CONTROLLER%>?estado=Propietario&accion=Search'">--->
        <input type="submit" name="registrar" value="Registar">		
		<%}else if ( swmodo == 1 ){//modo update%>
      	
       	<input type="button" name="anular" value="Anular" onClick="window.location='<%=CONTROLLER%>?estado=Propietario&accion=Nullify'">
		<%}%>
      </td>
    </tr>
  </table>  
</form>
<br>
<center><p><font color="red"><strong><%=msg%></strong></font></p></center>
</body>
</html>
