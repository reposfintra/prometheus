<%@page contentType="text/html"%>
<%@page session="true"%>
<%@page import="java.util.*"%>
<%@page import="com.tsp.operation.model.*"%>
<%@page import="com.tsp.operation.model.beans.*"%>
<%@page import="com.tsp.util.*"%>
<%@include file="/WEB-INF/InitModel.jsp"%>
<html>
<script>
function CamposPropietario(){
	
	if (form1.ced.value == ""){
		alert("Verifique que haya ingresado la Cedula");
		return (false);
	}
	if (form1.pnombre.value == ""){
		alert("Verifique que haya ingresado el Primer Nombre del Propietario");
		return (false);
	}
	if (form1.papellido.value == ""){
		alert("Verifique que haya ingresado el Primer Apellido del Propietario");
		return (false);
	}
	if (form1.sapellido.value == ""){
		alert("Verifique que haya ingresado el Segundo Apellido del Propietario");
		return (false);
	}
	form1.submit();
}
</script>
<head>
<title>Ingresar Propietario</title>
<script type='text/javascript' src="<%= BASEURL %>/js/validar.js"></script>
<script SRC='<%=BASEURL%>/js/boton.js'></script>
   <link href="<%= BASEURL %>/css/estilostsp.css" rel='stylesheet'>
   <link href="../../css/estilostsp.css" rel="stylesheet" type="text/css">
   <link href="../../../css/estilostsp.css" rel="stylesheet" type="text/css">
</head>
<body onresize="redimensionar()" onload = 'redimensionar()'>
<div id="capaSuperior" style="position:absolute; width:100%; height:100px; z-index:0; left: 0px; top: 0px;">
 <jsp:include page="/toptsp.jsp?encabezado=Propietarios"/>
</div>
<div id="capaCentral" style="position:absolute; width:100%; height:87%; z-index:0; left: 0px; top: 100px; overflow: scroll;">
<form name="form1" method="post" action="<%=CONTROLLER%>?estado=Propietario&accion=Insert" onSubmit="return validarCamposLlenos();">
<table width="650" border="2" align="center" bgcolor="#FFFFFF" class="tabla">
  <tr>
    <td  height="63">
  <table width="100%"  align="center"  class="tablaInferior">
    <tr>
      <td width="392" height="24"  class="subtitulo1">Registrar Propietario </td>
      <td width="392"  class="barratitulo"><img src="<%=BASEURL%>/images/titulo.gif" width="32" height="20"></td>
    </tr>
  </table>
  <table width="100%"  align="center">
    <tr class="fila">
      <td width="100" nowrap ><strong>Cedula</strong></td>
      <td colspan="3" nowrap>
	    <input name="ced" type="text" class="textbox" id="ced" value="<%=request.getParameter("ced")%>" maxlength="15"></td>
      </tr>
    <tr class="fila">
      <td width="100" nowrap ><strong>Primer Nombre </strong></td>
      <td width="192" nowrap ><input name="pnombre" type="text" class="textbox" id="pnombre" value="<%=request.getParameter("pnombre")%>" size="34" maxlength="30"></td>
      <td width="118" nowrap>Segundo Nombre </td>
      <td width="194"nowrap ><input name="snombre" type="text" class="textbox" id="snombre" value="<%=request.getParameter("snombre")%>" size="34" maxlength="30"></td>
    </tr>
    <tr  class="fila">
      <td nowrap ><strong>Primer Apellido </strong></td>
      <td nowrap><input name="papellido" type="text" class="textbox" id="papellido" value="<%=request.getParameter("papellido")%>" size="34" maxlength="30"></td>
      <td nowrap><strong>Segundo Apellido </strong></td>
      <td nowrap ><input name="sapellido" type="text" class="textbox" id="sapellido" value="<%=request.getParameter("sapellido")%>" size="34" maxlength="30"></td>
    </tr>
    <tr class="fila">
      <td ><strong>Distrito</strong></td>
      <td colspan="3">
		<select name="dstrct" class="listmenu" id="dstrct">
          <option value="FINV">FINV</option>
		  <option value="GPS">GPS</option>
        </select></td>
      </tr>
  </table>  </td>
  </tr>
  </table>
  <table width="541" border="0" align="center">
    <tr>
      <td colspan="4" nowrap align="center">
        <input name="registrar" type="hidden"  class="boton" value="Registrar">
        <input name="regresar" type="hidden"  class="boton" onClick="window.location='<%=BASEURL%>/propietarios/propietarioInsert.jsp?msg='" value="Regresar">		
      </td>
    </tr>
  </table>
</form>
<center>
	<img src="<%=BASEURL%>/images/botones/aceptar.gif" name="i_aceptar" onClick="return CamposPropietario();" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);"> <img src="<%=BASEURL%>/images/botones/cancelar.gif" name="i_cancelar" onClick="form1.reset();" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);"> <img src="<%=BASEURL%>/images/botones/salir.gif" name="i_salir" onClick="parent.close();" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);">
</center>
<p>
<%if(!request.getParameter("msg").equals("")){%>
	<p>
   <table border="2" align="center">
     <tr>
    <td><table width="100%"  border="1" align="center"  bordercolor="#F7F5F4" bgcolor="#FFFFFF">
      <tr>
        <td width="229" align="center" class="mensajes"><%=request.getParameter("msg")%></td>
        <td width="29" background="<%=BASEURL%>/images/cuadronaranja.JPG">&nbsp;</td>
        <td width="58">&nbsp;</td>
      </tr>
    </table></td>
  </tr>
</table>
  </p>
   <%}%>
</div>
</body>
</html>
