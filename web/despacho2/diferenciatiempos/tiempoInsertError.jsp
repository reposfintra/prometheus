<%@ page session="true"%>
<%@ page errorPage="../error/error.jsp"%>
<%@page import="com.tsp.operation.model.beans.*, java.util.*"%>
<%@ include file="/WEB-INF/InitModel.jsp"%>
<%@ taglib uri="http://jakarta.apache.org/taglibs/input-0.90" prefix="input" %>

<html>
<head>
<title>Ingresar codigos por demora</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<script src="<%=BASEURL%>/js/validar.js"></script>
<script src="<%=BASEURL%>/js/boton.js"></script>
<link href="<%=BASEURL%>/css/estilostsp.css" rel="stylesheet" type="text/css">
<link href="../../css/estilostsp.css" rel="stylesheet" type="text/css">

</head>

<body>
<div id="capaSuperior" style="position:absolute; width:100%; height:100px; z-index:0; left: 1px; top: 1px;">
 <jsp:include page="/toptsp.jsp?encabezado=Ingresar Tiempo Error"/>
</div>
<div id="capaCentral" style="position:absolute; width:100%; height:87%; z-index:0; left: 0px; top: 100px; overflow: scroll;">
<form name="form1" method="post" action="<%=CONTROLLER%>?estado=Tiempo&accion=Insert&cmd=show" onSubmit="">
<table border="2" align="center" width="448">
  <tr>
    <td>
	<table width="99%" align="center">
  <tr>
    <td width="49%"  class="subtitulo1"><span class="Estilo2"><strong><strong><strong>INGRESAR TIEMPO</strong></strong></strong></span></td>
    <td width="51%"  class="barratitulo"><img src="<%=BASEURL%>/images/titulo.gif" width="32" height="20"></td>
  </tr>
</table>
  <table width="99%" align="center" class="Letras">
    <tr class="fila">
      <td colspan="2" nowrap><strong><U>        DATOS DEL STANDARD</U></strong></td>
    </tr>
    <tr class="fila">
      <td width="210" nowrap><strong>Distrito:</strong></td>
      <td width="276" nowrap><select name="dstrc" class="listmenu" id="dstrc">
        <option value="FINV">FINV</option>
      </select></td>
    </tr>
    <tr class="fila">
      <td nowrap><strong>Standard Job : </strong></td>
      <td width="276" nowrap><input name="sj" type="text" class="textbox" id="sj" value="<%=request.getParameter("sj")%>" maxlength="6"></td>
    </tr>
    <tr class="fila">
      <td nowrap><strong>Codigo de la CF: </strong></td>
      <td nowrap><input name="cf" type="text" class="textbox" id="cf" value="<%=request.getParameter("cf")%>" maxlength="15"></td>
    </tr>
    <tr class="fila">
      <td colspan="2" nowrap><strong><U>        DATOS DEL LAS FECHAS </U></strong></td>
    </tr>
    <tr class="fila">
      <td nowrap><strong>Fecha 1 </strong></td>
      <td nowrap><input name="fec1" type="text" class="textbox" id="fec1" value="<%=request.getParameter("fec1")%>" maxlength="30"></td>
    </tr>
    <tr class="fila">
      <td nowrap><strong>Fecha 2 </strong></td>
      <td nowrap><input name="fec2" type="text" class="textbox" id="fec2" value="<%=request.getParameter("fec2")%>" maxlength="6"></td>
    </tr>
    <tr class="fila">
      <td nowrap><strong>Secuencia en reporte: </strong></td>
      <td nowrap>         <input name="sec" type="text" class="textbox" id="sec" value="<%=request.getParameter("sec")%>"></td>
    </tr>
    <tr class="fila">
      <td colspan="2" nowrap><strong><U>      OTROS DATOS 
      </U></strong></td>
    </tr>
    <tr class="fila">
      <td nowrap><strong>Codigo del Reporte</strong></td>
      <td nowrap><input name="reporte" type="text" class="textbox" id="reporte" value="<%=request.getParameter("reporte")%>" maxlength="15"></td>
    </tr>
  </table>
</td>
</tr>
</table>

  <br>
  <div align="center">
    <input name="Guardar" type="image"  id="Guardar" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" src="<%=BASEURL%>/images/botones/aceptar.gif" align="middle"  style="cursor:hand" height="21">
&nbsp; <img src="<%=BASEURL%>/images/botones/salir.gif"  name="imgsalir"  height="21" onMouseOver="botonOver(this);" onClick="parent.close();" onMouseOut="botonOut(this);" style="cursor:hand"> </div>
  <table width="530" border="1" align="center" bgcolor="ECE0D8" class="Letras">
  </table>
</form>
<iframe width=188 height=166 name="gToday:normal:agenda.js:gfPop:plugins.js" id="gToday:normal:agenda.js:gfPop:plugins.js" src="js/Calendario/ipopeng.htm" scrolling="no" frameborder="0" style="visibility:visible; z-index:999; position:absolute; left:-500px; top:0px;"> </iframe>
</div>
</body>
</html>
