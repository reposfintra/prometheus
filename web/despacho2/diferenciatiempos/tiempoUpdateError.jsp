<%@ page session="true"%>
<%@ page errorPage="../error/error.jsp"%>
<%@page import="com.tsp.operation.model.beans.*, java.util.*"%>
<%@ include file="/WEB-INF/InitModel.jsp"%>
<%@ taglib uri="http://jakarta.apache.org/taglibs/input-0.90" prefix="input" %>

<html>
<head>
<title>Ingresar codigos por demora</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<script src="<%=BASEURL%>/js/validar.js"></script>
<script src="<%=BASEURL%>/js/boton.js"></script>
<script src="<%=BASEURL%>/js/reporte.js"></script>
<link href="<%=BASEURL%>/css/estilostsp.css" rel="stylesheet" type="text/css">
<link href="../../css/estilostsp.css" rel="stylesheet" type="text/css">

</head>

<body>
<div id="capaSuperior" style="position:absolute; width:100%; height:100px; z-index:0; left: 1px; top: 1px;">
 <jsp:include page="/toptsp.jsp?encabezado=Modificar Tiempo"/>
</div>
<div id="capaCentral" style="position:absolute; width:100%; height:87%; z-index:0; left: 0px; top: 100px; overflow: scroll;">
	<% model.tiempoService.listar();
	   Vector tiempos = model.tiempoService.getFechas();
 	%>
<table border="2" align="center" width="581">
  <tr>
    <td>
	<table width="99%" align="center">
  <tr>
    <td width="52%" height="22"  class="subtitulo1"><span class="Estilo2"><strong>ESCOJA EL TIEMPO A MODIFICAR </strong></span></td>
    <td width="48%"  class="barratitulo"><img src="<%=BASEURL%>/images/titulo.gif" width="32" height="20"></td>
  </tr>
</table>
<table width="99%" border="1" align="center" class="letras">
    <tr class="tblTitulo">
      <td width="26%"><div align="center"><strong>DISTRITO
      </strong></div></td>
      <td width="23%"><strong>STANDARD</strong></td>
      <td width="11%"><strong>CF</strong></td>
      <td width="21%"><strong>FECHA1</strong></td>
      <td width="19%"><strong>FECHA2</strong></td>
    </tr>
	<%for(int i =0; i<tiempos.size(); i++){
		Tiempo t = (Tiempo) tiempos.elementAt(i);
	%>
	<tr class="<%=(i % 2 == 0 )?"filagris":"filaazul"%>" onMouseOver='cambiarColorMouse(this)' style="cursor:hand" title="Modificar Tiempo..." onClick="window.location='<%=CONTROLLER%>?estado=Tiempo&accion=Update&buscar=ok&dstrct=<%=t.getdstrct()%>&sj=<%=t.getsj()%>&cf=<%=t.getcf_code()%>&fec1=<%=t.gettime_code_1()%>&fec2=<%=t.gettime_code_2()%>'">
      <td><%=t.getdstrct()%></td>
      <td><%=t.getsj()%></td>
      <td><%=t.getcf_code()%></td>
      <td><%=t.gettime_code_1()%></td>
      <td><%=t.gettime_code_2()%></td>
    </tr>
	<%}%>
</table>
</td>
</tr>
</table>


<form name="form1" method="post" action="<%=CONTROLLER%>?estado=Tiempo&accion=Update&cmd=show" onSubmit="">
<%
if(request.getAttribute("tiempo")!=null){
Tiempo t= (Tiempo)request.getAttribute("tiempo");%>
   <table border="2" align="center" width="500">
  <tr>
    <td>
	<table width="99%" align="center">
  <tr>
    <td width="52%" height="22"  class="subtitulo1"><span class="Estilo2"><strong><strong>MODIFICAR TIEMPO</strong></strong></span></td>
    <td width="48%"  class="barratitulo"><img src="<%=BASEURL%>/images/titulo.gif" width="32" height="20"></td>
  </tr>
</table>
  <table width="99%" align="center" class="Letras">
    <tr class="fila">
      <td colspan="2" nowrap><strong><U>        DATOS DEL STANDARD</U></strong></td>
    </tr>
    <tr class="fila">
      <td width="217" nowrap><strong>Distrito:</strong></td>
      <td width="377" nowrap><%=t.getdstrct()%>
      <input name="dstrct" type="hidden" id="dstrct" value="<%=t.getdstrct()%>"></td>
    </tr>
    <tr class="fila">
      <td nowrap><strong>Standard Job : </strong></td>
      <td width="377" nowrap><%=t.getsj()%>
      <input name="sj" type="hidden" id="sj" value="<%=t.getsj()%>"></td>
    </tr>
    <tr class="fila">
      <td nowrap><strong>Codigo de la CF: </strong></td>
      <td nowrap><%=t.getcf_code()%>
      <input name="cf" type="hidden" id="cf" value="<%=t.getcf_code()%>"></td>
    </tr>
    <tr class="fila">
      <td colspan="2" nowrap><strong><U>        DATOS DEL LAS FECHAS </U></strong></td>
    </tr>
    <tr class="fila">
      <td  nowrap><strong>Codigo de la Fecha 1 </strong></td>
      <td nowrap><input name="fec1" type="text" class="textbox" id="fec1"  value="<%=request.getParameter("fec1")%>" maxlength="4"></td>
    </tr>
    <tr class="fila">
      <td nowrap><strong>Codigo de la Fecha 2 </strong></td>
      <td nowrap><input name="fec2" type="text" class="textbox" id="fec2"  value="<%=request.getParameter("fec2")%>" maxlength="4"></td>
    </tr>
    <tr class="fila">
      <td nowrap><strong>Secuencia en reporte: </strong></td>
      <td nowrap>          <input name="sec" type="text" class="textbox" id="sec" value="<%=request.getParameter("sec")%>"></td>
    </tr>
    <tr class="fila">
      <td colspan="2" nowrap><strong><U>      OTROS DATOS 
      </U></strong></td>
    </tr>
    <tr class="fila">
      <td nowrap><strong>Codigo del Reporte</strong></td>
      <td nowrap><input name="reporte" type="text" class="textbox" id="reporte" value="<%=request.getParameter("reporte")%>" maxlength="15"></td>
    </tr>
  </table>
</td>
</tr>
</table>
  <br>
  <div align="center">
    <input type="submit" name="Submit" value="Actualizar">
    <input type="button" name="Submit2" value="Regresar">
  </div>
  
<%}%>
</form>

<iframe width=188 height=166 name="gToday:normal:agenda.js:gfPop:plugins.js" id="gToday:normal:agenda.js:gfPop:plugins.js" src="js/Calendario/ipopeng.htm" scrolling="no" frameborder="0" style="visibility:visible; z-index:999; position:absolute; left:-500px; top:0px;"> </iframe>
</div>
</body>
</html>
