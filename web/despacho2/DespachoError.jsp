<%@ page session="true"%>
<%@ page errorPage="/error/error.jsp"%>
<%@page import="com.tsp.operation.model.beans.*, java.util.*,java.text.*"%>
<%@ include file="/WEB-INF/InitModel.jsp"%>

<html>
<head>
<title>Despacho - Inicio Despacho Masivo</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<script src="<%=BASEURL%>/js/validar.js"></script>
<script src="<%=BASEURL%>/js/boton.js"></script>
<link href="<%=BASEURL%>/css/estilostsp.css" rel="stylesheet" type="text/css">
<link href="../css/estilostsp.css" rel="stylesheet" type="text/css">
</head>

<body onLoad="<%if(request.getParameter("reload")!=null){%>form1.orden.focus();<%}else{%>form1.remision.focus();<%}%>" topmargin="0" leftmargin="0" rightmargin="0">
<div id="capaSuperior" style="position:absolute; width:100%; height:100px; z-index:0; left: 1px; top: 1px;">
 <jsp:include page="/toptsp.jsp?encabezado=DESPACHOS"/>
</div>
<div id="capaCentral" style="position:absolute; width:100%; height:87%; z-index:0; left: 0px; top: 100px; overflow: scroll;">
<form name="form1" method="post" action="<%=CONTROLLER%>?estado=Despacho&accion=Validar&cmd=show" onSubmit="return ValidarFormulario(this)">
  <%String fecpla= request.getParameter("fecdsp");
java.util.Date date = new java.util.Date();
    SimpleDateFormat s = new SimpleDateFormat("yyyy-MM-dd HH:mm");
	if(fecpla == null)
		fecpla = s.format(date);   
 %>

  <%Usuario usuario = (Usuario) session.getAttribute("Usuario");
  	String remision="";
	String placa="";
	String trailer="";
	String conductor="";
	String orden="";
	String pesolm="";
	String pesovm="";
	String pesonm="";
	String anticipo="";
	String gacpm="";
	String standard="";
	String pesoll="";
	String pesov="";
	String peson="";
	String peajea="";
	String peajeb="";
	String peajec="";
	String proveedora="";
	String proveedorAcpm="";
	String tiquetes="";
	
		remision=request.getParameter("remision");
		placa=request.getParameter("placa");
		trailer=request.getParameter("trailer");
		conductor=request.getParameter("conductor");
		orden=request.getParameter("orden");
		pesolm=request.getParameter("pesolm");
		pesovm=request.getParameter("pesovm");
		pesonm=request.getParameter("pesonm");
		anticipo=request.getParameter("anticipo");
		gacpm=request.getParameter("gacpm");
		standard=request.getParameter("standard");
		pesoll=request.getParameter("pesoll");
		pesov=request.getParameter("pesov");
		peson=request.getParameter("peson");
		peajea=request.getParameter("peajea");
		peajeb=request.getParameter("peajeb");
		peajec=request.getParameter("peajec");
		proveedora=request.getParameter("proveedora");
		proveedorAcpm =request.getParameter("proveedorAcpm");
		tiquetes=request.getParameter("tiquetes");
	
  	
  %>
<table border="2" align="center" width="90%">
  <tr>
    <td>
	<table width="100%" align="center">
  <tr>
    <td width="49%"  class="subtitulo1">DATOS PREELIMINARES </td>
    <td width="51%"  class="barratitulo"><img src="<%=BASEURL%>/images/titulo.gif" width="32" height="20"></td>
  </tr>
</table>
  <table width="100%" align="center" class="LETRAS">
    <tr class="fila">
      <td width="16%"><strong>REMISION</strong></td>
      <td><span class="letras">
        <input name="remision" type="text" class="textbox" id="remision" size="6" maxlength="6" onKeyPress="soloDigitos(event,'decNO')" value="<%=remision%>">
      </span></td>
      <td colspan="2"><strong>FECHA DEL DESPACHO</strong></td>
      <td colspan="4"><input name="fecdsp" type="text" class="textbox" id="fecdsp" value="<%=request.getParameter("fecdsp")%>"></td>
    </tr>
    <tr class="fila">
      <td rowspan="2"><strong>PLACA</strong></td>
      <td width="14%"><div align="left"><span class="letras">
          <input name="placa" type="text" class="textbox" id="placa" size="8" maxlength="7" value="<%=placa.toUpperCase()%>" >
          </span><br>
      <span class="style1 Estilo1"></span></div></td>
      <td width="14%" rowspan="4"><strong>TRAILER</strong></td>
      <td width="14%" rowspan="4"><div align="center"><span class="Estilo6">
          <input name="trailer" type="text" class="textbox" id="trailer" size="8" maxlength="7" value="<%=trailer%>">
      </span></div></td>
      <td width="16%" rowspan="2"><strong>CONDUCTOR</strong></td>
      <td colspan="3"><div align="left">
          <input name="conductor" type="text" class="textbox" id="conductor" size="12" maxlength="12" value="<%=conductor%>">
          <br>
</div></td>
    </tr>
    <tr class="fila">
      <td><span class="style1 "><a href="<%=CONTROLLER%>?estado=Menu&accion=Enviar&numero=18&placa=<%=request.getParameter("placa")%>&conductor=<%=request.getParameter("conductor")%>" target="_blank">Agregar Placa</a></span><span class="style1 Estilo1"></span></td>
      <td colspan="3"><span class="Estilo6"><span class="style1"><a href="<%=CONTROLLER%>?estado=Menu&accion=Conductor&conductor=<%=request.getParameter("conductor")%>" target="_blank">Agregar Conductor</a></span></span></td>
    </tr>
    <tr class="fila">
      <td colspan="2"><div align="center"><span class="letras"><%=request.getParameter("nombreProp")%></span></div></td>
      <td colspan="4" rowspan="2"><div align="center"><span class="letras"><%=request.getParameter("nombre")%></span></div></td>
    </tr>
    <tr class="fila">
      <td><span class="Estilo3">TAG TRAFICO</span></td>
      <td><input name="trafico" type="text" class="textbox" id="trafico" value="<%=request.getParameter("trafico")%>" size="10"></td>
    </tr>
    
    <tr class="fila">
      <td><strong>ESTANDARD</strong></td>
      <td colspan="7"><span class="Estilo6">
        <%
		 if(model.stdjobdetselService.existStandardsProy(usuario.getProject())){
			List list = model.stdjobdetselService.getStandardsProy(usuario.getProject(),usuario.getBase());%>
        <select name="standard" class="listmenu" id="standard" onChange="cambiarFormulario2('<%=BASEURL%>/despacho2/InicioDespacho.jsp?reload=ok');">
          <%if(request.getParameter("reload")==null){%>
		  <option value="0">Seleccione Alguno</option>
		  <%}%>
		            <%
		   	Iterator it=list.iterator();
				while (it.hasNext()){
					Stdjobdetsel std = (Stdjobdetsel) it.next();
					String desc = std.getSj_desc();
					String sj=std.getSj();
					if(sj.equals(standard)){%>
          <option value="<%=sj%>" selected><%=desc%></option>
		  			<%}else{%>
					<option value="<%=sj%>"><%=desc%></option>
					<%}%>
          <%}%>
        </select>
        <%}else{%>
        <strong>No existe ning&uacute;n StandardJob Registrado.</strong>
        <%}%>
      </span></td>
    </tr>
    <tr class="fila">
      <td colspan="8">&nbsp;</td>
    </tr>
	<tr class="fila">
      <td><strong>ORDEN DE CARGA</strong></td>
      <td><input name="orden" type="text" class="textbox" id="orden" size="10" maxlength="10" value="<%=orden%>"></td>
      <td><div align="right"><strong>PESO LLENO MINA </strong></div></td>
      <td><div align="center">
        <input name="pesolm" type="text" class="textbox" id="pesolm2" size="10" onChange="buscarPesoN();" onKeyPress="soloDigitos(event,'decOK')" value="<%=pesolm%>">
</div></td>
      <td><div align="right"><span class="letras"><strong>PESO VACIO MINA</strong></span></div></td>
      <td width="9%">
        <div align="center">
          <input name="pesovm" type="text" class="textbox" id="pesovm2" size="10" onChange="buscarPesoN();" onKeyPress="soloDigitos(event,'decOK')" value="<%=pesovm%>">      
      </div></td>
      <td width="8%"><div align="center"><span class="letras"><strong>PESO NETO MINA</strong></span></div></td>
      <td width="9%"><div align="center">
        <input name="pesonm" type="text" class="textbox" id="pesonm" size="10" maxlength="13" readonly value="<%=pesonm%>">
      </div></td>
    </tr>
	<tr class="fila">
      <td colspan="8"><div align="center"><strong>FECHAS INICIALES </strong></div></td>
    </tr>
	
    <tr class="fila">
      <td colspan="8"><table width="100%">
        <%List listTabla2 = model.tbltiempoService.getTblTiempos(standard);
			Iterator itTbla2=listTabla2.iterator();
			while (itTbla2.hasNext()){
				Tbltiempo tbl = (Tbltiempo) itTbla2.next();
				String id_tabla=tbl.getTimeCode();
				
				%>
        <tr>
          <td width="38%"><%=tbl.getTimeDescription()%></td>
          <td width="62%" bgcolor="<%=(String)request.getAttribute("error"+tbl.getTimeCode())%>"><input name="<%=id_tabla%>" type="text" class="textbox" id="<%=id_tabla%>" size="18"  value="<%if(request.getParameter(id_tabla)!=null){%><%=request.getParameter(id_tabla)%><%}%>" >
            <a href="javascript:void(0);" class="link" onFocus="if(self.gfPop)gfPop.fFocus(document.form1.<%=id_tabla%>);if(self.gfPop)gfPop.fPopCalendar(document.form1.<%=id_tabla%>);return false;"  HIDEFOCUS ><img class="link" src="<%=BASEURL%>/js/Calendario/cal.gif" alt="Click para ver Calendario" width="16" height="16"></a>
            </td>        
        </tr>
		<%}%>
      </table></td>
    </tr>
    
	<tr bordercolor="#0066CC" class="fila">
      <td colspan="8">
        <div align="left"><strong>DATOS FINALES </strong></div></td>
    </tr>
    <tr class="fila">
      <td><div align="center" class="letras">
        <div align="left"><strong>PESO LLENO DESCARGUE </strong></div>
      </div></td>
      <td><span class="Estilo6">
      <input name="pesoll" type="text" class="textbox" id="pesoll" size="12" maxlength="13" onChange="buscarPesoNoNum();" value="<%=pesoll%>" >
</span></td>
      <td><strong>PESO VACIO DESCARGUE</strong></td>
      <td><input name="pesov" type="text" class="textbox" id="pesov" onChange="buscarPeso();" size="12" maxlength="13" value="<%=pesov%>"></td>
      <td><strong>PESO NETO DESCARGUE</strong></td>
      <td colspan="3">
        <div align="left">
          <input name="peson" type="text" class="textbox" id="peson" size="12" maxlength="13" readonly value="<%=peson%>">
      </div></td>
    </tr>
    <tr class="fila">
      <td colspan="8"><div align="center">&nbsp;</div></td>
    </tr>
    <tr class="fila">
      <td colspan="8"><div align="center"><strong>FECHAS FINALES </strong></div></td>
    </tr>
    <tr class="fila">
      <td colspan="8">
	  <table width="100%">
        <%List listTabla = model.tbltiempoService.getTblTiemposSalida(usuario.getBase());
			Iterator itTbla=listTabla.iterator();
			while (itTbla.hasNext()){
				Tbltiempo tbl = (Tbltiempo) itTbla.next();
				String id_tabla=tbl.getTimeCode();
				%>
		<tr>
          <td width="38%"><span class="LETRAS"><strong><%=tbl.getTimeDescription()%><strong> </strong></strong></span></td>
          <td width="62%" bgcolor="<%=(String)request.getAttribute("error"+tbl.getTimeCode())%>"><input name="<%=id_tabla%>" type="text" class="textbox" id="<%=id_tabla%>" size="18"  value="<%if(request.getParameter(id_tabla)!=null){%><%=request.getParameter(id_tabla)%><%}%>" onChange="">
		  <a href="javascript:void(0);" class="link" onFocus="if(self.gfPop)gfPop.fFocus(document.form1.<%=id_tabla%>);if(self.gfPop)gfPop.fPopCalendar(document.form1.<%=id_tabla%>);return false;"  HIDEFOCUS ><img class="link" src="<%=BASEURL%>/js/Calendario/cal.gif" alt="Click para ver Calendario" width="16" height="16"></a></td>
        </tr>
		<%}%>
      </table>
	  </td>
    </tr>
    <tr bordercolor="#0066CC" class="fila">
      <td colspan="8"><strong>ANTICIPOS</strong></td>
    </tr>
    <tr class="fila">
      <td><strong>EFECTIVO</strong></td>
      <td><input name="anticipo" type="text" class="textbox" size="12" id="anticipo" value="<%=anticipo%>"></td>
      <td><strong>PROVEEDOR</strong></td>
      <td colspan="5"><%
		 if(model.proveedoranticipoService.existProveedoresAnticipo(usuario.getDstrct())){
			List list = model.proveedoranticipoService.getProveedoresAnticipo(usuario.getDstrct());%>
        <select name="proveedora" class="listmenu" id="proveedora">
          <%
		   	Iterator it=list.iterator();
				while (it.hasNext()){
					
					Proveedor_Anticipo pa = (Proveedor_Anticipo) it.next();
					
					String  nit= pa.getNit()+"/"+pa.getCodigo();
					String desc=pa.getNombre()+" "+pa.getCodigo();
					if(nit.equals(proveedora)){
				%>
          <option value="<%=nit%>" selected><%=desc%> </option>
          <%}else{%>
		  <option value="<%=nit%>"><%=desc%> </option>
		  <%}
				}
			
		%>
        </select>
        <%}else{%>
        <strong>No existe ning&uacute;n Proveedor Registrado.</strong>
      <%}%></td>
    </tr>
    <tr class="fila">
      <td><strong>ACPM</strong></td>
      <td><input name="gacpm" type="text" class="textbox" size="12" value="<%=gacpm%>"></td>
      <td><strong>PROVEEDOR</strong></td>
      <td colspan="5"><%
		 if(model.proveedoracpmService.existProveedoresAcpm(usuario.getDstrct())){
			List list = model.proveedoracpmService.getProveedoresACPM(usuario.getDstrct());%>
        <select name="proveedorAcpm" class="listmenu" id="proveedorAcpm">
          <%
		   	Iterator it=list.iterator();
				while (it.hasNext()){
					
					Proveedor_Acpm pa = (Proveedor_Acpm) it.next();
					
					String  nit= pa.getNit()+"/"+pa.getCodigo();
					String desc=pa.getNombre()+" "+pa.getCodigo();
					if(nit.equals(proveedorAcpm)){
				%>
          <option value="<%=nit%>" selected><%=desc%> </option>
          <%}else{%>
		  <option value="<%=nit%>"><%=desc%> </option>
		  <%}
				
				}
			
		%>
        </select>
        <%}else{%>
        <strong>No existe ning&uacute;n Proveedor Registrado.</strong>
      <%}%></td>
    </tr>
    <tr class="fila">
      <td rowspan="2"><strong>TIQUETES</strong></td>
      <td><strong>TIPO A:</strong> <span class="letras">
      </span></td>
      <td> <strong>TIPO B: 
        </strong><span class="letras">
</span></td>
      <td><strong>TIPO C:</strong> <span class="letras">
      </span></td>
      <td colspan="4" rowspan="2"><strong>PROVEEDOR 
          <%
		 if(model.proveedortiquetesService.existProveedoresTiquetes(usuario.getDstrct())){
			List list = model.proveedortiquetesService.getProveedoresTIQUETES(usuario.getDstrct());%>
          <select name="tiquetes" class="listmenu" id="tiquetes">
            <%
		   	Iterator it=list.iterator();
				while (it.hasNext()){
					
					Proveedor_Tiquetes pa = (Proveedor_Tiquetes) it.next();
					
					String  nit= pa.getNit()+"/"+pa.getCodigo();
					String desc=pa.getNombre()+" "+pa.getCodigo();
					
				if(nit.equals(tiquetes)){
				%>
          <option value="<%=nit%>" selected><%=desc%> </option>
          <%}else{%>
		  <option value="<%=nit%>"><%=desc%> </option>
		  <%}
				}
			
		%>
          </select>
          <%}else{%>
          <strong>No existe ning&uacute;n Proveedor Registrado.</strong>
          <%}%>
      </strong></td>
    </tr>
    <tr class="fila">
      <td>
        <input name="peajea" type="text" class="textbox" id="peajea2" size="6" value="<%=peajea%>">
     </td>
      <td>
        <input name="peajeb" type="text" class="textbox" id="peajeb2" size="6" value="<%=peajeb%>">
     </td>
      <td><span class="letras">
        <input name="peajec" type="text" class="textbox" id="peajec2" size="6" value="<%=peajec%>">
      </span></td>
    </tr>
	<%model.anticiposService.vecAnticipos(usuario.getBase(),standard);
		Vector descuentos = model.anticiposService.getAnticipos();
		if(descuentos.size()>0){%>
    <tr bordercolor="#0066CC" class="fila">
	
      <td colspan="8"><strong>OTROS DESCUENTOS </strong></td>
    </tr>
	<%
		for(int k = 0 ; k<descuentos.size();k++){
			Anticipos ant = (Anticipos) descuentos.elementAt(k); 
			String codigo = ant.getAnticipo_code();
		%>
    <tr class="fila">
      <td height="32"><b><%=ant.getAnticipo_desc().toUpperCase()%></b></td>
      <td><input type="text" class="textbox" name="<%=ant.getAnticipo_code()%>" value="<%=request.getParameter(ant.getAnticipo_code())%>"></td>
      <td><strong>PROVEEDOR</strong></td>
      <td colspan="10">
        <%model.proveedoresService.listaAnticipoProvee(codigo);
		Vector provee = model.proveedoresService.getProveedores();%>
        <select name="provee<%=codigo%>" class="listmenu">
          <%for(int m =0; m<provee.size(); m++){
				Proveedores prov = (Proveedores)provee.elementAt(m);
				String nit =prov.getNit()+"/"+prov.getSucursal();
				if(nit.equals(request.getParameter("provee"+codigo))){
			%>
          <option value="<%=prov.getNit()%>/<%=prov.getSucursal()%>" selected><%=prov.getNombre()%> <%=prov.getSucursal()%></option>
          <%}else{%>
          <option value="<%=prov.getNit()%>/<%=prov.getSucursal()%>"><%=prov.getNombre()%> <%=prov.getSucursal()%></option>
          <%}
		  }%>
      </select>        </td>
	<%}
	}%>
  </table>
</td>
</tr>
</table>
  <div align="center"><br>
    <input name="Guardar" type="image"  id="Guardar" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" src="<%=BASEURL%>/images/botones/aceptar.gif" align="middle"  style="cursor:hand" height="21">
&nbsp; <img src="<%=BASEURL%>/images/botones/cancelar.gif"  name="imgsalir"  height="21" align="absmiddle" style="cursor:hand" onClick="form1.reset();" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);">&nbsp; <img src="<%=BASEURL%>/images/botones/salir.gif"  name="imgsalir"  height="21" align="absmiddle" style="cursor:hand" onClick="parent.close();" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);"> </div>
</form>
   <iframe width=188 height=166 name="gToday:datetime:agenda.js:gfPop:plugins_24.js" id="gToday:datetime:agenda.js:gfPop:plugins_24.js" src="<%=BASEURL%>/js/Calendario/ipopeng2.htm" scrolling="no" frameborder="0" style="visibility:visible; z-index:999; position:absolute; left:-500px; top:0px;">
</iframe>
</div>
</body>
</html>
