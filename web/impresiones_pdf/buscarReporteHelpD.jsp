<!--  
	 - Author(s)       :      LREALES
	 - Description     :      AYUDA DESCRICTIVA - Reporte Factura Destinatario
	 - Date            :      05/04/2005  
	 - Copyright Notice:      TRANSPORTES SANCHEZ POLO S.A.
-->
<%@page session="true"%> 
<%@page errorPage="/error/ErrorPage.jsp"%>
<%@page import="com.tsp.operation.model.beans.*, java.util.*"%>
<%@ include file="/WEB-INF/InitModel.jsp"%>
<%@ taglib uri="http://jakarta.apache.org/taglibs/input-0.90" prefix="input" %>
<html>
<head>
    <title>Descripcion de los campos a ingresar en el reporte</title>
    <link href="<%= BASEURL %>/css/estilostsp.css" rel="stylesheet" type="text/css"> 
    <link href="/css/estilostsp.css" rel="stylesheet" type="text/css">  
</head>
<body> 
<div id="capaSuperior" style="position:absolute; width:100%; height:100px; z-index:0; left: 0px; top: 0px;">
<jsp:include page="/WEB-INF/toptsp.jsp?encabezado=Ayuda - Reporte Facturas Destinatarios"/>
</div>
<div id="capaCentral" style="position:absolute; width:100%; height:85%; z-index:0; left: 0px; top: 100px;"> 
   <table width="65%" border="2" align="center" id="tabla1" >
        <td>
         <table width="100%" height="50%" align="center">
              <tr>
                <td class="subtitulo1"><strong>Impresi&oacute;n</strong></td>
                <td width="50%" class="barratitulo"><img src="<%=BASEURL%>/images/titulo.gif" width="32" height="20"></td>
              </tr>
         </table>
         <table width="100%" borderColor="#999999" bgcolor="#F7F5F4">
           <tr>
             <td colspan="2" class="tblTitulo"><strong>Reporte Facturas Destinatarios - Pantalla Preliminar</strong></td>
           </tr>
           <tr>
             <td colspan="2" class="bordereporte"><strong>INFORMACION DEL REPORTE</strong></td>
           </tr>
           <tr>
             <td width="22%"><span class="fila"><strong>Caja de Opciones</strong></span></td>
             <td width="78%"><span class="ayudaHtmlTexto">Campo de selecci&oacute;n donde escoges si deseas imprimir el reporte con las facturas y destinatarios de una 'Planilla' o una 'Remesa'.</span></td>
           </tr>
           <tr>
             <td><span class="fila"><strong>N&deg;</strong></span></td>
             <td><span class="ayudaHtmlTexto">Campo para ingresar el c&oacute;digo de la planilla o de la remesa, el cual es de m&aacute;ximo 10 caracteres que representa una combinaci&oacute;n de letras y numeros.</span></td>
           </tr>
           <tr>
             <td><span class="fila"><strong>Boton Buscar</strong></span></td>
             <td><span class="ayudaHtmlTexto">Boton que valida la busqueda de el c&oacute;digo digitado en el campo<strong> N&deg;.</strong></span></td>
           </tr>
           <tr>
             <td><span class="fila"><strong>Boton Cancelar</strong></span></td>
             <td><span class="ayudaHtmlTexto">Boton que cancela todas los operaciones realizadas, y resetea la pagina llevandola a su estado inicial.</span></td>
           </tr>
           <tr>
             <td><span class="fila"><strong>Boton Salir</strong></span></td>
             <td><span class="ayudaHtmlTexto">Boton para salir de la vista 'Impresi&oacute;n' y volver a la vista del men&uacute;.</span></td>
           </tr>
           <tr>
             <td><span class="fila"><strong>Boton Imprimir</strong></span></td>
             <td><span class="ayudaHtmlTexto">Boton para generar el PDF del reporte deseado y asi poder imprimirlo.</span></td>
           </tr>
         </table></td>
        </table>
		<p></p>
		<center>
		<img src = "<%=BASEURL%>/images/botones/salir.gif" style = "cursor:hand" name = "imgsalir" onClick = "parent.close();" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);">
		</center>
</div>  
</body>
</html>