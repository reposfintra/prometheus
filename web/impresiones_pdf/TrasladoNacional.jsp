<!--
- Autor : Ing. Juan Manuel Escando Perez
- Date  : 29 Diciembre de 2005
- Copyrigth Notice : Fintravalores S.A. S.A
-->
<%--
-@(#)
--Descripcion : Pagina JSP, que permite buscar una planilla para la impresion de la hoja de control de viaje
--%>

<%-- Declaracion de librerias--%>
<%@page contentType="text/html"%>
<%@page session="true"%>
<%@page errorPage="/error/ErrorPage.jsp"%>
<%@page import="java.util.*"%>
<%@page import="com.tsp.operation.model.*"%>
<%@page import="com.tsp.operation.model.beans.*"%>
<%@include file="/WEB-INF/InitModel.jsp"%>
<%
    String Mensaje = (request.getParameter("Mensaje")!=null)?request.getParameter("Mensaje"):"";
%>
<html>
<head><title>Impresion de Planillas</title></head>
<link href="<%= BASEURL %>/css/estilostsp.css" rel="stylesheet" type="text/css">
<script type='text/javascript' src="<%= BASEURL %>/js/boton.js"></script>
<script src='<%=BASEURL%>/js/validacionesImpresionPlanilla.js'></script>
<script src='<%= BASEURL %>/js/validar.js'></script>
<body onLoad="redimensionar();" onResize="redimensionar();">
<div id="capaSuperior" style="position:absolute; width:100%; height:100px; z-index:0; left: 0px; top: 0px;">
<jsp:include page="/WEB-INF/toptsp.jsp?encabezado=Impresión de Traslado Nacional"/>
</div>

<div id="capaCentral" style="position:absolute; width:100%; height:85%; z-index:0; left: 0px; top: 100px; overflow: scroll;"> 
	<form action="<%=CONTROLLER%>?estado=Traslado&accion=NacionalManager" method="post" name="forma" >
		<table align="center" width="400" border="2">
			<tr>
				<td>
					<TABLE width="100%" align="center"  class="tablaInferior">
				        <tr class="fila">
							<td width="60%" class="subtitulo1">Impresión de Traslado Nacional</td>
							<td width="40%" class="barratitulo"><img src="<%=BASEURL%>/images/titulo.gif" width="32" height="20"></td>
						</tr>
						<tr class="fila">
							<td align="center">Numero de la OT</td>
							<td><input name='documento' type='text' class="textbox" size="10" maxlength="10" onKeyPress="soloAlfa(event);"></td>
						</tr>
					</TABLE>
				</td>
			</tr>
		</table>	
		<input name="opcion" type='hidden' value='Buscar' >
		<p align="center">
			<input type="image" src="<%=BASEURL%>/images/botones/imprimir.gif" title="imprimir" style="cursor:hand" name="Aceptar_" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);">&nbsp;
			<img src="<%=BASEURL%>/images/botones/salir.gif" style="cursor:hand" name="salir" title="Salir al Menu Principal" onMouseOver="botonOver(this);" onClick="parent.close();" onMouseOut="botonOut(this);" >&nbsp;
		</p>
	</form>
	<br>
	<%--Mensaje de informacion--%>
	<%if(!Mensaje.equals("")){%>
		<table border="2" align="center">
			<tr>
				<td>
					<table width="100%"  border="1" align="center"  bordercolor="#F7F5F4" bgcolor="#FFFFFF">
						<tr>
							<td width="229" align="center" class="mensajes"><%= Mensaje %></td>
							<td width="29" background="<%=BASEURL%>/images/cuadronaranja.JPG">&nbsp;</td>
							<td width="58">&nbsp;</td>
						</tr>
					</table>
				</td>
			</tr>
		</table>
	<%}%>
</div>
</body>
</html>
