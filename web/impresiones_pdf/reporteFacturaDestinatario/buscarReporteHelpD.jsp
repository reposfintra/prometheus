<!--  
	 - Author(s)       :      LREALES
	 - Description     :      AYUDA DESCRICTIVA - Reporte Factura Destinatario
	 - Date            :      05/04/2005  
	 - Copyright Notice:      TRANSPORTES SANCHEZ POLO S.A.
-->
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN""http://www.w3.org/TR/html4/loose.dtd">
<%@page session="true"%> 
<%@page errorPage="/error/ErrorPage.jsp"%>
<%@page import="com.tsp.operation.model.beans.*, java.util.*"%>
<%@ include file="/WEB-INF/InitModel.jsp"%>
<%@ taglib uri="http://jakarta.apache.org/taglibs/input-0.90" prefix="input" %>
<html>
<head>
<title>Descripcion de los campos a ingresar en el reporte</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link href="<%= BASEURL %>/css/estilostsp.css" rel="stylesheet" type="text/css">
<script type='text/javascript' src="<%= BASEURL %>/js/boton.js"></script>
</head>

<body>
<% String BASEIMG = BASEURL +"/images/ayuda/impresiones_pdf/"; %>
<br>
<table width="696"  border="2" align="center">
  <tr>
    <td width="811" >
      <table width="100%" border="0" align="center">
        <tr  class="subtitulo">
          <td height="24" colspan="2"><div align="center">Impresi&oacute;n</div></td>
        </tr>
        <tr class="subtitulo1">
          <td colspan="2">Reporte Facturas Destinatarios - Pantalla Preliminar</td>
        </tr>
		<tr class="subtitulo1">
          <td colspan="2">INFORMACION DEL REPORTE</td>
        </tr>
        <tr>
          <td width="149" class="fila">
		    <div align="center">
		      <input type="image" src = "<%=BASEIMG%>image005.JPG">
		      </div></td>
          <td width="525"  class="ayudaHtmlTexto">Campo de selecci&oacute;n donde escoges si deseas imprimir el reporte con las facturas y destinatarios de una 'Planilla' o una 'Remesa'.</td>
        </tr>
        <tr>
          <td  class="fila">
		    <div align="center">
		      <input type="image" src = "<%=BASEIMG%>image004.JPG">
		      </div></td>
          <td  class="ayudaHtmlTexto">Campo para ingresar el c&oacute;digo de la planilla o de la remesa, el cual es de m&aacute;ximo 10 caracteres que representa una combinaci&oacute;n de letras y numeros.</td>
        </tr>
        <tr>
          <td class="fila">
		    <div align="center">
		      <input type="image" src = "<%=BASEURL%>/images/botones/imprimir.gif">
	          </div></td>
          <td  class="ayudaHtmlTexto">Boton que valida la busqueda de el c&oacute;digo digitado en el campo.</td>
        </tr>
		<tr>
          <td class="fila">
		    <div align="center">
		      <input type="image" src = "<%=BASEURL%>/images/botones/cancelar.gif">
		      </div></td>
          <td  class="ayudaHtmlTexto">Boton que cancela todas los operaciones realizadas, y resetea la pagina llevandola a su estado inicial.</td>
        </tr>
		<tr>
          <td width="149" class="fila">
		    <div align="center">
		      <input type="image" src = "<%=BASEURL%>/images/botones/salir.gif">
		      </div></td>
          <td width="525"  class="ayudaHtmlTexto">Boton para salir de la vista 'Impresi&oacute;n' y volver a la vista del men&uacute;.</td>
        </tr>
        <tr>
          <td width="149" class="fila">
		    <div align="center">
		      <input type="image" src = "<%=BASEURL%>/images/botones/imprimir.gif">
		      </div></td>
          <td width="525"  class="ayudaHtmlTexto">Boton para generar el PDF del reporte deseado y asi poder imprimirlo.</td>
        </tr>
      </table>
    </td>
  </tr>
</table>
<p></p>
<center>
<img src = "<%=BASEURL%>/images/botones/salir.gif" style = "cursor:hand" name = "imgsalir" onClick = "parent.close();" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);">
</center>
</body>
</html>
