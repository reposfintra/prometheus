<!--  
	 - Author(s)       :      LREALES
	 - Description     :      AYUDA DESCRICTIVA - Reporte Factura Destinatario
	 - Date            :      05/04/2005  
	 - Copyright Notice:      TRANSPORTES SANCHEZ POLO S.A.
-->
<%@ include file="/WEB-INF/InitModel.jsp"%>
<HTML>
<HEAD>
<TITLE></TITLE>
<META http-equiv=Content-Type content="text/html; charset=windows-1252">
<link href="../css/estilostsp.css" rel="stylesheet" type="text/css"> 
<link href="<%=BASEURL%>/css/estilostsp.css" rel="stylesheet" type="text/css">
<script type='text/javascript' src="<%= BASEURL %>/js/boton.js"></script> 
</HEAD>
<BODY> 
<% String BASEIMG = BASEURL +"/images/ayuda/impresiones_pdf/"; %>
  <table width="95%"  border="2" align="center">
    <tr>
      <td height="117" >
        <table width="100%" border="0" align="center">
          <tr  class="subtitulo">
            <td height="20"><div align="center">MANUAL DE IMPRESIONES WEB</div></td>
          </tr>
          <tr class="subtitulo1">
            <td>Descripci&oacute;n del funcionamiento del programa para imprimir un Reporte de Facturas y Destinatarios </td>
          </tr>
          <tr>
            <td  class="ayudaHtmlTexto"><div align="center"></div></td>
          </tr>
<tr>
            <td  class="ayudaHtmlTexto"><p class="ayudaHtmlTexto">En la siguiente pantalla se escoje si desea  el reporte de una  Planilla o Remesa, y se digita el n&uacute;mero que coresponda a dicha opci&oacute;n.</p>            </td>
          </tr>
<tr>
            <td  class="ayudaHtmlTexto"><div align="center"><IMG height=135 src="<%=BASEIMG%>image001.JPG" width=577 border=0 v:shapes="_x0000_i1143"></div></td>
          </tr>
<tr>
  <td  class="ayudaHtmlTexto"><p>&nbsp;</p>
    <p>El sistema verifica que no hayan campos vacios.</p></td>
</tr>
<tr>
  <td  class="ayudaHtmlTexto"><div align="center"><IMG height=123 src="<%=BASEIMG%>image_error001.JPG" width=299 border=0 v:shapes="_x0000_i1052"></div></td>
</tr>
<tr>
  <td  class="ayudaHtmlTexto"><p>&nbsp;</p>
    <p>El sistema verifica que toda la informaci&oacute;n ingresada est&eacute; correcta y s&iacute; exista en el sistema, de lo contrario nos muestra el siguiente mensaje, y se debe corregir y volver a presionar BUSCAR.</p></td>
</tr>
<tr>
  <td  class="ayudaHtmlTexto"><div align="center"><IMG height=168 src="<%=BASEIMG%>image_error002.JPG" width=381 border=0 v:shapes="_x0000_i1052"></div></td>
</tr>
<tr>
            <td  class="ayudaHtmlTexto"><p class="ayudaHtmlTexto">&nbsp;</p>
            <p class="ayudaHtmlTexto">Al llenar la informaci&oacute;n debidamente y presionar BUSCAR,  en la pantalla aparecen los datos que coinciden con la b&uacute;squeda realizada.</p></td>
          </tr>
<tr>
            <td  class="ayudaHtmlTexto"><div align="center"><IMG height=523 src="<%=BASEIMG%>image002.JPG" width=939 border=0 v:shapes="_x0000_i1052"></div></td>
          </tr>
<tr>
            <td  class="ayudaHtmlTexto"><p class="ayudaHtmlTexto">&nbsp;</p>
            <p class="ayudaHtmlTexto">Al verificar que la informaci&oacute;n mostrada es la deseada para imprimir, entonces se procede a generar el archivo PDF para la impresi&oacute;n, dandole Click en el Boton Imprimir.</p></td>
          </tr>
<tr>
            <td  class="ayudaHtmlTexto"><div align="center"><IMG height=617 src="<%=BASEIMG%>image003.JPG" width=743 border=0 v:shapes="_x0000_i1054"></div></td>
          </tr>
      </table></td>
    </tr>
  </table>
  <p></p>
<center>
<img src = "<%=BASEURL%>/images/botones/salir.gif" style = "cursor:hand" name = "imgsalir" onClick = "parent.close();" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);">
</center>
</BODY>
</HTML>
