<!--
	 - Author(s)       :      LREALES
	 - Description     :      Vista que maneja Reporte Factura Destinatario
	 - Date            :      04/04/2005  
	 - Copyright Notice:      TRANSPORTES SANCHEZ POLO S.A.
-->
<%@page session="true"%> 
<%@page errorPage="/error/ErrorPage.jsp"%>
<%@page import="com.tsp.operation.model.beans.*, java.util.*"%>
<%@ include file="/WEB-INF/InitModel.jsp"%>
<%@ taglib uri="http://jakarta.apache.org/taglibs/input-0.90" prefix="input" %>
<%
  String path = application.getRealPath(model.menuService.getRealUrl(request.getRequestURI()));
  String datos[] = model.menuService.getContenidoMenu(BASEURL,request.getRequestURI(),path);
%>
<html>
<head>
    <title>Reporte Facturas Destinatarios</title>
    <link href="<%= BASEURL %>/css/estilostsp.css" rel="stylesheet" type="text/css"> 
    <link href="../../css/estilostsp.css" rel="stylesheet" type="text/css"> 
    <script language="JavaScript" type="text/javascript" src="<%=BASEURL%>/js/script.js"></script>
    <script type='text/javascript' src="<%= BASEURL %>/js/boton.js"></script>
    <link href="/css/estilostsp.css" rel="stylesheet" type="text/css">
    <script type='text/javascript' src="<%=BASEURL%>/js/validar.js"></script> 
    <script>
		var controlador ="<%=CONTROLLER%>";
		
		function BuscarReporte ( tipo, numero ){
		
			if( tipo != "" && numero != "" ){
			
				document.formulario.action = controlador+"?estado=BuscarPdf&accion=Reporte&tipo="+tipo+"&numero="+numero;
				document.formulario.submit();
				
			} else{
			
				alert( "Los Campos Estan Vacios! Por Favor Verificar!!" );
				
			}
		}
		
		function ImprimirReporte ( tipo, numero ){
		
			if( tipo != "" && numero != "" ){
			
				document.formulario.action = controlador+"?estado=Pdf&accion=Reporte";
				document.formulario.submit();
				
			} else{
			
				alert( "Los Campos Estan Vacios! Por Favor Verificar!!" );
				
			}
		}		
    </script>
</head>
  <%
    //Usuario usuario = (Usuario) session.getAttribute("Usuario");
    String Mensaje = (request.getParameter("Mensaje")!=null)?request.getParameter("Mensaje"):"";
    String sw = (request.getParameter("sw")!=null)?request.getParameter("sw"):"";  
  %>
<body onLoad="redimensionar();" onResize="redimensionar();">

 
<div id="capaSuperior" style="position:absolute; width:100%; height:100px; z-index:0; left: 0px; top: 0px;">
<jsp:include page="/WEB-INF/toptsp.jsp?encabezado=Reporte Facturas Destinatarios"/>
</div>

<div id="capaCentral" style="position:absolute; width:100%; height:85%; z-index:0; left: 0px; top: 100px; overflow: scroll;">
 <form action='<%= BASEURL %>/impresiones_pdf/reporteFacturaDestinatario/buscarReporte.jsp?numero=""' method='post' name='formulario'>
 
   <table width="30%" border="2" align="center">
   <tr>
      <td>    
           <table width='100%' align='center' class='tablaInferior'>
              <tr>
                <td class="barratitulo" >
                   <table cellpadding='0' cellspacing='0' width='100%'>
                     <tr class="fila">
                      <td width="50%" align="left" class="subtitulo1">&nbsp;Reporte</td>
                      <td width="50%" align="left"><img src="<%= BASEURL %>/images/titulo.gif" width="32" height="20" align="left"><%=datos[0]%></td>
                    </tr>
                   </table>   
				</td>
              </tr>
              <tr class="letrafila">
                   <td width='40%' class="fila" >
                        <div align="center">
  <select name="tipo" id="tipo" <%=(sw.equals("Ok")?"disabled":"")%> >
                      <option value="p" <%= (request.getParameter("tipo")!=null) && request.getParameter("tipo").equals("p")? "selected" : ""%> >Planilla</option>
                      <option value="r" <%= (request.getParameter("tipo")!=null) && request.getParameter("tipo").equals("r")? "selected" : ""%> >Remesa</option>
                    </select>
  N&deg; 
                        <input name="numero" type="text"  class="textbox" id="numero" size="10" value="<%= request.getParameter("numero")==null? "" : request.getParameter("numero")%>" maxlength="10" <%=(sw.equals("Ok")?"readonly":"")%>  > 
				     </div></td>
              </tr>
        </table>
      </td>
   </tr>
   </table>   
   <br>
   
   <!--botones -->
   
   <center>
   	 <p>
        <%if ( sw.equals("Ok") ){%>
	     <img src = "<%=BASEURL%>/images/botones/imprimir.gif" style = "cursor:hand" name = "imgimprimir"   onClick = "ImprimirReporte(tipo.value, numero.value); this.disabled=true;" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);">
        <%} else{%>
	     <img src = "<%=BASEURL%>/images/botones/buscar.gif"   style = "cursor:hand" name = "imgbuscar"     onClick = "BuscarReporte(tipo.value, numero.value);"   onMouseOver="botonOver(this);" onMouseOut="botonOut(this);">   
        <%}%>
	     <img src = "<%=BASEURL%>/images/botones/cancelar.gif" style = "cursor:hand" name = "imgcancelar"   onClick = "formulario.reset(); formulario.submit();"                                          onMouseOver="botonOver(this);" onMouseOut="botonOut(this);">
	     <img src = "<%=BASEURL%>/images/botones/salir.gif"    style = "cursor:hand" name = "imgsalir"      onClick = "parent.close();"                                                onMouseOver="botonOver(this);" onMouseOut="botonOut(this);">
</p>
   </center>
   
    <p><script>formulario.numero.focus();</script></p>
  <p><% if( !Mensaje.equals("") ){%></p>
  <table border="2" align="center">
          <tr>
            <td><table width="100%"  border="1" align="center"  bordercolor="#F7F5F4" bgcolor="#FFFFFF">
              <tr>
                <td width="260" align="center" class="mensajes"><%=Mensaje%></td>
                <td width="29" background="<%=BASEURL%>/images/cuadronaranja.JPG">&nbsp;</td>
                <td width="58">&nbsp;</td>
              </tr>
            </table></td>
          </tr>
  </table>
    <p>
      <% } %>
    </p>
    <p>
      <%
	  Vector info = null;
	  String tit = "";
	  if ( "p".equals(request.getParameter("tipo")) ) {
	      model.reporteFacDesService.listaPlanilla( request.getParameter("numero") );
	  	  info = model.reporteFacDesService.getVectorPlanilla();
		  tit = "PLANILLA";
	  }
          if ( "r".equals(request.getParameter("tipo")) ){
		  model.reporteFacDesService.listaRemesa( request.getParameter("numero") );
	      info = model.reporteFacDesService.getVectorRemesa();
		  tit = "REMESA";
	  }
		  
	  if( ( info != null  && info.size() > 0 ) ){ %>
        </p>
    <table width="100%" border="2" align="center" id="tabla1" >
    <tr>
        <td width="" >
           <table width="100%" height="50%" align="center">
              <tr>
                <td class="subtitulo1">Informaci&oacute;n de la <%=tit%> # <%=request.getParameter("numero")%> ..</td>
                <td width="50%" class="barratitulo"><img src="<%=BASEURL%>/images/titulo.gif" width="32" height="20"></td>
              </tr>
          </table>

         <table id="tabla3" width="100%">                     
				<%
						//int i = info.size();
						//HojaReportes reporte = ( HojaReportes ) info.lastElement();
						for( int i = 0; i < info.size(); i++ ){
	  						HojaReportes reporte = ( HojaReportes ) info.elementAt( i );
							
				     %>
			<tr id="titulos">
				<td align="left" nowrap class="fila" abbr="">Destinatario(s):</td>
				<td colspan="7" align="left" nowrap class="letra" abbr=""><%=(reporte.getDestinatario()!=null)?reporte.getDestinatario():""%></td>
			  </tr> 
            <tr id="titulos">
              <td  align="left"  nowrap class="fila" abbr="">Tipo Documento:</td>
              <td width="11%"  align="left"  nowrap class="letra" abbr=""><%=(reporte.getTipo_documento()!=null)?reporte.getTipo_documento():""%></td>
              <td width="8%"  align="left"  nowrap class="fila" abbr="">Documento:</td>
              <td width="12%"  align="left"  nowrap class="letra" abbr=""><%=(reporte.getDocumento()!=null)?reporte.getDocumento():""%></td>
              <td width="18%"  align="left"  nowrap class="fila" abbr="">Tipo Documento Relacionado:</td>
              <td  align="left"  nowrap class="letra" abbr=""><%=(reporte.getTipo_doc_rel()!=null)?reporte.getTipo_doc_rel():""%></td>
              <td width="15%"  align="left" nowrap class="fila" abbr=""> Documento Relacionado:</td>
              <td width="12%"  align="left" nowrap class="letra" abbr=""><%=(reporte.getDoc_rel()!=null)?reporte.getDoc_rel():""%></td>
            </tr>
            <tr id="titulos">
              <td width="11%"  align="left"  nowrap class="fila" abbr="">Fecha Cumplido:</td>
              <td colspan="3"  align="left"  nowrap class="letra" abbr=""><%=(reporte.getFecha_cum()!=null)?reporte.getFecha_cum():""%></td>
              <td  align="left"  nowrap class="fila" abbr="">Cantidad Cumplida:</td>
              <td width="13%" align="left" nowrap class="letra"  abbr=""><%=(reporte.getCantidad_cum()!=null)?reporte.getCantidad_cum():""%></td>
              <td align="left" nowrap class="fila"  abbr="">Discrepancia:</td>
              <td align="left" nowrap class="letra"  abbr=""><%=(reporte.getDiscrepancia()!=null)?reporte.getDiscrepancia():""%></td>
              </tr>
            <tr id="titulos">
            <td  align="left"  nowrap class="fila" abbr="">Observaciones:</td>
            <td colspan="7"  align="left"  nowrap class="letra" abbr=""><%=(reporte.getObservacion()!=null)?reporte.getObservacion():""%></td>
            </tr>
       		 <%}
			 }%>           
          </table> 

        </td>
    </tr>
 </table>

 </form>
</div>  
<%=datos[1]%>
</body>
</html>
