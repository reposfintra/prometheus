<%@ include file="/WEB-INF/InitModel.jsp"%>

<HTML>
<HEAD>
<TITLE></TITLE>
<META http-equiv=Content-Type content="text/html; charset=windows-1252">
<link href="../css/estilostsp.css" rel="stylesheet" type="text/css"> 
<link href="<%=BASEURL%>/css/estilostsp.css" rel="stylesheet" type="text/css"> 
</HEAD>
<BODY> 
  <table width="95%"  border="2" align="center">
    <tr>
      <td height="117" >
        <table width="100%" border="0" align="center">
          <tr  class="subtitulo">
            <td height="20"><div align="center">MANUAL DE IMPRESIONES WEB</div></td>
          </tr>
          <tr class="subtitulo1">
            <td>Descripci&oacute;n del funcionamiento del programa de Imprimir un Reporte de Facturas y Destinatarios </td>
          </tr>
          <tr>
            <td  height="18"  class="ayudaHtmlTexto">Para entrar a la p&aacute;gina WEB de impresiones de FINTRAVALORES S.A. se debe acceder a trav&eacute;s de  internet explorer la p&aacute;gina <A 
href="http://localhost:8080/fintravalores/">http://localhost:8080/fintravalores/</A>.            </td>
          </tr>
          <tr>
            <td height="18"  class="ayudaHtmlTexto"><span class="ayudaHtmlTexto">Aqu&iacute; se ingresa el usuario y el password y se escoge el perfil del funcionario. Los Jefes de Negocio o despachadores deben escoger el perfil JEFE DE NEGOCIO</span></td>
          </tr>
          <tr>
            <td  class="ayudaHtmlTexto"><div align="center"><IMG height=410 src="<%=BASEURL%>/images/ayuda/impresiones_pdf/image001.jpg" width=567 border=0 v:shapes="_x0000_i1164"></div></td>
          </tr>
          <tr>
            <td  class="ayudaHtmlTexto"><span class="ayudaHtmlTexto">Aparece el Menu. Se Ingresa a trav&eacute;s de la opci&oacute;n OPERACI&Oacute;N/DESPACHO/IMPRESI&Oacute;N/Reporte Facturas Destinatarios </span></td>
          </tr>
<tr>
            <td  class="ayudaHtmlTexto"><div align="center"><IMG height=411 src="<%=BASEURL%>/images/ayuda/impresiones_pdf/image002.jpg" width=566 border=0 v:shapes="_x0000_i1145"></div></td>
          </tr>
<tr>
            <td  class="ayudaHtmlTexto"><p class="ayudaHtmlTexto">En la siguiente pantalla se escoje si desea  el reporte de una  Planilla o Remesa, y se digita el n&uacute;mero que coresponda a dicha opci&oacute;n.</p>            </td>
          </tr>
<tr>
            <td  class="ayudaHtmlTexto"><div align="center"><IMG height=384 src="<%=BASEURL%>/images/ayuda/impresiones_pdf/image003.jpg" width=567 border=0 v:shapes="_x0000_i1143"></div></td>
          </tr>
<tr>
  <td  class="ayudaHtmlTexto">El sistema verifica que no hayan campos vacios </td>
</tr>
<tr>
  <td  class="ayudaHtmlTexto"><div align="center"><IMG height=214 src="<%=BASEURL%>/images/ayuda/impresiones_pdf/image_error001.jpg" width=567 border=0 v:shapes="_x0000_i1052"></div></td>
</tr>
<tr>
  <td  class="ayudaHtmlTexto">El sistema verifica que toda la informaci&oacute;n ingresada est&eacute; correcta y s&iacute; exista en el sistema, de lo contrario nos muestra el siguiente mensaje, y se debe corregir y volver a presionar BUSCAR</td>
</tr>
<tr>
  <td  class="ayudaHtmlTexto"><div align="center"><IMG height=214 src="<%=BASEURL%>/images/ayuda/impresiones_pdf/image_error002.jpg" width=567 border=0 v:shapes="_x0000_i1052"></div></td>
</tr>
<tr>
            <td  class="ayudaHtmlTexto"><span class="ayudaHtmlTexto">Al llenar la informaci&oacute;n debidamente y presionar BUSCAR,  en la pantalla aparecen los datos que coinciden con la b&uacute;squeda realizada</span></td>
          </tr>
<tr>
            <td  class="ayudaHtmlTexto"><div align="center"><IMG height=214 src="<%=BASEURL%>/images/ayuda/impresiones_pdf/image004.jpg" width=567 border=0 v:shapes="_x0000_i1052"></div></td>
          </tr>
<tr>
            <td  class="ayudaHtmlTexto"><span class="ayudaHtmlTexto">Al verificar que la informaci&oacute;n mostrada es la deseada para imprimir, entonces se procede a generar el archivo PDF para la impresi&oacute;n, dandole Click en el Boton Imprimir</span></td>
          </tr>
<tr>
            <td  class="ayudaHtmlTexto"><div align="center"><IMG height=214 src="<%=BASEURL%>/images/ayuda/impresiones_pdf/image005.jpg" width=567 border=0 v:shapes="_x0000_i1054"></div></td>
          </tr>
      </table></td>
    </tr>
  </table>
</BODY>
</HTML>
