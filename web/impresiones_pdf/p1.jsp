<%@page contentType="text/html"%>
<%@page session="true"%>
<%@page errorPage="/error/ErrorPage.jsp"%>
<%@page import="java.util.*"%>
<%@page import="java.util.Vector"%>
<%@page import="java.text.*"%>
<%@page import="com.tsp.pdf.*"%>
<%@page import="com.tsp.operation.model.beans.*"%>
<%@page import="com.tsp.util.*"%>
<%@page import="com.tsp.operation.model.*"%>
<%@ include file="/WEB-INF/InitModel.jsp"%>
<%@ page import="java.io.File"%>
<html>
<head>
<title>
p1
</title>
</head>
<jsp:useBean id="sistema" scope="application" class="com.tsp.pdf.Sistema"/>
<body bgcolor="#ffffff">
<h1>
JBuilder Generated JSP
</h1>
<%
  String leyenda1="1";
  String oorigen1="2";
  String dia1="3";
  String mes1="4";
  String ano1="5";
  String cargan1="6";
  String corigen1="7";
  String remitente1="8";
  String consignatario1="9";
  String destinatario1="10";
  String direccion1="11";
  String ciudad1="12";
  String planillas1="13";
  String placas1="14";
  String conductor1="15";

  Item1 item = new Item1("m1","b1","k1","me1");
  Item1 item2 = new Item1("m2","b2","k2","me2");
  Vector detalle=new Vector();
  detalle.add(item);
  detalle.add(item2);
  String leyenda2="leyenda2";

  String flete1="20";
  String pagadero1="21";
  String nit1="22";
  String ciudad11="23";
  String manifiesto1="24";
  String aduana1="25";
  String jefe1="26";

  Vector copias=new Vector();
  copias.add("TRAFICO");
  copias.add("AGENCIA");

  Proforma proforma = sistema.obtenerProforma(

  leyenda1,
  oorigen1,
  dia1,
  mes1,
  ano1,
  cargan1,
  corigen1,
  remitente1,
  consignatario1,
  destinatario1,
  direccion1,
  ciudad1,
  planillas1,
  placas1,
  conductor1,

  detalle,
  leyenda2,

  flete1,
  pagadero1,
  nit1,
  ciudad11,
  manifiesto1,
  aduana1,
  jefe1,

  copias
  );
  String ruta = application.getRealPath("/");
  File xslt = new File(ruta + "Templates/p1.xsl");
  File pdf = new File(ruta + "pdf/p1.pdf");
  proforma.generarPDF(xslt, pdf);
  response.sendRedirect(BASEURL+"/pdf/p1.pdf");
%>
</body>
</html>
