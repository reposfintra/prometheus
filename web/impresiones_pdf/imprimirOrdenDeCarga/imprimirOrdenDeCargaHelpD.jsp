<!--  
	 - Author(s)       :      LREALES
	 - Description     :      AYUDA DESCRIPTIVA - Impresion Orden de Carga en PDF
	 - Date            :      13/05/2006 
	 - Copyright Notice:      TRANSPORTES SANCHEZ POLO S.A.
-->
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN""http://www.w3.org/TR/html4/loose.dtd">
<%@page session="true"%> 
<%@page errorPage="/error/ErrorPage.jsp"%>
<%@page import="com.tsp.operation.model.beans.*, java.util.*"%>
<%@ include file="/WEB-INF/InitModel.jsp"%>
<%@ taglib uri="http://jakarta.apache.org/taglibs/input-0.90" prefix="input" %>
<html>
<head>
<title>AYUDA DESCRIPTIVA - Impresion Orden de Carga en PDF</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link href="<%= BASEURL %>/css/estilostsp.css" rel="stylesheet" type="text/css">
<script type='text/javascript' src="<%= BASEURL %>/js/boton.js"></script>
</head>

<body>
<br>
<table width="696"  border="2" align="center">
  <tr>
    <td width="811" >
      <table width="100%" border="0" align="center">
        <tr  class="subtitulo">
          <td height="24" colspan="2"><div align="center">Impresi&oacute;n</div></td>
        </tr>
        <tr class="subtitulo1">
          <td colspan="2">Impresi&oacute;n Orden de Carga en PDF - Pantalla Preliminar</td>
        </tr>
		<tr class="subtitulo1">
          <td colspan="2">INFORMACION DEL REPORTE</td>
        </tr>
        <tr>
          <td  class="fila">Orden de carga N&deg;</td>
          <td  class="ayudaHtmlTexto">Campo para ingresar el c&oacute;digo de la orden de carga, el cual es de m&aacute;ximo 12 caracteres.</td>
        </tr>
        <tr>
          <td class="fila">Bot&oacute;n Buscar</td>
          <td  class="ayudaHtmlTexto">Bot&oacute;n que valida la busqueda de el c&oacute;digo digitado en el campo.</td>
        </tr>
		<tr>
          <td class="fila">Bot&oacute;n Cancelar</td>
          <td  class="ayudaHtmlTexto">Bot&oacute;n que cancela todas los operaciones realizadas, y resetea la pagina llevandola a su estado inicial.</td>
        </tr>
		<tr>
          <td width="149" class="fila">Bot&oacute;n Salir</td>
          <td width="525"  class="ayudaHtmlTexto">Bot&oacute;n para salir de la vista 'Impresi&oacute;n' y volver a la vista del men&uacute;.</td>
        </tr>
        <tr>
          <td width="149" class="fila">Bot&oacute;n Imprimir</td>
          <td width="525"  class="ayudaHtmlTexto">Bot&oacute;n para generar el PDF de la Orden de Carga deseada y as&iacute; poder imprimirla.</td>
        </tr>
      </table>
    </td>
  </tr>
</table>
<p></p>
<center>
<img src = "<%=BASEURL%>/images/botones/salir.gif" style = "cursor:hand" name = "imgsalir" onClick = "parent.close();" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);">
</center>
</body>
</html>
